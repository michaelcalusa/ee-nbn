# Anonymous Apex Code Execution Directory
To run automated, anonymous Apex code, follow these instructions:

1. Remove any existing code that you do not wish to run.
1. Copy the code to be executed into this directory.
2. Add the filename to the text file ```index.txt```.  This file contains the names of all code files to be executed __in the order of execution__.
3. Check in code and push to the desired branch.

Execution will happen via the GoCD deployment pipeline(s).