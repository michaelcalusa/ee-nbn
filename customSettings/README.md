Developers can upload custom settings into Salesforce org using **sfdx bulk upsert** method via *GoCD pipeline*. 


This is designed to have dedicated folder for each target environment to support environment specific requirements. 

* BAUUAT
    *  index.txt
    *  CustomSettings.csv
* PreProd
* MRSF1
* MRSF2
* RSVT

# `Key Points`

Each target environment requires 2 key files
* index.txt
* Custom Settings.csv

## `index.txt`
Entries in the index.txt will be processed

## `Custom Settings file`
* Naming Convention
    * File name should match the name of the Custom Settings. 
    * sfdx bulk upsert method is used to upload the data. So, please make sure to include all columns in the file. 
    * To avoid any formatting issues, it's ideal to use text file editors to prepare the csv file & **avoid using excel (if possible)**
* File Format
    * csv is the only supported format. 


Progress and status of the load can be viewed at **"Bulk Data Load Jobs"** in target Salesforce org.