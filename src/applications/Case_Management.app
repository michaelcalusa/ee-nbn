<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>An App which provides a gateway to manage  Team Assignments, State Assignments , Case Assignment Matrix and Customer Service Teams. This has been developed as a part of nbnMosaic. This is provided to all CM and CAT Team Leaders.</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Case Management</label>
    <logo>NBN_Logos/NBN_Console_Logo.png</logo>
    <tabs>standard-Case</tabs>
    <tabs>Case_Assignment__c</tabs>
    <tabs>Secondary_Assignment__c</tabs>
    <tabs>Case_Assignment_Matrix__c</tabs>
    <tabs>Customer_Service_Team__c</tabs>
</CustomApplication>
