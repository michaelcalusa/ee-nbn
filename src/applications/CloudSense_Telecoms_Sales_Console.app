<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>CloudSense Telecoms Sales Console</label>
    <logo>NBN_Logos/NBN_Logo.png</logo>
    <tabs>csbb__Callout_Template__c</tabs>
    <tabs>csbb__Configuration__c</tabs>
    <tabs>csbb__Configuration_Association__c</tabs>
    <tabs>csord__Order__c</tabs>
</CustomApplication>
