<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Contact_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Contact</pageOrSobjectType>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Residential_Sales_Account_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Account</pageOrSobjectType>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Residential_Sales_Case_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Case</pageOrSobjectType>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Onboarding_Process_Record_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Onboarding_Process__c</pageOrSobjectType>
    </actionOverrides>
    <brand>
        <headerColor>#0070D2</headerColor>
        <logo>nbn_Masterbrand_Logo_RGB</logo>
        <logoVersion>1</logoVersion>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <description>This is Lightning app for NBN Account Management users</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Residential Sales</label>
    <navType>Standard</navType>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Access_Seeker</recordType>
        <type>Flexipage</type>
        <profile>NBN Account Management</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Access_Seeker</recordType>
        <type>Flexipage</type>
        <profile>NBN Customer Enablement Team</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Lead_Record_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Lead</pageOrSobjectType>
        <recordType>Lead.Access_Seeker</recordType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Residential_Sales_Customer_Company_Account_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Customer</recordType>
        <type>Flexipage</type>
        <profile>NBN Customer Enablement Team</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Residential_Sales_Customer_Company_Account_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Customer</recordType>
        <type>Flexipage</type>
        <profile>NBN Account Management</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Residential_Sales_Customer_Company_Account_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Company</recordType>
        <type>Flexipage</type>
        <profile>NBN Customer Enablement Team</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Residential_Sales_Customer_Company_Account_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>Account</pageOrSobjectType>
        <recordType>Account.Company</recordType>
        <type>Flexipage</type>
        <profile>NBN Account Management</profile>
    </profileActionOverrides>
    <tabs>standard-home</tabs>
    <tabs>standard-Account</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Opportunity</tabs>
    <tabs>standard-Lead</tabs>
    <tabs>standard-Case</tabs>
    <tabs>standard-report</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>standard-Feed</tabs>
    <tabs>standard-Task</tabs>
    <tabs>lmsilt__Event__c</tabs>
    <tabs>standard-Event</tabs>
    <tabs>standard-File</tabs>
    <tabs>standard-ContentNote</tabs>
    <uiType>Lightning</uiType>
</CustomApplication>
