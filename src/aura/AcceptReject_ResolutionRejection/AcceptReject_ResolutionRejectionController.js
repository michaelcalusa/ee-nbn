({
    acceptClickAction: function(component, event, helper) {
        helper.loadReasonValues(component, event, helper);
        component.set("v.startFlag",true);
     	component.set("v.declineSelectFlag", false);
        component.set("v.declineReason","");
        component.set("v.declineComment","");
        helper.enbaleDisableSubmit(component, event, helper);
        //--MR1812/CUSTSA-20339 end
    },
    declineClickAction: function(component, event, helper) {
        helper.loadReasonValues(component, event, helper);
        component.set("v.startFlag",true);
        component.set("v.acceptSelectFlag", false);
        component.set("v.declineSelectFlag", true);
        component.set("v.declineReason","");
        component.set("v.declineComment","");
        helper.enbaleDisableSubmit(component, event, helper);
    },
    performAction : function(component, event, helper) {
        component.set("v.disableActionLink",true); 
        component.set("v.docComposerComponentVisible",true);
        component.set("v.docComposerVisible",true);
    },
    handleCloseConfirmed:function(component, event, helper) {
        helper.resetAttributes(component);
    },
    submitResponse:function(component, event, helper) {
        helper.submitResponseAction(component, event, helper);
    },
    validate : function(component, event, helper){
        helper.enbaleDisableSubmit(component, event, helper);
    },
    handleChange:function(component, event, helper) {
        helper.enbaleDisableSubmit(component, event, helper);
    }
})