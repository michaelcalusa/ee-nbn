({
    //--MR1812/CUSTSA-20339 
    enbaleDisableSubmit: function(component, event, helper){
        var declineComment = component.get("v.declineComment");
        var selectedReason = component.get("v.declineReason");
        var flag = !(component.get("v.declineSelectFlag"));
        var mandatorySign = component.find("mandatorySign");
        if (flag && /Other/.test(selectedReason)) {
            $A.util.removeClass(mandatorySign, 'slds-hide');
            if(declineComment){
                component.set('v.disableSubmitButton',false);
                component.set("v.acceptSelectFlag", false);
            }
            else{
                component.set('v.disableSubmitButton',true);
            	component.set("v.acceptSelectFlag", true);
            }
        }
        else if(flag) {
            $A.util.addClass(mandatorySign, 'slds-hide');
            if(selectedReason){
                component.set('v.disableSubmitButton',false);
                component.set("v.acceptSelectFlag", false);
            }
            else{
                component.set('v.disableSubmitButton',true);
                component.set("v.acceptSelectFlag", true);
            }
        }
        else{
            if(declineComment && selectedReason)
                component.set('v.disableSubmitButton',false);
            else
                component.set('v.disableSubmitButton',true);
        }
    },
    loadReasonValues : function(component, event, helper) {
        component.set('v.ServerSideAction','c.getPickListValuesIntoList');
        component.set('v.useCache',true);
        component.set('v.ServerSideActionFaliure',$A.get("$Label.c.GenericError"));
        helper.performOperatorAction(component,{
                                                "actionType": event.target.id
                                                }, (response, component, self) => {
                                                    var records = JSON.parse(response.getReturnValue());
                                                    component.set('v.reasonValues', records.map(function (item) {
                                                        return {
                                                            label: item,
                                                            value: item
                                                        };
                                                    }));
        											component.set('v.startFlag',false);
                                                });
    },
    submitResponseAction: function(component, event, helper) {
        var declineReason = component.get("v.declineReason");
        var declineComment = component.get("v.declineComment");
        //--MR1812/CUSTSA-20339 
		var actionTypeVal = component.get("v.comboReasonLabel");
        var initialSubmitMessage;                
        //close the docked composer
        component.set("v.docComposerComponentVisible",false);
        component.set("v.confirmationModalVisible",false);
        if(!component.get('v.declineSelectFlag')){
        	component.set("v.declineReason","");
            component.set("v.declineComment","");
       	}
        var action = component.get("c.acceptRejectResolutionRejection");
        var self = this;        
        var inm = component.get("v.incidentRecordObj");
    	component.set('v.ServerSideAction','c.acceptRejectResolutionRejection');
        component.set('v.useCache',false);
        component.set('v.ServerSideActionFaliure',$A.get("$Label.c.AcceptRejectErrorMessage"));
        helper.performOperatorAction(component,{
                                                "strRecordId": inm.Id,
                                                "isDeclined": component.get('v.declineSelectFlag'),
                                                "strCurrentSLAType": inm.SLARegionandServiceRestorationTypeCalc__c,
                                                "declineReason" : declineReason,
                                                "declineComment": declineComment
                                                }, (response, component, self) => {
                                                    var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
                									addNoteRefreshEvent.fire();
                                                    var inm = response.getReturnValue();
                                                    self.fireSpinnerEvent(inm);
                                                    self.resetAttributes(component);
                                                });
    },
	resetAttributes : function(component){
    	component.set("v.docComposerVisible",false);
        component.set("v.confirmationModalVisible",false);
        component.set("v.txtNoteValue","");
        component.set('v.disableSubmitButton',true);
        component.set('v.declineReason','');
        component.set("v.declineComment","");
        component.set("v.startFlag",true);
	}
})