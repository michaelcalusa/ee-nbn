({
    performAction : function(component, event, helper){
        component.set("v.disableActionLink", true);
        helper.performOperatorAction(component,{"strRecordId": component.get("v.incidentRecordObj.Id"),
            									"strCurrentSLAType": component.get("v.incidentRecordObj.SLARegionandServiceRestorationTypeCalc__c")}, helper.callBack);
    }
})