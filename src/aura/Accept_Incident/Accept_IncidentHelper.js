({
    callBack : function(response, component, helper){
        var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
        addNoteRefreshEvent.fire();
        var inm = response.getReturnValue();
        helper.fireSpinnerEvent(inm);
    }
})