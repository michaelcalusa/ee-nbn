({
    performAction: function(component,event,helper){
        component.set('v.docComposerComponentVisible',true);
        component.set('v.docComposerVisible',true);
        component.set("v.confirmationModalVisible",false);
    },
    fetchInitData : function(component, event, helper){
        if(!component.get('v.disableWOButton')){
            helper.getWorkOrderDetailsValues(component, event, helper);
        }
    },
    setWorkOrderData : function(component, event, helper) {
        if(event.getParam("recordId") == component.get("v.incidentRecordObj.Id")){
            helper.getWorkOrderHistoryValues(component, event, helper);
        }
    },
    submitResponse:function(component, event, helper) {
        component.set('v.showWOBar', false );
        if(document.getElementById("fieldTechnician").checked) {
             component.set("v.addNoteComposerVisible",false);
             var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Submitting tech note.",
                        "message": "This should only take 10-15 secs."
            		});
                   
                    toastEvent.fire();
            helper.addTechNoteAction(component, event, helper);
        }else{
           helper.addNoteAction(component, event, helper); 
        }
	},
    handleCloseConfirmed:function(component, event, helper) {
    	component.set("v.txtNoteValue","");
        component.set("v.txtTechNoteValue","");
        component.set('v.showWOBar', false );
        helper.validate(component);
	},
    DisplayWOBar:function(component, event, helper) {
        if(component.get("v.disableWOButton") === false){
			component.set("v.showWOBar",true);
        }
        else{
            component.set("v.showWOBar",false);
        }
        helper.validate(component);
	},
    validate : function(component, event, helper){
        helper.validate(component)
    },
    ClickFirst:function(component, event, helper) {
        component.set("v.showWOBar",false);
        helper.validate(component);
	},
    ClickSecond:function(component, event, helper) {
        component.set("v.showWOBar",false);
        helper.validate(component);
	}
})