({
    addTechNoteAction: function(component, event, helper) {
             
        var currentIncident = component.get("v.incidentRecordObj");
        var techType = component.get("v.TechnologyType");
        var engageType = component.get("v.EngagementWOType");
        var specVersion = component.get("v.specVersion");
        var woNum = component.get('v.activeWO');
        var addInfo = component.get("v.txtTechNoteValue");
        var parent = component.get('v.ParentWO');
        var WRequest = component.get('v.WorkRequest');
        var WOSpecID = component.get('v.WOSpecification');
        var WOApptID = component.get('v.WOAppointmentID');
        
        var action = component.get("c.saveAddTechNotesGiven");
        var noteType = 'techNote'; 
        var seekerID = component.get('v.accessSeeker');
        
        var WOvalues = '{"woNum": "' + woNum + '","woEngageType" : "' + engageType + '","woSpecVersion": "' + specVersion + 
            '","woTechType" : "' + techType +  '","addInfo" : "' + addInfo + '","incidentId" : "' + 
            currentIncident.Name + '","strProdCat" : "' + currentIncident.Prod_Cat__c  + 
            '","strAVCId" : "' + currentIncident.AVC_Id__c + '","strPRI" : "' + currentIncident.PRI_ID__c +
            '","strAccessSeekerId" : "' + seekerID + '","parent" : "' + parent + '","WRequest" : "' + WRequest + 
            '","WOSpecID" : "' + WOSpecID + '","locationId" :"' + currentIncident.locId__c + '","WorkOrderApptID" : "' + 
            WOApptID + '","incidentName" :"' + currentIncident.Name + '","noteType" : "' + noteType + 
            '"}' ;
        
        component.set('v.ServerSideAction','c.saveAddTechNotesGiven');
        component.set('v.useCache',false);
        component.set('v.ServerSideActionFaliure',$A.get("$Label.c.AddTechNotesErrorMessage"));
        component.set('v.ServerSideActionFailureHeader',$A.get("$Label.c.TechNoteErrorHeader"));
        helper.performOperatorAction(component,{
            "strWOWrapper" : WOvalues
        }, (response, component, helper) => {
            component.set("v.txtNoteValue","");
            component.set("v.txtNoteValue","");
            component.set("v.txtTechNoteValue","");
            component.set('v.showWOBar', false );
            helper.resetAttributes(component,event,helper);
            component.set('v.disableActionLink',false);
        	helper.validate(component);
        });
        
    },
    
    
    addNoteAction: function(component, event, helper) {
        var action = component.get("c.saveAddNotesGiven");
        var currentIncident = component.get("v.incidentRecordObj");

        var noteType = '';
        if (document.getElementById("nbnExternalNote") !== null && document.getElementById("nbnExternalNote").checked) {
            noteType = 'external'
        } else if (document.getElementById("nbnInternalNote") !== null && document.getElementById("nbnInternalNote").checked) {
            noteType = 'internal'
        } 
        
        component.set('v.ServerSideAction','c.saveAddNotesGiven');
        component.set('v.useCache',false);
        component.set('v.ServerSideActionFaliure',$A.get("$Label.c.AddTechNotesErrorMessage"));
        component.set('v.ServerSideActionFailureHeader',$A.get("$Label.c.TechNoteErrorHeader"));
        
        helper.performOperatorAction(component,{
            "incidentID": currentIncident.Id,
            "noteType": noteType,
            "txtNoteValue": component.get("v.txtNoteValue")
        }, (response, component, helper) => {
            var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
			addNoteRefreshEvent.fire();
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
            "type": "success",
            "message": $A.get("$Label.c.AddNotesSuccessMessage")
            });
            component.set("v.txtNoteValue","");
            component.set("v.txtNoteValue","");
            component.set("v.txtTechNoteValue","");
            component.set('v.showWOBar', false );
            helper.resetAttributes(component,event,helper);
        	helper.validate(component);
            toastEvent.fire();
        	component.set('v.disableActionLink',false);
        });
    },
    
    getWorkOrderHistoryValues: function(component, event, helper) {
        let wohistorywrap = event.getParam('wohistorywrapper'); 
        if(wohistorywrap && wohistorywrap.techNote){
            var result = wohistorywrap.techNote;
            var size = result.nbnworkhistoryview.length;
            var v = 0;
            console.log('length is -' + size);
            if(result.nbnworkhistoryview.length > 1){
                for (var i = 0; i < result.nbnworkhistoryview.length; i++){
                    if((result.nbnworkhistoryview[i].status === $A.get("$Label.c.Jigsaw_TechNoteActiveWO") ||
                        result.nbnworkhistoryview[i].status === $A.get("$Label.c.Jigsaw_techNoteActiveWO1"))){
                        v = i;
                    }
                }
            }
            component.set('v.activeWO',result.nbnworkhistoryview[v].wonum );
            component.set('v.EngagementWOType',result.nbnworkhistoryview[v].nbnrequirement );
            component.set('v.ParentWO',result.nbnworkhistoryview[v].parent );
            component.set('v.accessSeeker', result.nbnworkhistoryview[v].nbnworkforce);
            component.set('v.TechnologyType',result.nbnaccesstech);
            component.set('v.disableWOButton', false );
        }
        else{  
            component.set("v.disableWOButton", true);
            component.set('v.showWOBar', false );
        }
    },
    getWorkOrderDetailsValues: function(component, event, helper) {
        var action = component.get("c.getDetailWorkOrderValues");
        action.setParams({
            woNumber : component.get('v.activeWO')      
        });
        action.setCallback(this, function(data) {
            var response = data.getState();            
            if(response === 'SUCCESS')
            {	
                var WODetailWrapper = data.getReturnValue();
                console.log('WODetail Add note' + WODetailWrapper.nbnwospecversion);
                if(!$A.util.isEmpty(WODetailWrapper) && !$A.util.isUndefined(WODetailWrapper)){
                    var engType = component.get('v.EngagementWOType');
                    if((WODetailWrapper.nbnwospecversion == '4.3.0' || WODetailWrapper.nbnwospecversion == '2.3.0') && (component.get('v.TechnologyType') =='FTTB' || component.get('v.TechnologyType') == 'FTTN') && (engType =='COMMITMENT' || engType == 'APPOINTMENT')){
                        console.log('WO values in Add note second API -' + JSON.stringify(WODetailWrapper));
                        component.set('v.specVersion',WODetailWrapper.nbnwospecversion);
                        component.set('v.WOSpecification',WODetailWrapper.nbnwospecid);
                        component.set('v.disableWOButton', false );
                                     
                    }else{
                         component.set("v.disableWOButton", true);
                   		 component.set('v.showWOBar', false );
                    }                  
                }else{
                     component.set("v.disableWOButton", true);
                     component.set('v.showWOBar', false );
                }
            }else{
                   component.set("v.disableWOButton", true);
                   component.set('v.showWOBar', false );
            }
        });
        $A.enqueueAction(action);  
    },
    validate : function(component){
        if((component.get('v.txtNoteValue').trim().length > 0 && !component.get('v.showWOBar') ||
            component.get('v.txtTechNoteValue').trim().length > 0 && component.get('v.showWOBar'))){
            component.set('v.disableSubmitButton',false);
        }
        else{
            component.set('v.disableSubmitButton',true);
        }
    }
})