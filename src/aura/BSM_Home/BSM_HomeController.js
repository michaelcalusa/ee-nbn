/**
 * Created by gobindkhurana on 9/7/18.
 */
({
     init: function (cmp, event, helper) {	 
     // Create the action
     var action = cmp.get("c.getBizCommunityPermissions");

     var communityPermissions;

      // Add callback behavior for when response is received
     action.setCallback(this, function(response) {
     var state = response.getState();
     if (state === "SUCCESS") {
     communityPermissions = response.getReturnValue();
     console.log('BSM_HomeController.getBizCommunityPermissions - Successful response received - communityPermissions: ' + communityPermissions);

         if(communityPermissions.includes('Has_EE_Community_Permissions__c')){
            cmp.set("v.hasEEAccess", true);
         }  
         if(communityPermissions.includes('Has_NS_Community_Permissions__c')){
            cmp.set("v.hasNSAccess", true);
         }
         if(communityPermissions.includes('Has_NE_Community_Permissions__c')){
            cmp.set("v.hasNEAccess", true);
         }
         
         console.log('v.hasEEAccess'+cmp.get("v.hasEEAccess"));
         console.log('v.hasNSAccess'+cmp.get("v.hasNSAccess"));
         console.log('v.hasNEAccess'+cmp.get("v.hasNEAccess"));

     }   else {
     console.log('BSM_HomeController.getBizCommunityPermissions - communityPermissions- Failed response with state: ' + state);
     }

     });
     // Send action off to be executed
     $A.enqueueAction(action);
   }
})