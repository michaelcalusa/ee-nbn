({
    
    cancelOppty: function(component, event, helper) {      
        var action = component.get("c.updateOpptyStage");
        var recId = component.get("v.recordId");
        console.log(recId);
        action.setParams({
            "recId": recId 
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            //console.log(state);
        });
        $A.enqueueAction(action);
        $A.get('e.force:refreshView').fire();
        $A.get("e.force:closeQuickAction").fire();
    } ,
    
    closeWindow:function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
    /*doInit: function(component, event, helper) {
        // Create the action
        var action = component.get("c.getReportId");
        
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.reportId", response.getReturnValue());
            }
            
        });
        $A.enqueueAction(action);
    }*/
})