({
	myAction : function(component, event, helper) {
        var action = component.get("c.getStates");   
        action.setCallback(this, function(response){            
            var state = response.getState();   
            if (state === "SUCCESS") {                 
                var options = response.getReturnValue();
                var opt = [];
                for(var o=0; o<=options.length-1; o++)
                {                    
                    opt.push(options[o]);
                }               
                component.set("v.stateListValues",opt);
            }            
            else if (state === "ERROR") {
                console.log('error==');
            }
        });
        $A.enqueueAction(action);
        
	},
    displayChanged : function(component, event, helper){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(wrapInstance.yourDtlWrapper != undefined){
            if(wrapInstance.yourDtlWrapper.role == 'All Other requestors'){
                component.set("v.readOnlySameDetails",true);
            }
            else{
                component.set("v.readOnlySameDetails",false);
            }
            if(component.get("v.sameAsYourDetails")){
                helper.copydetailsHelper(component, event, helper, true);
            }
        }
        helper.copyTOwrapInstance(component, event, helper, true);
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
        
    },
    handleCollapsableSection : function(component, event, helper){
       if(!component.get("v.disableSection")){
        component.set("v.displaySection",!(component.get("v.displaySection")));
        if(component.get("v.displaySection")){
            var disEvent = component.getEvent("displaySectionChange");
            disEvent.setParams({
                "firedComponentName" : 'ContractSignatory'
            });
            disEvent.fire();
        }
       }
    },
    processCustomerPersonalDetails : function(component, event, helper){
        	var div = document.getElementById("cpd-containerId");
            var subDiv = document.querySelectorAll('input, select');
            var myArray = [];
        	myArray.push('cpd-streetAddress');
            for(var i = 0; i < subDiv.length; i++) {
                var elem = subDiv[i];
                if(elem.id.indexOf('cpd-') === 0) {
                        myArray.push(elem.id);    
                }
            }
        	var errorCarier = [];
            for(var inputElementId=0; inputElementId<myArray.length; inputElementId++){
                var errorDivId = document.getElementById(myArray[inputElementId]+'-div');
                var errorSpanId = document.getElementById(myArray[inputElementId]+'-error'); 
                var fieldId = myArray[inputElementId];   
                var fieldVal;
                if (fieldId == 'cpd-streetAddress')
            	{
            	    console.log(component.get("v.addressStreetName"));
                	fieldVal = component.get("v.addressStreetName"); 
            	}
                else
                {
                    fieldVal = document.getElementById(myArray[inputElementId]).value;
                }
                var err = helper.validatehelper(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);
                errorCarier.push(err);
            }
        	
        	component.set('v.hasError', false);
            for(var i = 0; i < errorCarier.length; i++){
                if(errorCarier[i] == true){
                    component.set('v.hasError', true);
                } 
            }
            if(!component.get("v.hasError")){
                helper.copyTOwrapInstance(component, event, helper, true);
            	var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
                document.getElementById('cpd-givenName').value = wrapInstance.conSignWrapper.givenName;
                document.getElementById('cpd-surName').value = wrapInstance.conSignWrapper.surName;
                document.getElementById('cpd-emailAddress').value = wrapInstance.conSignWrapper.emailAddress;
                document.getElementById('cpd-phoneNumber').value = wrapInstance.conSignWrapper.phoneNumber;
                document.getElementById('cpd-additionalPhoneNumber').value = wrapInstance.conSignWrapper.additionalPhoneNumber;
                wrapInstance.conSignWrapper.contractSignSameAsApplicant = component.get("v.sameAsYourDetails");
                console.log('document.getElementById(cpd-state).value '+document.getElementById('cpd-state').value);
                wrapInstance.conSignWrapper.state = document.getElementById('cpd-state').value;
                wrapInstance.conSignWrapper.addressStreetName = component.get('v.addressStreetName');
                component.set("v.addressState",wrapInstance.conSignWrapper.state);
                component.set("v.selectedAddress",component.get('v.addressStreetName'));
                component.set("v.newDevApplicantWrapperInstance",wrapInstance);
                
                var action = component.get("c.saveNewApplicant");
                action.setParams({
                    "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance"))
                });
                action.setCallback(this, function(response){
                    if(response.getState() == 'SUCCESS'){
                        var wrapInstanceTemp = component.get("v.newDevApplicantWrapperInstance");
                        wrapInstanceTemp.newDevApplicantId = response.getReturnValue();
                        component.set("v.newDevApplicantWrapperInstance",wrapInstanceTemp);
                        var saveEvent = component.getEvent("saveRecEvent");
                        saveEvent.setParams({
                            "wrapperInstance" : component.get("v.newDevApplicantWrapperInstance"),
                            "componentSaved" : 'contractSignatory'
                        });
                        saveEvent.fire();
                    }
                    else{
                        console.log(response.getError()[0].message);
                    }
                });
                $A.enqueueAction(action); 
                 if(wrapInstance.yourDtlWrapper.role == 'All Other requestors'){
                   
                    var updatedWrapInstance = component.get("v.newDevApplicantWrapperInstance");
                    var action2 = component.get("c.getServiceDeliveryType");
                    var pc;           
                    if(updatedWrapInstance.devDtlWrapper.numberOfPremises!=undefined && updatedWrapInstance.devDtlWrapper.numberOfEssentialServices!=undefined){
                        pc = +updatedWrapInstance.devDtlWrapper.numberOfPremises + +updatedWrapInstance.devDtlWrapper.numberOfEssentialServices;
                    }
                    else
                    {
                        pc = updatedWrapInstance.devDtlWrapper.numberOfPremises;
                    }
                    
                    action2.setParams({ 
                        "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance")),              
                        "isEssentialService" : updatedWrapInstance.devDtlWrapper.essentialServices,
                        "buildType" : updatedWrapInstance.devDtlWrapper.whatAreYouBuilding,
                        "MTM" : updatedWrapInstance.devMapInfo.techType,
                        "premiseCount" : pc,
                        "isPitPipePrivate" : updatedWrapInstance.pitAndPipeDetailsWrapper.isPitAndPipePrivate,
                        "entity": updatedWrapInstance.billingDtlWrapper.RegisteredEntityName,
                        "entityCode" : updatedWrapInstance.billingDtlWrapper.entityTypeCode
                    });
                    action2.setCallback(this, function(response){
                        if(response.getState() == 'SUCCESS'){
                            component.set("v.serviceDeliveryDtlsMap",response.getReturnValue());                    
                            // get CostDetails and flag to display costdetails component 
                            var sendAppTo; 
                            var isTechnicalAssessment = false;
                            var mapSD = component.get("v.serviceDeliveryDtlsMap");                   
                            var action3 = component.get("c.costDetails");
                            var sdType = mapSD['ServiceDelivery'];
                            var dType =  mapSD['DwellingType'];
                            var cusClass =  'Class3/4';
                            if((sdType == 'SD-2' && cusClass == 'Class3/4') || (sdType == 'Technical Assessment')){
                                sendAppTo = 'Salesforce';
                                if(sdType == 'Technical Assessment'){
                                    isTechnicalAssessment = true;
                                    sdType = 'SD Unknown'
                                }                        
                            }
                            else if(sdType == 'SD-1' || cusClass == 'Class1/2'){
                                sendAppTo = 'CRMoD';
                            } 
                            var wrapInstanceTemp = component.get("v.newDevApplicantWrapperInstance");
                            wrapInstanceTemp.serviceDeliveryDtlWrapper.serviceDeliveryType = sdType;
                            wrapInstanceTemp.serviceDeliveryDtlWrapper.dwellingType = dType;
                            wrapInstanceTemp.serviceDeliveryDtlWrapper.upgradedCustomerClass = cusClass;
                            wrapInstanceTemp.serviceDeliveryDtlWrapper.sendApplicationTo = sendAppTo;
                            wrapInstanceTemp.serviceDeliveryDtlWrapper.TechnicalAssessment = isTechnicalAssessment;
                            wrapInstanceTemp.billingDtlWrapper.custType = 'Residential';
                            component.set("v.newDevApplicantWrapperInstance",wrapInstanceTemp);                            
                            action3.setParams({ 
                                "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance")), 
                            });
                            action3.setCallback(this, function(response){
                                if(response.getState() == 'SUCCESS'){                            
                                    component.set("v.costDtlMap",response.getReturnValue());
                                    var mapCost = component.get("v.costDtlMap");                            
                                    var action4 = component.get("c.saveNewApplicant");                    	                       
                                    var displayCostDetails = mapCost['DisplayPaymentDetails']; 
                                    var wrapInstanceTemp = component.get("v.newDevApplicantWrapperInstance");                    		
                                    component.set("v.newDevApplicantWrapperInstance",wrapInstanceTemp);                            
                                    action4.setParams({ 
                                        "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance")), 
                                    });
                                    action4.setCallback(this, function(response){
                                        if(response.getState() == 'SUCCESS'){                               
                                            // fire costDetailsEvent
                                            var costDetailsEvent = component.getEvent("costDtlsEvent");
                                            costDetailsEvent.setParams({
                                                "wrapperInstance" : component.get("v.newDevApplicantWrapperInstance"), 
                                                "displayCostDtls": displayCostDetails
                                            });
                                            costDetailsEvent.fire();   
                                            // fire saveEvent
                                            var saveEvent = component.getEvent("saveRecEvent");
                                            saveEvent.setParams({
                                                "wrapperInstance" : component.get("v.newDevApplicantWrapperInstance"),
                                                "componentSaved" : 'costDetails'
                                            });
                                            saveEvent.fire();
                                        }
                                        else{
                                            console.log(response.getState()+' message is '+response.getError()[0].message);
                                        }
                                    });
                                    $A.enqueueAction(action4);
                                }
                                else{
                                    console.log(response.getState()+' message is '+response.getError()[0].message);
                                }
                            });
                            $A.enqueueAction(action3);
                        }
                        else{
                            console.log(response.getState()+' message is '+response.getError()[0].message);
                        }
                    });
                    
                    $A.enqueueAction(action2);                    
                }
                // END 
            }
            else{
                console.log('errorrs');
            }
        	
    },
    validateFields : function(cmp, event, helper){
        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);       
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;
        
        var fieldId = event.currentTarget.id;       
        var fieldVal = document.getElementById(fieldId).value;
        helper.validatehelper(cmp, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);
    },
    copyYourDetails : function(component, event, helper){
        var checkId = event.currentTarget.id;
        var checked = document.getElementById(checkId).checked;
        component.set("v.sameAsYourDetails",checked);
        helper.copydetailsHelper(component, event, helper, checked);
    },
   populateAddressInfo : function(component, event, helper) {  
       if(event.getParam('cmpName')=='ContractSignatory')
       {
           	var mapAddressResult = event.getParam('mapAddressResults');
            component.set("v.disableAdd", true);
           	var displaySelectedAdd = mapAddressResult["displaySelectedAddress"];
            var streetName = mapAddressResult["StreetName"]; 
            var suburb = mapAddressResult["Suburb"];
            var state = mapAddressResult["State"]; 
            var postalCode = mapAddressResult["PostalCode"];
           
            component.set("v.selectedAddress",displaySelectedAdd);
            component.set("v.addressStreetName", streetName);
            component.set("v.addressSuburb", suburb);
            component.set("v.addressState", state);
            component.set("v.addressPostalCode", postalCode);
            var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
            wrapInstance.conSignWrapper.townSuburb = suburb;
            wrapInstance.conSignWrapper.state = state;
            wrapInstance.conSignWrapper.postalCode = postalCode;
           wrapInstance.conSignWrapper.addressStreetName = streetName;
            component.set("v.newDevApplicantWrapperInstance",wrapInstance);
           helper.copyTOwrapInstance(component, event, helper, false);
       }
        
    },
    enableAddress : function(component, event, helper){
       component.set("v.disableAdd", false);
    },
    
    populateState: function(component, event, helper){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
		component.set("v.addressState",document.getElementById('cpd-state').value);//wrapInstance.billingDtlWrapper.addressState);
        console.log('insied populate change '+wrapInstance.billingDtlWrapper.addressState+' xxx '+document.getElementById('cpd-state').value);
        
    },
    handlenoResultEvent : function(component, event, helper){
        var streetAddress = event.getParam('streetAddress');
        component.set("v.addressStreetName", streetAddress);
        //component.set("v.selectedAddress", streetAddress);
        //var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        //wrapInstance.conSignWrapper.addressStreetName = streetAddress;
        //component.set("v.newDevApplicantWrapperInstance",wrapInstance);
    }
})