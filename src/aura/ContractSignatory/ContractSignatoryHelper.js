({
    validatehelper : function(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal){
        var errorflag = false;
        if(fieldId == 'cpd-additionalPhoneNumber' && fieldVal != ''){
            if(((fieldVal.substring(0,2) != '02' && fieldVal.substring(0,2) != '03' && fieldVal.substring(0,2) != '04' && fieldVal.substring(0,2) != '07' && fieldVal.substring(0,2) != '08') || fieldVal.length != 10 || !fieldVal.match(/^[0-9]+$/))){
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid Phone number';
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorflag = true;
            }
            else{
                $A.util.removeClass(errorSpanId,'help-block-error');               
                $A.util.removeClass(errorDivId,'has-error');              
                $A.util.addClass(errorDivId,'has-success');
            }
        }
        else if(fieldVal == '' && fieldId != 'cpd-additionalPhoneNumber'){
            $A.util.addClass(errorSpanId,'help-block-error');               
            $A.util.addClass(errorDivId,'has-error');              
            $A.util.removeClass(errorDivId,'has-success');               
            errorflag = true; 
        }
        else if(fieldId == 'cpd-emailAddress' && !fieldVal.match( /^[^\s@]+@[^\s@]+\.[^\s@]+$/)){
            document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid email address';
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
            errorflag = true;
        }
        else if((fieldId=='cpd-phoneNumber') && ((fieldVal.substring(0,2) != '02' && fieldVal.substring(0,2) != '03' && fieldVal.substring(0,2) != '04' && fieldVal.substring(0,2) != '07' && fieldVal.substring(0,2) != '08') || fieldVal.length != 10 || !fieldVal.match(/^[0-9]+$/))){
            document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid phone number';
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
            errorflag = true;
        }
        else if((fieldId=='cpd-postcode')){
            if((!fieldVal.match(/^[0-9]+$/) || (fieldVal.length != 4 && fieldVal.length != 3))){
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid post code';
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorflag = true;
            }
            else{
                var postcodeValid = false;
                if((fieldVal >= 1000 && fieldVal <= 1999) || (fieldVal >= 2000 && fieldVal <= 2599) || (fieldVal >= 2620 && fieldVal <= 2899) || (fieldVal >= 2921 && fieldVal <= 2999)){
                   document.getElementById('cpd-state').value = 'NSW';
                   postcodeValid = true;
                }
                else if((fieldVal >= 200 && fieldVal <= 299) || (fieldVal >= 2600 && fieldVal <= 2619) || (fieldVal >= 2900 && fieldVal <= 2920)){
                    document.getElementById('cpd-state').value = 'ACT';
                    postcodeValid = true;
                }
                else if((fieldVal >= 3000 && fieldVal <= 3999) || (fieldVal >= 8000 && fieldVal <= 8999)){
                    document.getElementById('cpd-state').value = 'VIC';
                    postcodeValid = true;
                }
                else if((fieldVal >= 4000 && fieldVal <= 4999) || (fieldVal >= 9000 && fieldVal <= 9999 )){
                    document.getElementById('cpd-state').value = 'QLD';
                    postcodeValid = true;
                }
                else if((fieldVal >= 5000 && fieldVal <= 5799) || (fieldVal >= 5800 && fieldVal <= 5999 )){
                    document.getElementById('cpd-state').value = 'SA';
                    postcodeValid = true;
                }
                else if((fieldVal >= 6000 && fieldVal <= 6797) || (fieldVal >= 6800 && fieldVal <= 6999 )){
                    document.getElementById('cpd-state').value = 'WA';
                    postcodeValid = true;
                }
                else if((fieldVal >= 7000 && fieldVal <= 7799) || (fieldVal >= 7800 && fieldVal <= 7999 )){
                    document.getElementById('cpd-state').value = 'TAS';
                    postcodeValid = true;
                }
                else if((fieldVal >= 800 && fieldVal <= 899) || (fieldVal >= 900 && fieldVal <= 999  )){
                    document.getElementById('cpd-state').value = 'NT';
                    postcodeValid = true;
                }
                if(postcodeValid){
                    $A.util.removeClass(errorSpanId,'help-block-error');               
                    $A.util.removeClass(errorDivId,'has-error');              
                    $A.util.addClass(errorDivId,'has-success');
                }
                else{
                   /* document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid post code';
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;*/
                }
            }
        }
        else if(fieldId=='cpd-state'){
            var validState = false;
            var postCode = document.getElementById('cpd-postcode').value;
            if(fieldVal == 'NSW' && ((postCode >= 1000 && postCode <= 1999) || (postCode >= 2000 && postCode <= 2599) || (postCode >= 2620 && postCode <= 2899) || (postCode >= 2921 && postCode <= 2999))){
                validState = true;
            }
            else if(fieldVal == 'ACT' && ((postCode >= 200 && postCode <= 299) || (postCode >= 2600 && postCode <= 2619) || (postCode >= 2900 && postCode <= 2920))){
                validState = true;
            }
            else if(fieldVal == 'VIC' && ((postCode >= 3000 && postCode <= 3999) || (postCode >= 8000 && postCode <= 8999))){
                validState = true;
            }
            else if(fieldVal == 'QLD' && ((postCode >= 4000 && postCode <= 4999) || (postCode >= 9000 && postCode <= 9999 ))){
                validState = true;
            }
            else if(fieldVal == 'SA' && ((postCode >= 5000 && postCode <= 5799) || (postCode >= 5800 && postCode <= 5999 ))){
                validState = true;
            }
            else if(fieldVal == 'WA' && ((postCode >= 6000 && postCode <= 6797) || (postCode >= 6800 && postCode <= 6999 ))){
                validState = true;
            }
            else if(fieldVal == 'TAS' && ((postCode >= 7000 && postCode <= 7799) || (postCode >= 7800 && postCode <= 7999 ))){
                validState = true;
            }
            else if(fieldVal == 'NT' && ((postCode >= 800 && postCode <= 899) || (postCode >= 900 && postCode <= 999  ))){
                validState = true;
            }
            if(validState){
                    $A.util.removeClass(errorSpanId,'help-block-error');               
                    $A.util.removeClass(errorDivId,'has-error');              
                    $A.util.addClass(errorDivId,'has-success');
                }
                else{
                    /*document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Selected state and post code does not match.';
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;*/
                }
        }
        else{
            $A.util.removeClass(errorSpanId,'help-block-error');               
            $A.util.removeClass(errorDivId,'has-error');              
            $A.util.addClass(errorDivId,'has-success');
        }
        
        return errorflag;
    },
    copyTOwrapInstance : function(component, event, helper, copyAddressDetails){
		var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(document.getElementById('cpd-givenName') != null){
            wrapInstance.conSignWrapper.givenName = document.getElementById('cpd-givenName').value;
        }
        if(document.getElementById('cpd-surName') != null){
            wrapInstance.conSignWrapper.surName = document.getElementById('cpd-surName').value;
        }
        if(document.getElementById('cpd-emailAddress') != null){
            wrapInstance.conSignWrapper.emailAddress = document.getElementById('cpd-emailAddress').value;
        }
        if(document.getElementById('cpd-phoneNumber') != null){
            wrapInstance.conSignWrapper.phoneNumber = document.getElementById('cpd-phoneNumber').value;
        }
        if(document.getElementById('cpd-additionalPhoneNumber') != null){
            wrapInstance.conSignWrapper.additionalPhoneNumber = document.getElementById('cpd-additionalPhoneNumber').value;
        }
        if(copyAddressDetails){
            if(document.getElementById('cpd-townSuburb') != null){
                wrapInstance.conSignWrapper.townSuburb = document.getElementById('cpd-townSuburb').value;
            }
            if(document.getElementById('cpd-postcode') != null){
                wrapInstance.conSignWrapper.postalCode = document.getElementById('cpd-postcode').value;
            }
            if(document.getElementById('cpd-state') != null){
                wrapInstance.conSignWrapper.state = document.getElementById('cpd-state').value;
            }
            component.set("v.selectedAddress", component.get("v.addressStreetName"));
        }
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
    },
    
    copydetailsHelper : function(component, event, helper, copy){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(copy){
            wrapInstance.conSignWrapper.givenName = wrapInstance.yourDtlWrapper.givenName;
            wrapInstance.conSignWrapper.surName = wrapInstance.yourDtlWrapper.surName;
            wrapInstance.conSignWrapper.emailAddress = wrapInstance.yourDtlWrapper.emailAddress;
            wrapInstance.conSignWrapper.phoneNumber = wrapInstance.yourDtlWrapper.phoneNumber;
            wrapInstance.conSignWrapper.additionalPhoneNumber = wrapInstance.yourDtlWrapper.additionalPhoneNumber;
            component.set("v.readOnlyAttribute",true);
        }
        else{
            wrapInstance.conSignWrapper.givenName = '';
            wrapInstance.conSignWrapper.surName = '';
            wrapInstance.conSignWrapper.emailAddress = '';
            wrapInstance.conSignWrapper.phoneNumber = '';
            wrapInstance.conSignWrapper.additionalPhoneNumber = '';
            component.set("v.readOnlyAttribute",false);
        }
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
    }
})