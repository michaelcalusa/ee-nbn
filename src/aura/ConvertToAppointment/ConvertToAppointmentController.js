({
    buttonhovered : function(component, event, helper) {
        var toggleText = component.find("buttonTxtNote");
        $A.util.toggleClass(toggleText, "btnAffirmative");
    },
    openRequestInfo : function(component, event, helper) {
        component.set("v.addNoteComponentVisible",true);  
        component.set("v.addNoteComposerVisible",true);
        component.set("v.disableActionLink", true);
    },
    SubmitConvertToAppointment:function(component, event, helper) {
        // fire the event for spinners to start
        var varCurrentIncident = component.get("v.attrCurrentIncidentRecord");
        var currentWorkOrderStatus = varCurrentIncident.inm.Current_work_order_status__c;
        var currentCommitmentWorkOrderStatus = (currentWorkOrderStatus && currentWorkOrderStatus.toUpperCase() != 'HELD' && currentWorkOrderStatus.toUpperCase() != 'PENDING' && currentWorkOrderStatus.toUpperCase() != 'COMPLETED' && currentWorkOrderStatus.toUpperCase() != 'CANCELLED' && currentWorkOrderStatus.toUpperCase() != 'CLOSED');
        component.set("v.addNoteComposerVisible",false);
        if(varCurrentIncident.inm.Engagement_Type__c == "Commitment" && ((!currentWorkOrderStatus && varCurrentIncident.inm.AppointmentId__c && varCurrentIncident.inm.Appointment_Status__c && varCurrentIncident.inm.Appointment_Status__c.toUpperCase() == "RESERVED") || (currentWorkOrderStatus && currentCommitmentWorkOrderStatus))) {
            var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
            fireSpinnerRefresh.setParams({"userAction" :"cancelCommitment", "stopSpinner" : false, "spinnersToStart": "IncidentStatus,SLA", "currentRecordId" : varCurrentIncident.inm.Id});
        	fireSpinnerRefresh.fire();
            var resultsToast = $A.get("e.force:showToast");
        	resultsToast.setParams({
                "title": "Submitting convert to appointment.",
                "message":"This should only take 10-15 secs."
            });
        	resultsToast.fire();
        	helper.submitRequestForConvertToAppointment(component, event, helper);
        }
        else {
           helper.submitRequestForConvertToAppointmentInconclusive(component, event, helper);
        }
    },
    handleMinimizeMaximize:function(component, event, helper) {
        var section = component.find("dcSection");        
        if($A.util.hasClass(section, 'slds-is-open'))
        {            
            $A.util.addClass(section, 'slds-is-closed');
            $A.util.removeClass(section, 'slds-is-open');
        }
        else
        {            
            $A.util.removeClass(section, 'slds-is-closed');
            $A.util.addClass(section, 'slds-is-open');
        }
    },
    handleClose:function(component, event, helper) {
        component.set("v.confirmationModalVisible",true);
    },
    handleCloseConfirmed:function(component, event, helper) {
        component.set("v.addNoteComposerVisible",false);
        component.set("v.confirmationModalVisible",false);
        component.set("v.disableActionLink", false);
    },
    handleCancelConfirmationModal:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
    },
    toggleTextValue :function(component, event, helper) {
        component.set("v.templateChangeConfirmationModalVisible", false);  
        helper.getToggleTextValue(component, event, helper);
    }  
})