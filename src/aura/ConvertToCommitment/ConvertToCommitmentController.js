({
    buttonhovered : function(component, event, helper){
        var toggleText = component.find("buttonTxtNote");
        $A.util.toggleClass(toggleText, "btnAffirmative");
    },
    openRequestInfo : function(component, event, helper) {
        //Fire Summary Table Event To Dismiss The Section While Actions Are Performed
        //var summaryTableEvent = component.getEvent("handleSummaryTableEvent");
        //summaryTableEvent.fire();
        component.set("v.addNoteComponentVisible",true);  
        component.set("v.addNoteComposerVisible",true);
        component.set("v.disableActionLink", true);
    },
    SubmitConvertToCommitment:function(component, event, helper) {
        // fire the event for spinners to start
        component.set("v.addNoteComposerVisible",false);
        var varCurrentIncident = component.get("v.attrCurrentIncidentRecord");
        //Updated as part of CUSTA-20472 user story - Check appointment status if it is already cancelled or expired don't send cancel appointment request for cancellation to Maximo.
        if(varCurrentIncident.EngagementType === "Appointment" && varCurrentIncident.inm.AppointmentId__c && varCurrentIncident.inm.Appointment_Status__c && varCurrentIncident.inm.Appointment_Status__c != 'EXPIRED' && varCurrentIncident.inm.Appointment_Status__c != 'CANCELLED') {
            var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
            fireSpinnerRefresh.setParams({"userAction" :"cancelAppointment", "stopSpinner" : false, "spinnersToStart": "IncidentStatus", "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
            fireSpinnerRefresh.fire();
        	var resultsToast = $A.get("e.force:showToast");
       		resultsToast.setParams({
                "title": "Submitting convert to commitment.",
                "message":"This should only take 10-15 secs."
            });
        	resultsToast.fire();
        	helper.submitRequestForConvertToCommitment(component, event, helper);
        }
        else {
            helper.submitRequestForConvertToCommitmentInconclusive(component, event, helper);
        }
    },
    handleMinimizeMaximize:function(component, event, helper) {
        var section = component.find("dcSection");        
        if($A.util.hasClass(section, 'slds-is-open'))
        {            
            $A.util.addClass(section, 'slds-is-closed');
            $A.util.removeClass(section, 'slds-is-open');
        }
        else
        {            
            $A.util.removeClass(section, 'slds-is-closed');
            $A.util.addClass(section, 'slds-is-open');
        }
    },
    handleClose:function(component, event, helper) {
        component.set("v.confirmationModalVisible",true);
    },
    handleCloseConfirmed:function(component, event, helper) {
        component.set("v.addNoteComposerVisible",false);
        component.set("v.confirmationModalVisible",false);
        component.set("v.disableActionLink", false);
    },
    handleCancelConfirmationModal:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
    },
    toggleTextValue :function(component, event, helper) {
        component.set("v.templateChangeConfirmationModalVisible", false);
        helper.getToggleTextValue(component, event, helper); 
    }
})