({
    submitRequestForConvertToCommitmentInconclusive : function(component, event, helper) {
        var action = component.get("c.submitRequestInconclusive");
        action.setParams({
            "incidentID": component.get("v.attrCurrentIncidentRecord").inm.Id,
            "noteType": "",
            "txtNoteValue": "",
            "UserAction" : "changeEUETCommitment"
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                var varattrCurrentIncientRecord = response.getReturnValue();
                var spinnersToFire = '';
                if (varattrCurrentIncientRecord) {
                    if(varattrCurrentIncientRecord.Awaiting_Current_Incident_Status__c)
                        spinnersToFire = 'IncidentStatus';
                    if(varattrCurrentIncientRecord.Awaiting_Current_SLA_Status__c)
                        spinnersToFire = spinnersToFire + 'SLA';
                    var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
                    spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : varattrCurrentIncientRecord, "spinnersToStart" : spinnersToFire, "stopSpinner" : false});
                    spinnerHandlerEvent.fire();
                    component.set("v.addNoteComposerVisible",false);
                    component.set("v.confirmationModalVisible",false);
                    component.set("v.txtNoteValue","");
                } else {                    
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Something went wrong. Please contact system support team"
                    });
                    toastEvent.fire();
                    component.set("v.disableActionLink", false);
                }
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    submitRequestForConvertToCommitment: function(component, event, helper) {
        var action = component.get("c.cancelAppointment");
        action.setParams({
            "recId": component.get("v.attrCurrentIncidentRecord").inm.Id,
            "SObjectType": "Incident_Management__c",
            "closureCode": "RES-CAN-NBN",
            "integrationType" : "cancelAppointment",
            "noteType": "external",
            "txtNoteValue": ""
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                if (response.getReturnValue() == '') {
                    component.set("v.confirmationModalVisible",false);   
                } else {
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Maximo has failed to cancel the appointment/commitment. Please contact system support team"
                    });
                    toastEvent.fire(); 
                    component.set("v.disableActionLink",false);
                    var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
        			fireSpinnerRefresh.setParams({"userAction" :"cancelAppointment", "stopSpinner" : true, "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
					fireSpinnerRefresh.fire();
                }
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})