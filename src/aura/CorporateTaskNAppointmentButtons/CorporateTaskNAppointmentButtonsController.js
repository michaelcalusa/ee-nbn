({
	createCorporateAffairsTaskRecord: function(component, event, helper) {
        var recID = component.get("v.recordId");
        if(recID.startsWith("003") || recID.startsWith("00Q")){
          var createRecordEvent = $A.get("e.force:createRecord");
          var recordTypeId = $A.get("$Label.c.Task_Corporate_Affairs_Task");      
          createRecordEvent.setParams({
             "entityApiName": "Task",
             "recordTypeId": recordTypeId,
              "defaultFieldValues": {
                    'WhoId' : component.get("v.recordId")        
                }
             
          });
          createRecordEvent.fire();            
        }
        else{
          var createRecordEvent = $A.get("e.force:createRecord");
          var recordTypeId = $A.get("$Label.c.Task_Corporate_Affairs_Task");      
          createRecordEvent.setParams({
             "entityApiName": "Task",
             "recordTypeId": recordTypeId,
              "defaultFieldValues": {
                    'WhatId' : component.get("v.recordId")        
                }
             
          });
          createRecordEvent.fire();
        }        
   },

   createAppointmentEventRecord: function(component, event, helper) {
        var recID = component.get("v.recordId");
        if(recID.startsWith("003") || recID.startsWith("00Q")){
          var createRecordEvent = $A.get("e.force:createRecord");
          var recordTypeId = $A.get("$Label.c.Event_Appointment_Event");     
          createRecordEvent.setParams({
             "entityApiName": "Event",
             "recordTypeId": recordTypeId,
              "defaultFieldValues": {
                    'WhoId' : component.get("v.recordId")        
                }
             
          });
          createRecordEvent.fire();
        }
       else{
          var createRecordEvent = $A.get("e.force:createRecord");
          var recordTypeId = $A.get("$Label.c.Event_Appointment_Event");     
          createRecordEvent.setParams({
             "entityApiName": "Event",
             "recordTypeId": recordTypeId,
              "defaultFieldValues": {
                    'WhatId' : component.get("v.recordId")        
                }
             
          });
          createRecordEvent.fire();           
       }
   }
})