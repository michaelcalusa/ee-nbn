({
    doInit : function(cmp, event, helper) {
        var dis = cmp.get('v.recordId');
        console.log(dis + "@@@@@");
        var action = cmp.get("c.getccOrder");
        action.setParams({ "caseRecordId" : cmp.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                cmp.set("v.order", response.getReturnValue());
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    handleClick: function (cmp, event, helper) {
       var sId = cmp.get('v.order.Id');
       console.log(sId);
       var siteUrl = '#/sObject/' + sId + '/view';
       var workspaceAPI = cmp.find("workspace");
       workspaceAPI.getEnclosingTabId().then(function(tabId) {
            console.log(tabId);
            workspaceAPI.openSubtab({
                parentTabId: tabId,
                url: siteUrl,
                focus: true
            });
       })
        .catch(function(error) {
            console.log(error);
        });
    }
})