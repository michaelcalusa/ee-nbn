({
    doInit: function(cmp, event, helper) { 
        helper.getStates(cmp);     
    },
    
    processCustomerBusinessDetails : function(component, event, helper){
        var div = document.getElementById("cbd-containerId");
        var subDiv = document.querySelectorAll('input, select');
        var myArray = [];
        myArray.push('cpd-bi-streetAddress');
        if(component.get('v.showBusinessDetails'))
        {
            myArray.push('cbd-abnNumber');
        }
        
        for(var i = 0; i < subDiv.length; i++) {
            var elem = subDiv[i];
            if(elem.id.indexOf('cbd-') === 0|| elem.id.indexOf('cpd-bi') === 0) {
                myArray.push(elem.id);    
            }
        }
        var errorCarier = [];
        for(var inputElementId=0; inputElementId<myArray.length; inputElementId++){
            var errorDivId = document.getElementById(myArray[inputElementId]+'-div');
            var errorSpanId = document.getElementById(myArray[inputElementId]+'-error'); 
            var fieldId = myArray[inputElementId];
            var fieldVal;
            if (fieldId == 'cpd-bi-streetAddress')
            {
                fieldVal = component.get("v.addressStreetName"); 
            }
            else if (fieldId == 'cbd-abnNumber' && component.get('v.showBusinessDetails'))
            {
                fieldVal =  component.get('v.abnNumber');
            }
            else 
            {
                fieldVal = document.getElementById(myArray[inputElementId]).value;
            }
            var err = helper.validatehelper(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);
            errorCarier.push(err);
        }
        component.set('v.hasError', false);
        for(var i = 0; i < errorCarier.length; i++){
            if(errorCarier[i] == true){
                component.set('v.hasError', true);
            } 
        }
        if(!component.get("v.hasError"))
        {
            var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
            wrapInstance.billingDtlWrapper.doYouHaveABN = component.get('v.showBusinessDetails');
            if(component.get('v.abnSearchStatus')=='SUCCESS')
            {
                wrapInstance.billingDtlWrapper.abn = component.get('v.abnNumber');
                wrapInstance.billingDtlWrapper.acn = component.get('v.acnNumber');
                wrapInstance.billingDtlWrapper.RegisteredEntityName = component.get('v.RegisteredEntityName');
                wrapInstance.billingDtlWrapper.entityTypeCode = component.get('v.entityTypeCode');
                wrapInstance.billingDtlWrapper.entityStatusCode = component.get('v.entityStatusCode');
                wrapInstance.billingDtlWrapper.entityDescription = component.get('v.entityDescription');
                wrapInstance.billingDtlWrapper.custClass = component.get('v.custClass');
                // logic to define customer type based on Billing entity ABN Number
               
                if(component.get('v.abnNumber')!=null || component.get('v.trusteeABN')!=null){
                     wrapInstance.billingDtlWrapper.custType = 'Company';
                }
                else{
                     wrapInstance.billingDtlWrapper.custType = 'Residential';
                }
                if(component.get('v.eType'))
                {                    
                    wrapInstance.billingDtlWrapper.TrusteeName = document.getElementById('cbd-TrusteeName').value;
                    if(component.get('v.trusteeABN')!=''){
                        wrapInstance.billingDtlWrapper.TrusteeABN = component.get('v.trusteeABN');
                    }
                    wrapInstance.billingDtlWrapper.businessName ='';
                }
                else {
                    wrapInstance.billingDtlWrapper.businessName = component.get('v.businessName');
                    wrapInstance.billingDtlWrapper.TrusteeName = '';
                    wrapInstance.billingDtlWrapper.TrusteeABN = '';
                }
            }
            else{
                 wrapInstance.billingDtlWrapper.custType = 'Residential';
                 wrapInstance.billingDtlWrapper.custClass = 'Class3/4';
            }
            wrapInstance.billingDtlWrapper.sameAsMyDetails = component.get('v.sameAsMyDetails');
            
            wrapInstance.billingDtlWrapper.billingsameAsMyDetails = component.get('v.billingsameAsMyDetails');
            wrapInstance.billingDtlWrapper.addressSuburb = document.getElementById('cpd-bi-townSuburb').value;
            wrapInstance.billingDtlWrapper.addressState = document.getElementById('cpd-bi-state').value;
            wrapInstance.billingDtlWrapper.addressPostalCode = document.getElementById('cpd-bi-postcode').value;
            component.set("v.selectedAddress",component.get('v.addressStreetName'));
            if(component.get('v.addressStreetName')!='')
            {
                wrapInstance.billingDtlWrapper.addressStreetName = component.get('v.addressStreetName');
            }
            else
            {
                wrapInstance.billingDtlWrapper.addressStreetName = component.get('v.selectedAddress');
            }
            wrapInstance.billingDtlWrapper.givenName = document.getElementById('cbd-Billing-firstName').value;
            wrapInstance.billingDtlWrapper.surName = document.getElementById('cbd-Billing-lastName').value;
            wrapInstance.billingDtlWrapper.phoneNumber = document.getElementById('cbd-Billing-phoneNumber').value;
            wrapInstance.billingDtlWrapper.billingMobileNumber = document.getElementById('cbd-Billing-mobileNumber').value;
            wrapInstance.billingDtlWrapper.emailAddress = document.getElementById('cbd-Billing-emailAddress').value;
            wrapInstance.billingDtlWrapper.billingconEmailAddress = document.getElementById('cbd-Billing-confirmemailAddress').value;
            wrapInstance.billingDtlWrapper.accountPayableFirstName = document.getElementById('cbd-accountPayable-firstName').value;
            wrapInstance.billingDtlWrapper.accountPayableLastName = document.getElementById('cbd-accountPayable-lastName').value;
            wrapInstance.billingDtlWrapper.accountPayablePhoneNumber = document.getElementById('cbd-accountPayable-phoneNumber').value;
            wrapInstance.billingDtlWrapper.accountPayableMobileNumber = document.getElementById('cbd-accountPayable-mobileNumber').value;
            wrapInstance.billingDtlWrapper.accountPayableEmailAddress = document.getElementById('cbd-accountPayable-emailAddress').value;
            wrapInstance.billingDtlWrapper.accountPayableconEmailAddress = document.getElementById('cbd-accountPayable-confirmemailAddress').value;
            component.set("v.addressState",wrapInstance.billingDtlWrapper.addressState);
            component.set("v.newDevApplicantWrapperInstance",wrapInstance);
            var action1 = component.get("c.saveNewApplicant");
            action1.setParams({
                "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance"))
            });
            action1.setCallback(this, function(response){
                if(response.getState() == 'SUCCESS'){
                    var wrapInstanceTemp = component.get("v.newDevApplicantWrapperInstance");
                    wrapInstanceTemp.newDevApplicantId = response.getReturnValue();
                    component.set("v.newDevApplicantWrapperInstance",wrapInstanceTemp);
                    var saveEvent = component.getEvent("saveRecEvent");
                    saveEvent.setParams({
                        "wrapperInstance" : component.get("v.newDevApplicantWrapperInstance"),
                        "componentSaved" : 'CustomerBusinessDetails'
                    });
                    saveEvent.fire();
                }
                else{
                    console.log(response.getState()+' message is '+response.getError()[0].message);
                }
            });
            $A.enqueueAction(action1); 
            
            // Identify the Serivce Delivery and Class
            var updatedWrapInstance = component.get("v.newDevApplicantWrapperInstance");
            var action2 = component.get("c.getServiceDeliveryType");
            var pc;
            if(updatedWrapInstance.devDtlWrapper.numberOfPremises!=undefined && updatedWrapInstance.devDtlWrapper.numberOfEssentialServices!=undefined){
                pc = +updatedWrapInstance.devDtlWrapper.numberOfPremises + +updatedWrapInstance.devDtlWrapper.numberOfEssentialServices;
            }
            else
            {
                pc = updatedWrapInstance.devDtlWrapper.numberOfPremises;
            }
            
            action2.setParams({ 
                "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance")),              
                "isEssentialService" : updatedWrapInstance.devDtlWrapper.essentialServices,
                "buildType" : updatedWrapInstance.devDtlWrapper.whatAreYouBuilding,
                "MTM" : updatedWrapInstance.devMapInfo.techType,
                "premiseCount" : pc,
                "isPitPipePrivate" : updatedWrapInstance.pitAndPipeDetailsWrapper.isPitAndPipePrivate,
                "entity": updatedWrapInstance.billingDtlWrapper.RegisteredEntityName,
                "entityCode" : updatedWrapInstance.billingDtlWrapper.entityTypeCode
            });
            action2.setCallback(this, function(response){
                if(response.getState() == 'SUCCESS'){
                    component.set("v.serviceDeliveryDtlsMap",response.getReturnValue());
                    
                     // get CostDetails and flag to display costdetails component 
                    var sendAppTo; 
                    var isTechnicalAssessment = false;
                    var mapSD = component.get("v.serviceDeliveryDtlsMap");                   
                    var action3 = component.get("c.costDetails");
                    var sdType = mapSD['ServiceDelivery'];
                    var dType =  mapSD['DwellingType'];
                    var cusClass =  mapSD['CustomerClass'];
                    if((sdType == 'SD-2' && cusClass == 'Class3/4') || (sdType == 'Technical Assessment')){
                        sendAppTo = 'Salesforce';
                        if(sdType == 'Technical Assessment'){
                            isTechnicalAssessment = true;
                            sdType = 'SD Unknown'
                        }                        
                    }
                    else if(sdType == 'SD-1' || cusClass == 'Class1/2'){
                        sendAppTo = 'CRMoD';
                    } 
                                      
                    
                    var wrapInstanceTemp = component.get("v.newDevApplicantWrapperInstance");
                      wrapInstanceTemp.serviceDeliveryDtlWrapper.serviceDeliveryType = sdType;
           			  wrapInstanceTemp.serviceDeliveryDtlWrapper.dwellingType = dType;
                      wrapInstanceTemp.serviceDeliveryDtlWrapper.upgradedCustomerClass = cusClass;
                      wrapInstanceTemp.serviceDeliveryDtlWrapper.sendApplicationTo = sendAppTo;
                      wrapInstanceTemp.serviceDeliveryDtlWrapper.TechnicalAssessment = isTechnicalAssessment;
                    component.set("v.newDevApplicantWrapperInstance",wrapInstanceTemp);
                    action3.setParams({ 
                        "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance")), 
                    });
                    action3.setCallback(this, function(response){
                        if(response.getState() == 'SUCCESS'){
                            component.set("v.costDtlMap",response.getReturnValue());
                            var mapCost = component.get("v.costDtlMap");                            
                    		var action4 = component.get("c.saveNewApplicant");
                    		var pCost = mapCost['PremisesCost'];
                    		var eCost =  mapCost['EssentailServicesCost'];
                    		var totalCost =  mapCost['TotalCost'];
                            var oneOffCost =  mapCost['OneOffCost'];                            
                            var displayCostDetails = mapCost['DisplayPaymentDetails']; 
                            var piName = mapCost['PriceItemName'];
                    
                    		var wrapInstanceTemp = component.get("v.newDevApplicantWrapperInstance");
                     		wrapInstanceTemp.costDtlWrapper.premisesCost = pCost;
           			  		wrapInstanceTemp.costDtlWrapper.nonPremisesCost = eCost;
                      		wrapInstanceTemp.costDtlWrapper.totalCost = totalCost;
                            wrapInstanceTemp.costDtlWrapper.oneOffCost = oneOffCost;
                            wrapInstanceTemp.costDtlWrapper.priceItemName = piName;
                    		component.set("v.newDevApplicantWrapperInstance",wrapInstanceTemp);
                    		action4.setParams({ 
                        		"wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance")), 
                    		});
                    		action4.setCallback(this, function(response){
                        	if(response.getState() == 'SUCCESS'){
                       
                                // fire costDetailsEvent
								var costDetailsEvent = component.getEvent("costDtlsEvent");
                                costDetailsEvent.setParams({
                                    "wrapperInstance" : component.get("v.newDevApplicantWrapperInstance"), 
                                    "displayCostDtls": displayCostDetails
                                });
                                costDetailsEvent.fire();   
                                // fire saveEvent
                                var saveEvent = component.getEvent("saveRecEvent");
                                saveEvent.setParams({
                                    "wrapperInstance" : component.get("v.newDevApplicantWrapperInstance"),
                                    "componentSaved" : 'costDetails'
                                });
                                saveEvent.fire();
                        	}
                        	else{
                            	console.log(response.getState()+' message1 is '+response.getError()[0].message+' '+response.getReturnValue());
                        	}
                   		});
                     	$A.enqueueAction(action4);
                        }
                        else{
                            console.log(response.getState()+' message 2is '+response.getError()[0].message+' '+response.getReturnValue());
                        }
                    });
                     $A.enqueueAction(action3);
                }
                else{
                    console.log(response.getState()+' message 22is '+response.getError()[0].message+' '+response.getReturnValue());
                }
            });
 
            $A.enqueueAction(action2); 
        }
        else{
            console.log('caught errors in next');
            //update parent component to disable Submit button.
            //cater the cases where the user has come back to billing section from submission page and changes anything that cause error
            var disableSubmit = component.getEvent("disableSubmitButton");
            disableSubmit.setParams({
                "disableSubmission" : 'true'
            });
            disableSubmit.fire();
            console.log('fired');  
        }     
    },
    
    checkBillingDetails : function (component, event, helper){        
        
        var y= document.getElementById('cbd-billingsameAsMyDetails').checked;
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(y)
        {
            wrapInstance.billingDtlWrapper.givenName = wrapInstance.yourDtlWrapper.givenName;
            wrapInstance.billingDtlWrapper.surName = wrapInstance.yourDtlWrapper.surName;
            wrapInstance.billingDtlWrapper.emailAddress = wrapInstance.yourDtlWrapper.emailAddress;
            wrapInstance.billingDtlWrapper.billingconEmailAddress = wrapInstance.yourDtlWrapper.emailAddress;
            wrapInstance.billingDtlWrapper.phoneNumber = wrapInstance.yourDtlWrapper.phoneNumber;
            component.set("v.billingsameAsMyDetails",true);
            component.set("v.readOnlyAttribute",true);
        }
        else if (!y)
        {
            wrapInstance.billingDtlWrapper.givenName = '';
            wrapInstance.billingDtlWrapper.surName = '';
            wrapInstance.billingDtlWrapper.emailAddress = '';
            wrapInstance.billingDtlWrapper.billingconEmailAddress = '';
            wrapInstance.billingDtlWrapper.phoneNumber = '';
            component.set("v.readOnlyAttribute",false);
            component.set("v.billingsameAsMyDetails",false);
        }
    },
    
    checkAccountPayable : function (component, event, helper){        
        var x= document.getElementById('cbd-sameAsMyDetails').checked;  
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(x)
        {
            wrapInstance.billingDtlWrapper.accountPayableFirstName = wrapInstance.yourDtlWrapper.givenName;
            wrapInstance.billingDtlWrapper.accountPayableLastName = wrapInstance.yourDtlWrapper.surName;
            wrapInstance.billingDtlWrapper.accountPayableEmailAddress = wrapInstance.yourDtlWrapper.emailAddress;
            wrapInstance.billingDtlWrapper.accountPayableconEmailAddress = wrapInstance.yourDtlWrapper.emailAddress;
            wrapInstance.billingDtlWrapper.accountPayablePhoneNumber = wrapInstance.yourDtlWrapper.phoneNumber;
            component.set("v.accreadOnlyAttribute",true);
            component.set("v.sameAsMyDetails",true);
        }
        else if (!x)
        {
            wrapInstance.billingDtlWrapper.accountPayableFirstName = '';
            wrapInstance.billingDtlWrapper.accountPayableLastName = '';
            wrapInstance.billingDtlWrapper.accountPayableEmailAddress = '';
            wrapInstance.billingDtlWrapper.accountPayableconEmailAddress = '';
            wrapInstance.billingDtlWrapper.accountPayablePhoneNumber = '';
            component.set("v.accreadOnlyAttribute",false);
            component.set("v.sameAsMyDetails",false);
        }
    },
    
    validateFields : function(cmp, event, helper){
        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;        
        var fieldId = event.currentTarget.id;       
        var fieldVal = document.getElementById(fieldId).value;
        
        helper.validatehelper(cmp, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);
        
    },
    
    
    
    populateABNInfo : function(cmp, event) {   
        var abnSearchStatus = event.getParam('Status');
        var mapABNResult = event.getParam('mapABNResults');
        var abnType = event.getParam('abnType');
        var abnNumber =  mapABNResult["ABNNumber"].replace(/ +/g, "");
        var acnNumber = mapABNResult["ACNNumber"]; 
        var businessName = mapABNResult["BusinessName"];
        var RegisteredEntityName = mapABNResult["RegisteredEntityName"];
        var entityTypeCode = mapABNResult["entityTypeCode"];
        var entityStatusCode = mapABNResult["entityStatusCode"];
        var entityDescription = mapABNResult["entityDescription"];
        var eType = mapABNResult["eType"];
        var custClass = mapABNResult["custClass"];
        if(typeof eType == 'string')
        {
            if(eType == 'false')
            {
                eType = false;
            }
            else
            {
                eType = true;
            }
        }

        if(abnType == 'ABN')
        {
            cmp.set("v.abnNumber", abnNumber);
            cmp.set("v.acnNumber", acnNumber);
            cmp.set("v.businessName", businessName);
            cmp.set("v.entityTypeCode",entityTypeCode);
            cmp.set("v.RegisteredEntityName",RegisteredEntityName);
            cmp.set("v.entityDescription",entityDescription);
            cmp.set("v.eType",eType);
            cmp.set("v.abnSearchStatus", abnSearchStatus);
            cmp.set("v.custClass",custClass);
        }
        else if (abnType == 'Trustee ABN')
        {
            cmp.set("v.trusteeABN", abnNumber);
            cmp.set("v.trusteeName", businessName);
        }
        else 
        {
            cmp.set("v.RegisteredEntityName",'');
            cmp.set("v.trusteeName", '');
        }
        
        
    },    
    
    handleCollapsableSection: function(component, event, helper){  
        if(!component.get("v.disableSection")){
            component.set("v.displaySection",!(component.get("v.displaySection")));
            if(component.get("v.displaySection")){
                var disEvent = component.getEvent("displaySectionChange");
                disEvent.setParams({
                    "firedComponentName" : 'CustomerBusinessDetails'
                });
                disEvent.fire();
                var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
                component.set("v.addressState", wrapInstance.billingDtlWrapper.addressState);
            } 
        }
        
    },
    checkBusinessDetailsRequired : function (cmp, event, helper){
 
        var isBusinessDetailsRequiredFlag = event.currentTarget.dataset.value;
        var togglebuttonId = event.currentTarget.id;
        var toggleYesButton = document.getElementById('cpd-bi-yes-toggleButtonId');
        var toggleNoButton = document.getElementById('cpd-bi-no-toggleButtonId');
        if(isBusinessDetailsRequiredFlag == 'Yes')
        {
            $A.util.addClass(toggleYesButton,'btn-primary');
            $A.util.removeClass(toggleNoButton,'btn-primary');
            cmp.set('v.showBusinessDetails',true);
            
            
        }
        else if(isBusinessDetailsRequiredFlag == 'No')
        {
            $A.util.addClass(toggleNoButton,'btn-primary');
            $A.util.removeClass(toggleYesButton,'btn-primary');
            cmp.set('v.showBusinessDetails',false);
            var wrapInstance = cmp.get("v.newDevApplicantWrapperInstance");
            cmp.set("v.abnNumber", '');
            cmp.set("v.acnNumber", '');
            cmp.set("v.businessName", '');
            cmp.set("v.trusteeName", '');
            cmp.set("v.trusteeABN", '');
            cmp.set("v.entityTypeCode",'');
            cmp.set("v.RegisteredEntityName",'');
            cmp.set("v.entityDescription",'');
            cmp.set("v.eType",'');
            cmp.set("v.custClass",'');
            cmp.set("v.abnSearchStatus", '');
            wrapInstance.billingDtlWrapper.abn = '';
            wrapInstance.billingDtlWrapper.acn = '';
            wrapInstance.billingDtlWrapper.RegisteredEntityName = '';
            wrapInstance.billingDtlWrapper.entityTypeCode = '';
            wrapInstance.billingDtlWrapper.entityStatusCode = '';
            wrapInstance.billingDtlWrapper.entityDescription = '';
            wrapInstance.billingDtlWrapper.custClass = '';
            wrapInstance.billingDtlWrapper.TrusteeName = '';
            wrapInstance.billingDtlWrapper.TrusteeABN = '';
            wrapInstance.billingDtlWrapper.businessName = '';            
        }
    },
    
    populateAddressInfo : function(cmp, event) {  
        if(event.getParam('cmpName')=='BillingInformation')
        {
            var mapAddressResult = event.getParam('mapAddressResults');
            console.log(JSON.stringify(mapAddressResult));
            var displaySelectedAdd = mapAddressResult["displaySelectedAddress"];
            var streetName = mapAddressResult["StreetName"]; 
            var suburb = mapAddressResult["Suburb"];
            var state = mapAddressResult["State"]; 
            var postalCode = mapAddressResult["PostalCode"];
            
            cmp.set("v.selectedAddress",displaySelectedAdd);
            cmp.set("v.addressStreetName", streetName);
            cmp.set("v.addressSuburb", suburb);
            cmp.set("v.addressState", state);
            cmp.set("v.addressPostalCode", postalCode);
            
            var wrapInstance = cmp.get("v.newDevApplicantWrapperInstance");
            wrapInstance.billingDtlWrapper.addressSuburb = suburb;
            wrapInstance.billingDtlWrapper.addressState = state;
            wrapInstance.billingDtlWrapper.addressPostalCode = postalCode;
            cmp.set("v.newDevApplicantWrapperInstance",wrapInstance);
            
            document.getElementById('cpd-bi-townSuburb').readOnly = true;
           	document.getElementById('cpd-bi-postcode').readOnly = true;
           	document.getElementById('cpd-bi-state').disabled = true;
           	cmp.set("v.disableAdd", true);
        }
        
    },
    enableAddress : function(component, event, helper){
        console.log(' enable address ');
        component.set("v.disableAdd", false);
        
    },
    
    populateState: function(component, event, helper){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        component.set("v.addressState",event.target.value);
    },
    handlenoResultEvent : function(component, event, helper){
        var streetAddress = event.getParam('streetAddress');
        component.set("v.addressStreetName", streetAddress);       
    }
})