({
     toggleValidationIconId : function(cmp, event, validationIconId, hasError){ 
        if(!hasError){
             $A.util.addClass(validationIconId,'fc-tick-circle');
             $A.util.removeClass(validationIconId,'fc-exclamation-circle'); 
            
        } 
        else
        {
			 $A.util.addClass(validationIconId,'fc-exclamation-circle'); 
             $A.util.removeClass(validationIconId,'fc-tick-circle');
        }
    }, 
    
    getStates : function(cmp) {
        var action = cmp.get("c.getStates");   
        action.setCallback(this, function(response){            
            var state = response.getState();   
            
            if (state === "SUCCESS") {                 
                var options = response.getReturnValue();
                var opt = [];
                for(var o=0; o<=options.length-1; o++)
                {                    
                    opt.push(options[o]);
                }             
                cmp.set("v.stateListValues",opt);
            }            
            else if (state === "ERROR") {
                var appEvent = $A.get("e.c:HandleCallbackError");
                appEvent.setParams({
                    "errors" : response.getError(),
                    "errorComponentName" : "someUniqueName"
                });
                appEvent.fire();  
            }
            
        });
        $A.enqueueAction(action);
    } ,  
    
	 fireProcessCustomerBusinessDetails : function(cmp){
        var myEvent = $A.get("e.c:processCustomerBusinessDetails");       
        myEvent.setParams({
            "mapCustomerBusinessDetails":cmp.get('v.mapCustomerBusinessDetails')       
        });      
        myEvent.fire();
    },
    
     validatehelper : function(cmp, event, helper, errorDivId, errorSpanId, fieldId, fieldVal){
        var errorflag = false;
       	if(fieldVal == ''){
            if ((fieldId=='cbd-accountPayable-mobileNumber' || fieldId=='cbd-accountPayable-phoneNumber' || fieldId=='cbd-Billing-mobileNumber' || fieldId=='cbd-Billing-phoneNumber')){
                if(fieldId=='cbd-accountPayable-mobileNumber' && document.getElementById('cbd-accountPayable-phoneNumber').value == ''){
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;
               }
                else if(fieldId=='cbd-accountPayable-phoneNumber' && document.getElementById('cbd-accountPayable-mobileNumber').value == ''){
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;
               }
                else if(fieldId=='cbd-Billing-phoneNumber' && document.getElementById('cbd-Billing-mobileNumber').value == ''){
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;
               }
                else if(fieldId=='cbd-Billing-mobileNumber' && document.getElementById('cbd-Billing-phoneNumber').value == ''){
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;
               }
                else
            	{
                	$A.util.removeClass(errorSpanId,'help-block-error');
                	$A.util.removeClass(errorDivId,'has-error');
                	$A.util.addClass(errorDivId,'has-success');
            	}
            }
            else
            {
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorflag = true;
            }
            
        }
        else if((fieldId=='cbd-accountPayable-confirmemailAddress' || fieldId=='cbd-accountPayable-emailAddress' || fieldId=='cbd-Billing-confirmemailAddress' || fieldId=='cbd-Billing-emailAddress')){

                if(!fieldVal.match( /^[^\s@]+@[^\s@]+\.[^\s@]+$/)){
                	document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid email address';
                	$A.util.addClass(errorSpanId,'help-block-error');
                	$A.util.addClass(errorDivId,'has-error');
                	$A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;
            	}
                else if(fieldId=='cbd-Billing-confirmemailAddress' && document.getElementById('cbd-Billing-emailAddress').value!= fieldVal){
                    	document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a matching email address';
                    	$A.util.addClass(errorSpanId,'help-block-error');
                    	$A.util.addClass(errorDivId,'has-error');
                    	$A.util.removeClass(errorDivId,'has-success');
                    	errorflag = true;
                }
                else  if(fieldId=='cbd-accountPayable-confirmemailAddress' && document.getElementById('cbd-accountPayable-emailAddress').value!= fieldVal){
                    	document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a matching email address';
                    	$A.util.addClass(errorSpanId,'help-block-error');
                    	$A.util.addClass(errorDivId,'has-error');
                    	$A.util.removeClass(errorDivId,'has-success');
                    	errorflag = true;
                }
                else{
                	$A.util.removeClass(errorSpanId,'help-block-error');
                	$A.util.removeClass(errorDivId,'has-error');
                	$A.util.addClass(errorDivId,'has-success');
            	}
            }
        	else if ((fieldId=='cbd-accountPayable-mobileNumber' || fieldId=='cbd-accountPayable-phoneNumber' || fieldId=='cbd-Billing-mobileNumber' || fieldId=='cbd-Billing-phoneNumber'))
            {
                if ((fieldVal.substring(0,2) != '02' && fieldVal.substring(0,2) != '03' && fieldVal.substring(0,2) != '04' && fieldVal.substring(0,2) != '07' && fieldVal.substring(0,2) != '08') || !fieldVal.match(/^\d{10}$/)){
            	
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;
               }
        		else{
                	$A.util.removeClass(errorSpanId,'help-block-error');
                	$A.util.removeClass(errorDivId,'has-error');
                	$A.util.addClass(errorDivId,'has-success');
            	}
            }
    		      
        	else if((fieldId=='cpd-bi-postcode')){
            if((!fieldVal.match(/^[0-9]+$/) || (fieldVal.length != 4 && fieldVal.length != 3))){
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid post code';
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorflag = true;
            }
            else{
                var postcodeValid = false;
                if((fieldVal >= 1000 && fieldVal <= 1999) || (fieldVal >= 2000 && fieldVal <= 2599) || (fieldVal >= 2620 && fieldVal <= 2899) || (fieldVal >= 2921 && fieldVal <= 2999)){
                   document.getElementById('cpd-bi-state').value = 'NSW';
                   postcodeValid = true;
                }
                else if((fieldVal >= 200 && fieldVal <= 299) || (fieldVal >= 2600 && fieldVal <= 2619) || (fieldVal >= 2900 && fieldVal <= 2920)){
                    document.getElementById('cpd-bi-state').value = 'ACT';
                    postcodeValid = true;
                }
                else if((fieldVal >= 3000 && fieldVal <= 3999) || (fieldVal >= 8000 && fieldVal <= 8999)){
                    document.getElementById('cpd-bi-state').value = 'VIC';
                    postcodeValid = true;
                }
                else if((fieldVal >= 4000 && fieldVal <= 4999) || (fieldVal >= 9000 && fieldVal <= 9999 )){
                    document.getElementById('cpd-bi-state').value = 'QLD';
                    postcodeValid = true;
                }
                else if((fieldVal >= 5000 && fieldVal <= 5799) || (fieldVal >= 5800 && fieldVal <= 5999 )){
                    document.getElementById('cpd-bi-state').value = 'SA';
                    postcodeValid = true;
                }
                else if((fieldVal >= 6000 && fieldVal <= 6797) || (fieldVal >= 6800 && fieldVal <= 6999 )){
                    document.getElementById('cpd-bi-state').value = 'WA';
                    postcodeValid = true;
                }
                else if((fieldVal >= 7000 && fieldVal <= 7799) || (fieldVal >= 7800 && fieldVal <= 7999 )){
                    document.getElementById('cpd-bi-state').value = 'TAS';
                    postcodeValid = true;
                }
                else if((fieldVal >= 800 && fieldVal <= 899) || (fieldVal >= 900 && fieldVal <= 999  )){
                    document.getElementById('cpd-bi-state').value = 'NT';
                    postcodeValid = true;
                }
                if(postcodeValid){
                    $A.util.removeClass(errorSpanId,'help-block-error');               
                    $A.util.removeClass(errorDivId,'has-error');              
                    $A.util.addClass(errorDivId,'has-success');
                }
                else{
                    /*document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid post code';
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;*/
                }
            }
        }
        else if(fieldId=='cpd-bi-state'){
            var validState = false;
            var postCode = document.getElementById('cpd-bi-postcode').value;
            if(fieldVal == 'NSW' && ((postCode >= 1000 && postCode <= 1999) || (postCode >= 2000 && postCode <= 2599) || (postCode >= 2620 && postCode <= 2899) || (postCode >= 2921 && postCode <= 2999))){
                validState = true;
            }
            else if(fieldVal == 'ACT' && ((postCode >= 200 && postCode <= 299) || (postCode >= 2600 && postCode <= 2619) || (postCode >= 2900 && postCode <= 2920))){
                validState = true;
            }
            else if(fieldVal == 'VIC' && ((postCode >= 3000 && postCode <= 3999) || (postCode >= 8000 && postCode <= 8999))){
                validState = true;
            }
            else if(fieldVal == 'QLD' && ((postCode >= 4000 && postCode <= 4999) || (postCode >= 9000 && postCode <= 9999 ))){
                validState = true;
            }
            else if(fieldVal == 'SA' && ((postCode >= 5000 && postCode <= 5799) || (postCode >= 5800 && postCode <= 5999 ))){
                validState = true;
            }
            else if(fieldVal == 'WA' && ((postCode >= 6000 && postCode <= 6797) || (postCode >= 6800 && postCode <= 6999 ))){
                validState = true;
            }
            else if(fieldVal == 'TAS' && ((postCode >= 7000 && postCode <= 7799) || (postCode >= 7800 && postCode <= 7999 ))){
                validState = true;
            }
            else if(fieldVal == 'NT' && ((postCode >= 800 && postCode <= 899) || (postCode >= 900 && postCode <= 999  ))){
                validState = true;
            }
            if(validState){
                    $A.util.removeClass(errorSpanId,'help-block-error');               
                    $A.util.removeClass(errorDivId,'has-error');              
                    $A.util.addClass(errorDivId,'has-success');
                }
                else{
                    /*document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Selected state and post code does not match.';
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;*/
                }
        }
        else if((fieldId=='abnSearch-abn' && cmp.get('v.abnNumber') == undefined))
        {
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
            
            if(fieldId=='abnSearch-abn'){
                document.getElementById('abnSearch-abn-error').innerText="Please fill in ABN Number";
            }
            errorflag = true;
        }
        else{
            $A.util.removeClass(errorSpanId,'help-block-error');
            $A.util.removeClass(errorDivId,'has-error');
            $A.util.addClass(errorDivId,'has-success');
        }  
        return errorflag;
            
    },
    
    expandCollapseCustomerPersonalSection : function(cmp, event,bodyPanelId,iconId) {
        
        if(!$A.util.hasClass(bodyPanelId,'panel-body hide'))
        {              
            $A.util.addClass(bodyPanelId,'hide');
            if($A.util.hasClass(iconId,'fc-chevron-up'))
            {
                $A.util.removeClass(iconId,'fc-chevron-up');   
                $A.util.addClass(iconId,'fc-chevron-down');                 
            }
        }
        else
        {
            $A.util.removeClass(bodyPanelId,'hide'); 
            $A.util.removeClass(iconId,'fc-chevron-down');   
            $A.util.addClass(iconId,'fc-chevron-up');   
        }
         
                    
            bodyPanelId.scrollIntoView();
        
    }
})