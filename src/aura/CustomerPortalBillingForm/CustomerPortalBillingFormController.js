({
   
     handleSaveCase: function(component, event, helper) {
        var subjectHasError = helper.validSubjectHelper(component, event, helper);
        var rtpeHasError = helper.recordTypeChangeHelper(component, event, helper);
        var subCatHasError = helper.SubcategoryChangeHelper(component, event, helper);
        console.log(' subject '+subjectHasError+' rtype '+rtpeHasError+' sub cat '+subCatHasError);
        if(!subjectHasError && !rtpeHasError && !subCatHasError){
            var mapfieldsflag = component.get("v.SectionOneFieldsFlag");
            console.log('mapfieldsflag '+mapfieldsflag);
            var catFlag = mapfieldsflag.CaseType;
            var subCatFlag = mapfieldsflag.SubType;
            var subjectFlag = mapfieldsflag.Subject;
            console.log('catFlag '+Category+' subCatFlag '+SubCategory+' subjectFlag '+subjectFlag);   
                       
            
            var Category = document.getElementById("caserecordtypes").value;
            var SubCategory =document.getElementById("casetypes").value;
            component.set("v.AttachmentFlag",true);
            component.set("v.selectedRecType",Category);
            component.set("v.selectedSubCat",SubCategory);
            console.log('rtype '+Category+' subcat '+SubCategory);
            component.set("v.showAttachmentSection",true);
            if (catFlag=='1' && subCatFlag=='1' && subjectFlag=='1' ) {
                if(Category == 'Billing Enquiry'){
                    
                    if(!component.get("v.uploadAttachmentForBillingEnquiry")){
                        component.set("v.showAttachmentSection",true);
                        component.set("v.SubmitcancelFlag",true);
                        
                    }
                    else{
                        component.set("v.showAttachmentSection",false);  
                         component.set("v.SubmitcancelFlag",true);
                    }
                }        
                else if(Category == 'Billing Claim' || Category == 'Billing Dispute'){
                    component.set("v.showAttachmentSection",true);
                    component.set("v.SubmitcancelFlag",true);
                    
                }
            } else {
                component.set("v.AttachmentFlag",false);
                component.set("v.showAttachmentSection",false);
                component.set("v.SubmitcancelFlag",false);
            }
            
            //If the cat isnot billing enquiry, we invisbile the checkbox, but make the show attachment flag as always true
            
            var saveCaseAction = component.get("c.saveCase");        
            saveCaseAction.setParams({
                "cas": component.get("v.newCase"),
                "recordType":document.getElementById("caserecordtypes").value,
                "casetype":document.getElementById("casetypes").value
            });
            //alert(component.find("caseTypeOptionId").get("v.value"));
            // Configure the response handler for the action
            saveCaseAction.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    // $A.get("e.force:refreshView").fire();
                    component.set("v.newCase", response.getReturnValue());
                    console.log('case id '+JSON.stringify(response.getReturnValue()));
                    component.set("v.message", "Your Case Number is ");
                    component.set("v.disableRec",true);
                    component.set("v.disableSubCat",true);
                    component.set("v.disableSub",true);
                    //var cmpTarget = component.find('changeIt');
                    //$A.util.addClass(cmpTarget,'slds-theme--success');
                    
                }
                else if (state === "ERROR") {
                    console.log('Problem saving contact, response state: ' + response.getError()[0].message);
                }
                   /* else {
                        console.log('Unknown problem, response state: ' + state);
                    }*/
            });
            
            
            
            // Send the request to create the new contact
            $A.enqueueAction(saveCaseAction);
            //component.set("v.viewForm",false);
            $("#classForm").collapse("hide");
            //$A.util.addClass(classForm, 'hide');
        }
        
    },
   
    
    handleSubmitCase : function(component, event, helper) {
        var RecordtypeName = component.get("v.rtypeSelected");
        var Typeids = component.get("v.subCatValue");
        var attachment = component.get("v.ParentAttachmentLength");
        
        console.log(' RecordtypeName '+RecordtypeName+' Typeids '+Typeids+ 'attachment' +attachment);
 
        console.log(' show attach '+component.get("v.showAttachmentSection"));
        
        //Being update, before we do the validation check, we need to check whether
        //or not the case is stil exists
        helper.IfCaseDeleteCheckHelper(component,event,helper);
        //console.log('the delete flag is '+component.get("v.IfCaseDeleteFlag"));
       
        //End update
        /*
        var validationerror = 'false';
        if(component.get("v.showAttachmentSection")){
            console.log('validationerror 0 '+validationerror);
            console.log('validationerror 0 '+typeof validationerror);
            validationerror= helper.UIInputTagValidate(component,event,'','','attachmentSection');
            console.log('validation error in 1 '+validationerror);
            console.log('validation error in 1 '+typeof validationerror);
        }
        else{
            validationerror='true';
        }
        console.log('validation error final '+validationerror);
        console.log('validation error final '+typeof validationerror);
        if(validationerror=='true'){
            //component.set("v.SubmitFlag",false);
            //added to send email
            var sendEmailAction = component.get("c.sendBillingAckEmail");     
            var caseId = component.get("v.newCase").Id;
            var conId = component.get("v.newCase").ContactId;
            sendEmailAction.setParams({
                "caseID": caseId,
                "contactID": conId,
                
            });
            console.log ('CaseID' +JSON.stringify(component.get("v.newCase")));
            console.log ('ContactID' +component.get("v.newCase").ContactId);
            // Configure the response handler for the action
            sendEmailAction.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    // $A.get("e.force:refreshView").fire();
                    console.log('SUCCESS '+response.getReturnValue());
                    component.set("v.SubmitFlag",false); 
                    component.set("v.showAttachmentSection",true); 
                }
                else if (state === "ERROR") {
                    console.log('Problem saving contact, response state: ' + state+' err '+response.getError()[0].message);
                }
                    else {
                        console.log('Unknown problem, response state: ' + state);
                    }
            });
            
            // Send the request to create the new contact
            $A.enqueueAction(sendEmailAction);
            
            //Added by Wayne for RSP billing improvement
            helper.UpdateExistingCaseFormStatus(component,event);
        }
        */
    },
    
     doInit : function(component,event,helper){
        var contactid ;
        var getUserInfoAction = component.get("c.getUserDetails");
        getUserInfoAction.setCallback(this,function(response){
	        var state = response.getState();            
	        if(state === "SUCCESS"){
	            component.set("v.loginUser", response.getReturnValue());
               
                var contactid = response.getReturnValue().ContactId;
                console.log('the Contact ID is '+contactid);
                component.set("v.UserId",contactid);
                //alert(contactid);
	        }
            
        });
        $A.enqueueAction(getUserInfoAction);
        
        var getCaseTypeOptions = component.get("c.getRecordTypeTypesMap");
        
        getCaseTypeOptions.setCallback(this,function(response){
            console.log('Call Back Respone ==>'+ response);
        	var state = response.getState(); 
            if(state === "SUCCESS"){
                component.set("v.caseTypeMaps", response.getReturnValue());                 
                var caseTypeOptions = response.getReturnValue(); 
                console.log('caseTypeOptions: ' + caseTypeOptions);   
                var mapIter = Object.keys(caseTypeOptions);                 
                var RecordTypeOptions = [];
                for (var i = 0; i < mapIter.length; i++){
                    var RecordTypeOption = {label:mapIter[i], value:mapIter[i]};
                    RecordTypeOptions.push(RecordTypeOption);
                    
                }
                console.log(' RecordTypeOptions Call Back Respone ==>'+ RecordTypeOptions);
                component.set("v.caserecordtypes", RecordTypeOptions);
            }
            
        });
        $A.enqueueAction(getCaseTypeOptions);
         

         
         //var userDetail = component.get("v.getUserDetails");
         //console.log('userdetails is '+userDetail);
        //var contactID = userDetail.contactID;
        // console.log('thecontact Id is '+contactID);
         
         /*
         alert('contactid is '+contactid);
         var GaneshAction = component.get("c.checkContactRole");
         GaneshAction.setParams({ ContactID : contactid });
         GaneshAction.setCallback(this,function(response){
             var state = response.getState();            
             if(state === "SUCCESS"){
                 
                 component.set("v.showFormFlag", response.getReturnValue());               
             }
             
         });
         $A.enqueueAction(GaneshAction);
         */
    } ,
    
    itemsChange : function(component,event,helper){
        //alert('user ID changed');
        var buffer = component.get("v.UserId");
        //alert(buffer);
        //
        var RoleAction = component.get("c.checkContactRole");
        RoleAction.setParams({ ContactID : buffer });
        RoleAction.setCallback(this,function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                //alert('the return value is '+response.getReturnValue());
                component.set("v.showFormflag", response.getReturnValue());               
            }
            
        });
        $A.enqueueAction(RoleAction);
    },
    
    SubcategoryChange : function(component, event, helper){
        var retVar = helper.SubcategoryChangeHelper(component, event, helper);
        console.log(' subcat Chnage '+retVar);
    },
    
    recordTypeChange : function(component, event, helper){
        var retVar = helper.recordTypeChangeHelper(component, event, helper);
        console.log(' rtype Chnage '+retVar);
    },
    
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
     },
    
     validTotalAmount : function(component,event,helper){
      	helper.validateTotalAmountHelper(component,event);  
    },
    
    validNumberTransaction : function(component,event,helper){
      	helper.validNumberTransactionHelper(component,event);  
    },
    
    /*validNumberTransaction : function(component,event,helper){
      	helper.validNumberTransactionHelper(component,event);  
    },*/
    
    checkBoxChange : function(component, event, helper){
        component.set("v.showAttachmentSection",!component.get("v.uploadAttachmentForBillingEnquiry"));
    },
    
    OkModel : function(component, event, helper){
        	
        	var action = component.get("c.deleteCaseRec");
            action.setParams({
                "cs": component.get("v.newCase")
            });		
            action.setCallback(this, function(response) {
                var state = response.getState();
                var typeOptions = [];
                if(state === "SUCCESS") {
                    $A.get('e.force:refreshView').fire();
				}
                else{
                }
		    });
            // Send the request to create the new contact
            $A.enqueueAction(action); 
        	

        
               
    },
    
    closeModel : function(component, event, helper){
        
        component.set("v.isOpen",false); 	
               
    },
    
    deletecase : function(component, event, helper){
        
        
        //var confirmMessage = confirm("Are you sure?");
        component.set("v.isOpen",true);
        
        /*if(confirmMessage){
        	var action = component.get("c.deleteCaseRec");
            action.setParams({
                "cs": component.get("v.newCase")
            });		
            action.setCallback(this, function(response) {
                var state = response.getState();
                var typeOptions = [];
                if(state === "SUCCESS") {
                    $A.get('e.force:refreshView').fire();
				}
                else{
                }
		    });
            // Send the request to create the new contact
            $A.enqueueAction(action);    
        }
        else{
            
        }*/
        
    },
    
    closeFocusedTab : function(component, event, helper){
        
       window.close();
        
    },
    
       
   
    validSubject : function(component,event,helper){
        var retVar = helper.validSubjectHelper(component,event);
        console.log('subject '+retVar);
    },
    handleComponentEvent : function(component, event, helper){
        console.log('event captured');
        component.set("v.attachmentError",false);
    },
    
    collapsSecHandle : function(component, event, helper){
        console.log(' handle sec '+document.getElementById("casetypes"));
        //document.getElementById("casetypes").value = component.get("v.selectedSubCat");
        
    }
       
    
})