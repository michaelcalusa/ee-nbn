({
    UIInputTagValidate : function(component,event,message,Targetid,AuraId){
        console.log('AuraId>>>>>>>>>>'+AuraId);  
        if(AuraId == 'attachmentSection'){
            console.log(' target attachment '+component.get("v.ParentAttachmentLength"));
            if(component.get("v.ParentAttachmentLength")){
                console.log(' if ');
                component.set("v.attachmentError",false);
                component.set("v.TestFlag",true);
                //component.set("v.SubmitFlag",true);   
                return 'true';
            }
            else{
                console.log(' else1 ');
                component.set("v.attachmentError",true);
                component.set("v.TestFlag",false);
                //component.set("v.SubmitFlag",true);
				return 'false';
            }
        }
        else{
        	var field = component.find(AuraId);
            var fieldvalue = field.get('v.value');
            var NumberOnly;
            console.log('field id '+field);
            console.log('field Value '+fieldvalue);
            if(AuraId == 'NumberTransactions'){
                if(/^\d+$/.test(fieldvalue)){
                    NumberOnly = true;
                }else{
                    NumberOnly = false;
                }  
            }  
            
            if(AuraId == 'TotalAmount'){
                if(/^\d+(\.\d{1,2})?$/.test(fieldvalue)){
                    NumberOnly = true;
                }else{
                    NumberOnly = false;
                }
            }
            
            if(fieldvalue){
                if(AuraId == 'NumberTransactions'){
                    //Amount should be 7 digit or less 
                    if(fieldvalue.length <= 7 && NumberOnly){
                        console.log('good');
                        this.setFieldSuccess(Targetid,component);
                        field.set("v.errors", null);                
                    }else if(fieldvalue.length != 7 || !(NumberOnly)){
                        console.log('not valid');
                        this.setFieldError(Targetid,component);
                         field.set("v.errors", [{message:message}]); 
                    }
                   
                
                }   
                
                if(AuraId == 'TotalAmount'){
                //Amount should be 7 digit or less 
                if(fieldvalue.length <= 11 && NumberOnly){
                    console.log('good'+'fieldvalue'+fieldvalue+'NumberOnly'+NumberOnly);
                    
                    this.setFieldSuccess(Targetid,component);
                    field.set("v.errors", null);
                    if(AuraId == 'TotalAmount'){
                        component.set ("v.Amountflag",true);
                    }
                   
                    console.log('aAmountflag'+v.Amountflag);
                }else if(fieldvalue.length != 11 || !(NumberOnly)){
                    console.log('not valid');
                    this.setFieldError(Targetid,component);
                     field.set("v.errors", [{message:message}]); 
                }
               }
            }else{
                console.log('bad'+'fieldvalue'+fieldvalue+'NumberOnly'+NumberOnly);
             	component.set ("v.Amountflag",false);
               /*console.log('bad');
                this.setFieldError(Targetid,component); 
                field.set("v.errors", [{message:"Please enter a Value"}]);*/
            }    
        }
             
    },
    
    
    
    UIInputDescValidate : function(component,event,message,Targetid,mapLabel,AuraId){
        var field = component.find(AuraId);
        console.log('field aura '+field);
        var fieldvalue = field.get('v.value');
        //alert(fieldvalue);
       
        if(fieldvalue){
            console.log('good');
            this.setFieldSuccess(Targetid,component);
            this.UpdateMapflag(component,mapLabel,'1');
            field.set("v.errors", null);
            return false;
        }else{
            console.log('bad');
            this.setFieldError(Targetid,component);
            this.UpdateMapflag(component,mapLabel,'0');
            field.set("v.errors", [{message:message}]);
            return true;
        }      
    },
    
    
        
   setFieldSuccess : function(formGroupDivAuraId, component){
        console.log('before setfieldsuccess')
        var formGroupDiv = component.find(formGroupDivAuraId);
        console.log('setfieldsuccess');
        $A.util.removeClass(formGroupDiv, 'has-error');
        $A.util.addClass(formGroupDiv, 'has-success');       
    },
    
    setFieldError : function(formGroupDivAuraId, component){ 
        var formGroupDiv = component.find(formGroupDivAuraId);
        $A.util.removeClass(formGroupDiv, 'has-success');
        $A.util.addClass(formGroupDiv, 'has-error');              
    },
    
	validateTotalAmountHelper : function(component,event){
        this.UIInputTagValidate(component,event,'Please enter valid amount','billing-TotalAmount','TotalAmount');
    	
    } ,
    
    validNumberTransactionHelper : function(component,event){
        this.UIInputTagValidate(component,event,'Please enter valid number of transactions','billing-NumberTransaction','NumberTransactions');
    	
    },
    
    /*validDescriptionHelper : function (component,event){
        this.UIInputDescValidate(component,event,'Please enter description','billing-description','description');
    	
          
    },*/
    
    validSubjectHelper: function (component,event){
        var retVar = this.UIInputDescValidate(component,event,'Please enter subject','billing-subject','Subject','subject');
        return retVar;
    },
    
    
    UpdateMapflag: function(component,key,flagvalue){
        var flagMap = component.get('v.SectionOneFieldsFlag'); 
        //console.log('getting map successfully');
        flagMap[key] = flagvalue;
		component.set('v.SectionOneFieldsFlag',flagMap);
		//console.log('map update finished');        
    },
    /*validNumberTransactionHelper : function(component,event){
        this.UIInputTagValidate(component,event,'Please enter Valid Number of Transactions','billing-description','description');
    	this.SectionTwoSummaryCheck(component,event);
    }*/
    recordTypeChangeHelper : function(component,event,helper){
        console.log("reached rtype change");
        component.set("v.rtypeSelected",document.getElementById("caserecordtypes").value);
        var selectedvalue = document.getElementById("caserecordtypes").value;         
        var RecordTypeMap = component.get("v.caseTypeMaps")[selectedvalue];
        
        
        if(selectedvalue){
            //we show the tick mark
            helper.setFieldSuccess("billing-record-type",component);
			helper.UpdateMapflag(component,'CaseType','1');	            
            
            
            // Prepare the action to create the new contact
            var getTypeAction = component.get("c.getTypeOptions");
            getTypeAction.setParams({
                "recordTypeTypesMaps": RecordTypeMap
            });
            
            getTypeAction.setCallback(this, function(response) {
                var state = response.getState();
                var typeOptions = [];
                if(state === "SUCCESS") {
                    var ReturnValues = response.getReturnValue();
                    console.log('Returned value key is '+ReturnValues.key);	               
                    var typeOptions = [];
                    for (var i = 0; i < ReturnValues.length; i++){
                        
                        for (var key in ReturnValues[i]) {
                            console.log('the key is '+key);                        
                            console.log('the value is '+ReturnValues[i][key]);
                            var typeOption = {label:key, value:ReturnValues[i][key]};                        
                        }
                        typeOptions.push(typeOption);
                        
                    }
                    
                }
                component.set("v.casetypes", typeOptions);            
            });
            // Send the request to create the new contact
            $A.enqueueAction(getTypeAction);
            return false;
            
        }else{
            //alert('no good');
            helper.setFieldError("billing-record-type",component);
            component.set("v.casetypes",null);
            return true;
        }
        
    },
    
    SubcategoryChangeHelper : function(component,event,helper){
        console.log("reached SUbcategory change");
        component.set("v.subCatValue",document.getElementById("casetypes").value);
        var selectedvalue = document.getElementById("casetypes").value;         
        if(selectedvalue){
          helper.setFieldSuccess("billing-type",component);
          helper.UpdateMapflag(component,'SubType','1');
            return false;
        } else {
            helper.setFieldError("billing-type",component);
            helper.UpdateMapflag(component,'SubType','0'); 
            return true
        }
    },
    
    IfCaseDeleteCheckHelper : function(component,event,helper){
        var caseRecordId = component.get('v.newCase').Id;
        console.log('IfCaseDeleteCheckHelper case record id is '+caseRecordId);
		
        if(caseRecordId){
            console.log('found the case id, doing delete check now '+caseRecordId);
            var CaseDeleteCheckAction = component.get("c.CaseDeletionCheck");;            
            CaseDeleteCheckAction.setParams({
                "Inputcaseid": caseRecordId              
            });
            
            
            CaseDeleteCheckAction.setCallback(this, function(response){
                var state = response.getState();
                console.log('the status is '+state);
                if(state === 'SUCCESS'){
                    var result = response.getReturnValue();
                    console.log('IfCaseDeleteCheckHelper case return result is '+result);
                    if(!document.getElementById("RSPBillingTimeoutErrorMsg").classList.contains("invisible")){
                        document.getElementById("RSPBillingTimeoutErrorMsg").classList.add("invisible");
                    }
                    
                    //return result;  
                    if(result){
                        console.log('the case is found');
                        component.set("v.IfCaseDeleteFlag",false);  
                        console.log('execution the logic function');
                        helper.LogicFunctionsAfterDeleteCheck(component,event,helper); 
                    }else{
                        component.set("v.IfCaseDeleteFlag",true); 
                    }
             
                }else if (state === "ERROR") {
                    console.log('error state log');
                    component.set("v.IfCaseDeleteFlag",true); 
                    console.log('error state log step 2');
                    if(document.getElementById("RSPBillingTimeoutErrorMsg").classList.contains("invisible")){
                        document.getElementById("RSPBillingTimeoutErrorMsg").classList.remove("invisible");
                    }
                    console.log('error state log step 3');
                    
                    var flag = component.get("v.attachmentError");
                    console.log('the flag is '+flag);
                    if(flag){
                        component.set("v.attachmentError",false)
                    }
                }
				//console.log('the delete flag is '+component.get("v.IfCaseDeleteFlag"));                
            });           
            $A.enqueueAction(CaseDeleteCheckAction);      
        }
        
    },
    
    LogicFunctionsAfterDeleteCheck : function(component,event,helper){
        var validationerror = 'false';
        if(component.get("v.showAttachmentSection")){
            console.log('validationerror 0 '+validationerror);
            console.log('validationerror 0 '+typeof validationerror);
            validationerror= helper.UIInputTagValidate(component,event,'','','attachmentSection');
            console.log('validation error in 1 '+validationerror);
            console.log('validation error in 1 '+typeof validationerror);
        }else{
            validationerror='true';
        }
        console.log('validation error final '+validationerror);
        console.log('validation error final '+typeof validationerror);
        if(validationerror=='true'){
            //component.set("v.SubmitFlag",false);
            //added to send email
            var sendEmailAction = component.get("c.sendBillingAckEmail");     
            var caseId = component.get("v.newCase").Id;
            var conId = component.get("v.newCase").ContactId;
            sendEmailAction.setParams({
                "caseID": caseId,
                "contactID": conId,               
            });
            console.log ('CaseID' +JSON.stringify(component.get("v.newCase")));
            console.log ('ContactID' +component.get("v.newCase").ContactId);
            // Configure the response handler for the action
            sendEmailAction.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    // $A.get("e.force:refreshView").fire();
                    console.log('SUCCESS '+response.getReturnValue());
                    component.set("v.SubmitFlag",false); 
                    component.set("v.showAttachmentSection",true); 
                }
                else if (state === "ERROR") {
                    console.log('Problem saving contact, response state: ' + state+' err '+response.getError()[0].message);
                }
                    else {
                        console.log('Unknown problem, response state: ' + state);
                    }
            });
            
            // Send the request to create the new contact
            $A.enqueueAction(sendEmailAction);
            
            //Added by Wayne for RSP billing improvement
            helper.UpdateExistingCaseFormStatus(component,event);
        }
               
    },
    
    UpdateExistingCaseFormStatus : function(component,event){
        //Added by wayne to update case form submit status when click submit button
        var existingcaseId = component.get("v.newCase").Id;
        console.log('case id is '+existingcaseId);
        if((existingcaseId)){
            console.log('case id is '+existingcaseId);
            var UpdateCaseFormStautsAction = component.get("c.updateExistingCaseFormStatus");
            UpdateCaseFormStautsAction.setParams({
                "caseid": existingcaseId              
            });
            
            UpdateCaseFormStautsAction.setCallback(this, function(response) {
                var state = response.getState();
                if(state === 'SUCCESS'){
                    var result = response.getReturnValue();
                    console.log('Updating case successfully '+result);
                }else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors){
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +errors[0].message);
                        }
                    }else{
                        console.log("Unknown error");
                    }
                }                
            });           
            $A.enqueueAction(UpdateCaseFormStautsAction);            
        }         
    },
    
})