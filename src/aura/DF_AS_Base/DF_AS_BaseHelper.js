({	
	ERROR_RESPONSE_STATUS: 'ERROR',
	ERROR_RESPONSE_TYPE: 'Banner',

	convertArrayOfObjectsToCSV: function(cmp, objectRecords, keys){
        // declare variables
        var csvStringResult, counter, columnDivider, lineDivider;

	    // in the keys valirable store fields API Names as a key 
        // this labels use in CSV file header  
       
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n'; 
        
        csvStringResult = '';
        csvStringResult += keys.join(columnDivider);
        csvStringResult += lineDivider;
 
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;           
             for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
 
              // add , [comma] after every String value,. [except first]
                  if(counter > 0){ 
                      csvStringResult += columnDivider; 
                   }   
               
               csvStringResult += '"'+ objectRecords[i][skey]+'"'; 
               
               counter++;
 
            } // inner for loop close 
             csvStringResult += lineDivider;
          }// outer main for loop close 
       
       // return the CSV formate String 
        return csvStringResult;        
    },

	refreshLds: function (cmp, ldsName, attrFieldsToQuery) {		
		var fieldsToQuery = cmp.get(attrFieldsToQuery);
		
		if(fieldsToQuery != null && fieldsToQuery.length)
		{
			cmp.find(ldsName).reloadRecord();
		}
	},

	handleLdsError: function (cmp, ldsName, error) {
		cmp.set("v.responseStatus", "ERROR");
        cmp.set("v.responseMessage", $A.get("$Label.c.DF_Application_Error"));
        cmp.set("v.messageType", "Banner");		
		console.log('lds error - ' + ldsName + ' : ' + error);
	},

	getFormatedDate: function(dateToFormat){

        var dd = dateToFormat.getDate();
        var mm = dateToFormat.getMonth() + 1; //January is 0!
        var yyyy = dateToFormat.getFullYear();

        // if date is less then 10, then append 0 before date   
        if(dd < 10){
            dd = '0' + dd;
        } 

        // if month is less then 10, then append 0 before date    
        if(mm < 10){
            mm = '0' + mm;
        }

		return yyyy+'-'+mm+'-'+dd;
	},
	
	validateInputCmpEmpty: function(inputCmp) {
		return this.validateInputCmpEmptyAndUpdateValidity(inputCmp, true);
	},

	validateInputCmpEmptyWithoutUpdatingValidity: function(inputCmp) {
		return this.validateInputCmpEmptyAndUpdateValidity(inputCmp, false);
	},

	//updateInputValidity - Sometimes we do not want to update Input Validity because if the input is disabled, the error message does not go away
	validateInputCmpEmptyAndUpdateValidity: function(inputCmp, updateInputValidity){
		var value = inputCmp.get("v.value");
		var valid = true;
		if ($A.util.isEmpty(value) || $A.util.isUndefined(value)) {
			if(updateInputValidity) {
        		this.setValueMissing(inputCmp);
				inputCmp.showHelpMessageIfInvalid();
			}
			valid = false;
        }else if ((typeof value) == 'string' && $A.util.isEmpty(value.trim())) {			
			inputCmp.set("v.value", null);
			if(updateInputValidity) {
				this.setValueMissing(inputCmp);				
				inputCmp.showHelpMessageIfInvalid();
			}
			valid = false;
		}
		return valid;
	},

	//This helper method is to resolve the issue of validation message not showing until focus twice
	isAllValid: function (cmp, tempCtrl, fieldGroupName) {
		var allValid = cmp.find(fieldGroupName).reduce( 
			function (validFields, inputCmp) {				
				inputCmp.showHelpMessageIfInvalid();
				this.reFocus(inputCmp, tempCtrl);				
				return validFields && inputCmp.get('v.validity').valid;
		}, true);

		return allValid;
	},

	getTodayDateString: function (cmp) {
		var today = new Date();		
		var dd = today.getDate();
		var mm = today.getMonth() + 1; //January is 0!
		var yyyy = today.getFullYear();
		return yyyy + '-' + mm + '-' + dd;
	},

	get30DaysBeforeTodayDateString: function (cmp) {
		var numberOfDaysToAdd = -30;		
		var dateAfter30Days = new Date(new Date().getTime() + (numberOfDaysToAdd*24*60*60*1000));	
		
		var dd = dateAfter30Days.getDate();
		var mm = dateAfter30Days.getMonth() + 1; //January is 0!
		var yyyy = dateAfter30Days.getFullYear();
		return yyyy + '-' + mm + '-' + dd;
	},

	get1DayAfterDateString: function (cmp, date) {	
		var startDate = new Date(date);
		var numberOfDaysToAdd = 1;
		var dateAfter1Day = new Date(startDate.getTime() + (numberOfDaysToAdd*24*60*60*1000));	
		
		var dd = dateAfter1Day.getDate();
		var mm = dateAfter1Day.getMonth() + 1; //January is 0!
		var yyyy = dateAfter1Day.getFullYear();
		return yyyy + '-' + mm + '-' + dd;
	},	

	isValidDate: function (dateString) {
        if(dateString == "" || dateString == null){ 
        	return true;
        }else{
            var regEx = /^\d{4}-\d{2}-\d{2}$/;
            if(!dateString.match(regEx)) return false;  // Invalid format
            var d = new Date(dateString);
            if(!d.getTime() && d.getTime() !== 0) return false; // Invalid date
            return d.toISOString().slice(0,10) === dateString;
        }
    },

    isValidDateTime: function (datetimeString) {
        if(datetimeString == "" || datetimeString == null){ 
        	return true;
        }else{
            var regEx = /^\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d(\.\d+)?(([+-]\d\d:\d\d)|Z)?$/i;
            if(!datetimeString.match(regEx)){ 
            	return false; 
            } else {
            return true;
            } 
        }
    },

	convertPickList: function(cmp, attrName) {
		var options = cmp.get(attrName);
			options = options.map(function(item){
				return { label: item, value: item };
		});
		cmp.set(attrName, options);
	},

	showASSpinner: function(cmp) {
		// Display spinner when aura:waiting (server waiting)		
		cmp.set("v.toggleASSpinner", true);
	},

	hideASSpinner: function(cmp) {	
   		// Hide when aura:doneWaiting		
		window.setTimeout(
			$A.getCallback(function() {
				var spinner = cmp.find('spinner');
				if(spinner){					
					var evt = spinner.get("e.toggle");
					evt.setParams({ isVisible : false });
					evt.fire();					
				}
				cmp.set("v.toggleASSpinner", false);
			}), 1
		);	
    },

})