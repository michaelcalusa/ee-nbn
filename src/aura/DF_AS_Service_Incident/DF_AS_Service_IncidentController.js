({		
	onTestIdClick: function (cmp, event, helper) {		
		var selectedTestId = event.target.getAttribute("data-id");
		cmp.set('v.selectedTestId', selectedTestId);
		cmp.set('v.displayedDetailItem', cmp.get('v.testDetailsComponent'));
	},

	onClickBPILink: function (cmp, event, helper) {             
        var selectedBpiId = event.target.getAttribute("data-Id");
		cmp.set('v.selectedBPIId', selectedBpiId);
		cmp.set('v.sourceDisplayedDetailItem', cmp.get('v.componentName'));
		cmp.set('v.displayedDetailItem', cmp.get('v.activeServiceSearchComponent'));		
	},

	onSiteOperatingHoursChange: function (cmp, event, helper) {
		cmp.set("v.availabilityDateValidationError" , false);
		helper.toggleAvailabilityWindowFields(cmp);
    }, 

	onDateChange: function (cmp, event, helper) {
		helper.validateDate(cmp);
	},

    cancel: function (cmp, event, helper) {
        cmp.set('v.displayedDetailItem', '');
    },

	returnToSearch: function (cmp, event, helper) {
		if(cmp.get('v.mode') == 'New') cmp.set('v.refreshSearch', true);
        cmp.set('v.displayedDetailItem', '');
		$A.util.addClass(event.getSource(), 'slds-hide');
    },	

    goBack: function (cmp, event, helper) {
        var currentStep = cmp.get('v.currentStep');
        var minStep = cmp.get('v.minStep');
        if (currentStep > minStep) currentStep -= 1;
		else {
			cmp.set('v.defaultTabId', "1");
			cmp.set('v.currentTabId', '1');
		}
        cmp.set('v.currentStep', currentStep);
		helper.clearCustomValidation(cmp);
    },

    goNext: function (cmp, event, helper) {
		
		if (!helper.validate(cmp, event)) return;

        var currentStep = cmp.get('v.currentStep');
        var maxStep = cmp.get('v.maxStep');		
        if (currentStep < maxStep) currentStep += 1;        
		cmp.set('v.currentStep', currentStep);
		if (currentStep == maxStep) {
			helper.setShortTimeFormat(cmp, helper);
			cmp.set("v.incidentFields.Technology__c", 'Fibre');
		}
    },    

    onOrderRecordUpdated: function (cmp, event, helper) {		
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {} else if (changeType === "LOADED") {            		
        } else if (changeType === "REMOVED") {} else if (changeType === "CHANGED") {            
            
        }
    },

	onTestRecordUpdated: function (cmp, event, helper) {		
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {} else if (changeType === "LOADED") {
			if(cmp.get('v.mode') == 'New')
			{
				cmp.set('v.selectedOVCs', [cmp.get("v.testFields.Req_OVC_Id__c")]);
			}
			cmp.set('v.isTestFieldLoaded', true);
        } else if (changeType === "REMOVED") {} else if (changeType === "CHANGED") {            
        }
    },

	onIncRecordUpdated: function (cmp, event, helper) {
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {} else if (changeType === "LOADED") {
			if(cmp.get('v.mode') == 'Edit') {
				var selectedBPIId = cmp.get('v.incidentFields.PRI_ID__c');
				helper.getProductInfoFromServiceCache(cmp, selectedBPIId);

				cmp.set('v.dfOrderId', cmp.get('v.incidentFields.DF_Order__c'));
				cmp.set('v.testId', cmp.get('v.incidentFields.Test_Reference__c'));				
				helper.setShortTimeFormat(cmp, helper);
				cmp.find("forceRecordOrdCmp").reloadRecord();
				
				if(cmp.get('v.testId') != null) {
					cmp.find("forceRecordTestCmp").reloadRecord();
				}

				var industryStatus = cmp.get("v.incidentFields.Industry_Status__c");
				var inProgressPendingStatus = 'In Progress Pending';
				var inProgressHeldStatus = 'In Progress Held';
				var resolvedStatus = 'Resolved';
				var closedStatus = 'Closed';				

				cmp.set('v.isAdditionalInfoRequired', (industryStatus == inProgressPendingStatus) ? true : false);
				cmp.set('v.isReasonCodeVisiable', (industryStatus == inProgressPendingStatus || industryStatus == inProgressHeldStatus) ? true : false);
				cmp.set('v.isResolved', (industryStatus == resolvedStatus || industryStatus == closedStatus) ? true : false);

			}						
        } else if (changeType === "REMOVED") {} else if (changeType === "CHANGED") {            
            
        }
    },
    
	handleTestIdChange: function (cmp, event, helper) {
		if(cmp.get('v.testId') != null) {
			cmp.find("forceRecordTestCmp").reloadRecord();
		}
	},

    submit: function (cmp, event, helper) {		
        cmp.set("v.incidentFields.DF_Order__c", cmp.get('v.dfOrderFields.Id'));				
		cmp.set('v.incidentFields.Test_Reference__c', cmp.get('v.testFields.Id'));
		
        cmp.find("forceRecordIncidentCmp").saveRecord(function (saveResult) {            
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
				cmp.set("v.incId", saveResult.recordId);				
				//Update Incident__c in TND_Result__c
				helper.apex(cmp, 'v.responseResult', 'c.updateTestRecordwithIncident', {
					testId: cmp.get('v.testFields.Id'),
					incidentId: saveResult.recordId
				}, false)
				.then(function (result) {					
					cmp.set("v.responseStatus", "OK");
					cmp.set("v.responseMessage", cmp.get('v.customSettings.Service_INC_Details_Save_OK'));
					cmp.set("v.messageType", "Banner");
				})
				.catch(function (result) {					
					var cmp = result.sourceCmp;
					if(cmp){
						var helper = result.sourceHelper;				
						helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");		
					}else
					{
						console.log(result);
					}
				});
            } else {
                cmp.set("v.responseStatus", "ERROR");
                cmp.set("v.responseMessage", $A.get("$Label.c.DF_Application_Error"));
                cmp.set("v.messageType", "Banner");
                if (saveResult.state === "INCOMPLETE") {

                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    console.log('Problem saving incident, error: ' +
                        JSON.stringify(saveResult.error));
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state +
                        ', error: ' + JSON.stringify(saveResult.error));
                }
            }
        });

    },

    init: function (cmp, event, helper) {
		//debugger;
		if(cmp.get("v.mode") == 'New'){
			helper.prepareNewFormData(cmp, helper);
		}else{
			helper.prepareEditFormData(cmp, helper);
		}
    },

    toggleAddInfo : function(cmp, event, helper) {
        cmp.set("v.isAdditionInfoVisible", true);			
		window.setTimeout(
			$A.getCallback(function() {
				var ctrl = cmp.find('addInfoCmp');
				if(ctrl){		
					ctrl.focus();									
				}					
			}), 200
		);    	
    },

    toggleAddNotes : function(cmp, event, helper) {
		cmp.set("v.isAdditionNotesVisible", true);			
		window.setTimeout(
			$A.getCallback(function() {
				var ctrl = cmp.find('addNotesCmp');
				if(ctrl){		
					ctrl.focus();									
				}					
			}), 500
		);        
    },

    saveAdditionalNotes : function(cmp, event, helper) {

       var inputNotes = cmp.find("addNotesCmp").get("v.value"); 
       var promise1 = helper.apex(cmp, 'v.insertedNoteId', 'c.createIncidentNotes', {incId: cmp.get('v.incId'),noteDetail:inputNotes,noteType:'General Information'}, false);

        promise1
            .then(function (result) {   
            	var cmp = result.sourceCmp;
				var promise = helper.apex(cmp, 'v.allNotesList', 'c.getIncidentNotes', {incId: cmp.get('v.incId')}, false);
				promise.catch(function (result) {
				var error = result.error;
                $A.reportError("error message", error);
				});
				cmp.set("v.isAdditionInfoVisible", false);	
				cmp.set("v.isAdditionNotesVisible", false);
		    })
            .catch(function (result) {
                var cmp = result.sourceCmp;
				var helper = result.sourceHelper;				
                helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
            });
    },

    saveAdditionalInfo : function(cmp, event, helper) {

       var inputNotes = cmp.find("addInfoCmp").get("v.value"); 
       var promise1 = helper.apex(cmp, 'v.insertedNoteId', 'c.createIncidentNotes', {incId: cmp.get('v.incId'),noteDetail:inputNotes,noteType:'Detail Clarification'}, false);

        promise1
            .then(function (result) {    
            	var cmp = result.sourceCmp;   
				var promise = helper.apex(cmp, 'v.allNotesList', 'c.getIncidentNotes', {incId: cmp.get('v.incId')}, false);
				promise.catch(function (result) {
				var error = result.error;
                $A.reportError("error message", error);
				});
				cmp.set("v.isAdditionInfoVisible", false);	
				cmp.set("v.isAdditionNotesVisible", false);			
            })
            .catch(function (result) {
                var cmp = result.sourceCmp;
				if(cmp){
					var helper = result.sourceHelper;				
					helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");		
				}else
				{
					console.log(result);
				}
            });
    },

	handleAccountLdsError: function (cmp, event, helper) {
		helper.handleLdsError(cmp, 'account', event.getParam("value"));
	},	

	handleIncLdsError: function (cmp, event, helper) {
		helper.handleLdsError(cmp, 'incident', event.getParam("value"));
	},

	handleTnDLdsError: function (cmp, event, helper) {
		helper.handleLdsError(cmp, 'tnd', event.getParam("value"));
	},

	handleOrderLdsError: function (cmp, event, helper) {
		helper.handleLdsError(cmp, 'order', event.getParam("value"));
	},
		
	handleServiceCacheProductInfoUpdate: function (cmp, event, helper) {
		if(cmp.get('v.mode') == 'New' && cmp.get('v.incidentFields') != null) {
			cmp.set('v.incidentFields.serviceRestorationSLA__c', cmp.get('v.serviceCacheProductInfo.sla'));
			cmp.set('v.incidentFields.serviceRegion__c', cmp.get('v.serviceCacheProductInfo.enterpriseEthernetServiceLevelRegion'));
			cmp.set('v.incidentFields.locId__c', cmp.get('v.serviceCacheProductInfo.locationId'));
		}
	},
})