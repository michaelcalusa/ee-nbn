({
	setShortTimeFormat: function(cmp, helper) {
		var time = cmp.get('v.incidentFields.Availability_Start_Time__c')
		if(!$A.util.isEmpty(time) && !$A.util.isUndefined(time)) {			
			cmp.set('v.short_Availability_Start_Time__c', time.substring(0, 5));
		}
	},

	getProductInfoFromServiceCache: function (cmp, selectedBPIId) {		
		var promise1 = this.apex(cmp, 'v.serviceCacheProductInfo', 'c.searchServiceCache', { searchString: selectedBPIId }, false);

		Promise.all([promise1])
		.then(function(result) {			
		})
		.catch(function(result) {
			var cmp = result.sourceCmp;
			if(cmp){
				var errorMessage = $A.get("$Label.c.DF_Application_Error");
				var helper = result.sourceHelper;				
				var isServiceCacheAction = result.sourceAction == 'c.searchServiceCache';			

				// Only show error banner when it is not related to service cache. This is due to:
				// EE_AS_Test_Creation component already handles the error banner in tab 1
				// EE_AS_Multiple_OVC component already handles the error banner in tab 2

				if(!isServiceCacheAction)
				{
					helper.setErrorMsg(cmp, "ERROR", errorMessage , "BANNER");
				}
			}else
			{
				console.log(result);
			}
		});
	},

	prepareNewFormData: function(cmp, helper) {		
		var selectedBPIId = cmp.get('v.selectedBPIId');
		this.getProductInfoFromServiceCache(cmp, selectedBPIId);

		// Prepare a new record from template
		cmp.find("forceRecordIncidentCmp").getNewRecord(
			cmp.get("v.incObjectApiName"), // sObject type (objectApiName)
			null, // recordTypeId
			false, // skip cache?
			$A.getCallback(function () {				
				cmp.set('v.incidentFields.PRI_ID__c', cmp.get('v.selectedBPIId'));
			})
		);

		var promise1 = this.apex(cmp, 'v.customSettings', 'c.getASCustomSettings', {
            componentName: cmp.get('v.componentName')
        }, false);

		var promise2 = this.apex(cmp, 'v.siteInductionEssentialOptions', 'c.getASPickListValuesIntoList', {
            objectApiName: cmp.get('v.incObjectApiName'),
            fieldApiName: 'Site_Induction_Essentials__c'
        }, true);

        var promise3 = this.apex(cmp, 'v.securityRequiredOptions', 'c.getASPickListValuesIntoList', {
            objectApiName: cmp.get('v.incObjectApiName'),
            fieldApiName: 'Security_Required__c'
        }, true);

        var promise4 = this.apex(cmp, 'v.currentWhiteCardOptions', 'c.getASPickListValuesIntoList', {
            objectApiName: cmp.get('v.incObjectApiName'),
            fieldApiName: 'Current_White_Card__c'
        }, true);

        var promise5 = this.apex(cmp, 'v.securityClearanceOptions', 'c.getASPickListValuesIntoList', {
            objectApiName: cmp.get('v.incObjectApiName'),
            fieldApiName: 'Security_Clearance__c'
        }, true);

        var promise6 = this.apex(cmp, 'v.siteAccessClearanceOptions', 'c.getASPickListValuesIntoList', {
            objectApiName: cmp.get('v.incObjectApiName'),
            fieldApiName: 'Security_Clearance__c'
        }, true);

        var promise7 = this.apex(cmp, 'v.siteOperationHoursOptions', 'c.getASPickListValuesIntoList', {
            objectApiName: cmp.get('v.incObjectApiName'),
            fieldApiName: 'Site_Operating_Hours__c'
        }, true);

        var promise8 = this.apex(cmp, 'v.availabilityStartTimeOptions', 'c.getASPickListCustomSettings', {
            picklistName: 'AvailabilityStartTime'
        }, false);

		var promise9 = this.apex(cmp, 'v.faultTypeOptions', 'c.getASPickListCustomSettings', {
            picklistName: 'FaultType'
        }, false);		

        Promise.all([promise1, promise2, promise3, promise4, promise5, promise6, promise7, promise8, promise9])
            .then(function (result) {				
                helper.convertPickList(cmp, 'v.siteInductionEssentialOptions');
                helper.convertPickList(cmp, 'v.securityRequiredOptions');
                helper.convertPickList(cmp, 'v.currentWhiteCardOptions');
                helper.convertPickList(cmp, 'v.securityClearanceOptions');
                helper.convertPickList(cmp, 'v.siteAccessClearanceOptions');
                helper.convertPickList(cmp, 'v.siteOperationHoursOptions');

                var availabilityStartTimeOptions = cmp.get('v.availabilityStartTimeOptions');
                availabilityStartTimeOptions.sort(helper.dynamicSort(cmp, 'label'));
                cmp.set('v.availabilityStartTimeOptions', availabilityStartTimeOptions);
				var faultTypeOptions = cmp.get('v.faultTypeOptions');
                faultTypeOptions.sort(helper.dynamicSort(cmp, 'label'));
                cmp.set('v.faultTypeOptions', faultTypeOptions);

                cmp.set('v.initDone', true);
            })
            .catch(function (result) {
                var cmp = result.sourceCmp;
				if(cmp){
					var helper = result.sourceHelper;				
					helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");		
				}else
				{
					console.log(result);
				}
            });
	},
	
	prepareEditFormData: function (cmp, helper) {
		cmp.set("v.currentStep", cmp.get('v.detailStepId'));
		cmp.set("v.onlyShowDefaultTabId", false);
		var promise1 = this.apex(cmp, 'v.customSettings', 'c.getASCustomSettings', {
            componentName: cmp.get('v.componentName')
        }, false);
        var promise2 = helper.apex(cmp, 'v.allNotesList', 'c.getIncidentNotes', {incId: cmp.get('v.incId')}, false);
        Promise.all([promise1,promise2])
            .then(function (result) {       
				cmp.find("forceRecordIncidentCmp").reloadRecord();				
				cmp.set("v.currentTabId", "2");	
				cmp.set('v.initDone', true);			
            })
            .catch(function (result) {
                var cmp = result.sourceCmp;
				if(cmp){
					var helper = result.sourceHelper;				
					helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");		
				}else
				{
					console.log(result);
				}
            });
	},

	clearCustomValidation: function(cmp){
		cmp.set("v.summaryValidationError", false);
		cmp.set("v.availabilityDateValidationError" , false);
	},

	validate: function (cmp, event) {
		var currentStep = cmp.get("v.currentStep");
		var tempCtrl = cmp.find("btnNext");
		var valid = true;
		var fieldGroupName;

		if (currentStep == 1) {
			fieldGroupName = 'step1Fields';
			var ctrls = cmp.find(fieldGroupName);
			var ctrl = ctrls.filter(this.getComponentByName("Fault_Type__c"))[0];
			this.validateSelectCmpEmpty(ctrl);			
			ctrl = ctrls.filter(this.getComponentByName("Incident_Notes__c"))[0];
			this.validateInputCmpEmpty(ctrl);
			
			valid = this.isAllFieldsValid(cmp, fieldGroupName);
			
			if(!this.validateMultipleOVC(cmp))
			{
				valid = false;
			}
		}	

		if (currentStep == 2) {
			fieldGroupName = 'step2Fields';
			var ctrls = cmp.find(fieldGroupName);
			var ctrl = ctrls.filter(this.getComponentByName("Technical_Contact_Name__c"))[0];
			this.validateInputCmpEmpty(ctrl);
			
			ctrl = ctrls.filter(this.getComponentByName("Technical_Contact_Mobile_Number__c"))[0];
			this.validateInputCmpEmpty(ctrl);
			
			ctrl = ctrls.filter(this.getComponentByName("Site_Contact_Name__c"))[0];
			this.validateInputCmpEmpty(ctrl);			

			ctrl = ctrls.filter(this.getComponentByName("Site_Contact_Mobile_Number__c"))[0];
			this.validateInputCmpEmpty(ctrl);			

			ctrl = ctrls.filter(this.getComponentByName("Business_Name__c"))[0];
			this.validateInputCmpEmpty(ctrl);			

			ctrl = ctrls.filter(this.getComponentByName("Site_Induction_Essentials__c"))[0];
			this.validateSelectCmpEmpty(ctrl);
			
			ctrl = ctrls.filter(this.getComponentByName("Security_Required__c"))[0];
			this.validateSelectCmpEmpty(ctrl);

			ctrl = ctrls.filter(this.getComponentByName("Current_White_Card__c"))[0];
			this.validateSelectCmpEmpty(ctrl);

			ctrl = ctrls.filter(this.getComponentByName("Security_Clearance__c"))[0];
			this.validateSelectCmpEmpty(ctrl);

			ctrl = ctrls.filter(this.getComponentByName("Site_Access_Clearance__c"))[0];
			this.validateSelectCmpEmpty(ctrl);

			valid = this.isAllFieldsValid(cmp, fieldGroupName);	
		}

		if (currentStep == 3) {
			
			fieldGroupName = 'step3Fields';
			var ctrls = cmp.find(fieldGroupName); 			

			ctrl = ctrls.filter(this.getComponentByName("Site_Operating_Hours__c"))[0];
			this.validateSelectCmpEmpty(ctrl);			
			valid = this.isAllFieldsValid(cmp, fieldGroupName);	

			var summaryValid = true;
			if (cmp.get("v.otherSiteOperateingHourSelected")) {
				ctrl = cmp.find("Additional_Comments__c");
				summaryValid = this.validateInputCmpEmptyWithoutUpdatingValidity(ctrl);
				if (!summaryValid){
					cmp.set("v.summaryValidationError", true);
					cmp.set("v.summaryValidationErrorMsg", cmp.get("v.customSettings.Service_INC_Details_Summary_Required"));
					valid = false;
				}else{
					cmp.set("v.summaryValidationError", false);
				}
			
				ctrl = ctrls.filter(this.getComponentByName("Availability_Start_Time__c"))[0];
				this.validateSelectCmpEmpty(ctrl);			

				valid = this.isAllFieldsValid(cmp, fieldGroupName);	
				if(!this.validateDate(cmp) || !summaryValid){
					valid = false;
				}
			}
		}

		if(!valid) event.getSource().focus();
		
		return valid;
	},

	validateDate: function(cmp){

		var valid = true;
		var selectedDate = cmp.get("v.incidentFields.Availability_Date__c");						
		var selectedDateValid = true;

		if($A.util.isEmpty(selectedDate) || $A.util.isUndefined(selectedDate)) {
			cmp.set("v.availabilityDateValidationError" , true);	
			cmp.set("v.availabilityDateValidationErrorMsg" , cmp.get('v.customSettings.Service_INC_Details_Date_Required'));				
			selectedDateValid = false;
		}else if (!this.isValidDate(selectedDate))
		{
			cmp.set("v.availabilityDateValidationError" , true);	
			cmp.set("v.availabilityDateValidationErrorMsg" , cmp.get('v.customSettings.Service_INC_Details_Date_Invalid'));
			selectedDateValid = false;
		}else
		{
			var site_Operating_Hours__c = cmp.get("v.incidentFields.Site_Operating_Hours__c");
			var otherHoursSelected = site_Operating_Hours__c == 'Other';
			
			if(otherHoursSelected){
				var localCurrentDate = new Date();
				var localYear = localCurrentDate.getFullYear();
				var localMonth = localCurrentDate.getMonth() + 1;
				var localDate = localCurrentDate.getDate();
				var localHours = localCurrentDate.getHours();				
				var availability_Date__c = localYear + (localMonth > 9 ? '-' : '-0') + localMonth + (localDate > 9 ? '-' : '-0') + localDate;
				var availability_Start_Time__c = (localHours > 9 ? '' : '0') + localHours + ':00:00.000Z';

				var currentDatetime = availability_Date__c + availability_Start_Time__c;
				var selectedDatetime = cmp.get('v.incidentFields.Availability_Date__c') + cmp.get('v.incidentFields.Availability_Start_Time__c');
			}

			if(selectedDatetime < currentDatetime){
				cmp.set("v.availabilityDateValidationError" , true);	
				cmp.set("v.availabilityDateValidationErrorMsg" , cmp.get('v.customSettings.Service_INC_Details_Date_Future'));
				selectedDateValid = false;
			}
		}

		if(selectedDateValid) cmp.set("v.availabilityDateValidationError" , false);	
		else valid = false;
		
		return valid;
	},	

	toggleAvailabilityWindowFields: function(cmp) {
		var site_Operating_Hours__c = cmp.get("v.incidentFields.Site_Operating_Hours__c");
		var otherHoursSelected = site_Operating_Hours__c == 'Other';

        cmp.set('v.otherSiteOperateingHourSelected', otherHoursSelected);

		if(!otherHoursSelected && site_Operating_Hours__c != ''){
			this.selectCurrentDateTime(cmp);			
		}

		if(site_Operating_Hours__c == '')
		{
			cmp.set('v.incidentFields.Availability_Date__c', null);
			cmp.set('v.incidentFields.Availability_Start_Time__c', null);
		}

		cmp.set("v.summaryValidationError", false);
	},

	selectCurrentDateTime: function(cmp) {		
		var localCurrentDate = new Date();
		var localYear = localCurrentDate.getFullYear();
		var localMonth = localCurrentDate.getMonth() + 1;
		var localDate = localCurrentDate.getDate();
		var localHours = localCurrentDate.getHours();				
		var availability_Date__c = localYear + (localMonth > 9 ? '-' : '-0') + localMonth + (localDate > 9 ? '-' : '-0') + localDate;
		var availability_Start_Time__c = (localHours > 9 ? '' : '0') + localHours + ':00:00.000Z';

		cmp.set('v.incidentFields.Availability_Date__c', availability_Date__c);
		cmp.set('v.incidentFields.Availability_Start_Time__c', availability_Start_Time__c);
	},

	validateMultipleOVC: function (cmp) {

        var valid = true;
        var selectedOVCs = cmp.get('v.selectedOVCs');
		
		cmp.set('v.incidentFields.Impacted_OVC__c', selectedOVCs.join(','));		
        
        if (selectedOVCs != undefined && selectedOVCs.length > 0) {
            cmp.set('v.multipleOVCValidationError', false);			
        } else {
            valid = false;
            cmp.set('v.multipleOVCValidationError', true);
        }

        return valid;
    },

})