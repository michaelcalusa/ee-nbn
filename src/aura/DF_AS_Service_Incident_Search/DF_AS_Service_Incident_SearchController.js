({

	onSelectedTicketTypeChanged: function (cmp, event, helper) {
		var selectedTicketType = cmp.get('v.selectedTicketType');
		var isSlaFilterDisabled = true;

		cmp.set('v.selectedTicketSLA', '');		

		switch(selectedTicketType) {
			case cmp.get('v.serviceIncidentRecordType'):
				cmp.set('v.ticketStatusOptions', cmp.get('v.ticketStatusOptionsServiceIncident'));
				isSlaFilterDisabled = false;
				break;	

			case cmp.get('v.unplannedOutageRecordType'):
				cmp.set('v.ticketStatusOptions', cmp.get('v.ticketStatusOptionsUnplannedOutage'));
				break;
						
			case cmp.get('v.serviceAlertRecordType'):
				cmp.set('v.ticketStatusOptions', cmp.get('v.ticketStatusOptionsServiceAlert'));
				break;

			case cmp.get('v.changeRequestRecordType'):
				cmp.set('v.ticketStatusOptions', cmp.get('v.ticketStatusOptionsChangeRequest'));
				break;

			case cmp.get('v.serviceRequestRecordType'):
				cmp.set('v.ticketStatusOptions', cmp.get('v.ticketStatusOptionsServiceRequest'));
				break;
								
			default:
				cmp.set('v.ticketStatusOptions', []);
		}		

		cmp.set('v.isSlaFilterDisabled', isSlaFilterDisabled);
	},

	onStartDateChange: function (cmp, event, helper) {
		helper.validateStartDateEndDate(cmp, event);
	},

	onEndDateChange: function (cmp, event, helper) {
		helper.validateStartDateEndDate(cmp, event);
	},

	//helper.apex(cmp, 'v.statusOptions', 'c.getPicklistValueByRecordType', { objectApiName: 'Incident_Management__c', recordTypeName: 'Service Incident', fieldApiName: 'Incident_Status__c' }, false); 

	init: function (cmp, event, helper) {

		var promise1 = helper.apex(cmp, 'v.customSettings', 'c.getASCustomSettings', { componentName: cmp.get('v.componentName') }, false);
		var promise2 = helper.apex(cmp, 'v.ticketStatusOptionsServiceIncident', 'c.getASPickListCustomSettings', { picklistName: 'TicketStatusSI' }, false); 		
		var promise3 = helper.apex(cmp, 'v.ticketStatusOptionsUnplannedOutage', 'c.getASPickListCustomSettings', { picklistName: 'TicketStatusNI' }, false); 		
		var promise4 = helper.apex(cmp, 'v.ticketStatusOptionsChangeRequest', 'c.getASPickListCustomSettings', { picklistName: 'TicketStatusCRQ' }, false); 		
		var promise5 = helper.apex(cmp, 'v.ticketStatusOptionsServiceAlert', 'c.getASPickListCustomSettings', { picklistName: 'TicketStatusSA' }, false);
		var promise5 = helper.apex(cmp, 'v.ticketStatusOptionsServiceRequest', 'c.getASPickListCustomSettings', { picklistName: 'TicketStatusSR' }, false);
		var promise6 = helper.apex(cmp, 'v.ticketTypeOptions', 'c.getASPickListCustomSettings', { picklistName: 'TicketType' }, false); 
		var promise7 = helper.apex(cmp, 'v.ticketSLAOptions', 'c.getASPickListCustomSettings', { picklistName: 'TicketSLA' }, false); 
		var promise8 = helper.apex(cmp, 'v.globalCustomSettings', 'c.getASCustomSettings', { componentName: cmp.get('v.globalCustomSettingComponentName') }, false);

		Promise.all([promise1, promise2, promise3, promise4, promise5, promise6, promise7, promise8])
		.then(function(result) {
			//If search service request feature is toggled off, hide 'service request' in type drop down
			var globalCustomSettings = cmp.get('v.globalCustomSettings');
			if(!globalCustomSettings.Feature_Search_Service_Request) 
			{
				var ticketTypeOptions = cmp.get('v.ticketTypeOptions').filter(function(item){ return item.label != cmp.get('v.serviceRequestRecordType') });
				cmp.set('v.ticketTypeOptions', ticketTypeOptions);
				helper.sortPickListOptionsByLabel(cmp, helper, 'v.ticketTypeOptions');
			}

			helper.sortPickListOptionsByLabel(cmp, helper, 'v.ticketStatusOptionsServiceIncident');
			helper.sortPickListOptionsByLabel(cmp, helper, 'v.ticketStatusOptionsUnplannedOutage');
			helper.sortPickListOptionsByLabel(cmp, helper, 'v.ticketStatusOptionsChangeRequest');
			helper.sortPickListOptionsByLabel(cmp, helper, 'v.ticketStatusOptionsServiceAlert');
			
			cmp.set("v.initDone", true);
		})
		.catch(function(result) {
			var cmp = result.sourceCmp;
			if(cmp){
				var helper = result.sourceHelper;				
				helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");		
			}else
			{
				console.log(result);
			}
		});

		helper.doSearch(cmp);		
	},

	searchTermChange: function(cmp, event, helper) {		
		//Do search if enter key is pressed.
		if(event.getParams().keyCode == 13) {
			cmp.set('v.isTriggeredBySearch', true);
			helper.doSearch(cmp, event);				
		}
	},

	search: function(cmp, event, helper) {
		cmp.set('v.isTriggeredBySearch', true);
		helper.doSearch(cmp, event);
	},    
	
	clear: function (cmp, event, helper) {
		cmp.set('v.searchTerm', '');
		cmp.set('v.selectedTicketStatus', '');
		cmp.set('v.selectedTicketType', '');
		cmp.set('v.selectedStartDate', '');		
		cmp.set('v.selectedEndDate', '');
		cmp.set('v.selectedTicketSLA', '');
		cmp.set('v.startDateValidationError', false);
		cmp.set('v.endDateValidationError', false);
		cmp.set('v.isTriggeredBySearch', false);
		helper.doSearch(cmp, event);
	},

	refreshSearch: function(cmp, event, helper) {
		if(cmp.get('v.refreshSearch'))
		{
			helper.doSearch(cmp, event);
			cmp.set('v.refreshSearch', false);
		}
	},

	toggleSearchOptions: function(cmp, event, helper) {
		var isSearchOptionExpanded = cmp.get('v.isSearchOptionExpanded');		
		if(!isSearchOptionExpanded)
		{
			cmp.set('v.selectedStartDate', '');
			cmp.set('v.selectedEndDate', '');
			cmp.set('v.selectedTicketSLA', '');
			cmp.set('v.startDateValidationError', false);
			cmp.set('v.endDateValidationError', false);
		}
		cmp.set('v.isSearchOptionExpanded', isSearchOptionExpanded ? false : true);

		event.preventDefault();
	},

	goToTestResults: function(cmp, event, helper) {
		event.preventDefault();		
		cmp.set('v.activeSiblingCmp', 'DF_AS_Test');
	},		

	onClick: function (cmp, event, helper) {				
		cmp.set('v.isIncidentDetailOpen', true);
		window.location.hash = "isIncidentDetailOpen";        
        window.addEventListener("hashChange", helper.handleHashChange(location.hash));		
	},

	onComponentTransit: function (cmp, event, helper) {	
		helper.clearErrors(cmp);
	},
	
})