({		
	doSearch: function (cmp, event) {		
		this.clearErrors(cmp);

		if(this.validateStartDateEndDate(cmp, event)) return;

		if(this.validateStartDateEndDateExist(cmp, event)) return;
		
		var searchTerm = cmp.get('v.searchTerm');

		if(searchTerm == null || searchTerm == undefined) searchTerm = "";

		this.apex(cmp, 'v.productCurrentList', 'c.searchServiceCache', { 
			productIdentifier: searchTerm		
		}, false)		
		.then(function(result) {			
			var productCurrentList = cmp.get('v.productCurrentList');			
			if(productCurrentList != null && productCurrentList != undefined)
			{
				if (productCurrentList[0].bpiId == undefined) cmp.set('v.productCurrentList', []);
			}			
		})
		.catch(function(result) {			
			var cmp = result.sourceCmp;
			if(cmp){
				var helper = result.sourceHelper;				
				helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.EE_AS_SVC_NOT_AVAILABLE"), "BANNER");		
			}else
			{
				console.log(result);
			}
		});	
		
		var selectedStartDate = cmp.get('v.selectedStartDate');
		var selectedEndDate = cmp.get('v.selectedEndDate');
		
		var selectedTicketType = cmp.get('v.selectedTicketType');
		var selectedTicketStatus = cmp.get('v.selectedTicketStatus');		
		var selectedTicketSLA = cmp.get('v.selectedTicketSLA');

		this.apex(cmp, 'v.serviceEventFullList', 'c.searchIncidents', { 
			identifier: cmp.get('v.searchTerm'),			
			incidentType: selectedTicketType == 'All' ? '' : selectedTicketType,
			incidentStatus: selectedTicketStatus == 'All' ? '' : selectedTicketStatus, 
			dateRaisedFrom: selectedStartDate,
			dateRaisedTo: selectedEndDate,			
			serviceRestorationSla: selectedTicketSLA == 'All' ? '' : selectedTicketSLA
		}, false)		
		.then(function(result) {
			//debugger;
		})
		.catch(function(result) {
			var cmp = result.sourceCmp;
			if(cmp){
				var helper = result.sourceHelper;				
				helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");		
			}else
			{
				console.log(result);
			}
		});	
	},

	validateStartDateEndDate: function(cmp, event) {
		
		var hasError = false;		
		var selectedStartDate = cmp.get("v.selectedStartDate");
		var selectedEndDate = cmp.get("v.selectedEndDate");
		var isStartDateValid = this.isValidDate(selectedStartDate);
		var isEndDateValid = this.isValidDate(selectedEndDate);

		if(!isStartDateValid){
			cmp.set("v.startDateValidationError" , true);	
			cmp.set("v.startDateValidationErrorMsg" , cmp.get('v.customSettings.Service_INC_Search_Date_Invalid'));
			hasError = true;
		}else
			cmp.set("v.startDateValidationError" , false);			
        
		if(!isEndDateValid){
			cmp.set("v.endDateValidationError" , true);	
			cmp.set("v.endDateValidationErrorMsg" , cmp.get('v.customSettings.Service_INC_Search_Date_Invalid'));
			hasError = true;
		}else
			cmp.set("v.endDateValidationError" , false);

        if(isStartDateValid && isEndDateValid){
			if(!$A.util.isEmpty(selectedEndDate) && !$A.util.isUndefined(selectedEndDate) && selectedStartDate >= selectedEndDate) {
				cmp.set("v.endDateValidationError" , true);
				cmp.set("v.endDateValidationErrorMsg" , cmp.get('v.customSettings.Service_INC_Search_End_Date'));
				hasError = true;
			}else
			{
				cmp.set("v.startDateValidationError" , false);
				cmp.set("v.endDateValidationError" , false);
			}
        }

		return hasError;
	},	

	validateStartDateEndDateExist: function(cmp, event) {
		
		var hasError = false;
		var selectedStartDate = cmp.get("v.selectedStartDate");
		var selectedEndDate = cmp.get("v.selectedEndDate");

		if(!$A.util.isEmpty(selectedStartDate) && !$A.util.isUndefined(selectedStartDate)) {
			if($A.util.isEmpty(selectedEndDate) || $A.util.isUndefined(selectedEndDate)) {
				cmp.set("v.endDateValidationError" , true);	
				cmp.set("v.endDateValidationErrorMsg" , cmp.get('v.customSettings.Service_INC_Search_Date_Required'));				
				hasError = true;
			}
		}
				
		if(!$A.util.isEmpty(selectedEndDate) && !$A.util.isUndefined(selectedEndDate)) {
			if($A.util.isEmpty(selectedStartDate) || $A.util.isUndefined(selectedStartDate)) {
				cmp.set("v.startDateValidationError" , true);	
				cmp.set("v.startDateValidationErrorMsg" , cmp.get('v.customSettings.Service_INC_Search_Date_Required'));
				hasError = true;
			}
		}

		return hasError;
	},

	handleHashChange: function (hashVal) {
		//alert( 'I got called by # value change in the url' + hashVal);
	},
		
})