({
    handleRowSelection: function (cmp, event, helper) {
        
    },

	clickIncLink: function (cmp, event, helper) {
		helper.clearErrors(cmp);
		var selectedIncidentId = event.target.getAttribute("data-Id");
		var recordTypeInfo = cmp.get('v.fullList').filter(function(item){ return item.Id == selectedIncidentId })[0].RecordType.Name
		cmp.set('v.selectedIncidentId', selectedIncidentId);
		
		switch(recordTypeInfo) {
			case cmp.get('v.serviceIncidentRecordType'):
				cmp.set('v.displayedDetailItem', cmp.get('v.serviceIncidentComponent'));
				break;	

			case cmp.get('v.unplannedOutageRecordType'):
				cmp.set('v.displayedDetailItem', cmp.get('v.unplannedOutageComponent'));
				break;
						
			case cmp.get('v.serviceAlertRecordType'):
				cmp.set('v.displayedDetailItem', cmp.get('v.serviceAlertComponent'));
				break;

			case cmp.get('v.changeRequestRecordType'):
				cmp.set('v.displayedDetailItem', cmp.get('v.changeRequestComponent'));
				break;

			case cmp.get('v.serviceRequestRecordType'):
				cmp.set('v.displayedDetailItem', cmp.get('v.serviceRequestComponent'));
				break;
				
			default:
		}		
	},

	handleDisplayedDetailItemChange : function (cmp, event, helper) {
	
		if(cmp.get('v.displayedDetailItem') == cmp.get('v.activeServiceSearchComponent'))
		{			
			window.setTimeout(
				$A.getCallback(function() {
					var activeServiceChildCmp = cmp.find('activeServiceChildCmp');
					activeServiceChildCmp.showServiceSummary(cmp.get('v.selectedBPIId'));
				}), 500
			);		
		}
	},

	onClickBPILink: function (cmp, event, helper) {
        helper.clearErrors(cmp);        
        var selectedBpiId = event.target.getAttribute("data-Id");
		cmp.set('v.selectedBPIId', selectedBpiId);
		cmp.set('v.sourceDisplayedDetailItem', '');		
		cmp.set('v.displayedDetailItem', cmp.get('v.activeServiceSearchComponent'));		
	},	

	clickOrderLink: function (cmp, event, helper) {
		helper.clearErrors(cmp);
		var selectedOrder = {
			Id: event.target.getAttribute("data-Id")
        };
		cmp.set('v.selectedOrder', selectedOrder);
		cmp.set('v.sourceDisplayedDetailItem', '');
        cmp.set('v.displayedDetailItem', cmp.get('v.orderSummaryComponent'));
	},

    handleRowAction: function (cmp, event, helper) {		
        var row = event.getParam('row');
        var action = event.getParam('action');
        switch (action.name) {
            case 'Incident_Number__c':                
                cmp.set('v.displayedDetailItem', cmp.get('v.serviceIncidentComponent'));
				cmp.set('v.selectedIncidentId', row.Id);				
                break;
            case 'PRI_ID__c':
                var selectedOrder = {
                    Id: row.DF_Order__c
                };
                cmp.set('v.selectedOrder', selectedOrder);
                cmp.set('v.displayedDetailItem', cmp.get('v.orderSummaryComponent'));
                break;
        }
    },

    init: function (cmp, event, helper) {	
		
		helper.clearErrors(cmp);
		helper.updateDatatableAndFooter(cmp);	
		helper.populateDictinctBPIArray(cmp);

        var promise1 = helper.apex(cmp, 'v.customSettings', 'c.getASCustomSettings', {
            componentName: cmp.get('v.componentName')
        }, false);
        
		var promise2 = helper.apex(cmp, 'v.globalCustomSettings', 'c.getASCustomSettings', { componentName: cmp.get('v.globalCustomSettingComponentName') }, false);		

        Promise.all([promise1, promise2])
            .then(function (result) {                
                var buttonColumns = cmp.get('v.buttonColumns');
                var hiddenColumns = cmp.get('v.hiddenColumns');                
                helper.updateDatatableAndFooter(cmp);

				var tableTooltip;
				var globalCustomSettings = cmp.get('v.globalCustomSettings');
				if(!globalCustomSettings.Feature_Search_Service_Request) 
				{
					tableTooltip = cmp.get('v.customSettings.Service_INC_Search_EVT_Table_No_SR');
				} else {
					tableTooltip = cmp.get('v.customSettings.Service_INC_Search_EVT_Table_Tooltip');
				}
				
				cmp.set('v.tableTooltip', tableTooltip);
                cmp.set('v.initDone', true);
            })
            .catch(function (result) {
                if(cmp){
				var helper = result.sourceHelper;				
				helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");		
			}else
			{
				console.log(result);
			}
            });
    },

    refreshDatatable: function (cmp, event, helper) {
		helper.clearErrors(cmp);
        cmp.set('v.pageNumber', 1);
        helper.updateDatatableAndFooter(cmp);	
		helper.populateDictinctBPIArray(cmp);
    },

    navigateDatatablePage: function (cmp, event, helper) {
		helper.clearErrors(cmp);
        helper.updateDatatableAndFooter(cmp);		
		helper.populateDictinctBPIArray(cmp);
    },

    updateListColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
		//debugger;
        // Assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);

        helper.sortListData(cmp, fieldName, sortDirection);
    },

	updateColumnSorting: function (cmp, event, helper) {
		helper.updateColumnSortingWithSubProperty(cmp, event, helper);
    },
})