({
	populateDictinctBPIArray: function (cmp){		
		//Check distinct BPI to render the link conditionally		
		var currentList = cmp.get('v.currentList');
		currentList.map(function(item){
			var dictinctBPIArray = [];
			if(item.Incident_Account_Relationships__r != undefined)
			{
				item.Incident_Account_Relationships__r.map(function(childItem){
					var bpi = childItem.BPI_Id__c;
					if(bpi != undefined && dictinctBPIArray.indexOf(bpi) == -1 ){
						dictinctBPIArray.push(bpi);
					};
				});				
			}
			item.dictinctBPIArray = dictinctBPIArray;
		});

		cmp.set('v.currentList', currentList);
	},	
})