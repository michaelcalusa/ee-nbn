({
	handleRowAction: function (cmp, event, helper) {				
		var selectedOrder = { Id: event.getParam('row').Id };
		cmp.set('v.selectedOrder', selectedOrder );
		cmp.set('v.displayedDetailItem', 'DF_Order_Submitted_Summary');
	},

	onBpiIdClick: function (cmp, event, helper) {
		var selectedBpiId = event.target.getAttribute("data-Id");
		cmp.set('v.selectedBPIId', selectedBpiId);
		cmp.set('v.displayedDetailItem', 'DF_Active_Service_Search');		
		var activeServiceChildCmp = cmp.find('activeServiceChildCmp');
		activeServiceChildCmp.showServiceSummary(selectedBpiId);
	},
	
	init: function (cmp, event, helper) {
		var promise1 = helper.apex(cmp, 'v.customSettings', 'c.getASCustomSettings', { componentName: cmp.get('v.componentName') }, false);
		var promise2 = helper.apex(cmp, 'v.globalCustomSettings', 'c.getASCustomSettings', { componentName: cmp.get('v.globalCustomSettingComponentName') }, false);

		Promise.all([promise1, promise2])		
		.then(function(result) {
			var globalCustomSettings = cmp.get('v.globalCustomSettings');
			if(globalCustomSettings.Feature_Raise_Service_Request) cmp.set('v.isRaiseServiceRequestFeatureEnabled', true);
			cmp.set('v.initDone', true);
		})
		.catch(function(result) {
			if(cmp){
				var helper = result.sourceHelper;				
				helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");		
			}else
			{
				console.log(result);
			}
		});	
	},

	createServiceRequest: function (cmp, event, helper) {		
		var currentList = cmp.get("v.currentList");		
		cmp.set('v.selectedOrderId', currentList[0].df_OrderId);
		cmp.set('v.selectedBPIId', currentList[0].bpiId);
		cmp.set('v.displayedDetailItem', cmp.get('v.serviceRequestComponent'));
	},	

	createIncident: function (cmp, event, helper) {		
		var currentList = cmp.get("v.currentList");		
		cmp.set('v.selectedOrderId', currentList[0].df_OrderId);
		cmp.set('v.selectedBPIId', currentList[0].bpiId);
		cmp.set('v.displayedDetailItem', cmp.get('v.serviceIncidentComponent'));
	},	
})