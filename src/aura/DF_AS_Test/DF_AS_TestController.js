({
	back: function (cmp, event, helper) {
		cmp.set('v.activeSiblingCmp', 'DF_AS_Service_Incident_Search');
	},

	searchTermChange: function(cmp, event, helper) {		
		//Do search if enter key is pressed.
		if(event.getParams().keyCode == 13) {
			cmp.set('v.isTriggerdBySearch', true);
			helper.doSearch(cmp, event, helper);
		}
	},

	search: function(cmp, event, helper) {
		cmp.set('v.isTriggerdBySearch', true);
		helper.doSearch(cmp, event, helper);
	},
    
	clear: function(cmp, event, helper) {		
		cmp.set('v.isTriggerdBySearch', false);
		cmp.set('v.searchTerm', '');
		helper.doSearch(cmp, event, helper);		
	},	

	clickTestIdLink: function(cmp, event, helper) {				
		var selectedTestId = event.target.getAttribute("data-Id");
		var selectedOrderId = event.target.getAttribute("data-order-id");		
		cmp.set('v.selectedTestId', selectedTestId);
		cmp.set('v.selectedOrderId', selectedOrderId);
		
        cmp.set('v.displayedDetailItem', 'EE_AS_Test_Details');
		//cmp.set('v.displayedDetailItem', 'DF_AS_Service_Incident');		
    },

    init: function (cmp, event, helper) {
    	cmp.set('v.isTriggerdBySearch', false);
		helper.doSearch(cmp, event, helper);
    },

	refreshDatatable: function (cmp, event, helper) {
		cmp.set('v.pageNumber', 1);
        helper.updateDatatableAndFooterByParameters(cmp, 'v.fullList', 'v.currentList', 'v.pageNumber', 'v.selectedCount', 'v.maxPage');
	},

	navigateDatatablePage: function (cmp, event, helper) {		
		helper.updateDatatableAndFooterByParameters(cmp, 'v.fullList', 'v.currentList', 'v.pageNumber', 'v.selectedCount', 'v.maxPage');
	},

	updateColumnSorting: function (cmp, event, helper) {		
        helper.updateColumnSortingWithSubProperty(cmp, event, helper);
    },
})