({
	disableClearSearchButton: function(cmp, disabled){
		cmp.set('v.isClearSearchButtonDisabled', disabled);
	},

	doSearch: function (cmp, event, helper) {				
		this.apex(cmp, 'v.fullList', 'c.searchTestResults', { 
			searchFilter: cmp.get('v.searchTerm')			
		}, false)		
		.then(function(result) {
			if (cmp.get('v.isTriggerdBySearch')) helper.disableClearSearchButton(cmp, false);
			else helper.disableClearSearchButton(cmp, true);
		})
		.catch(function(result) {
			var error = result.error;			
			var cmp = result.sourceCmp;
			$A.reportError("error message", error);
			cmp.set("v.responseStatus", result.ERROR_RESPONSE_STATUS);
			cmp.set("v.responseMessage", $A.get("$Label.c.DF_Application_Error"));
			cmp.set("v.messageType", result.ERROR_RESPONSE_TYPE);
		});	
	},

})