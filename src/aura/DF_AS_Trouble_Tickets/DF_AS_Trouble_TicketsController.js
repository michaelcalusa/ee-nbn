({
	init: function (cmp, event, helper) {
		
	},

	handleTopBannerEvent: function (cmp, event, helper) {

		var message = event.getParam("message");
		var isSuccessMessage = event.getParam("isSuccessMessage");
		var isErrorMessage = event.getParam("isErrorMessage");
		var resetMessage = event.getParam("resetMessage");
		  
		if(isSuccessMessage)
			helper.showTopBannerSuccessMessage(cmp, message);

		if(isErrorMessage)
			helper.showTopBannerErrorMessage(cmp, message);

		if(resetMessage)
			helper.clearErrors(cmp);						
	},

	showLandingPage: function (cmp, event, helper) {
		cmp.set('v.activeChildCmp', 'DF_AS_Service_Incident_Search');
		var incidentSearchCmp = cmp.find('incidentSearchCmp');
		if (incidentSearchCmp) {
			incidentSearchCmp.set('v.productTableDisplayedDetailItem', '');
			incidentSearchCmp.set('v.eventTableDisplayedDetailItem', '');
		}
	},
	
})