({
	init: function (cmp, event, helper) {       
		//helper action here
		console.log('init here');
		var bpiId = cmp.get('v.BPIId');
		if(bpiId != undefined || bpiId != null) {
		    helper.loadModifyOptions(cmp);
        }
         
    },
    
    backToSource: function(cmp, event, helper) {	
		var sourceDisplayedDetailItem = cmp.get('v.sourceDisplayedDetailItem');
		cmp.set('v.displayedDetailItem', sourceDisplayedDetailItem);
	},

	showServiceSummary: function (cmp, event, helper) {
		cmp.set('v.showSearchBox', false);
		cmp.set('v.showModifyButton', false);		
		var params = event.getParam('arguments');
        if (params) {
            cmp.set('v.searchTerm', params.searchTerm);
        }
		helper.loadProductSummary(cmp);     
    },	

    search: function (cmp, event, helper) {
		helper.loadProductSummary(cmp);     
    },
    
    searchTermChange: function (cmp, event, helper) {
        //Do search if enter key is pressed.
        if (event.getParams().keyCode == 13) {
			helper.loadProductSummary(cmp);            
        }
    },
    
    modify: function (cmp, event, helper) {
		var bpiId = cmp.get('v.BPIId');
		console.log('bpiId: ' + bpiId);
        var modifyType = event.getParam("value");
		helper.validateBpiOnModify(cmp, bpiId, modifyType);       
    }
})