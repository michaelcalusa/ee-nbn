({
	loadProductSummary: function (cmp) {
        
        var isValidInput = false;
        var searchTermVal = cmp.get('v.searchTerm');
        isValidInput = this.validateSearchInput(cmp, searchTermVal.trim());
        
        console.log('isValidInput::: ' + isValidInput);
        
        if(isValidInput) {
        	
        	var action = cmp.get('c.searchServiceCache');
            
            action.setParams({
                "searchTerm": searchTermVal.trim()
            });
            
        	action.setCallback(this, function(result){
                var state = result.getState(); // get the response state
                cmp.set('v.svcCacheSearch', undefined); //reset for new search
                console.log('loadProductSummary:state::: ' + state);
                if(state == 'SUCCESS' && result.getReturnValue() != null) {
                    cmp.set('v.svcCacheSearch', result.getReturnValue());
                    //get service cache status and set message display if error found
                    if(((cmp.get('v.svcCacheSearch.status') === 'NOT_FOUND' || cmp.get('v.svcCacheSearch.status') === undefined)
                       		&& cmp.get('v.svcCacheSearch.ovcs') === undefined || cmp.get('v.svcCacheSearch.ovcs') === '')
                    		||	cmp.get('v.svcCacheSearch.status') ==='404'){
                    	this.setMessageDisplay(cmp);
                    	//cmp.set("v.Message", true);
                    	//this.clearErrors(cmp);
                    } else {
                        this.setBPIId(cmp);
                    	this.loadOVCs(cmp, searchTermVal);
                        this.setModifyButton(cmp);
                    	//cmp.set("v.Message", false);
    	                this.clearErrors(cmp);
                    }
                } else {
    				var errors = result.getError();
    				if (errors) {
                        if (errors[0] && errors[0].message) {
                        	console.log(this.ERR_MSG_ERROR_OCCURRED + errors[0].message);
                        }
                    } else {
                    	console.log(this.ERR_MSG_UNKNOWN_ERROR);                 
                    }
    				var errorLabel = $A.get("$Label.c.DF_Application_Error");
    				this.setErrorMsg(cmp, "ERROR", errorLabel, "Banner");		
    				//cmp.set('v.Message', false);
                }
                cmp.set('v.isTriggeredBySearch', true);
            });
        	$A.enqueueAction(action);
        	this.loadModifyOptions(cmp);
        }
    },

    loadModifyOptions: function(cmp) {
        var modactions=cmp.get('c.getModifyActionList');
        var self = this;
        modactions.setCallback(this, function(actionResult) {
            this.setModifyButton(cmp);
          console.log('actionResult.getReturnValue():: ' + actionResult.getReturnValue());

          var modifyList = actionResult.getReturnValue();
          modifyList.forEach(function(element) { element.disabled = false; });
          modifyList.sort(function(a, b){
            var x = a.MasterLabel.toLowerCase();
            var y = b.MasterLabel.toLowerCase();
            if (x < y) {return -1;}
            if (x > y) {return 1;}
            return 0;
        });

          cmp.set("v.modifyList", modifyList);
          var OVCList = cmp.get("v.ovcList");
            if(OVCList!=null){
                modifyList.forEach(function(element) { element.disabled = false; });
                this.removeModifyOptionList(cmp);
                this.disableAddOVCLink(cmp);
                this.disableChangeUniVLanIdLink(cmp);
            }

        });

        $A.enqueueAction(modactions);
    },
    
    setMessageDisplay: function (cmp) {
    	var SVC_NOT_FOUND = $A.get("$Label.c.DF_Product_or_Service_not_found");//'Product or Service not found in Service Cache.';
        var SVC_NOT_AVAILABLE ='Service Cache not available';
    	if(cmp.get('v.svcCacheSearch.status') == 'NOT_FOUND') {
    		//cmp.set("v.messageDisplay", SVC_NOT_FOUND);
    		this.setErrorMsg(cmp, "ERROR", SVC_NOT_FOUND, "Banner");
    	} else if (cmp.get('v.svcCacheSearch.status') == '404') 
        {
    		//cmp.set("v.messageDisplay", SVC_NOT_AVAILABLE);
    		this.setErrorMsg(cmp, "ERROR", SVC_NOT_AVAILABLE, "Banner");
    	}
        else 
        {
    		//cmp.set("v.messageDisplay", SVC_NOT_AVAILABLE);
    		this.setErrorMsg(cmp, "ERROR", SVC_NOT_AVAILABLE, "Banner");
    	}
    },
    
    loadOVCs: function (cmp, searchTermVal) {
    	console.log('loadProductSummary:resultv.svcCacheSearch.ovcs::: ' + cmp.get('v.svcCacheSearch.ovcs'));
        cmp.set('v.ovcList', cmp.get('v.svcCacheSearch.ovcs'));
        var ovcList = cmp.get('v.svcCacheSearch.ovcs');
        var cnt = 0;
        var searchByOVC = searchTermVal.startsWith("OVC");
        if(searchByOVC) {
        	for(var i=0;i <= ovcList.length;i++) {
            	if(searchTermVal == ovcList[i].ovcId) {
            		cnt = i;
            		break;
            	}
            }
        }
        console.log('loadProductSummary:cnt ' + cnt);
        cmp.set('v.selectedOvc', cmp.get('v.svcCacheSearch.ovcs')[cnt]);
    },
    
    setBPIId:function(cmp){
        cmp.set("v.BPIId",cmp.get("v.svcCacheSearch.bpiId"));
        console.log(':::v.BPIId:: ' + cmp.get('v.BPIId'));
	},
    setModifyButton:function(cmp)
    {
        /*var action = cmp.get("c.isModifyAllowed");
        action.setCallback(this,function(result)
        {
          var state = result.getState();
		    if (state == "SUCCESS") {
	                var results = result.getReturnValue();
	                if (results == true) {*/
                    			cmp.set("v.showModifyButton",true);       
                           /*}
            }
    });
	        $A.enqueueAction(action);*/
    },
	
	validateBpiOnModify:function(cmp, bpiID, modifyType) {
		var MODIFY_IN_PROCESS = $A.get("$Label.c.DF_Modify_Is_In_Process");
		var errorLabel = $A.get("$Label.c.DF_Application_Error");
		if(bpiID != null) {
            if(modifyType == 'Add_new_OVC') {
                if(cmp.get("v.svcCacheSearch.ovcType").ovcType == 'Access EPL') {
                    this.displayPopUpMessage(cmp, bpiID, 'You cannot Add OVC with OVC Type Access EPL', 'Error');
                    return;
                }
                else if(cmp.get("v.ovcList").length > 7) {
                    this.displayPopUpMessage(cmp, bpiID, 'Maximum number of OVCs already added to the product.', 'Error');
                    return;
                }
            }
            var action = cmp.get("c.isBPIOnModify");
			action.setParams({
	        	"bpiID": bpiID
	    	});
			//debugger;
			action.setCallback(this, function(result){
	        	console.log('result:: ');
		        var state = result.getState();
		        console.log('state:: ' + state);
	            if (state == "SUCCESS") {
	                var results = result.getReturnValue();
	                console.log('results:: ' + results);
	                if (results == true) {
	                	this.displayPopUpMessage(cmp, bpiID, MODIFY_IN_PROCESS, 'Error');
	                	console.log('disable modify button or display error message');
	                } else {
	                	this.modifyOrder(cmp, modifyType); 
	                	console.log('go to summary detail page');
	                }
	            } else {
	            	var errors = result.getError();
    				if (errors) {
                        if (errors[0] && errors[0].message) {
                        	console.log(this.ERR_MSG_ERROR_OCCURRED + errors[0].message);
                        }
                    } else {
                    	console.log(this.ERR_MSG_UNKNOWN_ERROR);                 
                    }
    				this.displayPopUpMessage(cmp, bpiID, errorLabel, 'Error');
	            }
            });
	        $A.enqueueAction(action);
		}
	},
	
	displayPopUpMessage: function(cmp, bpiId, errorMsg, errorType) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
		    title: errorType + "!",
		    message: errorMsg + ": " + bpiId,
		    type: errorType
		});
		toastEvent.fire();
	},
    
    validateSearchInput: function (cmp, searchTermVal) {
    	console.log('validateSearchInput::: ');
    	var isValid = true;
    	console.log('validateSearchInput:searchTermVal::: ' + searchTermVal);
    	var searchPattern = /\b[BPI/g0-9]{15}$|\b[UNI/g0-9]{15}$|\b[OVC/g0-9]{15}$/;
    	var errorLabel = 'Search ids not inputted in the right format. Eg. BPI000000012345, UNI900000001234, OVC000000012345';
    	
    	if(searchTermVal != undefined && searchTermVal != '') {
    		if(!searchTermVal.match(searchPattern)) {
    			this.setErrorMsg(cmp, "ERROR", errorLabel, "Banner");
    			//cmp.set('v.Message', false);
    			isValid = false;
    		}
    	} else {
    		isValid = false;
    		cmp.set('v.isTriggeredBySearch', false);
    		this.clearErrors(cmp);
    	}
    	return isValid;
    },
    
    clearErrors: function (cmp) {
		cmp.set('v.responseStatus', '');
		cmp.set('v.responseMessage', '');
		cmp.set('v.messageType', '');
	},
    
    modifyOrder: function (cmp, modifyType) {
        /*cmp.set('v.showSearchBox', false);
		cmp.set('v.showModifyButton', false);
        cmp.set('v.isTriggeredBySearch', false);
		cmp.set('v.showModifyOrder', true);*/
        var bpiId = cmp.get("v.BPIId");
        //var homeDtEvt = cmp.getEvent("homePageEvent"); 
        //homeDtEvt.setParams({"requestType" : "modifyOrder", "BPIId":bpiId });
        //homeDtEvt.fire();
        var svcList = cmp.get("v.svcCacheSearch");
        var ovcListing = cmp.get("v.ovcList");
        var selectOVC = cmp.get("v.selectedOvc");
        var searchTerm = cmp.get("v.searchTerm");
        
		var modifyOrderEvt = cmp.getEvent("modifyOrderPageEvent");
		var typeOfModify = modifyType;
        modifyOrderEvt.setParams({"BPIId":bpiId, "ModifyType":typeOfModify});

		modifyOrderEvt.fire();
        
	},
	
    // Sets error msg properties
    setErrorMsg: function(cmp, errorStatus, errorMessage, errorType) {        	
        // Set error response comp attribute values 
        cmp.set("v.responseStatus", errorStatus);	
        cmp.set("v.responseMessage", errorMessage);	
        cmp.set("v.messageType", errorType);  
    },

    //Disable Add Add OVC Link if Access Type = Access EPL or 8 OVC's already exist
    disableAddOVCLink: function(cmp) {
    	var OVCList = cmp.get("v.ovcList");
		var modifyList = cmp.get("v.modifyList");
        //reset disabled status List for new search
       
        
        if(OVCList != null) {
            var ovcType = cmp.get("v.svcCacheSearch").ovcType;
            if(OVCList.length>7 || ovcType == "Access EPL"){
                modifyList.forEach(function(element) { 
                    if(element.DeveloperName == "Add_new_OVC"){
                        element.disabled = true;  
                    }	
                   });
            }
    	}
        cmp.set("v.modifyList",modifyList);

   },
    
    //Disable Change UNI VLAN ID Link if Access Type = Access EPL 
    //CPST-3871
    disableChangeUniVLanIdLink: function(cmp) {
    	var OVCList = cmp.get("v.ovcList");
		var modifyList = cmp.get("v.modifyList");
        //reset disabled status List for new search
        
        
        if(OVCList != null) {
            var ovcType = cmp.get("v.svcCacheSearch").ovcType;
            if( ovcType == "Access EPL"){
                modifyList.forEach(function(element) { 
                    if(element.DeveloperName == "Change_UNI_VLAN_ID"){
                        element.disabled = true;  
                    }	
                   });
            }
    	}
        cmp.set("v.modifyList",modifyList);

   },

   removeModifyOptionList: function(cmp) {
       var OVCList = cmp.get("v.ovcList");
   	   var modifyList = cmp.get("v.modifyList");
       var optionToRemove = [];
       if(OVCList != null) {
           for(var i=0 ; i<modifyList.length; i++) {
               if(modifyList[i].Toggle_Modify_Option__c==false)
                   optionToRemove.push(modifyList[i]);
           }
       }
       var updateModifyList = modifyList.filter(function(element) {
          return optionToRemove.indexOf(element) === -1;
        });
       cmp.set("v.modifyList",updateModifyList);

   }
})