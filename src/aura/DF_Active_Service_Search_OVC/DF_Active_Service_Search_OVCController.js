({
	clickOvc: function(cmp, event, helper) {	

		var ovcId = event.target.getAttribute("data-id");
		var selectedOvc = cmp.get('v.ovcList').filter(function(item){ return item.ovcId == ovcId })[0];
		cmp.set('v.selectedOvc', selectedOvc);
		
	},	
	
	init: function (cmp, event, helper) {
		cmp.set('v.initDone', true);
	}
	
})