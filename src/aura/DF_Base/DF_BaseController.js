({
	init: function (cmp, event, helper) {

	},

	showSpinner: function(cmp, event, helper) {
		helper.showSpinner(cmp);
	},
      
	hideSpinner: function(cmp, event, helper) {
		helper.hideSpinner(cmp);
    },
})