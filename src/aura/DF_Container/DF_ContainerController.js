({
	init: function(cmp, event, helper) {				
		var displayMode = cmp.get(" v.displayMode");		
		 
		if (displayMode == 'Service Feasibility')
		{
			$A.createComponent(
				"c:DF_ServiceFeasibility", {
					
				},
				function (newCmp) {
					if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
					}
				}
			);
		}

		if (displayMode == 'Orders')
		{
			$A.createComponent(
				"c:DF_Order_Submitted_Search", {
					
				},
				function (newCmp) {
					if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
					}
				}
			);
		}		
	},
	
	scriptsLoaded: function(cmp, event, helper) {		
	},	
})