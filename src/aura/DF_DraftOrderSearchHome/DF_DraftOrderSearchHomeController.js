({
	searchTermChange: function (cmp, event, helper) {
        //Do search if enter key is pressed.
        if (event.getParams().keyCode == 13) {
            cmp.set('v.isTriggerdBySearch', true);
			helper.getOrderRecords(cmp);            
        }
    },

    search: function (cmp, event, helper) {
        cmp.set('v.isTriggerdBySearch', true);		
		helper.getOrderRecords(cmp);        
    },

    clear: function (cmp, event, helper) {
        helper.clearErrors(cmp);
        cmp.set('v.isTriggerdBySearch', false);
        cmp.set('v.searchTerm', '');
		helper.getOrderRecords(cmp);        
        helper.toogleClearSearchButton(cmp, true);
        helper.disableClearSearchButton(cmp, true);
    },

    clickOrderiLink: function (cmp, event, helper) {
		var id = event.target.getAttribute("data-id");
        console.log('id ' +id); //added
        var quoteId = id.split(':')[0];
        var oppBunId = id.split(':')[1];
        var status = id.split(':')[3];
        var isPricing = id.split(':')[2];
        console.log('quoteId ' +quoteId + ' oppBunId'+oppBunId + ' status '+status+ ' isPricing '+isPricing);        
        
        //helper.getOrderSummary(cmp, event, oppBunId, quoteId);
        //helper.navigateToQuote(cmp, event, quoteId);
        //helper.createDF_Order(cmp, event, quoteId);
        
        //var a = helper.apex(cmp, "v.pageUrlSFOrderSummary", 'c.getCustomSetting', { settingName : helper.CUSTOM_SETTING_PAGE_URL_SF }, false);
        if(isPricing == 'false' && status == undefined) {
            //var url = "servicefeasibility?page=ordersummary&parentOppId="+ oppBunId;		
			//window.open(url, '_top');
			var siteDtEvt = cmp.getEvent("siteDetailPageEvent"); 
                siteDtEvt.setParams({"oppBundleId" : oppBunId });
                siteDtEvt.fire(); 
        }
        else if(isPricing == 'true' && status == undefined) {
            var url = "servicefeasibility?page=ordersummary&parentOppId="+ oppBunId;		
			window.open(url, '_top');
			
        }
        else if(isPricing == 'true' && status == 'Quick Quote') {
            //quote
            helper.getOrderSummary(cmp, oppBunId, quoteId);
            cmp.set("v.bundleId",oppBunId);
			//helper.navigateToQuote(cmp);
			
        }
        else if(isPricing == 'true' && status == 'Order Draft') {
            helper.getOrderSummary(cmp, oppBunId, '');        	
        	helper.createDF_Order(cmp, event, quoteId);
			//var opEvt = cmp.getEvent("oppBundleEvent");   
            //        opEvt.setParams({"oppBundleId" : bunId });
              //      opEvt.fire(); 

        }        
    },
    
	
    init: function (cmp, event, helper) {

		//Check if url parameter exists  
		
		var orderIdParam = helper.getUrlParameter(cmp, helper.ORDER_ID_PARAM_NAME);
		var parentOppIdParam = helper.getUrlParameter(cmp, helper.PARENT_OPP_ID_PARAM_NAME);
        
        if (orderIdParam != null && parentOppIdParam != null) {
			cmp.set('v.orderIdParam', orderIdParam);
			cmp.set('v.parentOppIdParam', parentOppIdParam);

			helper.apex(cmp, "v.pageUrlSFOrderSummary", 'c.getCustomSetting', { settingName : helper.CUSTOM_SETTING_PAGE_URL_SF }, false)
			.then(function(result) {
				var backUrl = cmp.get('v.pageUrlSFOrderSummary') + '?' + helper.PARENT_OPP_ID_PARAM_NAME + '=' + parentOppIdParam + '&page=ordersummary'
				cmp.set('v.backUrl', backUrl);
				cmp.set('v.isOrderDetailsOpen', true);
				cmp.set('v.isInitDone', true); 
			})
        } 
		else {
            helper.getOrderRecords(cmp);
			cmp.set('v.isInitDone', true); 
		}
    },
    
    renderPage: function (cmp, event, helper) {
        helper.renderPage(cmp);
    },

   updateColumnSorting: function (cmp, event, helper) {
		helper.updateColumnSortingWithSubProperty(cmp, event, helper);
    },
})