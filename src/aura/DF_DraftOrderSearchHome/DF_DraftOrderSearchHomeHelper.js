({
    ERR_MSG_ERROR_OCCURRED: 'Error occurred. Error message: ',
    ERR_MSG_UNKNOWN_ERROR: 'Error message: Unknown error',
    MAX_COUNT: '5000',
	
	getOrderRecords : function(cmp) {		
		var isTriggerBySearch = cmp.get('v.isTriggerdBySearch');
		var searchTermVal = cmp.get('v.searchTerm');
        
		if (isTriggerBySearch) this.toogleClearSearchButton(cmp, false);
			
        var action = cmp.get("c.getRecords");
		
		action.setParams({"searchTerm": searchTermVal, "isTriggerBySearch": Boolean(isTriggerBySearch)});
		action.setCallback(this, function(response) {
        
		var state = response.getState();
		console.log('response state :  ' + state);
        	if(state === "SUCCESS") {
				this.clearErrors(cmp);

                var records = JSON.parse(response.getReturnValue());
                //console.log('record values :  ' + response.getReturnValue());
				cmp.set('v.fullList', records);
				cmp.set('v.currentList', records);				
				cmp.set("v.pageNumber", 1);
                var pageCountVal = cmp.get("v.selectedCount");
				cmp.set("v.maxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
				this.renderPage(cmp);
	        }
            else{
				var errors = response.getError();
				if (errors) {
                    if (errors[0] && errors[0].message) {
                    	console.log(this.ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                	console.log(this.ERR_MSG_UNKNOWN_ERROR);                 
                }

				var errorLabel = $A.get("$Label.c.DF_Application_Error");

				cmp.set("v.responseStatus", state);
                cmp.set("v.messageType","Banner");				
                cmp.set("v.responseMessage", errorLabel);				
            }
        });
        $A.enqueueAction(action);
	},    

    renderPage: function (cmp) {

        var records = cmp.get("v.fullList"),

            pageNumber = cmp.get("v.pageNumber"),
            pageCountVal = cmp.get("v.selectedCount"),
            pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));

        cmp.set('v.currentList', []);
        cmp.set("v.currentList", pageRecords);
    },        

    toogleClearSearchButton: function (cmp, disabled) {
        cmp.set('v.isClearSearchButtonDisabled', disabled);
    },

    getOrderSummary: function (cmp, bunId, quoteId) {
		var action = cmp.get('c.getOrderSummaryData');      
        action.setParams({
            "bundleId" : bunId
        });
        action.setCallback(this, function(callbackResult){
            var state = callbackResult.getState(); // get the response state
            	
            if(state == 'SUCCESS' && callbackResult.getReturnValue() != null) {
                var returnValue = JSON.parse(callbackResult.getReturnValue());
                cmp.set('v.fullList', returnValue);
                //this.navigateToQuote(cmp, event, quoteId);
                //setting the opp bundle id in oppBundleEvent
                if(quoteId == '') {
                    var opEvt = cmp.getEvent("oppBundleEvent");   
                    opEvt.setParams({"oppBundleId" : bunId });
                    opEvt.fire(); 
                }
                else {
                    var fullList = cmp.get("v.fullList");
                    console.log('fullList :  ' + fullList);
                    var results = fullList.filter(function (item) {
                        return item.id === quoteId;
                    });
                    debugger;
                    var quoteDtEvt = cmp.getEvent("quoteItemPageEvent"); 
                    quoteDtEvt.setParams({"selectedQuote" : results[0], "parentOppId":quoteId});
                    quoteDtEvt.fire();    
                    
                }
            }
        });
        $A.enqueueAction(action); 
    },
    
    navigateToQuote : function(cmp, event, quoteId) {
        console.log('navigate ');
        var fullList = cmp.get("v.fullList");
        console.log('fullList :  ' + fullList);
        /*var results = fullList.filter(function (item) {
            return item.id === quoteId;
        });
        console.log('results '+results);
        var evt = $A.get("e.force:navigateToComponent");
        debugger;
        evt.setParams({
            componentDef : "c:DF_SF_Quote_Item",
            componentAttributes: {
                location : results[0]
            }
        });
        evt.fire();*/
    },
    
    createDF_Order: function (cmp, event, id) {
        console.log('=======DF_SF_QuoteHelper==== createDF_Order START');
        var location = cmp.get("v.location");
        // var dfQuoteId = location.Id;
        console.log('=======DF_SF_QuoteHelper==== createDF_Order quoteId' + id);
        var action = cmp.get("c.createDF_Order");
        action.setParams({
            dfQuoteId: id
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('=======DF_SF_QuoteHelper==== createDF_Order state ' + state);
            if (state === "SUCCESS") {
                var resp = response.getReturnValue();
                console.log('response :  ' + resp);
                if (resp != "ERROR") {

                    var fullList = cmp.get('v.fullList');
					console.log('fullList :  ' + fullList);
                    var results = fullList.filter(function (item) {
                        return item.id === id;
                    });
                    //cmp.set("v.selectedLocation", results[0]); //added
                    var orderDtEvt = cmp.getEvent("orderDetailPageEvent");
                    //console.log('clickOrderLink' + JSON.stringify(results[0]));//added
                    results[0].OrderId = resp;
                    results[0].orderId = resp;
                    cmp.set('v.location',results[0]);
                    console.log('response :  ' + results[0]);
                    console.log('=======DF_SF_QuoteHelper==== createDF_Order results ' + JSON.stringify(results[0]));
                    orderDtEvt.setParam("location", results[0]);
                    orderDtEvt.fire();
                    
                    //setting opportunity bundle id with event
                    //var opEvt = cmp.getEvent("oppBundleEvent");   
                    //opEvt.setParams({"oppBundleId" : bunId });
                    //opEvt.fire(); 
                }
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },
})