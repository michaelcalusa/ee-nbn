/**
 * Created by philipstafford-jones on 25/1/19.
 */
({
    init: function(cmp, event, helper) {

        const featureName     = cmp.get('v.name');
        console.log("Fetch Feature Toggle: " + featureName);
        //init feature toggle & placeholder text
        helper.apex(cmp, 'v.dummy', 'c.isFeatureToggleEnabled', { featureName: featureName }, false)
            .then(function(result) {
                if (result.data) {
                    cmp.set("v.value", true);
                    console.log('feature toggle ' + featureName + ': true');
                } else {
                    cmp.set("v.value", false);
                    console.log('feature toggle ' + featureName + ': false');
                }
            }).catch(function() {
                console.log('feature toggle ' + featureName + ': got error set to false');
                cmp.set("v.value", false);
        });
        return true;
        },


});