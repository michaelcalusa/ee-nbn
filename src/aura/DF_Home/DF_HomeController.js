({
    init: function (cmp, event, helper) {
        // Set timeout value for hidding spinner to resolve the aura rendering issue that causes sometimes button click event not firing 
		cmp.set('v.spinnerTimeout', 1000);

        // Get custom label values
		var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");   	
        cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR); 	
        
        var action = cmp.get("c.getUserDetails");
        var loggedInDetails;
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                loggedInDetails = response.getReturnValue();
                if(loggedInDetails === "business"){
                    cmp.set("v.isBusiness", true);
                    cmp.set("v.isBusinessPlus", false);
                    cmp.set("v.isAssurance", false);
                }
                else if(loggedInDetails === "businessPlus"){
                    cmp.set("v.isBusinessPlus", true);
                    cmp.set("v.isBusiness", false);
                    cmp.set("v.isAssurance", false);
                }
                else if(loggedInDetails === "assurance"){
                    cmp.set("v.isAssurance", true);
                    cmp.set("v.isBusiness", false);
                    cmp.set("v.isBusinessPlus", false);
                }
                cmp.set("v.isServiceFeasibility", true);
            }
            else if(state === "ERROR") {
                cmp.set("v.responseStatus",state);
                cmp.set("v.type","Banner");
                cmp.set("v.responseMessage",errorLabel);
            }
        })
        $A.enqueueAction(action);
    },
    handleHomeEvent: function (cmp, event, helper) {
        var reqType = event.getParam("requestType");
        
        if(reqType === "feasibility"){
            cmp.set("v.loadServiceFeasibility", true);
            cmp.set("v.loadOrders", false);
            cmp.set("v.loadTickets", false);
            cmp.set("v.loadHome", false);
            cmp.set("v.batchId", null);  
            cmp.set("v.lstSitePage", false);  
            cmp.set("v.sfPage", false);  
            cmp.set("v.ragPage", false); 
            cmp.set("v.quotePage", false);    
            cmp.set("v.orderPage", false);
            cmp.set('v.isOrderDetailsOpen', false);
			cmp.set('v.isQuoteDetailsOpen', false);  
            cmp.set("v.loadDraftOrders", false);
            cmp.set("v.loadServices", false);
            cmp.set("v.loadModifyOrder", false);
        }
        else if(reqType === "order"){
            cmp.set("v.loadServiceFeasibility", false);
            cmp.set("v.loadOrders", true);
            cmp.set("v.loadTickets", false);
            cmp.set("v.loadHome", false);
            cmp.set("v.batchId", null);  
            cmp.set("v.lstSitePage", false);  
            cmp.set("v.sfPage", false);  
            cmp.set("v.ragPage", false); 
            cmp.set("v.quotePage", false);    
            cmp.set("v.orderPage", false);
            cmp.set('v.isOrderDetailsOpen', false);
			cmp.set('v.isQuoteDetailsOpen', false);  
            cmp.set("v.loadDraftOrders", false);
            cmp.set("v.loadServices", false);
            cmp.set("v.loadModifyOrder", false);
        }
        else if(reqType === "assurance"){
            cmp.set("v.loadServiceFeasibility", false);
            cmp.set("v.loadOrders", false);
            cmp.set("v.loadTickets", true);
            cmp.set("v.loadHome", false);
            cmp.set("v.batchId", null);  
            cmp.set("v.lstSitePage", false);  
            cmp.set("v.sfPage", false);  
            cmp.set("v.ragPage", false); 
            cmp.set("v.quotePage", false);    
            cmp.set("v.orderPage", false);
            cmp.set('v.isOrderDetailsOpen', false);
			cmp.set('v.isQuoteDetailsOpen', false);  
            cmp.set("v.loadDraftOrders", false);
            cmp.set("v.loadServices", false);
            cmp.set("v.loadModifyOrder", false);
        } else if(reqType === "services"){
            cmp.set("v.loadServiceFeasibility", false);
            cmp.set("v.loadOrders", false);
            cmp.set("v.loadTickets", false);
            cmp.set("v.loadHome", false);
            cmp.set("v.batchId", null);  
            cmp.set("v.lstSitePage", false);  
            cmp.set("v.sfPage", false);  
            cmp.set("v.ragPage", false); 
            cmp.set("v.quotePage", false);    
            cmp.set("v.orderPage", false);
            cmp.set('v.isOrderDetailsOpen', false);
			cmp.set('v.isQuoteDetailsOpen', false);  
            cmp.set("v.loadDraftOrders", false);
            cmp.set("v.loadServices", true);
            cmp.set("v.loadModifyOrder", false);
            cmp.set("v.BPIId",null);
	        cmp.set("v.svcCacheSearch",null);
	        cmp.set("v.ovcList",null);
	        cmp.set("v.selectedOvc",null);
	        cmp.set("v.isTriggeredBySearch",false);
        } else if (reqType === "quote") {
            helper.clearPages(cmp);
            cmp.set("v.quotePage", true);
        } else {
            cmp.set("v.loadHome", true);
            cmp.set("v.loadServiceFeasibility", false);
            cmp.set("v.loadOrders", false);
            cmp.set("v.loadTickets", false);
            cmp.set("v.batchId", null);  
            cmp.set("v.lstSitePage", false);  
            cmp.set("v.sfPage", false);  
            cmp.set("v.ragPage", false); 
            cmp.set("v.quotePage", false);    
            cmp.set("v.orderPage", false);
            cmp.set('v.isOrderDetailsOpen', false);
			cmp.set('v.isQuoteDetailsOpen', false);  
			cmp.set("v.loadServices", false);
            cmp.set("v.loadModifyOrder", false);
        } 	        
		var childCmp = cmp.find('callOrderDetailsOpen');
        if (childCmp) childCmp.toggleOrderSubmittedSearch(cmp.get('v.isOrderDetailsOpen')); 

		var troubleTicketsCmp = cmp.find('troubleTicketsCmp');
		if (troubleTicketsCmp) troubleTicketsCmp.showLandingPage();
    },
    updateOppBundleId : function(cmp, event, helper) {
        var opptBundleId = event.getParam("oppBundleId");
        cmp.set("v.batchId", opptBundleId);  
        cmp.set("v.lstSitePage", true);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
        cmp.set("v.quotePage", false);    
        cmp.set("v.orderPage", false); 
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", false);
        cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);  
		cmp.set("v.loadServices", false);
        cmp.set("v.loadModifyOrder", false);
    },
    
    goToRAGPage :function(cmp, event, helper) {
        var opptBundleId = event.getParam("oppBundleId");
        console.log('ragPage opptBundleId : ' + opptBundleId);
        if(opptBundleId!=null)
            cmp.set("v.batchId", opptBundleId);
        
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", true); 
        cmp.set("v.quotePage", false);    
        cmp.set("v.orderPage", false);
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", false);
        cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);  
		cmp.set("v.loadServices", false);
        cmp.set("v.loadModifyOrder", false);
    },
    
    goToQuotePage :function(cmp, event, helper) {
		
		cmp.set('v.isFromQuotePage', event.getParam("isFromQuotePage")); 
		var batchId = cmp.get("v.batchId");
		if($A.util.isEmpty(batchId) || $A.util.isUndefined(batchId)) cmp.set('v.batchId', event.getParam("batchId")); 
		cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);  		
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
        cmp.set("v.quotePage", true); 
        cmp.set("v.orderPage", false);
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", false);
        cmp.set("v.loadServices", false);
        cmp.set("v.loadModifyOrder", false);
    },
    
    goToOrderPage :function(cmp, event, helper) {
        var location = event.getParam("location");
        cmp.set("v.location", location); 
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
        cmp.set("v.quotePage", false); 
        cmp.set("v.orderPage", true);
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", false);
        cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);  
		cmp.set("v.loadServices", false);
        cmp.set("v.loadModifyOrder", false);
    },
    
    handleOrderEvent: function (cmp, event, helper) {
        //debugger;
        var selOrder = event.getParam("selectedOrder");
		var isFromQuotePage = event.getParam("isFromQuotePage");
		var showConfirmationModify = event.getParam("showConfirmationModify");
        cmp.set('v.selectedOrder', selOrder);
		cmp.set('v.isFromQuotePage', isFromQuotePage);
		cmp.set('v.showConfirmationModify', showConfirmationModify);

		if(isFromQuotePage) cmp.set('v.batchId', event.getParam("batchId"));		

		//var fullList = cmp.get('v.fullList');
        //console.log(fullList);
        //var orderId = event.target.getAttribute("data-Id");
        //var selectedOrder = fullList.filter(function(item){ return item.Id == orderId});		
        cmp.set('v.isOrderDetailsOpen', true);
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", false);
        cmp.set("v.batchId", null);  
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
        cmp.set("v.quotePage", false);    
        cmp.set("v.orderPage", false);
        cmp.set("v.loadServices", false);
        cmp.set("v.loadModifyOrder", false);
        var childCmp = cmp.find('orderSubmittedSearchComp');
        if (childCmp) childCmp.orderSubmittedSearchComp(cmp.get('v.showConfirmationModify'));
    },
    handleQuoteDetailEvent: function (cmp, event, helper) {
        var selQuote = event.getParam("selectedQuote");
        console.log('selectedquote'+selQuote);        
        cmp.set('v.selectedLocation', selQuote);		
        cmp.set('v.isOrderDetailsOpen', false);
        cmp.set('v.isQuoteDetailsOpen', true);        
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", false);
        cmp.set("v.batchId", null);  
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
        cmp.set("v.quotePage", false);    
        cmp.set("v.orderPage", false);
        cmp.set("v.loadServices", false);
        cmp.set("v.loadModifyOrder", false);
    },
    clickOrderLink: function (cmp, event, helper) {
        cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);  
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", true);
        cmp.set("v.batchId", null);  
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
        cmp.set("v.quotePage", false);    
        cmp.set("v.orderPage", false);
        cmp.set("v.isOrders", true);
        cmp.set("v.isServiceFeasibility", false);    
        cmp.set("v.isTickets", false);
        cmp.set("v.isDraftOrders", false);
        cmp.set("v.loadServices", false);
        cmp.set("v.loadModifyOrder", false);
    },
    clickFeasibilityLink: function (cmp, event, helper) {
        cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);  
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", true);
        cmp.set("v.batchId", null);  
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
        cmp.set("v.quotePage", false);    
        cmp.set("v.orderPage", false);
        cmp.set("v.isOrders", false);
        cmp.set("v.isServiceFeasibility", true);    
        cmp.set("v.isTickets", false);
        cmp.set("v.isDraftOrders", false);
        cmp.set("v.loadServices", false);
        cmp.set("v.loadModifyOrder", false);
    },
    clickAssuranceLink: function (cmp, event, helper) {
        cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);  
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", true);
        cmp.set("v.loadHome", false);
        cmp.set("v.batchId", null);  
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
        cmp.set("v.quotePage", false);    
        cmp.set("v.orderPage", false);
        cmp.set("v.isOrders", false);
        cmp.set("v.isServiceFeasibility", false);            
        cmp.set("v.isDraftOrders", false);
        cmp.set("v.loadServices", false);
        cmp.set("v.loadModifyOrder", false);		
    },
    clickDraftOrderLink: function (cmp, event, helper) {
        cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);  
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", true);
        cmp.set("v.batchId", null);  
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
        cmp.set("v.quotePage", false);    
        cmp.set("v.orderPage", false);
        cmp.set("v.isOrders", false);
        cmp.set("v.isServiceFeasibility", false);    
        cmp.set("v.isTickets", false);
        cmp.set("v.isDraftOrders", true);
        cmp.set("v.loadServices", false);
        cmp.set("v.loadModifyOrder", false);
    },
    
    clickModifyOrder: function (cmp, event, helper)	{
        var bpiId = event.getParam("BPIId");
        var modifyType = event.getParam("ModifyType");
        cmp.set("v.BPIId",bpiId);
        cmp.set("v.modifyType",modifyType);
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", false);
        cmp.set("v.batchId", null);  
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
        cmp.set("v.quotePage", false);    
        cmp.set("v.orderPage", false);
        cmp.set('v.isOrderDetailsOpen', false);
        cmp.set('v.isQuoteDetailsOpen', false);
        cmp.set("v.loadDraftOrders", false);
        cmp.set("v.loadServices", false);
        cmp.set("v.loadModifyOrder", true);
    },
   
    goToActiveSearchPage: function (cmp, event, helper)	{
        var bpiId = event.getParam("BPIId");
        var svcCacheSearch = event.getParam("svcCacheSearch");
        var OVCList = event.getParam("ovcList");
        var selectedOvc = event.getParam("selectedOvc");
        cmp.set("v.BPIId",bpiId);
        cmp.set("v.svcCacheSearch",svcCacheSearch);
        cmp.set("v.ovcList",OVCList);
        cmp.set("v.selectedOvc",selectedOvc);
        cmp.set("v.isTriggeredBySearch",true);
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", false);
        cmp.set("v.batchId", null);  
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
        cmp.set("v.quotePage", false);    
        cmp.set("v.orderPage", false);
        cmp.set('v.isOrderDetailsOpen', false);
	cmp.set('v.isQuoteDetailsOpen', false);
        cmp.set("v.loadDraftOrders", false);
        cmp.set("v.loadServices", true);
        cmp.set("v.loadModifyOrder", false);
    }
})