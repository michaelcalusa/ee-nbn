({ 
	clearPages: function (cmp) {
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", false);
        cmp.set("v.batchId", null);
        cmp.set("v.lstSitePage", false);
        cmp.set("v.sfPage", false);
        cmp.set("v.ragPage", false);
        cmp.set("v.quotePage", false);
        cmp.set("v.orderPage", false);
        cmp.set('v.isOrderDetailsOpen', false);
        cmp.set('v.isQuoteDetailsOpen', false);
        cmp.set("v.loadDraftOrders", false);
        cmp.set("v.loadServices", false);
        cmp.set("v.loadModifyOrder", false);
    }
})