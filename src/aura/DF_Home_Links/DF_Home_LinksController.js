({
    init: function (cmp, event, helper) {
        
        // Get custom label values
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");   	
        cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR); 	
        
        var action = cmp.get("c.getUserDetails");
        var loggedInDetails;
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                loggedInDetails = response.getReturnValue();
                if(loggedInDetails === "business"){
                    cmp.set("v.isBusiness", true);
                    cmp.set("v.isBusinessPlus", false);
                    cmp.set("v.isAssurance", false);
                }
                else if(loggedInDetails === "businessPlus"){
                    cmp.set("v.isBusinessPlus", true);
                    cmp.set("v.isBusiness", false);
                    cmp.set("v.isAssurance", false);
                }
                    else if(loggedInDetails === "assurance"){
                        cmp.set("v.isAssurance", true);
                        cmp.set("v.isBusiness", false);
                        cmp.set("v.isBusinessPlus", false);
                    }
            }
            else if(state === "ERROR") {
                cmp.set("v.responseStatus",state);
                cmp.set("v.type","Banner");
                cmp.set("v.responseMessage",errorLabel);
            }
        })
        $A.enqueueAction(action);
    },
    clickFeasibilityLink: function(cmp, event, helper) {		
        var homeDtEvt = cmp.getEvent("homePageEvent"); 
        homeDtEvt.setParams({"requestType" : "feasibility" });
        homeDtEvt.fire();
    },
    clickConnectOrderLink: function(cmp, event, helper) {		
        var homeDtEvt = cmp.getEvent("homePageEvent"); 
        homeDtEvt.setParams({"requestType" : "quote" });
        homeDtEvt.fire();
    },
    clickAcceptedOrdersLink: function(cmp, event, helper) {		
        var homeDtEvt = cmp.getEvent("homePageEvent"); 
        homeDtEvt.setParams({"requestType" : "order" });
        homeDtEvt.fire();
    },
    //Raja: Added below for Active Services
    clickActiveServicesLink: function(cmp, event, helper){
        var homeDtEvt = cmp.getEvent("homePageEvent"); 
        homeDtEvt.setParams({"requestType" : "services" });
        homeDtEvt.fire();
    },
    clickServiceIncidentLink: function(cmp, event, helper) {		
        var homeDtEvt = cmp.getEvent("homePageEvent"); 
        homeDtEvt.setParams({"requestType" : "assurance" });
        homeDtEvt.fire();
    }
})