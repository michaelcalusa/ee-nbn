({
	init: function (cmp, event, helper) {
        
        // Get custom label values
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");   	
        cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR); 	
        
        var action = cmp.get("c.getActiveRequestDetails");
        var loggedInDetails;
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var userInformation = JSON.parse(response.getReturnValue());
                cmp.set("v.userInfo", userInformation);
                loggedInDetails = cmp.get("v.userInfo").userType;
                if(loggedInDetails === "business"){
                    cmp.set("v.isBusiness", true);
                    cmp.set("v.isBusinessPlus", false);
                    cmp.set("v.isAssurance", false);
                }
                else if(loggedInDetails === "businessPlus"){
                    cmp.set("v.isBusinessPlus", true);
                    cmp.set("v.isBusiness", false);
                    cmp.set("v.isAssurance", false);
                }
                    else if(loggedInDetails === "assurance"){
                        cmp.set("v.isAssurance", true);
                        cmp.set("v.isBusiness", false);
                        cmp.set("v.isBusinessPlus", false);
                    }
            }
            else if(state === "ERROR") {
                cmp.set("v.responseStatus",state);
                cmp.set("v.type","Banner");
                cmp.set("v.responseMessage",errorLabel);
            }
        })
        $A.enqueueAction(action);
    }
})