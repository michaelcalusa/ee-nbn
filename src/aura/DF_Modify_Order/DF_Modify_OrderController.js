({
    init: function(cmp, event, helper) {
        //if(cmp.get("v.location")==null){
        //	var location = new Map();
       //     location.OrderId = '';
       //    cmp.set('v.location', location);
       // } 
      if(cmp.get("v.modifyType")=='Change_CoS_Profile_And_Bandwidth'){
        helper.getPreviousOrderNonBillableOVCValue(cmp);
      }
        helper.loadModifyOrderDetails(cmp);
        var modType = cmp.get("v.modifyType");
        var modifyType = modType.replace(/_/g, " ");
        if(modifyType == 'Change Service Restoration SLA'){
            helper.getOriginalESLA(cmp);
            //helper.checkESLA(cmp);
        }
       
        //cmp.set("v.originalSLA", "onLoad");
        
	},
   onRequiredSelectChange: function(cmp, event, helper) {    	
		helper.validateRequiredSelect(cmp, event);
	},
    onAfterBHSelectChange: function(cmp, event, helper){
        helper.validateAfterBH(cmp);
    },

	clickEditButton: function(cmp, event, helper) {		
		var OVCId = event.getSource().get("v.value");
		cmp.set("v.OVCId", OVCId);
		/*
        var selectedOVCId = cmp.get("v.selectedUNI.OVCId"); */
		helper.resetOVCMessage(cmp);
		helper.showOVCDetails(cmp, OVCId);
		if(cmp.get('v.ModifyOrderSetting.Mapping_Mode__c')) helper.ovcEnableMappingMode(cmp);
	},
    
    closeModal: function(cmp, event, helper) {		
        helper.closeModal(cmp);
        helper.ovcCalculateBandWidth(cmp);
	},
	
	clickActiveServiceSummaryButton: function(cmp, event, helper) {		
		console.log(':::clickActiveServiceSummaryButton::::');
		helper.goToActiveServiceSummaryPage(cmp, event);
	},	
    
    saveLocalButton:function(cmp, event, helper) {        
        helper.validateOVCNonBillable(cmp);
        helper.validateOVCCos(cmp);
        helper.validateAfterBH(cmp);
        var errorMessage = "";
        if (cmp.get("v.areOVCCosEmpty")) {
                if (errorMessage != "") {
                    errorMessage += "<br>" + $A.get("$Label.c.DF_No_COS_Selection_Error");
                } else {
                    errorMessage = $A.get("$Label.c.DF_No_COS_Selection_Error");
                }
        }
        else if (cmp.get("v.areOVCNonBillableEmpty")) {
            if (errorMessage != "") {
                errorMessage += "<br>" + $A.get("$Label.c.DF_Missing_OVC_Values");
            } else {
                errorMessage = $A.get("$Label.c.DF_Missing_OVC_Values");
            }
        }
        if (errorMessage !== "") {
            //validationError = true;
            cmp.set("v.uniBannertype", "Banner");
            cmp.set("v.responseStatusUNIPage", "ERROR");
            if(cmp.get("v.responseMessageUNIPage") == null || cmp.get("v.responseMessageUNIPage") == '' || cmp.get("v.responseMessageUNIPage") == undefined)
            	cmp.set("v.responseMessageUNIPage", errorMessage);            
            console.log('ERROR saveValidation '+errorMessage);
			cmp.set("v.saveValidationError", true); 
            return;
        } else {
            cmp.set("v.responseStatusUNIPage", "");
            cmp.set("v.responseMessageUNIPage", "");
            cmp.set("v.uniBannertype", "");
            cmp.set("v.saveValidationError", false);
        }
        if(!cmp.get("v.isAfterBHEmpty")){ 
            helper.svrSave(cmp);
        }
    },
    
    saveOVC: function(cmp, event, helper) {		
        cmp.set('v.goNext', false);	
		helper.saveOVC(cmp, event);
		
		if(cmp.get("v.ovcSaveValidationError"))
		{
			cmp.set("v.ovcSaveValidationError",false);
			return;
		}else
		{
			cmp.set("v.responseStatus", "OK");
            cmp.set("v.responseMessage", $A.get("$Label.c.DF_Quick_Quote_OVC_Save_Success"));
            cmp.set("v.type", "Banner");
		}
	},
    ovcBandWidthOnChange: function(cmp, event, helper){
    	helper.ovcCalculateBandWidth(cmp,event);
        helper.ovcEnableMappingMode(cmp);
	},
    saveAndCloseOVC: function(cmp, event, helper) {
        cmp.set('v.goNext', false);	
		helper.saveOVC(cmp, event);
		if(cmp.get("v.ovcSaveValidationError"))
		{
			cmp.set("v.ovcSaveValidationError",false);
			return;
		}   
		helper.closeModal(cmp, event);
	},
    addNewOVC: function(cmp, event, helper){
		helper.addNewOVC(cmp,event);
        //cmp.set("v.addOVC", "Y");
	//	console.log('## Back to Controller before showOVC ##');
	},
	removeOVCRow: function(cmp, event, helper) {
		var OVCId = event.getSource().get("v.value");
		var OVCList	= cmp.get("v.OVCList");	
            
		OVCList = OVCList.filter(function(item) { return item.OVCId != OVCId; });
           
		cmp.set("v.OVCList", OVCList);	
		cmp.set("v.isMaxOVCCount",false);
		
        helper.deleteOVC(cmp,OVCId);
	},
    
    onCEVLAINIDChange: function(cmp, event, helper) {
		helper.validateCEVLAINID(cmp, event);
       // if(cmp.get("v.uniVLANValid")===true){
        	helper.validateCEVLANIdCount(cmp, event);
       // }
	},
    clearDEVLANErrors: function(cmp, event, helper) {
        
		var ctrls = cmp.find("ovcField");		
		//debugger;
		var ctrl = ctrls.filter(function(item){ 
			return item.get("v.name") == "CEVLAINID";
		})[0];
        cmp.set("v.uniVLANValid",true);
        ctrl.setCustomValidity("");
        ctrl.set('v.validity', {
                  valid: true,
                  badInput : false
                 });	
    },
    onChangeSLA: function(cmp, event, helper) {    
        helper.saveToCS(cmp, cmp.get("v.selectedUNI.Id"));
		helper.checkESLA(cmp);
	},
})