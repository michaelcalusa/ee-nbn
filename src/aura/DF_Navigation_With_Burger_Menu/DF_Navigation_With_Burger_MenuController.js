({
    init: function (cmp, event, helper) {
        
        // Get custom label values
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");   	
        cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR); 	
        
        var action = cmp.get("c.getUserDetails");
        var loggedInDetails;
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                loggedInDetails = response.getReturnValue();
                if(loggedInDetails === "business"){
                    cmp.set("v.isBusiness", true);
                    cmp.set("v.isBusinessPlus", false);
                    cmp.set("v.isAssurance", false);
                }
                else if(loggedInDetails === "businessPlus"){
                    cmp.set("v.isBusinessPlus", true);
                    cmp.set("v.isBusiness", false);
                    cmp.set("v.isAssurance", false);
                }
                    else if(loggedInDetails === "assurance"){
                        cmp.set("v.isAssurance", true);
                        cmp.set("v.isBusiness", false);
                        cmp.set("v.isBusinessPlus", false);
                    }
            }
            else if(state === "ERROR") {
                cmp.set("v.responseStatus",state);
                cmp.set("v.type","Banner");
                cmp.set("v.responseMessage",ERR_MSG_APP_ERROR);
            }
        })
        $A.enqueueAction(action);
    },
    menuClick : function(component, event, helper) {
        var menuButtonnHandle = component.find('menuButtonId');
        var isOpen = $A.util.hasClass(component.find("menuButtonId"), "slds-is-open");
        if(isOpen){
            $A.util.removeClass(menuButtonnHandle, 'slds-is-open');
        }else{
            $A.util.addClass(menuButtonnHandle, 'slds-is-open');
        }
    },
    clickHomeLink: function(cmp, event, helper) {        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/"
        });
        urlEvent.fire();
        helper.hideBurgerMenu(cmp, event, helper);
    },
    clickDashboardLink: function(cmp, event, helper) {        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/ee"
        });
        urlEvent.fire();
        helper.hideBurgerMenu(cmp, event, helper);
    },
    clickFeasibilityLink: function(cmp, event, helper) {	        
        var homeDtEvt = cmp.getEvent("homePageEvent"); 
        homeDtEvt.setParams({"requestType" : "feasibility" });
        homeDtEvt.fire();
        helper.hideBurgerMenu(cmp, event, helper);
    },
    clickAcceptedOrdersLink: function(cmp, event, helper) {	        
        var homeDtEvt = cmp.getEvent("homePageEvent"); 
        homeDtEvt.setParams({"requestType" : "order" });
        homeDtEvt.fire();
        helper.hideBurgerMenu(cmp, event, helper);
    },
    clickServiceIncidentLink: function(cmp, event, helper) {		        
        var homeDtEvt = cmp.getEvent("homePageEvent"); 
        homeDtEvt.setParams({"requestType" : "assurance" });
        homeDtEvt.fire();
        helper.hideBurgerMenu(cmp, event, helper);
    },
    //Begin: Raja: Included below for view services
    clickActiveServicesLink: function(cmp, event, helper) {		        
        var homeDtEvt = cmp.getEvent("homePageEvent"); 
        homeDtEvt.setParams({"requestType" : "services" });
        homeDtEvt.fire();
        helper.hideBurgerMenu(cmp, event, helper);
    },//End: Raja
    showSpinner : function (cmp, event, helper) {
        helper.toggleLoadingSpinner(cmp, true);
    },
    hideSpinner: function(cmp, event, helper) {	
        helper.toggleLoadingSpinner(cmp, false);
    },
    closeBurgerMenu: function(cmp, event, helper) {	
        window.setTimeout(
    		$A.getCallback(function() {
        		helper.hideBurgerMenu(cmp, event, helper); 
    		}), 500
		);
    }
})