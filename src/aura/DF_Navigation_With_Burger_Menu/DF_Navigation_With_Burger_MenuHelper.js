({
	toggleLoadingSpinner: function (cmp, isVisible) {
		if(isVisible)
		{
			cmp.set('v.showLoadingSpinner', true);
		}else
		{	
			window.setTimeout(
				$A.getCallback(function() {				
					cmp.set("v.showLoadingSpinner", false);
				}), 1
			);		
		}
	},

    hideBurgerMenu: function(cmp, event, helper) {	
        var menuButtonnHandle = cmp.find('menuButtonId');
        var isOpen = $A.util.hasClass(cmp.find("menuButtonId"), "slds-is-open");
        if(isOpen){
            $A.util.removeClass(menuButtonnHandle, 'slds-is-open');
        }
    }
})