({
    doInit: function (cmp, event, helper) {
        console.log('==== Controller - doInit =====');
        var location = (!event.getParam("location")) ? cmp.get("v.location") : event.getParam("location");
        $A.createComponent(
            "c:DF_SF_Order_Item", {
                "location": location
            },
            function (newCmp) {
                if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
                }
            }
        );
         console.log('==== END Controller - doInit =====');
    },

    goToOrderContactDetailsPage: function (cmp, event, helper) {
        console.log('==== Controller - goToOrderContactDetailsPage =====');

        var location = (!event.getParam("location")) ? cmp.get("v.location") : event.getParam("location");
        //var opportunityId = (!event.getParam("opportunityId")) ? cmp.get("v.opportunityId") : event.getParam("opportunityId");

        console.log('==== Controller - goToOrderContactDetailsPage ===== location ' +JSON.stringify(location));//added
        $A.createComponent(
            "c:DF_Order_Contact_Details", {
                "location": location
            },
            function (newCmp) {
                if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
                }
            }
        );
    },

    goToOrderSiteDetailsPage: function (cmp, event, helper) {
        console.log('==== Controller - goToOrderSiteDetailsPage =====');

        var location = (!event.getParam("location")) ? cmp.get("v.location") : event.getParam("location");

        $A.createComponent(
            "c:DF_Order_Site_Details", {
                "location": location
            },
            function (newCmp) {
                if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
                }
            }
        );
    },

    goToOrderInstallationDetailsPage: function (cmp, event, helper) {
        console.log('==== Controller - goToOrderInstallationDetailsPage =====');

        var location = (!event.getParam("location")) ? cmp.get("v.location") : event.getParam("location");

        $A.createComponent(
            "c:DF_Order_Installation_Details", {
                "location": location
            },
            function (newCmp) {
                if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
                }
            }
        );
    },

    goToOrderItemPage: function (cmp, event, helper) {
        console.log('==== Controller - goToOrderItemPage =====');
        var location = (!event.getParam("location")) ? cmp.get("v.location") : event.getParam("location");

        $A.createComponent(
            "c:DF_SF_Order_Item", {
                "location": location
            },
            function (newCmp) {
                if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
                }
            }
        );
    },

    goToQuotePage: function (cmp, event, helper) {
        console.log('==== Controller - goToQuotePage =====');
		
		var location = (!event.getParam("location")) ? cmp.get("v.location") : event.getParam("location");
        $A.createComponent(
            "c:DF_SF_Quote", {
                selectedLocation: location
            },
            function (newCmp) {
                if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
                }
            }
        );
    }

})