({
    ERR_MSG_ERROR_OCCURRED: 'Error occurred. Error message: ',
    ERR_MSG_UNKNOWN_ERROR: 'Error message: Unknown error',
    MAX_COUNT: '5000',
	
	getOrderRecords : function(cmp) {		
		var isTriggerBySearch = cmp.get('v.isTriggerdBySearch');
		var searchTermVal = cmp.get('v.searchTerm');
        
		if (isTriggerBySearch) this.toogleClearSearchButton(cmp, false);
			
        var action = cmp.get("c.getRecords");
		
		action.setParams({"searchTerm": searchTermVal,"searchDate": null,"searchStatus": null, "isTriggerBySearch": Boolean(isTriggerBySearch)});
		action.setCallback(this, function(response) {
        
		var state = response.getState();
		console.log('response state :  ' + state);
        	if(state === "SUCCESS") {
				this.clearErrors(cmp);

                var records = JSON.parse(response.getReturnValue());
                console.log('record values :  ' + response.getReturnValue());
				cmp.set('v.fullList', records);
				cmp.set('v.currentList', records);				
				cmp.set("v.pageNumber", 1);
                var pageCountVal = cmp.get("v.selectedCount");
				cmp.set("v.maxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
				this.renderPage(cmp);
	        }
            else{
				var errors = response.getError();
				if (errors) {
                    if (errors[0] && errors[0].message) {
                    	console.log(this.ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                	console.log(this.ERR_MSG_UNKNOWN_ERROR);                 
                }

				var errorLabel = $A.get("$Label.c.DF_Application_Error");

				cmp.set("v.responseStatus", state);
                cmp.set("v.messageType","Banner");				
                cmp.set("v.responseMessage", errorLabel);				
            }
        });
        $A.enqueueAction(action);
	},    

    renderPage: function (cmp) {

        var records = cmp.get("v.fullList"),

            pageNumber = cmp.get("v.pageNumber"),
            pageCountVal = cmp.get("v.selectedCount"),
            pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));

        cmp.set('v.currentList', []);
        cmp.set("v.currentList", pageRecords);
    },        

    toogleClearSearchButton: function (cmp, disabled) {
        cmp.set('v.isClearSearchButtonDisabled', disabled);
    },

    sortListData: function (cmp, columnName, sortDirection) {
        var asc = 'ascending';

        var data = cmp.get("v.currentList");
        var reverse = sortDirection !== asc;

        data.sort(this.sortBy(columnName, reverse))
        cmp.set("v.currentList", data);
    },

    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function (x) {
                return primer(x[field])
            } :
            function (x) {
                return x[field]
            };

        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },

})