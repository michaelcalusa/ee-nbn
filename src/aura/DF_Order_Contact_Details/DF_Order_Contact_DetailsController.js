({
	init: function(cmp, event, helper) {	
		console.log('Order Contact Details ==== Controller - init =====');
		
		var loc =  cmp.get("v.location");
		console.log('ORder Contact Details location ' + JSON.stringify(loc));
		var id =  cmp.get("v.location").id;
		console.log('ORder Contact Details id ' + id);
	
		helper.loadStateOptions(cmp, event);
		//helper.loadMockData(cmp, event);
		helper.loadBusinessContact(cmp, event);
        helper.loadSiteContact(cmp, event);
	},

    clickBackButton: function(cmp, event, helper) {			
		helper.goToPreviousPage(cmp, event);		
	},

    clickOrderSummaryButton: function(cmp, event, helper) {			
    	helper.goToOrderSummaryPage(cmp, event);	
	},	

    clickNextButton: function(cmp, event, helper) {			
		helper.goToNextPage(cmp, event);		
	},
	
    clickAddBusinessContact: function(cmp, event, helper) {			
    	helper.addBusinessContact(cmp, event);	
	},
	
    clickEditBusinessContact: function(cmp, event, helper) {			
    	var busContIndex = event.getSource().get("v.value");
    	helper.editBusinessContact(cmp, event, busContIndex);	
	},
	
	clickDeleteBusinessContact: function(cmp, event, helper) {			

    	helper.deleteBusinessContact(cmp, event);	
	},	

    clickAddSiteContact: function(cmp, event, helper) {			
    	helper.addSiteContact(cmp, event);	
	},
	
    clickEditSiteContact: function(cmp, event, helper) {			
    	var siteContId = event.getSource().get("v.value");
    	helper.editSiteContact(cmp, event, siteContId);	
	},
	
	clickDeleteSiteContact: function(cmp, event, helper) {			
    	helper.deleteSiteContact(cmp, event);	
	},			
    
	clickBusContModalCancelButton: function(cmp, event, helper) {		
		helper.busContModalCancel(cmp, event);			
	},
	
	clickBusContModalSaveAndCloseButton: function(cmp, event, helper) {		
		helper.busContModalSave(cmp, event);
	},
	
	clickSiteContModalCancelButton: function(cmp, event, helper) {		
		helper.siteContModalCancel(cmp, event);			
	},
	
	clickSiteContModalSaveAndCloseButton: function(cmp, event, helper) {
		helper.siteContModalSave(cmp, event);		
	}
})