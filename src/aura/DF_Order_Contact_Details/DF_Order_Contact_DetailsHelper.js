({
    goToNextPage: function (cmp, event) {
        if(this.validateContacts(cmp)){
            console.log('Helper.goToNextPage - location: ' + cmp.get("v.location"));
    
            var evt = cmp.getEvent("orderSiteDetailsPageEvent");
            evt.setParams({
                "location": cmp.get("v.location")
            }); // Set with its location attra
            evt.fire();
        }
    },
	
    goToPreviousPage: function (cmp, event) {
        console.log('==== Helper - goToPreviousPage =====');
        var evt = cmp.getEvent("orderItemPageEvent");
        evt.setParam("location", cmp.get('v.location'));
        evt.fire();
    },
    validateContacts: function(cmp){
        
        var businessContacts = cmp.get("v.attrBusinessContactList");
        var siteContacts = cmp.get("v.attrSiteContactList");
        var ERR_MSG_APP_ERROR = "There must be at least one Site Contact and one Business Contact";
        //cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);
        if(businessContacts.length<1 || siteContacts.length<1){
        	
            this.setErrorMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "BANNER");
            return false;
        }
        this.clearErrors(cmp)
        
        return true;
        
        
    },
    goToOrderSummaryPage: function (cmp, event) {
        
        var location = cmp.get('v.location');				
		var evt = cmp.getEvent("quoteDetailPageEvent");        
		evt.setParams({
			"location": cmp.get("v.location")
		});
		evt.fire();
    },

    

    loadBusinessContact: function (cmp, event) {
        //add code here 
        console.log('==== Helper - loadBusinessContact =====');
        // Get custom label values
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);
        var location = cmp.get("v.location");
        //var orderJSON = cmp.get("v.orderJSON");

        var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
        var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

        // Create the action
        var action = cmp.get("c.getBusinessContactsAndOppContactRole");
        var quoteId = cmp.get("v.location").id;
        var oppName = cmp.get("v.location").quoteId;
        // Set apex controller action inputs
        action.setParams({
            "quoteId": quoteId,
            "oppName":oppName
        });


        // Add callback behavior for when response is received
        action.setCallback(this, function (response) {
            var state = response.getState();
            
            console.log('loadBusinessContact - state: ' + state);

            if (state === "SUCCESS") {
                // Get response string
                var responseReturnValue = response.getReturnValue();
                console.log('loadBusinessContact - responseReturnValue: ' + responseReturnValue);

                // Parse json string into js objects list
                //GET NUMBER OF ITEMS console.log('responseReturnValue.length ' +response.getReturnValue().value.length);
                var businessContactDataObjList = JSON.parse(response.getReturnValue());
                for(var i = 0; i<businessContactDataObjList.length; i++){
                    console.log('loadBusinessContact in loop');
                    businessContactDataObjList[i].busContIndex = i+1;
                }
                console.log('loadBusinessContact - businessContactDataObjList: ' + JSON.stringify(businessContactDataObjList));
                cmp.set("v.attrBusinessContactList", businessContactDataObjList);
                location.businessContactList = businessContactDataObjList;
               // orderJSON.businessContactList = businessContactDataObjList;
                cmp.set("v.location", location);
              //  cmp.set("v.orderJSON", orderJSON);

            } else {
                 	//Set error response comp with generic application error        
					//function(cmp, errorStatus, errorMessage, errorType) {
                	helper.setErrorMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "BANNER");

                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                    console.log(ERR_MSG_UNKNOWN_ERROR);
                }

            }
        });

        // Send action off to be executed
        $A.enqueueAction(action);
    },

    loadSiteContact: function (cmp, event) {
        //add code here 
        console.log('==== Helper - loadSiteContact =====');
        var location = cmp.get("v.location");
        //var orderJSON = cmp.get("v.orderJSON");
        // Get custom label values
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);

        var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
        var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

        // Create the action
        var action = cmp.get("c.getSiteContactsAndOppContactRole");
        var quoteId = cmp.get("v.location").id;
        var oppName = cmp.get("v.location").quoteId;
        // Set apex controller action inputs
        action.setParams({
            "quoteId": quoteId,
            "oppName":oppName
        });


        // Add callback behavior for when response is received
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('loadSiteContact - state: ' + state);

            if (state === "SUCCESS") {
                // Get response string
                var responseReturnValue = response.getReturnValue();
                console.log('loadSiteContact - responseReturnValue: ' + responseReturnValue);

                // Parse json string into js objects list
                var siteContactDataObjList = JSON.parse(response.getReturnValue());
                for(var i = 0; i<siteContactDataObjList.length; i++){
                    siteContactDataObjList[i].siteContIndex = i+1;
                }
                console.log('loadSiteContact - siteContactDataObjList: ' + JSON.stringify(siteContactDataObjList));
                cmp.set("v.attrSiteContactList", siteContactDataObjList);
                location.siteContactList = siteContactDataObjList;
                //orderJSON.siteContactList = siteContactDataObjList;
                cmp.set("v.location", location);
				//cmp.set("v.orderJSON", orderJSON);
            } else {
                // Set error response comp with generic application error        
                //helper.setErrorMsg(cmp, "ReadySection", "ERROR", ERR_MSG_APP_ERROR, "Banner");

                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                    console.log(ERR_MSG_UNKNOWN_ERROR);
                }

            }
        });

        // Send action off to be executed
        $A.enqueueAction(action);
    },

    addBusinessContact: function (cmp, event) {
        console.log('==== Helper - addBusinessContact =====');

        var businessContactList = cmp.get("v.attrBusinessContactList");
        var maxbusContId = businessContactList.length == 0 ? 0 : Math.max.apply(Math, businessContactList.map(function (item) {
            return item.busContIndex;
        }));

        var newBusinessContact = {
            'busContIndex': maxbusContId + 1,
            'busContId':'',
            'busContFirstName': '',
            'busContSurname': '',
            'busContRole': '',
            'busContEmail': '',
            'busContNumber': '',
            'busContStreetAddress': '',
            'busContSuburb': '',
            'busContPostcode': '',
            'busContState': ''
        };

		cmp.set("v.attrSelectedBusinessContact", newBusinessContact);
        cmp.set("v.attrIsBusContModalOpen", true);

        //businessContactList.push(newBusinessContact);
        //cmp.set("v.attrBusinessContactList", businessContactList);
    },

    editBusinessContact: function (cmp, event, busContIndex) {
        console.log('==== Helper - editBusinessContact =====');

        console.log('Helper.editBusinessContact - busContIndex: ' + busContIndex);

        var businessContactList = cmp.get("v.attrBusinessContactList");  
        var results = businessContactList.filter(function (item) {
            return item.busContIndex == busContIndex;
        });
        console.log('results '+ JSON.stringify(results) + '  ' +results.length);
        
        cmp.set("v.attrSelectedBusinessContact", this.cloneBusinessContact(results[0]));
        cmp.set("v.attrIsBusContModalOpen", true);
        
    },

    deleteBusinessContact: function (cmp, event) {
        console.log('==== Helper - deleteBusinessContact =====');

        var busContId = event.getSource().get("v.value");
        console.log('==== Helper - deleteBusinessContact busContId ===== ' + busContId);

        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);

        var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
        var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

        // Create the action
        var action = cmp.get("c.deleteSiteOrBusinessContact");
        var quoteId = cmp.get("v.location").id;
        // Set apex controller action inputs
        action.setParams({
            "contactId": busContId
        });


        // Add callback behavior for when response is received
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('deleteBusinessContact - state: ' + state);

            if (state === "SUCCESS") {
                // Get response string
                var responseReturnValue = response.getReturnValue();
                console.log('deleteBusinessContact - responseReturnValue: ' + responseReturnValue);

                // Parse json string into js objects list
                var siteContactDataObjList = JSON.parse(response.getReturnValue());
               // console.log('deleteBusinessContact - delete: ' + siteContactDataObjList);
                var businessContactList = cmp.get("v.attrBusinessContactList");
        		businessContactList = businessContactList.filter(function (item) {
            		return item.busContId != busContId;
        			});
        		cmp.set("v.attrBusinessContactList", businessContactList);
                var location = cmp.get("v.location");
				location.businessContactList = businessContactList; 
 				cmp.set("v.location", location);

            } else {
                // Set error response comp with generic application error        
                //helper.setErrorMsg(cmp, "ReadySection", "ERROR", ERR_MSG_APP_ERROR, "Banner");

                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                    console.log(ERR_MSG_UNKNOWN_ERROR);
                }
                var businessContactList = cmp.get("v.attrBusinessContactList");
        		businessContactList = businessContactList.filter(function (item) {
            		return item.busContId != busContId;
        			});
        		cmp.set("v.attrBusinessContactList", businessContactList);

            }
        });

        // Send action off to be executed
        $A.enqueueAction(action);

    },

    cloneBusinessContact: function (businessContact) {
        console.log('==== Helper - cloneBusinessContact =====');

        var clonedBusinessContact = {
            'busContId': businessContact.busContId,
            'busContIndex':businessContact.busContIndex,
            'busContFirstName': businessContact.busContFirstName,
            'busContSurname': businessContact.busContSurname,
            'busContRole': businessContact.busContRole,
            'busContEmail': businessContact.busContEmail,
            'busContNumber': businessContact.busContNumber,
            'busContStreetAddress': businessContact.busContStreetAddress,
            'busContSuburb': businessContact.busContSuburb,
            'busContPostcode': businessContact.busContPostcode,
            'busContState': businessContact.busContState
        };

        return clonedBusinessContact;
    },

    addSiteContact: function (cmp, event) {
        console.log('==== Helper - addSiteContact =====');

        var siteContactList = cmp.get("v.attrSiteContactList");
        var maxsiteContId = siteContactList.length == 0 ? 0 : Math.max.apply(Math, siteContactList.map(function (item) {
            return item.siteContIndex;
        }));

        var newSiteContact = {
            'siteContId':'',
            'siteContIndex': maxsiteContId + 1,
            'siteContFirstName': '',
            'siteContSurname': '',
            'siteContEmail': '',
            'siteContNumber': ''
        };
		cmp.set("v.attrSelectedSiteContact", newSiteContact);
        cmp.set("v.attrIsSiteContModalOpen", true);

        //siteContactList.push(newSiteContact);
        //cmp.set("v.attrSiteContactList", siteContactList);
    },

    editSiteContact: function (cmp, event, siteContIndex) {
        console.log('==== Helper - editSiteContact =====');

        console.log('Helper.editSiteContact - siteContId: ' + siteContIndex);

        var siteContactList = cmp.get("v.attrSiteContactList");

        var results = siteContactList.filter(function (item) {
            return item.siteContIndex == siteContIndex;
        });

        cmp.set("v.attrSelectedSiteContact", this.cloneSiteContact(results[0]));
        cmp.set("v.attrIsSiteContModalOpen", true);
    },

  deleteSiteContact: function (cmp, event) {
        console.log('==== Helper - deleteSiteContact =====');

        var siteContId = event.getSource().get("v.value");
       // var siteContactList = cmp.get("v.attrSiteContactList");
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);
		console.log('!!!!!' + siteContId );
        var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
        var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

        // Create the action
        var action = cmp.get("c.deleteSiteOrBusinessContact");
        var quoteId = cmp.get("v.location").id;
        // Set apex controller action inputs
        action.setParams({
            "contactId": siteContId
        });


        // Add callback behavior for when response is received
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('deleteBusinessContact - state: ' + state);

            if (state === "SUCCESS") {
                // Get response string
                var responseReturnValue = response.getReturnValue();
                console.log('deleteBusinessContact - responseReturnValue: ' + responseReturnValue);

                // Parse json string into js objects list
                var siteContactDataObjList = JSON.parse(response.getReturnValue());
                
                var siteContactList = cmp.get("v.attrSiteContactList");
                siteContactList = siteContactList.filter(function (item) {
            			return item.siteContId != siteContId;
        			});
       			cmp.set("v.attrSiteContactList", siteContactList);
				var  location = cmp.get("v.location");
				location.siteContactList = siteContactList; 
 				cmp.set("v.location", location);

            } else {
                // Set error response comp with generic application error        
                //helper.setErrorMsg(cmp, "ReadySection", "ERROR", ERR_MSG_APP_ERROR, "Banner");

                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                    console.log(ERR_MSG_UNKNOWN_ERROR);
                }
                var siteContactList = cmp.get("v.attrSiteContactList");
                siteContactList = siteContactList.filter(function (item) {
            			return item.siteContId != siteContId;
        			});
       			cmp.set("v.attrSiteContactList", siteContactList);
            }
        });

        // Send action off to be executed
        $A.enqueueAction(action);

    },
    
    cloneSiteContact: function (siteContact) {
        console.log('==== Helper - cloneSiteContact =====');

        var clonedSiteContact = {
            'siteContId': siteContact.siteContId,
            'siteContIndex':siteContact.siteContIndex,
            'siteContFirstName': siteContact.siteContFirstName,
            'siteContSurname': siteContact.siteContSurname,
            'siteContEmail': siteContact.siteContEmail,
            'siteContNumber': siteContact.siteContNumber
        };

        return clonedSiteContact;
    },

    busContModalCancel: function (cmp) {
        cmp.set("v.attrIsBusContModalOpen", false);
		cmp.set("v.responseStatusContactPage", "");
        cmp.set("v.responseMessageContactPage", "");
        cmp.set("v.contactBannertype",""); 
    },

    busContModalSave: function (cmp, event) {
    	console.log('==== Helper - busContModalSave =====');
        var isValid = true;

        //if (!cmp.find('BusContNumber').get('v.validity').valid) isValid = false;
		//if (!cmp.find('BusContEmail').get('v.validity').valid) isValid = false;
		
		isValid = this.checkRequiredFields(cmp, 'busContFields');
		console.log('==== isValid ===== ' + isValid);
        if (isValid) {			
            var location = cmp.get("v.location");
            var op = JSON.stringify(location);
            console.log('==== Helper - busContModalSave =====' + op);
			var updatedBusinessContactList;
            var selectedBusinessContact = cmp.get("v.attrSelectedBusinessContact");
            var businessContactList = cmp.get("v.attrBusinessContactList");
            var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

            // Create the action
            var action = cmp.get("c.addBusinessContactToContactsAndOppContactRole");
            // Set apex controller action inputs
            var quoteId = cmp.get("v.location").id;
            var businessContactString = selectedBusinessContact;
            var contactRoleType = "Business Contact";
            action.setParams({
                "quoteId": quoteId,
                "jsonContactDetails": JSON.stringify(businessContactString),
                "contactRoleType": contactRoleType
            });
            // Add callback behavior for when response is received
            action.setCallback(this, function (response) {
                var state = response.getState();
                console.log('busContModalSave - state: ' + state);

                if (state === "SUCCESS") {
                    // Get response string
                    // 
                    //if not in list
					var filteredList = businessContactList.filter(function (item) {
               			 return item.busContIndex == selectedBusinessContact.busContIndex;
            		});

					if(filteredList.length == 0){
						businessContactList.push(selectedBusinessContact);
						cmp.set("v.attrBusinessContactList", selectedBusinessContact);
					}
					//loadBusinessContact(cmp,event);
                    var responseReturnValue = response.getReturnValue();
                    console.log('busContModalSave - responseReturnValue: ' + responseReturnValue);
					cmp.set("v.responseStatusContactPage", "");
                	cmp.set("v.responseMessageContactPage", "");
                	cmp.set("v.contactBannertype",""); 
                    selectedBusinessContact.busContId = responseReturnValue;
                    console.log('!!!!!!!'+ JSON.stringify(selectedBusinessContact));
                    updatedBusinessContactList = businessContactList.map(function (item) {
                		return item.busContIndex === selectedBusinessContact.busContIndex ? selectedBusinessContact : item;
           			 });
                    
                    location.businessContactList = updatedBusinessContactList;
                    cmp.set("v.attrBusinessContactList", updatedBusinessContactList);
            		cmp.set("v.attrSelectedBusinessContact", selectedBusinessContact);

					location.businessContactList = updatedBusinessContactList; 
 					cmp.set("v.location", location);
                    this.clearErrors(cmp);
                    this.busContModalCancel(cmp);	
                } else {                    
                    //Set error response comp with generic application error        
                   // helper.setErrorMsg(cmp, "ReadySection", "ERROR", ERR_MSG_APP_ERROR, "Banner");
                   
                   // cmp.set("v.contactBannertype","Banner");
                    //cmp.set("v.responseStatusContactPage", "ERROR"); 
                        
					                   
                    //cmp.set("v.attrBusinessContactList", businessContactList);
            		//cmp.set("v.attrSelectedBusinessContact", selectedBusinessContact);
                    var errors = response.getError();
					var ERR_MSG_ERROR_OCCURRED = errors[0].message;
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            if(errors[0].message.indexOf("INVALID_EMAIL_ADDRESS")>-1){
                                this.setErrorMsg(cmp, "ERROR", "Invalid email Address", "BANNER"); 
                                cmp.set("v.responseMessageContactPage", "Invalid Email Address");
                            }
                            else{
                               	cmp.set("v.responseMessageContactPage", errors[0].message); 
                            } 
                            
                        }
                    } else {
                        console.log(ERR_MSG_UNKNOWN_ERROR);
                    }
                    
                   
                }
            });

            // Send action off to be executed
            $A.enqueueAction(action);


			
            console.log('==== Helper - selectedBusinessContact firstname =====' + selectedBusinessContact.busContFirstName);
        }
    },
    
    checkRequiredFields: function(cmp, field)	{
    	var isNotEmpty = true;
        var requiredFields = cmp.find(field);
        var blank=0;
        if(requiredFields.length!=undefined) {
            var allValid = requiredFields.reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
            }, true);
            if (!allValid) {
                isNotEmpty = false; 
            }
        }
        
        return isNotEmpty;
    },
    
    siteContModalCancel: function (cmp) {
        cmp.set("v.attrIsSiteContModalOpen", false);
		cmp.set("v.responseStatusContactPage", "");
        cmp.set("v.responseMessageContactPage", "");
        cmp.set("v.contactBannertype",""); 
    },

 siteContModalSave: function (cmp, event) {

        var isValid = true;
		var statusFlag;
        //if (!cmp.find('SiteContNumber').get('v.validity').valid) isValid = false;
		//if (!cmp.find('SiteContEmail').get('v.validity').valid) isValid = false;
		isValid = this.checkRequiredFields(cmp, 'siteContFields');
		console.log('==== isValid ===== ' + isValid);
		
        if (isValid) {
			
            var location = cmp.get("v.location");
            var selectedSiteContact = cmp.get("v.attrSelectedSiteContact");
            var siteContactList = cmp.get("v.attrSiteContactList");
			var updatedSiteContactList;
			var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

            // Create the action
            var action = cmp.get("c.addSiteContactToContactsAndOppContactRole");
            // Set apex controller action inputs
            var quoteId = cmp.get("v.location").id;
            var siteContactString = selectedSiteContact;
            var contactRoleType = "Site Contact";
            action.setParams({
                "quoteId": quoteId,
                "jsonContactDetails": JSON.stringify(siteContactString),
                "contactRoleType": contactRoleType
            });
            // Add callback behavior for when response is received
            action.setCallback(this, function (response) {
                var state = response.getState();

                if (state === "SUCCESS") {
                    statusFlag = "SUCCESS";

					//if not in list
					var filteredList = siteContactList.filter(function (item) {
               			 return item.siteContIndex == selectedSiteContact.siteContIndex;
            		});

					if(filteredList.length == 0){
						siteContactList.push(selectedSiteContact);
						cmp.set("v.attrSiteContactList", selectedSiteContact);
					}

                    // Get response string
                    var responseReturnValue = response.getReturnValue();
                    cmp.set("v.responseStatusContactPage", "");
                	cmp.set("v.responseMessageContactPage", "");
                	cmp.set("v.contactBannertype",""); 
                    selectedSiteContact.siteContId = responseReturnValue;
                    updatedSiteContactList = siteContactList.map(function (item) {
               			 return item.siteContIndex === selectedSiteContact.siteContIndex ? selectedSiteContact : item;
            		});
                    
                    location.siteContactList = updatedSiteContactList;

                    cmp.set("v.attrSiteContactList", updatedSiteContactList);
                    cmp.set("v.attrSelectedSiteContact", selectedSiteContact);

                    location.siteContactList = updatedSiteContactList; 
                     cmp.set("v.location", location);
             		this.clearErrors(cmp);
                    this.siteContModalCancel(cmp);
                } else {
                    //Set error response comp with generic application error        
                   // helper.setErrorMsg(cmp, "ReadySection", "ERROR", ERR_MSG_APP_ERROR, "Banner");
                    
                    cmp.set("v.contactBannertype","Banner");
                    cmp.set("v.responseStatusContactPage", "ERROR"); 
                                        
                    //updatedSiteContactList=siteContactList;
                    var errors = response.getError();

                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            if(errors[0].message.indexOf("INVALID_EMAIL_ADDRESS")>-1){
                                this.setErrorMsg(cmp, "ERROR", "Invalid email Address", "BANNER"); 
                                cmp.set("v.responseMessageContactPage", "Invalid Email Address");
                            }
                            else{
                               	cmp.set("v.responseMessageContactPage", errors[0].message); 
                            } 
                          
                        }
                    } else {
                        console.log(ERR_MSG_UNKNOWN_ERROR);
                    }
                   
                }
            });

            // Send action off to be executed
            $A.enqueueAction(action);

        }
    },

    loadStateOptions: function (cmp, event) {
        console.log('==== Helper - loadStateOptions =====');

        var opts = [{
                value: "",
                label: "Select"
            },
            {
                value: "NSW",
                label: "New South Wales"
            },
            {
                value: "VIC",
                label: "Victoria"
            },
            {
                value: "QLD",
                label: "Queensland"
            },
            {
                value: "WA",
                label: "Western Australia"
            },
            {
                value: "SA",
                label: "South Australia"
            },
            {
                value: "TAS",
                label: "Tasmania"
            },
            {
                value: "ACT",
                label: "Australian Capital Territory"
            },
            {
                value: "NT",
                label: "Northern Territory"
            }
        ];
        cmp.set("v.attrStateOptions", opts);
    },
        // Sets error msg properties
    setErrorMsg: function(cmp, errorStatus, errorMessage, errorType) {        	
        // Set error response comp attribute values 
        cmp.set("v.responseStatus", errorStatus);	
        cmp.set("v.responseMessage", errorMessage);	
        cmp.set("v.messageType", errorType);  
    },
    
	clearErrors : function(cmp) {
        // Set error response comp attribute values 
        cmp.set("v.responseStatus", "");	
        cmp.set("v.responseMessage", "");	
        cmp.set("v.messageType", "");
	}

})