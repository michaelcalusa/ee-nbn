({
	init: function(cmp, event, helper) {	
		console.log('==== Controller - init =====');
		
		helper.loadNTDMountingOptions(cmp, event);
		helper.loadPowerSupply1Options(cmp, event);
		helper.loadPowerSupply2Options(cmp, event);
        helper.loadOrderInstallationDetails(cmp, event);
	},

    clickClearButton: function(cmp, event, helper) {			
    	console.log('==== Controller - clickClearButton =====');
    	
		helper.clear(cmp, event);		
	},

    clickBackButton: function(cmp, event, helper) {			
    	console.log('==== Controller - clickBackButton =====');
    	
		helper.goToPreviousPage(cmp, event);		
	},
    clickOrderSaveButton: function(cmp, event, helper) {			
    	console.log('==== Controller - clickBackButton =====');
    	
		helper.setOrderInstallationDetails(cmp, event);	
	},	
    clickOrderSummaryButton: function(cmp, event, helper) {			
    	console.log('==== Controller - clickOrderSummaryButton =====');

    	helper.goToOrderSummaryPage(cmp, event);	
	},

    clickSubmitOrderButton: function(cmp, event, helper) {			
    	console.log('==== Controller - clickSubmitOrderButton =====');

    	helper.submitOrder(cmp, event);	
	},		
    
	onDateChange: function(cmp, event, helper){
		helper.validateFutureDate(cmp, event, false);
	},

	onRequiredSelectChange: function(cmp, event, helper) {    	
     	if(helper.validateRequiredSelect(cmp, event)){
            if(event.getSource().get("v.name") == "NTDMounting"){
                helper.setPowerSupplyOptionsForNtdMounting(cmp);
            }
        }
	}

})