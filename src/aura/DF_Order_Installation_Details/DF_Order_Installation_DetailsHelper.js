({  
    clear: function(cmp, event) {
        console.log('==== Helper - clear =====');
        
		// Clear form fields
        cmp.set("v.attrCustomerRequiredDate", "");
        cmp.set("v.attrNTDMounting", "");
        cmp.set("v.attrPowerSupply1", "");
        cmp.set("v.attrPowerSupply2", "");
        cmp.set("v.attrSupportingDoc", "");
        cmp.set("v.attrInstallDetailsNotes", ""); 
	},

    loadOrderInstallationDetails: function (cmp, event){
        var self = this;
        var action = cmp.get("c.loadDFOrderInstallationDetails");
        var location = cmp.get("v.location");
        var dfOrderId = location.OrderId;
        console.log('==== DF ORDER site details Helper ==== dfOrderId ' + dfOrderId); 
        action.setParams({
            dfOrderId: dfOrderId
			});
        
        action.setCallback(this, function(response) {
            var results = JSON.parse(response.getReturnValue());
            var state = response.getState();
           
            console.log('==== DF ORDER Installation details Helper ==== setSiteDetails respose' + JSON.stringify(results ));
            
            // add error handling 
            if(state === "SUCCESS"){				
                cmp.set("v.attrCustomerRequiredDate", results["Customer Required Date"]=="null"?"":results["Customer Required Date"][0]);
        		cmp.set("v.attrNTDMounting", results["NTD Mounting"][0]);
       			cmp.set("v.attrPowerSupply1", results["Power Supply 1"][0]);
        		cmp.set("v.attrPowerSupply2", results["Power Supply 2"][0]);
        		cmp.set("v.attrInstallDetailsNotes", results["Installation Notes"][0]);
                location.orderInstallationDetials = results[0];
                cmp.set("v.location",location) ;
                //alert("loaded order installation details with ntd mounting: " + results["NTD Mounting"][0]);
                self.setPowerSupplyOptionsForNtdMounting(cmp);
            }  //do some error handling 
        })
        $A.enqueueAction(action);
        
        
    },
    
    setOrderInstallationDetails: function (cmp, event){
        var action = cmp.get("c.setDFOrderInstallationDetails");
        var location = cmp.get("v.location");
        var dfOrderId = location.OrderId;
          console.log('==== DF ORDER installation details Helper ==== setOrderInstallationDetails orderId' + dfOrderId );
        console.log('==== DF ORDER installation details Helper ==== setOrderInstallationDetails variables' + cmp.get("v.attrCustomerRequiredDate")+' '+ cmp.get("v.attrNTDMounting")+' '+ 
           cmp.get("v.attrPowerSupply1")+' '+ 
            cmp.get("v.attrPowerSupply2")+' '+ 
         cmp.get("v.attrInstallDetailsNotes") );
        action.setParams({
            dfOrderId: dfOrderId,
            custReqDate: cmp.get("v.attrCustomerRequiredDate"),
            ntdMounting: cmp.get("v.attrNTDMounting")+"",
            powerSupp1: cmp.get("v.attrPowerSupply1")+"",
            powerSupp2: cmp.get("v.attrPowerSupply2")+"",
            installationNotes: cmp.get("v.attrInstallDetailsNotes")+""
        });
        //=="null"?"":cmp.get("v.attrCustomerRequiredDate")+"", ;//=="null"?"":("v.attrInstallDetailsNotes")+""
        action.setCallback(this, function(response) {
            var list = response.getReturnValue();
            //do some error handling 
            console.log('==== DF ORDER installation details Helper ==== setOrderInstallationDetails respose' + response );
        })
        $A.enqueueAction(action);
        
    },    
	
    goToPreviousPage: function(cmp, event) {
        console.log('==== Helper - goToPreviousPage =====');
        
        console.log('Helper.goToPreviousPage - location: ' + JSON.stringify(cmp.get("v.location")));
       // this.saveValidation(cmp,event);
      //  if(cmp.get("v.saveValidationError"))
     //   {
      //      return;
      //  }
        this.setOrderInstallationDetails(cmp, event);
        var evt = cmp.getEvent("orderSiteDetailsPageEvent");
        evt.setParams({
            "location": cmp.get("v.location")
        }); // Set with its location attra
        evt.fire();
	}, 
	
	goToOrderSummaryPage: function(cmp, event) {

		var location = cmp.get('v.location');				
		var evt = cmp.getEvent("quoteDetailPageEvent");        
		evt.setParams({
			"location": cmp.get("v.location")
		});
		evt.fire();
	},	

	submitOrder: function(cmp, event) {
        
        console.log('==== Helper - submitOrder =====');
        this.saveValidation(cmp,event);
        if(cmp.get("v.saveValidationError"))
        {
            return;
        }
         this.setOrderInstallationDetails(cmp, event);
        alert("The Order Has Been Submitted");
           this.updateOrderJSON(cmp);
        var location = cmp.get("v.location");
        location.status = "Submitted";
        cmp.set("v.location", location);
        this.goToOrderSummaryPage(cmp, event);
	}, 	

     updateOrderJSON: function(cmp){
       var location = cmp.get("v.location");
         var dfOrderId = location.orderId==null?location.OrderId:location.orderId;
       var action = cmp.get("c.updateOrderJSON");
       var jLocation = JSON.stringify(location);

       console.log('===== updateOrderJSON ===== orderId ' + dfOrderId);
       console.log('===== updateOrderJSON ===== location ' + jLocation);
       action.setParams({
          	dfOrderId: dfOrderId,
           location: jLocation
        });
       action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('===== updateOrderJSON ===== state ' + state)
            if (state === "SUCCESS") {
                 
            } else //if (state === "ERROR") 
            {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                console.log('updateOrderJSON Error ' + errormessage);
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
            }

        });
        $A.enqueueAction(action);
        console.log('===== updateOrderJSON ===== END');
       
            
   },

	loadNTDMountingOptions: function (cmp, event) {
		console.log('==== Helper - loadNTDMountingOptions =====');
		
        var opts = [
            { value: "", label: "Select" },
			{ value: "Rack", label: "Rack" },
			{ value: "Wall", label: "Wall" }
         ];
        cmp.set("v.attrNTDMountingOptions", opts);
    },
	
    loadPowerSupply1Options: function (cmp, event) {
		console.log('==== Helper - loadPowerSupply1Options =====');
		
        var opts = [
            { value: "", label: "Select" },
			{ value: "DC (-48V)", label: "DC (-48V)" },
			{ value: "AC (240V)", label: "AC (240V)" }
         ];
        cmp.set("v.attrPowerSupply1Options", opts);
    },
	
    loadPowerSupply2Options: function (cmp, event) {
		console.log('==== Helper - loadPowerSupply2Options =====');
		
        var opts = [
            { value: "", label: "Select" },
			{ value: "DC (-48V)", label: "DC (-48V)" },
			{ value: "AC (240V)", label: "AC (240V)" },
			{ value: "Not Required (NR)", label: "Not Required (NR)" }
         ];
        cmp.set("v.attrPowerSupply2Options", opts);
    },
    
    loadPowerSupply2OptionsForWallMountedNtd: function (cmp, event) {
        //remove the DC option
        var opts = [
            { value: "", label: "Select" },
			{ value: "AC (240V)", label: "AC (240V)" },
			{ value: "Not Required (NR)", label: "Not Required (NR)" }
         ];
        cmp.set("v.attrPowerSupply2Options", opts);
    },

    setPowerSupplyOptionsForNtdMounting: function(cmp){
        if(cmp.get("v.attrNTDMounting") == "Wall"){
            cmp.set("v.attrPowerSupply1", "AC (240V)");
            this.loadPowerSupply2OptionsForWallMountedNtd(cmp);
            cmp.set("v.isNtdWallMounting", true);
        }else{
            this.loadPowerSupply2Options(cmp);
            cmp.set("v.isNtdWallMounting", false);
        }
    },
    
    validateWallMountingPowerSupply: function(cmp){
        if(cmp.get("v.attrNTDMounting") == "Wall"){
            if(cmp.get("v.attrPowerSupply1") != "AC (240V)" ||
               ( cmp.get("v.attrPowerSupply2") != "AC (240V)" && cmp.get("v.attrPowerSupply2") != "Not Required (NR)") ){
                //console.log("Invalid power supply options for wall mounted NTD");
                return false;
            }
        }
        return true;
    },

	validateFutureDate: function(cmp, event, checkEmpty) {
		var tempCtrl = cmp.find("btnNext");
		var hasError = false;
		var today = new Date();  

        today.setDate(today.getDate() + 31);     
		 
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        // if date is less then 10, then append 0 before date   
        if(dd < 10){
            dd = '0' + dd;
        } 

        // if month is less then 10, then append 0 before date    
        if(mm < 10){
            mm = '0' + mm;
        }
        
            

		var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
		var attrCustomerRequiredDate = cmp.get("v.attrCustomerRequiredDate");
		var customerRequiredDateInputField = cmp.find("CustomerRequiredDate");

		var customerRequiredDate = customerRequiredDateInputField.get("v.value");
        
        if(!this.isValidDate(customerRequiredDate)){
            cmp.set("v.dateValidationError" , true);
			cmp.set("v.dateValidationErrorMsg" , $A.get("$Label.c.DF_Customer_Required_Date_Error"));			
            hasError = true;
        }else if(attrCustomerRequiredDate !="" && attrCustomerRequiredDate < todayFormattedDate){
			cmp.set("v.dateValidationError" , true);
			cmp.set("v.dateValidationErrorMsg" , $A.get("$Label.c.DF_Customer_Required_Date_Error"));			
            hasError = true;
        }else
		{
			if(checkEmpty && !attrCustomerRequiredDate){
				cmp.set("v.dateValidationError" , true);
				cmp.set("v.dateValidationErrorMsg" , $A.get("$Label.c.DF_Field_Empty_Error"));			
				hasError = true;
			}else
			{
				cmp.set("v.dateValidationError" , false);
			}			
		}

		customerRequiredDateInputField.focus();	
		tempCtrl.focus();
		customerRequiredDateInputField.focus();	
		event.getSource().focus();

		return hasError;
	},

    saveValidation : function(cmp,event) {
        
		var tempCtrl = event.getSource();
		var fieldGroupName = "installationField"
		var ctrls = cmp.find(fieldGroupName);		

		var powerSupply1Ctrl = ctrls.filter(this.getComponentByName("PowerSupply1"))[0];
		var powerSupply2Ctrl = ctrls.filter(this.getComponentByName("PowerSupply2"))[0]; 
		var ntdMountingCtrl = ctrls.filter(this.getComponentByName("NTDMounting"))[0]; 

		this.validateSelectCmpEmpty(powerSupply1Ctrl);
		this.validateSelectCmpEmpty(powerSupply2Ctrl);
		this.validateSelectCmpEmpty(ntdMountingCtrl);

		var allValid = this.isAllFieldsValid(cmp, fieldGroupName);

		if(this.validateFutureDate(cmp, event, false)) allValid = false;
        
        if(!this.validateWallMountingPowerSupply(cmp)){
            allValid = false;
        }

		tempCtrl.focus();        
        
        if (!allValid) cmp.set("v.saveValidationError",true);
        else cmp.set("v.saveValidationError",false);
    },
    
	validateRequiredSelect: function(cmp, event) {	
		var ctrl = event.getSource();		
		var tempCtrl = cmp.find("btnNext");

		this.validateSelectCmpEmpty(ctrl);

		var valid = ctrl.get("v.validity").valid;

		if (!valid) {            
			cmp.set("v.ovcSaveValidationError", true);
			this.reFocus(ctrl, tempCtrl);
        }
		
		event.getSource().focus();

		return valid;
	},
    isValidDate: function (dateString) {
        if(dateString == "" || dateString == null){ 
        	return true;
        }else{
              var regEx = /^\d{4}-\d{2}-\d{2}$/;
              if(!dateString.match(regEx)) return false;  // Invalid format
              var d = new Date(dateString);
              if(!d.getTime() && d.getTime() !== 0) return false; // Invalid date
              return d.toISOString().slice(0,10) === dateString;
            }
        }
})