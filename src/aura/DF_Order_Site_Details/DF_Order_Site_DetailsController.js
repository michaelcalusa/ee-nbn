({
	init: function(cmp, event, helper) {			
		console.log('==== Controller - init =====');		
		helper.loadYesNoOptions(cmp, event);
        helper.loadSiteDetails(cmp);
	},

    clickClearButton: function(cmp, event, helper) {			
    	console.log('==== Controller - clickClearButton =====');    	
		helper.clear(cmp, event);		
	},
	
    clickBackButton: function(cmp, event, helper) {			
    	console.log('==== Controller - clickBackButton =====');    	
		helper.goToPreviousPage(cmp, event);		
	},

    clickOrderSummaryButton: function(cmp, event, helper) {			
    	console.log('==== Controller - clickOrderSummaryButton =====');
    	helper.goToOrderSummaryPage(cmp, event);	
	},

    clickNextButton: function(cmp, event, helper) {			
    	console.log('==== Controller - clickNextButton =====');    	
		helper.goToNextPage(cmp, event);
	},

	onRequiredSelectChange: function(cmp, event, helper) {    	
		helper.validateRequiredSelect(cmp, event);
	},
})