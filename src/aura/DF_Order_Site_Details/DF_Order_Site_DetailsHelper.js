({    
    loadSiteDetails: function(cmp){

		
		var action = cmp.get("c.loadDFOrderSiteDetails");
        var location = cmp.get("v.location");
        var dfOrderId = location.OrderId;
        console.log('==== DF ORDER site details Helper ==== dfOrderId ' + dfOrderId); 
        action.setParams({
            dfOrderId: dfOrderId
			});
        
        action.setCallback(this, function(response) {
            var results = JSON.parse(response.getReturnValue());
            var state = response.getState();            
            if(state === "SUCCESS"){			
                cmp.set("v.attrTradingName", results["Trading Name"][0]);
        		cmp.set("v.attrHeritageSite", results["Heritage Site"][0]);
       			cmp.set("v.attrSiteDetailsNotes", results["Site Notes"][0]);
        		cmp.set("v.attrInductionRequired", results["Induction Required"][0]);
        		cmp.set("v.attrSecurityRequired", results["Security Required"][0]);
                location.SiteDetails = JSON.parse(response.getReturnValue());
                cmp.set("v.location", location );                				
            }  //do some error handling 
            else{
                console.log('loadSiteDetails '+state +'results' + JSON.stringify(results ))
            }
        });
        
		$A.enqueueAction(action);
		
    },
    
    clear: function(cmp, event) {
        console.log('==== Helper - clear =====');
        
		// Clear form fields
        cmp.set("v.attrTradingName", "");
        cmp.set("v.attrHeritageSite", "");
        cmp.set("v.attrSiteDetailsNotes", "");
        cmp.set("v.attrInductionRequired", "");
        cmp.set("v.attrSecurityRequired", "");
	},

    setSiteDetails: function(cmp, event){
		var action = cmp.get("c.setDFOrderSiteDetails");
        var location = cmp.get("v.location");
        var dfOrderId = location.OrderId;
          console.log('==== DF ORDER site details Helper ==== setSiteDetails orderId' + dfOrderId );
        action.setParams({
            dfOrderId: dfOrderId,
            tradingName: cmp.get("v.attrTradingName"),
            heritageSite: cmp.get("v.attrHeritageSite"),
            siteNotes: cmp.get("v.attrSiteDetailsNotes"),
            inductionReqd: cmp.get("v.attrInductionRequired"),
            securityReqd: cmp.get("v.attrSecurityRequired")
        });
        
        action.setCallback(this, function(response) {
            var list = response.getReturnValue();
            //do some error handling 
            console.log('==== DF ORDER site details Helper ==== setSiteDetails respose' + response );
        })
        $A.enqueueAction(action);
    },

    goToPreviousPage: function(cmp, event) {
        console.log('==== Helper - goToPreviousPage =====');
        
        //this.saveValidation(cmp,event);
       // if(cmp.get("v.saveValidationError"))
        //{
        //    return;
       // }

        console.log('Helper.goToPreviousPage - location: ' + cmp.get("v.location"));
        this.setSiteDetails(cmp, event);
        var evt = cmp.getEvent("orderContactDetailsPageEvent");  
        evt.setParams({"location" : cmp.get("v.location")}); // Set with its location attra
        evt.fire();        
	}, 

	goToOrderSummaryPage: function(cmp, event) {
        console.log('==== Helper - goToOrderSummaryPage =====');
       
        var location = cmp.get('v.location');				
		var evt = cmp.getEvent("quoteDetailPageEvent");        
		evt.setParams({
			"location": cmp.get("v.location")
		});
		evt.fire();
	},
	
    goToNextPage: function(cmp, event) {
        console.log('==== Helper - goToNextPage =====');

        this.saveValidation(cmp,event);
        if(!cmp.get("v.saveValidationError"))
        {
        
			this.setSiteDetails(cmp,event);
			console.log('Helper.goToNextPage - location: ' + cmp.get("v.location"));
        
			var evt = cmp.getEvent("orderInstallationDetailsPageEvent");    
			evt.setParams({"location" : cmp.get("v.location")}); // Set with its location attra
			evt.fire();
		}
		        
	}, 
	
	loadYesNoOptions: function (cmp, event) {
		console.log('==== Helper - loadYesNoOptions =====');
		
        var opts = [
            { value: "", label: "Select" },
			{ value: "Yes", label: "Yes" },
			{ value: "No", label: "No" }
         ];
        cmp.set("v.attrYesNoOptions", opts);
    },

    saveValidation: function(cmp,event) {	
		
		var tempCtrl = event.getSource();
		var fieldGroupName = "siteField"
		var ctrls = cmp.find(fieldGroupName);		
		
		var heritageSiteCtrl = ctrls.filter(this.getComponentByName("HeritageSite"))[0];
		var inductionRequiredCtrl = ctrls.filter(this.getComponentByName("InductionRequired"))[0]; 
		var securityRequiredCtrl = ctrls.filter(this.getComponentByName("SecurityRequired"))[0]; 

		this.validateSelectCmpEmpty(heritageSiteCtrl);
		this.validateSelectCmpEmpty(inductionRequiredCtrl);
		this.validateSelectCmpEmpty(securityRequiredCtrl);

		var allValid = this.isAllFieldsValid(cmp, fieldGroupName);

        if (!allValid)
		{
			cmp.set("v.saveValidationError",true);			
		}
        else cmp.set("v.saveValidationError",false);
		
		tempCtrl.focus();
    },
	
	validateRequiredSelect: function(cmp, event) {	
		var ctrl = event.getSource();		
		var tempCtrl = cmp.find("btnNext");

		this.validateSelectCmpEmpty(ctrl);

		var valid = ctrl.get("v.validity").valid;

		if (!valid) {            			
			this.reFocus(ctrl, tempCtrl);
        }
		
		event.getSource().focus();

		return valid;
	}


})