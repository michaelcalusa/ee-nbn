({
	onCancelOrder: function (cmp, event, helper) {
		cmp.set('v.isReminderVisiable', true);
	},

	cancelReminder: function (cmp, event, helper) {
		cmp.set('v.isReminderVisiable', false);
        helper.clearErrors(cmp);
	},

	confirmCancellation: function (cmp, event, helper) {		
		var promise1 = helper.apex(cmp, 'v.isCancellable', 'c.isCancellableOrder', {  orderId: cmp.get('v.orderId') }, false);
		
		promise1.then(function (result) {
			var isCancellable = cmp.get('v.isCancellable');
			var helper = result.sourceHelper;
			if (isCancellable) {				
				var promise2 = helper.apex(cmp, 'v.result', 'c.cancelOrder', {  orderId: cmp.get('v.orderId') }, false);
				return promise2;
			} else {				
				return Promise.reject({ sourceCmp: cmp, sourceHelper: helper, error: 'Not_Cancellable', sourceAction: '' });
			}

		}).then(function (result) {
			var helper = result.sourceHelper;
            if(result.data != undefined && result.data.length > 0)
            {                    
                helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
            }else
            {   
				cmp.set('v.isReminderVisiable', false);
				cmp.set('v.isCancellable', false);
				cmp.set('v.isCancellationDone', true);			
            }
		}).catch(function (result) {
			if(result.sourceCmp)
			{
				var cmp = result.sourceCmp;
				var helper = result.sourceHelper;
				if(result.error =='Not_Cancellable')
				{
					helper.setErrorMsg(cmp, "ERROR", cmp.get("v.customSettings.Order_Cancel_Not_Cancellable"), "BANNER");
				}else
				{					
					helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
				}
			}
        });
	},

	init: function (cmp, event, helper) {
		var promise1 = helper.apex(cmp, 'v.isCancellable', 'c.isCancellableOrder', {  orderId: cmp.get('v.orderId') }, false);
		var promise2 = helper.apex(cmp, 'v.customSettings', 'c.getASCustomSettings', { componentName: cmp.get('v.componentName') }, false);
		var promise3 = helper.apex(cmp, 'v.globalCustomSettings', 'c.getASCustomSettings', { componentName: cmp.get('v.globalCustomSettingComponentName') }, false);

        Promise.all([promise1, promise2, promise3])
            .then(function (result) { 				
	            var globalCustomSettings = cmp.get('v.globalCustomSettings');
				if(globalCustomSettings.Feature_Cancel_Order) cmp.set('v.isOrderCancelFeatureEnabled', true);
			    cmp.set('v.initDone', true);
            })
            .catch(function (result) {				
                var cmp = result.sourceCmp;
				var helper = result.sourceHelper;
                helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");				
            });
	}
})