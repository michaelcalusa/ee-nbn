({	 
	reload: function(cmp, event, helper) {			
        var a = cmp.get('c.init');
        $A.enqueueAction(a);
	},  

	init: function(cmp, event, helper) {		
		var action = cmp.get('c.getOrderNotifications');
        var ordId;
        if(cmp.get('v.selectedOrder.Id') == null || cmp.get('v.selectedOrder.Id') == '') {
            ordId = cmp.get('v.orderId');
        }
        else {
            ordId = cmp.get('v.selectedOrder.Id');
        }
        action.setParams({
            "orderId" : ordId
        });
        
        action.setCallback(this, function(result){
            var state = result.getState(); // get the response state
            
            if(state == 'SUCCESS') {                
                cmp.set('v.fullList', result.getReturnValue());
                cmp.set("v.currentList", result.getReturnValue());
                
                helper.renderPage(cmp);
            }
        });
        $A.enqueueAction(action);
	},
	 
	scriptsLoaded: function(cmp, event, helper) {
		/*if(cmp.get("v.loadSampleData"))
		{
			cmp.set("v.fullList", dfCommonStaticResourceLib.getSampleOrderNotificationData());
			helper.renderPage(cmp);
		}*/
        
	},

	renderPage: function(cmp, event, helper) {    
        helper.renderPage(cmp);
    },   
	
})