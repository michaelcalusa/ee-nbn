({
	renderPage: function(cmp) {
		
        var records = cmp.get("v.fullList"),	
        pageNumber = cmp.get("v.pageNumber"),
        pageCountVal = cmp.get("v.selectedCount"),

        pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));
		
        cmp.set("v.maxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
		cmp.set('v.currentList', []);
        cmp.set("v.currentList", pageRecords);
        
    },	
})