({	
	updateColumnSorting: function (cmp, event, helper) {
		helper.updateColumnSortingWithSubProperty(cmp, event, helper);
    },

    searchTermChange: function (cmp, event, helper) {
        //Do search if enter key is pressed.
        if (event.getParams().keyCode == 13) {
            cmp.set('v.isTriggerdBySearch', true);
			helper.getOrderRecords(cmp);            
        }
    },

    onDateChange: function (cmp, event, helper) {
        helper.validateDate(cmp);
    },

    search: function (cmp, event, helper) {
        cmp.set('v.isTriggerdBySearch', true);		
		helper.getOrderRecords(cmp);        
    },

    clear: function (cmp, event, helper) {
        helper.clearErrors(cmp);
        cmp.set('v.isTriggerdBySearch', false);
        cmp.set('v.searchTerm', '');
        cmp.set('v.selectedStatus', '');
        cmp.set('v.selectedDate', '');
        cmp.set('v.dateValidationError', false);
		helper.getOrderRecords(cmp);        
        helper.toogleClearSearchButton(cmp, true);
    },

    clickOrderiLink: function (cmp, event, helper) {
		
		var fullList = cmp.get('v.fullList');
		var orderId = event.target.getAttribute("data-Id");

		var selectedOrder = fullList.filter(function(item){ return item.Id == orderId});		
		cmp.set('v.selectedOrder', selectedOrder[0]);
        cmp.set('v.isOrderDetailsOpen', true);        
    },
    
    toggleIsOrderDetailsOpen: function (cmp, event, helper) {
		var params = event.getParam('arguments');
		if (params) {
			cmp.set('v.isOrderDetailsOpen', params.isOrderDetailsOpen);
		}
    },	

    toggleShowModifySuccess: function (cmp, event, helper) {
        var params = event.getParam('arguments');
        if (params) {
            cmp.set('v.showConfirmationModify', params.showConfirmationModify);
        }
    },

    init: function (cmp, event, helper) {       

		//Check if url parameter exists  
		
		var orderIdParam = helper.getUrlParameter(cmp, helper.ORDER_ID_PARAM_NAME);
		var parentOppIdParam = helper.getUrlParameter(cmp, helper.PARENT_OPP_ID_PARAM_NAME);
        
        if (orderIdParam != null && parentOppIdParam != null) {
			cmp.set('v.orderIdParam', orderIdParam);
			cmp.set('v.parentOppIdParam', parentOppIdParam);

			helper.apex(cmp, "v.pageUrlSFOrderSummary", 'c.getCustomSetting', { settingName : helper.CUSTOM_SETTING_PAGE_URL_SF }, false)
			.then(function(result) {
				var backUrl = cmp.get('v.pageUrlSFOrderSummary') + '?' + helper.PARENT_OPP_ID_PARAM_NAME + '=' + parentOppIdParam + '&page=ordersummary'
				cmp.set('v.backUrl', backUrl);
				cmp.set('v.isOrderDetailsOpen', true);
				cmp.set('v.isInitDone', true); 
			})
        } 
		else {
            helper.loadStatusValues(cmp);
			cmp.set('v.isInitDone', true); 
		}
    },

    renderPage: function (cmp, event, helper) {
        helper.renderPage(cmp);
    },    
})