({
    ERR_MSG_ERROR_OCCURRED: 'Error occurred. Error message: ',
    ERR_MSG_UNKNOWN_ERROR: 'Error message: Unknown error',
    MAX_COUNT: '5000',
	INVALID_DATE_ERROR: 'Invalid Date',
	DEFAULT_DROPDOWN_OPTION: 'Select',

    loadStatusValues : function(cmp) {		
		var action = cmp.get("c.getPickListValuesIntoList");
		
		action.setCallback(this, function(response) {
        var state = response.getState();		
            if(state === "SUCCESS") {
				this.clearErrors(cmp);
				var records = JSON.parse(response.getReturnValue());    
				            
				cmp.set('v.statusOptions', records.map(function (item) {
					return {
						        label: item,
                                value: item
                            };
                    }));

				this.getOrderRecords(cmp);
	        }
            else{
				var errors = response.getError();
				if (errors) {
                    if (errors[0] && errors[0].message) {
                    	console.log(this.ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                	console.log(this.ERR_MSG_UNKNOWN_ERROR);                 
                }

				var errorLabel = $A.get("$Label.c.DF_Application_Error");

				cmp.set("v.responseStatus", state);
                cmp.set("v.messageType","Banner");				
                cmp.set("v.responseMessage", errorLabel);				
            }
        });
        $A.enqueueAction(action);
	},

    getOrderRecords : function(cmp) {		
		var isTriggerBySearch = cmp.get('v.isTriggerdBySearch');
		var searchTermVal = cmp.get('v.searchTerm');
        var searchDateVal = cmp.get('v.selectedDate');
		
		if (isTriggerBySearch) this.toogleClearSearchButton(cmp, false);
			
        var searchStatusVal = (cmp.get('v.selectedStatus') == "" || cmp.get('v.selectedStatus') == "Select") ? null : cmp.get('v.selectedStatus');
		var action = cmp.get("c.getRecords");
		
		action.setParams({"searchTerm": searchTermVal,"searchDate": searchDateVal,"searchStatus": searchStatusVal, "isTriggerBySearch": Boolean(isTriggerBySearch)});
		action.setCallback(this, function(response) {
        
		var state = response.getState();		
        	if(state === "SUCCESS") {
				this.clearErrors(cmp);
                var records = JSON.parse(response.getReturnValue());                
				cmp.set('v.fullList', records);
				cmp.set('v.currentList', records);				
				cmp.set("v.pageNumber", 1);
                var pageCountVal = cmp.get("v.selectedCount");
				cmp.set("v.maxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
				this.renderPage(cmp);
	        }
            else{
				var errors = response.getError();
				if (errors) {
                    if (errors[0] && errors[0].message) {
                    	console.log(this.ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                	console.log(this.ERR_MSG_UNKNOWN_ERROR);                 
                }

				var errorLabel = $A.get("$Label.c.DF_Application_Error");

				cmp.set("v.responseStatus", state);
                cmp.set("v.messageType","Banner");				
                cmp.set("v.responseMessage", errorLabel);				
            }
        });
        $A.enqueueAction(action);
	},    

    renderPage: function (cmp) {

        var records = cmp.get("v.fullList"),

            pageNumber = cmp.get("v.pageNumber"),
            pageCountVal = cmp.get("v.selectedCount"),
            pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));

        cmp.set('v.currentList', []);
        cmp.set("v.currentList", pageRecords);
    },        

    toogleClearSearchButton: function (cmp, disabled) {
        cmp.set('v.isClearSearchButtonDisabled', disabled);
    },

	validateDate: function(cmp) {
		var isValid = true;
		
		var dateReg = /^\d{4}[./-]\d{1,2}[./-]\d{2}$/;

		var selectedDate = cmp.get("v.selectedDate");
		var orderDateInputField = cmp.find("orderDate");
		
		if(!selectedDate) return isValid;

		orderDateInputField.focus();		

        if(selectedDate && !selectedDate.trim().match(dateReg)){
			cmp.set("v.dateValidationError" , true);
			cmp.set("v.dateValidationErrorMsg" , this.INVALID_DATE_ERROR);			
            isValid = false;
        }else
		{
			if(new Date(selectedDate.split('/').reverse().join('/')) == this.INVALID_DATE_ERROR)
			{
				cmp.set("v.dateValidationError" , true);
				cmp.set("v.dateValidationErrorMsg" , this.INVALID_DATE_ERROR);			
				isValid = false;
			}else
			{
				cmp.set("v.dateValidationError" , false);
			}
		}

		return isValid;
	}	
})