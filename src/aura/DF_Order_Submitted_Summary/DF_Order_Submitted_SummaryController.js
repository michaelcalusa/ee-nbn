({
    //CPST-8240
    handleTabIdChange: function(cmp, event, helper) {       
        var currentTabId = cmp.get('v.currentTabId');
        if (currentTabId == '1') {
            var a = cmp.get('c.init');
            $A.enqueueAction(a);
        } else if (currentTabId == '2') {
            var notificationListCmp = cmp.find("notificationList");
            if (notificationListCmp) {
                notificationListCmp.reload();
            }
        }
    },

	init: function(cmp, event, helper) {
        var orderId;
        
        if(cmp.get('v.selectedOrder.Id') != undefined && cmp.get('v.selectedOrder.Id') != null && cmp.get('v.selectedOrder.Id') != '') 
	       	orderId = cmp.get('v.selectedOrder.Id');

        var promise0 = helper.apex(cmp, 'v.customSettings', 'c.getASCustomSettings', { componentName: cmp.get('v.componentName') }, false);
		var promise1 = helper.apex(cmp, 'v.ordersummary', 'c.getOrderSummaryDetails', {  orderId: orderId }, false);		
		var promise2 = helper.apex(cmp, 'v.location', 'c.getLocationDetails', {  orderId: orderId }, false);		
		var promise3 = helper.apex(cmp, 'v.businessContactList', 'c.getBizContacts', {  orderId: orderId }, false);		
		var promise4 = helper.apex(cmp, 'v.siteContactList', 'c.getSiteContacts', {  orderId: orderId }, false);		
		var promise5 = helper.apex(cmp, 'v.ovcList', 'c.getOVCDetails', {  orderId: orderId }, false);		
		var promise6 = helper.apex(cmp, 'v.NTD', 'c.getNTDDetails', {  orderId: orderId }, false);		
        var promise7 = helper.apex(cmp, 'v.UNI', 'c.getUNIDetails', {  orderId: orderId }, false);
        var promise8 = helper.apex(cmp, 'v.toggleAfterBH', 'c.getCustomSetting', { settingName: 'TOGGLE_AFTERBH_ON'}, false);//CPST-7018

        Promise.all([promise0, promise1, promise2, promise3, promise4, promise5, promise6, promise7, promise8])
            .then(function (result) {				
			    cmp.set('v.initDone', true);
				
                var sortedOVCList = cmp.get('v.ovcList');
                sortedOVCList = sortedOVCList.sort(helper.sortOVCForDisplay);
                cmp.set('v.ovcList',sortedOVCList);
                cmp.set('v.selectedOvc', cmp.get('v.ovcList')[0]);
            })
            .catch(function (result) {				
                var cmp = result.sourceCmp;
				var helper = result.sourceHelper;				
                helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
            });
	},
    
	clickBack: function(cmp, event, helper) {
        var ref = cmp.get("v.backUrl");
        window.open(cmp.get("v.backUrl"), '_top');
	},
		
	returnToSearch: function(cmp, event, helper) {	
		cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.showConfirmationModify', false);
	},

	backToQuotePage: function(cmp, event, helper) {	
        var quotePageEvt = cmp.getEvent("quoteDetailPageEvent");      
		quotePageEvt.setParam("batchId", cmp.get("v.parentOppIdParam")); 
        quotePageEvt.fire();
	},

	backToSource: function(cmp, event, helper) {	
		var sourceDisplayedDetailItem = cmp.get('v.sourceDisplayedDetailItem');
		cmp.set('v.displayedDetailItem', sourceDisplayedDetailItem);		
	},

	handleCancellationDone: function(cmp, event, helper) {	
		helper.setBannerMsg(cmp, "OK", cmp.get('v.customSettings.Order_Cancel_Success'));
		var childCmp = cmp.find('locationSummaryCmp');
		if(childCmp) childCmp.set('v.isStatusCancelInitiatedTriggerFromUI', true);

		var scrollOptions = {
            left: 0,
            top: 0,
            behavior: 'smooth'
        }
        window.scrollTo(scrollOptions);
	},

})