({
       
    sortOVCForDisplay: function (a, b) {
        if(typeof a.ovcId  != undefined && typeof b.ovcId  != undefined 
          	&& a.ovcId  != null &&  b.ovcId  != null){
            if (a.ovcId   < b.ovcId ){
                return -1;
            }
            if (a.ovcId   > b.ovcId  ){
                return 1;
            }
        } else {
            if (a.id  < b.id ){
                return -1;
            }
            if (a.id  > b.id ){
                return 1;
            }
        }
        return 0;
    },
})