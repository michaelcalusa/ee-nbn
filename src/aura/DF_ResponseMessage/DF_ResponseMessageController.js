({
	navigateToInternalObjectDetailPage: function (cmp, event, helper) {			
		var sObectEvent = $A.get("e.force:navigateToSObject");
		sObectEvent .setParams({
			"recordId": cmp.get('v.sObjectRecordId'),
			"slideDevName": "detail"
		});
		sObectEvent.fire();
	},

	init: function (cmp, event, helper) {
	
	},
})