({
    doSave: function(component, event, helper) {
		var cmpTarget = component.find('inputContainer');
		$A.util.removeClass(cmpTarget, 'slds-has-error');
		component.set("v.showErrorMessage", false); 
        if (component.find("fileId").getElement().files.length > 0) {
			console.log(component.find("fileId").getElement().files[0].name);
			if(!component.find("fileId").getElement().files[0].name.toLowerCase().endsWith("csv")){
				//Show validation message for file format;
				component.set("v.showErrorMessage", true); 
				$A.util.addClass(cmpTarget, 'slds-has-error');
				component.set("v.errorMessage", "Invalid file format");
			}else{
				helper.uploadHelper(component, event);
			}
            
        } else {			
            //Show validation message for missing file;
			component.set("v.showErrorMessage", true); 
			$A.util.addClass(cmpTarget, 'slds-has-error');
			component.set("v.errorMessage", "Complete this field");
		}
    },
	downloadfile : function (component, event, helper){ 
        
        var action = component.get("c.downloadAttachment");
		
        // set call back 
        action.setCallback(this, function(response) {
             
            var state = response.getState();
            if (state === "SUCCESS") {                
                var urlVal = response.getReturnValue();
                console.log('==== baseurl ====='+urlVal);
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": urlVal
                });
                urlEvent.fire();
                // handle the response errors        
            } else if (state === "ERROR") {
				component.set("v.responseStatus", state);
                component.set("v.type","Banner");				
                component.set("v.responseMessage", errorLabel);				
            }
        });
        // enqueue the action
        $A.enqueueAction(action);
    },
    cancel: function(component, event, helper) {
        //perform cancel operation
		component.find("fileId").getElement().value = "";
        helper.toggleError(component, false);
		component.set("v.responseStatus", "");
        component.set("v.type","");				
        component.set("v.responseMessage", "");     
    }
})