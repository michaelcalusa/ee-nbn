({
    MAX_FILE_SIZE: 204800, //Max file size 200 KB 
    CHUNK_SIZE: 204800,    //Chunk Max size 200 KB. Will be helpful when this size increases in future 

    toggleError: function(component, show)
	{	
		if(show == false)
		{
			component.set("v.showErrorMessage", false); 
			var cmpTarget = component.find('inputContainer');
			$A.util.removeClass(cmpTarget, 'slds-has-error');
		}else{
			component.set("v.showErrorMessage", true); 
			var cmpTarget = component.find('inputContainer');
			$A.util.addClass(cmpTarget, 'slds-has-error');
		}
	},

    uploadHelper: function(component, event) {
		
        var errorFileSizeLabel = $A.get("$Label.c.DF_File_Size_Too_Large_Error");
        // start/show the loading spinner   
        component.set("v.showLoadingSpinner", true);
        // get the selected files using aura:id [return array of files]
        var fileInput = component.find("fileId").getElement();
    	
        // get the first file using array index[0]  
        var file = fileInput.files[0];
        
        var self = this;
        // check the selected file size, if select file size greter then MAX_FILE_SIZE,
        // then show a alert msg to user,hide the loading spinner and return from function  
        if (file.size > self.MAX_FILE_SIZE) {            
			self.toggleError(component, true);
            component.set("v.errorMessage", errorFileSizeLabel);
            return;
        }
        
        // create a FileReader object 
        var objFileReader = new FileReader();
        // set onload function of FileReader object   
        objFileReader.onload = $A.getCallback(function() {
            var fileContents = objFileReader.result;
            var base64 = 'base64,';
            var dataStart = fileContents.indexOf(base64) + base64.length;
            fileContents = fileContents.substring(dataStart);
            // call the uploadProcess method 
            self.uploadProcess(component, file, fileContents, event);
        });
        
        objFileReader.readAsDataURL(file);        
    },
    
    uploadProcess: function(component, file, fileContents, event) {
        // set a default size or startpostiton as 0 
        var startPosition = 0;
        // calculate the end size or endPostion using Math.min() function which is return the min. value   
        var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
        // start with the initial chunk, and set the attachId(last parameter)is null in begin
        this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '', event);
    },    
    goToNextPage: function(component, event) {
        console.log('==== Helper - goToNextPage =====');
        
        var opEvt = component.getEvent("oppBundleEvent");    
        var OppId = component.get("v.parentOppBundleId");       
        opEvt.setParams({"oppBundleId" : OppId });
        opEvt.fire();     
	},
    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId, event) {
        
        var errorLabel = $A.get("$Label.c.DF_Application_Error");
		// call the apex method 'saveChunk'
        var getchunk = fileContents.substring(startPosition, endPosition);
        var action = component.get("c.saveChunk");
		action.setParams({
            fileName: file.name,
            base64Data: encodeURIComponent(getchunk),
            contentType: file.type,
            fileId: attachId
        });
        
        // set call back 
        action.setCallback(this, function(response) {
            // store the response / Attachment Id   
            var state = response.getState();
            if (state === "SUCCESS") {
                /*// update the start position with end postion for change in chunk size for larger files in future
                startPosition = endPosition;
                endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
                // check if the start postion is still less then end postion 
                // then call again 'uploadInChunk' method , 
                // else, diaply alert msg and hide the loading spinner
                if (startPosition < endPosition) {
                    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, attachId);
                } */                
                var responseOpptId = response.getReturnValue();
                component.set("v.parentOppBundleId", responseOpptId);
                console.log('==== Helper - parentOppBundleId ====='+responseOpptId);
                this.goToNextPage(component, event); 
                // handle the response errors        
            } else if (state === "ERROR") {
				component.set("v.responseStatus", state);
                component.set("v.type","Banner");				
                component.set("v.responseMessage", errorLabel);				
            }
        });
        // enqueue the action
        $A.enqueueAction(action);
    }
})