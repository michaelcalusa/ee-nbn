({	
	searchTermChange: function(cmp, event, helper) {		
		//Do search if enter key is pressed.
		if(event.getParams().keyCode == 13) {
			cmp.set('v.isTriggerdBySearch', true);
			helper.doSearch(cmp, event);				
		}
	},

	search: function(cmp, event, helper) {
		cmp.set('v.isTriggerdBySearch', true);
		helper.doSearch(cmp, event, helper);
	},
    
	clear: function(cmp, event, helper) {			   
		helper.clearErrors(cmp);
		cmp.set('v.isTriggerdBySearch', false);
		helper.getRecords(cmp);
		cmp.set('v.searchTerm', '');
		helper.disableClearSearchButton(cmp, true);
	},	

	clickLink: function(cmp, event, helper) {		
		var bundleId = event.target.getAttribute("data-Id");	
		var locCount = event.target.getAttribute("data-locCount");	
		var eventToFire;
		if(locCount > 0){
			eventToFire = cmp.getEvent("siteDetailPageEvent");
			eventToFire.setParams({"oppBundleId" : bundleId });
		}else{
			eventToFire = cmp.getEvent("oppBundleEvent");
			eventToFire.setParams({"oppBundleId" : bundleId });
		}
        
        eventToFire.fire();
    },	

    init: function (cmp, event, helper) {
    	cmp.set('v.isTriggerdBySearch', false);
        
        //init feature toggle & placeholder text
        helper.apex(cmp, 'v.isArgoEnhancementsEnabled', 'c.isArgoEnhancementsEnabled', {}, false)
            .then(function(result) {
                if (result.data) {
                    cmp.set("v.searchTermPlaceholder", "Search by Bundle ID, Quote ID, LOC ID");
                } else {
                    cmp.set("v.searchTermPlaceholder", "Search by Enterprise Ethernet Bundle ID");
                }
            }).catch(function(result) {
            	cmp.set("v.isArgoEnhancementsEnabled", false);
				cmp.set("v.searchTermPlaceholder", "Search by Enterprise Ethernet Bundle ID");
			});
        helper.getRecords(cmp);		
    },    
	
	renderPage: function(cmp, event, helper) {    
        helper.renderPage(cmp);
    },   

	updateColumnSorting: function(cmp, event, helper) {

		var sortedByColumn = cmp.get('v.sortedByColumn');
		var sortedDirection = cmp.get('v.sortedDirection'); 
		var clickedColumnName = event.target.getAttribute("data-column");
		var asc = 'ascending';
		var desc = 'decending';

		if(clickedColumnName == sortedByColumn){
			if(sortedDirection == asc)
				cmp.set('v.sortedDirection', desc);
			else	
				cmp.set('v.sortedDirection', asc);
		}else
		{
			cmp.set('v.sortedByColumn', clickedColumnName);
			cmp.set('v.sortedDirection', desc);
		}

		helper.sortListData(cmp, clickedColumnName, sortedDirection); 
				
	},
})