({
	ERR_MSG_ERROR_OCCURRED: 'Error occurred. Error message: ',
	ERR_MSG_UNKNOWN_ERROR: 'Error message: Unknown error',     
	MAX_COUNT: '5000', 

    doSearch: function(cmp, event, helper){
		
		this.disableClearSearchButton(cmp, false); 
		cmp.set("v.pageNumber", 1);

		var searchTerm = cmp.get('v.searchTerm').trim();

            var isTriggerBySearch = cmp.get('v.isTriggerdBySearch');
            if ( $A.util.isEmpty(searchTerm) ) {
                isTriggerBySearch = false;
            }
            var action = cmp.get("c.getRecords");

            action.setParams({"searchTerm": searchTerm, "isTriggerBySearch": Boolean(isTriggerBySearch)});
            action.setCallback(this, function (response) {

                var state = response.getState();

                if (state === "SUCCESS") {

                    this.clearErrors(cmp);

                    var records = JSON.parse(response.getReturnValue());
                    cmp.set('v.fullList', records);

                    var pageCountVal = cmp.get("v.selectedCount");
                    cmp.set("v.maxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));

                    this.renderPage(cmp);
                }
                else {

                    var errors = response.getError();

                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log(this.ERR_MSG_ERROR_OCCURRED + errors[0].message);
                        }
                    } else {
                        console.log(this.ERR_MSG_UNKNOWN_ERROR);
                    }

                    var errorLabel = $A.get("$Label.c.DF_Application_Error");

                    cmp.set("v.responseStatus", state);
                    cmp.set("v.messageType", "Banner");
                    cmp.set("v.responseMessage", errorLabel);
                    cmp.set('v.fullList', []);
                    cmp.set('v.currentList', []);

                }
            });
            $A.enqueueAction(action);
	},

	renderPage: function(cmp) {

        var records = cmp.get("v.fullList"),

        pageNumber = cmp.get("v.pageNumber"),
        pageCountVal = cmp.get("v.selectedCount"),
        pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));

		cmp.set('v.currentList', []);
        cmp.set("v.currentList", pageRecords);
    },	

	getRecords : function(cmp) {
			
		var isTriggerBySearch = cmp.get('v.isTriggerdBySearch');

		var action = cmp.get("c.getRecords");
		
		action.setParams({"searchTerm": '', "isTriggerBySearch": Boolean(isTriggerBySearch)});
		action.setCallback(this, function(response) {
        
		var state = response.getState();
		
        	if(state === "SUCCESS") {
				
				this.clearErrors(cmp);

                var records = JSON.parse(response.getReturnValue());
                cmp.set('v.fullList', records);
				cmp.set('v.currentList', records);

                var pageCountVal = cmp.get("v.selectedCount");

				cmp.set("v.maxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));

				this.renderPage(cmp);
	        }
            else{

                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                    	console.log(this.ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                	console.log(this.ERR_MSG_UNKNOWN_ERROR);                 
                }

				var errorLabel = $A.get("$Label.c.DF_Application_Error");

				cmp.set("v.responseStatus", state);
                cmp.set("v.messageType","Banner");				
                cmp.set("v.responseMessage", errorLabel);				
            }
        });
        $A.enqueueAction(action);
	},

	disableClearSearchButton: function(cmp, disabled){
		cmp.set('v.isClearSearchButtonDisabled', disabled);
	},

	sortListData: function(cmp, columnName, sortDirection) {
		var asc = 'ascending';

        var data = cmp.get("v.currentList");
        var reverse = sortDirection !== asc;

        data.sort(this.sortBy(columnName, reverse))
        cmp.set("v.currentList", data);
	},

	 sortBy: function(field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};

        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
})