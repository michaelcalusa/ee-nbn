({     
    // Perform API call
	searchByLocationID : function(cmp, event) {
        console.log('==== Helper - searchByLocationID =====');

        var ERR_MSG_APP_ERROR = cmp.get("v.attrAPP_ERROR");
		var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
		var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';     
        
        // Get values from comp search attributes
        var locationID = cmp.get("v.attrLocationID");
        
        // Create the action
		var action = cmp.get("c.searchByLocationID"); 
        
        // Set apex controller action inputs
        action.setParams({
            'locId': locationID
        });          
        
        var responseOpptId;
        
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('Helper.searchByLocationID - state: ' + state);
            
            if (state === "SUCCESS") {
                responseOpptId = response.getReturnValue();
                console.log('Helper.searchByLocationID - Successful response received - responseOpptId: ' + responseOpptId);                
                
                cmp.set("v.parentOppId", responseOpptId);
                
                // Go to next page
                this.goToNextPage(cmp, event);
            } else {
                console.log('Helper.searchByLocationID - Failed response with state: ' + state);

                // Set error response comp with generic application error        
    			this.setErrorMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "Banner");
                
                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                    	console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                	console.log(ERR_MSG_UNKNOWN_ERROR);                 
                }                   
            }
        });
    
        // Send action off to be executed
        $A.enqueueAction(action);
	},

    // Perform API call
	searchByAddress : function(cmp, event) {
        console.log('==== Helper - searchByAddress =====');

        var ERR_MSG_APP_ERROR = cmp.get("v.attrAPP_ERROR");
		var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
		var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

        // Get values from comp search attributes
        var state = cmp.get("v.attrState");
		var postcode = cmp.get("v.attrPostcode");
		var suburbLocality = cmp.get("v.attrSuburbLocality");        
		var streetName = cmp.get("v.attrStreetName");   
		var streetType = cmp.get("v.attrStreetType");
		var streetLotNumber = cmp.get("v.attrStreetLotNumber");        
		var unitType = cmp.get("v.attrUnitType");
		var unitNumber = cmp.get("v.attrUnitNumber");
		var level = cmp.get("v.attrLevel");        
		var complexSiteName = cmp.get("v.attrComplexSiteName");
		var complexBuildingName = cmp.get("v.attrComplexBuildingName");
		var complexStreetName = cmp.get("v.attrComplexStreetName");        
		var complexStreetType = cmp.get("v.attrComplexStreetType");   
		var complexStreetNumber = cmp.get("v.attrComplexStreetNumber");                      
        
        // Create the action
		var action = cmp.get("c.searchByAddress"); 
        
        // Set apex controller action inputs
        action.setParams({
            'state': state,
            'postcode': postcode,
            'suburbLocality': suburbLocality,
            'streetName': streetName,
            'streetType': streetType,
            'streetLotNumber': streetLotNumber,
            'unitType': unitType,
            'unitNumber': unitNumber,
            'level': level,            
            'complexSiteName': complexSiteName,
            'complexBuildingName': complexBuildingName,
            'complexStreetName': complexStreetName,
            'complexStreetType': complexStreetType,
            'complexStreetNumber': complexStreetNumber
        });          
        
        var responseOpptId;
        
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
			console.log('Helper.searchByAddress - state: ' + state);
            
            if (state === "SUCCESS") {
                responseOpptId = response.getReturnValue();
                console.log('Helper.searchByAddress - Successful response received - responseOpptId: ' + responseOpptId);                
                
                cmp.set("v.parentOppId", responseOpptId);
                
                // Go to next page
                this.goToNextPage(cmp, event);
            } else {
                console.log('Helper.searchByAddress - Failed response with state: ' + state);

                // Set error response comp with generic application error        
    			this.setErrorMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "Banner");
                
                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                    	console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                	console.log(ERR_MSG_UNKNOWN_ERROR);                 
                }                   
            }
        });
    
        // Send action off to be executed
        $A.enqueueAction(action);
        console.log('==== Helper.searchByAddress - enqueuedAction....');
	},    
    
    // Perform API call
	searchByLatLong : function(cmp, event) {
        console.log('==== Helper - searchByLatLong =====');

        var ERR_MSG_APP_ERROR = cmp.get("v.attrAPP_ERROR");
		var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
		var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

        // Get values from comp search attributes
        var latitude = cmp.get("v.attrLatitude");
		var longitude = cmp.get("v.attrLongitude");
        
		console.log('Helper.searchByLatLong - latitude: ' + latitude);
		console.log('Helper.searchByLatLong - longitude: ' + longitude);
		
        // Create the action
		var action = cmp.get("c.searchByLatLong"); 
        
        // Set apex controller action inputs
        action.setParams({
            'latitude': latitude,
            'longitude': longitude
        });          
        
        var responseOpptId;
        
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
			console.log('Helper.searchByLatLong - state: ' + state);
            
            if (state === "SUCCESS") {
                responseOpptId = response.getReturnValue();
                console.log('Helper.searchByLatLong - Successful response received - responseOpptId: ' + responseOpptId);                
                
                cmp.set("v.parentOppId", responseOpptId);
                
                // Go to next page
                this.goToNextPage(cmp, event);
            } else {
                console.log('Helper.searchByLatLong - Failed response with state: ' + state);

                // Set error response comp with generic application error        
    			this.setErrorMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "Banner");
                
                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                    	console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                	console.log(ERR_MSG_UNKNOWN_ERROR);                 
                }                   
            }
        });
    
        // Send action off to be executed
        $A.enqueueAction(action);
	},
    
    goToNextPage: function(cmp, event) {
        console.log('==== Helper - goToNextPage =====');
        
        var opEvt = cmp.getEvent("oppBundleEvent");    
        var OppId = cmp.get("v.parentOppId");       
        opEvt.setParams({"oppBundleId" : OppId });
        opEvt.fire();     
	}, 
    
    validateLocationIDSearchInput : function(cmp, locationID) {
        console.log('==== Helper - validateLocationID =====');
		var isValid = true;
        var LOC_PREFIX = 'LOC';
        var ERR_MSG_BADINPUT = 'Enter a valid value. '
        var ERR_MSG_LOC_PREFIX_MISSING = ERR_MSG_BADINPUT + 'Location ID must be in the following format e.g. LOC123456789012';                
        var inputStrLength = locationID.length;
        
        if (inputStrLength != 15) {
			isValid = false;
        } else {
        	var UPPERCASE_LOCATION_ID = locationID.toUpperCase();        	

            if (!UPPERCASE_LOCATION_ID.startsWith(LOC_PREFIX, 0)) {
                // LOC prefix not found 
                isValid = false;
                cmp.find("locationID").set('v.validity', {valid:false, badInput:true});
                
                // Set field control error msg
                cmp.find("locationID").set('v.messageWhenBadInput', ERR_MSG_LOC_PREFIX_MISSING);
            } else {               
                // Get last 12 chars of LOCID
                var locDigits = locationID.substr(3, 12);
                console.log('==== Helper.validateLocationIDSearchInput - locDigits: ' + locDigits);
                
                // Validate if last 12 chars of LOCID are numeric
                if (isNaN(locDigits)) {
                    // Some non numerics found in last 12 chars of LOCID
                    isValid = false; 
                    cmp.find("locationID").set('v.validity', {valid:false, badInput:true});
                    
                    // Set field control error msg
                    cmp.find("locationID").set('v.messageWhenBadInput', ERR_MSG_LOC_PREFIX_MISSING);                    
                }           
            }            
        }
        
        console.log('Helper.validateLocationIDSearchInput - isValid: ' + isValid);
        
        return isValid;
	},

	validateAddressSearchInput : function(cmp) {
        console.log('==== Helper - validateAddressSearchInput =====');
		var isValid = true;

        // Get values from  inputs
        var postcode = cmp.get("v.attrPostcode");
        var level = cmp.get("v.attrLevel");
		
        // Call all validate methods for each input field - throw error if any issues with any fields
        if (isValid) {
            if (!this.validateState(cmp)) {
                isValid = false;
            }
            
            if (postcode !== null && postcode !== '' && typeof postcode != 'undefined') {
                if (!this.validatePostcode(cmp, postcode)) {
                    isValid = false;
                }            	
            }        

            if (!this.validateSuburbLocality(cmp)) {
                isValid = false;
            }   
            
            if (!this.validateStreetName(cmp)) {
                isValid = false;
            }
            
            if (!this.validateStreetType(cmp)) {
                isValid = false;
            }     
            
            if (!this.validateStreetLotNumber(cmp)) {
                isValid = false;
            }
            
            if (!this.validateUnitType(cmp)) {
                isValid = false;
            }   
            
            if (!this.validateUnitNumber(cmp)) {
                isValid = false;
            }       
            
            if (level !== null && level !== '' && typeof level != 'undefined') {            	
                if (!this.validateLevel(cmp, level)) {
                    isValid = false;
                }            	
            }
            
            if (!this.validateComplexSiteName(cmp)) {
                isValid = false;
            }  
            
            if (!this.validateComplexBuildingName(cmp)) {
                isValid = false;
            }     
            
            if (!this.validateComplexStreetName(cmp)) {
                isValid = false;
            }       

            if (!this.validateComplexStreetType(cmp)) {
                isValid = false;
            }  
            
            if (!this.validateComplexStreetNumber(cmp)) {
                isValid = false;
            }            
        }
          
        console.log('Helper.validateAddressSearchInput - isValid: ' + isValid);
        
        return isValid;		
	},

	validateAddressSearchInputMandatoryFields : function(cmp, streetLotNumber, streetName, suburbLocality, state, postcode) {
        console.log('==== Helper - validateAddressSearchInputMandatoryFields =====');

        var ERR_MSG_VALUE_MISSING = 'Complete this field. ';
        var ERR_MSG_MISSING_STATE_OR_POSTCODE = ERR_MSG_VALUE_MISSING + 'State or Postcode field must be entered.';      
        
        var isValid = true;
        
        console.log('Helper.validateAddressSearchInputMandatoryFields - streets: ' + streetLotNumber);
        console.log('Helper.validateAddressSearchInputMandatoryFields - state: ' + state);
        console.log('Helper.validateAddressSearchInputMandatoryFields - postcode: ' + postcode);
        
    	if ((streetLotNumber == undefined || streetLotNumber == "") 
    			|| (streetName == undefined || streetName == "") 
    				|| (suburbLocality == undefined || suburbLocality == "")) {
    		this.showErrorRequiredFields(cmp, 'streetLotNumber');
    		this.showErrorRequiredFields(cmp, 'streetName');
    		this.showErrorRequiredFields(cmp, 'suburbLocality');
            isValid = false;                    
        } else {            
            if ((state == undefined || state == "") && (postcode == undefined || postcode == "")) {
            	$A.util.addClass(cmp.find("postCodeStateErr"),"show");
				isValid = false;
            } else {
            	$A.util.removeClass(cmp.find("postCodeStateErr"), "show");
            }            
        }   
        
        
        console.log('Helper.validateAddressSearchInputMandatoryFields - isValid: ' + isValid);
        
        return isValid;
	},     
	
	showErrorRequiredFields: function(cmp, field)	{
		var requiredFields = cmp.find(field);
		requiredFields.showHelpMessageIfInvalid();
	},

	validateLatLongSearchInput : function(cmp) {
        console.log('==== Helper - validateLatLongSearchInput =====');
		var isValid = false;        
        //var compFieldsValid = cmp.find("latLong").checkValidity();
        var lat = cmp.get('v.attrLatitude');
        var long = cmp.get('v.attrLongitude');
        var ERROR_MSG_LAT_INVALID = 'Latitude should be a decimal number in a range [-90, 90]';
        var ERROR_MSG_LONG_INVALID = 'Longitude should be a decimal number in a range [-180, 180]';
        //if (!compFieldsValid) {
		//	isValid = false;            
        //}
        var cntValidLat = 0;
        var cntValidLong = 0;
        debugger;
        console.log('lat:: ' + lat);
        console.log('long:: ' + long);
        if ((lat == undefined || lat == "") && (long == undefined || long == "")) {
        	this.showErrorRequiredFields(cmp, 'latitude');
        	this.showErrorRequiredFields(cmp, 'longitude');
        	cmp.set("v.errorMessageLat", "");
            cmp.set("v.errorMessageLong", "");
        } else {
        	if (lat != "" && (lat <= 90 && lat >= -90)) {
        		cntValidLat++;            	
            	$A.util.removeClass(cmp.find("latError"), "show");
            }
        	if (long != "" && (long <= 180 && long >= -180)) {
            	cntValidLong++;
            	$A.util.removeClass(cmp.find("longError"), "show");
            }
        }
        
        //check if all lat long are valid
        if(cntValidLat == 1 && cntValidLong == 1) {
        	isValid = true;
        } else if (cntValidLat == 1 && cntValidLong == 0) {
        	if(long == undefined || long == "") {
        		this.showErrorRequiredFields(cmp, 'longitude');
        		cmp.set("v.errorMessageLong", "");
        	} else {
        		$A.util.addClass(cmp.find("longError"),"show");
            	cmp.set("v.errorMessageLong", ERROR_MSG_LONG_INVALID);
        	}
        } else if (cntValidLat == 0 && cntValidLong == 1) {
        	if(lat == undefined || lat == "") {
        		this.showErrorRequiredFields(cmp, 'latitude');
        		cmp.set("v.errorMessageLat", "");
        	} else {
        		$A.util.addClass(cmp.find("latError"),"show");
            	cmp.set("v.errorMessageLat", ERROR_MSG_LAT_INVALID);
        	}
        }
        console.log('Helper.validateLatLongSearchInput - isValid: ' + isValid);
        
        return isValid;		
	},

	validateState : function(cmp) {
        console.log('==== Helper - validateState =====');
		var isValid = true;        
        var compFieldValid = cmp.find("state").get("v.validity").valid;
        
        if (!compFieldValid) { 
			isValid = false;            
        }
        
        console.log('Helper.validateState - isValid: ' + isValid);
        
        return isValid;		
	},
    
	validatePostcode : function(cmp, postcode) {
        console.log('==== Helper - validatePostcode =====');
		var isValid = true;        
        var compFieldValid = cmp.find("postcode").get("v.validity").valid;
        
        if (!compFieldValid) {
			isValid = false;                         
        } else {
			// Validate if numeric
			if (isNaN(postcode)) {				
				// Non numerics found
				isValid = false;  
                
                cmp.find("postcode").set('v.validity', {valid:false, typeMismatch:true});                          
			}        
        }
            
        console.log('Helper.validatePostcode - isValid: ' + isValid);
        
        return isValid;		
	},   

	validateSuburbLocality : function(cmp) {
        console.log('==== Helper - validateSuburbLocality =====');
		var isValid = true;        
        var compFieldValid = cmp.find("suburbLocality").get("v.validity").valid;
        
        if (!compFieldValid) {
			isValid = false;            
        }
         
        console.log('Helper.validateSuburbLocality - isValid: ' + isValid);
        
        return isValid;		
	},

	validateStreetName : function(cmp) {
        console.log('==== Helper - validateStreetName =====');
		var isValid = true;
        var compFieldValid = cmp.find("streetName").get("v.validity").valid;
        
        if (!compFieldValid) {
			isValid = false;            
        }
        
        console.log('Helper.validateStreetName - isValid: ' + isValid);
        
        return isValid;		
	},

	validateStreetType : function(cmp) {
        console.log('==== Helper - validateStreetType =====');
		var isValid = true;
        var compFieldValid = cmp.find("streetType").get("v.validity").valid;
        
        if (!compFieldValid) { 
			isValid = false;            
        }
           
        console.log('Helper.validateStreetType - isValid: ' + isValid);
        
        return isValid;		
	},

	validateStreetLotNumber : function(cmp) {
        console.log('==== Helper - validateStreetLotNumber =====');
		var isValid = true;
        var compFieldValid = cmp.find("streetLotNumber").get("v.validity").valid;
        
        if (!compFieldValid) { 
			isValid = false;            
        }
            
        console.log('Helper.validateStreetLotNumber - isValid: ' + isValid);
        
        return isValid;		
	},

	validateUnitType : function(cmp) {
        console.log('==== Helper - validateUnitType =====');
		var isValid = true;        
        var compFieldValid = cmp.find("unitType").get("v.validity").valid;
        
        if (!compFieldValid) {
			isValid = false;            
        }
               
        console.log('Helper.validateUnitType - isValid: ' + isValid);
        
        return isValid;		
	},

	validateUnitNumber : function(cmp) {
        console.log('==== Helper - validateUnitNumber =====');
		var isValid = true;
        var compFieldValid = cmp.find("unitNumber").get("v.validity").valid;
        
        if (!compFieldValid) {  
			isValid = false;            
        }

        console.log('Helper.validateUnitNumber - isValid: ' + isValid);
        
        return isValid;		
	},

	validateLevel : function(cmp, level) {
        console.log('==== Helper - validateLevel =====');
		var isValid = true;        
        var compFieldValid = cmp.find("level").get("v.validity").valid;
        
        if (!compFieldValid) {  
			isValid = false;            
        } else {
			// Validate if numeric
			if (isNaN(level)) {
				// Non numerics found
				isValid = false;  
                cmp.find("level").set('v.validity', {valid:false, typeMismatch:true});                               
			}        
        }
            
        console.log('==== Helper.validateLevel - isValid: ' + isValid);
        
        return isValid;		
	},

	validateComplexSiteName : function(cmp) {
        console.log('==== Helper - validateComplexSiteName =====');
		var isValid = true;        
        var compFieldValid = cmp.find("complexSiteName").get("v.validity").valid;
        
        if (!compFieldValid) {  
			isValid = false;            
        }
          
        console.log('Helper.validateComplexSiteName - isValid: ' + isValid);
        
        return isValid;		
	},

	validateComplexBuildingName : function(cmp) {
        console.log('==== Helper - validateComplexBuildingName =====');
		var isValid = true;                
        var compFieldValid = cmp.find("complexBuildingName").get("v.validity").valid;
        
        if (!compFieldValid) {   
			isValid = false;            
        }
              
        console.log('Helper.validateComplexBuildingName - isValid: ' + isValid);
        
        return isValid;		
	},

	validateComplexStreetName : function(cmp) {
        console.log('==== Helper - validateComplexStreetName =====');
		var isValid = true;        
        var compFieldValid = cmp.find("complexStreetName").get("v.validity").valid;
        
        if (!compFieldValid) { 
			isValid = false;            
        }
          
        console.log('Helper.validateComplexStreetName - isValid: ' + isValid);
        
        return isValid;		
	},

	validateComplexStreetType : function(cmp) {
        console.log('==== Helper - validateComplexStreetType =====');
		var isValid = true;        
        var compFieldValid = cmp.find("complexStreetType").get("v.validity").valid;
        
        if (!compFieldValid) { 
			isValid = false;            
        }
             
        console.log('Helper.validateComplexStreetType - isValid: ' + isValid);
        
        return isValid;		
	},

	validateComplexStreetNumber : function(cmp) {
        console.log('==== Helper - validateComplexStreetNumber =====');
		var isValid = true;       
        var compFieldValid = cmp.find("complexStreetNumber").get("v.validity").valid;
        
        if (!compFieldValid) { 
			isValid = false;            
        }
               
        console.log('Helper.validateComplexStreetNumber - isValid: ' + isValid);
        
        return isValid;		
	},
	
    // Sets error msg properties
    setErrorMsg: function(cmp, errorStatus, errorMessage, errorType) {        	
        // Set error response comp attribute values 
        cmp.set("v.errorResponseStatus", errorStatus);	
        cmp.set("v.errorResponseMessage", errorMessage);	
        cmp.set("v.errorResponseType", errorType);  
    },
    
	clearErrors : function(cmp) {
        // Set error response comp attribute values 
        cmp.set("v.errorResponseStatus", "");	
        cmp.set("v.errorResponseMessage", "");	
        cmp.set("v.errorResponseType", "");
        cmp.set("v.errorMessageLat", "");
        cmp.set("v.errorMessageLong", "");
        $A.util.removeClass(cmp.find("postCodeStateErr"), "show");
        $A.util.removeClass(cmp.find("longError"), "show");
        $A.util.removeClass(cmp.find("latError"), "show");
	}
})