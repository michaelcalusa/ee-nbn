({
	onRequiredSelectChange: function(cmp, event, helper) {    	 
		helper.validateRequiredSelect(cmp, event);
	},

	closeModal: function(cmp, event, helper) {		
        helper.closeModal(cmp, event);	
		helper.ovcCalculateBandWidth(cmp);
	},

	back: function(cmp, event, helper) {		
		cmp.getEvent("hideQuickQuoteDetailsEvt").fire();
		
		helper.goToOrderSummaryPage(cmp, event);	
	},

    clickNextButton: function(cmp, event, helper) {	        
        var curdate = new Date();
        console.log("clickNextButton Start  time: " + curdate.toLocaleString('en-GB', { timeZone: 'UTC' }));
            
    	console.log('DF_SF_Order_ItemController.clickNextButton - start');
    	cmp.set('v.goNext', true);	
    	helper.goToNextPage(cmp, event);    	
    	console.log('DF_SF_Order_ItemController.clickNextButton - end');
	},	
	
    clickEditButton: function(cmp, event, helper) {		
        var OVCId = event.getSource().get("v.value");
		helper.resetOVCMessage(cmp);
        helper.showOVCDetails(cmp, event, OVCId);
        helper.ovcEnableMappingMode(cmp);
	},
    
    ovcBandWidthOnChange: function(cmp, event, helper){
    	helper.ovcCalculateBandWidth(cmp,event);
        helper.ovcEnableMappingMode(cmp);
	},
    
	handleUNIPageError: function(cmp, event, helper) {		
		if(cmp.get("v.responseStatusUNIPage") =="ERROR") helper.hideOrderLoadingSpinner(cmp);
	},	
	
	clickOrderSummaryButton: function(cmp, event, helper) {		
		helper.goToOrderSummaryPage(cmp, event);
	},	
	
	init: function(cmp, event, helper) {
		cmp.set('v.showOrderLoadingSpinner', true);		
      cmp.set("v.NONBILLING_ATTR_CONFIG", helper.NONBILLING_ATTR_CONFIG);
      cmp.set('v.goNext', false);	
      helper.loadBasketId(cmp);				
	},

	saveAndCloseOVC: function(cmp, event, helper) {
        cmp.set('v.goNext', false);	
		helper.saveOVC(cmp, event);
		if(cmp.get("v.ovcSaveValidationError"))
		{
			cmp.set("v.ovcSaveValidationError",false);
			return;
		}
		helper.closeModal(cmp, event);
	},

	saveOVC: function(cmp, event, helper) {		
        cmp.set('v.goNext', false);	
		helper.saveOVC(cmp, event);
		
		if(cmp.get("v.ovcSaveValidationError"))
		{
			cmp.set("v.ovcSaveValidationError",false);
			return;
		}else
		{
			cmp.set("v.responseStatus", "OK");
            cmp.set("v.responseMessage", $A.get("$Label.c.DF_Quick_Quote_OVC_Save_Success"));
            cmp.set("v.type", "Banner");
		}
	},

	save: function(cmp, event, helper) {
        cmp.set('v.goNext', false);	
		helper.save(cmp);
	},
    
   	saveLocalButton: function(cmp, event, helper) {
		helper.saveLocal(cmp);
	},

	saveAndClose: function(cmp, event, helper) {
        cmp.set('v.goNext', false);	
		helper.save(cmp);
		cmp.getEvent("hideQuickQuoteDetailsEvt").fire();
	},

	addNewOVC: function(cmp, event, helper) {
		helper.addNewOVC(cmp, event);		
	},

	removeOVCRow: function(cmp, event, helper) {
		var OVCId = event.getSource().get("v.value");
		var OVCList	= cmp.get("v.OVCList");	
            
		OVCList = OVCList.filter(function(item) { return item.OVCId != OVCId; });
           
		cmp.set("v.OVCList", OVCList);	
		cmp.set("v.isMaxOVCCount",false);
        
         helper.deleteOVC(cmp,OVCId);
	},
	
	onRouteTypeChange: function(cmp, event, helper) {				
		helper.validateRouteType(cmp, event);
	},

	onChangeET: function(cmp, event, helper) {
		/*var otCmp = cmp.find("OVCType");
		var etCmp = cmp.find("EncapsulationType");
		//var selectedETValue = event.getSource().get("v.value");
		//var selectedOVCType = cmp.get("v.selectedUNI").oVCType;
		var selectedETValue = etCmp.get("v.value");
		var selectedOVCType = otCmp.get("v.value");
		//alert(cmp.get("v.selectedUNI").oVCType);

		if(selectedOVCType == "EVPL" && selectedETValue == "Untagged"){
			//alert("1");
			//etCmp.set('v.validity', {valid:false, badInput :true});
			//etCmp.set("v.errors", [{message:"Untagged cannot be selected when OVCType is EVPL"}]);

			etCmp.focus();
			etCmp.set('v.messageWhenValueMissing','Untagged cannot be selected when OVCType is EVPL ');
			etCmp.showHelpMessageIfInvalid();
			etCmp.set('v.validity', {valid:false, valueMissing :true});
			return;
		}
		else{
			//etCmp.set("v.errors",null);
			//etCmp.set('v.validity', {valid:true, badInput :false});
			etCmp.set('v.validity', {valid:true, valueMissing :false});
		}
		if(selectedETValue == "Untagged"){
			cmp.set("v.disableTPID", true);
			cmp.find("TPID").set("v.value","Select");
			cmp.find("MappingMode").set("v.value","DSCP");
		}
		if(selectedETValue == "Single (802.1Q)")
		{
			cmp.set("v.disableTPID", false);
			cmp.find("TPID").set("v.value","0x8100");
		}
		if(selectedETValue == "Double (802.1AD)")
		{
			
			helper.onChangeTPID(cmp,event);
			
			
		}*/
	},

	onChangeIT: function(cmp,event,helper) {
		/*var itCmp = cmp.find("InterfaceTypes");
		//var cpcCmp = cmp.find("ComboPartConfiguration");

		var selectITValue = itCmp.get("v.value");

		if(selectITValue == "10BaseT" || selectITValue == "100Base-10Base-T (Cat-3)") {
			cpcCmp.set("v.value","RJ45");
		}

		if(selectITValue == "100Base-Tx (Cat-5)" || selectITValue == "1000Base-T (Cat-6)"
			|| selectITValue == "1000Base-SX (Multi-Mode Fibre)" || selectITValue == "1000Base-LX (Single-Mode Fibre)") {
			cpcCmp.set("v.value","SFP");
		}*/
	},

	onChangeTPID: function(cmp,event,helper) {
		var otSelectedValue = cmp.find("OVCType").get("v.value");
		if(otSelectedValue == "Access EVPL")
		{
			helper.validateRequiredSelect(cmp, event);
		}		
	},

	onChangeOVCType: function(cmp,event,helper) {
		var otCmp = cmp.find("OVCType");
		var ovclistCmp = cmp.get("v.OVCList");
		var selectedOVCType = cmp.find("OVCType").get("v.value");
		if(selectedOVCType == "Access EPL") {
			if(ovclistCmp.length > 1) {
				//error you cannot switch to epl when more than one OVC exists
				//otCmp.set('v.validity', {valid:false, badInput :true});
				//otCmp.set("v.errors", [{message:"you cannot switch to epl when more than one OVC exists"}]);
				otCmp.focus();
				otCmp.set('v.messageWhenValueMissing',$A.get("$Label.c.DF_EPL_OVC_Count_Error"));
				otCmp.showHelpMessageIfInvalid();
				otCmp.set('v.validity', {valid:false, valueMissing :true});
				return;
			}
			else if( ovclistCmp.length == 1) {
				cmp.set("v.isMaxOVCCount",true);
				//otCmp.set("v.errors",null);
				//otCmp.set('v.validity', {valid:true, badInput :false});
				otCmp.set('v.validity', {valid:true, valueMissing :false});
			}
			
		}
		else if( ovclistCmp.length == 8){
			cmp.set("v.isMaxOVCCount",true);
		}
		else {
			cmp.set("v.isMaxOVCCount",false);
		}
	},

	onchangePOI: function(cmp,event,helper) {
		var selectPoi = cmp.find("POI").get("v.value");
		var poiRouteTypeMap = cmp.get("v.poiRouteTypeMap");
		for(var key in poiRouteTypeMap) {
			//alert(key);
			//alert(poiRouteTypeMap[key]);
			if(key == selectPoi) {
				cmp.find("routeType").set("v.value",poiRouteTypeMap[key]);
				return;
			}
		}
	},

	onChangeOType: function(cmp,event,helper) {
		
		helper.updateTPIDStatus(cmp);

		var selectedOVCType = cmp.get("v.selectedUNI.oVCType");

		var ovclistCmp = cmp.get("v.OVCList");
		var otCmp = cmp.find("OVCType");

		if(selectedOVCType == "Access EPL") {
			
			if(ovclistCmp.length > 1) {
				//error you cannot switch to epl when more than one OVC exists
				//otCmp.set('v.validity', {valid:false, badInput :true});
				//otCmp.set("v.errors", [{message:"you cannot switch to epl when more than one OVC exists"}]);
				otCmp.focus();
				otCmp.set('v.messageWhenValueMissing',$A.get("$Label.c.DF_EPL_OVC_Count_Error"));
				otCmp.showHelpMessageIfInvalid();
				otCmp.set('v.validity', {valid:false, valueMissing :true});
				return;
			}
			else if( ovclistCmp.length == 1) {
				cmp.set("v.isMaxOVCCount",true);
				//otCmp.set("v.errors",null);
				//otCmp.set('v.validity', {valid:true, badInput :false});
				otCmp.set('v.validity', {valid:true, valueMissing :false});
			}
			
		}
		else if( ovclistCmp.length == 8){
			cmp.set("v.isMaxOVCCount",true);
		}
		else {
			helper.validateRequiredSelect(cmp, event);
			cmp.set("v.isMaxOVCCount",false);
		}
	},
	
    onCEVLAINIDChange: function(cmp, event, helper) {
		helper.validateCEVLAINID(cmp, event);
       // if(cmp.get("v.uniVLANValid")===true){
        	helper.validateCEVLANIdCount(cmp, event);
       // }
	}

    
})