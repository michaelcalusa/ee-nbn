({
    ATTR_CONFIG: [{
            csAttr: 'Term',
            slAttr: 'term',
            slOptionAttr: 'v.UTOptions',
            objectType: 'uni'
        },
        {
            csAttr: 'After Hours Site Visit',
            slAttr: 'AHA',
            slOptionAttr: 'v.AHAOptions',
            objectType: 'uni'
        },
        {
            csAttr: 'BTD Type',
            slAttr: 'BTDType',
            slOptionAttr: 'v.BTDTypeOptions',
            objectType: 'uni'
        },
        {
            csAttr: 'Zone',
            slAttr: 'zone',
            slOptionAttr: 'v.zoneOptions',
            objectType: 'uni'
        },
        {
            csAttr: 'eSLA',
            slAttr: 'SLA',
            slOptionAttr: 'v.SLAOptions',
            objectType: 'uni'
        },
        {
            csAttr: 'CoS High',
            slAttr: 'coSHighBandwidth',
            slOptionAttr: 'v.coSHighBandwidthOptions',
            objectType: 'ovc'
        },
        {
            csAttr: 'CoS Medium',
            slAttr: 'coSMediumBandwidth',
            slOptionAttr: 'v.coSMediumBandwidthOptions',
            objectType: 'ovc'
        },
        {
            csAttr: 'CoS Low',
            slAttr: 'coSLowBandwidth',
            slOptionAttr: 'v.coSLowBandwidthOptions',
            objectType: 'ovc'
        },
        {
            csAttr: 'POI',
            slAttr: 'POI',
            //slOptionAttr: 'v.POIOptions',
            objectType: 'ovc'
        },
        {
            csAttr: 'Route Type',
            slAttr: 'routeType',
            slOptionAttr: 'v.routeTypeOptions',
            objectType: 'ovc'
        },
        {
            csAttr: 'Interface Bandwidth',
            slOptionAttr: 'v.IBOptions',
            slAttr: 'interfaceBandwidth',
            objectType: 'uni'
        },
        {
            csAttr: 'Once off charge',
            slAttr: 'SCNR'
        },
        {
            csAttr: 'Recurring charge',
            slAttr: 'SCR'
        },
        {
            csAttr: 'Total_Routing_Charge_afterDiscount',
            slAttr: 'routeRecurring'
        },
        {
            csAttr: 'Hidden_TotalCoS_Charges',
            slAttr: 'coSRecurring'
        },
        {
            csAttr: 'CSA',
            slAttr: 'CSA'
        },
        {
            csAttr: 'Id',
            slAttr: 'Id'
        },
        {
            csAttr: 'Id',
            slAttr: 'OVCId'
        },
        {
            csAttr: 'Name',
            slAttr: 'OVCName'
        },
        {
            csAttr: 'OVC Recurring Charge',
            slAttr: 'monthlyCombinedCharges'
        }
    ],

    NONBILLING_ATTR_CONFIG: [{
            nbAttr: 'mappingMode',
            slAttr: 'mappingMode',
            slOptionAttr: 'v.MMOptions'
        },
        {
            nbAttr: 'OVC Type',
            slOptionAttr: 'v.OTOptions',
            objectType: 'uni'
        },
        {
            nbAttr: 'TPID',
            slOptionAttr: 'v.TPIDOptions',
            objectType: 'uni'
        },
        {
            nbAttr: 'TPID',
            slOptionAttr: 'v.TPIDOptionsStatic',
            objectType: 'uni'
        },

        {
            nbAttr: 'Interface Type',
            slOptionAttr: 'v.ITOptions',
            objectType: 'uni'
        },
        {
            nbAttr: 'NNIGroupId',
            slAttr: 'NNIGroupId'
        },
        {
            nbAttr: 'sTag',
            slAttr: 'sTag'
        },
        {
            nbAttr: 'ceVlanId',
            slAttr: 'ceVlanId'
        },
        {
            nbAttr: 'ovcMaxFrameSize',
            slAttr: 'ovcMaxFrameSize'
        }
    ],

    ORDER_STATUS: 'Order Draft',
    PROD_CHARGE_NAME: 'Direct Fibre - Product Charges',

    showOVCDetails: function (cmp, event, OVCId) {
        var OVCList = cmp.get("v.OVCList");
        var otCmp;
        var results = OVCList.filter(function (item) {
            return item.OVCId == OVCId;
        });
        cmp.set("v.selectedOVC", this.cloneOVC(results[0]));
        cmp.set("v.isModalOpen", true);

        var OVC = cmp.get("v.selectedOVC");
        //disable uni vlan if ovc type is Access EPL and set to empty
        var selectedOVCType = cmp.get("v.selectedUNI.oVCType");
        cmp.set("v.disableUNIVLANID", false);
        cmp.set("v.isRequired", true);
        if (selectedOVCType == "Access EPL") {
            cmp.set("v.disableUNIVLANID", true);
            cmp.set("v.isRequired", false);
            cmp.set("v.selectedOVC.ceVlanId", "");
        }
        if (OVC.CSA != "") {
            var action = cmp.get("c.doesCSADefaultToLocal");
            action.setParams({
                cSAId: OVC.CSA
            });

            action.setCallback(this, function (response) {
                var state = response.getState();

                if (state === "SUCCESS") {
                    var results = response.getReturnValue();
                    if (results === true) {
                        otCmp = cmp.find("routeType");
                        OVC.routeType = "Local";
                        cmp.set("v.selectedOVC", OVC);
                        otCmp.set("v.disabled", true);
                    } else {
                        otCmp = cmp.find("routeType");
                        otCmp.set("v.disabled", false);
                    }
                }
                this.ovcCalculateBandWidth(cmp, event);
                this.loadAllUniVlanRanges(cmp);
            });
            $A.enqueueAction(action);

        }


    },

    loadMockData: function (cmp) {
        this.initialValidation(cmp, event);
    },

    loadOVCList: function (cmp, event) {
        var location = cmp.get("v.location");
        if (location.OVCs.length == 0) {
            this.addNewOVC(cmp, event);
        } else {
            cmp.set("v.OVCList", location.OVCs);
        }
    },

    cloneUNI: function (UNI) {

        var clonedUNI = {
            'BTDType': UNI.BTDType,
            'interfaceBandwidth': UNI.interfaceBandwidth,
            'SLA': UNI.SLA,
            'term': UNI.term,
            'zone': UNI.zone,
            'AHA': UNI.AHA
        };

        return clonedUNI;
    },

    cloneOVC: function (OVC, nbOVC) {

        var clonedOVC = {
            'OVCId': OVC.OVCId,
            'CSA': OVC.CSA,
            'NNIGroupId': OVC.NNIGroupId,
            'routeType': OVC.routeType,
            'coSHighBandwidth': OVC.coSHighBandwidth,
            'coSMediumBandwidth': OVC.coSMediumBandwidth,
            'coSLowBandwidth': OVC.coSLowBandwidth,
            'routeRecurring': OVC.routeRecurring,
            'coSRecurring': OVC.coSRecurring,
            'ovcMaxFrameSize': 'Jumbo (9000 Bytes)',
            'POI': OVC.POI,
            'status': OVC.status,
            'sTag': OVC.sTag,
            'ceVlanId': OVC.ceVlanId,
            'mappingMode': OVC.mappingMode
        };

        return clonedOVC;
    },

    addNewOVC: function (cmp, event) {
        cmp.set('v.showOrderLoadingSpinner', true);
        var action = cmp.get("c.addOVC");
        var location = cmp.get('v.location');
        var basketIdVal = location.basketId;
        var prodChargeID = cmp.get('v.prodchargeConfigurationId');
        var csaVal = cmp.get('v.csa');
        var errorMessage = "";

        action.setParams({
            basketId: basketIdVal,
            configId: prodChargeID,
            csa: csaVal
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results === "Success") {
                    this.loadCSConfig(cmp);
                    cmp.set("v.responseStatusUNIPage", "");
                    cmp.set("v.responseMessageUNIPage", "");
                    cmp.set("v.uniBannertype", "");
                } else if (results === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showModalLoadingSpinner", false);
                }
                this.hideOrderLoadingSpinner(cmp);
            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },

    hideOrderLoadingSpinner: function (cmp) {
        window.setTimeout(
            $A.getCallback(function () {
                cmp.set("v.showOrderLoadingSpinner", false);
            }), 1000
        );
    },

    deleteOVC: function (cmp, OVCId) {
        cmp.set("v.showOrderLoadingSpinner", true);
        var action = cmp.get("c.removeOVC");
        var location = cmp.get('v.location');
        var basketIdVal = location.basketId;
        var relatedAtt = 'OVC';
        var errorMessage = "";
        var OVCList = cmp.get("v.OVCList");

        action.setParams({
            basketId: basketIdVal,
            configId: OVCId,
            relatedProductAtt: relatedAtt
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results === "Success") {
                    this.loadCSConfig(cmp);
                    cmp.set("v.responseStatusUNIPage", "");
                    cmp.set("v.responseMessageUNIPage", "");
                    cmp.set("v.uniBannertype", "");
                } else if (results === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showModalLoadingSpinner", false);
                }

            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
            }
            cmp.set("v.showOrderLoadingSpinner", true);
        });
        $A.enqueueAction(action);
    },

    saveOVC: function (cmp, event) {
        this.ovcSaveValidation(cmp, event);
        this.ovcCalculateBandWidth(cmp, event);
        if (cmp.get("v.ovcSaveValidationError")) {
            return;
        }

        var location = cmp.get("v.location");
        var orderJSON = cmp.get("v.orderJSON");
        var selectedOVC = cmp.get("v.selectedOVC");
        var OVCList = cmp.get("v.OVCList");
        var selectedUNI = cmp.get("v.selectedUNI");

        var updatedOVCList = OVCList.map(function (item) {
            return item.OVCId === selectedOVC.OVCId ? selectedOVC : item
        });

        location.OVCs = updatedOVCList;
        location.UNI = [selectedUNI];
        orderJSON.OVCs = updatedOVCList;
        cmp.set("v.OVCList", updatedOVCList);
        cmp.set("v.selectedOVC", selectedOVC);
        cmp.set("v.orderJSON", orderJSON);
        this.saveToCS(cmp);

        location.status = this.ORDER_STATUS;
        cmp.set('v.location', location);
        cmp.set("v.orderJSON", orderJSON);

        this.updateDFQuoteStatus(cmp);
        this.saveOVCNonBillable(cmp);
        var compEvent = cmp.getEvent("saveQuickQuoteDetailsEvt");
        compEvent.setParam("location", location);
        compEvent.fire();
    },

    saveOVCNonBillable: function (cmp) {

        var action = cmp.get("c.setNonBillableOVCValues");
        var location = cmp.get('v.location');
        var OVCs = location.OVCs;
        var dfOrderId = location.OrderId;

        var jsonOVCnonBillable = JSON.stringify(OVCs);
        console.debug('jsonOVCnonBillable: ' + jsonOVCnonBillable);
        action.setParams({
            dfOrderId: dfOrderId,
            jsonOVCnonBillable: jsonOVCnonBillable
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('===== saveOVCNonBillable ===== state ' + state)
            if (state === "SUCCESS") {
                //debugger;
            } else { //if (state === "ERROR") 
                var errorMessage = $A.get("$Label.c.DF_Application_Error");
                console.log('updateDFOrder Error ' + errormessage);
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },

    loadOVCNonBillableToUI: function (cmp) {
        var action = cmp.get("c.getNonBillableOVCValues");
        var location = cmp.get('v.location');
        var orderJSON = cmp.get("v.orderJSON");
        var dfOrderId = location.OrderId;
        var selectedOVC = cmp.get("v.selectedOVC");
        cmp.set("v.NONBILLING_ATTR_CONFIG", this.NONBILLING_ATTR_CONFIG);
        action.setParams({
            dfOrderId: dfOrderId
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var results = JSON.parse(response.getReturnValue());
                var location = cmp.get('v.location');
                var updatedOVCList = [];
                if (results !== null && results.length > 0) {
                    location.OVCs.map(function (item) {
                        results.map(function (resultItem) {
                            if (resultItem.OVCId == item.OVCId) {
                                cmp.get("v.NONBILLING_ATTR_CONFIG").map(function (arrtItem) {
                                    if (!arrtItem.slOptionAttr || arrtItem.slOptionAttr == "v.MMOptions") {
                                        item[arrtItem.slAttr] = resultItem[arrtItem.nbAttr];
                                    }
                                });
                            }
                        });
                        updatedOVCList.push(item);
                    });
                    cmp.set('v.NonBillableOVCList', updatedOVCList);
                    location.OVCs = updatedOVCList;
                    orderJSON.OVCs = updatedOVCList;
                    cmp.set('v.location', location);
                    cmp.set('v.OVCList', updatedOVCList);
                    cmp.set("v.orderJSON", orderJSON);                    
                }
            } else { 
                var errorMessage = $A.get("$Label.c.DF_Application_Error");
                console.log('updateDFOrder Error ' + errorMessage);
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
            }
            
            //CPST-7198
            if (cmp.get('v.isSamZoneMappingUpdated')) {
                cmp.set('v.isSamZoneMappingUpdated', false);
                this.loadBasketId(cmp);
            } else this.hideOrderLoadingSpinner(cmp);
        });
        $A.enqueueAction(action);
    },

    save: function (cmp) {
        var location = cmp.get("v.location");
        var OVCList = cmp.get('v.NonBillableOVCList');
        var selectedUNI = cmp.get("v.selectedUNI");

        location.OVCs = OVCList;
        location.UNI = [selectedUNI];
        location.status = this.ORDER_STATUS;
        cmp.set('v.location', location);

        this.saveToCS(cmp);
        this.svrSave(cmp);
    },

    svrSave: function (cmp) {
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
        var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';
        var location = cmp.get("v.location");
        location.OVCs = cmp.get('v.NonBillableOVCList');
        cmp.set('v.location', location);
        var orderJS = cmp.get("v.orderJSON");
        orderJS.OVCs = cmp.get('v.NonBillableOVCList');
        var orderJSON = JSON.stringify(orderJS);
        cmp.set('v.orderJSON', orderJS);
        var dfOrderId = location.OrderId.toString();
        var dfQuoteId = location.id;
        var interfaceType = this.nullToEmptyString(cmp.find("InterfaceTypes").get("v.value"));
        var ovcType = this.nullToEmptyString(cmp.find("OVCType").get("v.value"));
        //var mappingMode  = this.nullToEmptyString(cmp.find("MappingMode").get("v.value"));        
        var tpid = this.nullToEmptyString(cmp.find("TPID").get("v.value"));
        //this.svrGetOrderValidatorResponseData(cmp);  
        // Create the action
        var action = cmp.get("c.invokeSave");
        cmp.set('v.showOrderLoadingSpinner', true);
        action.setParams({
            "dfQuoteId": dfQuoteId,
            "dfOrderId": dfOrderId,
            "interfaceType": interfaceType,
            "ovcType": ovcType,
            "tpidx": tpid,
            "location": orderJSON
        });
        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.svrSave - orderJSON: ' + orderJSON);

        // Add callback behavior for when response is received
        action.setCallback(this, function (response) {
            var curdate = new Date();
            console.log("Callback to invokeSave  response received time: " + curdate.toLocaleString('en-GB', {
                timeZone: 'UTC'
            }));

            var state = response.getState();
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.svrSave - state: ' + state);

            if (state === "SUCCESS") {
                this.getOrderValidatorSync(cmp);
                // this.svrGetOrderValidatorResponseData(cmp);    	            	        
            } else {
                // Set error response comp with generic application error        	
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", ERR_MSG_APP_ERROR);
                cmp.set("v.showModalLoadingSpinner", false);

                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                    console.log(ERR_MSG_UNKNOWN_ERROR);
                }
            }
        });

        // Send action off to be executed
        $A.enqueueAction(action);
        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.svrSave - action(invokeSave) enqueued...');
    },

    getOrderValidatorSync: function (cmp) {
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
        var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

        var location = cmp.get("v.location");

        var orderJSON = cmp.get("v.orderJSON");
        var dfOrderId = location.OrderId.toString();
        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.svrGetOrderValidatorResponseData - dfOrderId: ' + dfOrderId);

        // Create the action
        var action = cmp.get("c.invokeOrderValidatorSynchronousCallout");

        action.setParams({
            "dfOrderId": dfOrderId
        });

        action.setCallback(this, function (response) {
            var curdate = new Date();
            console.log("Callback to invokeOrderValidatorSynchronousCallout  response received time: " + curdate.toLocaleString('en-GB', {
                timeZone: 'UTC'
            }));

            var state = response.getState();
            if (state === "SUCCESS") {
                var responseReturnValue = response.getReturnValue();
                // Parse json string into js objects list
                var responseReturnDataObjList = JSON.parse(responseReturnValue);
                //var orderValidationStatus = responseReturnDataObjList[0].orderValidationStatus;
                var orderValidationStatus = responseReturnDataObjList.orderValidationStatus;
                var orderValidatorResponseJSON = responseReturnDataObjList.orderValidatorResponseJSON;

                cmp.set("v.responseStatusUNIPage", "");
                cmp.set("v.responseMessageUNIPage", "");
                cmp.set("v.uniBannertype", "");

                var location = cmp.get('v.location');

                location.status = this.ORDER_STATUS;
                orderJSON.OVCs = cmp.get("v.NonBillableOVCList");
                cmp.set("v.location", orderJSON); //Changed to teh cleaner JSON
                if (orderValidationStatus.toLowerCase() == 'success') {
                    var evt = cmp.getEvent("orderContactDetailsPageEvent");

                    evt.setParams({

                        "location": cmp.get("v.location")

                    });

                    evt.fire();

                } else if (orderValidationStatus == 'failed') {
                    // Populate validation errors for each field on UI
                    this.displayOrderValidatorErrors(cmp, orderValidatorResponseJSON);
                    return;
                } else { // eg. Pending, In Error      	        	
                    // Set error response comp with generic application error        	
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", ERR_MSG_APP_ERROR);
                    cmp.set("v.showModalLoadingSpinner", false);

                    return;
                }
            } else {
                // Set error response comp with generic application error        	
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", ERR_MSG_APP_ERROR);
                cmp.set("v.showModalLoadingSpinner", false);

                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                    console.log(ERR_MSG_UNKNOWN_ERROR);
                }
            }

        });
        var curdate = new Date();
        console.log("Callback invokeOrderValidatorSynchronousCallout enqueued time: " + curdate.toLocaleString('en-GB', {
            timeZone: 'UTC'
        }));
        $A.enqueueAction(action);
    },
    svrGetOrderValidatorResponseData: function (cmp) {
        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.svrGetOrderValidatorResponseData - START');

        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
        var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

        var location = cmp.get("v.location");

        var orderJSON = cmp.get("v.orderJSON");
        var dfOrderId = location.OrderId.toString();
        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.svrGetOrderValidatorResponseData - dfOrderId: ' + dfOrderId);

        // Create the action
        var action = cmp.get("c.getOrderValidatorResponseData");

        action.setParams({
            "dfOrderId": dfOrderId
        });

        // Add callback behavior for when response is received
        action.setCallback(this, function (response) {
            var state = response.getState();
            this.hideOrderLoadingSpinner(cmp);

            if (state === "SUCCESS") {
                // Get response string
                var responseReturnValue = response.getReturnValue();
                // Parse json string into js objects list
                var responseReturnDataObjList = JSON.parse(responseReturnValue);
                var orderValidationStatus = responseReturnDataObjList.orderValidationStatus;
                var orderValidatorResponseJSON = responseReturnDataObjList.orderValidatorResponseJSON;
                cmp.set("v.responseStatusUNIPage", "");
                cmp.set("v.responseMessageUNIPage", "");
                cmp.set("v.uniBannertype", "");

                var location = cmp.get('v.location');

                location.status = this.ORDER_STATUS;
                orderJSON.OVCs = cmp.get("v.NonBillableOVCList");
                cmp.set("v.location", orderJSON); //Changed to teh cleaner JSON

                if (orderValidationStatus == 'Success') {
                    var evt = cmp.getEvent("orderContactDetailsPageEvent");

                    evt.setParams({
                        "location": cmp.get("v.location")
                    });

                    evt.fire();
                } else if (orderValidationStatus == 'Failed') {
                    // Populate validation errors for each field on UI
                    this.displayOrderValidatorErrors(cmp, orderValidatorResponseJSON);
                    return;
                } else { // eg. Pending, In Error      	        	
                    // Set error response comp with generic application error        	
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", ERR_MSG_APP_ERROR);
                    cmp.set("v.showModalLoadingSpinner", false);

                    return;
                }
            } else {
                // Set error response comp with generic application error        	
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", ERR_MSG_APP_ERROR);
                cmp.set("v.showModalLoadingSpinner", false);

                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                    console.log(ERR_MSG_UNKNOWN_ERROR);
                }
            }
        });
        $A.enqueueAction(action);

        //  window.setTimeout(
        //         $A.getCallback(function() {                    	
        // Send action off to be executed
        //             $A.enqueueAction(action);
        //              console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.svrGetOrderValidatorResponseData - action(getOrderValidatorResponseData) enqueued...');                	                	
        //          }), 1000
        //      ); 

        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.svrGetOrderValidatorResponseData - END');

    },

    saveLocal: function (cmp) {
        var location = cmp.get("v.location");
        var OVCList = cmp.get("v.OVCList");
        var selectedUNI = cmp.get("v.selectedUNI");

        location.OVCs = OVCList;
        location.UNI = [selectedUNI];
        location.status = this.ORDER_STATUS;
        cmp.set('v.location', location);

        this.saveToCS(cmp);

        this.updateDFQuoteStatus(cmp);

        this.updateDFOrder(cmp);
    },


    updateDFOrder: function (cmp) {
        console.log('===== updateDFOrder ===== START')
        var location = cmp.get('v.location');
        var selectedUNI = cmp.get("v.selectedUNI");

        var action = cmp.get("c.updateDF_OrderNonBillable");
        var errorMessage = "";

        var interfaceType = this.nullToEmptyString(cmp.find("InterfaceTypes").get("v.value"));
        var ovcType = this.nullToEmptyString(cmp.find("OVCType").get("v.value"));
        //var mappingMode  = this.nullToEmptyString(cmp.find("MappingMode").get("v.value"));
        var tpid = this.nullToEmptyString(cmp.find("TPID").get("v.value"));
        var dfOrderId = location.OrderId.toString();

        action.setParams({
            dfOrderId: dfOrderId,
            interfaceType: interfaceType,
            ovcType: ovcType,

            tpidx: tpid

        }); // mappingMode: mappingMode,
        console.log('===== updateDFOrder ===== ' + interfaceType + ' ' + ovcType + '  ' + tpid + ' ' + dfOrderId);
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('===== updateDFOrder ===== state ' + state)
            if (state === "SUCCESS") {

            } else //if (state === "ERROR") 
            {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                console.log('updateDFOrder Error ' + errormessage);
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
            }

        });
        $A.enqueueAction(action);
        console.log('===== updateDFOrder ===== END')
    },

    saveToCS: function (cmp) {
        var configList = cmp.get("v.configs");
        var OVCList = cmp.get("v.OVCList");
        var selectedUNI = cmp.get("v.selectedUNI");
        var selectedOVC = cmp.get("v.selectedOVC");

        var prodConfig = configList.filter(this.getAttByName('Direct Fibre - Product Charges'))[0];

        prodConfig.atts.filter(this.getAttByName('Interface Bandwidth'))[0].cscfga__Value__c = selectedUNI.interfaceBandwidth;
        prodConfig.atts.filter(this.getAttByName('BTD Type'))[0].cscfga__Value__c = selectedUNI.BTDType;
        prodConfig.atts.filter(this.getAttByName('After Hours Site Visit'))[0].cscfga__Value__c = selectedUNI.AHA;
        prodConfig.atts.filter(this.getAttByName('Zone'))[0].cscfga__Value__c = selectedUNI.zone;
        prodConfig.atts.filter(this.getAttByName('eSLA'))[0].cscfga__Value__c = selectedUNI.SLA;
        prodConfig.atts.filter(this.getAttByName('Term'))[0].cscfga__Value__c = selectedUNI.term;

        var configItem = configList.filter(function (item) {
            return item.Id == selectedOVC.OVCId;
        })[0];

        if (configItem) {

            configItem.atts.filter(this.getAttByName('CoS High'))[0].cscfga__Value__c = selectedOVC.coSHighBandwidth; //=="0"?"":selectedOVC.coSHighBandwidth;
            configItem.atts.filter(this.getAttByName('CoS Medium'))[0].cscfga__Value__c = selectedOVC.coSMediumBandwidth; //=="0"?"":selectedOVC.coSMediumBandwidth;
            configItem.atts.filter(this.getAttByName('CoS Low'))[0].cscfga__Value__c = selectedOVC.coSLowBandwidth; //=="0"?"":selectedOVC.coSLowBandwidth;

            configItem.atts.filter(this.getAttByName('POI'))[0].cscfga__Value__c = selectedOVC.POI;
            configItem.atts.filter(this.getAttByName('Route Type'))[0].cscfga__Value__c = selectedOVC.routeType;
        }

        var result = JSON.parse(JSON.stringify(cmp.get("v.configs")));
        var configs = {};
        var attributes = {}
        var parentId = "";
        for (var i = 0; i < result.length; i++) {
            var key = result[i].Id;
            attributes[key + '-attributes'] = result[i].atts;
            delete result[i]['atts'];
            configs[key] = result[i];
        }

        var location = cmp.get('v.location');
        var action = cmp.get("c.updateProduct");
        var errorMessage = "";

        action.setParams({
            basketId: location.basketId,
            configs: JSON.stringify(configs),
            attributes: JSON.stringify(attributes)
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            var goFlag = cmp.get("v.goNext");
            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results != "Error") {
                    this.calculatetotals(cmp); //26/11
                    this.loadCSConfig(cmp);
                    cmp.set("v.responseStatusUNIPage", "");
                    cmp.set("v.responseMessageUNIPage", "");
                    cmp.set("v.uniBannertype", "");

                    cmp.set("v.responseStatus", "OK");
                    cmp.set("v.responseMessage", $A.get("$Label.c.DF_Order_OVC_Save_Success"));
                    cmp.set("v.type", "Banner");
                } else if (results === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showModalLoadingSpinner", false);
                }
            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                console.log('saveToCS Error ' + errorMessage);
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
            }

        });
        $A.enqueueAction(action);
    },

    updateDFQuoteStatus: function (cmp) {
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
        var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

        var quoteId = cmp.get("v.location").id;
        var quoteStatus = cmp.get("v.dfQuoteStatusDraft");
        // Create the action
        var action = cmp.get("c.processUpdateDFQuoteStatus");
        if (!quoteStatus) {
            action.setParams({
                "quoteId": quoteId
            });

            // Add callback behavior for when response is received
            action.setCallback(this, function (response) {
                var state = response.getState();
                console.log('state: ' + state);

                if (state === "SUCCESS") {
                    // Get response string
                    var responseReturnValue = response.getReturnValue();
                    cmp.set("v.dfQuoteStatusDraft", true);
                    cmp.set("v.responseStatusUNIPage", "");
                    cmp.set("v.responseMessageUNIPage", "");
                    cmp.set("v.uniBannertype", "");
                } else {
                    // Set error response comp with generic application error        	
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", ERR_MSG_APP_ERROR);
                    cmp.set("v.showModalLoadingSpinner", false);

                    var errors = response.getError();

                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                        }
                    } else {
                        console.log(ERR_MSG_UNKNOWN_ERROR);
                    }
                }
            });

            // Send action off to be executed
            $A.enqueueAction(action);
        }
    },

    closeModal: function (cmp, event) {
        cmp.set("v.isModalOpen", false);
        cmp.set("v.selectedOVC", []);
        cmp.set("v.ovcSaveValidationError", false);
        cmp.set("v.responseStatus", "");
        cmp.set("v.responseMessage", "");
        cmp.set("v.type", "");
    },

    goToOrderSummaryPage: function (cmp, event) {

        var location = cmp.get('v.location');
        var evt = cmp.getEvent("quoteDetailPageEvent");
        evt.setParams({
            "location": cmp.get("v.location")
        });
        evt.fire();
    },

    goToNextPage: function (cmp, event) {
        this.saveValidation(cmp, event); // Synchronous processing
        if (cmp.get("v.saveValidationError")) {
            return;
        }
        //update ovc cevlanid empty value
        var selectedOVCType = cmp.get("v.selectedUNI.oVCType");
        if (selectedOVCType == 'Access EPL') {
            this.updateUniVlanIdForAccessEPL(cmp);
        } else {
            this.saveOVCNonBillable(cmp);
        }

        this.saveNext(cmp);
    },

    updateUniVlanIdForAccessEPL: function (cmp) {
        var OVCList = cmp.get("v.OVCList");
        var updatedOVCList = [];
        for (var i = 0; i < OVCList.length; i++) {
            OVCList[i].ceVlanId = "";
            updatedOVCList.push(OVCList[i]);
        }
        cmp.set("v.OVCList", updatedOVCList);
        cmp.set('v.NonBillableOVCList', updatedOVCList);
        this.saveOVCNonBillable(cmp);
    },

    saveValidation: function (cmp, event) {
        var tempCtrl = event.getSource();
        var ahaSelectedValue = cmp.find("AfterHoursAppointment").get("v.value");
        var itSelectedValue = cmp.find("InterfaceTypes").get("v.value");
        var tpidSelectedValue = cmp.find("TPID").get("v.value");
        var otSelectedValue = cmp.find("OVCType").get("v.value");


        var validationError = false;
        var errorMessage = "";

        //var mpInputCmp = cmp.find("MappingMode");
        //this.validateSelectCmpEmpty(mpInputCmp);
        //if (!mpInputCmp.get("v.validity").valid) {            			
        //	this.reFocus(mpInputCmp, tempCtrl);
        //	validationError = true;
        //}         

        var otInputCmp = cmp.find("OVCType");
        this.validateSelectCmpEmpty(otInputCmp);
        if (!otInputCmp.get("v.validity").valid) {
            this.reFocus(otInputCmp, tempCtrl);
            validationError = true;
        }

        if (validationError == false && ((!tpidSelectedValue || tpidSelectedValue == "Select" || tpidSelectedValue == null) && otSelectedValue == "Access EVPL")) {
            cmp.find("TPID").set('v.messageWhenValueMissing', $A.get("$Label.c.DF_Field_Empty_Error"));
            cmp.find("TPID").set('v.validity', {
                valid: false,
                valueMissing: true
            });
            $A.util.addClass(cmp.find("TPID"), "slds-has-error");
            $A.util.removeClass(cmp.find("TPID"), "hide-error-message");
            validationError = true;
            cmp.find("TPID").focus();
            tempCtrl.focus();
            cmp.find("TPID").focus();
        } else {
            $A.util.removeClass(cmp.find("TPID"), "slds-has-error");
            $A.util.addClass(cmp.find("TPID"), "hide-error-message");
        }

        var itInputCmp = cmp.find("InterfaceTypes");
        this.validateSelectCmpEmpty(itInputCmp);
        if (!itInputCmp.get("v.validity").valid) {
            this.reFocus(itInputCmp, tempCtrl);
            validationError = true;
        }

        var ahaInputCmp = cmp.find("AfterHoursAppointment");
        this.validateSelectCmpEmpty(ahaInputCmp);
        if (!ahaInputCmp.get("v.validity").valid) {
            this.reFocus(ahaInputCmp, tempCtrl);
            validationError = true;
        }

        ////Validate ovctype count
        var selectedOVCType = cmp.find("OVCType").get("v.value");

        var ovclistCmp = cmp.get("v.OVCList");
        var otCmp = cmp.find("OVCType");

        if (selectedOVCType == "EPL") {
            if (ovclistCmp.length > 1) {
                //error you cannot switch to epl when more than one OVC exists
                otCmp.set('v.messageWhenValueMissing', $A.get("$Label.c.DF_EPL_OVC_Count_Error"));
                otCmp.set('v.validity', {
                    valid: false,
                    valueMissing: true
                });
                validationError = true;
                otCmp.focus();
                tempCtrl.focus();
                otCmp.focus();
            }

        } else if (ovclistCmp.length == 8) {
            cmp.set("v.isMaxOVCCount", true);
        } else {
            cmp.set("v.isMaxOVCCount", false);
        }
        ////
        if (validationError == false) {
            this.validateOVCNonBillable(cmp);
            this.validateOVCCos(cmp);
            this.isTotalCIRLTSeventyTotalBandwidth(cmp);
            //alert(cmp.get("v.isTotalCIRLT70PerTotalBW"));
            /*  if (cmp.get("v.isTotalCIRLT70PerTotalBW")) {
                if (errorMessage != "") {
                    errorMessage += "<br>" + $A.get("$Label.c.DF_Total_OVC_CIR_GT_70_Percent");
                } else {
                    errorMessage = $A.get("$Label.c.DF_Total_OVC_CIR_GT_70_Percent");
                }
            }
    
            if (cmp.get("v.isTotalPIRLT1p0PerTotalBW")) {
                if (errorMessage != "") {
                    errorMessage += "<br>" + $A.get("$Label.c.DF_Total_OVC_PIR_GT_Interface_bandwidth");
                } else {
                    errorMessage = $A.get("$Label.c.DF_Total_OVC_PIR_GT_Interface_bandwidth");
                }
            }
            if(cmp.get("v.isTotalCIRLT70PerTotalBW")) {
                if(errorMessage !="") {
                    errorMessage +="<br>" + $A.get("$Label.c.DF_Total_OVC_CIR_GT_70_Percent");
                }
                else {
                    errorMessage =$A.get("$Label.c.DF_Total_OVC_CIR_GT_70_Percent");
                }
            }
    
            if(cmp.get("v.isTotalPIRLT1p0PerTotalBW")) {
                if(errorMessage !="") {
                    errorMessage +="<br>" + $A.get("$Label.c.DF_Total_OVC_PIR_GT_Interface_bandwidth");
                }
                else {
                    errorMessage =$A.get("$Label.c.DF_Total_OVC_PIR_GT_Interface_bandwidth");
                }
            }*/
            if (cmp.get("v.areOVCCosEmpty")) {
                if (errorMessage != "") {
                    errorMessage += "<br>" + $A.get("$Label.c.DF_No_COS_Selection_Error");
                } else {
                    errorMessage = $A.get("$Label.c.DF_No_COS_Selection_Error");
                }
            }
            if (cmp.get("v.areOVCNonBillableEmpty")) {
                if (errorMessage != "") {
                    errorMessage += "<br>" + $A.get("$Label.c.DF_Missing_OVC_Values");
                } else {
                    errorMessage = $A.get("$Label.c.DF_Missing_OVC_Values");
                }
            }

        }
        if (errorMessage !== "") {
            validationError = true;
            cmp.set("v.uniBannertype", "Banner");
            cmp.set("v.responseStatusUNIPage", "ERROR");
            cmp.set("v.responseMessageUNIPage", errorMessage);
            console.log('ERROR saveValidation ' + errorMessage);
        } else {
            cmp.set("v.responseStatusUNIPage", "");
            cmp.set("v.responseMessageUNIPage", "");
            cmp.set("v.uniBannertype", "");
        }

        if (validationError) cmp.set("v.saveValidationError", true);
        else cmp.set("v.saveValidationError", false);
        tempCtrl.focus();

    },

    displayOrderValidatorErrors: function (cmp, orderValidatorResponseJSON) {
        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - START');

        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - orderValidatorResponseJSON: ' + orderValidatorResponseJSON);

        var errorMessage = 'Order Validator - validation has failed. Please fix the issues then try again.';

        // Parse json string into js objects list
        var orderValidatorResponseJSONObj = JSON.parse(orderValidatorResponseJSON);

        // Get validationErrors node
        var validationErrorsList = orderValidatorResponseJSONObj.validationErrors;
        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - validationErrorsList.length: ' + validationErrorsList.length);

        var locationTypeErrorsList;
        var btdTypeErrorsList;
        var productTypeErrorsList;
        var uniTypeErrorsList;
        var ovcTypeErrorsList;

        // Loop thru ovValidationErrorsList	and setup up sub lists for each field
        validationErrorsList.forEach(function (element) {

            var validationErrorType = element.type;
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - validationErrorType: ' + validationErrorType);

            if (validationErrorType == 'Location') {
                locationTypeErrorsList = element.typeErrors;
            } else if (validationErrorType == 'BTD') {
                btdTypeErrorsList = element.typeErrors;
            } else if (validationErrorType == 'Product') {
                productTypeErrorsList = element.typeErrors;
            } else if (validationErrorType == 'Uni') {
                uniTypeErrorsList = element.typeErrors;
            } else if (validationErrorType == 'OVC') {
                ovcTypeErrorsList = element.typeErrors;
            }
        });

        if (locationTypeErrorsList != null) {
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - locationTypeErrorsList.length: ' + locationTypeErrorsList.length);

            /*** Process errors for Location node ***/
            var locationTypeFieldsList;
            var afterHoursAppointmentFieldErrorsList;

            locationTypeFieldsList = locationTypeErrorsList[0].fields;
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - locationTypeFieldsList.length: ' + locationTypeFieldsList.length);

            // Loop thru locationTypeFieldsList	and setup up sub lists for each field
            locationTypeFieldsList.forEach(function (element) {

                var fieldName = element.fieldName;
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - fieldName: ' + fieldName);

                if (fieldName == 'afterHoursAppointment') {
                    afterHoursAppointmentFieldErrorsList = element.fieldErrors;
                }

            });

            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - afterHoursAppointmentFieldErrorsList.length: ' + afterHoursAppointmentFieldErrorsList.length);

            // Process errors for field: afterHoursAppointment
            var afterHoursAppointmentFieldError;
            afterHoursAppointmentFieldError = afterHoursAppointmentFieldErrorsList[0].description;
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - afterHoursAppointmentFieldError: ' + afterHoursAppointmentFieldError);

            if (afterHoursAppointmentFieldError != null) {
                cmp.find("AfterHoursAppointment").focus();
                cmp.find("AfterHoursAppointment").set('v.messageWhenValueMissing', afterHoursAppointmentFieldError);
                cmp.find("AfterHoursAppointment").showHelpMessageIfInvalid();
                cmp.find("AfterHoursAppointment").set('v.validity', {
                    valid: false,
                    valueMissing: true
                });
            }
        }

        if (btdTypeErrorsList != null) {
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - btdTypeErrorsList.length: ' + btdTypeErrorsList.length);

            /*** Process errors for BTD node ***/
            var btdFieldsList;
            var btdTypeFieldErrorsList;

            btdFieldsList = btdTypeErrorsList[0].fields;
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - btdFieldsList.length: ' + btdFieldsList.length);

            // Loop thru btdFieldsList and setup up sub lists for each field
            btdFieldsList.forEach(function (element) {
                var fieldName = element.fieldName;
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - fieldName: ' + fieldName);

                if (fieldName == 'btdType') {
                    btdTypeFieldErrorsList = element.fieldErrors;
                }
            });

            // Process errors for field: btdType
            var btdTypeFieldError;
            btdTypeFieldError = btdTypeFieldErrorsList[0].description;
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - btdTypeFieldError: ' + btdTypeFieldError);

            // Note: Not displaying error on screen (check this)

            if (btdTypeFieldError != null) {
                // Need to set comp attributes for disabled UI fields
                cmp.set("v.btdTypeError", true);
                cmp.set("v.btdTypeErrorMsg", btdTypeFieldError);

                cmp.find("btdType").focus();
                cmp.find("btdType").set('v.messageWhenValueMissing', btdTypeFieldError);
                cmp.find("btdType").showHelpMessageIfInvalid();
                cmp.find("btdType").set('v.validity', {
                    valid: false,
                    valueMissing: true
                });
            }
        }

        if (productTypeErrorsList != null) {
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - productTypeErrorsList.length: ' + productTypeErrorsList.length);

            /*** Process errors for Product node ***/
            var productFieldsList; // node for all product fields

            var termFieldErrorsList; // error list for term field
            var serviceRestorationSLAFieldErrorsList; // error list for serviceRestorationSLA field
            var zoneFieldErrorsList; // error list for zone field

            productFieldsList = productTypeErrorsList[0].fields;

            // Loop thru productFieldsList (node) and setup sub lists for each field
            productFieldsList.forEach(function (element) {

                var fieldName = element.fieldName;
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - fieldName: ' + fieldName);

                if (fieldName == 'term') {
                    termFieldErrorsList = element.fieldErrors;
                } else if (fieldName == 'serviceRestorationSLA') {
                    serviceRestorationSLAFieldErrorsList = element.fieldErrors;
                } else if (fieldName == 'zone') {
                    zoneFieldErrorsList = element.fieldErrors;
                }
            });

            if (termFieldErrorsList != null) {
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - termFieldErrorsList.length: ' + termFieldErrorsList.length);

                // Process errors for field: term
                var termFieldError;
                termFieldError = termFieldErrorsList[0].description;
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - termFieldError: ' + termFieldError);

                if (termFieldError != null) {
                    cmp.find("UNITerm").focus();
                    cmp.find("UNITerm").set('v.messageWhenValueMissing', termFieldError);
                    cmp.find("UNITerm").showHelpMessageIfInvalid();
                    cmp.find("UNITerm").set('v.validity', {
                        valid: false,
                        valueMissing: true
                    });
                }
            }

            if (serviceRestorationSLAFieldErrorsList != null) {
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - serviceRestorationSLAFieldErrorsList.length: ' + serviceRestorationSLAFieldErrorsList.length);

                // Process errors for field: serviceRestorationSLA
                var serviceRestorationSLAFieldError;
                serviceRestorationSLAFieldError = serviceRestorationSLAFieldErrorsList[0].description;
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - serviceRestorationSLAFieldError: ' + serviceRestorationSLAFieldError);

                if (serviceRestorationSLAFieldError != null) {
                    cmp.find("SLA").focus();
                    cmp.find("SLA").set('v.messageWhenValueMissing', serviceRestorationSLAFieldError);
                    cmp.find("SLA").showHelpMessageIfInvalid();
                    cmp.find("SLA").set('v.validity', {
                        valid: false,
                        valueMissing: true
                    });
                }
            }

            if (zoneFieldErrorsList != null) {
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - zoneFieldErrorsList.length: ' + zoneFieldErrorsList.length);

                // Process errors for field: zone
                var zoneFieldError;
                zoneFieldError = zoneFieldErrorsList[0].description;
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - zoneFieldError: ' + zoneFieldError);

                if (zoneFieldError != null) {
                    // Need to set comp attributes for disabled UI fields
                    cmp.set("v.zoneError", true);
                    cmp.set("v.zoneErrorMsg", zoneFieldError);
                }
            }
        }

        if (uniTypeErrorsList != null) {
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - uniTypeErrorsList.length: ' + uniTypeErrorsList.length);

            /*** Process errors for Uni node ***/
            var uniFieldsList; // node for all Uni fields

            var interfaceBandwidthFieldErrorsList; // error list for interfaceBandwidth field
            var interfaceTypeFieldErrorsList; // error list for interfaceType field
            var ovcTypeFieldErrorsList; // error list for ovcType field
            var tpIdFieldErrorsList; // error list for tpId field
            var cosMappingModeFieldErrorsList; // error list for cosMappingMode field        

            uniFieldsList = uniTypeErrorsList[0].fields;
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - uniFieldsList.length: ' + uniFieldsList.length);

            // Loop thru productFieldsList (node) and setup sub lists for each field
            uniFieldsList.forEach(function (element) {
                var fieldName = element.fieldName;
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - fieldName: ' + fieldName);

                if (fieldName == 'interfaceBandwidth') {
                    interfaceBandwidthFieldErrorsList = element.fieldErrors;
                } else if (fieldName == 'interfaceType') {
                    interfaceTypeFieldErrorsList = element.fieldErrors;
                } else if (fieldName == 'ovcType') {
                    ovcTypeFieldErrorsList = element.fieldErrors;
                } else if (fieldName == 'tpId') {
                    tpIdFieldErrorsList = element.fieldErrors;
                } else if (fieldName == 'cosMappingMode') {
                    cosMappingModeFieldErrorsList = element.fieldErrors;
                }
            });

            if (interfaceBandwidthFieldErrorsList != null) {
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - interfaceBandwidthFieldErrorsList.length: ' + interfaceBandwidthFieldErrorsList.length);

                // Process errors for field: interfaceBandwidth
                var interfaceBandwidthFieldError;
                interfaceBandwidthFieldError = interfaceBandwidthFieldErrorsList[0].description;
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - interfaceBandwidthFieldError: ' + interfaceBandwidthFieldError);

                if (interfaceBandwidthFieldError != null) {
                    cmp.set("v.interfaceBandwidthError", true);
                    cmp.set("v.interfaceBandwidthErrorMsg", interfaceBandwidthFieldError);

                    cmp.find("InterfaceBandwidth").focus();
                    cmp.find("InterfaceBandwidth").set('v.messageWhenValueMissing', interfaceBandwidthFieldError);
                    cmp.find("InterfaceBandwidth").showHelpMessageIfInvalid();
                    cmp.find("InterfaceBandwidth").set('v.validity', {
                        valid: false,
                        valueMissing: true
                    });
                }
            }

            if (interfaceTypeFieldErrorsList != null) {
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - interfaceTypeFieldErrorsList.length: ' + interfaceTypeFieldErrorsList.length);

                // Process errors for field: interfaceType
                var interfaceTypeFieldError;
                interfaceTypeFieldError = interfaceTypeFieldErrorsList[0].description;
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - interfaceTypeFieldError: ' + interfaceTypeFieldError);

                if (interfaceTypeFieldError != null) {
                    cmp.find("InterfaceTypes").focus();
                    cmp.find("InterfaceTypes").set('v.messageWhenValueMissing', interfaceTypeFieldError);
                    cmp.find("InterfaceTypes").showHelpMessageIfInvalid();
                    cmp.find("InterfaceTypes").set('v.validity', {
                        valid: false,
                        valueMissing: true
                    });
                }
            }

            if (ovcTypeFieldErrorsList != null) {
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - ovcTypeFieldErrorsList.length: ' + ovcTypeFieldErrorsList.length);

                // Process errors for field: ovcType
                var ovcTypeFieldError;
                ovcTypeFieldError = ovcTypeFieldErrorsList[0].description;
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - ovcTypeFieldError: ' + ovcTypeFieldError);

                if (ovcTypeFieldError != null) {
                    cmp.find("OVCType").focus();
                    cmp.find("OVCType").set('v.messageWhenValueMissing', ovcTypeFieldError);
                    cmp.find("OVCType").showHelpMessageIfInvalid();
                    cmp.find("OVCType").set('v.validity', {
                        valid: false,
                        valueMissing: true
                    });
                }
            }

            if (tpIdFieldErrorsList != null) {
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - tpIdFieldErrorsList.length: ' + tpIdFieldErrorsList.length);

                // Process errors for field: tpId
                var tpIdFieldError;
                tpIdFieldError = tpIdFieldErrorsList[0].description;
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - tpIdFieldError: ' + tpIdFieldError);

                if (tpIdFieldError != null) {
                    // Need to set comp attributes for disabled UI fields
                    cmp.set("v.tpidError", true);
                    cmp.set("v.tpidErrorMsg", tpIdFieldError);

                    /*
                    cmp.find("TPID").focus();
                    cmp.find("TPID").set('v.messageWhenValueMissing', tpIdFieldError);
                    cmp.find("TPID").showHelpMessageIfInvalid();
                    cmp.find("TPID").set('v.validity', {
                        valid: false,
                        valueMissing: true
                    });
                    */
                }
            }

            if (cosMappingModeFieldErrorsList != null) {
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosMappingModeFieldErrorsList.length: ' + cosMappingModeFieldErrorsList.length);

                // Process errors for field: cosMappingMode
                var cosMappingModeFieldError;
                cosMappingModeFieldError = cosMappingModeFieldErrorsList[0].description;
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosMappingModeFieldError: ' + cosMappingModeFieldError);

                if (cosMappingModeFieldError != null) {
                    cmp.find("MappingMode").focus();
                    cmp.find("MappingMode").set('v.messageWhenValueMissing', cosMappingModeFieldError);
                    cmp.find("MappingMode").showHelpMessageIfInvalid();
                    cmp.find("MappingMode").set('v.validity', {
                        valid: false,
                        valueMissing: true
                    });
                }
            }
        }

        if (ovcTypeErrorsList != null) {
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - ovcTypeErrorsList.length: ' + ovcTypeErrorsList.length);

            // Get location
            var location = cmp.get("v.location");
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - location: ' + location);

            // Get all ovcs
            var OVCList = cmp.get("v.OVCList");
            console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - OVCList: ' + OVCList);

            /*** Process errors for OVC node ***/
            // Store strings of validation errors for each possible OVC		    
            // Loop thru each ovcFieldsList (each OVC) of the parent ovcTypeErrorsList and process all error strings
            ovcTypeErrorsList.forEach(function (element) {
                var ovcFieldsList; // node for all OVC fields	        	
                var ovcValidationErrorStr = ''; // Store UI Validation Errors string for an OVC instance

                // Declare lists to process each ovc
                var routeTypeFieldErrorsList; // error list for routeType field
                var poiFieldErrorsList; // error list for poi field
                var sVLanIdFieldErrorsList; // error list for sVLanId field
                var uniVLanIdFieldErrorsList; // error list for uniVLanId field
                var cosHighBandwidthFieldErrorsList; // error list for cosHighBandwidth field
                var cosMediumBandwidthFieldErrorsList; // error list for cosMediumBandwidth field
                var cosLowBandwidthFieldErrorsList; // error list for cosLowBandwidth field
                var cosBandwidthCombinedErrorsList; // error list for shared cosLowBandwidth / cosMediumBandwidth / cosHighBandwidth field errors
                var maximumFrameSizeFieldErrorsList; // error list for maximumFrameSize field
                var nniFieldErrorsList; // error list for nni field    			    	        	

                var ovcId = element.id; // This maps closely to OVC.Name eg. OVC 1
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - ovcId (OVCName): ' + ovcId);

                // Get fields list
                ovcFieldsList = element.fields;
                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - ovcFieldsList: ' + ovcFieldsList);

                // Loop thru ovcFieldsList (node) and setup sub lists for each field
                ovcFieldsList.forEach(function (element) {
                    var fieldName = element.fieldName;
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - fieldName: ' + fieldName);

                    if (fieldName == 'routeType') {
                        routeTypeFieldErrorsList = element.fieldErrors;
                    } else if (fieldName == 'poi') {
                        poiFieldErrorsList = element.fieldErrors;
                    } else if (fieldName == 'sVLanId') {
                        sVLanIdFieldErrorsList = element.fieldErrors;
                    } else if (fieldName == 'uniVLanId') {
                        uniVLanIdFieldErrorsList = element.fieldErrors;
                    } else if (fieldName == 'cosHighBandwidth') {
                        cosHighBandwidthFieldErrorsList = element.fieldErrors;
                    } else if (fieldName == 'cosMediumBandwidth') {
                        cosMediumBandwidthFieldErrorsList = element.fieldErrors;
                    } else if (fieldName == 'cosLowBandwidth') {
                        cosLowBandwidthFieldErrorsList = element.fieldErrors;
                    } else if (fieldName == 'cosLowBandwidth|cosMediumBandwidth|cosHighBandwidth') {
                        cosBandwidthCombinedErrorsList = element.fieldErrors;
                    } else if (fieldName == 'maximumFrameSize') {
                        maximumFrameSizeFieldErrorsList = element.fieldErrors;
                    } else if (fieldName == 'nni') {
                        nniFieldErrorsList = element.fieldErrors;
                    }
                });

                if (routeTypeFieldErrorsList != null) {
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - routeTypeFieldErrorsList.length: ' + routeTypeFieldErrorsList.length);

                    // Process errors for field: routeType
                    var routeTypeFieldError;
                    routeTypeFieldError = routeTypeFieldErrorsList[0].description;
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - routeTypeFieldError: ' + routeTypeFieldError);

                    if (routeTypeFieldError != null) {
                        // Add to newline seperated string
                        ovcValidationErrorStr = ovcValidationErrorStr + '<div>' + 'Field: routeType' + '<br>';
                        ovcValidationErrorStr = ovcValidationErrorStr + 'Error: ' + routeTypeFieldError + '<div/>' + '<br>';
                        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - routeTypeFieldError - has been appended to ovcValidationErrorStr... ');
                    }
                }

                if (poiFieldErrorsList != null) {
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - poiFieldErrorsList.length: ' + poiFieldErrorsList.length);

                    // Process errors for field: poi
                    var poiFieldError;
                    poiFieldError = poiFieldErrorsList[0].description;
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - poiFieldError: ' + poiFieldError);

                    if (poiFieldError != null) {
                        ovcValidationErrorStr = ovcValidationErrorStr + '<div>' + 'Field: poi' + '<br>';
                        ovcValidationErrorStr = ovcValidationErrorStr + 'Error: ' + poiFieldError + '<div/>' + '<br>';

                        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - poiFieldError - has been appended to ovcValidationErrorStr... ');
                    }
                }

                if (sVLanIdFieldErrorsList != null) {
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - sVLanIdFieldErrorsList.length: ' + sVLanIdFieldErrorsList.length);

                    // Process errors for field: sVLanId
                    var sVLanIdFieldError;
                    sVLanIdFieldError = sVLanIdFieldErrorsList[0].description;
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - sVLanIdFieldError: ' + sVLanIdFieldError);

                    if (sVLanIdFieldError != null) {
                        ovcValidationErrorStr = ovcValidationErrorStr + '<div>' + 'Field: sVLanId' + '<br>';
                        ovcValidationErrorStr = ovcValidationErrorStr + 'Error: ' + sVLanIdFieldError + '<div/>' + '<br>';
                        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - sVLanIdFieldError - has been appended to ovcValidationErrorStr... ');
                    }
                }

                if (uniVLanIdFieldErrorsList != null) {
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - uniVLanIdFieldErrorsList.length: ' + uniVLanIdFieldErrorsList.length);

                    // Process errors for field: uniVLanId
                    var uniVLanIdFieldError;
                    uniVLanIdFieldError = uniVLanIdFieldErrorsList[0].description;
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - uniVLanIdFieldError: ' + uniVLanIdFieldError);

                    if (uniVLanIdFieldError != null) {
                        ovcValidationErrorStr = ovcValidationErrorStr + '<div>' + 'Field: uniVLanId' + '<br>';
                        ovcValidationErrorStr = ovcValidationErrorStr + 'Error: ' + uniVLanIdFieldError + '<div/>' + '<br>';
                        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - uniVLanIdFieldError - has been appended to ovcValidationErrorStr... ');
                    }
                }

                if (cosHighBandwidthFieldErrorsList != null) {
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosHighBandwidthFieldErrorsList.length: ' + cosHighBandwidthFieldErrorsList.length);

                    // Process errors for field: cosHighBandwidth
                    var cosHighBandwidthFieldError;
                    cosHighBandwidthFieldError = cosHighBandwidthFieldErrorsList[0].description;
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosHighBandwidthFieldError: ' + cosHighBandwidthFieldError);

                    if (cosHighBandwidthFieldError != null) {
                        ovcValidationErrorStr = ovcValidationErrorStr + '<div>' + 'Field: cosHighBandwidth' + '<br>';
                        ovcValidationErrorStr = ovcValidationErrorStr + 'Error: ' + cosHighBandwidthFieldError + '<div/>' + '<br>';
                        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosHighBandwidthFieldError - has been appended to ovcValidationErrorStr... ');
                    }
                }

                if (cosMediumBandwidthFieldErrorsList != null) {
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosMediumBandwidthFieldErrorsList.length: ' + cosMediumBandwidthFieldErrorsList.length);

                    // Process errors for field: cosMediumBandwidth
                    var cosMediumBandwidthFieldError;
                    cosMediumBandwidthFieldError = cosMediumBandwidthFieldErrorsList[0].description;
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosMediumBandwidthFieldError: ' + cosMediumBandwidthFieldError);

                    if (cosMediumBandwidthFieldError != null) {
                        ovcValidationErrorStr = ovcValidationErrorStr + '<div>' + 'Field: cosMediumBandwidth' + '<br>';
                        ovcValidationErrorStr = ovcValidationErrorStr + 'Error: ' + cosMediumBandwidthFieldError + '<div/>' + '<br>';
                        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosMediumBandwidthFieldError - has been appended to ovcValidationErrorStr... ');
                    }
                }

                if (cosLowBandwidthFieldErrorsList != null) {
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosLowBandwidthFieldErrorsList.length: ' + cosLowBandwidthFieldErrorsList.length);

                    // Process errors for field: cosLowBandwidth
                    var cosLowBandwidthFieldError;
                    cosLowBandwidthFieldError = cosLowBandwidthFieldErrorsList[0].description;
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosLowBandwidthFieldError: ' + cosLowBandwidthFieldError);

                    if (cosLowBandwidthFieldError != null) {
                        ovcValidationErrorStr = ovcValidationErrorStr + '<div>' + 'Field: cosLowBandwidth' + '<br>';
                        ovcValidationErrorStr = ovcValidationErrorStr + 'Error: ' + cosLowBandwidthFieldError + '<div/>' + '<br>';
                        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosLowBandwidthFieldError - has been appended to ovcValidationErrorStr... ');
                    }
                }

                if (cosBandwidthCombinedErrorsList != null) {
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosBandwidthCombinedErrorsList.length: ' + cosBandwidthCombinedErrorsList.length);

                    // Process errors for field: cosBandwidthCombinedErrors
                    var cosBandwidthFieldError;
                    cosBandwidthFieldError = cosBandwidthCombinedErrorsList[0].description;
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosBandwidthFieldError: ' + cosBandwidthFieldError);

                    if (cosBandwidthFieldError != null) {
                        ovcValidationErrorStr = ovcValidationErrorStr + '<div>' + 'Field: cosLowBandwidth | cosMediumBandwidth | cosHighBandwidth' + '<br>';
                        ovcValidationErrorStr = ovcValidationErrorStr + 'Error: ' + cosBandwidthFieldError + '<div/>' + '<br>';
                        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - cosBandwidthFieldError - has been appended to ovcValidationErrorStr... ');
                    }
                }

                if (maximumFrameSizeFieldErrorsList != null) {
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - maximumFrameSizeFieldErrorsList.length: ' + maximumFrameSizeFieldErrorsList.length);

                    // Process errors for field: maximumFrameSize
                    var maximumFrameSizeFieldError;
                    maximumFrameSizeFieldError = maximumFrameSizeFieldErrorsList[0].description;
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - maximumFrameSizeFieldError: ' + maximumFrameSizeFieldError);

                    if (maximumFrameSizeFieldError != null) {
                        ovcValidationErrorStr = ovcValidationErrorStr + '<div>' + 'Field: maximumFrameSize' + '<br>';
                        ovcValidationErrorStr = ovcValidationErrorStr + 'Error: ' + maximumFrameSizeFieldError + '<div/>' + '<br>';
                        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - maximumFrameSizeFieldError - has been appended to ovcValidationErrorStr... ');
                    }
                }

                if (nniFieldErrorsList != null) {
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - nniFieldErrorsList.length: ' + nniFieldErrorsList.length);

                    // Process errors for field: nni
                    var nniFieldError;
                    nniFieldError = nniFieldErrorsList[0].description;
                    console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - nniFieldError: ' + nniFieldError);

                    if (nniFieldError != null) {
                        ovcValidationErrorStr = ovcValidationErrorStr + '<div>' + 'Field: nni' + '<br>';
                        ovcValidationErrorStr = ovcValidationErrorStr + 'Error: ' + nniFieldError + '<div/>' + '<br>';
                        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - nniFieldError - has been appended to ovcValidationErrorStr... ');
                    }
                }

                var index;

                // After processing all field errors for an OVC, add to each OVC dedicated var to store
                if (ovcId == 'OVC-1') {
                    index = 0;
                } else if (ovcId == 'OVC-2') {
                    index = 1;
                } else if (ovcId == 'OVC-3') {
                    index = 2;
                } else if (ovcId == 'OVC-4') {
                    index = 3;
                } else if (ovcId == 'OVC-5') {
                    index = 4;
                } else if (ovcId == 'OVC-6') {
                    index = 5;
                } else if (ovcId == 'OVC-7') {
                    index = 6;
                } else if (ovcId == 'OVC-8') {
                    index = 7;
                }

                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - index: ' + index);

                // Update the Validation Errors property (for UI)			    			    
                //var OVCInstance = OVCList[index];	    

                //console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - ovcValidationErrorStr: ' + ovcValidationErrorStr);

                // Update this OVC with validation error
                //OVCInstance.validationErrors = ovcValidationErrorStr;
                //OVCList[index] = OVCInstance; // added BG
                //cmp.set('v.OVCList',OVCList);// added BG
                //location.OVCs = OVCList;// added BG
                //cmp.set("v.location",location);// added BG
                var OVCInstance = OVCList.filter(function (e) {
                    return e.OVCName == ovcId;
                });

                console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - ovcValidationErrorStr: ' + ovcValidationErrorStr);

                // Update this OVC with validation error
                OVCInstance.validationErrors = ovcValidationErrorStr;

                for (var i = 0; i < OVCList.length; i++) {
                    if (OVCList[i].OVCName === OVCInstance[0].OVCName) {
                        OVCList[i].validationErrors = OVCInstance.validationErrors;
                        break;
                    }
                }

                //if(!added) arr.push(obj);

                //OVCList[index] = OVCInstance; // added BG
                cmp.set('v.OVCList', OVCList); // added BG
                location.OVCs = OVCList; // added BG
                cmp.set("v.location", location); // added BG

            });
        }

        cmp.set("v.uniBannertype", "Banner");
        cmp.set("v.responseStatusUNIPage", "ERROR");
        cmp.set("v.responseMessageUNIPage", errorMessage);

        cmp.set("v.saveValidationError", true);

        console.log('DF_SF_Order_Item.DF_SF_Order_ItemHelper.displayOrderValidatorErrors - END');
    },
    ovcCalculateBandWidth: function (cmp, event) {
        var selectedOVC = cmp.get("v.selectedOVC");
        var ovcCOSH = selectedOVC.coSHighBandwidth == "" || selectedOVC.coSHighBandwidth == "-" ? 0 : parseInt(selectedOVC.coSHighBandwidth);
        var ovcCOSM = selectedOVC.coSMediumBandwidth == "" || selectedOVC.coSMediumBandwidth == "-" ? 0 : parseInt(selectedOVC.coSMediumBandwidth);
        //var COSL = selectedOVC.coSLowBandwidth == "" || selectedOVC.coSLowBandwidth == "-" ? 0 : parseInt(selectedOVC.coSLowBandwidth);
        var cosValueMap = cmp.get("v.cosValueMap");
        var ovcCOSH_EIR = (ovcCOSH / 100) * cosValueMap["COS_H_EIR"];
        var ovcCOSM_EIR = (ovcCOSM / 100) * cosValueMap["COS_M_EIR"];
        var ovcCOSH_CIR = ovcCOSH - ovcCOSH_EIR;
        var ovcCOSM_CIR = ovcCOSM - ovcCOSM_EIR;
        var ovcTotalCIR = ovcCOSH_CIR + ovcCOSM_CIR;

        if (!isNaN(ovcTotalCIR)) {
            if (ovcTotalCIR >= 952) {
                cmp.set("v.ovcTotalCIR", 952 + " Mbps");
            } else {
                cmp.set("v.ovcTotalCIR", ovcTotalCIR + " Mbps");
            }
        }

        var OVCList = cmp.get("v.OVCList");

        var totalCIRonAllOVCs = 0;
        var totalPIRonAllOVCs = 0;
        var totalBandWidthonAllOVCs = 0;
        var ovcCount = 0;

        for (var index in OVCList) {
            var ovc = OVCList[index];
            if (typeof selectedOVC.OVCId != 'undefined' && ovc.OVCId === selectedOVC.OVCId) {
                ovc = selectedOVC;
            }
            var COSH = ovc.coSHighBandwidth == "" || ovc.coSHighBandwidth == "-" ? 0 : parseInt(ovc.coSHighBandwidth);
            var COSM = ovc.coSMediumBandwidth == "" || ovc.coSMediumBandwidth == "-" ? 0 : parseInt(ovc.coSMediumBandwidth);
            var COSL = ovc.coSLowBandwidth == "" || ovc.coSLowBandwidth == "-" ? 0 : parseInt(ovc.coSLowBandwidth);

            var COSH_EIR = (COSH / 100) * cosValueMap["COS_H_EIR"];
            var COSM_EIR = (COSM / 100) * cosValueMap["COS_M_EIR"];
            var COSL_EIR = (COSL / 100) * cosValueMap["COS_L_EIR"];

            var COSH_CIR = COSH - COSH_EIR;
            var COSM_CIR = COSM - COSM_EIR;
            var COSL_CIR = COSL - COSL_EIR;

            var totalOVCCIR = COSH_CIR + COSM_CIR; // + COSL_CIR;
            var totalOVCEIR = COSH_EIR + COSM_EIR; // + COSL_EIR;
            var totalOVCPIR = totalOVCCIR + totalOVCEIR;
            var totalBandWidth = COSH + COSM; // + COSL;

            if (COSH_CIR > 0 && COSM_CIR > 0) {
                if (totalOVCCIR > 952) {
                    cmp.set("v.totalCIRError", true);
                    return;
                } else {
                    cmp.set("v.totalCIRError", false);
                }
            }

            totalCIRonAllOVCs = totalCIRonAllOVCs + totalOVCCIR;
            totalPIRonAllOVCs = totalPIRonAllOVCs + totalOVCPIR;
            totalBandWidthonAllOVCs = totalBandWidthonAllOVCs + totalBandWidth;
            ovcCount++;
        }

        if (totalCIRonAllOVCs >= 952) {
            cmp.set("v.totalMaxCIR", 952 + " Mbps");
        } else {
            cmp.set("v.totalMaxCIR", totalCIRonAllOVCs + " Mbps");
        }

        if (totalCIRonAllOVCs > 1000 && ovcCount == 1) {
            cmp.set("v.totalCIRError", true);
        } else if (totalCIRonAllOVCs > 952 && ovcCount > 1) {
            cmp.set("v.totalCIRError", true);
        } else {
            //if(cmp.get("v.totalCIRError") != true)
            cmp.set("v.totalCIRError", false);
        }
        //cmp.set("v.ovcTotalBandwidth",totalBandWidthonAllOVCs+" Mbps");
    },

    ovcSaveValidation: function (cmp, event) {

        // var selectedPOI = cmp.find("POI").get("v.value");
        var ovc = cmp.get("v.selectedOVC");
        var hasPIRError = false;
        var hasCIRError = false;
        //var selectedPOI = ovc.POI;
        var areCOSNull = false;
        var isTotalBWLTInterfaceBandwidth = false;
        var isTotalEIRLTInterfaceBandwidth = false;
        //var isTotalCIRLTInterfaceBandwidth = false;
        var COSH = ovc.coSHighBandwidth == "" || ovc.coSHighBandwidth == "-" ? 0 : parseInt(ovc.coSHighBandwidth);
        var COSM = ovc.coSMediumBandwidth == "" || ovc.coSMediumBandwidth == "-" ? 0 : parseInt(ovc.coSMediumBandwidth);
        var COSL = ovc.coSLowBandwidth == "" || ovc.coSLowBandwidth == "-" ? 0 : parseInt(ovc.coSLowBandwidth);

        var COSHset = ovc.coSHighBandwidth == "" || ovc.coSHighBandwidth == 0 ? 0 : 1;
        var COSMset = ovc.coSMediumBandwidth == "" || ovc.coSMediumBandwidth == 0 ? 0 : 1;
        var COSLset = ovc.coSLowBandwidth == "" || ovc.coSLowBandwidth == 0 ? 0 : 1;
        var CoSValueCount = COSHset + COSMset + COSLset

        var cosValueMap = cmp.get("v.cosValueMap");
        var COSH_EIR = (COSH / 100) * cosValueMap["COS_H_EIR"];
        var COSM_EIR = (COSM / 100) * cosValueMap["COS_M_EIR"];
        var COSL_EIR = (COSL / 100) * cosValueMap["COS_L_EIR"];

        var COSH_CIR = COSH - COSH_EIR;
        var COSM_CIR = COSM - COSM_EIR;
        var COSL_CIR = COSL - COSL_EIR;

        var totalOVCCIR = COSH_CIR + COSM_CIR; // + COSL_CIR;
        var totalOVCEIR = COSH_EIR + COSM_EIR; // + COSL_EIR;
        var totalOVCPIR = totalOVCCIR + totalOVCEIR;
        var totalBandwidth = COSH + COSM; // + COSL;

        //cmp.set("v.ovcTotalPIR",totalOVCPIR);
        if (totalOVCCIR >= 952) {
            cmp.set("v.ovcTotalCIR", 952);
        } else {
            cmp.set("v.ovcTotalCIR", totalOVCCIR);
        }
        //cmp.set("v.ovcTotalBandwidth",totalBandwidth);
        /*var COSH_CIR = COSH;
        var COSM_CIR = COSM/2;
        var COSL_CIR = 0;*/


        var errorMessage = "";

        if (COSH === 0 && COSM === 0 && COSL === 0) {
            areCOSNull = true;
            cmp.set("v.areOVCCosEmpty", true);
        } else {
            cmp.set("v.areOVCCosEmpty", false);
        }


        if (areCOSNull) {
            errorMessage = $A.get("$Label.c.DF_All_COS_Empty");
            cmp.find("CoSHighBandwidth").focus();
        }

        // if(totalOVCCIR > ((1024/100)*70)){
        // 	if(errorMessage !="" && hasCIRError == false) {
        // 		errorMessage =errorMessage + "<br>" + $A.get("$Label.c.DF_Total_OVC_CIR_GT_70_Percent");
        //         hasCIRError = true;
        // 	}
        // 	else if(errorMessage =="") {
        // 		errorMessage =$A.get("$Label.c.DF_Total_OVC_CIR_GT_70_Percent");
        //        hasCIRError = true;
        // 	}
        // }
        this.ovcCalculateBandWidth(cmp, event);
        var totalAllOVCCIR = cmp.get("v.totalCIRError");
        if (totalOVCCIR > 1000 || totalAllOVCCIR) {
            //if(errorMessage !="" && hasPIRError === false) {
            errorMessage = errorMessage + $A.get("$Label.c.DF_Total_CIR_must_be_equal_or_less_than_interface_bandwidth");
            hasPIRError = true;
            //}
            //else if(errorMessage =="") {
            //	errorMessage =$A.get("$Label.c.DF_Total_OVC_PIR_GT_Interface_bandwidth");
            //    hasPIRError = true;
            //}
        }


        //Check if Total CIR on all OVCS is <= 700 Mbps
        //this.isTotalCIRLTSeventyTotalBandwidth(cmp);
        //alert(cmp.get("v.isTotalCIRLT70PerTotalBW"));
        //if (cmp.get("v.isTotalCIRLT70PerTotalBW")) {
        //   if (errorMessage != "" && hasCIRError === false) {
        //       errorMessage += "<br>" + $A.get("$Label.c.DF_Total_OVC_CIR_GT_70_Percent");
        //       hasCIRError = true;
        //   } else if(errorMessage ==""){
        //       errorMessage = $A.get("$Label.c.DF_Total_OVC_CIR_GT_70_Percent");
        //       hasCIRError = true;
        //  }
        //}

        //if (cmp.get("v.isTotalPIRLT1p0PerTotalBW")) {
        //    if (errorMessage != "" && hasPIRError === false) {
        //        errorMessage += "<br>" + $A.get("$Label.c.DF_Total_OVC_PIR_GT_Interface_bandwidth");
        //        hasPIRError = true;
        //    } else if(errorMessage ==""){
        //       errorMessage = $A.get("$Label.c.DF_Total_OVC_PIR_GT_Interface_bandwidth");
        //      hasPIRError = true;
        //   }
        // }
        if (CoSValueCount <= 1) {

        }
        var rangeCount = this.countCEVLANIDRanges(cmp);
        if (rangeCount > 8) {
            errorMessage = $A.get("$Label.c.DF_Too_Many_VLAN_Id_Ranges_for_UNI");
        }


        if (errorMessage !== "") {
            cmp.set("v.ovcSaveValidationError", true);
            cmp.set("v.type", "Banner");
            cmp.set("v.responseStatus", "ERROR");
            cmp.set("v.responseMessage", errorMessage);
            cmp.find("CoSHighBandwidth").focus();
            console.log('ovcSaveValidation ' + errorMessage);
        } else {
            cmp.set("v.ovcSaveValidationError", false);
            cmp.set("v.responseStatus", "");
            cmp.set("v.responseMessage", "");
            cmp.set("v.type", "");
        }


        this.validateCEVLAINID(cmp);
        this.validateRouteType(cmp, event);

        var allValid = this.isAllFieldsValid(cmp, 'ovcField');
        var mappingModeValid = this.isOVCMappingModeValid(cmp);

        event.getSource().focus();
        if (!allValid || !mappingModeValid || cmp.get("v.duplicatedVLANId") || !cmp.get("v.uniVLANValid")) {
            cmp.set("v.ovcSaveValidationError", true);

        }
        if (!mappingModeValid) {

            var ctrl = cmp.find("MappingMode");
            ctrl.set('v.validity', {
                valid: false,
                badInput: true
            });
            cmp.find("MappingMode").focus();
        }

    },

    validateOVCCos: function (cmp) {
        var areCOSNull = false;
        var isTotalBWLTInterfaceBandwidth = false;
        var isTotalEIRLTInterfaceBandwidth = false;
        //var isTotalCIRLTInterfaceBandwidth = false;

        for (var index in cmp.get("v.OVCList")) {
            var ovc = cmp.get("v.OVCList")[index];
            var COSH = ovc.coSHighBandwidth == "" || ovc.coSHighBandwidth == "-" ? 0 : parseInt(ovc.coSHighBandwidth);
            var COSM = ovc.coSMediumBandwidth == "" || ovc.coSMediumBandwidth == "-" ? 0 : parseInt(ovc.coSMediumBandwidth);
            var COSL = ovc.coSLowBandwidth == "" || ovc.coSLowBandwidth == "-" ? 0 : parseInt(ovc.coSLowBandwidth);


            if (COSH === 0 && COSM === 0 && COSL === 0) {
                areCOSNull = true;
            }
        }

        if (areCOSNull) {
            cmp.set("v.areOVCCosEmpty", true);
        } else {
            cmp.set("v.areOVCCosEmpty", false);
            //this.isTotalCIRLTSeventyTotalBandwidth(cmp);
        }
    },
    validateOVCNonBillable: function (cmp) {
        var areOVCValuesMissing = false;
        //check ovc type for access EVPL and validate univlan value if empty
        var selectedOVCType = cmp.get("v.selectedUNI.oVCType");
        for (var index in cmp.get("v.OVCList")) {
            var ovc = cmp.get("v.OVCList")[index];
            if (typeof ovc.NNIGroupId === 'undefined' || ovc.NNIGroupId == null || ovc.NNIGroupId == 'Select') {
                areOVCValuesMissing = true;
            } else if (typeof ovc.sTag === 'undefined' || ovc.sTag == null || ovc.sTag == 'Select') {
                areOVCValuesMissing = true;
            } else if (selectedOVCType == "Access EVPL" &&
                (typeof ovc.ceVlanId === 'undefined' || ovc.ceVlanId == "" || ovc.ceVlanId == null ||
                    ovc.ceVlanId == 'Select')) {
                areOVCValuesMissing = true;
            } else if (typeof ovc.ovcMaxFrameSize === 'undefined' || ovc.ovcMaxFrameSize == null || ovc.ovcMaxFrameSize == 'Select') {
                areOVCValuesMissing = true;
            }
        }
        if (areOVCValuesMissing) {
            cmp.set("v.areOVCNonBillableEmpty", true);
        } else {
            cmp.set("v.areOVCNonBillableEmpty", false);
        }



    },

    isTotalCIRLTSeventyTotalBandwidth: function (cmp) {
        var OVCList = cmp.get("v.OVCList");
        var selectedOVC = cmp.get("v.selectedOVC");
        var totalCIRonAllOVCs = 0;
        var totalPIRonAllOVCs = 0;
        var isTotalCIRLTInterfaceBandwidth = false;
        var isTotalPIRLT1p0PerTotalBW = false;
        var cosValueMap = cmp.get("v.cosValueMap");
        console.log(OVCList);
        console.log(cmp.get("v.selectedOVC"));
        for (var index in OVCList) {
            var ovc = OVCList[index];
            if (ovc.OVCId === selectedOVC.OVCId) {
                ovc = selectedOVC;
            }
            var COSH = ovc.coSHighBandwidth == "" || ovc.coSHighBandwidth == "-" ? 0 : parseInt(ovc.coSHighBandwidth);
            var COSM = ovc.coSMediumBandwidth == "" || ovc.coSMediumBandwidth == "-" ? 0 : parseInt(ovc.coSMediumBandwidth);
            var COSL = ovc.coSLowBandwidth == "" || ovc.coSLowBandwidth == "-" ? 0 : parseInt(ovc.coSLowBandwidth);

            var cosValueMap = cmp.get("v.cosValueMap");
            var COSH_EIR = (COSH / 100) * cosValueMap["COS_H_EIR"];
            var COSM_EIR = (COSM / 100) * cosValueMap["COS_M_EIR"];
            var COSL_EIR = (COSL / 100) * cosValueMap["COS_L_EIR"];

            var COSH_CIR = COSH - COSH_EIR;
            var COSM_CIR = COSM - COSM_EIR;
            var COSL_CIR = COSL - COSL_EIR;

            var totalOVCCIR = COSH_CIR + COSM_CIR + COSL_CIR;
            var totalOVCEIR = COSH_EIR + COSM_EIR + COSL_EIR;
            var totalOVCPIR = totalOVCCIR + totalOVCEIR;

            /*
            var COSH_CIR = ovc.coSHighBandwidth == "" || ovc.coSHighBandwidth == "-"?0:(parseInt(ovc.coSHighBandwidth)/100)*cosValueMap["COS_H_CIR"];
            var COSM_CIR = ((ovc.coSMediumBandwidth == "" || ovc.coSMediumBandwidth == "-"?0:parseInt(ovc.coSMediumBandwidth))/100)*cosValueMap["COS_M_CIR"];
            var COSL_CIR = 0;
            */

            totalCIRonAllOVCs = totalCIRonAllOVCs + totalOVCCIR;
            totalPIRonAllOVCs = totalPIRonAllOVCs + totalOVCPIR
        }
        //alert(totalCIRonAllOVCs);
        if (totalCIRonAllOVCs > ((1024 / 100) * 70)) {
            isTotalCIRLTInterfaceBandwidth = true;
            cmp.set("v.isTotalCIRLT70PerTotalBW", true);
            //alert(totalCIR);
        } else {
            isTotalCIRLTInterfaceBandwidth = false;
            cmp.set("v.isTotalCIRLT70PerTotalBW", false);
        }

        if (totalPIRonAllOVCs > 1024) {
            isTotalPIRLT1p0PerTotalBW = true;
            cmp.set("v.isTotalPIRLT1p0PerTotalBW", true);
            //alert(totalCIR);
        } else {
            isTotalPIRLT1p0PerTotalBW = false;
            cmp.set("v.isTotalPIRLT1p0PerTotalBW", false);
        }

        /*if(isTotalCIRLTInterfaceBandwidth) {
        	cmp.set("v.isTotalCIRLT70PerTotalBW", true);
        }*/

    },

    initialValidation: function (cmp, event) {
        this.updateTPIDStatus(cmp);
    },

    updateTPIDStatus: function (cmp) {
        var selectedOVCType = cmp.get("v.selectedUNI.oVCType");
        if (selectedOVCType == "Access EPL") {
            //Disable TPID
            //Handles EPL sets default tpid val
            //cmp.set("v.selectedUNI.tPID", "0x1800");
            // cmp.set("v.disableTPID", true);
            var eplOpts = [{
                value: "0x8100",
                label: "0x8100"
            }];
            cmp.set("v.selectedUNI.tPID", "0x8100");
            cmp.set("v.TPIDOptions", eplOpts);
            cmp.set("v.disableTPID", true);
            $A.util.removeClass(cmp.find("TPID"), "slds-has-error"); // remove red border
            $A.util.addClass(cmp.find("TPID"), "hide-error-message");
        } else {
            //Enable TPID 
            //reload options from 
            cmp.set("v.disableTPID", false);
            var evplOptions = cmp.get("v.TPIDOptionsStatic");
            //cmp.set("v.selectedUNI.tPID","Select" );
            //cmp.find("TPID").set("v.value","");
            cmp.set("v.TPIDOptions", evplOptions);
        }
    },

    loadBasketId: function (cmp) {
        var orderJSON = cmp.get("v.location");
        cmp.set("v.orderJSON", orderJSON);
        var action = cmp.get("c.getBasketId");
        var basketID;
        var errorMessage = "";

        action.setParams({
            quoteId: cmp.get("v.location").id
        });

        action.setCallback(this, function (response) {

            var state = response.getState();
            if (state === "SUCCESS") {
                //cmp.set("v.showLoadingSpinner", true);
                basketID = response.getReturnValue();
                console.debug("====DF_SF_ORDER_ITEM_ HELPER ====LoadbasketId = " + basketID);
                if (basketID != "Error") {
                    var location = cmp.get("v.location");

                    //CPST-7198						  
                    this.apex(cmp, 'v.isSamZoneMappingUpdated', 'c.isBasketZoneMappingUpdated', {
                            basketId: basketID
                        }, false)
                        .catch(function (result) {
                            var cmp = result.sourceCmp;
                            var helper = result.sourceHelper;
                            helper.showUniErrorMessage(cmp, errorMessage);
                        });

                    location.basketId = basketID;
                    cmp.set('v.location', location);
                    this.loadCSOptions(cmp);

                    this.loadNonBillableOptions(cmp);
                    this.loadNonBillableValues(cmp);

                    cmp.set("v.responseStatusUNIPage", "");
                    cmp.set("v.responseMessageUNIPage", "");
                    cmp.set("v.uniBannertype", "");

                } else if (basketID === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showLoadingSpinner", false);
                }

            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showLoadingSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },

    loadCSOptions: function (cmp) {

        var location = cmp.get('v.location');

        var uniAttrList = this.getCSOptionNameArray('uni');
        var ovcAttrList = this.getCSOptionNameArray('ovc');
        this.loadCSOptionsByAttrList(cmp, uniAttrList, ovcAttrList, location.basketId);
    },

    loadNonBillableOptions: function (cmp) {
        var action = cmp.get("c.getNonBillableOptionsList");
        var errorMessage = "";

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                if (res != "Error") {
                    var results = JSON.parse(response.getReturnValue());
                    // console.log('======= loadNonBillableOptions ======= results ' + JSON.stringify(results));
                    //NONBILLING_ATTR_CONFIG -nbAttr
                    this.NONBILLING_ATTR_CONFIG.map(function (item) {
                        //console.log('======= loadNonBillableOptions ======= item ' + JSON.stringify(item));
                        if (item.slOptionAttr && results[item.nbAttr]) {
                            //console.log('======= loadNonBillableOptions ======= item.slOptionAttr && results[item.nbAttr]');
                            //Add Default Val
                            var opts = [{
                                value: "",
                                label: "Select"
                            }];
                            var fullOpts = opts.concat(results[item.nbAttr].map(function (result) {
                                return {
                                    label: result,
                                    value: result
                                };
                            }));
                            console.debug('======= loadNonBillableOptions ======= OPTS ' + JSON.stringify(fullOpts));
                            cmp.set(item.slOptionAttr, fullOpts);
                            //cmp.set(item.slOptionAttr, results[item.nbAttr].map(function (result) {
                            //    return {
                            //        label: result,
                            //        value: result
                            //    };
                            //}));
                        }
                    });
                }

            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
                console.log('ERROR loadNonBillableOptions ' + errorMessage);
            }

        });

        $A.enqueueAction(action);
    },

    loadNonBillableValues: function (cmp) {
        console.log('======= loadNonBillableValues ======= Start');
        var action = cmp.get("c.getNonBillableOptionsValues");
        var errorMessage = "";
        var location = cmp.get('v.location');
        var orderJSON = cmp.get("v.orderJSON");
        var dfOrderId = location.OrderId

        action.setParams({
            dfOrderId: dfOrderId
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            var selectedUNI = cmp.get("v.selectedUNI");
            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                if (res != "Error") {
                    var results = JSON.parse(response.getReturnValue());
                    //console.log('======= loadNonBillableValues ======= results ' + JSON.stringify(results));
                    selectedUNI.interfaceTypes = results["Interface Type"][0];
                    selectedUNI.tPID = results["TPID"][0];
                    selectedUNI.oVCType = results["OVC Type"][0];
                    //selectedUNI.mappingMode = results["Mapping Mode"][0];
                    location.UNI = [selectedUNI];
                    orderJSON.UNI = [selectedUNI];
                    cmp.set("v.selectedUNI", selectedUNI);
                    cmp.set("v.location", location);
                    cmp.set("v.orderJSON", orderJSON);
                    this.updateTPIDStatus(cmp);
                }

            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
                console.log('ERROR loadNonBillableValues ' + errorMessage);
            }

        });

        $A.enqueueAction(action);
    },

    getCSOptionNameArray: function (objectType) {
        return this.ATTR_CONFIG.reduce(function (list, item) {
            if (item.objectType === objectType && item.slOptionAttr) {
                list.push(item.csAttr);
            } //Get CloudSense attr name by object type								
            return list;
        }, []);
    },

    loadCSOptionsByAttrList: function (cmp, attrNameList, ovcAttrList, basketId) {
        console.log('=====loadCSOptionsByAttrList===== BEGIN');
        var action = cmp.get("c.getOptionsList");
        var errorMessage = "";

        action.setParams({
            basketId: basketId,
            attNameList: attrNameList,
            ovcAttrList: ovcAttrList
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                console.log('=====loadCSOptionsByAttrList===== state ' + state);
                var res = response.getReturnValue();
                if (res != "Error") {
                    var results = JSON.parse(response.getReturnValue());
                    var csOptionAttrList = this.getCSOptionNameArray(null);
                    //console.log('=====loadCSOptionsByAttrList===== results' + JSON.stringify(results));
                    this.ATTR_CONFIG.map(function (item) {
                        if (item.slOptionAttr && results[item.csAttr]) {

                            cmp.set(item.slOptionAttr, results[item.csAttr].map(function (result) {
                                return {
                                    label: result.Name == '--None--' ? 'None' : result.Name,
                                    value: result.cscfga__Value__c
                                };
                            }));
                        }
                    });

                    this.loadCSConfig(cmp);

                    cmp.set("v.responseStatusUNIPage", "");
                    cmp.set("v.responseMessageUNIPage", "");
                    cmp.set("v.uniBannertype", "");
                } else if (res === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showModalLoadingSpinner", false);
                    console.log('ERROR loadCSOptionsByAttrList ' + errorMessage);

                }
            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
                console.log('ERROR loadCSOptionsByAttrList ' + errorMessage);
            }
        });
        $A.enqueueAction(action);
    },

    showUniErrorMessage: function (cmp, errorMessage) {
        cmp.set("v.uniBannertype", "Banner");
        cmp.set("v.responseStatusUNIPage", "ERROR");
        cmp.set("v.responseMessageUNIPage", errorMessage);
        cmp.set("v.showModalLoadingSpinner", false);
    },

    resetUNIMessage: function (cmp) {
        cmp.set("v.responseStatusUNIPage", "");
        cmp.set("v.responseMessageUNIPage", "");
        cmp.set("v.uniBannertype", "");
    },

    loadCSConfig: function (cmp) {
        var action = cmp.get("c.getExistingQuickQuoteData");
        var responseVal;
        var errorMessage = $A.get("$Label.c.DF_Application_Error");

        action.setParams({
            quoteId: cmp.get("v.location").id
        });

        action.setCallback(this, function (response) {

            var state = response.getState();
            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results != "Error") {
                    responseVal = JSON.parse(response.getReturnValue());

                    var extractedProductConfig = responseVal.ProductConfigMap;
                    cmp.set("v.structure", extractedProductConfig);

                    var attributes = {};
                    var configs = {};
                    for (var key in extractedProductConfig) {
                        if (extractedProductConfig.hasOwnProperty(key)) {
                            if (key.indexOf('-attributes') > -1) {
                                attributes[key.substr(0, key.indexOf('-attributes'))] = extractedProductConfig[key];
                            } else {
                                configs[key] = extractedProductConfig[key];
                            }
                        }
                    }

                    for (var key in attributes) {
                        if (attributes.hasOwnProperty(key)) {
                            configs[key].atts = attributes[key];
                        }
                    }

                    var configList = [];
                    for (var key in configs) {
                        if (configs.hasOwnProperty(key)) {
                            configList.push(configs[key]);
                        }
                    }

                    cmp.set("v.configs", configList);
                    this.resetUNIMessage(cmp);
                    this.loadCSToUI(cmp, responseVal);

                } else if (results === "Error") {
                    this.showUniErrorMessage(cmp, errorMessage);
                    console.log('ERROR loadCSConfig ' + errorMessage);
                }

            } else if (state === "ERROR") {
                this.showUniErrorMessage(cmp, errorMessage);
                console.log('ERROR loadCSConfig ' + errorMessage);
            }
        });
        $A.enqueueAction(action);
    },

    sortOVC: function (a, b) {
        if (a.CreatedDate < b.CreatedDate)
            return -1;
        if (a.CreatedDate > b.CreatedDate)
            return 1;
        return 0;
    },

    loadCSToUI: function (cmp, responseVal) {
        console.debug('=====loadCSToUI ===== START ');
        var configList = cmp.get("v.configs");
        var location = cmp.get("v.location");
        var OVCList = [];
        var selectedUNI = cmp.get("v.selectedUNI");
        var selectedOVC = cmp.get("v.selectedOVC");

        location.address = responseVal.Address;
        location.locId = responseVal.LocId;
        location.basketId = responseVal.BasketId;
        location.quoteId = responseVal.QuoteId;
        location.FBC = responseVal.Fbc;
        location.SCNR = location.SCNR !== responseVal.Scnr ? responseVal.Scnr : location.SCNR;
        location.SCR = location.SCR !== responseVal.Scr ? responseVal.Scr : location.SCR;
        location.DealDescription = responseVal.DealDescription;
        location.EtpApplies = responseVal.EtpApplies;

        var prodChargeName = 'Direct Fibre - Product Charges';

        var tempOVCList = configList.filter(function (item) {
            return item.Name != prodChargeName;
        });

        var tempProdChargeList = configList.filter(function (item) {
            return item.Name == prodChargeName;
        });

        var sortedTempOVCList = tempOVCList.sort(this.sortOVC);

        if (tempProdChargeList.length > 0) {
            var configAtts = tempProdChargeList[0].atts;
            selectedUNI.Id = tempProdChargeList[0].Id;
            selectedUNI.interfaceBandwidth = configAtts.filter(this.getAttByName('Interface Bandwidth'))[0].cscfga__Value__c;
            selectedUNI.BTDType = configAtts.filter(this.getAttByName('BTD Type'))[0].cscfga__Value__c;
            selectedUNI.AHA = configAtts.filter(this.getAttByName('After Hours Site Visit'))[0].cscfga__Value__c;
            selectedUNI.zone = configAtts.filter(this.getAttByName('Zone'))[0].cscfga__Value__c;
            selectedUNI.SLA = configAtts.filter(this.getAttByName('eSLA'))[0].cscfga__Value__c;
            selectedUNI.term = configAtts.filter(this.getAttByName('Term'))[0].cscfga__Value__c;

            location.SCNR = configAtts.filter(this.getAttByName('Once off charge'))[0].cscfga__Value__c;
            location.SCR = configAtts.filter(this.getAttByName('Recurring charge'))[0].cscfga__Value__c;
        }

        for (var i = 0; i < sortedTempOVCList.length; i++) {

            var configAtts = sortedTempOVCList[i].atts;

            if (selectedOVC.OVCId == sortedTempOVCList[i].Id) {
                selectedOVC.coSHighBandwidth = configAtts.filter(this.getAttByName('CoS High'))[0].cscfga__Value__c;
                selectedOVC.coSMediumBandwidth = configAtts.filter(this.getAttByName('CoS Medium'))[0].cscfga__Value__c;
                selectedOVC.coSLowBandwidth = configAtts.filter(this.getAttByName('CoS Low'))[0].cscfga__Value__c;

                if (typeof (configAtts.filter(this.getAttByName('Total_Routing_Charge_afterDiscount'))[0].cscfga__Value__c) != "undefined") {
                    selectedOVC.routeRecurring = configAtts.filter(this.getAttByName('Total_Routing_Charge_afterDiscount'))[0].cscfga__Value__c;
                } else {
                    selectedOVC.routeRecurring = "0.0";
                }
                if (typeof (configAtts.filter(this.getAttByName('Hidden_TotalCoS_Charges'))[0].cscfga__Value__c) != "undefined") {
                    selectedOVC.coSRecurring = configAtts.filter(this.getAttByName('Hidden_TotalCoS_Charges'))[0].cscfga__Value__c;
                } else {
                    selectedOVC.coSRecurring = "0.0";
                }
                selectedOVC.coSRecurring = configAtts.filter(this.getAttByName('Hidden_TotalCoS_Charges'))[0].cscfga__Value__c;
                selectedOVC.routeType = configAtts.filter(this.getAttByName('Route Type'))[0].cscfga__Value__c;
                selectedOVC.POI = configAtts.filter(this.getAttByName('POI'))[0].cscfga__Value__c;
                selectedOVC.CSA = configAtts.filter(this.getAttByName('CSA'))[0].cscfga__Value__c;
                selectedOVC.OVCName = sortedTempOVCList[i].Name;
                console.debug('=====loadCSToUI ===== selectedOVC ' + selectedOVC);
                OVCList.push(selectedOVC);
            } else {
                var tempRR;
                if (typeof (configAtts.filter(this.getAttByName('Total_Routing_Charge_afterDiscount'))[0].cscfga__Value__c) != "undefined") {
                    tempRR = configAtts.filter(this.getAttByName('Total_Routing_Charge_afterDiscount'))[0].cscfga__Value__c;
                } else {
                    tempRR = "0.0";
                }
                var tempCoSR;
                if (typeof (configAtts.filter(this.getAttByName('Hidden_TotalCoS_Charges'))[0].cscfga__Value__c) != "undefined") {
                    tempCoSR = configAtts.filter(this.getAttByName('Hidden_TotalCoS_Charges'))[0].cscfga__Value__c;
                } else {
                    tempCoSR = "0.0";
                }

                var newOVC = {
                    'OVCId': sortedTempOVCList[i].Id,
                    'OVCName': sortedTempOVCList[i].Name,
                    'CSA': configAtts.filter(this.getAttByName('CSA'))[0].cscfga__Value__c,
                    'monthlyCombinedCharges': configAtts.filter(this.getAttByName('OVC Recurring Charge'))[0].cscfga__Value__c,
                    'routeType': configAtts.filter(this.getAttByName('Route Type'))[0].cscfga__Value__c,
                    'coSHighBandwidth': configAtts.filter(this.getAttByName('CoS High'))[0].cscfga__Value__c,
                    'coSMediumBandwidth': configAtts.filter(this.getAttByName('CoS Medium'))[0].cscfga__Value__c,
                    'coSLowBandwidth': configAtts.filter(this.getAttByName('CoS Low'))[0].cscfga__Value__c,

                    'routeRecurring': tempRR, //configAtts.filter(this.getAttByName('Total_Routing_Charge_afterDiscount'))[0].cscfga__Value__c,
                    'coSRecurring': tempCoSR, // configAtts.filter(this.getAttByName('Hidden_TotalCoS_Charges'))[0].cscfga__Value__c,
                    'POI': configAtts.filter(this.getAttByName('POI'))[0].cscfga__Value__c,
                    'CSA': configAtts.filter(this.getAttByName('CSA'))[0].cscfga__Value__c,
                    'status': 'Incomplete'
                };

                OVCList.push(newOVC);
            }
        }


        location.OVCs = OVCList;
        location.UNI = [selectedUNI];
        location.prodChargeConfigId = selectedUNI.Id;
        location.ovcConfigId = OVCList[0].OVCId;
        //console.debug('=====loadCSToUI ===== OVCList ' +JSON.stringify(OVCList));
        var prodchargeConfigurationId = selectedUNI.Id;
        var csa = OVCList[0].CSA;

        cmp.set("v.OVCList", OVCList);
        cmp.set('v.location', location);
        cmp.set("v.selectedUNI", selectedUNI);
        cmp.set("v.selectedOVC", selectedOVC);
        cmp.set("v.prodchargeConfigurationId", prodchargeConfigurationId);
        cmp.set("v.csa", csa);
        console.log('Final spinner off');
        //this.saveOVCNonBillable(cmp);
        this.calculatetotals(cmp);
        this.loadOVCNonBillableToUI(cmp); //moved to end of calctotals
        this.ovcCalculateBandWidth(cmp, "");
    },

    getAttByName: function (name) {
        return function (item) {
            return item.Name == name;
        }
    },

    calculatetotals: function (cmp) {

        var location = cmp.get('v.location');
        var basketIdVal = location.basketId;
        var prodChargeID = cmp.get('v.prodchargeConfigurationId');
        var errorMessage = "";
        var OVCList = cmp.get("v.OVCList");
        var configList = cmp.get("v.configs");
        var Unizoneprice = 0;
        var DBPZoneDiscount = 0;
        var ZoneTermDiscount = 0;
        var tempProdChargeList = configList.filter(function (item) {
            return item.Name == 'Direct Fibre - Product Charges';
        });
        var configAtts = tempProdChargeList[0].atts;
        configAtts.forEach(function (element) {
            if (element.Name == 'Hidden_Zone_Charges' && element.cscfga__Value__c != null && element.cscfga__Is_Line_Item__c) {
                Unizoneprice = parseFloat(element.cscfga__Value__c);
            }
            if (element.Name == 'Hidden_DBP_Zone_Discount' && element.cscfga__Value__c != null && element.cscfga__Is_Line_Item__c) {
                DBPZoneDiscount = parseFloat(element.cscfga__Value__c);
            }
            if (element.Name == 'Zone_Term_Discount_Recurring' && element.cscfga__Value__c != null && element.cscfga__Is_Line_Item__c) {
                ZoneTermDiscount = parseFloat(element.cscfga__Value__c);
            }
        });
        var ovcSCR = OVCList.reduce(function (sum, item) {
            return (sum * 1) + (Math.round(parseFloat((item.routeRecurring * Math.pow(10, 2)).toFixed(2))) / Math.pow(10, 2) * 1) + (Math.round(parseFloat((item.coSRecurring * Math.pow(10, 2)).toFixed(2))) / Math.pow(10, 2) * 1);
        }, 0);

        var ovcCoSCharge = OVCList.reduce(function (sum, item) {
            return (sum * 1) + (Math.round(parseFloat((item.coSRecurring * Math.pow(10, 2)).toFixed(2))) / Math.pow(10, 2) * 1);
        }, 0);
        ovcCoSCharge = ovcCoSCharge + Unizoneprice - DBPZoneDiscount - ZoneTermDiscount;
        //Added for DBP
        var OVCTotalBws = OVCList.reduce(function (sum, item) {
            return (sum * 1) + (Math.round(parseFloat(((parseFloat(item.coSHighBandwidth) + parseFloat(item.coSMediumBandwidth) + parseFloat(item.coSLowBandwidth)) * Math.pow(10, 2)).toFixed(2))) / Math.pow(10, 2) * 1);
        }, 0);
        //Ends for DBP
        if (isNaN(ovcSCR) && ovcSCR != null) {
            ovcSCR = null;
        }
        console.log('Hidden ovc charges: ');
        console.log(ovcSCR);

        var action = cmp.get("c.updateHiddenOVCCharges");
        var errorMessage = "";

        action.setParams({
            basketId: location.basketId,
            configId: prodChargeID,
            totalOVC: ovcSCR,
            totalCoSBw: OVCTotalBws,
            totalOVCCosCharges: ovcCoSCharge
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results != "Error") {
                    location.SCR = results;
                    cmp.set('v.location', location);
                    this.loadOVCNonBillableToUI(cmp);
                } else if (results === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showModalLoadingSpinner", false);
                    console.log('ERROR calculatetotals ' + errorMessage);
                }
            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
                console.log('ERROR calculatetotals ' + errorMessage);
            }

        });
        $A.enqueueAction(action);
    },

    resetOVCMessage: function (cmp) {
        cmp.set("v.responseStatus", "");
        cmp.set("v.responseMessage", "");
        cmp.set("v.type", "");
    },

    validateCEVLANIdCount: function (cmp) {
        var ctrls = cmp.find("ovcField");
        var ctrl = ctrls.filter(function (item) {
            return item.get("v.name") == "CEVLAINID";
        })[0];

        var validity = ctrl.get('v.validity');
        var ceVlanValue = ctrl.get("v.value");
        var vlanArray = ceVlanValue.split(',');
        var vlanIdValid = true;
        if (vlanArray.length > 32) {
            vlanIdValid = false;
        }
        // var otCmp = cmp.find("CEVLAINID");
        if (vlanIdValid === false) {
            ctrl.setCustomValidity($A.get("$Label.c.DF_Too_Many_VLAN_Ids"));
            //ctrl.set('v.messageWhenBadInput', $A.get("$Label.c.DF_Too_Many_VLAN_Ids"));
            ctrl.set('v.validity', {
                valid: false,
                badInput: true
            });

            //  cmp.set("v.isCEVLANValueInvalid", true );
            //ctrl.focus();
        } else {
            if (!cmp.get("v.duplicatedVLANId") && cmp.get("v.badVLANId")) {
                ctrl.setCustomValidity("");
                ctrl.set('v.validity', {
                    valid: true,
                    valueMissing: false
                });
            }
        }
    },

    validateVlanIdRage: function (vlanRanges, cmp) {
        var maxVlanId = 4094;
        var validVlandFullRange = new Map();
        var AllVLANRanges = new Map();
        var vlanIdValid = true;
        var vlanArray = vlanRanges.split(',');
        var duplicateVlandIdFound = false;
        var dupliateVLanIds = [];
        var currentOVCId = cmp.get("v.selectedOVC").OVCId;
        var tempVLrangeMap = new Map();

        if (cmp.get("v.badVLANId")) { //skip if input already bad
            return vlanIdValid;
        }
        AllVLANRanges = cmp.get('v.uniVLANIds');

        vlanArray.map(function (item) {
            if (item.indexOf('-') > 0) {
                var subArray = item.split('-');
                var startVlanId = subArray[0].trim() * 1;
                var endVlanId = subArray[1].trim() * 1;

                if (startVlanId > maxVlanId || endVlanId > maxVlanId) {
                    vlanIdValid = false;
                }

                if (startVlanId > endVlanId || startVlanId == endVlanId) {
                    vlanIdValid = false;
                }

                for (var i = startVlanId; i <= endVlanId; i++) {
                    if (validVlandFullRange.has(i * 1)) {
                        vlanIdValid = false;
                    } else {
                        validVlandFullRange.set(i * 1);
                    }
                }
                //check for duplicates in other ovc vlan id's
                for (var i = startVlanId; i <= endVlanId; i++) {
                    if (tempVLrangeMap !== null && (tempVLrangeMap.has(i * 1)) || (AllVLANRanges !== null && AllVLANRanges.has(i * 1) && currentOVCId != AllVLANRanges.get(i * 1))) {
                        console.log("!!! duplicate VLAN Id: " + i);
                        duplicateVlandIdFound = true;
                        dupliateVLanIds.push(i * 1);
                        vlanIdValid = false;
                    } else {
                        tempVLrangeMap.set(i * 1);
                    }
                }
            } else {
                if (item.trim() > maxVlanId) {
                    vlanIdValid = false;
                }

                if (validVlandFullRange.has(item.trim() * 1)) {
                    duplicateVlandIdFound = true;
                    vlanIdValid = false;
                } else {
                    validVlandFullRange.set(item.trim() * 1);
                    duplicateVlandIdFound = false;
                }
                //check for duplicate single item
                if (AllVLANRanges !== null && AllVLANRanges.has(item.trim() * 1) && currentOVCId != AllVLANRanges.get(item.trim() * 1)) {
                    duplicateVlandIdFound = true;
                    dupliateVLanIds.push(item.trim() * 1);
                    vlanIdValid = false;
                }
            }
        });

        var ctrls = cmp.find("ovcField");
        var ctrl = ctrls.filter(function (item) {
            return item.get("v.name") == "CEVLAINID";
        })[0];

        if (duplicateVlandIdFound) {
            var errorMessage = $A.get("$Label.c.DF_Duplicate_UNI_VLAN_ID_found");
            ctrl.setCustomValidity(errorMessage);
            ctrl.set('v.validity', {
                valid: false,
                badInput: true
            });
            ctrl.showHelpMessageIfInvalid();
            cmp.set("v.duplicatedVLANId", true);
            cmp.set("v.uniVLANValid", false);
        } else {
            cmp.set("v.duplicatedVLANId", false);
            cmp.set("v.uniVLANValid", true);
            ctrl.setCustomValidity("");
            ctrl.set('v.validity', {
                valid: true
            });
        }


        return vlanIdValid;
    },
    loadAllUniVlanRanges: function (cmp) {
        var AllVLANRanges = new Map();
        var OVCList = cmp.get("v.OVCList");

        for (var i = 0; i < OVCList.length; i++) {
            var vlanRanges = OVCList[i].ceVlanId;
            var OVCId = OVCList[i].OVCId;
            if (vlanRanges !== undefined) {
                var vlanArray = vlanRanges.split(',');
                vlanArray.map(function (item) {
                    if (item.indexOf('-') > 0) {
                        var subArray = item.split('-');
                        var startVlanId = subArray[0].trim() * 1;
                        var endVlanId = subArray[1].trim() * 1;

                        for (var j = startVlanId; j <= endVlanId; j++) {
                            if (AllVLANRanges.has(j * 1)) {} else {
                                AllVLANRanges.set(j * 1, OVCId);
                            }
                        }
                    } else {
                        if (AllVLANRanges.has(item.trim() * 1)) {

                        } else {
                            AllVLANRanges.set(item.trim() * 1, OVCId);
                        }
                    }
                });
            }
        }
        cmp.set('v.uniVLANIds', AllVLANRanges);
        //console.log("!!! AllVLANRanges: " + JSON.stringify(AllVLANRanges));
    },
    countCEVLANIDRanges: function (cmp) {

        var OVCList = cmp.get("v.OVCList");
        var OVCVLANRanges = [];
        var rangeCount = 0;
        var currentOVC = cmp.get("v.selectedOVC");
        for (var i = 0; i < OVCList.length; i++) {
            if (OVCList[i].OVCId != cmp.get("v.selectedOVC.OVCId") && typeof OVCList[i].ceVlanId !== 'undefined' && OVCList[i].OVCId !== 'undefined') {
                OVCVLANRanges.push(OVCList[i].ceVlanId);
            }
        }

        if (cmp.get("v.selectedOVC.ceVlanId") !== undefined) {
            OVCVLANRanges.push(cmp.get("v.selectedOVC.ceVlanId"));
        }

        for (var i = 0; i < OVCVLANRanges.length; i++) {
            rangeCount = rangeCount + (OVCVLANRanges[i].match(/-/g) || []).length;

        }
        return rangeCount;

    },

    checkCEVLAINIDHasBadPattern: function (cmp) {
        // if bad ,- pattern or too many dashes
        var ctrls = cmp.find("ovcField");
        var ctrl = ctrls.filter(function (item) {
            return item.get("v.name") == "CEVLAINID";
        })[0];

        var validity = ctrl.get('v.validity');
        var ceVlanValue = ctrl.get("v.value");

        if (ceVlanValue !== undefined) {

            var commaCount = (ceVlanValue.split(",").length - 1);
            var dashCount = (ceVlanValue.split("-").length - 1);
            var badPattern = ceVlanValue.indexOf(",,") + ceVlanValue.indexOf("--") + ceVlanValue.indexOf("-,") + ceVlanValue.indexOf(",-") > -4;
            //check for -- ,, -, etc
            if (cmp.get("v.uniVLANValid") &&
                (badPattern || dashCount > commaCount + 1)) {
                cmp.set("v.badVLANId", true);
                cmp.set("v.uniVLANValid", false);
                return true;
            } else {
                cmp.set("v.badVLANId", false);
                cmp.set("v.uniVLANValid", true);
            }
        }
        return false;

    },
    validateCEVLAINID: function (cmp) {

        var inValid = false;
        var ctrls = cmp.find("ovcField");

        //debugger;

        var ctrl = ctrls.filter(function (item) {
            return item.get("v.name") == "CEVLAINID";
        })[0];

        var validity = ctrl.get('v.validity');
        var ceVlanValue = ctrl.get("v.value");
        cmp.set("v.duplicatedVLANId", false);
        var vlanDuplicated; // = cmp.get("v.duplicatedVLANId");
        if ((!validity.valueMissing)) {
            if (!this.validateVlanIdRage(ceVlanValue, cmp) || this.checkCEVLAINIDHasBadPattern(cmp)) {
                vlanDuplicated = cmp.get("v.duplicatedVLANId");
                inValid = true;
                ctrl.setCustomValidity("Your entry does not match the allowed pattern.");
                ctrl.set('v.validity', {
                    valid: false,
                    badInput: true
                });
                ctrl.showHelpMessageIfInvalid();
                cmp.set("v.uniVLANValid", false);
            } else {
                vlanDuplicated = cmp.get("v.duplicatedVLANId");
                if (!vlanDuplicated) {
                    cmp.set("v.uniVLANValid", true);
                    ctrl.setCustomValidity("");
                    ctrl.set('v.validity', {
                        valid: true,
                        badInput: false
                    });
                }
            }

        }
    },

    validateRouteType: function (cmp, event) {

        var routeTypeCtrl = cmp.find("routeType");
        var tempCtrl = cmp.find("btnModalSaveOVC");

        this.validateSelectCmpEmpty(routeTypeCtrl);

        var valid = routeTypeCtrl.get("v.validity").valid;

        if (!valid) {
            cmp.set("v.ovcSaveValidationError", true);
            this.reFocus(routeTypeCtrl, tempCtrl);
        }

        event.getSource().focus();

        return valid;
    },

    validateRequiredSelect: function (cmp, event) {
        var ctrl = event.getSource();
        var tempCtrl = cmp.find("btnNext");

        this.validateSelectCmpEmpty(ctrl);

        var valid = ctrl.get("v.validity").valid;

        if (!valid) {
            this.reFocus(ctrl, tempCtrl);
        }

        event.getSource().focus();

        return valid;
    },

    ovcEnableMappingMode: function (cmp) {
        var selectedOVC = cmp.get("v.selectedOVC");
        var COSH = selectedOVC.coSHighBandwidth == "" || selectedOVC.coSHighBandwidth == 0 ? 0 : 1;
        var COSM = selectedOVC.coSMediumBandwidth == "" || selectedOVC.coSMediumBandwidth == 0 ? 0 : 1;
        var COSL = selectedOVC.coSLowBandwidth == "" || selectedOVC.coSLowBandwidth == 0 ? 0 : 1;
        var numberOfCosFieldsSet = COSH + COSM + COSL;
        var el = cmp.find("MappingMode");


        if (numberOfCosFieldsSet == 0) {
            selectedOVC.mappingMode = "";
            cmp.set("v.selectedOVC", selectedOVC);
        } else
        if (numberOfCosFieldsSet == 1) {
            selectedOVC.mappingMode = "PCP";
            cmp.set("v.selectedOVC", selectedOVC);

            $A.util.removeClass(el, "slds-has-error"); // remove red border
            $A.util.addClass(el, "hide-error-message");
            el.set("v.disabled", true);

        } else {
            el.set("v.disabled", false);
            //    selectedOVC.mappingMode = "";//Raja: This was nullifying the value populated thru Bulk Upload
            cmp.set("v.selectedOVC", selectedOVC);
            //el.focus();
        }
    },
    isOVCMappingModeValid: function (cmp) {
        var selectedOVC = cmp.get("v.selectedOVC");
        var COSH = selectedOVC.coSHighBandwidth == "" || selectedOVC.coSHighBandwidth == 0 ? 0 : 1;
        var COSM = selectedOVC.coSMediumBandwidth == "" || selectedOVC.coSMediumBandwidth == 0 ? 0 : 1;
        var COSL = selectedOVC.coSLowBandwidth == "" || selectedOVC.coSLowBandwidth == 0 ? 0 : 1;
        var numberOfCosFieldsSet = COSH + COSM + COSL;
        var isValid = true;
        var el = cmp.find("MappingMode");
        if (numberOfCosFieldsSet > 1 && (selectedOVC.mappingMode == "" || selectedOVC.mappingMode == "Select")) {
            isValid = false;
        }
        return isValid;
    },
    isAllFieldsValid: function (cmp, fields) {
        var allValid = cmp.find(fields).reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);

        return allValid;
    },
    nullToEmptyString: function (value) {
        return (value == null || value === "null" || typeof value == 'undefined') ? "" : value
    },
    saveNext: function (cmp) {
        var location = cmp.get("v.location");
        var OVCList = cmp.get('v.NonBillableOVCList');
        var selectedUNI = cmp.get("v.selectedUNI");

        location.OVCs = OVCList;
        location.UNI = [selectedUNI];
        location.status = this.ORDER_STATUS;
        cmp.set('v.location', location);
        console.log('My comments location map');
        console.log(location);
        this.saveToCSNext(cmp);

    },
    saveToCSNext: function (cmp) {
        var configList = cmp.get("v.configs");
        var OVCList = cmp.get("v.OVCList");
        var selectedUNI = cmp.get("v.selectedUNI");
        var selectedOVC = cmp.get("v.selectedOVC");

        var prodConfig = configList.filter(this.getAttByName('Direct Fibre - Product Charges'))[0];

        prodConfig.atts.filter(this.getAttByName('Interface Bandwidth'))[0].cscfga__Value__c = selectedUNI.interfaceBandwidth;
        prodConfig.atts.filter(this.getAttByName('BTD Type'))[0].cscfga__Value__c = selectedUNI.BTDType;
        prodConfig.atts.filter(this.getAttByName('After Hours Site Visit'))[0].cscfga__Value__c = selectedUNI.AHA;
        prodConfig.atts.filter(this.getAttByName('Zone'))[0].cscfga__Value__c = selectedUNI.zone;
        prodConfig.atts.filter(this.getAttByName('eSLA'))[0].cscfga__Value__c = selectedUNI.SLA;
        prodConfig.atts.filter(this.getAttByName('Term'))[0].cscfga__Value__c = selectedUNI.term;

        var configItem = configList.filter(function (item) {
            return item.Id == selectedOVC.OVCId;
        })[0];

        if (configItem) {

            configItem.atts.filter(this.getAttByName('CoS High'))[0].cscfga__Value__c = selectedOVC.coSHighBandwidth; //=="0"?"":selectedOVC.coSHighBandwidth;
            configItem.atts.filter(this.getAttByName('CoS Medium'))[0].cscfga__Value__c = selectedOVC.coSMediumBandwidth; //=="0"?"":selectedOVC.coSMediumBandwidth;
            configItem.atts.filter(this.getAttByName('CoS Low'))[0].cscfga__Value__c = selectedOVC.coSLowBandwidth; //=="0"?"":selectedOVC.coSLowBandwidth;

            configItem.atts.filter(this.getAttByName('POI'))[0].cscfga__Value__c = selectedOVC.POI;
            configItem.atts.filter(this.getAttByName('Route Type'))[0].cscfga__Value__c = selectedOVC.routeType;
        }

        var result = JSON.parse(JSON.stringify(cmp.get("v.configs")));
        var configs = {};
        var attributes = {}
        var parentId = "";
        for (var i = 0; i < result.length; i++) {
            var key = result[i].Id;
            attributes[key + '-attributes'] = result[i].atts;
            delete result[i]['atts'];
            configs[key] = result[i];
        }

        var location = cmp.get('v.location');
        var action = cmp.get("c.updateProduct");
        var errorMessage = "";

        action.setParams({
            basketId: location.basketId,
            configs: JSON.stringify(configs),
            attributes: JSON.stringify(attributes)
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            var goFlag = cmp.get("v.goNext");
            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results != "Error") {
                    this.loadCSConfigNext(cmp);
                    cmp.set("v.responseStatusUNIPage", "");
                    cmp.set("v.responseMessageUNIPage", "");
                    cmp.set("v.uniBannertype", "");

                    cmp.set("v.responseStatus", "OK");
                    cmp.set("v.responseMessage", $A.get("$Label.c.DF_Order_OVC_Save_Success"));
                    cmp.set("v.type", "Banner");
                } else if (results === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showModalLoadingSpinner", false);
                }
            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                console.log('saveToCS Error ' + errorMessage);
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
            }

        });
        $A.enqueueAction(action);
    },
    loadCSConfigNext: function (cmp) {
        console.log('==========DF_SF_OrderItemHelper========= loadCSConfig')
        var action = cmp.get("c.getExistingQuickQuoteData");
        var responseVal;

        action.setParams({
            quoteId: cmp.get("v.location").id
        });

        action.setCallback(this, function (response) {

            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('==========DF_SF_OrderItemHelper========= loadCSConfig - State ' + state);
                var results = response.getReturnValue();
                if (results != "Error") {
                    responseVal = JSON.parse(response.getReturnValue());
                    console.log('ZZZZ get quick quote data');
                    console.log(responseVal);

                    var extractedProductConfig = responseVal.ProductConfigMap;

                    cmp.set("v.structure", extractedProductConfig);
                    //console.log('==========DF_SF_OrderItemHelper========= loadCSConfig extractedProductConfig'+ JSON.stringify(extractedProductConfig));	
                    var attributes = {};
                    var configs = {};
                    for (var key in extractedProductConfig) {
                        if (extractedProductConfig.hasOwnProperty(key)) {
                            if (key.indexOf('-attributes') > -1) {
                                attributes[key.substr(0, key.indexOf('-attributes'))] = extractedProductConfig[key];
                            } else {
                                configs[key] = extractedProductConfig[key];
                            }
                        }
                    }

                    for (var key in attributes) {
                        if (attributes.hasOwnProperty(key)) {
                            configs[key].atts = attributes[key];
                        }
                    }

                    var configList = [];
                    for (var key in configs) {
                        if (configs.hasOwnProperty(key)) {
                            configList.push(configs[key]);
                        }
                    }

                    cmp.set("v.configs", configList);

                    this.loadCSToUINext(cmp, responseVal);

                    cmp.set("v.responseStatusUNIPage", "");
                    cmp.set("v.responseMessageUNIPage", "");
                    cmp.set("v.uniBannertype", "");
                } else if (results === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showModalLoadingSpinner", false);
                    console.log('ERROR loadCSConfig ' + errorMessage);
                }

            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
                console.log('ERROR loadCSConfig ' + errorMessage);
            }
        });
        $A.enqueueAction(action);
    },
    loadCSToUINext: function (cmp, responseVal) {
        console.debug('=====loadCSToUI ===== START ');
        var configList = cmp.get("v.configs");
        var location = cmp.get("v.location");
        var OVCList = [];
        var selectedUNI = cmp.get("v.selectedUNI");
        var selectedOVC = cmp.get("v.selectedOVC");

        location.address = responseVal.Address;
        location.locId = responseVal.LocId;
        location.basketId = responseVal.BasketId;
        location.quoteId = responseVal.QuoteId;
        location.FBC = responseVal.Fbc;
        location.SCNR = location.SCNR !== responseVal.Scnr ? responseVal.Scnr : location.SCNR;
        location.SCR = location.SCR !== responseVal.Scr ? responseVal.Scr : location.SCR;

        var prodChargeName = 'Direct Fibre - Product Charges';

        var tempOVCList = configList.filter(function (item) {
            return item.Name != prodChargeName;
        });

        var tempProdChargeList = configList.filter(function (item) {
            return item.Name == prodChargeName;
        });

        var sortedTempOVCList = tempOVCList.sort(this.sortOVC);
        // console.debug('sortedTempOVCList '+ sortedTempOVCList);



        if (tempProdChargeList.length > 0) {
            var configAtts = tempProdChargeList[0].atts;
            selectedUNI.Id = tempProdChargeList[0].Id;
            selectedUNI.interfaceBandwidth = configAtts.filter(this.getAttByName('Interface Bandwidth'))[0].cscfga__Value__c;
            selectedUNI.BTDType = configAtts.filter(this.getAttByName('BTD Type'))[0].cscfga__Value__c;
            selectedUNI.AHA = configAtts.filter(this.getAttByName('After Hours Site Visit'))[0].cscfga__Value__c;
            selectedUNI.zone = configAtts.filter(this.getAttByName('Zone'))[0].cscfga__Value__c;
            selectedUNI.SLA = configAtts.filter(this.getAttByName('eSLA'))[0].cscfga__Value__c;
            selectedUNI.term = configAtts.filter(this.getAttByName('Term'))[0].cscfga__Value__c;

            location.SCNR = configAtts.filter(this.getAttByName('Once off charge'))[0].cscfga__Value__c;
            location.SCR = configAtts.filter(this.getAttByName('Recurring charge'))[0].cscfga__Value__c;
        }

        for (var i = 0; i < sortedTempOVCList.length; i++) {

            var configAtts = sortedTempOVCList[i].atts;

            if (selectedOVC.OVCId == sortedTempOVCList[i].Id) {
                selectedOVC.coSHighBandwidth = configAtts.filter(this.getAttByName('CoS High'))[0].cscfga__Value__c;
                selectedOVC.coSMediumBandwidth = configAtts.filter(this.getAttByName('CoS Medium'))[0].cscfga__Value__c;
                selectedOVC.coSLowBandwidth = configAtts.filter(this.getAttByName('CoS Low'))[0].cscfga__Value__c;
                selectedOVC.routeRecurring = configAtts.filter(this.getAttByName('Total_Routing_Charge_afterDiscount'))[0].cscfga__Value__c;
                selectedOVC.coSRecurring = configAtts.filter(this.getAttByName('Hidden_TotalCoS_Charges'))[0].cscfga__Value__c;
                selectedOVC.routeType = configAtts.filter(this.getAttByName('Route Type'))[0].cscfga__Value__c;
                selectedOVC.POI = configAtts.filter(this.getAttByName('POI'))[0].cscfga__Value__c;
                selectedOVC.CSA = configAtts.filter(this.getAttByName('CSA'))[0].cscfga__Value__c;
                selectedOVC.OVCName = sortedTempOVCList[i].Name;
                console.debug('=====loadCSToUI ===== selectedOVC ' + selectedOVC);
                OVCList.push(selectedOVC);
            } else {
                var newOVC = {
                    'OVCId': sortedTempOVCList[i].Id,
                    'OVCName': sortedTempOVCList[i].Name,
                    'CSA': configAtts.filter(this.getAttByName('CSA'))[0].cscfga__Value__c,
                    'monthlyCombinedCharges': configAtts.filter(this.getAttByName('OVC Recurring Charge'))[0].cscfga__Value__c,
                    'routeType': configAtts.filter(this.getAttByName('Route Type'))[0].cscfga__Value__c,
                    'coSHighBandwidth': configAtts.filter(this.getAttByName('CoS High'))[0].cscfga__Value__c,
                    'coSMediumBandwidth': configAtts.filter(this.getAttByName('CoS Medium'))[0].cscfga__Value__c,
                    'coSLowBandwidth': configAtts.filter(this.getAttByName('CoS Low'))[0].cscfga__Value__c,
                    'routeRecurring': configAtts.filter(this.getAttByName('Total_Routing_Charge_afterDiscount'))[0].cscfga__Value__c,
                    'coSRecurring': configAtts.filter(this.getAttByName('Hidden_TotalCoS_Charges'))[0].cscfga__Value__c,
                    'POI': configAtts.filter(this.getAttByName('POI'))[0].cscfga__Value__c,
                    'CSA': configAtts.filter(this.getAttByName('CSA'))[0].cscfga__Value__c,
                    'status': 'Incomplete'
                };

                OVCList.push(newOVC);
            }
        }


        location.OVCs = OVCList;
        location.UNI = [selectedUNI];
        location.prodChargeConfigId = selectedUNI.Id;
        location.ovcConfigId = OVCList[0].OVCId;
        //console.debug('=====loadCSToUI ===== OVCList ' +JSON.stringify(OVCList));
        var prodchargeConfigurationId = selectedUNI.Id;
        var csa = OVCList[0].CSA;

        cmp.set("v.OVCList", OVCList);
        cmp.set('v.location', location);
        cmp.set("v.selectedUNI", selectedUNI);
        cmp.set("v.selectedOVC", selectedOVC);
        cmp.set("v.prodchargeConfigurationId", prodchargeConfigurationId);
        cmp.set("v.csa", csa);
        console.log('Final spinner off');
        this.calculatetotalsNext(cmp);
        this.loadOVCNonBillableToUI(cmp);
        this.ovcCalculateBandWidth(cmp, "");
    },
    calculatetotalsNext: function (cmp) {

        var location = cmp.get('v.location');
        var basketIdVal = location.basketId;
        var prodChargeID = cmp.get('v.prodchargeConfigurationId');
        var errorMessage = "";
        var OVCList = cmp.get("v.OVCList");
        var configList = cmp.get("v.configs");
        var Unizoneprice = 0;
        var DBPZoneDiscount = 0;
        var ZoneTermDiscount = 0;
        var tempProdChargeList = configList.filter(function (item) {
            return item.Name == 'Direct Fibre - Product Charges';
        });
        var configAtts = tempProdChargeList[0].atts;
        configAtts.forEach(function (element) {
            if (element.Name == 'Hidden_Zone_Charges' && element.cscfga__Value__c != null && element.cscfga__Is_Line_Item__c) {
                Unizoneprice = parseFloat(element.cscfga__Value__c);
            }
            if (element.Name == 'Hidden_DBP_Zone_Discount' && element.cscfga__Value__c != null && element.cscfga__Is_Line_Item__c) {
                DBPZoneDiscount = parseFloat(element.cscfga__Value__c);
            }
            if (element.Name == 'Zone_Term_Discount_Recurring' && element.cscfga__Value__c != null && element.cscfga__Is_Line_Item__c) {
                ZoneTermDiscount = parseFloat(element.cscfga__Value__c);
            }
        });
        var ovcCoSCharge = OVCList.reduce(function (sum, item) {
            return (sum * 1) + (Math.round(parseFloat((item.coSRecurring * Math.pow(10, 2)).toFixed(2))) / Math.pow(10, 2) * 1);
        }, 0);
        ovcCoSCharge = ovcCoSCharge + Unizoneprice - DBPZoneDiscount - ZoneTermDiscount;
        var ovcSCR = OVCList.reduce(function (sum, item) {
            return (sum * 1) + (Math.round(parseFloat((item.routeRecurring * Math.pow(10, 2)).toFixed(2))) / Math.pow(10, 2) * 1) + (Math.round(parseFloat((item.coSRecurring * Math.pow(10, 2)).toFixed(2))) / Math.pow(10, 2) * 1);
        }, 0);
        //Added for DBP
        var OVCTotalBws = OVCList.reduce(function (sum, item) {
            return (sum * 1) + (Math.round(parseFloat(((parseFloat(item.coSHighBandwidth) + parseFloat(item.coSMediumBandwidth) + parseFloat(item.coSLowBandwidth)) * Math.pow(10, 2)).toFixed(2))) / Math.pow(10, 2) * 1);
        }, 0);
        if (isNaN(ovcSCR) && ovcSCR != null) {
            ovcSCR = null;
        }
        console.log('Hidden ovc charges: ');
        console.log(ovcSCR);

        var action = cmp.get("c.updateHiddenOVCCharges");
        var errorMessage = "";

        action.setParams({
            basketId: location.basketId,
            configId: prodChargeID,
            totalOVC: ovcSCR,
            totalCoSBw: OVCTotalBws,
            totalOVCCosCharges: ovcCoSCharge
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results != "Error") {
                    location.SCR = results;
                    cmp.set('v.location', location);
                    this.svrSave(cmp);
                } else if (results === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showModalLoadingSpinner", false);
                    console.log('ERROR calculatetotals ' + errorMessage);
                }
            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
                console.log('ERROR calculatetotals ' + errorMessage);
            }

        });
        $A.enqueueAction(action);
    }


})