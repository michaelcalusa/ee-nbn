({	
    init: function (cmp, event, helper) {

		// Get custom label values
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);
		helper.getBulkToggleSettings(cmp);
        /*
        if($A.get("$Label.c.DF_Disable_Bulk_Order_Upload") == 'Y'){
            cmp.set("v.disableBulk", true);
            cmp.set("v.showErrorMessage", true);
            cmp.set("v.errorMessage",$A.get("$Label.c.DF_Bulk_Order_Err_Msg"));
        }
		*/

        if ( cmp.get('v.searchEnabledFeatureToggle') ) {
            if (!$A.util.isEmpty(cmp.find('orderSearch').get('v.value'))) {
                helper.searchOrder(cmp, event,helper);
                return;
            }
        }

        var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
        var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';                  		
        helper.apex(cmp, "v.fullList", 'c.getOrderSummaryData', {  opptyBundleId: cmp.get("v.parentOppId") }, true)
		.then(function(result) {
			helper.checkOrderPermissions(cmp);
            helper.processOrderSummary(cmp);
		})
		.catch(function(error) {
			$A.reportError("error message here", error);
		});		
    },
    
    exportCSV: function(cmp, event, helper){
        helper.fetchOrderdetails(cmp);
        //var csvData = helper.convertArrayObjtoCSV(cmp, quoteData);
        //var csvData = helper.convertArrayOfObjectsToCSV(cmp, quoteData);
        
    },
    
    uploadCSV: function(component, event, helper) {
        //validate csv file
        var cmpTarget = component.find('inputContainer');
		var NO_ROWS = $A.get("$Label.c.EE_Bulk_Order_No_Row");
		//cmp.set("v.startProcess", true);
		if(helper.validateUploadFile(component)) {
		    var fileUpload = document.getElementById("fileId");
		    var fr = new FileReader();
		    
		    //read file contents and rows
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
                $A.util.removeClass(cmpTarget, 'slds-has-error');
		        component.set("v.showErrorMessage", false); 
                reader.onload = function (e) {
                    var table = document.createElement("table");
                    var rows = e.target.result.split("\n");
					var dfQIdNo;
                    var dfQIdList = [];
                    for (var i = 0; i < rows.length; i++) {
                        var cells = rows[i].split(",");
                        if (cells.length > 1) {
                            var row = table.insertRow(-1);
							var dfQ = 0;
                            dfQ = cells.length - 1;
                            dfQIdNo = dfQ;
                            for (var j = 0; j < cells.length; j++) {
                                var cell = row.insertCell(-1);
                                cell.innerHTML = cells[j];
								if(j == dfQIdNo){
                                    cells[j] = cells[j].replace(/(\r\n|\n|\r)/gm, '');
									dfQIdList.push(cells[j]);
								}
                            }
                        }
                    }
					console.log('Final dfQIdList');
                    console.log(dfQIdList);
                    var rows = table.getElementsByTagName("tr").length;
                    var totalRow = rows-1;                    
                    //check csv if contains atleast 1 record
                    if(totalRow != -1 && totalRow >= 1) {
                        window.setTimeout(
                            $A.getCallback(function() {
                                helper.processQuoteStatus(component,event,dfQIdList);
                                var a = component.get('c.init');
                                $A.enqueueAction(a);
                            }), 500
                        );
                    } else {
                        //assign error here
                        helper.fileUploadErrorMsg(component, cmpTarget, NO_ROWS);
                    }
                }
    
                reader.readAsText(fileUpload.files[0]);
            }
            window.setTimeout(
                $A.getCallback(function() {
                    //helper.uploadCSV(component,event);
                    var a = component.get('c.init');
                    $A.enqueueAction(a);
                }), 1000
            );
		}
		
    },
    
    handleSelectAllLoc : function(cmp, event, helper) {
        var isAllCbSelected = cmp.get('v.isAllCbSelected');
        var checkboxes = cmp.find('selLoc');
        var chk = (checkboxes.length == null) ? [checkboxes] : checkboxes;
        chk.forEach(function(checkbox) {
            if(isAllCbSelected) {
                checkbox.set('v.checked', true);  
                cmp.set('v.isAllCbSelected', true);
            } else {
                checkbox.set('v.checked', false);    
                cmp.set('v.isAllCbSelected', false);
            }
        });
    },
    
    handleSelect : function(cmp, event, helper) {
        var checkboxes = cmp.find('selLoc');
        var chk = (checkboxes.length == null) ? [checkboxes] : checkboxes;
        var quoteData = cmp.get('v.currentList');
        var cntCheck = 0;
        chk.forEach(function(checkbox) {
            if(checkbox.get('v.checked')) {
                cntCheck++;
            }
        });
        
        if(cntCheck == quoteData.length) {
            cmp.set('v.isAllCbSelected', true);
        } else {
            cmp.set('v.isAllCbSelected', false);
        }
    },
	
	viewOrder: function(cmp, event, helper) {
		var orderPageEvt = cmp.getEvent("orderPageEvent");                    		
		var selectedOrder = { Id: event.target.getAttribute("data-orderId") };				
        orderPageEvt.setParam("selectedOrder", selectedOrder);
		orderPageEvt.setParam("isFromQuotePage", true);
		orderPageEvt.setParam("batchId", cmp.get("v.parentOppId"));
		
        orderPageEvt.fire();
	},

	clear: function(cmp, event, helper) {			   
		cmp.set('v.currentList', cmp.get('v.fullList'));
		cmp.set('v.searchTerm', '');
	},

	hideQuickQuoteDetails: function(cmp, event, helper) {		
		cmp.set("v.isQuickQuoteDetailsOpen", false);
	},

	saveQuickQuoteDetails: function(cmp, event, helper) {

		var fullList = cmp.get('v.fullList');
		var selectedLocation = cmp.get("v.selectedLocation");
		var updatedList = fullList.map(function(item) { 
				if(item.id == selectedLocation.id) return selectedLocation; 
				else return item; 
			});

		cmp.set('v.fullList', updatedList);		
	},
		
	clickQuoteLink: function(cmp, event, helper) {		
		var id = event.target.getAttribute("data-id");				
		helper.showQuickQuoteDetails(cmp, event, id);		
    },
    
    // function for save the Records 
    save: function(cmp, event, helper) {        
        
            var action = cmp.get("c.save");
            action.setParams({
                "quoteList": cmp.get("v.currentList"),
                "oppty": cmp.get("v.oppId")
            });
            // set call back 
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {                    
                    var sitesDetails = JSON.parse(response.getReturnValue());
                	cmp.set("v.currentList", sitesDetails);
                }
                else {
					var errors = response.getError();

					if (errors) {
						if (errors[0] && errors[0].message) {
                    		console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
						}
					} else {
                		console.log(ERR_MSG_UNKNOWN_ERROR);                 
					}
                	console.log('Problem getting quote, response state: ' + state); 
            	}
            });
            // enqueue the server side action  
            $A.enqueueAction(action);
    },
    
    gotoRAGPage: function (cmp, event, helper) {
        if ($A.util.isEmpty(cmp.get('v.parentOppId'))) {
            return false;
        }
		var ragDtEvt = cmp.getEvent("siteDetailPageEvent");
        ragDtEvt.fire(); 
	},

    gotoHomePage: function (cmp, event, helper) {
        var ragDtEvt = cmp.getEvent("homePageEvent");
        ragDtEvt.fire();
    },

    clickOrderLink: function (cmp, event, helper) {
        console.log('==== clickOrderLink========');
		var id = event.target.getAttribute("data-id");
        console.log('id ' +id) //added
        helper.createDF_Order(cmp, event, id);		
        console.log('==== END  clickOrderLink========');
	},   
	
	renderPage: function(cmp, event, helper) {    
        helper.renderPage(cmp);
    },

    search: function(cmp, event, helper) {
        helper.searchOrder(cmp, event, helper);
    },

    searchTermChange  : function(cmp, event, helper) {
        //Do search if enter key is pressed.
        if(event.getParams().keyCode == 13) {
            cmp.set('v.isTriggerdBySearch', true);
            helper.searchOrder(cmp, event, helper);
        }

    },
    
    doneRendering: function(cmp, event, helper) {
        helper.disableUploadIfNeeded(cmp);
    },

})