({  
    fetchOrderdetails: function (cmp){
        var csvStringResult, counter, keys, columnDiv, lineDiv;
		var bundleName = cmp.get("v.parentOppName");        
        columnDiv = ',';
        lineDiv = '\n';

        var actColHeader = cmp.get("c.bulkCol");
        actColHeader.setParams({opptyBundleId:cmp.get('v.oppId')});
        actColHeader.setCallback(this, function(response){
            var state = response.getState();
            console.log('Fetch the Columns');
            if(state === 'SUCCESS'){
                keys = response.getReturnValue();
                console.log(keys);
                csvStringResult = '';
                csvStringResult += keys.join(columnDiv);
                csvStringResult += lineDiv;

                var quoteData = cmp.get('v.fullList');
                var statusQuote = ['status'];
                for(var i=0; i<quoteData.length; i++){
                    counter = 0;
                    var csvString;
                    var kStat = statusQuote.indexOf("status");
                    var statusVal = quoteData[i][statusQuote[kStat]];
                    if(statusVal!='Submitted') {
                        for(var sTempkey in keys){
                            var sKey = keys[sTempkey];
                            if(counter>0){
                                csvStringResult += columnDiv;
                            }
                            csvString = this.prepopulateRequiredCSV(cmp, quoteData, i, sKey);
                            console.log('csvString: ' + csvString);
                            csvStringResult += csvString;
                            counter++;
                        }
                        csvStringResult += lineDiv;
                    }
                }
                console.log('csvStringResult:: ' + csvStringResult);
                console.log('### Inside exportCSV ###');
                var hiddenElt = document.createElement('a');
                hiddenElt.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvStringResult);
                hiddenElt.target = '_self';
                hiddenElt.download = 'BulkOrderUpload_'+bundleName+'.csv';
                document.body.appendChild(hiddenElt);
                hiddenElt.click();

            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(actColHeader);
    },
    
    //CPST-7502
    getBulkToggleSettings : function (cmp) {
        var action = cmp.get("c.getCustomSetting"); 
        action.setParams({
            settingName: 'TOGGLE_OFF_BULKORDERUPLOAD'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(cmp.isValid() && state == "SUCCESS") {
                cmp.set("v.toggleOffBulk", JSON.parse(response.getReturnValue()));
                cmp.set("v.disableBulk", JSON.parse(response.getReturnValue()));
                cmp.set("v.showErrorMessage", JSON.parse(response.getReturnValue()));
                cmp.set("v.errorMessage",$A.get("$Label.c.DF_Bulk_Order_Err_Msg"));
            } else {
                console.log("Error: " + state);
            }        
        });
        $A.enqueueAction(action);
    },

    prepopulateRequiredCSV: function(cmp, quoteData, i, sKey) {
        var csvVal = '';
        var keysQuote = ['locId','quoteId','address','id','orderId'];
        if(sKey == 'LocationId') {
            var k = keysQuote.indexOf("locId");
            csvVal = quoteData[i][keysQuote[k]];
        } else if(sKey == 'QuoteId') {
            var k = keysQuote.indexOf("quoteId");
            csvVal = quoteData[i][keysQuote[k]];
        } else if(sKey == 'Address') {
            var k = keysQuote.indexOf("address");
            csvVal = quoteData[i][keysQuote[k]];
        } else if(sKey == 'SalesforceDFQId') {
            var k = keysQuote.indexOf("id");
            csvVal = quoteData[i][keysQuote[k]];
        } else if(sKey == 'SalesforceDFOId') {
            var k = keysQuote.indexOf("orderId");
            csvVal = quoteData[i][keysQuote[k]] != null ? quoteData[i][keysQuote[k]] : '';
        }
        return csvVal;
    },
    
       validateUploadFile: function(component) {
        var isValid = true;
        var cmpTarget = component.find('inputContainer');
		$A.util.removeClass(cmpTarget, 'slds-has-error');
		component.set("v.showErrorMessage", false); 
		var oppBundleFormat = /BulkOrderUpload_EEB-\d{10}.csv$/;
		var csvNameFormat = 'BulkOrderUpload_';
		var INVALID_FILE_NAME = $A.get("$Label.c.EE_Bulk_Order_Invalid_File_Name");
		var FILE_TOO_LARGE = $A.get("$Label.c.EE_Bulk_Order_Too_Large");
		var FIELD_VAL_MISSING= "Complete this field";
		var INVALID_FILE_FORMAT = "Invalid file format";
		var bundleName = component.get("v.parentOppName");  
        
        if (component.find("fileId").getElement().files.length > 0) {
            var fileName = component.find("fileId").getElement().files[0].name;
            var uploadFile = document.getElementById("fileId");
		    console.log('size: ' + uploadFile.files[0].size);
			console.log('fileName: ' + fileName);
			if(!fileName.toLowerCase().endsWith("csv")) {
				//Show validation message for file format;
				this.fileUploadErrorMsg(component, cmpTarget, INVALID_FILE_FORMAT);
			    isValid = false;
			} else {
			    //check file name format
			    if(!fileName.includes(bundleName) ||String(fileName).match(oppBundleFormat)==null) {
				    this.fileUploadErrorMsg(component, cmpTarget, INVALID_FILE_NAME);
			        isValid = false;
			    } else if(uploadFile.files[0].size > 26214400) {
				    this.fileUploadErrorMsg(component, cmpTarget, FILE_TOO_LARGE);
			        isValid = false;
			    }
			}
            
        } else {			
            //Show validation message for missing file;
			this.fileUploadErrorMsg(component, cmpTarget, FIELD_VAL_MISSING);
			isValid = false;
		}
		return isValid;
    },
    
    fileUploadErrorMsg: function(component, cmpTarget, errorMessage) {
        component.set("v.showErrorMessage", true); 
		$A.util.addClass(cmpTarget, 'slds-has-error');
		component.set("v.errorMessage", errorMessage);
    },
    
    uploadCSV: function(component, event) {
        component.set("v.showLoadingSpinner", true);
        var fileInput = component.find("fileId").getElement().files;
        var file = fileInput[0];
        var self = this;
        
        // create a FileReader object 
        var objFileReader = new FileReader();
        // set onload function of FileReader object   
        objFileReader.onload = $A.getCallback(function() {
            var fileContents = objFileReader.result;
            var base64 = 'base64,';
            var dataStart = fileContents.indexOf(base64) + base64.length;
 
            fileContents = fileContents.substring(dataStart);
            // call the uploadProcess method 
            self.uploadProcess(component, file, fileContents);
        });
 
        objFileReader.readAsDataURL(file);
    },
    
    uploadProcess: function(component, file, fileContents) {
        var fromPos = 0;
        var CHUNK_SIZE = 950000; 
        var toPos = Math.min(fileContents.length, fromPos + CHUNK_SIZE);
		
        // start with the initial chunk
        
        this.uploadInChunk(component, file, fileContents, fromPos, toPos, '');   
    },
     
    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
        var getchunk = fileContents.substring(startPosition, endPosition);
        var action = component.get("c.saveCSV");
        var CHUNK_SIZE = 950000;
        action.setParams({
            parentId: component.get("v.parentId"),
            fileName: file.name,
            base64Data: encodeURIComponent(getchunk),
            contentType: file.type,
            fileId: attachId
        });
 
        // set call back 
        action.setCallback(this, function(response) {
            // store the response / Attachment Id   
            var responseValues = JSON.parse(response.getReturnValue()); 
            var attachId = responseValues.fileId;
            var ErrorMessage = responseValues.ErrorMessage;
            
            if(ErrorMessage.indexOf('Line')!== -1 || ErrorMessage.indexOf('Failed')!== -1){
                var cmpTarget = component.find('inputContainer');
            	this.fileUploadErrorMsg(component,cmpTarget,ErrorMessage);
                this.resetQuoteStatus(component,event);
                return;
            }
            
            var state = response.getState();
            if (state === "SUCCESS") {
                // update the start position with end postion
                startPosition = endPosition;
                endPosition = Math.min(fileContents.length, startPosition + CHUNK_SIZE);
                if (startPosition < endPosition) {
                    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, attachId);
                }				
				this.showSuccessToast(component);// CPST-6972
				 
                window.setTimeout(
                    $A.getCallback(function() {
                        var a = component.get('c.init');
                        $A.enqueueAction(a);
                    }), 500
                );
                // handel the response errors        
            } else if (state === "INCOMPLETE") {
                alert("From server: " + response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        // enqueue the action
        $A.enqueueAction(action);
    },

	//CPST-6972
    showSuccessToast : function(component) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: $A.get("$Label.c.DF_BulkOrder_Toast"),
            duration:'15000',
            type: 'success',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
	
    processQuoteStatus: function(component, event, dfQIdList) {
            var action = component.get("c.updateQuoteStatusInProcess");
            console.log(component.get("v.parentId"));
            action.setParams({
                oppBundleId: component.get("v.parentId"),
                quoteIdList: dfQIdList
            });

            // set call back
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                   /*var a = component.get('c.init');
                   $A.enqueueAction(a);*/
                   this.uploadCSV(component,event);
                // handel the response errors
                } else if (state === "INCOMPLETE") {
                    alert("From server: " + response.getReturnValue());
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            // enqueue the action
            $A.enqueueAction(action);
        },
    
    resetQuoteStatus: function(component, event) {
                 var action = component.get("c.resetDFQuoteErrors");
                 console.log(component.get("v.parentId"));
                 action.setParams({
                     oppBundleId: component.get("v.parentId")
                 });

                 // set call back
                 action.setCallback(this, function(response) {
                     var state = response.getState();
                     if (state === "SUCCESS") {
                        window.setTimeout(
                                            $A.getCallback(function() {
                                                var a = component.get('c.init');
                                                $A.enqueueAction(a);
                                            }), 500
                                        );
                     // handel the response errors
                     } else if (state === "INCOMPLETE") {
                         alert("From server: " + response.getReturnValue());
                     } else if (state === "ERROR") {
                         var errors = response.getError();
                         if (errors) {
                             if (errors[0] && errors[0].message) {
                                 console.log("Error message: " + errors[0].message);
                             }
                         } else {
                             console.log("Unknown error");
                         }
                     }
                 });
                 // enqueue the action
                 $A.enqueueAction(action);
             },
    
    processOrderSummary: function(cmp) {
        var orderSummaryDataObjList = cmp.get("v.fullList");
		cmp.set('v.fullList', orderSummaryDataObjList);
		if ( cmp.get('v.searchEnabledFeatureToggle') ) {
		    if (orderSummaryDataObjList.length > 0) {
                cmp.set("v.parentOppName", cmp.get("v.fullList")[0].oppBundleName);
                cmp.set("v.parentOppId", cmp.get("v.fullList")[0].oppBundleId);
            } else {
                cmp.set("v.parentOppName", '');
                cmp.set("v.parentOppId", '');
            }
        } else {
            var parentOpptyName = cmp.get("v.fullList")[0].oppBundleName;
            cmp.set("v.parentOppName", parentOpptyName);
        }

        var pageCountVal = cmp.get("v.selectedCount");

        if (orderSummaryDataObjList != null) {
            cmp.set("v.maxPage", Math.floor((orderSummaryDataObjList.length + (pageCountVal - 1)) / pageCountVal));
            this.renderPage(cmp);
        }
    },    

    showQuickQuoteDetails: function (cmp, event, id) {
        var fullList = cmp.get('v.fullList');
        var results = fullList.filter(function (item) {
            return item.id === id;
        });
        cmp.set("v.selectedLocation", results[0]);
        cmp.set("v.isQuickQuoteDetailsOpen", true);
    },

    renderPage: function (cmp) {
        var records = cmp.get("v.fullList"),
            pageNumber = cmp.get("v.pageNumber"),
            pageCountVal = cmp.get("v.selectedCount"),
            pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));

        cmp.set('v.currentList', []);
        cmp.set("v.currentList", pageRecords);
    },

    //create DF_Order__c object calls createDF_Order in controller
    createDF_Order: function (cmp, event, id) {        
        var location = cmp.get("v.location");        
        var action = cmp.get("c.createDF_Order");
        action.setParams({
            dfQuoteId: id
        });

        action.setCallback(this, function (response) {
            var state = response.getState();            
            if (state === "SUCCESS") {
                var resp = response.getReturnValue();
                if (resp != "ERROR") {

                    var fullList = cmp.get('v.fullList');

                    var results = fullList.filter(function (item) {
                        return item.id === id;
                    });
                    cmp.set("v.selectedLocation", results[0]); //added
                    var orderDtEvt = cmp.getEvent("orderDetailPageEvent");                    
                    results[0].OrderId = resp;
                    results[0].orderId = resp;
                    cmp.set('v.location',results[0]);                    
                    orderDtEvt.setParam("location", results[0]);
                    orderDtEvt.fire();
                }
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },

    disableUploadIfNeeded: function(cmp) {
        if (!cmp.get('v.searchEnabledFeatureToggle')) {
            return;
        }
        
        var isQuoteDetailsOpened = cmp.get('v.isQuickQuoteDetailsOpen');
        if (!isQuoteDetailsOpened) {
            // To disable or enable links in current page
            this.toggleLinksInCurrentPage(cmp);
        }
        
        // To disable or enable upload related components
        var hasProcessingItem = this.hasProcessingQuote(cmp);
        this.toggleUpload(cmp, hasProcessingItem);
    },
    
    hasProcessingQuote: function(cmp) {
        var fullList = cmp.get('v.fullList');
        var hasProcessing = false;
        for (var i = 0; i < fullList.length; i++) {
            if (fullList[i].status === 'Processing') {
                hasProcessing = true;
                break;
            }
        }
        return hasProcessing;
    },
    
    toggleLinksInCurrentPage: function(cmp) {
        var currentList = cmp.get('v.currentList');
        var quoteLink = null, orderLink = null;
        for (var i = 0; i < currentList.length; i++) {
            quoteLink = document.getElementById('quoteLink-' + currentList[i].quoteId);
            orderLink = document.getElementById('orderLink-' + currentList[i].quoteId);
            
            if (currentList[i].status === 'Processing') {
            	this.toggleLinkEvent(quoteLink, true);
            	this.toggleLinkEvent(orderLink, true);
            } else {
            	this.toggleLinkEvent(quoteLink, false);
            	this.toggleLinkEvent(orderLink, false);
            }
			
            //CPST-7246
            if(currentList[i].status === 'Feasibility Expired'){
                this.hideLinkEvent(quoteLink, true);
                this.hideLinkEvent(orderLink, true);
            }
        }
    },
	
    //CPST-7246
    hideLinkEvent: function(link, isHidden){
        if(link != null){
            if(isHidden)
                link.style.display = 'none';
        }
    },
    
    toggleLinkEvent: function(link, isDisabled) {
        if (link != null) {
            if (isDisabled) {
                link.classList.add("not-active-link");
            } else {
                link.classList.remove("not-active-link");
            }
        }
    },
    
    toggleUpload: function(cmp, isDisabled) {
        var fileUpload = document.getElementById("fileId");
        if(fileUpload != null) 
        	fileUpload.disabled = isDisabled;
        cmp.set('v.disableBulk', isDisabled);
    },
    
    searchOrder: function(cmp, event, helper) {
        // Get custom label values
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);
        var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
        var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';
        var searchValue = cmp.find('orderSearch').get('v.value');
        if ($A.util.isEmpty(searchValue)) {
            searchValue = cmp.get('v.parentOppName');
        }
        helper.apex(cmp, "v.fullList", 'c.searchOrderSummaryData', {searchValue: searchValue}, true)
            .then(function (result) {
                cmp.set('v.pageNumber', '1');
                helper.processOrderSummary(cmp);
            })
            .catch(function (error) {
                $A.reportError("error message here", error);
            });
    },
    
    checkOrderPermissions : function(cmp) {
        
        // Create the action
		var action = cmp.get("c.getEEUserPermissionLevel");
                
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {                        	
            	// Get response string
            	var responseReturnValue = response.getReturnValue();
            	if (responseReturnValue == 4) {                    
            		// enable buttons/links for order
            		this.enableOrderButtons(cmp, true);
            	} else {
            		// disable buttons
					this.enableOrderButtons(cmp, false);       	
            	}
            } 
        });
        
        // Send action off to be executed
        $A.enqueueAction(action);
	},
    
    enableOrderButtons: function(cmp, isEnable) {      	
		cmp.set("v.isOrderLevelPermission", isEnable); 
    },

})