({
    closeModal: function(cmp, event, helper) {			
        helper.closeModal(cmp, event);		
    },
    
    back: function(cmp, event, helper) {	
        cmp.getEvent("hideQuickQuoteDetailsEvt").fire();
    },
    
    clickEditButton: function(cmp, event, helper) {		
        var OVCId = event.getSource().get("v.value");	
		helper.resetOVCMessage(cmp);
        helper.showOVCDetails(cmp, event, OVCId);		
    },
    
    init: function(cmp, event, helper) {	
        cmp.set('v.goNext', false);	
        helper.checkOrderPermissions(cmp);
        helper.loadBasketId(cmp);
        
    },
    
    saveAndCloseOVC: function(cmp, event, helper) {
        helper.saveOVC(cmp, event);		
        helper.closeModal(cmp, event);
    },
    
    saveOVC: function(cmp, event, helper) {		
        helper.saveOVC(cmp, event);
    },
    
    save: function(cmp, event, helper) {
        cmp.set('v.goNext', false);
        helper.save(cmp);
    },
    
    saveAndNext: function(cmp, event, helper) {
        cmp.set('v.goNext', true);
        helper.save(cmp);
    },
    
    skip: function(cmp, event, helper) {
        cmp.set('v.goNext', true);
		helper.createDF_Order(cmp, event, cmp.get("v.location").id); //added
       
        //helper.gotoNextPage(cmp);
    },
    
    addNewOVC: function(cmp, event, helper) {
        helper.addNewOVC(cmp, event);		
    },
    
    removeOVCRow: function(cmp, event, helper) {
        var OVCId = event.getSource().get("v.value");
        var OVCList	= cmp.get("v.OVCList");	
        
        OVCList = OVCList.filter(function(item) { return item.OVCId != OVCId; });
        cmp.set("v.OVCList", OVCList);
        
        helper.deleteOVC(cmp,OVCId);
        
        
    },		
    
    showSpinner: function(cmp, event, helper) {
        // Display spinner when aura:waiting (server waiting)
        cmp.set("v.showLoadingSpinner", true);
    },
    
    hideSpinner: function(cmp, event, helper) {	
        // Hide when aura:doneWaiting
        cmp.set("v.showLoadingSpinner", false);    
    }
})