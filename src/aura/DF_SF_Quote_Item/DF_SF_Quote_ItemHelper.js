({
    ATTR_CONFIG: [{
            csAttr: 'Term',
            slAttr: 'term',
            slOptionAttr: 'v.UTOptions',
            objectType: 'uni'
        },
        {
            csAttr: 'After Hours Site Visit',
            slAttr: 'AHA',
            slOptionAttr: 'v.AHAOptions',
            objectType: 'uni'
        },
        {
            csAttr: 'BTD Type',
            slAttr: 'BTDType',
            slOptionAttr: 'v.BTDTypeOptions',
            objectType: 'uni'
        },
        {
            csAttr: 'Zone',
            slAttr: 'zone',
            slOptionAttr: 'v.zoneOptions',
            objectType: 'uni'
        },
        {
            csAttr: 'eSLA',
            slAttr: 'SLA',
            slOptionAttr: 'v.SLAOptions',
            objectType: 'uni'
        },
        {
            csAttr: 'CoS High',
            slAttr: 'coSHighBandwidth',
            slOptionAttr: 'v.coSHighBandwidthOptions',
            objectType: 'ovc'
        },
        {
            csAttr: 'CoS Medium',
            slAttr: 'coSMediumBandwidth',
            slOptionAttr: 'v.coSMediumBandwidthOptions',
            objectType: 'ovc'
        },
        {
            csAttr: 'CoS Low',
            slAttr: 'coSLowBandwidth',
            slOptionAttr: 'v.coSLowBandwidthOptions',
            objectType: 'ovc'
        },
        {
            csAttr: 'POI',
            slAttr: 'POI',
            //slOptionAttr: 'v.POIOptions',
            objectType: 'ovc'
        },
        {
            csAttr: 'Route Type',
            slAttr: 'routeType',
            slOptionAttr: 'v.routeTypeOptions',
            objectType: 'ovc'
        },
        {
            csAttr: 'Interface Bandwidth',
            slOptionAttr: 'v.IBOptions',
            slAttr: 'interfaceBandwidth',
            objectType: 'uni'
        },
        {
            csAttr: 'Once off charge',
            slAttr: 'SCNR'
        },
        {
            csAttr: 'Recurring charge',
            slAttr: 'SCR'
        },
        {
            csAttr: 'Total_Routing_Charge_afterDiscount',
            slAttr: 'routeRecurring'
        },
        {
            csAttr: 'Hidden_TotalCoS_Charges',
            slAttr: 'coSRecurring'
        },
        {
            csAttr: 'CSA',
            slAttr: 'CSA'
        },
        {
            csAttr: 'Id',
            slAttr: 'Id'
        },
        {
            csAttr: 'Id',
            slAttr: 'OVCId'
        },
        {
            csAttr: 'Name',
            slAttr: 'OVCName'
        },
        {
            csAttr: 'OVC Recurring Charge',
            slAttr: 'monthlyCombinedCharges'
        }
    ],

    //hide and show pop up
    showOVCDetails: function (cmp, event, OVCId) {
        var OVCList = cmp.get("v.OVCList");
        var otCmp;
        var results = OVCList.filter(function (item) {
            return item.OVCId == OVCId;
        });
        var OVC = cmp.get("v.selectedOVC");
        if (OVC.CSA != "") {
            var action = cmp.get("c.doesCSADefaultToLocal");
            action.setParams({
                csa: OVC.CSA
            });

            action.setCallback(this, function (response) {
                var state = response.getState();

                if (state === "SUCCESS") {
                    var results = response.getReturnValue();
                    if (results === true) {
                        otCmp = cmp.find("routeType");
                        OVC.routeType = "Local";
                        cmp.set("v.selectedOVC", OVC);
                        otCmp.set("v.disabled", true);


                    } else {
                        otCmp = cmp.find("routeType");
                        otCmp.set("v.disabled", false);
                    }
                }
            });
            $A.enqueueAction(action);

        }

        cmp.set("v.selectedOVC", this.cloneOVC(results[0]));
        cmp.set("v.isModalOpen", true);
    },

    cloneOVC: function (OVC) {

        var clonedOVC = {
            'OVCId': OVC.OVCId,
            'OVCName': OVC.OVCName,
            'NNI': OVC.NNI,
            'routeType': OVC.routeType,
            'coSHighBandwidth': OVC.coSHighBandwidth,
            'coSMediumBandwidth': OVC.coSMediumBandwidth,
            'coSLowBandwidth': OVC.coSLowBandwidth,
            'routeRecurring': OVC.routeRecurring,
            'coSRecurring': OVC.coSRecurring,
            'POI': OVC.POI,
            'status': OVC.status
        };

        return clonedOVC;
    },

    validateRouteType: function (cmp, event) {

        var routeTypeCtrl = cmp.find("routeType");
        var tempCtrl = cmp.find("btnModalSaveOVC");

        //this.validateSelectCmpEmpty(routeTypeCtrl);

        var valid = routeTypeCtrl.get("v.validity").valid;

        if (!valid) {
            cmp.set("v.ovcSaveValidationError", true);
            this.reFocus(routeTypeCtrl, tempCtrl);
        }

        event.getSource().focus();

        return valid;
    },

    validateOVC: function (cmp, event) {

        var valid = true;
        valid = this.validateRouteType(cmp, event);
        return valid;

    },

    saveOVC: function (cmp, event) {

        if (!this.validateOVC(cmp, event)) {
            return;
        }

        var location = cmp.get("v.location");
        var selectedOVC = cmp.get("v.selectedOVC");
        var OVCList = cmp.get("v.OVCList");
        var selectedUNI = cmp.get("v.selectedUNI");

        var updatedOVCList = OVCList.map(function (item) {
            return item.OVCId === selectedOVC.OVCId ? selectedOVC : item
        });

        location.OVCs = updatedOVCList;
        location.UNI = [selectedUNI];

        cmp.set("v.OVCList", updatedOVCList);
        cmp.set("v.selectedOVC", selectedOVC);

        this.saveToCS(cmp);

        location.status = 'Quick Quote';
        cmp.set('v.location', location);

        this.updateDFQuoteStatus(cmp);

        var compEvent = cmp.getEvent("saveQuickQuoteDetailsEvt");
        compEvent.setParam("location", location);
        compEvent.fire();
    },

    save: function (cmp, event) {
        var location = cmp.get("v.location");
        var OVCList = cmp.get("v.OVCList");
        var selectedUNI = cmp.get("v.selectedUNI");

        location.OVCs = OVCList;
        location.UNI = [selectedUNI];
        location.status = 'Quick Quote';
        cmp.set('v.location', location);
        this.saveToCS(cmp);
        //changing the sequence a bit though it doesnt matter but to ensure save is triggered first
        this.createDF_Order(cmp, event, cmp.get("v.location").id); //added

        this.updateDFQuoteStatus(cmp);
        //if not proceed to order then trigger event
        var goFlag = cmp.get("v.goNext");
        if (!goFlag) {
            var compEvent = cmp.getEvent("saveQuickQuoteDetailsEvt");
            compEvent.setParam("location", location);
            compEvent.fire();
        }
    },

    updateDFQuoteStatus: function (cmp) {
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
        var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

        var quoteId = cmp.get("v.location").id;

        // Create the action
        var action = cmp.get("c.processUpdateDFQuoteStatus");

        action.setParams({
            "quoteId": quoteId
        });

        // Add callback behavior for when response is received
        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                // Get response string
                var responseReturnValue = response.getReturnValue();
                this.resetUNIMessage(cmp);
            } else {
                // Set error response comp with generic application error        	
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", ERR_MSG_APP_ERROR);
                cmp.set("v.showModalLoadingSpinner", false);

                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                    console.log(ERR_MSG_UNKNOWN_ERROR);
                }
            }
        });

        // Send action off to be executed
        $A.enqueueAction(action);
    },

    closeModal: function (cmp, event) {
        this.resetOVCMessage(cmp);
        cmp.set("v.isModalOpen", false);

    },

    showUniErrorMessage: function (cmp, errorMessage) {
        cmp.set("v.uniBannertype", "Banner");
        cmp.set("v.responseStatusUNIPage", "ERROR");
        cmp.set("v.responseMessageUNIPage", errorMessage);
        cmp.set("v.showModalLoadingSpinner", false);
    },

    loadBasketId: function (cmp) {

        var action = cmp.get("c.getBasketId");
        var basketID;
        var errorMessage = $A.get("$Label.c.DF_Application_Error");

        action.setParams({
            quoteId: cmp.get("v.location").id
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                basketID = response.getReturnValue();
                if (basketID != "Error") {
                    var location = cmp.get("v.location");
                    location.basketId = basketID;
                    cmp.set('v.location', location);
                    this.loadCSOptions(cmp);
                    this.resetUNIMessage(cmp);
                } else if (basketID === "Error") {
                    this.showUniErrorMessage(cmp, errorMessage);
                }

            } else if (state === "ERROR") {
                this.showUniErrorMessage(cmp, errorMessage);
            }
        });
        $A.enqueueAction(action);
    },

    loadCSConfig: function (cmp) {

        var action = cmp.get("c.getQuickQuoteData");
        var responseVal;
        var errorMessage = $A.get("$Label.c.DF_Application_Error");
        action.setParams({
            quoteId: cmp.get("v.location").id
        });

        action.setCallback(this, function (response) {

            var state = response.getState();
            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results != "Error") {
                    responseVal = JSON.parse(response.getReturnValue());

                    var extractedProductConfig = responseVal.ProductConfigMap;

                    cmp.set("v.structure", extractedProductConfig);

                    var attributes = {};
                    var configs = {};
                    for (var key in extractedProductConfig) {
                        if (extractedProductConfig.hasOwnProperty(key)) {
                            if (key.indexOf('-attributes') > -1) {
                                attributes[key.substr(0, key.indexOf('-attributes'))] = extractedProductConfig[key];
                            } else {
                                configs[key] = extractedProductConfig[key];
                            }
                        }
                    }

                    for (var key in attributes) {
                        if (attributes.hasOwnProperty(key)) {
                            configs[key].atts = attributes[key];
                        }
                    }

                    var configList = [];
                    for (var key in configs) {
                        if (configs.hasOwnProperty(key)) {
                            configList.push(configs[key]);
                        }
                    }

                    cmp.set("v.configs", configList);

                    //CPST-7198
                    this.apex(cmp, 'v.isSamZoneMappingUpdated', 'c.isBasketZoneMappingUpdated', {
                            basketId: cmp.get("v.location.basketId")
                        }, false)
                        .then(function (result) {
                            var helper = result.sourceHelper;
                            if (cmp.get('v.isSamZoneMappingUpdated')) {
                                console.log('SAM Zone Mapping Has Been Updated');
                                helper.save(cmp);
                            }
                            helper.resetUNIMessage(cmp);
                        })
                        .catch(function (result) {
                            var cmp = result.sourceCmp;
                            var helper = result.sourceHelper;
                            helper.showUniErrorMessage(cmp, errorMessage);
                        });

                    this.loadCSToUI(cmp, responseVal);

                } else if (results === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    this.showUniErrorMessage(cmp, errorMessage);
                }

            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                this.showUniErrorMessage(cmp, errorMessage);
            }
        });
        $A.enqueueAction(action);
    },

    sortOVC: function (a, b) {
        if (a.CreatedDate < b.CreatedDate)
            return -1;
        if (a.CreatedDate > b.CreatedDate)
            return 1;
        return 0;
    },

    loadCSToUI: function (cmp, responseVal) {

        var configList = cmp.get("v.configs");
        var location = cmp.get("v.location");
        var OVCList = [];
        var selectedUNI = cmp.get("v.selectedUNI");
        var selectedOVC = cmp.get("v.selectedOVC");

        location.address = responseVal.Address;
        location.locId = responseVal.LocId;
        location.basketId = responseVal.BasketId;
        location.quoteId = responseVal.QuoteId;
        location.FBC = responseVal.Fbc;
        //location.SCNR = location.SCNR !== responseVal.Scnr? responseVal.Scnr : location.SCNR;
        //location.SCR = location.SCR !== responseVal.Scr ? responseVal.Scr : location.SCR;

        location.DealDescription = responseVal.DealDescription;
        location.EtpApplies = responseVal.EtpApplies;
        var prodChargeName = 'Direct Fibre - Product Charges';

        var tempOVCList = configList.filter(function (item) {
            return item.Name != prodChargeName;
        });

        var tempProdChargeList = configList.filter(function (item) {
            return item.Name == prodChargeName;
        });

        var sortedTempOVCList = tempOVCList.sort(this.sortOVC);

        if (tempProdChargeList.length > 0) {
            var configAtts = tempProdChargeList[0].atts;
            selectedUNI.Id = tempProdChargeList[0].Id;
            selectedUNI.interfaceBandwidth = configAtts.filter(this.getAttByName('Interface Bandwidth'))[0].cscfga__Value__c;
            selectedUNI.BTDType = configAtts.filter(this.getAttByName('BTD Type'))[0].cscfga__Value__c;
            selectedUNI.AHA = configAtts.filter(this.getAttByName('After Hours Site Visit'))[0].cscfga__Value__c;
            selectedUNI.zone = configAtts.filter(this.getAttByName('Zone'))[0].cscfga__Value__c;
            //selectedUNI.AAT = configAtts.filter(this.getAttByName('Access Availability Target'))[0].cscfga__Value__c;
            selectedUNI.SLA = configAtts.filter(this.getAttByName('eSLA'))[0].cscfga__Value__c;
            selectedUNI.term = configAtts.filter(this.getAttByName('Term'))[0].cscfga__Value__c;

            location.SCNR = configAtts.filter(this.getAttByName('Once off charge'))[0].cscfga__Value__c;
            location.SCR = configAtts.filter(this.getAttByName('Recurring charge'))[0].cscfga__Value__c;
        }

        for (var i = 0; i < sortedTempOVCList.length; i++) {

            var configAtts = sortedTempOVCList[i].atts;

            if (selectedOVC.OVCId == sortedTempOVCList[i].Id) {
                selectedOVC.coSHighBandwidth = configAtts.filter(this.getAttByName('CoS High'))[0].cscfga__Value__c;
                selectedOVC.coSMediumBandwidth = configAtts.filter(this.getAttByName('CoS Medium'))[0].cscfga__Value__c;
                selectedOVC.coSLowBandwidth = configAtts.filter(this.getAttByName('CoS Low'))[0].cscfga__Value__c;
                selectedOVC.routeRecurring = configAtts.filter(this.getAttByName('Total_Routing_Charge_afterDiscount'))[0].cscfga__Value__c;
                selectedOVC.coSRecurring = configAtts.filter(this.getAttByName('Hidden_TotalCoS_Charges'))[0].cscfga__Value__c;
                selectedOVC.routeType = configAtts.filter(this.getAttByName('Route Type'))[0].cscfga__Value__c;
                selectedOVC.POI = configAtts.filter(this.getAttByName('POI'))[0].cscfga__Value__c;
                selectedOVC.CSA = configAtts.filter(this.getAttByName('CSA'))[0].cscfga__Value__c;
                selectedOVC.OVCName = sortedTempOVCList[i].Name;

                OVCList.push(selectedOVC);
            } else {
                var newOVC = {
                    'OVCId': sortedTempOVCList[i].Id,
                    'OVCName': sortedTempOVCList[i].Name,
                    'CSA': configAtts.filter(this.getAttByName('CSA'))[0].cscfga__Value__c,
                    'monthlyCombinedCharges': configAtts.filter(this.getAttByName('OVC Recurring Charge'))[0].cscfga__Value__c,
                    'routeType': configAtts.filter(this.getAttByName('Route Type'))[0].cscfga__Value__c,
                    'coSHighBandwidth': configAtts.filter(this.getAttByName('CoS High'))[0].cscfga__Value__c,
                    'coSMediumBandwidth': configAtts.filter(this.getAttByName('CoS Medium'))[0].cscfga__Value__c,
                    'coSLowBandwidth': configAtts.filter(this.getAttByName('CoS Low'))[0].cscfga__Value__c,
                    'routeRecurring': configAtts.filter(this.getAttByName('Total_Routing_Charge_afterDiscount'))[0].cscfga__Value__c,
                    'coSRecurring': configAtts.filter(this.getAttByName('Hidden_TotalCoS_Charges'))[0].cscfga__Value__c,
                    'POI': configAtts.filter(this.getAttByName('POI'))[0].cscfga__Value__c,
                    'CSA': configAtts.filter(this.getAttByName('CSA'))[0].cscfga__Value__c,
                    'status': 'Incomplete'
                };

                OVCList.push(newOVC);
            }
        }

        location.OVCs = OVCList;
        location.UNI = [selectedUNI];
        location.prodChargeConfigId = selectedUNI.Id;
        location.ovcConfigId = OVCList[0].OVCId;

        var prodchargeConfigurationId = selectedUNI.Id;
        var csa = OVCList[0].CSA;

        cmp.set("v.OVCList", OVCList);
        cmp.set('v.location', location);
        cmp.set("v.selectedUNI", selectedUNI);
        cmp.set("v.selectedOVC", selectedOVC);
        cmp.set("v.prodchargeConfigurationId", prodchargeConfigurationId);
        cmp.set("v.csa", csa);
        //console.log('Final spinner off');
        this.calculatetotals(cmp);
    },

    getAttByName: function (name) {
        return function (item) {
            return item.Name == name;
        }
    },

    gotoNextPage: function (cmp) {
        var location = cmp.get("v.location");
        var orderDtEvt = cmp.getEvent("orderDetailPageEvent");
        orderDtEvt.setParam("location", location);
        orderDtEvt.fire();
    },

    saveToCS: function (cmp) {
        var configList = cmp.get("v.configs");
        var OVCList = cmp.get("v.OVCList");
        var selectedUNI = cmp.get("v.selectedUNI");
        var selectedOVC = cmp.get("v.selectedOVC");

        var prodConfig = configList.filter(this.getAttByName('Direct Fibre - Product Charges'))[0];

        prodConfig.atts.filter(this.getAttByName('Interface Bandwidth'))[0].cscfga__Value__c = selectedUNI.interfaceBandwidth;
        prodConfig.atts.filter(this.getAttByName('BTD Type'))[0].cscfga__Value__c = selectedUNI.BTDType;
        prodConfig.atts.filter(this.getAttByName('After Hours Site Visit'))[0].cscfga__Value__c = selectedUNI.AHA;
        prodConfig.atts.filter(this.getAttByName('Zone'))[0].cscfga__Value__c = selectedUNI.zone;
        prodConfig.atts.filter(this.getAttByName('eSLA'))[0].cscfga__Value__c = selectedUNI.SLA;
        prodConfig.atts.filter(this.getAttByName('Term'))[0].cscfga__Value__c = selectedUNI.term;

        var configItem = configList.filter(function (item) {
            return item.Id == selectedOVC.OVCId;
        })[0];

        if (configItem) {

            configItem.atts.filter(this.getAttByName('CoS High'))[0].cscfga__Value__c = selectedOVC.coSHighBandwidth;
            configItem.atts.filter(this.getAttByName('CoS Medium'))[0].cscfga__Value__c = selectedOVC.coSMediumBandwidth;
            configItem.atts.filter(this.getAttByName('CoS Low'))[0].cscfga__Value__c = selectedOVC.coSLowBandwidth;

            configItem.atts.filter(this.getAttByName('POI'))[0].cscfga__Value__c = selectedOVC.POI;
            configItem.atts.filter(this.getAttByName('Route Type'))[0].cscfga__Value__c = selectedOVC.routeType;
        }

        var result = JSON.parse(JSON.stringify(cmp.get("v.configs")));
        var configs = {};
        var attributes = {}
        var parentId = "";
        for (var i = 0; i < result.length; i++) {
            var key = result[i].Id;
            attributes[key + '-attributes'] = result[i].atts;
            delete result[i]['atts'];
            configs[key] = result[i];
        }

        var location = cmp.get('v.location');
        var action = cmp.get("c.updateProduct");
        var errorMessage = "";

        action.setParams({
            basketId: location.basketId,
            configs: JSON.stringify(configs),
            attributes: JSON.stringify(attributes)
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results != "Error") {
                    this.calculatetotals(cmp); //added by Suman Gunaganti
                    this.loadCSConfig(cmp);
                    this.resetUNIMessage(cmp);

                    cmp.set("v.responseStatusOVC", "OK");
                    cmp.set("v.responseMessageOVC", $A.get("$Label.c.DF_Quick_Quote_OVC_Save_Success"));
                    cmp.set("v.messageTypeOVC", "Banner");
                } else if (results === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showModalLoadingSpinner", false);
                }
            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
            }

        });
        $A.enqueueAction(action);
    },

    calculatetotals: function (cmp) {

        var location = cmp.get('v.location');
        var basketIdVal = location.basketId;
        var prodChargeID = cmp.get('v.prodchargeConfigurationId');
        var errorMessage = "";
        var OVCList = cmp.get("v.OVCList");

        var ovcSCR = OVCList.reduce(function (sum, item) {
            return (sum * 1) + (Math.round(parseFloat((item.routeRecurring * Math.pow(10, 2)).toFixed(2))) / Math.pow(10, 2) * 1) + (Math.round(parseFloat((item.coSRecurring * Math.pow(10, 2)).toFixed(2))) / Math.pow(10, 2) * 1);
        }, 0);

        var OVCTotalBws = OVCList.reduce(function (sum, item) {
            return (sum * 1) + (Math.round(parseFloat(((parseFloat(item.coSHighBandwidth) + parseFloat(item.coSMediumBandwidth) + parseFloat(item.coSLowBandwidth)) * Math.pow(10, 2)).toFixed(2))) / Math.pow(10, 2) * 1);
        }, 0);

        if (isNaN(ovcSCR) && ovcSCR != null) {
            ovcSCR = null;
        }
        //console.log('Hidden ovc charges: ');
        //console.log(ovcSCR);

        var action = cmp.get("c.updateHiddenOVCCharges");
        var errorMessage = "";

        action.setParams({
            basketId: location.basketId,
            configId: prodChargeID,
            totalOVC: ovcSCR,
            TotalCoSBw: OVCTotalBws
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results != "Error") {
                    location.SCR = results;
                    cmp.set('v.location', location);
                    var goFlag = cmp.get("v.goNext");
                    if (goFlag) {
                        this.gotoNextPage(cmp);
                    } else {
                        this.resetUNIMessage(cmp);
                        cmp.set("v.showModalLoadingSpinner", false);
                    }
                } else if (results === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showModalLoadingSpinner", false);
                }
            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
            }

        });
        $A.enqueueAction(action);
    },

    loadCSOptionsByAttrList: function (cmp, attrNameList, ovcAttrList, basketId) {
        //console.log('loadCSOptionsByAttrList>>');
        var action = cmp.get("c.getOptionsList");
        var errorMessage = "";

        action.setParams({
            basketId: basketId,
            attNameList: attrNameList,
            ovcAttrList: ovcAttrList
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                var res = response.getReturnValue();
                if (res != "Error") {
                    var results = JSON.parse(response.getReturnValue());
                    var csOptionAttrList = this.getCSOptionNameArray(null);

                    this.ATTR_CONFIG.map(function (item) {
                        if (item.slOptionAttr && results[item.csAttr]) {

                            cmp.set(item.slOptionAttr, results[item.csAttr].map(function (result) {
                                return {
                                    label: result.Name == '--None--' ? 'Select' : result.Name,
                                    value: result.cscfga__Value__c
                                };
                            }));
                        }
                    });

                    this.loadCSConfig(cmp);
                    this.resetUNIMessage(cmp);

                } else if (res === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showModalLoadingSpinner", false);

                }
            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    getCSOptionNameArray: function (objectType) {
        return this.ATTR_CONFIG.reduce(function (list, item) {
            if (item.objectType === objectType && item.slOptionAttr) {
                list.push(item.csAttr);
            } //Get CloudSense attr name by object type								
            return list;
        }, []);
    },

    loadCSOptions: function (cmp) {

        var location = cmp.get('v.location');

        var uniAttrList = this.getCSOptionNameArray('uni');
        var ovcAttrList = this.getCSOptionNameArray('ovc');
        var attrList = uniAttrList.concat(this.getCSOptionNameArray('ovc'));

        this.loadCSOptionsByAttrList(cmp, uniAttrList, ovcAttrList, location.basketId);
    },

    deleteOVC: function (cmp, OVCId) {

        var action = cmp.get("c.removeOVC");
        var location = cmp.get('v.location');
        var basketIdVal = location.basketId;
        var relatedAtt = 'OVC';
        var errorMessage = "";
        var OVCList = cmp.get("v.OVCList");

        action.setParams({
            basketId: basketIdVal,
            configId: OVCId,
            relatedProductAtt: relatedAtt
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results === "Success") {
                    this.loadCSConfig(cmp);
                    this.resetUNIMessage(cmp);

                } else if (results === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showModalLoadingSpinner", false);
                }

            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },

    addNewOVC: function (cmp, event) {

        var action = cmp.get("c.addOVC");
        var location = cmp.get('v.location');
        var basketIdVal = location.basketId;
        var prodChargeID = cmp.get('v.prodchargeConfigurationId');
        var csaVal = cmp.get('v.csa');
        var errorMessage = "";

        action.setParams({
            basketId: basketIdVal,
            configId: prodChargeID,
            csa: csaVal
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                var results = response.getReturnValue();
                if (results === "Success") {
                    this.loadCSConfig(cmp);
                    this.resetUNIMessage(cmp);
                } else if (results === "Error") {
                    errorMessage = $A.get("$Label.c.DF_Application_Error");
                    cmp.set("v.uniBannertype", "Banner");
                    cmp.set("v.responseStatusUNIPage", "ERROR");
                    cmp.set("v.responseMessageUNIPage", errorMessage);
                    cmp.set("v.showModalLoadingSpinner", false);
                }

            } else if (state === "ERROR") {
                errorMessage = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.uniBannertype", "Banner");
                cmp.set("v.responseStatusUNIPage", "ERROR");
                cmp.set("v.responseMessageUNIPage", errorMessage);
                cmp.set("v.showModalLoadingSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },

    resetOVCMessage: function (cmp) {
        cmp.set("v.responseStatusOVC", "");
        cmp.set("v.responseMessageOVC", "");
        cmp.set("v.messageTypeOVC", "");
    },

    resetUNIMessage: function (cmp) {
        cmp.set("v.responseStatusUNIPage", "");
        cmp.set("v.responseMessageUNIPage", "");
        cmp.set("v.uniBannertype", "");
    },

    //create DF_Order__c object calls createDF_Order in controller
    createDF_Order: function (cmp, event, id) {
        //console.log('=======DF_SF_QuoteHelper==== createDF_Order START');
        var location = cmp.get("v.location");
        // var dfQuoteId = location.Id;
        //console.log('=======DF_SF_QuoteHelper==== createDF_Order quoteId' + id);
        var action = cmp.get("c.createDF_Order");
        action.setParams({
            dfQuoteId: id
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            //console.log('=======DF_SF_QuoteHelper==== createDF_Order state ' + state);
            if (state === "SUCCESS") {
                var resp = response.getReturnValue();
                if (resp != "ERROR") {
                    //console.log('=======DF_SF_QuoteHelper==== createDF_Order orderId ' + resp);
                    location.OrderId = resp;
                    cmp.set("v.location", location);
                    var goFlag = cmp.get("v.goNext");
                    if (goFlag) {
                        this.gotoNextPage(cmp);
                    }
                }
            } else if (state === "INCOMPLETE") {
                // do something
                console.log("Invalid Basket : " + resp);
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
    },

    checkOrderPermissions: function (cmp) {

        // Create the action
        var action = cmp.get("c.getEEUserPermissionLevel");

        // Add callback behavior for when response is received
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Get response string
                var responseReturnValue = response.getReturnValue();
                if (responseReturnValue == 4) {
                    // enable buttons/links for order
                    this.enableOrderButtons(cmp, true);
                } else {
                    // disable buttons
                    this.enableOrderButtons(cmp, false);
                }
            }
        });

        // Send action off to be executed
        $A.enqueueAction(action);
    },

    enableOrderButtons: function (cmp, isEnable) {
        cmp.set("v.isOrderLevelPermission", isEnable);
    },

})