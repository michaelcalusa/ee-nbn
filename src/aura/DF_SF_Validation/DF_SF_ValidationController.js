({
    init: function (cmp, event, helper) {
        var errorLabel = $A.get("$Label.c.DF_Application_Error");

        //Show entries values. This controls the no of records displayed in a page
        var opts = [
            { value: "5", label: "" },
            { value: "5", label: "5" },
            { value: "10", label: "10" },
            { value: "15", label: "15" },
            { value: "20", label: "20" },
            { value: "25", label: "25" }
        ];
        cmp.set("v.countOptions", opts);

        //Set the column fields
        if ( !cmp.get('v.isArgoEnhancementsEnabled') ) {
            cmp.set('v.columns', [
                {label: 'Location ID', fieldName: 'locId', type: 'text'},
                {label: 'Address', fieldName: 'address', type: 'text', initialWidth: 450},
                {label: 'Latitude', fieldName: 'latitude', type: 'text'},
                {label: 'Longitude', fieldName: 'longitude', type: 'text'},
                {label: 'Status', fieldName: 'status', type: 'email', initialWidth: 180}
            ]);
        }

        //Declare the process that calls the apex controller method
        helper.fetchSiteData(cmp, event, helper, false);

        var getArgoEnhancementsToggleAction = cmp.get("c.isArgoEnhancementsEnabled");
        getArgoEnhancementsToggleAction.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                cmp.set("v.isArgoEnhancementsEnabled", response.getReturnValue());
                // add 'Edit' column
                if (cmp.get("v.isArgoEnhancementsEnabled")) {
                    cmp.set('v.columns', [
                        {label: 'Location ID', fieldName: 'locId', type: 'text', sortable: true},
                        {label: 'Address', fieldName: 'address', type: 'text', sortable: true, initialWidth: 450},
                        {label: 'Latitude', fieldName: 'latitude', type: 'text', sortable: true},
                        {label: 'Longitude', fieldName: 'longitude', type: 'text', sortable: true},
                        {label: 'Status', fieldName: 'status', type: 'email', sortable: true, initialWidth: 180}
                    ]);
                    $A.util.removeClass(expButt, 'slds-hide');
                    helper.addEditColumn(cmp);
                }else{//MRTEST-53531
                    var expButt = cmp.find("export-location-button");
                    $A.util.addClass(expButt, 'slds-hide');
                }
            } else {
                cmp.set("v.isArgoEnhancementsEnabled", false);
                //MRTEST-53531
                var expButt = cmp.find("export-location-button");
                $A.util.addClass(expButt, 'slds-hide');
            }
        });

        $A.enqueueAction(getArgoEnhancementsToggleAction);
    },

    refreshSiteData: function (cmp, event, helper) {
        helper.fetchSiteData(cmp, event, helper, false);
    },

    handleRowAction: function(cmp, event, helper) {
        if (cmp.get("v.isArgoEnhancementsEnabled")) {
            // only when toggle is ON
            var action = event.getParam('action');
            if (action.name === 'editRowItem') {
                var rowData = event.getParam('row');
                console.log('row: ' + JSON.stringify(rowData));

                helper.openModalWithData(cmp, helper, rowData);
            }
        }
    },

    closeModal: function(cmp, event, helper) {
        helper.closeModalWindow(cmp, event, helper, false);
    },

    handleSearchTypeChange: function(cmp, event, helper) {
        helper.clearModalMsg(cmp);
        helper.showModalSearchForm(cmp);
    },

    onMultipleAddressSelect: function(cmp, event, helper) {
        var unstructuredAddress = cmp.get("v.unstructuredAddress");
        var structuredAddressList = cmp.get("v.structuredAddressList");
        var foundMatchingStructuredAddress = false;

        for (var i = 0; i < structuredAddressList.length; i++) {
            var structuredAddress = structuredAddressList[i];
            console.log("found structuredAddress: " + JSON.stringify(structuredAddress));
            if(unstructuredAddress == structuredAddress.unstructured){
                helper.populateAddressFormFromStructuredAddress(cmp, helper, structuredAddress);
                foundMatchingStructuredAddress = true;
                break;
            }
        }

        if(!foundMatchingStructuredAddress){
            helper.clearAddressTerms(cmp, helper);
        }
    },

    performEdit: function(cmp, event, helper) {
        var isValid = helper.validateSearchForm(cmp, helper);
        console.log("performSearch:" + cmp.get("v.searchTypeValue") + ", form validity:" + isValid);
        if (isValid) {
            helper.doModalEdit(cmp, event);
        }
    },

    handleCount: function(cmp, event, helper) {
        //This gets triggered every time the user changes the no of records displayed per page
        cmp.set("v.pageNumber", 1);
        var records = cmp.get("v.sitedata");
        cmp.set("v.currentList", records);
        var pageCountVal = cmp.get("v.selectedCount");
        cmp.set("v.maxPage", Math.floor((records.length+(pageCountVal-1))/pageCountVal));
        helper.renderPage(cmp);
        var childCmp = component.find("cComp");
        childCmp.resetValues();
    },

    renderPage: function(cmp, event, helper) {
        helper.renderPage(cmp);
    },

    goto: function(cmp, event, helper) {
        var homeDtEvt = cmp.getEvent("homePageEvent");
        homeDtEvt.setParams({"requestType" : cmp.get('v.sourceRequestType') });
        homeDtEvt.fire();
    },

    proceed: function(cmp, event, helper) {
        var errorLabel = $A.get("$Label.c.DF_Application_Error");
        //Make a call to the apex method to submit a service feasibility request
        var action = cmp.get("c.processSiteData");
        var OppId = cmp.get("v.parentOppId");
        action.setParams({"oppBundleId": cmp.get("v.parentOppId") });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var siteDtEvt = cmp.getEvent("siteDetailPageEvent");
                siteDtEvt.fire();
            }
            else if(state === "ERROR") {
                cmp.set("v.responseStatus",state);
                cmp.set("v.type","Banner");
                cmp.set("v.responseMessage",errorLabel);
            }
        });

        $A.enqueueAction(action);
    },
    
    goToRaiseRequest:function(cmp, event, helper) {
        var eUrl= $A.get("e.force:navigateToURL");
        var serviceReqHost = $A.get("$Label.c.Service_Request_Host");
        console.log('serviceReqHost:: ' + serviceReqHost);
        eUrl.setParams({
            "url": 'https://' + serviceReqHost + '/online_customers/page/ticket/servicerequest/new/setup/display' 
        });
        eUrl.fire();
    },

    exportLocations : function(cmp, event, helper) {
        var locations = cmp.get("v.sitedata");
        var csvStringResult = '"Location Id", Address, Latitude, Longitude, Status\n';
        for (var i = 0; i < locations.length; i++) {
            csvStringResult = csvStringResult
                + '"' + helper.defaultIfEmpty(locations[i].locId, 'N/A') +'",'
                + '"' + helper.defaultIfEmpty(locations[i].address, 'N/A') +'",'
                + '"' + helper.defaultIfEmpty(locations[i].latitude, 'N/A') +'",'
                + '"' + helper.defaultIfEmpty(locations[i].longitude, 'N/A') +'",'
                + '"' + helper.defaultIfEmpty(locations[i].status, 'N/A') +'"\n';
        }

        var hiddenElt = document.createElement('a');
        hiddenElt.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvStringResult);
        hiddenElt.target = '_self';
        hiddenElt.download = cmp.get('v.parentOppName') + '-locations.csv';
        document.body.appendChild(hiddenElt);
        hiddenElt.click();
    },

    updateSortOrder : function (cmp, event, helper) {
        helper.updateColumnSorting(cmp, event, helper, 'v.sitedata');
    }
});