({
	init: function (cmp, event, helper) {		    	

		//Go to SF order summary page if query string parameters are detected.
    	var parentOppIdParam = helper.getUrlParameter(cmp, helper.PARENT_OPP_ID_PARAM_NAME);
		var pageParam = helper.getUrlParameter(cmp, helper.PARENT_OPP_ID_PARAM_NAME);
        
        if (pageParam != null && parentOppIdParam != null) {
			cmp.set('v.batchId', parentOppIdParam);
			helper.goToQuotePage(cmp);
		}
    },    

	updateOppBundleId : function(cmp, event, helper) {
        var opptBundleId = event.getParam("oppBundleId");
        cmp.set("v.batchId", opptBundleId);  
        cmp.set("v.lstSitePage", true);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
		cmp.set("v.quotePage", false);    
		cmp.set("v.orderPage", false);   
    },
    
    goToRAGPage :function(cmp, event, helper) {
        var opptBundleId = event.getParam("oppBundleId");
        console.log('ragPage opptBundleId : ' + opptBundleId);
        if(opptBundleId!=null)
            cmp.set("v.batchId", opptBundleId);

        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", true); 
		cmp.set("v.quotePage", false);    
		cmp.set("v.orderPage", false);    
    },

	goToQuotePage :function(cmp, event, helper) {
        helper.goToQuotePage(cmp);
    },

    goToOrderPage :function(cmp, event, helper) {
		var location = event.getParam("location");
        cmp.set("v.location", location); 
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
		cmp.set("v.quotePage", false); 
		cmp.set("v.orderPage", true);
    },	
})