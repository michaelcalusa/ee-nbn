({	
	goToQuotePage :function(cmp) {
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
		cmp.set("v.quotePage", true); 
		cmp.set("v.orderPage", false);
    },
})