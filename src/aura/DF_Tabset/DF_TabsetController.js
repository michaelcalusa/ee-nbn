({
	onSelect: function (cmp, event, helper) {

		var defaultTabId = cmp.get("v.defaultTabId");
		var onlyShowDefaultTabId = cmp.get("v.onlyShowDefaultTabId");

		if(onlyShowDefaultTabId)
		{
			cmp.set("v.tabId", defaultTabId);
		}
	},

	handleChange: function(cmp) {

		var selected = cmp.get("v.tabId");  
        cmp.find("tabs").set("v.selectedTabId", selected);

    },

})