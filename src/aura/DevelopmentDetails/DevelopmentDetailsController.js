({
    onload : function(component, event, helper){              
        var buildingTypes = component.get("c.getBuildingTypes");
        var devWorkTypes = component.get("c.getDevWorkTypes");
        var appNum = component.get("c.getApplicationNumber");        
        appNum.setParams({
            "newDevId" : component.get("v.newDevApplicantWrapperInstance").newDevApplicantId               
        });
        
        buildingTypes.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                component.set("v.buildingTypeList",response.getReturnValue());
            }            
        });
        var action1 = component.get("c.getwhattypeofbuild");
        action1.setCallback(this, function(response){
            if(response.getState() === 'SUCCESS'){
                var buildtypeList = response.getReturnValue();
                component.set("v.buildtypeList",buildtypeList);
                
                var buildtypeMap = [];
                for(var i = 0; i < buildtypeList.length; i++){
                    var buildRole = buildtypeList[i];
                    if(buildtypeList[i] == 'Lead-In Conduit'){
                        buildtypeMap.push({"name" : "Lead-in Conduit (LIC)", "desc"  : "All the premises face an existing road and there is no requirement for pit and pipe or pathway works.","img"  :$A.get('$Resource.LeadinConduit'), "class" :"LIC", "displayImage":true});
                    }
                    else if(buildtypeList[i] == 'Pit and Pipe'){
                        buildtypeMap.push({"name" : "Pit and pipe", "desc"  : "Development may be a Single Dwelling Unit (SDU), super lots, horizontal or hybrid multi dwelling units. It may also include private roads. Build work includes pits, pipes and conduits.","img"  :$A.get('$Resource.Pitandpipe'),"class" :"PitnPipe", "displayImage":true});
                    }
                        else if(buildtypeList[i] == 'Pathways'){
                            buildtypeMap.push({"name" : "Pathways", "desc"  : "Development may be a Multi Dwelling Units (MDU) and Multi Premises Sites (MPS) that are usually vertically constructed.","img"  :$A.get('$Resource.Pathwaysonly'), "class" :"Pathways", "displayImage":true});
                        }
                }
                component.set("v.BuildType",buildtypeMap);
            }
            else if(state === "ERROR") {
                console.log('error==');
            }
        });
        devWorkTypes.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                component.set("v.devWorkTypeList",response.getReturnValue());
            }
        });
        
        appNum.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                component.set("v.applicationNumber",response.getReturnValue());
            }            
        });
        
        $A.enqueueAction(buildingTypes);
        $A.enqueueAction(devWorkTypes);
        $A.enqueueAction(action1);
        $A.enqueueAction(appNum);
    },
    
    displayMapTemp : function(component, event, helper) {
		
	},
    
    handleCollapsableSection : function(component, event, helper){
        if(!component.get("v.disableSection")){
            component.set("v.displaySection",!(component.get("v.displaySection")));
            if(component.get("v.displaySection")){
                var disEvent = component.getEvent("displaySectionChange");
                disEvent.setParams({
                    "firedComponentName" : 'developmentDetails'
                });
                disEvent.fire();
                var wrapInstance = component.get("v.newDevApplicantWrapperInstance");              
                if(wrapInstance.devDtlWrapper.receivedNbnConditionalApproval != null){
                    if(wrapInstance.devDtlWrapper.receivedNbnConditionalApproval == true){
                        console.log('true conditonal approval inside button checked' );                        
                        component.set("v.unCheckedConditionalApproval",false);    
                    }
                    else{
                        component.set("v.checkedConditionalApproval",false);
                        component.set("v.unCheckedConditionalApproval",true);   
                    }   
                }
            }
        }
    },
    
    checkPremisesAboveEssentialServices : function (component, event, helper) {
        var buttonId = event.currentTarget.id;
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(buttonId == 'cpd-yes-toggleBtnId'){
            $A.util.addClass(document.getElementById('cpd-yes-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById('cpd-no-toggleBtnId'),'btn-primary');
            component.set('v.showPremisesAboveEssentialServices',true);
            component.set('v.showNumberOfEssentialServices',true);
            component.set('v.selEssentialServices',true);
        }
        else if(buttonId == 'cpd-no-toggleBtnId'){
            $A.util.addClass(document.getElementById('cpd-no-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById('cpd-yes-toggleBtnId'),'btn-primary');
            component.set('v.showPremisesAboveEssentialServices',false); 
            component.set('v.showNumberOfEssentialServices',false);
            component.set('v.selEssentialServices',false);
            wrapInstance.devDtlWrapper.numberOfEssentialServices = 0;
        }
        helper.clearLICData(component, event, helper);
    },
    
    buildingTypeChanged : function(component, event, helper) {
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        wrapInstance.devDtlWrapper.receivedNbnConditionalApproval = false;
        var selBuildingType = document.getElementById('cpd-selectBuildingType').value;         
        component.set("v.choosedbuildingType",event.target.value);         
        if(selBuildingType == 'Mixed residential and commercial use') {
            component.set('v.showNumberOfPremisesRes',true);
            component.set('v.showNumberOfPremisesComm',true);              
        }
        else{
            component.set('v.showNumberOfPremisesRes',false);
            component.set('v.showNumberOfPremisesComm',false);
            wrapInstance.devDtlWrapper.numberOfResidentialPremises = '';
            wrapInstance.devDtlWrapper.numberOfCommercialPremises = '';
        }
	},
    
    devWorkTypeChanged : function(component, event, helper) {
        var selDevWorkType = document.getElementById('cpd-inputSelectDevType').value;   
        var selNumberOfPremises = document.getElementById('cpd-numberOfPremises').value; 
        var  msgPremisesEqualTo1= "";
        component.set('v.choosedDevelopmentType',selDevWorkType);
        if (selDevWorkType == 'Sub divisions or empty lots') {                 
                 component.set('v.showSubDivEmptyLotsSection',true);       
                 component.set('v.showOutBuildingsSection',false); 
                 component.set('v.showPremisesGreterThan1Sec',false);                                  
                 component.set('v.showRejectionMessage1Sec',false);
                 component.set('v.showBlankLotsCondApprSec',false);
                 component.set('v.showOwnerExistingPremisesSec',false);
                 component.set('v.showOtherOwnerExistingPremisesSec',false);
                 component.set('v.showApplicantNextButton',false);
        }
        else if (selDevWorkType == 'Knockdown and rebuild') {                            
                 if (selNumberOfPremises > 1) {                       
                     component.set('v.showPremisesGreterThan1Sec',true); 
                     component.set('v.showRejectionMessage1Sec',false);
                     component.set('v.showSubDivEmptyLotsSection',false);
                     component.set('v.showBlankLotsCondApprSec',false); 
                     component.set('v.showOwnerExistingPremisesSec',false);
                     component.set('v.showOtherOwnerExistingPremisesSec',false);
                     component.set('v.showApplicantNextButton',true);
                 }
                 else if (selNumberOfPremises = 1) {
                          var rejectionMessage1__Text1 = "";
                          var rejectionMessage1__Text2 = "";
                          var rejectionMessage1__Text3 = "";
                          component.set('v.showRejectionMessage1Sec',true);
                          component.set('v.showPremisesGreterThan1Sec',false);
                          component.set('v.showSubDivEmptyLotsSection',false);                                                   
                          component.set('v.showBlankLotsCondApprSec',false); 
                          component.set('v.showOwnerExistingPremisesSec',false);
                          component.set('v.showOtherOwnerExistingPremisesSec',false);
                          component.set('v.showApplicantNextButton',false);
                          component.set('v.disableDevDetails',true);
                           // Disable Form logic
                           component.set("v.showBuildTypesSection",false);
                           component.set('v.disableNewDevForm',false);
                           var disableForm = component.getEvent("disableForm");
                           disableForm.setParams({
                               "disableNewDevForm" : true
                           });
                           disableForm.fire();
                           //Save Record
                           var processRec = component.get('c.processDevelopmentDetails');
                           $A.enqueueAction(processRec);
                 }
        }
        else if(selDevWorkType == 'Other (including granny flats)'){                
                component.set('v.showSubDivEmptyLotsSection',false);
                component.set('v.showPremisesGreterThan1Sec',false);                 
                component.set('v.showRejectionMessage1Sec',false);
                component.set('v.showBlankLotsCondApprSec',false);
                component.set('v.showOwnerExistingPremisesSec',false);
                component.set('v.showOtherOwnerExistingPremisesSec',false); 
                component.set('v.showApplicantNextButton',true);
        }
	},
    
    showBlankLotCondApprSec : function(component, event, helper) {
        component.set('v.showBlankLotsCondApprSec',true);
        component.set('v.showApplicantNextButton',true);
        component.set('v.showRejectionMessage2',false);
        component.set('v.hideRejectionMessage2',true);
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        wrapInstance.devDtlWrapper.receivedNbnConditionalApproval = true;
        component.set("v.checkedConditionalApproval",true);
        component.set("v.unCheckedConditionalApproval",false);	
    },
    
    hideBlankLotCondApprSec : function(component, event, helper) {      	
        component.set('v.showBlankLotsCondApprSec',false); 
        component.set('v.showApplicantNextButton',true);
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        wrapInstance.devDtlWrapper.receivedNbnConditionalApproval = false;
        wrapInstance.devDtlWrapper.applicationReferenceNumber = null;
        component.set("v.checkedConditionalApproval",false);
        component.set("v.unCheckedConditionalApproval",true);      
    },
    
    radioBtnChangedPropertyOwner : function(component, event, helper) {
             component.set("v.selectedRadioPropertyOwner",true);        
             component.set('v.showOwnerExistingPremisesSec',true);
             component.set('v.showOtherOwnerExistingPremisesSec',false);
             component.set('v.showApplicantNextButton',false);
             component.set('v.disableField',true);
             component.set('v.numberOfpremisesDisabled',true);
             component.set('v.disableKnockDownSecFields',true);
             var processRec = component.get('c.processDevelopmentDetails');
             $A.enqueueAction(processRec);
    },     
    radioBtnChangedOtherPropertyOwner : function(component, event, helper) {
            component.set("v.selectedRadioPropertyOwner",false);
            component.set('v.showOtherOwnerExistingPremisesSec',true); 
            component.set('v.showOwnerExistingPremisesSec',false);
            component.set('v.showApplicantNextButton',false);   
            // Disable Form logic
            component.set('v.disableNewDevForm',false);
        	component.set("v.showBuildTypesSection",false);
            var disableForm = component.getEvent("disableForm");
            disableForm.setParams({
                 "disableNewDevForm" : false
           });
           disableForm.fire();
    },
    validateAddress : function(component, event, helper) {
        //alert('validateAddress called');
        var isAddressPopulated =  component.get("v.addressPopulated");  
        
        var fieldId = event.currentTarget.id;       
        var fieldVal = document.getElementById(fieldId).value;        
        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);
        if(isAddressPopulated=="false" || isAddressPopulated==undefined){
            document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please select the address before proceeding with premises.';
            document.getElementById(fieldId).value='';
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }        
    },
    validateFields : function(component, event, helper) {    	                        
		var fieldId = event.currentTarget.id;       
        var fieldVal = document.getElementById(fieldId).value;        
        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);       
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;
        var mobileNumSpanId = document.getElementById("cpd-propertyOwnerMobileNumber-error");
        var mobileNumDivId = document.getElementById("cpd-propertyOwnerMobileNumber-div");
        var phoneNumSpanId = document.getElementById("cpd-propertyOwnerPhoneNumber-error");
        var phoneNumDivId = document.getElementById("cpd-propertyOwnerPhoneNumber-div");
    	
        if (fieldVal == '') {
            if(fieldId == 'cpd-numberOfPremises' ) {
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill number of premises.';                
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');                
            }
            else if (fieldId == 'cpd-numberOfEssentialSer' ) {
                     document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill number of non-premises services.';           
                     $A.util.addClass(errorSpanId,'help-block-error');
                     $A.util.addClass(errorDivId,'has-error');
                     $A.util.removeClass(errorDivId,'has-success');
            }
            else if (fieldId == 'cpd-numberOfPremisesRes' ) {
                     document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill number of residential premises.';
                     $A.util.addClass(errorSpanId,'help-block-error');
                     $A.util.addClass(errorDivId,'has-error');
                     $A.util.removeClass(errorDivId,'has-success');
            }
            else if (fieldId == 'cpd-numberOfPremisesComm') {
                     document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill number of commercial premises.';
                     $A.util.addClass(errorSpanId,'help-block-error');
                     $A.util.addClass(errorDivId,'has-error');
                     $A.util.removeClass(errorDivId,'has-success');
            }
            else if (fieldId == 'cpd-propertyOwnerFirstName') {
                     document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill Property Owner First Name';
                     $A.util.addClass(errorSpanId,'help-block-error');
                     $A.util.addClass(errorDivId,'has-error');
                     $A.util.removeClass(errorDivId,'has-success');
            }
            else if (fieldId == 'cpd-propertyOwnerLastName') {
                     document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill Property Owner Last Name';
                     $A.util.addClass(errorSpanId,'help-block-error');
                     $A.util.addClass(errorDivId,'has-error');
                     $A.util.removeClass(errorDivId,'has-success');
            }
            else if (fieldId == 'cpd-propertyOwnerEmailAddress') {
                     document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill Property Owner Email address';
                     $A.util.addClass(errorSpanId,'help-block-error');
                     $A.util.addClass(errorDivId,'has-error');
                     $A.util.removeClass(errorDivId,'has-success');
            }
  
            else if(fieldId == 'cpd-estimatedOccupancyDate')
            {
               $A.util.addClass(errorSpanId,'help-block-error');
               $A.util.addClass(errorDivId,'has-error');
               $A.util.removeClass(errorDivId,'has-success'); 
            }
        }
        else {
            	//S NPD
            	if(fieldId == 'cpd-numberOfPremises' && (component.get("v.addressPopulated")=="false" || component.get("v.addressPopulated")==undefined)) {
                    document.getElementById(fieldId).value='';
                	document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please select the address before proceeding with premises.';                
                	$A.util.addClass(errorSpanId,'help-block-error');
                	$A.util.addClass(errorDivId,'has-error');
                	$A.util.removeClass(errorDivId,'has-success');                
            	}
            	//E NPD
                if(!fieldVal.match(/^[0-9]+$/) &&  (fieldId == 'cpd-numberOfPremises' || fieldId == 'cpd-numberOfEssentialSer' || fieldId == 'cpd-numberOfPremisesRes' || fieldId == 'cpd-numberOfPremisesComm')){
                    document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a number';
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                }
                else if(fieldVal ==0 && (fieldId == 'cpd-numberOfPremises' || fieldId == 'cpd-numberOfEssentialSer')){
                    document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a number 1 or greater';
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                }
                else if ( fieldVal != '' && (fieldId == 'cpd-propertyOwnerFirstName' || fieldId == 'cpd-propertyOwnerLastName')) { 
                                $A.util.addClass(errorSpanId,'help-block-error');               
                                $A.util.removeClass(errorDivId,'has-error');              
                                $A.util.addClass(errorDivId,'has-success');      
                  }
                else if (fieldId == 'cpd-propertyOwnerPhoneNumber' && fieldVal != '') { 
                         if(((fieldVal.substring(0,2) != '02' && fieldVal.substring(0,2) != '03' && fieldVal.substring(0,2) != '04' && fieldVal.substring(0,2) != '07' && fieldVal.substring(0,2) != '08') || fieldVal.length != 10 || !fieldVal.match(/^[0-9]+$/))){                    
                              document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid Phone number';
                              $A.util.addClass(errorSpanId,'help-block-error');
                              $A.util.addClass(errorDivId,'has-error');
                              $A.util.removeClass(errorDivId,'has-success');
                         }
                         else {
                                $A.util.addClass(errorSpanId,'help-block-error');               
                                $A.util.removeClass(errorDivId,'has-error');              
                                $A.util.addClass(errorDivId,'has-success');                             
                                //To make mobile number field not mandatory(When phone number entered)                               
                                $A.util.addClass(mobileNumSpanId,'help-block-error');    
                                $A.util.removeClass(mobileNumDivId,'has-error');  
                                $A.util.removeClass(mobileNumDivId,'has-success');
                         }        
                  }
                 else if (fieldId == 'cpd-propertyOwnerMobileNumber' && fieldVal != '') { 
                         if(((fieldVal.substring(0,2) != '04' || fieldVal.length != 10 || !fieldVal.match(/^[0-9]+$/)))){
                              document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid Mobile number';
                              $A.util.addClass(errorSpanId,'help-block-error');
                              $A.util.addClass(errorDivId,'has-error');
                              $A.util.removeClass(errorDivId,'has-success');
                          }
                          else {
                                 $A.util.addClass(errorSpanId,'help-block-error');               
                                 $A.util.removeClass(errorDivId,'has-error');              
                                 $A.util.addClass(errorDivId,'has-success');
                                 //To make phone number field not mandatory(When mobile number entered)                                  
                                 $A.util.addClass(phoneNumSpanId,'help-block-error');    
                                 $A.util.removeClass(phoneNumDivId,'has-error');  
                                 $A.util.removeClass(phoneNumDivId,'has-success');
                              
                          }        
                  }
                else if((fieldId=='cpd-propertyOwnerEmailAddress')){
                    if(!fieldVal.match( /^[^\s@]+@[^\s@]+\.[^\s@]+$/)){
                        document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid Email address';
                        $A.util.addClass(errorSpanId,'help-block-error');
                        $A.util.addClass(errorDivId,'has-error');
                        $A.util.removeClass(errorDivId,'has-success'); 
                    }
                    else{
                        $A.util.addClass(errorSpanId,'help-block-error');
                        $A.util.removeClass(errorDivId,'has-error');
                        $A.util.addClass(errorDivId,'has-success'); 
                    }
                }
        else {
            	//NPD-11506 removed fieldId == 'cpd-numberOfPremises' from below if
                if(fieldId == 'cpd-numberOfEssentialSer' || fieldId == 'cpd-numberOfPremisesRes' || fieldId == 'cpd-numberOfPremisesComm'){
                    document.getElementById(event.currentTarget.dataset.errorspanid).innerText = '';
                    
                }
            	//NPD-11506 added below if
            	if(fieldId != 'cpd-numberOfPremises'){
                	$A.util.removeClass(errorSpanId,'help-block-error');
                	$A.util.removeClass(errorDivId,'has-error'); 
                	$A.util.addClass(errorDivId,'has-success'); 
                }
            	//NPD-11506 commented below 5 lines
            	//if(fieldId == 'cpd-numberOfPremises')
                //{
                  //  component.set('v.numberOfPremisesValue',document.getElementById('cpd-numberOfPremises').value);
                  //  helper.clearLICData(component, event, helper);                    
                //}
            	if(fieldId == 'cpd-estimatedOccupancyDate' || fieldId == 'cpd-councilRefNumber'){
                    helper.copyEstimatedOD(component, event, helper);
                }          
            	// S NPD-11506
            	var addPopulated=component.get("v.addressPopulated");
            	if(fieldId == 'cpd-numberOfPremises' && addPopulated=="true"){
                    document.getElementById(event.currentTarget.dataset.errorspanid).innerText = '';
                                    
                	$A.util.removeClass(errorSpanId,'help-block-error');
                	$A.util.removeClass(errorDivId,'has-error'); 
                	$A.util.addClass(errorDivId,'has-success'); 
            
                    component.set('v.numberOfPremisesValue',document.getElementById('cpd-numberOfPremises').value);
                    helper.clearLICData(component, event, helper);                    
                }            	
            	// E NPD-11506
        }
    }
        // S NPD-11506                
        //var isAddressPopulated =  component.get("v.addressPopulated");        
        //this.validateAddress(component, event, helper);
        // E NPD-11506
        
    },  
    validateEstimatedOccupancyDate : function(component, event, helper){
        helper.validateEstimatedOccupancyDateHelper(component, event, helper);
    },
    
    processDevelopmentDetails : function(component, event, helper) {    	
        window.location.hash = '#cpd-estimatedOccupancyDate-div';
        var div = document.getElementById("cpd-containerId");
        var subDiv = document.querySelectorAll('input, select');
        var myArray = [];
        for(var i = 0; i < subDiv.length; i++) {
            var elem = subDiv[i];
            if(elem.id.indexOf('cpd-') === 0) {
                myArray.push(elem.id);
            }
        }
        var errorCarier = [];
        for(var inputElementId=0; inputElementId<myArray.length; inputElementId++){
            var errorDivId = document.getElementById(myArray[inputElementId]+'-div');
            var errorSpanId = document.getElementById(myArray[inputElementId]+'-error'); 
            var fieldId = myArray[inputElementId];      
            var fieldVal = document.getElementById(fieldId).value;
            var err = helper.validatehelper(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);
            errorCarier.push(err);
        }
        component.set('v.hasError', false);
        for(var i = 0; i < errorCarier.length; i++){
            if(errorCarier[i] == true){
                component.set('v.hasError', true);
            } 
        }
        if(component.get("v.newDevApplicantWrapperInstance").devMapInfo.siteName == null || component.get("v.newDevApplicantWrapperInstance").devMapInfo.siteName == ''){
            component.set("v.errorinMap",true);
            component.set('v.hasError', true);
        }
        if(!component.get("v.hasError")){             
            var wrapInstance = component.get("v.newDevApplicantWrapperInstance");      
            var numOfEssServices = document.getElementById('cpd-numberOfEssentialSer');                      
            var numOfResPremises = document.getElementById('cpd-numberOfPremisesRes');
            var numOfCommPremises = document.getElementById('cpd-numberOfPremisesComm');  
            wrapInstance.devDtlWrapper.numberOfPremises  = document.getElementById('cpd-numberOfPremises').value;
            component.set('v.numberOfPremisesValue',document.getElementById('cpd-numberOfPremises').value);        
            console.log('wrapp'+wrapInstance.devDtlWrapper.whatAreYouBuilding);
            var noOfPremises = component.get('v.numberOfPremisesValue');
        	component.set('v.typeOfWhatAreYouBuildingOptions', helper.getWhatAreYouBuildingOptions(noOfPremises, component.get('v.showNumberOfEssentialServices')));
            
            if (numOfEssServices) {
                wrapInstance.devDtlWrapper.numberOfEssentialServices = document.getElementById('cpd-numberOfEssentialSer').value;
            }            
            if (numOfResPremises) {
                wrapInstance.devDtlWrapper.numberOfResidentialPremises = document.getElementById('cpd-numberOfPremisesRes').value;
            }
            if(numOfCommPremises) {
                wrapInstance.devDtlWrapper.numberOfCommercialPremises = document.getElementById('cpd-numberOfPremisesComm').value;
            }
            wrapInstance.devDtlWrapper.buildingType = document.getElementById('cpd-selectBuildingType').value;
            wrapInstance.devDtlWrapper.developmentWorkType = document.getElementById('cpd-inputSelectDevType').value;
            wrapInstance.devDtlWrapper.cancelledPhoneAndInternet = component.get("v.selectedRadioCancPhAndInt");
            wrapInstance.devDtlWrapper.isPropertyOwner = component.get("v.selectedRadioPropertyOwner");
            wrapInstance.devDtlWrapper.essentialServices = component.get("v.selEssentialServices");
    
            if(document.getElementById('cpd-propertyOwnerFirstName')){
                wrapInstance.devDtlWrapper.propertyOwnerFirstName = document.getElementById('cpd-propertyOwnerFirstName').value;    
            }
            if(document.getElementById('cpd-propertyOwnerLastName')){
                wrapInstance.devDtlWrapper.propertyOwnerLastName = document.getElementById('cpd-propertyOwnerLastName').value;    
            }
            if(document.getElementById('cpd-propertyOwnerPhoneNumber')){
                wrapInstance.devDtlWrapper.propertyOwnerPhone = document.getElementById('cpd-propertyOwnerPhoneNumber').value;    
            }
            if(document.getElementById('cpd-propertyOwnerMobileNumber')){
                wrapInstance.devDtlWrapper.propertyOwnerMobile = document.getElementById('cpd-propertyOwnerMobileNumber').value;    
            }
            if(document.getElementById('cpd-propertyOwnerEmailAddress')){
                wrapInstance.devDtlWrapper.propertyOwnerEmail = document.getElementById('cpd-propertyOwnerEmailAddress').value;    
            }  
            if(document.getElementById('cpd-appRefNumber')){
                wrapInstance.devDtlWrapper.applicationReferenceNumber = document.getElementById('cpd-appRefNumber').value;    
            } 
            
            component.set("v.newDevApplicantWrapperInstance",wrapInstance);                
            var checkValidDevAddress = component.get("c.checkforvalidDevAddress"); // check whether NewDev application is valid
            checkValidDevAddress.setParams({
                "wrapperString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance"))                
            });
            checkValidDevAddress.setCallback(this, function(response) {
                if (response.getState() == 'SUCCESS') {
                    wrapInstance.devDtlWrapper.validDevAddress = response.getReturnValue();
                    component.set("v.newDevApplicantWrapperInstance",wrapInstance);
                    // save record
                    var action1 = component.get("c.saveNewApplicant");
                    action1.setParams({
                                         "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance"))                
                    });
                    console.log('JSON.stringify(component.get("v.newDevApplicantWrapperInstance")) '+JSON.stringify(component.get("v.newDevApplicantWrapperInstance")));
                    action1.setCallback(this, function(response) {
                        if(response.getState() == 'SUCCESS'){
                            component.set("v.savedMapData",true);
                            if(component.get("v.showOtherOwnerExistingPremisesSec") == true) {
                                component.set('v.disableField',true);
                                component.set('v.rejMessage1OtherPropOwnerConfirm',true); 
                                component.set('v.disableKnockDownSecFields',true);
                            }             
                            console.log('response.getReturnValue() '+response.getReturnValue());
                            var wrapInstanceTemp = component.get("v.newDevApplicantWrapperInstance");
                            wrapInstanceTemp.newDevApplicantId = response.getReturnValue();
                            component.set("v.newDevApplicantWrapperInstance",wrapInstanceTemp);                        
                            if (wrapInstanceTemp.devDtlWrapper.validDevAddress) {
                                if(!(wrapInstance.devDtlWrapper.developmentWorkType == 'Knockdown and rebuild' && wrapInstance.devDtlWrapper.numberOfPremises == 1)) { // To not display BuildTypes section if Rejection Message1 (DevType - KnockDown & Rebuild and Premises = 1)
                                   component.set("v.showBuildTypesSection",true);
                                }
                                else {
                                    component.set("v.showBuildTypesSection",false);
                                    var disableForm = component.getEvent("disableForm");
                                    disableForm.setParams({
                                        "disableNewDevForm" : true
                                    });
                                    disableForm.fire();
                                }
                            }
                            else {
                                component.set('v.disableNextButton',true);
                                component.set('v.numberOfpremisesDisabled',true);
                                component.set('v.disableDevDetails',true);
                                component.set("v.showBuildTypesSection",false);
                                var disableForm = component.getEvent("disableForm");
                                disableForm.setParams({
                                    "disableNewDevForm" : true
                                });
                                disableForm.fire();
                                if( wrapInstanceTemp.devMapInfo.FLFResult == 'outside') {                                               
                                    if(component.get("v.showRejectionMessage1Sec") == true){
                                        component.set('v.showRejectionMessage2',false);
                                    }
                                    else {
                                        component.set('v.showRejectionMessage2',true);
                                    }    
                                }
                                else if( wrapInstanceTemp.devMapInfo.FLFResult == 'inside' && wrapInstanceTemp.devDtlWrapper.validDevAddress == false) {                                  
                                    if(component.get("v.showRejectionMessage1Sec") == true){
                                        component.set('v.showRejectionMessage3',false); 
                                    } 
                                    else {
                                        component.set('v.showRejectionMessage3',true);
                                    }   
                                }
                            }
                        }
                        else{
                            console.log(response.getState()+' message is >>>'+response.getError()[0].message);
                        }
                    });
                    $A.enqueueAction(action1);
                }
                else {
                	console.log(response.getState()+' Error message is >>>'+response.getError()[0].message);
                }
            });
            $A.enqueueAction(checkValidDevAddress);
        }
        else{
        	console.log('errorrs');	
        }
    },

    radioBtnChangedPhAndInt : function(component, event, helper){
        if(event.target.value == 'true'){    
           component.set("v.selectedRadioCancPhAndInt",true);
           component.set('v.showApplicantNextButton',false);
           component.set('v.selRadioPhAndIntConn',true);
        }
    },
    
    radioBtnChangedCancPhAndInt : function(component, event, helper) {
         component.set("v.selectedRadioCancPhAndInt",false); 
         component.set('v.showApplicantNextButton',false);
         component.set('v.selRadioPhAndIntConn',true);
    },
    buildTypeSelected : function(component, event, helper){
        //component.set("v.buildTypeError",false);
        //var buildType = component.get("v.BuildType");
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        wrapInstance.devDtlWrapper.numberOfPremises = component.get("v.numberOfPremisesValue");
        var noOfPremises = parseInt(wrapInstance.devDtlWrapper.numberOfPremises);
        //component.set('v.selectedBuildType',event.target.value);
        /*for (var i=0;i<buildType.length;i++)
        {
            if(buildType[i].name == event.target.value)
            {
               $A.util.addClass(document.getElementById(component.get('v.selectedBuildType')+'BuildType'),'LICSeleted'); 
            }
            else
            {
                $A.util.removeClass(document.getElementById(buildType[i].name+'BuildType'),'LICSeleted'); 
            }
        }*/
        if(event.target.value == 'Lead-in Conduit'){
            component.set('v.displayPitAndPipePathwaysSection',false);
            component.set('v.displayLICSection',true);
            component.set('v.displayLICMessage',false); 
            wrapInstance.pitAndPipeDetailsWrapper.isPitAndPipePrivate = false;
            wrapInstance.devDtlWrapper.whatAreYouBuilding ='Lead-in Conduit';
        }
        else if(event.target.value == 'Pit and Pipe'){
            component.set('v.pandpBuildType','pit and pipe');
            wrapInstance.devDtlWrapper.whatAreYouBuilding ='Pit and Pipe';
            if(noOfPremises<3){	   
                component.set('v.displayPitAndPipePathwaysSection',false);
                component.set('v.displayLICSection',false);  
                component.set('v.displayLICMessage',true);                 
            }
            else{   
                component.set('v.displayPitAndPipePathwaysSection',true);
                component.set('v.displayLICSection',false);    
                component.set('v.onlyForPitAndPipe',true);
            }
        }
        else if(event.target.value == 'Pathways'){
            component.set('v.pandpBuildType','pathway');
            wrapInstance.devDtlWrapper.whatAreYouBuilding ='Pathways';
            wrapInstance.pitAndPipeDetailsWrapper.isPitAndPipePrivate = false;
            if(noOfPremises<3){   
                component.set('v.displayPitAndPipePathwaysSection',false);
                component.set('v.displayLICSection',false);  
                component.set('v.displayLICMessage',true);                      
            }
            else{ 
                component.set('v.displayPitAndPipePathwaysSection',true);
                component.set('v.displayLICSection',false); 
                component.set('v.onlyForPitAndPipe',false);
            }
        }
        else{
            component.set('v.displayPitAndPipePathwaysSection',false);
            component.set('v.displayLICSection',false);   
        }
        //Save Record
        var processRec = component.get('c.processDevelopmentDetails');
        $A.enqueueAction(processRec);
    },
    
    handleMapData : function(component, event, helper){
        //alert('handleMapData on DevelopmentDetailsController.js called');
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        component.set("v.userAddressChange",event.getParam("userAddressChange"));
        if(event.getParam("userAddressChange")){
            //alert(event.getParam("streetnumber"));
            wrapInstance.devMapInfo.streetnumber = event.getParam("streetnumber");
            wrapInstance.devMapInfo.streetname = event.getParam("streetname"); 
            wrapInstance.devMapInfo.postCode = event.getParam("postCode");
            wrapInstance.devMapInfo.suburb = event.getParam("suburb");
            wrapInstance.devMapInfo.state = event.getParam("state");
        }
        else{
            var mapInfo = event.getParam("mapInfo");
            wrapInstance.devMapInfo.siteName = mapInfo.siteName;
            wrapInstance.devMapInfo.LocId = mapInfo.LocId;
            wrapInstance.devMapInfo.techType = (mapInfo.techType == 'NULL') ? '' : mapInfo.techType;//mapInfo.techType;
            wrapInstance.devMapInfo.serviceStatus = mapInfo.serviceStatus;
            wrapInstance.devMapInfo.rfsMessage = mapInfo.rfsMessage;
            wrapInstance.devMapInfo.serviceType = mapInfo.serviceType;
            wrapInstance.devMapInfo.serviceCategory = mapInfo.serviceCategory;
            wrapInstance.devMapInfo.FLFResult = mapInfo.FLFResult;
            wrapInstance.devMapInfo.latitude = mapInfo.latitude;
            wrapInstance.devMapInfo.longitude = mapInfo.longitude;
            wrapInstance.devMapInfo.street = mapInfo.street;
            wrapInstance.devMapInfo.country = mapInfo.country;
            wrapInstance.devMapInfo.postCode = mapInfo.postCode;
            wrapInstance.devMapInfo.suburb = mapInfo.suburb;
            wrapInstance.devMapInfo.state = mapInfo.state;
            wrapInstance.devMapInfo.streetnumber = mapInfo.streetnumber;
            wrapInstance.devMapInfo.streetname = mapInfo.streetname;
            wrapInstance.devMapInfo.samId = mapInfo.samId;
            wrapInstance.devMapInfo.fsaId = mapInfo.fsaId;
        }
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
        console.log(' data for MapAddress '+JSON.stringify(wrapInstance.devMapInfo));
    },    
    
    displaySectionChange : function(component, event, helper){ 
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(wrapInstance.yourDtlWrapper.role == 'All Other requestors'){
            component.set("v.numberOfPremisesValue","1");
            component.set("v.numberOfpremisesDisabled",true);
            helper.clearLICData(component, event, helper);
            component.set("v.ownerOccupierMessage","You have selected that you will be occupying this development address. As a resident, you can only submit an application for a single premises. If you have more than one premises, please change your role under ‘Your Details’ section above to edit the number of premises.");
        }
        else{ 
            component.set("v.numberOfPremisesValue",wrapInstance.devDtlWrapper.numberOfPremises);
            component.set("v.numberOfpremisesDisabled",false);
            component.set("v.ownerOccupierMessage","");                
        }
    },
    
    premisesChange : function(component, event, helper){
        console.log('inside premises change method');
        console.log(event.getParam("oldValue"));
        console.log(event.getParam("Value"));
        
        if(event.getParam("oldValue")!=null && event.getParam("oldValue")!=event.getParam("Value")){      
            var processRec = component.get('c.processDevelopmentDetails');
            processRec.setCallback(this, function(response){
                if(response.getState() == 'SUCCESS'){
                    //Clearing build type selection on premise change
                    var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
                    wrapInstance.devDtlWrapper.whatAreYouBuilding="";
                    component.set("v.newDevApplicantWrapperInstance",wrapInstance);
                    //component.set("v.buildTypeError",true);
                    var saveEvent = $A.get("e.c:NewDevBillingSaveEvent");
                    saveEvent.setParams({"wrapperInstance" : component.get("v.newDevApplicantWrapperInstance")})                 
                    console.log('firing billing save event');
                    saveEvent.fire(); 
                }
                else{
                    console.log('failed premises change method');
                }
                console.log('response is : '  + JSON.stringify(response.getReturnValue()));
            });
            $A.enqueueAction(processRec);
          //}	
        }
    }
    
})