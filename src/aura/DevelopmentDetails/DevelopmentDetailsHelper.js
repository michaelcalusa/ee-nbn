({
    validatehelper : function(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal){
        var errorflag = false;
        if(fieldVal == '' && fieldId != 'cpd-appRefNumber' && fieldId != 'cpd-councilRefNumber'){
            if(fieldId == 'cpd-propertyOwnerPhoneNumber' || fieldId == 'cpd-propertyOwnerMobileNumber') {
                if(fieldId=='cpd-propertyOwnerPhoneNumber' && document.getElementById('cpd-propertyOwnerMobileNumber').value == ''){
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;
                }
                else if(fieldId=='cpd-propertyOwnerMobileNumber' && document.getElementById('cpd-propertyOwnerPhoneNumber').value == ''){
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;
                }
                    else if(fieldId=='cpd-propertyOwnerPhoneNumber' && document.getElementById('cpd-propertyOwnerMobileNumber').value != ''){
                        $A.util.removeClass(errorSpanId,'help-block-error');
                        $A.util.removeClass(errorDivId,'has-error');
                        $A.util.addClass(errorDivId,'has-success');
                        errorflag = false;
                    }
                        else if(fieldId=='cpd-propertyOwnerMobileNumber' && document.getElementById('cpd-propertyOwnerPhoneNumber').value != ''){
                            $A.util.removeClass(errorSpanId,'help-block-error');
                            $A.util.removeClass(errorDivId,'has-error');
                            $A.util.addClass(errorDivId,'has-success');
                            errorflag = false;
                        }
            }
            else
            {
                $A.util.addClass(errorSpanId,'help-block-error');               
                $A.util.addClass(errorDivId,'has-error');              
                $A.util.removeClass(errorDivId,'has-success');               
                errorflag = true;
            }
            
        }
        else if((fieldVal.match(/^[0-9]+$/)==null) &&  (fieldId == 'cpd-numberOfPremises' || fieldId == 'cpd-numberOfEssentialSer' || fieldId == 'cpd-numberOfPremisesRes' || fieldId == 'cpd-numberOfPremisesComm')){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
            errorflag = true;
        }
            else if(fieldVal ==0 && (fieldId == 'cpd-numberOfPremises' || fieldId == 'cpd-numberOfEssentialSer')){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorflag = true;
            }      
                else{
                    $A.util.removeClass(errorSpanId,'help-block-error');               
                    $A.util.removeClass(errorDivId,'has-error');              
                    $A.util.addClass(errorDivId,'has-success');
                }           
        return errorflag;
    },
    getWhatAreYouBuildingOptions: function (numberOfPremises, essentialServices) {
        console.log('numberOfPremises=='+numberOfPremises);
        console.log('essentialServices=='+essentialServices);

        numberOfPremises = Number(numberOfPremises);
        if (isNaN(numberOfPremises)) {
            return [
                { label: 'Select...', value: '' }
            ];
        }

        if (numberOfPremises <= 2) {
            if (essentialServices === true) {
                return [
                    { label: 'Select...', value: '' },
                    { label: 'Pathways', value: 'Pathways' }
                ];
            }
            return [
                { label: 'Select...', value: '' },
                { label: 'Lead-in Conduit', value: 'Lead-in Conduit' }
            ];
        }

        if (numberOfPremises >= 9) {
            return [
                { label: 'Select...', value: '' },
                { label: 'Pit and Pipe', value: 'Pit and Pipe' },
                { label: 'Pathways', value: 'Pathways' }
            ];
        }

        if (essentialServices === 'Yes') {
            return [
                { label: 'Select...', value: '' },
                { label: 'Pit and Pipe', value: 'Pit and Pipe' },
                { label: 'Pathways', value: 'Pathways' }
            ];
        }

        return [
            { label: 'Select...', value: '' },
            { label: 'Lead-in Conduit', value: 'Lead-in Conduit' },
            { label: 'Pit and Pipe', value: 'Pit and Pipe' },
            { label: 'Pathways', value: 'Pathways' }
        ];
    },
    clearLICData: function (component, event, helper)
    {
        component.set('v.selectedBuildType','');
        var buildType = component.get("v.BuildType");
        for (var i=0;i<buildType.length;i++)
        {
            
            $A.util.removeClass(document.getElementById(buildType[i].name+'BuildType'),'LICSeleted'); 
            
        }
        component.set("v.displayLICMessage", false);
        component.set("v.displayLICSection",false);
        component.set('v.displayPitAndPipePathwaysSection', false);
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        wrapInstance.devDtlWrapper.licInstalled = false;
        wrapInstance.devDtlWrapper.licType = '';
        wrapInstance.devDtlWrapper.grannyFlat = false;
        wrapInstance.premisesList = [];
        wrapInstance.pitAndPipeDetailsWrapper = {};
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
        
    },
    copyEstimatedOD: function (component, event, helper)
    {
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(document.getElementById('cpd-estimatedOccupancyDate')){
            wrapInstance.devDtlWrapper.estimatedOccupancydt = document.getElementById('cpd-estimatedOccupancyDate').value;    
        }
        if(document.getElementById('cpd-councilRefNumber')){
            wrapInstance.devDtlWrapper.councilRefNo = document.getElementById('cpd-councilRefNumber').value;    
        }
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
        
    },
    validateEstimatedOccupancyDateHelper: function(component, event, helper){
        var fieldVal = component.find('cpd-estimatedOccupancyDate').get('v.value');
        if(fieldVal == undefined || fieldVal == ''){
            $A.util.addClass(document.getElementById('cpd-estimatedOccupancyDate-error'),'help-block-error');
            $A.util.addClass(document.getElementById('cpd-estimatedOccupancyDate-div'),'has-error');
            $A.util.removeClass(document.getElementById('cpd-estimatedOccupancyDate-div'),'has-success');    
            return true;
        }
        else if(fieldVal != undefined && fieldVal != ''){
            var res = fieldVal.split("-");
            if(res.length == 3 && res[0].length == 4 && res[1].length == 2 && res[2].length == 2 && 
              	!isNaN(res[0]) && !isNaN(res[1]) && !isNaN(res[2]) && res[1] <= 12 && res[2] <= 31){
                $A.util.removeClass(document.getElementById('cpd-estimatedOccupancyDate-error'),'help-block-error');
                $A.util.removeClass(document.getElementById('cpd-estimatedOccupancyDate-div'),'has-error');
                $A.util.addClass(document.getElementById('cpd-estimatedOccupancyDate-div'),'has-success');  
                return false;
            }
            else {
                $A.util.addClass(document.getElementById('cpd-estimatedOccupancyDate-error'),'help-block-error');
                $A.util.addClass(document.getElementById('cpd-estimatedOccupancyDate-div'),'has-error');
                $A.util.removeClass(document.getElementById('cpd-estimatedOccupancyDate-div'),'has-success');    
                return true;
            }
        }
    }
})