({
    // CUSTSA-16897 - (Developer: SyedMoosaNazir@nbnco.com.au)
    handleMinimizeMaximize:function(component, event, helper) {
        var section = component.find("dcSection");
        if($A.util.hasClass(section, 'slds-is-open')) {            
            $A.util.addClass(section, 'slds-is-closed');
            $A.util.removeClass(section, 'slds-is-open');
        }
        else {            
            $A.util.removeClass(section, 'slds-is-closed');
            $A.util.addClass(section, 'slds-is-open');
        }
    },
    showTechnicianPopup : function(component, event, helper) {
        helper.getRelevantFaultTypes(component, event, helper);
    },
    cancelTechnicianPopup : function(component, event, helper) {
        component.set("v.showTechnicianPopUp",false);
        component.set("v.confirmationModalVisible",true);
    },
    handleCloseConfirmed:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
        component.set("v.showTechnicianPopUp",false);
        component.set("v.showDispatchTechnicianComponent",false);
        component.set("v.txtDispatchTechnicianValue","");
        component.set("v.disableActionLink",false);
    },
    handleCancelConfirmationModal:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
        component.set("v.showTechnicianPopUp",true);
    },
    dispatch : function(component, event, helper) {
        component.set("v.showTechnicianPopUp",false);
        component.set("v.disableActionLink",true);
        helper.dispatchTech(component, event, helper);
    }
    /*enableDispatch : function(component, event, helper) {
        //component.set("v.dispatchTechnicianDisabled",false);
        component.set("v.disableActionLink",false);	
    },
    handleBlur: function (component, event) {
        var validity = component.find('myFaultDetails').get('v.validity');
        console.log('@@' + validity.valid); //returns true
        component.set("v.ErrorMessage",'Number of Allowed Characters Exceeded');
    }*/
})