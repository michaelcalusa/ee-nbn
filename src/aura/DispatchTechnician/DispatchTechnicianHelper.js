({
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    dispatchTech : function(component, event, helper) {
        console.log("Inside Dispatch Tech");
        var currentIncidentWrapper = component.get("v.attrCurrentIncidentRecord");
        var action = component.get("c.dispatchTechnician");
        action.setParams({
            strRecordId : currentIncidentWrapper.inm.Id,
            strFaultType : component.get("v.selectedFaultType"),
            strFaultDetails : component.get("v.textAreaFaultDetails"),
            strAdditionalInformation : component.get("v.textAreaAdditionalInfo")
        });
        //console.log('@@In dispatch Helper after Action'); 
        action.setCallback(this, function(data) {
            var state = data.getState();
            var result = data.getReturnValue();
            var toastTitle='';
            var toastMessage='';
            var toastType='';
            var toastKey='';
            //check if result is successfull
            if(state == "SUCCESS") {
                console.log('@@State Success'); 
                if(!$A.util.isEmpty(result) && !$A.util.isUndefinedOrNull(result)) {
                    console.log('Response' + result);
                    if(result == 'REQUESTED') {
                        toastTitle = 'success';
                        toastMessage = 'Creating ticket of work for technician. This should only take 10-15 secs.';
                        toastType ='other';
                        toastKey = 'info';       
                        var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
                        fireSpinnerRefresh.setParams({"userAction" :"createWorkRequest", "stopSpinner" : false, "spinnersToStart": "IncidentStatus", "currentRecordId" : currentIncidentWrapper.inm.Id});
                        fireSpinnerRefresh.fire();
                        //helper.getUpdatedStatus(component,event, helper);
                        //helper.subscribeToPlatformEvent(component,helper);
                    }
                    else {
                        toastMessage = $A.get("$Label.c.DispatchTechErrorMessage");
                        toastTitle = 'error';
                        toastType ='error';
                        toastKey = 'info';
                        component.set("v.showTechnicianPopUp",true);
                    }
                    helper.displayToast(component, toastTitle, toastMessage,toastKey,toastType,10000);
                }
                else {
                    toastMessage = $A.get("$Label.c.DispatchTechErrorMessage");
                    toastTitle = 'error';
                    toastType ='error';
                    toastKey = 'info';
                    component.set("v.showTechnicianPopUp",true);
                    helper.displayToast(component, toastTitle, toastMessage,toastKey,toastType,10000);
                }
            } else if(state == "ERROR"){
                console.log('@@State Error'); 
            }
        });
        $A.enqueueAction(action);
    },
    displayToast : function(component, type, message,tkey,ttype,dduration) {
        var toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
            type: type,
            message: message,
            key:tkey,
            type:ttype,
            duration:dduration
        });
        toastEvent.fire();
    },
    compareValue: function(a, b){
        if(a != null && a != 'undefined' && a.toUpperCase() == b.toUpperCase()){
            return true;
        }
        return false; 
    },
    getRelevantFaultTypes : function(component, event, helper) {
        var action = component.get("c.getFaultTypes");
        var currentIncidentWrapper = component.get("v.attrCurrentIncidentRecord");
        action.setParams({
            incidentId: currentIncidentWrapper.inm.Id
        });
        action.setCallback(this, function(data) {
            var result = data.getReturnValue();
            for (var key in result){
                var label = key;
                var value = result[key];
                var allVals = value.split(",");
                component.set('v.FaultTypes', allVals);
                component.set('v.selectedFaultType', key);
                component.set("v.showDispatchTechnicianComponent",true);
                component.set("v.showTechnicianPopUp",true);
                component.set("v.disableActionLink",true);
            }
        });
        $A.enqueueAction(action);
    },
    getUpdatedStatus : function(component,event,helper){
        var currentIncidentManagementRecordWrapper = component.get("v.attrCurrentIncidentRecord");
        var recordId = currentIncidentManagementRecordWrapper.inm.Id;
        var actionUpdated = component.get("c.getUpdatedIncidentRecord");
        actionUpdated.setParams({
            incidentID : recordId          
        });
        actionUpdated.setCallback(this,function(aUpdated){
            var state = aUpdated.getState();
            //check if result is successfull
            if(state == "SUCCESS"){
                var spinnersToFire = '';
                var varattrCurrentIncientRecord = aUpdated.getReturnValue();
                if(varattrCurrentIncientRecord.Awaiting_Current_Incident_Status__c)
                    spinnersToFire = 'IncidentStatus';                
                
                var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
                spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : varattrCurrentIncientRecord, "spinnersToStart" : spinnersToFire, "stopSpinner" : false});
                spinnerHandlerEvent.fire();
                
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": $A.get("$Label.c.dispatchTechWarningMessage")
                });
                toastEvent.fire();                    
            }
        });
        $A.enqueueAction(actionUpdated);
    }
})