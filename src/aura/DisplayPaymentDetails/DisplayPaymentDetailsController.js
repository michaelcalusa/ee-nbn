({
	myAction : function(component, event, helper) {
        
      /*  var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        var isEssentialService =  wrapInstance.disPymtDtlWrapper.essentialServices;
        var buildType = wrapInstance.disPymtDtlWrapper.buildingType;
        var dwellingType = wrapInstance.disPymtDtlWrapper.dwellingType;
        var premiseCount = wrapInstance.disPymtDtlWrapper.numberOfPremises;
        var essentialServicesCount = wrapInstance.disPymtDtlWrapper.numberOfEssentialServices;
        var developmentClass = wrapInstance.disPymtDtlWrapper.customerClass;
        var serviceDeliveryType = wrapInstance.disPymtDtlWrapper.serviceDeliveryType; */
        
        
        var isEssentialService =  true;
        var buildType = 'Multi Dwelling Unit';
        var dwellingType = 'MDU';
        var premiseCount = '100';
        var essentialServicesCount = '50';
        var developmentClass = 'Class 3';
        var serviceDeliveryType = 'SD2';
        
        
		var action = component.get("c.costDetails");
        action.setParams({
            "isEssentialService" : isEssentialService,
            "buildType" : buildType,
            "dwellingType" : dwellingType,
            "premiseCount" : premiseCount,
            "essentialServicesCount" : essentialServicesCount,
            "developmentClass" : developmentClass,
            "serviceDeliveryType" : serviceDeliveryType
        })
        action.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){                
                //alert(JSON.stringify(response.getReturnValue()));
                component.set("v.resultMap",response.getReturnValue());
                var dispPayment = false;
				if(component.get("v.resultMap.DisplayPaymentDetails") == 'Yes')                
              		dispPayment = true;
                component.set("v.displayPaymentSection",dispPayment)
            }
        });
        $A.enqueueAction(action);
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        var regName;
        if(wrapInstance.billingDtlWrapper.RegisteredEntityName === '' || wrapInstance.billingDtlWrapper.RegisteredEntityName === undefined)
        {
            regName = '';
        }
        else
        {
            regName = ' and '+ wrapInstance.billingDtlWrapper.RegisteredEntityName;
        }
        component.set("v.regName",regName);
	},
    
    openBillingSection: function(component, event, helper){  
        var disEvent = component.getEvent("displaySectionChange");
        disEvent.setParams({
            "firedComponentName" : 'CustomerBusinessDetails'
        });
        disEvent.fire();
        console.log('Opened Billing Section from Display Payment Component');
    } 

})