({
	init: function (cmp, event, helper) {		
		var selectedBpi = cmp.get('v.bpi');
		var recordId = cmp.get('v.recordId');
		
		//Load OVC info from service cache for the senario that the incident has NOT been created so record ID is null.
		if(recordId == undefined || recordId == null)
		{
			helper.getProductInfoFromServiceCache(cmp, selectedBpi);		
		}
	},

	handleAllOVCClick: function (cmp, event, helper) {
		var isAllCoSSelected = cmp.get('v.isAllOVCSelected');
		var selectedButtonLabel = event.getSource().get("v.label");		

		switch(selectedButtonLabel) {
			case helper.YES:
				helper.OvcAttrs.map(function(item){
					cmp.set(item, helper.YES);
				});
				var selectedOvcList = cmp.get('v.selectedOVCs');
				var ovcList = cmp.get('v.OVCs');

				var i;
				for (i = 0; i < 8 && i < ovcList.length; i++) { 
					if(selectedOvcList.indexOf(ovcList[i].ovcId) == -1) selectedOvcList.push(ovcList[i].ovcId);
				}

				cmp.set('v.selectedOVCs', selectedOvcList);
				cmp.set('v.isAllOVCSelected', true);
				break;
			case helper.NO:
				helper.OvcAttrs.map(function(item){
					cmp.set(item, '');
				});
				cmp.set('v.isAllOVCSelected', false);
				cmp.set('v.selectedOVCs', []);
				break;
			default:
		}
	},

	handleOVC1Click: function (cmp, event, helper) {
		helper.toggleOVCButton(cmp, event, 'v.OVC1Selected', cmp.get('v.OVCs')[0].ovcId);	
	},

	handleOVC2Click: function (cmp, event, helper) {
		helper.toggleOVCButton(cmp, event, 'v.OVC2Selected', cmp.get('v.OVCs')[1].ovcId);	
	},

	handleOVC3Click: function (cmp, event, helper) {
		helper.toggleOVCButton(cmp, event, 'v.OVC3Selected', cmp.get('v.OVCs')[2].ovcId);	
	},

	handleOVC4Click: function (cmp, event, helper) {
		helper.toggleOVCButton(cmp, event, 'v.OVC4Selected', cmp.get('v.OVCs')[3].ovcId);	
	},

	handleOVC5Click: function (cmp, event, helper) {
		helper.toggleOVCButton(cmp, event, 'v.OVC5Selected', cmp.get('v.OVCs')[4].ovcId);	
	},

	handleOVC6Click: function (cmp, event, helper) {
		helper.toggleOVCButton(cmp, event, 'v.OVC6Selected', cmp.get('v.OVCs')[5].ovcId);
	},

	handleOVC7Click: function (cmp, event, helper) {
		helper.toggleOVCButton(cmp, event, 'v.OVC7Selected', cmp.get('v.OVCs')[6].ovcId);
	},

	handleOVC8Click: function (cmp, event, helper) {
		helper.toggleOVCButton(cmp, event, 'v.OVC8Selected', cmp.get('v.OVCs')[7].ovcId);	
	},

	onIncRecordUpdated: function (cmp, event, helper) {		
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {} else if (changeType === "LOADED") {
			//Load OVC info from service cache for the senario that the incident has been created so record ID is not null.
			var ovcs = cmp.get('v.incidentFields.Impacted_OVC__c');
			if(ovcs != null && ovcs != undefined)
			{
				cmp.set('v.selectedOVCs', cmp.get('v.incidentFields.Impacted_OVC__c').split(','));
			}
			var selectedBpi = cmp.get('v.incidentFields.PRI_ID__c');
			helper.getProductInfoFromServiceCache(cmp, selectedBpi);
		}			
    },

	handleIncLdsError: function (cmp, event, helper) {
		helper.handleLdsError(cmp, 'incident', event.getParam("value"));
	},
})