({
    YES: 'Yes',
    NO: 'No',
	NEW_MODE: 'New',
	EDIT_MODE: 'Edit',
	OvcAttrs: ['v.OVC1Selected', 'v.OVC2Selected', 'v.OVC3Selected', 'v.OVC4Selected',
				'v.OVC5Selected', 'v.OVC6Selected', 'v.OVC7Selected', 'v.OVC8Selected'],

	toggleOVCButton: function (cmp, event, ovcAttr, ovcID) {
        var selectedOVC = cmp.get(ovcAttr);
        var isAllOVCSelected = cmp.get('v.isAllOVCSelected');
        var selectedButtonLabel = event.getSource().get("v.label");
		var selectedOvcList = cmp.get('v.selectedOVCs');

        switch (selectedButtonLabel) {
            case this.YES:
                if (selectedOVC != this.YES) cmp.set(ovcAttr, this.YES);
                if (isAllOVCSelected == false) cmp.set('v.isAllOVCSelected', null);
				if (selectedOvcList.indexOf(ovcID) == -1) selectedOvcList.push(ovcID);
                break;
            case this.NO:
                if (selectedOVC != this.NO) cmp.set(ovcAttr, this.NO);
                if (isAllOVCSelected == true) cmp.set('v.isAllOVCSelected', null);
				var index = selectedOvcList.indexOf(ovcID);
				if (index > -1) selectedOvcList.splice(index, 1);;
                break;
            default:
        }		
		cmp.set('v.selectedOVCs', selectedOvcList);
    },

	getProductInfoFromServiceCache: function (cmp, selectedBpi) {		
		var promise1 = this.apex(cmp, 'v.serviceCacheProductInfo', 'c.searchServiceCache', { searchString: selectedBpi }, false);
		
		Promise.all([promise1])
		.then(function(result) {			
			var helper = result[0].sourceHelper;
			helper.loadOVCInfo(cmp);
		})
		.catch(function(result) {
			var cmp = result.sourceCmp;
			if(cmp){
				var errorMessage = $A.get("$Label.c.DF_Application_Error");
				if(result.sourceAction == 'c.searchServiceCache') errorMessage = $A.get("$Label.c.EE_AS_SVC_NOT_AVAILABLE");  
				var helper = result.sourceHelper;				
				helper.setErrorMsg(cmp, "ERROR", errorMessage , "BANNER");	
			}else
			{
				console.log(result);
			}
		});		
	},

	loadOVCInfo: function (cmp) {

		var ovcList = cmp.get('v.serviceCacheProductInfo').ovcs;		
		
		if(ovcList != null && ovcList != undefined)
		{
			var selectedOvcList = cmp.get('v.selectedOVCs');
			this.OvcAttrs.map(function(item){ cmp.set(item, ''); });

			if (ovcList.length > 0 && selectedOvcList.indexOf(ovcList[0].ovcId) > -1)
			{
				cmp.set('v.OVC1Selected', this.YES);
				ovcList[0].uiSelected = true;
			}

			if (ovcList.length > 1 && selectedOvcList.indexOf(ovcList[1].ovcId) > -1)
			{
				cmp.set('v.OVC2Selected', this.YES);
				ovcList[1].uiSelected = true;
			}

			if (ovcList.length > 2 && selectedOvcList.indexOf(ovcList[2].ovcId) > -1)
			{
				cmp.set('v.OVC3Selected', this.YES);
				ovcList[2].uiSelected = true;
			}

			if (ovcList.length > 3 && selectedOvcList.indexOf(ovcList[3].ovcId) > -1)
			{
				cmp.set('v.OVC4Selected', this.YES);
				ovcList[3].uiSelected = true;
			}

			if (ovcList.length > 4 && selectedOvcList.indexOf(ovcList[4].ovcId) > -1)
			{
				cmp.set('v.OVC5Selected', this.YES);
				ovcList[4].uiSelected = true;
			}

			if (ovcList.length > 5 && selectedOvcList.indexOf(ovcList[5].ovcId) > -1)
			{
				cmp.set('v.OVC6Selected', this.YES);
				ovcList[5].uiSelected = true;
			}

			if (ovcList.length > 6 && selectedOvcList.indexOf(ovcList[6].ovcId) > -1)
			{
				cmp.set('v.OVC7Selected', this.YES);
				ovcList[6].uiSelected = true;
			}

			if (ovcList.length > 7 && selectedOvcList.indexOf(ovcList[7].ovcId) > -1)
			{
				cmp.set('v.OVC8Selected', this.YES);
				ovcList[7].uiSelected = true;
			}
		}

		cmp.set('v.OVCs', ovcList);
	},
	
})