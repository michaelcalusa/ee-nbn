/**
 * Created by michael.calusa on 26/02/2019.
 */
({
    init: function(component, event, helper) {
        var orderId;

        if(component.get('v.recordId') != undefined && component.get('v.recordId') != null && component.get('v.recordId') != '')
            orderId = component.get('v.recordId');
        //var promise1 = helper.apex(component, 'v.serviceDetail', 'c.getServiceDetails', {  orderId: orderId }, false);
        var promise2 = helper.apex(component, 'v.location', 'c.getLocationDetails', {  orderId: orderId }, false);
        var promise3 = helper.apex(component, 'v.businessContactList', 'c.getBizContacts', {  orderId: orderId }, false);
        var promise4 = helper.apex(component, 'v.siteContactList', 'c.getSiteContacts', {  orderId: orderId }, false);
        var promise5 = helper.apex(component, 'v.ntdDetail', 'c.getNTDDetails', {  orderId: orderId }, false);
        var promise6 = helper.apex(component, 'v.ovcList', 'c.getOVCDetails', {  orderId: orderId }, false);
        var promise7 = helper.apex(component, 'v.UNI', 'c.getUNIDetails', {  orderId: orderId }, false);

        Promise.all([promise2,promise3,promise4,promise5,promise6,promise7])
        .then(function (result) {
            helper.toggleSection(component,event,'businessCont');
            helper.toggleSection(component,event,'siteCont');
            helper.toggleSection(component,event,'BTD');
            helper.toggleSection(component,event,'service');
            helper.toggleSection(component,event,'OVC');
            helper.toggleSection(component,event,'UNI');
        })
        .catch(function (result) {
            var component = result.sourceCmp;
            var helper = result.sourceHelper;
            helper.setErrorMsg(component, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
        });

    },

    toggleService : function(component, event, helper) {
       helper.toggleSection(component,event,'service');
    },

    toggleBizCont : function(component, event, helper) {
       helper.toggleSection(component,event,'businessCont');
    },

    toggleSiteCont : function(component, event, helper) {
       helper.toggleSection(component,event,'siteCont');
    },
    
    toggleBTD : function(component, event, helper) {
       helper.toggleSection(component,event,'BTD');
    },

    toggleOVC : function(component, event, helper) {
       helper.toggleSection(component,event,'OVC');
    },

    toggleUNI : function(component, event, helper) {
       helper.toggleSection(component,event,'UNI');
    }
})