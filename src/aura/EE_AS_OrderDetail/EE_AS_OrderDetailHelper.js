/**
 * Created by michael.calusa on 27/02/2019.
 */
({
    toggleSection : function(component,event,secId) {
    	  var acc = component.find(secId);
            	for(var cmp in acc) {
            	$A.util.toggleClass(acc[cmp], 'slds-show');
            	$A.util.toggleClass(acc[cmp], 'slds-hide');
           }
    	},
})