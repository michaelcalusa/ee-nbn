({
    init: function (cmp, event, helper) {

    },

    back: function (cmp, event, helper) {
        cmp.set('v.displayedDetailItem', '');
    },

    onIncRecordUpdated: function (cmp, event, helper) {
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {} else if (changeType === "LOADED") {			

			var promise1 = helper.apex(cmp, 'v.impactedServices', 'c.getImpactedServicesByIncidentId', {
				incNumber: cmp.get('v.incidentFields.Name'),
			}, false);		       

			Promise.all([promise1])
			.then(function (result) {
				var data = cmp.get('v.impactedServices');
				if(data.length > 0)
				{
					var impactedBPI = [];
					data.map(function(item){
						impactedBPI.push(item.BPI_Id__c);
					});

					var impactedOVC = [];
					data.map(function(item){
						impactedOVC.push(item.OVC_Id__c);
					});
					
					var helper = result[0].sourceHelper;					

					cmp.set('v.impactedBPI', helper.populateDictinctArray(cmp, impactedBPI).join(","));
					cmp.set('v.impactedOVC', helper.populateDictinctArray(cmp, impactedOVC).join(","));
				}					
			})
            .catch(function (result) {
                var cmp = result.sourceCmp;
				if(cmp){
					var helper = result.sourceHelper;				
					helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");		
				}else
				{
					console.log(result);
				}
            });
		} else if (changeType === "REMOVED") {} else if (changeType === "CHANGED") {

        }
    },

    handleIncLdsError: function (cmp, event, helper) {
        helper.handleLdsError(cmp, 'account', event.getParam("value"));
    },    

	handleIncSimpleLdsError: function (cmp, event, helper) {
        helper.handleLdsError(cmp, 'incident simple', event.getParam("value"));
    },
})