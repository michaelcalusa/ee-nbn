({
	populateDictinctArray: function (cmp, sourceArray){		
		var dictinctArray = [];
		sourceArray.map(function(item){			
			if(item != undefined && dictinctArray.indexOf(item) == -1 ) {
				dictinctArray.push(item);
			};
		});

		return dictinctArray;
	},
})