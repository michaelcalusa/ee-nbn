({
	handleTnDLdsError: function (cmp, event, helper) {
		helper.handleLdsError(cmp, 'TnD', event.getParam("value"));
	},

	onTestRecordUpdated: function (cmp, event, helper) {		
		var changeType = event.getParams().changeType;		
        if (changeType === "ERROR") {} else if (changeType === "LOADED") {		
        
        } else if (changeType === "REMOVED") {} else if (changeType === "CHANGED") {                    
        }		
	},

	init: function (cmp, event, helper) {		
		
	},
})