({   
	onSelectedTestTypeChange: function (cmp, event, helper) {
		// Reset error messages and selected CoS		
		cmp.set('v.multipleCosValidationError', false);
		cmp.set("v.nptDateValidationError", false);
		helper.clearErrors(cmp);
		helper.resetCoSButton(cmp);
		cmp.set('v.selectedOVC', null);

		// Set default values 
		var multipleCosAllowed = 'Multiple';
		var singleCosAllowed = 'Single';
		var noCosAllowed = 'None';
		var isPacketSizeVisible = false;
		var isDurationVisible = false;
		var isNPTinputVisible = false;
		var isMTUSizeVisible = false;
		var isPacketSizePerSecondVisible = false;
		var cosSelectionType;		
		var selectedTestStartDate = null;
		var selectedTestStartTime = null;
		var selectedTestEndDate = null;
		var selectedTestEndTime = null;	
		var selectedNPTDuration = null;
		var testType = cmp.get('v.selectedTestType');
		
		switch(testType) {
			case cmp.get('v.testTypeLoopback'):
				cosSelectionType = multipleCosAllowed;
				isPacketSizeVisible = true;
				isDurationVisible = false;
				isNPTinputVisible = false;
				isMTUSizeVisible = false;
				isPacketSizePerSecondVisible = false;
				break;
			case cmp.get('v.testTypeLinkTrace'):
				cosSelectionType = multipleCosAllowed;
				isPacketSizeVisible = true;
				isDurationVisible = false;
				isNPTinputVisible = false;
				isMTUSizeVisible = false;
				isPacketSizePerSecondVisible = false;
				break;
			case cmp.get('v.testTypeOctetCount'):
				cosSelectionType = noCosAllowed;
				isPacketSizeVisible = false;
				isDurationVisible = true;
				isNPTinputVisible = false;
				isMTUSizeVisible = false;
				break;
			case cmp.get('v.testTypeNPTStatistical'):
				cosSelectionType = singleCosAllowed;
				isPacketSizeVisible = false;
				isDurationVisible = false;
				isNPTinputVisible = true;
				isMTUSizeVisible = false;
				isPacketSizePerSecondVisible = false;
				selectedTestStartTime = '00:00:00.000';
				selectedTestEndTime = '00:00:00.000';
				break;
			case cmp.get('v.testTypeNPTProactive'):
				cosSelectionType = singleCosAllowed;
				isPacketSizeVisible = false;
				isPacketSizePerSecondVisible = true;
				isDurationVisible = false;
				isNPTinputVisible = true;
				isMTUSizeVisible = true;
				isPacketSizePerSecondVisible = true;
				selectedNPTDuration = '1';		
				var localCurrentDate = new Date();
				var localYear = localCurrentDate.getFullYear();
				var localMonth = localCurrentDate.getMonth() + 1;
				var localDate = localCurrentDate.getDate();
				var localHours = localCurrentDate.getHours();
				var localMins = localCurrentDate.getMinutes();
				
				selectedTestStartDate = localYear + (localMonth > 9 ? '-' : '-0') + localMonth + (localDate > 9 ? '-' : '-0') + localDate;				
				selectedTestStartTime = (localHours > 9 ? '' : '0') + localHours + ':' + (localMins > 9 ? '' : '0') + localMins  + ':00.000';

				let timeOptions = cmp.get('v.timeOptions');
				let label = selectedTestStartTime.substring(0,5), value = selectedTestStartTime;
				let newOption={label,value};
				var containsShortTime = timeOptions.find( function( ele ) {
					return ele.label === label;
				} );
			
				if(!containsShortTime){
				timeOptions.push(newOption);
				cmp.set('v.timeOptions',timeOptions);
				helper.sortPickListOptionsByLabel(cmp, helper, 'v.timeOptions');
				}
				break;
			default:
				cosSelectionType = noCosAllowed;
				isPacketSizeVisible = false;
				isDurationVisible = false;
				isNPTinputVisible = false;
				isMTUSizeVisible = false;
				isPacketSizePerSecondVisible = false;				
		}		

		cmp.set('v.cosSelectionType', cosSelectionType);
		cmp.set('v.isPacketSizeVisible', isPacketSizeVisible);
		cmp.set('v.isDurationVisible', isDurationVisible);
		cmp.set('v.isNPTinputVisible', isNPTinputVisible);
		cmp.set('v.isMTUSizeVisible', isMTUSizeVisible);
		cmp.set('v.selectedTestStartDate', selectedTestStartDate);
		cmp.set('v.selectedTestStartTime', selectedTestStartTime);		
		cmp.set('v.selectedTestEndDate', selectedTestEndDate);
		cmp.set('v.selectedTestEndTime', selectedTestEndTime);
		cmp.set('v.selectedNPTDuration', selectedNPTDuration);
		cmp.set('v.isPacketSizePerSecondVisible', isPacketSizePerSecondVisible);		 
	},

	onTNPTDurationChange: function (cmp, event, helper) {
		// Calculate test end date and time by adding duriation to test start date and time 			
		var isNptProactive = cmp.get('v.selectedTestType') == cmp.get('v.testTypeNPTProactive');
		var selectedNPTDuration = cmp.get('v.selectedNPTDuration');
		if(isNptProactive && selectedNPTDuration != null && selectedNPTDuration != undefined)
		{	
			// 15 minutes interval
			var selectedStartDateTimeVal = cmp.get('v.selectedStartDateTimeVal');
			var endDateTime = new Date((new Date(selectedStartDateTimeVal)).getTime() + selectedNPTDuration * 15 * 60000);
			var localYear = endDateTime.getFullYear();
			var localMonth = endDateTime.getMonth() + 1;
			var localDate = endDateTime.getDate();
			var localHours = endDateTime.getHours();
			var localMins = endDateTime.getMinutes();
			
			var selectedTestEndDate = localYear + (localMonth > 9 ? '-' : '-0') + localMonth + (localDate > 9 ? '-' : '-0') + localDate;			
			var selectedTestEndTime = (localHours > 9 ? '' : '0') + localHours + ':' + (localMins > 9 ? '' : '0') + localMins  + ':00.000';
			
			cmp.set('v.selectedTestEndDate', selectedTestEndDate);
			cmp.set('v.selectedTestEndTime', selectedTestEndTime);
		}		
	},

    onTestStartDateEndDateChange: function (cmp, event, helper) {
		var selectedTestStartDate = cmp.get('v.selectedTestStartDate');
		var selectedTestStartTime = cmp.get('v.selectedTestStartTime');
		var selectedTestEndDate = cmp.get('v.selectedTestEndDate');
		var selectedTestEndTime = cmp.get('v.selectedTestEndTime');
		var isNptProactive = cmp.get('v.selectedTestType') == cmp.get('v.testTypeNPTProactive');

		if(isNptProactive)
		{
			if(selectedTestStartTime != null  && selectedTestStartTime != undefined)
				cmp.set('v.selectedTestStartTimeShortFormat', selectedTestStartTime.substring(0,5));

			if(selectedTestEndTime != null  && selectedTestEndTime != undefined)
				cmp.set('v.selectedTestEndTimeShortFormat', selectedTestEndTime.substring(0,5));
		}

		var selectedStartDateTimeVal;
		var selectedEndDateTimeVal;

		if (!$A.util.isEmpty(selectedTestStartDate) && !$A.util.isUndefined(selectedTestStartTime) && 
			!$A.util.isEmpty(selectedTestStartDate) && !$A.util.isUndefined(selectedTestStartTime) &&
			selectedTestStartDate != null && selectedTestStartTime != null) 
		{
			selectedStartDateTimeVal = selectedTestStartDate + 'T' + selectedTestStartTime;
		}

		if (!$A.util.isEmpty(selectedTestEndDate) && !$A.util.isUndefined(selectedTestEndTime) && 
			!$A.util.isEmpty(selectedTestEndDate) && !$A.util.isUndefined(selectedTestEndTime) &&
			selectedTestEndDate != null && selectedTestEndTime != null)
		{
			selectedEndDateTimeVal = selectedTestEndDate + 'T' + selectedTestEndTime;
		}

		cmp.set('v.selectedStartDateTimeVal', selectedStartDateTimeVal);
		cmp.set('v.selectedEndDateTimeVal', selectedEndDateTimeVal);	
		var selectedNPTDuration = cmp.get('v.selectedNPTDuration');
		if(isNptProactive && selectedNPTDuration != null && selectedNPTDuration != undefined)
		{	
			// 15 minutes interval
			var selectedStartDateTimeVal = cmp.get('v.selectedStartDateTimeVal');
			var endDateTime = new Date((new Date(selectedStartDateTimeVal)).getTime() + selectedNPTDuration * 15 * 60000);
			var localYear = endDateTime.getFullYear();
			var localMonth = endDateTime.getMonth() + 1;
			var localDate = endDateTime.getDate();
			var localHours = endDateTime.getHours();
			var localMins = endDateTime.getMinutes();
			
			var selectedTestEndDate = localYear + (localMonth > 9 ? '-' : '-0') + localMonth + (localDate > 9 ? '-' : '-0') + localDate;			
			var selectedTestEndTime = (localHours > 9 ? '' : '0') + localHours + ':' + (localMins > 9 ? '' : '0') + localMins  + ':00.000';
			
			cmp.set('v.selectedTestEndDate', selectedTestEndDate);
			cmp.set('v.selectedTestEndTime', selectedTestEndTime);
		}	
	},

    cancelTest: function (cmp, event, helper) {
		cmp.set('v.isCancelTestReminderVisiable', true);
	},

	handleAllCoSClick: function (cmp, event, helper) {
		var isAllCoSSelected = cmp.get('v.isAllCoSSelected');
		var selectedButtonLabel = event.getSource().get("v.label");

		switch(selectedButtonLabel) {
			case helper.YES:
				cmp.set('v.selectedCosHigh', helper.YES);
				cmp.set('v.selectedCosMedium', helper.YES);
				cmp.set('v.selectedCosLow', helper.YES);
				cmp.set('v.isAllCoSSelected', true);
				break;
			case helper.NO:
				cmp.set('v.selectedCosHigh', '');
				cmp.set('v.selectedCosMedium', '');
				cmp.set('v.selectedCosLow', '');
				cmp.set('v.isAllCoSSelected', false);
				break;
			default:				
		}
	}, 

	handleCoSHighClick: function (cmp, event, helper) {
		helper.toggleCoSButton(cmp, event, 'v.selectedCosHigh');	

		//NPT specifc cos logic
		var isNPTinputVisible = cmp.get('v.isNPTinputVisible');
		if (isNPTinputVisible == true) cmp.set('v.selectedCosMedium', helper.NO);	
	},

	handleCoSLowClick: function (cmp, event, helper) {
		helper.toggleCoSButton(cmp, event, 'v.selectedCosLow');		
	},

	handleCoSMediumClick: function (cmp, event, helper) {
		helper.toggleCoSButton(cmp, event, 'v.selectedCosMedium');		

		//NPT specifc cos logic
		var isNPTinputVisible = cmp.get('v.isNPTinputVisible');
		if (isNPTinputVisible == true) cmp.set('v.selectedCosHigh', helper.NO);	
	},

	onTestRecordUpdated: function (cmp, event, helper) {
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {			
		} else if (changeType === "LOADED") {												
			
        } else if (changeType === "REMOVED") {} else if (changeType === "CHANGED") {            
            
        }
    },

	onOrderRecordUpdated: function (cmp, event, helper) {
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {
			helper.setErrorMsg(cmp, changeType, $A.get("$Label.c.DF_Application_Error"), "BANNER");
			console.log(cmp.get('v.dfOrderError'));
		} else if (changeType === "LOADED") {
			cmp.set('v.initDone', false);
			var promise1 = helper.apex(cmp, 'v.testTypeOptions', 'c.getASPickListCustomSettings', {
				picklistName: cmp.get('v.isInternalUser') ? 'TestTypeInternal' : 'TestTypeExternal'
			}, false);
			
			var selectedBpi = cmp.get('v.dfOrderFields.BPI_Id__c')
			cmp.set('v.bpi', selectedBpi);

			var promise2 = helper.apex(cmp, 'v.serviceCacheProductInfo', 'c.searchServiceCache', { searchString: selectedBpi }, false);

			Promise.all([promise1, promise2])
            .then(function (result) {
				cmp.set('v.initDone', true);
				helper.populateOVCDropDown(cmp);
            })
            .catch(function (result) {				
                var cmp = result.sourceCmp;
				if(cmp){
					cmp.set('v.initDone', true);					
					var errorMessage = $A.get("$Label.c.DF_Application_Error");
					
					if(result.sourceAction == 'c.searchServiceCache') {
						errorMessage = $A.get("$Label.c.EE_AS_SVC_NOT_AVAILABLE");  
						cmp.set('v.serviceCacheDown', true);
					}

					var helper = result.sourceHelper;				
					helper.setErrorMsg(cmp, "ERROR", errorMessage , "BANNER");		
				}else
				{
					console.log(result);
				}
            });

        } else if (changeType === "REMOVED") {} else if (changeType === "CHANGED") {            
            
        }
    },

    init: function (cmp, event, helper) {		
		var selectedBpi = cmp.get('v.bpi');

		var promise0 = helper.apex(cmp, 'v.openEvents', 'c.searchOpenIncidents', { bpi: selectedBpi }, false);		
		var promise1 = helper.apex(cmp, 'v.timeOptions', 'c.getASPickListCustomSettings', { picklistName: 'TndTime'  }, false);
		var promise2 = helper.apex(cmp, 'v.durationOptions', 'c.getASPickListCustomSettings', { picklistName: 'TndDuration'  }, false);
		
		Promise.all([promise0, promise1, promise2])
            .then(function (result) {
				helper = result[0].sourceHelper;
				helper.sortPickListOptionsByLabel(cmp, helper, 'v.timeOptions');
				helper.sortPickListOptionsByLabel(cmp, helper, 'v.durationOptions');

				// Populate open events for reminder
				var openEvents = cmp.get('v.openEvents');
				var ni = openEvents['Network Incident'];
				var si = openEvents['Service Incident'];
				var sa = openEvents['Service Alert'];
				var crq = openEvents['Change Request'];

				if(!$A.util.isEmpty(ni) || !$A.util.isUndefined(ni)) {
					var values = [];
					ni.map(function(item){
						var incNo = item.Incident_Number__c;
						if(!$A.util.isEmpty(incNo) || !$A.util.isUndefined(incNo)) values.push(incNo);
					});
					cmp.set('v.openNetworkIncidents', values.join(', '));
					cmp.set('v.hasOpenEvents', true);
				}

				if(!$A.util.isEmpty(si) || !$A.util.isUndefined(si)) {
					var values = [];
					si.map(function(item){
						var incNo = item.Incident_Number__c;
						if(!$A.util.isEmpty(incNo) || !$A.util.isUndefined(incNo)) values.push(incNo);
					});
					cmp.set('v.openServiceIncidents', values.join(', '));
					cmp.set('v.hasOpenEvents', true);
				}

				if(!$A.util.isEmpty(crq) || !$A.util.isUndefined(crq)) {
					var values = [];
					crq.map(function(item){
						var incNo = item.Incident_Number__c;
						if(!$A.util.isEmpty(incNo) || !$A.util.isUndefined(incNo)) values.push(incNo);
					});
					cmp.set('v.openChangeRequests', values.join(', '));
					cmp.set('v.hasOpenEvents', true);
				}

				if(!$A.util.isEmpty(sa) || !$A.util.isUndefined(sa)) {
					var values = [];
					sa.map(function(item){
						var incNo = item.Incident_Number__c;
						if(!$A.util.isEmpty(incNo) || !$A.util.isUndefined(incNo)) values.push(incNo);
					});
					cmp.set('v.openServiceAlerts', values.join(', '));
					cmp.set('v.hasOpenEvents', true);
				}
            })
            .catch(function (result) {
                var cmp = result.sourceCmp;
				if(cmp){
					var helper = result.sourceHelper;				
					helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");		
				}else
				{
					console.log(result);
				}
            });

		// First try to get order Id from URL
		var orderId;
        var sPageURL = decodeURIComponent(window.location.search.substring(1));	
        var sURLVariables = sPageURL.split('&');
        
        for(var count = 0; count < sURLVariables.length; count++) {
            var orderStr = sURLVariables[count].split('=');
            if(orderStr[0] == 'orderId'){
                orderId = orderStr[1];
                cmp.set('v.orderId', orderId);
            }
        }
		
		// Then try to get order Id from force:HasRecordId interface
		var recordId = cmp.get('v.recordId');
		
		if(!$A.util.isEmpty(recordId) || !$A.util.isUndefined(recordId)) {
			cmp.set('v.orderId', recordId); 		
		}

		cmp.find("forceRecordOrdCmp").reloadRecord();	
		cmp.set('v.selectedPacketSizeRange', cmp.get('v.defaultPacketSize'));
		cmp.set('v.selectedPacketSizePerSecond', cmp.get('v.defaultPacketSizePerSecond'));		
		cmp.set('v.selectedDuration', cmp.get('v.defaultDuration'));

		var promiseList = [];

		var promise1 = helper.apex(cmp, 'v.customSettings', 'c.getASCustomSettings', {
            componentName: cmp.get('v.componentName')
        }, false);

		var promise2 = helper.apex(cmp, 'v.testTypeOptions', 'c.getASPickListCustomSettings', {
            picklistName: cmp.get('v.isInternalUser') ? 'TestTypeInternal' : 'TestTypeExternal'
        }, false);

		var promise3 = helper.apex(cmp, 'v.pendingTestFullList', 'c.getCurrentTestResultsForUser', { }, false);
		var promise4 = helper.apex(cmp, 'v.previousTestFullList', 'c.getCompletedTestResultsForUser', { }, false);
		
		var promise5 = helper.apex(cmp, 'v.serviceCacheProductInfo', 'c.searchServiceCache', { searchString: selectedBpi }, false);
		
		promiseList = [promise1, promise2, promise3, promise4];

		if(selectedBpi != undefined && selectedBpi != null) promiseList.push(promise5);

        Promise.all(promiseList)
            .then(function (result) {
				var recordId = cmp.get('v.recordId');
				if(recordId == null || recordId == undefined){	
					helper.populateOVCDropDown(cmp);
				}
                cmp.set('v.initDone', true);
            })
            .catch(function (result) {
                var cmp = result.sourceCmp;
				if(cmp){
					var errorMessage = $A.get("$Label.c.DF_Application_Error");
					if(result.sourceAction == 'c.searchServiceCache') errorMessage = $A.get("$Label.c.EE_AS_SVC_NOT_AVAILABLE");
					var helper = result.sourceHelper;
					helper.setErrorMsg(cmp, "ERROR", errorMessage , "BANNER");					
					cmp.set('v.initDone', true);
				}else
				{
					console.log(result);
				}
            });
    },
	
	refreshTestTypeDefaultValues: function (cmp, event, helper) {
		cmp.set('v.selectedPacketSizeRange', cmp.get('v.defaultPacketSize'));
		cmp.set('v.selectedDuration', cmp.get('v.defaultDuration'));
	},

	refreshPendingTestDatatable: function (cmp, event, helper) {
		cmp.set('v.pendingTestPageNumber', 1);
		cmp.set('v.isPendingTestFeched' , true);
		helper.resetSelectedTestId(cmp);
        helper.updateDatatableAndFooterByParameters(cmp, 'v.pendingTestFullList', 'v.pendingTestCurrentList', 'v.pendingTestPageNumber', 'v.selectedCountPreviousTest', 'v.pendingTestMaxPage');
	},

	refreshPreviousTestDatatable: function (cmp, event, helper) {
		cmp.set('v.previousTestPageNumber', 1);
		cmp.set('v.isPreviousTestFeched' , true);
		helper.resetSelectedTestId(cmp);
        helper.updateDatatableAndFooterByParameters(cmp, 'v.previousTestFullList', 'v.previousTestCurrentList', 'v.previousTestPageNumber', 'v.selectedCountPendingTest', 'v.previousTestMaxPage');
	},

	navigatePendingTestDatatablePage: function (cmp, event, helper) {
		helper.resetSelectedTestId(cmp);
		helper.updateDatatableAndFooterByParameters(cmp, 'v.pendingTestFullList', 'v.pendingTestCurrentList', 'v.pendingTestPageNumber', 'v.selectedCountPreviousTest', 'v.pendingTestMaxPage');
	},

	navigatePreviousTestDatatablePage: function (cmp, event, helper) {
		helper.resetSelectedTestId(cmp);
		helper.updateDatatableAndFooterByParameters(cmp, 'v.previousTestFullList', 'v.previousTestCurrentList', 'v.previousTestPageNumber', 'v.selectedCountPendingTest', 'v.previousTestMaxPage');
	},

	runTest: function (cmp, event, helper) {
		
		var valid = helper.validateRunTest(cmp, helper);
		if(!valid) return;

		var classOfService = [];
		var selected = 'Yes';
		if(cmp.get('v.selectedCosHigh') == selected) classOfService.push('HIGH');
		if(cmp.get('v.selectedCosMedium') == selected) classOfService.push('MEDIUM');
		if(cmp.get('v.selectedCosLow') == selected) classOfService.push('LOW');

		var localSelectedStartDateTimeVal = new Date(cmp.get('v.selectedStartDateTimeVal'));
		var localSelectedEndDateTimeVal = new Date(cmp.get('v.selectedEndDateTimeVal'));

		var utcSelectedStartDateTimeVal; 
		var utcSelectedEndDateTimeVal; 

		if(localSelectedStartDateTimeVal.toString() != 'Invalid Date') utcSelectedStartDateTimeVal = (new Date(localSelectedStartDateTimeVal)).toISOString();
		if(localSelectedEndDateTimeVal.toString() != 'Invalid Date') utcSelectedEndDateTimeVal = (new Date(localSelectedEndDateTimeVal)).toISOString();
		
		var isNPTProactive = cmp.get('v.selectedTestType') == cmp.get('v.testTypeNPTProactive');

		var promise1 = helper.apex(cmp, 'v.runTestResponse', 'c.runTndTest', {            
			reqType: cmp.get('v.selectedTestType'), 
			ovcId: cmp.get('v.selectedOVC'),
			classOfService: classOfService, 
			packetSize: isNPTProactive ? cmp.get('v.selectedPacketSizePerSecond') : cmp.get('v.selectedPacketSizeRange'),			
			orderID: cmp.get('v.orderId'),
			duration: cmp.get('v.selectedDuration'),
			mtuSize: cmp.get('v.selectedMtuSize'),
			durationStartDate: utcSelectedStartDateTimeVal,
			durationEndDate: utcSelectedEndDateTimeVal,
			productID: cmp.get('v.bpi')
        }, false);		

        Promise.all([promise1])
            .then(function (result) {   
				var status;
				var message;				
				var runTestResponse = cmp.get('v.runTestResponse');
				if(runTestResponse != null && runTestResponse != undefined && runTestResponse.length > 0)
				{					
					var newTestId = runTestResponse[0].recordId;
					if(newTestId != null && newTestId != undefined)
					{	
						message = cmp.get('v.customSettings.Run_Test_Save_OK');
						cmp.set('v.newTestId', newTestId)
						
						if(cmp.get('v.isInternalUser'))
						{						
							message = cmp.get('v.customSettings.Run_Test_Save_OK_Interal_User');
							var messageChildCmp = cmp.find('msgComp');
							messageChildCmp.set('v.sObjectRecordId', newTestId);
							messageChildCmp.set('v.sObjectRecordName', runTestResponse[0].recordName);
							messageChildCmp.set('v.showInternalObjectDetailLink', true);							
						}
						else helper.refreshData(cmp, event, helper);

						helper.showTopBannerSuccessMessage(cmp, message);
					}else{						
						message = $A.get("$Label.c.DF_Application_Error");
						helper.showTopBannerErrorMessage(cmp, message);
					}
				}
            })
            .catch(function (result) {			
               var cmp = result.sourceCmp;
				if(cmp){
					var helper = result.sourceHelper;				
					helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");		
				}else
				{
					console.log(result);
				}
            });		
    },

	clickTestIdLink: function (cmp, event, helper) {		
		var testId = event.target.getAttribute("data-Id");	
		cmp.set('v.testDetailsId', testId);
		cmp.set('v.displayedDetailItem', 'EE_AS_Test_Details');
    },	
	
	continueToRaiseIncident: function (cmp, event, helper) {		
		cmp.set('v.isContinue', true);
		helper.handleRaiseServiceIncident(cmp, event, helper);
	},

	cancelReminder: function (cmp, event, helper) {		
		cmp.set('v.isReminderVisiable', false);
	}, 

	raiseServiceIncident: function (cmp, event, helper) {
		helper.handleRaiseServiceIncident(cmp, event, helper);
    },

	refreshTestResultsAfterCancelTest: function (cmp, event, helper) {		
		if(cmp.get('v.cancelTestDone')){			
			helper.resetSelectedTestId(cmp);
			cmp.set('v.cancelTestDone', false);	
			helper.refreshData(cmp, event, helper);
		}
    },

	refreshTestResults: function (cmp, event, helper) {		
		helper.refreshData(cmp, event, helper);
    },
		
	onCoSMultipleModeChange: function (cmp, event, helper) {
		helper.validateMultipleCos(cmp, helper);
	},
	
	onSelectedOvcChange:function (cmp, event, helper) {
		helper.clearErrors(cmp);

		var isHighCosPurchased = false;	
		var isMediumCosPurchased = false;
		var isLowCosPurchased = false;
		var selectedOVC = cmp.get('v.selectedOVC');
		var ovcInfo = [];
		var serviceCacheProductInfo = cmp.get('v.serviceCacheProductInfo');
		var isNPT = cmp.get('v.selectedTestType')
		if (serviceCacheProductInfo.ovcs)
		{
			ovcInfo = serviceCacheProductInfo.ovcs.filter(function(item){ return item.ovcId == selectedOVC });			
		}

		if(ovcInfo.length > 0){
			if(ovcInfo[0].cosLowBandwidth) isLowCosPurchased = true;
			if(ovcInfo[0].cosMediumBandwidth) isMediumCosPurchased = true;
			if(ovcInfo[0].cosHighBandwidth) isHighCosPurchased = true;
		}

    cmp.set('v.isCosLowInNPT', false);
		if((isNPT==cmp.get('v.testTypeNPTStatistical'))
		    && (!isHighCosPurchased && !isMediumCosPurchased && isLowCosPurchased)) {
		    cmp.set('v.isCosLowInNPT', true);
        cmp.set("v.nptDateValidationError", false);
        cmp.set("v.nptDateValidationErrorMsg", null);
    }

		cmp.set('v.isHighCosPurchased', isHighCosPurchased);
		cmp.set('v.isMediumCosPurchased', isMediumCosPurchased);
		cmp.set('v.isLowCosPurchased', isLowCosPurchased);

		helper.resetCoSButton(cmp);
	},

	onSelectedTestIdChange: function (cmp, event, helper) {				
		var selectedTestId = event.getSource().get('v.value');
		cmp.set('v.selectedTestId', selectedTestId);
		var selectedRow = cmp.get("v.pendingTestFullList").filter(function(item){ return item.Id == selectedTestId })[0];
		//cancel and raise incident button logic changed for CPST-5494 
		cmp.set("v.isSelectedTestCancellable", selectedRow.Is_Cancelable__c == 'True' || (selectedRow.Status__c ==  cmp.get('v.testStatusPending') && (selectedRow.TimedOut__c == true ? true : false)));
		cmp.set("v.isSelectedTestComplete", selectedRow.Status__c ==  cmp.get('v.testStatusComplete') || cmp.get('v.testStatusCancelled') || selectedRow.TimedOut__c == true ? true : false);
    },	

	handleTnDLdsError: function (cmp, event, helper) {
		helper.handleLdsError(cmp, 'TnD', event.getParam("value"));
	},

	handleDfOrderLdsError: function (cmp, event, helper) {
		helper.handleLdsError(cmp, 'DF Order', event.getParam("value"));
	},

	updatePendingTestColumnSorting: function (cmp, event, helper) {

        var sortedByColumn = cmp.get('v.sortedByColumn');
        var sortedDirection = cmp.get('v.sortedDirection');
        var clickedColumnName = event.target.getAttribute("data-column");
        var asc = 'ascending';
        var desc = 'decending';

        if (clickedColumnName == sortedByColumn) {
            if (sortedDirection == asc)
                cmp.set('v.sortedDirection', desc);
            else
                cmp.set('v.sortedDirection', asc);
        } else {
            cmp.set('v.sortedByColumn', clickedColumnName);
            cmp.set('v.sortedDirection', desc);
        }
    },
})