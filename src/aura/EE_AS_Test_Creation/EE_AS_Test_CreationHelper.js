({
    YES: 'Yes',
    NO: 'No',

	populateOVCDropDown: function (cmp, event, helper) {
		// Populate OVC drop down list
		var ovcOptions = [];
		var ovcList = cmp.get('v.serviceCacheProductInfo').ovcs;		

		if(ovcList != null && ovcList != undefined)
		{
			ovcList.map(function(item){				
				var serviceCacheOvcId = item.ovcId;
				if( serviceCacheOvcId != '')
				ovcOptions.push({ label: serviceCacheOvcId, value: serviceCacheOvcId });				
			});

			cmp.set('v.ovcOptions', ovcOptions);
		}	

		if (ovcOptions.length == 0) {
			this.setErrorMsg(cmp, "ERROR", "OVC ID not available", "BANNER");
		}
	},	

    handleRaiseServiceIncident: function (cmp, event, helper) {
        var hasOpenEvents = cmp.get('v.hasOpenEvents');
        var isContinue = cmp.get('v.isContinue');

        if (hasOpenEvents && !isContinue) {
            cmp.set('v.isReminderVisiable', true);
            return;
        }

        cmp.set('v.defaultTabId', '2');
        cmp.set('v.currentTabId', '2');
        cmp.set('v.confirmedSelectedTestId', cmp.get('v.selectedTestId'));
        cmp.set('v.isReminderVisiable', false);
        cmp.set('v.isContinue', false);
    },

	resetCoSButton: function (cmp) {
		cmp.set('v.isAllCoSSelected', null);
        cmp.set('v.selectedCosHigh', null);
		cmp.set('v.selectedCosMedium', null);
		cmp.set('v.selectedCosLow', null);
		cmp.set('v.multipleCosValidationError', false);
    },

    toggleCoSButton: function (cmp, event, cosAttr) {
        var selectedCos = cmp.get(cosAttr);
        var isAllCoSSelected = cmp.get('v.isAllCoSSelected');
        var selectedButtonLabel = event.getSource().get("v.label");

        switch (selectedButtonLabel) {
            case this.YES:
                if (selectedCos != this.YES) cmp.set(cosAttr, this.YES);
                if (isAllCoSSelected == false) cmp.set('v.isAllCoSSelected', null);
                break;
            case this.NO:
                if (selectedCos != this.NO) cmp.set(cosAttr, this.NO);
                if (isAllCoSSelected == true) cmp.set('v.isAllCoSSelected', null);
                break;
            default:
        }
    },

    resetSelectedTestId: function (cmp) {
        cmp.set('v.selectedTestId', '');
        cmp.set('v.isSelectedTestCancellable', false);
        cmp.set('v.isSelectedTestComplete', false);
    },

    refreshData: function (cmp, event, helper) {
        var promise1 = helper.apex(cmp, 'v.pendingTestFullList', 'c.getCurrentTestResultsForUser', {}, false);
        var promise2 = helper.apex(cmp, 'v.previousTestFullList', 'c.getCompletedTestResultsForUser', {}, false);

        Promise.all([promise1, promise2])
            .then(function (result) {
                helper.resetSelectedTestId(cmp);
            })
            .catch(function (result) {
                var cmp = result.sourceCmp;
				if(cmp){
					var helper = result.sourceHelper;				
					helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");		
				}else
				{
					console.log(result);
				}
            });
    },

    validateRunTest: function (cmp, helper) {
        var testType = cmp.get('v.selectedTestType');
        var cosSelectionType = cmp.get('v.cosSelectionType');

        var multipleCosAllowed = 'Multiple';
        var singleCosAllowed = 'Single';
        var noCosAllowed = 'None';

        var isNPTinputVisible = cmp.get('v.isNPTinputVisible');
        var isValidCoSNPT = cmp.get('v.isCosLowInNPT');

        var valid = true;

        if (!this.validateSharedFields(cmp, helper)) valid = false;

        if(isValidCoSNPT) {
           valid = false;
        } else {
            switch (cosSelectionType) {
                case multipleCosAllowed:
                    if (!this.validateMultipleCos(cmp, helper)) valid = false;
                    break;
                case singleCosAllowed:
                    if (!this.validateMultipleCos(cmp, helper)) valid = false;
                    break;
                default:
            }
        }

        //NPT Validation
        if (isNPTinputVisible == true) {
            if (!this.validateNPTDateFields(cmp, helper)) valid = false;
        }

        return valid;
    },

    validateSharedFields: function (cmp, helper) {
        var valid = true;
        var fieldGroupName = 'sharedFields';
        var ctrls = cmp.find(fieldGroupName);
        var isPacketSizeVisible = cmp.get('v.isPacketSizeVisible');
        var isPacketSizePerSecondVisible = cmp.get('v.isPacketSizePerSecondVisible');
        var isMTUSizeVisible = cmp.get('v.isMTUSizeVisible');

        if (ctrls.length == undefined) {
            // Handle when only one componet is found -- non array case
            this.validateSelectCmpEmpty(ctrls);
            ctrls.showHelpMessageIfInvalid();
            valid = ctrls.get('v.validity').valid;
        } else {
            var ctrl = ctrls.filter(this.getComponentByName("OVC"))[0];
            this.validateSelectCmpEmpty(ctrl);
        }

        if (isPacketSizeVisible) {
            var ctrl = ctrls.filter(this.getComponentByName("packetSize"))[0];
            this.validateInputCmpEmpty(ctrl);
            valid = this.isAllFieldsValid(cmp, fieldGroupName);
        }

        if (isMTUSizeVisible) {
            var ctrl = ctrls.filter(this.getComponentByName("mtuSize"))[0];
            this.validateInputCmpEmpty(ctrl);
            valid = this.isAllFieldsValid(cmp, fieldGroupName);
        }

        if (isPacketSizePerSecondVisible) {
            var ctrl = ctrls.filter(this.getComponentByName("packetSizePerSecond"))[0];
            this.validateInputCmpEmpty(ctrl);
            valid = this.isAllFieldsValid(cmp, fieldGroupName);
        }

        return valid;
    },

    validateMultipleCos: function (cmp, helper) {
        var valid = true;
		var selectedOVC = cmp.get('v.selectedOVC');

		if(selectedOVC != undefined && selectedOVC != "")
		{
			var cosHigh = cmp.get('v.selectedCosHigh');
			var cosMedium = cmp.get('v.selectedCosMedium');
			var cosLow = cmp.get('v.selectedCosLow');

			if (cosHigh != this.YES && cosMedium != this.YES && cosLow != this.YES) {
				valid = false;
				cmp.set('v.multipleCosValidationError', true);
			} else {
				cmp.set('v.multipleCosValidationError', false);
			}
		}

        return valid;
    },

    validateNPTDateFields: function (cmp, helper) {

        var valid = true;
        cmp.set("v.nptDateValidationError", false);

        var selectedStartDateTimeVal = cmp.get('v.selectedStartDateTimeVal');
        var selectedEndDateTimeVal = cmp.get('v.selectedEndDateTimeVal');

        var testType = cmp.get('v.selectedTestType');
		
        var today = new Date();
        var weekStartDate = new Date(new Date().getTime() - 86400000 * 7);
		var weekStartDateFuture = new Date(new Date().getTime() + 86400000 * 7);
		var maxEndDate = new Date(new Date().getTime() + 86400000);
        var durationStartDate = new Date(selectedStartDateTimeVal); 
        var durationEndDate = new Date(selectedEndDateTimeVal); 
        var diff = Math.abs(durationStartDate - durationEndDate);
        var diffMinutes = Math.floor((diff / 1000) / 60);

        if ($A.util.isEmpty(selectedStartDateTimeVal) || $A.util.isUndefined(selectedStartDateTimeVal) || $A.util.isEmpty(selectedEndDateTimeVal) || $A.util.isUndefined(selectedEndDateTimeVal)) {
            cmp.set("v.nptDateValidationError", true);
            cmp.set("v.nptDateValidationErrorMsg", cmp.get('v.customSettings.Test_NPT_Duration_Required'));
            valid = false;
        } else if (!this.isValidDateTime(selectedStartDateTimeVal) || !this.isValidDateTime(selectedEndDateTimeVal)) {
            cmp.set("v.nptDateValidationError", true);
            cmp.set("v.nptDateValidationErrorMsg", cmp.get('v.customSettings.Test_NPT_Duration_Invalid'));
            valid = false;
        } else {
            if (durationStartDate >= durationEndDate) {
                cmp.set("v.nptDateValidationError", true);
                cmp.set("v.nptDateValidationErrorMsg", cmp.get('v.customSettings.Test_NPT_Duration_Start_Lessthan_End'));
                valid = false;
            } else if (diffMinutes > 1440) {
                cmp.set("v.nptDateValidationError", true);
                cmp.set("v.nptDateValidationErrorMsg", cmp.get('v.customSettings.Test_NPT_Duration_Max_Hours'));
                valid = false;
            }            

            if (testType == cmp.get('v.testTypeNPTStatistical')) {
                if (durationStartDate > today || (durationStartDate < weekStartDate)) {

                    cmp.set("v.nptDateValidationError", true);
                    cmp.set("v.nptDateValidationErrorMsg", cmp.get('v.customSettings.Test_NPT_Duration_Start_Invalid'));
                    valid = false;
                } else if (durationEndDate > maxEndDate || (durationEndDate < weekStartDate)) {

                    cmp.set("v.nptDateValidationError", true);
                    cmp.set("v.nptDateValidationErrorMsg", cmp.get('v.customSettings.Test_NPT_Duration_End_Invalid'));
                    valid = false;
                }
            } else if(testType == cmp.get('v.testTypeNPTProactive')){
					//Allowing past five minutes as start time and Next 7 days in future
			       if (durationStartDate < (today.getTime() - 300000) || (durationStartDate > weekStartDateFuture)) {
                    cmp.set("v.nptDateValidationError", true);
                    cmp.set("v.nptDateValidationErrorMsg", cmp.get('v.customSettings.NPT_Proactive_Duration_Start_Invalid'));
                    valid = false;
                }
			}
        }

        return valid;
    }
})