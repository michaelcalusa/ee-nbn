({	
	handleTestLdsError: function (cmp, event, helper) {
		helper.handleLdsError(cmp, 'Test', event.getParam("value"));
	},

	handleSimpleTestLdsError: function (cmp, event, helper) {
		helper.handleLdsError(cmp, 'Simple Test', event.getParam("value"));
	},
 
	init: function (cmp, event, helper) {			
		setInterval(
			$A.getCallback(function() {
				helper.refreshLds(cmp, 'forceRecordTestFieldsCmp', 'v.fieldsToQuery')
			}), 5000); // Check LDS updates every 5 seconds
	},

	back: function (cmp, event, helper) {
		var sourceDisplayedDetailItem = cmp.get('v.sourceDisplayedDetailItem');
		cmp.set('v.displayedDetailItem', sourceDisplayedDetailItem);
	},
		
	onSimpleTestRecordUpdated: function (cmp, event, helper) {
		var changeType = event.getParams().changeType;		
        if (changeType === "ERROR") {} else if (changeType === "LOADED") {			
            helper.toggleLinkTraceTestResult(cmp);
        } else if (changeType === "REMOVED") {} else if (changeType === "CHANGED") {            
            helper.toggleLinkTraceTestResult(cmp);
        }		
		
		cmp.set('v.initDone', true);	
	},

	onTestRecordUpdated: function (cmp, event, helper) {		
		
		var fieldsToQuery = [];
		var recordTypeInfo = cmp.get('v.testRecord.recordTypeInfo.name');

		switch(recordTypeInfo) {
			case cmp.get('v.linktraceTestType'):
				fieldsToQuery = [					
					'CoS_High_AAS_to_EAS__c', 'CoS_High_AAS_to_ECS__c', 'CoS_High_BNTD_to_AAS__c', 'CoS_High_EAS_to_EFS__c', 'CoS_High_Packets_Received__c', 
					'CoS_High_Packets_Sent__c', 'CoS_High_Result__c', 'CoS_High_Traces_Received__c',
					'CoS_High_Traces_Sent__c', 'CoS_Low_AAS_to_EAS__c',	'CoS_Low_AAS_to_ECS__c', 'CoS_Low_BNTD_to_AAS__c', 'CoS_Low_EAS_to_EFS__c', 'CoS_Low_Packets_Received__c',
					'CoS_Low_Packets_Sent__c', 'CoS_Low_Result__c', 'CoS_Low_Traces_Received__c', 'CoS_Low_Traces_Sent__c',
					'CoS_Medium_AAS_to_EAS__c', 'CoS_Medium_AAS_to_ECS__c', 'CoS_Medium_BNTD_to_AAS__c', 'CoS_Medium_EAS_to_EFS__c', 'CoS_Medium_Packets_Received__c',
					'CoS_Medium_Packets_Sent__c', 'CoS_Medium_Result__c', 'CoS_Medium_Traces_Received__c', 'CoS_Medium_Traces_Sent__c',
					'CoS_High_AAS_to_ECS_2__c', 'CoS_High_ECS2_to_ECS1__c',
					'CoS_Low_AAS_to_ECS_2__c', 'CoS_Low_ECS2_to_ECS1__c',
					'CoS_Medium_AAS_to_ECS_2__c', 'CoS_Medium_ECS2_to_ECS1__c',
					'CoS_High_EAS_to_ECS__c', 'CoS_Low_EAS_to_ECS__c', 'CoS_Medium_EAS_to_ECS__c',
					'BNTD_LOGICAL_NAME__c', 'BNTD_PHYSICAL_NAME__c', 
					'AAS_LOGICAL_NAME__c', 'AAS_PHYSICAL_NAME__c',
					'LOGICAL_NAME_ENNI__c', 'PHYSICAL_NAME_ENNI__c',
					'LOGICAL_NAME_INNI__c',	'PHYSICAL_NAME_INNI__c'];				
				break;	

			case cmp.get('v.btdStatusTestType'):
				fieldsToQuery = [					
					'Access_Seeker_Id__c', 
					'Access_Seeker_Name__c',
					'Result__c', 
					'SVLANENNI__c',
					'BTD_Active_Software_Version__c',	
					'Service_Profile__c',
					'BTD_Operational_State__c',
					'BTD_Last_Change_Date__c',
					'S_VLAN_INNI__c'];
				break;

			case cmp.get('v.unieStatusTestType'):
				fieldsToQuery = [					
					'Result__c', 
					'Ethernet_Port_Operational_State__c',
					'Ethernet_Port_Link_State__c', 
					'Ethernet_Port_Last_Change_Date__c'];
				break;
			default:
		}

		fieldsToQuery.push('Reference_Id__c');
		if(!cmp.get('v.isInternalUser')) fieldsToQuery.push('Error_External__c');

		cmp.set('v.fieldsToQuery', fieldsToQuery);
		cmp.set('v.simpleRecordId', cmp.get('v.recordId'));
		cmp.find("forceRecordTestFieldsCmp").reloadRecord();
	},

	toggleLinkTraceResult: function (cmp, event, helper) {
		if(cmp.get('v.isResultExpanded')) 
		{	
			cmp.set('v.isResultExpanded', false);
		}
		else
		{
			cmp.set('v.isResultExpanded', true);
		}

	}
})