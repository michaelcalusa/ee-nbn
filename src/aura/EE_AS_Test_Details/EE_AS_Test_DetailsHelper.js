({
	toggleLinkTraceTestResult: function(cmp) {		

		if(cmp.get('v.testRecord.recordTypeInfo.name') == cmp.get('v.linktraceTestType')) {

			// Detect if this is a small POI and the mode of it
			var coS_High_AAS_to_ECS__c = cmp.get("v.simpleTestFields.CoS_High_AAS_to_ECS__c");
			var coS_High_AAS_to_ECS_2__c = cmp.get("v.simpleTestFields.CoS_High_AAS_to_ECS_2__c");
			var coS_Low_AAS_to_ECS__c = cmp.get("v.simpleTestFields.CoS_Low_AAS_to_ECS__c");
			var coS_Low_AAS_to_ECS_2__c = cmp.get("v.simpleTestFields.CoS_Low_AAS_to_ECS_2__c");
			var coS_Medium_AAS_to_ECS__c = cmp.get("v.simpleTestFields.CoS_Medium_AAS_to_ECS__c");
			var coS_Medium_AAS_to_ECS_2__c = cmp.get("v.simpleTestFields.CoS_Medium_AAS_to_ECS_2__c");

			var coS_High_EAS_to_ECS__c = cmp.get("v.simpleTestFields.CoS_High_EAS_to_ECS__c");
			var coS_Low_EAS_to_ECS__c = cmp.get("v.simpleTestFields.CoS_Low_EAS_to_ECS__c");
			var coS_Medium_EAS_to_ECS__c = cmp.get("v.simpleTestFields.CoS_Medium_EAS_to_ECS__c");					

			var isLinkTraceSmallPoiModeA = false;
			var isLinkTraceSmallPoiModeB = false;
			
			// When AAS_ECS or AAS_ECS 2 has value, it is a small POI mode A 
			if(
				(!$A.util.isEmpty(coS_High_AAS_to_ECS__c) && !$A.util.isUndefined(coS_High_AAS_to_ECS__c)) ||
				(!$A.util.isEmpty(coS_High_AAS_to_ECS_2__c) && !$A.util.isUndefined(coS_High_AAS_to_ECS_2__c)) ||
				(!$A.util.isEmpty(coS_Low_AAS_to_ECS__c) && !$A.util.isUndefined(coS_Low_AAS_to_ECS__c)) ||
				(!$A.util.isEmpty(coS_Low_AAS_to_ECS_2__c) && !$A.util.isUndefined(coS_Low_AAS_to_ECS_2__c)) ||
				(!$A.util.isEmpty(coS_Medium_AAS_to_ECS__c) && !$A.util.isUndefined(coS_Medium_AAS_to_ECS__c)) ||
				(!$A.util.isEmpty(coS_Medium_AAS_to_ECS_2__c) && !$A.util.isUndefined(coS_Medium_AAS_to_ECS_2__c))
			){
				isLinkTraceSmallPoiModeA = true;
			}	
			
			// When EAS_ECS has value, it is a small POI mode B 
			if(
				(!$A.util.isEmpty(coS_Low_EAS_to_ECS__c) && !$A.util.isUndefined(coS_Low_EAS_to_ECS__c)) ||
				(!$A.util.isEmpty(coS_Medium_EAS_to_ECS__c) && !$A.util.isUndefined(coS_Medium_EAS_to_ECS__c)) ||
				(!$A.util.isEmpty(coS_High_EAS_to_ECS__c) && !$A.util.isUndefined(coS_High_EAS_to_ECS__c)) 
			){
				isLinkTraceSmallPoiModeB = true;
			}

			cmp.set('v.isLinkTraceSmallPoiModeA', isLinkTraceSmallPoiModeA);
			cmp.set('v.isLinkTraceSmallPoiModeB', isLinkTraceSmallPoiModeB);

			// Toggle CoS results according to the result status
			var coS_Medium_Result__c = cmp.get('v.simpleTestFields.CoS_Medium_Result__c');
			var coS_Low_Result__c = cmp.get('v.simpleTestFields.CoS_Low_Result__c');
			var coS_High_Result__c = cmp.get('v.simpleTestFields.CoS_High_Result__c');

			var resultNotAvailable = cmp.get('v.resultNotAvailable').toLowerCase();
			var resultCancelled = cmp.get('v.resultCancelled').toLowerCase();  
			var resultPending = cmp.get('v.resultPending').toLowerCase();  
			
			if(!$A.util.isEmpty(coS_High_Result__c) && !$A.util.isUndefined(coS_High_Result__c)) {
				coS_High_Result__c = coS_High_Result__c.toLowerCase();
				if(coS_High_Result__c != resultCancelled && coS_High_Result__c != resultNotAvailable && coS_High_Result__c != resultPending) {
					cmp.set('v.isLinkTraceCosHighResultVisiable', true);
				}
			}

			if(!$A.util.isEmpty(coS_Medium_Result__c) && !$A.util.isUndefined(coS_Medium_Result__c)) {
				coS_Medium_Result__c = coS_Medium_Result__c.toLowerCase();
				if(coS_Medium_Result__c != resultCancelled && coS_Medium_Result__c != resultNotAvailable && coS_Medium_Result__c != resultPending) {
					cmp.set('v.isLinkTraceCosMediumResultVisiable', true);
				}
			}

			if(!$A.util.isEmpty(coS_Low_Result__c) && !$A.util.isUndefined(coS_Low_Result__c)) {
				coS_Low_Result__c = coS_Low_Result__c.toLowerCase();
				if(coS_Low_Result__c != resultCancelled && coS_Low_Result__c != resultNotAvailable && coS_Low_Result__c != resultPending) {
					cmp.set('v.isLinkTraceCosLowResultVisiable', true);
				}
			}
		}
	},	
})