({
	cancelReminder: function (cmp, event, helper) {
		cmp.set('v.isReminderVisiable', false);
        helper.clearErrors(cmp);
		$A.get('e.force:refreshView').fire();
	},

	fetchTest: function (cmp, event, helper) {
		var promise1 = helper.apex(cmp, 'v.result', 'c.fetchResultForTndProactive', {  id: cmp.get('v.recordId') }, false);
		
        Promise.all([promise1])
            .then(function (result) {             				
				cmp.set('v.isReminderVisiable', true);

			    var helper = result[0].sourceHelper;
                if(result[0].data.length > 0)
                {   
                    helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
                }
            })
            .catch(function (result) {				
                var cmp = result.sourceCmp;
				var helper = result.sourceHelper;				
                helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
            });
	},

	handleTnDLdsError: function (cmp, event, helper) {
		helper.handleLdsError(cmp, 'TnD', event.getParam("value"));
	},

	onTestRecordUpdated: function (cmp, event, helper) {		
		var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {} else if (changeType === "LOADED") {			
        
        } else if (changeType === "REMOVED") {} else if (changeType === "CHANGED") {                    
        }		
	},

	init: function (cmp, event, helper) {		
		var promise1 = helper.apex(cmp, 'v.customSettings', 'c.getASCustomSettings', {
            componentName: cmp.get('v.componentName')
        }, false);
		
        Promise.all([promise1])
            .then(function (result) {                
                
            })
            .catch(function (result) {
                var cmp = result.sourceCmp;
				var helper = result.sourceHelper;				
                helper.setErrorMsg(cmp, $A.get("$Label.c.DF_Application_Error"));
            });
	},
})