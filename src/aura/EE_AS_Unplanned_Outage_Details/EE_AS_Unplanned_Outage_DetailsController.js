({
    init: function (cmp, event, helper) {		
    },

    back: function (cmp, event, helper) {
        cmp.set('v.displayedDetailItem', '');
    },

    onIncRecordUpdated: function (cmp, event, helper) {
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {} else if (changeType === "LOADED") {
			
			var promise1 = helper.apex(cmp, 'v.impactedServices', 'c.getImpactedServicesByIncidentId', {
				incNumber: cmp.get('v.incidentFields.Name'),
			}, false);		       

			var promise2 = helper.apex(cmp, 'v.accessSeekerName', 'c.getCurrentUserAccountName', { }, false);

			var promise3 = helper.apex(cmp, 'v.customSettings', 'c.getASCustomSettings', {
				componentName: cmp.get('v.componentName')
			}, false);

			var promise4 = helper.apex(cmp, 'v.allNotesList', 'c.getNetworkIncidentNotes', { incId: cmp.get('v.incId') }, false);

			Promise.all([promise1, promise2, promise3, promise4])
			.then(function (result) {
							
				var data = cmp.get('v.impactedServices');
				if(data.length > 0) 
				{
					cmp.set('v.fileName', cmp.get('v.incidentFields.Name') + ' ' + data.length + ' Impacted Services.csv');
				}
			})
            .catch(function (result) {
                var error = result.error;
                var cmp = result.sourceCmp;
                $A.reportError("error message", error);
                cmp.set("v.responseStatus", result.ERROR_RESPONSE_STATUS);
                cmp.set("v.responseMessage", error);
                cmp.set("v.messageType", result.ERROR_RESPONSE_TYPE);
            });
		} else if (changeType === "REMOVED") {} else if (changeType === "CHANGED") {

        }
    },

	onIncSimpleRecordUpdated: function (cmp, event, helper) {		
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {} else if (changeType === "LOADED") {			
		} else if (changeType === "REMOVED") {} else if (changeType === "CHANGED") {
        }
    },

	handleIncSimpleLdsError: function (cmp, event, helper) {
        helper.handleLdsError(cmp, 'incident simple', event.getParam("value"));
    },

    handleIncLdsError: function (cmp, event, helper) {
        helper.handleLdsError(cmp, 'incident', event.getParam("value"));
    },
		
    downloadCSV: function (cmp, event, helper) {		
		var data = cmp.get('v.impactedServices') ; 		
		var keys = ['Access Seeker ID', 'Product ID', 'NNI', 'OVC'];
				
		var objectArray = data.map(function(item) {
			return { 
				'Access Seeker ID': $A.util.isEmpty(item.Access_Seeker_ID__c) || $A.util.isUndefined(item.Access_Seeker_ID__c) ? '' : item.Access_Seeker_ID__c, 
				'Product ID': $A.util.isEmpty(item.BPI_Id__c) || $A.util.isUndefined(item.BPI_Id__c) ? '' : item.BPI_Id__c,
				'NNI': $A.util.isEmpty(item.NNI_Id__c) || $A.util.isUndefined(item.NNI_Id__c) ? '' : item.NNI_Id__c,
				'OVC': $A.util.isEmpty(item.OVC_Id__c) || $A.util.isUndefined(item.OVC_Id__c) ? '' : item.OVC_Id__c 
			};
		});

		// call the helper function which "return" the CSV data as a String
		
		var csv = helper.convertArrayOfObjectsToCSV(cmp, objectArray, keys);
		if (csv == null) {
			return;
		}

		// ####--code for create a temp. <a> html tag [link tag] for download the CSV file--####     
		var hiddenElement = document.createElement('a');
		hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
		hiddenElement.target = '_self'; // 
		hiddenElement.download = cmp.get('v.fileName'); // CSV file Name* you can change it.[only name not .csv] 
	
		var container = cmp.find("csvLink").getElement();
		container.appendChild(hiddenElement); // Required for FireFox browser          
		hiddenElement.click(); // using click() js function to download csv file 
    },
})