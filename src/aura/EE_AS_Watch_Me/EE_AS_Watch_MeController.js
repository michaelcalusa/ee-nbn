({
	init: function (cmp, event, helper) {
		var promise1 = helper.apex(cmp, 'v.isWatching', 'c.isWatching', {
            incidentId: cmp.get('v.recordId')
        }, false);
			
        Promise.all([promise1])
            .then(function (result) {				
				if(cmp.get('v.recordId') != undefined)				
					cmp.set('v.initDone', true);
            })
            .catch(function (result) {
                var cmp = result.sourceCmp;
				if(cmp){					
					var helper = result.sourceHelper;				
					helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
					cmp.set('v.initDone', true);
				}else
				{
					console.log(result);
				}
            });
	},

	addWatchMe: function (cmp, event, helper) {		
		debugger;		
		var promise1 = helper.apex(cmp, 'v.result', 'c.watchMe', {
            incidentId: cmp.get('v.recordId')
        }, false);			

        Promise.all([promise1])
            .then(function (result) {
				cmp.set('v.isWatching', true);
            })
            .catch(function (result) {
                var cmp = result.sourceCmp;
				if(cmp){					
					var helper = result.sourceHelper;				
					helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
					cmp.set('v.initDone', true);
				}else
				{
					console.log(result);
				}
            });
	},
	
	onIncRecordUpdated: function (cmp, event, helper) {		
        var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {} else if (changeType === "LOADED") {
			//var ovcs = cmp.get('v.incidentFields.Impacted_OVC__c');		
		}			
    },

	handleIncLdsError: function (cmp, event, helper) {
		helper.handleLdsError(cmp, 'incident', event.getParam("value"));
	},
})