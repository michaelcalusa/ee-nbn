({
	init: function (cmp, event, helper) {

		var mode = cmp.get('v.mode');
		if(mode == 'New')
			helper.prepareNewForm(cmp, helper);
		else 
			helper.prepareEditForm(cmp);
	},
    
    toggleAddInfo : function(cmp, event, helper) {
        cmp.set("v.isAdditionInfoVisible", true);			
		window.setTimeout(
			$A.getCallback(function() {
				var ctrl = cmp.find('addInfoCmp');
				if(ctrl){		
					ctrl.focus();									
				}					
			}), 200
		);    	
    },

    toggleAddNotes : function(cmp, event, helper) {
		cmp.set("v.isAdditionNotesVisible", true);			
		window.setTimeout(
			$A.getCallback(function() {
				var ctrl = cmp.find('addNotesCmp');
				if(ctrl){		
					ctrl.focus();									
				}					
			}), 500
		);        
    },

    saveAdditionalNotes : function(cmp, event, helper) {

       var inputNotes = cmp.find("addNotesCmp").get("v.value"); 
       var promise1 = helper.apex(cmp, 'v.insertedNoteId', 'c.createIncidentNotes', {incId: cmp.get('v.incId'),noteDetail:inputNotes,noteType:'General Information'}, false);

        promise1
            .then(function (result) {   
            	var cmp = result.sourceCmp;
				var promise = helper.apex(cmp, 'v.allNotesList', 'c.getIncidentNotes', {incId: cmp.get('v.incId')}, false);
				promise.catch(function (result) {
				var error = result.error;
                $A.reportError("error message", error);
				});
				cmp.set("v.isAdditionInfoVisible", false);	
				cmp.set("v.isAdditionNotesVisible", false);
		    })
            .catch(function (result) {
                var cmp = result.sourceCmp;
				var helper = result.sourceHelper;				
                helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
            });
    },

    saveAdditionalInfo : function(cmp, event, helper) {

       var inputNotes = cmp.find("addInfoCmp").get("v.value"); 
       var promise1 = helper.apex(cmp, 'v.insertedNoteId', 'c.createIncidentNotes', {incId: cmp.get('v.incId'),noteDetail:inputNotes,noteType:'Detail Clarification'}, false);

        promise1
            .then(function (result) {    
            	var cmp = result.sourceCmp;   
				var promise = helper.apex(cmp, 'v.allNotesList', 'c.getIncidentNotes', {incId: cmp.get('v.incId')}, false);
				promise.catch(function (result) {
				var error = result.error;
                $A.reportError("error message", error);
				});
				cmp.set("v.isAdditionInfoVisible", false);	
				cmp.set("v.isAdditionNotesVisible", false);			
            })
            .catch(function (result) {
                var cmp = result.sourceCmp;
				if(cmp){
					var helper = result.sourceHelper;				
					helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");		
				}else
				{
					console.log(result);
				}
            });
    },
	
	returnToSearch: function (cmp, event, helper) {
		if(cmp.get('v.mode') == 'New') cmp.set('v.refreshSearch', true);
        cmp.set('v.displayedDetailItem', '');
		$A.util.addClass(event.getSource(), 'slds-hide');
		var evt = cmp.getEvent("topBannerEvent"); 
		evt.setParams({"resetMessage" : "true"});
		evt.fire();
    },	

	submit: function (cmp, event, helper) {
        cmp.find("forceRecordIncidentCmp").saveRecord(function (saveResult) {            
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
				cmp.set("v.incId", saveResult.recordId);
				var evt = cmp.getEvent("topBannerEvent"); 
				evt.setParams({"isSuccessMessage" : "true", "message" : cmp.get('v.customSettings.Service_REQ_Details_Save_OK') });
				evt.fire();
            } else {
				var evt = cmp.getEvent("topBannerEvent"); 
				evt.setParams({"isErrorMessage" : "true", "message" : $A.get("$Label.c.DF_Application_Error") });
				evt.fire();				
                if (saveResult.state === "INCOMPLETE") {
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    console.log('Problem saving incident, error: ' + JSON.stringify(saveResult.error));
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                }
            }
        });
    },

	handleSelectedServiceRequestTypeChange: function (cmp, event, helper) {		
		var serviceRequestIssueOptions;
		var allServiceRequestNIssuesTypes = cmp.get('v.allServiceRequestNIssuesTypes');
		var selectedServiceRequestType = cmp.get('v.incidentFields.Service_Request_Type__c');
		var requestTypes = cmp.get('v.allServiceRequestNIssuesTypes').filter(function(item){ return item.Value__c == selectedServiceRequestType; });
		
		if (requestTypes.length > 0) {
			var selectedServiceRequestTypeInternalName = requestTypes[0].Name;
			serviceRequestIssueOptions = allServiceRequestNIssuesTypes
			.filter(function(item){ return item.Parent__c == selectedServiceRequestTypeInternalName; })
			.map(function(item){ return { label: item.Label__c, value: item.Value__c };	});
		}

        cmp.set('v.serviceRequestIssueOptions', serviceRequestIssueOptions);
    },

	cancel: function (cmp, event, helper) {
        cmp.set('v.displayedDetailItem', '');
    },

	onIncRecordUpdated: function (cmp, event, helper) {
		var changeType = event.getParams().changeType;
        if (changeType === "ERROR") {} else if (changeType === "LOADED") {
			if(cmp.get('v.mode') == 'Edit') {
				var selectedBPIId = cmp.get('v.incidentFields.PRI_ID__c');				
				helper.getProductInfoFromServiceCache(cmp, selectedBPIId);
				
				var industryStatus = cmp.get("v.incidentFields.Industry_Status__c");
				var inProgressPendingStatus = 'In Progress Pending';
				var inProgressHeldStatus = 'In Progress Held';
				var resolvedStatus = 'Resolved';
				var closedStatus = 'Closed';				

				cmp.set('v.isAdditionalInfoRequired', (industryStatus == inProgressPendingStatus) ? true : false);
				cmp.set('v.isReasonCodeVisiable', (industryStatus == inProgressPendingStatus || industryStatus == inProgressHeldStatus) ? true : false);
				cmp.set('v.isResolved', (industryStatus == resolvedStatus || industryStatus == closedStatus) ? true : false);		
			}						
        } else if (changeType === "REMOVED") {} else if (changeType === "CHANGED") {            
            
        }
	},

	goBack: function (cmp, event, helper) {
        var currentStep = cmp.get('v.currentStep');
        var minStep = cmp.get('v.minStep');
        if (currentStep > minStep) currentStep -= 1;		
        cmp.set('v.currentStep', currentStep);
		helper.smoothScrollToPageTop(cmp);
    },

    goNext: function (cmp, event, helper) {		
		if (!helper.validate(cmp, event)) return;
        var currentStep = cmp.get('v.currentStep');
        var maxStep = cmp.get('v.maxStep');		
        if (currentStep < maxStep) currentStep += 1;
				
		cmp.set('v.currentStep', currentStep);
		helper.smoothScrollToPageTop(cmp);
    },   

	handleServiceCacheProductInfoUpdate: function (cmp, event, helper) {		
		if(cmp.get('v.mode') == 'New' && cmp.get('v.incidentFields') != null) {
			cmp.set('v.incidentFields.serviceRestorationSLA__c', cmp.get('v.serviceCacheProductInfo.sla'));
			cmp.set('v.incidentFields.serviceRegion__c', cmp.get('v.serviceCacheProductInfo.enterpriseEthernetServiceLevelRegion'));
			cmp.set('v.incidentFields.locId__c', cmp.get('v.serviceCacheProductInfo.locationId'));
		}
	},
	
	onClickBPILink: function (cmp, event, helper) {             
        var selectedBpiId = event.target.getAttribute("data-Id");
		cmp.set('v.selectedBPIId', selectedBpiId);
		cmp.set('v.sourceDisplayedDetailItem', cmp.get('v.componentName'));
		cmp.set('v.displayedDetailItem', cmp.get('v.activeServiceSearchComponent'));		
	},
})