({
    validate: function (cmp, event) {
        var currentStep = cmp.get("v.currentStep");
        var tempCtrl = cmp.find("btnNext");
        var valid = true;
        var fieldGroupName;

        if (currentStep == 1) {
            fieldGroupName = 'step1Fields';
            var ctrls = cmp.find(fieldGroupName);

            var ctrl = ctrls.filter(this.getComponentByName("Service_Request_Type__c"))[0];
            if (ctrl) this.validateSelectCmpEmpty(ctrl);

            ctrl = ctrls.filter(this.getComponentByName("Service_Request_Issue_Type__c"))[0];
            if (ctrl) this.validateSelectCmpEmpty(ctrl);

            ctrl = ctrls.filter(this.getComponentByName("Incident_Notes__c"))[0];
            if (ctrl) this.validateInputCmpEmpty(ctrl);

            valid = this.isAllFieldsValid(cmp, fieldGroupName);
        }

        if (currentStep == 2) {
            fieldGroupName = 'step2Fields';
            var ctrls = cmp.find(fieldGroupName);
            var ctrl = ctrls.filter(this.getComponentByName("Technical_Contact_Name__c"))[0];
            this.validateInputCmpEmpty(ctrl);

            ctrl = ctrls.filter(this.getComponentByName("Technical_Contact_Mobile_Number__c"))[0];
            this.validateInputCmpEmpty(ctrl);

            ctrl = ctrls.filter(this.getComponentByName("Site_Contact_Name__c"))[0];
            this.validateInputCmpEmpty(ctrl);

            ctrl = ctrls.filter(this.getComponentByName("Site_Contact_Mobile_Number__c"))[0];
            this.validateInputCmpEmpty(ctrl);

            ctrl = ctrls.filter(this.getComponentByName("Business_Name__c"))[0];
            this.validateInputCmpEmpty(ctrl);

            ctrl = ctrls.filter(this.getComponentByName("Site_Induction_Essentials__c"))[0];
            this.validateSelectCmpEmpty(ctrl);

            ctrl = ctrls.filter(this.getComponentByName("Security_Required__c"))[0];
            this.validateSelectCmpEmpty(ctrl);

            ctrl = ctrls.filter(this.getComponentByName("Current_White_Card__c"))[0];
            this.validateSelectCmpEmpty(ctrl);

            ctrl = ctrls.filter(this.getComponentByName("Security_Clearance__c"))[0];
            this.validateSelectCmpEmpty(ctrl);

            ctrl = ctrls.filter(this.getComponentByName("Site_Access_Clearance__c"))[0];
            this.validateSelectCmpEmpty(ctrl);

            valid = this.isAllFieldsValid(cmp, fieldGroupName);
        }

        if (!valid) event.getSource().focus();

        return valid;
    },

    prepareNewForm: function (cmp, helper) {

        var promise1 = helper.apex(cmp, 'v.serviceRequestRecordTypeId', 'c.getRecordTypeId', {
            objectApiName: cmp.get("v.incObjectApiName"),
            recordTypeDeveloperName: cmp.get('v.serviceRequestRecordTypeDeveloperName')
        }, false);

        // Get dependant pick list values from custom settings
        var promise2 = helper.apex(cmp, 'v.allServiceRequestNIssuesTypes', 'c.getASDependantPickListFromCustomSettings', {
            picklistName: 'ServiceRequestTypeNIssue'
        }, false);

        var promise3 = helper.apex(cmp, 'v.siteInductionEssentialOptions', 'c.getASPickListValuesIntoList', {
            objectApiName: cmp.get('v.incObjectApiName'),
            fieldApiName: 'Site_Induction_Essentials__c'
        }, true);

        var promise4 = helper.apex(cmp, 'v.securityRequiredOptions', 'c.getASPickListValuesIntoList', {
            objectApiName: cmp.get('v.incObjectApiName'),
            fieldApiName: 'Security_Required__c'
        }, true);

        var promise5 = helper.apex(cmp, 'v.currentWhiteCardOptions', 'c.getASPickListValuesIntoList', {
            objectApiName: cmp.get('v.incObjectApiName'),
            fieldApiName: 'Current_White_Card__c'
        }, true);

        var promise6 = helper.apex(cmp, 'v.securityClearanceOptions', 'c.getASPickListValuesIntoList', {
            objectApiName: cmp.get('v.incObjectApiName'),
            fieldApiName: 'Security_Clearance__c'
        }, true);

        var promise7 = helper.apex(cmp, 'v.siteAccessClearanceOptions', 'c.getASPickListValuesIntoList', {
            objectApiName: cmp.get('v.incObjectApiName'),
            fieldApiName: 'Security_Clearance__c'
        }, true);

        var promise8 = helper.apex(cmp, 'v.siteOperationHoursOptions', 'c.getASPickListValuesIntoList', {
            objectApiName: cmp.get('v.incObjectApiName'),
            fieldApiName: 'Site_Operating_Hours__c'
        }, true);

		var promise9 = this.apex(cmp, 'v.customSettings', 'c.getASCustomSettings', {
            componentName: cmp.get('v.componentName')
        }, false);

		var promise10 = this.apex(cmp, 'v.currentUserAccountAccessSeekerId', 'c.getCurrentUserAccountAccessSeekerId', { }, false);
		
        Promise.all([promise1, promise2, promise3, promise4, promise5, promise6, promise7, promise8, promise9, promise10])
            .then(function (result) {
                // Prepare a new record from template
                cmp.find('forceRecordIncidentCmp').getNewRecord(
                    cmp.get('v.incObjectApiName'), // sObject type (objectApiName)
                    cmp.get('v.serviceRequestRecordTypeId'), // recordTypeId
                    false, // skip cache?
                    $A.getCallback(function () {						
						cmp.set('v.incidentFields.Access_Seeker_ID__c', cmp.get('v.currentUserAccountAccessSeekerId'));
						var selectedBPIId = cmp.get('v.selectedBPIId');
                        cmp.set('v.incidentFields.PRI_ID__c', selectedBPIId);						
						helper.getProductInfoFromServiceCache(cmp, selectedBPIId);
                    })
                );

				helper.convertPickList(cmp, 'v.siteInductionEssentialOptions');
                helper.convertPickList(cmp, 'v.securityRequiredOptions');
                helper.convertPickList(cmp, 'v.currentWhiteCardOptions');
                helper.convertPickList(cmp, 'v.securityClearanceOptions');
                helper.convertPickList(cmp, 'v.siteAccessClearanceOptions');
                helper.convertPickList(cmp, 'v.siteOperationHoursOptions');

                var allServiceRequestNIssuesTypes = cmp.get('v.allServiceRequestNIssuesTypes');

                // Populate service request type pick list. It's a top level pick list so partent__c is always empty
                var serviceRequestTypeOptions = allServiceRequestNIssuesTypes
                    .filter(function (item) {
                        if (!item.Parent__c) return item;
                    })
                    .map(function (item) {
                        return {
                            label: item.Label__c,
                            value: item.Value__c
                        };
                    });

                serviceRequestTypeOptions.sort(helper.dynamicSort(cmp, 'label'));
                cmp.set('v.serviceRequestTypeOptions', serviceRequestTypeOptions);
                cmp.set('v.initDone', true);
            })
            .catch(function (result) {
                var cmp = result.sourceCmp;
                if (cmp) {
                    var evt = cmp.getEvent("topBannerEvent"); 
					evt.setParams({"isErrorMessage" : "true", "message" : $A.get("$Label.c.DF_Application_Error") });
					evt.fire();				
                } else {
                    console.log(result);
                }
            });
    },

    prepareEditForm: function (cmp) {
		cmp.set("v.currentStep", cmp.get('v.detailStepId'));		
		var promise1 = this.apex(cmp, 'v.customSettings', 'c.getASCustomSettings', {
            componentName: cmp.get('v.componentName')
        }, false);
		
		var promise2 = this.apex(cmp, 'v.allNotesList', 'c.getIncidentNotes', { incId: cmp.get('v.incId') }, false);	
        
        Promise.all([promise1, promise2])
            .then(function (result) {       
				cmp.find("forceRecordIncidentCmp").reloadRecord();				
				cmp.set('v.initDone', true);			
            })
            .catch(function (result) {
                var cmp = result.sourceCmp;
				if(cmp){
					var evt = cmp.getEvent("topBannerEvent"); 
					evt.setParams({"isErrorMessage" : "true", "message" : $A.get("$Label.c.DF_Application_Error") });
					evt.fire();					
				}else
				{
					console.log(result);
				}
            });
    },

	getProductInfoFromServiceCache: function (cmp, selectedBPIId) {
		var promise1 = this.apex(cmp, 'v.serviceCacheProductInfo', 'c.searchServiceCache', { searchString: selectedBPIId }, false);

		Promise.all([promise1])
		.then(function(result) {
		})
		.catch(function(result) {
			var cmp = result.sourceCmp;
			if(cmp) {
				var errorMessage = $A.get("$Label.c.DF_Application_Error");				
				if(result.sourceAction == 'c.searchServiceCache') errorMessage = $A.get("$Label.c.EE_AS_SVC_NOT_AVAILABLE");				
				var evt = cmp.getEvent("topBannerEvent"); 
				evt.setParams({"isErrorMessage" : "true", "message" : errorMessage });
				evt.fire();
			}else
			{
				console.log(result);
			}
		});
	},	
})