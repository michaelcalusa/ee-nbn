({
    fetchTransitionRecord : function(component, event, helper) {
        
        var action = component.get("c.getTransitionRecord");
        var recordId = component.get("v.recordId");
        var userId = $A.get("$SObjectType.CurrentUser.Id");

        
        action.setParams({
            opportunityId : recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() != null) {
                    $A.get("e.force:closeQuickAction").fire();
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Sorry!",
                        "message": "Only 1 Transition Engagement is allowed per Opportunity",
                        "type": "Error"
                    });
                    toastEvent.fire();
                    
                }else{
                    /* component.find("transitionRecordCreator").getNewRecord(
                        "Transition_Engagement__c", // sObject type (objectApiName)
                        null,      // recordTypeId
                        true,     // skip cache?
                        $A.getCallback(function() {
                            var rec = component.get("v.transitionRecord");
                            var error = component.get("v.transitionRecordError");
                            if(error || (rec === null)) {
                                console.log("Error initializing record template: " + error);
                                return;
                            }
                            console.log("Record template initialized: " + rec.sobjectType);
                        })
                    ); */
                    $A.get("e.force:closeQuickAction").fire();
                    var createMyCustomObjRecEvent = $A.get("e.force:createRecord");
                    createMyCustomObjRecEvent.setParams({
                        "entityApiName": "Transition_Engagement__c",
                        "defaultFieldValues": {                            
                            'Opportunity__c' : recordId,
                            'Assigned_To__c' : userId,
                        }
                    });
                    createMyCustomObjRecEvent.fire();
                }
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        
        $A.enqueueAction(action);
    },
    
   /* saveTransitionRecord : function (component, event, helper) {
        component.get("v.transitionRecordFields").Opportunity__c = component.get("v.recordId");
        component.find("transitionRecordCreator").saveRecord(function(saveResult) {           
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                $A.get("e.force:closeQuickAction").fire();
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success",
                    "message": "Transction Record has been added successfully",
                    "type": "Success"
                });
                toastEvent.fire();
                
            } else if (saveResult.state === "INCOMPLETE") {
                console.log("User is offline, device doesn't support drafts.");
                
            } else if (saveResult.state === "ERROR") {
                console.log('Problem saving contact, error: ' + JSON.stringify(saveResult.error));
                
            } else {
                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));               
            }
        }); 
    }*/
})