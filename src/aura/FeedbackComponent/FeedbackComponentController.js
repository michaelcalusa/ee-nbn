({
	submitFeedback : function(component, event, helper) {
		console.log('fb', component.get("v.feedbackText"));
        
        var feedback = component.get("v.feedbackText");
        var cmpTarget = component.find('feedbackError');
        var feedbackBox = component.find('feedbackBox');
        
        if (!helper.isNullOrWhitespace(feedback)) {
            $A.util.addClass(cmpTarget, 'hideField');
            $A.util.removeClass(feedbackBox, 'errorBox');
            helper.submitFeedback(component);
        }
        else {
            console.log('whitespace');
            $A.util.removeClass(cmpTarget, 'hideField');
            $A.util.addClass(feedbackBox, 'errorBox');
        }
	}
})