({
	submitFeedback : function(component) {
		console.log('submitFeedback');
        var action = component.get("c.sendFeedback");
        action.setParams({
            feedback : component.get("v.feedbackText"),
            articleId : component.get("v.articleId")
        });

        action.setCallback(this, function(response) {
        	var state = response.getState();
            
        	if (component.isValid() && state === 'SUCCESS') {
                console.log('feedback sent');
                
                var resp = response.getReturnValue();
                console.log('response', response);
                
                //flip feedback to something else
                component.set("v.feedbackSent", true);
        	}
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
		});
        $A.enqueueAction(action);
	},
    
    isNullOrWhitespace : function(input) {
    	if (typeof input === 'undefined' || input == null)
            	return true;
        return input.replace(/\s/g, '').length < 1;
    }
})