({
    doInit : function(cmp, event, helper) {
        var action = cmp.get("c.getListContentDocument");
	    action.setParams({
	            "myRecordId":  cmp.get("v.myRecordId")
	    });  
	    
        action.setCallback(this, function(response){
            console.log('Call Back Respone ==>'+ response);
            var state = response.getState();
          //  alert("CallBack Status:" + state);
            if (state === "SUCCESS") {
                cmp.set("v.FileList", response.getReturnValue());
                cmp.set("v.attachmentLength",response.getReturnValue().length);
            }
        });
	    $A.enqueueAction(action);
      
	},
	  
		
		
		
	
    
    handleUploadFinished: function (cmp, event) {        
        
        var uploadedFiles = event.getParam("files");
        var action = cmp.get("c.getListContentDocument");
	    action.setParams({
	            "myRecordId":  cmp.get("v.myRecordId")
	    });  
	    
        action.setCallback(this, function(response){
            console.log('Call Back Respone ==>'+ response);
            var state = response.getState();
           // alert("CallBack Status:" + state);
            if (state === "SUCCESS") {
                cmp.set("v.FileList", response.getReturnValue());
                cmp.set("v.attachmentLength",response.getReturnValue().length);
                //alert(response.getReturnValue().length);
            }
        });
	    $A.enqueueAction(action);
       var cmpEvent = cmp.getEvent("uploadfinishedEvent");
        console.log('event fired');
        cmpEvent.fire();
        console.log('event finished');
    } ,
    
     toggleIcon : function(component,event,helper){
        var IconLocation = component.find("fileAttachedOverallValidStatus");
        var OverallInfoLocation = component.find("fileAttachedDetailStatus");
        
        helper.togglingIcons(IconLocation);  
        helper.togglingIcons(OverallInfoLocation);
    },
    
    removeFile : function(component, event, helper){
        var target = event.target;
        var dataEle = target.getAttribute("data-selected-Index");
     //   alert("Remove Id" + dataEle);
        console.log("Remove Content DocumentId  ==>"+ dataEle);
        var removeFileAction = component.get("c.removeContentDocument");
        removeFileAction.setParams({
	            "contentDocumentId":  dataEle,
	            "myRecordId":  component.get("v.myRecordId")
	    });  
        removeFileAction.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.FileList", response.getReturnValue());
                console.log('response ', response.getReturnValue());
                component.set("v.attachmentLength",response.getReturnValue().length);
            }
        });       
        $A.enqueueAction(removeFileAction);  
    }
    
})