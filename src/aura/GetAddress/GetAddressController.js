({
    doInit : function(component, event, helper){
        /*console.log(component.get("v.displaySelectedAddress"));
        if(component.get("v.displaySelectedAddress") != null){
            var AddressTerm = component.get("v.displaySelectedAddress");
            var resultLimit = component.get("v.AddressLimit");
            var indexTypeAhead = component.get("v.AddressTypeAhead");
            if( AddressTerm.length > indexTypeAhead ){
                helper.searchGoogleMapHelper(component, event, AddressTerm, resultLimit,true);
            }
        }*/
    },
	GoogleMapSearch : function(component, event, helper) {
        //alert('GoogleMapSearch');
		var AddressTerm = document.getElementById("searchAddTextId").value;
        var resultLimit = component.get("v.AddressLimit");
        var indexTypeAhead = component.get("v.AddressTypeAhead");
        console.log('*** length'+ AddressTerm);
        
        if( AddressTerm.length > indexTypeAhead ){
            helper.searchGoogleMapHelper(component, event, AddressTerm, resultLimit,false);
        }
        else{  
            component.set("v.listOfSearchRecords", null); 
            var forclose = component.find("typeahead-00B-9722");
            $A.util.addClass(forclose, 'lookup_menu_close');
            $A.util.removeClass(forclose, 'lookup_menu_open');
            
            var myEvent = $A.get("e.c:nullAddressSearchEvent");                    
            myEvent.fire();
        }   
	},
    
    selectAddress : function(component, event, helper) {
        //alert('selectAddress');
        component.set("v.hasError",false);
        var PlaceID = event.currentTarget.dataset.placeid;
        var addr = event.currentTarget.dataset.stradd;
        component.set("v.displaySelectedAddress",addr);
        console.log(' PlaceID '+PlaceID);
        helper.getAddressDetails(component, event, PlaceID);

        var forclose = component.find("typeahead-00B-9722");
        $A.util.addClass(forclose, 'lookup_menu_close');
        $A.util.removeClass(forclose, 'lookup_menu_open');
    },
    
    addressInfo : function(component, event, helper) {
    },
    searchOnSelect : function(component, event, helper){
        //alert('searchOnSelect');
        var AddressTerm = document.getElementById("searchAddTextId").value;
        var resultLimit = component.get("v.AddressLimit");
        var indexTypeAhead = component.get("v.AddressTypeAhead");
        if( AddressTerm.length > indexTypeAhead ){
            helper.searchGoogleMapHelper(component, event, AddressTerm, resultLimit,true);
        }
    }
})