({
	searchGoogleMapHelper : function(component,event,getInputkeyWord,resultLimit,searchButton) {
		var action = component.get("c.searchGoogleMap"); 
        action.setParams({
            'strAddressTerm': getInputkeyWord,
            'intAddressLimit': resultLimit
        });
        
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(' resssss '+state);
            
            if (state === "SUCCESS") {
                console.log('inside if');
                var storeResponse = response.getReturnValue();
                console.log('storeResponse---'+JSON.stringify(storeResponse));
                component.set("v.listOfSearchRecords", storeResponse);
                console.log('storeResponse.length=='+ storeResponse.length);
                if(storeResponse.length == 0){
                    if(searchButton){
                        component.set("v.hasError",true);
                    }
        			var forclose = component.find("typeahead-00B-9722");
                    $A.util.addClass(forclose, 'lookup_menu_close');
                    $A.util.removeClass(forclose, 'lookup_menu_open');
                    
                    // Set the map for events
                    var myMap = component.get("v.mapAddressResults");
                    myMap["StreetName"] = getInputkeyWord; 
                    myMap["displaySelectedAddress"] = '';
                    myMap["Suburb"] = '';
                    myMap["State"] = '';
                    myMap["PostalCode"] = '';
                    
                    component.set('v.mapAddressResults',myMap);
                    console.log('ABN Map ==>'+ myMap);
                    
                    var nullSearchEvent;
                    if(component.get("v.cmpName") == 'ContractSignatory'){
                    	nullSearchEvent = component.getEvent("nullResultEventContractSign");  
                        nullSearchEvent.setParams({
                        "streetAddress": getInputkeyWord
                    });
                    nullSearchEvent.fire(); 
                    }
                    else if(component.get("v.cmpName") == 'BillingInformation'){
                        nullSearchEvent = component.getEvent("nullResultEvent");
                        nullSearchEvent.setParams({
                        "streetAddress": getInputkeyWord
                    });
                    nullSearchEvent.fire(); 
                    }
                    
                    
					
                }
                else{
                    if(searchButton){
                        component.set("v.hasError",false);
                        var forclose = component.find("typeahead-00B-9722");
                        $A.util.addClass(forclose, 'lookup_menu_close');
                        $A.util.removeClass(forclose, 'lookup_menu_open');
                        console.log(' place id '+storeResponse[0].strPlaceId);
                        this.getAddressDetails(component,event,storeResponse[0].strPlaceId);
                    }
                    else{
                        var findTypeahead = component.find("typeahead-00B-9722");
                        $A.util.addClass(findTypeahead, 'lookup_menu_open');
                        $A.util.removeClass(findTypeahead, 'lookup_menu_close');
                    }
                }
                /*
                console.log(storeResponse[0].strAddress);
                var noAddressFound = storeResponse[0].strAddress;
                if (noAddressFound == 'No Address Found'){
                    component.set("v.listOfSearchRecords", storeResponse);
                    var myEvent = $A.get("e.c:GetAddressInfoEvent");                    
                    myEvent.setParams({
                        "StreetName":getInputkeyWord,
                        "Suburb":'',
                        "State":'',
                        "PostalCode":''
                    });
                    myEvent.fire();
                }
                */
            }  
        });
        // enqueue the Action  
        $A.enqueueAction(action);
	},
    getAddressDetails : function(component,event,getPlaceId) {
		var action = component.get("c.getAddressDetails");
        action.setParams({
            'strPlaceID': getPlaceId
        });
        
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('response.getState()--'+response.getState());
            console.log('response value '+JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('334455 '+JSON.stringify(storeResponse));
                component.set("v.Address", storeResponse.strStreet);
                
                // Set the map for events
                var myMap = component.get("v.mapAddressResults");
                myMap["displaySelectedAddress"] = component.get("v.displaySelectedAddress");
                myMap["StreetName"] = storeResponse.strStreet; 
                myMap["Suburb"] = storeResponse.strSuburb;
                myMap["State"] = storeResponse.strState;
                myMap["PostalCode"] = storeResponse.strPostalCode;
                myMap["latitude"] = storeResponse.latitude;
                myMap["longitude"] = storeResponse.longitude;
                    
                component.set('v.mapAddressResults',myMap);
               //  console.log('334455444 '+JSON.stringify(component.get('v.mapAddressResults'));
                var myEvent = $A.get("e.c:GetAddressInfoEvent");
                
                myEvent.setParams({
                    "mapAddressResults":component.get("v.mapAddressResults"),
                    "cmpName":component.get("v.cmpName"),
                    "userSearchString": component.get("v.displaySelectedAddress")
                });
                console.log(' user search in get address '+component.get("v.displaySelectedAddress"));
                console.log(' possible user search in get address '+document.getElementById("searchAddTextId").value);
                myEvent.fire();
            }     
        });
        
        // enqueue the Action  
        $A.enqueueAction(action);
	}
})