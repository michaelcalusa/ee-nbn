({
    ABNSearch : function(component, event, helper) {
        var ABNNo = document.getElementById(component.get('v.labelId')).value;
        var errorDivId = document.getElementById(component.get('v.labelId')+'-abnSearch-abn-div');
        var errorSpanId = document.getElementById(component.get('v.labelId')+'-abnSearch-abn-error');   
        component.set('v.ABNNumber',ABNNo);
        
        if(ABNNo!='' && ABNNo!=undefined){
            var errorDivId = document.getElementById(component.get('v.labelId')+'-abnSearch-abn-div');
            var errorSpanId = document.getElementById(component.get('v.labelId')+'-abnSearch-abn-error');  
            if($A.util.hasClass(errorSpanId,'help-block-error'))
            {
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error'); 
                // $A.util.addClass(errorDivId,'has-success'); 
                
            }
            helper.searchABNHelper(component, event, ABNNo);           
        }
        else{
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');     
            document.getElementById(component.get('v.labelId')+'-abnSearch-abn-error').innerText='';
            component.set("v.showErrors", true);           
            component.set("v.SearchMessage", 'Please enter a valid ABN');            
            document.getElementById(component.get('v.labelId')+'-abnSearch-abn-error').innerText=component.get('v.SearchMessage');
        }
    },
    
    validateABN : function(cmp, event, helper){
        var fieldId = event.currentTarget.id;       
        var fieldVal = document.getElementById(fieldId).value;
        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);      
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;       
        
        

            if(fieldVal=='' || !fieldVal.match(/^(\d *?){11}$/)){

                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    document.getElementById(event.currentTarget.dataset.errorspanid).innerText='Please enter a valid ABN';
            }
            else{
                	$A.util.removeClass(errorSpanId,'help-block-error');
               		$A.util.removeClass(errorDivId,'has-error');
                	
            	}   

    },
    
    onbusinessGroup : function(component, event) {
        var selectedBusiness = event.target.getAttribute('data-businessGroup');
        var selectedACN = event.target.getAttribute('data-acnnumber');
        var selectedRegisteredEntityName = event.target.getAttribute('data-RegisteredEntityName');
        var selectedentityTypeCode = event.target.getAttribute('data-entityTypeCode');
        var selectedentityStatusCode = event.target.getAttribute('data-entityStatusCode');
        var selectedentityDescription = event.target.getAttribute('data-entityDescription');
       	var selectedeType = false;
        var abnType;
        var selectedcustClass = event.target.getAttribute('data-custClass');
           selectedeType = event.target.getAttribute('data-eType');
       

        component.set("v.BusinessName", selectedBusiness);
        component.set("v.showResult", true);
        component.set("v.ACNNumber", selectedACN);
        
        // Set the map for events
		var myMap = component.get("v.mapABNResults");
        myMap["BusinessName"] = selectedBusiness; 
        myMap["ACNNumber"] = selectedACN;
        myMap['RegisteredEntityName'] = selectedRegisteredEntityName;
        myMap['entityStatusCode'] = selectedentityStatusCode;
        myMap['entityTypeCode'] = selectedentityTypeCode;
        myMap['entityDescription'] = selectedentityDescription;
      	myMap['eType'] = selectedeType;
        myMap['custClass'] = selectedcustClass;
        myMap["ABNNumber"] = component.get('v.ABNNumber');
        myMap["isApplicantABN"] = component.get('v.isApplicantABN');
        
        component.set('v.mapABNResults',myMap);
        // Set the visibility off for results and error section
        component.set("v.showBusinessResult", false);
        component.set("v.showErrors", false);
        
        if(component.get('v.labelId') == 'searchTrusteeABNTextId')
        {
            abnType = 'Trustee ABN';
        }
        else if(component.get('v.labelId') == 'searchABNTextId')
        {
            abnType = 'ABN';
        }
        // Set the event to use values
        var myEvent = $A.get("e.c:ABNSearchInformation");
        myEvent.setParams({
            "mapABNResults":component.get('v.mapABNResults'),
            "Status":'SUCCESS',
            "abnType":abnType
        });
        myEvent.fire();
    },
    
    searchABNInformation : function(component, event, helper) {
    }
})