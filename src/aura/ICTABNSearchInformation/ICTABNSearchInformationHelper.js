({
    searchABNHelper : function(component,event,getInputkeyWord) {
        var action = component.get("c.doABNSearch"); 
        var abnType;
        action.setParams({
            'strABNNumber': getInputkeyWord
        });
        
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if (storeResponse.strErrorMsg.length == 0) {

                        component.set("v.showBusinessResult", false);
                        component.set("v.showErrors", false);
                        component.set("v.showResult", false);
                        component.set("v.SearchMessage", '');
                        
                        // Set the map for events
                        var myMap = component.get('v.mapABNResults');
                        myMap['BusinessName'] = storeResponse.lstBusinessName[0]; 
                        myMap['RegisteredEntityName'] = storeResponse.lstBusinessName[0];
                        myMap['entityStatusCode'] = storeResponse.entityStatusCode;
                        myMap['entityTypeCode'] = storeResponse.entityTypeCode;
                        myMap['entityDescription'] = storeResponse.entityDescription;
                        myMap['eType'] = storeResponse.eType;
                        myMap['custClass'] = storeResponse.custClass;
                        myMap['ACNNumber'] = storeResponse.strABNNumber;
                        myMap['ABNNumber'] = getInputkeyWord;
                        myMap['isApplicantABN'] = component.get('v.isApplicantABN');
                        component.set('v.mapABNResults',myMap);
                        if(component.get('v.labelId') == 'searchTrusteeABNTextId')
                        {
                            abnType = 'Trustee ABN';
                        }
                        else if(component.get('v.labelId') == 'searchABNTextId')
                        {
                            abnType = 'ABN';
                        }
                        // Set the event to use values
                        var myEvent = $A.get("e.c:ABNSearchInformation");
                        myEvent.setParams({
                            "mapABNResults":component.get('v.mapABNResults'),
                            "Status":'SUCCESS',
                            "abnType":abnType
                        });                       
                        myEvent.fire();

                } else {
                    component.set("v.ABNSearchResult", storeResponse);
                    component.set("v.showBusinessResult", false);
                    component.set("v.showErrors", true);
                    component.set("v.SearchMessage", storeResponse.strErrorMsg);
                    component.set("v.BusinessName", '');
                    component.set("v.showResult", false);
                    var errorDivId = document.getElementById(component.get('v.labelId')+'-abnSearch-abn-div');
                    var errorSpanId = document.getElementById(component.get('v.labelId')+'-abnSearch-abn-error');  
                    
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error'); 
                    document.getElementById(component.get('v.labelId')+'-abnSearch-abn-error').innerText='Please enter a valid ABN';                    
                    
					// Set the map for events
                    var myMap = component.get('v.mapABNResults');
                    myMap['BusinessName'] = ''; 
                    myMap['RegisteredEntityName'] = ''; 
                    myMap['entityStatusCode'] = ''; 
                    myMap['entityTypeCode'] = ''; 
                    myMap['entityDescription'] = ''; 
                    myMap['ACNNumber'] = '';
                    myMap['eType'] = '';
                    myMap['custClass'] = '';
                    myMap['ABNNumber'] = getInputkeyWord;
                    myMap['isApplicantABN'] = component.get('v.isApplicantABN');
                
                    component.set('v.mapABNResults',myMap);
                    
                    // Set the event to use values
                    var myEvent = $A.get("e.c:ABNSearchInformation");
                    myEvent.setParams({
                        "mapABNResults":component.get('v.mapABNResults'),
                        "Status":'SUCCESS'
                    });
                    myEvent.fire();
                }                
            }     
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    }
})