({
	init : function(component, event, helper) {  
		helper.getUserInformation(component, event);
    },
	populateAddressInfo : function(component, event, helper) {  
        var mapAddressResult = event.getParam('mapAddressResults');
        
        var streetName = mapAddressResult["StreetName"]; 
        var suburb = mapAddressResult["Suburb"];
        var state = mapAddressResult["State"]; 
        var postalCode = mapAddressResult["PostalCode"];
        var showValidation = mapAddressResult["ShowValidation"];
        
        console.log('streetName'+streetName);
        console.log('suburb'+suburb);
        console.log('state'+state);
        console.log('postalCode'+postalCode);
        console.log('showValidation'+showValidation);
        
        component.set("v.showValidation", showValidation);
        component.set("v.addressStreetName", streetName);
        component.set("v.addressSuburb", suburb);
        component.set("v.addressState", state);
        component.set("v.addressPostalCode", postalCode);
         
        if(showValidation != undefined){
			helper.validateAddressFields(component, event, helper);            
        }
    },
    
    validateFields : function(cmp, event, helper){
        console.log('field validation');
        console.log('event div==>'+ event.currentTarget.dataset.errordivid);
        console.log('event id==>'+ event.currentTarget.id);
        
        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);       
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;
        
        var fieldId = event.currentTarget.id;       
        var fieldVal = document.getElementById(fieldId).value;
        
        console.log('errorDivId' + errorDivId);
        console.log('errorSpanId' + errorSpanId);
        console.log('errorMsg' + errorMsg);
        console.log('fieldId' + fieldId);
        console.log('fieldVal' + fieldVal);
        
        
        if(fieldId=='ict-phoneno' && !fieldVal.match(/^\d{10}$/)){
            document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid phone number';
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldId=='ict-roletype'){
            if(fieldVal == 'Please choose'){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
            else{
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error'); 
                $A.util.addClass(errorDivId,'has-success'); 
            }
        }
        else if(fieldId=='ict-abnno'){
            var trimmedrep = fieldVal.replace(" ", "").trim();
            var isInvalid = false;
            if(trimmedrep !== "" && (!isNaN(parseFloat(trimmedrep)) && isFinite(trimmedrep)) && trimmedrep.length === 11){
                var totalval = (Number(trimmedrep.substring(0,1)) - 1) * 10;
                totalval = totalval+(Number(trimmedrep.substring(2,1)) * 1);
                totalval = totalval+(Number(trimmedrep.substring(3,2)) * 3);
                totalval = totalval+(Number(trimmedrep.substring(4,3)) * 5);
                totalval = totalval+(Number(trimmedrep.substring(5,4)) * 7);
                totalval = totalval+(Number(trimmedrep.substring(6,5)) * 9);
                totalval = totalval+(Number(trimmedrep.substring(7,6)) * 11);
                totalval = totalval+(Number(trimmedrep.substring(8,7)) * 13);
                totalval = totalval+(Number(trimmedrep.substring(9,8)) * 15);
                totalval = totalval+(Number(trimmedrep.substring(10,9)) * 17);
                totalval = totalval+(Number(trimmedrep.substring(11,10)) * 19);
                if(totalval % 89 !== 0){
                    isInvalid = true;
                }
                console.log('fieldVal abn in');
            }else{
                isInvalid = true;
                console.log('fieldVal abn out');
            }
            if(isInvalid){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }else{
            	if($A.util.hasClass(errorSpanId,'help-block-error')){
                    $A.util.removeClass(errorSpanId,'help-block-error');
                    $A.util.removeClass(errorDivId,'has-error'); 
                    $A.util.addClass(errorDivId,'has-success'); 
                }
                $A.util.addClass(errorDivId,'has-success'); 
                $A.util.addClass(errorSpanId,'help-none-error');
            }
        }
        else if(fieldId=='ict-employeeno'){
            if(fieldVal == 'Please choose'){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
            else{
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error'); 
                $A.util.addClass(errorDivId,'has-success'); 
            }
        } 
        else if(fieldVal == ''){
            $A.util.removeClass(errorDivId,'has-success');
        }
        else{
            if($A.util.hasClass(errorSpanId,'help-block-error'))
            {
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error'); 
                $A.util.addClass(errorDivId,'has-success'); 
            }
            $A.util.addClass(errorDivId,'has-success'); 
            $A.util.addClass(errorSpanId,'help-none-error');
        }
    },
    
    saveAdditionalInformation:function(cmp, event, helper){ 
        console.log('---saveAdditionalInformation---');
        helper.validateFieldsOnSubmission(cmp, event, helper, true, true, false);
        
        console.log('--hasError--', cmp.get('v.hasError'));
        if(!cmp.get('v.hasError')){
            helper.processPreferences(cmp, event);
            if(!cmp.get('v.showErrorMsg')){
            	helper.processAdditionalInformation(cmp, event);     
            }
        }
    },
    
    navigateToAccreditationPage: function (component, event, helper) {
        helper.goToAccreditationPage(component, event, helper);
    }
})