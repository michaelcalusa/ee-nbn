({
	getUserInformation : function(component,event) {
		var action = component.get("c.getContactInformation"); 
        console.log('--getUserInformation--');
        // set a callBack    
        action.setCallback(this, function(response) {
            console.log('--response--' + response + '--status--' + response.getState());
            var state = response.getState();
            var storeResponse = response.getReturnValue();
            if (state === "SUCCESS") {
                
                if(storeResponse.LastName != undefined){
                    
                    component.set("v.businessAdviserId", storeResponse.Contact_ID__c);
                    component.set("v.accountId", storeResponse.Account.Account_ID__c);
                    
                    component.set("v.FirstName", storeResponse.FirstName);
                    component.set("v.LastName", storeResponse.LastName);
                    component.set("v.Email", storeResponse.Email);
                    component.set("v.Account", storeResponse.Account.Name);
                    component.set("v.TradingName", storeResponse.Trading_Name__c);
                    //MSEU-9621
                    component.set("v.Website", storeResponse.Account.Website);
                    component.set("v.Phone", storeResponse.Phone);
                    component.set("v.jobtitle", storeResponse.Job_Title__c);
                    
                    component.set("v.Employees", storeResponse.Number_of_Employees_in_your_company__c);
                    component.set("v.ABNNumber", storeResponse.ABN__c);
                    component.set("v.addressStreetName", storeResponse.MailingStreet);
                    component.set("v.addressSuburb", storeResponse.MailingCity);
                    component.set("v.addressState", storeResponse.MailingState);
                    component.set("v.addressPostalCode", storeResponse.MailingPostalCode);
                    component.set("v.roletype", storeResponse.Lead_Role__c);
					component.set("v.ICTContactRole", storeResponse.ICT_Contact_Role__c);
                    component.set("v.accreditationStatus", storeResponse.Account.Engagement_Status__c);
                    component.set("v.certifiedCount", storeResponse.Account.Certified_Count__c);
                    if(storeResponse.Accreditation__r) {
                        component.set("v.isContactFormSubmitter", true);
                    }else{
                        component.set("v.isContactFormSubmitter", false);
                    }
                    
                    
                    if(storeResponse.Lead_Role__c == ''){
                        component.set('v.roletype', 'Please choose');
                    }
                    if(storeResponse.Number_of_Employees_in_your_company__c == ''){
                        component.set('v.Employees', 'Please choose');
                    }
                }
            }
            else{

            }
            
            console.log('--storeResponse.MailingStreet--' + storeResponse.MailingStreet);
            if(storeResponse.MailingStreet != undefined){
                // Set the map for events
                var myMap = component.get("v.mapAddressInfoResults");
                myMap["StreetName"] = storeResponse.MailingStreet; 
                myMap["Suburb"] = storeResponse.MailingCity;
                myMap["State"] = storeResponse.MailingState;
                myMap["PostalCode"] = storeResponse.MailingPostalCode;
                
                component.set('v.mapAddressInfoResults',myMap);
                console.log('Address Map ==>'+ myMap);
                
                var myEvent = $A.get("e.c:GetAddressInfoEvent");                    
                myEvent.setParams({
                    "mapAddressResults":component.get("v.mapAddressInfoResults")
                });
                myEvent.fire();
            }
        });
        // enqueue the Action  
		
        //Get the Contact Roles
        var actionRole = component.get("c.roleOptions"); 
        console.log('--roleOptions--');
        // set a callBack    
        actionRole.setCallback(this, function(response) {
            console.log("role option text" +response.getReturnValue());
            console.log("get state" +response.getState());
            var stateRole = response.getState();
            
            if (stateRole === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log(storeResponse);
                component.set("v.roleOptList", storeResponse);
            }
            else{

            }
        });
        
        //Get the Employee picklist selection
        var actionEmployee = component.get("c.businessOptions"); 
        console.log('--businessOptions--');
        // set a callBack    
        actionEmployee.setCallback(this, function(response) {
            console.log("role option text" +response.getReturnValue());
            console.log("get state" +response.getState());
            var stateEmployee = response.getState();
            
            if (stateEmployee === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log(storeResponse);
                component.set("v.businessOptList", storeResponse);
            }
            else{

            }
        });
        
        //Get the Individual Preferences
        var actionIndividual = component.get("c.getICTPreferences"); 
        console.log('--getICTPreferences--');
        // set a callBack    
        actionIndividual.setCallback(this, function(response) {
            console.log("--Individual---" +response.getReturnValue());
            console.log("--Individual get state--" +response.getState());
            var stateIndividual = response.getState();
            
            if (stateIndividual === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('---storeResponse---' + storeResponse);
                component.set("v.IndividualWrapper", storeResponse);
                
                console.log('---individual id---' + storeResponse.pID);
                if(storeResponse.pID != undefined){
                	component.set("v.prefRecordID", storeResponse.pID);
                    component.set("v.prefBusinessNews", storeResponse.newsUpdates);
                    component.set("v.prefBusinessProducts", storeResponse.productInfo);
                    component.set("v.prefProgramUpdates", storeResponse.ictPrograms);
                }
                else{
                    console.log('--No Individual record found--');
                }
            }
        });
        
        // enqueue the Action  
        $A.enqueueAction(actionRole);
        $A.enqueueAction(actionEmployee);
        $A.enqueueAction(actionIndividual);
        $A.enqueueAction(action);
	}, 

    validateAddressFields : function(cmp, event, helper){
        console.log('validateAddressFields');
        var div = document.getElementById("ict-containerId");
        var subDiv = div.getElementsByTagName('input');
        var myArray = [];
        // street address add to the array
        myArray.push('ict-streetAddress');
        // add state to the array
        myArray.push('ict-state');        
        for(var i = 0; i < subDiv.length; i++) {
            var elem = subDiv[i];
            if(elem.id.indexOf('ict-') === 0) {
                myArray.push(elem.id);
                // console.log('element id==>'+elem.id);
            }
        }
        var errorFlag = false;
        for(var inputElementId=0; inputElementId<myArray.length; inputElementId++){
            var errorDivId = document.getElementById(myArray[inputElementId]+'-div');
            var errorSpanId = document.getElementById(myArray[inputElementId]+'-error'); 
            var fieldId = myArray[inputElementId];      
            var fieldVal;
            
            console.log('errorDivId :'+errorDivId+'== errorSpanId :'+errorSpanId );
			if(fieldId != 'ict-streetAddress'){
                console.log('fieldId :'+fieldId+'== fieldVal' );
                fieldVal = document.getElementById(fieldId).value;
            }
            
			if(fieldId=='ict-streetAddress' && cmp.get('v.addressStreetName') == undefined)
            { 
                $A.util.removeClass(errorDivId,'has-success');
            }
            else if(fieldId=='ict-townSuburb' && cmp.get('v.addressSuburb') == undefined)
            { 
                $A.util.removeClass(errorDivId,'has-success');
            }
            else if(fieldId=='ict-state' && cmp.get('v.addressState') == undefined)
            { 
                $A.util.removeClass(errorDivId,'has-success');
            }
            else if(fieldId=='ict-postcode' && cmp.get('v.addressPostalCode') == undefined)
            { 
                $A.util.removeClass(errorDivId,'has-success');
            }
            else if(fieldId=='ict-streetAddress' && cmp.get('v.addressStreetName') != undefined)
            { 
                $A.util.addClass(errorDivId,'has-success');
                $A.util.addClass(errorSpanId,'help-none-error');
            }
            else if(fieldId=='ict-townSuburb' && cmp.get('v.addressSuburb') != undefined)
            { 
                $A.util.addClass(errorDivId,'has-success');
                $A.util.addClass(errorSpanId,'help-none-error');
            }
            else if(fieldId=='ict-state' && cmp.get('v.addressState') != undefined)
            { 
                $A.util.addClass(errorDivId,'has-success');
                $A.util.addClass(errorSpanId,'help-none-error');
            }
            else if(fieldId=='ict-postcode' && cmp.get('v.addressPostalCode') != undefined)
            { 
                $A.util.addClass(errorDivId,'has-success');
                $A.util.addClass(errorSpanId,'help-none-error');
            }
        }
    },
    
    validateFieldsOnSubmission : function(cmp, event, helper, isPhone, isEmployeeNumber, isABNNumber){
        console.log('validateFieldOnSubmission');
        var div = document.getElementById("ict-containerId");
        var subDiv = div.getElementsByTagName('input');
        var myArray = [];
        var myInfoFieldError = false;
        
        
        // street address add to the array
        myArray.push('ict-streetAddress');
        // add state to the array
        myArray.push('ict-state');        
        myArray.push('ict-employeeno');
        myArray.push('ict-roletype');
        for(var i = 0; i < subDiv.length; i++) {
            var elem = subDiv[i];
            if(elem.id.indexOf('ict-') === 0) {
                myArray.push(elem.id);
                // console.log('element id==>'+elem.id);
            }
        }
        var errorFlag = false;
        for(var inputElementId=0; inputElementId<myArray.length; inputElementId++){
            var errorDivId = document.getElementById(myArray[inputElementId]+'-div');
            var errorSpanId = document.getElementById(myArray[inputElementId]+'-error'); 
            var fieldId = myArray[inputElementId];      
            var fieldVal;
            
            console.log('errorDivId :'+errorDivId+'== errorSpanId :'+errorSpanId );
			if(fieldId != 'ict-streetAddress'){
                console.log('fieldId :'+fieldId+'== fieldVal' );
                fieldVal = document.getElementById(fieldId).value;
            }
            
            if(fieldId=='ict-phoneno' && (fieldVal=='' || !fieldVal.match(/^\d{10}$/))){
                if(isPhone){
                    // Form submission
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorFlag = true;
                    
                    
                    if(document.getElementsByClassName('myinfocolumn')[0].getAttribute('aria-expanded') == 'false') {
                        document.getElementsByClassName('myinfocolumn')[0].click();
                    }
                    
                    
                    $A.util.addClass(errorSpanId,'help-block-error');
                    
                    console.log('** phoneno **' );
                    
                    
                }
                else{
                    // Do nothing - Address selection
                }
            }
            else if(fieldId=='ict-abnno'){
                var trimmedrep = fieldVal.replace(" ", "").trim();
                var isInvalid = false;
                if(trimmedrep !== "" && (!isNaN(parseFloat(trimmedrep)) && isFinite(trimmedrep)) && trimmedrep.length === 11){
                    var totalval = (Number(trimmedrep.substring(0,1)) - 1) * 10;
                    totalval = totalval+(Number(trimmedrep.substring(2,1)) * 1);
                    totalval = totalval+(Number(trimmedrep.substring(3,2)) * 3);
                    totalval = totalval+(Number(trimmedrep.substring(4,3)) * 5);
                    totalval = totalval+(Number(trimmedrep.substring(5,4)) * 7);
                    totalval = totalval+(Number(trimmedrep.substring(6,5)) * 9);
                    totalval = totalval+(Number(trimmedrep.substring(7,6)) * 11);
                    totalval = totalval+(Number(trimmedrep.substring(8,7)) * 13);
                    totalval = totalval+(Number(trimmedrep.substring(9,8)) * 15);
                    totalval = totalval+(Number(trimmedrep.substring(10,9)) * 17);
                    totalval = totalval+(Number(trimmedrep.substring(11,10)) * 19);
                    if(totalval % 89 !== 0){
                        isInvalid = true;
                        errorFlag = true;
                        if(document.getElementsByClassName('mycompanyinfocolumn')[0].getAttribute('aria-expanded') == 'false') {
                            document.getElementsByClassName('mycompanyinfocolumn')[0].click();
                        }
                    }
                    console.log('fieldVal abn in');
                }else{
                    isInvalid = true;
                    errorFlag = true;
                    
                    console.log('fieldVal abn out');
                    
                    if(document.getElementsByClassName('mycompanyinfocolumn')[0].getAttribute('aria-expanded') == 'false') {
                        document.getElementsByClassName('mycompanyinfocolumn')[0].click();
                    }
                }
                if(isInvalid){
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    
                }else{
                    if($A.util.hasClass(errorSpanId,'help-block-error')){
                        $A.util.removeClass(errorSpanId,'help-block-error');
                        $A.util.removeClass(errorDivId,'has-error'); 
                        $A.util.addClass(errorDivId,'has-success');
                        
                    }
                    $A.util.addClass(errorDivId,'has-success'); 
                    $A.util.addClass(errorSpanId,'help-none-error');
                    
                }
            }/*
            else if(fieldId=='ict-streetAddress' && cmp.get('v.addressStreetName') == undefined)
            { 
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
            }
            else if(fieldId=='ict-townSuburb' && cmp.get('v.addressSuburb') == undefined)
            { 
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
            }
            else if(fieldId=='ict-state' && cmp.get('v.addressState') == undefined)
            { 
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
            }
            else if(fieldId=='ict-postcode' && cmp.get('v.addressPostalCode') == undefined)
            { 
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
            }*/
            //MSEU-9621
            else if(fieldId=='ict-jobtitle' && cmp.get('v.jobtitle') == undefined)
            { 
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                //errorFlag = true;
                console.log('** jobtitle **' );
            }
            else if(fieldId=='ict-roletype' && fieldVal == 'Please choose'){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
                
                console.log('** roletype **' );
                
                if(document.getElementsByClassName('myinfocolumn')[0].getAttribute('aria-expanded') == 'false') {
                    document.getElementsByClassName('myinfocolumn')[0].click();
                }
            }
			else if(fieldId=='ict-employeeno' && fieldVal == 'Please choose'){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
                
                console.log('** employeeno **' );
                
                if(document.getElementsByClassName('mycompanyinfocolumn')[0].getAttribute('aria-expanded') == 'false') {
                    document.getElementsByClassName('mycompanyinfocolumn')[0].click();
                }
            }
            else
            {
                console.log('else case - ' + fieldId);
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error');
                $A.util.addClass(errorDivId,'has-success');
                $A.util.addClass(errorSpanId,'help-none-error');
            }
        }
      
		if(errorFlag == true){
            cmp.set('v.hasError', true);
            
             
        }
        else if(errorFlag == false){
            cmp.set('v.hasError', false);
        }
    },
    processAdditionalInformation : function(component,event) {
        console.log('--into process additional information helper--');
        var phone = document.getElementById('ict-phoneno').value; 
        console.log('--Phone--' + phone);
        var ABNNumber = document.getElementById('ict-abnno').value;
        console.log('--ABNNumber--' + ABNNumber);
        var employeeCount = document.getElementById('ict-employeeno').value;
        console.log('--EmployeeCount--' + employeeCount);
        var streetName = component.get("v.addressStreetName"); 
        console.log('--streetName--' + streetName);
        var suburb = component.get("v.addressSuburb");
        console.log('--suburb--' + suburb);
        var state = component.get("v.addressState"); 
        console.log('--state--' + state);
        var postalCode = component.get("v.addressPostalCode");
        console.log('--postalCode--'+ postalCode);
        //MSEU-9621
        var roletype = document.getElementById("ict-roletype").value; 
        console.log('--roletype--'+ roletype);
        var jobtitle = document.getElementById("ict-jobtitle").value;
        console.log('--jobtitle--' + jobtitle);
		var tradingName = document.getElementById("ict-tradingname").value; 
        console.log('--tradingName--'+ tradingName);        
        
        console.log('--Phone--' + phone);
        console.log('--ABNNumber--' + ABNNumber);
        console.log('--EmployeeCount--' + employeeCount);
        console.log('--streetName--' + streetName);
        console.log('--suburb--' + suburb);
        console.log('--state--' + state);
        console.log('--postalCode--'+ postalCode);
        //MSEU-9621
        console.log('--jobtitle--' + jobtitle);
        console.log('--roletype--'+ roletype);
		var action = component.get("c.processAdditionalInformation");
        action.setParams({
            'strPhone': phone,
            'strABNNumber': ABNNumber,
            'strEmployeeCount': employeeCount,
            'strStreetName': streetName,
            'strSuburb': suburb,
            'strState': state,
            'strPostalcode': postalCode,
            //MSEU-9621
            'strRoleType': roletype,
            'strJobTitle': jobtitle,
            'strTradingName' : tradingName
        });
        
        console.log('--processAdditionalInformation--');
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('--processAdditionalInformation storeResponse--' + storeResponse);
                if(storeResponse){
                    component.set("v.showInformationSection", false);
                    component.set("v.showSuccessMsg", true);                    
                    component.set("v.showErrorMsg", false);                    
                }
                else{
                    component.set("v.showInformationSection", true);
                    component.set("v.showSuccessMsg", false);  
                    component.set("v.showErrorMsg", true);
                }
            }
            else{
                component.set("v.showInformationSection", true);
				component.set("v.showSuccessMsg", false);
                component.set("v.showErrorMsg", false);
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
	},
    
    goToAccreditationPage: function (component, event, helper) {
        
        var navigationLightningEvent = $A.get("e.force:navigateToURL");
        navigationLightningEvent.setParams({
          "url": "/accreditation-information",
            "isredirect" : true
         });
        navigationLightningEvent.fire();
        
    },
    
    processPreferences : function(component,event) {
        console.log('--processPreferences helper starts here--');
        var recId = component.get("v.prefRecordID");
        console.log('----recId------' + recId);
		var prefNews = component.find("idPrefNews").get("v.value");
		console.log('----prefNews------' + prefNews);

        var prefProducts = component.find("idPrefProducts").get("v.value");
		console.log('----prefProducts------' + prefProducts);

        var prefPrograms = component.find("idPrefPrograms").get("v.value");
		console.log('----prefPrograms------' + prefPrograms);
         
        var action = component.get("c.processICTPreferences");
        action.setParams({
            'strPID': recId,
            'strNewsUpdates': prefNews,
            'strProductInfo': prefProducts,
            'strICTPrograms': prefPrograms
        });
        
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('--processICTPreferences storeResponse--' + storeResponse);
                if(storeResponse){
                  console.log('---Preferences saved - do nothing---');
                }
                else{
                    component.set("v.showInformationSection", true);
                    component.set("v.showSuccessMsg", false);  
                    component.set("v.showErrorMsg", true);
                }
            }
            else{
                component.set("v.showInformationSection", true);
				component.set("v.showSuccessMsg", false);
                component.set("v.showErrorMsg", false);
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
        
        console.log('--processPreferences helper ends here--');
    }
})