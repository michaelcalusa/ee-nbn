({
	GoogleMapSearch : function(component, event, helper) {
		var AddressTerm = document.getElementById("searchAddTextId").value;
        var resultLimit = component.get("v.AddressLimit");
        var indexTypeAhead = component.get("v.AddressTypeAhead");
        console.log('*** length'+ AddressTerm);
        
        if( AddressTerm.length > indexTypeAhead ){
            helper.searchGoogleMapHelper(component, event, AddressTerm, resultLimit);
        }
        else{  
            component.set("v.listOfSearchRecords", null); 

            var forclose = component.find("typeahead-00B-9722");
            $A.util.addClass(forclose, 'lookup_menu_close');
            $A.util.removeClass(forclose, 'lookup_menu_open');
        }   
	},
    
    selectAddress : function(component, event, helper) {
        var PlaceID = event.currentTarget.dataset.placeid;
        
        helper.getAddressDetails(component, event, PlaceID);

        var forclose = component.find("typeahead-00B-9722");
        $A.util.addClass(forclose, 'lookup_menu_close');
        $A.util.removeClass(forclose, 'lookup_menu_open');
    },
    
    addressInfo : function(component, event, helper) {
		var mapAddressResult = event.getParam('mapAddressResults');
        
        var streetName = mapAddressResult["StreetName"]; 
        
        console.log('streetName'+streetName);

        component.set("v.Address", streetName);
    },
    
    validateFields : function(cmp, event, helper){
        console.log('field validation');
        console.log('event div==>'+ event.currentTarget.dataset.errordivid);
        console.log('event id==>'+ event.currentTarget.id);
        
        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);       
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;
        
        var fieldId = event.currentTarget.id;       
        var fieldVal = document.getElementById(fieldId).value;
        
        console.log('errorDivId' + errorDivId);
        console.log('errorSpanId' + errorSpanId);
        console.log('errorMsg' + errorMsg);
        console.log('fieldId' + fieldId);
        console.log('fieldVal' + fieldVal);
        
        if(fieldVal == ''){
            $A.util.removeClass(errorSpanId,'help-none-error');
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else{
            if($A.util.hasClass(errorSpanId,'help-block-error'))
            {
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error'); 
                $A.util.addClass(errorDivId,'has-success'); 
            }
            $A.util.addClass(errorDivId,'has-success'); 
        }
    }
})