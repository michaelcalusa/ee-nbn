({
	searchGoogleMapHelper : function(component,event,getInputkeyWord,resultLimit) {
		var action = component.get("c.searchGoogleMap"); 
        action.setParams({
            'strAddressTerm': getInputkeyWord,
            'intAddressLimit': resultLimit
        });
        
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.listOfSearchRecords", storeResponse);
                
                if(storeResponse.length == 0){
        			var forclose = component.find("typeahead-00B-9722");
                    $A.util.addClass(forclose, 'lookup_menu_close');
                    $A.util.removeClass(forclose, 'lookup_menu_open');
                    
                    // Set the map for events
                    var myMap = component.get("v.mapAddressResults");
                    myMap["StreetName"] = getInputkeyWord; 
                    myMap["Suburb"] = '';
                    myMap["State"] = '';
                    myMap["PostalCode"] = '';
                    
                    component.set('v.mapAddressResults',myMap);
                    console.log('ABN Map ==>'+ myMap);
                    
					var myEvent = $A.get("e.c:GetAddressInfoEvent");                    
                    myEvent.setParams({
                        "mapAddressResults":component.get("v.mapAddressResults")
                    });
                    myEvent.fire();
                }
                else{
                    var findTypeahead = component.find("typeahead-00B-9722");
            		$A.util.addClass(findTypeahead, 'lookup_menu_open');
            		$A.util.removeClass(findTypeahead, 'lookup_menu_close');
                }
                /*
                console.log(storeResponse[0].strAddress);
                var noAddressFound = storeResponse[0].strAddress;
                if (noAddressFound == 'No Address Found'){
                    component.set("v.listOfSearchRecords", storeResponse);
                    var myEvent = $A.get("e.c:GetAddressInfoEvent");                    
                    myEvent.setParams({
                        "StreetName":getInputkeyWord,
                        "Suburb":'',
                        "State":'',
                        "PostalCode":''
                    });
                    myEvent.fire();
                }
                */
            }  
        });
        // enqueue the Action  
        $A.enqueueAction(action);
	},
    getAddressDetails : function(component,event,getPlaceId) {
		var action = component.get("c.getAddressDetails"); 
        action.setParams({
            'strPlaceID': getPlaceId
        });
        
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                component.set("v.Address", storeResponse.strStreet);
                
                // Set the map for events
                var myMap = component.get("v.mapAddressResults");
                myMap["StreetName"] = storeResponse.strStreet; 
                myMap["Suburb"] = storeResponse.strSuburb;
                myMap["State"] = storeResponse.strState;
                myMap["PostalCode"] = storeResponse.strPostalCode;
                myMap["showValidation"] = 'NoValidation';
                
                component.set('v.mapAddressResults',myMap);
                
                var myEvent = $A.get("e.c:GetAddressInfoEvent");
                
                myEvent.setParams({
                    "mapAddressResults":component.get("v.mapAddressResults")
                });
                myEvent.fire();
            }     
        });
        
        // enqueue the Action  
        $A.enqueueAction(action);
	}
})