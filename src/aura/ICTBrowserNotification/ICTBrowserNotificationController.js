({
	init : function(component, event, helper) {
		var browserwindow = navigator.userAgent;
        console.log('--browser information--' + browserwindow);
        if(browserwindow.toLowerCase().indexOf('chrome') == -1){
            component.set("v.isChrome", true);
        }
        else{
            component.set("v.isChrome", false);
        }
	}
})