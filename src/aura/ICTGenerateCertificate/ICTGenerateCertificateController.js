({
    initialize: function(component, event, helper) {
        var action = component.get("c.retCertificates");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('----storeResponse---' + storeResponse);
                component.set("v.LearningRecords", storeResponse);
                if(storeResponse != null){
                    component.set("v.tstTkrName", storeResponse[0].cntctName);
                    component.set("v.showNoCert", false);
                    component.set("v.showCert", true);
                }
                else{
                    component.set("v.showNoCert", true);
                    component.set("v.showCert", false);
                }
            }
            else{
            }
        });
        $A.enqueueAction(action);
    },
    
    openModal: function(component, event) {
        var cmpldat = $A.localizationService.formatDate(event.currentTarget.dataset.compdat, "DD/MM/YYYY");
        component.set("v.certName", event.currentTarget.dataset.certnam);
        component.set("v.adviserID", event.currentTarget.dataset.advid);
        component.set("v.cmpltDate", $A.localizationService.formatDate(event.currentTarget.dataset.compdat, "DD/MM/YYYY"));
        component.set("v.isOpen", true);
    },
    
    closeModal: function(component, event, helper) {
        component.set("v.isOpen", false);
        parent.location.reload();
    },
    
    printCert: function(component, event, helper) {
		var prtContent = component.find("printablearea");
        var WinPrint = window.open('/ictpartnerprogram/s/certificate?tuser='+component.get("v.tstTkrName")+'&tcdate='+component.get("v.cmpltDate")+'&advID='+component.get("v.adviserID"), '', 'left=0,top=0,width='+parseInt(window.innerWidth)+',height='+parseInt(window.innerHeight)+',toolbar=0,scrollbars=0,status=0,layout=Landscape');
    },
})