({
    doInit: function(component, event, helper) {   
        // Get the Opportunity Phase picklist selection
        var actionPhase = component.get("c.leadViewOptions"); 
        console.log('--leadViewOptions--');
        // set a callBack    
        actionPhase.setCallback(this, function(response) {
            console.log("--phase option response--" + response.getReturnValue());
            console.log("--phase option response status--" + response.getState());
            var phaseStatus = response.getState();
            
            if (phaseStatus === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.lisviewList", storeResponse);
            }
        });
        
        // enqueue the Action  
        $A.enqueueAction(actionPhase);
        
        
        // Get the URL parameter
        var url = window.location.href;
        var leadViewID;
        var AllView = component.get('{!v.AllListView}');
        var MyView = component.get('{!v.MyListView}');
        
        console.log('****getURLParametersHelper****' + url);
		console.log('****index****' + url.indexOf(component.get('{!v.paramLeadViewId}')));
        // Verify that component is accessed from end user only
        if(url.indexOf(component.get('{!v.paramLeadViewId}')) !== -1){
			// Fetch Contact ID
            var regexFN = new RegExp("[?&]" + component.get('{!v.paramLeadViewId}') + "(=([^&#]*)|&|#|$)");
            var resultsFN = regexFN.exec(url);
            leadViewID = decodeURIComponent(resultsFN[2].replace(/\+/g, " "));
			
            console.log('****leadViewID****' + leadViewID);
            if(leadViewID == '1'){
                component.set("v.listviewtype", AllView);
            }
            else if(leadViewID == '2'){
                component.set("v.listviewtype", MyView);
            }
            else{
                component.set("v.listviewtype", AllView);
            }
            
        }  
        else{
            component.set("v.listviewtype", AllView);
        }
		console.log('****listviewtype****' + component.get('{!v.listviewtype}'));
    },
    
    selectedListView : function(component, event, helper) {
        console.log('---selectedListView---');
		//console.log('--event id--'+ event.currentTarget.id);
		var navurl;
        var fieldVal = document.getElementById(event.currentTarget.id).value;
        
        var AllView = component.get('{!v.AllListView}');
        var MyView = component.get('{!v.MyListView}');
        
        if(fieldVal == AllView){
            navurl = "/leads?vid=1";
            component.set("v.listviewtype", AllView);
        }
        else if(fieldVal == MyView){
            navurl = "/leads?vid=2"
            component.set("v.listviewtype", MyView);
        }
        
        console.log('--navurl--' + navurl);
        //window.location.href = navurl;
 		var navigationLightningEvent = $A.get("e.force:navigateToURL");
        navigationLightningEvent.setParams({
            "url": navurl,
            "isredirect" : true
        });
        navigationLightningEvent.fire();
	},
    onRender : function(component, event, helper) {
        document.getElementById('ict-listviewtype').value = component.get('{!v.listviewtype}');
    }
})