({
    doInit: function(component, event, helper) {   
        // Get the Opportunity Phase picklist selection
        var actionPhase = component.get("c.oppViewOptions"); 
        console.log('--oppViewOptions--');
        // set a callBack    
        actionPhase.setCallback(this, function(response) {
            console.log("--phase option response--" + response.getReturnValue());
            console.log("--phase option response status--" + response.getState());
            var phaseStatus = response.getState();
            
            if (phaseStatus === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.lisviewList", storeResponse);
            }
        });
        
        // enqueue the Action  
        $A.enqueueAction(actionPhase);
        
        
        // Get the URL parameter
        var url = window.location.href;
        var oppViewID;
        var AllView = component.get('{!v.AllListView}');
        var MyView = component.get('{!v.MyListView}');
        
        console.log('****getURLParametersHelper****' + url);
		console.log('****index****' + url.indexOf(component.get('{!v.paramOppViewId}')));
        // Verify that component is accessed from end user only
        if(url.indexOf(component.get('{!v.paramOppViewId}')) !== -1){
			// Fetch Contact ID
            var regexFN = new RegExp("[?&]" + component.get('{!v.paramOppViewId}') + "(=([^&#]*)|&|#|$)");
            var resultsFN = regexFN.exec(url);
            oppViewID = decodeURIComponent(resultsFN[2].replace(/\+/g, " "));
			
            console.log('****oppViewID****' + oppViewID);
            if(oppViewID == '1'){
                component.set("v.listviewtype", AllView);
            }
            else if(oppViewID == '2'){
                component.set("v.listviewtype", MyView);
            }
            else{
                component.set("v.listviewtype", AllView);
            }
            
        }  
        else{
            component.set("v.listviewtype", AllView);
        }
		console.log('****listviewtype****' + component.get('{!v.listviewtype}'));
    },
    
    selectedListView : function(component, event, helper) {
        console.log('---selectedListView---');
		//console.log('--event id--'+ event.currentTarget.id);
		var navurl;
        var fieldVal = document.getElementById(event.currentTarget.id).value;
        
        var AllView = component.get('{!v.AllListView}');
        var MyView = component.get('{!v.MyListView}');
        
        if(fieldVal == AllView){
            navurl = "/opportunities?vid=1";
            component.set("v.listviewtype", AllView);
        }
        else if(fieldVal == MyView){
            navurl = "/opportunities?vid=2"
            component.set("v.listviewtype", MyView);
        }
        
        console.log('--navurl--' + navurl);
        //window.location.href = navurl;
 		var navigationLightningEvent = $A.get("e.force:navigateToURL");
        navigationLightningEvent.setParams({
            "url": navurl,
            "isredirect" : true
        });
        navigationLightningEvent.fire();
	},
    onRender : function(component, event, helper) {
        document.getElementById('ict-listviewtype').value = component.get('{!v.listviewtype}');
    },
    createOpportunity : function(component, event, helper) {
        /*
        var navigationLightningEvent = $A.get("e.force:navigateToURL");
        navigationLightningEvent.setParams({
            "url": "/opportunity-view",
            "isredirect" : true
        });
        navigationLightningEvent.fire();
        */
        window.location.href = $A.get("$Label.c.ICT_Community_URL") + "ictpartnerprogram/s/opportunity-view";
    }
})