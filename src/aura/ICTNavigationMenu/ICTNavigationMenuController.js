({
    init : function(component, event, helper) {  
        helper.identifyUserAccess(component, event);
		helper.getCertifiedStatus(component, event);
    },
    
    mouseOverEvent: function(component, event, helper) {
        //component.set("c.showInfoSubMenu", false);
    },
    
    onClick : function(component, event, helper) {
        var id = event.target.dataset.menuItemId;
        if (id) {
            component.getSuper().navigate(id);
         }
    },
    
    onMenuClick : function(component, event, helper) {
        var menudiv = document.getElementById("ict-menuid");
        
        if(menudiv.outerHTML.indexOf('menuClass') > 0){
            $A.util.addClass(menudiv,'in');
            $A.util.removeClass(menudiv,'menuClass');
        }
        else{
            $A.util.removeClass(menudiv,'in');
            $A.util.addClass(menudiv,'menuClass');
        }
    }
})