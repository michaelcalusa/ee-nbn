({
    identifyUserAccess : function(component,event) {
        console.log('--identifyUserAccess--');
        
		var action = component.get("c.identifyAccessLevel"); 
        action.setParams({
            'strURL': window.location.href
        });

        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('--identifyUserAccess storeResponse--' + storeResponse);
                if(!storeResponse){
                    var navigationLightningEvent = $A.get("e.force:navigateToURL");
                    navigationLightningEvent.setParams({
                        "url": "/errorpage",
                        "isredirect" : true
                    });
                    navigationLightningEvent.fire();  
                }
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);        
    },
    
	getCertifiedStatus : function(component,event) {
		var action = component.get("c.checkCertifiedUser"); 
        console.log('--getCertifiedStatus--');
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.isCertifiedUser", storeResponse.isCertifiedUser);
                component.set("v.isPartnerCommunityUser", storeResponse.isPartnerCommunityUser);
                component.set("v.isSalesDelegateUser", storeResponse.isSalesDelegateUser);
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
	}
})