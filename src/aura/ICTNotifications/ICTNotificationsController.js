({
	init : function(component, event, helper) {
		var action = component.get("c.userNotifications");

        action.setCallback(this, function(a){
			var response = a.getReturnValue();
            console.log('--notification--response--' + response);
            if (response) {
                component.set("v.hasNotification", true);
            }
            else{
                component.set("v.hasNotification", false);
            }
        });
    	$A.enqueueAction(action);
	}
})