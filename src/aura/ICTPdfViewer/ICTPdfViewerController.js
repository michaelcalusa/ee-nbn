({
    doInit : function(component, event, helper) {

		helper.getPDFThumbnail(component, event);
	},
    
    openSingleFile: function(component, event, helper) {
        $A.get('e.lightning:openFiles').fire({
            recordIds: [component.get("v.currentContentDocumentId")]
        });
	}

})