({
    getPDFThumbnail : function(component, event) {
        
        //This is a slesforce standard URL to get thumbnail of any PDF file.
        var imageURL = '/ictpartnerprogram/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&operationContext=CHATTER&page=0&versionId=';
        
        var action = component.get("c.getLatestContentVersion");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var versionAndDocumentId = response.getReturnValue().split('-');
                imageURL = imageURL + versionAndDocumentId[0];
                
                component.set("v.currentContentDocumentId", versionAndDocumentId[1]);
                component.set("v.documentTitle", versionAndDocumentId[2]);
                component.set("v.thumbnailURL", imageURL);
 
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                    
            }
       
        });
        
        $A.enqueueAction(action);

    }
    
})