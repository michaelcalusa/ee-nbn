({
    initialize : function(component, event, helper) {
		helper.getURLParametersHelper(component , event, helper);
	},
	doneRendering : function(component, event, helper) {
        var windowURL = window.location.href;
        console.log('--windowURL--' + windowURL);
        
        if(windowURL.toLowerCase().indexOf('/s/certificate') != -1){
            
            window.print();
        }
        else{
            component.set("v.isCertPrint", false);
        }
	}
})