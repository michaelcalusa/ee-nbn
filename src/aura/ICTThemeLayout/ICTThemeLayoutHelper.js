({
   getURLParametersHelper: function(component, event, helpler) {
        //name = name.replace(/[\[\]]/g, "\\$&");
        var url = window.location.href;
        var contactName, completedDate, adviserID;
        console.log('****getURLParametersHelper****' + url);
        
   		// Verify that component is accessed from end user only
        if(url.indexOf(component.get('{!v.paramContact}')) !== -1){
			// Fetch contactName
            var regexFN = new RegExp("[?&]" + component.get('{!v.paramContact}') + "(=([^&#]*)|&|#|$)");
            var resultsFN = regexFN.exec(url);
            contactName = decodeURIComponent(resultsFN[2].replace(/\+/g, " "));

            // Fetch completedDate
            var regexFN = new RegExp("[?&]" + component.get('{!v.paramCompletionDate}') + "(=([^&#]*)|&|#|$)");
            var resultsFN = regexFN.exec(url);
            completedDate = decodeURIComponent(resultsFN[2].replace(/\+/g, " "));
            
            // Fetch completedDate
            var regexFN = new RegExp("[?&]" + component.get('{!v.paramAdviserID}') + "(=([^&#]*)|&|#|$)");
            var resultsFN = regexFN.exec(url);
            adviserID = decodeURIComponent(resultsFN[2].replace(/\+/g, " "));

            component.set("v.certTestTakerName", contactName);
            component.set("v.certCompletedDate", completedDate);
            component.set("v.certAdviserID", adviserID);
            console.log('****contactName****' + contactName);
            console.log('****completedDate****' + completedDate);
        }
    }
})