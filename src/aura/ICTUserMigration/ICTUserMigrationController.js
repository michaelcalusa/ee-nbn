({
	doInit: function(component, event, helper) {           
        helper.fetchContactStatus(component, event, helper);   
    },
    
    onCheck: function(component, event, helper) {           
        var userConfirmation = component.find("idConfirmUser").get("v.value");
        console.log('----onCheck userConfirmation------' + userConfirmation);
        if(userConfirmation){
            component.set("v.isValidate", false);
        }
    },
  
	deactivateCCPUser: function(component, event, helper) { 
        var userConfirmation = component.find("idConfirmUser").get("v.value");
		console.log('----userConfirmation------' + userConfirmation);
        if(userConfirmation){
            component.set("v.btndisabledDU", true);
            component.set("v.isValidate", false);
            helper.processCCPUSer(component, event, helper);   
        }
        else{
            component.set("v.isValidate", true);
        }
    },
    
    createPCUser: function(component, event, helper) { 
        component.set("v.btndisabledCPU", true);
        helper.createPartnerUSer(component, event, helper); 
    },
    
    migrateCFSRecords: function(component, event, helper) { 
        component.set("v.btndisabledCFS", true);
        helper.migrateCFSData(component, event, helper);   
    },
    
    finishProcess: function(component, event, helper) {    
        helper.reloadContactPage(component, event, helper);   
    }
})