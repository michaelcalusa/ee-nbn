({
	fetchContactStatus : function(component, event, helper) {
        console.log('--fetchContactStatus start--');
  		
        console.log('--recordId--' + component.get('v.recordId'));
        var action = component.get("c.getMigrateUserModel"); 
		action.setParams({
            'contactId': component.get('v.recordId')
        });
        
        // set a callBack    
        action.setCallback(this, function(response) {
            console.log("--response--" + response.getReturnValue());
            console.log("--response status--" + response.getState());
            var status = response.getState();
            
            if (status === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('--storeResponse end--' + storeResponse);
                
                console.log('--hasPermission--' + storeResponse.hasPermission);
                console.log('--isActivePCUserExist--' + storeResponse.isActivePCUserExist);
                console.log('--isActiveCCPUserExist--' + storeResponse.isActiveCCPUserExist);
                console.log('--isRelatedDataMigrationNeeded--' + storeResponse.isRelatedDataMigrationNeeded);
                console.log('--isInactiveCCPUserExist--' + storeResponse.isInactiveCCPUserExist);
                console.log('--isSalesDelegateRoleExist--' + storeResponse.isSalesDelegateRoleExist);
                console.log('--isInvalidContactId--' + storeResponse.isInvalidContactId);
                console.log('--isContactAdditionalInfoProvided--' + storeResponse.isContactAdditionalInfoProvided);
                console.log('--oldUserId--' + storeResponse.oldUserId);
                console.log('--newUserId--' + storeResponse.newUserId);
                console.log('--completedStep--' + storeResponse.completedStep);
                console.log('--nextStep--' + storeResponse.nextStep);
                console.log('--isUserFound--' + storeResponse.isUserFound);
                console.log('--portalUsers--' + storeResponse.portalUsers);
                console.log('--portalUsers-Username-' + storeResponse.portalUsers.Username);
                console.log('--portalUsers-isUserTranscriptsExist-' + storeResponse.portalUsers.isUserTranscriptsExist);
                console.log('--portalUsers-userTranscripts-' + storeResponse.portalUsers.userTranscripts);
                console.log('--portalUsers-isTrainingUserLicensesExist-' + storeResponse.portalUsers.isUserTranscriptsExist);
                console.log('--portalUsers-transcriptTrainingUserLicenses-' + storeResponse.portalUsers.transcriptTrainingUserLicenses);
                
                component.set('v.previousUserId', storeResponse.oldUserId);
                component.set('v.username', storeResponse.portalUsers.Username);
                // user found or not
                if(storeResponse.isUserFound){
                    if(storeResponse.isActivePCUserExist && storeResponse.nextStep == 'None'){
                        // if user already is Partner user
                        component.set('v.isFinished', true);
                        component.set('v.currentStep', '4');
                        component.set('v.showProgress', true);
                    }else{
                        component.set('v.isUserNotFound', false);
						if(storeResponse.hasPermission){
                            if(storeResponse.isSalesDelegateRoleExist){
                                if(storeResponse.isActiveCCPUserExist && !storeResponse.isActivePCUserExist && !storeResponse.isInactiveCCPUserExist){
                                    component.set('v.isCCPFound', true);
                                    component.set('v.username', storeResponse.portalUsers.Username);
                                    component.set('v.currentStep', '1');
                                    component.set('v.showProgress', true);
                                }
                                
                                // Show - CCP User is Inactive and PC is not found
                                if(storeResponse.isInactiveCCPUserExist && !storeResponse.isActivePCUserExist && !storeResponse.isActiveCCPUserExist){
                                    component.set('v.isCCPInactivated', true);
                                    component.set('v.currentStep', '2');
									component.set('v.showProgress', true);
                                }
                                
                                // Show - CCP User is Inactive and PC found and Data has not moved
                                if(storeResponse.nextStep == 'MigrateRelatedData'){
                                    component.set('v.isPCNoCFS', true);
                                    component.set('v.currentStep', '3');
                                    component.set('v.showProgress', true);
                                } 
                            }
                            else{
                                // show validation messages
                                component.set('v.isSalesDelegated', true);
                                component.set('v.showProgress', false);
                            } 
                        }
                       else{
                           // show validation messages
                           component.set('v.hasPermission', true);
                           component.set('v.showProgress', false);
                       } 
                    }
                }else{
                    component.set('v.isUserNotFound', true);
                    component.set('v.showProgress', false);
                }
            }
            else{
                component.set('v.showError', true);
                component.set('v.showProgress', false);
                
                component.set('v.isCCPFound', false);
                component.set('v.isCCPInactivated', false);
                component.set('v.isPCNoCFS', false);
                component.set('v.isFinished', false);
            }
        });
        
        // enqueue the Action  
        $A.enqueueAction(action);

        console.log('--fetchContactStatus end--');
	},
    
    processCCPUSer : function(component, event, helper) {
        console.log('--processCCPUSer start--');
        var action = component.get("c.disableUserByContactId"); 
		action.setParams({
            'contactId': component.get('v.recordId')
        });
        
        // set a callBack    
        action.setCallback(this, function(response) {
            console.log("--response--" + response.getReturnValue());
            console.log("--response status--" + response.getState());
            var status = response.getState();
            
            if (status === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('--storeResponse end--' + storeResponse);
				
                console.log('--success--' + storeResponse.success);
                console.log('--message--' + storeResponse.message);
                console.log('--whatId--' + storeResponse.whatId);
                console.log('--newRecordId--' + storeResponse.newRecordId);
                
                if(storeResponse.success){
                    component.set('v.isCCPFound', false);
                    component.set('v.isCCPInactivated', false);
                    component.set('v.isPCNoCFS', false);
                    component.set('v.isCCPFound', false);
                    component.set('v.isCreatePCUser', true);
                    component.set('v.currentStep', '2');
                }  
            }
            else{
                component.set('v.showError', true);
                component.set('v.showProgress', false);
                
                component.set('v.isCCPFound', false);
                component.set('v.isCCPInactivated', false);
                component.set('v.isPCNoCFS', false);
                component.set('v.isFinished', false);
            }
        });
        
        // enqueue the Action  
        $A.enqueueAction(action);        
        console.log('--processCCPUSer end--');
    },
    
    createPartnerUSer : function(component, event, helper) {
        console.log('--createPartnerUSer start--');
        var action = component.get("c.createUserByContactId"); 
		action.setParams({
            'contactId': component.get('v.recordId')
        });
        
        // set a callBack    
        action.setCallback(this, function(response) {
            console.log("--response--" + response.getReturnValue());
            console.log("--response status--" + response.getState());
            var status = response.getState();
            
            if (status === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('--storeResponse end--' + storeResponse);
				
                console.log('--success--' + storeResponse.success);
                console.log('--message--' + storeResponse.message);
                console.log('--whatId--' + storeResponse.whatId);
                console.log('--newRecordId--' + storeResponse.newRecordId);
                
                if(storeResponse.success){
                    component.set('v.isCCPFound', false);
                    component.set('v.isCCPInactivated', false);
                    component.set('v.isPCNoCFS', false);
                    component.set('v.isCreatePCUser', false);
                    component.set('v.isCFSMigration', true);
                    
                    component.set('v.previousUserId', storeResponse.whatId);
                    component.set('v.currentStep', '3');
                }  
            }
            else{
                component.set('v.showError', true);
                component.set('v.showProgress', false);
                
                component.set('v.isCCPFound', false);
                component.set('v.isCCPInactivated', false);
                component.set('v.isPCNoCFS', false);
                component.set('v.isFinished', false);
            }
        });
        
        // enqueue the Action  
        $A.enqueueAction(action);        
        console.log('--createPartnerUSer end--');
    },
    
    migrateCFSData : function(component, event, helper) {
        console.log('--createPartnerUSer start--');
        console.log('--old user id sent--' + component.get('v.previousUserId'));
        var action = component.get("c.migrateTranscriptsWithOldUserId"); 
		action.setParams({
            'oldUserId': component.get('v.previousUserId')
        });
        
        // set a callBack    
        action.setCallback(this, function(response) {
            console.log("--response--" + response.getReturnValue());
            console.log("--response status--" + response.getState());
            var status = response.getState();
            
            if (status === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('--storeResponse end--' + storeResponse);
				
                console.log('--success--' + storeResponse.success);
                console.log('--message--' + storeResponse.message);
                console.log('--whatId--' + storeResponse.whatId);
                console.log('--newRecordId--' + storeResponse.newRecordId);
                
                if(storeResponse.success){
                    component.set('v.isCCPFound', false);
                    component.set('v.isCCPInactivated', false);
                    component.set('v.isPCNoCFS', false);
                    component.set('v.isCreatePCUser', false);
                    component.set('v.isCFSMigration', false);
                    component.set('v.isFinished', true);
                    component.set('v.showFinish', true);
                    component.set('v.previousUserId', storeResponse.whatId);
                    component.set('v.currentStep', '4');
                }  
            }
            else{
                component.set('v.showError', true);
                component.set('v.showProgress', false);
                
                component.set('v.isCCPFound', false);
                component.set('v.isCCPInactivated', false);
                component.set('v.isPCNoCFS', false);
                component.set('v.isFinished', false);
            }
        });
        
        // enqueue the Action  
        $A.enqueueAction(action);        
        console.log('--createPartnerUSer end--');        
    },
    
    reloadContactPage : function(component, event, helper) {
        console.log('--reloadContactPage start--'); 
        
        var contactRecordId = component.get('v.recordId');
        console.log('--contactRecordId--' + contactRecordId);
		var navigationLightningEvent = $A.get("e.force:navigateToURL");
        navigationLightningEvent.setParams({
            "url": "/" + contactRecordId,
            "isredirect" : true
        });
        navigationLightningEvent.fire();  
        
        console.log('--reloadContactPage end--'); 
    }
})