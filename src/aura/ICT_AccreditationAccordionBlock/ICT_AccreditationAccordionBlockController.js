({
    validateFields : function(cmp, event, helper){
        console.log('field validation');
        console.log('event div==>'+ event.currentTarget.dataset.errordivid);
        console.log('event id==>'+ event.currentTarget.id);
        
        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);     
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;
        
        var fieldId = event.currentTarget.id;
        var fieldVal = document.getElementById(fieldId).value;
        
        console.log('errorDivId' + errorDivId);
        console.log('errorSpanId' + errorSpanId);
        console.log('errorMsg' + errorMsg);
        console.log('fieldId' + fieldId);
        console.log('fieldVal' + fieldVal);
        
        console.log('field index ict-phoneno' + fieldId.indexOf('ict-phoneno'));
        console.log('field index ict-email' + fieldId.indexOf('ict-email'));
        
        if(fieldId.indexOf('ict-phoneno') > -1 && !fieldVal.match(/^\d{10}$/)){
            console.log('-----Phone----');
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldId.indexOf('ict-email') > -1 && !fieldVal.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
            console.log('-----email----');
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldVal == ''){
            console.log('-----Empty----' + errorDivId);
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else{
            console.log('-----Else----' + errorSpanId);
            if($A.util.hasClass(errorSpanId,'help-block-error'))
            {
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error'); 
                $A.util.addClass(errorDivId,'has-success'); 
            }
            $A.util.addClass(errorDivId,'has-success'); 
            $A.util.addClass(errorSpanId,'help-none-error');
        }
    },
    
    validateEmail : function(component, event, helper) {
        console.log('--event--' + event.getSource());
		var enteredEmailId = event.getSource().get('v.value');
        //Source of Email Validation Regex : http://sfdcmonkey.com/2017/04/11/email-validation-lightning-component/
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; 
        
        if(enteredEmailId == '' || enteredEmailId.match(emailRegex)) {
            event.getSource().set("v.errors", null);
            helper.notifyParentComponent(component, false);
        }else{
            event.getSource().set("v.errors", [{message:"Please enter correct Email Id"}]);
            helper.notifyParentComponent(component, true);
        }
	},
    
    validatePhone : function (component, event, helper) {
        var enteredPhone = event.getSource().get('v.value');
        if(enteredPhone == '' || enteredPhone.match(/^\d{10}$/)) {
            event.getSource().set("v.errors", null);
            helper.notifyParentComponent(component, false);
        }else{
            event.getSource().set("v.errors", [{message:"Please enter 10 digit valid mobile number"}]);
            helper.notifyParentComponent(component, true);
        }
    },
    
    closeOtherAccordions : function (component, event, helper) {
        var clickedBlocked = component.get("v.blockId");
        
        var anchorBlockA = document.getElementsByClassName("myinfocolumn-contactInfo1")[0];
        if(clickedBlocked != 'contactInfo1' && !anchorBlockA.classList.contains('collapsed')) {
            anchorBlockA.click();
        }
            
        var anchorBlockB = document.getElementsByClassName("myinfocolumn-contactInfo2")[0];
        if(clickedBlocked != 'contactInfo2' && !anchorBlockB.classList.contains('collapsed')) {
            anchorBlockB.click();
        }    
         
        var anchorBlockC = document.getElementsByClassName("myinfocolumn-contactInfo3")[0];
        if(clickedBlocked != 'contactInfo3' && !anchorBlockC.classList.contains('collapsed')) {
            anchorBlockC.click();
        }
    },
    
    handleCheckboxChange : function(component, event, helper) {
        //checkbox is checked
        //debugger;
        console.log('---handleCheckboxChange---' + event.getParam('value'));
        if(event.getParam('value') == "option0") {
            console.log('---first name--' + component.get("v.userObject").Contact.FirstName);
            if(component.get("v.userObject").Contact.FirstName != null){
            	component.set("v.firstName", component.get("v.userObject").Contact.FirstName);
                console.log('---first name-if-');
            }
            else{
                component.set("v.firstName", '');
                console.log('---first name-else-');
            }
            
            component.set("v.lastName", component.get("v.userObject").Contact.LastName);
            component.set("v.email", component.get("v.userObject").Contact.Email);
            
            console.log('---phone--' + component.get("v.userObject").Contact.Phone);
            if(component.get("v.userObject").Contact.Phone != null){
                component.set("v.phone", component.get("v.userObject").Contact.Phone);
            }
            else{
                component.set("v.phone", '');
            }
            
            console.log('---Job_Title__c--' + component.get("v.userObject").Contact.Job_Title__c);
            if(component.get("v.userObject").Contact.Job_Title__c != null){
                component.set("v.jobtitle", component.get("v.userObject").Contact.Job_Title__c);
            }
            else{
                component.set("v.jobtitle", '');
            }
            //component.set("v.recordMode", true);
        }else if(event.getParam('value') == "option2"){
            component.set("v.firstName", "");
            component.set("v.lastName", "");
            component.set("v.email", "");
            component.set("v.phone", "");
            component.set("v.jobtitle", "");
        }
        //Get Business Principal data 
        else if(event.getParam('value') == "option1"){ 
            console.log('--Business Principal--');
            var blockBP = component.get("v.blockTitleBP");
            var blockMD = component.get("v.blockTitleMD");
            var cp = component.find("Marketing delegate-ict-firstName");
            console.log('---bp--first--' + document.getElementById(blockBP + '-ict-firstName').value);
            console.log('------' + cp);
            component.set("v.firstName", document.getElementById(blockBP + '-ict-firstName').value);
            component.set("v.lastName", document.getElementById(blockBP + '-ict-lastname').value);
            component.set("v.email", document.getElementById(blockBP + '-ict-email').value);
            component.set("v.phone", document.getElementById(blockBP + '-ict-phoneno').value);
            component.set("v.jobtitle", document.getElementById(blockBP + '-ict-jobtitle').value);
            console.log('---bp--first--out');
        }
        //get Marketing Delegate data
          else if(event.getParam('value') == "option3"){
            console.log('--Marketing Delegate--');
            var blockMD = component.get("v.blockTitleMD");
            component.set("v.firstName", document.getElementById(blockMD + '-ict-firstName').value);
            component.set("v.lastName", document.getElementById(blockMD + '-ict-lastname').value);
            component.set("v.email", document.getElementById(blockMD + '-ict-email').value);
            component.set("v.phone", document.getElementById(blockMD + '-ict-phoneno').value);
            component.set("v.jobtitle", document.getElementById(blockMD + '-ict-jobtitle').value);
        }
        
        //event.stopPropogation();
    }
        
})