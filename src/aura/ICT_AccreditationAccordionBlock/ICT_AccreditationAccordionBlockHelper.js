({
	notifyParentComponent : function(component, isError) {
        
        var cmpEvent = component.getEvent("tellParentComponent");
        cmpEvent.setParams({
            "blockId" : component.get("v.blockId"),
            "isError" : isError
        });
        cmpEvent.fire();
    }
})