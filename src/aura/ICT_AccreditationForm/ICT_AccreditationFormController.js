({
    doInit: function(component, event, helper) {           
        helper.fetchAccreditationRecord(component, event, helper);   
        
    },
    
    
    reloadAccreditationRecord: function(component, event, helper) {      
        component.find("accreditationRecordCreator").reloadRecord();       
    },
    
    handleAccountRecordChange : function(component, event, helper) {
      debugger;  
    },
 
    
    handleDraftSave : function(component, event, helper) {		      
        helper.draftSaveAccreditationRecord(component, event, helper);               
    },
    
    
    handleSubmitForAccreditation : function (component, event, helper) {
        helper.submitForAccreditation(component, event, helper);
    },
    
    recordErrorBlocks : function(component, event, helper) {
    	helper.storeErrorAccordions(component, event, helper);
    	
	}
    

})