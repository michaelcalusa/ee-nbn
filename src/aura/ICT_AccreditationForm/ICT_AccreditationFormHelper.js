({
    fetchAccreditationRecord : function(component, event, helper) {
        console.log('---fetchAccreditationRecord---');
        
        var action = component.get("c.getAccreditationRecord");
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('---state---' + state);
            if (state === "SUCCESS") {
                
                var returnValue = JSON.parse(response.getReturnValue());
                component.set("v.currentContactId", returnValue.contactId);
                component.set("v.currentAccountId", returnValue.accountId);
                component.set("v.userDetails", returnValue.loggedInUserDetails);
                //Setting poller for Continuous poll to detect change in Account Status Values
                /*
                var interval = window.setInterval(
                    $A.getCallback(function() { 
                        helper.fetchAccountStatus(component,helper, returnValue.accountId);
                    }), 5000
                );
                component.set("v.setIntervalId", interval);
                */
                //Poller Setting finished
                
                
                //Setting Existing Accreditation Record - Draft one
                if(returnValue.accreditationRequestId == null) {
                    component.find("accreditationRecordCreator").getNewRecord(
                        "Accreditation_Request__c",
                        null,
                        true, 
                        $A.getCallback(function() {
                            var rec = component.get("v.accreditationRecord");
                            var error = component.get("v.accreditationRecordError");
                            if(error || (rec === null)) {
                                console.log("Error initializing record template: " + error);
                                return;
                            }
                            console.log("Record template initialized: " + rec.sobjectType);
                        })
                    );
                    
                    component.set("v.recordVisibilityMode", 'false');
                    component.set("v.accreditationButtonText", 'Submit for accreditation');
                    
                }else{
                    
                    console.log('--record id response----' + returnValue.accreditationRequestId);
                    component.set("v.recordId", returnValue.accreditationRequestId);
                    
                    if(returnValue.accountAccreditationStatus) {
                        if(returnValue.accountAccreditationStatus == 'Not Accredited') {
                            
                            component.set("v.recordVisibilityMode", 'false');
                            component.set("v.accreditationButtonText", 'Submit for accreditation');
                        }else{
                            if(returnValue.accountAccreditationStatus == 'Accreditation In Progress') {
                                component.set("v.accreditationButtonText", 'Accredetation in progress');
                            }else{
                                component.set("v.accreditationButtonText", 'Accredited!');
                                component.set("v.formSubmittedForAcreditation", true);
                            }
                            
                            component.set("v.recordVisibilityMode", 'true');
                            
                        }
                    }
                }
                
            }
            else if (state === "INCOMPLETE") {
                
            }
                else if (state === "ERROR") {
                    
                }
        });
        $A.enqueueAction(action);
        
    },    
    
    draftSaveAccreditationRecord : function (component, event, helper) {
        console.log('-----draftSaveAccreditationRecord start----');
        var self = this;
        if(this.isErrorOnPage(component)) {
            console.log('--error--');
            this.setSuccessErrorMessage(component, false, true, 'Please correct all form fields');
            
        }else{
            //this.setSuccessErrorMessage(component, false, false, '');
            console.log('-----contact id----' + component.get("v.currentContactId"));
            component.get("v.accreditationRecordFields").Contact__c = component.get("v.currentContactId");
			
            console.log('-----start Business principal value assingation----');
            // Business principal
            var blockBP = component.get("v.blockBP");
            console.log('----' + blockBP);
            console.log('----' + document.getElementById(blockBP + '-ict-phoneno').value);
            console.log('----' + document.getElementById(blockBP + '-ict-email').value);
            console.log('----' + document.getElementById(blockBP + '-ict-firstName').value);
            console.log('----' + document.getElementById(blockBP + '-ict-lastname').value);
            console.log('----' + document.getElementById(blockBP + '-ict-jobtitle').value);
            component.get("v.accreditationRecordFields").Business_Principal_Phone__c = document.getElementById(blockBP + '-ict-phoneno').value;
            component.get("v.accreditationRecordFields").Business_Principal_E_mail__c = document.getElementById(blockBP + '-ict-email').value;
            component.get("v.accreditationRecordFields").Business_Principal_First_Name__c = document.getElementById(blockBP + '-ict-firstName').value;
            component.get("v.accreditationRecordFields").Business_Principal_Last_Name__c = document.getElementById(blockBP + '-ict-lastname').value;
            component.get("v.accreditationRecordFields").Business_Principal_Job_Title__c = document.getElementById(blockBP + '-ict-jobtitle').value;
            
            console.log('-----start Marketing delegate Principal value assingation----');
            // Marketing delegate Principal
            var blockMD = component.get("v.blockMD");
            console.log('----' + blockMD);
            console.log('----' + document.getElementById(blockMD + '-ict-phoneno').value);
            component.get("v.accreditationRecordFields").Marketing_Delegate_Phone__c = document.getElementById(blockMD + '-ict-phoneno').value;
            component.get("v.accreditationRecordFields").Marketing_Delegate_E_mail__c = document.getElementById(blockMD + '-ict-email').value;
            component.get("v.accreditationRecordFields").Marketing_Delegate_First_Name__c = document.getElementById(blockMD + '-ict-firstName').value;
            component.get("v.accreditationRecordFields").Marketing_Delegate_Last_Name__c = document.getElementById(blockMD + '-ict-lastname').value;
            component.get("v.accreditationRecordFields").Marketing_Delegate_Job_Title__c = document.getElementById(blockMD + '-ict-jobtitle').value;
            
            console.log('-----start Programme administrator value assingation----');
            // Programme administrator
            var blockAdmin = component.get("v.blockAdmin");
            console.log('----' + blockAdmin);
            console.log('----' + document.getElementById(blockAdmin + '-ict-phoneno').value);
            component.get("v.accreditationRecordFields").Program_Administrator_Phone__c = document.getElementById(blockAdmin + '-ict-phoneno').value;
            component.get("v.accreditationRecordFields").Program_Administrator_E_mail__c = document.getElementById(blockAdmin + '-ict-email').value;
            component.get("v.accreditationRecordFields").Program_Administrator_First_Name__c = document.getElementById(blockAdmin + '-ict-firstName').value;
            component.get("v.accreditationRecordFields").Program_Administrator_Last_Name__c = document.getElementById(blockAdmin + '-ict-lastname').value;
            component.get("v.accreditationRecordFields").Program_Administrator_Job_Title__c = document.getElementById(blockAdmin + '-ict-jobtitle').value;

            
            component.find("accreditationRecordCreator").saveRecord(function(saveResult) {
                
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    // record is saved successfully
                    self.setSuccessErrorMessage(component, true, false, 'Your draft has been successfully saved.');
                    console.log("Your draft has been successfully saved.");
                    
                } else if (saveResult.state === "INCOMPLETE") {
                    console.log("User is offline, device doesn't support drafts.");
                    self.setSuccessErrorMessage(component, false, true, 'Please check your internet connection');
                    
                    
                } else if (saveResult.state === "ERROR") {
                    console.log('Problem saving contact, error: ' + JSON.stringify(saveResult.error));
                    self.setSuccessErrorMessage(component, false, true, 'Session expired. Please reload the page.');
                    
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                    self.setSuccessErrorMessage(component, false, true, 'Error saving Draft. Please contact your admin');	                    
                }
            });
        }
        console.log('-----draftSaveAccreditationRecord end----');
    },
    
    
    submitForAccreditation : function (component, event, helper) {
        
        var self = this;
        if(this.isErrorOnPage(component)) {
            this.setSuccessErrorMessage(component, false, true, 'Please correct all form fields');
        }else if(this.isFirstAndSecondContactInfoFilled(component)) {
            this.setSuccessErrorMessage(component, false, true, 'All fields are required for form submission.');
        }else{
            
            //Stop Poller
            //window.clearInterval(component.get("v.setIntervalId"));
            //check for Account status again to be sure that it has not been changed
            var action1 = component.get("c.getAccountStatus");
            action1.setParams({
                accountId : component.get("v.currentAccountId")
            });
            
            action1.setCallback(this, function(response) {
                
                var state = response.getState();
                if(state == "SUCCESS") {
                    if(response.getReturnValue() == 'Accreditation In Progress') {
                        component.set("v.recordVisibilityMode", 'true');
                        component.set("v.formSubmittedForAcreditation", true);
                    }else if(response.getReturnValue() == 'Not Accredited') {
                        component.get("v.accreditationRecordFields").Contact__c = component.get("v.currentContactId");
                        
                        // Business principal
                        var blockBP = component.get("v.blockBP");
                        component.get("v.accreditationRecordFields").Business_Principal_Contact_Role__c = 'Business Principle';
                        component.get("v.accreditationRecordFields").Business_Principal_Phone__c = document.getElementById(blockBP + '-ict-phoneno').value;
                        component.get("v.accreditationRecordFields").Business_Principal_E_mail__c = document.getElementById(blockBP + '-ict-email').value;
                        component.get("v.accreditationRecordFields").Business_Principal_First_Name__c = document.getElementById(blockBP + '-ict-firstName').value;
                        component.get("v.accreditationRecordFields").Business_Principal_Last_Name__c = document.getElementById(blockBP + '-ict-lastname').value;
                        component.get("v.accreditationRecordFields").Business_Principal_Job_Title__c = document.getElementById(blockBP + '-ict-jobtitle').value;
                        
						// Marketing delegate Principal
                        var blockMD = component.get("v.blockMD");
                        component.get("v.accreditationRecordFields").Marketing_Delegate_Contact_Role__c = 'Marketing Delegate';
                        component.get("v.accreditationRecordFields").Marketing_Delegate_Phone__c = document.getElementById(blockMD + '-ict-phoneno').value;
                        component.get("v.accreditationRecordFields").Marketing_Delegate_E_mail__c = document.getElementById(blockMD + '-ict-email').value;
                        component.get("v.accreditationRecordFields").Marketing_Delegate_First_Name__c = document.getElementById(blockMD + '-ict-firstName').value;
                        component.get("v.accreditationRecordFields").Marketing_Delegate_Last_Name__c = document.getElementById(blockMD + '-ict-lastname').value;
                        component.get("v.accreditationRecordFields").Marketing_Delegate_Job_Title__c = document.getElementById(blockMD + '-ict-jobtitle').value;
                        
                        // Programme administrator
                        var blockAdmin = component.get("v.blockAdmin");
                        component.get("v.accreditationRecordFields").Program_Administrator_Contact_Role__c = 'Administrator';
                        component.get("v.accreditationRecordFields").Program_Administrator_Phone__c = document.getElementById(blockAdmin + '-ict-phoneno').value;
                        component.get("v.accreditationRecordFields").Program_Administrator_E_mail__c = document.getElementById(blockAdmin + '-ict-email').value;
                        component.get("v.accreditationRecordFields").Program_Administrator_First_Name__c = document.getElementById(blockAdmin + '-ict-firstName').value;
                        component.get("v.accreditationRecordFields").Program_Administrator_Last_Name__c = document.getElementById(blockAdmin + '-ict-lastname').value;
                        component.get("v.accreditationRecordFields").Program_Administrator_Job_Title__c = document.getElementById(blockAdmin + '-ict-jobtitle').value;
                        
                        component.find("accreditationRecordCreator").saveRecord(function(saveResult) {
                            
                            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                                // record is saved successfully
                                var action = component.get("c.createNewAccreditationCase");
                                action.setParams({ 
                                    accountId : component.get("v.currentAccountId"),
                                    contactId : component.get("v.currentContactId")
                                });
                                
                                action.setCallback(this, function(response) {
                                    var state = response.getState();
                                    
                                    if (state === "SUCCESS") {
                                        
                                        component.set("v.recordVisibilityMode", 'true');
                                        component.set("v.accreditationButtonText", 'Accreditation in progress');
                                        //self.setSuccessErrorMessage(component, true, false, 'Your data has been sent successfully for accreditation');
                                        component.set("v.formSubmittedForAcreditation", true);
                                        
                                    }
                                    else if (state === "INCOMPLETE") {
                                        self.setSuccessErrorMessage(component, false, true, 'Error saving data. Please contact your Admin');
                                    }
                                        else if (state === "ERROR") {
                                            self.setSuccessErrorMessage(component, false, true, 'Error saving data. Please contact your Admin');
                                        }
                                });
                                $A.enqueueAction(action); 
                                
                                
                            } else if (saveResult.state === "INCOMPLETE") {
                                console.log("User is offline, device doesn't support drafts.");
                                
                            } else if (saveResult.state === "ERROR") {
                                console.log('Problem saving accreditation Form, error: ' + JSON.stringify(saveResult.error));
                                
                            } else {
                                console.log('Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
                                
                            }
                            
                            
                            
                            
                            
                        });
                    }  
                }
                else if(state === "ERROR") {
                    
                }
                
                
            });
            
            $A.enqueueAction(action1);   
            
        }
        
        
    },
    
    storeErrorAccordions : function(component, event, helper) {
        var errorBlockId = "v." + event.getParam("blockId") + 'Error';
        var isError = event.getParam("isError");
        component.set(errorBlockId, isError);                          
    },
    
    
    isErrorOnPage: function (component) {
        var isError = false;
        
        if(component.get("v.contactInfo1Error")) {
            isError = true;
            if(document.getElementsByClassName("myinfocolumn-contactInfo1")[0].classList.contains('collapsed')) {
                document.getElementsByClassName("myinfocolumn-contactInfo1")[0].click();
            }
            
        } 
        
        if(component.get("v.contactInfo2Error")) {
            isError = true;
            if(document.getElementsByClassName("myinfocolumn-contactInfo2")[0].classList.contains('collapsed')) {
                document.getElementsByClassName("myinfocolumn-contactInfo2")[0].click();
            }
            
        }
        
        if(component.get("v.contactInfo3Error")) {
            isError = true;
            if(document.getElementsByClassName("myinfocolumn-contactInfo3")[0].classList.contains('collapsed')) {
                document.getElementsByClassName("myinfocolumn-contactInfo3")[0].click();
            }
            
        }
        
        return isError;
    },
    
    
    isFirstAndSecondContactInfoFilled : function (component) {
        console.log('--isFirstAndSecondContactInfoFilled--');
        var isError = false;
        var blockBP = component.get("v.blockBP");
        if(!document.getElementById(blockBP + '-ict-phoneno').value  ||
           !document.getElementById(blockBP + '-ict-email').value  ||
           !document.getElementById(blockBP + '-ict-firstName').value  ||
           !document.getElementById(blockBP + '-ict-lastname').value ||
           !document.getElementById(blockBP + '-ict-jobtitle').value
          ) {
            isError = true;
        }
        console.log('--Business principal section--' + isError);
        
        if(isError){
            
            if(document.getElementsByClassName("myinfocolumn-contactInfo1")[0].classList.contains('collapsed')) {
                document.getElementsByClassName("myinfocolumn-contactInfo1")[0].click();
            }
            return isError;
        }
        
        
        var isSecondError = false;
        var blockMD = component.get("v.blockMD");
        if(!document.getElementById(blockMD + '-ict-phoneno').value  ||
           !document.getElementById(blockMD + '-ict-email').value  ||
           !document.getElementById(blockMD + '-ict-firstName').value  ||
           !document.getElementById(blockMD + '-ict-lastname').value ||
           !document.getElementById(blockMD + '-ict-jobtitle').value
          ) {
            isSecondError = true;
        }
        console.log('--Marketing delegate section--' + isSecondError);
        if(isSecondError) {
            
            if(document.getElementsByClassName("myinfocolumn-contactInfo2")[0].classList.contains('collapsed')) {
                document.getElementsByClassName("myinfocolumn-contactInfo2")[0].click();
            }
            
            return isSecondError;
        }
        
        
        var isThirdError = false;
        var blockAdmin = component.get("v.blockAdmin");
        if(!document.getElementById(blockAdmin + '-ict-phoneno').value  ||
           !document.getElementById(blockAdmin + '-ict-email').value  ||
           !document.getElementById(blockAdmin + '-ict-firstName').value  ||
           !document.getElementById(blockAdmin + '-ict-lastname').value ||
           !document.getElementById(blockAdmin + '-ict-jobtitle').value
          ) {
            isThirdError = true;
        }
        console.log('--Program administrator--' + isThirdError);
        if(isThirdError) {
            
            if(document.getElementsByClassName("myinfocolumn-contactInfo3")[0].classList.contains('collapsed')) {
                document.getElementsByClassName("myinfocolumn-contactInfo3")[0].click();
            }
            
            return isThirdError;
        }
        
        return false;
        
    },
    /*
    fetchAccountStatus : function(component, helper, accountId) {
        var action = component.get("c.getAccountStatus");
        
        action.setParams({
            accountId : accountId
        });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS") {
                
                if(response.getReturnValue() == 'In Progress') {
                    component.set("v.recordVisibilityMode", 'true');
                }
                
            }
            else if(state === "ERROR") {
                //Handle Error here
            }
            
        });
        
        $A.enqueueAction(action);
        
        
    },
    */
    setSuccessErrorMessage : function (component, showSuccessMsg, showErrorMsg, errorSuccessMessage) {
        component.set("v.showSuccessMsg", showSuccessMsg);
        component.set("v.showErrorMsg", showErrorMsg);        
        component.set("v.errorSuccessMessage", errorSuccessMessage);
        document.documentElement.scrollTop = 0;
    }
    
})