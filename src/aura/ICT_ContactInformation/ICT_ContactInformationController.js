({
    doInit : function(component, event, helper) {  
        helper.fetchContactRecord(component, event);
    },
    
    populateContactInfo : function(component, event) {   
        console.log('----populateContactInfo-----');
        var mapContact = event.getParam('mapContactInformation');
        
        component.set("v.Salutation", mapContact["Salutation"]);
        component.set("v.FirstName", mapContact["FirstName"]);
        component.set("v.LastName", mapContact["LastName"]);
        component.set("v.Email", mapContact["Email"]);
        component.set("v.Title", mapContact["Title"]);
        component.set("v.Phone", mapContact["Phone"]);
        component.set("v.Mobile", mapContact["Mobile"]);
        component.set("v.preferredContact", mapContact["PreferredContactMethod"]);
        component.set("v.companySize", mapContact["CompanySize"]);
        component.set("v.addressStreet", mapContact["Street"]);
        component.set("v.addressCity", mapContact["City"]);
        component.set("v.addressPostalCode", mapContact["PostalCode"]);
        component.set("v.addressState", mapContact["State"]);
    },
    
    //perform validation for all fields
    validateFields : function(cmp, event, helper){
        console.log('--field validation--start here--');
        console.log('--event div--'+ event.currentTarget.dataset.errordivid);
        console.log('--event id--'+ event.currentTarget.id);
        
        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        console.log('--errorDivId--' + errorDivId);
        
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);   
        console.log('--errorSpanId--' + errorSpanId);
        
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;
        console.log('--errorMsg--' + errorMsg);
        
        var fieldId = event.currentTarget.id;  
        console.log('--fieldId--' + fieldId);
        
        var fieldVal = document.getElementById(fieldId).value;
        console.log('--fieldVal--' + fieldVal + '--length---' + fieldVal.length);
        
        var executeValidation = cmp.get("v.executeValidation");
		console.log('--executeValidation--' + executeValidation);
        if(fieldId=='ict-lastName' && fieldVal == ''){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldId=='ict-account' && fieldVal == ''){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldId=='ict-phoneno' && fieldVal.length > 0 && !fieldVal.match(/^\d{10}$/)){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldId=='ict-mobile' && fieldVal.length > 0 && !fieldVal.match(/^\d{10}$/)){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldId=='ict-email' && !fieldVal.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');        
        }
        else if(fieldId=='ict-preferredmethod'){
            if(fieldVal == 'Please choose'){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
            else{
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error'); 
                $A.util.addClass(errorDivId,'has-success'); 
            }
        }
        else if(fieldVal == ''){
            $A.util.removeClass(errorDivId,'has-success');
        }
        else{
            if($A.util.hasClass(errorSpanId,'help-block-error'))
            {
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error'); 
                $A.util.addClass(errorDivId,'has-success'); 
            }
            $A.util.addClass(errorDivId,'has-success'); 
            $A.util.addClass(errorSpanId,'help-none-error');
        }
	},
    
    // save the contact 
    saveContactInformation:function(cmp, event, helper){ 
        console.log('---saveContactInformation---');
        helper.validateFieldsOnSubmission(cmp, event, helper, true, true, false);
        
        console.log('--hasError--', cmp.get('v.hasError'));
        if(!cmp.get('v.hasError')){
            helper.processContactInformation(cmp, event);            
        }}
    //helper.processContactInformation(cmp, event); 
})