({
    fetchContactRecord : function(component,event) {
        console.log('----fetchContactRecord-----');
        var url = window.location.href;
        var leadID;
        console.log('****getURLParametersHelper****' + url + '***' + url.indexOf('editor'));
        
        if(url.indexOf('editor') == -1){        
            if(url.indexOf(component.get('{!v.paramLeadId}')) !== -1){
                component.set("v.executeValidation", true);
            }
        
        	//Salutation picklist selection
            var actionSalutation = component.get("c.getSalutationPicklistValues"); 
            console.log('--salutationOptList--');
            // set a callBack    
            actionSalutation.setCallback(this, function(response) {
                console.log("--Salutation option response--" + response.getReturnValue());
                console.log("--Salutation option response status--" + response.getState());
                var phaseStatus = response.getState();
                
                if (phaseStatus === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    component.set("v.salutationOptList", storeResponse);
                }
            });
            
            // enqueue the Action  
            $A.enqueueAction(actionSalutation);
            
            //Preferred Contact Method picklist selection
            var actionPreferredMethod = component.get("c.getPreferredContactPicklistValues"); 
            console.log('--preferredmethodList--');
            // set a callBack    
            actionPreferredMethod.setCallback(this, function(response) {
                console.log("--Preferred Contact option response--" + response.getReturnValue());
                console.log("--Preferred Contact option response status--" + response.getState());
                var phaseStatus = response.getState();
                
                if (phaseStatus === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    component.set("v.preferredmethodList", storeResponse);
                }
            });
            
            // enqueue the Action  
            $A.enqueueAction(actionPreferredMethod);
            
            // Get contact information
            if(url.indexOf(component.get('{!v.paramOppId}')) !== -1){
                // Fetch Opportunity ID
                var regexFN = new RegExp("[?&]" + component.get('{!v.paramOppId}') + "(=([^&#]*)|&|#|$)");
                var resultsFN = regexFN.exec(url);
                var oppID = decodeURIComponent(resultsFN[2].replace(/\+/g, " "));
                
                if(oppID != '' && oppID != undefined){
                    // Get the contact record
                    var actionCon = component.get("c.getOpportunityRecord"); 
                    console.log('--getOpportunityRecord--');
                    actionCon.setParams({
                        'strRecordId': oppID
                    });
                    
                    // set a callBack    
                    actionCon.setCallback(this, function(response) {
                        console.log("--contact response--" + response.getReturnValue());
                        var storeResponse = response.getReturnValue();
                        console.log("--contact id --" + storeResponse.oppRecord.Id);
                        var oppStatus = response.getState();
                        
                        if (oppStatus == "SUCCESS") {
                            console.log("--contact success--");
                            
                            console.log("--contact storeResponse--" + storeResponse.oppRecord);
                            console.log("--contact storeResponse--oppRecord-ID-" + storeResponse.oppRecord.Id);
                            if(storeResponse.conRecord.Id != undefined && storeResponse.hasOpptyAccess){
                                component.set("v.Salutation", storeResponse.conRecord.Salutation);
                                component.set("v.FirstName", storeResponse.conRecord.FirstName);
                                component.set("v.LastName", storeResponse.conRecord.LastName);
                                component.set("v.Email", storeResponse.conRecord.Email);
                                component.set("v.Title", storeResponse.conRecord.Job_Title__c);
                                component.set("v.Phone", storeResponse.conRecord.Phone);
                                component.set("v.Mobile", storeResponse.conRecord.MobilePhone);
                                component.set("v.preferredContact", storeResponse.conRecord.Preferred_Contact_Method__c);
                                component.set("v.companySize", storeResponse.conRecord.Company_Size__c);
                                component.set("v.addressStreet", storeResponse.conRecord.MailingStreet);
                                component.set("v.addressCity", storeResponse.conRecord.MailingCity);
                                component.set("v.addressPostalCode", storeResponse.conRecord.MailingPostalCode);
                                component.set("v.addressState", storeResponse.conRecord.MailingState);
                            }
                        }
                    });    
                    $A.enqueueAction(actionCon); 
                }
            }
        }
	}, 

    validateFieldsOnSubmission : function(component, event, helper) {
        console.log('---validateFieldOnSubmission---');
        console.log('--Account----' + component.get('v.Account'));
        console.log('--LastName----' + component.get('v.LastName'));
        
        var div = document.getElementById("ict-containerId");
        var subDiv = div.getElementsByTagName('input');
        var myArray = [];
        var myInfoFieldError = false;
        
        for(var i = 0; i < subDiv.length; i++) {
            var elem = subDiv[i];
            if(elem.id.indexOf('ict-') === 0) {
                myArray.push(elem.id);
                // console.log('element id==>'+elem.id);
            }
        }
        
        var errorFlag = false;
        for(var inputElementId=0; inputElementId<myArray.length; inputElementId++){
            var errorDivId = document.getElementById(myArray[inputElementId]+'-div');
            var errorSpanId = document.getElementById(myArray[inputElementId]+'-error'); 
            var fieldId = myArray[inputElementId];      
            var fieldVal = document.getElementById(fieldId).value;
            
            console.log('errorDivId :----'+errorDivId+'== errorSpanId :----'+errorSpanId );
            console.log('fieldId :----' + fieldId + '== fieldVal----' + fieldVal);
            
            if(fieldId=='ict-lastName' && fieldVal == '')
            { 
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
                console.log('** last name **' );
            }
               else if(fieldId=='ict-phoneno' && fieldVal.length > 0 && !fieldVal.match(/^\d{10}$/)){
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorFlag = true;
                }
                    else if(fieldId=='ict-mobile' && fieldVal.length > 0 && !fieldVal.match(/^\d{10}$/)){
                        $A.util.addClass(errorSpanId,'help-block-error');
                        $A.util.addClass(errorDivId,'has-error');
                        $A.util.removeClass(errorDivId,'has-success');
                        errorFlag = true;
                    }
                        else if(fieldId=='ict-email' && !fieldVal.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
                            $A.util.addClass(errorSpanId,'help-block-error');
                            $A.util.addClass(errorDivId,'has-error');
                            $A.util.removeClass(errorDivId,'has-success');    
                            errorFlag = true;
                        }
                            else if(fieldId=='ict-preferredmethod'){
                                if(fieldVal == 'Please choose'){
                                    $A.util.addClass(errorSpanId,'help-block-error');
                                    $A.util.addClass(errorDivId,'has-error');
                                    $A.util.removeClass(errorDivId,'has-success');
                                    errorFlag = true;
                                }
                                else{
                                    $A.util.removeClass(errorSpanId,'help-block-error');
                                    $A.util.removeClass(errorDivId,'has-error'); 
                                    $A.util.addClass(errorDivId,'has-success'); 
                                }
                            }
                                else{
                                    $A.util.removeClass(errorSpanId,'help-block-error');
                                    $A.util.removeClass(errorDivId,'has-error');                     
                                }
        }
        
        console.log('--errorFlag--' + errorFlag);
        // Check the error presence
        if(errorFlag == true){
            component.set('v.hasError', true);
        }
        else if(errorFlag == false){
            component.set('v.hasError', false);
        }
    },
    processContactInformation : function(component,event) {
        console.log('--into process contact information helper--');
        var Salutation = document.getElementById('ict-salutation').value; 
		var FirstName = document.getElementById('ict-firstName').value; 
		var LastName = document.getElementById('ict-lastName').value; 
		var Email = document.getElementById('ict-email').value; 
		var Title = document.getElementById('ict-title').value; 
		var phone = document.getElementById('ict-phoneno').value; 
		var Mobile = document.getElementById('ict-mobile').value; 
		var preferredContact = document.getElementById('ict-prefcontactmethod').value; 
		var companySize = document.getElementById('ict-employeeno').value; 

        var streetName = component.get("v.addressStreetName"); 
        var suburb = component.get("v.addressSuburb");
        var state = component.get("v.addressState"); 
        var postalCode = component.get("v.addressPostalCode");
    
        console.log('--Salutation--' + Salutation);
        console.log('--FirstName--' + FirstName);
        console.log('--LastName--' + LastName);
		console.log('--Email--' + Email);
		console.log('--Title--' + Title);
        console.log('--Phone--' + phone);
		console.log('--Mobile--' + Mobile);
		console.log('--PreferredContactMethod--' + preferredContact);
        console.log('--EmployeeCount--' + companySize);
        console.log('--streetName--' + streetName);
        console.log('--suburb--' + suburb);
        console.log('--state--' + state);
        console.log('--postalCode--'+ postalCode);

		var action = component.get("c.processContactInformation");
        action.setParams({
			'strSalutaion': Salutation,
			'strFirstName': FirstName,
			'strLastName': LastName,
			'strEmail': Email,
			'strTitle': Title,
			'strPhone': phone,
            'strMobile': Mobile,
			'strPreferredContact': preferredContact,
            'strEmployeeCount': companySize,
            'strStreetName': streetName,
            'strSuburb': suburb,
            'strState': state,
            'strPostalcode': postalCode
        });
        
        console.log('--processContactInfo--');
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('--processContactInfo storeResponse--' + storeResponse);
                if(storeResponse){
                    component.set("v.showInformationSection", false);
                    component.set("v.showSuccessMsg", true);                    
                    component.set("v.showErrorMsg", false);                    
                }
                else{
                    component.set("v.showInformationSection", true);
                    component.set("v.showSuccessMsg", false);  
                    component.set("v.showErrorMsg", true);
                }
            }
            else{
                component.set("v.showInformationSection", true);
				component.set("v.showSuccessMsg", false);
                component.set("v.showErrorMsg", false);
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
	}
})