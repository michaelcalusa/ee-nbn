({
    doInit: function(component, event, helper) {           
        helper.fetchLeadRecord(component, event, helper);   
    },
    
	saveLeadDetails : function(component, event, helper){
		console.log('---saveLeadDetails---');
        helper.validateFieldsOnSubmission(component, event, helper);
        
        console.log('--hasError--', component.get('v.hasError'));
        if(!component.get('v.hasError')){
            helper.processLeadInformation(component, event, helper);            
        }
    },
    
    cancelSaveProcess : function(cmp, event, helper){
        console.log('---cancelSaveProcess---');
        cmp.set("v.ReadView", true);
		window.location.href = cmp.get('v.recordURL');
    },
    
    editLead : function(cmp, event, helper){
        cmp.set("v.ReadView", false);
        cmp.set('v.showDeclineReasons',false);
        cmp.set('v.declineReason','Please select decline reason');
    },

    acceptLead : function(component, event, helper){
        console.log('--acceptLead--start here--');
        component.set('v.isAccepted',true);
		helper.processLeadAcceptDecline(component, event, helper);
    },

    declineLead : function(component, event, helper){
        console.log('--declineLead--start here--');
        //var hasDeclineSelected = component.get('v.showDeclineReasons');
        var declinedReason = component.get("v.declineReason");
        console.log('--declinedReason--' + declinedReason); 

        if(declinedReason == undefined){
            component.set('v.showDeclineReasons',true);
            console.log('--declineLead---undefined');
        }
        else if(declinedReason == 'Please select decline reason'){
            component.set('v.showDeclineReasons',true);
            console.log('--declineLead---Select option'); 
        }
        else{
            console.log('--declineLead--process');
            component.set('v.isAccepted',false);
            helper.processLeadAcceptDecline(component, event, helper);
        }
        
        var errorDeclineDivId = document.getElementById('ict-decline-div');
        var errorDeclineSpanId = document.getElementById('ict-decline-error'); 
        if(declinedReason == '' || declinedReason == undefined){
            $A.util.addClass(errorDeclineSpanId,'help-block-error');
            $A.util.addClass(errorDeclineDivId,'has-error');
            $A.util.removeClass(errorDeclineDivId,'has-success');    
        }        
        console.log('--declinedReason-2-' + declinedReason);
    },
    
    leadConvert : function(cmp, event, helper){
        var leadId = cmp.get("v.recordId");
        var navigationLightningEvent = $A.get("e.force:navigateToURL");
        navigationLightningEvent.setParams({
            "url": "/opportunity-view?lid=" + leadId,
            "isredirect" : true
        });
        navigationLightningEvent.fire();   
    },
    
    validateFields : function(cmp, event, helper){
        console.log('--field validation--start here--');
        console.log('--event div--'+ event.currentTarget.dataset.errordivid);
        console.log('--event id--'+ event.currentTarget.id);
        
        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        console.log('--errorDivId--' + errorDivId);
        
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);   
        console.log('--errorSpanId--' + errorSpanId);
        
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;
        console.log('--errorMsg--' + errorMsg);
        
        var fieldId = event.currentTarget.id;  
        console.log('--fieldId--' + fieldId);
        
        var fieldVal = document.getElementById(fieldId).value;
        console.log('--fieldVal--' + fieldVal + '--length---' + fieldVal.length);

        if(fieldId=='ict-lastName' && fieldVal == ''){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldId=='ict-decline'){
            if(fieldVal == 'Please select decline reason'){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
            else{
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error'); 
                $A.util.addClass(errorDivId,'has-success'); 
            }
            cmp.set("v.declineReason", fieldVal);
        }
        else if(fieldId=='ict-account' && fieldVal == ''){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldId=='ict-phoneno' && fieldVal.length > 0 && !fieldVal.match(/^\d{10}$/)){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldId=='ict-mobile' && fieldVal.length > 0 && !fieldVal.match(/^\d{10}$/)){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldId=='ict-email' && !fieldVal.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');        
        }
        else if(fieldId=='ict-preferredmethod'){
            if(fieldVal == 'Please choose'){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
            else{
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error'); 
                $A.util.addClass(errorDivId,'has-success'); 
            }
        }
        else if(fieldVal == ''){
            $A.util.removeClass(errorDivId,'has-success');
        }
        else{
            if($A.util.hasClass(errorSpanId,'help-block-error'))
            {
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error'); 
                $A.util.addClass(errorDivId,'has-success'); 
            }
            $A.util.addClass(errorDivId,'has-success'); 
            $A.util.addClass(errorSpanId,'help-none-error');
        }
	}
})