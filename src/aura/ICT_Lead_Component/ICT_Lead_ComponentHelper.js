({
    fetchLeadRecord : function(component, event, helper) {
        console.log('---fetchLeadRecord---');
        var leadID;
        var url = window.location.href;
        component.set('v.recordURL', url);
		if(url.indexOf('editor') == -1){
            // Verify that component is accessed from end user only
            if(url.indexOf(component.get('{!v.paramLeadId}')) !== -1){
                // Fetch Lead ID
                var regexFN = new RegExp("[?&]" + component.get('{!v.paramLeadId}') + "(=([^&#]*)|&|#|$)");
                var resultsFN = regexFN.exec(url);
                leadID = decodeURIComponent(resultsFN[2].replace(/\+/g, " "));
				console.log('---leadID---' + leadID);
                component.set("v.ReadView", true);
            }

			// Get the Lead Decline Reason picklist selection
            
            var actionReasons = component.get("c.getDeclineReasonPicklistValues"); 
            console.log('--declineOptList--');
            // set a callBack    
            actionReasons.setCallback(this, function(response) {
                console.log("--Decline Reason option response--" + response.getReturnValue());
                console.log("--Decline Reason option response status--" + response.getState());
                var declineStatus = response.getState();
                
                if (declineStatus === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    component.set("v.declineOptList", storeResponse);
                }
            });
            
            // enqueue the Action  
            $A.enqueueAction(actionReasons);
            
            // Get the Lead Salutation picklist selection
            
            var actionSalutation = component.get("c.getSalutationPicklistValues"); 
            console.log('--salutationOptList--');
            // set a callBack    
            actionSalutation.setCallback(this, function(response) {
                console.log("--Salutation option response--" + response.getReturnValue());
                console.log("--Salutation option response status--" + response.getState());
                var phaseStatus = response.getState();
                
                if (phaseStatus === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    component.set("v.salutationOptList", storeResponse);
                }
            });
            
            // enqueue the Action  
            $A.enqueueAction(actionSalutation);
            
            // Get the Lead Preferred Contact Method picklist selection
            
            var actionPreferredMethod = component.get("c.getPreferredContactPicklistValues"); 
            console.log('--preferredmethodList--');
            // set a callBack    
            actionPreferredMethod.setCallback(this, function(response) {
                console.log("--Preferred Contact option response--" + response.getReturnValue());
                console.log("--Preferred Contact option response status--" + response.getState());
                var phaseStatus = response.getState();
                
                if (phaseStatus === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    component.set("v.preferredmethodList", storeResponse);
                }
            });
            
            // enqueue the Action  
            $A.enqueueAction(actionPreferredMethod);
            
            // Get the Lead Company Size picklist selection
            
            var actionCompSize = component.get("c.getCompanySizePicklistValues"); 
            console.log('--companysizeList--');
            // set a callBack    
            actionCompSize.setCallback(this, function(response) {
                console.log("--Company Size option response--" + response.getReturnValue());
                console.log("--Company Size option response status--" + response.getState());
                var phaseStatus = response.getState();
                
                if (phaseStatus === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    component.set("v.companysizeList", storeResponse);
                }
            });
            
            // enqueue the Action  
            $A.enqueueAction(actionCompSize);
        
            // Call the controller to get Lead record
            var actionLead = component.get("c.getLeadRecord");
            actionLead.setParams({
                'strRecordId': leadID
            });
            actionLead.setCallback(this, function(response) {
                var state = response.getState();
                var storeResponse = response.getReturnValue();
                console.log('---state---' + state);
                console.log('---storeResponse----' + storeResponse);
                if (state === "SUCCESS") {
                    console.log('--Response---Record id--' + storeResponse.Id + '--Owner id--' + storeResponse.OwnerId);
                    //Setting Existing Lead Record - Draft one
                    if(storeResponse.Id != undefined && storeResponse.Id != null) {
                        // Set the visibility for Accept and Decline
                        var LeadOWner = storeResponse.OwnerId;
                        if(LeadOWner.startsWith('00G')){
                            component.set('v.showLeadChoices',true);
                            console.log('---queue owner---');
                        }
                        else{
                            component.set('v.showLeadChoices',false);
                            console.log('---partner user---');
                        }
                        
                        // Set the Lead details
                        component.set("v.recordId", storeResponse.Id);
                        component.set("v.Salutation", storeResponse.Salutation);
                        component.set("v.FirstName", storeResponse.FirstName);
                        component.set("v.LastName", storeResponse.LastName);
                        component.set("v.Email", storeResponse.Email);
                        component.set("v.Account", storeResponse.Company);
                        component.set("v.Title", storeResponse.Title__c);
                        component.set("v.Phone", storeResponse.Phone);
                        component.set("v.Mobile", storeResponse.MobilePhone);
                        component.set("v.PreferredContact", storeResponse.Preferred_Contact_Method__c);
                        component.set("v.ABNNumber", storeResponse.ABN__c);
                        component.set("v.TradingName", storeResponse.Trading_Name__c);
                        component.set("v.ComapnySize", storeResponse.Company_Size__c);
                        component.set("v.addressStreetName", storeResponse.Street);
                        component.set("v.addressCity", storeResponse.City);
                        component.set("v.addressState", storeResponse.State);
                        component.set("v.addressPostalCode", storeResponse.PostalCode);
                    }
                    else{
                        var navigationLightningEvent = $A.get("e.force:navigateToURL");
                        navigationLightningEvent.setParams({
                            "url": "/errorpage",
                            "isredirect" : true
                        });
                        navigationLightningEvent.fire();   
                    }
                }
            });
            $A.enqueueAction(actionLead);
        }
	},
    
    processLeadAcceptDecline : function(component, event, helper) {
        console.log('----processLeadAccept----');
        var redirectURL;
        var leadRecordId = component.get("v.recordId");
		var declinedReason = component.get("v.declineReason");
        console.log('--declinedReason--' + declinedReason); 
        
        // Call the controller to get Lead record
        var actionLead;
        if(component.get('v.isAccepted')){
            console.log('----accept----');
            var leadId = component.get("v.recordId");
            redirectURL = "/lead-view?lid=" + leadId;
            actionLead = component.get("c.processAcceptance");
            actionLead.setParams({
                'strRecordId': leadRecordId
            });
        }else{
            console.log('----decline----');
            redirectURL = "/leads";
            actionLead = component.get("c.processDecline");
			actionLead.setParams({
                'strRecordId': leadRecordId,
                'strDeclineReason': declinedReason
            });
        }
  
        // set a callBack    
        actionLead.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('--saveLeadRecord storeResponse--' + storeResponse);
                if(storeResponse != '' || storeResponse != undefined){
                    if(storeResponse == 'success'){
                        
                        var navigationLightningEvent = $A.get("e.force:navigateToURL");
                        navigationLightningEvent.setParams({
							"url": redirectURL,
                            "isredirect" : true
                         });
                        navigationLightningEvent.fire(); 
                    }
                    else{
                        component.set("v.showErrorMsg", true);
                    }
            	}
                else{
                    component.set("v.showErrorMsg", true);
                }
            }
        });
        $A.enqueueAction(actionLead);
    },
   
    validateFieldsOnSubmission : function(component, event, helper) {
        console.log('---validateFieldOnSubmission---');
        console.log('--Account----' + component.get('v.Account'));
        console.log('--LastName----' + component.get('v.LastName'));
        
        var div = document.getElementById("ict-containerId");
        var subDiv = div.getElementsByTagName('input');
        var myArray = [];
        var myInfoFieldError = false;
		
        for(var i = 0; i < subDiv.length; i++) {
            var elem = subDiv[i];
            if(elem.id.indexOf('ict-') === 0) {
                myArray.push(elem.id);
                // console.log('element id==>'+elem.id);
            }
        }
        
        var errorFlag = false;
        for(var inputElementId=0; inputElementId<myArray.length; inputElementId++){
            var errorDivId = document.getElementById(myArray[inputElementId]+'-div');
            var errorSpanId = document.getElementById(myArray[inputElementId]+'-error'); 
            var fieldId = myArray[inputElementId];      
            var fieldVal = document.getElementById(fieldId).value;
            
            console.log('errorDivId :----'+errorDivId+'== errorSpanId :----'+errorSpanId );
            console.log('fieldId :----' + fieldId + '== fieldVal----' + fieldVal);
            
            if(fieldId=='ict-lastName' && fieldVal == '')
            { 
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
                console.log('** last name **' );
            }
            else if(fieldId=='ict-account' && fieldVal == ''){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
            }
            else if(fieldId=='ict-phoneno' && fieldVal.length > 0 && !fieldVal.match(/^\d{10}$/)){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
            }
            else if(fieldId=='ict-mobile' && fieldVal.length > 0 && !fieldVal.match(/^\d{10}$/)){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
            }
            else if(fieldId=='ict-email' && !fieldVal.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');    
                errorFlag = true;
            }
			else if(fieldId=='ict-preferredmethod'){
				if(fieldVal == 'Please choose'){
					$A.util.addClass(errorSpanId,'help-block-error');
					$A.util.addClass(errorDivId,'has-error');
					$A.util.removeClass(errorDivId,'has-success');
                    errorFlag = true;
				}
				else{
					$A.util.removeClass(errorSpanId,'help-block-error');
					$A.util.removeClass(errorDivId,'has-error'); 
					$A.util.addClass(errorDivId,'has-success'); 
				}
			}
            else{
					$A.util.removeClass(errorSpanId,'help-block-error');
					$A.util.removeClass(errorDivId,'has-error');                     
            }
        }

        var preferredcontact = document.getElementById('ict-preferredmethod').value;  
        console.log('--preferredcontact--' + preferredcontact);
        
		var errorPCDivId = document.getElementById('ict-preferredmethod-div');
        var errorPCSpanId = document.getElementById('ict-preferredmethod-error'); 
        
        if(preferredcontact == 'Please choose'){
            $A.util.addClass(errorPCSpanId,'help-block-error');
            $A.util.addClass(errorPCDivId,'has-error');
            $A.util.removeClass(errorPCDivId,'has-success');
            errorFlag = true;
        }
        
        console.log('--errorFlag--' + errorFlag);
        // Check the error presence
        if(errorFlag == true){
            component.set('v.hasError', true);
        }
        else if(errorFlag == false){
            component.set('v.hasError', false);
        }
    },

    processLeadInformation : function(component, event, helper) {
        console.log('--processLeadInformation--');
        
        var leadRecordId = component.get("v.recordId");
        console.log('--leadRecordId--' + leadRecordId);
        var salutation = document.getElementById('ict-salutation').value; 
        console.log('--salutation--' + salutation);
        var firstname = document.getElementById('ict-firstName').value; 
        console.log('--firstname--' + firstname);
        var lastname = document.getElementById('ict-lastName').value; 
        console.log('--lastname--' + lastname);
        var email = document.getElementById('ict-email').value;  
        console.log('--email--' + email);
        var company = document.getElementById('ict-account').value;  
        console.log('--company--' + company);
        var title = document.getElementById('ict-title').value; 
        console.log('--title--' + title);
        var phone = document.getElementById('ict-phoneno').value; 
        console.log('--phone--' + phone);
        var mobile = document.getElementById('ict-mobile').value; 
        console.log('--mobile--' + mobile);
        var preferredcontact = document.getElementById('ict-preferredmethod').value;  
        console.log('--preferredcontact--' + preferredcontact);
        var tradingname = document.getElementById('ict-tradingname').value; 
        console.log('--tradingname--' + tradingname);
        var street = document.getElementById('ict-street').value; 
        console.log('--street--' + street);
        var city = document.getElementById('ict-city').value; 
        console.log('--city--' + city);
        var state = document.getElementById('ict-state').value;  
        console.log('--state--' + state);
        var postcode = document.getElementById('ict-postcode').value; 
        console.log('--postcode--' + postcode);
        
        if(salutation == 'Please choose'){
            salutation = '';
        }
        
        var action = component.get("c.saveLeadRecord");
        action.setParams({
            'strRecordId': leadRecordId,
            'strSalutation': salutation,
            'strFirstName': firstname,
            'strLastName': lastname,
            'strEmail': email,
            'strCompany': company,
            'strTitle': title,
            'strPhone': phone,
            'strMobile': mobile,
			'strPreferredContact': preferredcontact,
            'strTradingName': tradingname,
            'strStreet': street,
            'strCity': city,
            'strState': state,
			'strPostCode': postcode
        });        
        
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('--saveLeadRecord storeResponse--' + storeResponse);
                if(storeResponse != '' || storeResponse != undefined){
                    if(storeResponse == 'success'){
                        var leadId = component.get("v.recordId");
                        var navigationLightningEvent = $A.get("e.force:navigateToURL");
                        navigationLightningEvent.setParams({
                          "url": "/lead-view?lid=" + leadId,
                            "isredirect" : true
                         });
                        navigationLightningEvent.fire(); 
                    }
                    else{
                        component.set("v.showErrorMsg", true);
                    }
            	}
                else{
                    component.set("v.showErrorMsg", true);
                }
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    }
})