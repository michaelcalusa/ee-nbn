({
	doInit : function(component, event, helper) {
        console.log('----ICT Opportunity---Init--');
        var url = window.location.href;
        
        if(url.indexOf('editor') == -1){
            // Verify that component is accessed from end user only
            if(url.indexOf(component.get('{!v.paramOppId}')) !== -1){
                // Fetch Opportunity ID
                var regexFN = new RegExp("[?&]" + component.get('{!v.paramOppId}') + "(=([^&#]*)|&|#|$)");
                var resultsFN = regexFN.exec(url);
                var oppID = decodeURIComponent(resultsFN[2].replace(/\+/g, " "));
                
                component.set("v.OppId", oppID);
                component.set("v.isTabView", true);
            }
        }
	}
})