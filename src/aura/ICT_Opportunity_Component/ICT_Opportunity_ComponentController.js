({
	doInit: function(component, event, helper) {           
        helper.fetchOpportunityRecord(component, event, helper);   
    },
    
    saveOpportunityDetails : function(cmp, event, helper){
		console.log('---saveOpportunityDetails---');
        helper.validateFieldsOnSubmission(cmp, event, helper);
        
        console.log('--hasError--', cmp.get('v.hasError'));
        if(!cmp.get('v.hasError')){
            cmp.set("v.btndisabled", true);
            helper.processOpportunityInformation(cmp, event, helper);            
        }
        else{
            cmp.set("v.btndisabled", false);
        }
    },

    
    cancelSaveProcess : function(cmp, event, helper){
        console.log('---saveOpportunityDetails---');
        
		var opportunityID = cmp.get('{!v.oppRecordId}');
        console.log('---opportunityID---' + cmp.get('{!v.oppRecordId}'));
        var navigationLightningEvent = $A.get("e.force:navigateToURL");
        if(opportunityID == '' || opportunityID == undefined){
            navigationLightningEvent.setParams({
              "url": "/opportunities",
                "isredirect" : true
            });
        }
        else{
            
            navigationLightningEvent.setParams({
              "url": "/opportunity-view?oid=" + opportunityID,
                "isredirect" : true
            });
        }
        navigationLightningEvent.fire();
    },
    
    eidtOpportunity : function(cmp, event, helper){
        cmp.set("v.ReadView", false);
        var opportunityID = cmp.get('{!v.oppRecordId}');
        var url = cmp.get('v.recordURL');
        window.location.href = url + "&e=1";
    },
    
    validateLightningFields : function(cmp, event, helper){
        console.log('--Lightning field validation--start here--');
		var errorDivId = document.getElementById('ict-closedate-div');
        var errorSpanId = document.getElementById('ict-closedate-error');       
        
        console.log('--errorDivId--' + errorDivId);
        console.log('--errorSpanId--' + errorSpanId);
        
		var oppclosedate = cmp.find("ict-closedate").get("v.value");
        console.log('--oppclosedate--' + oppclosedate);
		if(oppclosedate == '' || oppclosedate == undefined){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else{
            $A.util.removeClass(errorSpanId,'help-block-error');
            $A.util.removeClass(errorDivId,'has-error'); 
            $A.util.addClass(errorDivId,'has-success');
        }
    },
    
    validateFields : function(cmp, event, helper){
        console.log('--field validation--start here--');
        console.log('--event div--'+ event.currentTarget.dataset.errordivid);
        console.log('--event id--'+ event.currentTarget.id);
        
        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        console.log('--errorDivId--' + errorDivId);
        
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);   
        console.log('--errorSpanId--' + errorSpanId);
        
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;
        console.log('--errorMsg--' + errorMsg);
        
        var fieldId = event.currentTarget.id;  
        console.log('--fieldId--' + fieldId);
        
        var fieldVal = document.getElementById(fieldId).value;
        console.log('--fieldVal--' + fieldVal);

        if(fieldId=='ict-oppname' && fieldVal == ''){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldId=='ict-phasetype'){
            if(fieldVal == 'Please choose'){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
            else{
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error'); 
                $A.util.addClass(errorDivId,'has-success'); 
            }
        }
        else if(fieldId=='ict-entityname' && fieldVal == ''){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldId=='ict-site' && !fieldVal.match(/^[0-9]*$/)){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if((fieldId=='ict-NSTC5' || fieldId=='ict-NSTC20' || fieldId=='ict-NSTC2' || fieldId=='ict-NSTC4' || fieldId=='ict-NSTC1' || fieldId=='ict-NSSLA') && fieldVal == ''){
            $A.util.removeClass(errorSpanId,'help-block-error');
            $A.util.removeClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if((fieldId=='ict-NSTC5' || fieldId=='ict-NSTC20' || fieldId=='ict-NSTC2' || fieldId=='ict-NSTC4' || fieldId=='ict-NSTC1' || fieldId=='ict-NSSLA') && !fieldVal.match(/^[0-9]*$/)){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(fieldVal == ''){
            $A.util.removeClass(errorDivId,'has-success');
        }
        else{
            if($A.util.hasClass(errorSpanId,'help-block-error'))
            {
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error'); 
                $A.util.addClass(errorDivId,'has-success'); 
            }
            $A.util.addClass(errorDivId,'has-success'); 
            $A.util.addClass(errorSpanId,'help-none-error');
        }
	},
    populateABNInfo : function(cmp, event) {   
        var abnSearchStatus = event.getParam('Status');
        var mapABNResult = event.getParam('mapABNResults');
        var abnType = event.getParam('abnType');
        //var businessName = mapABNResult["BusinessName"];
		var abnNumber =  mapABNResult["ABNNumber"].replace(/ +/g, "");
        var entityName =  mapABNResult["RegisteredEntityName"];
        
        console.log('----entityName---' + entityName);
        console.log('----abnNumber---' + abnNumber);
        cmp.set("v.abnNumber", abnNumber);
        cmp.set("v.entityName", entityName);
        
		var errorDivId = document.getElementById('ict-entityname-div');
        var errorSpanId = document.getElementById('ict-entityname-error');  
        
        $A.util.removeClass(errorSpanId,'help-block-error');
        $A.util.removeClass(errorDivId,'has-error'); 
        $A.util.addClass(errorDivId,'has-success');
        
		var errorABNDivId = document.getElementById('ict-abnNumber-div');
        var errorABNSpanId = document.getElementById('ict-abnNumber-error');  
        
        $A.util.removeClass(errorABNSpanId,'help-block-error');
        $A.util.removeClass(errorABNDivId,'has-error'); 
        $A.util.addClass(errorABNDivId,'has-success');
    }
})