({
	fetchOpportunityRecord : function(component, event, helper) {
        var url = window.location.href;
        var oppID;
        var leadID;
        console.log('****getURLParametersHelper****' + url + '***' + url.indexOf('editor'));
        component.set('v.recordURL', url);
        if(url.indexOf('editor') == -1){
            // Verify that component is accessed from end user only
            if(url.indexOf(component.get('{!v.paramOppId}')) !== -1){
                // Fetch Opportunity ID
                var regexFN = new RegExp("[?&]" + component.get('{!v.paramOppId}') + "(=([^&#]*)|&|#|$)");
                var resultsFN = regexFN.exec(url);
                
                oppID = decodeURIComponent(resultsFN[2].replace(/\+/g, " "));
                
                component.set("v.oppRecordId", oppID);
                component.set("v.ReadView", true);
                component.set("v.hasFileUpload", oppID);
            }
			
            if(url.indexOf('e=') !== -1){
                console.log('---yes--');
                component.set("v.ReadView", false);
                var newurl = url.replace('e=1','').replace('&','');
                component.set('v.recordURL', newurl);
            }
            
            if(url.indexOf(component.get('{!v.paramLeadId}')) !== -1){
                // Fetch Lead ID
                var regexFN = new RegExp("[?&]" + component.get('{!v.paramLeadId}') + "(=([^&#]*)|&|#|$)");
                var resultsFN = regexFN.exec(url);
                leadID = decodeURIComponent(resultsFN[2].replace(/\+/g, " "));
                component.set("v.executeValidation", true);
                component.set("v.leadRecordId", leadID);
                component.set("v.ReadView", false);
            }
            
            // Get the Opportunity Phase picklist selection
            
            var actionPhase = component.get("c.phaseOptions"); 
            console.log('--phaseOptions--');
            // set a callBack    
            actionPhase.setCallback(this, function(response) {
                console.log("--phase option response--" + response.getReturnValue());
                console.log("--phase option response status--" + response.getState());
                var phaseStatus = response.getState();
                
                if (phaseStatus === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    component.set("v.phaseOptList", storeResponse);
                }
            });
            
            // enqueue the Action  
            $A.enqueueAction(actionPhase);
            
            
            // Get Opportunity information
            console.log('--oppID--' + oppID);
            if(oppID != '' && oppID != undefined){
                // Get the opportunity record
                var actionOpp = component.get("c.getOpportunityRecord"); 
                console.log('--getOpportunityRecord--');
                actionOpp.setParams({
                    'strRecordId': component.get('{!v.oppRecordId}')
                });
                
                // set a callBack    
                actionOpp.setCallback(this, function(response) {
                    console.log("--opportunity response--" + response.getReturnValue());
                    var storeResponse = response.getReturnValue();
                    console.log("--opportunity id --" + storeResponse.oppRecord.Id);
                    var oppStatus = response.getState();
                    
                    if (oppStatus == "SUCCESS") {
                        console.log("--opportunity success--");
                        
                        console.log("--opportunity storeResponse--" + storeResponse.oppRecord);
                        console.log("--opportunity storeResponse--oppRecord-ID-" + storeResponse.oppRecord.Id);
                        if(storeResponse.oppRecord.Id != undefined && storeResponse.hasOpptyAccess){
                            component.set("v.oppRecordId", storeResponse.oppRecord.Id);
                            component.set("v.name", storeResponse.oppRecord.Name);
                            component.set("v.phasetype", storeResponse.oppRecord.StageName);
                            component.set("v.closeDate", storeResponse.oppRecord.CloseDate);
                            component.set("v.abnNumber", storeResponse.oppRecord.Account.ABN__c);
                            component.set("v.entityName", storeResponse.oppRecord.Account.Name);
                            component.set("v.tradingName", storeResponse.oppRecord.Trading_Name__c);
                            component.set("v.comments", storeResponse.oppRecord.Comments__c);
                            component.set("v.totalSites", storeResponse.oppRecord.Total_Sites__c);
                            component.set("v.siteDescription", storeResponse.oppRecord.Site_Information__c);
                            component.set("v.NSTC5", storeResponse.oppRecord.Number_of_services_TC2_5_10Mbps__c);
                            component.set("v.NSTC20", storeResponse.oppRecord.Number_of_services_TC2_20_100Mbps__c);
                            component.set("v.NSTC2", storeResponse.oppRecord.Number_of_services_TC2_100Mbps__c);
                            component.set("v.NSTC4", storeResponse.oppRecord.Number_of_services_TC4__c);
                            component.set("v.NSTC1", storeResponse.oppRecord.Number_of_services_TC1__c);
                            component.set("v.NSSLA", storeResponse.oppRecord.Number_of_services_eSLA__c);
                            
                            /*
                            // Set the Contact Information map for events
                            var myMap = component.get('v.mapContactInfo');
                            myMap['Salutation'] = storeResponse.conRecord.Salutation;
                            myMap['FirstName'] = storeResponse.conRecord.FirstName;
                            myMap['LastName'] = storeResponse.conRecord.LastName;
                            myMap['Email'] = storeResponse.conRecord.Email;
                            myMap['Title'] = storeResponse.conRecord.Job_Title__c;
                            myMap['Phone'] = storeResponse.conRecord.Phone;
                            myMap['Mobile'] = storeResponse.conRecord.MobilePhone;
                            myMap['PreferredContactMethod'] = storeResponse.conRecord.Preferred_Contact_Method__c;
                            myMap['CompanySize'] = storeResponse.conRecord.Company_Size__c;
                            myMap['Street'] = storeResponse.conRecord.Street;
                            myMap['City'] = storeResponse.conRecord.City;
                            myMap['PostalCode'] = storeResponse.conRecord.PostalCode;
                            myMap['State'] = storeResponse.conRecord.State;
                            
                            // Set the event to use values
                            var myEvent = $A.get("e.c:ICTContactInformationEvent");
                            myEvent.setParams({
                                "mapContactInformation":component.get('v.mapContactInfo')
                            });                       
                            myEvent.fire();*/
                        }
                        else{
                            var navigationLightningEvent = $A.get("e.force:navigateToURL");
                            navigationLightningEvent.setParams({
                              "url": "/errorpage",
                                "isredirect" : true
                             });
                            navigationLightningEvent.fire();   
                        }
                    }
                });    
                $A.enqueueAction(actionOpp); 
            }
            
            // Get Lead information
            console.log('--leadID--' + leadID);
            if(leadID != '' && leadID != undefined){
                // Get the Lead record
                var actionLead = component.get("c.getLeadRecord"); 
                console.log('--getLeadRecord--');
                actionLead.setParams({
                    'strRecordId': component.get('{!v.leadRecordId}')
                });
                
                // set a callBack    
                actionLead.setCallback(this, function(response) {
                    var state = response.getState();
                var storeResponse = response.getReturnValue();
                console.log('---state---' + state);
                console.log('---storeResponse----' + storeResponse);
                    
                    if (state === "SUCCESS") {
                        console.log('--Response---Record id--' + storeResponse.leadRecord.Id + '--Owner id--' + storeResponse.leadRecord.OwnerId);
                        //Setting Existing Lead Record - Draft one
                        if(storeResponse.leadRecord.Id != undefined && storeResponse.leadRecord.Id != null) {
                           
                            // Set the Lead details
                            component.set("v.recordId", storeResponse.leadRecord.Id);
                            component.set("v.abnNumber", storeResponse.leadRecord.ABN__c);
                            component.set("v.entityName", storeResponse.strEntityName);
                            component.set("v.tradingName", storeResponse.leadRecord.Trading_Name__c);
                            
                            console.log('--contact---' + storeResponse.leadRecord.City);
                            // Set the Contact Information map for events
                            var myMap = component.get('v.mapContactInfo');
                            myMap['Salutation'] = storeResponse.leadRecord.Salutation;
                            myMap['FirstName'] = storeResponse.leadRecord.FirstName;
                            myMap['LastName'] = storeResponse.leadRecord.LastName;
                            myMap['Email'] = storeResponse.leadRecord.Email;
                            myMap['Title'] = storeResponse.leadRecord.Title__c;
                            myMap['Phone'] = storeResponse.leadRecord.Phone;
                            myMap['Mobile'] = storeResponse.leadRecord.MobilePhone;
                            myMap['PreferredContactMethod'] = storeResponse.leadRecord.Preferred_Contact_Method__c;
                            myMap['CompanySize'] = storeResponse.leadRecord.Company_Size__c;
                            myMap['Street'] = storeResponse.leadRecord.Street;
                            myMap['City'] = storeResponse.leadRecord.City;
                            myMap['PostalCode'] = storeResponse.leadRecord.PostalCode;
                            myMap['State'] = storeResponse.leadRecord.State;
                            
                            // Set the event to use values
                            var myEvent = $A.get("e.c:ICTContactInformationEvent");
                            myEvent.setParams({
                                "mapContactInformation":component.get('v.mapContactInfo')
                            });                       
                            myEvent.fire();
                        }
                        else{
                            var navigationLightningEvent = $A.get("e.force:navigateToURL");
                            navigationLightningEvent.setParams({
                                "url": "/errorpage",
                                "isredirect" : true
                            });
                            navigationLightningEvent.fire();   
                        }
                    }
                });    
                $A.enqueueAction(actionLead); 
            }
        }
	},
    
    validateFieldsOnSubmission : function(component, event, helper) {
        console.log('validateFieldOnSubmission');
        console.log('--name----' + component.get('v.name'));
        console.log('--closeDate----' + component.get('v.closeDate'));
        console.log('--abnNumber----' + component.get('v.abnNumber'));
        console.log('--businessName----' + component.get('v.businessName'));
        console.log('--comments----' + component.get('v.comments'));
        console.log('--totalSites----' + component.get('v.totalSites'));
        console.log('--siteDescription----' + component.get('v.siteDescription'));
        console.log('--phase----' + component.get('v.phase'));
        
        var div = document.getElementById("ict-containerId");
        var subDiv = div.getElementsByTagName('input');
        var myArray = [];
        var myInfoFieldError = false;
		
        myArray.push('ict-phasetype');
        for(var i = 0; i < subDiv.length; i++) {
            var elem = subDiv[i];
            if(elem.id.indexOf('ict-') === 0) {
                myArray.push(elem.id);
                // console.log('element id==>'+elem.id);
            }
        }
        var errorFlag = false;
        for(var inputElementId=0; inputElementId<myArray.length; inputElementId++){
            var errorDivId = document.getElementById(myArray[inputElementId]+'-div');
            var errorSpanId = document.getElementById(myArray[inputElementId]+'-error'); 
            var fieldId = myArray[inputElementId];      
            var fieldVal = document.getElementById(fieldId).value;
            
            console.log('errorDivId :----'+errorDivId+'== errorSpanId :----'+errorSpanId );
            console.log('fieldId :----' + fieldId + '== fieldVal----' + fieldVal);
            
            var executeValidation = component.get("v.executeValidation");
			console.log('--executeValidation--' + executeValidation);
            
            if(fieldId=='ict-oppname' && fieldVal == '')
            { 
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
                console.log('** opportunity name **' );
            }
            else if(fieldId=='ict-entityname' && fieldVal == '')
            {
                console.log('--else case entityname--' + fieldId);
                errorFlag = true;
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
            else if(fieldId=='ict-phasetype' && fieldVal == 'Please choose'){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                //errorFlag = true;
                
                console.log('** phasetype **' );
            }
            else if(fieldId=='ict-site' && !fieldVal.match(/^[0-9]*$/)){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
            }
            else if((fieldId=='ict-NSTC5' || fieldId=='ict-NSTC20' || fieldId=='ict-NSTC2' || fieldId=='ict-NSTC4' || fieldId=='ict-NSTC1' || fieldId=='ict-NSSLA') && !fieldVal.match(/^[0-9]*$/)){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
            }
            else if(fieldId=='ict-lastName' && fieldVal == ''){ 
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
                console.log('** last name **' );
            }
            else if(fieldId=='ict-phoneno' && fieldVal.length > 0 && !fieldVal.match(/^\d{10}$/)){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
            }
            else if(fieldId=='ict-mobile' && fieldVal.length > 0 && !fieldVal.match(/^\d{10}$/)){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorFlag = true;
            }
            else if(fieldId=='ict-email' && fieldVal != '' && !fieldVal.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');    
                errorFlag = true;
            }
			else if(fieldId=='ict-preferredmethod'){
				if(fieldVal == 'Please choose'){
					$A.util.addClass(errorSpanId,'help-block-error');
					$A.util.addClass(errorDivId,'has-error');
					$A.util.removeClass(errorDivId,'has-success');
                    errorFlag = true;
				}
				else{
					$A.util.removeClass(errorSpanId,'help-block-error');
					$A.util.removeClass(errorDivId,'has-error'); 
					$A.util.addClass(errorDivId,'has-success'); 
				}
			}
        }
      	
		// Validate ABN Number
		console.log('** abn number **' + component.get('v.abnNumber'));
		var errorABNDivId = document.getElementById('ict-abnNumber-div');
        var errorABNSpanId = document.getElementById('ict-abnNumber-error'); 
        if(component.get('v.abnNumber') == '' || component.get('v.abnNumber') == undefined){
            $A.util.addClass(errorABNSpanId,'help-block-error');
            $A.util.addClass(errorABNDivId,'has-error');
            $A.util.removeClass(errorABNDivId,'has-success');    
            errorFlag = true;
        }
        
        // Validate Close Date
        var errorDateDivId = document.getElementById('ict-closedate-div');
        var errorDateSpanId = document.getElementById('ict-closedate-error');       
        
        console.log('--errorDateDivId--' + errorDateDivId);
        console.log('--errorDateSpanId--' + errorDateSpanId);
        
		var oppclosedate = component.find("ict-closedate").get("v.value");
		if(oppclosedate == '' || oppclosedate == undefined){
            $A.util.addClass(errorDateSpanId,'help-block-error');
            $A.util.addClass(errorDateDivId,'has-error');
            $A.util.removeClass(errorDateDivId,'has-success');
            errorFlag = true;
        }

        console.log('--errorFlag--' + errorFlag);
        // Check the error presence
        if(errorFlag == true){
            component.set('v.hasError', true);
        }
        else if(errorFlag == false){
            component.set('v.hasError', false);
        }
    },
    
    processOpportunityInformation : function(component, event, helper) {
        console.log('--processOpportunityInformation--');
        var leadConvertFlag = true;
        var leadRecordId = component.get("v.leadRecordId");
        var oppRecordId = component.get("v.oppRecordId");
        console.log('--oppRecordId--' + oppRecordId);
        var name = document.getElementById('ict-oppname').value;
        console.log('--name--' + name);
        var phasetype = document.getElementById('ict-phasetype').value;
        console.log('--phasetype--' + phasetype);
        var closeDate = component.get("v.closeDate"); 
        console.log('--closeDate--' + closeDate);
        var abnNumber = component.get("v.abnNumber"); 
        console.log('--abnNumber--' + abnNumber);
        var entityName = component.get("v.entityName"); 
        console.log('--entityName--' + entityName);
        var tradingName = document.getElementById('ict-tradingname').value;
        console.log('--tradingName--' + tradingName);
        var comments = document.getElementById('ict-comments').value;
        console.log('--comments--' + comments);
        var totalSites = document.getElementById('ict-site').value; 
        console.log('--totalSites--' + totalSites);
        var NSTC5 = document.getElementById('ict-NSTC5').value; 
        console.log('--NSTC5--' + NSTC5);
        var NSTC20 = document.getElementById('ict-NSTC20').value; 
        console.log('--NSTC20--' + NSTC20);
        var NSTC2 = document.getElementById('ict-NSTC2').value; 
        console.log('--NSTC2--' + NSTC2);
        var NSTC4 = document.getElementById('ict-NSTC4').value; 
        console.log('--NSTC4--' + NSTC4);
        var NSTC1 = document.getElementById('ict-NSTC1').value; 
        console.log('--NSTC1--' + NSTC1);
        var NSSLA = document.getElementById('ict-NSSLA').value; 
        console.log('--NSSLA--' + NSSLA);
        var siteDescription = document.getElementById('ict-siteDescription').value; 
        console.log('--siteDescription--' + siteDescription);


        var salutation = document.getElementById('ict-salutation').value; 
        console.log('--salutation--' + salutation);
        var firstname = document.getElementById('ict-firstName').value; 
        console.log('--firstname--' + firstname);
        var lastname = document.getElementById('ict-lastName').value; 
        console.log('--lastname--' + lastname);
        var email = document.getElementById('ict-email').value;  
        console.log('--email--' + email);
        var title = document.getElementById('ict-title').value; 
        console.log('--title--' + title);
        var phone = document.getElementById('ict-phoneno').value; 
        console.log('--phone--' + phone);
        var mobile = document.getElementById('ict-mobile').value; 
        console.log('--mobile--' + mobile);
        var preferredcontact = document.getElementById('ict-preferredmethod').value;  
        console.log('--preferredcontact--' + preferredcontact);
        var street = document.getElementById('ict-street').value; 
        console.log('--street--' + street);
        var city = document.getElementById('ict-city').value; 
        console.log('--city--' + city);
        var state = document.getElementById('ict-state').value;  
        console.log('--state--' + state);
        var postcode = document.getElementById('ict-postcode').value; 
        console.log('--postcode--' + postcode);
        
        var action = component.get("c.saveOpportunityRecord");
        action.setParams({
            'strRecordId': oppRecordId,
            'strName': name,
            'strPhase': phasetype,
            'strCloseDate': closeDate,
            'strABNNumber': abnNumber,
            'strEntityName': entityName,
            'strTradingName': tradingName,
            'strComments': comments,
            'strTotalSites': totalSites,
			'strSiteDescription': siteDescription,
            'strNSTC5': NSTC5,
            'strNSTC20': NSTC20,
            'strNSTC2': NSTC2,
            'strNSTC4': NSTC4,
            'strNSTC1': NSTC1,
            'strNSSLA': NSSLA,
            
            'strSalutation': salutation,
            'strFirstName': firstname,
            'strLastName': lastname,
            'strEmail': email,
            'strTitle': title,
            'strPhone': phone,
            'strMobile': mobile,
            'strPreferredContact': preferredcontact,
            'strStreet': street,
            'strCity': city,
            'strState': state,
            'strPostCode': postcode,
            'strLeadId': leadRecordId
        });
        
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('--saveOpportunityRecord storeResponse--' + storeResponse);
                if(storeResponse != '' || storeResponse != undefined){
                   /*
                    var navigationLightningEvent = $A.get("e.force:navigateToURL");
                    navigationLightningEvent.setParams({
                      "url": "/opportunity-view?oid=" + storeResponse,
                        "isredirect" : true
                     });
                    navigationLightningEvent.fire();  
                    */
					var url = component.get('v.recordURL');
                    console.log('----url string-1--' + url);
                    if(url.indexOf('lid=') !== -1){
						url = url.replace('lid=','');  
                        console.log('----url string-2--' + url);
                        var commurl = component.get("v.ICTCommURL");
                        window.location.href = commurl + "ictpartnerprogram/s/opportunity-view?oid=" + storeResponse;
                    }
                    else{
                        window.location.href = url;
                    }
            	}
                else{
                    component.set("v.showErrorMsg", true);
                }
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
        
    }
})