({
    qsToEventMap: {
        'startURL'  : 'e.c:ICTSetStartURL'
    },
    
    getURLParametersHelper: function(component, event, helpler) {
        //name = name.replace(/[\[\]]/g, "\\$&");
        var url = window.location.href;
        var contactID;
        console.log('****getURLParametersHelper****' + url);
        // Verify that component is accessed from end user only
        if(url.indexOf(component.get('{!v.paramContactId}')) !== -1){
			// Fetch Contact ID
            var regexFN = new RegExp("[?&]" + component.get('{!v.paramContactId}') + "(=([^&#]*)|&|#|$)");
            var resultsFN = regexFN.exec(url);
            contactID = decodeURIComponent(resultsFN[2].replace(/\+/g, " "));
			
            component.set("v.contactID", contactID);
            console.log('****Contact ID****' + contactID);
                       
            // Verify if user already exist
            var action = component.get("c.checkCommunityUser");

            action.setParams({strContactID:contactID});
            action.setCallback(this, function(a){
            	var response = a.getReturnValue();
                
                component.set("v.retrieveUserInformation",response);
                if(response.isUserExist == true){
                    console.log('****User exist****');
                    component.set("v.showConfirmation",true);
                    component.set("v.showRegistration",false);
                }
                else if(response.isUserExist == false && response.strErrorMessage == '')
                {
                    console.log('****User does not exist****' + response.strErrorMessage + '---' + response.isUserExist);
                    component.set("v.showConfirmation",false);
                	component.set("v.showRegistration",true);
                    component.set("v.emailLabel",response.strUserEmail);
                }
                else{
                    console.log('****URL not correct****');
                    component.set("v.showConfirmation",false);
                	component.set("v.showRegistration",true);
                    component.set("v.errorMessage", response.strErrorMessage);
                    component.set("v.showError",true);
                }
            });
            $A.enqueueAction(action);
        }
        else{
			// If email is NULL then URL is malformed. Show error
            component.set("v.errorMessage", $A.get("$Label.c.ICT_URL_Validation"));
            component.set("v.showError",true);
            component.set("v.showRegistration",true);
        }
    },
    
    handleSelfRegister: function (component, event, helpler) {
        var startUrl = component.get("v.startUrl");
        var contactID = component.get("v.contactID");
        var firstname = component.get("v.firstnameLabel");//find("firstname").get("v.value");
        var lastname = component.get("v.lastnameLabel");//find("lastname").get("v.value");
        var email = component.get("v.emailLabel");//component.find("email").get("v.value");
        var password = component.find("password").get("v.value");
        var confirmPassword = component.find("confirmPassword").get("v.value");
        var action = component.get("c.selfRegister");
        
        startUrl = decodeURIComponent(startUrl);

        action.setParams({
            firstname:firstname, 
            lastname:lastname, 
            email:email, 
            password:password, 
            confirmPassword:confirmPassword, 
            startUrl:startUrl,
            strContactID:contactID
        });
        action.setCallback(this, function(a){
          var rtnValue = a.getReturnValue();
          if (rtnValue !== null) {
             component.set("v.errorMessage",rtnValue);
             component.set("v.showError",true);
          }
        });
    	$A.enqueueAction(action);
    }    
})