({
    doInit : function(component, event, helper) {
        console.log('--doInit - Site Information Component--');
		helper.FileAccesshelper(component, event, helper);
	},
    
	handleUploadFinished : function(component, event, helper) {
        console.log('--handleUploadFinished - Site Information Component--');
		helper.FileAccesshelper(component, event, helper);
	}
})