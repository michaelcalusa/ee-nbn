({
	FileAccesshelper : function(component, event, helper) {
        console.log('--FileAccesshelper - Site Information Component--');
		var action = component.get("c.showOpportunityAttachments");
        var oppid = component.get("v.oppRecordId");
        console.log('--oppid--' + oppid);
        
        action.setParams({
            'strOpportunityID': oppid
        });
        
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                console.log('--storeResponse--' + storeResponse.length);
                if(storeResponse.length != 0 ){
                    console.log('--Files exist--');
                	component.set("v.attachemtIDs", storeResponse);  
                    component.set("v.isTabView", true);
                }
                else{
                    console.log('--Files do not exist--');
                    component.set("v.isTabView", false);
                }
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);		
	}
})