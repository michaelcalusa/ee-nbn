({
    handleCurrentIncidentRecord: function(component, event, helper) {
        if(component.get('v.incidentRecord')){
            helper.getIncidentOverview(component);
        }
     }
})