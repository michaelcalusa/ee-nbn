({  
    getIncidentOverview : function(component) {        
        var result = component.get('v.incidentRecord');
        component.set("v.strValue1", result.Incident_Type__c);
        // Add Op Cat Tier 1 attribute
        component.set("v.str_OPCat_Tier1", result.Op_Cat__c);
        component.set("v.strValue2", result.Fault_Type__c);
        component.set("v.strValue3", result.Channel_Reported__c);
        component.set("v.strValue4", result.Access_Seeker_Name__c);
        component.set("v.strValue5", result.Reported_Date__c);
        if(!result.Fault_Type__c){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title"   : $A.get("$Label.c.JIGSAW_No_Fault_Type_Title"),
                "type"   : "warning",
                "mode": "sticky",
                "message": $A.get("$Label.c.JIGSAW_No_Fault_Type_Message")
            });
            toastEvent.fire();
        }
        if(result.Resolution_Rejection_Count__c){
            component.set("v.resolutionRejectCount", result.Resolution_Rejection_Count__c);
        }
        if(result.repeatIncResolutionGoal__c)
        {
            if (result.repeatIncResolutionGoal__c.indexOf('-') != -1)
            {  
                component.set("v.strValue6", result.repeatIncResolutionGoal__c.substr(0, result.repeatIncResolutionGoal__c.indexOf('-'))); 
            }
            else
            {
                component.set("v.strValue6", result.repeatIncResolutionGoal__c);
            }
        }  
    }  
})