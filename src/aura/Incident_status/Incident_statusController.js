({
    myAction : function(component, event, helper) {
        helper.helperMethod(component,event,helper,false);
    },
    handleApplicationEventFired : function(cmp, event) {
        var context = event.getParam("parameter");
        cmp.set("v.mostRecentEvent", context);
        var numApplicationEventsHandled =  parseInt(cmp.get("v.numApplicationEventsHandled")) + 1;
        cmp.set("v.numApplicationEventsHandled", numApplicationEventsHandled);
    },
    handleSpinnerEvent : function(component, event) {
        var context = event.getParam("showSpinner");
        component.set("v.Details",context);
    },
    startSpinners: function(component, event, helper) {
        if(!event.getParam("stopSpinner") && (event.getParam("currentRecordId") === component.get("v.recordId")))
        {
            if(event.getParam("spinnersToStart").includes("IncidentStatus"))
            {
                //Start Of Setting Error Handling Attributes.
                component.set("v.currentStatusReceived",false);
                component.set("v.showIncidentStatus",true);
                component.set("v.showIncidentStatusError",false);
                //End Of Setting Error Handling Attributes.
                var bgColorTarget = component.find('incidentStatusBgColor');
                if(!$A.util.hasClass(bgColorTarget, 'bg-grey'))
                    $A.util.addClass(bgColorTarget, 'bg-grey');
                component.set("v.DetailsRSP",'Updating Current Status');
                var currentUserAction = event.getParam("userAction");
                if(currentUserAction === "rspRejectResolution")
                {
                    component.set("v.DynamicIncidentSubMsg",'Waiting for reject resolution for RSP to complete.')
                }
                if(currentUserAction === "rspAcceptResolution")
                {
                    component.set("v.DynamicIncidentSubMsg",'Waiting for accept resolution for RSP to complete.')
                }
                if(currentUserAction === "offsiteTechnician")
                {
                    component.set("v.DynamicIncidentSubMsg",'Waiting for offsite technician to complete')
                }
                if(currentUserAction === "onsiteTechnician")
                {
                    component.set("v.DynamicIncidentSubMsg",'Waiting for onsite technician to complete')
                }
                if(currentUserAction === "cancelCommitment")
                {
                    component.set("v.DynamicIncidentSubMsg",$A.get("$Label.c.convertToCmmtMessage"))
                }
                if(currentUserAction === "cancelAppointment")
                {
                    component.set("v.DynamicIncidentSubMsg",$A.get("$Label.c.convertToApptMessage"))
                }
                if(currentUserAction === "createWorkRequest")
                {
                    component.set("v.DynamicIncidentSubMsg",$A.get("$Label.c.DispatchTechSubMessage"))
                }
                helper.checkTimer(component,event,helper);
            }
        }
        else
        {
            /*//Start Of Setting Error Handling Attributes.
            component.set("v.currentStatusReceived",true);
            component.set("v.showIncidentStatus",true);
            component.set("v.showIncidentStatusError",false);
            //End Of Setting Error Handling Attributes.
            component.set("v.DetailsRSP","");
            var bgColorTarget = component.find('incidentStatusBgColor');
            if($A.util.hasClass(bgColorTarget, 'bg-grey'))
                $A.util.removeClass(bgColorTarget, 'bg-grey');*/
            helper.helperMethod(component,event,helper,false);
        }
    },
    handleUpdateSpinnerEvent: function(component, event, helper) {
        if(event.getParam("attrCurrentIncidentRecord").Id === component.get("v.recordId") && event.getParam("spinnersToStart").includes("IncidentStatus"))
        {
            if(!event.getParam("stopSpinner")) 
            {
                console.log("Inside Handler Function");
                var bgColorTarget = component.find('incidentStatusBgColor');
                if(!$A.util.hasClass(bgColorTarget, 'bg-grey'))
                    $A.util.addClass(bgColorTarget, 'bg-grey');
                var updatedIncidentWrapperRecord = event.getParam("attrCurrentIncidentRecord");
                component.set("v.DetailsRSP",'Updating Current Status');
                component.set("v.DynamicIncidentSubMsg",updatedIncidentWrapperRecord.IncidentSubMessage__c);
                //Gray Toast Message section
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "message" : "Submitting Request" + '\n' + updatedIncidentWrapperRecord.DynamicToastForIncident__c
                });
                resultsToast.fire();
                //Start Of Setting Error Handling Attributes.
                component.set("v.currentStatusReceived",false);
                component.set("v.showIncidentStatus",true);
                component.set("v.showIncidentStatusError",false);
                //End Of Setting Error Handling Attributes.
                helper.checkTimer(component,event,helper);
            }
            else
            {
                if(event.getParam("showSuccessToastMessage") && event.getParam("showSuccessToastMessage") === true)
                    helper.helperMethod(component,event,helper,true);
                else if(event.getParam("hardRefreshComponent") && event.getParam("hardRefreshComponent") === true)
                {
                    helper.helperMethod(component,event,helper,false);
                }
                else
                {
					helper.helperMethod(component,event,helper,false);
                }
            }
        }
    },
    cancelAppointment : function(component, event, helper) {
        console.log("In Incident Status Controller");
        helper.cancelAppointment(component, event, helper);
    },
    /*reloadIncidentpanel : function(component, event, helper) {
       // if(event.getParam("incidentRecordId") === component.get("v.recordId"))
       // {
            helper.helperMethod(component,event,helper,false);
       // }
    }*/
})