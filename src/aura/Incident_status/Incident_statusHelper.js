({
    helperMethod: function(component, event, helper, showToastMessage) {
        var action = component.get("c.get_CurrentIncident");
        action.setParams({
            incidentId: component.get("v.recordId")
        });
        action.setCallback(this, function(data) {
            //Reset The Component Variables to blank before setting the new values.
            component.set("v.Current_status", "");
            component.set("v.Current_status_msg", "");
            component.set("v.Flag", "");
            component.set("v.displayDateTimeSlot", false);
            component.set("v.Details", "");
            component.set("v.DetailsRSP", "");
            component.set("v.strSuggestedAction", "");
            component.set("v.pendingMessageDetails", "");
            component.set("v.resolvedMessageDetails", "");
            component.set("v.tier1Msg", "");
            component.set("v.tier2Msg", "");
            component.set("v.tier3Msg", "");
            component.set("v.resolutionDate","");
            component.set("v.displayDateTimeSlot", false);
            component.set("v.CompleteDate", false);
            component.set("v.isResolved", false);
            component.set("v.incidentStatusIcon", $A.get('{!$Resource.nbnSpogiconStatusStar}'));
            component.set("v.hideSearch","");
            component.set("v.hideSearchResolved","");
            component.set("v.showAppointmentCommitmentLinks",false);
            component.set("v.showErrorHandlingButton",false);
            component.set("v.currentStatusReceived",false);
            component.set("v.showPRD",false);
            var ret = data.getReturnValue();
            if(ret){
                var dataIs = data.getReturnValue().inm;
                if(showToastMessage)
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success",
                        "type": "success",
                        "message": "Action performed successfully."
                    });
                    toastEvent.fire();
                    if(dataIs.User_Action__c && dataIs.User_Action__c == "relateToIncident")
                    {
                        //Gray Toast Message section for si/ni linking action.
                        var resultsToast = $A.get("e.force:showToast");
                        resultsToast.setParams({
                            "message" : "Submitting Request" + "\n" + dataIs.DynamicToastForIncident__c
                        });
                        resultsToast.fire();
                    }
                } 
                this.setComponentBody(component,helper,ret,dataIs);
            }
        });
        $A.enqueueAction(action);
    },
    setComponentBody: function(component,helper,ret,dataIs) {
        component.set("v.currentIncident", dataIs);
        component.set("v.DynamicToastMsg", ret.DynamicToastMsg);
        component.set("v.DynamicIncidentSubMsg", ret.DynamicIncidentSubMsg);
        component.set("v.EngagementType", dataIs.Engagement_Type__c);
        var incidentRecordFieldValues = {"AppointmentId__c":dataIs.AppointmentId__c,"Engagement_Type__c":dataIs.Engagement_Type__c,"Access_Seeker_ID__c":dataIs.Access_Seeker_ID__c,"Prod_Cat__c" : dataIs.Prod_Cat__c};
        component.set("v.currentIncidentRecord", incidentRecordFieldValues);
        var dd = new Date(dataIs.closedDate__c);
        if (dd.getDate()) {
            component.set("v.AppDateIS", dd);
        }
        if(dataIs != null && dataIs.Jigsaw_WO_Selected_PCR_Desc__c != null && dataIs.Jigsaw_WO_Selected_PCR_Desc__c != ''){
            var pcrDescvalue = dataIs.Jigsaw_WO_Selected_PCR_Desc__c.split("/");
            if(pcrDescvalue.length === 3){
                console.log('pcr value array is --' + pcrDescvalue);
                var p = pcrDescvalue[0];
                var c = pcrDescvalue[1];
                var r = pcrDescvalue[2];
                component.set("v.pDesc", pcrDescvalue[0] + " /");
                component.set("v.cDesc", pcrDescvalue[1] + " /");
                component.set("v.rDesc", pcrDescvalue[2] );
            }
            var incStatus = dataIs.Current_Status__c;
            var woStatus = dataIs.Current_work_order_status__c;
            console.log('PCR value is before -- ');
            if(incStatus ===  'In Progress' &&  woStatus && woStatus.toUpperCase() == 'CANCELLED'){
                component.set("v.showPCR","true");
            }
            if(incStatus ===  'Tech Offsite' && woStatus && (woStatus.toUpperCase() === 'FINISHED' || woStatus.toUpperCase() === 'COMPLETED' || woStatus.toUpperCase() === 'HELD' || woStatus.toUpperCase() === 'PENDING' || woStatus.toUpperCase() === 'CANCELLED' )){
                component.set("v.showPCR","true");
                console.log('PCR value is -- ' +component.get("v.showPCR"));
            }
        }
        if(ret.awaitingResponseIncident === true || dataIs.MaximoEngagementActionPending__c) {
            let waitingTime = (new Date() - new Date(dataIs.Recent_Action_Triggered_Time__c));
            console.log('waiting Time'+ waitingTime/60000);
            if(Number.isNaN(waitingTime) ||  (waitingTime / 60000 ) >= 3){
                console.log('waiting Time stopping spinner');
                //Start Of Setting Error Handling Attributes.
                component.set("v.showIncidentStatus",false);
                component.set("v.showIncidentStatusError",true);
                //End Of Setting Error Handling Attributes.
                //check on error from maximo system for cancel appointment if error display the button for manually pushing the action to RPA
                if((ret.UserActionValue === "cancelAppointment" || ret.UserActionValue === "cancelCommitment") && !$A.util.isEmpty(dataIs.CancelAppointmentErrorMessage__c))
                {
                    component.set("v.showErrorHandlingButton",true);   
                }
                component.set("v.DetailsRSP",'Updating Current Status');
                var bgColorTarget = component.find('incidentStatusBgColor');
                $A.util.addClass(bgColorTarget, 'bg-grey');
            }
            else{
                waitingTime = $A.get("$Label.c.JIGSAW_Action_Waiting_Time_Limit") - waitingTime;
                component.set('v.waitingTime',waitingTime);
                var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
            	fireSpinnerRefresh.setParams(
                    {"userAction" :dataIs.User_Action__c, 
                     "stopSpinner" : false,
                     "spinnersToStart": "IncidentStatus",
                     "currentRecordId" : dataIs.Id
                    });
            	fireSpinnerRefresh.fire();
            }
        }
        else
        {
            //Start Of Setting Error Handling Attributes.
            component.set("v.currentStatusReceived",true);
            component.set("v.showIncidentStatus",true);
            component.set("v.showIncidentStatusError",false);
            //End Of Setting Error Handling Attributes.
            //Set Appointment Link Section Details
            if(dataIs.Engagement_Type__c === 'Appointment' && dataIs.AppointmentId__c)
            {
                component.set("v.showAppointmentCommitmentLinks",true);
                component.set("v.showLinkText","Show Appointment Details");
                component.set("v.hideLinkText","Hide Appointment Details");
                if(component.get('v.CISAppointMentRequestEnabled') == 'true'){
                    helper.getAppointmentDetailsFromCIS_Incident_Status(component,helper,dataIs);
                }
                else{
                    helper.setAppointmentDetailsTransactionId(component,dataIs);
                }
            }
            if(dataIs.Engagement_Type__c === 'Commitment' && dataIs.AppointmentId__c)
            {
                component.set("v.showAppointmentCommitmentLinks",true);
                component.set("v.showLinkText","Show Commitment Details");
                component.set("v.hideLinkText","Hide Commitment Details");
            }
            //Set Component Status
            if(ret.RPAErrorOccured == true) {
                component.set("v.DetailsRSP","");
            }
            if (null != dataIs.locId__c) {
                helper.getTimezone(component, dataIs.locId__c);
            }
            if ((dataIs.Engagement_Type__c == 'Inconclusive') && ((dataIs.Incident_Status__c == 'In Progress' || dataIs.Incident_Status__c == 'Assigned') && dataIs.Status_Reason__c == 'Accepted')) {
                component.set("v.Current_status", "Incident Accepted");
                component.set("v.Current_status_msg", 'Accepted');
                component.set("v.displayDateTimeSlot", false);
                component.set("v.Details", "Suggested Action");
            }
            else if(ret.Current_status == 'Pending') {
                ret.Details = dataIs.Status_Reason__c;
                component.set("v.Current_status", ret.Current_status);
                component.set("v.Flag", "See Recent History for details");
                component.set("v.strSuggestedAction", ret.suggestedAction);
                // Added the below code for CUSTA#10022.
                component.set("v.hideSearch",true); 
                if(ret.UserActionValue == 'requestNewAppoinmentFromRSP' || ret.UserActionValue == 'changeEUETAppointment' ) {
                    // START Ref: CUSTSA-27098- show Request Appointment from RSP action for Appointment reserved.
                    //component.set("v.pendingMessageDetails", 'Appointment Required'); 
                    if(ret.UserActionValue == 'requestNewAppoinmentFromRSP' && dataIs.Engagement_Type__c && dataIs.Engagement_Type__c.toUpperCase() === 'APPOINTMENT'  && dataIs.Appointment_Status__c && dataIs.Appointment_Status__c.toUpperCase() ==='RESERVED'){
                        component.set("v.pendingMessageDetails", 'Appointment Reschedule Required'); 
                    }
                    else {
                        component.set("v.pendingMessageDetails", 'Appointment Required'); 
                    }
                }else {
                    component.set("v.pendingMessageDetails", ret.pendingMessageDetails); 
                }
            }
            //Added by RM CUSTSA-26909
                else if(ret.Current_status == 'Rejected' || ret.Current_status == 'Cancelled'){
                    component.set("v.Flag", "See Recent History for details");
                    component.set("v.Current_status", ret.Current_status);
                }
                    else if(ret.Current_status == 'Closed'){
                        component.set("v.Flag", "See Recent History below for full closure info.");
                        component.set("v.Current_status", ret.Current_status);
                    }
            // Added the below code for CUSTA#11034.
                        else if(ret.Current_status == 'Resolved') {
                            ret.Details = dataIs.Status_Reason__c;
                            component.set("v.Current_status", ret.Current_status);
                            component.set("v.Flag", "See Recent History below for full resolution info.");
                            component.set("v.strSuggestedAction", ret.suggestedAction); 
                            // Added the below code for CUSTA#11034.
                            component.set("v.hideSearchResolved",true); 
                            component.set("v.resolvedMessageDetails", ret.resolvedMessageDetails); 
                            component.set("v.tier1Msg", ret.tier1Msg);
                            component.set("v.tier2Msg", ret.tier2Msg);
                            component.set("v.tier3Msg", ret.tier3Msg);
                            component.set("v.resolutionDate",ret.resolutionDate);
                            component.set("v.isResolved", true);
                        }
            // Added the below code for CUSTA#10163.
                            else if(ret.Current_status == 'On Hold') {
                                ret.Details = dataIs.Status_Reason__c;
                                component.set("v.Current_status", ret.Current_status);
                                component.set("v.Flag", "See Recent History for details.");
                                component.set("v.strSuggestedAction", ret.suggestedAction); 
                                // Added the below code for CUSTA#10163.
                                component.set("v.hideSearch",true);
                                component.set("v.showPRD",true);
                                component.set("v.pendingMessageDetails", ret.pendingMessageDetails);
                                console.log("Pending Message Details :- " + ret.pendingMessageDetails);
                            }
                                else if(ret.Current_status == 'In Progress') {
                                    component.set("v.Current_status", 'Incident Accepted');
                                } 
                                    else {
                                        component.set("v.Current_status", ret.Current_status);
                                    }
            component.set("v.Current_status_msg", ret.Current_status_msg);
            component.set("v.Details", ret.Details);
            component.set("v.displayDateTimeSlot", ret.displayDateTimeSlot);
            component.set("v.isResolved", ret.isResolved);
            component.set("v.Information_Summary", ret.Information_Summary);
            console.log("Current Status Msg :- " + ret.Current_status);
            if (ret.suggestedAction != '' || ret.suggestedAction != 'undefined') {
                component.set("v.strSuggestedAction", ret.suggestedAction);
            }
            if (dataIs.Engagement_Type__c || (dataIs.Industry_Status__c == 'Resolved' && !dataIs.Industry_Sub_Status__c && dataIs.Incident_Status__c == 'Resolved')) {
                var monthArr = new Array(12);
                monthArr[0] = "Jan";
                monthArr[1] = "Feb";
                monthArr[2] = "March";
                monthArr[3] = "April";
                monthArr[4] = "May";
                monthArr[5] = "June";
                monthArr[6] = "July";
                monthArr[7] = "Aug";
                monthArr[8] = "Sep";
                monthArr[9] = "Oct";
                monthArr[10] = "Nov";
                monthArr[11] = "Dec";
                var datesDate = ret;
                var dateChoose;
                var dateStart;
                var dateEnd;
                if (dataIs.Engagement_Type__c == 'Appointment') {
                    dateChoose = datesDate.AppStartdt;
                    dateStart = datesDate.AppStartdt;
                    dateEnd = datesDate.AppEnddt;
                    
                } else if (dataIs.Engagement_Type__c == 'Commitment') {
                    dateChoose = datesDate.CmtDt;
                    dateStart = datesDate.CmtDt;
                    dateEnd = '';
                }
                if (dataIs.Industry_Status__c == 'Resolved' && !dataIs.Industry_Sub_Status__c && dataIs.Incident_Status__c == 'Resolved') {
                    component.set("v.displayTier", true);
                    dateChoose = datesDate.ResDt;
                    dateStart = datesDate.ResDt;
                    dateEnd = '';
                    component.set("v.CompleteDate", true);
                }
                var dd = new Date(dateChoose);
                if (dd.getDate()) {
                    component.set("v.AppDateIS", dd);
                }
                component.set("v.AppMonthIS", monthArr[dd.getMonth()]);
                var startTime = new Date(dateStart);
                var hours = (startTime.getHours());
                var mid = 'am';
                if (hours == 0) { //At 00 hours we need to show 12 am
                    hours = 12;
                } else if (hours >= 12) {
                    if (hours !== 12) {
                        hours = hours - 12;
                    }
                    mid = 'pm';
                }
                var sminutes = startTime.getMinutes();
                if (sminutes < 10) {
                    sminutes = "0" + sminutes;
                }
                var startTimeSlot = hours + ':' + sminutes + ' ' + mid;
                var endTime = new Date(dateEnd);
                var hours1 = (endTime.getHours());
                var mid1 = 'am';
                if (hours1 == 0) { //At 00 hours we need to show 12 am
                    hours1 = 12;
                } else if (hours1 >= 12) {
                    if (hours1 !== 12) {
                        hours1 = hours1 - 12;
                    }
                    mid1 = 'pm';
                }
                var minutes = endTime.getMinutes();
                if (minutes < 10) {
                    minutes = "0" + minutes;
                }
                var endTimeSlot = ' - ' + hours1 + ':' + minutes + ' ' + mid1;
                if (dateEnd == '') {
                    endTimeSlot = ''
                }
                var Tslot = startTimeSlot + endTimeSlot;
                if (Tslot.indexOf('NaN') == -1) {
                    component.set("v.TimeSlot", Tslot);
                }
            }
            helper.applyCSS(component);
            helper.strikeThroughLogic(component,helper,ret,dataIs);
        }
        // START: Add code wrt- CUSTSA-25647 : Display rejection count on incident status.
        // Commented below code as per requirement change to not show count for now.
        /*if(dataIs != null && dataIs.Resolution_Rejection_Count__c > 0){
            component.set("v.showRejectionCount", true);
            if(dataIs.Resolution_Rejection_Count__c == 1)
                component.set("v.countText", "st resolution rejection from RSP");
            if(dataIs.Resolution_Rejection_Count__c == 2)
                component.set("v.countText", "nd resolution rejection from RSP");
            if(dataIs.Resolution_Rejection_Count__c == 3)
                component.set("v.countText", "rd resolution rejection from RSP");
            if(dataIs.Resolution_Rejection_Count__c > 3)
                component.set("v.countText", "th resolution rejection from RSP");
        }
        else{
            component.set("v.showRejectionCount", false);
            component.set("v.countText", "");
        } */
        // END
    },
    strikeThroughLogic : function(component,helper,ret,dataIs){
        if (ret != undefined && ret.inm != undefined) {
            if (ret.inm.Appointment_Type__c == 'Appointment') {
                /*Created by RM CUSTSA-27555*/
                if (ret.inm.Current_Status__c == 'Tech Offsite' && ret.inm.Appointment_Status__c && (ret.inm.Appointment_Status__c.toUpperCase() == 'COMPLETED' || ret.inm.Appointment_Status__c.toUpperCase() == 'INCOMPLETE')) {
                    /*AC01*/
                    component.set("v.strikeThrough", true);
                } else if ((ret.inm.Current_Status__c == 'On Hold' || ret.inm.Current_Status__c == 'Resolution Rejected' || ret.inm.Current_Status__c == 'Pending') && ret.inm.Appointment_Status__c && (ret.inm.Appointment_Status__c.toUpperCase() == 'COMPLETED' || ret.inm.Appointment_Status__c.toUpperCase() == 'INCOMPLETE' || ret.inm.Appointment_Status__c.toUpperCase() == 'CANCELLED')) {
                    /*AC02*/
                    component.set("v.strikeThrough", true);
                } else if (ret.pendingGroupName != undefined && ret.pendingGroupName == "Appointment Reschedule Required" && ret.inm.Current_Status__c == 'Pending' && ret.inm.Engagement_Type__c == 'Appointment' && ret.inm.Appointment_Status__c && (ret.inm.Appointment_Status__c.toUpperCase() == "RESERVED" || ret.inm.Appointment_Status__c.toUpperCase() == "BOOKED")) {
                    /*AC03*/
                    component.set("v.strikeThrough", true);
                }
            }else if(ret.inm.Appointment_Type__c == 'Commitment'){
                /*Created by RM CUSTSA-27998*/
                if (ret.inm.Current_Status__c == 'Tech Offsite' && (ret.Current_status_msg == "Commitment Completed" || ret.Current_status_msg == "Commitment Incomplete")) {
                    /*AC01*/
                    component.set("v.strikeThrough", true);
                }else if ((ret.inm.Current_Status__c == 'On Hold' || ret.inm.Current_Status__c == 'Resolution Rejected' || ret.inm.Current_Status__c == 'Pending') && (ret.Current_status_msg == 'Commitment Completed' || ret.Current_status_msg == 'Commitment Incomplete' || ret.Current_status_msg == 'Commitment Cancelled')) {
                    /*AC02*/
                    component.set("v.strikeThrough", true);
                }
            }

        }
    },
    applyCSS: function(component) {
        console.log("Inside Apply CSS.");
        var bgColorTarget = component.find('incidentStatusBgColor');
        //LDS to pick up changes and update the class with new values
        if ($A.util.hasClass(bgColorTarget, 'bg-yellow'))
            $A.util.removeClass(bgColorTarget, 'bg-yellow');
        if ($A.util.hasClass(bgColorTarget, 'bg-amber'))
            $A.util.removeClass(bgColorTarget, 'bg-amber');
        if ($A.util.hasClass(bgColorTarget, 'bg-grey'))
            $A.util.removeClass(bgColorTarget, 'bg-grey');
        if ($A.util.hasClass(bgColorTarget, 'bg-green'))
            $A.util.removeClass(bgColorTarget, 'bg-green');
        if ( component.get("v.DetailsRSP") == 'Updating Current Status') {
            $A.util.addClass(bgColorTarget, 'bg-grey');
        } else {
            if (component.get("v.Current_status") == 'Resolution Rejected' || component.get("v.Current_status") == 'Pending') {
                $A.util.addClass(bgColorTarget, 'bg-yellow');
            } else if (component.get("v.Current_status") == 'On Hold' || component.get("v.Current_status") == 'Rejected' || component.get("v.Current_status") == 'Cancelled' || component.get("v.DetailsRSP") == 'Updating Current Status') {
                $A.util.addClass(bgColorTarget, 'bg-grey');
            } else if (component.get("v.Current_status") == 'Resolved' || component.get("v.Current_status") == 'Closed') {
                console.log('Inside Resolved Status');
                $A.util.addClass(bgColorTarget, 'bg-green');
            }
                else if (component.get("v.Current_status") == 'Awaiting Technician' || component.get("v.Current_status") == 'Tech Onsite') {
                    $A.util.addClass(bgColorTarget, 'bg-amber');
                }
        }
        //Add Styles To Icon Target
        var iconTarget = component.find('incidentStatusIconClass');
        //LDS to pick up changes and update the class with new values - create a method to remove the below
        if ($A.util.hasClass(iconTarget, 'in-progress'))
            $A.util.removeClass(iconTarget, 'in-progress');
        if ($A.util.hasClass(iconTarget, 'accepted'))
            $A.util.removeClass(iconTarget, 'accepted');
        if ($A.util.hasClass(iconTarget, 'truck-roll'))
            $A.util.removeClass(iconTarget, 'truck-roll');
        if($A.util.hasClass(iconTarget, 'tech-onsite'))
            $A.util.removeClass(iconTarget, 'tech-onsite');
        if($A.util.hasClass(iconTarget, 'pause'))
            $A.util.removeClass(iconTarget, 'pause');
        if($A.util.hasClass(iconTarget, 'resolution-rejected'))
            $A.util.removeClass(iconTarget, 'resolution-rejected');
        if($A.util.hasClass(iconTarget, 'rejected'))
            $A.util.removeClass(iconTarget, 'rejected');
        if($A.util.hasClass(iconTarget, 'cancelled'))
            $A.util.removeClass(iconTarget, 'cancelled');
        if($A.util.hasClass(iconTarget, 'empty-status'))
            $A.util.removeClass(statusEmptyTarget, 'empty-status');
        if (component.get("v.Current_status") == 'In Progress' || component.get("v.Current_status") == 'Tech Offsite') {
            component.set("v.incidentStatusIcon", $A.get('$Resource.nbnSpogiconStatusToolsWhite'));
            $A.util.addClass(iconTarget, 'in-progress');
        } else if (component.get("v.Current_status") == 'Incident Accepted') {
            component.set("v.incidentStatusIcon", $A.get('$Resource.nbnSpogiconStatusPersonCallCentre'));
            $A.util.addClass(iconTarget, 'accepted');
        } else if (component.get("v.Current_status") == 'Awaiting Technician') {
            component.set("v.incidentStatusIcon", $A.get('$Resource.nbnSpogiconStatusTruckRoll'));
            $A.util.addClass(iconTarget, 'truck-roll');
        } else if (component.get("v.Current_status") == 'Tech Onsite') {
            component.set("v.incidentStatusIcon", $A.get('$Resource.nbnSpogiconStatusPersonFieldTechnician'));
            $A.util.addClass(iconTarget, 'tech-onsite');
        }else if (component.get("v.Current_status") == 'Resolved' || component.get("v.Current_status") == 'Closed') {
            component.set("v.incidentStatusIcon", $A.get('$Resource.nbnSpogiconStatusChecked'));           
        }else if (component.get("v.Current_status") == 'Pending' || component.get("v.Current_status_msg") == 'Held') {
            component.set("v.incidentStatusIcon", $A.get('$Resource.nbnSpogiconStatusPause'));
            $A.util.addClass(iconTarget, 'pause');
        }else if (component.get("v.Current_status") == 'Resolution Rejected') {
            component.set("v.incidentStatusIcon", $A.get('$Resource.nbnSpogiconStatusQuestion'));
            $A.util.addClass(iconTarget, 'resolution-rejected');
        }else if (component.get("v.Current_status") == 'Pending' || component.get("v.Current_status") == 'On Hold') {
            component.set("v.incidentStatusIcon", $A.get('$Resource.nbnSpogiconStatusPause'));
            $A.util.addClass(iconTarget, 'pause');
        }else if (component.get("v.Current_status") == 'Rejected') {
            component.set("v.incidentStatusIcon", $A.get('$Resource.nbnSpogiconStatusRejected'));
            $A.util.addClass(iconTarget, 'rejected');
        }else if (component.get("v.Current_status") == 'Cancelled') {
            component.set("v.incidentStatusIcon", $A.get('$Resource.nbnSpogiconStatusCancelled'));
            $A.util.addClass(iconTarget, 'cancelled');
        }else if (!component.get("v.Current_status")) {
            var statusEmptyTarget = component.find('incidentStatusEmpty');
            $A.util.addClass(statusEmptyTarget, 'empty-status');
        }
    },
    getTimezone: function(component, LOC) {
        console.log('>>>>> LOC ', LOC);
        var action = component.get("c.getTimeZone");
        action.setParams({
            "strlocationStateIs": LOC
        });
        action.setCallback(this, function(a) {
            var state = a.getState();
            console.log('state113 ', state);
            //check if result is successfull
            if (state == "SUCCESS") {
                var result = a.getReturnValue();
                console.log('result ASDF ', result);
                if (!$A.util.isEmpty(result) && !$A.util.isUndefined(result)) {
                    //if(!$A.util.isEmpty(result[i])){
                    console.log('timezone ', a.getReturnValue());
                    component.set("v.timeZone", result);
                    //}  
                } else {}
            } else if (state == "ERROR") {}
        });
        $A.enqueueAction(action);
    },
    setAppointmentDetailsTransactionId : function(component,dataIs)
    {
        var action = component.get("c.getAppointmentDetailsRequest");
        action.setParams({
            "appointmentId": dataIs.AppointmentId__c,
            "accessSeekerId": dataIs.Access_Seeker_ID__c,
            "integrationType" : "postGetAppointment"
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Appointment Details Transaction Id :- "+ response.getReturnValue());
                if (!$A.util.isUndefined(response.getReturnValue())  && !$A.util.isEmpty(response.getReturnValue())) {
                    component.set("v.appointmentDetailsTransactionId", response.getReturnValue());
                }
				// CUSTSA-30242 : Placehold Event to enable the Location Panel in case of Toggeld off CIS Callout
                var appEvent = $A.get("e.c:JIGSAW_AppointmentDetails");
                appEvent.setParams({
                    "appointmentDetails" : response.getReturnValue(),
                    "recordId" : component.get("v.recordId")
                });
                appEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    getAppointmentDetailsFromCIS_Incident_Status : function(component,helper,dataIs)
    {
        var apptResponse;
        var action = component.get("c.getAppointmentDetailsFromCIS");
        action.setParams({
            "integrationType": "getAppointmentDetailsCIS",
            "accessSeekerId" : dataIs.Access_Seeker_ID__c,
            "appointmentId": dataIs.AppointmentId__c
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                apptResponse = response.getReturnValue();
                if (apptResponse){
                    if (apptResponse.httpStatusCode == "200"){
                        component.set("v.appointmentResponse",apptResponse);
                        //fire event with the success response
                    }
                    else if (apptResponse.httpStatusCode != "200"){
                        component.set("v.appointmentResponse",apptResponse);
                        //fire event with the error response
                    }
                }
                var appEvent = $A.get("e.c:JIGSAW_AppointmentDetails");
                appEvent.setParams({
                    "appointmentDetails" : component.get("v.appointmentResponse"),
                    "recordId" : component.get("v.recordId")
                });
                appEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    checkTimer:  function(component,event,helper) {
        var currentRunningComponent = this;
        console.log('waiting Time to set for Current Status'  + component.get('v.waitingTime')/60000);
        window.setTimeout(
            $A.getCallback(function() {
                if(component.isValid())
                {
                    if(!component.get("v.currentStatusReceived") && !component.get("v.showIncidentStatusError"))
                    {
                        helper.helperMethod(component,event,helper,false);
                        var appEvent = $A.get("e.c:WaitingForUpdate");
                        appEvent.setParams({
                            "waitingForUpdate" : false
                        });
                        appEvent.fire();	
                        /*component.set("v.showErrorHandlingButton",false);
                        component.set("v.showIncidentStatusError",true);
                        component.set("v.showIncidentStatus",false);*/
                    }
                }
            }),component.get('v.waitingTime')  //time after which the component reloads to check for any change in the record data
        );
    },
    cancelAppointment : function(component,event,helper) {
        var action = component.get("c.cancelAppointmentManually");
        action.setParams({
            "currentIncidentRecordId": component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var varattrCurrentIncientRecord = response.getReturnValue();
                var spinnersToFire = '';
                if (varattrCurrentIncientRecord) {
                    if(varattrCurrentIncientRecord.Awaiting_Current_Incident_Status__c)
                        spinnersToFire = 'IncidentStatus';
                    if(varattrCurrentIncientRecord.Awaiting_Current_SLA_Status__c)
                        spinnersToFire = spinnersToFire + ',' +'SLA';
                    var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
                    spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : varattrCurrentIncientRecord, "spinnersToStart" : spinnersToFire, "stopSpinner" : false});
                    spinnerHandlerEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    }
})