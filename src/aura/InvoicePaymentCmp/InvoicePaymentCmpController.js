({
	handleAppEvent: function(component, event, helper) {
		var appEventParams = event.getParam("invokePaymentComponentwithOptionsparams");
        if(appEventParams['applicationId']){
            component.set("v.AppId", appEventParams['applicationId']);
            component.set("v.showNewDevPayByInvoiceButton",true);
            component.set("v.showNewDevInvoicePaymentError", false);
        }
        else {
            component.set("v.showNewDevPayByInvoiceButton",false);
            component.set("v.showNewDevInvoicePaymentError",true);        
        }        
	},
    
    doSubmitPaybyInvoice: function(component, event, helper) {
		var appId = component.get("v.AppId");
        helper.logInvoicePaymentTxn(component, appId);
	}
})