({
	logInvoicePaymentTxn : function(cmp, appId) {
		var paymentCevent = $A.get("e.c:paymentComponentResponse");
        var paramMap = cmp.get("v.paymentComponentResponseParams");
        var action = cmp.get("c.logInvoicepaymenttransaction");
        action.setParams({
            applicationId : appId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                paramMap['transactionName'] = 'paybyInvoice';
        		paramMap['status'] = 'success';
                paymentCevent.setParams({
            		"paymentComponentResponseParams" : paramMap
        		});
                
            }
            else if (state === "ERROR") {
                console.log("Error: " + errorMessage);
                //Fire event paymentComponentResponse with error results of Payment Component Rendering
            	paramMap['transactionName'] = 'paybyInvoice';
        		paramMap['status'] = 'error';
                paymentCevent.setParams({
            		"paymentComponentResponseParams" : paramMap
        		});
            }
            paymentCevent.fire();
        });
        $A.enqueueAction(action);
	}
})