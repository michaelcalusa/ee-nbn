({
	performAction : function(component, event, helper) {
		component.set('v.docComposerComponentVisible',true);
        helper.loadReasonValues(component, event, helper);
	},
    
    submitResponse : function(component, event, helper){
        helper.submitResponseOnBehalfOfRSP(component, event, helper);
        helper.resetAttributes(component, event, helper);
    },
    
    acceptClickAction : function(component, event, helper){
        component.set('v.selectedOption','Accept');
        component.set('v.disableSubmitButton',false);
    },
    
    rejectClickAction : function(component, event, helper){
        component.set('v.selectedOption','Reject');
        helper.validate(component, event, helper);
    },
    
    handleCloseConfirmed: function(component, event, helper) {
        helper.resetAttributes(component, event, helper);
    },
    
    validate : function(component, event, helper){
       helper.validate(component, event, helper);
    }
})