({
    resetAttributes : function(component, event, helper){
        component.set('v.selectedOption','');
        component.set('v.disableSubmitButton',true);
        component.set("v.rejectReason",'');
        component.set("v.rejectComment",'');
        component.set("v.docComposerComponentVisible",false);
        component.set("v.confirmationModalVisible",false);
        component.set("v.docComposerVisible",false);
    },
    loadReasonValues : function(component, event, helper) {
        component.set('v.ServerSideAction','c.getPickListValuesIntoList');
        component.set('v.useCache',true);
        component.set('v.ServerSideActionFaliure',$A.get("$Label.c.GenericError"));
        helper.performOperatorAction(component,{
                                                "actionType": 'Resolution Rejection Reason'
                                                }, (response, component, self) => {
                                                    var records = JSON.parse(response.getReturnValue());
                                                    component.set('v.reasonValues', records.map(function (item) {
                                                        return {
                                                            label: item,
                                                            value: item
                                                        };
                                                    }));
                                            		component.set('v.docComposerVisible',true);
                                                    component.set("v.disableActionLink",true);
                                                });
    },
    submitResponseOnBehalfOfRSP : function(component, event, helper) {
        var self = this;
        var action = component.get("c.acceptRejectForRSP");
        var selectedOption  = component.get("v.selectedOption");
        var rejectReason = component.get("v.rejectReason");
        var rejectComment = component.get("v.rejectComment");
    
        if(selectedOption){
            component.set('v.ServerSideAction','c.acceptRejectForRSP');
            component.set('v.useCache',false);
            component.set('v.ServerSideActionFaliure',$A.get("$Label.c.JIGSAW_AcceptRejectForRSPErrorMessage"));
            helper.performOperatorAction(component,{
                                                    "selectedOption": selectedOption,
                                                    "actionName": selectedOption == 'Accept' ? "rspAcceptResolution" : "rspRejectResolution",
                                                    "IncidentRecordId": component.get('v.incidentRecordObj.Id'),
                                                    "rejectReason" : rejectReason,
                                                    "rejectComment" : rejectComment
                                                }, (response, component, self1) => {
                                                        var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
                                                        addNoteRefreshEvent.fire();
                                                        var inm = response.getReturnValue();
                                                        self.fireSpinnerEvent(inm);
                                                    	self.resetAttributes(component, event, helper);
                                                    });
        }
    },
    validate : function(component, event, helper){
        var rejectReason = component.get("v.rejectReason");
        var rejectComment = component.get("v.rejectComment");
        if(rejectReason != undefined && rejectReason != '' && rejectComment != undefined && rejectComment != '' && rejectComment.trim() != ''){
            component.set('v.disableSubmitButton',false);
        }else{
            component.set('v.disableSubmitButton',true);
        }
    }
})