({
    performAction: function(component, event, helper){
        //disable all the links
        component.set("v.disableActionLink",true);        
        helper.executeChildAction(component, event, helper);
    },
    submitResponse : function(component, event, helper){
        var actionCmp = component.getConcreteComponent();
		var action = actionCmp.get('c.submitResponse');
        // Calling Sub Component Controller Action Named As PerformAction
        $A.enqueueAction(action);
    },
    handleCancelConfirmationModal : function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
    },
    handleCloseConfirmed : function(component, event, helper) {
        component.set("v.docComposerVisible",false);
        component.set("v.docComposerComponentVisible",false);
        component.set("v.confirmationModalVisible",false);
        component.set("v.disableActionLink",false);
        let subCompAction = component.getConcreteComponent().get('c.handleCloseConfirmed');
        if(subCompAction){
            $A.enqueueAction(subCompAction);
        }
    },
    handleMinimizeMaximize : function(component, event, helper) {
        var section = component.find("dcSection");
        if($A.util.hasClass(section, 'slds-is-open')){
            $A.util.addClass(section, 'slds-is-closed');
            $A.util.removeClass(section, 'slds-is-open');
        }else{
            $A.util.removeClass(section, 'slds-is-closed');
            $A.util.addClass(section, 'slds-is-open');
        }
    },
    handleClose : function(component, event, helper) {
        component.set("v.confirmationModalVisible",true);
    },
})