({
	executeChildAction : function(component, event, helper) {
		var actionCmp = component.getConcreteComponent();
		var action = actionCmp.get('c.performAction');
        // Calling Sub Component Controller Action Named As PerformAction
        $A.enqueueAction(action);
	},
    performOperatorAction : function(component, params, callback){
        var self = this;
        var action = component.get(component.get("v.ServerSideAction"));
        action.setParams(params);
        
        if(component.get('v.useCache')){
            action.setStorable();
        }
        action.setCallback(this, (response) => {
            var state = response.getState();
            if (state === "SUCCESS") {
            	// Calling CallBack Method if Passed in param from subcomponent. 
                if (callback) callback(response, component, self);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": component.get('v.ServerSideActionFailureHeader'),
                        "type": "error",
                        "message": component.get('v.ServerSideActionFaliure')
                    });
                    toastEvent.fire();
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                          errors[0].message + " IN " + component.get("v.ComponentName"));
                    }
                } else {
                    console.log("Unknown error IN " + component.get("v.ComponentName"));
                }
            }
        });
        $A.enqueueAction(action);
    },
    fireSpinnerEvent : function(inm){
        var spinnersToFire = inm.Awaiting_Current_Incident_Status__c ? 'IncidentStatus' : '';
        spinnersToFire = inm.Awaiting_Current_SLA_Status__c ? spinnersToFire + 'SLA' : spinnersToFire;
        var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
        spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : inm, "spinnersToStart" : spinnersToFire, "stopSpinner" : false});
        spinnerHandlerEvent.fire();
    },
    callBack : function(){
        conspole.log('decalaration only'); 
    },
    disableSubmitButton : function(Cmp, isdisabled){
        Cmp.set('v.disableSubmitButton',true);
    },
    resetAttributes : function(component, event, helper){
        component.set("v.docComposerComponentVisible",false);
        component.set("v.confirmationModalVisible",false);
        component.set("v.docComposerVisible",false);
    },
    refreshActionPanel : function(component, helper){
        var currentRunningComponent = this;
        console.log('BaseOpAction : Initiating Refreshing Action Panel');
        window.setTimeout(
            $A.getCallback(function() {
                if(component.isValid())
                {
                    console.log('BaseOpAction : Calling Refreshing Action Panel');
                    let inm = component.get('v.incidentRecordObj');
                    if(inm){
                        let waitingTime = new Date() - new Date(inm.Recent_Action_Triggered_Time__c);
                        if(((waitingTime) < $A.get("$Label.c.JIGSAW_Action_Waiting_Time_Limit"))
                           && component.get("v.disableActionLink")
                           && !inm.Awaiting_Current_Incident_Status__c
                           && !inm.Awaiting_Current_SLA_Status__c){
                            var appEvent = $A.get("e.c:WaitingForUpdate");
                            appEvent.setParams({
                                "waitingForUpdate" : false
                            });
                            appEvent.fire();	
                            currentRunningComponent.refreshActionPanel(component, event, helper);
                        }
                    }
                }
            }),5000  //time after which the component reloads to check for any change in the record data
        );
    }
})