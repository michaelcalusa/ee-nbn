({

    doInit: function(component, event, helper) {
        //helper.getVisibility(component, event, helper);
        if (component.get("v.isHealthCardAvailable") == "true") {
            helper.requestHealthCard(component, event, helper, null);
            component.set('v.expanded', false);
        }
    },
	handleCurrentIncidentRecord: function(component, event, helper) {
        if(component.get('v.recordId') === event.getParam("recordId")){
            var incidentData = event.getParam("currentIncidentRecord");
            let curStatus = incidentData.inm.Current_Status__c;
            if(curStatus != 'Closed' && curStatus != 'Cancelled' && curStatus != 'Rejected'){
               component.set('v.isTerminal',false);
            }
            else{
                component.set('v.isTerminal',true);
            }
        }
    },
    handleClickCPE: function(component, event, helper) {
        component.set('v.selectedCPEHistoryTabId', 'two');
    },

    handleAccordion: function(component, event, helper) {
        $A.util.toggleClass(component.find('blockA'), 'slds-hide');


        if (component.find('iconAccordion').get('v.iconName') == 'utility:chevrondown') {
            component.find('iconAccordion').set('v.iconName', 'utility:chevronright');
            component.find('spanAccordianTitle').getElement().innerText = 'Show More';
            component.find('spanAccordianTitle').getElement().title = 'Show More';
            component.set('v.expanded', false);
        } else {
            component.find('iconAccordion').set('v.iconName', 'utility:chevrondown');
            component.find('spanAccordianTitle').getElement().innerText = 'Show Less';
            component.find('spanAccordianTitle').getElement().title = 'Show Less';
            component.set('v.expanded', true);
        }

    },
    handleErrorEvent: function(component, event, helper) {
        if (event.getParam('recordId') == component.get('v.recordId') && event.getParam('errorKey') == 'RejectedIncident') {
            component.set('v.isRejectedIncident', true);
        }
    },
    handleSnapshotClick: function(component, event, helper) {
        helper.takeSnapshot(component, event, helper);
    },
    handleToggleSnapshot: function(component, event, helper) {
        helper.toggleToCurrentHealthCard(component, event, helper);
    },
    selectSnapshotAction: function(component, event, helper) {
        component.set('v.isOnCurrentServiceHealthCardView',false);
        var selectedSnapshotId = component.get('v.selectedFilter');

        if (selectedSnapshotId == "current") {
            component.set("v.isSnapshot", false);
        } else {
            
            var optionList = component.get('v.options');

            var selectedOption = optionList.find(function(a) {
                return a.value == selectedSnapshotId;
            });

            var selectedDatetime = selectedOption.label.split(' by ')[0];
            var snapshotText = 'Snapshot on '+selectedDatetime;
            
            component.set('v.snapshotHeaderText', snapshotText );
            component.set("v.isSnapshot", true);
        }
        component.set("v.selectedSnapshotId", selectedSnapshotId );
        
        helper.requestHealthCard(component, event, helper, selectedSnapshotId);
    }
})