({

    requestHealthCard: function(component, event, helper, snapshotId) {

        component.set("v.options", [{
            "label": "Most Recent Service Health",
            "value": "current"
        }]);

        if (snapshotId == 'current') {
            var result = component.get("v.SHCResult");
            helper.processHealthCard(component, event, helper, result);
        } else {

            var action = component.get("c.requestHealthCard");
            action.setBackground();
            var recordId = component.get("v.recordId");

            action.setParams({
                "incidentId": recordId,
                "snapShotId": snapshotId
            });
            action.setCallback(this, function(a) {
                //get the response state
                var state = a.getState();
                component.set("v.showSpinner", false);
                component.set("v.showHealthCard", true);
                if (state == "SUCCESS") {
                    var result = a.getReturnValue();
                    var fromTimer = component.get("v.fromTimer");
                    var isSnapshot = component.get("v.isSnapshot");
                    var snapshotId =  component.get("v.selectedSnapshotId");
                    
                    if(!isSnapshot && fromTimer){
                        helper.processHealthCard(component, event, helper, result);    
                    }else if(isSnapshot && !fromTimer && snapshotId != 'current'){
                        helper.processHealthCard(component, event, helper, result); 
                    }else{
                        helper.setTimer2(component, event, helper, result.refreshTimeInMinutes);
                    }
                    
                    if (fromTimer) {
                        component.set('v.SHCResult', result);
                        component.set('v.snapshotTitle', 'Save Snapshot');
                        component.set('v.fromTimer', false);
                        component.set('v.disableSnapshot', false);
                    }

                } else if (state == "ERROR") {
                    component.set("v.errorDesc", 'Data Not available');
                    component.set("v.errorCode", 'UNKNOWN');
                }
            });
            $A.enqueueAction(action);
        }

    },
    processHealthCard: function(component, event, helper, result) {

        if (!$A.util.isEmpty(result) && !$A.util.isUndefined(result)) {
            var mapErrorStrips1 = [];
            var mapErrorStrips2 = [];
            var mapErrorStrips3 = [];
            for (var key in result.mapErrorStrips) {
                if (key.indexOf('Error') != -1)
                    mapErrorStrips1.push({
                        value: result.mapErrorStrips[key],
                        key: key
                    });
                if (key.indexOf('Warning') != -1)
                    mapErrorStrips2.push({
                        value: result.mapErrorStrips[key],
                        key: key
                    });
                if (key.indexOf('Info') != -1)
                    mapErrorStrips3.push({
                        value: result.mapErrorStrips[key],
                        key: key
                    });
            }
            component.set("v.mapErrorStrips1", mapErrorStrips1);
            component.set("v.mapErrorStrips2", mapErrorStrips2);
            component.set("v.mapErrorStrips3", mapErrorStrips3);

            if (!$A.util.isEmpty(result.errorDesc) && !$A.util.isUndefined(result.errorDesc)) {
                component.set("v.errorDesc", result.errorDesc);

                if (!$A.util.isEmpty(result.errorCode) && !$A.util.isUndefined(result.errorCode))
                    component.set("v.errorCode", result.errorCode);
                else
                    component.set("v.errorCode", 'UNKNOWN');
            } else {

                helper.createSnapshotPickList(component, event, helper, result);
                
                // Health Card Payload
                var healthCardPayload = JSON.parse(result.healthCardPayload);
                var fromTimer = component.get("v.fromTimer");
                if (fromTimer) {
                    component.set("v.healthCardPayload", result.healthCardPayload);
                    component.set("v.speedPayload", result.speedPayload);

                    component.set("v.timer", 0);
                    //component.set("v.timerText", 'Last Refreshed 0 minutes ago. (Refreshed every ' + result.refreshTimeInMinutes + ' minutes)');
                    component.set("v.timerText", 'Refreshed 0 minutes ago');
                    helper.setTimer2(component, event, helper, result.refreshTimeInMinutes);
                }

                // Speed Payload
                helper.buildSpeed(component, result.speedPayload, healthCardPayload);

                // RAG Status
                helper.buildRAG(component, healthCardPayload);

                // Op Status
                helper.buildOpStatus(component, healthCardPayload);

                // DropOut
                helper.buildDropout(component, healthCardPayload);

                // CPE
                helper.buildCPE(component, healthCardPayload);

                // LineDetails
                helper.buildLineDetails(component, healthCardPayload);

                // MLT
                helper.buildMLT(component, healthCardPayload);

                //Tooltips
                helper.buildTooltips(component, healthCardPayload, result.speedPayload);

                //CPE History
                helper.buildCPEHistory(component, healthCardPayload);



                // Speed attributes
                component.set("v.performance", result.performance);
                component.set("v.currentSpeedDownStream", result.currentSpeedDownStream);
                component.set("v.currentSpeedUpStream", result.currentSpeedUpStream);
                component.set("v.averageSpeed7DownStream", result.averageSpeed7DownStream);
                component.set("v.averageSpeed7UpStream", result.averageSpeed7UpStream);
                component.set("v.averageSpeed2HrDownStream", result.averageSpeed2HrDownStream);
                component.set("v.averageSpeed2HrUpStream", result.averageSpeed2HrUpStream);
                component.set("v.speedDescription", result.speedDescription);
                component.set("v.indicatorCode", result.indicatorCode);
                component.set("v.speedIndicator", result.speedIndicator);
                component.set("v.helpSpeed", result.helpSpeed);
                component.set("v.threshold", result.threshold);
            }
        }
    },

    buildRAG: function(component, healthCardPayload) {
        let rag = {};

        if (healthCardPayload.healthCardSummary && healthCardPayload.healthCardSummary.connectivityRagStatus) {
            if (healthCardPayload.healthCardSummary.connectivityRagStatus == 'Red' ||
                healthCardPayload.healthCardSummary.connectivityRagStatus == 'Amber' ||
                healthCardPayload.healthCardSummary.connectivityRagStatus == 'Green') {
                rag.connectivityRagStatus = healthCardPayload.healthCardSummary.connectivityRagStatus;
            } else {
                rag.connectivityRagStatus = 'NA';
            }
        } else {
            rag.connectivityRagStatus = 'NA';
        }

        if (healthCardPayload.healthCardSummary && healthCardPayload.healthCardSummary.stabilityRagStatus) {
            if (healthCardPayload.healthCardSummary.stabilityRagStatus == 'Red' ||
                healthCardPayload.healthCardSummary.stabilityRagStatus == 'Amber' ||
                healthCardPayload.healthCardSummary.stabilityRagStatus == 'Green') {
                rag.stabilityRagStatus = healthCardPayload.healthCardSummary.stabilityRagStatus;
            } else {
                rag.stabilityRagStatus = 'NA';
            }

        } else {
            rag.stabilityRagStatus = 'NA';
        }

        component.set('v.rag', rag);
    },

    buildOpStatus: function(component, healthCardPayload) {
        let opStatus = {};
        if (!healthCardPayload.healthCardSummary) {
            opStatus.serviceState = 'N/A';
            opStatus.serviceStateLastChangeTimestamp = 'N/A';
        } else {
            opStatus.serviceState = this.validateAttribute(healthCardPayload.healthCardSummary.serviceState);
            opStatus.serviceStateLastChangeTimestamp = this.formatDateTime(healthCardPayload.healthCardSummary.serviceStateLastChangeTimestamp);
        }
        component.set('v.opStatus', opStatus);
    },

    buildDropout: function(component, healthCardPayload) {
        let dropOut = {};
        if (!healthCardPayload.healthCardSummary) {
            dropOut.dropoutsPast24Hours = 'N/A';
            dropOut.dropoutsPast48Hours = 'N/A';
            dropOut.dropout24HoursFault = 'N/A';
            dropOut.dropout48HoursFault = 'N/A';
            dropOut.sumReInitialisationCountersForPast24Hours = 'N/A';
            dropOut.sumReInitialisationCountersForPast48Hours = 'N/A';
            dropOut.sumLossOfPowerCountersForPast24Hours = 'N/A';
            dropOut.sumLossOfPowerCountersForPast48Hours = 'N/A';
            dropOut.sumUnavailableSecondCountersForPast24Hours = 'N/A';
            dropOut.sumUnavailableSecondCountersForPast48Hours = 'N/A';
            dropOut.sevTotal24 = 'N/A';
            dropOut.sevTotal48 = 'N/A';
            dropOut.dlmStatus = 'N/A';

        } else {
            dropOut.dropoutsPast24Hours = this.validateAttribute(healthCardPayload.healthCardSummary.dropoutsPast24Hours);
            dropOut.dropoutsPast48Hours = this.validateAttribute(healthCardPayload.healthCardSummary.dropoutsPast48Hours);
            dropOut.dropout24HoursFault = healthCardPayload.healthCardSummary.dropout24HoursFault;
            dropOut.dropout48HoursFault = healthCardPayload.healthCardSummary.dropout48HoursFault;
            dropOut.sumReInitialisationCountersForPast24Hours = this.validateAttribute(healthCardPayload.healthCardSummary.sumReInitialisationCountersForPast24Hours);
            dropOut.sumReInitialisationCountersForPast48Hours = this.validateAttribute(healthCardPayload.healthCardSummary.sumReInitialisationCountersForPast48Hours);
            dropOut.sumLossOfPowerCountersForPast24Hours = this.validateAttribute(healthCardPayload.healthCardSummary.sumLossOfPowerCountersForPast24Hours);
            dropOut.sumLossOfPowerCountersForPast48Hours = this.validateAttribute(healthCardPayload.healthCardSummary.sumLossOfPowerCountersForPast48Hours);
            dropOut.sumUnavailableSecondCountersForPast24Hours = this.formatDuration(healthCardPayload.healthCardSummary.sumUnavailableSecondCountersForPast24Hours);
            dropOut.sumUnavailableSecondCountersForPast48Hours = this.formatDuration(healthCardPayload.healthCardSummary.sumUnavailableSecondCountersForPast48Hours);
            dropOut.dlmStatus = this.validateAttribute(healthCardPayload.healthCardSummary.dlmStatus);
            var sevTotal24 = healthCardPayload.healthCardSummary.sumSeverelyErroredSecondsDownstreamForPast24Hours + healthCardPayload.healthCardSummary.sumSeverelyErroredSecondsUpstreamForPast24Hours;
            dropOut.sevTotal24 = this.formatDuration(sevTotal24);

            var sevTotal48 = healthCardPayload.healthCardSummary.sumSeverelyErroredSecondsDownstreamForPast48Hours + healthCardPayload.healthCardSummary.sumSeverelyErroredSecondsUpstreamForPast48Hours;
            dropOut.sevTotal48 = this.formatDuration(sevTotal48);
        }
        component.set('v.dropOut', dropOut);
    },

    buildCPE: function(component, healthCardPayload) {
        let cpe = {};
        if (!healthCardPayload.healthCardSummary) {
            cpe.cpeHistoryText = 'N/A';
            cpe.cpeMake = 'N/A';
            cpe.cpeModel = 'N/A';
            cpe.cpeSerialNumber = 'N/A';
            cpe.cpeFirmwareVersion = 'N/A';
            cpe.cpeCompatibility = 'N/A';
            cpe.cpeType = 'N/A';
            cpe.cpeTypeRagStatus = 'N/A';
            cpe.cpeCompatibilityRagStatus = 'N/A';

            cpe.cpeMacAddress = 'N/A';
            cpe.cpeTerminationType = 'N/A';
        } else {
            cpe.cpeHistoryText = this.designCPEHistoryText(component, healthCardPayload.healthCardSummary);
            cpe.cpeMake = this.validateAttribute(healthCardPayload.healthCardSummary.cpeMake);
            cpe.cpeModel = this.validateAttribute(healthCardPayload.healthCardSummary.cpeModel);
            cpe.cpeSerialNumber = this.validateAttribute(healthCardPayload.healthCardSummary.cpeSerialNumber);
            cpe.cpeFirmwareVersion = this.validateAttribute(healthCardPayload.healthCardSummary.cpeFirmwareVersion);
            cpe.cpeCompatibility = this.validateAttribute(healthCardPayload.healthCardSummary.cpeCompatibility);
            cpe.cpeType = this.validateAttribute(healthCardPayload.healthCardSummary.cpeType);
            cpe.cpeTypeRagStatus = this.validateAttribute(healthCardPayload.healthCardSummary.cpeTypeRagStatus);
            cpe.cpeCompatibilityRagStatus = this.validateAttribute(healthCardPayload.healthCardSummary.cpeCompatibilityRagStatus);

            cpe.cpeMacAddress = this.validateAttribute(healthCardPayload.healthCardSummary.cpeMacAddress);
            cpe.cpeTerminationType = this.validateAttribute(healthCardPayload.healthCardSummary.cpeTerminationType);
            cpe.cpeTerminationTypeTimestamp = this.formatDateTime(healthCardPayload.healthCardSummary.cpeTerminationTypeTimestamp);
            cpe.cpeMacAddressTimestamp = this.formatDateTime(healthCardPayload.healthCardSummary.cpeMacAddressTimestamp);
            cpe.cpeTimestamp = this.formatDateTime(healthCardPayload.healthCardSummary.cpeTimestamp);
            cpe.cpeChangeInLast30Days = this.validateAttribute(healthCardPayload.healthCardSummary.cpeChangeInLast30Days);
        }
        component.set('v.cpe', cpe);




    },



    buildLineDetails: function(component, healthCardPayload) {
        let lineDetails = {};
        if (!healthCardPayload.healthCardSummary) {

            lineDetails.lineLengthFromSelt = 'N/A';
            lineDetails.lineLengthFrom375 = 'N/A';
            lineDetails.calculated375Attenuation = 'N/A';
            lineDetails.loopAttenuationWarning = 'N/A';
            lineDetails.loopAttenuationDownstream = 'N/A';
            lineDetails.noiseMarginDownstream = 'N/A';
            lineDetails.noiseMarginUpstream = 'N/A';
            lineDetails.actualRateAdaptationModeDownstream = 'N/A';
            lineDetails.actualRateAdaptationModeUpstream = 'N/A';

        } else {
            var lineLengthFrom375 = this.validateAttribute(healthCardPayload.healthCardSummary.lineLengthFrom375);
            var lineLengthAccuracyFrom375 = this.validateAttribute(healthCardPayload.healthCardSummary.lineLengthAccuracyFrom375);

            if (lineLengthFrom375 != 'N/A' && lineLengthAccuracyFrom375 != 'N/A') {
                lineDetails.lineLengthFrom375 = lineLengthFrom375 + ' ' + lineLengthAccuracyFrom375;
            } else {
                lineDetails.lineLengthFrom375 = 'N/A';
            }

            var lineLengthFromSelt = this.validateAttribute(healthCardPayload.healthCardSummary.lineLengthFromSelt);
            var lineLengthAccuracyFromSelt = this.validateAttribute(healthCardPayload.healthCardSummary.lineLengthAccuracyFromSelt);

            if (lineLengthFromSelt != 'N/A' && lineLengthAccuracyFromSelt != 'N/A') {
                lineDetails.lineLengthFromSelt = lineLengthFromSelt + ' ' + lineLengthAccuracyFromSelt;
            } else {
                lineDetails.lineLengthFromSelt = 'N/A';
            }



            lineDetails.calculated375Attenuation = this.validateAttribute(healthCardPayload.healthCardSummary.calculated375Attenuation);


            lineDetails.loopAttenuationWarning = this.validateAttribute(healthCardPayload.healthCardSummary.loopAttenuationWarning);
            lineDetails.loopAttenuationDownstream = this.validateAttribute(healthCardPayload.healthCardSummary.loopAttenuationDownstream);
            lineDetails.loopAttenuationUpstream = this.validateAttribute(healthCardPayload.healthCardSummary.loopAttenuationUpstream);


            lineDetails.noiseMarginDownstream = this.validateAttribute(healthCardPayload.healthCardSummary.noiseMarginDownstream);
            lineDetails.noiseMarginUpstream = this.validateAttribute(healthCardPayload.healthCardSummary.noiseMarginUpstream);

            lineDetails.actualRateAdaptationModeDownstream = this.validateAttribute(healthCardPayload.healthCardSummary.actualRateAdaptationModeDownstream);
            lineDetails.actualRateAdaptationModeUpstream = this.validateAttribute(healthCardPayload.healthCardSummary.actualRateAdaptationModeUpstream);

            lineDetails.actualRateAdaptationModeDownstreamRagStatus = this.validateAttribute(healthCardPayload.healthCardSummary.actualRateAdaptationModeDownstreamRagStatus);
            lineDetails.actualRateAdaptationModeUpstreamRagStatus = this.validateAttribute(healthCardPayload.healthCardSummary.actualRateAdaptationModeUpstreamRagStatus);

            lineDetails.calculated375AttenuationRagStatus = this.validateAttribute(healthCardPayload.healthCardSummary.calculated375AttenuationRagStatus);

        }

        component.set('v.lineDetails', lineDetails);
    },
    buildSpeed: function(component, payload_speed, healthCardPayload) {
        let speed = {};
        speed.performance = 'na_grey';
        speed.averageNetDataRateUpstream = 'N/A';
        speed.averageNetDataRateDownstream = 'N/A';
        speed.attainableNetDataRateUpstream = 'N/A';
        speed.attainableNetDataRateDownstream = 'N/A';
        speed.attainableNetDataRateTimestamp = 'N/A';
        speed.sdcAverageNetDataRateUpstream = 'N/A';
        speed.sdcAverageNetDataRateDownstream = 'N/A';
        speed.thresholdWbaUpstream = 'N/A';
        speed.thresholdWbaDownstream = 'N/A';
        speed.thresholdUc1Upstream = 'N/A';
        speed.thresholdUc1Downstream = 'N/A';
        speed.thresholdUc2Upstream = 'N/A';
        speed.thresholdUc2Downstream = 'N/A';
        speed.thresholdBaselineDownstream = 'N/A';
        speed.thresholdBaselineUpstream = 'N/A';
        speed.thresholdTc2Upstream = 'N/A';
        speed.thresholdTc2Downstream = 'N/A';
        speed.tc2UpstreamCapable = 'N/A';
        speed.tc2DownstreamCapable = 'N/A';
        speed.indicator = 'N/A';
        speed.indicatorCode = 'N/A';
        speed.indicatorDescription = 'N/A';
        speed.incidentEligibility = 'N/A';

        speed.actualNetDataRateDownstream = 'N/A';
        speed.actualNetDataRateUpstream = 'N/A';
        speed.correctedAttainableRateDownstream = 'N/A';
        speed.correctedAttainableRateUpstream = 'N/A';

        var speedPayload;
        try {
            speedPayload = JSON.parse(payload_speed);
        } catch (err) {
            component.set('v.speed', speed);
            //return;
            // Exception in parsing JSON already handled in APEX and will be added in Error Strip.
        }

        if (speedPayload) {
            speed.indicator = this.validateAttribute(speedPayload.indicator);

            if (speed.indicator == 'Green') {
                speed.performance = 'okay_green';
            } else if (speed.indicator == 'Red') {
                speed.performance = 'bad_red';
            } else if (speed.indicator == 'Amber') {
                speed.performance = 'alert_amber';
            }


            speed.unitType = speedPayload.indicator;
            speed.averageNetDataRateUpstream = this.addUnitType(speedPayload.averageNetDataRateUpstream, speedPayload.unitType);
            speed.averageNetDataRateDownstream = this.addUnitType(speedPayload.averageNetDataRateDownstream, speedPayload.unitType);
            speed.attainableNetDataRateUpstream = this.addUnitType(speedPayload.attainableNetDataRateUpstream, speedPayload.unitType);
            speed.attainableNetDataRateDownstream = this.addUnitType(speedPayload.attainableNetDataRateDownstream, speedPayload.unitType);
            speed.attainableNetDataRateTimestamp = speedPayload.attainableNetDataRateTimestamp;
            speed.sdcAverageNetDataRateUpstream = this.addUnitType(speedPayload.sdcAverageNetDataRateUpstream, speedPayload.unitType);
            speed.sdcAverageNetDataRateDownstream = this.addUnitType(speedPayload.sdcAverageNetDataRateDownstream, speedPayload.unitType);
            speed.thresholdWbaUpstream = this.addUnitType(speedPayload.thresholdWbaUpstream, speedPayload.unitType);
            speed.thresholdWbaDownstream = this.addUnitType(speedPayload.thresholdWbaDownstream, speedPayload.unitType);
            speed.thresholdUc1Upstream = this.addUnitType(speedPayload.thresholdUc1Upstream, speedPayload.unitType);
            speed.thresholdUc1Downstream = this.addUnitType(speedPayload.thresholdUc1Downstream, speedPayload.unitType);
            speed.thresholdUc2Upstream = this.addUnitType(speedPayload.thresholdUc2Upstream, speedPayload.unitType);
            speed.thresholdUc2Downstream = this.addUnitType(speedPayload.thresholdUc2Downstream, speedPayload.unitType);
            speed.thresholdBaselineDownstream = this.addUnitType(speedPayload.thresholdBaselineDownstream, speedPayload.unitType);
            speed.thresholdBaselineUpstream = this.addUnitType(speedPayload.thresholdBaselineUpstream, speedPayload.unitType);
            speed.thresholdTc2Upstream = this.addUnitType(speedPayload.thresholdTc2Upstream, speedPayload.unitType);
            speed.thresholdTc2Downstream = this.addUnitType(speedPayload.thresholdTc2Downstream, speedPayload.unitType);
            speed.tc2UpstreamCapable = this.validateAttribute(speedPayload.tc2UpstreamCapable);
            speed.tc2DownstreamCapable = this.validateAttribute(speedPayload.tc2DownstreamCapable);

            speed.indicatorCode = this.validateAttribute(speedPayload.indicatorCode);
            speed.indicatorDescription = this.validateAttribute(speedPayload.indicatorDescription);
            speed.incidentEligibility = this.validateAttribute(speedPayload.incidentEligibility);
        }

        // few attributes coming from Health Card API
        if (healthCardPayload.healthCardSummary) {
            speed.actualNetDataRateDownstream = this.validateAttribute(healthCardPayload.healthCardSummary.actualNetDataRateDownstream);
            speed.actualNetDataRateUpstream = this.validateAttribute(healthCardPayload.healthCardSummary.actualNetDataRateUpstream);
            speed.correctedAttainableRateDownstream = this.validateAttribute(healthCardPayload.healthCardSummary.correctedAttainableRateDownstream);
            speed.correctedAttainableRateUpstream = this.validateAttribute(healthCardPayload.healthCardSummary.correctedAttainableRateUpstream);
        }
        component.set('v.speed', speed);
    },
    addUnitType: function(d, unitType) {
        if ($A.util.isEmpty(d) || d.toString().toUpperCase() == 'N/A') {
            return 'N/A';
        }
        if ($A.util.isEmpty(unitType)) {
            return d;
        }
        return d + ' ' + unitType;
    },
    buildMLT: function(component, healthCardPayload) {
        let mlt = {};

        let HCSummary = healthCardPayload.healthCardSummary;
        mlt.electricalMeasurementOfLoopTimestamp = 'N/A';
        mlt.foreignDcAbVoltage = 'N/A';
        mlt.foreignDcAEarthVoltage = 'N/A';
        mlt.foreignDcBEarthVoltage = 'N/A';
        mlt.foreignAcAbVoltage = 'N/A';
        mlt.foreignAcAEarthVoltage = 'N/A';
        mlt.foreignAcBEarthVoltage = 'N/A';
        mlt.abResistance = 'N/A';
        mlt.aEarthResistance = 'N/A';
        mlt.bEarthResistance = 'N/A';
        mlt.abCapacitance = 'N/A';
        mlt.aEarthCapacitance = 'N/A';
        mlt.bEarthCapacitance = 'N/A';
        mlt.capacitiveBalance = 'N/A';

        mlt.DC_Label = 'Foreign DC is not available';
        mlt.AC_Label = 'Foreign AC is not available';
        mlt.Resistance_Label = 'Insulating resistance is not available';
        mlt.Capacitively_Label = 'Line capacitively balanced is not available';

        // Check Notes
        mlt.isMLTAvailable = true;

        for (var key in healthCardPayload.notes) {
            if (healthCardPayload.notes[key].id == '000003' || healthCardPayload.notes[key].id == '000005') {
                mlt.isMLTAvailable = false;
                mlt.mltError = healthCardPayload.notes[key].description;
            }
        }

        if (HCSummary) {
            mlt.electricalMeasurementOfLoopTimestamp = new Date(HCSummary.electricalMeasurementOfLoopTimestamp);
            if (isNaN(mlt.electricalMeasurementOfLoopTimestamp.getTime())) {
                mlt.electricalMeasurementOfLoopTimestamp = 'N/A';
            }
            //DC
            mlt.foreignDcAbVoltage = this.validateAttribute(HCSummary.foreignDcAbVoltage);
            mlt.foreignDcAbVoltageRagStatus = this.validateAttribute(HCSummary.foreignDcAbVoltageRagStatus);

            mlt.foreignDcAEarthVoltage = this.validateAttribute(HCSummary.foreignDcAEarthVoltage);
            mlt.foreignDcAEarthVoltageRagStatus = this.validateAttribute(HCSummary.foreignDcAEarthVoltageRagStatus);

            mlt.foreignDcBEarthVoltage = this.validateAttribute(HCSummary.foreignDcBEarthVoltage);
            mlt.foreignDcBEarthVoltageRagStatus = this.validateAttribute(HCSummary.foreignDcBEarthVoltageRagStatus);


            mlt.DC_RAG = 'N/A';
            if (mlt.foreignDcAbVoltageRagStatus == 'Green' && mlt.foreignDcAEarthVoltageRagStatus == 'Green' && mlt.foreignDcBEarthVoltageRagStatus == 'Green') {
                mlt.DC_RAG = 'Green';
            }
            if (mlt.foreignDcAbVoltageRagStatus == 'Red' || mlt.foreignDcAEarthVoltageRagStatus == 'Red' || mlt.foreignDcBEarthVoltageRagStatus == 'Red') {
                mlt.DC_RAG = 'Red';
            }

            //AC
            mlt.foreignAcAbVoltage = this.validateAttribute(HCSummary.foreignAcAbVoltage);
            mlt.foreignAcAbVoltageRagStatus = this.validateAttribute(HCSummary.foreignAcAbVoltageRagStatus);

            mlt.foreignAcAEarthVoltage = this.validateAttribute(HCSummary.foreignAcAEarthVoltage);
            mlt.foreignAcAEarthVoltageRagStatus = this.validateAttribute(HCSummary.foreignAcAEarthVoltageRagStatus);

            mlt.foreignAcBEarthVoltage = this.validateAttribute(HCSummary.foreignAcBEarthVoltage);
            mlt.foreignAcBEarthVoltageRagStatus = this.validateAttribute(HCSummary.foreignAcBEarthVoltageRagStatus);


            mlt.AC_RAG = 'N/A';
            if (mlt.foreignAcAbVoltageRagStatus == 'Green' && mlt.foreignAcAEarthVoltageRagStatus == 'Green' && mlt.foreignAcBEarthVoltageRagStatus == 'Green') {
                mlt.AC_RAG = 'Green';
            }
            if (mlt.foreignAcAbVoltageRagStatus == 'Red' || mlt.foreignAcAEarthVoltageRagStatus == 'Red' || mlt.foreignAcBEarthVoltageRagStatus == 'Red') {
                mlt.AC_RAG = 'Red';
            }

            // Insulating Resistance
            mlt.abResistance = this.validateAttribute(HCSummary.abResistance);
            mlt.abResistanceRagStatus = this.validateAttribute(HCSummary.abResistanceRagStatus);

            mlt.aEarthResistance = this.validateAttribute(HCSummary.aEarthResistance);
            mlt.aEarthResistanceRagStatus = this.validateAttribute(HCSummary.aEarthResistanceRagStatus);

            mlt.bEarthResistance = this.validateAttribute(HCSummary.bEarthResistance);
            mlt.bEarthResistanceRagStatus = this.validateAttribute(HCSummary.bEarthResistanceRagStatus);


            mlt.Resistance_RAG = 'N/A';
            if (mlt.abResistanceRagStatus == 'Green' && mlt.aEarthResistanceRagStatus == 'Green' && mlt.bEarthResistanceRagStatus == 'Green') {
                mlt.Resistance_RAG = 'Green';
            }
            if (mlt.abResistanceRagStatus == 'Red' || mlt.aEarthResistanceRagStatus == 'Red' || mlt.bEarthResistanceRagStatus == 'Red') {
                mlt.Resistance_RAG = 'Red';
            }

            // Capacitively
            mlt.abCapacitance = this.validateAttribute(HCSummary.abCapacitance);
            mlt.aEarthCapacitance = this.validateAttribute(HCSummary.aEarthCapacitance);
            mlt.bEarthCapacitance = this.validateAttribute(HCSummary.bEarthCapacitance);
            mlt.capacitiveBalance = this.validateAttribute(HCSummary.capacitiveBalance);
            mlt.capacitiveBalanceRagStatus = this.validateAttribute(HCSummary.capacitiveBalanceRagStatus);


            mlt.Capacitive_RAG = 'N/A';
            if (mlt.capacitiveBalanceRagStatus == 'Green') {
                mlt.Capacitive_RAG = 'Green';
            }
            if (mlt.capacitiveBalanceRagStatus == 'Red') {
                mlt.Capacitive_RAG = 'Red';
            }

            // Tooltip section for MLT
            if (HCSummary.tooltips) {

                if (HCSummary.tooltips.foreignDcVoltageRagStatus) {
                    mlt.DC_Label = HCSummary.tooltips.foreignDcVoltageRagStatus;
                }
                if (HCSummary.tooltips.foreignAcVoltageRagStatus) {
                    mlt.AC_Label = HCSummary.tooltips.foreignAcVoltageRagStatus;
                }
                if (HCSummary.tooltips.resistanceRagStatus) {
                    mlt.Resistance_Label = HCSummary.tooltips.resistanceRagStatus;
                }
                if (HCSummary.tooltips.capacitiveBalanceRagStatus) {
                    mlt.Capacitively_Label = HCSummary.tooltips.capacitiveBalanceRagStatus;
                }
                if (mlt.DC_RAG == 'Red' || mlt.AC_RAG == 'Red' || mlt.Resistance_RAG == 'Red' || mlt.Capacitive_RAG == 'Red') {
                    mlt.viewMeasurement_RAG = 'Red';
                } else {
                    mlt.viewMeasurement_RAG = 'Green';
                }
            }
        }

        // check if any MLT attribute is available
        /*
        if(mlt.foreignDcAbVoltage == 'N/A' && mlt.foreignDcAEarthVoltage == 'N/A' &&
        	mlt.foreignDcBEarthVoltage == 'N/A' && mlt.foreignAcAbVoltage == 'N/A' &&
        	mlt.foreignAcAEarthVoltage == 'N/A' && mlt.foreignAcBEarthVoltage == 'N/A' &&
           	mlt.abResistance == 'N/A' && mlt.aEarthResistance == 'N/A' &&
           	mlt.bEarthResistance == 'N/A' && mlt.abCapacitance == 'N/A' &&
           mlt.aEarthCapacitance == 'N/A' && mlt.bEarthCapacitance == 'N/A' &&
           mlt.capacitiveBalance == 'N/A'){
            mlt.isMLTAvailable = false;
        }*/

        component.set('v.mlt', mlt);
    },

    designCPEHistoryText: function(component, objSummary) {
        var count = objSummary.numOfCpeChangesInLast30Days;
        if (!isNaN(parseInt(count))) {
            count = parseInt(count);
        } else {
            return 'N/A';
        }

        if (count == 0) {
            return 'No CPE change in last 30 days';
        }
        if (count > 0) {
            component.set('v.isCPEHistory', true);
            return count + ' CPE Change(s)';
        }
        return 'NA';

    },

    validateAttribute: function(s) {
        if ($A.util.isEmpty(s)) {
            return 'N/A';
        }
        return s;
    },

    formatDateTime: function(d) {
        if (this.validateAttribute(d) == 'N/A') return 'N/A';
        try {
            var timestamp = Date.parse(d);
            if (isNaN(timestamp) == false) {
                return $A.localizationService.formatDateTime(timestamp, "DD/MM/YYYY HH:mm");
            } else {
                return d; //sometimes "UNKNOWN" values comes from Api and we need to display same on UI.
            }

        } catch (err) {
            return 'N/A';
        }
        //return '31/01/19 11:47';
    },

    buildCPEHistory: function(component, healthCardPayload) {
        var columns = [{
            label: 'Date',
            fieldName: 'timestamp',
            type: 'date',
            typeAttributes: {
                day: "2-digit",
                month: "2-digit",
                year: "2-digit",
                hour: "2-digit",
                minute: "2-digit"
            }
        }, {
            label: 'MAC Address',
            fieldName: 'macAddresses',
            type: 'text'
        }, {
            label: 'Make',
            fieldName: 'make',
            type: 'text'
        }, {
            label: 'Model',
            fieldName: 'model',
            type: 'text'
        }, {
            label: 'Compatibility',
            fieldName: 'compatibility',
            type: 'text',
            cellAttributes: {
                iconPosition: "right",
                "class": {
                    "fieldName": "iconClass"
                },
                "iconName": {
                    "fieldName": "iconName"
                }
            }
        }, {
            label: 'Serial Number',
            fieldName: 'serialNumber',
            type: 'text'
        }, {
            label: 'Firmware',
            fieldName: 'firmwareVersion',
            type: 'text'
        }]



        component.set('v.columnsHistory', columns);

        var results = [];
        if (healthCardPayload && healthCardPayload.macAddressesForPast48Hours) {
            for (var key in healthCardPayload.macAddressesForPast48Hours) {
                var obj = {};
                obj.macAddresses = healthCardPayload.macAddressesForPast48Hours[key].value;
                obj.timestamp = healthCardPayload.macAddressesForPast48Hours[key].timestamp;
                results.push(obj);
            }
        }

        if (healthCardPayload && healthCardPayload.cpeListFromPast30Days) {
            for (var key in healthCardPayload.cpeListFromPast30Days) {
                var obj = {};
                obj.timestamp = healthCardPayload.cpeListFromPast30Days[key].timestamp;
                obj.make = healthCardPayload.cpeListFromPast30Days[key].make;
                obj.model = healthCardPayload.cpeListFromPast30Days[key].model;
                obj.compatibility = healthCardPayload.cpeListFromPast30Days[key].compatibility;

                if (healthCardPayload.cpeListFromPast30Days[key].compatibilityRagStatus == 'Green') {
                    obj.iconName = 'utility:success';
                    obj.iconClass = 'iconUp';
                }
                if (healthCardPayload.cpeListFromPast30Days[key].compatibilityRagStatus == 'Red') {
                    obj.iconName = 'utility:clear';
                    obj.iconClass = 'iconDown';
                }

                obj.serialNumber = healthCardPayload.cpeListFromPast30Days[key].serialNumber;
                obj.firmwareVersion = healthCardPayload.cpeListFromPast30Days[key].firmwareVersion;
                results.push(obj);
            }
        }

        //console.log(JSON.stringify(results));
        results.sort(this.sortBy('timestamp', true)); // false-> asc, true-> dec
        //console.log(JSON.stringify(results));
        //console.log(JSON.stringify(columns));
        //to do: change format again
        for (var key in columns) {
            if (columns[key].fieldName == 'timestamp') {
                columns[key].type = 'text';
                columns[key].typeAttributes = '';
            }
        }
        //console.log(JSON.stringify(columns));

        for (var key in results) {
            var formattedTime = results[key].timestamp;
            var formattedTime = $A.localizationService.formatDateTime(formattedTime, "DD/MM/YYYY HH:mm");
            results[key].timestamp = formattedTime;

            // to do
            //results[key].compatibility = results[key].compatibilityRagStatus + results[key].compatibility;


        }
        //console.log(JSON.stringify(results));
        component.set('v.dataHistory', results);

    },

    sortBy: function(field, reverse, primer) {
        var key = primer ?
            function(x) {
                return primer(x[field])
            } :
            function(x) {
                return x[field]
            };
        reverse = !reverse ? 1 : -1;
        return function(a, b) {
            return a = key(a) ? key(a) : '', b = key(b) ? key(b) : '', reverse * ((a > b) - (b > a));
        }
    },


    buildTooltips: function(component, healthCardPayload, speedPayload) {
        if (!healthCardPayload.healthCardSummary) return;
        if (!healthCardPayload.healthCardSummary.tooltips) return;

        let toolTips = {};
        toolTips.potsServicePresent = healthCardPayload.healthCardSummary.tooltips.potsServicePresent;
        toolTips.stabilityRagStatus = healthCardPayload.healthCardSummary.tooltips.stabilityRagStatus;
        toolTips.connectivityRagStatus = healthCardPayload.healthCardSummary.tooltips.connectivityRagStatus;

        // Op Status
        toolTips.serviceState = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.serviceState, healthCardPayload.healthCardSummary.serviceStateTimestamp);
        toolTips.serviceStateLastChangeTimestamp = healthCardPayload.healthCardSummary.tooltips.serviceStateLastChangeTimestamp;

        // Dropouts
        toolTips.dropoutFault = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.dropoutFault, healthCardPayload.healthCardSummary.dropoutFaultTimestamp);
        toolTips.sumReInitialisationCounters = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.sumReInitialisationCounters, healthCardPayload.healthCardSummary.reinitialisationCountersTimestamp);
        toolTips.sumLossOfPowerCounters = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.sumLossOfPowerCounters, healthCardPayload.healthCardSummary.lossOfPowerCountersTimestamp);
        toolTips.sumUnavailableSecondCounters = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.sumUnavailableSecondCounters, healthCardPayload.healthCardSummary.unavailableSecondCountersTimestamp);
        toolTips.dlmStatus = healthCardPayload.healthCardSummary.tooltips.dlmStatus;

        // CPE
        toolTips.cpeMacAddress = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.cpeMacAddress, healthCardPayload.healthCardSummary.cpeMacAddressTimestamp);
        toolTips.cpeTerminationType = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.cpeTerminationType, healthCardPayload.healthCardSummary.cpeTerminationTypeTimestamp);
        toolTips.cpeTimestamp = this.apendTimestamp('', healthCardPayload.healthCardSummary.cpeTimestamp);
        toolTips.cpeMake = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.cpeMake, healthCardPayload.healthCardSummary.cpeTimestamp);
        toolTips.cpeModel = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.cpeModel, healthCardPayload.healthCardSummary.cpeTimestamp);
        toolTips.cpeSerialNumber = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.cpeSerialNumber, healthCardPayload.healthCardSummary.cpeTimestamp);
        toolTips.cpeFirmwareVersion = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.cpeFirmwareVersion, healthCardPayload.healthCardSummary.cpeTimestamp);
        toolTips.cpeCompatibility = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.cpeCompatibility, healthCardPayload.healthCardSummary.cpeTimestamp);
        toolTips.cpeType = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.cpeType, healthCardPayload.healthCardSummary.cpeTypeTimestamp);

        // speed
        var currentSpeedTimestamp;
        try {
            var speedPayload = JSON.parse(speedPayload);
            currentSpeedTimestamp = speedPayload.attainableNetDataRateTimestamp;
        } catch (err) {}

        toolTips.currentSpeed = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.currentSpeed, currentSpeedTimestamp);
        toolTips.averageSpeed7Days = healthCardPayload.healthCardSummary.tooltips.averageSpeed7Days;
        toolTips.averageSpeed2Hours = healthCardPayload.healthCardSummary.tooltips.averageSpeed2Hours;
        toolTips.actualNetDataRate = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.actualNetDataRate, healthCardPayload.healthCardSummary.actualNetDataRateTimestamp);
        toolTips.correctedAttainableRate = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.correctedAttainableRate, healthCardPayload.healthCardSummary.correctedAttainableRateTimestamp);


        // Line Details

        toolTips.lineLengthFromSelt = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.lineLengthFromSelt, healthCardPayload.healthCardSummary.lineLengthFromSeltTimestamp);
        toolTips.lineLengthFrom375 = healthCardPayload.healthCardSummary.tooltips.lineLengthFrom375;
        toolTips.calculated375Attenuation = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.calculated375Attenuation, healthCardPayload.healthCardSummary.calculated375AttenuationTimestamp);
        toolTips.loopAttenuation = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.loopAttenuation, healthCardPayload.healthCardSummary.loopAttenuationDownstreamTimestamp);
        toolTips.noiseMargin = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.noiseMargin, healthCardPayload.healthCardSummary.noiseMarginTimestamp);
        toolTips.actualRateAdaptationMode = this.apendTimestamp(healthCardPayload.healthCardSummary.tooltips.actualRateAdaptationMode, healthCardPayload.healthCardSummary.actualRateAdaptationModeTimestamp);

        component.set('v.toolTips', toolTips);
    },

    apendTimestamp: function(s, d) {
        var timeStamp = this.formatDateTime(d);
        if (timeStamp != 'N/A') {
            if (typeof s != 'undefined' && s != 'undefined') {
                return '(' + timeStamp + ') ' + s;
            }
            return '(' + timeStamp + ') ';
        }
        return s;
    },


    setTimer2: function(component, event, helper, cutOff) {
        var interval = window.setTimeout(
            $A.getCallback(function() {

                if (component.isValid()) {
                    //window.clearInterval(interval);

                    var t = component.get('v.timer');
                    t = t + 1;
                    if (t == cutOff) {
                        //component.set("v.timerText", 'Last sdsdsds');
                        component.set('v.fromTimer', true);
                        helper.requestHealthCard(component, event, helper, null);
                        component.set('v.timer', -1);

                    } else {
                        component.set('v.timer', t);
                        //component.set("v.timerText", 'Last Refreshed ' + t + ' minutes ago. (Refreshed every '+ cutOff + ' minutes)');
                        component.set("v.timerText", 'Refreshed ' + t + ' minutes ago');
                        helper.setTimer2(component, event, helper, cutOff);
                    }

                }
            }), 60000
        );
    },

    getVisibility: function(component, event, helper) {
        var action = component.get("c.isHealthCardAvailable");
        action.setCallback(this, function(a) {
            if (a.getState() == "SUCCESS") {
                if (a.getReturnValue() == true) {
                    component.set("v.isHealthCardAvailable", true);
                    component.set('v.fromTimer', true);
                    helper.requestHealthCard(component, event, helper, null);
                }
            }
        });
        $A.enqueueAction(action);

    },

    formatDuration: function(val) {
        var i;
        try {
            if (!isNaN(parseInt(val))) {
                i = parseInt(val);
            } else {
                return 'N/A';
            }

            if (i < 0) {
                return 'N/A';
            }

            if (i > 0 && i < 60) {
                return i + 's';
            }

            if (i >= 60) {
                var seconds = i % 60;
                if (seconds > 0) {
                    return parseInt(i / 60) + 'min, ' + seconds + 's';
                } else {
                    return parseInt(i / 60) + 'min';
                }
            }

            return i + 's';
        } catch (err) {
            return 'N/A';
        }

    },

    createSnapshotPickList: function(component, event, helper, result) {
        var snapshotArray = result.snapShotMap;

        if (snapshotArray != undefined) {
            var tempObjArray = [];
            for (var snapId in snapshotArray) {
                var checkDuplicate = component.get("v.options").find(function(a) {
                    return a.value == snapId;
                });
                //only add if the id does not exist in the picklist to avoid duplications
                if (checkDuplicate == undefined) {
                    console.log(snapId);
                    console.log(snapshotArray[snapId]);
                    var tempObj = {};
                    tempObj["label"] = snapId;
                    tempObj["value"] = snapshotArray[snapId];
                    tempObjArray.push(tempObj);
                }

            }
            tempObjArray.unshift({
                "label": "Most Recent Service Health",
                "value": "current"
            });

            if (tempObjArray.length > 0) {
                component.set("v.options", tempObjArray);
            }
        }
    },

    takeSnapshot: function(component, event, helper) {
        component.set('v.disableSnapshot', true);
        component.set('v.snapshotTitle', 'Save Snapshot will be available on next service health refresh.');
        var healthCardPayload = component.get('v.healthCardPayload');
        var speedPayload = component.get('v.speedPayload');
        var recordId = component.get("v.recordId");
        /*Save snapshot action here*/
        var action = component.get("c.saveSnapshotAction");

        action.setParams({
            "healthCardPayload": healthCardPayload,
            "speedPayload": speedPayload,
            "recordId": recordId
        });

        action.setCallback(this, function(response) {
            var status = response.getState();
            if (status == 'SUCCESS') {
                var result = response.getReturnValue();
                if (!result.errorOccured) {
                    
                    helper.createSnapshotPickList(component, event, helper, result);
                    var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
                    addNoteRefreshEvent.fire();

                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "success",
                        "message": "Service Health snapshot has been recorded."
                    });
                    toastEvent.fire();
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "error",
                        "message": "Unable to save the Service Health snapshot."
                    });
                    toastEvent.fire();
                }
            } else {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "type": "error",
                    "message": "Unable to save the Service Health snapshot."
                });
                toastEvent.fire();
            }
        });

        $A.enqueueAction(action);
    },

    toggleToCurrentHealthCard: function(component, event, helper) {
        component.set("v.isSnapshot", false);
        component.set("v.selectedFilter", "current");
    }
})