({
	handleClickView : function(component, event, helper) {
		$A.util.toggleClass(component.find('sectionView'), 'slds-hide');
	},
    handleClickView_Speed : function(component, event, helper) {
		$A.util.toggleClass(component.find('sectionViewSpeed'), 'slds-hide');
	},
    handleClickClose : function(component, event, helper) {
		$A.util.toggleClass(component.find('sectionView'), 'slds-hide');
	},
    handleClickClose_Speed : function(component, event, helper) {
		$A.util.toggleClass(component.find('sectionViewSpeed'), 'slds-hide');
	}
})