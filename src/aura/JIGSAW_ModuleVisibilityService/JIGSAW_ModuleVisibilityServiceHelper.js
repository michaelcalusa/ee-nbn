({
	prepareConfigMap : function(component, event, helper) {
        let params = event.getParam('arguments');
        let configs = params.moduleConfigsObj;
        var inm = component.get('v.incidentRecordObj');
        if(inm){
            for(let cmp in configs){
                let moduleConfigs = configs[cmp];
                for(let conf in moduleConfigs){
                    let config = moduleConfigs[conf];
                    if(config.Visibility_Method__c && helper[config.Visibility_Method__c]){
                        config.Enabled__c = helper[config.Visibility_Method__c](inm);
                    }
                    moduleConfigs[conf] = config;
                }
                configs[cmp] = moduleConfigs;
            }
        }
	},
    displayAssignToMeAction : function(inm){
        let queueList = $A.get("$Label.c.JIGSAW_QueuesWithNoAssignToMe");
        let currentGroupName = inm.Assigned_Group__c;
        if(currentGroupName && queueList) {
            return this.checkValueInArray(currentGroupName,queueList.split(';')) ? false : true;
        }
        if(!inm.Fault_Type__c){
            return false;
        }
        return true;
    },
    displayExternalNoteAction : function(inm){

        if(!inm.assignee__c) {
            return false;
        } 
        return true;
    },
    displayConvertToAppointment : function(inm){
        let EngagementType = inm.Engagement_Type__c || '';
        let currentStatus = inm.Current_Status__c || '';
        if (this.compareValue(currentStatus,"IN PROGRESS") && this.checkValueInArray(EngagementType,["Inconclusive","Commitment"])) {
            return true;
        }
        else if (this.compareValue(currentStatus, "TECH OFFSITE")
                 && this.compareValue(EngagementType, "Commitment")
                 && this.checkValueInArray(inm.Current_work_order_status__c, ["COMPLETED","FINISHED"]))
                 {
            return true;
        }
        return false
    },
    displayConvertToCommitement : function(inm){
        let EngagementType = inm.Engagement_Type__c;
        let priorityAssist = inm.SLARegionandServiceRestorationTypeCalc__c || '';
        if (this.checkValueInArray(EngagementType,["Inconclusive","Appointment"]) && !priorityAssist.includes("Enhanced") && !priorityAssist.includes("Priority Assist")) {
            return true;
        }
        return false;
    },
    displayRequestAppointmentLink : function(inm){
        let EngagementType = inm.Engagement_Type__c;
        let currentWorkOrderStatus = inm.Current_work_order_status__c || '';
        let appoinmentStatus = inm.Appointment_Status__c || '';
        if(this.compareValue(EngagementType, "Appointment")) {
            if(inm.Technology__c && inm.Operate_And_Managed_By__c && inm.Technology__c == 'NHUR' && inm.Operate_And_Managed_By__c == 'Foxtel')
            {
                return false;
            }
            else if(inm.AppointmentId__c && (this.checkValueInArray(appoinmentStatus,["COMPLETED","CANCELLED"])))
            {
                return true;
            }
            else if(inm.AppointmentId__c && this.compareValue(appoinmentStatus, "INCOMPLETE") && this.compareValue(currentWorkOrderStatus, "PENDING"))
            {
                return true;
            }
            else if(!inm.AppointmentId__c && (!(appoinmentStatus) || this.compareValue(appoinmentStatus, "NOT RESERVED")))
            {
                return true;
            }
            // START Ref: CUSTSA-27098- show Request Appointment from RSP action for Appointment reserved.
			// Roll Back of 27098 Production Incident 
            /*if(this.compareValue(inm.Current_Status__c, "In Progress") && this.compareValue(appoinmentStatus, "RESERVED"))
            {
                return true;
            }*/
        }
        return false;
    },
    displayRequestMoreInfoLink : function(inm) {
        if(inm.Technology__c && inm.Operate_And_Managed_By__c && inm.Technology__c == 'NHUR' && inm.Operate_And_Managed_By__c == 'Foxtel')
        {
            return false;
        }
        else {
            if(this.compareValue(inm.Current_Status__c, "In Progress")) {
                return true;
            }
            let EngagementType = inm.Engagement_Type__c;
            let currentWorkOrderStatus = inm.Current_work_order_status__c || '';
            let appoinmentStatus = inm.Appointment_Status__c || '';
            if(EngagementType.toUpperCase() == 'APPOINTMENT') {
                if(appoinmentStatus.toUpperCase() == 'INCOMPLETE' && (this.checkValueInArray(currentWorkOrderStatus,["HELD","PENDING"])))
                {
                    return true;
                }
                else if(appoinmentStatus.toUpperCase() == 'COMPLETED' && (this.checkValueInArray(currentWorkOrderStatus,["FINISHED","COMPLETED"])))
                {
                    return true;
                }
            }
            else if(EngagementType.toUpperCase() == 'COMMITMENT') {
                if(this.checkValueInArray(currentWorkOrderStatus,["HELD","PENDING"])) {
                    return true;
                }
                else if((this.checkValueInArray(currentWorkOrderStatus,["FINISHED","COMPLETED"]))) {
                    return true;
                }
            }
        }
        return false;
    },
    displayDispatchTechnician : function(inm){
        let currentWorkOrderStatus = inm.Current_work_order_status__c;
        var currentCommitmentWorkOrderStatus = (currentWorkOrderStatus 
                                                && !this.checkValueInArray(currentWorkOrderStatus,["HELD","PENDING","COMPLETED","CANCELLED","CLOSED"]));
        if (inm.Awaiting_Ticket_of_Work__c) {
            return false;
        }
        //Updated Conditions as part of CUSTA-20472 User Story.
        if(inm.AppointmentId__c && this.compareValue(inm.Current_Status__c, 'IN PROGRESS'))
        {
            if(this.compareValue(inm.Engagement_Type__c, 'APPOINTMENT') && this.compareValue(inm.Appointment_Status__c,'RESERVED'))
            {
                return true;
            }
            if(this.compareValue(inm.Engagement_Type__c, 'COMMITMENT')
               && !inm.Work_Orders__r &&
               ((!currentWorkOrderStatus && inm.AppointmentId__c
                 && inm.Appointment_Status__c 
                 && inm.Appointment_Status__c.toUpperCase() == "RESERVED")
                || (currentWorkOrderStatus && currentCommitmentWorkOrderStatus))
              )
            {
                return true;
            }
        }
    },
    checkValueInArray : function(a,listOfTokens){
        var val = false;
        if(listOfTokens && Array.isArray(listOfTokens)){
            listOfTokens.forEach((value) => {
                if(!val){
                	val = this.compareValue(a,value);
            	}
            });
        }
        return val;        
    },
    compareValue : function(a,b){
        return (a && b && (a.trim().toUpperCase() == b.trim().toUpperCase())) ? true : false;
    },
    displayInternalNote : function(inm){
        
        //disabling add note for NHUR Foxtel incidents
        if(inm.Technology__c == 'NHUR' && inm.Operate_And_Managed_By__c == 'Foxtel' && (inm.Current_Status__c == 'RSP Responded' 
                            || inm.Current_Status__c == 'Resolution Rejected' || inm.Current_Status__c == 'Awaiting Technician'
                            || inm.Current_Status__c == 'Tech Onsite' || inm.Current_Status__c == 'Tech Offsite')){
            return false;
        }
        if(inm.Technology__c == 'NHUR' && (inm.Current_Status__c == 'RSP Responded' || inm.Current_Status__c == 'Resolution Rejected')){
            return false;
        }
        return true;
    },
    displayReassignIncident : function(inm){
        //disabling reassign/assign/resolve for NHUR Foxtel incidents
        if(inm.Technology__c == 'NHUR' && inm.Operate_And_Managed_By__c == 'Foxtel'){
            return false;
        }
        if(inm.Technology__c == 'NHAS' && inm.Operate_And_Managed_By__c == 'Optus' 
           && (inm.Current_Status__c == 'Awaiting Technician' || inm.Current_Status__c == 'Tech Onsite' || inm.Current_Status__c == 'Tech Offsite')){
            return false;
        }
        return true;
    },
    displayAssignReassignIncident : function(inm){
        
        //disabling reassign/assign/resolve for NHUR Foxtel incidents
        if(inm.Technology__c == 'NHUR' && inm.Operate_And_Managed_By__c == 'Foxtel'){
            return false;
        }
        return true;
    },
    displayRespondForRSP : function(inm){
        
        var robotAssigneeList = $A.get("$Label.c.JIGSAW_Auto_TOW_Robot_Name").split(';');
        //disabling respond for rsp for NHUR Foxtel incidents when assignee is robot
        if(robotAssigneeList.includes(inm.assignee__c) && inm.Technology__c == 'NHUR' && inm.Operate_And_Managed_By__c == 'Foxtel'){
            return false;
        }
        return true;
    },
    displayOpCatsAction : function(inm){
        if(inm.Technology__c == 'NHUR' && inm.Operate_And_Managed_By__c == 'Foxtel'){
            return false;
        }else{
            return true;
        } 
    },
    displayUpdatePRDAction : function(inm){
        if(inm.Technology__c == 'NHUR' && inm.Operate_And_Managed_By__c == 'Foxtel'){
            return false;
        }
        else{
            return true;
        }
    }
})