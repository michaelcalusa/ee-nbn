({
    handleMoreInfoEvent: function(component, event, helper){
        //debugger;
        //reset the calling component value to initialize the chld components
        component.set('v.callingMoreInfoComponent','');
    	var showMoreInfoTable = event.getParam("showMoreInfoTable");
        var moreInfoId = event.getParam("moreInfoId");
        var workRequestNumber = event.getParam("workRequestNumber");
        var callingMoreInfoComponent = event.getParam("callingMoreInfoComponent");
        var moreInfoCardHeader = event.getParam("moreInfoCardHeader");
        var deserializedObject = event.getParam("deserializedObject");
        var recordId = event.getParam("recordId");
        var Dayscount = event.getParam("Dayscount");
        
        if(recordId == component.get('v.recordId')) {
            component.set('v.deserializedObject', deserializedObject);
            component.set('v.moreInfoId', moreInfoId);
            component.set('v.tableHeader', moreInfoCardHeader);
            component.set('v.workRequestNumber', workRequestNumber);
            component.set('v.callingMoreInfoComponent',callingMoreInfoComponent);
            component.set('v.displayMoreInfo', showMoreInfoTable);
            component.set('v.dayCount', Dayscount);
        }       
	},
    
    handleClose: function(component, event, helper) {
        component.set('v.displayMoreInfo', false);
    } 
 
})