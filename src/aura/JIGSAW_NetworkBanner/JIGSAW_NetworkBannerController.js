({
    sectionOne : function(component, event, helper) {
        helper.helperToggleClass(component,event,'articleOne');
    },
    
    sectionTwo : function(component, event, helper) {
        helper.helperToggleClass(component,event,'articleTwo');
    },
    
    handleCurrentIncidentRecord: function(component, event, helper) {
        if(component.get('v.recordId') === event.getParam("recordId")){
            var incidentData = event.getParam("currentIncidentRecord");
            helper.getCurrentIncidentStatus(component,incidentData);
        }
    }
})