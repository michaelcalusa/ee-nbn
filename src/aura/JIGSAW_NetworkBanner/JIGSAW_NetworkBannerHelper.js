({
    helperToggleClass : function(component,event,secId) {
        var acc = component.find(secId);
        for(var cmp in acc) {
            $A.util.toggleClass(acc[cmp], 'slds-show');  
            $A.util.toggleClass(acc[cmp], 'slds-hide');  
        }
    },
    
    getCurrentIncidentStatus:function(component, incidentData,helper) {
        var dataIM = incidentData;
        var currentStatus = dataIM.Current_status;
        console.log('Incident Data Banner-->' + dataIM);
        console.log('Incident Status-->' + currentStatus);
        if (dataIM){
            if(currentStatus == 'Cancelled' || currentStatus == 'Rejected'){
                component.set("v.showNetworkBanner",false);
            }
            else{
                this.loadBanner(component, event,helper);
            }
        }
    },
    
    loadBanner: function(component, event, helper) {
        var responseWrapper;
        var action = component.get("c.fetchNetworkIncidents");
        action.setParams({
            incId: component.get('v.recordId'),
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
			if (state === 'SUCCESS') {
                if (response){
                    responseWrapper = response.getReturnValue();
                    console.log('Columns Response-->' + responseWrapper);
                    if (responseWrapper.validNI == true && responseWrapper.validCRQ == true){
                        component.set("v.showNetworkBanner",true);
                        component.set("v.showNIBanner",true);
                        component.set("v.showCRQBanner",true);
                    }
                    else if (responseWrapper.validNI == true){
                        component.set("v.showNetworkBanner",true);
                        component.set("v.showNIBanner",true);
                    }
                    else if (responseWrapper.validCRQ == true){
                        component.set("v.showNetworkBanner",true);  
                        component.set("v.showCRQBanner",true);
                    }
                    else if ((responseWrapper.errorCode && responseWrapper.errorCode != '200') || responseWrapper ==''){
                        component.set("v.showNetworkBanner",false);
                        component.set("v.errorOccured",true);
                    }
                    else if ((responseWrapper.errorCode && responseWrapper.errorCode == 'UNKNOWN')){
                        component.set("v.showNetworkBanner",false);
                        component.set("v.errorOccured",true);
                    }
                    else{
                        component.set("v.showNetworkBanner",false);
                    }
                }
            }
            else if (state === 'ERROR') {
                var errors = response.getError();
                component.set("v.errorOccured",true);
                console.log("Error message: " + errors[0].message)
            }
            this.fetchNetworkIncidentData(component,helper,responseWrapper);
        });
        $A.enqueueAction(action); 
    },
	
	fetchNetworkIncidentData: function(component,helper,responseWrapper) {
            console.log('Columns Response Helper Method 2-->' + responseWrapper);
			if (responseWrapper){
                if (responseWrapper.validNI == true || responseWrapper.validCRQ == true){
                    if (responseWrapper.validNI == true){
                        var booleanNI = responseWrapper.validNI;
                        var mycolumnsNI = responseWrapper.lstColumnsNI;
                        var myDataNI = responseWrapper.lstSobjectDataNI;
                        console.log('NI Data-->' +JSON.stringify(myDataNI));
                        var recordListNI = [];
                        component.set("v.networkIncidentLinking",myDataNI);
                        //set network incident details in incident management action component
                        var setNetworkIncidentDetails = $A.get("e.c:Jigsaw_SetNetworkIncidentAttribute");
                    	setNetworkIncidentDetails.setParams({"recordId" : component.get("v.recordId"), "NetworkIncidentsObject" : myDataNI});
                    	setNetworkIncidentDetails.fire();                
                        console.log('networkIncidentLinking-->' +JSON.stringify(component.get("v.networkIncidentLinking")));
                        for (var i = 0; i < myDataNI.length; i++) {
                            console.log('Data NI-->' + JSON.stringify(myDataNI[i]));
                            if (!$A.util.isUndefined(myDataNI[i])) {
                                recordListNI.push(myDataNI[i].incidentId);
                            	}
                            }
                        component.set ('v.networkIncidentNumber',recordListNI);
                        component.set('v.networkIncidentHeader', [{
                            type: 'text',
                            label: 'Network Incident ID',
                            initialWidth: 165,
                            fieldName: 'incidentId'
                        },
                        {
                            type: 'text',
                            label: 'Status',
                            initialWidth: 90,
                            fieldName: 'statusNI'
                        },
                        {
                            type: 'text',
                            label: 'Priority',
							initialWidth: 85,
                            fieldName: 'priority'
                        },
                         {
                            type: 'text',
                            label: 'Raised',
                            initialWidth: 160,
                            fieldName: 'raisedNI'
                        },
                         {
                            type: 'text',
                            label: 'Summary',
                            initialWidth: 200,
                            fieldName: 'summaryNI'
                        },
                          {
                            type: 'text',
                            label: 'SLA Due by',
                            initialWidth: 160,
                            fieldName: 'slaDueBy'
                        },
                         {
                            type: 'text',
                            label: 'Expected Resolution Date',
                            initialWidth: 160,
                            fieldName: 'estimatedResolutionDateTime'
                        },
                         {
                            type: 'text',
                            label: 'Actual Resolution Date',
                            initialWidth: 160,
                            fieldName: 'actualResolutionDateTime'
                        },
                        {
                            type: 'text',
                            label: 'Closed Date',
                            initialWidth: 160,
                            fieldName: 'closedDate'
                        }
                         ]);
                        component.set("v.networkIncidentData",myDataNI);
                    }
                    if (responseWrapper.validCRQ == true){
                        var booleanCRQ = responseWrapper.validCRQ;  
                        var mycolumnsCRQ = responseWrapper.lstColumnsCRQ;
                        var myDataCRQ = responseWrapper.lstSobjectDataCRQ;
                        var recordListCRQ = [];
                        console.log('CRQ Columns-->' + mycolumnsCRQ);
                        for (var i = 0; i < myDataCRQ.length; i++) {
                            console.log('Data CRQ-->' + JSON.stringify(myDataCRQ[i]));
                            if (!$A.util.isUndefined(myDataCRQ[i])) {
                                recordListCRQ.push(myDataCRQ[i].CRQNumber);
                            	}
                            }
                        console.log ('CRQ Data-->'+ JSON.stringify(recordListCRQ));
                        component.set('v.changeOutageItems',recordListCRQ);
                        console.log ('CRQ Data JSON-->'+ JSON.stringify(component.get("v.changeOutageItems")));
                        component.set('v.CRQHeader', [{
                            type: 'text',
                            label: 'Planned Outage ID',
                            initialWidth: 180,
                            fieldName: 'CRQNumber'
                        },
                        {
                            type: 'text',
                            label: 'Status',
                            initialWidth: 210,
                            fieldName: 'statusCRQ'
                        },
                        {
                            type: 'DateTime',
                            label: 'Raised',
                            initialWidth: 190,
                            fieldName: 'raisedCRQ'
                        },
                         {
                            type: 'text',
                            label: 'Summary',
                             initialWidth: 190,
                            fieldName: 'summaryCRQ'
                        },
                          {
                            type: 'text',
                            label: 'Scheduled Start',
                              initialWidth: 190,
                            fieldName: 'scheduledStart'
                        },
                          {
                            type: 'text',
                            label: 'Scheduled End',
                              initialWidth: 190,
                            fieldName: 'scheduledEnd'
                        },
                        {
                            type: 'text',
                            label: 'Actual Start Date',
                              initialWidth: 190,
                            fieldName: 'actualStartDate'
                        },
                          {
                            type: 'text',
                            label: 'Actual End Date',
                              initialWidth: 190,
                            fieldName: 'actualEndDate'
                        }
                         ]);
                        component.set("v.CRQData",myDataCRQ);
                    }
            }
		}
            else  {
                component.set("v.showNetworkBanner",false);
                component.set("v.errorOccured",true);
                //console.log("Error message: " + errors[0].message)
            }
    }  
})