({
    requestNT: function(component, event, helper) {
        
        component.set("v.transactionStatus", 'Retrieving data, please wait...');
        component.set("v.showSpinner", true);
        component.set("v.showNetworkBtn", false);
        component.set("v.errorDesc", '');
        component.set("v.errorCode", '');
        component.set("v.timeOutHappened", '');
        helper.requestNetworktrail(component, event, helper);
    },
    
    handleClose: function(component, event, helper){
        component.set("v.showNetworkBtn", true);
    },
    
    handleWaitingForUpdateEvent: function(component, event, helper) {
       if(event.getParam("recordId") === component.get("v.recordId")) {
            // adding one more "." in spinner to know on UI that application event raised.
            component.set("v.transactionStatus", component.get("v.transactionStatus") + '.');
            
            // Request to show respone only when timeout not happend and result already not shown and only if show network trail button is clicked.
            if(component.get("v.showNetworkBtn") == false){
                if($A.util.isUndefined(component.get("v.networkTrailJson")) || $A.util.isEmpty(component.get("v.networkTrailJson"))){
                   if(component.get("v.timeOutHappened") != 'YES'){
                       helper.requestPNIResponse(component, event, helper);
                    }
                }
            }
        }
    },
    
    sectionOne : function(component, event, helper) {
        helper.helperToggleClass(component,event,'articleOne');
    },
    
    handleRowAction : function(component, event, helper) {
        var url = event.getParam('row').TargetUrl;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
    }
})