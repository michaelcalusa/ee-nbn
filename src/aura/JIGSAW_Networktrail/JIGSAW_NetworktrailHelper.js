({
    setTimer:  function(component, timeoutInMilliSec) {
        window.setTimeout(
             $A.getCallback(function() {
                 var resultOccured = component.get("v.networkTrailJson");
                 if($A.util.isUndefined(resultOccured) || $A.util.isEmpty(resultOccured)){
                     component.set("v.errorDesc", "Request timed out.");
                     component.set("v.showSpinner", false);
                     component.set("v.timeOutHappened", 'YES');
                 }
             }), timeoutInMilliSec
         );
     },
     
     
     requestPNIResponse: function(component, event, helper) {
         
         var updatedRecord = component.get("v.incidentRecord");
         console.log("NetworkTrail Updated Record" + updatedRecord);
         //var action = component.get("c.requestPNIResponse");
         var action = component.get("c.requestForPNIResponse");
         action.setParams({
                 "cpiId": updatedRecord.Copper_Path_Id__c,
                 "incidentId": updatedRecord.Name,
                 "technologyType": updatedRecord.Prod_Cat__c
         });
         action.setCallback(this,function(a){
           //get the response state
             var state = a.getState();
           //check if result is successfull
             if(state == "SUCCESS"){
                 var result = a.getReturnValue();
                 if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                     component.set("v.NetworkTrailResult: ", result); 
                     this.processDisplay(component, event, helper,result,updatedRecord);
                 }
                 else{
                     // doing nothing as response is not received.
                 }
             }
             else if(state == "ERROR"){
                 component.set("v.errorDesc", 'Data Not available');
                 component.set("v.errorCode", 'UNKNOWN');
             }
         });
         //adds the server-side action to the queue        
         $A.enqueueAction(action);
     },
     
     requestNetworktrail: function(component, event, helper) {
        
         var updatedRecord = component.get("v.incidentRecord");
      // var action = component.get("c.requestNetworkTrail");
         var action = component.get("c.requestNetworkTrailResponse");
         action.setParams({
                 "cpiId": updatedRecord.Copper_Path_Id__c,
                 "technologyType": updatedRecord.Prod_Cat__c,
                 "incidentId": updatedRecord.Name,
                 "incRecId": updatedRecord.Id
         });
         console.log('@@@ CPI Id:' + updatedRecord.Copper_Path_Id__c);
         console.log('@@@ incidentId:' + updatedRecord.Name);
         action.setCallback(this,function(a){
          //get the response state
             var state = a.getState();
          //check if result is successfull
             if(state == "SUCCESS"){
                 var result = a.getReturnValue();
                 if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                     console.log('result');
                     component.set("v.NetworkTrailResult: ", result);
                   // Code Refactor process              
                     this.processDisplay(component, event, helper,result,updatedRecord);
                 }
                 else{
                     component.set("v.errorDesc", 'Data Not available');
                     component.set("v.errorCode", 'UNKNOWN');
                 }
             }
             else if(state == "ERROR"){
                 component.set("v.errorDesc", 'Data Not available');
                 component.set("v.errorCode", 'UNKNOWN');
             }
         });
         //adds the server-side action to the queue        
         $A.enqueueAction(action);
     },
     
     
     buildMDF: function(component, event, networkTrailObj) {
 
         component.set('v.mdfColumns', [{
                 type: 'text',
                 label: 'ID',
                 fieldName: 'mdfID',
                 initialWidth: 215,
                 sortable : false
             }, 
             {
                 type: 'text',
                 label: 'Type',
                 fieldName: 'subtype',
                 initialWidth: 170,
                 sortable : false
             },
             {
                 type: 'text',
                 label: 'C Pair',
                 fieldName: 'cpair',
                 initialWidth: 100,
                 sortable : false
             },
             {
                 type: 'text',
                 label: 'X Pair',
                 fieldName: 'xpair',
                 initialWidth: 100,
                 sortable : false
             },
             {
                 type: 'text',
                 label: 'Location ID',
                 fieldName: 'locationId',
                 initialWidth: 180,
                 sortable : false
             }
         ]);
 
         var mdfsList = [];
         
         if(networkTrailObj.fttbSite != undefined && networkTrailObj.fttbSite.mdfs != undefined){
             for (var key in networkTrailObj.fttbSite.mdfs) {
 
                 if(networkTrailObj.fttbSite.mdfs[key] != undefined){
                     var cPair = 'N/A';
                     var xPair = 'N/A';
 
                     if(networkTrailObj.fttbSite.mdfs[key].chassisSubtype == 'MDF_C'){
                         cPair = networkTrailObj.fttbSite.mdfs[key].portName;
                     }else if(networkTrailObj.fttbSite.mdfs[key].chassisSubtype == 'MDF_X'){
                         xPair = networkTrailObj.fttbSite.mdfs[key].portName;
                     }
                     /*else if(networkTrailObj.fttbSite.mdfs[key].chassisSubtype == 'CCF_C'){
                         cPair = networkTrailObj.fttbSite.mdfs[key].portName;
                     }else if(networkTrailObj.fttbSite.mdfs[key].chassisSubtype == 'CCF_X'){
                         xPair = networkTrailObj.fttbSite.mdfs[key].portName;
                     }*/
                 //Find index of specific object using findIndex method.    
                     var indexMDFId = mdfsList.findIndex((obj => obj.mdfID == networkTrailObj.fttbSite.mdfs[key].id));
                     console.log('indexMDFId' + indexMDFId);
                     console.log('mdfsList' + mdfsList);
                     if(indexMDFId > -1){
                         mdfsList[indexMDFId].cpair = cPair;
                         mdfsList[indexMDFId].xPair = xPair;
                     }
                     else{
                         // Get locationId value.Show N/A, if not exists in payload
                         var mdfLocId = (networkTrailObj.fttbSite.mdfs[key].locationId) ? networkTrailObj.fttbSite.mdfs[key].locationId : "N/A";
                         // if mdf type is distribution Frame, show subtype as a MDF type.
                         var mdfSubType =  (networkTrailObj.fttbSite.mdfs[key].type == 'Distribution Frame') ? networkTrailObj.fttbSite.mdfs[key].subtype : "N/A";
                         if(empty(mdfSubType)){
                             mdfSubType = 'N/A';
                         }
                         if(empty(cPair)){
                             cPair = 'N/A';
                         }
                         if(empty(xPair)){
                             xPair = 'N/A';
                         }
                         mdfsList.push({
                             mdfID: networkTrailObj.fttbSite.mdfs[key].id,
                             subtype: mdfSubType,
                             cpair: cPair,
                             xpair: xPair,
                             locationId: mdfLocId
                         }); 
                    }
                 }
             }
         }
         console.log('mdfList :' + JSON.stringify(mdfsList));
         component.set('v.mdfData', mdfsList);
     },
     
     buildPillarTable: function(component, event, networkTrailObj) {
         
         console.log('networkTrailObj is ---' + networkTrailObj);
         component.set('v.pillarColumns', [{
             type: 'text',
             label: 'Pillar ID',
             fieldName: 'PillarID',
             sortable:false,
             initialWidth: 160
             
         }, {
             type: 'text',
             label: 'Pillar Type',
             fieldName: 'PillarType',
             sortable:false,
             initialWidth: 120
         }, {
             type: 'text',
             label: 'X Pair',
             fieldName: 'XPair',
             sortable:false,
             initialWidth: 90
         }, {
             type: 'text',
             label: 'M Pair',
             fieldName: 'MPair',
             sortable:false,
             initialWidth: 90
         }, {
             type: 'text',
             label: 'O Pair',
             fieldName: 'OPair',
             sortable:false,
             initialWidth: 90
         }, {
             type: 'text',
             label: 'C Pair',
             fieldName: 'CPair',
             sortable:false,
             initialWidth: 80
         }, {
             type: 'button', 
             label: 'Latitude/Longitude',
             fieldName: 'LatitudeLongitude',
             sortable:false,
             initialWidth: 222,
             typeAttributes: { label : { fieldName : 'LatitudeLongitude'},
                               variant :"base",
                               value: { fieldName : 'TargetUrl' }
                             },
             cellAttributes:
            { 
             iconName: { fieldName: 'iconName' },
             iconLabel: { fieldName: 'iconLabel' },
             iconPosition: 'right',
             iconAlternativeText: 'Percentage Confidence' 
            }
         }]);
 
 
         var PillardsList = [];
         console.log('networkTrailObj.pillars--' +networkTrailObj.pillars);
         for (var key in networkTrailObj.pillars) {
             console.log('Key is -' + key);
             console.log('networkTrailObj.pillars is -' + networkTrailObj.pillars);
             var TerminationModule1Data = '';
             var TerminationModule2Data = '';
             var LatitudeLongitudeData = 'N/A';
             
             var xPair = 'N/A';
             var mPair = 'N/A';
             var oPair = 'N/A';
             var cPair = 'N/A';
             
             for (var pKey in networkTrailObj.pillars[key].terminationModules) {
                 if(networkTrailObj.pillars[key].terminationModules[pKey] != undefined){
                     if(networkTrailObj.pillars[key].terminationModules[pKey].subtype == 'X Pair' && networkTrailObj.pillars[key].terminationModules[pKey].twistedPairId)
                         xPair = networkTrailObj.pillars[key].terminationModules[pKey].twistedPairId;
                     if(networkTrailObj.pillars[key].terminationModules[pKey].subtype == 'M Pair' && networkTrailObj.pillars[key].terminationModules[pKey].twistedPairId)
                         mPair = networkTrailObj.pillars[key].terminationModules[pKey].twistedPairId;
                     if(networkTrailObj.pillars[key].terminationModules[pKey].subtype == 'O Pair' && networkTrailObj.pillars[key].terminationModules[pKey].twistedPairId)
                         oPair = networkTrailObj.pillars[key].terminationModules[pKey].twistedPairId;
                     if(networkTrailObj.pillars[key].terminationModules[pKey].subtype == 'C Pair' && networkTrailObj.pillars[key].terminationModules[pKey].twistedPairId)
                         cPair = networkTrailObj.pillars[key].terminationModules[pKey].twistedPairId;
                 } 
             }
             
             if (networkTrailObj.pillars[key].locationLatitude != undefined &&
                 networkTrailObj.pillars[key].locationLatitude != '' &&
                 networkTrailObj.pillars[key].locationLongitude != '' &&
                 networkTrailObj.pillars[key].locationLongitude != undefined) {
                 LatitudeLongitudeData = networkTrailObj.pillars[key].locationLatitude + ',' + networkTrailObj.pillars[key].locationLongitude;
             }
             
             var pillarType = (networkTrailObj.pillars[key].variant) ? networkTrailObj.pillars[key].variant: "N/A";
             PillardsList.push({
                 PillarID: networkTrailObj.pillars[key].id,
                 PillarType: pillarType,
                 XPair: xPair.toString(),
                 MPair: mPair.toString(),
                 OPair: oPair.toString(),
                 CPair: cPair.toString(),
                 TargetUrl : "http://maps.google.com/?q=" + LatitudeLongitudeData.toString(),
                 LatitudeLongitude: LatitudeLongitudeData.toString(),
                 iconName : LatitudeLongitudeData != 'N/A' ? "utility:new_window" : 'utility:new_window'
             });
         }
         console.log('PillardsList :' + JSON.stringify(PillardsList));
         component.set('v.pillarData', PillardsList);
     },
 
     buildCopperDistribCables: function(component, event, networkTrailObj) {
         component.set('v.CDCColumns', [{
             type: 'text',
             label: 'Copper Pair Range',
             fieldName: 'CDCName',
             sortable:'false',
             initialWidth: 520
         }, {
             type: 'text',
             label: 'Length',
             fieldName: 'length',
             sortable:'false',
             initialWidth: 100
         }, {
             type: 'text',
             label: 'Length Type',
             fieldName: 'lenghtType',
             sortable:'false',
             initialWidth: 150
         }]);
 
         var CDCList = [];
         for (var key in networkTrailObj.copperDistributionCables) {
             var CDCNamedata = 'N/A';
 
             if (networkTrailObj.copperDistributionCables[key].name != undefined &&
                 networkTrailObj.copperDistributionCables[key].name != '') {
                 CDCNamedata = networkTrailObj.copperDistributionCables[key].name;
             }
 
             var lengthdata = 'N/A';
             if (networkTrailObj.copperDistributionCables[key].length != undefined &&
                 networkTrailObj.copperDistributionCables[key].length != '') {
                 lengthdata = networkTrailObj.copperDistributionCables[key].length + ' m';
             }
 
             var lenghtTypeData = 'N/A';
             if (networkTrailObj.copperDistributionCables[key].lenghtType != undefined &&
                 networkTrailObj.copperDistributionCables[key].lenghtType != '') {
                 lenghtTypeData = networkTrailObj.copperDistributionCables[key].lenghtType;
             }
 
             CDCList.push({
                 CDCName: CDCNamedata,
                 length: lengthdata,
                 lenghtType: lenghtTypeData
             });
         }
         console.log('CDCList :' + JSON.stringify(CDCList));
         component.set('v.CDCData', CDCList);
     },
     
     
     
   /*  buildLeadinCable: function(component, event, networkTrailObj) {
         component.set('v.licColumns', [{
             type: 'text',
             label: 'Name',
             fieldName: 'licName'
         }, {
             type: 'text',
             label: 'Length',
             fieldName: 'length'
         }, {
             type: 'text',
             label: 'Length Type',
             fieldName: 'lenghtType'
         }, {
             type: 'text',
             label: 'Installation Type',
             fieldName: 'installationType'
         }]);
         var licList = [];
         if (networkTrailObj.copperLeadinCable != undefined) {
             var licNamedata = '';
             if (networkTrailObj.copperLeadinCable.name != undefined &&
                 networkTrailObj.copperLeadinCable.name != '') {
                 licNamedata = networkTrailObj.copperLeadinCable.name;
             }
 
             var lengthdata = '';
             if (networkTrailObj.copperLeadinCable.length != undefined &&
                 networkTrailObj.copperLeadinCable.length != '') {
                 lengthdata = networkTrailObj.copperLeadinCable.length;
             }
 
             var lenghtTypeData = '';
             if (networkTrailObj.copperLeadinCable.lenghtType != undefined &&
                 networkTrailObj.copperLeadinCable.lenghtType != '') {
                 lenghtTypeData = networkTrailObj.copperLeadinCable.lenghtType;
             }
             var installationTypeData = '';
             if (networkTrailObj.copperLeadinCable.supportStructureType != undefined &&
                 networkTrailObj.copperLeadinCable.supportStructureType != '') {
                 if (networkTrailObj.copperLeadinCable.supportStructureType == 'Pole') {
                     installationTypeData = 'Aerial';
                 }
                 if (networkTrailObj.copperLeadinCable.supportStructureType == 'Pit') {
                     installationTypeData = 'Underground';
                 }
             }
 
             licList.push({
                 licName: licNamedata,
                 length: lengthdata,
                 lenghtType: lenghtTypeData,
                 installationType: installationTypeData
             });
         }
         component.set('v.licData', licList);
     }, */
     
     processDisplay: function(component, event, helper, result, updatedRecord) {
         
         console.log(" parse Result is -" + JSON.parse(result));
         var resultResponse = JSON.parse(result);
         var resultNT;
         if(resultResponse.payload)
             resultNT = JSON.parse(resultResponse.payload);
         if(!$A.util.isEmpty(resultResponse.transactionId) && !$A.util.isUndefined(resultResponse.transactionId)){
             helper.setTimer(component, resultResponse.timeoutInMilliSec);
         }
         
         if(!resultResponse.payload){
             component.set("v.errorDesc", resultResponse.errorDesc);
             component.set("v.errorCode", resultResponse.errorCode);
         }
         else{
             if(updatedRecord.Prod_Cat__c == 'FTTB' && !resultNT.fttbSite){
                 component.set("v.errorDesc", "Invalid JSON, FTTB Not found.");
                 component.set("v.errorCode", "UNKNOWN");
             }
             
             if(resultNT && (resultNT.status == 'Failure' || resultNT.exception != null)){
                 if(resultNT.exception.message){   
                     component.set("v.errorDesc", resultNT.exception.message);
                 }
                 else{
                     component.set("v.errorDesc", "Failed status found with no details");
                 }
                 if(resultNT.exception.code){   
                     component.set("v.errorCode", resultNT.exception.code);
                 }
                 else{
                     component.set("v.errorCode", "UNKNOWN");
                 }
             }
             else{
                 // show result.
                 component.set("v.networkTrailJson", resultNT);
                 if(updatedRecord.Prod_Cat__c == 'NCAS-FTTN'){
                     helper.buildPillarTable(component, event, resultNT);
                     helper.calculateCableLength(component, event, resultNT);
                     helper.buildCopperDistribCables(component, event, resultNT);
                   //  helper.buildLeadinCable(component, event, resultNT);
                 }
                 else if(updatedRecord.Prod_Cat__c == 'NCAS-FTTB'){
                     console.log("inside mdf log");
                     // build MDF Table
                     helper.buildMDF(component, event, resultNT);
                     
                 }
                 if (resultNT.boundaryPoints && resultNT.boundaryPoints.networkBoundaryPointTimeStamp){
                     var bpddate = $A.localizationService.formatDate(resultNT.boundaryPoints.networkBoundaryPointTimeStamp, "DD/MM/YYYY");
                     component.set("v.bpdDate", bpddate.toString());
                 }
             }
         }
      // END
     },
     
     calculateCableLength : function(component,event,networkTrailObj) {
         
         let pillarToNodeLen = 0;
         let pillarToJointLen = 0;
         let leadInLen;
         let totalLength = 0;
         if (networkTrailObj.boundaryPoints && networkTrailObj.boundaryPoints.pillarToNodeLength){
             pillarToNodeLen = parseFloat(networkTrailObj.boundaryPoints.pillarToNodeLength).toFixed(1);
             totalLength = totalLength + parseFloat(pillarToNodeLen);
             component.set("v.pillarToNodelength", pillarToNodeLen + " m");
         } 
         
         if (networkTrailObj.boundaryPoints && networkTrailObj.boundaryPoints.locationToPillarLength && networkTrailObj.boundaryPoints.locationToPillarLength !=''){
             pillarToJointLen = parseFloat(networkTrailObj.boundaryPoints.locationToPillarLength).toFixed(1);
             totalLength = totalLength + parseFloat(pillarToJointLen);
             component.set("v.pillarToJointlength", pillarToJointLen + " m");
         } 
         
         if (networkTrailObj.copperLeadinCable && networkTrailObj.copperLeadinCable.length && networkTrailObj.copperLeadinCable !=''){
             leadInLen = parseFloat(networkTrailObj.copperLeadinCable.length).toFixed(1);
             component.set("v.leadInlength", leadInLen + " m");
             component.set("v.leadInlengthInstallationType", networkTrailObj.copperLeadinCable.installationType);
         } 
         
         
         if (networkTrailObj.copperLeadinCable && networkTrailObj.copperLeadinCable.lengthType && networkTrailObj.copperLeadinCable.lengthType !=''){
             if(networkTrailObj.copperLeadinCable.lengthType =='AsBuilt')
                 component.set("v.leadInlengthType", "As built");
             if(networkTrailObj.copperLeadinCable.lengthType =='Designed')
                 component.set("v.leadInlengthType", "As designed");
         }
         component.set("v.totalCablelength", totalLength.toFixed(1) + " m");
         
     },
     
     helperToggleClass : function(component,event,secId) {
         var acc = component.find(secId);
         for(var cmp in acc) {
             $A.util.toggleClass(acc[cmp], 'slds-show');  
             $A.util.toggleClass(acc[cmp], 'slds-hide');  
         }
     }
 })