({
    setWorkOrderData : function(component, event, helper) {
        if(event.getParam("recordId") == component.get("v.attrCurrentIncidentRecord.inm.Id")){
            helper.getWorkOrderHistoryValues(component, event, helper);
        }
    },
    onsiteTech:function(component, event, helper) {
        helper.submitOnsiteTechnician(component, event, helper);
    },
    openRequestInfo : function(component, event, helper) {
        helper.getWorkOrderDetailsValues(component, event, helper);
    },
    handleClose:function(component, event, helper) {
        component.set("v.onsiteTechnicianComposerVisible",false);
        component.set("v.confirmationModalVisible",true);
    },
    handleCancelConfirmationModal:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
        component.set("v.onsiteTechnicianComposerVisible",true);
    },
    handleCloseConfirmed:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
        component.set("v.onsiteTechnicianComposerVisible",false);
        component.set("v.onsiteTechnicianComponentVisible",false);
        component.set("v.disableActionLink",false);
    },
    handleMinimize : function(component, event, helper) {
        var onsiteTechSectionDiv = component.find("onsiteTechnicianSection");
        if($A.util.hasClass(onsiteTechSectionDiv, 'slds-is-open'))
        {            
            $A.util.addClass(onsiteTechSectionDiv, 'slds-is-closed');
            $A.util.removeClass(onsiteTechSectionDiv, 'slds-is-open');
        }
        else
        {            
            $A.util.removeClass(onsiteTechSectionDiv, 'slds-is-closed');
            $A.util.addClass(onsiteTechSectionDiv, 'slds-is-open');
        }   
    },
    SubmitonTech : function(component, event, helper) {  
        component.set("v.onsiteTechnicianComponentVisible",false);
        component.set("v.disableActionLink",true);
        var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
            fireSpinnerRefresh.setParams({"userAction" :"onsiteTechnician", "stopSpinner" : false, "spinnersToStart": "IncidentStatus", "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
            fireSpinnerRefresh.fire();
        	var resultsToast = $A.get("e.force:showToast");
       		resultsToast.setParams({
                "title": "Submitting on-site technician (step 1 of 2).",
                "message":"This should only take 10-15 secs."
            });
        	resultsToast.fire();
        helper.CreateWorkOrder(component, event, helper);
    }
    
})