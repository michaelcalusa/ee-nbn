({
    getWorkOrderHistoryValues: function(component, event, helper) {
        let wohistorywrap = event.getParam('wohistorywrapper'); 
        if(wohistorywrap && wohistorywrap.onsiteTechnician && component.get('v.showOpActionLink')){
            var result = wohistorywrap.onsiteTechnician;
            if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result) && result.nbnworkhistoryview){
                var size = result.nbnworkhistoryview.length;
                var v = 0;
                if(result.nbnworkhistoryview.length > 1){
                    for (var i = 0; i < result.nbnworkhistoryview.length; i++){
                        if(result.nbnworkhistoryview[i].status === $A.get("$Label.c.Jigsaw_OnsiteActiveWO")){
                            v = i;
                        }
                    }
                }
                
                component.set('v.activeWO',result.nbnworkhistoryview[v].wonum );
                component.set("v.disableAction",false);
                component.set('v.EngagementWOType',result.nbnworkhistoryview[v].nbnrequirement );
                component.set('v.accessSeeker', result.nbnworkhistoryview[v].nbnworkforce);
                component.set('v.ParentWO',result.nbnworkhistoryview[v].parent );
                component.set('v.WorkRequest',result.ticketid );
                component.set('v.TechnologyType',result.nbnaccesstech);
                component.set('v.WOAppointmentID',result.nbnworkhistoryview[v].nbnappointmentid);
            }else{                    
                var currentIncidentWrapper = component.get("v.attrCurrentIncidentRecord");        
                var currentStatus = currentIncidentWrapper.Current_status;
                var isLoggedInUserIsAssignee = currentIncidentWrapper.isLoggedInUserEqualsAssignee;
                if (isLoggedInUserIsAssignee && currentStatus == "Awaiting Technician") {
                    component.set("v.disableOnsiteActionLink", true);
                    component.set("v.disableAction",true);
                    var newbutton = component.find('newontechbutton');
                    $A.util.addClass(newbutton, 'HideLinkToClick');
                }   
            }            
        }
    },
    getWorkOrderDetailsValues: function(component, event, helper) {
        var self = this;
        var action = component.get("c.getDetailWorkOrderValues");
        var currentIncidentManagementRecordWrapper = component.get("v.attrCurrentIncidentRecord");
       // alert(component.get('v.activeWO'));
        action.setParams({
            woNumber : component.get('v.activeWO')      
        });
        action.setCallback(this, function(data) {
            var response = data.getState();            
          //     alert(response);
            if(response === 'SUCCESS')
            {	
                var WODetailWrapper = data.getReturnValue();
                console.log('Gaurav Inside WODetail' + WODetailWrapper.nbnwospecversion);
                if(!$A.util.isEmpty(WODetailWrapper) && !$A.util.isUndefined(WODetailWrapper)){
                    var engType = component.get('v.EngagementWOType');
                    if((WODetailWrapper.nbnwospecversion == '4.3.0' || WODetailWrapper.nbnwospecversion == '2.3.0') && (component.get('v.TechnologyType') =='FTTB' || component.get('v.TechnologyType') == 'FTTN') && (engType.toUpperCase() =='COMMITMENT' || engType.toUpperCase() == 'APPOINTMENT')){
                        component.set('v.specVersion',WODetailWrapper.nbnwospecversion);
                        component.set('v.WOSpecification',WODetailWrapper.nbnwospecid);
                        //component.set('v.TechnologyType',techType.substring(0, 4));
                        component.set("v.onsiteTechnicianComponentVisible",true);  
                        component.set("v.onsiteTechnicianComposerVisible",true);  
                        component.set("v.disableActionLink", true);                     
                    }else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : $A.get("$Label.c.OnsiteTechnicianErrorHeader"),
                            message: $A.get("$Label.c.OnsiteTechnicianNOSupportedWO"),
                            type: 'Error'
                        });
                        toastEvent.fire();
                    }                  
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.OnsiteTechnicianErrorHeader"),
                        message: 'Something went wrong, Please contact system administrator',
                        type: 'Error'
                    });
                    toastEvent.fire();
                }
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.OnsiteTechnicianErrorHeader"),
                    message: 'Something went wrong, Please contact system administrator',
                    type: 'Error'
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);  
    },    
    CreateWorkOrder: function(component, event, helper){         
        var action = component.get("c.SaveWorkOrderRecordOnsiteTechnician");        
        var techType = component.get("v.TechnologyType");
        var engageType = component.get("v.EngagementWOType");
        var specVersion = component.get("v.specVersion");
        var currentIncidentWrapper = component.get("v.attrCurrentIncidentRecord"); 
        var woNum = component.get('v.activeWO');
        var addInfo = component.get('v.internalResolutionNote');
        var parent = component.get('v.ParentWO');
        var WRequest = component.get('v.WorkRequest');
        var WOSpecID = component.get('v.WOSpecification');
        var technicianID =  component.get('v.technicianID');
        var WOApptID = component.get('v.WOAppointmentID');
       var seekerID = component.get('v.accessSeeker');
        
        //console.log('techtype - ' + techType + '  EngageType -' + engageType + '  version -' + specVersion);
        //console.log('currentIncidentWrapper - ' + currentIncidentWrapper + '  woNum -' + woNum + ' pcrCode -' + pcrCode);
        //console.log('isfault - ' + isfault + '  faultRootCause -' + faultRootCause + '  addInfo -' + addInfo);
        //console.log('woActionType - ' + woActionType + '  servicePoint -' + servicePoint );  
        
        var woTechType;
        if(techType.indexOf('FTTN') !== -1)
            woTechType = 'FTTN Service Assurance';
        else
            woTechType = 'End User Assurance FTTB';
        //console.log('techtype - ' + woTechType + '  EngageType -' + engageType + '  version -' + specVersion);
        //console.log('currentIncidentWrapper - ' + currentIncidentWrapper + '  woNum -' + woNum + ' pcrCode -' + pcrCode);
        ///console.log('isfault - ' + isfault + '  faultRootCause -' + faultRootCause + '  addInfo -' + addInfo);
        //console.log('woActionType - ' + woActionType + '  servicePoint -' + servicePoint );
        
         var woValues = '{"woNum": "' + woNum + '","woEngageType" : "' + engageType + '","woSpecVersion": "' + specVersion + 
            '","woTechType" : "' + techType + '","addInfo" : "' + addInfo.replace('\n','').replace('\r','') + '","incidentId" : "' + 
            currentIncidentWrapper.inm.Name + '","strProdCat" : "' + currentIncidentWrapper.inm.Prod_Cat__c  + 
            '","strAVCId" : "' + currentIncidentWrapper.inm.AVC_Id__c + '","strPRI" : "' + currentIncidentWrapper.inm.PRI_ID__c +
            '","strAccessSeekerId" : "' + seekerID + '","parent" : "' + parent + '","WRequest" : "' + WRequest + 
            '","WOSpecID" : "' + WOSpecID + '","locationId" :"' + currentIncidentWrapper.inm.locId__c + '","WorkOrderApptID" : "' + 
            WOApptID +  '","reasonCode" : "TECH-ON-SITE"' + ',"technicianID" : "' + technicianID  +  '"}' ;
        
        	console.log('WOvalues after bind -' + woValues );
        action.setParams({
            "strWOWrapper" : woValues
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var returnResponse = response.getReturnValue();
            var toastEvent = $A.get("e.force:showToast");
            if (state === "SUCCESS") {   
               // alert('return response is --' + JSON.stringify(returnResponse));
                console.log('return response is --' + JSON.stringify(returnResponse));
               // alert('return response UpdatedIncidentRecordValue is --' + JSON.stringify(returnResponse.UpdatedIncidentRecordValue));
                console.log('return response UpdatedIncidentRecordValue is --' + JSON.stringify(returnResponse.UpdatedIncidentRecordValue));
               // alert('misising -' + returnResponse.MissingData.length);
                if(returnResponse.MissingData.length > 0){                    
                    console.log('false field missed 1');
                    toastEvent.setParams({
                        title : $A.get("$Label.c.OnsiteTechnicianErrorHeader"),
                        message: 'Error in posting request due to missed fields :'+returnResponse.MissingData,
                        type: 'Error'
                    });
                    toastEvent.fire();
                    component.set("v.disableActionLink",false);
                    var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
                    fireSpinnerRefresh.setParams({"userAction" :"onsiteTechnician", "stopSpinner" : true, "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
                    fireSpinnerRefresh.fire();
                }
                else if(returnResponse.MissingData.length < 1 && returnResponse.UpdatedIncidentRecordValue && returnResponse.errorCode == ''){
                    console.log('true');
                //    alert('success spinner active');
                    var varattrCurrentIncientRecord = returnResponse.UpdatedIncidentRecordValue;
                   /* var spinnersToFire = '';
                    if(varattrCurrentIncientRecord.Awaiting_Current_Incident_Status__c)
                        spinnersToFire = 'IncidentStatus';
                    var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
                    spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : varattrCurrentIncientRecord, "spinnersToStart" : spinnersToFire, "stopSpinner" : false});
                    spinnerHandlerEvent.fire();
                    component.set("v.attrCurrentIncidentRecord",varattrCurrentIncientRecord);*/
                    component.set("v.onsiteTechnicianComponentVisible",false);
                   
                }
                else if(returnResponse.MissingData.length < 1 && returnResponse.UpdatedIncidentRecordValue == undefined && returnResponse.errorCode != '202'){
                 //   alert('Error Posting request 2');
                    toastEvent.setParams({
                        title : $A.get("$Label.c.OnsiteTechnicianErrorHeader"),
                        message: $A.get("$Label.c.onsiteTechnicianErrorRetry"),
                        type: 'Error'
                    });
                    toastEvent.fire();  
                    component.set("v.disableActionLink",false);
                    var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
                    fireSpinnerRefresh.setParams({"userAction" :"onsiteTechnician", "stopSpinner" : true, "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
                    fireSpinnerRefresh.fire();
                }
                else{
                    //    alert('Error something wrong 3');
                        toastEvent.setParams({
                            title : $A.get("$Label.c.OnsiteTechnicianErrorHeader"),
                            message: 'Something went wrong either retry else contact system administrator.',
                            type: 'Error'
                        });
                        toastEvent.fire();  
                        component.set("v.disableActionLink",false);
                        var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
                        fireSpinnerRefresh.setParams({"userAction" :"onsiteTechnician", "stopSpinner" : true, "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
                        fireSpinnerRefresh.fire();
                    }
                
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.OnsiteTechnicianErrorHeader"),
                        message: 'Error in submitting onsite technician, Please contact system administrator',
                        type: 'Error'
                    });
                    toastEvent.fire();
                    component.set("v.disableActionLink",false);
                    var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
                    fireSpinnerRefresh.setParams({"userAction" :"onsiteTechnician", "stopSpinner" : true, "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
                    fireSpinnerRefresh.fire();
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.OnsiteTechnicianErrorHeader"),
                        message: 'Something went wrong, Please contact system administrator',
                        type: 'Error'
                    });
                    toastEvent.fire();
                    component.set("v.disableActionLink",false);
                    var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
                    fireSpinnerRefresh.setParams({"userAction" :"onsiteTechnician", "stopSpinner" : true, "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
                    fireSpinnerRefresh.fire();
                }
            }
        });           
        $A.enqueueAction(action);    
        
    }
    
})