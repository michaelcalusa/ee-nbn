({
    
    handleOpCat : function(component, event, helper) {
        helper.submitOpCat(component, event, helper);
        component.set("v.showDocComposer", false);
    },
    
    
    doInit : function(component, event, helper) {
         
        if(component.get('v.incidentRecord.Id') === event.getParam("recordId")){
            var incidentData = event.getParam("currentIncidentRecord");
            if(incidentData && incidentData.inm){
                component.set("v.attrCurrentIncidentRecord", incidentData);
                helper.loadMetaData(component);
            }
        }
        
    },
    
    onChangeT1: function(component, event, helper) {     
        var newT1 = event.getSource().get("v.value");
        var newT2 = component.get("v.selectedValueT2");
        //alert('newT1: ' + newT1 + 'newT2: ' + newT2);
        var oldT1 = component.get("v.attrCurrentIncidentRecord").inm.Op_Cat__c;
        var oldT2 = component.get("v.attrCurrentIncidentRecord").inm.Fault_Type__c;
        
        if((newT1 == oldT1 && newT2 == oldT2) || newT1 == '- Please Select -' || newT2 == '- Please Select -'){
            component.set("v.isSubmitDisabled", true);
        }else{
            component.set("v.isSubmitDisabled", false);
        }
        
        var map_T1_T2 = component.get("v.map_T1_T2");
        var lstT2 = map_T1_T2[newT1];
        component.set("v.optionsT2", lstT2);
    },
    
    
    /*onChangeT2: function(component, event, helper) {     
        var newT1 = component.get("v.selectedValueT1");
        var newT2 = event.getSource().get("v.value");
        var newT3;
        
        //alert('newT1: ' + newT1 + 'newT2: ' + newT2);
        var oldT1 = component.get("v.attrCurrentIncidentRecord").inm.Op_Cat__c;
        var oldT2 = component.get("v.attrCurrentIncidentRecord").inm.Fault_Type__c;
        var oldT3;
        
        if(helper.checkIfIncidentIsHFC){
            newT3 = component.get("v.selectedValueT3");
            oldT3 = component.get("v.attrCurrentIncidentRecord").inm.Fault_Details__c;
        }
        if((newT1 == oldT1 && newT2 == oldT2) || newT1 == '- Please Select -' || newT2 == '- Please Select -'){
            component.set("v.isSubmitDisabled", true);
        }else{
            component.set("v.isSubmitDisabled", false);
            // Load Tier 3 List if the Technology is NHAS and NHUR
            if(helper.checkIfIncidentIsHFC){
            	helper.loadMetaDataT3(component);
            }
        }
    },*/
    /*----CUSTSA - 31682-------*/
    onChangeT2: function(component, event, helper) { 
        if(helper.checkIfIncidentIsHFC){
            // Load Tier 3
            helper.loadMetaDataT3(component, event, helper);
        }else{
            // Check the validation for submit button
            helper.validateDropdowns(component, event, helper);
        }
    },
    
    onChangeT3: function(component, event, helper){
        helper.validateDropdowns(component, event, helper);
    },
    
    /*----CUSTSA - 31682 Ends-------*/
    
	handleCancelConfirmationModal:function(component, event, helper) {
        component.set("v.closeDialogVisible",false);
	},
    
    handleCancelAndClose:function(component, event, helper) {
        component.set("v.closeDialogVisible", true);
	},
    
    handleCloseConfirmed:function(component, event, helper) {
        component.set("v.closeDialogVisible", false);
        component.set("v.showDocComposer", false);
        component.set("v.disableActionLink", false);
        //CUSTSA-31682
        component.set("v.showT3", false);
        component.set("v.selectedValueT2",'- Please Select -');
    },

 	openDocComp : function(component, event, helper) {
        component.set("v.disableActionLink", true);
		component.set('v.showDocComposer', !component.get("v.showDocComposer"));
	},
    
    handleMinimize : function(component, event, helper) {
    	var section = component.find("dcSection");
        
        if($A.util.hasClass(section, 'slds-is-open')){ 
            $A.util.addClass(section, 'slds-is-closed');
       		$A.util.removeClass(section, 'slds-is-open');
        }
        else{
			$A.util.removeClass(section, 'slds-is-closed');
       		$A.util.addClass(section, 'slds-is-open');
        }
    },

    getVisibility: function(component, event, helper) {
        var attrMapByModule = component.get("v.mapComponentVisibility");
        if(!attrMapByModule['JIGSAW_OPCAT']){
           component.set("v.hideOpsCat", true);
        }
    }
})