({
    submitOpCat: function(component, event, helper) {
        var action = component.get("c.submitOpCat");
        
        var currentIncident = component.get("v.attrCurrentIncidentRecord");
        action.setParams({
            "incidentID": currentIncident.inm.Id,
            "Tier1": component.get("v.selectedValueT1"),
            "Tier2": component.get("v.selectedValueT2"),
            "oldTier1": currentIncident.inm.Op_Cat__c,
            "oldTier2": currentIncident.inm.Op_Cat_Tier_2__c,
            "Tier3": helper.checkIfIncidentIsHFC ?component.get("v.selectedValueT3"):'',
            "oldTier3": helper.checkIfIncidentIsHFC ?currentIncident.inm.Op_Cat_Tier_3__c:'',
            "techType" : currentIncident.inm.Prod_Cat__c
        });
        
        
        action.setCallback(this, function(response) {
            var toastEvent = $A.get("e.force:showToast");
            if (response.getState() === "SUCCESS" && response.getReturnValue()) {
                toastEvent.setParams({
                    "title": $A.get("$Label.c.Jigsaw_Update_Op_Cat_Submit_Title"),
                    "message": $A.get("$Label.c.Jigsaw_Update_Op_Cat_Submit_Message")
                });
                toastEvent.fire();
                
                var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
				addNoteRefreshEvent.fire();
                
                // Start Spinner
                var varattrCurrentIncientRecord = response.getReturnValue();
                var spinnersToFire = '';
                if(varattrCurrentIncientRecord.Awaiting_Current_Incident_Status__c)
                    spinnersToFire = 'IncidentStatus';
                
                if(varattrCurrentIncientRecord.Awaiting_Current_SLA_Status__c)
                    spinnersToFire = spinnersToFire + 'SLA';
                
                var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
                
                spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : varattrCurrentIncientRecord, "spinnersToStart" : spinnersToFire, "stopSpinner" : false});
                spinnerHandlerEvent.fire();
                //component.set("v.attrCurrentIncidentRecord",varattrCurrentIncientRecord);
                
                component.set("v.disableActionLink",true);
            
            
            }
            else {
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Some internal error occurred"
                });
                toastEvent.fire();
            }
        });
        
        $A.enqueueAction(action);
    },
    
    
    loadMetaData: function(component) {
        var plzSelect = '- Please Select -';
        var action = component.get("c.getDependentMap");
        var currentIncident = component.get("v.attrCurrentIncidentRecord");
        action.setParams({
            'techType' : currentIncident.inm.Prod_Cat__c
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var res = response.getReturnValue();
                component.set("v.map_T1_T2", res);
                
                // SET Tier 1
                var T1 = [];
                //T1.push(plzSelect);
                for (var singlekey in res) {
                    T1.push(singlekey);
                }
                
                var selectedT1;
                if(currentIncident.inm.Op_Cat__c){
                    selectedT1 = currentIncident.inm.Op_Cat__c;
                }
                else{
                    T1.splice(0, 0, plzSelect);
                    selectedT1 = plzSelect;
                }
                component.set("v.optionsT1", T1);
                component.set("v.selectedValueT1", selectedT1);
                
                // SET Tier 2
                var T2 = [];
                T2.push(plzSelect);
                for (var k in res[selectedT1]) {
                    T2.push(res[selectedT1][k]);
                }
                component.set("v.optionsT2", T2);
                
                var selectedT2;
                if(currentIncident.inm.Fault_Type__c){
                    selectedT2 = currentIncident.inm.Fault_Type__c;
                }
                else{
                    selectedT2 = plzSelect;
                }
                component.set("v.selectedValueT2", selectedT2);
            }else{
                console.log('Something went wrong..');
            }
        });
        $A.enqueueAction(action);
    },
    
    
    loadMetaDataT3: function(component, event, helper) {
        var action = component.get("c.getDependentMapT3");
        var plzSelect = '- Please Select -';
        action.setBackground();
        var currentIncident = component.get("v.attrCurrentIncidentRecord");
        var selectedT2 = component.get("v.selectedValueT2");
        action.setParams({
            'techType' : currentIncident.inm.Prod_Cat__c,
            'T1' : component.get("v.selectedValueT1"),
            'T2' : component.get("v.selectedValueT2")
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var res = response.getReturnValue();
                //console.log('Tier 3 ',response.getReturnValue());
                component.set("v.map_T2_T3", res);
                
                //console.log('res '+JSON.stringify(res));
                
                // SET Tier 3
                var T3 = [];
                T3.push(plzSelect);
                //console.log('selectedT2 '+selectedT2);
                for (var k in res[selectedT2]) {
                    T3.push(res[selectedT2][k]);
                }
                //console.log('res '+JSON.stringify(res));
                //console.log('T3 '+JSON.stringify(T3));
                
                // checking if the selected T2 have tier 3 option otherwise set null
                if(T3.length > 1){
               	 	component.set("v.optionsT3", T3);
                	var selectedT3;
                    if(currentIncident.inm.Fault_Details__c){
                        selectedT3 = currentIncident.inm.Fault_Details__c;
                    }else{
                        selectedT3 = plzSelect;
                    }
                    component.set("v.selectedValueT3", selectedT3);
                	component.set("v.showT3", true);
                }else{
                    component.set("v.showT3", false);
                    component.set("v.selectedValueT3", '');
                }
                helper.validateDropdowns(component, event, helper);
            }else{
                console.log('Something went wrong..');
            }
        });
        $A.enqueueAction(action);
    },
    
    checkIfIncidentIsHFC: function(component){
        var currentIncident = component.get("v.attrCurrentIncidentRecord");
        if(currentIncident.inm.Prod_Cat__c == 'NHAS' || currentIncident.inm.Prod_Cat__c == 'NHUR'){
            return true;
        }else{
            return false;
        }
    },
    
    validateDropdowns: function(component, event, helper){
        var newT1 = component.get("v.selectedValueT1");
        var newT2 = component.get("v.selectedValueT2");
        var newT3;
        
        //alert('newT1: ' + newT1 + 'newT2: ' + newT2);
        var oldT1 = component.get("v.attrCurrentIncidentRecord").inm.Op_Cat__c;
        var oldT2 = component.get("v.attrCurrentIncidentRecord").inm.Fault_Type__c;
        var oldT3;
        //console.log('check '+JSON.stringify(component.get("v.attrCurrentIncidentRecord").inm));
        
        if(helper.checkIfIncidentIsHFC){
            newT3 = component.get("v.selectedValueT3");
            oldT3 = component.get("v.attrCurrentIncidentRecord").inm.Op_Cat_Tier_3__c;
        }
        
        if((newT1 == oldT1 && newT2 == oldT2) || newT1 == '- Please Select -' || newT2 == '- Please Select -' 
           || (helper.checkIfIncidentIsHFC && newT3 == '- Please Select -')){
            component.set("v.isSubmitDisabled", true);
        }else{
            component.set("v.isSubmitDisabled", false);
        }
    }
    
})