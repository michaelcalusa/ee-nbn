({
    performAction: function(component, event, helper){
        //show the docked composer   
        component.set("v.docComposerComponentVisible", true);
        component.set("v.docComposerVisible", true);
    },
    doInit : function(component, event, helper){
        if(component.get('v.showOpActionLink')){
            helper.getIncident(component, event, helper);
        	helper.ServiceRestorationSLAMap(component, event, helper);
        }
    },
    submitResponse : function(component, event, helper){
        var currentIncidentWrapper = component.get("v.attrCurrentIncidentRecord");
        var strApptIdWithresponseType = component.get("v.AppointmentId");
        if(component.get("v.responseType") == 'NewAppointment'){
            var apptIdVal = component.find('aptId').get('v.value');
            strApptIdWithresponseType = apptIdVal ? apptIdVal + '-' + component.get("v.responseType") : component.get("v.responseType");
        }
        helper.performOperatorAction(component, 
                                     { "strRecordId": currentIncidentWrapper.inm.Id,
                                       "strAppointmentId": strApptIdWithresponseType, //component.get("v.AppointmentId"),
                                       "strMessageSeeker": component.get("v.rspComment")
                                     },
                                     helper.handleResponseReceived
                                    );
        helper.resetAttributes(component, event, helper);
    },
    handleCloseConfirmed : function(component, event, helper) {
        helper.resetAttributes(component, event, helper);
    },
    validate : function(component, event, helper){
        var rspComment = component.get("v.rspComment");
        var charRemaining = component.get("v.commentCharRemaing");
        if(rspComment != undefined && rspComment != '' && rspComment.trim() != ''){
           if(component.get('v.responseType') == 'AppointmentReschedule' || component.get('v.responseType') == 'MoreInfoResponse' ){
                component.set("v.disableSubmitButton", false);
           }
           else if(component.find('aptId') && component.find('aptId').checkValidity() && component.find('aptId').get('v.value') && component.get("v.validationStatus") == true ){
                component.set("v.disableSubmitButton", false);
           }
           else{
            component.set("v.disableSubmitButton", true);  
           }
        }else{
            component.set("v.disableSubmitButton", true);
        }
    },
    //CUSTSA-27232 Appointment validate callout to CIS
    validateAPT : function(component, event, helper){
        //CUSTSA-27232 Appointment validate callout to CIS
        component.set("v.validationResponseText",'');
        component.set("v.validationStatus",false);
        component.set("v.validationResponse",'');
        var aptInputField = component.find('aptId');
        var apptIdVal = component.find('aptId').get('v.value');
        if(apptIdVal && !aptInputField.get('v.validity').patternMismatch ){
            component.set("v.validationResponse",'Processing');
            component.set("v.validationResponseText",'Validating Appointment ID.');
            helper.getAppointmentValidate(component, event, helper);
        }
    }
})