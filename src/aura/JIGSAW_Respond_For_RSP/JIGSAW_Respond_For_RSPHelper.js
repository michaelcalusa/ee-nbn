({
    getIncident : function(component, event, helper) {
        var currentIncident = component.get("v.attrCurrentIncidentRecord");
        var reasonCode = currentIncident.inm.reasonCode__c;
        var asRequestDescription = currentIncident.inm.asRequestDescription__c;
        var action = component.get("c.getPendingReasonType");
        action.setParams({
            "reasonCode": reasonCode,
            "asRequestDescription": asRequestDescription
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('response state :  ' + state);
            if(state === "SUCCESS") 
            {
                var result = response.getReturnValue();
                if(result && result != '')
                {
                    component.set("v.responseType",result);
                }
                else{
                    component.set('v.disableActionLink',true);
                }
            }
            else
            {
                console.log(this.ERR_MSG_UNKNOWN_ERROR);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": $A.get("$Label.c.GenericError")
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    resetAttributes : function(component, event, helper){
        component.set("v.disableSubmitButton", true);
        component.set("v.rspComment",'');
        component.set("v.docComposerComponentVisible", false);
        component.set("v.docComposerVisible", false);
        component.set("v.confirmationModalVisible", false);
        //CUSTSA-27232 Appointment validate callout to CIS
        component.set("v.validationResponseText",'');
        component.set("v.validationStatus",false);
        component.set("v.validationResponse",'');
    },
    handleResponseReceived : function(response, component, self) {
        var toastEvent = $A.get("e.force:showToast");
        //fire the event to auto refresh the recent history module
        var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
        addNoteRefreshEvent.fire();
        var varattrCurrentIncientRecord = response.getReturnValue();
        var spinnersToFire = '';
        if (varattrCurrentIncientRecord) {
            if(varattrCurrentIncientRecord.Awaiting_Current_Incident_Status__c)
                spinnersToFire = 'IncidentStatus';
            if(varattrCurrentIncientRecord.Awaiting_Current_SLA_Status__c)
                spinnersToFire = spinnersToFire + 'SLA';
            var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
            spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : varattrCurrentIncientRecord, "spinnersToStart" : spinnersToFire, "stopSpinner" : false});
            spinnerHandlerEvent.fire();
        }
    },
    // CUSTSA-27232 - Get Appointment details from CIS to validate AppointmentID
    getAppointmentValidate : function(component,event,helper)
    {
        var currentIncident = component.get("v.attrCurrentIncidentRecord");
        var action = component.get("c.getAppointmentValidateFromCIS");
        action.setParams({
            "integrationType": "getAppointmentDetailsCIS",
            "accessSeekerId" : currentIncident.inm.Access_Seeker_ID__c,
            "appointmentId": component.find('aptId').get('v.value')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("Appointment Details Transaction Id :- "+ JSON.stringify(response.getReturnValue()));
                var currentAppointmentDetails = response.getReturnValue();
                console.log("currentAppointmentDetails :- "+ JSON.stringify(currentAppointmentDetails));
                if (!$A.util.isUndefined(currentAppointmentDetails) && !$A.util.isEmpty(currentAppointmentDetails)) {
                    console.log("currentAppointmentDetails :- "+ JSON.stringify(currentAppointmentDetails));
                    if(currentAppointmentDetails.hasOwnProperty('httpStatusCode')){
                        // callout SUCCESS
                        console.log('status code : ' + currentAppointmentDetails.httpStatusCode);
                        if (currentAppointmentDetails.httpStatusCode == "200"){
                            component.set("v.validationStatus",false);
                            component.set("v.validationResponseText",'');
                            this.ValidateAppointment(component,currentAppointmentDetails,currentIncident.inm);
                        }
                        // If not able to connect with CIS server.
                        else if(currentAppointmentDetails.httpStatusCode == "500" || currentAppointmentDetails.httpStatusCode == "502"){
                            component.set("v.validationResponse",'Caution');
                            component.set("v.validationResponseText",'Unable to validate Apointment ID. Please manually validate in Maximo.');
                            component.set("v.validationStatus",true);
                            var aptBox = component.find('aptId');
                            $A.util.addClass(aptBox, 'borderCaution');
                            var rspComment = component.get("v.rspComment");
                            if(rspComment){
                                component.set("v.disableSubmitButton", false);
                            }
                            else{
                                component.set("v.disableSubmitButton", true);  
                            }
                        }
                        // Error returned by CIS
                            else if(currentAppointmentDetails.httpStatusCode == "403" ){ //|| currentAppointmentDetails.httpStatusCode == "013007"
                                component.set("v.validationResponse",''); 
                                //Forbidden. The Access Seeker provided is not authorised to access this resource. 
                                component.set("v.validationResponseText",'Appointment is invalid. Please retry with a valid Appointment ID.'); //The Access Seeker provided is not authorised to access this resource
                                component.set("v.validationStatus",false);
                                var msgCmp = component.find('validationMsg');
                                $A.util.removeClass(msgCmp, 'greyColor'); 
                                $A.util.addClass(msgCmp, 'redColor');
                                var aptBox = component.find('aptId');
                                $A.util.addClass(aptBox, 'borderRed');
                                component.set("v.disableSubmitButton", true);  
                            }
                                else{
                                    component.set("v.validationStatus",true);
                                    component.set("v.validationResponse",'Caution');
                                    component.set("v.validationResponseText",'Unable to validate Apointment ID. Please manually validate in Maximo.');
                                    var aptBox = component.find('aptId');
                                    $A.util.removeClass(aptBox, 'borderRed');
                                    $A.util.addClass(aptBox, 'borderCaution');
                                    var msgCmp = component.find('validationMsg');
                                    $A.util.removeClass(msgCmp, 'redColor');
                                	$A.util.addClass(msgCmp, 'greyColor'); 
                               		var rspComment = component.get("v.rspComment");
                                    if(rspComment){
                                        component.set("v.disableSubmitButton", false);
                                    }
                                    else{
                                        component.set("v.disableSubmitButton", true);   
                                    }
                                }
                    }
                    // "httpStatusCode" if null. Handle with Generic message
                    else{
                        console.log('come into not call issue error');	
                        component.set("v.validationResponse",''); 
                        //Forbidden. The Access Seeker provided is not authorised to access this resource. 
                        component.set("v.validationResponseText",'Appointment is invalid. Please retry with a valid Appointment ID.'); //The Access Seeker provided is not authorised to access this resource
                        component.set("v.validationStatus",false);
                        var msgCmp = component.find('validationMsg');
                        $A.util.removeClass(msgCmp, 'greyColor'); 
                        $A.util.addClass(msgCmp, 'redColor');
                        var aptBox = component.find('aptId');
                        $A.util.addClass(aptBox, 'borderRed');
                        component.set("v.disableSubmitButton", true);
                    }
                }
                // "currentAppointmentDetails" if null. Handle with Generic message
                else{
                    component.set("v.showAppointmentMoreInfoTable", false);
                    component.set("v.showAppointmentMoreInfoPending", true);
                    console.log('currentAppointmentDetails===>'+currentAppointmentDetails);
                    component.set("v.appointmentMoreInfoTransactionMessage", " API Response not received. Please try again.");
                    component.set("v.validationStatus",false);
                    
                }
            }
            else if (state === 'ERROR') {
                var errors = response.getError();
                component.set("v.showAppointmentMoreInfoTable", false);
                component.set("v.showAppointmentMoreInfoPending", true);
                component.set("v.appointmentMoreInfoTransactionMessage", errors[0].message);
                component.set("v.validationStatus",false);
                
            }
            
        });
        $A.enqueueAction(action);
    },
    //CUSTSA-27232 Appointment validate callout to CIS. Check for parameters when CIS callout return success
    ValidateAppointment : function(component, currentAppointmentDetails, currentIncidentWrapper) {
        var errorString = '';
        console.log(' aptid is 2 - -' + component.get("v.AppointmentId"));
        if(!currentIncidentWrapper.locId__c || !currentAppointmentDetails.involvesPlace.id || currentAppointmentDetails.involvesPlace.id.toLowerCase() != currentIncidentWrapper.locId__c.toLowerCase()){
            errorString += errorString?', Location Id':'Location Id';
        }
        if (!currentAppointmentDetails.lifecycleStatus || currentAppointmentDetails.lifecycleStatus.toLowerCase() != 'reserved'){
            errorString += errorString?', Life Cycle Status' : 'Life Cycle Status';
        }
        if (!currentIncidentWrapper.Technology__c || !currentAppointmentDetails.demandType || (currentIncidentWrapper.Technology__c == 'FTTN' || 
                                                                                               currentIncidentWrapper.Technology__c === 'FTTB' || 
                                                                                               currentIncidentWrapper.Technology__c === 'NHAS') && 
            currentAppointmentDetails.demandType.toLowerCase() !== 'service restoration' ) {
            
            errorString += errorString?', Demand Type':'Demand Type';
        }
        else if (currentIncidentWrapper.Technology__c == 'NHUR' && 
                 currentAppointmentDetails.demandType.toLowerCase() != 'nhur service restoration'){
            
            errorString += errorString?', Demand Type':'Demand Type';
        }
        if (!currentAppointmentDetails.segment || (currentIncidentWrapper.Segment__c && currentIncidentWrapper.Segment__c.toLowerCase() == 'business' && currentAppointmentDetails.segment.toLowerCase() != 'business')){
            errorString += errorString?', Segment':'Segment';
        }  
        if(currentAppointmentDetails.involvesPlace.serviceRestorationSLA  && currentIncidentWrapper.serviceRestorationSLA__c){
            
            var serviceRestorationSLAMap = component.get('v.restorationSLAMap');
            var incKey = currentIncidentWrapper.serviceRestorationSLA__c;
             // Check for SLA type and Priority Assist value, if  any mismatch, show the error message to check SLA type.
            if(serviceRestorationSLAMap && 
               (serviceRestorationSLAMap[incKey] != currentAppointmentDetails.involvesPlace.serviceRestorationSLA || 
                !currentIncidentWrapper.PriorityAssist__c || !currentAppointmentDetails.priorityAssist || 
                currentAppointmentDetails.priorityAssist.toLowerCase() != currentIncidentWrapper.PriorityAssist__c.toLowerCase())){
                errorString += errorString?', SLA type':'SLA type';
            }
        }
        else
            errorString += errorString?', SLA type':'SLA type';  
       /* if(!currentIncidentWrapper.PriorityAssist__c || !currentAppointmentDetails.priorityAssist || currentAppointmentDetails.priorityAssist.toLowerCase() != currentIncidentWrapper.PriorityAssist__c.toLowerCase()){
            errorString += errorString?', Priority Assist':'Priority Assist';
        } */
        
        console.log(' error string is -- ' + errorString);	
        
        if(errorString){
            errorString = 'Appointment is invalid.  Please check ' + errorString + '.';
            component.set("v.validationResponseText",errorString);
            component.set("v.validationStatus",false);
            component.set("v.validationResponse",'');
            var msgCmp = component.find('validationMsg');
            $A.util.removeClass(msgCmp, 'greyColor'); 
            $A.util.addClass(msgCmp, 'redColor');
            var aptBox = component.find('aptId');
            $A.util.addClass(aptBox, 'borderRed');
            component.set("v.disableSubmitButton", true);
        }
        else{
            component.set("v.validationStatus",true);
            component.set("v.validationResponse",'Success');
            component.set("v.validationResponseText",'');
            var aptBox = component.find('aptId');
            $A.util.removeClass(aptBox, 'borderRed');
            var rspComment = component.get("v.rspComment");
            if(rspComment){
                component.set("v.disableSubmitButton", false);
            }
            else{
                component.set("v.disableSubmitButton", true); 
            }
            
        }
        
    },
    
    //CUSTSA-27232  Create Key value pair for ServiceRestorationSLA values Key = Incident -> serviceRestorationSLA , value = Remedy -> serviceRestorationSLA
    ServiceRestorationSLAMap : function(component, event, helper){
        
        var serviceRestorationSLAMap = {
            "PRI-Standard SLA":"Standard",
            "Enhanced_12_24x7":"Enhanced - 12 (24/7)",
            "PRI-Enhanced-12 SLA":"Enhanced - 12",
            "Enhanced_6_24x7":"Enhanced - 6 (24/7)",
            "Enhanced_6_7am_9pm":"Enhanced - 6",
            "Enhanced_8_24x7":"Enhanced - 8 (24/7)",
            "Enhanced_8_7am_9pm":"Enhanced - 8",
            "Enhanced_4_24x7":"Enhanced - 4 (24/7)",
            "Enhanced_4_7am_9pm":"Enhanced - 4"
        };
        component.set('v.restorationSLAMap',serviceRestorationSLAMap);
    }
    
})