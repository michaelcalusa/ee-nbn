({
	setMoreInfoTable : function(component, event, helper) {
	    
	    //debugger;
        var jsonPayload = JSON.parse(component.get('v.deserializedObject'));
        var moreInfoId = component.get('v.moreInfoId');
        
        //variable to be displayed in table [Product details variables]
        var installationOpt = '';
        var serviceRestorationSLA = '';
        var dslStabilityProfile = '';
        
        //variables to be displayed in table [Data, UNI-DSL variables]
        var trafficClass4 = '';
        var trafficClass2 = '';
        var trafficClass1 = '';
        var cvcId = '';
        var ctag = '';
        let accessLoopId = '';
        
        if(jsonPayload != undefined && jsonPayload != '{}'){
            if(jsonPayload.included.length > 0){
                for(var i = 0; i < jsonPayload.included.length; i++){
                    if(jsonPayload.included[i].id == moreInfoId){
                        if(jsonPayload.included[i].attributes != undefined && jsonPayload.included[i].attributes != '{}'){
                            if(jsonPayload.included[i].attributes.orderAttrs != undefined && jsonPayload.included[i].attributes.orderAttrs != '{}'){
                                
                                //code to find Installation options
                                if(jsonPayload.included[i].attributes.orderAttrs.installationOptionsCentralSplitter != undefined 
                                && jsonPayload.included[i].attributes.orderAttrs.installationOptionsCentralSplitter != '' 
                                && jsonPayload.included[i].attributes.orderAttrs.installationOptionsCentralSplitter == 'Yes'){
                                  
                                  installationOpt = 'Central Splitter';
                                }
                                                               
                                //code to find Service Restoration SLA
                                if(jsonPayload.included[i].attributes.orderAttrs.serviceRestorationSla != undefined
                                && jsonPayload.included[i].attributes.orderAttrs.serviceRestorationSla != ''){
                                    
                                    serviceRestorationSLA = jsonPayload.included[i].attributes.orderAttrs.serviceRestorationSla;
                                }
                                
                                //code to find DLS Stability Profile
                                if(jsonPayload.included[i].attributes.orderAttrs.uniDsl != undefined
                                && jsonPayload.included[i].attributes.orderAttrs.uniDsl.length > 0){
                                    
                                    if(jsonPayload.included[i].attributes.orderAttrs.uniDsl[0].dslStabilityProfile != undefined
                                    && jsonPayload.included[i].attributes.orderAttrs.uniDsl[0].dslStabilityProfile != ''){
                                        
                                        dslStabilityProfile = jsonPayload.included[i].attributes.orderAttrs.uniDsl[0].dslStabilityProfile;
                                    }
                                }
                                
                                //code to find trafic classes
                                if(jsonPayload.included[i].attributes.orderAttrs.avcD != undefined
                                && jsonPayload.included[i].attributes.orderAttrs.avcD.length > 0){
                                    
                                    if(jsonPayload.included[i].attributes.orderAttrs.avcD[0].bandwidthProfile != undefined
                                    && jsonPayload.included[i].attributes.orderAttrs.avcD[0].bandwidthProfile != ''){
                                        
                                        var bandwithProfile = jsonPayload.included[i].attributes.orderAttrs.avcD[0].bandwidthProfile;
                                        var bandwithProfileArray = bandwithProfile.split(',');
                                        
                                        for(var j=0; j< bandwithProfileArray.length; j++){
                                            
                                            //code to find traffic class 4 details
                                            if(bandwithProfileArray[j].indexOf('TC4') != -1){
                                                
                                                var downloadSpeed = bandwithProfileArray[j].split('_')[0].replace('D','');
                                                var uploadSpeed = bandwithProfileArray[j].split('_')[1].replace('U','');
                                                var speed = bandwithProfileArray[j].split('_')[2];
                                                
                                                //select the upper range of the speed if seperated by '-'
                                                downloadSpeed = downloadSpeed.split('-').length > 1 ? downloadSpeed.split('-')[1] : downloadSpeed.split('-')[0]; 
                                                uploadSpeed = uploadSpeed.split('-').length > 1 ? uploadSpeed.split('-')[1] : uploadSpeed.split('-')[0]; 
                                                
                                                trafficClass4 = downloadSpeed+'/'+uploadSpeed+' '+speed;
                                                
                                            }
                                            
                                            //code to find traffic class 2 details
                                            if(bandwithProfileArray[j].indexOf('TC2') != -1){
                                                
                                                var downloadSpeed = bandwithProfileArray[j].split('_')[0].replace('D','');
                                                var uploadSpeed = bandwithProfileArray[j].split('_')[1].replace('U','');
                                                var speed = bandwithProfileArray[j].split('_')[2];
                                                
                                                //select the upper range of the speed if seperated by '-'
                                                downloadSpeed = downloadSpeed.split('-').length > 1 ? downloadSpeed.split('-')[1] : downloadSpeed.split('-')[0]; 
                                                uploadSpeed = uploadSpeed.split('-').length > 1 ? uploadSpeed.split('-')[1] : uploadSpeed.split('-')[0]; 
                         
                                                trafficClass2 = downloadSpeed+'/'+uploadSpeed+' '+speed;
                                            }
                                            
                                            //code to find traffic class 1 details
                                            if(bandwithProfileArray[j].indexOf('TC1') != -1){
                                                
                                                var downloadSpeed = bandwithProfileArray[j].split('_')[0].replace('D','');
                                                var uploadSpeed = bandwithProfileArray[j].split('_')[1].replace('U','');
                                                var speed = bandwithProfileArray[j].split('_')[2];
                                                
                                                //select the upper range of the speed if seperated by '-'
                                                downloadSpeed = downloadSpeed.split('-').length > 1 ? downloadSpeed.split('-')[1] : downloadSpeed.split('-')[0]; 
                                                uploadSpeed = uploadSpeed.split('-').length > 1 ? uploadSpeed.split('-')[1] : uploadSpeed.split('-')[0]; 
                         
                                                trafficClass1 = downloadSpeed+'/'+uploadSpeed+' '+speed;
                                            }
                                        }
                                    }
                                    
                                    //code to find cvcId
                                    if(jsonPayload.included[i].attributes.orderAttrs.avcD[0].cvcId != undefined
                                    && jsonPayload.included[i].attributes.orderAttrs.avcD[0].cvcId != ''){
                                        
                                        cvcId = jsonPayload.included[i].attributes.orderAttrs.avcD[0].cvcId;
                                    }
                                    
                                    //code to find ctag
                                    if(jsonPayload.included[i].attributes.orderAttrs.avcD[0].nniCvlanId != undefined
                                    && jsonPayload.included[i].attributes.orderAttrs.avcD[0].nniCvlanId != ''){
                                        
                                        ctag = jsonPayload.included[i].attributes.orderAttrs.avcD[0].nniCvlanId;
                                    }

                                    //code to find access identification loop for HFC incident
                                    if(jsonPayload.included[i].attributes.orderAttrs.avcD[0].accessLoopIdentification != undefined
                                        && jsonPayload.included[i].attributes.orderAttrs.avcD[0].accessLoopIdentification != ''){
                                            
                                            accessLoopId = jsonPayload.included[i].attributes.orderAttrs.avcD[0].accessLoopIdentification;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            component.set('v.installationOpt',installationOpt);
            component.set('v.serviceRestorationSLA',serviceRestorationSLA);
            component.set('v.dslStabilityProfile',dslStabilityProfile);
            component.set('v.trafficClass4',trafficClass4);
            component.set('v.trafficClass2',trafficClass2);
            component.set('v.trafficClass1',trafficClass1);
            component.set('v.cvcId',cvcId);
            component.set('v.ctag',ctag);
            component.set('v.accessLoopId',accessLoopId);
            
        }
	
	}
})