({
    toggleLinkText : function(component, event, helper) {
        //toggle text for show hide
        component.set("v.toggleText",!component.get("v.toggleText"));
        var appEvent = $A.get("e.c:JIGSAW_ShowHideDataSummaryEvent");
        appEvent.setParam("callingComponent", component.get("v.callingComponent"));
        appEvent.setParam("showTable", component.get("v.toggleText"));
        appEvent.setParam("Dayscount",component.get("v.Dayscount"));
        appEvent.setParam("recordId", component.get("v.recordId"));
        appEvent.setParam("tableHeader", component.get("v.tableHeader"));
        appEvent.setParam("locId", component.get("v.locId"));
        appEvent.setParam("appointmentTransactionId", component.get("v.appointmentTransactionId"));
        appEvent.setParam("currentIncidentRecord", component.get("v.currentIncidentRecord"));
        appEvent.fire();
    },
    handleApplicationEvent : function(component, event, helper){
        if(event.getParam("recordId") === component.get("v.recordId")) {
            //this block of code is executed when clicked on X(close) button on summary table        
            var toggleLink = event.getParam('toggleComponentlink');
            var parentComponentReload = event.getParam('parentComponentReload');
            if(toggleLink && parentComponentReload == component.get("v.callingComponent")){
                component.set("v.toggleText",!component.get("v.toggleText"));
            }
            //this block of code is executed when show/hide link is clicked on another module
            var callingComponent = event.getParam("callingComponent");
            var showTable = event.getParam("showTable");
            if(callingComponent != component.get("v.callingComponent") && showTable){
                if(component.get("v.toggleText")){
                    component.set("v.toggleText",!component.get("v.toggleText"));
                }
            }
        }
    }
})