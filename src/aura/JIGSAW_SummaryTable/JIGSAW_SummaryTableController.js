({
    handleApplicationEvent: function(component, event, helper) {
        var recordId = event.getParam("recordId");
        var callingComponent = event.getParam("callingComponent");
        var showTable = event.getParam("showTable");
        var Dayscount = event.getParam("Dayscount");
        var tableHeader = event.getParam("tableHeader");
        var locId = event.getParam("locId");
        var showMoreInfoTable = event.getParam("showMoreInfoTable");
        var currentIncidentRecord = event.getParam("currentIncidentRecord");
        if(showMoreInfoTable == undefined || showMoreInfoTable != true) {
            //helper.resetDataTable(component, event, helper); //MRTEST 3473
            if(recordId == component.get('v.recordId')) {
                helper.resetDataTable(component, event, helper);
                component.set('v.parentComponent', callingComponent);
                component.set('v.Dayscount', Dayscount);
                component.set('v.locId', locId);
                component.set("v.showTable", event.getParam("showTable"));
                if (component.get("v.showTable") == true) {
                    if (callingComponent === "Incident_Status") {
                        component.set("v.confirmationModalVisible", true);
                        component.set("v.displaySummaryTable", false);
                        var incidentEngagementType = currentIncidentRecord.Engagement_Type__c;
                        if((currentIncidentRecord.Prod_Cat__c && currentIncidentRecord.Prod_Cat__c == "NHUR") || component.get('v.CISAppointMentRequestEnabled') != 'true')
                        {
                            component.set("v.showAccessTab", false);
                        }
						// No Callout made for Commitment. Fetch from incident record
                        if( incidentEngagementType=='Commitment') {
                            component.set("v.showAppointmentMoreInfo", false);
                            component.set("v.showCommitmentMoreInfo", true);
                        }
                        else {
                            component.set("v.showAppointmentMoreInfo", true);
                            component.set("v.showCommitmentMoreInfo", false);
							// Make Microservice getAppointment Callout
                            if (event.getParam("appointmentTransactionId") && component.get('v.CISAppointMentRequestEnabled') != 'true') {
                                helper.getAppointmentDetailsFromServer(component, event.getParam("appointmentTransactionId"));
                            }
							// Make CIS getAppointment Callout CUSTSA-24874, CUSTSA-24875
                            else if(component.get('v.CISAppointMentRequestEnabled') === 'true'){
                                helper.getAppointmentDetailsFromCIS(component, currentIncidentRecord);
                            }
							// Handling Unknown / not valid condition
                            else {
                                component.set("v.showAppointmentMoreInfoTable", false);
                                component.set("v.showAppointmentMoreInfoPending", true);
                                component.set("v.appointmentMoreInfoTransactionMessage","Appointment More Info Request Failed, kindly Refresh The Browser To Raise New Request");
                            }
                        }
                    }
                    else {
                        component.set("v.confirmationModalVisible", true);
                        component.set("v.showAppointmentMoreInfo", false);
                        component.set("v.showCommitmentMoreInfo", false);
                        component.set("v.displaySummaryTable", true);
                        component.set("v.tableHeader", tableHeader);
                        helper.handlerSummaryEvent(component, event, helper,callingComponent);
                    }
                }
                else {
                    component.set("v.confirmationModalVisible", false);
                }
            }
        }
    },
    updateColumnSorting: function(cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, event, helper, fieldName, sortDirection);
    },
    
    next: function(component, event, helper) {
        //debugger;
        var sObjectList = component.get("v.lstRecords");
        var end = parseInt(component.get("v.endPage"));
        var start = parseInt(component.get("v.startPage"));
        var pageSize = parseInt(component.get("v.rowsPerPage"));
        var PagList = [];
        var counter = 0;
        for (var i = end + 1; i < end + pageSize + 1; i++) {
            if (sObjectList.length > i) {
                PagList.push(sObjectList[i]);
            }
            counter++;
        }
        start = start + counter;
        end = end + counter;
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.PaginationList', PagList);
    },
    previous: function(component, event, helper) {
        var sObjectList = component.get("v.lstRecords");
        var end = parseInt(component.get("v.endPage"));
        var start = parseInt(component.get("v.startPage"));
        var pageSize = parseInt(component.get("v.rowsPerPage"));
        var PagList = [];
        var counter = 0;
        for (var i = start - pageSize; i < start; i++) {
            if (i > -1) {
                PagList.push(sObjectList[i]);
                counter++;
            } else {
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.PaginationList', PagList);
    },
    
    handleClose: function(component, event, helper) {
        helper.resetDataTable(component, event, helper);
        var appEvent = $A.get("e.c:JIGSAW_ShowHideDataSummaryEvent");
        appEvent.setParam("parentComponentReload", component.get("v.parentComponent"));
        appEvent.setParam("toggleComponentlink", true);
        appEvent.setParam("recordId", component.get('v.recordId'));
        appEvent.fire();
    },
    
    
    
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    
    // this function automatic call when button in data table is clicked
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        //alert(action.name);
        switch (action.name) {
            case 'open_incident':
                helper.open_incident(cmp,row);
                break;
                
            case 'open_incident_WO':
                helper.open_incident_WO(cmp,row);
                break;    
                
            case 'open_work_order_detail':
                helper.open_work_order_detail(cmp,row);
                break;
            case 'open_OrderMoreInfo':
                helper.open_product_order_view_more(cmp,row);
                break;
            default:
                //helper.showRowDetails(row);
                break;
        }
    }
    
})