({
    resetDataTable : function(component,event,helper){
        component.set("v.confirmationModalVisible", false);
        component.set("v.sortedBy","");
        component.set("v.sortedDirection","asc");
        component.set("v.mycolumns", []);
        component.set("v.lstRecords", []);
        component.set("v.PaginationList", []);
        component.set("v.totalSize", 0);
        component.set("v.tableHeader","");
        component.set("v.startPage", 0);                
        component.set("v.endPage", 0);
    },
    setDataTable : function (component, event, helper){
        
        var pageSize = parseInt(component.get("v.rowsPerPage"));
        var records = component.get("v.lstRecords");
        component.set("v.startPage", 0);                
        component.set("v.endPage", pageSize - 1);
        var PagList = [];
        for ( var i=0; i< pageSize; i++ ) {
            if ( records.length > i )
                PagList.push(records[i]);    
        }
        component.set('v.PaginationList', PagList);
        
    },
    handlerSummaryEvent: function(component, event, helper, callingComponent) {
        
        if (callingComponent == 'INCIDENT') {
            helper.fetchIncidentData(component, event, helper);
        }
        if (callingComponent == 'SERVICE') {
            helper.fetchServiceChanges(component, event, helper);
        }
        if (callingComponent == 'WORKORDER') {
            helper.fetchWorkOrder(component, event, helper);
        }
        
    },
    
    fetchServiceChanges: function(component, event, helper) {
        var action = component.get("c.fetchRecords");
     
        action.setParams({
            callingComponent: 'SERVICE',
            filter: component.get('v.recordId'),
            dayCount: '0',
            locId: component.get('v.locId')
        });
           //alert('test');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var myCols = response.getReturnValue().lstColumns;
                var jsonPayload = response.getReturnValue().deserializedObject;
                component.set('v.deserializedObject',jsonPayload);
                myCols[0].typeAttributes = {
                    label: {
                        fieldName: 'Orderid'
                    },
                    name:'open_OrderMoreInfo',
                    title: 'Click for more info',
                    disabled: {
                        fieldName: 'disabled_Orderid'
                    },
                    class: {
                        fieldName: 'orderClass'
                    },
                };
                var lstData = response.getReturnValue().lstSobjectData;
                lstData = lstData.map(function(rowData) {
                    switch (rowData.orderType) {
                            
                        case 'Modify':
                            rowData.disabled_Orderid = false;
                            rowData.orderClass = 'ordModify';
                            break;
                        default:
                            rowData.disabled_Orderid = true;
                            rowData.orderClass = 'ordDisconnect hideTitle';
                            break;
                    }
                    if(rowData.dateAccepted != '' && rowData.dateAccepted != undefined){
                        rowData.dateAccepted_date = new Date(rowData.dateAccepted.replace( /(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3"));
                        rowData.dateAccepted = rowData.dateAccepted.substring(0,rowData.dateAccepted.length - 3);
                    }
                    if(rowData.dateCompleted != '' && rowData.dateCompleted != undefined){
                        rowData.dateCompleted_date = new Date(rowData.dateCompleted.replace( /(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3"));
                        rowData.dateCompleted = rowData.dateCompleted.substring(0,rowData.dateCompleted.length - 3);
                    }
                    
                    return rowData;
                });
                component.set("v.sortedBy","dateAccepted");
                component.set("v.sortedDirection","desc");
                component.set("v.mycolumns", myCols);
                component.set("v.lstRecords", lstData);
                component.set("v.totalSize", lstData.length);
                helper.sortData(component,event,helper,"dateAccepted","desc");
                
                var priId = response.getReturnValue().priId;
                component.set("v.tableHeader",component.get('v.tableHeader').replace('PRI',priId));
                
                //set datatable
                helper.setDataTable(component ,event, helper);
                
            } else if (state === 'ERROR') {
                var errors = response.getError();
                console.log("Error message: " + errors[0].message);
                component.set("v.summaryTableError", errors[0].message);
                component.set("v.summaryError",true);
            }
        });
        $A.enqueueAction(action);
    },
    
    
    // Sunil Fetch Incidents Details
    fetchIncidentData: function(component, event, helper) {
        var action = component.get("c.fetchRecords");
        action.setParams({
            callingComponent: 'INCIDENT',
            filter: component.get('v.recordId'),
            dayCount: '0',
            locId: component.get('v.locId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                //debugger;
                var resSobject = response.getReturnValue().lstSobjectData;
                var currentRecordId = component.get('v.recordId');

                //add Technology type column for HFC incidents
                var incidentRec = component.get("v.incidentRecord");

                // Update Columns
                var mycolumns = response.getReturnValue().lstColumns;
                var columnsUpdated = [];
                for (var singlekey in mycolumns) {
                    //alert(singlekey);
                    var arrayOfCols = {};
                    arrayOfCols['fieldName'] = mycolumns[singlekey].fieldName;
                    arrayOfCols['label'] = mycolumns[singlekey].label;
                    arrayOfCols['sortable'] = mycolumns[singlekey].sortable;
                    arrayOfCols['type'] = mycolumns[singlekey].type;
                    
                    if(mycolumns[singlekey].fieldName == 'Incident_Number__c'){
                        arrayOfCols['type'] = 'button';
                        arrayOfCols['initialWidth'] = 158;
                        arrayOfCols['fieldName'] = 'Id';
                        arrayOfCols['typeAttributes'] =  { label: { fieldName: 'Incident_Number__c' }, name:'open_incident', title: 'Click here to open in a new tab', disabled: {fieldName: 'disabled_fieldName'}, class: {fieldName: 'class_fieldName'} };
                    }

                     //AVC column is not applicable for HFC
                     if(mycolumns[singlekey].fieldName != 'AVC_Id__c' || (incidentRec.Technology__c != 'NHAS' && incidentRec.Technology__c != 'NHUR')){
                        columnsUpdated.push(arrayOfCols);
                    }

                }
                
                if(incidentRec.Technology__c == 'NHAS' || incidentRec.Technology__c == 'NHUR'){
                    var arrayOfCols = {};
                    arrayOfCols['fieldName'] = 'Technology__c';
                    arrayOfCols['label'] = 'Technology';
                    arrayOfCols['sortable'] = false;
                    arrayOfCols['type'] = 'text';
                    
                    columnsUpdated.splice(1, 0, arrayOfCols);
                } 

                // Update data
                for (var i = 0; i < resSobject.length; i++) {
                    if (!$A.util.isUndefined(resSobject[i])) {
                        if (resSobject[i].MTTR__c != 'undefined' && resSobject[i].MTTR__c != null && resSobject[i].MTTR__c > 0) {
                            resSobject[i].MTTR__c = resSobject[i].MTTR__c + ' hrs';
                        } else {
                            resSobject[i].MTTR__c = '';
                        }
                        
                        if (resSobject[i].Reported_Date__c != 'undefined' && resSobject[i].Reported_Date__c != null){
                            resSobject[i].ReportedDate = resSobject[i].Reported_Date__c;
                            resSobject[i].Reported_Date__c = $A.localizationService.formatDate(resSobject[i].Reported_Date__c, "DD/MM/YYYY HH:mm");
                        }
                        if (resSobject[i].closedDate__c != 'undefined' && resSobject[i].closedDate__c != null){
                            resSobject[i].closedDate = resSobject[i].closedDate__c;
                            resSobject[i].closedDate__c = $A.localizationService.formatDate(resSobject[i].closedDate__c, "DD/MM/YYYY HH:mm");
                        }
                        
                        if (resSobject[i].Id != currentRecordId){
                            resSobject[i].Id = resSobject[i].Id;
                            resSobject[i].disabled_fieldName = false;
                            resSobject[i].class_fieldName = 'ordDisconnect';
                        }else{
                            resSobject[i].Id = '';
                            resSobject[i].disabled_fieldName = true;
                            resSobject[i].class_fieldName = 'ordModify';
                        }
                    }
                }
                
                
                
                
                /*
            	 component.set('v.mycolumns', [
                    {label: 'Incident Name', fieldName: 'Id', type: 'url', sortable: true, typeAttributes: { label: { fieldName: 'Incident_Number__c' } }},
                    {label: 'Fault Type', fieldName: 'Fault_Type__c', type: 'Text',sortable: true}
                ]);
              
              */
                
                var locationId = response.getReturnValue().locId;
                component.set("v.tableHeader",component.get('v.tableHeader').replace('LOC',locationId));
                
                
                console.log(columnsUpdated);
                console.log(resSobject);
                
                component.set("v.mycolumns", columnsUpdated);
                component.set("v.lstRecords", resSobject);
                component.set("v.totalSize", resSobject.length);
                
                helper.setDataTable(component,event,helper);
                
            } else if (state === 'ERROR') {
                var errors = response.getError();
                console.log("Error message: " + errors[0].message);
            }
        });
        $A.enqueueAction(action);
    },
    
    sortData: function(component,event,helper, fieldName, sortDirection) {
        var data = component.get("v.lstRecords");
        var reverse = sortDirection !== 'asc';
        if(fieldName == 'Reported_Date__c'){
            fieldName = 'ReportedDate';
        }
        if(fieldName == 'closedDate__c'){
            fieldName = 'closedDate';
        }
        if(fieldName == 'dateAccepted'){
            fieldName = 'dateAccepted_date';
        }
        if(fieldName == 'dateCompleted'){
            fieldName = 'dateCompleted_date';
        }
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.lstRecords", data);
        helper.setDataTable(component,event,helper);
    },
    
    sortBy: function(field, reverse, primer) {
        var key = primer ?
            function(x) {
                return primer(x[field])
            } :
        function(x) {
            return x[field]
        };
        reverse = !reverse ? 1 : -1;
        return function(a, b) {
            //return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
            return a = key(a)?key(a):'', b = key(b)?key(b):'', reverse * ((a > b) - (b > a));
        }
    },
    
    getAppointmentDetailsFromServer: function(component, appointmentTransactionId) {
        var action = component.get("c.getAppointmentDetails");
        action.setParams({
            "appointmentTransactionId": appointmentTransactionId,
            "integrationType": "getAppointmentDetails"
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(response.getReturnValue());
            if (state === "SUCCESS") {
                var currentAppointmentDetails = response.getReturnValue();
                if (currentAppointmentDetails && currentAppointmentDetails.transactionStatus != "Error") {
                    this.formatSunsetAppointmentDates(component,currentAppointmentDetails);
                }
                else
                {
                    component.set("v.showAppointmentMoreInfoTable", false);
                    component.set("v.showAppointmentMoreInfoPending", true);
                    if(currentAppointmentDetails.hasOwnProperty('exception'))
                    	component.set("v.appointmentMoreInfoTransactionMessage", currentAppointmentDetails.exception.message);
                    if(currentAppointmentDetails.hasOwnProperty('errors'))
                        component.set("v.appointmentMoreInfoTransactionMessage", currentAppointmentDetails.errors[0].errorMessage);
                }
            }
            else if (state === 'ERROR') {
                var errors = response.getError();
                component.set("v.showAppointmentMoreInfoTable", false);
                component.set("v.showAppointmentMoreInfoPending", true);
                component.set("v.appointmentMoreInfoTransactionMessage", errors[0].message);
            }
        });
        $A.enqueueAction(action);
    },
    
    getAppointmentDetailsFromCIS: function(component, currentIncidentRecord) {
        var action = component.get("c.getAppointmentDetailsFromCIS");
        action.setParams({
            "integrationType": "getAppointmentDetailsCIS",
            "accessSeekerId" : currentIncidentRecord.Access_Seeker_ID__c,
            "appointmentId": currentIncidentRecord.AppointmentId__c
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var currentAppointmentDetails = response.getReturnValue();
                if (!$A.util.isUndefined(currentAppointmentDetails) && !$A.util.isEmpty(currentAppointmentDetails)) {
					if(currentAppointmentDetails.hasOwnProperty('httpStatusCode')){
						if (currentAppointmentDetails.httpStatusCode == "200") {
                            if(currentAppointmentDetails && currentAppointmentDetails.accreditationRequirements)
                            {
                                component.set("v.accreditationRequirementsValues",currentAppointmentDetails.accreditationRequirements.split(","));
                            }
                            else
                            {
                            	component.set("v.accreditationRequirementsValues",null); 
                            }
                            if(currentAppointmentDetails && currentAppointmentDetails.siteConsiderations)
                            {
                                component.set("v.siteConsiderationsLabel",currentAppointmentDetails.siteConsiderations.split("\n"));
                            }
                            else
                            {
                                component.set("v.siteConsiderationsLabel",null);
                            }
							this.formatSunsetAppointmentDates(component,currentAppointmentDetails,component.get('v.CISAppointMentRequestEnabled') == 'true');
						}
						else if(currentAppointmentDetails.httpStatusCode == "500" || currentAppointmentDetails.httpStatusCode == "502"){
							component.set("v.showAppointmentMoreInfoTable", false);
							component.set("v.showAppointmentMoreInfoPending", true);
							if(currentAppointmentDetails.hasOwnProperty('message')){
								component.set("v.appointmentMoreInfoTransactionMessage",currentAppointmentDetails.message);
							}
							else{
								component.set("v.appointmentMoreInfoTransactionMessage","Some error occurred. Please try again.");
							}	
						}
						else{
							component.set("v.showAppointmentMoreInfoTable", false);
							component.set("v.showAppointmentMoreInfoPending", true);
							if(currentAppointmentDetails.hasOwnProperty('code') && currentAppointmentDetails.hasOwnProperty('description')){
								component.set("v.appointmentMoreInfoTransactionMessage",currentAppointmentDetails.description);
							}
							else{
								component.set("v.appointmentMoreInfoTransactionMessage","Some error occurred. Please try again.");
							}
						}
					}
					// "httpStatusCode" if null. Handle with Generic message
					else{
						component.set("v.showAppointmentMoreInfoTable", false);
						component.set("v.showAppointmentMoreInfoPending", true);
						console.log('currentAppointmentDetails===>'+currentAppointmentDetails);
						component.set("v.appointmentMoreInfoTransactionMessage", "Some error occurred. Please try again.");
					}
				}
				// "currentAppointmentDetails" if null. Handle with Generic message
				else{
					component.set("v.showAppointmentMoreInfoTable", false);
					component.set("v.showAppointmentMoreInfoPending", true);
					console.log('currentAppointmentDetails===>'+currentAppointmentDetails);
					component.set("v.appointmentMoreInfoTransactionMessage", "Some error occurred. Please try again.");
				}
            }
            else if (state === 'ERROR') {
                var errors = response.getError();
                component.set("v.showAppointmentMoreInfoTable", false);
                component.set("v.showAppointmentMoreInfoPending", true);
                component.set("v.appointmentMoreInfoTransactionMessage", errors[0].message);
            }
        });
        $A.enqueueAction(action);
    },
    
    formatSunsetAppointmentDates : function(component, appointmentDetails, cisRequestEnabled) {
        appointmentDetails.appointmentSunsetExpiryDateTime = cisRequestEnabled ? appointmentDetails.involvesAppointmentSlot.validFor  : appointmentDetails.appointmentSunsetExpiryDateTime;
        appointmentDetails.firstAvailableStartDateTime = cisRequestEnabled ? appointmentDetails.involvesFirstAvailableAppointmentSlot.appointmentDate.startDateTime : appointmentDetails.firstAvailableStartDateTime;
		appointmentDetails.firstAvailableEndDateTime = cisRequestEnabled ? appointmentDetails.involvesFirstAvailableAppointmentSlot.appointmentDate.endDateTime : appointmentDetails.firstAvailableEndDateTime;
        if(appointmentDetails.appointmentSunsetExpiryDateTime)
        {
            var sunsetExpiryDate = cisRequestEnabled ? new Date(appointmentDetails.appointmentSunsetExpiryDateTime) : new Date(Number(appointmentDetails.appointmentSunsetExpiryDateTime));
            appointmentDetails.appointmentSunsetExpiryDateTime = this.formatDate(sunsetExpiryDate);
        }
        if(!$A.util.isUndefined(appointmentDetails.firstAvailableStartDateTime) && !$A.util.isEmpty(appointmentDetails.firstAvailableStartDateTime))
        {
            var firstavailablestartdatetime = cisRequestEnabled ? new Date(appointmentDetails.firstAvailableStartDateTime) : new Date(Number(appointmentDetails.firstAvailableStartDateTime));
            appointmentDetails.firstAvailableStartDateTime = this.formatDate(firstavailablestartdatetime);
        }
        else
        {
            appointmentDetails.firstAvailableStartDateTime = '';
        }
        if(!$A.util.isUndefined(appointmentDetails.firstAvailableEndDateTime) && !$A.util.isEmpty(appointmentDetails.firstAvailableEndDateTime))
        {
            var firstavailableenddateTime = cisRequestEnabled ? new Date(appointmentDetails.firstAvailableEndDateTime) :new Date(Number(appointmentDetails.firstAvailableEndDateTime));
            appointmentDetails.firstAvailableEndDateTime = this.formatDate(firstavailableenddateTime);
        }
        else
        {
            appointmentDetails.firstAvailableEndDateTime = '';
        }
        //Construct Final Formatted Text to be shown on Table
        if(((appointmentDetails.firstAvailableStartDateTime) && !$A.util.isEmpty(appointmentDetails.firstAvailableStartDateTime)) && (!$A.util.isUndefined(appointmentDetails.firstAvailableEndDateTime) && !$A.util.isEmpty(appointmentDetails.firstAvailableEndDateTime)))
        {
            var firstAvailableStartDate = appointmentDetails.firstAvailableStartDateTime.split(" at ");
            var firstAvailableEndDate = appointmentDetails.firstAvailableEndDateTime.split(" at ");
            if(firstAvailableStartDate[0] === firstAvailableEndDate[0])
            {
                appointmentDetails.FirstAvailableAppointmentFormattedText =  appointmentDetails.firstAvailableStartDateTime + "- " + firstAvailableEndDate[1];
            }
            else
            {
                appointmentDetails.FirstAvailableAppointmentFormattedText = appointmentDetails.firstAvailableStartDateTime + "- " + appointmentDetails.firstAvailableEndDateTime;
            }   
        }
        appointmentDetails.slaMet = appointmentDetails.slaMet ? (appointmentDetails.slaMet.toUpperCase() == 'TRUE' ? 'Yes' : 'No') : component.get("v.defaultMissingString");
        //Check Transaction Status of the request if completed show the details else show error message
        if(appointmentDetails.hasOwnProperty('transactionStatus') && appointmentDetails.transactionStatus && appointmentDetails.transactionStatus ===  "Completed")
        {
            component.set("v.appointmentDetails", appointmentDetails);
            component.set("v.showAppointmentMoreInfoTable", true);
            component.set("v.showAppointmentMoreInfoPending", false);
        }
        else if(appointmentDetails.hasOwnProperty('transactionStatus') && appointmentDetails.transactionStatus && appointmentDetails.transactionStatus ===  "Requested")
        {
            component.set("v.showAppointmentMoreInfoTable", false);
            component.set("v.showAppointmentMoreInfoPending", true);
            component.set("v.appointmentMoreInfoTransactionMessage", "Apponitment more info request is still under processing with maximo, kindly retry after some time");
        }
        else if(component.get('v.CISAppointMentRequestEnabled') == 'true' && appointmentDetails){
            appointmentDetails.metadata = {'appointmentId' : appointmentDetails.appointmentId };
            component.set("v.appointmentDetails", appointmentDetails);
            component.set("v.showAppointmentMoreInfoTable", true);
            component.set("v.showAppointmentMoreInfoPending", false);
        }
    },
    formatDate : function(currentWorkingDate) {
        var locale = 'en-AU';
        var options = { hour: 'numeric', minute: 'numeric',timeZone: 'Australia/Sydney',timeZoneName: 'short'};
        var formattedtime = (new Intl.DateTimeFormat('en-AU', options).format(currentWorkingDate)).replace('AEST','').replace('AEDT','');
        var formmatedDate = (currentWorkingDate.toLocaleString(locale, {weekday : "short"})).replace(".","") + " " + (currentWorkingDate.toLocaleString(locale, {month : "short"})).replace(".","") + " " + currentWorkingDate.toLocaleString(locale, {day : "numeric"}) + "," + " "  + currentWorkingDate.toLocaleString(locale, {year : "numeric"}) + " " + "at" + " " + formattedtime;
        return formmatedDate;
    },
    // Nishank
    fetchWorkOrder: function(component, event, helper) {
        var action = component.get("c.fetchRecords");
        action.setParams({
            callingComponent: 'WORKORDER',
            filter: component.get('v.recordId'),
            dayCount: component.get('v.Dayscount'),
            locId: component.get('v.locId')
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === 'SUCCESS') {
                
                
               
                    
                var lstData = response.getReturnValue().lstSobjectData;
                var myCols = response.getReturnValue().lstColumns;
                
                myCols[0].type =  'button';
                myCols[0].fieldName =  'SFDCId';
                myCols[0].typeAttributes = { label: { fieldName: 'incident' }, name:'open_incident_WO', title: 'Click here to open in a new tab', disabled: {fieldName: 'disabled_fieldName'}, class: {fieldName: 'class_fieldName'} };
                
                myCols[4].type =  'button';
                myCols[4].fieldName =  'workOrder';
                myCols[4].typeAttributes = { label: { fieldName: 'workOrder' }, name:'open_work_order_detail', title: 'Click for more info', disabled: {fieldName: 'disabled_WO'}, class: {fieldName: 'class_fieldName'} };
                
                
                var currentRecordId = component.get('v.recordId');
                lstData = lstData.map(function(rowData) {
                    switch (rowData.SFDCId) {
                        case ('Not Available'):
                            //alert('na' + rowData.SFDCId);
                            rowData.SFDCId = '';
                            rowData.disabled_fieldName = true;
                            rowData.class_fieldName = 'ordModify';
                            break;
                        default:
                            //alert('d' + rowData.SFDCId);
                            rowData.disabled_fieldName = false;
                            rowData.class_fieldName = 'ordDisconnect';
                            break;
                    }
                    switch (rowData.workOrder) {
                        default:
                            rowData.class_work_order = 'class_work_order';
                            break;
                    }
                    return rowData;
                });
                
                console.log(myCols);
                console.log(lstData);
                
                component.set("v.mycolumns", myCols);
                component.set("v.lstRecords", lstData);
                component.set("v.totalSize", lstData.length);
                
                var locId = response.getReturnValue().locId;
                component.set("v.tableHeader",component.get('v.tableHeader').replace('LOC',locId));
                
                //set datatable
                helper.setDataTable(component ,event, helper);
               
                
            } else if (state === 'ERROR') {
                var errors = response.getError();
                console.log("Error message: " + errors[0].message);
                component.set("v.summaryTableError", errors[0].message);
                component.set("v.summaryError",true);
            }
        });
        $A.enqueueAction(action);
    },
    
    open_work_order_detail : function(component, row) {
        var data = component.get('v.PaginationList');
        
        console.log(row);
        console.log(data);
        
        data = data.map(function(rowData) {
            if (rowData.workOrder === row.workOrder) {
                //rowData.disabled_WO = false;
                var appEvent = $A.get("e.c:JIGSAW_ShowHideDataSummaryEvent");
                appEvent.setParam("moreInfoId", row.workOrder);
                appEvent.setParam("showMoreInfoTable", true);
                appEvent.setParam("callingMoreInfoComponent", 'WORKORDER');
                appEvent.setParam("moreInfoCardHeader", 'Work Order '+rowData.workOrder+': More Info');
                appEvent.setParam("recordId",component.get('v.recordId'));
                appEvent.setParam("Dayscount",component.get('v.Dayscount'));
                appEvent.setParam("workRequestNumber", row.workRequest);
                appEvent.fire();    
            }else{
                //rowData.disabled_WO = false;
            }
            return rowData;
        });
        component.set("v.PaginationList", data);
    },
    
    
    open_incident_WO : function(component, row) {
        this.openIncidentHelper(component,row.SFDCId);
    },
    
    openIncidentHelper : function(component, recId){
        var workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then((res =>{
            if(res){
            	workspaceAPI.openTab({
                    pageReference: {
                        "type": "standard__recordPage",
                        "attributes": {
                            "recordId":recId,
                            "actionName":"view"
                        },
                        "state": {}
                    },
                    focus: true
                }).then(function(response) {
                    workspaceAPI.getTabInfo({
                        tabId: response
                    }).then(function(tabInfo) {
                        console.log("The recordId for this tab is: " + tabInfo.recordId);
                    });
                });
        	}
            else
			{
               var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                  "recordId": recId
                });
                navEvt.fire();                                 
             }
        }))
        .catch(function(error) {
            console.log(error);
        });
    },
    open_incident : function(component, row) {    
        this.openIncidentHelper(component,row.Id);
    },
    open_product_order_view_more : function(component, row){
        var data = component.get('v.PaginationList');
        data = data.map(function(rowData) {
            if (rowData.Orderid === row.Orderid) {
                var appEvent = $A.get("e.c:JIGSAW_ShowHideDataSummaryEvent");
                appEvent.setParam("moreInfoId", row.Orderid);
                appEvent.setParam("showMoreInfoTable", true);
                appEvent.setParam("callingMoreInfoComponent", 'SERVICE');
                appEvent.setParam("moreInfoCardHeader", 'Service Change '+rowData.Orderid+': More Info');
                appEvent.setParam("deserializedObject", component.get('v.deserializedObject'));
                appEvent.setParam("recordId",component.get('v.recordId'));
                appEvent.fire(); 
            }
            return rowData;
        });
    }
})