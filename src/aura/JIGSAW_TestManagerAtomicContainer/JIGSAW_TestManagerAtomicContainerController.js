({
	createTestDetails : function(component, event, helper) {
	    /*Condition to determine wther atomic details are rendered in guided workflow or as stand alone atomic wf*/
	    var inGuidedWF = component.get("v.inGuidedWF");
	    var toggleAccordianCollapsed = component.get("v.toggleAccordianCollapsed");
	    var displayAtomicTestDetailSection = component.get("v.displayAtomicTestDetailSection");
	    
	    if(inGuidedWF){
	        if(toggleAccordianCollapsed){
	            displayAtomicTestDetailSection = false;
	        }else{
	            displayAtomicTestDetailSection = true
	        }
	    }else{
	        displayAtomicTestDetailSection = true;
	    }
	    component.set("v.displayAtomicTestDetailSection",displayAtomicTestDetailSection);
	    /*End of logic for toggle detail section*/
		helper.processAtomicTestDetails(component, event, helper);
	},
	selectAtomicTest : function(component, event, helper){
	    component.set("v.toggleAccordianCollapsed",!component.get("v.toggleAccordianCollapsed"));
	    
	    /*Condition to determine wther atomic details are rendered in guided workflow or as stand alone atomic wf*/
	    var inGuidedWF = component.get("v.inGuidedWF");
	    var toggleAccordianCollapsed = component.get("v.toggleAccordianCollapsed");
	    var displayAtomicTestDetailSection = component.get("v.displayAtomicTestDetailSection");
	    
	    if(inGuidedWF){
	        if(toggleAccordianCollapsed){
	            displayAtomicTestDetailSection = false;
	        }else{
	            displayAtomicTestDetailSection = true
	        }
	    }else{
	        displayAtomicTestDetailSection = true;
	    }
	    
	    component.set("v.displayAtomicTestDetailSection",displayAtomicTestDetailSection);
	    /*End of logic for toggle detail section*/
	    
	}
})