({
    processAtomicTestDetails: function(component, event, helper) {
        
        var responseObject = {};
        var isInGuidedWF = component.get("v.inGuidedWF");
        if (isInGuidedWF != undefined && isInGuidedWF) {
            responseObject = component.get("v.testDetailsResponseObjectArray")[component.get("v.atomicIndex")];
        } else {
            responseObject = component.get("v.testDetailsResponseObject"); // This varibale holds the JSON payload and custom setting list based on test type
        }
        
        if (responseObject != undefined) {
            var testDetailJSON = JSON.parse(responseObject.jsonResponse); // Test Details JSON responseObject
            var lstCustSet = responseObject.lstCustSet; //custom setting list queried based on selected test type ordered by Table_Row_Number__c field ASC.
            
            if (lstCustSet != undefined) {
                //in case of individial atomic test rendering
                helper.atomicProcessor(component, event, helper, testDetailJSON, lstCustSet);
            } else {
                var testType = helper.getValues(testDetailJSON, 'type', '', '')[0];
                
                if (testType != undefined) {
                    //in case of guided wf, we need to get custom setting for individual test
                    var action = component.get("c.getAtomicCSList");
                    
                    //set params for getTestDetailsAsWRI method
                    // This method makes the second call out to get test details based on wriid
                    action.setParams({
                        testType: testType
                    });
                    
                    action.setCallback(this, function(res) {
                        if (res.getState() == 'SUCCESS') {
                            var lstCustSet = res.getReturnValue();
                            if (lstCustSet == undefined) {
                                lstCustSet = [];
                            }
                            
                            responseObject = component.get("v.testDetailsResponseObjectArray")[component.get("v.atomicIndex")];
                            var testDetailJSON = JSON.parse(responseObject.jsonResponse);
                            
                            helper.atomicProcessor(component, event, helper, testDetailJSON, lstCustSet);
                            
                        } else {
                            var errors = res.getError();
                            if (errors) {
                                if (errors[0] && errors[0].message) {
                                    console.log("Error message: " +
                                                errors[0].message);
                                }
                            } else {
                                console.log("Unknown error");
                            }
                        }
                    });
                    
                    $A.enqueueAction(action);
                }
            }
        }
    },
    
    atomicProcessor: function(component, event, helper, testDetailJSON, lstCustSet) {
        
        var testEndDate = $A.localizationService.formatDateTime(parseInt(testDetailJSON.executionTimestamp), 'DD/MM/YYYY HH:mm');
        
        //get exceptions/summary for other channels in the test detail resoponse
        var exceptionDisplayValue = [];
        
        //get exceptions block from the JSON
        //var exp = helper.getValues(testDetailJSON, 'exceptions', '');
        var exp = helper.getValues(testDetailJSON, 'exceptions', 'diagnosticDetails', 'diagnosticDetails');
        for (var v in exp) {
            for (var i in exp[v]) {
                if (exp[v][i].code != undefined && exp[v][i].message != undefined) {
                    exceptionDisplayValue.push("Code " + exp[v][i].code + " - " + exp[v][i].message);
                }
            }
        }
        
        
        var genericTestDetails = {};
        
        //genericTestDetails["testStartDateTime"] = testStartDate;
        genericTestDetails["testEndDateTime"] = testEndDate;
        genericTestDetails["result"] = helper.getValues(testDetailJSON, 'result', 'diagnosticDetails', 'diagnosticDetails')[0];
        genericTestDetails["channel"] = testDetailJSON.channel;
        genericTestDetails["summaryForChannel"] = exceptionDisplayValue;
        
        //set the generic test details
        component.set('v.genericTestDetails', genericTestDetails);
        
        var testType = helper.getValues(testDetailJSON, 'type', 'diagnosticDetails', 'diagnosticDetails')[0];
        var testResultValue = helper.getValues(testDetailJSON, 'status', 'diagnosticDetails', 'diagnosticDetails')[0];
        //loop over the custom setting list 
        //create list of table data (Label and value)
        var testDetailsTableList = [];
        var headerArray = [];
        var isLQDInProgress = false;
        if (testType == "Single End Line Test" || testType == "SELT") {
            //check if JSON response is for SELT
            var returnObject = helper.processSeltTest(component, event, helper, lstCustSet, testDetailJSON);
            testDetailsTableList = returnObject["testDetailsTableList"];
            headerArray = returnObject["headerArray"];
        } else {
            //For in progress LQD in any guided workflow limit the detail view table record to only show monitoring period (CUSTSA-28519)
            if(testType == "LQD Test" && testResultValue == "In Progress"){
                isLQDInProgress = true;
                lstCustSet = lstCustSet.filter(function(a){
                    return (a.JSON_Key__c == 'monitoringPeriod' && a.Test_Type__c == "LQD Test" && a.Is_Visible_in_Jigsaw__c == true);
                });
                
                for(var i in lstCustSet){
                    lstCustSet[i].Table_Heading__c = 'Test Details';
                }
            }
            
            for (var i in lstCustSet) {
                var tempObj = {};
                if (lstCustSet[i].JSON_Key__c != undefined) {
                    if (lstCustSet[i].Is_Visible_in_Jigsaw__c) {
                        //get value of the JSON_Key__c from response JSON using helper.getValues
                        // var value = helper.getValues(testDetailJSON, lstCustSet[i].JSON_Key__c, ''); // value returned is always in form of array
                        var value = helper.getValues(testDetailJSON, lstCustSet[i].JSON_Key__c, lstCustSet[i].Parent_JSON_Key__c, lstCustSet[i].Parent_JSON_Key__c);
                        // check if array is not undefined and contains only one value, either string or object type
                        if (value != undefined && value.length == 1) {
                            //if string directly add that to the display table list
                            if (typeof value[0] == 'string' || typeof value[0] == 'number') {
                                if (value[0] != undefined && value[0] != '' && value[0] != 'null' && value[0] != 'Null' && value[0] != 'NULL') {
                                    var label = lstCustSet[i].SF_Label_Name__c;
                                    var displayValue = '';
                                    var header = lstCustSet[i].Table_Heading__c;
                                    //Add unit type to the value using Concatenate_With__c JSON Key
                                    if (lstCustSet[i].Concatenate_With__c != undefined) {
                                        //displayValue = value[0] + " " + helper.getValues(testDetailJSON, lstCustSet[i].Concatenate_With__c, '');
                                        var concValue = helper.getValues(testDetailJSON, lstCustSet[i].Concatenate_With__c, lstCustSet[i].Parent_JSON_Key__c, lstCustSet[i].Parent_JSON_Key__c)[0];
                                        if (concValue != undefined) {
                                            displayValue = value[0] + " " + concValue;
                                        } else {
                                            displayValue = value[0];
                                        }
                                        
                                    } else {
                                        displayValue = value[0];
                                    }
                                    
                                    //check of JSON key is date time value. If yes convert it into local time zone format
                                    if (lstCustSet[i].Is_DateTime__c) {
                                        displayValue = $A.localizationService.formatDateTime(Date.parse(displayValue), 'DD/MM/YYYY HH:mm');
                                    }
                                    
                                    //check if the JSON key is Graph
                                    if (lstCustSet[i].Is_Graph__c) {
                                        tempObj["isGraph"] = true;
                                    } else {
                                        tempObj["isGraph"] = false;
                                    }
                                    
                                    //set data in tempObj
                                    tempObj["label"] = label;
                                    tempObj["value"] = displayValue;
                                    tempObj["header"] = header;
                                    
                                    //push the temp value to main display table list
                                    testDetailsTableList.push(tempObj);
                                    headerArray.push(header);
                                }
                            } else if (typeof value[0] == 'object') {
                                // call utility method to parse the Json and get the list of 
                                // required label and values
                                // once we get the list after processing, 
                                // concat that list 
                                if (lstCustSet[i].Is_Array__c) {
                                    var filteredCust = lstCustSet.filter(function(data) {
                                        return data.Parent_JSON_Key__c == lstCustSet[i].JSON_Key__c;
                                    });
                                    var porecessedArrayObj = helper.processArrayUtility(value[0], lstCustSet[i].SF_Label_Name__c, filteredCust, lstCustSet[i].JSON_Key__c, lstCustSet[i]);
                                    testDetailsTableList = testDetailsTableList.concat(porecessedArrayObj["returnList"]);
                                    headerArray = headerArray.concat(porecessedArrayObj["headerArray"]);
                                }
                            }
                        }
                    }
                }
                
            }
        }
        
        //Graph Logic
        var graphUrls = [];
        if (helper.getValues(testDetailJSON, 'imageUrls', 'diagnosticDetails', 'diagnosticDetails')[0] != undefined) {
            graphUrls = graphUrls.concat(helper.getValues(testDetailJSON, 'imageUrls', 'diagnosticDetails', 'diagnosticDetails')[0]);
        }
        
        if (helper.getValues(testDetailJSON, 'classificationHistoryImageUrls', 'diagnosticDetails', 'diagnosticDetails')[0] != undefined) {
            graphUrls = graphUrls.concat(helper.getValues(testDetailJSON, 'classificationHistoryImageUrls', 'diagnosticDetails', 'diagnosticDetails')[0]);
        }
        
        
        
        if (graphUrls != undefined && graphUrls.length > 0) {
            //CUSTSA-28521 Updated by RM - Added WorflowreferenceID while creating graphs so that they can be cached
            //as an attachments under that particular workflowrefrenceId.
            var wriId = component.get('v.selectedTest').worfklowReferenceId != undefined ? component.get('v.selectedTest').worfklowReferenceId : null;
            var parentWriId = component.get("v.inGuidedWF") ? component.get("v.parentWorkflowRefId") : null;
            var graphResponse = helper.proccessGraphs(component, event, helper, graphUrls, lstCustSet, wriId, parentWriId); //CUSTSA-28521 Updated by RM
            
            testDetailsTableList = testDetailsTableList.concat(graphResponse["testDetailsTableList"]);
            headerArray = headerArray.concat(graphResponse["headerArray"]);
        }
        
        
        //bind the final list of header values
        headerArray = headerArray.filter(function(value, index, self) {
            return self.indexOf(value) === index;
        });
        
        
        component.set("v.headerArray", headerArray);
        //custom calculation for LQD in progeress to conver monitoring period from secs to mins
        //and calculate the estimated time remaing
        if(isLQDInProgress){
            var monitoringPerdiodTempObj = testDetailsTableList.find(function(a){
                return a.label == "Monitorting Period";
            });
            if(monitoringPerdiodTempObj != undefined){
                var monitoringMins = Math.floor(parseFloat(monitoringPerdiodTempObj.value)/60);
                monitoringPerdiodTempObj.value = monitoringMins + ' minutes'
                
                var EstimatedDateTime = new Date(testDetailJSON.diagnosticDetails.executionTimestamp);
                EstimatedDateTime.setMinutes(EstimatedDateTime.getMinutes() + monitoringMins);
                
                //Push EstimatedDateTime to testDetailsTableList as first element
                var tempObj = {};
                tempObj["label"] = "Estimated Completion Time";
                tempObj["value"] = EstimatedDateTime;
                tempObj["header"] = 'Test Details';
                tempObj["isDateFormat"] = true;
                testDetailsTableList.unshift(tempObj);
            }    
        }
        
        debugger;
        
        // bind the final list to the display table attribute to Iterate over and create table
        component.set("v.testDetailArrayData", testDetailsTableList);
        
    },
    proccessGraphs: function(component, event, helper, graphUrls, lstCustSet, wriId, parentWriId) {
        //CUSTSA-28521 Updated by RM - Added WorflowreferenceID while creating graphs so that they can be cached
        //as an attachments under that particular workflowrefrenceId.
        var returnObject = {};
        var testDetailsTableList = [];
        var headerArray = [];
        
        for (var index in graphUrls) {
            var graphCalloutUrl;
            var graphName;
            var tempObj = {};
            //calloutUrl
            if (graphUrls[index].split('/gtas-api/').length > 0 && graphUrls[index].split('/gtas-api/')[1] != undefined) {
                var graphCalloutUrl = '/gtas-api/' + graphUrls[index].split('/gtas-api/')[1];
            }
            
            //graph name
            if (graphUrls[index].split('_img_').length > 0 && graphUrls[index].split('_img_')[graphUrls[index].split('_img_').length - 1] != undefined) {
                var graphName = graphUrls[index].split('_img_')[graphUrls[index].split('_img_').length - 1];
            }
            
            if (graphName != undefined && graphName != '') {
                var filteredCustomSetting = lstCustSet.filter(function(data) {
                    return data.JSON_Key__c == graphName;
                })
                
                if (filteredCustomSetting != undefined && filteredCustomSetting.length > 0) {
                    //logic here
                    tempObj["label"] = filteredCustomSetting[0].SF_Label_Name__c;
                    tempObj["value"] = graphCalloutUrl;
                    tempObj["isGraph"] = true;
                    tempObj["header"] = filteredCustomSetting[0].Table_Heading__c;
                    tempObj["sequence"] = filteredCustomSetting[0].Table_Row_Number__c;
                    tempObj["parentWriId"] = parentWriId;
                    tempObj["wriId"] = wriId; //CUSTSA-28521 Added by RM - this will be used to store the graph as cache under this wriID
                    tempObj["graphName"] = graphName //CUSTSA-28521 Added by RM - this graph name will be used as attachment name for graph cache
                    headerArray.push(filteredCustomSetting[0].Table_Heading__c);
                    testDetailsTableList.push(tempObj)
                }
            }
        }
        
        //sort the graph sequence based on config file
        testDetailsTableList = testDetailsTableList.sort(function(a,b){
            return a.sequence - b.sequence;
        })
        
        
        returnObject["testDetailsTableList"] = testDetailsTableList;
        returnObject["headerArray"] = headerArray;
        
        return returnObject;
        
    },
    processSeltTest: function(component, event, helper, lstCustSet, testDetailJSON) {
        var returnObject = {};
        var testDetailsTableList = [];
        var headerArray = [];
        for (var i in lstCustSet) {
            var tempObj = {};
            if (lstCustSet[i].JSON_Key__c != undefined) {
                if (lstCustSet[i].Is_Visible_in_Jigsaw__c) {
                    //get value of the JSON_Key__c from response JSON using helper.getValues
                    // var value = helper.getValues(testDetailJSON, lstCustSet[i].JSON_Key__c, ''); // value returned is always in form of array
                    var value = helper.getValues(testDetailJSON, lstCustSet[i].JSON_Key__c, lstCustSet[i].Parent_JSON_Key__c, lstCustSet[i].Parent_JSON_Key__c);
                    // check if array is not undefined and contains only one value, either string or object type
                    if (value != undefined && value.length == 1) {
                        if (Array.isArray(value[0])) {
                            var retObj = helper.processSeltArrays(component, event, helper, lstCustSet, value[0], lstCustSet[i].JSON_Key__c);
                            testDetailsTableList = testDetailsTableList.concat(retObj["testDetailsTableList"]);
                            headerArray = headerArray.concat(retObj["headerArray"]);
                        } else {
                            if (value[0] != undefined && value[0] != '' && value[0] != 'null' && value[0] != 'Null' && value[0] != 'NULL') {
                                var label = lstCustSet[i].SF_Label_Name__c;
                                var header = lstCustSet[i].Table_Heading__c;
                                var displayValue = '';
                                //Add unit type to the value using Concatenate_With__c JSON Key
                                if (lstCustSet[i].Concatenate_With__c != undefined) {
                                    //displayValue = value[0] + " " + helper.getValues(testDetailJSON, lstCustSet[i].Concatenate_With__c, '');
                                    displayValue = value[0] + " " + helper.getValues(testDetailJSON, lstCustSet[i].Concatenate_With__c, lstCustSet[i].Parent_JSON_Key__c, lstCustSet[i].Parent_JSON_Key__c)[0];
                                } else {
                                    displayValue = value[0];
                                }
                                
                                //check of JSON key is date time value. If yes convert it into local time zone format
                                if (lstCustSet[i].Is_DateTime__c) {
                                    displayValue = $A.localizationService.formatDateTime(Date.parse(displayValue), 'DD/MM/YYYY HH:mm');
                                }
                                
                                //check if the JSON key is Graph
                                if (lstCustSet[i].Is_Graph__c) {
                                    tempObj["isGraph"] = true;
                                } else {
                                    tempObj["isGraph"] = false;
                                }
                                
                                //set data in tempObj
                                tempObj["label"] = label;
                                tempObj["value"] = displayValue;
                                tempObj["header"] = header;
                                
                                //push the temp value to main display table list
                                testDetailsTableList.push(tempObj);
                                headerArray.push(header);
                            }
                        }
                    }
                }
            }
        }
        
        returnObject["testDetailsTableList"] = testDetailsTableList
        returnObject["headerArray"] = headerArray
        
        return returnObject;
    },
    
    processSeltArrays: function(component, event, helper, lstCustSet, arrayValues, jsonKey) {
        
        var returnObject = {};
        var testDetailsTableList = [];
        var headerArray = [];
        
        var filterChilds = lstCustSet.filter(function(data) {
            return data.Parent_JSON_Key__c == jsonKey
        });
        var FilterChildDisplay = lstCustSet.filter(function(data) {
            return (data.Parent_JSON_Key__c == jsonKey && data.Table_Row_Number__c != undefined)
        });
        if (arrayValues != undefined && arrayValues.length > 0) {
            for (var index in arrayValues) {
                for (var i in FilterChildDisplay) {
                    //console.log('#####################################################################');
                    //console.log('JSON Key : ' + FilterChildDisplay[i].JSON_Key__c);
                    //console.log('Value : ' + helper.getValues(arrayValues[index], FilterChildDisplay[i].JSON_Key__c, "", ""));
                    //console.log('Label : ' + FilterChildDisplay[i].SF_Label_Name__c.replace('"n"', parseInt(parseInt(index) + 1)));
                    var tempObj = {};
                    
                    var header = FilterChildDisplay[i].Table_Heading__c;
                    tempObj["header"] = header;
                    tempObj["label"] = FilterChildDisplay[i].SF_Label_Name__c.replace('"n"', parseInt(parseInt(index) + 1));
                    tempObj["value"] = helper.getValues(arrayValues[index], FilterChildDisplay[i].JSON_Key__c, "", "")[0];
                    
                    if (Array.isArray(tempObj["value"]) && tempObj["value"].length > 0 && (typeof tempObj["value"][0] == "string" || typeof tempObj["value"][0] == "number")) {
                        var val = '';
                        for (var r in tempObj["value"]) {
                            val = val + tempObj["value"][r] + ' ';
                        }
                        tempObj["value"] = val;
                    } else {
                        var retObj = helper.processSeltArrays(component, event, helper, lstCustSet, tempObj["value"], FilterChildDisplay[i].JSON_Key__c);
                        testDetailsTableList = testDetailsTableList.concat(retObj["testDetailsTableList"]);
                        headerArray = headerArray.concat(retObj["headerArray"]);
                    }
                    
                    if (tempObj["value"] != undefined && (typeof tempObj["value"] == "string" || tempObj["value"] == "number") && tempObj["value"] != '' && tempObj["value"] != 'null' && tempObj["value"] != 'Null' && tempObj["value"] != 'NULL') {
                        
                        if (FilterChildDisplay[i].Is_DateTime__c) {
                            tempObj["value"] = $A.localizationService.formatDateTime(Date.parse(tempObj["value"]), 'DD/MM/YYYY HH:mm');
                        }
                        
                        if (FilterChildDisplay[i].Concatenate_With__c != undefined && tempObj["value"] != "NOT_FOUND") {
                            tempObj["value"] = tempObj["value"] + ' ' + helper.getValues(arrayValues[index], FilterChildDisplay[i].Concatenate_With__c, '', '')[0];
                        }
                        
                        testDetailsTableList.push(tempObj);
                        headerArray.push(header);
                    }
                }
            }
        }
        
        returnObject["testDetailsTableList"] = testDetailsTableList
        returnObject["headerArray"] = headerArray
        
        return returnObject;
    },
    
    //this method returns an array of values that match on a certain key
    getValues: function(obj, key, parentJsonKey, cParent) {
        var helper = this;
        var objects = [];
        
        //if parent key is passed as parameter then do strict search else do normal search on complete json
        if (parentJsonKey != undefined && parentJsonKey != '' && cParent != undefined && cParent != '') {
            //strict search block where parent key of required value should match with parent key in custom setting
            for (var i in obj) {
                if (typeof obj[i] == 'object') {
                    if (i == key && parentJsonKey === cParent) {
                        objects.push(obj[i]);
                    } else {
                        objects = objects.concat(helper.getValues(obj[i], key, parentJsonKey, Array.isArray(obj) ? cParent : i));
                    }
                } else if (i == key && parentJsonKey === cParent) {
                    objects.push(obj[i]);
                }
            }
        } else {
            //normal search. This block travers on entire object and give all the possible values which matches the same key value passed in param.
            //this block will gives an array of all the values where key matches exactly anywhere in the JSON
            //not to be used if same key value exists in different objects under obj param.
            for (var i in obj) {
                if (!obj.hasOwnProperty(i)) continue;
                if (typeof obj[i] == 'object') {
                    if (i == key) {
                        objects.push(obj[i]);
                    } else {
                        objects = objects.concat(helper.getValues(obj[i], key));
                    }
                } else if (i == key) {
                    objects.push(obj[i]);
                }
            }
        }
        
        return objects;
    },
    
    processArrayUtility: function(obj, sfLabel, lstCustSet, parentJsonKey, currentCustSet) {
        var helper = this;
        var returnObject = {};
        var returnList = [];
        var headerArray = [];
        if (Array.isArray(obj) && obj.length > 0 && (typeof obj[0] == "string" || typeof obj[0] == "number")) {
            var tempObj = {};
            var header = currentCustSet.Table_Heading__c;
            tempObj["label"] = currentCustSet.SF_Label_Name__c;
            var arrayVal = '';
            for (var i in obj) {
                arrayVal = arrayVal + obj[i];
                if (i != (obj.length - 1)) {
                    arrayVal = arrayVal + ' , ';
                }
            }
            tempObj["value"] = arrayVal;
            tempObj["header"] = header;
            headerArray.push(header);
            returnList.push(tempObj);
        } else {
            for (var i in Object.keys(obj)) {
                if (i != undefined) {
                    var cs = lstCustSet.filter(function(data) {
                        return (data.JSON_Key__c == Object.keys(obj)[i] && data.Parent_JSON_Key__c == parentJsonKey)
                    });
                    if (cs != undefined && cs.length > 0) {
                        if (cs[0].Is_Array__c) {
                            for (var t in obj[Object.keys(obj)[i]]) {
                                
                                var sortingList = [];
                                for (var m in Object.keys(obj[Object.keys(obj)[i]][t])) {
                                    var childCS = lstCustSet.filter(function(data) {
                                        return (data.JSON_Key__c == Object.keys(obj[Object.keys(obj)[i]][t])[m])
                                    });
                                    if (childCS.length != 0) {
                                        var tempObj = {};
                                        //var index = parseInt(t) + 1; -- Commented by RM for CUSTSA-25822
                                        var index = parseInt(t); // Added by RM for CUSTSA-25822 to display exact band number
                                        var label = sfLabel + " Per Band " + index + " " + childCS[0].SF_Label_Name__c
                                        var header = childCS[0].Table_Heading__c;
                                        //var value = helper.getValues(obj, Object.keys(obj[Object.keys(obj)[i]][t])[m], '')[t];
                                        var value = helper.getValues(obj, Object.keys(obj[Object.keys(obj)[i]][t])[m], cs[0].JSON_Key__c, cs[0].JSON_Key__c)[t];
                                        //CUSTSA-28523 Added an extra check for "127.0" and  "N/A" to be excluded from Attenuation and Loop Attenuation results
                                        if (value != undefined && value != '' && value != 'null' && value != 'Null' && value != 'NULL' && value.toUpperCase() != "N/A" && value != "127.0") {
                                            //value = value + " " + helper.getValues(obj, childCS[0].Concatenate_With__c, '')[0];
                                            value = value + " " + helper.getValues(obj, childCS[0].Concatenate_With__c, childCS[0].Parent_JSON_Key__c, childCS[0].Parent_JSON_Key__c)[0];
                                            tempObj["label"] = label;
                                            tempObj["value"] = value;
                                            tempObj["placement"] = childCS[0].Table_Row_Number__c;
                                            tempObj["header"] = header;
                                            //add temp obj to sorting list
                                            sortingList.push(tempObj);
                                            headerArray.push(header);
                                        }
                                    }
                                }
                                //sort the sorting list based on table row number
                                var tempSortingList = sortingList.sort(function(a, b) {
                                    return a["placement"] - b["placement"];
                                });
                                //add sorted list to returnList
                                for (var k in tempSortingList) {
                                    var tempSortObj = {};
                                    tempSortObj["label"] = tempSortingList[k]["label"];
                                    tempSortObj["value"] = tempSortingList[k]["value"];
                                    tempSortObj["header"] = tempSortingList[k]["header"];
                                    returnList.push(tempSortObj);
                                }
                            }
                        }
                    }
                }
            }
        }
        returnObject["returnList"] = returnList;
        returnObject["headerArray"] = headerArray;
        return returnObject;
    }
    
})