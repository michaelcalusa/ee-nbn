({
    displayRowLogic: function(component, event, helper) {

        var test = component.get("v.testDetail");
        var header = component.get("v.header");

        if (header == test.header) {
            component.set("v.displayRow", true);
        }

    },

    displayGraphs: function(component, event, helper) {
        component.set("v.disableGraphButton", true);
        component.set("v.loadingDetails", true);

        var test = component.get("v.testDetail");
        var action = component.get("c.getGraphsAsString");
        var graphName = test.graphName;
        var parentWriId = test.parentWriId;
        var wriId = test.wriId;
        action.setParams({
            "graphUrl": test.value,
            "graphName" : graphName,
            "wriId" : wriId,
            "parentWriId" : parentWriId
        });

        action.setCallback(this, function(res) {
            if (res.getState() == 'SUCCESS') {
                //debugger;
                var result = res.getReturnValue();
                
                if(result.errorOccured != undefined && result.errorOccured != true){
                    component.set("v.imgUrl", result.imageUrl);
                    component.set("v.loadingDetails", false);
                    component.set("v.displayGraph", true);
                    
                }else{
                    //display error
                    component.set("v.errorText",result.errorText);
                    component.set("v.errorOccured",true);
                }
                
            } else {
                var errors = res.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    toggleIsZoomed : function(component, event, helper){
        component.set("v.isImageZoomed", !component.get("v.isImageZoomed"));
    }
})