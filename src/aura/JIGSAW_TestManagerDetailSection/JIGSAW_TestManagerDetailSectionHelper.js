({
    getTestDetails: function(component, event, helperdetail) {
        //start the spinners until we get the response from the api
        component.set("v.loadingDetails", true);

        //get all the test detail values from selected test from summary section
        var selectedTest = component.get("v.selectedTest");
        console.log(selectedTest);
        var wri = selectedTest.worfklowReferenceId;
        var role = selectedTest.role;
        var channel = selectedTest.channel;
        var accessekerId = selectedTest.accessekerId;
        var testType = selectedTest.workflowType;
        var type = selectedTest.type != undefined ? selectedTest.type.trim() : '';
        var workflowName = selectedTest.workflowName;

        var action = component.get("c.getTestDetailsAsWRI");

        //set params for getTestDetailsAsWRI method
        // This method makes the second call out to get test details based on wriid
        action.setParams({
            wriId: wri,
            testType: testType,
            role: role,
            channel: channel,
            accessekerId: accessekerId,
            type: type,
            workflowName: workflowName
        });

        action.setCallback(this, function(results) {

            if (results.getState() == 'SUCCESS') {
                var returnResponse = results.getReturnValue();
                if (!returnResponse.errorOccured) {
                    //in case the status code is 200
                    //set the response object to an attirbute to be used by atomic/guided template components for 
                    //further processing.
                    
                    //check if the internal callout to TND from GTAS is failed
                    if (returnResponse.jsonResponse != undefined) {
                        var jsResponse = JSON.parse(returnResponse.jsonResponse);

                        if (JSON.parse(returnResponse.jsonResponse).exception != undefined && JSON.parse(returnResponse.jsonResponse).exception.code != undefined && JSON.parse(returnResponse.jsonResponse).exception.Message != undefined) {

                            var errorText = JSON.parse(returnResponse.jsonResponse).exception.code + ' ' + JSON.parse(returnResponse.jsonResponse).exception.Message;
                            //in case the status code is not 200 (Callout error scenario )
                            component.set("v.detailCalloutFailed", true);
                            component.set("v.detailCalloutErrorText", errorText);
                        } else {

                            component.set("v.testDetailsResponseObject", returnResponse);
                            component.set("v.detailCalloutFailed", false);

                            //save cache for test details starts here..
                            if (!returnResponse.isFromCache) {
                                var jsonResponse = JSON.parse(returnResponse.jsonResponse);
                                //This var is used to stop caching of test detail response if the any atomic test is in progress
                                var containsInProgressTest = false;
                                
                                if(jsonResponse.diagnosticDetails != undefined ){
                                    for(var index in jsonResponse.diagnosticDetails){
                                        if(jsonResponse.diagnosticDetails[index].status == 'In Progress'){
                                            containsInProgressTest = true;
                                        }
                                    }
                                }
                            
                                if (jsonResponse != undefined && jsonResponse.diagnosticDetails != undefined && jsonResponse.diagnosticDetails[0].type != undefined && !containsInProgressTest) {
                                    var testName = returnResponse.workflowName;
                                    var wriid = jsonResponse.testReferenceId;
                                    var testResult = component.get('v.selectedTest') != undefined ?component.get('v.selectedTest').result : '';
                                    var avc = component.get("v.AVCID");
                                    var status = jsonResponse.status;
                                    var testType = returnResponse.testType;
                                    var JSONPayload = returnResponse.jsonResponse
                                    var role = returnResponse.role;
                                    var channel = returnResponse.channel;
                                    var accessekerId = returnResponse.accessekerId;
                                    var executionTimestamp = jsonResponse.executionTimestamp != undefined ? jsonResponse.executionTimestamp.toString() : '';

                                    var action = component.get("c.saveTestDetailCache");

                                    action.setParams({
                                        wriid: wriid,
                                        testType: testType,
                                        testResult: testResult,
                                        testStatus: status,
                                        avcID: avc,
                                        testName: testName,
                                        JSONPayload: JSONPayload,
                                        role: role,
                                        channel: channel,
                                        accessekerId: accessekerId,
                                        executionTimestamp: executionTimestamp
                                    });

                                    action.setCallback(this, function(results) {
                                        if (results.getState() == 'SUCCESS') {
                                            if (results.getReturnValue() == false) {
                                                console.log('Unable to insert cache for test details.');
                                            }
                                        } else {
                                            console.log('Unable to insert cache for test details.');
                                        }
                                    });

                                    $A.enqueueAction(action);


                                }
                                //save cache for test details ends here..
                            }
                        }
                    }
                } else {
                    //in case the status code is not 200 (Callout error scenario )
                    component.set("v.detailCalloutFailed", true);
                    component.set("v.detailCalloutErrorText", returnResponse.errorText);
                }
                component.set("v.loadingDetails", false);
            } else {
                //error handleing code in case there is some apex/internal server error
                component.set("v.detailCalloutFailed", true);
                component.set("v.detailCalloutErrorText", 'Something went wrong. Please contact system admin for more information.');
                component.set("v.loadingDetails", false);
            }
        });
        $A.enqueueAction(action);

    }
})