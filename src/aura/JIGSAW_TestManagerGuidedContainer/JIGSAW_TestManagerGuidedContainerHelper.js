({
    processGuidedTestDetails: function(component, event, helper) {
        var responseObject = component.get("v.testDetailsResponseObject"); // This varibale holds the JSON payload and custom setting list based on test type
        if (responseObject != undefined) {
            var testDetailJSON = JSON.parse(responseObject.jsonResponse); // Test Details JSON responseObject
            var lstCustSet = responseObject.lstCustSet; //custom setting list queried based on selected test type ordered by Table_Row_Number__c field ASC.
            
            var testEndDate = $A.localizationService.formatDateTime(parseInt(testDetailJSON.executionTimestamp), 'DD/MM/YYYY HH:mm');
            
            
            
            var genericTestDetails = {};
            
            //genericTestDetails["testStartDateTime"] = testStartDate;
            genericTestDetails["testEndDateTime"] = testEndDate;
            
            if (testDetailJSON.workflowOutcome != undefined && testDetailJSON.workflowOutcome.status != undefined && testDetailJSON.workflowOutcome.status != '') {
                genericTestDetails["result"] = testDetailJSON.workflowOutcome.status;
            }else if(testDetailJSON.result != undefined && testDetailJSON.result != ''){
                genericTestDetails["result"] = testDetailJSON.result;
            }else{
                genericTestDetails["result"] = '';
            }
            
            genericTestDetails["channel"] = testDetailJSON.channel;
            genericTestDetails["workflowOutcome"] = helper.getValues(testDetailJSON, 'workflowOutcome', '', '')[0];
            
            var diagnosticDetailsArray = helper.getValues(testDetailJSON, 'diagnosticDetails', '', '')[0];
            
            //sort list of diagnostic details based on execution timestamp in decending order
            diagnosticDetailsArray = diagnosticDetailsArray.sort(function compareIndexFound(a, b) {
                if (a.executionTimestamp > b.executionTimestamp) {
                    return -1;
                }
                if (a.executionTimestamp < b.executionTimestamp) {
                    return 1;
                }
                return 0;
            });
            
            var selectedAtomicTestArray = [];
            var testDetailsResponseObjectArray = [];
            
            for (var index in diagnosticDetailsArray) {
                //create a list of selected test
                var tempObj = {};
                tempObj["worfklowReferenceId"] = diagnosticDetailsArray[index].testId;
                tempObj["role"] = component.get("v.selectedTest").role;
                tempObj["status"] = diagnosticDetailsArray[index].status;
                tempObj['timeStampWithTime'] = $A.localizationService.formatDateTime(parseInt(diagnosticDetailsArray[index].executionTimestamp), "DD/MM/YYYY HH:mm");
                tempObj["channel"] = component.get("v.selectedTest").channel;
                tempObj["workflowName"] = diagnosticDetailsArray[index].type;
                tempObj["result"] = diagnosticDetailsArray[index].result;
                tempObj["workflowType"] = diagnosticDetailsArray[index].type;
                tempObj["accessekerId"] = component.get("v.selectedTest").accessekerId;
                tempObj["resolutionTextArray"] = [];
                if (diagnosticDetailsArray[index].resolutions != undefined) {
                    for (var r in diagnosticDetailsArray[index].resolutions) {
                        tempObj["resolutionTextArray"].push(diagnosticDetailsArray[index].resolutions[r]); // Updated by RM CUSTSA-28504
                    }
                }
                
                selectedAtomicTestArray.push(tempObj);
                testDetailsResponseObjectArray.push({
                    "jsonResponse" : JSON.stringify({"diagnosticDetails": diagnosticDetailsArray[index]})
                });
            }
            
            //set the generic test details
            component.set("v.genericTestDetails", genericTestDetails);
            component.set("v.testDetailsResponseObjectArray", testDetailsResponseObjectArray);
            component.set("v.selectedAtomicTestArray", selectedAtomicTestArray);
            
        }
    },
    
    
    //this method returns an array of values that match on a certain key
    getValues: function(obj, key, parentJsonKey, cParent) {
        var helper = this;
        var objects = [];
        
        //if parent key is passed as parameter then do strict search else do normal search on complete json
        if (parentJsonKey != undefined && parentJsonKey != '' && cParent != undefined && cParent != '') {
            //strict search block where parent key of required value should match with parent key in custom setting
            for (var i in obj) {
                if (typeof obj[i] == 'object') {
                    if (i == key && parentJsonKey === cParent) {
                        objects.push(obj[i]);
                    } else {
                        objects = objects.concat(helper.getValues(obj[i], key, parentJsonKey, Array.isArray(obj) ? cParent : i));
                    }
                } else if (i == key && parentJsonKey === cParent) {
                    objects.push(obj[i]);
                }
            }
        } else {
            //normal search. This block travers on entire object and give all the possible values which matches the same key value passed in param.
            //this block will gives an array of all the values where key matches exactly anywhere in the JSON
            //not to be used if same key value exists in different objects under obj param.
            for (var i in obj) {
                if (!obj.hasOwnProperty(i)) continue;
                if (typeof obj[i] == 'object') {
                    if (i == key) {
                        objects.push(obj[i]);
                    } else {
                        objects = objects.concat(helper.getValues(obj[i], key));
                    }
                } else if (i == key) {
                    objects.push(obj[i]);
                }
            }
        }
        
        return objects;
    }
})