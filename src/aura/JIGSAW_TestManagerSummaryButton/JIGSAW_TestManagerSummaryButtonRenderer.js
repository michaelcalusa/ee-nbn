({
    afterRender:function(component,event,helper){
        this.superAfterRender();
        var testIterator = component.get('v.testIterator');
        var selectedTest = component.get('v.selectedTest');
        if (testIterator.worfklowReferenceId == selectedTest.worfklowReferenceId) {
            component.find('testButton').focus();
        }
    }
})