({
    loadTestSummaryDetails: function(component, event, helper) {
        helper.setTestSummaryList(component, event, helper);
    },
    filterTestResults: function(component, event, helper) {
        helper.filterTests(component, event, helper);
    }
})