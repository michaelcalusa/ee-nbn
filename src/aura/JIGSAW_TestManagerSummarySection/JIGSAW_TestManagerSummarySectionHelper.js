({
    setTestSummaryList: function(component, event, helper) {

        component.set("v.options", [{
            "label": "All Roles",
            "value": "All Roles"
        }, {
            "label": "nbn",
            "value": "nbn"
        }, {
            "label": "RSP",
            "value": "RSP"
        }, {
            "label": "Field tech",
            "value": "Field tech"
        }]);

        var result = component.get('v.testHistoryObject');
        var errorOccured = result.errorOccured;
        var AVC = result.AVC;
        if (AVC != undefined) {
            component.set("v.AVCID", AVC);
        }
        var responseSinceDate = result.responseSinceDate;
        if (responseSinceDate != undefined) {
            component.set("v.responseSinceDate", responseSinceDate);
        }
        
        if (!errorOccured) {

            var jsonResponse = JSON.parse(result.jsonResponse);

            //check if exception block is present
            //Code to handle if the AVC is wrong
            if (jsonResponse.exception != undefined) {
                component.set("v.initialCalloutFailed", true);
                if (jsonResponse.exception.code != undefined &&
                    jsonResponse.exception.Message != undefined) {
                    component.set("v.errorText", jsonResponse.exception.code + ' ' + jsonResponse.exception.Message);
                }
                //if no result found / Error occured during first callout
                component.set("v.noTestResultsFound", true);
            } else {
                var totalCount = jsonResponse != undefined ? jsonResponse.length : 0;
                component.set("v.totalCount", totalCount);

                var finalSummaryList = [];

                if (jsonResponse != undefined && totalCount > 0) {
                    for (var index in jsonResponse) {
                        var tempObj = {};
                        tempObj["worfklowReferenceId"] = jsonResponse[index].worfklowReferenceId;
                        tempObj["role"] = jsonResponse[index].role;
                        tempObj["status"] = jsonResponse[index].status;
                        tempObj["timeStamp"] = parseInt(jsonResponse[index].timeStamp);
                        tempObj["channel"] = jsonResponse[index].channel;
                        tempObj["workflowName"] = jsonResponse[index].workflowName;
                        tempObj["workflowType"] = jsonResponse[index].workflowType;

                        if (jsonResponse[index].workflowType == "Guided") {
                            if (jsonResponse[index].workflowOutcome != undefined && jsonResponse[index].workflowOutcome.status != undefined && jsonResponse[index].workflowOutcome.status != '') {
                                tempObj["result"] = jsonResponse[index].workflowOutcome.status;
                            } else if (jsonResponse[index].result != undefined && jsonResponse[index].result != '') {
                                tempObj["result"] = jsonResponse[index].result;
                            } else {
                                tempObj["result"] = '';
                            }
                        } else if (jsonResponse[index].workflowType == "Atomic" && jsonResponse[index].result != undefined && jsonResponse[index].result != '') {
                            tempObj["result"] = jsonResponse[index].result;
                        } else {
                            tempObj["result"] = '';
                        }

                        tempObj["accessekerId"] = jsonResponse[index].accessekerId;
                        tempObj["resolutionTextArray"] = [];

                        if (jsonResponse[index].testDetails != undefined && jsonResponse[index].testDetails[0].type != undefined) {
                            tempObj["type"] = jsonResponse[index].testDetails[0].type;
                        }
                        
                        //Updated by RM CUSTSA-28504                        
                        //Updated the logic to get resolution text from resolutions section in testDetails instead of resolutionsText key.
                        if (jsonResponse[index].workflowType == "Atomic") {
                            if (jsonResponse[index].testDetails && jsonResponse[index].testDetails[0].resolutions != undefined) {
                                tempObj["resolutionTextArray"] = jsonResponse[index].testDetails[0].resolutions;
                                for (var i in jsonResponse[index].testDetails[0].resolutions) {
                                    tempObj["resolutionText"] = tempObj["resolutionText"] != undefined ? tempObj["resolutionText"] + ' | ' + jsonResponse[index].testDetails[0].resolutions[i].message : jsonResponse[index].testDetails[0].resolutions[i].message;
                                }
                            } else {
                                if (jsonResponse[index].status == 'Cancelled') {
                                    tempObj["resolutionText"] = 'Cancelled';
                                }
                            }

                        } else if (jsonResponse[index].workflowType == "Guided") {
                            if (jsonResponse[index].workflowOutcome != undefined) {
                                tempObj["workflowOutcome"] = jsonResponse[index].workflowOutcome;
                                tempObj["resolutionText"] = jsonResponse[index].workflowOutcome.description;
                            } else {
                                if ((jsonResponse[index].status != undefined && jsonResponse[index].status == 'Completed') && (jsonResponse[index].result != undefined && jsonResponse[index].result == 'Passed')) {
                                    tempObj["resolutionText"] = 'Passed';
                                } else if ((jsonResponse[index].status != undefined && jsonResponse[index].status == 'Completed') && (jsonResponse[index].result != undefined && jsonResponse[index].result == 'Failed')) {
                                    tempObj["resolutionText"] = 'Failed';
                                } else if (jsonResponse[index].status != undefined && jsonResponse[index].status == 'Cancelled') {
                                    tempObj["resolutionText"] = 'Cancelled';
                                } else {
                                    tempObj["resolutionText"] = '';
                                }
                            }
                            tempObj["resolutionTextArray"].push(tempObj["resolutionText"]);
                        }

                        finalSummaryList.push(tempObj);
                    }
                    finalSummaryList = finalSummaryList.sort(function compareIndexFound(a, b) {
                        if (a.timeStamp > b.timeStamp) {
                            return -1;
                        }
                        if (a.timeStamp < b.timeStamp) {
                            return 1;
                        }
                        return 0;
                    });

                    for (var i in finalSummaryList) {
                        finalSummaryList[i]['timeStampWithTime'] = $A.localizationService.formatDateTime(parseInt(finalSummaryList[i].timeStamp), 'DD/MM/YYYY HH:mm');
                        finalSummaryList[i].timeStamp = $A.localizationService.formatDateTime(parseInt(finalSummaryList[i].timeStamp), "DD/MM/YYYY");
                    }


                    if (finalSummaryList.length >= 0) {
                        component.set("v.selectedTest", finalSummaryList[0]);
                        component.set("v.noTestResultsFound", false);
                        component.set("v.initialCalloutFailed", false);
                        component.set('v.filterOnn', false);
                        component.set("v.finalSummaryList", finalSummaryList);
                        component.set("v.displayList", finalSummaryList);
                        component.set("v.displayListLength", finalSummaryList.length);
                    }
                } else {
                    //if no result found
                    component.set("v.noTestResultsFound", true);
                    component.set("v.selectedTest", undefined);
                }
            }
        } else {
            component.set("v.initialCalloutFailed", true);
            component.set("v.errorText", result.errorText);
            //if no result found / Error occured during first callout
            component.set("v.noTestResultsFound", true);
        }
    },

    date_sort_desc: function(test1, test2) {
        // This is a comparison function that will result in dates being sorted in
        // DESCENDING order.
        if (test1.timeStamp > test2.timeStamp) return -1;
        if (test1.timeStamp < test2.timeStamp) return 1;
        return 0;
    },

    date_sort_asc: function(test1, test2) {
        // This is a comparison function that will result in dates being sorted in
        // ASCENDING order. As you can see, JavaScript's native comparison operators
        // can be used to compare dates. This was news to me.
        if (test1.timeStamp > test2.timeStamp) return 1;
        if (test1.timeStamp < test2.timeStamp) return -1;
        return 0;
    },

    filterTests: function(component, event, helper) {
        //This method is used to filter the summary responses based on user selected value for Role.
        //debugger;

        var completeTMList = component.get('v.finalSummaryList');
        var tempFilterArray = completeTMList.filter(function(tm) {
            if (component.get('v.selectedFilter') == 'All Roles') {
                return true;
            } else {
                return tm.role == component.get('v.selectedFilter');
            }
        });


        component.set('v.displayList', tempFilterArray);
        component.set('v.displayListLength', tempFilterArray.length);
        if (component.get('v.selectedFilter') == 'All Roles') {
            component.set('v.filterOnn', false);
        } else {
            component.set('v.filterOnn', true);
        }

    }
})