({
    updateColumnSorting: function(cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, event, helper, fieldName, sortDirection);
    },
    
    updateColumnSorting_TestTab: function(cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData_TestTab(cmp, event, helper, fieldName, sortDirection);
    },
    
    
    handleInit: function(component, event, helper) {
         helper.loadFirstTab(component, event, helper);
    },
    
    workOrderTab: function(component, event, helper) {
        helper.loadFirstTab(component, event, helper);
    },
    
    faultDetailsTab: function(component, event, helper) {
 		helper.load_FaultDetails(component, event, helper);
    },
    
    techNotesTab: function(component, event, helper) {
        helper.load_TechNotes(component, event, helper);
    },
    
    testsTab: function(component, event, helper) {
        helper.load_TestTab(component, event, helper);
    },
    
    next: function(component, event, helper) {
        var sObjectList = component.get("v.data_techNotes");
        var end = parseInt(component.get("v.endPage"));
        var start = parseInt(component.get("v.startPage"));
        var pageSize = parseInt(component.get("v.rowsPerPage"));
        var PagList = [];
        var counter = 0;
        for (var i = end + 1; i < end + pageSize + 1; i++) {
            if (sObjectList.length > i) {
                PagList.push(sObjectList[i]);
            }
            counter++;
        }
        start = start + counter;
        end = end + counter;
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.PaginationList', PagList);
    },
    previous: function(component, event, helper) {
        var sObjectList = component.get("v.data_techNotes");
        var end = parseInt(component.get("v.endPage"));
        var start = parseInt(component.get("v.startPage"));
        var pageSize = parseInt(component.get("v.rowsPerPage"));
        var PagList = [];
        var counter = 0;
        for (var i = start - pageSize; i < start; i++) {
            if (i > -1) {
                PagList.push(sObjectList[i]);
                counter++;
            } else {
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.PaginationList', PagList);
    },
    
    next_test: function(component, event, helper) {
        var sObjectList = component.get("v.data_tests");
        var end = parseInt(component.get("v.endPage_test"));
        var start = parseInt(component.get("v.startPage_test"));
        var pageSize = parseInt(component.get("v.rowsPerPage"));
        var PagList = [];
        var counter = 0;
        for (var i = end + 1; i < end + pageSize + 1; i++) {
            if (sObjectList.length > i) {
                PagList.push(sObjectList[i]);
            }
            counter++;
        }
        start = start + counter;
        end = end + counter;
        component.set("v.startPage_test", start);
        component.set("v.endPage_test", end);
        component.set('v.PaginationList_test', PagList);
    },
    previous_test: function(component, event, helper) {
        var sObjectList = component.get("v.data_tests");
        var end = parseInt(component.get("v.endPage_test"));
        var start = parseInt(component.get("v.startPage_test"));
        var pageSize = parseInt(component.get("v.rowsPerPage"));
        var PagList = [];
        var counter = 0;
        for (var i = start - pageSize; i < start; i++) {
            if (i > -1) {
                PagList.push(sObjectList[i]);
                counter++;
            } else {
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        component.set("v.startPage_test", start);
        component.set("v.endPage_test", end);
        component.set('v.PaginationList_test', PagList);
    },
    
    
    
    // this function automatic call when button in data table is clicked
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        //alert(action.name);
        switch (action.name) {
            case 'open_summary':
                helper.open_summary(cmp,row);
                break;
            default:
                //helper.showRowDetails(row);
                break;
        }
    }
 
})