({
    resetDataTable : function(component,event,helper){
        //component.set("v.confirmationModalVisible", false);
        component.set("v.sortedBy","");
        //component.set("v.sortedDirection","asc");
        component.set("v.mycolumns", []);
        component.set("v.data_techNotes", []);
        component.set("v.PaginationList", []);
        component.set("v.totalSize", 0);
        component.set("v.tableHeader","");
        component.set("v.startPage", 0);                
        component.set("v.endPage", 0);
    },
    
    sortData: function(component,event,helper, fieldName, sortDirection) {
        var data = component.get("v.data_techNotes");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.data_techNotes", data);
        
    },
    
    sortData_TestTab: function(component,event,helper, fieldName, sortDirection) {
        var data = component.get("v.data_tests");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.data_tests", data);
        
    },
    
    setDataTable : function (component, event, helper){
        
        var pageSize = parseInt(component.get("v.rowsPerPage"));
        var records = component.get("v.data_techNotes");
        component.set("v.startPage", 0);                
        component.set("v.endPage", pageSize - 1);
        var PagList = [];
        for ( var i=0; i< pageSize; i++ ) {
            if ( records.length > i )
                PagList.push(records[i]);    
        }
        component.set('v.PaginationList', PagList);
        
    },
    
    setDataTable_test : function (component, event, helper){
        
        var pageSize = parseInt(component.get("v.rowsPerPage"));
        var records = component.get("v.data_tests");
        component.set("v.startPage_test", 0);                
        component.set("v.endPage_test", pageSize - 1);
        var PagList = [];
        for ( var i=0; i< pageSize; i++ ) {
            if ( records.length > i )
                PagList.push(records[i]);    
        }
        component.set('v.PaginationList_test', PagList);
        
    },
    
    
    sortBy: function(field, reverse, primer) {
        var key = primer ?
            function(x) {
                return primer(x[field])
            } :
        function(x) {
            return x[field]
        };
        reverse = !reverse ? 1 : -1;
        return function(a, b) {
            return a = key(a)?key(a):'', b = key(b)?key(b):'', reverse * ((a > b) - (b > a));
        }
    },
    
    
    clearAll: function(component, event, helper) {
        // this method set all tabs to hide and inactive
        var getAllLI = document.getElementsByClassName("customClassForTab");
        var getAllDiv = document.getElementsByClassName("customClassForTabData");
        for (var i = 0; i < getAllLI.length; i++) {
            getAllLI[i].className = "slds-tabs--scoped__item slds-text-title--caps customClassForTab";
            getAllDiv[i].className = "slds-tabs--scoped__content slds-hide customClassForTabData";
        }
    },
    
    
    // First Tab
    loadFirstTab: function(component, event, helper) {
        console.log('first tab clicked');
        
        var tab1 = component.find('workOrderId');
        var TabOnedata = component.find('workOrderDataId');
        
        var tab2 = component.find('siteDetailsId');
        var TabTwoData = component.find('siteDetailsDataId');
        
        var tab3 = component.find('techNotesId');
        var TabThreeData = component.find('techNotesDataId');
        
        var tab4 = component.find('testsId');
        var TabFourData = component.find('testsDataId');
        
        // Hide and deactivate others tab
        $A.util.removeClass(tab2, 'slds-active');
        $A.util.removeClass(TabTwoData, 'slds-show');
        $A.util.addClass(TabTwoData, 'slds-hide');
        
        $A.util.removeClass(tab3, 'slds-active');
        $A.util.removeClass(TabThreeData, 'slds-show');
        $A.util.addClass(TabThreeData, 'slds-hide');
        
        $A.util.removeClass(tab4, 'slds-active');
        $A.util.removeClass(TabFourData, 'slds-show');
        $A.util.addClass(TabFourData, 'slds-hide');
        
        var moreInfoId = component.get('v.moreInfoId');
        var recordId = component.get('v.recordId');
        var workRequestNumber = component.get('v.workRequestNumber');
        
        console.log('moreInfoId' + component.get("v.moreInfoId"));
        console.log(component.get("v.recordId"));
        console.log('workRequestNumber' + component.get("v.workRequestNumber"));
        
        if($A.util.isUndefinedOrNull(component.get("v.objFirstTabResult"))){
        var action = component.get("c.fetch_FirstTab");
        action.setParams({
            workOrderNumber : component.get("v.moreInfoId"),
            incidentId : component.get("v.recordId"),
            dayCount : component.get("v.dayCount"),
            workRequestNumber : component.get("v.workRequestNumber")
            
        });
        
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                    console.log('result');
                    console.log(result);
                    //start of new code to deserialize work order details as a part of CUSTSA-28209
                    //initialize the variable that are displayed on the UI
                    result.techOffsite_additionalInfo = '';
                    result.nbnType = '';
                    result.nbnDetails = '';
                    result.faultOutsideNbnDeclarartion = '';
                    result.faultOutsideNbnDeclarartion_rootCause = '';
                    result.workingServiceTestPoint = '';
                    if(!$A.util.isUndefinedOrNull(result.workOrderDetailsJSON)){
                        var workOrderDetails = JSON.parse(result.workOrderDetailsJSON);
                        result.techOffsite_additionalInfo = (!$A.util.isUndefinedOrNull(workOrderDetails.remarkdesc_longdescription)) ? workOrderDetails.remarkdesc_longdescription : 
                                                                                                                                       '';
                        //get the asset attributes from work order spec
                        var workOrderSpec = workOrderDetails.workorderspec;
                        for(var i in workOrderSpec){
                            var tmpSpecVar = workOrderSpec[i];
                            switch(tmpSpecVar.assetattrid.toUpperCase()) {
                            case "CONFIRM NETWORK BOUNDARY POINT":
                              result.nbnType =  tmpSpecVar.gbsspecvalue;
                              break;
                            case "NETWORK BOUNDARY DETAILS":
                              result.nbnDetails = tmpSpecVar.gbsspecvalue;
                              break;
                            case "FAULT OUTSIDE NBN DEMARCATION":
                              result.faultOutsideNbnDeclarartion = tmpSpecVar.gbsspecvalue;
                              break;
                            case "FAULT OUTSIDE NBN DEMARCATION - ROOT CAUSE":
                              result.faultOutsideNbnDeclarartion_rootCause = tmpSpecVar.gbsspecvalue;
                              break;
                            case "WORKING SERVICE TEST POINT":
                              result.workingServiceTestPoint = tmpSpecVar.gbsspecvalue;
                              break;
                            default:
                              // execute none
                            }
                        }
                    }
                    component.set("v.objFirstTabResult", result);
                    //end of new code to deserialize work order details as a part of CUSTSA-28209
                    if(!$A.util.isEmpty(result.showFirstTab) && !$A.util.isUndefined(result.showFirstTab)){
                        component.set("v.showFirstTab", result.showFirstTab);
                    }
                    
                    if(!$A.util.isEmpty(result.showSecondTab) && !$A.util.isUndefined(result.showSecondTab)){
                        component.set("v.showSecondTab", result.showSecondTab); 
                    }
                    
                    if(!$A.util.isEmpty(result.showThirdTab) && !$A.util.isUndefined(result.showThirdTab)){
                        component.set("v.showThirdTab", result.showThirdTab); 
                    }
                    
                    if(!$A.util.isEmpty(result.showFourthTab) && !$A.util.isUndefined(result.showFourthTab)){
                        component.set("v.showFourthTab", result.showFourthTab); 
                    }
                    
                    component.set("v.errorCode", result.errorCode);
                    component.set("v.errorDesc", result.errorDesc);
                    
                    if(!$A.util.isEmpty(result.errorDesc) && !$A.util.isUndefined(result.errorDesc)){
                        component.set("v.showError", true);
                    }
                    
                    if($A.util.isUndefined(result.showFirstTab) && $A.util.isUndefined(result.showSecondTab) && $A.util.isUndefined(result.showThirdTab) && $A.util.isUndefined(result.showFourthTab)){
                        component.set("v.showError", true);
                    }else{
                        component.set("v.showError", false);
                    }
                    
                }
                else{
                    component.set("v.showError", true); 
                    component.set("v.errorDesc", 'Data Not available'); 
                }
            } else if(state == "ERROR"){
                component.set("v.showError", true); 
                component.set("v.errorDesc", a.getError()); 
            }
            
            if(component.get("v.showFirstTab") == true){
                //Show
                $A.util.addClass(tab1, 'slds-active');
                $A.util.addClass(TabOnedata, 'slds-show');
                $A.util.removeClass(TabOnedata, 'slds-hide');
            }else{
                // Hide
                $A.util.removeClass(tab1, 'slds-active');
                $A.util.removeClass(TabOnedata, 'slds-show');
                $A.util.addClass(TabOnedata, 'slds-hide');
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
        } else{
            var tmpResult = component.get("v.objFirstTabResult");
            if(!$A.util.isUndefinedOrNull(tmpResult.showFirstTab) && tmpResult.showFirstTab){
                $A.util.addClass(tab1, 'slds-active');
                $A.util.addClass(TabOnedata, 'slds-show');
                $A.util.removeClass(TabOnedata, 'slds-hide');
            }
        }
    },
    
    // Fault Details
    load_FaultDetails: function(component, event, helper) {
        console.log('Fault Details tab clicked');
        
        var tab1 = component.find('workOrderId');
        var TabOnedata = component.find('workOrderDataId');
        
        var tab2 = component.find('siteDetailsId');
        var TabTwoData = component.find('siteDetailsDataId');
        
        var tab3 = component.find('techNotesId');
        var TabThreeData = component.find('techNotesDataId');
        
        var tab4 = component.find('testsId');
        var TabFourData = component.find('testsDataId');
        
        //show and Active site details Tab
        $A.util.addClass(tab2, 'slds-active');
        $A.util.removeClass(TabTwoData, 'slds-hide');
        $A.util.addClass(TabTwoData, 'slds-show');
        
        // Hide and deactivate others tab
        $A.util.removeClass(tab1, 'slds-active');
        $A.util.removeClass(TabOnedata, 'slds-show');
        $A.util.addClass(TabOnedata, 'slds-hide');
        
        $A.util.removeClass(tab3, 'slds-active');
        $A.util.removeClass(TabThreeData, 'slds-show');
        $A.util.addClass(TabThreeData, 'slds-hide');
        
        $A.util.removeClass(tab4, 'slds-active');
        $A.util.removeClass(TabFourData, 'slds-show');
        $A.util.addClass(TabFourData, 'slds-hide');

        if($A.util.isUndefinedOrNull(component.get("v.data_faultDetails"))) {

        var action = component.get("c.fetch_FaultDetails");
        action.setParams({
            workOrderNumber : component.get("v.moreInfoId"),
            incidentId : component.get("v.recordId"),
            workRequestNumber : component.get("v.workRequestNumber")
        });
        
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                    component.set("v.data_faultDetails", result);
                    console.log(result);
                }
                else{
                    // Show error
                }
            }else if(state == "ERROR"){
                // show error
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
        }
    },
    
    
    // Test Tab
    load_TestTab: function(component, event, helper) {
        console.log('Test Tab clicked');
        
        var tab1 = component.find('workOrderId');
        var TabOnedata = component.find('workOrderDataId');
        
        var tab2 = component.find('siteDetailsId');
        var TabTwoData = component.find('siteDetailsDataId');
        
        var tab3 = component.find('techNotesId');
        var TabThreeData = component.find('techNotesDataId');
        
        var tab4 = component.find('testsId');
        var TabFourData = component.find('testsDataId');
        
        //show and Active tests Tab
        $A.util.addClass(tab4, 'slds-active');
        $A.util.removeClass(TabFourData, 'slds-hide');
        $A.util.addClass(TabFourData, 'slds-show');
        
        // Hide and deactivate others tab
        $A.util.removeClass(tab1, 'slds-active');
        $A.util.removeClass(TabOnedata, 'slds-show');
        $A.util.addClass(TabOnedata, 'slds-hide');
        
        $A.util.removeClass(tab2, 'slds-active');
        $A.util.removeClass(TabTwoData, 'slds-show');
        $A.util.addClass(TabTwoData, 'slds-hide');
        
        $A.util.removeClass(tab3, 'slds-active');
        $A.util.removeClass(TabThreeData, 'slds-show');
        $A.util.addClass(TabThreeData, 'slds-hide');
        
        if($A.util.isUndefinedOrNull(component.get("v.data_tests")) || 
           $A.util.isUndefinedOrNull(component.get("v.columns_tests")) || 
           $A.util.isUndefinedOrNull(component.get("v.totalRecordsTestTab")) || 
           $A.util.isUndefinedOrNull(component.get("v.totalSize_test"))
           ) {
        var action = component.get("c.fetch_TestTab");
        action.setParams({
            workOrderNumber : component.get("v.moreInfoId"),
            incidentId : component.get("v.recordId")
        });
        
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                    component.set("v.data_tests", result.lstSobjectData);
                    component.set("v.columns_tests", result.lstColumns);
                    component.set("v.totalRecordsTestTab", result.totalRecords);
                    component.set("v.totalSize_test", result.totalRecords);
                    console.log(result);
                    helper.setDataTable_test(component ,event, helper);
                }
                else{
                    // Show error
                }
            }else if(state == "ERROR"){
                // show error
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
        }
    },
    
    // Tech Notes
    load_TechNotes: function(component, event, helper) {
        console.log('tech notes tab clicked');
        
        var tab1 = component.find('workOrderId');
        var TabOnedata = component.find('workOrderDataId');
        
        var tab2 = component.find('siteDetailsId');
        var TabTwoData = component.find('siteDetailsDataId');
        
        var tab3 = component.find('techNotesId');
        var TabThreeData = component.find('techNotesDataId');
        
        var tab4 = component.find('testsId');
        var TabFourData = component.find('testsDataId');
        
        //show and Active tech notes Tab
        $A.util.addClass(tab3, 'slds-active');
        $A.util.removeClass(TabThreeData, 'slds-hide');
        $A.util.addClass(TabThreeData, 'slds-show');
        
        // Hide and deactivate others tab
        $A.util.removeClass(tab1, 'slds-active');
        $A.util.removeClass(TabOnedata, 'slds-show');
        $A.util.addClass(TabOnedata, 'slds-hide');
        
        $A.util.removeClass(tab2, 'slds-active');
        $A.util.removeClass(TabTwoData, 'slds-show');
        $A.util.addClass(TabTwoData, 'slds-hide');
        
        $A.util.removeClass(tab4, 'slds-active');
        $A.util.removeClass(TabFourData, 'slds-show');
        $A.util.addClass(TabFourData, 'slds-hide');
        
        if($A.util.isUndefinedOrNull(component.get("v.data_techNotes")) ||
           $A.util.isUndefinedOrNull(component.get("v.columns_techNotes")) ||
           $A.util.isUndefinedOrNull(component.get("v.totalRecordsTechNotes")) ||
           $A.util.isUndefinedOrNull(component.get("v.totalSize"))
           ) {
        var action = component.get("c.fetch_TechNotes");
        action.setParams({
            workOrderNumber : component.get("v.moreInfoId"),
            incidentId : component.get("v.recordId")
        });
        
        
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                    
                    // Update Columns for Summary Link
                    var mycolumns = result.lstColumns;
                    for (var singlekey in mycolumns) {
                        if(mycolumns[singlekey].fieldName == 'summary'){
                            mycolumns[singlekey].type = 'button';
                            mycolumns[singlekey].fieldName = 'summary';
                            mycolumns[singlekey].typeAttributes =  { label: { fieldName: 'summary' }, name:'open_summary', title: 'Click here to open the summary', disabled: {fieldName: 'disabled_fieldName'}, class: {fieldName: 'class_fieldName'} };
                        }
                    }
                    
                    // Update data
                    var myData = result.lstSobjectData;
                    for (var i = 0; i < myData.length; i++) {
                        myData[i].class_fieldName = 'summaryButton';
                    }
                    
                    component.set("v.data_techNotes", myData);
                    component.set("v.columns_techNotes", mycolumns);
                    component.set("v.totalRecordsTechNotes", result.totalRecords);
                    component.set("v.totalSize", result.totalRecords);
                    
                    
                    //set datatable
                    helper.sortData(component,event,helper,"createdate","desc");
                    helper.setDataTable(component ,event, helper);
                    
                    
                    console.log('Tech Notes Data >>>>> ');
                    console.log(result.totalRecords);
                    console.log(myData);
                    console.log(mycolumns);
                }
                else{
                    // Show error
                }
            }else if(state == "ERROR"){
                // show error
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
        }
    },
    
    open_summary : function(component, row) {
        component.set("v.summaryDetail", row.longSummary);
    }
    
})