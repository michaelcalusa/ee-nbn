({
    doInit : function(component, event, helper) {
        
        var myPageRef = component.get("v.pageReference");
        if(myPageRef){
            component.set("v.mostRecentEvent", myPageRef.state.c__mostRecentEvent);
            component.set("v.strCompTitle", myPageRef.state.c__strCompTitle);
            component.set("v.strVisibilityInt", myPageRef.state.c__strVisibilityInt);
            component.set("v.recordId", myPageRef.state.c__strRecordId);
            component.set("v.sObjectName", myPageRef.state.c__sObjectName);
            component.set("v.strDateSort", myPageRef.state.c__sortType);
            component.set("v.strTruncVal", myPageRef.state.c__truncVal);
            component.set("v.strPostType", myPageRef.state.c__postType);
            component.set("v.filterCondition", myPageRef.state.c__strFilterConditions);
            component.set("v.intBodyLength", myPageRef.state.c__intBodyLength);
            helper.getChatterPosts(component, event, helper);
        }
    },
	
})