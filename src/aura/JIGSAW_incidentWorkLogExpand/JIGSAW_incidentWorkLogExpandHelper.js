({
	getChatterPosts : function(component, event, helper) {        
        var action = component.get("c.getChatterPosts");
        var lengthOfBodyToShow = component.get("v.intBodyLength");
        console.log("Chatter Filters :- " + component.get("v.filterCondition"));
        action.setParams({
            strRecordId : component.get("v.recordId"),
            sObjectName : component.get("v.sObjectName"),
            sortType : component.get("v.strDateSort"),
            truncVal : component.get("v.strTruncVal"),
            postType : component.get("v.strPostType"),
            strFilterConditions : component.get("v.filterCondition") ? component.get("v.filterCondition") : ""
        });
        action.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();
            if(state == "SUCCESS") {
                var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                    component.set("v.fItem", result);
                }
                else {
                    component.set("v.fItem", result);
                    $A.get('e.force:refreshView').fire();
                }
            } 
            else if(state == "ERROR"){
            }
        });
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },
})