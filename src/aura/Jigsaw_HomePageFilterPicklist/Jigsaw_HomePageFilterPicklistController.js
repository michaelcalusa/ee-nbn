({
    doInit : function(component, event, helper) {
	},
    // method to set values under filter dropdown
    SetValues : function (component, event, helper) {
        helper.FillDropdown(component, event, helper);
    },
     // Method to open dropdown on mouseover.
	 handleMouseEnter: function(component, event, helper) {
        component.set("v.dropdownOver", true);
		component.set("v.isChangeSeletion", false);
    },
    // Call event 'selectChange' to get latest selected values under drpdown.
    handleMouseLeave: function(component, event, helper) {
    	component.set("v.dropdownOver", false);
    	var dv = component.find('filterdiv');
    	$A.util.removeClass(dv, 'slds-is-open');
        
        var compEvent = component.getEvent("selectChange");
        compEvent.setParams({ "values": component.get("v.selectedItems") });
		compEvent.setParams({ "isChangeValue": component.get("v.isChangeSeletion") });
        compEvent.fire();
    },
    // Method call on dropdown(button) click
    filterClick: function(component, event, helper) {
        var mainDiv = component.find('filterdiv');
        $A.util.addClass(mainDiv, 'slds-is-open');
    },
    // Method to close dropdown in some time when loose focus from dropdown.
	handleMouseOutButton: function(component, event, helper) {
     window.setTimeout(
      $A.getCallback(function() {
        if (component.isValid()) {
          //if dropdown over, user has hovered over the dropdown, so don't close.
          if (component.get("v.dropdownOver")) {
            return;
          }
          var mainDiv = component.find('filterdiv');
          $A.util.removeClass(mainDiv, 'slds-is-open');
        }
      }), 200
    );
  },
    // Get all the selected values from dropdown to mark them checked within dropdown
    handleSelection: function(component, event, helper) {
    var item = event.currentTarget;
    if (item && item.dataset) {
      var value = item.dataset.value;
      var selected = item.dataset.selected;
	  component.set("v.isChangeSeletion", true);
      var options = component.get("v.options");
	  options.forEach(function(element) {
            if(value == 'All'){
                element.selected = selected == "true" ? false : true;
            	if(element.label != value)
                    element.selected = false;
            }else{
                if(element.label == 'All')
                    element.selected = false;
                if (element.label == value) {
                    element.selected = selected == "true" ? false : true;
                } 
            }
        });
        //var mainDiv = component.find('filterdiv');
        //$A.util.removeClass(mainDiv, 'slds-is-open');
        
      component.set("v.options", options);
      var values = helper.getSelectedValues(component);
      var labels = helper.getSelectedLabels(component);

      helper.setInfoText(component, labels);
     // helper.despatchSelectChangeEvent(component, values);

    }
  }
})