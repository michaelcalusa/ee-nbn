({
    // Fill dropdown values with label and selected property.
    FillDropdown: function(component, event, helper) {
       var opt =[];
        var items = component.get("v.lstFilter");
        //('items are -' + items);
        var listType = component.get("v.headerName");
         if(items && items.length > 0){
             opt.push({label: 'All',selected:true});
             for(var i=0; i< items.length ;i++){
                 opt.push({label:items[i],selected:false});   
             }
             component.set("v.options",opt); 
             this.selectOptionHelper(component,items[0].label,'true');  
             // Make Queue list to disable if logged in user associated with only one group.
             if(listType && listType == 'Queue'){
                 if(items.length < 2){
					 component.set("v.infoText", items[0]);
                     component.set("v.isDisableList",true);
                 }
             }
         }
         else if(listType && listType == 'Queue'){
			component.set("v.isDisableList",true);
         }
     },
    // Set text on dropdown front with all selected values.
    setInfoText: function(component, values) {
        if (values.length == 0) {
            component.set("v.infoText", "Select an option...");
        }
        if (values.length == 1) {
            component.set("v.infoText", values[0]);
        }
        else if (values.length > 1) {
            component.set("v.infoText", values.length + " options selected");
        }
        if(values)
            component.set("v.selectedItems",values);
       /* if(values.length >0){
            component.set("v.selectedItems",values);
        } */
        
    },
	// Set selected values into component event to pass values to parent component.
    despatchSelectChangeEvent: function(component,values){
        var compEvent = component.getEvent("selectChange");
        compEvent.setParams({ "values": values });
        compEvent.fire();
    },
    // Toggle the value selection under dropdown.
    selectOptionHelper : function(component,label,isCheck) {
        var selectedOption='';
		var allOptions = component.get('v.options');
        var count=0;
        for(var i=0;i<allOptions.length;i++){
            
            if(allOptions[i].label == label) { 
                if(isCheck=='true'){
                    allOptions[i].selected = false; 
                }
                else{ 
                    allOptions[i].selected = true; 
                } 
            } 
            if(allOptions[i].selected){ 
                selectedOption=allOptions[i].label; 
                count++; 
            } 
        }
        if(count>1){
            selectedOption = count+' items selected';
        }
        component.set("v.selectedOptions",selectedOption);
        component.set('v.options',allOptions);
    },
    // Method to return all selected values from dropdown.
    getSelectedValues: function(component){
    var options = component.get("v.options");
    var values = [];
    options.forEach(function(element) {
      if (element.selected) {
        values.push(element.value);
      }
    });
    return values;
  },
 // Method to return all selected labels from dropdown.
  getSelectedLabels: function(component){
    var options = component.get("v.options");
    var labels = [];
    options.forEach(function(element) {
      if (element.selected) {
        
			labels.push(element.label);
      }
    });
    return labels;
  }
})