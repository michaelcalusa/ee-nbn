({
	getTechValue : function(component, event, helper) {
		
        //create technology Mapping
        helper.createTechnologyMap(component);
        
        let accessTech = component.get("v.accessTechnology");
        let fullName = component.get("v.technologyNameMap")[accessTech];
        
        //check if acronym exists in map
        if(!$A.util.isUndefined(fullName)){
            accessTech = fullName;
        }
        component.set("v.displayName", accessTech);
	}
})