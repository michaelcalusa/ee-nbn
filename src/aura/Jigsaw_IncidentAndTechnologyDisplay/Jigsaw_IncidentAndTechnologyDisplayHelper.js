({
	createTechnologyMap : function(component) {
		
        let accessTechLabel = $A.get("$Label.c.Jigsaw_TechnologyMapping");
        let arrayAccessTech = accessTechLabel.split(',')
        let mapTechByName = {};
        for(let i = 0 ; i < arrayAccessTech.length ; i++){
            let technologyVal = arrayAccessTech[i].split('=>');
            mapTechByName[technologyVal[0].trim()] = technologyVal[1].trim();
        }
        component.set("v.technologyNameMap", mapTechByName);
	}
})