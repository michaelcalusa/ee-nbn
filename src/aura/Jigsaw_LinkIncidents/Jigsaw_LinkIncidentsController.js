({
	handleNetworkObjectDetailsChange : function(component, event, helper) {
        var currentIncidentWrapper = component.get("v.attrCurrentIncidentRecord");
        var currentStatus = currentIncidentWrapper.Current_status;
        var isLoggedInUserIsAssignee = currentIncidentWrapper.isLoggedInUserEqualsAssignee;
        //var showLinkIncidents = helper.validateLinkedIncidents(component.get("v.NetworkIncidentsObjectDetails"));
        if (isLoggedInUserIsAssignee && currentStatus == "On Hold") {
            component.set("v.showOpActionLink", true);
        }
    },
    openLinkIncidentsComposer : function(component, event, helper) {
        component.set("v.linkIncidentsComponentVisible",true);
        component.set("v.linkIncidentsComposerVisible",true);
        component.set("v.disableActionLink", true);
    },
    handleCancelConfirmationModal:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
        component.set("v.linkIncidentsComposerVisible",true);
    },
    handleClose:function(component, event, helper) {
        component.set("v.linkIncidentsComposerVisible",false);
        component.set("v.confirmationModalVisible",true);
    },
    handleCloseConfirmed:function(component, event, helper) {
        component.set("v.SelectedNetworkIncidents",null);
        component.set("v.messageToAccessSeeker",null);
        component.set("v.confirmationModalVisible",false);
        component.set("v.placeIncidentOnHoldComposerVisible",false);
        component.set("v.placeIncidentOnHoldComponentVisible",false);
        component.set("v.disableActionLink",false);
    },
    handleMinimize : function(component, event, helper) {
        var linkIncidentsSectionDiv = component.find("linkIncidentsSection");
        if($A.util.hasClass(linkIncidentsSectionDiv, 'slds-is-open'))
        {            
            $A.util.addClass(linkIncidentsSectionDiv, 'slds-is-closed');
            $A.util.removeClass(linkIncidentsSectionDiv, 'slds-is-open');
            component.set("v.docIconMinMax","utility:expand_alt");
            component.set("v.minMaxTooltipText","Maximize Link incident(s)");
        }
        else
        {            
            $A.util.removeClass(linkIncidentsSectionDiv, 'slds-is-closed');
            $A.util.addClass(linkIncidentsSectionDiv, 'slds-is-open');
            component.set("v.docIconMinMax","utility:minimize_window");
            component.set("v.minMaxTooltipText","Minimize Link incident(s)");
        }
    },
    setLinkNetworkIncidents : function(component, event, helper) {
        helper.submitLinkNetworkIncidents(component, event, helper);
    }
})