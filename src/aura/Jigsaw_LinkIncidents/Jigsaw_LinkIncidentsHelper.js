({
	validateLinkedIncidents : function(networkObjects) {
        var hasNetworkIncidentsToBelinked = false;
        if(networkObjects)
        {
            for(var i = 0; i < networkObjects.length; i++)
            {
                if(networkObjects[i].relatedToSI != "Yes")
                {
                    hasNetworkIncidentsToBelinked = true;
                }
            }
        }
        return hasNetworkIncidentsToBelinked;
    },
    //submit action to server
    submitLinkNetworkIncidents : function(component, event, helper) {
        var currentIncidentWrapper = component.get("v.attrCurrentIncidentRecord");
        var action = component.get("c.linkNetworkIncidents");
        action.setParams({
            "strRecordId": currentIncidentWrapper.inm.Id,
            "strNbnInternalNote": component.get("v.messageToAccessSeeker"),
            "strNetworkIncidents" : component.get("v.SelectedNetworkIncidents")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS") 
            {
                var varattrCurrentIncientRecord = response.getReturnValue();
                if(varattrCurrentIncientRecord)
                {
                    var toastEvent = $A.get("e.force:showToast");
                    //fire the event to auto refresh the recent history module
                    var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
                    addNoteRefreshEvent.fire();
                    //Gray Toast Message section for si/ni linking action.
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                    	"message" : "Submitting Request" + "\n" + varattrCurrentIncientRecord.DynamicToastForIncident__c
                    });
                    resultsToast.fire(); 
                    component.set("v.linkIncidentsComposerVisible",false);
                    component.set("v.confirmationModalVisible",false);
                    component.set("v.linkIncidentsComponentVisible",false);
                }
                else 
                {
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": $A.get("$Label.c.LinkIncidentsErrorMessage")
                    });
                    toastEvent.fire();
                }
            }
            else if(state == "INCOMPLETE") 
        	{
                // do something
            }
            else if(state == "ERROR") 
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})