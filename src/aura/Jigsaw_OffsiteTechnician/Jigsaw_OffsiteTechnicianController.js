({
    fetchInitData : function(component, event, helper) {
        if(!component.get('v.disableAction')){
            helper.getTier1codes(component, event, helper);
            helper.getWorkOrderRootCauseTemplate(component, event, helper);
        }
    },
    setWorkOrderData : function(component, event, helper) {
        if(event.getParam("recordId") == component.get("v.attrCurrentIncidentRecord.inm.Id")){
            helper.getWorkOrderHistoryValues(component, event, helper);
        }
    },
    onselectedTier1Code : function(component, event, helper) {
        component.set('v.selectedTier1Code',event.getParam('name'));
        component.set('v.tier2CodesData',null);
        component.set('v.selectedTier2Code',null);
        component.set('v.tier3CodesData',null);
        component.set('v.selectedTier3Code',null);
        component.set('v.industryCode',null);
        helper.getTier2codes(component, event, helper);
     
    },
    onselectedTier2Code : function(component, event, helper) {
        component.set('v.selectedTier2Code',event.getParam('name'));
        component.set('v.tier3CodesData',null);
        component.set('v.selectedTier3Code',null);
        component.set('v.industryCode',null);
        helper.getTier3codes(component, event, helper);
    },
    onselectedTier3Code : function(component, event, helper) {
        component.set('v.selectedTier3Code',event.getParam('name'));
        helper.getIndustryCodes(component, event, helper);
    },
    offsiteTech:function(component, event, helper) {
        helper.submitOffsiteTechnician(component, event, helper);
    },
    openRequestInfo : function(component, event, helper) {
        //Commented as per new requirement to keep drill down open while performing operator action
        //Fire Summary Table Event To Dismiss The Section While Actions Are Performed
       // var summaryTableEvent = component.getEvent("handleSummaryTableEvent");
       // summaryTableEvent.fire();
       helper.getWorkOrderDetailsValues(component, event, helper);
    },
    handleClose:function(component, event, helper) {
        component.set("v.offsiteTechnicianComposerVisible",false);
        component.set("v.confirmationModalVisible",true);
    },
    handleCancelConfirmationModal:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
        component.set("v.offsiteTechnicianComposerVisible",true);
    },
    handleCloseConfirmed:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
        component.set("v.offsiteTechnicianComposerVisible",false);
        component.set("v.offsiteTechnicianComponentVisible",false);
        component.set("v.disableActionLink",false);
    },
    handleMinimize : function(component, event, helper) {
        var offsiteTechSectionDiv = component.find("offsiteTechnicianSection");
        if($A.util.hasClass(offsiteTechSectionDiv, 'slds-is-open'))
        {            
            $A.util.addClass(offsiteTechSectionDiv, 'slds-is-closed');
            $A.util.removeClass(offsiteTechSectionDiv, 'slds-is-open');
        }
        else
        {            
            $A.util.removeClass(offsiteTechSectionDiv, 'slds-is-closed');
            $A.util.addClass(offsiteTechSectionDiv, 'slds-is-open');
        }   
    },
    yesClickAction: function(component, event, helper) {
        console.log('###accept click action###');
        component.set("v.IsOffsiteTechnicianWorkingServiceTestPoint",'true');  
        component.set("v.isfaultValue",'true');
        
    },
    noClickAction: function(component, event, helper) {
        console.log('###accept click action###');
        component.set("v.IsOffsiteTechnicianWorkingServiceTestPoint",'false');
        component.set("v.OffsiteTechnicianOffsiteTechnicianRootCause",'');
		component.set("v.isfaultValue",'false');
    },
    
    onselectedPValue : function(component, event, helper) {
        component.set('v.cCodesData',null);
        component.set('v.selectedTier2Code',null);
        component.set('v.rCodesData',null);
        component.set('v.selectedTier3Code',null);
        component.set('v.actionCompleteORIncomplete', null);
        console.log(' come into onselectedPValue');
        var indx = component.find("selectProblem").get("v.value");
        var qKey = component.get('v.quickSelectedKey');
        if(qKey === ''){
            var selectedVal = component.find("QuickSelSatus");
        	selectedVal.set("v.value",'');
            component.set('v.quickSelectedPCR','');
        }            
        helper.getWOCauseValues(component, event, helper);       

    },
    onselectedCValue : function(component, event, helper) {
        component.set('v.rCodesData',null);
        component.set('v.selectedTier3Code',null);
        component.set('v.actionCompleteORIncomplete', null);
        console.log(' come into onselectedCValue');
        var indx = component.find("selectProblem").get("v.value");
        var qKey = component.get('v.quickSelectedKey');
        if(qKey === ''){
            var selectedVal = component.find("QuickSelSatus");
        	selectedVal.set("v.value",'');
            component.set('v.quickSelectedPCR','');
        }
        helper.getWOResolutioncodes(component, event, helper);        
    },
     onselectedRValue : function(component, event, helper) {
       console.log(' come into onselectedRValue');
        component.set('v.actionCompleteORIncomplete', null);
        helper.getWOSeletedActivityType(component, event, helper);      
    },
    
    handleChangeOffsiteTechnicianResolutionCategoryQuickSelect : function(component, event, helper) {
    	console.log('come into select change method');
        var selectedVal = component.find("QuickSelSatus");
        var nav =	selectedVal.get("v.value");
        if(nav !== 'undefined' && nav !== '' ){
            component.set('v.quickSelectedPCR',nav);           
        }
        var navKey =	selectedVal.get("v.key");
        console.log(' selectedVal is --: '+ component.get('v.quickSelectedPCR'));
        console.log(' nav value : '+ nav);
        console.log(' navKey value : '+ navKey);
        component.set('v.quickSelectedKey',nav);
        helper.getAutoSelectPCR(component, event, helper);
    },
    
    handleChangeOffsiteTechnicianOffsiteTechnicianRootCause : function(component, event, helper) {
    	console.log('come into select change method');
        var selectedVal = component.find("statusRootCause");
        var nav =	selectedVal.get("v.value");
        if(nav !== 'undefined' && nav !== '' ){
            component.set('v.OffsiteTechnicianOffsiteTechnicianRootCause',nav);           
        }
    },
    
    SubmitOffTech : function(component, event, helper) {        	
       component.set("v.offsiteTechnicianComponentVisible",false);
        component.set("v.disableActionLink",true);
        var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
            fireSpinnerRefresh.setParams({"userAction" :"offsiteTechnician", "stopSpinner" : false, "spinnersToStart": "IncidentStatus", "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
            fireSpinnerRefresh.fire();
        	var resultsToast = $A.get("e.force:showToast");
       		resultsToast.setParams({
                "title": "Submitting offsite technician (step 1 of 2).",
                "message": "This should only take 10-15 secs."
            });
        	resultsToast.fire();
        helper.CreateWorkOrder(component, event, helper);      
    }
    
})