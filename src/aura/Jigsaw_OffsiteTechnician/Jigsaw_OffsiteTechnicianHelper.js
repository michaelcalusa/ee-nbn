({
    getWorkOrderRootCauseTemplate: function(component, event, helper){
        var action = component.get("c.getWorkOrderRootCause");           
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {  
                var arrayOfMapKeys = [];
                var StoreResponse = response.getReturnValue();
                console.log('StoreResponse' + StoreResponse);
                component.set('v.ListOffsiteTechnicianOffsiteTechnicianRootCause', StoreResponse);
                for (var singlekey in StoreResponse) {
                    arrayOfMapKeys.push(singlekey);
                }                
                component.set('v.ListOffsiteTechnicianOffsiteTechnicianRootCause', arrayOfMapKeys);
            } 
        });           
        $A.enqueueAction(action);
    },
    getWorkOrderHistoryValues: function(component, event, helper) {
        let wohistorywrap = event.getParam('wohistorywrapper'); 
        if(wohistorywrap && wohistorywrap.offsiteTechnician && component.get('v.showOpActionLink')){
            var result = wohistorywrap.offsiteTechnician;
            var size = result.nbnworkhistoryview.length;
            var v = 0;
            if(result.nbnworkhistoryview.length > 1){
                for (var i = 0; i < result.nbnworkhistoryview.length; i++){
                    if(result.nbnworkhistoryview[i].status === $A.get("$Label.c.Jigsaw_OffsiteActiveWO")){
                        v = i;
                    }
                }
            }
            component.set('v.activeWO',result.nbnworkhistoryview[v].wonum );
			component.set("v.disableAction",false);
            component.set('v.EngagementWOType',result.nbnworkhistoryview[v].nbnrequirement );
            component.set('v.accessSeeker', result.nbnworkhistoryview[v].nbnworkforce);
            component.set('v.ParentWO',result.nbnworkhistoryview[v].parent );
            component.set('v.WorkRequest',result.ticketid );
            component.set('v.TechnologyType',result.nbnaccesstech);
            component.set('v.WOAppointmentID',result.nbnworkhistoryview[v].nbnappointmentid);
        }else{                    
            component.set("v.show_resolve_link", true);
            component.set("v.disableOffsiteActionLink", true);
            var newbutton = component.find('newOfftechbutton');
            component.set("v.disableAction",true);
            $A.util.addClass(newbutton, 'HideLinkToClick');
        }
    },
    getWorkOrderDetailsValues: function(component, event, helper) {
        var self = this;
        var action = component.get("c.getDetailWorkOrderValues");
        var currentIncidentManagementRecordWrapper = component.get("v.attrCurrentIncidentRecord");
        action.setParams({
            woNumber : component.get('v.activeWO')      
        });
        action.setCallback(this, function(data) {
            var response = data.getState();            
            if(response === 'SUCCESS')
            {	
                var WODetailWrapper = data.getReturnValue();
                console.log('Gaurav Inside WODetail' + WODetailWrapper.nbnwospecversion);
                if(!$A.util.isEmpty(WODetailWrapper) && !$A.util.isUndefined(WODetailWrapper)){
                    var engType = component.get('v.EngagementWOType');
                    if((WODetailWrapper.nbnwospecversion == '4.3.0' || WODetailWrapper.nbnwospecversion == '2.3.0') && (component.get('v.TechnologyType') =='FTTB' || component.get('v.TechnologyType') == 'FTTN') && (engType =='COMMITMENT' || engType == 'APPOINTMENT')){
                        component.set('v.specVersion',WODetailWrapper.nbnwospecversion);
                        component.set('v.WOSpecification',WODetailWrapper.nbnwospecid);
                        //component.set('v.TechnologyType',techType.substring(0, 4));
                        self.getWorkOrderQuickResolutionValues(component, event, helper);
                        self.getWorkOrderProblemValues(component, event, helper);                        
                        component.set("v.offsiteTechnicianComponentVisible",true);  
                        component.set("v.offsiteTechnicianComposerVisible",true);  
                        component.set("v.disableActionLink", true);                     
                    }else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : $A.get("$Label.c.OffsiteTechnicianErrorHeader"),
                            message: $A.get("$Label.c.OffsiteTechnicianNOSupportedWO"),
                            type: 'Error'
                        });
                        toastEvent.fire();
                    }                  
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.OffsiteTechnicianErrorHeader"),
                        message: 'Something went wrong, Please contact system administrator',
                        type: 'Error'
                    });
                    toastEvent.fire();
                }
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : $A.get("$Label.c.OffsiteTechnicianErrorHeader"),
                    message: 'Something went wrong, Please contact system administrator',
                    type: 'Error'
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);  
    },
    getTier1codes: function(component, event, helper) {
        var self = this;
        var action = component.get("c.getCategoryResoltuionCodes");
        var currentIncidentManagementRecordWrapper = component.get("v.attrCurrentIncidentRecord");
        action.setParams({
            productCategory : currentIncidentManagementRecordWrapper.inm.Prod_Cat__c,
            operationCategory : currentIncidentManagementRecordWrapper.inm.Op_Cat__c
        });
        action.setCallback(this, function(data) {
            var tier1Codesactionstatus = data.getState();
            if(tier1Codesactionstatus === 'SUCCESS')
            {
                var strtier1codes = data.getReturnValue();
                component.set('v.resolutionCodesCustomMetaData', strtier1codes);
                var distinctNames = this.getDistinctNames(strtier1codes,"Resolution_Category_Tier_1_Code__c");
                component.set('v.tier1CodesData', distinctNames.sort());
            }
        });
        $A.enqueueAction(action);
    },
    getTier2codes : function(component, event, helper) {
        var selectedTier1Code = component.get('v.selectedTier1Code');
        var tier2Codes = this.filterResolutionCodes(component.get('v.resolutionCodesCustomMetaData'),{key : "Resolution_Category_Tier_1_Code__c", value : selectedTier1Code});
        var distinctNames = this.getDistinctNames(tier2Codes,"Resolution_Category_Tier_2_Code__c");
        component.set('v.tier2CodesData', distinctNames.sort());
    },
    getTier3codes : function(component, event, helper) {
        var selectedTier1Code = component.get('v.selectedTier1Code');
        var selectedTier2Code = component.get('v.selectedTier2Code');
        var tier2Codes = this.filterResolutionCodes(component.get('v.resolutionCodesCustomMetaData'),{key : "Resolution_Category_Tier_1_Code__c", value : selectedTier1Code});
        var tier3Codes = this.filterResolutionCodes(tier2Codes,{key : "Resolution_Category_Tier_2_Code__c", value : selectedTier2Code});
        var distinctNames = this.getDistinctNames(tier3Codes,"Resolution_Category_Tier_3_Code__c");
        component.set('v.tier3CodesData', distinctNames.sort());
        if(distinctNames.length === 1)
        {
            component.set('v.selectedTier3Code', distinctNames[0]); 
            this.getIndustryCodes(component, event, helper);
        }
    },
    getIndustryCodes : function(component, event, helper) {
        var selectedTier1Code = component.get('v.selectedTier1Code');
        var selectedTier2Code = component.get('v.selectedTier2Code');
        var selectedTier3Code = component.get('v.selectedTier3Code');
        var tier2Codes = this.filterResolutionCodes(component.get('v.resolutionCodesCustomMetaData'),{key : "Resolution_Category_Tier_1_Code__c", value : selectedTier1Code});
        var tier3Codes = this.filterResolutionCodes(tier2Codes,{key : "Resolution_Category_Tier_2_Code__c", value : selectedTier2Code});
        var industryCodes = this.filterResolutionCodes(tier3Codes,{key : "Resolution_Category_Tier_3_Code__c", value : selectedTier3Code});
        component.set('v.industryCode', industryCodes[0].Industry_Code__c);
        component.set('v.industryDescription', industryCodes[0].Industry_Description__c);
    },
       //Functions to filter Json Data object
    getDistinctNames : function(resolutioncodesarray,name){
        var lookup = {};
        var result = [];
        for (var item, i = 0; item = resolutioncodesarray[i++];) {
            var loopname = item[name];
            if (!(loopname in lookup)) {
                lookup[loopname] = 1;
                result.push(loopname);
            }
        }
        return result;
    },
    filterResolutionCodes : function(resolutioncodesarray,searchobject){
        var filteredArray = resolutioncodesarray.filter(function(currentitem){
            return (currentitem[searchobject.key] === searchobject.value);
        });
        return filteredArray;
    },
    getWorkOrderQuickResolutionValues: function(component, event, helper){
        console.log('techtype - ' + component.get("v.TechnologyType") + '  EngageType -' + component.get("v.EngagementWOType") + '  version -' + component.get("v.specVersion"));
        var action = component.get("c.getResolutionDropdownValues"); 
        var techType = component.get("v.TechnologyType");
        var engageType = component.get("v.EngagementWOType");
        var specVersion = component.get("v.specVersion");
        action.setParams({
            "usrEngageType": engageType, // "Appointment", //engageType,
            "woTechType":  techType, // "FTTN", //techType,
            "woVersion": "" //specVersion //""
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {  
                var QuickSelectValues = [];
                var quickResponse = response.getReturnValue();
                console.log('get quickResponse : ' + quickResponse);
                var quickRes = [];
                for(var key in quickResponse){
                    quickRes.push({value:quickResponse[key], key:key}); //Here we are creating the list to show on UI.
                    console.log('value:quickRes[key] :' + quickResponse[key]);
                    console.log('vkey:' + key);
                }
                component.set('v.quickRecords', quickRes);
            } 
        });           
        $A.enqueueAction(action);
    },
    getWorkOrderProblemValues: function(component, event, helper){
        var action = component.get("c.getPCRecords"); 
        var techType = component.get("v.TechnologyType");
        var engageType = component.get("v.EngagementWOType");
        var specVersion = component.get("v.specVersion");
        console.log('techtype - ' + techType + '  EngageType -' + engageType + '  version -' + specVersion);
        action.setParams({
            "usrEngageType":  engageType, //engageType,Appointment
            "woTechType":   techType, //techType, 
            "woVersion": specVersion            
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {  
                var QuickSelectValues = [];
                var pcrRecords = response.getReturnValue();
                console.log('get quickResponse : ' + pcrRecords);
                component.set('v.pcrRecords', pcrRecords);
                var distinctNames = this.getDistinctNames(pcrRecords,"P_Description__c");
                component.set('v.pCodesData', distinctNames.sort());         	
            } 
        });           
        $A.enqueueAction(action);
    },    
    getWOCauseValues: function(component, event, helper){
        var selectedTier1Code = component.get('v.selectedTier1Code');
        console.log('selectedTier1Code value :' +selectedTier1Code);
        var tier2Codes = this.filterPCRCodes(component.get('v.pcrRecords'),{key : "P_Description__c", value : selectedTier1Code});
        console.log('tier2 codes are '+tier2Codes);
        var distinctNames = this.getDistinctNames(tier2Codes,"C_Description__c");
        console.log('distinctNames cause value :' +distinctNames);
        component.set('v.cCodesData', distinctNames.sort());
        if(distinctNames.length === 1)
        {
            component.set('v.selectedTier2Code', distinctNames[0]); 
            component.set('v.rCodesData',null);
            component.set('v.selectedTier3Code',null);
            this.getWOResolutioncodes(component, event, helper);
        }        
    },    
    getWOResolutioncodes : function(component, event, helper) {
        var selectedTier1Code = component.get('v.selectedTier1Code');
        var selectedTier2Code = component.get('v.selectedTier2Code');
        console.log('come into getWOResolutioncodes ' );
        console.log('selectedTier1Code value :' +selectedTier1Code);
        console.log('selectedTier2Code value :' +selectedTier2Code);
        var tier2Codes = this.filterPCRCodes(component.get('v.pcrRecords'),{key : "P_Description__c", value : selectedTier1Code});
        var tier3Codes = this.filterPCRCodes(tier2Codes,{key : "C_Description__c", value : selectedTier2Code});
        console.log('tier3Codes for R value :' +tier3Codes);
        var distinctNames = this.getDistinctNames(tier3Codes,"R_Description__c");
        console.log('distinctNames for R value :' +distinctNames);
        component.set('v.rCodesData', distinctNames.sort());
        if(distinctNames.length === 1)
        {
            component.set('v.selectedTier3Code', distinctNames[0]); 
            this.getWOSeletedActivityType(component, event, helper);
        }        
    },    
    getWOSeletedActivityType: function(component, event, helper){
        var selectedTier1Code = component.get('v.selectedTier1Code');
        var selectedTier2Code = component.get('v.selectedTier2Code');
        var selectedTier3Code = component.get('v.selectedTier3Code');
        console.log('come into getWOResolutioncodes ' );
        console.log('selectedTier1Code value :' +selectedTier1Code);
        console.log('selectedTier2Code value :' +selectedTier2Code);
        var pDesValues = this.filterPCRCodes(component.get('v.pcrRecords'),{key : "P_Description__c", value : selectedTier1Code});
        var cDescCodes = this.filterPCRCodes(pDesValues,{key : "C_Description__c", value : selectedTier2Code});
        var rDescCodes = this.filterPCRCodes(cDescCodes,{key : "R_Description__c", value : selectedTier3Code});
        console.log('tier3Codes for R value :' +rDescCodes);
        var actionType = this.getDistinctNames(rDescCodes,"Action_Type__c");
        console.log('actionType Selected is :' + actionType);        
        var pcrCode = this.getDistinctNames(rDescCodes,"PCR_Code__c");
        console.log('pcrCode Selected is :' + pcrCode[0] );
        component.set('v.selectedPCR', pcrCode[0]);        
        var pcrDesc = this.getDistinctNames(rDescCodes,"PCR_Description__c");
        console.log('pcr Description Selected is :' + pcrDesc[0] );
        component.set('v.selectedPCRDesc', pcrDesc[0]);        
        component.set('v.actionCompleteORIncomplete', actionType[0]); 
        var quickSelPCR = component.get('v.quickSelectedPCR');
        component.set('v.quickSelectedKey','');
        if(pcrCode[0]!== quickSelPCR){
            var selectedVal = component.find("QuickSelSatus");
            selectedVal.set("v.value",'');
            component.set('v.quickSelectedPCR','');
        }
    },    
    getAutoSelectPCR: function(component, event, helper){
        var selectedQuickPCR = component.get('v.quickSelectedPCR');
        console.log(' selectedQuickPCR ' + selectedQuickPCR);
        if(selectedQuickPCR === '' || selectedQuickPCR === null || selectedQuickPCR === 'undefined'){
            console.log(' come into the null select PCR if');
            component.set('v.selectedTier1Code', null); 
            component.set('v.cCodesData',null);
            component.set('v.selectedTier2Code',null);
            component.set('v.rCodesData',null);
            component.set('v.selectedTier3Code',null);
            component.set('v.actionCompleteORIncomplete',null);
        }
        else{
            console.log('selectedQuickPCR value :' +selectedQuickPCR);
            var PCRDesValues = this.filterPCRCodes(component.get('v.pcrRecords'),{key : "PCR_Code__c", value : selectedQuickPCR});
            console.log('PCRDesValues codes are '+PCRDesValues);
            var pDescValue = this.getDistinctNames(PCRDesValues,"P_Description__c");
            console.log('pDescValue Problem value :' +pDescValue);
            //component.set('v.cCodesData', distinctNames.sort());
            if(pDescValue.length === 1)
            {
                component.set('v.pCodesData',helper.highlightSelected(component.get('v.pCodesData'),component.get('v.pCodesData').indexOf(pDescValue[0]),0));
                component.set('v.selectedTier1Code', pDescValue[0]); 
                this.getWOCauseValues(component, event, helper);
                var cDescValue = this.getDistinctNames(PCRDesValues,"C_Description__c");
                console.log('cDescValue Cause value :' +cDescValue);
                if(cDescValue.length === 1)
                {	
                    component.set('v.cCodesData',helper.highlightSelected(component.get('v.cCodesData'),component.get('v.cCodesData').indexOf(cDescValue[0]),0));
                    component.set('v.selectedTier2Code', cDescValue[0]); 
                    this.getWOResolutioncodes(component, event, helper);
                    var rDescValue = this.getDistinctNames(PCRDesValues,"R_Description__c");
                    console.log('rDescValue Cause value :' +rDescValue);
                    if(rDescValue.length === 1)
                    {
                        component.set('v.rCodesData',helper.highlightSelected(component.get('v.rCodesData'),component.get('v.rCodesData').indexOf(rDescValue[0]),0));
                        component.set('v.selectedTier3Code', rDescValue[0]);
                        this.getWOSeletedActivityType(component, event, helper);
                    }
                }
            }
        }
    },
    getWorkOrderResolutionValues: function(component, event, helper){},    
    filterPCRCodes : function(pcrcodesarray,searchobject){
        console.log('pcrcodesarray :-' + pcrcodesarray);
        console.log('searchobject :-' + searchobject.value);
        var filteredArray = pcrcodesarray.filter(function(currentitem){
            return (currentitem[searchobject.key] === searchobject.value);
        });
        return filteredArray;
    },    
    highlightSelected : function (arr, old_index, new_index) {
        while (old_index < 0) {
            old_index += arr.length;
        }
        while (new_index < 0) {
            new_index += arr.length;
        }
        if (new_index >= arr.length) {
            var k = new_index - arr.length;
            while ((k--) + 1) {
                arr.push(undefined);
            }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);  
        return arr;
    },    
    CreateWorkOrder: function(component, event, helper){         
        var action = component.get("c.SaveWorkOrderRecord");        
        var techType = component.get("v.TechnologyType");
        var engageType = component.get("v.EngagementWOType");
        var specVersion = component.get("v.specVersion");
        var currentIncidentWrapper = component.get("v.attrCurrentIncidentRecord"); 
        var woNum = component.get('v.activeWO');
        var pcrCode = component.get('v.selectedPCR');
        //  var isfault = component.get('v.IsOffsiteTechnicianWorkingServiceTestPoint');
        var isfault = component.get('v.isfaultValue');
        var faultRootCause = component.get('v.OffsiteTechnicianOffsiteTechnicianRootCause');
        var addInfo = component.get('v.internalResolutionNote');
        var parent = component.get('v.ParentWO');
        var WRequest = component.get('v.WorkRequest');
        var WOSpecID = component.get('v.WOSpecification');
        var woActionType = component.get('v.actionCompleteORIncomplete');
        var servicePoint = component.get('v.OffsiteTechnicianWorkingServiceTestPoint');        
        var closureSummary =  component.get('v.closureSummary');
        var WOApptID = component.get('v.WOAppointmentID');
        if(closureSummary.length < 1)
            closureSummary = component.get('v.selectedPCRDesc'); 
            
        var selectedPCRDesc = component.get('v.selectedPCRDesc');
        var selectedPCRCode = component.get('v.selectedPCR');
        
        var netBoundaryPoint = component.get('v.OffsiteTechnicianNetworkBoundaryPoint');
        var netBoundaryPointDetails = component.get('v.OffsiteTechnicianNetworkBoundaryPointDetails');
        var seekerID = component.get('v.accessSeeker');
        console.log('techtype - ' + techType + '  EngageType -' + engageType + '  version -' + specVersion);
        console.log('currentIncidentWrapper - ' + currentIncidentWrapper + '  woNum -' + woNum + ' pcrCode -' + pcrCode);
        console.log('isfault - ' + isfault + '  faultRootCause -' + faultRootCause + '  addInfo -' + addInfo);
        console.log('woActionType - ' + woActionType + '  servicePoint -' + servicePoint );  
        
        var woTechType;
        if(techType.indexOf('FTTN') !== -1)
            woTechType = 'FTTN Service Assurance';
        else
            woTechType = 'End User Assurance FTTB';
        console.log('techtype - ' + woTechType + '  EngageType -' + engageType + '  version -' + specVersion);
        console.log('currentIncidentWrapper - ' + currentIncidentWrapper + '  woNum -' + woNum + ' pcrCode -' + pcrCode);
        console.log('isfault - ' + isfault + '  faultRootCause -' + faultRootCause + '  addInfo -' + addInfo);
        console.log('woActionType - ' + woActionType + '  servicePoint -' + servicePoint );
        
        var WOvalues = '{"woNum": "' + woNum + '","woEngageType" : "' + engageType + '","woSpecVersion": "' + specVersion + 
            '","woTechType" : "' + techType + '","woPCRCode" : "' + pcrCode + '","isFault" : "' + isfault + '","rootCause" : "' + 
            faultRootCause + '","WoActionType" : "' + woActionType + '","wkServiceTestPoint" : "' + servicePoint + 
            '","addInfo" : "' + addInfo + '","closureSummaryText" : "' + closureSummary + '","incidentId" : "' + 
            currentIncidentWrapper.inm.Name + '","strProdCat" : "' + currentIncidentWrapper.inm.Prod_Cat__c  + 
            '","strAVCId" : "' + currentIncidentWrapper.inm.AVC_Id__c + '","strPRI" : "' + currentIncidentWrapper.inm.PRI_ID__c +
            '","strAccessSeekerId" : "' + seekerID + '","parent" : "' + parent + '","WRequest" : "' + WRequest + 
            '","WOSpecID" : "' + WOSpecID + '","netBoundaryPoint" : "'  + netBoundaryPoint + '","netBoundaryPointDetails" : "' + 
            netBoundaryPointDetails + '","locationId" :"' + currentIncidentWrapper.inm.locId__c + '","WorkOrderApptID" : "' + 
            WOApptID + '","selectedPCRCode" :"' + selectedPCRCode + '","selectedPCRDesc" :"' + selectedPCRDesc + 
            '"}' ;
        
        console.log( 'WOvalues value is --' + WOvalues);
        
        action.setParams({
            "strWOWrapper" : WOvalues
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var returnResponse = response.getReturnValue();
            var toastEvent = $A.get("e.force:showToast");
            if (state === "SUCCESS") {   
                console.log('return response is --' + JSON.stringify(returnResponse));
                console.log('return response UpdatedIncidentRecordValue is --' + JSON.stringify(returnResponse.UpdatedIncidentRecordValue));
                if(returnResponse.MissingData.length > 0){                    
                    console.log('false field missed 1');
                    toastEvent.setParams({
                        title : $A.get("$Label.c.OffsiteTechnicianErrorHeader"),
                        message: 'Error in posting request due to missed fields :'+returnResponse.MissingData,
                        type: 'Error'
                    });
                    toastEvent.fire();
                    component.set("v.disableActionLink",false);
                    var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
                    fireSpinnerRefresh.setParams({"userAction" :"offsiteTechnician", "stopSpinner" : true, "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
                    fireSpinnerRefresh.fire();
                }
                else if(returnResponse.MissingData.length < 1 && returnResponse.UpdatedIncidentRecordValue && returnResponse.errorCode == ''){
                    console.log('true');
                    var varattrCurrentIncientRecord = returnResponse.UpdatedIncidentRecordValue;
                    component.set("v.offsiteTechnicianComponentVisible",false);
                   
                }
                else if(returnResponse.MissingData.length < 1 && returnResponse.UpdatedIncidentRecordValue == undefined && returnResponse.errorCode != '202'){
                    toastEvent.setParams({
                        title : $A.get("$Label.c.OffsiteTechnicianErrorHeader"),
                        message: $A.get("$Label.c.OffsiteTechnicianErrorRetry"),
                        type: 'Error'
                    });
                    toastEvent.fire();  
                    component.set("v.disableActionLink",false);
                    var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
        			fireSpinnerRefresh.setParams({"userAction" :"offsiteTechnician", "stopSpinner" : true, "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
					fireSpinnerRefresh.fire();
                }
                else{
                        toastEvent.setParams({
                            title : $A.get("$Label.c.OffsiteTechnicianErrorHeader"),
                            message: 'Something went wrong either retry else contact system administrator.',
                            type: 'Error'
                        });
                        toastEvent.fire();  
                        component.set("v.disableActionLink",false);
                    	var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
                        fireSpinnerRefresh.setParams({"userAction" :"offsiteTechnician", "stopSpinner" : true, "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
                        fireSpinnerRefresh.fire();
                    }
                
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.OffsiteTechnicianErrorHeader"),
                        message: 'Error in submitting offsite technician, Please contact system administrator',
                        type: 'Error'
                    });
                    toastEvent.fire();
                    component.set("v.disableActionLink",false);
                    var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
        			fireSpinnerRefresh.setParams({"userAction" :"offsiteTechnician", "stopSpinner" : true, "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
					fireSpinnerRefresh.fire();
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title : $A.get("$Label.c.OffsiteTechnicianErrorHeader"),
                        message: 'Something went wrong, Please contact system administrator',
                        type: 'Error'
                    });
                    toastEvent.fire();
                    component.set("v.disableActionLink",false);
                    var fireSpinnerRefresh = $A.get("e.c:Fire_Spinner_Event");
        			fireSpinnerRefresh.setParams({"userAction" :"offsiteTechnician", "stopSpinner" : true, "currentRecordId" : component.get("v.attrCurrentIncidentRecord").inm.Id});
					fireSpinnerRefresh.fire();
                }
            }
        });           
        $A.enqueueAction(action);    
        
    }
    
})