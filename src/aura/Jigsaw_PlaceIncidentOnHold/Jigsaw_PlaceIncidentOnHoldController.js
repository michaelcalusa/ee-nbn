({
	doinit : function(component, event, helper) {
        helper.getIncidentStatus(component, event, helper);
    },
    openPlaceIncidentOnHoldComposer : function(component, event, helper) {
        component.set("v.placeIncidentOnHoldComponentVisible",true);
        helper.getCommunicationTemplateValues(component, event, helper);
    },
    handlePlannedRemediationDateChange : function(component, event, helper) {
        var dd;
        var mm;
        var yyyy;
        //get SelectedPlannedRemediationDate
        var selectedPlannedRemediationDate = component.get("v.plannedRemediationDate");
        var formattedTodaysDate;
        var todaysDate = new Date();
        //Start of set formattedtoday's date
        dd = todaysDate.getDate();
        mm = todaysDate.getMonth() + 1; //January is 0!
        yyyy = todaysDate.getFullYear();
        // if date is less then 10, then append 0 before date   
        if(dd < 10) {
            dd = '0' + dd;
        }
        // if month is less then 10, then append 0 before date    
        if(mm < 10) {
            mm = '0' + mm;
        }
        formattedTodaysDate = yyyy + '-' + mm + '-' + dd;
        if(selectedPlannedRemediationDate != '' && selectedPlannedRemediationDate <= formattedTodaysDate) 
        {
            component.set("v.dateValidationError" , true);
        }
        else 
        {
            component.set("v.dateValidationError" , false);
        }
        //End of set formattedtoday's date
    },
    handleCancelConfirmationModal:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
        component.set("v.placeIncidentOnHoldComposerVisible",true);
    },
    handleClose:function(component, event, helper) {
        component.set("v.placeIncidentOnHoldComposerVisible",false);
        component.set("v.confirmationModalVisible",true);
    },
    handleCloseConfirmed:function(component, event, helper) {
        component.set("v.selectedTemplateValue","1234");
        component.set("v.plannedRemediationDate",null);
        component.set("v.messageToAccessSeeker",null);
        component.set("v.SelectedNetworkIncidents",null);
        component.set("v.templateChangeConfirmationModalVisible",false);
        component.set("v.confirmationModalVisible",false);
        component.set("v.placeIncidentOnHoldComposerVisible",false);
        component.set("v.placeIncidentOnHoldComponentVisible",false);
        component.set("v.disableActionLink",false);
    },
    handleMinimize : function(component, event, helper) {
        var placeIncidentOnHoldSectionDiv = component.find("placeIncidentOnHoldSection");
        if($A.util.hasClass(placeIncidentOnHoldSectionDiv, 'slds-is-open'))
        {            
            $A.util.addClass(placeIncidentOnHoldSectionDiv, 'slds-is-closed');
            $A.util.removeClass(placeIncidentOnHoldSectionDiv, 'slds-is-open');
        }
        else
        {            
            $A.util.removeClass(placeIncidentOnHoldSectionDiv, 'slds-is-closed');
            $A.util.addClass(placeIncidentOnHoldSectionDiv, 'slds-is-open');
        }
    },
    confirmTemplateValueChange :function(component, event, helper) {
        if(component.get("v.messageToAccessSeeker") && component.get("v.messageToAccessSeeker").length > 0)
        {
            component.set("v.templateChangeConfirmationModalVisible", true);
        }
        else
        {
            component.set("v.templateChangeConfirmationModalVisible", false);
            let templateOptions = component.get("v.templateCustomMetaData");
            let regex = new RegExp("\\{0\\}","g");
            let fileteredTemplate = helper.filterTemplates(templateOptions,{key : "MasterLabel", value : component.get("v.selectedTemplateValue")});
            fileteredTemplate = fileteredTemplate[0].Template_Value__c.replace(regex,component.get("v.SelectedNetworkIncidents"));
            component.set('v.messageToAccessSeeker',fileteredTemplate);
        }
    },
    setMessageToRSP : function(component, event, helper) {
        component.set("v.templateChangeConfirmationModalVisible", false);
        var templateOptions = component.get("v.templateCustomMetaData");
        var fileteredTemplate = helper.filterTemplates(templateOptions,{key : "MasterLabel", value : component.get("v.selectedTemplateValue")});
        component.set('v.messageToAccessSeeker',fileteredTemplate[0].Template_Value__c);
    },
    placeIncidentOnHold : function(component, event, helper) {
        helper.submitPlaceIncidentOnHold(component, event, helper);
    }
})