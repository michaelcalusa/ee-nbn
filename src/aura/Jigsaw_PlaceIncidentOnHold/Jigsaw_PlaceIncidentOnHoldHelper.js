({
    getCommunicationTemplateValues: function(component, event, helper) {
        var self = this;
        var action = component.get("c.getCommunicationTemplates");
        action.setParams({
            strCategoryName : "Place_Incident_On_Hold"
        });
        action.setCallback(this, function(data) {
            var gettemplateactionstatus = data.getState();
            if(gettemplateactionstatus == 'SUCCESS')
            {
                var temmplateMetadataRecords = data.getReturnValue();
                temmplateMetadataRecords.unshift({"Id":"1234","MasterLabel" : "Please select...","Template_Name__c" : "Please select...","Template_Value__c":""});
                component.set('v.templateCustomMetaData', temmplateMetadataRecords);
                component.set("v.placeIncidentOnHoldComposerVisible",true);
       			component.set("v.disableActionLink", true);
            }
        });
        $A.enqueueAction(action);
    },
    //submit action to server
    submitPlaceIncidentOnHold : function(component, event, helper) {
        var templateOptions = component.get("v.templateCustomMetaData");
        var fileteredTemplate = helper.filterTemplates(templateOptions,{key : "MasterLabel", value : component.get("v.selectedTemplateValue")});
        var strRequestDescription = fileteredTemplate[0].Template_Description__c;
        var strPlannedRemediationDate = component.get("v.plannedRemediationDate");
        var dtplannedRemediationDate;
        if(strPlannedRemediationDate)
        {
        	var dateParts = strPlannedRemediationDate.split("-");
            dtplannedRemediationDate = dateParts[0] + "-" + dateParts[1] + "-" + dateParts[2];
            var strPlannedRemediationTime = component.get("v.plannedRemediationTime");
            if(strPlannedRemediationTime)
            {
                var timeInSeconds = strPlannedRemediationTime.substring(0,5);
                dtplannedRemediationDate = dtplannedRemediationDate + ' ' + timeInSeconds + ':' + '00';
            }else{
                dtplannedRemediationDate = dtplannedRemediationDate + ' ' + '00:00' + ':' + '00';
            }

        }
        var currentIncidentWrapper = component.get("v.attrCurrentIncidentRecord");
        var action = component.get("c.placeHoldOnIncidentRecord");
        action.setParams({
            "strRecordId": currentIncidentWrapper.inm.Id,
            "strRequestDescription": strRequestDescription,
            "strPlannedRemediationDate": dtplannedRemediationDate,
            "strMessageSeeker": component.get("v.messageToAccessSeeker"),
            "strNetworkIncidents" : component.get("v.SelectedNetworkIncidents")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                //fire the event to auto refresh the recent history module
                var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
                addNoteRefreshEvent.fire();
                var varattrCurrentIncientRecord = response.getReturnValue();
                var spinnersToFire = '';
                if (varattrCurrentIncientRecord) {
                    if(varattrCurrentIncientRecord.Awaiting_Current_Incident_Status__c)
                        spinnersToFire = 'IncidentStatus';
                    if(varattrCurrentIncientRecord.Awaiting_Current_SLA_Status__c)
                        spinnersToFire = spinnersToFire + 'SLA';
                    var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
                    spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : varattrCurrentIncientRecord, "spinnersToStart" : spinnersToFire, "stopSpinner" : false});
                    spinnerHandlerEvent.fire();
                    component.set("v.templateChangeConfirmationModalVisible",false);
                    component.set("v.confirmationModalVisible",false);
                    component.set("v.placeIncidentOnHoldComposerVisible",false);
                    component.set("v.placeIncidentOnHoldComponentVisible",false);
               }
               else {
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": $A.get("$Label.c.PIHErrorMessage")
                    });
                    toastEvent.fire();
               }
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    filterTemplates : function(templateOptions,searchobject){
        var filteredArray = templateOptions.filter(function(currentitem){
            return (currentitem[searchobject.key] === searchobject.value);
        });
        return filteredArray;
    }
})