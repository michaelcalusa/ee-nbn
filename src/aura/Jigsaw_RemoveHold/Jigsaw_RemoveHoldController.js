({
    openRemoveHoldComposer : function(component, event, helper) {
        component.set("v.removeHoldComponentVisible",true);  
        component.set("v.removeHoldComposerVisible",true);  
        component.set("v.disableActionLink", true);
    },
    handleClose:function(component, event, helper) {
        component.set("v.removeHoldComposerVisible",false);
        component.set("v.confirmationModalVisible",true);
    },
    handleCancelConfirmationModal:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
        component.set("v.removeHoldComposerVisible",true);
    },
    handleCloseConfirmed:function(component, event, helper) {
        component.set("v.messageToAccessSeeker","");
        component.set("v.confirmationModalVisible",false);
        component.set("v.resolveIncidentComposerVisible",false);
        component.set("v.resolveIncidentComponentVisible",false);
        component.set("v.disableActionLink",false);
    },
    handleMinimize : function(component, event, helper) {
        var removeHoldSectionDiv = component.find("removeHoldSection");
        if($A.util.hasClass(removeHoldSectionDiv, 'slds-is-open'))
        {            
            $A.util.addClass(removeHoldSectionDiv, 'slds-is-closed');
            $A.util.removeClass(removeHoldSectionDiv, 'slds-is-open');
        }
        else
        {            
            $A.util.removeClass(removeHoldSectionDiv, 'slds-is-closed');
            $A.util.addClass(removeHoldSectionDiv, 'slds-is-open');
        }   
    },
    //Changes done as per user story 20475.(check current appointment status if it is in resered status send cancellation request to maximo otherwise proceed with resolveIncident)
    removeHold : function(component, event, helper) {
    	helper.submitRemoveHold(component, event, helper);
    }
})