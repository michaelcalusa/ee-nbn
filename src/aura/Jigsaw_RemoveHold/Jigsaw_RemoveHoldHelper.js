({
    //submit action to server
    submitRemoveHold : function(component, event, helper) {
        var self = this;
        var currentIncidentWrapper = component.get("v.attrCurrentIncidentRecord");
        var action = component.get("c.removeHoldOnIncidentRecord");
        action.setParams({
            "strRecordId": currentIncidentWrapper.inm.Id,
            "strMessageSeeker": component.get("v.messageToAccessSeeker")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                //fire the event to auto refresh the recent history module
                var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
                addNoteRefreshEvent.fire();
                var varattrCurrentIncientRecord = response.getReturnValue();
                var spinnersToFire = '';
                if (varattrCurrentIncientRecord) {
                    if(varattrCurrentIncientRecord.Awaiting_Current_Incident_Status__c)
                        spinnersToFire = 'IncidentStatus';
                    if(varattrCurrentIncientRecord.Awaiting_Current_SLA_Status__c)
                        spinnersToFire = spinnersToFire + 'SLA';
                    var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
                    spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : varattrCurrentIncientRecord, "spinnersToStart" : spinnersToFire, "stopSpinner" : false});
                    spinnerHandlerEvent.fire();
                    component.set("v.attrCurrentIncidentRecord",varattrCurrentIncientRecord);
                    component.set("v.removeHoldComponentVisible",false);
                }
                else {
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": $A.get("$Label.c.RemoveHoldOnIncidentErrorMessage")
                    });
                    toastEvent.fire();
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})