({
    doInit : function(component, event, helper) {
        helper.setFilterNonFilterAttributes(component,event,helper);
    },
    handleChange : function(component, event, helper) {
        if(event.getSource().get('v.checked'))
        {
        	component.set("v.SelectedNetworkIncidents",event.getSource().get("v.value"));
        }
	},
    setNetworkIncidentsAttribute : function(component, event, helper) {
    	var incidentNumberInput = event.getSource().get("v.validity");
        if(incidentNumberInput.valid)
        {
            var netWorkIncidentNumber = component.get("v.NetworkIncidentNumber");
            component.set("v.SelectedNetworkIncidents",netWorkIncidentNumber);
        }
		else
        {
            component.set("v.SelectedNetworkIncidents",null);
        }
    }
})