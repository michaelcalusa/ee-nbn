({
	setFilterNonFilterAttributes : function(component,event,helper) {
		var networkIncidentObjects = component.get("v.NetworkIncidentsObjectDetails");
        var linkedNetworkIncidentObjects = [];
        var nonLinkedNetworkIncidentObjects = [];
        if(networkIncidentObjects)
        {
            for(var i = 0; i < networkIncidentObjects.length; i++)
            {
                if(networkIncidentObjects[i].relatedToSI == "Yes")
                {
                    linkedNetworkIncidentObjects.push(networkIncidentObjects[i]);    
                }
                else
                {
                    nonLinkedNetworkIncidentObjects.push(networkIncidentObjects[i]);
                }
            }
            component.set("v.LinkedNetworkIncidentObjectDetails",linkedNetworkIncidentObjects);
            component.set("v.UnLinkedNetworkIncidentObjectDetails",nonLinkedNetworkIncidentObjects);
        }
	}
})