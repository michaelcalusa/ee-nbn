({
    performAction : function(component, event, helper) {
        var inm = component.get("v.incidentRecordObj");
        if(inm.Planned_Remediation_Date__c)
        {
            var plannedRemediationDate = new Date(inm.Planned_Remediation_Date__c);
            component.set("v.plannedRemediationDate",(plannedRemediationDate.getFullYear() + "-" + (plannedRemediationDate.getMonth() + 1) + "-" + plannedRemediationDate.getDate()));
            component.set("v.plannedRemediationTime",$A.localizationService.formatDate(plannedRemediationDate, "hh:mm a"));
        }
        component.set("v.dateChangeOccurred",false);
        component.set('v.docComposerComponentVisible',true);
        component.set('v.docComposerVisible',true);
        component.set("v.disableActionLink",true);
    }, 
    handlePlannedRemediationDateChange : function(component, event, helper) {
        var strPlannedRemediationDate = component.get("v.plannedRemediationDate");
        var dtplannedRemediationDate;
        if(strPlannedRemediationDate){    
            var dateParts = strPlannedRemediationDate.split("-");
            dtplannedRemediationDate = dateParts[0] + "-" + dateParts[1] + "-" + dateParts[2];
            var strPlannedRemediationTime = component.get("v.plannedRemediationTime");
            if(strPlannedRemediationTime)
            {   
                var timeInSeconds = strPlannedRemediationTime.substring(0,5);
                dtplannedRemediationDate = dtplannedRemediationDate + ' ' + timeInSeconds + ':' + '00';
                dtplannedRemediationDate = new Date(dtplannedRemediationDate);
                //End of set formattedtoday's date       
                var inm = component.get("v.incidentRecordObj");             
                var plannedRemediationDateOnIncident = inm.Planned_Remediation_Date__c ? new Date(inm.Planned_Remediation_Date__c) : new Date();
                if(dtplannedRemediationDate <= new Date()){
                    component.set("v.dateValidationError" , true);
                }
                else if(!((dtplannedRemediationDate.getTime() == plannedRemediationDateOnIncident.getTime())
                          && (dtplannedRemediationDate.getDate() == plannedRemediationDateOnIncident.getDate())
                          && (dtplannedRemediationDate.getMonth() == plannedRemediationDateOnIncident.getMonth())
                          && (dtplannedRemediationDate.getYear() == plannedRemediationDateOnIncident.getYear()))){
                    component.set("v.dateChangeOccurred",true);
                    component.set("v.dateValidationError" , false);
                }
                    else{
                        component.set("v.dateChangeOccurred",false);
                        component.set("v.dateValidationError" , false);
                    }
                helper.validate(component, event, helper);
            }else{
                dtplannedRemediationDate = dtplannedRemediationDate + ' ' + '00:00' + ':' + '00';
                component.set('v.disableSubmitButton',true);
            }
        } 
    },   
    validate : function(component, event, helper){
        helper.validate(component, event, helper);
    }, 
    handleCloseConfirmed:function(component, event, helper) {
        helper.resetAttributes(component,event,helper);
        component.set("v.disableActionLink",false);
    },
    submitResponse : function(component, event, helper) {
        helper.submitUpdatePlannedRemediationDate(component, event, helper);
    }
})