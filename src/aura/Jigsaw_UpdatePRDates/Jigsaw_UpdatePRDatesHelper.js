({
    //submit action to server
    submitUpdatePlannedRemediationDate : function(component, event, helper) {
        var self = this;
        var strPlannedRemediationDate = component.get("v.plannedRemediationDate");
        var dtplannedRemediationDate;
        if(strPlannedRemediationDate){
        	var dateParts = strPlannedRemediationDate.split("-");
            dtplannedRemediationDate = dateParts[0] + "-" + dateParts[1] + "-" + dateParts[2];
            var strPlannedRemediationTime = component.get("v.plannedRemediationTime");
            if(strPlannedRemediationTime){
                var timeInSeconds = strPlannedRemediationTime.substring(0,5);
                dtplannedRemediationDate = dtplannedRemediationDate + ' ' + timeInSeconds + ':' + '00';
            }else{
                dtplannedRemediationDate = dtplannedRemediationDate + ' ' + '00:00' + ':' + '00';
            }
        }
        var inm = component.get("v.incidentRecordObj");
        helper.performOperatorAction(component,{
                                                "strRecordId": inm.Id,
                                                "strPlannedRemediationDate": dtplannedRemediationDate,
                                                "strMessageSeeker": component.get("v.messageToAccessSeeker")
        										}, (response, component, self1) => {
                                                        var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
                                                        addNoteRefreshEvent.fire();
                                                        var inm = response.getReturnValue();
                                                        var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
                                                        spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : inm, "spinnersToStart" : "IncidentStatus", "stopSpinner" : true,"showSuccessToastMessage" : false,"hardRefreshComponent" : true});
                                                        spinnerHandlerEvent.fire();
                                                    	component.set("v.disableActionLink",true);
                                                    	var toastEvent = $A.get("e.force:showToast");
                                                        toastEvent.setParams({
                                                            "title": "Success",
                                                            "type": "success",
                                                            "message": "Action performed successfully."
                                                        });
        												toastEvent.fire();
                                                    	self.resetAttributes(component, event, helper);
                                                        var appEvent = $A.get("e.c:WaitingForUpdate");
                                                        appEvent.setParams({
                                                            "waitingForUpdate" : false
                                                        });
            											appEvent.fire();
                                                    });
    },
    validate : function(component, event, helper){
        if(component.get('v.dateChangeOccurred') && !component.get('v.dateValidationError') && component.get('v.messageToAccessSeeker')){
            component.set('v.disableSubmitButton',false);
        }
        else{
            component.set('v.disableSubmitButton',true);
        }
    },
    resetAttributes : function(component, event, helper){
        component.set("v.messageToAccessSeeker",null);
        component.set("v.plannedRemediationDate",null);
        component.set("v.docComposerComponentVisible",false);
        component.set("v.confirmationModalVisible",false);
        component.set("v.docComposerVisible",false);
    }
})