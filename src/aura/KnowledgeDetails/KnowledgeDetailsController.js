({
	onload : function(component, event, helper) {
        
        helper.getarticleDetails(component,component.get("v.selectedKnowledgeId"));
        
		
	},
    
    displayTaskDetailPage  : function(component, event, helper) {  
         component.set("v.displayStageAppDetailsBoolean",true);	
         component.set("v.displayTaskDetail",false);
      
         if (component.get("v.showArtListBoolean")) {
             var showSupportClass3and4Page = component.getEvent("showSupportClass3and4PageEvent");
             showSupportClass3and4Page.fire();
         }
         else {
             var displayTaskDetailsPage = component.getEvent("displayTaskDetailsPage");
             displayTaskDetailsPage.fire(); 
             var appEvent = $A.get("e.c:NewDevPortalShowHeaderAppEvent");
             appEvent.setParams({ "showHeader" : true });
             appEvent.fire();
         }
    }
})