({
	doInit : function(component, event, helper) {
        //S NPD-5247
        component.set('v.typeOfLICOptions', [
            { label: 'Select...', value: '' },
            { label: 'Left side of the premises', value: 'LICLeft' },
            { label: 'Right side of the premises', value: 'LICRight' },
            { label: 'Centre of the premises', value: 'LICCentre' },
            { label: 'None of the above', value: 'LICOther' }            
        ]);
        //E NPD-5247
        
		var devLICType = [];
         devLICType.push({"name" : "Horizontal", "desc"  :$A.get('$Resource.LeadinConduit'), "count":"col-1-in-4 margin-bottom-10"});
         devLICType.push({"name" : "Vertical", "desc"  :$A.get('$Resource.DevelopmentDetailsLICV'), "count":"col-2-in-4 margin-bottom-10"});
         component.set("v.devLICType",devLICType);
       
            
            var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
            component.set('v.displayLIC',wrapInstance.devDtlWrapper.licInstalled);
        	if(wrapInstance.devDtlWrapper.grannyFlat)
            {
                component.set('v.displayGPremises','Yes');
            }
            else if(!wrapInstance.devDtlWrapper.grannyFlat && component.get('v.noOfPremises')<9)
            {
                component.set('v.displayGPremises','No');
            }
        	else
            {
                component.set('v.displayGPremises','NA');
            }
          
        var grannylicType = [];
        grannylicType.push({"name" : "GrannyFlatLeft", "desc"  :$A.get('$Resource.DevelopmentDetailsGFL')});
        grannylicType.push({"name" : "GrannyFlatRight", "desc"  :$A.get('$Resource.DevelopmentDetailsGFR')});
        component.set("v.grannyLICType",grannylicType);
        var licType = [];
        /*licType.push({"name" : "LICLeft", "desc"  :$A.get('$Resource.DevelopmentDetailsLICLeft')});
        licType.push({"name" : "LICRight", "desc"  :$A.get('$Resource.DevelopmentDetailsLICRight')});*/
        /*//S NPD-5247
        licType.push({"name" : "SideDuplex", "desc"  :$A.get('$Resource.DevelopmentDetailsLICSD')});
        licType.push({"name" : "CentreDuplex", "desc"  :$A.get('$Resource.DevelopmentDetailsLICCD')});
        licType.push({"name" : "Cornerblock", "desc"  :$A.get('$Resource.DevelopmentDetailsLICCB')});
		E NPD-5247*/
        licType.push({"name" : "LICLeft", "desc"  :$A.get('$Resource.DevelopmentDetailsLICLeftNew')});
        licType.push({"name" : "LICRight", "desc"  :$A.get('$Resource.DevelopmentDetailsLICRightNew')});
        licType.push({"name" : "LICCentre", "desc"  :$A.get('$Resource.DevelopmentDetailsLICCenterNew')});
        licType.push({"name" : "LICOther", "desc"  :$A.get('$Resource.DevelopmentDetailsLICOtherNew')});
        //S NPD-5247
        licType.push({"name" : "LICNone", "desc"  :$A.get('$Resource.DevelopmentDetailsLICOtherNew')});
        //E NPD-5247
       	component.set("v.premiseLicType",licType);
        var licValues = [];
        var licChecks = [];
        var premisesMap = wrapInstance.premisesList; 
        var noOfPremisesMap = [];
        
        if((component.get('v.noOfPremises')<9))
        {
            if(premisesMap.length>0)
            {
                premisesMap =wrapInstance.premisesList;
                if(component.get('v.displayGPremises')==='No')
                {
                    for(var p= 1; p<=component.get('v.noOfPremises'); p++)
                    { 
                        var premise;
                        if(premisesMap[p-1] === undefined)
                        {
                            premise = {};
                        }
                        else
                        {
                            premise = premisesMap[p-1];                        
                        }
                        premise.name = p;
                        licValues[p] = premise.licType; 
                        if(premise.licType=== null || premise.licType === undefined || premise.licType === '')
                        {
                            licChecks[p] = false;
                            premise.btn = false;
                        }
                        else
                        {
                            licChecks[p] = true;
                            premise.btn = true;
                        }
                        premisesMap[p-1] = premise;
                    }
                    component.set('v.licValues',licValues);
                    component.set("v.noOfPremisesMap",premisesMap);
                    component.set("v.licChecks",licChecks);
                }
                else
                {
                        var premise = premisesMap[0];
                        component.set('v.grannyUnitNumber',premise.unitNumber);
                        component.set('v.grannyStreetNumber',premise.streetNumber);
                        component.set('v.grannyLotNumber',premise.lotNumber);
                        component.set("v.grannyLICTypes", premise.licType);
                        if(premise.licType === undefined)
                        {
                            component.set('v.grannyLICCheck',false);
                        }  
                        else
                        {
                            component.set('v.grannyLICCheck',true);
                        }
                    
                    for(var i = 0; i < component.get("v.noOfPremises"); i++)
                    {
                        noOfPremisesMap.push({"name" :i+1, "unitNumber" :'', "streetNumber" :'', "lotNumber" :'', "streetName" :'', "licType" :'', "premiseId" :'', "btn" :false});            
                    }
                    component.set("v.noOfPremisesMap",noOfPremisesMap);
                    wrapInstance.premisesList = noOfPremisesMap;
                }	
                    
            }
            else
            {
                
                for(var i = 0; i < component.get("v.noOfPremises"); i++)
                    {
                        noOfPremisesMap.push({"name" :i+1, "unitNumber" :'', "streetNumber" :'', "lotNumber" :'', "streetName" :'', "licType" :'', "premiseId" :'', "btn" :false});            
                    }
                    component.set("v.noOfPremisesMap",noOfPremisesMap);
                    wrapInstance.premisesList = noOfPremisesMap;
            }
            component.set("v.newDevApplicantWrapperInstance",wrapInstance);
        }
        
       
    },
    
    checkDevFBInstalled : function(component, event, helper) {

        var togglebuttonId = event.currentTarget.id;
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(togglebuttonId == 'licd-licDevFront-yes-toggleBtnId')
        {
            $A.util.addClass(document.getElementById('licd-licDevFront-yes-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById('licd-licDevFront-no-toggleBtnId'),'btn-primary');
            component.set('v.displayLIC',true);
            wrapInstance.devDtlWrapper.licInstalled = component.get('v.displayLIC');        
        }
        else if(togglebuttonId == 'licd-licDevFront-no-toggleBtnId')
        {
            $A.util.addClass(document.getElementById('licd-licDevFront-no-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById('licd-licDevFront-yes-toggleBtnId'),'btn-primary');
            component.set('v.displayLIC',false);  
            wrapInstance.devDtlWrapper.licInstalled = component.get('v.displayLIC');
            wrapInstance.devDtlWrapper.licType = '';
            component.set('v.licType','');
        }
	},
       
    checkGrannyFlat : function(component, event, helper) {
        var togglebuttonId = event.currentTarget.id;
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(togglebuttonId == 'licd-grannyflat-yes-toggleBtnId')
        {
            $A.util.addClass(document.getElementById('licd-grannyflat-yes-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById('licd-grannyflat-no-toggleBtnId'),'btn-primary');
            component.set('v.displayGPremises','Yes');
            wrapInstance.devDtlWrapper.grannyFlat = true;
            
           var premise = {};
            premise.unitNumber = document.getElementById('Grannypd-unitnumber').value;
            premise.streetNumber = document.getElementById('Grannypd-streetnumber').value;
            premise.lotNumber = document.getElementById('Grannypd-lotnumber').value;
            premise.streetName = document.getElementById('Grannypd-streetname').value;
            premise.licType = component.get("v.grannyLICTypes");
            premise.premiseId = wrapInstance.newDevApplicantId+'Premise-'+p;
            premiseMap[0]=premise;              
        }
        else if(togglebuttonId == 'licd-grannyflat-no-toggleBtnId')
        {
            $A.util.addClass(document.getElementById('licd-grannyflat-no-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById('licd-grannyflat-yes-toggleBtnId'),'btn-primary');
            component.set('v.displayGPremises','No');  
            wrapInstance.devDtlWrapper.grannyFlat = false;
            component.set('v.grannyUnitNumber','');
            component.set('v.grannyStreetNumber','');
            component.set('v.grannyLotNumber','');
            component.set("v.grannyLICTypes", '');
           	component.set('v.grannyLICCheck',false);
         	var premise = {};
            premise.unitNumber = document.getElementById('NonGranny1pd-unitnumber').value;
            premise.streetNumber = document.getElementById('NonGranny1pd-streetnumber').value;
            premise.lotNumber = document.getElementById('NonGranny1pd-lotnumber').value;
            premise.streetName = document.getElementById('NonGranny1pd-streetname').value;
            premise.licType = licValues[1];
            premise.premiseId = wrapInstance.newDevApplicantId+'Premise-'+1;
            premiseMap[0]=premise;                           
        }
        wrapInstance.premisesList = premiseMap;
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
	},
    
        
    processLICDetails : function(component, event, helper){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        var subDiv = document.querySelectorAll('input, select');
        var myArray = [];
        var licChecks = component.get('v.licChecks');
        var licValues = component.get("v.licValues");
        
        for(var i = 0; i < subDiv.length; i++) {
            var elem = subDiv[i];
            if(elem.id.indexOf('licd-') === 0 || elem.id.includes('Granny')) {
                myArray.push(elem.id); 
                
            }
        }
         if(component.get('v.grannyLICCheck'))
         {
             myArray.push('Grannypd-lic') ;
         }
         for (var l=1;l<=licChecks.length;l++)
         {
             if(licChecks[l])
             {
                 myArray.push('NonGrannyLICType'+l);
             }
         }
        if(component.get('v.displayLIC'))
        {
            myArray.push('Dev-lic');
        }
        var errorCarier = [];
        for(var inputElementId=0; inputElementId<myArray.length; inputElementId++){
            var errorDivId = document.getElementById(myArray[inputElementId]+'-div');
            var errorSpanId = document.getElementById(myArray[inputElementId]+'-error'); 
            var fieldId = myArray[inputElementId];
            var fieldVal;
            if(fieldId== 'Grannypd-lic' && component.get('v.grannyLICCheck'))
            {
                fieldVal = component.get("v.grannyLICTypes");
            }  
            else if (fieldId.includes('NonGrannyLICType'))
            {
                if(licValues.length>=fieldId.substr('NonGrannyLICType'.length,1))
                {
                   fieldVal = licValues[fieldId.substr('NonGrannyLICType'.length,1)]; 
                }
                else
                {
                    fieldVal = '';
                }
                
            }
            else if (fieldId == 'Dev-lic')
            {
                fieldVal = wrapInstance.devDtlWrapper.licType;
            }
            else
            {
                fieldVal = document.getElementById(myArray[inputElementId]).value;
            }
            if(!(fieldId.includes('-unitnumber')))
            {
            	var err = helper.validatehelper(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);
            	errorCarier.push(err);
            }
        }
        component.set('v.hasError', false);
        for(var i = 0; i < errorCarier.length; i++){
            if(errorCarier[i] == true){
                component.set('v.hasError', true);
            } 
        }
        var occupancyDate = component.get("v.newDevApplicantWrapperInstance").devDtlWrapper.estimatedOccupancydt;
        var res = occupancyDate.split("-");
        
        if( occupancyDate == undefined || occupancyDate == ''){
            component.set('v.hasError', true);
        }
        else if(!((res.length == 3) && res[0].length == 4 && res[1].length == 2 && res[2].length == 2 && 
           !isNaN(res[0]) && !isNaN(res[1]) && !isNaN(res[2]) && res[1] <= 12 && res[2] <= 31)){
            component.set('v.hasError', true);
        }
        
        if(!component.get("v.hasError"))
        {
            var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
            wrapInstance.devDtlWrapper.licInstalled = component.get('v.displayLIC');
            wrapInstance.devDtlWrapper.licType = component.get('v.licType');
            wrapInstance.pitAndPipeDetailsWrapper.noOfPremisesForStage = wrapInstance.devDtlWrapper.numberOfPremises;
            if(wrapInstance.devDtlWrapper.numberOfEssentialServices != '')
            {
                wrapInstance.pitAndPipeDetailsWrapper.noOfNonPremisesForStage = wrapInstance.devDtlWrapper.numberOfEssentialServices;
            }
            
            var licValues = component.get('v.licValues');
            var premiseMap = [];
            var label;
            if(component.get('v.displayGPremises')==='Yes')
            {
               for(var p= 1; p<=component.get('v.noOfPremises'); p++)
            	{ 
                    var premise = {};
                    premise.unitNumber = document.getElementById('Grannypd-unitnumber').value;
                    component.set('v.grannyUnitNumber',document.getElementById('Grannypd-unitnumber').value);
                    premise.streetNumber = document.getElementById('Grannypd-streetnumber').value;
                    component.set('v.grannyStreetNumber',document.getElementById('Grannypd-streetnumber').value);
                    premise.lotNumber = document.getElementById('Grannypd-lotnumber').value;
                    component.set('v.grannyLotNumber',document.getElementById('Grannypd-lotnumber').value);
                    premise.streetName = document.getElementById('Grannypd-streetname').value;
                    premise.licType = component.get("v.grannyLICTypes");
                    premise.premiseId = wrapInstance.newDevApplicantId+'Premise-'+p;
                    premiseMap.push(premise);
            	}                
            }
            else if (component.get('v.displayGPremises')==='No' && component.get('v.noOfPremises')< 9)
            {
               
				for(var p= 1; p<=component.get('v.noOfPremises'); p++)
            	{ 
                    var premise = {};
                    premise.unitNumber = document.getElementById('NonGranny'+p+'pd-unitnumber').value;
                    premise.streetNumber = document.getElementById('NonGranny'+p+'pd-streetnumber').value;
                    premise.lotNumber = document.getElementById('NonGranny'+p+'pd-lotnumber').value;
                    premise.streetName = document.getElementById('NonGranny'+p+'pd-streetname').value;
                    premise.licType = licValues[p];
                    premise.premiseId = wrapInstance.newDevApplicantId+'Premise-'+p;
                    premiseMap.push(premise);
            	}               
            }
            
            wrapInstance.premisesList = premiseMap;
           
            component.set("v.newDevApplicantWrapperInstance",wrapInstance);
            var action1 = component.get("c.saveNewApplicant");
            action1.setParams({
                "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance"))
            });
        	action1.setCallback(this, function(response){
                if(response.getState() == 'SUCCESS'){
                    var wrapInstanceTemp = component.get("v.newDevApplicantWrapperInstance");
                    wrapInstanceTemp.newDevApplicantId = response.getReturnValue();
                    component.set("v.newDevApplicantWrapperInstance",wrapInstanceTemp);
                    var saveEvent = component.getEvent("saveRecEvent");
                    saveEvent.setParams({
                        "wrapperInstance" : component.get("v.newDevApplicantWrapperInstance"),
                        "componentSaved" : 'developmentDetails'
                    });
                    saveEvent.fire();
                    
                }
                else{
                    console.log(response.getState()+' message is '+response.getError()[0].message);
                }
            });
            $A.enqueueAction(action1);    
            component.set("v.displayRequiredErrorMsg",false);
        }
        else{
            console.log('caught errors');
            component.set("v.displayRequiredErrorMsg",true);
        }    
   
    },
    
    validateFields : function(component, event, helper){

        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;
        var licCheck = component.get('v.licChecks');
        var fieldId = event.currentTarget.id;       
        var fieldVal = document.getElementById(fieldId).value;
        var licValues = component.get('v.licValues');
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        var premiseMap = wrapInstance.premisesList;
        var noOfPremisesMap = component.get("v.noOfPremisesMap");
        var label = fieldId.substr('NonGranny'.length,1);
        if(!(fieldId.includes('-unitnumber')))
        {
        	helper.validatehelper(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);
        }
        if(component.get('v.displayGPremises')==='Yes')
        {
                var premise = {};
                premise.unitNumber = document.getElementById('Grannypd-unitnumber').value;
                premise.streetNumber = document.getElementById('Grannypd-streetnumber').value;
                premise.lotNumber = document.getElementById('Grannypd-lotnumber').value;
                premise.streetName = document.getElementById('Grannypd-streetname').value;
                premise.licType = component.get("v.grannyLICTypes");
                premise.premiseId = wrapInstance.newDevApplicantId+'Premise-'+1;
                premiseMap[0]=premise;            
        }
        else
        {
             
                var premise = {};
                premise.unitNumber = document.getElementById('NonGranny'+label+'pd-unitnumber').value;
                premise.streetNumber = document.getElementById('NonGranny'+label+'pd-streetnumber').value;
                premise.lotNumber = document.getElementById('NonGranny'+label+'pd-lotnumber').value;
                premise.streetName = document.getElementById('NonGranny'+label+'pd-streetname').value;
                premise.licType = licValues[label];
                premise.premiseId = wrapInstance.newDevApplicantId+'Premise-'+label;
            	premise.name = label*1;
            	if(licCheck[label]=== undefined || licCheck[label]=== null || !licCheck[label])
                {
                   premise.btn = false; 
                }
            	else
                {
                    premise.btn = true;
                }
                premiseMap[label-1]=premise;
                noOfPremisesMap[label-1] = premise;
        }
        
        wrapInstance.premisesList = premiseMap;
        
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
        
    },
    
    devLICRadioChange : function(component, event, helper){
        component.set("v.licType",event.target.value);
        var selectedValue = event.target.value;
        var devLICTypes = component.get("v.devLICType");
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        wrapInstance.devDtlWrapper.licType = component.get('v.licType');
        
        for (var i=0;i<devLICTypes.length;i++)
        {
            if(devLICTypes[i].name == selectedValue)
            {
               $A.util.addClass(document.getElementById(component.get('v.licType')+'LIC'),'LICSeleted'); 
            }
            else
            {
                $A.util.removeClass(document.getElementById(devLICTypes[i].name+'LIC'),'LICSeleted'); 
            }
        }
    },
    
    grannylicLocationCheck : function(component, event, helper) {
		var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        var togglebuttonId = event.currentTarget.id;
        var premiseMap = wrapInstance.premisesList;
        if(togglebuttonId == 'pd-grannylicLocation-yes-toggleBtnId')
        {
            $A.util.addClass(document.getElementById('pd-grannylicLocation-yes-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById('pd-grannylicLocation-no-toggleBtnId'),'btn-primary');
            component.set('v.grannyLICCheck',true);
           	var premise = {};
            premise.unitNumber = document.getElementById('Grannypd-unitnumber').value;
            premise.streetNumber = document.getElementById('Grannypd-streetnumber').value;
            premise.lotNumber = document.getElementById('Grannypd-lotnumber').value;
            premise.streetName = document.getElementById('Grannypd-streetname').value;
            premise.licType = component.get("v.grannyLICTypes");
            premise.premiseId = wrapInstance.newDevApplicantId+'Premise-'+1;
            premiseMap[0] =premise;   
        }
        else if(togglebuttonId == 'pd-grannylicLocation-no-toggleBtnId')
        {
            $A.util.addClass(document.getElementById('pd-grannylicLocation-no-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById('pd-grannylicLocation-yes-toggleBtnId'),'btn-primary');
            component.set('v.grannyLICCheck',false);  
            component.set("v.grannyLICTypes",false);
            var premise = {};
            premise.unitNumber = document.getElementById('Grannypd-unitnumber').value;
            premise.streetNumber = document.getElementById('Grannypd-streetnumber').value;
            premise.lotNumber = document.getElementById('Grannypd-lotnumber').value;
            premise.streetName = document.getElementById('Grannypd-streetname').value;
            premise.licType = '';
            component.set("v.grannyLICTypes", '');
            premise.premiseId = wrapInstance.newDevApplicantId+'Premise-'+1;
            premiseMap[0] =premise;                 
        }
        wrapInstance.premisesList = premiseMap;
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
	},
    
    licLocationCheck : function(component, event, helper) {
        var togglebuttonId = event.currentTarget.id;
        var id = 'NonGranny';
        var licCheck = component.get('v.licChecks');
        var licValues = component.get('v.licValues');
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        var premiseMap = wrapInstance.premisesList;        
        
        var noOfPremisesMap = component.get("v.noOfPremisesMap");
        var label = togglebuttonId.substr(id.length,1);
        
        if(togglebuttonId == 'NonGranny'+label+'pd-licLocation-yes-toggleBtnId')
        {
            $A.util.addClass(document.getElementById('NonGranny'+label+'pd-licLocation-yes-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById('NonGranny'+label+'pd-licLocation-no-toggleBtnId'),'btn-primary');
            licCheck[label]=true;
            component.set('v.licChecks',licCheck);
            
            var premise = {};
            premise.unitNumber = document.getElementById('NonGranny'+label+'pd-unitnumber').value;
            premise.streetNumber = document.getElementById('NonGranny'+label+'pd-streetnumber').value;
            premise.lotNumber = document.getElementById('NonGranny'+label+'pd-lotnumber').value;
            premise.streetName = document.getElementById('NonGranny'+label+'pd-streetname').value;
            premise.licType = licValues[label];
            premise.premiseId = wrapInstance.newDevApplicantId+'Premise-'+label;
            premise.name = label*1;
            premise.btn = true;
            premiseMap[label-1] = premise;
            noOfPremisesMap[label-1] = premise;        
        }
        else if(togglebuttonId == 'NonGranny'+label+'pd-licLocation-no-toggleBtnId')
        {
            $A.util.addClass(document.getElementById('NonGranny'+label+'pd-licLocation-no-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById('NonGranny'+label+'pd-licLocation-yes-toggleBtnId'),'btn-primary');
            licCheck[label]=false;
            licValues[label] = '';
            component.set('v.licChecks',licCheck);
            var premise = {};
            premise.unitNumber = document.getElementById('NonGranny'+label+'pd-unitnumber').value;
            premise.streetNumber = document.getElementById('NonGranny'+label+'pd-streetnumber').value;
            premise.lotNumber = document.getElementById('NonGranny'+label+'pd-lotnumber').value;
            premise.streetName = document.getElementById('NonGranny'+label+'pd-streetname').value;
            premise.licType = '';
            premise.premiseId = wrapInstance.newDevApplicantId+'Premise-'+label;            
            premise.name = label*1;
            premise.btn = false;
            premiseMap[label - 1] = premise;
            noOfPremisesMap[label-1] = premise;
            component.set('v.licValues',licValues);
        }
        component.set("v.noOfPremisesMap",noOfPremisesMap);
        wrapInstance.premisesList = premiseMap;
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
	},
    
    grannyLICRadioChange : function(component, event, helper){
        component.set("v.grannyLICTypes",event.target.value);
        var gLicTypes = component.get("v.grannyLICType");
        var selectedValue = event.target.value;
        for (var i=0;i<gLicTypes.length;i++)
        {
            if(gLicTypes[i].name == selectedValue)
            {
               $A.util.addClass(document.getElementById(component.get('v.grannyLICTypes')+'granny'),'LICSeleted'); 
            }
            else
            {
                $A.util.removeClass(document.getElementById(gLicTypes[i].name+'granny'),'LICSeleted'); 
            }
        }
        var licValues = component.get('v.licValues');
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        var premiseMap = [];
        var label;
        
            var premise = {};
            premise.unitNumber = document.getElementById('Grannypd-unitnumber').value;
            premise.streetNumber = document.getElementById('Grannypd-streetnumber').value;
            premise.lotNumber = document.getElementById('Grannypd-lotnumber').value;
            premise.streetName = document.getElementById('Grannypd-streetname').value;
            premise.licType = component.get("v.grannyLICTypes");
            premise.premiseId = wrapInstance.newDevApplicantId+'Premise-'+1;
            premiseMap[0] =premise;
        
         wrapInstance.premisesList = premiseMap;
        
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
    },
    
    nGLICRadioChange : function(component, event, helper){             
        //New Developed Code        
        var currentPremise = event.getSource().get('v.label');     
        var selectedValue = '';
        
       	var cp=currentPremise-1;
        selectedValue = component.get("v.typeofLICCollection["+cp+"]");
        
        var licId = selectedValue+currentPremise;        
        var licTypes = component.get("v.premiseLicType");
        var licValues = component.get("v.licValues");
        licValues[licId.substr(selectedValue.length,1)] =selectedValue;        
        component.set("v.licValues",licValues);
                        
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        var premiseMap = wrapInstance.premisesList;
        var noOfPremisesMap = component.get("v.noOfPremisesMap");
        var label = licId.substr(selectedValue.length,1);
        var premise = {};
        premise.unitNumber = document.getElementById('NonGranny'+label+'pd-unitnumber').value;
        premise.streetNumber = document.getElementById('NonGranny'+label+'pd-streetnumber').value;
        premise.lotNumber = document.getElementById('NonGranny'+label+'pd-lotnumber').value;
        premise.streetName = document.getElementById('NonGranny'+label+'pd-streetname').value;
        //TODO - Make sure the picklist LIC_Type__c already has values available being passed on
        premise.licType = selectedValue;
        premise.premiseId = wrapInstance.newDevApplicantId+'Premise-'+label;
        premise.name = label*1;
        premise.btn = true;
        premiseMap[label - 1] = premise;
        noOfPremisesMap[label-1] = premise;
        component.set("v.noOfPremisesMap",noOfPremisesMap);
        wrapInstance.premisesList = premiseMap; 
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);                
        
        //Code from latest branch
        /*
        var licId = event.currentTarget.id;
        var selectedValue = event.target.value;
        var licTypes = component.get("v.premiseLicType");
        var licValues = component.get("v.licValues");
        licValues[licId.substr(selectedValue.length,1)] =selectedValue;
        component.set("v.licValues",licValues);
        for (var i=0;i<licTypes.length;i++)
        {
            if(licTypes[i].name == selectedValue)
            {
               $A.util.addClass(document.getElementById(selectedValue+licId.substr(selectedValue.length,1)+licId.substr(selectedValue.length,1)),'LICSeleted'); 
            }
            else
            {
                $A.util.removeClass(document.getElementById(licTypes[i].name+licId.substr(selectedValue.length,1)+licId.substr(selectedValue.length,1)),'LICSeleted'); 
            }
        }        
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        var premiseMap = wrapInstance.premisesList;
        var noOfPremisesMap = component.get("v.noOfPremisesMap");
        var label = licId.substr(selectedValue.length,1);
        var premise = {};
        premise.unitNumber = document.getElementById('NonGranny'+label+'pd-unitnumber').value;
        premise.streetNumber = document.getElementById('NonGranny'+label+'pd-streetnumber').value;
        premise.lotNumber = document.getElementById('NonGranny'+label+'pd-lotnumber').value;
        premise.streetName = document.getElementById('NonGranny'+label+'pd-streetname').value;
        premise.licType = selectedValue;
        premise.premiseId = wrapInstance.newDevApplicantId+'Premise-'+label;
        premise.name = label*1;
        premise.btn = true;
        premiseMap[label - 1] = premise;
        noOfPremisesMap[label-1] = premise;
        component.set("v.noOfPremisesMap",noOfPremisesMap);
        wrapInstance.premisesList = premiseMap; 
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
        */
    }
})