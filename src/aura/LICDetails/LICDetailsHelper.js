({
	validatehelper : function(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal){
        var errorflag = false;
        var num;
        var id = 'NonGranny'
       	if(fieldVal == '' || fieldVal === undefined){
            if(component.get('v.displayGPremises')=='Yes')
            {
                num='Granny';
            }
            else
            {
                num = 'NonGranny'+fieldId.substr(id.length,1);
            }
            
            if ((fieldId==num+'pd-streetnumber' || fieldId==num+'pd-lotnumber'))
            {
                if(fieldId==num+'pd-streetnumber' && document.getElementById(num+'pd-lotnumber').value == '')
                {
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;
               }
                else if(fieldId==num+'pd-lotnumber' && document.getElementById(num+'pd-streetnumber').value == '')
                {
                    $A.util.addClass(errorSpanId,'help-block-error');
                    $A.util.addClass(errorDivId,'has-error');
                    $A.util.removeClass(errorDivId,'has-success');
                    errorflag = true;
                }
                
                else
            	{
                	$A.util.removeClass(errorSpanId,'help-block-error');
                	$A.util.removeClass(errorDivId,'has-error');
                	$A.util.addClass(errorDivId,'has-success');
            	}
            }
            else
            {
            	$A.util.addClass(errorSpanId,'help-block-error');
            	$A.util.addClass(errorDivId,'has-error');
            	$A.util.removeClass(errorDivId,'has-success');
           		errorflag = true;
            }
            
        }
        else{
            $A.util.removeClass(errorSpanId,'help-block-error');
            $A.util.removeClass(errorDivId,'has-error');
            $A.util.addClass(errorDivId,'has-success');
        }
        return errorflag;
    }
})