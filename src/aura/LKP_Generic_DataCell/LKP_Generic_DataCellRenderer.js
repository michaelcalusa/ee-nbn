({
	// Your renderer method overrides go here
	 render : function(component, helper) {
        var ret = this.superRender();
        var DataRow = component.get('v.dataRow');
        var FieldName = component.get('v.fieldName');
        var outputText = component.find("outputTextId");
        var res = FieldName.split(".");        
        for (var i = 0; i < res.length; i++) {
            if(DataRow[res[i]] != null && DataRow[res[i]] != undefined)
            	DataRow = DataRow[res[i]];
            else
                DataRow = '';
        }
        outputText.set("v.value",DataRow);
        component.set("v.currentValue",DataRow);
        return ret;
    }
})