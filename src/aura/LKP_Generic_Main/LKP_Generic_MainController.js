({	
    doInit: function(component, event, helper) {
        helper.showDIV(component,'spinnerDIV');
        var whereClause = component.get("v.filterCriteria");
        var limitOfRecords = component.get("v.limitOfRecords");
        var showRecent = component.get("v.showRecentRecordsOnLoad");
        var orderBy = '';
        if(showRecent){
            if(whereClause!=''){
                whereClause += " AND ";
            }
            whereClause += " LastViewedDate !=null ";
            orderBy = " LastViewedDate DESC ";
        }else{
            helper.hideDIV(component,'recentLabel');
        }
        helper.getDataList(component, whereClause, orderBy, limitOfRecords);
    },
    doSort : function(component, event, helper) {
        var sortDir 			= component.get("v.sortOrder");
        var columnsToShow 		= component.get("v.columnsToShow"); 
        var columnHeadersToShow = component.get("v.columnHeadersToShow");
        if(sortDir == 'asc')
            sortDir = 'desc';
        else if(sortDir == 'desc')
            sortDir = 'asc';
        var sortColumnIndex = parseInt(event.currentTarget.id);
        var sortColumn = columnsToShow[sortColumnIndex];
        var sortColumnHeader = columnHeadersToShow[sortColumnIndex];
        var returnedRows = component.get("v.returnedRows");
        returnedRows = _.orderBy(returnedRows, [sortColumn],[sortDir]);
        component.set("v.returnedRows",returnedRows);
        component.set("v.sortOrder" , sortDir);
        component.set("v.sortedCol" , sortColumnHeader);
    },
    searchEvents: function(component, event, helper) {
    	console.log(event.getParams().keyCode);
        if(event.getParams().keyCode == 13){
            helper.searchAllRecords(component);
        }
   	},
    searchRecords : function(component, event, helper) {
        helper.searchAllRecords(component);
	},
    
    closeLKP : function(component, event, helper){
		component.set('v.shown',false);
    },
    showSearch : function(component, event, helper){
        var searchTxt = event.getParam('searchText');
        if(searchTxt != null && searchTxt.trim() !='')
        {
            component.set("v.searchText", searchTxt);
			helper.searchAllRecords(component);
        }
    }
    
})