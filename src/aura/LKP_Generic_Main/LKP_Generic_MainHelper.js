({
    getDataList : function(component, whereClause, orderBy, limitOfRecords) {
        var action = component.get("c.getRows");
        action.setAbortable();
        limitOfRecords = limitOfRecords+1;
        console.log('***WHERE*** '+whereClause); 
        console.log('***ORDER BY*** '+orderBy);
        console.log('***component.get(v.lkpObject)*** '+ component.get('v.lkpObject')); 
        console.log('***component.get(v.columnList)*** '+ component.get('v.columnList'));
        action.setParams({'objectName' : component.get('v.lkpObject'),
                          'selectList' : component.get('v.columnList'),
                          'whereClause' : whereClause,
                          'orderBy'	: orderBy,
                          'limitOfRecords' : limitOfRecords
                         });
        
        action.setCallback(this,
                           function(response){
                               if (response.getState() === "SUCCESS"){
                                   console.log('@@Result datalist---- ' + JSON.stringify(response.getReturnValue()));
                                   component.set('v.returnedRows',response.getReturnValue());
                                   if(response.getReturnValue().length == limitOfRecords){
                                       component.set('v.isMaxRows',true);
                                   }else
                                       component.set('v.isMaxRows',false);
                               }else{
                                   console.log('ERROR on QUERYING DATA: getDataList()');
                               }
                               this.hideDIV(component,'spinnerDIV');
                           }
                          );
        $A.enqueueAction(action);
    },	
    searchAllRecords : function(component) {
        var inpBox = component.find("inpSearchBox");
        var searchQuery = inpBox.get("v.value");
        component.set('v.isMaxRows',false);
        //if(searchQuery.length > 1){
        var filterCriteria = component.get("v.filterCriteria"); 
        var limitOfRecords = component.get("v.limitOfRecords");
        var columnToSearch = component.get("v.columnToSearch");
        var orderBy = component.get("v.orderBy"); 
        var whereClause = '';
        //inpBox.set("v.errors",null);
        component.set('v.returnedRows',[]);
        this.hideDIV(component,'recentLabel');
        this.showDIV(component,'spinnerDIV');
        if(searchQuery.length > 1){
            if(columnToSearch==''){
                whereClause = "Name like '%"+searchQuery+"%'";
            }else{
                whereClause = columnToSearch+" like '%"+searchQuery+"%'";
            }
        }
        if(filterCriteria!=''){
            if(whereClause!='')
            	whereClause += " AND ";    
            whereClause += filterCriteria;
        }
        this.getDataList(component, whereClause, orderBy, limitOfRecords);
        //}
        //else
        //    inpBox.set("v.errors",[{message:"Please enter at least 2 characters"}]);
    },
    hideDIV : function(component, divName){
        $A.util.addClass(component.find(divName),'slds-hide');
    },
    
    showDIV : function(component, divName){
        $A.util.removeClass(component.find(divName),'slds-hide');
    }
    
})