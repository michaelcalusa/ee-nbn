({
    
    // Search the location using location auto complete API
	searchLocation : function(component, event, helper) {
        var keycode = event.code;
        //var locationID = event.currentTarget.dataset.locationid;
        //ArrowUp //ArrowDown        
        //alert(keycode);
            var CurrentLocationId = component.get("v.Location");
            console.log('CurrentLocationId' + CurrentLocationId);
            if(keycode != "ArrowDown" && keycode != "ArrowUp" && keycode != "Enter"){
                //alert('test');
                component.set("v.Location", ""); 
        		component.set("v.LocationId", ""); 
        		component.set("v.LotNumber", "");  
                var usePlacesGateway = component.get("v.usePlacesGateway");
                //console.log('use places', usePlacesGateway);
                var locationTerm = document.getElementById("searchLocationId").value;
                var resultLimit = component.get("v.LocationLimit");
                var indexTypeAhead = component.get("v.LocationTypeAhead");
                //console.log('*** length'+ locationTerm);
                
                // Start the API call after 3 charaters
                if( locationTerm.length > indexTypeAhead ){
                    helper.searchLocationaHelper(component, event, locationTerm, resultLimit,usePlacesGateway);
                    //helper.test(component,event);
                }
                else{  
                    component.set("v.listOfSearchRecords", null); 
                    
                    var forclose = component.find("locationTypeAhead");
                    $A.util.addClass(forclose, 'lookup_menu_close');
                    $A.util.removeClass(forclose, 'lookup_menu_open');
                    component.set("v.ErrorMessage",'');
                    component.set("v.showError",false);
                }   
            }
        

	},
    // Select the location and location id
    selectLocation : function(component, event, helper) {
        var locationID = event.currentTarget.dataset.locationid;
        var location = event.currentTarget.dataset.formattedaddress;
        var lotNumber = event.currentTarget.dataset.lotnumber;
        
        // Set the location and location id
        component.set("v.Location", location); 
        component.set("v.LocationId", locationID); 
        component.set("v.LotNumber", lotNumber);
        
        console.log('*** LocationId ***'+ locationID);
        
        var forclose = component.find("locationTypeAhead");
        $A.util.addClass(forclose, 'lookup_menu_close');
        $A.util.removeClass(forclose, 'lookup_menu_open');
        
        var forSearch = component.find("btnLocationSearch");
        $A.util.removeClass(forSearch, 'disabled');
               
        var appEvent = $A.get("e.c:MarSingleLocationSearch");
        appEvent.fire();
        console.log('fire the app event');
        if(component.get("v.newDevsMap")){
            var locationID = component.get("v.LocationId");
            var lotNumber = component.get("v.LotNumber");
            helper.getLocationDetails(component, event, locationID, lotNumber);
        }
    },
    
    MarSingleappEvent : function(component,event,helper){
      var appEvent = $A.get("e.c:MarSingleLocationSearch");
	  appEvent.fire();
      console.log('fire the app event');
    },
    
    // Add the location using location API    
    addLocation : function(component, event, helper) {
        var locationID = component.get("v.LocationId");
        var lotNumber = component.get("v.LotNumber");
        helper.getLocationDetails(component, event, locationID, lotNumber);
    }
})