({
    keyboardControl : function(component,event){
        var liSelected = component.get("v.selectedItem");
        var index = component.get("v.TrackIndex");;
        //alert(liSelected);
        var $container = $('#locationTypeAhead');
        var list = $('ul.lookup_list li');
        
        
        $("#searchLocationId").keydown(function(e){
            var TypeAheadClass = $("#locationTypeAhead").attr('class');
            var key = e.keyCode;
            
            if(key == 40){
                var listItems = $('ul.lookup_list li');
                //var atag = $('li.lookup_bg a');
                //alert(listItems.length);
                if(liSelected) {
                    liSelected.removeClass('selected');
                    var next = liSelected.next();
                    if(next.length > 0) {
                        liSelected = next.addClass('selected');
                    } else {
                        liSelected = listItems.eq(0).addClass('selected');
                    }
                } else {
                    liSelected = listItems.eq(0).addClass('selected');
                }
                
                component.set("v.selectedItem",liSelected);
                
            }else if (key == 38){
                //alert("up");
                if(liSelected) {
                    liSelected.removeClass('selected');
                    var next = liSelected.prev();
                    if(next.length > 0) {
                        liSelected = next.addClass('selected');
                    } else {
                        liSelected = li.last().addClass('selected');
                    }
                } else {
                    liSelected = li.last().addClass('selected');
                }
                component.set("v.selectedItem",liSelected);
            }else if(key == 13){
                //alert(listItems.length);

                var listItems = $('ul.lookup_list li');
                var elements = document.getElementsByClassName("selected");
                var targeid = elements[0].id
                liSelected.removeClass('selected');
                //alert(targeid);
                var childNodeshere = document.getElementById(targeid).firstChild;
                //alert(childNodeshere.dataset.locationid);
                var location = (childNodeshere.dataset.formattedaddress);
                var locationID = childNodeshere.dataset.locationid;
                var lotNumber = childNodeshere.dataset.lotnumber;
                
                // Set the location and location id
                component.set("v.Location", location); 
                component.set("v.LocationId", locationID); 
                component.set("v.LotNumber", lotNumber);
                
                console.log('*** LocationId ***'+ locationID);
                
                var forclose = component.find("locationTypeAhead");
                $A.util.addClass(forclose, 'lookup_menu_close');
                $A.util.removeClass(forclose, 'lookup_menu_open');
                
                var forSearch = component.find("btnLocationSearch");
                $A.util.removeClass(forSearch, 'disabled');
                
                var appEvent = $A.get("e.c:MarSingleLocationSearch");
                appEvent.fire();
                console.log('fire the app event');
                
            }
            
        });
    },
    
    
	searchLocationaHelper : function(component,event,getInputkeyWord,resultLimit, usePlacesGateway) {
		var action = component.get("c.getLocations"); 
        action.setParams({
            'strLocation': getInputkeyWord,
            'intLocationLimit': resultLimit,
            'usePlacesGateway': usePlacesGateway
        }); 
		
		
        
        var forSearch = component.find("btnLocationSearch");
        $A.util.addClass(forSearch, 'disabled');
        
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('*** searchLocationaHelper state ***'+ response.getState());
            if (state === "SUCCESS") {
                
                var storeResponse = response.getReturnValue();
                //console.log('** storeResponse ==>'+ JSON.stringify(storeResponse));
                
                component.set("v.listOfSearchRecords", storeResponse.locationWrappers);
                
                //console.log('google result ', storeResponse.googleResult); 
                var lookupResultDiv = component.find("locationTypeAhead");
                if(storeResponse.googleResult == true){
                    $A.util.addClass(lookupResultDiv, 'google-maps');                	
                } else {
                	$A.util.removeClass(lookupResultDiv, 'google-maps');
                }
                //console.log('*** searchLocationaHelper length ***'+ storeResponse.locationWrappers.length);
                
                
                if(storeResponse.locationWrappers.length == 0){
                    $A.util.addClass(lookupResultDiv, 'lookup_menu_close');
                    $A.util.removeClass(lookupResultDiv, 'lookup_menu_open');
					
                    component.set("v.ErrorMessage",''); 
                    component.set("v.showError",false);
                }
                else if(storeResponse.locationWrappers.length == 1){
                    // if error or not eligible returned
                    //console.log('*** searchLocationaHelper 2 ***' + storeResponse.locationWrappers[0].strLocation + '--' + storeResponse.locationWrappers[0].strError);
                    if(storeResponse.locationWrappers[0].strError == ''){
                        $A.util.removeClass(lookupResultDiv, 'lookup_menu_close');
                        $A.util.addClass(lookupResultDiv, 'lookup_menu_open');
                        
                        //console.log('*** searchLocationaHelper 2 error ***');
                        component.set("v.ErrorMessage",''); 
                        component.set("v.showError",false); 
                    }
                    else{
                        $A.util.removeClass(lookupResultDiv, 'lookup_menu_open');
            			$A.util.addClass(lookupResultDiv, 'lookup_menu_close');
                        
                        //console.log('*** searchLocationaHelper 2 result ***');
                        component.set("v.ErrorMessage",storeResponse.locationWrappers[0].strError); 
                        component.set("v.showError",true); 
                    }
                }
                else{
            		$A.util.addClass(lookupResultDiv, 'lookup_menu_open');
            		$A.util.removeClass(lookupResultDiv, 'lookup_menu_close');
                    //console.log('*** searchLocationaHelper 3 ***');
                    component.set("v.ErrorMessage",''); 
                    component.set("v.showError",false);
                }
            } 
            else if(state === "ERROR"){
                
            }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
	},
    
    getLocationDetails : function(component,event,getLocationId,getLotNumber) {
        var action;
        if(component.get("v.newDevsMap")){
            action = component.get("c.getLocationDetailsNewDevs"); 
        }
        else{
            action = component.get("c.getLocationDetails"); 
        }
        //var action = component.get("c.getLocationDetails"); 
        action.setParams({
            'strLocationID': getLocationId,
            'strLotNumber': getLotNumber
        });
        var forSearch = component.find("btnLocationSearch");
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(' state '+state);
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
      			console.log('==storeResponse=='+JSON.stringify(storeResponse.data[0].attributes.geopoint.latitude));
                if(component.get("v.newDevsMap")){
                    var addressSelectedEvent = $A.get("e.c:addressSelectedEvent");
                    addressSelectedEvent.setParams({
                        "latitude" : storeResponse.data[0].attributes.geopoint.latitude,
                        "longitude" : storeResponse.data[0].attributes.geopoint.longitude,
                    });                       
                    addressSelectedEvent.fire();
                }
                if(storeResponse.strErrorMessage != null && storeResponse.strErrorMessage != ''){
                    console.log('*** ServiceEligibility Error***'+ storeResponse.strErrorMessage);
                    $A.util.addClass(forSearch, 'disabled');
                    component.set("v.showError",true); 
                    component.set("v.ErrorMessage",storeResponse.strErrorMessage);
                    
                    // Invoke eligibility error GA event
                    var myGAEvent = $A.get("e.c:GoogleAnalyticsEvent");
                    myGAEvent.setParams({
                        "hitType":$A.get("$Label.c.GADefaultEvent"),
                        "eventCategory":$A.get("$Label.c.GACategoryTechChoice"),
                        "eventAction":$A.get("$Label.c.GAEligibilityStart"),
                        "eventLabel":$A.get("$Label.c.GAEligibilityFailed"),
                        "eventValue":$A.get("$Label.c.GAErrorCode")
                    });                       
                    myGAEvent.fire();
                }
                else{
                    console.log('*** ServiceEligibility Technology***'+ storeResponse.strTechnology);
                    
                    //component.set("v.ServiceEligibilityData", storeResponse);
                    //component.set("v.showLocationResult",true); 
                    //component.set("v.showLocationSearch",false); 
                    component.set("v.showError",false); 
                    component.set("v.ErrorMessage",''); 
                    
                    // Set the map for events
                    var myMap = component.get("v.mapLocationDetails");
                    myMap["Technology"] = storeResponse.strTechnology; 
                    myMap["Address"] = component.get("v.Location");
                    
                    component.set('v.mapLocationDetails',myMap);
                    console.log('*** mapLocationDetails ***'+ myMap);
                    
                    component.set('v.mapLocationCapture',storeResponse.mapAddress);
                    console.log('*** mapLocationCapture ***'+ storeResponse.mapAddress);
                    
                    var myEvent = $A.get("e.c:LocationSearchInfo");                    
                    myEvent.setParams({
                        "mapLocationDetails":component.get("v.mapLocationDetails"),
                        "showLocationSearch":false,
                        "showLocationResult":true,
                        "mapLocationCaptured":component.get("v.mapLocationCapture")
                    });
                    myEvent.fire();
                    
                    console.log('*** LocationSearch Map event end***');
                    
                    // Invoke eligibility success GA event
                    var myGAEvent = $A.get("e.c:GoogleAnalyticsEvent");
                    myGAEvent.setParams({
                        "hitType":$A.get("$Label.c.GADefaultEvent"),
                        "eventCategory":$A.get("$Label.c.GACategoryTechChoice"),
                        "eventAction":$A.get("$Label.c.GAEligibilityStart"),
                        "eventLabel":$A.get("$Label.c.GAEligibilityPass"),
                        "eventValue":$A.get("$Label.c.GAEventValue")
                    });                       
                    myGAEvent.fire();
                }
            }    
        });
        $A.enqueueAction(action);
	},
})