({
    initAfterScriptsLoaded: function(component,event,helper){ 
        console.log("JS Loaded");
    }, 
    doInit : function(component,event,helper){ 
        
        helper.validateGivenNameHelperTest(component,event);
        
        var myGAEvent = $A.get("e.forceCommunity:analyticsInteraction");
        myGAEvent.setParams({
            "hitType":'event',
            "eventCategory":'SingleMAR',
            "eventAction":'Form Loading',
            "eventLabel":'Form Loading successfully',
            "eventValue":200
        }); 
        myGAEvent.fire();        
        var action= component.get("c.getStateOptions");
        action.setCallback(this,function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var stateReturnValues = response.getReturnValue();
                //console.log('Returned value key is '+stateReturnValues.key);
                var stateOptions = [];
                for (var i = 0; i < stateReturnValues.length; i++){
                    
                    for (var key in stateReturnValues[i]) {
                       // console.log('the key is '+key);                        
                       // console.log('the value is '+stateReturnValues[i][key]);
                        var stateOption = {label:key, value:stateReturnValues[i][key]};                        
                    }
                    stateOptions.push(stateOption);
                    
                }
                component.set("v.stateOptions", stateOptions);
            }
            
        });
        $A.enqueueAction(action);
        var action2= component.get("c.getRoadTypes");
        action2.setCallback(this,function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var ReturnValues = response.getReturnValue();
                //console.log('Returned value key is '+ReturnValues.key);
                var RoadTypeOptions = [];
                for (var i = 0; i < ReturnValues.length; i++){
                    
                    for (var key in ReturnValues[i]) {
                        //console.log('the key is '+key);                        
                        //console.log('the value is '+ReturnValues[i][key]);
                        var RoadTypeOption = {label:key, value:ReturnValues[i][key]};                        
                    }
                    RoadTypeOptions.push(RoadTypeOption);
                    
                }
                component.set("v.RoadTypeOptions", RoadTypeOptions);
            }
            
        });
        $A.enqueueAction(action2);
        
        var action3= component.get("c.getMedAlarmBrand");
        action3.setCallback(this,function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var ReturnValues = response.getReturnValue();
                //console.log('Returned value key is '+ReturnValues.key);
                var AlarmBrandOptions = [];
                for (var i = 0; i < ReturnValues.length; i++){
                    
                    for (var key in ReturnValues[i]) {
                        //console.log('the key is '+key);                        
                        //console.log('the value is '+ReturnValues[i][key]);
                        var BrandOption = {label:key, value:ReturnValues[i][key]};                        
                    }
                    AlarmBrandOptions.push(BrandOption);
                    
                }
                component.set("v.MedicalAlarmBrand", AlarmBrandOptions);
            }
            
        });
        $A.enqueueAction(action3);
        
        
        var action4= component.get("c.getMedAlarmCall");
        action4.setCallback(this,function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var ReturnValues = response.getReturnValue();
                //console.log('Returned value key is '+ReturnValues.key);
                var AlarmCallOptions = [];
                for (var i = 0; i < ReturnValues.length; i++){
                    
                    for (var key in ReturnValues[i]) {
                        //console.log('the key is '+key);                        
                        //console.log('the value is '+ReturnValues[i][key]);
                        var CallOption = {label:key, value:ReturnValues[i][key]};                        
                    }
                    AlarmCallOptions.push(CallOption);
                    
                }
                component.set("v.MedicalAlarmCall", AlarmCallOptions);
            }
            
        });
        $A.enqueueAction(action4);
        
        var action5= component.get("c.getHearMethod");
        action5.setCallback(this,function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var ReturnValues = response.getReturnValue();
                //log('Returned value key is '+ReturnValues.key);
                var HearOptions = [];
                for (var i = 0; i < ReturnValues.length; i++){
                    
                    for (var key in ReturnValues[i]) {
                        //console.log('the key is '+key);                        
                        //console.log('the value is '+ReturnValues[i][key]);
                        var HearOption = {label:key, value:ReturnValues[i][key]};                        
                    }
                    HearOptions.push(HearOption);
                    
                }
                component.set("v.HearMethod", HearOptions);
            }
            
        });
        $A.enqueueAction(action5);
        
        var action6= component.get("c.getMedAlarmTypes");
        action6.setCallback(this,function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var ReturnValues = response.getReturnValue();
                //console.log('Returned value key is '+ReturnValues.key);
                var MedTypeOptions = [];
                for (var i = 0; i < ReturnValues.length; i++){
                    
                    for (var key in ReturnValues[i]) {
                        //console.log('the key is '+key);                        
                        //console.log('the value is '+ReturnValues[i][key]);
                        var MedTypeOption = {label:key, value:ReturnValues[i][key]};                        
                    }
                    MedTypeOptions.push(MedTypeOption);
                    
                }
                component.set("v.MedicalAlarmType", MedTypeOptions);
            }
            
        });
        $A.enqueueAction(action6);
             
    },
    
    SetAltContact : function(component,event,helper){
        component.set("v.ifAltContact","true"); 
        helper.SectionTwoSummaryCheck(component,event);
    },
    
    SetAltContact2 : function(component,event,helper){
        component.set("v.ifAltContact","false");
        component.set("v.newMedicalAlarm.Alt_First_Name__c","");
        component.set("v.newMedicalAlarm.Alt_Last_Name__c","");
        component.set("v.newMedicalAlarm.Alt_Phone_Number__c","");
        component.set("v.newMedicalAlarm.Alt_Additional_Phone_Number__c","");
        component.set("v.newMedicalAlarm.Alt_Email_Address__c","");        
        helper.SectionTwoSummaryCheck(component,event);
    },
    
    validStateTerritory : function(component,event,helper){
		helper.validateStateHelper(component,event);       
    },
    
    validTownSuburb : function(component,event,helper){
        helper.validateTownSuburbHelper(component,event);        
    },
    
    validStreetName : function(component,event,helper){
        helper.validateStreetNameHelper(component,event);        
    },
	
    validStreetType : function(component,event,helper){
        helper.validateStreetTypeHelper(component,event);
    },
    
    validStreetNumber : function(component,event,helper){
      	helper.validateStreetNumberHelper(component,event);  
    },
    
    validUnitNumber : function(component,event,helper){
      	helper.validateUnitNumberHelper(component,event);
    },
    
    validMedAlarmType : function(component,event,helper){
        helper.validateMedAlarmTypeHelper(component,event);
    },
    
    validMedAlarmBrand : function(component,event,helper){
      	helper.validateMedAlarmBrandHelper(component,event);  
    },
    
    validOtherBrand : function(component,event,helper){
      	helper.validOtherBrandHelper(component,event);  
    },
    
    validMedAlarmCall : function(component,event,helper){
      	helper.validateMedAlarmCallHelper(component,event);  
    },
    
    validMedAlarmHear : function(component,event,helper){
      	helper.validateMedAlarmHearHelper(component,event);  
    },
    
    validGivenName : function(component,event,helper){
      	helper.validateGivenNameHelper(component,event);  
    },
    
    validFamilyName : function(component,event,helper){
        helper.validateFamilyNameHelper(component,event);
    },
    
    validPhoneNo : function(component,event,helper){
      	helper.validatePhoneNoHelper(component,event);  
    },
    
    validAdditionalPhoneNo : function(component,event,helper){
        helper.validateAdditionalPhoneHelepr(component,event);
    },
    
    validEmailAddress : function(component,event,helper){
        helper.validateEmailAddressHelper(component,event);
    },
    
    validAltContactGivenName : function(component,event,helper){
      	helper.validateAltGivenNameHelper(component,event);  
    },
    
    validAltContactFamilyName : function(component,event,helper){
      	helper.validAltContactFamilyNameHelper(component,event);  
    },
    
    validAltContactPhoneNo : function(component,event,helper){
      	helper.validAltContactPhoneNoHelper(component,event);  
    },
    
    validAltContactAdditionalPhoneNo : function(component,event,helper){
		helper.validAltContactAdditionalPhoneHelper(component,event);        
    },
    
    validAltContactEmailAddress : function(component,event,helper){
        helper.validAltContactEmailAddress(component,event);
    },
    
    validTermsAndConditions : function(component,event,helper){
      	helper.validTermsAndConsHelper(component,event);  
    },
    
    validConsent : function(component,event,helper){
      	helper.validConsentsHelper(component,event);
    },
    
    toggleIconAddressDetails : function(component,event,helper){
        
        var IconLocation = component.find("MedicalAlarmAddressOverallStatus");
        var detailLocation = component.find("MedicalAlarmAddressStatus");
        helper.togglingIconsV2(IconLocation,"#AddressDetails");
		helper.togglingIconsV2(detailLocation,"#AddressDetails"); 
        //$("#AddressDetails").collapse("hide");
        //$("#MedicalAlarmDetails").collapse("show");
    },
    
    KeyboardToggleIconAddressDetails : function(component,event,helper){
        var key = event.key;
        //alert(key);
        if(key == 'Enter'){
            //alert('pressing the enter');
            var IconLocation = component.find("MedicalAlarmAddressOverallStatus");
            var detailLocation = component.find("MedicalAlarmAddressStatus");
            helper.togglingIconsV2(IconLocation,"#AddressDetails");
            helper.togglingIconsV2(detailLocation,"#AddressDetails"); 
            $("#MedicalAlarmDetails").collapse("show");
            //$("#AddressDetails").collapse("hide");
        }
    },
    
    toggleIconMedicalAlarmDetails : function(component,event,helper){
        var IconLocation = component.find("MedicalAlarmOverallValidStatus");
        var detailLocation = component.find("MedicalAlarmDetailStatus");
        helper.togglingIconsV2(IconLocation,"#MedicalAlarmDetails");
		helper.togglingIconsV2(detailLocation,"#MedicalAlarmDetails");  
        //$("#MedicalAlarmDetails").collapse("hide");
        //$("#MedicalContactDetails").collapse("show");
    },
    
    KeyboardtoggleIconMedicalAlarmDetails : function(component,event,helper){        
        var key = event.key;
        //alert(key);
        if(key == 'Enter'){
            //alert('pressing the enter');
            var IconLocation = component.find("MedicalAlarmOverallValidStatus");
            var detailLocation = component.find("MedicalAlarmDetailStatus");
            helper.togglingIconsV2(IconLocation,"#MedicalAlarmDetails");
            helper.togglingIconsV2(detailLocation,"#MedicalAlarmDetails");  
            $("#MedicalContactDetails").collapse("show");
            //$("#MedicalAlarmDetails").collapse("hide");
        }                
    },
    
    toggleIconContactDetails : function(component,event,helper){
        var IconLocation = component.find("MedicalContactOverallValidStatus");
        var detailLocation = component.find("MedicalContactDetailStatus");
        helper.togglingIconsV2(IconLocation,"#MedicalContactDetails");
		helper.togglingIconsV2(detailLocation,"#MedicalContactDetails"); 
    },
    
    
    handleLocSearchEvent : function(component,event,helper){
      	helper.validateAddressHelper(component,event); 
    },
    
	clickCreate: function(component, event, helper) {
        var buffer = component.get("v.newMedicalAlarm.Alt_Email_Address__c");
        //alert(buffer);
        
        helper.validateAddressHelper(component,event);
        //helper.validateLocIDHelper(component,event);
        //helper.validateMedAlarmTypeHelper(component,event);
        
        helper.validateMedAlarmBrandHelper(component,event);
        helper.validateMedAlarmCallHelper(component,event); 
        
        helper.validateGivenNameHelper(component,event);
        helper.validateFamilyNameHelper(component,event);
        helper.validatePhoneNoHelper(component,event);
        helper.validateAdditionalPhoneHelepr(component,event);
        helper.validateEmailAddressHelper(component,event);
        
        helper.validateMedAlarmHearHelper(component,event);
        helper.validTermsAndConsHelper(component,event); 
        
        var ifOtherBrand = component.get("v.ifOtherBrand");
        if(ifOtherBrand == 'true'){
            helper.validOtherBrandHelper(component,event);  
        }
                
        var ifAltContact = component.get("v.ifAltContact"); 
        if(ifAltContact == 'true'){
            helper.validConsentsHelper(component,event);
            helper.validateAltGivenNameHelper(component,event);
            helper.validAltContactFamilyNameHelper(component,event);
            helper.validAltContactPhoneNoHelper(component,event); 
            helper.validAltContactAdditionalPhoneHelper(component,event); 
            helper.validAltContactEmailAddress(component,event);
        }
        
       	
                
        var ifsectionOneRdy = component.get("v.ifMedicalAlarmReady");
        var isSectionTwoRdy = component.get("v.ifContactReady");
        var ifAddressrdy = component.get("v.ifAddressReady");
        
        if((ifAddressrdy) == true){
            $("#AddressDetails").collapse("hide");
            
            if((ifsectionOneRdy) == true){
                $("#MedicalAlarmDetails").collapse("hide");
                
                if((isSectionTwoRdy) == true){
                    //console.log('section two ready');
                    $("#MedicalContactDetails").collapse("hide");           
                }else{
                    //console.log('section two not ready');   
                    $("#MedicalContactDetails").collapse("show");                
                }
            }else{
                $("#MedicalAlarmDetails").collapse("show");    
                $("#MedicalContactDetails").collapse("hide");    
                
            }
        }else{
            $("#AddressDetails").collapse("show");
            $("#MedicalContactDetails").collapse("hide");    
            $("#MedicalAlarmDetails").collapse("hide"); 
        }
        
        if(ifsectionOneRdy && isSectionTwoRdy && ifAddressrdy){
            var newMedAlarm = component.get("v.newMedicalAlarm");
            //alert('Checking value '+newMedAlarm.First_Name__c);
            //console.log('create new record');
            
            $("#ContactSectionErrorMSG").addClass("hide");
            $("#MedicalAlarmSectionErrorMSG").addClass("hide");
            $("#AddressSectionErrorMSG").addClass("hide");
            
            helper.createMedAlarm(component, newMedAlarm);
        }else{
            //$("#myModal").modal("show");
           	//alert("Please enter all the complusary fields"); 
            if(ifsectionOneRdy){
                 $("#MedicalAlarmSectionErrorMSG").addClass("hide");
            }else if(!ifsectionOneRdy){
                $("#MedicalAlarmSectionErrorMSG").removeClass("hide");
            }

            if(isSectionTwoRdy){
                $("#ContactSectionErrorMSG").addClass("hide");
            }else if(!isSectionTwoRdy){
                $("#ContactSectionErrorMSG").removeClass("hide");
            }
            
            if(ifAddressrdy){
                $("#AddressSectionErrorMSG").addClass("hide");
            }else if(!ifAddressrdy){
                $("#AddressSectionErrorMSG").removeClass("hide");
            }
        }         		
	},
    
    getAdditionalInfo : function(component,event){
        var fieldvalue = document.getElementById('AdditonalInfo').value.trim();
        //alert(fieldvalue);
        component.set("v.newMedicalAlarm.Additional_Information_Text__c",fieldvalue);
    },
    
    addCheckboxClass : function(component,event){
        //console.log('adding css class for checkbox');
        $('#has-accept-terms').addClass('testfocusCheckboxCss');  
    },
})