({
    createMedAlarm: function(component, MedAlarm) {
        var action = component.get("c.saveMedAlarm");
        action.setParams({
            "medicalAlarm": MedAlarm
        });	
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                //alert('data saved successfully');
                var result = response.getReturnValue();
                component.set("v.webformId",result);
                console.log('the id is'+result);
                
                var ErrorDiv = document.getElementById("Main-Content");
                $A.util.addClass(ErrorDiv, 'hide');
                
                window.scrollTo(0, 0);
                var ConfirmDiv = document.getElementById("Confirmation-Content");
                $A.util.removeClass(ConfirmDiv, 'hide');
                
                var myGAEvent = $A.get("e.forceCommunity:analyticsInteraction");
                myGAEvent.setParams({
                    "hitType":'event',
                    "eventCategory":'SingleMAR',
                    "eventAction":'Form submission',
                    "eventLabel":'Form Submission Successfully',
                    "eventValue":200
                });                       
                myGAEvent.fire();
				
				
				
                
            }else{
                alert('failed to save data, please enter select a specific address location');
                
                var myGAEvent = $A.get("e.forceCommunity:analyticsInteraction");
                myGAEvent.setParams({
                    "hitType":'event',
                    "eventCategory":'SingleMAR',
                    "eventAction":'Form submission',
                    "eventLabel":'Form Submission failed',
                    "eventValue":400
                });                       
                myGAEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    AddressValidate : function(component,event){
        var storedInfo = component.get("v.newMedicalAlarm");
        var SiteAddress = storedInfo["Site_Name__c"];
        var SiteLocIdPrefix = storedInfo["Location_ID__c"].substring(0,3);
        console.log('SiteLocIdPrefix is '+SiteLocIdPrefix); 
        var SplitAddress = SiteAddress.split(",");

        var ErrorDiv2 = document.getElementById("LocationSearchErrorTwo");

        
        console.log('SiteAddress is '+SiteAddress); 
        console.log('the site address split length is '+SplitAddress.length);
        var ErrorDiv = document.getElementById("LocationSearchError");        
        if(SiteAddress){
            $A.util.addClass(ErrorDiv, 'hide');
            this.UpdateMapflag(component,'site_Name','1');
            
            //if(SplitAddress.length == 3 && SplitAddress[2] == ' Australia'){    
            if(SplitAddress.length <= 3  && SiteLocIdPrefix != "LOC"){            
                //alert("no good");
                $A.util.removeClass(ErrorDiv2, 'hide');
                this.UpdateMapflag(component,'site_Name','0');            
            }else {
                $A.util.addClass(ErrorDiv2, 'hide');
                this.UpdateMapflag(component,'site_Name','1');
            }
            
        }else{
            $A.util.removeClass(ErrorDiv, 'hide');
            this.UpdateMapflag(component,'site_Name','0');
        }
    },
    
    LocIDValidate : function(component,event){
      	var storedInfo = component.get("v.newMedicalAlarm");
        var LocID = storedInfo["Location_ID__c"];
		console.log('LocId is '+LocID); 
        var ErrorDiv = document.getElementById("LocationSearchError");        
        if(LocID){
            $A.util.addClass(ErrorDiv, 'hide');
            this.UpdateMapflag(component,'LocId','1');
        }else{
            $A.util.removeClass(ErrorDiv, 'hide');
            this.UpdateMapflag(component,'LocId','0');
        }
    },
    
    validateAddressHelper :function(component,event){
        this.AddressValidate(component,event);
        this.AddressSectionSummaryCheck(component,event);
    },
    
    validateLocIDHelper : function(component,event){
      	this.LocIDValidate(component,event);
        this.SectionOneSummaryCheck(component,event);
    },
    
    validateStateHelper: function(component,event){       
        this.PicklistFieldValidate(component,"marsingle-State-error","marsingle-State",
                                  "v.newMedicalAlarm.State__c","state","stateTerritory");
    },
    
    validateStreetTypeHelper : function(component,event){
        this.PicklistFieldValidate(component,"marsingle-RoadType-error","marsingle-Road-type",
                                   "v.newMedicalAlarm.Road_Suffix__c","RoadType","RoadType");
    },
    
    validateMedAlarmTypeHelper : function(component,event){
        this.PicklistFieldValidate(component,"marsingle-medicalType-error","marsingle-Medical-Type",
                                   "v.newMedicalAlarm.MAR_Alarm_Type__c","MARType","MedicalAlarmType");
        this.SectionOneSummaryCheck(component,event);
    },
    
    validateMedAlarmBrandHelper : function(component,event){
        this.PicklistFieldValidate(component,"marsingle-medicalBrand-error","marsingle-Medical-brand",
                                   "v.newMedicalAlarm.MAR_Alarm_Brand__c","MARBrand","MedicalAlarmBrand");
    	this.SectionOneSummaryCheck(component,event);
    },
    
    validateMedAlarmCallHelper : function(component,event){
        this.PicklistFieldValidate(component,"marsingle-medicalCall-error","marsingle-Medical-call",
                                   "v.newMedicalAlarm.Who_Does_Alarm_Call__c","AlarmCall","MedicalAlarmCall");
        this.SectionOneSummaryCheck(component,event);
    },
    
    validOtherBrandHelper : function(component,event){
        this.UIInputTagValidate(component,event,'Please specify the other brand','marsingle-Otherbrand','MAROtheBrand','OtherBrand','#OtherBrandMissingMSG','v.newMedicalAlarm.MAR_Alarm_Brand_Other__c');
        this.SectionOneSummaryCheck(component,event);
    },
    
    validateMedAlarmHearHelper : function(component,event){
        this.PicklistFieldValidate(component,"marsingle-medicalHear-error","marsingle-Medical-hear",
                                   "v.newMedicalAlarm.How_did_you_hear__c","Hear","MedicalAlarmHear");
		
		this.SectionTwoSummaryCheck(component,event);        
    },
    
    
    PicklistFieldValidate : function(component,errorID,fieldID,variable,Maplabel,picklistValueID){
        //var eventID = event.target.id;
        //console.log('eventid is '+eventID);
        //var selectedvalue = event.target.value;
        var selectedvalue = document.getElementById(picklistValueID).value;
        console.log('eventValue is '+selectedvalue);
        var ErrorDiv = document.getElementById(errorID);
        
        if(selectedvalue){
            console.log('good');
            component.set(variable,selectedvalue);
            this.setFieldSuccess(fieldID,component); 
            this.UpdateMapflag(component,Maplabel,'1');
            $A.util.addClass(ErrorDiv, 'hide');
            
            if(picklistValueID == 'MedicalAlarmBrand' && selectedvalue == 'Other'){
                console.log('select other brand');
                component.set("v.ifOtherBrand",'true');
                console.log(component.get("v.ifOtherBrand"));
            }else if(picklistValueID == 'MedicalAlarmBrand' && selectedvalue != 'Other'){
                console.log('not select other brand');
                component.set("v.ifOtherBrand",'false');
                component.set("v.newMedicalAlarm.MAR_Alarm_Brand_Other__c"," ");
                console.log(component.get("v.ifOtherBrand"));
            }
        }else{
            console.log('no good');
            this.setFieldError(fieldID,component);
            this.UpdateMapflag(component,Maplabel,'0');
            $A.util.removeClass(ErrorDiv, 'hide');
        }
        
    },
    
    UpdateMapflag: function(component,key,flagvalue){
        var flagMap = component.get('v.AllFieldsFlag'); 
        //console.log('getting map successfully');
        flagMap[key] = flagvalue;
		component.set('v.AllFieldsFlag',flagMap);
		//console.log('map update finished');        
    },
    
    updateSectionTwoFlag : function(component,key,flagvalue){
        var flagMap = component.get('v.SectionTwoFieldsFlag'); 
        console.log('getting section 2 map successfully');
        flagMap[key] = flagvalue;
        component.set('v.SectionTwoFieldsFlag',flagMap);
        console.log('map2 update finished');
    },
    
    togglingIcons : function(Location){
        $A.util.toggleClass(Location, 'hide');    
	},
    
    togglingIconsV2 : function(Location, accordionLocation){
        var accordionStatus = $(accordionLocation).attr('class');  
        var accordionLocationlist = ["#AddressDetails","#MedicalAlarmDetails","#MedicalContactDetails"];
        console.log("current class is "+accordionStatus);     
        if(accordionStatus == 'collapsible-control-content collapse in'){
            $A.util.removeClass(Location, 'hide');
        }else{
            $A.util.addClass(Location, 'hide');
            $("#MedicalContactDetails").collapse("hide");
            $("#MedicalAlarmDetails").collapse("hide");
            $("#AddressDetails").collapse("hide");
        }
        
        
    },
    
    UIInputTagNameValidate : function(component,event,variableName,Targetid,mapLabel,AuraId,ErrorId, MissingId,DataAssignId){
       /* var field = component.find(AuraId);
        var fieldvalue = field.get('v.value').trim(); */
        var fieldvalue = document.getElementById(AuraId).value; 
        //alert('fieldvalue via js is'+fieldvalue);
        var letteronly = (/^[a-zA-Z\s-]*$/).test(fieldvalue);
        //alert('result is '+letteronly);
        var info = component.get("v.newMedicalAlarm.First_Name__c");
        //alert('fieldvalue itslef is '+info);
        if(fieldvalue.length>0){
            if(letteronly){
                console.log('good');
                this.setFieldSuccess(Targetid,component);
                this.UpdateMapflag(component,mapLabel,'1');                 
                $(ErrorId).addClass("hide");  
                $(MissingId).addClass("hide");
                component.set(DataAssignId,fieldvalue);
            }else{
                console.log('bad');
                this.setFieldError(Targetid,component); 
                this.UpdateMapflag(component,mapLabel,'0');               
                $(ErrorId).removeClass("hide"); 
                $(MissingId).addClass("hide");
                component.set(DataAssignId,fieldvalue);
            }
        }else{
            console.log('bad');
            this.setFieldError(Targetid,component); 
            this.UpdateMapflag(component,mapLabel,'0');
           // field.set("v.errors", [{message: 'Please enter '+variableName}]);           
            $(MissingId).removeClass("hide");
            $(ErrorId).addClass("hide");
            component.set(DataAssignId,null);
        }    
    },
    
    UIInputTagValidate : function(component,event,message,Targetid,mapLabel,AuraId,MissingId,AssignValueId){
        var fieldvalue = document.getElementById(AuraId).value.trim();
        if(fieldvalue.length>0){
            console.log('good');
            this.setFieldSuccess(Targetid,component);
            this.UpdateMapflag(component,mapLabel,'1'); 
            $(MissingId).addClass("hide");
            component.set(AssignValueId,fieldvalue);
        }else{
            console.log('bad');
            this.setFieldError(Targetid,component); 
            this.UpdateMapflag(component,mapLabel,'0');
            $(MissingId).removeClass("hide");
            component.set(AssignValueId,null);
        }      
    },
    
    UIInputTagPhoneValidate : function(component,event,message,Targetid,mapLabel,AuraId, ErrorId, MissingId,DataAssignId){
       // var field = component.find(AuraId);
       // var fieldvalue = field.get('v.value');
       //         
        var fieldvalue = document.getElementById(AuraId).value; 
        var NumberOnly;
        if(/^\d+$/.test(fieldvalue)){
            NumberOnly = true;
        }else{
            NumberOnly = false;
        }
        
        
        if(fieldvalue){
            if(fieldvalue.length == 10 && NumberOnly){
                console.log('good');
                this.setFieldSuccess(Targetid,component);
                this.UpdateMapflag(component,mapLabel,'1');
                $(ErrorId).addClass("hide");  
                $(MissingId).addClass("hide"); 
				component.set(DataAssignId,fieldvalue);                
            }else if(fieldvalue.length != 10 || !(NumberOnly)){
                console.log('not valid');
                this.setFieldError(Targetid,component);
                this.UpdateMapflag(component,mapLabel,'0');
                $(ErrorId).removeClass("hide");  
                $(MissingId).addClass("hide");  
                component.set(DataAssignId,fieldvalue); 
            }

        }else{
            if (MissingId.length != 0) {
                console.log('bad');
                this.setFieldError(Targetid,component); 
                this.UpdateMapflag(component,mapLabel,'0');
                $(MissingId).removeClass("hide"); 
                component.set(DataAssignId,fieldvalue); 
            } else {
                this.setFieldNetural(Targetid,component);
                this.UpdateMapflag(component,mapLabel,'1');
                component.set(DataAssignId,null); 
            }
            $(ErrorId).addClass("hide");  
        }
    },
    
   /* UIInputTagAltPhoneValidate : function(component,event,Targetid,mapLabel,AuraId){
        console.log('validaitng additional phone number');
        var field = component.find(AuraId);
        var fieldvalue = field.get('v.value');
        var NumberOnly;
        if(/^\d+$/.test(fieldvalue)){
            NumberOnly = true;
        }else{
            NumberOnly = false;
        }
        
        
        if(fieldvalue){
            if(fieldvalue.length == 10 && NumberOnly){
                console.log('good');
                this.setFieldSuccess(Targetid,component);
                this.UpdateMapflag(component,mapLabel,'1');
                field.set("v.errors", null);                
            }else if(fieldvalue.length != 10 || !(NumberOnly)){
                console.log('not valid');
                this.setFieldError(Targetid,component);
                this.UpdateMapflag(component,mapLabel,'0');
                field.set("v.errors", [{message:"Please enter a valid phone number"}]);  
            }
            
        }else{
            console.log('good');
            this.setFieldNetural(Targetid,component);
            this.UpdateMapflag(component,mapLabel,'1');
            field.set("v.errors", null);  
        }
    },
    */
    EmailAddressValidate : function(component,event,Targetid,mapLabel,AuraId, ErrorId, DataAssignId){
        //var field = component.find(AuraId);
        //var fieldvalue = field.get('v.value');
        var fieldvalue = document.getElementById(AuraId).value; 
        
        if(fieldvalue){           
            var atpos = fieldvalue.indexOf("@");
            var dotpos = fieldvalue.lastIndexOf(".");
            if (atpos<1 || dotpos<atpos+2 || dotpos+2>=fieldvalue.length) {
                console.log('not valid');
                this.setFieldError(Targetid,component);
                this.UpdateMapflag(component,mapLabel,'0');
               // field.set("v.errors", [{message:"Please fill in a valid email address"}]); 
  			    $(ErrorId).removeClass("hide"); 
            }else{
                this.setFieldSuccess(Targetid,component);
                this.UpdateMapflag(component,mapLabel,'1');
                 $(ErrorId).addClass("hide");                
            }
            component.set(DataAssignId,fieldvalue);
        }else{
            console.log('good');
            this.setFieldNetural(Targetid,component);
            this.UpdateMapflag(component,mapLabel,'1');
            $(ErrorId).addClass("hide");
            component.set(DataAssignId,null);
        } 
        
        //add extra logic here to clear out the error message upon submission
        //if needed
        
    },
    
    checkboxValidate : function(component,fieldId,ErrorID,mapLabel){
        
        console.log("validation checkbox");
        var checkedValue = document.getElementById(fieldId).checked;
        console.log('fieldvalue is '+checkedValue);
        var ErrorDiv = document.getElementById(ErrorID);
        
        if(checkedValue == true){
            console.log('good');
            this.UpdateMapflag(component,mapLabel,'1');
            $A.util.addClass(ErrorDiv, 'hide');            
            
        }else{
            console.log('bad');
            this.UpdateMapflag(component,mapLabel,'0');
            $A.util.removeClass(ErrorDiv, 'hide');
        }
             
    },
    
    validateFamilyNameHelper : function(component,event){
        //this.UIInputTagValidate(component,event,'Please enter your family name','marsingle-Your-FamilyName','LastName','FamilyName','#FamilyNameErrorMSG', '#FamilyNameMissingMSG');
    	this.UIInputTagNameValidate(component,event,'Family Name','marsingle-Your-FamilyName','LastName','FamilyName','#FamilyNameErrorMSG', '#FamilyNameMissingMSG','v.newMedicalAlarm.Last_Name__c');
        this.SectionTwoSummaryCheck(component,event);
    },
    
    validateGivenNameHelper : function(component,event){
        this.UIInputTagNameValidate(component,event,'Given Name','marsingle-Your-GivenName','FirstName','GivenName','#GivenNameErrorMSG', '#GivenNameMissingMSG','v.newMedicalAlarm.First_Name__c');
    	this.SectionTwoSummaryCheck(component,event);
    },
    
    validateGivenNameHelperTest : function(component,event){
        //this.UIInputTagValidate(component,event,'','marsingle-Your-GivenName','FirstName','GivenName');
      //  var field = component.find('GivenName');
       // field.set('v.aria-describedby', "165:0");
        
      //  var expnameElm = component.find('GivenName').getElement();
      //  var expnameElm = component.find('GivenName')
      //  alert (expnameElm.getElement());
      //  expnameElm.getElementsByTagName('input')[0].setAttribute('aria-describedby', "165:0");
       
    },
    
   /* validateTownSuburbHelper : function(component,event){
        this.UIInputTagValidate(component,event,'Please enter town and suburb','marsingle-Town','Suburb','TownSuburb');
    },
    
    validateStreetNameHelper : function(component,event){
      	this.UIInputTagValidate(component,event,'Please enter street name','marsingle-Street-Name','RoadName','StreetName');
    },
    
    validateStreetNumberHelper : function(component,event){
      	this.UIInputTagValidate(component,event,'Please enter a street number','marsingle-streetNumber','RoadNumber','streetNumber');  
    },
    
    validateUnitNumberHelper : function(component,event){
        this.UIInputTagValidate(component,event,'Please enter a unit number','marsingle-UnitNumber','UnitNo','UnitNumber');  
    },
    */
    
    validatePhoneNoHelper : function(component,event){
        this.UIInputTagPhoneValidate(component,event,'Please enter your phone number','marsingle-Your-Phone','PhoneNo','PhoneNo','#PhoneNoErrorMSG','#PhoneNoMissingMSG','v.newMedicalAlarm.Phone_Number__c');  
    	this.SectionTwoSummaryCheck(component,event);
    },
    
    validateAdditionalPhoneHelepr : function(component,event){
      //	this.UIInputTagAltPhoneValidate(component,event,'marsingle-Your-AdditionalPhone','AdditionalPhone','AdditionalPhoneNo','','#AdditionalPhoneNoErrorMSG');
        this.UIInputTagPhoneValidate(component,event,'Please enter your additional phone number', 'marsingle-Your-AdditionalPhone','AdditionalPhone','AdditionalPhoneNo','#AdditionalPhoneNoErrorMSG','','v.newMedicalAlarm.Additional_Phone_Number__c');
    	this.SectionTwoSummaryCheck(component,event);
    },
    
    validateEmailAddressHelper : function(component,event){
      	this.EmailAddressValidate(component,event,'marsingle-Your-EmailAddress','Email','EmailAddress','#EmailAddressErrorMSG','v.newMedicalAlarm.Email_Address__c');
    	this.SectionTwoSummaryCheck(component,event);
    },
    
    validateAltGivenNameHelper : function(component,event){
        this.UIInputTagNameValidate(component,event,'alt contact Given Name','marsingle-AltContact-GivenName','AltFirstName','AltContactGivenName','#AltContactGivenNameErrorMSG', '#AltContactGivenNameMissingMSG','v.newMedicalAlarm.Alt_First_Name__c');        
        this.SectionTwoSummaryCheck(component,event);
    },
    
    validAltContactFamilyNameHelper : function(component,event){
        this.UIInputTagNameValidate(component,event,'alt contact Family Name','marsingle-AltContact-FamilyName','AltLastName','AltContactFamilyName','#AltContactFamilyNameErrorMSG', '#AltContactFamilyNameMissingMSG','v.newMedicalAlarm.Alt_Last_Name__c'); 
        this.SectionTwoSummaryCheck(component,event);
    },
    
    validAltContactPhoneNoHelper : function(component,event){
      	this.UIInputTagPhoneValidate(component,event,'Please fill in the phone number','marsingle-AltContact-Phone','AltPhone','AltContactPhoneNo','#AltContactPhoneNoErrorMSG','#AltContactPhoneNoMissingMSG','v.newMedicalAlarm.Alt_Phone_Number__c');   
        this.SectionTwoSummaryCheck(component,event);

    },
    
    validAltContactAdditionalPhoneHelper : function(component,event){
       // this.UIInputTagAltPhoneValidate(component,event,'marsingle-AltContact-AdditionalPhone','AltAdditionalPhone','AltContactAdditionalPhoneNo','AltContactAdditionalPhoneNoErrorMSG','');
        this.UIInputTagPhoneValidate(component,event,'Please fill in the Additional phone number','marsingle-AltContact-AdditionalPhone','AltAdditionalPhone','AltContactAdditionalPhoneNo','#AltContactAdditionalPhoneNoErrorMSG','','v.newMedicalAlarm.Alt_Additional_Phone_Number__c');
        this.SectionTwoSummaryCheck(component,event);
    },
    
    validAltContactEmailAddress : function(component,event){
        this.EmailAddressValidate(component,event,'marsingle-AltContact-EmailAddress','AltEmail','AltContactEmailAddress','#AltContactEmailAddressErrorMSG','v.newMedicalAlarm.Alt_Email_Address__c');
        this.SectionTwoSummaryCheck(component,event);
    },
    
    validTermsAndConsHelper : function(component,event){
        this.checkboxValidate(component,'has-accept-terms','marsingle-CheckedTerm-error','CheckedTerm');
        this.SectionTwoSummaryCheck(component,event);
    },
    
    validConsentsHelper : function(component,event){
      	this.checkboxValidate(component,'has-consent','marsingle-hasConsent-error','checkedConsent'); 
        this.SectionTwoSummaryCheck(component,event);
    },
    

    
    setFieldError : function(formGroupDivAuraId, component){ 
        var formGroupDiv = component.find(formGroupDivAuraId);
        $A.util.removeClass(formGroupDiv, 'has-success');
        $A.util.addClass(formGroupDiv, 'has-error');              
    },
    
    setFieldNetural : function(formGroupDivAuraId, component){
        var formGroupDiv = component.find(formGroupDivAuraId);
        $A.util.removeClass(formGroupDiv, 'has-success');
        $A.util.removeClass(formGroupDiv, 'has-error');  
    },
    
    setFieldSuccess : function(formGroupDivAuraId, component){
        console.log('before setfieldsuccess')
        var formGroupDiv = component.find(formGroupDivAuraId);
        console.log('setfieldsuccess');
        $A.util.removeClass(formGroupDiv, 'has-error');
        $A.util.addClass(formGroupDiv, 'has-success');       
    },
    
    AddressSectionSummaryCheck : function(component,event){
        var AllFlags = component.get("v.AllFieldsFlag");
        var IconLocation = component.find("MedicalAlarmAddressOverallStatus");        
        var addressSummary = component.get("v.newMedicalAlarm.Site_Name__c");        
        var sectionOneList = [];
        var AddressReady = AllFlags['site_Name'];
        sectionOneList.push(AddressReady);
        
        var result = 1;
        for(var index =0;index < sectionOneList.length;index++){
            result = result * sectionOneList[index];
            if(sectionOneList[index]=='0'){
                component.set("v.ifAddressReady",false);
                $A.util.removeClass(IconLocation,'fc-tick-circle');              
                $A.util.addClass(IconLocation,'fc-exclamation-circle');
                component.set("v.overallSummaryAddress",null);
            }            
        }
        
        if(result == 1){
            component.set("v.ifAddressReady",true);   
            $A.util.addClass(IconLocation,'fc-tick-circle');
            $A.util.removeClass(IconLocation,'fc-exclamation-circle');
            component.set("v.overallSummaryAddress",addressSummary);
            
            //remove the address section upon submission error message if there is any
            $('#AddressSectionErrorMSG').addClass('hide');
            
        }
        
        console.log('if the address is ready '+component.get("v.ifAddressReady"));
        
    },
    
    SectionOneSummaryCheck : function(component,event){
        var AllFlags = component.get("v.AllFieldsFlag");
        var IconLocation = component.find("MedicalAlarmOverallValidStatus");
        var alarmSummary = component.get("v.newMedicalAlarm.MAR_Alarm_Brand__c");        
        
        var sectionOneList = [];
        //var AddressReady = AllFlags['site_Name'];
        //var MARTypeReady = AllFlags['MARType'];
        var AlarmCallReady = AllFlags['AlarmCall'];
        var MARBrandReady = AllFlags['MARBrand'];
        //sectionOneList.push(AddressReady);
        //sectionOneList.push(MARTypeReady);
        sectionOneList.push(AlarmCallReady);
        sectionOneList.push(MARBrandReady);
        
        var checkOtherBrandFlag = component.get("v.ifOtherBrand");
        if(checkOtherBrandFlag == 'true'){
            var otherBrandReady = AllFlags['MAROtheBrand'];
            sectionOneList.push(otherBrandReady);
        }
        
        
        var result = 1;
        for(var index =0;index < sectionOneList.length;index++){
            result = result * sectionOneList[index];
            if(sectionOneList[index]=='0'){
                component.set("v.ifMedicalAlarmReady",false);
                $A.util.removeClass(IconLocation,'fc-tick-circle');
                $A.util.addClass(IconLocation,'fc-exclamation-circle');
                component.set("v.overallSummaryMedicaAlarm",null)
            }            
        }
        
        if(result == 1){
            component.set("v.ifMedicalAlarmReady",true);   
            $A.util.addClass(IconLocation,'fc-tick-circle');
            $A.util.removeClass(IconLocation,'fc-exclamation-circle');
            //Add related Icons
            component.set("v.overallSummaryMedicaAlarm",alarmSummary);
            $('#MedicalAlarmSectionErrorMSG').addClass('hide');
        }
        
        console.log(component.get("v.ifMedicalAlarmReady"));
  
    },
    
    SectionTwoSummaryCheck : function(component,event){        
        var AllFlags = component.get("v.AllFieldsFlag");
        var ifAltContact = component.get("v.ifAltContact");
        var IconLocation = component.find("MedicalContactOverallValidStatus");
        
        var contactSummary = component.get("v.newMedicalAlarm.First_Name__c") + ' ' + component.get("v.newMedicalAlarm.Last_Name__c"); 
        
        var sectionTwoList = [];
        var FirstNameReady = AllFlags['FirstName'];
        var LastNameReady = AllFlags['LastName'];
        var PhoneNoReady = 1; // AllFlags['PhoneNo'];
        var EmailReady = 1; // AllFlags['Email'];
        var AdditionalPhoneReady = 1; // AllFlags['AdditionalPhone'];
        var HearReady = AllFlags['Hear'];
        var CheckedTermReady = AllFlags['CheckedTerm'];
        
        if(ifAltContact == 'true'){
           // console.log('there is a alt contact');
            var AltFirstNameReady = AllFlags['AltFirstName'];
            var AltLastNameReady = AllFlags['AltLastName'];
            var AltPhoneReady = 1; // AllFlags['AltPhone'];
            var AltEmailReady = 1; // AllFlags['AltEmail'];
            var checkedConsentReady = AllFlags['checkedConsent'];
            var AltAdditionalPhoneReady = 1; // AllFlags['AltAdditionalPhone'];
        }else{
           // console.log('there is no alt contact');
            var AltFirstNameReady = 1;
            var AltLastNameReady = 1;
            var AltPhoneReady = 1;
            var AltEmailReady = 1;
            var AltAdditionalPhoneReady = 1;
            var checkedConsentReady = 1;
        }
        
        sectionTwoList.push(FirstNameReady);
        sectionTwoList.push(LastNameReady);
        sectionTwoList.push(PhoneNoReady);
        sectionTwoList.push(EmailReady);
        sectionTwoList.push(AdditionalPhoneReady);
        sectionTwoList.push(checkedConsentReady);
        sectionTwoList.push(HearReady);
        sectionTwoList.push(CheckedTermReady);        
        sectionTwoList.push(AltFirstNameReady);
        sectionTwoList.push(AltLastNameReady);
        sectionTwoList.push(AltPhoneReady);
        sectionTwoList.push(AltEmailReady);
        sectionTwoList.push(AltAdditionalPhoneReady);
        
        var result = 1;
        for(var index =0;index < sectionTwoList.length;index++){
           // console.log(sectionTwoList[index]);
            result = result * sectionTwoList[index];
            if(sectionTwoList[index]=='0'){
                component.set("v.ifContactReady",false);
                $A.util.removeClass(IconLocation,'fc-tick-circle');
                $A.util.addClass(IconLocation,'fc-exclamation-circle');
                component.set("v.overallSummaryContact",contactSummary);
                //add related Icons
            }            
        }
        
        if(result == 1){
            component.set("v.ifContactReady",true);   
            $A.util.addClass(IconLocation,'fc-tick-circle');
            $A.util.removeClass(IconLocation,'fc-exclamation-circle');
            component.set("v.overallSummaryContact",contactSummary);
            $('#ContactSectionErrorMSG').addClass('hide');
        }
        
        console.log(component.get("v.ifContactReady"));
        
    },
    
    
})