({
    // Your renderer method overrides go here
    afterRender: function (component, helper) {
        this.superAfterRender();
        // interact with the DOM here
        console.log('starting ajax request');
        
        var ajax = new XMLHttpRequest();
        ajax.open("GET",$A.get('$Resource.MarFormRefreshSvgIcon'),true);
        ajax.responseType = "document";
        console.log('get ready');
        ajax.onload = function(e) {
            try {
                console.log(ajax.readyState);
                var svg = ajax.responseXML.documentElement;
                console.log('svg doc element is '+svg);
                svg.setAttribute("class", "hidden");
                
                var container = component.find("svgcontainer").getElement();
                container.innerHTML = svg.outerHTML; 
            }
            catch(e){console.log(e);}
        }
        ajax.send();
        
        
        /*
        var svg = $A.get('$Resource.MarFormRefreshSvgIcon');
        console.log('svg doc element is '+svg);
        svg.setAttribute("class", "hidden");
        
        
        var container = component.find("svgcontainer").getElement();
        container.innerHTML = svg.outerHTML;    
        */
        
        
        console.log('after render funtion');
        var svgns = "http://www.w3.org/2000/svg";
        var xlinkns = "http://www.w3.org/1999/xlink";
        
        //#Blog Icon
        var svgroot = document.createElementNS(svgns, "svg");
        svgroot.setAttribute("class", 'svg-icon');
        
        var shape = document.createElementNS(svgns, "use");
        shape.setAttributeNS(xlinkns, "xlink:href", "#blog");
        svgroot.appendChild(shape);
        var container = component.find("blogSvg").getElement();
        container.insertBefore(svgroot, container.firstChild);
        
        //#Facebook Icon
        var svgroot = document.createElementNS(svgns, "svg");
        svgroot.setAttribute("class", 'svg-icon');
        
        var shape = document.createElementNS(svgns, "use");
        shape.setAttributeNS(xlinkns, "xlink:href", "#facebook");
        svgroot.appendChild(shape);
        var container = component.find("FacebookSvg").getElement();
        container.insertBefore(svgroot, container.firstChild);
        
        //#Twitter Icon
        var svgroot = document.createElementNS(svgns, "svg");
        svgroot.setAttribute("class", 'svg-icon');
        
        var shape = document.createElementNS(svgns, "use");
        shape.setAttributeNS(xlinkns, "xlink:href", "#twitter");
        svgroot.appendChild(shape);
        var container = component.find("TwitterSvg").getElement();
        container.insertBefore(svgroot, container.firstChild);
        
        //#Youtube Icon
        var svgroot = document.createElementNS(svgns, "svg");
        svgroot.setAttribute("class", 'svg-icon');
        
        var shape = document.createElementNS(svgns, "use");
        shape.setAttributeNS(xlinkns, "xlink:href", "#youtube");
        svgroot.appendChild(shape);
        var container = component.find("YoutubeSvg").getElement();
        container.insertBefore(svgroot, container.firstChild);
        
        //#Youtube Icon
        var svgroot = document.createElementNS(svgns, "svg");
        svgroot.setAttribute("class", 'svg-icon');
        
        var shape = document.createElementNS(svgns, "use");
        shape.setAttributeNS(xlinkns, "xlink:href", "#linkedin");
        svgroot.appendChild(shape);
        var container = component.find("LinkedInSvg").getElement();
        container.insertBefore(svgroot, container.firstChild);
        
    }, 
    
})