({
    Cancel : function(component, event, helper){
        var closewindow = $A.get("e.force:closeQuickAction");
        closewindow.fire();
    },
    
    ValidateInputParms: function(component, event, helper){
        var dataRecs = component.get("v.dataRows");
        var noErrors = true;
        var noOfRecSelected;
        var recsSelected;
        var noRecs = true;
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        if(component.get("v.objectRec") === 'Opportunity'){  
            noOfRecSelected = component.find("milestoneSelected").length;
            recsSelected =  component.find("milestoneSelected");
        }
        else{
            noOfRecSelected = component.find("milestoneSelected_mp").length;
            recsSelected =  component.find("milestoneSelected_mp");            
        }
        console.log(noOfRecSelected);
        console.log(recsSelected);       
        for( var i=0; i < noOfRecSelected; i++) {
            if(  recsSelected[i].get("v.value") == true ){
                noRecs = false;
                var milestSelected = component.find("mp_validationErr");
                var closeField = component.find("closeDate");
                var startDField = component.find("startDate");
                var ExpCompDField = component.find("expCompDate");
                //var mileStatus = component.find("Status");
                var ClDateErrorDiv = component.find("error");
                var ClDateError1Div = component.find("error1");
                var ClDateError2Div = component.find("error2");
                var StDateErrorDiv = component.find("error3");
                var StDateError1Div = component.find("error4");
                var StDateError2Div = component.find("error6");
                var ExpCompDateErrorDiv = component.find("error5");
                if(dataRecs[i].Close_Date__c > today){
                    /*Clear previous errors */
                    $A.util.addClass(ClDateError2Div[i],"slds-hide");	
                    $A.util.addClass(ClDateErrorDiv[i],"slds-hide");
                    $A.util.addClass(StDateError1Div[i],"slds-hide");
                    $A.util.addClass(StDateErrorDiv[i],"slds-hide");
                    $A.util.addClass(ExpCompDateErrorDiv[i],"slds-hide");
                    $A.util.addClass(StDateError2Div[i],"slds-hide");
                    /* Highlight the current error */
                    $A.util.removeClass(ClDateError1Div[i],"slds-hide");	
                    $A.util.addClass(ClDateError1Div[i],"slds-show");																
                    $A.util.addClass(closeField[i], 'slds-has-error');
                    noErrors = false;                        
                }
                else if(!dataRecs[i].Start_Date__c){
                    /*Clear previous errors */
                    $A.util.addClass(StDateError1Div[i],"slds-hide");
                    $A.util.addClass(ClDateError1Div[i],"slds-hide");	
                    $A.util.addClass(ClDateErrorDiv[i],"slds-hide");
                    $A.util.addClass(ClDateError2Div[i],"slds-hide");
                    $A.util.addClass(ExpCompDateErrorDiv[i],"slds-hide");
                    $A.util.addClass(StDateError2Div[i],"slds-hide");
                    /* Highlight the current error */
                    $A.util.removeClass(StDateErrorDiv[i],"slds-hide");	
                    $A.util.addClass(StDateErrorDiv[i],"slds-show");																
                    $A.util.addClass(startDField[i], 'slds-has-error');
                    noErrors = false; 
                }
                    else if(!dataRecs[i].Is_Completed__c && (dataRecs[i].Start_Date__c > dataRecs[i].Close_Date__c)){
                        /*Clear previous errors */
                        $A.util.addClass(StDateErrorDiv[i],"slds-hide");
                        $A.util.addClass(ClDateError1Div[i],"slds-hide");	
                        $A.util.addClass(ClDateErrorDiv[i],"slds-hide");
                        $A.util.addClass(ClDateError2Div[i],"slds-hide");
                        $A.util.addClass(ExpCompDateErrorDiv[i],"slds-hide");
                        $A.util.addClass(StDateError2Div[i],"slds-hide");
                        /* Highlight the current error */
                        $A.util.removeClass(StDateError1Div[i],"slds-hide");	
                        $A.util.addClass(StDateError1Div[i],"slds-show");																
                        $A.util.addClass(startDField[i], 'slds-has-error');
                        noErrors = false; 
                    }
                    else if(!dataRecs[i].Is_Completed__c && (dataRecs[i].Status__c === 'Completed' && dataRecs[i].Start_Date__c > today)){
                        /*Clear previous errors */
                        $A.util.addClass(StDateErrorDiv[i],"slds-hide");
                        $A.util.addClass(ClDateError1Div[i],"slds-hide");	
                        $A.util.addClass(ClDateErrorDiv[i],"slds-hide");
                        $A.util.addClass(ClDateError2Div[i],"slds-hide");
                        $A.util.addClass(ExpCompDateErrorDiv[i],"slds-hide");
                        /* Highlight the current error */
                        $A.util.removeClass(StDateError2Div[i],"slds-hide");	
                        $A.util.addClass(StDateError2Div[i],"slds-show");																
                        $A.util.addClass(startDField[i], 'slds-has-error');
                        noErrors = false; 
                    }                
                        else if(!dataRecs[i].Is_Completed__c && dataRecs[i].Expected_Completion_Date__c < dataRecs[i].Start_Date__c){
                            /*Clear previous errors */
                            $A.util.addClass(StDateError1Div[i],"slds-hide");
                            $A.util.addClass(StDateErrorDiv[i],"slds-hide");
                            $A.util.addClass(ClDateError1Div[i],"slds-hide");	
                            $A.util.addClass(ClDateErrorDiv[i],"slds-hide");
                            $A.util.addClass(ClDateError2Div[i],"slds-hide");
                            $A.util.addClass(StDateError2Div[i],"slds-hide");
                            /* Highlight the current error */
                            $A.util.removeClass(ExpCompDateErrorDiv[i],"slds-hide");	
                            $A.util.addClass(ExpCompDateErrorDiv[i],"slds-show");																
                            $A.util.addClass(ExpCompDField[i], 'slds-has-error');
                            noErrors = false; 
                        }
                            else{
                                //Clear all the errors 
                                $A.util.addClass(StDateError1Div[i],"slds-hide");
                                $A.util.addClass(StDateErrorDiv[i],"slds-hide");
                                $A.util.addClass(ClDateError1Div[i],"slds-hide");	
                                $A.util.addClass(ClDateErrorDiv[i],"slds-hide");
                                $A.util.addClass(ClDateError2Div[i],"slds-hide");
                                $A.util.addClass(ExpCompDateErrorDiv[i], "slds-hide");
                                $A.util.addClass(StDateError2Div[i],"slds-hide");
                            }                
            }
        }
        if(component.get("v.objectRec") === 'Opportunity'){
            if(noRecs == true){
                component.set("v.error", "Please select atleast one Milestone to create a Milestone Plan");
            }
        }else{noRecs = false;}
        if(noErrors == true && noRecs==false){
            this.Submit(component, event, helper);}
    },
    Submit: function(component, event, helper){
        var recordId = component.get("v.oppId");
        var dataRecs = component.get("v.dataRows");
        var selectedRecs = [];
        var successMessage;
        
        if(component.get("v.objectRec") === 'Opportunity'){  
            successMessage = "Milestone Plan and its Milestones were created successfully.";
            for( var i=0; i < component.find("milestoneSelected").length; i++) {
                if( component.find("milestoneSelected")[i].get("v.value") == true ){
                    if(dataRecs[i].OwnerId.Id != null){
                        dataRecs[i].OwnerId = dataRecs[i].OwnerId.Id;}
                    selectedRecs.push(dataRecs[i]);}
            }
            
            var action = component.get("c.createMilestoneRecs"); 
            action.setParams({
                opportunityId : recordId,
                msPlanName: component.get("v.msPlanName"),
                milestoneDataLst: JSON.stringify(selectedRecs)
            });}
        else{
            successMessage = "Milestones were created/updated successfully."
            for( var i=0; i < component.find("milestoneSelected_mp").length; i++) {
                if( component.find("milestoneSelected_mp")[i].get("v.value") == true ){
                    /* Setting default Q to the record. It is set in the apex controller for new records */
                    if(dataRecs[i].OwnerId.Id != null){
                        dataRecs[i].OwnerId = dataRecs[i].OwnerId.Id;}

                        selectedRecs.push(dataRecs[i]);
                } 
                else{
                    if(dataRecs[i].Id != null) {
                        dataRecs[i].Status__c = 'Not Required'; 
                        selectedRecs.push(dataRecs[i]);
                    }
                }
            }
            var action = component.get("c.updateMilestoneRecs");            
            action.setParams({
                msPlanId : recordId,
                milestoneDataLst: JSON.stringify(selectedRecs)
            });            
        }
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                console.log('In Success');
                console.log(response.getReturnValue);
                
                var closewindow = $A.get("e.force:closeQuickAction");
                closewindow.fire();
                
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "message": successMessage,
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            }
            else if(state === "ERROR") {
                var errors = action.getError();
                if(errors){
                    if (errors[0] && errors[0].message) {
                        component.set("v.error", "An unknown error has occured. Please contact your System Administrator.");
                        console.log(errors);   
                        console.log(errors[0]); 
                        console.log(errors[0].message); 
                    }}
            }
            
        });
        $A.enqueueAction(action);
    },
    
})