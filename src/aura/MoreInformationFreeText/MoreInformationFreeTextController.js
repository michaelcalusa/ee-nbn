({
	openActionWindow : function(component, event, helper) {
        var buttonstate = component.get("v.buttonstate");
        component.set("v.buttonstate", !buttonstate);
		var urlEvent = $A.get("e.force:navigateToURL");
        var url = component.get("v.col3");
    	urlEvent.setParams({
      		"url": url
    	});
    		urlEvent.fire();
		}
})