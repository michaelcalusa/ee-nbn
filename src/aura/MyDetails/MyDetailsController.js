({
	myAction : function(component, event, helper) {
        component.set("v.selectedRadio",'Developer');
        var action1 = component.get("c.roleOptions");
        action1.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                var roleList = response.getReturnValue();
                component.set("v.roleList",roleList);
                var roleMap = [];
                for(var i = 0; i < roleList.length; i++){
                    var currRole = roleList[i];
                     if(roleList[i] == 'Developer'){
                        roleMap.push({"name" : "Developer", "showABN" : "true" ,"desc"  : "A person or company that builds new or renovated existing, properties either itself or using a Builder."});
                    }
                    else if(roleList[i] == 'Builder'){
                        roleMap.push({"name" : "Builder", "showABN" : "true", "desc"  : "A person, other than the Developer, who is contracted to build a new property or renovate an existing property for another person/company/entity."});
                    }
                    else if(roleList[i] == 'Authorised representative of a developer or builder'){
                        roleMap.push({"name" : "Authorised representative of a developer or builder", "showABN" : "true", "desc"  : "A person who has been given authority by the Developer/Builder to submit a request on their behalf."});
                    }
                    else if(roleList[i] == 'Owner builder'){
                        roleMap.push({"name" : "Owner builder", "showABN" : "true", "desc"  : "A person who both owns the property and is also directly responsible for the building work on the property."});
                    }
                    else if(roleList[i] == 'All Other requestors'){
                        roleMap.push({"name" : "All Other requestors", "showABN" : "false" , "desc" : "For example, an owner-occupier, landlord, tenant, property manager, strata manager or body corporate of any premises (home or business)."});
                    }
                }
                component.set("v.roleDescMap",roleMap);
            }
        });
        $A.enqueueAction(action1);
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        wrapInstance.yourDtlWrapper.workWithNBN = false;
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
        
        if(component.get("v.workWithnbn")){
            $A.util.addClass(document.getElementById('cpd-yes-toggleButtonId'),'btn-primary');
            $A.util.removeClass(document.getElementById('cpd-no-toggleButtonId'),'btn-primary');
        }
        else{
            $A.util.removeClass(document.getElementById('cpd-yes-toggleButtonId'),'btn-primary');
            $A.util.addClass(document.getElementById('cpd-no-toggleButtonId'),'btn-primary');
        }
	},
    handleCollapsableSection : function(component, event, helper){
         if(!component.get("v.disableSection")){
        component.set("v.displaySection",!(component.get("v.displaySection")));
        if(component.get("v.displaySection")){
            var disEvent = component.getEvent("displaySectionChange");
            disEvent.setParams({
                "firedComponentName" : 'myDetails'
            });
            disEvent.fire();
        }
      }
    },
    processCustomerPersonalDetails : function(component, event, helper){
        var div = document.getElementById("cpd-containerId");
        var subDiv = div.getElementsByTagName('input');
        var myArray = [];
        for(var i = 0; i < subDiv.length; i++) {
            var elem = subDiv[i];
            if(elem.id.indexOf('cpd-') === 0) {
                myArray.push(elem.id);
            }
        }
        var errorCarier = [];
        for(var inputElementId=0; inputElementId<myArray.length; inputElementId++){
            var errorDivId = document.getElementById(myArray[inputElementId]+'-div');
            var errorSpanId = document.getElementById(myArray[inputElementId]+'-error'); 
            var fieldId = myArray[inputElementId];      
            var fieldVal = document.getElementById(fieldId).value;
            var abnNumber = component.get("v.abnNumber");
            var abnName = component.get("v.RegisteredEntityName");
            var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
            var applicantHasABN = wrapInstance.yourDtlWrapper.applicantDoYouHaveABN;
            if (abnNumber != null && applicantHasABN != false) {
                var abnerr = helper.validatehelperABNname(component, event, helper, abnNumber, abnName);
                errorCarier.push(abnerr);
            }
            var err = helper.validatehelper(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);
            errorCarier.push(err);
        }
        component.set('v.hasError', false);
        for(var i = 0; i < errorCarier.length; i++){
            if(errorCarier[i] == true){
                component.set('v.hasError', true);
            } 
        }
        console.log(' lov '+component.get("v.hasError"));
        if(!component.get("v.hasError")){
        	var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
            wrapInstance.yourDtlWrapper.role = component.get("v.selectedRadio");
            wrapInstance.yourDtlWrapper.givenName = document.getElementById('cpd-givenName').value;
            wrapInstance.yourDtlWrapper.surName = document.getElementById('cpd-surName').value;
            wrapInstance.yourDtlWrapper.emailAddress = document.getElementById('cpd-emailAddress').value;
            wrapInstance.yourDtlWrapper.phoneNumber = document.getElementById('cpd-phoneNumber').value;
            wrapInstance.yourDtlWrapper.additionalPhoneNumber = document.getElementById('cpd-additionalPhoneNumber').value;
             // save applicant abn details here
            if(component.get("v.showABN")==true){
                wrapInstance.yourDtlWrapper.applicantABN = component.get("v.abnNumber");
                wrapInstance.yourDtlWrapper.applicantEntityType = component.get("v.entityTypeCode");
                wrapInstance.yourDtlWrapper.applicantBusinessName = component.get("v.RegisteredEntityName");
                wrapInstance.yourDtlWrapper.applicantDoYouHaveABN = component.get("v.showABN");                
            }
            component.set("v.newDevApplicantWrapperInstance",wrapInstance);
            var action1 = component.get("c.saveNewApplicant");
            action1.setParams({
                "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance"))
            });
            
            action1.setCallback(this, function(response){
                if(response.getState() == 'SUCCESS'){
                    var wrapInstanceTemp = component.get("v.newDevApplicantWrapperInstance");
                    wrapInstanceTemp.newDevApplicantId = response.getReturnValue();
                    component.set("v.newDevApplicantWrapperInstance",wrapInstanceTemp);
                    var saveEvent = component.getEvent("saveRecEvent");
                    saveEvent.setParams({
                        "wrapperInstance" : component.get("v.newDevApplicantWrapperInstance"),
                        "componentSaved" : 'myDetails'
                    });
                    saveEvent.fire();
                }
                else{
                    console.log(response.getState()+' message is '+response.getError()[0].message);
                }
            });
            $A.enqueueAction(action1);    
        }
        else{
            console.log('caught errors');
        }
        
        
    },
    validateFields : function(component, event, helper){
		var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);       
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;
        
        var fieldId = event.currentTarget.id;       
        var fieldVal = document.getElementById(fieldId).value; 
        helper.validatehelper(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);
    },
    
    toggleButton : function(component, event, helper){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        var buttonId = event.currentTarget.id;
        if(buttonId == 'cpd-yes-toggleButtonId'){
            wrapInstance.yourDtlWrapper.workWithNBN = true;
            $A.util.addClass(document.getElementById('cpd-yes-toggleButtonId'),'btn-primary');
            $A.util.removeClass(document.getElementById('cpd-no-toggleButtonId'),'btn-primary');
            component.set("v.workWithnbn",true);
        }
        else if(buttonId == 'cpd-no-toggleButtonId'){
            $A.util.addClass(document.getElementById('cpd-no-toggleButtonId'),'btn-primary');
            $A.util.removeClass(document.getElementById('cpd-yes-toggleButtonId'),'btn-primary');
            wrapInstance.yourDtlWrapper.workWithNBN = false;
            component.set("v.workWithnbn",false);
        }
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
    },
    toggleABNButton : function(component, event, helper){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        var buttonId = event.currentTarget.id;
        
        if(buttonId == 'cpd-abn-yes-toggleButtonId'){
            wrapInstance.yourDtlWrapper.workWithNBN = true;
            $A.util.addClass(document.getElementById('cpd-abn-yes-toggleButtonId'),'btn-primary');
            $A.util.removeClass(document.getElementById('cpd-abn-no-toggleButtonId'),'btn-primary');
            component.set("v.showABN",true);
        }
        else if(buttonId == 'cpd-abn-no-toggleButtonId'){
            $A.util.addClass(document.getElementById('cpd-abn-no-toggleButtonId'),'btn-primary');
            $A.util.removeClass(document.getElementById('cpd-abn-yes-toggleButtonId'),'btn-primary');
            wrapInstance.yourDtlWrapper.workWithNBN = false;
            component.set("v.showABN",false);            
            var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
           	component.set("v.abnNumber", '');        	
        	component.set("v.businessName", '');
        	component.set("v.entityTypeCode",'');
        	component.set("v.RegisteredEntityName",'');    
        	component.set("v.abnSearchStatus", '');            
            wrapInstance.yourDtlWrapper.applicantABN = '';
            wrapInstance.yourDtlWrapper.applicantEntityType = '';
            wrapInstance.yourDtlWrapper.applicantBusinessName = '';
            wrapInstance.yourDtlWrapper.applicantDoYouHaveABN = false;            
        }
        component.set("v.newDevApplicantWrapperInstance",wrapInstance);
    },
    
    
    radioButtonChange : function(component, event, helper){
        component.set("v.selectedRadio",event.target.value);
        component.set("v.showABN",false);
        component.set("v.abnSearchStatus",'');
        component.set("v.RegisteredEntityName",'');
        var hideBillingEvent = component.getEvent("hideBillingEvent");
        if(event.target.value == 'All Other requestors')
            {
                hideBillingEvent.setParams({
                    "disableBillingInfo" : false                        
                    
                });
                hideBillingEvent.fire();                
            }
        else
        {
             hideBillingEvent.setParams({
                "disableBillingInfo" : true                        
                
            });
            hideBillingEvent.fire(); 
            
        }
    },
    
      populateABNInfo : function(cmp, event) { 
          
        var abnSearchStatus = event.getParam('Status');
        var mapABNResult = event.getParam('mapABNResults');  
        var isApplicantABN = mapABNResult["isApplicantABN"];
          if(isApplicantABN == true){
               var abnNumber =  mapABNResult["ABNNumber"].replace(/ +/g, "");       
              var businessName = mapABNResult["BusinessName"];
              var RegisteredEntityName = mapABNResult["RegisteredEntityName"];
              var entityTypeCode = mapABNResult["entityTypeCode"];
              var entityStatusCode = mapABNResult["entityStatusCode"];

              cmp.set("v.abnNumber", abnNumber);
              cmp.set("v.businessName", businessName);
              cmp.set("v.entityTypeCode",entityTypeCode);
              cmp.set("v.RegisteredEntityName",RegisteredEntityName);
              cmp.set("v.abnSearchStatus", abnSearchStatus);              
              console.log('Enable Next Button');
              document.getElementById("cpd-nextButtonId").disabled = false;
              
          } 
    }   
})