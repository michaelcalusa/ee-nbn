({
    validatehelper : function(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal){
        var errorflag = false;
        if(fieldId == 'cpd-additionalPhoneNumber' && fieldVal != ''){
            if(((fieldVal.substring(0,2) != '02' && fieldVal.substring(0,2) != '03' && fieldVal.substring(0,2) != '04' && fieldVal.substring(0,2) != '07' && fieldVal.substring(0,2) != '08') || fieldVal.length != 10 || !fieldVal.match(/^[0-9]+$/))){
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid Phone number';
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorflag = true;
            }
            else{
                $A.util.removeClass(errorSpanId,'help-block-error');               
                $A.util.removeClass(errorDivId,'has-error');              
                $A.util.addClass(errorDivId,'has-success');
            }
        }
        else if(fieldVal=='' && fieldId != 'cpd-additionalPhoneNumber'){   
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
            errorflag = true;
        }
        else if((fieldId=='cpd-emailAddress' || fieldId=='cpd-confirmemailAddress')){
            if(!fieldVal.match( /^[^\s@]+@[^\s@]+\.[^\s@]+$/)){
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid Email address';
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success'); 
                errorflag = true;
            }
            else if(fieldId=='cpd-confirmemailAddress' && fieldVal != document.getElementById('cpd-emailAddress').value){
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a matching email address';
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
                errorflag = true;
            }
            else{
                $A.util.removeClass(errorSpanId,'help-block-error');
                $A.util.removeClass(errorDivId,'has-error');
                $A.util.addClass(errorDivId,'has-success'); 
            }
                
        }
        else if((fieldId=='cpd-phoneNumber') && ((fieldVal.substring(0,2) != '02' && fieldVal.substring(0,2) != '03' && fieldVal.substring(0,2) != '04' && fieldVal.substring(0,2) != '07' && fieldVal.substring(0,2) != '08') || fieldVal.length != 10 || !fieldVal.match(/^[0-9]+$/))){
            document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a valid phone number';
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
            errorflag = true;
        }
        else{
            $A.util.removeClass(errorSpanId,'help-block-error');               
            $A.util.removeClass(errorDivId,'has-error');              
            $A.util.addClass(errorDivId,'has-success');
        }
        return errorflag;
    },
    
    validatehelperABNname : function(component, event, helper, abnNumber, abnName) {
       var errorflag = false; 
        if (abnName == '') {
            console.log('Disable Next Button function');
            document.getElementById("cpd-nextButtonId").disabled = true;
            errorflag = true;
        } 
        else {
                console.log('EnableNextButton function');
                document.getElementById("cpd-nextButtonId").disabled = false;
                errorflag = false;
        }
        return errorflag;
    }
})