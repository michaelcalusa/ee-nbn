({  

    // this function calls when Incident Number (button) in data table is clicked
    handleRowActions: function (cmp, event, helper) {
        console.log('abc');
        var action = event.getParam('action');
        var row = event.getParam('row');
		if(action.name == 'open_incidentRec'){
			helper.open_incidentRec(cmp,row);
		}
    },
    
    search: function(component, event, helper) {
        helper.getQuickSearchResults (component, event, helper);
        component.set("v.searchExecuted",true);
    },
    
    // CUSTSA-16813 - Developer: syedmoosanazir@nbnco.com.au
    searchOnEnterKey: function(component, event, helper) {
        if(event.getParams().keyCode == 13){
            helper.getQuickSearchResults (component, event, helper);
            component.set("v.searchExecuted",true);
        }
    },
    
    // Client-side controller called by the onsort event handler
    updateColumnSorting: function(component, event, helper){
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        //helper.sortData(cmp, event, helper, fieldName, sortDirection);
        var sortString  = "ORDER BY "+fieldName+ " "+sortDirection;
        component.set("v.sortString", sortString);
        helper.getQuickSearchResults(component, event, helper);
    },
    
    next : function(component, event, helper) 
    {
        var objectList = component.get("v.lstObjects");
        var end = component.get("v.end");
        var start = component.get("v.start");
        var rowsPerPage = component.get("v.rowsPerPage");
        var objectpaginationList = [];
        
        var objectListSize = component.get("v.lstObjects").length;
        
        end = Number(end);
        rowsPerPage = Number(rowsPerPage);
        start = Number(start);
        
        var counter = 0;
        for(var i=end+1;i<end+rowsPerPage+1; i++)
        {
            if(objectList.length > end && !$A.util.isUndefined(objectList[i])){
                objectList[i].urlIs =   '/'+objectList[i].Id;
                objectpaginationList.push(objectList[i]);
            }
            counter++;
        }
        start = start + counter;
        end = end + counter;
        
        component.set("v.start",start);
        component.set("v.end",end);  
        component.set("v.objectPaginationList", objectpaginationList);
    },
    
    previous : function(component, event, helper) 
    {
        var objectList = component.get("v.lstObjects");
        var end = component.get("v.end");
        var start = component.get("v.start");
        var rowsPerPage = component.get("v.rowsPerPage");
        var objectpaginationList = [];
         
        var counter = 0;
        for(var i= start-rowsPerPage; i < start ; i++)
        {
            if(i > -1)
            {
                objectpaginationList.push(objectList[i]);
                objectList[i].urlIs =   '/'+objectList[i].Id;
                counter ++;
            }   
            else
            {
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        
        component.set("v.start",start);
        component.set("v.end",end);
        component.set("v.objectPaginationList", objectpaginationList);
    },
    
})