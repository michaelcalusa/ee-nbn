({
    getQuickSearchResults : function(component, event, helper) {  
        var action = component.get("c.doQuickSearch");
        var rowsPerPage = component.get("v.rowsPerPage");
        component.set("v.totalSize", null);
        component.set("v.lstObjects", null);
        component.set("v.objectPaginationList", null);
  
        action.setParams({
            searchText : component.get("v.searchText"),
            sObjectName : component.get("v.strObject"),
            componentName : component.get("v.componentName"),
            sortString : !$A.util.isUndefinedOrNull(component.get("v.sortString")) ? component.get("v.sortString") : "ORDER BY CurrentslaDueDateCalc__c ASC"
        });
        //Setting the Callback
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            //check if result is successfull
            if(state == "SUCCESS"){
				var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
					var resSobject = result.lstSobjectData;
					if(!$A.util.isEmpty(resSobject) && !$A.util.isUndefined(resSobject)){
						// Update Columns
						var mycolumns = result.lstColumns;
						var columnsUpdated = [];
						for (var singlekey in mycolumns) {
							var arrayOfCols = {};
							arrayOfCols['fieldName'] = mycolumns[singlekey].fieldName;
							arrayOfCols['label'] = mycolumns[singlekey].label;
							arrayOfCols['sortable'] = mycolumns[singlekey].sortable;
							arrayOfCols['type'] = mycolumns[singlekey].type;
							arrayOfCols['initialWidth'] = component.get("v.intIntialColumnWidth");
							
							if(mycolumns[singlekey].fieldName == 'Incident_Number__c'){
								arrayOfCols['type'] = 'button';
								arrayOfCols['initialWidth'] = 158;
								arrayOfCols['fieldName'] = 'Id';
								arrayOfCols['typeAttributes'] =  { label: { fieldName: 'Incident_Number__c' }, name:'open_incidentRec', title: 'Click here to open in a new tab', disabled: {fieldName: 'disabled_fieldName'}, class: {fieldName: 'class_fieldName'} };
							}
							columnsUpdated.push(arrayOfCols);
						}
						
						// Update data
						for (var i = 0; i < resSobject.length; i++) {
							if (!$A.util.isUndefined(resSobject[i])) {
                                if (resSobject[i].CurrentslaDueDateCalc__c){
                                    resSobject[i].CurrentslaDueDateCalc__c = $A.localizationService.formatDate(resSobject[i].CurrentslaDueDateCalc__c, "DD/MM/YYYY hh:mm a");
                                }
                                if (resSobject[i].LastModifiedDate){
                                    resSobject[i].LastModifiedDate = $A.localizationService.formatDate(resSobject[i].LastModifiedDate, "DD/MM/YYYY hh:mm a");
                                }
                                if (resSobject[i].Reported_Date__c){
                                    resSobject[i].Reported_Date__c = $A.localizationService.formatDate(resSobject[i].Reported_Date__c, "DD/MM/YYYY hh:mm a");
                                }
                                //Current_Status__c
                                if (resSobject[i].Current_Status__c && resSobject[i].Current_Status__c == 'In Progress'){
                                    resSobject[i].Current_Status__c = 'Incident Accepted';
                                }
								if (resSobject[i].Id != 'undefined'){
									resSobject[i].Id = resSobject[i].Id;
									resSobject[i].disabled_fieldName = false;
									resSobject[i].class_fieldName = 'IncidentHyperLink';
								}
							}
						}
						component.set("v.tableColumns", columnsUpdated);
						component.set("v.lstObjects", resSobject);
						component.set("v.totalSize", resSobject.length);
						//set datatable
						helper.setDataTable(component, event, helper);
					}
					else{
					  var toastEvent = $A.get("e.force:showToast");
						toastEvent.setParams({
							"type":"error",
							"title": "Failure!",
							"message": "No record found."
						});
						toastEvent.fire();    
					}	
				}
				else{
                  var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type":"error",
                        "title": "Failure!",
                        "message": "No record found."
                    });
                    toastEvent.fire();    
                }
            } else if(state == "ERROR"){
				var errors = response.getError();
                console.log("Error message from Incident Search Function: " + errors[0].message);
            }
        });
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },
	setDataTable : function (component, event, helper){
        var pageSize = parseInt(component.get("v.rowsPerPage"));
        var records = component.get("v.lstObjects");
        component.set("v.start", 0);                
        component.set("v.end", pageSize - 1);		
        var PagList = [];
        for ( var i=0; i< pageSize; i++ ) {
            if ( records.length > i )
                PagList.push(records[i]);    
        }
        component.set('v.objectPaginationList', PagList);
        
    },
	open_incidentRec : function(component, row) {    
        var workspaceAPI = component.find("workspace");
        console.log('row.Id'+row.Id);
        workspaceAPI.isConsoleNavigation().then((res =>{
            if(res){
            	workspaceAPI.openTab({
                    pageReference: {
                        "type": "standard__recordPage",
                        "attributes": {
                            "recordId":row.Id,
                            "actionName":"view"
                        },
                        "state": {}
                    },
                    focus: true
                }).then(function(response) {
                    workspaceAPI.getTabInfo({
                        tabId: response
                    }).then(function(tabInfo) {
                        console.log("The recordId for this tab is: " + tabInfo.recordId);
                    });
                });
        	}
            else
			{
               var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                  "recordId": row.Id
                });
                navEvt.fire();                                 
             }
        }))
        .catch(function(error) {
            console.log(error);
        });
    },
    

    
	sortData: function (component,result, helper) {   
        var sortedBy = component.get("v.sortedBy");
        var rowsPerPage = component.get("v.rowsPerPage");
        var sortedDirection = component.get("v.sortedDirection");
        var reverse = sortedDirection !== 'asc';
        
        //sorts the rows based on the column header that's clicked
        result.sort(this.sortBy(sortedBy, reverse))
        component.set("v.lstObjects", result);
        component.set("v.totalSize", component.get("v.lstObjects").length);
		//set datatable
		this.setDataTable(component, event, helper);
    }, 
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a)?key(a):'', b = key(b)?key(b):'', reverse * ((a > b) - (b > a));
        }
    }, 
    getSortable: function (sortableColumn) {  
        return true;
    },
})