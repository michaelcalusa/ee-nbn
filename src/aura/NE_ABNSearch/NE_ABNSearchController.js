/**
 * Created by philipstafford-jones on 14/1/19.
 */
({
    onblur: function (cmp, event, helper) {
        // onblur - validate/ set error and search ABN
        var isValid = helper.checkABN(cmp, event, helper);
        cmp.set('v.result','');

        // if valid search
        if (isValid == true) {
            var eventCmp = event.getSource();
            var abnNumber = eventCmp.get('v.value');
            helper.searchABN(cmp, event, helper, abnNumber);
        }
        var customOnblurFunction = cmp.get('v.onblur');
        if ( $A.util.isEmpty(customOnblurFunction) === false ) {
            cmp.customBlur();
        }
        return isValid;
    },

    customBlur : function(cmp,event,helper) {
        console.log("ABNSearch: Error LOCAL custom  blur function called !");
    },

    scrollToTop: function(cmp, event, helper) {
        cmp.find("abnSearchId").focus();
    },

    validate: function(cmp, event, helper) {
        // check ABN is set and no error shown on component
        var abnCmp = cmp.find("abnSearchId");
        var value = abnCmp.get('v.value');
        var required = cmp.get('v.required');

        if (required == false && $A.util.isEmpty(value)) {
            abnCmp.setCustomValidity('');
            abnCmp.reportValidity();
            return true;
        }
        if (required == true && $A.util.isEmpty(value)) {
            abnCmp.setCustomValidity('Complete this field.');
            abnCmp.reportValidity();
            abnCmp.focus();
            abnCmp.focus();
        }

        var isValid = helper.checkABN(cmp, event, helper);

        if (required == true && isValid == false) {
            abnCmp.focus();
            abnCmp.focus();
        }
        isValid &= helper.checkComponentErrorStatus(cmp, event, helper);
        return isValid;
    },

    reportValidity : function(cmp,event,helper) {
        return cmp.find("abnSearchId").reportValidity();
    }
})