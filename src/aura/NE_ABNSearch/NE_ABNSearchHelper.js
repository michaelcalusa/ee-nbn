/**
 * Created by philipstafford-jones on 14/1/19.
 */
({
    searchABN : function(component, event, helper, abnNumber) {
            var action = component.get("c.doABNSearch");
            component.set('v.result', '');
            action.setParams({'strABNNumber': abnNumber});
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    if (storeResponse.strErrorMsg.length == 0 && storeResponse.lstBusinessName.length >= 1) {
                        if (storeResponse.entityStatusCode === "Active") {

                            var myMap = {};
                            myMap['BusinessName'] = storeResponse.lstBusinessName[0];
                            myMap['entityStatusCode'] = storeResponse.entityStatusCode;
                            myMap['entityTypeCode'] = storeResponse.entityTypeCode;

                            component.set('v.result', storeResponse.lstBusinessName[0]);
                        } else {
                            helper.setComponentStatus(event.getSource(), false, "Invalid ABN: " + storeResponse.entityStatusCode);
                            component.set('v.result', '');
                        }
                    } else {
                        var myMap = {};
                        helper.setComponentStatus(event.getSource(), false, "Invalid ABN - not found");
                        component.set('v.result', '');
                    }
                }
            });

            // enqueue the Action
            $A.enqueueAction(action);
    },

    checkABN: function(cmp, event, helper) {
        var abnCmp = cmp.find('abnSearchId');
        var abn = abnCmp.get("v.value");
        if (abn) {
            var isValid = helper.checkValidity(abnCmp, helper.validateABN, 'Invalid ABN');
            return isValid;
        } else {
            abnCmp.setCustomValidity('');
            abnCmp.reportValidity();
            return false;
        }
    },

    checkComponentErrorStatus: function(cmp, event, helper) {
        var cmp = cmp.find('abnSearchId');
        var isValid = cmp.checkValidity();
        return isValid;
    },

    /**
     * validate ABN
     * @param {string} abn
     * @return {boolean} is ABN number valid
     *
     * 0. ABN must be 11 digits long
     * 1. Subtract 1 from the first (left) digit to give a new eleven digit number
     * 2. Multiply each of the digits in this new number by its weighting factor
     * 3. Sum the resulting 11 products
     * 4. Divide the total by 89, noting the remainder
     * 5. If the remainder is zero the number is valid
     */
    validateABN: function(abn){
        var isValid = true;
        //remove all spaces
        abn = abn && abn.replace(/\s/g, '');

        //0. ABN must be 11 digits long
        isValid &= abn && /^\d{11}$/.test(abn);

        if(isValid){
            var weightedSum = 0;
            var weight = [10, 1, 3, 5, 7, 9, 11, 13, 15, 17, 19];

            //Rules: 1,2,3
            for (var i = 0; i < weight.length; i++) {
                weightedSum += (parseInt(abn[i]) - ((i === 0) ? 1 : 0)) * weight[i];
            }
            //Rules: 4,5
            isValid &= ((weightedSum % 89) === 0);
        }
        return isValid;
    },



})