/**
 * Created by philipstafford-jones on 18/12/18.
 */
({
    isValidPostcode: function(cmp, event, helper) {
        var postcodeCmp = helper.findCmpByUiLabel(cmp, "billDetailsPostcode", "Postcode");

        // clear any errors on the state field if they are resolved
        var stateCmp = helper.findCmpByUiLabel(cmp, "billDetailsPostcode", "State/Territory");
        var state = stateCmp.get('v.value');
        if (state != undefined) {
            var isValid = helper.isValidState(stateCmp, event, helper, postcodeCmp);
        }
        return helper.isValidPostcode(postcodeCmp);
    },

    isValidState: function(cmp, event, helper) {
        var postcodeCmp = helper.findCmpByUiLabel(cmp, "billDetailsPostcode", "Postcode");
        var stateCmp = helper.findCmpByUiLabel(cmp, "billDetailsPostcode", "State/Territory");
        return helper.isValidState(stateCmp, event, helper, postcodeCmp);
    },


    validate : function(cmp, event, helper) {
        var isValid = true;
        cmp.find("billDetailsPostcode").reverse().forEach(function(nestedCmp) {
            isValid &= nestedCmp.validate();
        });

        var stateCmp = helper.findCmpByUiLabel(cmp, "billDetailsPostcode", "State/Territory");
        var postcodeCmp = helper.findCmpByUiLabel(cmp, "billDetailsPostcode", "Postcode");
        isValid &= helper.isValidState(stateCmp, event, helper, postcodeCmp);

        isValid &= helper.isAllFieldsValid(cmp, "billDetails");

        helper.findFieldArray(cmp, "billDetailsAbn").reverse().forEach(function(nestedCmp) {
            isValid &= nestedCmp.validate();
        });

        isValid &= helper.isAllFieldsValid(cmp, "asDetails");

        return isValid;
    },

    handleComponentEvent : function(cmp, event, helper) {
        var postcodeCmp = helper.findCmpByUiLabel(cmp, "billDetailsPostcode", "Postcode");
        var stateCmp = helper.findCmpByUiLabel(cmp, "billDetailsPostcode", "State/Territory");
        return helper.isValidState(stateCmp, event, helper, postcodeCmp);
    }

})