/**
 * Created by philipstafford-jones on 29/11/18.
 */
({
    handleComponentChangeEvent: function(cmp,event) {
        console.log('received event');
        var childCmp = event.getSource();
        var isValid = childCmp.get('v.value');
 
        // if this is now valid clear the error box on the any-of set
        if ( isValid ) {
            // remove errors
            $A.util.removeClass(cmp.getElements()[0], 'error-box');
            $A.util.addClass(cmp.getElements()[1], 'hide');
        }
        event.stopPropagation();
    }
})