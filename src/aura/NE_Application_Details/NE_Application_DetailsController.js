({
    validate : function(cmp, event, helper) {
        var isValid = true;
        isValid &= cmp.find("deliveryRequirements").validate();
        isValid &= helper.validateTrafficClass(cmp);
        cmp.find("serviceDetails").reverse().forEach(function(nestedCmp) {
            isValid &= nestedCmp.validate();
        });
        return isValid;
    },

    validateTrafficClass: function(cmp,event,helper) {
        helper.validateTrafficClass(cmp, true);
    },
    
    uploadFinished: function(cmp, event, helper) {
        var uploadedFiles = event.getParam("files");
        
        if(uploadedFiles && uploadedFiles.length > 0) {
            var documentIds = helper.getOrDefaultArray(cmp.get("v.appForm.deliveryRequirements.documentIds"));
            var fileNames = helper.getOrDefaultArray(cmp.get("v.appForm.deliveryRequirements.fileNames"));
            
            var uploadedFileCnt = uploadedFiles.length;
            for (var i = 0; i < uploadedFileCnt; i++) {
                documentIds.push(uploadedFiles[i].documentId);
                fileNames.push(uploadedFiles[i].name);
            }
            
            cmp.set("v.appForm.deliveryRequirements.documentIds", documentIds);
            cmp.set("v.appForm.deliveryRequirements.fileNames", fileNames);
        }
    },
    
    removeUpload: function(cmp, event, helper) {
        //convert into number
        var index = event.target.getAttribute("data-id") * 1; 
        
        var documentIds = helper.getOrDefaultArray(cmp.get("v.appForm.deliveryRequirements.documentIds"));
        var fileNames = helper.getOrDefaultArray(cmp.get("v.appForm.deliveryRequirements.fileNames"));
        
        if (index > documentIds.length) {
            helper.debug(cmp, 'Removed index: '+ index + ' is greater than file array, skipping...');
            return;
        }
        
        documentIds.splice(index, 1);
        fileNames.splice(index, 1);
        
        cmp.set("v.appForm.deliveryRequirements.documentIds", documentIds);
        cmp.set("v.appForm.deliveryRequirements.fileNames", fileNames);
    },
    
    clearTrafficClassError : function(cmp, event, helper) {
        console.log("Clear traffic class error");
        var cmpTarget = cmp.find("traffic-class-any-of");
        if ( isValidTrafficClass(cmp) ) {
            $A.util.removeClass(cmpTarget.getElements()[0], 'error-box');
            $A.util.addClass(cmpTarget.getElements()[1], 'hide');
        }
    },
    
    isWeekdayInFuture: function (cmp, event, helper) {
        var eventCmp = event.getSource();
        helper.isFutureDate(eventCmp) && helper.isWorkingDay(eventCmp);
    },
    
    scrollToTop: function(cmp, event, helper) {
        console.log('inside scrollToTop for Application_Details');
        helper.scrollToTop(cmp, 'serviceDetails');
    }
    
})