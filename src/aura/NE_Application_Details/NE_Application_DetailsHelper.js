({
    validateTrafficClass : function(cmp) {
        var isValid = true;
        var cmpTarget = cmp.find("traffic-class-any-of");

        if ( this.isValidTrafficClass(cmp) ) {
            // remove errors
            $A.util.removeClass(cmpTarget.getElements()[0], 'error-box');
            $A.util.addClass(cmpTarget.getElements()[1], 'hide');
        } else {
            // add errors
            $A.util.addClass(cmpTarget.getElements()[0], 'error-box');
            $A.util.removeClass(cmpTarget.getElements()[1], 'hide');
            isValid = false;
        }
        return isValid;
    },

    isValidTrafficClass: function(cmp) {
        var appDetails = cmp.get('v.appForm.serviceRequirements');
       
        if (appDetails  && this.empty(appDetails.trafficClass4)
            && this.empty(appDetails.trafficClass2)
            && this.empty(appDetails.trafficClass1)) {
            return false;
        } else {
            return true;
        }
    },
    
    getOrDefaultArray: function(arrayObj) {
        if($A.util.isArray(arrayObj)) {
            return arrayObj;
        }
        return [];
    }
})