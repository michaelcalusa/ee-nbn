({
    onSearchQuery: function (cmp, event, helper) {
        //console.log('handling search query');
        cmp.set("v.pageNumber", 1);
    },
    
    onSearchResults: function(cmp, event, helper) {
        //console.log('handling search results');
        if(event.getParam("hasError")){
            var errorLabel = $A.get("$Label.c.DF_Application_Error");
            helper.setErrorBanner(cmp, errorLabel);
            cmp.set('v.fullList', []);
            cmp.set('v.currentList', []);
        } else{
            var searchResults = event.getParam("searchResults");
            helper.clearMessage(cmp);
            cmp.set('v.fullList', searchResults);
            var pageCountVal = cmp.get("v.selectedCount");
            cmp.set("v.maxPage", Math.floor((searchResults.length + (pageCountVal - 1)) / pageCountVal));
            helper.renderPage(cmp);
        }
    },
    
    clickLink: function (cmp, event, helper) {
        var appId = event.target.getAttribute("data-Id");
        var eventToFire = cmp.getEvent("viewOppEvent");
        eventToFire.setParams({"oppBundleId": appId});
        eventToFire.fire();
    },
    
    renderPage: function (cmp, event, helper) {
        helper.renderPage(cmp);
    },
    
    updateColumnSorting: function (cmp, event, helper) {
        var sortedDirection = cmp.get('v.sortedDirection');
        var clickedColumnName = event.target.getAttribute("data-column");
        var asc = 'ascending';
        var desc = 'descending';
        cmp.set('v.sortedByColumn', clickedColumnName);
        cmp.set('v.sortedDirection', sortedDirection !== desc ? desc : asc);
        
        helper.sortListData(cmp, clickedColumnName, sortedDirection);
    },
});