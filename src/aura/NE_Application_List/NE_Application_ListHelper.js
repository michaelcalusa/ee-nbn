({
    ERR_MSG_ERROR_OCCURRED: 'Error occurred. Error message: ',
    ERR_MSG_UNKNOWN_ERROR: 'Error message: Unknown error',
    MAX_COUNT: '5000',

    renderPage: function (cmp) {

        var records = cmp.get("v.fullList"),
            pageNumber = cmp.get("v.pageNumber"),
            pageCountVal = cmp.get("v.selectedCount"),
            pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));

        cmp.set("v.currentList", pageRecords);
    },

    sortListData: function (cmp, columnName, sortDirection) {
        var data = cmp.get("v.currentList");
        var reverse = sortDirection !== 'ascending';

        data.sort(this.sortBy(columnName, reverse));
        cmp.set("v.currentList", data);
    },

    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function (x) {
                return primer(x[field])
            } :
            function (x) {
                return x[field]
            };

        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    }

});