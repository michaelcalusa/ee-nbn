({
    init: function (cmp, event, helper) {
        helper.doSearch(cmp, event);
    },    
    searchTermChange: function (cmp, event, helper) {
        //Do search if enter key is pressed.       
        self.debug(cmp, 'search term changed: keycode is '+event.getParams().keyCode);
        if (event.getParams().keyCode == 13) {
            helper.doSearch(cmp, event);
        }
    },
    searchDateChange: function (cmp, event, helper) {
        
    },
    searchStatusChange: function (cmp, event, helper) {
        
    },
    search: function (cmp, event, helper) {
        helper.doSearch(cmp, event);
    },
    clear: function (cmp, event, helper) {
        cmp.set('v.searchTerm', '');
        cmp.set('v.searchDate', '');
        cmp.set('v.searchStatus', '');
        helper.doSearch(cmp, event);
    }
})