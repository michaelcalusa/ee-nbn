({
    doSearch: function (cmp, event) {
        var self = this;
        
        var searchTerm = cmp.get('v.searchTerm').trim();
        var searchDate = cmp.get('v.searchDate').trim();
        var searchStatus = cmp.get('v.searchStatus');
        var maxResults = cmp.get('v.maxResults');
        
        //signal search in progress
        self.debug(cmp, 'firing applicationSearchQueryEvent');
        cmp.getEvent("applicationSearchQueryEvent").setParams({
            "searchTerm" : searchTerm, 
            "searchDate" : searchDate, 
            "searchStatus" : searchStatus
        }).fire();
        
        self.debug(cmp, 'non formatted searchDate: '+searchDate);
        var ymdDatePattern = /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/;
        var dmyDatePattern = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-](\d{4})$/;
        if(ymdDatePattern.test(searchDate)){
            //must pass searchDate in correct format with time component so Apex DateTime.valueOf('yyyy-MM-dd HH:mm:ss') parsing works
            searchDate = searchDate.replace(new RegExp('/', 'g'), "-");
            searchDate += " 00:00:00";
            self.debug(cmp, 'formatted searchDate: '+searchDate);
        }else if(dmyDatePattern.test(searchDate)){
            //manual input causes different format to be saved so we must change format back to yyyy-MM-dd
            var match = dmyDatePattern.exec(searchDate);
            searchDate = match[3] + "-" + match[2] + "-" + match[1] + " 00:00:00";
            self.debug(cmp, 'formatted searchDate: '+searchDate);
        }else if(searchDate.length > 0){
            self.debug(cmp, 'invalid date: '+searchDate);
            searchDate = "1970-01-01 00:00:00";//if searchDate is invalid make sure no results are returned
        }
        
        var getRecordsPromise = self.callApex(cmp, "c.searchApplicationsAsJson", {
            "searchTerm": searchTerm,
            "searchDate": searchDate,
            "searchStatus": searchStatus,
            "maxResults": maxResults
        }).then(
            $A.getCallback(function (data) {
                var records = JSON.parse(data);
                //signal search results are available for display
                self.debug(cmp, 'firing applicationSearchResultEvent');
                cmp.getEvent("applicationSearchResultEvent").setParams({
            		"searchResults" : records,
                    "hasError" : false
        		}).fire();
            })
        ).catch(
            $A.getCallback(function (error) {
                //signal search results are available for display
                self.debug(cmp, 'firing applicationSearchResultEvent with hasError');
                cmp.getEvent("applicationSearchResultEvent").setParams({
            		"searchResults" : [],
                    "hasError" : true
        		}).fire();
            })
        );
    }
})