({
    validate: function(component, event, helper) {
        return helper.validate(component, event, helper);
	},
    
	checkABN: function(cmp, event, helper) {
        var abnCmp = event.getSource();
        if (abnCmp.get("v.value")) {
        	helper.checkValidity(abnCmp, helper.validateABN, 'Invalid ABN'); 
        } else {
            abnCmp.setCustomValidity('');
            abnCmp.reportValidity();
        }
    },
})