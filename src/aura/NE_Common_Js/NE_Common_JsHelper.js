({
    isDebugEnabled: function(cmp) {
        return cmp.get("v.debug");
    },
    
    debug: function(cmp, message) {
        if (cmp.get("v.debug")) {
            console.log(message);
        }
    },
    
    fireNavToUrlEvent: function(url) {        
        $A.get("e.force:navigateToURL").setParams({
            "url": url
        }).fire();
    },
    
    // pageName: 'Home', 'Dashboard', 'CreateApp'
    fireHomePageEvent: function(cmp, pageName) {
        cmp.getEvent("homePageEvent").setParams({
            "requestType" : pageName 
        }).fire();
    },
    
    setErrorBanner: function (cmp, errorMessage) {
        cmp.getEvent("homeMessageEvent").setParams({
            "msgType" : 'BANNER', 
            "status" : 'ERROR', 
            "message" : errorMessage
        }).fire();
        this.smoothScrollToPageTop(cmp);
    },
    
    setOkBanner: function (cmp, message) {
        cmp.getEvent("homeMessageEvent").setParams({
            "msgType" : 'BANNER', 
            "status" : 'OK', 
            "message" : message
        }).fire();
    },
    
    setMessage: function (cmp, status, message, msgType) {
        cmp.getEvent("homeMessageEvent").setParams({
            "msgType" : msgType, 
            "status" : status, 
            "message" : message
        }).fire();
    },
    
    clearMessage: function (cmp) {
        cmp.getEvent("homeMessageEvent").setParams({
            "msgType" : "", "status" : "", "message" : ""
        }).fire();
    }, 
    
    showErrorToast: function(message) {
        $A.get("e.force:showToast").setParams({
            "type": 'error',
            "title": 'Error!',
            "message": message
        }).fire();
    },

    empty: function(string) {
        return (string === undefined || string === '' || string === null);
    },


    /*
     An example below:
     
     var self = this;
     var savePromise = helper.callApex(cmp, "c.apexMethodToCall", { "testParam" : 'testValue' });
         savePromise.then($A.getCallback(function(data){
	        // self.renderPage(cmp, data);
	     })).catch($A.getCallback(function(error){
	        if (error && error.message && error.message.indexOf('VALIDATION_FAILED') !== -1) {
	        	// server side validation error, depending on the value returned from Apex method
	        	
	        } else {
	        	// server side unexpected error
	        	
	        }
	   }));
     */
    callApex: function(cmp, apexMethod, apexParams) {
        return new Promise($A.getCallback(function(resolve, reject) {
            var action = cmp.get(apexMethod);
            action.setParams(apexParams);
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log("SUCCESS on calling Apex method: " + apexMethod);
                    resolve(response.getReturnValue());
                } else if (state === "ERROR") {
                    var errorMsg = null;
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) {
                        errorMsg = "Error on calling Apex method: " + apexMethod + " with param: " + JSON.stringify(apexParams) + ", message: " + errors[0].message;
                    } else {
                        errorMsg = "Unknown error on calling Apex method: " + apexMethod + " with param: " + JSON.stringify(apexParams);
                    }
                    console.log(errorMsg);
                    reject(new Error(errorMsg));
                } else {
                    // INCOMPLETE, ABORTED, etc.
                    var errorMsg = "Failed on calling Apex method: " + apexMethod + " with param: " + JSON.stringify(apexParams) + ", state: " + state;
                    console.log(errorMsg);
                    reject(new Error(errorMsg));
                }
            });
            $A.enqueueAction(action);
        }));
    },
    
    findFieldArray: function (cmp, fieldGroupName) {
        var input = cmp.find(fieldGroupName);
        var inputArray = [];
        if($A.util.isArray(input)){
            inputArray = input;
        }else{
            inputArray.push(input);
        }
        return inputArray;
    },
    
    //This helper method is to resolve the issue of validation message not showing until focus twice
    isAllFieldsValid: function (cmp, fieldGroupName) {
        var inputArray = this.findFieldArray(cmp, fieldGroupName);
        //reverse order of array to validate inputs appearing at the top of the page last so that they get the focus
        inputArray.reverse();
        var allValid = inputArray.reduce( 
            function (validFields, inputCmp) {

                //isValid &= nestedCmp.validate();
                inputCmp.showHelpMessageIfInvalid();
                var fieldIsValid = inputCmp.get('v.validity').valid;
                if (!fieldIsValid) {
                    inputCmp.focus();
                    inputCmp.focus();
                }
                return validFields && fieldIsValid;
            }, true);
        
        return allValid;
    },
    
    // validatorFn should take only one parameter which is the value of component
    checkValidity: function(cmp, validatorFn, errorMsg) {
    	if (!validatorFn(cmp.get("v.value"))) {
            cmp.setCustomValidity(errorMsg);
        } else {
            cmp.setCustomValidity('');
        }
        return cmp.reportValidity();
    },
    
    // Find the component by its UI label in the group.
    // Sometimes we want to do some additional validation on specific component,
    // but they usually share same aura id.
    findCmpByUiLabel: function(cmp, fieldGroupName, label){
        var cmpList = this.findFieldArray(cmp, fieldGroupName);
        for (var i = 0; i < cmpList.length; i++) {
            if (cmpList[i].get('v.label') === label) {
                return cmpList[i];
            }
        }
        return null;
    },
    

    
    //custom validators that operate on a given input (cmp) and return true if valid else false
    myCustomValidator : function(cmp){
        console.log("inside myCustomValidator, value of " + cmp.get('v.label') + " is " + cmp.get('v.value'));
        return true;
    },

    isFutureDateCheck: function(value) {
         var today = new Date();
         if(Date.parse(value) <= today.getTime()){
            return false;
        } else {
            return true;
        }
    },

    isFutureDate: function(cmp) {
        var value = cmp.get("v.value");
        var currDate = new Date(value).setHours(0,0,0,0);
        var startOfToday = new Date().setHours(0,0,0,0);
        if(currDate <= startOfToday ){
            cmp.setCustomValidity("Must be a future date");
        } else {
            cmp.setCustomValidity('');
        }
        return cmp.reportValidity();
    },

    isWeekend: function( dateStr ) {
        var date = new Date(dateStr);
        var isWeekend =  date.getDay() == 0 || date.getDay() == 6;
        return isWeekend;
    },

    isWorkingDay: function(cmp) {
        var isWorkingDay = this.isWeekend(cmp.get('v.value')) === false;
        this.setComponentStatus(cmp, isWorkingDay, "Date must be Monday to Friday");
        return isWorkingDay;
        },

    isWeekdayInFuture: function(cmp) {
        if (this.isFutureDate(cmp) === false) {
            return false;
        }
        return this.isWorkingDay(cmp);
    },

    setComponentStatus: function ( cmp, status, errorMessage) {
        if( status === false ){
            cmp.setCustomValidity(errorMessage);
        } else {
            cmp.setCustomValidity('');
        }
        return cmp.reportValidity();
    },

    scrollToTop: function(cmp, auraId) {
        setTimeout( function() {
               	var cmp =  getFirstComponent(auraId);
                if ( isFunction(cmp.focus) ) {
                	cmp.focus(); // all components must implement a focus() method
                }
            }, 100 );
    },

    smoothScrollToPageTop: function(cmp) {
        var scrollOptions = {
            left: 0,
            top: 0,
            behavior: 'smooth'
        }
        window.scrollTo(scrollOptions);
    },
    
    isFunction: function (fn) {
    	return typeof fn === 'function';
	},
    
    getFirstComponent: function(auraId) {
        var cmpList =  cmp.find(auraId);
        if (Array.isArray(cmpList)) {
            return cmpList[0];
        } else {
            return cmpList;
        }
 	},

    getStateFromPostcode: function(postcode) {

        var state = '';

        if (postcode.length != 4) {
            return state;
        }
        if ( this.inRange(postcode, '0200', '0299') || this.inRange(postcode, '2600', '2617') || this.inRange(postcode, '2900', '2920') ) {
            state =  'ACT';
        }
        if ( this.inRange(postcode, '1000', '2599') || this.inRange(postcode, '2618', '2899') || this.inRange(postcode, '2921', '2999') ) {
            state = 'NSW';
        }
        if ( this.inRange(postcode, '0800', '0999') ) {
            state = 'NT';
        }
        if ( this.inRange(postcode, '4000', '4999') || this.inRange(postcode, '9000', '9999') ) {
            state = 'QLD';
        }
        if ( this.inRange(postcode, '5000', '5999') ) {
            state = 'SA';
        }
        if ( this.inRange(postcode, '6000', '6999') ) {
            state = 'WA';
        }
        if ( this.inRange(postcode, '7000', '7499') || this.inRange(postcode, '7800', '7999') ) {
            state = 'TAS';
        }
        if ( this.inRange(postcode, '3000', '3999') || this.inRange(postcode, '8000', '8999') ) {
            state = 'VIC';
        }
        return state;
    },

    isValidPostcode: function (cmp) {
        var postcode = cmp.get("v.value");
        var isValid = false;
        if (postcode != undefined) {
            isValid = this.getStateFromPostcode(postcode) != '';
        }
        this.setComponentStatus(cmp, isValid, "Must be a valid postcode");
        return isValid;
    },

    isValidState: function (cmp, event, helper, postcodeComponent) {
        var state = cmp.get("v.value");
        var postcode = postcodeComponent.get('v.value');
        var isValid = false;
        if (state != undefined && postcode != undefined) {
            isValid = this.getStateFromPostcode(postcode) === state;
        }
        this.setComponentStatus(postcodeComponent.find("inputId"), isValid, "Postcode/State combination invalid");
        this.setComponentStatus(cmp.find("comboId"), isValid, "State must match postcode");

        return isValid;
    },

    inRange: function(value, lower, upper) {
        return (value >= lower && value <= upper);
    },


    validate: function(component, event, helper) {
        var isValid = true;
        var validatorMap = component.get("v.validateableInputs");
        Object.keys(validatorMap).reverse().forEach(function(auraId) {
            //get custom validators
            var customValidatorMap = validatorMap[auraId];
            Object.keys(customValidatorMap).reverse().forEach(function(uiLabel) {
                var customComponent = helper.findCmpByUiLabel(component, auraId, uiLabel);
                var customValidator = customValidatorMap[uiLabel];
                if(customComponent !== null && typeof helper[customValidator] === 'function') {
                    console.log("applying custom validator "+customValidator+" for "+uiLabel);
                    isValid &= helper[customValidator](customComponent);
                }
            });
            //apply standard inbuilt validations
            isValid &= helper.isAllFieldsValid(component, auraId);
        });
        return isValid;
    },
})