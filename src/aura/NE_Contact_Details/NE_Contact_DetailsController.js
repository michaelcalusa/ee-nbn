({
    /*
    scrollToTop: function(cmp, event, helper) {
        console.log('inside scrollToTop for NE_AccessSeekerDetails');
        helper.scrollToTop(cmp, 'businessDetails');
    },
    */
    
    validateABN: function(cmp, event, helper) {
        var abnCmp = event.getSource();
        if (!$A.util.isEmpty(abnCmp.get("v.value"))) {
            helper.checkValidity(abnCmp, helper.validateABN, 'Invalid ABN');
        } else {
            abnCmp.setCustomValidity('');
        }
        helper.validateAllOrNothing(cmp, event.getSource().getLocalId());
    },
    
    validateAllOrNothing: function(cmp, event, helper) {
        var auraId = event.getSource().getLocalId();
        if (auraId == 'siteContactsABN') {
           auraId = 'siteContacts';
        }
        var isValid = helper.validateAllOrNothing(cmp, auraId);
        return isValid;
    },

    validate : function(cmp, event, helper) {
        var isValid = true;

        isValid &= helper.isAllFieldsValid(cmp, "siteContacts");

        helper.findFieldArray(cmp, "siteContactsABN").reverse().forEach(function(nestedCmp) {
            isValid &= nestedCmp.validate();
        });

        isValid &= helper.isAllFieldsValid(cmp, "secondaryContactDetails");
        isValid &= helper.isAllFieldsValid(cmp, "primaryContactDetails");
        isValid &= helper.isAllFieldsValid(cmp, "businessDetails");

        helper.findFieldArray(cmp, "contactDetailsABN").reverse().forEach(function(nestedCmp) {
            isValid &= nestedCmp.validate();
        });

        return isValid;
    },
})