({
    validateAllOrNothing: function(cmp, auraGroupId) {
        console.log("validate all or nothing " + auraGroupId);
        var groupFilledIn = false;
        var cmpList = this.findFieldArray(cmp, auraGroupId);

        if (auraGroupId == 'siteContacts') {
            var inputCmp = cmp.find('siteContactsABN');
            cmpList.push( inputCmp );
        }

        for (var i = 0; i < cmpList.length; i++) {
            if (!$A.util.isEmpty(cmpList[i].get('v.value'))) {
                groupFilledIn = true;
                break;
            }
        }
        cmp.set("v." + auraGroupId + "Required", groupFilledIn);

        for (var i = 0; i < cmpList.length; i++) {
            cmpList[i].reportValidity();
        }
    }
});