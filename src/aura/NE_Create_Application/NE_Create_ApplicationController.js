({
	backToDashboard : function(cmp, event, helper) {
        helper.fireHomePageEvent(cmp, 'Dashboard');
	},
})