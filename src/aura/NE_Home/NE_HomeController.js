({
    init: function (cmp, event, helper) {
        // Create the action
        var action = cmp.get("c.getBizCommunityPermissions");
        var communityPermissions;
        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                communityPermissions = response.getReturnValue();
                if(communityPermissions.includes('Has_NE_Community_Permissions__c')){
                    cmp.set("v.hasNEAccess", true);
                }
                console.log('v.hasNEAccess'+cmp.get("v.hasNEAccess"));

            }   else {
                console.log('BSM_HomeController.getBizCommunityPermissions - communityPermissions- Failed response with state: ' + state);
            }
            cmp.set("v.initCompleted", true);

        });
        // Send action off to be executed
        $A.enqueueAction(action);
    },

    handleHomePageEvent: function (cmp, event, helper) {
        var pageName = new String(event.getParam("requestType")).trim();
        event.stopPropagation();
        helper.gotoPage(cmp, pageName);
    },
    
    handleViewOppEvent: function (cmp, event, helper) {
        var viewAppIdArray = new String(event.getParam("oppBundleId")).trim().split(",");
        var viewAppId = viewAppIdArray[0];
        
        cmp.set("v.viewAppId", viewAppId);
        
        event.stopPropagation();
        if(viewAppIdArray.length > 1 && viewAppIdArray[1] === 'showBanner') {
            helper.gotoPage(cmp, 'ViewAppWithBanner');
        } else {
            helper.gotoPage(cmp, 'ViewApp');
        }
    },
    
    handleHomeMessageEvent: function (cmp, event, helper) {
        var status = new String(event.getParam("status")).trim();
        var msgType = new String(event.getParam("msgType")).trim();
        var message = event.getParam("message");
        event.stopPropagation();
        
        cmp.set("v.responseStatus", status);
        cmp.set("v.responseMessage", message);
        cmp.set("v.messageType", message);
    },
    
    showSpinner : function (cmp, event, helper) {
        cmp.set("v.showLoadingSpinner", true);
    },
    
    hideSpinner: function(cmp, event, helper) {
        cmp.set("v.showLoadingSpinner", false);
    },
})