({  
    gotoPage: function(cmp, pageName) {
        console.log('goto page: '+ pageName);
        switch(pageName) {
            case "Home":
                this.fireNavToUrlEvent("/");
                cmp.set("v.pageName", "Dashboard");
                break;
            case "CreateApp":
            case "ViewApp":
            case "ViewAppWithBanner":
                cmp.set("v.pageName", pageName);
                break;
            case "Dashboard":
            default: // default to Dashboard page
                cmp.set("v.pageName", "Dashboard");
                break;
        }
    },
    
    /*
    updatePageUrl: function(cmp, pageName) {
        var currentUrl = location.href;
        var baseUrl = currentUrl.substring(0, currentUrl.indexOf('#'));
        location.href = baseUrl + '#' + pageName;
    },
    */
})