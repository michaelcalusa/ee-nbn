({
    clickLink: function(cmp, event, helper) {
        var linkId = event.target.getAttribute("data-id");
        helper.fireHomePageEvent(cmp, linkId);
    }
});