({
    init: function(cmp, event, helper) {
        helper.callApex(cmp, "c.getStats", {})
        .then($A.getCallback(function (stats) {
            helper.clearMessage(cmp);
            
            cmp.set("v.countByUser", stats.countByUser);
            cmp.set("v.countByRsp", stats.countByRsp);
        })).catch($A.getCallback(function (error) {
            var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
            helper.setErrorBanner(cmp, ERR_MSG_APP_ERROR);
        }));
    },
})