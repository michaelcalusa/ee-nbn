/**
 * Created by philipstafford-jones on 29/11/18.
 */
({
    onblur: function (cmp, event, helper) { },
    scrollToTop: function(cmp, event, helper) {
        cmp.find("inputId").focus();
    },
})