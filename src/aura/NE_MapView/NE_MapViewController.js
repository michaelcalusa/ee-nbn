({
    handleExistingOrNewLocation : function(cmp, event, helper) {
     
        var allButtons = cmp.find("existingNewLocationButtons"); 
        allButtons.forEach(function(button) { 
            button.set("v.variant", "neutral"); 
        })
        event.getSource().set("v.variant", "brand");                               
        
        if(event.getSource().get("v.label") == "Existing Location") {
            cmp.set("v.appForm.locationDetails.isExistingLocation", true);
            //set dummy value to pass required validation as input will be hidden
            cmp.set("v.appForm.locationDetails.locationId", "");
            cmp.set("v.appForm.locationDetails.cpi", "");
            cmp.set("v.appForm.locationDetails.serviceId", "");
            
            var existingLocationType = cmp.get("v.appForm.locationDetails.existingLocationType");
            var allLocationTypeButtons = cmp.find("existingLocationTypeButtons"); 
        	allLocationTypeButtons.forEach(function(button) { 
                if(button.get("v.label") == existingLocationType){
                    button.set("v.variant", "brand"); 
                }else{
                    button.set("v.variant", "neutral"); 
                }
        	})
        } 
        else{
            
            cmp.set("v.appForm.locationDetails.isExistingLocation", false);
            //set dummy value to pass required validation as input will be hidden
            cmp.set("v.appForm.locationDetails.locationId", "LOC000000000000");
            cmp.set("v.appForm.locationDetails.cpi", "unknown");
            cmp.set("v.appForm.locationDetails.serviceId", "unknown");
        }  
    },
    handleExistingLocationType : function(cmp, event, helper) {
     
        var allButtons = cmp.find("existingLocationTypeButtons"); 
        allButtons.forEach(function(button) { 
            button.set("v.variant", "neutral"); 
        })
        event.getSource().set("v.variant", "brand");
        var buttonLabel = event.getSource().get("v.label");   
        cmp.set("v.appForm.locationDetails.existingLocationType", buttonLabel);
    },
    renderMap : function(cmp, event, helper) {
        
        var isValid = true;
        cmp.find("latlong").reverse().forEach(function(c) { 
            isValid &= c.validate(); 
        });
        if(isValid){
            var latitude = '' + cmp.get("v.appForm.locationDetails.latitude");
            var longitude = '' + cmp.get("v.appForm.locationDetails.longitude");
            var loc = {
                location: {
                    'Latitude': latitude,
                    'Longitude': longitude
                },
                title: 'Network extension location',
                description: 'Lat: '+latitude+', Long: '+longitude
            };
            cmp.set("v.appForm.locationDetails.isValidLatLong", true);
            helper.updateMarkers(cmp, [loc]); 
                    
            var event = cmp.getEvent("pageChangeEvent");
            event.setParams({
                msgType: "PageChanged",
                status:  "validLatLong",
                message: "tab-page-1"
            });
            event.fire();
        }
    },
    validate : function(cmp, event, helper) {
       var isValid = true;
       if(cmp.get("v.appForm.locationDetails.isValidLatLong")){
            isValid &= cmp.find("stateTerritory").validate(); 
            cmp.find("locationDetails").reverse().forEach(function(nestedCmp) {
            	isValid &= nestedCmp.validate();
        	});
           	if(cmp.get("v.appForm.locationDetails.isExistingLocation")){
           		cmp.find("existingLocationId").reverse().forEach(function(nestedCmp) {
            		isValid &= nestedCmp.validate();
        		});
           	}
       }else{
            cmp.set("v.appForm.locationDetails.isValidLatLong", true);
            isValid = false;
       }
       cmp.find("latlong").reverse().forEach(function(nestedCmp) { 
           isValid &= nestedCmp.validate();
       });
       return isValid;
    },

    scrollToTop: function(cmp, event, helper) {
        console.log('inside scrollToTop for NE_MapView');
        helper.scrollToTop(cmp, 'latlong');
    }
    
})