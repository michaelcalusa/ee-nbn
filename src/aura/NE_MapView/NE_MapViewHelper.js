({
	updateMarkers: function(cmp, newLocations) {
    	// this is a workaround due to Salesforce issue, 
    	// https://salesforce.stackexchange.com/questions/233867/lightningmap-doesnt-refresh
    	
        let container = cmp.find('map-container');
        $A.createComponent(
            'lightning:map',
            {
                mapMarkers: newLocations,
                zoomLevel: 16
            },
            function(newMapCmp, status, errorMessage) {
            	if (status === "SUCCESS") {
            		// replace the old map component
            		container.set('v.body', [newMapCmp]); 
                } else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.");
                } else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                }
            }
        );
    }
})