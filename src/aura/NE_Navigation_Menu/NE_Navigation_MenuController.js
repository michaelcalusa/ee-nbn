({   
    clickMenu: function(cmp, event, helper) {
        $A.util.toggleClass(cmp.find('menuButtonId'), 'slds-is-open');
    },
    
    closeMenu: function(cmp, event, helper) {
        window.setTimeout($A.getCallback(function() {
            $A.util.removeClass(cmp.find('menuButtonId'), 'slds-is-open');
        }), 200);
    },
    
    clickMenuItem: function(cmp, event, helper) {
        var menuId = event.target.getAttribute("data-id");
        helper.fireHomePageEvent(cmp, menuId);
        $A.util.removeClass(cmp.find('menuButtonId'), 'slds-is-open');
    },
    
})