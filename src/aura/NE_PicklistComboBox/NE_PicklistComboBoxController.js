({
    doInit : function(component, event, helper) {
        
        if(component.get("v.loadFromPicklist")){
            
            var action = component.get("c.getPicklistData");
            var objName = component.get("v.picklistObjectName");
            var fldName = component.get("v.picklistFieldName");
            
            helper.callApex(component, "c.getPicklistData", {objectName : objName, fieldName :  fldName})
            .then($A.getCallback(function(data){
                
                if(helper.isDebugEnabled(component)){
                	console.log("successfully fetched "+objName+"."+fldName+" picklist");
            	}
                var options = [];
                if ( component.get('v.required') == false ) {
                	options.push({"label":"Select an Option", "value":""});
                }
                data.forEach(function(item) { 
                    options.push({"label": item.label,"value": item.value});
                    if(item.selected){
                        component.set("v.value", item.value);
                    }
                });
                component.set("v.options", options);
	        	 
	     	})).catch($A.getCallback(function(error){
                if(helper.isDebugEnabled(component)){
                	console.log("failed to fetch "+objName+"."+fldName+" picklist");
            	}
	   		}));
		}
 	},
 	onChange : function(component, event, helper) {
        var event = component.getEvent("componentChangeEvent")
        event.setParams({
            msgType: "ComponentChanged",
            status:  "ValueUpdated",
            message: "NE_PicklistComboBox"
        });
        event.fire();
	},
})