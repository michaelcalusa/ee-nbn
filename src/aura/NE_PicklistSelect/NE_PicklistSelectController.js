({
    doInit : function(component, event, helper) {
        var self = this;
        
        if(component.get("v.loadFromPicklist")){
            
            var action = component.get("c.getPicklistData");
            var objName = component.get("v.picklistObjectName");
            var fldName = component.get("v.picklistFieldName");
            /*
            action.setParams({
                objectName : objName,
                fieldName :  fldName
            });   
            action.setCallback(this, function(res) {   
                if (res.getState() == "SUCCESS") { 
                    var options = [];
                    options.push({"class": "optionClass", "label": "Select", "value": ""});
                    res.getReturnValue().forEach(function(item) {
                        options.push({"class": "optionClass", "label": item.label,"value": item.value});
                        if(item.selected){
                            component.set("v.inputValue", item.value);
                        }
                    });
                    component.set("v.options", options);
                } else{
                    console.log("failed to fetch "+objName+"."+fldName+" picklist");
                }
            }); 
            //action.setBackground(true);
            //action.setStorable(); 
            $A.enqueueAction(action);
            */
                 
     		helper.callApex(component, "c.getPicklistData", {objectName : objName, fieldName :  fldName})
            .then($A.getCallback(function(data){
                if(helper.isDebugEnabled(component)){
                	console.log("successfully fetched "+objName+"."+fldName+" picklist");
            	}
                var options = [];
                options.push({"class": "optionClass", "label": "Select", "value": ""});
                data.forEach(function(item) {
                    options.push({"class": "optionClass", "label": item.label,"value": item.value});
                    if(item.selected){
                        component.set("v.inputValue", item.value);
                    }
                });
                component.set("v.options", options);
	        	 
	     	})).catch($A.getCallback(function(error){
                if(helper.isDebugEnabled(component)){
                	console.log("failed to fetch "+objName+"."+fldName+" picklist");
            	}
	   		}));
            
        }
    },
    onChange : function(component, event, helper) {
        //TODO: fire event to signal selected state has changed ?
        if(helper.isDebugEnabled(component)){
        	console.log("Selected option changed");
        }
    },
    
})