<aura:component controller="NE_Application_ReviewController" extends="c:NE_Common_Js" description="NE_Review_Application">
    
    <!-- if appId is not empty, it's in view mode -->
    <aura:attribute name="appId" type="String" default="" />
    <aura:attribute name="showBanner" type="Boolean" default="false" />
    <aura:attribute name="appForm" type="NE_ApplicationFormVO" />
    <aura:registerEvent name="pageChangeEvent" type="c:NE_Message_Event"/>
    <aura:handler name="init" value="{! this }" action="{! c.init }" />	
    
    <aura:if isTrue="{! notequals(v.appId, '') }">
        <c:DF_Section_Header header="Application Summary" />
        
        <div id="appSummary">
            <lightning:layout class="slds-m-bottom_x-large">
                <lightning:layoutItem size="6">
                    <table>
                        <c:NE_LineItem label="Application Number"	value="{! v.appForm.applicationSummary.applicationNumber }" />
                        <c:NE_LineItem label="Updated Date" >
                            <lightning:formattedDateTime value="{! v.appForm.applicationSummary.updatedDate }" day="2-digit" month="2-digit" year="numeric" />
                        </c:NE_LineItem>
                        <c:NE_LineItem label="Created By"			value="{! v.appForm.applicationSummary.createdByName }" />
                    </table>
                </lightning:layoutItem>
                
                <lightning:layoutItem size="6">
                    <table>
                        <c:NE_LineItem label="Quote Status"			value="{! v.appForm.applicationSummary.quoteStatus }" />
                        <aura:if isTrue="{! notequals(v.appForm.applicationSummary.quote, null) }">
                            <c:NE_LineItem label="Quote">
                                <ui:outputCurrency value="{!v.appForm.applicationSummary.quote}" format="$##,##,###,###.00"/>
                            </c:NE_LineItem>
                            <aura:set attribute="else">
                                <c:NE_LineItem label="Quote" 		value="-" />
                            </aura:set>
                        </aura:if>
                    </table>
                </lightning:layoutItem>
            </lightning:layout >
        </div>
    </aura:if>
    
    <c:DF_Section_Header header="Location Details" />
    <div id="locationDetails">
        <lightning:layout>
            <lightning:layoutItem size="6">
                <table>
                    <aura:if isTrue="{! v.appForm.locationDetails.isExistingLocation }">
                        <c:NE_LineItem label="Request Type"     value="{! v.appForm.locationDetails.existingLocationType }" />
                        <aura:set attribute="else">
                            <c:NE_LineItem label="Request Type"     value="New Location" />
                        </aura:set>
                    </aura:if>
                    <c:NE_LineItem label="Latitude"             value="{! v.appForm.locationDetails.latitude }" />
                    <c:NE_LineItem label="Longitude"            value="{! v.appForm.locationDetails.longitude }" />
                    <aura:if isTrue="{! v.appForm.locationDetails.isExistingLocation }">
                        <c:NE_LineItem label="Location ID"          value="{! v.appForm.locationDetails.locationId }" />
                        <aura:if isTrue="{! v.appForm.locationDetails.existingLocationType != 'New Line'}">
                            <c:NE_LineItem label="CPI"                  value="{! v.appForm.locationDetails.cpi }" />
                        </aura:if>
                    </aura:if>
                    <c:NE_LineItem label="Street or Lot Number" value="{! v.appForm.locationDetails.streetNumber }" />
                </table>
            </lightning:layoutItem>
            
            <lightning:layoutItem size="6">
                <table>
                    <c:NE_LineItem label="Street Name"              value="{! v.appForm.locationDetails.streetName }" />
                    <c:NE_LineItem label="Town/Suburb"              value="{! v.appForm.locationDetails.suburb }" />
                    <c:NE_LineItem label="Postcode"                 value="{! v.appForm.locationDetails.postcode }" />
                    <c:NE_LineItem label="State/Territory"          value="{! v.appForm.locationDetails.state }" />
                    <aura:if isTrue="{! v.appForm.locationDetails.isExistingLocation }">
                        <c:NE_LineItem label="Service ID - FNN/ULL"     value="{! v.appForm.locationDetails.serviceId }" />
                    </aura:if>
                </table>
            </lightning:layoutItem>
        </lightning:layout >
        <lightning:layout class="slds-m-bottom_x-large">
            <lightning:layoutItem size="12">
                <table>
                    <c:NE_LineItem width="25%" label="Additional Details regarding location" value="{!v.appForm.locationDetails.notes }" />
                </table>
            </lightning:layoutItem>
        </lightning:layout>
    </div>
    
    <c:DF_Section_Header header="Application/Service Details" />
    <lightning:layout>
        <lightning:layoutItem size="6">
            <table id="serviceDetails">
                <c:NE_LineItem label="Service Details" class="nbn-text-heading_extra-small" />
                <c:NE_LineItem label="Application Type"                 value="{! v.appForm.serviceDetails.applicationType }" />
                <c:NE_LineItem label="Application Unique Identifier"    value="{! v.appForm.serviceDetails.applicationUniqueId }" />
                <c:NE_LineItem label="Service End Point"                value="{! v.appForm.serviceDetails.serviceEndPoint }" />
                <c:NE_LineItem label="Service End Point Identifier"     value="{! v.appForm.serviceDetails.serviceEndPointId }" />
            </table>
        </lightning:layoutItem>
        
        <lightning:layoutItem size="6">
            <table id="serviceRequirements">
                <c:NE_LineItem label="Service Requirements" class="nbn-text-heading_extra-small" />
                <c:NE_LineItem label="Traffic Class 4" value="{! v.appForm.serviceRequirements.trafficClass4 }" />
                <c:NE_LineItem label="Traffic Class 2" value="{! v.appForm.serviceRequirements.trafficClass2 }" />
                <c:NE_LineItem label="Traffic Class 1" value="{! v.appForm.serviceRequirements.trafficClass1 }" />
            </table>
        </lightning:layoutItem>
    </lightning:layout>
    
    <div id="deliveryRequirements">
        <lightning:layout>
            <lightning:layoutItem size="6" class="slds-p-top_large">
                <table>
                    <c:NE_LineItem label="Delivery Requirements" class="nbn-text-heading_extra-small" />
                    <c:NE_LineItem label="Proposed Project Delivery Date" >
                        <lightning:formattedDateTime value="{! v.appForm.deliveryRequirements.proposedProjectDeliveryDate }"
                                                     day="2-digit" month="2-digit" year="numeric" />
                    </c:NE_LineItem>
                </table>
            </lightning:layoutItem>
        </lightning:layout>
        <lightning:layout class="slds-m-bottom_x-large">
            <lightning:layoutItem size="12">
                <table>
                    <c:NE_LineItem width="25%" label="Additional details regarding application" value="{! v.appForm.deliveryRequirements.additionalDetails }" />
                    <aura:if isTrue="{! equals(v.appId, '') }">
                        <c:NE_LineItem width="25%" label="Attached File">
                            <!--
                            <aura:iteration items="{!v.appForm.deliveryRequirements.attachments}" var="attachment">
                                <a href="{!'/bsm/servlet/servlet.FileDownload?file=' + attachment.documentId}" download="{!attachment.fileName}" target="_blank">{! attachment.fileName}</a> <br/>
                            </aura:iteration>
                            -->
                            <aura:iteration items="{!v.appForm.deliveryRequirements.fileNames}" var="fileName">
                                {! fileName } <br/>
                            </aura:iteration>
                    </c:NE_LineItem>
                    </aura:if>
                </table>
            </lightning:layoutItem>
        </lightning:layout>
    </div>
    
    <c:DF_Section_Header header="Access Seeker Details" />
    <div id="Access Seeker Details">
        <lightning:layout class="slds-m-bottom_x-large">
            <lightning:layoutItem size="6">
                <table id="accessSeekerDetails">
                    <c:NE_LineItem label="Access Seeker Details" class="nbn-text-heading_extra-small" />
                    <c:NE_LineItem label="Access Seeker Id"             value="{! v.appForm.asDetails.asId }" />
                    <c:NE_LineItem label="First Name"                   value="{! v.appForm.asDetails.firstName }" />
                    <c:NE_LineItem label="Last Name"                    value="{! v.appForm.asDetails.lastName }" />
                    <c:NE_LineItem label="Contact Number"               value="{! v.appForm.asDetails.contactNumber }" />
                    <c:NE_LineItem label="Additional Contact Number"    value="{! v.appForm.asDetails.additionalContactNumber }" />
                    <c:NE_LineItem label="Email"                        value="{! v.appForm.asDetails.email }" />
                </table>
            </lightning:layoutItem>
        </lightning:layout>
    </div>

    <div id="Access Seeker Billing Details">
        <lightning:layout class="slds-m-bottom_x-large">
            <lightning:layoutItem size="6">
                <table id="billingDetails">
                    <c:NE_LineItem label="Access Seeker Billing Details" class="nbn-text-heading_extra-small" />
                    <c:NE_LineItem label="Billing Account ABN"                          value="{! v.appForm.billDetails.abn }" />
                    <c:NE_LineItem label="Billing Account Name"         value="{! v.appForm.billDetails.companyName }" />
                    <c:NE_LineItem label="Billing Contact First Name"                   value="{! v.appForm.billDetails.firstName }" />
                    <c:NE_LineItem label="Billing Contact Last Name"                    value="{! v.appForm.billDetails.lastName }" />
                    <c:NE_LineItem label="Billing Contact Phone Number"               value="{! v.appForm.billDetails.contactNumber }" />
                    <c:NE_LineItem label="Additional Billing Contact Phone Number"    value="{! v.appForm.billDetails.additionalContactNumber }" />
                    <c:NE_LineItem label="Billing Contact Email ID"                        value="{! v.appForm.billDetails.email }" />
                </table>
            </lightning:layoutItem>
            <lightning:layoutItem size="6">
                <table id="billingAddress">
                    <c:NE_LineItem label="Billing Address" class="nbn-text-heading_extra-small" />
                    <c:NE_LineItem label="Street"              	value="{! v.appForm.billDetailsAddress.street }" />
                    <c:NE_LineItem label="City"         		value="{! v.appForm.billDetailsAddress.city }" />
                    <c:NE_LineItem label="Postcode"              value="{! v.appForm.billDetailsAddress.postcode }" />
                    <c:NE_LineItem label="State/Territory"      value="{! v.appForm.billDetailsAddress.state }" />
                </table>
            </lightning:layoutItem>
        </lightning:layout>
    </div>

    <c:DF_Section_Header header="Contact Details" />
    <div>
        <lightning:layout class="slds-m-bottom_large">
            <lightning:layoutItem size="6">
                <table id="businessDetails">
                    <c:NE_LineItem label="Business Details" class="nbn-text-heading_extra-small" />
                    <c:NE_LineItem label="ABN"                      value="{! v.appForm.businessDetails.abn }" />
                    <c:NE_LineItem label="Business Name"            value="{! v.appForm.businessDetails.companyName }" />
                    <c:NE_LineItem label="Business Contact Number"  value="{! v.appForm.businessDetails.contactNumber }" />
                    <c:NE_LineItem label="Business Email"           value="{! v.appForm.businessDetails.email }" />
                </table>
            </lightning:layoutItem>

            <lightning:layoutItem size="6">
                <table id="primaryContact">
                    <c:NE_LineItem label="Primary Contact Details" class="nbn-text-heading_extra-small" />
                    <c:NE_LineItem label="Role"             value="{! v.appForm.primaryContactDetails.role }" />
                    <c:NE_LineItem label="First Name"       value="{! v.appForm.primaryContactDetails.firstName }" />
                    <c:NE_LineItem label="Last Name"        value="{! v.appForm.primaryContactDetails.lastName }" />
                    <c:NE_LineItem label="Contact Number"   value="{! v.appForm.primaryContactDetails.contactNumber }" />
                    <c:NE_LineItem label="Email"            value="{! v.appForm.primaryContactDetails.email }" />
                </table>
            </lightning:layoutItem>
        </lightning:layout>
    </div>
    <div>
        <lightning:layout class="slds-m-bottom_x-large">
            <lightning:layoutItem size="6">
                <table id="secondaryContact">
                    <c:NE_LineItem label="Secondary Contact Details" class="nbn-text-heading_extra-small" />
                    <c:NE_LineItem label="Role" value="{! v.appForm.secondaryContactDetails.role }" />
                    <c:NE_LineItem label="First Name" value="{! v.appForm.secondaryContactDetails.firstName }" />
                    <c:NE_LineItem label="Last Name" value="{! v.appForm.secondaryContactDetails.lastName }" />
                    <c:NE_LineItem label="Contact Number" value="{! v.appForm.secondaryContactDetails.contactNumber }" />
                    <c:NE_LineItem label="Email" value="{! v.appForm.secondaryContactDetails.email }" />
                </table>
            </lightning:layoutItem>

            <lightning:layoutItem size="6">
                <table id="siteContact">
                    <c:NE_LineItem label="Site Contacts" class="nbn-text-heading_extra-small" />
                    <c:NE_LineItem label="ABN"              value="{! v.appForm.siteContacts.abn }" />
                    <c:NE_LineItem label="Company Name"     value="{! v.appForm.siteContacts.companyName }" />
                    <c:NE_LineItem label="Role"             value="{! v.appForm.siteContacts.role }" />
                    <c:NE_LineItem label="First Name"       value="{! v.appForm.siteContacts.firstName }" />
                    <c:NE_LineItem label="Last Name"        value="{! v.appForm.siteContacts.lastName }" />
                    <c:NE_LineItem label="Contact Number"   value="{! v.appForm.siteContacts.contactNumber }" />
                    <c:NE_LineItem label="Email"   value="{! v.appForm.siteContacts.email }" />
                </table>
            </lightning:layoutItem>
        </lightning:layout>
    </div>
    <div>
        <aura:if isTrue="{! equals(v.appId, '') }">
            <hr class="slds-m-bottom_medium" />

            <c:NE_Section_Heading header="Contract Agreement" />
            <lightning:layout >
                <lightning:layoutItem>
                    <lightning:input type="checkbox"
                                     name="contractAgreed"
                                     label="I acknowledge that each nbn&trade; Network Extension application request may incur a non-refundable application fee of $550 (incl. GST) to be paid in full before the preparation of the design and construction quotation – this fee covers the preparation of the quotation for the proposed work."
                                     checked="{! v.appForm.contract.contractAgreed }"
                                     onchange="{! c.handleChange }"/>
                </lightning:layoutItem>
            </lightning:layout>
        </aura:if>
    </div>
</aura:component>