({
    init: function (cmp, event, helper) {
        var appId = cmp.get("v.appId");
        if($A.util.isEmpty(appId)) {
            // not in view mode
            return;
        }
        
        helper.clearMessage(cmp);
        
        helper.callApex(cmp, "c.viewApplication", { 'appId' : appId })
        .then($A.getCallback(function (appForm) {
            cmp.set("v.appForm", appForm);
            
            if (cmp.get("v.showBanner")) {
                helper.setOkBanner(cmp, 'Successfully Submitted Application, ID ' + appId);
                cmp.set("v.showBanner", false);
            } 
        })).catch($A.getCallback(function (error) {
            var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
            helper.setErrorBanner(cmp, ERR_MSG_APP_ERROR);
        }));   
    },
    
    handleChange: function (cmp, event, helper) {
        var evt = cmp.getEvent("pageChangeEvent")
        evt.setParams({
            msgType: "PageChanged",
            status:  "ValueUpdated",
            message: "tab-page-5"
        });
        evt.fire();
    },

})