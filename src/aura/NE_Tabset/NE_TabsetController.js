/**
 * Created by philipstafford-jones on 20/11/18.
 */
({
    doInit: function(cmp, event, helper) {
        helper.callApex(cmp, "c.initApplicationForm", {})
            .then($A.getCallback(function(data){
                cmp.set("v.appForm", data);
            }));
    },
    nextTabButton: function (cmp, event, helper) {
            var tabNumber = Number(cmp.get('v.selectedTabId'));
            var isValid = helper.validatePage(cmp, "tab-page-" + tabNumber.toString());
            if (isValid == true) {
                tabNumber++;
                if (tabNumber > cmp.get('v.lastCompletedTabNumber')) {
                    cmp.set('v.lastCompletedTabNumber', tabNumber);
                }
                cmp.set('v.selectedTabId', tabNumber.toString());
                window.scrollTo(0,0);
            }
        },
    backTabButton: function (cmp, event, helper) {
            var tabNumber = Number(cmp.get('v.selectedTabId')) - 1;
            cmp.set('v.selectedTabId', tabNumber.toString() );
            window.scrollTo(0,0);
        },
    setSelectedTab : function(cmp, event, helper) {
        // update the progress bar
        var selected = cmp.get("v.selectedTabId");
        cmp.find("progress").set("v.currentStep", selected);
    },
    selectTab : function (cmp, event, helper) {

        var selectedTabNumber = Number(cmp.get('v.selectedTabId'));
        var lastCompletedTabNumber = cmp.get('v.lastCompletedTabNumber')

        console.log(" selected Tab: " + selectedTabNumber +
                    " lastCompleted: " + lastCompletedTabNumber);

        // validate the current page and check if tabs disabled
        if ( cmp.get('v.disableTabs') == true ) {
            cmp.set('v.selectedTabId', currTab);
            return;
        }

        // lock the flow to not proceed beyond an invalid page
        if (cmp.get('v.enableFlowLock') == true) {
            var lastValidPage = helper.getLastValidPage(cmp, lastCompletedTabNumber);

            // allow to select one beyond last valid page
            if ( selectedTabNumber > lastValidPage + 1 ) {
                cmp.set('v.selectedTabId', (lastValidPage+1).toString() );
            }

            if ( lastValidPage  >= lastCompletedTabNumber ) {
                cmp.set('v.lastCompletedTabNumber', lastValidPage);
            }

            console.log(" selected Tab: " + selectedTabNumber +
                        " lastCompleted: " + lastCompletedTabNumber +
                        " lastValidPage: " + lastValidPage);
        }

        // refresh the last page data and update the current Tab number
        var currTab = cmp.get('v.currTab');
        if (currTab != cmp.get('v.selectedTabId')) {
        	helper.firePageChangeEvent(cmp, "tab-page-" + currTab);
            cmp.set('v.currTab', cmp.get('v.selectedTabId'));
        }
    },

    handlePageChangeEvent : function(cmp,event) {
      var tabNumber = event.getParam("message");
      var pageComponent = cmp.find(tabNumber);
      cmp.set("v.appForm", pageComponent.get("v.appForm"));
      event.stopPropagation();
    },

    submitTabSetButton: function (cmp, event, helper) {
        var appForm = cmp.get('v.appForm');
        //console.log(appForm);
        
        helper.clearMessage(cmp);

        helper.callApex(cmp, "c.createApplication", {'appForm':JSON.stringify(appForm)} )
                .then($A.getCallback(function (createApplicationResponse) {
                    console.log(createApplicationResponse);
                    var eventToFire = cmp.getEvent("viewOppEvent");
                    var jsonResponse =  JSON.parse(createApplicationResponse);
                    if (jsonResponse.status === 'OK') {
                        // show banner on view page instead
                        eventToFire.setParams({"oppBundleId": jsonResponse.body+",showBanner"});
                        eventToFire.fire();
                    } else if (jsonResponse.status === 'TIMEOUT') {
                        console.log("The user session has timed out.");
                        helper.setErrorBanner(cmp, jsonResponse.body);
                    }
                })).catch($A.getCallback(function (error) {
                    console.log("error: " + error);
            		var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
                    helper.setErrorBanner(cmp, ERR_MSG_APP_ERROR);
                }));
    }

});