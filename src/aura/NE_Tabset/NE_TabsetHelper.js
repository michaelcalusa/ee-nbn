/**
 * Created by philipstafford-jones on 28/11/18.
 */
({
    firePageChangeEvent: function (cmp, pageName) {
        console.log("Firing pageChangeEvent = " + pageName);
        var event = cmp.getEvent("pageChangeEvent");
        event.setParams({
            msgType: "PageChanged",
            status:  "ValueUpdated",
            message: pageName
        });
        event.fire();
        console.log("Fired pageChangeEvent = " + pageName);
    },
    validatePage: function(cmp, pageId) {
        var pageComponent = cmp.find(pageId);
        if (pageComponent.validate != undefined) {
            var status = pageComponent.validate();
            cmp.set("v.appForm", pageComponent.get("v.appForm"));
            if (status == false) {
                return false;
            }
        } else {
            console.log("WARNING: Validator not defined for component with aura:id = " + pageId);
            console.log(" must add <aura:method name='validate' /> to your component");
        }
        return true;
    },
    getLastValidPage: function(cmp, selectedTabNumber) {
        var page = 1;
        while ( page < selectedTabNumber && this.validatePage(cmp, 'tab-page-' + page) == true ) {
            page++
        };
        page--;
        console.log("LastValidPage: " + page);
        return page;
    },

})