({
	doInit: function(component, event, helper) { 
        console.log('==== doInit ====');
         helper.getCustomerAccounts(component);
    },
    
    handleAccountChange: function(component, event, helper) { 
        console.log('==== fn:handleAccountChange ====');
        helper.getAccountRelatedDetails(component); 
    },
    
    resetDropdown: function(component, event, helper) { 
        console.log('==== fn:resetDropdwon ====');
        var changevalue = component.get("v.changeValue");
        if(changevalue == 'searchBox'){
            component.find("accountNameId").set("v.value","Select Customer Account");
        }
    }
})