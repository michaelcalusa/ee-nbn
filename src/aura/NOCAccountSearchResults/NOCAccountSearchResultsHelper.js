({
	getCustomerAccounts: function(component){
        console.log('==== Helper - getCustomerAccounts =====');         
        var action = component.get("c.getAccountsList"); 
        action.setParams({
            "accRecordType": "Customer"
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if (state === "SUCCESS") {                
                component.set("v.customerAccountsList", response.getReturnValue()); 
            } 
        });
        $A.enqueueAction(action);
    },
    
    getAccountRelatedDetails: function(component){
         var action = component.get("c.getContactsList"); 
         console.log('==accname==='+ component.find("accountNameId").get("v.value"));
        action.setParams({
            "accRecordId": component.find("accountNameId").get("v.value"),
            "contactType": '',
            "fieldToSort": '',
            "SortingOrder": ''
        });
         action.setCallback(this, function(response){
            var state = response.getState();            
            if (state === "SUCCESS") { 
              console.log('displayData ---->' + JSON.stringify(response.getReturnValue()));
                
              component.set("v.tableDataToDisplay", response.getReturnValue());
                component.set("v.accountName", component.find("accountNameId").get("v.value"));
                var cusRecords=[];
			    var nbnRecords=[];
                var isSearchResult = 'false';
                var finalResult = component.get("v.tableDataToDisplay");
                if(finalResult!='')
                {
                    var source = finalResult[0].searchSource;
                    var conT = finalResult[0].conType;
                    isSearchResult = 'True';
                    for(var x in finalResult)
                    {
                        var contype = finalResult[x].conType
                        if(contype == 'nbn')
                        {
                            nbnRecords.push(finalResult[x]);
                        }
                        if(contype == 'customer')
                        {
                            cusRecords.push(finalResult[x]);
                        }	
                    }
                }
                
                component.set("v.nbnContactList", nbnRecords);
                component.set("v.customerContactList", cusRecords);
                var myEvent = $A.get("e.c:SearchAccountChange");
        	    myEvent.setParams({
                  "searchResult": component.get("v.tableDataToDisplay"),
                  "isSearchResult":isSearchResult,
                  "accountName":component.get("v.accountName") ,   
                  "searchSource":source,
                  "nbnContactList": component.get("v.nbnContactList"),
                  "customerContactList":component.get("v.customerContactList")
               });
       		 myEvent.fire();
            }
        });
     $A.enqueueAction(action);
        
        
    }
})