({
    doInit: function(component, event, helper) { 
        console.log('*** doInit ***');
        
        //var searchSectionId = component.find("searchSectionId");
       // $A.util.addClass(searchSectionId, 'hideMe'); 
        
    }, 
    
	getAccountRelatedDetails : function(component, event, helper) {
		var SearchResult = event.getParam("searchResult");
        var isSearchResult = event.getParam("isSearchResult");
        var accName = event.getParam("accountName");
        var searchSource = event.getParam("searchSource");
        var nbnConResult = event.getParam("nbnContactList");
        var cusConResult = event.getParam("customerContactList");
        var selectedContactName = event.getParam("selectedContactName");
        console.log('nbnconresult ==>'+ nbnConResult);
        component.set("v.tableDataToDisplay", SearchResult);  
        component.set("v.isSearchResult", isSearchResult);  
        component.set("v.accountName", accName);   
        component.set("v.searchSource", searchSource); 
         component.set("v.nbnContactList", nbnConResult); 
         component.set("v.customerContactList", cusConResult); 
        component.set("v.selectedContactName", selectedContactName);
	},
    
    handleSortingFeature: function(component, event, helper){ 
        console.log("==== handleSortingFeature ====");
         var contacttype = event.currentTarget.dataset.contacttype;
         var fieldlabel  = event.currentTarget.dataset.fieldlabel;
         var sortingorder;
         var currentSortingMap; 
        
        if(contacttype == 'nbn')
        {
            currentSortingMap = component.get("v.nbnSortingMap");
        }
        else if(contacttype == 'customer')
        {
            currentSortingMap = component.get("v.cusSortingMap");
        }
        sortingorder = currentSortingMap[fieldlabel];
        if(sortingorder == 'ASC')
        {
            sortingorder = 'DESC'
        }
        else if(sortingorder == 'DESC')
        {
            sortingorder = 'ASC'
        }
        
        var searchSource = component.get("v.searchSource");
        console.log('=== searchSource ===='+searchSource);
        if(searchSource == 'accDropdown')
        {
            console.log('=== Logic Block - accDropdown====');
            var action = component.get("c.getContactsList"); 
        	action.setParams({
            "accRecordId": component.get("v.accountName"),
            "contactType": contacttype,
            "fieldToSort":  fieldlabel,
            "SortingOrder": sortingorder            
            });
        }
        else if(searchSource == 'searchBox')
        {
            console.log('=== Logic Block - searchBox====');
            console.log('=== selectedContactName====' + component.get("v.selectedContactName"));
              console.log("==== contacttype ==== "+ contacttype);
        console.log("==== fieldlabel ==== "+ fieldlabel);
            var action = component.get("c.getContacts"); 
            action.setParams({
            "strContactName": component.get("v.selectedContactName"),
            "contactType": contacttype,
            "fieldToSort": fieldlabel,
            "SortingOrder": sortingorder
            });
        }
       
         action.setCallback(this, function(response){
            var state = response.getState();            
            if (state === "SUCCESS") { 
              console.log('displayData ---->' + JSON.stringify(response.getReturnValue()));
              component.set("v.tableDataToDisplay", response.getReturnValue());
                var finalResult = component.get("v.tableDataToDisplay");
               
                var conT = finalResult[0].conType;
                var cusRecords=[];
			    var nbnRecords=[];
			    
                for(var x in finalResult)
                {
                    var contype = finalResult[x].conType
                    if(contype == 'nbn')
                    {
                        nbnRecords.push(finalResult[x]);
                    }
                    if(contype == 'customer')
                    {
                        cusRecords.push(finalResult[x]);
                    }	
                }
                
                if(contacttype == 'nbn')
                {
                    component.set("v.nbnContactList", nbnRecords);
                    component.set("v.nbnSortedField", fieldlabel);
                }
                else if(contacttype == 'customer')
                {
                     component.set("v.customerContactList", cusRecords); 
                     component.set("v.cusSortedField", fieldlabel);
                }
  
                if(response.getReturnValue().length>0){
                    if(sortingorder=='ASC'){
                        currentSortingMap[fieldlabel] = 'ASC';
                        if(contacttype == 'customer')
                        {
                            component.set("v.cusSortingOrder", fieldlabel+'ASC');
                        }
                        else if(contacttype == 'nbn')
                        {
                            component.set("v.nbnSortingOrder", fieldlabel+'ASC');
                        }
                    } 
                    if(sortingorder=='DESC'){
                        currentSortingMap[fieldlabel] = 'DESC';
                        if(contacttype == 'customer')
                        {
                            component.set("v.cusSortingOrder", fieldlabel+'DESC');
                        }
                        else if(contacttype == 'nbn')
                        {
                            component.set("v.nbnSortingOrder", fieldlabel+'DESC');
                        }
                    } 
                    console.log('===nbnSortignOrder==='+component.get("v.nbnSortingOrder"));
                    console.log('===cusSortignOrder==='+component.get("v.cusSortingOrder"));
                }
            }
        });
     $A.enqueueAction(action);        
    },
    handleSearchChange: function(component, event, helper){ 
        console.log('======= handle Search Change =======');       
    }

})