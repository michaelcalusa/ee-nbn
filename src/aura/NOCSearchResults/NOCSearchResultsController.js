({
	keyPressController : function(component, event, helper) {
        console.log('** keyPressController **');
        var getInputkeyWord = component.get("v.conName"); 
        if( getInputkeyWord.length > 3 ){
            var findTypeahead = component.find("typeahead-00B-9722");
            $A.util.addClass(findTypeahead, 'lookup_menu_open');
            $A.util.removeClass(findTypeahead, 'lookup_menu_close');
            
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
            component.set("v.listOfSearchRecords", null ); 
            component.set("v.SearchMessage", '');
            var forclose = component.find("typeahead-00B-9722");
            $A.util.addClass(forclose, 'lookup_menu_close');
            $A.util.removeClass(forclose, 'lookup_menu_open');
        }         
    },
    
    selectContact : function(component, event, helper){
        console.log('**** selectContact *****');
        component.set("v.SearchMessage", '');
        component.set("v.SearchMessage", '');
        var getSelectContactName = event.currentTarget.dataset.contactname;
        
        helper.contactDetailsHelper(component,event,getSelectContactName);

		var forsearch = component.find("searchContactTextId");
        forsearch.set("v.value",getSelectContactName);
        
        var forclose = component.find("typeahead-00B-9722");
        $A.util.addClass(forclose, 'lookup_menu_close');
        $A.util.removeClass(forclose, 'lookup_menu_open');
    },
    
    resetSearchBox: function(component, event, helper) { 
        console.log('==== fn:resetSearchBox ====');
 		var searchBox = component.find("searchContactTextId");
        var changevalue = component.get("v.changeValue");
        if(changevalue == 'accDropdown'){
            searchBox.set("v.value",'');
        }  
    }
})