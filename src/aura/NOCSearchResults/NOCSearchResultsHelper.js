({
	searchHelper : function(component,event,getInputkeyWord) {
        console.log('** Helper - searchHelper **');
        var action = component.get("c.searchContact"); 
        action.setParams({
            'searchKeyWord': getInputkeyWord
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // if storeResponse size is equal 0 ,display No Result Found... message on screen.
                console.log('*** length'+ storeResponse.length);
                if (storeResponse.length == 0) {
                    console.log('No Result Found...');
                    component.set("v.SearchMessage", 'No results found');
                    console.log('return value'+ component.get("v.SearchMessage"));
                } else {
                    component.set("v.SearchMessage", '');
                }                
                // set searchResult list with return value from server.
                // console.log('** StoreResponse ==>'+ storeResponse);
                component.set("v.listOfSearchRecords", storeResponse);
            }     
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    },
    
	contactDetailsHelper : function(component,event,getSelectContactName) {
        console.log('** Helper - contactDetailsHelper **');
		var action = component.get("c.getContacts"); 
        action.setParams({
            'strContactName': getSelectContactName,
             'contactType': '',
            'fieldToSort':  '',
            'SortingOrder': ''
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                // set searchResult list with return value from server.
                // console.log('** StoreResponse ==>'+ storeResponse);
                component.set("v.listOfContactRecords", storeResponse);
                var finalResult = component.get("v.listOfContactRecords");
                var source = finalResult[0].searchSource;
                console.log('=== source ==='+ source);
                
                 var cusRecords=[];
			    var nbnRecords=[];
			    
                for(var x in finalResult)
                {
                    var contype = finalResult[x].conType
                    if(contype == 'nbn')
                    {
                        nbnRecords.push(finalResult[x]);
                    }
                    if(contype == 'customer')
                    {
                        cusRecords.push(finalResult[x]);
                    }	
                }
                component.set("v.nbnContactList", nbnRecords);
                component.set("v.customerContactList", cusRecords);
                
                /// SUKUMAR EVENT LOGIC
                var myEvent = $A.get("e.c:SearchAccountChange");
        	myEvent.setParams({
                "searchResult": component.get("v.listOfContactRecords"),
                "isSearchResult":'True',
                "accountName":'',
                "searchSource": source,
                "nbnContactList": component.get("v.nbnContactList"),
                "customerContactList":component.get("v.customerContactList"),
                "selectedContactName":getSelectContactName
            });
       		 myEvent.fire();
                
            }     
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    }
})