/**
 * Created by philipstafford-jones on 20/3/19.
 */
({
    CUSTOM_SETTING_PAGE_URL_SF: 'PAGE_URL_SF',
    CUSTOM_SETTING_PAGE_URL_ORDER: 'PAGE_URL_ORDER',
    ORDER_ID_PARAM_NAME: 'orderId',
    PAGE_PARAM_NAME: 'page',
    PARENT_OPP_ID_PARAM_NAME: 'parentOppId',

    //This method is used to make server calls with Promise
    apex: function (cmp, cmpAttrName, apexAction, params, isSerialized) {
        var self = this;
        var result = new Promise($A.getCallback(
            function (resolve, reject) {
                
                var action = cmp.get(apexAction);
                action.setParams(params);
                action.setCallback(this, function (callbackResult) {
                    try {
                        var state = callbackResult.getState();
                        if (state === 'SUCCESS') {
                            var returnValue = (isSerialized) ? JSON.parse(callbackResult.getReturnValue()) : callbackResult.getReturnValue();
                            cmp.set(cmpAttrName, returnValue);
                            var results = {
                                sourceCmp: cmp,
                                sourceHelper: self,
                                data: returnValue,
                                sourceAction: apexAction
                            };
                            resolve(results);
                        } else if (state === 'ERROR') {
                            console.log('ERROR', callbackResult.getError());
                            var results = {
                                sourceCmp: cmp,
                                sourceHelper: self,
                                ERROR_RESPONSE_STATUS: 'ERROR',
                                ERROR_RESPONSE_TYPE: 'Banner',
                                error: callbackResult.getError(),
                                sourceAction: apexAction
                            };
                            reject(results);
                        } else {
                            // INCOMPLETE, ABORTED, etc.
                            var results = {
                                sourceCmp: cmp,
                                sourceHelper: self,
                                ERROR_RESPONSE_STATUS: 'ERROR',
                                ERROR_RESPONSE_TYPE: 'Banner',
                                error: 'Failed on calling Apex method: ' + apexAction + ', state: ' + state,
                                sourceAction: apexAction
                            };
                            reject(results);
                        }
                    } catch (e) {
                        console.log('Catching exception: ' + e.message);
                    }
                });
                $A.get("e.c:NS_Spinner_Event").fire();
                $A.enqueueAction(action, false);
            }));
        
        return result;
    },

    getApexProxy: function(theLightningComponent, theApexMethodName, doNotCloseSpinner){
        
        //------------------ Start Define Action Proxy Object -------------------------
        function ProxiedAction(theLightningComponent, theApexMethodName, doNotCloseSpinner){
            this.theLightningComponent = theLightningComponent;
            this.theApexMethodName = theApexMethodName;
            this.actionDelegate = theLightningComponent.get(theApexMethodName);
            this.doNotCloseSpinner = doNotCloseSpinner;
            if (this.doNotCloseSpinner == true) {
                console.log('doNotCloseSpinner is set');
            }
        };
        ProxiedAction.prototype.setParams = function(configObj){
            
            this.actionDelegate.setParams(configObj);
        };
        ProxiedAction.prototype.setCallback = function(theLightningComponent, callbackFunction){
            var self = this;
            this.actionDelegate.setCallback(theLightningComponent, function(response) {

                // console.log("ProxiedAction beginning of callback");
                try{
                    // console.log("ProxiedAction callback to delegate");
                    callbackFunction(response);
                } catch(e) {
                    console.log("ProxiedAction error in callback to delegate. Be sure to replace reference to 'this' with reference to 'helper' in callback function.");
                    console.log(e);
                };
                // console.log("ProxiedAction end of callback");
                // handle spinner etc.
                // if (self.doNotCloseSpinner !== true) {
                    var evt = $A.get('e.c:NS_Spinner_Event');
                    evt.setParam('show', false);
                    evt.fire();
                // } else {
                //     console.log('DO NOT CLOSE SPINNER SET !!!')
                // }
            })
        };
        ProxiedAction.prototype.delegate = function(){
            var evt = $A.get('e.c:NS_Spinner_Event');
            evt.fire();
            return this.actionDelegate;
        };
        //------------------ End Define Action Proxy Object -------------------------

        return new ProxiedAction(theLightningComponent, theApexMethodName, doNotCloseSpinner);
    },
    
    getUrlParameter: function (cmp, paramName) {

        var url = decodeURIComponent(window.location.href);
        var regex = new RegExp("[?&]" + paramName + "(=([^&#]*)|&|#|$)");
        var results = regex.exec(url);

        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));

    },

    isSelectEmpty: function (value) {
        return (!value || value === "Select" || value === "" || value === "null" || value ==="--None--");
    },

    fireHideSpinnerEvent : function(cmp) {
        var evt = $A.get('e.c:NS_Spinner_Event');
        evt.setParam('show', false);
        evt.fire();
    },

    //This helper method is to resolve the issue of validation message not showing until focus twice
    isAllFieldsValid: function (cmp, fieldGroupName) {
        var allValid = cmp.find(fieldGroupName).reduce(
            function (validFields, inputCmp) {
                inputCmp.focus();
                inputCmp.focus();
                inputCmp.showHelpMessageIfInvalid();
                return validFields && inputCmp.get('v.validity').valid;
            }, true);

        return allValid;
    },

    getComponentByName: function(name){
        return function(item) {
            return item.get("v.name") == name;
        };
    },

    setValueMissing: function(inputCmp){
        inputCmp.set('v.validity', { valid:false, valueMissing :true });
    },

    validateSelectCmpEmpty: function(inputCmp){
        if (this.isSelectEmpty(inputCmp.get("v.value"))) {
            this.setValueMissing(inputCmp);
            inputCmp.showHelpMessageIfInvalid();
        }
    },

    reFocus: function(inputCmp, tempCtrl){
        inputCmp.focus();
        tempCtrl.focus();
        inputCmp.focus();
    },

    updateColumnSorting: function(cmp, event, helper, fullList) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');

        // Assign the latest attribute with the sorted column fieldName and sorted direction
        if($A.util.isEmpty(fieldName) || $A.util.isUndefined(fieldName)) {
            fieldName = cmp.get("v.sortedBy");
        }
        if($A.util.isEmpty(sortDirection) || $A.util.isUndefined(sortDirection)) {
            sortDirection = cmp.get("v.sortedDirection");
        }

        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);

        helper.sortData(cmp, fieldName, sortDirection, fullList);
        helper.sliceList(cmp, fullList, 'v.currentList')
    },

    sortData: function(cmp, fieldName, sortDirection, dataAttribute) {
        if (!dataAttribute) {
            dataAttribute = 'v.data';
        }
        var data = cmp.get(dataAttribute);
        var reverse = sortDirection !== 'asc';
        // Sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set(dataAttribute, data);
    },

    sliceList: function (cmp, fullList, slicedList ) {
        var records = cmp.get(fullList),
            pageNumber = cmp.get("v.pageNumber"),
            pageCountVal = cmp.get("v.selectedCount"),
            pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));
        cmp.set(slicedList, pageRecords);
        if(records.length > 0) {
            cmp.set("v.maxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
        } else {
            cmp.set("v.maxPage", 1);
        }
    },

    updateColumnSortingWithSubProperty: function(cmp, event, helper){
        var sortedBy = cmp.get('v.sortedBy');
        var sortedDirection = cmp.get('v.sortedDirection');

        var fieldName = event.target.getAttribute("data-column");
        var subPropertyName = event.target.getAttribute("data-column-sub-property");

        var asc = 'asc';
        var desc = 'desc';

        if (fieldName == sortedBy) {
            if (sortedDirection == asc)
                sortedDirection = desc;
            else
                sortedDirection = asc;
        } else {
            cmp.set('v.sortedBy', fieldName);
            sortedDirection = desc;
        }

        cmp.set('v.sortedDirection', sortedDirection);

        if($A.util.isEmpty(subPropertyName) || $A.util.isUndefined(subPropertyName)) {
            helper.sortListData(cmp, fieldName, sortedDirection);
        }else{
            helper.sortListDataBySubProperty(cmp, fieldName, subPropertyName, sortedDirection);
        }
    },

    sortListDataBySubProperty: function(cmp, fieldName, subPropertyName, sortDirection) {
        cmp.set('v.sortedBySubProperty', subPropertyName);

        var data = cmp.get("v.fullList");
        var reverse = sortDirection !== 'asc';

        data.sort(this.sortBySubProperty(fieldName, reverse, subPropertyName))
        cmp.set("v.fullList", data);

        this.updateDatatableAndFooter(cmp);
    },

    sortBySubProperty: function(field, reverse, subPropertyName, primer) {
        var subProperty = subPropertyName;
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {
                if(x[field] != undefined && x[field][subProperty] != undefined)
                    return x[field][subProperty];
                else
                    return '';
            };

        reverse = !reverse ? 1 : -1;

        return function (a, b) {
            return a = key(a) ? key(a) : '', b = key(b) ? key(b): '', reverse * ((a > b) - (b > a));
        }
    },

    sortListData: function(cmp, fieldName, sortDirection) {
        var data = cmp.get("v.fullList");
        var reverse = sortDirection !== 'asc';

        // Sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.fullList", data);

        this.updateDatatableAndFooter(cmp);
    },

    sortData: function(cmp, fieldName, sortDirection, dataAttribute) {
        if (!dataAttribute) {
            dataAttribute = 'v.data';
        }
        var data = cmp.get(dataAttribute);
        var reverse = sortDirection !== 'asc';
        // Sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set(dataAttribute, data);
    },

    updateColumnSorting: function(cmp, event, helper, fullList) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');

        // Assign the latest attribute with the sorted column fieldName and sorted direction
        if($A.util.isEmpty(fieldName) || $A.util.isUndefined(fieldName)) {
            fieldName = cmp.get("v.sortedBy");
        }
        if($A.util.isEmpty(sortDirection) || $A.util.isUndefined(sortDirection)) {
            sortDirection = cmp.get("v.sortedDirection");
        }

        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);

        helper.sortData(cmp, fieldName, sortDirection, fullList);
        helper.sliceList(cmp, fullList, 'v.currentList')
    },

    updateDatatableAndFooterByParameters: function (cmp, attrFullList, attrCurrentList, attrPageNumber, attrSelectedCount, attrMaxPage) {
        var records = cmp.get(attrFullList),
            pageNumber = cmp.get(attrPageNumber),
            pageCountVal = cmp.get(attrSelectedCount),
            pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));
        cmp.set(attrCurrentList, pageRecords);
        if(records.length > 0)
            cmp.set(attrMaxPage, Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
        else
            cmp.set(attrMaxPage, 1);
    },

    updateDatatableAndFooter: function (cmp) {
        var records = cmp.get("v.fullList"),
            pageNumber = cmp.get("v.pageNumber"),
            pageCountVal = cmp.get("v.selectedCount"),
            pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));
        cmp.set("v.currentList", pageRecords);
        if(records.length > 0)
            cmp.set("v.maxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
        else
            cmp.set("v.maxPage", 1);
    },

    sliceList: function (cmp, fullList, slicedList ) {
        var records = cmp.get(fullList),
            pageNumber = cmp.get("v.pageNumber"),
            pageCountVal = cmp.get("v.selectedCount"),
            pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));
        cmp.set(slicedList, pageRecords);
        if(records.length > 0) {
            cmp.set("v.maxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
        } else {
            cmp.set("v.maxPage", 1);
        }
    },

    sortBy: function(field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a) ? key(a) : '', b = key(b) ? key(b):'', reverse * ((a > b) - (b > a));
        }
    },

    sortPickListOptionsByLabel: function(cmp, helper, picklistOptionAttr){
        var picklistOptions = cmp.get(picklistOptionAttr);
        picklistOptions.sort(helper.dynamicSort(cmp, 'label'));
        cmp.set(picklistOptionAttr, picklistOptions);
    },

    dynamicSort: function(cmp, property) {
        var sortOrder = 1;
        if(property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a,b) {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        }
    },

    smoothScrollToPageTop: function(cmp) {
        var scrollOptions = {
            left: 0,
            top: 0,
            behavior: 'smooth'
        }
        window.scrollTo(scrollOptions);
    },

    showTopBannerSuccessMessage: function(cmp, message) {
        this.setBannerMsg(cmp, "OK", message);
        this.smoothScrollToPageTop(cmp);
    },

    showTopBannerErrorMessage: function(cmp, message) {
        this.setBannerMsg(cmp, "ERROR", message);
        this.smoothScrollToPageTop(cmp);
    },

    trimToEmpty: function(strValue) {
        if ($A.util.isUndefinedOrNull(strValue)) {
            return '';
        }
        return strValue.replace(/^\s+|\s+$/g, '');
    },

    isNotBlank: function(strValue) {
        return this.trimToEmpty(strValue).length > 0;
    },

    showErrorRequiredFields: function(cmp, field)	{
        console.log('show error required fields');
        var requiredFields = cmp.find(field);
        requiredFields.showHelpMessageIfInvalid();
        return requiredFields.checkValidity();
    },

    setFieldError: function(cmp, auraId, error)	{
        var field = cmp.find(auraId);
        field.setCustomValidity(error);
        field.showHelpMessageIfInvalid();
        return field.checkValidity();
    },

    gotoRAGPage: function (cmp, event, helper) {
        var ragDtEvt = cmp.getEvent("siteDetailPageEvent");
        ragDtEvt.fire();
    },

    gotoDashboard: function(cmp, event, helper) {
        console.log('goto Dashboard');
        var evt = cmp.getEvent("homePageEvent");
        // evt.setParam('requestType', '');
        evt.fire();
    },

    showErrorBannerFromResponse: function(cmp, event, helper, response, modal) {
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
        var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';


        var errors = response.getError();
        if (errors) {
            // standard errors
            if (errors[0] && errors[0].message) {
                var error = ERR_MSG_ERROR_OCCURRED + errors[0].message;
                if( error.indexOf("INVALID_EMAIL_ADDRESS")>-1 ) {
                    error = "Invalid email Address";
                }
                helper.setErrorMsg(cmp, "ERROR", error, "BANNER", modal);
            } else
                // page errors
                if (errors[0] && errors[0].pageErrors && errors[0].pageErrors[0] && errors[0].pageErrors[0].statusCode) {
                    //helper.setErrorMsg(cmp, "ERROR", ERR_MSG_ERROR_OCCURRED + errors[0].pageErrors[0].statusCode, "BANNER", modal);
                    // raise unknown error message and log error to console
                    helper.setErrorMsg(cmp, "ERROR", ERR_MSG_UNKNOWN_ERROR, "BANNER", modal);
                    console.log("Apex call returned Page error: " +errors[0].pageErrors[0].statusCode);
            } else {
                // catch all unknown error
                helper.setErrorMsg(cmp, "ERROR", ERR_MSG_UNKNOWN_ERROR, "BANNER", modal);
                console.log("Apex call returned: UNKNOWN ERROR TYPE");
            }
        } else {
            helper.setErrorMsg(cmp, "ERROR", ERR_MSG_UNKNOWN_ERROR, "BANNER", modal);
            console.log("Apex call returned: NO ERRORS SET");
        }
    },

    // e.g: helper.setBannerMsg(cmp, "OK", "Test message");
    setBannerMsg: function (cmp, responseStatus, responseMessage) {
        cmp.set("v.responseStatus", responseStatus);
        cmp.set("v.responseMessage", responseMessage);
    },

    // Sets error msg properties
    // e.g: helper.setErrorMsg(cmp, "ERROR", "TEST", "BANNER");
    setErrorMsg: function (cmp, responseStatus, responseMessage, messageType, modal) {
        if (modal === true) {
            cmp.set("v.modalResponseStatus", responseStatus);
            cmp.set("v.modalResponseMessage", responseMessage);
            cmp.set("v.modalResponseType", messageType);
        } else {
            cmp.set("v.responseStatus", responseStatus);
            cmp.set("v.responseMessage", responseMessage);
            cmp.set("v.messageType", messageType);
        }
    },

    clearErrors: function (cmp) {
            cmp.set("v.responseStatus", "");
            cmp.set("v.responseMessage", "");
            cmp.set("v.messageType", "");
            // and clear modals too
            cmp.set("v.modalResponseStatus", "");
            cmp.set("v.modalResponseMessage", "");
            cmp.set("v.modalResponseType", "");
    },

    clearAttributeObject: function(cmp, attributeName) {
        var obj = cmp.get(attributeName);
        for (var val in obj) {
            obj[val] = '';
        }
        cmp.set(attributeName, obj);
    }

})