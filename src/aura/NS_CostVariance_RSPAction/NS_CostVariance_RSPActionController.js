({
	myAction : function(component, event, helper) {
		
	},   
    
    executeRSPAction : function (component, event, helper) {		        

        var rspResponseBtnId = event.getSource().getLocalId();        
        var rspResponseOnCostVariance ;

        var action = helper.getApexProxy(component, 'c.updateRSPResponseOnCostVariance');
         
        if(rspResponseBtnId == "btnAccept")
        {
            rspResponseOnCostVariance = "Accepted";        	
        }
        else if(rspResponseBtnId == "btnReject")
        {
            rspResponseOnCostVariance = "Rejected";        	
        }
         
        action.setParams({
            "quoteId" : component.get('v.quoteId'),
            "rspResponse" : rspResponseOnCostVariance,
            "quoteName" : component.get('v.quoteName'),
            "orderGuid" : component.get('v.orderGuid')
        	});
  
        action.setCallback(this, function(result){
            var state = result.getState(); // get the response state
            if(state == 'SUCCESS' && result.getReturnValue() != null) {                
                component.set("v.rspResponseOnCostVariance",rspResponseOnCostVariance);
            }
        });
        $A.enqueueAction(action.delegate());
	}
})