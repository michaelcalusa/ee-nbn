/**
 * Created by gobindkhurana on 4/9/18.
 */
({
    init: function (cmp, event, helper) {

        helper.getBizCommunityPermissions(cmp, event, helper);

        // Get custom label values
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);

        var action = helper.getApexProxy(cmp, "c.getUserDetails");
        var loggedInDetails;

        action.setCallback(this,function(response){

            var state = response.getState();
            if(state === "SUCCESS"){
                loggedInDetails = response.getReturnValue();
                if(loggedInDetails === "business"){
                    cmp.set("v.isBusiness", true);
                    cmp.set("v.isBusinessPlus", false);
                    cmp.set("v.isAssurance", false);
                }
                else if(loggedInDetails === "businessPlus"){
                    cmp.set("v.isBusinessPlus", true);
                    cmp.set("v.isBusiness", false);
                    cmp.set("v.isAssurance", false);
                    console.log('=v.isBusinessPlus='+cmp.get("v.isBusinessPlus"));
                }
                else if(loggedInDetails === "assurance"){
                    cmp.set("v.isAssurance", true);
                    cmp.set("v.isBusiness", false);
                    cmp.set("v.isBusinessPlus", false);
                }
                cmp.set("v.isServiceFeasibility", true);
            }
            else if(state === "ERROR") {
                cmp.set("v.responseStatus",state);
                cmp.set("v.type","Banner");
                cmp.set("v.responseMessage",errorLabel);
            }
        });

        $A.enqueueAction(action.delegate());

        var baseUrl = window.location.href;
        console.log('-----window-----'+baseUrl);

        var splittedArray = baseUrl.includes('locid=') ? baseUrl.split('locid=') : [];
        console.log('--splittedArray-- ', splittedArray);

        var finalLocId = !$A.util.isEmpty(splittedArray) ? splittedArray[1] : '';
        console.log('--finalLocId---', finalLocId);

        if(!$A.util.isEmpty(finalLocId))
        {
            cmp.set("v.loadServicesFeasibility", true);
            cmp.set("v.loadOrders", false);
            cmp.set("v.loadTickets", false);
            cmp.set("v.loadHome", false);
            cmp.set("v.batchId", null);
            cmp.set("v.lstSitePage", false);
            cmp.set("v.sfPage", false);
            cmp.set("v.ragPage", false);
            cmp.set("v.quotePage", false);
            cmp.set("v.orderPage", false);
            cmp.set('v.isOrderDetailsOpen', false);
            cmp.set('v.isQuoteDetailsOpen', false);
            cmp.set("v.loadDraftOrders", false);
            cmp.set("v.loadServices", false);

        }
        

    },
    handleHomeEvent: function (cmp, event, helper) {
        var reqType = event.getParam("requestType");
        if(reqType === "feasibility"){
            cmp.set("v.loadServicesFeasibility", true);
            cmp.set("v.loadOrders", false);
            cmp.set("v.loadTickets", false);
            cmp.set("v.loadHome", false);
            cmp.set("v.batchId", null);
            cmp.set("v.lstSitePage", false);
            cmp.set("v.sfPage", false);
            cmp.set("v.ragPage", false);
            cmp.set("v.quotePage", false);
            cmp.set("v.orderPage", false);
            cmp.set('v.isOrderDetailsOpen', false);
			cmp.set('v.isQuoteDetailsOpen', false);
            cmp.set("v.loadDraftOrders", false);
            cmp.set("v.loadServices", false);
            console.log("==v.loadServicesFeasibility==: ",cmp.get("v.loadServicesFeasibility"));
        }
        else if(reqType == "orderSummary"){
            
            cmp.set('v.isFromQuotePage', event.getParam("isFromQuotePage"));
			var batchId = cmp.get("v.batchId");
			if($A.util.isEmpty(batchId) || $A.util.isUndefined(batchId)) cmp.set('v.batchId', event.getParam("batchId"));
            cmp.set("v.quotePage", true);
            cmp.set("v.loadServicesFeasibility", false);
            cmp.set("v.loadOrders", false);
            cmp.set("v.loadTickets", false);
            cmp.set("v.loadHome", false);
            cmp.set("v.batchId", null);
            cmp.set("v.lstSitePage", false);
            cmp.set("v.sfPage", false);
            cmp.set("v.ragPage", false);
            cmp.set("v.orderPage", false);
            cmp.set('v.isOrderDetailsOpen', false);
			cmp.set('v.isQuoteDetailsOpen', false);
            cmp.set("v.loadDraftOrders", false);
            cmp.set("v.loadServices", false);
            console.log("==v.quotePage==: "+cmp.get("v.quotePage"));
        }
        else if(reqType === "order"){
            cmp.set("v.loadServicesFeasibility", false);
            cmp.set("v.loadOrders", true);
            cmp.set("v.loadTickets", false);
            cmp.set("v.loadHome", false);
            cmp.set("v.batchId", null);
            cmp.set("v.lstSitePage", false);
            cmp.set("v.sfPage", false);
            cmp.set("v.ragPage", false);
            cmp.set("v.quotePage", false);
            cmp.set("v.orderPage", false);
            cmp.set('v.isOrderDetailsOpen', false);
			cmp.set('v.isQuoteDetailsOpen', false);
            cmp.set("v.loadDraftOrders", false);
            cmp.set("v.loadServices", false);
            console.log("==v.loadOrders==: ",cmp.get("v.loadOrders"));
        }
        else if(reqType === "assurance"){
            cmp.set("v.loadServicesFeasibility", false);
            cmp.set("v.loadOrders", false);
            cmp.set("v.loadTickets", true);
            cmp.set("v.loadHome", false);
            cmp.set("v.batchId", null);
            cmp.set("v.lstSitePage", false);
            cmp.set("v.sfPage", false);
            cmp.set("v.ragPage", false);
            cmp.set("v.quotePage", false);
            cmp.set("v.orderPage", false);
            cmp.set('v.isOrderDetailsOpen', false);
			cmp.set('v.isQuoteDetailsOpen', false);
            cmp.set("v.loadDraftOrders", false);
            cmp.set("v.loadServices", false);
        } else if(reqType === "services"){
            cmp.set("v.loadServicesFeasibility", false);
            cmp.set("v.loadOrders", false);
            cmp.set("v.loadTickets", false);
            cmp.set("v.loadHome", false);
            cmp.set("v.batchId", null);
            cmp.set("v.lstSitePage", false);
            cmp.set("v.sfPage", false);
            cmp.set("v.ragPage", false);
            cmp.set("v.quotePage", false);
            cmp.set("v.orderPage", false);
            cmp.set('v.isOrderDetailsOpen', false);
			cmp.set('v.isQuoteDetailsOpen', false);
            cmp.set("v.loadDraftOrders", false);
            cmp.set("v.loadServices", true);
        }
        else{
            cmp.set("v.loadHome", true);
            cmp.set("v.loadServicesFeasibility", false);
            cmp.set("v.loadOrders", false);
            cmp.set("v.loadTickets", false);
            cmp.set("v.batchId", null);
            cmp.set("v.lstSitePage", false);
            cmp.set("v.sfPage", false);
            cmp.set("v.ragPage", false);
            cmp.set("v.quotePage", false);
            cmp.set("v.orderPage", false);
            cmp.set('v.isOrderDetailsOpen', false);
			cmp.set('v.isQuoteDetailsOpen', false);
			cmp.set("v.loadServices", false);
        }
    },
    updateOppBundleId : function(cmp, event, helper) {
        var opptBundleId = event.getParam("oppBundleId");
        cmp.set("v.batchId", opptBundleId);
        cmp.set("v.lstSitePage", true);
        cmp.set("v.sfPage", false);
        cmp.set("v.ragPage", false);
        cmp.set("v.quotePage", false);
        cmp.set("v.orderPage", false);
        cmp.set("v.loadServicesFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", false);
        cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);
		cmp.set("v.loadServices", false);
    },

    goToRAGPage :function(cmp, event, helper) {
        var opptBundleId = event.getParam("oppBundleId");
        console.log('ragPage opptBundleId : ' + opptBundleId);
        if(opptBundleId!=null)
            cmp.set("v.batchId", opptBundleId);

        cmp.set("v.lstSitePage", false);
        cmp.set("v.sfPage", false);
        cmp.set("v.ragPage", true);
        cmp.set("v.quotePage", false);
        cmp.set("v.orderPage", false);
        cmp.set("v.loadServicesFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", false);
        cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);
		cmp.set("v.loadServices", false);
    },

    goToQuotePage :function(cmp, event, helper) {

		cmp.set('v.isFromQuotePage', event.getParam("isFromQuotePage"));
		var batchId = cmp.get("v.batchId");
		if($A.util.isEmpty(batchId) || $A.util.isUndefined(batchId)) cmp.set('v.batchId', event.getParam("batchId"));

        cmp.set("v.lstSitePage",                false);
        cmp.set("v.sfPage",                     false);
        cmp.set("v.ragPage",                    false);
        cmp.set("v.quotePage",                  true);
        cmp.set("v.orderPage",                  false);
        cmp.set("v.loadServicesFeasibility",    false);
        cmp.set("v.loadOrders",                 false);
        cmp.set("v.loadTickets",                false);
        cmp.set("v.loadHome",                   false);
        cmp.set('v.isOrderDetailsOpen',         false);
		cmp.set('v.isQuoteDetailsOpen',         false);
		cmp.set("v.loadServices",               false);
    },

    goToOrderPage :function(cmp, event, helper) {
        var location = event.getParam("location");
		console.log('==location in NS_Dashboard.js=='+location);
        cmp.set("v.location", location);
        cmp.set("v.lstSitePage", false);
        cmp.set("v.sfPage", false);
        cmp.set("v.ragPage", false);
		cmp.set("v.quotePage", false);
		cmp.set("v.orderPage", true);
    },

    handleOrderEvent: function (cmp, event, helper) {
        console.log('--@handleOrderEvent--');
        
        
        console.log('--batchId---', event.getParam("batchId")); 
       
        
        var selOrder = event.getParam("selectedOrder");
        console.log('selOrder',selOrder);
        cmp.set('v.selectedOrder', selOrder);
        var isFromQuotePage = event.getParam("isFromQuotePage");
        console.log('isFromQuotePage',isFromQuotePage);
		cmp.set('v.isFromQuotePage', isFromQuotePage);		
        if(isFromQuotePage){
            cmp.set('v.batchId', event.getParam("batchId"));

        }
        cmp.set("v.isOrderDetailsOpen", true);
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", false);
        cmp.set("v.batchId", null);  
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
        cmp.set("v.quotePage", false);    
        cmp.set("v.orderPage", false);
        event.stopPropagation();
    },
    clickOrderLink: function (cmp, event, helper) {
        cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);
        cmp.set("v.loadServicesFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", true);
        cmp.set("v.batchId", null);
        cmp.set("v.lstSitePage", false);
        cmp.set("v.sfPage", false);
        cmp.set("v.ragPage", false);
        cmp.set("v.quotePage", false);
        cmp.set("v.orderPage", false);
        cmp.set("v.isOrders", true);
        cmp.set("v.isServiceFeasibility", false);
        cmp.set("v.isTickets", false);
        cmp.set("v.isDraftOrders", false);
        cmp.set("v.loadServices", false);
        console.log('=v.isOrders='+cmp.get("v.isOrders"));
    },
    clickFeasibilityLink: function (cmp, event, helper) {		
        cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);
        cmp.set("v.loadServicesFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", true);
        cmp.set("v.batchId", null);
        cmp.set("v.lstSitePage", false);
        cmp.set("v.sfPage", false);
        cmp.set("v.ragPage", false);
        cmp.set("v.quotePage", false);
        cmp.set("v.orderPage", false);
        cmp.set("v.isOrders", false);
        cmp.set("v.isServiceFeasibility", true);
        cmp.set("v.isTickets", false);
        cmp.set("v.isDraftOrders", false);
        cmp.set("v.loadServices", false);
    },
    clickAssuranceLink: function (cmp, event, helper) {
        cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);
        cmp.set("v.loadServicesFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", true);
        cmp.set("v.loadHome", false);
        cmp.set("v.batchId", null);
        cmp.set("v.lstSitePage", false);
        cmp.set("v.sfPage", false);
        cmp.set("v.ragPage", false);
        cmp.set("v.quotePage", false);
        cmp.set("v.orderPage", false);
        cmp.set("v.isOrders", false);
        cmp.set("v.isServiceFeasibility", false);
        cmp.set("v.isDraftOrders", false);
        cmp.set("v.loadServices", false);
    },
    clickDraftOrderLink: function (cmp, event, helper) {
        cmp.set('v.isOrderDetailsOpen', false);
		cmp.set('v.isQuoteDetailsOpen', false);
        cmp.set("v.loadServicesFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", true);
        cmp.set("v.batchId", null);
        cmp.set("v.lstSitePage", false);
        cmp.set("v.sfPage", false);
        cmp.set("v.ragPage", false);
        cmp.set("v.quotePage", false);
        cmp.set("v.orderPage", false);
        cmp.set("v.isOrders", false);
        cmp.set("v.isServiceFeasibility", false);
        cmp.set("v.isTickets", false);
        cmp.set("v.isDraftOrders", true);
        cmp.set("v.loadServices", false);
    },
})