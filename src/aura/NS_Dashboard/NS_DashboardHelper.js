/**
 * Created by gobindkhurana on 4/9/18.
 */
({
    getBizCommunityPermissions: function(cmp, event, helper) {
        // Create the action
        var action = cmp.get("c.getBizCommunityPermissions");
        var communityPermissions;

        // Add callback behavior for when response is received
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                communityPermissions = response.getReturnValue();
                if (communityPermissions.includes('Has_NS_Community_Permissions__c')) {
                    cmp.set("v.hasNSAccess", true);
                }
                console.log('v.hasNSAccess' + cmp.get("v.hasNSAccess"));

            } else {
                console.log('BSM_HomeController.getBizCommunityPermissions - communityPermissions- Failed response with state: ' + state);
            }
            cmp.set("v.initCompleted", true);

        });
        // Send action off to be executed
        $A.enqueueAction(action);
    }
})