/**
 * Created by gobindkhurana on 4/9/18.
 */
({
    hideBurgerMenu: function(cmp, event, helper) {
        var menuButtonnHandle = cmp.find('menuButtonId');
        var isOpen = $A.util.hasClass(cmp.find("menuButtonId"), "slds-is-open");
        if(isOpen){
            $A.util.removeClass(menuButtonnHandle, 'slds-is-open');
        }
    },
})