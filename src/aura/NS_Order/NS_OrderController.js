({
    doInit: function (cmp, event, helper) {
        console.log('==== Controller - doInit =====');
        var location = (!event.getParam("location")) ? cmp.get("v.location") : event.getParam("location");
        console.log('NS_Oder::doInit',location);
        //var ordId =  (!event.getParam("ordId")) ? cmp.get("v.ordId") : event.getParam("ordId");
        $A.createComponent(
            "c:NS_Order_Item", {
                "location": location
                //"ordId" : ordId
            },
            function (newCmp) {
                if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
                }
            }
        );
         console.log('==== END Controller - doInit =====');
    },

    goToOrderContactDetailsPage: function (cmp, event, helper) {
        console.log('==== Controller - goToOrderContactDetailsPage =====');

        var location = (!event.getParam("location")) ? cmp.get("v.location") : event.getParam("location");
        //var opportunityId = (!event.getParam("opportunityId")) ? cmp.get("v.opportunityId") : event.getParam("opportunityId");

        console.log('==== Controller - goToOrderContactDetailsPage ===== location ' +JSON.stringify(location));//added
       // console.log('==== Controller - goToOrderContactDetailsPage ===== opportunityId ' +opportunityId);//added
        $A.createComponent(
            "c:NS_Order_Contact_Details", {
                "location": location

            },
            function (newCmp) {
                if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
                }
            }
        );
    },

    goToOrderInstallationDetailsPage: function (cmp, event, helper) {
        console.log('==== Controller - goToOrderInstallationDetailsPage =====');

        var location = (!event.getParam("location")) ? cmp.get("v.location") : event.getParam("location");

        $A.createComponent(
            "c:NS_Order_Installation_Details", {
                "location": location
            },
            function (newCmp) {
                if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
                }
            }
        );
    },

    goToOrderItemPage: function (cmp, event, helper) {
        console.log('==== Controller - goToOrderItemPage =====');
        var location = (!event.getParam("location")) ? cmp.get("v.location") : event.getParam("location");

        var progressBarStep = helper.trimToEmpty(location['progressBarStep']);
        if (progressBarStep.length === 0) {
            progressBarStep = '1';
        }

        $A.createComponent(
            "c:NS_Order_Item", {
                "location": location,
                "nsCurrentTabValue":  progressBarStep
            },
            function (newCmp) {
                if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
                }
            }
        );
    },

    goToQuotePage: function (cmp, event, helper) {
        console.log('==== Controller - goToQuotePage =====');
		
		var location = (!event.getParam("location")) ? cmp.get("v.location") : event.getParam("location");
        var batchId = (!event.getParam("batchId")) ? cmp.get("v.parentOppId") : event.getParam("batchId");
        
        $A.createComponent(
            "c:NS_SF_Quote", {
                selectedLocation: location,
                parentOppId : batchId
                //parentOppId : 'a6Zp00000008QNoEAM'
            },
            function (newCmp) {
                if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
                }
            }
        );
    },

    goToOrderSummaryPage: function (cmp, event, helper) {
        //event type is c:NS_OrderEvent
        console.log('==== Controller - submitted new Order -> goToOrderSummaryPage =====');
        var batchId = (!event.getParam("batchId")) ? cmp.get("v.parentOppId") : event.getParam("batchId");
        var orderId = event.getParam("selectedOrder");
        var isFromQuotePage = event.getParam("isFromQuotePage");
        var isNewOrderCreated = event.getParam("isNewOrderCreated");

        $A.createComponent(
            "c:NS_Order_Submitted_Summary", {
                parentOppIdParam : batchId,
                ordId : orderId,
                isFromQuotePage :  isFromQuotePage,
                isNewOrderCreated : isNewOrderCreated,
                isFromSearchPage : false
            },
            function (newCmp) {
                if (cmp.isValid()) {
                    cmp.set("v.body", newCmp);
                }
            }
        );
    }

})