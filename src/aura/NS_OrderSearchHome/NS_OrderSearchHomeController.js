/**
 * Created by gobindkhurana on 4/9/18.
 */
({

    init: function (cmp, event, helper) {
        //Check if url parameter exists
        var orderIdParam = helper.getUrlParameter(cmp, helper.ORDER_ID_PARAM_NAME);
        var parentOppIdParam = helper.getUrlParameter(cmp, helper.PARENT_OPP_ID_PARAM_NAME);

        if (orderIdParam != null && parentOppIdParam != null) {
            cmp.set('v.orderIdParam', orderIdParam);
            cmp.set('v.parentOppIdParam', parentOppIdParam);

            helper.apex(cmp, "v.pageUrlSFOrderSummary", 'c.getCustomSetting', { settingName : helper.CUSTOM_SETTING_PAGE_URL_SF }, false)
                .then(function(result) {
                    var backUrl = cmp.get('v.pageUrlSFOrderSummary') + '?' + helper.PARENT_OPP_ID_PARAM_NAME + '=' + parentOppIdParam + '&page=ordersummary'
                    cmp.set('v.backUrl', backUrl);
                    cmp.set('v.isOrderDetailsOpen', true);
                    cmp.set('v.isInitDone', true);
                })
        }
        else {
            helper.getOrderRecords(cmp, event, helper);
            cmp.set('v.isInitDone', true);
        }
    },

	searchTermChange: function (cmp, event, helper) {
        //Do search if enter key is pressed.
        if (event.getParams().keyCode == 13) {
            cmp.set('v.isTriggerdBySearch', true);
			helper.getOrderRecords(cmp, event, helper);
        }
    },

    search: function (cmp, event, helper) {
        cmp.set('v.isTriggerdBySearch', true);
		helper.getOrderRecords(cmp, event, helper);
    },

    clear: function (cmp, event, helper) {
        helper.clearErrors(cmp);
        cmp.set('v.isTriggerdBySearch', false);
        cmp.set('v.searchTerm', '');
		helper.getOrderRecords(cmp, event, helper);
        helper.toogleClearSearchButton(cmp, true);
        helper.disableClearSearchButton(cmp, true);
    },

    clickOrderiLink: function (cmp, event, helper) {

		var fullList = cmp.get('v.fullList');
		var orderId = event.target.getAttribute("data-Id");
        console.log('orderId',orderId);


        var batchId =  cmp.get('v.parentOppIdParam');
        console.log('batchId',batchId);
        
        console.log('orderId',orderId);

        try{
        var orderSummaryEvt = cmp.getEvent("NS_orderSummaryEvent");

        console.log('Inside Event');
        //cmp.set('v.isOrderDetailsOpen', true);
        orderSummaryEvt.setParams({
            "selectedOrder": orderId,
            "isFromQuotePage": "false"
           // "batchId": batchId
            
        });
        orderSummaryEvt.fire();
        }
        catch(err){
            console.log('Error Message', err);
        }

    },

    renderPage: function (cmp, event, helper) {
        helper.renderPage(cmp, event, helper);
    },

    updateColumnSorting: function (cmp, event, helper) {
            try{
                var sortedBy = cmp.get('v.sortedBy');
                var sortedDirection = cmp.get('v.sortedDirection');
                
                var fieldName = event.target.getAttribute("data-column");
                var subPropertyName = event.target.getAttribute("data-column-sub-property");
                console.log('sortedBy: ' + sortedBy);
                console.log('sortedDirection: ' + sortedDirection);
                console.log('fieldName: ' + fieldName);
                console.log('subPropertyName: ' + subPropertyName);
                console.log('subPropertyName' + $A.util.isEmpty(subPropertyName) + ' ' + $A.util.isUndefined(subPropertyName));
                
                helper.updateColumnSortingWithSubProperty(cmp, event, helper);
            }catch(e){
                console.log(e);
            }
    },

})