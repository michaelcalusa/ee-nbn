/**
 * Created by gobindkhurana on 4/9/18.
 */
({
    ERR_MSG_ERROR_OCCURRED: 'Error occurred. Error message: ',
    ERR_MSG_UNKNOWN_ERROR: 'Error message: Unknown error',
    MAX_COUNT: '5000',

	getOrderRecords : function(cmp, event, helper) {
		var isTriggerBySearch = cmp.get('v.isTriggerdBySearch');
		var searchTermVal = cmp.get('v.searchTerm');

		if (isTriggerBySearch) helper.toogleClearSearchButton(cmp, false);

        var action = helper.getApexProxy(cmp, "c.getRecords");

		action.setParams({  "searchTerm": searchTermVal,
                            "searchDate": null,
                            "searchStatus": null,
                            "isTriggerBySearch": Boolean(isTriggerBySearch)});

		action.setCallback(this, function(response) {
		    var state = response.getState();
		    console.log('response state :  ' + state);
        	if(state === "SUCCESS") {
				helper.clearErrors(cmp);

                var records = JSON.parse(response.getReturnValue());
                console.log('record values :  ' + response.getReturnValue());
				cmp.set('v.fullList', records);
				cmp.set('v.currentList', records);
				cmp.set("v.pageNumber", 1);
                var pageCountVal = cmp.get("v.selectedCount");
				cmp.set("v.maxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
				helper.renderPage(cmp);
	        }
            else{
				var errors = response.getError();
				if (errors) {
                    if (errors[0] && errors[0].message) {
                    	console.log(helper.ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                	console.log(helper.ERR_MSG_UNKNOWN_ERROR);
                }

				var errorLabel = $A.get("$Label.c.DF_Application_Error");

				cmp.set("v.responseStatus", state);
                cmp.set("v.messageType","Banner");
                cmp.set("v.responseMessage", errorLabel);
            }
        });
        $A.enqueueAction(action.delegate());
	},

    renderPage: function (cmp) {

        var records = cmp.get("v.fullList"),

            pageNumber = cmp.get("v.pageNumber"),
            pageCountVal = cmp.get("v.selectedCount"),
            pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));

        cmp.set('v.currentList', []);
        cmp.set("v.currentList", pageRecords);
    },

    toogleClearSearchButton: function (cmp, disabled) {
        cmp.set('v.isClearSearchButtonDisabled', disabled);
    },

})