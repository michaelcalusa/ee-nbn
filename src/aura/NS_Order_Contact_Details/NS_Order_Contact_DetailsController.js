({
	init: function(cmp, event, helper) {	
		console.log('Order Contact Details ==== Controller - init =====');
		
		var loc =  cmp.get("v.location");
        //var loc =  "a6ap00000008iwz";
		console.log('ORder Contact Details location ' + JSON.stringify(loc));
		//var id =  "a6ap00000008iwz";
		
		console.log("Opportunity Id: " + loc.OpportunityId);
        
		cmp.set("v.opportunityId", loc.OpportunityId);
        
        var id =  cmp.get("v.opportunityId");
		console.log('ORder Contact Details id ' + id);
        
        console.log("ORDER Id: " + loc.orderId);
		cmp.set("v.orderId", loc.orderId);
        
	
		helper.loadStateOptions(cmp, event, helper);
		helper.loadBusinessContact(cmp, event, helper);
        helper.loadSiteContact(cmp, event, helper);
	},
    
    clickBackButton: function(cmp, event, helper) {
		helper.goToPreviousPage(cmp, event);		
	},

    clickOrderSummaryButton: function(cmp, event, helper) {			
    	helper.goToOrderSummaryPage(cmp, event);	
	},	

    clickNextButton: function(cmp, event, helper) {	
        
        cmp.set("v.siteContactErrorMsg", "");
        cmp.set("v.businessContactErrorMsg", "");
        
        if(cmp.get("v.attrBusinessContactList").length == 0)
        {            
            cmp.set("v.businessContactErrorMsg", "Please add at least one business contact") ;
        }
        
        if(cmp.get("v.attrSiteContactList").length == 0)
        {            
            cmp.set("v.siteContactErrorMsg", "Please add at least one site contact") ;
        }
        
    	if(!cmp.get("v.siteContactErrorMsg") && !cmp.get("v.businessContactErrorMsg"))
        {
			helper.goToNextPage(cmp, event);		
        }
	},
	
    clickAddBusinessContact: function(cmp, event, helper) {			
    	helper.addBusinessContact(cmp, event, helper);
	},
	
    clickEditBusinessContact: function(cmp, event, helper) {			
    	var busContIndex = event.getSource().get("v.value");

    	helper.editBusinessContact(cmp, event, busContIndex);

	},

    clickAddSiteContact: function(cmp, event, helper) {			
    	helper.addSiteContact(cmp, event, helper);
	},
	
    clickEditSiteContact: function(cmp, event, helper) {			
    	var siteContId = event.getSource().get("v.value");
    	helper.editSiteContact(cmp, event, siteContId);	
	},
    
	clickBusContModalCancelButton: function(cmp, event, helper) {		
		helper.busContModalCancel(cmp, event);			
	},
	
	clickBusContModalSaveAndCloseButton: function(cmp, event, helper) {		
		helper.busContModalSave(cmp, event, helper);
        helper.loadSiteContact(cmp, event, helper);
	},
	
	clickSiteContModalCancelButton: function(cmp, event, helper) {		
		helper.siteContModalCancel(cmp, event);			
	},
	
	clickSiteContModalSaveAndCloseButton: function(cmp, event, helper) {
		helper.siteContModalSave(cmp, event, helper, true);
        helper.loadBusinessContact(cmp, event, helper);
	},

    clickBusContModalClearButton: function(cmp, event, helper) {
		//helper.clearAttributeObject(cmp, 'v.attrSelectedBusinessContact');
		helper.clearBusinessContact(cmp, event, helper);
	},

    clickSiteContModalClearButton: function(cmp, event, helper) {
        //helper.clearAttributeObject(cmp, 'v.attrSelectedSiteContact');
		helper.clearSiteContact(cmp, event, helper);
    },

    updateSiteContact: function(cmp, event, helper) {
        if ( cmp.get('v.siteContactSame') === true ) {
            helper.copyBusinessContactToSiteContact(cmp, event, helper);
        } else {
        	helper.unlinkSiteContact(cmp, event, helper);
        	// clear the site contact list
            cmp.set('v.attrSelectedSiteContact', []);
            cmp.set('v.attrSiteContactList', []);
		}

    },
    
})