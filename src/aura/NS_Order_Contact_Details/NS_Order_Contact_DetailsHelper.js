({
    goToNextPage: function (cmp, event) {
        var evt = cmp.getEvent("NSorderInstallationDetailsPageEvent");
        evt.fire();
    },
	
    goToPreviousPage: function (cmp, event) {
        console.log('==== Helper - goToPreviousPage =====');
        var evt = cmp.getEvent("NSorderItemPageEvent");

        // added Progress Bar Step
        var locationMap = cmp.get('v.location');
        locationMap['progressBarStep'] = '3';

        evt.setParam("location", locationMap);
        evt.fire();
    },

    clearBusinessContact: function(cmp, event, helper) {
        var bc = cmp.get("v.attrSelectedBusinessContact");
        bc.busContFirstName = '';
        bc.busContSurname = '';
        bc.busContEmail = '';
        bc.busContNumber = '';
        cmp.set("v.attrSelectedBusinessContact", bc);
    },

    clearSiteContact: function(cmp, event, helper) {
        var bc = cmp.get("v.attrSelectedSiteContact");
        bc.siteContFirstName = '';
        bc.siteContSurname = '';
        bc.siteContEmail = '';
        bc.siteContNumber = '';
        cmp.set("v.attrSelectedSiteContact", bc);
    },

    validateContacts: function(cmp){
        
        var businessContacts = cmp.get("v.attrBusinessContactList");
        var siteContacts = cmp.get("v.attrSiteContactList");
        var ERR_MSG_APP_ERROR = "There must be at least one Site Contact and one Business Contact";
        if(businessContacts.length<1 || siteContacts.length<1){
            this.setErrorMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "BANNER");
            return false;
        }
        this.clearErrors(cmp)
        return true;
    },

    goToOrderSummaryPage: function (cmp, event) {
        console.log('==== Helper - goToOrderSummaryPage =====');
        var evt = cmp.getEvent("NSquoteDetailPageEvent");
        evt.fire();
    },

    loadBusinessContact: function (cmp, event, helper) {
        console.log('==== Helper - loadBusinessContact =====');
        var location = cmp.get("v.location");

        var action = helper.getApexProxy(cmp, "c.getBusinessContactsAndOppContactRole");
        var quoteId = cmp.get("v.location").id;
        var oppName = cmp.get("v.location").quoteId;
        action.setParams({
            "quoteId": quoteId,
            "oppName":oppName
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('loadBusinessContact - state: ' + state);

            if (state === "SUCCESS") {
                var responseReturnValue = response.getReturnValue();
                console.log('loadBusinessContact - responseReturnValue: ' + responseReturnValue);
                var businessContactDataObjList = JSON.parse(response.getReturnValue());
                for(var i = 0; i<businessContactDataObjList.length; i++){
                    console.log('loadBusinessContact in loop');
                    businessContactDataObjList[i].busContIndex = i+1;
                }
                console.log('loadBusinessContact - businessContactDataObjList: ' + JSON.stringify(businessContactDataObjList));
                cmp.set("v.attrBusinessContactList", businessContactDataObjList);
                location.businessContactList = businessContactDataObjList;
                cmp.set("v.location", location);
                helper.setSameAsBusinessContact(cmp, event, helper);
            } else {
                helper.showErrorBanner(cmp, event, helper, response);
            }
        });
        $A.enqueueAction(action.delegate());
    },

    loadSiteContact: function (cmp, event, helper) {
        console.log('==== Helper - loadSiteContact =====');
        var location = cmp.get("v.location");
        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);

        var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
        var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

        var action = helper.getApexProxy(cmp, "c.getSiteContactsAndOppContactRole");
        var quoteId = cmp.get("v.location").id;
        var oppName = cmp.get("v.location").quoteId;
        action.setParams({
            "quoteId": quoteId,
            "oppName":oppName
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('loadSiteContact - state: ' + state);

            if (state === "SUCCESS") {
                var responseReturnValue = response.getReturnValue();
                console.log('loadSiteContact - responseReturnValue: ' + responseReturnValue);

                var siteContactDataObjList = JSON.parse(response.getReturnValue());
                for(var i = 0; i<siteContactDataObjList.length; i++){
                    siteContactDataObjList[i].siteContIndex = i+1;
                }
                console.log('loadSiteContact - siteContactDataObjList: ' + JSON.stringify(siteContactDataObjList));
                cmp.set("v.attrSiteContactList", siteContactDataObjList);

                location.siteContactList = siteContactDataObjList;
                cmp.set("v.location", location);

                helper.setSameAsBusinessContact(cmp, event, helper);

            } else {
                helper.showErrorBanner(cmp, event, helper, response);
            }
        });
        $A.enqueueAction(action.delegate());
    },

    setSameAsBusinessContact: function(cmp, event, helper) {

        var siteContactList     = cmp.get('v.attrSiteContactList');
        var businessContactList = cmp.get('v.attrBusinessContactList');

        if (siteContactList.length > 0 &&
            businessContactList.length > 0 &&
            siteContactList[0].siteContFirstName === businessContactList[0].busContFirstName &&
            siteContactList[0].siteContSurname === businessContactList[0].busContSurname &&
            siteContactList[0].siteContEmail === businessContactList[0].busContEmail &&
            siteContactList[0].siteContNumber === businessContactList[0].busContNumber) {
            cmp.set('v.siteContactSame', true);
        } else {
            cmp.set('v.siteContactSame', false);
        }
    },

    addBusinessContact: function (cmp, event, helper) {
        console.log('==== Helper - addBusinessContact =====');

        var businessContactList = cmp.get("v.attrBusinessContactList");
        var maxbusContId = businessContactList.length == 0 ? 0 : Math.max.apply(Math, businessContactList.map(function (item) {
            return item.busContIndex;
        }));

        var newBusinessContact = {
            'busContIndex': maxbusContId + 1,
            'busContId':'',
            'busContFirstName': '',
            'busContSurname': '',
            'busContRole': '',
            'busContEmail': '',
            'busContNumber': '',
            'busContStreetAddress': '',
            'busContSuburb': '',
            'busContPostcode': '',
            'busContState': ''
        };

		cmp.set("v.attrSelectedBusinessContact", newBusinessContact);
        cmp.set('v.businessContactErrorMsg','');
        cmp.set("v.attrIsBusContModalOpen", true);
    },

    editBusinessContact: function (cmp, event, busContIndex) {
        console.log('==== Helper - editBusinessContact =====');

        console.log('Helper.editBusinessContact - busContIndex: ' + busContIndex);

        var businessContactList = cmp.get("v.attrBusinessContactList");  
        var results = businessContactList.filter(function (item) {
            return item.busContIndex == busContIndex;
        });
        console.log('results '+ JSON.stringify(results) + '  ' +results.length);
        
        cmp.set("v.attrSelectedBusinessContact", this.cloneBusinessContact(results[0]));
        cmp.set("v.attrIsBusContModalOpen", true);
        
    },

    cloneBusinessContact: function (businessContact) {
        console.log('==== Helper - cloneBusinessContact =====');

        var clonedBusinessContact = {
            'busContId': businessContact.busContId,
            'busContIndex':businessContact.busContIndex,
            'busContFirstName': businessContact.busContFirstName,
            'busContSurname': businessContact.busContSurname,
            'busContRole': businessContact.busContRole,
            'busContEmail': businessContact.busContEmail,
            'busContNumber': businessContact.busContNumber,
            'busContStreetAddress': businessContact.busContStreetAddress,
            'busContSuburb': businessContact.busContSuburb,
            'busContPostcode': businessContact.busContPostcode,
            'busContState': businessContact.busContState
        };

        return clonedBusinessContact;
    },

    addSiteContact: function (cmp, event) {
        console.log('==== Helper - addSiteContact =====');

        var siteContactList = cmp.get("v.attrSiteContactList");
        var maxsiteContId = siteContactList.length == 0 ? 0 : Math.max.apply(Math, siteContactList.map(function (item) {
            return item.siteContIndex;
        }));

        var newSiteContact = {
            'siteContId':'',
            'siteContIndex': maxsiteContId + 1,
            'siteContFirstName': '',
            'siteContSurname': '',
            'siteContEmail': '',
            'siteContNumber': ''
        };
		cmp.set("v.attrSelectedSiteContact", newSiteContact);
        cmp.set("v.attrIsSiteContModalOpen", true);
    },

    editSiteContact: function (cmp, event, siteContIndex) {
        console.log('==== Helper - editSiteContact =====');

        console.log('Helper.editSiteContact - siteContId: ' + siteContIndex);

        var siteContactList = cmp.get("v.attrSiteContactList");

        var results = siteContactList.filter(function (item) {
            return item.siteContIndex == siteContIndex;
        });

        cmp.set("v.attrSelectedSiteContact", this.cloneSiteContact(results[0]));
        cmp.set("v.attrIsSiteContModalOpen", true);
    },

    cloneSiteContact: function (siteContact) {
        console.log('==== Helper - cloneSiteContact =====');

        var clonedSiteContact = {
            'siteContId': siteContact.siteContId,
            'siteContIndex':siteContact.siteContIndex,
            'siteContFirstName': siteContact.siteContFirstName,
            'siteContSurname': siteContact.siteContSurname,
            'siteContEmail': siteContact.siteContEmail,
            'siteContNumber': siteContact.siteContNumber
        };

        return clonedSiteContact;
    },

    busContModalCancel: function (cmp) {
        cmp.set("v.attrIsBusContModalOpen", false);
		cmp.set("v.responseStatusContactPage", "");
        cmp.set("v.responseMessageContactPage", "");
        cmp.set("v.contactBannertype",""); 
    },

    busContModalSave: function (cmp, event, helper) {
		console.log('==== Helper - busContModalSave =====');
		var isValid = this.checkRequiredFields(cmp, 'busContFields');
		console.log('==== isValid ===== ' + isValid);

        if (isValid) {			
            var location = cmp.get("v.location");
            var op = JSON.stringify(location);
            console.log('==== Helper - busContModalSave =====' + op);
			var updatedBusinessContactList;
            var selectedBusinessContact = cmp.get("v.attrSelectedBusinessContact");
            var businessContactList = cmp.get("v.attrBusinessContactList");
            debugger;
            var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

            var action = helper.getApexProxy(cmp, "c.addBusinessContactToContactsAndOppContactRole");
            var quoteId = cmp.get("v.location").id;
            var businessContactString = selectedBusinessContact;
            var contactRoleType = "Business Contact";
            action.setParams({
                "quoteId": quoteId,
                "jsonContactDetails": JSON.stringify(businessContactString),
                "contactRoleType": contactRoleType,
                "strOrderId" : cmp.get("v.orderId")
            });

            action.setCallback(this, function (response) {
                var state = response.getState();
                console.log('busContModalSave - state: ' + state);
                helper.clearErrors(cmp);
                if (state === "SUCCESS") {
					var filteredList = businessContactList.filter(function (item) {
               			 return item.busContIndex == selectedBusinessContact.busContIndex;
            		});

					if(filteredList.length == 0){
						businessContactList.push(selectedBusinessContact);
						cmp.set("v.attrBusinessContactList", selectedBusinessContact);
					}
                    var responseReturnValue = response.getReturnValue();
                    console.log('busContModalSave - responseReturnValue: ' + responseReturnValue);
					cmp.set("v.responseStatusContactPage", "");
                	cmp.set("v.responseMessageContactPage", "");
                	cmp.set("v.contactBannertype",""); 
                    selectedBusinessContact.busContId = responseReturnValue;
                    console.log('!!!!!!!'+ JSON.stringify(selectedBusinessContact));
                    updatedBusinessContactList = businessContactList.map(function (item) {
                		return item.busContIndex === selectedBusinessContact.busContIndex ? selectedBusinessContact : item;
           			 });
                    
                    location.businessContactList = updatedBusinessContactList;
                    cmp.set("v.attrBusinessContactList", updatedBusinessContactList);
            		cmp.set("v.attrSelectedBusinessContact", selectedBusinessContact);

					location.businessContactList = updatedBusinessContactList; 
 					cmp.set("v.location", location);
                    helper.clearErrors(cmp);
                    helper.busContModalCancel(cmp);

                    if ( cmp.get('v.siteContactSame') === true ) {
                        helper.copyBusinessContactToSiteContact(cmp, event, helper);
                    }
                } else {
                    helper.showErrorBannerFromResponse(cmp, event, helper, response, true);
                }
            });
            $A.enqueueAction(action.delegate());
            console.log('==== Helper - selectedBusinessContact firstname =====' + selectedBusinessContact.busContFirstName);

        }
    },
    
    checkRequiredFields: function(cmp, field)	{
        var isNotEmpty = true;
        var requiredFields = cmp.find(field);
        if(requiredFields.length!=undefined) {
            var allValid = requiredFields.reduce(function (validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid;
            }, true);
            if (!allValid) {
                isNotEmpty = false; 
            }
        }
        return isNotEmpty;
    },

    siteContModalCancel: function (cmp) {
        cmp.set("v.attrIsSiteContModalOpen", false);
		cmp.set("v.responseStatusContactPage", "");
        cmp.set("v.responseMessageContactPage", "");
        cmp.set("v.contactBannertype",""); 
    },

 	siteContModalSave: function (cmp, event, helper, checkRequiredFields) {

		var statusFlag;
		var isValid = true;

		if ( checkRequiredFields === true ) {
            isValid = this.checkRequiredFields(cmp, 'siteContFields');
            console.log('==== isValid ===== ' + isValid);
        }


        if (isValid) {
            var location = cmp.get("v.location");
            var selectedSiteContact = cmp.get("v.attrSelectedSiteContact");
            var siteContactList = cmp.get("v.attrSiteContactList");
			var updatedSiteContactList;
			var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

			// pull the site contact from the first record
            if ( checkRequiredFields != true ) {
                    selectedSiteContact = siteContactList[0];
            }


            var action = helper.getApexProxy(cmp, "c.addSiteContactToContactsAndOppContactRole");

            var quoteId = cmp.get("v.location").id;
            var siteContactString = selectedSiteContact;
            var contactRoleType = "Site Contact";

            action.setParams({
                "quoteId": quoteId,
                "jsonContactDetails": JSON.stringify(siteContactString),
                "contactRoleType": contactRoleType,
                 "strOrderId" : cmp.get("v.orderId")
            });

            action.setCallback(this, function (response) {
                var state = response.getState();
                helper.clearErrors(cmp);

                if (state === "SUCCESS") {
                    statusFlag = "SUCCESS";

					var filteredList = siteContactList.filter(function (item) {
               			 return item.siteContIndex == selectedSiteContact.siteContIndex;
            		});

					if(filteredList.length == 0){
						siteContactList.push(selectedSiteContact);
						cmp.set("v.attrSiteContactList", selectedSiteContact);
					}

                    var responseReturnValue = response.getReturnValue();

                    cmp.set("v.responseStatusContactPage", "");
                	cmp.set("v.responseMessageContactPage", "");
                	cmp.set("v.contactBannertype",""); 

                	selectedSiteContact.siteContId = responseReturnValue;
                    updatedSiteContactList = siteContactList.map(function (item) {
               			 return item.siteContIndex === selectedSiteContact.siteContIndex ? selectedSiteContact : item;
            		});
                    
                    location.siteContactList = updatedSiteContactList;

                    cmp.set("v.attrSiteContactList", updatedSiteContactList);
                    cmp.set("v.attrSelectedSiteContact", selectedSiteContact);

                    location.siteContactList = updatedSiteContactList; 
                     cmp.set("v.location", location);
             		helper.clearErrors(cmp);
                    helper.siteContModalCancel(cmp);
                } else {
                    helper.showErrorBannerFromResponse(cmp, event, helper, response, true);
                }
            });
            $A.enqueueAction(action.delegate());
        }
    },

    unlinkSiteContact: function (cmp, event, helper) {

        var action = helper.getApexProxy(cmp, "c.unlinkSiteContactToContactsAndOppContactRole");

        action.setParams({
                "quoteId":          cmp.get("v.location").id,
                "contactRoleType":  "Site Contact",
                "strOrderId" :      cmp.get("v.orderId")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            //helper.clearErrors(cmp);

            if (state === "SUCCESS") {
                console.log('Unlinked the site contact');
            } else {
                helper.showErrorBannerFromResponse(cmp, event, helper, response, true);
            }
        });
        $A.enqueueAction(action.delegate());
    },


    loadStateOptions: function (cmp, event) {
        console.log('==== Helper - loadStateOptions =====');

        var opts = [{
                value: "",
                label: "Select"
            },
            {
                value: "NSW",
                label: "New South Wales"
            },
            {
                value: "VIC",
                label: "Victoria"
            },
            {
                value: "QLD",
                label: "Queensland"
            },
            {
                value: "WA",
                label: "Western Australia"
            },
            {
                value: "SA",
                label: "South Australia"
            },
            {
                value: "TAS",
                label: "Tasmania"
            },
            {
                value: "ACT",
                label: "Australian Capital Territory"
            },
            {
                value: "NT",
                label: "Northern Territory"
            }
        ];
        cmp.set("v.attrStateOptions", opts);
    },

    copyBusinessContactToSiteContact: function(cmp, event, helper) {

        var businessContactList = cmp.get('v.attrBusinessContactList');
        if (businessContactList.length > 0) {
            // reset the site contact list too one item  only copied from first business contact
            var bc = cmp.get("v.attrSelectedBusinessContact");
            if (bc.busContIndex == undefined ||  bc === null || bc === undefined)  {
                var bcList = cmp.get('v.attrBusinessContactList');
                bc = bcList[0];
            }
            var siteContactList = [];
            siteContactList.push({
                'siteContId': '',
                'siteContIndex': 1,
                'siteContFirstName': bc.busContFirstName,
                'siteContSurname': bc.busContSurname,
                'siteContEmail': bc.busContEmail,
                'siteContNumber': bc.busContNumber
            });
            cmp.set('v.attrSelectedSiteContact', siteContactList[0]);
            cmp.set('v.attrSiteContactList', siteContactList);

            helper.siteContModalSave(cmp, event, helper);
        }
    },



})