({
	init: function(cmp, event, helper) {	
		console.log('==== Controller - init =====');
		
		var location = cmp.get("v.location");
		console.log("ORDER Id: " + location.orderId);
		cmp.set("v.orderId", location.orderId);

        helper.loadOrderInstallationDetails(cmp, event, helper);
        helper.getHolidays(cmp,event, helper);
	},

    saveLocalButton : function(cmp, event, helper){
        console.log('==== Controller - clickSaveandCloseButton =====');
    	helper.saveOrderDetails(cmp, event, helper, true);
    },

    clickBackButton: function(cmp, event, helper) {
    	console.log('==== Controller - clickBackButton =====');
    	
		helper.goToPreviousPage(cmp, event);		
	},
	
    clickOrderSummaryButton: function(cmp, event, helper) {			
    	console.log('==== Controller - clickOrderSummaryButton =====');
        helper.saveOrderDetails(cmp, event, helper, true);
	},

    clickSubmitOrderButton: function(cmp, event, helper) {	

        var isError = false;
        
        // To check if Terms were accepted or not
        if(!cmp.get("v.termsAccepted")) {
            isError = true;
            cmp.set("v.termsAcceptedError" , $A.get("$Label.c.NS_Order_Terms_Acceptance_Error"));            
        } else {
            cmp.set("v.termsAcceptedError" , "");            
        }

        var isValid = helper.validateBusinessDays(cmp, event, helper);

        if (isValid === false) {
            isError = true;
        }

        if(!isError) {
        	cmp.set("v.openInstallationPopup", true);
        }

	},		
    
	onDateChange: function(cmp, event, helper){
        helper.validateBusinessDays(cmp, event, helper);
	},

    closeInstallationModal:function(cmp, event, helper) {
        cmp.set("v.openInstallationPopup", false);
    },
    
    confirmSubmission:function(cmp, event, helper) {  
        console.log('==== Controller - confirmSubmission =====');

        var action = helper.getApexProxy(cmp, "c.saveOrderDetails");
        console.log("INSIDE SAVE");
        helper.setOrderJsonValues(cmp, event);
        var orderJson= JSON.stringify(cmp.get("v.orderJSON"));
        console.log('==SAVE=='+orderJson);
        action.setParams({
            strOrderId : cmp.get("v.orderId"),
            strOrderJson: orderJson
        });
        action.setCallback(this, function() {
            console.log('save order details complete');
            helper.createCSOrder(cmp, event, helper);
        });
        $A.enqueueAction(action.delegate());

    }
})