({  
    getHolidays: function(cmp, event, helper) {
        
        var action = helper.getApexProxy(cmp, "c.getUpcomingHolidays");
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") 
			{
                var holidayDates = response.getReturnValue();
                cmp.set("v.attrHolidays",holidayDates);                
            }
        });
        $A.enqueueAction(action.delegate());
    },

    populateAttributeValues: function(cmp, event){
        var orderJson = cmp.get("v.orderJSON");
        cmp.set("v.attrCustomerRequiredDate", orderJson.resourceOrder.customerRequiredDate);
        cmp.set("v.attrBatteryBckUp", orderJson.resourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem[0].itemInvolvesResource.batteryBackupRequired);
        console.log('---batterybackup',orderJson.resourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem[0].itemInvolvesResource.batteryBackupRequired);
        cmp.set("v.attrInstallDetailsNotes", orderJson.resourceOrder.additionalNotes);
    },

    setOrderJsonValues: function(cmp, event){
        try{
            var orderJson = cmp.get("v.orderJSON");
            console.log('orderJson',orderJson);
            orderJson.resourceOrder.customerRequiredDate=cmp.get("v.attrCustomerRequiredDate");
            orderJson.resourceOrder.additionalNotes=cmp.get("v.attrInstallDetailsNotes");
            
            var batteryBackup = cmp.get("v.attrBatteryBckUp");
            console.log('::Battery Backup value before',batteryBackup);
            
            orderJson.resourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem[0].itemInvolvesResource.batteryBackupRequired = batteryBackup;
            console.log('::Battery Backup value after', orderJson.resourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem[0].itemInvolvesResource.batteryBackupRequired);
            
            cmp.set("v.orderJSON", orderJson);
        }catch(err){
            console.log(err);
        }
        
    },
    
    loadOrderInstallationDetails: function (cmp, event, helper){
        
        var action = helper.getApexProxy(cmp, "c.getOrderDetails");

        action.setParams({
            strOrderId: cmp.get("v.orderId")
        });
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resp = response.getReturnValue();
                if (resp != "ERROR") {
                    cmp.set('v.orderJSON',JSON.parse(response.getReturnValue()));
                    console.log('==v.orderJSON=='+JSON.stringify(cmp.get('v.orderJSON')));
                    helper.populateAttributeValues(cmp, event, helper);
                }
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action.delegate());
    },

    goToPreviousPage: function(cmp, event) {
        console.log('==== Helper - goToPreviousPage =====');
        console.log('Helper.goToPreviousPage - location: ' + JSON.stringify(cmp.get("v.location")));

        var evt = cmp.getEvent("NSorderContactDetailsPageEvent");
        evt.fire();
        
    }, 
    
    goToOrderSummaryPage: function(cmp, event, isNewOrderCreated) {
        var evt = cmp.getEvent("NSorderSummaryPageEvent");
        evt.setParams({
            "selectedOrder": cmp.get("v.orderId"),
            "isFromQuotePage": true, //render button link back to quote (pre-order summary) page
            "isNewOrderCreated": isNewOrderCreated
       });
        console.log('goto Order summary page event fired');
        evt.fire();
   },

    goToPreOrderSummaryPage: function(cmp, event) {
            var location = cmp.get('v.location');
            console.log("LOCATION: "+ location);
            var evt = cmp.getEvent("NSquoteDetailPageEvent");
            evt.setParams({
                "location": location
            });
            evt.fire();
    },

   saveOrderDetails: function(cmp, event, helper, gotoPreOrderSummaryPage){
       this.setOrderJsonValues(cmp, event);
       var orderJson= JSON.stringify(cmp.get('v.orderJSON'));

       var action = helper.getApexProxy(cmp, "c.saveOrderDetails");

       action.setParams({
           strOrderId : cmp.get("v.orderId"),
           strOrderJson: orderJson
       });
       action.setCallback(this, function() {
           console.log('save order details complete: ' + gotoPreOrderSummaryPage);
           if (gotoPreOrderSummaryPage === true) {
               helper.goToPreOrderSummaryPage(cmp, event);
           }
       });
       $A.enqueueAction(action.delegate());
   },

   createCSOrder: function(cmp, event, helper){
       console.log('==== Helper - createCSOrder =====');
       var dfOrderId= cmp.get("v.orderId");

       var action = helper.getApexProxy(cmp, "c.createCSOrders");

       action.setParams({
           strOrderId : cmp.get("v.orderId")
       });

       action.setCallback(this, function (response) {
           var state = response.getState();
           console.log('===== createCSOrder ===== state ' + state)
           if (state === "SUCCESS") {
               var resp = response.getReturnValue();
               console.log("resp: " + resp);
               if(resp == "Passed"){
                   helper.submitOrder(cmp, event, helper);
               }
           }

       });
       $A.enqueueAction(action.delegate());
   },

   submitOrder: function(cmp, event, helper) {
       console.log('==== Helper - submitOrder =====');

       var action = helper.getApexProxy(cmp, "c.submitOrder");

       action.setParams({
           strOrderId : cmp.get("v.orderId")
       });
       action.setCallback(this, function() {
           console.log('submit Order complete');
           helper.goToOrderSummaryPage(cmp, event, true);
       })
       $A.enqueueAction(action.delegate());
   },


   validateBusinessDays : function(cmp,event, helper) {
        var inputValue = cmp.get("v.attrCustomerRequiredDate");

        var inputCmp = cmp.find("CustomerRequiredDate");

        debugger;
        if  ($A.util.isEmpty(inputValue)) {
            if (inputCmp.checkValidity() === false) {
                return false;
            }
            helper.setError(inputCmp, "");
            return true;
        }

        var inputDate = new Date(inputValue);
        var counter = 0;
        var currentDate = new Date();
        var holidayDates = cmp.get("v.attrHolidays");

        while(true) {
            currentDate.setDate(currentDate.getDate()+1);
            var dayOfWeek = currentDate.getDay();
            var currentDateString = currentDate.getFullYear() + "-" + ("0" + (currentDate.getMonth() + 1)).slice(-2) + "-" + ("0" + currentDate.getDate()).slice(-2);
            // Debugging code
            // --------------------------
            if(holidayDates.indexOf(currentDateString) != -1) {
                   console.log('Holiday Found ' + currentDateString);
            }
            // ------------------------------

            if(dayOfWeek >= 1 && dayOfWeek <= 5 && holidayDates.indexOf(currentDateString) == -1) {
                   counter++;
            }

            if(currentDate >= inputDate) {
                   break;
            }
        }

       // validate the business days
       var validDayCount = $A.get("$Label.c.NS_Customer_Required_Date_DaysCount");
       if(parseInt(counter) < parseInt(validDayCount)) {
           helper.setError(inputCmp, $A.get("$Label.c.NS_Customer_Required_Date_Error"));
           return false;
       } else {
           helper.setError(inputCmp, "");
       }

       return true;
       },

    setError: function(cmp, error) {
        cmp.setCustomValidity(error);
        cmp.reportValidity();
        },


})