({
	init : function(component, event, helper) {
        // set step value of progress bar
        var tabValue = component.get("v.nsCurrentTabValue");
        if (typeof tabValue === 'string') {
            component.set("v.nsCurrentTabValue", parseInt(tabValue, 10));
        }

        component.set("v.nsIsNewOrder","false");
        component.set("v.isValidationError", false);
        var location = component.get("v.location");
        /*Set this value from Location Attribute*/
		
        console.log("ORDER Id: " + location.orderId);
		component.set("v.orderId", location.orderId);
        //component.set("v.orderId", 'a7Ip00000003WDk');
        console.log("Order: " + component.get("v.orderId"));

        var action = helper.getApexProxy(component, "c.getOrderDetails");

        action.setParams({
            strOrderId: component.get("v.orderId")//"a7Ip00000003WDk"//
        });
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var resp = response.getReturnValue();
                if (resp != "ERROR") {
                    component.set('v.orderJSON',JSON.parse(response.getReturnValue()));
                    helper.populateAttributeValues(component,helper);
                    helper.getComponentsOnLoad(component);
                }
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action.delegate());
	},

    clickNextButton : function(component, event, helper){
        helper.validateAttributes(component, event, helper);
        var isError = component.get("v.isValidationError");

        if(isError){
            component.set("v.responseStatusLocation", "ERROR");
            component.set("v.responseMessageLocationPage", "Please fill mandatory fields");
            component.set("v.locationBannertype", "Error");
            component.set("v.isValidationError", true);
        }else{
            helper.proceedToNextScreen(component, event, helper);
            component.set("v.responseStatusLocation", "");
            component.set("v.responseMessageLocationPage", "");
            component.set("v.locationBannertype", "");
            component.set("v.isValidationError", false);
        }
    },

    clickBackButton : function(component, event, helper){
        var tabVal = component.get("v.nsCurrentTabValue");
        if(tabVal>1){
            tabVal--;
        }
        component.set("v.nsCurrentTabValue",tabVal);
        helper.setOwnerAsTrue(component);

        helper.clearErrors(component);
    },

    saveLocalButton : function(component, event, helper){
        helper.validateAttributes(component, event);
        var isError = component.get("v.isValidationError");
        if(!isError){
            helper.saveOrderDetails(component, event, helper, false, true);
        }
    },

    clickOrderSummaryButton : function(component, event, helper){
        helper.goToOrderSummaryPage(component, event, helper);
    },

})