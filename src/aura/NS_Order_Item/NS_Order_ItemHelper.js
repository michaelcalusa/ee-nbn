({
    goToOrderSummaryPage: function (component, event) {
        
        var location = component.get('v.location');
        console.log("LOCATION: "+ location);
        var evt = component.getEvent("NSquoteDetailPageEvent");        
        evt.setParams({
            "location": location
        });
        evt.fire();
    },
    populateAttributeValues: function(component, event){
        var orderJson = component.get("v.orderJSON");
        component.set("v.nsSiteTypeValue",orderJson.resourceOrder.siteType);
        component.set("v.nsSiteNameValue",orderJson.resourceOrder.siteName);
        component.set("v.nsHeritageSiteValue",orderJson.resourceOrder.heritageSite);
        component.set("v.nsTradingNameValue",orderJson.resourceOrder.tradingName);
        component.set("v.nsPropertyOwnerStatusValue",orderJson.resourceOrder.propertyOwnershipStatus);
        if(!($A.util.isEmpty(orderJson.resourceOrder.propertyOwnershipStatus)) && !(orderJson.resourceOrder.propertyOwnershipStatus.toUpperCase() == 'OWNED' ||orderJson.resourceOrder.propertyOwnershipStatus.toUpperCase() == 'LEASED')){
            component.set("v.nsPropertyOwnerStatusValue", "Other");
            component.set("v.nsOtherPropertyOwnerStatusValue", orderJson.resourceOrder.propertyOwnershipStatus);
        }else{
            component.set("v.nsPropertyOwnerStatusValue", orderJson.resourceOrder.propertyOwnershipStatus);
        }
        if(!($A.util.isEmpty(orderJson.resourceOrder.keysRequired)) && !(orderJson.resourceOrder.keysRequired.toUpperCase() == 'NO' ||orderJson.resourceOrder.keysRequired.toUpperCase() == 'UNSURE')){
            component.set("v.nsAreKeysRequiredValue", "Yes");
            component.set("v.nsKeysReqdValue", orderJson.resourceOrder.keysRequired);
        }else{
            component.set("v.nsAreKeysRequiredValue", orderJson.resourceOrder.keysRequired);
        }
        component.set("v.nsSecurityRequirementsValue",orderJson.resourceOrder.securityRequirements);
        component.set("v.nsSiteAccessInstructionsValue",orderJson.resourceOrder.siteAccessInstructions);
        component.set("v.nsContractedLocationInstructionsValue",orderJson.resourceOrder.contractedLocationInstructions);
        component.set("v.nsBuildOwnerValue",orderJson.resourceOrder.ownerAwareOfProposal);
        component.set("v.nsInductionRequiredValue",orderJson.resourceOrder.inductionRequired);
        component.set("v.nsSafetyHazardsValue",orderJson.resourceOrder.safetyHazards);
        component.set("v.nsAfterHoursSiteVisitValue",orderJson.resourceOrder.afterHoursSiteVisit);
        component.set("v.nsJsaSwmValue",orderJson.resourceOrder.jsaswmRequired);
        component.set("v.nsWorkPracticeValue",orderJson.resourceOrder.workPracticesRequired);
        component.set("v.nsWorkingAtHeightsValue",orderJson.resourceOrder.workingAtHeightsConsideration);
        component.set("v.nsSiteNotesValue",orderJson.resourceOrder.notes);
        component.set("v.nsConfinedSpacesValue",orderJson.resourceOrder.confinedSpaceRequirements);        
    },
    setOrderJsonValues: function(component, event){
        var orderJson = component.get("v.orderJSON");
        orderJson.resourceOrder.siteType = component.get("v.nsSiteTypeValue");
        orderJson.resourceOrder.siteName = component.get("v.nsSiteNameValue");
        orderJson.resourceOrder.heritageSite = component.get("v.nsHeritageSiteValue");
        orderJson.resourceOrder.tradingName = component.get("v.nsTradingNameValue");
        orderJson.resourceOrder.propertyOwnershipStatus = component.get("v.nsPropertyOwnerStatusValue");
        if(!($A.util.isEmpty(component.get("v.nsPropertyOwnerStatusValue"))) && !(component.get("v.nsPropertyOwnerStatusValue").toUpperCase() == 'OWNED' || component.get("v.nsPropertyOwnerStatusValue").toUpperCase() == 'LEASED')){
            orderJson.resourceOrder.propertyOwnershipStatus = component.get("v.nsOtherPropertyOwnerStatusValue");
        }else{
            orderJson.resourceOrder.propertyOwnershipStatus = component.get("v.nsPropertyOwnerStatusValue");
        }
        if(!($A.util.isEmpty(component.get("v.nsAreKeysRequiredValue"))) && !(component.get("v.nsAreKeysRequiredValue").toUpperCase() == 'NO' || component.get("v.nsAreKeysRequiredValue").toUpperCase() == 'UNSURE')){
            orderJson.resourceOrder.keysRequired = component.get("v.nsKeysReqdValue");
        }else{
            orderJson.resourceOrder.keysRequired = component.get("v.nsAreKeysRequiredValue");
        }
        orderJson.resourceOrder.securityRequirements = component.get("v.nsSecurityRequirementsValue");
        orderJson.resourceOrder.siteAccessInstructions = component.get("v.nsSiteAccessInstructionsValue");
        orderJson.resourceOrder.contractedLocationInstructions = component.get("v.nsContractedLocationInstructionsValue");
        orderJson.resourceOrder.ownerAwareOfProposal = component.get("v.nsBuildOwnerValue");
        orderJson.resourceOrder.inductionRequired = component.get("v.nsInductionRequiredValue");
        orderJson.resourceOrder.safetyHazards = component.get("v.nsSafetyHazardsValue");
        orderJson.resourceOrder.afterHoursSiteVisit = component.get("v.nsAfterHoursSiteVisitValue");
        orderJson.resourceOrder.jsaswmRequired = component.get("v.nsJsaSwmValue");
        orderJson.resourceOrder.workPracticesRequired = component.get("v.nsWorkPracticeValue");
        orderJson.resourceOrder.workingAtHeightsConsideration = component.get("v.nsWorkingAtHeightsValue");
        orderJson.resourceOrder.notes = component.get("v.nsSiteNotesValue");
        orderJson.resourceOrder.confinedSpaceRequirements = component.get("v.nsConfinedSpacesValue");
        component.set("v.orderJSON", orderJson);
    },

    proceedToNextScreen: function(component, event, helper){
        var tabVal = component.get("v.nsCurrentTabValue");
        
        if(tabVal<3){
            tabVal++;
            helper.getComponentsOnLoad(component);
            helper.saveOrderDetails(component, event, helper);
        }else{
            helper.saveOrderDetails(component, event, helper, true);

        }
        component.set("v.nsCurrentTabValue",tabVal);
        /*Set Owner Aware as true by default if the owner is raising the request*/
        this.setOwnerAsTrue(component);
    },

    saveOrderDetails: function(component, event, helper, gotoNextPage, gotoOrderSummaryPage){
        console.log('saveOrderDetails gotoNextPage: ' + gotoNextPage);

        var action = helper.getApexProxy(component, "c.saveOrderDetails");
        helper.setOrderJsonValues(component, event);
        var orderJson= JSON.stringify(component.get('v.orderJSON'));
        action.setParams({
            strOrderId : component.get("v.orderId"),
            strOrderJson: orderJson
        });
        // var nextPage = gotoNextPage;
        action.setCallback(this, function() {
            console.log('saved the order gotoNextPage: ' + gotoNextPage);
            if (gotoNextPage === true) {
                var evt = component.getEvent("NSorderContactDetailsPageEvent");
                evt.setParams({
                    "location": component.get("v.location")
                });
                evt.fire();
            }
            if (gotoOrderSummaryPage === true) {
                helper.goToOrderSummaryPage(component, event);
            }
        });
        $A.enqueueAction(action.delegate());
    },

    validateAttributes: function(component, event, helper){
        var tabVal = component.get("v.nsCurrentTabValue");
        if(tabVal==1){
            this.validateScreen1(component, event, helper);
        }else if(tabVal==2){
            this.validateScreen2(component, event, helper);
        }else if(tabVal==3){
            this.validateScreen3(component, event, helper);
        }
    },
    validateScreen1: function(component, event, helper){
        component.set("v.isValidationError", false);
        var srcCmp = event.getSource();
        this.validateField(component, component.find("nsSiteName"), srcCmp);
        this.validateField(component, component.find("nsSiteType"), srcCmp);
        this.validateField(component, component.find("nsTradingName"), srcCmp);
        this.validateField(component, component.find("nsHeritageSite"), srcCmp);
        this.validateField(component, component.find("nsPropertyOwnerStatusRadio"), srcCmp);
        if(!($A.util.isEmpty(component.get("v.nsPropertyOwnerStatusValue"))) && !(component.get("v.nsPropertyOwnerStatusValue").toUpperCase() == 'OWNED' || component.get("v.nsPropertyOwnerStatusValue").toUpperCase() == 'LEASED')){
            this.validateField(component, component.find("nsOtherPropertyOwnerStatus"), srcCmp);
        }
    },
    validateScreen2: function(component, event, helper){
        component.set("v.isValidationError", false);
        var srcCmp = event.getSource();
        this.validateField(component, component.find("nsAreKeysRequiredRadio"), srcCmp);
        if(!($A.util.isEmpty(component.get("v.nsAreKeysRequiredValue"))) && !(component.get("v.nsAreKeysRequiredValue").toUpperCase() == 'NO' || component.get("v.nsAreKeysRequiredValue").toUpperCase() == 'UNSURE')){
            this.validateField(component, component.find("nsKeysReqd"), srcCmp);
        }
        this.validateField(component, component.find("nsSecurityRequirements"), srcCmp);
        this.validateField(component, component.find("nsSiteAccessInstructions"), srcCmp);
        this.validateField(component, component.find("nsContractedLocationInstructions"), srcCmp);
        this.validateField(component, component.find("nsBuildOwnerRadio"), srcCmp);
    },
    validateScreen3: function(component, event, helper){
        component.set("v.isValidationError", false);
        var srcCmp = event.getSource();
        this.validateField(component, component.find("nsInductionRequiredRadio"), srcCmp);
        this.validateField(component, component.find("nsSafetyHazardsRadio"), srcCmp);
        this.validateField(component, component.find("nsAfterHoursSiteVisitRadio"), srcCmp);
        this.validateField(component, component.find("nsJsaSwmRadio"), srcCmp);
        this.validateField(component, component.find("nsWorkPracticeRadio"), srcCmp);
    },
    validateField: function(component, inputCmp, sourceCmp){
        if($A.util.isEmpty(inputCmp.get("v.value"))){
            inputCmp.set("v.required","true");
            this.setMissingValueMessage(component,inputCmp);
            inputCmp.focus();
            sourceCmp.focus();
            inputCmp.focus();          
            $A.util.addClass(inputCmp, "slds-has-error");
            component.set("v.isValidationError", true);
        }
	},
    reFocus: function(inputCmp, tempCtrl){
		inputCmp.focus();
		tempCtrl.focus();
		inputCmp.focus();
	},
    getComponentsOnLoad: function(component){
        this.setMissingValueMessage(component,component.find("nsSiteName"));
        this.setMissingValueMessage(component,component.find("nsSiteType"));
        this.setMissingValueMessage(component,component.find("nsTradingName"));
        this.setMissingValueMessage(component,component.find("nsHeritageSite"));
        this.setMissingValueMessage(component,component.find("nsPropertyOwnerStatusRadio"));
        this.setMissingValueMessage(component,component.find("nsAreKeysRequiredRadio"));
        this.setMissingValueMessage(component,component.find("nsSecurityRequirements"));
        this.setMissingValueMessage(component,component.find("nsSiteAccessInstructions"));
        this.setMissingValueMessage(component,component.find("nsContractedLocationInstructions"));
        this.setMissingValueMessage(component,component.find("nsBuildOwnerRadio"));
        this.setMissingValueMessage(component,component.find("nsInductionRequiredRadio"));
        this.setMissingValueMessage(component,component.find("nsSafetyHazardsRadio"));
        this.setMissingValueMessage(component,component.find("nsAfterHoursSiteVisitRadio"));
        this.setMissingValueMessage(component,component.find("nsJsaSwmRadio"));
        this.setMissingValueMessage(component,component.find("nsWorkPracticeRadio"));
    },
    setMissingValueMessage: function(component, inputComponent){
        if(!$A.util.isEmpty(inputComponent)){
            //inputComponent.set("v.messageWhenValueMissing", inputComponent.get("v.name") + " is mandatory");
            inputComponent.set("v.messageWhenValueMissing", "This field is mandatory");
        }
    },
    resetRequiredField: function(component, inputComponent){
         if(!$A.util.isEmpty(inputComponent.get("v.value"))){
             inputComponent.set("v.required",null);
             $A.util.removeClass(inputComponent, "slds-has-error");
         }
    },
    setOwnerAsTrue: function(component){
        if((component.get("v.nsPropertyOwnerStatusValue").toUpperCase() == 'OWNED')){
            component.set("v.nsBuildOwnerValue", "Yes");
            component.find("nsBuildOwnerRadio").set("v.disabled", true);
        }
    }
})