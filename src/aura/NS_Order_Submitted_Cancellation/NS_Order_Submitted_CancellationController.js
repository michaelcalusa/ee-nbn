/**
 * Created by philipstafford-jones on 15/4/19.
 */
({
    init: function (cmp, event, helper) {
        console.log('Order cancel init handler called');
        const promise1 = helper.apex( cmp,
            'v.canCancel',
            'c.canCancelOrder',
            { orderId: cmp.get('v.orderId') },
            false);

        Promise.all([promise1])
            .then(function () {
                cmp.set('v.isOrderCancelFeatureEnabled', true);
                cmp.set('v.initDone', true);
            })
            .catch(function () {
                helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
            }).finally(function(){
            helper.fireHideSpinnerEvent(cmp);
        });
    },

    showModal: function (cmp, event, helper) {
        cmp.set('v.showModal', true);
    },

    hideModal: function (cmp, event, helper) {
        cmp.set('v.showModal', false);
        helper.clearErrors(cmp);
    },

    confirmCancellation: function (cmp, event, helper) {
        const orderId = cmp.get('v.orderId');
        console.log('orderId: ' + orderId);

        console.log('comp: ' + cmp.getName());

        const action = helper.getApexProxy(cmp, 'c.cancelOrder');

        action.setParams({ orderId: orderId });

        action.setCallback(this, function (response) {
            const state = response.getState();
            console.log('state: ' + state);
            const result = response.getReturnValue();

            if (state === "SUCCESS") {
                if(result.data !== undefined && result.data.length > 0) {
                    helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
                } else {
                    const  evt = $A.get('e.c:NS_ResponseMessage_Event');
                    evt.setParam('status', 'OK');
                    evt.setParam('transient', true);
                    evt.setParam('message', 'Successfully submitted cancellation for this Order');
                    evt.setParam('scrollToTop', true);
                    evt.fire();

                    cmp.set('v.showModal', false);
                    cmp.set('v.canCancel', false);
                }
            } else {
                if(result && result.error && result.error[0].message === 'Can_Not_Cancel') {
                    helper.setErrorMsg(cmp, "ERROR", 'Order can not be cancelled', "BANNER");
                } else {
                        helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
                }
            }
        });
        $A.enqueueAction(action.delegate());

        // Promise.all([promise])
        //     .then(function (result) {
        //         if(result.data != undefined && result.data.length > 0) {
        //             helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
        //         } else {
        //             debugger;
        //             var  evt = $A.get('e.c:NS_ResponseMessage_Event');
        //             evt.setParam('status', 'OK');
        //             evt.setParam('transient', true);
        //             evt.setParam('message', 'Successfully submitted cancellation for this Order');
        //             evt.fire();
        //
        //             cmp.set('v.showModal', false);
        //             cmp.set('v.canCancel', false);
        //         }
        //      }).catch(function (result) {
        //          debugger;
        //         if(result.error[0].message == 'Can_Not_Cancel') {
        //             v.customSettings.Order_Cancel_Not_Cancellable
                    // helper.setErrorMsg(cmp, "ERROR", 'Can not cancel', "BANNER");
                // } else {
                //     helper.setErrorMsg(cmp, "ERROR", $A.get("$Label.c.DF_Application_Error"), "BANNER");
                // }
            // }).finally(function(){
            //     helper.fireHideSpinnerEvent(cmp);
            // });
    },

});