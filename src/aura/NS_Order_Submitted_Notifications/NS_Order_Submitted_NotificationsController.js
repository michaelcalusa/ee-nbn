/*NS Order Notifications*/
({	 
	clickLink: function(cmp, event, helper) {	
		
	},  

	init: function(cmp, event, helper) {
		var action = helper.getApexProxy(cmp, 'c.getOrderNotifications');

        var ordId;
        if(cmp.get('v.selectedOrder.Id') == null || cmp.get('v.selectedOrder.Id') == '') {
            ordId = cmp.get('v.orderId');
        }
        else {
            ordId = cmp.get('v.selectedOrder.Id');
        }

        action.setParams({
            "orderId" : ordId
        });
        
        action.setCallback(this, function(result){
            var state = result.getState(); // get the response state
            
            if(state == 'SUCCESS') {                
                cmp.set('v.fullList', result.getReturnValue());
                cmp.set("v.currentList", result.getReturnValue());
                helper.setAvcFlag(cmp);
                helper.renderPage(cmp);
            }
        });
        $A.enqueueAction(action.delegate());
	},
	 
	scriptsLoaded: function(cmp, event, helper) {
	},

	renderPage: function(cmp, event, helper) {    
        helper.renderPage(cmp);
    },   
	
})