/*NS Order Summary*/
({
	renderPage: function(cmp) {
		
        var records = cmp.get("v.fullList"),	
        pageNumber = cmp.get("v.pageNumber"),
        pageCountVal = cmp.get("v.selectedCount"),

        pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));
		
        cmp.set("v.maxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
		cmp.set('v.currentList', []);
        cmp.set("v.currentList", pageRecords);
        
    },

    setAvcFlag: function(component){
        var fullList = component.get("v.fullList");
        var avcNote = component.find("avcNote");
        console.log('LENGTH: ' + fullList.length);
        for(var i=0;i< fullList.length; i++){
            console.log('Status: ' + fullList[i].status);
            if(fullList[i].status==='Completed'){
                $A.util.removeClass(avcNote, "slds-hide");
            }
        }
    }
})