/* NS Order summary*/
({
	init: function(cmp, event, helper) {
        //debugger;
        //
        //Uncomment below two lines
        var orderId = cmp.get("v.ordId");
        console.log('------orderId---',orderId);
        
        var batchId = cmp.get("v.parentOppIdParam");
        if (batchId != null) {
            cmp.set("v.batchId", batchId);
        }
        console.log('------batchId---', cmp.get('v.batchId') );

        helper.loadAllData(cmp, event, helper, orderId);

		// helper.loadLocation(cmp, event, helper, orderId);
        // helper.loadBusinessContactList(cmp, event, helper, orderId);
        // helper.loadSiteContactList(cmp, event, helper, orderId);
        // helper.loadNTD(cmp, event, helper, orderId);

        if(cmp.get("v.isNewOrderCreated")){
            helper.showSuccessfullyCreatedOrderMsg(cmp, helper);
            cmp.set('v.isNewOrderCreated', false)
        }
	},
	
	clickBack: function(cmp, event, helper) {
        console.log('insideClickBack');
        var ref = cmp.get("v.backUrl");
        console.log('backurl',ref);
        window.open(cmp.get("v.backUrl"), '_top');
		
	},
		
	returnToSearch: function(cmp, event, helper) {	
        console.log('Inside Return to search');
		cmp.set('v.isOrderDetailsOpen', false);
	},

	backToQuotePage: function(cmp, event, helper) {	
        console.log('insidebacktoQuotePage');

        try{
       	    var batchId =  cmp.get("v.batchId");
            console.log('batchId',batchId);
		    var quotePageEvt = cmp.getEvent("quoteDetailPageEvent");
            quotePageEvt.setParam("batchId",batchId);
            console.log('batchId in OrderSummary',batchId);
            quotePageEvt.fire();
        }catch(err){
            console.log('Error Message',err);
        }
	},

	backToSource: function(cmp, event, helper) {	
		var sourceDisplayedDetailItem = cmp.get('v.sourceDisplayedDetailItem');
		cmp.set('v.displayedDetailItem', sourceDisplayedDetailItem);		

	},

    onActivateTab: function (cmp, event, helper) {
	    var notificationCmp = cmp.find('notificationsTab');
	    notificationCmp.update(notificationCmp, event, helper);
    },

    onActivateDetailsTab: function (cmp, event, helper) {
	    cmp.update(cmp, event, helper);
	    var cancelCmp =cmp.find("cancelOrder");
	    if (cancelCmp) {
            cancelCmp.update(cmp, event, helper);
        }
    }

})