/*NS Order Summary*/
({
    loadAllData: function (cmp, event, helper, orderId) {

        var action = helper.getApexProxy(cmp, 'c.getAllOrderDetails');

        action.setParams({
            "orderId" : orderId //cmp.get('v.selectedOrder.Id')
        });

        action.setCallback(this, function(result){
            var state = result.getState(); // get the response state
            if(state == 'SUCCESS' && result.getReturnValue() != null) {
                var orderData = result.getReturnValue();
                cmp.set('v.location', orderData.location);
                cmp.set('v.businessContactList', orderData.businessContacts);
                cmp.set('v.siteContactList', orderData.siteContacts);
                cmp.set('v.NTD', orderData.NTD);

            }
            console.log('---Helper:::',result.getReturnValue());
        });
        $A.enqueueAction(action.delegate());
    },


    showSuccessfullyCreatedOrderMsg: function(cmp, helper) {
        helper.setBannerMsg(cmp, "OK", "Successfully submitted order for this product!");
        window.setTimeout($A.getCallback(function() {
            helper.clearErrors(cmp);
        }), 5000);
    }

})