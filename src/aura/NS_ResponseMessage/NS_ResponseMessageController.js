({
    handleComponentEvent : function(cmp, event, helper) {

        console.log('Handle NS_ResponseMessage_Event');

        const message =   event.getParam('message');
        const status =     event.getParam('status');
        const transient = event.getParam('transient');
        const show =      event.getParam('show');
        const scrollToTop =      event.getParam('scrollToTop');

        // set the handler attributes based on event data
        if (show === true) {
            cmp.set('v.status', status);
            cmp.set('v.message', message);
        }

        if (transient === true) {
            cmp.set('v.status', status);
            cmp.set('v.message', message);
            window.setTimeout(
                $A.getCallback(function() {
                    cmp.set('v.message', '');
                    cmp.set('v.status', '');
                }), cmp.get('v.delay'));
        }

        if (show === false && transient === false) {
            cmp.set('v.message', '');
        }

        if (scrollToTop === true) {
            console.log('Scroll to top');
            helper.smoothScrollToPageTop(cmp);
        }
    }

});