/**
 * Created by Gobind.Khurana on 18/05/2018.
 */
    ({
        init: function (cmp, event, helper) {
        	console.log('==== Controller.init =====');

        	// Get custom label values
        	var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        	cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);

        	var ERR_MSG_FORM_INPUT_ERROR = $A.get("$Label.c.DF_Form_Input_Error");
        	cmp.set("v.attrFORM_INPUT_ERROR", ERR_MSG_FORM_INPUT_ERROR);
            
            var baseUrl = window.location.href;
            console.log('-----window-----'+baseUrl);
           
            var splittedArray = baseUrl.includes('locid=') ? baseUrl.split('locid=') : [];
            console.log('--splittedArray-- ', splittedArray);
            
            var finalLocId = !$A.util.isEmpty(splittedArray) ? splittedArray[1] : '';
            console.log('--finalLocId---', finalLocId);
            
            cmp.set("v.attrLocationID", finalLocId);
            
            var locationID = cmp.get("v.attrLocationID");
			
            console.log('--locationID---', locationID);
			
            if (!$A.util.isEmpty(locationID)){
            var validateLocId = helper.validateLocationIDSearchInput(cmp, locationID);
                console.log('');
                if (validateLocId){
                    var validSearchRequest = helper.searchByLocationID(cmp, event);
                }
                else {
            	    // Raise error if found
            	    console.log('ERR_MSG_INVALID_INPUTS',ERR_MSG_INVALID_INPUTS);
				    var ERR_MSG_FORM_INPUT_ERROR = cmp.get("v.attrFORM_INPUT_ERROR");
            	    var ERR_MSG_INVALID_INPUTS = 'Search By Location ID inputs were invalid';
                    // Set error response comp attribute values
    			    helper.setErrorMsg(cmp, "ERROR", ERR_MSG_FORM_INPUT_ERROR, "Banner");
            	}
            }
        },

    	performSearchByLocationID: function(cmp, event, helper) {
            console.log('==== Controller.performSearchByLocationID =====');

            var ERR_MSG_FORM_INPUT_ERROR = cmp.get("v.attrFORM_INPUT_ERROR");
            var ERR_MSG_INVALID_INPUTS = 'Search By Location ID inputs were invalid';

            // Get values from inputs
            var locationID = cmp.get("v.attrLocationID");

            
            // Perform validation
            var validSearchRequest = helper.validateLocationIDSearchInput(cmp, locationID);

            // Set attribute values (if no error)
            if (validSearchRequest) {
                // Perform search
                helper.searchByLocationID(cmp, event, helper);
            } else {
            	// Raise error if found
            	console.log(ERR_MSG_INVALID_INPUTS);

                // Set error response comp attribute values
    			helper.setErrorMsg(cmp, "ERROR", ERR_MSG_FORM_INPUT_ERROR, "Banner");
            }
        },

        performSearchByAddress: function(cmp, event, helper) {
            console.log('==== Controller.performSearchByAddress =====');

            var ERR_MSG_FORM_INPUT_ERROR = cmp.get("v.attrFORM_INPUT_ERROR");
            var ERR_MSG_INVALID_INPUTS = 'Search By Address inputs were invalid';
            var ERR_MSG_INVALID_REQ_INPUTS = 'Search By Address required field inputs were invalid';

            // Get values from inputs
            var state = cmp.get("v.attrState");
            var postcode = cmp.get("v.attrPostcode");
            var suburbLocality = cmp.get("v.attrSuburbLocality");
            var streetName = cmp.get("v.attrStreetName");
            var streetType = cmp.get("v.attrStreetType");
            var streetLotNumber = cmp.get("v.attrStreetLotNumber");
            var unitType = cmp.get("v.attrUnitType");
            var unitNumber = cmp.get("v.attrUnitNumber");
            var level = cmp.get("v.attrLevel");
            var complexSiteName = cmp.get("v.attrComplexSiteName");
            var complexBuildingName = cmp.get("v.attrComplexBuildingName");
            var complexStreetName = cmp.get("v.attrComplexStreetName");
            var complexStreetType = cmp.get("v.attrComplexStreetType");
            var complexStreetNumber = cmp.get("v.attrComplexStreetNumber");

            // Perform validation

            // Validate if all required fields were entered
            var hasValidMandatoryFields = helper.validateAddressSearchInputMandatoryFields(cmp, streetLotNumber, streetName, suburbLocality, state, postcode);
            console.log('Controller.performSearchByAddress - hasValidMandatoryFields: ' + hasValidMandatoryFields);

            if (hasValidMandatoryFields) {
    	        // Validate all fields
    	        var validSearchRequest = helper.validateAddressSearchInput(cmp);

    	        // Set attribute values (if no error)
    	        if (validSearchRequest) {
    	            // Perform search
    	            helper.searchByAddress(cmp, event, helper);
    	        } else {
    	        	// Raise error if found
    	        	console.log(ERR_MSG_INVALID_INPUTS);

    	            // Set error response comp attribute values
    				helper.setErrorMsg(cmp, "ERROR", ERR_MSG_FORM_INPUT_ERROR, "Banner");
    	        }
            } else {
            	// Raise error if found
            	console.log(ERR_MSG_INVALID_REQ_INPUTS);

                // Set error response comp attribute values
    			helper.setErrorMsg(cmp, "ERROR", ERR_MSG_FORM_INPUT_ERROR, "Banner");
            }
        },

        performSearchByLatLong: function(cmp, event, helper) {
            console.log('==== Controller.performSearchByLatLong =====');

            var ERR_MSG_FORM_INPUT_ERROR = cmp.get("v.attrFORM_INPUT_ERROR");
            var ERR_MSG_INVALID_INPUTS = 'Search By Latitude/Longitude inputs were invalid';

            // Get values from inputs
            var latitude = cmp.get("v.attrLatitude");
            var longitude = cmp.get("v.attrLongitude");

            // Perform validation - validate that at least one of these inputs is populated
            var validSearchRequest = helper.validateLatLongSearchInput(cmp, latitude, longitude);

            // Set attribute values (if no error)
            if (validSearchRequest) {
                // Perform search
    			helper.searchByLatLong(cmp, event, helper);
            } else {
            	// Raise error if found
            	console.log(ERR_MSG_INVALID_INPUTS);

                // Set error response comp attribute values
    			helper.setErrorMsg(cmp, "ERROR", ERR_MSG_FORM_INPUT_ERROR, "Banner");
            }
        },

        handleSearchTypeChange: function (cmp, event, helper) {
            console.log('==== Controller.handleSearchTypeChange =====');

            var changeValue = event.getParam("value");

            if (changeValue == 'locationIDSearch') {
                cmp.set("v.showLocationIDSearchForm", true);
                cmp.set("v.showAddressSearchForm", false);
                cmp.set("v.showLatLongSearchForm", false);
            } else if (changeValue == 'addressSearch') {
                cmp.set("v.showLocationIDSearchForm", false);
                cmp.set("v.showAddressSearchForm", true);
                cmp.set("v.showLatLongSearchForm", false);
    		} else if (changeValue == 'latLongSearch') {
                cmp.set("v.showLocationIDSearchForm", false);
                cmp.set("v.showAddressSearchForm", false);
                cmp.set("v.showLatLongSearchForm", true);
            } else {
            }

    		// Clear errormsg
            helper.clearErrors(cmp);
        },

        clearLocationIDSearchForm: function(cmp, event, helper) {
            console.log('==== Controller.clearLocationIDSearchForm =====');

    		// Clear form fields
            //cmp.find("locationID").set("v.value", "");
            cmp.set("v.attrLocationID", "");

            // Clear errormsg
            helper.clearErrors(cmp);
        },

        clearAddressSearchForm: function(cmp, event, helper) {
            console.log('==== Controller.clearAddressSearchForm =====');

    		// Clear form fields
            cmp.set("v.attrState", "");
            cmp.set("v.attrPostcode", "");
            cmp.set("v.attrSuburbLocality", "");
            cmp.set("v.attrStreetName", "");
            cmp.set("v.attrStreetType", "");
            cmp.set("v.attrStreetLotNumber", "");
            cmp.set("v.attrUnitType", "");
            cmp.set("v.attrUnitNumber", "");
            cmp.set("v.attrLevel", "");
            cmp.set("v.attrComplexSiteName", "");
            cmp.set("v.attrComplexBuildingName", "");
            cmp.set("v.attrComplexStreetName", "");
            cmp.set("v.attrComplexStreetType", "");
            cmp.set("v.attrComplexStreetNumber", "");

            // Clear errormsg
            helper.clearErrors(cmp);
        },

        clearLatLongSearchForm: function(cmp, event, helper) {
    		// Clear form fields
        	cmp.set("v.attrLatitude", "");
        	cmp.set("v.attrLongitude", "");

            // Clear errormsg
            helper.clearErrors(cmp);
        },

        clearState: function(cmp, event, helper) {
    		// Clear State field on focus
        	cmp.set("v.attrState", "");

            // Clear validity.valid for state
            cmp.find("state").set('v.validity', {valid:true, valueMissing:false});
        },

        clearPostCode: function(cmp, event, helper) {
    		// Clear PostCode field on focus
        	cmp.set("v.attrPostcode", "");

            // Clear validity.valid for postcode
            cmp.find("postcode").set('v.validity', {valid:true, valueMissing:false});
        },
    });