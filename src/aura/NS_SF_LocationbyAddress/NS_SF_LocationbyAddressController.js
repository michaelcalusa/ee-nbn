/**
 * Created by Dheeraj.Mandavilli on 19/06/2018.
 */
    ({
        init: function (cmp, event, helper) {
        	console.log('==== Controller.init =====');

        	// Get custom label values
        	var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        	cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);

        	var ERR_MSG_FORM_INPUT_ERROR = $A.get("$Label.c.DF_Form_Input_Error");
        	cmp.set("v.attrFORM_INPUT_ERROR", ERR_MSG_FORM_INPUT_ERROR);
            
            cmp.fetchUnitType(cmp, event, helper);
            cmp.fetchStreetType(cmp, event, helper);

        },

        fetchUnitTypeValues: function(cmp,event,helper){

            var partsOfStr = [];

            var unitTypeNameValue;
            var unitTypeListMap = [];

            var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
            var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

            // Create the action
            var action = helper.getApexProxy(cmp, "c.retUnitTypeValues");

            // Add callback behavior for when response is received
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log('retUnitTypeValues - state: ' + state);
                
                if (state === "SUCCESS") {
                    unitTypeNameValue = response.getReturnValue();

                    for(var unitTypeIndex in unitTypeNameValue)
                    {
                        partsOfStr = unitTypeNameValue[unitTypeIndex].split(',');

                        unitTypeListMap.push({value:partsOfStr[1], key:partsOfStr[0]});

                    }

                    cmp.set("v.unitTypeMap",unitTypeListMap);

                } else {
                    console.log('retUnitTypeValues - Failed response with state: ' + state);
                    var ERR_MSG_APP_ERROR = cmp.get("v.attrAPP_ERROR");
                    try{
                        helper.setErrorMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "Banner");
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                            }
                        } else {
                            console.log(ERR_MSG_UNKNOWN_ERROR);
                        }
                    }
                    catch(err)
                    {
                        console.log('=Catch Exception=: '+err);
                    }
                }
            });

            $A.enqueueAction(action.delegate());
            console.log('==== retUnitTypeValues - enqueuedAction....');
    	},

        fetchStreetTypeValues: function(cmp, event, helper){

            var partsOfStr = [];
            
            var streetTypeNameValue;
            var streetTypeListMap = [];
            
            var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
            var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';
            
            // Create the action
            var action = helper.getApexProxy(cmp, "c.retStreetTypeValues");
            
            // Add callback behavior for when response is received
            action.setCallback(this, function(response) {
                var state = response.getState();
                console.log('retStreetTypeValues - state: ' + state);
                
                if (state === "SUCCESS") {
                    streetTypeNameValue = response.getReturnValue();
                    for(var streetTypeIndex in streetTypeNameValue)
                    {
                        partsOfStr = streetTypeNameValue[streetTypeIndex].split(',');
                        streetTypeListMap.push({value:partsOfStr[1], key:partsOfStr[0]});
                    }
                    cmp.set("v.streetTypeMap",streetTypeListMap);
                                        
                } else {
                    console.log('retUnitTypeValues - Failed response with state: ' + state);
                    var ERR_MSG_APP_ERROR = cmp.get("v.attrAPP_ERROR");
                    try{
                        helper.setErrorMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "Banner");
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                            }
                        } else {
                            console.log(ERR_MSG_UNKNOWN_ERROR);
                        }
                    }
                    catch(err)
                    {
                        console.log('retStreetTypeValues: '+err);
                    }
                }
            });
            
            // Send action off to be executed
            $A.enqueueAction(action.delegate());
            console.log('==== retStreetTypeValues - enqueuedAction....');
    	},


		performSearchByAddress: function(cmp, event, helper) {
            // Validate if all required fields were entered
            var hasValidMandatoryFields = helper.validateAddressSearchInputMandatoryFields(cmp, event, helper);
            console.log('Controller.performSearchByAddress - hasValidMandatoryFields: ' + hasValidMandatoryFields);

            if (hasValidMandatoryFields) {
                helper.clearErrors(cmp);
                helper.searchByAddress(cmp, event, helper);
            } else {
                var ERR_MSG_FORM_INPUT_ERROR = cmp.get("v.attrFORM_INPUT_ERROR");
                var ERR_MSG_INVALID_REQ_INPUTS = 'Search By Address required field inputs were invalid';

                console.log(ERR_MSG_INVALID_REQ_INPUTS);
                helper.setErrorMsg(cmp, "ERROR", ERR_MSG_FORM_INPUT_ERROR, "Banner");
            }
        },

        clearAddressSearchForm: function(cmp, event, helper) {
            console.log('==== Controller.clearAddressSearchForm =====');

    		// Clear form fields
            cmp.set("v.attrState", "");
            cmp.set("v.attrPostcode", "");
            cmp.set("v.attrSuburbLocality", "");
            cmp.set("v.attrStreetName", "");
            cmp.set("v.attrStreetType", "");
            cmp.set("v.attrStreetLotNumber", "");
            cmp.set("v.attrUnitType", "");
            cmp.set("v.attrUnitNumber", "");
            cmp.set("v.attrLevel", "");

            // Clear errormsg
            helper.clearErrors(cmp);
        },

        clearPostCode: function(cmp, event, helper) {
            if (!$A.util.isEmpty(cmp.get('v.attrState'))){
            	cmp.set("v.attrPostcode", "");
            	helper.setFieldError(cmp, 'postcode', '');
            }
        },

        validatePostCode:function(cmp, event, helper) {
            var postcode = cmp.get("v.attrPostcode");
            if ($A.util.isEmpty(postcode) && $A.util.isEmpty(cmp.get("v.attrState"))) {
                helper.setFieldError(cmp, 'postcode', 'State or Postcode field must be entered.');
            }
        },
        
        validateStreetName:function(cmp, event, helper) {
            var validVal4= cmp.get("v.attrStreetName ");
            validVal4 = !$A.util.isEmpty(validVal4) ? validVal4.replace(/[0-9]/g, '') : '';
            cmp.set("v.attrStreetName",validVal4);
        },

    });