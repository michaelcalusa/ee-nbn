/**
 * Created by Dheeraj.Mandavilli on 19/06/2018.
 */
    ({
       
        // Perform API call
    	searchByAddress : function(cmp, event, helper) {
            console.log('==== Helper - searchByAddress =====');

            var ERR_MSG_APP_ERROR = cmp.get("v.attrAPP_ERROR");
    		var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
    		var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

            // Get values from comp search attributes
            var state = cmp.get("v.attrState");
    		var postcode = cmp.get("v.attrPostcode");
    		var suburbLocality = cmp.get("v.attrSuburbLocality");
    		var streetName = cmp.get("v.attrStreetName");
    		var streetType = cmp.get("v.attrStreetType");
    		var streetLotNumber = cmp.get("v.attrStreetLotNumber");
    		var unitType = cmp.get("v.attrUnitType");
    		var unitNumber = cmp.get("v.attrUnitNumber");
            var level = cmp.get("v.attrLevel");

            // Create the action
    		var action = helper.getApexProxy(cmp, "c.searchByAddress");
            // Set apex controller action inputs
            action.setParams({
                "productType": cmp.get("v.productType").replace(/\s/g,"_"),
                'state': state,
                'postcode': postcode,
                'suburbLocality': suburbLocality,
                'streetName': streetName,
                'streetType': streetType,
                'streetLotNumber': streetLotNumber,
                'unitType': unitType,
                'unitNumber': unitNumber,
                'level': level
            });

            var responseOpptId;

            // Add callback behavior for when response is received
            action.setCallback(this, function(response) {
                var state = response.getState();
    			console.log('Helper.searchByAddress - state: ' + state);

                if (state === "SUCCESS") {
                    responseOpptId = response.getReturnValue();
                    console.log('Helper.searchByAddress - Successful response received - responseOpptId: ' + responseOpptId);
                    cmp.set("v.parentOppId", responseOpptId);
                    helper.goToNextPage(cmp, event);
                } else {
                    console.log('Helper.searchByAddress - Failed response with state: ' + state);
        			helper.setErrorMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "Banner");
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                        	console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                        }
                    } else {
                    	console.log(ERR_MSG_UNKNOWN_ERROR);
                    }
                }
            });
            $A.enqueueAction(action.delegate());
            console.log('==== Helper.searchByAddress - enqueuedAction....');
    	},

        goToNextPage: function(cmp, event) {
            console.log('==== Helper - goToNextPage =====');

            var opEvt = cmp.getEvent("oppBundleEvent");
            var OppId = cmp.get("v.parentOppId");
            console.log('OppId: '+OppId);
            opEvt.setParams({"oppBundleId" : OppId });
            opEvt.fire();
    	},

    	validateAddressSearchInputMandatoryFields : function(cmp, event, helper) {
            console.log('==== Helper - validateAddressSearchInputMandatoryFields =====');

            var isValid = true;
            var requiredFields = ['streetLotNumber', 'streetName', 'streetType', 'suburbLocality'];
            
            requiredFields.forEach(function(field){
            	isValid &= helper.showErrorRequiredFields(cmp, field);
            });
            
            var state = cmp.get('v.attrState');
            var postcode = cmp.get('v.attrPostcode');
            if ($A.util.isEmpty(state) && $A.util.isEmpty(postcode)) {
                isValid = false;
                this.setFieldError(cmp, 'postcode', 'State or Postcode field must be entered.');
            } else {
                this.setFieldError(cmp, 'postcode', '');
            }
            isValid &= this.showErrorRequiredFields(cmp, 'postcode');
            
            console.log('Helper.validateAddressSearchInputMandatoryFields - isValid: ' + isValid);
            return isValid;
    	},

        // Sets error msg properties
        setErrorMsg: function(cmp, errorStatus, errorMessage, errorType) {
            // Set error response comp attribute values
            cmp.set("v.errorResponseStatus", errorStatus);
            cmp.set("v.errorResponseMessage", errorMessage);
            cmp.set("v.errorResponseType", errorType);
        },

    	clearErrors : function(cmp) {
            // Set error response comp attribute values
            cmp.set("v.errorResponseStatus", "");
            cmp.set("v.errorResponseMessage", "");
            cmp.set("v.errorResponseType", "");
    	}
        
    })