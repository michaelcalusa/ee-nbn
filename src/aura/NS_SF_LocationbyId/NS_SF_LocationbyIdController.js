/**
 * Created by Dheeraj.Mandavilli on 19/06/2018.
 */
    ({
        init: function (cmp, event, helper) {
        	console.log('==== Controller.init =====');

        	// Get custom label values
        	var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        	cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);

        	var ERR_MSG_FORM_INPUT_ERROR = $A.get("$Label.c.DF_Form_Input_Error");
        	cmp.set("v.attrFORM_INPUT_ERROR", ERR_MSG_FORM_INPUT_ERROR);
            
            var baseUrl = window.location.href;
            console.log('-----window-----'+baseUrl);
           
            var splittedArray = baseUrl.includes('locid=') ? baseUrl.split('locid=') : [];
            console.log('--splittedArray-- ', splittedArray);
            
            var finalLocId = !$A.util.isEmpty(splittedArray) ? splittedArray[1] : '';
            console.log('--finalLocId---', finalLocId);
            
            cmp.set("v.attrLocationID", finalLocId);

            
            
        },

    	performSearchByLocationID: function(cmp, event, helper) {
            console.log('==== Controller.performSearchByLocationID =====');

            var validSearchRequest = helper.validateLocationIDSearchInput(cmp);

            if (validSearchRequest) {
                helper.clearErrors(cmp);
                helper.searchByLocationID(cmp, event, helper);
            } else {
            	console.log('Search By Location ID inputs were invalid');
    			helper.setErrorMsg(cmp, "ERROR", cmp.get("v.attrFORM_INPUT_ERROR"), "Banner");
            }
        },

        clearLocationIDSearchForm: function(cmp, event, helper) {
            console.log('==== Controller.clearLocationIDSearchForm =====');
            cmp.set("v.attrLocationID", "");
            helper.clearErrors(cmp);
        },

    });