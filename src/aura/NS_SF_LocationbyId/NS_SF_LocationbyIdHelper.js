/**
 * Created by Dheeraj.Mandavilli on 19/06/2018.
 */
    ({
        // Perform API call
    	searchByLocationID : function(cmp, event, helper) {
            console.log('==== Helper - searchByLocationID =====');

            var ERR_MSG_APP_ERROR = cmp.get("v.attrAPP_ERROR");
    		var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
    		var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

            // Get values from comp search attributes
            var locationID = cmp.get("v.attrLocationID");
    		var aProxy = helper.getApexProxy(cmp, "c.searchByLocationID");

            console.log('Product Type: '+ cmp.get("v.productType").replace(/\s/g,"_"));

            // Set apex controller action inputs
            aProxy.setParams({
                "productType": cmp.get("v.productType").replace(/\s/g,"_"),
                'locId': locationID
            });

            var responseOpptId;

            // Add callback behavior for when response is received
            aProxy.setCallback(this, function(response) {
                var state = response.getState();
                console.log('Helper.searchByLocationID - state: ' + state);

                if (state === "SUCCESS") {
                    responseOpptId = response.getReturnValue();

                    console.log('Helper.searchByLocationID - Successful response received - responseOpptId: ' + responseOpptId);
                    cmp.set("v.parentOppId", responseOpptId);
                    helper.goToNextPage(cmp, event);
                } else {
                    console.log('Helper.searchByLocationID - Failed response with state: ' + state);

        			helper.setErrorMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "Banner");
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                        	console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                        }
                    } else {
                    	console.log(ERR_MSG_UNKNOWN_ERROR);
                    }
                }
            });
            $A.enqueueAction(aProxy.delegate());
    	},

        goToNextPage: function(cmp, event) {
            console.log('==== Helper - goToNextPage =====');

            var opEvt = cmp.getEvent("oppBundleEvent");
            var OppId = cmp.get("v.parentOppId");
            console.log('OppId: '+OppId);
            opEvt.setParams({"oppBundleId" : OppId });
            opEvt.fire();
    	},

        validateLocationIDSearchInput : function(cmp, locationID) {
            console.log('==== Helper - validateLocationID =====');
            var inputCmp = cmp.find("locationID");
            inputCmp.reportValidity();
            return inputCmp.checkValidity();
    	},

        // Sets error msg properties
        setErrorMsg: function(cmp, errorStatus, errorMessage, errorType) {
            // Set error response comp attribute values
            cmp.set("v.errorResponseStatus", errorStatus);
            cmp.set("v.errorResponseMessage", errorMessage);
            cmp.set("v.errorResponseType", errorType);
        },

    	clearErrors : function(cmp) {
            // Set error response comp attribute values
            cmp.set("v.errorResponseStatus", "");
            cmp.set("v.errorResponseMessage", "");
            cmp.set("v.errorResponseType", "");
    	}
    })