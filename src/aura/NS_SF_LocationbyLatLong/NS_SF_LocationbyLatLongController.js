/**
 * Created by Dheeraj.Mandavilli on 19/06/2018.
 */
    ({
        init: function (cmp, event, helper) {
        	console.log('==== Controller.init =====');

        	// Get custom label values
        	var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        	cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);

        	var ERR_MSG_FORM_INPUT_ERROR = $A.get("$Label.c.DF_Form_Input_Error");
        	cmp.set("v.attrFORM_INPUT_ERROR", ERR_MSG_FORM_INPUT_ERROR);

        },


        performSearchByLatLong: function(cmp, event, helper) {
            console.log('==== Controller.performSearchByLatLong =====');

            var ERR_MSG_FORM_INPUT_ERROR = cmp.get("v.attrFORM_INPUT_ERROR");
            var ERR_MSG_INVALID_INPUTS = 'Search By Latitude/Longitude inputs were invalid';

            helper.clearErrors(cmp);

            // Get values from inputs
            var latitude = cmp.get("v.attrLatitude");
            var longitude = cmp.get("v.attrLongitude");

            // Perform validation - validate that at least one of these inputs is populated
            var validSearchRequest = helper.validateLatLongSearchInput(cmp, latitude, longitude);

            // Set attribute values (if no error)
            if (validSearchRequest) {
                // Perform search
    			helper.searchByLatLong(cmp, event, helper);
            } else {
            	// Raise error if found
            	console.log(ERR_MSG_INVALID_INPUTS);

                // Set error response comp attribute values
    			helper.setErrorMsg(cmp, "ERROR", ERR_MSG_FORM_INPUT_ERROR, "Banner");
            }
        },

        clearLatLongSearchForm: function(cmp, event, helper) {
    		// Clear form fields
        	cmp.set("v.attrLatitude", "");
        	cmp.set("v.attrLongitude", "");

            // Clear errormsg
            helper.clearErrors(cmp);
        },

    });