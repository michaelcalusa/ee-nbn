/**
 * Created by Dheeraj.Mandavilli on 19/06/2018.
 */
    ({
      
        // Perform API call
    	searchByLatLong : function(cmp, event, helper) {
            console.log('==== Helper - searchByLatLong =====');

            var ERR_MSG_APP_ERROR = cmp.get("v.attrAPP_ERROR");
    		var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
    		var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

            // Get values from comp search attributes
            var latitude = cmp.get("v.attrLatitude");
    		var longitude = cmp.get("v.attrLongitude");

    		console.log('Helper.searchByLatLong - latitude: ' + latitude);
    		console.log('Helper.searchByLatLong - longitude: ' + longitude);

            // Create the action
    		var action = helper.getApexProxy(cmp, "c.searchByLatLong");

            // Set apex controller action inputs
            action.setParams({
                'latitude': latitude,
                'longitude': longitude,
                 "productType": cmp.get("v.productType").replace(/\s/g,"_")
            });

            var responseOpptId;

            // Add callback behavior for when response is received
            action.setCallback(this, function(response) {
                var state = response.getState();
    			console.log('Helper.searchByLatLong - state: ' + state);

                if (state === "SUCCESS") {
                    responseOpptId = response.getReturnValue();
                    console.log('Helper.searchByLatLong - Successful response received - responseOpptId: ' + responseOpptId);

                    cmp.set("v.parentOppId", responseOpptId);

                    // Go to next page
                    helper.goToNextPage(cmp, event);
                } else {
                    console.log('Helper.searchByLatLong - Failed response with state: ' + state);

                    // Set error response comp with generic application error
        			helper.setErrorMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "Banner");
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                        	console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                        }
                    } else {
                    	console.log(ERR_MSG_UNKNOWN_ERROR);
                    }
                }
            });
            $A.enqueueAction(action.delegate());
    	},

        goToNextPage: function(cmp, event) {
            console.log('==== Helper - goToNextPage =====');

            var opEvt = cmp.getEvent("oppBundleEvent");
            var OppId = cmp.get("v.parentOppId");
            console.log('OppId: '+OppId);
            opEvt.setParams({"oppBundleId" : OppId });
            opEvt.fire();
    	},


    	validateLatLongSearchInput : function(cmp) {
    		var inputCmp =  cmp.find("latLong");
    		inputCmp.reportValidity();
            return inputCmp.checkValidity();
    	},

        // Sets error msg properties
        setErrorMsg: function(cmp, errorStatus, errorMessage, errorType) {
            // Set error response comp attribute values
            cmp.set("v.errorResponseStatus", errorStatus);
            cmp.set("v.errorResponseMessage", errorMessage);
            cmp.set("v.errorResponseType", errorType);
        },

    	clearErrors : function(cmp) {
            // Set error response comp attribute values
            cmp.set("v.errorResponseStatus", "");
            cmp.set("v.errorResponseMessage", "");
            cmp.set("v.errorResponseType", "");
    	}
    })