/**
 * Created by Gobind.Khurana on 18/05/2018.
 */
({
    init: function (cmp, event, helper) {
		// Get custom label values
    	var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
    	cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);

		var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
		var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';
        
        var opptyBundleId = cmp.get("v.parentOppId");
        console.log('opptyBundleId: ' + opptyBundleId);
        cmp.set("v.batchId",opptyBundleId);
        var batchId = cmp.get("v.batchId");
        if($A.util.isEmpty(batchId) || $A.util.isUndefined(batchId)) cmp.set('v.batchId', event.getParam("batchId"));
        console.log('batchId: ' + batchId);
        
        cmp.set("v.noRecords",true);
        console.log('v.noRecords',cmp.get("v.noRecords"));

        if ( !$A.util.isEmpty(opptyBundleId) ) {
            console.log('firing c.getOrderSummaryData');
            var action = helper.getApexProxy(cmp, "c.getOrderSummaryData");
            action.setParams({"opptyBundleId": opptyBundleId});
            action.setCallback(this, function (response) {
                var state = response.getState();

                if (state === "SUCCESS") {
                    // Get response string
                    var responseReturnValue = response.getReturnValue();

                    // Parse json string into js objects list
                    var orderSummaryDataObjList = JSON.parse(response.getReturnValue());
                    cmp.set('v.fullList', orderSummaryDataObjList);

                    //populate the parent opportunity bundle name
                    var parentOpptyName = cmp.get("v.fullList")[0].oppBundleName;
                    cmp.set('v.parentOppName', parentOpptyName);

                    var pageCountVal = cmp.get("v.selectedCount");

                    if (orderSummaryDataObjList != null) {
                        cmp.set("v.maxPage", Math.floor((orderSummaryDataObjList.length + (pageCountVal - 1)) / pageCountVal));
                        cmp.set('v.noRecords', false);
                        helper.renderPage(cmp);
                    }
                } else {
                    // Set error response comp with generic application error
                    helper.setErrorMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "Banner");
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                        }
                    } else { console.log(ERR_MSG_UNKNOWN_ERROR); }
                }

                //Check if url parameter exists

                var orderIdParam = helper.getUrlParameter(cmp, helper.ORDER_ID_PARAM_NAME);
                var parentOppIdParam = helper.getUrlParameter(cmp, helper.PARENT_OPP_ID_PARAM_NAME);

                if (orderIdParam != null && parentOppIdParam != null) {
                    console.log('in nested APEX call');
                    cmp.set('v.orderIdParam', orderIdParam);
                    cmp.set('v.parentOppIdParam', parentOppIdParam);

                    helper.apex(cmp,    "v.pageUrlSFOrderSummary",
                                        'c.getCustomSetting',
                                        {settingName: helper.CUSTOM_SETTING_PAGE_URL_SF}, false)
                        .then(function (result) {
                            var backUrl = cmp.get('v.pageUrlSFOrderSummary') +
                                '?' + helper.PARENT_OPP_ID_PARAM_NAME + '=' + parentOppIdParam + '&page=ordersummary'
                            cmp.set('v.backUrl', backUrl);
                            cmp.set('v.isOrderDetailsOpen', true);
                            console.log('FINISHED THE APEX BLOCK');
                        })
                        .finally(function (result) {
                            var evt = $A.get('e.c:NS_Spinner_Event');
                            evt.setParam('show', false);
                            console.log('FIRE THE SPINNER IN FINALLY BLOCK');
                            evt.fire();
                        });
                }

            });
            $A.enqueueAction(action.delegate());
        }
    },
    
    viewOrder: function(cmp, event, helper) {

		var orderSummaryEvt = cmp.getEvent("NS_orderSummaryEvent");  
        console.log('---invieworder', orderSummaryEvt);
        var selectedOrdr = event.target.getAttribute("data-orderId");
		console.log('---selectedOrdr', selectedOrdr); 

        orderSummaryEvt.setParams({
            "selectedOrder": selectedOrdr,
            "isFromQuotePage": "true",
            "batchId": cmp.get("v.parentOppId")
            
        });
        orderSummaryEvt.fire();
	},

	clear: function(cmp, event, helper) {
		cmp.set('v.currentList', cmp.get('v.fullList'));
		cmp.set('v.searchTerm', '');
        cmp.set('v.isTriggerdBySearch', false);
	},

	hideQuickQuoteDetails: function(cmp, event, helper) {
		cmp.set("v.isQuickQuoteDetailsOpen", false);
	},

	saveQuickQuoteDetails: function(cmp, event, helper) {

		var fullList = cmp.get('v.fullList');
		var selectedLocation = cmp.get("v.selectedLocation");
		var updatedList = fullList.map(function(item) {
				if(item.id == selectedLocation.id) return selectedLocation;
				else return item;
			});

		cmp.set('v.fullList', updatedList);
	},

	clickQuoteLink: function(cmp, event, helper) {
		var id = event.target.getAttribute("data-id");
		helper.showQuickQuoteDetails(cmp, event, id);
    },

    // function for save the Records
    save: function(cmp, event, helper) {

        var action = helper.getApexProxy(cmp, "c.save");
        action.setParams({
            "quoteList": cmp.get("v.currentList"),
            "oppty": cmp.get("v.oppId")
        });

        // set call back
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var sitesDetails = JSON.parse(response.getReturnValue());
                cmp.set("v.currentList", sitesDetails);
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                    console.log(ERR_MSG_UNKNOWN_ERROR);
                }
                console.log('Problem getting quote, response state: ' + state);
            }
        });
        $A.enqueueAction(action.delegate());
    },



    clickBackButton: function (cmp, event, helper) {
        if ( $A.util.isEmpty(cmp.get('v.parentOppName')) ) {
            helper.gotoDashboard(cmp, event, helper);
        } else {
            helper.gotoRAGPage(cmp, event, helper);
        }
    },

	clickOrderLink: function (cmp, event, helper) {
        console.log('==== clickOrderLink========');
		var id = event.target.getAttribute("data-id");
        console.log('id ' +id) //added
        helper.createNS_Order(cmp, event, helper, id);
        console.log('==== END  clickOrderLink========');
	},

	renderPage: function(cmp, event, helper) {
        helper.renderPage(cmp);
    },
    
    searchTermChange: function (cmp, event, helper) {
        //Do search if enter key is pressed.
        if (event.getParams().keyCode == 13) {
            cmp.set('v.isTriggerdBySearch', true);
            //cmp.set('v.showBackButton',false);
			helper.getOrderRecords(cmp, event, helper);
        } 
    },
    
    search: function (cmp, event, helper) {
        cmp.set('v.isTriggerdBySearch', true);
		helper.getOrderRecords(cmp, event, helper);
    },
    
    clickFeasibilityLink: function(cmp, event, helper) {
        console.log('inside ClickFeasiblityLink');
        var homeDtEvt = cmp.getEvent("homePageEvent");
        homeDtEvt.setParams({"requestType" : "feasibility" });
        homeDtEvt.fire();
    },

	updateColumnSorting: function (cmp, event, helper) {		
		helper.updateColumnSortingWithSubProperty(cmp, event, helper);
    },
})