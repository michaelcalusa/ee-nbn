/**
 * Created by Gobind.Khurana on 18/05/2018.
 */
({
    showQuickQuoteDetails: function(cmp, event, id) {
        
        var fullList = cmp.get('v.fullList');
        var results = fullList.filter(function(item) { return item.id === id; });
        cmp.set("v.selectedLocation", results[0]);
        cmp.set("v.isQuickQuoteDetailsOpen", true);
        
    },
    
    renderPage: function(cmp) {
        var records = cmp.get("v.fullList"),
            
        pageNumber = cmp.get("v.pageNumber"),
        pageCountVal = cmp.get("v.selectedCount"),
        pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));

        cmp.set('v.currentList', []);
        cmp.set("v.currentList", pageRecords);
    },
    
    setErrorMsg: function(cmp, errorStatus, errorMessage, errorType) {
        cmp.set("v.errorResponseStatus", errorStatus);
        cmp.set("v.errorResponseMessage", errorMessage);
        cmp.set("v.errorResponseType", errorType);
    },
    
    //create NS_Order__c object calls createNS_Order in controller
    createNS_Order: function (cmp, event, helper, id) {
        console.log('=======NS_SF_QuoteHelper==== createNS_Order START');
        var location = cmp.get("v.location");
        console.log('==var location=='+location);
        console.log('=======NS_SF_QuoteHelper==== createNS_Order quoteId' + id);

        var action = helper.getApexProxy(cmp, "c.createNS_Order");

        action.setParams({
            nsQuoteId: id
        });
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('=======NS_SF_QuoteHelper==== createNS_Order state ' + state);
            if (state === "SUCCESS") {
                var resp = response.getReturnValue();
                if (resp != "ERROR") {
                    
                    var fullList = cmp.get('v.fullList');
                    
                    var results = fullList.filter(function (item) {
                        return item.id === id;
                    });
                    cmp.set("v.selectedLocation", results[0]); //added
                    
                    
                    var orderDtEvt = cmp.getEvent("NS_orderDetailPageEvent");
                    //var orderDtEvt = cmp.getEvent("NSorderItemPageEvent");
                    //        console.log('==NS_orderDetailPageEvent=='+JSON.stringify(orderDtEvt));
                    console.log('TRY');
                    //console.log('clickOrderLink' + JSON.stringify(results[0]));//added
                    
                    results[0].orderId = resp;
                    //results[0].OpportunityId = resp;
                    cmp.set('v.location',results[0]);
                    console.log('=======NS_SF_QuoteHelper==== createNS_Order results ' + JSON.stringify(results[0]));
                    console.log('==v.Location=='+JSON.stringify(cmp.get("v.location")));
                    
                    
                    //    console.log('==orderDtEvt==='+JSON.stringify(orderDtEvt);
                    orderDtEvt.setParam('location', results[0]);
                    console.log('==orderDtEvt.getParam()==='+JSON.stringify(orderDtEvt.getParam("location")));
                    orderDtEvt.fire();
                    console.log('======orderDtEvt fired=====');
                    
                    
                }
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action.delegate());
    },
    
    getOrderRecords : function(cmp, event, helper) {
        var isTriggerBySearch = cmp.get('v.isTriggerdBySearch');
        var searchTermVal = cmp.get('v.searchTerm');
        
        console.log('searchTermVal',searchTermVal);
        console.log('isTriggerBySearch',isTriggerBySearch);

        var action = helper.getApexProxy(cmp, "c.getRecords");

        action.setParams({
                "searchTerm": searchTermVal,
                "isTriggerBySearch": Boolean(isTriggerBySearch) });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // Get response string
                var responseReturnValue = response.getReturnValue();
                var orderSummaryDataObjList = JSON.parse(response.getReturnValue());

                if ($A.util.isEmpty(orderSummaryDataObjList)){
                    console.log('orderSummaryDataObjList',JSON.stringify(orderSummaryDataObjList));
                    cmp.set("v.noRecords",true);
                    cmp.set('v.fullList', orderSummaryDataObjList);
                    cmp.set('v.parentOppName','');
                    helper.renderPage(cmp);
                } else {
                    cmp.set('v.fullList', orderSummaryDataObjList);
                	cmp.set('v.noRecords',false);
                    console.log('===v.fullList in NS_SF_QuoteController=='+JSON.stringify(response.getReturnValue()));
                	console.log('orderSummaryDataObjList',JSON.stringify(orderSummaryDataObjList));
                	//populate the parent opportunity bundle name
                    var parentOpptyName = cmp.get("v.fullList")[0].oppBundleName;
                    cmp.set('v.parentOppName',parentOpptyName);
                    console.log('Opp Bundle Name'+parentOpptyName);

                    //populate the parent opportunity bundle name
                    var batchId = cmp.get("v.fullList")[0].oppBundleId;
                    cmp.set('v.batchId',batchId);
                    cmp.set('v.parentOppId',batchId);
                    console.log('batch Id and parentOppId'+batchId);

                    var pageCountVal = cmp.get("v.selectedCount");
                    cmp.set("v.maxPage", Math.floor((orderSummaryDataObjList.length +
                        (pageCountVal - 1)) / pageCountVal));
                    helper.renderPage(cmp);
               	}
            } else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                    console.log(ERR_MSG_UNKNOWN_ERROR);
                }
                var errorLabel = $A.get("$Label.c.DF_Application_Error");
                cmp.set("v.responseStatus", state);
                cmp.set("v.messageType","Banner");
                cmp.set("v.responseMessage", errorLabel);
            }
        });
        $A.enqueueAction(action.delegate());
    }
})