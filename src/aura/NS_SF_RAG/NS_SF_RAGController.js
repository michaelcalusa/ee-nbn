/**
 * Created by Gobind.Khurana on 18/05/2018.
 */
({
	init: function (cmp, event, helper) {
        console.log('parentOppId: ' + cmp.get('v.parentOppId'));
		// Get custom label values
    	var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
    	cmp.set("v.attrAPP_ERROR", ERR_MSG_APP_ERROR);

    	var ERR_MSG_REC_SELECT_ERROR = $A.get("$Label.c.DF_Record_Selection_Error");
    	cmp.set("v.attrERR_MSG_REC_SELECT_ERROR", ERR_MSG_REC_SELECT_ERROR);

		var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
		var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

        var OppId = cmp.get("v.parentOppId");
        if ($A.util.isEmpty(OppId) == false) {
            // Add callback behavior for when response is received
            var action = helper.getApexProxy(cmp, "c.getRAGData");
            action.setParams({"oppId": cmp.get("v.parentOppId")});
            action.setCallback(this, function (response) {

                var state = response.getState();
                console.log('state: ' + state);
                if (state === "SUCCESS") {
                    // Get response string
                    var responseReturnValue = response.getReturnValue();

                    // Parse json string into js objects list
                    var sitesDetailsObjList = JSON.parse(response.getReturnValue());

                    cmp.set("v.fullList", sitesDetailsObjList);

                    //populate the parent opportunity bundle name
                    if ($A.util.isArray(sitesDetailsObjList) && sitesDetailsObjList.length > 0) {
                        var parentOpptyName = sitesDetailsObjList[0].oppBundleName;
                    	cmp.set("v.parentOppName", parentOpptyName);
                    }

                    // Populate the 3 different lists with all recs retrieved
                    helper.buildLists(cmp, event, helper);

                    // Paginate for Ready list
                    // Get full Ready list
                    var records = cmp.get("v.fullReadyList");
                    var pageCountVal = cmp.get("v.readySelectedCount");

                    if (records != null) {
                        cmp.set("v.readyListMaxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
                        helper.renderReadyPage(cmp);
                        cmp.set('v.isReadyListPageChanged', false);
                    }

                    // Paginate for Pending list
                    // Get full Pending list
                    var records = cmp.get("v.fullPendingList");
                    var pageCountVal = cmp.get("v.pendingSelectedCount");

                    if (records != null) {
                        cmp.set("v.pendingListMaxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
                        helper.renderPendingPage(cmp);
                        cmp.set('v.isPendingListPageChanged', false);
                    }

                    // Paginate for In Progress list
                    // Get full In Progress list
                    var records = cmp.get("v.fullInProgressList");
                    var pageCountVal = cmp.get("v.inProgressSelectedCount");

                    if (records != null) {
                        cmp.set("v.inProgressListMaxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
                        helper.renderInProgressPage(cmp);
                    }
                } else {
                    // Set error response comp with generic application error
                    helper.setErrorMsg(cmp, "ReadySection", "ERROR", ERR_MSG_APP_ERROR, "Banner");

                    var errors = response.getError();

                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                        }
                    } else {
                        console.log(ERR_MSG_UNKNOWN_ERROR);
                    }

                    // Disable buttons
                    helper.disableProceedToPricingButton(cmp, event, helper);
                    helper.disableProceedToDesktopAssessButton(cmp, event, helper);
                }
            });
            $A.enqueueAction(action.delegate());
        }
	},

    proceedToPricing: function(cmp, event, helper) {
		var ERR_MSG_REC_SELECT_ERROR = cmp.get("v.attrERR_MSG_REC_SELECT_ERROR");
		// Clear all errors for all lists
		helper.clearReadyListErrors(cmp);
		helper.clearPendingListErrors(cmp);

		// Get selected recs from comp attribute
		var selectedRows = cmp.get("v.selectedReadyRecords");
        console.log('Controller Selected List'+JSON.stringify(selectedRows));

		// Display error if no recs selected
		if (selectedRows.length > 0) {
			helper.proceedToPricing(cmp, event, helper);
		} else {
	        // Set error response comp attribute values
	        helper.setErrorMsg(cmp, "ReadySection", "ERROR", ERR_MSG_REC_SELECT_ERROR, "Banner");
		}
    },

    proceedToDesktopAssessment: function(cmp, event, helper) {
    	var ERR_MSG_REC_SELECT_ERROR = cmp.get("v.attrERR_MSG_REC_SELECT_ERROR");

		// Clear all errors for all lists
		helper.clearReadyListErrors(cmp, event, helper);
		helper.clearPendingListErrors(cmp, event, helper);

		// Get selected recs from comp attribute
		var selectedRows = cmp.get("v.selectedPendingRecords");

		// Display error if no recs selected
		if (selectedRows.length > 0) {
			// Update all selected DFQuote recs to approved
            cmp.set("v.toggleAssessmentPopup", true);
		} else {
	        // Set error response comp attribute values
	        helper.setErrorMsg(cmp, "PendingSection", "ERROR", ERR_MSG_REC_SELECT_ERROR, "Banner");
		}
    },

    closeAssessmentModal: function(cmp, event, helper) {
        console.log('closeAssessmentModal called' );
        cmp.set("v.toggleAssessmentPopup", false);
    },

    confirmAssessment: function(cmp, event, helper) {
        console.log('confirmAssessment called' );
        helper.updateDFQuotesToApprovedAndPublishEvent(cmp, event, helper);
        cmp.set("v.toggleAssessmentPopup", false);
        cmp.set("v.showConfirmationPopup", true);
    },

    closeConfirmAssessmentModal: function(cmp, event, helper) {
        console.log('closeConfirmAssessmentModal called' );
        cmp.set("v.showConfirmationPopup", false);
        
        var a = cmp.get('c.init');
        $A.enqueueAction(a);
    },

    getSelectedReadyRecords: function (cmp, event, helper) {
		// Clear errors
		helper.clearReadyListErrors(cmp, event, helper);
		helper.clearPendingListErrors(cmp, event, helper);

        var isReadyListPageChanged = cmp.get('v.isReadyListPageChanged');
        var selectedRows = event.getParam('selectedRows');

        if (!isReadyListPageChanged || selectedRows.length > 0) {
            var selectedList = cmp.get("v.selectedReadyRecords");
            var currentList = cmp.get("v.currentReadyList");
            var fullList = cmp.get("v.fullReadyList");

            helper.selectCheckBox(selectedRows, selectedList, currentList, fullList);
            cmp.set("v.selectedReadyRecords", selectedList);
        }
        cmp.set('v.isReadyListPageChanged', false);

        var selectedListAfter = cmp.get("v.selectedReadyRecords");
        helper.restoreCheckBox(cmp, selectedListAfter, "v.selectedReadyIds");
    },

    getSelectedPendingRecords: function (cmp, event, helper) {
		// Clear errors
		helper.clearPendingListErrors(cmp, event, helper);
		helper.clearReadyListErrors(cmp, event, helper);

        var isPendingListPageChanged = cmp.get('v.isPendingListPageChanged');
        var selectedRows = event.getParam('selectedRows');
        //console.log("selectedRows:"+JSON.stringify(selectedRows));

        if (!isPendingListPageChanged || selectedRows.length > 0) {
            var selectedList = cmp.get("v.selectedPendingRecords");
            var currentList = cmp.get("v.currentPendingList");
            var fullList = cmp.get("v.fullPendingList");

            helper.selectCheckBox(selectedRows, selectedList, currentList, fullList);
            cmp.set("v.selectedPendingRecords", selectedList);
        }
        cmp.set('v.isPendingListPageChanged', false);

        var selectedListAfter = cmp.get("v.selectedPendingRecords");
        helper.restoreCheckBox(cmp, selectedListAfter, "v.selectedPendingIds");
    },

    renderReadyPage: function(cmp, event, helper) {
        helper.renderReadyPage(cmp);
    },

    renderPendingPage: function(cmp, event, helper) {
        helper.renderPendingPage(cmp);
    },

    renderInProgressPage: function(cmp, event, helper) {
        helper.renderInProgressPage(cmp);
    },

    updateReadyListColumnSorting: function(cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');

        // Assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);

        helper.sortReadyListData(cmp, fieldName, sortDirection);
    },

    updatePendingListColumnSorting: function(cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');

        // Assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);

        helper.sortPendingListData(cmp, fieldName, sortDirection);
    },

    updateInProgressListColumnSorting: function(cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');

        // Assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);

        helper.sortInProgressListData(cmp, fieldName, sortDirection);
    },

})