/**
 * Created by Gobind.Khurana on 18/05/2018.
 */
({
	buildLists : function(cmp, event, helper) {
	    // remove 'Australia' from Address
		var sitesDetailsObjList = cmp.get("v.fullList");

        console.log('==sitesDetailsObjList=='+JSON.stringify(sitesDetailsObjList));

        for (var key = 0, size = sitesDetailsObjList.length; key < size; key++) {
            console.log('==sitesDetailsObjList[key].Address=='+JSON.stringify(sitesDetailsObjList[key].Address));
            sitesDetailsObjList[key].Address = sitesDetailsObjList[key].Address.replace(' Australia','');
            console.log('==sitesDetailsObjList[key].Address trimmed=='+JSON.stringify(sitesDetailsObjList[key].Address));
        }

        // Lists to populate comp attribute lists with
        var readyObjList = [];
        var pendingObjList = [];
        var inProgressObjList = [];

        // Loop thru sitesDetailsObjList
		sitesDetailsObjList.forEach(function(element) {
		    var status = element.SFStatus;
		    var rag = element.RAG;

		    if (status == 'Ready') {
		    	readyObjList.push(element);
		    } else if (status == 'Pending') {
	    		pendingObjList.push(element);
		    } else if (status == 'In Progress') {
	    		inProgressObjList.push(element);
		    }
		});

        console.log('helper.buildLists - readyObjList.length: ' + readyObjList.length);
        console.log('helper.buildLists - pendingObjList.length: ' + pendingObjList.length);
        console.log('helper.buildLists - inProgressObjList.length: ' + inProgressObjList.length);

        // Flush lists
        cmp.set("v.fullReadyList", null);
        cmp.set("v.fullPendingList", null);
        cmp.set("v.fullInProgressList", null);


        if (readyObjList.length > 0) {
        	cmp.set("v.fullReadyList", readyObjList);

        	// Check user has correct permissions - if not disable button
        	helper.hasValidPermissions(cmp, event, helper);
        } else {
        	cmp.set("v.fullReadyList", "");

			// If list is empty - disable button
			this.disableProceedToPricingButton(cmp, event);
        }

        if (pendingObjList.length > 0) {
        	cmp.set("v.fullPendingList", pendingObjList);
        } else {
        	console.log('helper.buildLists - pendingObjList: ' + pendingObjList);
        	cmp.set("v.fullPendingList", "");

        	// If list is empty - disable button
        	helper.disableProceedToDesktopAssessButton(cmp, event);
        }

        if (inProgressObjList.length > 0) {
        	cmp.set("v.fullInProgressList", inProgressObjList);
        } else {
        	cmp.set("v.fullInProgressList", "");
        }
	},

	proceedToPricing : function(cmp, event, helper) {
		var ERR_MSG_APP_ERROR = cmp.get("v.attrAPP_ERROR");
		var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
		var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

		// Get selected recs from comp attribute
		var selectedRows = cmp.get("v.selectedReadyRecords");
        var dfQuoteIdList = [];

        // Build the DFQuoteId array
        for (var i = 0; i < selectedRows.length; i++) {
            dfQuoteIdList.push(selectedRows[i].SQId);
        }

        // Create the action
		var action = helper.getApexProxy(cmp, "c.processProceedToPricing");

        // Set input list with js array/object list
        action.setParams({"dfQuoteIdList" : dfQuoteIdList});


        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('state: ' + state);

            if (state === "SUCCESS") {
                var quoteDtEvt = cmp.getEvent("quoteDetailPageEvent");
                quoteDtEvt.fire();
            } else {
                // Set error response comp with generic application error
                helper.setErrorMsg(cmp, "ReadySection", "ERROR", ERR_MSG_APP_ERROR, "Banner");

                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                    	console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                	console.log(ERR_MSG_UNKNOWN_ERROR);
                }
            }
        });
        $A.enqueueAction(action.delegate());
	},

	updateDFQuotesToApprovedAndPublishEvent : function(cmp, event, helper) {
		var ERR_MSG_APP_ERROR = cmp.get("v.attrAPP_ERROR");
		var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
		var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

		// Get selected recs from comp attribute
		var selectedRows = cmp.get("v.selectedPendingRecords");

        var dfQuoteIdList = [];
		console.log('--selectedRows--@updateDFQuotesToApprovedAndPublishEvent--', selectedRows);
        // Build the DFQuoteId array
        for (var i = 0; i < selectedRows.length; i++) {
            dfQuoteIdList.push(selectedRows[i].SQId);
        }

		console.log('helper.updateDFQuotesToApproved - dfQuoteIdList.length: ' + dfQuoteIdList.length);

        // Create the action
		var action = helper.getApexProxy(cmp, "c.updateDFQuotesToApprovedAndPublishEvent");

        // Set input list with js array/object list
        action.setParams({"dfQuoteIdList": dfQuoteIdList});

        // Add callback behavior for when response is received
        action.setCallback(this,function(response) {

            var state = response.getState();
            console.log('helper.updateDFQuotesToApproved - state: ' + state);

            if (state === "SUCCESS") {
            	// Get response string
            	var responseReturnValue = response.getReturnValue();

            } else {
                // Set error response comp with generic application error
                helper.setErrorMsg(cmp, "PendingSection", "ERROR", ERR_MSG_APP_ERROR, "Banner");

                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                    	console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                	console.log(ERR_MSG_UNKNOWN_ERROR);
                }
            }
        });

        // Send action off to be executed
        $A.enqueueAction(action.delegate());
	},

    clearReadyListErrors : function(cmp, event) {
        // Set error response comp attribute values
        cmp.set("v.errorResponseStatusReady", "");
        cmp.set("v.errorResponseMessageReady", "");
        cmp.set("v.errorResponseTypeReady", "");
	},

	clearPendingListErrors : function(cmp, event) {
        // Set error response comp attribute values
        cmp.set("v.errorResponseStatusPending", "");
        cmp.set("v.errorResponseMessagePending", "");
        cmp.set("v.errorResponseTypePending", "");
	},


    hasRagItem: function(ragList, ragItem) {
        for(var i=0; i < ragList.length; i++) {
            if (ragItem.SQId === ragList[i].SQId) {
                return true;
            }
        }
        return false;
    },

    addRagItemIfNotExist: function(ragList, ragItem) {
        if (!this.hasRagItem(ragList, ragItem)) {
            ragList.push(ragItem);
            return 1;
        }
        return 0;
    },

    addRagItemsIfNotExist: function(ragList, ragItems) {
        var addedCount = 0;
        for(var i=0; i<ragItems.length; i++) {
            addedCount += this.addRagItemIfNotExist(ragList, ragItems[i]);
        }
        return addedCount;
    },

    removeRagItems: function(ragList, ragItems) {
        var removedCount = 0;
        for(var i = ragList.length - 1; i >= 0; i--) {
            if (this.hasRagItem(ragItems, ragList[i])) {
                removedCount++;
                ragList.splice(i, 1);
            }
        }
        return removedCount;
    },

    restoreCheckBox: function(cmp, selectedList, bindingAttribute) {
        var selectedRowsIds = [];
        for(var i=0; i<selectedList.length; i++) {
            selectedRowsIds.push(selectedList[i].SQId);
        }
        cmp.set(bindingAttribute, selectedRowsIds);
    },

    selectCheckBox: function(selectedRows, selectedList, currentList, fullList) {
        var selectedCountBefore = selectedList.length;
        var pageSize = currentList.length;

        // deselect all rows in this page
        this.removeRagItems(selectedList, currentList);

        // add selected rows
        var addedCount = this.addRagItemsIfNotExist(selectedList, selectedRows);

        var selectedCountAfter = selectedList.length;
        var changedCount = Math.abs(selectedCountBefore - selectedCountAfter);

        if(pageSize === changedCount || addedCount === pageSize || addedCount === 0) {
            if (addedCount > 0) {
                // select all action
                this.addRagItemsIfNotExist(selectedList, fullList);
            } else {
                // deselect all action
                selectedList.splice(0, selectedList.length);
            }
        }
    },

    // Sets the records that have to be displayed while user clicks through the pagination bar
    renderReadyPage: function(cmp) {
        var records = cmp.get("v.fullReadyList"),
            pageNumber = cmp.get("v.readyListPageNumber"),
            pageCountVal = cmp.get("v.readySelectedCount"),
            pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));

        cmp.set('v.isReadyListPageChanged', true);
        cmp.set('v.currentReadyList', []);
        cmp.set("v.currentReadyList", pageRecords);

        var selectedList = cmp.get("v.selectedReadyRecords");
        this.restoreCheckBox(cmp, selectedList, "v.selectedReadyIds");
    },

    // Sets the records that have to be displayed while user clicks through the pagination bar
    renderPendingPage: function(cmp) {
        var records = cmp.get("v.fullPendingList"),
            pageNumber = cmp.get("v.pendingListPageNumber"),
            pageCountVal = cmp.get("v.pendingSelectedCount"),
            pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));

        cmp.set('v.isPendingListPageChanged', true);
        cmp.set('v.currentPendingList', []);
        cmp.set("v.currentPendingList", pageRecords);

        var selectedList = cmp.get("v.selectedPendingRecords");
        this.restoreCheckBox(cmp, selectedList, "v.selectedPendingIds");
    },

    // Sets the records that have to be displayed while user clicks through the pagination bar
    renderInProgressPage: function(cmp) {
        var records = cmp.get("v.fullInProgressList"),
            pageNumber = cmp.get("v.inProgressListPageNumber"),
            pageCountVal = cmp.get("v.inProgressSelectedCount"),
            pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));

		cmp.set("v.currentInProgressList", []);
        cmp.set("v.currentInProgressList", pageRecords);
    },

    // Sets error msg properties
    setErrorMsg: function(cmp, errorLocation, errorStatus, errorMessage, errorType) {
        // Set error response comp attribute values
        if (errorLocation == 'ReadySection') {
	        cmp.set("v.errorResponseStatusReady", errorStatus);
	        cmp.set("v.errorResponseMessageReady", errorMessage);
	        cmp.set("v.errorResponseTypeReady", errorType);

        } else if (errorLocation == 'PendingSection') {
	        cmp.set("v.errorResponseStatusPending", errorStatus);
	        cmp.set("v.errorResponseMessagePending", errorMessage);
	        cmp.set("v.errorResponseTypePending", errorType);
        }
    },

    sortReadyListData: function(cmp, fieldName, sortDirection) {
        var data = cmp.get("v.currentReadyList");
        var reverse = sortDirection !== 'asc';

        // Sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.currentReadyList", data);
    },

    sortPendingListData: function(cmp, fieldName, sortDirection) {
        var data = cmp.get("v.currentPendingList");
        var reverse = sortDirection !== 'asc';

        // Sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.currentPendingList", data);
    },

    sortInProgressListData: function(cmp, fieldName, sortDirection) {
        var data = cmp.get("v.currentInProgressList");
        var reverse = sortDirection !== 'asc';

        // Sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.currentInProgressList", data);
    },

    sortBy: function(field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};

        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },

	hasValidPermissions : function(cmp, event, helper) {
		var ERR_MSG_APP_ERROR = cmp.get("v.attrAPP_ERROR");
		var ERR_MSG_ERROR_OCCURRED = 'Error occurred. Error message: ';
		var ERR_MSG_UNKNOWN_ERROR = 'Error message: Unknown error';

        // Create the action
		var action = helper.getApexProxy(cmp, "c.hasValidPermissions");

        // Add callback behavior for when response is received
        action.setCallback(this, function(response) {

            var state = response.getState();
            console.log('helper.hasValidPermissions - state: ' + state);

            if (state === "SUCCESS") {
            	// Get response string
            	var responseReturnValue = response.getReturnValue();

            	if (responseReturnValue != true) {
            		// Disable button
            		cmp.set("v.disableProceedToPricingBtnFlg", true);
            		helper.disableProceedToPricingButton(cmp, event);
            	} else {
            		// Enable button
					cmp.set("v.disableProceedToPricingBtnFlg", false);
					helper.enableProceedToPricingButton(cmp, event);
            	}
            } else {
                // Set error response comp with generic application error
                helper.setErrorMsg(cmp, "ReadySection", "ERROR", ERR_MSG_APP_ERROR, "Banner");

                var errors = response.getError();

                if (errors) {
                    if (errors[0] && errors[0].message) {
                    	console.log(ERR_MSG_ERROR_OCCURRED + errors[0].message);
                    }
                } else {
                	console.log(ERR_MSG_UNKNOWN_ERROR);
                }
            }
        });
        $A.enqueueAction(action.delegate());
	},

    enableProceedToPricingButton: function(cmp, event) {
		cmp.set("v.disableProceedToPricingBtnFlg", false);
    },

    disableProceedToPricingButton: function(cmp, event) {
		cmp.set("v.disableProceedToPricingBtnFlg", true);
    },

    enableProceedToDesktopAssessButton: function(cmp, event) {
		cmp.set("v.disableProceedToDesktopAssessmentBtnFlg", false);
    },

    disableProceedToDesktopAssessButton: function(cmp, event) {
		cmp.set("v.disableProceedToDesktopAssessmentBtnFlg", true);
    },
})