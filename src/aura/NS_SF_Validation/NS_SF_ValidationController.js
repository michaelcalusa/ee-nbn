/**
 * Created by Gobind.Khurana on 18/05/2018.
 */
    ({
        init: function (cmp, event, helper) {
            helper.fetchSiteData(cmp, event, helper, false);
        },

        refreshSiteData: function (cmp, event, helper) {
            helper.fetchSiteData(cmp, event, helper, false);
        },

        handleRowAction: function(cmp, event, helper) {
            var action = event.getParam('action');
            if (action.name === 'editRowItem') {
                var rowData = event.getParam('row');
                //console.log('row: ' + JSON.stringify(rowData));
                helper.openModalWithData(cmp, helper, rowData);
            }
        },

        closeModal: function(cmp, event, helper) {
            helper.closeModalWindow(cmp, event, helper, false);
        },

        handleSearchTypeChange: function(cmp, event, helper) {
            helper.clearModalMsg(cmp);
            helper.showModalSearchForm(cmp);
        },

        onMultipleAddressSelect: function(cmp, event, helper) {
            var unstructuredAddress = cmp.get("v.unstructuredAddress");
            var structuredAddressList = cmp.get("v.structuredAddressList");
            var foundMatchingStructuredAddress = false;

            for (var i = 0; i < structuredAddressList.length; i++) {
                var structuredAddress = structuredAddressList[i];
                //console.log("found structuredAddress: " + JSON.stringify(structuredAddress));
                if(unstructuredAddress == structuredAddress.unstructured){
                    helper.populateAddressFormFromStructuredAddress(cmp, helper, structuredAddress);
                    foundMatchingStructuredAddress = true;
                    break;
                }
            }

            if(!foundMatchingStructuredAddress){
                helper.clearAddressTerms(cmp, helper);
            }
        },

        performEdit: function(cmp, event, helper) {
            var isValid = helper.validateSearchForm(cmp, helper);
            //console.log("performSearch:" + cmp.get("v.searchTypeValue") + ", form validity:" + isValid);
            if (isValid) {
                helper.doModalEdit(cmp, event, helper);
            }
        },

        handleCount: function(cmp, event, helper) {
            //This gets triggered every time the user changes the no of records displayed per page
            cmp.set("v.pageNumber", 1);
            var records = cmp.get("v.sitedata");
            cmp.set("v.currentList", records);
            var pageCountVal = cmp.get("v.selectedCount");
            cmp.set("v.maxPage", Math.floor((records.length+(pageCountVal-1))/pageCountVal));
            helper.renderPage(cmp);
            var childCmp = component.find("cComp");
            childCmp.resetValues();
        },

        renderPage: function(cmp, event, helper) {
            helper.renderPage(cmp);
        },

        goto: function(cmp, event, helper) {
            var homeDtEvt = cmp.getEvent("homePageEvent");
            homeDtEvt.setParams({"requestType" : cmp.get('v.sourceRequestType') });
            homeDtEvt.fire();
        },

        submitFeasibilityRequest: function(cmp, event, helper) {

            var errorLabel = $A.get("$Label.c.DF_Application_Error");
            //Make a call to the apex method to submit a service feasibility request
            var action = helper.getApexProxy(cmp, "c.processSiteData");
            var OppId = cmp.get("v.parentOppId");

            action.setParams({
                "oppBundleId": cmp.get("v.parentOppId"),
                "productType": cmp.get("v.productType").replace(/\s/g, "_")
            });

            action.setCallback(this,function(response){

                var state = response.getState();
                if(state === "SUCCESS"){
                    console.log("fire siteDetailPageEvent");
                    // put the spinner up to avoid double submit
                    var siteDtEvt = cmp.getEvent("siteDetailPageEvent");
                    siteDtEvt.fire();
                }
                else if(state === "ERROR") {
                    cmp.set("v.responseStatus",state);
                    cmp.set("v.type","Banner");
                    cmp.set("v.responseMessage",errorLabel);
                }
            });
            $A.enqueueAction(action.delegate());
        },

        updateSortOrder : function (cmp, event, helper) {
            helper.updateColumnSorting(cmp, event, helper, 'v.sitedata');
        }

    })