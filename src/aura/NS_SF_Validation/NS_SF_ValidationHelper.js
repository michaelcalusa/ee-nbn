/**
 * Created by Gobind.Khurana on 18/05/2018.
 */
({
    //helper method that sets the records that has to be displayed while user clicks through the pagination bar
    renderPage: function(component) {
        var records = component.get("v.sitedata"),
            pageNumber = component.get("v.pageNumber"),
            pageCountVal = component.get("v.selectedCount"),
            pageRecords = records.slice(((pageNumber-1)*pageCountVal), (pageNumber*pageCountVal));
        component.set("v.currentList", pageRecords);
    },

    removeAustraliaAddress: function(component){
       try{
	    // remove 'Australia' from Address
		var sitesDetailsObjList = component.get("v.fullList");

		for (var key = 0, size = sitesDetailsObjList.length; key < size; key++) {
            sitesDetailsObjList[key].address = sitesDetailsObjList[key].address.replace(' Australia','');
        }
        component.set("v.fullList",sitesDetailsObjList);
       }
       catch(err)
       {
           console.log('=Error in NS_SF_ValidationHelper.js='+err);
       }
    },

    fetchSiteData: function(cmp, event, helper, showMsg) {
        var errorLabel = $A.get("$Label.c.DF_Application_Error");

        var OppId = cmp.get("v.parentOppId");
        var sitesDetails;
        var action = helper.getApexProxy(cmp, "c.getSiteData");
        action.setParams({"oppBundleId": cmp.get("v.parentOppId") });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS") {
                sitesDetails = JSON.parse(response.getReturnValue());
                if (sitesDetails.length > 0) {
                    cmp.set("v.sitedata", sitesDetails);
                    var flag = cmp.get("v.sitedata")[0].addCount;
                    var parentOpptyName = cmp.get("v.sitedata")[0].oppBundleName;
                    cmp.set("v.parentOppName", parentOpptyName);

                    if (flag == false) {
                        cmp.set("v.serviceFeasibilityFlag", true);
                    }
                    else if (flag == true) {
                        cmp.set("v.serviceFeasibilityFlag", false);
                    }

                    cmp.set("v.responseStatus", state);
                    cmp.set("v.responseMessage", '');
                } else {
                    cmp.set("v.responseStatus","ERROR");
                    cmp.set("v.type","Banner");
                    cmp.set("v.responseMessage","No locations found");
                }

                //Obtain the pagecount for pagination
                var records = cmp.get("v.sitedata");
                var pageCountVal = cmp.get("v.selectedCount");
                cmp.set("v.maxPage", Math.floor((records.length+(pageCountVal-1))/pageCountVal));

                if (!$A.util.isEmpty(cmp.get("v.sortedBy"))) {
                    // retain the sorting state after refresh the page
                    helper.updateColumnSorting(cmp, event, helper, 'v.sitedata');
                }
                helper.renderPage(cmp);

                if (showMsg) {
                    helper.showSuccessfulMessageFor5Seconds(cmp, helper);
                }
            }
            else if(state === "ERROR") {
                cmp.set("v.responseStatus",state);
                cmp.set("v.type","Banner");
                cmp.set("v.responseMessage",errorLabel);
            }
            cmp.set("v.refreshParent", false);
        });
        $A.enqueueAction(action.delegate());
    },

    setModalMsg: function(cmp, errorStatus, errorMessage, errorType) {
        cmp.find("modalScroller").scrollTo('top');
        cmp.set("v.responseStatusModal", errorStatus);
        cmp.set("v.responseMessageModal", errorMessage);
        cmp.set("v.typeModal", errorType);
    },

    clearModalMsg: function(cmp) {
        cmp.set("v.responseStatusModal", "");
        cmp.set("v.responseMessageModal", "");
        cmp.set("v.typeModal", "");
    },

    openModalWithData: function(cmp, helper, rowData) {
        cmp.set("v.isModalOpen", true);
        cmp.set("v.currentSfRequestId", rowData.requestId);
        helper.clearModalMsg(cmp);
        helper.clearSearchTerms(cmp, helper);
        helper.clearMultipleAddressLists(cmp);
        var hasMultipleAddresses = (rowData.status === 'Invalid - Multiple Addresses');
        cmp.set('v.hasMultipleAddresses', hasMultipleAddresses);
        var searchTypeFromApex = rowData.searchType;

        if (searchTypeFromApex === 'SearchByAddress') {
            // call apex to fetch original address info
            helper.apex(cmp, 'v.addressResult', 'c.getSfRequestIncludingResponseAddress', { "requestId": rowData.requestId }, false)
                .then(function(result) {
                    var addressResult = JSON.parse(result.data);
                    if(!addressResult.failure) {
                        if(hasMultipleAddresses){
                            //console.log("detected multiple addresses");
                            helper.extractMultipleAddresses(cmp, helper, addressResult);
                        }
                        helper.setAddressInfo(cmp, helper, addressResult);
                    } else {
                        helper.setLocIdInfo(cmp, helper, rowData);
                    }
                }).catch(function(result) {
                    helper.setLocIdInfo(cmp, helper, rowData);
                });
        } else if (searchTypeFromApex === 'SearchByLatLong') {
            helper.setLatLongInfo(cmp, helper, rowData);
        } else {
            // default to search by location id
            helper.setLocIdInfo(cmp, helper, rowData);
        }

        if('SearchByAddress' !== searchTypeFromApex && hasMultipleAddresses) {
            //console.log("detected multiple addresses");
            helper.apex(cmp, 'v.addressResult', 'c.getSfRequestIncludingResponseAddress', {"requestId": rowData.requestId}, false)
                .then(function (result) {
                    var multipleAddressJsonResult = JSON.parse(result.data);
                    if (!multipleAddressJsonResult.failure) {
                        helper.extractMultipleAddresses(cmp, helper, multipleAddressJsonResult);
                    } else {
                        console.log('error fetching multiple address data');
                    }
                }).catch(function (result) {
                    console.log('error fetching multiple address data');
                });
        }

    },

    extractMultipleAddresses: function(cmp, helper, multipleAddressJsonResult){
        //console.log("extracting multiple addresses");
        var responseJson = JSON.parse(multipleAddressJsonResult.Response__c);

        var unstructuredAddressList = [];
        unstructuredAddressList.push("Select");
        for(var i=0; i < responseJson.AddressList.length; i++){
            //console.log('found ' + responseJson.AddressList[i]);
            unstructuredAddressList.push(responseJson.AddressList[i]);
        }
        cmp.set('v.unstructuredAddressList', unstructuredAddressList);
        //console.log('unstructuredAddressList size is ' + cmp.get('v.unstructuredAddressList').length);
        cmp.set('v.unstructuredAddress', "Select");

        var structuredAddressList = [];
        for(var i=0; i < responseJson.structuredAddressList.length; i++){
            //console.log(' found structured address obj ' + JSON.stringify(responseJson.structuredAddressList[i]));
            structuredAddressList.push(responseJson.structuredAddressList[i]);
        }
        cmp.set('v.structuredAddressList', structuredAddressList);
    },

    setLocIdInfo: function(cmp, helper, rowData) {
        cmp.set("v.searchTypeValue", 'locationIDSearch');
        cmp.set("v.attrLocationID", helper.defaultIfEmpty(rowData.locId,''));
        cmp.set("v.originalSearch", cmp.get("v.attrLocationID"));
        //console.log("set locationId, originalSearch is " + cmp.get("v.originalSearch"));
        helper.showModalSearchForm(cmp);
    },

    setLatLongInfo: function(cmp, helper, rowData) {
        cmp.set("v.searchTypeValue", 'latLongSearch');
        cmp.set("v.attrLatitude", helper.defaultIfEmpty(rowData.latitude,''));
        cmp.set("v.attrLongitude", helper.defaultIfEmpty(rowData.longitude,''));
        cmp.set("v.originalSearch", "latitude: " + cmp.get("v.attrLatitude") + ", longitude: " + cmp.get("v.attrLongitude"));
        //console.log("set lat and long, originalSearch is " + cmp.get("v.originalSearch"));
        helper.showModalSearchForm(cmp);
    },

    setAddressInfo: function(cmp, helper, addressResult) {
        cmp.set("v.searchTypeValue", 'addressSearch');

        cmp.set("v.attrState", helper.defaultIfEmpty(addressResult.State__c,''));
        cmp.set("v.attrPostcode", helper.defaultIfEmpty(addressResult.Postcode__c,''));
        cmp.set("v.attrSuburbLocality", helper.defaultIfEmpty(addressResult.Suburb_Locality__c,''));
        cmp.set("v.attrStreetName", helper.defaultIfEmpty(addressResult.Street_Name__c,''));
        cmp.set("v.attrStreetType", helper.defaultIfEmpty(addressResult.Street_Type__c,''));
        cmp.set("v.attrStreetLotNumber", helper.defaultIfEmpty(addressResult.Street_or_Lot_Number__c,''));
        cmp.set("v.attrUnitType", helper.defaultIfEmpty(addressResult.Unit_Type__c,''));
        cmp.set("v.attrUnitNumber", helper.defaultIfEmpty(addressResult.Unit_Number__c,''));
        cmp.set("v.attrLevel", helper.defaultIfEmpty(addressResult.Level__c,''));
        cmp.set("v.attrComplexSiteName", helper.defaultIfEmpty(addressResult.Complex_or_Site_Name__c,''));
        cmp.set("v.attrComplexBuildingName", helper.defaultIfEmpty(addressResult.Building_Name_in_a_Complex__c,''));
        cmp.set("v.attrComplexStreetName", helper.defaultIfEmpty(addressResult.Street_Name_in_a_Complex__c,''));
        cmp.set("v.attrComplexStreetType", helper.defaultIfEmpty(addressResult.Street_Type_in_a_Complex__c,''));
        cmp.set("v.attrComplexStreetNumber", helper.defaultIfEmpty(addressResult.Street_Number_in_a_Complex__c,''));
        cmp.set("v.originalSearch", cmp.get("v.attrUnitType") + " " +
            cmp.get("v.attrUnitNumber") + " " +
            cmp.get("v.attrStreetLotNumber") + " " +
            cmp.get("v.attrStreetName") + " " +
            cmp.get("v.attrStreetType") + " " +
            cmp.get("v.attrSuburbLocality") + " " +
            cmp.get("v.attrState") + " " +
            cmp.get("v.attrPostcode"));

        //console.log("set address, originalSearch is " + cmp.get("v.originalSearch"));
        helper.showModalSearchForm(cmp);
    },

    populateAddressFormFromStructuredAddress: function(cmp, helper, structuredAddress) {
        cmp.set("v.attrState", helper.defaultIfEmpty(structuredAddress.state,''));
        cmp.set("v.attrPostcode", helper.defaultIfEmpty(structuredAddress.postcode,''));
        cmp.set("v.attrSuburbLocality", helper.defaultIfEmpty(structuredAddress.suburbLocality,''));
        cmp.set("v.attrStreetName", helper.defaultIfEmpty(structuredAddress.streetName,''));
        cmp.set("v.attrStreetType", helper.defaultIfEmpty(structuredAddress.streetType,''));
        var complexRoadNumber = helper.buildComplexRoadNumber(helper.defaultIfEmpty(structuredAddress.streetLotNumber1, ''), helper.defaultIfEmpty(structuredAddress.streetLotNumber2, ''));
        cmp.set("v.attrStreetLotNumber", complexRoadNumber);
        cmp.set("v.attrUnitType", helper.defaultIfEmpty(structuredAddress.unitType,''));
        cmp.set("v.attrUnitNumber", helper.defaultIfEmpty(structuredAddress.unitNumber,''));
        cmp.set("v.attrLevel", helper.defaultIfEmpty(structuredAddress.levelNumber, '')); //(helper.defaultIfEmpty(structuredAddress.levelNumber, '') + ' ' + helper.defaultIfEmpty(structuredAddress.levelType, '')).trim());

        cmp.set("v.attrComplexSiteName", helper.defaultIfEmpty(structuredAddress.siteName,''));
        cmp.set("v.attrComplexBuildingName", helper.defaultIfEmpty(structuredAddress.complexSiteName,''));
        cmp.set("v.attrComplexStreetName", helper.defaultIfEmpty(structuredAddress.complexRoadName,''));
        cmp.set("v.attrComplexStreetType", helper.defaultIfEmpty(structuredAddress.complexRoadTypeCode, ''));//+ " " + helper.defaultIfEmpty(structuredAddress.complexRoadSuffixCode, '')
        complexRoadNumber = helper.buildComplexRoadNumber(helper.defaultIfEmpty(structuredAddress.complexRoadNumber1, ''), helper.defaultIfEmpty(structuredAddress.complexRoadNumber2, ''));
        cmp.set("v.attrComplexStreetNumber", complexRoadNumber);

        cmp.set("v.searchTypeValue", 'addressSearch');
        helper.showModalSearchForm(cmp);
    },

    buildComplexRoadNumber: function(number1, number2) {
        var complexRoadNumber = "";
        if(number1.length > 0){
            complexRoadNumber += number1;
        }
        if(number2.length > 0){
            if(complexRoadNumber.length > 0){
                complexRoadNumber += "-";
            }
            complexRoadNumber += number2;
        }
        return complexRoadNumber;
    },

    doModalEdit: function(cmp, event, helper) {
        this.clearModalMsg(cmp);

        var ERR_MSG_APP_ERROR = $A.get("$Label.c.DF_Application_Error");
        var self = this;

        var searchType = cmp.get("v.searchTypeValue");
        var sfRequestId = cmp.get("v.currentSfRequestId");
        console.log("searchType:"+searchType+", sfRequestId="+sfRequestId);
        if (searchType === 'addressSearch') {
            // Get values from comp search attributes
            var state = cmp.get("v.attrState");
            var postcode = cmp.get("v.attrPostcode");
            var suburbLocality = cmp.get("v.attrSuburbLocality");
            var streetName = cmp.get("v.attrStreetName");
            var streetType = cmp.get("v.attrStreetType");
            var streetLotNumber = cmp.get("v.attrStreetLotNumber");
            var unitType = cmp.get("v.attrUnitType");
            var unitNumber = cmp.get("v.attrUnitNumber");
            var level = cmp.get("v.attrLevel");
            var complexSiteName = cmp.get("v.attrComplexSiteName");
            var complexBuildingName = cmp.get("v.attrComplexBuildingName");
            var complexStreetName = cmp.get("v.attrComplexStreetName");
            var complexStreetType = cmp.get("v.attrComplexStreetType");
            var complexStreetNumber = cmp.get("v.attrComplexStreetNumber");

            // call apex
            self.apex(cmp, 'v.addressResult', 'c.editByAddress',
                {'sfRequestId': sfRequestId,
                    'state': state,
                    'postcode': postcode,
                    'suburbLocality': suburbLocality,
                    'streetName': streetName,
                    'streetType': streetType,
                    'streetLotNumber': streetLotNumber,
                    'unitType': unitType,
                    'unitNumber': unitNumber,
                    'level': level,
                    'complexSiteName': complexSiteName,
                    'complexBuildingName': complexBuildingName,
                    'complexStreetName': complexStreetName,
                    'complexStreetType': complexStreetType,
                    'complexStreetNumber': complexStreetNumber }, false)
                .then(function(result) {
                    self.handleNewSiteData(cmp, result, event, self);
                }).catch(function(result) {
                	self.setModalMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "Banner");
            	}).finally(function(){
            	    helper.fireHideSpinnerEvent(cmp);
                });
        } else if (searchType === 'latLongSearch') {
            var latitude = cmp.get("v.attrLatitude");
            var longitude = cmp.get("v.attrLongitude");
            // call apex
            self.apex(cmp, 'v.addressResult', 'c.editByLatLong', { "sfRequestId": sfRequestId, "latitude": latitude, "longitude": longitude }, false)
                .then(function(result) {
                    self.handleNewSiteData(cmp, result, event, self);
                }).catch(function(result) {
                	self.setModalMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "Banner");
            	}).finally(function(){
                    helper.fireHideSpinnerEvent(cmp);
                });;
        } else {
            // default to search by location id
            var locId = cmp.get("v.attrLocationID");
            // call apex
            self.apex(cmp, 'v.addressResult', 'c.editByLocId', { "sfRequestId": sfRequestId, "locationId": locId }, false)
                .then(function(result) {
                    self.handleNewSiteData(cmp, result, event, self);
                }).catch(function(result) {
                    self.setModalMsg(cmp, "ERROR", ERR_MSG_APP_ERROR, "Banner");
                }).finally(function(){
                    helper.fireHideSpinnerEvent(cmp);
                });;
        }
    },

    handleNewSiteData: function(cmp, result, event, helper) {
        cmp.set("v.refreshParent", true);
        var siteData = JSON.parse(result.data);
        if(!siteData.failure) {
            var duplicate = helper.checkDuplicateAndUpdateResultList(cmp, siteData);
            if (siteData.status === 'Valid') {
                helper.closeModalWindow(cmp, event, helper, !duplicate);
                return;
            }
        }
        helper.setModalMsg(cmp, "ERROR", "Invalid, please update to a correct one", "Banner");
    },

    clearSearchTerms: function(cmp, helper) {
        cmp.set("v.attrLocationID", '');
        cmp.set("v.attrLatitude", '');
        cmp.set("v.attrLongitude", '');
        helper.clearAddressTerms(cmp, helper);
    },

    clearAddressTerms: function(cmp, helper) {
        cmp.set("v.attrState", '');
        cmp.set("v.attrPostcode", '');
        cmp.set("v.attrSuburbLocality", '');
        cmp.set("v.attrStreetName", '');
        cmp.set("v.attrStreetType", '');
        cmp.set("v.attrStreetLotNumber", '');
        cmp.set("v.attrUnitType", '');
        cmp.set("v.attrUnitNumber", '');
        cmp.set("v.attrLevel", '');
        cmp.set("v.attrComplexSiteName", '');
        cmp.set("v.attrComplexBuildingName", '');
        cmp.set("v.attrComplexStreetName", '');
        cmp.set("v.attrComplexStreetType", '');
        cmp.set("v.attrComplexStreetNumber", '');
    },

    clearMultipleAddressLists: function(cmp) {
        cmp.set('v.hasMultipleAddresses', false);
        cmp.set('v.unstructuredAddress', "Select");
        cmp.set('v.unstructuredAddressList', []);
        cmp.set('v.structuredAddressList', []);
    },

    closeModalWindow: function(cmp, event, helper, showMsg) {
        if (cmp.get("v.refreshParent")) {
            helper.fetchSiteData(cmp, event, helper, showMsg);
        }
        cmp.set("v.isModalOpen", false);
    },

    showSuccessfulMessageFor5Seconds: function(cmp, helper) {
        helper.setBannerMsg(cmp, "OK", "Record updated successfully");
        window.setTimeout($A.getCallback(function() {
            helper.clearErrors(cmp);
        }), 5000);
    },

    checkDuplicateAndUpdateResultList: function(cmp, newRecord) {
        //check duplicate in full list
        var isDuplicate = false;
        var fullList = cmp.get("v.sitedata");
        for (var i = 0; i < fullList.length; i++) {
            var oldRecord = fullList[i];
            if (oldRecord.requestId !== newRecord.requestId && oldRecord.locId === newRecord.locId){
                isDuplicate = true;
                break;
            }
        }

        var currentList = cmp.get("v.currentList");

        for (var i = 0; i < currentList.length; i++) {
            var oldRecord = currentList[i];
            if (oldRecord.requestId === newRecord.requestId){

                if(isDuplicate) {
                    oldRecord.status = 'Duplicate';
                } else {
                    oldRecord.status = newRecord.status;
                }

                oldRecord.searchType = newRecord.searchType;
                oldRecord.longitude = newRecord.longitude;
                oldRecord.locId = newRecord.locId;
                oldRecord.latitude = newRecord.latitude;
                oldRecord.address = newRecord.address;
                oldRecord.addCount = newRecord.addCount;
                break;
            }
        }

        cmp.set("v.currentList", currentList);

        return isDuplicate;
    },

    validateSearchForm: function(cmp, helper) {
        var searchType = cmp.get("v.searchTypeValue");
        if (searchType === 'addressSearch') {
            var allValid = true;
            var postcodeInput = cmp.find("postcode");
            if ($A.util.isEmpty(cmp.get("v.attrState")) && $A.util.isEmpty(cmp.get("v.attrPostcode"))) {
                allValid = false;
                postcodeInput.setCustomValidity('State or Postcode field must be entered.');
                postcodeInput.reportValidity();
                postcodeInput.focus();
            } else {
                postcodeInput.setCustomValidity('');
                allValid = helper.validateSingle(cmp, "postcode");
            }
            return allValid & helper.isAllFieldsValid(cmp, "addressDetails");
        } else if (searchType === 'latLongSearch') {
            return helper.isAllFieldsValid(cmp, "coordinate");
        } else {
            // default to search by location id
            return helper.validateSingle(cmp, "locationID");
        }
    },

    validateSingle: function(cmp, cmpName) {
        var inputCmp = cmp.find(cmpName);
        inputCmp.showHelpMessageIfInvalid();
        var isValid = inputCmp.checkValidity();
        if (!isValid) {
            inputCmp.focus();
        }
        return isValid;
    },

    showModalSearchForm: function(cmp) {
        var searchType = cmp.get("v.searchTypeValue");
        if (searchType === 'addressSearch') {
            cmp.set("v.showLocationIDSearchForm", false);
            cmp.set("v.showAddressSearchForm", true);
            cmp.set("v.showLatLongSearchForm", false);
        } else if (searchType === 'latLongSearch') {
            cmp.set("v.showLocationIDSearchForm", false);
            cmp.set("v.showAddressSearchForm", false);
            cmp.set("v.showLatLongSearchForm", true);
        } else {
            // default to search by location id
            cmp.set("v.showLocationIDSearchForm", true);
            cmp.set("v.showAddressSearchForm", false);
            cmp.set("v.showLatLongSearchForm", false);
        }
    },

    defaultIfEmpty : function(str, defaultString) {
        return (!str || 0 === str.length) ? defaultString : str;
    }

})