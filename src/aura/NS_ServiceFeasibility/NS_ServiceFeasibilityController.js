/**
 * Created by Gobind.Khurana on 21/05/2018.
 */
({
	updateOppBundleId : function(cmp, event, helper) {
        var opptBundleId = event.getParam("oppBundleId");
        console.log('opptBundleId: '+opptBundleId);
        cmp.set("v.batchId", opptBundleId);
        cmp.set("v.lstSitePage", true);
        cmp.set("v.sfPage", false);
        cmp.set("v.ragPage", false);
		cmp.set("v.quotePage", false);
		cmp.set("v.orderPage", false);
    },

    goToRAGPage :function(cmp, event, helper) {
        var opptBundleId = event.getParam("oppBundleId");
        console.log('ragPage opptBundleId : ' + opptBundleId);
        if(opptBundleId!=null)
            cmp.set("v.batchId", opptBundleId);

        cmp.set("v.lstSitePage", false);
        cmp.set("v.sfPage", false);
        cmp.set("v.ragPage", true);
		cmp.set("v.quotePage", false);
		cmp.set("v.orderPage", false);
    },

	goToQuotePage :function(cmp, event, helper) {
        cmp.set("v.lstSitePage", false);
        cmp.set("v.sfPage", false);
        cmp.set("v.ragPage", false);
		cmp.set("v.quotePage", true);
		cmp.set("v.orderPage", false);
    },

    goToOrderPage :function(cmp, event, helper) {
		var location = event.getParam("location");
		console.log('==location in NS_ServiceFeasibility.js=='+location);
        cmp.set("v.location", location);
        cmp.set("v.lstSitePage", false);
        cmp.set("v.sfPage", false);
        cmp.set("v.ragPage", false);
		cmp.set("v.quotePage", false);
		cmp.set("v.orderPage", true);
    },
    
    handleOrderEvent: function (cmp, event, helper) {
        console.log('--@handleOrderEvent--');
        
        
        console.log('--batchId---', event.getParam("batchId")); 
       
        
        var selOrder = event.getParam("selectedOrder");
		var isFromQuotePage = event.getParam("isFromQuotePage");
        console.log('--selOrder.Id--', selOrder.Id);
        cmp.set('v.selectedOrder', selOrder.Id);
		cmp.set('v.isFromQuotePage', isFromQuotePage);		
        if(isFromQuotePage){
            cmp.set('v.batchId', event.getParam("batchId"));
        }
            		

		//var fullList = cmp.get('v.fullList');
        //console.log(fullList);
        //var orderId = event.target.getAttribute("data-Id");
        //var selectedOrder = fullList.filter(function(item){ return item.Id == orderId});		
        cmp.set("v.isOrderDetailsOpen", true);
        cmp.set("v.loadServiceFeasibility", false);
        cmp.set("v.loadOrders", false);
        cmp.set("v.loadTickets", false);
        cmp.set("v.loadHome", false);
        cmp.set("v.batchId", null);  
        cmp.set("v.lstSitePage", false);  
        cmp.set("v.sfPage", false);  
        cmp.set("v.ragPage", false); 
        cmp.set("v.quotePage", false);    
        cmp.set("v.orderPage", false);
        event.stopPropagation();
    },

    navigateToTestDashboard: function(cmp, event, helper)
    {
        var eUrl= $A.get("e.force:navigateToURL");

            eUrl.setParams({

              "url": '/ns2'

            });

            eUrl.fire();
    }
})