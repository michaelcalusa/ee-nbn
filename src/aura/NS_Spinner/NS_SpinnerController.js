/**
 * Created by philipstafford-jones on 20/3/19.
 */
({

    handleSpinnerEvent: function(cmp, event, helper) {

        var debug = event.getParam("debug");
        if (debug) {
            console.log("spinner debug: " + debug);
        }

        var show = event.getParam('show');
        if (show == true || show == undefined) {
            helper.showSpinner(cmp, event, helper);
        } else {
            helper.hideSpinner(cmp, event, helper);
        }
    },

})