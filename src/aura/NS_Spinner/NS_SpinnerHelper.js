/**
 * Created by philipstafford-jones on 22/3/19.
 */
({
    timerHandle: 0,

    startTime: 0,

    showSpinner: function (cmp, event, helper) {
        var requests = cmp.get('v.requests') + 1;
        // if it is the first show Spinner request then set a timeout to force close (backstop)
        if (requests <= 1) {
            var d = new Date();
            this.startTime = d.getTime();

            var timer = window.setTimeout(
            $A.getCallback(function () {
                if (cmp.get('v.value') != false) {
                    console.log('NS_Spinner: FORCED CLOSE SPINNER ON 30 second TIMEOUT');
                }
                cmp.set('v.value', false);
                cmp.set('v.requests', 0);
            }), 25000);
            this.timerHandle = timer;
        }
        // set requests and raise the spinner
        cmp.set('v.requests', requests);
        cmp.set('v.value', true);
    },

    hideSpinner: function (cmp, event, helper) {
        var requests = cmp.get('v.requests') - 1;
        cmp.set('v.requests', requests);

        // if the requests is zero then close the spinner and clear the timer
        // otherwise just decrement the requests by one
        if (requests <= 0) {
            cmp.set('v.value', false);
            window.clearTimeout(this.timerHandle);
            if (requests < 0) {
                console.log('NS_Spinner warning: requests = ' + requests);
                cmp.set('v.requests', 0);
            }
            var d = new Date();
            var duration = (d.getTime() - this.startTime);
            console.log("NS_Spinner: Total spinner duration: " + duration);
        }
    },
})