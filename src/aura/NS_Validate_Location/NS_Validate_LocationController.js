/**
 * Created by Gobind.Khurana on 17/05/2018.
 */
({
    	performSearchByLocationID: function(cmp, event, helper) {
            console.log('==== Controller.performSearchByLocationID =====');

            var ERR_MSG_FORM_INPUT_ERROR = cmp.get("v.attrFORM_INPUT_ERROR");
            var ERR_MSG_INVALID_INPUTS = 'Search By Location ID inputs were invalid';

            // Get values from inputs
            var locationID = cmp.get("v.attrLocationID");
            console.log('locationID: '+locationID);

            // Perform validation
            var validSearchRequest = helper.validateLocationIDSearchInput(cmp, locationID);

            // Set attribute values (if no error)
            if (validSearchRequest) {
                // Perform search
                helper.searchByLocationID(cmp, event);
            } else {
            	// Raise error if found
            	console.log(ERR_MSG_INVALID_INPUTS);

                // Set error response comp attribute values
    			helper.setErrorMsg(cmp, "ERROR", ERR_MSG_FORM_INPUT_ERROR, "Banner");
            }
        },
})