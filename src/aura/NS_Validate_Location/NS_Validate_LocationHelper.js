/**
 * Created by Gobind.Khurana on 17/05/2018.
 */
({
       validateLocationIDSearchInput : function(cmp, locationID) {
            console.log('==== Helper - validateLocationID =====');
    		var isValid = true;
            var LOC_PREFIX = 'LOC';
            var ERR_MSG_BADINPUT = 'Enter a valid value. '
            var ERR_MSG_LOC_PREFIX_MISSING = ERR_MSG_BADINPUT + 'Location ID must be in the following format e.g. LOC123456789012';
            var inputStrLength = locationID.length;

            if (inputStrLength != 15) {
    			isValid = false;
            } else {
            	var UPPERCASE_LOCATION_ID = locationID.toUpperCase();

                if (!UPPERCASE_LOCATION_ID.startsWith(LOC_PREFIX, 0)) {
                    // LOC prefix not found
                    isValid = false;
                    cmp.find("locationID").set('v.validity', {valid:false, badInput:true});

                    // Set field control error msg
                    cmp.find("locationID").set('v.messageWhenBadInput', ERR_MSG_LOC_PREFIX_MISSING);
                } else {
                    // Get last 12 chars of LOCID
                    var locDigits = locationID.substr(3, 12);
                    console.log('==== Helper.validateLocationIDSearchInput - locDigits: ' + locDigits);

                    // Validate if last 12 chars of LOCID are numeric
                    if (isNaN(locDigits)) {
                        // Some non numerics found in last 12 chars of LOCID
                        isValid = false;
                        cmp.find("locationID").set('v.validity', {valid:false, badInput:true});

                        // Set field control error msg
                        cmp.find("locationID").set('v.messageWhenBadInput', ERR_MSG_LOC_PREFIX_MISSING);
                    }
                }
            }
      console.log('Helper.validateLocationIDSearchInput - isValid: ' + isValid);

        return isValid;
	},

})