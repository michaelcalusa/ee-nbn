({
    initialize: function(component, event, helper) {
      	$A.get("e.siteforce:registerQueryEventMap").setParams({"qsToEvent" : helper.qsToEventMap}).fire(); 
        $A.get("e.siteforce:registerQueryEventMap").setParams({"qsToEvent" : helper.qsToEventMap2}).fire();
        
        component.set('v.isUsernamePasswordEnabled', helper.getIsUsernamePasswordEnabled(component, event, helper));
		helper.getURLParametersHelper(component, event, helper);
        component.set("v.isSelfRegistrationEnabled", helper.getIsSelfRegistrationEnabled(component, event, helper));
        component.set("v.communityForgotPasswordUrl", helper.getCommunityForgotPasswordUrl(component, event, helper));
        component.set("v.communitySelfRegisterUrl", helper.getCommunitySelfRegisterUrl(component, event, helper));
        component.set('v.errors', helper.getDefaultErrors());
    },
    
    waiting: function(component, event, helper) {
    	component.set("v.HideSpinner", true);
    },
        
    validateLogin: function (component, event, helper) { 
        console.log('Inside - validate');
        var errors = Object.assign({}, component.get('v.errors')); 
        errors.usernameLabel = helper.validateEmail(component.get('v.usernameLabel'));
        errors.passwordLabel2 = helper.validatePassword(component.get('v.passwordLabel2'));
        component.set('v.errors', errors);
        component.set('v.loginContext', true);
        if (errors.usernameLabel.length == 0 && errors.passwordLabel2.length == 0){
            console.log('calling login');
            helper.handleLogin(component, event, helper);
        }
    },
    validateRegister: function (component, event, helper) { 
        console.log('Inside - validate');
        var errors = Object.assign({}, component.get('v.errors')); 
        errors.emailLabel = helper.validateEmail(component.get('v.emailLabel'));
        errors.otpLabel = helper.validateRegistrationCode(component.get('v.otpLabel'));
        errors.passwordLabel = helper.validatePassword(component.get('v.passwordLabel'));
        errors.confirmPasswordLabel = helper.validateConfirmPassword(component.get('v.passwordLabel'), component.get('v.confirmPasswordLabel'));
        errors.termsCondLabel = helper.validateTermsBox(component.get('v.termsCondLabel'));
        component.set('v.errors', errors); 
        console.log('End - validate');
         if (errors.emailLabel.length == 0 && errors.otpLabel.length == 0 && errors.passwordLabel.length == 0 && errors.confirmPasswordLabel.length == 0 && errors.termsCondLabel.length == 0){
            console.log('calling register');
            helper.handleSelfRegister(component, event, helper);
        }
    },
    
    validateField: function (component, event, helper) {
        var fieldName = helper.findFieldNameFromDomEvent(event.getParam('domEvent'));
        var errors = Object.assign({}, component.get('v.errors'));      

        if (fieldName === 'usernameLabel') {
            errors.usernameLabel = helper.validateEmail(component.get('v.usernameLabel'));
        }
        if (fieldName === 'passwordLabel2') {
            errors.passwordLabel2 = helper.validatePassword(component.get('v.passwordLabel2'));
        }
        if (fieldName === 'emailLabel') {
            errors.emailLabel = helper.validateEmail(component.get('v.emailLabel'));
        }
        if (fieldName === 'otpLabel') {
            errors.otpLabel = helper.validateRegistrationCode(component.get('v.otpLabel'));
        }
        if (fieldName === 'passwordLabel') {console.log(component.get('v.passwordLabel'));
            errors.passwordLabel = helper.validatePassword(component.get('v.passwordLabel'));
        }
        if (fieldName === 'confirmPasswordLabel') {
            errors.confirmPasswordLabel = helper.validateConfirmPassword(component.get('v.passwordLabel'), component.get('v.confirmPasswordLabel'));
        }
        if (fieldName === 'termsCondLabel') {
            errors.termsCondLabel = helper.validateTermsBox(component.get('v.termsCondLabel'));
        }
        component.set('v.errors', errors);
    },
    
	doneWaiting: function(component, event, helper) {
    	component.set("v.HideSpinner", false);
    },
    
    setStartUrl: function (component, event, helpler) {
        var startUrl = event.getParam('startURL');
        if(startUrl) {
            component.set("v.startUrl", startUrl);
        }
    },
    
    setExpId: function (component, event, helper) {
        var expId = event.getParam('expid');
        if (expId) {
            component.set("v.expid", expId);
        }
        helper.setBrandingCookie(component, event, helper);
    },
    
    onKeyUp: function(component, event, helpler){
        //checks for "enter" key
        if (event.getParam('keyCode')===13) {
            helpler.handleLogin(component, event, helpler);
        }
    },
    
    navigateToForgotPassword: function(cmp, event, helper) {
        var forgotPwdUrl = cmp.get("v.communityForgotPasswordUrl");
        if ($A.util.isUndefinedOrNull(forgotPwdUrl)) {
            forgotPwdUrl = cmp.get("v.forgotPasswordUrl");
        }
        var attributes = { url: forgotPwdUrl };
        $A.get("e.force:navigateToURL").setParams(attributes).fire();
    },
    
    navigateToSelfRegister: function(cmp, event, helper) {
        var selrRegUrl = cmp.get("v.communitySelfRegisterUrl");
        if (selrRegUrl == null) {
            selrRegUrl = cmp.get("v.selfRegisterUrl");
        }
    
        var attributes = { url: selrRegUrl };
        $A.get("e.force:navigateToURL").setParams(attributes).fire();
    } 
})