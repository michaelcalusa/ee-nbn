({
    qsToEventMap: {
        'startURL'  : 'e.c:NewDevSetStartURL'
    },
    
    findFieldNameFromDomEvent: function (domEvent) {
        // for IE, find fieldName in parent
        if (domEvent.target.parentElement && domEvent.target.parentElement.dataset.fieldName) {
            return domEvent.target.parentElement.dataset.fieldName;
        }
        return domEvent.target.dataset.fieldName;
    },
    
    getDefaultErrors: function () {
        return {
            usernameLabel: [],
            passwordLabel2: [],
            emailLabel: [],
            otpLabel: [],
            passwordLabel: [],
            confirmpasswordLabel: [],
            termsCondLabel: []
        };
    },
    
    validateEmail: function (value) {
        var errors = [];
        if (!value) {
            errors.push('Invalid email address');
            return errors;
        }
        if (!value.match(/[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?/)) {
            errors.push('Invalid email address');
        }
        return errors;
    },
            
    validateRegistrationCode: function (value) {
        var errors = [];
        if (!value) {
            errors.push('Invalid Code');
            return errors;
        }
        if (value.length < 5) {
            errors.push('Invalid Code.');
        }
        return errors;
    },
        
    validatePassword: function (value) {
        var errors = [];
        var plengthmsg ='Password must be at least '+$A.get("$Label.c.New_Dev_Community_Password_Length")+' characters long';
        if (!value) {
            errors.push('Invalid Password');
            return errors;
        }
        if (value.length < $A.get("$Label.c.New_Dev_Community_Password_Length")){
            errors.push(plengthmsg);
        }
        return errors;
    },
    
    validateConfirmPassword: function (value1, value2) {
        var errors = [];
        if (!value1) {
            errors.push('Invalid Password');
            return errors;
        }
        if (value1 !== value2) {
            errors.push('Pasword do not match');
        }
        return errors;
    },
    
	validateTermsBox: function (value) {
        var errors = [];
        if (!value) {
            errors.push('Terms and Conditions must be agreed to.');
            return errors;
        }
        return errors;
    },
        
    getURLParametersHelper: function(component, event, helpler) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParamCode = [];
        var sParamId = [];
        var i;
        for (i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('='); //to split the key from the value.
            if (sParameterName[0] === 'portalcode') { 
                sParamCode[0] = 'portalcode';
                if (sParameterName[1] === undefined){ 
                    sParamCode[1] = 'Not found';
                }else{
                    sParamCode[1] = sParameterName[1];
                }
            }
            if (sParameterName[0] === 'did'){
                sParamId[0] = 'did';
                if (sParameterName[1] === undefined){ 
                    sParamId[1] = 'Not found';
                }else{
                    sParamId[1] = sParameterName[1];
                }
            }
        }
        console.log('sParamCode1=='+sParamCode[1]);
        if(sParamCode[1] !='Not found'){
            component.set("v.contactID", sParamCode[1]);
            component.set("v.did", sParamId[1]);
            // Verify if user already exist
            var action = component.get("c.checkCommunityUser");
            action.setParams({
                strContactID:sParamCode[1]
            });
            action.setCallback(this, function(a){
            	var response = a.getReturnValue();               
                component.set("v.retrieveUserInformation",response);
                if(response.isUserExist == true){
                    console.log('****Contact Exists with the Portal Account****');
                    component.set("v.loginContext", true);
                    component.set("v.registerContext", false);
                    component.set("v.isUsernamePasswordEnabled", true);
                }else if(response.isUserExist == false && response.strErrorMessage == $A.get("$Label.c.NewDev_Portal_Register_Error")){
                    //case where users exists with same email and differnt name
                    console.log('****Contact Exsits with No Portal Account, No unique Email****' + response.strErrorMessage + '---' + response.isUserExist);
                    component.set("v.loginContext", true);
                    component.set("v.registerContext", true);
                    component.set("v.usernameLabel",response.strUserEmail);
                    component.set("v.errorMessage", response.strErrorMessage);
                    component.set("v.showError",true);
                }else if(response.isUserExist == false && response.strErrorMessage == ''){
                    //all good case, new user
                    component.set("v.loginContext", false);
                    component.set("v.registerContext", true);
                    console.log('****Contact exists with No Portal Account, Unique Email****' + response.strErrorMessage + '---' + response.isUserExist);
                    component.set("v.showConfirmation",false);
                    component.set("v.emailLabel",response.strUserEmail);
                    component.set("v.isUsernamePasswordEnabled", false);
                }else{
                    console.log('****URL not correct****');
                    component.set("v.errorMessage", response.strErrorMessage);
                    component.set("v.showError",true);
                }
            });
            $A.enqueueAction(action);
        }
        else{
            component.set("v.errorMessage", $A.get("$Label.c.NewDev_URL_Validation"));
            component.set("v.showError",true);
        }
    },

    handleSelfRegister: function (component, event, helpler) {
        console.log('inside helper');
        var startUrl = component.get("v.startUrl");
        var contactID = component.get("v.contactID");
        var firstname = component.get("v.firstnameLabel");
        var lastname = component.get("v.lastnameLabel");
        var email = component.get("v.emailLabel");
        var password = component.get("v.passwordLabel");
        var confirmPassword = component.get("v.confirmPasswordLabel");
        var accessCode = component.get("v.otpLabel");
        var action = component.get("c.selfRegister");
        
        startUrl = decodeURIComponent(startUrl);
        console.log('startUrl '+startUrl+' contactID '+contactID+' firstname '+firstname+' lastname '+lastname);
		console.log('email '+email+' password '+password+' confirmPassword '+confirmPassword+' accessCode '+accessCode);
        
        action.setParams({
            firstname:firstname, 
            lastname:lastname, 
            email:email, 
            password:password, 
            confirmPassword:confirmPassword, 
            startUrl:startUrl,
            strContactID:contactID,
            accessCode:accessCode
        });
        action.setCallback(this, function(a){
          var rtnValue = a.getReturnValue();
          if (rtnValue === 'There has been an issue with your login attempt, please try again later. If the issue persists or you require assistance, please contact ICTChannel@nbnco.com.au') {
             component.set("v.errorMessage",'Email already registered. Either provide a unique Email address or Login to Portal with your existing credentials.');
             component.set("v.showError",true);
          }
          else{
             component.set("v.errorMessage",rtnValue);
             component.set("v.showError",true);   
          }
        });
    	$A.enqueueAction(action);
    },
    
    /*qsToEventMap: {
        'startURL'  : 'e.c:setStartUrl'
    },

    qsToEventMap2: {
        'expid'  : 'e.c:setExpId'
    },*/
    
    handleLogin: function (component, event, helpler) {
        var username = component.get("v.usernameLabel");
        var password = component.get("v.passwordLabel2");
        var contactCode = component.get("v.contactID");
        var action = component.get("c.login");
        var startUrl = component.get("v.startUrl");
        var sURL = window.location.href;
        var sURLPart;
        if (sURL.indexOf("did=") !== -1){
        	sURLPart = sURL.substring(sURL.search('did=')+4);
        }else{
            sURLPart = '';
        }
        startUrl = decodeURIComponent(startUrl);
        action.setParams({
            username:username, 
            password:password, 
            startUrl:startUrl,
            did:sURLPart,
           	strContactID:contactCode
        });
        action.setCallback(this, function(a){
            var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set("v.errorMessage",rtnValue);
                component.set("v.showError",true);
            }
        });
        $A.enqueueAction(action);
    },
    
    getIsUsernamePasswordEnabled : function (component, event, helpler) {
        var action = component.get("c.getIsUsernamePasswordEnabled");
        action.setCallback(this, function(a){
        var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.isUsernamePasswordEnabled',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    getIsSelfRegistrationEnabled : function (component, event, helpler) {
        var action = component.get("c.getIsSelfRegistrationEnabled");
        action.setCallback(this, function(a){
        var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.isSelfRegistrationEnabled',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    getCommunityForgotPasswordUrl : function (component, event, helpler) {
        var action = component.get("c.getForgotPasswordUrl");
        action.setCallback(this, function(a){
        var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.communityForgotPasswordUrl',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },
    
    getCommunitySelfRegisterUrl : function (component, event, helpler) {
        var action = component.get("c.getSelfRegistrationUrl");
        action.setCallback(this, function(a){
        var rtnValue = a.getReturnValue();
            if (rtnValue !== null) {
                component.set('v.communitySelfRegisterUrl',rtnValue);
            }
        });
        $A.enqueueAction(action);
    },

    setBrandingCookie: function (component, event, helpler) {
        var expId = component.get("v.expid");
        if (expId) {
            var action = component.get("c.setExperienceId");
            action.setParams({expId:expId});
            action.setCallback(this, function(a){ });
            $A.enqueueAction(action);
        }        
    }   
})