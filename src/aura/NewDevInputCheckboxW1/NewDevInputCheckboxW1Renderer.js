({
    afterRender: function (component) {
        this.superAfterRender();
        var checkbox = component.find('checkbox').getElement();
        checkbox.addEventListener('change', $A.getCallback(function () {
            component.set('v.value', checkbox.checked);
        }));
    }
})