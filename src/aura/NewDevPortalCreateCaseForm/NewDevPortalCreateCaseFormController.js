({
	myAction : function(component, event, helper) {
		var action = component.get("c.getStageApplicationwithStatusList");
        action.setCallback(this, function(response){
            component.set("v.stageAppList",response.getReturnValue());
        });
        $A.enqueueAction(action);
        
        var initCase = component.get("c.initializeCase");
        initCase.setCallback(this, function(response){
            component.set("v.caserec",response.getReturnValue());
        });
        $A.enqueueAction(initCase);
	},
    validateFields : function(component, event, helper){
		var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);
        var fieldId = event.currentTarget.id;       
        var fieldVal = document.getElementById(fieldId).value; 
        var error = helper.validateHelper(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);
    },
    createCase : function(component, event, helper){
        var subDiv = document.querySelectorAll('input,textarea,select');
        var myArray = [];
        for(var i = 0; i < subDiv.length; i++) {
          	var elem = subDiv[i];
            if(elem.id != 'cpd-searchKnowledge'){
            	myArray.push(elem.id);                
            }
        }
        var errorCarier = [];
        for(var inputElementId=0; inputElementId<myArray.length; inputElementId++){
            var errorDivId = document.getElementById(myArray[inputElementId]+'-div');
            var errorSpanId = document.getElementById(myArray[inputElementId]+'-error'); 
            var fieldId = myArray[inputElementId];   
            var fieldVal = document.getElementById(fieldId).value;
            var err = helper.validateHelper(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);
            errorCarier.push(err);
        }
        var errorflag = false;
        for(var i = 0; i < errorCarier.length; i++){
            if(errorCarier[i] == true){
                errorflag = true;
				break;                
            } 
        }
        if(!errorflag){
            component.set("v.showSpinner",true);
			var createCs = component.get("c.createComplexQuery");
            var Caserec = component.get("v.caserec");
            Caserec.Subject = document.getElementById('sub').value;
            Caserec.Description = document.getElementById('description').value;
            Caserec.Stage_Application__c = document.getElementById('stage').value;
            component.set("v.caserec",Caserec);
            createCs.setParams({
                "cs" : component.get("v.caserec")
            });
            createCs.setCallback(this, function(response){
                if(response.getState()  === "SUCCESS"){
                	component.set("v.caserec",response.getReturnValue());
                    component.set("v.showPopup",true);
                    component.set("v.showSpinner",false);
                    console.log('success');
                }
                else if (response.getState() === "ERROR") {
                    component.set("v.showSpinner",false);
                    console.log('error '+JSON.stringify(response.getError()));
                }
            });
            $A.enqueueAction(createCs);           
        }
    },
    closeModel: function(component, event, helper) {
      component.set("v.showPopup", false);
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/"
        });
        urlEvent.fire();
   },
    cancelCase : function(component, event, helper){
        document.getElementById('compDiv').style.display = 'none';
    }
})