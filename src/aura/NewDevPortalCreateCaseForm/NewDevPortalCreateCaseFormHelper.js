({
	helperMethod : function() {
		
	},
    
    validateHelper : function(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal){
        var errorflag = false;
        if(fieldVal==''){
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
            if(fieldId == 'sub'){
                errorSpanId.innerText = 'Please fill in the subject';
            }
            if(fieldId == 'stage'){
                errorSpanId.innerText = 'Please choose a stage application';
            }
            if(fieldId == 'description'){
                errorSpanId.innerText = 'Please fill in the description';
            }
            errorflag = true;
        }
        else{
            $A.util.removeClass(errorSpanId,'help-block-error');
            $A.util.removeClass(errorDivId,'has-error');
            $A.util.addClass(errorDivId,'has-success');
            errorSpanId.innerText = '';
            errorflag = false;
        }
        return errorflag;
    }
})