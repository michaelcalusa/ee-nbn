({  
    // Load current profile picture
    onInit: function(component, event, helper) {
        helper.getFileList(component, event, helper);
    },
    handleUploadFinished : function(component, event, helper){
        helper.getFileList(component, event, helper);
    },
    
    changeTaskStatus: function(component, event, helper) {
        var action = component.get("c.updateTaskStatus");
        action.setParams({
           tskId : component.get("v.recordId"),
        });
        action.setCallback(this, function(a){
            var response = a.getReturnValue();
            var toastEvent = $A.get("e.force:showToast");    
            if(response.status == 'Submitted'){              
               toastEvent.setParams({
                 title: 'Success!',
                 message: 'Task '+response.taskType+' was successfully marked as completed.',
                 duration:' 5000',
                 key: 'info_alt',
                 type: 'Success',
                 mode: 'dismissible'
                });  
                component.set("v.recordId",'');
                component.set("v.canUploadDocs",false);
                var cmpEvent = component.getEvent("disableUploadFile");
                cmpEvent.setParams({
                    "disableUploadFileSec":true,
                    "completedTaskId":response.taskId
                });
                cmpEvent.fire();
            }else{
                var err; 
                if(response.status == 'No Attachment'){
                    err = 'Task '+response.taskType+' could not be saved. Please check the documents attached and try again.';
                 }else {
                    err = 'You may not have permission to submit this task. Please contact nbn or your portal administrator.';
                 }
                toastEvent.setParams({
                 title: 'Error Message!',
                 message: err,
                 duration:' 5000',
                 key: 'info_alt',
                 type: 'error',
                 mode: 'dismissible'
                }); 
            }
          toastEvent.fire();
       }); 
       $A.enqueueAction(action);     
    },
    
    removeFile : function(component, event, helper){
        var target = event.target;
        var dataEle = target.getAttribute("data-selected-Index");
        var removeFileAction = component.get("c.removeContentDocument");
        removeFileAction.setParams({
	            "contentDocumentId":  dataEle,
	            "myRecordId":  component.get("v.recordId")
	    });  
        removeFileAction.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.FileList", response.getReturnValue());
            }
        });       
        $A.enqueueAction(removeFileAction);  
    }
    
})