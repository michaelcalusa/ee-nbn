({
    getFileList : function(component, event, helper){
        var action = component.get("c.getListContentDocument");
	    action.setParams({
	            "myRecordId":  component.get("v.recordId")
	    });  
	    
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.FileList", response.getReturnValue());
            }
            else{
                console.log('errors '+response.getError()[0].message);
            }
        });
	    $A.enqueueAction(action);
    }

})