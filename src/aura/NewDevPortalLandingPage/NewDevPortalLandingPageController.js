({
	onload : function(component, event, helper) {                
		var getStgApps = component.get("c.getStageApplications");	
        getStgApps.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                var listOfStg = response.getReturnValue();
                for(var i = 0; i < listOfStg.length; i++){
                    listOfStg[i].Name = helper.truncateStgAppName(component, listOfStg[i].Name);
                    listOfStg[i].Account_Name__c = helper.truncateAccName(component, listOfStg[i].Account_Name__c);
                } 
                component.set("v.listOfStgApp",listOfStg);
                component.set("v.sortAscSubmitDate", false);
                component.set("v.sortField", "CreatedDate");                               
            }
            else{
                console.log('Errored');
            }
        });
        
        var getCompletedStgApps = component.get("c.getCompletedStageApplications");	
        getCompletedStgApps.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                var listOfCompStg = response.getReturnValue();
                for(var i = 0; i < listOfCompStg.length; i++){
                    listOfCompStg[i].Name = helper.truncateStgAppName(component, listOfCompStg[i].Name);
                    listOfCompStg[i].Account_Name__c = helper.truncateAccName(component, listOfCompStg[i].Account_Name__c);
                }
                component.set("v.listOfCompletedStgApp",listOfCompStg);                              
            }
            else{
                console.log('Errored');
            }
        });
        $A.enqueueAction(getStgApps);
        $A.enqueueAction(getCompletedStgApps);	        
	},
    
    sortByDevName : function(component, event, helper) {
        helper.sortBy(component, "Name", component.get("v.sortAscDevname"));
    },
    
    sortByStage : function(component, event, helper) {
        helper.sortBy(component, "Stage__c",component.get("v.sortAscStage"));
    },
    
    sortByAccName : function(component, event, helper) {
        helper.sortBy(component, "Account_Name__c",component.get("v.sortAscCompany"));
    },
    
    sortByCreatedDate : function(component, event, helper) {
        helper.sortBy(component, "CreatedDate",component.get("v.sortAscSubmitDate"));
    },
    
    sortByEFSCD : function(component, event, helper) {
        helper.sortBy(component, "Estimated_First_Service_Connection_Date__c",component.get("v.sortAscEFSCD"));
    },
    
    sortByComplDevName : function(component, event, helper) {
        helper.sortByCompl(component, "Name", component.get("v.sortComplAscDevname"));
    },
    
    sortByComplStage : function(component, event, helper) {
        helper.sortByCompl(component, "Stage__c",component.get("v.sortComplAscStage"));
    },
    
    sortByComplAccName : function(component, event, helper) {
        helper.sortByCompl(component, "Account_Name__c",component.get("v.sortComplAscCompany"));
    },
    
    sortByComplCreatedDate : function(component, event, helper) {
        helper.sortByCompl(component, "CreatedDate",component.get("v.sortComplAscSubmitDate"));
    },
    
    sortByComplEFSCD : function(component, event, helper) {
        helper.sortByCompl(component, "Estimated_First_Service_Connection_Date__c",component.get("v.sortComplAscEFSCD"));
    },    
    
  displayStageAppDetails  : function(component, event, helper) {
       var selectedStageAppId = event.currentTarget.dataset.stageappid;
       var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": '/stage-application/'+selectedStageAppId
        });
        urlEvent.fire();
    },
    
    handleDisplayLandingPage : function(component, event, helper) {
       component.set("v.displayLandingPageBoolean",event.getParam("showLandingPageDetail"));
       component.set("v.displayStageAppDetailsBoolean",event.getParam("showStgAppDetail")); 
    }
})