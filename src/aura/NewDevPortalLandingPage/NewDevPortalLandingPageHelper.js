({
	sortBy : function(component, field, sortAscFlg) {
        var sortAsc = sortAscFlg;
        var sortField = component.get("v.sortField"),
            records = component.get("v.listOfStgApp");
        sortAsc = sortField != field || !sortAsc;
        records.sort(function(a,b){
            var t1 = a[field] == b[field],
                t2 = (!a[field] && b[field]) || (a[field] < b[field]);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        if(field == 'Name'){
			component.set("v.sortAscDevname", sortAsc);            
        }
        else if(field == 'Stage__c'){
        	component.set("v.sortAscStage", sortAsc);    
        }
        else if(field == 'Account_Name__c'){
        	component.set("v.sortAscCompany", sortAsc);    
        }
        else if(field == 'CreatedDate'){
        	component.set("v.sortAscSubmitDate", sortAsc);    
        }
        else if(field == 'Estimated_First_Service_Connection_Date__c'){
        	component.set("v.sortAscEFSCD", sortAsc);    
        }        
        component.set("v.sortField", field);
        component.set("v.listOfStgApp", records);
    },
    
    sortByCompl : function(component, field, sortAscFlg) {                
        var sortAsc = sortAscFlg;
        var sortField = component.get("v.sortField"),
            records = component.get("v.listOfCompletedStgApp");
        sortAsc = sortField != field || !sortAsc;
        records.sort(function(a,b){
            var t1 = a[field] == b[field],
                t2 = (!a[field] && b[field]) || (a[field] < b[field]);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        if(field == 'Name'){
			component.set("v.sortComplAscDevname", sortAsc);            
        }
        else if(field == 'Stage__c'){
        	component.set("v.sortComplAscStage", sortAsc);    
        }
        else if(field == 'Account_Name__c'){
        	component.set("v.sortComplAscCompany", sortAsc);    
        }
        else if(field == 'CreatedDate'){
        	component.set("v.sortComplAscSubmitDate", sortAsc);    
        }
        else if(field == 'Estimated_First_Service_Connection_Date__c'){
        	component.set("v.sortComplAscEFSCD", sortAsc);    
        }        
        component.set("v.sortField", field);
        component.set("v.listOfCompletedStgApp", records);
    },
    
    truncateStgAppName : function(component, stg){       
        var first30Stg = stg.slice(0,30);
        var remaningStg = stg.slice(30);
        var stgNameToDisp ;
        if(remaningStg.length >30){      
            stgNameToDisp = first30Stg + '<br/>' + remaningStg.slice(0,30) + '...';    
        }
        else{
            stgNameToDisp = first30Stg + '<br/>' + remaningStg;
        }
        return stgNameToDisp;
       
        
    },
    
    truncateAccName : function(component, acc){
        var first30Acc = acc.slice(0,25);
        var remaningAcc = acc.slice(25);
        var accNameToDisp ;
        if(remaningAcc.length >25){
            accNameToDisp = first30Acc + '<br/>' + remaningAcc.slice(0,25) + '...';    
        }
        else
        {
            accNameToDisp = first30Acc + '<br/>' + remaningAcc;    
        }
        return accNameToDisp;
    }
})