({
    onload : function(component, event, helper) {
		helper.getTaskDetails(component,component.get("v.recordId"));
    },

    onChangeofTaskId : function(component, event, helper) {
		helper.getTaskDetails(component,component.get("v.recordId"));
    },
    
    handleDisplayTaskDetailPage: function(component, event, helper) {
		component.set("v.displayTaskDetail",true);
        component.set("v.displayKnowledgeDetail",false);
        
    },
       displayKnowledgeDetails  : function(component, event, helper) {   
       component.set("v.displayTaskDetail",false);
       var selectedKnowledgeId = event.target.getAttribute("data-knowledgeId");
       var articleList = component.get("v.articleList");
       component.set("v.selectedKnowledgeId",selectedKnowledgeId); 
       component.set('v.displayTaskDetail',false);
        for (var i=0;i<articleList.length;i++)
       {
           if(selectedKnowledgeId ===articleList[i].Id)
           {
               component.set('v.selectedTitle',articleList[i].Title);
               component.set('v.selectedContent',articleList[i].New_Development_Content__c);
               
           }
       }
       
       component.set("v.displayKnowledgeDetail",true);
       var appEvent = $A.get("e.c:NewDevPortalShowHeaderAppEvent");
       appEvent.setParams({ "showHeader" : 'false' });
       appEvent.fire();                  
    },
   onTaskComplete : function(component, event, helper) {
		helper.getTaskDetails(component,event.getParam("completedTaskId"));
    }     
})