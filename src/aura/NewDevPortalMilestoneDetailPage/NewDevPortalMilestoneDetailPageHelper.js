({
	getTaskDetails : function(component,taskId) {
        if(taskId) 
        {
            var taskDetails = component.get("c.getTaskDetails");
            taskDetails.setParams({
                "taskId" : taskId
            });
            taskDetails.setCallback(this, function(response){
                if(response.getState() == 'SUCCESS'){
                    component.set("v.taskDetailsList",response.getReturnValue());
                    component.set("v.displayLandingPageBoolean",false);	                    
                    var listOfTask = response.getReturnValue();
                    for(var i = 0; i < listOfTask.length; i++){
                        if(listOfTask[i].Deliverable_Status__c == 'Submitted' || listOfTask[i].Deliverable_Status__c == 'Pending Documents'){
                            component.set("v.displayExplanation",true);   
                            if(listOfTask[i].Deliverable_Status__c == 'Pending Documents'){
                                component.set("v.canuploadDocs",true);    
                            }
                        }
                        var desc = listOfTask[i].Description;
                        if(desc){
                            if(desc.includes("Feedback from nbn:")){
                                var modDesc = desc.replace(/Feedback from nbn:/g,"<br/><br/><b>Feedback from nbn:</b>");                                
                                listOfTask[i].Description = modDesc;
                            }
                            var nbn = listOfTask[i].Description;
                            var nbnBold = nbn.replace(/nbn/gi,"<b>nbn</b>");
                            listOfTask[i].Description = nbnBold;
                            component.set("v.displayComment",true);	                            
                        }
                        component.set("v.sAId",listOfTask[i].WhatId); 
                    }
                    component.set("v.taskDetailsList",listOfTask);                                                               
                }
                else{
                    console.log('Error occured');
                }
            });
            
            var taskExp = component.get("c.getTaskExplanation");
            taskExp.setParams({
                "taskId" : taskId
            });
            taskExp.setCallback(this, function(response){
                if(response.getState() == 'SUCCESS'){
                    component.set("v.taskExplanation",response.getReturnValue());
                }
                else{
                    console.log('Error occured');
                    var errors = response.getError();
                    if(errors){
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    }
                }
            });
            var knowledgeDetails = component.get("c.getKnowledgeArticle");
            knowledgeDetails.setParams({
                "taskId" : taskId
            });
            knowledgeDetails.setCallback(this, function(response){
                if(response.getState() == 'SUCCESS'){
                    var articleList = response.getReturnValue();
                    var summaryTruncated;
                    for (var i=0;i<articleList.length;i++)
                    {
                        summaryTruncated = articleList[i].Summary;
                        
                        summaryTruncated = summaryTruncated.slice(0,20);
                        articleList[i].SummaryTruncated = summaryTruncated;
                        articleList[i].readMore = false;
                        
                        
                        
                    }                   
                    component.set("v.articleList",articleList);
                }                                               
                
                else{
                    console.log('Error occured');
                }
            });
    		$A.enqueueAction(knowledgeDetails); 
            $A.enqueueAction(taskDetails);  
            $A.enqueueAction(taskExp); 
        }
        else{
            console.log('Selected Stage Application Id is null');
        }
		
	}
})