({
	onload : function(component, event, helper) { 
        var selTaskFilter;
        component.set("v.stageAppId",component.get("v.recordId"));
        if (component.get("v.selectedTaskFilter") == null) {
            selTaskFilter = 'Approved/waiting';
        }
        else {
              selTaskFilter = component.get("v.selectedTaskFilter");
        }
        helper.getStageAppDetails(component, event, helper, component.get("v.recordId"), selTaskFilter);
	},
    
    displayTaskDetails  : function(component, event, helper) {   
       var selectedTaskId = event.currentTarget.dataset.title;
        component.set("v.taskId",selectedTaskId);  
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": '/task/'+selectedTaskId+'?StageName='+component.get("v.stageAppId")
        });
        urlEvent.fire();
    },

    getSelectedFilterTasks : function(component, event, helper) {
        var taskFilter = document.getElementById('taskFiter').value;
        helper.getStageAppDetails(component, event, helper, component.get("v.stageAppId"), taskFilter);
    },
 
    getSelectedStageAppDetails : function(component, event, helper) {
        var params = event.getParam('arguments');
        var selTaskFilter = 'Approved/waiting';
        if (params) {
       		 var stageappid = params.stgappid;
        	 helper.getStageAppDetails(component, event, helper, stageappid, selTaskFilter);
        }  
    } 
})