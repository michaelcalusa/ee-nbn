({
	getStageAppDetails : function(component, event, helper, stageAppId, selectedTaskFilter) { 
      if(stageAppId) 
        {
            // To get Stage Application details
            var stageAppDetails = component.get("c.getStageAppDetails");
            stageAppDetails.setParams({
        			"stageAppId" : stageAppId
            });
            stageAppDetails.setCallback(this, function(response){
                if(response.getState() == 'SUCCESS'){
                    var stgDet = response.getReturnValue();
                    for(var i = 0; i < stgDet.length; i++){
                        if(i==0){                            
                            component.set("v.srcStageAppName",stgDet[i].Name);
                            component.set("v.srcBuildType",stgDet[i].Build_Type__c);
                            component.set("v.srcActualTech",stgDet[i].Local_Technology__c);
                            component.set("v.srcPremisesCount",stgDet[i].No_of_Premises__c);
                            component.set("v.srcEFSCD",stgDet[i].Estimated_Ready_for_Service_Date__c);
                            component.set("v.srcPlannedTech",stgDet[i].Local_Technology__c); 
                            component.set("v.srcStage",stgDet[i].Stage__c); 
                        }
                        stgDet[i].Name = helper.truncateStgAppName(component, stgDet[i].Name)
                    }
                    component.set("v.stageAppDetailsList",response.getReturnValue());
                    component.set("v.displayStageAppDetailsBoolean",true);
                }
                else{
                    console.log('Error occured in getting Stage App Details');
                }
            }) 
            // To get Milestones(Tasks) associated with Stage Application 
            var taskDetails = component.get("c.getStageAppTasks");
            taskDetails.setParams({
                "stgAppID" : stageAppId,
                "selTaskFilter" : selectedTaskFilter
            });
            taskDetails.setCallback(this, function(response){
                if(response.getState() == 'SUCCESS'){
                    component.set("v.taskList",response.getReturnValue());
                }
                else{
                    console.log('Error occured in getting task details');
                }
            });
            
            $A.enqueueAction(stageAppDetails);
            $A.enqueueAction(taskDetails);
            
        }
        else{
                    console.log('Selected Stage Application Id is null');
        } 
    },
  
    truncateStgAppName : function(component, stg){       
        var numOfCharToDisplay = 30;  
        var stgNameToDisp='';
        while (stg.length > numOfCharToDisplay) {
               stgNameToDisp = stgNameToDisp+stg.slice(0,numOfCharToDisplay)+'<br/>'; 
               stg = stg.slice(numOfCharToDisplay); 
        }
        stgNameToDisp = stgNameToDisp+stg.slice(0,stg.length); 
        return stgNameToDisp;
    }      
})