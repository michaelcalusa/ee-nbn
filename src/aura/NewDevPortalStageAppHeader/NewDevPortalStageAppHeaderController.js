({
    onload : function(component, event, helper) {
        
        var recId = component.get("v.recordId")
        var stageAppDetails = component.get("c.getStageAppHeaderDetail");
        stageAppDetails.setParams({
            "recId" : recId
        });
        
        stageAppDetails.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                var stgDet = response.getReturnValue();
                component.set("v.stageAppName",stgDet.Name);
                component.set("v.buildType",stgDet.Build_Type__c);
                component.set("v.actualTech",stgDet.Local_Technology__c);
                component.set("v.premisesCount",stgDet.No_of_Premises__c);
                component.set("v.EFSCD",stgDet.Estimated_Ready_for_Service_Date__c);
                component.set("v.plannedTech",stgDet.Local_Technology__c);
                component.set("v.stage",stgDet.Stage__c); 
            }
            else{
                console.log('Error occured in getting Stage App Details on task');
            }
            
        }) 
        $A.enqueueAction(stageAppDetails);              
    },
    
    displayStageDetailPage : function(component, event, helper) {           
        
        // the function that reads the url parameters
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('?'),
                sParameterName,
                i;
            
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                
                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };       
        var addrs;
        if(getUrlParameter('StageName')!==undefined){
            addrs =  '/stage-application/'+getUrlParameter('StageName'); 
        }else{
            addrs = '/';
        }  
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": addrs
        });
        urlEvent.fire();                
    },
    handleApplicationEvent : function(component, event) {
        var displayHeader = event.getParam("showHeader");
        component.set("v.displayHeader",displayHeader);
    }
})