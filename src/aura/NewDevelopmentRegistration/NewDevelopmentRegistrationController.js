({
    initialize: function(component, event, helper) {
      $A.get("e.siteforce:registerQueryEventMap").setParams({"qsToEvent" : helper.qsToEventMap}).fire();       
        // Retrieve query string parameters for First Name, Last Name and Email
        helper.getURLParametersHelper(component , event, helper);
    },
    
    handleSelfRegister: function (component, event, helpler) {
        var ErrorBox = component.find("cpd-givenName-error");
        var checkTerm = component.find("ch1").get("v.value");
        var errorPass= component.find("cpd-password-error");          
        var passwrd = component.find("password").get("v.value");        
        var errorConPass= component.find("cpd-Confrmpassword-error"); 
        var confirmPass = component.find("confirmPassword").get("v.value"); 
        if(checkTerm == false){
           $A.util.addClass(ErrorBox, 'err');          
        }
        else if(passwrd==''){
           $A.util.addClass(errorPass, 'err');           
        }
        else if(confirmPass=='' || (confirmPass!='' && (passwrd != confirmPass))){
            $A.util.addClass(errorConPass, 'err');            
        }
        else{
          $A.util.removeClass(errorPass,'err');
          $A.util.removeClass(ErrorBox, 'err');
          $A.util.removeClass(errorConPass, 'err');
          helpler.handleSelfRegister(component, event, helpler);
        }
    },
    setStartUrl: function (component, event, helpler) {
        var startUrl = event.getParam('startURL');
        if(startUrl) {
            component.set("v.startUrl", startUrl);
        }
    },
    
    passwordCheck: function(component, event, helpler){
        var errorPass= component.find("cpd-password-error");          
        var passwrd = component.find("password").get("v.value");  
        var errorConPass= component.find("cpd-Confrmpassword-error"); 
        var confirmPass = component.find("confirmPassword").get("v.value");
        if(passwrd==''){
           $A.util.addClass(errorPass, 'err');  
        }
        else if(confirmPass!='' && (passwrd != confirmPass)){
            $A.util.addClass(errorConPass, 'err'); 
            $A.util.removeClass(errorPass,'err');
        }
        else{
            $A.util.removeClass(errorPass,'err'); 
            $A.util.removeClass(errorConPass, 'err'); 
        }
    },
    
    passwordMatch: function(component, event, helpler){       
        var errorConPass= component.find("cpd-Confrmpassword-error");        
        var passwrd = component.find("password").get("v.value");
        var confirmPass = component.find("confirmPassword").get("v.value");    
        if(confirmPass=='' || (confirmPass!='' && (passwrd != confirmPass))){
            $A.util.addClass(errorConPass, 'err');           
        }
        else{           
            $A.util.removeClass(errorConPass,'err');
        }
    }
})