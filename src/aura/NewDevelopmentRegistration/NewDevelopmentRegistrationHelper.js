({
    qsToEventMap: {
        'startURL'  : 'e.c:NewDevSetStartURL'
    },
    
    getURLParametersHelper: function(component, event, helpler) {
        var url = window.location.href;
        var contactID;
        if(url.indexOf('=') !==''){
			contactID = url.substring( url.indexOf('=') + 1);            
            component.set("v.contactID", contactID);
            // Verify if user already exist
            var action = component.get("c.checkCommunityUser");

            action.setParams({strContactID:contactID});
            action.setCallback(this, function(a){
            	var response = a.getReturnValue();
                
                component.set("v.retrieveUserInformation",response);
                if(response.isUserExist == true){
                    console.log('****User exist****');
                    component.set("v.showConfirmation",true);
                    component.set("v.showRegistration",false);
                }
                else if(response.isUserExist == false && response.strErrorMessage == '')
                {
                    console.log('****User does not exist****' + response.strErrorMessage + '---' + response.isUserExist);
                    component.set("v.showConfirmation",false);
                	component.set("v.showRegistration",true);
                    component.set("v.emailLabel",response.strUserEmail);
                }
                else{
                    console.log('****URL not correct****');
                    component.set("v.showConfirmation",false);
                	component.set("v.showRegistration",true);
                    component.set("v.errorMessage", response.strErrorMessage);
                    component.set("v.showError",true);
                }
            });
            $A.enqueueAction(action);
        }
        else{
			// If email is NULL then URL is malformed. Show error
            component.set("v.errorMessage", $A.get("$Label.c.NewDev_URL_Validation"));
            component.set("v.showError",true);
            component.set("v.showRegistration",true);
        }
    },
    
    handleSelfRegister: function (component, event, helpler) {
        var startUrl = component.get("v.startUrl");
        var contactID = component.get("v.contactID");
        var firstname = component.get("v.firstnameLabel");
        var lastname = component.get("v.lastnameLabel");
        var email = component.get("v.emailLabel");
        var password = component.find("password").get("v.value");
        var confirmPassword = component.find("confirmPassword").get("v.value");
        var accessCode = component.find("accessCode").get("v.value");
        var action = component.get("c.selfRegister");
        
        startUrl = decodeURIComponent(startUrl);
        console.log('startUrl '+startUrl+' contactID '+contactID+' firstname '+firstname+' lastname '+lastname);
		console.log('email '+email+' password '+password+' confirmPassword '+confirmPassword+' accessCode '+accessCode);
        
        action.setParams({
            firstname:firstname, 
            lastname:lastname, 
            email:email, 
            password:password, 
            confirmPassword:confirmPassword, 
            startUrl:startUrl,
            strContactID:contactID,
            accessCode:accessCode
        });
        action.setCallback(this, function(a){
          var rtnValue = a.getReturnValue();
          if (rtnValue === 'There has been an issue with your login attempt, please try again later. If the issue persists or you require assistance, please contact ICTChannel@nbnco.com.au') {
             component.set("v.errorMessage",'We are experiencing technical issues.<br/>Please try again after some time.');
             component.set("v.showError",true);
          }
          else{
             component.set("v.errorMessage",rtnValue);
             component.set("v.showError",true);   
          }
        });
    	$A.enqueueAction(action);
    }    
})