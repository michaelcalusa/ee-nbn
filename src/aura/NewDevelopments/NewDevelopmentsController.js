({
	myAction : function(component, event, helper) {
		var action = component.get("c.initWrapper");
        action.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                component.set("v.newDevApplicantWrapperInstance",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    
    checkServiceDelivery : function(component, event, helper) {
		var action = component.get("c.getServiceDeliveryType");        
        var essentialservice = component.get("v.essentialservice");
        var buildingType = component.get("v.buildingtype");
        var mtm = component.get("v.mtm");
        var pitandpipepvt = component.get("v.pitandpipepvt");
        var permisecount = component.get("v.pCount");
        action.setParams({
                "isEssentialService": essentialservice, 
                "buildingType": buildingType ,
                "MTM": mtm,
                "premiseCount": permisecount,
                "isPitPipePrivate": pitandpipepvt                
            });
        action.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                alert('** Service DeliveryDetails ***'+ JSON.stringify(response.getReturnValue()));
                component.set("v.mapServiceDeliveryDetails",response.getReturnValue());
            }
            else {
                console.log('** Service DeliveryDetails - Error ***');
            }
        });
        $A.enqueueAction(action);
	},
    
    
    
    invokeTechAssessmentProcess: function(cmp, event, helper){
        var appMap = cmp.get("v.newDevApplicationWrapper");  
        appMap['technicalassessment'] = 'Yes'; 
        appMap['servicedeliverytype'] = '';
         cmp.set("v.newDevApplicationWrapper",appMap);
        var noPayment = true;
        helper.processApplicationForm(cmp, event, helper, noPayment);
    },    
    
      processResponseFromPaymentComponent: function(cmp, event, helper){
        var responseMap = event.getParams('paymentComponentResponseParams');
        helper.processApplicationForm(cmp, event, helper);
    },
    
    invokePaymentOptions :  function(cmp, event, helper){
        cmp.set('v.showPaymentOptionsComponent',true);      
    },
    
      processApplicationSubmissionProcess :  function(cmp, event, helper){
         if(cmp.get('v.displayPaymentSection')){
             var validationPass = helper.paymentProceedButtonCheck(cmp, event, helper);
             if(validationPass){
              	cmp.set('v.showPaymentOptionsComponent',true);   
             }
         }
          else if(cmp.get('v.newDevApplicantWrapperInstance').serviceDeliveryDtlWrapper.serviceDeliveryType == 'SD Unknown'){
              var validationPass = helper.paymentProceedButtonCheck(cmp, event, helper);
             if(validationPass){
                 document.getElementById('smallDevTC').style.display = "none";
              	helper.processSubmit(cmp, event, helper,'','');  
                helper.callServer(cmp, 'login', {newDevId : cmp.get("v.newDevApplicantWrapperInstance").newDevApplicantId});  
             }
          }
         else{            
            helper.processSubmit(cmp, event, helper,'','');
            helper.callServer(cmp, 'login', {newDevId : cmp.get("v.newDevApplicantWrapperInstance").newDevApplicantId});  
         }
    },   
    
    handleDisplayChange : function(component, event, helper){
        var changedComponentName = event.getParam("firedComponentName");
        if(changedComponentName == 'pageTerms'){
            window.location.hash = '#mainHeading'; 
            component.set("v.displayContractSignature",false);
            component.set("v.displayMyDetails",true);
            component.set("v.BillingInformation",false);
            component.set("v.displayDevDetails",false);
        }
        else if(changedComponentName == 'myDetails'){
            window.location.hash = '#myDetails'; 
            component.set("v.displayContractSignature",false);
            component.set("v.displayMyDetails",true);
            component.set("v.BillingInformation",false);
            component.set("v.displayDevDetails",false);
        }
        else if(changedComponentName == 'ContractSignatory'){
            window.location.hash = '#contractSignatory';
            component.set("v.displayContractSignature",true);
            component.set("v.displayMyDetails",false);
            component.set("v.BillingInformation",false);
            component.set("v.displayDevDetails",false);
        }
        else if(changedComponentName == 'CustomerBusinessDetails'){
            window.location.hash = '#customerBusinessDetails';
            component.set("v.displayMyDetails",false);
            component.set("v.displayContractSignature",false);
            component.set("v.BillingInformation",true);
            component.set("v.displayDevDetails",false);
        }
        else if(changedComponentName == 'developmentDetails'){
            window.location.hash = '#devDetails';
            component.set("v.displayMyDetails",false);
            component.set("v.displayContractSignature",false);
            component.set("v.CustomerBusinessDetails",false);
            component.set("v.displayDevDetails",true);
        }
    },
    handleSaveEvent : function(component, event, helper){
        var wrapperInstance = event.getParam("wrapperInstance");
        component.set("v.newDevApplicantWrapperInstance",wrapperInstance);
        var savedSection = event.getParam("componentSaved");
        if(savedSection == 'pageTerms'){
            window.location.hash = '#mainHeading';
            component.set("v.createPage",false);
            component.set("v.displayMyDetails",true);
        	component.set("v.displayContractSignature",false);
        }
        else if(savedSection == 'myDetails'){
            window.location.hash = '#devDetails';
            component.set("v.createPage",false);
            component.set("v.displayMyDetails",false);
            component.set("v.displayDevDetails",true);
            component.set("v.displayContractSignature",false);
            component.set("v.BillingInformation",false);
            component.set("v.isYourDetailsSaved",true);
        }
        else if(savedSection == 'developmentDetails'){
            window.location.hash = '#contractSignatory';
            component.set("v.createPage",false);
            component.set("v.displayMyDetails",false);
            component.set("v.displayDevDetails",false);
            component.set("v.displayContractSignature",true);
            component.set("v.BillingInformation",false);
             component.set("v.isDevelopmentDetailsSaved",true);
        }
        else if(savedSection == 'contractSignatory'){
            window.location.hash = '#customerBusinessDetails';
            component.set("v.createPage",false);
            component.set("v.displayMyDetails",false);
            component.set("v.displayDevDetails",false);
            component.set("v.displayContractSignature",false);
            component.set("v.BillingInformation",true);
            component.set("v.isContractSignatoryDetailsSaved",true);
        }
        else if(savedSection == 'CustomerBusinessDetails'){
            component.set("v.createPage",false);
            component.set("v.displayMyDetails",false);
            component.set("v.displayDevDetails",false);
            component.set("v.displayContractSignature",false);
            component.set("v.BillingInformation",false);
            component.set("v.isBillingDetailsSaved",true);
        }
        else if(savedSection == 'costDetails' && component.get("v.isYourDetailsSaved") && component.get("v.isDevelopmentDetailsSaved") && component.get("v.isContractSignatoryDetailsSaved") && component.get("v.isBillingDetailsSaved")){
            component.set("v.disableSubmissionButton",false);
        }
        else if(savedSection == 'costDetails' && component.get("v.isYourDetailsSaved") && component.get("v.isDevelopmentDetailsSaved") &&  wrapperInstance.yourDtlWrapper.role == 'All Other requestors'){
              component.set("v.disableSubmissionButton",false);   
        } 
    },
    
    handleDisplaySection : function(component, event, helper) {
         var createPageValue = event.getParam("createPageValue");
         component.set("v.createPage", createPageValue);
    },
    
    handleDisableBillingEvent : function(component, event, helper){
        console.log('Inside Handler');
         
        component.set("v.disableBillingInfo", event.getParam("disableBillingInfo"));
    },
    handleDisableFormEvent : function(component, event, helper){
        component.set("v.disableNewDevAccordion", event.getParam("disableNewDevForm"));
    },
    handleDisplayPaymentSection : function(component, event, helper) {  
        var costWrapperInstance = event.getParam("wrapperInstance");
        var dsplyCost = event.getParam("displayCostDtls");
        if(dsplyCost == 'Yes'){
             component.set("v.displayPaymentSection",true);
            if(costWrapperInstance.pitAndPipeDetailsWrapper.totalNumberOfPremisesForStage != null){
                component.set("v.totalPremiseCount", costWrapperInstance.pitAndPipeDetailsWrapper.totalNumberOfPremisesForStage);
            }
            else{                
                if(costWrapperInstance.devDtlWrapper.numberOfPremises != null){
                	component.set("v.totalPremiseCount", costWrapperInstance.devDtlWrapper.numberOfPremises);
                }
            }              
        }
        else{
            component.set("v.displayPaymentSection",false);
        }        
    },
    
    termsAcceptedCheck : function(component, event, helper){
        helper.paymentProceedButtonCheck(component, event, helper);
    },
     
    handleBillingOnPremise : function(component, event, helper){      
       component.set("v.disableSubmissionButton",true);
       component.set("v.isDevelopmentDetailsSaved",false);
       component.set("v.isBillingDetailsSaved",false); 
        component.set("v.displayPaymentSection",false); 
    },

    handleDisableSubmitButton : function(component, event, helper){
        console.log('disable submission button event');
        component.set("v.disableSubmissionButton", true);
    }
})