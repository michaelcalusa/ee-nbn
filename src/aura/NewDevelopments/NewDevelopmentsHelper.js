({
    getPaymentInputs: function(cmp,event,helper){
        var action = cmp.get('c.generateUniqueApplicationId');        
        action.setCallback(this, function(response){
             action.setParams({
                "strlength": '20'               
            });
            var state = response.getState();
            
            if (state === "SUCCESS") {                    
                var returnResponse = response.getReturnValue();                                   
                cmp.set('v.applicationReference',returnResponse);
                // Jump to top of the page
                window.scrollTo(0, 0);
            }
            else if(state === "ERROR"){
                // set application submission to false
               console.log('Error');
                // Jump to top of the page
                window.scrollTo(0, 0);
            }
        });
        $A.enqueueAction(action);         
    },

    callServer: function (component, method, data) {
        var self = this;
        var action = component.get('c.' + method);
        action.setParams(data);
        console.log('%ccalling c.' + method, 'color:blue', JSON.parse(JSON.stringify(data)));
        var p = new Promise($A.getCallback(function (resolve, reject) {
            action.setCallback(self, $A.getCallback(function (response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    console.log('%cresponse c.' + method, 'color:blue', JSON.parse(JSON.stringify(response.getReturnValue())));
                    return resolve(response.getReturnValue());
                }

                if (state === 'ERROR') {
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) {
                        console.error(errors[0].message); 
                        return reject(new Error(errors[0].message));
                    }
                    console.error('UNKNOWN_ERROR');
                    return reject(new Error('UNKNOWN_ERROR'));
                }

                console.error(state);
                return reject(new Error(state));
            }));
        }));
        $A.enqueueAction(action);
        component.set('v.showSpinner', true);
        return p.then($A.getCallback(function (data) {
            component.set('v.showSpinner', false); 
            return data;
        })).catch($A.getCallback(function (err) {
                component.set('v.showSpinner', false); 
                component.set('v.errorMsg', err.message);
                console.log('error message-->'+component.get('v.errorMsg'));
                if(component.get('v.errorMsg') !== undefined)
                {
                    component.find('reassignToastWF').show();
                    throw err;
                }
                console.error(err);
        }));
    },

    processApplicationForm : function(cmp, event, helper) {
        var paymentResponse = event.getParam("paymentComponentResponseParams");
        cmp.set('v.paymentComponentResponseParams',paymentResponse);
        var paymentMethod = paymentResponse.transactionName;
        var paymentStatus = paymentResponse.status;
        var receiptNumber = '';
        if(paymentMethod == 'submitpayment' && paymentStatus != 'error'){
            receiptNumber = paymentResponse.ReceiptNumber;
        }
        if((paymentMethod == 'submitpayment' || paymentMethod == 'paybyInvoice') && paymentStatus != 'error'){
            var action = cmp.get("c.submitProcess");
             action.setParams({
                 "newDevId" : cmp.get("v.newDevApplicantWrapperInstance").newDevApplicantId,
                 "paymentMethod" : paymentMethod,
                 "receiptNumber" : receiptNumber,
                 "wrapString" : JSON.stringify(cmp.get("v.newDevApplicantWrapperInstance"))
             });
             action.setCallback(this, function(response){
                 var state = response.getState();
                 if(state === "SUCCESS"){
                     /*var retVal = response.getReturnValue();
                     console.log('response.getReturnValue()123 '+JSON.stringify(response.getReturnValue()));
                     cmp.set("v.applicationReferenceNumber",retVal.applicationReferenceNumber);
                     cmp.set("v.paymentReferenceNumber",retVal.paymentReferenceNumber);
                     cmp.set("v.applicantEmail",retVal.applicantEmail);
                     cmp.set("v.totalCost",retVal.totalCost);
                     cmp.set("v.billingContactName",retVal.billingContactName);
                     cmp.set("v.billingContactEmail",retVal.billingContactEmail);
                     cmp.set("v.confirmationType",retVal.confirmationType);
                     //cmp.set("v.applicationSubmitted",true);                     
                     cmp.set("v.portalURL",retVal.portalURL);
                      if(retVal.CustomerClass=='Class3/4' && retVal.SDType==='SD-2')
                     {
                         cmp.set("v.displayRefNum",true);
                     }
                     else
                     {
                         cmp.set("v.displayRefNum",false);
                     }
                    
                     if(retVal.SDType==='SD Unknown')
                     {
                       cmp.set("v.isTechAss",true);
                       
                     }*/
                     return this.callServer(cmp, 'login', {newDevId : cmp.get("v.newDevApplicantWrapperInstance").newDevApplicantId}); 
                     //return 'success';
                 }
                 else{
                     console.log(' ERROR ALERT ON SUBMIT '+response.getError()[0].message);
                     return 'error';
                 }
             });
             $A.enqueueAction(action);
        }
    },
    
    processSubmit : function(component, event, helper, paymentMethod, receiptNumber){
        var action = component.get("c.submitProcess");
             action.setParams({
                 "newDevId" : component.get("v.newDevApplicantWrapperInstance").newDevApplicantId,
                 "paymentMethod" : paymentMethod,
                 "receiptNumber" : receiptNumber,
                 "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance"))
             });
             action.setCallback(this, function(response){
                 var state = response.getState();
                 if(state === "SUCCESS"){
                     var retVal = response.getReturnValue();
                     component.set("v.applicationReferenceNumber",retVal.applicationReferenceNumber);
                     component.set("v.paymentReferenceNumber",retVal.paymentReferenceNumber);
                     component.set("v.applicantEmail",retVal.applicantEmail);
                     component.set("v.totalCost",retVal.totalCost);
                     component.set("v.billingContactName",retVal.billingContactName);
                     component.set("v.billingContactEmail",retVal.billingContactEmail);
                     component.set("v.confirmationType",retVal.confirmationType);
                     component.set("v.portalURL",retVal.portalURL);
                     //component.set("v.applicationSubmitted",true);
                     if(retVal.CustomerClass=='Class3/4' && retVal.SDType==='SD-2'){
                         component.set("v.displayRefNum",true);
                     }
                else{
                         component.set("v.displayRefNum",false);
                     }
                    
                if(retVal.SDType==='SD Unknown'){
                       component.set("v.isTechAss",true);
                     }
                     return 'success';
                 }
                 else{
                     console.log(' ERROR ALERT ON SUBMIT '+response.getError()[0].message);
                     return 'error';
                 }
             });
             $A.enqueueAction(action);
    },
    
    paymentProceedButtonCheck : function(component, event, helper){
        var checked = document.getElementById('tc-agreeTerms-checkbox').checked;
        var errorSpanId = document.getElementById('tc-agreeTerms-error');
        var errorDivId = document.getElementById('tc-agreeTerms-div');
        if(checked){
            $A.util.removeClass(errorSpanId,'help-block-error');               
            $A.util.removeClass(errorDivId,'has-error');              
            $A.util.addClass(errorDivId,'has-success');
            document.getElementById('tc-agreeTerms-error').innerHTML = '';
            return true;
        }
        else{
            $A.util.addClass(errorSpanId,'help-block-error');               
            $A.util.addClass(errorDivId,'has-error');              
            $A.util.removeClass(errorDivId,'has-success');
            document.getElementById('tc-agreeTerms-error').innerHTML = 'Please tick the checkbox to continue';
            return false;
        }
    }
})