({
    doInit: function (component, event, helper) {
		console.log('In FileUpload doInit');
       	var sPageURL = decodeURIComponent(window.location.search.substring(1));
       	var sParameterName = sPageURL.split('=');
        component.set("v.recordId",sParameterName[1]);
        component.set("v.fileUploaded",false);
        var parentRecordId = component.get("v.recordId");
        helper.getFileList(component, event, helper, parentRecordId);
        helper.getBuildType(component, event, helper, parentRecordId);
    },
    
    handleUploadFinished: function (component, event, helper) {
        var uploadedFiles = event.getParam("files"); 
        var parentRecordId = component.get("v.recordId");
        helper.getFileList(component, event, helper, parentRecordId);
        component.set("v.fileUploaded",true);
    },
    
    onDevelopmentIdChanged: function (component, event, helper) {
        console.log('Id changed');
        var recordId = component.get('v.recordId');
        if (recordId && Object.keys(recordId).length !== 0) {
            window.onbeforeunload = helper.onBeforeunload;
        }
    },
    
    removeFile : function(component, event, helper){
        var target = event.target;
        var dataEle = target.getAttribute("data-selected-Index");
        var index = target.dataset.index;
        var removeFileAction = component.get("c.removeContentDocument");
        removeFileAction.setParams({
	           "contentDocumentId":  dataEle,
	           "myRecordId":  component.get("v.recordId")
	    });  
        removeFileAction.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var filelist=  component.get("v.FileList");
                var docIds = component.get("v.conDocIds");
                filelist.splice(index, 1); 
                component.set("v.FileList", filelist);  
                docIds.splice(docIds.indexOf(dataEle), 1);
                component.set("v.conDocIds", docIds);              
            }
        });       
        $A.enqueueAction(removeFileAction);  
    },
    
    updateTaskStatus: function (component, event, helper){
        //move uploaded file on task
        var action = component.get("c.updateTask"); 
        action.setParams({
            "myRecordId": component.get("v.recordId")
        });  
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") { 
                window.onbeforeunload = null;
                window.location.href=response.getReturnValue();            
            }else{
                console.log('errors '+response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
    }
})