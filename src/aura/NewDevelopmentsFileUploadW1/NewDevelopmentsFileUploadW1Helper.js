({
    getBuildType: function(component, event, helper, appId)
    {
        var action = component.get("c.buildTypeCheck");
        action.setParams({
            "newDevId": appId 
        });
        action.setCallback(this, function(response){        
            var state = response.getState();
            if (state === "SUCCESS"){                
                component.set('v.buildType',response.getReturnValue());
                if( response.getReturnValue() === true){
                    component.set('v.contextContent', $A.get("$Label.c.NewDev_ServicePlan_Text"));
                }     
            }
            else{
                console.log('errors '+response.getError()[0].message);
            }
        });
    	$A.enqueueAction(action);                    
	},
            
    getFileList : function(component, event, helper, recordId){            
        var action = component.get("c.getListContentDocument"); 
        action.setParams({
        	"myRecordId": recordId
        });  
       	action.setCallback(this, function(response){
            var state = response.getState();
            var NumofFiles;
            if (state === "SUCCESS") {                
                var fileList = response.getReturnValue();
                component.set("v.FileList", fileList);                
                NumofFiles = component.get("v.FileList").length;
                component.set("v.NumofFiles", NumofFiles);
            }
            else{
                console.log('errors '+response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);       
    },
                
    onBeforeunload: function (event) {
        event = event || window.event;
        var message = 'This will take you back to the nbn homepage and you will loose all information entered, Do you wish to proceed?';
        if (event) {
            event.returnValue = message;
        }
        return message;
    },
})