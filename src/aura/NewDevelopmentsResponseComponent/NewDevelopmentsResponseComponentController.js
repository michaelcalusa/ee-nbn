({
	 doInit: function(component, event, helper) { 
        var rMap = component.get('v.responseMap'); 
        var receiptNo = rMap['receiptnumber'];
        component.set('v.applicationReferenceNumber',rMap['referencenumber']);
        component.set('v.receiptNumber',receiptNo);
    }, 
    
	handleResponseMap: function(component, event, helper) { 
    },
})