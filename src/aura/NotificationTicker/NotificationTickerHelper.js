({
	cycle : function (component) {
        var notifications = component.get("v.notifications");
        
        component.set("v.notification", notifications[component.get("v.notificationCounter")]);
        var i;
        
		
		
		
        window.setInterval(
            $A.getCallback(function() { 
                if (component.isValid()){
                    i = component.get("v.notificationCounter");
                    i++;
                    component.set("v.notificationCounter", i);
                    if (component.get("v.notificationCounter") >= notifications.length)
                        	component.set("v.notificationCounter", 0);
                    
                    component.set("v.notification", notifications[component.get("v.notificationCounter")]);
                }
            }), component.get("v.cycleInterval")
        ); 
    },
    
    getNotifications : function (component) {
        console.log('getNotifications');
        var action = component.get("c.getNotificationTitles");
        action.setParams({
            topic : component.get("v.Topic")
        });

        action.setCallback(this, function(response) {
        	var state = response.getState();
            
        	if (component.isValid() && state === 'SUCCESS') {
                var resp = response.getReturnValue();
                var articles = [];
                articles = JSON.parse(resp);
                
                component.set("v.notifications", articles);
                
                this.cycle(component);
        	}
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
		});
        $A.enqueueAction(action);
    },
    
    getCommunityName : function (component) {
        var action = component.get("c.getCommunityNameStr");

        action.setCallback(this, function(response) {
        	var state = response.getState();
            
        	if (component.isValid() && state === 'SUCCESS') {
                var resp = response.getReturnValue();
                var communityName = "";
                communityName = resp;
            	console.log(resp);
                component.set("v.commName", communityName);
        	}
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
		});
        $A.enqueueAction(action);
    },
})