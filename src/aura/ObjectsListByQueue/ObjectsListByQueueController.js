({
    doInit : function(component, event, helper) {
        // Get all groupnames belongs to logged in User
        helper.getUserGroups(component, event, helper);
        // Commented below method which get all the list of incidents. Now calling inside from helper.getUserGroups()
        //helper.getTableResult(component, event, helper,null);
        // Get all filter list values for Technology, SLA and Current Status
        helper.getFilterLists(component, event, helper);
    },
	// this function automatic call when button in data table is clicked
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
		if(action.name == 'open_incident'){
			helper.open_incident(cmp,row);
		}
    },
	
	updateColumnSorting: function(cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);
        cmp.set("v.Spinner",true);
        //helper.sortData(cmp, event, helper, fieldName, sortDirection);
        var sortString  = " ORDER BY "+fieldName+ " "+sortDirection;
        helper.getTableResult(cmp, event, helper,sortString);
    },
    
    next: function(component, event, helper) {
        //debugger;
        var sObjectList = component.get("v.lstRecords");
        var end = parseInt(component.get("v.endPage"));
        var start = parseInt(component.get("v.startPage"));
        var pageSize = parseInt(component.get("v.rowsPerPage"));
        var PagList = [];
        var counter = 0;
        for (var i = end + 1; i < end + pageSize + 1; i++) {
            if (sObjectList.length > i) {
                PagList.push(sObjectList[i]);
            }
            counter++;
        }
        start = start + counter;
        end = end + counter;
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.PaginationList', PagList);
    },
    previous: function(component, event, helper) {
        var sObjectList = component.get("v.lstRecords");
        var end = parseInt(component.get("v.endPage"));
        var start = parseInt(component.get("v.startPage"));
        var pageSize = parseInt(component.get("v.rowsPerPage"));
        var PagList = [];
        var counter = 0;
        for (var i = start - pageSize; i < start; i++) {
            if (i > -1) {
                PagList.push(sObjectList[i]);
                counter++;
            } else {
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.PaginationList', PagList);
    },
    // get all selected Technology values
    handleSelectTech : function(component, event, helper) {
    	component.set("v.selectedTech", event.getParam("values"));
        var isChange = event.getParam("isChangeValue");
        if(isChange)
        	helper.prepareFilters(component, event, helper);
	},
    // get all selected Queue values
    handleSelectQueue : function(component, event, helper) {
    	component.set("v.selectedQueue", event.getParam("values"));
		var isChange = event.getParam("isChangeValue");
        if(isChange)
			helper.prepareFilters(component, event, helper);
	},
    // get all selected SLA values
 	handleSelectSLA : function(component, event, helper) {
    	component.set("v.selectedSLA", event.getParam("values"));
        var isChange = event.getParam("isChangeValue");
        if(isChange)
        	helper.prepareFilters(component, event, helper);
	},
    // get all selected current status values
	handleSelectStatus : function(component, event, helper) {
    	component.set("v.selectedStatus", event.getParam("values"));
		var isChange = event.getParam("isChangeValue");
        if(isChange)
			helper.prepareFilters(component, event, helper);
        
	},
    // Method when any date gets select under SubmitFrom or SubmitTo
    setDateRange : function(component, event, helper) {
    	helper.prepareFilters(component, event, helper);
    }

	
})