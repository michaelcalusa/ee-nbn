({
    // method to get all list of incidents.
	getTableResult: function(component, event, helper,sortStr) {
        
        var queue = component.get("v.selectedQueue");
        var tech = component.get("v.selectedTech");
        var sla = component.get("v.selectedSLA");
        var status = component.get("v.selectedStatus");
        var submitFrom = component.get("v.submittedFrom");
        var submitTo = component.get("v.submittedTo");
        queue = (queue.length > 0 && queue[0] != 'All')?queue:component.get("v.lstQueue");
        //queue = component.get("v.lstQueue");
        var action = component.get("c.getIncidentsFromLC");
		//CUSTSA-28602 updated by RM - Added an extra param for sorting
		action.setParams({
            componentName : component.get("v.componentName"),
            sObjectName : component.get("v.strObject"),
            sortStr : sortStr,
            lstQueue : queue,
            lstTech : component.get("v.selectedTech"),
            lstSLA : component.get("v.selectedSLA"),
            lstStatus : component.get("v.selectedStatus"),
            dateFrom : submitFrom,
            dateTo : submitTo
        });
		action.setCallback(this, function(response) { 
			var state = response.getState();
            if (state === 'SUCCESS') {
				var resSobject = response.getReturnValue().lstSobjectData;
				// Update Columns
                var mycolumns = response.getReturnValue().lstColumns;
                var columnsUpdated = [];
                for (var singlekey in mycolumns) {
                    var arrayOfCols = {};
                    arrayOfCols['fieldName'] = mycolumns[singlekey].fieldName;
                    arrayOfCols['label'] = mycolumns[singlekey].label;
                    if(mycolumns[singlekey].fieldName == "Reported_Date__c" || mycolumns[singlekey].fieldName == "CurrentslaDueDateCalc__c" 
                    || mycolumns[singlekey].fieldName == "LastModifiedDate"){
                        arrayOfCols['sortable'] = true;
                    }else{
                        arrayOfCols['sortable'] = false;
                    }
                    arrayOfCols['type'] = mycolumns[singlekey].type;
                    arrayOfCols['initialWidth'] = component.get("v.intIntialColumnWidth");
                    
                    if(mycolumns[singlekey].fieldName == 'Incident_Number__c'){
                        arrayOfCols['type'] = 'button';
                        arrayOfCols['initialWidth'] = 158;
                        arrayOfCols['fieldName'] = 'Id';
                        arrayOfCols['typeAttributes'] =  { label: { fieldName: 'Incident_Number__c' }, name:'open_incident', title: 'Click here to open in a new tab', disabled: {fieldName: 'disabled_fieldName'}, class: {fieldName: 'class_fieldName'} };
                    }
                    columnsUpdated.push(arrayOfCols);
                }
				
				// Update data
                for (var i = 0; i < resSobject.length; i++) {
                    if (!$A.util.isUndefined(resSobject[i])) {
                      // START CUSTSA-28210 - Upadate SLA due date and add LastModifiedDate format.
                        if (resSobject[i].CurrentslaDueDateCalc__c){
                            resSobject[i].CurrentslaDueDateCalc__c = $A.localizationService.formatDate(resSobject[i].CurrentslaDueDateCalc__c, "DD/MM/YYYY hh:mm a");
                        }
                        if (resSobject[i].LastModifiedDate){
                            resSobject[i].LastModifiedDate = $A.localizationService.formatDate(resSobject[i].LastModifiedDate, "DD/MM/YYYY hh:mm a");
                        }
                        if (resSobject[i].Reported_Date__c){
                            resSobject[i].Reported_Date__c = $A.localizationService.formatDate(resSobject[i].Reported_Date__c, "DD/MM/YYYY hh:mm a");
                        }
                        //Current_Status__c
                        if (resSobject[i].Current_Status__c && resSobject[i].Current_Status__c == 'In Progress'){
                            resSobject[i].Current_Status__c = 'Incident Accepted';
                        }
                     // END
                        if (resSobject[i].Id){
                            //resSobject[i].Id = resSobject[i].Id;
                            resSobject[i].disabled_fieldName = false;
                            resSobject[i].class_fieldName = 'IncidentHyperLink';
                        }
                    }
                }
				component.set("v.mycolumns", columnsUpdated);
                component.set("v.lstRecords", resSobject);
                component.set("v.totalSize", resSobject.length);
				//set datatable
                helper.setDataTable(component, event, helper);
			} else if (state === 'ERROR') {
                var errors = response.getError();
                console.log("Error message from getTableResult.js: " + errors[0].message);
                component.set("v.Spinner",false);
            }
		});
        $A.enqueueAction(action);
	},
	setDataTable : function (component, event, helper){
        var pageSize = parseInt(component.get("v.rowsPerPage"));
        var records = component.get("v.lstRecords");
        component.set("v.startPage", 0);                
        component.set("v.endPage", pageSize - 1);		
        var PagList = [];
        for ( var i=0; i< pageSize; i++ ) {
            if ( records.length > i )
                PagList.push(records[i]);    
        }
        component.set('v.PaginationList', PagList);
        component.set("v.Spinner",false);
    },
	sortData: function(component,event,helper, fieldName, sortDirection) {
        var data = component.get("v.lstRecords");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.lstRecords", data);
        helper.setDataTable(component,event,helper);
    },
    
    sortBy: function(field, reverse, primer) {
        var key = primer ?
            function(x) {
                return primer(x[field])
            } :
        function(x) {
            return x[field]
        };
        reverse = !reverse ? 1 : -1;
        return function(a, b) {
            return a = key(a)?key(a):'', b = key(b)?key(b):'', reverse * ((a > b) - (b > a));
        }
    },
	open_incident : function(component, row) {  
        var workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then((res =>{
            if(res){
            	workspaceAPI.openTab({
                    pageReference: {
                        "type": "standard__recordPage",
                        "attributes": {
                            "recordId":row.Id,
                            "actionName":"view"
                        },
                        "state": {}
                    },
                    focus: true
                }).then(function(response) {
                    workspaceAPI.getTabInfo({
                        tabId: response
                    }).then(function(tabInfo) {
                        console.log("The recordId for this tab is: " + tabInfo.recordId);
                    });
                });
        	}
            else
			{
               var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                  "recordId": row.Id
                });
                navEvt.fire();                                 
             }
        }))
        .catch(function(error) {
            console.log(error);
        });
    },
    // Static list of all possible values for SLA, Technology and Current status.
    getFilterLists: function(component, event, helper) {
        
        var slaArray = ["PRI-Standard SLA",
            				"Enhanced_4_24x7","Enhanced_4_7am_9pm","Enhanced_6_24x7","Enhanced_6_7am_9pm","Enhanced_8_24x7","Enhanced_8_7am_9pm","Enhanced_12_24x7","PRI-Enhanced-12 SLA"];
        var statusArray = ["Awaiting Technician","Closed","Incident Accepted","New Incident", "On Hold","Pending","RSP Responded","Resolved",
            				"Resolution Rejected","Tech Offsite","Tech Onsite"];
        var techTypeArray = ["NCAS-FTTN","NCAS-FTTB","NHAS"];
        
        component.set("v.lstSLA", slaArray);
        component.set("v.lstTech", techTypeArray);
        component.set("v.lstStatus", statusArray);
        // Set max date to current date under Submittedfrom and SubmitedTo date inputs.
       	var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
		component.set("v.maxDate", today)

    },
    // Get all the associated queue list for logged in user.
	getUserGroups: function(component, event, helper) {
		var action = component.get("c.getUserGroupsFromLC");
		action.setCallback(this, function(response) { 
		var state = response.getState();
		if (state === 'SUCCESS') {
			var queueList = response.getReturnValue();
			if(queueList && queueList.length > 0){
				component.set("v.lstQueue",queueList);
				console.log('queues are -' + component.get("v.lstQueue"));
				component.set("v.isHideFilterSection",false);
			}
			else{
				component.set("v.isHideFilterSection",true);
			}
			this.getTableResult(component, event, helper, null);
		}
	});
	$A.enqueueAction(action);
	},
    // Method to call updated filtered incidents.    
    prepareFilters : function(component, event, helper) {
    	this.getTableResult(component, event, helper,null);
        component.set("v.Spinner",true);
    }
})