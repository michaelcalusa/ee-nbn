({
    initialize: function(component, event, helper) {
        var action = component.get("c.returnOrders");
		action.setParams({
            "caseid":  component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if(storeResponse != null){
                    component.set("v.ordrRecords", storeResponse);
                    var ordrList = component.get("v.ordrRecords");
                    component.set("v.ordrListSize",ordrList.length);
                    var ordrMap = new Map();
                    for(var i = 0, size = ordrList.length; i < size ; i++){
                        ordrMap.set(ordrList[i].Order_ID__c,ordrList[i].Id);
                    }
                    component.set("v.orderMap", ordrMap);
                    if(ordrList.length === 0){
                        var thead = component.find("tableheader");
                        $A.util.addClass(thead,'slds-hide');
                    }
                    //component.set("v.showNoRec", false);
                    component.set("v.showRec", true);
                }else{
                    //component.set("v.showNoRec", true);
                    component.set("v.showRec", false);
                }
            }else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        });
        $A.enqueueAction(action);
    },
    
    
    
    
    
    handleClick: function (component, event, helper) {
        var currOrdr = event.currentTarget;
        var ordrMap = component.get("v.orderMap");
        var ordrUrl = '#/sObject/' + ordrMap.get(currOrdr.title) + '/view';
        var workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then((res =>{
            if(res){
				workspaceAPI.getEnclosingTabId().then(function(tabId) {
                    workspaceAPI.openTab({
                        url: ordrUrl,
                        focus: true
                    });
                });
            }
            else{
            	var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                  "url": ordrUrl
                });
                urlEvent.fire();
        	}
        }))
        .catch(function(error) {
            console.log(error);
        });
    },
    
    
    createRecord: function(component, event, helper) {
      var createRecordEvent = $A.get("e.force:createRecord");      
      createRecordEvent.setParams({
         "entityApiName": "Customer_Connections_Order__c",
      });
      createRecordEvent.fire();
   },

})