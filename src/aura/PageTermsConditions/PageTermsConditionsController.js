({
    onload : function(component, event, helper) {   
        console.log(window.navigator.userAgent);
        if (window.navigator.userAgent.includes("Trident") || window.navigator.userAgent.includes("MSIE")) {
            console.log('IE BROWSER');
            component.set("v.displayPageTerms",false);           
        }
        else{
            console.log('Other Browser');
            component.set("v.displayPageTerms",true);
        }
    },
    
    showWebForm : function(component, event, helper) {
        console.log('inside function showWebform');
        var checkConfirmation = document.getElementById("tc-confirmation-checkbox");
        console.log('checkConfirmation--'+checkConfirmation);
        var checkTermsAndConditions = document.getElementById("tc-termsandconditions-checkbox");
        console.log('checkTermsAndConditions---'+checkTermsAndConditions);
        var errorConfirmation = document.getElementById("tc-confirmation-error");
        console.log('errorConfirmation---'+errorConfirmation);
        var errorTermsAndConditions = document.getElementById("tc-termsandconditions-error");
        console.log('errorTermsAndConditions---'+errorTermsAndConditions);
        if(checkConfirmation.checked && checkTermsAndConditions.checked){
            //var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
            var action2 = component.get("c.generateUniqueApplicationId");           
            action2.setParams({
                "uniqueCodelength" : '20'
            });
            action2.setCallback(this, function(response){
                if(response.getState() == 'SUCCESS'){
                    var wrapInstanceTemp = component.get("v.newDevApplicantWrapperInstance");
                    wrapInstanceTemp.otherAppDtlWrapper.applicationReferenceNumber = response.getReturnValue();
                    component.set("v.newDevApplicantWrapperInstance",wrapInstanceTemp); 
                    var action1 = component.get("c.saveNewApplicant");                    
                    action1.setParams({
                        "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance"))
                    });
                    action1.setCallback(this, function(response){
                        if(response.getState() == 'SUCCESS'){
                            var wrapInstanceTemp = component.get("v.newDevApplicantWrapperInstance");
                            wrapInstanceTemp.newDevApplicantId = response.getReturnValue();
                            component.set("v.newDevApplicantWrapperInstance",wrapInstanceTemp);
                            // fire save event
                            var saveEvent = component.getEvent("saveRecEvent");
                            saveEvent.setParams({
                                "wrapperInstance" : component.get("v.newDevApplicantWrapperInstance"),
                                "componentSaved" : 'pageTerms'
                            });
                            saveEvent.fire();
                            
                        }
                        else{
                            console.log(response.getState()+' message is '+response.getError()[0].message);
                        }
                    });
                    $A.enqueueAction(action1);
                    
                    
                }
                else{
                    console.log(response.getState()+' message is '+response.getError()[0].message);
                }
            });
            $A.enqueueAction(action2);
            
        }
        else{            
            if(!checkConfirmation.checked && checkTermsAndConditions.checked){
                $A.util.addClass(errorConfirmation,'help-block-error');
                $A.util.removeClass(errorTermsAndConditions,'help-block-error');
            }
            else if(!checkTermsAndConditions.checked && checkConfirmation.checked)
            {
                $A.util.addClass(errorTermsAndConditions,'help-block-error');
                $A.util.removeClass(errorConfirmation,'help-block-error');
            } 
                else
                {
                    $A.util.addClass(errorConfirmation,'help-block-error');
                    $A.util.addClass(errorTermsAndConditions,'help-block-error');
                }
        } 
    }
})