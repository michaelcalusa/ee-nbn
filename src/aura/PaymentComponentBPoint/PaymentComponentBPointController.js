({
	doInitialise : function(component, event, helper) {
        var isSandbox = false;
        var actionUtil = component.get("c.runningInASandbox");
        actionUtil.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                isSandbox = response.getReturnValue(); 
                component.set("v.isSandbox",isSandbox);
            }
            console.log(component.get("v.Context"));
            if(component.get("v.Context") === "NewDev"){
                var action = component.get("c.getAuthKeywithAmount");
                action.setParams({"Amount": component.get("v.Amount"), "ApplicationId": component.get("v.applicationId")});
            }
            else{
                var action = component.get("c.getAuthKey");
            	action.setParams({ "PriceItemName": component.get("v.priceItemName"), "ApplicationID": component.get("v.applicationId") });
            }
            action.setCallback( this, function(response) { 
                
                var state = response.getState();
                //Start IF
                if (state === "SUCCESS") {
                    //Start new handling
                    var responseFromServer = JSON.parse(response.getReturnValue());
                    if(responseFromServer.isError == false){
                        var sucessResponse = responseFromServer.sucessResponse;
                        component.set("v.bpointIframeURL", sucessResponse.iframeUrl);
                        component.set("v.paymentAmount",sucessResponse.amount);
                        component.set("v.toggleHideClass","slds-show");
                        component.set("v.showIFrame",true);
                        component.set("v.authKey",sucessResponse.authKey);
                        
                        component.set("v.showbpointRenderErrorSection",false);
                        component.set("v.showbpointMainSection",true);
                        component.set("v.showbpointPaymentErrorSection",false);
                        
                        //Fire event paymentComponentResponse with sucess results
                        helper.fireevtPaymentComponentResponse(component,sucessResponse.Sucess.transactionName,sucessResponse.Sucess.status);
                    }
                    else if(responseFromServer.isError){
                        var errorResponse = responseFromServer.errorResponse;
                        console.log("Error getting AuthKey - " + errorResponse.Error.description);
                        //alert("Error getting AuthKey - " + errorResponse.Error.description);
                        //Todo: fire event paymentComponentResponse with error results
                        
						var errorMessages="";
                        if(errorResponse.errorMessages && errorResponse.errorMessages){
                            //component.set("v.retry", errorResponse.errorMessages.retry);
                            if(errorResponse.errorMessages.messages){
                                for(var i=0;i<errorResponse.errorMessages.messages.length;i++){
                                    if(errorMessages === ""){
                                        errorMessages = errorResponse.errorMessages.messages[i];
                                    }
                                    else{
                                        errorMessages = errorMessages + "\n"  +  errorResponse.errorMessages.messages[i];
                                    }
                                }
                            }
                            else{
                            	errorMessages = component.get("v.unknownError");
                        	}
                        }
                            
                        component.set("v.bpointRenderErrorMessage",errorMessages);
                        component.set("v.showbpointRenderErrorSection",true);
                        component.set("v.showbpointMainSection",false);
                        component.set("v.showbpointPaymentErrorSection",false);
                        //Fire event paymentComponentResponse with error results
                        helper.fireevtPaymentComponentResponse(component,errorResponse.Error.transactionName,errorResponse.Error.status);
                    }
                    //End new handling
                    
                }
                //End IF
                else if (state === "ERROR"){
                    var errors = response.getError();
                    var errorConcat = "";
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " +
                                        errors[0].message);
                            errorConcat = errorConcat + errors[0].message;
                            
                        }
                        //alert("errorConcat - " + errorConcat);
                        component.set("v.bpointRenderErrorMessage",component.get("v.unknownError"));
                        component.set("v.showbpointRenderErrorSection",true);
                        component.set("v.showbpointMainSection",false);
                        component.set("v.showbpointPaymentErrorSection",false);
                    }//End IF 
                    else {
                        console.log("Unknown error");
                        component.set("v.bpointRenderErrorMessage",component.get("v.unknownError"));
                        component.set("v.showbpointRenderErrorSection",true);
                        component.set("v.showbpointMainSection",false);
                        component.set("v.showbpointPaymentErrorSection",false);
                    }
                    //End If Else
                    //Fire event paymentComponentResponse with unknown error results
                    helper.fireevtPaymentComponentResponse(component,"renderbpointiframe","error");
                }
                //End If Else
            });
            $A.enqueueAction(action);
        });
    	$A.enqueueAction(actionUtil);
        

	},
    doSubmitPayment : function(component, event, helper) {
        component.set("v.spinnerShow", true);
		var isSandbox = component.get("v.isSandbox");
		var action = component.get("c.submitPayment");
        action.setParams({"authKey":component.get("v.authKey"),"ApplicationID": component.get("v.applicationId") });
        action.setCallback( this, function(response) { 
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseFromServer = JSON.parse(response.getReturnValue());
                if(responseFromServer.isError == false){
                    component.set("v.showIFrame",false);
                    component.set("v.showbpointRenderErrorSection",false);
                    component.set("v.showbpointMainSection",false);
                    component.set("v.showbpointPaymentErrorSection",false);
                    var sucessResponse = responseFromServer.sucessResponse;

                    if(isSandbox){
                        //component.set("v.paymentResponseMessage",JSON.stringify(sucessResponse.tResponse.APIResponse) +"-"+ JSON.stringify(sucessResponse.tResponse.TxnResp));
                        component.set("v.textColorCSS","black");
                        component.set("v.paymentResponseMessage","API Response -" + JSON.stringify(sucessResponse.tResponse.APIResponse));   
                    }
                    
                    //Fire event paymentComponentResponse with sucess results
                    //alert(JSON.stringify(sucessResponse.tResponse));
                    helper.fireevtPaymentComponentResponsewithReceiptNumber(component,sucessResponse.Sucess.transactionName,sucessResponse.Sucess.status,sucessResponse.tResponse.TxnResp.ReceiptNumber);
                }
                else if(responseFromServer.isError){
                    var errorResponse = responseFromServer.errorResponse;
                    component.set("v.textColorCSS","red");
                    component.set("v.paymentResponseMessage",JSON.stringify(responseFromServer.errorResponse));
                    
                    var errorMessages="";
                    if(errorResponse.errorMessages && errorResponse.errorMessages){
                        //component.set("v.retry", errorResponse.errorMessages.retry);
                        if(errorResponse.errorMessages.messages){
                            for(var i=0;i<errorResponse.errorMessages.messages.length;i++){
                                if(errorMessages === ""){
                                    errorMessages = errorResponse.errorMessages.messages[i];
                                }
                                else{
                                    errorMessages = errorMessages + "\n"  +  errorResponse.errorMessages.messages[i];
                                }
                            }
                        }
                        else{
                            errorMessages = component.get("v.unknownError");
                        }
                        //alert("Error Messages - " + errorMessages);
                    }
                            
                    component.set("v.bpointPaymentErrorMessage",errorMessages);
                    
                    component.set("v.showbpointRenderErrorSection",false);
                    component.set("v.showbpointMainSection",false);
                    component.set("v.showbpointPaymentErrorSection",true);                    
                    
                    helper.fireevtPaymentComponentResponse(component,responseFromServer.errorResponse.Error.transactionName,responseFromServer.errorResponse.Error.status);
                }
            }
            else if (state === "ERROR"){
                var errors = response.getError();
                var errorConcat = "";
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                        errorConcat = errorConcat + errors[0].message;
                        
                    }
                    //alert("errorConcat - " + errorConcat);
                    component.set("v.bpointPaymentErrorMessage",component.get("v.unknownError"));
                    component.set("v.showbpointRenderErrorSection",false);
                    component.set("v.showbpointMainSection",false);
                    component.set("v.showbpointPaymentErrorSection",true);                    
                }//End IF 
                else {
                    console.log("Unknown error");
                    component.set("v.bpointPaymentErrorMessage",component.get("v.unknownError"));
                    component.set("v.showbpointRenderErrorSection",false);
                    component.set("v.showbpointMainSection",false);
                    component.set("v.showbpointPaymentErrorSection",true);                    
                }
                //End If Else
                
                //Fire event paymentComponentResponse with unknown error results
                helper.fireevtPaymentComponentResponse(component,"submitpayment","error");
            }
				component.set("v.spinnerShow", false);  
        	});
        
        $A.enqueueAction(action);
    	
	},
    
    doReload : function(component, event, helper) {
        
            var tranEvent = $A.get("e.c:invokePaymentComponent");
            //tranEvent.setParams({"invokePaymentComponentParams":{Reload: 'Y'}});
        	tranEvent.setParams({"invokePaymentComponentParams":{applicationId: component.get("v.applicationId"),priceItemName: component.get("v.priceItemName"), Context: component.get("v.Context"), Amount: component.get("v.Amount")}});
            tranEvent.fire();
        	component.destroy();
    
    }
    ,
    
    doToggle : function(component, event, helper) {
        
           var overlay = component.find("overlay");
		   $A.util.toggleClass(overlay, "overlayShow");
    
    },
    
    showHelp : function(component, event, helper) {
        var helpText = component.find("helpText");
		$A.util.toggleClass(helpText, "helpShow");
    }
})