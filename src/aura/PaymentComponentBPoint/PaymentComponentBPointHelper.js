({
	fireevtPaymentComponentResponse : function(component, strtransactionname,strstatus) {
        //Intialize and fire event paymentComponentResponse with results of Payment Component Rendering or submit payment
        var paymentCevent = $A.get("e.c:paymentComponentResponse");
        var paramMap = component.get("v.paymentComponentResponseParams");
        paramMap['transactionName'] = strtransactionname;
        paramMap['status'] = strstatus;
        paymentCevent.setParams({
            "paymentComponentResponseParams" : paramMap
        });
        paymentCevent.fire();
        
        var isSandbox = component.get("v.isSandbox");
        if(isSandbox){
        	//alert("paymentComponentResponse Event Fired with Response(will be handled by online form) - " + JSON.stringify(paramMap));
        }
	},
    fireevtPaymentComponentResponsewithReceiptNumber : function(component, strtransactionname,strstatus,strReceiptNumber) {
        //Intialize and fire event paymentComponentResponse with results of Payment Component Rendering or submit payment
        var paymentCevent = $A.get("e.c:paymentComponentResponse");
        var paramMap = component.get("v.paymentComponentResponseParams");
        paramMap['transactionName'] = strtransactionname;
        paramMap['status'] = strstatus;
        paramMap['ReceiptNumber'] = strReceiptNumber;
        //alert('ReceiptNumber is -' + strReceiptNumber);
        paymentCevent.setParams({
            "paymentComponentResponseParams" : paramMap
        });
        paymentCevent.fire();
        
        var isSandbox = component.get("v.isSandbox");
        if(isSandbox){
        	//alert("paymentComponentResponse Event Fired with Response(will be handled by online form) - " + JSON.stringify(paramMap));
        }
	}
})