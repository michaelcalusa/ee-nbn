({	doInit : function(cmp, evt, helper){
    	var optionsEventMap = cmp.get("v.VFParams");
        console.log(cmp.get("v.premisecount"));
    	optionsEventMap['applicationreference'] = cmp.get("v.applicationreference");
    	optionsEventMap['priceitemname'] = cmp.get("v.priceitemname");
    	optionsEventMap['premisecount'] = cmp.get("v.premisecount");
    	optionsEventMap['Context'] = cmp.get("v.Context");
    	//console.log(optionsEventMap['premisecount']);
    	var paymentCmpwithOptionsEvent = $A.get("e.c:invokePaymentComponentwithOptionsEvent");
        paymentCmpwithOptionsEvent.setParams({"invokePaymentComponentwithOptionsparams": optionsEventMap});
        paymentCmpwithOptionsEvent.fire();
    },
    
  loadOptions : function(cmp, evt, helper) {
        if (cmp.get("v.optionscmp_loaded") === true) {
        	alert('Component already loaded!');
            evt.stopPropagation();
            cmp.set("v.optionscmp_displayed", true);
        } 
        else {
            var ctx = cmp.get("v.Context");
            var action = cmp.get("c.getAmountNewDevelopments");
            action.setParams({PriceItemName: cmp.get("v.priceitemname"), PremiseCount: cmp.get("v.premisecount").toString(), Context: cmp.get("v.Context")});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state === "SUCCESS") {
                    var totalAmount = response.getReturnValue();
                    cmp.set("v.Amount", totalAmount);
                    if(totalAmount != null){
                      $A.createComponent("c:PaymentOptions", 
                        {
                            "Amount" : totalAmount,
                            "Context" : ctx
                        }, 
                        function(optionscomponent, status, errorMessage){
                            //Add the new button to the body array
                            if (status === "SUCCESS") {
                                var body = cmp.get("v.body");
                                body.push(optionscomponent);
                                cmp.set("v.body", body);
                            }
                            else if (status === "INCOMPLETE") {
                                console.log("No response from server or client is offline.")
                            // Show offline error
                            }
                            else if (status === "ERROR") {
                                console.log("Error: " + errorMessage);
                                // Show error message
                            }
                        });
                    }
                }
            });
            cmp.set("v.optionscmp_displayed", true);
            cmp.set("v.optionscmp_loaded", true);
		}
      $A.enqueueAction(action);
	},
    
    
    handleComponentEvent: function(cmp, evt, helper) {
        var selection = evt.getParam("paymentoptionselected");
        //alert('You selected: ');
       if (selection == 'PaybyInvoice') {
           if(cmp.get("v.paymentcmp_loaded") == true){
               var pc = document.getElementById("BPointMainSection");
               if(pc != null) pc.style.display = "none";
           }
           if(cmp.get("v.invoicecmp_loaded") == true){
                var invoiceCmp = document.getElementById("invoiceCmp");
             	if(invoiceCmp != null) invoiceCmp.style.display = "block";                             
            }
           else{
	        $A.createComponent("c:InvoicePaymentCmp",
            {
                AppId: cmp.get("v.applicationreference")
	        },
	        function(invoiceCmp, status, errorMessage){
	              if(status === "SUCCESS"){
	            	  var body = cmp.get("v.payment_details_widget");
	            	  body.push(invoiceCmp);
	            	  cmp.set("v.payment_details_widget", body);
                      cmp.set("v.invoicecmp_loaded", true);
	                }	
	              else if (status === "INCOMPLETE"){
	            	  	console.log("No response from server or client is offline.")
	              		}
	              else if (status === "ERROR") {
	                    console.log("Error: " + errorMessage);
	                    // Show error message
	              		}
	        
	        	});
       		}
        }
        else if (selection == 'PaybyCreditCard') {
            if(cmp.get("v.invoicecmp_loaded") == true){
             var invoiceCmp = document.getElementById("invoiceCmp");
             invoiceCmp.style.display = "none";
				console.log('new');               
            }
            if(cmp.get("v.paymentcmp_loaded") == true){
                    var pc = document.getElementById("BPointMainSection");
             		pc.style.display = "block";
            }             

            else {
                console.log('paymentwrapper');
                $A.createComponent("c:PaymentComponentWrapper", 
            {

   			}, 
            function(paymentCmpWrapper, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = cmp.get("v.payment_details_widget");
                    cmp.set("v.payment_details_widget", body);
                    body.push(paymentCmpWrapper);
                    cmp.set("v.payment_details_widget", body);
                    console.log('paymentwrapper2');
                    var paymentCmpEvent = $A.get("e.c:invokePaymentComponent");
                    var paymentCmpEventparamsMap = cmp.get("v.paymentCmpEventparams");
                    paymentCmpEventparamsMap['applicationId'] = cmp.get("v.applicationreference");
                    paymentCmpEventparamsMap['priceItemName'] = cmp.get("v.priceitemname");
                    paymentCmpEventparamsMap['Context'] = cmp.get("v.Context");
                    paymentCmpEventparamsMap['Amount'] = cmp.get("v.Amount");
                    paymentCmpEvent.setParams({"invokePaymentComponentParams": paymentCmpEventparamsMap});
                    paymentCmpEvent.fire();
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            });
        } 
            
        }
        evt.stopPropagation();
	},
     
    handleApplicationEvent: function(cmp, evt) {
        var paymentCmpStatus = evt.getParam("paymentComponentResponseParams");
        if(paymentCmpStatus['transactionName'] === "renderbpointiframe" && paymentCmpStatus['status'] === "success"){
            cmp.set("v.paymentcmp_loaded", true);
            //cmp.set("v.errorcmp_loaded", false);
            console.log('payment cmp loaded ' + cmp.get("v.paymentcmp_loaded"));                   
        }
        else if((paymentCmpStatus['transactionName'] === "paybyInvoice" || paymentCmpStatus['transactionName'] === "submitpayment") && paymentCmpStatus['status'] === "success"){
            console.log('==Hide Payment Options==');
            cmp.set("v.optionscmp_displayed", false);            
        }
        else if(paymentCmpStatus['transactionName'] === "submitpayment" && paymentCmpStatus['status'] === "error"){
          cmp.set("v.errorcmp_loaded", true);
        }
	}
})