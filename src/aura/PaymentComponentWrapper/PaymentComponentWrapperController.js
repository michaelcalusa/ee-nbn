({
	handleinvokePaymentComponent : function(component, event, helper) {
        paramsMap = event.getParam("invokePaymentComponentParams");
        var Bpointparams = {
				"priceItemName" : paramsMap["priceItemName"] ,
                "applicationId" : paramsMap["applicationId"] ,
                "Context" : ((paramsMap["Context"] != null) ? paramsMap["Context"]: '') ,
            	"Amount" : ((paramsMap["Amount"] != null) ? paramsMap["Amount"]: '')
            };
		$A.createComponent(
            "c:PaymentComponentBPoint",
            Bpointparams,
            function(paymentComp, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = component.get("v.body");
                    body.push(paymentComp);
                    component.set("v.body", body);
                    component.set("v.isSandbox",paymentComp.get("v.isSandbox"));
                    
                    //Fire event paymentComponentResponse with success results of Payment Component Rendering
                    //Todo check in Sprint 5, if we need to fire this event 
                    //as similar event is fired by PaymentComponentBpoint
                    helper.fireevtPaymentComponentResponse(component,"renderbpointiframe","success");
                }
                else if (status === "INCOMPLETE") {                    
                    console.log("Payment Component creation is incomplete.");
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    //Fire event paymentComponentResponse with error results of Payment Component Rendering
                    helper.fireevtPaymentComponentResponse(component,"renderbpointiframe","error");
                }
            }
        );
         
	},
    ///testinitPaymentComponent is not required after testing
    testinitPaymentComponent : function(component, event, helper) {
        var paramsMap = component.get("v.testParams");
        
        if(paramsMap !== null && paramsMap["applicationId"] !== null && paramsMap["applicationId"] === "testapplicationid")
        {
            $A.createComponent(
                "c:PaymentComponentBPoint",
                {
                    "priceItemName" : paramsMap["priceItemName"] ,
                    "applicationId" : paramsMap["applicationId"]
                },
                function(paymentComp, status, errorMessage){
                    //Add the new button to the body array
                    if (status === "SUCCESS") {
                        var body = component.get("v.body");
                        body.push(paymentComp);
                        component.set("v.body", body);
                        component.set("v.isSandbox",paymentComp.get("v.isSandbox"));
                        
                        //Fire event paymentComponentResponse with success results of Payment Component Rendering
                        helper.fireevtPaymentComponentResponse(component,"renderbpointiframe","success");
                    }
                    else if (status === "ERROR") {
                        console.log("Error: " + errorMessage);
                        //Fire event paymentComponentResponse with error results of Payment Component Rendering
                        helper.fireevtPaymentComponentResponse(component,"renderbpointiframe","error");
                    }
                }
        	);
        }
         
    }
})