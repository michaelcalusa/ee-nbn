({
	fireevtPaymentComponentResponse : function(component, strtransactionname,strstatus) {
        //Intialize and fire event paymentComponentResponse with results of Payment Component Rendering
        var paymentCevent = $A.get("e.c:paymentComponentResponse");
        var paramMap = component.get("v.paymentComponentResponseParams");
        paramMap['transactionName'] = strtransactionname;
        paramMap['status'] = strstatus;
        paymentCevent.setParams({
            "paymentComponentResponseParams" : paramMap
        });
        paymentCevent.fire();
        
        var isSandbox = component.get("v.isSandbox");
        if(isSandbox){
        }
	}
})