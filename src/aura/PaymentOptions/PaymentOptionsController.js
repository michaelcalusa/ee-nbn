({  
	doInit: function(cmp, event, helper) {
        if(cmp.get("v.Context") == 'NewDev'){
            console.log('component');
            cmp.set("v.isNewDev", true);
            cmp.set("v.showPaymentOptionText", false);
            if(cmp.get("v.Amount") && cmp.get("v.Context")){
                cmp.set("v.showNewDevPaymentOptionSection",true);
                cmp.set("v.showNewDevPaymentOptionErrorSection", false);
                //helper.helperMethod(cmp, event);
                //cmp.find("newdev").hidden = "false";
                //cmp.find("optionsRadios1").checked = "checked";
                var cmpEvent = cmp.getEvent("poEvent");
                console.log('Setting PaybyCreditcard selected by default');
                cmpEvent.setParams({
                    "paymentoptionselected" : "PaybyCreditCard"});
				cmpEvent.fire();
            }
            else{
                cmp.set("v.showNewDevPaymentOptionErrorSection", true);
                cmp.set("v.showNewDevPaymentOptionSection",false);
            }
            
        }
        //event.stopPropagation();
    },
    
	onSelectedPaymentOption : function(cmp, evt, helper) {
		var cmpEvent = cmp.getEvent("poEvent");
        //var selected = document.querySelector('input[name="selectedpaymentoption"]:checked').value;
        var selected = evt.target.value;
        console.log(selected);
        if(selected == "PaybyInvoice"){
            cmp.set("v.paymentOptionText", $A.get("$Label.c.New_Dev_Pay_By_Invoice_Text"));
            cmp.set("v.showPaymentOptionText", true);
            cmpEvent.setParams({
            "paymentoptionselected" : "PaybyInvoice"});            
			console.log(cmpEvent);            
        }
        if(selected == "PaybyCreditCard"){
            cmp.set("v.paymentOptionText", $A.get("$Label.c.New_Dev_Credit_Card_Text"));
            cmp.set("v.showPaymentOptionText", true);
            cmpEvent.setParams({
            "paymentoptionselected" : "PaybyCreditCard"});
        }
        cmpEvent.fire();
        //evt.stopPropagation();
	}
})