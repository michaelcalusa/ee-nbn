({
	onload : function(component, event, helper) {
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        console.log("Wrapper Variable in pit and pipe on load" + JSON.stringify(wrapInstance));
        if(wrapInstance.devDtlWrapper.numberOfEssentialServices == null){
            component.set("v.nonPremisesCount",0);
        }     
        else{
            component.set("v.nonPremisesCount",wrapInstance.devDtlWrapper.numberOfEssentialServices);
        }
        
        if(wrapInstance.pitAndPipeDetailsWrapper.isPitAndPipePrivate == null || wrapInstance.pitAndPipeDetailsWrapper.isPitAndPipePrivate == false){
            component.set("v.selPitAndPipeLocation",false);
        }
		else{
            component.set("v.selPitAndPipeLocation",true);
        }
        
        if(wrapInstance.pitAndPipeDetailsWrapper.isPitOrPathAlreadyDeveloped == null || wrapInstance.pitAndPipeDetailsWrapper.isPitOrPathAlreadyDeveloped == false){
            component.set("v.selPitAndPipeOrPathAlreadyDev",false);
        }
		else{
            component.set("v.selPitAndPipeOrPathAlreadyDev",true);
        }
        
        if(wrapInstance.pitAndPipeDetailsWrapper.areYouProducingDevLots == null || wrapInstance.pitAndPipeDetailsWrapper.areYouProducingDevLots == false){
            component.set("v.displayNoOfLots",false);
        }
		else{
            component.set("v.displayNoOfLots",true);
        }
        
        if(wrapInstance.pitAndPipeDetailsWrapper.areYouBuildNewRdInfra == null || wrapInstance.pitAndPipeDetailsWrapper.areYouBuildNewRdInfra == false){
            component.set("v.displayTrenchWorksStartDate",false);
        }
		else{
            component.set("v.displayTrenchWorksStartDate",true);
        }
        
        if(wrapInstance.pitAndPipeDetailsWrapper.anyExternalWorks == null || wrapInstance.pitAndPipeDetailsWrapper.anyExternalWorks == false){
            component.set("v.displayExternalWorksSection",false);
        }
		else{
            component.set("v.displayExternalWorksSection",true);
        }
        
        if(wrapInstance.pitAndPipeDetailsWrapper.externalRoad == null || wrapInstance.pitAndPipeDetailsWrapper.externalRoad == false){
            component.set("v.anyExternalRoad",false);
        }
		else{
            component.set("v.anyExternalRoad",true);
        }
        
        if(wrapInstance.pitAndPipeDetailsWrapper.utilityWorks == null || wrapInstance.pitAndPipeDetailsWrapper.utilityWorks == false){
            component.set("v.anyUtilityWorks",false);
        }
		else{
            component.set("v.anyUtilityWorks",true);
        }
        
        if(wrapInstance.pitAndPipeDetailsWrapper.otherCivilWorks == null || wrapInstance.pitAndPipeDetailsWrapper.otherCivilWorks == false){
            component.set("v.anyOtherCivilWorks",false);
        }
		else{
            component.set("v.anyOtherCivilWorks",true);
        }        
	},
    validateFields : function(component, event, helper) {
        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);       
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;
        var fieldId = event.currentTarget.id;       
        var fieldVal = document.getElementById(fieldId).value;       
        console.log(fieldVal);
        if (fieldVal == '') {
            if(fieldId == 'pnp-PreferredDevName') {
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill preferred reference name for this development.';
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
            else if(fieldId == 'pnp-NoOfStages') {
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill total number of stages for your development.';
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
            else if(fieldId == 'pnp-CurrentStage') {
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill current stage for this development.';
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
            else if(fieldId == 'pnp-PreferredStageName') {
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill preferred reference name for this stage.';
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
            else if(fieldId == 'pnp-NoOfPremisesInCurrentStage') {
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill estimated number of premises in this stage.';
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
            else if(fieldId == 'pnp-NoOfNonPremisesInCurrentStage') {
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill estimated number of non-premises in this stage.';
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
            
            else if(fieldId =='pnp-NoOfLots'){
                document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please fill number of lots';
                $A.util.addClass(errorSpanId,'help-block-error');
                $A.util.addClass(errorDivId,'has-error');
                $A.util.removeClass(errorDivId,'has-success');
            }
        } 
        else if(fieldVal==0 && (fieldId == 'pnp-NoOfStages' || fieldId == 'pnp-CurrentStage' || fieldId == 'pnp-NoOfPremisesInCurrentStage' || fieldId =='pnp-NoOfLots' )){
            document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a number 1 or greater';
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        else if(!fieldVal.match(/^[0-9]+$/) && (fieldId == 'pnp-NoOfStages' || fieldId == 'pnp-CurrentStage' || fieldId == 'pnp-NoOfPremisesInCurrentStage' || fieldId == 'pnp-NoOfNonPremisesInCurrentStage' || fieldId =='pnp-NoOfLots' || fieldId =='pnp-NoOfPremisesWithTele')){
            document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a number';
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
        }
        
            else {
            
            $A.util.removeClass(errorSpanId,'help-block-error');
            $A.util.removeClass(errorDivId,'has-error');
            $A.util.addClass(errorDivId,'has-success');
        }
        
    },
    checkpitAndPipePrivateLocation : function(component, event, helper) {
		var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(event.currentTarget.id == "PitAndPipeLocationYes"){                   
            wrapInstance.pitAndPipeDetailsWrapper.isPitAndPipePrivate = true;    
            $A.util.addClass(document.getElementById('PitAndPipeLocationYes'),'btn-primary');
            $A.util.removeClass(document.getElementById('PitAndPipeLocationNo'),'btn-primary');
            component.set("v.selPitAndPipeLocation",true);            
        }
        else if(event.currentTarget.id == "PitAndPipeLocationNo"){            
            wrapInstance.pitAndPipeDetailsWrapper.isPitAndPipePrivate = false;
            $A.util.addClass(document.getElementById('PitAndPipeLocationNo'),'btn-primary');
            $A.util.removeClass(document.getElementById('PitAndPipeLocationYes'),'btn-primary');            
            component.set("v.selPitAndPipeLocation",false);
        }	
        //component.set("v.newDevApplicantWrapperInstance",wrapInstance);
	},
    
    checkPitAndPipeOrPathAlreadyDev : function(component, event, helper){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(event.currentTarget.id == "PitAndPipeOrPathAlreadyDevYes"){                   
            wrapInstance.pitAndPipeDetailsWrapper.isPitOrPathAlreadyDeveloped = true;    
            $A.util.addClass(document.getElementById('PitAndPipeOrPathAlreadyDevYes'),'btn-primary');
            $A.util.removeClass(document.getElementById('PitAndPipeOrPathAlreadyDevNo'),'btn-primary');
            component.set("v.selPitAndPipeOrPathAlreadyDev",true);            
        }
        else if(event.currentTarget.id == "PitAndPipeOrPathAlreadyDevNo"){            
            wrapInstance.pitAndPipeDetailsWrapper.isPitOrPathAlreadyDeveloped = false;
            wrapInstance.pitAndPipeDetailsWrapper.pitOrPathCompletedDate = null;
            $A.util.addClass(document.getElementById('PitAndPipeOrPathAlreadyDevNo'),'btn-primary');
            $A.util.removeClass(document.getElementById('PitAndPipeOrPathAlreadyDevYes'),'btn-primary');            
            component.set("v.selPitAndPipeOrPathAlreadyDev",false);
        }	        
        //component.set("v.newDevApplicantWrapperInstance",wrapInstance);
    },
    
    checkDevLots : function(component, event, helper){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(event.currentTarget.id == "DevLotsYes"){                   
            wrapInstance.pitAndPipeDetailsWrapper.areYouProducingDevLots = true;    
            $A.util.addClass(document.getElementById('DevLotsYes'),'btn-primary');
            $A.util.removeClass(document.getElementById('DevLotsNo'),'btn-primary');
            component.set("v.displayNoOfLots",true);            
        }
        else if(event.currentTarget.id == "DevLotsNo"){            
            wrapInstance.pitAndPipeDetailsWrapper.areYouProducingDevLots = false;
            wrapInstance.pitAndPipeDetailsWrapper.noOfLots = null;
            $A.util.addClass(document.getElementById('DevLotsNo'),'btn-primary');
            $A.util.removeClass(document.getElementById('DevLotsYes'),'btn-primary');            
            component.set("v.displayNoOfLots",false);
        }	        
        //component.set("v.newDevApplicantWrapperInstance",wrapInstance);
    },

    checkNewRoadInfrastructure : function(component, event, helper){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(event.currentTarget.id == "NewRoadInfrastructureYes"){                   
            wrapInstance.pitAndPipeDetailsWrapper.areYouBuildNewRdInfra = true;    
            $A.util.addClass(document.getElementById('NewRoadInfrastructureYes'),'btn-primary');
            $A.util.removeClass(document.getElementById('NewRoadInfrastructureNo'),'btn-primary');
            component.set("v.displayTrenchWorksStartDate",true);            
        }
        else if(event.currentTarget.id == "NewRoadInfrastructureNo"){            
            wrapInstance.pitAndPipeDetailsWrapper.areYouBuildNewRdInfra = false;
            wrapInstance.pitAndPipeDetailsWrapper.estTrenchWorksStartDate = null;
            $A.util.addClass(document.getElementById('NewRoadInfrastructureNo'),'btn-primary');
            $A.util.removeClass(document.getElementById('NewRoadInfrastructureYes'),'btn-primary');            
            component.set("v.displayTrenchWorksStartDate",false);
        }	        
        //component.set("v.newDevApplicantWrapperInstance",wrapInstance);
    },
    
    checkAnyExternalWorkPlanned : function(component, event, helper){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(event.currentTarget.id == "AnyExternalWorkPlannedYes"){                   
            wrapInstance.pitAndPipeDetailsWrapper.anyExternalWorks = true;    
            $A.util.addClass(document.getElementById('AnyExternalWorkPlannedYes'),'btn-primary');
            $A.util.removeClass(document.getElementById('AnyExternalWorkPlannedNo'),'btn-primary');
            component.set("v.displayExternalWorksSection",true);            
        }
        else if(event.currentTarget.id == "AnyExternalWorkPlannedNo"){            
            wrapInstance.pitAndPipeDetailsWrapper.anyExternalWorks = false;
            wrapInstance.pitAndPipeDetailsWrapper.externalRoad = false;		
            wrapInstance.pitAndPipeDetailsWrapper.utilityWorks = false;		
            wrapInstance.pitAndPipeDetailsWrapper.otherCivilWorks = false;
            $A.util.addClass(document.getElementById('AnyExternalWorkPlannedNo'),'btn-primary');
            $A.util.removeClass(document.getElementById('AnyExternalWorkPlannedYes'),'btn-primary');            
            component.set("v.displayExternalWorksSection",false);
        }	        
        //component.set("v.newDevApplicantWrapperInstance",wrapInstance);
    },
    
    processDevelopmentDetails : function(component, event, helper){
        console.log('pit pipe details  '+JSON.stringify(component.get("v.newDevApplicantWrapperInstance")));
        component.set("v.displayRequiredErrorMsg",false);
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
    	console.log("Wrapper Variable" + JSON.stringify(wrapInstance));
        var div = document.getElementById("pnp-containerId");
        var subDiv = document.querySelectorAll('input, select');
        console.log('Dev Elements '+subDiv);
        console.log('Dev Elements '+JSON.stringify(subDiv));
        var myArray = [];
        for(var i = 0; i < subDiv.length; i++) {
            var elem = subDiv[i];
            if(elem.id.indexOf('pnp-') === 0) {
                myArray.push(elem.id);
                console.log('element id==>'+elem.id);
            }
        }
        //myArray.push('cpd-estimatedOccupancyDate');
        var errorCarier = [];
        for(var inputElementId=0; inputElementId<myArray.length; inputElementId++){
            var errorDivId = document.getElementById(myArray[inputElementId]+'-div');
            var errorSpanId = document.getElementById(myArray[inputElementId]+'-error'); 
            var fieldId = myArray[inputElementId];      
            var fieldVal = document.getElementById(fieldId).value;
            var err;
            if(fieldId != 'pnp-NoOfPremisesWithTele' && fieldId != 'pnp-NewRoadExternal' && fieldId != 'pnp-UtilityWorks' && fieldId != 'pnp-OtherCivilWorks'){
            	err = helper.validatehelper(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);    
            }  
            console.log("errorCarier$$"+errorCarier);
            console.log("errww"+err+' type '+typeof err);  
            
            errorCarier.push(err);
        }
        console.log('6t767&&&'+errorCarier.length);
        component.set('v.hasError', false);
        for(var i = 0; i < errorCarier.length; i++){
            if(errorCarier[i] == true){
                component.set('v.hasError', true);
            } 
        }
        var occupancyDate = component.get("v.newDevApplicantWrapperInstance").devDtlWrapper.estimatedOccupancydt;
        console.log('occupancyDate '+occupancyDate);
        var res = occupancyDate.split("-");
        
        if( occupancyDate == undefined || occupancyDate == ''){
            component.set('v.hasError', true);
        }
        else if(!((res.length == 3) && res[0].length == 4 && res[1].length == 2 && res[2].length == 2 && 
           !isNaN(res[0]) && !isNaN(res[1]) && !isNaN(res[2]) && res[1] <= 12 && res[2] <= 31)){
            component.set('v.hasError', true);
        }
        console.log(' lov '+component.get("v.hasError"));
        if(!component.get("v.hasError")){
            console.log("No Valdaition error");
            if(wrapInstance.devDtlWrapper.whatAreYouBuilding == 'Pit and Pipe'){
                console.log("inside dev name");
                var devName = document.getElementById('pnp-PreferredDevName');
                if(devName){
                    console.log("inside if dev name");
                    wrapInstance.pitAndPipeDetailsWrapper.preferredDevelopmentName = document.getElementById('pnp-PreferredDevName').value;    
                }    
            }
            else if(wrapInstance.devDtlWrapper.whatAreYouBuilding == 'Pathways Only'){
                wrapInstance.pitAndPipeDetailsWrapper.preferredDevelopmentName = '';
                wrapInstance.pitAndPipeDetailsWrapper.isPitAndPipePrivate = false;
            }
            console.log("dev name completed");
            var PCountForStage = document.getElementById('pnp-NoOfPremisesInCurrentStage').value;
            var eCountForStage = document.getElementById('pnp-NoOfNonPremisesInCurrentStage').value;
			wrapInstance.pitAndPipeDetailsWrapper.noOfStages = document.getElementById('pnp-NoOfStages').value;
            wrapInstance.pitAndPipeDetailsWrapper.currentStage = document.getElementById('pnp-CurrentStage').value;
            wrapInstance.pitAndPipeDetailsWrapper.preferredStageName = document.getElementById('pnp-PreferredStageName').value;            
            wrapInstance.pitAndPipeDetailsWrapper.noOfPremisesForStage = PCountForStage;
            wrapInstance.pitAndPipeDetailsWrapper.noOfNonPremisesForStage = eCountForStage;
            wrapInstance.pitAndPipeDetailsWrapper.totalNumberOfPremisesForStage = +PCountForStage + +eCountForStage;
            
            var noOfPremisesAlreadyDev = document.getElementById('pnp-NoOfPremisesWithTele');
            if(noOfPremisesAlreadyDev){
            	wrapInstance.pitAndPipeDetailsWrapper.noOfPremisesAlreadyDeveloped = document.getElementById('pnp-NoOfPremisesWithTele').value;    
            }
            console.log("noOfPremisesAlreadyDev completed");
			var pandpCompletedDt = document.getElementById('pnp-PitAndPipeCompletedDate');
            if(pandpCompletedDt){
            	wrapInstance.pitAndPipeDetailsWrapper.pitOrPathCompletedDate = document.getElementById('pnp-PitAndPipeCompletedDate').value;    
            }
            var noofLot = document.getElementById('pnp-NoOfLots');
            if(noofLot){
            	wrapInstance.pitAndPipeDetailsWrapper.noOfLots = document.getElementById('pnp-NoOfLots').value;    
            }
            var trenchWorkDate = document.getElementById('pnp-TrenchWorksStartDate');
            if(trenchWorkDate){
            	wrapInstance.pitAndPipeDetailsWrapper.estTrenchWorksStartDate = document.getElementById('pnp-TrenchWorksStartDate').value;    
            }
			var refNum = document.getElementById('cpd-councilRefNumber');
            if(refNum){
            	wrapInstance.devDtlWrapper.councilRefNo = document.getElementById('cpd-councilRefNumber').value;
            }
            if(document.getElementById('cpd-estimatedOccupancyDate')){
            	wrapInstance.devDtlWrapper.estimatedOccupancydt = document.getElementById('cpd-estimatedOccupancyDate').value;    
        	}            
            console.log("estTrenchWorksStartDate completed");
            var action1 = component.get("c.saveNewApplicant");
            console.log('DEVDETAILS>>>'+JSON.stringify(component.get("v.newDevApplicantWrapperInstance")));
            action1.setParams({
                "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance"))                
            });
            action1.setCallback(this, function(response) {
                console.log('Pit/Pathways Details Saved '+response.getState());
                if(response.getState() == 'SUCCESS'){
                    var saveEvent = component.getEvent("saveRecEvent");
                    saveEvent.setParams({
                        "wrapperInstance" : component.get("v.newDevApplicantWrapperInstance"),
                        "componentSaved" : 'developmentDetails'
                    });
                    saveEvent.fire();
                }
            });
            $A.enqueueAction(action1);
        }
        else{
            console.log('Pit/Pathways Validation Errors');
            component.set("v.displayRequiredErrorMsg",true);
        }
    },   
	
    checkExternalWorks : function(component, event, helper){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");                
        if(document.getElementById('pnp-NewRoadExternal').checked == true){
            wrapInstance.pitAndPipeDetailsWrapper.externalRoad = true;
        }
        else{
            wrapInstance.pitAndPipeDetailsWrapper.externalRoad = false;
        }        
        //component.set("v.newDevApplicantWrapperInstance",wrapInstance);
    },
    
	checkUtilityWorks : function(component, event, helper){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(document.getElementById('pnp-UtilityWorks').checked == true){
        	wrapInstance.pitAndPipeDetailsWrapper.utilityWorks = true;
        }
        else{
            wrapInstance.pitAndPipeDetailsWrapper.utilityWorks = false;
        }
        //component.set("v.newDevApplicantWrapperInstance",wrapInstance);
    },
    
    checkOtherCivilWorks : function(component, event, helper){
        var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
        if(document.getElementById('pnp-OtherCivilWorks').checked == true){
        	wrapInstance.pitAndPipeDetailsWrapper.otherCivilWorks = true;
        }
        else{
            wrapInstance.pitAndPipeDetailsWrapper.otherCivilWorks = false;
        }
        //component.set("v.newDevApplicantWrapperInstance",wrapInstance);
    }
    
})