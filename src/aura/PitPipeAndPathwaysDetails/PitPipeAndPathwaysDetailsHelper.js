({
	validatehelper : function(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal){
       var errorflag = false;
        console.log('$$%^^'+fieldVal);      
        if(fieldVal == '' && (fieldId != 'pnp-NewRoadExternal' || fieldId != 'pnp-UtilityWorks' || fieldId != 'pnp-OtherCivilWorks')){
            $A.util.addClass(errorSpanId,'help-block-error');               
            $A.util.addClass(errorDivId,'has-error');              
            $A.util.removeClass(errorDivId,'has-success');               
            errorflag = true; 
            console.log('errorflagb==>'+errorflag);
       }
       else if(fieldVal==0 && (fieldId == 'pnp-NoOfStages' || fieldId == 'pnp-CurrentStage' || fieldId == 'pnp-NoOfPremisesInCurrentStage' || fieldId =='pnp-NoOfLots' )){
           // document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a number 1 or greater';
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');            
            errorflag = true;
            console.log('errorflag0==>'+errorflag);
       }
       else if(!fieldVal.match(/^[0-9]+$/) && (fieldId == 'pnp-NoOfStages' || fieldId == 'pnp-CurrentStage' || fieldId == 'pnp-NoOfPremisesInCurrentStage' || fieldId == 'pnp-NoOfNonPremisesInCurrentStage' || fieldId =='pnp-NoOfLots' || fieldId =='pnp-NoOfPremisesWithTele')){
            //document.getElementById(event.currentTarget.dataset.errorspanid).innerText = 'Please enter a number';
            $A.util.addClass(errorSpanId,'help-block-error');
            $A.util.addClass(errorDivId,'has-error');
            $A.util.removeClass(errorDivId,'has-success');
            errorflag = true;
            console.log('errorflagsneg==>'+errorflag);
        } 
       else{
            $A.util.removeClass(errorSpanId,'help-block-error');               
            $A.util.removeClass(errorDivId,'has-error');              
            $A.util.addClass(errorDivId,'has-success');
            console.log('errorflagno==>'+errorflag);
        }
       console.log('errorflagrtn==>'+errorflag);
       return errorflag;
   }
})