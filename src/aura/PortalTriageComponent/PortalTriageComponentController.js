({
	doInit : function(component, event, helper) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var sURL = window.location.href;
        var sURLPart = sURL.substring(0,sURL.search('new-developments'));
        var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
        var sParamCode = [];
        var sParamId = [];
        var i;
		console.log(sURLVariables[0]);
        console.log(sURLVariables[1]);
        for (i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('='); //to split the key from the value.
            console.log(sParameterName[0]);
            console.log(sParameterName[1]);
            if (sParameterName[0] === 'portalCode') { 
                sParamCode[0] = 'portalCode';
                if (sParameterName[1] === undefined){ 
                    sParamCode[1] = 'Not found';
                }else{
                    sParamCode[1] = sParameterName[1];
                }
            }
            if (sParameterName[0] === 'did'){
                sParamId[0] = 'did';
                if (sParameterName[1] === undefined){ 
                    sParamId[1] = 'Not found';
                }else{
                    sParamId[1] = sParameterName[1];
                }
            }
        }
        component.set('v.portalCode',sParamCode[1]);
        var action = component.get("c.identifyContact");
        action.setParams({
            "portalUCode": component.get('v.portalCode')    
        });
        action.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
               	component.set("v.idContact",response.getReturnValue());
                if(component.get("v.idContact"))
                {
                    window.location = sURLPart+"login/";
                }
                else
                {
                   //window.location = sURLPart+"login/SelfRegister?portalcode="+sParamCode[1]+"&did="+sParamId[1];
                   window.location = sURLPart+"custom-register?portalcode="+sParamCode[1]+"&did="+sParamId[1];
                }
            }
            else {
                console.log('** Portal - Error ***');
            }
        });
        $A.enqueueAction(action);
    }
})