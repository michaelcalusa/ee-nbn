({
	doInit : function(component, event, helper) {
		var grannylicType = [];
        console.log('mapp1 '+grannylicType);
        grannylicType.push({"name" : "GrannyFlatLeft", "desc"  :$A.get('$Resource.DevelopmentDetailsGFL')});
        grannylicType.push({"name" : "GrannyFlatRight", "desc"  :$A.get('$Resource.DevelopmentDetailsGFR')});
        component.set("v.grannyLICType",grannylicType);
        var licType = [];
        console.log('mapp2 '+licType);
        licType.push({"name" : "LICLeft", "desc"  :$A.get('$Resource.DevelopmentDetailsLICLeft')});
        licType.push({"name" : "LICRight", "desc"  :$A.get('$Resource.DevelopmentDetailsLICRight')});
        licType.push({"name" : "SideDuplex", "desc"  :$A.get('$Resource.DevelopmentDetailsLICSD')});
        licType.push({"name" : "CentreDuplex", "desc"  :$A.get('$Resource.DevelopmentDetailsLICCD')});
        licType.push({"name" : "Cornerblock", "desc"  :$A.get('$Resource.DevelopmentDetailsLICCB')});
       component.set("v.licType",licType);
        
	},
    
    validateFields : function(component, event, helper){
        console.log('field validation');
        
        console.log('id ==>'+ event.currentTarget.id);
        
        var errorDivId = document.getElementById(event.currentTarget.dataset.errordivid);
        console.log('errordivid 1==>'+ event.currentTarget.dataset.errordivid);
        console.log('errordivid 2==>'+ errorDivId);
        var errorSpanId = document.getElementById(event.currentTarget.dataset.errorspanid);
        console.log('errordivid 3==>'+ event.currentTarget.dataset.errorspanid);
        console.log('errorSpanId 4==>'+ errorSpanId);
        var errorMsg = document.getElementById(event.currentTarget.dataset.errorspanid).innerText;
        
        var fieldId = event.currentTarget.id;       
        var fieldVal = document.getElementById(fieldId).value;
        
        helper.validatehelper(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);
    },
    
    grannylicLocationCheck : function(component, event, helper) {
		
        console.log('data value ==>'+ event.currentTarget.dataset.value);

        var togglebuttonId = event.currentTarget.id;
        
        if(togglebuttonId == 'pd-grannylicLocation-yes-toggleBtnId')
        {
            $A.util.addClass(document.getElementById('pd-grannylicLocation-yes-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById('pd-grannylicLocation-no-toggleBtnId'),'btn-primary');
            component.set('v.grannyLICCheck',true);
            console.log(component.get('v.grannyLICCheck'));
            
            
        }
        else if(togglebuttonId == 'pd-grannylicLocation-no-toggleBtnId')
        {
            $A.util.addClass(document.getElementById('pd-grannylicLocation-no-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById('pd-grannylicLocation-yes-toggleBtnId'),'btn-primary');
            component.set('v.grannyLICCheck',false);         
            console.log(component.get('v.grannyLICCheck'));
        }
	},
    
    licLocationCheck : function(component, event, helper) {
		
        console.log('data value ==>'+ event.currentTarget.dataset.value);

        var togglebuttonId = event.currentTarget.id;
        
        if(togglebuttonId == component.get('v.labelId')+component.get('v.premiseNum')+'pd-licLocation-yes-toggleBtnId')
        {
            $A.util.addClass(document.getElementById(component.get('v.labelId')+component.get('v.premiseNum')+'pd-licLocation-yes-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById(component.get('v.labelId')+component.get('v.premiseNum')+'pd-licLocation-no-toggleBtnId'),'btn-primary');
            component.set('v.licCheck',true);
            console.log(component.get('v.licCheck'));
            
            
        }
        else if(togglebuttonId == component.get('v.labelId')+component.get('v.premiseNum')+'pd-licLocation-no-toggleBtnId')
        {
            $A.util.addClass(document.getElementById(component.get('v.labelId')+component.get('v.premiseNum')+'pd-licLocation-no-toggleBtnId'),'btn-primary');
            $A.util.removeClass(document.getElementById(component.get('v.labelId')+component.get('v.premiseNum')+'pd-licLocation-yes-toggleBtnId'),'btn-primary');
            component.set('v.licCheck',false);         
            console.log(component.get('v.licCheck'));
        }
	},
    
    processPremiseDetails : function(component, event, helper){
        var wrapperInstance = event.getParam("wrapperInstance");
		console.log(JSON.stringify(wrapperInstance)); 
        console.log(JSON.stringify(component.get("v.newDevApplicantWrapperInstance")));
        //var div = document.getElementById(component.get('v.labelId')+component.get('v.premiseNum')+'pd-containerId');
        var subDiv = document.querySelectorAll('input, select');
        console.log('subdiv12345 '+JSON.stringify(subDiv));
        var myArray = [];
        
        console.log(div);
        console.log(document.getElementById("pd-containerId"));
             
        for(var i = 0; i < subDiv.length; i++) {
            var elem = subDiv[i];
            console.log(component.get('v.premiseNum'));
            if(elem.id.includes('Granny')) {
            myArray.push(elem.id); 
            }
        }
        console.log('myArray '+myArray);
        var errorCarier = [];
        for(var inputElementId=0; inputElementId<myArray.length; inputElementId++){
            console.log(myArray[inputElementId]);
            var errorDivId = document.getElementById(myArray[inputElementId]+'-div');
            console.log(errorDivId);
            var errorSpanId = document.getElementById(myArray[inputElementId]+'-error'); 
            console.log(errorSpanId);
            var fieldId = myArray[inputElementId];
            var fieldVal= document.getElementById(myArray[inputElementId]).value;
            
            console.log(' here these '+fieldId+' '+fieldVal);
            if(!(fieldId.includes('-unitnumber')))
            {
				var err = helper.validatehelper(component, event, helper, errorDivId, errorSpanId, fieldId, fieldVal);
            	errorCarier.push(err);                             
            }               
            
        }
        
        component.set('v.hasError', false);
        for(var i = 0; i < errorCarier.length; i++){
            if(errorCarier[i] == true){
                component.set('v.hasError', true);
            } 
        }
        if(!component.get("v.hasError"))
        {
            console.log('== processLICDetails ==inside if');
            var wrapInstance = component.get("v.newDevApplicantWrapperInstance");
            var premiseMap = [];
            console.log('wrapInstance---->'+wrapInstance);
            console.log('wrapInstance---->'+wrapInstance.premiseDtlWrapper);
            if(component.get('v.labelId')==='Granny')
            {
                premiseMap.push({"unitNumber" : document.getElementById(component.get('v.labelId')+'pd-unitnumber').value, "streetNumber"  :document.getElementById(component.get('v.labelId')+'pd-streetnumber').value
                                 ,"lotNumber" : document.getElementById(component.get('v.labelId')+'pd-lotnumber').value, "streetName"  :document.getElementById(component.get('v.labelId')+'pd-streetname').value, 
                                 "licType" :component.get('v.grannyLICTypes'), "premiseId" :wrapInstance.newDevApplicantId});
            }
            else
            {
                        	                                                                           
                    premiseMap.push({"unitNumber" : document.getElementById(component.get('v.labelId')+component.get('v.premiseNum')+'pd-unitnumber').value, "streetNumber"  :document.getElementById(component.get('v.labelId')+component.get('v.premiseNum')+'pd-streetnumber').value
                                     ,"lotNumber" : document.getElementById(component.get('v.labelId')+component.get('v.premiseNum')+'pd-lotnumber').value, "streetName"  :document.getElementById(component.get('v.labelId')+component.get('v.premiseNum')+'pd-streetname').value,
                                     "licType" :component.get('v.nGrannyLICTypes'), "premiseId" :wrapInstance.newDevApplicantId+'Premise-'+component.get('v.premiseNum')});
                
            }
            console.log('334455 '+JSON.stringify(premiseMap));
            component.set("v.newDevApplicantWrapperInstance",wrapInstance);
            
           	var action1 = component.get("c.savePremiseDetails");
            console.log('334455 '+JSON.stringify(component.get("v.newDevApplicantWrapperInstance")));
            action1.setParams({
                "wrapString" : JSON.stringify(component.get("v.newDevApplicantWrapperInstance")),
                                              "premiseDetails":JSON.stringify(premiseMap)
            });
        	/*action1.setCallback(this, function(response){
                console.log('statssss '+response.getState());
                if(response.getState() == 'SUCCESS'){
                    console.log('ret string '+response.getReturnValue());
                    var wrapInstanceTemp = component.get("v.newDevApplicantWrapperInstance");
                    //wrapInstanceTemp.newDevApplicantId = response.getReturnValue();
                    component.set("v.newDevApplicantWrapperInstance",wrapInstanceTemp);
                    var saveEvent = component.getEvent("saveRecEvent");
                    saveEvent.setParams({
                        "wrapperInstance" : component.get("v.newDevApplicantWrapperInstance"),
                        "componentSaved" : 'LICDetails'
                    });
                    saveEvent.fire();
                    PD
                    var savePdEvent = component.getEvent("saveRecPDEvent");
                    savePdEvent.setParams({
                        "wrapperInstance" : component.get("v.newDevApplicantWrapperInstance"),
                        "componentSaved" : 'LICDetails'
                    });
                    savePdEvent.fire();
                }
                else{
                    console.log(response.getState()+' message is '+response.getError()[0].message);
                }
            });*/
            $A.enqueueAction(action1);    
        }
        else{
            console.log('caught errors');
        }    
   
    },
    
    grannyLICRadioChange : function(component, event, helper){
        console.log(' i am here '+event.target.value+' comp '+component.get("v.grannyLICTypes"));
        component.set("v.grannyLICTypes",event.target.value);
        var gLicTypes = component.get("v.grannyLICType");
        var selectedValue = event.target.value;
        for (var i=0;i<gLicTypes.length;i++)
        {
            if(gLicTypes[i].name == selectedValue)
            {
                console.log('inside if'+document.getElementById(component.get('v.grannyLICTypes')+'granny'));
               $A.util.addClass(document.getElementById(component.get('v.grannyLICTypes')+'granny'),'LICSeleted'); 
            }
            else
            {
                console.log('inside else'+document.getElementById(gLicTypes[i].name+'granny'));
                $A.util.removeClass(document.getElementById(gLicTypes[i].name+'granny'),'LICSeleted'); 
            }
        }
        console.log(' i am here1 '+event.target.value+' comp '+component.get("v.grannyLICTypes"));
    },
    
    nGLICRadioChange : function(component, event, helper){
        console.log(' i am here '+event.target.value+' comp '+component.get("v.nGrannyLICTypes"));
        component.set("v.nGrannyLICTypes",event.target.value);
        var selectedValue = event.target.value;
        var licTypes = component.get("v.licType");
        console.log(JSON.stringify(licTypes));
        console.log(document.getElementById(component.get('v.nGrannyLICTypes')));
        console.log(component.get('v.nGrannyLICTypes')+component.get('v.premiseNum'));
        for (var i=0;i<licTypes.length;i++)
        {
            if(licTypes[i].name == selectedValue)
            {
                console.log('inside if');
               $A.util.addClass(document.getElementById(component.get('v.nGrannyLICTypes')+component.get('v.premiseNum')),'LICSeleted'); 
            }
            else
            {
                $A.util.removeClass(document.getElementById(licTypes[i].name+component.get('v.premiseNum')),'LICSeleted'); 
            }
        }
        
        console.log(' i am here1 '+event.target.value+' comp '+component.get("v.nGrannyLICTypes"));
    },
})