({
    handleApplicationEvent : function(component, event, helper){
        if(event.getParam("recordId") === component.get("v.recordId")) {
            var locId = event.getParam('locID');
            component.set("v.locId", locId);
            helper.docall(component);
        }
    },
    handleCurrentIncidentRecord: function(component, event, helper) {
        if(component.get('v.recordId') == event.getParam("recordId")) {
            var incidentData = event.getParam("currentIncidentRecord");
            if(incidentData.inm.Repeat_Incident__c && incidentData.inm.Repeat_Incident__c.toUpperCase() == "REPEAT")
            {
               component.set("v.repeatIncident", true); 
            }
            else
            {
                component.set("v.repeatIncident", false);
            }
        }
    },
    handleApplicationEventFired : function(cmp, event) {
        var context = event.getParam("parameter");
        cmp.set("v.mostRecentEvent", context);
        console.log('context---'+context);
        var numApplicationEventsHandled = parseInt(cmp.get("v.numApplicationEventsHandled")) + 1;
        cmp.set("v.numApplicationEventsHandled", numApplicationEventsHandled);
    },
    handleErrorEvent : function(component, event, helper){
        if(event.getParam('recordId') == component.get('v.recordId') && event.getParam('errorKey') == 'RejectedIncident'){
            component.set('v.isRejectedIncident',true);
        }
    }
})