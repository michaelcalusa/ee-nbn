({
    docall : function(component) {
        var action = component.get("c.get_previousIncedentCounts");
        action.setParams({
            "Daycount":component.get("v.Dayscount"),
            "incidentId":component.get("v.recordId"),
            "locId":component.get("v.locId")
        });
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var result = a.getReturnValue();
                if(result){
                    console.log('result ',result);
                    if(result.errorDetails){
                        component.set("v.errorCode", '10000');
                		component.set("v.errorDetails", result.errorDetails);
                    }else{
                        component.set("v.countIS",result.CountOfIncedents);
                    }
                }
            } else if(state == "ERROR"){
                component.set("v.errorCode", '10000');
                component.set("v.errorDetails", a.getError());
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    }
})