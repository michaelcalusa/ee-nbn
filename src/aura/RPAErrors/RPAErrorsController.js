({
	doInit : function(component, event, helper) {
		helper.getErrorsForAllIncidents(component, event, helper);
	},
    
    fixerror : function(component, event, helper) {
		helper.updateIncident(component, event, helper);
	},
    
    handleShowModal: function(component, event, helper) {
        var modalBody;
        var error = event.getSource().get("v.name");
        var before = error.before;
        var after = error.after;
        var tmpMapBefore = [];
        for( var key in before){
        	tmpMapBefore.push({key: key, value: before[key]});
        }
        var tmpMapAfter = [];
        for( var key in after){
        	tmpMapAfter.push({key: key, value: after[key]});
        }
        $A.createComponent("c:RPABeforeAndAfterModal", {beforeIncident: tmpMapBefore, afterIncident: tmpMapAfter},
			function(content, status) {
            	if (status === "SUCCESS") {
                	modalBody = content;
                    component.find('overlayLib').showCustomModal({
                   		header: "MORE INFORMATION",
                        body: modalBody, 
                        showCloseButton: true,
                        cssClass: "mymodal",
                    })
                }
            });
    }
})