({
	getErrorsForAllIncidents: function(component, event, helper) {
        var action = component.get("c.getErrorsForAllIncidents");
        action.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();
            //check if result is successfull
            console.log('state ' + state);
            if (state == "SUCCESS") {
                var result = a.getReturnValue();
                if (!$A.util.isEmpty(result) && !$A.util.isUndefined(result)) {
                	component.set("v.errors",result);   
                }
            } else if (state == "ERROR") {}
        });
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },
    
    updateIncident: function(component, event, helper) {
        var action = component.get("c.updateIncident");
        action.setParams({
            recordId : event.getSource().get("v.name")
        });
        action.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();
            //check if result is successfull
            console.log('state ' + state);
            if (state == "SUCCESS") {
                console.log('error fixed');
                helper.getErrorsForAllIncidents(component, event, helper);
            } else if (state == "ERROR") {}
        });
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    }
})