({
    fixerror : function(component, event, helper) {
        var spinner = component.find("rpaErrorFixSpinner");
        $A.util.removeClass(spinner, "slds-hide");
        component.set("v.isRetry", false);
        helper.updateIncident(component, event, helper);
    },
    
    retryError : function(component, event, helper) {
        component.set("v.showRPAComponent", false);
        component.set("v.isRetry", true);
        helper.updateIncident(component, event, helper);
    },
    
    handleShowModal: function(component, event, helper) {
        var modalBody;
        var error = event.getSource().get("v.name");
        var before = error.before;
        var after = error.after;
        var tmpMapBefore = [];
        for( var key in before){
        	tmpMapBefore.push({key: key, value: before[key]});
        }
        var tmpMapAfter = [];
        for( var key in after) {
        	tmpMapAfter.push({key: key, value: after[key]});
        }
        $A.createComponent("c:RPABeforeAndAfterModal", {beforeIncident: tmpMapBefore, afterIncident: tmpMapAfter},
			function(content, status) {
            	if (status === "SUCCESS") {
                	modalBody = content;
                    console.log('modal content-->'+modalBody);
                    component.find('overlayLib').showCustomModal({
                   		header: "MORE INFORMATION",
                        body: modalBody, 
                        showCloseButton: true,
                        cssClass: "mymodal"
                    })
                }
            });
    },

    recordUpdated: function(component, event, helper) {
        var changeType = event.getParams().changeType;
        if (changeType === "CHANGED" ||
            changeType === "LOADED") 
        { 
            var currentIncRecord = component.get("v.currentIncidentRecord");
            console.log('record loaded in RPA error card'+JSON.stringify(currentIncRecord));
            if(currentIncRecord.RPA_Error_Occurred__c){
                //when RPA fails
                helper.getErrorForIncident(component, event, helper);
            }else{
                //When the RPA error is retried or fixed mananully
                component.set("v.showRPAComponent", false);
                var spinner = component.find("rpaErrorFixSpinner");
        	    $A.util.addClass(spinner, "slds-hide");
            }
        }
    },
})