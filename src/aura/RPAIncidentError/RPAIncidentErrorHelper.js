({
	getErrorForIncident: function(component, event, helper) {
        var action = component.get("c.getErrorForIncident");
        action.setParams({
            incNum : component.get("v.currentIncidentRecord.Name")
        });
        action.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();
            //check if result is successfull
            console.log('state ' + state);
            if (state == "SUCCESS") {
                var result = a.getReturnValue();
                if (!$A.util.isUndefinedOrNull(result)) {
                    //map to store the translations of all the user actions
                    var incidentRec = component.get("v.currentIncidentRecord");
                    var SLARegionandServiceRestorationTypeCalc;
                    var priorityAssist;
                    var prodCatTier;
                    if(!$A.util.isUndefinedOrNull(incidentRec)){
                        SLARegionandServiceRestorationTypeCalc = $A.util.isUndefinedOrNull(incidentRec.SLARegionandServiceRestorationTypeCalc__c) ?  'blank' : 
                                                                                                                                                     incidentRec.SLARegionandServiceRestorationTypeCalc__c;
                        priorityAssist = $A.util.isUndefinedOrNull(incidentRec.PriorityAssist__c) ? 'blank' : 
                                                                                                    incidentRec.PriorityAssist__c;
                        prodCatTier = $A.util.isUndefinedOrNull(incidentRec.prodCatTier3__c) ? 'blank' : 
                                                                                               incidentRec.prodCatTier3__c;
                    }
                    var IncBoolCheckForSLAType = (
                                            SLARegionandServiceRestorationTypeCalc.includes('Standard') || 
                                            SLARegionandServiceRestorationTypeCalc.includes('Enhanced') || 
                                            SLARegionandServiceRestorationTypeCalc.includes('Priority Assist')
                                         );
                    var SLABoolCheckForSLAType = SLARegionandServiceRestorationTypeCalc.includes('Standard');
                    var prodCatTierBoolCheck = prodCatTier.includes('Standard');
                    var isPriorityAssistNo = (priorityAssist === 'No');
                    var actionSpinnerBoolValMap = { 'addNote' : {
                                                                    inc : false, 
                                                                    sla : false,
                                                                    actionTranslation : 'Add Note'
                                                                }, 
                                                    'acceptIncident' : {
                                                                            inc : IncBoolCheckForSLAType, 
                                                                            sla : SLABoolCheckForSLAType,
                                                                            actionTranslation : 'Accept Incident'
                                                                        }, 
                                                    'resolveIncident' : {
                                                                            inc : true, 
                                                                            sla : true,
                                                                            actionTranslation : 'Resolve Incident'
                                                                        },
                                                    'User Assigned' : {
                                                                        inc : false, 
                                                                        sla : false,
                                                                        actionTranslation : 'Assign incident to me'
                                                                    }, 
                                                    'acceptAndAssign' : {
                                                                            inc : true, 
                                                                            sla : isPriorityAssistNo 
                                                                                  && 
                                                                                  prodCatTierBoolCheck,
                                                                            actionTranslation : 'Assign and accept incident'
                                                                          },
                                                    'declineResolutionRejection' : {
                                                                                        inc : IncBoolCheckForSLAType, 
                                                                                        sla : SLABoolCheckForSLAType,
                                                                                        actionTranslation : 'Decline Resolution Rejection'
                                                                                    },
                                                    'acceptResolutionRejection' : {
                                                                                        inc : IncBoolCheckForSLAType, 
                                                                                        sla : SLABoolCheckForSLAType,
                                                                                        actionTranslation : 'Accept Resolution Rejection'
                                                                                    },
                                                    'changeEUETAppointment' : {
                                                                                    inc : true, 
                                                                                    sla : true,
                                                                                    actionTranslation : 'Convert to Appointment'
                                                                                },
                                                    'changeEUETCommitment' : {
                                                                                 inc : true, 
                                                                                 sla : false,
                                                                                 actionTranslation : 'Convert to Commitment'
                                                                              },
                                                    'reassignIncident' : {
                                                                                inc : false, 
                                                                                sla : false,
                                                                                actionTranslation : 'Re-assign Incident'
                                                                            },
                                                    'requestInffromRSP' : {
                                                                                        inc : true, 
                                                                                        sla : true,
                                                                                        actionTranslation : 'Request Information from RSP'
                                                                                     },
                                                    'requestNewAppoinmentFromRSP' : {
                                                                                            inc : true, 
                                                                                            sla : true,
                                                                                            actionTranslation : 'Request New Appointment From RSP'
                                                                                        }, 
                                                    'nbnHeld' : {
                                                                        inc : true, 
                                                                        sla : false,
                                                                        actionTranslation : 'Place on Hold'
                                                                      },
                                                    'nbnOutOfHeld' : {
                                                                            inc : true, 
                                                                            sla : false,
                                                                            actionTranslation : 'Remove Hold'
                                                                        },
                                                    'relateToIncident' : {
                                                                                inc : false, 
                                                                                sla : false,
                                                                                actionTranslation : 'Relate to Incident'
                                                                            },
                                                    'rspAcceptResolution' : {
                                                                                  inc : true, 
                                                                                  sla : true,
                                                                                  actionTranslation : 'Accept resolution for RSP'
                                                                              }, 
                                                    'rspRejectResolution' : {
                                                                                  inc : true, 
                                                                                  sla : true,
                                                                                  actionTranslation : 'Reject resolution for RSP'
                                                                              },
                                                    'techNote' : {
                                                                      inc : false, 
                                                                      sla : false,
                                                                      actionTranslation : 'Tech Note'
                                                                  },
                                                    'dispatchTechnician' : {
                                                                                inc : true, 
                                                                                sla : false,
                                                                                actionTranslation : 'Dispatch Technician'
                                                                            },
                                                    'ASRespondedToInfoRequested' : {
                                                                                            inc : true, 
                                                                                            sla : true,
                                                                                            actionTranslation : 'AS Responded to Info Requested'
                                                                                        },
                                                    'changePRD' : {
                                                                                            inc : false, 
                                                                                            sla : false,
                                                                                            actionTranslation : 'Update planned remediation date'
                                                                                        },
                                                    'techOffsite' : {
                                                                                inc : true, 
                                                                                sla : false,
                                                                                actionTranslation : 'Offsite technician'
                                                                            },
                                                    'techOnsite' : {
                                                                                inc : true, 
                                                                                sla : false,
                                                                                actionTranslation : 'Onsite technician'
                                                                          }
                    };
                    component.set("v.actionSpinnerBoolValMap", actionSpinnerBoolValMap);
                    //set the retry button attributes to default values
                    component.set("v.disableRetryButton", false);
                    var possibleErrorCodes = $A.get("$Label.c.JIGSAW_Retry_Error_Codes").split(';');
                    console.log('Error codes:'+possibleErrorCodes);
                    var i;
                    for(i=0; i < result.length; i++){
                        //get human readable action 
                        var tmpAction = result[i].action;
                        if($A.util.isUndefinedOrNull(actionSpinnerBoolValMap[tmpAction])){
                            result[i].translatedAction = tmpAction;
                        }else{
                            result[i].translatedAction = actionSpinnerBoolValMap[tmpAction].actionTranslation;
                        }
                        //diable\enable the retry button
                        var retryCounterLabel = Number($A.get("$Label.c.JIGSAW_Retry_Counter"));
                        if(retryCounterLabel === 0){
                            component.set("v.hideRetryButton",true);
                        } 
                        else{
                            component.set("v.hideRetryButton",false);
                            if( !$A.util.isUndefinedOrNull(result[i].errorCode) && 
                                possibleErrorCodes.includes(result[i].errorCode) )
                            {
                                component.set("v.disableRetryButton", true);
                            }
                            else{
                                if(result[i].retryCounter >= retryCounterLabel){
                                    component.set("v.disableRetryButton", true);
                                }
                            }
                        }
                    }
                    component.set("v.errors",result);
                    component.set("v.showRPAComponent", true);
                }
            } else if (state == "ERROR") {}
        });
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },
    updateIncident: function(component, event, helper) {
        //On retry create an incident management record to update the boolean values which will trigger the spinners
        var awaitingCurrentIncStatus = false;
        var awaitingCurrentSLAStatus = false;
        if(component.get("v.isRetry")){
            var incId;
            var action;
            var errorList = component.get("v.errors");
            var i;
            for(i=0; i < errorList.length; i++){
                incId = errorList[i].incidentNum;
                action = errorList[i].action;
                break;
            }
            var actionSpinnerBoolValMap = component.get("v.actionSpinnerBoolValMap");
            if(!$A.util.isUndefinedOrNull(incId) && !$A.util.isUndefinedOrNull(action)) {
                if(!$A.util.isUndefinedOrNull(actionSpinnerBoolValMap[action])) {
                    awaitingCurrentIncStatus = actionSpinnerBoolValMap[action].inc;
                    awaitingCurrentSLAStatus = actionSpinnerBoolValMap[action].sla;
                }
            }
        }
        var action = component.get("c.updateIncident");
        action.setParams({
            recordId : event.getSource().get("v.name"),
            isRetry : component.get("v.isRetry"),
            awaitingCurrentIncStatus : awaitingCurrentIncStatus,
            awaitingCurrentSLAStatus : awaitingCurrentSLAStatus
        });
        action.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();
            //check if result is successfull
            console.log('state ' + state);
            if (state == "SUCCESS") {
                var resp = a.getReturnValue();
                console.log('error fixed for inc rec:'+JSON.stringify(resp));
                if(component.get("v.isRetry")){
                    helper.fireSpinnerEvent(resp[0]);
                }
            } else if (state == "ERROR") {
                component.set("v.disableRetryButton", true);
            }
        });
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },
    fireSpinnerEvent : function(inm){
        //fire the toast message when none of the spinner start
        if(inm.User_Action__c === 'addNote')
        {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
            "type": "success",
            "message": $A.get("$Label.c.AddNotesSuccessMessage")
            });
            toastEvent.fire();
        }
        else if(!inm.Awaiting_Current_Incident_Status__c && 
            inm.User_Action__c != 'User Assigned')
        {
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                "message" : "Submitting Request" + '\n' + inm.DynamicToastForIncident__c
            });
            resultsToast.fire();
        }
        var spinnersToFire = inm.Awaiting_Current_Incident_Status__c ? 'IncidentStatus' : '';
        spinnersToFire = inm.Awaiting_Current_SLA_Status__c ? spinnersToFire + 'SLA' : 
                                                              spinnersToFire;
        var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
        spinnerHandlerEvent.setParams({
                                        "attrCurrentIncidentRecord" : inm, 
                                        "spinnersToStart" : spinnersToFire, 
                                        "stopSpinner" : false
                                     });
        spinnerHandlerEvent.fire();
    }
})