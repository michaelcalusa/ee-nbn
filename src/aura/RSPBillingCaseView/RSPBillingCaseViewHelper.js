({
	getRSPBillingCases: function(component, event, helper){
        console.log(' -- Helper - getRSPBillingCases --');
        var action = component.get("c.RSPBillingCases"); 
  
  
  
  
  
        action.setCallback(this, function(response){
            var state = response.getState();    
            console.log(' -- Response Size --' + state);
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();  
                console.log(' -- Stored Response --' + storeResponse +'--' + storeResponse.isContactMatrix);
                component.set("v.showMatrixView", storeResponse.isContactMatrix);
                
                if(storeResponse.isContactMatrix == false){
                    component.set("v.listOfCaseRecords", storeResponse.lstBillingCases);  
                    
                    
                    if(storeResponse.lstBillingCases.length > 0){
                        console.log(' -- Case view --');
                        component.set("v.showCaseView", true);    
                        component.set("v.showNoRecordsView", false);    
                    }
                    else{
                        console.log(' -- No Case view --');
                        component.set("v.showCaseView", false);    
                        component.set("v.showNoRecordsView", true);    
                    }                    
                }
            } 
        });
		
		
		
        $A.enqueueAction(action);
    }
})