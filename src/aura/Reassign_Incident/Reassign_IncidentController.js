({
    openRequestInfo : function(component, event, helper) { 
        //Fire Summary Table Event To Dismiss The Section While Actions Are Performed
        var summaryTableEvent = component.getEvent("handleSummaryTableEvent");
        summaryTableEvent.fire();
		component.set("v.addNoteComponentVisible",true);  
        component.set("v.addNoteComposerVisible",true);
        component.set("v.disableActionLink",true);  
    },
    
    reassignIncident:function(component, event, helper) {
        helper.getReassignIncidentData (component, event, helper);
        component.set("v.disableRequestForInfo",false);
        component.set("v.disableActionLink",false);
    },
    
    handleMinimizeMaximize:function(component, event, helper) {
        var section = component.find("dcSection");
        if($A.util.hasClass(section, 'slds-is-open'))
        {            
            $A.util.addClass(section, 'slds-is-closed');
            $A.util.removeClass(section, 'slds-is-open');
        }
        else
        {            
            $A.util.removeClass(section, 'slds-is-closed');
            $A.util.addClass(section, 'slds-is-open');
        }
    },
    
    handleClose:function(component, event, helper) {
        component.set("v.confirmationModalVisible",true);
    },
    
    handleCloseConfirmed:function(component, event, helper) {
        component.set("v.addNoteComposerVisible",false);
        component.set("v.confirmationModalVisible",false);
        component.set("v.txtNoteValue","");
        component.set("v.disableActionLink",false);
    },
    
    handleCancelConfirmationModal:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
    },
    
    handleMinimizeClose : function(component, event, helper){
        component.set("v.maximizemodal",true);
        component.set("v.resolveIncidentComposerVisible",false);
        component.set("v.confirmationModalVisible",true);
    },
    
    changeValue : function (component, event, helper) {
      component.set("v.lookupValue", false);
    },

    handleValueChange : function (component, event, helper) {
        // handle value change
        console.log("old value: " + event.getParam("oldValue"));
        console.log("current value: " + event.getParam("value"));
        component.set("v.isSelected", true);
        if (event.getParam("value") == ''){
            component.set("v.isSelected", false);           
        }
    }
    
})