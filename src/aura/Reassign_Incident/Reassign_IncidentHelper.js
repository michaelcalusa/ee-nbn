({
    getReassignIncidentData: function(component, event){
    	var selectedLookUpValue = component.find('selectedValue').get('v.value');
        var self = this;        
        var action = component.get("c.reassignIncidentAssignee");
        var currentIncidentManagementRecordWrapper = component.get("v.attrCurrentIncidentRecord");
        action.setParams({
            "strRecordId": currentIncidentManagementRecordWrapper.inm.Id,
            "assigneeId": selectedLookUpValue
        });
        action.setCallback (this,function(response) {
            var state = response.getState();
            var toastEvent = $A.get("e.force:showToast");
            if (state === "SUCCESS") { 
                if(response.getReturnValue() != 'false'){
                    var WaitingForUpdate = $A.get("e.c:WaitingForUpdate");
                    WaitingForUpdate.setParams({"waitingForUpdateAttribute" : false });
                    WaitingForUpdate.fire();
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": $A.get("$Label.c.ReassignIncidentSuccessMessage") +' '+ response.getReturnValue()
                    });
                    component.set("v.reassignIncidentComponentVisible",false);
                    component.set("v.addNoteComposerVisible",false);
                    component.set("v.isSelected",false);
                    component.set("v.disableActionLink",true); 
                    component.set("v.disableAddNote",false);
                    component.set("v.disableDispatchTechnician",true);
                    component.set("v.show_resolve_link",true);
                    component.set("v.disableAssignAndAccept",false);
                    toastEvent.fire();
                }
                else{
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": $A.get("$Label.c.ReassignIncidentErrorMessage")
                    });
                    toastEvent.fire();
                }
            } 
            else {
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": $A.get("$Label.c.ReassignIncidentErrorMessage")
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
})