({    
    recordUpdate: function(component, event, helper) {   
        
        var successMessage = $A.get("$Label.c.SDM_SuccessfulRefresh");
        var failureMessage = $A.get("$Label.c.SDM_RefreshFailure");  
        var emptyLocIdMessage = $A.get("$Label.c.SDM_EmptyLocationId"); 
        var locIdNotFoundInCIS = $A.get("$Label.c.SDM_LocationNotFoundCIS");
        var loadedOppRecord = component.get('v.opportunityRecord');
        //check for loaded opp record
        console.log('@@opportunityRecord -- ' + JSON.stringify(component.get('v.opportunityRecord')));
        
        //check for empty location ID
        if(loadedOppRecord.SDM_NBN_Location_ID__c != null &&
           loadedOppRecord.SDM_NBN_Location_ID__c != ''){
            
            var action = component.get("c.refreshOpptyOrders");
            action.setParams({
                "locId": loadedOppRecord.SDM_NBN_Location_ID__c//component.get('v.opportunityRecord') 
            });
            action.setCallback(this, function(response) {
                if(response.getState() == 'SUCCESS'){             
                    console.log('@@Returned value -- ' + response.getReturnValue());
                    if(response.getReturnValue() == 200)
                        component.set('v.responseStatus', successMessage);
                    else if(response.getReturnValue() == 404)
                        component.set('v.responseStatus', locIdNotFoundInCIS);
                    else
                        component.set('v.responseStatus', failureMessage);                
                    
                }            
            });
            $A.enqueueAction(action);
            $A.get('e.force:refreshView').fire();
            
        }
        else{
            component.set('v.responseStatus', emptyLocIdMessage);
        }
        
        //$A.get("e.force:closeQuickAction").fire();        
    }
})