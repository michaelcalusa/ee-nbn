/*******************************************************************************
 * Component Name : RelatedCaseListController Test Class Type :
 * CaseCreateRelated_Test Version : 1.0 Created Date: 21/11/2017 Function :
 * Retrieve all the related cases for a single Location ID for RSP and
 * Complaints Modification Log : Developer Date Description
 * ----------------------------------------------------------------------------
 * Mohith Ramachandraiah 21/11/2017 Created
 ******************************************************************************/
({
    init: function (cmp, event, helper) {
        var action = cmp.get("c.getCases");
        action.setParams({
            "recordId":  cmp.get("v.recordId")
        });  
        
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                cmp.set("v.mydata", response.getReturnValue());
                var csList = cmp.get("v.mydata");
                cmp.set("v.caseListSize",csList.length);
                var csMap = new Map();
                for(var i = 0, size = csList.length; i < size ; i++){
                    var item = csList[i];
                    csMap.set(csList[i].CaseNumber,csList[i].Id);
                }
                cmp.set("v.caseMap", csMap);
                if(csList.length === 0){
                    var thead = cmp.find("tableheader");
                    $A.util.addClass(thead,'slds-hide');
                }
                
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    },
    
    
    
    
    
    handleClick: function (cmp, event, helper) {
        var currCase = event.currentTarget;
        var csMap = cmp.get("v.caseMap");
        var caseUrl = '#/sObject/' + csMap.get(currCase.title) + '/view';
        var workspaceAPI = cmp.find("workspace"); 
        workspaceAPI.getAllTabInfo().then(function(response) {
            console.log(response);
        })
        console.log(currCase.title);
        console.log(caseUrl);
        console.log()
        workspaceAPI.getEnclosingTabId().then(function(tabId) {
            console.log(tabId);
            console.log(currCase.title);
            console.log(caseUrl);
         /*
			 * workspaceAPI.openSubtab({ parentTabId: tabId, url: caseUrl,
			 * focus: true });
			 */
             workspaceAPI.openTab({
                url: caseUrl,
                focus: true
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    }    
    
    
    
    
})