({
    performAction : function(component, event, helper){
        component.set('v.docComposerComponentVisible',true);
        component.set('v.docComposerVisible',true);
        component.set("v.confirmationModalVisible",false);
    },
    fetchInitData : function(component, event, helper){
        if(component.get('v.showOpActionLink')){
            helper.getReasonCodeDescription(component, event, helper);
        }     
    },
    submitResponse:function(component, event, helper) {
        helper.addRequestNoteAction(component, event, helper);
    },
    handleCloseConfirmed:function(component, event, helper) {
        helper.resetAttributes(component);
    },
    toggleTextValue :function(component, event, helper) {
        helper.getToggleTextValue(component, event, helper);
    },
    validate : function(component, event, helper){
        helper.validate(component);
    }
})