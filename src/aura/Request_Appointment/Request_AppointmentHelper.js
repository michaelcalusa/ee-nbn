({ 
    getReasonCodeDescription: function(component, event, helper){
        component.set('v.ServerSideAction','c.getReasonCode');
        component.set('v.useCache',true);
        component.set('v.ServerSideActionFaliure',$A.get("$Label.c.GenericError"));
        helper.performOperatorAction(component,{
                                                "incidentRec": component.get("v.incidentRecordObj")
                                                }, (response, component, self) => {
                                                    var arrayOfMapKeys = [];
                                                    var StoreResponse = response.getReturnValue();
                                                    component.set('v.TemplateMap', StoreResponse);
                                                    for (var singlekey in StoreResponse) {
                                                        arrayOfMapKeys.push(singlekey);
                                                    }
                                                    component.set('v.Template', arrayOfMapKeys);
                                                });
    },
    getToggleTextValue: function(component, event, helper) {
        var sel = component.find("mySelect");
        var nav =	sel.get("v.value");
        var TemplateMap = component.get('v.TemplateMap');
        component.set("v.txtNoteValue", TemplateMap[nav]);
        helper.validate(component);
    },
    addRequestNoteAction: function(component, event, helper) {
        var self = this;        
        var action = component.get("c.saveRequestAppointment");
        var incRec = component.get("v.incidentRecordObj");
        component.set('v.ServerSideAction','c.saveRequestAppointment');
        component.set('v.useCache',false);
        component.set('v.ServerSideActionFaliure',$A.get("$Label.c.RequestAppointmentErrorMessage"));
        helper.performOperatorAction(component,{
                                                            "incidentID": incRec.Id,
                                                            "noteType": 'external',
                                                            "txtNoteValue": component.get("v.txtNoteValue"),
                                                            "selectedValue" : component.get("v.selectedValue")
                                                        }, (response, component, self) => {
                                                            var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
                                                            addNoteRefreshEvent.fire();
                                                            var inm = response.getReturnValue();
                                                            self.fireSpinnerEvent(inm);
                                                            self.resetAttributes(component);
                                                });
    },
    resetAttributes : function(component){
        component.set("v.docComposerVisible",false);
        component.set("v.confirmationModalVisible",false);
        component.set('v.selectedValue','Please select...');
        component.set('v.disableSubmitButton',true);
        component.set("v.txtNoteValue","");
    },
    validate : function(component){
        let txtNote = component.get('v.txtNoteValue');
        if(txtNote && txtNote.trim().length > 0 && component.get('v.selectedValue')  != 'Please select...'){
            component.set('v.disableSubmitButton',false);
        }
        else{
            component.set('v.disableSubmitButton',true);
        }
    }
})