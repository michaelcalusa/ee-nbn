({
    openRequestInfo : function(component, event, helper) {     
        helper.getCommunicationTemplateValue(component, event, helper);
    },
    
    addInformationNote:function(component, event, helper) {
        helper.addRequestNoteAction(component, event, helper);    
    },
    
    handleMinimizeMaximize:function(component, event, helper) {
        var section = component.find("dcSection");        
        if($A.util.hasClass(section, 'slds-is-open'))
        {            
            $A.util.addClass(section, 'slds-is-closed');
            $A.util.removeClass(section, 'slds-is-open');
        }
        else
        {            
            $A.util.removeClass(section, 'slds-is-closed');
            $A.util.addClass(section, 'slds-is-open');
        }                
    },
    
    handleClose:function(component, event, helper) {
        component.set("v.confirmationModalVisible",true);
    },
    
    handleCloseConfirmed:function(component, event, helper) {
        component.set("v.addNoteComposerVisible",false);
        component.set("v.confirmationModalVisible",false);
        component.set("v.disableActionLink", false);        
    },
    
    handleCancelConfirmationModal:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
    },
    
    confirmTemplateValueChange :function(component, event, helper) {
        if(component.get("v.txtNoteValue").length > 0){
            component.set("v.templateChangeConfirmationModalVisible", true);
        }else{
            helper.getToggleTextValue(component, event, helper);  
        }       
    },
    
    CloseConfirmTemplateValueChange :function(component, event, helper) {        
        component.set("v.templateChangeConfirmationModalVisible", false);
        
    },
    
    toggleTextValue :function(component, event, helper) {
        component.set("v.templateChangeConfirmationModalVisible", false);        
        helper.getToggleTextValue(component, event, helper);
        
    },
    
})