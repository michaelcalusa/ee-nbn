({
    getCommunicationTemplateValue: function(component, event, helper){
        var action = component.get("c.getCommunicationTemplate");   
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {                
                var arrayOfMapKeys = [];
                var StoreResponse = response.getReturnValue();
                console.log('StoreResponse' + StoreResponse);
                component.set('v.TemplateMap', StoreResponse);
                for (var singlekey in StoreResponse) {
                    arrayOfMapKeys.push(singlekey);
                }                
                component.set('v.Template', arrayOfMapKeys);
                component.set("v.addNoteComposerVisible",true);        
        		component.set("v.disableActionLink", true);
            } 
        });           
        $A.enqueueAction(action);
    },
    
    getToggleTextValue: function(component, event, helper){
        var sel = component.find("mySelect");
        var nav =	sel.get("v.value");
        var TemplateMap = component.get('v.TemplateMap');
        component.set("v.txtNoteValue", TemplateMap[nav]);
    },    
        
    addRequestNoteAction: function(component, event, helper) {
        var self = this;        
        var action = component.get("c.saveRequestInfoRSP");
        var currentIncidentManagementRecordWrapper = component.get("v.attrCurrentIncidentRecord");
        var noteType = '';
        noteType = 'external'
        
        action.setParams({
            "incidentID": currentIncidentManagementRecordWrapper.inm.Id,
            "noteType": noteType,
            "txtNoteValue": component.get("v.txtNoteValue")
        });
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                //fire the event to auto refresh the recent history module
                var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
                addNoteRefreshEvent.fire();
                var varattrCurrentIncientRecord = response.getReturnValue();
                var spinnersToFire = '';
               if (varattrCurrentIncientRecord) {
                    if(varattrCurrentIncientRecord.Awaiting_Current_Incident_Status__c)
                        spinnersToFire = 'IncidentStatus';
                    if(varattrCurrentIncientRecord.Awaiting_Current_SLA_Status__c)
                        spinnersToFire = spinnersToFire + 'SLA';
                    var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
                    spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : varattrCurrentIncientRecord, "spinnersToStart" : spinnersToFire, "stopSpinner" : false});
                    spinnerHandlerEvent.fire();
                                
                    component.set("v.addNoteComposerVisible",false);
                    component.set("v.confirmationModalVisible",false);
                    component.set("v.txtNoteValue","");
                   
                } else {
                    
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message":$A.get("$Label.c.ReqMoreInfoErrorMessage")
                    });
                    toastEvent.fire();
                    
                }
               
            } else if (state === "INCOMPLETE") {
                var toastEvent = $A.get("e.force:showToast");
                 toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message":$A.get("$Label.c.ReqMoreInfoErrorMessage")
                    });
                    toastEvent.fire();
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        
        $A.enqueueAction(action);
    },
    
})