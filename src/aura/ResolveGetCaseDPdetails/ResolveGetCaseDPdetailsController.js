({
	doinit : function(component, event, helper) {
        console.log('recID ' +component.get("v.recordId"));
        component.set("v.recordId", component.get("v.recordId"));
        
        var action = component.get("c.getCaseDetails");     
        action.setParams({caseId : component.get("v.recordId") });
        action.setCallback(this, function(response) {
            var res = response.getState();
            if(res == 'SUCCESS') {  
               var cs = response.getReturnValue();
               console.log(cs);
               component.set("v.caseRec", cs);
            }
        });    
        $A.enqueueAction(action);
    },
    
    getP6Information : function(component, event, helper) {
        if (component.get("v.caseRec.Phase__c")!= null && component.get("v.caseRec.Category__c")!= null && component.get("v.caseRec.Sub_Category__c") != null && component.get("v.caseRec.Site__r.SAM__c")!= null && component.get("v.caseRec.Site__r.Rollout_Type__c")!= null && component.get("v.caseRec.Site__r.Technology_Type__c")!= null) {
            var action = component.get("c.getDPFromCaseButtonLex"); 
            action.setParams({cse : component.get("v.caseRec") });
            action.setCallback(this, function(response) {
                var res = response.getState();
                var respvalue = response.getReturnValue();
                if(res == 'SUCCESS') {  
                    if (respvalue == 'SUCCESS') {
                        alert('Fetch P6 details is successfull. Please check Case DP details related list');
                    }
                    else if (respvalue == 'InvalidProfile') {
                        alert('Sorry You Cannot use this option'); 
                    }
                }
            });    
            $A.enqueueAction(action);
        }
        else {
            alert('Please enter all mandatory fields required for fetching P6 information'); 
        }
    }
})