({
    onselectedTier1Code : function(component, event, helper) {
        component.set('v.selectedTier1Code',event.getParam('name'));
        component.set('v.tier2CodesData',null);
        component.set('v.selectedTier2Code',null);
        component.set('v.tier3CodesData',null);
        component.set('v.selectedTier3Code',null);
        component.set('v.industryCode',null);
        component.set("v.dlmTemplateText",null);
        helper.getTier2codes(component, event, helper);
    },
    onselectedTier2Code : function(component, event, helper) {
        component.set('v.selectedTier2Code',event.getParam('name'));
        component.set('v.tier3CodesData',null);
        component.set('v.selectedTier3Code',null);
        component.set('v.industryCode',null);
        component.set("v.dlmTemplateText",null);
        helper.getTier3codes(component, event, helper);
    },
    onselectedTier3Code : function(component, event, helper) {
        component.set('v.selectedTier3Code',event.getParam('name'));
        helper.getIndustryCodes(component, event, helper);
    },
    insertTemplateMessage : function(component, event, helper) {
        
        var templateMessage = component.get("v.dlmTemplateText");
        component.set("v.messageToAccessSeeker", templateMessage);
    },
    //Changes done as per user story 20475.(check current appointment status if it is in resered status send cancellation request to maximo otherwise proceed with resolveIncident)
    resolveIncident:function(component, event, helper) {
        helper.getCurrentAppointmentStatus(component, event, helper);
        if(component.get("v.currentAppointmentStatus") && component.get("v.currentAppointmentStatus") === "RESERVED")
        {
            helper.cancelappoinmentandresolveincident(component, event, helper);
        }
        else
        {   
            helper.submitResolveIncident(component, event, helper);
        }
    },
    openRequestInfo : function(component, event, helper) {
        helper.getTier1codes(component, event, helper);
    },
    handleClose:function(component, event, helper) {
        component.set("v.resolveIncidentComposerVisible",false);
        component.set("v.confirmationModalVisible",true);
    },
    handleCancelConfirmationModal:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
        component.set("v.resolveIncidentComposerVisible",true);
    },
    handleCloseConfirmed:function(component, event, helper) {
        component.set("v.confirmationModalVisible",false);
        component.set("v.resolveIncidentComposerVisible",false);
        component.set("v.resolveIncidentComponentVisible",false);
        component.set("v.disableActionLink",false);
    },
    handleMinimize : function(component, event, helper) {
        var resolveIncidentSectionDiv = component.find("resolveIncidentSection");
        if($A.util.hasClass(resolveIncidentSectionDiv, 'slds-is-open'))
        {            
            $A.util.addClass(resolveIncidentSectionDiv, 'slds-is-closed');
            $A.util.removeClass(resolveIncidentSectionDiv, 'slds-is-open');
        }
        else
        {            
            $A.util.removeClass(resolveIncidentSectionDiv, 'slds-is-closed');
            $A.util.addClass(resolveIncidentSectionDiv, 'slds-is-open');
        }   
    }
})