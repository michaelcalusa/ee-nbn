({
    //get current status and other Incident management records from this method
    getCurrentAppointmentStatus: function(component, event, helper) {
        var self = this;
        var currentIncidentWrapper = component.get("v.attrCurrentIncidentRecord");
        var currentStatus = currentIncidentWrapper.Current_status;
        var isLoggedInUserIsAssignee = currentIncidentWrapper.isLoggedInUserEqualsAssignee;
        var currentWorkOrderStatus = currentIncidentWrapper.inm.Current_work_order_status__c;
        var currentCommitmentWorkOrderStatus = (currentWorkOrderStatus && currentWorkOrderStatus.toUpperCase() != 'PENDING' && currentWorkOrderStatus.toUpperCase() != 'HELD' && currentWorkOrderStatus.toUpperCase() != 'COMPLETED' && currentWorkOrderStatus.toUpperCase() != 'CANCELLED' && currentWorkOrderStatus.toUpperCase() != 'CLOSED');
        if((currentIncidentWrapper.inm.Engagement_Type__c && (currentIncidentWrapper.inm.Engagement_Type__c.toUpperCase() === "APPOINTMENT")) && currentIncidentWrapper.inm.AppointmentId__c && (currentIncidentWrapper.inm.Appointment_Status__c && (currentIncidentWrapper.inm.Appointment_Status__c.toUpperCase() != "CANCELLED")))
        {
            component.set("v.currentAppointmentStatus", currentIncidentWrapper.inm.Appointment_Status__c.toUpperCase());	    
        }
		//Jira CUSTSA-21252      
        if((currentIncidentWrapper.inm.Engagement_Type__c && (currentIncidentWrapper.inm.Engagement_Type__c.toUpperCase() === "COMMITMENT")) && ((!currentWorkOrderStatus && currentIncidentWrapper.inm.AppointmentId__c && currentIncidentWrapper.inm.Appointment_Status__c && currentIncidentWrapper.inm.Appointment_Status__c.toUpperCase() == "RESERVED") || (currentWorkOrderStatus && currentCommitmentWorkOrderStatus)))
        {
            component.set("v.currentAppointmentStatus","RESERVED");   
        }
    },
    getTier1codes: function(component, event, helper) {
        var self = this;
        var action = component.get("c.getCategoryResoltuionCodes");
        var currentIncidentManagementRecordWrapper = component.get("v.attrCurrentIncidentRecord");
        action.setParams({
            productCategory : currentIncidentManagementRecordWrapper.inm.Prod_Cat__c,
            operationCategory : currentIncidentManagementRecordWrapper.inm.Op_Cat__c
        });
        action.setCallback(this, function(data) {
            var tier1Codesactionstatus = data.getState();
            if(tier1Codesactionstatus === 'SUCCESS')
            {
                var strtier1codes = data.getReturnValue();
                component.set('v.resolutionCodesCustomMetaData', strtier1codes);
                var distinctNames = this.getDistinctNames(strtier1codes,"Resolution_Category_Tier_1_Code__c");
                this.displayInsertTemplate(component,strtier1codes);
                component.set('v.tier1CodesData', distinctNames.sort());
                component.set("v.resolveIncidentComponentVisible",true);  
                component.set("v.resolveIncidentComposerVisible",true);  
                component.set("v.disableActionLink", true);
            }
        });
        $A.enqueueAction(action);
    },
    getTier2codes : function(component, event, helper) {
        var selectedTier1Code = component.get('v.selectedTier1Code');
        var tier2Codes = this.filterResolutionCodes(component.get('v.resolutionCodesCustomMetaData'),{key : "Resolution_Category_Tier_1_Code__c", value : selectedTier1Code});
        var distinctNames = this.getDistinctNames(tier2Codes,"Resolution_Category_Tier_2_Code__c");
        component.set('v.tier2CodesData', distinctNames.sort());
    },
    getTier3codes : function(component, event, helper) {
        var selectedTier1Code = component.get('v.selectedTier1Code');
        var selectedTier2Code = component.get('v.selectedTier2Code');
        var tier2Codes = this.filterResolutionCodes(component.get('v.resolutionCodesCustomMetaData'),{key : "Resolution_Category_Tier_1_Code__c", value : selectedTier1Code});
        var tier3Codes = this.filterResolutionCodes(tier2Codes,{key : "Resolution_Category_Tier_2_Code__c", value : selectedTier2Code});
        var distinctNames = this.getDistinctNames(tier3Codes,"Resolution_Category_Tier_3_Code__c");
        component.set('v.tier3CodesData', distinctNames.sort());
        if(distinctNames.length === 1)
        {
            component.set('v.selectedTier3Code', distinctNames[0]); 
            this.getIndustryCodes(component, event, helper);
        }
    },
    getIndustryCodes : function(component, event, helper) {
        var selectedTier1Code = component.get('v.selectedTier1Code');
        var selectedTier2Code = component.get('v.selectedTier2Code');
        var selectedTier3Code = component.get('v.selectedTier3Code');
        var tier2Codes = this.filterResolutionCodes(component.get('v.resolutionCodesCustomMetaData'),{key : "Resolution_Category_Tier_1_Code__c", value : selectedTier1Code});
        var tier3Codes = this.filterResolutionCodes(tier2Codes,{key : "Resolution_Category_Tier_2_Code__c", value : selectedTier2Code});
        var industryCodes = this.filterResolutionCodes(tier3Codes,{key : "Resolution_Category_Tier_3_Code__c", value : selectedTier3Code});
        component.set('v.industryCode', industryCodes[0].Industry_Code__c);
        component.set('v.industryDescription', industryCodes[0].Industry_Description__c);
        this.getSelectedTemplateMessage(component, industryCodes);
    },
    getSelectedTemplateMessage : function(component, industryCodes) {
        
        //set template text if available
        if(component.get("v.showTemplateButton") && industryCodes[0].Template_Name__c){
        //if(industryCodes[0].Template_Name__c){
            let templateDataList = component.get("v.templateData");
            if(templateDataList){
                let selectedTemplate = this.filterResolutionCodes(templateDataList,{key : "DeveloperName", value : industryCodes[0].Template_Name__c});
                if(selectedTemplate){
                    component.set("v.dlmTemplateText", selectedTemplate[0].Template_Value__c);
                }
            }
        }
    },
    //submit action to server
    submitResolveIncident : function(component, event, helper) {
        var self = this;
        var currentIncidentWrapper = component.get("v.attrCurrentIncidentRecord");
        var action = component.get("c.resolveIncidentManagementRecord");
        action.setParams({
            "strRecordId": currentIncidentWrapper.inm.Id,
            "strCategorytier1code": component.get("v.selectedTier1Code"),
            "strCategorytier2code": component.get("v.selectedTier2Code"),
            "strCategorytier3code": component.get("v.selectedTier3Code"),
            "strIndustrycode": component.get("v.industryCode"),
            "strIndustryDescription": component.get("v.industryDescription"),
            "strMessageSeeker": component.get("v.messageToAccessSeeker"),
            "strInternalResolutionNote": component.get("v.internalResolutionNote")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                //fire the event to auto refresh the recent history module
                var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
                addNoteRefreshEvent.fire();
                var varattrCurrentIncientRecord = response.getReturnValue();
                var spinnersToFire = '';
                if (varattrCurrentIncientRecord) {
                    if(varattrCurrentIncientRecord.Awaiting_Current_Incident_Status__c)
                        spinnersToFire = 'IncidentStatus';
                    if(varattrCurrentIncientRecord.Awaiting_Current_SLA_Status__c)
                        spinnersToFire = spinnersToFire + 'SLA';
                    var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
                    spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : varattrCurrentIncientRecord, "spinnersToStart" : spinnersToFire, "stopSpinner" : false});
                    spinnerHandlerEvent.fire();
                    component.set("v.attrCurrentIncidentRecord",varattrCurrentIncientRecord);
                    component.set("v.resolveIncidentComponentVisible",false);
                }
                else {
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": $A.get("$Label.c.ResolveIncidentErrorMessage")
                    });
                    toastEvent.fire();
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    cancelappoinmentandresolveincident : function(component, event, helper) {
        var action = component.get("c.cancelAppointmentInMaximo");
        action.setParams({
            "currentIncidentRecordId": component.get("v.attrCurrentIncidentRecord").inm.Id,
            "closureCode": "RES-CAN-NBN",
            "integrationType" : "cancelAppointment"
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                if(response.getReturnValue()) {
                    helper.submitResolveIncident(component, event, helper);
                }
                else
                {
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Maximo has failed to cancel the appointment/commitment. Please contact system support team"
                    });
                    toastEvent.fire();
                    component.set("v.disableActionLink",false);
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    //Functions to filter Json Data object
    getDistinctNames : function(resolutioncodesarray,name){
        var lookup = {};
        var result = [];
        for (var item, i = 0; item = resolutioncodesarray[i++];) {
            var loopname = item[name];
            if (!(loopname in lookup)) {
                lookup[loopname] = 1;
                result.push(loopname);
            }
        }
        return result;
    },
    displayInsertTemplate : function(component,resolutionArray){

        //check if template exists for resolution codes and show button
        var showBtn = component.get("v.showTemplateButton");
        for(var i = 0; i < resolutionArray.length ; i++){
            if(!showBtn){
                if(resolutionArray[i]['Template_Name__c']){
                    component.set("v.showTemplateButton", true);
                    
                    //get Resolution Category templates
                    this.getTemplateData(component);
                    break;
                }
            }
        }

       
        
    },
    getTemplateData : function(component){

        if(component.get("v.showTemplateButton")){
            var action = component.get("c.getCommunicationTemplates");
            action.setParams({
                strCategoryName : 'Resolve_incident'
            });
            action.setCallback(this, function(data) {
                var responseStatus = data.getState();
                if(responseStatus === 'SUCCESS')
                {
                    var templateData = data.getReturnValue();
                    component.set('v.templateData', templateData);
                    
                }
            });
            $A.enqueueAction(action);
        }
    },
    filterResolutionCodes : function(resolutioncodesarray,searchobject){
        var filteredArray = resolutioncodesarray.filter(function(currentitem){
            return (currentitem[searchobject.key] === searchobject.value);
        });
        return filteredArray;
    }
})