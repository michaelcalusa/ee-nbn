({
    doInit : function(component, event, helper) {
        
        var oppsFromVf = component.get('v.selectedOppfromVF');
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var temp = JSON.parse(oppsFromVf);

        if(temp.length == 0){
            let button = component.find('set_owner_button');
            button.set('v.disabled',true);
			var appEvent = $A.get("e.c:SDM_showToastEvent");
            appEvent.setParam("message", "Select at least one Opportunity to reassign. Please click on 'Cancel' to go back !!");
            appEvent.setParam("type", "warning");
            appEvent.setParam("duration", "15000");
            appEvent.fire();
        }
        else
        	component.set('v.returnedOpportunities', temp);
        
        //if(temp.length == 0)
        	//window.history.back();
        
        if(component.get('v.showInfo') == 'true'){
            //alert('You are only eligible to change owner for Opportunities owned by you');
            var appEvent = $A.get("e.c:SDM_showToastEvent");
            appEvent.setParam("message", "You are only eligible to change owner for Opportunities owned by you");
            appEvent.setParam("type", "warning");
            appEvent.setParam("duration", "15000");
            appEvent.fire();
        }
        
    },
    
    showLKP : function(component, event, helper) {
        var clickedLkp = event.getParam('fieldLabel');
        var searchTextLKP = event.getParam('searchText');     
        console.log('@@clickedLkp : ' + clickedLkp);
        if(searchTextLKP != null && searchTextLKP != ''){
            component.set('v.searchText',searchTextLKP);
        }
        
        if(clickedLkp=='Select User'){           
            component.set('v.showUserLKP',true);
            component.find("lkp_User_Comp").set('v.fieldLabel',clickedLkp);
        }
        
    }, 
    
    userChange : function(component, event, helper) {     
        component.set('v.showUserLKP',false);
        var userLkp	= component.find("userLkp"); 
        var oppName		= component.get('v.selectedUser.Name');
        var oppId 		= component.get('v.selectedUser.Id');
        userLkp.set("v.recordName", oppName);
        userLkp.set("v.recordId", oppId);            
        
    },
    
    cancel: function(component, event, helper) {     
        window.history.back();
    },
    
    setOwners: function(component, event, helper) { 
        
        var selectedUser = component.get('v.selectedUser');        
        var newOppList = [];
        
        if(selectedUser == null){
            var appEvent = $A.get("e.c:SDM_showToastEvent");
            appEvent.setParam("message", "Please click on the search icon to select a valid user !!");
            appEvent.setParam("type", "warning");
            appEvent.setParam("duration", "7000");
            appEvent.fire();
            return;
        }
        
        component.get('v.returnedOpportunities').forEach(function(opp){ 
            
            opp.OwnerId = selectedUser.Id;
            var detailtemp = JSON.stringify(opp);
            newOppList.push(detailtemp);
            
        });
        
        //component.set('v.returnedOpportunities', newOppList); 'sobjectType':'Opportunity'
        
        var action = component.get("c.UpdateOppOwners");
        action.setParams({
            "updatedOpps": newOppList
        });
        
        
        action.setCallback(this, function(response) {
            var res = response.getReturnValue();

            if(response.getState() == 'SUCCESS'){ 
                var appEvent = $A.get("e.c:SDM_showToastEvent");
                if(res == 'success'){
                    appEvent.setParam("message", "Opportunities updated with new owner !!");
                    appEvent.setParam("type", "success");
                    appEvent.setParam("duration", "3000");
                    appEvent.fire();
                    window.location.reload();     
                    window.location.href = component.get('v.filterName');
                }
                else if(res == 'fail'){
                    appEvent.setParam("message", "The owner selected is not eligible to own the opportunities.");
                    appEvent.setParam("type", "warning");
                    appEvent.setParam("duration", "15000");
                    appEvent.fire();  
                }
            }            
        });
        $A.enqueueAction(action);        
        
        //alert('Opprtunities has been updated !!');
        //$A.get('e.force:refreshView').fire();
        //window.history.back();  
        //console.log('@@filter -- *** ' + component.get('v.filterName')); 
    },
    
    userLoad: function(component, event, helper) {     
        
        var oppsFromVf = component.get('v.selectedOppfromVF');
        console.log('@@Test 11 - ' + JSON.stringify(oppsFromVf));  
        component.set('v.returnedOpportunities', JSON.parse(oppsFromVf));
        console.log('@@Test 2 - ' + JSON.stringify(component.get('v.returnedOpportunities')));
        
        var returnopp = component.get('v.returnedOpportunities');
        console.log('##Check length -- ' + returnopp.length); 
        //var userId = $A.get("$SObjectType.CurrentUser.Id");
        console.log('##Logged in user--- '+ JSON.stringify(component.get('v.userRecord')));
    }
    
})