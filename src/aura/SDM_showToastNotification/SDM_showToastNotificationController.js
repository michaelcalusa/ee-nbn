({
    hideNotify : function(component, event, helper) {
        $A.util.addClass(component.find('toastNotification'),'slds-hide');
    },
	displayToast : function(component, event) {
        var type = event.getParam("type");
        var message = event.getParam("message");
        var duration = event.getParam("duration");
        component.set('v.notificationMsg',message);
        switch(type.toLowerCase()){
            case 'success':
                $A.util.removeClass(component.find('toastNotifyTheme'),'slds-theme--error');
                $A.util.removeClass(component.find('toastNotifyTheme'),'slds-theme--warning');
                $A.util.addClass(component.find('toastNotifyTheme'),'slds-theme--success');
                break;
            case 'warning':
                $A.util.removeClass(component.find('toastNotifyTheme'),'slds-theme--success');
                $A.util.removeClass(component.find('toastNotifyTheme'),'slds-theme--error');
                $A.util.addClass(component.find('toastNotifyTheme'),'slds-theme--warning');
                break;
            case 'error':
                $A.util.removeClass(component.find('toastNotifyTheme'),'slds-theme--success');
                $A.util.removeClass(component.find('toastNotifyTheme'),'slds-theme--warning');
                $A.util.addClass(component.find('toastNotifyTheme'),'slds-theme--error');
        }
        $A.util.removeClass(component.find('toastNotification'),'slds-hide');
        
        window.setTimeout(
            $A.getCallback(function() {
                if (component.isValid()) {
                    $A.util.addClass(component.find('toastNotification'),'slds-hide');
                }
            }), duration
        )
	}
})