/**
 * Created by Gobind.Khurana on 21/05/2018.
 */
({
    doSave: function(cmp, event, helper) {
		var cmpTarget = cmp.find('inputContainer');
		$A.util.removeClass(cmpTarget, 'slds-has-error');
		cmp.set("v.showErrorMessage", false);
        if (cmp.find("fileId").getElement().files.length > 0) {
			console.log(cmp.find("fileId").getElement().files[0].name);
			if(!cmp.find("fileId").getElement().files[0].name.toLowerCase().endsWith("csv")){
				//Show validation message for file format;
				cmp.set("v.showErrorMessage", true);
				$A.util.addClass(cmpTarget, 'slds-has-error');
				cmp.set("v.errorMessage", "Invalid file format");
			}else{
                console.log('@dosave');
				helper.uploadHelper(cmp, event, helper);
			}

        } else {
            //Show validation message for missing file;
			cmp.set("v.showErrorMessage", true);
			$A.util.addClass(cmpTarget, 'slds-has-error');
			cmp.set("v.errorMessage", "Complete this field");
		}
    },

	downloadfile : function (cmp, event, helper){

        var action = helper.getApexProxy(cmp, "c.downloadAttachment");

        // set call back
        action.setCallback(this, function(response) {

            var state = response.getState();
            if (state === "SUCCESS") {
                var urlVal = response.getReturnValue();
                console.log('==== baseurl ====='+urlVal);
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": urlVal
                });
                urlEvent.fire();
                // handle the response errors
            } else if (state === "ERROR") {
				cmp.set("v.responseStatus", state);
                cmp.set("v.type","Banner");
                cmp.set("v.responseMessage", errorLabel);
            }
        });
        $A.enqueueAction(action.delegate());
    },

    cancel: function(cmp, event, helper) {
        //perform cancel operation
		cmp.find("fileId").getElement().value = "";
        helper.toggleError(cmp, false);
		cmp.set("v.responseStatus", "");
        cmp.set("v.type","");
        cmp.set("v.responseMessage", "");
    }
})