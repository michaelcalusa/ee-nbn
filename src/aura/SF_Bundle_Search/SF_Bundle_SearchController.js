/**
 * Created by Gobind.Khurana on 21/05/2018.
 */
({
    init: function (cmp, event, helper) {
        cmp.set('v.isTriggerdBySearch', false);
        helper.doSearch(cmp, event, helper);
    },

	searchTermChange: function(cmp, event, helper) {
		//Do search if enter key is pressed.
		if(event.getParams().keyCode == 13) {
			cmp.set('v.isTriggerdBySearch', true);
			helper.doSearch(cmp, event, helper);
		}
	},

	search: function(cmp, event, helper) {
		cmp.set('v.isTriggerdBySearch', true);
		console.log(cmp.get('v.productType'));
		helper.doSearch(cmp, event, helper);
	},

	clear: function(cmp, event, helper) {
		helper.clearErrors(cmp);
		cmp.set('v.isTriggerdBySearch', false);
		//helper.getRecords(cmp);
		cmp.set('v.searchTerm', '');
		helper.doSearch(cmp, event, helper);
		helper.disableClearSearchButton(cmp, true);
	},

	clickLink: function(cmp, event, helper) {
		var bundleId = event.target.getAttribute("data-Id");
		var locCount = event.target.getAttribute("data-locCount");
		console.log('bundleId is ' + bundleId);
		console.log('locCount is ' + locCount);
		var eventToFire;
		if(locCount > 0){
			eventToFire = cmp.getEvent("siteDetailPageEvent");
			eventToFire.setParams({"oppBundleId" : bundleId });
		}else{
			eventToFire = cmp.getEvent("oppBundleEvent");
			eventToFire.setParams({"oppBundleId" : bundleId });
		}

        eventToFire.fire();
    },

	renderPage: function(cmp, event, helper) {
        helper.renderPage(cmp);
    },

	updateColumnSorting: function (cmp, event, helper) {		
		helper.updateColumnSortingWithSubProperty(cmp, event, helper);
    },
})