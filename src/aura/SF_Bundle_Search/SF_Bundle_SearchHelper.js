/**
 * Created by Gobind.Khurana on 21/05/2018.
 */
({
	ERR_MSG_ERROR_OCCURRED: 'Error occurred. Error message: ',
	ERR_MSG_UNKNOWN_ERROR: 'Error message: Unknown error',
	MAX_COUNT: '5000',

    doSearch: function(cmp, event, helper) {

		this.disableClearSearchButton(cmp, false);
		cmp.set("v.pageNumber", 1);

		var searchTerm = cmp.get('v.searchTerm').trim();
		var productType = cmp.get('v.productType').replace(/\s/g, "_");
		var isTriggerBySearch = cmp.get('v.isTriggerdBySearch');
		if ($A.util.isEmpty(searchTerm)) {
            isTriggerBySearch = false;
		}
		console.log('is triggered by search: ' + isTriggerBySearch);

		var action = helper.getApexProxy(cmp, "c.getRecords");

		action.setParams({
			"searchTerm": searchTerm, 
			"isTriggerBySearch": Boolean(isTriggerBySearch),
			"productType": productType
		});

		action.setCallback(this, function(response) {
			var state = response.getState();
			if(state === "SUCCESS") {
				helper.clearErrors(cmp);
				var records = JSON.parse(response.getReturnValue());
				cmp.set('v.fullList', records);
				cmp.set('v.currentList', records);
				var pageCountVal = cmp.get("v.selectedCount");
				cmp.set("v.maxPage", Math.floor((records.length + (pageCountVal - 1)) / pageCountVal));
				helper.renderPage(cmp);
			} else {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.log(helper.ERR_MSG_ERROR_OCCURRED + errors[0].message);
					}
				} else {
					console.log(helper.ERR_MSG_UNKNOWN_ERROR);
				}
				var errorLabel = $A.get("$Label.c.DF_Application_Error");
				cmp.set("v.responseStatus", state);
				cmp.set("v.messageType","Banner");
				cmp.set("v.responseMessage", errorLabel);
				cmp.set('v.fullList', []);
				cmp.set('v.currentList', []);
			}
		});
		$A.enqueueAction(action.delegate());
	},

	renderPage: function(cmp) {
        var records = cmp.get("v.fullList"),
        pageNumber = cmp.get("v.pageNumber"),
        pageCountVal = cmp.get("v.selectedCount"),
        pageRecords = records.slice(((pageNumber - 1) * pageCountVal), (pageNumber * pageCountVal));
		cmp.set('v.currentList', []);
        cmp.set("v.currentList", pageRecords);
    },

	disableClearSearchButton: function(cmp, disabled){
		cmp.set('v.isClearSearchButtonDisabled', disabled);
	},

	clearErrors : function(cmp) {
        cmp.set("v.responseStatus", "");
        cmp.set("v.responseMessage", "");
        cmp.set("v.messageType", "");
	},

})