({
    handleApplicationEventFired: function(component, event) {
        var context = event.getParam("parameter");
        component.set("v.mostRecentEvent", context);
        var numApplicationEventsHandled =
            parseInt(component.get("v.numApplicationEventsHandled")) + 1;
        component.set("v.numApplicationEventsHandled", numApplicationEventsHandled);
    },
    handleCurrentIncidentRecord: function(component, event, helper) {
        if(component.get('v.recordId') === event.getParam("recordId")){
            var incidentData = event.getParam("currentIncidentRecord");
            helper.getCurrentIncidentObject(component,incidentData.inm,helper);
        }
     },
    startSpinners: function(component, event, helper) {
        if(!event.getParam("stopSpinner") && (event.getParam("currentRecordId") === component.get("v.recordId")))
        {
            var currentUserAction = event.getParam("userAction");
            if(event.getParam("spinnersToStart").includes("SLA"))
            {
                helper.startSpinner(component,helper);
            }
        }
    },
    handleSpinnerEvents: function(component, event, helper) {
        if(event.getParam("attrCurrentIncidentRecord").Id === component.get("v.recordId") && event.getParam("spinnersToStart").includes("SLA"))
        {
            if(!event.getParam("stopSpinner")) {
                helper.startSpinner(component,helper);
            }
            else {
                if(event.getParam("showSuccessToastMessage") && event.getParam("showSuccessToastMessage") === true)
                {
                    helper.getIncident(component,helper);
                }
            }
        }
    },
    handleErrorEvent : function(component, event, helper){
        if(event.getParam('recordId') == component.get('v.recordId') && event.getParam('errorKey') == 'RejectedIncident'){
            component.set('v.isRejectedIncident',true);
        }
    }
})