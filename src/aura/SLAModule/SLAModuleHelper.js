({
    getIncident: function(component, helper) {
        var action = component.get("c.getIncident");
        action.setParams({
            strRecordId: component.get("v.recordId"),
            componentName: component.get("v.componentName"),
            sObjectName: component.get("v.sObjectName")
        });
        action.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();
            //check if result is successfull
            if (state == "SUCCESS") {
                var result = a.getReturnValue();
                if (!$A.util.isEmpty(result) && !$A.util.isUndefined(result) && result[0]) {
                    helper.getCurrentIncidentObject(component,result[0],helper);
                }
            } else if (state == "ERROR") {}
        });
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },
    getCurrentIncidentObject: function(component, dataIM, helper) {
        component.set("v.showPausedSLA","false");
        component.set("v.ProgressCheck90","false");
        component.set("v.ProgressCheck75", "false");
        component.set("v.noMilestoneFound","false");
        component.set("v.DetailsRSP", "");
        var bgColourTarget = component.find('slaModuleBgColor');
        if($A.util.hasClass(bgColourTarget,'bg-grey')) {
            $A.util.removeClass(bgColourTarget,'bg-grey');
        }
        var iconClassTarget = component.find('slaModuleIconClass');
        if(!$A.util.hasClass(iconClassTarget, 'icon-sla'))
            $A.util.addClass(iconClassTarget, 'icon-sla');
        if(dataIM){
            component.set("v.strValue1", dataIM.Current_SLA__c);
            component.set("v.strValue2", dataIM.CurrentSLATypeCalc__c);
            component.set("v.strValue3", dataIM.SLAResponseOrResolve__c);
            component.set("v.strValue4", dataIM.CurrentslaDueDateCalc__c);
            component.set("v.strValue5", helper.formatSLADetails(dataIM.SLARegionandServiceRestorationTypeCalc__c));
            component.set("v.strValue6", dataIM.SLA_Status__c);
            component.set("v.incidentCurrentStatus", dataIM.Current_Status__c);
            component.set("v.incidentCurrentEngagement", dataIM.Engagement_Type__c);
            component.set("v.UserAction", dataIM.User_Action__c);
            if(dataIM.Aged_Incident__c && dataIM.Aged_Incident__c.toUpperCase() == "AGED" && (dataIM.Current_Status__c && dataIM.Current_Status__c.toUpperCase() != "CANCELLED" && dataIM.Current_Status__c.toUpperCase() != "REJECTED")) 
            {
                component.set("v.agedIncident", true);
                component.set("v.overflowdays", dataIM.Overflow_Days__c);
            }
            else
            {
                component.set("v.agedIncident", false);
            }
            if(( 
                	dataIM.Awaiting_Current_SLA_Status__c == true
            	) || (
                   dataIM.Engagement_Type__c == "Commitment" 
                   && 
                   dataIM.MaximoEngagementActionPending__c
               )) {
                let waitingTime = (new Date() - new Date(dataIM.Recent_Action_Triggered_Time__c));
                if(Number.isNaN(waitingTime) || (waitingTime / 60000 ) >= 3){
                    component.set("v.DetailsRSP", 'Updating SLA'); 
                    var iconClassTarget = component.find('slaModuleIconClass');
                    var bgColorTarget = component.find('slaModuleBgColor');
                    if(!$A.util.hasClass(bgColorTarget, 'bg-grey'))
                        $A.util.addClass(bgColorTarget, 'bg-grey');
                    if($A.util.hasClass(iconClassTarget, 'icon-sla'))
                        $A.util.removeClass(iconClassTarget, 'icon-sla');
                    //Start Of Setting Error Handling Attributes.
                    component.set("v.currentSLAReceived",false);
                    component.set("v.showSLAModule",false);
                    component.set("v.showSLAModuleError",true);
                }
                else{
                    waitingTime = $A.get("$Label.c.JIGSAW_Action_Waiting_Time_Limit") - waitingTime;
                	component.set('v.waitingTime',waitingTime);
                    helper.startSpinner(component, helper);
                }
                //End Of Setting Error Handling Attributes.
            } else if(dataIM.Awaiting_Current_SLA_Status__c == false){
                //Start Of Setting Error Handling Attributes.
                component.set("v.showSLAModule",true);
                component.set("v.showSLAModuleError",false);
                //if(component.get("v.showSLAModuleError"))
                component.set("v.currentSLAReceived",true);
                //End Of Setting Error Handling Attributes.
                //added an Extra check for sla status = in progress to display response sla during RSP Responed
                //add By Rohit Mathur MRTEST-1282
                /*if((dataIM.User_Action__c == 'requestNewAppoinmentFromRSP' ||  dataIM.User_Action__c == 'requestInffromRSP' || dataIM.User_Action__c == 'changeEUETAppointment') && dataIM.SLA_Status__c != 'In Process' && component.get("v.incidentCurrentStatus") != 'RSP Responded') 
                {
                    component.set("v.DetailsRSP",'Response Received'); 
                    var bgColorTarget = component.find('slaModuleBgColor');
                    $A.util.addClass(bgColorTarget, 'bg-grey');
                }*/
            }
            
            //If Incident Current Status is Pending then Display Gray Background and Paused Icon 11268 AC01
            if (((component.get("v.incidentCurrentStatus") == 'Resolved' || component.get("v.incidentCurrentStatus") == 'Closed') && (component.get("v.strValue6") == 'Pending' || component.get("v.strValue6") == 'Warning' || component.get("v.strValue6") == 'Met' || component.get("v.strValue6") == 'Missed')) || component.get("v.incidentCurrentStatus") == 'Pending' ) {
                console.log("Inside SLA Module Resolved Section");
                var bgColorTarget = component.find('slaModuleBgColor');
                $A.util.addClass(bgColorTarget, 'bg-grey');
                component.set("v.showPausedSLA",true);
                if( component.get("v.incidentCurrentStatus") == 'Closed' && (component.get("v.strValue6") == 'Met' || component.get("v.strValue6") == 'Missed')){
                    component.set("v.showCLosedSlaStatus",true);
                    var slaStatus = 'SLA '+ component.get("v.strValue6");
                    component.set("v.closedSlaStaus",slaStatus);
                }else{
                    component.set("v.showCLosedSlaStatus",false);
                }
            } else {
                var action = component.get("c.getMilestoneForSLA");
                action.setParams({
                    slaId: component.get("v.strValue1")
                });
                action.setCallback(this, function(a) {
                    var state = a.getState();
                    var dataMil = a.getReturnValue();
                    if (state == "SUCCESS") {
                        //debugger;
                        if (!$A.util.isEmpty(dataMil)) {
                            component.set("v.noMilestoneFound", false);
                            for (i in dataMil) {
                                if (i == "75%ProgressCheck") {
                                    component.set("v.ProgressCheck75", dataMil[i]);
                                }
                                if (i == "90%ProgressCheck") {
                                    component.set("v.ProgressCheck90", dataMil[i]);
                                }
                            }
                        } else {
                            component.set("v.noMilestoneFound", true);
                        }
                        
                    } else {
                        
                    }
                    helper.applyCSS(component);
                });
                $A.enqueueAction(action);
            }
        }       
		if(dataIM.Prod_Cat__c && dataIM.Prod_Cat__c == "NHUR" && dataIM.Operate_And_Managed_By__c && dataIM.Operate_And_Managed_By__c == "Foxtel")
        {
			component.set("v.showSLAModule", false);
			component.set("v.showSLAModuleError", false);
			component.set("v.nhurFoxtell", true);
		}
    },
    startSpinner: function(component, helper){
        var bgColorTarget = component.find('slaModuleBgColor');
        var iconClassTarget = component.find('slaModuleIconClass');
        if(!$A.util.hasClass(bgColorTarget, 'bg-grey'))
            $A.util.addClass(bgColorTarget, 'bg-grey');
        if($A.util.hasClass(iconClassTarget, 'icon-sla'))
            $A.util.removeClass(iconClassTarget, 'icon-sla');
        component.set("v.DetailsRSP", 'Updating SLA');
        //Start Of Setting Error Handling Attributes.
        component.set("v.currentSLAReceived",false);
        component.set("v.showSLAModule",true);
        component.set("v.showSLAModuleError",false);
        //End Of Setting Error Handling Attributes.
        helper.checkTimer(component,helper);        
    },
    applyCSS: function(component) {
        //JIRA 11268
        var bgColorTarget = component.find('slaModuleBgColor');
        //LDS to pick up changes and update the class with new values
        if($A.util.hasClass(bgColorTarget,'bg-yellow')){
            $A.util.removeClass(bgColorTarget,'bg-yellow');
        }
        if($A.util.hasClass(bgColorTarget,'bg-pink')){
            $A.util.removeClass(bgColorTarget,'bg-pink');
        }
        if (component.get("v.incidentCurrentStatus") != 'Pending') {
            if (!component.get("v.noMilestoneFound")) {
                //Case where there are milestone reocrds available for current SLA
                if ((component.get("v.strValue3") == 'Response' || component.get("v.strValue3") == 'Resolve') && component.get("v.ProgressCheck75") && component.get("v.ProgressCheck90")) {
                    //AC02.1 11268
                    //AC02.1 11268
                    //Default background applied by SLAModule.css
                } else if ((component.get("v.strValue3") == 'Response' || component.get("v.strValue3") == 'Resolve') && !component.get("v.ProgressCheck75") && component.get("v.ProgressCheck90")) {
                    //AC03.1 11268
                    //AC03.1 11268
                    $A.util.addClass(bgColorTarget, 'bg-yellow');
                } else if ((component.get("v.strValue3") == 'Response' || component.get("v.strValue3") == 'Resolve') && !component.get("v.ProgressCheck75") && !component.get("v.ProgressCheck90")) {
                    //AC04.1 11268
                    //AC04.1 11268
                    $A.util.addClass(bgColorTarget, 'bg-pink');
                } 
            } else {
                //In case where there are no milestone records found for current SLA
                if (component.get("v.strValue6") == 'Warning') {
                    $A.util.addClass(bgColorTarget, 'bg-yellow');
                } else if (component.get("v.strValue6") == 'Missed Goal') {
                    $A.util.addClass(bgColorTarget, 'bg-pink');
                }
                    else if (component.get("v.strValue6") == 'Missed') {
                        $A.util.addClass(bgColorTarget, 'bg-pink');
                    }
            }
        }
        var iconClassTarget = component.find('slaModuleIconClass');
        if($A.util.hasClass(iconClassTarget,'response'))
            $A.util.removeClass(iconClassTarget,'response');        
        if (component.get("v.strValue3") == 'Response' && component.get("v.incidentCurrentStatus") != 'Pending') {
            $A.util.addClass(iconClassTarget, 'response');
        }
    },
    
    getColumnLabelAndAttributes: function(component, helper) {
        component.set("v.currentSLAReceived",false);
        var action = component.get("c.getColumnLabelAndAttributes");
        var labelOrder;
        action.setParams({
            componentName: component.get("v.componentName"),
            sObjectName: component.get("v.sObjectName")
        });
        action.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();
            //check if result is successfull
            if (state === "SUCCESS") {
                var componentMap = a.getReturnValue();
                if (!$A.util.isEmpty(componentMap) && !$A.util.isUndefined(componentMap)) {
                    for (var key in componentMap) {
                        var labelMap = componentMap[key];
                        for (var keyL in labelMap) {
                            var OrderAndTypeMap = labelMap[keyL];
                            for (var keyOT in OrderAndTypeMap) {
                                labelOrder = keyOT;
                                this.setColumnLabelsAndOrder(component, labelOrder, key);
                            }
                        }
                    }
                }
                this.getIncident(component, helper);
            }
            else if (state == "ERROR") {}
        });
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },
    setColumnLabelsAndOrder: function(component, labelOrder, key) {
        if (labelOrder === 1) {
            component.set("v.strMapValue1", key);
        }
        if (labelOrder === 2) {
            component.set("v.strMapValue2", key);
        }
        if (labelOrder === 3) {
            component.set("v.strMapValue3", key);
        }
        if (labelOrder === 4) {
            component.set("v.strMapValue4", key);
        }
        if (labelOrder === 5) {
            component.set("v.strMapValue5", key);
        }
    },
    formatSLADetails : function (slaDetails) {
        let slaDetailsFinalArray = [];
        let slaDetailsArray = (slaDetails) ? slaDetails : " ";
        if(slaDetailsArray)
        {
            slaDetailsFinalArray.push(slaDetailsArray.slice(0,slaDetailsArray.indexOf(" ")));
            slaDetailsFinalArray.push((slaDetailsArray.slice(slaDetailsArray.indexOf(" "),slaDetailsArray.length)).trim());
        }
        console.log("SLA Details :- " + slaDetailsFinalArray);
        return slaDetailsFinalArray;
    },
    checkTimer:  function(component,helper) {
        var currentRunningComponent = this;
        console.log('waiting Time to set for SLA Status'  + component.get('v.waitingTime')/60000);
        window.setTimeout(
            $A.getCallback(function() {
                if(component.isValid())
                {
                    if(!component.get("v.currentSLAReceived") && !component.get("v.showSLAModuleError"))
                    {
                        /*component.set("v.showSLAModule",false);
                        component.set("v.showSLAModuleError",true);*/
                        helper.getIncident(component, helper);
                    }
                }
            }),component.get('v.waitingTime') //time after which the component reloads to check for any change in the record data
        );
    }
})