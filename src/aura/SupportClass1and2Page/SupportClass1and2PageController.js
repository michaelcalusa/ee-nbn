({
	onload : function(component, event, helper) {
        var accCustClass = component.get("c.getAccCustomerClass");
		accCustClass.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                component.set("v.isClass12",response.getReturnValue());
            }
            else{
                console.log('Errored');
            }
        });
        
       
        $A.enqueueAction(accCustClass);
	}
})