({
	onload : function(component, event, helper) {
		component.set("v.showSupportClass3and4PageBoolean",true);
	},
    
    searchKnowledge : function(component, event, helper) {
        var serachKnowledgeText = document.getElementById('cpd-searchKnowledge').value
        if (serachKnowledgeText != null && serachKnowledgeText != '') {
            var knowledgeSearchResult = component.get("c.getKnowledgeArticles");
            knowledgeSearchResult.setParams({
                "knowledgeSearchText" : serachKnowledgeText
            });
            knowledgeSearchResult.setCallback(this, function(response) {
                if (response.getState() == 'SUCCESS') {
                    var sArticles = [];
                    var searchedArticles = response.getReturnValue();
                    if (searchedArticles != null) {
                        var numOfSearchedArticles = searchedArticles.length;
                        for(var i = 0; i < numOfSearchedArticles; i++) {
                            sArticles.push(searchedArticles[i]);
                        }
                    }    
                    component.set("v.searchedArticleList",sArticles);
                }
                else {
                    console.log('Knowledge Search Result>>'+response.getState()+' message is >>>'+response.getError()[0].message);
                }
            });
            $A.enqueueAction(knowledgeSearchResult);
        }
        else {
               component.set("v.searchedArticleList",null);
        } 
   },
    
   searchKnow: function(component, event, helper) {
      if(event.keyCode == 13){
        var sKnow = component.get('c.searchKnowledge');
        $A.enqueueAction(sKnow);
      }
   }, 
    
   getKnowledgeDetail : function(component, event, helper) {
       var selectedknowledgeid = event.currentTarget.dataset.knowledgeid;
       component.set("v.selectedKnowledgeId",selectedknowledgeid);
       component.set("v.showKnowledgeDetailsBoolean", true);
       component.set("v.showSupportClass3and4PageBoolean",false);         
       component.set("v.showArticleListBoolean",true);
       var knowledgeDetails = component.get("c.getKnowledgeDetails");
        knowledgeDetails.setParams({
            knowledgeId : selectedknowledgeid
        });
        knowledgeDetails.setCallback(this, function(response) {
         	if(response.getState() == 'SUCCESS') {
                    var knowDet = response.getReturnValue();
                	for (var i=0; i<knowDet.length; i++) {
                        if (selectedknowledgeid ===knowDet[i].Id) {
               				  component.set("v.selectedArticleTitle",knowDet[i].Title);
                              component.set("v.selectedArticleNewDevContent",knowDet[i].New_Development_Content__c);
                        }
                    }
            }
            else {
                 console.log('Get Knowledge Details Status>>'+response.getState()+' message is >>>'+response.getError()[0].message);
        	}
        });  
        $A.enqueueAction(knowledgeDetails);
   },
    
    handleShowSupportClass3and4Page : function(component, event, helper) {
       component.set("v.showSupportClass3and4PageBoolean", true);
       component.set("v.showKnowledgeDetailsBoolean", false);
   }
})