({
	createTaskRecord: function(component, event, helper) {
      var createRecordEvent = $A.get("e.force:createRecord");
      var recordTypeId = $A.get("$Label.c.Task_Task_Record_Type_Id");      
      createRecordEvent.setParams({
         "entityApiName": "Task",
         "recordTypeId": recordTypeId,
          "defaultFieldValues": {
		        'WhatId' : component.get("v.recordId")        
		    }
         
      });
      createRecordEvent.fire();
   },
   
   createNoteRecord: function(component, event, helper) {
      var createRecordEvent = $A.get("e.force:createRecord");
      var recordTypeId = $A.get("$Label.c.Task_Note_Record_Type_Id");    
      createRecordEvent.setParams({
         "entityApiName": "Task",
         "recordTypeId": recordTypeId,
          "defaultFieldValues": {
		        'WhatId' : component.get("v.recordId")        
		    }
         
      });
      createRecordEvent.fire();
   },

   createInteractionRecord: function(component, event, helper) {
      var createRecordEvent = $A.get("e.force:createRecord");
      var recordTypeId = $A.get("$Label.c.Task_Interaction_Record_Type_Id");     
      createRecordEvent.setParams({
         "entityApiName": "Task",
         "recordTypeId": recordTypeId,
          "defaultFieldValues": {
		        'WhatId' : component.get("v.recordId")        
		    }
         
      });
      createRecordEvent.fire();
   }
})