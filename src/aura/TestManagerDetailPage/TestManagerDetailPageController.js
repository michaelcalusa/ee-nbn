({
    doInit: function(component, event, helper) {
		//debugger;
    },
    testManagerHandler: function(component, event, helper){
        //debugger;
        var recordId = event.getParam("recordId");
        if (recordId != undefined && recordId == component.get("v.recordId")) {
            var visibility = event.getParam("testManagerVisible");
            if (visibility != undefined) {
                component.set("v.displayTM", visibility);
                if(visibility != undefined && visibility){
                    helper.getTestSummary(component, event, helper);
                }
            }
        }
    },

    gotoURL: function(component, event, helper) {
        var workspaceAPI = component.find("workspace");


        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;

            //var appEvent = $A.get("e.c:JIGSAW_toggleTestManager");
            var appEvent =$A.get("e.c:JIGSAW_toggleTestManager");
            appEvent.setParams({
                "testManagerVisible": false
            });
            appEvent.setParams({
                "recordId": component.get("v.recordId")
            });
            appEvent.setParams({
                "focusedTabId": focusedTabId
            });
            appEvent.fire();
        }).catch(function(error) {
            console.log(error);
        });
    }
})