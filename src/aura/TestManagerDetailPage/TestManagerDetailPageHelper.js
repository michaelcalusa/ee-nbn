({
	getTestSummary : function(component, event, helper) {
		var recordId =  component.get("v.recordId");
		var queryDuration = component.get("v.queryDuration");
		if(queryDuration == undefined){
		    queryDuration = '0';
		}
		var action = component.get("c.getTestHistorySummary");
        action.setParams({
            recordId: recordId,
            duration: queryDuration
        });
        action.setCallback(this, function(results) {
            if(results.getState() == 'SUCCESS'){
                // set testHistoryObject attribute to be passed to Summary section 
                component.set('v.testHistoryObject',results.getReturnValue());
            }else{
                // set initialCalloutFailed attribute to handle any apex related issues/ Internal server erros
                // this attribute is shared with both detail section and summary section to display error screen
                component.set('v.initialCalloutFailed',true);
            }
        });
        $A.enqueueAction(action);
	}
})