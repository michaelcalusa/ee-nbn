({
    docall : function(component) {
        var action = component.get("c.countServiceOrders");
        action.setParams({
            "dayCount": component.get("v.Dayscount"),
            "incidentId": component.get("v.recordId")
        });
        
        action.setBackground();
        action.setCallback(this,function(a){
            var state = a.getState();
            if(state == "SUCCESS") {
                var result = a.getReturnValue();
                var avcId = result.avcId;
                var locationId = result.locId;
                var prodCat = result.prdCatVal;
                //CUSTSA-24704
                var orderDate = result.orderCompletionDate;
                var appEvent = $A.get("e.c:JIGSAW_ServiceChangeCalloutDone");
                appEvent.setParams({"avcId":avcId,
                                    "locID":locationId,
									"recordId" : component.get("v.recordId"),
                                    "orderCompletionDate":orderDate,
                                    "prodCat": prodCat
                                   });
                appEvent.fire();
                if(prodCat && prodCat == "NHUR")
                {
                    component.set("v.serviceChangeModuleApplicable",false);
                }
                else
                {
                    component.set("v.completedOrders", result.mapOrderCount.completedOrders);
                    component.set("v.inFlightOrders", result.mapOrderCount.inFlightOrders);
                    component.set("v.errorCode", result.mapOrderCount.expCode);
                    component.set("v.errorDetails", result.errorDetails);
                }
            } else if(state == "ERROR"){
                if(a.getError() && a.getError()[0].message == $A.get("$Label.c.JIGSAW_RejectionError")){
                    component.set('v.isRejectedIncident',true);
                    var appEvent = $A.get("e.c:JIGSAW_ErrorBroadcast");
                    appEvent.setParams({"errorKey":"RejectedIncident",
                                        "recordId" : component.get("v.recordId")
                                       });
                    appEvent.fire();
                    console.log('Incident is rejected itself.');
                }
            }
        });
        
        $A.enqueueAction(action);
    }
})