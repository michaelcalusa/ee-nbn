({
    handleApplicationEventWO : function(component, event, helper){
        if(event.getParam("recordId") === component.get("v.recordId")) {
            var locId = event.getParam('locID');
            var prodCat = event.getParam('prodCat');
            component.set("v.locId", locId);
            if(!$A.util.isUndefinedOrNull(prodCat) && 
                                 prodCat != 'NHAS' &&
                                 prodCat != 'NHUR') {
                helper.doCall(component, helper);
            } else{
                component.set("v.isHFC", true);
                var cacheList = [];
                var appEvent = $A.get("e.c:calloutDone");
                appEvent.setParams({"cacheList":component.get('v.cacheList'),
                                     "recordId":component.get('v.recordId')});
                appEvent.fire();
            }
        }
    },
    handleApplicationEventFired : function(cmp, event) {
        var context = event.getParam("parameter");
        console.log('context---'+context);
        cmp.set("v.mostRecentEvent", context);
        var numApplicationEventsHandled = parseInt(cmp.get("v.numApplicationEventsHandled")) + 1;
        cmp.set("v.numApplicationEventsHandled", numApplicationEventsHandled);
    },
    handleApplicationEvent : function(component, event) {
        component.set("v.showTable",event.getParam("showTable"));
        if (component.get("v.showTable") == true){
            component.set("v.confirmationModalVisible",true);   
        }else{
            component.set("v.confirmationModalVisible",false);  
        }
    },
    handleErrorEvent : function(component, event, helper){
        if(event.getParam('recordId') == component.get('v.recordId') && event.getParam('errorKey') == 'RejectedIncident'){
            component.set('v.isRejectedIncident',true);
        }
    }
})