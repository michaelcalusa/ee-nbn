({
    doCall : function(component,helper) {
        var actionWork = component.get("c.get_workOrderHistory");
        actionWork.setParams({
            "daycount":component.get("v.Dayscount"),
            "recId":component.get("v.recordId"), 
            "integrationType": "WorkOrderHistory",
            "locId": component.get("v.locId")
        });
        actionWork.setCallback(this,function(a){
            //get the response state 
            var state = a.getState();
            //alert(state);
            if(state == "SUCCESS"){
                var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                    console.log('result ',result);
                    if(result.errorCode == "200"){
                        result.errorCode = "0";
                    }
                    
                    //cache logic 20/8/2018
                    var cacheList = [];
                    if(result.deserializedObject){
                        cacheList.push(result.deserializedObject); 
                        console.log(cacheList);
                    }
                    component.set('v.cacheList',cacheList);
                    
                    var appEvent = $A.get("e.c:calloutDone");
                    appEvent.setParams({"cacheList":component.get('v.cacheList'),
                                        "recordId":component.get('v.recordId')});
                    appEvent.fire();
                    //end of Cahce logic
                    
                    component.set("v.countIS", result.totalRecords);
                    if(result.errorCode != '1000'){
                      component.set("v.errorCode", result.errorCode);
                    }else{
                        component.set("v.errorCode", 'UNKNOWN');
                    }
                    component.set("v.errorDetails", result.errorDetails);
                    
                }
            } else if(state == "ERROR"){
                component.set("v.countIS", 0);
                component.set("v.errorCode", 'UNKNOWN');
                component.set("v.errorDetails", a.getError());
            }
            helper.applyCSS(component);
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(actionWork);
    },
    
    applyCSS: function(component) {
        var bgColorTarget = component.find('incidentStatusBgColor');
        if (component.get("v.countIS") >= 0) {
            $A.util.addClass(bgColorTarget, 'bg-yellow');
        }else{
            $A.util.addClass(bgColorTarget, 'bg-white');
        }
    }
})