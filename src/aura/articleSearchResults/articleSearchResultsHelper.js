({
	getArticles : function(component) {
		console.log('getArticles');
        var action = component.get("c.getArticles");
        action.setParams({
            searchTerm : component.get("v.searchTerm"),
            topicId : component.get("v.topicId")
        });

		
		
		
        action.setCallback(this, function(response) {
        	var state = response.getState();
        	if (component.isValid() && state === 'SUCCESS') {
                var resp = response.getReturnValue();
                var articles = [];
                articles = JSON.parse(resp);
                
            	//console.log(resp);
                component.set("v.articles", articles);
                //console.log('articles', articles);
                if (articles != null)
                		this.formatDateFields(component);
                this.doneWaiting(component);
        	}
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
                this.doneWaiting(component);
            }
		});
        $A.enqueueAction(action);
	},
    
    formatDateFields : function(component) {
        var articles = component.get("v.articles");
        
        for (var i = 0 ; i < articles.length; i++) {
          var firstDateSplit = this.splitDate(articles[i].FirstPublishedDate);
          var lastDateSplit = this.splitDate(articles[i].LastPublishedDate);
          articles[i].FirstPublishedDate = firstDateSplit[2]+'/'+firstDateSplit[1]+'/'+firstDateSplit[0];
          articles[i].LastPublishedDate = lastDateSplit[2]+'/'+lastDateSplit[1]+'/'+lastDateSplit[0];
      	  
          //assign relevance values so we do not have to requery
          articles[i].Relevance = i;
        }
        component.set("v.articles", articles);
	},
    
    splitDate : function(dateString) {
    	var year = dateString.substring(0, 4);
        var month = dateString.substring(5, 7);
        var day = dateString.substring(8, 10);
        var dateArray = [year,month,day];
        return dateArray;
    },
    
    sortResults : function(component) {
        console.log('testfunction sortResults');
        var articles = component.get("v.articles");
        var sortArray = component.find("sortOption").get("v.value").split(" ");
        var sortOption = sortArray[0];
        var sortAsc = false;
        
        if (sortArray.length > 1) {
            if(sortArray[1] == 'ASC') {
                sortAsc = true;
            }
        }
        console.log('sortAsc', sortAsc);
        
        if (sortOption == 'ViewCount') {
            console.log('ViewCount');
            articles = articles.sort(function (a, b) {
                return b.ViewCount - a.ViewCount;
            });
        }
        else if (sortOption == 'FirstPublishedDate') {
            console.log('FirstPublishedDate');
            articles = articles.sort(function (a, b) {
                if (a.FirstPublishedDate < b.FirstPublishedDate) {
                	return 1;
                }
                if (a.FirstPublishedDate > b.FirstPublishedDate) {
                    return -1;
                }
                
                // names must be equal
                return 0;
            });
        }
        else if (sortOption == 'LastPublishedDate') {
            console.log('LastPublishedDate');
            articles = articles.sort(function (a, b) {
                if (a.LastPublishedDate < b.LastPublishedDate) {
                	return 1;
                }
                if (a.LastPublishedDate > b.LastPublishedDate) {
                    return -1;
                }
                
                // names must be equal
                return 0;
            });
        }
        else {
            console.log('relevance');
            articles = articles.sort(function (a, b) {
                return a.Relevance - b.Relevance;
            });
        }
        
        if (sortAsc) {
            articles.reverse();
        }
        component.set("v.articles", articles);
        //var sortedArticles;
	},
    
    doneWaiting : function (component) {
        $A.util.addClass(component.find('spinnerContainer'), 'hide');
    }
})