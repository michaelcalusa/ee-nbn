({
    callBack : function(response, component, helper){
        var inm = response.getReturnValue();
        helper.fireSpinnerEvent(inm);
        component.set('v.userName',inm.assignee__c);
        component.set('v.strPhotoURL',inm.Assignee_Photo__c);
    }
})