({
	doInit : function(component, event, helper) {
		//retrieve application parameters and show confirmation message
		var sPageURL = decodeURIComponent(window.location.search.substring(1));
       	var sParameterName = sPageURL.split('=');
		component.set("v.recordId",sParameterName[1]);
		console.log('sParameterName==='+sParameterName[1]);               
		var action1 = component.get("c.getDetails"); 
		action1.setParams({
			"newDevId": sParameterName[1]
		});  
		action1.setCallback(this, function(response){
			var state = response.getState();
			if (state === "SUCCESS") { 
				var returnMap = response.getReturnValue();
				console.log('inside==='+returnMap);               
				//component.set("v.returnFinalMap", returnMap); 
				component.set('v.applicationReferenceNumber',returnMap.applicationReferenceNumber); 
				component.set("v.confirmationType",returnMap.confirmationType);
				component.set("v.applicantEmail",returnMap.applicantEmail);
				component.set("v.totalCost",returnMap.totalCost);
				component.set("v.paymentReferenceNumber",returnMap.paymentReferenceNumber);
				component.set("v.portalURL",returnMap.portalURL);
				component.set("v.accessCode",returnMap.uniqueCode); 
				component.set("v.billingContactName",returnMap.billingContactName);
				component.set("v.billingContactEmail",returnMap.billingContactEmail); 
				//component.set("v.showSection",'CONFIRMATION');                     
				if(returnMap.CustomerClass=='Class3/4' && returnMap.SDType==='SD-2'){
					component.set("v.displayRefNum",true);
				}
				else{
					component.set("v.displayRefNum",false);
				}
				if(returnMap.SDType==='SD Unknown'){
					component.set("v.isTechAss",true);
				}
			}
			else{
				console.log('errors '+response.getError()[0].message);
			}
		});
		$A.enqueueAction(action1);
		window.scrollTo(0, 0);
	}
})