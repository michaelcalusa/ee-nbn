({
    doInit: function(component, event, helper) { 
        console.log('*** doInit ***');
        var action = component.get("c.getAccountId");   
        action.setCallback(this, function(response){
            var state = response.getState();            
            if (state === "SUCCESS") { 
                var accDetails = response.getReturnValue().split("|");
                console.log('accDetails ==>'+ accDetails);
                component.set("v.accountName", accDetails[0]);
                component.set("v.accountId", accDetails[1]);
                component.set("v.isEditor", accDetails[2]);
                component.set("v.isThisEditorAccess", accDetails[2]);                
                component.set("v.Heading","General Contact Details"); 
                
                // Call helper to get the Roles 
                helper.getRole(component); 
            }
        });
        $A.enqueueAction(action);
        // Hide all divs on load      
        var findGeneralContactDetails = component.find("generalContactDetailsId");
        $A.util.addClass(findGeneralContactDetails, 'showMe'); 
        
        var findNotesDetails = component.find("notesDetailsId");
        $A.util.addClass(findNotesDetails, 'hideMe'); 
        
        var findTableDetails = component.find("TableDetailsId");
        $A.util.addClass(findTableDetails, 'hideMe');
        
        var findDetailSection = component.find("detailSectionId");
        $A.util.addClass(findDetailSection, 'hideMe'); 
        
        var findCustomerDetailsTitle = component.find("customerdetailsTitleId");
        $A.util.addClass(findCustomerDetailsTitle, 'hideMe');
        
        var findNBNContactDetailsTitle = component.find("nbncontactdetailsTitleId");
        $A.util.addClass(findNBNContactDetailsTitle, 'hideMe');
        
       /* var findMyContactsTitle = component.find("myContactsTitleId");
        $A.util.addClass(findMyContactsTitle, 'showMe'); */
        
        var findMyContactsTitleMessage = component.find("myContactsTitleMessageId");
        $A.util.addClass(findMyContactsTitleMessage, 'showMe');                
        
        var findEditSection = component.find("editSectionId");
        $A.util.addClass(findEditSection, 'hideMe');
        
        var findSuccessMessage = component.find("successMessageId");
        $A.util.addClass(findSuccessMessage, 'hideMe');
        
        
        
        var findRoleAndRoleType = component.find("role_roleTypeId");
        $A.util.addClass(findRoleAndRoleType, 'showMe');
        
         var findRoleAndRoleTypeDetails = component.find("role_roleType_details_Id");
        $A.util.addClass(findRoleAndRoleTypeDetails, 'hideMe'); 
        
        var findCustomerButtons = component.find("CustomerButtonsId");                   
        $A.util.addClass(findCustomerButtons, 'hideMe');
        
        var findnbnButtons = component.find("nbnButtonsId");                   
        $A.util.addClass(findnbnButtons, 'hideMe');
        
        var findCustomerButtons = component.find("CustomerButtonsId_Top");                   
        $A.util.addClass(findCustomerButtons, 'hideMe');
        
        var findnbnButtons = component.find("nbnButtonsId_Top");                   
        $A.util.addClass(findnbnButtons, 'hideMe');
             
        var forOpen = component.find("typeahead-00B-9722");
        $A.util.addClass(forOpen, 'lookup_menu_close'); 
        $A.util.removeClass(forOpen, 'lookup_menu_open');
    },
    
    saveRecord: function(component){
        
        console.log('** SAVE RECORD **');
        console.log('updated wrapper ==>'+component.get("v.cmObject"));
        console.log('SearchMessage ==>'+ component.get("v.SearchMessage"));
        var hasErrors = 'False';
        if(component.get("v.SearchMessage")!='' && component.get("v.SearchMessage")!=undefined)
        {
            console.log('SearchContactTextId ==>'+component.find("searchContactTextId"));
            var searchBoxId = component.find("searchContactTextId");
            searchBoxId.set("v.errors",[{message:component.get("v.SearchMessage")}]);
            console.log('errors ==>'+component.get("v.errors"));
            hasErrors = 'True';
        }
                        
        var level1 = component.get("v.cmObject");
        var elem = [];
        elem = component.find("fieldId");
        var targetFieldToFocus='';
         console.log('targetFieldToFocus ==>'+ targetFieldToFocus);
         
        for(var x in elem)
        {
            console.log('elem value ==>'+ elem[x].get("v.label"));
            var targetFieldLabel = elem[x].get("v.label");
            var targetFieldValue = elem[x].get("v.value");
            var targetGlobalId = elem[x].getGlobalId(); 
            var targetField = $A.getComponent(targetGlobalId);
           
            
                       
            for(var x in level1)
            {
                var level2 = level1[x];
                for(var y in level2)
                {
                    var level3 = level2[y];
                    for(var z in level3)
                    {                              
                        if(targetFieldLabel == level3[z].fieldLabel)
                        {
                            console.log(level3[z].fieldType); 
                            if(level3[z].fieldType == 'PHONE' && targetFieldValue!=undefined && targetFieldValue!=''){                           
                                if(targetFieldValue.match(/^\d{10}$/)) 
                                {  
                                    targetField.set("v.errors",null); 
                                   // hasErrors = 'False';
                                }  
                                else  
                                {    
                                    if(targetFieldToFocus=='')
                                    {
                                        targetFieldToFocus = targetField;
                                    }
                                    targetField.set("v.errors", [{message:"Please enter a valid phone number"}]);
                                     hasErrors = 'True';
                                } 
                            } 
                            if(level3[z].fieldType == 'EMAIL' && targetFieldValue!=undefined && targetFieldValue!=''){
                                if(targetFieldValue.match( /^[^\s@]+@[^\s@]+\.[^\s@]+$/)) 
                                {  
                                    targetField.set("v.errors",null); 
                                    //hasErrors = 'False';
                                }  
                                else  
                                {   
                                     if(targetFieldToFocus=='')
                                    {
                                        targetFieldToFocus = targetField;
                                    }
                                    targetField.set("v.errors", [{message:"Please enter a valid email address"}]);
                                    hasErrors = 'True';
                                }          
                            }
                            if(level3[z].fieldType == 'STRING'){
                                if(targetFieldLabel == 'Last Name')  
                                {
                                    if(targetFieldValue!='') 
                                    {  
                                        targetField.set("v.errors",null); 
                                        //hasErrors = 'False';
                                    }  
                                    else  
                                    {    
                                         if(targetFieldToFocus=='')
                                    {
                                        targetFieldToFocus = targetField;
                                    }
                                        targetField.set("v.errors", [{message:"Please fill in the Last Name"}]);
                                        hasErrors = 'True';
                                    }
                                } 
                            } 
                        }                       
                    }                 
                }
            }
            
        }
        
       
        
        // To move focus to the top error field.
       /*  if(targetFieldToFocus!=''){
               console.log('*** targetFieldToFocus ==>'+ targetFieldToFocus);
             console.log('*** targetFieldToFocus el ==>'+ targetFieldToFocus.getElement());
             targetFieldToFocus.getElement().focus();                            
        }*/
        
        var findTypeahead = component.find("typeahead-00B-9722");
        $A.util.addClass(findTypeahead, 'lookup_menu_close'); 
        $A.util.removeClass(findTypeahead, 'lookup_menu_open');
        
        console.log('*** component.get("v.errors") ==>'+ component.get("v.errors"));
        console.log('*** hasErrors ==>'+ hasErrors);
        //if(component.get("v.errors")==undefined){  
        if(hasErrors == 'False'){            
            var action = component.get("c.upsertSelectedRecord");        
            var recId = component.get("v.tmRecordId");
            //component.find("roleTypeId").get("v.value")
            //console.log('*** roleTypeValue ==>'+ event.currentTarget.dataset.rtype);
            //  $A.util.json.encode(component.get("v.cmObject")),
            action.setParams({
                "updatedWrapper":  JSON.stringify(component.get("v.cmObject")),                
                "recId": recId,
                "roleValue": component.find("roleId").get("v.value"),
                "roleTypeValue": component.get("v.rType"), 
                "fsType":"CUS"
            });  
            
            action.setCallback(this, function(response){
                var state = response.getState();
                console.log('*** state ==>'+ state);
                if (state === "SUCCESS") {
                    if(response.getReturnValue() == "Success")
                    {
                        var findSuccessMessage = component.find("successMessageId");
                        $A.util.removeClass(findSuccessMessage, 'hideMe');
                        $A.util.addClass(findSuccessMessage, 'showMe'); 
                        
                       /* var findMyContactsTitle = component.find("myContactsTitleId");
                        $A.util.removeClass(findMyContactsTitle, 'hideMe');
                        $A.util.addClass(findMyContactsTitle, 'showMe'); */
                        
                        // hide all remaining divs on the page
                        var findEditSection = component.find("editSectionId");
                        $A.util.removeClass(findEditSection, 'showMe');
                        $A.util.addClass(findEditSection, 'hideMe');
                        
                        var findCustomerDetailsTitle = component.find("customerdetailsTitleId");
                        $A.util.removeClass(findCustomerDetailsTitle, 'showMe');
                        $A.util.addClass(findCustomerDetailsTitle, 'hideMe');
                        
                        var findRoleAndRoleType = component.find("role_roleTypeId");
                        $A.util.removeClass(findRoleAndRoleType, 'showMe');
                        $A.util.addClass(findRoleAndRoleType, 'hideMe');
                        
                        var findRoleAndRoleTypeDetails = component.find("role_roleType_details_Id");
                        $A.util.removeClass(findRoleAndRoleTypeDetails, 'showMe');
                        $A.util.addClass(findRoleAndRoleTypeDetails, 'hideMe');
                        
                        var findMyContactsTitleMessage = component.find("myContactsTitleMessageId");
                        $A.util.removeClass(findMyContactsTitleMessage, 'showMe');
                        $A.util.addClass(findMyContactsTitleMessage, 'hideMe');
                        
                        var forOpen = component.find("typeahead-00B-9722");
                        $A.util.addClass(forOpen, 'lookup_menu_close'); 
                        $A.util.removeClass(forOpen, 'lookup_menu_open');
                        
                        // Jump to top of the page
                        //event.preventDefault();
        				window.scrollTo(0, 0);
                        
                        // Set ERR flag to fals
                        component.set("v.showError","false");
                    }
                }
                else if(state === "ERROR"){
                    //{!$Label.c.CM_System_Error_Message}
                    component.set("v.errMessage", "System Error: Please contact System Administrator");
                    component.set("v.showError","True");
                    //CM_System_Error_Message
                     // Jump to top of the page
                     // It has an issue with Mozilla browser
                       // event.preventDefault();
        				window.scrollTo(0, 0);
                }
              /* else if(state === "ERROR"){ 
                   var errors = action.getError();
                   //var ferrors = errors.fieldErrors;
                   console.log(errors);
                   for(var v in errors){
                       var v1 = errors[v];
                       console.log(v1);
                       for(var x in v1)
                       {
                           var y = v1[x];
                           //console.log( y);
                           for(var z in y){
                               var zz = y[z];
                               console.log(zz[0].statusCode);
                               console.log(zz[0].message);
                           }
                       }
                       
                   }
                   
                    if (errors) {  
                        console.log('errors ==>'+errors);
                        if (errors[0] && errors[0].message) {    
                             //console.log('** get Error ==>'+ errors[0]);                            
                             component.set("v.errMessages", errors[0].message);                            
                        }  
                    }
                }*/
            });
            $A.enqueueAction(action);            
        }
    },
    
    handleRoleChange: function(component, event, helper) {
        console.log("** handleRoleChange **");
       /* var findGeneralContactDetails = component.find("generalContactDetailsId"); */
        var findNotesDetails = component.find("notesDetailsId");
        var findTableDetails = component.find("TableDetailsId");
        var findDetailSection = component.find("detailSectionId");
        var findEditSection = component.find("editSectionId"); 
        var findCustomerDetailsTitle = component.find("customerdetailsTitleId");
        var findNBNContactDetailsTitle = component.find("nbncontactdetailsTitleId");
       // var findMyContactsTitle = component.find("myContactsTitleId"); 
        
        var findRoleAndRoleTypeDetails = component.find("role_roleType_details_Id");
        $A.util.removeClass(findRoleAndRoleTypeDetails, 'showMe');
        $A.util.addClass(findRoleAndRoleTypeDetails, 'hideMe');
       
       /* $A.util.removeClass(findMyContactsTitle, 'hideMe');  
        $A.util.addClass(findMyContactsTitle, 'showMe');  
        
        $A.util.removeClass(findGeneralContactDetails,'showMe');  
        $A.util.addClass(findGeneralContactDetails, 'hideMe'); */
        
        $A.util.removeClass(findNotesDetails, 'showMe');  
        $A.util.addClass(findNotesDetails, 'hideMe');
        
        $A.util.removeClass(findTableDetails, 'showMe');
        $A.util.addClass(findTableDetails, 'hideMe'); 
        
        $A.util.removeClass(findDetailSection, 'showMe');
        $A.util.addClass(findDetailSection, 'hideMe'); 
        
        $A.util.removeClass(findEditSection, 'showMe');
        $A.util.addClass(findEditSection, 'hideMe');
        
        $A.util.removeClass(findCustomerDetailsTitle, 'showMe');
        $A.util.addClass(findCustomerDetailsTitle, 'hideMe');
        
        $A.util.removeClass(findNBNContactDetailsTitle, 'showMe');        
        $A.util.addClass(findNBNContactDetailsTitle, 'hideMe');
        
         var findCustomerButtons = component.find("CustomerButtonsId");
        $A.util.removeClass(findCustomerButtons, 'showMe');
        $A.util.addClass(findCustomerButtons, 'hideMe');
        
        var findnbnButtons = component.find("nbnButtonsId");
        $A.util.removeClass(findnbnButtons, 'showMe');
        $A.util.addClass(findnbnButtons, 'hideMe');
        
        var findCustomerButtons = component.find("CustomerButtonsId_Top");
        $A.util.removeClass(findCustomerButtons, 'showMe');
        $A.util.addClass(findCustomerButtons, 'hideMe');
        
        var findnbnButtons = component.find("nbnButtonsId_Top");
        $A.util.removeClass(findnbnButtons, 'showMe');
        $A.util.addClass(findnbnButtons, 'hideMe');
        
        component.set("v.roleTypeOptions", "");
        component.set("v.showBillingMessage","false");
        helper.getRoleTypes(component);
       /* console.log("single value roletype");
        var length = component.get("v.noOfRoleTypeOptions");
        if(length == "1"){
            console.log("execute");
            helper.getContactMatrixRecords(component); 
            helper.noOfRoleTypeOptions(component);             
        }*/
    },
    
    handleSortingFeature: function(component, event, helper){
         //console.log('**** contactstype ==>'+ event.currentTarget.dataset.contactstype);
         component.set("v.ContactType", event.currentTarget.dataset.contactstype);
         component.set("v.SortingField", event.currentTarget.dataset.fieldlbl);
        
         helper.getContactMatrixRecords(component); 
    },
    
    handleRoleTypeChange: function(component, event, helper) {
        console.log("***handleRoleTypeChange***");
        component.set("v.ContactType", undefined);
        /* var findGeneralContactDetails = component.find("generalContactDetailsId");        
        $A.util.addClass(findGeneralContactDetails, 'showMe');*/
        
        var findNotesDetails = component.find("notesDetailsId");        
        $A.util.addClass(findNotesDetails, 'showMe');
        
        var findDetailSection = component.find("detailSectionId");
        $A.util.removeClass(findDetailSection, 'showMe');
        $A.util.addClass(findDetailSection, 'hideMe'); 
        
         var findEditSection = component.find("editSectionId"); 
        $A.util.removeClass(findEditSection, 'showMe');
        $A.util.addClass(findEditSection, 'hideMe');
        
        var findTableDetails1 = component.find("TableDetailsId"); 
        $A.util.removeClass(findTableDetails1, 'hideMe');
        $A.util.addClass(findTableDetails1, 'showMe');
        
        var findSuccessMessage = component.find("successMessageId");
        $A.util.addClass(findSuccessMessage, 'hideMe');
        $A.util.removeClass(findSuccessMessage, 'showMe');
        
        var findRoleAndRoleType = component.find("role_roleTypeId");
        $A.util.removeClass(findRoleAndRoleType, 'hideMe');
        $A.util.addClass(findRoleAndRoleType, 'showMe');
        
        var findRoleAndRoleTypeDetails = component.find("role_roleType_details_Id");
        $A.util.removeClass(findRoleAndRoleTypeDetails, 'showMe');
        $A.util.addClass(findRoleAndRoleTypeDetails, 'hideMe');
        
        var findMyContactsTitleMessage = component.find("myContactsTitleMessageId");
        $A.util.removeClass(findMyContactsTitleMessage, 'hideMe');
        $A.util.addClass(findMyContactsTitleMessage, 'showMe');
        
         var findCustomerButtons = component.find("CustomerButtonsId");
        $A.util.removeClass(findCustomerButtons, 'showMe');
        $A.util.addClass(findCustomerButtons, 'hideMe');
        
        var findnbnButtons = component.find("nbnButtonsId");
        $A.util.removeClass(findnbnButtons, 'showMe');
        $A.util.addClass(findnbnButtons, 'hideMe');
        
        var findCustomerButtons = component.find("CustomerButtonsId_Top");
        $A.util.removeClass(findCustomerButtons, 'showMe');
        $A.util.addClass(findCustomerButtons, 'hideMe');
        
        var findnbnButtons = component.find("nbnButtonsId_Top");
        $A.util.removeClass(findnbnButtons, 'showMe');
        $A.util.addClass(findnbnButtons, 'hideMe');
        
        helper.getContactMatrixRecords(component); 
        //component.set("v.Heading","General Contact Details");
        
    },
    
    getSelectedValues: function(component, event, helper) {
        console.log("getContactMatrixRecords");
        helper.getContactMatrixRecords(component);       
    },
    
    sendEmailTo: function(component, event, helper){        
        var fieldValue = event.currentTarget.dataset.fieldval;
        var fieldLabel = event.currentTarget.dataset.fieldlbl;
        console.log('*** fieldval ==>'+ event.currentTarget.dataset.fieldval);
        console.log('*** fieldLabel ==>'+ event.currentTarget.dataset.fieldlbl);
        if(fieldLabel == "Email")
        {
             window.location.href = "mailto:" + fieldValue ;            
        }
        else if (fieldLabel == "Business Phone")
        {
             window.location.href = "tel:" + fieldValue ;
        }
       
        //+ "?subject=" + subject;
    },
    
    dialPhone: function(component, event, helper){        
        var phoneNumber = event.currentTarget.dataset.phonenumber;
        console.log('*** emailVal ==>'+ event.currentTarget.dataset.phonenumber);
        window.location.href = "tel:" + phoneNumber ;
        //+ "?subject=" + subject;
    },
    
    SelectedRecordDetails: function(component, event, helper) { 
        console.log('*** SelectedRecordDetails  rtype ==>'+ event.currentTarget.dataset.rtype);
        component.set("v.rType", event.currentTarget.dataset.rtype);
        var editFlag = "false";
        helper.readContactMatrixRecord(component, event, editFlag); 
        
        var fsType = event.currentTarget.dataset.fieldsettype;
        if(fsType == "Customer")
        {
            var findme5 = component.find("customerdetailsTitleId"); 
            $A.util.removeClass(findme5, 'hideMe');
            $A.util.addClass(findme5, 'showMe');
            
            var findme4 = component.find("nbncontactdetailsTitleId"); 
            $A.util.removeClass(findme4, 'showMe');
            $A.util.addClass(findme4, 'hideMe');          
            
        }
        else if(fsType == "nbn"){
            var findme5 = component.find("customerdetailsTitleId"); 
            $A.util.removeClass(findme5, 'showMe');
            $A.util.addClass(findme5, 'hideMe');
            
            var findme4 = component.find("nbncontactdetailsTitleId"); 
            $A.util.removeClass(findme4, 'hideMe');
            $A.util.addClass(findme4, 'showMe'); 
        }
        
        //console.log("findme3 ==>"+ findme3);
        /* var findMyContactsTitle = component.find("myContactsTitleId");        
        $A.util.removeClass(findMyContactsTitle, 'showMe');  
        $A.util.addClass(findMyContactsTitle, 'hideMe'); */
        
        var findMyContactsTitleMessage = component.find("myContactsTitleMessageId");
         $A.util.removeClass(findMyContactsTitleMessage, 'showMe');
        $A.util.addClass(findMyContactsTitleMessage, 'hideMe');  
        
        var findEditSection = component.find("editSectionId"); 
        $A.util.removeClass(findEditSection, 'showMe');
        $A.util.addClass(findEditSection, 'hideMe');
        
        var findDetailSection = component.find("detailSectionId"); 
        $A.util.addClass(findDetailSection, 'showMe');
        
        var findTableDetails = component.find("TableDetailsId"); 
        $A.util.removeClass(findTableDetails, 'showMe');
        $A.util.addClass(findTableDetails, 'hideMe');
        
        /* var findme3 = component.find("myContactsTitleId");         
        $A.util.addClass(findme3, 'hideMe'); */
        
       /* var findme6 = component.find("generalContactDetailsId"); 
        $A.util.removeClass(findme6, 'showMe');
        $A.util.addClass(findme6, 'hideMe'); */
        
        var findNotesDetails = component.find("notesDetailsId"); 
        $A.util.removeClass(findNotesDetails, 'showMe');
        $A.util.addClass(findNotesDetails, 'hideMe');
        
        var findRoleAndRoleTypeDetails = component.find("role_roleType_details_Id");
        $A.util.removeClass(findRoleAndRoleTypeDetails, 'hideMe');
        $A.util.addClass(findRoleAndRoleTypeDetails, 'showMe');
        
        var findRoleAndRoleType = component.find("role_roleTypeId");
        $A.util.removeClass(findRoleAndRoleType, 'showMe');
        $A.util.addClass(findRoleAndRoleType, 'hideMe');
        
    },
    
    goback : function(component, event) {
        console.log('** goback **');
                component.set("v.errMessage","");
        component.set("v.showError","false");
        component.set("v.SearchMessage",'');
        var searchBoxId = component.find("searchContactTextId");
        searchBoxId.set("v.errors",null);
        
        
        var findMyContactsTitleMessage = component.find("myContactsTitleMessageId");
        $A.util.addClass(findMyContactsTitleMessage, 'showMe'); 
        $A.util.removeClass(findMyContactsTitleMessage, 'hideMe');
        
        var findDetailSection = component.find("detailSectionId"); 
        $A.util.removeClass(findDetailSection, 'showMe');
        $A.util.addClass(findDetailSection, 'hideMe');
          
        var findTableDetails = component.find("TableDetailsId"); 
        $A.util.removeClass(findTableDetails, 'hideMe');
        $A.util.addClass(findTableDetails, 'showMe');
        
        var findGeneralContactDetails = component.find("generalContactDetailsId");
        $A.util.removeClass(findGeneralContactDetails, 'hideMe');
        $A.util.addClass(findGeneralContactDetails, 'showMe');
        
        var findNotesDetails = component.find("notesDetailsId"); 
        $A.util.removeClass(findNotesDetails, 'hideMe');
        $A.util.addClass(findNotesDetails, 'showMe');
        
        var findCustomerDetailsTitle = component.find("customerdetailsTitleId");
        $A.util.removeClass(findCustomerDetailsTitle, 'showMe');
        $A.util.addClass(findCustomerDetailsTitle, 'hideMe');
        
        var findNBNContactDetailsTitle = component.find("nbncontactdetailsTitleId");
        $A.util.removeClass(findNBNContactDetailsTitle, 'showMe');
        $A.util.addClass(findNBNContactDetailsTitle, 'hideMe');
        
        /*var findMyContactsTitle = component.find("myContactsTitleId");
        $A.util.removeClass(findMyContactsTitle, 'hideMe');
        $A.util.addClass(findMyContactsTitle, 'showMe');*/
        
        var findEditSection = component.find("editSectionId"); 
        $A.util.removeClass(findEditSection, 'showMe');
        $A.util.addClass(findEditSection, 'hideMe');
        
        var findCustomerButtons = component.find("CustomerButtonsId");
        $A.util.removeClass(findCustomerButtons, 'showMe');
        $A.util.addClass(findCustomerButtons, 'hideMe');
        
        var findnbnButtons = component.find("nbnButtonsId");
        $A.util.removeClass(findnbnButtons, 'showMe');
        $A.util.addClass(findnbnButtons, 'hideMe');
        
        var findCustomerButtons = component.find("CustomerButtonsId_Top");
        $A.util.removeClass(findCustomerButtons, 'showMe');
        $A.util.addClass(findCustomerButtons, 'hideMe');
        
        var findnbnButtons = component.find("nbnButtonsId_Top");
        $A.util.removeClass(findnbnButtons, 'showMe');
        $A.util.addClass(findnbnButtons, 'hideMe');
        
        var findRoleAndRoleTypeDetails = component.find("role_roleType_details_Id");
        $A.util.removeClass(findRoleAndRoleTypeDetails, 'showMe');
        $A.util.addClass(findRoleAndRoleTypeDetails, 'hideMe');
        
        var findRoleAndRoleType = component.find("role_roleTypeId");
        $A.util.removeClass(findRoleAndRoleType, 'hideMe');
        $A.util.addClass(findRoleAndRoleType, 'showMe');
        
        var forOpen = component.find("typeahead-00B-9722");
        $A.util.addClass(forOpen, 'lookup_menu_close'); 
        $A.util.removeClass(forOpen, 'lookup_menu_open');
        
    },
    
    editRecord : function(component, event, helper) {
        console.log('** editRecord **');  
        console.log('** rtype **'+ event.currentTarget.dataset.rtype);
        
        if(event.currentTarget.dataset.action == 'Edit')
        {
            var editFlag = "true";
            helper.readContactMatrixRecord(component, event, editFlag);            
            component.set("v.tmRecordId", event.currentTarget.dataset.record);
            component.set("v.rType", event.currentTarget.dataset.rtype);
        }
        
        var findMyContactsTitleMessage = component.find("myContactsTitleMessageId");
        $A.util.addClass(findMyContactsTitleMessage, 'hideMe'); 
        $A.util.removeClass(findMyContactsTitleMessage, 'showMe');
        
        var findCustomerEmail = component.find("Customer Contact Email");
        $A.util.removeClass(findEditSection, 'showMe');
        $A.util.addClass(findEditSection, 'hideMe');
        
        var findEditSection = component.find("editSectionId"); 
        $A.util.removeClass(findEditSection, 'hideMe');
        $A.util.addClass(findEditSection, 'showMe');
        
        var findDetailSection = component.find("detailSectionId"); 
        $A.util.removeClass(findDetailSection, 'showMe');
        $A.util.addClass(findDetailSection, 'hideMe');
        
        var findTableDetails = component.find("TableDetailsId"); 
        $A.util.removeClass(findTableDetails, 'showMe');
        $A.util.addClass(findTableDetails, 'hideMe');
        
       /* var findGeneralContactDetails = component.find("generalContactDetailsId");
        $A.util.removeClass(findGeneralContactDetails, 'showMe');
        $A.util.addClass(findGeneralContactDetails, 'hideMe');*/
        
        var findNotesDetails = component.find("notesDetailsId");
        $A.util.removeClass(findNotesDetails, 'showMe');
        $A.util.addClass(findNotesDetails, 'hideMe');
        
        var findCustomerDetailsTitle = component.find("customerdetailsTitleId");
        $A.util.removeClass(findCustomerDetailsTitle, 'hideMe');
        $A.util.addClass(findCustomerDetailsTitle, 'showMe');
        
        var findNBNContactDetailsTitle = component.find("nbncontactdetailsTitleId");
        $A.util.removeClass(findNBNContactDetailsTitle, 'showMe');
        $A.util.addClass(findNBNContactDetailsTitle, 'hideMe');
        
       /* var findMyContactsTitle = component.find("myContactsTitleId");
        $A.util.removeClass(findMyContactsTitle, 'showMe');
        $A.util.addClass(findMyContactsTitle, 'hideMe'); */
        
        var findnbnButtons = component.find("nbnButtonsId");
        $A.util.removeClass(findnbnButtons, 'showMe');
        $A.util.addClass(findnbnButtons, 'hideMe');
        
        var findCustomerButtons = component.find("CustomerButtonsId");
        $A.util.removeClass(findCustomerButtons, 'showMe');
        $A.util.addClass(findCustomerButtons, 'hideMe');
        
        var findnbnButtons_Top = component.find("nbnButtonsId_Top");
        $A.util.removeClass(findnbnButtons_Top, 'showMe');
        $A.util.addClass(findnbnButtons_Top, 'hideMe');
        
        var findCustomerButtons_Top = component.find("CustomerButtonsId_Top");
        $A.util.removeClass(findCustomerButtons_Top, 'showMe');
        $A.util.addClass(findCustomerButtons_Top, 'hideMe');        
        
        var findTypeahead = component.find("typeahead-00B-9722");
        $A.util.addClass(findTypeahead, 'lookup_menu_close'); 
        $A.util.removeClass(findTypeahead, 'lookup_menu_open');
        
        var findRoleAndRoleTypeDetails = component.find("role_roleType_details_Id");
        $A.util.removeClass(findRoleAndRoleTypeDetails, 'hideMe');
        $A.util.addClass(findRoleAndRoleTypeDetails, 'showMe');
        
        var findRoleAndRoleType = component.find("role_roleTypeId");
        $A.util.removeClass(findRoleAndRoleType, 'showMe');
        $A.util.addClass(findRoleAndRoleType, 'hideMe');
        
    },
    
    keyPressController : function(component, event, helper) {
        console.log('** keyPressController **');
        var getInputkeyWord = component.get("v.conName"); 
        if( getInputkeyWord.length > 0 ){
            var findTypeahead = component.find("typeahead-00B-9722");
            $A.util.addClass(findTypeahead, 'lookup_menu_open');
            $A.util.removeClass(findTypeahead, 'lookup_menu_close');
            
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
            component.set("v.listOfSearchRecords", null ); 
            var forclose = component.find("typeahead-00B-9722");
            $A.util.addClass(forclose, 'lookup_menu_close');
            $A.util.removeClass(forclose, 'lookup_menu_open');
        }         
    },
    
    selectContact : function(component, event, helper){
        console.log('**** selectContact *****');
        
        var action = component.get("c.getContactRelatedFields");
        // console.log('getContactRelatedFields.recordId'+ event.currentTarget.dataset.record);
        // component.set("v.tmRecorsId", event.currentTarget.dataset.record);
        action.setParams({
            "recordId": event.currentTarget.dataset.contactid
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('** getContactRelatedFields ==>'+ JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS") {
                component.set("v.cmObjectEdit", response.getReturnValue()); 
                var Edit1 = component.get("v.cmObjectEdit");             
                var level1 = component.get("v.cmObject");
                for(var x in level1)
                {
                    var level2 = level1[x];
                    for(var y in level2)
                    {
                        var level3 = level2[y];
                        for(var z in level3)
                        {    
                            
                            console.log('field label ==>'+ level3[z].fieldLabel);
                            console.log('field label value==>'+ Edit1[level3[z].fieldLabel]);
                            
                            if(Edit1[level3[z].fieldLabel]!=undefined)
                            {  
                                level3[z].fieldValue = Edit1[level3[z].fieldLabel];   
                            }
                            else
                            {
                                level3[z].fieldValue = '';                                
                            }
                            
                            if(level3[z].cusContactId!='')
                            {
                                level3[z].cusContactId = Edit1["Id"];
                                console.log('** cusContactId ==>'+ Edit1["Id"]);
                            } 
                            
                        }
                    }
                }
                component.set("v.cmObject", level1); 
                //console.log('v.cmObject ==>'+ JSON.Stringify(component.get("v.cmObject")));
            }
        }); 
        $A.enqueueAction(action);
       
        // console.log('getSelectContactName'+ event.currentTarget.dataset.contactid);
        var getSelectContactRecord = event.currentTarget.dataset.record;
        var getSelectContactName = event.currentTarget.dataset.contactname;
        component.set("v.conId", event.currentTarget.dataset.contactid);
        component.set("v.oContact", event.currentTarget.dataset.record);
        component.set("v.conName", event.currentTarget.dataset.contactname);
        component.set("v.cusEmail", event.currentTarget.dataset.contactemail);
        component.set("v.cusPhone", event.currentTarget.dataset.contactphone);
        component.set("v.cusMobile", event.currentTarget.dataset.contactmobile);         
        
        component.set("v.listOfSearchRecords", null );
        var forclose = component.find("typeahead-00B-9722");
        $A.util.addClass(forclose, 'lookup_menu_close');
        $A.util.removeClass(forclose, 'lookup_menu_open');
    },
    
    Validate : function(component, event, helper){
        var targetField = event.getSource();
        var targetFieldLabel = targetField.get("v.label");
        var targetFieldValue = targetField.get("v.value");
        var level1 = component.get("v.cmObject");
        for(var x in level1)
        {
            var level2 = level1[x];
            for(var y in level2)
            {
                var level3 = level2[y];
                for(var z in level3)
                {                              
                    if(targetFieldLabel == level3[z].fieldLabel)
                    {
                        console.log(level3[z].fieldType); 
                        if(level3[z].fieldType == 'PHONE' && targetFieldValue!= undefined && targetFieldValue!=''){                           
                            if(targetFieldValue.match(/^\d{10}$/)) 
                            {  
                                targetField.set("v.errors",null); 
                            }  
                            else  
                            {                                   
                                targetField.set("v.errors", [{message:"Please enter a valid phone number"}]);
                            } 
                        } 
                        if(level3[z].fieldType == 'EMAIL' && targetFieldValue!= undefined && targetFieldValue!=''){
                            if(targetFieldValue.match( /^[^\s@]+@[^\s@]+\.[^\s@]+$/)) 
                            {  
                                targetField.set("v.errors",null); 
                            }  
                            else  
                            {                                   
                                targetField.set("v.errors", [{message:"Please enter a valid email address"}]);
                            }          
                        }
                        if(level3[z].fieldType == 'STRING'){
                           if(targetFieldLabel == 'Last Name')  
                           {
                               if(targetFieldValue!='') 
                           		{  
                                	targetField.set("v.errors",null); 
                            	}  
                            	else  
                            	{                                   
                                	targetField.set("v.errors", [{message:"Please fill in the Last Name"}]);
                            	}
                           }
                             
                        } 
                    }
                }                 
            }
        }
    }
})