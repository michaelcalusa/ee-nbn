({
    getRole: function(component){
        console.log(' ** Helper - getRole **');
        var action = component.get("c.getRoles"); 
        action.setParams({
            "accId": component.get("v.accountId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if (state === "SUCCESS") {
                // console.log('getRole ---->' + JSON.stringify(response.getReturnValue()));
                component.set("v.roleOptions", response.getReturnValue());
            } 
        });
        $A.enqueueAction(action);
    },
    
    getRoleTypes: function(component){
        console.log(' ** Helper - getRoleTypes **');
        var action = component.get("c.getRoleTypes");
        console.log("roleValue ==>"+ component.find("roleId").get("v.value"));
         console.log("accId ==>"+ component.get("v.accountId"));
        action.setParams({
            "roleValue": component.find("roleId").get("v.value"),
            "accId": component.get("v.accountId")
        });  
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('getRoleTypes ---->' + JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS") {
                component.set("v.roleTypeOptions", response.getReturnValue());
                component.set("v.disableFlag", "false");
                //var res = component.get("v.roleTypeOptions");
               /* console.log("length ==>" + res.length);
                helper.getContactMatrixRecords(component); 
            helper.noOfRoleTypeOptions(component); */  
            }
        });
        $A.enqueueAction(action);
    },
    
    hideNshowTables: function(component){
        
         console.log("***handleRoleTypeChange***");
        
        /* var findGeneralContactDetails = component.find("generalContactDetailsId");        
        $A.util.addClass(findGeneralContactDetails, 'showMe');*/
        
        var findNotesDetails = component.find("notesDetailsId");        
        $A.util.addClass(findNotesDetails, 'showMe');
        
        var findDetailSection = component.find("detailSectionId");
        $A.util.removeClass(findDetailSection, 'showMe');
        $A.util.addClass(findDetailSection, 'hideMe'); 
        
         var findEditSection = component.find("editSectionId"); 
        $A.util.removeClass(findEditSection, 'showMe');
        $A.util.addClass(findEditSection, 'hideMe');
        
        var findTableDetails1 = component.find("TableDetailsId"); 
        $A.util.removeClass(findTableDetails1, 'hideMe');
        $A.util.addClass(findTableDetails1, 'showMe');
        
        var findSuccessMessage = component.find("successMessageId");
        $A.util.addClass(findSuccessMessage, 'hideMe');
        $A.util.removeClass(findSuccessMessage, 'showMe');
        
        var findRoleAndRoleType = component.find("role_roleTypeId");
        $A.util.removeClass(findRoleAndRoleType, 'hideMe');
        $A.util.addClass(findRoleAndRoleType, 'showMe');
        
        var findRoleAndRoleTypeDetails = component.find("role_roleType_details_Id");
        $A.util.removeClass(findRoleAndRoleTypeDetails, 'showMe');
        $A.util.addClass(findRoleAndRoleTypeDetails, 'hideMe');
        
        var findMyContactsTitleMessage = component.find("myContactsTitleMessageId");
        $A.util.removeClass(findMyContactsTitleMessage, 'hideMe');
        $A.util.addClass(findMyContactsTitleMessage, 'showMe');
        
         var findCustomerButtons = component.find("CustomerButtonsId");
        $A.util.removeClass(findCustomerButtons, 'showMe');
        $A.util.addClass(findCustomerButtons, 'hideMe');
        
        var findnbnButtons = component.find("nbnButtonsId");
        $A.util.removeClass(findnbnButtons, 'showMe');
        $A.util.addClass(findnbnButtons, 'hideMe');
        
        var findCustomerButtons = component.find("CustomerButtonsId_Top");
        $A.util.removeClass(findCustomerButtons, 'showMe');
        $A.util.addClass(findCustomerButtons, 'hideMe');
        
        var findnbnButtons = component.find("nbnButtonsId_Top");
        $A.util.removeClass(findnbnButtons, 'showMe');
        $A.util.addClass(findnbnButtons, 'hideMe');
    },
       
    getContactMatrixRecords: function(component, event){
        console.log(' ** Helper - getContactMatrixRecords **');
        var rValue = component.find("roleId").get("v.value");
        //var rtValue = component.find("roleTypeId").get("v.value")
        //&& ( rtValue == 'Enquiry/Billing Change Contact' || rtValue == 'Accounts Payable Contact') || rtValue == 'Billing Dispute Contact' || rtValue == 'All' || rtValue == 'Account Inforamtion'
        if((rValue == 'Operational - Billing'))
        {
            component.set("v.isEditor","false");
            component.set("v.showBillingMessage","true");            
            console.log("isEditor ==> "+component.get("v.isEditor"));
        }
        else
        {
            component.set("v.isEditor",component.get("v.isThisEditorAccess"));
            component.set("v.showBillingMessage","false"); 
            console.log("isEditor2 ==> "+component.get("v.isEditor"));
        }
        
		
        var action = component.get("c.getContactMatrixRecords");
        //component.find("roleTypeId").get("v.value"),
        var ct = component.get("v.ContactType");
        console.log("v.ContactType ==> "+ component.get("v.ContactType"));
        var conType;
        if(ct == undefined){
            conType="";
        }
        else
        {
            conType = ct;
        }
        
        var sortOrder;
        var sf = component.get("v.SortingField");
        if(sf == "Name" && ct == "cus")
        {            
            sortOrder = component.get("v.CusNameSortingOrder");
        }
        else if(sf == "Role Type" && ct == "cus")
        {
            sortOrder = component.get("v.CusRoleTypeSortingOrder");
        }
        else if(sf == "Name" && ct == "nbn")
        {
             sortOrder = component.get("v.nbnNameSortingOrder");    
        }
        else if(sf == "Role Type" && ct == "nbn")
        {
            sortOrder = component.get("v.nbnRoleTypeSortingOrder");
        }
        else
        {
            sortOrder = "";
        }
        
        action.setParams({
            "roleValue": component.find("roleId").get("v.value"),
            "roleTypeValue": "",
            "fsType": "CUS",
           "accountID":component.get("v.accountId"),
           "contactType": conType,
            "fieldToSort": component.get("v.SortingField"),
            "SortingOrder": sortOrder
        });
		//console.log('**** contactstype ==>'+ event.currentTarget.dataset.contactstype);
		
        
        if(ct == undefined || ct == "cus")
        {
            console.log('**** cus ContactType ==>'+ component.get("v.ContactType"));
            action.setCallback(this, function(response){
            var state = response.getState();
            console.log('result ---->' + JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS") {
                component.set("v.cmObject_CUS_default", response.getReturnValue());                
                component.set("v.Heading3","Customer");
                if(response.getReturnValue().length>0)
                {
                    component.set("v.Heading3","Customer");
                    var cmObjectCusDefault = component.get("v.cmObject_CUS_default");
                    for(var x in cmObjectCusDefault)
                	{
                    var level2 = cmObjectCusDefault[x];
                    for(var y in level2)
                    {
                        var level3 = level2[y];
                        for(var z in level3)
                        {   
                            console.log('***level3[z].Notes ==>'+ level3[z].Notes);
                            if(level3[z].Notes!= "" && level3[z].Notes!= undefined)
                            {
                                component.set("v.tmNotes", level3[z].Notes); 
                            } 
                            else
                            {
                                component.set("v.tmNotes", "Notes not available"); 
                            }
						}
					}
				   }
                    
                }
                else
                {
                    component.set("v.Heading3","");
                }
           	 }
        	});
            
            if(sf == "Name" && ct == "cus")
            {            
                sortOrder = component.get("v.CusNameSortingOrder");
                if(sortOrder == "ASC") 
                {
                   component.set("v.CusNameSortingOrder","DESC"); 
                }
                else
                {
                    component.set("v.CusNameSortingOrder","ASC"); 
                }  
            }
            else if(sf == "Role Type" && ct == "cus")
            {
                sortOrder = component.get("v.CusRoleTypeSortingOrder");
                if(sortOrder == "ASC") 
                {
                   component.set("v.CusRoleTypeSortingOrder","DESC"); 
                }
                else
                {
                    component.set("v.CusRoleTypeSortingOrder","ASC"); 
                }  
            }           
            
         /*   if(ct == "cus")
            {
                var so = component.get("v.SortingOrder");
                if(so == "ASC") 
                {
                   component.set("v.SortingOrder","DESC"); 
                }
                else
                {
                    component.set("v.SortingOrder","ASC"); 
                }                
            }*/
           $A.enqueueAction(action);
        }
        
        
            var action1 = component.get("c.getContactMatrixRecords");
           
        action1.setParams({
            "roleValue": component.find("roleId").get("v.value"),
            "roleTypeValue": "",
            "fsType": "NBN",
            "accountID":component.get("v.accountId"),
            "contactType": conType,
             "fieldToSort": component.get("v.SortingField"),
            "SortingOrder": sortOrder
        });
        
        if(ct == undefined || ct == "nbn"){
        console.log('**** nbn ContactType ==>'+ component.get("v.ContactType"));
        action1.setCallback(this, function(response){
            var state = response.getState();
            console.log('nbn default result1 ---->' + JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS") {
                component.set("v.cmObject_NBN_default", response.getReturnValue()); 
                
                if(response.getReturnValue().length>0)
                {
                    component.set("v.Heading2","nbn");
                     var cmObjectnbnDefault = component.get("v.cmObject_NBN_default");
                    for(var x in cmObjectnbnDefault)
                	{
                    var level2 = cmObjectnbnDefault[x];
                    for(var y in level2)
                    {
                        var level3 = level2[y];
                        for(var z in level3)
                        {   
                            console.log('***level3[z].Notes ==>'+ level3[z].Notes);
                            if(level3[z].Notes!= "" && level3[z].Notes!= undefined)
                            {
                                component.set("v.tmNotes", level3[z].Notes); 
                            } 
                            else
                            {
                                component.set("v.tmNotes", "Notes not available"); 
                            }
						}
					}
				   }
                }
                else
                {
                    component.set("v.Heading2","");
                }
                
              }
          });
            
           
            if(sf == "Name" && ct == "nbn")
            {
                sortOrder = component.get("v.nbnNameSortingOrder");
                if(sortOrder == "ASC") 
                {
                   component.set("v.nbnNameSortingOrder","DESC"); 
                }
                else
                {
                    component.set("v.nbnNameSortingOrder","ASC"); 
                }   
            }
            else if(sf == "Role Type" && ct == "nbn")
            {
                sortOrder = component.get("v.nbnRoleTypeSortingOrder");
                if(sortOrder == "ASC") 
                {
                   component.set("v.nbnRoleTypeSortingOrder","DESC"); 
                }
                else
                {
                    component.set("v.nbnRoleTypeSortingOrder","ASC"); 
                } 
            }  
            
        /*   if(ct == "nbn")
            {
                var so = component.get("v.SortingOrder");
                if(so == "ASC") 
                {
                   component.set("v.SortingOrder","DESC"); 
                }
                else
                {
                    component.set("v.SortingOrder","ASC"); 
                }                
            } */
          $A.enqueueAction(action1); 
        }  
        
       
        
        
        component.set("v.roleValue",component.find("roleId").get("v.value")); 
        //component.set("v.roleTypeValue",component.find("roleTypeId").get("v.value"));
    },
    
     readContactMatrixRecord: function(component, event, editFlag) {
        console.log(' ** Helper - readContactMatrixRecord **');
         console.log(' ** Helper - editFlag' + editFlag);
              
        var action = component.get("c.getSelectedContactMatrixRecord"); 
         console.log('role value ==>'+ component.find("roleId").get("v.value"));
         console.log('&&&& &&&&  rtype ==>'+ event.currentTarget.dataset.rtype);
       // console.log('roleType value ==>'+ component.find("roleTypeId").get("v.value"));
        console.log('record Id ==>'+ event.currentTarget.dataset.record);
        console.log('fsType ==>'+ event.currentTarget.dataset.fieldsettype);
         
        // component.set("v.tmRecorsId", event.currentTarget.dataset.record);
        // component.find("roleTypeId").get("v.value"), 
        var fsType = event.currentTarget.dataset.fieldsettype
        action.setParams({
            "roleValue": component.find("roleId").get("v.value"),
            "roleTypeValue": event.currentTarget.dataset.rtype,         
            "recordId": event.currentTarget.dataset.record,
             "fsType": fsType
        });
        action.setCallback(this, function(response){
            var state = response.getState();
             console.log('getSelectedContactMatrixRecord result ---->' + JSON.stringify(response.getReturnValue()));
            if (state === "SUCCESS") {
                component.set("v.cmObject", response.getReturnValue()); 
                component.set("v.ShowButtons","true");                
                if(fsType == 'Customer' )
                {
                    //component.set("v.ShowEdit","true");
                    var findCustomerButtons = component.find("CustomerButtonsId");
                    $A.util.removeClass(findCustomerButtons, 'hideMe');
        	        $A.util.addClass(findCustomerButtons, 'showMe');
                    
                    var findnbnButtons = component.find("nbnButtonsId");
                    $A.util.removeClass(findnbnButtons, 'showMe');
        	        $A.util.addClass(findnbnButtons, 'hideMe');
                    
                    var findCustomerButtons = component.find("CustomerButtonsId_Top");
                    $A.util.removeClass(findCustomerButtons, 'hideMe');
        	        $A.util.addClass(findCustomerButtons, 'showMe');
                    
                    var findnbnButtons = component.find("nbnButtonsId_Top");
                    $A.util.removeClass(findnbnButtons, 'showMe');
        	        $A.util.addClass(findnbnButtons, 'hideMe');
                }
                else
                {
                    var findnbnButtons = component.find("nbnButtonsId");
                    $A.util.removeClass(findnbnButtons, 'hideMe');
        	        $A.util.addClass(findnbnButtons, 'showMe');
                    
                    var findCustomerButtons = component.find("CustomerButtonsId");
                    $A.util.removeClass(findCustomerButtons, 'showMe');
        	        $A.util.addClass(findCustomerButtons, 'hideMe');
                    
                    var findnbnButtons = component.find("nbnButtonsId_Top");
                    $A.util.removeClass(findnbnButtons, 'hideMe');
        	        $A.util.addClass(findnbnButtons, 'showMe');
                    
                    var findCustomerButtons = component.find("CustomerButtonsId_Top");
                    $A.util.removeClass(findCustomerButtons, 'showMe');
        	        $A.util.addClass(findCustomerButtons, 'hideMe');
                }
                
                if(editFlag=="true")
                {
                    var findnbnButtons = component.find("nbnButtonsId");
                    $A.util.removeClass(findnbnButtons, 'showMe');
        	        $A.util.addClass(findnbnButtons, 'hideMe');
                    
                    var findCustomerButtons = component.find("CustomerButtonsId");
                    $A.util.removeClass(findCustomerButtons, 'showMe');
        	        $A.util.addClass(findCustomerButtons, 'hideMe');
                    
                    var findnbnButtons = component.find("nbnButtonsId_Top");
                    $A.util.removeClass(findnbnButtons, 'showMe');
        	        $A.util.addClass(findnbnButtons, 'hideMe');
                    
                    var findCustomerButtons = component.find("CustomerButtonsId_Top");
                    $A.util.removeClass(findCustomerButtons, 'showMe');
        	        $A.util.addClass(findCustomerButtons, 'hideMe');
                }
                var level1 = component.get("v.cmObject");
                for(var x in level1)
                {
                    var level2 = level1[x];
                    for(var y in level2)
                    {
                        var level3 = level2[y];
                        for(var z in level3)
                        {   
                            console.log('Field Labels'+ level3[z].fieldLabel);
                            if(level3[z].recordId!= "")
                            {
                                component.set("v.tmRecordId", level3[z].recordId); 
                            } 
                            // console.log('z recordId ==>'+ component.get("v.tmRecordId"));
                            if(level3[z].cusContactId!= "")
                            {
                                component.set("v.conId", level3[z].cusContactId); 
                            } 
                            if(level3[z].fieldLabel== "Last Name")
                            {
                               if(level3[z].fieldValue!=undefined)
                                { 
                                    var LName = level3[z].fieldValue;
                                }
                                else
                                {
                                    var LName = '';
                                }
                            } 
                            if(level3[z].fieldLabel== "First Name")
                            {
                                if(level3[z].fieldValue!=undefined){
                                    var FName = level3[z].fieldValue;  
                                }
                                else
                                {
                                    var FName='';
                                }
                            }
                            if(level3[z].fieldLabel== "Customer Contact Phone")
                            {
                                component.set("v.cusPhone", level3[z].fieldValue); 
                            }
                            if(level3[z].fieldLabel== "Customer Contact Mobile Phone")
                            {
                                component.set("v.cusMobile", level3[z].fieldValue); 
                            }
                            if(level3[z].fieldLabel== "Customer Contact Email")
                            {
                                component.set("v.cusEmail", level3[z].fieldValue); 
                            }
                        }
                    }
                }
                var nameValue = FName+' '+LName;
                var nValue = nameValue.trim();               
                component.set("v.conName", nValue); 
                
            }
        }); 
        $A.enqueueAction(action);
     },
    
    searchHelper : function(component,event,getInputkeyWord) {
        console.log('** Helper - searchHelper **');
        var action = component.get("c.fetchContact"); 
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'accountID':component.get("v.accountId")
        });
        // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // if storeResponse size is equal 0 ,display No Result Found... message on screen.
                console.log('*** length'+ storeResponse.length);
                if (storeResponse.length == 0) {
                    console.log('No Result Found...');
                    component.set("v.SearchMessage", 'User not found, please contact your Customer Delivery Manager');
                    console.log('return value'+ component.get("v.SearchMessage"));
                } else {
                    component.set("v.SearchMessage", '');
                }                
                // set searchResult list with return value from server.
                // console.log('** StoreResponse ==>'+ storeResponse);
                component.set("v.listOfSearchRecords", storeResponse);
            }     
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    },   
})