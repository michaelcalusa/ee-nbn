({
    doInit : function(component, event, helper) {
        //helper.getCurrentIncidentObject(component);
        //helper.getIncidentNotes(component);
    },
    
    handleApplicationEventFired : function(cmp, event) {
        var context = event.getParam("parameter");
        console.log('context---'+context);
        cmp.set("v.mostRecentEvent", context);
        var numApplicationEventsHandled =
            parseInt(cmp.get("v.numApplicationEventsHandled")) + 1;
        cmp.set("v.numApplicationEventsHandled", numApplicationEventsHandled);
    },
    
    handleCurrentIncidentRecord: function(component, event, helper) {
        if(component.get('v.recordId') === event.getParam("recordId")){
            var incidentData = event.getParam("currentIncidentRecord");
            helper.getCurrentIncidentObject(component,incidentData);
        }
     }
 })