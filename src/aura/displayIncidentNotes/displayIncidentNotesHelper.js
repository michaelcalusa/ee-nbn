({
    //This below function is not required as in to remove the dependency on the custom setting.
    /*getIncidentNotes : function(component, event, helper) {        
        var action = component.get("c.getIncident");
        console.log('Record Id--->'+component.get("v.recordId"));
        action.setParams({
            strRecordId : component.get("v.recordId"),
            componentName : component.get("v.componentName"),
            sObjectName : component.get("v.sObjectName")
        });
        
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                    for(var i in result){
                        console.log('result[i].Incident_Notes__c--->'+result[i].Incident_Notes__c); 
                        if (!$A.util.isEmpty(result[i])){
                            component.set("v.incidentNotes", result[i].Incident_Notes__c);  
                        }  
                    }
                }
                else{
                }
            } else if(state == "ERROR"){
            }
            
        });
        //helper.getCurrentIncidentObject(component, event, helper);
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },*/
    
    getCurrentIncidentObject: function(component, incidentData) {
        var dataIM = incidentData.inm;
        console.log('Object Return Values-->'+ incidentData);
        component.set("v.attrCurrentIncidentRecord",incidentData);  
        var currentIncidentWrapper = component.get("v.attrCurrentIncidentRecord");
        console.log('Incident Notes -->', dataIM.Incident_Notes__c); 
        if(!$A.util.isEmpty(dataIM) && !$A.util.isUndefined(dataIM)){
            for(var i in dataIM){
                if (!$A.util.isEmpty(dataIM.Incident_Notes__c)){
                    component.set("v.incidentNotes", dataIM.Incident_Notes__c); 
                }  
            }
        }
    }
    
})