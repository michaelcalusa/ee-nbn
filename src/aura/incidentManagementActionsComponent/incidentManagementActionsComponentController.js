({
    doInit : function(component, event, helper) {
        component.set("v.refreshActionPanelCondition", false);
        helper.getIncident(component, event, helper);
        helper.setTimer(component);
       
    },
    handleWaitingForUpdateEvent : function(component, event, helper) {
        var waitingForUpdate = event.getParam("waitingForUpdate");
        if(waitingForUpdate) {
            component.set("v.disableActionLink", true);
        }
        else {
            component.set("v.refreshActionPanelCondition", false);
            helper.getIncident(component, event, helper);
        }
    },
    handleRecordUpdated : function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "CHANGED") {
            var changedFields = eventParams.changedFields;
            //var changedFieldsJSONString = JSON.stringify(changedFields);
            var updatedRecord = component.get("v.incidentRecord");
            console.log('updatedRecord are ' + JSON.stringify(updatedRecord)); 
            console.log('changedFields are ' + JSON.stringify(changedFields));
            var changedFieldsJSONString = JSON.stringify(helper.calculateChangeFieldJSON(component.get('v.oldIncidentRecord'),updatedRecord));
            component.set('v.oldIncidentRecord',JSON.parse(JSON.stringify(updatedRecord)));
            // CUSTSA-16673 Update Fault Type
            if(!updatedRecord.RPA_Error_Occurred__c && updatedRecord.User_Action__c =='updateOperationCategorisation'){  
                if(changedFieldsJSONString.indexOf('Op_Cat__c') != -1 || changedFieldsJSONString.indexOf('Fault_Type__c') != -1) {
                    var appEvent = $A.get("e.c:JIGSAW_IncidentRecBroadcast");
                    var varAttrCurrentIncidentRecord = component.get("v.attrCurrentIncidentRecord");
                    varAttrCurrentIncidentRecord.inm.Op_Cat__c = updatedRecord.Op_Cat__c;
                    varAttrCurrentIncidentRecord.inm.Fault_Type__c = updatedRecord.Fault_Type__c;
                    component.set('v.incidentRecord.Op_Cat__c',updatedRecord.Op_Cat__c);
                    component.set('v.incidentRecord.Fault_Type__c',updatedRecord.Fault_Type__c);
                    appEvent.setParams({
                        "currentIncidentRecord" : varAttrCurrentIncidentRecord,
                        "recordId" : component.get("v.recordId")
                    });
                    appEvent.fire();
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title"   : $A.get("$Label.c.Jigsaw_Update_Op_Cat_Success_Title"),
                        "type"   : "Success",
                        "message": $A.get("$Label.c.Jigsaw_Update_Op_Cat_Success_Message")
                    });
                    toastEvent.fire();
                    component.set('v.disableActionLink',false);
                }
            }
            
            //fire the auto refresh for work log component
            if(changedFieldsJSONString.indexOf('Add_Note_Change__c') != -1) {
                var addNoteRefreshEvent = $A.get("e.c:Add_Note_Refresh_Event");
                addNoteRefreshEvent.fire();
            }
            //Fire auto refresh for work order history
            if(changedFieldsJSONString.indexOf('Current_work_order_status__c') != -1 ) {
                var responseWRStatus = updatedRecord.Current_work_order_status__c;
                if(responseWRStatus && responseWRStatus.indexOf('STARTED') != -1) {
                    var workHistoryRefreshEvent = $A.get("e.c:Jigsaw_RefreshWorkHistory");
                    workHistoryRefreshEvent.setParam("incidentRecordId", component.get("v.recordId"));
                    workHistoryRefreshEvent.fire();
                }
            }
            //Add code related to JIRA CUSTA-20303
            if(changedFieldsJSONString.indexOf('Jigsaw_WO_TOW_Response_Status')!= -1) {
                var responseStatus = updatedRecord.Jigsaw_WO_TOW_Response_Status__c;
                // Added undefined check for responseStatus for MRTEST-19792
                if(responseStatus && responseStatus.indexOf("ERROR") !== -1) {
                    if(updatedRecord.User_Action__c =='offsiteTechnician'){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": $A.get("$Label.c.OffsiteTechnicianErrorHeader"),
                            "type": "error",
                            "message": updatedRecord.Jigsaw_WO_TOW_Response_Message__c
                        });
                    }else if(updatedRecord.User_Action__c =='onsiteTechnician'){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": $A.get("$Label.c.OnsiteTechnicianErrorHeader"),
                            "type": "error",
                            "message": updatedRecord.Jigsaw_WO_TOW_Response_Message__c
                        });
                    }else if(updatedRecord.User_Action__c =='techNote') {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": $A.get("$Label.c.TechNoteErrorHeader"),
                            "type": "error",
                            "message": updatedRecord.Jigsaw_WO_TOW_Response_Message__c
                        });
                    }
                    toastEvent.fire();
                    var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
                    spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : updatedRecord, "spinnersToStart" : 'IncidentStatus', "stopSpinner" : true,"showSuccessToastMessage" : false,"hardRefreshComponent" : true});
                    spinnerHandlerEvent.fire();
                    helper.getIncident(component, event, helper);
                    component.set('v.disableActionLink',false);
                }
                if(responseStatus && responseStatus.indexOf("SUCCESS") !== -1)  {
                    if(updatedRecord.User_Action__c =='offsiteTechnician'){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "message": $A.get("$Label.c.OffsiteTechnicianSubmitSecondRequest")
                        });
                        toastEvent.fire();
                    }else if(updatedRecord.User_Action__c =='onsiteTechnician'){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "message": $A.get("$Label.c.OnsiteTechnicianSubmitSecondRequest")
                        });
                        toastEvent.fire();
                    }else if(updatedRecord.User_Action__c =='techNote'){
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title"   : "Success",
                            "type"   : "success",
                            "message" : $A.get("$Label.c.TechNoteSubmitSucess")
                        });
                        toastEvent.fire();
                    }
                    
                }
            }
            // END CUSTA-20303
            //Start of Incident Status Spinner and toast messages section.
            //Check if the change is related to Engagement type actions (success case), show success and RPA Grey toast messages.
            if(changedFieldsJSONString.indexOf('RandomAction__c') != -1 && changedFieldsJSONString.indexOf('MaximoEngagementActionPending__c')!= -1 && changedFieldsJSONString.indexOf('CancelAppointmentErrorMessage__c') == -1) {
                var cancelMessage ='';
                if(updatedRecord.Engagement_Type__c === "Appointment" && updatedRecord.User_Action__c === "changeEUETCommitment") {            
                    cancelMessage ="Appointment cancelled successfully!";
                }
                else if(updatedRecord.Engagement_Type__c === "Commitment" && updatedRecord.User_Action__c === "changeEUETAppointment") {            
                    cancelMessage ="Commitment cancelled successfully!";
                }
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title" : "Submitting Request",
                    "message" : updatedRecord.DynamicToastForIncident__c
                });
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": cancelMessage
                });
                toastEvent.fire();
                resultsToast.fire();
            }
            //Check if the change is related to Engagement type actions maximo platform event(Error Case),Then call spinner handler to stop the spinner and show exception message on incident status componet for user manual intervention.
            else if(!updatedRecord.RPA_Error_Occurred__c && updatedRecord.MaximoEngagementActionPending__c && (changedFieldsJSONString.indexOf('CancelAppointmentErrorMessage__c') != -1 && updatedRecord.CancelAppointmentErrorCode__c != "013001"))
            {
                helper.handleSpinners(component,helper,"IncidentStatus",false,true);
            }
            //Check if the change is related to Dispatch Technician maximo platform event(Success Case),Then call spinner handler to start the spinner for sending notification to RPA.
            //Added or updated as part of User Story CUSTA-20487
                else if (!updatedRecord.RPA_Error_Occurred__c && (changedFieldsJSONString.indexOf('User_Action__c') != -1 && updatedRecord.User_Action__c === 'dispatchTechnician'))
                {
                    var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
                    spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : updatedRecord, "spinnersToStart" : 'IncidentStatus', "stopSpinner" : false,"showSuccessToastMessage" : false,"hardRefreshComponent" : false});
                    spinnerHandlerEvent.fire();
                }
            //Check if the change is related to Dispatch Technician maximo platform event(Error Case),Then call spinner handler to stop the spinner and enable operator actions.
            //Added or updated as part of User Story CUSTA-20487
                    else if (!updatedRecord.RPA_Error_Occurred__c && updatedRecord.User_Action__c === 'createWorkRequest' && changedFieldsJSONString.indexOf('DispatchTechExceptionMessage__c') != -1 && updatedRecord.DispatchTechExceptionMessage__c)
                    {
                        console.log("Dispatch Tech Exception Message :- " + updatedRecord.DispatchTechExceptionMessage__c);
                        var dispatchTechErrormeesage = $A.get('e.force:showToast');
                        var errorMessage = $A.get("$Label.c.DispatchTechErrorMessage");
                        dispatchTechErrormeesage.setParams({
                            type: 'error',
                            message: errorMessage + '\n Error Details :- ' +updatedRecord.DispatchTechExceptionMessage__c,
                            key:'info',
                            type:'error',
                            duration:30000
                        });
                        dispatchTechErrormeesage.fire();
                        helper.handleSpinners(component,helper,"IncidentStatus",false,false);
                        component.set("v.refreshActionPanelCondition", false);
                        helper.getIncident(component, event, helper);
                    }
            //Check if the change is related to Current Awaiting Status and Current SLA Status,Then call spinner handler to stop the spinners.
                        else if (!updatedRecord.RPA_Error_Occurred__c && (changedFieldsJSONString.indexOf('Awaiting_Current_Incident_Status__c') != -1 && !updatedRecord.Awaiting_Current_Incident_Status__c && changedFieldsJSONString.indexOf('Awaiting_Current_SLA_Status__c') != -1 && !updatedRecord.Awaiting_Current_SLA_Status__c))
                        {
                            helper.handleSpinners(component,helper,"IncidentStatus,SLA",true,false);
                        }
            //Check if the change is related to Current Awaiting Status only ,Then call spinner handler to stop the spinner.
                            else if (!updatedRecord.RPA_Error_Occurred__c && (changedFieldsJSONString.indexOf('Awaiting_Current_Incident_Status__c') != -1 && !updatedRecord.Awaiting_Current_Incident_Status__c) && changedFieldsJSONString.indexOf('DispatchTechExceptionMessage__c') == -1)
                            {
                                helper.handleSpinners(component,helper,"IncidentStatus",true,false);
                                //Check the status of disable operations checkbox and teh user action if the useraction is si ni linking action send refresh spinner event to incident status component.
                                if(changedFieldsJSONString.indexOf('DisableOperatorActions__c') != -1 && updatedRecord.DisableOperatorActions__c && updatedRecord.User_Action__c == "relateToIncident")
                                {
                                    component.set("v.disableActionLink", true);
                                }
                                //Check if the change is caused due to changeEuETCommitment as there is no change in current status we need to refresh the operator actions in the below section.
                                if(updatedRecord.User_Action__c == "changeEUETCommitment" && updatedRecord.Engagement_Type__c == "Commitment")
                                {
                                    component.set("v.refreshActionPanelCondition", false);
                                    helper.getIncident(component, event, helper);
                                }
                                if(updatedRecord.User_Action__c == "dispatchTechnician" || updatedRecord.User_Action__c === "onsiteTechnician" || updatedRecord.User_Action__c == "User Assigned")
                                {
                                    component.set("v.refreshActionPanelCondition", false);
                                    helper.getIncident(component, event, helper);
                                }                
                            }
            //Check if the change is related to Current Awaiting SLA Staus only ,Then call spinner handler to stop the spinner.
                                else if(!updatedRecord.RPA_Error_Occurred__c && (changedFieldsJSONString.indexOf('Awaiting_Current_SLA_Status__c') != -1 && !updatedRecord.Awaiting_Current_SLA_Status__c))
                                {
                                    helper.handleSpinners(component,helper,"SLA",true,false);
                                }
            //Applicable For SINI Linking Operation Action.
            if(changedFieldsJSONString.indexOf('Awaiting_Current_Incident_Status__c') != -1 && !updatedRecord.Awaiting_Current_Incident_Status__c && updatedRecord.User_Action__c && updatedRecord.User_Action__c == "relateToIncidentCompleted")
            {
                //Gray Toast Message section for si/ni linking action.
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "message" : "Submitting Request" + "\n" + updatedRecord.DynamicToastForIncident__c
                });
                resultsToast.fire(); 
            }
            if(changedFieldsJSONString.indexOf('User_Action__c') != -1 && updatedRecord.User_Action__c == "relateToIncidentCompleted")
            {
                //Refresh Banner Component after success of linking action.
                var refreshNetworkBannerEvent = $A.get("e.c:JIGSAW_IncidentRecBroadcast");
                refreshNetworkBannerEvent.setParams({
                    "currentIncidentRecord" : component.get("v.attrCurrentIncidentRecord"),
                    "recordId" : component.get("v.recordId")
                });
                refreshNetworkBannerEvent.fire();
                //fire green toast message for sini linking.
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success",
                    "type": "success",
                    "message": "Link incident(s) successfully completed." + "\n" + "See Recent History for record of the event."
                });
                toastEvent.fire();
                component.set("v.refreshActionPanelCondition", false); 
                component.set("v.showAssignToMeLink",false);
                component.set("v.hasNewStatus", false);
                component.set("v.canBeAssignedToDiffUser", false);
                component.set("v.Accept", "");
                helper.getIncident(component, event, helper);
            }
            //Check if the change is related to Current Status or RPA Error Occurred,Then Reload Operator Actions and call spinner handlers to stop the spinners.
            // Adding Current work order status to the current status change for the consideration of tech onsite and tech off site
            if( (
                    (
                        changedFieldsJSONString.indexOf('Current_Status__c') != -1 
                        || 
                        changedFieldsJSONString.indexOf('Appointment_Status__c') != -1
                    ) 
                    && 
                    changedFieldsJSONString.indexOf('User_Action__c') == -1 
                ) 
                || 
                changedFieldsJSONString.indexOf('User_Federation_Id__c') != -1 
                || 
                changedFieldsJSONString.indexOf('Current_work_order_status__c') != -1 
                || 
                (
                    (
                        changedFieldsJSONString.indexOf('RPA_Error_Occurred__c') != -1 
                        || 
                        updatedRecord.RPA_Error_Occurred__c
                    )
                    &&
                    !changedFieldsJSONString.includes('RPA_Retry_Counter__c')
                )
              )
            {
                console.log('refreshing Left Panel');
                // reload the attributes to reflect the current state
                component.set("v.refreshActionPanelCondition", false); 
                component.set("v.showAssignToMeLink",false);
                component.set("v.hasNewStatus", false);
                component.set("v.canBeAssignedToDiffUser", false);
                component.set("v.Accept", "");
                if (updatedRecord.RPA_Error_Occurred__c)
                {
                    helper.handleSpinners(component,helper,"IncidentStatus,SLA",false,false);
                }
                else if (((changedFieldsJSONString.indexOf('Current_Status__c') != -1 && changedFieldsJSONString.indexOf('Awaiting_Current_Incident_Status__c') == -1) || changedFieldsJSONString.indexOf('Current_work_order_status__c') != -1) && !updatedRecord.Awaiting_Current_Incident_Status__c)
                {
                    console.log('refreshing and stopping spinners');
                    helper.handleSpinners(component,helper,"IncidentStatus",false,true);	     
                }
                helper.getIncident(component, event, helper);
            }
            //Check if the change is related to Current SLA or Current SLA Status or SLA Type Tagged to incident then call spinner handler to reload SLA Component.
            if((changedFieldsJSONString.indexOf('Current_SLA__c') != -1 || changedFieldsJSONString.indexOf('SLA_Status__c') != -1 || changedFieldsJSONString.indexOf('SLA_Type__c') != -1) && !updatedRecord.Awaiting_Current_SLA_Status__c) {
                helper.handleSpinners(component,helper,"SLA",true,false);
                //helper.getIncident(component, event, helper);
            }
            // Network trail
            if((changedFieldsJSONString.indexOf('Network_Trail_Transaction_Id__c') != -1)) {
                helper.fireNetworktrailEvent(component,helper);
            }  
        }
        else if(eventParams.changeType === "LOADED") {
            if(!component.get('v.oldIncidentRecord') && component.get('v.incidentRecord')){
                 let incRec = component.get('v.incidentRecord');
                 component.set("v.oldIncidentRecord",JSON.parse(JSON.stringify(incRec)));
            }
        } 
            else if(eventParams.changeType === "REMOVED") {
                //do nothing or show a toast
            } 
                else if(eventParams.changeType === "ERROR") {
                    console.log('Error: ' + component.get("v.error"));
                }
    },
    setNetworkIncidentsAttribute : function(component,event,helper) {
        var recordId = event.getParam("recordId");
        if(recordId == component.get('v.recordId'))
        {
            var networkIncidentsObjectDetails = event.getParam("NetworkIncidentsObject");
            if(networkIncidentsObjectDetails)
            {
                component.set("v.NetworkIncidentsObjectDetails", networkIncidentsObjectDetails);
            }
        }
    },
    refreshActionPanel : function(component, event, helper){
        if(component.get('v.refreshActionPanelCondition')){
            if(component.get('v.incidentRecordObj.Current_Status__c') !== 'Rejected'){
                helper.evaluateVisibility(component,helper);
            }
            else{
                helper.enableOperatorActions(component,{'ADD_NOTES':[{'Enabled__c' : true}]});
            }
        }
    }
})