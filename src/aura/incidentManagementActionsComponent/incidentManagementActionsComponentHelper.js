({
    getIncident: function(component, event, helper) {
        var action = component.get("c.get_CurrentIncident");
        action.setParams({
            incidentId: component.get("v.recordId")
        });
        action.setCallback(this, function(data) {
            var status = data.getState();
            if (status == "SUCCESS") {
                var ret = data.getReturnValue();
                component.set("v.attrCurrentIncidentRecord",ret);
                
                // CUSTSA-28426 Setting IncObjRecord to bind across left navigation panel
                var dataIM = data.getReturnValue().inm;
                component.set("v.incidentRecordObj",dataIM);
                this.handlerRPAError(component,dataIM);
                
                //Event to fire with the current incident record values.
                var appEvent = $A.get("e.c:JIGSAW_IncidentRecBroadcast");
                appEvent.setParams({
                    "currentIncidentRecord" : ret,
                    "recordId" : component.get("v.recordId")
                });
                appEvent.fire();
                
                // Reinitializing the left action panel components with updated data
                if(!component.get('v.disableActionLink') || !component.get("v.refreshActionPanelCondition")){
                    console.log('IncidentManagementActions Cmp : Refreshing Action panel');
                    component.set("v.refreshActionPanelCondition", false);
                    component.set("v.refreshActionPanelCondition",true);
                }
            }
        });
        $A.enqueueAction(action);
    },
    setTimer:  function(component) {
        var that = this;
        window.setTimeout(
            $A.getCallback(function() {
                if(component.isValid())
                {
                    if(component.find("incRecordLoader") != undefined) {
                        component.find("incRecordLoader").reloadRecord();
                        that.setTimer(component);
                    }
                }
            }),1000 //time after which the component reloads to check for any change in the record data
        );
    },
    handlerRPAError : function(component,dataIM){
        if(dataIM.RPA_Error_Occurred__c){
            component.set("v.disableActionLink", true);
            var toastEvent = $A.get('e.force:showToast');
            if(dataIM.User_Action__c != "relateToIncident")
            {
                toastEvent.setParams({
                    type: 'error',
                    message: component.get("v.RPAErrorMessage"),
                    key:'info',
                    type:'error',
                    duration:10000
                });
            }
            else
            {
                toastEvent.setParams({
                    type: 'error',
                    message: "Sorry, we were unable to perform this action in Remedy." + "\n" + 
                    "In the RPA Sync Error Log below select the 'Acknowledge' button" + "\n" +
                    "and try again using the 'Link incident(s)' Jigsaw operator action.",
                    key:'info',
                    type:'error',
                    duration:30000
                });
            }
            toastEvent.fire();
        }
        else{
            component.set("v.disableActionLink", (dataIM.Awaiting_Current_Incident_Status__c || dataIM.MaximoEngagementActionPending__c || dataIM.DisableOperatorActions__c));
        }
    },
    handleSpinners : function(component, helper, spinnerhandlernames, showSuccessToastMessage, hardRefresh) {
        var updatedRecord = component.get("v.incidentRecord");
        var spinnerHandlerEvent = $A.get("e.c:HandleSpinnerEvent");
        spinnerHandlerEvent.setParams({"attrCurrentIncidentRecord" : updatedRecord, "spinnersToStart" : spinnerhandlernames, "stopSpinner" : true,"showSuccessToastMessage" : showSuccessToastMessage,"hardRefreshComponent" : hardRefresh});
        spinnerHandlerEvent.fire();
    },
    fireNetworktrailEvent : function(component, helper) {
        var currentWorkingIncidentRecord = component.get("v.recordId");
        var networktrailevt = $A.get("e.c:JIGSAW_NetworkTrailEvt");        
        networktrailevt.setParam("networktrailCached",true);
        networktrailevt.setParam("recordId",currentWorkingIncidentRecord);
        networktrailevt.fire();
    },
    evaluateVisibility : function(component, helper){
        var action = component.get('c.validateOperatorActionVisibility');
        action.setParams({'inm': component.get('v.incidentRecordObj'),
                          'isLoggedInUserAsAssignee':component.get("v.attrCurrentIncidentRecord.isLoggedInUserEqualsAssignee"),
                          'currentUserGroup' : component.get("v.attrCurrentIncidentRecord.currentUserGroupName")});
        action.setCallback(this, function(response){
            let configs = response.getReturnValue();
            if(configs){
              var actionServiceCmp = component.find('ActionService');   
                actionServiceCmp.evaluateVisibility(configs);
                helper.enableOperatorActions(component, configs);
                component.set("v.mapComponentVisibility", configs);
                if(configs['JIGSAW_ONSITETECHNICIAN'] || configs['JIGSAW_OFFSITEETECHNICIAN']){
                    helper.retrieveWorkOrderValues(component);
                }
            }
        });
        $A.enqueueAction(action);
    },
    enableOperatorActions : function(component, configs){
        var actionComponents = component.find('OpAction');
        let compVisibilityMap = {};
		if(actionComponents && Array.isArray(actionComponents)){
			actionComponents.forEach((actionCmp) => {
            let cmpName = actionCmp.get('v.ComponentName'); 
            if(configs && cmpName && configs.hasOwnProperty(cmpName.toUpperCase()) && (configs[cmpName.toUpperCase()])){
                configs[cmpName.toUpperCase()].forEach((moduleConfig) => {
                    if(moduleConfig.Enabled__c){
            			compVisibilityMap[cmpName.toUpperCase()] = true;
                    	actionCmp.set('v.showOpActionLink',true);
                    }
                    if(moduleConfig.DeveloperName == 'AddExternalNote_OperatorAction' && moduleConfig.Enabled__c){
            			actionCmp.set('v.showExternalNoteLabel',true);
                    } else if(moduleConfig.DeveloperName == 'AddTechNote_OperatorAction' && moduleConfig.Enabled__c){
                        actionCmp.set('v.techNoteButtonVisibility',true);
                    }
            	});
    			configs[cmpName.toUpperCase()] = actionCmp.get('v.showOpActionLink');
            }
			});
		}
    },
	calculateChangeFieldJSON : function(oldRec, newRec){
        let changeFieldJSON = [];
        Object.keys(newRec).forEach((fieldAPI) => {
           if(!oldRec.hasOwnProperty(fieldAPI) || (newRec[fieldAPI] != oldRec[fieldAPI])){
                changeFieldJSON.push(fieldAPI)
            }
        });
        return changeFieldJSON;
        
    },													
	retrieveWorkOrderValues : function(component){
        var action = component.get("c.getWorkOrderValuesMap");
        var inm = component.get('v.incidentRecordObj');
        action.setBackground();
        if($A.util.isUndefined(component.get("v.Dayscount")))
            component.set("v.Dayscount",'60');
       
        action.setParams({
            incidentNumber : inm.Id,
            incidentName : inm.Name,
            dayCount : component.get('v.Dayscount'),
            actionStatus : inm.Current_Status__c
        });
        action.setCallback(this,(res) => {
            if(res.getState() == 'SUCCESS'){
            	var appEvent = $A.get("e.c:JIGSAW_WOOrderHistoryData");
            	appEvent.setParams({'wohistorywrapper' : res.getReturnValue(),'recordId':inm.Id});
        		appEvent.fire();
        	}
        });
        $A.enqueueAction(action);
    }
})