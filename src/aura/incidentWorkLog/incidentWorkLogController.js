({   
    doInit : function(component, event, helper) {
        helper.getChatterPosts(component);
	    var workLogFilters = $A.get("$Label.c.incidentLogFilterValues");
        var workLogFilterOptions = "[" +  workLogFilters.split(";") + "]";
        component.set("v.filterOptions", JSON.parse(workLogFilterOptions));
    },
    handleApplicationEventFired : function(cmp, event) {
        var context = event.getParam("parameter");
        cmp.set("v.mostRecentEvent", context);
        var numApplicationEventsHandled =
        parseInt(cmp.get("v.numApplicationEventsHandled")) + 1;
        cmp.set("v.numApplicationEventsHandled", numApplicationEventsHandled);
    },
    handleClick : function(component, event, helper) {
        var clickedRow = event.getSource().get("v.value");
        helper.helperFun(component,event,clickedRow);
    },
    handleExpandHistory : function(component,event,helper) {
        if(!component.get("v.boolExpandHistory"))
        {
            component.set("v.boolExpandHistory",true);
            helper.expandHistory(component,event,true);
        }
        else
        {
            component.set("v.boolExpandHistory",false);
            helper.expandHistory(component,event,false);
        }
    },
    handleAddNoteRefreshEventFired : function(component, event, helper) {
        helper.getChatterPosts(component, event, helper);
    },
    onSelectedFilter : function(component, event, helper) {
        var filterOptions = component.get("v.filterOptions");
        var fileteredCondition = helper.getFilterConditons(filterOptions,{key : "value", value : component.get("v.selectedFilterValue")});
        component.set('v.filterCondition',fileteredCondition[0].Condition);
        component.set("v.selectedIncidentLogFilterLabel",fileteredCondition[0].label);
        helper.getChatterPosts(component);
    },
    openSubtab: function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getEnclosingTabId().then(function(enclosingTabId) {
            workspaceAPI.openSubtab({
                parentTabId: enclosingTabId,
                pageReference: {
                    "type": "standard__component",
                    "attributes": {
                        "componentName": "c__JIGSAW_incidentWorkLogExpand"
                    },
                    "state": {
                        "uid": "1",
                        "c__mostRecentEvent" : component.get("v.mostRecentEvent"), 
                        "c__strCompTitle" : component.get("v.strCompTitle"),
                        "c__strVisibilityInt" : component.get("v.strVisibilityInt"),
                        "c__strRecordId" : component.get("v.recordId"),
                        "c__sObjectName" : component.get("v.sObjectName"),
                        "c__sortType" : component.get("v.strDateSort"),
                        "c__truncVal" : component.get("v.strTruncVal"),
                        "c__postType" : component.get("v.strPostType"),
                        "c__strFilterConditions" : component.get("v.filterCondition") ? component.get("v.filterCondition") : "",
                        "c__intBodyLength" : component.get("v.intBodyLength")
                    }
                }
            }).then(function(subtabId) {
                console.log("The new subtab ID is:" + subtabId);
                workspaceAPI.setTabLabel({
                    tabId: subtabId,
                    label: "Recent History"
                });
            }).catch(function(error) {
                console.log("error");
            });
        });
    },
})