({
    getChatterPosts : function(component, event, helper) {        
        var action = component.get("c.getChatterPosts");
        var lengthOfBodyToShow = component.get("v.intBodyLength");
        component.set("v.boolExpandHistory", false);
        console.log("Chatter Filters :- " + component.get("v.filterCondition"));
        action.setParams({
            strRecordId : component.get("v.recordId"),
            //componentName : component.get("v.componentName"),
            sObjectName : component.get("v.sObjectName"),
            sortType : component.get("v.strDateSort"),
            truncVal : component.get("v.strTruncVal"),
            postType : component.get("v.strPostType"),
            strFilterConditions : component.get("v.filterCondition") ? component.get("v.filterCondition") : ""
        });
        action.setCallback(this, function(a) {
            //get the response state
            var state = a.getState();
            if(state == "SUCCESS") {
                var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                    component.set("v.fItem", result);
                }
                else {
                    component.set("v.fItem", result);
                    $A.get('e.force:refreshView').fire();
                }
            } 
            else if(state == "ERROR"){
            }
        });
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },
    getObjects : function(component,recordToDisplay) { 
        var filterStr='';
        if(component.get("v.filterValues")) {
            var filterVals = component.get("v.filterValues");
            //alert('filterVals: '+filterVals);
            if(filterVals) {
                if(filterVals.length > 0) {
                    for(var i = 0;i<filterVals.length;i++) {
                        if(filterStr)
                            filterStr += ",'"+filterVals[i]+"'";                            
                        else
                            filterStr = '('+component.get("v.filterFields")[0]+component.get("v.filterOperators")[0]+"('"+filterVals[i]+"'";
                        
                    }
                    if(filterStr)
                        filterStr += '))';
                }
                /*else
                    filterStr = component.get("v.filterFields")[0]+component.get("v.filterOperators")[0]+"'"+component.get("v.filterValues")[0]+"'";*/
            }
        }    
        //alert('filterStr: '+filterStr);        
        var rowsPerPage = recordToDisplay;
        var action = component.get("c.getSObjectRecords");
        action.setParams({
            SObjectName : component.get("v.strObject"),
            componentName : component.get("v.componentName"),
            filterStr : filterStr
        });
        
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var result = a.getReturnValue();
                
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                    component.set("v.lstObjects",result);
                    var workLogItemsList = []; 
                    for(var i=0;i<result.length; i++){
                        workLogItemsList.push(result[i]);   
                    }
                }
                else{
                    component.set("v.lstObjects","");
                }
                
                component.set("v.workLogItemsList", workLogItemsList);
                this.setTableColumns(component);
                
            } else if(state == "ERROR"){
            }
        });   
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },
    expandHistory : function(component,event,expand) {
        var objectList = [];
        objectList = component.get("v.fItem");
        for(var i in objectList) {
            if(expand) {
                objectList[i].showMore = true;
            }
            else {
                objectList[i].showMore = false;
            }  
        }
        component.set("v.fItem", objectList);  
    },
    helperFun : function(component,event,clickedRow){
        var objectList = [];
        objectList = component.get("v.fItem");
        var label = event.getSource().get("v.label");
        for(var i in objectList){
            if(clickedRow == objectList[i].Id){
                if(label=='Show more'){
                    objectList[i].showMore = true;
                } 
                else{
                    objectList[i].showMore = false;
                }
                break;
            }
        }
        component.set("v.fItem", objectList);
        this.checkShowLessDetails(component);
    },
    checkShowLessDetails : function(component) {
        var showLessExists = false;
        var showLessBtnList = [];
        showLessBtnList = component.find("btnShowMore");
        for(var i = 0;i < showLessBtnList.length; i++)
        {
            if(showLessBtnList[i].get("v.label") === "Show less") {
                console.log("Inside For Loop");
                showLessExists = true;
            }
        }
        if(!showLessExists) {
            component.set("v.boolExpandHistory",false);
        }
        else
        {
            component.set("v.boolExpandHistory",true);
        }
    },
    getFilterConditons : function(feedItemsArray,searchobject) {
        var filteredArray = feedItemsArray.filter(function(currentitem) {
            return (currentitem[searchobject.key] === searchobject.value);
        });
        return filteredArray;
    }
})