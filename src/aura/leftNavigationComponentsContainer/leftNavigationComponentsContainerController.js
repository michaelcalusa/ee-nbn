({
    saveCache : function(component, event, helper){
        if(component.get('v.recordId') == event.getParam('recordId')){
            
            //get cache from work history module
            if(event.getParam('cacheList')){
                component.set('v.workHistoryCalloutDone',true);
                var cacheList = component.get("v.cacheList");
                var tempCacheList = component.get("v.cacheList") || [];
                var finalCacheList = tempCacheList.concat(event.getParam('cacheList'));
                component.set("v.cacheList",finalCacheList);
            }
            var serviceCalloutDone = component.get("v.serviceCalloutDone");
            var locationCalloutDone= component.get("v.locationCalloutDone");
            var workHistoryCalloutDone = component.get("v.workHistoryCalloutDone");
            var cacheList = component.get("v.cacheList");
            if(event.getParam('avcid'))
                component.set('v.avcid',event.getParam('avcid'));
            if(event.getParam('cpiid'))
                component.set('v.cpiid',event.getParam('cpiid'));
            if(event.getParam('locid'))
                component.set('v.locid',event.getParam('locid'));
            if(event.getParam('techtype'))
                component.set('v.techtype',event.getParam('techtype'));
            
            var incidentRecord = component.get('v.incidentRecord');
            var incObjFieldExists = incidentRecord && incidentRecord.AVC_Id__c && incidentRecord.Copper_Path_Id__c && incidentRecord.locId__c;
            
            if(serviceCalloutDone && locationCalloutDone && workHistoryCalloutDone && (!incObjFieldExists || (cacheList && cacheList.length))){
                var action = component.get("c.saveCacheRecords");
                action.setParams({            
                    cacheListParam :JSON.stringify(cacheList),
                    cpiid : component.get('v.cpiid'),
                    avcid : component.get('v.avcid'),
                    locid : component.get('v.locid'),
                    techtype : component.get('v.techtype'),
                    incId : component.get('v.recordId'),
                });  
                action.setBackground();        
                action.setCallback(this, function(response){  
                    if(response.getState() != 'SUCCESS' && !response.getReturnValue()){
                        console.log('Error in Saving Cache Record & updating values on inm record' + JSON.stringify(response.getError()));
                    }
                });
                $A.enqueueAction(action);
            }   
        }     
    }
})