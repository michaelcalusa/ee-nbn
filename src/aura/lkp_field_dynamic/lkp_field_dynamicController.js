({
    /*
     Init method runs at start
    */
    doInit : function(component, event, helper) {
		var lookupList = component.find('lookuplist');
		$A.util.addClass(lookupList, 'slds-hide');
        /*
        if(component.get("v.recordName") != '')
        {
            component.set("v.searchString", component.get("v.recordName"));
            // Hide the Input Element
            var inputElement = component.find('lookup');
            $A.util.addClass(inputElement, 'slds-hide');
            
            // Show the Lookup pill
            var lookupPill = component.find("lookup-pill");
            $A.util.removeClass(lookupPill, 'slds-hide');
            
            // Lookup Div has selection
            var inputElement = component.find('lookup-div');
            $A.util.addClass(inputElement, 'slds-has-selection');
        }
        */
    },
    
    /*
     Make the Sobjected Selected and hide the lookup input
    */
    showSelectedRecord : function(component, event, helper) {
        
        var selRecord = component.get("v.selectedRecord");
        if(selRecord != null)
        {
        	var selRecord = component.get("v.selectedRecord");
            
            var primaryField = component.get("v.primaryFieldAPI");
            
        	// Update the Searchstring with the Label
        	component.set("v.searchString", selRecord[primaryField]);
        
            //Hide the lookup result list
            var lookupList = component.find('lookuplist');
            $A.util.removeClass(lookupList, 'slds-show');
            $A.util.addClass(lookupList, 'slds-hide');
            
            // Hide the Input Element
            var inputElement = component.find('lookup');
            $A.util.addClass(inputElement, 'slds-hide');
            
            // Show the Lookup pill
            var lookupPill = component.find("lookup-pill");
            $A.util.removeClass(lookupPill, 'slds-hide');
            
            // Lookup Div has selection
            var inputElement = component.find('lookup-div');
            $A.util.addClass(inputElement, 'slds-has-selection');
        }
        else{
            helper.clearSelection(component);
        }
    },
    /*
     open the modal popup by firing the event
    */
    openLKP : function(component, event, helper) {
        var openLKPEvent = component.getEvent("openLKP");
        var searchString = component.get('v.searchString');
        var fieldLabel = component.get('v.label');
        var lkpCompId = component.get('v.lkpComponentId');//Added by Surabhi
        //Added by Surabhi
        if(lkpCompId != undefined && lkpCompId != '')
            fieldLabel = lkpCompId;
        //Ended by Surabhi
        console.log('-------searchString--'+searchString);
        openLKPEvent.setParams({"searchText": searchString, "fieldLabel": fieldLabel});
        openLKPEvent.fire();
        var searchEvt = $A.get("e.c:LKP_Search_EVT");
        searchEvt.setParams({"searchText": searchString});
        searchEvt.fire();
	},
    /*
    Search an SObject for a match
    */
    search : function(cmp, event, helper) {
        cmp.set('v.hideMainSpinner',true);
        if(event.getParams().keyCode===13){           
            var openLKPEvent = cmp.getEvent("openLKP");
            var searchString = cmp.get('v.searchString');
            var fieldLabel = cmp.get('v.label');
            console.log('-------searchString--'+searchString);
            openLKPEvent.setParams({"searchText": searchString, "fieldLabel": fieldLabel});
            openLKPEvent.fire();
            var searchEvt = $A.get("e.c:LKP_Search_EVT");
            searchEvt.setParams({"searchText": searchString});
            searchEvt.fire();
        }
        else
        	helper.doSearch(cmp,event);
        
    },
    
    /*
    Select an SObject from a list
    */
    select: function(cmp, event, helper) {
        helper.handleSelection(cmp, event);
    },
    
    /*
    Clear the currently selected Record
    */
    clear: function(cmp, event, helper) {
        helper.clearSelection(cmp);    
    },
    /*
    	Hide the search result div , if focus goes out of the inputtext
    */
    hideLookupList : function(component, event, helper) {
    	var lookupList = component.find('lookuplist');
        console.log('---------hide-------');
        
        window.setTimeout(
            $A.getCallback(function() {
                if (component.isValid()) {
                    $A.util.removeClass(lookupList, 'slds-show');
                    $A.util.addClass(lookupList, 'slds-hide');
                    component.set('v.hideMainSpinner',false);
                }
            }), 1000
        )
    }
})