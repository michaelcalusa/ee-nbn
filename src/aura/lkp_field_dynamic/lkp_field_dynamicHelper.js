({
    /*
    Perform the SObject search via an Apex Controller
    */
    doSearch : function(cmp,event) {
        var searchString = cmp.get('v.searchString');//event.target.value;
        var lookupList = cmp.find('lookuplist');
		var maxRecordLimit = cmp.get("v.maxRecordLimit");
        var filterField =cmp.get("v.filterField");
        var filterRecords= cmp.get("v.filterRecords");
        var primaryFieldAPI = cmp.get("v.primaryFieldAPI");
        console.log('---filterField---'+filterField);
         
        // We need at least 2 characters for an effective search
        if (typeof searchString === 'undefined')
        {
            // Hide the lookuplist
            $A.util.addClass(lookupList, 'slds-hide');
            $A.util.removeClass(lookupList, 'slds-show');
            cmp.set('v.matches', null);
            return;
        }
        
        var recentItem = cmp.find('recentItemDiv');
        if(searchString == ''){
            $A.util.addClass(recentItem, 'slds-show');
            $A.util.removeClass(recentItem, 'slds-hide');
        }else{
            $A.util.addClass(recentItem, 'slds-hide');
            $A.util.removeClass(recentItem, 'slds-show');
        }
         //
         cmp.set('v.searchString',searchString);
        // Get the API Name
        var sObjectAPIName = cmp.get('v.sObjectAPIName');
 		var fieldsToShow = cmp.get('v.fieldsToShow');
        var additionalfields = cmp.get('v.additionalfields');
        var fieldList = fieldsToShow.concat(additionalfields);	// added by Vignesh
        // Create an Apex action
        var action = cmp.get('c.lookup');
 
        // Mark the action as abortable, this is to prevent multiple events from the keyup executing
        action.setAbortable();
 
        // Set the parameters
        action.setParams({ "searchString" : searchString, 
                          "sObjectAPIName" : sObjectAPIName, 
                          "maxRecordLimit" : maxRecordLimit, 
                          "fieldList" : fieldList,
                          "filterField"    :filterField,
                          "filterRecords" : filterRecords,
                          "primaryFieldAPI" : primaryFieldAPI
                         });
                           
        // Define the callback
        action.setCallback(this, function(response) {
            var state = response.getState();
 
            // Callback succeeded
            if (cmp.isValid() && state === "SUCCESS")
            {
                // Get the search matches
                var matches = response.getReturnValue();
 
                // If we have no matches, return nothing
                if (matches.length == 0)
                {
                   // $A.util.removeClass(lookupList, 'slds-show');
       				//$A.util.addClass(lookupList, 'slds-hide');
                    cmp.set('v.matches', null);
                    //return;
                }else{
                    // Store the results
                	cmp.set('v.matches', response.getReturnValue());
                }
                $A.util.removeClass(lookupList, 'slds-hide');
       			$A.util.addClass(lookupList, 'slds-show');
                cmp.set('v.hideMainSpinner',false);
                //console.log('return reult:--'+JSON.stringify(matches));
                
            }
            else if (state === "ERROR") // Handle any error by reporting it
            {
                var errors = response.getError();                 
              	console.log('------'+errors);
            }
        });
         
        // Enqueue the action                  
        $A.enqueueAction(action);                
    },
 
    /*
    Handle the Selection of an Item or record
    */
    handleSelection : function(cmp, event) {
        
        // The selected object record index
        var selectedItem = event.currentTarget;
        var recIndex = selectedItem.dataset.record;
        
        var selRecord = cmp.get("v.matches");
        
        var primaryFieldAPI = cmp.get("v.primaryFieldAPI");
        
        // Update the selectedRecord with the selected record
        cmp.set("v.selectedRecord", selRecord[recIndex]);

        // Update the Searchstring with the primary field value
        cmp.set("v.searchString", selRecord[recIndex][primaryFieldAPI]);
        
        // Hide the Lookup List
        var lookupList = cmp.find("lookuplist");
        $A.util.removeClass(lookupList, 'slds-show');
        $A.util.addClass(lookupList, 'slds-hide');
 
        // Hide the Input Element
        var inputElement = cmp.find('lookup');
        $A.util.addClass(inputElement, 'slds-hide');
 
        // Show the Lookup pill
        var lookupPill = cmp.find("lookup-pill");
        $A.util.removeClass(lookupPill, 'slds-hide');
 
        // Lookup Div has selection
        var inputElement = cmp.find('lookup-div');
        $A.util.addClass(inputElement, 'slds-has-selection');
 
    }, 
    /*
    Clear the Selection
    */
    clearSelection : function(cmp) {
        // Clear the Searchstring and seleced record
        cmp.set("v.searchString", '');
        cmp.set("v.selectedRecord", null);
        
        // Hide the Lookup pill
        var lookupPill = cmp.find("lookup-pill");
        $A.util.addClass(lookupPill, 'slds-hide');
 
        // Show the Input Element
        var inputElement = cmp.find('lookup');
        $A.util.removeClass(inputElement, 'slds-hide');
 
        // Lookup Div has no selection
        var inputElement = cmp.find('lookup-div');
        $A.util.removeClass(inputElement, 'slds-has-selection');
    }
     
})