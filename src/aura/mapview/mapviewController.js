({
	myAction : function(component, event, helper) {
        
        
	},
    afterLoadEvent : function(component, event, helper){
        if(!component.get("v.loaddone")){
            var latitude = component.get("v.latitude");
            var longitude = component.get("v.longitude");
            console.log(' lat in map view '+latitude+' lang in '+longitude);
            if(latitude != undefined && longitude != undefined){
                helper.getLocationOnLatLangHelper(component, event, helper, latitude, longitude,'devChange');
                helper.mapCreateHelper(component, event, helper, latitude, longitude);
            }
            component.set("v.loaddone",true);
        }
    },
    dragBooleanChange : function (component, event, helper){
        var latitude = component.get("v.latitude");
        var longitude = component.get("v.longitude");
        helper.getLocationOnLatLangHelper(component, event, helper, latitude, longitude,'user');
    },
    populateAddressInfo : function(component, event, helper){
        var cmpName = event.getParam('cmpName');
        var MapRes = event.getParam('mapAddressResults');
        var userSearchString = event.getParam('userSearchString');
        component.set("v.userSearchString",userSearchString);
        if(cmpName == 'mapView'){
            console.log('map view mapAddressResults '+JSON.stringify(MapRes));
            console.log(MapRes["latitude"]+' vavas '+MapRes["longitude"]);
            helper.getLocationOnLatLangHelper(component, event, helper, parseFloat(MapRes["latitude"]), parseFloat(MapRes["longitude"]),'user');
            helper.mapCreateHelper(component, event, helper, MapRes["latitude"], MapRes["longitude"]);
        }
	},
 	enableAddress : function(component, event, helper){
    	
	},
    retAddressChange : function(component, event, helper){
    	var checkCmp = component.find("retAddress");
		var resultCmp = component.find("userAddress");
        if(checkCmp.get("v.value")==true){        
			resultCmp.set("v.value", false);
        }
        var latitude = component.get("v.latitude");
        var longitude = component.get("v.longitude");
		helper.getLocationOnLatLangHelper(component, event, helper, latitude, longitude,'user');
        component.set("v.addressPopulated1","true");
	},
    userAddressChange : function(component, event, helper){
    	var checkCmp = component.find("userAddress");
		var resultCmp = component.find("retAddress");
        if(checkCmp.get("v.value")==true){        
			resultCmp.set("v.value", false);
        }
        
        var userSearchAddress = component.get("v.userSearchString");                              
        var b = userSearchAddress.trim().split(',');
        var c = '';
        if(b.length>0){
        	if (b[0].match(/^\d/)) {
   				c=b[0].trim().split(' ');            
            	if(c.length>0){
            		component.set("v.streetnumber",c[0]);    
            	}            
            	var i;
            	var tempStreetName='';
				for (i = 1; i < c.length; i++) { 
    				tempStreetName += c[i] + " ";
				}
        		component.set("v.streetname",tempStreetName);
			}
            else if(b[0].match(/^lot\d+/)) {
                c=b[0].trim().split(' ');            
            	if(c.length>0){
            		component.set("v.streetnumber",c[0]);    
            	}            
            	var i;
            	var tempStreetName='';
				for (i = 1; i < c.length; i++) { 
    				tempStreetName += c[i] + " ";
				}
        		component.set("v.streetname",tempStreetName);
            }
            else if(b[0].match(/^lot\W/) || b[0].match(/^LOT\W/) || b[0].match(/^Lot\W/)) {
                c=b[0].trim().split(' ');            
            	if(c.length>0){
            		component.set("v.streetnumber",c[0]+' '+c[1]);    
            	}            
            	var i;
            	var tempStreetName='';
				for (i = 2; i < c.length; i++) { 
    				tempStreetName += c[i] + " ";
				}
        		component.set("v.streetname",tempStreetName);
            }
        	else{            	           	        	
        		component.set("v.streetname",b[0]);        
        	}
        }
        if(checkCmp.get("v.value")==true){        
			resultCmp.set("v.value", false);
            component.set("v.addressPopulated1","true");        
        	component.set("v.displayEditAddress",true);
        }
        else if(checkCmp.get("v.value")==false){        			
            component.set("v.addressPopulated1","false");        
        	component.set("v.displayEditAddress",false);
        }
		
        
        //component.find('cpd-streetNumber-div').focus();
        //component.find('cpd-streetNumber').focus();
        //var input = component.find("cpd-streetNumber").getElement(); 
        //input.focus();
        
        //setTimeout(function(){ 
  		//component.find("cpd-streetNumberLE").getElement().focus();
		//}, 500);
		try {
		var cmpEvent = component.getEvent("mapAddressSelectedEvent");
            cmpEvent.setParams({
                "userAddressChange" : true,
                "streetnumber" : component.get("v.streetnumber"),
                "streetname" : component.get("v.streetname"),
                "suburb" : component.get("v.suburb"),
                "postCode" : component.get("v.postCode"),
                "state" : component.get("v.state"),
                "userSearchString" : component.get("v.userSearchString")
            });
            cmpEvent.fire();               
        }
        catch(err) {
    		alert(err.message);
		}
	},
    editAddress : function(component, event, helper){
        component.set("v.displayEditAddress",true);
        
    }, 
    addressTypeChangeLE : function(component, event, helper){
        
        try {
		var cmpEvent = component.getEvent("mapAddressSelectedEvent");
            cmpEvent.setParams({
                "userAddressChange" : true,
                "streetnumber" : component.get("v.streetnumber"),
                "streetname" : component.get("v.streetname"),
                "suburb" : component.get("v.suburb"),
                "postCode" : component.get("v.postCode"),
                "state" : component.get("v.state"),
                "userSearchString" : component.get("v.userSearchString")
            });
            cmpEvent.fire();   
        }
        catch(err) {
    		alert(err.message);
		}    
        
        
    },
    addressTypeChange : function(component, event, helper){
        var validPassed = false;        
        var fieldVal = component.get("v.postCode");
        if((!fieldVal.match(/^[0-9]+$/) || (fieldVal.length != 4 && fieldVal.length != 3))){
        	validPassed = false;
            component.set("v.postCodeError",true);
        }
        else{
            validPassed = true;
            component.set("v.postCodeError",false);
        }
                
        if(validPassed){
        	var cmpEvent = component.getEvent("mapAddressSelectedEvent");
            cmpEvent.setParams({
                "userAddressChange" : true,
                "streetnumber" : component.get("v.streetnumber"),
                "streetname" : document.getElementById('cpd-streetName').value,
                "suburb" : document.getElementById('cpd-suburb').value,
                "postCode" : document.getElementById('cpd-postCode').value,
                "state" : document.getElementById('cpd-state').value,
                "userSearchString" : component.get("v.userSearchString")
            });
            cmpEvent.fire();    
        }
        
    }
})