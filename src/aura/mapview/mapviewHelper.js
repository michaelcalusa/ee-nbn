({
	
    mapCreateHelper : function(component, event, helper, latitude, longitude){
        document.getElementById('map').style.display = 'block';
        document.getElementById('mapInfo').style.display = 'block';
        document.getElementById('map').innerHTML = "<div id='locationmap' style='width: 100%; height: 100%;'></div>";
        var maplocation;
        maplocation = L.map('locationmap', {
        	center: [latitude,longitude],
        	zoom: 18
        });
        
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        {
        }).addTo(maplocation);
        var marker = L.marker([latitude, longitude],{draggable : true})
        .addTo(maplocation);
        marker.on('dragend', function(evt){
            var latlng = evt.target.getLatLng();
            maplocation.panTo(new L.LatLng(latlng.lat, latlng.lng));
            component.set("v.latitude",latlng.lat);
            component.set("v.longitude",latlng.lng);
            component.set("v.dragBoolean",!component.get("v.dragBoolean"));
        }).addTo(maplocation);
        component.set("v.latitude",latitude);
        component.set("v.longitude",longitude);
    },
    getLocationOnLatLangHelper : function(component, event, helper,latitude,longitude,source){
        //alert('**getLocationOnLatLangHelper Called source:'+source);
        var action = component.get("c.getLocationDetailsOnLatLang");
        action.setParams({
            "latitude" : latitude,
            "longitude" : longitude
        });
        action.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                var cmpEvent = component.getEvent("mapAddressSelectedEvent");
                cmpEvent.setParams({
                    "mapInfo" : response.getReturnValue(),
                    "userAddressChange" : false,
                    "userSearchString" : component.get("v.userSearchString")
                });
                component.set("v.selectedAddressChanged",response.getReturnValue().siteName);
                if(source == 'user'){
                    component.set("v.streetnumber",response.getReturnValue().streetnumber);
                    component.set("v.streetname",response.getReturnValue().streetname);
                    component.set("v.suburb",response.getReturnValue().suburb);
                    component.set("v.postCode",response.getReturnValue().postCode);
                    component.set("v.state",response.getReturnValue().state);
                    
                    //<!-- S NPD-11506 -->
                    /*
                    var retCmp = component.find("retAddress");					        
					retCmp.set("v.value", true);
                    var userCmp = component.find("userAddress");					        
					userCmp.set("v.value", false);
                    component.set("v.displayEditAddress",false);
        			*/
                    //<!-- E NPD-11506 -->
                }
                else if(source == 'devChange'){
                    component.set("v.streetnumber",component.get("v.streetnumber1"));
                    component.set("v.streetname",component.get("v.streetname1"));
                    component.set("v.suburb",component.get("v.suburb1"));
                    component.set("v.postCode",component.get("v.postCode1"));
                    component.set("v.state",component.get("v.state1"));
                }
                cmpEvent.fire();
            }
            else{
                console.log(' erroe '+response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
    }
})