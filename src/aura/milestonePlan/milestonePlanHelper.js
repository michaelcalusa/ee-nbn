({
    fetchTransitionRecord : function(component, event, helper) {
        var objRec = component.find("currentRecord");
        var action = component.get("c.getTransitionRecord");
        var recordId = component.get("v.recordId");
        var self = this;
        action.setParams({
            opportunityId : recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() != null) {
                    $A.get("e.force:closeQuickAction").fire();
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Sorry!",
                        "message": "Only 1 Milestone Plan is allowed per Opportunity",
                        "type": "Error"
                    });
                    toastEvent.fire();
                    
                }
                else{
                    component.set("v.NoError","True");
                    component.set("v.Secondscreen","True");
                    
                    self.Next(component, event, helper);
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    getMilestonePlanStatus : function(component, event, helper) {
        var objRec = component.find("currentRecord");
        var action = component.get("c.checkMilestonePlanRecordStatus");
        var recordId = component.get("v.recordId");
        var self = this;
        action.setParams({
            mpRecId : recordId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue() == true) {
                    $A.get("e.force:closeQuickAction").fire();
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Sorry!",
                        "message": "You cannot manage Milestones when Milestone Plan is completed.",
                        "type": "Error"
                    });
                    toastEvent.fire();
                    
                }
                else{
                    component.set("v.NoError","True");
                    component.set("v.Secondscreen","True");
                    
                    self.Next(component, event, helper);
                }
            }
            
        });
        $A.enqueueAction(action);
    },
    Cancel : function(component, event, helper){
        var closewindow = $A.get("e.force:closeQuickAction");
        closewindow.fire();
    },  
    Next : function(component, event, helper){
        if(component.get("v.sobjecttype") === 'Opportunity'){
            var action = component.get("c.fetchMilestoneMetadata");
            action.setParams({
                recordType : null
            });            
        }
        else{
            var action = component.get("c.fetchMilestoneRecs");
            action.setParams({
                recordType : null,
                milestonePlanId: component.get("v.recordId")
            });
        }

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Secondscreen","True");
                if(response.getReturnValue() != null) {
                    component.set("v.RecMilestones",response.getReturnValue());
                }
            }
            else if(state === "ERROR"){
                var error = response.getError();
                if (error[0] && error[0].message) {
                    console.log("Error message: " +
                          error[0].message);
                }
                
            }
        });
        $A.enqueueAction(action);
    }       
})