({
    doInit: function(component) {
        // Set the nbnTopTailHeader value. 
        var action = component.get("c.getNbnTopTailHeader");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.nbnTopTailHeader", response.getReturnValue());
                // alert("header succeeded");
            }
            else {
                alert("get nbn header failed");
            }
        });
        $A.enqueueAction(action);
        
        // Set the nbnTopTailFooter value. 
        var action = component.get("c.getNbnTopTailFooter");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.nbnTopTailFooter", response.getReturnValue());
                // alert("footer succeeded");
            }
            else {
                alert("get nbn footer failed");
            }
        });
        $A.enqueueAction(action);
    }
})