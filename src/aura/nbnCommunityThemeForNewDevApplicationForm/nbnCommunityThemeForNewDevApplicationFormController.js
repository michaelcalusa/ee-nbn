({
    doInit: function(component) {
        //var sPageURL = decodeURIComponent(window.location.href);
        //console.log('page url: ' + sPageURL);
        // Set the nbnTopTailHeader value.
        /* don't require apex controller to get header & footer
        var action = component.get("c.getNbnTopTailHeader");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.nbnTopTailHeader", response.getReturnValue());
                // alert("header succeeded");
            }
            else {
                alert("get nbn header failed");
            }
        });
        $A.enqueueAction(action);
        
        // Set the nbnTopTailFooter value. 
        var action = component.get("c.getNbnTopTailFooter");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.nbnTopTailFooter", response.getReturnValue());
                // alert("footer succeeded");
            }
            else {
                alert("get nbn footer failed");
            }
        });
        $A.enqueueAction(action);
        */
        
    },
    
    toggingFunctionOne: function (component, event, helper) {
    	console.log('toggle one');
        var x = document.getElementById("id13779132B");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        
        var y = document.getElementById("sectionOneTogglingIcon");
        console.log('the class of section one is '+y);
        if(y.classList.contains('fc-plus')){
            y.classList.remove('fc-plus');
            y.classList.add('fc-minus');
        }else{
            y.classList.remove('fc-minus');
            y.classList.add('fc-plus');
        }
        
    },
    
    toggingFunctionTwo: function (component, event, helper) {
    	var x = document.getElementById("id42037907B");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        
        var y = document.getElementById("sectionTwoTogglingIcon");
        console.log('the class of section one is '+y);
        if(y.classList.contains('fc-plus')){
            y.classList.remove('fc-plus');
            y.classList.add('fc-minus');
        }else{
            y.classList.remove('fc-minus');
            y.classList.add('fc-plus');
        }
        
    },

    toggingFunctionThree: function (component, event, helper) {
    	
        var x = document.getElementById("id33655208B");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        
        var y = document.getElementById("sectionThreeTogglingIcon");
        console.log('the class of section one is '+y);
        if(y.classList.contains('fc-plus')){
            y.classList.remove('fc-plus');
            y.classList.add('fc-minus');
        }else{
            y.classList.remove('fc-minus');
            y.classList.add('fc-plus');
        }
    },

    toggingFunctionFour: function (component, event, helper) {
    	
         var x = document.getElementById("id24357242B");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        
        var y = document.getElementById("sectionFourTogglingIcon");
        console.log('the class of section one is '+y);
        if(y.classList.contains('fc-plus')){
            y.classList.remove('fc-plus');
            y.classList.add('fc-minus');
        }else{
            y.classList.remove('fc-minus');
            y.classList.add('fc-plus');
        }
    },


})