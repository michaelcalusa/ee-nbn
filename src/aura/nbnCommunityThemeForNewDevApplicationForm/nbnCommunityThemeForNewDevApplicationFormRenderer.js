({
    render: function(component, helper) {
        var ret = this.superRender();
        var ajax = new XMLHttpRequest();
        ajax.open("GET", $A.get('$Resource.NBNSVG'), true);
        ajax.responseType = "document";
        ajax.onload = function(e) {
            try {
                console.log(ajax.readyState);
                var svg = ajax.responseXML.documentElement;
                svg.setAttribute("class", "hidden");
                var container = component.find("svgcontainer").getElement();
                container.innerHTML = svg.outerHTML; 
            }
            catch(e){console.log(e);}
        }
        ajax.send(); 
        
        var svgns = "http://www.w3.org/2000/svg";
        var xlinkns = "http://www.w3.org/1999/xlink";
        /* blog svg
        var blogSvg = document.createElementNS(svgns, "svg");
        blogSvg.setAttribute("class", 'svg-icon');
        var blogShape = document.createElementNS(svgns, "use");
    	blogShape.setAttributeNS(xlinkns, "xlink:href", "#blog");
    	blogSvg.appendChild(blogShape);
        var blogContainer = component.find("blogSvg").getElement();
    	blogContainer.insertBefore(blogSvg, blogContainer.firstChild);
    	// facebook svg
    	var facebookSvg = document.createElementNS(svgns, "svg");
        facebookSvg.setAttribute("class", 'svg-icon');
        var facebookShape = document.createElementNS(svgns, "use");
    	facebookShape.setAttributeNS(xlinkns, "xlink:href", "#facebook");
    	facebookSvg.appendChild(facebookShape);
        var facebookContainer = component.find("facebookSvg").getElement();
    	facebookContainer.insertBefore(facebookSvg, facebookContainer.firstChild);
        // twitter svg
    	var twitterSvg = document.createElementNS(svgns, "svg");
        twitterSvg.setAttribute("class", 'svg-icon');
        var twitterShape = document.createElementNS(svgns, "use");
    	twitterShape.setAttributeNS(xlinkns, "xlink:href", "#twitter");
    	twitterSvg.appendChild(twitterShape);
        var twitterContainer = component.find("twitterSvg").getElement();
    	twitterContainer.insertBefore(twitterSvg, twitterContainer.firstChild);
        // youtube svg
    	var youtubeSvg = document.createElementNS(svgns, "svg");
        youtubeSvg.setAttribute("class", 'svg-icon');
        var youtubeShape = document.createElementNS(svgns, "use");
    	youtubeShape.setAttributeNS(xlinkns, "xlink:href", "#youtube");
    	youtubeSvg.appendChild(youtubeShape);
        var youtubeContainer = component.find("youtubeSvg").getElement();
    	youtubeContainer.insertBefore(youtubeSvg, youtubeContainer.firstChild);
        // linkedIn svg
    	var linkedInSvg = document.createElementNS(svgns, "svg");
        linkedInSvg.setAttribute("class", 'svg-icon');
        var linkedInShape = document.createElementNS(svgns, "use");
    	linkedInShape.setAttributeNS(xlinkns, "xlink:href", "#linkedin");
    	linkedInSvg.appendChild(linkedInShape);
        var linkedInContainer = component.find("linkedInSvg").getElement();
    	linkedInContainer.insertBefore(linkedInSvg, linkedInContainer.firstChild);*/
        return ret;
    },
    
	// Your renderer method overrides go here
    afterRender: function(component, helper) { 
        console.log('afterrender');  
    }
})