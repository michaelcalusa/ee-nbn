({
    doInit: function(component, event, helper) {
        var workspaceAPI = component.find("workspaceTemplate");
        workspaceAPI.isConsoleNavigation().then((res) =>{
            if(res){
                workspaceAPI.getTabURL().then(function(data){
                console.log('Tab URL : '+ data);
                if(data != undefined){
                    var urlRecordId = data.match(new RegExp("Incident_Management__c/" + "(.*)" + "/view"));
                        if(urlRecordId != undefined && urlRecordId[1] != undefined){
                            component.set("v.recordId", urlRecordId[1]);
                        }
                	}
            	});
            }
            else{
                 var urlRecordId = window.location.href.match(new RegExp("Incident_Management__c/" + "(.*)" + "/view"));
                 if(urlRecordId != undefined && urlRecordId[1] != undefined){
                    component.set("v.recordId", urlRecordId[1]);
                 }
            }                          
        }).catch(function(error) {
            console.log(error);
        });
    },
    
    handleApplicationEventFired: function(component, event, helper) {
        var workspaceAPI = component.find("workspaceTemplate");
        var visibility = event.getParam("testManagerVisible");
        var focusedTabIdEvent = event.getParam("focusedTabId");
        var eventRecordId = event.getParam("recordId");
        var urlRecordId = window.location.pathname.match(new RegExp("Incident_Management__c/" + "(.*)" + "/view"))[1] ;
        if(eventRecordId != undefined && urlRecordId != undefined && eventRecordId == urlRecordId && eventRecordId == component.get("v.recordId")){
            if (visibility != undefined) {
                component.set("v.visibility", visibility);
            }
            console.log(component.get("v.visibility"));
        }
    }
})