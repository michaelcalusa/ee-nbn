({
    doInit: function(component, event, helper) {
        //CUSTSA-28211 Enable test manager link when incident is in closed status for 30 days from incident closed date
        //Keep test manager link disabled for incidents in terminal status(CLOSED (30 days after incident closed date, ), CANCELLED, REJECTED)
        if(component.get('v.incidentRecord') && component.get('v.incidentRecord').Current_Status__c){
            if(component.get('v.incidentRecord').Current_Status__c == 'Cancelled' || component.get('v.incidentRecord').Current_Status__c == 'Rejected' || component.get('v.incidentRecord').Current_Status__c == 'Closed'){
                if(component.get('v.incidentRecord').Current_Status__c == 'Closed'){
                    var closeDateDiff = Math.floor((Math.abs(Date.now() - Date.parse(component.get('v.incidentRecord').closedDate__c))/1000)/ 86400);
                    if(closeDateDiff != undefined && closeDateDiff >= 60){
                        component.set('v.isTMDisabled',true);
                    }else{
                        component.set('v.isTMDisabled',false);
                    }
                }else{
                    component.set('v.isTMDisabled',true);
                }
            }else{
                component.set('v.isTMDisabled',false);
            }
        }
    },
    
    gotoURL: function(component, event, helper) {

        var workspaceAPI = component.find("workspace");
        workspaceAPI.isConsoleNavigation().then((res) => {
            if (res) {
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    var focusedTabId = response.tabId;

                    //var appEvent = $A.get("e.c:JIGSAW_toggleTestManager");
                    //var appEvent = $A.get("e.c:JIGSAW_toggleTestManager");

                    var appEvent = $A.get("e.c:JIGSAW_toggleTestManager");

                    //var appEvent = component.getEvent("JIGSAW_toggleTestManager");
                    appEvent.setParams({
                        "testManagerVisible": true
                    });
                    appEvent.setParams({
                        "recordId": component.get("v.recordId")
                    });
                    appEvent.setParams({
                        "focusedTabId": focusedTabId
                    });
                    appEvent.fire();
                });
            } else {
                var appEvent = $A.get("e.c:JIGSAW_toggleTestManager");
                appEvent.setParams({
                    "testManagerVisible": true
                });
                appEvent.setParams({
                    "recordId": component.get("v.recordId")
                });
                appEvent.fire();
            }
        }).catch(function(error) {
            console.log(error);
        });


    },

    handleApplicationEventFired: function(component, event, helper) {
        var recordId = event.getParam("recordId");
        if (recordId != undefined && recordId == component.get("v.recordId")) {
            var context = event.getParam("testManagerVisible");
            component.set("v.testManagerVisible", context);
        }

    },

    getVisibility: function(component, event, helper) {

        var attrMapByModule = component.get("v.mapComponentVisibility");
        if (!attrMapByModule['TESTMANAGERLINK']) {
            component.set("v.hideTestManager", true);
        }
    }
})