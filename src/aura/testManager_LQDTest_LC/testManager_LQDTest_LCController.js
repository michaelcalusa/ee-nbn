({
  
    
 onInit : function(component, event, helper) {

 
  },
    
    clearDuration:  function(component, event, helper){
        component.set("v.intInputVal","");
    },
    
  runTest : function(component, event, helper) {  
      var mDuration = component.find("mDuration");
 	  var monitoringDuration = mDuration.get("v.value");
      component.set("v.intInputVal",monitoringDuration);
      
      console.log('@@ mDuration : ' + monitoringDuration);
      if(monitoringDuration>=0){
          
      console.log('@@ Test 1 : ' + monitoringDuration);
      component.set('v.notifications', []);
      var action = component.get("c.makeCallout");
       action.setParams({
            IncidentId  : component.get("v.thisRecordId"),
            TestType : 'LQDTest',
           	MonitoringDuration : monitoringDuration,
            packetSize : null,
           	testMode : null
        });
        
        console.log('@@ Test 2 : ' + monitoringDuration);
        action.setCallback(this,function(a){
            //get the response state
            console.log('@@ Test 3 : ' + monitoringDuration);
            var state = a.getState();
            var listNotifications=[];
            var notificationMessage='';
            var notifications = component.get('v.notifications');
            
            var keyValue ='';
            
            //check if result is successfull
            if(state == "SUCCESS"){
              console.log('@@ Test 4 : ' + monitoringDuration);
              var resultMap = a.getReturnValue();
              console.log('@@Map' + resultMap);
              for (var key in resultMap){
                var label = key;
                keyValue = key;
                var value = resultMap[key];
                console.log('@@Notification' + key + '+++++++' + value);
                if(key==202){
                    component.set('v.correlationId', value);
                    notificationMessage = 'In Progress' + '<br/>';
                    console.log('@@Notification Message' + notificationMessage);
                    helper.subscribeToPlatformEvent(component,helper);
                }
                else{
                  notificationMessage = notificationMessage + key + '' + value + '<br/>';
                }
                

              }
            } else if(state == "ERROR"){
                
            }
            if(notificationMessage!=null){
                var newNotification = {
          time : $A.localizationService.formatDateTime(Date.now(), 'HH:mm'),
            message : notificationMessage
          };
            notifications.push(newNotification);
            component.set('v.notifications', notifications);
            }
            
      
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
      }
      else{
          component.set('v.notifications', 'Invalid Monitoring Duration');
      }
  },
    
    
  
  onClear : function(component, event, helper) {
    component.set('v.notifications', []);
  },
  onToggleMute : function(component, event, helper) {
    var isMuted = component.get('v.isMuted');
    component.set('v.isMuted', !isMuted);
    helper.displayToast(component, 'success', 'Notifications '+ ((!isMuted) ? 'muted' : 'unmuted') +'.');
  },
 

  
  
})