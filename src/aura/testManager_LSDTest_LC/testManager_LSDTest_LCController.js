({
  
    
 onInit : function(component, event, helper) {

 
  },
  runTest : function(component, event, helper) {  
      component.set('v.notifications', []);
      var action = component.get("c.makeCallout");
       action.setParams({
            IncidentId  : component.get("v.thisRecordId"),
            TestType : 'LSDTest',
            monitoringDuration : 0
        });
        
        
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            var listNotifications=[];
            var notificationMessage='';
            var notifications = component.get('v.notifications');
            
            var keyValue ='';
            
            //check if result is successfull
            if(state == "SUCCESS"){
              var resultMap = a.getReturnValue();
              console.log('@@Map' + resultMap);
              for (var key in resultMap){
                var label = key;
                keyValue = key;
                var value = resultMap[key];
                console.log('@@Notification' + key + '+++++++' + value);
                if(key==202){
                    component.set('v.correlationId', value);
                    notificationMessage = 'In Progress' + '<br/>';
                    console.log('@@Notification Message' + notificationMessage);
                    helper.subscribeToPlatformEvent(component,helper);
                }
                else{
                  notificationMessage = notificationMessage + value + '<br/>';
                }
                

              }
            } else if(state == "ERROR"){
                
            }
            if(notificationMessage!=null){
                var newNotification = {
          time : $A.localizationService.formatDateTime(Date.now(), 'HH:mm'),
            message : notificationMessage
          };
            notifications.push(newNotification);
            component.set('v.notifications', notifications);
            }
            
      
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
  },
    
    
  
  onClear : function(component, event, helper) {
    component.set('v.notifications', []);
  },
  onToggleMute : function(component, event, helper) {
    var isMuted = component.get('v.isMuted');
    component.set('v.isMuted', !isMuted);
    helper.displayToast(component, 'success', 'Notifications '+ ((!isMuted) ? 'muted' : 'unmuted') +'.');
  },
 

  
  
})