({
  onCometdLoaded: function(component) {
    console.log('@@ On CometD Loaded');
    var cometd = new org.cometd.CometD();
    component.set('v.cometd', cometd);

    if (component.get('v.sessionId') != null)
      this.connectCometd(component);
  },




  connectCometd: function(component) {
    var helper = this;

    // Configure CometD
    var cometdUrl = window.location.protocol + '//' + window.location.hostname + '/cometd/40.0/';
    var cometd = component.get('v.cometd');
    cometd.configure({
      url: cometdUrl,
      requestHeaders: {
        Authorization: 'OAuth ' + component.get('v.sessionId')
      },
      appendMessageTypeToURL: false
    });
    cometd.websocketEnabled = false;

    // Establish CometD connection
    console.log('Connecting to CometD: ' + cometdUrl);
    console.log('@@@ RecordId' + component.get("v.thisRecordId"));
    cometd.handshake(function(handshakeReply) {
      if (handshakeReply.successful) {
        //debugger;
        console.log('Connected to CometD.');
        // Subscribe to platform event
        var newSubscription = cometd.subscribe('/event/TNDInboundIntegration__e',

          function(platformEvent) {
            console.log('Platform event received: ' + JSON.stringify(platformEvent));
            helper.onReceiveNotification(component, platformEvent);
          }
        );
        // Save subscription for later
        var subscriptions = component.get('v.cometdSubscriptions');
        subscriptions.push(newSubscription);
        component.set('v.cometdSubscriptions', subscriptions);
        console.log('End of Connected to CometD.');
      } else
        console.error('Failed to connected to CometD.');
    });
  },





  disconnectCometd: function(component) {
    var cometd = component.get('v.cometd');

    // Unsuscribe all CometD subscriptions
    cometd.batch(function() {
      var subscriptions = component.get('v.cometdSubscriptions');
      subscriptions.forEach(function(subscription) {
        cometd.unsubscribe(subscription);
      });
    });
    component.set('v.cometdSubscriptions', []);

    // Disconnect CometD
    cometd.disconnect();

    console.log('CometD disconnected.');
  },






  subscribeToPlatformEvent: function(component, helper) {
    console.log('@@@@subscribeToPlatformEvent');

    if (component.get('v.isListening') == true) {
      component.set('v.isListening', false);
      var x = component.get('v.thisRecordId');


      helper.onCometdLoaded(component);
      component.set('v.cometdSubscriptions', []);

      component.set('v.notifications', [])
      // Disconnect CometD when leaving page
      window.addEventListener('unload', function(event) {
        helper.disconnectCometd(component);
      });
      //console.log('@@Test');

      // Retrieve session id
      var action = component.get('c.getSessionId');
      action.setCallback(this, function(response) {
        if (component.isValid() && response.getState() === 'SUCCESS') {
          component.set('v.sessionId', response.getReturnValue());
          if (component.get('v.cometd') != null)
            helper.connectCometd(component);
        } else
          console.error(response);
      });
      $A.enqueueAction(action);
      helper.displayToast(component, 'success', 'Ready to receive notifications.');
    }
    if (component.get('v.isListening') == true && component.get('v.resultReceived') == false) {
      helper.displayToast(component, 'warning', 'Already Listening to Platform Event. Please Refresh Browser to Subscribe again.');
    }


  },




  onReceiveNotification: function(component, platformEvent) {
    console.log('@@@@onReceiveNotification');
    console.log(platformEvent.data.payload.InboundJSONMessage__c);
    var helper = this;
    //debugger;
    var action = component.get("c.deserializePlatformEvent");
    action.setParams({
      InboundJSONMessage: platformEvent.data.payload.InboundJSONMessage__c,
      componentname: 'testManager_SELTTest_LC',
      JSONName: 'SELTTest',
      IncidentId: component.get("v.thisRecordId"),
      CorrelationId: component.get('v.correlationId')
    });

    action.setCallback(this, function(a) {
      //debugger;
      //get the response state
      var state = a.getState();
      var listNotifications = [];
      var notificationMessage = '';
      //check if result is successfull
      if (state == "SUCCESS") {
        var resultMap = a.getReturnValue();
        console.log('@@@resultMap');
        //console.log(resultMap);
        for (var mapKey in resultMap) {
          //console.log('@@@@@k' + mapKey + '+++++++' + resultMap[mapKey]);
          //notificationMessage = 'dev is testing';
          //debugger;
          if (mapKey.includes("HEADING_")) {
            notificationMessage = notificationMessage + resultMap[mapKey] + '<br/>';
          } else {
            if (mapKey.includes("___")) {
              var tempKey = mapKey.replace(mapKey.substring(0, mapKey.indexOf('___') + 3), "");
              //console.log('tempKey');
              //console.log(tempKey);
                
                  notificationMessage = notificationMessage + tempKey + ' : ' + resultMap[mapKey] + '<br/>';
                
            }
              
          }

        }
          
        // if notificationMessage is empty.
        if(notificationMessage.length < 1)  notificationMessage = a.getReturnValue();
          


        var newNotification = {
          time: $A.localizationService.formatDateTime(platformEvent.data.payload.CreatedDate, 'HH:mm'),
          message:   notificationMessage
        };

        component.set('v.notifications', newNotification);
        component.set('v.resultReceived', true);
      } else {

        var newNotification = {
          time: $A.localizationService.formatDateTime(platformEvent.data.payload.CreatedDate, 'HH:mm'),
            message: 'Error:' + a.getReturnValue()
        };

        component.set('v.notifications', newNotification);
        component.set('v.resultReceived', true);
      }
    });

    //adds the server-side action to the queue        
    $A.enqueueAction(action);

  },


  displayToast: function(component, type, message) {
    var toastEvent = $A.get('e.force:showToast');
    toastEvent.setParams({
      type: type,
      message: message
    });
    toastEvent.fire();
  }
})