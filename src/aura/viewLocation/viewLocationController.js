({
    handleApplicationEvent : function(component, event, helper) {
        if(event.getParam("recordId") == component.get("v.recordId"))
        {
            component.set("v.messageFromEvent", component.get("v.recordId"));
            let locId = event.getParam('locID');
            let prodCat = event.getParam('prodCat');
            //MRTEST-44061
             if((prodCat && prodCat == "NHUR") || (component.get('v.incidentRecord') && (component.get('v.incidentRecord.Engagement_Type__c') =='Commitment') || !component.get('v.incidentRecord.AppointmentId__c')))
            {
				component.set("v.appointmentCalloutDone",true);              
            }
            if(locId)
            {
                component.set("v.locId",locId);
                if(component.get('v.incidentRecord') && component.get('v.appointmentCalloutDone'))
                {
                    helper.getLocationList(component, event, helper);
                }
            }
        }
    },
    handleCurrentIncidentRecord: function(component, event, helper) {
        //MRTEST-44061
        if(component.get('v.incidentRecord') && component.get('v.locId') && ((component.get('v.incidentRecord.Engagement_Type__c') =='Commitment' || !component.get('v.incidentRecord.AppointmentId__c'))  || component.get('v.appointmentCalloutDone')))
        {
            if(component.get('v.incidentRecord.Engagement_Type__c') =='Commitment')
            {
				component.set("v.appointmentCalloutDone",true);              
            }
            helper.getLocationList(component, event, helper);
        }
    },
    handleAppointmentDetails: function(component, event, helper) 
    {
        if(event.getParam("recordId") == component.get('v.recordId'))
        {
            component.set("v.appointmentCalloutDone",true);
            let appointmentData = event.getParam("appointmentDetails");
            if(appointmentData)
            {
				component.set('v.appointmentResponse',appointmentData);
                if(component.get('v.incidentRecord') && appointmentData && component.get('v.locId'))
                {
                    helper.getLocationList(component, event, helper);
                }
            }
        }
    }
})