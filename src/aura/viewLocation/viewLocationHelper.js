({
    getLocationList : function(component, event, helper) {
        component.set("v.Error", false);
        var ComponentName = component.get("v.ComponentName");
        var incNum = component.get("v.messageFromEvent");   
        var locId = component.get("v.locId");
        var incdata = component.get("v.incidentRecord");
        var action = component.get("c.getLoc");
        action.setParams({            
            sLightningComponentName: ComponentName,
            incNum: incNum,
            locIdParam: locId,
            pri: incdata.PRI_ID__c,
            segment: incdata.Segment__c,
            accessSeekerID: incdata.Access_Seeker_ID__c
        });  
        action.setBackground();
        action.setCallback(this, function(response) {            
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS"){
                var results = response.getReturnValue(); 
                var locList = results.locList;
                var cacheList = results.cacheList;
                console.log("Location Details :- " + JSON.stringify(locList));
                var locidIndex = '';//this is to remove the extra locid from the list
                //Get the building name from CIS appointment call out on the page load.
                var appointmentResponse = component.get("v.appointmentResponse");
                var jstr = JSON.stringify(appointmentResponse);
                var deserializedApptDetails = JSON.parse(jstr);
                if(deserializedApptDetails && deserializedApptDetails.httpStatusCode == "200"){
                    if (deserializedApptDetails.buildingName){
                        locList.unshift('Building Name:' + ' ' +deserializedApptDetails.buildingName);
                    }
                    if (deserializedApptDetails.buildingType){
                         locList.unshift('Building Type:' + ' ' +deserializedApptDetails.buildingType);
                    }
                }
                if(locList){
                    locList.unshift(component.get('v.locId'));
                    for(var cc in locList) {
                        if(locList[cc] && locList[cc].includes('###')){
                            locidIndex = cc;
                        }
                        if(locList[cc].includes("Error:")) {
                            component.set("v.Error", true);
                            if((locList[cc].split(':'))[1])
                            {
                            	component.set("v.errorMessage", (locList[cc].split(':'))[1]);
                            }
                        }
                    }
                    // delete the extra attributes from the list
                    if(locidIndex != '' && incdata.Segment__c == 'Business') {
                        locList.splice(locidIndex,1);
                        locList.splice(locidIndex,5);
                    }
                    else if (locidIndex != '' && incdata.Segment__c == 'Residential') {
                        locList.splice(locidIndex,1);
                    }
                    component.set("v.LocWrapList",locList);
                }
                if(cacheList) {
                    var tempCacheList = component.get("v.cacheList");
                    if(tempCacheList == undefined){
                        tempCacheList = [];
                    }
                    var finalCacheList = tempCacheList.concat(cacheList);
                    component.set("v.cacheList",finalCacheList);
                }
                component.set("v.locationCalloutDone",true);
                var notifyEvent = $A.get("e.c:calloutDone");
                notifyEvent.setParams({"locid":component.get("v.locId"),
                                       "recordId":component.get('v.recordId')});
                notifyEvent.fire();
            }
        });
        $A.enqueueAction(action);
    }
})