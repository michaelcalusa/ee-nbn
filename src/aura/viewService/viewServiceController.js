({
    handleApplicationEvent : function(component, event, helper){
        if(event.getParam("recordId") == component.get("v.recordId")) {           
            var avcId = event.getParam('avcId');
            //CUSTSA-24704::: code added to map orderCompletionDate
            var orderCompletionDate = event.getParam('orderCompletionDate');
            if(avcId)
            {
            	component.set("v.avcID",avcId);    
            }
            if(orderCompletionDate)
            {
            	component.set("v.orderDate",orderCompletionDate);    
            }           
            var recordId = component.get("v.recordId");
            component.set("v.messageFromEvent", recordId);
            component.set("v.display",true);
            if(component.get('v.incidentRecord') && !component.get('v.CalloutOutRequested')) {
                component.set('v.CalloutOutRequested',true);
            	helper.getServiceList(component, event, helper);
            }
        }
    },
    handleCurrentIncidentRecord: function(component, event, helper) {
        if(component.get('v.incidentRecord')) {
            var avc = component.get('v.avcID');
            if(component.get('v.display') && !component.get('v.CalloutOutRequested')) { 
                component.set('v.CalloutOutRequested',true);
                helper.getServiceList(component, event, helper);           
            }
        }
    }
})