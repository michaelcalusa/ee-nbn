({
    getServiceList : function(component, event, helper) {
        component.set("v.Error", false);
        let currentIncidentRecord = component.get('v.incidentRecord');
        if(currentIncidentRecord.Prod_Cat__c && currentIncidentRecord.Prod_Cat__c != "NHUR") {
            var incNum = component.get("v.messageFromEvent");    
            var ComponentName=component.get("v.ComponentName"); 
            var avcID = component.get("v.avcID");
            if(!avcID){
                avcID = currentIncidentRecord.AVC_Id__c;
            }
            var orderDate = component.get("v.orderDate");
            var action = component.get("c.getServ");
            action.setParams({            
                sLightningComponentName :ComponentName,
                incNum:incNum,
                avcIdParam : avcID,
                prodCat: currentIncidentRecord.Prod_Cat__c
            });  
            action.setBackground();        
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS") {
                    var results = response.getReturnValue(); 
                    var servList = results.servList;
                    var cacheList = results.cacheList;
                    var techType = '';
                    var techTypeIndex = ''; //this is to remove the exrta tech type from the list
                    var avcid = '';
                    var avcIndex = ''; //this is to remove the extra avc from the list
                    var cpiId = '' ;
                    var orderDateTime;
                    if (orderDate){ 
                        if(orderDate.indexOf('/') > 0){
                           let datetimestring = (orderDate).split(" ");
                           let datestring = datetimestring[0].split("/");
                           orderDateTime = new Date(datestring[2],datestring[1]-1,datestring[0]);
                        }   
                        else{
                          let dateVal = new Date(orderDate);
                          orderDateTime = new Date(dateVal.getFullYear(), dateVal.getMonth(), dateVal.getDate());
                        }
                        var completionDate = "Connected " + helper.formatDate(orderDateTime);
                        servList.splice( 2, 0, completionDate);
                    }
                    if(currentIncidentRecord.Prod_Cat__c && currentIncidentRecord.Prod_Cat__c == "NHAS" && currentIncidentRecord.Operate_And_Managed_By__c)
                    {
                        servList.splice( 4, 0, "Maintainer: " + currentIncidentRecord.Operate_And_Managed_By__c);
                        //servList.push();
                    }
                    if(servList){
                        for(var cc in servList){
                            if(servList[cc]) {
                                if(servList[cc] == "HFC Data") {
                                    servList[cc] = "HFC " + currentIncidentRecord.Prod_Cat__c;
                                }
                                if(servList[cc].includes('%%%')){
                                    techType = servList[cc].split('%%%')[1];
                                    techTypeIndex = cc;
                                }
                                if(servList[cc] && servList[cc].startsWith('AVC') && new RegExp('^AVC[0-9]{12}').test(servList[cc])){
                                    avcid = servList[cc];
                                    avcIndex = cc;
                                }
                                if(servList[cc] && servList[cc].startsWith('CPI') && new RegExp('^CPI[0-9]{12}').test(servList[cc])){
                                    cpiId = servList[cc] ;
                                }
                                if(servList[cc].includes("Error:")) {
                                    component.set("v.Error", true);
                                    if((servList[cc].split(':'))[1])
                                    {
                                    	component.set("v.errorMessage", (servList[cc].split(':'))[1]);
                                    }
                                }
                            }
                        }
                        // delete the extra attributes from the list
                        if(techTypeIndex){
                            servList.splice(techTypeIndex,1);
                        }
                        //assign the final list to be displayed
                        component.set("v.ServWrapList",servList);
                    }
                    if(cacheList && cacheList.length){
                        var tempCacheList = component.get("v.cacheList") || [];
                        var finalCacheList = tempCacheList.concat(cacheList);
                        component.set("v.cacheList",finalCacheList);
                    }
                    component.set('v.serviceCalloutDone',true);
                    var notifyEvent = $A.get("e.c:calloutDone");
                    notifyEvent.setParams({"avcid":avcid,
                                        "techtype" : techType,
                                        "cpiid" : cpiId ,
                                        "recordId":component.get('v.recordId')});
                    notifyEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
        else if(currentIncidentRecord.Prod_Cat__c == "NHUR")
        {
            var servList = [];
            servList.push(currentIncidentRecord.PRI_ID__c);
            servList.push("NewLine");
            servList.push("HFC " + currentIncidentRecord.Prod_Cat__c);
            if(currentIncidentRecord.prodCatTier2__c)
            {
                servList.push("Product: " + currentIncidentRecord.prodCatTier2__c);
            }
            /*if(currentIncidentRecord.Operate_And_Managed_By__c && currentIncidentRecord.Operate_And_Managed_By__c == "NBN")
            {
                servList.push("Product: Broadband");
            }
            else if(currentIncidentRecord.Operate_And_Managed_By__c && currentIncidentRecord.Operate_And_Managed_By__c == "Foxtel")
            {
                servList.push("Product: Foxtel");
            }*/
            //assign the final list to be displayed
			// Making call out done to true, so that it will be considered to store cache
			component.set('v.serviceCalloutDone',true);
            component.set("v.ServWrapList",servList);
        }
    },
    formatDate : function(currentWorkingDate) {
        let locale = 'en-AU';
        let options = { weekday: 'short',month: 'short', day: 'numeric', year: 'numeric'};
        return currentWorkingDate.toLocaleDateString(locale, options);
    }
})