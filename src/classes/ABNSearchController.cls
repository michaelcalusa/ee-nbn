/***************************************************************************************************
    Class Name          : ABNSearchController
    Version             : 1.0 
    Created Date        : 16-Aug-2017
    Author              : Rupendra Kumar Vats
    Description         : Logic to call ABN services to return ABN Business Name and ACN number
    Input Parameters    : ABN Number 
    Output Parameters   : ABN Business Name and ACN number

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats       16-Aug-2017       Logic to call ABN services to return ABN Business Name and ACN numberNov
    * Krishna Sai		  03-Nov-2017       Added Logic to get Entity Status, Entity Code and Entity Description
    * Dilip Athley        23-Nov-2017       Modified the Logic to get Entity Status, Entity Code and Entity Description
    * Dilip Athley        23-Nov-2017       Added the logic to check whether the ABN is of Trust or Superannuation.
****************************************************************************************************/ 
public class ABNSearchController{
    
    @AuraEnabled
    public static ABNInformation doABNSearch(String strABNNumber){
        ABNInformation ABNObj;
        try{
            //Initialize end points
            String strEndPoint = 'http://abr.business.gov.au/ABRXMLSearch/AbrXmlSearch.asmx';
            String strSOAPAction = 'http://abr.business.gov.au/ABRXMLSearch/SearchByABNv201408';
            String strNS = 'http://abr.business.gov.au/ABRXMLSearch/';
            String strASICNumber = '', strError = '';
            String strEntityType;
            String strEnv = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><SearchByABNv201408 xmlns="http://abr.business.gov.au/ABRXMLSearch/"><searchString>' + strABNNumber + '</searchString><includeHistoricalDetails>N</includeHistoricalDetails><authenticationGuid>' + Label.ABNGUID + '</authenticationGuid></SearchByABNv201408></soap:Body></soap:Envelope>';
            // Process POST request
            HttpRequest req = new HttpRequest();
            req.setEndpoint(strEndPoint);
            //req.setEndpoint('callout:ABN_API');
            req.setHeader('SOAPAction', strSOAPAction);
            req.setMethod('POST');
            req.setBody(strEnv);
            req.setHeader('Content-Type', 'text/xml');
            Http http = new Http();
            HTTPResponse res = http.send(req);
            
            system.debug('--ABN Response--' + res.getBody());
            DOM.Document xmlDOC;
            Dom.XMLNode rootNode;
            String strResponse = res.getBody();
            
            if(strResponse.contains('<exception>')){
                // Service returned the known exception
                
                xmlDOC = new DOM.Document();
                xmlDOC.load(res.getBody());
                rootNode = xmlDOC.getRootElement();
                
                for(Dom.XMLNode child : rootNode.getChildElements()[0].getChildElements()[0].getChildElements()[0].getChildElements()){
                    //System.debug('---child-1--' + child.getName());
                    if(child.getName() == 'response'){
                        String strErrorMsg = child.getChildElement('exception',strNS).getChildElement('exceptionDescription',strNS).getText();
                        if(!string.isEmpty(strErrorMsg) && strErrorMsg.contains('not a valid ABN')){
                            strError = 'Please enter a valid ABN Number';
                        }
                        else
                        {
                            strError = strErrorMsg;
                        }
                        
                    }
                }
                
                ABNObj = new ABNInformation('',new List<String>(),false,strError,'','','',false,'');
            }
            else
            {
                // Service returned the ABN Details and process XML response
                
                xmlDOC = new DOM.Document();
                xmlDOC.load(res.getBody());
                rootNode = xmlDOC.getRootElement();
                
                Set<String> setBusiness = new Set<String>();
                List<String> lstBusiness = new List<String>();
                String entityStatusCode,entityTypeCode,entityDescription,custClass;
                boolean eType = false;
                for(Dom.XMLNode child : rootNode.getChildElements()[0].getChildElements()[0].getChildElements()[0].getChildElements()) {
                    System.debug('---child---' + child.getName());
                    if(child.getName() == 'response'){
                        if(!string.isEmpty(child.getChildElement('businessEntity201408',strNS).getChildElement('ASICNumber',strNS).getText())){
                            strASICNumber = child.getChildElement('businessEntity201408',strNS).getChildElement('ASICNumber',strNS).getText();
                                                     
                        }
                        else{
                            strASICNumber = '';
                        }
                        
                       System.debug('---strASICNumber---' + strASICNumber);
                        
                    
                        // Get the Business Name from 'mainName',businessName',entityStatusCode,entityTypeCode,entityDescription  elements
                        for(Dom.XMLNode bChild : child.getChildElement('businessEntity201408',strNS).getChildElements()){
                           system.debug('--bChild--' + bChild.getName());
                            if(bChild.getName() == 'mainName'){
                                if(!string.isEmpty(bChild.getChildElement('organisationName',strNS).getText())){
                                    setBusiness.add(bChild.getChildElement('organisationName',strNS).getText());    
                                }
                            }
                            
                            if(bChild.getName() == 'businessName'){
                                if(!string.isEmpty(bChild.getChildElement('organisationName',strNS).getText())){
                                    setBusiness.add(bChild.getChildElement('organisationName',strNS).getText());    
                                }
                            }
                              if(bChild.getName() == 'legalName'){
                                string givenName, otherGivenName='', familyName;
                                if(!string.isEmpty(bChild.getChildElement('givenName',strNS).getText())){
                                    givenName = bChild.getChildElement('givenName',strNS).getText()+' ';    
                                }
                               if(!string.isEmpty(bChild.getChildElement('otherGivenName',strNS).getText())){
                               		otherGivenName = ' '+bChild.getChildElement('otherGivenName',strNS).getText()+' ';
                               }
                               if(!string.isEmpty(bChild.getChildElement('familyName',strNS).getText())){
                               		familyName = bChild.getChildElement('familyName',strNS).getText();
                               }
                               setBusiness.add(givenName+otherGivenName+familyName);
                            }
                            
                              if(bChild.getName() == 'entityStatus'){
                                if(!string.isEmpty(bChild.getChildElement('entityStatusCode',strNS).getText())){
                                    entityStatusCode = bChild.getChildElement('entityStatusCode',strNS).getText();    
                                }
                            }
                            if(bChild.getName() == 'entityType'){
                                if(!string.isEmpty(bChild.getChildElement('entityTypeCode',strNS).getText())){
                                    entityTypeCode = bChild.getChildElement('entityTypeCode',strNS).getText();    
                                }
                            }
                            if(bChild.getName() == 'entityType'){
                                if(!string.isEmpty(bChild.getChildElement('entityDescription',strNS).getText())){
                                    entityDescription = bChild.getChildElement('entityDescription',strNS).getText();    
                                }
                            }
                        }
                    }
                }
                //Entity_Type_Mapping__mdt custom metadata has all the Trust and Superannuation type so will query that to retrieve whether the ABN is for them or not.
                lstBusiness.addAll(setBusiness);
                //Entity_Type_Mapping__mdt[] eT = [select id,Entity__c,Entity_Type__c from Entity_Type_Mapping__mdt where Entity_Type__c =:entityDescription];
                
               List<Entity_Type_Mapping__mdt> lstEntityMapping = [select id,Entity__c,Entity_Type__c from Entity_Type_Mapping__mdt];
                for(Entity_Type_Mapping__mdt etm: lstEntityMapping){
                    string e = etm.Entity_Type__c.toUpperCase();
                    string ed = entityDescription.toUpperCase();
                    if( e == ed){
                        etype = True;
                    }
                }
                
              /*  
                if (eT.size() > 0)
                {
                    etype = True;
                    system.debug('----eT' + eT);
                }
              	else
                {
                    eType = false;
                }*/
                
                custClass  = (String)[select Class__c,id,Entity__c,Entity_Code__c from NewDev_Billing_Entity_Mapping__mdt where Entity__c =:entityDescription and Entity_Code__c =:entityTypeCode limit 1].Class__c;
                
                              

               	ABNObj = new ABNInformation(strASICNumber,lstBusiness,true,'',entityStatusCode,entityTypeCode,entityDescription,eType,custClass);
               	system.debug('----setBusiness' + lstBusiness);
                
              
            }
        }
        Catch(Exception ex){
            System.debug('---Exception: ABNSearchController - doABNSearch()---' + ex.getMessage());
            ABNObj = new ABNInformation('',new List<String>(),false,'Error occured','','','',false,'');
        }
        system.debug('---ABNObj--' + ABNObj);
        return ABNObj;
    }
    
    public class ABNInformation implements NewDevelopmentsControllerInterface{
        @AuraEnabled
        public String strABNNumber {get;set;}
        @AuraEnabled
        public List<String> lstBusinessName {get;set;}
        @AuraEnabled
        public Boolean isSuccess {get;set;}
        @AuraEnabled
        public String strErrorMsg {get;set;}
        @AuraEnabled
        public String entityStatusCode {get;set;}
        @AuraEnabled
        public String entityTypeCode {get;set;}
        @AuraEnabled
        public String entityDescription {get;set;}
        @AuraEnabled
        public String custClass {get;set;}
        @AuraEnabled
        public boolean eType {get;set;}
        
        public ABNInformation(String strABNNo, List<String> lstBusiness, Boolean isSuccess, String strMsg, String entityStatusCode,String entityTypeCode,String entityDescription, boolean eType, String custClass){
            this.strABNNumber = strABNNo;
            this.lstBusinessName = lstBusiness;
            this.isSuccess = isSuccess;
            this.strErrorMsg = strMsg;
            this.entityStatusCode = entityStatusCode;
            this.entityTypeCode = entityTypeCode;
            this.eType = eType;
            this.entityDescription = entityDescription;
            this.custClass = custClass;
        }
    }
}