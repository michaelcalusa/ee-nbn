/***************************************************************************************************
    Class Name  : ABNSearchController_Test
    Class Type  : Test Class 
    Version     : 1.0 
    Created Date: 06-September-2017
    Function    : This class contains unit test scenarios for ABNSearchController apex class which handles view ABN API for Tech Choice
    Used in     : None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            06/09/2017                 Created
****************************************************************************************************/
@isTest(seeAllData = false)
public class ABNSearchController_Test{
    static testMethod void TestMethod_doABNSearch() {
        // Verify system call out exception
        ABNSearchController.doABNSearch('12345');
        
        // Verify Responses using Mock classes for Success and Failure
        MockHttpResponseGenerator mockResp = new MockHttpResponseGenerator();
        
        // Verify the success call out
        mockResp.strResponseType = 'ABNSuccess';
        Test.setMock(HttpCalloutMock.class, mockResp);
        ABNSearchController.doABNSearch('12345');
        
        // Verify the exception call out
        mockResp.strResponseType = 'ABNException';
        ABNSearchController.doABNSearch('12345');
        
    }
}