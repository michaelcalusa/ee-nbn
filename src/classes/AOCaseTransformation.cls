/*
Class Description
Creator: v-dileepathley
Purpose: This class will be used to Transform the Aged Order Data from Case Stage to Case base table
Modifications:

*/
public class AOCaseTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
	public String aoQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {
        
    }
    public AOCaseTransformation(String ejId)
    {
        //Creating the record for Scheduled Job(Batch_Job__c)
         exJobId = ejId;
         Batch_Job__c bj = new Batch_Job__c();
         bj.Name = 'AgedOrder_Transform'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        //Query the records from Case Stage which has Transformation Status as New.
            aoQuery = 'SELECT Id, Name, CreatedDate, CreatedById, LastModifiedById, Access_Seeker_Name__c, Access_Seeker_Id__c, Access_Seeker_Reference_Id__c, Commodity__c, Priority_Assist__c, NBNREFERENCEID__c, Order_Status__c, Location_Id__c, Installation_Date__c, Date_Ordered__c, Access_Seeker_Remediation_Date__c, Transformation_Status__c, BBU__c, Copper_Pair_ID__c, Copper_Pair_Service_Class__c, CSA_ID__c, NBNCATEGORY__c, Order_Type__c, HELD_Planned_Remediation_Date__c, Product_Type__c, Transition_Service_Identifier__c,  Schedule_Job__c FROM Aged_Orders_Case_Stage__c Where Transformation_Status__c = \'New\' order by createddate asc';
            return Database.getQueryLocator(aoQuery);   
    }
    public void execute(Database.BatchableContext BC,List<Aged_Orders_Case_Stage__c> aoIlist)
    {
        try
        {
            //Setting the allow field truncation to true, this will truncate the length if it exceeds the length of the column.
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.allowFieldTruncation = true;
            AssignmentRule AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
            dml.assignmentRuleHeader.assignmentRuleId= AR.id;
            //Variable declaration
            List<Case> caseList = new List<Case>();
            List<Aged_Orders_Case_Stage__c> sucList = new List<Aged_Orders_Case_Stage__c>();
            List<Aged_Orders_Case_Stage__c> usucList = new List<Aged_Orders_Case_Stage__c>();
            List<Site__c> siteList = new List<Site__c>();
            Id recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Aged Order').getRecordTypeId(); 
            Id uVRecTypeID = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId(); 
            list <string> accId = new list<string>();
            list <string> locIds = new list<string>();
            list <string> sAgedO = new list<string>();
            list <string> usAgedO = new list<string>();
            string asId, locId;
            map<string,Id> acc = new map<string,Id>();
            map<string,Id> site = new map<string,Id>();
            map <String, Case> caseMap = new map<String, Case>();
            map <String, Site__c> siteMap = new map<String, Site__c>();
            list<Database.Error> err;
            String msg, fAffected;
            String[]fAff;
            StatusCode sCode;
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
		

            
            //Getting the Access Seeker Id and Location Id into an String array, which will help in creating a Map.
            for (Aged_Orders_Case_Stage__c c: aoIlist)
            {
            	if(!String.isBlank(c.Access_Seeker_Id__c))
                {
                	asId = c.Access_Seeker_Id__c;
                	accId.add(asId); 
                }
                if(!String.isBlank(c.Location_Id__c))
                {
                    locId = c.Location_Id__c;
                    locIds.add(locId);
                }
            }
            
            //Creating the map of Access Seeker Id and Salesforce Id of account, which will help in associating the Case Record to Account.
            for( Account a: [Select Access_Seeker_ID__c,Id from Account where RecordType.DeveloperName = 'Retail_Service_Provider' and Access_Seeker_ID__c in :accId FOR Update])
            {
                acc.put(a.Access_Seeker_ID__c, a.Id); 
            }
            
            //Creating the map of Location and Salesforce Id of site, which will help in associating the Case Record to Site.
            for( Site__c st: [Select Location_ID__c,Id from Site__c where RecordType.DeveloperName != 'MDU_CP' and Location_ID__c in :locIds FOR Update])
            {
                site.put(st.Location_ID__c, st.Id); 
            }
            
            //Looping through each record and populate the base case fields.
            for (Aged_Orders_Case_Stage__c aoCase: aoIlist)
            {
                Case agedCase = new Case();
                agedCase.RecordTypeId = recordTypeId;
                agedCase.Access_Seeker_Id__c = aoCase.Access_Seeker_Id__c;
                if(!String.isBlank(aoCase.Access_Seeker_Id__c))
                {
                    agedCase.Access_Seeker_Name__c = acc.get(aoCase.Access_Seeker_Id__c);
                }
                if(!String.isBlank(aoCase.Location_Id__c))//checking whether the staging table record has Location Id
                {
                    if(!String.isBlank(site.get(aoCase.Location_Id__c)))//checking whether the Location Id exist in Salesforce Site Object
                    {
                        agedCase.Site__c = site.get(aoCase.Location_Id__c);
                    }
                    else // if Location Id is not present in Site than add it and associate it to the correct Case record.
                    {
                        agedCase.Site__r = new site__c(MDU_CP_On_Demand_Id__c = aoCase.Location_Id__c);
                        
                        Site__c newSite = new Site__c(Name = aoCase.Location_Id__c, MDU_CP_On_Demand_Id__c = aoCase.Location_Id__c,Location_Id__c = aoCase.Location_Id__c, Site_Name__c = aoCase.Location_Id__c, Site_Address__c =aoCase.Location_Id__c, RecordTypeId =  uVRecTypeID);
                        siteMap.put(aoCase.Location_Id__c,newSite);
                    }
                }
                agedCase.Access_Seeker_Reference_Id__c = aoCase.Access_Seeker_Reference_Id__c;
                agedCase.Commodity__c = aoCase.Commodity__c;
                agedCase.Priority_Assist__c = aoCase.Priority_Assist__c;
                agedCase.Order_ID__c = aoCase.NBNREFERENCEID__c;
                agedCase.Status_Of_The_Order__c = aoCase.Order_Status__c;
                agedCase.CSA_Id__c = aoCase.CSA_Id__c;
                agedCase.Installation_Type__c = aoCase.NBNCATEGORY__c;
                agedCase.Order_Type__c = aoCase.Order_Type__c;
                agedCase.Product_Type__c = aoCase.Product_Type__c;
                agedCase.Battery_Backup__c = aoCase.BBU__c;
                agedCase.Transition_Service_Identifier__c = aoCase.Transition_Service_Identifier__c;
                agedCase.Copper_Pair_ID__c = aoCase.Copper_Pair_ID__c;
                if(!String.isBlank(aoCase.Copper_Pair_Service_Class__c))
                {
                	agedCase.Copper_Pair_Service_Class__c = Decimal.valueOf(aoCase.Copper_Pair_Service_Class__c);
                }
                //agedCase.CreatedById = aoCase.CreatedById;
                agedCase.Date_Ordered__c = aoCase.Date_Ordered__c;
                agedCase.Planned_Remediation_Date__c = aoCase.HELD_Planned_Remediation_Date__c;
                
                agedCase.setOptions(dml);
                caseMap.put(agedCase.Order_ID__c, agedCase);  
                
                
            }//loop ends
            //inserting the sites, which re not present in Salesforce.
            siteList = siteMap.values();
            List<Database.saveResult> sResults = Database.insert(siteList,false);
            caseList = caseMap.values();
            Schema.SObjectField orderId = Case.Fields.Order_ID__c;
            //Upserting the records
            List<Database.upsertResult> uResults = Database.upsert(caseList,orderId,false);
            //traversing through the results and modifying the staging table
            for(Integer idx = 0; idx < uResults.size(); idx++)
            {   
                if(!uResults[idx].isSuccess())
                {
                    err=uResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='AgedCase_Transformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),
                    isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=caseList[idx].Order_ID__c,Schedule_Job__c=bjId));
                   // aoIList[idx].Transformation_Status__c = 'Error';
                   // aoIList[idx].Schedule_Job__c = bjId;
                   usAgedO.add(caseList[idx].Order_ID__c);
                }
                else
                {
                        if(String.isBlank(caseList[idx].Access_Seeker_Id__c) || String.isNotBlank(acc.get(caseList[idx].Access_Seeker_Id__c)))
                        {
                            sAgedO.add(caseList[idx].Order_ID__c);
                        }
                        else 
                        {
                            sAgedO.add(caseList[idx].Order_ID__c);
                            errList.add(new Error_Logging__c(Name='AgedCase_Transformation',Error_Message__c='Access Seeker Account Record is not associated',Fields_Afftected__c='Access Seeker Name',isCreated__c=uResults[idx].isCreated(),
                            isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=caseList[idx].Order_ID__c,Schedule_Job__c=bjId));
                        }
                    	//aoIList[idx].Schedule_Job__c = bjId;
                }
                    
              }
              for (Aged_Orders_Case_Stage__c aoSuc: [select Id, Transformation_status__c, Schedule_Job__c from Aged_Orders_Case_Stage__c where NBNREFERENCEID__c in: sAgedO FOR Update])
           	  {
                  aoSuc.Transformation_status__c = 'Created Case Record';
                  aoSuc.Schedule_Job__c = bjId;
                  sucList.add(aoSuc);
              }
            
              for (Aged_Orders_Case_Stage__c aoUsuc: [select Id, Transformation_status__c, Schedule_Job__c from Aged_Orders_Case_Stage__c where NBNREFERENCEID__c in: usAgedO FOR Update])
           	  {
                  aoUsuc.Transformation_status__c = 'Error';
                  aoUsuc.Schedule_Job__c = bjId;
                  usucList.add(aoUsuc);
              }
            
            Insert errList;
            update sucList;
            update usucList;
            if(errList.isEmpty())      
            {
                status = 'Completed';
            } 
            else
            {
                status = 'Error';
            }  
            recordCount = recordCount + aoIlist.size();       
        }
        catch (Exception e)
        {
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='AgedCase_Transformation',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }
    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        
        AsyncApexJob conJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
        {
            bj.Status__c= conJob.Status;
        }
        else
        {
             bj.Status__c=status;
        }
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = conJob.id;
        bj.End_Time__c  = conJob.CompletedDate;
        upsert bj;
       Database.executeBatch(new AgedWOTransformation(exJobId));
     }
}