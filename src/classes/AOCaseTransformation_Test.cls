/***************************************************************************************************
Class Name:  AOCaseTransformation_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 22-09-2017
Function    : This test class contains unit test scenarios for AOCaseTransformation schedule class.
Used in     : None
Modification Log:
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Dilip Athley      22-09-2017                Created
****************************************************************************************************/
@isTest
private class AOCaseTransformation_Test
{
    
    static void getRecords (){  
        // Custom Setting record creation
        AO_Transformation_Job__c AOTransformation = new AO_Transformation_Job__c ();
        AOTransformation.Name = 'AOTransformation';
        AOTransformation.on_off__c = true;
        insert AOTransformation;
        
        //system.assertequals(accRec,null);
        // Create Aged_Orders_Case_Stage__c record
        List<Aged_Orders_Case_Stage__c> listOfAO = new List<Aged_Orders_Case_Stage__c>();
        Aged_Orders_Case_Stage__c aoRec = new Aged_Orders_Case_Stage__c();
        aoRec.NBNREFERENCEID__c = 'ORD000001';
        aoRec.Order_Status__c = 'ACKNOWLEDGED';
        aoRec.CSA_Id__c = 'TestCSA';
        aoRec.NBNCATEGORY__c = 'Test Category';
        aoRec.Transformation_Status__c = 'New';
        aoRec.Access_Seeker_Reference_Id__c = 'ASC00001';
        aoRec.Commodity__c = 'FTTN';
        aoRec.Priority_Assist__c = true;
        aoRec.BBU__c = 'BBU';
        aoRec.Copper_Pair_ID__c = 'testcopper';
        aoRec.Copper_Pair_Service_Class__c = '12345';
        aoRec.Location_Id__c = 'LOC123456789012';
        aoRec.Access_Seeker_ID__c = 'ASI000000151588';
        aoRec.Date_Ordered__c = system.Now();
        aoRec.HELD_Planned_Remediation_Date__c = system.now();
        listOfAO.add(aoRec);
        Aged_Orders_Case_Stage__c aoRec1 = new Aged_Orders_Case_Stage__c();
        aoRec1.NBNREFERENCEID__c = '';
        aoRec1.Order_Status__c = 'ACKNOWLEDGED';
        aoRec1.CSA_Id__c = 'TestCSA';
        aoRec1.NBNCATEGORY__c = 'Test Category';
        aoRec1.Transformation_Status__c = 'New';
        aoRec1.Access_Seeker_Reference_Id__c = 'ASC000012';
        aoRec1.Commodity__c = 'FTTN';
        aoRec1.Priority_Assist__c = true;
        aoRec1.BBU__c = 'BBU';
        aoRec1.Copper_Pair_ID__c = 'testcopper1';
        aoRec1.Copper_Pair_Service_Class__c = '';
        aoRec1.Location_Id__c = 'LOC000125223911';
        aoRec1.Access_Seeker_ID__c = 'ASI00000022';
        aoRec1.Date_Ordered__c = system.Now();
        aoRec1.HELD_Planned_Remediation_Date__c = system.now();
        listOfAO.add(aoRec1);
        insert listOfAO;
        

    }
    static testMethod void AOCaseTransformationTest1(){
        getRecords();
        Id accRecTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        // Create Account record
        Account accRec = new Account();
        accRec.name = 'Test Account';
        accRec.on_demand_id__c = 'Account1';
        accRec.Access_Seeker_ID__c = 'ASI000000151588';
        accRec.RecordTypeId = accRecTypeID;
        insert accRec;
        Id uVRecTypeID = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId();
        Site__c newSite = new Site__c(Name = 'LOC000125223911', MDU_CP_On_Demand_Id__c = 'LOC000125223911',Location_Id__c = 'LOC000125223911', Site_Name__c = 'LOC000125223911', Site_Address__c ='LOC000125223911', RecordTypeId =  uVRecTypeID);
        insert newSite;
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('AOTransformationScheduleClass_Test',CRON_EXP, new AOTransformation());
        Test.stopTest();
    }

}