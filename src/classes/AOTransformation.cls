/*
Class Description
Creator: v-dileepathley
Purpose: This class will be used to schedule the job for Aged Order Case Transformation job
Modifications:

*/
public class AOTransformation implements Schedulable
{
    
	public static void execute(SchedulableContext ctx)
   	{
   		if (AO_Transformation_Job__c.getinstance('AOTransformation') <> null && AO_Transformation_Job__c.getinstance('AOTransformation').on_off__c)
   		{
	   		CronTrigger ct = [SELECT id, TimesTriggered,StartTime, NextFireTime,EndTime,State,CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger WHERE Id = :ctx.getTriggerId()];  
		    Extraction_Job__c ej = new Extraction_Job__c();
		    ej.Name = String.valueof(ct.ID);  // Extraction Job ID
		    ej.Start_Time__c = System.now();
		   //ej.End_Time__c = ct.EndTime; 
		    ej.Extraction_Job_Name__c = ct.CronJobDetail.Name+System.now();
		    ej.Status__c = 'Processing'; 
		    ej.RecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Extraction_Job__c'  and developername = 'AO_Case_Transformation'].Id;
		    Database.SaveResult sResult = database.insert(ej,false);
         	String ejId = sResult.getId();
		    Database.executeBatch(new AOCaseTransformation(ejId));
   		}
   	}
}