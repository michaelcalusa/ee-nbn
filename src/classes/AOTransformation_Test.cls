/***************************************************************************************************
Class Name:  AOTransformation_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 22-09-2017
Function    : This test class contains unit test scenarios for AOTransformation schedule class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Dilip Athley      22-09-2017                Created
****************************************************************************************************/
@isTest
public class AOTransformation_Test {
    static testMethod void AOTransformationScheduleClass_Test(){
        // Custom Setting record creation
        AO_Transformation_Job__c AOTransformation = new AO_Transformation_Job__c ();
        AOTransformation.Name = 'AOTransformation';
        AOTransformation.on_off__c = true;
        insert AOTransformation;
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('AOTransformationScheduleClass_Test',CRON_EXP, new AOTransformation());
        AOTransformation trnsfm = new AOTransformation();
        Test.stopTest();        
    }
}