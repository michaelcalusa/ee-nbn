/***************************************************************************************************
    Class Name          : InsertStageApplicationCRMOD
    Version             : 1.0 
    Created Date        : 15-Nov-2017
    Author              : Krishna Sai
    Description         : Class to Establish Account Contact Relationship in CRMOD.
    Input Parameters    : NewDev Application Object

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 

****************************************************************************************************/
public class AccountContactRelCRMOD {
    public static void InsertAccountContactRelCRMOD(NewDev_Application__c newDevAppFormAccConRel,String billingConId,string accConId,string Accountstr,string ContractSignatoryConId, string ApplicantConId){
       string jsessionId;
       String serviceEndpoint;
        Http http = new Http();
        HttpRequest req;
        
        try{
        if (string.isNotBlank(ContractSignatoryConId))
            {
                 String AccConpayablejsonInput = '{'+
                                             '\"AccountContacts\": '+'[ {'+
                                                            '\"ContactId\":'+'\"'+ (ContractSignatoryConId == null ?'' :ContractSignatoryConId) +'\"'+
                                                             
                                             '}]'+
                              '}';
                 // Process POST request
                              			  
                  HttpResponse ContractSignatoryRes;
                  req = new HttpRequest();
                  req.setMethod('POST');
                  req.setTimeout(120000);
                  req.setHeader('content-type', 'application/vnd.oracle.adf.resource+json');
      			  jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                  req.setHeader('Cookie', jsessionId);
                  req.setBody(AccConpayablejsonInput);
                  serviceEndpoint= '/' + Accountstr +'/' + 'child' + '/' + 'AccountContacts';
                  req.setEndpoint('callout:Insert_CRMOD_Account'+serviceEndpoint);
                  ContractSignatoryRes = http.send(req);
                  System.debug(ContractSignatoryRes.getBody());
            }
            
            if (string.isNotBlank(ApplicantConId))
            {
                 String ApplicantjsonInput = '{'+
                                             '\"AccountContacts\": '+'[ {'+
                                                            '\"ContactId\":'+'\"'+ ApplicantConId +'\"'+
                                                             
                                             '}]'+
                              '}';
                 // Process POST request
                              			  
                  HttpResponse ApplicantRes;
                  req = new HttpRequest();
                  req.setMethod('POST');
                  req.setTimeout(120000);
                  req.setHeader('content-type', 'application/vnd.oracle.adf.resource+json');
      			  jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                  req.setHeader('Cookie', jsessionId);
                  req.setBody(ApplicantjsonInput);
                  serviceEndpoint= '/' + Accountstr +'/' + 'child' + '/' + 'AccountContacts';
                  req.setEndpoint('callout:Insert_CRMOD_Account'+serviceEndpoint);
                  ApplicantRes = http.send(req);
                  System.debug(ApplicantRes.getBody());
            }
            
            
        if(string.isNotBlank(billingConId) )
        {
        	String AccContactjsonInput = '{'+
                                             '\"AccountContacts\": '+'[ {'+
                                                            '\"ContactId\":'+'\"'+ billingConId +'\"'+
                                                             
                                             '}]'+
                              '}';
            
        // Process POST request
                              			  
                  HttpResponse AccConBillingRes;
                  req = new HttpRequest();
                  req.setMethod('POST');
                  req.setTimeout(120000);
                  req.setHeader('content-type', 'application/vnd.oracle.adf.resource+json');
      			  jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                  req.setHeader('Cookie', jsessionId);
                  req.setBody(AccContactjsonInput);
                  serviceEndpoint= '/' + Accountstr +'/' + 'child' + '/' + 'AccountContacts';
                  req.setEndpoint('callout:Insert_CRMOD_Account'+serviceEndpoint);
                  AccConBillingRes = http.send(req);
                  System.debug(AccConBillingRes.getBody());
        }
            
            
            if (string.isNotBlank(accConId))
            {
                 String AccConpayablejsonInput = '{'+
                                             '\"AccountContacts\": '+'[ {'+
                                                            '\"ContactId\":'+'\"'+ accConId +'\"'+
                                                             
                                             '}]'+
                              '}';
                 // Process POST request
                              			  
                  HttpResponse AccConPayableRes;
                  req = new HttpRequest();
                  req.setMethod('POST');
                  req.setTimeout(120000);
                  req.setHeader('content-type', 'application/vnd.oracle.adf.resource+json');
      			  jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                  req.setHeader('Cookie', jsessionId);
                  req.setBody(AccConpayablejsonInput);
                  serviceEndpoint= '/' + Accountstr +'/' + 'child' + '/' + 'AccountContacts';
                  req.setEndpoint('callout:Insert_CRMOD_Account'+serviceEndpoint);
                  AccConPayableRes = http.send(req);
                  System.debug(AccConPayableRes.getBody());
            }
           
           InsertDevelopmentCRMOD.InsertDevCRMOD(newDevAppFormAccConRel, billingConId, Accountstr, accConId, ContractSignatoryConId,ApplicantConId);
        
        }
         catch(Exception ex)
        {
            GlobalUtility.logMessage('Error', 'AccountContactRelCRMOD', 'InsertAccountContactRelCRMOD', ' ', '', '', '', ex, 0);
        }
    }
             
    }