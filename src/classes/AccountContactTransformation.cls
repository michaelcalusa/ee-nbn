/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class will be used to transform and transfer Account-Contact Relationship records from Junction_Object_Int to 
AccountContactRelation table
Modifications:
*/
public class AccountContactTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
    public String junObjIntQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {            
    }
    public AccountContactTransformation(String ejId)
    {
    	 exJobId = ejId;
    	 Batch_Job__c bj = new Batch_Job__c();
    	 bj.Name = 'AccountContactTransform'+System.now(); 
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult;
         sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        junObjIntQuery = 'select Id,CRM_Record_Id__c,Object_Id__c,Child_Id__c,Primary_Account__c,Primary_Contact__c,Contact_Role__c from Junction_Object_Int__c where Transformation_Status__c = \'Exported from CRM\' AND Object_Name__c = \'Account\' AND Child_Name__c = \'Contact\' AND Event_Name__c = \'Associate\' order by crm_modified_date__c asc'; 
        return Database.getQueryLocator(junObjIntQuery);            
    }
    public void execute(Database.BatchableContext BC,List<Junction_Object_Int__c> listOfAccConInt)
    {        
      	try
        {
            List<String> crmAccConId = new List<String>();
            List<String> crmId = new List<String>();
            List<AccountContactRelation> listOfAccCon = new List<AccountContactRelation>();
            list <string> accId = new list<string>();
			list <string> conId = new list<string>();
            list<Junction_Object_Int__c> recordStatus = new list<Junction_Object_Int__c>();
            List<Error_Logging__c> errAccConList = new List<Error_Logging__c>();
			
			for (Junction_Object_Int__c junAcc: listOfAccConInt)
			{
			    string s = junAcc.Object_Id__c;
			    accId.add(s);
			}
			map<string,Id> acc = new map<string,Id>();
			for( Account a: [Select on_demand_id__c,Id from Account where on_demand_id__c in :accId FOR UPDATE])
			{
			    acc.put(a.on_demand_id__c, a.Id); 
			}
			
			for (Junction_Object_Int__c junCon: listOfAccConInt)
			{
			    string s = junCon.Child_Id__c;
			    conId.add(s);
			}
			map<string,Id> con = new map<string,Id>();
			for( Contact c: [Select on_demand_id_P2P__c,Id from Contact where on_demand_id_P2P__c in :conId FOR UPDATE])
			{
			    con.put(c.on_demand_id_P2P__c, c.Id); 
			}
			
           for(Junction_Object_Int__c accconInt : listOfAccConInt)
            {
                
                AccountContactRelation accCon = new AccountContactRelation();
                   
                accCon.AccountId = acc.get(accconInt.Object_Id__c);
                accCon.ContactId = con.get(accconInt.Child_Id__c);
                accCon.Primary_Account__c = accconInt.Primary_Account__c;
                accCon.Primary_Contact__c = accconInt.Primary_Contact__c;
                accCon.Roles = accconInt.Contact_Role__c; 
                accCon.On_Demand_Id__c = String.valueOf(acc.get(accconInt.Object_Id__c)) + String.valueOf(con.get(accconInt.Child_Id__c));
                if (String.isNotBlank(acc.get(accconInt.Object_Id__c)) && String.isNotBlank(con.get(accconInt.Child_Id__c)))
                {
                    listOfAccCon.add(accCon);
                    crmId.add(accconInt.Id); 
                }
                else
                {
                    errAccConList.add(new Error_Logging__c(Name='Account Contact Intersection',Error_Message__c='Account/Contact record is not available',Fields_Afftected__c='AccountId/ContactId',isSuccess__c=false,CRM_Record_Id__c=accconInt.Id,Schedule_Job__c=bjId));
                    accconInt.Transformation_Status__c = 'Error';
                    accconInt.Schedule_Job_Transformation__c = bjId;
                    recordStatus.add(accconInt);
                }
            }
            
            List<Database.SaveResult> upsertAccConResults;
      		upsertAccConResults = Database.insert(listOfAccCon,false);
        	System.debug('Insert finished:');
            List<Database.Error> upsertAccConError;
            String errorMessage, fieldsAffected;
      		String[] listOfAffectedFields;
            StatusCode sCode;
            
            system.debug(crmId);
            for(Integer j=0; j<upsertAccConResults.size();j++)
            {
                Junction_Object_Int__c updJunInt = new Junction_Object_Int__c();
                if(!upsertAccConResults[j].isSuccess())
                {
                    upsertAccConError = upsertAccConResults[j].getErrors();
                    for(Database.Error er: upsertAccConError)
                    {
                        sCode = er.getStatusCode();
                        errorMessage = er.getMessage();
                        listOfAffectedFields = er.getFields();
                    }
                    fieldsAffected = String.join(listOfAffectedFields,',');
                    errAccConList.add(new Error_Logging__c(Name='Account Contact Intersection',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isSuccess__c=upsertAccConResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=crmId[j],Schedule_Job__c=bjId));
                    updJunInt.Transformation_Status__c = 'Error';
                    updJunInt.Id = crmId[j];
                    updJunInt.Schedule_Job_Transformation__c = bjId;
                    
               		recordStatus.add(updJunInt);
                }
                else
                {
                    updJunInt.Transformation_Status__c = 'Copied to Base Table';
                    updJunInt.Id = crmId[j];
                    updJunInt.Schedule_Job_Transformation__c = bjId;
               		recordStatus.add(updJunInt);               
                }
            }
            Insert errAccConList;
            for (integer i = 0;i<recordStatus.size();i++)
            {
                System.debug(recordStatus[i]);
            }
            
            Update recordStatus;       
            if(errAccConList.isEmpty())      
            {
            	status = 'Completed';
            } 
            else
            {
            	status = 'Error';
            }  
             recordCount = recordCount+listOfAccConInt.size();
            system.debug(recordCount);
          
        }                           
        catch(Exception e)
        { 
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='Account Contact Junction Object Transformation',Error_Message__c= e.getMessage()+' Line Number: '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
          
    }    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob accConJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
       	if(String.isBlank(status))
       	{
       		bj.Status__c= accConJob.Status;
       	}
       	else
       	{
       		 bj.Status__c=status;
       	}
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = accConJob.id;
        bj.End_Time__c  = accConJob.CompletedDate;
        upsert bj;
       if (CRM_Transformation_Job__c.getinstance('AssociatePrimaryContact') <> null && CRM_Transformation_Job__c.getinstance('AssociatePrimaryContact').on_off__c)
   	   { 
       		Database.executeBatch(new AssociatePrimaryContact(exJobId));
       }
    }        
}