/***************************************************************************************************
Class Name:  AccountContactTransformation_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 31-10-2016
Function    : This class contains unit test scenarios for  AccountContactTransformation apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       31-10-2016                Created
****************************************************************************************************/
@isTest
private class AccountContactTransformation_Test {
    static void getRecords (){  
        // Custom Setting record creation
        TestDataUtility.getListOfCRMTransformationJobRecords(true);
        // Create Junction_Object_Int__c record
        date CRM_Modified_Date = date.newinstance(1963, 01,24);
        List<Junction_Object_Int__c> actintList = new List<Junction_Object_Int__c>();
        Junction_Object_Int__c actint = new Junction_Object_Int__c();
        actint.Name='TestName';
        actint.CRM_Modified_Date__c=CRM_Modified_Date;
        actint.Transformation_Status__c = 'Exported from CRM';
        actint.Object_Name__c = 'Account';
        actint.Child_Name__c = 'Contact';
        actint.Event_Name__c = 'Associate';
        actint.Object_Id__c = 'Account123';
        actint.Child_Id__c = 'Contact123';
        actintList.add(actint);
        insert actintList;
    }
    static testMethod void AccountContactTransformation_test1(){
        getRecords();
        // Create Account record
        Account accRec = new Account();
        accRec.name = 'Test Account';
        accRec.on_demand_id__c = 'Account123';
        insert accRec;
        // Create Contact record
        Contact contactRec = new Contact ();
        contactRec.on_demand_id_P2P__c = 'Contact123';
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        insert contactRec;
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());
        Test.stopTest(); 
    }
    static testMethod void AccountContactTransformation_test2(){
        getRecords();
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());
        Test.stopTest(); 
    }
    static testMethod void AccountContactTransformation_test3(){
        // Custom Setting record creation
        CRM_Transformation_Job__c CRM_Transformation_Job_CS = new CRM_Transformation_Job__c ();
        CRM_Transformation_Job_CS.Name = 'Transformation Job';
        CRM_Transformation_Job_CS.on_off__c = true;
        insert CRM_Transformation_Job_CS;
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('AccountContactTransformation_test3',CRON_EXP, new TransformationJob());
        TransformationJob trnsfm = new TransformationJob();
        Test.stopTest();        
    }
}