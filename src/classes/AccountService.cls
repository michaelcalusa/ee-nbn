public class AccountService {

    private static Map<String, Id> ACCOUNT_RECORD_TYPES = HelperUtility.pullAllRecordTypes('Account');

    public Account getAccount(String abn, String businessName) {
        List<Account> existingAccounts = [
                SELECT Id, BillingCountry
                FROM Account
                WHERE ABN__c = :abn
                AND Name = :businessName
        ];

        if (existingAccounts != null && existingAccounts.size() > 0) {
            return existingAccounts.get(0);
        }

        return null;
    }

    public Account getOrCreateAccount(String recordType, String abn, String businessName, String email, String contactNumber, String customerType) {
        Account account = getAccount(abn, businessName);

        if (account == null) {
            Id recordTypeId = ACCOUNT_RECORD_TYPES.get(recordType);
            account = new Account();
            account.ABN__c = abn;
            account.Name = businessName;
            account.RecordTypeId = recordTypeId;
            account.BillingCountry = 'Australia';
            account.Phone = contactNumber;
            account.Email__c = email;
            account.Customer_Type__c = customerType;

            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.AllowSave = true;
            Database.insert(account, dml);
        }
        return account;
    }

}