@IsTest
public class AccountServiceTest {
    private static Map<String, Id> ACCOUNT_RECORD_TYPES = HelperUtility.pullAllRecordTypes('Account');
    private static final String ABN = '11223491505';
    private static final String COMPANY_NAME = 'cool company';
    private static final AccountService accountService = new AccountService();

    @IsTest
    private static void shouldGetAccountByRecordTypeNameAndABN() {
        Test.startTest();

        Account existingAccount = createAccount('Customer');

        Account actualAccount = accountService.getAccount(ABN, COMPANY_NAME);

        Assert.equals(actualAccount.Id, existingAccount.Id);

        Test.stopTest();
    }

    @IsTest
    private static void shouldReturnNullWhenBusinessNameNotMatch() {
        Test.startTest();

        createAccount('Customer');

        Account actualAccount = accountService.getAccount(ABN, 'lame company');

        Assert.isNull(actualAccount);

        Test.stopTest();
    }

    @IsTest
    private static void shouldReturnNullWhenABNNotMatch() {
        Test.startTest();

        createAccount('Customer');

        Account actualAccount = accountService.getAccount('12345', COMPANY_NAME);

        Assert.isNull(actualAccount);

        Test.stopTest();
    }


    @IsTest
    private static void shouldNotCreateNewAccountWhenMatchingAccountExists() {
        Test.startTest();

        Account existingAccount = createAccount('Customer');

        Account actualAccount = accountService.getOrCreateAccount('Customer', ABN, COMPANY_NAME, '', '', '');

        Assert.equals(actualAccount.Id, existingAccount.Id);

        Test.stopTest();
    }

    @IsTest
    private static void shouldCreateNewAccountWhenABNNotMatch() {
        Test.startTest();

        Account existingAccount = createAccount('Customer');

        Account actualAccount = accountService.getOrCreateAccount('Customer', '86136533741', COMPANY_NAME, 'abc@test.com', '0278945612', 'Person');

        Assert.notEquals(actualAccount.Id, existingAccount.Id);

        Assert.equals(actualAccount.ABN__c, '86136533741');
        Assert.equals(actualAccount.Name, COMPANY_NAME);
        Assert.equals(actualAccount.Email__c, 'abc@test.com');
        Assert.equals(actualAccount.Phone, '0278945612');
        Assert.equals(actualAccount.Customer_Type__c, 'Person');

        Test.stopTest();
    }

    @IsTest
    private static void shouldCreateNewAccountWhenNameNotMatch() {
        Test.startTest();

        Account existingAccount = createAccount('Customer');

        Account actualAccount = accountService.getOrCreateAccount('Customer', ABN, 'lame company', 'abc@test.com', '0278945612', 'Organisation');

        Assert.notEquals(actualAccount.Id, existingAccount.Id);

        Assert.equals(actualAccount.ABN__c, ABN);
        Assert.equals(actualAccount.Name, 'lame company');
        Assert.equals(actualAccount.Email__c, 'abc@test.com');
        Assert.equals(actualAccount.Phone, '0278945612');
        Assert.equals(actualAccount.Customer_Type__c, 'Organisation');

        Test.stopTest();
    }

    private static Account createAccount(String recordType) {
        Id recordTypeId = ACCOUNT_RECORD_TYPES.get(recordType);
        Account existingAccount = new Account();
        existingAccount.RecordTypeId = recordTypeId;
        existingAccount.ABN__c = ABN;
        existingAccount.Name = COMPANY_NAME;
        insert existingAccount;
        return existingAccount;
    }

}