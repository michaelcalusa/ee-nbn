/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class will be used to transform and transfer Account records from Accounts_Int to Account
Environment Dependency : Need to change the Record Type Id based on the environment
Modifications:
Dilip Athley 07/02 - changed the code to include Developer and Builder values in Account Types field, as per the story # FY17-85
*/
public class AccountTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
    public String accQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    public class GeneralException extends Exception
    {
        
    }
    public AccountTransformation(String ejId)
    {
    	 exJobId = ejId;
    	 Batch_Job__c bj = new Batch_Job__c();
    	 bj.Name = 'AccountTransform'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        accQuery = 'select Id, CRM_Account_Id__c, Primary_Contact_Id__c, Status__c, Account_Name__c, Annual_Revenues__c, Main_Phone__c, Main_Fax__c, WebSite__c, ABN__c, Description__c, Number_Street__c,Address_2__c,Address_3__c, City__c,State__c,Post_Code__c,Country__c,Bill_Number_Street__c,Bill_Address_2__c, Bill_Address_3__c,Bill_City__c,Bill_State__c,Bill_Post_Code__c,Bill_Country__c, Developer_Billing_Account_Number__c, Credit_Check_Date__c,  Credit_Check_Result__c, Owner__c, Account_Types__c, Parent_Account_Id__c, Entity_Type__c, Registered_Entity_Name__c, ACN__c, Location__c, Approximate_of_properties__c,High_Risk__c,Legal_Review_Required__c,Trustee_Name__c,Trustee_ACN__c,Non_ABN_Type__c,Documents_Received__c,Eligible_for_Deemed_To_Accept__c,Upfront_Required__c,Credit_Check_Required__c,Credit_Check_Comments_Conditions__c,Billing_Site_ID__c,Developer_Classification__c,Developer_Group__c from Account_Int__c where Transformation_Status__c = \'Extracted from CRM\' order by CreatedDate'; 
        return Database.getQueryLocator(accQuery);    
        
    }
    
    public void execute(Database.BatchableContext BC,List<Account_Int__C> accInts)
    {
        try
        {            
            
            List<Account> oobAccs = new List<Account>();
            String rTypeIdPropertyDeveloper = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Account'  and name = 'Property Developer'].Id; 
            String rTypeIdCompany = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Account'  and name = 'Company'].Id;            
            String addr1, addr2, addr3, billAddr1, billAddr2, billAddr3;
            List<String> listOfAccIntIds = new List<String>();
            List<String> accParentId = new List<String>();
            map <String, Account> accMap = new map<String, Account>();
            for(Account_Int__C accInt : accInts)
            {

                Account a = new Account();
                
                Database.DMLOptions dml = new Database.DMLOptions();
            	dml.allowFieldTruncation = true;
                                
                a.Name = accInt.Account_Name__c;
                a.Location__c = accInt.Location__c;
                a.Registered_Entity_Name__c = accInt.Registered_Entity_Name__c;
                a.On_Demand_Id__c = accInt.CRM_Account_Id__c;                
                a.CRMOD_Owner__c = accInt.Owner__c;
                a.AnnualRevenue = accInt.Annual_Revenues__c;
                a.Phone = accInt.Main_Phone__c;
                a.Fax = accInt.Main_Fax__c;
                a.Website = accInt.WebSite__c;
                a.ABN__c = accInt.ABN__c;
                a.ACN__c = accInt.ACN__c;
                a.Entity_Type__c = accInt.Entity_Type__c;
                a.Description = accInt.Description__c;
                a.Approximate_of_properties__c = accint.Approximate_of_properties__c;
                a.High_Risk__c = accInt.High_Risk__c;
                a.Legal_Review_Required__c = accInt.Legal_Review_Required__c;
                a.Trustee_Name__c = accint.Trustee_Name__c;
                a.Trustee_ACN__c = accInt.Trustee_ACN__c;
                a.Non_ABN_Type__c = accInt.Non_ABN_Type__c;
                a.Documents_Received__c = accInt.Documents_Received__c;
                a.Eligible_for_Deemed_To_Accept__c = accInt.Eligible_for_Deemed_To_Accept__c;
                a.Upfront_Required__c = accInt.Upfront_Required__c;
                a.Credit_Check_Required__c = accInt.Credit_Check_Required__c;
                a.Credit_Check_Comments_Conditions__c = accInt.Credit_Check_Comments_Conditions__c;
                a.Billing_Site_ID__c = accInt.Billing_Site_ID__c;
                a.Developer_Classification__c = accInt.Developer_Classification__c;
                a.Developer_Group__c = accInt.Developer_Group__c;                
                if(accInt.Number_Street__c == null)
                {
                    addr1 = '';    
                }
                else
                {
                    addr1 = accInt.Number_Street__c;  
                }
                if(accInt.Address_2__c == null)
                {
                    addr2 = '';    
                }
                else
                {
                    addr2 = accInt.Address_2__c;
                }
                if(accInt.Address_3__c == null)
                {
                    addr3 = '';    
                }
                else
                {
                    addr3 = accInt.Address_3__c;
                }
                a.ShippingStreet = addr1 + ' ' + addr2 + ' ' + addr3;                 
                a.ShippingCity = accInt.City__c;
                a.ShippingState = accInt.State__c;
                a.ShippingPostalCode = accInt.Post_Code__c;
                a.ShippingCountry = accInt.Country__c;
                if(accInt.Bill_Number_Street__c == null)
                {
                    billAddr1 = '';
                }
                else
                {
                    billAddr1 = accInt.Bill_Number_Street__c;  
                }
                if(accInt.Bill_Address_2__c == null)
                {
                    billAddr2 = '';
                }
                else
                {
                    billAddr2 = accInt.Bill_Address_2__c;  
                }        
                if(accInt.Bill_Address_3__c == null)
                {
                    billAddr3 = '';
                }
                else
                {
                    billAddr3 = accInt.Bill_Address_3__c;  
                }
                a.BillingStreet = billAddr1 + ' ' + billAddr2 +' '+ billAddr3;
                a.BillingCity = accInt.Bill_City__c;
                a.BillingState = accInt.Bill_State__c;
                a.BillingPostalCode = accInt.Bill_Post_Code__c;
                a.BillingCountry = accInt.Bill_Country__c;               
                a.Billing_ID__c = accInt.Developer_Billing_Account_Number__c;
                String creditCheckDate = accInt.Credit_Check_Date__c;                
                if(!String.isBlank(creditCheckDate))
                {
                    //String ccd = creditCheckDate.substringBefore(' ');
                	//a.Last_Credit_Review_Complete__c = date.parse(ccd.replace('-','/'));     
                	a.Last_Credit_Review_Complete__c = StringToDate.strDate(creditCheckDate);
                }
                a.Credit_Rating__c = accInt.Credit_Check_Result__c;
                a.Status__c = accInt.status__c;

				String accTypes = accInt.Account_Types__c;
               	if(!String.isBlank(accTypes))
                {
                    String aTypes = accTypes.replace(',',';');
                    if(aTypes.contains('Developer') || aTypes.contains('Builder'))
                    {
                        a.RecordTypeId = rTypeIdPropertyDeveloper;                        
                        //String replaceBuilder = aTypes.replace('Builder', ''); v-dileepathley - commented the code as per the story FY17-85
                		//String replaceDeveloper = replaceBuilder.replace('Developer', '');v-dileepathley - commented the code as per the story FY17-85
                		//a.Account_Types__c = replaceDeveloper;v-dileepathley - commented the code as per the story FY17-85
                    } 
                    else
                    {
                        a.RecordTypeId = rTypeIdCompany;
                        //a.Account_Types__c = aTypes;v-dileepathley - commented the code as per the story FY17-85
                    }
                    a.Account_Types__c = aTypes;//v-dileepathley - added the code as per the story FY17-85
                }
                else
                {
                    a.RecordTypeId = rTypeIdCompany;   
                    a.Account_Types__c = accTypes;
                }
                /*if(!String.isBlank(accInt.Parent_Account_Id__c))
                {
                    listOfAccIntIds.add(accInt.CRM_Account_Id__c);
                }*/
                a.ownership__c = 'CRMOD';
                //oobAccs.add(a);          
                //accParentId.add(accInt.Parent_Account_Id__c);
                a.setOptions(dml);
                accMap.put(accInt.CRM_Account_Id__c,a);
            }
			oobAccs = accMap.values();
            List<Database.UpsertResult> upsertAccountResults = Database.Upsert(oobAccs,Account.Fields.On_Demand_Id__c,false);
            List<Database.Error> upsertAccountError;           
            String msg, fAffected;
            String[]fAff;
            StatusCode sCode;
            list<Error_Logging__c> errAccList = new List<Error_Logging__c>();            
            List<Account_Int__c> updAccs = new List<Account_Int__c>();            
            for(Integer idx = 0; idx < upsertAccountResults.size(); idx++)
            {  
                //Account_Int__c ai = new Account_Int__c();
                if(!upsertAccountResults[idx].isSuccess())
                {
                    upsertAccountError=upsertAccountResults[idx].getErrors();                  
                    for (Database.Error er: upsertAccountError)
                    {
                        sCode = er.getStatusCode();
                        msg = er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errAccList.add(new Error_Logging__c(Name='Account Transformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=upsertAccountResults[idx].isCreated(),isSuccess__c=upsertAccountResults[idx].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=oobAccs[idx].On_Demand_Id__c,Schedule_Job__c=bjId));
                    //ai.Transformation_Status__c = 'Error';
                    //ai.CRM_Account_Id__c = oobAccs[idx].On_Demand_Id__c;  
                    //ai.Schedule_Job_Transformation__c = bjId;
                    accInts[idx].Transformation_Status__c = 'Error';                   
                    accInts[idx].Schedule_Job_Transformation__c = bjId;
                    //updAccs.add(ai);
                }
                else
                {
                    if(!String.isBlank(accInts[idx].Parent_Account_Id__c))
                    {
                    	//ai.Transformation_Status__c = 'Ready for Parent Account Association'; 
                        accInts[idx].Transformation_Status__c = 'Ready for Parent Account Association'; 
                    }
                    else if(!String.isBlank(accInts[idx].Primary_Contact_Id__c) && accInts[idx].Primary_Contact_Id__c != 'No Match Row Id')
                    {
                        accInts[idx].Transformation_Status__c = 'Ready for Primary Contact Association';    
                    }
                    else
                    {
                        //ai.Transformation_Status__c = 'Copied to Base Table'; 
                        accInts[idx].Transformation_Status__c = 'Copied to Base Table'; 
                    }                    
                    //ai.CRM_Account_Id__c = oobAccs[idx].On_Demand_Id__c;
                    //ai.Schedule_Job_Transformation__c = bjId;
                    //updAccs.add(ai);                                      
                    accInts[idx].Schedule_Job_Transformation__c = bjId;
                }
            }
            Insert errAccList;
            Update accInts;  
            
            if(errAccList.isEmpty())      
            {
            	status = 'Completed';
            } 
            else
            {
            	status = 'Error';
            }
            recordCount = recordCount+ accInts.size();     
        }
        catch(Exception e)
        { 
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='Account Transformation',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
        
    }
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        
        AsyncApexJob accJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
       	if(String.isBlank(status))
       	{
       		bj.Status__c= accJob.Status;
       	}
       	else
       	{
       		 bj.Status__c=status;
       	}
        bj.Batch_Job_ID__c = accJob.id;
        bj.Record_Count__c= recordCount;
        bj.End_Time__c  = accJob.CompletedDate;
        upsert bj;
        if (CRM_Transformation_Job__c.getinstance('AssociateParentAccount') <> null && CRM_Transformation_Job__c.getinstance('AssociateParentAccount').on_off__c)
   		{
       		 Database.executeBatch(new AssociateParentAccount(exJobId));   
        }
    }    
    
}