/***************************************************************************************************
Class Name:  AccountTransformation_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 31-10-2016
Function    : This class contains unit test scenarios for AccountTransformation apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       31-10-2016                Created
****************************************************************************************************/
@isTest
private class AccountTransformation_Test {
    static void getRecords (){  
        // Custom Setting record creation
        TestDataUtility.getListOfCRMTransformationJobRecords(true);
        // Create Junction_Object_Int__c record
        date CRM_Modified_Date = date.newinstance(1963, 01,24);
        List<Junction_Object_Int__c> actintList = new List<Junction_Object_Int__c>();
        Junction_Object_Int__c actint = new Junction_Object_Int__c();
        actint.Name='TestName';
        actint.CRM_Modified_Date__c=CRM_Modified_Date;
        actint.Transformation_Status__c = 'Exported from CRM';
        actint.Object_Name__c = 'Account';
        actint.Child_Name__c = 'Contact';
        actint.Event_Name__c = 'Associate';
        actint.Object_Id__c = 'Account123';
        actint.Child_Id__c = 'Contact123';
        actintList.add(actint);
        insert actintList;
        // Create Account record
        Account accRec = new Account();
        accRec.name = 'Test Account';
        accRec.on_demand_id__c = 'Account1';
        insert accRec;
        // Create Account_Int__c record
        List<Account_Int__c> listOfAccountInt = new List<Account_Int__c> ();
        Account_Int__c accIntRec = new Account_Int__c ();
        accIntRec.Account_Name__c = 'AccName1';
        accIntRec.Transformation_Status__c = 'Extracted from CRM';
        accIntRec.CRM_Last_Modified_Date__c = system.now();
        accIntRec.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec.Account_Types__c = 'Developer,Builder';
        accIntRec.CRM_Account_Id__c = 'Account1';
        listOfAccountInt.add(accIntRec);
        Account_Int__c accIntRec2 = new Account_Int__c ();
        accIntRec2.Account_Name__c = 'AccName2';
        accIntRec2.Transformation_Status__c = 'Extracted from CRM';
        accIntRec2.CRM_Last_Modified_Date__c = system.now();
        accIntRec2.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec2.Account_Types__c = 'Abc';
        accIntRec2.Number_Street__c = 'test';
        accIntRec2.Address_2__c = 'test';
        accIntRec2.Address_3__c = 'test';
        accIntRec2.Bill_Number_Street__c = 'test';
        accIntRec2.Bill_Address_2__c = 'test';
        accIntRec2.Bill_Address_3__c = 'test';
        accIntRec2.CRM_Account_Id__c = 'Account2';
        listOfAccountInt.add(accIntRec2);
        Account_Int__c accIntRec3 = new Account_Int__c ();
        accIntRec3.Account_Name__c = 'AccName3';
        accIntRec3.Transformation_Status__c = 'Extracted from CRM';
        accIntRec3.CRM_Last_Modified_Date__c = system.now();
        accIntRec3.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec3.CRM_Account_Id__c = 'Account3';
        listOfAccountInt.add(accIntRec3);
        Account_Int__c accIntRec4 = new Account_Int__c ();
        accIntRec4.Account_Name__c = 'AccName4';
        accIntRec4.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec4.CRM_Last_Modified_Date__c = system.now();
        accIntRec4.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec4.CRM_Account_Id__c = 'Account4';
        listOfAccountInt.add(accIntRec4);
        insert listOfAccountInt;
        // Create Contact record
        Contact contactRec = new Contact ();
        contactRec.on_demand_id_P2P__c = 'Contact123';
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        insert contactRec;
    }
    static testMethod void AccountTransformationTest1(){
        getRecords();
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());
        Test.stopTest(); 
    }
}