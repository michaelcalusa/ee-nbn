public without sharing class AccountTriggerHandler extends TriggerHandler {
/*------------------------------------------------------------------------
Author:        Rohit Kumar
Company:       Salesforce
Description:   A class created to manage trigger actions from the Account object 
               Responsible for:
               1 - Updating Account reference on Customer On Boarding Steps for Account which was created after Lead Conversion and if Lead had association to Customer On Boarding Steps
               2 - Updating Account reference on Customer On Case for Account which was created after Lead Conversion and if Lead had association to Case
Test Class:    AccountTriggerHandler_Test
History
<Date>            <Authors Name>    <Brief Description of Change> 
--------------------------------------------------------------------------*/
     
    List<Account> accountUpdates = new List<Account>(); //Account List to Update
    List<Qualification_Step__c> qualificationStepUpdates = new List<Qualification_Step__c>(); // Qualification Step records
    List<Case> caseUpdates = new List<Case>(); // Case Update Records
    
    Set<id> accountId = new Set<id>(); // Set of Account Ids
    Set<id> leadId = new Set<id>(); // Set of Lead Ids
      
    Map<id,id> convertedLeadAccount = new Map<id,id>(); //Map of Converted Lead Id and Generated Account Id
    
    public AccountTriggerHandler(){
        if(Test.isRunningTest()){
            this.setMaxLoopCount(10);
        }
        else{
            this.setMaxLoopCount(2);
        }
        
        System.Debug( 'NBN: -> AccountTriggerHandler Invoked' );
    }
    
    protected override void afterInsert(map<id,sObject> newMap) {
        system.debug(trigger.isinsert);
        system.debug(trigger.isupdate);        
        //Check if this BSM record needs to be pushed to MDM   
       //validateToPushMDM(null, newMap, action);
        if(!HelperUtility.isTriggerMethodExecutionDisabled('CreateAccountOutboundMsg'))
        {
            string action = 'Insert';
        	OBMessagePayloadUtilities.createOBMessage(null, newMap, action, 'Account');
        }
		
       System.Debug('NBN: -> AccountTriggerHandler start of afterInsert' + accountId.size());    
       // Create a set of AccountId
        accountId.addAll(newMap.keySet());

        for (Id accId:newMap.keySet()){
            Account accObj = (account)newMap.get(accId);
            
            //Only add accounts that have the lead id populated
            if ( String.isNotBlank( accObj.Lead_Id__c ) ) {
                convertedLeadAccount.put(accObj.Lead_Id__c,accId);
            }
        } 
        // Call Method to Update Customer OnBoarding Reference if Account was generated from Lead        
        updateQualificationStepReference(convertedLeadAccount);
        // Call Method to Update Case Reference if Account was generated from Lead        
        updateCaseReference(convertedLeadAccount);
        System.Debug('NBN: -> AccountTriggerHandler end of afterInsert');  
    }

    protected override void afterUpdate(map<id,SObject> oldMap, map<id,SObject> newMap) {
        string action = 'Update';
        //Check if this BSM record needs to be pushed to MDM   
       //validateToPushMDM(oldMap, newMap, action);
       if(!HelperUtility.isTriggerMethodExecutionDisabled('CreateAccountOutboundMsg'))
       {
        	OBMessagePayloadUtilities.createOBMessage(oldMap, newMap, action, 'Account');
       }
    } 

    protected override void afterDelete(map<id,SObject> oldMap) {
        string action = 'Delete';
        //Check if this BSM record needs to be pushed to MDM   
       //validateToPushMDM(oldMap, null, action);
       if(!HelperUtility.isTriggerMethodExecutionDisabled('CreateAccountOutboundMsg'))
       {
        	OBMessagePayloadUtilities.createOBMessage(oldMap, null, action, 'Account');
       }
    }     
    
    protected override void beforeUpdate(map<id,SObject> oldMap, map<id,SObject> newMap) {
         system.debug('*** Account before Update Trigger ***');
         accountId.addAll(newMap.keySet());
         system.debug('** accountId ==>'+ accountId);
         
         map<String, Id> mapRecordType = new map<string, Id>();
         List<Account> lstCustomerAccounts = new List<Account>();
         map<string, Account> mapCustomerAcc = new map<string, Account>();
         mapRecordType = HelperUtility.pullAllRecordTypes('Account');
         
         for(SObject sObj:  newMap.values()){
            Account accObj = (account)sObj;
            if(accObj.recordTypeId == string.valueof(mapRecordType.get('Customer')) && accObj.Is_This_Contact_Matrix_Field_Update__c){
                mapCustomerAcc.put(accObj.id,accObj);
            }
         }
         
         if(mapCustomerAcc.size()>0){
            checkFieldUpdates(mapCustomerAcc, oldMap);
         }
         
    } 
        

    /*end trigger handler overrides*/
    //====================================================================
    
     /* private methods */
    //====================================================================
    /**
     * @desc This method 
     * @param [map<id,id> leadGeneratedAccount]
     */
    private void checkFieldUpdates(map<string,Account> mapCustomerAcc, map<id,SObject> oldMap){
        set<String> fieldSet = new Set<String>();
         string updatedFieldContent;
         List<Schema.FieldSetMember> lstFieldSetFields = new List<Schema.FieldSetMember>();
         set<String> changedFieldSet  = new Set<String>();
         map<string, string> mapFieldSet = new map<string, string>();
         map<string, string> mapUpdatedField = new map<string, string>();
         
         lstFieldSetFields = HelperUtility.readFieldSet('FS_ACCOUNT_FIELD_TRACKING', 'Account');
         system.debug('*** lstFieldSetFields ==>'+ lstFieldSetFields);
         for(Schema.FieldSetMember fields :lstFieldSetFields){
            fieldSet.add(fields.getFieldPath());
            mapFieldSet.put(fields.getFieldPath(), fields.getlabel());
         }
        
        if(mapFieldSet!=null)
        {
            for (string accId:mapCustomerAcc.keySet()){
                Account accObj = mapCustomerAcc.get(accId);
                updatedFieldContent = '<b> Customer Account Name: </b> '+ accObj.Name + '\n' ;          
                for(string s: mapFieldSet.keyset()){
                    if(accObj.get(s) != oldMap.get(accObj.Id).get(s)){
                      changedFieldSet.add(s);//adding fields whose value changed
                      updatedFieldContent += mapFieldSet.get(s)+ ' is Changed from ' + oldMap.get(accObj.Id).get(s) + ' to ' + accObj.get(s) + '\n';
                    }
                }
                mapUpdatedField.put(accId, updatedFieldContent);                
            }       
        }
        
        if(changedFieldSet.size()>0){
          for (Id accId:mapCustomerAcc.keySet()){
            Account accObj = mapCustomerAcc.get(accId);
            accObj.Updated_Fields_Info__c = mapUpdatedField.get(accId);
          }           
        }   
    }   
    
 
    /* private methods */
    //====================================================================
    /**
     * @desc This method would update Customer On Boarding with generated account id records which were associated with lead being converted
     * @param [map<id,id> leadGeneratedAccount]
     */
    private void updateQualificationStepReference(map<id,id> leadGeneratedAccount){
      
       System.Debug( 'NBN: -> leadGeneratedAccountsize - ' + leadGeneratedAccount.size());
       
       if ( leadGeneratedAccount.size() > 0 ) {
           // Create a set of all lead Id which got converted    
           leadId.addAll(leadGeneratedAccount.keyset());
    
           System.Debug( 'NBN: -> updateQualificationStepReference Method Invoked');
    
           // Query all Qualification Step records associated with Lead, so that they can be updated with new Account Id
           for(Qualification_Step__c qualificationSteps : [Select Id,
                                                               Related_Lead__c,
                                                               Related_Account__c 
                                                           FROM Qualification_Step__c 
                                                           WHERE Related_Lead__c in : leadId ]) {
               // Create list of Customer On Boarding records 
               qualificationStepUpdates.add(new Qualification_Step__c( Id = qualificationSteps.Id, Related_Account__c = leadGeneratedAccount.get(qualificationSteps.Related_Lead__c)));
           }
    
          System.Debug( 'NBN: -> Total number of Qualification Step records being updated ' + qualificationStepUpdates.size() );
    
           try{
             if(qualificationStepUpdates.size() > 0){
                // Update the onboarding records
                Database.SaveResult[] lsr = Database.update(qualificationStepUpdates, false );
                 }
                 //log the error 
             }catch(Exception ex){ GlobalUtility.logMessage('Error','AccountTriggerHandler','updateQualificationStepReference','','','','',ex,0); }
        }
    }
    
     /* private methods */
    //====================================================================
    /**
     * @desc This method would update Case generated account id records which were associated with lead being converted
     * @param [map<id,id> leadGeneratedAccount]
     */
    private void updateCaseReference(map<id,id> leadGeneratedAccount){
      
       System.Debug( 'NBN: -> leadGeneratedAccountsize - ' + leadGeneratedAccount.size());
      
       if ( leadGeneratedAccount.size() > 0 ) {
           // Create a set of all lead Id which got converted    
           leadId.addAll(leadGeneratedAccount.keyset());
    
           System.Debug( 'NBN: -> updateCaseReference Method Invoked');
    
           // Query all Case records associated with Lead, so that they can be updated with new Account Id
           for(Case relatedCase : [Select Id,Lead__c
                                   FROM Case 
                                   WHERE Lead__c in : leadId ]) {
                                   
               System.Debug( 'NBN: -> Matching Account Id for Case ' + leadGeneratedAccount.get(relatedCase.Lead__c) );
                                   
               // Create list of Case
               caseUpdates.add(new case( Id = relatedCase.Id, AccountId = leadGeneratedAccount.get(relatedCase.Lead__c)));
           }
    
          System.Debug( 'NBN: -> Total number of Case records being updated ' + caseUpdates.size() );
    
           try{
             if(caseUpdates.size() > 0){
                // Update the onboarding records
                Database.SaveResult[] lsr = Database.update(caseUpdates, false );
                 }
                 //log the error 
             }catch(Exception ex){ GlobalUtility.logMessage('Error','AccountTriggerHandler','updateCaseReference','','','','',ex,0); }
        }
    }

      /* end private methods */
    //====================================================================
    
     // exception class
    public class AccountTriggerHandlerException extends Exception {} 

}