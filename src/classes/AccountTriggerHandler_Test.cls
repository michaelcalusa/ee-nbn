@isTest
private class AccountTriggerHandler_Test{
/*------------------------------------------------------------------------
Author:        Ashutosh
Company:       Salesforce
Description:   Test class for AccountTriggerHandler Class
               1 - Test Lead Conversion Process
               2 - Test if qualification steps are mapped to an account.
               3 - Test if Case is mapped to an Account
               
Class:           AccountTriggerHandler
History
<Date>            <Authors Name>    <Brief Description of Change> 
23rd-Nov-2015       Rohit Kumar       Modified test class to cover updateCaseReference method 
26th-Mar-2017       Sukumar Salla     Modified test class to cover checkFieldUpdates
14th-Aug-2018       Shubham Jaiswal   Added steps to complete customer qualification steps
--------------------------------------------------------------------------*/    
    static list<lead> testLeadList;
    static list<Case> testCaseList;
    
    /***
        Test Lead Conversion Process
    ***/
    static testMethod void testConvertLead(){
    
        testLeadList = TestDataUtility.createLeadTestRecords(1,true,'John','Smith','NBNTestCompany','Qualified');
        testCaseList = TestDataUtility.createCaseTestRecords(1,false); // Create Case Record
     //   Id profileId=[Select id from profile where name='NBN Account Management'].Id;
      //  User userObj=TestDataUtility.createTestUser(true,profileId);
        // insert case with lead id
        testCaseList[0].Lead__c = testLeadList[0].id;
        // insert Case
        //insert testCaseList;
        
        List<Qualification_Step__c> qStep = [Select Status__c
                                                From 
                                             Qualification_Step__c
                                                 Where
                                             Related_Lead__c=:testLeadList[0].id];
        for(Qualification_Step__c qS:qStep){
            qS.Status__c = 'Completed';
        }                                      
        
        update qStep;      
          
        test.startTest();
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(testLeadList[0].id);
        lc.setDoNotCreateOpportunity(false);
        lc.setConvertedStatus('Qualified');
      //  system.runas(userObj){
         Database.LeadConvertResult lcr = Database.convertLead(lc);
       // }
       
        test.stopTest();
        /*System.assert(lcr.isSuccess()); 
        
        Lead lObj = [Select ConvertedAccountId from Lead where id=:testLeadList[0].id Limit 1];
        System.assert(lObj.ConvertedAccountId<>null);
        System.assert( [Select id from Qualification_Step__c where Related_Account__c=:lObj.ConvertedAccountId].size()>0 );
        System.assert( [Select id from Case where AccountId=:lObj.ConvertedAccountId].size()>0);*/
    }
    
    
    /*** TEST CheckFieldUpdats Process - Account - BeforeUpdate ***/
     static testMethod void testCheckFieldUpdates(){
        List<Account> lstAcc = TestDataUtility.getListOfAccounts(1, true);
        for(Account acc: lstAcc){
            acc.ACN__c = '12345678';
            acc.Is_This_Contact_Matrix_Field_Update__c= true;
        }  
        update lstAcc;     
     } 
}