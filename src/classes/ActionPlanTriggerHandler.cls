/************************************************************************************************************
Version                 : 1.0 
Created Date            : 
Description/Function    :   
Used in                 : 
Modification Log        :
* Developer                   Date                   Description
* -------------------------------------------------------------------------                
* SUKUMAR SALLA             13-06-2017              Created the Class
**************************************************************************************************************/
public class ActionPlanTriggerHandler{
    //Class level variables that are commonly used
    private boolean isExecuting = false;
    private integer batchSize;
    private List<Action_Plan__c> trgOldList = new List<Action_Plan__c> ();
    private List<Action_Plan__c> trgNewList = new List<Action_Plan__c> ();
    private Map<id,Action_Plan__c> trgOldMap = new Map<id,Action_Plan__c> ();
    private Map<id,Action_Plan__c> trgNewMap = new Map<id,Action_Plan__c> ();
    private static boolean isAsync;
    // Below 7 boolean variables are used to Prevent recursion
    public static boolean isBeforeInsertFirstRun = true;
    public static boolean isBeforeUpdateFirstRun = true;
    public static boolean isBeforeDeleteFirstRun = true;
    public static boolean isAfterInsertFirstRun = true;
    public static boolean isAfterUpdateFirstRun = true;
    public static boolean isAfterDeleteFirstRun = true;
    public static boolean isAfterUndeleteFirstRun = true;
    /***************************************************************************************************
    Method Name         : ActionPlanTriggerHandler 
    Method Type         : Constructor
    Version             : 1.0 
    Created Date        : 
    Function            : 
    Input Parameters    : 
    Output Parameters   : 
    Description         :  
    Used in             : 
    Modification Log    :
    * Developer                   Date                   Description
    * --------------------------------------------------------------------------------------------------                 
    * SUKUMAR SALLA                                       Created
    ****************************************************************************************************/  
    public ActionPlanTriggerHandler(boolean isExecuting, integer batchSize, List<Action_Plan__c> trgOldList, List<Action_Plan__c> trgNewList, Map<id,Action_Plan__c> trgOldMap, Map<id,Action_Plan__c> trgNewMap){
        this.isExecuting = isExecuting;
        this.BatchSize = batchSize;
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
        ActionPlanTriggerHandler.isAsync = System.isBatch() || System.isFuture();
    }
    public void OnBeforeInsert(){}
    public void OnBeforeUpdate(){
       system.debug('***ActionPlan Trigger - Before Update***');
        validateActionPlan(trgNewList, trgNewMap, trgOldMap);
    
    }
    public void OnBeforeDelete(){}
    public void OnAfterInsert(){
         //if(!HelperUtility.isTriggerMethodExecutionDisabled('AssociateAccountToContact')){
           // if(isBeforeInsertFirstRun){
                system.debug('***ActionPlan Trigger - After Insert***');
                createAssociatedSubTasks(trgNewList, null);
              //  isBeforeInsertFirstRun = false;
           // }
        //}     
    }
    public void OnAfterUpdate(){
        
    }
    public void OnAfterDelete(){}
    public void OnUndelete(){}
    /*---------------------------------------------------------------------------------------------------------------------------------------------------*/
    
     // This method used to 
    public void createAssociatedSubTasks(List<Action_Plan__c> trgNewList, Map<id,Action_Plan__c> trgOldMap){
         List<Task> listOfTasksToInsert = new List<Task>();
         List<Sub_Tasks__mdt> lstSubTasks = [select id, Sequence_Number__c, Task_Name__c, Task_Record_Type__c from Sub_Tasks__mdt where Task_Record_Type__c='Senior_Operational_Engagement' ORDER BY Sequence_Number__c ASC];
         String userId = UserInfo.getUserId();
        for( Action_Plan__c ap : trgNewList) {
            for(Sub_Tasks__mdt st: lstSubTasks){
                Task t = new Task();
                t.OwnerId = userId;
                t.Subject = st.Task_Name__c;
                t.Status = 'Open';
                t.Priority = 'Normal';
                t.WhatId = ap.Id;   
                t.RecordTypeId = GlobalCache.getRecordTypeId('Task', GlobalConstants.SENIOR_OPS_ENGAGEMENT_TASK_RECTYPE_NAME);  //'0120k0000004Mo6';
                listOfTasksToInsert.add(t);
            }           
        }       
       
        if(listOfTasksToInsert.size()>0)
        {
           Insert listOfTasksToInsert;
        }
    }
   // This method used to validate Dates on Action Plan based on related tasks
    public void validateActionPlan(List<Action_Plan__c> trgNewList, Map<id,Action_Plan__c> trgNewMap, Map<id,Action_Plan__c> trgOldMap){    
        
        Map<string, List<task>> mapActionPlansAndRelatedTasks = new Map<string, List<task>>();
        String allErrors = '';
                
        for(task t:[select id, whatId, Status, Start_Date__c, Completed_Date__c from Task where whatId in: trgNewMap.keyset()]){ 
            if(!mapActionPlansAndRelatedTasks.containskey(t.whatId)){
                mapActionPlansAndRelatedTasks.put(t.whatId, new List<task>());
                mapActionPlansAndRelatedTasks.get(t.whatId).add(t);
            }
            else{
                mapActionPlansAndRelatedTasks.get(t.whatId).add(t);
            }           
        }
        
        for(string apid: mapActionPlansAndRelatedTasks.keyset()){
            Action_Plan__c ap =  trgNewMap.get(apid);
            List<task> lstTasks = mapActionPlansAndRelatedTasks.get(apid);
            system.debug('**task ==>'+ lstTasks );
            Boolean startDateErrorFlag = false;
            Boolean completedDateErrorFlag = false;
            
            if(ap.Start_Date__c!=null && ap.Completed_Date__c!=null && ap.Start_Date__c>ap.Completed_Date__c && ap.Status__c=='Completed')
            {
                allErrors = allErrors+'</br>'+'Start Date should be set earlier than the Completed Date';
            }
            
            if(ap.Status__c!='Completed' && ap.Completed_Date__c!=null)
            {
                
                allErrors = allErrors+'</br>'+'Completed Date cannot be set.';
            }
            
            for(Task t: lstTasks){           
                if(t.Start_Date__c < ap.Start_Date__c){
                  if(!startDateErrorFlag){
                    allErrors = allErrors+'</br>'+'Start Date must be set earlier than any of Sub task Start Date';
                    startDateErrorFlag = True;
                  }
                }
                if(t.Completed_Date__c > ap.Completed_Date__c && ap.Status__c == 'Completed'){
                 if(!completedDateErrorFlag){
                    allErrors = allErrors+'</br>'+'Completed Date must be set later than any of Sub Task End Date';
                    completedDateErrorFlag = True;
                 }
                }
            }
             if(allErrors!=''){
                    ap.addError(allErrors, false);
                }
        }
    }
    
    // This methods returns the Account instance for the provided external contact
   /* Public Account createAccount(Contact extContact)
    {
        Account a = new Account();            
        a.Name = getAccountName(extContact);
        a.RecordTypeId = GlobalCache.getRecordTypeId('Account', GlobalConstants.houseHold_ACCOUNT_RECTYPE_NAME);
        a.OwnerId = extContact.OwnerId;     
        return a;
    }*/
}