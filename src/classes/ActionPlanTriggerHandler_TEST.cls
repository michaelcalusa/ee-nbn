@isTest
public class ActionPlanTriggerHandler_TEST {
   static testmethod void test1(){
        List<Account> lstAcc = TestDataUtility.getListOfAccounts(1, true);
        Action_Plan__c ap = new Action_Plan__c();
        ap.Account__c = lstAcc[0].id;
        insert ap;
       
        ap.Start_Date__c = system.today();
        update ap;
   }
}