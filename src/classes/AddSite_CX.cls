/***************************************************************************************************
Class Name:         AddSite_CX
Class Type:         Extension Class 
Version:            1.0 
Created Date:       28 July 2016
Function:           This class associate site to the current contact if its not already associated.
Input Parameters:   None 
Output Parameters:  None
Description:        Used in AddSite Visualforce page
Used in:            Siterole creation
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Sukumar Salla      10/07/2015      Created - Version 1.0 Refer ICRM-48 
****************************************************************************************************/ 

public class AddSite_CX {    
    public Site_Contacts__c siteContactObj {get; set;}
    Public Case caseRecord {get; Set;}
    Public String contactId {get; set;}
    Public String siteContactRecordTypeId {get; set;}
    Public String locIdString {get; set;}
    Public String locIAddressString {get; set;}
    Public String siteSFId {get; set;}
    Public Boolean showSaveButton{get; set;}
    Public String noValue{get; set;}
    Public Boolean throwError {get; Set;}
    Public Boolean hasrecords {get; set;}
    Public Boolean showAdvSearchComponent {get; set;}
    Public String caseId {get; set;}
    Public String newSiteId {get; set;}
    Public Set<String> setRelatedSites {get; set;}
    Public Map<String, Id> mapOfSiteRecordTypeNamesAndId {get; set;}
    Public AddressSearch_CX.addressWrapper unStructuredAddress {get; set;}
    
    public AddSite_CX(ApexPages.StandardController sController){          
        throwError = False;
        caseRecord = new Case();
        caseRecord = (Case)sController.getRecord();
        if (caseRecord!=null)
        {
            caseRecord = [select id, status, casenumber,site__c from case where id=:caseRecord.Id];
            caseId = caseRecord.Id;
        } 
        
        if(caseRecord.status=='Closed')
        {
            throwError = True;
            ApexPages.Message PageErrorMessage = new ApexPages.Message(ApexPages.Severity.ERROR,HelperUtility.getErrorMessage('032'));
            ApexPages.addMessage(PageErrorMessage);
        }  
    }
    
    public AddSite_CX(ApexPages.StandardSetController controller) {  
       showSaveButton = False;
       throwError = False;
       showAdvSearchComponent = False;
       contactId  = ApexPages.currentPage().getParameters().get('id');
       // Get the RecordType Id of SiteContact by passing the RecordType Name - Default RecordType is 'Customer Service' 
       // Using CustomLabel: DefaultSiteContactRecordType - Customer Service
       Id siteContactRecordTypeId = Schema.SObjectType.Site_Contacts__c.getRecordTypeInfosByName().get(Label.DefaultSiteContactRecordType).getRecordTypeId();
       siteContactObj = new Site_Contacts__c(RecordTypeID=siteContactRecordTypeId, Related_Contact__c=contactId);
       // Prepare map of Site Record Type and its corresponding ID
       mapOfSiteRecordTypeNamesAndId = new Map<String, Id>();
       mapOfSiteRecordTypeNamesAndId = HelperUtility.pullAllRecordTypes('site__c');
       // Prepare a set of RelatedSite ids
       setRelatedSites = new Set<String>(); 
       for(Site_Contacts__c c: [select id,Related_Contact__c,Related_Site__c from Site_Contacts__c where Related_Contact__c =: contactId])
           setRelatedSites.add(c.Related_Site__c);
           
       system.debug('***setRelatedSites*'+setRelatedSites);    
       system.debug('***mapofCaseRecordTypeNameandId*'+mapOfSiteRecordTypeNamesAndId );
    } 
   
    public pagereference saveSiteContact()
    {        
        pagereference p;
         List<Site__c> s = new List<Site__c>();
        // Site is available in SF map that ID to the related Site
        // If the Searched Site is already Associated to the Contact throw error message.
        // If Site is not available in SF create a new Unverified Site and map that ID to the related site.
        
        system.debug('****siteSFId'+siteSFId);
        if(siteSFId!=null && siteSFId!='')
        {
           If(setRelatedSites.size()>0)
              checkAssociatedSite(setRelatedSites, siteSFId);
           else
              siteContactObj.Related_Site__c = siteSFId;  
        }
        else
        {            
            Site__c newUnverifiedSite = new Site__c();
            if(HelperUtility.checkNullValue(locIdString))
               s = [select id, Location_Id__c from Site__c where Location_Id__c=: locIdString];            
           try{
                If(HelperUtility.checkNullValue(locIAddressString))
                {
                    // If a Site is available with the Location Id
                    if(s.size()>0)
                      newUnverifiedSite = s[0];                   
                    else
                    { 
                         newUnverifiedSite.RecordTypeID= (HelperUtility.checkNullValue(locIdString))? mapOfSiteRecordTypeNamesAndId.get('Verified') : mapOfSiteRecordTypeNamesAndId.get('Unverified');                       
                         newUnverifiedSite.Site_Address__c= 'locIAddressString';
                         newUnverifiedSite.Location_Id__c=locIdString;
                         newUnverifiedSite.Name = locIAddressString;                                                               
                    }

                    // perform ServiceQualification from External System.                   
                    newUnverifiedSite = HelperUtility.executeServiceQualification(newUnverifiedSite, newUnverifiedSite.Location_Id__c);   
                    System.debug('*** newUnverifiedSite ==>'+ newUnverifiedSite);
                    Upsert newUnverifiedSite; 

                    // Check If the site is already Associated with Contact or not                     
                   If(setRelatedSites.size()>0)
                      checkAssociatedSite(setRelatedSites, string.valueof(newUnverifiedSite.id));                       
                    else
                      siteContactObj.Related_Site__c=newUnverifiedSite.id;  
                }
                else
                {
                    throwError = True;
                    ApexPages.Message PageErrorMessage = new ApexPages.Message(ApexPages.Severity.ERROR,HelperUtility.getErrorMessage('005'));
                    ApexPages.addMessage(PageErrorMessage);
                }
            }
            catch(exception ex)
            {
               apexpages.addmessages(ex);
            }
        }
        // Insert SiteContact once we have associated Site.
        if(siteContactObj.Related_Site__c!=null)
        {
            insert siteContactObj;
            p = new PageReference('/'+siteContactObj.id);
            p.setRedirect(true);
        }
        else {
            p=null;
            showSaveButton = True;
        }                
        return p; 
    }
    
     public class sWrapper{
        public string technologyType { get; set; }
        public integer serviceClass { get; set; }
        public string rolloutType { get; set; }
    }
    
    public void getaddressDetails()
    {
        system.debug('*** siteSFId ==>'+ siteSFId );
        system.debug('*** locIdString ==>'+ locIdString);
        system.debug('*** locIAddressString ==>'+ locIAddressString); 
        if(HelperUtility.checkNullValue(siteSFId) || HelperUtility.checkNullValue(locIdString))
         hasrecords = True;
    }
    
    Public void flipAddressComponent(){
        showAdvSearchComponent = True;
    }
    
    Public void checkAssociatedSite(set<String> relatedSites, string relatedSiteId)
    {
        If(!relatedSites.contains(relatedSiteId))
            siteContactObj.Related_Site__c=relatedSiteId;
        else
        {
            throwError = True;
            ApexPages.Message PageErrorMessage = new ApexPages.Message(ApexPages.Severity.ERROR,HelperUtility.getErrorMessage('004'));
            ApexPages.addMessage(PageErrorMessage);
        }  

    }
    
   public pagereference updateCaseWithSiteInfo(){
       system.debug('*** newSiteId ==>'+ newSiteId);        
       case c= [select id,site__c,site__r.name from case where id=:caseId];
       c.site__c = ID.valueof(newSiteId);
       update c; 
         
       // Get all SiteContacts for the related site               
       set<string> setSiteContacts = new set<String>();
       if(newSiteId!=null)
       {
            for(Site_Contacts__c siteC : [Select id, Related_Contact__c, Related_Site__c, Role__c from Site_Contacts__c where Related_Site__c =: newSiteId])
            {
                setSiteContacts.add(siteC.Related_Contact__c);
            }
       }
       // Create siteContacts which are not already associated.
       List<Site_Contacts__c> lstSC = new List<Site_Contacts__c>();       
       for(Case_Contact__c cc: [Select id, Case__c, Case__r.Site__c, Contact__c, Role__c from Case_Contact__c where Case__c=:c.Id])
       {
          if(!setSiteContacts.Contains(string.valueof(cc.Contact__c)))
          {
                Site_Contacts__c sc = new Site_Contacts__c();
                sc.Related_Contact__c = cc.Contact__c;
                sc.Related_Site__c = cc.Case__r.Site__c;
                sc.Role__c = cc.Role__c ;
                lstSC.add(sc);
          }
        }
        // Insert SiteContacts.
        if(lstSC.size()>0)
         upsert lstSC;
        
        pagereference p;       
        p = new pagereference('/'+caseId);
        p.setRedirect(true);
        return p;
    }     
    
    @RemoteAction
    public static string getAddressSearchData(AddressSearch_CX.addressWrapper selectedAddress, string cId){
       site__c s = HelperUtility.instantiateUnStructuredVerifiedSite(selectedAddress);
       upsert s;      
       return string.valueof(s.id);
    }
   
    @RemoteAction
    public static AddressSearch_CX.addressWrapper createUnverifiedSite(AddressSearch_CX.StructuredAddress inputAddress){
        site__c siteRec = HelperUtility.instantiateStructuredUnVerifiedSite(inputAddress);
        insert siteRec;
        AddressSearch_CX.addressWrapper createdUnverifiedAddress = new AddressSearch_CX.addressWrapper ();
        List<Site__c> siteRecords = [SELECT Id, Name, Site_Address__c, Location_Id__c, Asset_Number__c FROM site__c WHERE Id =: siteRec.id LIMIT 1];
        createdUnverifiedAddress.address = siteRecords.get(0).Site_Address__c;
        createdUnverifiedAddress.locationId = siteRecords.get(0).Location_Id__c;
        createdUnverifiedAddress.addressFrom =  'UnVerified';
        createdUnverifiedAddress.assetNumber = siteRecords.get(0).Asset_Number__c;
        createdUnverifiedAddress.sfRecordId = siteRecords.get(0).Id;
        createdUnverifiedAddress.SiteSfRecordName = siteRecords.get(0).Name;
        return createdUnverifiedAddress;
    }
    
    
}