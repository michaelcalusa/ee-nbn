/***************************************************************************************************
Class Name:  AddSite_CX_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 04-11-2016
Function    : This class contains unit test scenarios for  AddSite_CX apex class controller.
Used in     : None
Modification Log :
* Developer                     Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN          04-11-2016                Created
***************************************************************************************************/
@isTest
private class AddSite_CX_Test {
    //Instance variables that are commonly used in multiple test methods
    static Case CaseRecord = new Case ();
    static List<Contact> listOfContacts = new List<Contact> ();
    static List<Site__c> listOfSites = new List<Site__c> ();
    static integer statusCode = 200;
    static Map<String, String> responseHeaders = new Map<String, String> ();
    static String json = '{'+
        '   \"data\":['+
        '      {'+
        '         \"type\":\"location\",'+
        '         \"id\":\"LOC000006657271\",'+
        '         \"attributes\":{'+
        '            \"externalTerritory\":\"FALSE\",'+
        '            \"address\":{'+
        '               \"unstructured\":\"LOT 401 411 CHATSWOOD RD SHAILER PARK QLD 4128\",'+
        '               \"unitNumber\":\"100\",'+
        '               \"levelNumber\":\"100\",'+
        '               \"roadSuffixCode\":\"RD\",'+
        '               \"roadNumber1\":\"411\",'+
        '               \"roadName\":\"CHATSWOOD\",'+
        '               \"roadTypeCode\":\"RD\",'+
        '               \"locality\":\"SHAILER PARK\",'+
        '               \"postCode\":\"4128\",'+
        '               \"state\":\"QLD\",'+
        '               \"lotNumber\":\"401\",'+
        '               \"aliasAddresses\":{'+
        '                  \"unstructured\":\"\"'+
        '               }'+
        '            },'+
        '            \"geoCode\":{'+
        '               \"latitude\":\"-27.647015\",'+
        '               \"longitude\":\"153.179405\",'+
        '               \"geographicDatum\":\"GDA94\",'+
        '               \"srid\":\"4283\"'+
        '            }'+
        '         },'+
        '         \"relationships\":{'+
        '            \"containmentBoundaries\":{'+
        '               \"data\":['+
        '                  {'+
        '                     \"id\":\"4SLA\",'+
        '                     \"type\":\"NetworkBoundary\"'+
        '                  },'+
        '                  {'+
        '                     \"id\":\"4LNH\",'+
        '                     \"type\":\"NetworkBoundary\"'+
        '                  },'+
        '                  {'+
        '                     \"id\":\"S4 - Greater Brisbane\",'+
        '                     \"type\":\"NetworkBoundary\"'+
        '                  }'+
        '               ]'+
        '            }'+
        '         }'+
        '      },'+
        '      {'+
        '         \"type\":\"location\",'+
        '         \"id\":\"LOC000006657272\",'+
        '         \"attributes\":{'+
        '            \"externalTerritory\":\"FALSE\",'+
        '            \"address\":{'+
        '               \"unstructured\":\"LOT 401 411 CHATSWOOD RD SHAILER PARK QLD 4128\",'+
        '               \"unitNumber\":\"100\",'+
        '               \"levelNumber\":\"100\",'+
        '               \"roadSuffixCode\":\"RD\",'+
        '               \"roadNumber1\":\"411\",'+
        '               \"roadName\":\"CHATSWOOD\",'+
        '               \"roadTypeCode\":\"RD\",'+
        '               \"locality\":\"SHAILER PARK\",'+
        '               \"postCode\":\"4128\",'+
        '               \"state\":\"QLD\",'+
        '               \"lotNumber\":\"401\",'+
        '               \"aliasAddresses\":{'+
        '                  \"unstructured\":\"\"'+
        '               }'+
        '            },'+
        '            \"geoCode\":{'+
        '               \"latitude\":\"-27.647015\",'+
        '               \"longitude\":\"153.179405\",'+
        '               \"geographicDatum\":\"GDA94\",'+
        '               \"srid\":\"4283\"'+
        '            }'+
        '         },'+
        '         \"relationships\":{'+
        '            \"containmentBoundaries\":{'+
        '               \"data\":['+
        '                  {'+
        '                     \"id\":\"4SLA\",'+
        '                     \"type\":\"NetworkBoundary\"'+
        '                  },'+
        '                  {'+
        '                     \"id\":\"4LNH\",'+
        '                     \"type\":\"NetworkBoundary\"'+
        '                  },'+
        '                  {'+
        '                     \"id\":\"S4 - Greater Brisbane\",'+
        '                     \"type\":\"NetworkBoundary\"'+
        '                  }'+
        '               ]'+
        '            }'+
        '         }'+
        '      }'+
        '   ]'+
        '}';
    /***************************************************************************************************
    Method Name:  getRecords
    Method Type: Constructor
    Version     : 1.0 
    Created Date: 13 July 2016
    Description:  Common Test data used for all test methods
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       13/07/2015                Created
    ****************************************************************************************************/
    static void getRecords (){
        // Create Account
        List<Account> listOfAccounts = TestDataUtility.getListOfAccounts(1, false);
        // Create Contact
        listOfContacts = TestDataUtility.getListOfContact(1, false);
        for(Contact contactRec : listOfContacts){
            contactRec.accountId = listOfAccounts.get(0).id;
        }
        insert listOfContacts;
        // Create Site
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        listOfSites = TestDataUtility.createSiteTestRecords(3, false);
        integer counter = 0;
        for(Site__c Site : listOfSites){
            Site.Location_Id__c = 'LOC11123456789'+counter;
            Site.MDU_CP_On_Demand_Id__c = 'LOC11123456789'+counter;
            Site.Asset_Number__c = '12345';
            Site.RecordTypeId = recordTypeId;
            Site.Site_Address__c = '100 CHATSWOOD, NSW 2150';
            Site.Name = '100 CHATSWOOD, NSW 2150';
            Site.Locality_Name__c = 'CHATSWOOD';
            Site.Road_Name__c = 'CHATSWOOD';
            counter++;
        }
        insert listOfSites;
        // Create Case
        List<Case> ListOfCases = TestDataUtility.createCaseTestRecords(1,false);
        ID queryRecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId();
        for(Case CaseRec : ListOfCases){
            CaseRec.ContactId = ListOfContacts.get(0).id;
            CaseRec.accountId = ListOfAccounts.get(0).Id;
            CaseRec.recordTypeId = queryRecordTypeId;
            CaseRec.Phase__c = 'Test';
            CaseRec.Category__c = 'Test';
            CaseRec.Status = 'Open';
            CaseRec.Priority = '1-ASAP';
            CaseRec.Subject = 'Test';
            CaseRec.Description = 'Test';
            caserec.Primary_Contact_Role__c='Developer/Builder';
        }
        insert ListOfCases;
        CaseRecord = ListOfCases.get(0);
        // Custom Setting - "Error Message" Records
        List<Error_Message__c> ListofErrorMessage = TestDataUtility.getListOfErrorMessage(true);
    }
    /***************************************************************************************************
    Method Name:  UpdateSiteButton_Test
    Method Type: Test Method
    Version     : 1.0 
    Created Date: 13 July 2016
    Description:  Testing Update Site button from Case layout
    Scenario Type:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       13/07/2015                Created
    ****************************************************************************************************/
    static testmethod void UpdateSiteButton_Test(){
        getRecords();
        Id [] fixedSearchResults= new Id[20];
        for(Site__c site:listOfSites){
            fixedSearchResults.add(site.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        // Testing Visualforce Page -- Standard Controller -- Case Record
        PageReference AddSitePage = Page.AddSite;
        Test.setCurrentPage(AddSitePage);
        ApexPages.StandardController StndCaseContr = new ApexPages.standardController(CaseRecord);
        ApexPages.CurrentPage().getparameters().put('id',CaseRecord.Id);
        AddSite_CX AddSite_CX_Con = new AddSite_CX(StndCaseContr);
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,json,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        AddressSearch_CX.getAddress('CHATSWOOD','5','false');
        AddressSearch_CX.addressWrapper wrap = new AddressSearch_CX.addressWrapper();
        wrap.address = listOfSites.get(0).Site_Address__c;
        wrap.locationId = listOfSites.get(0).Location_Id__c;
        wrap.addressFrom = 'UnVerified';
        wrap.sfRecordId = listOfSites.get(0).Id;
        wrap.assetNumber = listOfSites.get(0).Asset_Number__c;
        wrap.SiteSfRecordName = listOfSites.get(0).Name;
        AddSite_CX.getAddressSearchData(wrap, string.valueOf(CaseRecord.Id));
        AddressSearch_CX.StructuredAddress StructuredAddress_Class = new AddressSearch_CX.StructuredAddress ();
        StructuredAddress_Class.unitNumber = '100';
        StructuredAddress_Class.levelNumber = '100';
        StructuredAddress_Class.lotNumber = '100';
        StructuredAddress_Class.roadNumber1 = '100';
        StructuredAddress_Class.roadName = 'Miller';
        StructuredAddress_Class.roadSuffixCode = 'RD';
        StructuredAddress_Class.localityName = 'North Sydney';
        StructuredAddress_Class.postCode = '2150';
        StructuredAddress_Class.stateTerritoryCode = 'NSW';
        StructuredAddress_Class.AssetNumber = '100';
        StructuredAddress_Class.AssetType = 'Cabinet';
        AddSite_CX.createUnverifiedSite(StructuredAddress_Class);
        AddSite_CX_Con.newSiteId = string.valueOf(listOfSites.get(0).Id);
        AddSite_CX_Con.updateCaseWithSiteInfo();
        CaseRecord.Status = 'Closed';
        update CaseRecord;
        // Testing Visualforce Page -- Standard Controller -- Case Record
        PageReference AddSitePage2 = Page.AddSite;
        Test.setCurrentPage(AddSitePage2);
        ApexPages.StandardController StndCaseContr2 = new ApexPages.standardController(CaseRecord);
        ApexPages.CurrentPage().getparameters().put('id',CaseRecord.Id);
        AddSite_CX AddSite_CX_Con2 = new AddSite_CX(StndCaseContr2);
        Test.stopTest();
    }
    /***************************************************************************************************
    Method Name:  AddSiteButton_Test1
    Method Type: Test Method
    Version     : 1.0 
    Created Date: 13 July 2016
    Description:  Testing Add Site button from Case layout
    Scenario Type:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       13/07/2015                Created
    ****************************************************************************************************/
    static testmethod void AddSiteButton_Test1(){
        getRecords();
        Site_Contacts__c SiteContact = new Site_Contacts__c ();
        SiteContact.Related_Contact__c = listOfContacts.get(0).Id;
        SiteContact.Related_Site__c = listOfSites.get(1).Id;
        insert SiteContact;
        Id [] fixedSearchResults= new Id[20];
        for(Site__c site:listOfSites){
            fixedSearchResults.add(site.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        // Testing Visualforce Page -- Standard Controller -- Case Record
        PageReference AddSitePage = Page.AddSite;
        Test.setCurrentPage(AddSitePage);
        ApexPages.StandardSetController StndContactContr = new ApexPages.StandardSetController(listOfContacts);
        StndContactContr.setSelected(listOfContacts);
        ApexPages.CurrentPage().getparameters().put('id',listOfContacts.get(0).Id);
        AddSite_CX AddSite_CX_Con = new AddSite_CX(StndContactContr);
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,json,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        AddSite_CX_Con.siteSFId = listOfSites.get(0).Id;
        AddSite_CX_Con.locIdString = listOfSites.get(0).Location_Id__c;
        AddSite_CX_Con.locIAddressString = listOfSites.get(0).Name;
        AddSite_CX_Con.getaddressDetails();
        AddSite_CX_Con.saveSiteContact();
        AddSite_CX_Con.flipAddressComponent();
        //AddSite_CX_Con.checkAssociatedSite();
    }
    /***************************************************************************************************
    Method Name:  AddSiteButton_Test2
    Method Type: Test Method
    Version     : 1.0 
    Created Date: 13 July 2016
    Description:  Testing Add Site button from Case layout
    Scenario Type:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       13/07/2015                Created
    ****************************************************************************************************/
    static testmethod void AddSiteButton_Test2(){
        getRecords();
        Site_Contacts__c SiteContact = new Site_Contacts__c ();
        SiteContact.Related_Contact__c = listOfContacts.get(0).Id;
        SiteContact.Related_Site__c = listOfSites.get(1).Id;
        insert SiteContact;
        Id [] fixedSearchResults= new Id[20];
        for(Site__c site:listOfSites){
            fixedSearchResults.add(site.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        // Testing Visualforce Page -- Standard Controller -- Case Record
        PageReference AddSitePage = Page.AddSite;
        Test.setCurrentPage(AddSitePage);
        ApexPages.StandardSetController StndContactContr = new ApexPages.StandardSetController(listOfContacts);
        StndContactContr.setSelected(listOfContacts);
        ApexPages.CurrentPage().getparameters().put('id',listOfContacts.get(0).Id);
        AddSite_CX AddSite_CX_Con = new AddSite_CX(StndContactContr);
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,json,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        AddSite_CX_Con.locIdString = 'LOC123456789012';
        AddSite_CX_Con.locIAddressString = 'test Address';
        AddSite_CX_Con.getaddressDetails();
        AddSite_CX_Con.saveSiteContact();
        AddSite_CX_Con.flipAddressComponent();
    }
    /***************************************************************************************************
    Method Name:  AddSiteButton_Test3
    Method Type: Test Method
    Version     : 1.0 
    Created Date: 13 July 2016
    Description:  Testing Add Site button from Case layout
    Scenario Type:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       13/07/2015                Created
    ****************************************************************************************************/
    static testmethod void AddSiteButton_Test3(){
        getRecords();
        Site_Contacts__c SiteContact = new Site_Contacts__c ();
        SiteContact.Related_Contact__c = listOfContacts.get(0).Id;
        SiteContact.Related_Site__c = listOfSites.get(0).Id;
        insert SiteContact;
        Id [] fixedSearchResults= new Id[20];
        for(Site__c site:listOfSites){
            fixedSearchResults.add(site.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        // Testing Visualforce Page -- Standard Controller -- Case Record
        PageReference AddSitePage = Page.AddSite;
        Test.setCurrentPage(AddSitePage);
        ApexPages.StandardSetController StndContactContr = new ApexPages.StandardSetController(listOfContacts);
        StndContactContr.setSelected(listOfContacts);
        ApexPages.CurrentPage().getparameters().put('id',listOfContacts.get(0).Id);
        AddSite_CX AddSite_CX_Con = new AddSite_CX(StndContactContr);
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,json,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        AddSite_CX_Con.siteSFId = listOfSites.get(0).Id;
        AddSite_CX_Con.locIdString = listOfSites.get(0).Location_Id__c;
        AddSite_CX_Con.locIAddressString = listOfSites.get(0).Name;
        AddSite_CX_Con.getaddressDetails();
        AddSite_CX_Con.saveSiteContact();
        AddSite_CX_Con.flipAddressComponent();
        //AddSite_CX_Con.checkAssociatedSite();
    }
    /***************************************************************************************************
    Method Name:  AddSiteButton_Test_Exception
    Method Type: Test Method
    Version     : 1.0 
    Created Date: 13 July 2016
    Description:  Testing Add Site button from Case layout
    Scenario Type:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       13/07/2015                Created
    ****************************************************************************************************/
    static testmethod void AddSiteButton_Test_Exception(){
        getRecords();
        Site_Contacts__c SiteContact = new Site_Contacts__c ();
        SiteContact.Related_Contact__c = listOfContacts.get(0).Id;
        SiteContact.Related_Site__c = listOfSites.get(1).Id;
        insert SiteContact;
        Id [] fixedSearchResults= new Id[20];
        for(Site__c site:listOfSites){
            fixedSearchResults.add(site.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        // Testing Visualforce Page -- Standard Controller -- Case Record
        PageReference AddSitePage = Page.AddSite;
        Test.setCurrentPage(AddSitePage);
        ApexPages.StandardSetController StndContactContr = new ApexPages.StandardSetController(listOfContacts);
        StndContactContr.setSelected(listOfContacts);
        ApexPages.CurrentPage().getparameters().put('id',listOfContacts.get(0).Id);
        AddSite_CX AddSite_CX_Con = new AddSite_CX(StndContactContr);
        AddSite_CX_Con.locIdString = 'LOC123456789012';
        AddSite_CX_Con.locIAddressString = 'test Address';
        AddSite_CX_Con.getaddressDetails();
        AddSite_CX_Con.saveSiteContact();
        AddSite_CX_Con.flipAddressComponent();
    }
    /***************************************************************************************************
    Method Name:  AddSiteButton_locIAddressString_null
    Method Type: Test Method
    Version     : 1.0 
    Created Date: 13 July 2016
    Description:  Testing Add Site button from Case layout
    Scenario Type:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       13/07/2015                Created
    ****************************************************************************************************/
    static testmethod void AddSiteButton_locIAddressString_null(){
        getRecords();
        Site_Contacts__c SiteContact = new Site_Contacts__c ();
        SiteContact.Related_Contact__c = listOfContacts.get(0).Id;
        SiteContact.Related_Site__c = listOfSites.get(1).Id;
        insert SiteContact;
        Id [] fixedSearchResults= new Id[20];
        for(Site__c site:listOfSites){
            fixedSearchResults.add(site.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        // Testing Visualforce Page -- Standard Controller -- Case Record
        PageReference AddSitePage = Page.AddSite;
        Test.setCurrentPage(AddSitePage);
        ApexPages.StandardSetController StndContactContr = new ApexPages.StandardSetController(listOfContacts);
        StndContactContr.setSelected(listOfContacts);
        ApexPages.CurrentPage().getparameters().put('id',listOfContacts.get(0).Id);
        AddSite_CX AddSite_CX_Con = new AddSite_CX(StndContactContr);
        AddSite_CX_Con.getaddressDetails();
        AddSite_CX_Con.saveSiteContact();
        AddSite_CX_Con.flipAddressComponent();
    }
}