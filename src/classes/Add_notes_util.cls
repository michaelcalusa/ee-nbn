public class Add_notes_util {

        @AuraEnabled 
    public static boolean get_CurrentIncident(String incidentId){
        boolean show_notes_link = false;
        Incident_Management__c	dataIs;
       String query =  QueryUtils.getCommaSeparatedFieldForAnObject('Incident_Management__c');
			dataIs =database.query('select '+query + ' from Incident_Management__c where id=:incidentId');
                // [SELECT AppointmentId__c, SLAResponseOrResolve__c, Appointment_details__c, Appointment_Date_Time__c, Appointment_TimeSlot__c, serviceRegion__c, serviceRestorationSLA__c, SLARegionandServiceRestorationTypeCalc__c, CurrentslaDueDateCalc__c, CurrentSLATypeCalc__c, Appointment_Start_Date__c, Appointment_End_Date__c, Incident_Status__c, Appointment_Status__c, Commitment_Start_Date__c, ResolutionCategoryTier1__c, ResolutionCategoryTier2__c, ResolutionCategoryTier3__c, Resolution_Date__c, Commitment_End_Date__c, Appointment_Type__c, Engagement_Type__c, Current_SLA__c, Network_Incident__c, Access_Seeker_Name__c, AVC_Id__c, Fault_Type__c, Channel_Reported__c, Segment__c, Incident_Type__c, Access_Seeker_ID__c, Access_Seeker__c, locId__c, WOR_ID__c, PRI_ID__c, Op_Cat__c, Prod_Cat__c, Technology__c, Incident_Number__c, SLA_Type__c, SLA_Response_Date_Time__c, Target_Commitment_Date__c, Closed_Date__c, Reported_Date__c, Industry_Sub_Status__c, Industry_Status__c, Status_Reason__c, Priority__c, Incident_Notes__c, RecordTypeId, Name, OwnerId, Id FROM Incident_Management__c where id=:incidentId];

        	  if(dataIs.Industry_Status__c=='Acknowledged' || 
                 dataIs.Industry_Status__c=='In Progress' ||
                 dataIs.Industry_Status__c=='In Progress Pending' || 
                 (dataIs.Industry_Status__c=='In Progress Pending' && dataIs.Incident_Status__c=='Assigned') || 
                 dataIs.Industry_Status__c=='Resolved'){
               show_notes_link = true;
            }else{
               show_notes_link = false;
            }
          
        return show_notes_link;
            
            }
}