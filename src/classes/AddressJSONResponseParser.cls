public class AddressJSONResponseParser {
    /* Classes to parse JSON response for Address API */
    
    // Level 1
    public class AddressRoot {
        public Link links { get; set; }
        public List<Data> data { get; set; }
        public Meta meta { get; set; }
    }
    
    // Level 2
    public class Link {
        public String self { get; set; }
        public String first { get; set; }
        public String last { get; set; }        
    }    
    
    // Level 2
    public class Data {
        public String type { get; set; }
        public String id { get; set; }
        public DataRelationships relationships { get; set; }
        public DataAttributes attributes { get; set; }
    }

    // Level 2
    public class Meta {
        public Integer totalPages { get; set; }
        public Integer totalMatchedResources { get; set; }
    }
    
    // Level 3
    public class DataRelationships {
        public LocatedAt locatedAt { get; set; }
    }

    // Level 3
    public class DataAttributes {
        public List<AttributesAlias> aliases { get; set; }
        public AttributesPrincipal principal { get; set; }
        public AttributesMeta meta { get; set; }
        public GeoPoint geopoint { get; set; }
    }
    
    // Level 4
    public class LocatedAt {
        public LocatedAtData data { get; set; }
    }

    // Level 4
    public class AttributesAlias {
        public String planNumber { get; set; }
        public String postCode { get; set; }
        public String locality { get; set; }
        public String roadName { get; set; }
        public String roadTypeCode { get; set; }
        public String state { get; set; }
        public String fullText { get; set; }
        public String unitType { get; set; }
        public String roadNumber1 { get; set; }
        public String unitNumber { get; set; }
        public String lotNumber { get; set; }
        public String country { get; set; }
    }
    
    // Level 4
    public class AttributesPrincipal {
        public String planNumber { get; set; }
        public String postCode { get; set; }
        public String locality { get; set; }
        public String roadName { get; set; }
        public String roadTypeCode { get; set; }
        public String state { get; set; }
        public String fullText { get; set; }
        public String unitType { get; set; }
        public String roadNumber1 { get; set; }
        public String unitNumber { get; set; }
        public String lotNumber { get; set; }
        public String country { get; set; }
    }
    
    // Level 4
    public class AttributesMeta {
        public String relevanceScore { get; set; }
    }
    
    // Level 5
    public class LocatedAtData {
        public String id { get; set; }
        public String type { get; set; }
    }

    public class GeoPoint {
        public String latitude { get; set; }
        public String longitude { get; set; }
    }    

}