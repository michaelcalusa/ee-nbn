/***************************************************************************************************
Class Name:  AddressSearch_CX_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : This class contains unit test scenarios for AddressSearch_CX apex class
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       26/10/2015                 Created
****************************************************************************************************/
@isTest
private class AddressSearch_CX_Test {
    static integer statusCode = 200;
    static Map<String, String> responseHeaders = new Map<String, String> ();
    static String json = '{'+
        '   \"data\":['+
        '      {'+
        '         \"type\":\"location\",'+
        '         \"id\":\"LOC000006657271\",'+
        '         \"attributes\":{'+
        '            \"externalTerritory\":\"FALSE\",'+
        '            \"address\":{'+
        '               \"unstructured\":\"LOT 401 411 CHATSWOOD RD SHAILER PARK QLD 4128\",'+
        '               \"unitNumber\":\"100\",'+
        '               \"levelNumber\":\"100\",'+
        '               \"roadSuffixCode\":\"RD\",'+
        '               \"roadNumber1\":\"411\",'+
        '               \"roadName\":\"CHATSWOOD\",'+
        '               \"roadTypeCode\":\"RD\",'+
        '               \"locality\":\"SHAILER PARK\",'+
        '               \"postCode\":\"4128\",'+
        '               \"state\":\"QLD\",'+
        '               \"lotNumber\":\"401\",'+
        '               \"aliasAddresses\":{'+
        '                  \"unstructured\":\"\"'+
        '               }'+
        '            },'+
        '            \"geoCode\":{'+
        '               \"latitude\":\"-27.647015\",'+
        '               \"longitude\":\"153.179405\",'+
        '               \"geographicDatum\":\"GDA94\",'+
        '               \"srid\":\"4283\"'+
        '            }'+
        '         },'+
        '         \"relationships\":{'+
        '            \"containmentBoundaries\":{'+
        '               \"data\":['+
        '                  {'+
        '                     \"id\":\"4SLA\",'+
        '                     \"type\":\"NetworkBoundary\"'+
        '                  },'+
        '                  {'+
        '                     \"id\":\"4LNH\",'+
        '                     \"type\":\"NetworkBoundary\"'+
        '                  },'+
        '                  {'+
        '                     \"id\":\"S4 - Greater Brisbane\",'+
        '                     \"type\":\"NetworkBoundary\"'+
        '                  }'+
        '               ]'+
        '            }'+
        '         }'+
        '      },'+
        '      {'+
        '         \"type\":\"location\",'+
        '         \"id\":\"LOC000006657272\",'+
        '         \"attributes\":{'+
        '            \"externalTerritory\":\"FALSE\",'+
        '            \"address\":{'+
        '               \"unstructured\":\"LOT 401 411 CHATSWOOD RD SHAILER PARK QLD 4128\",'+
        '               \"unitNumber\":\"100\",'+
        '               \"levelNumber\":\"100\",'+
        '               \"roadSuffixCode\":\"RD\",'+
        '               \"roadNumber1\":\"411\",'+
        '               \"roadName\":\"CHATSWOOD\",'+
        '               \"roadTypeCode\":\"RD\",'+
        '               \"locality\":\"SHAILER PARK\",'+
        '               \"postCode\":\"4128\",'+
        '               \"state\":\"QLD\",'+
        '               \"lotNumber\":\"401\",'+
        '               \"aliasAddresses\":{'+
        '                  \"unstructured\":\"\"'+
        '               }'+
        '            },'+
        '            \"geoCode\":{'+
        '               \"latitude\":\"-27.647015\",'+
        '               \"longitude\":\"153.179405\",'+
        '               \"geographicDatum\":\"GDA94\",'+
        '               \"srid\":\"4283\"'+
        '            }'+
        '         },'+
        '         \"relationships\":{'+
        '            \"containmentBoundaries\":{'+
        '               \"data\":['+
        '                  {'+
        '                     \"id\":\"4SLA\",'+
        '                     \"type\":\"NetworkBoundary\"'+
        '                  },'+
        '                  {'+
        '                     \"id\":\"4LNH\",'+
        '                     \"type\":\"NetworkBoundary\"'+
        '                  },'+
        '                  {'+
        '                     \"id\":\"S4 - Greater Brisbane\",'+
        '                     \"type\":\"NetworkBoundary\"'+
        '                  }'+
        '               ]'+
        '            }'+
        '         }'+
        '      }'+
        '   ]'+
        '}';
    /***************************************************************************************************
    Method Name:  getAddress_Test
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void getAddress_Test(){
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        List<Site__c> UnverifiedSitesList = new List<Site__c>();
        integer numberOfRecords = 10;
        for(integer counter = 0 ; counter < numberOfRecords ; counter++){
            Site__c UnverifiedSite = new Site__c();
            UnverifiedSite.RecordTypeId = recordTypeId;
            UnverifiedSite.Site_Address__c = '100 CHATSWOOD, NSW 2150'+counter;
            UnverifiedSite.Name = '100 CHATSWOOD, NSW 2150'+counter;
            UnverifiedSite.Locality_Name__c = 'CHATSWOOD';
            UnverifiedSite.Road_Name__c = 'CHATSWOOD';
            UnverifiedSitesList.add(UnverifiedSite);
        }
        insert UnverifiedSitesList;
        Id [] fixedSearchResults= new Id[20];
        for(Site__c site:UnverifiedSitesList){
            fixedSearchResults.add(site.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest(); 
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,json,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        AddressSearch_CX.getAddress('CHATSWOOD','5','false');
        AddressSearch_CX.getAddress('CHATSWOOD','1','true');
        AddressSearch_CX.getAddress(null,'1','false');
        Test.stopTest();
    }
    /***************************************************************************************************
    Method Name:  getStructuredAddress_Test
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void getStructuredAddress_Test(){
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        List<Site__c> UnverifiedSitesList = new List<Site__c>();
        integer numberOfRecords = 10;
        for(integer counter = 0 ; counter < numberOfRecords ; counter++){
            Site__c UnverifiedSite = new Site__c();
            UnverifiedSite.RecordTypeId = recordTypeId;
            UnverifiedSite.Site_Address__c = '100 CHATSWOOD, NSW 2150'+counter;
            UnverifiedSite.Name = '100 CHATSWOOD, NSW 2150'+counter;
            UnverifiedSite.Unit_Number__c = '100';
            UnverifiedSite.Level_Number__c = '100';
            UnverifiedSite.Lot_Number__c = '401';
            UnverifiedSite.Road_Number_1__c = '411';
            UnverifiedSite.Road_Name__c = 'CHATSWOOD';
            UnverifiedSite.Road_Suffix_Code__c = 'RD';
            UnverifiedSite.Locality_Name__c = 'SHAILER PARK';
            UnverifiedSite.Post_Code__c = '4128';
            UnverifiedSite.State_Territory_Code__c = 'QLD';
            UnverifiedSitesList.add(UnverifiedSite);
        }
        insert UnverifiedSitesList;
        Id [] fixedSearchResults= new Id[20];
        for(Site__c site:UnverifiedSitesList){
            fixedSearchResults.add(site.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,json,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        string structuredAddressInput = '{"unitNumber":"100","levelNumber":"100","lotNumber":"401","roadNumber1":"411","roadName":"CHATSWOOD","roadSuffixCode":"RD","localityName":"SHAILER PARK","postCode":"4128","stateTerritoryCode":"QLD"}';
        AddressSearch_CX.getStructuredAddress(structuredAddressInput,'5');
        AddressSearch_CX.getStructuredAddress(structuredAddressInput,'1');
        AddressSearch_CX.getStructuredAddress(null,'1');
        structuredAddressInput = '{"unitNumber":"333","levelNumber":"100","lotNumber":"401","roadNumber1":"411","roadName":"CHATSWOOD","roadSuffixCode":"RD","localityName":"SHAILER PARK","postCode":"4128","stateTerritoryCode":"QLD"}';
        AddressSearch_CX.getStructuredAddress(structuredAddressInput,'5');
        structuredAddressInput = '{"unitNumber":"100","levelNumber":"333","lotNumber":"401","roadNumber1":"411","roadName":"CHATSWOOD","roadSuffixCode":"RD","localityName":"SHAILER PARK","postCode":"4128","stateTerritoryCode":"QLD"}';
        AddressSearch_CX.getStructuredAddress(structuredAddressInput,'5');
        structuredAddressInput = '{"unitNumber":"100","levelNumber":"100","lotNumber":"333","roadNumber1":"411","roadName":"CHATSWOOD","roadSuffixCode":"RD","localityName":"SHAILER PARK","postCode":"4128","stateTerritoryCode":"QLD"}';
        AddressSearch_CX.getStructuredAddress(structuredAddressInput,'5');
        structuredAddressInput = '{"unitNumber":"100","levelNumber":"100","lotNumber":"401","roadNumber1":"555","roadName":"CHATSWOOD","roadSuffixCode":"RD","localityName":"SHAILER PARK","postCode":"4128","stateTerritoryCode":"QLD"}';
        AddressSearch_CX.getStructuredAddress(structuredAddressInput,'5');
        structuredAddressInput = '{"unitNumber":"100","levelNumber":"100","lotNumber":"401","roadNumber1":"411","roadName":"MILLER","roadSuffixCode":"RD","localityName":"SHAILER PARK","postCode":"4128","stateTerritoryCode":"QLD"}';
        AddressSearch_CX.getStructuredAddress(structuredAddressInput,'5');
        structuredAddressInput = '{"unitNumber":"100","levelNumber":"100","lotNumber":"401","roadNumber1":"411","roadName":"CHATSWOOD","roadSuffixCode":"ABC","localityName":"SHAILER PARK","postCode":"4128","stateTerritoryCode":"QLD"}';
        AddressSearch_CX.getStructuredAddress(structuredAddressInput,'5');
        structuredAddressInput = '{"unitNumber":"100","levelNumber":"100","lotNumber":"401","roadNumber1":"411","roadName":"CHATSWOOD","roadSuffixCode":"RD","localityName":"TEST","postCode":"4128","stateTerritoryCode":"QLD"}';
        AddressSearch_CX.getStructuredAddress(structuredAddressInput,'5');
        structuredAddressInput = '{"unitNumber":"100","levelNumber":"100","lotNumber":"401","roadNumber1":"411","roadName":"CHATSWOOD","roadSuffixCode":"RD","localityName":"SHAILER PARK","postCode":"1234","stateTerritoryCode":"QLD"}';
        AddressSearch_CX.getStructuredAddress(structuredAddressInput,'5');
        structuredAddressInput = '{"unitNumber":"100","levelNumber":"100","lotNumber":"401","roadNumber1":"411","roadName":"CHATSWOOD","roadSuffixCode":"RD","localityName":"SHAILER PARK","postCode":"4128","stateTerritoryCode":"NSW"}';
        AddressSearch_CX.getStructuredAddress(structuredAddressInput,'5');
        Test.stopTest();
    }
    /***************************************************************************************************
    Method Name:  getQualificationDetailsfromExtSystem_Test
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void getQualificationDetailsfromExtSystem_Test(){
        Test.startTest();
        String jsonInput = '{'+
        '  \"data\": ['+
        '    {'+
        '      \"type\": \"location\",'+
        '      \"id\": \"LOC123456789012\",'+
        '      \"attributes\": {'+
        '        \"gnafId\": \"G8932387982982\",'+
        '        \"dwellingType\": \"MDU\",'+
        '        \"region\": \"Urban\",'+
        '        \"regionValueDefaulted\": false,'+
        '        \"externalTerritory\": false,'+
        '        \"landUse\": \"Commercial\",'+
        '        \"landSubUse\": \"Retail\",'+
        '        \"address\": {'+
        '          \"unstructured\": \"Building A 1-2 Park LANE N Parkview 12345 APT 1 F 1 15B-16 Trevenar ST N Ashbury NSW 2193\",'+
        '          \"siteName\": \"Parkview\",'+
        '          \"locationDescriptor\": \"Near the school\",'+
        '          \"levelType\": \"F\",'+
        '          \"levelNumber\": \"1\",'+
        '          \"unitType\": \"APT\",'+
        '          \"unitNumber\": \"1\",'+
        '          \"lotNumber\": \"12345\",'+
        '          \"planNumber\": \"N-78238\",'+
        '          \"roadNumber1\": \"15B\",'+
        '          \"roadNumber2\": \"16\",'+
        '          \"roadName\": \"Trevenar\",'+
        '          \"roadTypeCode\": \"ST\",'+
        '          \"roadSuffixCode\": \"N\",'+
        '          \"locality\": \"Ashbury\",'+
        '          \"postCode\": \"2193\",'+
        '          \"state\": \"NSW\",'+
        '          \"complexAddress\": {'+
        '            \"siteName\": \"Building A\",'+
        '            \"roadNumber1\": \"1\",'+
        '            \"roadNumber2\": \"2\",'+
        '            \"roadName\": \"Park\",'+
        '            \"roadTypeCode\": \"LANE\",'+
        '            \"roadSuffixCode\": \"N\"'+
        '          }'+
        '        },'+
        '        \"aliasAddresses\": ['+
        '          {'+
        '            \"unstructured\": \"Building A 1-2 Park LANE N Parkview 12345 APT 1 F 1 13A-16 Trevenar ST N Ashbury NSW 2193\",'+
        '            \"siteName\": \"Parkview\",'+
        '            \"locationDescriptor\": \"Near the school\",'+
        '            \"levelType\": \"F\",'+
        '            \"levelNumber\": \"1\",'+
        '            \"unitType\": \"APT\",'+
        '            \"unitNumber\": \"1\",'+
        '            \"lotNumber\": \"12345\",'+
        '            \"planNumber\": \"N-78238\",'+
        '            \"roadNumber1\": \"15\",'+
        '            \"roadNumber2\": \"16\",'+
        '            \"roadName\": \"Trevenar\",'+
        '            \"roadTypeCode\": \"ST\",'+
        '            \"roadSuffixCode\": \"N\",'+
        '            \"locality\": \"Ashbury\",'+
        '            \"postCode\": \"2193\",'+
        '            \"state\": \"NSW\",'+
        '            \"complexAddress\": {'+
        '              \"siteName\": \"Building A\",'+
        '              \"roadNumber1\": \"1\",'+
        '              \"roadNumber2\": \"2\",'+
        '              \"roadName\": \"Park\",'+
        '              \"roadTypeCode\": \"LANE\",'+
        '              \"roadSuffixCode\": \"N\"'+
        '            }'+
        '          }'+
        '        ],'+
        '        \"geocode\": {'+
        '          \"latitude\": \"37.8136\",'+
        '          \"longitude\": \"144.9631\",'+
        '          \"geographicDatum\": \"GDA94\",'+
        '          \"srid\": \"4283\"'+
        '        },'+
        '        \"primaryAccessTechnology\": {'+
        '          \"technologyType\": \"Fibre\",'+
        '          \"serviceClass\": 3,'+
        '          \"rolloutType\": \"Brownfield\"'+
        '        }'+
        '      },'+
        '      \"relationships\": {'+
        '        \"containmentBoundaries\": {'+
        '          \"data\": ['+
        '            {'+
        '              \"id\": \"ADA-001\",'+
        '              \"type\": \"networkBoundary\"'+
        '            },'+
        '            {'+
        '              \"id\": \"MPS-001\",'+
        '              \"type\": \"networkBoundary\"'+
        '            }'+
        '          ]'+
        '        },'+
        '        \"MDU\": {'+
        '          \"data\": {'+
        '            \"id\": \"LOC212345678901\",'+
        '            \"type\": \"location\"'+
        '          }'+
        '        }'+
        '      }'+
        '    }'+
        '  ],'+
        '  \"included\": ['+
        '    {'+
        '      \"type\": \"networkBoundary\",'+
        '      \"id\": \"ADA-001\",'+
        '      \"attributes\": {'+
        '        \"boundaryType\": \"ADA\",'+
        '        \"status\": \"INSERVICE\",'+
        '        \"technologyType\": \"Fibre\"'+
        '      }'+
        '    },'+
        '    {'+
        '      \"type\": \"networkBoundary\",'+
        '      \"id\": \"MPS-001\",'+
        '      \"attributes\": {'+
        '        \"boundaryType\": \"MPS\",'+
        '        \"status\": \"INSERVICE\",'+
        '        \"technologyType\": \"Fibre\"'+
        '      }'+
        '    }'+
        '  ]'+
        '}';
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,jsonInput,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        AddressSearch_CX.getQualificationDetailsfromExtSystem('LOC123456789012');
        AddressSearch_CX.ComplexAddress2 ComplexAddress2_Class = new AddressSearch_CX.ComplexAddress2 ();
        ComplexAddress2_Class.siteName = 'test';
        ComplexAddress2_Class.roadNumber1 = 'test';
        ComplexAddress2_Class.roadNumber2 = 'test';
        ComplexAddress2_Class.roadName = 'test';
        ComplexAddress2_Class.roadTypeCode = 'test';
        ComplexAddress2_Class.roadSuffixCode = 'test';
        AddressSearch_CX.AliasAddress AliasAddress_Class = new AddressSearch_CX.AliasAddress ();
        AliasAddress_Class.unstructured = 'test';
        AliasAddress_Class.siteName = 'test';
        AliasAddress_Class.locationDescriptor = 'test';
        AliasAddress_Class.levelType = 'test';
        AliasAddress_Class.levelNumber = 'test';
        AliasAddress_Class.unitType = 'test';
        AliasAddress_Class.unitNumber = 'test';
        AliasAddress_Class.lotNumber = 'test';
        AliasAddress_Class.planNumber = 'test';
        AliasAddress_Class.roadNumber1 = 'test';
        AliasAddress_Class.roadNumber2 = 'test';
        AliasAddress_Class.roadName = 'test';
        AliasAddress_Class.roadTypeCode = 'test';
        AliasAddress_Class.roadSuffixCode = 'test';
        AliasAddress_Class.locality = 'test';
        AliasAddress_Class.postCode = 'test';
        AliasAddress_Class.state = 'test';
        AddressSearch_CX.Source Source_Class = new AddressSearch_CX.Source ();
        Source_Class.parameter = 'test';
        AddressSearch_CX.Error Error_Class = new AddressSearch_CX.Error ();
        Error_Class.status = 'test';
        Error_Class.code = 'test';
        Error_Class.title = 'test';
        Error_Class.detail = 'test';
        AddressSearch_CX.ErrorRootObject ErrorRootObject_Class = new AddressSearch_CX.ErrorRootObject ();
        AddressSearch_CX.sWrapper sWrapper_Class = new AddressSearch_CX.sWrapper ();
        sWrapper_Class.errStatus = 'test';
        sWrapper_Class.code = 'test';
        sWrapper_Class.sourceParamater = 'test';
        sWrapper_Class.title = 'test';
        sWrapper_Class.detail = 'test';
        AddressSearch_CX.StructuredAddress StructuredAddress_Class = new AddressSearch_CX.StructuredAddress ();
        StructuredAddress_Class.AssetNumber = 'test';
        StructuredAddress_Class.AssetType = 'test';
        String jsonInput2 = '{'+
        '  \"data\": ['+
        '    {'+
        '      \"type\": \"location\",'+
        '      \"id\": \"LOC123456789012\",'+
        '      \"attributes\": {'+
        '        \"gnafId\": \"G8932387982982\",'+
        '        \"dwellingType\": \"SDU\",'+
        '        \"region\": \"Urban\",'+
        '        \"regionValueDefaulted\": false,'+
        '        \"externalTerritory\": false,'+
        '        \"landUse\": \"Commercial\",'+
        '        \"landSubUse\": \"Retail\",'+
        '        \"address\": {'+
        '          \"unstructured\": \"Building A 1-2 Park LANE N Parkview 12345 APT 1 F 1 15B-16 Trevenar ST N Ashbury NSW 2193\",'+
        '          \"siteName\": \"Parkview\",'+
        '          \"locationDescriptor\": \"Near the school\",'+
        '          \"levelType\": \"F\",'+
        '          \"levelNumber\": \"1\",'+
        '          \"unitType\": \"APT\",'+
        '          \"unitNumber\": \"1\",'+
        '          \"lotNumber\": \"12345\",'+
        '          \"planNumber\": \"N-78238\",'+
        '          \"roadNumber1\": \"15B\",'+
        '          \"roadNumber2\": \"16\",'+
        '          \"roadName\": \"Trevenar\",'+
        '          \"roadTypeCode\": \"ST\",'+
        '          \"roadSuffixCode\": \"N\",'+
        '          \"locality\": \"Ashbury\",'+
        '          \"postCode\": \"2193\",'+
        '          \"state\": \"NSW\",'+
        '          \"complexAddress\": {'+
        '            \"siteName\": \"Building A\",'+
        '            \"roadNumber1\": \"1\",'+
        '            \"roadNumber2\": \"2\",'+
        '            \"roadName\": \"Park\",'+
        '            \"roadTypeCode\": \"LANE\",'+
        '            \"roadSuffixCode\": \"N\"'+
        '          }'+
        '        },'+
        '        \"aliasAddresses\": ['+
        '          {'+
        '            \"unstructured\": \"Building A 1-2 Park LANE N Parkview 12345 APT 1 F 1 13A-16 Trevenar ST N Ashbury NSW 2193\",'+
        '            \"siteName\": \"Parkview\",'+
        '            \"locationDescriptor\": \"Near the school\",'+
        '            \"levelType\": \"F\",'+
        '            \"levelNumber\": \"1\",'+
        '            \"unitType\": \"APT\",'+
        '            \"unitNumber\": \"1\",'+
        '            \"lotNumber\": \"12345\",'+
        '            \"planNumber\": \"N-78238\",'+
        '            \"roadNumber1\": \"15\",'+
        '            \"roadNumber2\": \"16\",'+
        '            \"roadName\": \"Trevenar\",'+
        '            \"roadTypeCode\": \"ST\",'+
        '            \"roadSuffixCode\": \"N\",'+
        '            \"locality\": \"Ashbury\",'+
        '            \"postCode\": \"2193\",'+
        '            \"state\": \"NSW\",'+
        '            \"complexAddress\": {'+
        '              \"siteName\": \"Building A\",'+
        '              \"roadNumber1\": \"1\",'+
        '              \"roadNumber2\": \"2\",'+
        '              \"roadName\": \"Park\",'+
        '              \"roadTypeCode\": \"LANE\",'+
        '              \"roadSuffixCode\": \"N\"'+
        '            }'+
        '          }'+
        '        ],'+
        '        \"geocode\": {'+
        '          \"latitude\": \"37.8136\",'+
        '          \"longitude\": \"144.9631\",'+
        '          \"geographicDatum\": \"GDA94\",'+
        '          \"srid\": \"4283\"'+
        '        },'+
        '        \"primaryAccessTechnology\": {'+
        '          \"technologyType\": \"Fibre\",'+
        '          \"serviceClass\": 3,'+
        '          \"rolloutType\": \"Brownfield\"'+
        '        }'+
        '      },'+
        '      \"relationships\": {'+
        '        \"containmentBoundaries\": {'+
        '          \"data\": ['+
        '            {'+
        '              \"id\": \"ADA-001\",'+
        '              \"type\": \"networkBoundary\"'+
        '            },'+
        '            {'+
        '              \"id\": \"MPS-001\",'+
        '              \"type\": \"networkBoundary\"'+
        '            }'+
        '          ]'+
        '        },'+
        '        \"MDU\": {'+
        '          \"data\": {'+
        '            \"id\": \"LOC212345678901\",'+
        '            \"type\": \"location\"'+
        '          }'+
        '        }'+
        '      }'+
        '    }'+
        '  ],'+
        '  \"included\": ['+
        '    {'+
        '      \"type\": \"networkBoundary\",'+
        '      \"id\": \"ADA-001\",'+
        '      \"attributes\": {'+
        '        \"boundaryType\": \"ADA\",'+
        '        \"status\": \"INSERVICE\",'+
        '        \"technologyType\": \"Fibre\"'+
        '      }'+
        '    },'+
        '    {'+
        '      \"type\": \"networkBoundary\",'+
        '      \"id\": \"MPS-001\",'+
        '      \"attributes\": {'+
        '        \"boundaryType\": \"MPS\",'+
        '        \"status\": \"INSERVICE\",'+
        '        \"technologyType\": \"Fibre\"'+
        '      }'+
        '    }'+
        '  ]'+
        '}';
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse2 = new QueueCRMoD_Response_Test(statusCode,jsonInput2,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse2);
        AddressSearch_CX.getQualificationDetailsfromExtSystem('LOC123456789012');
        String jsonInput3 = '{'+
        '  \"data\": ['+
        '    {'+
        '      \"type\": \"location\",'+
        '      \"id\": \"LOC123456789012\",'+
        '      \"attributes\": {'+
        '        \"gnafId\": \"G8932387982982\",'+
        '        \"dwellingType\": \"ABC\",'+
        '        \"region\": \"Urban\",'+
        '        \"regionValueDefaulted\": false,'+
        '        \"externalTerritory\": false,'+
        '        \"landUse\": \"Commercial\",'+
        '        \"landSubUse\": \"Retail\",'+
        '        \"address\": {'+
        '          \"unstructured\": \"Building A 1-2 Park LANE N Parkview 12345 APT 1 F 1 15B-16 Trevenar ST N Ashbury NSW 2193\",'+
        '          \"siteName\": \"Parkview\",'+
        '          \"locationDescriptor\": \"Near the school\",'+
        '          \"levelType\": \"F\",'+
        '          \"levelNumber\": \"1\",'+
        '          \"unitType\": \"APT\",'+
        '          \"unitNumber\": \"1\",'+
        '          \"lotNumber\": \"12345\",'+
        '          \"planNumber\": \"N-78238\",'+
        '          \"roadNumber1\": \"15B\",'+
        '          \"roadNumber2\": \"16\",'+
        '          \"roadName\": \"Trevenar\",'+
        '          \"roadTypeCode\": \"ST\",'+
        '          \"roadSuffixCode\": \"N\",'+
        '          \"locality\": \"Ashbury\",'+
        '          \"postCode\": \"2193\",'+
        '          \"state\": \"NSW\",'+
        '          \"complexAddress\": {'+
        '            \"siteName\": \"Building A\",'+
        '            \"roadNumber1\": \"1\",'+
        '            \"roadNumber2\": \"2\",'+
        '            \"roadName\": \"Park\",'+
        '            \"roadTypeCode\": \"LANE\",'+
        '            \"roadSuffixCode\": \"N\"'+
        '          }'+
        '        },'+
        '        \"aliasAddresses\": ['+
        '          {'+
        '            \"unstructured\": \"Building A 1-2 Park LANE N Parkview 12345 APT 1 F 1 13A-16 Trevenar ST N Ashbury NSW 2193\",'+
        '            \"siteName\": \"Parkview\",'+
        '            \"locationDescriptor\": \"Near the school\",'+
        '            \"levelType\": \"F\",'+
        '            \"levelNumber\": \"1\",'+
        '            \"unitType\": \"APT\",'+
        '            \"unitNumber\": \"1\",'+
        '            \"lotNumber\": \"12345\",'+
        '            \"planNumber\": \"N-78238\",'+
        '            \"roadNumber1\": \"15\",'+
        '            \"roadNumber2\": \"16\",'+
        '            \"roadName\": \"Trevenar\",'+
        '            \"roadTypeCode\": \"ST\",'+
        '            \"roadSuffixCode\": \"N\",'+
        '            \"locality\": \"Ashbury\",'+
        '            \"postCode\": \"2193\",'+
        '            \"state\": \"NSW\",'+
        '            \"complexAddress\": {'+
        '              \"siteName\": \"Building A\",'+
        '              \"roadNumber1\": \"1\",'+
        '              \"roadNumber2\": \"2\",'+
        '              \"roadName\": \"Park\",'+
        '              \"roadTypeCode\": \"LANE\",'+
        '              \"roadSuffixCode\": \"N\"'+
        '            }'+
        '          }'+
        '        ],'+
        '        \"geocode\": {'+
        '          \"latitude\": \"37.8136\",'+
        '          \"longitude\": \"144.9631\",'+
        '          \"geographicDatum\": \"GDA94\",'+
        '          \"srid\": \"4283\"'+
        '        },'+
        '        \"primaryAccessTechnology\": {'+
        '          \"technologyType\": \"Fibre\",'+
        '          \"serviceClass\": 3,'+
        '          \"rolloutType\": \"Brownfield\"'+
        '        }'+
        '      },'+
        '      \"relationships\": {'+
        '        \"containmentBoundaries\": {'+
        '          \"data\": ['+
        '            {'+
        '              \"id\": \"ADA-001\",'+
        '              \"type\": \"networkBoundary\"'+
        '            },'+
        '            {'+
        '              \"id\": \"MPS-001\",'+
        '              \"type\": \"networkBoundary\"'+
        '            }'+
        '          ]'+
        '        },'+
        '        \"MDU\": {'+
        '          \"data\": {'+
        '            \"id\": \"LOC212345678901\",'+
        '            \"type\": \"location\"'+
        '          }'+
        '        }'+
        '      }'+
        '    }'+
        '  ],'+
        '  \"included\": ['+
        '    {'+
        '      \"type\": \"networkBoundary\",'+
        '      \"id\": \"ADA-001\",'+
        '      \"attributes\": {'+
        '        \"boundaryType\": \"ADA\",'+
        '        \"status\": \"INSERVICE\",'+
        '        \"technologyType\": \"Fibre\"'+
        '      }'+
        '    },'+
        '    {'+
        '      \"type\": \"networkBoundary\",'+
        '      \"id\": \"MPS-001\",'+
        '      \"attributes\": {'+
        '        \"boundaryType\": \"MPS\",'+
        '        \"status\": \"INSERVICE\",'+
        '        \"technologyType\": \"Fibre\"'+
        '      }'+
        '    }'+
        '  ]'+
        '}';
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse3 = new QueueCRMoD_Response_Test(statusCode,jsonInput3,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse3);
        AddressSearch_CX.getQualificationDetailsfromExtSystem('LOC123456789012');
        QueueCRMoD_Response_Test fakeResponse4 = new QueueCRMoD_Response_Test(400,jsonInput3,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse4);
        AddressSearch_CX.getQualificationDetailsfromExtSystem('LOC123456789012');
        QueueCRMoD_Response_Test fakeResponse5 = new QueueCRMoD_Response_Test(900,jsonInput3,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse5);
        AddressSearch_CX.getQualificationDetailsfromExtSystem('LOC123456789012');
    }
}