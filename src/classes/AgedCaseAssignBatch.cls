/*------------------------------------------------------------
Author:        Wayne Ni
Company:       Cognizant
Description:   For the case with ageold type, Apex batch job will requify its site info and reassign this case to the right ownership based on ]
               the technology or not, some magic bla bla

Test Class:    AgedCaseAssignBatch_test
Date-------------------Aurthor---------------------Description
8th-January-2018       Wayne Ni                    Created this Batch Apex

------------------------------------------------------------*/ 

global class AgedCaseAssignBatch implements Database.batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    
    public static List<Group> custConnQueue = [Select id,name from group where type = 'Queue' and name IN  ('Customer Connections Team','Right Second Time Team')];
    String CaseOwnerid;
    List<Id> caseIds = new List<Id>();
    string cctq;
    string rsttq;
	datetime dt = datetime.now()-1;
	String formattedDt = dt.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
    
    global AgedCaseAssignBatch(List<Id> Input){
        caseIds = Input;
        
        /*if(custConnQueue != null && custConnQueue.size() == 1){
            CaseOwnerid = custConnQueue[0].id;
        }*/
        for(group ccq : custConnQueue){
            if(ccq.name == 'Customer Connections Team')
                cctq = ccq.Id;
            else
                rsttq = ccq.Id;
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        //Order by Priority_assit_c flag so there is no need to add more steps to check priority 
        //Case with priority assit flag checked always be processed first  
        String FinalExecuteQuery;
        //system.debug('Query is '+testQuery);
        
        if(!(caseIds.isEmpty())){   
            system.debug('Non Scheduling');
            FinalExecuteQuery = 'SELECT Id,CaseNumber,Site__c,Loc_Id__c,Priority,Priority_Assist__c,Case_Assignment__c,OwnerId,Site_Technology_Type__c,ag_Business__c FROM Case WHERE Id IN: caseIds and Status != \'Closed\' ';
        }else if( caseIds.isEmpty()){
            system.debug('Scheduling');
            FinalExecuteQuery = 'SELECT Id,CaseNumber,Site__c,Loc_Id__c,Priority,Priority_Assist__c,Case_Assignment__c,OwnerId,Site_Technology_Type__c,ag_Business__c  FROM Case WHERE (ownerid IN (\''+ cctq +'\',\''+rsttq+'\') OR (AgedOrder_OwnerAssignment__c = TRUE AND CreatedDate > '+formattedDt+')) AND Status != \'Closed\' order by Age_of_Case__c Desc, Priority Asc';
        }
        
        system.debug('Final execute query is '+FinalExecuteQuery);
        return Database.getQueryLocator(FinalExecuteQuery);
    }
    
    
    global void execute(Database.BatchableContext BC, List<Case> caseScope){
        /* Overview: Get the list of case, the query the site_c info from the list of case, putting sites info into a new list
        Call the method from AutoSiteQualification apex class to verify each site for each case record
        After that, iterating through each case for the owner assignment flow  */
        system.debug('caseScope is'+caseScope);
        
        List<Id> sitesIds = new List<Id> ();
        
        //Iterating through the list of cases to get list of sites Id information
        system.debug('Iterating through case list to get site information');
        for(case c:caseScope){
            sitesIds.add(c.Site__c);            
        }       
        
        //Query the list of sites
        
        //Iterate through the site list to requalify each site
        system.debug('Reverify each site if not');
                
        //change it to query
        for(site__c s : [SELECT Id, Name,Serviceability_Class__c, Rollout_Type__c, Technology_Type__c,
                        Location_Id__c, Site_Address__c,Qualification_Date__c,RecordTypeId, Unit_Number__c,
                        Road_Name__c, Level_Number__c, Lot_Number__c, Road_Number_1__c, Road_Number_2__c, Road_Suffix_Code__c,Post_Code__c, 
                        Locality_Name__c, State_Territory_Code__c, Requalification_Failed_Date__c, CreatedDate FROM Site__c 
                        WHERE id IN: sitesIds]){
            system.debug('The site is '+s.Site_Address__c);
            SiteQualification_CX SiteQualification_CX_Con = new SiteQualification_CX(new ApexPages.StandardController(s));
            if(!Test.isRunningTest()){
            	SiteQualification_CX_Con.reQualifySiteRecord();
            }
        }
        
        //after requalify each site, iterate through each case for the ownership assignment flow
        system.debug('Initiate flow for each case'); 
        system.debug('@@@@@@@@@@@@@@@@@'+caseScope);
        //Updated by Shubham for ACT-14786
        //Updated by Shubham for ACT-15379: Added Order_Status__c field in below query
        for(case c: [SELECT Id,CaseNumber,Site__c,Loc_Id__c,CC_Team__c,Priority,Priority_Assist__c,Case_Assignment__c,OwnerId,
                     ag_Business__c,Resolver_Group__c,Site_Technology_Type__c,Site_State__c, Initial_SDP__c, Order_Age__c, State__c, CC_Order__c, Order_Status__c FROM Case WHERE Id IN:caseScope and  Status != 'Closed']){
            if(c.CC_Team__c == 'Customer Connections Team'){
                Map<String, Object> flowParams = new Map<String,Object>();   
                System.debug('Case Record before Flow Call'+c);
                flowParams.put('CaseRecord',c);
                Flow.Interview.CaseAutoAssignment AgedCaseFlow = new Flow.Interview.CaseAutoAssignment(flowParams);
                AgedCaseFlow.start();        
            }
            //Added by Shubham for ACT-14786 #Start
            else if(c.CC_Team__c == 'Right Second Time Team'){
            	system.debug('Check Order Age Value>'+c.Order_Age__c);
            	if(c.Resolver_Group__c == null)
            		c.Resolver_Group__c = '';
                if(c.Initial_SDP__c == null)
                    c.Initial_SDP__c = '~';	//Added ~ to skip auto-assignment if this field is blank
                Map<String, Object> flowParams = new Map<String,Object>();   
                System.debug('Case Record before R2T Flow Call'+c);
                flowParams.put('CaseRecord',c);
                Flow.Interview.CaseAutoAssignmentR2T AgedCaseFlow = new Flow.Interview.CaseAutoAssignmentR2T(flowParams);
                AgedCaseFlow.start();
            } 
            //Added by Shubham for ACT-14786 #End
        }
    }
    
    global void finish(Database.BatchableContext BC){} 
}