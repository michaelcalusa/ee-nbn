/*------------------------------------------------------------
Author:        Wayne Ni
Company:       Cognizant
Description:   Test Class for the apex batch: AgedCaseAssignBatch
Histroy:
Date-------------------Aurthotr---------------------Description
8th-January-2018       Wayne Ni                    Created this test class

------------------------------------------------------------*/ 

@isTest
private class AgedCaseAssignBatch_test {
    @isTest static void CaseOnwerShipAssignTest(){
        Id OldOwnerId;
        Id agedOrderRecTypID = Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId();
        List<Id> CaseIdList = new List<id>();
        system.debug('current running user before testing user is '+UserInfo.getUserId());
        
        //Create test users Setup test data  Create a unique UserName
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='NBN Customer Connections Team Leader'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8',FirstName='Customer Connections',LastName='User', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName);
        
        System.runAs(u) {
            // The following code runs as user 'u'
            System.debug('Current User Id: ' + UserInfo.getUserId ());
            System.debug('Current Profile: ' + UserInfo.getProfileId());
            
            //created some test Cases
            List<Case> TestCaseList = new List<Case>();
            
            for(integer i=0;i<1;i++){
                Case a = new Case(Loc_Id__c='LOC000001596238', Status ='New',recordTypeID = agedOrderRecTypID,OwnerId = u.Id);
                TestCaseList.add(a);            
            }
            
            Test.StartTest();
            insert TestCaseList;
            OldOwnerId = TestCaseList[0].OwnerId;
            for(case a:TestCaseList){
                system.debug('the case owner is '+a.OwnerId);
                CaseIdList.add(Id.valueOf(a.Id));
            }
            
           
            system.debug('Before scheduling the aged case batch');
            //ID batchprocessid =  Database.executeBatch(new AgedCaseAssignBatch(CaseIdList),Integer.valueOf(Label.AgedCaseBatchSize));
            //system.debug('The batch process Id is '+batchprocessid);
            Test.StopTest();

            
            for(case b:[SELECT Id,Site__c,Priority,Priority_Assist__c,Case_Assignment__c,OwnerId,Site_Technology_Type__c,ag_Business__c FROM Case WHERE Id IN: CaseIdList]){
                system.debug('the case owner after batch is '+b.OwnerId);
                system.assertNotEquals(b.OwnerId,OldOwnerId,'Owner Id is still the same (test user), flow is not working as intended');
            }                                   
        }        
        
    }
    
    @isTest static void SchedulerTest(){
        Test.startTest();
        String scheduleTime = '0 0 * * * ?';
        String jobId = System.schedule('AgedCaseAssignScheduleBatch Test Class Schedule', scheduleTime ,new AgedCaseAssignScheduleBatch());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(scheduleTime, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        Test.stopTest();
    }
}