/***************************************************************************************************
Class Name:         AgedCaseAssignScheduleBatch 
Class Type:         Schedule Class 
Version:            1.0 
Created Date:       3 Janauary 2018
Function:           This Schedule class is to invoke a batch class to re-assign aged order case 

Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Wayne Ni      3/1/2018      Created - Version 1.0 Refer MSEU-8031
****************************************************************************************************/ 

public class AgedCaseAssignScheduleBatch implements schedulable{
    
    public static void execute(SchedulableContext sc){ 
        
        List<Id> emptyCaseIdList = new List<id>();
        
        system.debug('Before scheduling the aged case batch');
        ID batchprocessid =  Database.executeBatch(new AgedCaseAssignBatch(emptyCaseIdList),Integer.valueOf(Label.AgedCaseBatchSize));
        system.debug('the batch job Id is '+batchprocessid);
    }
    
    /*Run the apex schedule batch every 15 minutes
    String scheduleTime = '0 15 0 * * ?';
    System.schedule('AgedCaseAssignScheduleBatch', scheduleTime ,new AgedCaseAssignScheduleBatch());*/ 
}