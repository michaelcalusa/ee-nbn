/*
Class Description
Creator: v-dileepathley
Purpose: This class will be used to Transform the Aged Order Data from Case Stage to Aged Work order table
Modifications:

*/
public class AgedWOTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
	public String awoQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {
        
    }
    public AgedWOTransformation(String ejId)
    {
         exJobId = ejId;
         Batch_Job__c bj = new Batch_Job__c();
         bj.Name = 'AgedWorkOrder_Transform'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        //Query the records from Case Stage which has Transformation Status as New.
            awoQuery = 'SELECT Id, Name, CreatedDate, CreatedById, LastModifiedById, NBNREFERENCEID__c, WONUM__c, Appointment_Date__c, NBNAPPOINTMENTID__c, NBNWORKFORCE__c, NBN_Requirement__c, Status__c, Status_Date__c, Description__c, NBNREASONCODE__c, Transformation_Status__c, Schedule_Job__c FROM Aged_Orders_Case_Stage__c Where Transformation_Status__c IN (\'Created Case Record\',\'Association Error\') order by createddate asc';
            return Database.getQueryLocator(awoQuery);   
    }
    
    public void execute(Database.BatchableContext BC,List<Aged_Orders_Case_Stage__c> awoIlist)
    {
        try
        {
            //Setting the allow field truncation to true, this will truncate the length if it exceeds the length of the column.
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.allowFieldTruncation = true;
            
            //Variable declaration
            List<Aged_Work_Order__c> awoList = new List<Aged_Work_Order__c>();
            list <string> caseId = new list<string>();
            string oId;
            map<string,Id> caseMap = new map<string,Id>();
            map <String, Aged_Work_Order__c> awoMap = new map<String, Aged_Work_Order__c>();
            list<Database.Error> err;
            String msg, fAffected;
            String[]fAff;
            StatusCode sCode;
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            
            for (Aged_Orders_Case_Stage__c c: awoIlist)
            {
            	if(!String.isBlank(c.NBNREFERENCEID__c))
                {
                	oId = c.NBNREFERENCEID__c;
                	caseId.add(oId); 
                }
            }
            
            //Creating the map of Order and Salesforce Id of case, which will help in associating the Work Order record to case.
            for( Case c: [Select Order_ID__c,Id from Case where RecordType.DeveloperName = 'Aged_Order' and Order_ID__c in :caseId FOR Update])
            {
                caseMap.put(c.Order_ID__c, c.Id); 
            }
            
            for (Aged_Orders_Case_Stage__c awoStage: awoIlist)
            {
                Aged_Work_Order__c awo = new Aged_Work_Order__c ();
                
                awo.Appointment_Date__c = awoStage.Appointment_Date__c;
                awo.Appointment_Id__c = awoStage.NBNAPPOINTMENTID__c;
                awo.Child_WOR__c = awoStage.WONUM__c;
                //awo.CreatedById = awoStage.CreatedById;
                //awo.CreatedDate = awoStage.CreatedDate;
                awo.Delivery_Partner__c = awoStage.NBNWORKFORCE__c;
                awo.NBN_Requirement__c = awoStage.NBN_Requirement__c;
                awo.Status__c = awoStage.Status__c;
                awo.Status_Date__c = awoStage.Status_Date__c;
                awo.Description__c = awoStage.Description__c;
                awo.Reason_Code__c = awoStage.NBNREASONCODE__c;
                if(!String.isBlank(awoStage.NBNREFERENCEID__c))
                    {
                        awo.Case__c = caseMap.get(awoStage.NBNREFERENCEID__c);
                    }
                awoMap.put(awoStage.WONUM__c, awo);
            }
            
            awoList = awoMap.values();
            Schema.SObjectField wor = Aged_Work_Order__c.Fields.Child_WOR__c;
            //Upserting the records
            List<Database.upsertResult> uResults = Database.upsert(awoList,wor,false);
            //traversing through the results and modifying the staging table
            for(Integer idx = 0; idx < uResults.size(); idx++)
            {   
                if(!uResults[idx].isSuccess())
                {
                    err=uResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='AgedCase_Transformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),
                    isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=awoIlist[idx].Id,Schedule_Job__c=bjId));
                    awoIList[idx].Transformation_Status__c = 'Error';
                    awoIList[idx].Schedule_Job__c = bjId;
                }
                else
                {
                        if(String.isBlank(awoIList[idx].NBNREFERENCEID__c) || String.isNotBlank(caseMap.get(awoIList[idx].NBNREFERENCEID__c)))
                        {
                            awoIList[idx].Transformation_Status__c = 'Copied to Base Tables';
                        }
                        else 
                        {
                            awoIList[idx].Transformation_Status__c = 'Association Error';
                            errList.add(new Error_Logging__c(Name='AgedCase_Transformation',Error_Message__c='Case is not associated to Aged Work Order',Fields_Afftected__c='Case',isCreated__c=uResults[idx].isCreated(),
                            isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=awoIList[idx].Id,Schedule_Job__c=bjId));
                        }
                    	awoIList[idx].Schedule_Job__c = bjId;
                }
                    
              }
            
            
            Insert errList;
            update awoIList;
            if(errList.isEmpty())      
            {
                status = 'Completed';
            } 
            else
            {
                status = 'Error';
            }  
            recordCount = recordCount + awoIlist.size();   
            
        }
        
        catch (Exception e)
        {
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='AgedWO_Transformation',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }
    
   public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        
        AsyncApexJob conJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
        {
            bj.Status__c= conJob.Status;
        }
        else
        {
             bj.Status__c=status;
        }
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = conJob.id;
        bj.End_Time__c  = conJob.CompletedDate;
        upsert bj;
     }
}