/***************************************************************************************************
Class Name:  AgedWOTransformation_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 22-09-2017
Function    : This test class contains unit test scenarios for AgedWOTransformation schedule class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Dilip Athley      22-09-2017                Created
****************************************************************************************************/
@isTest
public class AgedWOTransformation_Test
{
    static void getRecords (){  
        // Custom Setting record creation
        AO_Transformation_Job__c AOTransformation = new AO_Transformation_Job__c ();
        AOTransformation.Name = 'AOTransformation';
        AOTransformation.on_off__c = true;
        insert AOTransformation;
        
        //system.assertequals(accRec,null);
        // Create Aged_Orders_Case_Stage__c record
        List<Aged_Orders_Case_Stage__c> listOfAO = new List<Aged_Orders_Case_Stage__c>();
        Aged_Orders_Case_Stage__c aoRec = new Aged_Orders_Case_Stage__c();
        aoRec.NBNREFERENCEID__c = 'ORD000001';
        aoRec.Order_Status__c = 'ACKNOWLEDGED';
        aoRec.CSA_Id__c = 'TestCSA';
        aoRec.NBNCATEGORY__c = 'Test Category';
        aoRec.Transformation_Status__c = 'New';
        aoRec.Access_Seeker_Reference_Id__c = 'ASC00001';
        aoRec.Commodity__c = 'FTTN';
        aoRec.Priority_Assist__c = true;
        aoRec.BBU__c = 'BBU';
        aoRec.Copper_Pair_ID__c = 'testcopper';
        aoRec.Copper_Pair_Service_Class__c = '12345';
        aoRec.Location_Id__c = 'LOC123456789012';
        aoRec.Access_Seeker_ID__c = 'ASI000000151588';
        aoRec.Appointment_Date__c = System.now();
        aoRec.NBNAPPOINTMENTID__c = 'APT111111';
        aoRec.WoNUM__c = 'WOR1111112';
        aoRec.NBNWORKFORCE__c = 'DP19';
        aoRec.NBN_Requirement__c = 'testrequirement';
        aoRec.Status__c = 'ACKNOLEDGED';
        aoRec.Status_Date__c = System.now();
        aoRec.Description__c = 'TestDescription';
        aoRec.NBNREASONCODE__c = 'TEST';
        listOfAO.add(aoRec);
        Aged_Orders_Case_Stage__c aoRec1 = new Aged_Orders_Case_Stage__c();
        aoRec1.NBNREFERENCEID__c = 'ORD000002';
        aoRec1.Order_Status__c = 'ACKNOWLEDGED';
        aoRec1.CSA_Id__c = 'TestCSA';
        aoRec1.NBNCATEGORY__c = 'Test Category';
        aoRec1.Transformation_Status__c = 'New';
        aoRec1.Access_Seeker_Reference_Id__c = 'ASC000012';
        aoRec1.Commodity__c = 'FTTN';
        aoRec1.Priority_Assist__c = true;
        aoRec1.BBU__c = 'BBU';
        aoRec1.Copper_Pair_ID__c = 'testcopper1';
        aoRec1.Copper_Pair_Service_Class__c = '';
        aoRec1.Location_Id__c = 'LOC000125223911';
        aoRec1.Access_Seeker_ID__c = 'ASI00000022';
        aoRec1.Appointment_Date__c = System.now();
        aoRec1.NBNAPPOINTMENTID__c = 'APT111111';
        aoRec1.WoNUM__c = '';
        aoRec1.NBNWORKFORCE__c = 'DP19';
        aoRec1.NBN_Requirement__c = 'testrequirement';
        aoRec1.Status__c = 'ACKNOLEDGED';
        aoRec1.Status_Date__c = System.now();
        aoRec1.Description__c = 'TestDescription';
        aoRec1.NBNREASONCODE__c = 'TEST';
        listOfAO.add(aoRec1);
        insert listOfAO;
        

    }
    static testMethod void AOCaseTransformationTest1(){
        getRecords();
        Id accRecTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        // Create Account record
        Account accRec = new Account();
        accRec.name = 'Test Account';
        accRec.on_demand_id__c = 'Account1';
        accRec.Access_Seeker_ID__c = 'ASI000000151588';
        accRec.RecordTypeId = accRecTypeID;
        insert accRec;
        Id uVRecTypeID = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId();
        Site__c newSite = new Site__c(Name = 'LOC000125223911', MDU_CP_On_Demand_Id__c = 'LOC000125223911',Location_Id__c = 'LOC000125223911', Site_Name__c = 'LOC000125223911', Site_Address__c ='LOC000125223911', RecordTypeId =  uVRecTypeID);
        insert newSite;
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('AOTransformationScheduleClass_Test',CRON_EXP, new AOTransformation());
        Test.stopTest();
    }

}