/***************************************************************************************************
Class Name:         AltConDelOpenInteraction_schedulebatch
Class Type:         Schedule Class 
Version:            1.0 
Created Date:       3rd November 2017
Function:           This Schedule class is scheduled to run every 2 hours for Deleting Alternate Continuity interactions which are in Open Status
Input Parameters:   None 
Output Parameters:  None
Description:        Deleting Alternate Continuity interactions which are in Open Status
Modification Log:
* Developer         Date             Description
* --------------------------------------------------------------------------------------------------                 
* Narasimha Binkam      03/07/2017      Created - Version 1.0 Refer CUSTSA-2564for Epic description
****************************************************************************************************/ 

public class AltConDelOpenInteraction_schedulebatch implements schedulable
{
       public static void execute(SchedulableContext sc){
          AltConDeleteOpenInteractions_batch batchGCCDeletionObj = new AltConDeleteOpenInteractions_batch ();
          ID batchprocessid = Database.executeBatch(batchGCCDeletionObj);  
       }
       
       public static void start() {
         //every 2 hours
         String scheduleTime = '0 0 0/2 * * ?';
         System.schedule('GccDeletion', scheduleTime ,new AltConDelOpenInteraction_schedulebatch());
       }
   
    
}