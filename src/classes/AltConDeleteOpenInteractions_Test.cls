/***************************************************************************************************
Class Name:         AltConDeleteOpenInteractions_Test
Class Type:         Test Class 
Version:            1.0 
Created Date:       3rd November 2017
Function:           This Test class is for  batch class used for Deleting Alternate Continuity interactions which are in Open Status
Input Parameters:   None 
Output Parameters:  None
Description:        Deleting Alternate Continuity interactions which are in Open Status
Modification Log:
* Developer         Date             Description
* --------------------------------------------------------------------------------------------------                 
* Narasimha Binkam      03/07/2017      Created - Version 1.0 Refer CUSTSA-2564for Epic description
****************************************************************************************************/ 

@isTest
public class AltConDeleteOpenInteractions_Test {
    static testMethod void batchApexTest() {
        
        Database.BatchableContext BC;
        AltConDeleteOpenInteractions_batch batchGCCDeletionObj = new AltConDeleteOpenInteractions_batch();
        Database.QueryLocator QL = batchGCCDeletionObj.start(bc);
        Database.QueryLocatorIterator QIT =  QL.iterator();
        List<Task> taskList = new List<Task>();
        while (QIT.hasNext()) {
            Task taskRecord = (Task)QIT.next();
            //System.debug(Acc);
            taskList.add(taskRecord);
        }
        batchGCCDeletionObj.execute(BC, taskList);
        batchGCCDeletionObj.finish(BC);
        }

    static testMethod void scheduleGCCDeletionTest() {
        Test.StartTest();
        AltConDelOpenInteraction_schedulebatch.start();
        AltConDelOpenInteraction_schedulebatch scheduleGCCDeletionObj = new  AltConDelOpenInteraction_schedulebatch();
        String sch = '0 0 0/2 * * ?';
        system.schedule('GCCDeletionTest', sch, scheduleGCCDeletionObj );
        Test.stopTest();
    }
}