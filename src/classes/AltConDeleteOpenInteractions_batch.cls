/***************************************************************************************************
Class Name:         AltConDeleteOpenInteractions_batch
Class Type:         Batch Class 
Version:            1.0 
Created Date:       3rd November 2017
Function:           This Batch class is to invoke a Schedule class for Deleting Alternate Continuity interactions which are in Open Status
Input Parameters:   None 
Output Parameters:  None
Description:        Deleting Alternate Continuity interactions which are in Open Status
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Narasimha Binkam      03/07/2017      Created - Version 1.0 Refer CUSTSA-2564for Epic description
****************************************************************************************************/ 

global class AltConDeleteOpenInteractions_batch implements Database.Batchable<sObject>
{   
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        Datetime nowDateTime = System.Now();
        DateTime earlier = nowDateTime.addMinutes(-30);
        //Id devRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('GCC Interaction').getRecordTypeId();
     Id devRecordTypeId = GlobalCache.getRecordTypeId('Task','GCC Interaction');   
    return Database.getQueryLocator([SELECT id FROM task WHERE Status ='open' and recordtypeId =: devRecordTypeId and createddate <= :earlier]);
    } 
    
    global void execute(Database.BatchableContext BC, list<sObject> scope)
    {     
        delete scope;   
        //DataBase.emptyRecycleBin(scope); 
    }
    
    
    global void finish(Database.BatchableContext BC) 
    {                 
       
    }
}