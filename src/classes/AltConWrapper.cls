/***************************************************************************************************
Class Name:         AltConWrapper
Class Type:         WrapperClass 
Company :           Appirio
Version:            1.0 
Created Date:       17 May 2018
Function:           
Input Parameters:   None 
Output Parameters:  None
Description:        Wrapper class to hold deserialized JSON value
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Sunil      17/05/2018      Created - Version 1.0 
****************************************************************************************************/ 
@isTest 
public class AltConWrapper{
    public static testMethod void testController(){
        	AltConWrapper objWrapper = new AltConWrapper();
        	AltConWrapper.NetWorkTrialResp obj1 = new AltConWrapper.NetWorkTrialResp();
        	AltConWrapper.InboundJSONMessage obj2 = new AltConWrapper.InboundJSONMessage();
            AltConWrapper.Metadata  obj3 = new AltConWrapper.Metadata();
            AltConWrapper.fttbSite obj4 = new AltConWrapper.fttbSite();
            AltConWrapper.fttnNode obj5 = new AltConWrapper.fttnNode();
            AltConWrapper.copperMainCable obj6 = new AltConWrapper.copperMainCable();
            AltConWrapper.copperLeadinCable obj7 = new AltConWrapper.copperLeadinCable();
            AltConWrapper.priWrapper obj8 = new AltConWrapper.priWrapper();
            AltConWrapper.RootProdObject obj9 = new AltConWrapper.RootProdObject();
            AltConWrapper.PRIData obj11 = new AltConWrapper.PRIData();
            AltConWrapper.PRIRelationships obj12 = new AltConWrapper.PRIRelationships();
            AltConWrapper.PRIAddress obj123 = new AltConWrapper.PRIAddress();
            AltConWrapper.PRIRelationsShipData obj1111 = new AltConWrapper.PRIRelationsShipData();
            AltConWrapper.PRIIncluded obj454 = new AltConWrapper.PRIIncluded();
            AltConWrapper.IncludedPAttributes obj453 = new AltConWrapper.IncludedPAttributes();
            AltConWrapper.IncludedAddress obj44 = new AltConWrapper.IncludedAddress();
            AltConWrapper.PriAccessTech obj33 = new AltConWrapper.PriAccessTech();
            AltConWrapper.PRIAttributes obj22 = new AltConWrapper.PRIAttributes();
            AltConWrapper.copperDistributionCables obj99 = new AltConWrapper.copperDistributionCables();
            AltConWrapper.mdf objT = new AltConWrapper.mdf();
            AltConWrapper.tco objTT = new AltConWrapper.tco();
            AltConWrapper.pillars objTTT = new AltConWrapper.pillars();
            AltConWrapper.metadataFTT objTTTT = new AltConWrapper.metadataFTT();
            AltConWrapper.terminationModules objTTTTT = new AltConWrapper.terminationModules();

    }
    
    
    public class NetWorkTrialResp{
        public list<copperDistributionCables> copperDistributionCables{get;set;}
        public mdf mdf{get;set;}
        public copperLeadinCable copperLeadinCable{get;set;}
        public tco tco{get;set;}
        public list<pillars> pillars{get;set;}
        public metadataFTT metadata{get;set;}
        public fttnNode fttnNode{get;set;}
        public fttbSite fttbSite{get;set;}
        public String status{get;set;}
        public copperMainCable copperMainCable{get;set;}
        public String transactionId{get;set;}
        public String OPair;
        //created for multipillar Mapping
        public String OPair1;
        public String OPair2;
        public String OPair3;
        public String OPair4;
        public String MPair;
        //created for multipillar Mapping
        public String MPair1;
        public String MPair2;
        public String MPair3;
        public String MPair4;
        public TerminationModules ciu {get;set;} 
        public Dpu dpu {get;set;} 
        public PNIExc exception_x;
    }
    
    public class InboundJSONMessage{
        public String transactionId;
        public Metadata metadata;
        public String copperPathId;
        public String status;
        public String pairConnected;
        public PNIExc exception_x;
    }
    public class Metadata {
        public String nbnCorrelationId;
        public String tokenId;
        public String copperPathId;
        public String changeReason;
        public String requestedPair;
    }
    
    public class fttbSite{
       public String id;
       public String type;
       public String subtype;
       public String status;
       public String platformReferenceId;
       public String locationLatitude;
       public String locationLongitude;
       public List<mdfs> mdfs;
   }  
    public class fttnNode{
        public String id{get;set;}
        public String status{get;set;}
        public String subtype {get;set;}
        public String type{get;set;}
        public String platformReferenceId{get;set;}
        public String name{get;set;}
    }
    public class copperMainCable{ 
        public String id{get;set;}
        public String lengthType{get;set;}
        public String length{get;set;}
        public String twistedPairId{get;set;}
        public String platformReferenceId{get;set;}
        public String twistedPairName{get;set;}
        public String status{get;set;}
        public String twistedPairPlatformReferenceId{get;set;}
        public String subtype{get;set;}
        public String twistedPairStatus{get;set;}
        public String type{get;set;}
        public String twistedPairInstallationStatus{get;set;}
        public String name{get;set;}
    }
    public class copperLeadinCable{
        public String twistedPairPlatformReferenceId{get;set;}
        public String length{get;set;}
        public String lengthType{get;set;}
        public String platformReferenceId{get;set;}
        public String installationType{get;set;}
        public String status{get;set;}
        public String twistedPairId{get;set;}
        public String subtype{get;set;}
        public String twistedPairName{get;set;}
        public String type{get;set;}
        public String twistedPairStatus{get;set;}
        public String name{get;set;}
        public String twistedPairInstallationStatus{get;set;}
        public String id{get;set;}
    }
    public class priWrapper{
        public string priID;
        public string copperPathId;
        public string locId;
        public string address;
        public string type;
        public string techType;
    }
    
    public class RootProdObject{
        public List<PRIData> data ;
        public List<PRIIncluded> included ;
    }
    
    public class PRIData{
        public string type ;
        public string id ;
        public PRIAttributes attributes;
        public PRIRelationships relationships;
    }
    public class PRIRelationships{
        public PRIAddress address;
    }
    
    public class PRIAddress{
        public PRIRelationsShipData data;
    }
    public class PRIRelationsShipData{
        public string id;
    }
    public class PRIIncluded{
        public IncludedPAttributes attributes ;
        public string id;
    }
    
    public class IncludedPAttributes{
        public IncludedAddress address ;
        public string dwellingType;
        public string  landUse;
        public string region;
        public string orderStatus;
        public PriAccessTech PrimaryAccessTechnology{get; set;}
    }
    public class IncludedAddress{
        public string unstructured;
        public string state;
    }
    public class PriAccessTech{
        public String technologyType{get; set;}
        public String serviceClass {get; set;}
        public String rolloutType{get; set;}
    }
    
    public class PRIAttributes{
        public string orderCompletionDate;
        public string copperPathId;
        public string locationId;
        public string address;
        public string region;
        public string dwellingType;
        public string landUse;
        public string PrimaryAccessTechnology;
        public string priorityAssist;
        public string productStatus;
    }
    
    public class copperDistributionCables{
        public String subtype{get;set;}
        public String lengthType{get;set;}
        public String twistedPairCapacity{get;set;}
        public String length{get;set;}
        public String twistedPairId{get;set;}
        public String platformReferenceId{get;set;}
        public String twistedPairName{get;set;}
        public String status{get;set;}
        public String twistedPairPlatformReferenceId{get;set;}
        public String type{get;set;}
        public String twistedPairStatus{get;set;}
        public String name{get;set;}
        public String twistedPairInstallationStatus{get;set;}
        public String id{get;set;}
    }
    public class mdf{
        public String locationId{get;set;}
        public String locationLatitude{get;set;}
        public String locationLongitude{get;set;}
        public String platformReferenceId{get;set;}
        public String portId{get;set;}
        public String status{get;set;}
        public String portName{get;set;}
        public String subtype{get;set;}
        public String portPlatformReferenceId{get;set;}
        public String type{get;set;}
        public String portStatus{get;set;}
        public String name{get;set;}
        public String portInstallationStatus{get;set;}
        public String id{get;set;}
    }
    public class tco{
        public String locationLongitude{get;set;}
        public String status{get;set;}
        public String subtype{get;set;}
        public String platformReferenceId{get;set;}
        public String type{get;set;}
        public String locationId{get;set;}
        public String name{get;set;}
        public String locationLatitude{get;set;}
        public String id{get;set;}
    }
    public class pillars{
        public String type{get;set;}
        public String name{get;set;}
        public String id{get;set;}
        public String subtype{get;set;}
        public String distributionPairCapacity{get;set;}
        public String platformReferenceId{get;set;}
        public list<terminationModules> terminationModules{get;set;}
        public String status{get;set;}
    }
     public class metadataFTT{
        public String incidentId{get;set;}
        public String copperPathId{get;set;}
        public String tokenId{get;set;}
        public String accessServiceTechnologyType{get;set;}
        public String nbnCorrelationId{get;set;}
    }
    public class terminationModules{
        public String name{get;set;}
        public String id{get;set;}
        public String type{get;set;}
        public String twistedPairName{get;set;}
        public Integer twistedPairId{get;set;}
        public String twistedPairPlatformReferenceId{get;set;}
        public String platformReferenceId{get;set;}
        public String twistedPairStatus{get;set;}
        public String status{get;set;}
        public String twistedPairInstallationStatus{get;set;}
        public String subtype{get;set;}
        public String type_Z {get;set;}
        
    }
    public class Dpu {
        public String id {get;set;} 
        public String name {get;set;} 
        public String type_Z {get;set;} // in json: type
        public String status {get;set;} 
        public String platformReferenceId {get;set;} 
        public String portId {get;set;} 
        public String portName {get;set;} 
        public String portPlatformReferenceId {get;set;} 
        public String portStatus {get;set;} 
        public String portInstallationStatus {get;set;} 
        public String serialNumber {get;set;} 
    }
     public class PNIExc {
        public String code;
        public String message;
    }
    public class mdfs{
       public String id;
       public String type;
       public String subtype;
       public String status;
       public String platformReferenceId;
       public String chassisId;
       public String chassisType;
       public String chassisSubtype;
       public String chassisStatus;
       public String chassisPlatformReferenceId;
       public String portId;
       public String portName;
       public String portPlatformReferenceId;
       public String portStatus;
   }
}