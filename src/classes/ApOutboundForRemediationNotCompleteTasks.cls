/*
@Auther : Suman Gunaganti
@Date : 09/Dec/2018
@Description : publish platform events for remeditation task not completed
*/

public class ApOutboundForRemediationNotCompleteTasks {
    @InvocableMethod(label='Remedition not complete tasks')
    public static void RemediationNotCompleteTaskMethod(List<Id> taskIds){
    List<Developments__e> devents = new List<Developments__e>();
	New_Dev_Platform_Events__c identifierList = New_Dev_Platform_Events__c.getOrgDefaults();
    set<Id> StageAppIds = new set<Id>();
    for(Task t: [Select Id, WhatId, Status From Task Where Id IN :taskIds AND Status='Open']){
		    StageAppIds.add(t.WhatId);                                    
    	}
    for(Stage_Application__c StageApp:[Select Id, Application_ID__c from Stage_Application__c where Id in : StageAppIds]){
            Developments__e OpenTskEvent = new Developments__e();
            OpenTskEvent.Event_Id__c = identifierList.Remediation_not_complete__c;
            OpenTskEvent.Event_Type__c = 'Remediation Not Complete';
            OpenTskEvent.Source__c = 'Salesforce';
            OpenTskEvent.Call_Back_URL__c = identifierList.Call_Back_Base_URL__c+'sobjects/Stage_Application__c/'+StageApp.Id;
            OpenTskEvent.Correlation_Id__c = StageApp.Application_ID__c;
            OpenTskEvent.Triggering_Object__c = 'Stage_Application__c';
            OpenTskEvent.Event_Sub_Type__c = '';
        	devents.add(OpenTskEvent);
        }
     if(!devents.isEmpty()){
         EventBus.publish(devents);
     }            
    }
}