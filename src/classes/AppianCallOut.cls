/*
@Auther : Anjani Tiwari
@Date : 01/Nov/2017
@Description : Plateform Event subscription to send the data to Appian
*/
public class AppianCallOut{
    @future(callout=true)
    public static void SendInformationToAppian(String evtBody,String eventIdentifier,String StgAppId){
        system.debug('@@evtbody==='+evtBody);
        List<string> evtIdWithOppId;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        DF_Order_Settings__mdt ordSetting = [SELECT OrderAckEventCode__c,OrderAckSource__c,OrderAckBatchSize__c,
                                              OrderCreateBatchSize__c,OrderCreateCode__c,OrderCreateSource__c,
                                              OrderAccBatchSize__c,OrderAccEventCode__c,OrderAccSource__c,
                                              OrderCompleteBatchSize__c,OrderCompleteEventCode__c,
                                              OrderCompleteSource__c
                                              FROM   DF_Order_Settings__mdt
                                              WHERE  DeveloperName = 'DFOrderSettings'];
        DF_Order_Settings__mdt nsOrdSetting = NS_Order_Utils.getOrderSettings('NSOrderSettings');
        request.setEndpoint('callout:Appian');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');

        //getting the event id with opportunity id from eventIdentifier param for SubmitOrder callout
        if(eventIdentifier != null) {
            system.debug('::eventIdentifier::'+eventIdentifier);
            evtIdWithOppId = eventIdentifier.split(';');
            system.debug('::evtwithoppId::'+evtIdWithOppId);
        }
        if(evtIdWithOppId != null && evtIdWithOppId.size() == 2) {
            String accessSeekerId = '';
            String correlationid = '';

            //adding more hearder information for SubmitOrder
            request.setHeader('activityName','createOrder');
            request.setHeader('msgName','ManageProductOrdercreateOrderRequest');
            request.setHeader('businessServiceName','ManageProductOrder');
            request.setHeader('businessServiceVersion','V2.0');
            request.setHeader('msgType','Request');
            request.setHeader('timestamp',datetime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\''));
            request.setHeader('security','Placeholder Security');
            request.setHeader('communicationPattern','SubmitNotification');
            request.setHeader('orderPriority', '6');
            if(evtIdWithOppId[0] == ordSetting.OrderCreateCode__c) {
                //getting the access seeker id && correlation id
                List<DF_Quote__c> dfqList=[SELECT guid__c,Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c from DF_Quote__c where GUID__c = :evtIdWithOppId[1] LIMIT 1];
                if(!dfqList.isEmpty()) {
                    accessSeekerId = dfqList[0].Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c;
                    correlationid = dfqList[0].guid__c;
                }
            }else if(evtIdWithOppId[0]==nsOrdSetting.OrderCreateCode__c){
                //getting the access seeker id && correlation id
                List<DF_Quote__c> dfqList=[SELECT Order_GUID__c,Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c from DF_Quote__c where  Order_GUID__c= :evtIdWithOppId[1] LIMIT 1];
                if(!dfqList.isEmpty()) {
                    accessSeekerId = dfqList[0].Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c;
                    correlationid = dfqList[0].Order_GUID__c;
                }
                request.setHeader('msgName','ManageResourceOrdercreateOrderRequest');
                request.setHeader('businessServiceName','ManageResourceOrder');
                request.setHeader('businessChannel','Enterprise Portal');
            }else if(evtIdWithOppId[0]==nsOrdSetting.OrderCostVarianceCode__c){
                request.setHeader('msgName','ManageResourceOrderResumeOrderRequest');
                request.setHeader('activityName','resumeOrder');
                request.setHeader('businessChannel','Enterprise Portal');
                request.setHeader('businessServiceName','ManageResourceOrder');
                request.setHeader('communicationPattern','RequestReply');
            }
            request.setHeader('accessSeekerID',accessSeekerId);
            request.setHeader('correlationId',correlationid);
        }
        request.setBody(evtBody);
        try {

            System.debug('APPIAN Request: ' + request);
            System.debug('APPIAN Body: ' + request.getBody());
            System.debug('APPIAN Req: ' + String.valueOf(request));
              response = http.send(request);

              //HttpResponse response = http.send(request);
              system.debug('***Status Code****'+response.getStatusCode());
              if (response.getStatusCode() != 200){
                 system.debug('The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());

                  EventRetryAndErrorHandler.RetryOrthrowExMessage();   // resend the message again when it failed Max 2 times
              }else{
                  system.debug('Response is 200'+response.getBody());
                  if(eventIdentifier=='SF-SD-000003'){
                     Stage_Application__c stgApp  = [Select Id, Application_Status__c from Stage_Application__c where Id =: StgAppId];
                     system.debug('***stageApprec==='+stgApp);
                     stgApp.Application_Status__c = 'Service Delivery';
                    update stgApp;
                  }
                  if(evtIdWithOppId != null && evtIdWithOppId.size() == 2) {
                      if(evtIdWithOppId[0]==ordSetting.OrderCreateCode__c){
                         DF_Order__c order = [SELECT Id, Order_Status__c FROM DF_Order__c WHERE DF_Quote__r.GUID__c =:evtIdWithOppId[1]];
                         order.Order_Status__c = DF_Constants.QUOTE_STATUS_SUBMITTED;
                         update order;
                      }else if(evtIdWithOppId[0]==nsOrdSetting.OrderCreateCode__c){
                         DF_Order__c order = [SELECT Id, Order_Status__c,Order_Submitted_Date__c FROM DF_Order__c WHERE DF_Quote__r.GUID__c =:evtIdWithOppId[1] OR DF_Quote__r.Order_GUID__c =:evtIdWithOppId[1]];
                         order.Order_Status__c = DF_Constants.QUOTE_STATUS_SUBMITTED;
                         order.Order_Submitted_Date__c  = system.date.today();
                         update order;
                      }
                  }
              }
        }catch(Exception ex) {
             GlobalUtility.logMessage('Error', 'AppianCallOut', 'SendInformationToAppian', ' ', '', '', '', ex, 0);
        }
    }
}