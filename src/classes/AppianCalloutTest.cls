/***************************************************************************************************
Class Name: AppianCalloutMock_Test
Class Type: Test Class 
Created Date: 14-Dec-2017
Function    : Appian mock callout Test class.
Developer   : Anjani Tiwari
****************************************************************************************************/
@isTest
Public class AppianCalloutTest{
    
    static Stage_Application__c stageAppObj=new Stage_Application__c();
    static Map<String, String> TestMap;
    static List<Developments__e> devents = new List<Developments__e>();
    
    static{
        //Create test data
        Account testAccount = TestDataClass.CreateTestAccountData();
        Contact noBillContact = TestDataClass.CreateTestContactData();
        
        //Create a Dveleopments object record
        Development__c devObj = new Development__c();      
        devObj.Name='testName';
        devObj.Development_ID__c='AKHS833';
        devObj.Account__c=testAccount.Id;
        devObj.Primary_Contact__c=noBillContact.Id;
        devObj.Suburb__c='testsuburd';
        insert devObj;
        
        SA_Auto_Reference_Number__c refNumber = new SA_Auto_Reference_Number__c(Name = 'SA001', Last_Sequence_Number__c = '000000000');
        insert refNumber;
        
        // Create Stage Application object record
        stageAppObj.Name='testName';
        stageAppObj.Request_ID__c ='AKHS833';
        stageAppObj.Development__c=devObj.Id;
        stageAppObj.Account__c=testAccount.Id;
        stageAppObj.Primary_Contact__c=noBillContact.Id;
        insert stageAppObj; 
    }
    
    static testMethod void sendInformationToAppianPositiveTest() {
        //Callout Successful scenario.
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200,'responseBody',TestMap));
        AppianCallOut.SendInformationToAppian('{"eventIdentifier":"SF-SD-000003","externalSystemCorrelationId":"a130l0000009OuUAAU","eventBody":"eventbody"}', 'SF-SD-000003', stageAppObj.Id); 
        Test.stopTest();
    }
    
    static testMethod void sendInformationToAppianNegativeTest() {
        //Callout not Successful scenario.
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(201,'responseBody',TestMap)); 
        AppianCallOut.SendInformationToAppian('{"eventIdentifier":"SF-SD-000003","externalSystemCorrelationId":"a130l0000009OuUAAU","eventBody":"eventbody"}', 'SF-SD-000003', stageAppObj.Id); 
        Test.stopTest();
    }
    
}