/*------------------------------------------------------------
Author:        Dave Norris
Company:       Salesforce
Description:   Apex batch job that will purge all application log records that 
               exceed the number of days determine by the Application_log_Duration__c field
               in the Application_Settings__c Custom Setting
Test Class:    
History 
------------------------------------------------------------*/ 

global class ApplicationLogPurge_Batch implements Database.Batchable<sObject>, Database.Stateful{
    
    global String query;
    global String jobResult = 'Success'; //keeps track of any errors during job execution
    global DateTime jobStartDateTime;    //Keeps track of start time for the job
    
    global ApplicationLogPurge_Batch() {
    
        //Retrieve the purge day value from System Settings custom setting and pass in to batch execution
        Application_Log_Settings__c applicationLogSettings = Application_Log_Settings__c.getInstance();       
        Integer logDuration = (Integer)applicationLogSettings.Delete_After_Days__c;
       
        //For a test class ensure all records are processed as newly created logs will not meet age criteria
        if ( Test.isRunningTest() ) {
            query = 'SELECT Id, Age__c FROM Application_Log__c LIMIT 200';
        }
        else {
            query = 'SELECT Id, Age__c FROM Application_Log__c WHERE Age__c > ' + logDuration + ' LIMIT 100000';         
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {  
    
        jobStartDateTime = DateTime.now();
        
        return Database.getQueryLocator( query );  
        
    }
        
    global void execute(Database.BatchableContext BC, List<Application_Log__c> agedApplicationLogs) { 
        if( agedApplicationLogs.size() !=0 ){
            try {
                delete agedApplicationLogs;
            }
            catch ( Exception e ) {
                String errorMsg = 'Failed to purge Application_Log__c object. ' + '/n' + e.getMessage();
                GlobalUtility.logMessage('Error', 'ApplicationLogPurge_Batch', 'execute()', BC.getJobId(), 'AsyncApexJob Id', errorMsg, '', e, 0);
                jobResult = 'Fail';
            }
        }
    }        
    global void finish(Database.BatchableContext BC) {
        String logLevel = 'Info';
        //Query the AsyncApexJob object to retrieve the current job's latest information.
        AsyncApexJob jobInfo = [SELECT Id, Status, 
                                       NumberOfErrors, JobItemsProcessed,
                                       TotalJobItems, CreatedBy.Email
                                FROM AsyncApexJob 
                                WHERE Id = :BC.getJobId()];
                             
        long jobExecTime = 0;
        jobExecTime = DateTime.now().getTime() - jobStartDateTime.getTime();
        
        String jobMsg;
        
        if ( jobResult == 'Success' ) {
            jobMsg = 'ApplicationLogPurge_Batch Job Successful' +'\n';
        }
        else {
            jobMsg = 'ApplicationLogPurge_Batch Job Errored' +'\n';
            logLevel = 'Error';
        }
        
        
        jobMsg = jobMsg + '============================================' +'\n';
        jobMsg = jobMsg + 'Job #: ' + jobInfo.Id +  +'\n';
        jobMsg = jobMsg + 'Start Time: ' + jobStartDateTime + '\n';
        jobMsg = jobMsg + 'End Time: ' + DateTime.now() + '\n';
        jobMsg = jobMsg + 'Execution Time: ' + jobExecTime + '\n';
        jobMsg = jobMsg + 'Status: ' + jobInfo.Status +'\n';
        jobMsg = jobMsg + 'Items Processed: ' + jobInfo.JobItemsProcessed +'\n';
        jobMsg = jobMsg + 'No. of Errors: ' + jobInfo.NumberOfErrors +'\n';
        jobMsg = jobMsg + '============================================';


        GlobalUtility.logMessage( logLevel,
                                  'ApplcationLogPurge_Batch', 
                                  'finish()', 
                                  jobInfo.Id, 
                                  'AsyncApexJob Id', 
                                  jobmsg, 
                                  '', 
                                  null, 
                                  jobExecTime ); 
    }                 
}