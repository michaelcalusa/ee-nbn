/*------------------------------------------------------------
Author:        Dave Norris
Company:       Salesforce
Description:   Schedulable class for ApplicationLogPurge_Batch
               
Test Class:    
History 
------------------------------------------------------------*/
global class ApplicationLogPurge_BatchSchedule implements Schedulable
{  
    global void execute(SchedulableContext sc)
    {
        Integer scopeSize = 200;
        
        ApplicationLogPurge_Batch purgeApplicationLog = new ApplicationLogPurge_Batch();
        
        Database.executebatch( purgeApplicationLog, scopeSize ); 
    }   
}