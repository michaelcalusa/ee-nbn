@isTest
private class ApplicationLogPurge_Test {
/*------------------------------------------------------------
Author:        Dave Norris
Company:       salesforce.com
Description:   Test class for ApplicationLogPurge_Batch and ApplicationLogPurge_BatchSchedule.
               This test class runs the batch once with 200 records.
History
<Date>      <Authors Name>     <Brief Description of Change>
------------------------------------------------------------*/  
    private static testMethod void scheduleAndExportApplicationLogPurge(){
        //Create custom setting record required for creating debug records.
        Application_Log_Settings__c applicationLogSettingsForTests = CustomSettingUtility_Test.getApplicationLogSettingsForTests( new Application_Log_Settings__c ( Debug__c = true,
                                                                                                                                  Error__c = true,
                                                                                                                                  Warning__c = true,
                                                                                                                                  Info__c = true,
                                                                                                                                  Delete_After_Days__c = 1 ));
        
        //insert systemSettingsForTests;                                             
        //Create custom setting record requiredfor exporting log file.
       
        list<Application_Log__c> applicationLogs = new  list<Application_Log__c>();
        //create Seniors card record with status pending
        for(integer i=0; i<200; i++){
        
            Application_Log__c applicationLog = new Application_Log__c( Source__c = 'ApplicationLogPurge_Batch',
                                                                        Source_Function__c = 'execute()',
                                                                        Reference_Id__c = '123',
                                                                        Reference_Information__c = 'Test Id',
                                                                        Message__c = 'Error Message',
                                                                        RecordTypeId = GlobalCache.getRecordTypeId('Application_Log__c', 'Error'),
                                                                        Code__c = '',
                                                                        Timer__c = 0);
            applicationLogs.add( applicationLog );    
                                
        }   
        insert applicationLogs;
        
        //Start the batch run       
        Test.startTest();
        
        system.assertEquals( 200, [select id from Application_Log__c ].size() );
        
        Database.executeBatch( new ApplicationLogPurge_Batch() ); 
         
        String jobId = System.schedule( 'ApplicationLogPurge_Batch', 
                                        '0 0 0 3 9 ? 2022', 
                                        new ApplicationLogPurge_BatchSchedule());                               
        
        Test.stopTest();
        
        //Check that all errors created above have been purged
        List <Application_Log__c> errorLogs = [SELECT Id 
                                        FROM Application_log__c 
                                        WHERE RecordTypeId =: GlobalCache.getRecordTypeId('Application_Log__c', 'Error')];
                                        
        system.assertEquals( 0, errorLogs.size() );
        
        //Check that there is one record created as information after execution                                
        List <Application_Log__c> infoLogs = [SELECT Id 
                                        FROM Application_log__c 
                                        WHERE RecordTypeId =: GlobalCache.getRecordTypeId('Application_Log__c', 'Info')];
        
        system.assertEquals( 1, infoLogs.size() );
        
    }   
}