/*------------------------------------------------------------  
Author:        Dave Norris
Company:       Salesforce
Description:   A wrapper class to allow a number of application log
               objects to be built up in a trigger. This supports the
               bulkification of logging messages where necessary
Test Class:    Not applicable
History
<Date>      <Authors Name>     <Brief Description of Change>
------------------------------------------------------------*/
public class ApplicationLogWrapper {

    public string source {get;set;}
    public string sourceFunction {get;set;}
    public string referenceId {get;set;}
    public string referenceInfo{get;set;}
    public string logMessage {get;set;}
    public string payload {get;set;}
    public Exception ex {get;set;}
    public string debugLevel {get;set;}
    public string logCode {get;set;}
    public long timer {get;set;}
    

}