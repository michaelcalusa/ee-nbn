/**
 * Created by alan on 2019-02-26.
 */

public virtual class ArgumentCaptor extends ArgumentMatcher {

    private Object capturedArg;

    public override Boolean matches(Object arg){
        this.capturedArg = arg;
        return this.isMatch(arg);
    }

    public Object getCaptured(){
        return this.capturedArg;
    }

}