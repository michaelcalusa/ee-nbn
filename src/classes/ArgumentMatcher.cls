/**
 * Created by alan on 2019-02-26.
 */

public virtual class ArgumentMatcher {

    public virtual Boolean matches(Object arg){
        return this.isMatch(arg);
    }

    //override to implement matching
    public virtual Boolean isMatch(Object arg){
        return false;
    }

}