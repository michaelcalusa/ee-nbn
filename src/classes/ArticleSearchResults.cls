/*
 *@Author:       Allanah Mae San Miguel
 *@Created Date: 2017-04-17
 *@Description:  Controller for article search results component, uses sosl for search term searches, and soql otherwise
 */
public class ArticleSearchResults {
    @AuraEnabled
    public static String getArticles(String searchTerm, String topicId) {
        Set<Id> filteredArticleIds = new Set<Id>();
        List<SObject> searchResults = new List<SObject>();
            
        if (!String.isBlank(topicId)) {
            for (TopicAssignment ta : [SELECT EntityId FROM TopicAssignment WHERE TopicId=:topicId LIMIT 1000]) {
                filteredArticleIds.add(ta.EntityId);
            }
        	System.debug(filteredArticleIds);
        }
        
        if (!String.isBlank(searchTerm)) {
            String soslQuery = 'FIND :searchTerm IN ALL FIELDS';
            soslQuery += ' RETURNING KnowledgeArticleVersion(UrlName,Id, Title, Summary, ArticleType, LastPublishedDate, FirstPublishedDate, KnowledgeArticleId';
            soslQuery += ' WHERE PublishStatus = \'Online\' AND Language = \'en_US\'';
            if (!filteredArticleIds.isEmpty()) {
                soslQuery += ' AND Id IN: filteredArticleIds';
            }
            soslQuery += ')';
            
            List<List<SObject>> searchList = search.query(soslQuery);
            System.debug('searchList: ' + searchList);
            
            searchResults = new List<SObject>();
            if (!searchList.isEmpty()) {
                searchResults.addAll(searchList[0]);
            }
        }
        //if there is no search term, get all results via soql
        else {
            String soqlQuery = 'SELECT UrlName, Title, Summary, ArticleType, LastPublishedDate, FirstPublishedDate, KnowledgeArticleId';
            soqlQuery += ' FROM KnowledgeArticleVersion';
            soqlQuery += ' WHERE PublishStatus = \'Online\' AND Language = \'en_US\'';
            if (!filteredArticleIds.isEmpty()) {
            	soqlQuery += ' AND Id IN: filteredArticleIds';
            }
            searchResults = Database.query(soqlQuery);
        }
        
        if (!String.isBlank(topicId) && filteredArticleIds.isEmpty()) {
        	//if nothing under topic Id, return empty
        	System.debug('clearing...');
            return null;
        }
        else if (!searchResults.isEmpty()) {
            KnowledgeArticleVersion kav;
			Map<String, String> articleNameByType = new Map<String, String>();
            Map<Id, KnowledgeArticleVersion> versionByknowledgeArticle = new Map<Id, KnowledgeArticleVersion>();
            
            for (Sobject obj: searchResults) {
                kav = (KnowledgeArticleVersion) obj;
                versionByknowledgeArticle.put(kav.KnowledgeArticleId, kav);
                if (!articleNameByType.containsKey(kav.ArticleType)) {
                	articleNameByType.put(kav.ArticleType, Schema.getGlobalDescribe().get(kav.ArticleType).getDescribe().getLabel());
                }
            }
            
            String serializedArticles = json.serialize(searchResults);
                
            //getArticleTypeMapping
            
            //do a string replace
            String idString;
            String viewCount;
            String articleType;
            String firstPublishedDifference;
            String lastPublishedDifference;
            for (KnowledgeArticleViewStat viewStat : [SELECT Id, ViewCount, ParentId FROM KnowledgeArticleViewStat WHERE ParentId IN: versionByknowledgeArticle.keySet()]) {
                idString = '"Id":"' + versionByknowledgeArticle.get(viewStat.ParentId).Id + '"';
                viewCount = ',"ViewCount":"' + viewStat.ViewCount + '"';
                articleType = ',"ArticleTypeName":"' + articleNameByType.get(versionByknowledgeArticle.get(viewStat.ParentId).ArticleType) + '"';
                firstPublishedDifference = ',"FirstPublishedDifference":"' + String.valueOf(versionByknowledgeArticle.get(viewStat.ParentId).FirstPublishedDate.Date().daysBetween(Date.today())) + '"';
                lastPublishedDifference = ',"LastPublishedDifference":"' + String.valueOf(versionByknowledgeArticle.get(viewStat.ParentId).LastPublishedDate.Date().daysBetween(Date.today())) + '"';
                serializedArticles = serializedArticles.replaceFirst(idString, idString+viewCount+articleType+firstPublishedDifference+lastPublishedDifference);
                //System.debug(serializedArticles);
            }
            return serializedArticles;
        }
        return null;
    }
}