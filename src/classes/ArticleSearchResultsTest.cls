/*
 *@Author:       Allanah Mae San Miguel
 *@Created Date: 2017-05-04
 *@Description:  Test class for article search results
 */
@isTest
private class ArticleSearchResultsTest {

    @testSetup static void createTestData() {
        //articles
        List<FAQ__kav> equipmentArticles = new List<FAQ__kav>();
        equipmentArticles.add(new FAQ__kav (Title='1 Unit Test', UrlName='12345'));
        equipmentArticles.add(new FAQ__kav (Title='2 Bluewolf Test', UrlName='123456'));
        equipmentArticles.add(new FAQ__kav (Title='3 Broadband', UrlName='123457'));

        List<News__kav> newsArticles = new List<News__kav>();
        newsArticles.add(new News__kav (Title='1 Cat', UrlName='123458'));
        newsArticles.add(new News__kav (Title='2 Mouse', UrlName='123459'));
        newsArticles.add(new News__kav (Title='3 Dog Test', UrlName='123450'));

        insert equipmentArticles;
        insert newsArticles;

        Set<Id> articleIds = new Set<Id>();
        equipmentArticles = [SELECT Id, KnowledgeArticleId FROM FAQ__kav WHERE PublishStatus = 'Draft' AND Language = 'en_US' ORDER BY Title];
        for (FAQ__kav article : equipmentArticles) {
            KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
            articleIds.add(article.Id);
        }

        newsArticles = [SELECT Id, KnowledgeArticleId FROM News__kav WHERE PublishStatus = 'Draft' AND Language = 'en_US' ORDER BY Title];
        for (News__kav article : newsArticles) {
            KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
            articleIds.add(article.Id);
        }

        
        //topics
        List<Topic> topics = new List<Topic>();
        topics.add(new Topic(Name='Equipment'));
        topics.add(new Topic(Name='Aminals'));
        topics.add(new Topic(Name='Nothing'));

        insert topics;

        //topic assignment
        List<TopicAssignment> topicAssignments = new List<TopicAssignment>();
        TopicAssignment ta;
        for (FAQ__kav article : equipmentArticles) {
            ta = new TopicAssignment(TopicId = topics[0].Id, EntityId = article.Id);
            topicAssignments.add(ta);
        }
        for (News__kav article : newsArticles) {
            ta = new TopicAssignment(TopicId = topics[1].Id, EntityId = article.Id);
            topicAssignments.add(ta);
        }
        insert topicAssignments;

        //cannot modify KnowledgeArticleVersion in Test classes
    }

    @isTest static void searchTermTest() {
        List<Id>fixedSearchResults = new List<Id>();

        for (KnowledgeArticleVersion article : [SELECT Id, Title, Summary, ArticleType, LastPublishedDate, FirstPublishedDate, KnowledgeArticleId
                                                FROM KnowledgeArticleVersion
                                                WHERE Title LIKE '%Test%'
                                                AND PublishStatus = 'Online' AND Language = 'en_US']) {
            fixedSearchResults.add(article.Id);
        }
        System.debug('fixedSearchResults' + fixedSearchResults);
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();

            String results = articleSearchResults.getArticles('Test', null);
            List<KnowledgeArticleVersion> articles = (List<KnowledgeArticleVersion>)JSON.deserialize(results,List<KnowledgeArticleVersion>.class);
            //System.debug(articles);
            System.assertEquals(fixedSearchResults.size(), articles.size());
            
        Test.stopTest();
    }

    @isTest static void negativeSearchTermTest() {
        List<Id>fixedSearchResults = new List<Id>();

        for (KnowledgeArticleVersion article : [SELECT Id, Title, Summary, ArticleType, LastPublishedDate, FirstPublishedDate, KnowledgeArticleId
                                                FROM KnowledgeArticleVersion
                                                WHERE Title LIKE '%nbn%'
                                                AND PublishStatus = 'Online' AND Language = 'en_US']) {
            fixedSearchResults.add(article.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.startTest();
            Test.setFixedSearchResults(fixedSearchResults);
            String results = articleSearchResults.getArticles('nbn', null);
            System.assertEquals(null, results);
        Test.stopTest();
    }

    @isTest static void topicListTest() {
        Id topicId = [SELECT Id FROM Topic WHERE Name = 'Equipment' LIMIT 1].Id;
        List<TopicAssignment> equipmentArticles = [SELECT EntityId
                                                    FROM TopicAssignment
                                                    WHERE TopicId = :topicId];

        Set<Id> articleIds = new Set<Id>();
        for (TopicAssignment ta : equipmentArticles) {
            articleIds.add(ta.EntityId);
        }

        Test.startTest();
            String results = articleSearchResults.getArticles(null, topicId);
            List<KnowledgeArticleVersion> articles = (List<KnowledgeArticleVersion>)JSON.deserialize(results,List<KnowledgeArticleVersion>.class);
            System.assertEquals(equipmentArticles.size(), articles.size());

            for (KnowledgeArticleVersion article : articles) {
                if (!articleIds.contains(article.Id)) {
                    System.assert(false);
                }
            }

            topicId = [SELECT Id FROM Topic WHERE Name = 'Nothing' LIMIT 1].Id;
            results = articleSearchResults.getArticles(null, topicId);
            System.assertEquals(null, results);

        Test.stopTest();
    }

    //test both at once
    @isTest static void topicAndSearchTermTest() {

        Id topicId = [SELECT Id FROM Topic WHERE Name = 'Equipment' LIMIT 1].Id;
        List<TopicAssignment> equipmentArticles = [SELECT EntityId
                                                    FROM TopicAssignment
                                                    WHERE TopicId = :topicId];
        Set<Id> articleIds = new Set<Id>();
        for (TopicAssignment ta : equipmentArticles) {
            articleIds.add(ta.EntityId);
        }

        List<Id>fixedSearchResults = new List<Id>();

        for (KnowledgeArticleVersion article : [SELECT Id, Title, Summary, ArticleType, LastPublishedDate, FirstPublishedDate, KnowledgeArticleId
                                                FROM KnowledgeArticleVersion
                                                WHERE Title LIKE '%Test%'
                                                AND PublishStatus = 'Online' AND Language = 'en_US']) {
            fixedSearchResults.add(article.Id);
        }

        //filter agains both topic and search term
        Set<Id> expectedResults = new Set<Id>();
        for (Id matchingId : fixedSearchResults) {
            if (articleIds.contains(matchingId)) {
                expectedResults.add(matchingId);
            }
        }

        Test.setFixedSearchResults(fixedSearchResults);

        Test.startTest();

            String results = articleSearchResults.getArticles('Test', topicId);
            List<KnowledgeArticleVersion> articles = (List<KnowledgeArticleVersion>)JSON.deserialize(results,List<KnowledgeArticleVersion>.class);
            

            System.assertEquals(expectedResults.size(), articles.size());
            for (KnowledgeArticleVersion article : articles) {
                if (!expectedResults.contains(article.Id)) {
                    System.assert(false);
                }
            }
        Test.stopTest();
    }
}