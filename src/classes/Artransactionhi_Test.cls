@isTest
public class Artransactionhi_Test{

    private static testMethod void wwwNbnComAuDmmEnterpriseCommonTest(){
    
        wwwNbnComAuDmmEnterpriseCommon.SOAHeader_element  SOAHeader= new wwwNbnComAuDmmEnterpriseCommon.SOAHeader_element();
        wwwNbnComAuDmmEnterpriseCommon.ResultStatus_element ResultStatus= new wwwNbnComAuDmmEnterpriseCommon.ResultStatus_element();
        wwwNbnComAuDmmEnterpriseCommon.SOAResponseHeader_element SOAResponse= new wwwNbnComAuDmmEnterpriseCommon.SOAResponseHeader_element();
        
    }
    
    private static testMethod void wwwNbncoComAuServiceArtransactionhiTest(){
     
        wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element invHeader = new wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element();
        wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse response = new wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse();
        wwwNbncoComAuServiceArtransactionhi.AccountList_element acclst = new wwwNbncoComAuServiceArtransactionhi.AccountList_element();
        wwwNbncoComAuServiceArtransactionhi.Correlation_element corr = new wwwNbncoComAuServiceArtransactionhi.Correlation_element();
        wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryRequest ARTransHisReq = new wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryRequest();
        wwwNbncoComAuServiceArtransactionhi.InvoiceRecord_element invrec = new wwwNbncoComAuServiceArtransactionhi.InvoiceRecord_element();
        wwwNbncoComAuServiceArtransactionhi.InvoiceList_element invlst = new wwwNbncoComAuServiceArtransactionhi.InvoiceList_element();
        wwwNbncoComAuServiceArtransactionhi.CustomerDetails_element custdet = new wwwNbncoComAuServiceArtransactionhi.CustomerDetails_element();
        wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element custIden = new wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element();
        wwwNbncoComAuServiceArtransactionhi.AccountRecord_element accRec = new wwwNbncoComAuServiceArtransactionhi.AccountRecord_element();
        wwwNbncoComAuServiceArtransactionhi.AccountDetails_element accdet = new wwwNbncoComAuServiceArtransactionhi.AccountDetails_element();
        wwwNbncoComAuServiceArtransactionhi.Customer_element custelement = new wwwNbncoComAuServiceArtransactionhi.Customer_element ();
        wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element custreq = new wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element ();
        wwwNbncoComAuServiceArtransactionhi.Invoice_element invelement = new wwwNbncoComAuServiceArtransactionhi.Invoice_element ();
        wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element accIden = new wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element();
        wwwNbncoComAuServiceArtransactionhi.Account_element accelement = new wwwNbncoComAuServiceArtransactionhi.Account_element ();

    }
    
    private static testMethod void AsyncWwwNbncoComAuServiceArtransactionhiTest2(){
     Test.setMock(WebServiceMock.class,new syncServiceArtransactionMockImpl());
     AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture AsynNBN = new AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture();
     wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element customerreq = new wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element();
     wwwNbncoComAuServiceArtransactionhi.Invoice_element Invelement = new wwwNbncoComAuServiceArtransactionhi.Invoice_element();  
     wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse response = AsynNBN.getValue();
     AsyncWwwNbncoComAuServiceArtransactionhi.AsyncARTransactionHistoryBindingSOAPQSPort AsynAR = new AsyncWwwNbncoComAuServiceArtransactionhi.AsyncARTransactionHistoryBindingSOAPQSPort();
     Continuation cont = new Continuation(120);
     AsynNBN = AsynAR.beginRetrieveARTransactionHistory(cont,customerreq,Invelement,'20160528','20170227','NewDevInvoice','creditMemo');
     }
    
    private static testMethod void wwwNbncoComAuServiceArtransactionhiTest1(){
        
        Test.setMock(WebServiceMock.class,new syncServiceArtransactionMockImpl());
        wwwNbncoComAuServiceArtransactionhi.ARTransactionHistoryBindingSOAPQSPort NBNCom = new wwwNbncoComAuServiceArtransactionhi.ARTransactionHistoryBindingSOAPQSPort();
        wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryRequest retARTrans = new wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryRequest();
        wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element customerreq = new wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element();
        wwwNbncoComAuServiceArtransactionhi.Invoice_element Invelement = new wwwNbncoComAuServiceArtransactionhi.Invoice_element();
        wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse response = new wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse();
        response = NBNCom.RetrieveARTransactionHistory(customerreq,Invelement,'20160523','20170420','New Dev Invoice','Credit Memo');
     }
    
    
     
 }