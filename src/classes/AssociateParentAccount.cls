/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class will be used to establish Parent-Child Account Relationship
Modifications:
*/
public class AssociateParentAccount implements Database.Batchable<Sobject>, Database.Stateful
{
    public String accIntQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {            
    }
    
    public AssociateParentAccount(String ejId)
    {
    	 exJobId = ejId;
    	 Batch_Job__c bj = new Batch_Job__c();
    	 bj.Name = 'AssociateParentAccount'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        accIntQuery = 'select Id, CRM_Account_Id__c, Parent_Account_Id__c,Primary_Contact_Id__c from Account_Int__c where Transformation_Status__c = \'Ready for Parent Account Association\' order by CreatedDate'; 
        return Database.getQueryLocator(accIntQuery);            
    }
    public void execute(Database.BatchableContext BC,List<Account_Int__c> listOfAccInt)
    {        
      	try
        {                                    
            List<Account> listOfAcc = new List<Account>();
            
            list <string> odId = new list<string>();
			
			for (Account_Int__c c: listOfAccInt)
			{
			    string s = c.Parent_Account_Id__c;
			    odId.add(s);
			}
			map<string,Id> mapListOfAcc = new map<string,Id>();
			for( Account a: [Select on_demand_id__c,Id from Account where on_demand_id__c in :odId FOR UPDATE])
			{
			    mapListOfAcc.put(a.on_demand_id__c, a.Id); 
			}
            map <String, Account> accMap = new map<String, Account>();
            for(Account_Int__c accInt : listOfAccInt)
            {                                               
                Account a = new Account(On_Demand_Id__c = accInt.CRM_Account_Id__c);               
                //Account acc = new Account(On_Demand_Id__c = accInt.Parent_Account_Id__c);
                //a.ParentId = accInt.Parent_Account_Id__c;   
                a.ParentId = mapListOfAcc.get(accInt.Parent_Account_Id__c);
                //listOfAcc.add(a);
                accMap.put(accInt.CRM_Account_Id__c,a);
            }  
            listOfAcc = accMap.values();
            List<Database.UpsertResult> updateAccountResults = Database.Upsert(listOfAcc,Account.Field.On_Demand_Id__c,false);
            List<Database.Error> updateAccountError;
            String errorMessage, fieldsAffected;
      		String[] listOfAffectedFields;
            StatusCode sCode;
            List<Error_Logging__c> errAccountList = new List<Error_Logging__c>();
            List<Account_Int__c> updAccIntRes = new List<Account_Int__c>();
            for(Integer j=0; j<updateAccountResults.size();j++)
            {
                //Account_Int__c updAccInt = new Account_Int__c();
                if(!updateAccountResults[j].isSuccess())
                {
                    updateAccountError = updateAccountResults[j].getErrors();
                    for(Database.Error er: updateAccountError)
                    {
                        sCode = er.getStatusCode();
                        errorMessage = er.getMessage();
                        listOfAffectedFields = er.getFields();
                    }
                    fieldsAffected = String.join(listOfAffectedFields,',');
                    errAccountList.add(new Error_Logging__c(Name='Account Transformaiton - Parent Account Association',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isSuccess__c=updateAccountResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfAcc[j].On_Demand_Id__c,Schedule_Job__c=bjId));
                    //updAccInt.Transformation_Status__c = 'Error';
                    //updAccInt.CRM_Account_Id__c = listOfAcc[j].On_Demand_Id__c;
                    //updAccInt.Schedule_Job_Transformation__c = bjId;
                    //updAccIntRes.add(updAccInt);
                    listOfAccInt[j].Transformation_Status__c = 'Error';
                    listOfAccInt[j].Schedule_Job_Transformation__c = bjId;
                }
                else
                {
                    if(String.isNotBlank(mapListOfAcc.get(listOfAccInt[j].Parent_Account_Id__c)))
                    {
                        if(!String.isBlank(listOfAccInt[j].Primary_Contact_Id__c) && listOfAccInt[j].Primary_Contact_Id__c != 'No Match Row Id')
                        {
                        	listOfAccInt[j].Transformation_Status__c = 'Ready for Primary Contact Association';   
                        }
                        else
                        {
                            listOfAccInt[j].Transformation_Status__c = 'Copied to Base Table';
                        }
                        //updAccInt.Transformation_Status__c = 'Copied to Base Table';                        
                    }
                    else
                    {
                        if(!String.isBlank(listOfAccInt[j].Primary_Contact_Id__c))
                        {
                        	listOfAccInt[j].Transformation_Status__c = 'Ready for Primary Contact Association'; 
                            listOfAccInt[j].Association_Error__c = true;
                        }
                        else
                        {            
	                        listOfAccInt[j].Transformation_Status__c = 'Association Error';
    	                    errAccountList.add(new Error_Logging__c(Name='Account Transformaiton - Parent Account Association',Error_Message__c='Parent Account Record is not associated',Fields_Afftected__c='Parent Account',isCreated__c=updateAccountResults[j].isCreated(),
        	                isSuccess__c=updateAccountResults[j].isSuccess(),Record_Row_Id__c=updateAccountResults[j].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfAccInt[j].Id,Schedule_Job__c=bjId));                                               
                            
                        }                        
                        //updAccInt.Transformation_Status__c = 'Association Error';
                    }
                    
                    //updAccInt.CRM_Account_Id__c = listOfAcc[j].On_Demand_Id__c;
                    //updAccInt.Schedule_Job_Transformation__c = bjId;
                    //updAccIntRes.add(updAccInt);  
                    listOfAccInt[j].Schedule_Job_Transformation__c = bjId;                  
                }
            }
            system.debug('errAccountList===>'+errAccountList);
            Insert errAccountList;
            //Upsert updAccIntRes CRM_Account_Id__c;      
            Update listOfAccInt;
            if(errAccountList.isEmpty())      
            {
            	status = 'Completed';
            } 
            else
            {
            	status = 'Error';
            }  
            recordCount = recordCount + listOfAccInt.size();         
        }
        catch(Exception e)
        {   
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='AssociateParentAccountTransformation',Error_Message__c= e.getMessage()+' Line Number: '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob apAccJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
       	{
       		bj.Status__c= apAccJob.Status;
       	}
       	else
       	{
       		 bj.Status__c=status;
       	}
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = apAccJob.id;
        bj.End_Time__c  = apAccJob.CompletedDate;
        upsert bj;
        if (CRM_Transformation_Job__c.getinstance('ContactTransformation') <> null && CRM_Transformation_Job__c.getinstance('ContactTransformation').on_off__c)
   		{
        	Database.executeBatch(new ContactTransformation(exJobId));
        }
    }          
}