/***************************************************************************************************
Class Name:  AssociateParentAccount_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 31-10-2016
Function    : This class contains unit test scenarios for AssociateParentAccount apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       31-10-2016                Created
****************************************************************************************************/
@isTest
private class AssociateParentAccount_Test {
    static List<Account_Int__c> listOfAccountInt = new List<Account_Int__c> ();
    static void getRecords (){  
        // Custom Setting record creation
        TestDataUtility.getListOfCRMTransformationJobRecords(true);
        // Create Junction_Object_Int__c record
        date CRM_Modified_Date = date.newinstance(1963, 01,24);
        List<Junction_Object_Int__c> actintList = new List<Junction_Object_Int__c>();
        Junction_Object_Int__c actint = new Junction_Object_Int__c();
        actint.Name='TestName';
        actint.CRM_Modified_Date__c=CRM_Modified_Date;
        actint.Transformation_Status__c = 'Exported from CRM';
        actint.Object_Name__c = 'Account';
        actint.Child_Name__c = 'Contact';
        actint.Event_Name__c = 'Associate';
        actint.Object_Id__c = 'Account123';
        actint.Child_Id__c = 'Contact123';
        actintList.add(actint);
        insert actintList;
        List<Account> listOfAccounts = new List<Account> ();
        // Create Account record
        Account accRec = new Account();
        accRec.name = 'Test Account';
        accRec.on_demand_id__c = 'Account6';
        listOfAccounts.add(accRec);
        Account accRec2 = new Account();
        accRec2.name = 'Test Account';
        accRec2.on_demand_id__c = 'Account4';
        listOfAccounts.add(accRec2);
        Account accRec3 = new Account();
        accRec3.name = 'Test Account';
        accRec3.on_demand_id__c = 'Account7';
        listOfAccounts.add(accRec3);
        insert listOfAccounts;
        // Create Contact record
        Contact contactRec = new Contact ();
        contactRec.on_demand_id_P2P__c = 'Contact123';
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        insert contactRec;
    }
    static void setupAccountIntRecords1 (){
        // Create Account_Int__c record
        listOfAccountInt = new List<Account_Int__c> ();
        Account_Int__c accIntRec = new Account_Int__c ();
        accIntRec.Account_Name__c = 'AccName1';
        accIntRec.Transformation_Status__c = 'Extracted from CRM';
        accIntRec.CRM_Last_Modified_Date__c = system.now();
        accIntRec.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec.Account_Types__c = 'Developer,Builder';
        accIntRec.CRM_Account_Id__c = 'Account1';
        listOfAccountInt.add(accIntRec);
        Account_Int__c accIntRec2 = new Account_Int__c ();
        accIntRec2.Account_Name__c = 'AccName2';
        accIntRec2.Transformation_Status__c = 'Extracted from CRM';
        accIntRec2.CRM_Last_Modified_Date__c = system.now();
        accIntRec2.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec2.Account_Types__c = 'Abc';
        accIntRec2.Number_Street__c = 'test';
        accIntRec2.Address_2__c = 'test';
        accIntRec2.Address_3__c = 'test';
        accIntRec2.Bill_Number_Street__c = 'test';
        accIntRec2.Bill_Address_2__c = 'test';
        accIntRec2.Bill_Address_3__c = 'test';
        accIntRec2.CRM_Account_Id__c = 'Account2';
        listOfAccountInt.add(accIntRec2);
        Account_Int__c accIntRec3 = new Account_Int__c ();
        accIntRec3.Account_Name__c = 'AccName3';
        accIntRec3.Transformation_Status__c = 'Extracted from CRM';
        accIntRec3.CRM_Last_Modified_Date__c = system.now();
        accIntRec3.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec3.CRM_Account_Id__c = 'Account3';
        listOfAccountInt.add(accIntRec3);
        Account_Int__c accIntRec4 = new Account_Int__c ();
        accIntRec4.Account_Name__c = 'AccName4';
        accIntRec4.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec4.CRM_Last_Modified_Date__c = system.now();
        accIntRec4.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec4.Parent_Account_Id__c = 'Account4';
        accIntRec4.CRM_Account_Id__c = 'Account4';
        accIntRec4.Primary_Contact_Id__c = 'dddd';
        listOfAccountInt.add(accIntRec4);
        Account_Int__c accIntRec5 = new Account_Int__c ();
        accIntRec5.Account_Name__c = 'AccName5';
        accIntRec5.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec5.CRM_Last_Modified_Date__c = system.now();
        accIntRec5.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec5.Parent_Account_Id__c = 'Account6';
        accIntRec5.CRM_Account_Id__c = 'Account5';
        listOfAccountInt.add(accIntRec5);
        Account_Int__c accIntRec6 = new Account_Int__c ();
        accIntRec6.Account_Name__c = 'AccName6';
        accIntRec6.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec6.CRM_Last_Modified_Date__c = system.now();
        accIntRec6.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec6.Parent_Account_Id__c = 'Account7';
        accIntRec6.CRM_Account_Id__c = 'Account7';
        listOfAccountInt.add(accIntRec6);
        Account_Int__c accIntRec7 = new Account_Int__c ();
        accIntRec7.Account_Name__c = 'AccName5';
        accIntRec7.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec7.CRM_Last_Modified_Date__c = system.now();
        accIntRec7.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec7.Parent_Account_Id__c = 'bbb';
        accIntRec7.CRM_Account_Id__c = 'Account5';
        listOfAccountInt.add(accIntRec7);
        Account_Int__c accIntRec8 = new Account_Int__c ();
        accIntRec8.Account_Name__c = 'AccName6';
        accIntRec8.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec8.CRM_Last_Modified_Date__c = system.now();
        accIntRec8.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec8.Parent_Account_Id__c = 'ccc';
        accIntRec8.CRM_Account_Id__c = 'Account7';
        listOfAccountInt.add(accIntRec8);
        Account_Int__c accIntRec9 = new Account_Int__c ();
        accIntRec9.Account_Name__c = 'AccName4';
        accIntRec9.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec9.CRM_Last_Modified_Date__c = system.now();
        accIntRec9.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec9.CRM_Account_Id__c = 'Account4';
        accIntRec9.Primary_Contact_Id__c = 'abcde';
        listOfAccountInt.add(accIntRec9);
        insert listOfAccountInt;
    }
    static void setupAccountIntRecords2 (){
        listOfAccountInt = new List<Account_Int__c> ();
        Account_Int__c accIntRec10 = new Account_Int__c ();
        accIntRec10.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec10.CRM_Last_Modified_Date__c = system.now();
        accIntRec10.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec10.CRM_Account_Id__c = 'Account4';
        accIntRec10.Primary_Contact_Id__c = 'Contact123';
        accIntRec10.Parent_Account_Id__c = 'eee';
        listOfAccountInt.add(accIntRec10);
        Account_Int__c accIntRec11 = new Account_Int__c ();
        accIntRec11.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec11.CRM_Last_Modified_Date__c = system.now();
        accIntRec11.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec11.CRM_Account_Id__c = 'Account4';
        accIntRec11.Primary_Contact_Id__c = 'eeee';
        accIntRec11.Parent_Account_Id__c = 'eee';
        listOfAccountInt.add(accIntRec11); 
        insert listOfAccountInt;
    }
    static void setupAccountIntRecords3 (){
        listOfAccountInt = new List<Account_Int__c> ();
        Account_Int__c accIntRec12 = new Account_Int__c ();
        accIntRec12.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec12.CRM_Last_Modified_Date__c = system.now();
        accIntRec12.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec12.CRM_Account_Id__c = 'Account4';
        accIntRec12.Parent_Account_Id__c = 'eee';
        listOfAccountInt.add(accIntRec12);
        insert listOfAccountInt;
    }
    
    static testMethod void AssociateParentAccountTest(){
        getRecords();
        setupAccountIntRecords1();
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());   
        Test.stopTest();
    }
    static testMethod void AssociateParentAccountTest2(){
        getRecords();
        setupAccountIntRecords2();
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());   
        Test.stopTest();
    }
    static testMethod void AssociateParentAccountTest3(){
        getRecords();
        setupAccountIntRecords3();
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());   
        Test.stopTest();
    }
}