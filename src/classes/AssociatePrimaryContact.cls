/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class will be used to assocaite Primary Contact to Account
Modifications:
*/
public class AssociatePrimaryContact implements Database.Batchable<Sobject>, Database.Stateful
{ 
    public String accIntQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {            
    }
    
    public AssociatePrimaryContact(String ejId)
    {
    	 exJobId = ejId;
    	 Batch_Job__c bj = new Batch_Job__c();
    	 bj.Name = 'AssociatePrimaryContact'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        accIntQuery = 'select Id, CRM_Account_Id__c, Parent_Account_Id__c,Primary_Contact_Id__c,Association_Error__c from Account_Int__c where Transformation_Status__c = \'Ready for Primary Contact Association\' order by CreatedDate'; 
        return Database.getQueryLocator(accIntQuery);            
    }
    public void execute(Database.BatchableContext BC,List<Account_Int__c> listOfAccInt)
    {        
      	try
        {  
            List<Account> listOfAcc = new List<Account>();
            
            list <string> listOfConIds = new list<string>();
			
			for (Account_Int__c ai: listOfAccInt)
			{			    
			    listOfConIds.add(ai.Primary_Contact_Id__c);
			}
			map<string,Id> mapListOfCon = new map<string,Id>();
            for(Contact con : [select On_Demand_Id_P2P__c, Id from Contact where On_Demand_Id_P2P__c in :listOfConIds FOR UPDATE])
            {
                mapListOfCon.put(con.On_Demand_Id_P2P__c, con.id);            
            }
            map <String, Account> accMap = new map<String, Account>();            
            for(Account_Int__c accInt : listOfAccInt)
            {                                               
                Account a = new Account();                  
                a.On_Demand_Id__c = accInt.CRM_Account_Id__c;
                a.Primary_Contact__c = mapListOfCon.get(accInt.Primary_Contact_Id__c);                
                accMap.put(accInt.CRM_Account_Id__c,a);
            } 
            listOfAcc = accMap.values();
            List<Database.UpsertResult> updateAccountResults = Database.Upsert(listOfAcc,Account.Field.On_Demand_Id__c,false);
            List<Database.Error> updateAccountError;
            String errorMessage, fieldsAffected;
      		String[] listOfAffectedFields;
            StatusCode sCode;
            List<Error_Logging__c> errAccountList = new List<Error_Logging__c>();
            List<Account_Int__c> updAccIntRes = new List<Account_Int__c>();
            for(Integer j=0; j<updateAccountResults.size();j++)
            {
                //Account_Int__c updAccInt = new Account_Int__c();
                if(!updateAccountResults[j].isSuccess())
                {
                    updateAccountError = updateAccountResults[j].getErrors();
                    for(Database.Error er: updateAccountError)
                    {
                        sCode = er.getStatusCode();
                        errorMessage = er.getMessage();
                        listOfAffectedFields = er.getFields();
                    }
                    fieldsAffected = String.join(listOfAffectedFields,',');
                    if(listOfAccInt[j].Association_Error__c)
                    {
                    	errAccountList.add(new Error_Logging__c(Name='Account Transformaiton - Primary Contact & Parent Account Association',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isSuccess__c=updateAccountResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfAcc[j].On_Demand_Id__c,Schedule_Job__c=bjId));  
                    }
                    else
                    {
                        errAccountList.add(new Error_Logging__c(Name='Account Transformaiton - Primary Contact Association',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isSuccess__c=updateAccountResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfAcc[j].On_Demand_Id__c,Schedule_Job__c=bjId));
                    }
                    listOfAccInt[j].Transformation_Status__c = 'Association Error';
                    listOfAccInt[j].Schedule_Job_Transformation__c = bjId;                        
                }
                else
                {
                    if(listOfAccInt[j].Association_Error__c)
                    {
                    	errAccountList.add(new Error_Logging__c(Name='Account Transformaiton - Parent Account Association',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isSuccess__c=updateAccountResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfAcc[j].On_Demand_Id__c,Schedule_Job__c=bjId));  
                        listOfAccInt[j].Transformation_Status__c = 'Association Error';
                    }
                    else
                    {
                        listOfAccInt[j].Transformation_Status__c = 'Copied to Base Table';
                    	listOfAccInt[j].Schedule_Job_Transformation__c = bjId; 
                    }
                }                                                 
            }
            Insert errAccountList;
            //Upsert updAccIntRes CRM_Account_Id__c;      
            Update listOfAccInt;
            if(errAccountList.isEmpty())      
            {
            	status = 'Completed';
            } 
            else
            {
            	status = 'Error';
            }  
            recordCount = recordCount + listOfAccInt.size();              
        }
        catch(Exception e)
        {   
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='AssociatePrimaryContactTransformation',Error_Message__c= e.getMessage()+' Line Number: '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('c');
        AsyncApexJob apAccJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
       	{
       		bj.Status__c= apAccJob.Status;
       	}
       	else
       	{
       		 bj.Status__c=status;
       	}
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = apAccJob.id;
        bj.End_Time__c  = apAccJob.CompletedDate;
        upsert bj;
        
       
    }  
}