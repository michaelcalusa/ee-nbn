@isTest
global class AsyncArtransactionMock implements WebServiceMock {
    
    global void doInvoke( 
            Object stub, 
            Object request, 
            Map<String, Object> response, 
            String endpoint, 
            String soapAction, 
            String requestName, 
            String responseNS, 
            String responseName, 
            String responseType) {
            
            AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture response1 = new AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture();
             wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse respElement1 = response1.getValue();
            
        }

}