/*------------------------------------------------------------
Author:     Sunil Gupta
Company:       Appirio/Wipro
Description:   Queueable Apex Class calling from handleCISServiceandProductIntegration to make the DML and Apex Callout in two seperate thread. This will avoid the 
System.CalloutException: . Please commit or rollback before calling out

History
<Date>      <Author>     <Description>
14/03/2018  Sunil Gupta  Created  
------------------------------------------------------------*/
public class AsyncCache implements Queueable {
    private NBNIntegrationCache__c objCache;     
    Public AsyncCache(NBNIntegrationCache__c objCache){
        this.objCache =  objCache;
    }   
    public void execute(QueueableContext context){       
        Schema.SObjectField fieldAVC = NBNIntegrationCache__c.Fields.AVCId__c;      
        Schema.SObjectField fieldLoc = NBNIntegrationCache__c.Fields.LocId__c;     
        Schema.SObjectField fieldPRI = NBNIntegrationCache__c.Fields.PRIId__c;      
        
        if(String.isBlank(objCache.AVCId__c) == false){
          Database.upsert(objCache, fieldAVC);
        }
        if(String.isBlank(objCache.LocId__c) == false){
            Database.upsert(objCache, fieldLoc);
        }
        if(String.isBlank(objCache.PRIId__c) == false){
            Database.upsert(objCache, fieldPRI);
        }     
    }
}