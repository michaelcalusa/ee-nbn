@isTest
private class AsyncCacheTest{
    @testSetup
    public static void setup(){
        
        List<Account> accList = new List<Account>();
        for(integer ing =0;ing<50; ing++){
            Account acc = new Account(Name='TEst Acc',BillingState ='NY');
            accList.add(acc);
        }    
        
        for(integer ing =0;ing<50; ing++){
            Account acc = new Account(Name='TEst Acc',BillingState ='CA');
            accList.add(acc);
        } 
        
        insert accList;
        
        Contact con = new Contact(LastName='TestContact');
        insert con;
    }
    
    static testmethod  void testAsync(){
    
    HttpResponse response = new HttpResponse();
             response = integrationUtility.getServiceInfo('AVC000028840963'); 
           //  incidentManagementController_LC.cacheServiceIntegration('AVC000028840963',response );
        
        NBNIntegrationCache__c nbnIntcache = new NBNIntegrationCache__c();
        nbnIntcache.IntegrationType__c = 'ServiceInformation';
        nbnIntcache.JSONPayload__c = String.valueOf(response);
        nbnIntcache.AVCId__c = 'AVC000028840963';
        
            AsyncCache updateJob = new AsyncCache(nbnIntcache);
            // enqueue the job for processing
            
            ID jobID = System.enqueueJob(updateJob);
             DateTime StartDateTime = datetime.newInstance(2014, 9, 15, 12, 30, 0);
            FeedItemWrapper obj = new FeedItemWrapper('12', '12', 'Test', '25-05-1977', StartDateTime, 'Test', 'Test', 'Test', 'Test', 'Test',true,'Test','Test','Test');
           
           CheckRecursive.runOnce();
        
    }
    
    

} // class MileageTrackerTestSuite