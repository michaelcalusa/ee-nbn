public abstract class AsyncQueueableHandler implements Queueable {  
        
    private String handlerName;
    private Integer maxNoOfRows;
    private Integer maxNoOfParallels;
    
    private Integer MAX_FUTURE_CALLS;
    private Integer DEFAULT_MAX_ROWS;
    private Integer DEFAULT_MAX_PARALLELS;

    private Integer ASR_SETTINGS_MAX_ROWS;
    private Integer ASR_SETTINGS_MAX_PARALLELS;

    public static Boolean JOB_RUNNING_IN_CONTEXT = false;   
    
    public AsyncQueueableHandler(String hName) {   
        getAsyncReqConfigSettings(hName);                
        getAsyncReqSettings();                         
        setProperties(hName, null, null);
    }
    
    public AsyncQueueableHandler(String hName, Integer maxRows) { 
        system.debug('AsyncQueueableHandler.AsyncQueueableHandler.AsyncQueueableHandler - hName: ' + hName);
        system.debug('AsyncQueueableHandler.AsyncQueueableHandler.AsyncQueueableHandler - maxRows: ' + maxRows);

        getAsyncReqConfigSettings(hName);        
        getAsyncReqSettings();                    
        setProperties(hName, maxRows, null);
    }
    
    public AsyncQueueableHandler(String hName, Integer maxRows, Integer maxParallels) {  
        getAsyncReqConfigSettings(hName);        
        getAsyncReqSettings();         
        setProperties(hName, maxRows, maxParallels);
    }    
        
    public void execute(QueueableContext context) {                      
        system.debug('AsyncQueueableHandler.execute - context: ' + context);

        List<String> workParamsList = new List<String>();        
        List<String> paramList;         
        Map<Integer, List<String>> paramsMap = new Map<Integer, List<String>>();        

        Integer parallelNo = 1;     
        Integer rowCnt = 1;
        Integer paramsCnt = 0;
        
        try {
            // JobId is the AsyncApexJob Id
            Id existingJobId = context.getJobId();
            system.debug('AsyncQueueableHandler.execute - existingJobId: ' + existingJobId);
            
            paramList = getRequestParams();             
            system.debug('AsyncQueueableHandler.execute - paramList size: ' + paramList.size());

            if (!paramList.isEmpty()) {                         
                // Build map of params lists        
                for (String param : paramList) {                    
                    paramsCnt++;                    
                    
                    system.debug('AsyncQueueableHandler.execute - parallelNo: ' + parallelNo);
                    system.debug('AsyncQueueableHandler.execute - paramsCnt: ' + paramsCnt);
                    system.debug('AsyncQueueableHandler.execute - param: ' + param);        
                    
                    if (rowCnt < maxNoOfRows && paramsCnt != paramList.size() && String.isNotEmpty(param)) {                        
                        // Add parm to list
                        workParamsList.add(param);
                        rowCnt++;                   
                    } else {                        
                        // Add parm to list - final parm for this list
                        if (String.isNotEmpty(param)) {
                            workParamsList.add(param);
                        }                                               
                        
                        if (!workParamsList.isEmpty()) {
                            paramsMap.put(parallelNo, workParamsList);
                        }
                        
                        workParamsList = new List<String>();                        
                        parallelNo++;       
                        rowCnt = 1;             
                    }
                }                     
                
                system.debug('AsyncQueueableHandler.execute - paramsMap size: ' + paramsMap.size());
            
                // For each parallel - fire off executework
                for (Integer parallelNum = 1; parallelNum <= paramsMap.size(); parallelNum++) {
                    system.debug('AsyncQueueableHandler.execute - processing parallelNum: ' + parallelNum + ' of ' + paramsMap.size() + ' (' + 'Max Parallels: ' + maxNoOfParallels + ')');                                                                                             
                    system.debug('AsyncQueueableHandler.execute - paramsMapValue:' + paramsMap.get(parallelNum));
                    
                    // Get params list for parallel run
                    if (paramsMap.get(parallelNum) != null) {                       
                        // Perform logic in each handler
                        executeWork(paramsMap.get(parallelNum));
                    }
                }
                
                AsyncQueueableHandler.JOB_RUNNING_IN_CONTEXT = false;               
                system.debug('AsyncQueueableHandler.execute - AsyncQueueableHandler.JOB_RUNNING_IN_CONTEXT: ' + AsyncQueueableHandler.JOB_RUNNING_IN_CONTEXT);              
                
                run(existingJobId);
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    // Called by execute method
    // Method to be implemented by inheriting child class
    public abstract void executeWork(List<String> paramList);

    @TestVisible private List<String> getRequestParams() {              
        List<Async_Request__c> asyncReqList;
        List<String> paramList = new List<String>();
        Integer noOfParamsQueryLimit;
        
        try {    
            noOfParamsQueryLimit = getNoOfParamsQueryLimit();                                   
            system.debug('AsyncQueueableHandler.getRequestParams - noOfParamsQueryLimit: ' + noOfParamsQueryLimit);

            if (noOfParamsQueryLimit != null && noOfParamsQueryLimit > 0) {             
                asyncReqList = [SELECT   Params__c 
                                FROM     Async_Request__c 
                                WHERE    Handler__c = :handlerName 
                                AND      Status__c = :AsyncQueueableUtils.PENDING
                                ORDER BY Priority__c
                                LIMIT    :noOfParamsQueryLimit];

                system.debug('AsyncQueueableHandler.getRequestParams - asyncReqList size: ' + asyncReqList.size());

                if (!asyncReqList.isEmpty()) {                  
                    paramList = new List<String>();

                    for (Async_Request__c asyncReq: asyncReqList) {
                        paramList.add(asyncReq.Params__c );                     
                        asyncReq.Status__c = AsyncQueueableUtils.PROCESSED;
                    }

                    update asyncReqList;
                }
            }
            
            system.debug('AsyncQueueableHandler.getRequestParams - paramList size: ' + paramList.size());
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }                      
                
        return paramList;
    }

    @TestVisible private Integer getNoOfParamsQueryLimit() {                               
        Integer noOfParamsQueryLimit;
        
        try {
            system.debug('AsyncQueueableHandler.getNoOfParamsQueryLimit - maxNoOfParallels: ' + maxNoOfParallels);
            system.debug('AsyncQueueableHandler.getNoOfParamsQueryLimit - maxNoOfRows: ' + maxNoOfRows);
        
            // Validate maxNoOfParallels and maxNoOfRows
            if (maxNoOfRows == null) {              
                throw new CustomException('maxNoOfRows is null');
            }
            
            if (maxNoOfParallels == null) {             
                throw new CustomException('maxNoOfParallels is null');
            }                       
                
            noOfParamsQueryLimit = maxNoOfParallels * maxNoOfRows;
            system.debug('AsyncQueueableHandler.getNoOfParamsQueryLimit - noOfParamsQueryLimit: ' + noOfParamsQueryLimit);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }               
                
        return noOfParamsQueryLimit;
    }

    @TestVisible private void setProperties(String hName, Integer maxRows, Integer maxParallels) {      
        system.debug('AsyncQueueableHandler.setProperties - hName: ' + hName);
        system.debug('AsyncQueueableHandler.setProperties - maxRows: ' + maxRows);
        system.debug('AsyncQueueableHandler.setProperties - maxParallels: ' + maxParallels);

        try {           
            //handlerName = hName;
                        
            setMaxNoOfRows(maxRows);                        
            setMaxNoOfParallels(maxParallels);          
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }                   
    }

    @TestVisible private void setMaxNoOfRows(Integer maxRows) {     
        system.debug('AsyncQueueableHandler.setMaxNoOfRows - maxRows: ' + maxRows);

        /***
            Order of Preferences for setting Max Rows
            1) Value for max rows passed into constructor
            2) Value for max rows from Custom Settings for each process
            3) Value for max rows from Default Config Custom Settings
        ***/
                
        try {           
            system.debug('AsyncQueueableHandler.setMaxNoOfRows - ASR_SETTINGS_MAX_ROWS: ' + ASR_SETTINGS_MAX_ROWS);
            system.debug('AsyncQueueableHandler.setMaxNoOfRows - DEFAULT_MAX_ROWS: ' + DEFAULT_MAX_ROWS);           
                        
            // Set max rows
            if (maxRows != null) {
                maxNoOfRows = maxRows;
            } else if (ASR_SETTINGS_MAX_ROWS != null) {
                maxNoOfRows = ASR_SETTINGS_MAX_ROWS;
            } else {
                maxNoOfRows = DEFAULT_MAX_ROWS;
            }
            
            system.debug('AsyncQueueableHandler.setMaxNoOfRows - maxNoOfRows: ' + maxNoOfRows);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }      
    }
    
    @TestVisible private void setMaxNoOfParallels(Integer maxParallels) {
        system.debug('AsyncQueueableHandler.setMaxNoOfParallels - maxParallels: ' + maxParallels);

        /***
            Order of Preferences for setting Max Parallels
            1) Value for max parallels passed into constructor
            2) Value for max parallels from Custom Settings for each process
            3) Value for max parallels from Default Config Custom Settings
        ***/
        
        try {           
            system.debug('AsyncQueueableHandler.setMaxNoOfParallels - ASR_SETTINGS_MAX_PARALLELS: ' + ASR_SETTINGS_MAX_PARALLELS);
            system.debug('AsyncQueueableHandler.setMaxNoOfParallels - DEFAULT_MAX_PARALLELS: ' + DEFAULT_MAX_PARALLELS);
               
            // Set max parallels
            if (maxParallels != null) {
                maxNoOfParallels = maxParallels;
            } else if (ASR_SETTINGS_MAX_PARALLELS != null) {
                maxNoOfParallels = ASR_SETTINGS_MAX_PARALLELS;
            } else {
                maxNoOfParallels = DEFAULT_MAX_PARALLELS;
            }   

            system.debug('AsyncQueueableHandler.setMaxNoOfParallels - MAX_FUTURE_CALLS: ' + MAX_FUTURE_CALLS);

            // Validate against max future call limit           
            if (maxNoOfParallels > MAX_FUTURE_CALLS) {
                maxNoOfParallels = MAX_FUTURE_CALLS;
            }
            
            system.debug('AsyncQueueableHandler.setMaxNoOfParallels - maxNoOfParallels: ' + maxNoOfParallels);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    // Get default settings from custom settings Async_Request_Config_Settings__c
    @TestVisible private void getAsyncReqConfigSettings(String hName) {                
        Async_Request_Config_Settings__c asyncReqConfigSettings;
        handlerName = hName;   
        try {           
            // Use org defaults only
            asyncReqConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();         
            
            if (asyncReqConfigSettings != null) {
                MAX_FUTURE_CALLS = asyncReqConfigSettings.Max_No_of_Future_Calls__c.intValue();
                system.debug('AsyncQueueableHandler.getAsyncReqConfigSettings - MAX_FUTURE_CALLS: ' + MAX_FUTURE_CALLS);
                
                DEFAULT_MAX_ROWS = asyncReqConfigSettings.Max_No_of_Rows__c.intValue();
                system.debug('AsyncQueueableHandler.getAsyncReqConfigSettings - DEFAULT_MAX_ROWS: ' + DEFAULT_MAX_ROWS);
                
                DEFAULT_MAX_PARALLELS = asyncReqConfigSettings.Max_No_of_Parallels__c.intValue();
                system.debug('AsyncQueueableHandler.getAsyncReqConfigSettings - DEFAULT_MAX_PARALLELS: ' + DEFAULT_MAX_PARALLELS);
            }                                            
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }                          
    }

    // Get settings at handler level from custom settings Async_Request_Settings__c
    @TestVisible private void getAsyncReqSettings() {                  
        Async_Request_Settings__c asyncReqSettings;
        system.debug('::handlerName::'+handlerName);
        try {           
            asyncReqSettings = Async_Request_Settings__c.getValues(handlerName);    
            
            if (asyncReqSettings != null) {             
                ASR_SETTINGS_MAX_ROWS = asyncReqSettings.Max_No_of_Rows__c.intValue();
                system.debug('AsyncQueueableHandler.getAsyncReqSettings - ASR_SETTINGS_MAX_ROWS: ' + ASR_SETTINGS_MAX_ROWS);
                
                ASR_SETTINGS_MAX_PARALLELS = asyncReqSettings.Max_No_of_Parallels__c.intValue();
                system.debug('AsyncQueueableHandler.getAsyncReqSettings - ASR_SETTINGS_MAX_PARALLELS: ' + ASR_SETTINGS_MAX_PARALLELS);
            }                                            
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }                    
    }        
        
    public void run(Id existingJobId) {       
        Boolean anythingPendingToRun = false;   
        Boolean jobEnqueued = false;
                       
        try {
            system.debug('AsyncQueueableHandler.run - existingJobId: ' + existingJobId);            
            system.debug('AsyncQueueableHandler.run - AsyncQueueableHandler.JOB_RUNNING_IN_CONTEXT: ' + AsyncQueueableHandler.JOB_RUNNING_IN_CONTEXT);            
            
            if (!AsyncQueueableHandler.JOB_RUNNING_IN_CONTEXT) {                
                anythingPendingToRun = AsyncQueueableUtils.isAnythingPendingToRun(handlerName);
                system.debug('AsyncQueueableHandler.run - anythingPendingToRun: ' + anythingPendingToRun);
                
                if (anythingPendingToRun) {                 
                    jobEnqueued = AsyncQueueableUtils.enqueueJob(handlerName, existingJobId);
                    system.debug('AsyncQueueableHandler.run - jobEnqueued: ' + jobEnqueued);
                                    
                    if (jobEnqueued) {                      
                        AsyncQueueableHandler.JOB_RUNNING_IN_CONTEXT = true;
                        
                        system.debug('AsyncQueueableHandler.run: ' + handlerName + ' job has started');
                    } else {
                        system.debug('AsyncQueueableHandler.run: ' + handlerName + ' job is already running');
                    }
                }
            }           
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }               
    }      
}