@isTest
private class AsyncQueueableHandler_Test {

    @isTest 
    static void test_execute_constructor_type_2_withExistingHandler() {    	
    	/** Data setup **/
    	List<Async_Request__c> asyncReqList;
    	
    	String HANDLER_NAME = DF_SOACreateBillingEventHandler.HANDLER_NAME;
    	
		// Dummy params
		String PARAM_1 = 'ABCD123';	
  		
		// Data Setup
		createASRCustomSettings(HANDLER_NAME);
		
		// Create asyncReq
		Async_Request__c asyncReq1 = DF_TestData.createAsyncRequest(HANDLER_NAME, 'Pending', PARAM_1, null, null, 0, 0);
		insert asyncReq1;  					

   		// Generate mock response
		String response = '{"Ack":"true"}';
 
		// Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
        
        test.startTest();
        
        DF_SOACreateBillingEventHandler handler = new DF_SOACreateBillingEventHandler();
        handler.run(null);
        
        test.stopTest();
    }

    @isTest 
    static void test_execute_constructor_type_1_withDummyHandler() {    
    	/** Data setup **/	
    	List<Async_Request__c> asyncReqList;
    	
    	String HANDLER_NAME = 'DummyHandlerC1';
    	
		// Dummy params
		String PARAM_1 = 'ABCD123';
  		
		// Data Setup
		createASRCustomSettings(HANDLER_NAME);
		
		// Create asyncReq
		Async_Request__c asyncReq1 = DF_TestData.createAsyncRequest(HANDLER_NAME, 'Pending', PARAM_1, null, null, 0, 0);
		insert asyncReq1;  							

   		// Generate mock response
		String response = '{"Ack":"true"}';
 
		// Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
        
        test.startTest();
        
        DummyHandlerC1 handler = new DummyHandlerC1();
        handler.run(null);
        
        test.stopTest();
    }

    @isTest 
    static void test_execute_constructor_type_2_withDummyHandler() {    	
    	/** Data setup **/
    	List<Async_Request__c> asyncReqList;
    	
    	String HANDLER_NAME = 'DummyHandlerC2';
    	
		// Dummy params
		String PARAM_1 = 'ABCD123';
  		
		// Data Setup
		createASRCustomSettings(HANDLER_NAME);
		
		// Create asyncReq
		Async_Request__c asyncReq1 = DF_TestData.createAsyncRequest(HANDLER_NAME, 'Pending', PARAM_1, null, null, 0, 0);
		insert asyncReq1;  							

   		// Generate mock response
		String response = '{"Ack":"true"}';
 
		// Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
        
        test.startTest();
        
        DummyHandlerC2 handler = new DummyHandlerC2();
        handler.run(null);
        
        test.stopTest();
    }

    @isTest 
    static void test_execute_constructor_type_3_withDummyHandler() {    	
    	/** Data setup **/
    	List<Async_Request__c> asyncReqList;
    	
    	String HANDLER_NAME = 'DummyHandlerC3';
    	
		// Dummy params
		String PARAM_1 = 'ABCD123';
  		
		// Data Setup
		createASRCustomSettings(HANDLER_NAME);
		
		// Create asyncReq
		Async_Request__c asyncReq1 = DF_TestData.createAsyncRequest(HANDLER_NAME, 'Pending', PARAM_1, null, null, 0, 0);
		insert asyncReq1;  						

   		// Generate mock response
		String response = '{"Ack":"true"}';
 
		// Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
        
        test.startTest();
        
        DummyHandlerC3 handler = new DummyHandlerC3();
        handler.run(null);
        
        test.stopTest();
    }

    private static void createASRCustomSettings(String handler) {  
		Async_Request_Settings__c asrSettings = new Async_Request_Settings__c();
    	              
        try {	        
	        asrSettings.Name = handler;
	        asrSettings.Max_No_of_Rows__c = 1;
	        asrSettings.Max_No_of_Parallels__c = 50;
	        insert asrSettings;
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }  
    }    

	/** Data Creation **/
    @testSetup static void setup() {
    	try {
    		createASRConfigCustomSettings();  				        
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }     	
    }
    
    public static void createASRConfigCustomSettings() {                
        try {
	        Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
			asrConfigSettings.Max_No_of_Future_Calls__c = 50;
			asrConfigSettings.Max_No_of_Parallels__c = 10;
			asrConfigSettings.Max_No_of_Retries__c = 3;
			asrConfigSettings.Max_No_of_Rows__c = 1;			
	        upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }                
    }  

    // Sub Class 1 - to test constructor 1
	public class DummyHandlerC1 extends AsyncQueueableHandler {

	    public DummyHandlerC1() {    	
	        super('DummyHandlerC1');
	    }

	    public override void executeWork(List<String> paramsList) {	    	
	        invokeProcess(paramsList);
	    }    
	
	    private void invokeProcess(List<String> paramsList) {	
			DF_SOABillingEventService.createBillingEvent(paramsList[0]);
	    }
	}

    // Sub Class 2 - to test constructor 2
	public class DummyHandlerC2 extends AsyncQueueableHandler {

	    public DummyHandlerC2() {    	
	        super('DummyHandlerC2', 1);
	    }
	
	    public override void executeWork(List<String> paramsList) {	    	
	        invokeProcess(paramsList);
	    }    
	
	    private void invokeProcess(List<String> paramsList) {	
			DF_SOABillingEventService.createBillingEvent(paramsList[0]);
	    }
	}
    
    // Sub Class 3 - to test constructor 3
	public class DummyHandlerC3 extends AsyncQueueableHandler {

	    public DummyHandlerC3() {    	
	        super('DummyHandlerC3', 1, 2);
	    }
	
	    public override void executeWork(List<String> paramsList) {	    	
	        invokeProcess(paramsList);
	    }    
	
	    private void invokeProcess(List<String> paramsList) {	
			DF_SOABillingEventService.createBillingEvent(paramsList[0]);
	    }
	}   
     
}