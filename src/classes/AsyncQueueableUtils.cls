public class AsyncQueueableUtils {
    
    public static final String PENDING = 'Pending';
    public static final String PROCESSED = 'Processed';
    public static final String QUEUED = 'Queued';
    public static final String PROCESSING = 'Processing';
    public static final String PREPARING = 'Preparing';
    public static final Integer NUM_MINUTES = -60;

    // Errors
    public static final String ERR_INVALID_PARAMS = 'Invalid number of params supplied';    

    public static void createRequests(String handlerName, List<String> paramList) {     
        system.debug('AsyncQueueableUtils.createRequests - handlerName: ' + handlerName);
        system.debug('AsyncQueueableUtils.createRequests - paramList.size: ' + paramList.size());                                                       

        Map<String, Async_Request__c> existingAsyncReqMap = new Map<String, Async_Request__c>();        
        List<Async_Request__c> existingAsyncReqList;
        List<Async_Request__c> newAsyncReqList = new List<Async_Request__c>();

        try {
            existingAsyncReqList = [SELECT Params__c 
                                    FROM   Async_Request__c 
                                    WHERE  Params__c IN :paramList
                                    AND    Handler__c = :handlerName 
                                    AND    Status__c = :AsyncQueueableUtils.PENDING];

            system.debug('AsyncQueueableUtils.createRequests - existingAsyncReqList.size: ' + existingAsyncReqList.size());
                                                        
            for (Async_Request__c existingAsyncReq : existingAsyncReqList) {
                existingAsyncReqMap.put(existingAsyncReq.params__c, existingAsyncReq);
            }
                                
            for (String param : paramList) {                
                if (!existingAsyncReqMap.containsKey(param)) {                  
                    Async_Request__c newAsyncReq = new Async_Request__c();
                    newAsyncReq.Params__c = param;
                    newAsyncReq.Handler__c = handlerName;
                    newAsyncReq.Status__c = AsyncQueueableUtils.PENDING;
                    newAsyncReqList.add(newAsyncReq);
                }
            }           
            
            system.debug('AsyncQueueableUtils.createRequests - newAsyncReqList.size: ' + newAsyncReqList.size());
            
            if (!newAsyncReqList.isEmpty()) {               
                insert newAsyncReqList;

                // Only enqueue job if AsyncReqs have been created for current txn              
                AsyncQueueableUtils.enqueueJob(handlerName, null);                                  
            }           
        } catch (Exception e) {
            //throw new CustomException(e.getMessage());
            GlobalUtility.logMessage(DF_Constants.ERROR, AsyncQueueableUtils.class.getName(), 'createRequests', '', '', '', '', e, 0);
        } 
    } 

    public static Boolean enqueueJob(String handlerName, Id existingJobId) {
        system.debug('AsyncQueueableUtils.enqueueJob - handlerName: '+ handlerName);
        system.debug('AsyncQueueableUtils.enqueueJob - existingJobId: ' + existingJobId);       
         
        Boolean jobEnqueued = false;

        try {   
            system.debug('AsyncQueueableUtils.enqueueJob - Queued Already: ' + Limits.getQueueableJobs());
            system.debug('AsyncQueueableUtils.enqueueJob - Allowed to Queue: ' + Limits.getLimitQueueableJobs());
            
            if (!isAlreadyRunning(handlerName, existingJobId) && (Limits.getQueueableJobs() < Limits.getLimitQueueableJobs())) {                
                /***                
                    Add all handlers here.                
                ***/
                
                if (handlerName.equalsIgnoreCase(DF_SOACreateBillingEventHandler.HANDLER_NAME)) {                   
                    Id jobId = system.enqueueJob(new DF_SOACreateBillingEventHandler());
                    
                    if (jobId != null) {
                        jobEnqueued = true;
                    }
                }
                
                if (handlerName.equalsIgnoreCase(DF_LAPISearchByLocationHandler.HANDLER_NAME)) {                    
                    Id jobId = system.enqueueJob(new DF_LAPISearchByLocationHandler());
                    
                    if (jobId != null) {
                        jobEnqueued = true;
                    }
                }
                
                if (handlerName.equalsIgnoreCase(DF_LAPISearchByLatLongHandler.HANDLER_NAME)) {                 
                    Id jobId = system.enqueueJob(new DF_LAPISearchByLatLongHandler());
                    
                    if (jobId != null) {
                        jobEnqueued = true;
                    }
                }
                
                if (handlerName.equalsIgnoreCase(DF_LAPISearchByAddressHandler.HANDLER_NAME)) {                 
                    Id jobId = system.enqueueJob(new DF_LAPISearchByAddressHandler());
                    
                    if (jobId != null) {
                        jobEnqueued = true;
                    }
                }
                
                if (handlerName.equalsIgnoreCase(DF_AppianOrderSubmitHandler.HANDLER_NAME)) {                 
                    Id jobId = system.enqueueJob(new DF_AppianOrderSubmitHandler());
                    
                    if (jobId != null) {
                        jobEnqueued = true;
                    }
                }
                
                if (handlerName.equalsIgnoreCase(DF_AppianSFRequestHandler.HANDLER_NAME)) {                 
                    Id jobId = system.enqueueJob(new DF_AppianSFRequestHandler());
                    
                    if (jobId != null) {
                        jobEnqueued = true;
                    }
                }
				
				if (handlerName.equalsIgnoreCase(EE_AS_SvCacheHandler.HANDLER_NAME)) {                 
                    Id jobId = system.enqueueJob(new EE_AS_SvCacheHandler());
                    
                    if (jobId != null) {
                        jobEnqueued = true;
                    }
                }

                if (handlerName.equalsIgnoreCase(EE_CISLocationSearchHandler.HANDLER_NAME)) {
                    Id jobId = system.enqueueJob(new EE_CISLocationSearchHandler());
                    
                    if (jobId != null) {
                        jobEnqueued = true;
                    }
                }

                if (handlerName.equalsIgnoreCase(EE_PNIDistanceHandler.HANDLER_NAME)) {
                    Id jobId = system.enqueueJob(new EE_PNIDistanceHandler());
                    
                    if (jobId != null) {
                        jobEnqueued = true;
                    }
                }

                if (handlerName.equalsIgnoreCase(DF_SF_BulkOrderHandler.HANDLER_NAME)) {
                    Id jobId = system.enqueueJob(new DF_SF_BulkOrderHandler());

                    if (jobId != null) {
                        jobEnqueued = true;
                    }
                }
                
                if (handlerName.equalsIgnoreCase(NS_SOACreateBillingEventHandler.HANDLER_NAME)) {
                    Id jobId = system.enqueueJob(new NS_SOACreateBillingEventHandler());

                    if (jobId != null) {
                        jobEnqueued = true;
                    }
                }
                
            }                                                         
            
            system.debug('AsyncQueueableUtils.enqueueJob - jobEnqueued: ' + jobEnqueued);                      
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }           
     
        return jobEnqueued;
    }

    public static Boolean isAnythingPendingToRun(String handlerName) {     
        system.debug('AsyncQueueableUtils.isAnythingPendingToRun - handlerName: '+ handlerName);                
                     
        List<Async_Request__c> asyncReqList;
        Boolean anythingPendingToRun = false;

        try {
            asyncReqList = [SELECT Id 
                            FROM   Async_Request__c 
                            WHERE  Handler__c = :handlerName 
                            AND    Status__c = :PENDING LIMIT 1];

            system.debug('AsyncQueueableUtils.isAnythingPendingToRun - asyncReqList.size: ' + asyncReqList.size());

            if (!asyncReqList.isEmpty()) { 
                anythingPendingToRun = true;
            }
            
            system.debug('AsyncQueueableUtils.isAnythingPendingToRun - anythingPendingToRun: ' + anythingPendingToRun);                       
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }       

        return anythingPendingToRun;
    }

    private static Boolean isAlreadyRunning(String handlerName, Id existingJobId) {                
        system.debug('AsyncQueueableUtils.isAlreadyRunning - existingJobId: ' + existingJobId);                                        
        system.debug('AsyncQueueableUtils.isAlreadyRunning - handlerName: '+ handlerName);      
          
        Set<String> activeApexJobStatesSet;
        List<AsyncApexJob> activeApexJobsList;

        Boolean isAlreadyRunning = false;
        
        try {
            activeApexJobStatesSet = new Set<String>{AsyncQueueableUtils.QUEUED, AsyncQueueableUtils.PROCESSING, AsyncQueueableUtils.PREPARING};
            
            activeApexJobsList = [SELECT ApexClass.Name
                                  FROM   AsyncApexJob
                                  WHERE  ApexClass.Name =: handlerName 
                                  AND    Status in :activeApexJobStatesSet
                                  AND    Id <> :existingJobId];                                

            system.debug('AsyncQueueableUtils.isAlreadyRunning - activeApexJobsList.size: ' + activeApexJobsList.size());
                                             
            if (!activeApexJobsList.isEmpty()) {                   
                isAlreadyRunning = true;
            }                           
            
            system.debug('AsyncQueueableUtils.isAlreadyRunning - isAlreadyRunning: ' + isAlreadyRunning);           
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }       
                
        return isAlreadyRunning;
    }
    
    public static void retry(String handlerName, List<String> paramList, String errorMessage) {        
        system.debug('AsyncQueueableUtils.retry - handlerName: ' + handlerName);
        system.debug('AsyncQueueableUtils.retry - retry paramList: ' + paramList);
        system.debug('AsyncQueueableUtils.retry - errorMessage: ' + errorMessage);      
                
        List<Async_Request__c> asyncReqList;
        
        final Integer MAX_RETRIES;
        
        try {
            Async_Request_Config_Settings__c asyncReqConfigSettings;
            
            // Use org defaults only
            asyncReqConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();  
            
            if (asyncReqConfigSettings != null) {
                MAX_RETRIES = asyncReqConfigSettings.Max_No_of_Retries__c.intValue();
            }
            
            system.debug('AsyncQueueableUtils.retry - MAX_RETRIES: ' + MAX_RETRIES);

            asyncReqList = [SELECT Status__c, 
                                   No_of_Retries__c
                            FROM   Async_Request__c 
                            WHERE  Handler__c = :handlerName 
                            AND    Status__c = :AsyncQueueableUtils.PROCESSED
                            AND    SystemModstamp > :Datetime.now().addMinutes(NUM_MINUTES)
                            AND    Params__c  IN :paramList
                            ORDER  BY CreatedDate ASC                      
                            LIMIT  1000];                       
            
            system.debug('AsyncQueueableUtils.retry - asyncReqList.size: ' + asyncReqList.size());
            
            if (!asyncReqList.isEmpty()) {                         
                for (Async_Request__c asyncReq : asyncReqList) {                    
                    system.debug('AsyncQueueableUtils.retry - asyncReq.No_of_Retries__c: ' + asyncReq.No_of_Retries__c);
                    system.debug('AsyncQueueableUtils.retry - asyncReq.Status__c: ' + asyncReq.Status__c);
                    
                    if (String.isNotEmpty(errorMessage)) {
                        asyncReq.Error_Message__c = errorMessage;   
                    }
                    
                    if (asyncReq.No_of_Retries__c < MAX_RETRIES) {                      
                        asyncReq.Status__c = AsyncQueueableUtils.PENDING;
                        asyncReq.No_of_Retries__c++;
    
                        system.debug('AsyncQueueableUtils.retry - asyncReq.Status__c: ' + asyncReq.Status__c);
                        system.debug('AsyncQueueableUtils.retry - asyncReq.No_of_Retries__c: ' + asyncReq.No_of_Retries__c);
                    } else {
                        asyncReq.Status__c = DF_Constants.ERROR;
                    }
                }
                
                update asyncReqList;
                                
                Id dummyId = null;
                
                enqueueJob(handlerName, dummyId); 
            }           
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }    
    }    
          
}