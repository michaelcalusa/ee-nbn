@isTest
private class AsyncQueueableUtils_Test {

    @isTest static void test_createRequests() {         
        /** Data setup **/
		// Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
		dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
		insert dfQuote;
	
		// Create DF Order
		DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
		dfOrder.Site_Survey_Charges_JSON__c = '{"field":"value"}';
		insert dfOrder;	
    	   
		final String DELIMITER = ',';
		final String CHARGE_TYPE = DF_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY;
		String param = dfOrder.Id + DELIMITER + CHARGE_TYPE;        
        
		final String HANDLER_NAME = DF_SOACreateBillingEventHandler.HANDLER_NAME;

		List<String> paramList = new List<String>();
		paramList.add(param);
  
        test.startTest(); 	

		AsyncQueueableUtils.createRequests(HANDLER_NAME, paramList);
		// Run again to test for existing asr with same params
		AsyncQueueableUtils.createRequests(HANDLER_NAME, paramList);		

        test.stopTest();   

        // Assertions
        List<Async_Request__c> asyncReqList = new List<Async_Request__c>();
        asyncReqList = [SELECT Id
                        FROM   Async_Request__c];

        system.AssertEquals(false, asyncReqList.isEmpty());  
    }

    @isTest static void test_retry() {         
        /** Data setup **/
		// Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
		dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
		insert dfQuote;
	
		// Create DF Order
		DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
		dfOrder.Site_Survey_Charges_JSON__c = '{"field":"value"}';
		insert dfOrder;	
    	   
		final String DELIMITER = ',';
		final String CHARGE_TYPE = DF_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY;
		String param = dfOrder.Id + DELIMITER + CHARGE_TYPE;        
        
		final String HANDLER_NAME = DF_SOACreateBillingEventHandler.HANDLER_NAME;
		final String ERROR_MSG = 'A HTTP error occurred in class method DF_SOABillingEventService.createBillingEvent';

		List<String> paramList = new List<String>();
		paramList.add(param);

        test.startTest(); 	

		AsyncQueueableUtils.retry(HANDLER_NAME, paramList, ERROR_MSG);			

        test.stopTest();   

        // Assertions

    }
  
    @isTest static void test_isAnythingPendingToRun() {         
        /** Data setup **/
		// Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
		dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
		insert dfQuote;
	
		// Create DF Order
		DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
		dfOrder.Site_Survey_Charges_JSON__c = '{"field":"value"}';
		insert dfOrder;	
    	   
		final String DELIMITER = ',';
		final String CHARGE_TYPE = DF_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY;
		String param = dfOrder.Id + DELIMITER + CHARGE_TYPE;        
        
		final String HANDLER_NAME = DF_SOACreateBillingEventHandler.HANDLER_NAME;

		// Create pending asyncReq
		Async_Request__c asyncReq = DF_TestData.createAsyncRequest(HANDLER_NAME, 'Pending', param, null, null, null, null);
		insert asyncReq;

		Boolean anythingPendingToRun = false;

        test.startTest(); 	

		anythingPendingToRun = AsyncQueueableUtils.isAnythingPendingToRun(HANDLER_NAME);			

        test.stopTest();   

        // Assertions
		system.AssertEquals(true, anythingPendingToRun); 
    }

    @isTest static void test_enqueueJob() {
        Boolean queued = AsyncQueueableUtils.enqueueJob(NS_SOACreateBillingEventHandler.HANDLER_NAME, null);
        Assert.equals(true, queued);
    }

	/** Data Creation **/
    @testSetup static void setup() {
    	try {
    		createCustomSettings();
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    public static void createCustomSettings() {
        try {
	        Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
			asrConfigSettings.Max_No_of_Future_Calls__c = 50;
			asrConfigSettings.Max_No_of_Parallels__c = 10;
			asrConfigSettings.Max_No_of_Retries__c = 3;
			asrConfigSettings.Max_No_of_Rows__c = 1;
	        upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

}