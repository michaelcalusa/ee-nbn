global class AsyncRequestUtils {

    public static final String ERR_INVALID_HANDLER = 'Invalid Async Request Handler';
    public static final String ERR_ASR_NOT_FOUND = 'No related Async Request records were found';   
    public static final String ERR_ASRS_NOT_PROVIDED_FOR_PROCESSING = 'No related Async Request records were provided for processing';

    WebService static void resubmit(String asyncReqId) {        
        List<Async_Request__c> asyncReqList;
        
        Async_Request__c asyncReq;
        String handler;
        Decimal noOfResubmissions = 0;

        try {
            asyncReqList = [SELECT Handler__c,
                                   No_of_Resubmissions__c
                            FROM   Async_Request__c 
                            WHERE  Id = :asyncReqId];

            if (!asyncReqList.isEmpty()) {              
                asyncReq = asyncReqList[0];
                
                if (asyncReq.No_of_Resubmissions__c != null) {
                    noOfResubmissions = asyncReq.No_of_Resubmissions__c + 1;
                } else {
                    noOfResubmissions++;
                }

                // Update field values
                asyncReq.Status__c = AsyncQueueableUtils.PENDING;
                asyncReq.No_of_Retries__c = 0;
                asyncReq.Error_Message__c = '';
                asyncReq.No_of_Resubmissions__c = noOfResubmissions;
                update asyncReq;

                handler = asyncReq.Handler__c;

                if (String.isNotEmpty(handler)) {
                    // Enqueue handler job again (if required)
                    AsyncQueueableUtils.enqueueJob(handler, null);
                } else {
                    throw new CustomException(AsyncRequestUtils.ERR_INVALID_HANDLER);
                }
            } else {
                throw new CustomException(AsyncRequestUtils.ERR_ASR_NOT_FOUND);
            }
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }  
    }

    WebService static void resubmitBulk(List<String> asyncReqIdList) {      
        List<Async_Request__c> asyncReqList;
        List<Async_Request__c> asyncReqToUpdateList = new List<Async_Request__c>();
        Set<String> handlerSet = new Set<String>();

        try {
            asyncReqList = [SELECT Handler__c,
                                   No_of_Resubmissions__c
                            FROM   Async_Request__c 
                            WHERE  Id IN :asyncReqIdList];

            if (!asyncReqList.isEmpty()) {              
                for (Async_Request__c asyncReq : asyncReqList) {                                        
                    Decimal noOfResubmissions = 0;
                    String handler;
                    
                    if (asyncReq.No_of_Resubmissions__c != null) {
                        noOfResubmissions = asyncReq.No_of_Resubmissions__c + 1;
                    } else {
                        noOfResubmissions++;
                    }
    
                    // Update field values
                    asyncReq.Status__c = AsyncQueueableUtils.PENDING;
                    asyncReq.No_of_Retries__c = 0;
                    asyncReq.Error_Message__c = '';
                    asyncReq.No_of_Resubmissions__c = noOfResubmissions;
                    
                    asyncReqToUpdateList.add(asyncReq); 
                    handler = asyncReq.Handler__c;

                    if (String.isNotEmpty(handler)) {                           
                        handlerSet.add(handler);                        
                    }
                }   

                if (!asyncReqToUpdateList.isEmpty()) {                  
                    update asyncReqToUpdateList;
                    
                    // Loop thru all handlers and submit job for each
                    if (!handlerSet.isEmpty()) {                        
                        for (String handler : handlerSet) {                                             
                            // Enqueue handler job again (if required)
                            AsyncQueueableUtils.enqueueJob(handler, null);                  
                        }   
                    }                   
                }
            } else {
                throw new CustomException(AsyncRequestUtils.ERR_ASRS_NOT_PROVIDED_FOR_PROCESSING);
            }
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }  
    }

}