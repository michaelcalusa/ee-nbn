@isTest
private class AsyncRequestUtils_Test {

    @isTest static void test_resubmit() {         
        /** Data setup **/
        Decimal beforetestNoOfResubmissions = 0;
        Decimal aftertestNoOfResubmissions = 0;

		// Create asyncReq
		Async_Request__c asyncReq = DF_TestData.createAsyncRequest(DF_SOACreateBillingEventHandler.HANDLER_NAME, 'Error', '123456789', null, null, 0, 0);
		insert asyncReq;        

  		beforetestNoOfResubmissions = asyncReq.No_of_Resubmissions__c;
  
        test.startTest(); 	

		if (asyncReq != null) {
			// Test 1 - test with No_of_Resubmissions__c != null
        	AsyncRequestUtils.resubmit(String.valueOf(asyncReq.Id));

			// Test 2 - test with No_of_Resubmissions__c = null
			// Prepare data
			asyncReq.No_of_Resubmissions__c = null;
			update asyncReq;

        	AsyncRequestUtils.resubmit(String.valueOf(asyncReq.Id));
		}

        test.stopTest();   

        // Assertions
        aftertestNoOfResubmissions = [SELECT No_of_Resubmissions__c
                                 	  FROM   Async_Request__c 
                                 	  WHERE  Id = :asyncReq.Id LIMIT 1].No_of_Resubmissions__c;

        system.assert(aftertestNoOfResubmissions > beforetestNoOfResubmissions, 'aftertestNoOfResubmissions should be greater than beforetestNoOfResubmissions after resubmit is performed');
    }

    @isTest static void test_resubmit_withNoASRsFoundError() {         
        /** Data setup **/
        test.startTest(); 	

		try {
			AsyncRequestUtils.resubmit('abcd1234');   	
        } catch (Exception e) {
            // suppress error         
        }

        test.stopTest();   

        // Assertions
        List<Async_Request__c> asyncReqList = new List<Async_Request__c>();
        asyncReqList = [SELECT Id
                        FROM   Async_Request__c];

        system.AssertEquals(true, asyncReqList.isEmpty());    
    }

    @isTest static void test_resubmitBulk() {         
        /** Data setup **/
		// Create asyncReq
		Async_Request__c asyncReq1 = DF_TestData.createAsyncRequest(DF_SOACreateBillingEventHandler.HANDLER_NAME, 'Error', '123456789', null, null, 0, 0);
		insert asyncReq1;
		        
		Async_Request__c asyncReq2 = DF_TestData.createAsyncRequest(DF_SOACreateBillingEventHandler.HANDLER_NAME, 'Error', '123456789', null, null, 0, null);
		insert asyncReq2;        

		List<String> asyncReqIdList = new List<String>();
		asyncReqIdList.add(asyncReq1.Id);
		asyncReqIdList.add(asyncReq2.Id);
  
        test.startTest(); 	

		if (!asyncReqIdList.isEmpty()) {
        	AsyncRequestUtils.resubmitBulk(asyncReqIdList);
		}

        test.stopTest();   

        // Assertions
        List<Async_Request__c> asyncReqList = new List<Async_Request__c>();
        asyncReqList = [SELECT Id
                        FROM   Async_Request__c 
                        WHERE  Status__c = 'Error'];

        system.AssertEquals(true, asyncReqList.isEmpty());    
    }

    @isTest static void test_resubmitBulk_withNoASRsProvidedError() {         
        /** Data setup **/
        List<String> asyncReqIdList = new List<String>();
        
        test.startTest(); 	

		try {
			AsyncRequestUtils.resubmitBulk(asyncReqIdList);
        } catch (Exception e) {
            // suppress error         
        }

        test.stopTest();   

        // Assertions
        List<Async_Request__c> asyncReqList = new List<Async_Request__c>();
        asyncReqList = [SELECT Id
                        FROM   Async_Request__c];

        system.AssertEquals(true, asyncReqList.isEmpty()); 
    }

	/** Data Creation **/
    @testSetup static void setup() {
    	try {
    		createCustomSettings();  				        
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }     	
    }
    
    public static void createCustomSettings() {                
        try {
	        Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
			asrConfigSettings.Max_No_of_Future_Calls__c = 50;
			asrConfigSettings.Max_No_of_Parallels__c = 10;
			asrConfigSettings.Max_No_of_Retries__c = 3;
			asrConfigSettings.Max_No_of_Rows__c = 1;			
	        upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }                
    }  
     
}