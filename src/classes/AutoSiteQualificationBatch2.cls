/*------------------------------------------------------------
Author:        Ganesh Sawant
Company:       Cognizant
Description:   Apex batch job which will re-qualify Unverified Sites created as part of RSP/Aged Orders Migrations. The Site failed to re-qualify
                On first Attempt will be picke up again based on the Requalification Faild Date and value of Retry Days stored in the Custom Setting
                Number of records to be processed in each batch can be re configurable and can be set in Auto Requalify Limit in Custom Setting Customer Service Setting
Test Class:    SiteQualification_CX_Test
------------------------------------------------------------*/ 
global class AutoSiteQualificationBatch2 implements Database.batchable<sObject>, Database.AllowsCallouts, Database.Stateful{

   Customer_Service_Setting__c css = Customer_Service_Setting__c.getOrgDefaults();

   global Database.QueryLocator start(Database.BatchableContext BC){
      list<AsyncApexJob> currentJobList = [Select id, CreatedDate, CompletedDate From AsyncApexJob Where ApexClassId=:css.SiteQualificationClassID__c AND Status='Processing'];	  
      Id UVRecTypeID = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId();
   	  string query =  'SELECT Id, Name,Serviceability_Class__c, Rollout_Type__c, Technology_Type__c,'+
                      'Location_Id__c, Site_Address__c,Qualification_Date__c,RecordTypeId, Unit_Number__c,'+
                      'Road_Name__c, Level_Number__c, Lot_Number__c, Road_Number_1__c, Road_Number_2__c, Road_Suffix_Code__c, Road_Type_Code__c, Post_Code__c,'+ 
                      'Locality_Name__c, State_Territory_Code__c, Requalification_Failed_Date__c, CreatedDate, Require_Qualification__c FROM Site__c '+
                      'WHERE Location_Id__c!=NULL AND Require_Qualification__c=TRUE AND'+
                      ' (Requalification_Failed_Date__c= NULL OR Requalification_Failed_Date__c < LAST_N_DAYS:'+css.Auto_Requalify_Retry_days__c+') ORDER BY CreatedDate DESC  LIMIT '+css.Auto_Requalify_limit_2__c;

      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<site__c> scope){
   	list<AsyncApexJob> currentJobList = [Select id, CreatedDate, CompletedDate From AsyncApexJob Where ApexClassId=:css.SiteQualificationClassID__c AND Status='Processing'];
     for(site__c s : scope){
     	SiteQualification_CX SiteQualification_CX_Con = new SiteQualification_CX(new ApexPages.StandardController(s));
		SiteQualification_CX_Con.reQualifySiteRecord(); 
       if(!currentJobList.isEmpty() && (datetime.now().getTime() - currentJobList[0].CreatedDate.getTime())/(1000*60) > css.SiteJobAbortLimitInMins__c ) {         
           System.debug('CSS Setting'+css.SiteJobAbortLimitInMins__c);             
           for(AsyncApexJob a : currentJobList) {
           	System.AbortJob(a.Id);   
           }        		  
       }
     }
     //update scope;
   }

   global void finish(Database.BatchableContext BC){
                 
   }  
}