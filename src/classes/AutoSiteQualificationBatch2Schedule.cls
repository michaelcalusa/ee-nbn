/*------------------------------------------------------------
Author:        Ganesh Sawant
Company:       Cognizant
Description:   Apex schedule job that will execute AutoSiteQualification_Batch.
Test Class:    TBA
------------------------------------------------------------*/

public class AutoSiteQualificationBatch2Schedule  implements Schedulable{

    public static void execute(SchedulableContext ctx)
    {
        Database.executeBatch(new AutoSiteQualificationBatch2(),1);
    }
}