/***************************************************************************************************
    Class Name          : BJ_CommunityThemes
    Version             : 1.0 
    Created Date        : 08-Sep-2017
    Author              : Rupendra Kumar Vats
    Description         : Logic to call Website page and get the Header and Footer 

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats       08-Sep-2017       Logic to call Website page and get the Header and Footer
****************************************************************************************************/
public class BJ_CommunityThemes implements Database.Batchable<sObject>,Database.AllowsCallouts{

    public Database.QueryLocator start(Database.BatchableContext BC){
        string strQuery = 'SELECT id, Header__c, Footer__c FROM nbnCommunityThemes__c Limit 1';
        return Database.getQueryLocator(strQuery);
    }
    
    public void execute(Database.BatchableContext BC, List<nbnCommunityThemes__c> lstCommTheme){
        try{
            if(!lstCommTheme.isEmpty()){
                nbnTopTailController ctlr = new nbnTopTailController();
                
                String strHeader = ctlr.getnbnHeader();
                system.debug('---header---' + strHeader);
                
                String strFooter = ctlr.getnbnFooter();
                system.debug('---footer---' + strFooter);
            
                nbnCommunityThemes__c theme = new nbnCommunityThemes__c(Id = lstCommTheme[0].ID);
                theme.Header__c = strHeader;
                theme.Footer__c = strFooter;
                
                update theme;
            }
        }Catch(Exception ex){
            system.debug('----BJ_CommunityThemes----' + ex.getMessage());
        }
    }
    
    public void finish(Database.BatchableContext BC){
    
    }
}