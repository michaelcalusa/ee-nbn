/***************************************************************************************************
    Class Name  : BJ_CommunityThemes_Test
    Class Type  : Test Class 
    Version     : 1.0 
    Created Date: 08-Sep-2017
    Function    : This class contains unit test scenarios for BJ_CommunityThemes apex class which handles website header and footer
    Used in     : None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            08/09/2017                 Created
****************************************************************************************************/
@isTest(seeAllData = false)
public class BJ_CommunityThemes_Test{
    static testMethod void TestMethod_executeBatch() {
        nbnCommunityThemes__c theme = new nbnCommunityThemes__c();
        theme.Header__c = 'test header';
        theme.Footer__c = 'test footer';
        
        insert theme;
        
        MockHttpResponseGenerator mockResp = new MockHttpResponseGenerator();
        mockResp.strResponseType = 'WebsiteHeader';
        Test.setMock(HttpCalloutMock.class, mockResp);
        
        Test.startTest();
        database.executeBatch(new BJ_CommunityThemes());
        Test.stopTest();
    }
}