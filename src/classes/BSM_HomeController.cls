/**
 * Created by gobindkhurana on 25/7/18.
 */

public class BSM_HomeController {

    public static String BizCommunityPermissions{get;set;}

    @AuraEnabled
    public static String getBizCommunityPermissions(){

        User u = new User();
        u = [select Has_EE_Community_Permissions__c, Has_NS_Community_Permissions__c, Has_NE_Community_Permissions__c from user where Id = :UserInfo.getUserId() limit 1];

        BizCommunityPermissions = '';

        if(u.Has_EE_Community_Permissions__c == true)
        {
            BizCommunityPermissions += 'Has_EE_Community_Permissions__c ';
        }
        
        if(u.Has_NS_Community_Permissions__c == true)
        {
            BizCommunityPermissions += 'Has_NS_Community_Permissions__c ';
        }
        if(GlobalUtility.getReleaseToggle('Release_Toggles__mdt','EnableNetworkExtension','IsActive__c') && u.Has_NE_Community_Permissions__c)
        {
            BizCommunityPermissions += 'Has_NE_Community_Permissions__c ';
        }
        return BizCommunityPermissions;
    }

}