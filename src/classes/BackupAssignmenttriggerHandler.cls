public Without sharing class BackupAssignmenttriggerHandler{
    
    Public Static Void CheckforSameState(list<Backup_Assignment__c > newbackupassignments, Map<ID,Backup_Assignment__c> Oldnewbackupassignments){
        
        
        // First Check if the Backup to state and From State are Same. 
        for (Backup_Assignment__c  BA24mCheck : newbackupassignments){
            
            system.debug(BA24mCheck.Backup_To__c);
            system.debug(BA24mCheck.Backup_From__r.State__C);
            
            if(BA24mCheck.Backup_To__c == BA24mCheck.Backup_From_State__C){
                
                BA24mCheck.adderror('Please ensure that the backup state is not the same as the parent state');
            }
            
        }
        
        // Check if the Back from state already exisist to Current Backup to State. 
        
        list<Backup_Assignment__c> existingbackupassignmets = [select Id,Backup_to__C,Backup_from__C, order__C from Backup_Assignment__c];
        Set<String> NewstateUniqueIDs = new Set<String> ();
        
        for(Backup_Assignment__c EBA : existingbackupassignmets)
        {
            NewstateUniqueIDs.add(EBA.Backup_From__c  + EBA.backup_to__C);
        }
        
        for(Backup_Assignment__c NBA : newbackupassignments ){
            
            String UniquestateIDSearch = NBA.Backup_from__C  + NBA.backup_to__C;
            
            system.debug('santoshdebug'+ UniquestateIDSearch );
            
            if(Oldnewbackupassignments == null){
                if(NewstateUniqueIDs .contains(UniquestateIDSearch)){
                    
                    string state = NBA.backup_to__C;
                    NBA.adderror('There\'s already Backup Assignment record to '+ NBA.Backup_from_State__C+ ' with State: '+ state  );
                    
                }
            }else{
                if(NBA.backup_to__C != Oldnewbackupassignments.get(NBA.ID).backup_to__C ){
                    
                    if(NewstateUniqueIDs .contains(UniquestateIDSearch)){
                        
                        string state = NBA.backup_to__C;
                        NBA.adderror('There\'s already Backup Assignment record to '+ NBA.Backup_from_State__C+ ' with State: '+ state  );
                        
                    }
                }
            }
        }
    }
    
    Public Static Void CheckforOrder(list<Backup_Assignment__c > newbackupassignments){
        
        list<Backup_Assignment__c> existingbackupassignmets = [select Id,Backup_from__C, order__C from Backup_Assignment__c];
        Set<String> NewUniqueIDs = new Set<String> ();
        
        for(Backup_Assignment__c EBA : existingbackupassignmets)
        {
            NewUniqueIDs.add(EBA.Backup_from__C  + string.valueof(EBA.order__C));
        }
        
        for(Backup_Assignment__c NBA : newbackupassignments ){
           
           if (NBA.order__C >1)
           {
            String UniqueIDSearchprevious = NBA.Backup_from__C  + string.valueof(NBA.order__C - 1);
            
           
            String UniqueIDSearchcurrent = NBA.Backup_from__C  + string.valueof(NBA.order__C );
            system.debug('santoshdebug'+ UniqueIDSearchprevious );
            
            if(NewUniqueIDs.contains(UniqueIDSearchprevious)){
                
                system.debug('santoshdebug '+ UniqueIDSearchprevious + ' found' );
                if(NewUniqueIDs.contains(UniqueIDSearchcurrent)){
                    Decimal currentorder = NBA.Order__C  ;
                    NBA.adderror('There\'s already Backup Assignment record to '+ NBA.Backup_from_State__C+ ' with Order number'+ currentorder );
                }
            }
            else{
                Decimal previousorder = NBA.Order__C -1 ;
                NBA.adderror('There\'s no Backup Assignment record to '+ NBA.Backup_from_State__C+ ' with Order number'+ previousorder );
            }
        }
        }
    }  
}