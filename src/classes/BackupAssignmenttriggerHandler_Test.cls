/***************************************************************************************************
Class Name:  BackupAssignmenttriggerHandler_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 03-11-2016
Function    : This class contains unit test scenarios for  BackupAssignmenttriggerHandler apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Hari Kalannagari         03-11-2016                Created
****************************************************************************************************/
@isTest
private class BackupAssignmenttriggerHandler_Test {
    static testMethod void CheckforSameStatetest(){
        BackupAssignmenttriggerHandler bath = new BackupAssignmenttriggerHandler();
        Secondary_Assignment__c Sa = new Secondary_Assignment__c();
        sa.State__c='Tsdjdgshd';
        insert sa;
        
        List<Backup_Assignment__c> ba = new List<Backup_Assignment__c>();
          Backup_Assignment__c bua = new Backup_Assignment__c(Backup_From__c=sa.Id, Order__c=1.0);
          //bua.Backup_From__c=sa.Id;
          //bua.Order__c=1.0;
          //bua.backup_to__C='ACT';
        insert bua;
        ba.add(bua);
        
        Map<ID,Backup_Assignment__c> mapba = new Map<ID,Backup_Assignment__c>();
        List<Backup_Assignment__c> mapbua1 = [select Id,Backup_To__c from Backup_Assignment__c];
        Backup_Assignment__c mapbua = new Backup_Assignment__c();
        mapbua.Backup_From__c=sa.Id;
        mapbua.Backup_To__c='NSW';
        mapbua.Order__c=1.0;
        insert mapbua;
        //mapba.add(mapbua.Id);
        for(Backup_Assignment__c p: mapbua1){
            mapba.put(p.Id, mapbua);
        }
        BackupAssignmenttriggerHandler.CheckforSameState(ba,mapba);
    }
    
    static testMethod void CheckforOrdertest(){
        BackupAssignmenttriggerHandler bath = new BackupAssignmenttriggerHandler();
        Secondary_Assignment__c Sa = new Secondary_Assignment__c();
        sa.State__c='Tsdjdgshd';
        insert sa;
        
        List<Backup_Assignment__c> ba = new List<Backup_Assignment__c>();
          Backup_Assignment__c bua = new Backup_Assignment__c();
          bua.Backup_From__c=sa.Id;
          bua.Order__c=4.0;
          //bua.backup_to__C='ACT';
        insert bua;
        ba.add(bua);
        
        Map<ID,Backup_Assignment__c> mapba = new Map<ID,Backup_Assignment__c>();
        
        Backup_Assignment__c mapbua = new Backup_Assignment__c();
        mapbua.Backup_From__c=sa.Id;
        mapbua.Backup_To__c='NSW';
        mapbua.Order__c=1.0;
        insert mapbua;
        //mapba.add(mapbua.Id);
        BackupAssignmenttriggerHandler.CheckforOrder(ba);
        
         mapbua.Order__c=2.0;
         update mapbua;
    }
    /*static testMethod void CheckforOrderiftest(){
        BackupAssignmenttriggerHandler bath = new BackupAssignmenttriggerHandler();
        Secondary_Assignment__c Sa = new Secondary_Assignment__c();
        sa.State__c='Tsdjdgshd';
        insert sa;
        
        List<Backup_Assignment__c> ba = new List<Backup_Assignment__c>();
          Backup_Assignment__c bua = new Backup_Assignment__c(Backup_From__c=sa.Id, Order__c=1.0);
          //bua.Backup_From__c=sa.Id;
          //bua.Order__c=1.0;
          //bua.backup_to__C='ACT';
        insert bua;
        ba.add(bua);
        
        Map<ID,Backup_Assignment__c> mapba = new Map<ID,Backup_Assignment__c>();
        
        Backup_Assignment__c mapbua = new Backup_Assignment__c();
        mapbua.Backup_From__c=sa.Id;
        mapbua.Backup_To__c='NSW';
        mapbua.Order__c=4.0;
        insert mapbua;
        //mapba.add(mapbua.Id);
        BackupAssignmenttriggerHandler.CheckforOrder(ba);
    }*/
}