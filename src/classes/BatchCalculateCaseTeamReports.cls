/*------------------------------------------------------------  
Author:        Andrew Zhang
Company:       NBNco
Description:   Batch class to calculate reports for multiple cases
Test Class:    CaseTeamReportUtil_Test
History
<Date>      <Authors Name>     <Brief Description of Change>
------------------------------------------------------------*/

global class BatchCalculateCaseTeamReports implements Schedulable, Database.Batchable<sObject> {

    global void execute(SchedulableContext sc) {
        String batch1Scope = 'RecordType.Name IN (\'Complex Complaint\', \'Complex Query\') AND Status IN (\'Open\', \'Re-Opened\', \'Open - Escalated\', \'Pending\') ';
        String batch2Scope = 'RecordType.Name IN (\'Formal Complaint\', \'Urgent Complaint\', \'Billing / New Development Enquiry\') AND Status IN (\'Open\', \'Re-Opened\', \'Open - Escalated\', \'Pending\') ';
        
        database.executeBatch(new BatchCalculateCaseTeamReports(batch1Scope, null), 1);
        database.executeBatch(new BatchCalculateCaseTeamReports(batch2Scope, null), 1);
    }
      
    global String scope; 
    global List<Id> ids; 

    global BatchCalculateCaseTeamReports() {
    }
        
    global BatchCalculateCaseTeamReports(String scope, List<Id> ids) {
        this.scope= scope;
        this.ids = ids;
    }
               
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if (scope != null) 
            return Database.getQueryLocator('SELECT Id FROM Case WHERE ' + scope);
            
        else if (ids != null)
            return Database.getQueryLocator('SELECT Id FROM Case WHERE Id IN :ids');
            
        else
            return Database.getQueryLocator('SELECT Id FROM Case WHERE Id = null');
    } 
    
    global void execute(Database.BatchableContext BC, List<Case> scope) {
        if (scope.size() == 1){
            Case c = scope[0];
            CaseTeamReportUtil.createReports(c.Id);
        }
    }

    global void finish(Database.BatchableContext BC) {
    }
}