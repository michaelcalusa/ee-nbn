////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*  Name    : BatchDeactivation
 *  Purpose : This is a batch class which calls DeactivateUsers method to deactivate  CFS users
 *  Author  : Nancy Gupta
 *  Date    : 2017-11-09
 *  Version : 1.0Database.executeBatch(new BatchDeactivation());
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
global class BatchDeactivation implements Database.Batchable < SObject > , Database.Stateful {
   // global Integer before;
  //  global Integer after;
    global List<User> before=new List <User> ();
     string fuser  = 'Active';
     public String query = 'SELECT Name, CFS_Activation_Date__c, Id '
                             +'From User '+ 
                             'WHERE IsActive = true '+
                             ' AND lmscons__CFS_Status__c =\''+ fuser + '\'';
    global Database.QueryLocator start(Database.BatchableContext BC) {
            return Database.getQueryLocator(query);
        }
    global void execute(Database.BatchableContext BC, List < User > records) {
        List<User> userList = new List<User>();
        before=records;
        if ((before.size() <= 1000) && (before.size() > 0) && (before != null)) {
           DeactivateCFSuser.RunLoop();
        } 
        else {
        }
        update userList;
    }
    global void finish(Database.BatchableContext BC) {
      
    }
}