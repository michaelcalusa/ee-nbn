global class BatchMARCaseContactUpdateJob implements Database.batchable<sObject>, Database.Stateful {
    global Database.QueryLocator start(Database.BatchableContext BC){
        //querying the record type IDs for the Case of type medical alarm
        id MARRecTypID = Schema.SObjectType.case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId();
        string query = 'Select Id, Secondary_Contact_Role__c, Secondary_Contact__c FROM Case WHERE RecordTypeId=\'' 
            + MARRecTypID + '\' AND Secondary_Contact__c != NULL';
        //System.debug(query);
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Case> scope){
       // System.debug('------scope'+scope);
        
        for(Case c : scope) {
            Map<String, Object> myMap = new Map<String, Object>();
            myMap.put('CaseId', c.Id);
            myMap.put('ContactId', c.Secondary_Contact__c);   
            myMap.put('Role', c.Secondary_Contact_Role__c);    
            myMap.put('Primary', false);  
            myMap.put('Secondary', true);   
            //System.debug(myMap);
            try {
                Flow.Interview  myFlow = new Flow.Interview.Upsert_Case_Role(myMap);
                myFlow.start();   
                
            } catch(Exception e){
                system.debug('e'+e.getMessage());
                GlobalUtility.logMessage('Error','BatchMARSecondaryContactUpdateJob','Execute','','',e.getMessage(),'',e,0);
            } 
            myMap.clear();
        }
        
    }
    global void finish(Database.BatchableContext BC){
        
    }
}