@isTest
public class BatchMARCaseContactUpdateJobTest {
    @isTest static void TestMARCaseJob() {
        list<Site__c> siteRecords = new list<Site__c>();
        Id rtypeId = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId();    
        for(integer counter = 1 ; counter <= 3 ; counter++){
            siteRecords.add( new Site__c ( Name = '123 Test Address, Sydney, NSW' + '-' + counter, RecordtypeId = rtypeId, Earliest_Disconnection_Date__c = System.today()));
        }
        insert siteRecords; 
        
        //Create Contacts records
        list<Contact> contactRecords = new list<Contact>();
        Id recordId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId();    
        for(Integer counter = 1 ; counter <= 4 ; counter++){
            contactRecords.add(new Contact(LastName = 'TestContact' + '-' + counter, RecordtypeId = recordId, Preferred_Phone__c= '1234567890', Email='test'+ counter + '@test.com'));
        }
        
        insert contactRecords; 
        //System.debug(contactRecords);
        
        // Create MAR cases
        list<Case> caseRecord = new list<Case>();        
        Id rId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId();    
        for(Integer counter = 1 ; counter <= 3 ; counter++){       
            caseRecord.add(new Case(Site__c = siteRecords[counter-1].Id,RecordtypeId=rId,Case_Type__c='Courtesy Call',
                                    Record_Provided_By__c='Testing MAR Case'+ counter,Date_Provided__c=Date.Today(),
                                    Copper_Active__c = 'Yes',NBN_Active__c='Yes',Inflight_Order__c='Yes',
                                    ContactId=contactRecords[counter-1].Id,Primary_Contact_Role__c='Alarm Owner',
                                    Secondary_Contact__c=contactRecords[counter].Id,Secondary_Contact_Role__c='Preferred Contact'));
        }    
        insert caseRecord;
        test.startTest();
        Database.executeBatch(new BatchMARCaseContactUpdateJob(),5);
        test.stopTest();
        
    }
    
}