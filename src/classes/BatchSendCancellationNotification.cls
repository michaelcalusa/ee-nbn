/*================================================
    * @Developer - Gagan Agarwal
    * @Class Name : BatchSendCancellationNotification
    * @Purpose: This is a batch class used to notify for invoices which have issued date before than 21 days
    * @created date: May 15,2018
    * @Last modified date:
    * @Last modified by : 
================================================*/

global class BatchSendCancellationNotification implements Database.Batchable<sObject> {
    public static final string INVOICE = 'Invoice';
    public static final string NEWDEVSCLASS3 ='NewDevs Class 3-4';
    
    /*================================================
    * @Purpose: Query to get invoices which have issued date 21 days ago and unpaid balance >0
    * @created date: May 15,2018
    ================================================*/
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Date issuedDate = system.today().addDays(-22);
        Set<String> inVoiceStatus = new Set<String>{'Created','Issued'};
        String query = 'Select id,Stage_Application__c,Stage_Application__r.Development__c,Remediation_PitPipe_Count__c,Issued_Date__c,Status__c,Unpaid_Balance__c,Transaction_Type__c,Promise_to_Pay_Date__c,Collections_on_Hold__c from Invoice__c where Status__c IN :inVoiceStatus and Stage_Application__c !=null and Remediation_PitPipe_Count__c= 0 and (Promise_to_Pay_Date__c = null OR Promise_to_Pay_Date__c < Today) and Issued_Date__c =:issuedDate and Unpaid_Balance__c != null and Unpaid_Balance__c > 0 and recordtype.DeveloperName=:INVOICE and Transaction_Type__c=:NEWDEVSCLASS3 and Collections_on_Hold__c = false';
                        
        return Database.getQueryLocator(query);
    }
    
    /*================================================
    * @Purpose: Execute Method to send the email to stage application contacts
    * @created date: May 15,2018
    ================================================*/
    global void execute(Database.BatchableContext BC, List<Invoice__c> scope)
    {
        Set<Id> stageAppIds = new set<Id>();
        List<Case> caseList = new List<Case>();
        List<StageApplication_Contact__c> stageContactList = new List<StageApplication_Contact__c>();
        Boolean CaseExists = false;
        Boolean InvoiceUnpaid = false;
        map<string,EmailTemplate> mapTempnameID = new map<string,EmailTemplate>();
        list<Messaging.SingleEmailMessage> lstSingleMail = new list<Messaging.SingleEmailMessage>();
        for(Invoice__c invoice : scope){
            if(invoice.Stage_Application__c != null){
                stageAppIds.add(invoice.Stage_Application__c);                      
            }
        }
        List<Stage_Application__c> stageAppList = new list<Stage_Application__c>();
        List<Id> stageAppListToRemove = new list<Id>();
        for(EmailTemplate objEmailTemplate : [Select Id, Name,Subject,HtmlValue, developerName,Folder.developername from EmailTemplate 
                                            where Folder.developername !=null]){
            mapTempnameID.put(objEmailTemplate.developerName,objEmailTemplate);
        }
        Messaging.SingleEmailMessage emails;
        stageAppList = [Select id,Application_Number__c,
                        (Select id,recordtype.developername,status,Phase__c,Sub_Category__c,Category__c,Stage_Application__c 
                        from Cases__r where Phase__c =:NewDev_Constants.CASEPHASE and Category__c=:NewDev_Constants.CASECATEGORY
                        and Sub_Category__c =:NewDev_Constants.CASESUBCATEGORY and status = 'Open' and recordtype.developerName = 'Billing_New_Development_Enquiry'),
                        (Select id,Name,Contact__c,Stage_Application__c,Contact__r.Email,Contact__r.IsEmailBounced,Roles__c 
                        from Stage_Application_Contacts__r where Contact__r.Email !=null and Contact__r.IsEmailBounced = false),
                        Application_ID__c,Primary_Contact_Name__c from Stage_Application__c where id in:stageAppIds];
                        
        OrgWideEmailAddress owe = [SELECT ID,IsAllowAllProfiles,DisplayName,Address FROM OrgWideEmailAddress WHERE Address= 'noreply@nbnco.com.au' LIMIT 1];                
        
        for(Stage_Application__c objstageApp:stageAppList){
            
            String Subject = mapTempnameID.get('Cancellation_Notice_After_21_Days_InvoiceDue_Date').subject;
            Subject = subject.replace('{!Stage_Application__c.Application_Number__c}',objstageApp.Application_Number__c);
            String Body = mapTempnameID.get('Cancellation_Notice_After_21_Days_InvoiceDue_Date').htmlValue;
            Body = Body.replace('{!Stage_Application__c.Primary_Contact_Name__c}',objstageApp.Primary_Contact_Name__c).replace('{!Stage_Application__c.Application_Number__c}',objstageApp.Application_Number__c);
            String PlainTextBody = mapTempnameID.get('Cancellation_Notice_After_21_Days_InvoiceDue_Date').htmlValue; 
            
            List<String> contactEmailsList = new List<String>();
            
            if(objstageApp.cases__r.isEmpty() && !objstageApp.Stage_Application_Contacts__r.isEmpty()){
                emails = new Messaging.SingleEmailMessage();
                emails.setOrgWideEmailAddressId(owe.id);
                emails.SetSubject(Subject);
                emails.setHtmlBody(Body);
                emails.setWhatId(objstageApp.id);
                emails.setSaveAsActivity(true);
                for(StageApplication_Contact__c cont :objstageApp.Stage_Application_Contacts__r){
                    contactEmailsList.add(cont.Contact__r.Email);
                }
                emails.setToAddresses(contactEmailsList);
                lstSingleMail.add(emails);
            }
        }
        if(lstSingleMail.size()>0){
           Messaging.SendEmail(lstSingleMail);  
        }
    }
    /*================================================
    * @Purpose:Finish Method
    * @created date: May 15,2018
    ================================================*/
    global void finish(Database.BatchableContext BC){
    
    }
}