@isTest
public class BatchSendCancellationNotification_Test {

    static testMethod void myTest1(){
        Account a = DCBTestDataUtil.initAccount();
        a.Customer_Class__c = NewDev_Constants.CLASS_1_2;
        insert a; 
        
        Contact c = DCBTestDataUtil.getNewContact();
        c.Email = 'abc@test.com';
        update c;
        
        Invoice_Split__c split = new Invoice_Split__c();
        split.name = '100/0';
        split.Description__c = '100';
        insert split;
        
        Development__c dev = DCBTestDataUtil.getNewDevelopment(a.Id,c.Id);
        
        Stage_Application__c stageApp = DCBTestDataUtil.initStageApplication(dev.Id);
        stageApp.Service_Delivery_Type__c = NewDev_Constants.SD1;
        stageApp.Dwelling_Type__c = NewDev_Constants.SDU;
        stageApp.Account__c = a.Id;
	stageApp.Application_Number__c = 'TestBatch';
        //stageApp.Fibre_TV__c = true;
        //stageApp.Essential_Services_Count__c = 50;
        //stageApp.Premise_Count__c = 50;
        //stageApp.FibreTV_Unit_Price__c = 240.00;
        stageApp.Invoice_Split__c = split.id;
        stageApp.Primary_Contact__c = c.id;
        insert stageApp;
        
        List<Invoice__c> invoiceList = new List<Invoice__c>();
        Opportunity testOpty= DCBTestDataUtil.getNewOpportunity(stageApp.Id);
        testOpty.Type ='Construction';
        update testOpty;
        
        List<ObjectCommonSettings__c> objSetting = new list<ObjectCommonSettings__c>();
        ObjectCommonSettings__c setting = new ObjectCommonSettings__c();
        setting.Requested__c = 'Issued';
        setting.Name = 'Invoice Statuses';
        insert setting; 
        
        ObjectCommonSettings__c setting1 = new ObjectCommonSettings__c();
        setting1.New__c = 'New';
        setting1.Name = 'Invoice Statuses';
        insert setting1;
        
        StageApplication_Contact__c devContact = new StageApplication_Contact__c ();
        devContact.Stage_Application__c =  stageApp.Id;
        devContact.Contact__c = c.id;
        insert devContact;   
         
        Id InvoiceRecordTypeId1 = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Invoice').getRecordTypeId();
        Invoice__c invoice1 = new Invoice__c();
        invoice1.name = NewDev_Constants.TOBEISSUED;
        invoice1.account__c = a.id;
        invoice1.Stage_Application__c = stageApp.id;
        invoice1.Opportunity__c = testOpty.id;
        //invoice1.Solution__c = sol.id;
        invoice1.Status__c =  'Created';
        invoice1.Transaction_Type__c = NewDev_Constants.NEWDEVSCLASS3;
        invoice1.recordtypeId = InvoiceRecordTypeId1 ;
        invoice1.Unpaid_Balance__c = 500;
        invoice1.Issued_Date__c = date.valueOf(system.today().addDays(-22));
        invoice1.Promise_to_Pay_Date__c = date.valueOf(system.today().addDays(-22));
        invoice1.Collections_on_Hold__c = false;
        insert invoice1;
        
        Test.startTest();
        BatchSendCancellationNotification obj = new BatchSendCancellationNotification();
        DataBase.executeBatch(obj);
        Test.stopTest();

    }

}