global class BatchSendCancellationSchedule implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        // Implement any logic to be scheduled
       
        // We now call the batch class to be scheduled
        BatchSendCancellationNotification  batchSchedule = new BatchSendCancellationNotification();
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(batchSchedule,10);
    }
   
}