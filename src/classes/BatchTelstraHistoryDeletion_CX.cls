/***************************************************************************************************
Class Name:         BatchTelstraHistoryDeletion_CX
Class Type:         Batch Class 
Version:            1.0 
Created Date:       13 October 2017
Function:           This batch class is to purge 18 months post creation of Telstra Technician private data 
Input Parameters:   None 
Output Parameters:  None
Description:        Purging of Telstra Technician private data  of 18 months post creation
Modification Log:
* Developer             Date             Description
* --------------------------------------------------------------------------------------------------                 
* Narasimha Binkam      13/10/2017      Created - Version 1.0 Refer CUSTSA-2564for Epic description
****************************************************************************************************/ 

global class BatchTelstraHistoryDeletion_CX implements Database.Batchable<sObject>
{   
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        Date oldDate =  date.today().addMonths(-18);
        return Database.getQueryLocator([SELECT id,Telstra_Tech_Contact_number__c,Telstra_Tech_ID__c,Telstra_Tech_name__c  FROM task WHERE Completed_Date__c <= :oldDate ]);
    } 
    
    global void execute(Database.BatchableContext BC, list<sObject> scope)
    {     
        delete scope;   
    }
    
    global void finish(Database.BatchableContext BC) 
    {                 
       
    }
}