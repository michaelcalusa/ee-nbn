/***************************************************************************************************
Class Name:         BatchTelstraHistoryDeletion_Test
Class Type:         Test Class 
Version:            1.0 
Created Date:       13 October 2017
Function:           This is a test class to cover batch class and a schedule class to purge 18 months post creation of Telstra Technician private data 
Input Parameters:   None 
Output Parameters:  None
Description:        Purging of Telstra Technician private data  of 18 months post creation
Modification Log:
* Developer            Date             Description
* --------------------------------------------------------------------------------------------------                 
* Narasimha Binkam      13/10/2017      Created - Version 1.0 Refer CUSTSA-2564 for Epic description
****************************************************************************************************/ 

@isTest
public class BatchTelstraHistoryDeletion_Test{
      static testMethod void batchApexTest() {
        
        Database.BatchableContext BC;
        BatchTelstraHistoryDeletion_CX batchTelstraHistoryDeleteObj = new BatchTelstraHistoryDeletion_CX();
        Database.QueryLocator QL = batchTelstraHistoryDeleteObj.start(bc);
        Database.QueryLocatorIterator QIT =  QL.iterator();
        List<Task> taskList = new List<Task>();
        while (QIT.hasNext()) {
            Task taskRecord = (Task)QIT.next();
            //System.debug(Acc);
            taskList.add(taskRecord);
        }         
        batchTelstraHistoryDeleteObj.execute(BC, taskList);
        batchTelstraHistoryDeleteObj.finish(BC);
        }
        
        static testMethod void scheduleTelstraHistoryDeletionTest() {
        Test.StartTest();
        ScheduleTelstraHistoryDeletion_CX.start();
        ScheduleTelstraHistoryDeletion_CX telstraHistoryDeletionObj = new ScheduleTelstraHistoryDeletion_CX();
        String sch = '0 0 0 25 11 ?';
        system.schedule('Test check', sch, telstraHistoryDeletionObj);
        Test.stopTest();
        }
        }