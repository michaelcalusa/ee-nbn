/***************************************************************************************************
    Class Name          : BillingFormController
    Version             : 1.0 
    Created Date        : 25-Dec-2017
    Author              : Dolly Yadav
    Description         : Create the cases and sends out the email for the logged in Community Contact
    Test Class          : BillingFormControllerTest

    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Dolly Yadav       
****************************************************************************************************/ 

public without sharing class BillingFormController {

    
    @AuraEnabled
    public static Case saveCase(Case cas,String recordType, String casetype) {
        try{
            System.debug('Method Parameters'+cas+recordType+casetype);
            // Perform isAccessible() and isUpdateable() checks here
            Id customerAccountRecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId();
            User currentUser = [Select Id, Name, profileId,Email, ContactId From User Where Id = :UserInfo.getUserId()];
            List<Contact> conList = [Select id,AccountId,Account.RecordtypeId from Contact where email =:currentUser.Email]; 
            System.Debug('@@@@@@@ContactList'+conList);
            cas.recordtypeid = schema.sobjecttype.Case.getrecordtypeinfosbyname().get(recordType).getRecordTypeId();
            cas.origin = 'Web';
            cas.type = casetype;
            cas.Form_Status__c = 'In Progress';
            //if(conList != null && conList.size() > 0 && conList[0].Id != null){
            if(!conList.isEmpty()) {
                for(Contact con : conList){   
                    //if(conList[0].AccountId != null && conList[0].Account.RecordtypeId == customerAccountRecordTypeId){
                    if(con.AccountId != null && con.Account.RecordtypeId == customerAccountRecordTypeId && con.ID == currentUser.ContactId) {
                        //cas.AccountId = conList[0].AccountId;
                        //cas.contactId = conList[0].Id;
                        cas.AccountId = con.AccountId;
                        cas.ContactId = con.Id;
                        System.debug('Account and Contact IDs'+cas.AccountId+cas.contactId);
                    }
                }
            }    
            system.debug('*************'+cas);
            /*************************Case Assignment Logic********************************/
            //Fetching the assignment rules on case
            AssignmentRule AR = new AssignmentRule();
            AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
            
            //Creating the DMLOptions for "Assign using active assignment rules" checkbox
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;       
            //Setting the DMLOption on Case instance
            cas.setOptions(dmlOpts);
            /***************************End of Case Assignment Logic***********************/
            insert cas;
            System.debug('*******Case Created*****'+cas);
            Case casIns = [select id,casenumber,subject, Description, ContactId, Account.Name, Amount__c from Case where id=:cas.id];
            return casIns;
        }
        catch(exception ex){
            system.debug( 'ex '+ex.getMessage()+' line '+ex.getStackTraceString());
            return null;
        }
        
    }
    
    
    @AuraEnabled
    public static map<String,map<string,string>> getRecordTypeTypesMap(){
        map<String,map<string,string>> recordTypeTypesMap = new map<String,map<string,string>>();
        
        Map<String, CaseRecordTypeTypeMapping__c> CaseRecordTypeTypeMap = CaseRecordTypeTypeMapping__c.getAll();
        system.debug('RecordTypeTypesMap********'+CaseRecordTypeTypeMap);
        
        Schema.DescribeFieldResult fieldResult = case.Type.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        system.debug('ple******'+ple);
        
        list<String>RecordsTypeList = Label.RSPrecordtypeorder.split(',');
        for (string rt: RecordsTypeList) {                   
            Map<string,string> pickListValues = new map<string,string>();
            set<string> picklistset = new set<String>();
            picklistset.addAll(CaseRecordTypeTypeMap.get(rt).Types__c.split(';'));
            system.debug('picklistset******'+picklistset);
            
            for( Schema.PicklistEntry f : ple){
                //if (CaseRecordTypeTypeMap.get(rt).Types__c.equalsIgnoreCase(f.getLabel())) {  
                if(picklistset.contains(f.getLabel())){             
                    pickListValues.put(f.getLabel(), f.getValue());               
                }
            }
            recordTypeTypesMap.put(rt,pickListValues);
        } 
        system.debug('RecordTypeTypesMap********'+recordTypeTypesMap);     
        return recordTypeTypesMap;
    }
    
    @AuraEnabled
    public static List<Map<string,string>> getTypeOptions(map<string,string> recordTypeTypesMaps){       
        List<Map<string,string>> pickListValues = new List<Map<string,string>>();
        for( string typeKey : recordTypeTypesMaps.keyset()){
            Map<string,string> entry = new Map<string, string>();
            entry.put(typeKey, recordTypeTypesMaps.get(typeKey));
            pickListValues.add(entry);
        }       
        return pickListValues;
    }
    
    
    @AuraEnabled
    public static user getUserDetails(){
        system.debug('Current Login User ********'+UserInfo.getName()); 
        user u = [select id, name, ContactID, IsPortalEnabled from user where id = :UserInfo.getUserId()];
        system.debug(u);
        return u;
    }
    
    @AuraEnabled
    public static void deleteCaseRec(case cs){
        try{
            
            delete cs;
        }
        catch(exception ex){
            system.debug(' message '+ex.getMessage()+' line '+ex.getStackTraceString());
        }
        
    }
    
    @AuraEnabled
    public static boolean checkContactRole(string ContactID) {
        system.debug('the contact id is '+ContactID);
        
        list<Team_member__c> contactMatrixCheck = [Select id From Team_member__c Where Role__c IN ('Escalations - Billing', 'Operational - Billing') AND Customer_Contact__c=:ContactID];
        system.debug('the contact matrix is '+contactMatrixCheck);
        if(!contactMatrixCheck.isEmpty()){
            system.debug('true');
            return true;
        }else{
            system.debug('false');            
            return false;
        }
        
    }
    
    /************************
    Author:        Jairaj Jadhav
    Company:       Wipro Technologies
    Description:   This method sends an email once Billing case is created and attachments are attached to it from CustomerPortalBillingForm.
    History
    <Date>      <Authors Name>     <Brief Description of Change>
    ************************/
    @AuraEnabled
    public static string sendBillingAckEmail(string contactID, string caseID){
        try{
            EmailTemplate et = [Select Id from EmailTemplate where name =: label.BillingAckEmailTemplate limit 1];
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: label.nbncopaymentsEmail limit 1];
            if(owea.size() > 0){
                message.setOrgWideEmailAddressId(owea.get(0).Id);
            }
            String[] sendingToCCAdd = new String[]{label.RSPE2Cemail};
            message.setccAddresses(sendingToCCAdd);
            message.setTargetObjectId(contactID);
            message.setWhatId(caseID);
            message.optOutPolicy = 'FILTER';
            message.settemplateid(et.id);
            Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
            if (results[0].success) {
                System.debug('xxxxxxxxxx The email was sent successfully.');
                return 'email sent';
            } else {
                System.debug('The email failed to send: ' + results[0].errors[0].message);
                return 'xxxxxxxxxx '+results[0].errors[0].message;
            }
        }
        catch(exception ex){
            system.debug('xxxxxxx message '+ex.getMessage()+' line number '+ex.getStackTraceString());
            return 'message '+ex.getMessage()+' line number '+ex.getStackTraceString();
        }
    }
    
    /************************
    Author:        Wayne Ni
    Description:   Update case form submit status to 'Submit' after clicking submit button
    History
    <Date>      <Authors Name>     <Brief Description of Change>
    ************************/
    @AuraEnabled
    public static string CaseDeletionCheck(Id Inputcaseid){
        String IfCaseExist;
        Case existingCase = [Select id,Form_Status__c from Case where id =: Inputcaseid Limit 1];   
        system.debug(existingCase);
        if(existingCase != null){
            IfCaseExist = 'true';
        }else if(existingCase == null){
            IfCaseExist = 'false';
        }
        system.debug(IfCaseExist);
     	return IfCaseExist;
    }
    
    @AuraEnabled
    public static boolean updateExistingCaseFormStatus(Id caseid){
        Boolean UpdateResult;
        Case existingCase = [Select id,Form_Status__c from Case where id =: caseid Limit 1];    
        if(existingCase != null){
            existingCase.Form_Status__c = 'Submitted';
        }
        
        Database.SaveResult insertResult = Database.update(existingCase);
        
        if(insertResult.isSuccess()){
            UpdateResult = true;
        }else{
            UpdateResult = false;
        }
        
        return UpdateResult;
    }
}