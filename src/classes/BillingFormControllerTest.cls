@isTest
private class BillingFormControllerTest {
     @isTest(SeeAllData=true) 
    static void TestMethod_RSPBilling () {
        String strConRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRecordTypeID = rtMapByName.get('Customer Contact').getRecordTypeId();    
        
        String strAccRecordTypeID;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Customer').getRecordTypeId();  
        
        profile commProfile = [select id from profile where name='RSP Customer Community Plus Profile'];
        
        Account acc = new Account(Name = 'wholesale comm', RecordTypeID = strAccRecordTypeID, OwnerId = UserInfo.getUserId());
        insert acc;
        
        Contact con = new Contact(accountid=acc.id,firstName = 'test first', lastName = 'test last', recordtypeid=strConRecordTypeID);
        insert con;
        
        Team_member__c tm = new Team_member__c(Account__c=acc.Id, Customer_Contact__c=con.Id, Role__c='Operational - Billing', Role_Type__c='All', RecordTypeId = Schema.SObjectType.Team_member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId());
        insert tm;
        
        user communityUsr = new user();
        System.runAs (new User(Id = UserInfo.getUserId())){
            //UserRole r = new UserRole(name = 'TEST ROLE', PortalType='CustomerPortal', PortalAccountId=acc.Id);
            communityUsr = new user(/*PortalRole = 'Manager',userroleid=r.id,*/contactid=con.id,LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',LanguageLocaleKey='en_US',TimeZoneSidKey='Australia/Sydney',CommunityNickname='testtt',Alias='tttt',Email='test@test.com',firstName = 'test first', lastName = 'test last', username='testclassuser@test.com', profileid=commProfile.id);
            insert communityUsr;
        }
        map<string,string> testRecordTypeMap = new map<string,string>();
        testRecordTypeMap.put('other claims','Billing claims');
        
        system.runAs(communityUsr){
            test.startTest();
            Case c = new Case(RecordTypeId= Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing Dispute').getRecordTypeId(), Category__c = 'Billing Enquiry',type='Other claims', Number_of_Transactions__c = 2, Subject = 'Testing', Amount__c= 67);
            insert c;
            BillingFormController.saveCase(c,'Billing Dispute','other claims');
            BillingFormController.getRecordTypeTypesMap();
            //BillingFormController.getTypeOptions('other claims','Billing claims');
            BillingFormController.getTypeOptions(testRecordTypeMap);
            BillingFormController.getUserDetails();
            BillingFormController.CaseDeletionCheck(c.id);
            BillingFormController.updateExistingCaseFormStatus(c.id);
            BillingFormController.checkContactRole(con.Id);
            BillingFormController.sendBillingAckEmail(con.Id,c.Id);
            test.stopTest();
        }
    }
}