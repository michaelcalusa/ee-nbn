public class BldRegStagingRecordsProcessing {
    
    
    //Class variables
    public static final string Bld_Reg_Resource_Type = 'Single Building Registration';
    public static boolean InsertLeadFlag = true;
    
    @InvocableMethod
    public static void ProcessBuilderInvokeMethod(List<Global_Form_Staging__c> trgNewList){
      
      List<Id> recordIds = new List<Id> ();
      if(trgNewList != null && trgNewList.size() > 0){
          for(Global_Form_Staging__c singleRecord:trgNewList){
              recordIds.add(singleRecord.id);
          }            
      }
	  
      //Processing the 'completed' status pre-qualification records
      List<Global_form_staging__c> InterestedBldRegoRecords = [select id, Name, Status__c, Type__c, Data__c, Content_Type__c from Global_Form_Staging__c  where id in: recordIds];        
        
      List<Global_form_staging__c> CompletedBldRegoRecords = new List<Global_form_staging__c>();
      
      //For future development
      List<Global_form_staging__c> InProgressBldRegoRecords = new List<Global_form_staging__c>();  
      List<Global_form_staging__c> CancelledBldRegoRecordss = new List<Global_form_staging__c>();  
        
      for(Global_form_staging__c singleGlobalFormStagingRecord: InterestedBldRegoRecords){
          if(singleGlobalFormStagingRecord.status__c == 'Completed'){
             CompletedBldRegoRecords.add(singleGlobalFormStagingRecord);
          }/*
          else if(singleGlobalFormStagingRecord.status__c == 'In progress'){
             InProgressPreQuaRecords.add(singleGlobalFormStagingRecord); 
          }else if(singleGlobalFormStagingRecord.status__c == 'Cancelled'){
             CancelledPreQuaRecords.add(singleGlobalFormStagingRecord);
          }*/
      }
        
        
      if(CompletedBldRegoRecords != null && CompletedBldRegoRecords.size() > 0 ){
           BuildingRegoTriggerHandlerUtil.BuildingRegoCompletedProcessing(CompletedBldRegoRecords);
      }        
    }
    
}