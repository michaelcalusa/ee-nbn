@IsTest
public class BldRegStagingRecordsProcessing_Test {
    
    @testsetup static void testRecordSetup(){
        
        List<Global_Form_Staging__c> gfslist = new List<Global_Form_Staging__c>();
        Global_Form_Staging__c gfs1 = new Global_Form_Staging__c();
        gfs1.Content_Type__c = 'application/json';
        gfs1.Data__c = '{"registrantRole": "Strata Manager", "buildingType": "MDU Residential", "buildingAccessRequired": "Yes", "address": { "buildingAddress": { "addressId": "LOC100063211726", "addressSource": "Google", "formatedAddress": "8 LACHLAN ST WATERLOO NSW 2018" }, "postalAddress": { "addressId": "EiczMjAgUGl0dCBTdCwgU3lkbmV5IE5TVyAyMDAwLCBBdXN0cmFsaWEiMRIvChQKEglFQv-oPa4SaxHgSrXnZ30BExDAAioUChIJHxukjT6uEmsRfvqGwtWqzJY", "addressSource": "google", "formatedAddress": "320 Pitt Street Sydney NSW 2000" } }, "buildingContacts": { "strataManager": { "companyABN": "12345678912", "companyName": "North Sydney", "email": "shubhamjaiswal@nbnco.com.au", "givenName": "Shubham", "familyName": "Jaiswal", "phone": "0444444449", "buildingAccessInformation": "" }, "onSiteBuildingAccess": { "companyABN": "12345678912", "companyName": "Lightning", "email": "wayneni@nbnco.com.au", "givenName": "Wayne", "familyName": "Lee", "phone": "0444448744", "buildingAccessInformation": "Available 9 to 5 WeekDays" }, "bodyCorp": { "companyABN": "12345678901", "companyName": "ABC", "email": "ganeshsawant@nbnco.com.au", "givenName": "Ganesh", "familyName": "Sawant", "phone": "0444448744", "buildingAccessInformation": "" }, "authorizedAgents": { "companyABN": "12345678911", "companyName": "ABC", "email": "ganeshsawant@nbnco.com.au", "givenName": "Krishna", "familyName": "Kanth", "phone": "0444448743", "buildingAccessInformation": "" } }, "additionalBuildingDetails": { "planNumber": "20", "numberOfUnits": 100, "numberOfFloors": 10, "numberOfLiftEmergencyPhones": 2, "phoneNumberOfLiftEmergencyPhones": "04444444444, 0444444449", "numberOfMonitoredFireAlarms": 2, "phoneNumberOfMonitoredFireAlarms": "055555555, 0555555550", "numberOfATMs": 1, "numberOfVendingMachines": 1, "commsRoom": "Yes", "heritageListed": "No", "impactedByPlanningOverlays": "No", "crown": "No", "securityOrBackToBaseAlarms": "Yes", "additionalComments": "This Is new Construction" } }';
        gfs1.Status__c = 'Completed';
        gfs1.Type__c = 'Single Building Registration'; 
        gfslist.add(gfs1);
        
        
        insert gfslist;
                
    }
    
    
    @isTest static void ProcessBuilderInvokeMethod_Test(){

 		List<Global_Form_Staging__c> testrecords = [select id from Global_Form_Staging__c];
        system.debug('testrecords is '+testrecords);
        
        List<Global_Form_Staging__c> testrecords2 = new List<Global_Form_Staging__c>();
        
        if(testrecords != null){
            testrecords2.add(testrecords[0]);
            BldRegStagingRecordsProcessing.ProcessBuilderInvokeMethod(testrecords2);
        }

        
        List<Building_Registration__c> results = [select id from Building_Registration__c];
        system.assertNotEquals(null, results);        
    }
    
    @isTest static void ProcessBuilderInvokeMethod_Test2(){
        
        Global_Form_Staging__c gfs2 = new Global_Form_Staging__c();
        gfs2.Content_Type__c = 'application/json';
        gfs2.Data__c = '{"registrantRole": "Body Corp/Owners Corp", "buildingType": "MDU Residential", "buildingAccessRequired": "Yes", "address": { "buildingAddress": { "addressId": "LOC100063211726", "addressSource": "Google", "formatedAddress": "8 LACHLAN ST WATERLOO NSW 2018" }, "postalAddress": { "addressId": "EiczMjAgUGl0dCBTdCwgU3lkbmV5IE5TVyAyMDAwLCBBdXN0cmFsaWEiMRIvChQKEglFQv-oPa4SaxHgSrXnZ30BExDAAioUChIJHxukjT6uEmsRfvqGwtWqzJY", "addressSource": "google", "formatedAddress": "320 Pitt Street Sydney NSW 2000" } }, "buildingContacts": { "strataManager": { "companyABN": "12345678912", "companyName": "North Sydney", "email": "shubhamjaiswal@nbnco.com.au", "givenName": "Shubham", "familyName": "Jaiswal", "phone": "0444444449", "buildingAccessInformation": "" }, "onSiteBuildingAccess": { "companyABN": "12345678912", "companyName": "Lightning", "email": "wayneni@nbnco.com.au", "givenName": "Wayne", "familyName": "Lee", "phone": "0444448744", "buildingAccessInformation": "Available 9 to 5 WeekDays" }, "bodyCorp": { "companyABN": "12345678901", "companyName": "ABC", "email": "ganeshsawant@nbnco.com.au", "givenName": "Ganesh", "familyName": "Sawant", "phone": "0444448744", "buildingAccessInformation": "" }, "authorizedAgents": { "companyABN": "12345678911", "companyName": "ABC", "email": "ganeshsawant@nbnco.com.au", "givenName": "Krishna", "familyName": "Kanth", "phone": "0444448743", "buildingAccessInformation": "" } }, "additionalBuildingDetails": { "planNumber": "20", "numberOfUnits": 100, "numberOfFloors": 10, "numberOfLiftEmergencyPhones": 2, "phoneNumberOfLiftEmergencyPhones": "04444444444, 0444444449", "numberOfMonitoredFireAlarms": 2, "phoneNumberOfMonitoredFireAlarms": "055555555, 0555555550", "numberOfATMs": 1, "numberOfVendingMachines": 1, "commsRoom": "Yes", "heritageListed": "No", "impactedByPlanningOverlays": "No", "crown": "No", "securityOrBackToBaseAlarms": "Yes", "additionalComments": "This Is new Construction" } }';
        gfs2.Status__c = 'Completed';
        gfs2.Type__c = 'Single Building Registration'; 
        
        insert gfs2;

 		List<Global_Form_Staging__c> testrecords = new List<Global_Form_Staging__c>();
        //system.debug('testrecords is '+testrecords);
        testrecords.add(gfs2);
 		BldRegStagingRecordsProcessing.ProcessBuilderInvokeMethod(testrecords);
        
        List<Building_Registration__c> results = [select id from Building_Registration__c];
        system.assertNotEquals(null, results);        
    }

}