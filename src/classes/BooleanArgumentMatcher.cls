/**
 * Created by alan on 2019-02-26.
 */

@isTest
public class BooleanArgumentMatcher extends ArgumentMatcher{

    private Boolean expectedVal;

    public BooleanArgumentMatcher(Boolean expectedVal){
        super();
        this.expectedVal = expectedVal;
    }

    public override Boolean isMatch(Object arg){
        try{
            return expectedVal == Boolean.valueOf(arg);
        }catch(Exception e){
            return false;
        }
    }

}