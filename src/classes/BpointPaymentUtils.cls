//Utils class which has supporting classes to parse the http Responses from Bpoint 
//and to call Httprequest to BPOINT
public class BpointPaymentUtils {
	public class APIResponse {
		public Integer ResponseCode { get; set; }
		public String ResponseText { get; set; }
	}

	public class BpointPaymentResponse {
		public APIResponse APIResponse { get; set; }
		public String AuthKey { get; set; }
	}


    public class BpointAuthKeyResponse {
        public APIResponse APIResponse { get; set; }
        public String AuthKey { get; set; }
    }
   
    public class BpointAuthKeyRequest {
        public ProcessTxnData ProcessTxnData {get;set;} 
        public String RedirectionUrl {get;set;} 
        public IframeParameters IframeParameters {get;set;}
    }
        
    public class ProcessTxnData {
        public String Action {get;set;}
        public Decimal Amount {get;set;}
        public Integer TokenisationMode {get;set;} 
        public Boolean TestMode {get;set;}         
        public String SubType {get;set;} 
        public String Type_z {get;set;}
        public String Crn1 {get;set;}
        public String Currency_z {get;set;}
        public String MerchantReference {get;set;}
        public String BillerCode {get;set;}
    }
    
    public class IframeParameters {
        public String CSS {get;set;} 
        public Boolean ShowSubmitButton {get;set;}
    }
    
    
    public class TransactionResponse {
    public APIResponse APIResponse {get;set;} 
    public TxnResp TxnResp {get;set;} 
  }
    
    
  public class TxnResp {
    public String Action {get;set;} 
    public Integer Amount {get;set;} 
    public Integer AmountOriginal {get;set;} 
    public Integer AmountSurcharge {get;set;} 
    public String ThreeDSResponse {get;set;} 
    public String AuthoriseId {get;set;} 
    public String BankAccountDetails {get;set;} 
    public String BankResponseCode {get;set;} 
    public CVNResult CVNResult {get;set;} 
    public CardDetails CardDetails {get;set;} 
    public String CardType {get;set;} 
    public Decimal CurrencyValue {get;set;} 
    public String MerchantReference {get;set;} 
    public Boolean IsThreeDS {get;set;} 
    public Boolean IsCVNPresent {get;set;} 
    public String MerchantNumber   {get;set;} 
    public Integer OriginalTxnNumber {get;set;} 
    public String ProcessedDateTime {get;set;} 
    public String RRN {get;set;} 
    public String ReceiptNumber {get;set;} 
    public String Crn1 {get;set;} 
    public String Crn2 {get;set;} 
    public String Crn3 {get;set;} 
    public Integer ResponseCode {get;set;} 
    public String ResponseText {get;set;} 
    public String BillerCode {get;set;} 
    public String SettlementDate {get;set;} 
    public String Source {get;set;} 
    public Boolean StoreCard {get;set;} 
    public Boolean IsTestTxn {get;set;} 
    public String SubType {get;set;} 
    public String TxnNumber {get;set;} 
    public String DVToken {get;set;} 
    public String Type_Z {get;set;} // in json: Type
  }
    
    public class CardDetails {
    public String CardHolderName {get;set;} 
    public String ExpiryDate {get;set;} 
    public String MaskedCardNumber {get;set;} 
  }
  
  public class CVNResult {
    public String CVNResultCode {get;set;} 
  }

    //Added by Karan
    public class TxnReq {
        public boolean StoreCard {get;set;}
        
    }
    
    public class BPointProcessPayment{
       public TxnReq TxnReq  {get;set;}
    }
    
    public class BPointResponseIframeTxn{
       public APIResponse APIResponse {get;set;}
       public String ResultKey {get;set;}
       public String RedirectionUrl {get;set;}
    }

    public class CustomResponse{
    	public String transactionName {get;set;}
    	public String status {get;set;}
    	public String description {get;set;}
    }

    public class BPointErrorMessages{
        public List<String> messages{get;set;}
        public Boolean retry{get;set;}
    }

    public class CustomSuccessResponse{
    	public CustomResponse Sucess {get;set;}
    	public String iframeUrl {get;set;}
        public Decimal amount {get;set;}
        public String authKey{get;set;}
        public String ResultKey {get;set;}
        public TransactionResponse tResponse{get;set;}
    	public CustomSuccessResponse(String transactionName, String status,String url,String description,Decimal amount){
    		Sucess = new CustomResponse();
    		Sucess.status = status;
    		Sucess.transactionName = transactionName;
    		Sucess.description = description;
    		this.iframeUrl = url;
            this.amount = amount;
    	}
        public CustomSuccessResponse(String transactionName, String status,TransactionResponse tResponse){
            Sucess = new CustomResponse();
            Sucess.status = status;
            Sucess.transactionName = transactionName;
            this.tResponse = tResponse;
        }
        public CustomSuccessResponse(String transactionName, String status,String url,String authKey,String description,Decimal amount){
            Sucess = new CustomResponse();
            Sucess.status = status;
            Sucess.transactionName = transactionName;
            Sucess.description = description;
            this.iframeUrl = url;
            this.amount = amount;
            this.authKey = authKey;
        }
    }

    public class CustomBpointResponse{
    	public String BPointResponseCode {get;set;}
    	public String BPointResponseText {get;set;}
    }

    public class CustomErrorResponse{
    	public CustomResponse Error {get;set;}
    	public CustomBpointResponse bPointResponse{get;set;}
        public TransactionResponse tResponse{get;set;}
        public String ResultKey {get;set;}
        public BPointErrorMessages errorMessages{get; set;}
    	public CustomErrorResponse(String transactionName, String status, String BPointResponseCode, String BPointResponseText, String description){
    		Error = new CustomResponse();
    		Error.status = status;
    		Error.transactionName = transactionName;
    		Error.description = description;

    		bPointResponse = new CustomBpointResponse();
    		bPointResponse.BPointResponseCode = BPointResponseCode;
    		bPointResponse.BPointResponseText = BPointResponseText;

            errorMessages = new  BPointErrorMessages();
            errorMessages.messages = new List<String>();
            errorMessages.messages.add(description);
            errorMessages.retry = false;


    	}
        public CustomErrorResponse(String transactionName, String status, String ResultKey,String BPointResponseCode, String BPointResponseText, TransactionResponse tResponse){
            Error = new CustomResponse();
            Error.status = status;
            Error.transactionName = transactionName;


            bPointResponse = new CustomBpointResponse();
            bPointResponse.BPointResponseCode = BPointResponseCode;
            bPointResponse.BPointResponseText = BPointResponseText;

            this.tResponse = tResponse;
            this.ResultKey = ResultKey;

        }

        public CustomErrorResponse(String transactionName, String status, String BPointResponseCode, String BPointResponseText, String description,String errorMessageType){
            Error = new CustomResponse();
            Error.status = status;
            Error.transactionName = transactionName;
            Error.description = description;

            bPointResponse = new CustomBpointResponse();
            bPointResponse.BPointResponseCode = BPointResponseCode;
            bPointResponse.BPointResponseText = BPointResponseText;


            List<Payment_Component_Response_Matrix__c> errorRecords;

            if(errorMessageType == 'API Response Code')
            {
                errorRecords=[Select Response_Text__c,Retry__c from Payment_Component_Response_Matrix__c
                                                                        WHERE Type__c =:errorMessageType and Response_Code__c=:String.valueOf(BPointResponseCode)];
            }
            else if(errorMessageType == 'Transaction Response Code'){
                errorRecords=[Select Response_Text__c,Retry__c from Payment_Component_Response_Matrix__c
                                                                        WHERE Type__c =:errorMessageType and Bank_Response_Code__c=:String.valueOf(BPointResponseCode)];
            }
            else
            {
                errorRecords=[Select Response_Text__c,Retry__c from Payment_Component_Response_Matrix__c
                                                                        WHERE Type__c =:errorMessageType];   
            }
            for(Payment_Component_Response_Matrix__c obj: errorRecords)
            {
                
                if(errorMessages == null){
                    errorMessages = new  BPointErrorMessages();
                    
                }
                if(String.isNotEmpty(obj.Response_Text__c)){
                    if(errorMessages.messages == null)
                        errorMessages.messages = new List<String>();
                    errorMessages.messages.add(obj.Response_Text__c);
                }
                
                errorMessages.retry = obj.Retry__c;
            }

        }

        public CustomErrorResponse(String transactionName, String status, String ResultKey,String BPointResponseCode, String BPointResponseText, TransactionResponse tResponse,String errorMessageType){
            Error = new CustomResponse();
            Error.status = status;
            Error.transactionName = transactionName;


            bPointResponse = new CustomBpointResponse();
            bPointResponse.BPointResponseCode = BPointResponseCode;
            bPointResponse.BPointResponseText = BPointResponseText;

            this.tResponse = tResponse;
            this.ResultKey = ResultKey;
            List<Payment_Component_Response_Matrix__c> errorRecords=[Select Response_Text__c,Retry__c from Payment_Component_Response_Matrix__c
                                                                        WHERE Type__c =:errorMessageType and Bank_Response_Code__c =:String.valueOf(BPointResponseCode)];
            
            //Check if there are any errorRecords with Retry flag
            Boolean isRetryerror = false;
            for(Payment_Component_Response_Matrix__c obj: errorRecords)
            {
                if(obj.Retry__c != null && obj.Retry__c)
                {
                    isRetryerror = true;
                }
            }

            //If no errors with retry flag retrive hard failure errors
            if(!isRetryerror){
                errorRecords=[Select Response_Text__c,Retry__c from Payment_Component_Response_Matrix__c
                                                                        WHERE Type__c ='Hard Failure'];
            }


            for(Payment_Component_Response_Matrix__c obj: errorRecords)
            {
                if(errorMessages == null){
                    errorMessages = new  BPointErrorMessages();
                    
                }
                if(String.isNotEmpty(obj.Response_Text__c)){
                    if(errorMessages.messages == null)
                        errorMessages.messages = new List<String>();
                    errorMessages.messages.add(obj.Response_Text__c);
                }
                
                errorMessages.retry = obj.Retry__c;
            }

        }

    }

    //Custom Error frame work class which will be returned to UI both in sucessfull and error scenarios
    //when handling bpoint http responses
    public class ResponseToUi{
    	public CustomSuccessResponse sucessResponse{get;set;}
    	public CustomErrorResponse errorResponse{get;set;}
    	public Boolean isError{get;set;}
    	public ResponseToUi(CustomSuccessResponse sucessResp, CustomErrorResponse errorResp, Boolean isErr){
    		sucessResponse = sucessResp;
    		errorResponse = errorResp;
    		isError = isErr;
    	}
    }

    //Http get method to retrieve transaction details of BPoint payment
    public static responseToUi getTransactionResult(String resultKeyValue, Map<String, BPoint_Static_Parameters__mdt> mapBpointParams, String authorization, String ApplicationID) {

            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(mapBpointParams.get('Transaction_Result_URL').Parameter_Value__c + resultKeyValue);
            request.setMethod('GET');
            request.setHeader('Authorization', authorization);
            request.setHeader('Content-Type', 'application/json');
            HttpResponse response = http.send(request);

            TransactionResponse tResponse;
            CustomSuccessResponse sucessResponse = null;
            CustomErrorResponse errorResponse = null;
            ResponseToUi responseToUi = null;
            
            //http status code 200 indicates a valid response retured by Bpoint
            // And APIResponse.ResponseCode == 0 and TxnResponse.ResponseCode == 0 indicates a sucessfull payment
            if (response.getStatusCode() == 200 && string.isNotBlank(response.getBody()) && response.getBody()!='{}') {
                tResponse = (TransactionResponse)JSON.deserialize(response.getBody(), TransactionResponse.Class);
                if(tResponse.APIResponse.ResponseCode == 0 && tResponse.TxnResp.ResponseCode == 0){
                    sucessResponse = new CustomSuccessResponse('submitpayment', 'success',tResponse);
                }
                else{
                    system.debug('tResponse is - ' + tResponse);
                    //errorResponse = new CustomErrorResponse('submitpayment','error',resultKeyValue,tResponse.APIResponse.ResponseCode,tResponse.APIResponse.ResponseText, null);
                    errorResponse = new CustomErrorResponse('submitpayment','error',resultKeyValue,tResponse.TxnResp.BankResponseCode,tResponse.TxnResp.ResponseText, tResponse,'Transaction Response Code');
                }
            } else {
                //throw new AuraHandledException('Error while Retrieving TransactionResult in getTransactionResult');
                errorResponse = new CustomErrorResponse('submitpayment','error',resultKeyValue,String.valueOf(response.getStatusCode()),response.getStatus(), tResponse);  
            }

            if(sucessResponse == null){
                responseToUi = new ResponseToUi(sucessResponse, errorResponse, true);
                
                if(tResponse!= null){
                    //Log details
                BpointPaymentUtils.BpointInteractionLog logBpointInteraction = new BpointPaymentUtils.BpointInteractionLog('ProcessPayment',ApplicationID,'OK',String.valueOf(Datetime.now()),false);
                //logBpointInteraction.setAuthKeyTransactionDetails(String.valueOf(tResponse.TxnResp.amount),String.valueOf(tResponse.APIResponse.ResponseCode),tResponse.APIResponse.ResponseText,false);
                logBpointInteraction.setAuthKeyTransactionDetails(amoutwithDecimalInjection(tResponse.TxnResp.amount),String.valueOf(tResponse.APIResponse.ResponseCode),tResponse.APIResponse.ResponseText,false);    
                logBpointInteraction.setPaymentTransactionDetails(String.valueOf(tResponse.TxnResp.ResponseCode), tResponse.TxnResp.ResponseText, String.valueOf(tResponse.TxnResp.BankResponseCode), tResponse.TxnResp.CardDetails.MaskedCardNumber, tResponse.TxnResp.CardDetails.ExpiryDate, tResponse.TxnResp.CardType, tResponse.TxnResp.ReceiptNumber, true);    
                }
                
            }
            else{
                responseToUi = new ResponseToUi(sucessResponse, errorResponse, false);

                //Log details
                BpointPaymentUtils.BpointInteractionLog logBpointInteraction = new BpointPaymentUtils.BpointInteractionLog('ProcessPayment',ApplicationID,'OK',String.valueOf(Datetime.now()),false);
                //logBpointInteraction.setAuthKeyTransactionDetails(String.valueOf(tResponse.TxnResp.amount),String.valueOf(tResponse.APIResponse.ResponseCode),tResponse.APIResponse.ResponseText,false);
                logBpointInteraction.setAuthKeyTransactionDetails(amoutwithDecimalInjection(tResponse.TxnResp.amount),String.valueOf(tResponse.APIResponse.ResponseCode),tResponse.APIResponse.ResponseText,false);    
                logBpointInteraction.setPaymentTransactionDetails(String.valueOf(tResponse.TxnResp.ResponseCode), tResponse.TxnResp.ResponseText, String.valueOf(tResponse.TxnResp.BankResponseCode), tResponse.TxnResp.CardDetails.MaskedCardNumber, tResponse.TxnResp.CardDetails.ExpiryDate, tResponse.TxnResp.CardType, tResponse.TxnResp.ReceiptNumber, true);
            }
            //return tResponse;
            return responseToUi;
    }

    //Amount returned by Bpoint will not have any decimal position, so we introduce a decimal postion before last 2 digits
    public static string amoutwithDecimalInjection(Integer amount){

        String strAmount = String.valueOf(amount);
        String strDecimalAmount = strAmount.mid(0,strAmount.length() -2) + '.' + strAmount.mid(strAmount.length() -2,2);
        return strDecimalAmount;
    }

    public class BpointInteractionLog{
        
        private BPoint_Component_Transaction_Record__c logObject {get;set;}
        
        public BpointInteractionLog(String TransactionName, String ApplicationId,String Status,String Timestamp,Boolean SaveRecord){
            logObject = new BPoint_Component_Transaction_Record__c();
            logObject.Name = TransactionName;
            logObject.Application_Id__c = ApplicationId;
            logObject.Status__c = Status;
            logObject.Timestamp__c = Timestamp;
            if(SaveRecord)save();
        }
        
        public void save(){
            upsert logObject;
        }

        public void setAuthKeyTransactionDetails(String Amount, String APIResponseCode, String APIResponseText,Boolean SaveRecord){
            logObject.Amount__c = Amount;
            logObject.APIResponse__c = APIResponseCode;
            logObject.APIResponseText__c = APIResponseText;
            if(SaveRecord)save();
        }

        public void setPaymentTransactionDetails(String TxnResponseCode,String TxnResponseTxt,String BankResponseCode,
                                                String CardNum,String Expiry,String CardType,String ReceiptNumber,Boolean SaveRecord){
            logObject.TxnResponse__c = TxnResponseCode;
            logObject.TxnResponseText__c = TxnResponseTxt;
            logObject.BankResponseCode__c = BankResponseCode;
            logObject.CardNum__c = CardNum;
            logObject.Expiry__c = Expiry;
            logObject.CardType__c = CardType;
            logObject.ReceiptNumber__c = ReceiptNumber;
            if(SaveRecord)save();
        }

    }

    @future
    public static void logBpointError(String TransactionName,String ApplicationID)
    {
        BpointPaymentUtils.BpointInteractionLog logBpointInteraction = new BpointPaymentUtils.BpointInteractionLog(TransactionName,ApplicationID,'FAILED',String.valueOf(Datetime.now()),true);
    }
}