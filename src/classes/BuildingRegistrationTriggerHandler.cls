public class BuildingRegistrationTriggerHandler extends TriggerHandler {
    
    public BuildingRegistrationTriggerHandler() {
        if(Test.isRunningTest()){
            this.setMaxLoopCount(10);
        }
        else{
            this.setMaxLoopCount(2);
        }            
    }
    
    public override void beforeInsert(list<SObject> newlist) {
        for(Building_Registration__c br : (list<Building_Registration__c>)newlist){
            if(br.Registrant_Role__c!=null){
                if(br.Registrant_Role__c == 'Strata Manager' && br.Strata_Manager_Email_Address__c!=null)
                    br.Registrant_Email__c = br.Strata_Manager_Email_Address__c;
                else if(br.Registrant_Role__c == 'Authorized Agents/Legal Reps' && br.Authorised_Rep_Contact_Email__c!=null)
                    br.Registrant_Email__c = br.Authorised_Rep_Contact_Email__c;
                else if(br.Registrant_Role__c == 'Body Corp/Owners Corp' && br.Legal_Owner_Email_Address__c!=null)
                    br.Registrant_Email__c = br.Legal_Owner_Email_Address__c;
                    
            }
        }
    }
    
}