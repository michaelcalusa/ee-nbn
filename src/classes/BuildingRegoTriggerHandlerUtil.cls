/****************************************************************************************************************************
Class Name:         BuildingRegoTriggerHandlerUtil class.
Class Type:         Building Rego Trigger Handler Util class. 
Version:            1.0 
Created Date:       10th Oct,2018
Function:           This class is used to convert all the JSON data to actual building registration object
Modification Log:
* Developer             Date             Description
* --------------------------------------------------------------------------------------------------                 
* Wayne Ni      10/10/2018      Created - 
*****************************************************************************************************************************/
public class BuildingRegoTriggerHandlerUtil {
        
    //This method is used to convert all the JSON data to actual building registration object
    public static void BuildingRegoCompletedProcessing(List<Global_form_staging__c> input){               
        List<Global_Form_Staging__c> BuildingRegoStagingRecords = input;
               
        //system.debug('BuildingRegoStagingRecords size is '+ BuildingRegoStagingRecords.size());
        if(BuildingRegoStagingRecords != null && BuildingRegoStagingRecords.size() > 0 ){
            StagingBuildingConversion(BuildingRegoStagingRecords);
        }
        
    }
    
    //Method to convert actual staging web form object into the Building registeration
    public static void StagingBuildingConversion(List<Global_form_staging__c> stagingBuildingInput){
		List<Building_Registration__c> insertBuildings = new List<Building_Registration__c>();
        Set<string> GobalFormStagingNameSet = new Set<string>();
        List<GlobalFormEventTriggerHandlerUtil.GlobalFormErrorLogWrapper> GlobalErrorLogs = new List<GlobalFormEventTriggerHandlerUtil.GlobalFormErrorLogWrapper>();
			        
        for(Global_form_staging__c StagingRecord:stagingBuildingInput){
            
            singleBuildingRegoWrapper StagingBuilding = (singleBuildingRegoWrapper)JSON.deserialize(StagingRecord.Data__c,singleBuildingRegoWrapper.class);
            
            GobalFormStagingNameSet.add(StagingRecord.Name);
                        
            //Single Building Registeration insert logic
            Building_Registration__c  SingleBuildingReg = new Building_Registration__c ();
            
            //registrantRole buildingType buildingAccessRequired field mapping
            SingleBuildingReg.Global_Form_Staging__c = StagingRecord.Id;
            SingleBuildingReg.Registrant_Role__c = StagingBuilding.registrantRole;
            SingleBuildingReg.Building_Type__c  = StagingBuilding.buildingType;
            SingleBuildingReg.Building_Access_Required__c = StagingBuilding.buildingAccessRequired;
            SingleBuildingReg.Is_Bulk_Registration__c = false;
            SingleBuildingReg.Status__c = 'open';
            SingleBuildingReg.Name = StagingRecord.Name;
           
            
            //Address field mapping
            //Building Address mapping
            SingleBuildingReg.Building_Address__c =  StagingBuilding.address.buildingAddress.formatedAddress;
            SingleBuildingReg.Building_Address_Id__c =  StagingBuilding.address.buildingAddress.addressId;
            SingleBuildingReg.Building_Address_Source__c = StagingBuilding.address.buildingAddress.addressSource;
                        
            //Postal Address mapping
            SingleBuildingReg.Legal_Owner_Mailing_Address__c = StagingBuilding.address.postalAddress.formatedAddress;
            SingleBuildingReg.Legal_Owner_Mailing_Address_Id__c = StagingBuilding.address.postalAddress.addressId;
            SingleBuildingReg.Legal_Owner_Mailing_Address_Source__c = StagingBuilding.address.postalAddress.addressSource;
            
            //StrataManager field mapping
            if(StagingBuilding.buildingContacts.strataManager != null){
                SingleBuildingReg.Strata_Manager_Company_ABN__c = StagingBuilding.buildingContacts.strataManager.companyABN;
                SingleBuildingReg.Strata_Manager_Company_Name__c  = StagingBuilding.buildingContacts.strataManager.companyName;
                SingleBuildingReg.Strata_Manager_Email_Address__c  = StagingBuilding.buildingContacts.strataManager.email;
                SingleBuildingReg.Strata_Manager_First_Name__c = StagingBuilding.buildingContacts.strataManager.givenName;
                SingleBuildingReg.Strata_Manager_Last_Name__c = StagingBuilding.buildingContacts.strataManager.familyName;
                SingleBuildingReg.Strata_Manager_Phone_Number__c = StagingBuilding.buildingContacts.strataManager.phone;            
            }

            
            //OnSite contact field mapping
            if(StagingBuilding.buildingContacts.onSiteBuildingAccess != null){
                SingleBuildingReg.On_site_Building_Access_Company_ABN__c = StagingBuilding.buildingContacts.onSiteBuildingAccess.companyABN;
                SingleBuildingReg.On_site_Building_Access_Company_Name__c = StagingBuilding.buildingContacts.onSiteBuildingAccess.companyName;
                SingleBuildingReg.On_site_Building_Access_Email_Address__c = StagingBuilding.buildingContacts.onSiteBuildingAccess.email;
                SingleBuildingReg.On_site_Building_Access_First_Name__c = StagingBuilding.buildingContacts.onSiteBuildingAccess.givenName;
                SingleBuildingReg.On_site_Building_Access_Last_Name__c =  StagingBuilding.buildingContacts.onSiteBuildingAccess.familyName;
                SingleBuildingReg.On_site_Building_Access_Phone_Number__c = StagingBuilding.buildingContacts.onSiteBuildingAccess.phone;
                SingleBuildingReg.Building_Access_Information__c = StagingBuilding.buildingContacts.onSiteBuildingAccess.buildingAccessInformation;               
            }
            
            
            //Body Corp(Legal owner) field mapping bodyCorp
            if(StagingBuilding.buildingContacts.bodyCorp != null){
                SingleBuildingReg.Legal_Owner_Company_ABN__c = StagingBuilding.buildingContacts.bodyCorp.companyABN;
                SingleBuildingReg.Legal_Owner_Company_Name__c = StagingBuilding.buildingContacts.bodyCorp.companyName;
                SingleBuildingReg.Legal_Owner_Email_Address__c = StagingBuilding.buildingContacts.bodyCorp.email;
                SingleBuildingReg.Legal_Owner_First_Name__c = StagingBuilding.buildingContacts.bodyCorp.givenName;
                SingleBuildingReg.Legal_Owner_Last_Name__c = StagingBuilding.buildingContacts.bodyCorp.familyName;
                SingleBuildingReg.Legal_Owner_Phone_Number__c = StagingBuilding.buildingContacts.bodyCorp.phone;               
            }

            
            //Authroized agents field mapping
            if(StagingBuilding.buildingContacts.authorizedAgents != null){                
                SingleBuildingReg.Authorised_Rep_Contact_Company_ABN__c = StagingBuilding.buildingContacts.authorizedAgents.companyABN;
                SingleBuildingReg.Authorised_Rep_Contact_Company_Name__c = StagingBuilding.buildingContacts.authorizedAgents.companyName;
                SingleBuildingReg.Authorised_Rep_Contact_Email__c = StagingBuilding.buildingContacts.authorizedAgents.email;
                SingleBuildingReg.Authorised_Rep_Contact_First_Name__c = StagingBuilding.buildingContacts.authorizedAgents.givenName;
                SingleBuildingReg.Authorised_Rep_Contact_Last_Name__c = StagingBuilding.buildingContacts.authorizedAgents.familyName;
                SingleBuildingReg.Authorised_Rep_Contact_Phone__c = StagingBuilding.buildingContacts.authorizedAgents.phone;                
            }
            
            //Additional building details field mapping
            SingleBuildingReg.Plan_Number__c = StagingBuilding.additionalBuildingDetails.planNumber;
            SingleBuildingReg.Number_of_Vending_Machines__c = StagingBuilding.additionalBuildingDetails.numberOfVendingMachines;
            SingleBuildingReg.Number_of_ATMs__c = StagingBuilding.additionalBuildingDetails.numberOfATMs;
            SingleBuildingReg.Comms_Room__c = StagingBuilding.additionalBuildingDetails.commsRoom;
            SingleBuildingReg.Phone_Number_of_Monitored_Fire_Alarms__c = StagingBuilding.additionalBuildingDetails.phoneNumberOfMonitoredFireAlarms;
            SingleBuildingReg.Heritage_Listed__c = StagingBuilding.additionalBuildingDetails.heritageListed;
            SingleBuildingReg.Number_of_Monitored_Fire_Alarms__c = StagingBuilding.additionalBuildingDetails.numberOfMonitoredFireAlarms;
            SingleBuildingReg.Impacted_by_Planning_Overlays__c = StagingBuilding.additionalBuildingDetails.impactedByPlanningOverlays;
            SingleBuildingReg.Phone_Number_of_Lift_Emergency_Phones__c = StagingBuilding.additionalBuildingDetails.phoneNumberOfLiftEmergencyPhones;      
            SingleBuildingReg.Crown_Land__c = StagingBuilding.additionalBuildingDetails.crown;
            SingleBuildingReg.Number_of_Lift_Emergency_Phones__c = StagingBuilding.additionalBuildingDetails.numberOfLiftEmergencyPhones; 
            SingleBuildingReg.Security_Or_Back_to_Base_Alarms__c = StagingBuilding.additionalBuildingDetails.securityOrBackToBaseAlarms;
            SingleBuildingReg.Number_of_Floors__c = StagingBuilding.additionalBuildingDetails.numberOfFloors;
            SingleBuildingReg.Additional_Comments__c = StagingBuilding.additionalBuildingDetails.additionalComments;
            SingleBuildingReg.Number_of_Premises__c = StagingBuilding.additionalBuildingDetails.numberOfUnits;
            
            insertBuildings.add(SingleBuildingReg);
        }
        
		        
        try{   
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule= true;
            dmo.EmailHeader.triggerUserEmail = true;
            dmo.DuplicateRuleHeader.allowSave = true;
            
            List<Database.SaveResult> insertResults = Database.insert(insertBuildings,dmo);
            //system.debug('Database result is '+insertResults);
            
            for(integer i=0;i < insertResults.size();i++){
                if(insertResults[i].issuccess() == false){
                    Building_Registration__c errorBuilding = insertBuildings[i];
                    GlobalFormEventTriggerHandlerUtil.GlobalFormErrorLogWrapper msg = new GlobalFormEventTriggerHandlerUtil.GlobalFormErrorLogWrapper();
                    msg.debuglevel = 'Error';
                    msg.source = 'FormAPI_WebService';
                    msg.logMessage = string.valueof(insertResults[i].getErrors());
                    msg.sourceFunction = 'Error when inserting the building registeration records from Website form';
                    msg.referenceId =  null;
                    msg.referenceInfo = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+ errorBuilding.Global_Form_Staging__c;
                    msg.payload = '';
                    msg.ex = null;
                    msg.timer = 0;
                    msg.FormType = 'Single Building Registration';
                    GlobalErrorLogs.add(msg); 
                }                
            }

            //insert the Global Form API error logs
            if(GlobalErrorLogs != null && GlobalErrorLogs.size()>0){
                GlobalFormEventTriggerHandlerUtil.logMessage(GlobalErrorLogs);                
            }

            
        }catch(Exception e){
            if(!(test.isRunningTest())){
                GlobalFormEventTriggerHandlerUtil.retryPlatformEventAndLogError('Global Form Event trigger handler general exception thrown', 'StagingBuildingConversion() method', e, null);                                        
            }
        } 
        
    }
    
        
    public class singleBuildingRegoWrapper{
        public addressWrapper address{get;set;}
        public String buildingAccessRequired{get;set;}
        public buildingContactsWrapper buildingContacts{get;set;}
        public String buildingType{get;set;}
        public additionalBuildingDetailsWrapper additionalBuildingDetails{get;set;}
        public String registrantRole{get;set;}
    }
    
    public class BuildingContactWrapper{
        public String companyABN{get;set;}
        public String givenName{get;set;}
        public String email{get;set;}
        public String familyName{get;set;}
        public String companyName{get;set;}
        public String phone{get;set;}
        public String buildingAccessInformation{get;set;}       
    }
    
    public class buildingContactsWrapper{
        public BuildingContactWrapper strataManager{get;set;}
        public BuildingContactWrapper onSiteBuildingAccess{get;set;}
        public BuildingContactWrapper authorizedAgents{get;set;}
        public BuildingContactWrapper bodyCorp{get;set;}
    }
    
    public class additionalBuildingDetailsWrapper{
        public String planNumber{get;set;}
        public Integer numberOfVendingMachines{get;set;}
        public Integer numberOfATMs{get;set;}
        public String commsRoom{get;set;}
        public String phoneNumberOfMonitoredFireAlarms{get;set;}
        public String heritageListed{get;set;}
        public Integer numberOfMonitoredFireAlarms{get;set;}
        public String impactedByPlanningOverlays{get;set;}
        public String phoneNumberOfLiftEmergencyPhones{get;set;}
        public String crown{get;set;}
        public Integer numberOfLiftEmergencyPhones{get;set;}
        public String securityOrBackToBaseAlarms{get;set;}
        public Integer numberOfFloors{get;set;}
        public String additionalComments{get;set;}
        public Integer numberOfUnits{get;set;}
        
    }
    
    public class postalAddressWrapper{
        public String formatedAddress{get;set;}
        public String addressSource{get;set;}
        public String addressId{get;set;}
    }
    
    public class buildingAddressWrapper{
        public String formatedAddress{get;set;}
        public String addressSource{get;set;}
        public String addressId{get;set;}
    }
    
    public class addressWrapper{
        public postalAddressWrapper postalAddress{get;set;}
        public buildingAddressWrapper buildingAddress{get;set;}
    }
    
}