@IsTest
public class BuildingRegoTriggerHandlerUtil_Test {
    
    @testsetup static void testRecordSetup(){
        
        List<Global_Form_Staging__c> gfslist = new List<Global_Form_Staging__c>();
        Global_Form_Staging__c gfs1 = new Global_Form_Staging__c();
        gfs1.Content_Type__c = 'application/json';
        gfs1.Data__c = '{ "registrantRole": "Strata Manager", "buildingType": "MDU Residential", "buildingAccessRequired": "No", "address": { "buildingAddress": { "addressId": "LOC100063211726", "addressSource": "Google", "formatedAddress": "8 LACHLAN ST WATERLOO NSW 2018" }, "postalAddress": { "addressId": "EiczMjAgUGl0dCBTdCwgU3lkbmV5IE5TVyAyMDAwLCBBdXN0cmFsaWEiMRIvChQKEglFQv-oPa4SaxHgSrXnZ30BExDAAioUChIJHxukjT6uEmsRfvqGwtWqzJY", "addressSource": "google", "formatedAddress": "320 Pitt Street Sydney NSW 2000" } }, "buildingContacts": { "strataManager": { "companyABN": "12345678912", "companyName": "North Sydney", "email": "shubhamjaiswal@nbnco.com.au", "givenName": "Shubham", "familyName": "Jaiswal", "phone": "0444444449", "buildingAccessInformation": "" }, "onSiteBuildingAccess": { "companyABN": "12345678912", "companyName": "Lightning", "email": "wayneni@nbnco.com.au", "givenName": "Wayne", "familyName": "Lee", "phone": "0444448744", "buildingAccessInformation": "Available 9 to 5 WeekDays" }, "bodyCorp": { "companyABN": "12345678901", "companyName": "ABC", "email": "ganeshsawant@nbnco.com.au", "givenName": "Ganesh", "familyName": "Sawant", "phone": "0444448744", "buildingAccessInformation": "" }, "authorizedAgents": { "companyABN": "12345678911", "companyName": "ABC", "email": "ganeshsawant@nbnco.com.au", "givenName": "Krishna", "familyName": "Kanth", "phone": "0444448743", "buildingAccessInformation": "" } }, "additionalBuildingDetails": { "planNumber": "20", "numberOfUnits": 100, "numberOfFloors": 10, "numberOfLiftEmergencyPhones": 2, "phoneNumberOfLiftEmergencyPhones": "04444444444, 0444444449", "numberOfMonitoredFireAlarms": 2, "phoneNumberOfMonitoredFireAlarms": "055555555, 0555555550", "numberOfATMs": 1, "numberOfVendingMachines": 1, "commsRoom": "Yes", "heritageListed": "No", "impactedByPlanningOverlays": "No", "crown": "No", "securityOrBackToBaseAlarms": "Yes", "additionalComments": "This Is new Construction" } }';
        gfs1.Status__c = 'Completed';
        gfs1.Type__c = 'Single Building Registration'; 
        gfslist.add(gfs1);
        
        insert gfslist;
                
    }
    
    
    @isTest static void BuildingRegoCompletedProcessing_Test(){
                
        List<Global_Form_Staging__c> testrecords = [select Name,Content_Type__c,Data__c,Status__c,Type__c from Global_Form_Staging__c];
        //system.debug('testrecords is '+testrecords);
        BuildingRegoTriggerHandlerUtil.BuildingRegoCompletedProcessing(testrecords);
        
        List<Building_Registration__c> results = [select id from Building_Registration__c];
        system.debug('the result size of building rego in Scenario A is '+results.size());
        system.assertNotEquals(null, results);             
    }
    
    @isTest static void BuildingRegoCompletedProcessing_Test_Negative(){
		
		List<Global_Form_Staging__c> gfslist2 = new List<Global_Form_Staging__c>();        
        Global_Form_Staging__c gfs2 = new Global_Form_Staging__c();
        gfs2.Content_Type__c = 'application/json';
        gfs2.Data__c = '{ "registrantRole": "Authorized Agents/Legal Reps", "buildingType": "MDU Residential", "buildingAccessRequired": "Yes", "address": { "buildingAddress": { "addressId": "LOC100063211726", "addressSource": "Google", "formatedAddress": "8 LACHLAN ST WATERLOO NSW 2018" }, "postalAddress": { "addressId": "EiczMjAgUGl0dCBTdCwgU3lkbmV5IE5TVyAyMDAwLCBBdXN0cmFsaWEiMRIvChQKEglFQv-oPa4SaxHgSrXnZ30BExDAAioUChIJHxukjT6uEmsRfvqGwtWqzJY", "addressSource": "Yahoo", "formatedAddress": "320 Pitt Street Sydney NSW 2000" } }, "buildingContacts": { "strataManager": { "companyABN": "12345678912", "companyName": "North Sydney", "email": "shubhamjaiswal@nbnco.com.au", "givenName": "Shubham", "familyName": "Jaiswal", "phone": "0444444449", "buildingAccessInformation": "" }, "onSiteBuildingAccess": { "companyABN": "12345678912", "companyName": "Lightning", "email": "wayneni@nbnco.com.au", "givenName": "Wayne", "familyName": "Lee", "phone": "0444448744", "buildingAccessInformation": "Available 9 to 5 WeekDays" }, "bodyCorp": { "companyABN": "12345678901", "companyName": "ABC", "email": "ganeshsawant@nbnco.com.au", "givenName": "Ganesh", "familyName": "Sawant", "phone": "0444448744", "buildingAccessInformation": "" }, "authorizedAgents": { "companyABN": "12345678911", "companyName": "ABC", "email": "ganeshsawant@nbnco.com.au", "givenName": "Krishna", "familyName": "Kanth", "phone": "0444448743", "buildingAccessInformation": "" } }, "additionalBuildingDetails": { "planNumber": "20", "numberOfUnits": 100, "numberOfFloors": 10, "numberOfLiftEmergencyPhones": 2, "phoneNumberOfLiftEmergencyPhones": "04444444444, 0444444449", "numberOfMonitoredFireAlarms": 2, "phoneNumberOfMonitoredFireAlarms": "055555555, 0555555550", "numberOfATMs": 1, "numberOfVendingMachines": 1, "commsRoom": "Yes", "heritageListed": "No", "impactedByPlanningOverlays": "No", "crown": "No", "securityOrBackToBaseAlarms": "Yes", "additionalComments": "This Is new Construction" } }';
        gfs2.Status__c = 'Completed';
        gfs2.Type__c = 'Single Building Registration'; 
        gfslist2.add(gfs2);        
        insert gfslist2;
                
        BuildingRegoTriggerHandlerUtil.BuildingRegoCompletedProcessing(gfslist2);
        
        List<Building_Registration__c> results = [select id from Building_Registration__c];
        system.debug('the result size of building rego is '+results.size());
        system.assertEquals(0, results.size());             
        
    }

}