public with sharing class BusinessPlatformEventTriggerHandler {

    public static void eventTypesIdentification(list<BusinessEvent__e> busEvents){     
     
       List<BusinessEvent__e> appianCalloutList = new List<BusinessEvent__e>();
       List<BusinessEvent__e> nsAppianCalloutList = new List<BusinessEvent__e>();
       List<BusinessEvent__e> sfProcessingList = new List<BusinessEvent__e>();
       List<BusinessEvent__e> nsProcessingList = new List<BusinessEvent__e>();

       Business_Platform_Events__c customSettingValues = Business_Platform_Events__c.getOrgDefaults(); 
                 
        for(BusinessEvent__e bEvent: busEvents){
          System.debug('Actual event is :' + bEvent);
          System.debug('customSettingValues  is :' + customSettingValues);

            if(customSettingValues.SFToAppainSource__c.equalsIgnoreCase(bEvent.Source__c) 
                && String.isNotBlank(bEvent.Event_Sub_Type__c)
                && ('NBN_SELECT'.equalsIgnoreCase(bEvent.Event_Sub_Type__c))){                 
                nsAppianCalloutList.add(bEvent);
            }
            else if(customSettingValues.SFToAppainSource__c.equalsIgnoreCase(bEvent.Source__c)){                 
                appianCalloutList.add(bEvent);
            }
            else if(customSettingValues.AppainToSFSource__c.equalsIgnoreCase(bEvent.Source__c)
                    && (bEvent.Event_Type__c.equalsIgnoreCase('NS SFC Desktop Assessment Completed'))){
                nsProcessingList.add(bEvent);
            }
            else if(customSettingValues.AppainToSFSource__c.equalsIgnoreCase(bEvent.Source__c)){
                sfProcessingList.add(bEvent);
            }
        }
        if(nsAppianCalloutList.size()>0){
            List<List<BusinessEvent__e>> resultList = BusinessPlatformEventTriggerHandler.spliceBy(nsAppianCalloutList,Integer.valueOf(customSettingValues.Batch_Size__c));
            system.debug('Salesforce to Appain Event batches size  : ' + resultList.size());      
            for(List<BusinessEvent__e> evtLst : resultList){
                System.enqueueJob(new NS_SFtoAppainEventHandler(evtLst));
            }
        }
        if(appianCalloutList.size()>0){
            List<List<BusinessEvent__e>> resultList = BusinessPlatformEventTriggerHandler.spliceBy(appianCalloutList,Integer.valueOf(customSettingValues.Batch_Size__c));
            system.debug('Salesforce to Appain Event batches size  : ' + resultList.size());      
            for(List<BusinessEvent__e> evtLst : resultList){
                System.enqueueJob(new DF_SFtoAppainEventHandler(evtLst));
            }
        }

        if(sfProcessingList.size()>0){
            List<List<BusinessEvent__e>> resultList = BusinessPlatformEventTriggerHandler.spliceBy(sfProcessingList,Integer.valueOf(customSettingValues.Batch_Size__c));
            system.debug('Appain to Salesforce Event batches size : ' + resultList.size());
            for(List<BusinessEvent__e> evtLst : resultList){
                System.enqueueJob(new DF_AppainToSFEventHandler(evtLst));
            }
        }
        if(nsProcessingList.size()>0){
            List<List<BusinessEvent__e>> resultList = BusinessPlatformEventTriggerHandler.spliceBy(nsProcessingList,Integer.valueOf(customSettingValues.Batch_Size__c));
            system.debug('Appain to Salesforce NS Event batches size : ' + resultList.size());
            for(List<BusinessEvent__e> evtLst : resultList){
                System.enqueueJob(new SF_AppainToSFEventHandler(evtLst));
            }
        }
    }    


    public static List<List<BusinessEvent__e>> spliceBy(List<BusinessEvent__e> objs, Integer size){
        List<List<BusinessEvent__e>> resultList = new List<List<BusinessEvent__e>>();

        Integer numberOfChunks = objs.size() / size;

        for(Integer j = 0; j < numberOfChunks; j++ ){

             List<BusinessEvent__e> someList = new List<BusinessEvent__e>();

            for(Integer i = j * size; i < (j+1) * size; i++){
                someList.add(objs[i]);
            }

            resultList.add(someList);
        }

        if(numberOfChunks * size < objs.size()){
            List<BusinessEvent__e> aList = new List<BusinessEvent__e>();

            for(Integer k = numberOfChunks * size ; k < objs.size(); k++){
                aList.add(objs[k]);
            }

            resultList.add(aList);
        }


        return resultList;
    }
       
}