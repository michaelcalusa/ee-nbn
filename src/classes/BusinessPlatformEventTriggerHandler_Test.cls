@isTest
private class BusinessPlatformEventTriggerHandler_Test
{
	@isTest
	static void AppianSubscribedEvents_Test()
	{
		  Business_Platform_Events__c settings = Business_Platform_Events__c.getOrgDefaults();
	      settings.SFC_Desktop_Assessment_completed__c = 'AP-SF-000008';
	      settings.DF_SFC_Desktop_Assessment__c = 'SF-DF-000001';
	      upsert settings Business_Platform_Events__c.Id;

  		List<BusinessEvent__e> toPublish = new List<BusinessEvent__e>();
	  	for(Integer i=0;i<201;i++){
		    String msg = '{ this is record ' + i + '}';
		    BusinessEvent__e b = new BusinessEvent__e();
			b.Event_Id__c = 'SF-DF-000001';
		    b.Event_Record_Id__c = String.valueOf(i);
		    b.Event_Type__c = 'Desktop assesment';
		    b.Source__c = 'Salesforce';
		    b.Message_Body__c = msg;
		    toPublish.add(b);
		}

		Test.startTest();

		try{
			List<Database.SaveResult> results = EventBus.publish(toPublish);       

		}catch(Exception e){

		}

		Test.stopTest();

	}
}