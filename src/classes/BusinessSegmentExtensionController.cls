/*------------------------------------------------------------------------
Author:        Karan Shekhar
Company:       Wipro
Description:   A controller class which allows Sales to create/update business segment details on Account

Test Class:    BusinessSegmentExtensionController_Test
History
<Date>         <Authors Name>   <Brief Description of Change>
11/08/2016     Karan Shekhar    Created
--------------------------------------------------------------------------*/   
public with sharing class BusinessSegmentExtensionController {
    
    private final Business_Segment__c businessSeg;
    public String selectedCategory{get;set;}
    public Boolean renderFields{get;set;}    
    public List<FieldsWrapper> fieldWrapperList{get;set;}
    public List<FieldsWrapper> fieldWrapperList1{get;set;}
    public List<CategoryWrapper> categoryWrapperList{get;set;}
    public String accountId='';
    public Boolean isClicked{get;set;}
    public List<String> selectedSegmentList{get;set;}
    
    /*------------------------------------------------------------
    Author:        Karan Shekhar
    Company:       Wipro
    Description:   Constructor for controller,called when businesssegmentform page is loaded
    Inputs:        None
    Outputs:       None
    ------------------------------------------------------------*/ 
    public BusinessSegmentExtensionController(ApexPages.StandardController stdController) {
        this.businessSeg= (Business_Segment__c )stdController.getRecord();
        renderFields=false;
        if(apexpages.currentpage().getparameters().get('retURL')!=null){
                
                accountId= apexpages.currentpage().getparameters().get('retURL');   
                if(accountId.indexof('/')!=-1){
                        accountId=accountId.replace('/','');
                }
        }
        isClicked=false;
        //fetchCategory();
        
        
    }
    
    /*------------------------------------------------------------
    Author:        Karan Shekhar
    Company:       Wipro
    Description:   Method to fetch picklist values of Segment field on Business segment object dynamically
    Inputs:        None
    Outputs:       List<SelectOption>
    ------------------------------------------------------------*/ 
    public List<SelectOption> getSegment(){
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Business_Segment__c.Segment__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }              
        return options;
    }
    
    /*------------------------------------------------------------
    Author:        Karan Shekhar
    Company:       Wipro
    Description:   Method to fetch picklist values of Maturity level field on Business segment category object dynamically
    Inputs:        None
    Outputs:       List<SelectOption>
    ------------------------------------------------------------*/ 
    public List<SelectOption> getMaturityLevel(){
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Business_Segment_Category__c.Maturity_level__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('-None-','-None-')); 
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }              
        return options;
    }
    
    /*------------------------------------------------------------
    Author:        Karan Shekhar
    Company:       Wipro
    Description:   Method to fetch picklist values of Target volume field on Business segment object dynamically
    Inputs:        None
    Outputs:       List<SelectOption>
    ------------------------------------------------------------*/ 
    public List<SelectOption> getTargetVolume(){
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Business_Segment__c.Target_Volumes__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('-None-','-None-')); 
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }             
        return options;
    }
    
    /*------------------------------------------------------------
    Author:        Karan Shekhar
    Company:       Wipro
    Description:   Method to fetch  values of Category field on Business segment category object dynamically
    Inputs:        None
    Outputs:       List<String>
    ------------------------------------------------------------*/ 
    public List<String> getCategory(){
        List<String> options = new List<String>();
        
        Schema.DescribeFieldResult fieldResult = Business_Segment_Category__c.Category__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple){
            options.add(f.getValue());
        }       
        return options;
    }
    
    /*------------------------------------------------------------
    Author:        Karan Shekhar
    Company:       Wipro
    Description:   Method to create target volume,Market share,Category fields dynamially based in selection of segment in page
    Inputs:        None
    Outputs:       None
    ------------------------------------------------------------*/ 
    public void fetchSelectedCategory(){   
       // isClicked= true; 
        fieldWrapperList= new List<FieldsWrapper>();
        fieldWrapperList1= new List<FieldsWrapper>();
        selectedSegmentList= new List<String>();
        Map<String,Business_Segment__c> segmentToValueMap= new Map<String,Business_Segment__c>();
        Set<Id>  businessSegIdSet= new Set<id>(); 
        List<Business_Segment__c> businessSegmentList=[Select id,Account__c,Segment__c,Current_Fixed_line_Market_share__c,Target_Volumes__c from Business_Segment__c where Account__c=:accountId ];
        categoryWrapperList= new List<CategoryWrapper>();
        List<String> cateoryValues=getCategory();
        Map<String,Business_Segment_Category__c> categoryToValueMap= new Map<String,Business_Segment_Category__c>();  
        for(Business_Segment__c bussSegObj: businessSegmentList){
                if(bussSegObj.Segment__c!=null){
                
                        //fetch existing values for business segment
                        segmentToValueMap.put(bussSegObj.Segment__c,bussSegObj);
                        businessSegIdSet.add(bussSegObj.Id);
                }
        }
        List<Business_Segment_Category__c> existingBusinessSegCatList=[Select id,Category__c,Business_Segment__r.Segment__c,Maturity_Level__c,Targeted_End_Date__c,Activity_to_Address__c from Business_Segment_Category__c where Business_Segment__c IN: businessSegIdSet ];
        if(existingBusinessSegCatList!=null && existingBusinessSegCatList.size()>0){
            for(Business_Segment_Category__c bussCatObj:existingBusinessSegCatList){
                
                //fetch existing values for business segment category
                categoryToValueMap.put(bussCatObj.Business_Segment__r.Segment__c+bussCatObj.Category__c,bussCatObj);
            }
        }
        if(((selectedCategory.replace('[','')).replace(']',''))==null || ((selectedCategory.replace('[','')).replace(']',''))==''){
            
            //throw warning if no segment is selected
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select category'));
            renderFields=false;
        }
        else{
            isClicked=true;
            renderFields=true;
            for(String str:selectedCategory.split(',') ){
                String segment=(str.replace('[','')).replace(']','').trim();
                selectedSegmentList.add(segment);
                String marrket='Current Fixed line Market-share (%) for '+segment;
                String volumne='Target Volumes for '+segment;
                
                if(segmentToValueMap.containsKey(segment)){
                        
                        //create fields for market share & target volume based on selected segment & populate exisiting data for business segment, if any
                        fieldWrapperList.add(new FieldsWrapper(marrket,String.valueof(segmentToValueMap.get(segment).Current_Fixed_line_Market_share__c),volumne,getTargetVolume(),segmentToValueMap.get(segment).Target_Volumes__c) );  
                }
                else{
                        //create fields for market share & target volume based on selected segment
                        fieldWrapperList.add(new FieldsWrapper(marrket,'',volumne,getTargetVolume(),'') );         
                }
                 
            }
            //fetchCategory();    

        for(String selCat:selectedCategory.split(',')){
            String segment=(selCat.replace('[','')).replace(']','').trim();
            for(String str:cateoryValues){
            
                if(categoryToValueMap.containsKey(segment+str)){
                    
                    //create fields for categories based on selected segment & populate exisiting data for business segment category, if any
                   categoryWrapperList.add(new CategoryWrapper(segment,str,categoryToValueMap.get(segment+str)));              
                }
                else{
                    //create fields for categories based on selected segment
                    categoryWrapperList.add(new CategoryWrapper(segment,str,new Business_Segment_Category__c()));
                }
            
        } 
            
        }           
 
                     
        }
               
    }
    
   /* public void fetchCategory(){
        
        categoryWrapperList= new List<CategoryWrapper>();
        List<String> cateoryValues=getCategory();
        Map<String,Business_Segment_Category__c> categoryToValueMap= new Map<String,Business_Segment_Category__c>();
        List<Business_Segment_Category__c> existingBusinessSegCatList=[Select id,Account__c,Category__c,Maturity_Level__c,Targeted_End_Date__c,Activity_to_Address__c from Business_Segment_Category__c where Account__c=:accountId ];
        for(Business_Segment_Category__c bussCatObj: existingBusinessSegCatList){
                if(bussCatObj.Category__c!=null){
                        
                        categoryToValueMap.put(bussCatObj.Category__c,bussCatObj);
                }
        }
        
        for(String str:cateoryValues){
            
            if(categoryToValueMap.containsKey(str)){
                
                categoryWrapperList.add(new CategoryWrapper(str,getMaturityLevel(),categoryToValueMap.get(str).Activity_to_Address__c,categoryToValueMap.get(str).Targeted_End_Date__c,categoryToValueMap.get(str).Maturity_Level__c,''));              
            }
            else{
                
                categoryWrapperList.add(new CategoryWrapper(str,getMaturityLevel(),'',null,'',''));
            }
            
        }
    }*/
    
    /*------------------------------------------------------------
    Author:        Karan Shekhar
    Company:       Wipro
    Description:   Method to redirect to Account page when cancel button is clicked on page
    Inputs:        None
    Outputs:       Pagereference
    ------------------------------------------------------------*/ 
    public Pagereference cancel(){
        
        pagereference pr= new pagereference('/'+accountId);
        return pr;
    }
        
    /*------------------------------------------------------------
    Author:        Karan Shekhar
    Company:       Wipro
    Description:   Method to save Business segment & Business segment category details
    Inputs:        None
    Outputs:       Pagereference
    ------------------------------------------------------------*/ 
    public Pagereference save(){
        
        List<Business_Segment__c> businessSegList= new List<Business_Segment__c>();
        Map<String,Id> segNameToIDMap= new Map<String,Id>();
        List<Business_Segment_Category__c> businessSegCatList= new List<Business_Segment_Category__c>();
        Boolean valueAdded=false;
        Business_Segment__c  businessSegObj;
        Set<Id>  businessSegIdSet= new Set<id>();
        Business_Segment_Category__c  businessCatObj;
        Map<String,Business_Segment__c> segmentToValueMap= new Map<String,Business_Segment__c>();
        Map<String,Business_Segment_Category__c> categoryToValueMap= new Map<String,Business_Segment_Category__c>();
        Savepoint sp = Database.setSavepoint();
        
        try{
            List<Business_Segment__c> businessSegmentList=[Select id,Account__c,Segment__c from Business_Segment__c where Account__c=:accountId ];
           // List<Business_Segment_Category__c> existingBusinessSegCatList=[Select id,Account__c,Category__c from Business_Segment_Category__c where Account__c=:accountId ];
            if(businessSegmentList!=null && businessSegmentList.size()>0){
                
                for(Business_Segment__c segmentObj: businessSegmentList){
                    
                    segmentToValueMap.put(segmentObj.Segment__c.trim(),segmentObj);
                    businessSegIdSet.add(segmentObj.Id);
                }
                
            }  
        List<Business_Segment_Category__c> existingBusinessSegCatList=[Select id,Category__c,Business_Segment__r.Segment__c,Maturity_Level__c,Targeted_End_Date__c,Activity_to_Address__c from Business_Segment_Category__c where Business_Segment__c IN: businessSegIdSet ];
        if(existingBusinessSegCatList!=null && existingBusinessSegCatList.size()>0){
            for(Business_Segment_Category__c bussCatObj:existingBusinessSegCatList){
                
                categoryToValueMap.put(bussCatObj.Business_Segment__r.Segment__c+bussCatObj.Category__c,bussCatObj);
            }
        }                    
            if(fieldWrapperList!=null && fieldWrapperList.size()>0){
                
                for(FieldsWrapper fieldWrapperObj: fieldWrapperList){
                    valueAdded=false;
                    if((fieldWrapperObj.marketSharefieldLabel).indexof('Current Fixed line Market-share (%) for ')!=-1 ){
                            if(segmentToValueMap.containsKey((fieldWrapperObj.marketSharefieldLabel).replace('Current Fixed line Market-share (%) for ','').trim())){
                                
                                businessSegObj=segmentToValueMap.get((fieldWrapperObj.marketSharefieldLabel).replace('Current Fixed line Market-share (%) for ','').trim());
                                
                                
                            }
                            else{
                                 businessSegObj= new Business_Segment__c();   
                                 businessSegObj.Account__c=accountId; 
                                
                            }
                        
                    }
                    if((fieldWrapperObj.volumefieldLabel).indexof('Target Volumes for ')!=-1){
                            if(segmentToValueMap.containsKey((fieldWrapperObj.volumefieldLabel).replace('Target Volumes for ','').trim())){
                                
                                businessSegObj=segmentToValueMap.get((fieldWrapperObj.volumefieldLabel).replace('Target Volumes for ','').trim());
                                
                            }
                            else{
                                 businessSegObj= new Business_Segment__c();  
                                 businessSegObj.Account__c=accountId;  
                                
                            }
                                
                    } 
                    if((fieldWrapperObj.marketSharefieldLabel).indexof('Current Fixed line Market-share (%) for ')!=-1 ){
                        
                         if(segmentToValueMap.containsKey((fieldWrapperObj.marketSharefieldLabel).replace('Current Fixed line Market-share (%) for ','').trim())){
                        
                                  if((fieldWrapperObj.marketSharefieldLabel).indexof('Current Fixed line Market-share (%) for ')!=-1){
                                    
                                    businessSegObj.Segment__c=(fieldWrapperObj.marketSharefieldLabel).replace('Current Fixed line Market-share (%) for ','');                      
                                    valueAdded=true;
                                    
                                }
                                
                                businessSegObj.Current_Fixed_line_Market_share__c =fieldWrapperObj.marketSharefieldValue!=null && fieldWrapperObj.marketSharefieldValue!='' ?  Decimal.valueof(fieldWrapperObj.marketSharefieldValue) : null;
                                businessSegObj.Target_Volumes__c =fieldWrapperObj.volumeselectedValue!=null && fieldWrapperObj.volumeselectedValue!='-None-' ? fieldWrapperObj.volumeselectedValue : null;
                        
                        }
                    }  
                                
                    if(fieldWrapperObj.marketSharefieldValue!=null && fieldWrapperObj.marketSharefieldValue!=''){
                        
                        if((fieldWrapperObj.marketSharefieldLabel).indexof('Current Fixed line Market-share (%) for ')!=-1){
                            
                            businessSegObj.Segment__c=(fieldWrapperObj.marketSharefieldLabel).replace('Current Fixed line Market-share (%) for ','');                      
                            valueAdded=true;
                            
                        }
                        
                        businessSegObj.Current_Fixed_line_Market_share__c =Decimal.valueof(fieldWrapperObj.marketSharefieldValue);
                        businessSegObj.Target_Volumes__c =fieldWrapperObj.volumeselectedValue!=null && fieldWrapperObj.volumeselectedValue!='-None-' ? fieldWrapperObj.volumeselectedValue : null;                                                                                   
                    }
                    
                    if((fieldWrapperObj.volumefieldLabel).indexof('Target Volumes for ')!=-1){
                        if(segmentToValueMap.containsKey((fieldWrapperObj.volumefieldLabel).replace('Target Volumes for ','').trim())){
                                
                                if((fieldWrapperObj.volumefieldLabel).indexof('Target Volumes for ')!=-1){
                                    
                                    if(businessSegObj.Segment__c==null || businessSegObj.Segment__c==''){
                                        businessSegObj.Segment__c=(fieldWrapperObj.volumefieldLabel).replace('Target Volumes for ','');                                
                                    }
                                }
                                
                                businessSegObj.Target_Volumes__c =fieldWrapperObj.volumeselectedValue!=null && fieldWrapperObj.volumeselectedValue!='-None-' ? fieldWrapperObj.volumeselectedValue : null;   
                                valueAdded=true;
                                
                        }
                    }
                    if(fieldWrapperObj.volumeselectedValue!=null && fieldWrapperObj.volumeselectedValue!='-None-'){
                        if(businessSegObj==null)businessSegObj= new Business_Segment__c();
                        if((fieldWrapperObj.volumefieldLabel).indexof('Target Volumes for ')!=-1){
                            
                            if(businessSegObj.Segment__c==null || businessSegObj.Segment__c==''){
                                businessSegObj.Segment__c=(fieldWrapperObj.volumefieldLabel).replace('Target Volumes for ','');                                
                            }
                        }
                        
                        businessSegObj.Target_Volumes__c =fieldWrapperObj.volumeselectedValue!=null && fieldWrapperObj.volumeselectedValue!='-None-' ? fieldWrapperObj.volumeselectedValue : null;   
                        valueAdded=true;
                    }
                    if(valueAdded){
                        businessSegList.add(businessSegObj);
                    }
                    
                }               
            } 
            if(!businessSegList.isEmpty())  {
                try{
                    upsert businessSegList;
                    if(businessSegList!=null){
                        for(Business_Segment__c segObj:businessSegList){
                            
                            segNameToIDMap.put(segObj.Segment__c,segObj.Id);
                        }
                        system.debug('**segNameToIDMap**'+segNameToIDMap);
                        
                    }
                }
                catch(DMLException excp){
                    
                    system.debug('*excp****'+excp.getMessage());   
                    Database.rollback(sp); 
                    if(excp.getMessage().contains('NUMBER_OUTSIDE_VALID_RANGE, Current Fixed line Market-share (%): value outside of valid range on numeric field')) {
                        system.debug('**inside custom message*');
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Can only add 3 digits in market share field'));
                        return null;
                    }   
                    else{
                                            system.debug('**inside std message*');
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,excp.getDMLMessage(0)));
                        return null;
                    }                         
                    
                    return null;
                }
                catch(Exception excp){
                    
                    system.debug('*excp****'+excp.getMessage());  
                    Database.rollback(sp);                               
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,excp.getMessage()));
                    return null;
                }    
            }               
            
           for(CategoryWrapper cat:categoryWrapperList){
                
                    if(categoryToValueMap.containsKey(cat.segmentName+cat.categoryName)){
                        businessCatObj= categoryToValueMap.get(cat.segmentName+cat.categoryName); 
                        businessCatObj.Activity_to_address__c=(cat.businessCatObj.Activity_to_Address__c!=null && cat.businessCatObj.Activity_to_Address__c!='') ? cat.businessCatObj.Activity_to_Address__c:'';
                    businessCatObj.Category__c=(cat.categoryName!=null && cat.categoryName!='-None-') ? cat.categoryName:null;
                    businessCatObj.Maturity_level__c=(cat.businessCatObj.Maturity_Level__c!=null && cat.businessCatObj.Maturity_Level__c!='-None-') ? cat.businessCatObj.Maturity_Level__c:null;
                    businessCatObj.Targeted_End_Date__c=(cat.businessCatObj.Targeted_End_Date__c!=null) ? cat.businessCatObj.Targeted_End_Date__c:null;
                    businessSegCatList.add(businessCatObj);
                                          
                }else{
                        
                                if((cat.businessCatObj.Maturity_Level__c!=null && cat.businessCatObj.Maturity_Level__c!='-None-') || (cat.businessCatObj.Activity_to_Address__c!=null && cat.businessCatObj.Activity_to_Address__c!='') || (cat.businessCatObj.Targeted_End_Date__c!=null) ){
                                        
                                    businessCatObj= new Business_Segment_Category__c();
                                businessCatObj.Activity_to_address__c=(cat.businessCatObj.Activity_to_Address__c!=null && cat.businessCatObj.Activity_to_Address__c!='') ? cat.businessCatObj.Activity_to_Address__c:'';
                                    businessCatObj.Category__c=(cat.categoryName!=null && cat.categoryName!='-None-') ? cat.categoryName:null;
                                    businessCatObj.Maturity_level__c=(cat.businessCatObj.Maturity_Level__c!=null && cat.businessCatObj.Maturity_Level__c!='-None-') ? cat.businessCatObj.Maturity_Level__c:null;
                                    businessCatObj.Targeted_End_Date__c=(cat.businessCatObj.Targeted_End_Date__c!=null) ? cat.businessCatObj.Targeted_End_Date__c:null;
                                    businessSegCatList.add(businessCatObj);
                                    if(segNameToIDMap.containsKey(cat.segmentName)){
                                         businessCatObj.Business_Segment__c= segNameToIDMap.get(cat.segmentName);
                                    }
                                    else{
                                        
                                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill market share & target volume before adding category for '+cat.segmentName+' segment'));
                                        Database.rollback(sp); 
                                        return null;
                                    }
                           
                                        
                                }
                    

                    }
                
                
               /* if((cat.businessCatObj.Maturity_Level__c!=null && cat.businessCatObj.Maturity_Level__c!='-None-') || (cat.businessCatObj.Activity_to_Address__c!=null && cat.businessCatObj.Activity_to_Address__c!='') || (cat.businessCatObj.Targeted_End_Date__c!=null) ){
                     system.debug('**categoryToValueMap**'+categoryToValueMap);
                     system.debug('**segNameToIDMap**'+segNameToIDMap);
                    if(categoryToValueMap.containsKey(cat.segmentName+cat.categoryName)){
                                         businessCatObj= categoryToValueMap.get(cat.segmentName+cat.categoryName);                   
                        }else{
                    
                            businessCatObj= new Business_Segment_Category__c();
                            if(segNameToIDMap.containsKey(cat.segmentName)){
                                 businessCatObj.Business_Segment__c= segNameToIDMap.get(cat.segmentName);
                            }
                            else{
                                
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please fill market share & target volume before adding category for '+cat.segmentName+' segment'));
                                Database.rollback(sp); 
                                return null;
                            }
                           
                    }                                         
                    businessCatObj.Activity_to_address__c=(cat.businessCatObj.Activity_to_Address__c!=null && cat.businessCatObj.Activity_to_Address__c!='') ? cat.businessCatObj.Activity_to_Address__c:'';
                    businessCatObj.Category__c=(cat.categoryName!=null && cat.categoryName!='-None-') ? cat.categoryName:null;
                    businessCatObj.Maturity_level__c=(cat.businessCatObj.Maturity_Level__c!=null && cat.businessCatObj.Maturity_Level__c!='-None-') ? cat.businessCatObj.Maturity_Level__c:null;
                    businessCatObj.Targeted_End_Date__c=(cat.businessCatObj.Targeted_End_Date__c!=null) ? cat.businessCatObj.Targeted_End_Date__c:null;
                    businessSegCatList.add(businessCatObj);
                }*/
            }
            
            
            /*for(CategoryWrapper cat:categoryWrapperList){                
                if((cat.selectedMatLevel!=null && cat.selectedMatLevel!='-None-') || (cat.activityToAdd!=null && cat.activityToAdd!='') || (cat.targetedTimeFrame!=null) ){
                        if(categoryToValueMap.containsKey(cat.categoryName)){
                                             businessCatObj= categoryToValueMap.get(cat.categoryName);                   
                            }else{
                        
                                businessCatObj= new Business_Segment_Category__c();
                                businessCatObj.Account__c=accountId; 
                        }                    
                    businessCatObj.Activity_to_address__c=(cat.activityToAdd!=null && cat.activityToAdd!='') ? cat.activityToAdd:'';
                    businessCatObj.Category__c=(cat.categoryName!=null && cat.categoryName!='-None-') ? cat.categoryName:null;
                    businessCatObj.Maturity_level__c=(cat.selectedMatLevel!=null && cat.selectedMatLevel!='-None-') ? cat.selectedMatLevel:null;
                    businessCatObj.Targeted_End_Date__c=(cat.targetedTimeFrame!=null) ? cat.targetedTimeFrame:null;
                    businessSegCatList.add(businessCatObj);
                }
                
                
            }*/
            try{
                upsert businessSegCatList;
            }
            catch(DMLException excp){
                
                system.debug('*excp****'+excp.getMessage());    
                Database.rollback(sp);                             
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,excp.getDMLMessage(0)));
                return null;
            }
            catch(Exception excp){
                
                system.debug('*excp****'+excp.getMessage());    
                Database.rollback(sp);                            
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,excp.getMessage()));
                return null;
            }
            
            pagereference pr= new pagereference('/'+accountId);
            return pr;
        }
        catch(Exception excp){
            
            system.debug('*excp****'+excp.getMessage()+excp.getStackTraceString()); 
            Database.rollback(sp);                                
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,excp.getMessage()));
            return null;
        }
        
        
    }
    
    public class FieldsWrapper{
        
        public String marketSharefieldLabel{get;set;}
        public String marketSharefieldValue{get;set;}
        public String volumefieldLabel{get;set;}
        public List<SelectOption> volumefieldValue{get;set;}
        public String volumeselectedValue{get;set;}
        public FieldsWrapper(String fldLabel,String fldValue,String volLabel,List<SelectOption> volValue,String selectedval){
            
            this.marketSharefieldLabel=fldLabel;
            this.marketSharefieldValue=fldValue;
            this.volumefieldLabel=volLabel;
            this.volumefieldValue=volValue;
            this.volumeselectedValue=selectedval;
        }
        
    }
    
    public class CategoryWrapper{
        
        public String segmentName{get;set;}
        public String categoryName{get;set;}
        public Business_Segment_Category__c businessCatObj{get;set;}
        public CategoryWrapper(String segName,String catName, Business_Segment_Category__c busCat){
            
            this.segmentName=segName;
            this.categoryName=catName;
            this.businessCatObj=busCat;
            
        }
        
    }
    
    
}