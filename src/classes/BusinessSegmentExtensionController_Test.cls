@isTest
private class BusinessSegmentExtensionController_Test {
    
    static List<Account> ListOfAccounts = new List<Account> ();
    static void setupData(){
        
       // Create Account
        ListOfAccounts = TestDataUtility.getListOfAccounts(1,true);
    }
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        setupData();
        PageReference pageRef = Page.businesssegmentform;
        pageRef.getparameters().put('retURL','/'+ListOfAccounts[0].Id); 
        Test.setCurrentPage(pageRef);
        Test.startTest();        
        ApexPages.StandardController sc = new ApexPages.standardController(new Business_Segment__c());
        BusinessSegmentExtensionController businessSegObj = new BusinessSegmentExtensionController(sc);
        businessSegObj.getSegment();
        businessSegObj.getMaturityLevel();
        businessSegObj.cancel();
        businessSegObj.selectedCategory='[]';
        businessSegObj.fetchSelectedCategory(); 
        businessSegObj.selectedCategory='[Small Business]';
        businessSegObj.fetchSelectedCategory();
        businessSegObj.fieldWrapperList[0].marketSharefieldValue='10';
        businessSegObj.fieldWrapperList[0].volumeselectedValue='501 – 1000';
		businessSegObj.categoryWrapperList[0].segmentName='Small Business';
        businessSegObj.categoryWrapperList[0].categoryName='Product';
        businessSegObj.categoryWrapperList[0].businessCatObj.Activity_to_Address__c='Test Activity';
        businessSegObj.categoryWrapperList[0].businessCatObj.Targeted_End_Date__c=System.today();
        businessSegObj.save();
        BusinessSegmentExtensionController businessSegObj1 = new BusinessSegmentExtensionController(sc);
        businessSegObj.selectedCategory='[Small Business,Medium Business]';
        businessSegObj.fetchSelectedCategory();
        businessSegObj.fieldWrapperList[0].marketSharefieldValue='123';
        businessSegObj.fieldWrapperList[0].volumeselectedValue='501 – 1000';
        businessSegObj.fieldWrapperList[1].marketSharefieldValue='879';
        businessSegObj.fieldWrapperList[1].volumeselectedValue='501 – 1000';
		businessSegObj.categoryWrapperList[0].segmentName='Small Business';
        businessSegObj.categoryWrapperList[0].categoryName='Product';
        businessSegObj.categoryWrapperList[0].businessCatObj.Activity_to_Address__c='Test Activity';
        businessSegObj.categoryWrapperList[0].businessCatObj.Targeted_End_Date__c=System.today();
        businessSegObj.save();
        businessSegObj.fetchSelectedCategory();
        businessSegObj.fieldWrapperList[0].marketSharefieldValue='12';
        businessSegObj.fieldWrapperList[0].volumeselectedValue='501 – 1000';
        businessSegObj.fieldWrapperList[1].marketSharefieldValue='14';
        businessSegObj.fieldWrapperList[1].volumeselectedValue='501 – 1000';
		businessSegObj.categoryWrapperList[0].segmentName='Small Business';
        businessSegObj.categoryWrapperList[0].categoryName='Product';
        businessSegObj.categoryWrapperList[0].businessCatObj.Activity_to_Address__c='Test Activity';
        businessSegObj.categoryWrapperList[0].businessCatObj.Targeted_End_Date__c=System.today()-10;
        businessSegObj.save();
        businessSegObj.fetchSelectedCategory();
        businessSegObj.fieldWrapperList[0].marketSharefieldValue='12';
        businessSegObj.fieldWrapperList[0].volumeselectedValue='501 – 1000';
        businessSegObj.fieldWrapperList[1].marketSharefieldValue='3456';
        businessSegObj.fieldWrapperList[1].volumeselectedValue='501 – 1000';
		businessSegObj.categoryWrapperList[0].segmentName='Small Business';
        businessSegObj.categoryWrapperList[0].categoryName='Product';
        businessSegObj.categoryWrapperList[0].businessCatObj.Activity_to_Address__c='Test Activity';
        businessSegObj.categoryWrapperList[0].businessCatObj.Targeted_End_Date__c=System.today();
        businessSegObj.save();
        Test.stopTest();

       
        
    }
}