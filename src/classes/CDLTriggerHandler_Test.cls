@isTest
private class CDLTriggerHandler_Test {
    /*----------------------------------------------------------------------------------------
Author:        Dilip Athley (v-dileepathley)
Company:       NBNco
Description:   Test Class for CDLTriggerHandler
Test Class:    
History
<Date>            <Authors Name>    <Brief Description of Change> 

-----------------------------------------------------------------------------------------*/
    
    static testMethod void cDLTriggerHandlerTest() {
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        TestDataUtility.getListOfCRMODCredentialsRecords(true);
        AttachmentURL__c atURL = new AttachmentURL__c();
        atURL.Name = 'AttachmentURL';
        atURL.Value__c = 'Test';
        insert atURL;
        integer StatusCode = 200;
        
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];  
        User u = TestDataUtility.createTestUser(true,p.id);
        
        
        System.runAs(u){
            Account a = new Account(Name='TestAccount');
            insert a;
            Contact c = new Contact (AccountId= a.Id, FirstName = 'firstName' , LastName = 'lastName', email = 'test@example.com');
            insert c;
            Development__c dev = new Development__c(Name= 'TestDev',Account__c = a.id, Primary_Contact__c = c.id, Development_ID__c = 'dev01', Suburb__c = 'Test');
            insert dev;
            dev.active_status__c = 'Incomplete';
            update dev;
            
            //CS case management Team queue
            Customer_Service_Team__c csmt = new Customer_Service_Team__c(
                Name = 'CS Case Management Team',
                Email__c = 'tesxtea@example.com',
                Task_Assignment_Notification__c = true
            );
            insert csmt;
            
            //create Customer Service Setting
            Customer_Service_Setting__c css = new Customer_Service_Setting__c(
                SetupOwnerId = UserInfo.getOrganizationId(),
                Unassigned_User_Id__c = u.Id,
                CS_Case_Management_Team_Id__c = csmt.Id,
                Task_Record_Type_Id_Task__c = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Task').getRecordTypeId(),
                Task_Record_Type_Id_Note__c = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Note').getRecordTypeId(),
                Task_Record_Type_Id_Interaction__c = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Interaction').getRecordTypeId()
            );
            System.debug('===Customer_Service_Setting__c:' + css);
            insert css;
            
            SA_Auto_Reference_Number__c refNumber = new SA_Auto_Reference_Number__c(Name = 'SA001', Last_Sequence_Number__c = '000000000');
            insert refNumber;
            List<Id> idList = new List<Id>();      
            Stage_Application__c stgApp = new Stage_Application__c(
                Reference_Number__c ='ref-0001',
                Request_ID__c='TestRq1',  
                Dwelling_Type__c ='SDU Only',  
                Application_Status__c = 'Payment Received',
                Development__c=dev.id,
                Active_Status__c ='Active', 
                Name = 'TestApp', 
                Primary_Contact__c = c.Id,
                Account__c= a.Id,
                Estimated_Ready_for_Service_Date__c =system.today());          
            insert stgApp;
            
            
            stgApp = [Select Id,Application_ID__c, Relationship_Manager__c,Relationship_Manager__r.isActive,Estimated_Ready_for_Service_Date__c from Stage_Application__c where id = : stgApp.id];       
            idList.add(stgApp.Id);
            system.debug('@StageAppTest'+stgApp+stgApp.Application_ID__c);
            Site__c ste = new Site__c(Name = 'Ste name');
            insert ste;
            insert new New_Dev_Platform_Events__c(Unassigned_User_Id__c=u.Id);
            
            List<Task> tskList = new List<Task>();
            Task mp = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                               WhatId = stgApp.id,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Deliverables',Sub_Type__c ='Master Plan', WhoId = c.Id);
            tskList.add(mp);
            Task pp = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                               WhatId = stgApp.id,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Deliverables',Sub_Type__c ='Pit & Pipe Design', WhoId = c.Id);
            tskList.add(pp);
            Task sp = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                               WhatId = stgApp.id,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Deliverables',Sub_Type__c ='Stage Plan', WhoId = c.Id);
            tskList.add(sp);
            Task ssp = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                                WhatId = stgApp.id,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Deliverables',Sub_Type__c ='Service Plan', WhoId = c.Id);
            tskList.add(ssp);
            Task abd = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                                WhatId = stgApp.id,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Deliverables', WhoId = c.Id);
            tskList.add(abd);
            Task adr = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                                WhatId = stgApp.id,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Additional Document Required', WhoId = c.Id);
            tskList.add(adr);
            Task PCN = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                                WhatId = stgApp.id,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Deliverables',Sub_Type__c ='PCN', WhoId = c.Id);
            tskList.add(PCN );
            insert tskList;
            
            
            list<ContentVersion> cvList = new list<ContentVersion>();
            for(Integer i=0;i<10;i++){
                ContentVersion v = new ContentVersion();
                v.versionData = Blob.valueOf('Test Content');
                v.title = 'testing upload'+i;
                v.Synced__c = false;
                v.pathOnClient ='/somepath.txt';
                cvList.add(v);
            }
            insert cvList;
            
            set<id> cvId = new set<id>();
            for(ContentVersion cv : cvList)
                cvId.add(cv.Id);          
            
            List<ContentDocument> cdList = [SELECT LatestPublishedVersionId FROM ContentDocument WHERE LatestPublishedVersionId in: cvId ];
            List<ContentDocumentLink> cdlNewList = new List<ContentDocumentLink>();
            List<Task> tskListUpdate = new List<Task>();
            Integer i=0;
            for(Task t : tskList){
                if(t.Deliverable_Status__c == 'Pending Documents'){               
                    ContentDocumentLink cdlNew = new ContentDocumentLink();  
                    cdlNew.ContentDocumentId = cdList[i].Id ;
                    cdlNew.LinkedEntityId  = t.id; 
                    cdlNew.ShareType ='V';
                    cdlNew.Visibility = 'AllUsers';
                    cdlNewList.add(cdlNew);
                    i++;
                }
            }
            insert cdlNewList;  
            for(Task t : tskList){
                t.Deliverable_Status__c = 'Submitted';
                tskListUpdate.add(t); 
            }
            update tskListUpdate;
        }
    }
  
}