public with sharing class CS_API_Util {

    /*
        * Creates an APISession for the Product Basket creation    
        * Parameters : basket, basket for which the session is created.
        * @Return : cscfga.API_1.ApiSession returns an API session
    */
    public static cscfga.API_1.ApiSession createApiSession(cscfga__Product_Basket__c basket) {
        cscfga.SessionManager.SessionInfo session;
        
        if(basket == null) {
            session = cscfga.SessionManager.getOrCreateSession('SessionKey: ' + Datetime.now(), 'User Session');
        }
        else {
            session = cscfga.SessionManager.loadSession(basket.cscfga__User_Session__c);
        }
        cscfga.API_1.ApiSession apiSession = cscfga.API_1.getApiSession();
        apiSession.loadUserSession(session.userSession.Id);
        
        return apiSession;      
    }

    /*
        * Creates an Basket for the APISession Passed.   
        * Parameters : apiSession, apiSession object
        * @Return : cscfga__Product_Basket__c returns an Product Basket
    */
	public static cscfga__Product_Basket__c createBasket(cscfga.API_1.ApiSession apiSession) {
		cscfga__Product_Basket__c basket = apiSession.getBasket();
        return basket;   	
    }

    /*
        * Find production definition based on the id passed        
        * Parameters : defId, Product definition id 
        * @Return : List<cscfga__Product_Definition__c> list of product definition
    */
    public static List<cscfga__Product_Definition__c> findProdDefinition(String defId){
        sObject product_Definition_Obj = new cscfga__Product_Definition__c();
        List<cscfga__Product_Definition__c> lst_Prd_Definition = new List<cscfga__Product_Definition__c>();
        String queryStr = getQuery(product_Definition_Obj, ' WHERE cscfga__Active__c = true and Id = '+ '\'' + defId + '\'');
        if(queryStr != null && queryStr.trim().length()>0){ 
            lst_Prd_Definition = (List<cscfga__Product_Definition__c>)getQueryRecords(queryStr);
        }
        return lst_Prd_Definition;
    }

    /*
        * Adds the product configuration to the basket        
        * Parameters : defId, Product definition id 
        * Parameters : apiSession, apiSession to which the config needs to be added.
        * @Return : List<cscfga__Product_Definition__c> list of product definition
    */
    public static cscfga.ProductConfiguration addProductConfigToBasket(String prodDefId, cscfga.API_1.ApiSession apiSession) {
        cscfga.ProductConfiguration prdCfg = null;
        if(prodDefId != null) {
            apiSession.setProductToConfigure(new cscfga__Product_Definition__c(Id = prodDefId));
            
            apiSession.executeRules();
            prdCfg = apiSession.getConfiguration();
        }
        return prdCfg;
    }

    /*
        * Method to get a list of product configs linked to the basket    
        * Parameters : basketId, Id of the basket to be queried.
        * @Return : List<cscfga__Product_Configuration__c> list of product configuration
    */
    public static List<cscfga__Product_Configuration__c> getProductBasketConfigs(String basketId) {
        sObject product_Confg_Obj = new cscfga__Product_Configuration__c();
        List<cscfga__Product_Configuration__c> lst_Prd_Cfgs = new List<cscfga__Product_Configuration__c>();
        String queryStr = getQuery(product_Confg_Obj, ' WHERE cscfga__Product_Basket__c = '+ '\'' + basketId + '\'');
        if(queryStr != null && queryStr.trim().length()>0){ 
            lst_Prd_Cfgs = (List<cscfga__Product_Configuration__c>)getQueryRecords(queryStr);
        }
        return lst_Prd_Cfgs;
    }


    /*
        * Method to get a single string of object fields with seperator     
        * Parameters : List<String> theList, String separator
        * @Return : String
    */
    private static String joinList(List<String> theList, String separator) {
        if (theList == null) { return null; }
        if (separator == null) { separator = ''; }
        String joined = '';
        Boolean firstItem = true;
        for (String item : theList) {
            if(null != item) {
                if(firstItem){ firstItem = false; }
            else { joined += separator; }
                joined += item;
            }
        }
        return joined;
    }

    /*
        * Method to create query String with Condition      
        * Parameters : sObject ObjectName, String queryCondition
        * @Return : String
    */
    public static String getQuery(sObject ObjectName, String queryCondition){
        String query = 'SELECT '+ joinList(discoverAccessibleFields(ObjectName), ',') + ' FROM '+ ObjectName.getSObjectType();
        if(queryCondition != null && queryCondition.trim().length()>0){
            query = query + queryCondition;
        }
        return query;
    }
    /*
        * Method to get List of Fields which are accessible to User      
        * Parameters : sObject ObjectName
        * @Return : List<String>
    */
    private static List<String> discoverAccessibleFields(sObject newObj) {
        List<String> accessibleFields = new List<String>();
        Map<String, Schema.SobjectField> fields = newObj.getSObjectType().getDescribe().fields.getMap();
        for (String s : fields.keySet()) {
            if (fields.get(s).getDescribe().isAccessible()) {
                accessibleFields.add(s);
            }
        }
        return accessibleFields;
    }

    /*
        * Method to do run SOQL on dynamic query String      
        * Parameters : String queryString
        * @Return : List<sObject>
    */
    public static List<sObject> getQueryRecords(String queryString){
        if(queryString != null && queryString.trim().length()>0){
            queryString += ' limit '+ (Limits.getLimitQueryRows() - Limits.getQueryRows());
            List<sObject> lst_QueryRecords = Database.query(queryString);
            return lst_QueryRecords;
        }else{
            return null;
        }
    }

}