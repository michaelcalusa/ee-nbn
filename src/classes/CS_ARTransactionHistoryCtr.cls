/***************************************************************************************************
Class name :CS_ARTransactionHistoryCtr
Created By : Srujana Priyanka
Description : used to call the external webservice methods and return the transaction history details
Modifications :
******************************************************************************************************/

public with sharing class CS_ARTransactionHistoryCtr {
  
    private ApexPages.StandardController controller;
    public string AccountName{get;set;}
    public string OverallBalance{get;set;}
    public string OverallStatus{get;set;}
  //  public Date InputDate{get;set;}
   // public Date ToDate{get;set;}
    public string TransactionType{get;set;}
    public string LineofBusiness{get;set;}
    public string OriginalReference{get;set;}
    public string TransactionId{get;set;}
    public Account acc{get;set;}
    public string transactionDate{get;set;}
    public string totalAmount{get;set;}
    public string totalOutstandingAmount{get;set;}
    public string balance{get;set;}
    public string Accountstatus{get;set;}
    public account a{get;set;}
    public string hostReferenceId{get;set;}
    public string productReference{get;set;}
    public string correlationId{get;set;}
    public string businessChannel{get;set;}
    public string channelMedia{get;set;}
    public string fromDate{get;set;}
    public string toDate{get;set;}
    public boolean isError{get;set;}
    public string errorDescription{get;set;}
    public string uniqueCorrelationId;
    
    //public List<CS_ARTransactionHistoryWrapper> transactionwrapperlist{get;set;}
    //public wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse retrieveHistoryResponse {get;set;}
    //forasync 
    public AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture response{get;set;}
   // public wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse result {get;set;}
    //for sync
    //public wwwNbnComAuDmmEnterpriseCommon.SOAResponseHeader_element response;
    //wwwNbncoComAuServiceArtransactionhi.Correlation_element Correlation{get;set;}
    public wwwNbncoComAuServiceArtransactionhi.Customer_element Customer{get;set;}
    public wwwNbncoComAuServiceArtransactionhi.Invoice_element Invoice{get;set;}
    public string result{get;set;}
    public string sourceReferenceNumber{get;set;}
    public string transactionDueDate{get;set;}
    public string status{get;set;}
    public string transactionClass{get;set;}
    public List<invwrapper> invlist{get;set;}
    public List<Accountwrapper> acclist{get;set;}
    public string sourceReferenceId{get;set;}
  //public List<selectOption> TransType{get;set;}
   public wwwNbncoComAuServiceArtransactionhi.AccountDetails_element AccountDetails;
    //Added by Vish
    public wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element customerreq;
   
    
    
    
    public class invwrapper  {
     public String Accountname{get;set;}
     public String transactionClass{get;set;}
     public String transactionDate{get;set;}
     public string totalTaxableAmount{get;set;}
     public string totalOutstandingAmount{get;set;}
     public string transactionDueDate{get;set;}
     public string status{get;set;}
     public string productReference{get;set;}
     public string originalReference {get;set;}
     public string hostReferenceId{get;set;}

   }
    
    public class Accountwrapper{
      public string Accountname{get;set;}
      public string balance{get;set;}
      public string status{get;set;}
    }
    
  //  public wwwNbncoComAuServiceArtransactionhi.AccountDetails_element AccountDetails;
    //Added by Vish
  //  public wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element customerreq;
   
   
   
   /*****************************************************************************************
     @Method name : Constructor
     @Description : To auto populate Account name from custom button on account detail page
    *****************************************************************************************/

    public CS_ARTransactionHistoryCtr(ApexPages.StandardController controller) {
    
     acc = (Account)controller.getRecord();
     a = [Select id,name,ParentId,Parent.Name from account where id=:ApexPages.currentPage().getParameters().get('AccountId')];
     System.debug('@@@@@' +a.id);
     acc.Name =a.Name;
     acc.parentId = a.ParentId;
     response = new AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture(); 
     isError = false;
     errorDescription = 'Error: ';
   }  
    
   /*****************************************************************
     @Method name : getTransactionType
     @Description : To get the Transaction type values dynamically
    ***************************************************************/
    
    public List<selectOption> getTransactionclass(){
     List<selectoption> TransType = new List<selectoption>();
     
     TransType.add(new selectOption('Invoice','Invoice'));
     TransType.add(new selectOption('credit Memo','Credit Memo'));
     TransType.add(new selectOption('Payment','Payment'));
     return TransType;
   }
   
    /*****************************************************************
     @Method name : getLineofBusiness
     @Description : To get the Line of Business values dynamically
    ***************************************************************/
   
    public List<selectOption> getTransactionType(){
     List<selectoption> lnofbusiness = new List<selectoption>();
     
     lnofbusiness.add(new selectOption('1','New Development'));
     lnofbusiness.add(new selectOption('2','Recoverable Works'));
     lnofbusiness.add(new selectOption('3','Network Ext'));
     return lnofbusiness;
   }
    
    /*****************************************************************
     @Method name : Cancel
     @Description : To return back to the Account Detail Page
    ***************************************************************/
   
   public PageReference Cancel()
   {
        string currentRecordId = ApexPages.currentPage().getParameters().get('id');
        PageReference pageRef = new pageReference('/' +a.Id);
        system.debug('@@@@@@@@' +pageRef);
        pageRef.setRedirect(true);
        return pageRef;
      }
  
      
   
    /*****************************************************
     @Method name : ApplyFilter
     @Description : Action method to call external webservice
    *******************************************************/
    public continuation ApplyFilter()
    {
       //Clear Previous Table entries with each search
       invlist=new List<invwrapper>(); 
       invlist.clear();

       uniqueCorrelationId = String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
       system.debug('uniqueCorrelationId is:' + uniqueCorrelationId);
       
       Integer TIMEOUT_INT_SECS = 120;  
       Continuation cont = new Continuation(TIMEOUT_INT_SECS);
       cont.continuationMethod = 'processResponse';
       TransactionHistory__c arTransactionHistory = TransactionHistory__c.getValues('ARTransactionHistory');
       
       AsyncWwwNbncoComAuServiceArtransactionhi.AsyncARTransactionHistoryBindingSOAPQSPort  stub = new AsyncWwwNbncoComAuServiceArtransactionhi.AsyncARTransactionHistoryBindingSOAPQSPort();

       
       stub.clientCertName_x = arTransactionHistory.ClientCertName__c;
       stub.endpoint_x = arTransactionHistory.Endpoint__c;
       //stub.endpoint_x = 'http://requestb.in/15x97261';
       stub.reqHeader = new wwwNbnComAuDmmEnterpriseCommon.SOAHeader_element();
       //stub.reqHeader.correlationId = arTransactionHistory.CorrelationID__c;
       stub.reqHeader.correlationId = uniqueCorrelationId;
       stub.reqHeader.businessChannel = arTransactionHistory.BusinessChannel__c;
       stub.reqHeader.channelMedia = arTransactionHistory.ChannelMedia__c;
       
       
       Customer = new wwwNbncoComAuServiceArtransactionhi.Customer_element();
       Customer.CustomerIdentifier = new  wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element();
       if(a.ParentId == null){     
       Customer.CustomerIdentifier.sourceReferenceId = a.id;
       }
       else {
       
       Customer.CustomerIdentifier.sourceReferenceId = a.ParentId;
       
       }
       
       System.debug('sourceReferenceId' +Customer.CustomerIdentifier.sourceReferenceId);
       

       
       customerreq = new wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element();
       customerreq.CustomerIdentifier = new  wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element();
       //Commented by Vish- customerreq.CustomerIdentifier.sourceReferenceId = a.id;
       if(a.ParentId == null){     
       customerreq.CustomerIdentifier.sourceReferenceId = a.id;
       }
       else {
       
       customerreq.CustomerIdentifier.sourceReferenceId = a.ParentId;
       customerreq.Account = new wwwNbncoComAuServiceArtransactionhi.Account_element();
       customerreq.Account.AccountIdentifier = new wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element();
       customerreq.Account.AccountIdentifier.sourceReferenceId = a.id;
       
       }

       System.debug('sourceReferenceId' +customerreq.CustomerIdentifier.sourceReferenceId);
       
      //Added by Vish
       /****Method used to send request to the web service****/
       
       system.debug('fromdate>>' +fromdate);
       system.debug('ttype>>>' +Transactionclass);
       SYSTEM.DEBUG('TRANS###' +TransactionType);
       system.debug('todate>>' +todate);
       if(String.isEmpty(fromDate))fromDate = null;
       if(String.isEmpty(toDate))toDate = null;
       if(String.isEmpty(Transactionclass))Transactionclass = null;
       if(String.isEmpty(hostReferenceId))hostReferenceId = null;
       if(string.isEmpty(TransactionType))TransactionType = null; 
    //   fromdate='2017-03-06';
    //   todate='2017-03-08';

      if(fromDate != null && !isValidDate(fromDate))
      {
        isError = true;
        errorDescription = 'Error: Invalid From Date. Please input date in YYYY-MM-DD format.';
        return null;
      }
      if(toDate != null && !isValidDate(toDate))
      {
        isError = true;
        errorDescription = 'Error: Invalid To Date. Please input date in YYYY-MM-DD format.';
        return null;
      }
      Invoice = null;

      //If one of Transaction Id or Orginal Reference(input fields on Page) are not blank
      /*Uncomment when 2778 is fixed if(!String.isEmpty(hostReferenceId) || !String.isEmpty(OriginalReference))
      {
        Invoice = new wwwNbncoComAuServiceArtransactionhi.Invoice_element();
        Invoice.InvoiceHeader = new wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element();
        
        //If SourceReference Number input field on page is not blank
        if(!String.isEmpty(OriginalReference))
        {
          Invoice.InvoiceHeader.sourceReferenceNumber = OriginalReference;
        }
        else
        {
          Invoice.InvoiceHeader.sourceReferenceNumber = null;
        }

        //If Original reference input field is not blank
        if(!String.isEmpty(hostReferenceId))
        {
          Invoice.InvoiceHeader.Correlation = new wwwNbncoComAuServiceArtransactionhi.Correlation_element();
          Invoice.InvoiceHeader.Correlation.hostReferenceId = hostReferenceId;
        }
        else
        {
          //Invoice.InvoiceHeader.Correlation = new wwwNbncoComAuServiceArtransactionhi.Correlation_element();
          //Invoice.InvoiceHeader.Correlation.hostReferenceId = null; 
          Invoice.InvoiceHeader.Correlation = null;
        }
      }*/

      //If Orginal Reference(input field on Page) is not blank
      //Comment below if condition when 2778 is fixed
      if(!String.isEmpty(OriginalReference))
      {
        Invoice = new wwwNbncoComAuServiceArtransactionhi.Invoice_element();
        Invoice.InvoiceHeader = new wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element();
        Invoice.InvoiceHeader.Correlation = null;
        Invoice.InvoiceHeader.sourceReferenceNumber = OriginalReference;
      }

       response = stub.beginRetrieveARTransactionHistory(cont,customerreq,Invoice,fromDate,toDate,TransactionType,transactionClass);
       System.debug('Response : ' + response);
       return cont;
        
    }



    public Boolean isValidDate(String sDate)
    {
      Boolean isValidDate = true;
      try
      {
        Date d=Date.valueOf(sDate);
        }Catch(System.TypeException ex)
        {
          isValidDate = false;
        }
      return isValidDate;
    }


    public string getAccountName(String accId)
    {
        string accNameToDisplay='';

      if(a.ParentId !=null)accNameToDisplay = a.Parent.Name;
      else accNameToDisplay = a.Name;
      try
      {
        Account accnt = (Test.isRunningTest()) ? [Select Id,Name from Account] : [Select Id,Name from Account where Id=:accId];
        accNameToDisplay = accnt.Name;
      }catch(Exception ex)
      {
          //Exception occured due to missing Account in Salesforce
          //Do nothing as Parent Account Name is returned in this scenario
      }
      return accNameToDisplay;  
    }
    
    
    public object processResponse() {
    try{
        TransactionHistory__c arTransactionHistory = TransactionHistory__c.getValues('ARTransactionHistory');
        
  //  System.debug('Response is -' + response.TOsTRING());
    /****Method to catch response from web service****/
      isError = false;
        
      wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse result = response.getValue();   
      System.debug('result: '+result);
      System.debug('Result Status' + result.ResultStatus.status);
      System.debug('Result Error' + result.ResultStatus.errorCode);
      System.debug('Result Error Description' + result.ResultStatus.errorDescription);

      if( result.ResultStatus.errorDescription != null && result.ResultStatus.errorDescription !='' && result.ResultStatus.status == arTransactionHistory.Result_Status_Fail__c/*'Fail'*/ )
      {
        isError = true;
        errorDescription = 'Error: ' + result.ResultStatus.errorDescription;
        return null;// return and show error message to user
      }
     system.debug('result.Customer_type_info' +result.Customer_type_info);
    // System.debug('result: '+result);
     system.debug('result%%%:' +result.Customer.AccountList.AccountRecord[0].AccountDetails);
     System.debug('result>>>: ' +result.Customer.AccountList.AccountRecord[0]);
     System.debug('result>>>: ' +result.Customer.AccountList.AccountRecord[0].AccountDetails.InvoiceList.InvoiceRecord.size());
     System.debug('result@@@: ' +result.Customer.CustomerDetails.balance); 
     
        
    if(result!= Null) {
        
     
   invlist=new List<invwrapper>(); 
   invlist.clear();
   
   List<invwrapper> invListOne=new List<invwrapper>();   
   invListOne.clear();
   

    //Added below by Vish to cater for the scenario when there are invoices for both parent and child accounts
    acclist = new List<Accountwrapper>();
    for(wwwNbncoComAuServiceArtransactionhi.AccountRecord_element accnt:result.Customer.AccountList.AccountRecord)
    {
        Accountwrapper accwrapper = new Accountwrapper();
        accwrapper.status = accnt.AccountDetails.status;
        accwrapper.balance = '$' + accnt.AccountDetails.balance;
        //accwrapper.Accountname = Accountname;
        accwrapper.Accountname = getAccountName(accnt.AccountIdentifier.sourceReferenceId);
         
        acclist.add(accwrapper);
        System.debug('acclist' +acclist);

        if(a.name == accwrapper.Accountname)
        {
          OverallStatus = accnt.AccountDetails.status;
          OverallBalance = '$' + accnt.AccountDetails.balance;
        }

      for(wwwNbncoComAuServiceArtransactionhi.InvoiceRecord_element inv:accnt.AccountDetails.InvoiceList.InvoiceRecord)
      {
         invwrapper iw=new invwrapper();
         iw.originalReference = inv.InvoiceHeader.sourceReferenceNumber;
         iw.hostReferenceId = inv.InvoiceHeader.Correlation.hostReferenceId;
         iw.Accountname = accwrapper.Accountname;
         iw.totalTaxableAmount= '$' + inv.totalTaxableAmount;
         iw.transactionClass=inv.transactionClass;
         iw.totalOutstandingAmount='$' + inv.totalOutstandingAmount;
         iw.transactionDate = inv.transactionDate;
         iw.transactionDueDate = inv.transactionDueDate;
         iw.productReference = inv.productReference;
         iw.status = inv.status;
         
         invListOne.add(iw);   //added invListOne instaed invlist        
      }
    }

    //Added by Vish
    //Uncomment when 2778 is fixed invList.addAll(invListOne);

    //Comment below if else condition when 2778 is fixed
    if(!(String.isBlank(hostReferenceId))) 
    { 
    
      for(invwrapper i:invListOne) 
      {
      
        if(i.hostReferenceId == hostReferenceId ) 
        {
          system.debug('****Transaction Id matched*****');
          invList.add(i);
        } 
      }
      if(invList.size() == 0)
      {
        isError = true;
        errorDescription = 'Error: ' + 'No Data found. Invalid Transaction Id.';
        return null;// return and show error message to user
      }   
    } 
    else 
    {
        invList.addAll(invListOne);
    }

    
    
    /****If Condition to filter originalreference and hostreferenceId in VF Page***/
   
   /*if(!String.isBlank(originalReference) && !(String.isBlank(hostReferenceId))) 
   { 
    
      for(invwrapper i:invListOne) 
      {
      
        if((i.originalReference == originalReference && i.hostReferenceId == hostReferenceId )) 
        {
          system.debug('****ORID matched*****');
          invList.add(i);
        } 
      }   
    } 
    else if(!String.isBlank(originalReference) || !String.isBlank(hostReferenceId)) 
    { 
     
       for(invwrapper i:invListOne) 
       {
        
          if((i.originalReference == originalReference || i.hostReferenceId == hostReferenceId)) 
          {
            system.debug('****ORID matched*****');
            invList.add(i);
          } 
       }    
    }
    else 
    {
      
        invList.addAll(invListOne);
    }*/


/****For loop used to sort the transaction Date in Descending order****/
    for (Integer i = 0; i < invlist.size(); i++){
                  for (Integer j = i; j > 0; j--) {
                    if (date.valueOf(invlist[j - 1].transactionDate) < date.valueOf(invlist[j].transactionDate)){
                        invwrapper temp = invlist[j];
                        invlist[j] = invlist[j - 1];
                        invlist[j - 1] = temp;
                    }
               }
          } 
          
        }
    
       }
        catch(System.CalloutException ex)
        {
          System.debug('Callout Exception is - ' + ex);
          System.debug('Stack trace is -' + ex.getStackTraceString());
          System.debug('Message is -' + ex.getMessage());
          System.debug('Entire callout exception is - ' + string.valueOf(ex) + ex);
          isError = true;
          errorDescription = 'Please Contact the System Admin. Correlation Id is - ' + uniqueCorrelationId + '. Error: ' + ex.getMessage();
        }
        catch(Exception ex)
        {
          System.debug('Exception is - ' + ex);
          System.debug('Stack trace is -' + ex.getStackTraceString());
          System.debug('Message is -' + ex.getMessage());
          System.debug('Entire exception is - ' + string.valueOf(ex) + ex);
          isError = true;
          errorDescription = 'Please Contact the System Admin. Correlation Id is - ' + uniqueCorrelationId + '. Error: ' + ex.getMessage();

        } 
      return null;
    }
    
    
    
    

}