/***************************************************************************************************
Class name :CS_ARTransactionHistoryCtr_Test
Created By : Srujana Priyanka
Description : Test class for CS_ARTransactionHistoryCtr
Modifications :
******************************************************************************************************/
@isTest
public class CS_ARTransactionHistoryCtr_Test {

	@testSetup
    static void createTestData() {
		TransactionHistory__c arTransactionHistory = 
	   		new TransactionHistory__c(ClientCertName__c = 'icrm_sit_ssl', Endpoint__c = 'callout:DATAPOWER', ChannelMedia__c = 'PSaaS', BusinessChannel__c = 'SFDC', 
	   			CorrelationID__c = 'Scen4180118', Name = 'ARTransactionHistory', Result_Status_Fail__c = 'Fail');
		insert arTransactionHistory;
    }

    public static testmethod void testconstructor() {
       
      Account acc = new Account(Name = 'TestAccountName');
	  insert acc;
      ApexPages.StandardController sc = new ApexPages.standardController(acc);
      ApexPages.currentPage().getParameters().put('AccountId',acc.id);
      CS_ARTransactionHistoryCtr controller = new CS_ARTransactionHistoryCtr(sc);
      controller.AccountName = 'testAccount';
      controller.overallbalance = '100';
      controller.LineofBusiness = 'New Development';
      controller.OriginalReference = 'AYCA-3GGVCR';
      controller.productReference = 'Technology choice';
      controller.hostReferenceId = '1000457';
      controller.transactiondate = '2017-03-08';
      controller.fromDate = '2017-03-06';
      controller.ToDate = '2017-03-08';
      controller.sourceReferenceId = '9792250';
      controller.result = 'result';
      controller.sourceReferenceNumber ='975434';
      controller.status = 'overdue';
      controller.transactionduedate = '2017-03-17';
      controller.transactionId = '235678';
      controller.balance = '3453453';
      controller.Accountstatus = 'current';
      controller.overallstatus = 'overdue';
      controller.correlationId = 'Scen4180117';
      controller.businessChannel = 'SFDC';
      controller.channelMedia = 'PSaaS';
      controller.totalAmount = '1000';
      controller.totalOutstandingAmount = '5464';
      
      
    }
    
    public static testMethod void testTransactionType(){
        Account acc = new Account(Name = 'TestAccountName');
	    insert acc;
        ApexPages.StandardController sc = new ApexPages.standardController(acc);
         ApexPages.currentPage().getParameters().put('AccountId',acc.id);
        CS_ARTransactionHistoryCtr controller = new CS_ARTransactionHistoryCtr(sc);
        controller.getTransactionclass();
        controller.Transactionclass ='Invoice';
    }
    
    public static testMethod void testLineofBusiness(){
        Account acc = new Account(Name = 'TestAccountName');
	    insert acc;
        ApexPages.StandardController sc = new ApexPages.standardController(acc);
         ApexPages.currentPage().getParameters().put('AccountId',acc.id);
        CS_ARTransactionHistoryCtr controller = new CS_ARTransactionHistoryCtr(sc);
        controller.getTransactionType();
        controller.TransactionType ='New Development';
   }
    
    public static testMethod void testCancel(){    
       PageReference pageRef = Page.CS_ARTransactionHistory;
       Account acc = new Account(Name = 'TestAccountName');
	   insert acc;
       Test.setCurrentPage(pageRef);
       ApexPages.currentPage().getParameters().put('AccountId',acc.id);
       ApexPages.StandardController sc = new ApexPages.standardController(acc);
       CS_ARTransactionHistoryCtr controller = new CS_ARTransactionHistoryCtr(sc);
       System.assertNotEquals(null,controller.Cancel());
        
    }
    
    
    public static testMethod void testInvWrapper(){
        
        CS_ARTransactionHistoryCtr.invwrapper testwrapper = new CS_ARTransactionHistoryCtr.invwrapper();
        testwrapper.Accountname = 'testAccount';
        testwrapper.transactionClass = 'Credit Memo';
        testwrapper.transactionDate = '2017-03-08';
        testwrapper.totalTaxableAmount = '2475';
        testwrapper.totalOutstandingAmount = '1000';
        testwrapper.transactionDueDate = '2017-03-08';
        testwrapper.status = 'Current';
        testwrapper.productReference = 'CM-New Dev Invoice';
        testwrapper.originalReference = '228473478';
        testwrapper.hostReferenceId = 'BE-38-5';
        List<invwrapper> testinv = new List<invwrapper>();
     }
    
    public static testMethod void testAccountwrapper(){
        CS_ARTransactionHistoryCtr.Accountwrapper testwrapperacc = new CS_ARTransactionHistoryCtr.Accountwrapper();
        testwrapperacc.Accountname = 'Amazon';
        testwrapperacc.balance = '12345';
        testwrapperacc.status = 'overdue';
        List<CS_ARTransactionHistoryCtr.Accountwrapper> testacc = new List<CS_ARTransactionHistoryCtr.Accountwrapper>();
        testacc.add(testwrapperacc);
    }
    
    public static testMethod void testApplyFilter(){
		Account acc = new Account(Name = 'TestAccountName');
	   	insert acc;
	   	
       	ApexPages.currentPage().getParameters().put('AccountId',acc.id);
       	ApexPages.StandardController sc = new ApexPages.standardController(acc);
       	CS_ARTransactionHistoryCtr controller = new CS_ARTransactionHistoryCtr(sc);
        Continuation cont = new Continuation(120);
         
        AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture AsynNBN = new AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture();
        AsyncWwwNbncoComAuServiceArtransactionhi.AsyncARTransactionHistoryBindingSOAPQSPort ARTrans = new AsyncWwwNbncoComAuServiceArtransactionhi.AsyncARTransactionHistoryBindingSOAPQSPort(); 
       	ARTrans.reqHeader = new wwwNbnComAuDmmEnterpriseCommon.SOAHeader_element();
       	ARTrans.reqHeader.correlationId ='Scen4180118';
       	ARTrans.reqHeader.businessChannel = 'SFDC';
       	ARTrans.reqHeader.channelMedia = 'PSaaS';
        wwwNbncoComAuServiceArtransactionhi.Customer_element Customer = new wwwNbncoComAuServiceArtransactionhi.Customer_element();
        Customer.CustomerIdentifier = new  wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element();
        Customer.CustomerIdentifier.sourceReferenceId = acc.id;
        wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element customerreq = new wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element();
        wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element CustomerIdentifier = new wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element();
        CustomerIdentifier.sourceReferenceId = '9792250';
        
        wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element AccountIdentifier = new wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element();
        AccountIdentifier.sourceReferenceId = 'AYCA-3NS0Y8';
        
        wwwNbncoComAuServiceArtransactionhi.Account_element Account = new wwwNbncoComAuServiceArtransactionhi.Account_element();
        Account.AccountIdentifier = AccountIdentifier;
        
        customerreq.CustomerIdentifier = CustomerIdentifier;
        customerreq.Account = Account;
        
        
        wwwNbncoComAuServiceArtransactionhi.Invoice_element Invelement = new wwwNbncoComAuServiceArtransactionhi.Invoice_element();
        wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element InvoiceHeader = new wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element();
        InvoiceHeader.sourceReferenceNumber = 'SALESFORCE';
        Invelement.InvoiceHeader = InvoiceHeader;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class,new syncServiceArtransactionMockImpl());
      //  AsynNBN = ARTrans.beginRetrieveARTransactionHistory(cont,customerreq,Invelement,'20160528','20170227','1','creditMemo');
        controller.ApplyFilter();
        wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse respElement = new wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse();	
        Test.stopTest();
     }
     
     public static testMethod void testApplyFilter_AccountWithParent(){
		Account parentAcc = new Account(Name = 'TestParentAccountName');
	   	insert parentAcc;
		
		Account acc = new Account(Name = 'TestChildAccountName', ParentId = parentAcc.Id);
	   	insert acc;
	   	
       	ApexPages.currentPage().getParameters().put('AccountId',acc.id);
       	ApexPages.StandardController sc = new ApexPages.standardController(acc);
       	CS_ARTransactionHistoryCtr controller = new CS_ARTransactionHistoryCtr(sc);
        Continuation cont = new Continuation(120);
         
        AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture AsynNBN = new AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture();
        AsyncWwwNbncoComAuServiceArtransactionhi.AsyncARTransactionHistoryBindingSOAPQSPort ARTrans = new AsyncWwwNbncoComAuServiceArtransactionhi.AsyncARTransactionHistoryBindingSOAPQSPort(); 
       	ARTrans.reqHeader = new wwwNbnComAuDmmEnterpriseCommon.SOAHeader_element();
       	ARTrans.reqHeader.correlationId ='Scen4180118';
       	ARTrans.reqHeader.businessChannel = 'SFDC';
       	ARTrans.reqHeader.channelMedia = 'PSaaS';
        wwwNbncoComAuServiceArtransactionhi.Customer_element Customer = new wwwNbncoComAuServiceArtransactionhi.Customer_element();
        Customer.CustomerIdentifier = new  wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element();
        Customer.CustomerIdentifier.sourceReferenceId = acc.id;
        wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element customerreq = new wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element();
        wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element CustomerIdentifier = new wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element();
        CustomerIdentifier.sourceReferenceId = '9792250';
        
        wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element AccountIdentifier = new wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element();
        AccountIdentifier.sourceReferenceId = 'AYCA-3NS0Y8';
        
        wwwNbncoComAuServiceArtransactionhi.Account_element Account = new wwwNbncoComAuServiceArtransactionhi.Account_element();
        Account.AccountIdentifier = AccountIdentifier;
        
        customerreq.CustomerIdentifier = CustomerIdentifier;
        customerreq.Account = Account;
        
        
        wwwNbncoComAuServiceArtransactionhi.Invoice_element Invelement = new wwwNbncoComAuServiceArtransactionhi.Invoice_element();
        wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element InvoiceHeader = new wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element();
        InvoiceHeader.sourceReferenceNumber = 'SALESFORCE';
        Invelement.InvoiceHeader = InvoiceHeader;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class,new syncServiceArtransactionMockImpl());
      //  AsynNBN = ARTrans.beginRetrieveARTransactionHistory(cont,customerreq,Invelement,'20160528','20170227','1','creditMemo');
        controller.ApplyFilter();
        wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse respElement = new wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse();	
        Test.stopTest();
     }
    
    /*public static testMethod void testprocessresp(){
      
       Account acc = new Account(Name = 'TestAccountName');
	   insert acc;
       ApexPages.currentPage().getParameters().put('AccountId',acc.id);
       ApexPages.StandardController sc = new ApexPages.standardController(acc);
       CS_ARTransactionHistoryCtr controller = new CS_ARTransactionHistoryCtr(sc);
       AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture AsynNBN = new AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture();
        wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element invHeader = new wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element();
        wwwNbncoComAuServiceArtransactionhi.AccountList_element acclst = new wwwNbncoComAuServiceArtransactionhi.AccountList_element();
        wwwNbncoComAuServiceArtransactionhi.Correlation_element corr = new wwwNbncoComAuServiceArtransactionhi.Correlation_element();
        wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryRequest ARTransHisReq = new wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryRequest();
        wwwNbncoComAuServiceArtransactionhi.InvoiceRecord_element invrec = new wwwNbncoComAuServiceArtransactionhi.InvoiceRecord_element();
        wwwNbncoComAuServiceArtransactionhi.InvoiceList_element invlst = new wwwNbncoComAuServiceArtransactionhi.InvoiceList_element();
        wwwNbncoComAuServiceArtransactionhi.CustomerDetails_element custdet = new wwwNbncoComAuServiceArtransactionhi.CustomerDetails_element();
        wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element custIden = new wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element();
        wwwNbncoComAuServiceArtransactionhi.AccountRecord_element accRec = new wwwNbncoComAuServiceArtransactionhi.AccountRecord_element();
        wwwNbncoComAuServiceArtransactionhi.AccountDetails_element accdet = new wwwNbncoComAuServiceArtransactionhi.AccountDetails_element();
        wwwNbncoComAuServiceArtransactionhi.Customer_element custelement = new wwwNbncoComAuServiceArtransactionhi.Customer_element ();
        wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element custreq = new wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element ();
        wwwNbnComAuDmmEnterpriseCommon.ResultStatus_element ResultStatus= new wwwNbnComAuDmmEnterpriseCommon.ResultStatus_element();
        wwwNbnComAuDmmEnterpriseCommon.SOAHeader_element  SOAHeader= new wwwNbnComAuDmmEnterpriseCommon.SOAHeader_element();
        wwwNbnComAuDmmEnterpriseCommon.SOAResponseHeader_element SOAResponse= new wwwNbnComAuDmmEnterpriseCommon.SOAResponseHeader_element();
        wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element accIden = new wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element();
        wwwNbncoComAuServiceArtransactionhi.Account_element accelement = new wwwNbncoComAuServiceArtransactionhi.Account_element ();
         wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse response = new wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponse();
      
        wwwNbncoComAuServiceArtransactionhi.ARTransactionHistoryBindingSOAPQSPort NBNCom = new wwwNbncoComAuServiceArtransactionhi.ARTransactionHistoryBindingSOAPQSPort();
        wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryRequest retARTrans = new wwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryRequest();
        wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element customerreq = new wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element();
        response = AsynNBN.getValue();
        
        
        wwwNbncoComAuServiceArtransactionhi.Invoice_element Invelement = new wwwNbncoComAuServiceArtransactionhi.Invoice_element();
        Test.startTest();
        
        Test.setMock(WebServiceMock.class,new syncServiceArtransactionMockImpl());
       // controller.ApplyFilter();
        controller.processResponse();
        Test.stopTest();
     } */
     
     public static testMethod void testprocessresp_method1() {
     	Account acc = new Account(Name = 'TestAccountName');
	   	insert acc;
	   	
	   	ApexPages.currentPage().getParameters().put('AccountId',acc.id);
       	ApexPages.StandardController sc = new ApexPages.standardController(acc);
       	CS_ARTransactionHistoryCtr controller = new CS_ARTransactionHistoryCtr(sc);
        
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new syncServiceArtransactionMockImpl());
        
        controller.processResponse();
        
        controller.hostReferenceId = '1000457';
       	controller.processResponse();
        
       	controller.OriginalReference = 'CW-00000356';
		controller.hostReferenceId = '1000457';
       	controller.processResponse();
       	
       	Test.stopTest();
     }
     
     public static testMethod void testprocessresp_method2() {
     	Account acc = new Account(Name = 'TestAccountName');
	   	insert acc;
	   	
	   	ApexPages.currentPage().getParameters().put('AccountId',acc.id);
       	ApexPages.StandardController sc = new ApexPages.standardController(acc);
       	CS_ARTransactionHistoryCtr controller = new CS_ARTransactionHistoryCtr(sc);
        
        Test.startTest();
        
        //Test.setMock(WebServiceMock.class, new syncServiceArtransactionMockImpl());
        
       	controller.processResponse();
       	
       	Test.stopTest();
     }
     
     public static testMethod void testprocessresp_method3() {
        Test.startTest();
        
        Account acc = new Account(Name = 'TestAccountName');
	   	insert acc;
	   	
	   	ApexPages.currentPage().getParameters().put('AccountId',acc.id);
       	ApexPages.StandardController sc = new ApexPages.standardController(acc);
       	CS_ARTransactionHistoryCtr controller = new CS_ARTransactionHistoryCtr(sc);
        
        Test.setMock(WebServiceMock.class, new syncServiceArtransactionMockImpl());
        
        controller.processResponse();
        
        Test.stopTest();
     }
     
     public static testMethod void testprocessresp_method4() {
		Account acc = new Account(Name = 'TestAccountName');
	   	insert acc;
	   	
	   	ApexPages.currentPage().getParameters().put('AccountId',acc.id);
       	ApexPages.StandardController sc = new ApexPages.standardController(acc);
       	CS_ARTransactionHistoryCtr controller = new CS_ARTransactionHistoryCtr(sc);
        
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new syncErrorServiceArtransactionMockImpl());
        
        controller.processResponse();
        
        Test.stopTest();
     }
     
     public static testMethod void testprocessresp_invalidFromDate() {
     	Account acc = new Account(Name = 'TestAccountName');
	   	insert acc;
	   	
       	ApexPages.currentPage().getParameters().put('AccountId',acc.id);
       	ApexPages.StandardController sc = new ApexPages.standardController(acc);
       	CS_ARTransactionHistoryCtr controller = new CS_ARTransactionHistoryCtr(sc);
        Continuation cont = new Continuation(120);
         
        AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture AsynNBN = new AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture();
        AsyncWwwNbncoComAuServiceArtransactionhi.AsyncARTransactionHistoryBindingSOAPQSPort ARTrans = new AsyncWwwNbncoComAuServiceArtransactionhi.AsyncARTransactionHistoryBindingSOAPQSPort(); 
       	ARTrans.reqHeader = new wwwNbnComAuDmmEnterpriseCommon.SOAHeader_element();
       	ARTrans.reqHeader.correlationId ='Scen4180118';
       	ARTrans.reqHeader.businessChannel = 'SFDC';
       	ARTrans.reqHeader.channelMedia = 'PSaaS';
        wwwNbncoComAuServiceArtransactionhi.Customer_element Customer = new wwwNbncoComAuServiceArtransactionhi.Customer_element();
        Customer.CustomerIdentifier = new  wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element();
        Customer.CustomerIdentifier.sourceReferenceId = acc.id;
        wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element customerreq = new wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element();
        wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element CustomerIdentifier = new wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element();
        CustomerIdentifier.sourceReferenceId = '9792250';
        
        wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element AccountIdentifier = new wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element();
        AccountIdentifier.sourceReferenceId = 'AYCA-3NS0Y8';
        
        wwwNbncoComAuServiceArtransactionhi.Account_element Account = new wwwNbncoComAuServiceArtransactionhi.Account_element();
        Account.AccountIdentifier = AccountIdentifier;
        
        customerreq.CustomerIdentifier = CustomerIdentifier;
        customerreq.Account = Account;
        
        
        wwwNbncoComAuServiceArtransactionhi.Invoice_element Invelement = new wwwNbncoComAuServiceArtransactionhi.Invoice_element();
        wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element InvoiceHeader = new wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element();
        InvoiceHeader.sourceReferenceNumber = 'SALESFORCE';
        Invelement.InvoiceHeader = InvoiceHeader;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class,new syncServiceArtransactionMockImpl());
     
		controller.fromdate = '2017-13-10';
        controller.ApplyFilter();
       	
        Test.stopTest();
     }
     
     public static testMethod void testprocessresp_invalidToDate() {
     	Account acc = new Account(Name = 'TestAccountName');
	   	insert acc;
	   	
       	ApexPages.currentPage().getParameters().put('AccountId',acc.id);
       	ApexPages.StandardController sc = new ApexPages.standardController(acc);
       	CS_ARTransactionHistoryCtr controller = new CS_ARTransactionHistoryCtr(sc);
        Continuation cont = new Continuation(120);
         
        AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture AsynNBN = new AsyncWwwNbncoComAuServiceArtransactionhi.RetrieveARTransactionHistoryResponseFuture();
        AsyncWwwNbncoComAuServiceArtransactionhi.AsyncARTransactionHistoryBindingSOAPQSPort ARTrans = new AsyncWwwNbncoComAuServiceArtransactionhi.AsyncARTransactionHistoryBindingSOAPQSPort(); 
       	ARTrans.reqHeader = new wwwNbnComAuDmmEnterpriseCommon.SOAHeader_element();
       	ARTrans.reqHeader.correlationId ='Scen4180118';
       	ARTrans.reqHeader.businessChannel = 'SFDC';
       	ARTrans.reqHeader.channelMedia = 'PSaaS';
        wwwNbncoComAuServiceArtransactionhi.Customer_element Customer = new wwwNbncoComAuServiceArtransactionhi.Customer_element();
        Customer.CustomerIdentifier = new  wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element();
        Customer.CustomerIdentifier.sourceReferenceId = acc.id;
        wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element customerreq = new wwwNbncoComAuServiceArtransactionhi.CustomerReqWithOptionalAccount_element();
        wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element CustomerIdentifier = new wwwNbncoComAuServiceArtransactionhi.CustomerIdentifier_element();
        CustomerIdentifier.sourceReferenceId = '9792250';
        
        wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element AccountIdentifier = new wwwNbncoComAuServiceArtransactionhi.AccountIdentifier_element();
        AccountIdentifier.sourceReferenceId = 'AYCA-3NS0Y8';
        
        wwwNbncoComAuServiceArtransactionhi.Account_element Account = new wwwNbncoComAuServiceArtransactionhi.Account_element();
        Account.AccountIdentifier = AccountIdentifier;
        
        customerreq.CustomerIdentifier = CustomerIdentifier;
        customerreq.Account = Account;
        
        
        wwwNbncoComAuServiceArtransactionhi.Invoice_element Invelement = new wwwNbncoComAuServiceArtransactionhi.Invoice_element();
        wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element InvoiceHeader = new wwwNbncoComAuServiceArtransactionhi.InvoiceHeader_element();
        InvoiceHeader.sourceReferenceNumber = 'SALESFORCE';
        Invelement.InvoiceHeader = InvoiceHeader;
        
        Test.startTest();
        Test.setMock(WebServiceMock.class,new syncServiceArtransactionMockImpl());
        
        controller.todate = '2017-13-20';
        controller.ApplyFilter();
       	
        Test.stopTest();
     }
    
}