public Class CS_AccountTriggerHandler extends Triggerhandler
{
    public static boolean isError = false;
    public CS_AccountTriggerHandler()
    {
        if(Test.isRunningTest()){
            this.setMaxLoopCount(10);
        }
        else{
            this.setMaxLoopCount(2);
        }
        System.Debug( 'NBN: -> CS_AccountTriggerHandler Invoked' );
    }
    
    protected override void afterDelete(Map<id,SObject> oldMap)
    {
        UpdateChildCount(oldMap);
        
    }
    protected override void afterUndelete(Map<id,SObject> newMap)
    {
        UpdateChildCount(newMap);
    }

    protected override void afterInsert(Map<id,SObject> newMap)
    {
        UpdateChildCount(newMap);
        updateAccTypeForICTAccs(newMap);
    }

    protected override void afterUpdate(map<id,SObject> oldMap, map<id,SObject> newMap)
    {
        UpdateAllParentChildCount(oldMap,newMap);
        grpMmbrshpOnEngmntStatusChnge(oldMap,newMap);
    }

    protected override void beforeUpdate(map<id,SObject> oldMap, map<id,SObject> newMap)
    {
            
            checkForUnpaidBalance(oldMap, newMap);
            if(!isError)
                setChildAccountsInactive(oldMap, newMap);
    }

    //AC-2292 Inactive Child Accounts when Parent is made Inactive
    public void setChildAccountsInactive(map<id,SObject> oldMap,map<id,SObject> newMap)
    {
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Account.getRecordTypeInfosById();
        List<Id> accIds = new List<Id>();
        List<String> lstAccountRecordTypes = new List<String>();
        lstAccountRecordTypes.add('Property Developer');
        lstAccountRecordTypes.add('Company');
        lstAccountRecordTypes.add('Residential');

        for(SObject obj: newmap.values())
        {
            Account parentAcc = (Account)obj;
            Account oldParentAcc = (Account)(oldMap.get(obj.Id));
            if(parentAcc.Status__c != null && parentAcc.Status__c !='' && parentAcc.Status__c == 'Inactive' && oldParentAcc.Status__c != parentAcc.Status__c)
            {
                if(rtMap.get(parentAcc.RecordTypeId).getName() == 'Property Developer' || 
                    rtMap.get(parentAcc.RecordTypeId).getName() == 'Company' ||
                    rtMap.get(parentAcc.RecordTypeId).getName() == 'Residential' )
                {
                    accIds.add(obj.Id);
                }
            }
        }
        List<Account> childAccounts=[Select Id,Status__c from Account where ParentId in:accids and Status__c != 'Inactive'
                                        and RecordType.Name in:lstAccountRecordTypes];
        for(Account acc:childAccounts)
        {
            acc.Status__c = 'Inactive';
        }
        update childAccounts;
    }

  

    // AC-487: Cannot set account to inactive if there are unpaid balance
    public void checkForUnpaidBalance (map<id,SObject> oldMap, map<id,SObject> newMap) {
        map<Id, Decimal> accBalMap = new map<Id, Decimal>();
        map<Id, String> accIdNameMap = new map<Id, String>();

        set<Id> updSet = new set<Id>();

        for (SObject obj: newMap.values()) {
            Account acc = (Account) obj;
            Account oldAcc = (Account)oldMap.get(acc.Id);
            if (acc.Status__c == 'Inactive' && oldAcc.Status__c != 'Inactive') {
                updSet.add(acc.Id);
                if(!accIdNameMap.containsKey(acc.Id))
                {
                    accIdNameMap.put(acc.Id,acc.Name);
                }
                
            }
        }

        List<Account> childAccounts=[Select Id,Status__c,ParentId,Name from Account where ParentId in:updSet and Status__c != 'Inactive'];
        //and RecordType.Name in:lstAccountRecordTypes];
        map<Id,Id> mapParentChildIds = new map<Id,Id>();
        for(Account acc:childAccounts)
        {
            updSet.add(acc.Id);
            if(!mapParentChildIds.containsKey(acc.Id))mapParentChildIds.put(acc.Id,acc.ParentId);
            if(!accIdNameMap.containsKey(acc.Id))
            {
                    accIdNameMap.put(acc.Id,acc.Name);
            }
        }
        


        set<Id> errSet = new set<Id>();
        if (!updSet.isEmpty()) {
            AggregateResult[] groupedResults = [SELECT Account__c, SUM(Unpaid_Balance__c)unpaidBalance FROM Invoice__c where Account__c in :updSet group by Account__c];
            for (AggregateResult ar: groupedResults) {
                if (ar.get('unpaidBalance') != null && (Decimal)ar.get('unpaidBalance') > 0) {
                    errSet.add((Id)ar.get('Account__c'));
                }
            } 
        }
        if (!errSet.isEmpty()) {
            for (Id accId : errSet) {
                isError = true;
                string accName = '';
                if(accIdNameMap.containsKey(accId))accName = accIdNameMap.get(accId);
                if(newMap.containsKey(accId))
                    newMap.get(accId).addError('The Account:' + accName + ' cannot be Inactivated as the account has Open invoices. Please follow-up and close the Open invoices.');
                else if(mapParentChildIds.containsKey(accId) && newMap.containsKey(mapParentChildIds.get(accId)))
                {
                    newMap.get(mapParentChildIds.get(accId)).addError('The Account:' + accName + ' cannot be Inactivated as the account has Open invoices. Please follow-up and close the Open invoices.');
                }
            }            
        }
        //else
        //{
        //    setChildAccountsInactive(oldMap, newMap);
        //}
    }
    

    public void UpdateChildCount(Map<Id,SObject> mapIdAccount)
    {
        List<Id> parentIds=new List<Id>();

        for(SObject obj : mapIdAccount.values())
        {
            Account acc=(Account)obj;
            parentIds.add(acc.ParentId);

        }
        List<Account> accountListToUpdate = new List<Account>();
        for(Account acc : [Select Count_Of_Children__c,(Select Id from ChildAccounts) from Account where Id in:parentIds])
        {
            acc.Count_Of_Children__c = acc.ChildAccounts.size();
            accountListToUpdate.add(acc);
        }

        update accountListToUpdate;
    }
    
    public void UpdateAllParentChildCount(Map<Id,SObject> mapIdOldAccount,Map<Id,SObject> mapIdNewAccount)
    {
        List<Id> parentIds=new List<Id>();
        for(SObject obj : mapIdNewAccount.values())
        {
            Account acc=(Account)obj;
            parentIds.add(acc.ParentId);
        }
        for(SObject obj : mapIdOldAccount.values())
        {
            Account acc=(Account)obj;
            parentIds.add(acc.ParentId);

        }

        List<Account> accountListToUpdate = new List<Account>();
        for(Account acc : [Select Count_Of_Children__c,(Select Id from ChildAccounts) from Account where Id in:parentIds])
        {
            acc.Count_Of_Children__c = acc.ChildAccounts.size();
            accountListToUpdate.add(acc);
        }

        update accountListToUpdate;
    }
    
    /***
        User Story: 9486
        Description: This method calls ICTcontactMapping method of NewContactTriggerHandler class for assignment/unassignment of public groups on the basis of Engagement Status as part of resource profiling.
        Created by: Jairaj Jadhav
        Created Date: 13-03-2018
    ***/
    public void grpMmbrshpOnEngmntStatusChnge(Map<Id,SObject> mapIdOldAccount,Map<Id,SObject> mapIdNewAccount){
        set<id> engmntSttsChngAccIDs = new set<id>();
        try{
            for(SObject obj : mapIdNewAccount.values()){
                Account acc=(Account)obj;
                Account oldAcc=(Account)mapIdOldAccount.get(acc.id);
                if(oldAcc.Engagement_Status__c != acc.Engagement_Status__c && acc.RecordTypeId == schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Partner Account').getRecordTypeId()){
                    engmntSttsChngAccIDs.add(obj.id);
                }
            }
            
            if(!engmntSttsChngAccIDs.isEmpty() && engmntSttsChngAccIDs.size() > 0){
                id prtnrRcdtypID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Partner Contact').getRecordTypeId();
                Map<id,Contact> cntctIDCntctMap = new map<id,contact>([select id,recordtypeid, ICT_Community_User__c, ICT_Contact_Role__c from contact where ICT_Community_User__c = true AND recordtypeid=:prtnrRcdtypID AND AccountID IN: engmntSttsChngAccIDs]);
                if(!cntctIDCntctMap.keySet().isEmpty() && cntctIDCntctMap.keySet().size() > 0){
                    NewContactTriggerHandler contactTriggerHandler = new NewContactTriggerHandler(true, 5, null, null, null, null);
                    system.debug('before calling ICTcontactMapping method from Account trigger cntctIDCntctMap==>'+cntctIDCntctMap);
                    contactTriggerHandler.ICTcontactMapping(true, null, cntctIDCntctMap);
                }
            }
        }catch(Exception ex){
            GlobalUtility.logMessage('Error','ICT Partner Program Community','Public Group Membership','','Public Group Membership add remove','grpMmbrshpOnEngmntStatusChnge Error','',ex,0);
            system.debug('Exception:'+ex);
        }
    }
    
    /***
        User Story: 14584
        Description: This method updates the Account Types if an ICT Account is getting created and ICT Channel Partner has not been selected.
        Created by: Jairaj Jadhav
        Created Date: 17-07-2018
    ***/
    public void updateAccTypeForICTAccs(Map<id,SObject> newMap){
        List<id> ICTAccIDList = new List<id>();
        List<Account> ictAccList = new List<Account>();
        try{
            list<id> ictUsrIDList = new list<id>(new Map<Id, user>([select id from user where profile.name='BSM ICT']).keySet());
            id partnrAccRecTypID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId();
            for(sObject obj: newMap.values()){
                Account acc = (Account)obj;
                if(acc.recordtypeid == partnrAccRecTypID && ictUsrIDList.contains(acc.createdbyid) && (acc.Account_Types__c == null || !acc.Account_Types__c.contains('ICT Channel Partner'))){
                    ICTAccIDList.add(acc.id);
                }
            }
            if(!ICTAccIDList.isEmpty() && ICTAccIDList.size() > 0){
                for(Account acct: [select id, Account_Types__c from Account where ID IN: ICTAccIDList]){
                    acct.Account_Types__c = (acct.Account_Types__c != null && acct.Account_Types__c != '') ? acct.Account_Types__c+';ICT Channel Partner' : 'ICT Channel Partner';
                    ictAccList.add(acct);
                }
                if(!ictAccList.isEmpty() && ictAccList.size() > 0){
                    update ictAccList;
                }
            }
        }catch(exception ex){
            GlobalUtility.logMessage('Error','ICT Partner Program Community','Account Type Update','','ICT Account Type Update','updateAccTypeForICTAccs Error','',ex,0);
            system.debug('Exception:'+ex);
        }
    }
}