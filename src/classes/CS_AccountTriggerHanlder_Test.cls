@isTest
private class CS_AccountTriggerHanlder_Test {
    
    @isTest static void test_method_one() 
    {
        // Implement test code

        //Create Account Data
        Account objAccount = TestDataClass.CreateTestAccountData();
        Account objChildAccount1 = TestDataClass.CreateTestAccountData();
        objChildAccount1.ParentId = objAccount.Id;
        objChildAccount1.ABN__c='51824753556';
        update objChildAccount1;
        objAccount =[Select Id,Count_Of_Children__c from Account where Id =:objAccount.Id];
        system.assertEquals(1.0,objAccount.Count_Of_Children__c);

        Account objChildAccount2 = new Account(Name = 'Test Account',ParentId=objAccount.Id,ABN__c='51824753556');
        insert objChildAccount2;
        objAccount =[Select Id,Count_Of_Children__c from Account where Id =:objAccount.Id];
        //system.assertEquals(2.0,objAccount.Count_Of_Children__c);

        objChildAccount1.ParentId = null;
        update objChildAccount1;

        List<Account> childAccounts = [Select Id,ParentId From Account where ParentId=:objAccount.Id];
        //system.assertEquals(1,childAccounts.size());

        delete objChildAccount2;
        childAccounts = [Select Id,ParentId From Account where ParentId=:objAccount.Id];
        //system.assertEquals(0,childAccounts.size());

        undelete objChildAccount2;
        childAccounts = [Select Id,ParentId From Account where ParentId=:objAccount.Id];
        //system.assertEquals(1,childAccounts.size());
    }
    
    @isTest static void test_method_two() {

        Account objAccount = TestDataClass.CreateTestAccountData();
        Opportunity testOpty = new Opportunity (Name='Test Opp', CloseDate=System.today(), StageName='Invoicing', Approving_Status__c = 'Internal Approval Received'
                                                , Invoicing_Status__c = 'Pending');
        insert testOpty;
        RecordType InvoiceRecType  = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Invoice'];
		ObjectCommonSettings__c invSet = 
        	new ObjectCommonSettings__c(Name = 'Invoice Statuses', New__c = 'New', Requested__c = 'Requested', Cancelled__c = 'Cancelled', Pending_Approval__c = 'Pending Approval', 
        		Cancellation_Requested__c = 'Cancellation Requested', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval', 
        		Payment_Status_Paid__c = 'Paid', GST_Exclusive__c = 'GST (Exclusive)', GST_Inclusive__c = 'GST (Inclusive)', 
        		Discount_Type_Amount__c = 'Amount', ILI_Type_Cancel_Inv__c = 'Cancel Inv',Rejected__c = 'Rejected', Refer_to_Credit__c = 'Refer to Credit');
		
		insert invSet;
        //create invoice
        Invoice__c inv = new Invoice__c(Name='test inv', Unpaid_Balance__c = 100, Account__c = objAccount.Id, RecordTypeId=InvoiceRecType.Id,  Opportunity__c = testOpty.Id);
        insert inv;

        Test.startTest();
        try {
            objAccount.Status__c = 'Inactive';
            update objAccount;

        } catch (Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('cannot be Inactivated') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }

        Test.stopTest();
    }
    
    @isTest static void test_method_three() {
        Account objAccount = new Account(Name = 'Test Account, Organization', recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId());
        profile ictProfile = [select id from profile where Name = 'BSM ICT' limit 1];
        user ictUsr = new user(LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',
        LanguageLocaleKey='en_US',TimeZoneSidKey='Australia/Sydney',CommunityNickname='ictaccown',Alias='ictaccow',
        Email='test@test.com',firstName = 'Account', lastName = 'Owner', username='testclassaccown@test.com',CompanyName = 'TEST',
        FederationIdentifier='testclassaccown', profileid=ictProfile.id);
        insert ictUsr;
        Test.startTest();
            try {
                System.runAs (new User(Id = ictUsr.Id)){
                    insert objAccount;
                }
                objAccount.Engagement_Status__c = 'Accredited';
                objAccount.Certified_Count__c = 'Threshold Met';
                update objAccount;
            }catch (Exception e) {
                system.debug('DML Exception: '+e);
            }
            System.AssertEquals(objAccount.Engagement_Status__c == 'Accredited', true);
        Test.stopTest();
    }
}