/*************************************************************************************************************************************************************
Class Name : CS_CancelInvoiceCtrExt
Created By : Srujana Priyanka
Purpose : This class is associated with visualforce page 'CS_CancelInvoice' which is used to cancel the invoice and update the status of the invoice record.
Modifications:
****************************************************************************************************************************************************************/

public without sharing class CS_CancelInvoiceCtrExt {

    public Invoice__c inv {get; set;}
    public Boolean cannotCancel {get;set;}
    
    public list<Invoice__c> invList;
    public String selectedReasonCode {get; set;}
    
    //Returns picklist values and api names for reason code
    public List<SelectOption> getReasonCodes() {       
        return CS_ChangeInvoiceUtils.getReasonCodes('Invoice__c');
    }

    public CS_CancelInvoiceCtrExt() {
//        this.inv = (Invoice__c)stdController.getRecord();
        String invId = ApexPages.currentPage().getParameters().get('Id');
        ObjectCommonSettings__c invoiceStatuses = ObjectCommonSettings__c.getValues('Invoice Statuses');
        this.inv = [select Id, Account__c, Opportunity__c, Solution__c, Unpaid_Balance__c, Status__c, Previous_Status__c, Reason_Code__c,
                            Name, Amount__c
        from Invoice__c where Id=:invId];
        
        //Id solId = [select Solution__c from Invoice__c where Id=:inv.Id].Solution__c;
        //list<Invoice__c> invList = [select Id from Invoice__c where Solution__c=:this.inv.Solution__c];

        list<Invoice__c> invlist = [select Id from Invoice__c where Status__c != :invoiceStatuses.Rejected__c
                                    and Status__c != :invoiceStatuses.Cancelled__c and RecordType.name != :invoiceStatuses.Record_Type_Credit_Memo__c
                                    and Solution__c=:this.inv.Solution__c];
        if (invList.size() < 2) {
           cannotCancel=true;
        }
    }

    public PageReference Cancel() {
            PageReference invPage = new ApexPages.StandardController(this.inv).view();
            invPage.setRedirect(true);
            return invPage;
    }

    public PageReference SaveBtn() {

        Boolean isError = false;
        string strPreviousStatus = this.inv.Previous_Status__c;
        string strCurrentStatus = this.inv.Status__c;
        ObjectCommonSettings__c invoiceStatuses = ObjectCommonSettings__c.getValues('Invoice Statuses');
    
        try{
            system.debug('RCode:' + this.inv.Reason_Code__c);
            this.inv.Reason_Code__c = selectedReasonCode;
            this.inv.Previous_Status__c = this.inv.Status__c;
            this.inv.Status__c = invoiceStatuses.Cancellation_Pending_Approval__c; //'Cancellation Pending Approval';
            update this.inv;
            
        	// Firing the approval process
			CS_ChangeInvoiceUtils.submitApproval(this.inv.Id); 		

            PageReference invPage = new ApexPages.StandardController(this.inv).view();
            invPage.setRedirect(true);
            return invPage;
        }
        catch(DMLException ex){
           ApexPages.addMessage(new Apexpages.message(Apexpages.Severity.Error,ex.getMessage()));
           isError = true;
            //return null;
        }

        if(isError)
        {
            try
            {
                this.inv.Previous_Status__c = strPreviousStatus;
                this.inv.Status__c = strCurrentStatus;
                this.inv.Reason_Code__c = null;
                update this.inv;

                }catch(Exception ex){}
        }
        return null;
    }
}