/********************************************************************************************
Class Name : CS_CancelInvoiceCtrExt_TEST
Created By : Srujana Priyanka
Purpose : Test class for CS_CancelInvoiceCtrExt
*********************************************************************************************/

@isTest
public class CS_CancelInvoiceCtrExt_TEST {
    
    @testsetup static void setupCommonData() {
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		
		User uEGM = new User 
			( Alias = 'egmstdt'
			, Email = 'executiveGeneralManager@cloudsensesolutions.com.au.cpqdev'
			, EmailEncodingKey = 'ISO-8859-1'
			, LastName = 'Testing'
			, LanguageLocaleKey = 'en_US'
			, LocaleSidKey = 'en_AU'
			, ProfileId = p.Id
			, TimeZoneSidKey = 'Australia/Sydney'
			, UserName = 'executiveGeneralManager@nbnco.com.au.cpqdev'
			);

		insert uEGM;
		
		User uGM = new User 
			( Alias = 'gmstandt'
			, Email = 'generalManager@cloudsensesolutions.com.au.cpqdev'
			, EmailEncodingKey = 'ISO-8859-1'
			, LastName = 'Testing'
			, LanguageLocaleKey = 'en_US'
			, LocaleSidKey = 'en_AU'
			, ProfileId = p.Id
			, TimeZoneSidKey = 'Australia/Sydney'
			, UserName = 'generalManager@nbnco.com.au.cpqdev'
			, managerid = uEGM.id
			);

		insert uGM;
		
		User umgr = new User 
			( Alias = 'mstandt'
			, Email = 'manafer@cloudsensesolutions.com.au.cpqdev'
			, EmailEncodingKey = 'ISO-8859-1'
			, LastName = 'Testing'
			, LanguageLocaleKey = 'en_US'
			, LocaleSidKey = 'en_AU'
			, ProfileId = p.Id
			, TimeZoneSidKey = 'Australia/Sydney'
			, UserName = 'manafer@nbnco.com.au.cpqdev'
			, managerid = uGM.id
			);

		insert umgr;

      	User u = new User
      		( Alias = 'standt'
      		, Email = 'sysAdminuser@cloudsensesolutions.com.au.cpqdev'
      		, EmailEncodingKey = 'ISO-8859-1'
      		, LastName = 'Testing'
      		, LanguageLocaleKey = 'en_US'
      		, LocaleSidKey = 'en_AU'
      		, ProfileId = p.Id
      		, TimeZoneSidKey = 'Australia/Sydney'
      		, UserName = 'sitUser@cloudsensesolutions.com'
      		, managerid = umgr.id
      		);
      	
      	insert u;
      	
      	ObjectCommonSettings__c invSet = 
        	new ObjectCommonSettings__c(Name = 'Invoice Statuses', New__c = 'New', Requested__c = 'Requested', Cancelled__c = 'Cancelled', Pending_Approval__c = 'Pending Approval', 
        		Cancellation_Requested__c = 'Cancellation Requested', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval', Discount_Type_Amount__c = 'Amount',
        		Rejected__c = 'Rejected', Refer_to_Credit__c = 'Refer to Credit');
		
		insert invSet;
		
		ObjectCommonSettings__c solSet = 
        	new ObjectCommonSettings__c(Name = 'Solution Statuses', New__c = 'New', Requested__c = 'Requested', Cancelled__c = 'Cancelled', Pending_Approval__c = 'Pending Approval', 
        		Cancellation_Requested__c = 'Cancellation Requested', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval', 
        			Rejected__c = 'Rejected', Delivery__c = 'Delivery', Complete__c = 'Complete');
		
		insert solSet;
	}
	
	static User getUser() {
		return [SELECT id, name from User where UserName = 'sitUser@cloudsensesolutions.com' LIMIT 1];
	}
	
	static User getUser2() {
		return [SELECT id, name from User where UserName = 'executiveGeneralManager@nbnco.com.au.cpqdev' LIMIT 1];
	}
    
    static testMethod void CancelInvoice1(){
        
        System.runAs(getUser()) {
        	test.startTest();
            Opportunity testOpty = new Opportunity (Name='Test Opp', CloseDate=System.today(), StageName='Invoicing', Approving_Status__c = 'Internal Approval Received'
      		, Invoicing_Status__c = 'Pending');
            insert testOpty;
        	RecordType InvoiceRecType  = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Invoice'];
	        Invoice__c inv = new Invoice__c(Status__c='New', RecordTypeId=InvoiceRecType.Id,  Opportunity__c = testOpty.Id);
	        insert inv;
	        
	
	//        Test.setCurrentPageReference(new PageReference('Page.myPage'));
	        System.currentPageReference().getParameters().put('id', inv.Id);
	        
	        CS_CancelInvoiceCtrExt ctrExt = new CS_CancelInvoiceCtrExt();
	        ctrExt.getReasonCodes();
	        ctrExt.Cancel();
	        ctrExt.SaveBtn();
	
	        Invoice__c invResult = [SELECT id, Status__c from Invoice__c where Id = :inv.Id];
	        system.assertEquals('Cancellation Pending Approval', invResult.Status__c);
	        
	        test.stopTest();
        }
        
    }
    
    static testMethod void CancelInvoice2(){
        
        System.runAs(getUser2()) {
        	test.startTest();
            Opportunity testOpty = new Opportunity (Name='Test Opp', CloseDate=System.today(), StageName='Invoicing', Approving_Status__c = 'Internal Approval Received'
      		, Invoicing_Status__c = 'Pending');
            insert testOpty;
        	RecordType InvoiceRecType  = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Invoice'];
            Invoice__c inv = new Invoice__c(Status__c='New', RecordTypeId=InvoiceRecType.Id,  Opportunity__c = testOpty.Id);
	        insert inv;
	
	        System.currentPageReference().getParameters().put('id', inv.Id);
	        
	        CS_CancelInvoiceCtrExt ctrExt = new CS_CancelInvoiceCtrExt();
	        ctrExt.SaveBtn();
	        
	        test.stopTest();
	        
	        Boolean hasMessages = ApexPages.hasMessages(Apexpages.Severity.Error);
			
			system.assertEquals(true, hasMessages);
        }
        
    }
    
	static testMethod void CancelInvoice3(){
       
        System.runAs(getUser()) {
			test.startTest();
            Opportunity testOpty = new Opportunity (Name='Test Opp', CloseDate=System.today(), StageName='Invoicing', Approving_Status__c = 'Internal Approval Received'
      		, Invoicing_Status__c = 'Pending');
            insert testOpty;
        	RecordType InvoiceRecType  = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Invoice'];
            Invoice__c inv = new Invoice__c(Status__c='Created', RecordTypeId=InvoiceRecType.Id,  Opportunity__c = testOpty.Id);
        	insert inv;
   
			System.currentPageReference().getParameters().put('id', inv.Id);
			
			CS_CancelInvoiceCtrExt ctrExt = new CS_CancelInvoiceCtrExt();
			ctrExt.selectedReasonCode = 'Accounts Receivables Billing Error';
			
			delete inv;
			
			ctrExt.SaveBtn();
			
			test.stopTest();
			  
			Boolean hasMessages = ApexPages.hasMessages(Apexpages.Severity.Error);
			        
			system.assertEquals(true, hasMessages);
		}
       
	}
    
    
 }