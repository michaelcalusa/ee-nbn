/*************************************************
 Developed by: Li Tan
 Description: This class is associated with visualforce page 'CS_CancelSolution' which is used to cancel the Solution (Customer Order) and update the status of the soloice record.
 Version History:
 - v1.0, 2017-03-02, LT: Created
*/

public class CS_CancelSolutionCtrExt {

    public csord__Solution__c sol{get; set;}
    public String selectedReasonCode {get; set;}
    public Double totalAmount {
        get {
            
            AggregateResult[] groupedResults = [SELECT SUM(Amount__c)amt FROM csord__Order__c where csord__Solution__c=:sol.Id];
            return (Double)groupedResults[0].get('amt');
        }
    }
    
    public CS_CancelSolutionCtrExt(ApexPages.StandardController stdController) {
        this.sol = (csord__Solution__c)stdController.getRecord();
        this.sol = [select Id, Opportunity__c, Name, Status__c, Previous_Status__c,Reason_Code__c from csord__Solution__c where Id=:this.sol.Id];
    }

    //Returns picklist values and api names for reason code
    public List<SelectOption> getReasonCodes() {       
        return CS_ChangeInvoiceUtils.getReasonCodes('csord__Solution__c');
    }

    public PageReference Cancel() {
            PageReference solPage = new ApexPages.StandardController(this.sol).view();
            solPage.setRedirect(true);
            return solPage;
    }

    public PageReference Save() {
        Boolean isError = false;
        string strPreviousStatus = this.sol.Previous_Status__c;
        string strCurrentStatus = this.sol.Status__c;
        ObjectCommonSettings__c solutionStatuses = ObjectCommonSettings__c.getValues('Solution Statuses');
        
        try{
            system.debug('RCode:' + this.sol.Reason_Code__c);
            this.sol.Reason_Code__c = selectedReasonCode;
            this.sol.Previous_Status__c = this.sol.Status__c;
            this.sol.Status__c = solutionStatuses.Cancellation_Pending_Approval__c; //'Cancellation Pending Approval';
            this.sol.Amount__c = totalAmount;
            //this.sol.Reason_Code__c = this.sol.Reason_Code__c;
            update this.sol;
            
        	// Firing the approval process
			CS_ChangeInvoiceUtils.submitApproval(this.sol.Id);

            PageReference solPage = new ApexPages.StandardController(this.sol).view();
            solPage.setRedirect(true);
            return solPage;
        }
        catch(DMLException ex){
           ApexPages.addMessage(new Apexpages.message(Apexpages.Severity.Error,ex.getMessage()));
           isError = true;
            //return null;
        }

        if(isError)
        {
            try
            {
                this.sol.Previous_Status__c = strPreviousStatus;
                this.sol.Status__c = strCurrentStatus;
                this.sol.Amount__c = totalAmount;
                update this.sol;

                }catch(Exception ex){}
        }
        return null;
    }
}