/*************************************************
 Developed by: Li Tan
 Description: Test class for CS_CancelSolutionCtrExt
 Version History:
 - v1.0, 2017-03-09, LT: Created
*/

@isTest
private class CS_CancelSolutionCtrExt_TEST {
	
	@testsetup static void setupCommonData() {
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		
		User uEGM = new User 
			( Alias = 'mstandt'
			, Email = 'executiveGeneralManager@cloudsensesolutions.com.au.cpqdev'
			, EmailEncodingKey = 'ISO-8859-1'
			, LastName = 'Testing'
			, LanguageLocaleKey = 'en_US'
			, LocaleSidKey = 'en_AU'
			, ProfileId = p.Id
			, TimeZoneSidKey = 'Australia/Sydney'
			, UserName = 'executiveGeneralManager@nbnco.com.au.cpqdev'
			);

		insert uEGM;
		
		User uGM = new User 
			( Alias = 'mstandt'
			, Email = 'generalManager@cloudsensesolutions.com.au.cpqdev'
			, EmailEncodingKey = 'ISO-8859-1'
			, LastName = 'Testing'
			, LanguageLocaleKey = 'en_US'
			, LocaleSidKey = 'en_AU'
			, ProfileId = p.Id
			, TimeZoneSidKey = 'Australia/Sydney'
			, UserName = 'generalManager@nbnco.com.au.cpqdev'
			, managerid = uEGM.id
			);

		insert uGM;
		
		User umgr = new User 
			( Alias = 'mstandt'
			, Email = 'manafer@cloudsensesolutions.com.au.cpqdev'
			, EmailEncodingKey = 'ISO-8859-1'
			, LastName = 'Testing'
			, LanguageLocaleKey = 'en_US'
			, LocaleSidKey = 'en_AU'
			, ProfileId = p.Id
			, TimeZoneSidKey = 'Australia/Sydney'
			, UserName = 'manafer@nbnco.com.au.cpqdev'
			, managerid = uGM.id
			);

		insert umgr;

      	User u = new User
      		( Alias = 'standt'
      		, Email = 'sysAdminuser@cloudsensesolutions.com.au.cpqdev'
      		, EmailEncodingKey = 'ISO-8859-1'
      		, LastName = 'Testing'
      		, LanguageLocaleKey = 'en_US'
      		, LocaleSidKey = 'en_AU'
      		, ProfileId = p.Id
      		, TimeZoneSidKey = 'Australia/Sydney'
      		, UserName = 'sitUser@cloudsensesolutions.com'
      		, managerid = umgr.id
      		);
      	
      	insert u;
      	
      	ObjectCommonSettings__c solSet = 
        	new ObjectCommonSettings__c(Name = 'Solution Statuses', New__c = 'New', Requested__c = 'Requested', Cancelled__c = 'Cancelled', Pending_Approval__c = 'Pending Approval', 
        		Cancellation_Requested__c = 'Cancellation Requested', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval');
		
		insert solSet;
	}
	
	static User getUser() {
		return [SELECT id, name from User where UserName = 'sitUser@cloudsensesolutions.com' LIMIT 1];
	}
	
	static User getUser2() {
		return [SELECT id, name from User where UserName = 'executiveGeneralManager@nbnco.com.au.cpqdev' LIMIT 1];
	}
	
	@isTest static void testCtrExt() {
		// Implement test code
		System.runAs(getUser()) {
			Test.startTest();
        
	        csord__Solution__c solution = new csord__Solution__c (
	            Name = 'testSolution'
	            , Status__c = 'New'
	            , csord__Identification__c = 'testIdentification'
	            );
	        insert solution;
	
	        ApexPages.StandardController ctr = new ApexPages.standardController(solution);
	        CS_CancelSolutionCtrExt ctrExt = new CS_CancelSolutionCtrExt(ctr);
	
	        ctrExt.Cancel();
	        ctrExt.Save();
	
	        csord__Solution__c solResult = [SELECT id, Status__c from csord__Solution__c where Id = :solution.Id];
	        system.assertEquals('Cancellation Pending Approval', solResult.Status__c);
	       
	        Test.stopTest();
		}
		
	}
	
	@isTest static void testCtrExt2() {
		// Implement test code
		System.runAs(getUser2()) {
			Test.startTest();
        
	        csord__Solution__c solution = new csord__Solution__c (
	            Name = 'testSolution'
	            , Status__c = 'New'
	            , csord__Identification__c = 'testIdentification'
	            );
	        insert solution;
	
	        ApexPages.StandardController ctr = new ApexPages.standardController(solution);
	        CS_CancelSolutionCtrExt ctrExt = new CS_CancelSolutionCtrExt(ctr);
	
	        ctrExt.Save();
	       
	        Test.stopTest();
	        
	        Boolean hasMessages = ApexPages.hasMessages(Apexpages.Severity.Error);
			
			system.assertEquals(true, hasMessages);
		}
		
	}
	
	@isTest static void testCtrExt3() {
		
		System.runAs(getUser()) {
			Test.startTest();
        
	        csord__Solution__c solution = new csord__Solution__c (
	            Name = 'testSolution'
	            , Status__c = 'New'
	            , csord__Identification__c = 'testIdentification'
	            );
	        insert solution;
	
	        ApexPages.StandardController ctr = new ApexPages.standardController(solution);
	        CS_CancelSolutionCtrExt ctrExt = new CS_CancelSolutionCtrExt(ctr);
	
			delete solution;
	
	        ctrExt.Save();
	       
	        Test.stopTest();
	        
	        Boolean hasMessages = ApexPages.hasMessages(Apexpages.Severity.Error);
			
			system.assertEquals(true, hasMessages);
		}
		
	}
}