public class CS_ChangeAmountInvoiceCtrExt {
    
    public Invoice_Line_Item__c ili{get;set;}
    public string unitPriceValue {get;set;}
    public String selectedReasonCode{get;set;}
    public Boolean cannotChangeAmount {get;set;}
    public decimal delta {get;set;}
    public string invoiceStatus {get;set;}
    public String iliId {get; set;}
    public Map<String, String> invoicetransactionTypeMap = new Map<String, String>();
    public Map<String, String> creditMemoTypeMap = new Map<String, String>();
    public Map<String, Map<String, String>> transactionTypeMappingsMap = new Map<String, Map<String, String>>();

    //Returns picklist values and api names for reason code
    public List<SelectOption> getReasonCodes() {       
        return CS_ChangeInvoiceUtils.getReasonCodes('Invoice_Line_Item__c');
    }


    public CS_ChangeAmountInvoiceCtrExt() {
    
        for (Transaction_Type_Mapping__mdt ttm : 
            [SELECT id, Credit_Memo_Transaction_Type__c, Invoice_Transaction_Type__c, Opportunity_Record_Type__c from Transaction_Type_Mapping__mdt]) 
        {
            invoicetransactionTypeMap.put(ttm.Opportunity_Record_Type__c, ttm.Invoice_Transaction_Type__c);
            creditMemoTypeMap.put(ttm.Opportunity_Record_Type__c, ttm.Credit_Memo_Transaction_Type__c);
        }
        transactionTypeMappingsMap.put('Invoice', invoicetransactionTypeMap);
        transactionTypeMappingsMap.put('Credit Memo', creditMemoTypeMap);
        
        
        //this.ili = (Invoice_Line_Item__c) controller.getRecord();
        iliId = ApexPages.currentPage().getParameters().get('Id');
        this.ili = [select Name, Invoice__c, Memo_Line__c, Tax_Code__c, Memo_Line_Id__c, Inc_GST__c, Description__c, Amount__c, Quantity__c, Unit_Price__c
                    from Invoice_Line_Item__c where Id = :iliId];
        invoiceStatus = [Select Invoice__r.Status__c from Invoice_Line_Item__c where id = :iliId].Invoice__r.Status__c;
    }
    
    public String getUnitPriceValue() {
        return unitPriceValue;
    }
    
    public String getShowDelta() {
        return String.valueOf(delta);
    }
    
    public Boolean isNumber(string str) {
        /*Pattern isnumbers = Pattern.Compile('^[0-9](?:\.[0-9]{1,2})?$');
        Matcher numberMatch = isnumbers.matcher(str);
        
        return numberMatch.Matches();*/
        
        Boolean isacceptedDecimal = true;
        if(str != null){
            try{
                if(Decimal.valueOf(str)< 0)isacceptedDecimal = false;
                if((Decimal.valueOf(str)).scale() > 2)isacceptedDecimal = false;

            }
            catch(TypeException e){
               isacceptedDecimal = false; 
            }
        }
        system.debug(isacceptedDecimal);
        return isacceptedDecimal;
    }
    
    public boolean validateNewUnitPrice(string unitPriceValue) {
        
        if (String.isBlank(unitPriceValue)) {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'New Unit Price value cannot be null'));
            return false;
            
        } else if (!unitPriceValue.isNumeric()) {
            
            if (!isNumber(unitPriceValue)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'New Unit Price value entered should be > 0 and should not have more than 2 decimal places'));
                return false;
            }
        }
        
        return true;
    }
    
    public PageReference CalculateDelta() {
        
        if (!validateNewUnitPrice(unitPriceValue)) {
            
            cannotChangeAmount = false;
            return null;
        }
        
        /*if (String.isBlank(unitPriceValue)) {
            cannotChangeAmount = false;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'New Unit Price value cannot be null'));
            return null;
            
        } else if (!unitPriceValue.isNumeric()) {
            cannotChangeAmount = false;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'New Unit Price value entered is not numeric'));
            return null;
        }*/
        
        cannotChangeAmount = true;
        
        Invoice_Line_Item__c invoiceLineItem = 
            [SELECT Id, Order__r.Unit_Price__c, Unit_Price__c from Invoice_Line_Item__c where id = :iliId];
        
        delta = decimal.valueOf(unitPriceValue) - invoiceLineItem.Order__r.Unit_Price__c;
        //2821
        //delta = decimal.valueOf(unitPriceValue) - invoiceLineItem.Unit_Price__c;
        
        return null;
    }
    
    public PageReference Cancel() {
        //cannotChangeAmount = true;
        
        PageReference pageRef = new PageReference('/' + iliId);
        
        return pageRef; 
    }
    
    public PageReference NewUnitPrice() 
    {
        PageReference pageRef;
        Savepoint sp = Database.setSavepoint();
        try
        {
            ObjectCommonSettings__c invoiceStatuses = ObjectCommonSettings__c.getValues('Invoice Statuses');
        
            if (!validateNewUnitPrice(unitPriceValue))
                return null;
            
            Invoice_Line_Item__c invoiceLineItem = 
                [SELECT Id, name, Invoice__r.Id, Quantity__c, Line_Number__c, Description__c, Unit_Price__c, Memo_Line__c, 
                    Memo_Line_Id__c, Inc_GST__c, Tax_Code__c, Order_Line_Item__c, Order__r.Unit_Price__c, Order__r.Quantity__c,
                    Order__r.Id, Order__r.csord__Identification__c, Order__r.Name, Order_Line_Item__r.csord__Discount_Type__c, Amount__c,
                    Invoice__r.Related_Invoice__c, Invoice__r.Related_Invoice__r.Id
                    from Invoice_Line_Item__c where id = :iliId];
            
            // Compare new unit price entered with the original unit price value
            //2821
            if (decimal.valueOf(unitPriceValue).setScale(2) == invoiceLineItem.Order__r.Unit_Price__c/*invoiceLineItem.Unit_Price__c*/) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'The change you are requesting is equal to the original value.'));
                return null;
            }        
                    
            // Get all Invoice Line Items associated with the same Invoice
            List<Invoice_Line_Item__c> iliList = (invoiceLineItem.Invoice__r.Related_Invoice__c == null) ?
                [SELECT id, name from Invoice_Line_Item__c where Invoice__c = :invoiceLineItem.Invoice__r.Id] : 
                    [SELECT id, name from Invoice_Line_Item__c where Invoice__c = : invoiceLineItem.Invoice__r.Related_Invoice__r.Id];  
            
            // first validation
            if (invoiceLineItem.Order__r.Quantity__c == 0) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Action not allowed because no quantity left'));
                return null;
            }
            
            // validate new value for Unit Price field
            if (decimal.valueOf(unitPriceValue) <= decimal.valueOf('0')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Action not allowed. New Unit Price entered must be bigger than zero. Change Quantity instead'));
                return null;
            }
                
            Invoice__c invoiceOriginal = 
                [SELECT id, name, Related_Invoice__c, Related_Invoice__r.Id, Opportunity__c, Account__c, Contract__c,
                    Opportunity__r.Id, Solution__c, Unpaid_Balance__c, Opportunity__r.RecordType.Name 
                    from Invoice__c where Id = :invoiceLineItem.Invoice__r.Id];
                
            /*AggregateResult[] sumOppUnitPrice = 
                [SELECT sum(unit_price__c) sumUP from csord__Order__c 
                where csordtelcoa__Opportunity__c = :invoiceOriginal.Opportunity__r.Id and name != :invoiceLineItem.Order__r.Name]; 
            
            // calculate final total Unit Price from the Orders (i.e. new Unit Price value of this Order plus sum of all unit prices of remaining orders)
            decimal finalTotalUnitPrice = 
                (iliList.size() > 1) ? decimal.valueOf(unitPriceValue) + decimal.valueOf(string.valueOf(sumOppUnitPrice[0].get('sumUP'))) : decimal.valueOf(unitPriceValue);
            
            system.debug('finalTotalUP: ' + finalTotalUnitPrice + '; sumOppUnitPrice: ' + Integer.valueOf(sumOppUnitPrice[0].get('sumUP')));*/
            
            // value entered is valid so let's calculate the line item delta value
            decimal delta = decimal.valueOf(unitPriceValue) - invoiceLineItem.Order__r.Unit_Price__c;

            //2821
            //decimal delta = decimal.valueOf(unitPriceValue) - invoiceLineItem.Unit_Price__c;
            
            // Compare new Unit Price value with old one:
            // i. If new order unit price is greater, issue the delta as a new invoice.
            // ii. If new order unit price is lesser, issue a credit note.
            Invoice__c creditMemoNew, invoiceNew;
            //PageReference pageRef;
            
            if (delta <= 0) {
                system.debug('A Credit Memo Line Item will be generated with unit price value equals to ' + delta);
                
                creditMemoNew = new Invoice__c 
                        ( Status__c = invoiceStatuses.Pending_Approval__c//'Pending Approval'
                        , Name = invoiceStatuses.Invoice_Name__c//'To be issued'
                        , Opportunity__c = invoiceOriginal.Opportunity__c
                        , Account__c = invoiceOriginal.Account__c
                        , Contract__c = invoiceOriginal.Contract__c
                        , RecordTypeId = [Select Id, Name From RecordType WHERE Name = :invoiceStatuses.Record_Type_Credit_Memo__c/*'Credit Memo'*/].Id
                        , Related_Invoice__c = invoiceOriginal.Id
                        , Solution__c = invoiceOriginal.Solution__c
                        , Transaction_Type__c = transactionTypeMappingsMap.get(invoiceStatuses.Record_Type_Credit_Memo__c/*'Credit Memo'*/).get(invoiceOriginal.Opportunity__r.RecordType.Name)
                        , Payment_Status__c = null
                        , Reason_Code__c = selectedReasonCode
                        );
                
                insert creditMemoNew;
                
                Invoice_Line_Item__c creditMemoLineItemNew = new Invoice_Line_Item__c
                        ( Line_Number__c = invoiceLineItem.Line_Number__c
                        , Description__c = invoiceLineItem.Description__c
                        , RecordTypeId = Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByName().get('Credit Memo Line Item').getRecordTypeId()
                        , Quantity__c = invoiceLineItem.Quantity__c//invoiceLineItem.Order__r.Quantity__c
                        , Unit_Price__c = delta
                        , Memo_Line__c = invoiceLineItem.Memo_Line__c
                        , Memo_Line_Id__c = invoiceLineItem.Memo_Line_Id__c
                        , Inc_GST__c = invoiceLineItem.Inc_GST__c
                        , Tax_Code__c = invoiceLineItem.Tax_Code__c
                        , Invoice__c = creditMemoNew.Id
                        , Reason_Code__c = selectedReasonCode
                        , Order__c = invoiceLineItem.Order__r.Id
                        , ILI_Type__c = invoiceStatuses.ILI_Type_Price_Change__c//'Price Change'
                        , Related_Line_Item__c = invoiceLineItem.Id
                        );
                        
                insert creditMemoLineItemNew;
                
                // Firing the approval process
                CS_ChangeInvoiceUtils.submitApproval(creditMemoNew.Id);
                
                pageRef = new PageReference('/' + creditMemoNew.Id);
                
            } else {
                system.debug('A new Invoice Line Item will be generated with quantity value equals to ' + delta);
                
                invoiceNew = new Invoice__c
                        ( Status__c = invoiceStatuses.Pending_Approval__c//'Pending Approval'
                        , Name = invoiceStatuses.Invoice_Name__c//'To be issued'
                        , Opportunity__c = invoiceOriginal.Opportunity__c
                        , Account__c = invoiceOriginal.Account__c
                        , Contract__c = invoiceOriginal.Contract__c
                        , RecordTypeId = [Select Id, Name From RecordType WHERE Name = :invoiceStatuses.Record_Type_Invoice__c/*'Invoice'*/].Id
                        , Related_Invoice__c = invoiceOriginal.Id
                        , Solution__c = invoiceOriginal.Solution__c
                        , Unpaid_Balance__c = delta * invoiceLineItem.Quantity__c
                        , Transaction_Type__c = transactionTypeMappingsMap.get(invoiceStatuses.Record_Type_Invoice__c/*'Invoice'*/).get(invoiceOriginal.Opportunity__r.RecordType.Name)
                        , Reason_Code__c = selectedReasonCode
                        );  
                
                insert invoiceNew;
                
                Invoice_Line_Item__c invoiceLineItemNew = new Invoice_Line_Item__c
                        ( Line_Number__c = invoiceLineItem.Line_Number__c
                        , Description__c = invoiceLineItem.Description__c
                        , RecordTypeId = Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByName().get('Invoice Line Item').getRecordTypeId()
                        , Quantity__c = invoiceLineItem.Quantity__c//invoiceLineItem.Order__r.Quantity__c
                        , Unit_Price__c = delta
                        , Memo_Line__c = invoiceLineItem.Memo_Line__c
                        , Memo_Line_Id__c = invoiceLineItem.Memo_Line_Id__c
                        , Inc_GST__c = invoiceLineItem.Inc_GST__c
                        , Tax_Code__c = invoiceLineItem.Tax_Code__c
                        , Invoice__c = invoiceNew.Id
                        , Reason_Code__c = selectedReasonCode
                        , Order__c = invoiceLineItem.Order__r.Id
                        , ILI_Type__c = invoiceStatuses.ILI_Type_Price_Change__c//'Price Change'
                        , Related_Line_Item__c = invoiceLineItem.Id
                        );
                        
                insert invoiceLineItemNew;

                // Firing the approval process
                CS_ChangeInvoiceUtils.submitApproval(invoiceNew.Id);
                
                pageRef = new PageReference('/' + invoiceNew.Id);
            }
            
            //system.debug('FinalTotalUnitPrice: ' + finalTotalUnitPrice + '; SelectedReasonCode: ' + selectedReasonCode);
            system.debug('SelectedReasonCode: ' + selectedReasonCode);
        }catch(DmlException ex)
        {
            ApexPages.addMessage(new Apexpages.message(Apexpages.Severity.Error,ex.getMessage()));
            pageRef = null;
            Database.rollback(sp);
        }    
        
        return pageRef;
            
    }
    
}