@isTest
private class CS_ChangeAmountInvoiceCtrExt_TEST {

	@testsetup static void setupCommonData() {
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		
		User uEGM = new User 
			( Alias = 'mstandt'
			, Email = 'executiveGeneralManager@cloudsensesolutions.com.au.cpqdev'
			, EmailEncodingKey = 'ISO-8859-1'
			, LastName = 'Testing'
			, LanguageLocaleKey = 'en_US'
			, LocaleSidKey = 'en_AU'
			, ProfileId = p.Id
			, TimeZoneSidKey = 'Australia/Sydney'
			, UserName = 'executiveGeneralManager@nbnco.com.au.cpqdev'
			);

		insert uEGM;
		
		User uGM = new User 
			( Alias = 'mstandt'
			, Email = 'generalManager@cloudsensesolutions.com.au.cpqdev'
			, EmailEncodingKey = 'ISO-8859-1'
			, LastName = 'Testing'
			, LanguageLocaleKey = 'en_US'
			, LocaleSidKey = 'en_AU'
			, ProfileId = p.Id
			, TimeZoneSidKey = 'Australia/Sydney'
			, UserName = 'generalManager@nbnco.com.au.cpqdev'
			, managerid = uEGM.id
			);

		insert uGM;
		
		User umgr = new User 
			( Alias = 'mstandt'
			, Email = 'manafer@cloudsensesolutions.com.au.cpqdev'
			, EmailEncodingKey = 'ISO-8859-1'
			, LastName = 'Testing'
			, LanguageLocaleKey = 'en_US'
			, LocaleSidKey = 'en_AU'
			, ProfileId = p.Id
			, TimeZoneSidKey = 'Australia/Sydney'
			, UserName = 'manafer@nbnco.com.au.cpqdev'
			, managerid = uGM.id
			);

		insert umgr;

      	User u = new User
      		( Alias = 'standt'
      		, Email = 'sysAdminuser@cloudsensesolutions.com.au.cpqdev'
      		, EmailEncodingKey = 'ISO-8859-1'
      		, LastName = 'Testing'
      		, LanguageLocaleKey = 'en_US'
      		, LocaleSidKey = 'en_AU'
      		, ProfileId = p.Id
      		, TimeZoneSidKey = 'Australia/Sydney'
      		, UserName = 'sitUser@cloudsensesolutions.com'
      		, managerid = umgr.id
      		);
      	
      	insert u;
      	
      	ObjectCommonSettings__c invSet = 
        	new ObjectCommonSettings__c(Name = 'Invoice Statuses', New__c = 'New', Requested__c = 'Requested', Cancelled__c = 'Cancelled', Pending_Approval__c = 'Pending Approval', 
        		Cancellation_Requested__c = 'Cancellation Requested', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval', Invoice_Name__c = 'To be issued', 
				 ILI_Type_Qty_Change__c = 'Qty Change', Record_Type_Credit_Memo__c = 'Credit Memo', Record_Type_Invoice__c = 'Invoice', Discount_Type_Amount__c = 'Amount',
				 Rejected__c = 'Rejected', Refer_to_Credit__c = 'Refer to Credit');
		
		insert invSet;
		
		ObjectCommonSettings__c solSet = 
        	new ObjectCommonSettings__c(Name = 'Solution Statuses', New__c = 'New', Requested__c = 'Requested', Cancelled__c = 'Cancelled', Pending_Approval__c = 'Pending Approval', 
        		Cancellation_Requested__c = 'Cancellation Requested', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval', 
        			Rejected__c = 'Rejected', Delivery__c = 'Delivery', Complete__c = 'Complete');
		
		insert solSet;
	}
	
	static User getUser() {
		return [SELECT id, name from User where UserName = 'sitUser@cloudsensesolutions.com' LIMIT 1];
	}
	
	static User getUser2() {
		return [SELECT id, name from User where UserName = 'executiveGeneralManager@nbnco.com.au.cpqdev' LIMIT 1];
	}

	static Invoice_Line_Item__c prepareData() {
		Opportunity testOpty = new Opportunity
			( Name = 'Test Opp'
			, CloseDate = System.today()
			, StageName = 'Invoicing'
			, Approving_Status__c = 'Internal Approval Received'
			, Invoicing_Status__c = 'Pending'
			);
        
		insert testOpty;
         
        system.assert(testOpty.Id != null);
		
		
		csord__Solution__c testSolution = new csord__Solution__c
			( Name = 'Test Opp'
            , Opportunity__c = testOpty.Id
            , csord__Status__c = 'New'
            , Status__c = 'New'
            , csord__Identification__c = 'a2QN0000000Kq5KMAS'
            );
            
        insert testSolution;
        
        csord__Order__c testOrder = new csord__Order__c
			( Name = 'PCD Relocation'
			, csord__Solution__c = testSolution.Id
			, csordtelcoa__Opportunity__c = testOpty.Id
			, csord__Status2__c = 'Order Submitted'
			, csord__Order_Type__c = 'Product Delivery'
			, csord__Product_Type__c = 'Recoverable Works'
			, csord__Identification__c = 'Order_a1hN0000000n31JIAQ_0'
			);
		
		insert testOrder;
		
		Invoice__c testInvoice = new Invoice__c
				( Status__c = 'Requested'
				, Name = 'To be issued'
				, Opportunity__c = testOpty.Id
				, RecordTypeId = [Select Id, Name From RecordType WHERE Name ='Invoice'].Id
				, Solution__c = testSolution.Id
				, Unpaid_Balance__c = decimal.valueOf('4025.00')
				, Payment_Status__c = 'UnPaid'
				);	
		
		insert testInvoice;
		
		Invoice_Line_Item__c testInvoiceLineItem = new Invoice_Line_Item__c
				( Quantity__c = decimal.valueOf('1')
                , Unit_Price__c = decimal.valueOf('1150.00')
                , Memo_Line__c = 'Recoverable Works - CPE'
                , Inc_GST__c = true
                , Tax_Code__c = 'GST 10% (NON-CAPITAL)'
                , Invoice__c = testInvoice.Id
                , Order__c = testOrder.Id
                , ILI_Type__c = 'Initial Basket'
				);
				
		insert testInvoiceLineItem;
		
		csord__Order_Line_Item__c testOLI = new csord__Order_Line_Item__c
			( Name = 'PCD Relocation'
			, csord__Order__c = testOrder.Id
			, csord__Line_Description__c = 'PCD Relocation'
			, Solution__c = testSolution.Id
			, OLI_Type__c = 'Initial Basket'
			, Memo_Line__c = 'Recoverable Works - CPE'
			, Invoice__c = testInvoice.Id
			, Invoice_Line_Item__c = testInvoiceLineItem.Id
			, GST__c = 'GST (Inclusive)'
			, Cost__c = decimal.valueOf('1000.00')
			, Margin__c = decimal.valueOf('15.00')
			, Quantity__c = decimal.valueOf('1')
			, Unit_Price__c = decimal.valueOf('1150.00')
			, csord__Identification__c = 'a1FN0000000vnwwMAA_0'
			);	
		
		insert testOLI;
		
		return testInvoiceLineItem;
	}
	
	static List<Invoice_Line_Item__c> prepareData_multipleProducts() {
		Opportunity testOpty = new Opportunity
			( Name = 'Test Opp'
			, CloseDate = System.today()
			, StageName = 'Invoicing'
			, Approving_Status__c = 'Internal Approval Received'
			, Invoicing_Status__c = 'Pending'
			);
        
		insert testOpty;
         
        system.assert(testOpty.Id != null);
		
		
		csord__Solution__c testSolution = new csord__Solution__c
			( Name = 'Test Opp'
            , Opportunity__c = testOpty.Id
            , csord__Status__c = 'New'
            , Status__c = 'New'
            , csord__Identification__c = 'a2QN0000000Kq5KMAS'
            );
            
        insert testSolution;
        
        csord__Order__c testOrder = new csord__Order__c
			( Name = 'PCD Relocation'
			, csord__Solution__c = testSolution.Id
			, csordtelcoa__Opportunity__c = testOpty.Id
			, csord__Status2__c = 'Order Submitted'
			, csord__Order_Type__c = 'Product Delivery'
			, csord__Product_Type__c = 'Recoverable Works'
			, csord__Identification__c = 'Order_a1hN0000000n31JIAQ_0'
			);
		
		insert testOrder;
		
		csord__Order__c testOrder2 = new csord__Order__c
			( Name = 'NS Installation'
			, csord__Solution__c = testSolution.Id
			, csordtelcoa__Opportunity__c = testOpty.Id
			, csord__Status2__c = 'Order Submitted'
			, csord__Order_Type__c = 'Product Delivery'
			, csord__Product_Type__c = 'Recoverable Works'
			, csord__Identification__c = 'Order_a1hN0000000n31JIAR_0'
			);
		
		insert testOrder2;
		
		Invoice__c testInvoice = new Invoice__c
				( Status__c = 'Requested'
				, Name = 'To be issued'
				, Opportunity__c = testOpty.Id
				, RecordTypeId = [Select Id, Name From RecordType WHERE Name ='Invoice'].Id
				, Solution__c = testSolution.Id
				, Unpaid_Balance__c = decimal.valueOf('4025.00')
				, Payment_Status__c = 'UnPaid'
				);	
		
		insert testInvoice;
		
		Invoice_Line_Item__c testInvoiceLineItem = new Invoice_Line_Item__c
				( Quantity__c = decimal.valueOf('1')
                , Unit_Price__c = decimal.valueOf('1150.00')
                , Memo_Line__c = 'Recoverable Works - CPE'
                , Inc_GST__c = true
                , Tax_Code__c = 'GST 10% (NON-CAPITAL)'
                , Invoice__c = testInvoice.Id
                , Order__c = testOrder.Id
                , ILI_Type__c = 'Initial Basket'
				);
				
		insert testInvoiceLineItem;
		
		Invoice_Line_Item__c testInvoiceLineItem2 = new Invoice_Line_Item__c
				( Quantity__c = decimal.valueOf('1')
                , Unit_Price__c = decimal.valueOf('3967.50')
                , Memo_Line__c = 'Recoverable Works - Small'
                , Inc_GST__c = true
                , Tax_Code__c = 'GST 10% (NON-CAPITAL)'
                , Invoice__c = testInvoice.Id
                , Order__c = testOrder2.Id
                , ILI_Type__c = 'Initial Basket'
				);
				
		insert testInvoiceLineItem2;
		
		csord__Order_Line_Item__c testOLI = new csord__Order_Line_Item__c
			( Name = 'PCD Relocation'
			, csord__Order__c = testOrder.Id
			, csord__Line_Description__c = 'PCD Relocation'
			, Solution__c = testSolution.Id
			, OLI_Type__c = 'Initial Basket'
			, Memo_Line__c = 'Recoverable Works - CPE'
			, Invoice__c = testInvoice.Id
			, Invoice_Line_Item__c = testInvoiceLineItem.Id
			, GST__c = 'GST (Inclusive)'
			, Cost__c = decimal.valueOf('1000.00')
			, Margin__c = decimal.valueOf('15.00')
			, Quantity__c = decimal.valueOf('1')
			, Unit_Price__c = decimal.valueOf('1150.00')
			, csord__Identification__c = 'a1FN0000000vnwwMAA_0'
			);	
		
		insert testOLI;
		
		csord__Order_Line_Item__c testOLI2 = new csord__Order_Line_Item__c
			( Name = 'NS Installation'
			, csord__Order__c = testOrder2.Id
			, csord__Line_Description__c = 'NS Installation'
			, Solution__c = testSolution.Id
			, OLI_Type__c = 'Initial Basket'
			, Memo_Line__c = 'Recoverable Works - CPE'
			, Invoice__c = testInvoice.Id
			, Invoice_Line_Item__c = testInvoiceLineItem2.Id // changed !!!
			, GST__c = 'GST (Inclusive)'
			, Cost__c = decimal.valueOf('3450.00')
			, Quantity__c = decimal.valueOf('1')
			, Unit_Price__c = decimal.valueOf('3967.50')
			, csord__Identification__c = 'a1FN0000000vnwwMAB_0'
			);	
		
		insert testOLI2;
		
		
		Invoice__c testInvoiceCreated = new Invoice__c 
			( Status__c = 'Requested'
			, Name = 'To be issued'
			, Opportunity__c = testOpty.Id
			, RecordTypeId = [Select Id, Name From RecordType WHERE Name ='Invoice'].Id
			, Related_Invoice__c = testInvoice.Id
			, Solution__c = testSolution.Id
			, Payment_Status__c = 'UnPaid'
			);
		
		insert testInvoiceCreated;
		
		Invoice_Line_Item__c testIliCreated = new Invoice_Line_Item__c
			( Quantity__c = decimal.valueOf('3')
            , Unit_Price__c = decimal.valueOf('1150.00')
            , Memo_Line__c = 'Recoverable Works - CPE'
            , Inc_GST__c = true
            , Tax_Code__c = 'GST 10% (NON-CAPITAL)'
            , Invoice__c = testInvoiceCreated.Id
            , Reason_Code__c = 'Accounts Receivables Billing Error'
            , Order__c = testOrder.Id
            , ILI_Type__c = 'Qty Change'
            , Related_Line_Item__c = testInvoiceLineItem.Id
			);
				
		insert testIliCreated;
		
		csord__Order_Line_Item__c testOLI3 = new csord__Order_Line_Item__c
			( Name = 'PCD Relocation'
			, csord__Order__c = testOrder.Id
			, csord__Line_Description__c = 'PCD Relocation'
			, Solution__c = testSolution.Id
			, OLI_Type__c = 'Qty Change'
			, Memo_Line__c = 'Recoverable Works - CPE'
			, Invoice__c = testInvoiceCreated.Id
			, Invoice_Line_Item__c = testIliCreated.Id
			, GST__c = 'GST (Inclusive)'
			, Cost__c = decimal.valueOf('1150.00')
			, Quantity__c = decimal.valueOf('3')
			, Unit_Price__c = decimal.valueOf('1150.00') // changed from 3967.50 to 1150.00
			, csord__Identification__c = 'a1FN0000000vnwwMAC_0'
			);	
		
		insert testOLI3;	
		
		List<Invoice_Line_Item__c> testIliList = new List<Invoice_Line_Item__c>();
		testIliList.add(testInvoiceLineItem);
		testIliList.add(testInvoiceLineItem2);
		testIliList.add(testIliCreated);
		
		return testIliList;
	}

	static testMethod void changeInvoiceAmount_methodOne() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeAmountInvoiceCtrExt_TEST.prepareData();

      	System.runAs(getUser()) { 
		
			//*** Change Unit Price to 3450
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
			//CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt(sc);
			
			System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt();
			
			controller.getReasonCodes();
			controller.unitPriceValue = '3450';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit unit price change request for approval
			test.startTest();
				controller.NewUnitPrice();
			test.stopTest();
			
			
			// Instantiate the new ProcessWorkitemRequest object and populate it
	        Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
	        request.setAction('Approve');
	        request.setWorkitemId([SELECT Id FROM ProcessInstanceWorkitem].Id);
	        request.setComments('Approved');
	        
	        // Submit the request for approval
	        Approval.ProcessResult process_result = Approval.process(request);
			
			// Validating results
			Invoice_Line_Item__c iliTest = [SELECT id, name, Order__r.Id, Invoice__r.Id from Invoice_Line_Item__c where Id = :testInvoiceLineItem.Id];
			
			csord__Order__c parentOrderTest = [SELECT id, name, Unit_Price__c from csord__Order__c where Id = :iliTest.Order__r.Id];
			
			Invoice__c invoiceGeneratedTest = [SELECT id, name, Status__c, Record_Type_Name__c from Invoice__c where Related_Invoice__c = :iliTest.Invoice__r.Id];
			
			system.assertEquals(3450.00, parentOrderTest.Unit_Price__c);
			system.assertEquals('Requested', invoiceGeneratedTest.Status__c);
			system.assertEquals('Invoice', invoiceGeneratedTest.Record_Type_Name__c);
			
			
      	}
		
	}
	
	static testMethod void changeInvoiceAmount_methodTwo() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeAmountInvoiceCtrExt_TEST.prepareData();

      	System.runAs(getUser()) { 
		
			//*** Change Unit Price to 500
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
			//CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt(sc);
			
			System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt();

			controller.unitPriceValue = '500';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit unit price change request for approval
			test.startTest();
				controller.NewUnitPrice();
			test.stopTest();
			
			
			// Instantiate the new ProcessWorkitemRequest object and populate it
	        Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
	        request.setAction('Approve');
	        request.setWorkitemId([SELECT Id FROM ProcessInstanceWorkitem].Id);
	        request.setComments('Approved');
	        
	        // Submit the request for approval
	        Approval.ProcessResult process_result = Approval.process(request);
			
			// Validating results
			Invoice_Line_Item__c iliTest = [SELECT id, name, Order__r.Id, Invoice__r.Id from Invoice_Line_Item__c where Id = :testInvoiceLineItem.Id];
			
			csord__Order__c parentOrderTest = [SELECT id, name, Unit_Price__c from csord__Order__c where Id = :iliTest.Order__r.Id];
			
			Invoice__c creditMemoGeneratedTest = [SELECT id, name, Status__c, Record_Type_Name__c from Invoice__c where Related_Invoice__c = :iliTest.Invoice__r.Id];
			
			Invoice_Line_Item__c cmliGeneratedTest = [SELECT id, name, Unit_Price__c, Reason_Code__c from Invoice_Line_Item__c where Related_Line_Item__c = :iliTest.Id];
			
			system.assertEquals(500.00, parentOrderTest.Unit_Price__c);
			system.assertEquals('Requested', creditMemoGeneratedTest.Status__c);
			system.assertEquals('Credit Memo', creditMemoGeneratedTest.Record_Type_Name__c);
			system.assertEquals(-650.00, cmliGeneratedTest.Unit_Price__c);
			system.assertEquals('Duplicate Billing', cmliGeneratedTest.Reason_Code__c);
			
			
      	}
	}
	
	static testMethod void changeInvoiceAmount_methodThree() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeAmountInvoiceCtrExt_TEST.prepareData();

      	System.runAs(getUser()) { 
		
			//*** Change Unit Price to invalid number
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
			//CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt(sc);
			
			System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt();

			controller.unitPriceValue = '345B';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit unit price change request for approval
			test.startTest();
				controller.NewUnitPrice();
			test.stopTest();
			
			// Validating results
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			
			system.assertEquals('New Unit Price value entered should be > 0 and should not have more than 2 decimal places', msgs.get(0).getDetail());

      	}
	}
	
	static testMethod void changeInvoiceAmount_methodFour() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeAmountInvoiceCtrExt_TEST.prepareData();

      	System.runAs(getUser()) { 
		
			//*** Change Unit Price to invalid number
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
			//CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt(sc);
			
			System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt();

			controller.unitPriceValue = '';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit unit price change request for approval
			test.startTest();
				controller.NewUnitPrice();
			test.stopTest();	
			
			// Validating results
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			
			system.assertEquals('New Unit Price value cannot be null', msgs.get(0).getDetail());

      	}
	}
	
	static testMethod void changeInvoiceAmount_methodFive() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeAmountInvoiceCtrExt_TEST.prepareData();

      	System.runAs(getUser()) { 
		
			//*** Change Unit Price to invalid number
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
			//CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt(sc);
			
			System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt();

			controller.unitPriceValue = '-1234';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit unit price change request for approval
			test.startTest();
				controller.NewUnitPrice();
			test.stopTest();
			
			// Validating results
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			
			system.assertEquals('New Unit Price value entered should be > 0 and should not have more than 2 decimal places', msgs.get(0).getDetail());

      	}
	}
	
	static testMethod void changeInvoiceAmount_methodSix() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeAmountInvoiceCtrExt_TEST.prepareData();
		
		
		//*** Cancel Change Unit Price
        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
		//CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt(sc);
		
		System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
    	CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt();

		PageReference backToInvoiceLineItemURL = controller.Cancel();
		
		system.assertEquals('/' + testInvoiceLineItem.Id, backToInvoiceLineItemURL.getUrl());
	}
	
	static testMethod void changeInvoiceAmount_methodSeven() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeAmountInvoiceCtrExt_TEST.prepareData();
		
		
		//*** Change Unit Price - Calculate delta
        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
		//CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt(sc);
		
		System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
		CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt();

		controller.unitPriceValue = '2000';
		string unitPriceNew = controller.getUnitPriceValue();
		controller.CalculateDelta();
		string delta = controller.getShowDelta();
		
		system.assertEquals('850.00', delta);
		system.assertEquals('2000', unitPriceNew);
		
		//*** Change Unit Price - Calculate delta (New unit price value is an invalid number)
		//System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);

		//controller = new CS_ChangeAmountInvoiceCtrExt(sc);
		controller = new CS_ChangeAmountInvoiceCtrExt();
		controller.unitPriceValue = 'A';
		controller.CalculateDelta();
		
		List<Apexpages.Message> msgs = ApexPages.getMessages();
			
		system.assertEquals('New Unit Price value entered should be > 0 and should not have more than 2 decimal places', msgs.get(0).getDetail());
	}
	
	/*static testMethod void changeInvoiceAmount_methodEight() {
		
		List<Invoice_Line_Item__c> testInvoiceLineItemList = CS_ChangeAmountInvoiceCtrExt_TEST.prepareData_multipleProducts();
		
		Invoice_Line_Item__c testInvoiceLineItem = testInvoiceLineItemList.get(0);
		
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];

      	System.runAs(getUser()) { 
		
			//*** Change Quantity of first product to 0
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
			//CS_ChangeQuantityInvoiceCtrExt controller = new CS_ChangeQuantityInvoiceCtrExt(sc);
			
			System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeQuantityInvoiceCtrExt controller = new CS_ChangeQuantityInvoiceCtrExt();

			controller.quantityValue = '0';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit quantity change request for approval
			controller.NewQuantity();
			
			
			// Instantiate the new ProcessWorkitemRequest object and populate it
	        Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
	        request.setAction('Approve');
	        request.setWorkitemId([SELECT Id FROM ProcessInstanceWorkitem].Id);
	        request.setComments('Approved');
	        
	        // Submit the request for approval
	        Approval.ProcessResult process_result = Approval.process(request);
	        
	        // Verify the result
        	system.assert(process_result.isSuccess());	
			
			//*** Change Unit Price of same product to 0
			// Approve the submitted request
			//CS_ChangeAmountInvoiceCtrExt controller2 = new CS_ChangeAmountInvoiceCtrExt(sc);
			
			//System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeAmountInvoiceCtrExt controller2 = new CS_ChangeAmountInvoiceCtrExt();

			controller2.unitPriceValue = '0';
			controller2.selectedReasonCode = 'Duplicate Billing';
			// submit unit price change request for approval
			controller2.NewUnitPrice();
			
			// Validating results
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			
			system.assertEquals('Action not allowed because no quantity left', msgs.get(0).getDetail());
      	}
		
	}*/
	
	static testMethod void changeInvoiceQuantity_methodNine() {
		
		List<Invoice_Line_Item__c> testInvoiceLineItemList = CS_ChangeAmountInvoiceCtrExt_TEST.prepareData_multipleProducts();
		
		Invoice_Line_Item__c testInvoiceLineItem = testInvoiceLineItemList.get(2);

      	System.runAs(getUser()) { 
		
			//*** Change Unit Price of one product to 0
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
			//CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt(sc);
			
			System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt();

			controller.unitPriceValue = '0';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit unit price change request for approval
			test.startTest();
				controller.NewUnitPrice();
			test.stopTest();
			
			
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			
			system.assertEquals('Action not allowed. New Unit Price entered must be bigger than zero. Change Quantity instead', msgs.get(0).getDetail());
      	}
		
	}
	
	static testMethod void changeInvoiceQuantity_methodTen() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeAmountInvoiceCtrExt_TEST.prepareData();

      	System.runAs(getUser2()) { 
		
			//*** Change Unit Price to 500
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
			//CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt(sc);
			
			System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeAmountInvoiceCtrExt controller = new CS_ChangeAmountInvoiceCtrExt();

			controller.unitPriceValue = '500';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit unit price change request for approval
			test.startTest();
				controller.NewUnitPrice();
			test.stopTest();
			
			Boolean hasMessages = ApexPages.hasMessages(Apexpages.Severity.Error);
			
			system.assertEquals(true, hasMessages);
			
      	}
		
	}
	
	static testMethod void changeInvoiceAmount_methodEleven() {
		
		List<Invoice_Line_Item__c> testInvoiceLineItemList = CS_ChangeAmountInvoiceCtrExt_TEST.prepareData_multipleProducts();
		
		Invoice_Line_Item__c testInvoiceLineItem = testInvoiceLineItemList.get(0);
		
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];

      	System.runAs(getUser()) { 
			
			System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeAmountInvoiceCtrExt controller2 = new CS_ChangeAmountInvoiceCtrExt();

			controller2.unitPriceValue = '1150.00';
			controller2.selectedReasonCode = 'Duplicate Billing';
			// submit unit price change request for approval
			controller2.NewUnitPrice();
			
			// Validating results
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			
			system.assertEquals('The change you are requesting is equal to the original value.', msgs.get(0).getDetail());
      	}
		
	}
	
}