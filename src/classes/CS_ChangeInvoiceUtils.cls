/*************************************************
 Developed by: Hugo Pinto
 Description: Helper class for chagne order/change invoice
 Version History:
 - v1.0, 2017-02-27, HP: Created
 - v1.1, 2017-02-28, LT: Added createOLI method - called in CS_InvoiceTrigger
 - v1.2, 2017-03-02, LT: Added setOrderStatus method - called in CS_SolutionTrigger
 - v1.3, 2017-03-03, LT: Added canccelInvoices method - called in CS_SolutionTrigger
 - v1.3, 2017-03-07, LT: Added validatePayments method - called in CS_SolutionTrigger
*/

public class CS_ChangeInvoiceUtils {

    //Returns picklist values and api names for reason code
    public static List<SelectOption> getReasonCodes(String strObject) {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult;

        if(strObject == 'csord__Solution__c')
        {
            fieldResult = csord__Solution__c.Reason_Code__c.getDescribe();
        }else if(strObject == 'Invoice_Line_Item__c')
        {
            fieldResult = Invoice_Line_Item__c.Reason_Code__c.getDescribe();
        }
        else
        {
            fieldResult = Invoice__c.Reason_Code__c.getDescribe();
        }
        List<Schema.PicklistEntry> rCode = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : rCode) {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
            
        }       
        return options;
    }

    // check that all uncancelled invoices are Paid, otherwise display error message
    public static void validatePayments(map<Id, csord__Solution__c> solMap) {

        //map<Id, list<Invoice__c>> solToInvoicesMap = new map<Id, list<Invoice__c>>();
        ObjectCommonSettings__c invoiceStatuses = ObjectCommonSettings__c.getValues('Invoice Statuses');
        
        set<Id> unpaidSet = new set<Id>();
        list<Invoice__c> unpaidInvoices = new list<Invoice__c>([Select Id, Solution__c, Status__c, Payment_Status__c from Invoice__c 
                                                            where Solution__c = :solMap.keySet()
                                                            and RecordType.Name = 'Invoice'
                                                            and Status__c != :invoiceStatuses.Cancelled__c//'Cancelled'
                                                            and Payment_Status__c != :invoiceStatuses.Payment_Status_Paid__c/*'Paid'*/]);

        for (Invoice__c inv: unpaidInvoices) {
            unpaidSet.add(inv.Solution__c);
        }

        for (csord__Solution__c sol: solMap.values()) {
            if (unpaidSet.contains(Sol.Id)) {
                sol.addError('This customer order ' + sol.Customer_Order__c + ' cannot be changed to ' + sol.Status__c + ' because there are unpaid invoices.');
            }
        }
        
    }
    
    // cancel all invoices under this customer order
    public static void canccelInvoices(map<Id, csord__Solution__c> solMap) {
        
        ObjectCommonSettings__c invoiceStatuses = ObjectCommonSettings__c.getValues('Invoice Statuses');
        
        // get all invoices for all solutions
        list<Invoice__c> invList = new list<Invoice__c>([Select Id, Status__c, Reason_Code__c, Solution__c from Invoice__c where Solution__c in :solMap.keySet()
        												and Status__c != :invoiceStatuses.Rejected__c and Status__c != :invoiceStatuses.Refer_to_Credit__c
                                    and Status__c != :invoiceStatuses.Cancelled__c and RecordType.name != :invoiceStatuses.Record_Type_Credit_Memo__c]);
        for (Invoice__c inv: invList) {
            inv.Status__c = invoiceStatuses.Cancellation_Requested__c;//'Cancellation Requested';
            inv.Reason_Code__c = solMap.get(inv.Solution__c).Reason_Code__c;
        }
        update invList;
    }
    
    // Helper class to update status to previous status cancellation order is not approved.
    // The approval process will update the status to Rejected and this method will be called in the csord__Solution__c before trigger
    public static void setOrderStatus(map<Id, csord__Solution__c> newSolMap, map<Id, csord__Solution__c> oldSolMap) {
        
        for (csord__Solution__c sol: newSolMap.values()) {
            sol.Status__c = sol.Previous_Status__c;
        }
    }
    
    public static void submitApproval(Id id) {
        
        // Creates a new instance of Approval Process
        Approval.Processsubmitrequest procReq;

        // Creates a ProcessSubmitRequest object to submit the record for approval
        procReq = new Approval.ProcesssubmitRequest();

        procReq.setComments('Submitting new Invoice/Credit Memo for Approval');

        // Set the record id being submitted to approval
        procReq.setObjectId(id);
        
        //list<string> idList = new list<string>();
        //idList.add(UserInfo.getUserId());
        //User objUser =[Select ManagerId from User where Id=:UserInfo.getUserId()];
        //idList.add('00528000006B4Ot');
        //procReq.setNextApproverIds(idList);
        //procReq.setSubmitterId('00528000006B4Oe');
        //procReq.setSubmitterId(UserInfo.getUserId());
    
        // Submit the record for approval
        Approval.Processresult result = Approval.process(procReq);
    }
    
    // helper class to create order line item when invoice adjustments (cancel order, credit memo and debit memo) have been approved.
    public static void createOLI(map<Id, Invoice__c> newInvMap, map<Id, Invoice__c> oldInvMap) {
        
        set<Id> invIds = new set<Id>();
        set<Id> cancelledinvIds = new set<Id>();
        ObjectCommonSettings__c invoiceStatuses = ObjectCommonSettings__c.getValues('Invoice Statuses');
        
        // get only invoice that are change order or cancellation
        for (Invoice__c inv: newInvMap.values()) {
            if (inv.Status__c == invoiceStatuses.Requested__c/*'Requested'*/ && oldInvMap.get(inv.Id).Status__c != invoiceStatuses.Requested__c/*'Requested'*/) {
                invIds.add(inv.Id);
            }
            if (inv.Status__c == invoiceStatuses.Cancellation_Requested__c/*'Cancellation Requested'*/ && oldInvMap.get(inv.Id).Status__c != invoiceStatuses.Cancellation_Requested__c/*'Cancellation Requested'*/) {
                invIds.add(inv.Id);
                cancelledinvIds.add(inv.Id);
            }

        }
        
        
        // get all invoice line items for invoices
        list<Invoice_Line_Item__c> iliList = new list<Invoice_Line_Item__c>([Select Id, Invoice__c, Order__c, Order__r.Name, Order__r.csord__Identification__c, Order_Line_Item__c,
                                                Description__c, Quantity__c, Unit_Price__c, Amount__c, Memo_Line__c, Memo_Line_Id__c, Inc_GST__c, ILI_Type__c, Order__r.Quantity__c, Order__r.Unit_Price__c
                                                from Invoice_Line_Item__c where Invoice__c in: invIds and Invoice__r.Sent_to_AR_date__c = null]);
        
        list<csord__Order_Line_Item__c> cancelledOliList = new list<csord__Order_Line_Item__c>([Select Id, OLI_Type__c from csord__Order_Line_Item__c
                                                    where Invoice__c in: cancelledinvIds and Invoice__r.Sent_to_AR_date__c != null]);
        for(csord__Order_Line_Item__c oli:cancelledOliList)
        {
            oli.OLI_Type__c = invoiceStatuses.ILI_Type_Cancel_Inv__c;
        }
        update cancelledOliList;

        list<csord__Order_Line_Item__c> newOLIList = new list<csord__Order_Line_Item__c>();
        system.debug('DiscountType: ' + invoiceStatuses.Discount_Type_Amount__c);
        // for changed items
        for (Invoice_Line_Item__c ili: iliList) {
            csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c (
                                                OLI_Type__c = newInvMap.get(ili.Invoice__c).Status__c == invoiceStatuses.Requested__c/*'Requested'*/ ? ili.ILI_Type__c : invoiceStatuses.ILI_Type_Cancel_Inv__c/*'Cancel Inv'*/
                                                , csord__Order__c = ili.Order__c
                                                , csord__Line_Description__c = ili.Description__c
                                                , Memo_Line__c = ili.Memo_Line__c
                                                , Memo_Line_Id__c = ili.Memo_Line_Id__c
                                                , Quantity__c = newInvMap.get(ili.Invoice__c).Status__c == invoiceStatuses.Requested__c/*'Requested'*/ ? ili.Quantity__c : 0 - ili.Quantity__c
                                                , Unit_Price__c = ili.Unit_Price__c
                                                , GST__c = (ili.Inc_GST__c) ? invoiceStatuses.GST_Inclusive__c/*'GST (Inclusive)'*/ : invoiceStatuses.GST_Exclusive__c/*'GST (Exclusive)'*/
                                                , csord__Total_Price__c = newInvMap.get(ili.Invoice__c).Status__c == invoiceStatuses.Requested__c/*'Requested'*/ ? ili.Amount__c : 0 - ili.Amount__c
                                                , csord__Identification__c = ili.Order__r.csord__Identification__c
                                                , csord__Discount_Type__c = invoiceStatuses.Discount_Type_Amount__c/*'Amount'*/
                                                , csord__Discount_Value__c = 0
                                                , Name = ili.Order__r.Name
                                                , Solution__c = newInvMap.get(ili.Invoice__c).Solution__c
                                                , Invoice__c = ili.Invoice__c
                                                , Invoice_Line_Item__c = newInvMap.get(ili.Invoice__c).Status__c == invoiceStatuses.Requested__c/*'Requested'*/ ? ili.Id : null
                                                );
            newOLIList.add(oli);
        }
        insert newOLIList;
        
        // populate the order line item lookup in invoice line item for items that chagne order
        list<csord__Order_Line_Item__c> oliLIst = new list<csord__Order_Line_Item__c>([select Id, Invoice_Line_Item__c from csord__Order_Line_Item__c where Id = :newOLILIst]);
        map<Id, Id> iliToOliMap = new map<Id, Id>();
        for (csord__Order_Line_Item__c oli : oliList) {
            iliToOliMap.put(oli.Invoice_Line_Item__c, oli.Id);
        }
        
        for (Invoice_Line_Item__c ili: iliList) {
            if (newInvMap.get(ili.Invoice__c).Status__c != invoiceStatuses.Cancellation_Requested__c/*'Cancellation Requested'*/) {
                ili.Order_Line_Item__c = iliToOliMap.get(ili.Id);
            }
        }
        update iliList;
    }
        
}