@isTest
private class CS_ChangeInvoiceUtils_TEST {
	@testSetup
    static void createTestData() {
        Opportunity testOpty = new Opportunity (Name='Test Opp', CloseDate=System.today(), StageName='Invoicing', Approving_Status__c = 'Internal Approval Received'
      		, Invoicing_Status__c = 'Pending');
            insert testOpty;

		
		ObjectCommonSettings__c invSet = 
        	new ObjectCommonSettings__c(Name = 'Invoice Statuses', New__c = 'New', Requested__c = 'Requested', Cancelled__c = 'Cancelled', Pending_Approval__c = 'Pending Approval', 
        		Cancellation_Requested__c = 'Cancellation Requested', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval', 
        		Payment_Status_Paid__c = 'Paid', GST_Exclusive__c = 'GST (Exclusive)', GST_Inclusive__c = 'GST (Inclusive)', 
        		Discount_Type_Amount__c = 'Amount', ILI_Type_Cancel_Inv__c = 'Cancel Inv',Rejected__c = 'Rejected', Refer_to_Credit__c = 'Refer to Credit');
		
		insert invSet;
		
		ObjectCommonSettings__c solSet = 
        	new ObjectCommonSettings__c(Name = 'Solution Statuses', New__c = 'New', Requested__c = 'Requested', Cancelled__c = 'Cancelled', Pending_Approval__c = 'Pending Approval', 
        		Cancellation_Requested__c = 'Cancellation Requested', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval', 
        			Rejected__c = 'Rejected', Delivery__c = 'Delivery', Complete__c = 'Complete', Payment_Status_Paid__c = 'Paid', ILI_Type_Cancel_Inv__c = 'Cancel Inv',
        			GST_Inclusive__c = 'GST (Inclusive)', GST_Exclusive__c = 'GST (Exclusive)', Discount_Type_Amount__c = 'Amount');
		
		insert solSet;
        RecordType InvoiceRecType  = [SELECT Id FROM RecordType WHERE SobjectType = 'Invoice__c' AND DeveloperName = 'Invoice'];
    	csord__Solution__c sol = new csord__Solution__c (
            Name = 'testSolution'
            , Previous_Status__c = 'New'            
            , Status__c = 'New'
            , csord__Identification__c = 'testIdentification'
            );
        insert sol;

		Invoice__c inv = new Invoice__c (
            Name = 'testInvoice'
            , Solution__c = sol.Id
            , Status__c = 'New'
            , Payment_Status__c = 'Unpaid'
            , RecordTypeId=InvoiceRecType.Id,  Opportunity__c = testOpty.Id
            );
		insert inv;
    }
	
	@isTest static void CS_ChangeInvoiceUtils_Test1() {

        Test.startTest();

        csord__Solution__c sol = [select Id, Previous_Status__c, Status__c from csord__Solution__c];
        Invoice__c inv = [select Id, Solution__c, Status__c, Payment_Status__c from Invoice__c];

        /*** test CS_ChangeInvoiceUtils.setOrderStatus() ***/
        sol.Status__c = 'Cancellation Pending Approval';
        update sol;

        sol.Status__c = 'Rejected';
        update sol;
        csord__Solution__c solResult = [select Id, Status__c from csord__Solution__c where Id = :sol.id];
		system.assertEquals('New', solResult.Status__c);


		/*** test CS_ChangeInvoiceUtils.validatePayments() ***/
		sol.Status__c = 'Delivery';

		// unpaid invoice
		try {
			update sol;
		} catch(Exception e) {
			Boolean expectedExceptionThrown =  e.getMessage().contains('cannot be changed to Delivery') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}

		// paid invoice
		inv.Status__c = 'Cancelled';
		update inv;
		update sol;
		solResult = [select Id, Status__c from csord__Solution__c where Id = :sol.id];
		system.assertEquals('Delivery', solResult.Status__c);
        
        Test.stopTest();
    }

	@isTest static void CS_ChangeInvoiceUtils_Test2() {
        
        Test.startTest();

        csord__Solution__c sol = [select Id, Previous_Status__c, Status__c from csord__Solution__c];
        Invoice__c inv = [select Id, Solution__c, Status__c, Payment_Status__c from Invoice__c];
        
		/*** testCS_ChangeInvoiceUtils.canccelInvoices ***/        
		sol.Status__c = 'Cancellation Pending Approval';
		sol.Reason_Code__c = 'testResonCode';                    
		update Sol;
                    
        sol.Status__c = 'Cancelled';
        update sol;                    

		Invoice__c invResult = [select Id, Status__c, Reason_Code__c from Invoice__c where Id = :inv.Id];			
        system.debug('sol.Status__c:' + sol.Status__c);
        system.debug('invResult.Status__c:' + invResult.Status__c);
		system.assertEquals('Cancellation Requested', invResult.Status__c);
		system.assertEquals(Sol.Reason_Code__c, invResult.Reason_Code__c);
		
        Test.stopTest();
	}

}