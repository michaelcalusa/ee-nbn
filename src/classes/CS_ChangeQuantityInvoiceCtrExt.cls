/*************************************************
- Developed by: Vishwanatha Goki
- Date Created:  (dd/MM/yyyy)
- Description: Controller for Change quantity functionlity from Invoice Line Item detail page.
- Version History:
- v1.0 - 04/02/2017, VG: Created
- v1.1 - 15/11/2017, Ravindranshanmugam1: Modifications in calculating the changed invoice quantity. 
- v1.2 - 02/12/2017, RavindranShanmugam1: Modification to display the total order quantity
*/

public class CS_ChangeQuantityInvoiceCtrExt {
    
    // v1.1 Changes
    // NewDev constants
    public static Decimal SDU_MEMO_LINE_ID = 1462.0;
    public static Decimal MDU_MEMO_LINE_ID = 1464.0;
    public static Decimal NEWDEV_ACCR_LINE_ID = 19840.0;
    public static List<Integer> memolines = new List<Integer>{1462, 1464, 19840, 18840};   
    public static Id INVOICE_RECORDTYPEID;
    public static Id CREDITMEMO_RECORDTYPEID;

    public Invoice_Line_Item__c ili{get;set;}
    public string quantityValue {get;set;}
    public String selectedReasonCode{get;set;}
    public Boolean cannotChangeQty {get;set;}
    public decimal delta {get;set;}
    public String invoiceStatus {get;set;}
    public String iliId {get; set;}
    public Map<String, String> invoicetransactionTypeMap = new Map<String, String>();
    public Map<String, String> creditMemoTypeMap = new Map<String, String>();
    public Map<String, Map<String, String>> transactionTypeMappingsMap = new Map<String, Map<String, String>>();

    //v1.1 changes
    public Map<String, String> invoicetransactionNewDevTypeMap = new Map<String, String>();
    public Map<String, String> creditMemoNewDevTypeMap = new Map<String, String>();
    public Map<String, Map<String, String>> transactionTypeNewDevMappingsMap = new Map<String, Map<String, String>>();

    public boolean isValidProduct{get;set;}
    public boolean isValidInvoice{get;set;}
    public boolean isValidStatus{get;set;}
    public Decimal orderQuantity {get;set;}
    public String invalidInvoice_errorMessage{get;set;}
    //Returns picklist values and api names for reason code
    public List<SelectOption> getReasonCodes() {       
        return CS_ChangeInvoiceUtils.getReasonCodes('Invoice_Line_Item__c');
    }

    public CS_ChangeQuantityInvoiceCtrExt() {
    
        for (Transaction_Type_Mapping__mdt ttm : 
            [SELECT id, Credit_Memo_Transaction_Type__c, Invoice_Transaction_Type__c, Opportunity_Record_Type__c, Account_Address_Record_Type__c from Transaction_Type_Mapping__mdt]) 
        {
            invoicetransactionTypeMap.put(ttm.Opportunity_Record_Type__c, ttm.Invoice_Transaction_Type__c);
            creditMemoTypeMap.put(ttm.Opportunity_Record_Type__c, ttm.Credit_Memo_Transaction_Type__c);
            //V1.1 changes
            invoicetransactionNewDevTypeMap.put(ttm.Account_Address_Record_Type__c, ttm.Invoice_Transaction_Type__c);
            creditMemoNewDevTypeMap.put(ttm.Account_Address_Record_Type__c, ttm.Credit_Memo_Transaction_Type__c);
        }
        transactionTypeMappingsMap.put('Invoice', invoicetransactionTypeMap);
        transactionTypeMappingsMap.put('Credit Memo', creditMemoTypeMap);
        //V1.1 changes
        transactionTypeNewDevMappingsMap.put('Invoice', invoicetransactionNewDevTypeMap);
        transactionTypeNewDevMappingsMap.put('Credit Memo', creditMemoNewDevTypeMap);

        List<RecordType> invRecTypeList = [SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Invoice__c'];
        for (RecordType recType :invRecTypeList) {
            if (recType.DeveloperName == 'Invoice') 
                INVOICE_RECORDTYPEID = recType.Id;
            if (recType.DeveloperName == 'Credit_Memo')
                CREDITMEMO_RECORDTYPEID = recType.Id;
        }
        
        //this.ili = (Invoice_Line_Item__c) controller.getRecord();
        iliId = ApexPages.currentPage().getParameters().get('Id');
        this.ili = [select Name, Memo_Line__c, Tax_Code__c, Memo_Line_Id__c, Inc_GST__c, Line_Number__c, Description__c, Amount__c, Quantity__c, Unit_Price__c,
                           Invoice__c, Invoice__r.Related_Invoice__c, Invoice__r.Status__c, Invoice__r.Related_Invoice__r.Id, Invoice__r.RecordTypeId,
                           Order__r.Unit_Price__c, Order__c, Order__r.Quantity__c,  Order__r.Id, Order__r.csord__Identification__c, Order__r.Name, 
                           Order_Line_Item__c, Order_Line_Item__r.csord__Discount_Type__c 
                    from Invoice_Line_Item__c where id =: iliId];


// invoiceLineItem =   [SELECT Id, name, Invoice__r.Id, Quantity__c, Line_Number__c, Description__c, Unit_Price__c, Memo_Line__c, 
//                    Memo_Line_Id__c, Inc_GST__c, Tax_Code__c, Order_Line_Item__c, Order__r.Quantity__c, Order__r.Id, 
//                    Order__r.csord__Identification__c, Order__r.Name, Order_Line_Item__r.csord__Discount_Type__c, Order__c,
//                    Invoice__r.Related_Invoice__c, Invoice__r.Related_Invoice__r.Id, Order__r.Unit_Price__c
        //v1.2
        orderQuantity = ili.Order__r.Quantity__c;
        //v1.1 changes
        invoiceStatus = ili.Invoice__r.Status__c;
        isValidProduct = false;
        isValidInvoice = false;
        isValidStatus = false;

        List<Invoice__C> invoiceList_related_to_currentInv = [SELECT Id,Related_Invoice__c, RecordTypeId, Status__c FROM Invoice__c WHERE Related_Invoice__c = :ili.Invoice__c];

        // Change quantity is allowed only for MDU and SDU products
        if (memolines.contains(Integer.valueOf(ili.Memo_Line_Id__c)))
            isValidProduct = true;

        if (ili.Invoice__r.Related_Invoice__c == null) { // We are in the parent invoice
            boolean isNewInvoiceAllowed = true;
            for (Invoice__C relatedInvoice : invoiceList_related_to_currentInv) { 
                if (relatedInvoice.RecordTypeId == INVOICE_RECORDTYPEID && relatedInvoice.Status__c != 'Cancelled') {
                    isNewInvoiceAllowed = false;
                    invalidInvoice_errorMessage = 'A related invoice already exists. Please cancel the related invoice before making further amendments';
                    //'Cannot modify an which already has an active invoice. Please cancel the existing child invoice';
                    break;
                }
                if (relatedInvoice.RecordTypeId == CREDITMEMO_RECORDTYPEID) {
                    isNewInvoiceAllowed = true;

                }
            }
            isValidInvoice = isNewInvoiceAllowed;
        } else { // Change quantity not applicable for Child invoices
            isValidInvoice = false;
            invalidInvoice_errorMessage = 'Invoice Quantity can only be modified on the original invoice';
            //'Cannot modify a child invoice';
        } 


        //if ( invoiceStatus == 'Cancelled' || invoiceStatus='Cancellation Requested' || invoiceStatus == 'Requested' || 
        //            invoiceStatus == 'Rejected' || invoiceStatus == 'Cancellation Pending Approval' || invoiceStatus == 'Pending Approval')
        if (invoiceStatus == 'Created' || invoiceStatus =='Issued' ) {
            isValidStatus = true; 
        } else {
            for (Invoice__C relatedInvoice : invoiceList_related_to_currentInv) {
                if (relatedInvoice.Status__c == 'Created' || relatedInvoice.Status__c == 'Issued') {
                    isValidStatus = true;
                    break;
                }
            }
        }
    }
    
    public String getQuantityValue() {
        return quantityValue;
    }
    
    
    public String getShowDelta() {
        return String.valueOf(delta);
    }
    
    public Boolean isNumber(string str) {
        Pattern isnumbers = Pattern.Compile('^[-]?[0-9]+$');
        Matcher numberMatch = isnumbers.matcher(str);
        
        return numberMatch.Matches();
    }
    
    public boolean validateNewQuantity(string quantityValue) {
        
        if (String.isBlank(quantityValue)) {
            
            if (!isNumber(quantityValue)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'New Quantity cannot be blank'));
                return false;
            }
            
        } else if (!quantityValue.isNumeric()) {
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'New Quantity should be a Whole number > 0'));
            return false;
        }

        if(!String.isBlank(quantityValue) && Integer.valueOf(quantityValue) == 0)
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'New Quantity should be a Whole number > 0'));
            return false;
        }
        
        return true;
    }
    
    public PageReference CalculateDelta() {
        if (!validateNewQuantity(quantityValue)) {
            cannotChangeQty = false;
            return null;
        }
        
        /*if (String.isBlank(quantityValue)) {
            cannotChangeQty = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'New Quantity value cannot be null'));
            return null;
        } else if (!quantityValue.isNumeric()) {
            cannotChangeQty = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'New Quantity value entered is not numeric'));
            return null;
        }*/
        
        cannotChangeQty = true;
        
        //2821
        //v1.1 change - this SOQL is not required as ILI is already fetched
        //Invoice_Line_Item__c invoiceLineItem = 
        //    [SELECT Id, Order__r.Quantity__c,Quantity__c from Invoice_Line_Item__c where id = :iliId];
        
        // delta = decimal.valueOf(quantityValue) - invoiceLineItem.Quantity__c;
        delta =  decimal.valueOf(quantityValue) - ili.Order__r.Quantity__c;
        return null;
    }
    
    public PageReference Cancel() {
        
        cannotChangeQty = true;
        
        PageReference pageRef = new PageReference('/' + this.ili.Id);
        
        return pageRef; 
    }
    
    public PageReference NewQuantity() 
    {
        Savepoint sp = Database.setSavepoint();

        PageReference pageRef;
        try
        {
            ObjectCommonSettings__c invoiceStatuses = ObjectCommonSettings__c.getValues('Invoice Statuses');
        
            if (!validateNewQuantity(quantityValue))
                return null;
            
            Invoice_Line_Item__c invoiceLineItem = ili;
            //v1.1 
            /* Invoice_Line_Item__c invoiceLineItem =   [SELECT Id, name, Invoice__r.Id, Quantity__c, Line_Number__c, Description__c, Unit_Price__c, Memo_Line__c, 
                    Memo_Line_Id__c, Inc_GST__c, Tax_Code__c, Order_Line_Item__c, Order__r.Quantity__c, Order__r.Id, 
                    Order__r.csord__Identification__c, Order__r.Name, Order_Line_Item__r.csord__Discount_Type__c, Order__c,
                    Invoice__r.Related_Invoice__c, Invoice__r.Related_Invoice__r.Id, Order__r.Unit_Price__c
                    from Invoice_Line_Item__c where id = :iliId];
            */
            // Compare new quantity entered with the original quantity value
            //2821
            if (quantityValue == String.valueOf(invoiceLineItem.Quantity__c/*invoiceLineItem.Order__r.Quantity__c*/)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'The change you are requesting is equal to the original value.'));
                return null;
            }
            
            // Get all Invoice Line Items associated with the original Invoice
            List<Invoice_Line_Item__c> iliList = (invoiceLineItem.Invoice__r.Related_Invoice__c == null) ?
                [SELECT id, name from Invoice_Line_Item__c where Invoice__c = :invoiceLineItem.Invoice__r.Id] : 
                    [SELECT id, name from Invoice_Line_Item__c where Invoice__c = : invoiceLineItem.Invoice__r.Related_Invoice__r.Id];
            
            
            // validate new value for Quantity field
            if (iliList.size() == 1 && quantityValue == '0') {
                
                //if (quantityValue == '0') {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'New Quantity value cannot be zero'));
                    pageRef = null;
                //}
                
            } else if(iliList.size() >= 1) {
                
                Invoice__c invoiceOriginal = 
                    [SELECT id, name, Related_Invoice__c, Related_Invoice__r.Id, Opportunity__c, Account__c, Contract__c,
                        Opportunity__r.Id, Solution__c, Unpaid_Balance__c, Opportunity__r.RecordType.Name, Opportunity__r.contract.RecordType.Name 
                        from Invoice__c where Id = :invoiceLineItem.Invoice__r.Id];
                    
                AggregateResult[] sumOppQuantity = 
                    [SELECT sum(quantity__c) sumQty from csord__Order__c 
                    where csordtelcoa__Opportunity__c = :invoiceOriginal.Opportunity__r.Id and id != :invoiceLineItem.Order__c];
                

                // calculate final total Quantity from the Orders (i.e. new quantity value of this Order plus sum of all quantities of remaining orders)
                integer finalTotalQuantity = (iliList.size() > 1) ? Integer.valueOf(quantityValue) + Integer.valueOf(sumOppQuantity[0].get('sumQty')) : Integer.valueOf(quantityValue);
                
                system.debug('finalTotalQty: ' + finalTotalQuantity + '; sumOppQuantity: ' + Integer.valueOf(sumOppQuantity[0].get('sumQty')));
                
                // if the final total quantity is less than or equal to 0, prompt error to go to Cancel Invoice
                if (finalTotalQuantity <= 0) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Final total Quantity is less or equal to 0. Please Cancel this Invoice.'));
                    return null;
                }
                
                //2821 - v1.1 changes - Fetch the delta by comparing against Order not against ILI.quantity
                //decimal delta = decimal.valueOf(quantityValue) - invoiceLineItem.Quantity__c;
                decimal delta = decimal.valueOf(quantityValue) - invoiceLineItem.Order__r.Quantity__c;

                // Compare new Quantity value with old one:
                // i. If new order quantity is greater, issue the delta as a new invoice.
                // ii. If new order quantity is lesser, issue a credit note.
                Invoice__c creditMemoNew, invoiceNew;
                
                if (delta <= 0) {
                    system.debug('A Credit Memo Line Item will be generated with quantity value equals to ' + delta);
                    
                    creditMemoNew = new Invoice__c 
                            ( Status__c = invoiceStatuses.Pending_Approval__c//'Pending Approval'
                            , Name = invoiceStatuses.Invoice_Name__c//'To be issued'
                            , Opportunity__c = invoiceOriginal.Opportunity__c
                            , Account__c = invoiceOriginal.Account__c
                            , Contract__c = invoiceOriginal.Contract__c
                            , RecordTypeId = [Select Id, Name From RecordType WHERE Name = :invoiceStatuses.Record_Type_Credit_Memo__c/*'Credit Memo'*/].Id
                            , Related_Invoice__c = invoiceOriginal.Id
                            , Solution__c = invoiceOriginal.Solution__c
                            //, Transaction_Type__c = transactionTypeMappingsMap.get(invoiceStatuses.Record_Type_Credit_Memo__c/*'Credit Memo'*/).get(invoiceOriginal.Opportunity__r.RecordType.Name)
                            , Transaction_Type__c = transactionTypeNewDevMappingsMap.get(invoiceStatuses.Record_Type_Credit_Memo__c/*'Credit Memo'*/).get(invoiceOriginal.Opportunity__r.contract.RecordType.Name)
                            , Payment_Status__c = null
                            , Reason_Code__c = selectedReasonCode
                            );
                    
                    insert creditMemoNew;
                    
                    Invoice_Line_Item__c creditMemoLineItemNew = new Invoice_Line_Item__c
                            ( Line_Number__c = invoiceLineItem.Line_Number__c
                            , Description__c = invoiceLineItem.Description__c
                            , RecordTypeId = Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByName().get('Credit Memo Line Item').getRecordTypeId()
                            , Quantity__c = delta
                            , Unit_Price__c = invoiceLineItem.Unit_Price__c//invoiceLineItem.Order__r.Unit_Price__c
                            , Memo_Line__c = invoiceLineItem.Memo_Line__c
                            , Memo_Line_Id__c = invoiceLineItem.Memo_Line_Id__c
                            , Inc_GST__c = invoiceLineItem.Inc_GST__c
                            , Tax_Code__c = invoiceLineItem.Tax_Code__c
                            , Invoice__c = creditMemoNew.Id
                            , Reason_Code__c = selectedReasonCode
                            , Order__c = invoiceLineItem.Order__r.Id
                            , ILI_Type__c = invoiceStatuses.ILI_Type_Qty_Change__c//'Qty Change'
                            , Related_Line_Item__c = invoiceLineItem.Id
                            );
                            
                    insert creditMemoLineItemNew;
                    
                    // Firing the approval process
                    CS_ChangeInvoiceUtils.submitApproval(creditMemoNew.Id);
                    
                    pageRef = new PageReference('/' + creditMemoNew.Id);
                    
                } else {
                    system.debug('A new Invoice Line Item will be generated with quantity value equals to ' + delta);
                    
                    invoiceNew = new Invoice__c
                            ( Status__c = invoiceStatuses.Pending_Approval__c//'Pending Approval'
                            , Name = invoiceStatuses.Invoice_Name__c//'To be issued'
                            , Opportunity__c = invoiceOriginal.Opportunity__c
                            , Account__c = invoiceOriginal.Account__c
                            , Contract__c = invoiceOriginal.Contract__c
                            , RecordTypeId = [Select Id, Name From RecordType WHERE Name = :invoiceStatuses.Record_Type_Invoice__c/*'Invoice'*/].Id
                            , Related_Invoice__c = invoiceOriginal.Id
                            , Solution__c = invoiceOriginal.Solution__c
                            , Unpaid_Balance__c = delta * invoiceLineItem.Unit_Price__c
                            //, Transaction_Type__c = transactionTypeMappingsMap.get(invoiceStatuses.Record_Type_Invoice__c/*'Invoice'*/).get(invoiceOriginal.Opportunity__r.Contract.RecordType.Name)
                            , Transaction_Type__c = transactionTypeNewDevMappingsMap.get(invoiceStatuses.Record_Type_Invoice__c/*'Invoice'*/).get(invoiceOriginal.Opportunity__r.contract.RecordType.Name)
                            );  
                    
                    insert invoiceNew;
                    
                    Invoice_Line_Item__c invoiceLineItemNew = new Invoice_Line_Item__c
                            ( Line_Number__c = invoiceLineItem.Line_Number__c
                            , Description__c = invoiceLineItem.Description__c
                            , RecordTypeId = Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByName().get('Invoice Line Item').getRecordTypeId()
                            , Quantity__c = delta
                            , Unit_Price__c = invoiceLineItem.Unit_Price__c//invoiceLineItem.Order__r.Unit_Price__c
                            , Memo_Line__c = invoiceLineItem.Memo_Line__c
                            , Memo_Line_Id__c = invoiceLineItem.Memo_Line_Id__c
                            , Inc_GST__c = invoiceLineItem.Inc_GST__c
                            , Tax_Code__c = invoiceLineItem.Tax_Code__c
                            , Invoice__c = invoiceNew.Id
                            , Reason_Code__c = selectedReasonCode
                            , Order__c = invoiceLineItem.Order__r.Id
                            , ILI_Type__c = invoiceStatuses.ILI_Type_Qty_Change__c//'Qty Change'
                            , Related_Line_Item__c = invoiceLineItem.Id
                            );
                            
                    insert invoiceLineItemNew;

                    // Firing the approval process
                    CS_ChangeInvoiceUtils.submitApproval(invoiceNew.Id);
                    
                    pageRef = new PageReference('/' + invoiceNew.Id);
                }
                
                system.debug('FinalTotalQuantity: ' + finalTotalQuantity + '; SelectedReasonCode: ' + selectedReasonCode);
            }
        }catch(DmlException ex)
        {
            ApexPages.addMessage(new Apexpages.message(Apexpages.Severity.Error,ex.getMessage()));
            pageRef = null;
            Database.rollback(sp);
        }
        return pageRef;
    }
    
}