@isTest
private class CS_ChangeQuantityInvoiceCtrExt_TEST {

	@testsetup static void setupCommonData() {
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		
		User uEGM = new User 
			( Alias = 'mstandt'
			, Email = 'executiveGeneralManager@cloudsensesolutions.com.au.cpqdev'
			, EmailEncodingKey = 'ISO-8859-1'
			, LastName = 'Testing'
			, LanguageLocaleKey = 'en_US'
			, LocaleSidKey = 'en_AU'
			, ProfileId = p.Id
			, TimeZoneSidKey = 'Australia/Sydney'
			, UserName = 'executiveGeneralManager@nbnco.com.au.cpqdev'
			);

		insert uEGM;
		
		User uGM = new User 
			( Alias = 'mstandt'
			, Email = 'generalManager@cloudsensesolutions.com.au.cpqdev'
			, EmailEncodingKey = 'ISO-8859-1'
			, LastName = 'Testing'
			, LanguageLocaleKey = 'en_US'
			, LocaleSidKey = 'en_AU'
			, ProfileId = p.Id
			, TimeZoneSidKey = 'Australia/Sydney'
			, UserName = 'generalManager@nbnco.com.au.cpqdev'
			, managerid = uEGM.id
			);

		insert uGM;
		
		User umgr = new User 
			( Alias = 'mstandt'
			, Email = 'manafer@cloudsensesolutions.com.au.cpqdev'
			, EmailEncodingKey = 'ISO-8859-1'
			, LastName = 'Testing'
			, LanguageLocaleKey = 'en_US'
			, LocaleSidKey = 'en_AU'
			, ProfileId = p.Id
			, TimeZoneSidKey = 'Australia/Sydney'
			, UserName = 'manafer@nbnco.com.au.cpqdev'
			, managerid = uGM.id
			);

		insert umgr;

      	User u = new User
      		( Alias = 'standt'
      		, Email = 'sysAdminuser@cloudsensesolutions.com.au.cpqdev'
      		, EmailEncodingKey = 'ISO-8859-1'
      		, LastName = 'Testing'
      		, LanguageLocaleKey = 'en_US'
      		, LocaleSidKey = 'en_AU'
      		, ProfileId = p.Id
      		, TimeZoneSidKey = 'Australia/Sydney'
      		, UserName = 'sitUser@cloudsensesolutions.com'
      		, managerid = umgr.id
      		);
      	
      	insert u;
      	
      	ObjectCommonSettings__c invSet = 
        	new ObjectCommonSettings__c(Name = 'Invoice Statuses', New__c = 'New', Requested__c = 'Requested', Cancelled__c = 'Cancelled', Pending_Approval__c = 'Pending Approval', 
        		Cancellation_Requested__c = 'Cancellation Requested', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval', Invoice_Name__c = 'To be issued', 
				 ILI_Type_Qty_Change__c = 'Qty Change', Record_Type_Credit_Memo__c = 'Credit Memo', Record_Type_Invoice__c = 'Invoice', Discount_Type_Amount__c = 'Amount');
		
		insert invSet;
		
		ObjectCommonSettings__c solSet = 
        	new ObjectCommonSettings__c(Name = 'Solution Statuses', New__c = 'New', Requested__c = 'Requested', Cancelled__c = 'Cancelled', Pending_Approval__c = 'Pending Approval', 
        		Cancellation_Requested__c = 'Cancellation Requested', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval', 
        			Rejected__c = 'Rejected', Delivery__c = 'Delivery', Complete__c = 'Complete');
		
		insert solSet;
	}
	
	static User getUser() {
		return [SELECT id, name from User where UserName = 'sitUser@cloudsensesolutions.com' LIMIT 1];
	}
	
	static User getUser2() {
		return [SELECT id, name from User where UserName = 'executiveGeneralManager@nbnco.com.au.cpqdev' LIMIT 1];
	}

	static Invoice_Line_Item__c prepareData() {
		Opportunity testOpty = new Opportunity
			( Name = 'Test Opp'
			, CloseDate = System.today()
			, StageName = 'Invoicing'
			, Approving_Status__c = 'Internal Approval Received'
			, Invoicing_Status__c = 'Pending'
			);
        
		insert testOpty;
         
        system.assert(testOpty.Id != null);
		
		
		csord__Solution__c testSolution = new csord__Solution__c
			( Name = 'Test Opp'
            , Opportunity__c = testOpty.Id
            , csord__Status__c = 'New'
            , Status__c = 'New'
            , csord__Identification__c = 'a2QN0000000Kq5KMAS'
            );
            
        insert testSolution;
        
        csord__Order__c testOrder = new csord__Order__c
			( Name = 'PCD Relocation'
			, csord__Solution__c = testSolution.Id
			, csordtelcoa__Opportunity__c = testOpty.Id
			, csord__Status2__c = 'Order Submitted'
			, csord__Order_Type__c = 'Product Delivery'
			, csord__Product_Type__c = 'Recoverable Works'
			, csord__Identification__c = 'Order_a1hN0000000n31JIAQ_0'
			);
		
		insert testOrder;
		
		Invoice__c testInvoice = new Invoice__c
				( Status__c = 'Requested'
				, Name = 'To be issued'
				, Opportunity__c = testOpty.Id
				, RecordTypeId = [Select Id, Name From RecordType WHERE Name ='Invoice'].Id
				, Solution__c = testSolution.Id
				, Unpaid_Balance__c = decimal.valueOf('4025.00')
				, Payment_Status__c = 'UnPaid'
				);	
		
		insert testInvoice;
		
		Invoice_Line_Item__c testInvoiceLineItem = new Invoice_Line_Item__c
				( Quantity__c = decimal.valueOf('1')
                , Unit_Price__c = decimal.valueOf('1150.00')
                , Memo_Line__c = 'Recoverable Works - CPE'
                , Inc_GST__c = true
                , Tax_Code__c = 'GST 10% (NON-CAPITAL)'
                , Invoice__c = testInvoice.Id
                , Order__c = testOrder.Id
                , ILI_Type__c = 'Initial Basket'
				);
				
		insert testInvoiceLineItem;
		
		csord__Order_Line_Item__c testOLI = new csord__Order_Line_Item__c
			( Name = 'PCD Relocation'
			, csord__Order__c = testOrder.Id
			, csord__Line_Description__c = 'PCD Relocation'
			, Solution__c = testSolution.Id
			, OLI_Type__c = 'Initial Basket'
			, Memo_Line__c = 'Recoverable Works - CPE'
			, Invoice__c = testInvoice.Id
			, Invoice_Line_Item__c = testInvoiceLineItem.Id
			, GST__c = 'GST (Inclusive)'
			, Cost__c = decimal.valueOf('1000.00')
			, Margin__c = decimal.valueOf('15.00')
			, Quantity__c = decimal.valueOf('1')
			, Unit_Price__c = decimal.valueOf('1150.00')
			, csord__Identification__c = 'a1FN0000000vnwwMAA_0'
			);	
		
		insert testOLI;
		
		return testInvoiceLineItem;
	}
	
	static List<Invoice_Line_Item__c> prepareData_multipleProducts() {
		Opportunity testOpty = new Opportunity
			( Name = 'Test Opp'
			, CloseDate = System.today()
			, StageName = 'Invoicing'
			, Approving_Status__c = 'Internal Approval Received'
			, Invoicing_Status__c = 'Pending'
			);
        
		insert testOpty;
         
        system.assert(testOpty.Id != null);
		
		
		csord__Solution__c testSolution = new csord__Solution__c
			( Name = 'Test Opp'
            , Opportunity__c = testOpty.Id
            , csord__Status__c = 'New'
            , Status__c = 'New'
            , csord__Identification__c = 'a2QN0000000Kq5KMAS'
            );
            
        insert testSolution;
        
        csord__Order__c testOrder = new csord__Order__c
			( Name = 'PCD Relocation'
			, csord__Solution__c = testSolution.Id
			, csordtelcoa__Opportunity__c = testOpty.Id
			, csord__Status2__c = 'Order Submitted'
			, csord__Order_Type__c = 'Product Delivery'
			, csord__Product_Type__c = 'Recoverable Works'
			, csord__Identification__c = 'Order_a1hN0000000n31JIAQ_0'
			);
		
		insert testOrder;
		
		csord__Order__c testOrder2 = new csord__Order__c
			( Name = 'NS Installation'
			, csord__Solution__c = testSolution.Id
			, csordtelcoa__Opportunity__c = testOpty.Id
			, csord__Status2__c = 'Order Submitted'
			, csord__Order_Type__c = 'Product Delivery'
			, csord__Product_Type__c = 'Recoverable Works'
			, csord__Identification__c = 'Order_a1hN0000000n31JIAR_0'
			);
		
		insert testOrder2;
		
		Invoice__c testInvoice = new Invoice__c
				( Status__c = 'Requested'
				, Name = 'To be issued'
				, Opportunity__c = testOpty.Id
				, RecordTypeId = [Select Id, Name From RecordType WHERE Name ='Invoice'].Id
				, Solution__c = testSolution.Id
				, Unpaid_Balance__c = decimal.valueOf('4025.00')
				, Payment_Status__c = 'UnPaid'
				);	
		
		insert testInvoice;
		
		Invoice_Line_Item__c testInvoiceLineItem = new Invoice_Line_Item__c
				( Quantity__c = decimal.valueOf('1')
                , Unit_Price__c = decimal.valueOf('1150.00')
                , Memo_Line__c = 'Recoverable Works - CPE'
                , Inc_GST__c = true
                , Tax_Code__c = 'GST 10% (NON-CAPITAL)'
                , Invoice__c = testInvoice.Id
                , Order__c = testOrder.Id
                , ILI_Type__c = 'Initial Basket'
				);
				
		insert testInvoiceLineItem;
		
		Invoice_Line_Item__c testInvoiceLineItem2 = new Invoice_Line_Item__c
				( Quantity__c = decimal.valueOf('1')
                , Unit_Price__c = decimal.valueOf('3967.50')
                , Memo_Line__c = 'Recoverable Works - Small'
                , Inc_GST__c = true
                , Tax_Code__c = 'GST 10% (NON-CAPITAL)'
                , Invoice__c = testInvoice.Id
                , Order__c = testOrder2.Id
                , ILI_Type__c = 'Initial Basket'
				);
				
		insert testInvoiceLineItem2;
		
		csord__Order_Line_Item__c testOLI = new csord__Order_Line_Item__c
			( Name = 'PCD Relocation'
			, csord__Order__c = testOrder.Id
			, csord__Line_Description__c = 'PCD Relocation'
			, Solution__c = testSolution.Id
			, OLI_Type__c = 'Initial Basket'
			, Memo_Line__c = 'Recoverable Works - CPE'
			, Invoice__c = testInvoice.Id
			, Invoice_Line_Item__c = testInvoiceLineItem.Id
			, GST__c = 'GST (Inclusive)'
			, Cost__c = decimal.valueOf('1000.00')
			, Margin__c = decimal.valueOf('15.00')
			, Quantity__c = decimal.valueOf('1')
			, Unit_Price__c = decimal.valueOf('1150.00')
			, csord__Identification__c = 'a1FN0000000vnwwMAA_0'
			);	
		
		insert testOLI;
		
		csord__Order_Line_Item__c testOLI2 = new csord__Order_Line_Item__c
			( Name = 'NS Installation'
			, csord__Order__c = testOrder2.Id
			, csord__Line_Description__c = 'NS Installation'
			, Solution__c = testSolution.Id
			, OLI_Type__c = 'Initial Basket'
			, Memo_Line__c = 'Recoverable Works - CPE'
			, Invoice__c = testInvoice.Id
			, Invoice_Line_Item__c = testInvoiceLineItem2.Id
			, GST__c = 'GST (Inclusive)'
			, Cost__c = decimal.valueOf('3450.00')
			, Quantity__c = decimal.valueOf('1')
			, Unit_Price__c = decimal.valueOf('3967.50')
			, csord__Identification__c = 'a1FN0000000vnwwMAB_0'
			);	
		
		insert testOLI2;
		
		
		Invoice__c testInvoiceCreated = new Invoice__c 
			( Status__c = 'Requested'
			, Name = 'To be issued'
			, Opportunity__c = testOpty.Id
			, RecordTypeId = [Select Id, Name From RecordType WHERE Name ='Invoice'].Id
			, Related_Invoice__c = testInvoice.Id
			, Solution__c = testSolution.Id
			, Payment_Status__c = 'UnPaid'
			);
		
		insert testInvoiceCreated;
		
		Invoice_Line_Item__c testIliCreated = new Invoice_Line_Item__c
			( Quantity__c = decimal.valueOf('3')
            , Unit_Price__c = decimal.valueOf('1150.00')
            , Memo_Line__c = 'Recoverable Works - CPE'
            , Inc_GST__c = true
            , Tax_Code__c = 'GST 10% (NON-CAPITAL)'
            , Invoice__c = testInvoiceCreated.Id
            , Reason_Code__c = 'Accounts Receivables Billing Error'
            , Order__c = testOrder.Id
            , ILI_Type__c = 'Qty Change'
            , Related_Line_Item__c = testInvoiceLineItem.Id
			);
				
		insert testIliCreated;
		
		csord__Order_Line_Item__c testOLI3 = new csord__Order_Line_Item__c
			( Name = 'PCD Relocation'
			, csord__Order__c = testOrder.Id
			, csord__Line_Description__c = 'PCD Relocation'
			, Solution__c = testSolution.Id
			, OLI_Type__c = 'Qty Change'
			, Memo_Line__c = 'Recoverable Works - CPE'
			, Invoice__c = testInvoiceCreated.Id
			, Invoice_Line_Item__c = testIliCreated.Id
			, GST__c = 'GST (Inclusive)'
			, Cost__c = decimal.valueOf('1150.00')
			, Quantity__c = decimal.valueOf('3')
			, Unit_Price__c = decimal.valueOf('1150.00')
			, csord__Identification__c = 'a1FN0000000vnwwMAC_0'
			);	
		
		insert testOLI3;	
		
		List<Invoice_Line_Item__c> testIliList = new List<Invoice_Line_Item__c>();
		testIliList.add(testInvoiceLineItem);
		testIliList.add(testInvoiceLineItem2);
		testIliList.add(testIliCreated);
		
		return testIliList;
	}

	static testMethod void changeInvoiceQuantity_methodOne() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeQuantityInvoiceCtrExt_TEST.prepareData();
		
      	System.runAs(getUser()) { 
		
			//*** Change Quantity to 4
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
	        
	        System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeQuantityInvoiceCtrExt controller = new CS_ChangeQuantityInvoiceCtrExt();
			controller.quantityValue = '4';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit quantity change request for approval
			test.startTest();
				controller.NewQuantity();
			test.stopTest();
			
			
			// Instantiate the new ProcessWorkitemRequest object and populate it
	        Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
	        request.setAction('Approve');
	        request.setWorkitemId([SELECT Id FROM ProcessInstanceWorkitem].Id);
	        request.setComments('Approved');
	        
	        // Submit the request for approval
	        Approval.ProcessResult process_result = Approval.process(request);
			
			// Validating results
			Invoice_Line_Item__c iliTest = [SELECT id, name, Order__r.Id, Invoice__r.Id from Invoice_Line_Item__c where Id = :testInvoiceLineItem.Id];
			
			csord__Order__c parentOrderTest = [SELECT id, name, Quantity__c from csord__Order__c where Id = :iliTest.Order__r.Id];
			
			Invoice__c creditMemoGeneratedTest = [SELECT id, name, Status__c, Record_Type_Name__c from Invoice__c where Related_Invoice__c = :iliTest.Invoice__r.Id];
			
			system.assertEquals(4, parentOrderTest.Quantity__c);
			system.assertEquals('Requested', creditMemoGeneratedTest.Status__c);
			system.assertEquals('Invoice', creditMemoGeneratedTest.Record_Type_Name__c);
			
      	}
		
	}
	
	static testMethod void changeInvoiceAmount_methodTwo() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeQuantityInvoiceCtrExt_TEST.prepareData();

      	System.runAs(getUser()) { 
		
			//*** Change Quantity to invalid number
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
	        
	        System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeQuantityInvoiceCtrExt controller = new CS_ChangeQuantityInvoiceCtrExt();
			controller.quantityValue = '3B';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit quantity change request for approval
			test.startTest();
				controller.NewQuantity();
			test.stopTest();	
			
			// Validating results
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			
			system.assertEquals('New Quantity should be a Whole number > 0', msgs.get(0).getDetail());

      	}
	}
	
	static testMethod void changeInvoiceAmount_methodThree() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeQuantityInvoiceCtrExt_TEST.prepareData();

      	System.runAs(getUser()) { 
		
			//*** Change Quantity to invalid number
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
	        
	        System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeQuantityInvoiceCtrExt controller = new CS_ChangeQuantityInvoiceCtrExt();
			controller.quantityValue = '';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit quantity change request for approval
			test.startTest();
				controller.NewQuantity();
			test.stopTest();	
			
			// Validating results
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			
			system.assertEquals('New Quantity cannot be blank', msgs.get(0).getDetail());

      	}
	}
	
	static testMethod void changeInvoiceAmount_methodFour() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeQuantityInvoiceCtrExt_TEST.prepareData();

      	System.runAs(getUser()) { 
		
			//*** Change Quantity to 0
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
	        
	        System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeQuantityInvoiceCtrExt controller = new CS_ChangeQuantityInvoiceCtrExt();
			controller.quantityValue = '0';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit quantity change request for approval
			test.startTest();
				controller.NewQuantity();
			test.stopTest();	
			
			// Validating results
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			
			system.assertEquals('New Quantity should be a Whole number > 0', msgs.get(0).getDetail());

      	}
	}
	
	/*static testMethod void changeInvoiceQuantity_methodFive() {
		
		List<Invoice_Line_Item__c> testInvoiceLineItemList = CS_ChangeQuantityInvoiceCtrExt_TEST.prepareData_multipleProducts();
		
		Invoice_Line_Item__c testInvoiceLineItem = testInvoiceLineItemList.get(1);

      	System.runAs(getUser()) { 
		
			//*** Change Quantity of one product to 0
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
	        
	        System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeQuantityInvoiceCtrExt controller = new CS_ChangeQuantityInvoiceCtrExt();
			controller.quantityValue = '0';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit quantity change request for approval
			test.startTest();
				controller.NewQuantity();
			test.stopTest();
			
			
			// Instantiate the new ProcessWorkitemRequest object and populate it
	        Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
	        request.setAction('Approve');
	        request.setWorkitemId([SELECT Id FROM ProcessInstanceWorkitem].Id);
	        request.setComments('Approved');
	        
	        // Submit the request for approval
	        Approval.ProcessResult process_result = Approval.process(request);
			
			// Validating results
			Invoice_Line_Item__c iliTest = 
				[SELECT id, name, Order__r.Id, Invoice__r.Id, Order__r.csordtelcoa__Opportunity__r.Id from Invoice_Line_Item__c where Id = :testInvoiceLineItem.Id];
			
			csord__Order__c parentOrderTest = [SELECT id, name, Quantity__c from csord__Order__c where Id = :iliTest.Order__r.Id];
			
			Invoice__c creditMemoGeneratedTest = 
				[SELECT id, name, Status__c, Record_Type_Name__c from Invoice__c where Related_Invoice__c = :iliTest.Invoice__r.Id and Record_Type_Name__c = 'Credit Memo'];
			
			Invoice_Line_Item__c cmliGeneratedTest = [SELECT id, name, Quantity__c from Invoice_Line_Item__c where Invoice__c = :creditMemoGeneratedTest.Id];
			
			system.assertEquals(0, parentOrderTest.Quantity__c);
			system.assertEquals('Requested', creditMemoGeneratedTest.Status__c);
			system.assertEquals('Credit Memo', creditMemoGeneratedTest.Record_Type_Name__c);
			system.assertEquals(-1, cmliGeneratedTest.Quantity__c);
			
      	}
		
	}*/
	
	static testMethod void changeInvoiceQuantity_methodSix() {
		
		List<Invoice_Line_Item__c> testInvoiceLineItemList = CS_ChangeQuantityInvoiceCtrExt_TEST.prepareData_multipleProducts();
		
		Invoice_Line_Item__c testInvoiceLineItem = testInvoiceLineItemList.get(1);

      	System.runAs(getUser()) { 
		
			//*** Change Quantity of one product to 100
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
	        
	        System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeQuantityInvoiceCtrExt controller = new CS_ChangeQuantityInvoiceCtrExt();
			controller.quantityValue = '100';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit quantity change request for approval
			test.startTest();
				controller.NewQuantity();
			test.stopTest();
			
			
			// Instantiate the new ProcessWorkitemRequest object and populate it
	        Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
	        request.setAction('Approve');
	        request.setWorkitemId([SELECT Id FROM ProcessInstanceWorkitem].Id);
	        request.setComments('Approved');
	        
	        // Submit the request for approval
	        Approval.ProcessResult process_result = Approval.process(request);
	        
	        // Verify the result
        	system.assert(process_result.isSuccess());
			
			// Instantiate the new ProcessWorkitemRequest object and populate it
	        request = new Approval.ProcessWorkitemRequest();
	        request.setAction('Approve');
	        request.setWorkitemId([SELECT Id FROM ProcessInstanceWorkitem].Id);
	        request.setComments('Approved');
	        
	        
	        // Submit the request for approval
	        process_result = Approval.process(request);
			
			// Validating results
			Invoice_Line_Item__c iliTest = [SELECT id, name, Order__r.Id, Invoice__r.Id, Order__r.csordtelcoa__Opportunity__r.Id from Invoice_Line_Item__c where Id = :testInvoiceLineItem.Id];
			
			csord__Order__c parentOrderTest = [SELECT id, name, Quantity__c from csord__Order__c where Id = :iliTest.Order__r.Id];
			
			Invoice_Line_Item__c iliGeneratedTest = 
				[SELECT id, name, Invoice__r.Amount__c, Invoice__r.Record_Type_Name__c, Invoice__r.Status__c from Invoice_Line_Item__c where Related_Line_Item__c = :iliTest.Id];	
			
			system.assertEquals(100, parentOrderTest.Quantity__c);
			system.assertEquals('Requested', iliGeneratedTest.Invoice__r.Status__c);
			system.assertEquals('Invoice', iliGeneratedTest.Invoice__r.Record_Type_Name__c);
			system.assertEquals(392782.50, iliGeneratedTest.Invoice__r.Amount__c);
      	}
		
	}
	
	/*static testMethod void changeInvoiceQuantity_methodSeven() {
		
		List<Invoice_Line_Item__c> testInvoiceLineItemList = CS_ChangeQuantityInvoiceCtrExt_TEST.prepareData_multipleProducts();
		
		Invoice_Line_Item__c testInvoiceLineItem = testInvoiceLineItemList.get(0);
		Invoice_Line_Item__c testInvoiceLineItem2 = testInvoiceLineItemList.get(1);

      	System.runAs(getUser()) { 
		
			//*** Change Quantity of first product to 0
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
	        
	        System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeQuantityInvoiceCtrExt controller = new CS_ChangeQuantityInvoiceCtrExt();
			controller.quantityValue = '0';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit quantity change request for approval
			controller.NewQuantity();
			
			
			// Instantiate the new ProcessWorkitemRequest object and populate it
	        Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
	        request.setAction('Approve');
	        request.setWorkitemId([SELECT Id FROM ProcessInstanceWorkitem].Id);
	        request.setComments('Approved');
	        
	        // Submit the request for approval
	        Approval.ProcessResult process_result = Approval.process(request);
	        
	        // Verify the result
        	system.assert(process_result.isSuccess());
			
			//*** Change Quantity of second product to 0
			// Approve the submitted request
	        //ApexPages.StandardController sc2 = new ApexPages.standardController(testInvoiceLineItem2);
	        
	        System.currentPageReference().getParameters().put('id', testInvoiceLineItem2.Id);
			CS_ChangeQuantityInvoiceCtrExt controller2 = new CS_ChangeQuantityInvoiceCtrExt();
			controller2.quantityValue = '0';
			controller2.selectedReasonCode = 'Duplicate Billing';
			// submit quantity change request for approval
			controller2.NewQuantity();
			
			// Validating results
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			
			system.assertEquals('Final total Quantity is less or equal to 0. Please Cancel this Invoice.', msgs.get(0).getDetail());
      	}
		
	}*/
	
	static testMethod void changeInvoiceQuantity_methodEight() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeQuantityInvoiceCtrExt_TEST.prepareData();
		
		
		//*** Cancel Change Quantity
        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
        
        System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
		CS_ChangeQuantityInvoiceCtrExt controller = new CS_ChangeQuantityInvoiceCtrExt();
		test.startTest();
			PageReference backToInvoiceLineItemURL = controller.Cancel();
		test.stopTest();
		
		system.assertEquals('/' + testInvoiceLineItem.Id, backToInvoiceLineItemURL.getUrl());
	}
	
	static testMethod void changeInvoiceQuantity_methodNine() {
		
		Invoice_Line_Item__c testInvoiceLineItem = CS_ChangeQuantityInvoiceCtrExt_TEST.prepareData();
		
		
		//*** Change Quantity - Calculate delta
        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
        
        System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
		CS_ChangeQuantityInvoiceCtrExt controller = new CS_ChangeQuantityInvoiceCtrExt();
		controller.quantityValue = '4';
		string quantityNew = controller.getQuantityValue();
		controller.CalculateDelta();
		string delta = controller.getShowDelta();
		
		system.assertEquals('3', delta);
		system.assertEquals('4', quantityNew);
		
		List<SelectOption> rcs = controller.getReasonCodes();
		
		system.assertEquals('A/R ERROR', rcs.get(0).getValue());
		system.assertEquals('INC_PAY_TERM', rcs.get(5).getValue());
		system.assertEquals('TAX_EXEMPT', rcs.get(11).getValue());
		
		//*** Change Quantity - Calculate delta (New quantity value is invalid number)
		controller = new CS_ChangeQuantityInvoiceCtrExt();
		controller.quantityValue = 'A';
		controller.CalculateDelta();
		
		List<Apexpages.Message> msgs = ApexPages.getMessages();
			
		system.assertEquals('New Quantity should be a Whole number > 0', msgs.get(0).getDetail());
	}
	
	/*static testMethod void changeInvoiceQuantity_methodTen() {
		
		List<Invoice_Line_Item__c> testInvoiceLineItemList = CS_ChangeQuantityInvoiceCtrExt_TEST.prepareData_multipleProducts();
		
		Invoice_Line_Item__c testInvoiceLineItem = testInvoiceLineItemList.get(2);

      	System.runAs(getUser()) { 
		
			//*** Change Quantity of one product to 0
			// Approve the submitted request
	        //ApexPages.StandardController sc = new ApexPages.standardController(testInvoiceLineItem);
	        
	        System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeQuantityInvoiceCtrExt controller = new CS_ChangeQuantityInvoiceCtrExt();
			controller.quantityValue = '0';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit quantity change request for approval
			test.startTest();
				controller.NewQuantity();
			test.stopTest();
			
			
			// Instantiate the new ProcessWorkitemRequest object and populate it
	        Approval.ProcessWorkitemRequest request = new Approval.ProcessWorkitemRequest();
	        request.setAction('Approve');
	        request.setWorkitemId([SELECT Id FROM ProcessInstanceWorkitem].Id);
	        request.setComments('Approved');
	        
	        // Submit the request for approval
	        Approval.ProcessResult process_result = Approval.process(request);
			
			// Validating results
			Invoice_Line_Item__c iliTest = [SELECT id, name, Order__r.Id, Invoice__r.Id, Order__r.csordtelcoa__Opportunity__r.Id from Invoice_Line_Item__c where Id = :testInvoiceLineItem.Id];
			
			csord__Order__c parentOrderTest = [SELECT id, name, Quantity__c from csord__Order__c where Id = :iliTest.Order__r.Id];
			
			
			
			Invoice_Line_Item__c cmliGeneratedTest = 
				[SELECT id, name, Invoice__r.Amount__c, Invoice__r.Record_Type_Name__c, Invoice__r.Status__c from Invoice_Line_Item__c where Related_Line_Item__c = :iliTest.Id];	
			
			system.assertEquals(0, parentOrderTest.Quantity__c);
			system.assertEquals('Requested', cmliGeneratedTest.Invoice__r.Status__c);
			system.assertEquals('Credit Memo', cmliGeneratedTest.Invoice__r.Record_Type_Name__c);
			system.assertEquals(-4600.00, cmliGeneratedTest.Invoice__r.Amount__c);*/
			
			
			
			/*Invoice__c creditMemoGeneratedTest = [SELECT id, name, Status__c, Record_Type_Name__c from Invoice__c where Related_Invoice__c = :iliTest.Invoice__r.Id];
			
			Invoice_Line_Item__c cmliGeneratedTest = [SELECT id, name, Quantity__c from Invoice_Line_Item__c where Invoice__c = :creditMemoGeneratedTest.Id];
			
			system.assertEquals(0, parentOrderTest.Quantity__c);
			system.assertEquals('Requested', creditMemoGeneratedTest.Status__c);
			system.assertEquals('Credit Memo', creditMemoGeneratedTest.Record_Type_Name__c);
			system.assertEquals(-1, cmliGeneratedTest.Quantity__c);*/
			
      	/*}
		
	}*/
	
	static testmethod void changeInvoiceQuantity_methodEleven() {
		List<Invoice_Line_Item__c> testInvoiceLineItemList = CS_ChangeQuantityInvoiceCtrExt_TEST.prepareData_multipleProducts();
		
		Invoice_Line_Item__c testInvoiceLineItem = testInvoiceLineItemList.get(2);

      	System.runAs(getUser2()) { 
	        
	        System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeQuantityInvoiceCtrExt controller = new CS_ChangeQuantityInvoiceCtrExt();
			controller.quantityValue = '1';
			controller.selectedReasonCode = 'Duplicate Billing';
			// submit quantity change request for approval
			test.startTest();
				controller.NewQuantity();
			test.stopTest();
			
			Boolean hasMessages = ApexPages.hasMessages(Apexpages.Severity.Error);
			
			system.assertEquals(true, hasMessages);
      	}
	}
	
	/*static testMethod void changeInvoiceQuantity_methodTwelve() {
		
		List<Invoice_Line_Item__c> testInvoiceLineItemList = CS_ChangeQuantityInvoiceCtrExt_TEST.prepareData_multipleProducts();
		
		Invoice_Line_Item__c testInvoiceLineItem = testInvoiceLineItemList.get(0);
		
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];

      	System.runAs(getUser()) { 
			
			System.currentPageReference().getParameters().put('id', testInvoiceLineItem.Id);
			CS_ChangeQuantityInvoiceCtrExt controller2 = new CS_ChangeQuantityInvoiceCtrExt();

			controller2.quantityValue = '4';
			controller2.selectedReasonCode = 'Duplicate Billing';
			// submit unit price change request for approval
			controller2.NewQuantity();
			
			// Validating results
			List<Apexpages.Message> msgs = ApexPages.getMessages();
			
			system.assertEquals('The change you are requesting is equal to the original value.', msgs.get(0).getDetail());
      	}
		
	}*/
}