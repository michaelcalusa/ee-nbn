/*************************************************
 Developed by: Li Tan
 Description: Batch class to upgrade and revalidate Product Configurations, and resync the Product Baskets to Opportunity.
 Version History:
 - v1.0, 2017-03-13, LT: Created
*/

global class CS_CofigurationUpgradeBatch implements Database.Batchable<sObject>, Database.Stateful{

    global final string query;
    global set<Id> basketIdSet = new set<Id>();

    global CS_CofigurationUpgradeBatch(){

        //, cscfga__Configuration_Offer__c, cscfga__Product_Basket__c, cscfga__Product_Basket__r.cscfga__Opportunity__c, cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName    
        query = 'SELECT Id, cscfga__Product_Basket__c'
        		+ ' FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Definition__c != NULL'
        		+ ' AND cscfga__Product_Basket__c != NULL'
        		+ ' AND cscfga__Product_Definition__r.cscfga__IsArchived__c = true'
        		+ ' AND cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != ' + '\'' + 'Invoicing' + '\''
        		+ ' AND cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != ' + '\'' + 'Waiting For Payment' + '\''
        		+ ' AND cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != ' + '\'' + 'Completing' + '\''
        		+ ' AND cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != ' + '\'' + 'Closed - Complete' + '\''
        		+ ' AND cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != ' + '\'' + 'Closed - Incomplete' + '\''
        		+ ' AND cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != ' + '\'' + 'Closed Won' + '\''
        		+ ' AND cscfga__Product_Basket__r.cscfga__Opportunity__r.StageName != ' + '\'' + 'Closed Lost' + '\''
        		+ ' ORDER BY CreatedDate DESC';

       	system.debug(query);
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<cscfga__Product_Configuration__c> scope){

        list<Id> pcIdList = new list<Id>();

        for (cscfga__Product_Configuration__c pc: scope) {
            pcIdList.add(pc.id);
            basketIdSet.add(pc.cscfga__Product_Basket__c);
        }

        // upgrade configurations
        cfgug1.ProductConfigurationUpgrader.upgradeConfigurations(pcIdList);
		system.debug('*** ProductConfigurationUpgrader - Completed ***');

        // revalidate configurations (i.e. re-run the rules)
        set<Id> pcIdSet = new set<Id>(pcIdList); 
        cscfga.ProductConfigurationBulkActions.revalidateConfigurations(pcIdSet);
		system.debug('*** revalidateConfigurations - Completed ***');

    }

    global void finish(Database.BatchableContext BC){
        // resync basket to opportunities
        // , cscfga__opportunity__c, csbb__Synchronised_with_Opportunity__c, cscfga__Basket_Status__c  
        list<cscfga__Product_Basket__c> pbList = [select id 
                                        from cscfga__Product_Basket__c where cscfga__Basket_Status__c='Valid' and id in :basketIdSet];

        for (cscfga__Product_Basket__c pb: pbList) {
            pb.csbb__Synchronised_with_Opportunity__c = true;
            pb.csordtelcoa__Synchronised_with_Opportunity__c = true;
        }
        update pbList;

    }
}