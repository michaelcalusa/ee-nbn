global class CS_CofigurationUpgradeOfferBatch implements Database.Batchable<sObject>{

    global final string query;

    global CS_CofigurationUpgradeOfferBatch(){

        query = 'SELECT Id FROM cscfga__Product_Configuration__c where cscfga__Configuration_Offer__c != null AND cscfga__Product_Definition__r.cscfga__IsArchived__c = true';
        //query = 'SELECT Id FROM cscfga__Product_Configuration__c where cscfga__Configuration_Offer__c != null';

        system.debug(query);
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<cscfga__Product_Configuration__c> scope){

        list<Id> pcIdList = new list<Id>();

        for (cscfga__Product_Configuration__c pc: scope) {
            pcIdList.add(pc.id);
        }

        // upgrade configurations
        cfgug1.ProductConfigurationUpgrader.upgradeConfigurations(pcIdList);
        system.debug('*** ProductConfigurationUpgrader - Completed ***');

        // revalidate configurations (i.e. re-run the rules)
        set<Id> pcIdSet = new set<Id>(pcIdList); 
        cscfga.ProductConfigurationBulkActions.revalidateConfigurations(pcIdSet);
        system.debug('*** revalidateConfigurations - Completed ***');

    }

    global void finish(Database.BatchableContext BC){

    }
}