@isTest
public class CS_ConfigUpgradeBatchSchedClass_TEST {
	static testmethod void test() {
		Test.startTest();
		
		Opportunity testOpty = new Opportunity(Name = 'Test Opp', CloseDate = System.today(), StageName = 'New');
        
		insert testOpty;
         
        system.assert(testOpty.Id != null);
        
        cscfga__Product_Basket__c testBasket  = 
        	new cscfga__Product_Basket__c(cscfga__Basket_Status__c = 'Valid', csbb__Synchronised_with_Opportunity__c = false, Name = 'BasketGenerate_New_Invoice', 
        		cscfga__opportunity__c = testOpty.Id);
            
        insert testBasket;
        
        system.assert(testBasket.Id != null);
        
        cscfga__Product_Category__c testProdCateg = TestDataClass.createProdCateg();
        testProdCateg.Name = 'nbn';
        testProdCateg.cscfga__Active__c = true;
        
        insert testProdCateg;
    
        cscfga__Product_Definition__c testProDef = TestDataClass.createProDef(testProdCateg);
        testProDef.Name = 'Recoverable Works2';
        testProDef.csordtelcoa__Product_Type__c = 'Delivery';
        testProDef.cscfga__IsArchived__c = true;
        
        insert testProDef;
        
        cscfga__Product_Configuration__c  testProdConfig = new cscfga__Product_Configuration__c();
		
		// Add some products to the basket
		testProdConfig.cscfga__Product_Basket__c = testBasket.Id;
        testProdConfig.cscfga__Product_Definition__c = testProDef.id;
		testProdConfig.Name = 'PCD Relocation';
		
		insert testProdConfig;
        
        system.assert(testProdConfig.Id != null);

		String CRON_EXP = '0 0 0 6 * ?';

		// schedule the test job
		String jobId = System.schedule('CS_ConfigUpgradeBatchScheduledApexClass', CRON_EXP, new CS_ConfigUpgradeBatchSchedClass());
		
		// Get the information from the CronTrigger API object
      	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime from CronTrigger where id = :jobId];
		
		// Verify the job has not run
      	system.assertEquals(0, ct.TimesTriggered);
		
		Test.stopTest();
	}
}