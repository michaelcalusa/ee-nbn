/*************************************************
- Developed by: Hugo Pinto
- Date Created: 06/04/2017 (dd/MM/yyyy)
- Description: 
- Version History:
- v1.0 - 06/04/2017, HP: Created
*/

global class CS_ConfigUpgradeOfferBatchSchedClass implements Schedulable {
	global void execute(SchedulableContext sc) {
		CS_CofigurationUpgradeOfferBatch cuob = new CS_CofigurationUpgradeOfferBatch();
		
		database.executebatch(cuob,1);
	}
}