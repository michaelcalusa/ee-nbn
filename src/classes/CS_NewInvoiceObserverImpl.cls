/*************************************************
- Developed by: Hugo Pinto
- Date Created: 18/01/2017 (dd/MM/yyyy)
- Description: This class will read the values from Order, Order Line Item to create the Invoice and Invoice Line Item. 
                The default Invoice Record Type = Invoice.
                This class will be executed after order generation is complete. To do so, the class will implement 
                csordcb.ObserverApi.IObserver interface defined in the O&S Codebase package.
- Version History:
- v1.0 - 18/01/2017, HP: Created
- v1.1 - 24/02/2017, LT: Mapps attributes to OLI fields.
- v1.2 - 2017-03-07, LT: Populate csord__Type__c with transction type from invoice
- v1.3 - 2017-10-23, SG & RS: Updated transaction type for new Devs 
- v1.4 - 2018-03-20, Suman Gunaganti: redesigned to accommodate  invoice split functionality
- v1.5 - 2019-03-01, Shan Dhinakaran / Ramesh : To use Async mode, see inline comments

*************************************************/

global class CS_NewInvoiceObserverImpl implements csordcb.ObserverApi.IObserver {
    public Map<Id, List<id>> oppidMap = new Map<Id, List<ID>>();
    public Map<Id, set<Id>> opptyOrderidMap = new Map<Id, set<Id>>();
    public map<Id, Set<Id>> orderRequestToOppty = new map<Id, Set<Id>>();
    public List<String> listOfAllowedOpptyRTypes = new List<String>();
    public static csordtelcoa.OrderGenerationObservable observable;
       
    global CS_NewInvoiceObserverImpl() {
        system.debug('Calling CS_NewInvoiceObserverImpl constructor');
    }
    
    global void execute(csordcb.ObserverApi.Observable obs, Object arg) {
        system.debug('>>>>Entering CS_NewInvoiceObserverImpl '+arg);
        observable = (csordtelcoa.OrderGenerationObservable) obs;
        list<Id> orderIds = observable.getOrderIds();
        list<Id> orderRequestIds = observable.getOrderRequestIds();
        for(New_Invoice_Oberver_Settings__mdt nios: [Select Opportunity_Record_Type__c, Generate_Invoice_for_Opportunity_Rtype__c from New_Invoice_Oberver_Settings__mdt]){
            if(nios.Generate_Invoice_for_Opportunity_Rtype__c)listOfAllowedOpptyRTypes.add(nios.Opportunity_Record_Type__c);
        }
        for(csord__Order__c o: [Select csordtelcoa__Opportunity__c, csord__Order_Request__c, OpportunityRecordType__c 
                                From csord__Order__c 
                                where Id IN :orderIds AND OpportunityRecordType__c IN:listOfAllowedOpptyRTypes]){
            if(oppidMap.containsKey(o.csordtelcoa__Opportunity__c))
                oppidMap.get(o.csordtelcoa__Opportunity__c).add(o.Id);
            else 
                oppidMap.put(o.csordtelcoa__Opportunity__c, new List<ID>{o.Id});
            if(orderRequestToOppty.containsKey(o.csord__Order_Request__c)) 
                orderRequestToOppty.get(o.csord__Order_Request__c).add(o.csordtelcoa__Opportunity__c);
            else 
                orderRequestToOppty.put(o.csord__Order_Request__c, new Set<Id>{o.csordtelcoa__Opportunity__c});
            if(opptyOrderidMap.containsKey(o.csordtelcoa__Opportunity__c))
                opptyOrderidMap.get(o.csordtelcoa__Opportunity__c).add(o.Id);
            else 
                opptyOrderidMap.put(o.csordtelcoa__Opportunity__c, new Set<Id>{o.Id});
        }
// Updated from >1 to >0 due to Prod issue with Governer limits to always run in Async mode
        if(oppidMap.size()>0) {
            System.enqueueJob(new generateInvoicesAsync(orderRequestIds,opptyOrderidMap,oppidMap,orderRequestToOppty));
         }
       /* else if(oppidMap.size() == 1){
            system.debug('synchronous call started');
            generateInvoiceService.generateInvoices(orderRequestIds,opptyOrderidMap,oppidMap,orderRequestToOppty);
            system.debug('synchronous call Ended');
        }*/
       
    }
 
}