public Class CS_ProductBasketTriggerHandler extends Triggerhandler
{
    public static boolean isError = false;
    public CS_ProductBasketTriggerHandler()
    {
        if(Test.isRunningTest()){
            this.setMaxLoopCount(10);
        }
        else{
            this.setMaxLoopCount(2);
        }
        System.Debug( 'NBN: -> CS_ProductBasketTriggerHandler Invoked' );
    }
    
    protected override void beforeUpdate(map<id,SObject> oldMap, map<id,SObject> newMap)
    {
        getTotalCostForBasket(newMap);     
    }
    
    //Get Sum of Cost from Child Prod Config objects and set it at basket level in Total Cost
    public void getTotalCostForBasket(map<id,SObject> newMap)
    {
        set<Id> basketIds = new set<Id>();
        
        for(Sobject obj: newmap.values())
        {
            basketIds.add(obj.Id);
        }
        //Get Sum of Cost from Prod Config Request objects and set it at basket level
        AggregateResult[] basketConfigs=[Select cscfga__Product_Basket__c,Sum(Cost__c) total
                                        From cscfga__Product_Configuration__c
                                        where cscfga__Product_Basket__c in:basketIds
                                        GROUP BY cscfga__Product_Basket__c];
        
        for(AggregateResult result:basketConfigs)
        {
            cscfga__Product_Basket__c basket = (cscfga__Product_Basket__c)newmap.get((Id)result.get('cscfga__Product_Basket__c'));
            basket.Total_Cost__c = (Decimal)result.get('total');
        }
    }
}