@isTest
private class CS_ProductBasketTriggerHandler_Test {

    
    @isTest static void test_method_one() {
        // Create Test Opportunity
        Opportunity objOpp = TestDataClass.CreateTestOpportunityData();
        
        //Create Product basket under Opportunity
        cscfga__Product_Basket__c objBasket = TestDataClass.CreateTestBasketData(objOpp.Id);


        cscfga__Product_Category__c testProdCateg = TestDataClass.createProdCateg();
        insert testProdCateg;
    
        cscfga__Product_Definition__c testProDef = TestDataClass.createProDef(testProdCateg);
        insert testProDef;
    
        cscfga__Product_Configuration__c prodConfig = TestDataClass.createProdConfig(objBasket);
        prodConfig.cscfga__Configuration_Status__c ='Valid';
        prodConfig.cscfga__Product_Definition__c = testProDef.id;
        prodConfig.Cost__c = 100;
        insert prodConfig;
        
        objBasket = [Select Id, Total_Cost__c from cscfga__Product_Basket__c where Id =:objBasket.Id];
        
        System.assertEquals(objBasket.Total_Cost__c,prodConfig.Cost__c);
        

    }
    

    
    
}