public with sharing class CS_TransactionHistory {
 
   public Account acc{get;set;}

    public CS_TransactionHistory(ApexPages.StandardController controller) {
     this.acc = (Account)controller.getRecord();

    }
    
    public pageReference TransactionHistory(){
    
     PageReference pageRef= new PageReference('/apex/CS_ARTransactionHistory');
     pageRef.getParameters().put('accountId', acc.Id);
     pageRef.setRedirect(false);
     System.debug('account' +'accountId');
     return pageRef;
    
      
      
   }

}