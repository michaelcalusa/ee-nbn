/*--------------------------------------------------------------------------------------------------------------------------
Author:        Sidharth Manure
Description:   This Class calculates Business days between two Qualification steps and updtes on CQ step object fields. 
               This class is called from Process Builder 'Elapsed Days between CQ Steps'             
History
<Date>            <Authors Name>        <Brief Description of Change> 
13-11-2017        Sidharth Manure       Created.
---------------------------------------------------------------------------------------------------------------------------*/

public class CalculateBusinessDaysBetweenCQSteps {

 @InvocableMethod
 public static void businessdaysInSteps(list<Id> qualificationStepIdList) {
   System.debug('Qualification Step ID from PB>>>'+qualificationStepIdList);
   List<Qualification_Step__c> stepStartList;
   List<Qualification_Step__c> stepEndList;
   List<Id> qualificationStepLeadIdList;
   
   if (qualificationStepIdList != null && qualificationStepIdList.size()>0) {  
       qualificationStepLeadIdList = new List<Id>(); 
       try {
             stepStartList = [SELECT Id, task__c, status__c,Days_In_Enquiry_received_and_Lead_Triage__c, Date_Ended__c, Related_Lead__c, Credit_check_duration__c, Provision_of_contract_duration__c, Nbn_contract_execution_duration__c 
                               FROM Qualification_Step__c WHERE Id =:qualificationStepIdList]; // Getting Step End Completion Date record                       
              for (Integer i=0;i<stepStartList.size();i++) {
                   qualificationStepLeadIdList.add(stepStartList[i].Related_Lead__c);
              }
              if (qualificationStepLeadIdList != null && qualificationStepLeadIdList.size() >0 ) {                    
                  stepEndList = [SELECT Id, task__c, status__c,Days_In_Enquiry_received_and_Lead_Triage__c,Related_Lead__c,Date_Ended__c,Credit_check_duration__c,Provision_of_contract_duration__c,Nbn_contract_execution_duration__c 
                                  FROM Qualification_Step__c 
                                  WHERE Related_Lead__c =:qualificationStepLeadIdList and task__c IN ('CQ01. Enquiry received','CQ14. Valid Commitment Plan and Credit Application received',
                                        'CQ16. Credit Application Outcome notified','CQ18. WBA contract provisioned by Wholesale Supply','CQ19. Signed WBA contract received from RSP')]; // Getting Step Start Completion Date record
              }
              System.debug('stepStartList>>>'+stepStartList.size()+'**stepEndList>>>'+stepEndList.size());                                                      
             
              if (stepStartList != null && stepStartList.size() > 0 && stepEndList != null && stepEndList.size() >0) {
                  system.debug('Inside - Update business days');
                  for (integer i=0;i<stepStartList.size();i++){  
                       Date stepEndCompletionDate =  date.newinstance(stepStartList[i].Date_Ended__c.year(), stepStartList[i].Date_Ended__c.month(), stepStartList[i].Date_Ended__c.day());              
                       for (integer j=0; j<stepEndList.size(); j++) {
                            if (stepEndList[j].Date_Ended__c != null) {
                                Date stepStartCompletionDate = date.newinstance(stepEndList[j].Date_Ended__c.year(), stepEndList[j].Date_Ended__c.month(), stepEndList[j].Date_Ended__c.day());                                                            
                                if (stepStartList[i].Related_Lead__c == stepEndList[j].Related_Lead__c) {                                
                                    if (stepEndList[j].task__c == 'CQ01. Enquiry received') {                             
                                        System.debug('Inside New lead response duration');                                    
                                        stepStartList[i].Days_In_Enquiry_received_and_Lead_Triage__c = Integer.valueof(calculateWorkingDays(stepStartCompletionDate, stepEndCompletionDate)-1);
                                        if (stepStartList[i].task__c == 'CQ20. WBA contract executed by nbn') {
                                             stepStartList[i].Onboarding_phase_1_lead_duration__c = Integer.valueof(calculateWorkingDays(stepStartCompletionDate, stepEndCompletionDate)-1);
                                        }
                                    }
                                    else if (stepEndList[j].task__c == 'CQ14. Valid Commitment Plan and Credit Application received') {
                                             System.debug('Inside Credit check duration');                                    
                                             stepStartList[i].Credit_check_duration__c = Integer.valueof(calculateWorkingDays(stepStartCompletionDate, stepEndCompletionDate)-1);
                                    }
                                    else if (stepEndList[j].task__c == 'CQ16. Credit Application Outcome notified') {
                                             System.debug('Inside Provision of contract duration');                                    
                                             stepStartList[i].Provision_of_contract_duration__c = Integer.valueof(calculateWorkingDays(stepStartCompletionDate, stepEndCompletionDate)-1);
                                    }
                                    else if (stepEndList[j].task__c == 'CQ19. Signed WBA contract received from RSP') {
                                             System.debug('Inside nbn contract execution duration');                                    
                                             stepStartList[i].Nbn_contract_execution_duration__c = Integer.valueof(calculateWorkingDays(stepStartCompletionDate, stepEndCompletionDate)-1);
                                    }
                                }                                
                            }    
                        }
                   }            
                   update stepStartList; // Updating Step End Completion Date record with Business days   
              }              
        } catch(Exception e) {
              system.debug('Exception Occured'+e);
        }
    }       
  }
  
  /*----------------------------------------------------------------------------------------------------------------------
    Author:         Sidharth Manure
    Description:    "calculateWorkingDays" method calculates business days between two dates.
    ----------------------------------------------------------------------------------------------------------------------*/
  public static Integer calculateWorkingDays(Date startDate, Date endDate)
    { 
        system.debug('Inside calculateWorkingDay function');       
        Set<Date> holidaysSet = new Set<Date>();
        Integer workingDays = 0;       
        for (Holiday currHoliday : [Select ActivityDate from holiday]) {
             holidaysSet.add(currHoliday.ActivityDate);
        }       
        for (integer i=0; i <= startDate.daysBetween(endDate); i++) {
             Date dt = startDate + i;
             DateTime currDate = DateTime.newInstance(dt.year(), dt.month(), dt.day());
             String todayDay = currDate.format('EEEE');
             if (todayDay != 'Saturday' && todayDay !='Sunday' && (!holidaysSet.contains(dt))) {
                workingDays = workingDays + 1;
             }
        }       
        System.debug('--Working days'+workingDays);
        return workingDays;
    }
     
}