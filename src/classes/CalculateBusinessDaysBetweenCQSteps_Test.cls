/*------------------------------------------------------------------------
Author:        Sidharth
Description:   Test class for CalculateBusinessDaysBetweenCQSteps Class
                  1.Create Test Method to calculate business days between steps
               
Class:           CalculateBusinessDaysBetweenCQSteps 
History
<Date>            <Authors Name>    <Brief Description of Change> 
15-11-2017        Sidharth          Created
--------------------------------------------------------------------------*/  
@isTest
private class CalculateBusinessDaysBetweenCQSteps_Test {

  static list<Qualification_Step__c> qualificationStepsIdList;
        
  static void getRecords() { 
      list<RecordType> qualificationStepRecordTypeId; 
      Datetime dt = System.Now();        
      Lead ld = new Lead(Company = 'TestCompany', LastName = 'TestLastName', Status = 'Open');
      insert ld;
      system.debug('Test lead created>>>'+ld);   
      qualificationStepRecordTypeId = [SELECT Id, Name FROM RecordType WHERE sObjectType='Qualification_Step__c' and RecordType.Name ='System Generated Steps'];            
      qualificationStepsIdList= new list<Qualification_Step__c>{ new Qualification_Step__c(Related_Lead__c=ld.Id,RecordTypeId=qualificationStepRecordTypeId[0].Id,Task__c = 'CQ01. Enquiry received',Status__c='Completed',Date_Ended__c=dt-5),
                                                                 new Qualification_Step__c(Related_Lead__c=ld.Id,RecordTypeId=qualificationStepRecordTypeId[0].Id,Task__c = 'CQ02. Lead triaged and stock response sent',Status__c='Completed',Date_Ended__c=dt),
                                                                 new Qualification_Step__c(Related_Lead__c=ld.Id,RecordTypeId=qualificationStepRecordTypeId[0].Id,Task__c = 'CCQ14. Valid Commitment Plan and Credit Application received',Status__c='Completed',Date_Ended__c=dt-10),
                                                                 new Qualification_Step__c(Related_Lead__c=ld.Id,RecordTypeId=qualificationStepRecordTypeId[0].Id,Task__c = 'CQ16. Credit Application Outcome notified',Status__c='Completed',Date_Ended__c=dt-3),
                                                                 new Qualification_Step__c(Related_Lead__c=ld.Id,RecordTypeId=qualificationStepRecordTypeId[0].Id,Task__c = 'CQ18. WBA contract provisioned by Wholesale Supply',Status__c='Completed',Date_Ended__c=dt),
                                                                 new Qualification_Step__c(Related_Lead__c=ld.Id,RecordTypeId=qualificationStepRecordTypeId[0].Id,Task__c = 'CQ19. Signed WBA contract received from RSP',Status__c='Completed',Date_Ended__c=dt-5),
                                                                 new Qualification_Step__c(Related_Lead__c=ld.Id,RecordTypeId=qualificationStepRecordTypeId[0].Id,Task__c = 'CQ20. WBA contract executed by nbn',Status__c='Completed',Date_Ended__c=dt)
                                                               };
      insert qualificationStepsIdList;    
  }
        
  static testmethod void testMethod1() {               
         getRecords();
         List<Id> qualificationStepsMetric1IdList = new list<Id>();
         for (Integer i=0; i<qualificationStepsIdList.size();i++) {
              if (qualificationStepsIdList[i].Task__c == 'CQ02. Lead triaged and stock response sent') {
                  qualificationStepsMetric1IdList.add(qualificationStepsIdList[i].Id);
              }
         }
         system.debug('qualificationStepsMetric1IdList>>>'+qualificationStepsMetric1IdList);  
         Test.starttest();       
         CalculateBusinessDaysBetweenCQSteps.businessdaysInSteps(qualificationStepsMetric1IdList);
         Test.stoptest();
  }
   
  static testmethod void testMethod2() {               
         getRecords();
         system.debug('qs from func>>>'+qualificationStepsIdList);
         List<Id> qualificationStepsMetric2ListId = new list<Id>();
         for (Integer i=0; i<qualificationStepsIdList.size();i++) {
              if (qualificationStepsIdList[i].Task__c == 'CQ16. Credit Application Outcome notified') {
                  qualificationStepsMetric2ListId.add(qualificationStepsIdList[i].Id);
              }
         }
         system.debug('qualificationStepsMetric2ListId>>>'+qualificationStepsMetric2ListId);  
         Test.starttest();           
         CalculateBusinessDaysBetweenCQSteps.businessdaysInSteps(qualificationStepsMetric2ListId);
         Test.stoptest();
   }
   
   static testmethod void testMethod3() {              
         getRecords();
         system.debug('qs from func>>>'+qualificationStepsIdList);
         List<Id> qualificationStepsMetric3ListId = new list<Id>();
         for (Integer i=0; i<qualificationStepsIdList.size();i++) {
              if (qualificationStepsIdList[i].Task__c == 'CQ18. WBA contract provisioned by Wholesale Supply') {
                  qualificationStepsMetric3ListId.add(qualificationStepsIdList[i].Id);
              }
         }
         system.debug('qualificationStepsMetric3ListId>>>'+qualificationStepsMetric3ListId);
         Test.starttest();    
         CalculateBusinessDaysBetweenCQSteps.businessdaysInSteps(qualificationStepsMetric3ListId);
         Test.stoptest();
   }
   
   static testmethod void testMethod4() {               
         getRecords();
         system.debug('qs from func>>>'+qualificationStepsIdList);
         List<Id> qualificationStepsMetric4ListId = new list<Id>();
         for (Integer i=0; i<qualificationStepsIdList.size();i++) {
              if (qualificationStepsIdList[i].Task__c == 'CQ20. WBA contract executed by nbn') {
                  qualificationStepsMetric4ListId.add(qualificationStepsIdList[i].Id);
              }
         }
         system.debug('qualificationStepsMetric4ListId >>>'+qualificationStepsMetric4ListId);
         Test.starttest();    
         CalculateBusinessDaysBetweenCQSteps.businessdaysInSteps(qualificationStepsMetric4ListId);
         Test.stoptest();
   }
}