/*--------------------------------------------------------------------------------------------------------------------------
Author:        Sidharth Manure
Description:   This Class calculates Business days between two Onboarding steps and updtes on Onboarding step object fields. 
               This class is called from Process Builder 'Elapsed Days between Onboarding Steps'             
History
<Date>            <Authors Name>        <Brief Description of Change> 
14-11-2017        Sidharth Manure       Created.
---------------------------------------------------------------------------------------------------------------------------*/

public class CalculateBusinessDaysBetweenOBSteps {

 @InvocableMethod
 public static void businessdaysInSteps(list<Id> onboardingStepIdList) {
   System.debug('Onboarding StepID from PB>>>'+onboardingStepIdList);
   List<On_Boarding_Step__c> stepStartList;
   List<On_Boarding_Step__c> stepEndList;
   List<Id> onboardingStepProcessIdList;
   
   if (onboardingStepIdList != null && onboardingStepIdList.size()>0) {  
       onboardingStepProcessIdList = new List<Id>(); 
       try {
             stepStartList = [SELECT Id, Status__c, Step__c, Date_Ended__c, Related_Product__c , Schedule_RSP_operational_workshop_durati__c, Schedule_RSP_technical_workshop_duration__c  FROM On_Boarding_Step__c 
                              WHERE Id =:onboardingStepIdList]; // Getting Step End Completion Date record                         
              for (Integer i=0;i<stepStartList.size();i++) {
                   onboardingStepProcessIdList.add(stepStartList[i].Related_Product__c);
              }
              if (onboardingStepProcessIdList != null && onboardingStepProcessIdList.size() >0 ) {        
                  stepEndList = [SELECT Id, Status__c, Step__c, Date_Ended__c, Related_Product__c , Schedule_RSP_operational_workshop_durati__c, Schedule_RSP_technical_workshop_duration__c FROM On_Boarding_Step__c 
                                 WHERE Related_Product__c =:onboardingStepProcessIdList and Step__c IN ('3.03 Operational Onboarding Workshop confirmed with RSP', '1.01 Request received for Solutions Definition Workshop', '5.02 First AVC order received')]; // Getting Step Start Completion Date record
              }
              System.debug('stepStartList>>>'+stepStartList.size()+'**stepEndList>>>'+stepEndList.size());                                                      
             
              if (stepStartList != null && stepStartList.size() > 0 && stepEndList != null && stepEndList.size() >0) {
                  system.debug('Inside - Update business days');
                  for (integer i=0;i<stepStartList.size();i++){  
                       Date stepEndCompletionDate =  date.newinstance(stepStartList[i].Date_Ended__c.year(), stepStartList[i].Date_Ended__c.month(), stepStartList[i].Date_Ended__c.day());              
                       for (integer j=0; j<stepEndList.size(); j++) {
                            if (stepEndList[j].Date_Ended__c != null) {
                                Date stepStartCompletionDate = date.newinstance(stepEndList[j].Date_Ended__c.year(), stepEndList[j].Date_Ended__c.month(), stepEndList[j].Date_Ended__c.day());                                                            
                                if (stepStartList[i].Related_Product__c == stepEndList[j].Related_Product__c) {                                
                                    if (stepEndList[j].Step__c == '3.03 Operational Onboarding Workshop confirmed with RSP') {                             
                                        System.debug('Inside Schedule RSP operational workshop duration');                                    
                                        stepStartList[i].Schedule_RSP_operational_workshop_durati__c = Integer.valueof(calculateWorkingDays(stepStartCompletionDate, stepEndCompletionDate)-1);
                                    }
                                    else if(stepEndList[j].Step__c == '1.01 Request received for Solutions Definition Workshop') {
                                            System.debug('Inside Schedule RSP technical workshop duration');                                    
                                            stepStartList[i].Schedule_RSP_technical_workshop_duration__c = Integer.valueof(calculateWorkingDays(stepStartCompletionDate, stepEndCompletionDate)-1);
                                            if (stepStartList[i].Step__c == '5.03 First AVC order acknowledged' ) {
                                                stepStartList[i].Average_end_to_end_Onboarding_duration__c = Integer.valueof(calculateWorkingDays(stepStartCompletionDate, stepEndCompletionDate)-1);
                                            }
                                    }
                                    else if(stepEndList[j].Step__c == '5.02 First AVC order received') {                                                                             
                                            if (stepStartList[i].Step__c == '5.03 First AVC order acknowledged' ) {
                                                stepStartList[i].AVC_order_duration__c = Integer.valueof(calculateWorkingDays(stepStartCompletionDate, stepEndCompletionDate)-1);   
                                            }
                                    }
                                }                                
                            }    
                        }
                   }            
                   update stepStartList; //Updating Step End Completion Date record with Business days   
              }              
        } catch(Exception e) {
              system.debug('Exception Occured'+e);
        }
    }       
  } 
  
  /*----------------------------------------------------------------------------------------------------------------------
    Author:         Sidharth Manure
    Description:    "calculateWorkingDays" method calculates business days between two dates.
    ----------------------------------------------------------------------------------------------------------------------*/
  public static Integer calculateWorkingDays(Date startDate, Date endDate)
    { 
        system.debug('Inside calculateWorkingDay function');      
        Set<Date> holidaysSet = new Set<Date>();
        Integer workingDays = 0;       
        for (Holiday currHoliday : [Select ActivityDate from holiday]) {
             holidaysSet.add(currHoliday.ActivityDate);
        }       
        for (integer i=0; i <= startDate.daysBetween(endDate); i++) {
             Date dt = startDate + i;
             DateTime currDate = DateTime.newInstance(dt.year(), dt.month(), dt.day());
             String todayDay = currDate.format('EEEE');
             if (todayDay != 'Saturday' && todayDay !='Sunday' && (!holidaysSet.contains(dt))) {
                workingDays = workingDays + 1;
             }
        }       
        System.debug('--Working days'+workingDays);
        return workingDays;
    }
     
}