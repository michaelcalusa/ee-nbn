/*------------------------------------------------------------------------
Author:        Sidharth
Description:   Test class for CalculateBusinessDaysBetweenOBSteps Class
                  1.Create Test Method to calculate business days between steps
               
Class:           CalculateBusinessDaysBetweenOBSteps
History
<Date>            <Authors Name>    <Brief Description of Change> 
15-11-2017        Sidharth          Created
--------------------------------------------------------------------------*/  
@isTest
private class CalculateBusinessDaysBetweenOBSteps_Test {

  static list<On_Boarding_Step__c> onboardingStepsIdList;
        
  static void getRecords() { 
      list<RecordType> onboardingStepRecordTypeId; 
      Datetime dt = System.Now();
      Date dy = Date.valueof(System.Now());
      onboardingStepRecordTypeId = [SELECT Id, Name FROM RecordType WHERE sObjectType='Account' and RecordType.Name ='Customer'];          
      //Lead ld = new Lead(Company = 'TestCompany', LastName = 'TestLastName', Status = 'Open');
      Account acc = new Account(Name='Testaccount',RecordTypeId=onboardingStepRecordTypeId[0].Id,Tier__c='5',Type__c='ASP',Status__c='Active');
      insert acc;
      Onboarding_Process__c op = new Onboarding_Process__c(Account_ProdOnB__c=acc.ID,Onboarding_Product__c='LTSS',Start_Date__c=dy);
      insert op;  
      system.debug('Test account created>>>'+acc+'Onboarding Process created**'+op);   
      onboardingStepRecordTypeId = [SELECT Id, Name FROM RecordType WHERE sObjectType='On_Boarding_Step__c' and RecordType.Name ='System Created Product OnBoarding Steps'];            
      onboardingStepsIdList= new list<On_Boarding_Step__c>{ new On_Boarding_Step__c(Related_Product__c=op.Id,Status__c='Completed',Date_Ended__c=dt,Step__c='3.03 Operational Onboarding Workshop confirmed with RSP'),
                                                            new On_Boarding_Step__c(Related_Product__c=op.Id,Status__c='Completed',Date_Ended__c=dt,Step__c='3.04 Operational Onboarding Workshop scheduled'),
                                                            new On_Boarding_Step__c(Related_Product__c=op.Id,Status__c='Completed',Date_Ended__c=dt,Step__c='1.01 Request received for Solutions Definition Workshop'),
                                                            new On_Boarding_Step__c(Related_Product__c=op.Id,Status__c='Completed',Date_Ended__c=dt,Step__c='1.02 Solutions Definition Workshop scheduled'),
                                                            new On_Boarding_Step__c(Related_Product__c=op.Id,Status__c='Completed',Date_Ended__c=dt,Step__c='5.03 First AVC order acknowledged'),
                                                            new On_Boarding_Step__c(Related_Product__c=op.Id,Status__c='Completed',Date_Ended__c=dt,Step__c='5.02 First AVC order received')
                                                          };
      insert onboardingStepsIdList;    
  }
        
  static testmethod void testMethod1() {               
         getRecords();
         List<Id> onboardingStepsMetric1IdList = new list<Id>();
         for (Integer i=0; i<onboardingStepsIdList.size();i++) {
              if (onboardingStepsIdList[i].Step__c == '3.04 Operational Onboarding Workshop scheduled') {
                  onboardingStepsMetric1IdList .add(onboardingStepsIdList[i].Id);
              }
         }
         system.debug('onboardingStepsMetric1IdList >>>'+onboardingStepsMetric1IdList );  
         Test.starttest();       
         CalculateBusinessDaysBetweenOBSteps.businessdaysInSteps(onboardingStepsMetric1IdList);
         Test.stoptest();
  }
   
  static testmethod void testMethod2() {               
         getRecords();
         system.debug('os from func>>>'+onboardingStepsIdList);
         List<Id> onboardingStepsMetric2ListId = new list<Id>();
         for (Integer i=0; i<onboardingStepsIdList.size();i++) {
              if (onboardingStepsIdList[i].Step__c == '1.02 Solutions Definition Workshop scheduled') {
                  onboardingStepsMetric2ListId.add(onboardingStepsIdList[i].Id);
              }
         }
         system.debug('onboardingStepsMetric2ListId>>>'+onboardingStepsMetric2ListId);  
         Test.starttest();           
         CalculateBusinessDaysBetweenCQSteps.businessdaysInSteps(onboardingStepsMetric2ListId);
         Test.stoptest();
   }
   
   static testmethod void testMethod3() {               
         getRecords();
         system.debug('os from func>>>'+onboardingStepsIdList);
         List<Id> onboardingStepsMetric3ListId = new list<Id>();
         for (Integer i=0; i<onboardingStepsIdList.size();i++) {
              if (onboardingStepsIdList[i].Step__c == '5.03 First AVC order acknowledged') {
                  onboardingStepsMetric3ListId.add(onboardingStepsIdList[i].Id);
              }
         }
         system.debug('onboardingStepsMetric3ListId>>>'+onboardingStepsMetric3ListId);  
         Test.starttest();           
         CalculateBusinessDaysBetweenCQSteps.businessdaysInSteps(onboardingStepsMetric3ListId);
         Test.stoptest();
   }
   
   static testmethod void testMethod4() {               
         getRecords();
         system.debug('os from func>>>'+onboardingStepsIdList);
         List<Id> onboardingStepsMetric4ListId = new list<Id>();
         for (Integer i=0; i<onboardingStepsIdList.size();i++) {
              if (onboardingStepsIdList[i].Step__c == '5.02 First AVC order received') {
                  onboardingStepsMetric4ListId.add(onboardingStepsIdList[i].Id);
              }
         }
         system.debug('onboardingStepsMetric4ListId>>>'+onboardingStepsMetric4ListId);  
         Test.starttest();           
         CalculateBusinessDaysBetweenCQSteps.businessdaysInSteps(onboardingStepsMetric4ListId);
         Test.stoptest();
   }
   
}