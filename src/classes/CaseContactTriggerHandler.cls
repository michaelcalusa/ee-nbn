/*------------------------------------------------------------------------
Author:        Andrew Zhang
Company:       NBNco
Description:   Handler of Case Contact triggers, including prevent update on closed cases, update Contact Type On Contact
Test Class:    CaseContactTriggerHandler_Test
History
<Date>            <Authors Name>    <Brief Description of Change> 
--------------------------------------------------------------------------*/

public without sharing class CaseContactTriggerHandler{

    public static Boolean avoidTrigger = false;
      
    public static void beforeInsertHandler(){  
        preventUpdateOnClosedCase((List<Case_Contact__c>)Trigger.new);
    }
    
    public static void beforeUpdateHandler(){   
        preventUpdateOnClosedCase((List<Case_Contact__c>)Trigger.new);      
    }
    
    public static void beforeDeleteHandler(){ 
        preventUpdateOnClosedCase((List<Case_Contact__c>)Trigger.old);        
    }
    
    public static void afterInsertHandler(){ 
        updateContactTypeOnContact((List<Case_Contact__c>)Trigger.new); 
        createSiteContacts((List<Case_Contact__c>)Trigger.new);        
    }
    
    public static void afterUpdateHandler(){ 
        updateContactTypeOnContact((List<Case_Contact__c>)Trigger.new);
        //createSiteContacts((List<Case_Contact__c>)Trigger.new);        
    }
    
    
    // Update Contact Type field on Contact Object
    public static void updateContactTypeOnContact(List<Case_Contact__c> caseContacts){
       Set<Id> contactIdSet = new Set<Id>();
       Map<Id, List<Case_Contact__c>> caseContactMap = new  Map<Id, List<Case_Contact__c>>();
      
       for (Case_Contact__c cc : caseContacts){
           // Prepare Map of contact and related case contacts     
           if(!caseContactMap.containskey(cc.Contact__c))
           {
               caseContactMap.put(cc.Contact__c, new List<Case_Contact__c>());
               caseContactMap.get(cc.Contact__c).add(cc);
           }
           else
           {
               caseContactMap.get(cc.Contact__c).add(cc);
           }
           contactIdSet.add(cc.Contact__c);
       }
      
        Map<Id, Contact> contactMap = new Map<Id, Contact>([Select id,Contact_Type__c from Contact where id in: contactIdSet]);
        List<Contact> contactsToUpdate = new List<Contact>();

        for(id cid: caseContactMap.keyset())
        {
           List<Case_Contact__c> lstCaseContacts = caseContactMap.get(cid);
           List<String> contactTypeValues = new List<String>();
           Set<String> newContactTypeValuesSet = new Set<String>();
           Set<String> existingContactTypeValuesSet = new Set<String>();
           if(lstCaseContacts.size()>0)
           {
              for(Case_Contact__c cc: lstCaseContacts)
              {  
                 newContactTypeValuesSet.add(cc.Role__c);                
              }

              contact c = contactMap.get(cid);
              string existingValuesString = (c.Contact_Type__c!=null)? c.Contact_Type__c:'';
              List<String> existingvalues = existingValuesString.split(';');

              If(existingvalues.size()>0)
              {
                for(string val: existingvalues){ 
                  newContactTypeValuesSet.add(val);
                }
              }      
          
              c.Contact_Type__c = format(newContactTypeValuesSet); 
              c.Created_From_SearchWizard__c = True;
              contactsToUpdate.add(c);
           }
        }
        
        if(contactsToUpdate.size()>0)
          update contactsToUpdate;  
    }
    
    public static String format(Set<String> values) {
        if (values == null) return null;
        List<String> l = new List<String>(values);
        l.sort();
        return format(l);
    }
    
    public static String format(List<String> values) {
        if (values == null) return null;
        return String.join(values, ';');
    }
    
    
    // Create Site Contacts after the creation of case contact
    public static void createSiteContacts(List<Case_Contact__c> caseContacts){     
         List<Site_Contacts__c> lstSiteContactsToBeInserted = new List<Site_Contacts__c>();
         set<string> setCaseContactIds = new set<string>();
         set<string> setRelatedContactIds = new set<string>();
         map<string,string> mapCaseAndSite = new map<string, string>();
          map<string,string> mapContactAndSite = new map<string, string>();
         for(Case_Contact__c cc: caseContacts)
         {
            setCaseContactIds.add(cc.Case__c);
            setRelatedContactIds.add(cc.Contact__c);
         }
         for(case c: [select site__c from case where id in: setCaseContactIds ])
         {
             if(c.site__c!=null)             
              mapCaseAndSite.put(c.id, c.site__c);
         }
         for(contact con: [select id,Residential_Address__c from contact where id in: setRelatedContactIds]){
            
             if(con.Residential_Address__c!=null)
               mapContactAndSite.put(con.id, con.Residential_Address__c);           
         }
         for(Case_Contact__c cc: caseContacts)
         {
           if(mapCaseAndSite.get(cc.Case__c)!=null && mapCaseAndSite.get(cc.Case__c)!= mapContactAndSite.get(cc.Contact__c))
            {
                Site_Contacts__c sc = new Site_Contacts__c();
                sc.Related_Contact__c = cc.Contact__c;
                sc.Related_Site__c = mapCaseAndSite.get(cc.Case__c);
                sc.Role__c = cc.Role__c;            
                lstSiteContactsToBeInserted.add(sc);
            }   
         }
         database.saveresult[] results = database.insert(lstSiteContactsToBeInserted, false);
    }
    
    private static void preventUpdateOnClosedCase(List<Case_Contact__c> caseContacts){
    
        Set<Id> caseIdSet = new Set<Id>();
        for (Case_Contact__c cc : caseContacts){
            caseIdSet.add(cc.Case__c);
        }
        
        Map<Id, Case> caseMap = new Map<Id, Case>([SELECT Id, Status, RecordType.Name, CreatedDate, Is_Save_And_Close_Button_Clicked__c FROM Case WHERE Id IN :caseIdSet AND Status = 'Closed']);
        Set<Id> caseIdsToReset = new Set<Id>();
        
        for (Case_Contact__c cc : caseContacts){
            
            if (caseMap.containsKey(cc.Case__c)){
                Case caseObj = caseMap.get(cc.Case__c);
                
                if (caseObj.Is_Save_And_Close_Button_Clicked__c == false){                
                    if (caseObj.RecordType.Name == 'Query'   ||
                        caseObj.RecordType.Name == 'Complex Query' ||
                        caseObj.RecordType.Name == 'Complex Complaint' ||
                        caseObj.RecordType.Name == 'Formal Complaint' ||
                        caseObj.RecordType.Name == 'Urgent Complaint'){
                        cc.addError(label.Case_Contact_Query_Complaint_Error_Message);
                    }
                    
                    if (caseObj.RecordType.Name == 'LiFD'){
                        cc.addError(label.Case_Contact_LiFD_Error_Message);
                    }
                }
            }             
        } 
        

    }    

}