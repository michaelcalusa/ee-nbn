/*------------------------------------------------------------------------
Author:        Andrew Zhang
Company:       NBNco
Description:   Test class for CaseContactTriggerHandler
History
<Date>      <Authors Name>     <Brief Description of Change>
----------------------------------------------------------------------------*/

@isTest
private class CaseContactTriggerHandler_Test {
    
    static testmethod void testInsertUpdateDelete(){
                
        //Setup data
        Account account = new Account(
            Name = 'Test Account',
            RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
            Tier__c = '1',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert account;

        Contact contact = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId(),
            AccountId = account.Id,
            Email = 'teste11111@example.com',
            Preferred_Phone__c = '0211111111'
        );
        insert contact;

        Contact contact2 = new Contact(
            LastName = 'Test Contact2',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId(),
            AccountId = account.Id,
            Email = 'teste22222@example.com',
            Preferred_Phone__c = '0222222222'
        );
        insert contact2;
                    
        Site__c site = new Site__c(
            Name = 'test site'
        );
        insert site;
        
        Case caseObj = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId(),
            Phase__c = 'Information',
            Category__c = '(I) Communications',
            Sub_Category__c = 'Discovery Centre',
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
            ContactId = contact.Id,
            Primary_Contact_Role__c = 'General Public',
            Site__c = site.Id
        );
        insert caseObj;
        
        
        test.startTest();   
                
        caseObj.OwnerId = UserInfo.getUserId();
        caseObj.Status = 'Closed';
        update caseObj;
        
        //check case contact created
        List<Case_Contact__c> ccList = [SELECT Id, Case__c, Primary__c FROM Case_Contact__c WHERE Case__c = :caseObj.Id];
        System.assert(ccList.size()>0);
         
        //Check Cannot insert new case contact           
        try{
            insert new Case_Contact__c(
                Case__c = caseObj.Id,
                Role__c = 'Contractor',
                Contact__c = contact2.Id
            );             
        }catch(Exception e){
            System.Assert(e.getMessage().contains(label.Case_Contact_Query_Complaint_Error_Message));
        }
        
        
        //Check Cannot update old case contact
        try{
            Case_Contact__c oldCC = ccList[0];
            oldCC.Role__c = 'Contractor';
            update oldCC;
        }catch(Exception e){
            System.Assert(e.getMessage().contains(label.Case_Contact_Query_Complaint_Error_Message));
        }
        
        
        //Check Cannot delete case contact            
        try{
            delete ccList;
        }catch(Exception e){
            System.Assert(e.getMessage().contains(label.Case_Contact_Query_Complaint_Error_Message));
        }
                                
        
        test.stopTest();        
        

    }

    
    static testmethod void testLiFDCase(){
        
        //Setup data
        Account account = new Account(
            Name = 'Test Account',
            RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
            Tier__c = '1',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert account;
        
        Contact contact = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId(),
            AccountId = account.Id,
            Email = 'teste@example.com',
            Preferred_Phone__c = '0288889999'
        );
        insert contact;
        
        Site__c site = new Site__c(
            Name = 'test site'
        );
        insert site;
        
        Case caseObj = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('LiFD').getRecordTypeId(),
            LiFD_Category__c = 'HFC - Brownfields',
            Origin = 'Email',
            Status = 'Closed',
            Priority = '1-ASAP',
            ContactId = contact.Id
        );
        insert caseObj;
        
        
        test.startTest();   
         
        //Check Cannot insert new case contact
        try{
            insert new Case_Contact__c(
                Case__c = caseObj.Id,
                Role__c = 'Contractor',
                Contact__c = contact.Id
            );             
        }catch(Exception e){
            System.Assert(e.getMessage().contains(label.Case_Contact_LiFD_Error_Message));
        }

        test.stopTest();        
    }    
   
}