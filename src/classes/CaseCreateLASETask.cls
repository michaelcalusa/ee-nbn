/*------------------------------------------------------------------------
Author:        Nagalingeswar Reddy Vempalli
Company:       Wipro
Description:   A controller class which allows to create a LASE task for LASE Escalation case type.
			   https://jira.nbnco.net.au/browse/MSEU-862 -- Acceptance Critera 7
Test Class:    CaseCreateLASETask_Test
History
<Date>         <Authors Name>   <Brief Description of Change>
29/06/2017     Nagalingeswar Reddy Vempalli    Created
--------------------------------------------------------------------------*/  
global class CaseCreateLASETask {
/*------------------------------------------------------------
	Author:        Nagalingeswar Reddy Vempalli
    Company:       Wipro
    Description:   Method calling from Process builder 'Create LASE Governance board review task' to create a task when case Escalation status 
				   equal to 'Governance Board Review' and LASE Management Decision is 'Approved' or 'rejected' and Governance Board Decision is 
				   not set for 2 days since when LASE Management Decision was set.
    Inputs:        List of Case Ids
    Outputs:       none
------------------------------------------------------------*/ 
@InvocableMethod(label='Create LASE Task' description='To be called from Process builder to create a LASE task')
  global static void createLaseTask(List<id> caseId) {
      List<Case> casTaskList = new List<Case>();
      List<Task> laseTaskList = new List<Task>();
      Savepoint sp = Database.setSavepoint();
      
      try{
          Id recordTypeIdTask = GlobalCache.getRecordTypeId('Task', 'LASE Task');
          for(Case cs : [select id,LASE_Management_Decision__c,Governance_Board_Decision__c,Escalation_Status__c,OwnerId from Case where id =:caseId]){
            if(cs.Governance_Board_Decision__c == null && cs.LASE_Management_Decision__c != null && cs.Escalation_Status__c == 'Governance Board Review')
                  casTaskList.add(cs);
          }
         for(Case c : casTaskList){
              laseTaskList.add( new Task(	OwnerId         = c.OwnerId,
                                            Due_Date__c     = Date.today(),
                                            Priority        = 'Normal',
                                            RecordTypeId    = recordTypeIdTask,
                                            Status          = 'Open',
                                            Type_Custom__c  = 'Follow up - Frustrated Premises Governance Board Decision',
                                            Subject         = 'Follow up – Frustrated premises with Governance Board',
                                            WhatId          = c.Id,
                                            Outcome__c      = 'Open'
                                        )
                            );
          }
         insert laseTaskList;
      }Catch(Exception e){
          Database.rollback(sp);  
          system.debug('e'+e.getMessage());
          GlobalUtility.logMessage('Error','CaseCreateLASETask','createLaseTask','','',e.getMessage(),'',e,0);
      }
   }
}