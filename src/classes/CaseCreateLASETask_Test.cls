/***************************************************************************************************
Class Name	: CaseCreateLASETask_Test
Class Type	: Test Class 
Version     : 1.0 
Created Date: 31/07/2017
Function    : This class contains unit test scenarios for  CaseCreateLASETask apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Naga       31/07/2017                 Created
****************************************************************************************************/
@IsTest
public class CaseCreateLASETask_Test {
    /***************************************************************************************************
Method Name	:  testCreateTask
Method Type	: testmethod
Version     : 1.0 
Created Date: 31/07/2017
Description	:  Testing LASE task creation.
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Naga      31/07/2017                Created
****************************************************************************************************/
    static testMethod void testCreateTask() {
        Test.startTest();
        List<Id> csList = new List<Id>();
        Case cs = new Case();
        Default_Queue__c  defaultQueue = new Default_Queue__c();
        defaultQueue.name = '00G28000002HUlI';
        defaultQueue.Active__c = true;
        insert defaultQueue;
        String RTId = [SELECT Id FROM RecordType where SobjectType='Case' and Name='LASE Escalation'].Id;
        cs = new Case();
        cs.status = 'Open';
        cs.Priority = '3-General';
        cs.Origin = 'Phone';
        cs.Subject = 'Test subject';
        cs.Description = 'Test Description';
        cs.RecordTypeId = RTId ;
        cs.Escalation_Type__c = 'Objection';
        cs.LASE_Management_Decision__c = 'Approved';
        
        insert cs;
        cs.Escalation_Type__c = 'Propose frustrated premises';
        cs.Escalation_Status__c = 'Governance Board Review';
        update cs;
        
        csList.add(cs.Id);
        CaseCreateLASETask.createLaseTask(csList);
        Test.stopTest();
    }
}