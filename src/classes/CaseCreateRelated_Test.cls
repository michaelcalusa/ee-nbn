@IsTest
public class CaseCreateRelated_Test {
    /***************************************************************************************************
Method Name  :  testCreateRelatedCase
Method Type  : testmethod
Version     : 1.0 
Created Date: 30/11/2017
Description  :  Testing Customer connection
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Dolly      30/11/2017                Created
****************************************************************************************************/
   static testMethod void testCreateRelatedcase() {
    
    List <Case> lstCase = new List<Case>();
       
          //Setup data
                   
        Site__c site = new Site__c(
            Name = 'test site',
            Location_Id__c ='LOC000080307034'
        );
        insert site;
        
        Case caseObj = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            RecordTypeId = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
            Site__c = site.Id,
            Loc_Id__c='LOC000080307034'
        );
        //insert caseObj;
        lstCase.add(caseObj);
        insert  lstCase;
        
        Test.startTest(); 
        
        RelatedCasesController.getCases(caseObj.ID); 
        
        Test.stopTest();
        }
        
        static testMethod void testCreateRelatedcase1() {
         //Setup data
         List <Case> lstCase = new List<Case>();
                   
        Site__c site1 = new Site__c(
            Name = 'test site'
            //Location_Id__c ='LOC000080307034'
        );
        insert site1;
        
        Case caseObj1 = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            RecordTypeId = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
            Site__c = site1.Id,
            Loc_Id__c='LOC000080307034'
        );
        //insert caseObj;
        lstCase.add(caseObj1);
        Upsert  lstCase;
        
        Test.startTest(); 
        
        CaseSiteDetailsComponentController.getSite(caseObj1.ID);
        
        Test.stopTest();
    }
    
    static testMethod void testCreateRelatedcase2() {
         //Setup data
         List <Case> lstCase = new List<Case>();
                   
        Site__c site2 = new Site__c(
            Name = 'test site',
            Location_Id__c ='LOC000080307034'
        );
        insert site2;
        
        Case caseObj2 = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            RecordTypeId = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
            Site__c = site2.Id,
            Loc_Id__c='LOC000080307034'
        );
        //insert caseObj;
        lstCase.add(caseObj2);
        Upsert  lstCase;
        
        Test.startTest();
        Test.setCurrentPage(Page.AssignCase);
        ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(lstCase);
        stdSetController.setSelected(lstCase);
        CasesController ext = new CasesController(stdSetController);
        ext.click();
        ext.close();
        Test.stopTest();
        
        
    }
    
    static testmethod void testMethodOrder(){
        try{
            case ccCase = new case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),Loc_Id__c='LOC000080307033');
            insert ccCase;
            list<Customer_Connections_Order__c> ccOrdrList = new list<Customer_Connections_Order__c>();
            for(integer i = 0; i < 5; i++){
                Customer_Connections_Order__c ccOrdr = new Customer_Connections_Order__c(Order_ID__c='ord-123'+i,Location_ID__c='LOC000080307033');
                ccOrdrList.add(ccOrdr);
            }
            insert ccOrdrList;
            test.startTest();
                orderRelatedListController.returnOrders(ccCase.id);
            test.stopTest();
        }Catch(Exception ex){
            system.debug('---Exception--UserTriggerHandler_Test--enrollCFSUserTest()--' + ex.getMessage());
        }
    }

    static testMethod void testCreateRelatedcase3() {
        //Create test users Setup test data  Create a unique UserName
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        
        Profile p = [SELECT Id FROM Profile WHERE Name='NBN Customer Connections Team Leader'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8',FirstName='Customer Connections',LastName='User', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles',
        UserName=uniqueUserName);
        insert u;
        
        Profile p2 = [SELECT Id FROM Profile WHERE Name='NBN Right Second Time User'];
        User u2 = new User(Alias = 'standt', Email='standarduser2@testorg.com',
        EmailEncodingKey='UTF-8',FirstName='Right Second Time',LastName='User', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p2.Id,
        TimeZoneSidKey='America/Los_Angeles',
        UserName=uniqueUserName + '1');
        insert u2;

        Case caseObj2;
        System.runAs(u) {
            Site__c site2 = new Site__c(
                Name = 'test site',
                Location_Id__c ='LOC000080307454'
            );
            insert site2;
            
            Site__c site3 = new Site__c(
                Name = 'test site',
                Location_Id__c ='LOC000081307454'
            );
            insert site3;
            
            Customer_Connections_Order__c ord = new Customer_Connections_Order__c(
                order_ID__c = 'ORD000080307034',
                Location_Id__c ='LOC000080307454'
            );
            insert ord;
            
            Customer_Connections_Order__c ord2 = new Customer_Connections_Order__c(
                order_ID__c = 'ORD000081307034',
                Location_Id__c ='LOC000081307454'
            );
            insert ord2;
    
            caseObj2 = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            RecordTypeId = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
            Site__c = site2.Id,
            Loc_Id__c='LOC000080307034',
            Order_Id__c = 'ORD000080307034'
            );
            
            insert caseObj2;
          }   
          
          caseObj2.Loc_Id__c = 'LOC000081307454';
          caseObj2.ownerid = u.id;
          caseObj2.order_id__c = 'ORD000081307034';
          update caseObj2;
          
          caseObj2.ownerid = u2.id;
          caseObj2.order_id__c = null;
          update caseObj2;
    }
}