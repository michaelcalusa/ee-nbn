/*------------------------------------------------------------  
Author:        Andrew Zhang
Company:       NBNco
Description:   create task for SME Team and SME User, called from process builder - Creaeg Task for Case Manager. 
               Can not create the task in Process builder directly because of SME Team Id not available in Process builder.
History
<Date>      <Authors Name>     <Brief Description of Change>
------------------------------------------------------------*/

public without sharing Class CaseCreateTaskForSMEUtil{

    @InvocableMethod(label='Create Task for SME Team' description='To be called from Process builder')
    public static void createTaskForSME(List<ID> ids) {

        Customer_Service_Setting__c css = Customer_Service_Setting__c.getOrgDefaults();
        Id recordTypeIdTask = GlobalCache.getRecordTypeId('Task', 'Task');
            
        List<Task> tasks = new List<Task>();
        
        Set<String> teamNames = new Set<String>();
        List<Case> cases = [SELECT Id, CaseNumber, SME_Team__c, SME_Case_Owner_User__c, DP_Resolution_Status__c, DP_Resolution_Reason__c FROM Case WHERE Id IN :ids];
        for (Case c : cases){
            teamNames.add(c.SME_Team__c);
        }
        
        Map<String, Id> teamNameIdMap = new Map<String, Id>();
        for (Customer_Service_Team__c cst : [SELECT Id, Name FROM Customer_Service_Team__c WHERE Name IN :teamNames]){
            teamNameIdMap.put(cst.Name, cst.Id);
        }
        
        for (Case c : cases){
            
            if (c.SME_Case_Owner_User__c != null){
                tasks.add(
                    new Task(
                    OwnerId         = c.SME_Case_Owner_User__c,
                    Due_Date__c     = Date.today(),
                    Priority        = '3-General',
                    RecordTypeId    = recordTypeIdTask,
                    Status          = 'Open',
                    Type_Custom__c  = 'Action',
                    Sub_Type__c     = 'Required',
                    Subject         = 'Review Case ' + c.CaseNumber + ' - DP Resolution Status set to ' + c.DP_Resolution_Status__c + (c.DP_Resolution_Reason__c != null ? ' & ' + c.DP_Resolution_Reason__c  : ''),
                    WhatId          = c.Id,
                    Trigger_Notification__c = true
                    )
                );
            }
            
            if (teamNameIdMap.containsKey(c.SME_Team__c)){
                tasks.add(
                    new Task(
                    OwnerId         = css.Unassigned_User_Id__c,
                    Team__c         = teamNameIdMap.get(c.SME_Team__c),
                    Due_Date__c     = Date.today(),
                    Priority        = '3-General',
                    RecordTypeId    = recordTypeIdTask,
                    Status          = 'Open',
                    Type_Custom__c  = 'Action',
                    Sub_Type__c     = 'Required',
                    Subject         = 'Review Case ' + c.CaseNumber + ' - DP Resolution Status set to ' + c.DP_Resolution_Status__c + (c.DP_Resolution_Reason__c != null ? ' & ' + c.DP_Resolution_Reason__c  : ''),
                    WhatId          = c.Id
                    )
                );            
            }

        }
        insert tasks;
        
        System.debug('====createTaskForSME: ' + tasks);
    }
}