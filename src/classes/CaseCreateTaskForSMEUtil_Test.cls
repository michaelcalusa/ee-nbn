/*------------------------------------------------------------------------
Author:        Andrew Zhang
Company:       NBNco
Description:   Test class for CaseCreateTaskForSMEUtil
History
<Date>      <Authors Name>     <Brief Description of Change>
----------------------------------------------------------------------------*/

@isTest
private class CaseCreateTaskForSMEUtil_Test {
    
   @isTest(SeeAllData=true) 
    static void testCreateTaskForSME(){
        
        //Setup data
        Account account = new Account(
            Name = 'Test Account',
            RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
            Tier__c = '1',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert account;
           
        Contact contact = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId(),
            AccountId = account.Id,
            Email = 'teste@example122.com',
            Preferred_Phone__c = '0288889999'
        );
       
            insert contact;
        
        
        Case caseObj = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            //RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Complex Complaint').getRecordTypeId(),
            RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId(),
            Phase__c = 'Information',
            Category__c = '(I) Communications',
            Sub_Category__c = 'Discovery Centre',
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
            ContactId = contact.Id,
            Primary_Contact_Role__c = 'General Public'
        );
        insert caseObj;

        Profile caseManagerProfile = [SELECT Id FROM Profile WHERE Name = 'NBN Case Manager User' LIMIT 1];        
        User caseManager = TestDataUtility.createTestUser(false, caseManagerProfile.Id);
        insert caseManager;
        
        caseObj.OwnerId = caseManager.Id;
        
        test.startTest();   
        
        caseObj.DP_Resolution_Status__c = 'Activities Complete';
        caseObj.DP_Resolution_Reason__c = 'Resolution Met';
        update caseObj;
        
        //check Task created
        List<Task> tasks = [SELECT Id, Subject FROM Task WHERE WhatId = :caseObj.Id];
        for (Task t : tasks){
            System.debug('==== task: ' + t);
        }
        
        System.assert(tasks.size()>0);
        
        test.stopTest();        
        
    }


}