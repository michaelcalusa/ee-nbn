/*---------------------------------------------------------
 * Author: Alistair Borley
 * Company: CloudSense
 * Description: 
 * This class contains an action handler which is called by a button press on the Case screen
 * It converts a Case to an Opportunity
 * Modifications:
 * Modified Date: Viswanatha
 * Description: Setting a default value for Opportunity Name and removing mandatory check on Case Subject field
 * Modified By/Date: Naga/13-06-2017
 * Description: LASE escalation case type related chages MSEU-850
 * Modified By/Date: Suraj/18/04/2017
 * Description: NEST-5557 Functionality to copy the secondary contact to the Recoverable/Commercial Works Opportunity
 ---------------------------------------------------------*/

 public with sharing class CaseOpportunityIntegration {
    Case myCase;
    Id oppId;
    public Boolean caseConverted{get; set;}
    
    public CaseOpportunityIntegration(ApexPages.StandardController c) {
        myCase = (Case) c.getRecord();
        caseConverted = false;
        oppId = null;
    }

    public PageReference generateOpportunity() {


        System.debug('Generate Opportunity for Case :' + myCase.Id);

		//NEST-5557. Added Secondary_Contact__c in the query
        Case LocalCase= [select Secondary_Contact__c, ContactId, Id, CaseNumber, Description, AccountId,Opportunity__c,Site__c,Site__r.Name,MDU_CP_Site__c,MDU_CP_Site__r.Name,recordTypeId,Escalation_Type__c,SME_Team__c,Case_Record_Type_Name__c from Case where id = :myCase.Id];

        System.debug('Case Detail. ID: ' + LocalCase.Id + ' CaseNumber: ' + LocalCase.CaseNumber + ' AccountId: ' + LocalCase.AccountId);
        if(LocalCase.Case_Record_Type_Name__c == 'LASE_Escalation' && (LocalCase.Escalation_Type__c != 'Propose Unfrustrated premises' || LocalCase.SME_Team__c != 'Relocation Works Team')){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Please contact the LASE Team for further assistance at LASE_Ops_National@nbnco.com.au ');
            ApexPages.addMessage(myMsg);
            return null; 
        }
        
        //Check if case is already Converted/associated with an Opportunity
        if(LocalCase.Opportunity__c != null)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Case is already converted to an Opportunity');
            ApexPages.addMessage(myMsg); 
            oppId = LocalCase.Opportunity__c;
            caseConverted = true;
            return null; 
        }
        
        // Check that we have a relevant information to create the Opportunity

        // The Subject is used to set the name of the Opportunity
        //if (String.isBlank(LocalCase.Subject)) {
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please check the Case Subject, it can not be Blank or empty. Cannot convert Case to Opportunity');
            //ApexPages.addMessage(myMsg); 
            //return null;
            // Return the user to the Case 
            //return new PageReference('/'+myCase.Id);
        //}

        // Manufacture the Opportunity

        Opportunity myOpportunity;
        Date d = Date.today();
        String strDate = DateTime.newInstance(d.year(),d.month(),d.day()).format('dd/MM/YYYY');
        String oppName = 'RW - Opportunity ' + strDate;

        if(LocalCase.Site__c != null && (LocalCase.Site__r.Name != null || LocalCase.Site__r.name !=''))
        {
            oppName = LocalCase.Site__r.Name;
        }
		/*if(!String.isEmpty(LocalCase.MDU_CP_Site__c) && !String.isEmpty(LocalCase.MDU_CP_Site__r.Name))
        {
           oppName = LocalCase.MDU_CP_Site__r.Name;
        }*/
        /*myOpportunity = new Opportunity (
                Name = oppName,
                // RecordType = 'Recoverable Works Simple',
                AccountId = LocalCase.AccountId,
                StageName = 'New',
                CloseDate = date.today(),
                Source__c = 'Contact Centre',
                Site__c = LocalCase.Site__c,
                Description = LocalCase.Description
            );*/
        String escRecID = Schema.SObjectType.case.getRecordTypeInfosByName().get('LASE Escalation').getRecordTypeId();
		myOpportunity = new Opportunity();
		myOpportunity.Name = oppName;
        myOpportunity.AccountId = LocalCase.AccountId;
        myOpportunity.StageName = 'New';
        myOpportunity.CloseDate = date.today();
        if(escRecID.equals(LocalCase.recordTypeId)){
           myOpportunity.Source__c = 'LASE';
           myOpportunity.Site__c = LocalCase.Site__c;     
           myOpportunity.LASE_Escalation_Case_Reference__c = 'LASE_Escalation';
        }else{
           myOpportunity.Source__c = 'Contact Centre';
           myOpportunity.Site__c = LocalCase.Site__c; 
        }
        myOpportunity.Description = LocalCase.Description;
        insert myOpportunity;
        
        LocalCase.Opportunity__c = myOpportunity.Id;
        if(!escRecID.equals(LocalCase.recordTypeId)){
         LocalCase.Status = 'Closed';
        }
        LocalCase.Converted_To_Opportunity__c = True;
        if(LocalCase.ContactId != null)
        {
            OpportunityContactRole oppContactRole = new OpportunityContactRole();
            oppContactRole.ContactId = LocalCase.ContactId;
            oppContactRole.OpportunityId = myOpportunity.Id;
			
			//NEST-5557. Functionality to copy the secondary contact to the Opportunity
			List<OpportunityContactRole> lstOptyConRoles =  new List<OpportunityContactRole>();
			
			if(LocalCase.Case_Record_Type_Name__c == 'Complex_Query' || LocalCase.Case_Record_Type_Name__c == 'Complex_Complaint' || LocalCase.Case_Record_Type_Name__c == 'Formal_Complaint' ||LocalCase.Case_Record_Type_Name__c == 'Urgent_Complaint'){
				oppContactRole.Role = 'Billing Contact';
			}else{
				oppContactRole.Role = 'Requestor';
			}        	
			//Insert oppContactRole;
           
           	lstOptyConRoles.add(oppContactRole);

			if(LocalCase.Secondary_Contact__c != null && (LocalCase.Case_Record_Type_Name__c == 'Complex_Query' || LocalCase.Case_Record_Type_Name__c == 'Complex_Complaint' || LocalCase.Case_Record_Type_Name__c == 'Formal_Complaint' ||LocalCase.Case_Record_Type_Name__c == 'Urgent_Complaint')){      	
			    OpportunityContactRole oppContactRole2 = new OpportunityContactRole();
			    oppContactRole2.ContactId = LocalCase.Secondary_Contact__c;
			    oppContactRole2.OpportunityId = myOpportunity.Id;
			    oppContactRole2.Role = 'Requestor';
			  	lstOptyConRoles.add(oppContactRole2);
			}
          	insert lstOptyConRoles;
           	// End NEST-5557
        }

        try{
             Update LocalCase;
        }
        catch(Exception e){
            String exceptionMessage = e.getDmlMessage(0);
            exceptionMessage = exceptionMessage.replaceAll('\'','');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,exceptionMessage));
            return null; 
        }

        // Drop the User into the newly created Opportunity
        return new PageReference('/'+myOpportunity.Id);
    }

    public PageReference goBacktoCase() {
        // Return the user to the Case 
        return new PageReference('/'+myCase.Id);
    }
    
    public PageReference goToOpportunity() {
        return new PageReference('/'+oppId);
    }
    

  public class CaseOpportunityIntegrationException extends Exception{
    //intentionally left blank
  }

}