@istest (SeeAllData=true)
public class CaseOpportunityIntegration_Test
{
    /*------------------------------------------------------------------------
Author:        Viswanatha
Company:       CloudSense
Description:   Test class for CaseOpportunityIntegration Class
1 - Test Case To Opportunity Conversion
2 - Test Case already Converted Scenario
3 - Test if Case is mapped to an Account

Class:           CaseOpportunityIntegration_Test
History
Modified: 19/04/2018     Suraj Malla 
Description: NEST-5557 Test Cases to copy the secondary contact from Case to the
Recoverable/Commercial Works Opportunity
--------------------------------------------------------------------------*/  
    Private Static testmethod void TestCaseToOppConvert()
    {
        
        //Prepare Test Data
        Case objCase = TestDataClass.CreateTestCaseData();
        Account objAccount = TestDataClass.CreateTestAccountData();
        Contact objContact = TestDataClass.CreateTestContactData();
        objCase.AccountId = objAccount.Id;
        objCase.ContactId = objContact.Id;
        objCase.OwnerId = UserInfo.getUserId();
        update objCase;
        
        
        //Testing Case Opportunity Conversion
        ApexPages.currentPage().getParameters().put('id',objCase.id);
        ApexPages.StandardController stdCase = new ApexPages.StandardController(objCase);
        CaseOpportunityIntegration controllerCaseToOpp  = new CaseOpportunityIntegration(stdCase);
        
        String nextPage = controllerCaseToOpp.generateOpportunity().getUrl();
        
        objCase = [Select Origin,Status,Sub_Status__c,Subject,AccountID,ContactId,Opportunity__c from Case 
                   where Id=:objCase.Id];
        System.assertEquals('/'+objCase.Opportunity__c, nextPage);
        
        //Testing Case which is already converted
        stdCase = new ApexPages.StandardController(objCase);
        controllerCaseToOpp  = new CaseOpportunityIntegration(stdCase);
        controllerCaseToOpp.generateOpportunity();
        
        nextPage = controllerCaseToOpp.goBacktoCase().getUrl();
        System.assertEquals('/'+objCase.Id, nextPage);
        
        nextPage = controllerCaseToOpp.goToOpportunity().getUrl();
        System.assertEquals('/'+objCase.Opportunity__c, nextPage);
        
        
        //Testing Blank Subject
        //objCase.Subject = null;
        //objCase.Opportunity__c = null;
        //update objCase;
        
        //stdCase = new ApexPages.StandardController(objCase);
        //controllerCaseToOpp  = new CaseOpportunityIntegration(stdCase);
        //controllerCaseToOpp.generateOpportunity();
        
        //NEST-5557 Testing Secondary Contact Role creation on Case Opportunity conversion
               
        //Account 
        Account ac = new Account(name ='Commercial Works Household', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId());
        insert ac;
        
        //Create Primary and Secondary Contact 
        List<Contact> lstContacts = new List<Contact>();       
        Contact con = new Contact(LastName ='testCon', AccountId = ac.Id, Email = 'test@test.com', Preferred_Phone__c = '0400000000', RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId());
        lstContacts.add(con);
        
        Contact con2 = new Contact(LastName ='testCon2',AccountId = ac.Id, Email = 'test2@test.com', Preferred_Phone__c = '0400000001', RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId());
        lstContacts.add(con2);
        insert lstContacts;
                
        Test.startTest();
            Case objCase2 = new Case(RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Complex Complaint').getRecordTypeId(), AccountId = ac.Id, ContactId = con.Id, Phase__c = 'Information',
                                    Category__c = '(I) Communications', Sub_Category__c = 'Discovery Centre', Primary_Contact_Role__c = 'End User Residential', Authorisation_for_Contact__c = 'Primary First', Secondary_Contact__c =  con2.Id,
                                    Secondary_Contact_Role__c = 'Contractor', Subject = 'test',Description = 'testing',Origin = 'Email',HSE_Impact__c = 'None');
            insert objCase2; 

            ApexPages.currentPage().getParameters().put('id',objCase2.id);
            ApexPages.StandardController stdCase2 = new ApexPages.StandardController(objCase2);
            CaseOpportunityIntegration controllerCaseToOpp2 = new CaseOpportunityIntegration(stdCase2);
            controllerCaseToOpp2.generateOpportunity();
        Test.stopTest();
    }    
}