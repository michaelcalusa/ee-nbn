global class CaseSLAClockController{
    private final Case c;
    
    public CaseSLAClockController(ApexPages.StandardController stdController) {
        this.c= (Case)stdController.getRecord();
    }

    global class SLAInfo{
        public Decimal sla_days {get;set;}
        public Decimal remaining_days {get;set;}
        public Decimal eclipsed_days {get;set;} 
        public Decimal remaining_ms {get;set;}
        public Decimal eclipsed_ms {get;set;} 
        
        public Datetime sla_date  {get;set;}
        public Datetime closed_date  {get;set;}
        public String sla_status {get;set;}
        public String case_status {get;set;}

        
        SLAInfo(Case c){
            this.sla_days = c.SLA_Days__c;
            this.closed_date = c.ClosedDate;
            this.sla_date = c.SLA_Date__c;
            this.sla_status = c.SLA_Status__c;
            this.case_status = c.Status;
        }
    }
    
    @RemoteAction
    global static SLAInfo getRemainingDays(String caseId){
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault=true];                  
        /**
        MSEU 901 Commercial Billing Cases Will use the VLC State Specific Business Hours and Holidays and adding BusinessHoursID and RecordType to query
        **/
        Case c = [SELECT Id, SLA_Days__c, SLA_Date__c, SLA_Status__c, Status, ClosedDate, BusinessHoursID, RecordTypeID FROM Case WHERE Id = :caseId];
        SLAInfo sla = new SLAInfo(c);
                
        sla.remaining_ms = (Decimal)CaseUtil.getRemainingMs(bh, c);
        Decimal rd = sla.remaining_ms * 1.0 / (1000 * 60 * 60 * 8);
        sla.remaining_days = rd.setScale(2, RoundingMode.HALF_UP);  
        
        if (sla.remaining_ms >0){
            sla.eclipsed_ms = sla.sla_days * 1000 * 60 * 60 * 8 - sla.remaining_ms;
            sla.eclipsed_days = sla .sla_days - sla.remaining_days;
        }
        return sla;
    } 

}