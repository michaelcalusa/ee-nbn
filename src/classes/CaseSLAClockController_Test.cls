/*------------------------------------------------------------------------
Author:        Andrew Zhang
Company:       NBNco
Description:   Test class for CaseSLAClockController
History
<Date>      <Authors Name>     <Brief Description of Change>
----------------------------------------------------------------------------*/

@isTest
private class CaseSLAClockController_Test {
    
    @isTest(SeeAllData=true) 
    static void testSLAClock(){
        
        //Setup data
        Account account = new Account(
            Name = 'Test Account',
            RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
            Tier__c = '1',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert account;
        
        Contact contact = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId(),
            AccountId = account.Id,
            Email = 'teste@example321.com',
            Preferred_Phone__c = '0288889999'
        );
        insert contact;
        
        Case caseObj = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId(),
            Phase__c = 'Information',
            Category__c = '(I) Communications',
            Sub_Category__c = 'Discovery Centre',
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
            ContactId = contact.Id,
            Primary_Contact_Role__c = 'General Public'
        );
        insert caseObj;
                
        CaseSLAClockController controller = new CaseSLAClockController(new ApexPages.standardController(caseObj));
             
        System.debug('===SLA Info: ' + CaseSLAClockController.getRemainingDays(caseObj.Id));
    
    }
}