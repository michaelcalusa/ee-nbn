/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class will be used to transform and transfer Case Site records from Sites_Int to Site and also to create the Site Contact records
Modifications:
*/
public class CaseSite implements Database.Batchable<Sobject>, Database.Stateful
{
    public String siteQuery;
    public class GeneralException extends Exception
    {
        
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
            siteQuery = 'Select CRM_Case_Id__c,CRM_Contact_Id__c,Unique_Site__c,Address_2__c,Address_3__c,City__c,Latitude__c,Longitude__c,Premises_Type__c,Technology_Type__c,Rollout_Type__c,Location_ID__c,Number_Street__c,Post_Code__c,State__c,Combined_Address__c FROM Site_Int__c where Transformation_Status__c = \'Extracted from CRM for Initial Load\' order by createddate asc';
            return Database.getQueryLocator(siteQuery);   
    }
    public void execute(Database.BatchableContext BC,List<Site_Int__c> siteIntList)
    {
        try
        {
            String vsRecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Site__c'  and name = 'Verified'].Id;
            String uvsRecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Site__c'  and name = 'Unverified'].Id;
            
            list <Site__c> listOfVerifiedSite = new list<Site__c>();
            list <Site__c> listOfUnVerifiedSite = new list<Site__c>();
            map<string,Site__c> vSites = new map<string,Site__c>();
            map<string,Site__c> uvSites = new map<string,Site__c>();
            map<String,Case> caseMap = new map<String,Case>();
            
            list<Site_Int__c> usucSite = new list<Site_Int__c>();
            list<Site_Int__c> sucSite = new list<Site_Int__c>();
            list<Site_Contacts__c> siteCon = new list<Site_Contacts__c>();
            
            list <string> intvSiteId = new list<string>();
            list <string> intuvSiteId = new list<string>();
            
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            
            list<Case> caseList = new List<Case>();
            
            Boolean incomAdd = false;
            String address, lat, lng, pType;
            
            List<String> listOfCaseIds = new List<String>(); 
            Map<String, Id> mapListOfCase = new Map<String, Id>();
            List<String> listOfConIds = new List<String>();              
            Map<String, Id> mapListOfCon = new Map<String, Id>();            
      
            for(Site_Int__C sInt : siteIntList)
            {                                
                listOfCaseIds.add(sInt.CRM_Case_Id__c);
            } 
            for(Case c : [select On_Demand_Id__c, Id from Case where On_Demand_Id__c in :listOfCaseIds])
            {
                mapListOfCase.put(c.On_Demand_Id__c, c.id);            
            }

                                   
            for(Site_Int__c si:siteIntList)
            {          
                Database.DMLOptions dml = new Database.DMLOptions();
              dml.allowFieldTruncation = true;
                incomAdd = false;
                if(!String.isBlank(si.Location_ID__c))
                {
                  Site__c verifiedSite = new Site__c();    
                    verifiedSite.RecordTypeId=vsRecordTypeId;
                    verifiedSite.Location_Id__c = si.Location_ID__c;
                    verifiedSite.Site_Address__c = si.Combined_Address__c;                               
                    verifiedSite.Locality_Name__c = si.City__c;
                    if((String.isBlank(si.Number_Street__c) && String.isBlank(si.Address_2__c)) || String.isBlank(si.City__c) || String.isBlank(si.State__c) || String.isBlank(si.Post_Code__c))  
                    {
            incomAdd = true;                        
                    }
                    if (!String.isBlank(si.State__c))
                    {
                        if (si.State__c.toUpperCase() == 'ACT' ||si.State__c.toUpperCase() == 'NSW' || si.State__c.toUpperCase() == 'NT' || si.State__c.toUpperCase() == 'QLD' || 
                        si.State__c.toUpperCase() == 'TAS' || si.State__c.toUpperCase() == 'SA' || si.State__c.toUpperCase() == 'VIC' || si.State__c.toUpperCase() == 'WA')
                        {
                            verifiedSite.State_Territory_Code__c=si.State__c.toUpperCase();
                        }    
                        else
                        {
                            incomAdd = true;
                        }
                    }
                    if(String.isNotBlank(si.post_Code__c))
                    {
                        if (ValidatePostCode.validate(si.Post_Code__c))
                        {
                            verifiedSite.Post_Code__c=si.Post_Code__c;
                        }
                        else
                        {
                            incomAdd = true;
                        }
                    }
                    verifiedSite.Road_Number_1__c=si.Number_Street__c;
                    verifiedSite.Road_Number_2__c=si.Address_2__c;                                        
                    verifiedSite.MDU_CP_On_Demand_Id__c = si.Location_ID__c;
                    verifiedSite.Name= si.Combined_Address__c;                    
                    verifiedSite.Latitude__c = si.Latitude__c;
                    verifiedSite.Longitude__c = si.Longitude__c;                    
                    verifiedSite.Premises_Type__c = si.Premises_Type__c;  
                    verifiedSite.Technology_Type__c = si.Technology_Type__c;
                    verifiedSite.Rollout_Type__c = si.Rollout_Type__c;
                    verifiedSite.CRMOD_Case_Site__c = true;
                    verifiedSite.isSiteCreationThroughCode__c =true;    
                    verifiedSite.setOptions(dml);
                    if (!incomAdd)
                    {
                        vSites.put(si.Location_ID__c,verifiedSite);
                    }
                    else
                    {
                        si.Transformation_Status__c = 'Incomplete Address';                        
                        usucSite.add(si);
                        address = String.isNotBlank(si.Combined_Address__c) ?'Address: '+ si.Combined_Address__c : '';                        
                        lat = String.isNotBlank(si.Latitude__c) ?' Latitude: ' + si.Latitude__c : '' ;
                        lng = String.isNotBlank(si.Longitude__c) ?' Longitude: ' + si.Longitude__c : '';                        
                        pType = String.isNotBlank(si.Premises_Type__c) ?' Premise Type: ' + si.Premises_Type__c : '';
                        caseMap.put(mapListOfCase.get(si.CRM_Case_Id__c),new Case(Id = mapListOfCase.get(si.CRM_Case_Id__c),Site_Address__c = 'Location Id:'+si.Location_ID__c+ address +lat+lng+pType));
                        errList.add(new Error_Logging__c(Name='CaseSite_Tranformation',Error_Message__c= 'Incomplete Address',CRM_Record_Id__c =si.Id));
                    }                    
                }
                else
                {
          Site__c unVerifiedSite = new Site__c();        
                    unVerifiedSite.RecordTypeId=uvsRecordTypeId;
                    unVerifiedSite.Site_Address__c = si.Combined_Address__c;
                    unVerifiedSite.Locality_Name__c = si.City__c;
                    if((String.isBlank(si.Number_Street__c) && String.isBlank(si.Address_2__c)) || String.isBlank(si.City__c) || String.isBlank(si.State__c) || String.isBlank(si.Post_Code__c))  
                    {
            incomAdd = true;                        
                    }
                    if (!String.isBlank(si.State__c))
                    {
                        if (si.State__c.toUpperCase() == 'ACT' ||si.State__c.toUpperCase() == 'NSW' || si.State__c.toUpperCase() == 'NT' || si.State__c.toUpperCase() == 'QLD' || 
                        si.State__c.toUpperCase() == 'TAS' || si.State__c.toUpperCase() == 'SA' || si.State__c.toUpperCase() == 'VIC' || si.State__c.toUpperCase() == 'WA')
                        {
                            unVerifiedSite.State_Territory_Code__c=si.State__c.toUpperCase();
                        }    
                        else
                        {
                            incomAdd = true;
                        }
                    }
                    if(String.isNotBlank(si.post_Code__c))
                    {
                        if (ValidatePostCode.validate(si.Post_Code__c))
                        {
                            unVerifiedSite.Post_Code__c=si.Post_Code__c;
                        }
                        else
                        {
                            incomAdd = true;
                        }
                    }
                    unVerifiedSite.Road_Number_1__c=si.Number_Street__c;
                    unVerifiedSite.Road_Number_2__c=si.Address_2__c;                                        
                    unVerifiedSite.Site_Address_Unique__c=si.Combined_Address__c.toUpperCase();
                    unVerifiedSite.Name= si.Combined_Address__c;                    
                    unVerifiedSite.Latitude__c = si.Latitude__c;
                    unVerifiedSite.Longitude__c = si.Longitude__c;                    
                    unVerifiedSite.Premises_Type__c = si.Premises_Type__c;    
                    unVerifiedSite.Technology_Type__c = si.Technology_Type__c;
                    unVerifiedSite.Rollout_Type__c = si.Rollout_Type__c;
                    unVerifiedSite.CRMOD_Case_Site__c = true;
                    unVerifiedSite.isSiteCreationThroughCode__c =true;   
                    unVerifiedSite.setOptions(dml);
                    if (!incomAdd)
                    {
                        uvSites.put(si.Combined_Address__c.toUpperCase(),unVerifiedSite);
                        //System.debug('CRM Case Id '+si.CRM_Case_Id__c+ 'Address is '+ si.Combined_Address__c.toUpperCase());
                    }
                    else
                    {
                        si.Transformation_Status__c = 'Incomplete Address';                        
                        usucSite.add(si);
                        address = String.isNotBlank(si.Combined_Address__c) ?'Address: '+ si.Combined_Address__c : '';
                        lat = String.isNotBlank(si.Latitude__c) ?' Latitude: ' + si.Latitude__c : '' ;
                        lng = String.isNotBlank(si.Longitude__c) ?' Longitude: ' + si.Longitude__c : '';                        
                        pType = String.isNotBlank(si.Premises_Type__c) ?' Premise Type: ' + si.Premises_Type__c : '';
                        caseMap.put(mapListOfCase.get(si.CRM_Case_Id__c),new Case(Id = mapListOfCase.get(si.CRM_Case_Id__c),Site_Address__c = address +lat+lng+pType));
                        errList.add(new Error_Logging__c(Name='CaseSite_Tranformation',Error_Message__c= 'Incomplete Address',CRM_Record_Id__c =si.Id));
                    }                        
                }  
            } 
            
            listOfVerifiedSite = vSites.values();
            List<Database.upsertResult> upsertVerifiedSiteResults = Database.upsert(listOfVerifiedSite,Site__c.Fields.MDU_CP_On_Demand_Id__c,false);
                        
            listOfUnVerifiedSite = uvSites.values();
            List<Database.upsertResult> upsertUnVerifiedSiteResults = Database.upsert(listOfUnVerifiedSite,Site__c.Fields.Site_Address_Unique__c,false);
                 
            list<Database.Error> err;
            String msg, fAffected;
            String[]fAff;
            StatusCode sCode;
            for(Integer idx = 0; idx < upsertVerifiedSiteResults.size(); idx++)
            {   
                if(!upsertVerifiedSiteResults[idx].isSuccess())
                {
                    err=upsertVerifiedSiteResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='CaseSite_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=upsertVerifiedSiteResults[idx].isCreated(),
                    isSuccess__c=upsertVerifiedSiteResults[idx].isSuccess(),Record_Row_Id__c=upsertVerifiedSiteResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c = listOfVerifiedSite[idx].Location_Id__c));
                    intuvSiteId.add(listOfVerifiedSite[idx].Site_Address_Unique__c);
                }
                else
                {           
                    for(Site_Int__c siteInt: siteIntList)
                    {
                        if(!String.isBlank(siteInt.Location_Id__c))
                        {
                            if(listOfVerifiedSite[idx].Location_Id__c == siteInt.Location_Id__c)
                            {
                                //siteCon.add(new Site_Contacts__c(Related_Contact__c=mapListOfCon.get(siteInt.CRM_Contact_Id__c),Related_Site__c=upsertVerifiedSiteResults[idx].getId(),Site_Contact_Unique__c=mapListOfCon.get(siteInt.CRM_Contact_Id__c)+' '+upsertVerifiedSiteResults[idx].getId()));
                                caseMap.put(mapListOfCase.get(siteInt.CRM_Case_Id__c),new Case(Id = mapListOfCase.get(siteInt.CRM_Case_Id__c),  Site__c=upsertVerifiedSiteResults[idx].getId()));
                                intvSiteId.add(listOfVerifiedSite[idx].Location_Id__c);
                            }
                        }
                    }                   
                }
            } 
            for(Integer idx = 0; idx < upsertUnVerifiedSiteResults.size(); idx++)
            {   
                if(!upsertUnVerifiedSiteResults[idx].isSuccess())
                {
                    err=upsertUnVerifiedSiteResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='CaseSite_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=upsertUnVerifiedSiteResults[idx].isCreated(),
                    isSuccess__c=upsertUnVerifiedSiteResults[idx].isSuccess(),Record_Row_Id__c=upsertUnVerifiedSiteResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c = listOfUnVerifiedSite[idx].Site_Address_Unique__c));
                    intuvSiteId.add(listOfUnVerifiedSite[idx].Site_Address_Unique__c);
                }
                else
                {           
                    for(Site_Int__c siteInt: siteIntList)
                    {
                        if(!String.isBlank(siteInt.Combined_Address__c))
                        {
                            if(listOfUnVerifiedSite[idx].Site_Address_Unique__c == siteInt.Unique_Site__c)
                            {
                                //siteCon.add(new Site_Contacts__c(Related_Contact__c=mapListOfCon.get(siteInt.CRM_Contact_Id__c),Related_Site__c=upsertUnVerifiedSiteResults[idx].getId(),Site_Contact_Unique__c=mapListOfCon.get(siteInt.CRM_Contact_Id__c)+' '+upsertUnVerifiedSiteResults[idx].getId()));
                                caseMap.put(mapListOfCase.get(siteInt.CRM_Case_Id__c),new Case(Id = mapListOfCase.get(siteInt.CRM_Case_Id__c),  Site__c=upsertUnVerifiedSiteResults[idx].getId()));
                                intvSiteId.add(listOfUnVerifiedSite[idx].Site_Address_Unique__c);
                            }
                        }
                    }                   
                }
            } 
            
            caseList = caseMap.values();
            List<Database.upsertResult> uCResults = Database.upsert(caseList,false);
            
            for(Integer idx = 0; idx < uCResults.size(); idx++)
            {   
                if(!uCResults[idx].isSuccess())
                {
                    err=uCResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='Case_Site_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uCResults[idx].isCreated(),
                    isSuccess__c=uCResults[idx].isSuccess(),Record_Row_Id__c=uCResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=caseList[idx].Id));
                }
                else
                {
                }
            }
            

            
            
            for (Site_Int__c cSite: [select id,CRM_Case_Id__c,CRM_Contact_Id__c,Transformation_Status__c from Site_Int__c where Transformation_Status__c ='Extracted from CRM for Initial Load' and Unique_Site__c in : intvSiteId])
            {
                if(!String.isBlank(cSite.CRM_Contact_Id__c) && cSite.CRM_Contact_Id__c != 'No Match Row Id')
                {
                  cSite.Transformation_Status__c = 'Ready for Site Contact Association';                                
                }
                else
                {
                    cSite.Transformation_Status__c = 'Copied to Base Table'; 
                }
                
                sucSite.add(cSite);
            }
            for (Site_Int__c ucSite: [select id,Transformation_Status__c from Site_Int__c where Transformation_Status__c ='Extracted from CRM for Initial Load' and Unique_Site__c in : intuvSiteId])
            {
                ucSite.Transformation_Status__c = 'Error Site Creation';                
                usucSite.add(ucSite);
            }
            update sucSite;
            update usucSite;
            Insert errList;            
                       
        }
        catch (Exception e)
        {            
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='Case_Site_Tranformation',Error_Message__c= e.getMessage()+' '+e.getLineNumber()));
            Insert errList;
        }
    }
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        Database.executeBatch(new  CaseSiteContactAssociation());
    }         
}