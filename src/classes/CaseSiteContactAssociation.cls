public class CaseSiteContactAssociation implements Database.Batchable<Sobject>, Database.Stateful
{
    public String siteQuery;
    public class GeneralException extends Exception
    {
        
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
            siteQuery = 'Select CRM_Case_Id__c,CRM_Contact_Id__c,Contact_Role__c,Unique_Site__c,Address_2__c,Address_3__c,City__c,Latitude__c,Longitude__c,Premises_Type__c,Location_ID__c,Number_Street__c,Post_Code__c,State__c,Combined_Address__c FROM Site_Int__c where Transformation_Status__c = \'Ready for Site Contact Association\' order by createddate asc';
            return Database.getQueryLocator(siteQuery);   
    }
    public void execute(Database.BatchableContext BC,List<Site_Int__c> siteIntList)
    {
        try
        {
            List<String> crmId = new List<String>();
            List<String> listOfConIds = new List<String>(); 
            List<String> listOfVerSiteIds = new List<String>();
            List<String> listOfUnVerSiteIds = new List<String>();
            Map<String, Id> mapListOfCon = new Map<String, Id>();
            Map<String, Id> mapListOfVerSite = new Map<String, Id>();
            Map<String, Id> mapListOfUnVerSite = new Map<String, Id>();
            map <String, Site_Contacts__c> siteContactMap = new map<String, Site_Contacts__c>();
            list<Site_Contacts__c> siteCon = new list<Site_Contacts__c>();
            
            for(Site_Int__C si : siteIntList)
            { 
                if(!String.isBlank(si.CRM_Contact_Id__c) && si.CRM_Contact_Id__c != 'No Match Row Id')
                {                	 
                    listOfConIds.add(si.CRM_Contact_Id__c);
                }    
                if(!String.isBlank(si.Location_ID__c))
                {
                    listOfVerSiteIds.add(si.Location_ID__c);
                }
                else
                {
                    listOfUnVerSiteIds.add(si.Combined_Address__c.toUpperCase());
                }
            }
            for(Contact con : [select On_Demand_Id_P2P__c, Id from Contact where On_Demand_Id_P2P__c in :listOfConIds])
            {
                mapListOfCon.put(con.On_Demand_Id_P2P__c, con.id);            
            }
            for(Site__c vSite : [select MDU_CP_On_Demand_Id__c, Id from Site__c where MDU_CP_On_Demand_Id__c in :listOfVerSiteIds])
            {
                mapListOfVerSite.put(vSite.MDU_CP_On_Demand_Id__c, vSite.id);            
            }
            for(Site__c uvSite : [select Site_Address_Unique__c, Id from Site__c where Site_Address_Unique__c in :listOfUnVerSiteIds])
            {
                mapListOfUnVerSite.put(uvSite.Site_Address_Unique__c, uvSite.id);            
            }
            
            for(Site_Int__c si:siteIntList)
            {
                Site_Contacts__c sc = new Site_Contacts__c();
                crmId.add(si.CRM_Case_Id__c); 
                if(!String.isBlank(si.Location_ID__c))
                {
                    sc.Related_Site__c = mapListOfVerSite.get(si.Location_ID__c);                    
                }
                else
                {
                    sc.Related_Site__c = mapListOfUnVerSite.get(si.Combined_Address__c.toUpperCase());   
                }
                if(!String.isBlank(si.CRM_Contact_Id__c))
                {
                	sc.Related_Contact__c=mapListOfCon.get(si.CRM_Contact_Id__c);    
                }                
                sc.Site_Contact_Unique__c=sc.Related_Contact__c+' '+sc.Related_Site__c;
                sc.Role__c = si.Contact_Role__c;
                siteContactMap.put(sc.Site_Contact_Unique__c, sc);
            }     
            siteCon = siteContactMap.values();
            List<Database.UpsertResult> upsertSiteConResults = Database.Upsert(siteCon,Site_Contacts__c.Fields.Site_Contact_Unique__c,false);
            List<Database.Error> upsertSiteConError;           
            String msg, fAffected;
            String[]fAff;
            StatusCode sCode;
            list<Error_Logging__c> errSiteConList = new List<Error_Logging__c>();                              
            for(Integer idx = 0; idx < upsertSiteConResults.size(); idx++)
            {                  
                if(!upsertSiteConResults[idx].isSuccess())
                {
                    upsertSiteConError=upsertSiteConResults[idx].getErrors();                  
                    for (Database.Error er: upsertSiteConError)
                    {
                        sCode = er.getStatusCode();
                        msg = er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errSiteConList.add(new Error_Logging__c(Name='CaseSite_Contact_Association',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=upsertSiteConResults[idx].isCreated(),isSuccess__c=upsertSiteConResults[idx].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=siteCon[idx].Site_Contact_Unique__c));
                    siteIntList[idx].Transformation_Status__c = 'Error Site Creation';                                                           
                }
                else
                {
                    System.debug('upsertSiteConResults[idx].isSuccess()' + upsertSiteConResults[idx].isSuccess());
                    System.debug('upsertSiteConResults[idx].isCreated()' + upsertSiteConResults[idx].isCreated());
                    System.debug('upsertSiteConResults[idx].isCreated()' + upsertSiteConResults[idx].id);
                    siteIntList[idx].Transformation_Status__c = 'Copied to Base Table'; 
                }
            }
            Insert errSiteConList;
            Update siteIntList;  
        }        
        catch (Exception e)
        {            
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='CaseSite_Contact_Association',Error_Message__c= e.getMessage()+' '+e.getLineNumber()));
            Insert errList;
        }
    }
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
    }     
}