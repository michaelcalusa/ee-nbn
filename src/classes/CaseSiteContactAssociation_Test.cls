/***************************************************************************************************
Class Name:  CaseSiteContactAssociation_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 02-11-2016
Function    : This class contains unit test scenarios for CaseSiteContactAssociation apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Hari Kalannagari         02-11-2016                Created
****************************************************************************************************/
@isTest
private class CaseSiteContactAssociation_Test {   
    static testMethod void cssttest(){
        Database.QueryLocator QueryLoc;
        Database.BatchableContext BatchCont;
        date CRM_Modified_Date = date.newinstance(1963, 01,24);
        
        //Create Site Int record and assign to site int list
        List<Site_Int__c> ListOfsiteInt = new List<Site_Int__c> ();
        Site_Int__c stint = new Site_Int__c();
        stint.CRM_Case_Id__c='TestCaseId';
        stint.CRM_Contact_Id__c='TestContactId';
        stint.Address_2__c='Test Sydney1';
        stint.Address_3__c='Test Sydney2';
        stint.City__c='Sydney';
        stint.Latitude__c='TestLatitude';
        stint.Longitude__c='TestLonitude';
        stint.Premises_Type__c='TestPremisesType';
        stint.Location_ID__c='NorthWest';
        stint.Number_Street__c='locteststreet';
        stint.Post_Code__c='562012';
        stint.State__c='SydneyState';
        stint.Transformation_Status__c='Extracted from CRM for Initial Load';
        insert stint;
        ListOfsiteInt.add(stint);
        
        // Create Site record
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        Site__c UnverifiedSite = new Site__c();
        UnverifiedSite.RecordTypeId=recordTypeId;
        UnverifiedSite.Site_Address__c=stint.Combined_Address__c;
        UnverifiedSite.Locality_Name__c=stint.City__c;
        UnverifiedSite.Road_Number_1__c=stint.Number_Street__c;
        UnverifiedSite.Latitude__c = stint.Latitude__c;
        UnverifiedSite.Longitude__c = stint.Longitude__c;  
        //UnverifiedSite.MDU_CP_On_Demand_Id__c = 'NorthWest';
        UnverifiedSite.isSiteCreationThroughCode__c =true;
        insert UnverifiedSite;
        
        // Create Account record  
        Account accRec = new Account();
        accRec.name = 'Test Account';
        accRec.on_demand_id__c = 'Account1';
        insert accRec;
    
        // Create Contact record
        Contact contactRec = new Contact ();
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        //contactRec.On_Demand_Id_P2P__c = 'TestContactId';
        insert contactRec;
    
        // Create Contact record
        list<Site_Contacts__c> siteCon = new list<Site_Contacts__c>();
        Site_Contacts__c sitecon2 = new Site_Contacts__c();
        sitecon2.Related_Site__c=UnverifiedSite.Id;
        sitecon2.Related_Contact__c=contactRec.Id;
        insert sitecon2;
        siteCon.add(sitecon2);
        
        test.startTest();
        CaseSiteContactAssociation cssiteconass = new CaseSiteContactAssociation();
        cssiteconass.start(BatchCont);
        cssiteconass.execute(BatchCont, ListOfsiteInt);
        test.stopTest();
    }
    static testMethod void cssttest2(){
        Database.QueryLocator QueryLoc;
        Database.BatchableContext BatchCont;
        date CRM_Modified_Date = date.newinstance(1963, 01,24);
        
        //Create Site Int record and assign to site int list
        List<Site_Int__c> ListOfsiteInt = new List<Site_Int__c> ();
        Site_Int__c stint = new Site_Int__c();
        stint.CRM_Case_Id__c='TestCaseId';
        stint.CRM_Contact_Id__c='TestContactId';
        stint.Address_2__c='Test Sydney1';
        stint.Address_3__c='Test Sydney2';
        stint.City__c='Sydney';
        stint.Latitude__c='TestLatitude';
        stint.Longitude__c='TestLonitude';
        stint.Premises_Type__c='TestPremisesType';
        stint.Location_ID__c='';
        stint.Number_Street__c='locteststreet';
        stint.Post_Code__c='562012';
        stint.State__c='SydneyState';
        stint.Transformation_Status__c='Extracted from CRM for Initial Load';
        insert stint;
        ListOfsiteInt.add(stint);
        
        //Create site record
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        Site__c UnverifiedSite = new Site__c();
        UnverifiedSite.RecordTypeId=recordTypeId;
        UnverifiedSite.Site_Address__c=stint.Combined_Address__c;
        UnverifiedSite.Locality_Name__c=stint.City__c;
        UnverifiedSite.Road_Number_1__c=stint.Number_Street__c;
        UnverifiedSite.Latitude__c = stint.Latitude__c;
        UnverifiedSite.Longitude__c = stint.Longitude__c;                                         
        UnverifiedSite.isSiteCreationThroughCode__c =true;
        insert UnverifiedSite;
        
        // Create Account record  
        Account accRec = new Account();
        accRec.name = 'Test Account';
        accRec.on_demand_id__c = 'Account1';
        insert accRec;
    
        // Create Contact record
        Contact contactRec = new Contact ();
        contactRec.on_demand_id_P2P__c = 'Contact123';
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        insert contactRec;
    
        // Create Contact record
        list<Site_Contacts__c> siteCon = new list<Site_Contacts__c>();
        Site_Contacts__c sitecon2 = new Site_Contacts__c();
        sitecon2.Related_Site__c=UnverifiedSite.Id;
        sitecon2.Related_Contact__c=contactRec.Id;
        insert sitecon2;
        siteCon.add(sitecon2);
        
        test.startTest();
        CaseSiteContactAssociation cssiteconass = new CaseSiteContactAssociation();
        cssiteconass.start(BatchCont);
        cssiteconass.execute(BatchCont, ListOfsiteInt);
        test.stopTest();        
    }
    static testMethod void cssttest3(){
        Database.QueryLocator QueryLoc;
        Database.BatchableContext BatchCont;
        date CRM_Modified_Date = date.newinstance(1963, 01,24);
        
        //Create Site Int record and assign to site int list
        List<Site_Int__c> ListOfsiteInt = new List<Site_Int__c> ();
        Site_Int__c stint = new Site_Int__c();
        stint.CRM_Case_Id__c='TestCaseId';
        stint.CRM_Contact_Id__c='TestContactId';
        stint.Address_2__c='Test Sydney1';
        stint.Address_3__c='Test Sydney2';
        stint.City__c='Sydney';
        stint.Latitude__c='TestLatitude';
        stint.Longitude__c='TestLonitude';
        stint.Premises_Type__c='TestPremisesType';
        stint.Location_ID__c='NorthWest';
        stint.Number_Street__c='locteststreet';
        stint.Post_Code__c='562012';
        stint.State__c='SydneyState';
        stint.Transformation_Status__c='Extracted from CRM for Initial Load';
        insert stint;
        ListOfsiteInt.add(stint);
        
        // Create Site record
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        Site__c UnverifiedSite = new Site__c();
        UnverifiedSite.RecordTypeId=recordTypeId;
        UnverifiedSite.Site_Address__c=stint.Combined_Address__c;
        UnverifiedSite.Locality_Name__c=stint.City__c;
        UnverifiedSite.Road_Number_1__c=stint.Number_Street__c;
        UnverifiedSite.Latitude__c = stint.Latitude__c;
        UnverifiedSite.Longitude__c = stint.Longitude__c;  
        UnverifiedSite.MDU_CP_On_Demand_Id__c = 'NorthWest';
        UnverifiedSite.isSiteCreationThroughCode__c =true;
        insert UnverifiedSite;
        
        // Create Account record  
        Account accRec = new Account();
        accRec.name = 'Test Account';
        accRec.on_demand_id__c = 'Account1';
        insert accRec;
    
        // Create Contact record
        Contact contactRec = new Contact ();
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        contactRec.On_Demand_Id_P2P__c = 'TestContactId';
        insert contactRec;
    
        // Create Contact record
        list<Site_Contacts__c> siteCon = new list<Site_Contacts__c>();
        Site_Contacts__c sitecon2 = new Site_Contacts__c();
        sitecon2.Related_Site__c=UnverifiedSite.Id;
        sitecon2.Related_Contact__c=contactRec.Id;
        insert sitecon2;
        siteCon.add(sitecon2);
        
        test.startTest();
        CaseSiteContactAssociation cssiteconass = new CaseSiteContactAssociation();
        cssiteconass.start(BatchCont);
        cssiteconass.execute(BatchCont, ListOfsiteInt);
        test.stopTest();
    }
}