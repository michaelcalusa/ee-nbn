/***************************************************************************************************
Class Name  :  CaseSiteDetailsComponentController
Test Class Type  :   CaseCreateRelated_Test
Version     : 1.0 
Created Date: 01/12/2017
Function    : Method to get all the site id details for customer connection case.
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Shubham Jaiswal       01/12/2017                 Created
* Shubham Jaiswal       09/03/2018                 Updated - MSEU-10336 - getccOrder
****************************************************************************************************/

public class CaseSiteDetailsComponentController {
    
    @AuraEnabled
    public static Site__c getSite(Id caseRecordId) {
        Id unverRecTypeID = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId();
        Id verRecTypeID = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Verified').getRecordTypeId();
        
        Set<Id> recordTypeSet = new Set<Id>{unverRecTypeID,verRecTypeID };
        Site__c st = new Site__c();
        List<Site__c> stList = new List<Site__c>();
        Boolean isUnverified;
        
        if(caseRecordId != null)
        {
            List<Case> csList = [Select id,Loc_Id__c,Site__c From Case where Id=:caseRecordId];
            
            if(csList != null && csList.size() > 0 && csList[0].Loc_Id__c != null){
                stList = [SELECT Id, Name,Location_Id__c,SAM__c,Technology_Type__c,Premises_Type__c,recordtypeid  FROM Site__c where Location_Id__c =:csList[0].Loc_Id__c and recordtypeid IN :recordTypeSet ];
            }
            
            if(stList != null && stList.size() > 0 ){
                st = stList[0];
                isUnverified = (stList[0].recordtypeid == unverRecTypeID)? true:false;
                SiteQualification_CX SiteQualification_CX_Con = new SiteQualification_CX(new ApexPages.StandardController(st));
                if(!Test.isRunningTest() && stList[0].recordtypeid == unverRecTypeID ){
                    SiteQualification_CX_Con.reQualifySiteRecord();
                }
            }
            
            if(csList != null && csList.size() > 0 && csList[0].Loc_Id__c != null && isUnverified){
                stList = [SELECT Id, Name,Location_Id__c,SAM__c,Technology_Type__c,Premises_Type__c FROM Site__c where Location_Id__c =:csList[0].Loc_Id__c and recordtypeid IN :recordTypeSet ];
                st = stList[0];
            }
        }

        return st;
    }
    
    @AuraEnabled
    public static Customer_Connections_Order__c getccOrder(Id caseRecordId) {
        
        Customer_Connections_Order__c ord = new Customer_Connections_Order__c();
        List<Customer_Connections_Order__c> ordList = new List<Customer_Connections_Order__c>();
        
        if(caseRecordId != null)
        {
            List<Case> csList = [Select id,Order_ID__c,Site__c From Case where Id=:caseRecordId];
            
            if(csList != null && csList.size() > 0 && csList[0].Order_ID__c != null){
                ordList = [SELECT Id,Age_of_Order_Days__c,Appointment_Date__c,Appointment_ID__c,Location_ID__c,NBN_Reason_Code__c,Order_Created_Date__c,Order_ID__c,Plan_Remediation_Date__c,Serviceability_Class__c,Order_Status__c,Order_Status_Change__c,Recent_Work_Order_ID__c,RSP__c,Tech_Notes__c,Work_RequestID__c FROM Customer_Connections_Order__c where Order_ID__c =:csList[0].Order_ID__c];
                ord = ordList[0];
            }
        }

        return ord;
    }

}