/***************************************************************************************************
        Class Name:  CaseSite_Test
        Class Type: Test Class 
        Version     : 1.0 
        Created Date: 02-11-2016
        Function    : This class contains unit test scenarios for CaseSite apex class.
        Used in     : None
        Modification Log :
        * Developer                   Date                   Description
        * ----------------------------------------------------------------------------                 
        * Hari Kalannagari         02-11-2016                Created
        ****************************************************************************************************/
        @isTest
        private class CaseSite_Test {
            static testMethod void cssttest(){
                Database.QueryLocator QueryLoc;
                Database.BatchableContext BatchCont;
                date CRM_Modified_Date = date.newinstance(1963, 01,24);
                //Create Site Int
                List<Site_Int__c> ListOfsiteInt = new List<Site_Int__c> ();
                Site_Int__c stint = new Site_Int__c();
                stint.CRM_Case_Id__c='TestCaseId';
                stint.CRM_Contact_Id__c='TestContactId';
                stint.Address_2__c='Test Sydney1';
                stint.Address_3__c='Test Sydney2';
                stint.City__c='Sydney';
                stint.Latitude__c='TestLatitude';
                stint.Longitude__c='TestLonitude';
                stint.Premises_Type__c='TestPremisesType';
                stint.Location_ID__c='LOC123456789012';
                stint.Number_Street__c='locteststreet';
                stint.Post_Code__c='2015';
                stint.State__c='TAS';
                stint.Transformation_Status__c='Extracted from CRM for Initial Load';
                ListOfsiteInt.add(stint);
                insert ListOfsiteInt;
                
                // Create Contact record
                Contact contactRec = new Contact ();
                contactRec.on_demand_id_P2P__c = 'Contact123';
                contactRec.LastName = 'HAri';
                contactRec.Email = 'hari@nbnco.com.au';
                insert contactRec;
                
                test.startTest();
                CaseSite cssite = new CaseSite();
                cssite.start(BatchCont);
                cssite.execute(BatchCont, ListOfsiteInt);
                test.stopTest();
            }
            static testMethod void cssttest2(){
                Database.QueryLocator QueryLoc;
                Database.BatchableContext BatchCont;
                date CRM_Modified_Date = date.newinstance(1963, 01,24);
                //Create Site Int
                List<Site_Int__c> ListOfsiteInt = new List<Site_Int__c> ();
                Site_Int__c stint = new Site_Int__c();
                stint.CRM_Case_Id__c='TestCaseId';
                stint.CRM_Contact_Id__c='TestContactId';
                stint.Address_2__c='Test Sydney1';
                stint.Address_3__c='Test Sydney2';
                stint.City__c='Sydney';
                stint.Latitude__c='TestLatitude';
                stint.Longitude__c='TestLonitude';
                stint.Premises_Type__c='Health Facility';
                stint.Location_ID__c='LOC123456789012';
                stint.Number_Street__c='locteststreet';
                stint.Post_Code__c='2015';
                stint.State__c='TAS';
                stint.Transformation_Status__c='Extracted from CRM for Initial Load';
                ListOfsiteInt.add(stint);
                insert ListOfsiteInt;               
                // Create Contact record
                Contact contactRec = new Contact ();
                contactRec.on_demand_id_P2P__c = 'Contact123';
                contactRec.LastName = 'HAri';
                contactRec.Email = 'hari@nbnco.com.au';
                insert contactRec;
                
                test.startTest();
                CaseSite cssite = new CaseSite();
                cssite.start(BatchCont);
                cssite.execute(BatchCont, ListOfsiteInt);
                test.stopTest();
            }
            static testMethod void cssttest3(){
                Database.QueryLocator QueryLoc;
                Database.BatchableContext BatchCont;
                date CRM_Modified_Date = date.newinstance(1963, 01,24);
                // Create Site Int list
                List<Site_Int__c> ListOfsiteInt = new List<Site_Int__c> ();
                Site_Int__c stint = new Site_Int__c();
                stint.CRM_Case_Id__c='05912345';
                stint.CRM_Contact_Id__c='TestContactId';
                stint.Address_2__c='ASASEQWE';
                stint.Address_3__c='Test Sydney2';
                stint.City__c='Sydney';
                stint.Latitude__c='TestLatitude';
                stint.Longitude__c='TestLonitude';
                stint.Premises_Type__c='Health Facility';
                stint.Location_ID__c='';
                stint.Number_Street__c='HNO';
                stint.Post_Code__c='2015';
                stint.State__c='TAS';
                stint.Transformation_Status__c='Extracted from CRM for Initial Load';
                
                Site_Int__c stint1 = new Site_Int__c();
                stint1.CRM_Case_Id__c='TestCaseId';
                stint1.CRM_Contact_Id__c='TestContactId';
                stint1.Address_2__c='ASASEQWE';
                stint1.Address_3__c='Test Sydney2';
                stint1.City__c='Sydney';
                stint1.Latitude__c='TestLatitude';
                stint1.Longitude__c='TestLonitude';
                stint1.Premises_Type__c='TestPremisesType';
                stint1.Location_ID__c='';
                stint1.Number_Street__c='HNO';
                stint1.Post_Code__c='2015';
                stint1.State__c='Sydney';
                stint1.Transformation_Status__c='Extracted from CRM for Initial Load';
                
                ListOfsiteInt.add(stint);
                ListOfsiteInt.add(stint1);
                insert ListOfsiteInt;
                List<Site_Int__c> ListOfsiteInt1 = [Select CRM_Case_Id__c,CRM_Contact_Id__c,Unique_Site__c,Address_2__c,Address_3__c,City__c,Latitude__c,Longitude__c,Premises_Type__c,Technology_Type__c,Rollout_Type__c,Location_ID__c,Number_Street__c,Post_Code__c,State__c,Combined_Address__c FROM Site_Int__c where Transformation_Status__c = 'Extracted from CRM for Initial Load' order by createddate asc];
                ListOfsiteInt1[0].Post_Code__c = '2013';
                ListOfsiteInt1[1].Post_Code__c = '2013';
                update ListOfsiteInt1;
                
                // Create Contact record
                Contact contactRec = new Contact ();
                contactRec.on_demand_id_P2P__c = 'Contact123';
                contactRec.LastName = 'Syed';
                contactRec.Email = 'syed@nbnco.com.au';
                insert contactRec;
                test.startTest();
                CaseSite cssite = new CaseSite();
                cssite.start(BatchCont);
                cssite.execute(BatchCont, ListOfsiteInt1);
                test.stopTest();
            }
            static testMethod void cssttest4(){
                Database.QueryLocator QueryLoc;
                Database.BatchableContext BatchCont;
                date CRM_Modified_Date = date.newinstance(1963, 01,24);
                // Create Site Int list
                List<Site_Int__c> ListOfsiteInt = new List<Site_Int__c> ();
                
                Site_Int__c stint2 = new Site_Int__c();
                stint2.CRM_Case_Id__c='05912345';
                stint2.CRM_Contact_Id__c='TestContactId';
                //stint2.Unique_Site__c='TestUniqueSite';
                stint2.Address_2__c='ASASEQWE';
                stint2.Address_3__c='Test Sydney2';
                stint2.City__c='Sydney';
                stint2.Latitude__c='TestLatitude';
                stint2.Longitude__c='TestLonitude';
                stint2.Premises_Type__c='wer235';
                stint2.Location_ID__c='';
                stint2.Number_Street__c='HNO';
                stint2.Post_Code__c='2015';
                stint2.State__c='TAS';
                stint2.Transformation_Status__c='Extracted from CRM for Initial Load';
                
                ListOfsiteInt.add(stint2);
                insert ListOfsiteInt;
                List<Site_Int__c> ListOfsiteInt1 = [Select CRM_Case_Id__c,CRM_Contact_Id__c,Unique_Site__c,Address_2__c,Address_3__c,City__c,Latitude__c,Longitude__c,Premises_Type__c,Technology_Type__c,Rollout_Type__c,Location_ID__c,Number_Street__c,Post_Code__c,State__c,Combined_Address__c FROM Site_Int__c where Transformation_Status__c = 'Extracted from CRM for Initial Load' order by createddate asc];
                ListOfsiteInt1[0].Post_Code__c = '2013';
                update ListOfsiteInt1;
                ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
                
                // Create Contact record
                Contact contactRec = new Contact ();
                contactRec.on_demand_id_P2P__c = 'Contact123';
                contactRec.LastName = 'Syed';
                contactRec.Email = 'syed@nbnco.com.au';
                insert contactRec;
                test.startTest();
                CaseSite cssite = new CaseSite();
                cssite.start(BatchCont);
                cssite.execute(BatchCont, ListOfsiteInt1);
                test.stopTest();
            }
            
        }