/*------------------------------------------------------------  
Author     : Shubham Jaiswal
Company    : Wipro Ltd.
Description:   
Test Class :    
Used in    : CaseStageTrigger
<Date>      <Authors Name>     <Brief Description of Change>
05/04/2018  Ganesh Sawant      Record type initialization of incoming remedy data into either 'Assurance Jeopardy' or
                               'Activation Jeopardy' therefore eliminating the record type 'Ecalation Triage'
                               MSEU-11461
------------------------------------------------------------*/  
public class CaseStageTriggerHandler {

    private List<Case_Stage__c> trgOldList = new List<Case_Stage__c> ();
    private List<Case_Stage__c> trgNewList = new List<Case_Stage__c> ();
    private Map<id,Case_Stage__c> trgOldMap = new Map<id,Case_Stage__c> ();
    private Map<id,Case_Stage__c> trgNewMap = new Map<id,Case_Stage__c> ();
    private Map<String,List<Site__c>> locationIDtoSiteMap = new Map<String,List<Site__c>>();
    private Map<String,List<Account>> accessSeekerIDTSLtoAccountMap = new Map<String,List<Account>>();
    private Map<String,String> catSubCattoRecordTypeMap = new Map<String,String>();

    public CaseStageTriggerHandler(List<Case_Stage__c> trgOldList, List<Case_Stage__c> trgNewList, Map<id,Case_Stage__c> trgOldMap, Map<id,Case_Stage__c> trgNewMap){
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
        retrieveSites(trgNewList);
        retrieveAccounts(trgNewList);
        reteiveCatSubCatData();
        system.debug('@@@@@locationIDtoSiteMap' + locationIDtoSiteMap);
        system.debug('@@@@@accessSeekerIDTSLtoAccountMap' + accessSeekerIDTSLtoAccountMap);
    }
    
    public void OnAfterInsert()
    {
        if(!HelperUtility.isTriggerMethodExecutionDisabled('CreateCasefromCaseStage')){
            system.debug('OnAfterInsert - trgOldMap....'+trgNewMap); 
            CreateUpdateCasefromCaseStage(trgNewList, null);
        }
    }
    
    public void OnAfterUpdate()
    {
        if(!HelperUtility.isTriggerMethodExecutionDisabled('UpdateCasefromCaseStage')){
            system.debug('OnAfterUpdate - trgOldMap....'+trgOldMap); 
            CreateUpdateCasefromCaseStage(trgNewList, trgOldMap);
        }
    }
    
    public void CreateUpdateCasefromCaseStage(List<Case_Stage__c> trgNewList, Map<id,Case_Stage__c> trgOldMap)
    {  
        map<String, Id> mapRecordTypeCase = new map<string, Id>();
        map<String, Id> mapRecordTypeSite = new map<string, Id>();
        mapRecordTypeCase = HelperUtility.pullAllRecordTypes('Case');
        mapRecordTypeSite = HelperUtility.pullAllRecordTypes('Site__c');
        List<Case> upsertCaseList = new List<Case>();
        List<Site__c> newSiteList = new List<Site__c>();
        Set<String> setLocationIDTemp = new Set<String>();

        for(Case_Stage__c cst:trgNewList)
        {
            Case cs = new Case();
            cs.Remedy_Case_Stage__c = cst.id; 
            cs.Remedy_Case_Stage_Id__c = cst.id;        
            cs.Incident_Number__c = cst.IncidentID__c;
            cs.Incident_Raised_Date__c = cst.Incident_Raised_DateTime__c;
            //cs.Remedy_Industry_Status__c = cst.Industry_Status__c;
            cs.Technology__c = 'N/A';
            if(cst.Industry_Status__c == 'InProgress'){
                cs.Status = 'In Progress';
            }
            else{
                cs.Status = cst.Industry_Status__c;
            }
            if(cst.Industry_Status__c == 'Resolved'){
                cs.SME_Resolution_Status__c = 'Activities Complete';
                cs.SME_Resolution_Reason__c = 'Resolution Met';
            }
            else if(cst.Industry_Status__c == 'Closed'){
                cs.SME_Resolution_Status__c = 'Case Closed';
                cs.SME_Resolution_Reason__c = 'Resolution Met';
            }
            
            if(cst.LocationIDAnswer__c != null && cst.LocationIDAnswer__c != '')
                cs.Loc_Id__c = cst.LocationIDAnswer__c;
            else if(cst.LocationIDTSL__c != null && cst.LocationIDTSL__c != '')
                cs.Loc_Id__c = cst.LocationIDTSL__c;
            else if(cst.LocationIDCSL__c != null && cst.LocationIDCSL__c != '')
                cs.Loc_Id__c = cst.LocationIDCSL__c;            
            
            cs.Remedy_Notification_Type__c = cst.Notification_Type__c; 
            cs.OpCat1__c = cst.OpCat1__c;
            cs.OpCat2__c = cst.OpCat2__c; 
            cs.OpCat3__c = cst.OpCat3__c; 
            cs.Order_Number__c = cst.OrderNumber__c; 
            cs.Remedy_Planned_Resolution_Date__c = Date.valueOf(cst.Planned_Resolution_DateTime__c);
            cs.Priority = cst.Priority__c;      
            cs.Type = cst.Request_Type__c;
            cs.Origin = 'Remedy';
            cs.Description = cst.Comments__c;
            
            //Record Type Selection
            /*Based on MSEU-11461, we will be creating only 2 type of recordtype cases for RSP rememdy data
              and there will be no 'Escalation Triage' case going forward.
            */
            if(cst.Request_Type__c == 'User Service Restoration'){
                cs.recordtypeid = mapRecordTypeCase.get('Assurance Jeopardy');      
            } 
            else{
                cs.recordtypeid =  mapRecordTypeCase.get('Activations Jeopardy');
            }
            /*if(cst.Request_Type__c == 'User Service Request' && catSubCattoRecordTypeMap.containsKey(cst.OpCat2__c+cst.OpCat3__c) &&  mapRecordTypeCase.containsKey(catSubCattoRecordTypeMap.get(cst.OpCat2__c+cst.OpCat3__c))){
                if(catSubCattoRecordTypeMap.get(cst.OpCat2__c+cst.OpCat3__c) == 'Activations Jeopardy'){
                    cs.recordtypeid =  mapRecordTypeCase.get('Activations Jeopardy');    
                }
                else{
                    cs.recordtypeid =  mapRecordTypeCase.get('Escalations Triage');
                }
            }
            else if(cst.Request_Type__c == 'User Service Restoration'){
                cs.recordtypeid = mapRecordTypeCase.get('Assurance Jeopardy');      
            }
            else{
                cs.recordtypeid = mapRecordTypeCase.get('Escalations Triage');
            }*/

            
            //Account Selection
            if(accessSeekerIDTSLtoAccountMap.containsKey(cst.AccessSeeker_ID__c) && accessSeekerIDTSLtoAccountMap.get(cst.AccessSeeker_ID__c) != null && accessSeekerIDTSLtoAccountMap.get(cst.AccessSeeker_ID__c).size() > 0){
                cs.RSP_ISP__c = accessSeekerIDTSLtoAccountMap.get(cst.AccessSeeker_ID__c)[0].id;
                cs.RSP_ISP_Name__c = accessSeekerIDTSLtoAccountMap.get(cst.AccessSeeker_ID__c)[0].Name;
            }
            
            //Site Selection
            if(locationIDtoSiteMap.containsKey(cs.Loc_Id__c) && locationIDtoSiteMap.get(cs.Loc_Id__c) != null){
                if(locationIDtoSiteMap.get(cs.Loc_Id__c).size() == 1){
                    cs.Site__c = locationIDtoSiteMap.get(cs.Loc_Id__c)[0].Id;
                }
                else if(locationIDtoSiteMap.get(cs.Loc_Id__c).size() > 1){
                    
                    for(Site__c st:locationIDtoSiteMap.get(cs.Loc_Id__c)){
                        if(st.recordtypeid == mapRecordTypeSite.get('Verified')){
                            cs.Site__c = st.id;
                            break;
                        }
                    }
                    if(cs.Site__c == null){
                        for(Site__c st:locationIDtoSiteMap.get(cs.Loc_Id__c)){
                            if(st.recordtypeid == mapRecordTypeSite.get('Unverified')){
                                cs.Site__c = st.id;
                                break;
                            }
                        }
                    }
                }   
            }
            else if(cs.Loc_Id__c != null && cs.Loc_Id__c != '' && Pattern.matches('[L]{1}[O]{1}[C]{1}[0-9]{12}',cs.Loc_Id__c) ){
                Site__c stReference = new Site__c(Location_Id__c = cs.Loc_Id__c);
                cs.Site__r = stReference;
                if(!setLocationIDTemp.contains(cs.Loc_Id__c))
                {
                    Site__c st = new Site__c(Name = cs.Loc_Id__c, Location_Id__c = cs.Loc_Id__c, recordtypeid = mapRecordTypeSite.get('Unverified'),Description__c = 'Site created for Remedy Case Incident ' + cst.IncidentID__c);
                    setLocationIDTemp.add(cs.Loc_Id__c);
                    newSiteList.add(st);
                }
            }
            
            upsertCaseList.add(cs);
        }
        
        //Insert new Sites
        if(newSiteList.size() > 0){
            Database.SaveResult[] srList = Database.insert(newSiteList, false);
            
            list<Error_Logging__c> errSiteList = new List<Error_Logging__c>();
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {               
                    for(Database.Error err : sr.getErrors()) {
                        errSiteList.add(new Error_Logging__c(Name='Remedy - New Site Creation Error' ,CRM_Record_Id__c = newSiteList[0].Description__c.right(15),Error_Message__c=err.getMessage(),Fields_Afftected__c=String.valueof(err.getFields()),isSuccess__c=sr.isSuccess(),Status_Code__c=String.valueof(err.getStatusCode())));
                    }
                }
            }
            
            if(errSiteList.size() > 0){
                insert errSiteList;
            }
        }
        
        //Upsert Cases
        if(upsertCaseList.size() > 0){
            Schema.SObjectField f = Case.Fields.Remedy_Case_Stage_Id__c;
            Database.UpsertResult[] upsrList = Database.upsert(upsertCaseList,f,false);
            
            list<Error_Logging__c> errCaseList = new List<Error_Logging__c>();
            for (Database.UpsertResult upsr : upsrList) {
                if (!upsr.isSuccess()) {               
                    for(Database.Error err : upsr.getErrors()) {
                        errCaseList.add(new Error_Logging__c(Name='Remedy - Case Insert/Update Error' ,CRM_Record_Id__c = upsertCaseList[0].Remedy_Case_Stage_Id__c,Error_Message__c=err.getMessage(),Fields_Afftected__c=String.valueof(err.getFields()),isSuccess__c=upsr.isSuccess(),Status_Code__c=String.valueof(err.getStatusCode()),isCreated__c= upsr.isCreated()));
                    }
                }
            }
            
            if(errCaseList.size() > 0){
                insert errCaseList;
            }
        }
    }
    
    //Retrieving Accounts from AccessSeeker
    public void retrieveAccounts(List<Case_Stage__c> trgNewList)
    {
        map<String, Id> mapRecordType = new map<string, Id>();
        mapRecordType = HelperUtility.pullAllRecordTypes('Account');
        
        Set<String> accessSeekerIDSet = new Set<String>();
        Id rspRecordTypeId = mapRecordType.get('Retail Service Provider');
        for(Case_Stage__c cst:trgNewList)
        {
            if(cst.AccessSeeker_ID__c != null && cst.AccessSeeker_ID__c != '')
                accessSeekerIDSet.add(cst.AccessSeeker_ID__c.trim());
        }
        
        if(accessSeekerIDSet != null && accessSeekerIDSet.size() > 0)
        {
            for(Account ac :[SELECT id,Name,Access_Seeker_ID__c from Account WHERE Access_Seeker_ID__c IN:accessSeekerIDSet AND recordtypeid = :rspRecordTypeId AND Status__c = 'Active' FOR UPDATE] )
            {
                if(accessSeekerIDTSLtoAccountMap.containsKey(ac.Access_Seeker_ID__c) && accessSeekerIDTSLtoAccountMap.get(ac.Access_Seeker_ID__c) != null){
                    List<Account> accountList = new List<Account>(accessSeekerIDTSLtoAccountMap.get(ac.Access_Seeker_ID__c));
                    accountList.add(ac);
                    accessSeekerIDTSLtoAccountMap.put(ac.Access_Seeker_ID__c,accountList);
                }
                else{
                    accessSeekerIDTSLtoAccountMap.put(ac.Access_Seeker_ID__c,new List<Account>{ac});
                }
            }
        }   
    }
    
    //Retrieving Sites from LocationIDAnswer,LocationIDTSL,LocationIDCSL
    public void retrieveSites(List<Case_Stage__c> trgNewList)
    {
        Set<String> locationIDSet = new Set<String>();
        
        for(Case_Stage__c cst:trgNewList)
        {
            if(cst.LocationIDAnswer__c != null && cst.LocationIDAnswer__c != '')
                locationIDSet.add(cst.LocationIDAnswer__c.trim());
            else if(cst.LocationIDTSL__c != null && cst.LocationIDTSL__c != '')
                locationIDSet.add(cst.LocationIDTSL__c.trim());
            else if(cst.LocationIDCSL__c != null && cst.LocationIDCSL__c != '')
                locationIDSet.add(cst.LocationIDCSL__c.trim());
        }
        
        if(locationIDSet != null && locationIDSet.size() > 0)
        {
            for(Site__c st :[SELECT id,Location_Id__c,recordtypeid from Site__c WHERE Location_Id__c IN:locationIDSet FOR UPDATE] )
            {
                if(locationIDtoSiteMap.containsKey(st.Location_Id__c) && locationIDtoSiteMap.get(st.Location_Id__c) != null){
                    List<Site__c> siteList = new List<Site__c>(locationIDtoSiteMap.get(st.Location_Id__c));
                    siteList.add(st);
                    locationIDtoSiteMap.put(st.Location_Id__c,siteList);
                }
                else{
                    locationIDtoSiteMap.put(st.Location_Id__c,new List<Site__c>{st});
                }
            }
        }   
    }
    
    // Custom Setting for Category Sub Category Assignment
    public void reteiveCatSubCatData(){
        List<CaseStage_Category_Sub_Category__c> catSubCatList = CaseStage_Category_Sub_Category__c.getall().values();
        for(CaseStage_Category_Sub_Category__c catSubCat :catSubCatList){
            catSubCattoRecordTypeMap.put(catSubCat.Category__c+catSubCat.Sub_Category__c,catSubCat.Record_Type__c);
        }
    } 
}