@isTest
public class CaseStageTriggerHandler_Test {


    @testsetup
    static void createData(){
    
        CaseStage_Category_Sub_Category__c cst = new CaseStage_Category_Sub_Category__c();
        cst.Name = '1';
        cst.Category__c = 'Address Enquiry';
        cst.Sub_Category__c = 'Second Enquiry Response';
        cst.Record_Type__c = 'Activations Jeopardy';    
        insert cst;
        
        CaseStage_Category_Sub_Category__c cst2 = new CaseStage_Category_Sub_Category__c();
        cst2.Name = '2';
        cst2.Category__c = 'Inventory Update';        
        cst2.Sub_Category__c = 'NOC Support Tool';
        cst2.Record_Type__c = 'Escalations Triage';   
        insert cst2;

        
        map<String, Id> mapRecordType = new map<string, Id>();
        mapRecordType = HelperUtility.pullAllRecordTypes('Account');
        Account ac1 = new Account(Name='Test RSP Account', Access_Seeker_ID__c = 'ASI000000151632', recordtypeid = mapRecordType.get('Retail Service Provider'));
        insert ac1;
        
        Account ac2 = new Account(Name='Test RSP Account 2', Access_Seeker_ID__c = 'ASI000000151632', recordtypeid = mapRecordType.get('Retail Service Provider'));
        insert ac2;
        
        List<Site__c> siteList = new List<Site__c>();
        map<String, Id> mapRecordTypeSite = new map<string, Id>();
        mapRecordTypeSite = HelperUtility.pullAllRecordTypes('Site__c');
        
        Site__c st1 = new Site__c(Name = 'Test Site 1',Location_Id__c = 'LOC000058943492',recordtypeid = mapRecordTypeSite.get('Verified'));
        siteList.add(st1);
        
        Site__c st2 = new Site__c(Name = 'Test Site 2',Location_Id__c = 'LOC000058943493',recordtypeid = mapRecordTypeSite.get('Unverified'));
        siteList.add(st2);
        
        Site__c st3 = new Site__c(Name = 'Test Site 3',Location_Id__c = 'LOC000058943492',recordtypeid = mapRecordTypeSite.get('MDU/CP'));
        siteList.add(st3);

        Site__c st4 = new Site__c(Name = 'Test Site 4',Location_Id__c = 'LOC000058943493',recordtypeid = mapRecordTypeSite.get('MDU/CP'));
        siteList.add(st4);
        
        Site__c st5 = new Site__c(Name = 'Test Site 5',Location_Id__c = 'LOC000058943490',recordtypeid = mapRecordTypeSite.get('Verified'));
        siteList.add(st5);
        
        insert siteList;
        
        
        Case_Stage__c cs = new Case_Stage__c();
        cs.AccessSeeker_ID__c = 'ASI000000151632';
        cs.Comments__c = 'Test Comments';
        cs.IncidentID__c = 'INC000002238756790';
        cs.Incident_Raised_DateTime__c = datetime.now() - 1; 
        cs.Industry_Status__c = 'Closed';
        cs.LocationIDTSL__c = 'LOC000058943492';
        cs.Notification_Type__c = 'IncidentClosed'; 
        cs.OpCat1__c = '';
        cs.OpCat2__c = 'HFC Pilot';
        cs.OpCat3__c = 'Appointment related';
        cs.OrderNumber__c = 'ORD0000000000363';
        cs.Owner_FederationID__c = 'testFederationId';
        cs.Owner_Group__c = 'FTTX Assurance Tier1';
        cs.Planned_Resolution_DateTime__c = datetime.now() + 1;
        cs.Priority__c = 'Critical';
        cs.Request_Type__c = 'User Service Request';
        cs.Source__c = 'Email';
        
        insert cs;
        
        Case_Stage__c cs1 = new Case_Stage__c();
        cs1.AccessSeeker_ID__c = 'ASI000000151632';
        cs1.Comments__c = 'Test Comments';
        cs1.IncidentID__c = 'INC000002238756890';
        cs1.Incident_Raised_DateTime__c = datetime.now() - 1; 
        cs1.Industry_Status__c = 'Resolved';
        cs1.LocationIDTSL__c = 'LOC000058943492';
        cs1.Notification_Type__c = 'IncidentClosed'; 
        cs1.OpCat1__c = '';
        cs1.OpCat2__c = 'HFC Pilot';
        cs1.OpCat3__c = 'Appointment related';
        cs1.OrderNumber__c = 'ORD0000000000363';
        cs1.Owner_FederationID__c = 'testFederationId';
        cs1.Owner_Group__c = 'FTTX Assurance Tier1';
        cs1.Planned_Resolution_DateTime__c = datetime.now() + 1;
        cs1.Priority__c = 'Critical';
        cs1.Request_Type__c = 'User Service Restoration';
        cs1.Source__c = 'Email';
        
        insert cs1;
    }
    

    static testmethod void test1(){
        
        Case_Stage__c cs = [SELECT id from Case_Stage__c where Request_Type__c = 'User Service Request'][0];
        cs.OpCat2__c = 'Inventory Update';
        cs.OpCat3__c = 'NOC Support Tool';
        cs.Industry_Status__c = 'InProgress';
        update cs;

        cs.OpCat2__c = 'Address Enquiry';
        cs.OpCat3__c = 'Second Enquiry Response';
        update cs;
        
        cs.OpCat2__c = '';
        update cs;
 
    }
    
    static testmethod void test2(){
        
        Case_Stage__c cs = [SELECT id from Case_Stage__c where Request_Type__c = 'User Service Request'][0];
 
        cs.LocationIDAnswer__c = 'LOC000058943494';
        update cs;
        
        cs.LocationIDAnswer__c = '';
        update cs;
        
        cs.LocationIDCSL__c = 'LOC000058943493';
        cs.LocationIDAnswer__c = '';
        cs.LocationIDTSL__c = '';
        update cs;
        
        cs.LocationIDCSL__c = 'LOC000058943490';
        update cs;

        cs.LocationIDCSL__c = 'LOC000058943494,LOC000058943495,LOC000058943496';
        update cs;
    }
}