/*------------------------------------------------------------  
Author:        Andrew Zhang
Company:       NBNco
Description:   Create reports based case history
Test Class:    CaseTeamReportUtil_Test
History
<Date>      <Authors Name>     <Brief Description of Change>
12-09-2017  Ganesh Sawant      MSEU 901 Commercial Billing Cases Will use the VIC State Specific Business Hours and Holidays
------------------------------------------------------------*/

public without sharing Class CaseTeamReportUtil{
    
    //used in CaseAfterUpdate trigger
    public static boolean firstRun = true;
    
    public static void createReports(List<Id> caseIds){
        
        if (caseIds.size() == 1)
            createReportsFuture(caseIds[0]);
            
        if (caseIds.size() > 1)
            database.executeBatch(new BatchCalculateCaseTeamReports(null, caseIds), 1);
    }
    
    @future //use future because the case history not available in CaseAfterUpdate trigger
    public static void createReportsFuture(Id caseId){
        createReports(caseId);
    }
        
    public static void createReports(Id caseId){
    	BusinessHours bh;
        Case c = [SELECT Id, SME_Case__c, DP_Case__c, BusinessHoursID FROM Case WHERE Id = :caseId]; 
        ID caseBusinessHrID = c.BusinessHoursID;
        if(caseBusinessHrID!=NULL)
        	bh = [SELECT Id FROM BusinessHours WHERE ID =: caseBusinessHrID];  
        else         
        	bh = [SELECT Id FROM BusinessHours WHERE IsDefault=true];
        //Case c = [SELECT Id, SME_Case__c, DP_Case__c FROM Case WHERE Id = :caseId];
        List<CaseHistory> chs = [SELECT Id, CaseId, CreatedDate, Field, NewValue, OldValue FROM CaseHistory WHERE CaseId = :caseId order by createddate];

        List<Case_Team_Report__c> reports = getTeamReports(c, chs);
        
        for (Case_Team_Report__c report : reports){
            List<CaseHistory> histories = getHistoriesByTeam(chs, report);
            
            Decimal queuingDays = 0;
            Decimal workingDays = 0;
            
            for (CaseHistory ch : histories){
                //Assigned Start
                if (ch.NewValue == 'Assigned') {
                    report.Assigned_Start__c = ch.CreatedDate;
                    report.Assigned_End__c = null;
                }
                
                //Assigned End
                if (ch.OldValue == 'Assigned') {
                    if (report.Assigned_Start__c == null) report.Assigned_Start__c  = report.Team_Start__c;
                    
                    report.Assigned_End__c = ch.CreatedDate;
                    queuingDays += getBusinessDays(bh, report.Assigned_Start__c, report.Assigned_End__c);
                }
                
                //In Progress Start
                if (ch.NewValue == 'In Progress') {
                    report.In_Progress_Start__c = ch.CreatedDate;
                    report.In_Progress_End__c = null;
                }
                
                //In Progress End
                if (ch.OldValue == 'In Progress') {
                    if (report.In_Progress_Start__c == null) report.In_Progress_Start__c = report.Team_Start__c;
                    
                    report.In_Progress_End__c = ch.CreatedDate;
                    workingDays += getBusinessDays(bh, report.In_Progress_Start__c, report.In_Progress_End__c);
                }
                
                //Activities Complete
                if (ch.NewValue == 'Activities Complete') {
                    report.Completed_Date__c = ch.CreatedDate;
                }
                
            }
            
            //team has no history
            if (report.In_Progress_Start__c == null && report.In_Progress_End__c == null && 
                report.Assigned_Start__c == null && report.Assigned_End__c == null){
                
                report.Assigned_Start__c  = report.Team_Start__c;
                report.Assigned_End__c  = report.Team_End__c;
                queuingDays += getBusinessDays(bh, report.Assigned_Start__c, report.Assigned_End__c);
            }
            
            //no assigned end date
            if (report.In_Progress_Start__c == null && report.In_Progress_End__c == null && 
                report.Assigned_Start__c != null && report.Assigned_End__c == null){
                
                report.Assigned_End__c = report.Team_End__c;
                queuingDays += getBusinessDays(bh, report.Assigned_Start__c, report.Assigned_End__c);
            }
            
            //no in progress end date
            if (report.In_Progress_Start__c != null && report.In_Progress_End__c == null){
                report.In_Progress_End__c = report.Team_End__c;
                workingDays += getBusinessDays(bh, report.In_Progress_Start__c, report.In_Progress_End__c);
            }
            
            report.Queuing_Days__c = queuingDays;
            report.Working_Days__c = workingDays;
        }
        
        //count case smes and dps
        Integer smeCount = 0;
        Integer dpCount = 0;
        
        //aggregate by team, if a team assigned multiple times
        Map<String, Case_Team_Report__c> teamReportMap = new Map<String, Case_Team_Report__c>();
        
        for (Case_Team_Report__c report : reports){
            Case_Team_Report__c teamReport = teamReportMap.get(report.Name__c + '-' + report.Type__c);
            
            if (teamReport != null){
                teamReport.Queuing_Days__c = teamReport.Queuing_Days__c + report.Queuing_Days__c;
                teamReport.Working_Days__c = teamReport.Working_Days__c + report.Working_Days__c;
                teamReport.Attempts__c = teamReport.Attempts__c + 1;
            }else{
                report.Attempts__c = 1;
                teamReportMap.put(report.Name__c + '-' + report.Type__c, report);
                
                //count case smes and dps
                if (report.Type__c == 'SME Team') smeCount ++;
                if (report.Type__c == 'DP Team') dpCount ++;
            }
        }        
        
        //save case sme/dp count in report level, due to case is closed        
        for (Case_Team_Report__c report : teamReportMap.values()){
            report.Case_SME_Count__c = smeCount;
            report.Case_DP_Count__c = dpCount;
        }
        
        //clear existing reports
        delete [SELECT Id FROM Case_Team_Report__c WHERE Case__c = :caseId];

        //inser new reports
        insert teamReportMap.values();

        //user reports
        List<Case_Team_Report__c> userReports = new List<Case_Team_Report__c>();
        userReports.addAll(getUserReports(bh, c, 'SME User'));
        userReports.addAll(getUserReports(bh, c, 'DP User'));
        insert userReports;
    }
    
    public static Decimal getBusinessDays(BusinessHours bh, Datetime startDate, Datetime endDate){
        Decimal duration = BusinessHours.diff(bh.Id, startDate, endDate) * 1.0 / (1000 * 60 * 60 * 8);
        return duration.setScale(2, RoundingMode.HALF_UP);  
    }    

    public static List<CaseHistory> getHistoriesByTeam(List<CaseHistory> histories, Case_Team_Report__c report){
        List<CaseHistory> results = new List<CaseHistory>();
        for (CaseHistory ch : histories){
            if (report.Type__c == 'SME Team' && ch.Field == 'SME_Resolution_Status__c' && ch.CreatedDate >= report.Team_Start__c && ch.CreatedDate < report.Team_End__c ) 
                results.add(ch);
            
            if (report.Type__c == 'DP Team' && ch.Field == 'DP_Resolution_Status__c' && ch.CreatedDate >= report.Team_Start__c && ch.CreatedDate < report.Team_End__c )     
                results.add(ch);
        }
        return results;
    }    
    
    public static List<Case_Team_Report__c> getTeamReports(Case c, List<CaseHistory> histories){
        List<Case_Team_Report__c> reports = new List<Case_Team_Report__c>();

        //find team
        for (CaseHistory ch : histories){
            if (ch.Field == 'SME_Team__c' || ch.Field == 'DP_Team__c'){
                Case_Team_Report__c report = new Case_Team_Report__c();
                report.Type__c = ch.Field == 'SME_Team__c' ? 'SME Team' : 'DP Team';
                report.Name__c = String.valueOf(ch.NewValue);
                report.Team_Start__c = ch.CreatedDate;
                
                report.Case__c = c.Id;
                report.Reporting_Case__c = ch.Field == 'SME_Team__c' ? c.SME_Case__c : c.DP_Case__c;
                
                Case_Team_Report__c previousReport = getPreviousReport(reports, report.Type__c);
                if (previousReport != null) 
                    previousReport.Team_End__c = report.Team_Start__c ;
                
                reports.add(report);
            }
            
            if (ch.Field == 'Status' && String.valueOf(ch.NewValue) == 'Closed'){
                Case_Team_Report__c lastSMEReport = getPreviousReport(reports, 'SME Team');
                if (lastSMEReport != null && lastSMEReport.Team_End__c == null)
                    lastSMEReport.Team_End__c = ch.CreatedDate;
                
                Case_Team_Report__c lastDPReport = getPreviousReport(reports, 'DP Team');
                if (lastDPReport != null && lastDPReport.Team_End__c == null)
                    lastDPReport.Team_End__c = ch.CreatedDate;              
            }
        }
        
        //if still no end date, use current datetime
        Case_Team_Report__c lastSMEReport = getPreviousReport(reports, 'SME Team');
        if (lastSMEReport != null && lastSMEReport.Team_End__c == null)
            lastSMEReport.Team_End__c = Datetime.now();
        
        Case_Team_Report__c lastDPReport = getPreviousReport(reports, 'DP Team');
        if (lastDPReport != null && lastDPReport.Team_End__c == null)
            lastDPReport.Team_End__c = Datetime.now();
        
        return reports;
    }   
    
    public static Case_Team_Report__c getPreviousReport(List<Case_Team_Report__c> reports, String teamType){
        Integer size = reports.size();
        for (Integer i = size-1; i >=0; i--){
            if (reports[i].Type__c == teamType){
                return reports[i];
            }
        }
        return null;
    }

    public static List<Case_Team_Report__c> getUserReports(BusinessHours bh, Case c, String type){
    
        List<CaseHistory> histories = new List<CaseHistory>();
        if (type == 'SME User') histories = [SELECT Id, CaseId, CreatedDate, Field, NewValue, OldValue FROM CaseHistory WHERE CaseId = :c.SME_Case__c AND Field IN ('Owner', 'Status') order by createddate];
        if (type == 'DP User') histories = [SELECT Id, CaseId, CreatedDate, Field, NewValue, OldValue FROM CaseHistory WHERE CaseId = :c.DP_Case__c AND Field IN ('Owner', 'Status') order by createddate];
        
        List<CaseHistory> historiesNames = new List<CaseHistory>();
        List<CaseHistory> historiesIds = new List<CaseHistory>();       
        for (CaseHistory ch : histories){
            if (ch.Field == 'Owner'){
                if (String.valueOf(ch.NewValue).left(2) == '00')
                    historiesIds.add(ch);
                else
                    historiesNames.add(ch);
            }
        }
        
        Map<String, String> ownerMap = new Map<String, String>();
        for (Integer i=0; i<historiesIds.size(); i++){
            CaseHistory ch = historiesIds[i];
            CaseHistory ch2 = historiesNames[i];
            ownerMap.put(String.valueOf(ch.NewValue), String.valueOf(ch2.NewValue));
            ownerMap.put(String.valueOf(ch.OldValue), String.valueOf(ch2.OldValue));
        }
            
        Map<String, Case_Team_Report__c> reportsMap = new Map<String, Case_Team_Report__c>();
        Id lastUserId;
        
        for (CaseHistory ch : histories){
            
            if (String.valueOf(ch.NewValue).left(3) == '005'){
                String userId = String.valueOf(ch.NewValue);
                Case_Team_Report__c report = reportsMap.get(userId);
                
                if (report == null){
                    report = new Case_Team_Report__c();
                    report.Case__c = c.Id;
                    report.Reporting_Case__c = ch.CaseId;
                    report.Type__c = type;
                    report.Name__c = ownerMap.get(userId);
                    reportsMap.put(userId, report);
                }
                report.In_Progress_Start__c = ch.CreatedDate;
                report.In_Progress_End__c = null;
                
                lastUserId = userId;
            }
            
            if (String.valueOf(ch.OldValue).left(3) == '005'){
                Case_Team_Report__c report = reportsMap.get(String.valueOf(ch.OldValue));
                if (report != null){
                    report.In_Progress_End__c = ch.CreatedDate;
                    
                    if (report.Working_Days__c == null) report.Working_Days__c = 0;
                    
                    report.Working_Days__c += getBusinessDays(bh, report.In_Progress_Start__c, report.In_Progress_End__c);
                }
            }
            
            if (ch.Field == 'Status' && (String.valueOf(ch.NewValue) == 'Closed' || String.valueOf(ch.NewValue) == 'Cancelled')){
                Case_Team_Report__c report = reportsMap.get(lastUserId);
                if (report != null && report.In_Progress_End__c == null){
                    report.In_Progress_End__c = ch.CreatedDate;
                    
                    if (report.Working_Days__c == null) report.Working_Days__c = 0;
                    
                    report.Working_Days__c += getBusinessDays(bh, report.In_Progress_Start__c, report.In_Progress_End__c);
                }
            }
            
        }

        for (Case_Team_Report__c report : reportsMap.values()){
            //no in progress end date
            if (report.In_Progress_Start__c != null && report.In_Progress_End__c == null){
                report.In_Progress_End__c = Datetime.now();
                
                if (report.Working_Days__c == null) report.Working_Days__c = 0;
                report.Working_Days__c += getBusinessDays(bh, report.In_Progress_Start__c, report.In_Progress_End__c);
            }
        }
        
        return reportsMap.values();
    }   
    
}