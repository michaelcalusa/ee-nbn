/*------------------------------------------------------------------------
Author:        Andrew Zhang
Company:       NBNco
Description:   Test class for CaseTeamReportUtil
History
<Date>      <Authors Name>     <Brief Description of Change>
----------------------------------------------------------------------------*/

@isTest
private class CaseTeamReportUtil_Test {

    @isTest(SeeAllData=true) 
    static void testCreateReports(){
        
        List<CaseHistory> histories = [SELECT Id, CaseId FROM CaseHistory 
                                       WHERE Field = 'SME_Team__c' 
                                       AND Case.RecordType.Name IN ('Complex Query', 'Complex Complaint', 'Formal Complaint', 'Urgent Complaint') 
                                       LIMIT 10];
        
        List<Id> caseIds = new List<Id>();
        for (CaseHistory ch : histories){
            caseIds.add(ch.CaseId);
        }
        
        test.startTest();   
        
        if (caseIds.size() > 0){
            CaseTeamReportUtil.createReports(caseIds[0]);
        }
        
        if (caseIds.size() > 1){
            database.executeBatch(new BatchCalculateCaseTeamReports(null, new List<Id>{caseIds[1]}), 1);
        }
        
        //check Case Team Reports
        List<Case_Team_Report__c> reports = [SELECT Id FROM Case_Team_Report__c WHERE Case__c IN :caseIds];
        System.debug('====reports created: '+ reports.size());
      
        test.stopTest();        
        
    }
}