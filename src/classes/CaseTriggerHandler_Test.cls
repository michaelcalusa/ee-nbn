@isTest
private class CaseTriggerHandler_Test{
/*------------------------------------------------------------------------
Author:        SantoshRaoBompally
Company:       WIPRO Technologies 
Description:   Test class for CaseTriggerHandler Class
               1 - 
               
Class:           CaseTriggerHandler
History
<Date>            <Authors Name>    <Brief Description of Change> 
23rd-Nov-2015       Rohit Kumar       Modified test class to cover updateCaseReference method 
28-02-2018          Ganesh Sawant     Added test coverage for Additional Case History logic MSEU-5791
07-03-2018          Ganesh Sawant     Added test coverage for MAR Case updates MSEU-10397
19-03-2018          Dolly Yadav       Added code coverage or the  MSEU-10755
07-06-2018          Ganesh Sawant     Added code coverage for story MSEU-6038 
21-06-2018          Wayne Ni          Added code coverage for story MSEU-13842
09-07-2018          Shuo Li           Added code coverage or the  COMSTRUC-4586 
--------------------------------------------------------------------------*/    
    
     /***
        Test Case Closure Process
    ***/
    static testMethod void testCloseCase(){
    list<lead> testLeadList;
    list<Case> testCaseList;
    list<account> testAccountlist ; 
    list<user> testUserList ; 
    AccountTeamMember testAccountTeamMember ; 
    Test.startTest();
    testAccountlist = TestDataUtility.getListOfAccounts(1,true);
        Test.stopTest();
    ID Profileid = [select id from Profile where name = 'System Administrator'].id;
    system.debug(ProfileID);
  //  testUserList[0] = TestDataUtility.createTestUser(true,Profileid );
    User u = [select id,title, firstname, lastname, email, phone, mobilephone, fax, street, city, state, postalcode, country FROM User WHERE id =: UserInfo.getUserId()];
    
    testAccountTeamMember = new AccountTeamMember();
    testAccountTeamMember.AccountID = testAccountList[0].id ; 
    testAccountTeamMember.UserID = u.id ; // testUserList[0].id ; 
    testAccountTeamMember.AccountAccessLevel = 'Edit';
    
    insert testAccountTeamMember ; 
    
    /*testLeadList = TestDataUtility.createLeadTestRecords(1,false,'TestFirstName','TestLastName','TestCompany','Open');
    testCaseList = TestDataUtility.createCaseTestRecords(1,false); 
    testCaseList[0].recordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Credit Check').getRecordTypeId();
    testCaseList[0].AccountId = testAccountlist[0].id ;
    testcaseList[0].Lead__c = testLeadList[0].id; 
    insert testcaseList ;
    system.debug('========@@@2@@@@1111111111111'+[select status from case where id = :testcaseList     .get(0).Id]);

    case c = [select id,status,Outcome__c  from case where id = :testcaseList.get(0).Id];
    c.status = 'Completed';
    c.Outcome__c = 'Approved';
    update c; 
    system.debug('========@@@2@@@@'+[select status from case where id = :testcaseList.get(0).Id]);
    */
    
    case c = new case();
    c.recordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Credit Check').getRecordTypeId();
    c.status = 'Open';  
    insert c;
    
    
    c.status = 'Completed';
    c.Outcome__c = 'Approved';
    update c;
    
    
    }
    
    static testMethod void testAdditionalCaseHistory(){
        //Creating Case of 'Assurance Jeopardy'
        Case ajCase = new Case();
        ajCase.recordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Assurance Jeopardy').getRecordTypeId();
        ajCase.Origin = 'Email';
        ajCase.Type = 'Activations';
        insert ajCase;
        
        Case updateCase = [Select Id, SME_Case_Owner_User__c    From Case Where ID=: ajCase.ID];
        updateCase.SME_Case_Owner_User__c = UserInfo.getUserId();
        update updateCase;
        
        Case_History__c ch = [Select ID, Old_Value__c, New_Value__c From Case_History__c Where Case__c =: updateCase.ID];
        System.assertEquals(ch.Old_Value__c, null);
        User currentUser = [Select Id, Name From User Where ID =: UserInfo.getUserId()];
        System.assertEquals(ch.New_Value__c, currentUser.Name);
        
    }    

    static testMethod void testupdateMARCases(){
        site__c unverifiedSite = new site__c(Name='Test Site', RecordTypeID =schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId());
        insert unverifiedSite;
        Case marCase = new Case();
        marCase.RecordTypeID = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Medical Alarm').getRecordTypeId();
        marCase.Site__c = unverifiedSite.ID;
        marCase.Case_Type__c = 'Serviceable Call';
        marCase.Record_Provided_By__c = 'Testing MAR Case';
        marCase.Date_Provided__c = Date.Today();
        marCase.Copper_Active__c = 'Yes';
        marCase.NBN_Active__c = 'Yes';
        marCase.Inflight_Order__c = 'Yes';
        insert marCase;
        
        marCase.Status = 'In Progress';
        marCase.OwnerID = Userinfo.getUserId();
        marCase.MAR_Sub_Status__c = 'Serviceable Call Completed';
        marCase.NBN_UMA_Migration_Decision__c = 'Keep legacy alarm';
        //marCase.Case_Type__c = 'Courtesy Call';
        update marCase;
        
        Case marcaseUpdate = [Select Id, Serviceable_Call_Completed__c, Serviceable_Call_Completed_Date__c,NBN_UMA_Migration_Decision_Timestamp__c, status, MAR_Sub_Status__c,Case_Type__c From Case Where ID=: marCase.ID];
        System.assertEquals(marCaseUpdate.Serviceable_Call_Completed__c, TRUE);
        //System.assertEquals(marcaseUpdate.Serviceable_Call_Completed_Date__c, Datetime.now());
        //System.assertEquals(marcaseUpdate.NBN_UMA_Migration_Decision_Timestamp__c, Datetime.now());
        
        marcaseUpdate.Serviceable_Call_Completed__c = False;
        update marcaseUpdate;
        
        Case marcaseUpdate1 = [Select Id, Serviceable_Call_Completed__c, Serviceable_Call_Completed_Date__c,NBN_UMA_Migration_Decision_Timestamp__c, status, MAR_Sub_Status__c,Case_Type__c From Case Where ID=: marcaseUpdate.ID];
        system.debug('marcaseUpdate1.Serviceable_Call_Completed__c:' + marcaseUpdate1.Serviceable_Call_Completed__c);
        System.assertEquals(marcaseUpdate1.Serviceable_Call_Completed_Date__c, null);
       
    }   
    
    static testMethod void testupdateMARCasesFieldTrack(){
         site__c unverifiedSite = new site__c(Name='Test Site', RecordTypeID =schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId());
        insert unverifiedSite;
        Case marCase = new Case();
        marCase.RecordTypeID = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Medical Alarm').getRecordTypeId();
        marCase.Site__c = unverifiedSite.ID;
        marCase.Case_Type__c = 'Serviceable Call';
        marCase.Record_Provided_By__c = 'Testing MAR Case';
        marCase.Date_Provided__c = Date.Today();
        marCase.Copper_Active__c = 'Yes';
        marCase.NBN_Active__c = 'Yes';
        marCase.Inflight_Order__c = 'Yes';
        insert marCase;
        
        marCase.Status = 'In Progress';
        marCase.OwnerID = Userinfo.getUserId();
        marCase.MAR_Sub_Status__c = 'Serviceable Call Completed';
        marCase.NBN_UMA_Migration_Decision__c = 'Keep legacy alarm';
        marCase.Alarm_Category__c = 'LL';
        marCase.MAR_Power_Outage_Message_Provided__c = true;
        marCase.Has_the_address_been_verified_with_EU__c = true;
        marCase.Alarm_Tested_Verified_by_EU_Successfully__c = true;
        //marCase.Case_Type__c = 'Courtesy Call';
        update marCase;        
        
    } 
    
    
    //Test code for updating MAR secondary contact based on updates from SRBR file -- MSEU9001    
    @testSetup static void setup() {
        
        
        
    }
    /*
    @isTest static void testupdateSecondaryContactforMARCases_Positive() {
        
        Test.startTest();
        List<Contact> contactrecords = [SELECT Id, LastName FROM Contact where LastName LIKE 'TestContact%' ORDER BY LastName ];
        Case caserecord = [SELECT Id,ContactId,Primary_Contact_Role__c,Secondary_Contact__c,Secondary_Contact_Role__c FROM Case where Record_Provided_By__c='Testing MAR Case1'];
        caserecord.Secondary_Contact__c = contactrecords[2].Id;
        update caserecord;
        Case updCase = [SELECT Id,(SELECT Id,Contact__c,Role__c,Primary__c,Secondary__c FROM Case_Roles__r ORDER BY Secondary__c DESC) FROM Case WHERE Id=:caserecord.Id];
        Test.stopTest();
        System.assertEquals(contactrecords[2].Id, updCase.Case_Roles__r[0].Contact__c);
        System.assertEquals(True, updCase.Case_Roles__r[0].Secondary__c); 

    }
    
    


    @isTest static void testupdateSecondaryContactforMARCases_Negative() {
        
        Test.startTest();
        List<Contact> contactrecords = [SELECT Id, LastName FROM Contact where LastName LIKE 'TestContact%' ORDER BY LastName ];
        Case caserecord = [SELECT Id,ContactId,Primary_Contact_Role__c,Secondary_Contact__c,Secondary_Contact_Role__c FROM Case where Record_Provided_By__c='Testing MAR Case1'];
        Case_Contact__c cc = new Case_Contact__c(Contact__c = contactrecords[2].Id, Case__c = caserecord.Id, Role__c = 'Contractor', Secondary__c = False, Primary__c = False);
        insert cc;    
        caserecord.Secondary_Contact__c = contactrecords[2].Id;
        update caserecord;
        Case updCase = [SELECT Id,(SELECT Id,Contact__c,Role__c,Primary__c,Secondary__c FROM Case_Roles__r ORDER BY Secondary__c DESC) FROM Case WHERE Id=:caserecord.Id];
        Test.stopTest();
        //get the id of updated secondary contact
        Id newSecondaryid; 
        for(Case_Contact__c buffer: updCase.Case_Roles__r){
            if(buffer.Secondary__c == true){
                newSecondaryid = buffer.contact__c;
            }
            
        }        
        System.assertEquals(contactrecords[2].Id, newSecondaryid);
        System.assertEquals(True, updCase.Case_Roles__r[0].Secondary__c); 

    }    
    
    
    @isTest static void testupdateSecondaryContactforMARCases_RoleUpdateOnly() {
        
        Test.startTest();
        List<Contact> contactrecords = [SELECT Id, LastName FROM Contact where LastName LIKE 'TestContact%' ORDER BY LastName ];
        Case caserecord = [SELECT Id,ContactId,Primary_Contact_Role__c,Secondary_Contact__c,Secondary_Contact_Role__c FROM Case where Record_Provided_By__c='Testing MAR Case1'];
        caserecord.Secondary_Contact_Role__c = 'Contractor';
        update caserecord;
        Case updCase = [SELECT Id,(SELECT Id,Contact__c,Role__c,Primary__c,Secondary__c FROM Case_Roles__r ORDER BY Secondary__c DESC) FROM Case WHERE Id=:caserecord.Id];
        Test.stopTest();
        
        String updatedSecondaryRole;
        for(Case_Contact__c cc : updCase.Case_Roles__r){
            if(cc.Secondary__c == true){
                updatedSecondaryRole = cc.Role__c;
            }
        }
        
        system.assertEquals('Contractor', updatedSecondaryRole);
    } */
    
    static testMethod void testcreateComplexComplaintOnCorpAffairsEscalations() {
        
        list<Account> accInsertList = new list<Account>();
        list<Contact> conInsertList = new list<Contact>();
        list<Case> caseInsertList = new list<Case>();
        list<Case> cupdateList = new list<Case>();
        Account testAccount = new Account(Name='Test Institution',
                                         RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Institution').getRecordTypeId(),
                                         State__c='NSW');
        accInsertList.add(testAccount);
        Account householdAcc = new Account(Name='Test Household',
                                         RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId()
                                          ); 
        
        accInsertList.add(householdAcc);
        insert accInsertList;
        
        test.startTest();
        for(Account acc : accInsertList){
            if(Acc.Name == 'Test Institution'){
                Contact testCon = new Contact(FirstName = 'Corporate',
                                             LastName = 'Test MP',
                                             AccountId = acc.Id,
                                             RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Corporate Affairs').getRecordTypeId(),
                                             Email = 'test12345@test.com');
                conInsertList.add(testCon);                
            }
            else if(Acc.Name == 'Test Household'){
                Contact testExtCon = new Contact(FirstName = 'External',
                                             LastName = 'Test Contact',
                                             AccountId = acc.Id,
                                             RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId(),
                                             Email = 'test1@test.com'); 
                conInsertList.add(testExtCon);
            }
        }
        insert conInsertList;
        
        for(contact con : conInsertList){
            if(con.recordType.DeveloperName == 'Corporate_Affairs'){
            Case corporateAffairsCase = new Case(Subject = 'Test Corporate Case',
                                                 Description = 'Test Description',
                                                 RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Corporate Affairs').getRecordTypeId(),
                                                 ContactId = con.Id,
                                                 Primary_Contact_Role__c    = 'Government Representative',
                                                 Point_of_Authorised_Contact__c = 'Primary',
                                                 Escalation_Source__c = 'Senate',
                                                 origin = 'Email');  
                caseInsertList.add(corporateAffairsCase);
            }          
        }
        
        Case corporateAffairsCase1 = new Case(Subject = 'Test Corporate Case',
                                             Description = 'Test Description',
                                             RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Corporate Affairs').getRecordTypeId(),
                                             ContactId = conInsertList[0].Id,
                                             Primary_Contact_Role__c    = 'Government Representative',
                                             Secondary_Contact__c = conInsertList[1].Id,
                                             Secondary_Contact_Role__c = 'General Public',                                              
                                             Point_of_Authorised_Contact__c = 'Primary',
                                             Escalation_Source__c = 'Senate',
                                             origin = 'Email');
        caseInsertList.add(corporateAffairsCase1);
        insert caseInsertList;
        test.stopTest();
        String yourFiles = 'Lets assume this is your binary string of the files';
         
        ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
        conVer.PathOnClient = 'ionicLogo.png'; // The files name, extension is very important here which will help the file in preview.
        conVer.Title = 'Proposal '; // Display name of the files
        conVer.VersionData = EncodingUtil.base64Decode(yourFiles); // converting your binary string to Blog
        insert conVer;     
        
        // First get the content document Id from ContentVersion
        Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
         
        //Create ContentDocumentLink
        ContentDocumentLink cDe = new ContentDocumentLink();
        cDe.ContentDocumentId = conDoc;
        cDe.LinkedEntityId = caseInsertList[0].Id; // you can use objectId,GroupId etc
        cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
        cDe.Visibility = 'InternalUsers';
        insert cDe;        
        
                
        
        for(Case cupdate : caseInsertList){
            if(cupdate.Secondary_Contact__c == null){
                cupdate.status = 'Open - Escalated';
                cupdate.No_End_User_Contact_Available__c = TRUE;
                cupdateList.add(cupdate);
            }
            else if(cupdate.Secondary_Contact__c!=null){
                cupdate.status = 'Open - Escalated';                
            }
            cupdateList.add(cupdate);
        }
        update cupdateList;
        for(Case c : [Select id, Converted_To_Complex_Complaint__c From Case Where ID IN: cupdateList]){
            System.assertEquals(c.Converted_To_Complex_Complaint__c,TRUE);
        }
    }
    

    
    static testMethod void testAutoPopulateMPNameTestMethod() 
    {
        test.startTest();
        Id corpAffairConRecordId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Corporate Affairs').getRecordTypeId(); 
        Id corpAffairAccRecordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Institution').getRecordTypeId(); 
        Account testAcc = new Account(name='test corp affair dumping account',recordtypeid = corpAffairAccRecordId);
        insert testAcc;
        List<Contact> testConLists = new List<Contact>();
        contact testConTwo = new contact();
        testConTwo.RecordTypeId = corpAffairConRecordId;
        testConTwo.LastName = 'Test MP Contact two';
        testConTwo.AccountId = testAcc.id;
        testConTwo.Status__c = 'Active';
        testConTwo.Email = 'testConTwo@test.com';
        testConTwo.Contact_Type__c = 'Elected Representative';
        testConLists.add(testConTwo);
        insert testConLists;
        
        //Create Corp affair case
        Id corpAffairRecordId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Corporate Affairs').getRecordTypeId(); 
        case corpcase = new case();
        corpcase.recordtypeid = corpAffairRecordId;
        corpcase.Phase__c = 'Corporate Affairs';  
        corpcase.contactid = testConTwo.Id;
        corpcase.Origin = 'Email';
        corpcase.Primary_Contact_Role__c = 'General Public';
        corpcase.Subject  = 'test corp case';
        corpcase.Description = 'test';
        corpcase.Case_Manager_Status__c = 'Not Started';
        insert corpcase;
        
        Case testCorpcase = [select id,contactid,MP_Name__c from case where subject = 'test corp case' limit 1];
        Contact testcon2 = [select id,firstname,lastname from contact where LastName = 'Test MP Contact two' limit 1];
        
        testCorpcase.ContactId = testcon2.id;

        update testCorpcase;
        Case testCorpcaseAfterUpdate = [select id,contactid,MP_Name__c from case where subject = 'test corp case' limit 1];
        test.stopTest();
        
        string finalresultMP;
        if(testcon2.FirstName != null){
            finalresultMP = testcon2.FirstName + ' ' + testcon2.LastName;
        }else{
            finalresultMP = testcon2.LastName;  
        }
        system.debug('final result mp is '+finalresultMP);
        
        system.assertEquals(testCorpcaseAfterUpdate.MP_Name__c,finalresultMP);
    }    
}