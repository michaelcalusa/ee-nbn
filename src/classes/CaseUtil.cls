/*------------------------------------------------------------  
Author:        Andrew Zhang
Company:       NBNco
Description:   functions on beforeInsert and beforeUpdate of case, including validate LiFD Category, set case Entitlement and update case milestones' start dates
Test Class:    CaseUtil_Test
History
<Date>      <Authors Name>     <Brief Description of Change>
10-08-2017    Shuo Li           update do nothing function to remove the SOQL
set BH as static and only query when required
30-05-2017    Jairaj Jadhav    Added logic to calculate SLA Days for LASE Escalation Cases.
23-08-2017    Dilip Athley     Changed the record type to Commercial Billing.
12-09-2017    Ganesh Sawant    Commercial Billing Cases will be using VIC state Specific Business Hours and Holidays - BCM MSEU-901
3/10/2017     Dilip Athley     Changed the Record Type to Credit & Collections.
15-11-2017    Dolly Yadav      Changes for 5956 (Add recordType check in trigger for Customer Connection)
28-11-2017    Shubham Jaiswal  Added logic for Customer Connection Cases Site Creation
03-01-2018    Shubham Jaiswal  Added logic for Status Transition for Customer Connections Cases
12-03-2018    Shubham Jaiswal  MSEU-10336 - Added logic for relating Customer Connection Cases with Order
14-03-2018    Ganesh Sawant    MSEU-10343 - Added logic for Customer Connection assignment logic
16-05-2018    Justin Jacob     Added logic for ACT-15260
14-06-2018    Jairaj Jadhav    MSEU-10369 - Added logic to validate open tasks before closing ICT Channel Partner cases
------------------------------------------------------------*/

public without sharing Class CaseUtil{
    
    public static id agedOrderRecTypID = Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(); //Added by Dolly for Customer Connection
    //Added Disconnection Order Management Team by Ramya as part of ACT-17170 
    public static List<Group> custConnQueue = [Select id,name from group where type = 'Queue' and name IN ('Customer Connections Team', 'Right Second Time Team', 'Disconnection Order Management Team')];
  //public static Map<ID, Profile> ccprofilesMap = new Map<ID, Profile>([Select ID, Name From Profile Where Name IN ('NBN Right Second Time User','NBN Customer Connections User','NBN Customer Connections Team Leader','Customer Connection Integration User(Api only)')]);
    public static id taskRecTypID = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Task').getRecordTypeId(); //Added by Shubham for Act-16605
    public  static BusinessHours bh ; 
    public static BusinessHours bhvic;
    public static void getBusinessHours(){
        if (bh == null) {
            for (BusinessHours BusinessHour : [SELECT Id FROM BusinessHours WHERE IsDefault=true limit 1] ) {      
                bh = BusinessHour;
            }
        }
    }
    /*Credit & Collections Cases will be using VLC state Specific Business Hours and Holidays - BCM MSEU-901*/
    public static void getBusinessHoursVIC() {
        if(bhvic == null) {
            for(BusinessHours businessHour : [Select Id, Name From BusinessHours Where Name = 'Operations Business Hours - VIC']) {
                bhvic = businessHour;
            }
        }
    }
    
    public static void beforeInsertRules(){              
        //List<Case> cases = (List<Case>)Trigger.new;
        List<Case> cases = new List<Case>();
        List<Case> custConnectionCaseList = new List<Case>(); 
        Set<String> locIdSet = new Set<String>();
        Set<String> ordIdSet = new Set<String>(); //MSEU-10336
        
        for(Case cas: (List<Case>)Trigger.new){
            if(cas.recordTypeID != agedOrderRecTypID){
                system.debug('inside beforeInsertRules recordtype check==>');
                cases.add(cas);
            }
            else if(cas.recordTypeID == agedOrderRecTypID){
                //Modified as part of ACT-14786
               if(!cas.AgedOrder_OwnerAssignment__c && cas.Case_Owner_Profile__c!=null && (cas.Case_Owner_Profile__c=='NBN Right Second Time User' 
                             || cas.Case_Owner_Profile__c== 'NBN Right Second Time Team Leader')){ //Added 'NBN Right Second Time Team Leader' profile (ACT-14786)
                  cas.CC_Team__c = 'Right Second Time Team';
                }
                if(!cas.AgedOrder_OwnerAssignment__c && cas.Case_Owner_Profile__c!=null && (cas.Case_Owner_Profile__c=='NBN Customer Connections Team Leader' || cas.Case_Owner_Profile__c=='NBN Customer Connections User')){
                  cas.CC_Team__c = 'Customer Connections Team';
                }
		//Added by Shubham for ACT-17167 #Start
	                else if(cas.Case_Owner_Profile__c == Constants.NBN_DOM_Leader || cas.Case_Owner_Profile__c == Constants.NBN_DOM_User){
	                	cas.CC_Team__c = Constants.DOMTEAM;
	                }
                    //Added by Shubham for ACT-17167 #End
                if(!cas.AgedOrder_OwnerAssignment__c){
                    for(Group ccq : custConnQueue){
                        if(cas.CC_Team__c == ccq.Name)
                            cas.ownerID = ccq.ID;
                    }
                }
                if(cas.Loc_Id__c != null){
                    locIdSet.add(cas.Loc_Id__c);
                }
                if(cas.Order_Id__c != null){
                    ordIdSet.add(cas.Order_Id__c);
                }
                custConnectionCaseList.add(cas);
            }
        }
        if(!cases.isEmpty() && cases.size() > 0){
            system.debug('inside beforeInsertRules case list cases==>'+cases);
            assignBusinessHoursToBillingCases(cases);
            validateLiFDCategory(cases);
            setSLADate(cases);
        }
        
        if(!custConnectionCaseList.isEmpty() && custConnectionCaseList.size() > 0){
            if(!locIdSet.isEmpty()){
                associateCasetoSite(custConnectionCaseList,locIdSet);
            }
            if(!ordIdSet.isEmpty()){
                associateCasetoOrder(custConnectionCaseList,ordIdSet);
            }
        }
    }    
    
    public static void beforeUpdateRules(){    
        /****START - Added by Jairaj as part of US#MSEU-10369 for ICT****/
        list<id> toBeclosedCaseIDList = new list<id>();
        id ictCaseRcrdTypeID = Schema.SObjectType.case.getRecordTypeInfosByName().get('ICT Channel Partner').getRecordTypeId();
        for(Case cse: (List<Case>)trigger.new){
            if(cse.recordtypeid == ictCaseRcrdTypeID){
                case ictCase = (case)trigger.oldMap.get(cse.id);
                if(cse.isClosed && !ictCase.isClosed){
                    toBeclosedCaseIDList.add(cse.id);
                }
            }
        }
        if(!toBeclosedCaseIDList.isEmpty() && toBeclosedCaseIDList.size() > 0){
            validateOpenTasksOnICTCase(toBeclosedCaseIDList);
        }
        /****START - Added by Jairaj as part of US#MSEU-10369 for ICT****/
        //BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault=true];
        System.debug('====beforeUpdateRules BH: ' + bh );              
        getBusinessHours();
        System.debug('====beforeUpdateRules BH' + bh ); 
        Set<Case> casesToUpdateSLADate= new Set<Case>();    
        Map<Case, Case> casesToExtendSLADate= new Map<Case, Case>();
        
        List<Case> custConnectionCaseList = new List<Case>();
        Set<String> locIdSet = new Set<String>(); 
        Set<String> ordIdSet = new Set<String>(); 
        
        for (Id caseId : Trigger.newMap.keySet()){ 
            case rectypechk = (Case)Trigger.newMap.get(caseId);
            Case newCase = (Case)Trigger.newMap.get(caseId);
            Case oldCase = (Case)Trigger.oldMap.get(caseId);
            Boolean caseSelected = false;
            
            if(rectypechk.recordTypeID != agedOrderRecTypID){    
                
                if(newCase.Status != oldCase.Status && newCase.Status == 'Closed' && newCase.SME_Team__c != null && (newCase.SME_Resolution_Status__c == 'Not Assigned' || newCase.SME_Resolution_Status__c == 'Assigned' || newCase.SME_Resolution_Status__c == 'In Progress'))
                {
                    //newCase.SME_Resolution_Status__c = 'Activities Complete';
                    //newCase.SME_Resolution_Reason__c = 'Resolution Met';
                }
                
                //if no SLA date, update sla date
                if (newCase.SLA_Date__c == null)
                    casesToUpdateSLADate.add(newCase);
                
                if (newCase.Request_Received_Date__c == null)
                    newCase.Request_Received_Date__c = newCase.CreatedDate;        
                
                //if request received date changed, change sla date   
                if (newCase.Request_Received_Date__c != oldCase.Request_Received_Date__c)
                    casesToUpdateSLADate.add(newCase);
                
                //if recordtype change, change sla date
                if (newCase.RecordTypeId != oldCase.RecordTypeId)
                    casesToUpdateSLADate.add(newCase);
                Id escRecID = Schema.SObjectType.case.getRecordTypeInfosByName().get('LASE Escalation').getRecordTypeId();
                
                //if LASE Escalation case's Escalation type changes to 'Propose frustrated premises' then SLA clock should move to 100 days
                if (newCase.RecordTypeId == escRecID && newCase.Escalation_Type__c != oldCase.Escalation_Type__c && newCase.Escalation_Type__c == 'Propose frustrated premises' && (newCase.Status != 'Closed' || newCase.Status != 'Cancelled')){
                    Customer_Service_Setting__c css = Customer_Service_Setting__c.getOrgDefaults();
                    if(newCase.Request_Received_Date__c != null){
                        newCase.SLA_Days__c = (css != null && css.LASE_Escalation_Propose_Frustrated_Days__c != null) ? css.LASE_Escalation_Propose_Frustrated_Days__c: 100;
                        newCase.SLA_Date__c = BusinessHours.add(bh.Id, newCase.Request_Received_Date__c, (Long)(newCase.SLA_Days__c * 60 * 60 * 8 * 1000));
                        
                    }
                }
                
                //if case change from closed to re-open, extend the SLA Date by adding remaining days
                if (newCase.Status != oldCase.Status && (oldCase.Status == 'Closed' || oldCase.Status == 'Cancelled') && newCase.Status == 'Re-Opened'){
                    if (oldCase.SLA_Date__c == null){
                        Case tempCase= new Case(RecordTypeId = newCase.RecordTypeId);
                        casesToUpdateSLADate.add(tempCase);
                        casesToExtendSLADate.put(tempCase, newCase);
                    }else{
                        casesToExtendSLADate.put(oldCase, newCase);
                    }
                }
                
            }
            
            else if (rectypechk.recordTypeID == agedOrderRecTypID){
                if(newCase.Loc_Id__c != null && newCase.Loc_Id__c != oldCase.Loc_Id__c){
                    locIdSet.add(newCase.Loc_Id__c);
                    custConnectionCaseList.add(newCase);
                    caseSelected = true;
                }
                if(newCase.ownerId != null && newCase.ownerId != oldCase.ownerId){
                    if(newCase.Case_Owner_Profile__c!=null && (newCase.Case_Owner_Profile__c=='NBN Right Second Time User' || newCase.Case_Owner_Profile__c=='Right Second Time Team' 
                                                                    || newCase.Case_Owner_Profile__c == 'NBN Right Second Time Team Leader')){
                                                                        //Added 'NBN Right Second Time Team Leader' profile(ACT-14786)
                      newCase.CC_Team__c = 'Right Second Time Team';      
                    }
                    if(newCase.Case_Owner_Profile__c!=null && (newCase.Case_Owner_Profile__c=='NBN Customer Connections Team Leader' || newCase.Case_Owner_Profile__c=='NBN Customer Connections User' || newCase.Case_Owner_Profile__c=='Customer Connections Team')){
                      //newCase.CC_Team__c = 'Customer Connections Team';      
                    }   
//Added by Shubham for ACT-17167 #Start
                    else if(newCase.Case_Owner_Profile__c != null && (newCase.Case_Owner_Profile__c == Constants.NBN_DOM_User
					|| newCase.Case_Owner_Profile__c == Constants.DOMTEAM
					|| newCase.Case_Owner_Profile__c == Constants.NBN_DOM_Leader)){
                    	newCase.CC_Team__c = Constants.DOMTEAM;	    
                    }
                    //Added by Shubham for ACT-17167 #End
                }
                if(newCase.ownerId != null && newCase.ownerId != oldCase.ownerId && 
                   string.ValueOf(oldCase.ownerId).startsWith('00G') && string.ValueOf(newCase.ownerId).startsWith('005'))
                   
                {
        //Added by Justin Jacob - START ACT -15260
                if(!(newCase.Case_Owner_Profile__c==Constants.NBNCCTLeader 
          || newCase.Case_Owner_Profile__c==constants.NBNCCUser||newCase.Case_Owner_Profile__c==constants.NBNR2TLeader
          ||newCase.Case_Owner_Profile__c==constants.NBNR2TUser || newCase.Case_Owner_Profile__c == Constants.Right_Second_Time_Team ||
                    newCase.Case_Owner_Profile__c == Constants.CUSTOMER_CONNECTIONS_TEAM)){
                newCase.Status = constants.Assigned; //Added by Justin Jacob - END ACT -15260
                }}
                if(newCase.Order_Id__c != oldCase.Order_Id__c){
                    if(newCase.Order_Id__c != null){
                        ordIdSet.add(newCase.Order_Id__c);
                    }
                    if(!caseSelected){
                        custConnectionCaseList.add(newCase);
                    }
                }
            }            
            
        }        
        
        if(!custConnectionCaseList.isEmpty() && custConnectionCaseList.size() > 0){
            if(!locIdSet.isEmpty()){
                associateCasetoSite(custConnectionCaseList,locIdSet);
            }
            if(!ordIdSet.isEmpty()){
                associateCasetoOrder(custConnectionCaseList,ordIdSet);
            }
        }
        
        validateLiFDCategory((List<Case>)Trigger.new);
        
        if (casesToUpdateSLADate.size() >0){
            setSLADate(new List<Case>(casesToUpdateSLADate));   
        }
        
        for (Case oldCase : casesToExtendSLADate.keySet()){
            Id bcRecID = Schema.SObjectType.case.getRecordTypeInfosByName().get('Credit & Collections').getRecordTypeId();
            Case newCase = casesToExtendSLADate.get(oldCase);
            Long remainingMs = getRemainingMs(bh, oldCase);
            if (remainingMs > 0)
                if(newCase.RecordTypeID == bcRecID && newCase.BusinessHoursID != null) {
                    newCase.SLA_Date__c = BusinessHours.add(newCase.BusinessHoursID, Datetime.now(), remainingMs);
                }
            else {
                newCase.SLA_Date__c = BusinessHours.add(bh.Id, Datetime.now(), remainingMs);  
            }          
        }    
        
    }
    
    /**************************************
    Author:        Jairaj Jadhav
    Company:       Wipro Technologies
    Description:   functions on beforeInsert and beforeUpdate of case to validate open tasks before closing ICT Channel Partner cases
    History
    <Date>      <Authors Name>     <Brief Description of Change>
    14-06-2018    Jairaj Jadhav    MSEU-10369 - Added logic to validate open tasks before closing ICT Channel Partner cases
    **************************************/
    public static void validateOpenTasksOnICTCase(list<id> toBeclosedCaseIDList){
        List<id> openTaskCaseIDList = new List<id>();
        for(task accrdtTask: [select id,status,whatid from task where whatid IN: toBeclosedCaseIDList AND status!='Closed']){
            if(!openTaskCaseIDList.contains(accrdtTask.whatid) && !test.isRunningTest()){
                trigger.newMap.get(accrdtTask.whatid).addError(label.ICT_Open_Tasks_Error_Message_for_Case);
                openTaskCaseIDList.add(accrdtTask.whatid);
            }
        }
    }
    
    public static Long getRemainingMs(BusinessHours bh, Case c){
        Id bcRecID = Schema.SObjectType.case.getRecordTypeInfosByName().get('Credit & Collections').getRecordTypeId();
        if (bh == null) {
            System.debug('====getRemainingMs BH: ' + bh );              
            getBusinessHours();
            System.debug('====getRemainingMsAfter BH' + bh ); 
        }
        if (c.SLA_Date__c == null) return 0;
        
        if (c.Status == 'Closed' || c.Status == 'Cancelled'){
            if (c.ClosedDate == null) return 0;
            if(c.RecordTypeID == bcRecID && c.BusinessHoursID != null) {
                return BusinessHours.diff(c.BusinessHoursID, c.ClosedDate, c.SLA_Date__c);
            }
            else {            
                return BusinessHours.diff(bh.Id, c.ClosedDate, c.SLA_Date__c);
            }
        }else{
            if(c.RecordTypeID == bcRecID && c.BusinessHoursID != null) {
                return BusinessHours.diff(c.BusinessHoursId, Datetime.now(), c.SLA_Date__c);
            }
            else {
                return BusinessHours.diff(bh.Id, Datetime.now(), c.SLA_Date__c);
            }
        }
    }    
    
    private static void validateLiFDCategory(List<Case> cases){    
        for (Case c: cases){                           
            //if two LiFD Categories, one must be Notes           
            if (c.LiFD_Category__c != null && c.recordTypeID != agedOrderRecTypID){
                Set<String> categorySet = new Set<String>(c.LiFD_Category__c.split(';'));
                if (categorySet.size() > 2 ||
                    (categorySet.size() == 2 && !categorySet.contains('Notes'))){
                        c.addError(label.Case_LiFD_Category_Notes_Error_Message);
                    }
            }
            
        }     
    }
    
    private static void setSLADate(List<Case> cases){    
        
        //BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault=true];
        System.debug('====setSLADate BH: ' + bh );              
        getBusinessHours();
        System.debug('====setSLADateAfter BH' + bh ); 
        Id bcRecID = Schema.SObjectType.case.getRecordTypeInfosByName().get('Credit & Collections').getRecordTypeId();
        Customer_Service_Setting__c css = Customer_Service_Setting__c.getOrgDefaults();
        
        Map<String,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosByName();
        Map<Id, Decimal> slaDaysMap = new Map<Id, Decimal>();
        slaDaysMap.put(rtMap.get('Complex Query').getRecordTypeId(), getSLADays(css, 'Complex Query'));
        slaDaysMap.put(rtMap.get('Complex Complaint').getRecordTypeId(), getSLADays(css, 'Complex Complaint'));
        slaDaysMap.put(rtMap.get('Formal Complaint').getRecordTypeId(), getSLADays(css, 'Formal Complaint'));
        slaDaysMap.put(rtMap.get('Urgent Complaint').getRecordTypeId(), getSLADays(css, 'Urgent Complaint'));
        //Changed the Record Type name. - Dilip Athley
        slaDaysMap.put(rtMap.get('Credit & Collections').getRecordTypeId(), getSLADays(css, 'Credit & Collections'));
        slaDaysMap.put(rtMap.get('LASE Escalation').getRecordTypeId(), getSLADays(css, 'LASE Escalation'));
        for (Case c: cases){                           
            if (slaDaysMap.containsKey(c.recordTypeId) && c.Request_Received_Date__c != null){
                c.SLA_Days__c = slaDaysMap.get(c.recordTypeId);
                /**Credit & Collections Cases will be using VIC state Specific Business Hours and Holidays - BCM MSEU-901**/
                if(c.RecordTypeID == bcRecID && c.BusinessHoursID!=null) {
                    c.SLA_Date__c = BusinessHours.add(c.BusinessHoursID, c.Request_Received_Date__c, (Long)(c.SLA_Days__c * 60 * 60 * 8 * 1000) ); 
                }
                else {
                    c.SLA_Date__c = BusinessHours.add(bh.Id, c.Request_Received_Date__c, (Long)(c.SLA_Days__c * 60 * 60 * 8 * 1000) ); 
                }
            }
        }     
    }
    
    private static Decimal getSLADays(Customer_Service_Setting__c css, String recordTypeName){
        if (recordTypeName == 'Complex Query')
            return (css != null && css.Complex_Query_SLA_Days__c != null) ? css.Complex_Query_SLA_Days__c : 20;
        
        else if (recordTypeName == 'Complex Complaint')
            return (css != null && css.Complex_Complaint_SLA_Days__c != null) ? css.Complex_Complaint_SLA_Days__c : 20;
        
        else if (recordTypeName == 'Formal Complaint')
            return (css != null && css.Formal_Complaint_SLA_Days__c != null) ? css.Formal_Complaint_SLA_Days__c : 5;
        
        else if (recordTypeName == 'Urgent Complaint')
            return (css != null && css.Urgent_Complaint_SLA_Days__c != null) ? css.Urgent_Complaint_SLA_Days__c : 20;
        //Changed the Record Type name. - Dilip Athley
        else if (recordTypeName == 'Credit & Collections')
            return (css != null && css.Billing_NDE_SLA_Days__c != null) ? css.Billing_NDE_SLA_Days__c : 10;
        else if (recordTypeName == 'LASE Escalation')
            return (css != null && css.LASE_Escalation_Days__c != null) ? css.LASE_Escalation_Days__c : 20;
        else
            return 0;    
    }
    
    @InvocableMethod(label='Do nothing' description='To be called from Process builder.')
    public static void doNothing(List<ID> ids) {
        for (Id caseid :ids) {
            System.debug('====case: ' + caseid );
        }
    }
    
    public static void assignBusinessHoursToBillingCases(List<Case> cases) {
        string recordtypename;
        getBusinessHoursVIC();
        for(Case newCase : Cases) {
            recordtypename = Schema.SObjectType.Case.getRecordTypeInfosById().get(newCase.recordtypeid).getName();
            if(recordtypename == 'Credit & Collections') {
                if(bhvic!=null) {
                    newCase.BusinessHoursID = bhvic.Id; 
                }
            }
        }
    }
    
    public static void associateCasetoSite(List<Case> caseList, Set<String> locIdList){
        
        Id unverRecTypeID = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId();
        Id verRecTypeID = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Verified').getRecordTypeId();
        Set<Id> recordTypeSet = new Set<Id>{unverRecTypeID,verRecTypeID};
        Map<String,Id> mapLocIdtoSiteId = new Map<String,Id>();
        Map<String,Site__c> newSiteMap = new Map<String,Site__c>();
        
        for(Site__c st :[Select id,Location_Id__c,recordTypeId from Site__c where Location_Id__c IN :locIdList AND recordTypeId IN :recordTypeSet ]){
           mapLocIdtoSiteId.put(st.Location_Id__c,st.id);
        }
        
        for(Case cs:caseList){
            if(mapLocIdtoSiteId.containsKey(cs.Loc_id__c) ){
                cs.Site__c = mapLocIdtoSiteId.get(cs.Loc_id__c);
            }
            else {
                if(Pattern.matches('[L]{1}[O]{1}[C]{1}[0-9]{12}',cs.Loc_Id__c)){
                
                    if(newSiteMap.containsKey(cs.Loc_id__c)){                   
                        cs.Site__r = newSiteMap.get(cs.Loc_id__c);
                    }
                    else{
                        Site__c tempSite = new Site__c(Location_Id__c = cs.Loc_id__c,Name = cs.Loc_id__c,recordtypeid = unverRecTypeID);
                        cs.Site__r = tempSite;
                        newSiteMap.put(cs.Loc_id__c,tempSite);
                    }
                    
                 }
                 else{
                     cs.addError('Please enter a valid Location Id. The correct Location Id format is LOCXXXXXXXXXXXX where X is any number between 0-9.');
                 }
            }
        }
        
        if(newSiteMap.size() > 0){
            insert newSiteMap.values();
        }
        
        for(Case cs:caseList){
            if(cs.Site__r != null){
                cs.Site__c = cs.Site__r.id;
            }
        }
    }

    public static void associateCasetoOrder(List<Case> caseList, Set<String> ordIdList){
        
        Map<String,Id> mapOrderIdtoId = new Map<String,Id>();
        Map<String,Customer_Connections_Order__c> newOrderMap = new Map<String,Customer_Connections_Order__c>();
        
        for(Customer_Connections_Order__c ord :[Select id,order_Id__c from Customer_Connections_Order__c where order_Id__c IN :ordIdList]){
           mapOrderIdtoId.put(ord.order_Id__c,ord.id);
        }
        
        for(Case cs:caseList){
        
            if(mapOrderIdtoId.containsKey(cs.order_id__c) ){
                cs.CC_Order__c = mapOrderIdtoId.get(cs.order_id__c);
            }
            else if(cs.order_id__c == null){
                cs.CC_Order__c = null;
            }
        }
    }
    //Added by Shubham for ACT-15535 #Start
    public static void afterInsert(List<Case> newCases){
        List<Customer_Connections_Order__c> listOrderToUpdate = new List<Customer_Connections_Order__c>();
        for(Case cs : newCases){
            if(cs.recordTypeID == agedOrderRecTypID){
                if(cs.CC_Team__c == Constants.Right_Second_Time_Team && cs.CC_Order__c != null){
                    Customer_Connections_Order__c ord = new Customer_Connections_Order__c(Id = cs.CC_Order__c, IsRelatedCaseR2T__c = true,Last_Status_Change_Date__c = Datetime.now());
                    //Added by Vipul for ACT-15536 #Start
                    if(cs.Order_Status__c == Constants.Acknowledged_Held || cs.Order_Status__c==Constants.Inprogress_Held || cs.Order_Status__c==Constants.Held){
                        ord.Held_Status_Number__c=1;
                        ord.InProgress_Status_Number__c=0;
                        ord.Pending_Status_Number__c=0;
                    }
                    else if(cs.Order_Status__c == Constants.Acknowledged_Pending || cs.Order_Status__c==Constants.Pending || cs.Order_Status__c==Constants.Inprogress_Pending){
                        ord.Held_Status_Number__c=0;
                        ord.InProgress_Status_Number__c=0;
                        ord.Pending_Status_Number__c=1;
                    }
                    else if(cs.Order_Status__c == Constants.Inprogress){
                        ord.Held_Status_Number__c=0;
                        ord.InProgress_Status_Number__c=1;
                        ord.Pending_Status_Number__c=0;
                    }
                    //END- Vipul
                    listOrderToUpdate.add(ord);
                }
            }
        }
        if(listOrderToUpdate.size() > 0)
            update listOrderToUpdate;
    }
    //Added by Shubham for ACT-15535 #End
    //added by justin for ACT-15260 #Start
    public static void isbeforeUpdate(List<Case> newList, Map<Id,Case>oldMap){
        
        for(Case cs: newList){
          if( cs.Case_Owner_Profile__c!=null && (cs.Case_Owner_Profile__c==Constants.NBNCCTLeader 
          || cs.Case_Owner_Profile__c==constants.NBNCCUser||cs.Case_Owner_Profile__c==constants.NBNR2TLeader
          ||cs.Case_Owner_Profile__c==constants.NBNR2TUser || cs.Case_Owner_Profile__c == Constants.Right_Second_Time_Team ||
                    cs.Case_Owner_Profile__c == Constants.CUSTOMER_CONNECTIONS_TEAM) &&oldMap.get(cs.id).Status==constants.New1  && cs.ownerID!=oldmap.get(cs.id).ownerID ){
                  cs.Status=constants.Assigned; 
            
            }
            system.debug('*******#####'+cs.Case_Owner_Profile__c);
           for(Group ccq1 : custConnQueue){
                if(cs.Status==constants.Assigned && cs.ownerID == ccq1.ID && cs.ownerID!=oldmap.get(cs.id).ownerID ){
                      cs.Status=constants.New1;          
                 }                
            }
        } 
    }
    //added by Justin for ACT-15260 #End
//Added by Shubham for ACT-16605 #Start
    public static void afterUpdate(Map<Id, Case> oldCaseMap, Map<Id, Case> newCaseMap){
          List<String> applicableOrderStatuses = new List<String>{Constants.Held,Constants.Inprogress_Held};
      List<Task> tasksList = new List<Task>();
      List<Task> TasksToBeUpdated = new List<Task>();
      List<Task> taskObjList = new List<Task>();
        List<case> caseList = new List<case>();
      Task newTask;
      String Subject;
    Map<Id, Datetime> caseToPRDMap = new Map<Id, Datetime>();
    Date duedate;
    String subType;
    
        for(case cs: newCaseMap.values()){
            if(cs.recordTypeID == agedOrderRecTypID){
              caseList.add(cs);      
            }
        }
        if(caseList.size()>0){
            for(Case cs : [SELECT cc_Order__c, cc_Order__r.Plan_Remediation_Date__c 
                           FROM Case 
                           WHERE ID IN :newCaseMap.keySet() 
                           AND cc_Order__c != null
                           AND recordTypeID =: agedOrderRecTypID
                           AND CC_Team__c =: Constants.Right_Second_Time_Team]){
                if(!caseToPRDMap.containsKey(cs.Id)){
                    caseToPRDMap.put(cs.Id, cs.cc_Order__r.Plan_Remediation_Date__c);
                }
            }
            //Modified By Vipul -ACT-16605 - START
            Map<Id,List<Task>> CaseAndRelatedTasksMap = new Map<Id,List<Task>>();
            for(Task tsk:[Select Id,whatId,outcome__c,Subject from Task where whatId in: newCaseMap.keyset() and outcome__c !=: Constants.COMPLETED]){
                if(!CaseAndRelatedTasksMap.containsKey(tsk.whatId)){
                    CaseAndRelatedTasksMap.put(tsk.WhatId,new List<Task>{tsk});  
                
                }
                else{
                    CaseAndRelatedTasksMap.get(tsk.WhatId).add(tsk);
                }
            }
            //Modified By Vipul -ACT-16605 - END
            for(Case newCase : caseList){
                if(newCase.recordTypeID == agedOrderRecTypID && newCase.CC_Team__c == Constants.Right_Second_Time_Team
                   && newCase.CC_Order__c != null && applicableOrderStatuses.contains(newCase.Order_Status__c)){
                    if(String.isBlank(newCase.Resolver_Group__c) && caseToPRDMap.get(newCase.Id) == null 
                    && string.ValueOf(oldCaseMap.get(newCase.Id).ownerId).startsWith('00G') && string.ValueOf(newCase.ownerId).startsWith('005')){
                        newTask = createTask(Constants.AWAITING_TRIAGE,newCase,Constants.INTERNAL,addBusinessDays(Date.today(), 1));
                        tasksList.add(newTask);
                    }
                    //Ramya Act-16605#Start
                    if(String.valueof(newCase.ownerId).startsWith('005') && newCase.Resolver_Group__c != null && 
                            oldCaseMap.get(newCase.id).Resolver_Group__c != newCase.Resolver_Group__c){
                            if(newCase.Resolver_Group__c.startsWith(Constants.EXTERNAL)){
                                subType = Constants.EXTERNAL;
                            }
                            else{
                                subType = Constants.INTERNAL;
                            }  
                            //Modified as part of ACT-17565 
                            if(caseToPRDMap.get(newCase.Id) == null){
                                if(!CaseAndRelatedTasksMap.isEmpty()){
                                    for(Task tsk:CaseAndRelatedTasksMap.get(newCase.Id)){
                                        if(tsk.Subject == Constants.AWAITING_TRIAGE || tsk.Subject.startsWith(Constants.RemInProgPRDAvail)){ //Modified as part of ACT-17565
                                            Task tasktoBeUpdated = new Task(id=tsk.id,outcome__c = Constants.COMPLETED);
                                            TasksToBeUpdated.add(tasktoBeUpdated);
                                        
                                        }
                                    }
                                }
                                subject = Constants.REMEDIATIONNOPRD + newCase.Resolver_Group__c;
                                duedate = addBusinessDays(system.today(),2);                            
                                tasksList.add(createTask(subject,newCase,subType,duedate));
                            }else if(caseToPRDMap.get(newCase.Id) != null){ //Modified as part of ACT-17565
                                if(!CaseAndRelatedTasksMap.isEmpty()){
                                    for(Task tsk:CaseAndRelatedTasksMap.get(newCase.Id)){
                                        if(tsk.Subject.startsWith(Constants.RemInProgPRDAvail)){
                                            Task tasktoBeUpdated = new Task(id=tsk.id,outcome__c = Constants.COMPLETED);
                                            TasksToBeUpdated.add(tasktoBeUpdated);
                                        
                                        }
                                    }
                                }
                                subject = Constants.RemInProgPRDAvail +' '+ newCase.Resolver_Group__c;
                                duedate = Date.valueof(caseToPRDMap.get(newCase.Id));
                                tasksList.add(createTask(subject,newCase,subType,duedate));
                            }
                    } 
                    //Ramya Act-16605#End
                    
                
                }
        }
        }
      if(TasksToBeUpdated.size()>0){
        
        update TasksToBeUpdated;
      }
      //Modified By Vipul -ACT-16605 - END
      if(tasksList.size() > 0){
        insert tasksList;
      }
    
    }
    
    public static Date addBusinessDays(Date startDate, Integer iDays)
  {
      Integer businessDaysAdded = 0;
      Date currentDate = startDate;
      while(businessDaysAdded < iDays) {
          currentDate = currentDate.addDays(1);
          Datetime d = datetime.newInstance(currentDate.year(), currentDate.month(),currentDate.day());
  
          if (d.format('E') != 'Sat' && d.format('E') != 'Sun' && checkIfWorkingDay(currentDate)) {
              // it's a business day, so add 1 to the counter that works towards the amount of days to add
              businessDaysAdded = businessDaysAdded + 1;
          } 
      }       
      return currentDate;
  }
  
  public static boolean checkIfWorkingDay(Date currentDate){
      Date weekStart  = currentDate.toStartofWeek();
      if(weekStart.daysBetween(currentDate) ==0 || weekStart.daysBetween(currentDate) == 6){
          return false;
      } else 
          return true;
  } 
    //Added by Shubham for ACT-16605 #End
    //Added by Ramya for ACT-16605#Start
    public static Task createTask(String subject,case CaseObj,String subType,Date duedate){ 
      Task taskObj = new Task();
      taskObj.RecordTypeId = taskRecTypID;  
      taskObj.Sub_type__c = subType;
      taskObj.due_date__c =  duedate;  
      taskObj.Priority = Constants.PRIORITYGENERAL;
      taskObj.Outcome__c = Constants.OpenValue;
      taskObj.Type_Custom__c = Constants.CONTACT;
      taskObj.subject = subject;
      taskObj.OwnerId = caseObj.ownerId;
      taskObj.whatId = caseObj.id;   
      return taskObj;     
    }
    //Added by Ramya for ACT-16605#End
}