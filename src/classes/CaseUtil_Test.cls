/*------------------------------------------------------------------------
Author:        Andrew Zhang
Company:       NBNco
Description:   Test class for CaseUtil
History
<Date>      <Authors Name>     <Brief Description of Change>
30-05-2017    Jairaj Jadhav    To cover code added to calculate SLA Days for LASE Escalation Cases.
19-10-2017    Ganesh Sawant    Covered the Case before insert Logic of setting up vic bussiness hours
                               for Credit & Collections Team
----------------------------------------------------------------------------*/

@isTest
private class CaseUtil_Test {

    @isTest(SeeAllData=true) 
    static void testSetEntitlement(){

        
        //Setup data
        Account account = new Account(
            Name = 'Test Account',
            RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
            Tier__c = '1',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert account;
        
        Contact contact = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId(),
            AccountId = account.Id,
            Email = 'teste@example1.com',
            Preferred_Phone__c = '0288889999'
        );
        insert contact;
        
        Case caseObj = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            //RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Complex Complaint').getRecordTypeId(),
            RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId(),
            Phase__c = 'Information',
            Category__c = '(I) Communications',
            Sub_Category__c = 'Discovery Centre',
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
            ContactId = contact.Id,
            Primary_Contact_Role__c = 'General Public'
        );
        insert caseObj;
        
       /* Case escCaseObj = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('LASE Escalation').getRecordTypeId(),
            Escalation_Type__c = 'Objection',
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
            ContactId = contact.Id
        );
        insert escCaseObj;
        escCaseObj.Escalation_Type__c = 'Propose frustrated premises';
        update escCaseObj;
        escCaseObj.status = 'Closed';
        update escCaseObj;
        escCaseObj.status = 'Re-Opened';
        update escCaseObj;*/
        test.startTest();   
        
        //check Case has SLA Date
        Case c= [SELECT Id, SLA_Date__c FROM Case WHERE Id = :caseObj.Id];
        //System.assert(c.SLA_Date__c != null);
        
        test.stopTest();        
        
    }


    @isTest(SeeAllData=true) 
    static void testCreateTaskForCaseOwner(){
                
        //Setup data
        Account account = new Account(
            Name = 'Test Account',
            RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
            Tier__c = '1',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert account;
        
        Contact contact = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId(),
            AccountId = account.Id,
            Email = 'teste@example2.com',
            Preferred_Phone__c = '0288889999'
        );
        insert contact;
        
        Case caseObj = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId(),
            Phase__c = 'Information',
            Category__c = '(I) Communications',
            Sub_Category__c = 'Discovery Centre',
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
            ContactId = contact.Id,
            Primary_Contact_Role__c = 'General Public'
        );
        insert caseObj;
        
        
        
        test.startTest();   
        
        Profile caseManagerProfile = [SELECT Id FROM Profile WHERE Name = 'NBN Case Manager User' LIMIT 1];        
        User caseManager = TestDataUtility.createTestUser(false, caseManagerProfile.Id);
        insert caseManager;
        
        caseObj.OwnerId = caseManager.Id;
        caseObj.SME_Resolution_Status__c = 'In Progress';
        caseObj.SME_Resolution_Reason__c = 'Escalated';
        update caseObj;       
        
        //check Email sent        
        System.debug('===Email sent: ' + Limits.getEmailInvocations());
        
        test.stopTest();        
        
    }        
    
    static testMethod void testValidateLiFDCategory(){
        
        //Setup data
        Account account = new Account(
            Name = 'Test Account',
            RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
            Tier__c = '1',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert account;
        
        Contact contact = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('Customer Contact').getRecordTypeId(),
            AccountId = account.Id,
            Email = 'teste@example3.com',
            Preferred_Phone__c = '0288889999'
        );
        insert contact;
        
        Site__c site = new Site__c(
            Name = 'test site'
        );
        insert site;
        
        
        test.startTest();   
        
        //Check insert LiFD case with 2 LiFD Categories
        try{
            
            Case caseObj = new Case(
                Subject = 'Test Case',
                Description = 'Test Case',
                RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId(),
                LiFD_Category__c = 'HFC - Brownfields;LiFD Deferral Requested',
                Origin = 'Email',
                Status = 'Closed',
                Priority = '1-ASAP',
                ContactId = contact.Id
            );
            insert caseObj;
            
        }catch(Exception e){
            System.Assert(e.getMessage().contains(label.Case_LiFD_Category_Notes_Error_Message));
        }
        
        
        test.stopTest();        
    }  
    
    @isTest(SeeAllData=true)    
    Static void testassignBusinessHoursToBillingCases() {
    
    //Setup data
        Account account1 = new Account(
            Name = 'Test Account',
            RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
            Tier__c = '2',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert account1;
        
        
     Contact contact = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId(),
            AccountId = account1.Id,
            Email = 'teste@example4.com',
            Preferred_Phone__c = '0288889999'
        );
        insert contact;
        //Creating Case of record type 'Credit & Collections'
        test.startTest();
        Case cncCase = new Case(
                        Subject = 'Test Case billing123',
                Description = 'Test Case',
                RecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Query').getRecordTypeId(),
                Origin = 'Email',
                Status = 'Open',
                Priority = '3-General',
                Phase__c = 'Credit & Collections',
                Category__c = 'Commercial Works',
                Sub_Category__c = 'Application Withdrawn',
                Invoice_number__c = '123456789',
                Reference_number__c = 'ABCTest',
                 ContactId = contact.Id,
                 Primary_Contact_Role__c = 'General Public'
        );
        insert cncCase;
        test.stopTest();
        BusinessHours vicbusinessHour = [Select Id, Name From BusinessHours Where Name = 'Operations Business Hours - VIC'];
        List<Case> cncCasequery = [Select Id, BusinessHoursID From Case Where RecordType.DeveloperName = 'Billing_New_Development_Enquiry' AND Subject='Test Case billing123' LIMIT 1];  
        if(!cncCasequery.isempty()) {
            System.assertEquals(cncCasequery[0].BusinessHoursID , vicbusinessHour.ID);
        }
    }
    //Added by Vipul for ACT-15536 #Start
    @isTest   
    Static void checkforUpdatesOnOrder() {
    list<Customer_Connections_Order__c> ccOrdrList = new list<Customer_Connections_Order__c>();
            
       Customer_Connections_Order__c ccOrdr = new Customer_Connections_Order__c(Order_ID__c='ord-1234',Location_ID__c='LOC000080307033',Order_Status__c = 'Pending');
       ccOrdrList.add(ccOrdr);
       insert ccOrdrList;
       case ccCase = new case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),Loc_Id__c='LOC000080307033',CC_Order__c=ccOrdrList[0].id,CC_Team__c= 'Right Second Time Team');
       insert ccCase;   
        
    }
    @isTest   
    Static void checkforUpdatesOnOrder1() {
    list<Customer_Connections_Order__c> ccOrdrList = new list<Customer_Connections_Order__c>();
            
       Customer_Connections_Order__c ccOrdr = new Customer_Connections_Order__c(Order_ID__c='ord-1234',Location_ID__c='LOC000080307033',Order_Status__c = 'In Progress');
       ccOrdrList.add(ccOrdr);
       insert ccOrdrList;
       case ccCase = new case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),Loc_Id__c='LOC000080307033',CC_Order__c=ccOrdrList[0].id,CC_Team__c= 'Right Second Time Team');
       insert ccCase;   
        
    }
    @isTest   
    Static void checkforUpdatesOnOrder2() {
    list<Customer_Connections_Order__c> ccOrdrList = new list<Customer_Connections_Order__c>();
            
       Customer_Connections_Order__c ccOrdr = new Customer_Connections_Order__c(Order_ID__c='ord-1234',Location_ID__c='LOC000080307033',Order_Status__c = 'Held');
       ccOrdrList.add(ccOrdr);
       insert ccOrdrList;
       case ccCase = new case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),Loc_Id__c='LOC000080307033',CC_Order__c=ccOrdrList[0].id,CC_Team__c= 'Right Second Time Team');
       insert ccCase;   
        
    }
    //Added by Vipul for ACT-15536 #END
    //Added by Justin Jacob for ACT-15260 #Start
    @isTest
    Static void TestisbeforeUpdate(){
       
    System.runAs(new user(ID = UserInfo.getUserID())){
     List<Group> GroupList=new List<Group>();
    Group testGroup = new Group(Name='Customer Connections Team', type='Queue');
    GroupList.add(testGroup);
    insert GroupList;
    QueuesObject testQueue = new QueueSObject(QueueID = GroupList[0].id, SobjectType = 'Case');
    insert  testQueue;
    Case c= new Case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),
                             Loc_Id__c='LOC000080307033',Status='Assigned');
    
    insert c;
    c.OwnerId=GroupList[0].id;
    update c;   
        
}
} 
    @isTest
    Static void TestisbeforeUpdate2(){
    System.runAs(new user(ID = UserInfo.getUserID())){
    UserRole r = new UserRole(DeveloperName = 'test', Name = 'Manager of Customer Connections');
    insert r;
    
     User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'NBN Right Second Time Team Leader'].Id,
     LastName = 'last',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'Australia/Sydney',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id
     );
    insert u;
    Case c= new Case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),
                             Loc_Id__c='LOC000080307033',Status='New');
       System.runas(u){
         insert c;
        }  
        c.OwnerId=u.id;
        update c;
     }
   }
   //Added by Justin Jacob for ACT-15260 #END
   
   /****START - Added by Jairaj as part of US#MSEU-10369 for ICT****/
   @isTest(SeeAllData=false)    
    Static void testICTCaseValidation() {
        //Creating Case of record type 'ICT Channel Partner'
        account partnerAcc = new account(name='test',recordtypeid=Schema.SObjectType.account.getRecordTypeInfosByName().get('Partner Account').getRecordTypeId());
        insert partnerAcc;
        id ictRecordTypeID = Schema.SObjectType.case.getRecordTypeInfosByName().get('ICT Channel Partner').getRecordTypeId();
        Case ictCase = new Case(Subject = 'Test',Description = 'Test',RecordTypeId = ictRecordTypeID,Status = 'Open',Priority = '3-General',accountid=partnerAcc.id);
        insert ictCase;
        test.startTest();
        ictCase.status = 'Closed';
        ictCase.Outcome__c = 'Approved';
        update ictCase;
        test.stopTest();
        System.assertEquals(ictCase.status, 'Closed');
    }
    /****END - Added by Jairaj as part of US#MSEU-10369 for ICT****/
     
  //Added by Justin Jacob for ACT-17167 #START
@isTest
    Static void TestconditionInsertDOMTL(){
    
     UserRole r = new UserRole(DeveloperName = 'test', Name = 'Manager of Customer Connections');
     insert r;   
     User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'NBN DOM Team Leader'].Id,
     LastName = 'last',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'Australia/Sydney',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id
     );
     insert u;
     List<Case> CaseList=new List<Case>();
    List<Customer_Connections_Order__c> ccOrderList = new List<Customer_Connections_Order__c>();
    System.runAs(u){ 
    for(Integer i=0;i<1;i++){
    Customer_Connections_Order__c ccorder=new Customer_Connections_Order__c();
    ccorder.Order_ID__c='test'+i;
    ccorder.Location_ID__c='LOC000080307033'; 
    ccorder.Order_Status__c='Held';
    ccorder.Plan_Remediation_Date__c=system.today();
    ccOrderList.add(ccorder);
    }
    insert ccOrderList;
    for(Integer j=0;j<1;j++){
    Case c= new Case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),
                             Loc_Id__c='LOC000080307033',Order_ID__c='test'+j,Resolver_Group__c='Internal : NOC',CC_Team__c='Disconnection Order Management Team');
    CaseList.add(c);                         
    }                        
      
    Test.startTest();        
    insert CaseList; 
    //CaseList[0].ownerid = u.id;
    //update CaseList[0];
        
   Test.stopTest();
                  
}
        case caseOwner = [Select Id,status, ownerId from case where id =: caseList[0].id limit 1];
        if(String.valueOf(caseOwner.ownerId).startsWith('00G')){
            List<Group> lstQueueRecords = [Select Id, Name from Group where Type = 'Queue' AND Id =: caseOwner.ownerId];
            system.assertEquals('Disconnection Order Management Team',lstQueueRecords[0].Name);
            system.assertEquals('New',caseOwner.status);
        }
   
    }
 @isTest
    Static void TestconditionInsertDOMUR(){
    
     UserRole r = new UserRole(DeveloperName = 'test', Name = 'Manager of Customer Connections');
     insert r;   
     User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'NBN DOM User'].Id,
     LastName = 'last',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'Australia/Sydney',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id
     );
     insert u;
     List<Case> CaseList=new List<Case>();
    List<Customer_Connections_Order__c> ccOrderList = new List<Customer_Connections_Order__c>();
    System.runAs(u){ 
    for(Integer i=0;i<1;i++){
    Customer_Connections_Order__c ccorder=new Customer_Connections_Order__c();
    ccorder.Order_ID__c='test'+i;
    ccorder.Location_ID__c='LOC000080307033'; 
    ccorder.Order_Status__c='Held';
    ccorder.Plan_Remediation_Date__c=system.today();
    ccOrderList.add(ccorder);
    }
    insert ccOrderList;
    for(Integer j=0;j<1;j++){
    Case c= new Case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),
                             Loc_Id__c='LOC000080307033',Order_ID__c='test'+j,Resolver_Group__c='Internal : NOC',CC_Team__c='Disconnection Order Management Team');
    CaseList.add(c);                         
    }                        
      
    Test.startTest();        
    insert CaseList; 
    //CaseList[0].ownerid = u.id;
    //update CaseList[0];
        
   Test.stopTest();
                  
}
        case caseOwner = [Select Id,status, ownerId from case where id =: caseList[0].id limit 1];
        if(String.valueOf(caseOwner.ownerId).startsWith('00G')){
            List<Group> lstQueueRecords = [Select Id, Name from Group where Type = 'Queue' AND Id =: caseOwner.ownerId];
            system.assertEquals('Disconnection Order Management Team',lstQueueRecords[0].Name);
            system.assertEquals('New',caseOwner.status);
        }
   
    }   
 
    Static testMethod void TestconditionUpdate(){
    
     UserRole r = new UserRole(DeveloperName = 'test', Name = 'Manager of Customer Connections');
     insert r;   
     User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'NBN DOM Team Leader'].Id,
     LastName = 'last',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'Australia/Sydney',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id
     );
     insert u;
   
        List<Case> CaseList=new List<Case>();
        //List<Case> casesToUpdate = new List<Case>();
    List<Customer_Connections_Order__c> ccOrderList = new List<Customer_Connections_Order__c>();
    System.runAs(u){ 
    for(Integer i=0;i<1;i++){
    Customer_Connections_Order__c ccorder=new Customer_Connections_Order__c();
    ccorder.Order_ID__c='test'+i;
    ccorder.Location_ID__c='LOC000080307033'; 
    ccorder.Order_Status__c='Held';
    ccorder.Plan_Remediation_Date__c=system.today();
    ccOrderList.add(ccorder);
    }
    insert ccOrderList;
    for(Integer j=0;j<1;j++){
    Case c= new Case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),
                             Loc_Id__c='LOC000080307033',Order_ID__c='test'+j,Resolver_Group__c='Internal : NOC',CC_Team__c='Disconnection Order Management Team');
    CaseList.add(c);
        //CasetoUpdated.add(c);
    }                        
      
    Test.startTest();
      insert CaseList;
    Test.stopTest(); 
    }        
        System.runAs(new user(ID = UserInfo.getUserID())){
            CaseList[0].ownerId =u.id;
            update CaseList;
        
}
 case updatedCase = [Select Id,status, ownerId from case where id =: caseList[0].id limit 1];       
  system.assertEquals('Assigned',updatedCase.status);
}
//Added by Justin Jacob for 17167 #END
//Added by Ramya Narayanaswamy for 17170 #Start
//Testing scenario of bulk upload and case assignment to DOM queue
    private static testMethod void testDOMCases(){
        Profile integrationUserProfile = [SELECT Id FROM Profile WHERE Name = 'Customer Connection Integration User(Api only)' LIMIT 1];    
        User integrationUser = TestDataUtility.createTestUser(false, integrationUserProfile.Id);
        insert integrationUser;
        List<Customer_Connections_Order__c> ccOrderList = new List<Customer_Connections_Order__c>();
        List<case> caseList = new List<case>();
        system.runAs(integrationUser){
            for(Integer i=0;i<5;i++){
                Customer_Connections_Order__c ccorder=new Customer_Connections_Order__c(Order_ID__c='test Order' + i,Location_ID__c='LOC000080307033',
                                                                                        Order_Status__c='Held');
                ccOrderList.add(ccorder);
            }
            
            for(Integer i=0;i<5;i++){
                Case caseObj = new Case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),
                             Loc_Id__c='LOC000080307033',Status='New',Order_ID__c='test Order'+i,CC_Team__c='Disconnection Order Management Team',
                                        Initial_SDP__c = 'BSA Limited', Resolver_Group__c = 'External : SSL');   
                caseList.add(caseObj);  
            }
            Test.StartTest();
                insert ccOrderList;
                insert caseList;                            
            Test.StopTest();
            case caseOwner = [Select Id,status,ownerid from case where id =: caseList[0].id limit 1];
            if(String.valueOf(caseOwner.ownerId).startsWith('00G')){
            List<Group> lstQueueRecords = [Select Id, Name from Group where Type = 'Queue' AND Id =: caseOwner.ownerId];
            system.assertEquals('Disconnection Order Management Team',lstQueueRecords[0].Name);
            }
            //system.assertEquals(caseList[0].owner.name,'Disconnection Order Management Team');          
        }
        
    }
//Added by Ramya Narayanaswamy for 17170 #End
}