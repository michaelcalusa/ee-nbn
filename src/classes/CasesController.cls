/***************************************************************************************************
Class Name  :  CasesController
Test Class Type  :  CaseCreateRelated_Test 
Version     : 1.0 
Created Date: 01/12/2017
Function    : Method to get all the selected cases to mass update the Owner and send the notification mail 
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Dolly Yadav      01/12/2017                 Created
****************************************************************************************************/

public with sharing class CasesController {
    
    ApexPages.StandardSetController standardController;   
    public Boolean chkBx{get;set;}
    public  list<Case> css   { get;set; }
    public Boolean isErr{get;set;}
    
    public CasesController(ApexPages.StandardSetController controller) {        
        this.standardController = controller;
        css = standardController.getSelected();
        if (css.isempty()) {
            ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one row to change the Case Owner');
            ApexPages.addMessage(errMsg);
            isErr=True;
        }
   }
    
      
    public PageReference click(){
          system.debug('Email notification flag:  ' + chkBx);        
         // css = standardController.getSelected();  
          system.debug('Selected Casess:  ' + css);          
          Database.DMLOptions dlo = new Database.DMLOptions();
          dlo.EmailHeader.triggerUserEmail = chkBx;          
          Database.update(css, dlo);
         // return new Pagereference('/'+Case.getSObjectType().getDescribe().getKeyPrefix()+'/o');
        String profileId = [Select Id, Name from Profile where Name ='NBN Community Affairs'].Id;
    String listid;
        if(UserInfo.getProfileId() == profileId){
          listid = [SELECT Id, DeveloperName, SobjectType FROM ListView where sobjectType= 'Case' AND DeveloperName = 'Corporate_Affairs_Team_Case'].Id;    
        }
        else{
          listid= [SELECT Id, DeveloperName, SobjectType FROM ListView where sobjectType= 'Case' AND DeveloperName = 'My_Open_Cases_Customer_Connections'].Id;
        }     
        
        PageReference ref = new PageReference('/'+Case.getSObjectType().getDescribe().getKeyPrefix()+'?fcf='+listid);
        
          return ref;
    }
    
    public PageReference close(){    
        String profileId = [Select Id, Name from Profile where Name ='NBN Community Affairs'].Id;
    String listid;
        if(UserInfo.getProfileId() == profileId){
          listid = [SELECT Id, DeveloperName, SobjectType FROM ListView where sobjectType= 'Case' AND DeveloperName = 'Corporate_Affairs_Team_Case'].Id;    
        }
        else{
          listid= [SELECT Id, DeveloperName, SobjectType FROM ListView where sobjectType= 'Case' AND DeveloperName = 'My_Open_Cases_Customer_Connections'].Id;
        }
        PageReference ref = new PageReference('/'+Case.getSObjectType().getDescribe().getKeyPrefix()+'?fcf='+listid);
        
        return ref;
        
        
    }
    
    

}