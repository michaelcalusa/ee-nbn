/*
Class Description
Creator: Karan
Purpose: This class will be used to transform and transfer NTD Pre install cases records from Cases_Int to Case
Modifications:
*/
public class CasesTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
    public String conQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {
        
    }
    public CasesTransformation(String ejId)
    {
         exJobId = ejId;
         Batch_Job__c bj = new Batch_Job__c();
         bj.Name = 'CasesNTDTransform'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
            conQuery = 'Select Case_number__c,Combined_Address__c,Validate_Address__c,CRM_Case_Id__c,CRM_Last_Modified_Date__c,Location_ID__c,Lot_Number__c,Post_code__c,Schedule_Job__c,Schedule_Job_Transformation__c,State__c,Status__c,Street_Name__c,Street_Number__c,Street_Type__c,Suburb__c,Transformation_Status__c,Type__c,Unique_Site__c,Unit_shop_number__c from Cases_Int__c where Transformation_Status__c = \'Extracted from CRM\' order by createddate asc';
            return Database.getQueryLocator(conQuery);   
    }
    public void execute(Database.BatchableContext BC,List<Cases_Int__c> caseIntList)
    {
        try
        {
            
            List<Case> caseList = new List<Case>();
            Map<String,Case> caseMap = new Map<String,Case>();
            List<Cases_Int__c> caseIntWithNoSite= new List<Cases_Int__c>();
            Set<String> uniqueSiteSet= new Set<String>();
            Map<String,String> siteDetailsToSiteIdMap= new Map<String,String>();
            String recordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Case'  and name = 'NTD Preinstall'].Id;
            String locId;
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.allowFieldTruncation = true;
            for(Cases_Int__c caseItObj:caseIntList){
                
                uniqueSiteSet.add(caseItObj.Unique_Site__c  );
            
            }
            system.debug('*****uniqueSiteSet***'+uniqueSiteSet);
            List<Site__c> siteList=[Select id,MDU_CP_On_Demand_Id__c,Site_Address_Unique__c from Site__c where ((RecordType.DeveloperName='Verified' OR  RecordType.DeveloperName='Unverified') AND (MDU_CP_On_Demand_Id__c IN: uniqueSiteSet OR Site_Address_Unique__c IN: uniqueSiteSet ))];
            system.debug('******siteList*****'+siteList);
            for(Site__c siteObj: siteList){
                
                if(siteObj.MDU_CP_On_Demand_Id__c!=null && siteObj.MDU_CP_On_Demand_Id__c!=''){
                    
                    siteDetailsToSiteIdMap.put(siteObj.MDU_CP_On_Demand_Id__c,siteObj.id);
                }
                else{
                    
                    siteDetailsToSiteIdMap.put(siteObj.Site_Address_Unique__c.toUpperCase(),siteObj.id);
                }
            }
            system.debug('*******siteDetailsToSiteIdMap******'+siteDetailsToSiteIdMap);
            for(Cases_Int__c caseItObj:caseIntList){
                
                
                if(caseItObj.Location_ID__c!=null && caseItObj.Location_ID__c!=''){
                        
                    if(!siteDetailsToSiteIdMap.containsKey(caseItObj.Location_ID__c)){
                        if(caseItObj.Validate_Address__c){
                        
                        caseIntWithNoSite.add(caseItObj);
                        }
                    }
                }
                else{
                    
                    if(!siteDetailsToSiteIdMap.containsKey(caseItObj.Unique_Site__c.toUppercase())){
                        if(caseItObj.Validate_Address__c){
                            caseIntWithNoSite.add(caseItObj);
                        }
                        
                    }               
                }               
            }       

            system.debug('*****caseIntWithNoSite***'+caseIntWithNoSite);
            if(caseIntWithNoSite!=null && caseIntWithNoSite.size()>0){
                
                siteDetailsToSiteIdMap.putAll(createSiteObject(caseIntWithNoSite));
            }
            system.debug('****siteDetailsToSiteIdMap*****'+siteDetailsToSiteIdMap);
            for(Cases_Int__c caseItObj:caseIntList){
                
                Case caseObj = new Case();
                if(caseItObj.Location_ID__c!=null && caseItObj.Location_ID__c!='' && caseItObj.Validate_Address__c){
                        
                    if(siteDetailsToSiteIdMap.containsKey(caseItObj.Location_ID__c)){
                        
                        caseObj.Site__c=siteDetailsToSiteIdMap.get(caseItObj.Location_ID__c);
                    }
                }
                else{
                    
                    system.debug('*caseItObj.Validate_Address__c****'+caseItObj.Validate_Address__c);
                    system.debug('*siteDetailsToSiteIdMap****'+siteDetailsToSiteIdMap);
                    if(caseItObj.Location_ID__c==null && caseItObj.Location_ID__c=='' && siteDetailsToSiteIdMap.containsKey(caseItObj.Unique_Site__c.toUpperCase()) && caseItObj.Validate_Address__c){
                        
                        caseObj.Site__c=siteDetailsToSiteIdMap.get(caseItObj.Unique_Site__c.toUpperCase());
                    }    
                    else if(!caseItObj.Validate_Address__c){
                            system.debug('****Insideinvalid address**8');
                            locId = String.isNotBlank(caseItObj.Location_ID__c) ? 'Location Id:'+caseItObj.Location_ID__c+' ': '';
                            caseObj.Site_Address__c=locId+'Address: ' + caseItObj.Combined_Address__c;
                        }                   
                }
                caseObj.On_Demand_Case_Number__c=caseItObj.Case_number__c;
                caseObj.On_Demand_Id__c=caseItObj.CRM_Case_Id__c;
                caseObj.RecordTypeId=recordTypeId;
                caseObj.Status=caseItObj.Status__c;
                caseObj.Ownership__c='CRMOD';
                caseMap.put(caseItObj.CRM_Case_Id__c, caseObj);
               //caseList.add(caseObj);
            
            }
            caseList = caseMap.values();
            system.debug('*****caseList***'+caseList);
            Schema.SObjectField onDemandId = Case.Fields.On_Demand_Id__c;
            List<Database.upsertResult> uResults = Database.upsert(caseList,onDemandId,false);
            list<Database.Error> err;
            String msg, fAffected;
            String[]fAff;
            StatusCode sCode;
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            for(Integer idx = 0; idx < uResults.size(); idx++)
            {   
                if(!uResults[idx].isSuccess())
                {
                    err=uResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='NTDCases_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),
                    isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=caseIntList[idx].Id,Schedule_Job__c=bjId));
                    caseIntList[idx].Transformation_Status__c = 'Error';
                    caseIntList[idx].Schedule_Job_Transformation__c = bjId;
                }
                else
                {
                        /*if(String.isBlank(conIList[idx].Account_Id__c) || conIList[idx].Account_Id__c == 'No Match Row Id' || String.isNotBlank(acc.get(conIList[idx].Account_Id__c)))
                        {
                            conIList[idx].Transformation_Status__c = 'Copied to Base Table';
                        }
                        else 
                        {
                            conIList[idx].Transformation_Status__c = 'Association Error';
                            errList.add(new Error_Logging__c(Name='Contact_Tranformation',Error_Message__c='Account Record is not associated',Fields_Afftected__c='Account',isCreated__c=uResults[idx].isCreated(),
                            isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=conIList[idx].Id,Schedule_Job__c=bjId));
                        }*/
                    	caseIntList[idx].Transformation_Status__c = 'Copied to Base Table';
                        caseIntList[idx].SF_Case_Id__c = uResults[idx].getId();
                        caseIntList[idx].Schedule_Job_Transformation__c = bjId;
                        
                }   
                    
              }
            
            
            Insert errList;
            update caseIntList;
            if(errList.isEmpty())      
            {
                status = 'Completed';
            } 
            else
            {
                status = 'Error';
            }  
            recordCount = recordCount + caseIntList.size();       
        }
        catch (Exception e)
        {
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='NTDCases_Tranformation',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }
    
    public Map<String,String> createSiteObject(List<Cases_Int__c> caseWithNoSiteList){
        
        String verifiedRecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Site__c'  and name = 'Verified'].Id;
        String unverifiedRecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Site__c'  and name = 'Unverified'].Id;
        List<Site__c> newsiteList= new List<Site__c>();
        Map<String,String> locIdTOCaseIdMap= new Map<String,String>();
        Map<String,List<Cases_Int__c>> mapofLocationIdUniqueAddToListOfIntCases = new Map<String,List<Cases_Int__c>>();
        Map<String,Cases_Int__c> uniqueSiteRecordsToCreate = new Map<String,Cases_Int__c>();
        for(Cases_Int__c caseIntObj: caseWithNoSiteList){
            
            if(caseIntObj.Location_ID__c!=null && caseIntObj.Location_ID__c!=''){
                uniqueSiteRecordsToCreate.put(caseIntObj.Location_ID__c,caseIntObj);
            }
            else{
                uniqueSiteRecordsToCreate.put(caseIntObj.Combined_Address__c,caseIntObj);
            }
        }
        system.debug('****uniqueSiteRecordsToCreate***'+uniqueSiteRecordsToCreate);
        
        if(uniqueSiteRecordsToCreate!=null && uniqueSiteRecordsToCreate.size()>0){
            for(Cases_Int__c caseIntObj: uniqueSiteRecordsToCreate.values()){
            
                Site__c siteObj= new Site__c();
                siteObj.Location_Id__c=caseIntObj.Location_ID__c!=null && caseIntObj.Location_ID__c!='' ? caseIntObj.Location_ID__c :'';
                siteObj.Lot_Number__c=caseIntObj.Lot_Number__c!=null && caseIntObj.Lot_Number__c!='' ? caseIntObj.Lot_Number__c :'';
                siteObj.Unit_Number__c=caseIntObj.Unit_shop_number__c!=null && caseIntObj.Unit_shop_number__c!='' ? caseIntObj.Unit_shop_number__c :'';
                siteObj.Road_Number_1__c=caseIntObj.Street_Number__c!=null && caseIntObj.Street_Number__c!='' ? caseIntObj.Street_Number__c :'';
                siteObj.Road_Name__c=caseIntObj.Street_Name__c!=null && caseIntObj.Street_Name__c!='' ? caseIntObj.Street_Name__c    :'';
                siteObj.Locality_Name__c=caseIntObj.Suburb__c!=null && caseIntObj.Suburb__c!='' ? caseIntObj.Suburb__c :'';
                siteObj.Road_Type_Code__c=caseIntObj.Street_Type__c!=null && caseIntObj.Street_Type__c!='' ? caseIntObj.Street_Type__c :'';
                siteObj.State_Territory_Code__c=caseIntObj.State__c!=null && caseIntObj.State__c!='' ? caseIntObj.State__c :null;
                siteObj.Post_Code__c=caseIntObj.Post_code__c!=null && caseIntObj.Post_code__c!='' ? caseIntObj.Post_code__c :null;
                siteObj.Name=caseIntObj.Combined_Address__c.toUpperCase();
                siteObj.Site_Address__c=caseIntObj.Combined_Address__c;
                siteObj.CRMOD_Case_Site__c = true;
                if(siteObj.Location_Id__c!=null && siteObj.Location_Id__c!='' ){
                    siteObj.RecordTypeId=verifiedRecordTypeId;
                    siteObj.MDU_CP_On_Demand_Id__c=siteObj.Location_Id__c;
                }
                else{
                    siteObj.RecordTypeId=unverifiedRecordTypeId;
                    siteObj.Site_Address_Unique__c=caseIntObj.Combined_Address__c.toUpperCase();
                }
                
                newsiteList.add(siteObj);
            }
             insert newsiteList;
        }

       
        for(Site__c siteobj: newsiteList){
            
            if(siteobj.Location_Id__c!=null && siteobj.Location_Id__c!=''){
            
                
                locIdTOCaseIdMap.put(siteobj.Location_Id__c,siteobj.Id);
            }
            else{
                locIdTOCaseIdMap.put(siteobj.Site_Address_Unique__c,siteobj.Id);
            }
        
        }
        system.debug('****locIdTOCaseIdMap**'+locIdTOCaseIdMap);
        return locIdTOCaseIdMap;
    }
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        
        AsyncApexJob conJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
        {
            bj.Status__c= conJob.Status;
        }
        else
        {
             bj.Status__c=status;
        }
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = conJob.id;
        bj.End_Time__c  = conJob.CompletedDate;
        upsert bj;
    }   

}