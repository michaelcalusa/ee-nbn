/***************************************************************************************************
Class Name:  CasesTransformation_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 31-10-2016
Function    : This class contains unit test scenarios for CasesTransformation apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       31-10-2016                Created
****************************************************************************************************/
@isTest
private class CasesTransformation_Test {
    static date CRM_Modified_Date = date.newinstance(1963, 01,24);
    static List<Development__c> ListOfDevelopment = new List<Development__c> ();
    static List<Stage_Application__c> ListOfStageApplication = new List<Stage_Application__c> ();
    static List<Contact> ListOfContacts = new List<Contact> ();
    static List<Cases_Int__c> listOfCasesInt = new List<Cases_Int__c>();
    static void getRecords (){
        // Custom Setting record creation
        TestDataUtility.getListOfCRMTransformationJobRecords(true);
        // Create Account record
        List<Account> listOfAccounts = new List<Account> (); 
        Account accRec = new Account();
        accRec.name = 'Test Account';
        accRec.on_demand_id__c = 'Account1';
        listOfAccounts.add(accRec);
        insert listOfAccounts;
        // Create Contact record
        Contact contactRec = new Contact ();
        contactRec.on_demand_id_P2P__c = 'Contact1';
        contactRec.On_Demand_Id__c  = 'Contact1';
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        ListOfContacts.add(contactRec);
        insert ListOfContacts;
        // Create Development record
        Development__c Development1 = new Development__c ();
        Development1.Name = 'Test';
        Development1.Development_ID__c = 'Development1';
        Development1.Account__c = listOfAccounts.get(0).Id;
        Development1.Primary_Contact__c = ListOfContacts.get(0).Id;
        Development1.Suburb__c = 'North Sydney';
        ListOfDevelopment.add(Development1);
        insert ListOfDevelopment;
        // Create Junction_Object_Int__c record
        Junction_Object_Int__c JunctionObjectInt1 = new Junction_Object_Int__c();
        JunctionObjectInt1.Name='TestName';
        JunctionObjectInt1.CRM_Modified_Date__c=CRM_Modified_Date;
        JunctionObjectInt1.Transformation_Status__c = 'Exported from CRM';
        JunctionObjectInt1.Event_Name__c = 'Associate';
        JunctionObjectInt1.Object_Name__c = 'CustomObject3';
        JunctionObjectInt1.Child_Id__c = 'Contact1';
        JunctionObjectInt1.Object_Id__c = 'StageApplicationInt1';
        insert JunctionObjectInt1;
        // Create Site
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Verified').getRecordTypeId();
        List<Site__c> listOfSites = TestDataUtility.createSiteTestRecords(2, false);
        integer counter = 0;
        for(Site__c Site : listOfSites){
            Site.recordTypeId = recordTypeId;
            if(counter==0){
                Site.Location_Id__c = 'LOC112123456789';
                Site.MDU_CP_On_Demand_Id__c = 'LOC112123456789';
            }
            else{
                Site.Location_Id__c = 'LOC11123456789'+counter;
            }
            counter++;
        }
        insert listOfSites;
        // Create Cases_Int__c Records
        Cases_Int__c CasesIntRec1 = new Cases_Int__c();
        CasesIntRec1.Location_ID__c = 'LOC112123456789';
        CasesIntRec1.CRM_Last_Modified_Date__c = CRM_Modified_Date;
        CasesIntRec1.Lot_Number__c = '1001';
        CasesIntRec1.Street_Number__c = '50';
        CasesIntRec1.Street_Name__c = 'Miller';
        CasesIntRec1.Suburb__c = 'North Sydney';
        CasesIntRec1.State__c = 'NSW';
        CasesIntRec1.Post_code__c = '2150';
        listOfCasesInt.add(CasesIntRec1);
        Cases_Int__c CasesIntRec2 = new Cases_Int__c();
        CasesIntRec2.Location_ID__c = 'LOC112123456780';
        CasesIntRec2.CRM_Last_Modified_Date__c = CRM_Modified_Date;
        CasesIntRec2.Lot_Number__c = '100';
        CasesIntRec2.Street_Number__c = '50';
        CasesIntRec2.Street_Name__c = 'Miller';
        CasesIntRec2.Suburb__c = 'North Sydney';
        CasesIntRec2.State__c = 'NSW';
        CasesIntRec2.Post_code__c = '2150';
        listOfCasesInt.add(CasesIntRec2);
    }
    static void setUpStageApplicationIntRecords(){
        // Create Stage Application record
        List<Stage_Application_Int__c> ListOfStageApplicationInt = new List<Stage_Application_Int__c>();
        Stage_Application_Int__c StageApplicationIntRec1 = new Stage_Application_Int__c();
        StageApplicationIntRec1.Name = 'TestName';
        StageApplicationIntRec1.Primary_Contact_Id__c = 'Contact1';
        StageApplicationIntRec1.Account_Id__c = 'Account1';
        StageApplicationIntRec1.Development_Id__c = 'Development1';
        StageApplicationIntRec1.CRM_Last_Modified_Date__c = CRM_Modified_Date;
        StageApplicationIntRec1.Latitude__c = '80.209566';
        StageApplicationIntRec1.Longitude__c = '151.209566';
        StageApplicationIntRec1.Request_Id__c = 'StageApplicationInt1';
        StageApplicationIntRec1.Dwelling_Type__c = 'SDU Only';
        StageApplicationIntRec1.Active_Status__c = 'Active';
        StageApplicationIntRec1.Estimated_First_Service_Connection_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec1.Estimated_Ready_for_Service_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec1.Target_Delivery_Date__c = string.valueOf(CRM_Modified_Date);
        ListOfStageApplicationInt.add(StageApplicationIntRec1);
        Stage_Application_Int__c StageApplicationIntRec2 = new Stage_Application_Int__c();
        StageApplicationIntRec2.Name = 'TestName';
        StageApplicationIntRec2.Primary_Contact_Id__c = 'Contact11111';
        StageApplicationIntRec2.Account_Id__c = 'Account1111';
        StageApplicationIntRec2.Development_Id__c = 'Development1';
        StageApplicationIntRec2.CRM_Last_Modified_Date__c = CRM_Modified_Date;
        StageApplicationIntRec2.Latitude__c = '80.209566';
        StageApplicationIntRec2.Longitude__c = '151.209566';
        StageApplicationIntRec2.Request_Id__c = 'StageApplicationInt2';
        StageApplicationIntRec2.Dwelling_Type__c = 'SDU Only';
        StageApplicationIntRec2.Active_Status__c = 'Active';
        StageApplicationIntRec2.Estimated_First_Service_Connection_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec2.Estimated_Ready_for_Service_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec2.Target_Delivery_Date__c = string.valueOf(CRM_Modified_Date);
        ListOfStageApplicationInt.add(StageApplicationIntRec2);
        insert ListOfStageApplicationInt;
    }
    static testMethod void CasesTransformation_Test1(){
        getRecords();
        setUpStageApplicationIntRecords();
        insert listOfCasesInt;
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());   
        Test.stopTest();
    }
    static testMethod void CasesTransformation_Test2(){
        getRecords();
        setUpStageApplicationIntRecords();
        listOfCasesInt.get(1).Lot_Number__c = null;
        listOfCasesInt.get(1).Location_ID__c = null;
        insert listOfCasesInt;
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());   
        Test.stopTest();
    }

}