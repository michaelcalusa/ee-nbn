public without sharing class CheckforAvailableActiveUserHandler {
    
    public static void CheckforavailableuserinTeamAssignment(List<case_Assignment__C> TeamAssignments,Map<Id,case_Assignment__C> OlDTeamAssignments){
        
        List<Case_Assignment__C> Teamassignmentavailable = new List<Case_Assignment__C>()  ;
        
        for ( Case_Assignment__C Assignment : TeamAssignments ){
            
            if(OlDTeamAssignments != null ){
                
                if(Assignment.Available__c == true && Assignment.Available__c != OlDTeamAssignments.get(Assignment.id).Available__c  )
                {
                    Teamassignmentavailable = [select id from Case_Assignment__C where user__c = : Assignment.user__C AND Available__c = true AND User_Type__c != 'RSP Escalation'];
                    if(Teamassignmentavailable.size() > 0){    
                        Assignment.addError('An active Team Assignment record already exists for the selected Assigned User. Only one Team Assignment record marked as Available should exist per user');    
                    }
                }
            }
            else{
                if(Assignment.Available__c == true )
                {
                    Teamassignmentavailable = [select id from Case_Assignment__C where user__c = : Assignment.user__C AND Available__c = true AND User_Type__c != 'RSP Escalation'];
                    if(Teamassignmentavailable.size() > 0){       
                        Assignment.addError('An active Team Assignment record already exists for the selected Assigned User. Only one Team Assignment record marked as Available should exist per user'); 
                    }
                }       
            }       
        }
    }
}