/***************************************************************************************************
Class Name:         ClearIntegrationCache_BatchSchedule
Class Type:         Batch Class With Scheduable interface
Version:            1.0 
Created Date:       March 26, 2018
Function:           This Batch class is for clear the Integration Cache Object where records are older than a week.
Input Parameters:   None 
Output Parameters:  None

Modification Log:
* Developer          Date             Description
* Sridevi            4th Dec,2018     CUSTSA-24705 :Added the logic to make the deletion of records dynamic from Cache based on integration type.
* --------------------------------------------------------------------------------------------------                 
* 
****************************************************************************************************/ 

global class ClearIntegrationCache_BatchSchedule implements Database.Batchable<sObject>, Schedulable{
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        //CUSTSA-24705::: Added by Sridevi
        
            List<String> whereClauseString = new List<String>();
            String qry = 'SELECT Id,IntegrationType__c FROM NBNIntegrationCache__c WHERE ';
            for(JIGSAW_Cache_Config__mdt intRecord: [SELECT Jigsaw_Integration_Type__c,Jigsaw_Number_Of_Days__c  FROM JIGSAW_Cache_Config__mdt]) {
             Decimal daycount = intRecord.Jigsaw_Number_Of_Days__c -1;
             Integer intConverteddaycount = daycount.intValue();                  
             whereClauseString.add('(IntegrationType__c =\''+String.escapeSingleQuotes(intRecord.Jigsaw_Integration_Type__c)+'\'' + ' AND Createddate = last_n_days:'  + intConverteddaycount+')');             
            }
            qry += String.join(whereClauseString,' OR ');
            system.debug('query is***** ' + qry);
            return Database.getQuerylocator(qry);
    } 
    
    global void execute(Database.BatchableContext BC, list<sObject> scope){
        delete scope;
    }
    
    global void finish(Database.BatchableContext BC){
    }
    
    global void execute(SchedulableContext sc){
        // Implement any logic to be scheduled
        // We now call the batch class to be scheduled
        ClearIntegrationCache_BatchSchedule b = new ClearIntegrationCache_BatchSchedule ();
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b, 200);
    }
}