@isTest
public class ClearIntegrationCache_BatchSchedule_Test{
    
    // Testmethod for checking the cache object is clear with records older than 7 days.
    static testMethod void method1() {
        Datetime olderThan7 = Datetime.now().addDays(-10);
        List<NBNIntegrationCache__c> lstCache = new List<NBNIntegrationCache__c>();
        
        for(Integer i=0 ;i <10;i++){
            NBNIntegrationCache__c objCache = new NBNIntegrationCache__c();
            objCache.JSONPayload__c ='Test JSON ' + i;
            lstCache.add(objCache);
        }
        
        insert lstCache;
        
        for(NBNIntegrationCache__c obj :lstCache){
            Test.setCreatedDate(obj.Id, olderThan7);
        }

        Test.startTest();

            ClearIntegrationCache_BatchSchedule obj = new ClearIntegrationCache_BatchSchedule();
            DataBase.executeBatch(obj); 
            
            
            
            
            // Schedule the test job

          String jobId = System.schedule('testBasicScheduledApex', '0 0 0 3 9 ? 2022', 
              new ClearIntegrationCache_BatchSchedule());

            
            
            
        Test.stopTest();
        Integer countRecords = Integer.valueOf([SELECT Count(Id) FROM NBNIntegrationCache__c][0].get('Id'));
        //System.assertEquals(0, countRecords);
        
    }
}