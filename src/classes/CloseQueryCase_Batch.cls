/*------------------------------------------------------------
Author:        Shubham Jaiswal
Company:       Wipro 
Description:   Apex batch job that will close all Query Cases not having status 'Closed' & Phase,Category,Sub-Category data populated.
Test Class:    CloseQueryCase_Test
------------------------------------------------------------*/ 
public class CloseQueryCase_Batch implements Database.Batchable<Sobject>, Database.Stateful
{
    public String caseQuery;
    public String bjId;
    public String status = 'Completed';
    public Integer recordCount = 0;
    public String successCaseIds = '';
    public String failedCaseIds = '';
    public String schJobId;
    Set<String> errMessage = new Set<String>();
    
    public class GeneralException extends Exception
    {
        
    }
    
    public CloseQueryCase_Batch(String sjId)
    {
        schJobId = sjId;
        Batch_Job__c bj = new Batch_Job__c();
        bj.Name = 'CloseQueryCase '+System.now().format();
        bj.Start_Time__c = System.now();
        bj.Extraction_Job_ID__c = schJobId;
        bj.Type__c = 'Query Cases Update';
        Database.SaveResult sResult = database.insert(bj,false);
        bjId = sResult.getId();
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        caseQuery = 'SELECT Id,Phase__c,Category__c,Sub_Category__c,Status from Case WHERE Status != \'Closed\' AND recordtype.developername = \'Query\' AND Phase__c != null AND Category__c != null AND Sub_Category__c != null'; 
        return Database.getQueryLocator(caseQuery);    
    }
    
    public void execute(Database.BatchableContext BC,List<Case> queryCases)
    {
        List<Case> caseUpdateList = new List<Case>();// Cases updated with 'Closed' Status.
        for(Case cs: queryCases)
        {  
            Case csUpdated = new Case();
            csUpdated.Id = cs.Id;
            csUpdated.Status = 'Closed';
            
            if(Test.isRunningTest() && cs.Sub_Category__c == 'RSP Choices/Prices') // Fails Case record Update & generates Error in Test Run
            {
                csUpdated.Authorisation_for_Contact__c = 'Primary First';
            }

            caseUpdateList.add(csUpdated);         
        }
        
        recordCount = recordCount + caseUpdateList.size();
        // DML statement
        Database.SaveResult[] srList = Database.update(caseUpdateList, false);
        
        Set<Id> successCaseIdsSet = new Set<Id>();

        for (Database.SaveResult sr : srList) 
        {
            if (sr.isSuccess()) 
            {       
                successCaseIds = successCaseIds + String.valueof(sr.getId()) + ' ';
                successCaseIdsSet.add(sr.getId());
            }
            else 
            {
                status = 'Failed';
                // Operation failed, so get all errors   
                list<Error_Logging__c> errCaseList = new List<Error_Logging__c>();
                for(Database.Error err : sr.getErrors()) 
                {
                    if(!errMessage.contains(String.valueof(err.getMessage())))
                    {
                        errCaseList.add(new Error_Logging__c(Name='Query Case Update Error' ,Error_Message__c=err.getMessage(),Fields_Afftected__c=String.valueof(err.getFields()),isSuccess__c=sr.isSuccess(),Status_Code__c=String.valueof(err.getStatusCode()),CRM_Record_Id__c= sr.getId(),Schedule_Job__c=bjId));
                        errMessage.add(String.valueof(err.getMessage()));
                    }
                }
                
                insert errCaseList;
            }
        }  
        
        for(Case cs: caseUpdateList)
        {
            if(!successCaseIdsSet.contains(cs.id))
            {
                failedCaseIds = failedCaseIds + String.valueof(cs.id) + ' ';
            }
        }
    }
    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        
        AsyncApexJob caseJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        bj.Status__c=status;
        bj.Batch_Job_ID__c = caseJob.id;
        bj.Record_Count__c= recordCount;
        bj.End_Time__c  = caseJob.CompletedDate;
        bj.Successful_Records__c = successCaseIds;
        bj.Failed_Records__c = failedCaseIds;
        upsert bj;
    }    
    
}