/*------------------------------------------------------------
Author:        Shubham Jaiswal
Company:       Wipro 
Description:   Apex schedule job that will execute CloseQueryCase_Batch.
Test Class:    CloseQueryCase_Test
------------------------------------------------------------*/ 
public class CloseQueryCase_Schedule implements Schedulable
{
    public static void execute(SchedulableContext ctx)
    {
        CronTrigger ct = [SELECT id, TimesTriggered,StartTime, NextFireTime,EndTime,State,CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger WHERE Id = :ctx.getTriggerId()];  
        Extraction_Job__c ej = new Extraction_Job__c();
        ej.Name = String.valueof(ct.ID);  // Extraction Job ID
        ej.Start_Time__c = System.now(); 
        ej.Extraction_Job_Name__c = ct.CronJobDetail.Name+' '+System.now().format();
        ej.RecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Extraction_Job__c'  and name = 'Other Batch Jobs'].Id;
        Database.SaveResult sResult = database.insert(ej,false);
        String ejId = sResult.getId();
        Database.executeBatch(new CloseQueryCase_Batch(ejId),50);
    }
}