@isTest
private class CloseQueryCase_Test {
/*------------------------------------------------------------
Author:        Shubham Jaiswal
Company:       Wipro
Description:   Test class for CloseQueryCase_Batch and CloseQueryCase_Schedule.
------------------------------------------------------------*/  
    private static testMethod void scheduleAndCloseQueryCases(){
        
        Id queryRecordTypeId = [SELECT id FROM RecordType WHERE SobjectType = 'Case' AND Developername = 'Query'][0].id;
       
        //create Query Cases
        List<Case> caseList = new List<Case>();
        for(integer j=0; j<4; j++)
        {
            Case caseRecord = new Case( Phase__c = 'Information',
                                        Category__c = '(I) HFC',
                                        Sub_Category__c = 'Rollout - Map Error', 
                                        Anonymous_Reason__c = 'Not Identified',
                                        recordtypeid = queryRecordTypeId,
                                        Authorisation_for_Contact__c = '');
            caseList.add(caseRecord);   
        }
        
         Case caseRecord = new Case( Phase__c = 'Information',
                                        Category__c = '(I) HFC',
                                        Sub_Category__c = 'RSP Choices/Prices', 
                                        Anonymous_Reason__c = 'Not Identified',
                                        recordtypeid = queryRecordTypeId,
                                        Authorisation_for_Contact__c = '');
         caseList.add(caseRecord); 
        
        insert caseList;            

        //Start the batch run       
        Test.startTest();
        
        //system.assertEquals( 50, [select id from Case ].size() );
         
        String jobId = System.schedule( 'CloseQueryCase_Schedule', 
                                        '0 0 0 3 9 ? 2022', 
                                        new CloseQueryCase_Schedule());                               
        Test.stopTest();
    }   
}