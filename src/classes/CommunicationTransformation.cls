/*
Class Description
Creator: v-dileepathley
Purpose: This class will be used to transform and transfer Communication records from Communication_Int to Communication
Modifications:
Dilip Athley 05/04/2017 Added code to associate the records to Record Type.
*/
public class CommunicationTransformation // implements Database.Batchable<Sobject>, Database.Stateful
{
    /*********************************************************************
    US#4118: Below lines have been commented to decommission P2P jobs.
    Author: Jairaj Jadhav
    Company: Wipro Technologies
    
    public String commQuery;
    public string bjId;
    public string status;
    public Integer recordCount =0;
    public String exJobId;
    
    public CommunicationTransformation(String ejId)
    {
         exJobId = ejId;
         Batch_Job__c bj = new Batch_Job__c();
         bj.Name = 'CommunicationTransform'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }

    public Database.QueryLocator start(Database.BatchableContext BC)
    {
            //commQuery = 'Select Name,Addressee__c,Address_Lines__c,Communication_Subtype__c,Communication_Type__c,Owner__c,Condition__c,Contractor_Assigned__c,CRM_Modified_Date__c,Delivery_Method__c,End_Date__c,FMF_Good_Faith_Attempts__c,FSA__c,MDU_CP_Id__c,SAM__c,Sent_Date__c,SharePoint_URL__c,Signed_Date__c,Start_Date__c,Stock_Code__c from Communication_Int__c where Transformation_Status__c=\'Exported from CRM\' order by createddate asc';
            //return Database.getQueryLocator(commQuery);   
    }
    
    public void execute(Database.BatchableContext BC,List<Communication_Int__c> commIlist)
    {
        try
        {
            List<Communication__c> commList = new List<Communication__c>();
            map<String, Communication__c> commMap = new Map<String, Communication__c>();
            
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.allowFieldTruncation = true;
            
            list <string> siteId = new list<string>();

            for (Communication_Int__c c: commIlist)
            {
                if(!String.isBlank(c.MDU_CP_Id__c))
                {
                    string s = c.MDU_CP_Id__c;
                    siteId.add(s);
                }
            }
            map<string,Id> site = new map<string,Id>();
            for( Site__c a: [Select MDU_CP_On_Demand_Id__c,Id from Site__c where MDU_CP_On_Demand_Id__c in :siteId FOR UPDATE])
            {
                site.put(a.MDU_CP_On_Demand_Id__c, a.Id); 
            }
            
            //v-dileepathley - added the below line to get the record type id.
            String recordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Communication__c'  and developername = 'LiFD_Notices'].Id;
            
            for (Communication_Int__c com: commIlist)
            {
                Communication__c comm = new Communication__c();
                
                comm.Communication_ID__c = com.Name;
                comm.Addressee__c = com.Addressee__c;
                comm.Address_Lines__c = com.Address_Lines__c;
                comm.Communication_Type__c = com.Communication_Type__c;
                comm.Communication_Subtype__c = com.Communication_Subtype__c;
                comm.Contractor_Assigned__c=com.Contractor_Assigned__c;
                comm.Delivery_Method__c=com.Delivery_Method__c;             
                comm.FMF_Good_Faith_Attempts__c=com.FMF_Good_Faith_Attempts__c;
                comm.CRMOD_Owner__c = com.Owner__c;
                if(!String.isBlank(com.MDU_CP_Id__c))
                {
                    //comm.MDU_CP__r=new site__c(MDU_CP_On_Demand_Id__c = com.MDU_CP_Id__c);
                    comm.MDU_CP__c = site.get(com.MDU_CP_Id__c);
                }
                if(!String.isBlank(com.End_Date__c))
                {
                    comm.End_Date__c=StringToDate.strDate(com.End_Date__c);
                }
                if(!String.isBlank(com.Sent_Date__c))
                {
                    comm.Sent_Date__c=StringToDate.strDate(com.Sent_Date__c);
                }
                comm.Sharepoint_URL__c=com.SharePoint_URL__c;
                
                if(!String.isBlank(com.Signed_Date__c))
                {
                    comm.Signed_Date__c=StringToDate.strDate(com.Signed_Date__c);
                }
                if(!String.isBlank(com.Start_Date__c))
                {
                    comm.Start_Date__c=StringToDate.strDate(com.Start_Date__c);
                }
                comm.Stock_Code__c=com.Stock_Code__c;
                comm.Ownership__c = 'CRMOD';
                comm.RecordTypeId = recordTypeId;
                comm.setOptions(dml);
                //commList.add(comm);
                commMap.put(com.Name,comm); 
            }
            
            commList = commMap.values();
            Schema.SObjectField onDemandId = Communication__c.Fields.Communication_ID__c;
            List<Database.upsertResult> uResults = Database.upsert(commList,onDemandId,false);
            list<Database.Error> err;
            String msg, fAffected;
            String[]fAff;
            StatusCode sCode;
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            for(Integer idx = 0; idx < uResults.size(); idx++)
            {   
                if(!uResults[idx].isSuccess())
                {
                    err=uResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='Communication_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),
                    isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=commIlist[idx].Id,Schedule_Job__c=bjId));
                    commIlist[idx].Transformation_Status__c = 'Error';
                    commIlist[idx].Schedule_Job_Transformation__c = bjId;
                }
                else
                {
                        if(String.isBlank(commIlist[idx].MDU_CP_Id__c) || commIlist[idx].MDU_CP_Id__c == 'No Match Row Id' || String.isNotBlank(site.get(commIlist[idx].MDU_CP_Id__c)))
                        {
                            commIlist[idx].Transformation_Status__c = 'Copied to Base Table';
                        }
                        else 
                        {
                            commIlist[idx].Transformation_Status__c = 'Association Error';
                            errList.add(new Error_Logging__c(Name='Communication_Tranformation',Error_Message__c='MDU/CP Site Record is not associated',Fields_Afftected__c='MDU/CP',isCreated__c=uResults[idx].isCreated(),
                            isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=commIlist[idx].Id,Schedule_Job__c=bjId));
                        }
                }
            }
            
            Insert errList;
            update commIlist;
            if(errList.isEmpty())      
            {
                status = 'Completed';
            } 
            else
            {
                status = 'Error';
            }  
            recordCount = recordCount+ commIlist.size();    
        }
        
        catch (Exception e)
        {
            system.debug('e====>'+e);
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='Communication_Tranformation',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob commJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
        {
            bj.Status__c= commJob.Status;
        }
        else
        {
             bj.Status__c=status;
        }
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = commJob.id;
        bj.End_Time__c  = commJob.CompletedDate;
        upsert bj;
    }
    *********************************************************************/
}