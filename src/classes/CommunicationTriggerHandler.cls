/***************************************************************************************************
Class Name:  CommunicationTriggerHandler
Class Type: Test Class 
Version     : 1.0 
Created Date: 10-07-2017
Function    : This class contains CommunicationTriggerHandler apex class.
              Get business days between send data and start date
              Get business days between start date and send date
Used in     : ComunicationTrigger

****************************************************************************************************/
public class CommunicationTriggerHandler {
    Static Id commRecTypeLASEId = Schema.SObjectType.Communication__c.getRecordTypeInfosByName().get('LA&SE Communication').getRecordTypeId();
    public CommunicationTriggerHandler(){
        //List<Communication__c> co = [Select Id, Sent_Date__c, Start_Date__c, End_Date__c, Notification_Period__c, Install_Period__c from Communication__c ];
        BusinessHours bh = [SELECT id  From BusinessHours Where IsDefault=true];
         for (Communication__c comm: (List<Communication__c>)trigger.new){    
             if (comm.Start_Date__c != null && comm.Sent_Date__c != null &&(comm.Sent_Date__c < comm.Start_Date__c)) {  
                comm.Notification_Period_PH__c  = BusinessHours.diff(bh.Id, comm.Sent_Date__c, comm.Start_Date__c ) * 1.0 / (1000 * 60 * 60 * 8);
             }else {
                 comm.Notification_Period_PH__c  = null ;         
             }
             if (comm.Start_Date__c != null &&  comm.End_Date__c != null && (comm.Start_Date__c < comm.End_Date__c)) {
                 comm.Install_Period_PH__c =  BusinessHours.diff(bh.Id, comm.Start_Date__c, comm.End_Date__c  ) * 1.0 / (1000 * 60 * 60 * 8); 
            
             }else {
                comm.Install_Period_PH__c = null; 
             }
             
             String errorMessage = '';
             if (commRecTypeLASEId == comm.RecordTypeId && comm.Communication_Type__c=='Heritage Approval' && (comm.Document_Title__c==null || comm.Date_Approved__c==null || comm.Date_Submitted__c==null)) {
                 if (comm.Document_Title__c==null) {
                     errorMessage = '"Document Title" is mandatory<br />';
                 }
                 if (comm.Date_Submitted__c==null){
                     errorMessage = errorMessage + '"Date Submitted" is mandatory<br />';
                 }
                 if (comm.Date_Approved__c==null) {
                     errorMessage = errorMessage + '"Date Approved" is mandatory<br />';
                 }   
             }
             if (commRecTypeLASEId == comm.RecordTypeId && comm.Communication_Type__c=='CLAAN' && (comm.Document_Title__c==null || comm.LAAN_Start_Date__c==null || comm.LAAN_Finish_Date__c==null)) {
                 if (comm.Document_Title__c==null) {
                     errorMessage = '"Document Title" is mandatory<br />';
                 }
                 if (comm.LAAN_Start_Date__c==null){
                     errorMessage = errorMessage + '"LAAN Start Date" is mandatory<br />';
                 }
                 if (comm.LAAN_Finish_Date__c==null) {
                     errorMessage = errorMessage + '"LAAN Finish Date" is mandatory<br />';
                 }  
             }
             if (errorMessage != '') {
                 comm.addError('<span><br/>'+errorMessage+'<br/></span>', false);
             }
             
        }   
         
    }



}