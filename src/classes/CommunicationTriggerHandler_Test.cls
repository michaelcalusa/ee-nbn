/***************************************************************************************************
Class Name:  CommunicationTriggerHandler_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 10-07-2017
Function    : This class contains unit test scenarios for  CommunicationTriggerHandler apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Shuo Li                    10-07-2017                Created
****************************************************************************************************/
@isTest
private class CommunicationTriggerHandler_Test {

    static Id commRecTypeLASEId = Schema.SObjectType.Communication__c.getRecordTypeInfosByName().get('LA&SE Communication').getRecordTypeId();
    static Id accRecTypeInstId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Institution').getRecordTypeId();

    static testMethod void CommunicationTriggerUnitTest() {
    	  list<Communication__c>CommList = new list<Communication__c>();
          Communication__c Comm = new Communication__c();
          Comm.Sent_Date__c =  Date.newInstance(2017, 12,21);  
          Comm.Start_Date__c =  Date.newInstance(2017, 12,29);  
          Comm.End_Date__c  =  Date.newInstance(2018, 1,3);           
          CommList.add(Comm);
          
          Communication__c Comm1 = new Communication__c();
          Comm1.Sent_Date__c =  Date.newInstance(2017, 12,21);            
          CommList.add(Comm1);
          
          insert Commlist;  
        
    }
    
    static testMethod void CommunicationTriggerUnitTest2() {
        Account acc = new Account(Name='TestAccComm',RecordTypeId=accRecTypeInstId,Status__c='Active',State__c='NSW');
        insert acc;
        Communication__c Comm = new Communication__c(Account__c=acc.Id,Communication_Type__c='Heritage Approval',Signed_Date__c=Date.valueof('2019-12-31'));
        try {
               insert Comm;
        } catch (Exception ex) {
                 System.debug('Exception occured'+ex);
        }       
        
    }
    
    static testMethod void CommunicationTriggerUnitTest3() {
        Account acc = new Account(Name='TestAccComm',RecordTypeId=accRecTypeInstId,Status__c='Active',State__c='NSW');
        insert acc;
        Communication__c Comm = new Communication__c(Account__c=acc.Id,Communication_Type__c='CLAAN',Signed_Date__c=Date.valueof('2019-12-31'));
        try {
               insert Comm;
        } catch (Exception ex) {
                 System.debug('Exception occured'+ex);
        }       
        
    }
}