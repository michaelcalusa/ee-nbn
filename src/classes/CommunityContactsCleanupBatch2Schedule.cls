public class CommunityContactsCleanupBatch2Schedule implements Schedulable{

    public static void execute(SchedulableContext ctx)
    {
        Database.executeBatch(new batchCommunityContactsCleanup());
    }
}