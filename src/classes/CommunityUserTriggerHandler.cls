public without sharing class CommunityUserTriggerHandler extends TriggerHandler {
/*------------------------------------------------------------------------
Author:        Denny Chandra
Company:       Salesforce
Description:   A class created to manage trigger actions from the User object 
               Responsible for:
                1. After Insert: Checking/ unchecking Community User flag in the contact (only if the user is for the community user. Contact Id is not empty)
                2. After Update: Checking/ unchecking Community User flag in the contact (only if the user is for the community user. Contact Id is not empty)
                    a. Inactivate user (User: Active flag checked/ unchecked)
                        - set the flag to inactive: ContactId still remain after inactivation. User is still associated to the contact. You cant create a new contact because user is still associated with a contact although it's inactive.
                        - set the flag back to active: ContactId still remain after inactivation. 
                    b. Disabling user (Manage External User-Disable Customer User): ContactId still remain at the point of update (old and new). Subsequent query, ContactId is null 
Test Class:    CommunityUserTriggerHandler_Test
History
<Date>            <Authors Name>    <Brief Description of Change>
25/02/2016        Denny Chandra        Initial draft
--------------------------------------------------------------------------
    
    Map <Id,Boolean> contactIdsToUpdate = new Map<Id,Boolean>();    // List of Contact Id and flag to be updated
               
    public CommunityUserTriggerHandler() {
        System.Debug( 'NBN: CommunityUserTriggerHandler invoked' );
        
        if ( Test.isRunningTest() ) {
            this.setMaxLoopCount(10);
        }
        else {  //Set max loop count to 2 - this trigger needs to fire twice to cater for before and after events
            this.setMaxLoopCount(2);
        }      
    }

     trigger handler overrides  
    //====================================================================
    
    protected override void afterInsert(map<id,sObject> newMap) {
        System.Debug( 'NBN: CommunityUserTriggerHandler->afterInsert' );
        
        for( User newValueUser : ( List<User> ) (newMap.Values())) {    // Loop through each users update. Cast List<SObject> to List<User>            
            if (!(String.isBlank(newValueUser.ContactId))) {    // ONLY update if contact id is not empty
                System.Debug('NBN: CommunityUserTriggerHandler->afterInsert inside if IsActive New ' + newValueUser.IsActive);
                System.Debug('NBN: CommunityUserTriggerHandler->afterInsert inside if ContactId New ' + newValueUser.ContactId);
                contactIdsToUpdate.put( newValueUser.ContactId, newValueUser.IsActive );    // must use oldValueUser to cater both inactive and disable scenario.
            }
            
            if ( contactIdsToUpdate.size() > 0 ) {  // only update if there are at least 1 contact to be updated
                updateCommunityContacts( contactIdsToUpdate );                                                                    
            }     
        }
    }
    
    protected override void afterUpdate( map<id,SObject> oldMap, map<id,SObject> newMap ) {

        System.Debug( 'NBN: CommunityUserTriggerHandler->afterUpdate' );
        
        for( User newValueUser : ( List<User> ) (newMap.Values())) {    // Loop through each users update. Cast List<SObject> to List<User>
            User oldValueUser = (User) oldMap.get(newValueUser.Id);     // Cast SObject to User for oldValueUser. This is to retrieve additional fields

            System.Debug('NBN: CommunityUserTriggerHandler->afterUpdate IsActive New ' + newValueUser.IsActive + 'IsActive Old ' + oldValueUser.IsActive);
            System.Debug('NBN: CommunityUserTriggerHandler->afterUpdate ContactId New ' + newValueUser.ContactId + 'Contact Old ' + oldValueUser.ContactId);

            if (oldValueUser.ContactId == newValueUser.ContactId) { // no contact change
                if (!(String.isBlank(oldValueUser.ContactId))) {    // ONLY update if contact id is not empty
                    if (newValueUser.IsActive <> oldValueUser.IsActive) {   // only perform the update when active flag has changed and ContactId is not empty (User is associated to a contact)
                        contactIdsToUpdate.put( newValueUser.ContactId, newValueUser.IsActive ); 
                    }
                }
            }
            else {  // if contactId has changed for the same user. It could be updated via code (future proofing). a.from a value to null   b. from a value to another value
            	System.Debug('NBN: CommunityUserTriggerHandler->afterUpdate contactId changed ContactId New ' + newValueUser.ContactId + 'Contact Old ' + oldValueUser.ContactId);
                contactIdsToUpdate.put( oldValueUser.ContactId, false );    // scenario a and b. Deactivating the old contact as it no longer associated with a user.                                   

                if (!(String.isBlank(newValueUser.ContactId))) {    // set the flag of the new contact only when the new contact has an id (scenario b only) 
                    contactIdsToUpdate.put( newValueUser.ContactId, newValueUser.IsActive );                       
                }
            }
 
            if ( contactIdsToUpdate.size() > 0 ) {  // only update if there are at least 1 contact to be updated
                System.Debug('NBN: CommunityUserTriggerHandler->afterUpdate contactIdsToUpdate > 0');                
                updateCommunityContacts( contactIdsToUpdate );                                                                    
            }
        }          
    }

    end trigger handler overrides
    //====================================================================
    
 
     private methods 
    //====================================================================

    @future // call the Contact update async, to prevent MIXED_DML_OPERATION error.
    private static void updateCommunityContacts( Map<Id,Boolean> communityContactIds ) {
    
        System.Debug('NBN: CommunityUserTriggerHandler->updateCommunityContacts');
        List<Contact> communityContactUpdate = new List<Contact>(); 
            
        for( Id contactId : communityContactIds.keySet() ) {    // Loop through each ContactId    
            Boolean communityUserActive = communityContactIds.get(contactId);   // Get the Active flag to a variable
  
            System.Debug('NBN: CommunityUserTriggerHandler->updateCommunityContacts->inside for communityUserActive ' + communityUserActive);
            
            Contact con = new Contact( Id = contactId, Nbn360_User__c = communityUserActive);    // Prepare the Contact object
            communityContactUpdate.add(con);    // add it to the list for bulkify
        }
        
        if (communityContactUpdate.size() > 0) {    // perform the actual update
            System.Debug('NBN: CommunityUserTriggerHandler->updateCommunityContacts->inside communityContactUpdate');
            Database.SaveResult[] db = Database.update(communityContactUpdate, false);
        }
    }
   
     end private methods 
    //====================================================================
    
     // exception class
    public class CommunityUserTriggerHandlerException extends Exception {} 
*/
}