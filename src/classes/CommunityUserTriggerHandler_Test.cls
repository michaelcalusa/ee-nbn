/*------------------------------------------------------------------------
Author:        Denny Chandra
Company:       Salesforce
Description:   A test class created to test CommunityUserTriggerHandler methods
               Test executed:
               1 - setupTestData - Create test data required to execute methods
               2 - checkCreation - Test scenario for insert
               3 - checkUpdateDeactivate - Test scenario for deactivating community user
               4 - checkUpdateReactivate - Test scenario for reactivating inactive community user
               5 - checkUpdateDisable - Test scenario for disabling community user (NOT Complete. IsPortalEnabled is not allowed to be updated in test class)
        
In certain scenario where there is a neeed to update Setup Object (User) and Non-Setup Object (Account and Contact) and vice versa,
Test is run using System.runAs(currentUser) to perform an update by other user.
This is necessary to avoid the error below
- "MIXED_DML_OPERATION, DML operation on setup object is not permitted after you have updated a non-setup object (or vice versa): User, original object: Account: []"

History
<Date>      <Authors Name>     <Brief Description of Change>
20160302    Denny Chandra       Initial Draft
2016-06-20  Syed Moosa Nazir    Updated this test class to solve the "UNKNOWN_EXCEPTION, portal account owner must have a role:" exception.
                                Reason for the issue : Creator of the Community User, Account and Contact not associated with any Role.
-------------------------------------------------------------------------- */
@isTest
private class CommunityUserTriggerHandler_Test {
    /*
    static Contact testContact;
    static list<Contact> testContactList;

    static User testUser;
    static list<User> testUserList;
    static User currentUser = [Select id from User where Id = :UserInfo.getUserId()];

    
    static Id nbn360Contact = GlobalCache.getRecordTypeId('Contact', 'nbn360 Contact');
    //static Id customerContact = GlobalCache.getRecordTypeId('Contact', 'nbn360 Contact');
    
    static Profile prof = [SELECT Id FROM Profile WHERE Name='NBN360 Access Seeker Community'];     // unable to create a new profile via apex for test class due to current limitation
    
    static Profile SysProfile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
     
      Method to setup Test Data
 
    static void setupTestData(integer recordSize, Id recordType){
        String accountName = 'Account Test';
        // initializing User record to create Setup object records (Role and User)
        User UserToCreateSetupObj = TestDataUtility.createTestUser(false,SysProfile.id);
        // Creating Role to assign to a User. Current User should have role associated when creating Community User (including Accound & Contact record)
        UserRole testRole = new UserRole(name = 'TEST ROLE');
        User testRunUser = new User();
        System.runAs(UserToCreateSetupObj){
            Database.insert(testRole);
            testRunUser = TestDataUtility.createTestUser(false,SysProfile.id);
            testRunUser.userroleid = testRole.id;
            testRunUser.UserName = 'Test'+testRunUser.UserName;
        }
        if(recordSize == 1) { 
            System.runAs(testRunUser){
                testContact = TestDataUtility.createCommunityContactTestRecords(recordSize,true,'John','Smith', recordType, accountName)[0];
                testUser = TestDataUtility.createTestCommunityUserFromContact(true,prof.Id,testContact);
            }
        }
        else {
            testContactList = TestDataUtility.createCommunityContactTestRecords(recordSize,true,'John','Smith', recordType, accountName);
           // for (Contact con:  testContactList) {
         //       TestDataUtility.createTestCommunityUserFromContact(true, prof.Id, con);
        //      testUserList.add([SELECT Id, ContactId, IsActive, LastName FROM User WHERE ContactId = :con.Id]);
         //   }
        }
    }

    static testMethod void checkCreation() {
        // Test Starts -> Creating Community User......
        Test.startTest();
        setupTestData(1, nbn360Contact);

        System.runAs(currentUser) {        
            //system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkCreation->before User IsPortalEnabled =' + testUser.IsPortalEnabled);  
            testUser.IsPortalEnabled = true;    // ensure Community User is enabled. System does not allow setting this field to true upon creation via apex            
            //system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkCreation->before2 User IsPortalEnabled =' + testUser.IsPortalEnabled); 
            Database.SaveResult db = Database.update(testUser);
            
            system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkCreation->after User IsPortalEnabled =' + testUser.IsPortalEnabled); 
            system.debug('NBN Test: checkCommunityUserCreation->nbn360 user flag before =' + testContact.Nbn360_User__c);
        }
        Test.stopTest();
        
        // Checking Results........
        Contact resultContact = [select Id, Nbn360_User__c from Contact where Id= :testContact.Id]; // retrieve the latest info
        User resultUser = [select Id, IsActive, IsPortalEnabled from User where Id= :testUser.Id]; // retrieve the latest info
        system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkCreation-> nbn360 flag after =' + resultContact.Nbn360_User__c);
        system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkCreation-> resultUser after =' + resultUser);       

        //Validating Results...
        system.assertEquals( resultUser.IsPortalEnabled, true); //  User's IsPortalEnabled field is set to true because it's active
        system.assertEquals( resultUser.IsActive , true );      // User's IsActive field is set to true because it's active
        system.assertEquals( resultContact.Nbn360_User__c, true);   // Contact's nbn360 User flag is set to true because user is active        
    }
    
    static testMethod void checkUpdateDeactivate() { 
        // Prepare Pre-data................
        setupTestData(1, nbn360Contact);

        System.runAs(currentUser) { 
            //system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDeactivate->before User IsPortalEnabled =' + testUser.IsPortalEnabled);  
            testUser.IsPortalEnabled = true;    // ensure Community User is enabled. System does not allow setting this field to true upon creation via apex
            //system.debug('NBN Test: CommunityUserTriggerHandler_Test->ccheckUpdateDeactivate->before2 User IsPortalEnabled =' + testUser.IsPortalEnabled);    

            Database.SaveResult db = Database.update(testUser);
            system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDeactivate->after User IsPortalEnabled =' + testUser.IsPortalEnabled); 
        }

        // Test Starts -> Deactivate User......
        Test.startTest();        
        System.runAs(currentUser) {          
            system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDeactivate->Test Starts'); 
            testUser.IsActive = false;              // IsActive = false -> deactivating user
            Database.SaveResult db = Database.update(testUser);  

            system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDeactivate->nbn360 flag before Contact =' + testContact.Nbn360_User__c);
            system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDeactivate->before User IsActive =' + testUser.IsActive);  
        }
        Test.stopTest();            
        system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDeactivate-> after StopTest User IsActive =' + testUser.IsActive);
       
        // Checking Results........
        Contact resultContact = [select Id, Nbn360_User__c from Contact where Id= :testContact.Id]; // retrieve the latest info
        User resultUser = [select Id, IsActive, IsPortalEnabled from User where Id= :testUser.Id]; // retrieve the latest info
        system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDeactivate-> nbn360 flag after =' + resultContact.Nbn360_User__c);
        system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDeactivate-> resultUser after =' + resultUser);
        
        //Validating Results...
        system.assertEquals( resultUser.IsPortalEnabled, true); //  User's IsPortalEnabled field is set to true because User is not deactivated but it's still enabled active
        system.assertEquals( resultUser.IsActive , false );     // User's IsActive field is set to false because User is deactivated.
        system.assertEquals( resultContact.Nbn360_User__c, false);  // Contact's nbn360 User flag is set to false because User is deactivated.
    }    

    static testMethod void checkUpdateReactivate() { 
        // Prepare Pre-data................
        setupTestData(1, nbn360Contact);

        System.runAs(currentUser) { 
            //system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateReactivate->before User IsPortalEnabled =' + testUser.IsPortalEnabled);  
            testUser.IsPortalEnabled = true;    // ensure Community User is enabled. System does not allow setting this field to true upon creation via apex
            //system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateReactivate->before2 User IsPortalEnabled =' + testUser.IsPortalEnabled); 

            Database.SaveResult db = Database.update(testUser);
            system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateReactivate->after User IsPortalEnabled =' + testUser.IsPortalEnabled); 
        }

        // Prepare Pre-data -> Deactivate User. Setting the IsActive to false to make sure user is not active      
        System.runAs(currentUser) {          
            //system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateReactivate-> Prepare pre data deactivate user'); 
            testUser.IsActive = false;              // IsActive = false -> deactivating user
            Database.SaveResult db = Database.update(testUser);  
        }          

        // Test Starts -> re-activate User......
        Test.startTest();        
        System.runAs(currentUser) {          
            system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateReactivate->Test Starts'); 
            testUser.IsActive = true;               // IsActive = false -> deactivating user
            Database.SaveResult db = Database.update(testUser);  
        }
        Test.stopTest();            
        system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateReactivate-> after StopTest User IsActive =' + testUser.IsActive);
          
        // Checking Results........
        Contact resultContact = [select Id, Nbn360_User__c from Contact where Id= :testContact.Id]; // retrieve the latest info
        User resultUser = [select Id, IsActive, IsPortalEnabled from User where Id= :testUser.Id]; // retrieve the latest info
        system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateReactivate-> nbn360 flag after =' + resultContact.Nbn360_User__c);
        system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateReactivate-> resultUser after =' + resultUser);
        
        //Validating Results...
        system.assertEquals( resultUser.IsPortalEnabled, true); //  User's IsPortalEnabled field is set to true because User is not deactivated but it's still enabled active
        system.assertEquals( resultUser.IsActive , true );      // User's IsActive field is set to true because User is reactivated.
        system.assertEquals( resultContact.Nbn360_User__c, true);   // Contact's nbn360 User flag is set to true because User is reactivated.
    } 
 
   static testMethod void checkUpdateDisable() { 
        // Prepare Pre-data................
        setupTestData(1, nbn360Contact);

        System.runAs(currentUser) { 
            //system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDisable->before User IsPortalEnabled =' + testUser.IsPortalEnabled); 
            testUser.IsPortalEnabled = true;    // ensure Community User is enabled. System does not allow setting this field to true upon creation via apex
            //system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDisable->before2 User IsPortalEnabled =' + testUser.IsPortalEnabled);    

            Database.SaveResult db = Database.update(testUser);
            system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDisable->prepare Pre data - after User IsPortalEnabled =' + testUser.IsPortalEnabled); 
        }

        // Test Starts -> Disable User......
        Test.startTest();        
        System.runAs(currentUser) {          
            system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDisable->Test Starts ' + testUser); 
            testUser.IsPortalEnabled = false;
            testUser.IsActive = false;
            Database.SaveResult db = Database.update(testUser);  

            system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDisable-> nbn360 flag before Contact =' + testContact.Nbn360_User__c);
            system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDisable->before StopTest User=' + testUser);   
        }
        Test.stopTest();            
        system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDisable-> after StopTest User =' + testUser);
       
        // Checking Results........
        Contact resultContact = [select Id, Nbn360_User__c from Contact where Id= :testContact.Id]; // retrieve the latest info
        User resultUser = [select Id, IsActive, IsPortalEnabled from User where Id= :testUser.Id]; // retrieve the latest info
        system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDisable-> nbn360 flag after =' + resultContact.Nbn360_User__c);
        system.debug('NBN Test: CommunityUserTriggerHandler_Test->checkUpdateDisable-> resultUser after =' + resultUser);
        
        //Validating Results...  DC20160302: System does not seem to allow setting IsPortalEnabled via test method. Testing this test manually via UI works.
        //system.assertEquals( resultUser.IsPortalEnabled, false);  //  User's IsPortalEnabled field is set to false because User is disabled.
        //system.assertEquals( resultUser.IsActive , false );       // User's IsActive field is set to false because User is disabled.
        //system.assertEquals( resultContact.Nbn360_User__c, false);    // Contact's nbn360 User flag is set to false because User is disabled.
    }  */  
}