/***************************************************************************************************
Class Name:  ConsoleBanner_CX
Class Type: Extension Class 
Version     : 1.0 
Created Date: 10 July 2016
Function    : This class fetch the Case Field values when viewing Case Record in Console.
Input Parameters: None 
Output Parameters: None
Description:   Used in "ConsoleBanner" VF page.
Used in     : Case Creation
Modification Log :
* Developer                   Date          Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       10/07/2015      Created - Version 1.0 Refer ICRM-342 and ICRM-75 in JIRA for detailed Requirement 
****************************************************************************************************/ 
public with Sharing class ConsoleBanner_CX{
    // Class Variables
    public boolean isError {get;set;}
    public Case caseRecord {get;set;}
    private List<Profile> listOfProfile = new List<Profile> ();
    private boolean isProfileConfigured = false;
    public List<Schema.FieldSetMember> fieldSetMemberList_Column1 {get;set;}
    public List<Schema.FieldSetMember> fieldSetMemberList_Column2 {get;set;}
    public List<Schema.FieldSetMember> fieldSetMemberList_Column3 {get;set;}
    public List<Schema.FieldSetMember> fieldSetMemberList_Column4 {get;set;}
    /***************************************************************************************************
    Method Name:  ConsoleBanner_CX
    Method Type: Constructor
    Version     : 1.0 
    Created Date: 10 July 2016
    Function / Description : Fetch the record Id and record type Id from the "ConsoleBanner" visualforce page using standard controller.
    Input Parameters: ApexPages.StandardController
    Output Parameters: None
    Used in   : Used in "ConsoleBanner" VF page.
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       10/07/2015             Created - Version 1.0 
    ****************************************************************************************************/
    public ConsoleBanner_CX(ApexPages.StandardController stdController){
        // Method Variables
        List<id> listOfProfileId = new List<Id> ();
        isError = false;
        // Get Current User Profile Id
        listOfProfileId.add(userinfo.getProfileId());
        // Get the Profile by using SOQL query
        listOfProfile = HelperUtility.getProfile(listOfProfileId);
        // get Current Case Record Id from the URL
        Case caseRec = (Case)stdController.getRecord();
        fieldSetMemberList_Column1 = new List<Schema.FieldSetMember> ();
        fieldSetMemberList_Column2 = new List<Schema.FieldSetMember> ();
        fieldSetMemberList_Column3 = new List<Schema.FieldSetMember> ();
        fieldSetMemberList_Column4 = new List<Schema.FieldSetMember> ();
        getCaseRecord (caseRec);
    }
    /***************************************************************************************************
    Method Name:  getCaseRecord
    Method Type: void
    Version     : 1.0 
    Created Date: 10 July 2016
    Function / Description : Fetch the Case details.
    Input Parameters: Case object
    Output Parameters: None
    Used in   : Invoke by "ConsoleBanner_CX" Constructor
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       10/07/2015             Created - Version 1.0 
    ****************************************************************************************************/
    public void getCaseRecord (Case CaseRec){
        try{
            if(caseRec <> null && caseRec.RecordTypeId <> null){
                // get FieldSet name by iterating the Custom Setting
                for (Case_Console_Banner_Config__c caseConsoleBannerConfig : Case_Console_Banner_Config__c.getAll().values()){
                    if(caseConsoleBannerConfig.Profile_Name__c == listOfProfile.get(0).Name){
                        isProfileConfigured = true;
                    }
                }
                if(isProfileConfigured){
                    // get the Field details from the Field Set
                    fieldSetMemberList_Column1 =  HelperUtility.readFieldSet('FS_Case_Banner_Column1','Case');
                    fieldSetMemberList_Column2 =  HelperUtility.readFieldSet('FS_Case_Banner_Column2','Case');
                    fieldSetMemberList_Column3 =  HelperUtility.readFieldSet('FS_Case_Banner_Column3','Case');
                    fieldSetMemberList_Column4 =  HelperUtility.readFieldSet('FS_Case_Banner_Column4','Case');
                    Set<String> fieldNames = new Set<String> ();
                    for(Schema.FieldSetMember fld :fieldSetMemberList_Column1)
                        fieldNames.add(fld.getFieldPath());
                    for(Schema.FieldSetMember fld :fieldSetMemberList_Column2)
                        fieldNames.add(fld.getFieldPath());
                    for(Schema.FieldSetMember fld :fieldSetMemberList_Column3)
                        fieldNames.add(fld.getFieldPath());
                    for(Schema.FieldSetMember fld :fieldSetMemberList_Column4)
                        fieldNames.add(fld.getFieldPath());
                    string queryString = 'SELECT Id';
                    for(string fld :fieldNames)
                        queryString += ', ' + fld;
                    // Assign the Case Record Id in whereClause String to query Case Record
                    string whereClause = 'Id = \''+caseRec.Id+ '\' ';
                    queryString += ' FROM '+ 'Case' + ' WHERE ' + whereClause;
                    system.debug('queryString==> '+queryString);
                    caseRecord = new Case ();
                    // Make a SOQL query by using the formed Query string
                    caseRecord = database.query(queryString);
                    system.debug('caseRecord==> '+caseRecord);
                    Integer counter = 0;
                    while (counter < fieldSetMemberList_Column3.size()){
                        if(fieldSetMemberList_Column3.get(counter).getFieldPath() == 'DP_Team__c' && (caseRecord.get(fieldSetMemberList_Column3.get(counter).getFieldPath()) == null))
                            fieldSetMemberList_Column3.remove(counter);
                        else
                            counter++;
                    }
                    counter = 0;
                    while (counter < fieldSetMemberList_Column4.size()){
                        if(fieldSetMemberList_Column4.get(counter).getFieldPath() == 'HSE_Impact__c' && (caseRecord.get(fieldSetMemberList_Column4.get(counter).getFieldPath()) == false))
                            fieldSetMemberList_Column4.remove(counter);
                        else
                            counter++;
                    }
                }
                else{
                    isError = true;
                    ApexPages.Message PageErrorMess = new ApexPages.Message(ApexPages.Severity.INFO,HelperUtility.getErrorMessage('002'));
                    ApexPages.addMessage(PageErrorMess);
                }
            }
            else{
                isError = true;
                ApexPages.Message PageErrorMess = new ApexPages.Message(ApexPages.Severity.ERROR,HelperUtility.getErrorMessage('003'));
                // Commenting the below line. When Case is get created from console, Record Id and recordtype id is blank
                // ApexPages.addMessage(PageErrorMess);
            }           
        }
        catch(Exception ex){
            isError = true;
            system.debug('Error==>'+ex);
            ApexPages.Message PageErrorMess = new ApexPages.Message(ApexPages.Severity.ERROR,HelperUtility.getErrorMessage('001'));
            ApexPages.addMessage(PageErrorMess);
        }     
    }
}