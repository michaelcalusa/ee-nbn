/***************************************************************************************************
Class Name:  ConsoleBanner_CX_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 13 July 2016
Function    : This class contains unit test scenarios for  ConsoleBanner_CX apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       13/07/2015                 Created
****************************************************************************************************/
@isTest
public class ConsoleBanner_CX_Test{
    //Instance variables that are commonly used in multiple test methods
    static Case CaseRecord = new Case ();
    static User testRunningUser = new User ();
    /***************************************************************************************************
    Method Name:  getRecords
    Method Type: Constructor
    Version     : 1.0 
    Created Date: 13 July 2016
    Description:  Common Test data used for all test methods
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       13/07/2015                Created
    ****************************************************************************************************/
    static void getRecords (){
        // Add all the Profile names which are required for testing.
        List<string> listOfProfileName = new List<string> {'System Administrator'};
        // Query Profiles
        list<Profile> listOfProfiles = TestDataUtility.getListOfProfiles(listOfProfileName);
        // Instantiate User and assign values. Not creating User
        testRunningUser = TestDataUtility.createTestUser(true,listOfProfiles.get(0).id);
        // Create Account
        List<Account> listOfAccounts = TestDataUtility.getListOfAccounts(1, false);
        // Create Contact
        List<Contact> listOfContacts = TestDataUtility.getListOfContact(1, false);
        for(Contact contactRec : listOfContacts){
            contactRec.accountId = listOfAccounts.get(0).id;
        }

        insert listOfContacts;
        //System.assert(listOfContacts[0].Id != null, false);
        // Create Case
        List<Case> ListOfCases = TestDataUtility.createCaseTestRecords(1,false);
        ID recordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId();
        for(Case CaseRec : ListOfCases){
            CaseRec.ContactId = ListOfContacts.get(0).id;
            CaseRec.accountId = ListOfAccounts.get(0).Id;
            CaseRec.recordTypeId = recordTypeId;
            CaseRec.Phase__c = 'Test';
            CaseRec.Category__c = 'Test';
            CaseRec.Status = 'Open';
            CaseRec.Priority = '1-ASAP';
            CaseRec.Subject = 'Test';
            CaseRec.Description = 'Test';
            caserec.Primary_Contact_Role__c='Developer/Builder';
        }
        insert ListOfCases;
        //System.assert(ListOfCases[0].Id != null, false);
        CaseRecord = ListOfCases.get(0);
        // Custom Setting - "Error Message" Records
        List<Error_Message__c> ListofErrorMessage = TestDataUtility.getListOfErrorMessage(true);
    }
    /***************************************************************************************************
    Method Name:  BannerFunctionalityTestWithActualFieldSet
    Method Type: Test Method
    Version     : 1.0 
    Created Date: 13 July 2016
    Description:  Testing Banner
    Scenario Type:  Positive
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       13/07/2015                Created
    ****************************************************************************************************/
    static testmethod void BannerFunctionalityTestWithActualFieldSet(){
        getRecords();
        // Custom Setting - "Case Console Banner Config" Records
        List<Case_Console_Banner_Config__c> ListofCaseConsoleBannerConfig = TestDataUtility.getCaseConsoleBannerConfig(true);
        Test.startTest();
        System.runAs(testRunningUser){
            // Testing Visualforce Page
            PageReference consoleBannerPage = Page.ConsoleBanner;
            Test.setCurrentPage(consoleBannerPage); 
            ApexPages.StandardController StndCaseContr = new ApexPages.standardController(CaseRecord);
            ApexPages.CurrentPage().getparameters().put('id',CaseRecord.Id);
            ConsoleBanner_CX ConsoleBannerExtension = new ConsoleBanner_CX(StndCaseContr);
        }
        Test.stopTest();
    }
    /***************************************************************************************************
    Method Name:  BannerFunctionalityWithoutCase
    Method Type: Test Method
    Version     : 1.0 
    Created Date: 13 July 2016
    Description:  Testing Banner
    Scenario Type:  Negative
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       13/07/2015                Created
    ****************************************************************************************************/
    static testmethod void BannerFunctionalityWithoutCase(){
        getRecords();
        // Custom Setting - "Case Console Banner Config" Records
        List<Case_Console_Banner_Config__c> ListofCaseConsoleBannerConfig = TestDataUtility.getCaseConsoleBannerConfig(true);
        Test.startTest();
        System.runAs(testRunningUser){
            // Testing Visualforce Page
            PageReference consoleBannerPage = Page.ConsoleBanner;
            Test.setCurrentPage(consoleBannerPage);
            CaseRecord = new Case ();
            ApexPages.StandardController StndCaseContr = new ApexPages.standardController(CaseRecord);
            ConsoleBanner_CX ConsoleBannerExtension = new ConsoleBanner_CX(StndCaseContr);
        }
        Test.stopTest();
    }
    /***************************************************************************************************
    Method Name:  BannerFunctionalityWithoutBannerConfig
    Method Type: Test Method
    Version     : 1.0 
    Created Date: 13 July 2016
    Description:  Testing Banner
    Scenario Type:  Negative
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       13/07/2015                Created
    ****************************************************************************************************/
    static testmethod void BannerFunctionalityWithoutBannerConfig(){
        getRecords();
        Test.startTest();
        System.runAs(testRunningUser){
            // Testing Visualforce Page
            PageReference consoleBannerPage = Page.ConsoleBanner;
            Test.setCurrentPage(consoleBannerPage);
            ApexPages.StandardController StndCaseContr = new ApexPages.standardController(CaseRecord);
            ApexPages.CurrentPage().getparameters().put('id',CaseRecord.Id);
            ConsoleBanner_CX ConsoleBannerExtension = new ConsoleBanner_CX(StndCaseContr);
        }
        Test.stopTest();
    } 
}