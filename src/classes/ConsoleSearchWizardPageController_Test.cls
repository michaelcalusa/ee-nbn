/***************************************************************************************************
Class Name  :  ConsoleSearchWizardPageController_Test
Class Type  :  Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : This class contains unit test scenarios for ConsoleSearchWizardPageController apex class
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Naga       26/10/2015                 Created
****************************************************************************************************/
@isTest
public class ConsoleSearchWizardPageController_Test {
     Static Contact con;
     Static Case cs;
     Static Site__c site;
     Static List<Case_Console_Fields__c> csdataList;
     static integer statusCode = 200;
     static Map<String, String> responseHeaders = new Map<String, String> ();
    /***************************************************************************************************
    Method Name:  testInit
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Naga      26/10/2015                Created
    ****************************************************************************************************/
     static testMethod void testInit(){
         ConsoleSearchWizardPageController  obj = new ConsoleSearchWizardPageController ();
         insertCustomSettingData();
         insertContactAccountCaseData();
         obj.init();
         obj.contactID= null;
         obj.currentContactId = con.id;
         obj.recTypeString = 'Query';
         Test.startTest();
         ConsoleSearchWizardPageController.getCaseData(con.id);
         ConsoleSearchWizardPageController.getContactAddresses(con.id);
         ConsoleSearchWizardPageController.getCaseSearchData(cs.id);
         ConsoleSearchWizardPageController.SelectedAddressCases(con.id,site.id);
         ConsoleSearchWizardPageController.SelectedAddressCases(con.id,'NoAddress');
         obj.resetContactFieldSet();
         obj.resetContactFieldSet1();
         obj.selectedAnonymousReasonAvailable();
         obj.setContactIfNoAnonymousReason();
         obj.persistAddressValues();
         obj.resetNOCvalue();
         Test.stopTest();
     }
    /***************************************************************************************************
    Method Name:  testChangeCaseRecordType
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Naga      26/10/2015                Created
    ****************************************************************************************************/
     static testMethod void testChangeCaseRecordType(){
         ConsoleSearchWizardPageController  obj = new ConsoleSearchWizardPageController ();
         insertCustomSettingData();
         Test.startTest();
         obj.init();
         obj.recTypeString = 'Query';
         obj.gCaseRecType();
         Test.stopTest();
     }
    /***************************************************************************************************
    Method Name:  testUpdateContact
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Naga      26/10/2015                Created
    ****************************************************************************************************/
     static testMethod void testUpdateContact(){
         ConsoleSearchWizardPageController  obj = new ConsoleSearchWizardPageController ();
         insertContactAccountCaseData();
         Test.startTest();
         obj.init();
         obj.contactID= null;
         obj.currentContactId = con.id;
         obj.gContactFieldSet();
         obj.editContactObj = con;
         obj.UpdateContactRecord();
         Test.stopTest();
     }
     
    /***************************************************************************************************
    Method Name:  testCreateNewcaseandContact
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Naga      26/10/2015                Created
    ****************************************************************************************************/
     static testmethod void testCreateNewcaseandContact(){
         ConsoleSearchWizardPageController  obj = new ConsoleSearchWizardPageController ();
          obj.init();
          insertContactAccountCaseData();
          insertCustomSettingData();
          Case_Contact__c caseCt = new Case_Contact__c();
          caseCt.Role__c = 'End User Business';
          obj.caseContactObj = caseCt;
          obj.recTypeString = 'Query';
          Contact contactObj = new Contact ();
          contactObj.LastName= 'sukumar';
          contactObj.Email = 'sukumar@we.com';
          //contactObj.RecordTypeID = [SELECT Id FROM RecordType where SobjectType='Contact' and Name='External Contact'].Id;
          obj.contactObj  = contactObj;
          obj.selectedSiteID = site.id;
          obj.conRecordTypeId = [SELECT Id FROM RecordType where SobjectType='Contact' and Name='External Contact'].Id;
          obj.isResiAddressAsIssueAddress = true;
          obj.siteRT = site.recordTypeId;
          obj.siteLocld = site.Location_Id__c ;
          obj.siteAddress = site.Name ;
          obj.siteName = site.Name ;
          obj.siteRT = site.recordTypeId ;
          obj.siteTechType = site.Technology_Type__c;
          obj.siteSerCls = '10';
          obj.siteRollType = site.Rollout_Type__c ;
          obj.unitNumber = site.Unit_Number__c ;
          obj.levelNumber = site.Level_Number__c ;
          obj.postCode = site.Post_Code__c ;
          obj.locality = 'test' ;
          obj.roadTypeCode = site.Road_Type_Code__c ;
          obj.roadName = 'Test' ;
          obj.roadNumber1 = 'Test';
          obj.type = 'Test' ;
          obj.LotNumber = '1' ;
          obj.RoadSuffixCode = site.Road_Suffix_Code__c ;
          obj.AssetType = 'test' ;
          obj.AssetNumber = 'test' ;
          obj.stateTerritoryCode = site.state_Territory_Code__c ;
          obj.Latitude = site.Latitude__c ;
          obj.Longitude = site.Longitude__c ;
          obj.dwellingType = site.Premises_Type__c;
          obj.qualificationDate = String.valueOf(system.now()) ;
          obj.selectedIssueAddress = '{"Id":"a0Dp0000002EEtdEAG","Location_Id__c":"LOC100000Test16","Site_Address__c":"Level 1, Unit 1, 100 Paramatta St, Sydney, NSW 2000","Name":"Level 1, Unit 1, 100 Paramatta St, Sydney, NSW 2000","RecordTypeId":"012p00000000VPQAA2","Technology_Type__c":"Fibre166","Serviceability_Class__c":166,"Rollout_Type__c":"Brownfield166","Unit_Number__c":"1","Level_Number__c":"1","Post_Code__c":"2060","Locality_Name__c":"North Sydney","Road_Type_Code__c":"ST","Road_Name__c":"Miller","Road_Number_1__c":"50","Unit_Type_Code__c":"SDU","Qualification_Date__c":1478840569377}';
          Test.startTest();
          obj.CreateNewCaseSiteAndCase_Extension();
          
          obj.recTypeString = 'Complex Complaint';
          obj.CreateNewCaseSiteAndCase_Extension();
          
          contactObj.LastName= '';
          obj.CreateNewCaseSiteAndCase_Extension();
         Test.stopTest();
     }
     
    /***************************************************************************************************
    Method Name:  testCreatecaseWhenContactAvailable
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Naga      26/10/2015                Created
    ****************************************************************************************************/ 
    static testmethod void testCreatecaseWhenContactAvailable(){
         ConsoleSearchWizardPageController  obj = new ConsoleSearchWizardPageController ();
          obj.init();
          insertContactAccountCaseData();
          insertCustomSettingData();
          Case_Contact__c caseCt = new Case_Contact__c();
          caseCt.Role__c = 'End User Business';
          obj.caseObj.contactId = con.id;
          obj.caseContactObj = caseCt;
          obj.caseObj.Primary_Contact_Role__c =null;
          obj.contactId = con.id;
          obj.recTypeString = 'Query';
          obj.selectedSiteID = site.id;
          obj.isResiAddressAsIssueAddress = true;
          obj.siteRT = site.recordTypeId;
          obj.siteLocld = site.Location_Id__c ;
          obj.siteAddress = site.Name ;
          obj.siteName = site.Name ;
          obj.siteRT = site.recordTypeId ;
          obj.siteTechType = site.Technology_Type__c;
          obj.siteSerCls = '10';
          obj.siteRollType = site.Rollout_Type__c ;
          obj.unitNumber = site.Unit_Number__c ;
          obj.levelNumber = site.Level_Number__c ;
          obj.postCode = site.Post_Code__c ;
          obj.locality = 'test' ;
          obj.roadTypeCode = site.Road_Type_Code__c ;
          obj.roadName = 'Test' ;
          obj.roadNumber1 = 'Test';
          obj.type = 'Test' ;
          obj.LotNumber = '1' ;
          obj.RoadSuffixCode = site.Road_Suffix_Code__c ;
          obj.AssetType = 'test' ;
          obj.AssetNumber = 'test' ;
          obj.stateTerritoryCode = site.state_Territory_Code__c ;
          obj.Latitude = site.Latitude__c ;
          obj.Longitude = site.Longitude__c ;
          obj.dwellingType = site.Premises_Type__c;
          obj.qualificationDate = String.valueOf(system.now()) ;
          Test.startTest();
          obj.SaveWhenContactIsAvailable_Extension();
          
          obj.recTypeString = 'Complex Complaint';
          obj.SaveWhenContactIsAvailable_Extension();
          
          obj.caseContactObj = null;
          obj.SaveWhenContactIsAvailable_Extension();
         Test.stopTest();
     }
    /***************************************************************************************************
    Method Name:  testGetAddressSearchData
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Naga      26/10/2015                Created
    ****************************************************************************************************/ 
     static testMethod void testGetAddressSearchData(){
         insertContactAccountCaseData();
         AddressSearch_CX.addressWrapper stradd = new  AddressSearch_CX.addressWrapper();
         stradd.address = site.Name;
         stradd.locationId = site.Location_Id__c ;
         stradd.sfRecordId = site.id;
         Test.startTest();
        String jsonInput = '{'+
        '  \"data\": ['+
        '    {'+
        '      \"type\": \"location\",'+
        '      \"id\": \"LOC123456789012\",'+
        '      \"attributes\": {'+
        '        \"gnafId\": \"G8932387982982\",'+
        '        \"dwellingType\": \"MDU\",'+
        '        \"region\": \"Urban\",'+
        '        \"regionValueDefaulted\": false,'+
        '        \"externalTerritory\": false,'+
        '        \"landUse\": \"Commercial\",'+
        '        \"landSubUse\": \"Retail\",'+
        '        \"address\": {'+
        '          \"unstructured\": \"Building A 1-2 Park LANE N Parkview 12345 APT 1 F 1 15B-16 Trevenar ST N Ashbury NSW 2193\",'+
        '          \"siteName\": \"Parkview\",'+
        '          \"locationDescriptor\": \"Near the school\",'+
        '          \"levelType\": \"F\",'+
        '          \"levelNumber\": \"1\",'+
        '          \"unitType\": \"APT\",'+
        '          \"unitNumber\": \"1\",'+
        '          \"lotNumber\": \"12345\",'+
        '          \"planNumber\": \"N-78238\",'+
        '          \"roadNumber1\": \"15B\",'+
        '          \"roadNumber2\": \"16\",'+
        '          \"roadName\": \"Trevenar\",'+
        '          \"roadTypeCode\": \"ST\",'+
        '          \"roadSuffixCode\": \"N\",'+
        '          \"locality\": \"Ashbury\",'+
        '          \"postCode\": \"2193\",'+
        '          \"state\": \"NSW\",'+
        '          \"complexAddress\": {'+
        '            \"siteName\": \"Building A\",'+
        '            \"roadNumber1\": \"1\",'+
        '            \"roadNumber2\": \"2\",'+
        '            \"roadName\": \"Park\",'+
        '            \"roadTypeCode\": \"LANE\",'+
        '            \"roadSuffixCode\": \"N\"'+
        '          }'+
        '        },'+
        '        \"aliasAddresses\": ['+
        '          {'+
        '            \"unstructured\": \"Building A 1-2 Park LANE N Parkview 12345 APT 1 F 1 13A-16 Trevenar ST N Ashbury NSW 2193\",'+
        '            \"siteName\": \"Parkview\",'+
        '            \"locationDescriptor\": \"Near the school\",'+
        '            \"levelType\": \"F\",'+
        '            \"levelNumber\": \"1\",'+
        '            \"unitType\": \"APT\",'+
        '            \"unitNumber\": \"1\",'+
        '            \"lotNumber\": \"12345\",'+
        '            \"planNumber\": \"N-78238\",'+
        '            \"roadNumber1\": \"15\",'+
        '            \"roadNumber2\": \"16\",'+
        '            \"roadName\": \"Trevenar\",'+
        '            \"roadTypeCode\": \"ST\",'+
        '            \"roadSuffixCode\": \"N\",'+
        '            \"locality\": \"Ashbury\",'+
        '            \"postCode\": \"2193\",'+
        '            \"state\": \"NSW\",'+
        '            \"complexAddress\": {'+
        '              \"siteName\": \"Building A\",'+
        '              \"roadNumber1\": \"1\",'+
        '              \"roadNumber2\": \"2\",'+
        '              \"roadName\": \"Park\",'+
        '              \"roadTypeCode\": \"LANE\",'+
        '              \"roadSuffixCode\": \"N\"'+
        '            }'+
        '          }'+
        '        ],'+
        '        \"geocode\": {'+
        '          \"latitude\": \"37.8136\",'+
        '          \"longitude\": \"144.9631\",'+
        '          \"geographicDatum\": \"GDA94\",'+
        '          \"srid\": \"4283\"'+
        '        },'+
        '        \"primaryAccessTechnology\": {'+
        '          \"technologyType\": \"Fibre\",'+
        '          \"serviceClass\": 3,'+
        '          \"rolloutType\": \"Brownfield\"'+
        '        }'+
        '      },'+
        '      \"relationships\": {'+
        '        \"containmentBoundaries\": {'+
        '          \"data\": ['+
        '            {'+
        '              \"id\": \"ADA-001\",'+
        '              \"type\": \"networkBoundary\"'+
        '            },'+
        '            {'+
        '              \"id\": \"MPS-001\",'+
        '              \"type\": \"networkBoundary\"'+
        '            }'+
        '          ]'+
        '        },'+
        '        \"MDU\": {'+
        '          \"data\": {'+
        '            \"id\": \"LOC212345678901\",'+
        '            \"type\": \"location\"'+
        '          }'+
        '        }'+
        '      }'+
        '    }'+
        '  ],'+
        '  \"included\": ['+
        '    {'+
        '      \"type\": \"networkBoundary\",'+
        '      \"id\": \"ADA-001\",'+
        '      \"attributes\": {'+
        '        \"boundaryType\": \"ADA\",'+
        '        \"status\": \"INSERVICE\",'+
        '        \"technologyType\": \"Fibre\"'+
        '      }'+
        '    },'+
        '    {'+
        '      \"type\": \"networkBoundary\",'+
        '      \"id\": \"MPS-001\",'+
        '      \"attributes\": {'+
        '        \"boundaryType\": \"MPS\",'+
        '        \"status\": \"INSERVICE\",'+
        '        \"technologyType\": \"Fibre\"'+
        '      }'+
        '    }'+
        '  ]'+
        '}';
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,jsonInput,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        AddressSearch_CX.getQualificationDetailsfromExtSystem('LOC123456789012');
        ConsoleSearchWizardPageController.getAddressSearchData(stradd);
        ConsoleSearchWizardPageController.getSiteAddressSearchData1(stradd);
        AddressSearch_CX.addressWrapper stradd1 = new  AddressSearch_CX.addressWrapper();
        stradd1.address = site.Name;
        stradd1.locationId = site.Location_Id__c ;
        ConsoleSearchWizardPageController.getAddressSearchData(stradd1);
     }

    static void insertContactAccountCaseData(){
             con = new Contact();
             con.LastName='Test';
             con.Email = 'test@test.com';
             con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId();
             insert con;
             
             site = new Site__c();
             site.Name = '100 Waverton St, Sydney, NSW 2000';
             site.Location_Id__c = 'LOC123456789012';
             site.Site_Address__c = '100 Waverton St, Sydney, NSW 2000';
             site.Technology_Type__c = 'Copper';
             site.Serviceability_Class__c = 10;
             site.Rollout_Type__c = 'Brownfields';
             site.Unit_Number__c = '1';
             site.Level_Number__c = '2015';
             site.Post_Code__c = '2015';
             site.Locality_Name__c = 'Test';
             site.Road_Type_Code__c = 'ST';
             site.Road_Name__c = 'Test';
             site.Road_Number_1__c = 'Test';
             site.Unit_Type_Code__c = 'Test';
             site.Lot_Number__c = '12';
             site.Road_Suffix_Code__c = 'CT';
             //site.Asset_Type__c = 'Test';
             //site.Asset_Number__c = '';
             site.state_Territory_Code__c = 'ACT';
             site.Latitude__c = '-33.77216378';
             site.Longitude__c = '151.08294035';
             site.Premises_Type__c = 'Residential';
             site.recordTypeId =[SELECT Id FROM RecordType where SobjectType='Site__c' and Name='Verified'].Id;
         insert site;
         
         Site_Contacts__c siteCt = new Site_Contacts__c();
              siteCt.Is_Residential_Address__c = true;
              siteCt.Related_Contact__c = con.id;
              siteCt.Related_Site__c = site.id;
              siteCt.Role__c = 'General Public';
          insert siteCt ;
         
         String RTId = [SELECT Id FROM RecordType where SobjectType='Case' and Name='Query'].Id;
             cs = new Case();
             cs.Phase__c = 'Information';
             cs.Category__c = '(I) Communications';
             cs.Sub_Category__c = 'Discovery Centre';
             cs.status = 'Open';
             cs.Priority = '3-General';
             cs.Origin = 'Phone';
             cs.Subject = 'Test subject';
             cs.Description = 'Test Description';
             cs.RecordTypeId = RTId ;
             cs.Site__c = site.id;
             cs.ContactId = con.id;
             cs.Primary_Contact_Role__c = 'General Public';
          insert cs;
     }
     
     static void insertCustomSettingData(){
       csdataList = new List<Case_Console_Fields__c>();
       Case_Console_Fields__c csQueryData = new Case_Console_Fields__c();
       csQueryData.Name = 'Query';
       csQueryData.Field_Set_Name__c = 'FS_Query_Console_Fields';
       csQueryData.Record_Type__c = 'Query';
       csQueryData.Is_Anonymous_Reason_Applicable__c = true;
       
       Case_Console_Fields__c csUCData = new Case_Console_Fields__c();
       csUCData.Name = 'Urgent Complaint';
       csUCData.Field_Set_Name__c = 'FS_Urgent_Complaint_Console_Fields';
       csUCData.Record_Type__c = 'Urgent Complaint';
       csUCData.Is_Anonymous_Reason_Applicable__c = false;
       
       csdataList.add(csQueryData);
       csdataList.add(csUCData);
       
       insert csdataList;
    }
}