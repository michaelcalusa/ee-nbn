/*
Author              : Shuo
Created Date        : 14 June 2018
Description         : Controller extensions for Console Site Details panel
Test Class          : ConsoleSiteDetailsPanelController_Test
Modification Log    :
-----------------------------------------------------------------------------------------------------------------
History <Date>      <Authors Name>              <Brief Description of Change>

*/

public with sharing class ConsoleSiteDetailsPanelController {

    ApexPages.StandardSetController standardController;
    public Id siteId ;
    public Id caseId ;
    public set<String> RECORDTYPENAMELIST = new set<String>{'Medical Alarm','Fire & Lift'} ;
    public case displayCase {get;set;}
    public boolean autoRefreshSite {get;set;}
    public ConsoleSiteDetailsPanelController(ApexPages.StandardController stdController) {
         case c = (case)stdController.getRecord();
         autoRefreshSite = false;
         caseId = c.id;
         system.debug('Case:' + c);
         if (caseId != null) {
             String recordTypeName = Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfosById().get(c.RecordTypeId).getName();
             system.debug('recordTypeName' + recordTypeName);
             if (recordTypeNameLIST.contains(recordTypeName)) {
                 siteId = c.Site__c;
                 displayCase = getCase(caseId);
                 autoRefreshSite = true;
             } else {
                 siteId = null;
             }
         }
    }

//    function to get Case Site Details to the new site information after re-qualify
    public case getCase(id CaseId){
        case c = [select id, SiteRecordType__c ,site__r.Serviceability_Class__c, site__r.Rollout_Type__c, site__r.Technology_Type__c,
                site__r.Location_Id__c, site__r.Site_Address__c,site__r.Qualification_Date__c,site__r.RecordTypeId, site__r.Unit_Number__c,
                site__r.Road_Name__c, site__r.Level_Number__c, site__r.Lot_Number__c, site__r.Road_Number_1__c, site__r.Road_Number_2__c,
                site__r.Road_Suffix_Code__c,site__r.Post_Code__c, site__r.Locality_Name__c, site__r.State_Territory_Code__c,
                site__r.Requalification_Failed_Date__c,Site__r.Premises_Type__c,Site_Disconnect_Date__c,Site__r.SAM__c,
                site__r.MDU_LocId__c,Site__r.Latitude__c,Site__r.Longitude__c,site__r.Address_Site_Name__c
        from case where id = :CaseId];
        return c;
    }

//  function to call  vf controller to re-qualify site
    public void init(){
        system.debug('siteId: ' + siteId);
        system.debug('List:' + Schema.SObjectType.case.fieldSets.FS_Case_Related_Site_Details);
        if (siteId != null) {
            for (site__c s: [SELECT Id, Name,Serviceability_Class__c, Rollout_Type__c, Technology_Type__c,
                    Location_Id__c, Site_Address__c,Qualification_Date__c,RecordTypeId, Unit_Number__c,
                    Road_Name__c, Level_Number__c, Lot_Number__c, Road_Number_1__c, Road_Number_2__c,
                    Road_Suffix_Code__c,Post_Code__c, Locality_Name__c, State_Territory_Code__c,
                    Requalification_Failed_Date__c, CreatedDate FROM Site__c where id = :siteId]) {
                    if (string.isBlank(s.Location_Id__c)) {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'!!!ERROR: Site without Location Id. Please manually qualify the site.');
                        ApexPages.addMessage(myMsg);
                    } else {
                        SiteQualification_CX SiteQualification_CX_Con = new SiteQualification_CX(new ApexPages.StandardController(s));
                        try{
                            if (!test.isRunningTest()) {
                                SiteQualification_CX_Con.reQualifySiteRecord();
                            }
                            displayCase = getCase(caseId);
                        } catch(exception e){
                            system.debug('errorMsg: ' + e.getMessage());
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'!!!ERROR: ' +e.getMessage());
                            ApexPages.addMessage(myMsg);
                        }
                    }
            }
        }
        system.debug('autoRefreshSite: ' + autoRefreshSite);
        system.debug('displayCase: ' + displayCase);
    }
}