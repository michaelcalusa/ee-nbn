/*
Author              : Shuo
Created Date        : 14 June 2018
Description         : Test Class for ConsoleSiteDetailsPanelController
Modification Log    :
-----------------------------------------------------------------------------------------------------------------
History <Date>      <Authors Name>              <Brief Description of Change>

*/
@IsTest
private class ConsoleSiteDetailsPanelController_Test {
    @IsTest
    static void testConsoleSiteDetailsPanelController() {

        // Create Site record
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        Site__c UnverifiedSite = new Site__c();
        UnverifiedSite.RecordTypeId=recordTypeId;
        UnverifiedSite.Location_Id__c = 'LOC000106753648';
        UnverifiedSite.isSiteCreationThroughCode__c =true;
        insert UnverifiedSite;


        // Create Contact record
        ID conrecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId();
        Contact contactRec = new Contact ();
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        contactRec.recordtypeid = conrecordTypeId;
        insert contactRec;


        Id marRecTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId();
        Case marcase = new Case(RecordTypeId=marRecTypeId,
                                Site__c =  UnverifiedSite.Id,
                                ContactId=contactRec.id,
                                Case_Concat__c = String.valueof(UnverifiedSite.Id)+String.valueof(contactRec.id));
        insert marcase;

        Apexpages.StandardController sc = new Apexpages.standardController(marcase);
        ConsoleSiteDetailsPanelController ext = new ConsoleSiteDetailsPanelController(sc);
        ext.init();


    }
}