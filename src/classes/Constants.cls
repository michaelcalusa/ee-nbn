/*Created By : Vipul Jain
Purpose: A class to maintain Constants, as a pert of ACT-15536
Modified By: Justin Jacob
Purpose: as a part of ACT-15260 
*/

public class Constants {
    public static final String Acknowledged_Held='Acknowledged - Held';
    public static final String Inprogress_Held ='In progress - Held';
    public static final String Inprogress='In progress';
    public static final String Held='Held';
    public static final String New1 ='New';
    public static final String Assigned='Assigned';
    public static final String Inprogress_Pending='In progress - Pending';
    public static final String Acknowledged_Pending='Acknowledged - Pending';
    public static final String Pending='Pending';
    public static final String Right_Second_Time_Team='Right Second Time Team';
    public static final String NBNR2TLeader = 'NBN Right Second Time Team Leader';
    public static final String NBNR2TUser = 'NBN Right Second Time User';
    public static final String NBNCCTLeader='NBN Customer Connections Team Leader';
    public static final String NBNCCUser='NBN Customer Connections User';
    public static final String CUSTOMER_CONNECTIONS_TEAM = 'Customer Connections Team';
    //Modified FOr ACT-16605 - START
    public static final String AWAITING_TRIAGE = 'Awaiting remediation triage';
    public static final String COMPLETED = 'Completed';
    public static final String REMEDIATIONNOPRD = 'Remediation In Progress - No PRD ';
    public static final String CONTACT = 'Contact';
    public static final String EXTERNAL = 'External';
    public static final String INTERNAL = 'Internal';
    public static final String PRIORITYGENERAL = '3-General';
    public static final String RemInProgPRDAvail = 'Remediation In Progress - PRD Available';
    public static final String OpenValue = 'Open';
    public static final String CustConCM = 'Cust Conn CM';
    //Modified FOr ACT-16605 - END
    //Modified for ACT-17170 - Start
    public static final String DOMTEAM = 'Disconnection Order Management Team';
    //Modified for ACT-17170 - End
    //Added by Shubham for ACT-17167 #Start
    public static final String NBN_DOM_Leader = 'NBN DOM Team Leader';
    public static final String NBN_DOM_User = 'NBN DOM User';
    //Added by Shubham for ACT-17167 #End
    //Added Below Line by Vipul for ACT-17048 #Start
    public static Boolean stopRecursion=false;
    //Added by Sunaiyana
    public static final String stagesValue = 'Pre - nbn Order Planning';
    public static final String SDM_WORK_ORDER_RECORDTYPE = 'Activations WorkOrder';
    public static final String SDM_OPPORTUNITY_RECORDTYPE = 'Pre-Order';
    public static final String account_contact_recordType = 'Business End Customer';
    public static final String roleName = 'General Manager Business Segment Address Activate and Assure';
    public static final String profileName = 'SDM User Profile';
    public static final String redirectParam = 'vfRetURLInSFX';
}