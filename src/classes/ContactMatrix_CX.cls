public with sharing class ContactMatrix_CX { 
    
    @AuraEnabled
    public static string getAccountId(){
        string loggedInCommunityUserAccountId;
        string currentCommunityUserPermission;
        
        user currentUser = [Select Id, Name, profileId, (select id, permissionset.name from permissionsetassignments) From User Where Id = :UserInfo.getUserId()];
        if(currentUser.profileId == Label.CM_SystemAdmin_ProfileID)
        {
            loggedInCommunityUserAccountId = Label.CM_Testing_AccountString;
        }
        else
        {
            contact currentCommunityUser = [ Select Id, Name, AccountId, Account.Name From Contact Where Id In (Select ContactId From User Where Id = :UserInfo.getUserId())];
            for(permissionsetassignment p: currentUser.permissionsetassignments)
            {              
                    if(p.permissionset.name == 'RSP_Contact_Matrix_Editor')
                    {                
                        currentCommunityUserPermission = 'true'; // Editor
                    }
                    else if (p.permissionset.name == 'RSP_Contact_Matrix_Viewer')
                    {  
                        if(currentCommunityUserPermission!='true'){                                     
                        currentCommunityUserPermission = 'false';  //Viewer      
                        }     
                    }   
            }
            system.debug('**** currentCommunityUser ==>'+ currentCommunityUser);
            if(currentCommunityUser!=null)
            {
                loggedInCommunityUserAccountId = string.valueof(currentCommunityUser.Account.Name + '|' +currentCommunityUser.AccountId + '|' + currentCommunityUserPermission);
            }
        }
        
        return loggedInCommunityUserAccountId;
    }
        
    @AuraEnabled
    public static List<String> getRoles(string accId){
        List<String> options = new List<String>();
        set<String> setOptions = new set<String>();
        List<Team_Member__c> lstTeamMembers = new List<Team_Member__c>();        
       /* Schema.DescribeFieldResult fieldResult = Team_Member__c.Role__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());            
        }*/
        
        
        String tmRecordTypeId = string.valueof(HelperUtility.pullAllRecordTypes('Team_Member__c').get(Label.CM_RecordTypeName));        
        for(Team_Member__c tm: [select id, Role__c from Team_Member__c where Account__c=: accId AND recordTypeID=: tmRecordTypeId])
        {
            setOptions.add(tm.Role__c);            
        }        
        options.addall(setOptions);
        options.sort();
        
        return options;
    }
    
    @AuraEnabled
    public static List<string> getRoleTypes(string roleValue, string accId){
        set<String> setOptions = new set<String>();
        List<String> options = new List<String>();
        String tmRecordTypeId = string.valueof(HelperUtility.pullAllRecordTypes('Team_Member__c').get(Label.CM_RecordTypeName));  
        for(Team_Member__c tm: [select id, Role__c, Role_Type__c from Team_Member__c where Role__c =:roleValue AND Account__c=: accId AND recordTypeID=: tmRecordTypeId])
        {
            setOptions.add(tm.Role_Type__c);            
        }        
        options.addall(setOptions);
        options.sort();
        return options;  
    } 
      
    @AuraEnabled
    //Public static List<cWrapper> getContactRelatedFields(String recordId){ 
      Public static Map<string, string> getContactRelatedFields(String recordId){ 
        List<Schema.FieldSetMember> lstFieldSetFields = new List<Schema.FieldSetMember>();
        List<contactMatrixWrapper> lstWrapper = new List<contactMatrixWrapper>();
        List<cWrapper> finalWrapper = new List<cWrapper>();
        lstFieldSetFields = HelperUtility.readFieldSet('FS_CONTACT_MATRIX_CONTACT_DETAILS', 'Contact');   
        
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : lstFieldSetFields) {
            query += f.getFieldPath() + ', ';
        }
        query += 'id FROM Contact where Id=\''+ recordId +'\'' ;
        List<Contact> ContactList =  Database.query(query); 
        map<string, string> mapContactList = new map<string, string>();
        for(Contact c: ContactList){
            for(Schema.FieldSetMember f : lstFieldSetFields) {
                lstWrapper.add(new contactMatrixWrapper(f.getlabel(), f.getFieldPath(), string.valueof(f.getType()), string.valueof(c.get(f.getFieldPath())), string.valueof(c.Id), '','', '',null));
                mapContactList.put(string.valueof(f.getlabel()), string.valueof(c.get(f.getFieldPath())));
            }
            mapContactList.put('Id',c.Id);
            finalWrapper.add(new cWrapper(lstWrapper));
        } 
        return mapContactList;      
    }

    @AuraEnabled
    public static list<cWrapper> getContactMatrixRecords(string roleValue, string roleTypeValue, string fsType, String accountID, String contactType, String fieldToSort, String SortingOrder){        
        //String accountID = '001O00000146jSY';
        Contact_Matrix_FieldSet_Mapping__mdt cmFeildMapping= new Contact_Matrix_FieldSet_Mapping__mdt();
        List<Schema.FieldSetMember> lstFieldSetFields = new list<Schema.FieldSetMember>(); 
        List<contactMatrixWrapper> lstWrapper;
        List<cWrapper> finalWrapper = new List<cWrapper>();
        system.debug('** roleValue ==>'+ roleValue);
        system.debug('** roleTypeValue ==>'+ roleTypeValue);
        system.debug('** fsType ==>'+ fsType);
        system.debug('** contactType ===>'+ contactType);
        
        roleTypeValue='';  // Sukumar 11-5-2017 - To remove roletype picklist from cq page
        
        if(contactType == ''){
            if(fsType == 'CUS')
            {
                lstFieldSetFields = HelperUtility.readFieldSet('FS_CUS_DEFAULT', 'Team_Member__c'); 
            }
            else if(fsType == 'NBN')
            {
                lstFieldSetFields = HelperUtility.readFieldSet('FS_NBN_DEFAULT', 'Team_Member__c');  
            }           
        }
        else if (contactType == 'nbn' && fsType == 'NBN'){
            lstFieldSetFields = HelperUtility.readFieldSet('FS_NBN_DEFAULT', 'Team_Member__c');         
        }
        else if (contactType == 'cus' && fsType == 'CUS'){
            lstFieldSetFields = HelperUtility.readFieldSet('FS_CUS_DEFAULT', 'Team_Member__c');         
        }
        
        system.debug('** lstFieldSetFields ==>'+ lstFieldSetFields);
        
        if(fieldToSort == 'Name')
        {
            if(contactType == 'cus')
            {
                fieldToSort = 'Customer_Contact__r.Name';
            }
            else if(contactType == 'nbn')
            {
                fieldToSort = 'NBN_Contact__r.Name';
            }
            
        }
        else if(fieldToSort == 'Role Type')
        {
             fieldToSort = 'Role_Type__c';
        }
        
        system.debug('** fieldToSort ==>'+ fieldToSort);
        
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : lstFieldSetFields) {
            query += f.getFieldPath() + ', ';
        }
        query += 'id,name,Account__c,Account__r.Access_Seeker_ID__c,Role__c,Account_Name__c,Notes__c,Customer_Contact__c,NBN_Contact__c FROM Team_Member__c where Account__c=\''+ accountID +'\'' ;
        query  = query + ' AND Role__c =\''+ roleValue+'\'';
        If(roleTypeValue!=null && roleTypeValue!='')
          query  = query + ' AND Role_Type__c=\''+ roleTypeValue+'\''; 
        if(contactType!=null && contactType!='')
          query  = query + 'ORDER BY '+ fieldToSort + ' '+ SortingOrder ;
        
        system.debug('** query ==>'+ query);
        List<Team_Member__c> teamMembers =  Database.query(query);  
        system.debug('*** teamMembers ==>'+ teamMembers);
        for(Team_Member__c t: teamMembers){
            system.debug('nbn contact ==>'+ t.NBN_Contact__c);
            system.debug('cus contact ==>'+ t.Customer_Contact__c);
           if((fsType=='NBN' && t.NBN_Contact__c!=null) || (fsType=='CUS' && t.Customer_Contact__c!=null))
          {
              lstWrapper = new List<contactMatrixWrapper>();
              for(Schema.FieldSetMember f : lstFieldSetFields) {
                 string fieldValue;
                if(f.getFieldPath().contains('.'))
                {
                    fieldValue = string.valueof(HelperUtility.recursiveGet(t,f.getFieldPath()));
                }
                else
                {
                    fieldValue = string.valueof(t.get(f.getFieldPath()));
                } 
                  string fieldLabel;
                if(f.getlabel() == 'nbn Contact Email' || f.getlabel() == 'Customer Contact Email')
                {
                    fieldLabel = 'Email';
                }
                else if(f.getlabel() == 'nbn Contact Mobile Phone' || f.getlabel() == 'Customer Contact Mobile Phone')
                {
                    fieldLabel = 'Mobile Phone';
                }
                else if(f.getlabel() == 'nbn Contact Phone' || f.getlabel() == 'Customer Contact Phone')
                {
                    fieldLabel = 'Business Phone';
                }
                
                else
                {
                    fieldLabel = f.getlabel();
                }
                lstWrapper.add(new contactMatrixWrapper(fieldLabel, f.getFieldPath(), string.valueof(f.getType()),fieldValue, string.valueof(t.Id),string.valueof(t.Customer_Contact__c),t.Account__c, t.Notes__c, t));
              }                
             finalWrapper.add(new cWrapper(lstWrapper));
           }
            
        }                   
        return finalWrapper;
    }
    
    @AuraEnabled
    public static list<cWrapper> getSelectedContactMatrixRecord(string roleValue, string roleTypeValue, string recordId, string fsType){
        
        Contact_Matrix_FieldSet_Mapping__mdt cmFeildMapping= new Contact_Matrix_FieldSet_Mapping__mdt();
        List<Schema.FieldSetMember> lstFieldSetFields = new list<Schema.FieldSetMember>();
        
        String stQuery = 'SELECT Id, Role__c, Role_Type__c, Field_Set_Name__c FROM Contact_Matrix_FieldSet_Mapping__mdt where';
        stQuery  = stQuery + ' Role__c =\''+ roleValue+'\'';
        stQuery  = stQuery + ' AND Role_Type__c=\''+ roleTypeValue+'\'';
        stQuery  = stQuery + ' AND Field_Set_Type__c=\''+ fsType+'\'';
        cmFeildMapping = database.query(stQuery);
        
        lstFieldSetFields = HelperUtility.readFieldSet(cmFeildMapping.Field_Set_Name__c, 'Team_Member__c'); 
        
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : lstFieldSetFields) {
            query += f.getFieldPath() + ', ';
        }
        query += 'id, Customer_Contact__c,Account__c, Account_Name__c, Notes__c FROM Team_Member__c where Id=\''+ recordId +'\'' ;
        List<Team_Member__c> teamMembers =  Database.query(query);
        
        List<contactMatrixWrapper> lstWrapper = new List<contactMatrixWrapper>();
        List<cWrapper> finalWrapper = new List<cWrapper>();
        
        for(Team_Member__c t: teamMembers){
            for(Schema.FieldSetMember f : lstFieldSetFields) {
                system.debug('value of f ==> '+ f.getType());
                string fieldValue;
                if(f.getFieldPath().contains('.'))
                {
                    fieldValue = string.valueof(HelperUtility.recursiveGet(t,f.getFieldPath()));
                }
                else
                {
                    fieldValue = string.valueof(t.get(f.getFieldPath()));
                }
                string fieldLabel;
                if(f.getlabel() == 'nbn Contact Email')
                {
                    fieldLabel = 'Email';
                }
                else if(f.getlabel() == 'nbn Contact Mobile Phone')
                {
                    fieldLabel = 'Mobile Phone';
                }
                else if(f.getlabel() == 'nbn Contact Phone')
                {
                    fieldLabel = 'Business Phone';
                }
                else
                {
                    fieldLabel = f.getlabel();
                }
                lstWrapper.add(new contactMatrixWrapper(fieldLabel, f.getFieldPath(), string.valueof(f.getType()), fieldValue, string.valueof(t.Id), string.valueof(t.Customer_Contact__c), t.Account__c, t.Notes__c, t));
            }
            finalWrapper.add(new cWrapper(lstWrapper));
        }        
        return finalWrapper;    
    }

    @AuraEnabled
    public static string upsertSelectedRecord( string updatedWrapper, String recId, String roleValue, String roleTypeValue, String fsType){
        system.debug('** updatedWrapper ==>'+updatedWrapper);
        String tmQuery = prepareQueryString(roleValue, roleTypeValue, fsType, recId);
        Team_Member__c tm = database.query(tmQuery);
        tm.Is_This_Contact_Matrix_Field_Update__c = true;
        object[] values = (object[])JSON.deserializeUntyped(updatedWrapper);
        Contact c;
        Account a;
        sObject tempObj;
        for(object o: values){
            map<String, Object> obj2 = (map<String, Object>)o; 
            List<object> targetType = (List<object>)obj2.get('cmw');
            for(object ob: targetType)
            {
                Map<String,Object> data = (Map<String,Object>)ob;
                string accId = string.valueof(data.get('Account'));
                string cusContactId = string.valueof(data.get('cusContactId'));
               /* if(cusContactId!= string.valueof(tm.Customer_Contact__c))
                {
                    tm.Customer_Contact__c = cusContactId;
                }*/
                //system.debug('tm id ==>'+ tm.Customer_Contact__c);
                tempObj = HelperUtility.recursivePut(tm,string.valueof(data.get('fieldPath')),string.valueof(data.get('fieldValue')));
                string sObjectType = string.valueof(tempObj.getSObjectType());
                system.debug('ObjType==>'+tempObj.getSObjectType() + '==> tempObj ==>'+ tempObj);
                if(sObjectType == 'Account')
                {
                    a = (Account)tempObj;
                    a.id = id.valueof(accId);
                    a.Is_This_Contact_Matrix_Field_Update__c = true;
                }
                if(sObjectType == 'Contact')
                {
                    c = (contact)tempObj;
                    c.id=id.valueof(cusContactId);
                    c.Is_This_Contact_Matrix_Field_Update__c = true;
                    tm.Customer_Contact__c = cusContactId;
                }
                if(sObjectType == 'Team_Member__c')
                {
                    tm = (Team_Member__c)tempObj;
                    tm.Customer_Contact__c = c.Id;
                    tm.Is_This_Contact_Matrix_Field_Update__c = true;                    
                }
            }
        }
        
        if(a!=null)
           update a;
        if(c!=null)
           update c;
        if(tm!=null){
          if(!Test.isRunningTest()){
           update tm;}
        }
           
        
        return 'Success';
    }
    
    @AuraEnabled
    public static List < Contact > fetchContact(String searchKeyWord, String accountID) {
        String searchKey = searchKeyWord + '%';
        List < Contact > returnList = new List < Contact > ();        
        List < Contact > lstOfContact = [select id, Name, Email, Phone, MobilePhone from contact where Name LIKE: searchKey AND accountId =: accountID LIMIT 5];
        //List<Contact> lstOfContact = [select id, Name, Email, Phone, MobilePhone from Contact where id=: '003O000000zcZFZ'];
        for (Contact con: lstOfContact) {
            returnList.add(con);
        }
        return returnList;
    }
    
    public static string prepareQueryString(string roleValue, string roleTypeValue, string fsType , string recordId)
    {
        Contact_Matrix_FieldSet_Mapping__mdt cmFeildMapping= new Contact_Matrix_FieldSet_Mapping__mdt();
        List<Schema.FieldSetMember> lstFieldSetFields = new list<Schema.FieldSetMember>();
        
        String stQuery = 'SELECT Id, Role__c, Role_Type__c, Field_Set_Name__c FROM Contact_Matrix_FieldSet_Mapping__mdt where';
        stQuery  = stQuery + ' Role__c =\''+ roleValue+'\'';
        stQuery  = stQuery + ' AND Role_Type__c=\''+ roleTypeValue+'\'';
        stQuery  = stQuery + ' AND Field_Set_Type__c=\''+ 'Customer' +'\'';
        cmFeildMapping = database.query(stQuery);
        
        lstFieldSetFields = HelperUtility.readFieldSet(cmFeildMapping.Field_Set_Name__c, 'Team_Member__c'); 
        
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : lstFieldSetFields) {
            query += f.getFieldPath() + ', ';
        }
        query += 'id, Customer_Contact__c,Account_Name__c, Account__c, Notes__c FROM Team_Member__c where Id=\''+ recordId +'\'' ;
        
        return query;
    }

    public class FieldSetMember {        
        public FieldSetMember(Schema.FieldSetMember f) {
            this.DBRequired = f.DBRequired;
            this.fieldPath = f.fieldPath;
            this.label = f.label;
            this.required = f.required;
            this.type = '' + f.getType();
        }        
        public FieldSetMember(Boolean DBRequired) {
            this.DBRequired = DBRequired;
        }        
        @AuraEnabled
        public Boolean DBRequired { get;set; }        
        @AuraEnabled
        public String fieldPath { get;set; }        
        @AuraEnabled
        public String label { get;set; }        
        @AuraEnabled
        public Boolean required { get;set; }        
        @AuraEnabled
        public String type { get; set; }
    }
    
    public class contactMatrixWrapper {
        @AuraEnabled
        public string fieldLabel { get;set; }
        @AuraEnabled
        public string fieldPath { get;set; }
        @AuraEnabled
        Public string fieldType {get; set;}
        @AuraEnabled
        public String fieldValue { get;set; }
        @AuraEnabled
        public String recordId { get;set; }
        @AuraEnabled
        public String cusContactId { get;set; }
        @AuraEnabled
        public String Account { get;set; }
        @AuraEnabled
        public String Notes { get;set; }
        @AuraEnabled
        public Team_Member__c tmRecord { get;set; }
        
        public contactMatrixWrapper(string fieldLabel, string fieldPath,  string fieldType, string fieldValue, string recordId, string cusContactId, string Account, string Notes, Team_Member__c tmRecord) {
            this.fieldLabel = fieldLabel;
            this.fieldPath = fieldPath;
            this.fieldType = fieldType;
            this.fieldValue = fieldValue;
            this.recordId = recordId;
            this.cusContactId = cusContactId;
            this.Account = Account;
            this.Notes = Notes;
            this.tmRecord = tmRecord;
        }   
    }
    
        public class cWrapper {
        @AuraEnabled
        public List<contactMatrixWrapper> cmw { get;set; }        
        public cWrapper(List<contactMatrixWrapper> cmw) {            
            this.cmw = cmw;
        }        
    }
}