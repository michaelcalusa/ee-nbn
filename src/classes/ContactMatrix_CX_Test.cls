/*****************************************************************************
Class Name  :  ContactMatrix_CX_TEST
Class Type  :  Test Class 
Version     :  1.0 
Created Date:  25/03/2017
Function    :  This class contains unit test scenarios for ContactMatrix_CX apex class
Used in     :  None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* SUKUMAR SALLA             25/03/2017                 Created
******************************************************************************/
@isTest
public class ContactMatrix_CX_Test {
  
  static testMethod void testmethod1(){
    
    // Create Customer Account & Contact data
    List<Account> lstAcc = TestDataUtility.getListOfAccounts(1, true);
    List<Contact> lstCon = TestDataUtility.getListOfContact(1, false);
      
    for(contact con : lstCon){
         con.AccountId = lstAcc[0].id;
     }  
    insert lstCon;
    
    // Create Company Account & Contact data
    List<Account> lstCompanyAcc = TestDataUtility.getListOfCompanyAccounts(1, false);
    lstCompanyAcc[0].Name = 'nbn';
     insert lstCompanyAcc;
    List<Contact> lstCompanyCon = TestDataUtility.getListOfCompanyContact(1, false);
    
    for(contact con: lstCompanyCon){
       con.AccountId = lstCompanyAcc[0].id;
       con.Status__c= 'Active';
    }
    insert lstCompanyCon;
    
    //Create Team Member data (Contact Matrix)
    List<Team_Member__c> lstTeamMembers = TestDataUtility.insertListOfTeamMembers(1,true,lstAcc[0],lstCon[0],lstCompanyCon[0]); 
    system.debug('***lstTeamMembers ==>'+ lstTeamMembers );
    
    // Community Contact    
    Profile p = [SELECT Id FROM Profile WHERE Name='RSP Customer Community Plus Profile'];    
    Contact con = new Contact(LastName ='testCommunityUser',AccountId = lstAcc[0].id);
    insert con;  
    
    User user = new User(alias = 'testcu', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='testCommunityUser', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p.id, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
    insert user;
    
    //PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'RSP_Contact_Matrix_Editor'];
    //insert new PermissionSetAssignment(AssigneeId = user.id, PermissionSetId = ps.Id);
    
    system.runAs(user) {
            //statements to be executed by this test user.
            String updatedWrapper= '[{'+
               '\"cmw\":['+
                          '{' +
                             '\"Account\":\"'+ lstAcc[0].id  + '\",'+
                             '\"cusContactId\":\"'+lstCon[0].id+'\",'+
                             '\"fieldLabel\":\"Email\",'+
                             '\"fieldPath\":\"Customer_Contact__r.Email\",'+
                             '\"fieldType\":\"EMAIL\",'+
                             '\"fieldValue\":\"w@ewrwe.com\",'+
                             '\"recordId\":\"'+ lstTeamMembers[0].id+ '\"'+
                       '}]}]';
            ContactMatrix_CX obj = new ContactMatrix_CX();
            Test.startTest();
             ContactMatrix_CX.getAccountId();
             ContactMatrix_CX.getRoles(lstAcc[0].id);
             ContactMatrix_CX.getRoleTypes('Account Team',lstAcc[0].id); 
             ContactMatrix_CX.getContactRelatedFields(lstCon[0].id);             
             //ContactMatrix_CX.getContactMatrixRecords('Account Team','Official Notices Point of Contact','CUS',lstAcc[0].id);
             //ContactMatrix_CX.getContactMatrixRecords('Account Team','Official Notices Point of Contact','NBN',lstAcc[0].id);
             ContactMatrix_CX.getContactMatrixRecords('Account Team','Official Notices Point of Contact','CUS',lstAcc[0].id,'cus','Name','ASC');
             ContactMatrix_CX.getContactMatrixRecords('Account Team','Official Notices Point of Contact','NBN',lstAcc[0].id,'nbn','Name','ASC');
             ContactMatrix_CX.getSelectedContactMatrixRecord('Account Team','Official Notices Point of Contact',lstTeamMembers[0].id,'Customer');
             ContactMatrix_CX.fetchContact('test',lstAcc[0].id); 
             ContactMatrix_CX.upsertSelectedRecord(updatedWrapper, lstTeamMembers[0].id, 'Account Team','Official Notices Point of Contact', 'Customer');
             ContactMatrix_CX.prepareQueryString('Account Team','Official Notices Point of Contact','Customer',lstAcc[0].id);   
            Test.stopTest();
    }
  }
}