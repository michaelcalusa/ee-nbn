public class ContactService {
    private static Map<String, Id> CONTACT_RECORD_TYPES = HelperUtility.pullAllRecordTypes('Contact');

    public Contact getOrCreateContact(Id accountId, String contactRecordType, String firstName, String lastName, String contactNumber, String email, String additionalContactNumber, String jobTitle) {
        Contact contact = getContact(accountId, lastName, email);

        if (contact == null) {
            Id recordTypeId = CONTACT_RECORD_TYPES.get(contactRecordType);

            contact = new Contact();
            contact.AccountId = accountId;
            contact.RecordTypeId = recordTypeId;
            contact.FirstName = firstName;
            contact.LastName = lastName;
            contact.Email = email;
            contact.Phone = contactNumber;
            if (String.isNotBlank(additionalContactNumber)) {
                contact.OtherPhone = additionalContactNumber;
            }
            if (String.isNotBlank(jobTitle)) {
                contact.Job_Title__c = jobTitle;
            }

            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.AllowSave = true;
            Database.insert(contact, dml);
        }

        return contact;
    }

    private Contact getContact(Id accountId, String lastName, String email) {
        List<Contact> existingContacts = [
                SELECT Id
                FROM Contact
                WHERE AccountId = :accountId
                AND LastName = :lastName
                AND Email = :email
        ];

        if (existingContacts != null && existingContacts.size() > 0) {
            return existingContacts.get(0);
        }

        return null;
    }

}