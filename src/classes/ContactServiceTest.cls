@IsTest
public class ContactServiceTest {
    private static final String CONTACT_NUMBER = '04789456123';
    private static final String LAST_NAME = 'smith';
    private static final String EMAIL = 'john@smith.com';
    private static final String CUSTOMER_CONTACT = 'Customer Contact';

    private static Map<String, Id> ACCOUNT_RECORD_TYPES = HelperUtility.pullAllRecordTypes('Account');
    private static Map<String, Id> CONTACT_RECORD_TYPES = HelperUtility.pullAllRecordTypes('Contact');
    private static ContactService contactService = new ContactService();

    @IsTest
    private static void shouldReturnExistingContactWhenAccountLastNameAndEmailMatch() {
        Test.startTest();
        Id accountId = createAccount();

        Contact existingContact = createContact(accountId);

        Contact contact = contactService.getOrCreateContact(accountId, CUSTOMER_CONTACT, 'john', LAST_NAME, CONTACT_NUMBER, EMAIL, '', '');

        Assert.equals(contact.Id, existingContact.Id);

        Test.stopTest();
    }

    @IsTest
    private static void shouldCreateNewContactWhenAccountNotMatch() {
        Test.startTest();
        Id accountId = createAccount();
        Contact existingContact = createContact(accountId);
        Id recordTypeId = ACCOUNT_RECORD_TYPES.get('Customer');

        Account account = new Account();
        account.RecordTypeId = recordTypeId;
        account.ABN__c = '48123123124';
        account.Name = 'another company';
        insert account;

        Contact contact = contactService.getOrCreateContact(account.Id, CUSTOMER_CONTACT, 'john', LAST_NAME, CONTACT_NUMBER, EMAIL, '0212345678', 'manager');

        Assert.notEquals(contact.Id, existingContact.Id);

        Assert.equals('john', contact.FirstName);
        Assert.equals(LAST_NAME, contact.LastName);
        Assert.equals(CONTACT_NUMBER, contact.Phone);
        Assert.equals(EMAIL, contact.Email);
        Assert.equals('0212345678', contact.OtherPhone);
        Assert.equals('manager', contact.Job_Title__c);

        Test.stopTest();
    }

    @IsTest
    private static void shouldCreateNewContactWhenLastNameNotMatch() {
        Test.startTest();
        Id accountId = createAccount();
        Contact existingContact = createContact(accountId);

        Contact contact = contactService.getOrCreateContact(accountId, CUSTOMER_CONTACT, 'john', 'doe', CONTACT_NUMBER, EMAIL, '0212345678', 'manager');

        Assert.notEquals(contact.Id, existingContact.Id);

        Assert.equals('john', contact.FirstName);
        Assert.equals('doe', contact.LastName);
        Assert.equals(CONTACT_NUMBER, contact.Phone);
        Assert.equals(EMAIL, contact.Email);
        Assert.equals('0212345678', contact.OtherPhone);
        Assert.equals('manager', contact.Job_Title__c);

        Test.stopTest();
    }

    @IsTest
    private static void shouldCreateNewContactWhenEmailNotMatch() {
        Test.startTest();
        Id accountId = createAccount();
        Contact existingContact = createContact(accountId);

        Contact contact = contactService.getOrCreateContact(accountId, CUSTOMER_CONTACT, 'john', LAST_NAME, CONTACT_NUMBER, 'someotheremail@nowhere.com', '0212345678', 'manager');

        Assert.notEquals(contact.Id, existingContact.Id);

        Assert.equals('john', contact.FirstName);
        Assert.equals(LAST_NAME, contact.LastName);
        Assert.equals(CONTACT_NUMBER, contact.Phone);
        Assert.equals('someotheremail@nowhere.com', contact.Email);
        Assert.equals('0212345678', contact.OtherPhone);
        Assert.equals('manager', contact.Job_Title__c);

        Test.stopTest();
    }

    private static Contact createContact(Id accountId) {
        Contact existingContact = new Contact();
        existingContact.FirstName = 'Peter';
        existingContact.LastName = LAST_NAME;
        existingContact.Email = EMAIL;
        existingContact.Phone = CONTACT_NUMBER;
        existingContact.RecordTypeId = CONTACT_RECORD_TYPES.get(CUSTOMER_CONTACT);
        existingContact.AccountId = accountId;
        insert existingContact;
        return existingContact;
    }

    private static Id createAccount() {
        Id recordTypeId = ACCOUNT_RECORD_TYPES.get('Customer');
        Account account = new Account();
        account.RecordTypeId = recordTypeId;
        account.ABN__c = '11223491505';
        account.Name = 'a company';
        insert account;
        return account.Id;
    }
}