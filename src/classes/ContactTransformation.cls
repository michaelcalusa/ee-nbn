/*
Class Description
Creator: v-dileepathley
Purpose: This class will be used to transform and transfer Contact records from Contacts_Int to Contact
Modifications:
Dilip Athley 04/04/2017 - Added the Contact Type 'Consultant/Applicant' as part of BAU Story BAU-835.
*/
public class ContactTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
    public String conQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {
        
    }
    public ContactTransformation(String ejId)
    {
         exJobId = ejId;
         Batch_Job__c bj = new Batch_Job__c();
         bj.Name = 'ContactTransform'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
            conQuery = 'Select Account_Id__c,Address_2__c,Address_3__c,City__c,Contact_Types__c,Created_By__c,Modified_By__c,Owner__c,FSA__c,Latitude__c,Longitude__c,MDU_LOC_ID__c,Opt_Out__c,Premises_Type__c,Rollout_Type__c,SAM__c,Technology_Type__c,Description__c,Country__c,CRM_Contact_Id__c,CRM_Last_Modified_Date__c,CRM_Modified_Date__c,Development_Name__c,Email__c,Family_Name__c,Given_Name__c,Health_Safety_Considerations__c,IsProcessed__c,Location_ID__c,Mobile__c,Number_Street__c,Phone__c,Post_Code__c,PCM_Updated__c,Primary_Account_Name__c,Salutation__c,Schedule_Job__c,Stage_Application_Name__c,Social_Media_Account__c,Source__c,State__c,Status__c,Work_Email__c,Work_Mobile__c,Work_Phone__c,Validate_Address__c,Combined_Address__c,Work_Fax__c FROM Contact_Int__c where Transformation_Status__c = \'Extracted from CRM\' order by createddate asc';
            return Database.getQueryLocator(conQuery);   
    }
    public void execute(Database.BatchableContext BC,List<Contact_Int__C> conIlist)
    {
        try
        {
            
            List<Contact> conList = new List<Contact>();
            String recordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Contact'  and name = 'External Contact'].Id;
            
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.allowFieldTruncation = true;
            string conTypes = '';
            list <string> odId = new list<string>();
            String address, lat, lng, fsa, sam, mlId, pType, rType, tType, locId;
            for (contact_int__c c: conIlist)
            {
                if(!String.isBlank(c.Account_Id__c)&& c.Account_Id__c != 'No Match Row Id')
                {
                    string s = c.account_id__c;
                    odId.add(s);
                }
            }
            map<string,Id> acc = new map<string,Id>();
            map <String, Contact> conMap = new map<String, Contact>();
            for( Account a: [Select on_demand_id__c,Id from Account where on_demand_id__c in :odId FOR Update])
            {
                acc.put(a.on_demand_id__c, a.Id); 
            }
            for(Contact_Int__c con: conIlist)
            {
                conTypes = '';
                Contact cont = new Contact();
                
                cont.Salutation = con.Salutation__c;
                cont.FirstName = con.Given_Name__c;
                cont.LastName = con.Family_Name__c;
                cont.Description = con.Description__c;
                cont.CRM_OD_Created_By__c = con.Created_By__c;
                cont.CRMOD_Modified_By__c = con.Modified_By__c;
                cont.CRMOD_Owner__c = con.Owner__c;
                if(!String.isBlank(con.Account_Id__c)&& con.Account_Id__c != 'No Match Row Id')
                {
                    //Account acc = new Account(On_Demand_Id__c =  con.Account_Id__c);
                    cont.AccountId = acc.get(con.Account_Id__c);
                }
                
                if (!con.Validate_Address__c)
                {
                    locId = String.isNotBlank(con.Location_ID__c) ? 'Location Id:'+con.Location_ID__c+' ': '';
                    address = String.isNotBlank(con.Combined_Address__c) ?'Address: '+ con.Combined_Address__c : '';
                    lat = String.isNotBlank(con.Latitude__c) ?' Latitude: ' + con.Latitude__c : '' ;
                    lng = String.isNotBlank(con.Longitude__c) ?' Longitude: ' + con.Longitude__c : '';
                    fsa = String.isNotBlank(con.FSA__c) ?' FSA: ' + con.FSA__c : '';
                    sam = String.isNotBlank(con.SAM__c) ?' SAM: ' + con.SAM__c : '';
                    mlId = String.isNotBlank(con.MDU_LOC_ID__c) ?' MDU LOC Id: ' + con.MDU_LOC_ID__c : '';
                    pType = String.isNotBlank(con.Premises_Type__c) ?' Premise Type: ' + con.Premises_Type__c : '';
                    rType = String.isNotBlank(con.Rollout_Type__c) ?' Rollout Type: ' + con.Rollout_Type__c : '';
                    tType = String.isNotBlank(con.Technology_Type__c) ?' Technology Type:'+con.Technology_Type__c : '';
                    cont.Incomplete_Address__c =  locId+address+lat+lng +fsa+sam +mlId +pType+rType+tType;
                }
                cont.On_Demand_Id_P2P__c = con.CRM_Contact_Id__c;
                cont.Social_Media_Account__c = con.Social_Media_Account__c;
                cont.Opt_Out__c = con.Opt_Out__c;
                if(String.isNotBlank(con.Email__c))
                {
                    cont.Email = con.Email__c;
                }
                else
                {
                    cont.Email = con.Work_Email__c;
                }
                if(String.isNotBlank(con.Mobile__c))
                {
                    cont.Preferred_Phone__c = con.Mobile__c;
                }
                else if (String.isNotBlank(con.Phone__c))
                {
                    cont.Preferred_Phone__c = con.Phone__c;
                }
                else if (String.isNotBlank(con.Work_Phone__c))
                {
                    cont.Preferred_Phone__c = con.Work_Phone__c;
                }
                else
                {
                    cont.Preferred_Phone__c = con.Work_Mobile__c;
                }
                
                if(String.isNotBlank(con.Mobile__c) && String.isNotBlank(con.Phone__c))
                {
                    cont.Alternate_Phone__c = con.Phone__c;
                }
                else if ((String.isBlank(con.Mobile__c) && String.isNotBlank(con.Phone__c) && String.isNotBlank(con.Work_Phone__c)) || 
                (String.isNotBlank(con.Mobile__c) && String.isBlank(con.Phone__c) && String.isNotBlank(con.Work_Phone__c)))
                {
                    cont.Alternate_Phone__c = con.Work_Phone__c;
                }
                else if ((String.isBlank(con.Mobile__c) && String.isNotBlank(con.Phone__c) && String.isBlank(con.Work_Phone__c)) || 
                (String.isNotBlank(con.Mobile__c) && String.isBlank(con.Phone__c) && String.isBlank(con.Work_Phone__c)) ||
                (String.isBlank(con.Mobile__c) && String.isBlank(con.Phone__c) && String.isNotBlank(con.Work_Phone__c)))
                {
                    cont.Alternate_Phone__c = con.Work_Mobile__c;
                }
                cont.LeadSource = con.Source__c;
                cont.Preferred_Contact_Method__c = con.PCM_Updated__c;
                cont.Health_Safety_Considerations__c = con.Health_Safety_Considerations__c;
                cont.Fax = con.Work_Fax__c;
                if(String.isNotBlank(con.Contact_Types__c))
                {
                    if (con.Contact_Types__c.contains('Cabler')|| con.Contact_Types__c.contains('Consultant') || con.Contact_Types__c.contains('Consultant/Applicant') || con.Contact_Types__c.contains('Contractor') || 
                    con.Contact_Types__c.contains('Electrician')||con.Contact_Types__c.contains('Engineer')||con.Contact_Types__c.contains('Stakeholder'))
                    {
                        conTypes= 'Contractor';
                    }
                    if(con.Contact_Types__c.contains('Body Corp/Owners Corp')|| con.Contact_Types__c.contains('Strata Manager'))
                    {
                        if(String.isNotBlank(conTypes))
                        {
                            conTypes=conTypes+';'+'Body Corporate/Strata';
                        }
                        else
                        {
                            conTypes = 'Body Corporate/Strata';
                        }
                    }
                    if(con.Contact_Types__c.contains('Builder')|| con.Contact_Types__c.contains('Developer'))
                    {
                        if(String.isNotBlank(conTypes))
                        {
                            conTypes=conTypes+';'+'Developer/Builder';
                        }
                        else
                        {
                            conTypes = 'Developer/Builder';
                        }
                    }
                    if(con.Contact_Types__c.contains('Authorized Agents/Legal Reps'))
                    {
                        if(String.isNotBlank(conTypes))
                        {
                            conTypes=conTypes+';'+'Legal Representative';
                        }
                        else
                        {
                            conTypes = 'Legal Representative';
                        }
                    
                    }
                    if(con.Contact_Types__c.contains('Community Representative'))
                    {
                        if(String.isNotBlank(conTypes))
                        {
                            conTypes=conTypes+';'+'Community Representative';
                        }
                        else
                        {
                            conTypes = 'Community Representative';
                        }
                    
                    }
                    if(con.Contact_Types__c.contains('Journalist')|| con.Contact_Types__c.contains('Marketing Representative'))
                    {
                        if(String.isNotBlank(conTypes))
                        {
                            conTypes=conTypes+';'+'Media';
                        }
                        else
                        {
                            conTypes = 'Media';
                        }
                    }
                    if(con.Contact_Types__c.contains('Accounts Payable')|| con.Contact_Types__c.contains('Pit & Pipe Designer'))
                    {
                        if(String.isNotBlank(conTypes))
                        {
                            conTypes=conTypes+';'+'nbn Internal';
                        }
                        else
                        {
                            conTypes = 'nbn Internal';
                        }
                    }
                    if(con.Contact_Types__c.contains('Consumer'))
                    {
                        if(String.isNotBlank(conTypes))
                        {
                            conTypes=conTypes+';'+'End User Residential';
                        }
                        else
                        {
                            conTypes = 'End User Residential';
                        }
                    
                    }
                    if(con.Contact_Types__c.contains('Government Representative'))
                    {
                        if(String.isNotBlank(conTypes))
                        {
                            conTypes=conTypes+';'+'Government Representative';
                        }
                        else
                        {
                            conTypes = 'Government Representative';
                        }
                    
                    }
                    if(con.Contact_Types__c.contains('Access Seeker'))
                    {
                        if(String.isNotBlank(conTypes))
                        {
                            conTypes=conTypes+';'+'RSP/ISP/Access Seeker';
                        }
                        else
                        {
                            conTypes = 'RSP/ISP/Access Seeker';
                        }
                    
                    }
                    if(con.Contact_Types__c.contains('Utility Representative'))
                    {
                        if(String.isNotBlank(conTypes))
                        {
                            conTypes=conTypes+';'+'Utility Representative';
                        }
                        else
                        {
                            conTypes = 'Utility Representative';
                        }
                    
                    }
                    cont.Contact_Type__c = conTypes;
                }
                cont.RecordTypeId = recordTypeId;
                
                cont.setOptions(dml);
                conMap.put(con.CRM_Contact_Id__c,cont);
            }
            conList =  conMap.values();
            Schema.SObjectField onDemandId = Contact.Fields.On_Demand_Id_P2P__c;
            List<Database.upsertResult> uResults = Database.upsert(conList,onDemandId,false);
            list<Database.Error> err;
            String msg, fAffected;
            String[]fAff;
            StatusCode sCode;
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            for(Integer idx = 0; idx < uResults.size(); idx++)
            {   
                if(!uResults[idx].isSuccess())
                {
                    err=uResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='Contact_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),
                    isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=conIList[idx].Id,Schedule_Job__c=bjId));
                    conIList[idx].Transformation_Status__c = 'Error';
                    conIList[idx].Schedule_Job_Transformation__c = bjId;
                }
                else
                {
                        if(String.isBlank(conIList[idx].Account_Id__c) || conIList[idx].Account_Id__c == 'No Match Row Id' || String.isNotBlank(acc.get(conIList[idx].Account_Id__c)))
                        {
                            conIList[idx].Transformation_Status__c = 'Copied to Base Table';
                        }
                        else 
                        {
                            conIList[idx].Transformation_Status__c = 'Association Error';
                            errList.add(new Error_Logging__c(Name='Contact_Tranformation',Error_Message__c='Account Record is not associated',Fields_Afftected__c='Account',isCreated__c=uResults[idx].isCreated(),
                            isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=conIList[idx].Id,Schedule_Job__c=bjId));
                        }
                        conIList[idx].SF_Contact_Id__c = uResults[idx].getId();
                        conIList[idx].Schedule_Job_Transformation__c = bjId;
                }
                    
              }
            
            
            Insert errList;
            update conIlist;
            if(errList.isEmpty())      
            {
                status = 'Completed';
            } 
            else
            {
                status = 'Error';
            }  
            recordCount = recordCount + conIlist.size();       
        }
        catch (Exception e)
        {
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='Contact_Tranformation',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        
        AsyncApexJob conJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
        {
            bj.Status__c= conJob.Status;
        }
        else
        {
             bj.Status__c=status;
        }
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = conJob.id;
        bj.End_Time__c  = conJob.CompletedDate;
        upsert bj;
        if (CRM_Transformation_Job__c.getinstance('SiteTransformation') <> null && CRM_Transformation_Job__c.getinstance('SiteTransformation').on_off__c)
        {
            Database.executeBatch(new SiteTransformation(exJobId));
        }
        if (CRM_Transformation_Job__c.getinstance('DevelopmentTransformation') <> null && CRM_Transformation_Job__c.getinstance('DevelopmentTransformation').on_off__c)
        {
            Database.executeBatch(new DevelopmentTransformation(exJobId));
        }
        if (CRM_Transformation_Job__c.getinstance('MedicalAlarmRegisterTransformation') <> null && CRM_Transformation_Job__c.getinstance('MedicalAlarmRegisterTransformation').on_off__c)
        {
            Database.executeBatch(new MedicalAlarmRegisterTransformation(exJobId));
        }
        if (CRM_Transformation_Job__c.getinstance('AccountContactTransformation') <> null && CRM_Transformation_Job__c.getinstance('AccountContactTransformation').on_off__c)
        {
            Database.executeBatch(new AccountContactTransformation(exJobId));
        }
        /*********************************************************************
        US#4118: Below lines have been commented to decommission P2P jobs.
        Author: Jairaj Jadhav
        Company: Wipro Technologies
        if (CRM_Transformation_Job__c.getinstance('MDUCP_SiteTransformation') <> null && CRM_Transformation_Job__c.getinstance('MDUCP_SiteTransformation').on_off__c)
        {
            Database.executeBatch(new MDUCP_SiteTransformation(exJobId));
        }
        *********************************************************************/
    }   

}