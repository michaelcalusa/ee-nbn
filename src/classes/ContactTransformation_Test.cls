/***************************************************************************************************
Class Name:  ContactTransformation_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 31-10-2016
Function    : This class contains unit test scenarios for ContactTransformation apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       31-10-2016                Created
****************************************************************************************************/
@isTest
private class ContactTransformation_Test {
    static void getRecords (){
        // Custom Setting record creation
        TestDataUtility.getListOfCRMTransformationJobRecords(true);
        // Create Junction_Object_Int__c record
        date CRM_Modified_Date = date.newinstance(1963, 01,24);
        List<Junction_Object_Int__c> actintList = new List<Junction_Object_Int__c>();
        Junction_Object_Int__c actint1 = new Junction_Object_Int__c();
        actint1.Name='TestName';
        actint1.CRM_Modified_Date__c=CRM_Modified_Date;
        actint1.Transformation_Status__c = 'Exported from CRM';
        actint1.Object_Name__c = 'Account';
        actint1.Child_Name__c = 'Contact';
        actint1.Event_Name__c = 'Associate';
        actint1.Object_Id__c = 'Account123';
        actint1.Child_Id__c = 'Contact123';
        actintList.add(actint1);
        insert actintList;
        // Create Account record
        List<Account> listOfAccounts = new List<Account> (); 
        Account accRec = new Account();
        accRec.name = 'Test Account';
        accRec.on_demand_id__c = 'Account6';
        listOfAccounts.add(accRec);
        Account accRec2 = new Account();
        accRec2.name = 'Test Account';
        accRec2.on_demand_id__c = 'Account4';
        listOfAccounts.add(accRec2);
        Account accRec3 = new Account();
        accRec3.name = 'Test Account';
        accRec3.on_demand_id__c = 'Account7';
        listOfAccounts.add(accRec3);
        insert listOfAccounts;
        // Create Contact_Int__c Record
        List<Contact_Int__c> ListOfContactInt = new List<Contact_Int__c> ();
        Contact_Int__c Contact_Int1 = new Contact_Int__c ();
        Contact_Int1.Name = 'Contact_Int_Test';
        Contact_Int1.Transformation_Status__c = 'Extracted from CRM';
        Contact_Int1.CRM_Modified_Date__c = CRM_Modified_Date;
        Contact_Int1.Account_Id__c = listOfAccounts.get(0).Id;
        Contact_Int1.Email__c = 'Test@nbnco.com.au';
        Contact_Int1.Mobile__c = '0450297367';
        Contact_Int1.Phone__c = '0450297368';
        Contact_Int1.Contact_Types__c = 'Cabler';
        ListOfContactInt.add(Contact_Int1);
        Contact_Int__c Contact_Int2 = new Contact_Int__c ();
        Contact_Int2.Name = 'Contact_Int_Test';
        Contact_Int2.Transformation_Status__c = 'Extracted from CRM';
        Contact_Int2.CRM_Modified_Date__c = CRM_Modified_Date;
        Contact_Int2.Account_Id__c = listOfAccounts.get(0).Id;
        Contact_Int2.Email__c = 'Test@nbnco.com.au';
        Contact_Int2.Phone__c = '0450297367';
        Contact_Int2.Contact_Types__c = 'Strata Manager';
        ListOfContactInt.add(Contact_Int2);
        Contact_Int__c Contact_Int3 = new Contact_Int__c ();
        Contact_Int3.Name = 'Contact_Int_Test';
        Contact_Int3.Transformation_Status__c = 'Extracted from CRM';
        Contact_Int3.CRM_Modified_Date__c = CRM_Modified_Date;
        Contact_Int3.Account_Id__c = listOfAccounts.get(0).Id;
        Contact_Int3.Work_Email__c = 'Test@nbnco.com.au';
        Contact_Int3.Phone__c = '0450297367';
        Contact_Int3.Contact_Types__c = 'Authorized Agents/Legal Reps';
        ListOfContactInt.add(Contact_Int3);
        Contact_Int__c Contact_Int4 = new Contact_Int__c ();
        Contact_Int4.Name = 'Contact_Int_Test';
        Contact_Int4.Transformation_Status__c = 'Extracted from CRM';
        Contact_Int4.CRM_Modified_Date__c = CRM_Modified_Date;
        Contact_Int4.Account_Id__c = listOfAccounts.get(0).Id;
        Contact_Int4.Work_Email__c = 'Test@nbnco.com.au';
        Contact_Int4.Work_Phone__c = '0450297367';
        Contact_Int4.Contact_Types__c = 'Builder';
        ListOfContactInt.add(Contact_Int4);
        Contact_Int__c Contact_Int5 = new Contact_Int__c ();
        Contact_Int5.Name = 'Contact_Int_Test';
        Contact_Int5.Transformation_Status__c = 'Extracted from CRM';
        Contact_Int5.CRM_Modified_Date__c = CRM_Modified_Date;
        Contact_Int5.Account_Id__c = listOfAccounts.get(0).Id;
        Contact_Int5.Work_Email__c = 'Test@nbnco.com.au';
        Contact_Int5.Work_Phone__c = '0450297367';
        Contact_Int5.Contact_Types__c = 'Community Representative';
        ListOfContactInt.add(Contact_Int5);
        Contact_Int__c Contact_Int6 = new Contact_Int__c ();
        Contact_Int6.Name = 'Contact_Int_Test';
        Contact_Int6.Transformation_Status__c = 'Extracted from CRM';
        Contact_Int6.CRM_Modified_Date__c = CRM_Modified_Date;
        Contact_Int6.Account_Id__c = listOfAccounts.get(0).Id;
        Contact_Int6.Work_Email__c = 'Test@nbnco.com.au';
        Contact_Int6.Work_Phone__c = '0450297367';
        Contact_Int6.Contact_Types__c = 'Journalist';
        ListOfContactInt.add(Contact_Int6);
        Contact_Int__c Contact_Int7 = new Contact_Int__c ();
        Contact_Int7.Name = 'Contact_Int_Test';
        Contact_Int7.Transformation_Status__c = 'Extracted from CRM';
        Contact_Int7.CRM_Modified_Date__c = CRM_Modified_Date;
        Contact_Int7.Account_Id__c = listOfAccounts.get(0).Id;
        Contact_Int7.Work_Email__c = 'Test@nbnco.com.au';
        Contact_Int7.Work_Phone__c = '0450297367';
        Contact_Int7.Contact_Types__c = 'Accounts Payable';
        ListOfContactInt.add(Contact_Int7);
        Contact_Int__c Contact_Int8 = new Contact_Int__c ();
        Contact_Int8.Name = 'Contact_Int_Test';
        Contact_Int8.Transformation_Status__c = 'Extracted from CRM';
        Contact_Int8.CRM_Modified_Date__c = CRM_Modified_Date;
        Contact_Int8.Account_Id__c = listOfAccounts.get(0).Id;
        Contact_Int8.Work_Email__c = 'Test@nbnco.com.au';
        Contact_Int8.Work_Phone__c = '0450297367';
        Contact_Int8.Contact_Types__c = 'Consumer';
        ListOfContactInt.add(Contact_Int8);
        Contact_Int__c Contact_Int9 = new Contact_Int__c ();
        Contact_Int9.Name = 'Contact_Int_Test';
        Contact_Int9.Transformation_Status__c = 'Extracted from CRM';
        Contact_Int9.CRM_Modified_Date__c = CRM_Modified_Date;
        Contact_Int9.Account_Id__c = listOfAccounts.get(0).Id;
        Contact_Int9.Work_Email__c = 'Test@nbnco.com.au';
        Contact_Int9.Work_Phone__c = '0450297367';
        Contact_Int9.Contact_Types__c = 'Government Representative';
        ListOfContactInt.add(Contact_Int9);
        Contact_Int__c Contact_Int10 = new Contact_Int__c ();
        Contact_Int10.Name = 'Contact_Int_Test';
        Contact_Int10.Transformation_Status__c = 'Extracted from CRM';
        Contact_Int10.CRM_Modified_Date__c = CRM_Modified_Date;
        Contact_Int10.Account_Id__c = listOfAccounts.get(0).Id;
        Contact_Int10.Work_Email__c = 'Test@nbnco.com.au';
        Contact_Int10.Work_Phone__c = '0450297367';
        Contact_Int10.Contact_Types__c = 'Access Seeker';
        ListOfContactInt.add(Contact_Int10);
        Contact_Int__c Contact_Int11 = new Contact_Int__c ();
        Contact_Int11.Name = 'Contact_Int_Test';
        Contact_Int11.Transformation_Status__c = 'Extracted from CRM';
        Contact_Int11.CRM_Modified_Date__c = CRM_Modified_Date;
        Contact_Int11.Account_Id__c = listOfAccounts.get(0).Id;
        Contact_Int11.Work_Email__c = 'Test@nbnco.com.au';
        Contact_Int11.Work_Phone__c = '0450297367';
        Contact_Int11.Contact_Types__c = 'Contractor Utility Representative';
        ListOfContactInt.add(Contact_Int11);
        insert ListOfContactInt;
        // Create Contact record
        Contact contactRec = new Contact ();
        contactRec.on_demand_id_P2P__c = 'Contact123';
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        insert contactRec;
        setupAccountIntRecords1();
    }
    static void setupAccountIntRecords1 (){
        // Create Account_Int__c record
        List<Account_Int__c> listOfAccountInt = new List<Account_Int__c> ();
        listOfAccountInt = new List<Account_Int__c> ();
        Account_Int__c accIntRec = new Account_Int__c ();
        accIntRec.Account_Name__c = 'AccName1';
        accIntRec.Transformation_Status__c = 'Extracted from CRM';
        accIntRec.CRM_Last_Modified_Date__c = system.now();
        accIntRec.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec.Account_Types__c = 'Developer,Builder';
        accIntRec.CRM_Account_Id__c = 'Account1';
        listOfAccountInt.add(accIntRec);
        Account_Int__c accIntRec2 = new Account_Int__c ();
        accIntRec2.Account_Name__c = 'AccName2';
        accIntRec2.Transformation_Status__c = 'Extracted from CRM';
        accIntRec2.CRM_Last_Modified_Date__c = system.now();
        accIntRec2.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec2.Account_Types__c = 'Abc';
        accIntRec2.Number_Street__c = 'test';
        accIntRec2.Address_2__c = 'test';
        accIntRec2.Address_3__c = 'test';
        accIntRec2.Bill_Number_Street__c = 'test';
        accIntRec2.Bill_Address_2__c = 'test';
        accIntRec2.Bill_Address_3__c = 'test';
        accIntRec2.CRM_Account_Id__c = 'Account2';
        listOfAccountInt.add(accIntRec2);
        Account_Int__c accIntRec3 = new Account_Int__c ();
        accIntRec3.Account_Name__c = 'AccName3';
        accIntRec3.Transformation_Status__c = 'Extracted from CRM';
        accIntRec3.CRM_Last_Modified_Date__c = system.now();
        accIntRec3.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec3.CRM_Account_Id__c = 'Account3';
        listOfAccountInt.add(accIntRec3);
        Account_Int__c accIntRec4 = new Account_Int__c ();
        accIntRec4.Account_Name__c = 'AccName4';
        accIntRec4.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec4.CRM_Last_Modified_Date__c = system.now();
        accIntRec4.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec4.Parent_Account_Id__c = 'Account4';
        accIntRec4.CRM_Account_Id__c = 'Account4';
        accIntRec4.Primary_Contact_Id__c = 'dddd';
        listOfAccountInt.add(accIntRec4);
        Account_Int__c accIntRec5 = new Account_Int__c ();
        accIntRec5.Account_Name__c = 'AccName5';
        accIntRec5.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec5.CRM_Last_Modified_Date__c = system.now();
        accIntRec5.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec5.Parent_Account_Id__c = 'Account6';
        accIntRec5.CRM_Account_Id__c = 'Account5';
        listOfAccountInt.add(accIntRec5);
        Account_Int__c accIntRec6 = new Account_Int__c ();
        accIntRec6.Account_Name__c = 'AccName6';
        accIntRec6.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec6.CRM_Last_Modified_Date__c = system.now();
        accIntRec6.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec6.Parent_Account_Id__c = 'Account7';
        accIntRec6.CRM_Account_Id__c = 'Account7';
        listOfAccountInt.add(accIntRec6);
        Account_Int__c accIntRec7 = new Account_Int__c ();
        accIntRec7.Account_Name__c = 'AccName5';
        accIntRec7.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec7.CRM_Last_Modified_Date__c = system.now();
        accIntRec7.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec7.Parent_Account_Id__c = 'bbb';
        accIntRec7.CRM_Account_Id__c = 'Account5';
        listOfAccountInt.add(accIntRec7);
        Account_Int__c accIntRec8 = new Account_Int__c ();
        accIntRec8.Account_Name__c = 'AccName6';
        accIntRec8.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec8.CRM_Last_Modified_Date__c = system.now();
        accIntRec8.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec8.Parent_Account_Id__c = 'ccc';
        accIntRec8.CRM_Account_Id__c = 'Account7';
        listOfAccountInt.add(accIntRec8);
        Account_Int__c accIntRec9 = new Account_Int__c ();
        accIntRec9.Account_Name__c = 'AccName4';
        accIntRec9.Transformation_Status__c = 'Ready for Parent Account Association';
        accIntRec9.CRM_Last_Modified_Date__c = system.now();
        accIntRec9.Credit_Check_Date__c = string.valueOf(system.today());
        accIntRec9.CRM_Account_Id__c = 'Account4';
        accIntRec9.Primary_Contact_Id__c = 'abcde';
        listOfAccountInt.add(accIntRec9);
        insert listOfAccountInt;
    }
    static testMethod void ContactTransformation_Test(){
        getRecords();
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());   
        Test.stopTest();
    }
}