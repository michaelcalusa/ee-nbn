public without sharing class ContactTriggerHandler extends TriggerHandler {
/*------------------------------------------------------------------------
Author:        Dave Norris
Company:       Salesforce
Description:   A class created to manage trigger actions from the Contact object 
               Responsible for:
               1 - Adding an houseHold account for contacts that are nbn end users
               2 - Updating the Account name if the Contact name changes
               3 - Deleting the linked Household Account if the Contact is deleted
Test Class:    ContactTriggerHandler_Test
History
<Date>            <Authors Name>    <Brief Description of Change>
11/09/2015        Guy Beres         Set of Record Type Id on insert  
09/02/2016        Guy Beres         Update afterUpdate to only fire Account updates for End User Contacts.
--------------------------------------------------------------------------/
    
    List<Contact> houseHoldAccountInserts = new List<Contact>(); //Accounts to insert
    List<Contact> houseHoldAccountUpdates = new List<Contact>(); //Accounts to update
    List<Contact> houseHoldAccountDeletes = new List<Contact>(); //Accounts to delete
    String debugMsg;
    
    Map<Id,Id> accountUpdates = new Map<Id,Id>(); //Accounts to update with a newly inserted contact id
    Set<Id> customerAccountIds = new Set<Id>(); //A list of existing account ids for contacts already linked to an account
            
    public ContactTriggerHandler() {

        if ( Test.isRunningTest() ) {
            this.setMaxLoopCount(10);
        }
        else {
            //Set max loop count to 2 - this trigger needs to fire twice to cater for before and after events
            this.setMaxLoopCount(2);
        }
        
        System.Debug( 'NBN: -> ContactTriggerHandler invoked' );
        
    }

    / trigger handler overrides / 
    //====================================================================
    protected override void beforeInsert(list<sObject> sObjectList) {
        
        System.Debug( 'NBN: -> Executing before insert' );
      
        for( Contact endUser : ( List<Contact> ) sObjectList) {
        
            if ( endUser.RecordTypeId == GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME) ) {
                    customerAccountIds.add( endUser.AccountId );
            }
                
        }
        
        System.Debug( 'NBN: -> Found ' + customerAccountIds.size() + ' Contact records matching record type ' + GlobalConstants.END_USER_RECTYPE_NAME );
     
        Map<Id,Account> existingAccounts = new Map<Id,Account>( [ SELECT Id, 
                                                                  Linked_End_User_Id__c 
                                                                  FROM Account 
                                                                  WHERE Id IN :customerAccountIds]);
        
        for( Contact endUser : ( List<Contact> ) (sObjectList) ) {
        
            if ( ( endUser.AccountId != null && 
                    existingAccounts.size() > 0 && existingAccounts.get(endUser.AccountId) != null && existingAccounts.get(endUser.AccountId).Linked_End_User_Id__c != null ) ||
                    endUser.AccountId == null)
            {
                if ( endUser.AccountId != null && endUser.Account.Linked_End_User_Id__c != null ){
                    endUser.AccountId = null;
                }
                
                houseHoldAccountInserts.add( endUser );
            }
        }  
        
        System.Debug( 'NBN: -> Inserting ' + houseHoldAccountInserts.size() + ' Account record(s) ' );
 
        if ( houseHoldAccountInserts.size() > 0 ) {
            inserthouseHoldAccount( houseHoldAccountInserts );                                                                    
        }
      
        System.Debug( 'NBN: -> Ending before insert' );
 
    }
    
    protected override void afterInsert(map<id,sObject> newMap) {
        
        for( Contact endUser : ( list<Contact> ) (newMap.values()) ) {
            //grab the Accounts that need to have the newly assigned Contact Id to them if the contact is connected to an Account/
            if ( endUser.AccountId != null )
            {
                //contacts are connected to Accounts, make the connection in the other direction
                accountUpdates.put(endUser.AccountId, endUser.Id);
            }
        }
        
        if ( accountUpdates.size() > 0 )
        {
            //update Accounts that have newly created Contacts connected to them
            updateAccounts( accountUpdates );
        }
             
    }
    
    protected override void beforeUpdate( map<id,SObject> oldMap, map<id,SObject> newMap ) {
        
        for( Contact endUser : ( list<Contact> ) (newMap.values()) ) {
            //one-to-one account should be created if the account has been blanked out by the user/
            if ( endUser.AccountId == null )
            {
                houseHoldAccountInserts.add( endUser );
            }   
        }
        
        if ( houseHoldAccountInserts.size() > 0 ) {
            inserthouseHoldAccount( houseHoldAccountInserts );                                                                    
        }
               
    }
    
    protected override void afterUpdate( map<id,SObject> oldMap, map<id,SObject> newMap ) {

        Integer i = 0;          // processing counter
        
        List<Contact> oldEndUsers = ( List<Contact> ) (oldMap.Values());

        for( Contact endUser : ( List<Contact> ) (newMap.Values())) {
            if (
                (endUser.FirstName != oldEndUsers[i].FirstName ||
                endUser.LastName != oldEndUsers[i].LastName) &&
                endUser.RecordTypeId == GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME) // GB 09/02/2016: Updated           
            ) {
                houseHoldAccountUpdates.add( endUser );
            }
            
            i += 1;
        }    
        
        if ( houseHoldAccountUpdates.size() > 0 ) {
            updatehouseHoldAccount( houseHoldAccountUpdates );                                                                    
        }
                                                          
    }
    
    protected override void afterDelete( map<id,SObject> oldMap) {

        for( Contact endUser : ( List<Contact> ) (oldMap.Values()) ) {
        
            houseHoldAccountDeletes.add( endUser );
            
        }
    
        if ( houseHoldAccountDeletes.size() >  0)
        {
            //Contacts that are being deleted
            deletehouseHoldAccount( houseHoldAccountDeletes );
        }
        
    }
    /end trigger handler overrides/
    //====================================================================
    
 
    / private methods /
    //====================================================================
    private void inserthouseHoldAccount( Contact[] endUsers ) {
   
        List<Id> endUserIds = new List<Id>();
        Map<Id,Id> endUserAccountMap = new Map<Id,Id>();

        for (Contact endUser : endUsers)
        {
            //make sure we're only working with Contacts that have already been inserted
            if (endUser.Id != null)
            {
                endUserIds.add(endUser.Id);
            }
        }
        //get all the Accounts that are connected to the Contacts
        for ( Account acc : [SELECT Id, Linked_End_User_Id__c 
                            FROM Account 
                            WHERE Linked_End_User_Id__c in :endUserIds] )
        {
            endUserAccountMap.put(acc.Linked_End_User_Id__c, acc.Id);
        }
        
        List<Contact> contactUpdates = new List<Contact>();
        List<Account> accountInserts = new List<Account>();
   
        for(Contact endUser : endUsers)
        {
            //if we found an Account already connected to this Contact, connect the Contact to that Account
            if ( endUserAccountMap.containsKey( endUser.Id ) )
            {
                //if a user has blanked out the Account for a Contact, this will put it right back
                  endUser.AccountId = endUserAccountMap.get( endUser.Id );
                                   
            } else {   
                //construct the houseHold account for the Contact
                Account a = new Account();
                String aName = '';
                if (endUser.FirstName != null)
                {
                    aName += endUser.FirstName;
                    aName += ' ' ;
                }
                aName += endUser.LastName;
                aName += ' ';
                aName += 'Household';
                 
                a.Name = aName;
                //connect the Account to the Contact
                if (endUser.Id != null) {
                    a.Linked_End_User_Id__c = endUser.Id;
                }
                
                // GB 11/09/2015: Added set of Record Type for Household Account records created.
                a.RecordTypeId = GlobalCache.getRecordTypeId('Account', GlobalConstants.houseHold_ACCOUNT_RECTYPE_NAME);
                
                //a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household Account').getRecordTypeId();
               
                a.OwnerId = endUser.OwnerId;
                accountInserts.add(a);
            }
        }
            
        if (accountInserts.size() > 0) {
            
            Database.SaveResult[] lsr = Database.insert(accountInserts, false);

            Integer i = 0;
           
            for ( Contact endUser : endUsers )
            {
                //for each success, write the new AccountId to the Contact. These 
                if ( lsr[i].isSuccess() == true ) {
                    endUser.AccountId = lsr[i].getId();
                } else {

                    for( Database.Error insertError : lsr[i].getErrors() ){
                        endUser.addError ('Failed to create the associated Account record : ' + insertError.getStatusCode() + ': ' + insertError.getMessage() );
                    }        
                    
                }
                
                i += 1;
            }

        }  
        
    }
    
    private static void updateAccounts( Map<Id, Id> accounts )
    {
        List<Account> accountUpdates = new List<Account>(); 
        
        for ( Id i : accounts.keySet() )
        {
            Account a = new Account( Id=i, Linked_End_User_Id__c = accounts.get(i) );
            accountUpdates.add( a );
        }
        if ( accountUpdates.size() > 0 )
        {
            Database.SaveResult[] lsr = Database.update( accountUpdates, false );
        }
        
    }
    
    private void updatehouseHoldAccount( Contact[] endUsers ) {
    
        Map<Id,Contact> accConMap = new Map<Id,Contact>();
        
        for (Contact c : endUsers) 
        {
            accConMap.put(c.AccountId,c);

        }
        //get the corresponding Accounts
        List<Account> accts = [SELECT  a.name
                                FROM Account a 
                                WHERE a.Id IN :accConMap.keyset()];
                                
        List<Account> accountUpdates = new List<Account>();
        
        for( Account a : accts ) {     
            
            Contact c = accConMap.get(a.Id);
            //if any data has changed on the contact, push it all to the Account
            if (
                c != null && (
                c.FirstName + ' ' + c.LastName != a.Name)
            ) {
                // Update Account fields
                a.Name = c.FirstName + ' ' + c.LastName + ' Household';
                
                accountUpdates.add(a);
            }
           
        }   

        if ( accountUpdates.size() > 0 )
        {
            Database.SaveResult[] lsr = Database.update( accountUpdates, false );
        }        
    
    }
    
    private void deletehouseHoldAccount( Contact[] endUsers ) {
    
        List<Id> contactIds = new List<Id>();
        List<Account> accountDeletes = new List<Account>();
        
        System.Debug('@@@@Found something to delete 2' );
        
        for (Contact c : endUsers)
        {
            //make sure we're only working with Contacts that have already been deleted
            if (c.Id != null)
            {
                System.Debug('@@@@Found something to delete 3' );
                contactIds.add(c.Id);
            }
        }         
        //get all the Accounts that are connected to the Contacts
        for (Account acc : [SELECT Id, Linked_End_User_Id__c 
                            FROM Account 
                            WHERE Linked_End_User_Id__c in :contactIds])
        {  
            System.Debug('@@@@Found account delete');
            accountDeletes.add( acc );
        }
        
        if ( accountDeletes.size() > 0 )
        {  
           Database.DeleteResult[] lsr = Database.delete( accountDeletes, false );
        }
        
    }
    / end private methods /
    //====================================================================
    
     // exception class
    public class ContactTriggerHandlerException extends Exception {} 
*/
}