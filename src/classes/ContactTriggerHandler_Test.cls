/*------------------------------------------------------------------------
Author:        Dave Norris
Company:       Salesforce
Description:   A test class created to test ContactTriggerHandler methods
               Test executed:
               1 - setupTestData - Create test data required to execute methods
               2 - checkOnetoOneAccountDML - verify insert, update and delete operations
               3 - bulkTests - check for governor limit exceptions

History
<Date>      <Authors Name>     <Brief Description of Change>
5-Nov-2015   Rohit Kumar        Update test data to refer to TestDataUtility Class
--------------------------------------------------------------------------*/
@isTest
private class ContactTriggerHandler_Test {
  /*  
    static contact testContact;
    static list<Contact> testContactList;
    static Id endUserRecordTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME);
    
    /
      Method to setup Test Data
     / 
    static void setupTestData(integer recordSize){
        if(recordSize == 1)
            testContact = TestDataUtility.createContactTestRecords(recordSize,true,'John','Smith',endUserRecordTypeId)[0];
        else
            testContactList = TestDataUtility.createContactTestRecords(recordSize,true,'John','Smith',endUserRecordTypeId);
    }

    /// <name> checkOnetoOneAccountCreation </name>
    /// <summary> Test Method for checking an account is created for a customer </summary>
    static testMethod void checkOnetoOneAccountCreation()
    {     
        
        Test.startTest();
            // Create the test data
            setupTestData(1);
        Test.stopTest();
              
        //Query for the one to one account that should have been created
        Account[] oneToOneAccount = [SELECT Name
                                     FROM Account 
                                     WHERE Linked_End_User_Id__c =: testContact.id ];
        
        //One account should be related to the contact with household in the name
        system.assertEquals( 1, oneToOneAccount.size() );
        system.assertEquals( 'John Smith Household', oneToOneAccount[0].Name );
        
        
    }
    
    static testMethod void checkOnetoOneAccountUpdate()
    {     
        // Create the test data
        setupTestData(1);
               
        //Now update the last name and check this is reflected in the account name
        testContact.LastName = 'Jones';
        
        Test.startTest();
        
             update testContact;
        
        Test.stopTest();
        
        Account[] oneToOneAccount = [ SELECT Name
                                     FROM Account 
                                     WHERE Linked_End_User_Id__c =: testContact.id ];
                                     
        system.assertEquals( 'John Jones Household', oneToOneAccount[0].Name ); 
        
  
    }
    
    static testMethod void checkOnetoOneAccountDelete()
    {     
        // Create the test data
        setupTestData(1);
        
        testContact = [select id,AccountId from Contact where id =:testContact.id];
        
        testContact.AccountId = null;
        
        update testContact;

        Test.startTest();
        
         //Now delete the contact and verify the associated account is deleted
        delete testContact;
        
        Test.stopTest();
        
        
       Account[] oneToOneAccount = [ SELECT Name
                             FROM Account 
                             WHERE Linked_End_User_Id__c =: testContact.id ];
                                     
       system.assertEquals( 0, oneToOneAccount.size() );
        
    }
    
    static testMethod void bulkTests()
    {
        //Verify that no accounts have been created so far
        Account[] existingAccounts = [select id
                                     FROM Account 
                                     LIMIT 200];
        
        system.assertEquals( 0, existingAccounts.size() );  
        
        //Ensure governor limits are reset here by starting the test
        Test.startTest();
            // Create the test data
             setupTestData(200);
        Test.stopTest();
        
        //Verify that accounts have actually been created. 200 contacts should create 200 accounts
        Account[] createdAccounts = [select id
                                     FROM Account 
                                     LIMIT 200];
        
        system.assertEquals( 200, createdAccounts.size() );
        
    } 
    
    static testMethod void testBypassTrigger() {
         
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User bypassUser = TestDataUtility.createTestUser(false,p.id);
        
        bypassUser.Bypass_Triggers__c = 'ContactTriggerHandler';

        insert bypassUser;

        System.runAs( bypassUser ) {
        
            Test.startTest();
                setupTestData(1);
            Test.stopTest();
            
            //Query for any accounts created for the related contact
            Account[] oneToOneAccounts = [ SELECT Name
                                             FROM Account 
                                             WHERE Linked_End_User_Id__c =: testContact.id ];
          
            //Verify that no account was created as the trigger utility class should have been bypassed
            system.assertEquals( 1, oneToOneAccounts.size() );
        }   
    }
   */ 
}