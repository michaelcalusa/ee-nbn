/*
Class Description
Creator: Wayne Ni 
Purpose: This class is to process Corporate case emails

Change List:   
Name    Date          Story Number
Wayne Ni  May 21st 2018      MSEU-10066
Wayne Ni  June 14th 2018     MSEU-14206
Wayne Ni  July 5th 2018 	 For duplicate case secanrio when thread id is same without case senstive check
Wayne Ni  July 10th 2018	 Adding new method for the loop email scenario
Wayne Ni  Aug 14th 2018 	 Removed the custom label logic
*/

global class CorporateAffairsEmailServiceHandler  implements Messaging.InboundEmailHandler {
    
    public String CorpAffairTaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Corporate Affairs Task').getRecordTypeId();
    public String CorpAffairCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Corporate Affairs').getRecordTypeId();
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env){
        
        //system.debug(email);
        //system.debug('the to email address is '+email.toAddresses);
        //Email Result
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        //Email subject and body check for thread ID
        string regex = 'ref\\:[a-z0-9A-Z_]{5,}\\.[a-z0-9A-Z_]{5,}\\:ref';
        string subject = email.subject;
        string textbody = email.plainTextBody;
        string EmailContents = subject + ' '+textbody;
        string ThreadId;
        
        Matcher matcher = Pattern.compile(regex).matcher(EmailContents);                
        Boolean threadIdresult= matcher.find();
        
        if(threadIdresult == true){
            //existing case logic goes here
            ThreadId = matcher.group(0);
            //system.debug('Thread Id is '+ThreadId);
            
            //case existingCase = [select id, Thread_Id__c, Status, OwnerId, CaseNumber  from case where Thread_Id__c  =: ThreadId limit 1];
            //system.debug('the case id is ' +existingCase.id);
            
            List<case> AllPossibleCases = [select id, Thread_Id__c, Status, OwnerId, CaseNumber,recordtypeid  from case where Thread_Id__c  =: ThreadId and recordtypeid =: CorpAffairCaseRecordTypeId];
            //system.debug('All possibe cases are '+AllPossibleCases);
            //List<case> AllPossibleCases = [select id, Thread_Id__c, Status, OwnerId, CaseNumber,recordtypeid  from case where Thread_Id__c  =: ThreadId];
            Map<String,case> ThreadIdCaseMap = new Map<String,case>();
            //code update here to prevent duplicate case record and find exact right case
            for(case PossibleCase : AllPossibleCases){
                ThreadIdCaseMap.put(PossibleCase.Thread_Id__c,PossibleCase);
            }
            
            case existingCase = new case();
            if(ThreadIdCaseMap.containsKey(ThreadId)){
                existingCase =  ThreadIdCaseMap.get(ThreadId);                   
            }

            
            //if(existingCase != null && existingCase.recordtypeid == CorpAffairCaseRecordTypeId){
            try{
                if(existingCase.Id != null){
                    AttachEmailToCase(existingCase.id,email.toAddresses,email.fromAddress,email.fromName,email.subject, email.htmlBody, email.plainTextBody,
                                      email.binaryAttachments,email.textAttachments);
                    
                    //Added code here by Wayne for MSEU-14026 story
                    if(existingCase.Status == 'Closed' || existingCase.Status == 'Cancelled'){
                        CloseCancelledTaskCreation(existingCase, existingCase.Status);
                    }
                    
                }
            }catch(Exception e){           
                System.debug('Exception' + e);   
                GlobalUtility.logMessage('Error','CorpAffairEmailServiceHandler','handleInboundEmail: ThreadId Found Scenario','','','',email.subject + '    '+ email.plainTextBody,e,0);
            }   
            
        }else{                        
            //new case creation and linked contact logic goes here
            
            try{        
                //create new case for the new incoming email, Linked the contact and attach inbound email to the case
                
                //Do an email message check to make sure not creating duplicate case for the same emailmessage
                Boolean ifCreateCase = DuplicateEmailMessageCheckBeforeCaseCreation(email);
				//system.debug('if create case is '+ifCreateCase);
                if(ifCreateCase == true){
                    Case newCorpCaseId = newCorpCaseLogicProcessing(email);                    
                    //Send ack email and attach acknowledgement email to the case
                    SendAckEmail(newCorpCaseId,email,0);                    
                }
                
            }catch(Exception e){           
                //System.debug('Exception' + e);   
                GlobalUtility.logMessage('Error','CorpAffairEmailServiceHandler','handleInboundEmail: ThreadId NOT Found Scenario','','','',email.subject + '    '+ email.plainTextBody,e,0);
            }   
        }
    
        return result;                                                          
    }
    
    public Case newCorpCaseLogicProcessing(Messaging.inboundEmail inboundEmail){
        String FromAddress = inboundEmail.fromAddress;
        String FromName = inboundEmail.fromName;
        List<String> toAddress = inboundEmail.toAddresses;
        String Subject = inboundEmail.subject;
        String htmlBody = inboundEmail.htmlBody;
        String TextBody = inboundEmail.plainTextBody;
        String MessageId = inboundEmail.messageId;
        Messaging.InboundEmail.BinaryAttachment[] InboundEmailBinaryAttachments = inboundEmail.binaryAttachments;
        Messaging.InboundEmail.TextAttachment[] InboundEmailTextAttachments = inboundEmail.textAttachments;
        
        Id CorpAffairCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Corporate Affairs').getRecordTypeId();
      	Id CorpAffairContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Corporate Affairs').getRecordTypeId();
        Id corpaffairQueueId = [select Id from Group where  Type = 'Queue' AND developerName = 'Corporate_Affairs_Team' Limit 1].id;
        
        Case CorpAffairCase = new case();
        CorpAffairCase.Origin = 'Email';
        
        //length check for the subject, perform truncate if exceed 
        if(subject != null && subject.length() > 255 ){
            CorpAffairCase.subject = Subject.substring(0, 254);
        }else if(subject != null && subject.length() < 255 ){
            CorpAffairCase.subject = Subject;
        }

        
        //length check for the descritopn, perform truncate if exceed
        if( TextBody != null && TextBody.length() > 32000 ){
            CorpAffairCase.Description = TextBody.substring(0,31999);
        }else if(  TextBody != null && TextBody.length() < 32000 ){
            CorpAffairCase.Description = TextBody;
        }

                
        CorpAffairCase.RecordTypeId = CorpAffairCaseRecordTypeId;
        CorpAffairCase.Status = 'Open';
        CorpAffairCase.Priority = '3-General';
        CorpAffairCase.Phase__c = 'Corporate Affairs';
        CorpAffairCase.Category__c = 'Unknown';
        CorpAffairCase.Primary_Contact_Role__c = 'Government Representative';
        if(corpaffairQueueId!= null){
            CorpAffairCase.OwnerId = corpaffairQueueId;
        }
        
        //Return the contact Id to link to the case   
        system.debug('contacts logic now');
        contactResponseWrapper contactProcessingResponseWrapper = ContactProcessingForCorpCase(inboundEmail,FromAddress,CorpAffairContactRecordTypeId);
    
        CorpAffairCase.ContactId = contactProcessingResponseWrapper.Linkedcontact.Id;
        system.debug('before insert case');
        insert CorpAffairCase;  
                                     
        //create tasks for multiple and no contact scenario
        system.debug('Task creation');
        if(contactProcessingResponseWrapper.contactStatus != 'two'){
            TaskCreation(contactProcessingResponseWrapper,CorpAffairCase.Id,corpaffairQueueId);
        }
                                     
        //Attach the inbound and attchments to the case if there is any
        system.debug('Attch inbound email to the case');
        AttachEmailToCase(CorpAffairCase.Id,toAddress,FromAddress,FromName,Subject,htmlBody,TextBody,InboundEmailBinaryAttachments,InboundEmailTextAttachments);
            
        return CorpAffairCase;
    }
    
    public contactResponseWrapper ContactProcessingForCorpCase(Messaging.inboundEmail Email,String senderEmail,Id CorpAffairRecordtypeid){
        List<Contact> potenteialContact = [select name, id, LastModifiedDate from contact where Email =: senderEmail and
                                           recordtypeid =: CorpAffairRecordtypeid order by LastModifiedDate Desc];
        system.debug('Contact logic One');
        Contact linkedContact;
        contactResponseWrapper wrapperResponse = new contactResponseWrapper();
        Id CorpAffairContactRecordtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Corporate Affairs').getRecordTypeId();
        
        system.debug('Contact logic Two');
        if(potenteialContact != null && potenteialContact.size() == 0){
            //create new contact with new institution account
            Contact newCorpContact = new Contact();
            // = Email.fromName;
            
            //First Last name code update
            system.debug('full name is '+Email.fromName);
            system.debug(Email.fromName.contains(' '));
            
            if(Email.fromName != null && Email.fromName.contains(' ')){               
                String FirstName = Email.fromName.substringBefore(' ');
                String LastName = Email.fromName.substringAfter(' ');
                newCorpContact.FirstName = FirstName;
                newCorpContact.LastName = LastName;                
            }else if(Email.fromName != null && Email.fromName.contains(' ')==false){
                newCorpContact.LastName = Email.fromName;      
            }else if(Email.fromName == null){
                newCorpContact.LastName = Email.fromAddress;
            }

            
            newCorpContact.Email = senderEmail;
            newCorpContact.RecordTypeId = CorpAffairContactRecordtype;
                        
            List<Account> CorpAffairDumpingAccounts = [select id from Account where name = 'Corporate Affairs Account' limit 1]; 
            
            if(CorpAffairDumpingAccounts.size() >  0){
                newCorpContact.AccountId = CorpAffairDumpingAccounts[0].Id;  
            }
            insert newCorpContact;              
            System.debug('New Corp Affairs Contact Created to the Institution Account');
            
            wrapperResponse.contactStatus = 'one';
            wrapperResponse.Linkedcontact = newCorpContact;
                
        }else if(potenteialContact != null && potenteialContact.size() == 1){
            wrapperResponse.contactStatus = 'two';
            wrapperResponse.Linkedcontact =  potenteialContact[0];
            
        }else if(potenteialContact != null && potenteialContact.size() > 1){
            wrapperResponse.contactStatus = 'three';
            wrapperResponse.Linkedcontact =  potenteialContact[0];                       
        }       
        system.debug('Contact logic Three');
        return wrapperResponse;
    }

    public void TaskCreation(contactResponseWrapper wrapperResult, Id relatedCaseId,Id queueId){
        string comments;       
        Task t = new Task();                                    
        t.RecordTypeId = CorpAffairTaskRecordTypeId;
        t.Start_Date__c = system.today(); 
        t.Type_Custom__c  = 'Other';
        t.Sub_Type__c = 'Other';
        t.Priority = '3-General';
        t.Subject = '';
        t.Status = 'Open';
        t.WhatId = relatedCaseId; 
        /*
        Id TaskOwnerId = [Select id, Email from user where Email =: System.Label.Corporate_Affairs_Task_Owner_Email limit 1].Id;
        if(TaskOwnerId !=null){
            t.ownerId = TaskOwnerId;
        }
        */
        
        System.debug('New Task Created');        
        if(wrapperResult.contactStatus == 'one'){
            system.debug('First type task created');
            t.Subject = 'Verify Case Contact and Account - Contact Review And Contact Relink to Correct Corporate Affairs Account Might Be Required';
            comments = 'Sender email address does not exist as a Corporate Affairs contact. Please review case contact and relink to the correct Institution Account';            
        }else if(wrapperResult.contactStatus == 'three'){
            system.debug('Second type task created');
            t.Subject = 'Verify Case Contact and Account - Contact Review And Contact Merge Might Be required';
            comments = 'Sender email address exists for multiple Corporate Affairs contacts. Please review and merge these contacts.';
        }
        
        t.Description = comments;
        system.debug('before inserting the task');
        insert t;
        system.debug('Insert new task for this corp affair case');        
    }
    
    public void CloseCancelledTaskCreation(Case existingcase, string casestatus){
        String ProcessingCaseNumber = existingcase.CaseNumber;
        String ProcessingCaseOwnerId = existingcase.OwnerId;
        String taskcomments;
        String taskSubject;
                
        Task CloseCancelledCaseTask = new Task();
        CloseCancelledCaseTask.RecordTypeId = CorpAffairTaskRecordTypeId;
        CloseCancelledCaseTask.Type_Custom__c  = 'Other';
        CloseCancelledCaseTask.Sub_Type__c = 'Other';
        CloseCancelledCaseTask.Priority = '3-General';
        CloseCancelledCaseTask.Subject = '';
        CloseCancelledCaseTask.Status = 'Open';
        CloseCancelledCaseTask.Start_Date__c = system.today();
        CloseCancelledCaseTask.WhatId = existingcase.Id;
        
        if(casestatus == 'Closed'){
            taskcomments = 'An email had been received for Closed/Cancelled Case ' +  ProcessingCaseNumber +
                '. Please review case if further actions are required and close task.';
            taskSubject = 'An email had been received for Closed/Cancelled Case ' + ProcessingCaseNumber;
        }else if(casestatus == 'Cancelled'){
            taskcomments = 'An email had been received for Closed/Cancelled Case ' +  ProcessingCaseNumber +
                '. Please review case if further actions are required and close task.';
            taskSubject = 'An email had been received for Closed/Cancelled Case '+ProcessingCaseNumber;
        }
        
        CloseCancelledCaseTask.Subject = taskSubject;
        CloseCancelledCaseTask.Description = taskcomments;
    
        //check if the corp case is assigned to an user or queue
        if(ProcessingCaseOwnerId.startsWith('005') == true){
            CloseCancelledCaseTask.OwnerId = ProcessingCaseOwnerId;            
        }
        
        insert CloseCancelledCaseTask;              
    }
    
   
    public void SendAckEmail(Case createdCase,Messaging.inboundEmail InboundEmail, integer EmailContentSwitch){
        system.debug('Intializing acknolwedge email');
        string subject;
        List<string> Toaddress = InboundEmail.toAddresses;
        string html;
        string text;
        string AckEmailFromAddress;
        Id renderTemplateId;
		
        
        Messaging.reserveSingleEmailCapacity(1);
        Messaging.SingleEmailMessage sendEmail = new Messaging.SingleEmailMessage();
        system.debug('Intializaing single email');
        EmailMessage newEmail = new EmailMessage(); 
        
        OrgWideEmailAddress[] ElectorateOwea = [select Id,Address,DisplayName from OrgWideEmailAddress where DisplayName = 'NBNElectorateEnquiries'];
        OrgWideEmailAddress[] CorrespondenceOwea = [select Id,Address,DisplayName from OrgWideEmailAddress where DisplayName = 'NBNCorrespondence'];
        EmailTemplate NBNElectoratetemplateId = [Select id from EmailTemplate where name = 'Corporate affairs-nbnElectorateEnquiries' limit 1];
        EmailTemplate NBNCorrespondencetemplateId = [Select id from EmailTemplate where name = 'Corporate affairs-nbnCorrespondence' limit 1];
        
        //indentify which template to render
        system.debug('Identify which template to render');
        for(string SingleAddress: Toaddress){
            system.debug('The to email address is '+SingleAddress);
            if(SingleAddress == System.Label.Corporate_Affairs_NBN_Electorate){
                if(ElectorateOwea.size()>0){
                    system.debug('Getting into first if condition');
                    sendEmail.setOrgWideEmailAddressId(ElectorateOwea[0].id);
                    system.debug('Set org wide email address');
                    newEmail.FromAddress = ElectorateOwea[0].Address;
                    newEmail.FromName = ElectorateOwea[0].DisplayName;
                    system.debug('set up new email');
                    renderTemplateId = NBNElectoratetemplateId.Id;
                    system.debug('template ID');
                }
            }else if(SingleAddress == System.Label.Corporate_Affairs_NBN_Correspondence){
                if(CorrespondenceOwea.size()>0){
                    sendEmail.setOrgWideEmailAddressId(CorrespondenceOwea[0].id);
                    newEmail.FromAddress = CorrespondenceOwea[0].Address;
                    newEmail.FromName = CorrespondenceOwea[0].DisplayName;
                    renderTemplateId = NBNCorrespondencetemplateId.Id;
                }                
            }           
        }
        
        system.debug('Render template id is '+renderTemplateId);
        
        system.debug('Initializing the parameters for email and emailmessage');
        system.debug('The from address is '+InboundEmail.fromAddress);
        sendEmail.setToAddresses(new string[]{InboundEmail.fromAddress});
        newEmail.ToAddress = InboundEmail.fromAddress;

        if(renderTemplateId != null){
            system.debug('get rendering result from email template');        
            Messaging.SingleEmailMessage result = Messaging.renderStoredEmailTemplate(renderTemplateId,createdCase.ContactId,createdCase.id);
            system.debug('After rendering result from email template');
            
            
            sendEmail.setSubject(result.getSubject());                        
            sendEmail.setHtmlBody(result.getHtmlBody());
            sendEmail.setPlainTextBody(result.getPlainTextBody());
            
            newEmail.Subject = result.getSubject();
            newEmail.HtmlBody = result.getHtmlBody();
            newEmail.TextBody = result.getPlainTextBody();
            newEmail.ParentId = createdCase.Id;
            
            system.debug('Before sending email and inserting new emailmessage');
            Messaging.sendEmail(new Messaging.Email[]{sendEmail});
            insert newEmail;
            system.debug('After sending email and inserting new emailmessage');
        }

    }
    
    public void AttachEmailToCase(Id caseId,List<String> toAddress,string FromAddress, string FromName,String subject, String htmlBody, string textBody,
                                 Messaging.InboundEmail.BinaryAttachment[] Battach,Messaging.InboundEmail.TextAttachment[] Tattach){
        system.debug('step one');
        EmailMessage newEmail = new EmailMessage();
        system.debug('step two');
        newEmail.FromAddress = FromAddress;
        system.debug('step three');
        newEmail.FromName = FromName;
        //system.debug(FromName);
        //system.debug(toAddress.size());
        if(toAddress != null && toAddress.size() > 0){
          system.debug('step four');
          for(string singleAdd: toAddress){
              if(singleAdd == System.Label.Corporate_Affairs_NBN_Electorate){                  
                  newEmail.ToAddress = System.Label.Corporate_Affairs_NBN_Electorate;
              }else if(singleAdd == System.Label.Corporate_Affairs_NBN_Correspondence){
                  newEmail.ToAddress = System.Label.Corporate_Affairs_NBN_Correspondence;
              }
                
          }
        }
        system.debug('step five');
        //subject length check
        
        if(subject != null  &&  subject.length()< 3000){
            newEmail.Subject = subject;
        }else if(subject != null && subject.length() > 3000 ){
            newEmail.Subject = subject.substring(0, 2999);
        }
		
        if(textBody != null && textBody.length() < 32000 ){
            newEmail.TextBody = textBody;
        }else if(textBody != null && textBody.length() > 32000){
            newEmail.TextBody = textBody.substring(0, 31999);
        }
                                     
        if(htmlBody != null && htmlBody.length() < 32000){
            newEmail.HtmlBody = htmlBody;                          
        }else if(htmlBody != null && htmlBody.length() > 32000){
            newEmail.HtmlBody = htmlBody.substring(0, 31999);
        }                                     		
        
        //newEmail.HtmlBody = htmlBody; // email body
        newEmail.ParentId = caseId; 
        system.debug('before insert');  
        system.debug('case id is '+caseId);
        insert newEmail;
        system.debug('Corp affair email service handler, insering new email message');
                                     
        //Text Attachment check        
        if (Tattach != null && Tattach.size() > 0){
            List<Attachment> attBinList = new list<Attachment>();
            List<ContentVersion> cvList = new list<ContentVersion>();
            for(Messaging.InboundEmail.TextAttachment textAttachment : Tattach){
                Attachment att = new Attachment();
                att.name = textAttachment.filename;
                att.body = Blob.valueOf(textAttachment.body);
                att.parentid = newEmail.id;
                attBinList.add(att);
                
                //insert file, note or attachment for that case
                ContentVersion contentVersion_1 = new ContentVersion();
                contentVersion_1.Title = textAttachment.filename;
                contentVersion_1.PathOnClient = textAttachment.filename;
                contentVersion_1.VersionData = Blob.valueOf(textAttachment.body);
                contentVersion_1.IsMajorVersion = true;                
                cvList.add(contentVersion_1);
                
            }
            if(!attBinList.isEmpty()){
                insert attBinList;
            }
            
            if(!cvList.isEmpty()){
                List<Database.SaveResult> CVInsertResult = Database.insert(cvList);
                List<Id> cvId = new List<Id>();
                List<ContentDocumentLink> cvLinkList = new List<ContentDocumentLink>();
                for(Database.SaveResult looping : CVInsertResult){
                    if(looping.isSuccess()){
                        cvId.add(looping.getId());
                    }                    
                }
                List<ContentVersion> contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id in: cvId];
                for(ContentVersion singleCV :contentVersion_2){
                    ContentDocumentLink singleCDlink = new ContentDocumentLink();
                    singleCDlink.ContentDocumentId = singleCV.ContentDocumentId;
                    singleCDlink.LinkedEntityId = caseId;
                    singleCDlink.ShareType = 'I';
                    cvLinkList.add(singleCDlink);
                }
                insert cvLinkList;
            }            
        }
                                     
        //Binary Attachment check
        if (Battach != null && Battach.size() > 0){
            List<Attachment> attBinList = new list<Attachment>();
            List<ContentVersion> cvBinaryList = new list<ContentVersion>();
            for(Messaging.InboundEmail.binaryAttachment bAttachment : Battach){
                Attachment att = new Attachment();
                att.name = bAttachment.filename;
                att.body = bAttachment.body;
                att.parentid = newEmail.id;
                attBinList.add(att);
                
                //insert file, note or attachment for that case
                ContentVersion contentVersion_1 = new ContentVersion();
                contentVersion_1.Title = bAttachment.filename;
                contentVersion_1.PathOnClient = bAttachment.filename;
                contentVersion_1.VersionData = bAttachment.body;
                contentVersion_1.IsMajorVersion = true;                
                cvBinaryList.add(contentVersion_1);               
            }
            if(!attBinList.isEmpty()){
                insert attBinList;
            }
            
            if(!cvBinaryList.isEmpty()){
                List<Database.SaveResult> CVInsertResult = Database.insert(cvBinaryList);
                List<Id> cvId = new List<Id>();
                List<ContentDocumentLink> cvLinkList = new List<ContentDocumentLink>();
                for(Database.SaveResult looping : CVInsertResult){
                    if(looping.isSuccess()){
                        cvId.add(looping.getId());
                    }                    
                }
                List<ContentVersion> contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id in: cvId];
                for(ContentVersion singleCV :contentVersion_2){
                    ContentDocumentLink singleCDlink = new ContentDocumentLink();
                    singleCDlink.ContentDocumentId = singleCV.ContentDocumentId;
                    singleCDlink.LinkedEntityId = caseId;
                    singleCDlink.ShareType = 'I';
                    cvLinkList.add(singleCDlink);
                }
                insert cvLinkList;
            }
        }                                    
    }

    public class contactResponseWrapper{
        private string contactStatus;
        private Contact Linkedcontact;
    }
    
    //Added this method by Wayne Ni for incoming multiple loop email scenario
    public boolean DuplicateEmailMessageCheckBeforeCaseCreation(Messaging.inboundEmail incomingEmail){
        boolean ifCreateCase;
        boolean IfHasSameEmail;
        //Query the related email message send from the same person in the from address of inbound email
        List<EmailMessage> EmailMsgSearchResult = [select id,TextBody,ParentId,HtmlBody,subject from EmailMessage where FromAddress =: incomingEmail.fromAddress and subject =:incomingEmail.subject];
        
        //get the related case record Ids
        Set<Id> relatedCaseIds = new Set<Id>();
        for(EmailMessage singleEmail: EmailMsgSearchResult){
            relatedCaseIds.add(singleEmail.ParentId);
        }
        
        //convert the related case into map
        Map<Id,Case> MessageParentCaseMap = new Map<Id,Case>([select id, recordtypeid,status from case where id in: relatedCaseIds]);      

        //Solution one:        
        for(EmailMessage singleEmail: EmailMsgSearchResult){
            
            String emailBody;       
            if (incomingEmail.plainTextBody != null && singleEmail.textBody!= null && singleEmail.textBody.split('\n').size() == incomingEmail.plaintextBody.split('\n').size()){
                emailBody = incomingEmail.plainTextBody;
                system.debug('text body size is '+singleEmail.textBody.split('\n').size());
                system.debug(incomingEmail.plaintextBody.split('\n').size());
                IfHasSameEmail = EmailComparebySplitString(singleEmail.textBody.split('\n'),emailBody.split('\n'));
            } else if(incomingEmail.plainTextBody == null && incomingEmail.htmlBody != null && 
                singleEmail.htmlbody != null && singleEmail.htmlbody.split('\n').size() == incomingEmail.htmlbody.split('\n').size()){
                system.debug('html body compare');
                emailBody = incomingEmail.htmlBody;
                IfHasSameEmail = EmailComparebySplitString(singleEmail.HtmlBody.split('\n'),emailBody.split('\n'));
            } else if(string.isEmpty(incomingEmail.plainTextBody) && string.isEmpty(incomingEmail.htmlBody) && string.isEmpty(singleEmail.TextBody)
                     && string.isEmpty(singleEmail.htmlBody)){
                system.debug('empty email check');
                IfHasSameEmail = true;          
            }
            
            system.debug('if has same email '+IfHasSameEmail);
            /*
			if(singleEmail.TextBody  == incomingEmail.plainTextBody && MessageParentCaseMap.get(singleEmail.ParentId).recordtypeid == CorpAffairCaseRecordTypeId
			&& MessageParentCaseMap.get(singleEmail.ParentId).status == 'open'){*/
            if(IfHasSameEmail  == true && MessageParentCaseMap.get(singleEmail.ParentId).recordtypeid == CorpAffairCaseRecordTypeId
               && MessageParentCaseMap.get(singleEmail.ParentId).status == 'open'){                       
                   //system.debug('Do not create case for the same inbound email');
                   return false;                
            }                        
        }
               
        return true;        
    }

    public boolean EmailComparebySplitString(List<String> EmailMessageSplitString, List<String> IncomgEmailSplitString){
		integer length = EmailMessageSplitString.size();
        integer i;
        
        for(i=0;i<length;i++){
            //system.debug('each element is '+EmailMessageSplitString[i] + ' and '+IncomgEmailSplitString[i]);
            if(EmailMessageSplitString[i] != IncomgEmailSplitString[i]){
                //system.debug('difference is '+EmailMessageSplitString[i] + ' and '+IncomgEmailSplitString[i]);
                return false;
            }                
        }
        
        return true;

    }    
}