@IsTest
private class CorporateAffairsEmailServiceHandler_test {
    
    @testSetup static void setup() {
        // Create test cases data
        Id CorpAffairCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Corporate Affairs').getRecordTypeId();
        Id CorpAffairContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Corporate Affairs').getRecordTypeId();
        Id corpaffairQueueId = [select Id from Group where  Type = 'Queue' AND developerName = 'Corporate_Affairs_Team' Limit 1].id;
        Id CorpAffairContactRecordtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Corporate Affairs').getRecordTypeId();
        Id CorpAffairAccoumtRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Institution').getRecordTypeId();
        
        //create test Account data
        Account CorpAffairDumpingAccount = new Account(name='Test Corporate Affair Account', recordtypeid=CorpAffairAccoumtRecordTypeId);
        
        //create test contact data
        Contact newCorpContact = new Contact();
        newCorpContact.LastName = 'test contact';
        newCorpContact.Email = 'testcotact@email.com';
        newCorpContact.RecordTypeId = CorpAffairContactRecordtype;
        
        newCorpContact.AccountId = CorpAffairDumpingAccount.Id;   
        
        insert newCorpContact;              
        System.debug('New Corp Affairs Contact Created to the Institution Account');
        
        List<case> ClosedCancelledCases = new List<case>();
        Case CorpAffairCase = new case();
        CorpAffairCase.Origin = 'Email';
        CorpAffairCase.RecordTypeId = CorpAffairCaseRecordTypeId;
        CorpAffairCase.Status = 'Closed';
        CorpAffairCase.subject = 'This is a test corp affair closed case';
        CorpAffairCase.Priority = '3-General';
        CorpAffairCase.ContactId = newCorpContact.Id;
        CorpAffairCase.Primary_Contact_Role__c = 'Government Representative';
        ClosedCancelledCases.add(CorpAffairCase);
        
        Case CorpAffairCase2 = new case();
        CorpAffairCase2.Origin = 'Email';
        CorpAffairCase2.RecordTypeId = CorpAffairCaseRecordTypeId;
        CorpAffairCase2.Status = 'Cancelled';
        CorpAffairCase2.subject = 'This is a test corp affair cancelled case';
        CorpAffairCase2.Priority = '3-General';
        CorpAffairCase2.ContactId = newCorpContact.Id;
        CorpAffairCase2.Primary_Contact_Role__c = 'Government Representative';
        ClosedCancelledCases.add(CorpAffairCase2);
        
        Case CorpAffairCase3 = new case();
        CorpAffairCase3.Origin = 'Email';
        CorpAffairCase3.RecordTypeId = CorpAffairCaseRecordTypeId;
        CorpAffairCase3.Status = 'Open';
        CorpAffairCase3.subject = 'This is a test corp affair open case';
        CorpAffairCase3.Priority = '3-General';
        CorpAffairCase3.ContactId = newCorpContact.Id;
        CorpAffairCase3.Primary_Contact_Role__c = 'Government Representative';
        ClosedCancelledCases.add(CorpAffairCase3);
        
        insert ClosedCancelledCases;
        
        //create a new message for the loop incoming email scenario
        EmailMessage existingEmail = new EmailMessage();
        existingEmail.parentId = CorpAffairCase3.id;
        existingEmail.FromAddress = 'testfromemail@address.com';
        existingEmail.Subject = 'test existing email subject';
        existingEmail.TextBody = 'Exising Emaiil textbody for testing purpose only';
        insert existingEmail;
        
    }
    
    @isTest static void testMethod1(){
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        /* Test Case - inbound email with */
        email.subject = 'This is a test corp case email';
        email.fromAddress = 'someaddress@email.com';
        email.fromname = 'Tester testing';
        email.plainTextBody = 'Test Plain text body';
        email.htmlBody = 'Test Html body';
        email.toAddresses = new String[]{System.Label.Corporate_Affairs_NBN_Electorate};
            
        // add an Binary attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('Corp affair attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            
        // add an Text atatchment  
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'Corp affair attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments = new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
            
        //Scenario one: Email to NBN Electorate org wide email
        Test.startTest();
        CorporateAffairsEmailServiceHandler TestCorpEmailCaseHandler = new CorporateAffairsEmailServiceHandler();
        TestCorpEmailCaseHandler.handleInboundEmail(email,env);
        Test.stopTest();
        
    }
    
    
    @isTest static void testMethod2(){
        //Scenario two: Email to NBN Correspondence org wide email
        Messaging.InboundEmail email2 = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        
        //Test Case - inbound email with one unique contact
        email2.subject = 'This is a test corp case email second scenario';
        email2.fromAddress = 'someaddress@email.com';
        email2.fromname = 'Tester testing';
        email2.plainTextBody = 'Test Plain text body';
        email2.htmlBody = 'Test Html body';
        email2.toAddresses = new String[]{System.Label.Corporate_Affairs_NBN_Correspondence};
            
        Test.startTest();
        CorporateAffairsEmailServiceHandler TestCorpEmailCaseHandler2 = new CorporateAffairsEmailServiceHandler();
        TestCorpEmailCaseHandler2.handleInboundEmail(email2,env2);  
        Test.stopTest();
        
    }
    
    @isTest static void testMethod3(){
        //Scenario two: Email to NBN Correspondence org wide email
        Messaging.InboundEmail email2 = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        
        //Test Case - inbound email with one unique contact
        email2.subject = 'This is a test corp case email second scenario';
        email2.fromAddress = 'someaddress@email.com';
        email2.fromname = 'Tester testing';
        email2.plainTextBody = 'Test Plain text body';
        email2.htmlBody = 'Test Html body';
        email2.toAddresses = new String[]{System.Label.Corporate_Affairs_NBN_Correspondence};
            
        Test.startTest();
        CorporateAffairsEmailServiceHandler TestCorpEmailCaseHandler2 = new CorporateAffairsEmailServiceHandler();
        TestCorpEmailCaseHandler2.handleInboundEmail(email2,env2);  
        Test.stopTest();
        
    }
    
    //Scenario for the close case
    @isTest static void testMethod4(){
        case cancelledcase = [select id,Thread_Id__c from case where subject = 'This is a test corp affair closed case'];
        
        Messaging.InboundEmail email2 = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        
        //Test Case - inbound email with one unique contact
        email2.subject = 'This is a test corp case email second scenario '+cancelledcase.Thread_Id__c;
        email2.fromAddress = 'someaddress@email.com';
        email2.fromname = 'Tester testing';
        email2.plainTextBody = 'Test Plain text body '+cancelledcase.Thread_Id__c;
        email2.htmlBody = 'Test Html body '+cancelledcase.Thread_Id__c;
        email2.toAddresses = new String[]{System.Label.Corporate_Affairs_NBN_Correspondence};
            
        Test.startTest();
        CorporateAffairsEmailServiceHandler TestCorpEmailCaseHandler2 = new CorporateAffairsEmailServiceHandler();
        TestCorpEmailCaseHandler2.handleInboundEmail(email2,env2);  
        Test.stopTest();
        
    }
    
    //scenario for the cancelled case
    @isTest static void testMethod5(){
        case cancelledcase = [select id,Thread_Id__c from case where subject = 'This is a test corp affair cancelled case'];
        
        Messaging.InboundEmail email2 = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        
        //Test Case - inbound email with one unique contact
        email2.subject = 'This is a test corp case email second scenario '+cancelledcase.Thread_Id__c;
        email2.fromAddress = 'someaddress@email.com';
        email2.fromname = 'Tester testing';
        email2.plainTextBody = 'Test Plain text body '+cancelledcase.Thread_Id__c;
        email2.htmlBody = 'Test Html body '+cancelledcase.Thread_Id__c;
        email2.toAddresses = new String[]{System.Label.Corporate_Affairs_NBN_Correspondence};
            
        Test.startTest();
        CorporateAffairsEmailServiceHandler TestCorpEmailCaseHandler2 = new CorporateAffairsEmailServiceHandler();
        TestCorpEmailCaseHandler2.handleInboundEmail(email2,env2);  
        Test.stopTest();        
    }
    
    //scenario for the same email message scenario
    @isTest static void testMethod6(){
        //create a new message for the loop incoming email scenario
        /*
        EmailMessage existingEmail = new EmailMessage();
        existingEmail.parentId = CorpAffairCase3.id;
        existingEmail.FromAddress = 'testfromemail@address.com';
        existingEmail.Subject = 'test existing email subject';
        existingEmail.TextBody = 'Exising Emaiil textbody for testing purpose only';
		*/
        
        Messaging.InboundEmail email3 = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env3 = new Messaging.InboundEnvelope();
        
        //Test Case - inbound email with one unique contact
        email3.subject = 'test existing email subject';
        email3.fromAddress = 'testfromemail@address.com';
        email3.fromname = 'Tester testing';
        email3.plainTextBody = 'Exising Emaiil textbody for testing purpose only';
        email3.toAddresses = new String[]{System.Label.Corporate_Affairs_NBN_Correspondence};
            
        Test.startTest();
        CorporateAffairsEmailServiceHandler TestCorpEmailCaseHandler2 = new CorporateAffairsEmailServiceHandler();
        TestCorpEmailCaseHandler2.handleInboundEmail(email3,env3);  
        Test.stopTest();  
        
        
    }
    

}