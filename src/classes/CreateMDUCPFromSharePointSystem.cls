/*******************************************************************************************************
    User Story: US#807
    Class Type: Rest Class
    Description: This class is being used to receive incoming request from sharePoint to create/update Site/Communication/Tracking
        Information record in salesforce on the basis of Location ID.
    Created Date: 10th May 2017
    Created By: Nagalingeswara Reddy
    Last Modified By: Jairaj Jadhav
*******************************************************************************************************/
@RestResource(urlMapping='/CreateMDUCPSite/*')
global class CreateMDUCPFromSharePointSystem{
    /**************************************************************************************************
        Description: This post method is responsible for create/update of Site/Communication/Tracking Information record in salesforce
            on the basis of Location ID.
        Return Type: MDUCPResponse (Wrapper Class)
        Created By: Nagalingeswara Reddy
        Last Modified By: Jairaj Jadhav
    **************************************************************************************************/
    @HttpPost
    global static MDUCPResponse createSite(){
        String jsonRequest = RestContext.request.requestBody.toString();
        JSON2Apex res = parse(jsonRequest);
        system.debug('res==>'+res);
        Site__c mduCpSite;
        Communication__c communicationObj;
        MDUCPResponse obj = new MDUCPResponse();
        string errorMessage;
        string trckngRecStatus;
        boolean isNewSite = false;
        List<Site__c> mducpSiteList;
        try{
            if(res.EPNoticeLocationId != null && res.EPNoticeLocationId != ''){
                id siteRTid = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('MDU/CP').getRecordTypeId();
                mducpSiteList = [Select Id,Location_Id__c from Site__c where Location_Id__c =:res.EPNoticeLocationId and recordTypeId =: siteRTid];
                if(mducpSiteList.isEmpty()){
                    mduCpSite = new Site__c();
                    mduCpSite.Location_Id__c = res.EPNoticeLocationId;
                    mduCpSite.Name = res.EPNoticeLocationId;
                    mduCpSite.recordTypeId = siteRTid;
                    insert mduCpSite;
                    isNewSite = true;
                }else{
                    mduCpSite = mducpSiteList[0];
                }
                
                trckngRecStatus = upsertTrckng(mduCpSite.id, isNewSite, res);
                if(trckngRecStatus != 'success'){system.debug('if tracking failed==>'+trckngRecStatus);
                    obj.response_status = 'Partial';
                }
            }else{
                errorMessage = 'Location ID is missing in the request.';
            }
            if(errorMessage != null && errorMessage != ''){
                obj.response_status = 'Fail';
                obj.response_message = errorMessage;
            }else{
                communicationObj = new Communication__c();
                communicationObj.MDU_CP__c = mduCpSite.id;
                communicationObj.recordTypeId = Schema.SObjectType.Communication__c.getRecordTypeInfosByName().get('LiFD Notices').getRecordTypeId();
                if(res.EPNoticeCommunicationType != null && res.EPNoticeCommunicationType != ''){
                    communicationObj.Communication_Type__c = res.EPNoticeCommunicationType;
                }
                if(res.EPNoticeCommunicationSubtype != null && res.EPNoticeCommunicationSubtype != ''){
                    communicationObj.Communication_Subtype__c = res.EPNoticeCommunicationSubtype;
                }
                if(res.SentDate != null && res.SentDate != ''){
                    res.SentDate = res.SentDate.split('/')[2]+'-'+res.SentDate.split('/')[1]+'-'+res.SentDate.split('/')[0];
                    communicationObj.Sent_Date__c = Date.valueOf(res.SentDate);
                }
                if(res.SignedDate != null && res.SignedDate != ''){
                    res.SignedDate = res.SignedDate.split('/')[2]+'-'+res.SignedDate.split('/')[1]+'-'+res.SignedDate.split('/')[0];
                    communicationObj.Signed_Date__c = Date.valueOf(res.SignedDate);
                }
                if(res.StartDate != null && res.StartDate != ''){
                    res.StartDate = res.StartDate.split('/')[2]+'-'+res.StartDate.split('/')[1]+'-'+res.StartDate.split('/')[0];
                    communicationObj.Start_Date__c = Date.valueOf(res.StartDate);
                }
                if(res.EndDate != null && res.EndDate != ''){
                    res.EndDate = res.EndDate.split('/')[2]+'-'+res.EndDate.split('/')[1]+'-'+res.EndDate.split('/')[0];
                    communicationObj.End_Date__c = Date.valueOf(res.EndDate);
                }
                if(res.EPNoticeAddressee != null && res.EPNoticeAddressee != ''){
                    communicationObj.Addressee__c = res.EPNoticeAddressee;
                }
                if(res.CRMValueDeliveryPartner != null && res.CRMValueDeliveryPartner != ''){
                    communicationObj.Contractor_Assigned__c = res.CRMValueDeliveryPartner;
                }
                if(res.EPNoticeStockCode != null && res.EPNoticeStockCode != ''){
                    communicationObj.Stock_Code__c = res.EPNoticeStockCode;
                }
                if(res.CRMValueDeliveryMethod != null && res.CRMValueDeliveryMethod != ''){
                    communicationObj.Delivery_Method__c = res.CRMValueDeliveryMethod;
                }
                if(res.EPNoticeOriginURL != null && res.EPNoticeOriginURL != ''){
                    communicationObj.Sharepoint_URL__c = res.EPNoticeOriginURL;
                    communicationObj.Document_Title__c = res.EPNoticeOriginURL.contains('/') ? res.EPNoticeOriginURL.substringAfterLast('/').replace('%20', ' '): '';
                }
                if(res.CommunicationFullAddress != null && res.CommunicationFullAddress != ''){
                    communicationObj.Address_Lines__c = res.CommunicationFullAddress;
                }
                insert communicationObj;
                obj.response_message = 'Communication Record Created Successfully!!';
                obj.communication_id = communicationObj.id;
                system.debug('before status check==>'+obj);
                if(obj.response_status == 'Partial'){system.debug('inside partial success check==>'+obj.response_status);
                    obj.response_message = 'Communication Record Created Successfully. '+ trckngRecStatus;
                }else{
                    obj.response_status = 'OK';
                }
            }
        }catch(exception e){
            obj.response_status = 'Fail';
            obj.response_message = 'Exception: '+e;
        }
        system.debug('before status return==>'+obj);
        return obj;
    }
    
    public static string upsertTrckng(id siteID, boolean isNew, JSON2Apex res){
        try{
            MDU_CP_Tracking_Information__c mducpTrkngInfo;
            boolean isUpsert = true;
            boolean isNewTrckRec = true;
            boolean isinvalidDP = false;
            string returnMessage = 'success';
            if(!isNew){
                List<MDU_CP_Tracking_Information__c> mducpTrkngInfoList = [Select Id,Engagement_Progress__c,Active_Status__c,Contractor_Assigned__c from MDU_CP_Tracking_Information__c where Site__c =: siteID LIMIT 1];
                if(!mducpTrkngInfoList.isEmpty() && mducpTrkngInfoList.size() > 0){
                    mducpTrkngInfo = mducpTrkngInfoList[0];
                    isNewTrckRec = false;
                }else{
                    mducpTrkngInfo = new MDU_CP_Tracking_Information__c();
                    mducpTrkngInfo.Site__c = siteID;
                }
            }else{
                mducpTrkngInfo = new MDU_CP_Tracking_Information__c();
                mducpTrkngInfo.Site__c = siteID;
            }
            if(res.CRMValueDeliveryPartner == null || res.CRMValueDeliveryPartner == ''){
                returnMessage = 'Delivery Partner assignment could not occur as blank Delivery Partner received.';
                if(isNewTrckRec){
                    isUpsert = false;
                    returnMessage = 'MDU CP Tracking creation failed for '+res.EPNoticeLocationId+'. No Delivery Partner received.';
                }
            }else{
                List<Schema.PicklistEntry> cntrctrAssgndValList = MDU_CP_Tracking_Information__c.Contractor_Assigned__c.getDescribe().getPicklistValues();
                set<string> cntrctrAssgndValSet = new set<string>();
                if(!cntrctrAssgndValList.isEmpty() && cntrctrAssgndValList.size() > 0){
                    for(Schema.PicklistEntry cntrctrAssgndVal: cntrctrAssgndValList){
                        cntrctrAssgndValSet.add(cntrctrAssgndVal.getValue());
                    }
                    if(!cntrctrAssgndValSet.contains(res.CRMValueDeliveryPartner)){
                        returnMessage = 'Delivery Partner assignment could not occur as Delivery Partner '+res.CRMValueDeliveryPartner+' does not exist in the system.';
                        isinvalidDP = true;
                    }
                    if(isNewTrckRec && isinvalidDP){
                        isUpsert = false;
                        returnMessage = 'MDU CP Tracking creation failed for '+res.EPNoticeLocationId+'. Delivery Partner '+res.CRMValueDeliveryPartner+' does not exist in the system.';
                    }
                }
            }
            if(isUpsert){system.debug('inside upsert==>'+isUpsert);
                boolean isModified = false;
                if(res.EPNoticeActiveStatus != null && res.EPNoticeActiveStatus != ''){
                    mducpTrkngInfo.Active_Status__c = res.EPNoticeActiveStatus;
                    isModified = true;
                }else if(isNewTrckRec){
                    mducpTrkngInfo.Active_Status__c = 'New';
                    isModified = true;
                }
                if(res.EPNoticeEngagementProgress != null && res.EPNoticeEngagementProgress != ''){
                    mducpTrkngInfo.Engagement_Progress__c = res.EPNoticeEngagementProgress;
                    isModified = true;
                }else if(isNewTrckRec){
                    mducpTrkngInfo.Engagement_Progress__c = 'New';
                    isModified = true;
                }
                if(res.CRMValueDeliveryPartner != null && res.CRMValueDeliveryPartner != '' && !isinvalidDP){
                    mducpTrkngInfo.Contractor_Assigned__c = res.CRMValueDeliveryPartner;
                    isModified = true;
                }
                if(isModified){
                    upsert mducpTrkngInfo;
                }
                return returnMessage;
            }else{
                return returnMessage;
            }
        }catch(exception e){system.debug('failure==>'+e);
            return e.getMessage();
        }
    }
    
    global class MDUCPResponse{
        public String response_status;
        public String response_message;
        public String communication_id;
    }
    
    public class JSON2Apex{
        public String EPNoticeCommunicationType;
        public String EPNoticeCommunicationSubtype;
        public String EPNoticeAddressee;
        public String CRMValueDeliveryPartner;
        public String SentDate;
        public String SignedDate;
        public String StartDate;
        public String EndDate;
        public String EPNoticeLocationId;
        public String EPNoticeStockCode;
        public String CRMValueDeliveryMethod;
        public String EPNoticeOriginURL;
        public String CommunicationFullAddress;
        public String EPNoticeActiveStatus;
        public String EPNoticeEngagementProgress;
    }
    
    public static JSON2Apex parse(String json){
        return (JSON2Apex) System.JSON.deserialize(json, JSON2Apex.class);
    }
}