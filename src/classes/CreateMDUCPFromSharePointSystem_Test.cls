/***************************************************************************************************
Class Name:  CreateMDUCPFromSharePointSystem_Test
Class Type: Test Class
Created Date: 05th June 2017
Function    : This is test class for CreateMDUCPFromSharePointSystem.
****************************************************************************************************/
@isTest
public class CreateMDUCPFromSharePointSystem_Test{
    static testMethod void testMethod1(){
        List<site__c> mducpSiteList = new List<site__c>();
        for(integer i = 0; i < 2; i++){
            site__c mducpSite = new site__c();
            mducpSite.recordTypeId = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('MDU/CP').getRecordTypeId();
            mducpSite.name = 'LOC9900000000'+string.valueOf(49+i);
            mducpSite.Location_Id__c = 'LOC9900000000'+string.valueOf(49+i);
            mducpSiteList.add(mducpSite);
        }
        MDU_CP_Tracking_Information__c mducpTrkngInfo = new MDU_CP_Tracking_Information__c();
        mducpTrkngInfo.Active_Status__c = 'New';
        mducpTrkngInfo.Engagement_Progress__c = 'New';
        mducpTrkngInfo.Contractor_Assigned__c = 'Broadspectrum';
        string jsonRequest = '{"EPNoticeCommunicationType":"Schd 3 Notice/Legal Document", "EPNoticeCommunicationSubtype": "Installation (LiFD2) - FTTP", "EPNoticeAddressee": "To the Legal Entity", "CRMValueDeliveryPartner":"Daly" , "SentDate":"26/01/2015", "SignedDate": "26/01/2015", "StartDate": "11/02/2015", "EndDate":"18/04/2015", "EPNoticeLocationId": "LOC990000000048", "EPNoticeStockCode":"CON:048h Jul 2015", "CRMValueDeliveryMethod": "Post", "EPNoticeOriginURL":"https://docs-sit4.nbnco.net.au/shared/S0035/Cisco/notice/Regression%20test%2031052017.pdf", "CommunicationFullAddress": "ThisAddressl ThisAddress2 NSW 1111 ","EPNoticeEngagementProgress":"LIFD2 Notice Sent","EPNoticeActiveStatus":"In Progress"}';
        test.startTest();
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/DemoUrl';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof(jsonRequest);
            RestContext.request = req;
            RestContext.response= res;
            CreateMDUCPFromSharePointSystem.createSite();
            insert mducpSiteList;
            jsonRequest = '{"EPNoticeCommunicationType":"Schd 3 Notice/Legal Document", "EPNoticeCommunicationSubtype": "Installation (LiFD2) - FTTP", "EPNoticeAddressee": "To the Legal Entity", "CRMValueDeliveryPartner":"Daly" , "SentDate":"26/01/2015", "SignedDate": "26/01/2015", "StartDate": "11/02/2015", "EndDate":"18/04/2015", "EPNoticeLocationId": "LOC990000000049", "EPNoticeStockCode":"CON:048h Jul 2015", "CRMValueDeliveryMethod": "Post", "EPNoticeOriginURL":"https://docs-sit4.nbnco.net.au/shared/S0035/Cisco/notice/Regression%20test%2031052017.pdf", "CommunicationFullAddress": "ThisAddressl ThisAddress2 NSW 1111 ","EPNoticeEngagementProgress":"LIFD2 Notice Sent","EPNoticeActiveStatus":"In Progress"}';
            req.requestBody = Blob.valueof(jsonRequest);
            RestContext.request = req;
            CreateMDUCPFromSharePointSystem.createSite();
            mducpTrkngInfo.Site__c = mducpSiteList[1].id;
            insert mducpTrkngInfo;
            jsonRequest = '{"EPNoticeCommunicationType":"Schd 3 Notice/Legal Document", "EPNoticeCommunicationSubtype": "Installation (LiFD2) - FTTP", "EPNoticeAddressee": "To the Legal Entity", "CRMValueDeliveryPartner":"Daly" , "SentDate":"26/01/2015", "SignedDate": "26/01/2015", "StartDate": "11/02/2015", "EndDate":"18/04/2015", "EPNoticeLocationId": "LOC990000000050", "EPNoticeStockCode":"CON:048h Jul 2015", "CRMValueDeliveryMethod": "Post", "EPNoticeOriginURL":"https://docs-sit4.nbnco.net.au/shared/S0035/Cisco/notice/Regression%20test%2031052017.pdf", "CommunicationFullAddress": "ThisAddressl ThisAddress2 NSW 1111 ","EPNoticeEngagementProgress":"LIFD2 Notice Sent","EPNoticeActiveStatus":"In Progress"}';
            req.requestBody = Blob.valueof(jsonRequest);
            RestContext.request = req;
            CreateMDUCPFromSharePointSystem.createSite();
            jsonRequest = '{"EPNoticeCommunicationType":"Schd 3 Notice/Legal Document", "EPNoticeCommunicationSubtype": "Installation (LiFD2) - FTTP", "EPNoticeAddressee": "To the Legal Entity", "CRMValueDeliveryPartner":"Daly" , "SentDate":"26/01/2015", "SignedDate": "26/01/2015", "StartDate": "11/02/2015", "EndDate":"18/04/2015", "EPNoticeLocationId": "", "EPNoticeStockCode":"CON:048h Jul 2015", "CRMValueDeliveryMethod": "Post", "EPNoticeOriginURL":"https://docs-sit4.nbnco.net.au/shared/S0035/Cisco/notice/Regression%20test%2031052017.pdf", "CommunicationFullAddress": "ThisAddressl ThisAddress2 NSW 1111 ","EPNoticeEngagementProgress":"LIFD2 Notice Sent","EPNoticeActiveStatus":"In Progress"}';
            req.requestBody = Blob.valueof(jsonRequest);
            RestContext.request = req;
            CreateMDUCPFromSharePointSystem.createSite();
        test.stopTest();
    }
}