global with sharing class CustomButtonSynchronizeWithOpportunity extends csbb.CustomButtonExt {
  public String performAction (String basketId) { 
    //Error condition
    //'{"status":"error","title":"Error","text":"Synchronize to opportunity failed"}'
    
    //Success & redirect
    //'{"status":"ok","redirectURL":"http://www.google.com","target":"1"}';
    
    // Success and remain in page
    //return '{"status":"ok","title":"Success","text":"Synchronize to opportunity successful"}';
    
    String newUrl = CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(basketId);
    
    return '{"status":"ok", "title":"Success","text":"Basket Synchronised", "redirectURL":"' + newUrl + '"}';
  }

  public static String syncWithOpportunity (String basketId) {
    // delete orphaned root configurations (associated with the basket but not product configuration request)
    List <cscfga__Product_Configuration__c> orphanProdConfs = 
      [select c.id
      from cscfga__Product_Configuration__c c
      where 
        cscfga__product_basket__c = :basketId and
        cscfga__root_configuration__c = null and
        id not in 
          (select csbb__product_configuration__c
          from csbb__Product_Configuration_Request__c
          where csbb__product_basket__c = :basketId
          and csbb__product_configuration__c != null
          )
      ];
    
     System.debug('*** CW CustomButtons SynchronizeWithOpportunity ');
    
    if (orphanProdConfs.size() > 0) {
      delete orphanProdConfs;
    }
    
    cscfga__Product_Basket__c pb = [select id, cscfga__opportunity__c, csbb__Synchronised_with_Opportunity__c, cscfga__Basket_Status__c  
                                    from cscfga__Product_Basket__c where id = :basketId];

    if(pb.cscfga__Basket_Status__c != 'Valid') {

        return 'Synchronise with opportunity failed: product basket has to be valid.';
    }
         System.debug('*** CW PB status' + pb.cscfga__Basket_Status__c );
    if (pb.csbb__Synchronised_with_Opportunity__c)
    {
      pb.csbb__Synchronised_with_Opportunity__c=false;
      pb.csordtelcoa__Synchronised_with_Opportunity__c = false;
      update pb;
    }
      
    pb.csbb__Synchronised_with_Opportunity__c = true;
    pb.csordtelcoa__Synchronised_with_Opportunity__c = true;
    update pb;
      
    Opportunity oppToGoBackTo = [select id from Opportunity where id = :pb.cscfga__Opportunity__c];
    
    PageReference oppPage = new ApexPages.StandardController(oppToGoBackTo).view();
    
    return oppPage.getUrl();
  }
  
}