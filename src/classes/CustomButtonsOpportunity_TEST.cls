/*
* @author - Cloudsense AU
* @date - 5.1.2017
* @details - class covers both - CustomButtonSynchronizeWithOpportunity and CustomButtonReturnToOpportunity classes
*
*/

@isTest
global class CustomButtonsOpportunity_TEST{


    // covers CustomButtonReturnToOpportunity class .
    @isTest global static void testReturnToOpportunity(){
        
        Account testAccount =   TestDataClass.CreateTestAccountData(); //List<Account> testAccounts = TestDataUtility.getListOfAccounts(1,true);
        
        
        
        system.assert(testAccount.id!=null);
        
        Opportunity testOpty = TestDataClass.CreateTestOpportunityData(); //Opportunity testOpty = new Opportunity(Name = 'testOpty_testReturnToOpportunity'  , AccountId = testAccount.id, Amount=100);
        
        system.assert(testOpty.id!=null);
        
        cscfga__Product_Basket__c testBasket  = new cscfga__Product_Basket__c(Name = 'BasketReturn_To_Opty' , cscfga__opportunity__c =testOpty.id );
        
        
        insert testBasket;
        
        system.assert(testBasket.id!=null);
        CustomButtonReturnToOpportunity custB  = new CustomButtonReturnToOpportunity();
        String x =custB.performAction(testBasket.id+'');
        //testing 1st method
        System.assert( x!=null);
        //testing 2nd method
        System.assert( CustomButtonReturnToOpportunity.openOpportunity(testBasket.id)!=null );
    }
    
    //covers CustomButtonSynchronizeWithOpportunity class
    @isTest global static void testSyncWithOpportunity(){
    
        Account testAccount =   TestDataClass.CreateTestAccountData(); //List<Account> testAccounts = TestDataUtility.getListOfAccounts(1,true);
        
        system.assert(testAccount.id!=null);
        
        Opportunity testOpty = TestDataClass.CreateTestOpportunityData(); //new Opportunity(Name = 'testOpty_testReturnToOpportunity' , StageName = 'New' , ClosedDate=  , AccountId = testAccount.id, Amount=100);
        
        
        system.assert(testOpty.id!=null);
        
        cscfga__Product_Basket__c testBasket  = new cscfga__Product_Basket__c(cscfga__Basket_Status__c = 'Valid', csbb__Synchronised_with_Opportunity__c = true, Name = 'Sync_To_Opty' , cscfga__opportunity__c = testOpty.id );
        
        
        insert testBasket;
        
        system.assert(testBasket.id!=null);
        
        CustomButtonSynchronizeWithOpportunity custB  = new CustomButtonSynchronizeWithOpportunity();
        
        String URLx = custB.performAction(testBasket.id+'');
     
        System.assert( URLx!=null);
        
        
        //testing bad useCASE 
        
        cscfga__Product_Basket__c testBasketError  = new cscfga__Product_Basket__c( csbb__Synchronised_with_Opportunity__c = true, Name = 'Sync_To_Opty' , cscfga__opportunity__c = testOpty.id );
        insert testBasketError;
        
        system.assert(testBasketError.id!=null);
        
        CustomButtonSynchronizeWithOpportunity custBError  = new CustomButtonSynchronizeWithOpportunity();
        
        String urlY = custBError.performAction(testBasketError.id+'');
     
        System.assert( urlY!=null);

        //cscfga__Product_Configuration__c testConfigOrphan = new cscfga__Product_Configuration__c(cscfga__root_configuration__c= null, cscfga__product_basket__c = testBasketError.id ,);
        
        
        
        
        
        
        
    }


}