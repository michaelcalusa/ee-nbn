public class CustomSearch_LC{
/*------------------------------------------------------------  
Author:        Kashyap Murthuy
Company:       Contractor
Description:   Lightning controller used in NBNHomePageSearch component. Allows a user to search just on a specific object. Custom component used rather than global search so that the search
               be focused. 
Test Class:    CustomSearch_Test
History
<Date>      <Authors Name>     <Brief Description of Change>
------------------------------------------------------------*/
    
    @AuraEnabled
    public static JIGSAW_SummaryTableController.SummaryWrapper doQuickSearch(String searchText,String componentName, String sObjectName, String sortString){
        List<SObject> objList = new List<SObject>();
		JIGSAW_SummaryTableController.SummaryWrapper objWrapper = new JIGSAW_SummaryTableController.SummaryWrapper();
		// Add columns in the table
		List <JIGSAW_SummaryTableController.DataTableColumns> lstColumns = new List <JIGSAW_SummaryTableController.DataTableColumns> ();
		try{
			if(String.IsNotBlank(searchText)){
				// prepare columns
				QueueController_LC.LghtDataTableAttribts dataTableAttr = new QueueController_LC.LghtDataTableAttribts();
				dataTableAttr = QueueController_LC.prepareLghtDataTableAttribts(componentName, sObjectName);
				system.debug('mapOfColumnOrderTableAttr==>'+dataTableAttr.mapOfColumnOrderTableAttr);
				if(dataTableAttr.listOfColumnsOrder <> null)
					dataTableAttr.listOfColumnsOrder.sort();
				
				for(integer columnNumber :dataTableAttr.listOfColumnsOrder){
					if(dataTableAttr.mapOfColumnOrderTableAttr <> null && dataTableAttr.mapOfColumnOrderTableAttr.get(columnNumber) <> null)
						lstColumns.add(dataTableAttr.mapOfColumnOrderTableAttr.get(columnNumber));
				}
				// Prepare Data
				String objectFields = GlobalUtility.createSOQLFieldString(dataTableAttr.listOfFieldApiNames);
				if(String.isBlank(objectFields))
					objectFields = 'Id';
				// Dev: SyedMoosaNazirTN - Story: CUSTSA-10330 - AC2 - Remove leading or trailing spaces
				searchText = searchText.trim();
				if (searchText.containsIgnoreCase('PRI')){
					String translatedSearchText = searchText.toUpperCase();
					String soql = '';
					if(searchText.contains('*')){
						translatedSearchText = '';
						translatedSearchText = searchText.replace('*','%');
					}
					else{
						translatedSearchText = processSearchText('PRI',translatedSearchText);
					}
					soql = 'select ' + objectFields+ ' from '+ sObjectName +' where PRI_ID__c LIKE :translatedSearchText '+sortString+' NULLS LAST LIMIT 1000';
					objList = Database.query(soql);
				}
				if (searchText.containsIgnoreCase('WRQ')){
					String translatedSearchText = searchText.toUpperCase();
					String soql = '';
					if(searchText.contains('*')){
						translatedSearchText = '';
						translatedSearchText = searchText.replace('*','%');
					}
					else{
						translatedSearchText = processSearchText('WRQ',translatedSearchText);
					}
					soql = 'select ' + objectFields+ ' from '+ sObjectName +' where WOR_ID__c LIKE :translatedSearchText '+sortString+' NULLS LAST LIMIT 1000';
					objList = Database.query(soql);
				}
				if (searchText.containsIgnoreCase('INC')){
						// As this field is marked as case sensitive, need to do an explicit case conversion to cater to case insensitive search
						String translatedSearchText = searchText.toUpperCase();
						String soql = '';
						if(searchText.contains('*')){
							translatedSearchText = '';
							translatedSearchText = searchText.toUpperCase().replace('*','%');
						}
						else{
							translatedSearchText = processSearchText('INC',translatedSearchText);
						}
						system.debug('translatedSearchText==>	'+translatedSearchText);
						soql = 'select ' + objectFields+ ' from '+ sObjectName +' where Incident_Number__c LIKE :translatedSearchText '+sortString+' NULLS LAST LIMIT 1000';
						objList = Database.query(soql);
				}
				if ((!searchText.containsIgnoreCase('INC')) && (!searchText.containsIgnoreCase('WRQ')) && (!searchText.containsIgnoreCase('PRI'))){
					objList = new List<SObject>();
					String translatedSearchText = searchText.toUpperCase();
					String soql = '';
					if(searchText.contains('*')){
						translatedSearchText = '';
						translatedSearchText = searchText.replace('*','%');
						if (String.isBlank(soql)){
							soql = 'select ' + objectFields+ ' from '+ sObjectName +' where Incident_Number__c LIKE :translatedSearchText  OR WOR_ID__c LIKE :translatedSearchText OR PRI_ID__c LIKE :translatedSearchText '+sortString+' NULLS LAST LIMIT 1000';
							objList = Database.query(soql);
						}
					}
					else{
						String filteredSearchText = processSearchText('0',translatedSearchText);
						if (String.isBlank(soql)){
							soql = 'select ' + objectFields+ ' from '+ sObjectName +' where Incident_Number__c LIKE :filteredSearchText  OR WOR_ID__c LIKE :filteredSearchText OR PRI_ID__c LIKE :filteredSearchText '+sortString+' NULLS LAST LIMIT 1000';
							objList = Database.query(soql);
						}
					}
				}
			}
			// Fill Wrapper
			objWrapper.lstColumns = lstColumns;
			objWrapper.totalRecords = objList.size();
			objWrapper.lstSobjectData = objList;
		}catch(exception ex){
			system.debug('getMessage==>'+Ex.getMessage()+'getStackTraceString'+Ex.getStackTraceString());
			GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'CustomSearch_LC','doQuickSearch','','',Ex.getMessage(), Ex.getStackTraceString(),Ex, 0);        
        }
		return objWrapper;
    }
	// Dev: SyedMoosaNazirTN - Story: CUSTSA-18401 - Remove leading zeros after INC/PRI/WRQ. Example: "INC000001203" will be searched as "%1203%"
	public static String processSearchText (String searchTextToConsider, String SearchText){
		string translatedSearchText;
		if(SearchText.startsWithIgnoreCase(searchTextToConsider)){
			List<String> splitTranslatedSearchText;
			if(searchTextToConsider == '0')
				splitTranslatedSearchText = SearchText.split('');
			else
				splitTranslatedSearchText = SearchText.split(searchTextToConsider);
			if(splitTranslatedSearchText <> null && !splitTranslatedSearchText.isEmpty() && splitTranslatedSearchText.size() > 1 && splitTranslatedSearchText.get(1) <> null){
				List<String> splitIncidentNumberList;
				if(searchTextToConsider == '0')
					splitIncidentNumberList = splitTranslatedSearchText;
				else{
					string incidentNumberWithoutPrefix = splitTranslatedSearchText.get(1);
					splitIncidentNumberList = incidentNumberWithoutPrefix.split('');
				}
				string incidentNumber;
				for(string incidentCharacter : splitIncidentNumberList){
					if(incidentCharacter == '0' && String.isBlank(incidentNumber)){
						continue;
					}
					else{
						if(String.isBlank(incidentNumber))
							incidentNumber = incidentCharacter;
						else
							incidentNumber = incidentNumber + incidentCharacter;
					}
				}
				translatedSearchText = '%' + incidentNumber + '%';
			}
            else{
                system.debug('SearchText=='+SearchText);
                translatedSearchText = '%' + SearchText + '%';
            } 
		}
		else{
			translatedSearchText = '%' + SearchText + '%';
		}
        system.debug('translatedSearchText======'+translatedSearchText);
		if(String.isNotBlank(translatedSearchText))
			return translatedSearchText;
		else
			return SearchText;
	}
    public static String createSOQLFieldString(String[] SOQLFields){
        String commaSeparatedFields = 'Id';
        for(String fieldName : SOQLFields){
            if(String.isBlank(commaSeparatedFields)){
                commaSeparatedFields = fieldName;
            }else{
                commaSeparatedFields = commaSeparatedFields + ', ' + fieldName;
            }
        }
        return commaSeparatedFields;
    }

}