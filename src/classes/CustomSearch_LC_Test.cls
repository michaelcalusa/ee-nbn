@isTest
public class CustomSearch_LC_Test{
    static testMethod void SearchIncidentTest() {
        INSERT new LightningComponentConfigurations__c(name='NBNHomePageSearch',Component__c='NBNHomePageSearch',Object_API_Name__c='Incident_Management__c',Role_or_Profile_Based__c='Profile',Role_or_Profile_Name__c='System Administrator');
        Test.StartTest();
        CustomSearch_LC.doQuickSearch('INC000123','NBNHomePageSearch','Incident_Management__c','ORDER BY CurrentslaDueDateCalc__c ASC');
        CustomSearch_LC.doQuickSearch('INC*','NBNHomePageSearch','Incident_Management__c','ORDER BY CurrentslaDueDateCalc__c ASC');
        Test.StopTest();
    }
    static testMethod void SearchPriTest() {
        INSERT new LightningComponentConfigurations__c(name='NBNHomePageSearch',Component__c='NBNHomePageSearch',Object_API_Name__c='Incident_Management__c',Role_or_Profile_Based__c='Profile',Role_or_Profile_Name__c='System Administrator');
        Test.StartTest();
        CustomSearch_LC.doQuickSearch('PRI000123','NBNHomePageSearch','Incident_Management__c','ORDER BY CurrentslaDueDateCalc__c ASC');
        CustomSearch_LC.doQuickSearch('PRI*','NBNHomePageSearch','Incident_Management__c', 'ORDER BY CurrentslaDueDateCalc__c ASC');
        Test.StopTest();
    }
    static testMethod void SearchWorkRequestTest() {
        INSERT new LightningComponentConfigurations__c(name='NBNHomePageSearch',Component__c='NBNHomePageSearch',Object_API_Name__c='Incident_Management__c',Role_or_Profile_Based__c='Profile',Role_or_Profile_Name__c='System Administrator');
        Test.StartTest();
        CustomSearch_LC.doQuickSearch('WRQ000123','NBNHomePageSearch','Incident_Management__c','ORDER BY CurrentslaDueDateCalc__c ASC');
        CustomSearch_LC.doQuickSearch('WRQ*','NBNHomePageSearch','Incident_Management__c','ORDER BY CurrentslaDueDateCalc__c ASC');
        Test.StopTest();
    }
    static testMethod void SearchOtherTest() {
        INSERT new LightningComponentConfigurations__c(name='NBNHomePageSearch',Component__c='NBNHomePageSearch',Object_API_Name__c='Incident_Management__c',Role_or_Profile_Based__c='Profile',Role_or_Profile_Name__c='System Administrator');
        Test.StartTest();
        CustomSearch_LC.doQuickSearch('123','NBNHomePageSearch','Incident_Management__c','ORDER BY CurrentslaDueDateCalc__c ASC');
        Test.StopTest();
    }   
}