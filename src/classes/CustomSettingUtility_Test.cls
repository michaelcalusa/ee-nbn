/*------------------------------------------------------------  
Author:        Dave Norris
Company:       Salesforce
Description:   A utility class to manage custom settings for test classes
               Creates custom settings with required attribute values
History
26/03/2018      Sethuram     Added test setup method to spike up the coverage to 100%
                             Need to assert for unit testing - postpone to later release
------------------------------------------------------------*/
@isTest
public class CustomSettingUtility_Test {

    public static Application_Log_Settings__c applicationLogSettings;

    //get the settings. handles the case where the managed value doesn't exist yet
    public static Application_Log_Settings__c getApplicationLogSettingsForTests( Application_Log_Settings__c mySettings ) {
    /*------------------------------------------------------------
    Author:        Dave Norris
    Company:       Salesforce
    Description:   Function to return a new custom setting object for Application_Log_Settings__c object
    Inputs:        An sObject with required values
    Returns:       An Application_Log_Settings__c sObject populated with requested values
    History
    <Date>      <Authors Name>     <Brief Description of Change>
    ------------------------------------------------------------*/
        //clear out whatever settings exist
        delete [select id from Application_Log_Settings__c];

        //create our own based on what's passed in from the test
        applicationLogSettings = new Application_Log_Settings__c (
            Error__c = mySettings.Error__c,
            Debug__c = mySettings.Debug__c,
            Warning__c = mySettings.Warning__c,
            Info__c = mySettings.Info__c,
            Delete_After_Days__c = mySettings.Delete_After_Days__c
            );

        insert applicationLogSettings;

        return applicationLogSettings;
    }

    // method to insert test data for all the custom settings
    @testSetup static void setupMethod() {

      insert new InboundPlatformEventMap__c(name='incidentData', IntegrationType__c = 'RemedyToSalesforce', Is_Array__c=false, Is_Child_Element__c=false, Is_Object_Update__c=false, Is_Parent_Element__c=true, JSONParentKey__c='data', Child_Node_Name__c='', Object__c='Incident_Management__c');
      insert new InboundPlatformEventMap__c(name='sfAppointments', IntegrationType__c = 'RemedyToSalesforce', Is_Array__c=true, Is_Child_Element__c=true, Is_Object_Update__c=true, Is_Parent_Element__c=true, JSONParentKey__c='sfAppointments', Child_Node_Name__c='appointmentData', Object__c='Incident_Management__c');
      insert new InboundPlatformEventMap__c(name='sfAssociations', IntegrationType__c = 'RemedyToSalesforce', Is_Array__c=true, Is_Child_Element__c=true, Is_Object_Update__c=false, Is_Parent_Element__c=true, JSONParentKey__c='sfAssociations', Child_Node_Name__c='associationData', Object__c='Remedy_Association__c');

      insert new RemedyIncidentDetailsJSONtoSFFields__c(name='appointmentEndDate', IntegrationType__c = 'RemedyToSalesforce', DeveloperName__c='', Is_Datetime_Field__c=true, Is_External_Id__c=false, Is_Lookup_Field__c=false, JSONKey__c='appointmentEndDate', Object__c='Incident_Management__c', Parent_JSON_Key__c='appointmentData', Salesforce_Field__c='Appointment_End_Date__c');
      insert new RemedyIncidentDetailsJSONtoSFFields__c(name='appointmentIdentifier', IntegrationType__c = 'RemedyToSalesforce', DeveloperName__c='', Is_Datetime_Field__c=false, Is_External_Id__c=false, Is_Lookup_Field__c=false, JSONKey__c='appointmentIdentifier', Object__c='Incident_Management__c', Parent_JSON_Key__c='appointmentData', Salesforce_Field__c='AppointmentId__c');
      insert new RemedyIncidentDetailsJSONtoSFFields__c(name='appointmentNotes', IntegrationType__c = 'RemedyToSalesforce', DeveloperName__c='', Is_Datetime_Field__c=false, Is_External_Id__c=false, Is_Lookup_Field__c=false, JSONKey__c='appointmentNotes', Object__c='Incident_Management__c', Parent_JSON_Key__c='appointmentData', Salesforce_Field__c='Appointment_details__c');

      insert new LightningComponentConfigurations__c(name='displayIncidentNotes1',Component__c='displayIncidentNotes',Object_API_Name__c='Incident__c',Role_or_Profile_Based__c='Profile',Role_or_Profile_Name__c='System Administrator');
      insert new LightningComponentConfigurations__c(name='displayIncidentNotes2',Component__c='displayIncidentNotes',Object_API_Name__c='Incident__c',Role_or_Profile_Based__c='Role',Role_or_Profile_Name__c='CEO');

      insert new NBNIntegrationTranslation__c(name='AVC-D',DownStreamVal__c='',IntegrationValueReturned__c='avc-d',IntKey__c='Type',UpStreamVal__c='',User_Display_Value__c='Data');

      insert new RemedyIncidentDetailsJSONtoSFFields__c(name='appointmentStartDate',Object__c='Incident_Management__c',JSONKey__c='appointmentStartDate',Salesforce_Field__c='Appointment_Start_Date__c',IntegrationType__c='RemedyToSalesforce',Is_Datetime_Field__c=true,Parent_JSON_Key__c='appointmentData');

      insert new NBNTimeZone__c(name='VIC',TimeZone__c='Australia/Sydney');
    }

      public static testMethod void method1(){

       try {
          Map<String, Boolean> dateTimeTmp = CustomSettingsUtility.getDateTimeSaleforceFields('test');
          system.assertEquals(true,dateTimeTmp.get('Appointment_Start_Date__c'));
          List<LightningComponentConfigurations__c> tmpLgtCompConfig = CustomSettingsUtility.getLightningComponentConfigurationJSON('displayIncidentNotes','Incident__c');
          system.assertEquals(2,tmpLgtCompConfig.size());
          String tz = CustomSettingsUtility.getTimeZoneStringVal('VIC');
          system.assertEquals('Australian Eastern Standard Time',tz);
          String testDispVal = CustomSettingsUtility.getDisplayVal('test','avc-d','Type');
          system.assertEquals('Data',testDispVal);
          CustomSettingsUtility.getIntTrans(NBNIntegrationTranslation__c.getAll().values()[0],'avc-d','Type');
          CustomSettingsUtility.getLightningComponentConfiguration('displayIncidentNotes');
          CustomSettingsUtility.getLightningComponentConfiguration('displayIncidentNotes','Incident__c');
          CustomSettingsUtility.getObjectString('Account');
          CustomSettingsUtility.getSalesforceFieldsAsMap('test');
          CustomSettingsUtility.getSalesforceFields('test');
          CustomSettingsUtility.getAllRemedyIncidentDetailsJSONtoSFFieldsMap('{}');
          CustomSettingsUtility.getAllRemedyIncidentDetailsJSONtoSFFields('{}');
          CustomSettingsUtility.getAllInboundPlatformEventMap('RemedyToSalesforce');
          CustomSettingsUtility.getAll();
          CustomSettingsUtility.getFieldNames(new list<String>());
           String timeZone = CustomSettingsUtility.getTimeZoneStringVal('getTZ');
          CustomSettingsUtility.getObjectConfig(LightningComponentConfigurations__c.getAll().values(),'System Administrator','CEO');

          insert new NBNIntegrationCache__c(AVCId__c = 'AVC000028840963',JSONPayload__c = '{ "data" : { "type" : "avc-d", "id" : "AVC000000773235", "attributes" : { "elementManager" : "SAM", "bandwidthProfile" : "D25_U5-10_Mbps_TC4_P, D5_U5_Mbps_TC2_C", "avcType" : "1:1", "avcResourceId" : "ALK000000000006", "primaryAccessTechnology" : "Fibre To The Building", "poiName" : "2MAI - MAITLAND", "avcConfig" : { "minLay1UpBitRate" : "5100", "minLay1DownBitRate" : "25900", "insertDslLineRate" : "True", "icsServiceId" : "ILK000000414065" }, "aas" : { "aasName" : "SWAAS0000102", "svlan" : "3000", "ipAddress" : "1.1.1.1", "primaryPort" : "1/1/1", "secondaryPort" : "1/1/2", "connectedtoPort" : "1/1/3" }, "cvc" : { "cvcId" : "CVC000000190598", "cvcResourceId" : "CLK000000000006", "tc1BandwidthProfile" : "D200_U200_Mbps_TC1_C", "tc2BandwidthProfile" : "D200_U200_Mbps_TC2_C", "tc4BandwidthProfile" : "D2000_U2000_Mbps_TC4_C", "mcBandwidthProfile" : "D2000_U2000_Mbps_TC4_C" }, "uniDsl" : { "uniDslId" : "CPI300000418831", "uniDslVlanMode" : "Tagged", "dslStabilityProfile" : "Standard", "potsInterconnect" : 245456780, "uniDslMode" : "VDSL2", "exchangePairConnection" : "No", "csll" : { "id" : "PRI000004401447", "type" : "USL", "productStatus" : "Terminated" } }, "nniNodes" : [ { "nniId" : "SWEAS0000105", "nniType" : "i-nni", "nniCvlanId" : "1", "nniSvlanId" : "1", "nniLagId" : "102", "nniIpAddress" : "1.1.1.1" }, { "nniId" : "SWEAS0000106", "nniType" : "i-nni", "nniCvlanId" : "1", "nniSvlanId" : "1", "nniLagId" : "103", "nniIpAddress" : "2.2.2.2" }, { "nniId" : "SWEAS0000107", "nniType" : "e-nni", "nniCvlanId" : "1", "nniSvlanId" : "1", "nniLagId" : "104", "nniIpAddress" : "3.3.3.3" }, { "nniId" : "SWEAS0000108", "nniType" : "e-nni", "nniCvlanId" : "1", "nniSvlanId" : "1", "nniLagId" : "105", "nniIpAddress" : "4.4.4.4" } ] }, "relationships" : { "associatedWith" : { "data" : { "id" : "PRI000099674653", "type" : "ncasProductInstance" } }, "servicedBy" : { "data" : { "id" : "SWOLT0000112", "type" : "dslam" } } } }, "included" : [ { "type" : "ncasProductInstance", "id" : "PRI000099674653", "attributes" : { "locationId" : "LOC000142502552", "accessSeekerId" : "ASI0000002209977", "priorityAssit" : "Yes", "enhancedSla" : "Enhanced - 8" } }, { "type" : "dslam", "id" : "SWOLT0000112", "attributes" : { "rack" : "1", "shelf" : "1", "slot" : "LT3", "port" : "7", "dslamType" : "LNEU", "ontIpAddress" : "12.34.56.78", "dpboProfile" : "ADP-VDSL_12dB" } } ] }',IntegrationType__c = 'ServiceInformation');
           String jsonPayload = '{ "data" : { "type" : "avc-d", "id" : "AVC000000773235", "attributes" : { "elementManager" : "SAM", "bandwidthProfile" : "D25_U5-10_Mbps_TC4_P, D5_U5_Mbps_TC2_C", "avcType" : "1:1", "avcResourceId" : "ALK000000000006", "primaryAccessTechnology" : "Fibre To The Building", "poiName" : "2MAI - MAITLAND", "avcConfig" : { "minLay1UpBitRate" : "5100", "minLay1DownBitRate" : "25900", "insertDslLineRate" : "True", "icsServiceId" : "ILK000000414065" }, "aas" : { "aasName" : "SWAAS0000102", "svlan" : "3000", "ipAddress" : "1.1.1.1", "primaryPort" : "1/1/1", "secondaryPort" : "1/1/2", "connectedtoPort" : "1/1/3" }, "cvc" : { "cvcId" : "CVC000000190598", "cvcResourceId" : "CLK000000000006", "tc1BandwidthProfile" : "D200_U200_Mbps_TC1_C", "tc2BandwidthProfile" : "D200_U200_Mbps_TC2_C", "tc4BandwidthProfile" : "D2000_U2000_Mbps_TC4_C", "mcBandwidthProfile" : "D2000_U2000_Mbps_TC4_C" }, "uniDsl" : { "uniDslId" : "CPI300000418831", "uniDslVlanMode" : "Tagged", "dslStabilityProfile" : "Standard", "potsInterconnect" : 245456780, "uniDslMode" : "VDSL2", "exchangePairConnection" : "No", "csll" : { "id" : "PRI000004401447", "type" : "USL", "productStatus" : "Terminated" } }, "nniNodes" : [ { "nniId" : "SWEAS0000105", "nniType" : "i-nni", "nniCvlanId" : "1", "nniSvlanId" : "1", "nniLagId" : "102", "nniIpAddress" : "1.1.1.1" }, { "nniId" : "SWEAS0000106", "nniType" : "i-nni", "nniCvlanId" : "1", "nniSvlanId" : "1", "nniLagId" : "103", "nniIpAddress" : "2.2.2.2" }, { "nniId" : "SWEAS0000107", "nniType" : "e-nni", "nniCvlanId" : "1", "nniSvlanId" : "1", "nniLagId" : "104", "nniIpAddress" : "3.3.3.3" }, { "nniId" : "SWEAS0000108", "nniType" : "e-nni", "nniCvlanId" : "1", "nniSvlanId" : "1", "nniLagId" : "105", "nniIpAddress" : "4.4.4.4" } ] }, "relationships" : { "associatedWith" : { "data" : { "id" : "PRI000099674653", "type" : "ncasProductInstance" } }, "servicedBy" : { "data" : { "id" : "SWOLT0000112", "type" : "dslam" } } } }, "included" : [ { "type" : "ncasProductInstance", "id" : "PRI000099674653", "attributes" : { "locationId" : "LOC000142502552", "accessSeekerId" : "ASI0000002209977", "priorityAssit" : "Yes", "enhancedSla" : "Enhanced - 8" } }, { "type" : "dslam", "id" : "SWOLT0000112", "attributes" : { "rack" : "1", "shelf" : "1", "slot" : "LT3", "port" : "7", "dslamType" : "LNEU", "ontIpAddress" : "12.34.56.78", "dpboProfile" : "ADP-VDSL_12dB" } } ] }';
           NBNIntegrationTranslation__c instTrans = new NBNIntegrationTranslation__c();
                            String intkey = 'BandwidthProfile';
                            integrationWrapper.RootObject RtServiceObject = (integrationWrapper.RootObject)JSON.deserialize(jsonPayload,integrationWrapper.RootObject.Class);
                            NBNIntegrationTranslation__c intTrans = CustomSettingsUtility.getIntTrans(instTrans,String.valueOf(RtServiceObject.data.attributes.bandwidthProfile),intkey);
       }catch(exception ex){}
      }
        
}