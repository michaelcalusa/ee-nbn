/*------------------------------------------------------------
    Author:        Kashyap Murthy
    Company:       Contractor
    Description:   Utility Class for getting values from Custom Settings.
    Returns:       The record id for the account
    History
    <Date>      <Authors Name>     <Brief Description of Change> 
    ------------------------------------------------------------*/
public class CustomSettingsUtility {
    
    /*
        Method takes a input type of Integration Type and gets a List of map of field names.
    */
    public static Map<String, String> getFieldNames (List<String> fieldNamesList){
        Map<String, String> variableNameFieldNameMap = new Map<String, String>();
        for (String fieldName : fieldNamesList){         
            variableNameFieldNameMap.put(fieldName.substringBefore('='), fieldName.substringAfter('='));
        }
        return variableNameFieldNameMap; 
    }
    
    /*
        Method returns RemedyIncidentDetailsJSONtoSFFields__c map.
    */
    public static Map<String, RemedyIncidentDetailsJSONtoSFFields__c> getAll (){
        Map<String, RemedyIncidentDetailsJSONtoSFFields__c> remedy2SFDCFieldMap = RemedyIncidentDetailsJSONtoSFFields__c.getAll();
        return remedy2SFDCFieldMap; 
    }
    
    /*
        Method returns InboundPlatformEventMap__c map.
    */
    public static List<InboundPlatformEventMap__c> getAllInboundPlatformEventMap (String integrationType){
        List<InboundPlatformEventMap__c> inboundPlatformEventList = new List<InboundPlatformEventMap__c>();
        if(!String.isBlank(integrationType)){
            Map<String, InboundPlatformEventMap__c> indexFieldMap = InboundPlatformEventMap__c.getAll();
            for (String key : indexFieldMap.keySet()){
                InboundPlatformEventMap__c inboundPlatformEvent = indexFieldMap.get(key);
                if (inboundPlatformEvent.IntegrationType__c == integrationType){  inboundPlatformEventList.add(inboundPlatformEvent);    
                }
            }
        }
        
        return inboundPlatformEventList; 
    }
    
    /*
        Method returns RemedyIncidentDetailsJSONtoSFFields__c map.
    */
    public static List<RemedyIncidentDetailsJSONtoSFFields__c> getAllRemedyIncidentDetailsJSONtoSFFields (String parentJSONKey){
        List<RemedyIncidentDetailsJSONtoSFFields__c> inboundPlatformEventList = new List<RemedyIncidentDetailsJSONtoSFFields__c>(); 
        if(!String.isBlank(parentJSONKey)){
            Map<String, RemedyIncidentDetailsJSONtoSFFields__c> indexFieldMap = RemedyIncidentDetailsJSONtoSFFields__c.getAll();
            for (String key : indexFieldMap.keySet()){
                RemedyIncidentDetailsJSONtoSFFields__c inboundPlatformEvent = indexFieldMap.get(key);
                if (inboundPlatformEvent.Parent_JSON_Key__C == parentJSONKey){      inboundPlatformEventList.add(inboundPlatformEvent);    
                }
            }
         }
        return inboundPlatformEventList; 
    }
    
    public static Map<String, String> getAllRemedyIncidentDetailsJSONtoSFFieldsMap (String parentJSONKey){
        Map<String, String> inboundPlatformEventMap = new Map<String, String>();
        if(!String.isBlank(parentJSONKey)){
            Map<String, RemedyIncidentDetailsJSONtoSFFields__c> indexFieldMap = RemedyIncidentDetailsJSONtoSFFields__c.getAll();
            
            for (String key : indexFieldMap.keySet()){
                RemedyIncidentDetailsJSONtoSFFields__c inboundPlatformEvent = indexFieldMap.get(key);
                if (inboundPlatformEvent.Parent_JSON_Key__C == parentJSONKey){        inboundPlatformEventMap.put(inboundPlatformEvent.Parent_JSON_Key__C, inboundPlatformEvent.Salesforce_Field__c);    
                }
            }
        }  
        return inboundPlatformEventMap; 
    }
    
    
    
    /*
        Method returns only Salesforce Fields as Comma Separated String from the map.
    */
    public static String getSalesforceFields (String integrationType){
       List<String> salesforceFieldsList = new List<String>();
       if(!String.isBlank(integrationType)){
            Map<String, RemedyIncidentDetailsJSONtoSFFields__c> integrationType2SFDCFieldMap = RemedyIncidentDetailsJSONtoSFFields__c.getAll();
            for (String key : integrationType2SFDCFieldMap.keySet()){
                RemedyIncidentDetailsJSONtoSFFields__c record = integrationType2SFDCFieldMap.get(key);
                if (integrationType == record.IntegrationType__c){      
                    salesforceFieldsList.add(record.Salesforce_Field__c);    
                }
            }
        
       }
        return String.join(salesforceFieldsList, ',');
    }
    
    /*
        Method returns map of Salesforce Field and JSONKey 
    */
    public static Map<String, String> getSalesforceFieldsAsMap (String integrationType){
        Map<String, RemedyIncidentDetailsJSONtoSFFields__c> integrationType2SFDCFieldMap = RemedyIncidentDetailsJSONtoSFFields__c.getAll();        
        Map<String, String> salesforceFieldsAndJSONKeyMap = new Map<String, String>();
        if(!String.isBlank(integrationType)){
            for (String key : integrationType2SFDCFieldMap.keySet()){
                RemedyIncidentDetailsJSONtoSFFields__c record = integrationType2SFDCFieldMap.get(key);
                if (integrationType == record.IntegrationType__c){    salesforceFieldsAndJSONKeyMap.put(record.Salesforce_Field__c, record.JSONKey__c);   
                }
            }
        }
        return salesforceFieldsAndJSONKeyMap;
    }
    
    /*
        Method returns map of Salesforce Field and JSONKey 
    */
    public static String getObjectString (String integrationType){
        Map<String, RemedyIncidentDetailsJSONtoSFFields__c> integrationType2SFDCFieldMap = RemedyIncidentDetailsJSONtoSFFields__c.getAll();
        String sObjectStr = '';
        if(!String.isBlank(integrationType)){
            for (String key : integrationType2SFDCFieldMap.keySet()){
                RemedyIncidentDetailsJSONtoSFFields__c record = integrationType2SFDCFieldMap.get(key);
                if (integrationType == record.IntegrationType__c){    sObjectStr = record.Object__c;   
                }
            }
        }
        return sObjectStr;
    }
    
    /*
        Method returns List of LightningComponentConfigurations__c 
    */
    public static List<LightningComponentConfigurations__c> getLightningComponentConfiguration(String componentName,String sObjectName){
        Map<String, LightningComponentConfigurations__c> lightningComponentConfigurationsAllMap = LightningComponentConfigurations__c.getAll();        
        system.debug('componentName  '+componentName);
        system.debug('lightningComponentConfigurationsAllMap '+lightningComponentConfigurationsAllMap  );
        List<LightningComponentConfigurations__c> lstlightningComponentConfigurations = new List<LightningComponentConfigurations__c>();
        if(!(String.isBlank(componentName) && String.isBlank(sObjectName))){
            for(LightningComponentConfigurations__c custSet : lightningComponentConfigurationsAllMap.values()){
                system.debug('custSet  name  '+custSet );
                if (componentName == custSet.Component__c && sObjectName == custSet.Object_API_Name__c){      lstlightningComponentConfigurations.add(custSet);
                }
            } system.debug('return from custonUtility'+lstlightningComponentConfigurations);
        }
        return lstlightningComponentConfigurations;
    }
    
    /*
        Method returns List of LightningComponentConfigurations__c 
    */
    public static List<LightningComponentConfigurations__c> getLightningComponentConfigurationJSON(String componentName,String JSONName){
        Map<String, LightningComponentConfigurations__c> lightningComponentConfigurationsAllMap = LightningComponentConfigurations__c.getAll();        
        system.debug('componentName  '+componentName);
        system.debug('lightningComponentConfigurationsAllMap '+lightningComponentConfigurationsAllMap  );
        List<LightningComponentConfigurations__c> lstlightningComponentConfigurations = new List<LightningComponentConfigurations__c>();
        if(!(String.isBlank(componentName) && String.isBlank(JSONName))){
            for(LightningComponentConfigurations__c custSet : lightningComponentConfigurationsAllMap.values()){
                system.debug('custSet  name  '+custSet );
                if (componentName == custSet.Component__c && JSONName == custSet.Object_API_Name__c){   lstlightningComponentConfigurations.add(custSet);
                }
            } system.debug('return from custonUtility'+lstlightningComponentConfigurations);
        }
        return lstlightningComponentConfigurations;
    }
    
    
    /*
        Method returns List of LightningComponentConfigurations__c 
    */
    public static List<LightningComponentConfigurations__c> getLightningComponentConfiguration(String componentName){
        Map<String, LightningComponentConfigurations__c> lightningComponentConfigurationsAllMap = LightningComponentConfigurations__c.getAll();        
        system.debug('componentName  '+componentName);
        //system.debug('lightningComponentConfigurationsAllMap '+lightningComponentConfigurationsAllMap  );
        List<LightningComponentConfigurations__c> lstlightningComponentConfigurations = new List<LightningComponentConfigurations__c>();
        if(!String.isBlank(componentName)){
            for(LightningComponentConfigurations__c custSet : lightningComponentConfigurationsAllMap.values()){
                //system.debug('custSet  name  '+custSet );
                if (componentName == custSet.Component__c){ lstlightningComponentConfigurations.add(custSet);
                }
            }
        }
        system.debug('return from custonUtility'+lstlightningComponentConfigurations);
        return lstlightningComponentConfigurations;
    }
    
    public static List<LightningComponentConfigurations__c> getObjectConfig(List<LightningComponentConfigurations__c> lstLightningComponentConfig,String UserProfile,String userRole){
        List<LightningComponentConfigurations__c> lstLightningComponentConfigbyUser = new List<LightningComponentConfigurations__c>();
       
            for(LightningComponentConfigurations__c custSet : lstLightningComponentConfig){
                if(custSet.Role_or_Profile_Based__c == 'Profile' && custSet.Role_or_Profile_Name__c == UserProfile){   lstLightningComponentConfigbyUser.add(custSet);
                }
                if(custSet.Role_or_Profile_Based__c == 'Role' && custSet.Role_or_Profile_Name__c == userRole){    lstLightningComponentConfigbyUser.add(custSet);
                }
            }
        
        return lstLightningComponentConfigbyUser;
    }
    
    /*
        Method returns map of Salesforce Field and JSONKey 
    */
    public static Map<String, Boolean> getDateTimeSaleforceFields(String integrationType){
        
        Map<String, RemedyIncidentDetailsJSONtoSFFields__c> integrationType2SFDCFieldMap = RemedyIncidentDetailsJSONtoSFFields__c.getAll();
        Map<String, Boolean> sfdcFieldDateFieldMap = new Map<String, Boolean>();
        
        String sObjectStr = '';
        if(!String.isBlank(integrationType)){
            for (String key : integrationType2SFDCFieldMap.keySet()){
                RemedyIncidentDetailsJSONtoSFFields__c record = integrationType2SFDCFieldMap.get(key);
                if (record.Is_Datetime_Field__c){  sfdcFieldDateFieldMap.put(record.Salesforce_Field__c, record.Is_Datetime_Field__c);    
                }
            }
        }
        return sfdcFieldDateFieldMap;
    }
    
                //Class to return values to display to end user based on values returned on integration and a mapping in custom setting
      public static String getDisplayVal(String strValuetoDisplay, String intValue, String intkey){
          Map<String,String> mapvals = new Map<String,String>();
          List<String> matchingVal = new List<String>();
          Map<String, NBNIntegrationTranslation__c> IntTrans = NBNIntegrationTranslation__c.getAll();
          if(IntTrans!=null){
              for(NBNIntegrationTranslation__c dwt : IntTrans.values()){    
                  if(dwt != null && dwt.IntKey__c == intkey){      mapvals.put(dwt.IntegrationValueReturned__c,dwt.User_Display_Value__c);
                 }   
              }
              for(String valmap:mapvals.Keyset()){
                if(intValue==valmap){
                    strValuetoDisplay = mapvals.get(valmap);   matchingVal.add(strValuetoDisplay);
                }
                System.debug('@@Size of map list;' + matchingVal.size());
                if(matchingVal.size()==0){            strValuetoDisplay = intValue;   
                }
              }
          }
        return strValuetoDisplay;
    }
    
       //class to return once custom setting instance
      public static NBNIntegrationTranslation__c getIntTrans(NBNIntegrationTranslation__c instTrans, String intValue, String intkey){
          Map<String,String> mapvals = new Map<String,String>();
          List<String> matchingVal = new List<String>();
          Map<String, NBNIntegrationTranslation__c> IntTrans = NBNIntegrationTranslation__c.getAll();
          if(IntTrans!=null){
              for(NBNIntegrationTranslation__c dwt : IntTrans.values()){    
                  if(dwt != null && dwt.IntKey__c == intkey && !String.isBlank(dwt.IntegrationValueReturned__c) && dwt.IntegrationValueReturned__c.Equals(intValue)){
                     String instTransName = dwt.Name;instTrans = NBNIntegrationTranslation__c.getInstance(instTransName);
                 }    
              }
          }
        return instTrans;
    }
    
      //Class to return values to show the timezone that a given state belongs to.The TIMEZONESIDKKEY is fetched from custom settings
      public static String getTimeZoneStringVal(String strState){
          String timeZoneSIDkey = '';
          String strTimeZone = '';
          if(!String.isBlank(strState)){
              NBNTimeZone__c csTz = NBNTimeZone__c.getInstance(strState);
              if(csTz!=null){            timeZoneSIDkey = csTz.TimeZone__c;
              }
              TimeZone tz = TimeZone.getTimeZone(timeZoneSIDkey);
              strTimeZone = tz.getDisplayName();
          }
        return strTimeZone;
    }
     //Method to return EngagementInboundIntegrationcustom settings
    public static List<EngagementInboundIntegration__c> getAllEngagementInboundIntegrationSettings(String integrationType)
    {
        Map<String,EngagementInboundIntegration__c> indexFieldMap = EngagementInboundIntegration__c.getAll();
        List<EngagementInboundIntegration__c> lstEngagementInboundIntegrationSettings = new List<EngagementInboundIntegration__c>();
        if(!String.isBlank(integrationType)){
            for (String key : indexFieldMap.keySet())
            {
                EngagementInboundIntegration__c engagementIntegrationSetting = indexFieldMap.get(key);
                if (engagementIntegrationSetting.IntegrationType__c.Equals(integrationType))
                {  
                    lstEngagementInboundIntegrationSettings.add(engagementIntegrationSetting);    
                }
            }
        }
        system.debug('>>>>Custom Setting Value'+lstEngagementInboundIntegrationSettings);
        return lstEngagementInboundIntegrationSettings;     
    }

}