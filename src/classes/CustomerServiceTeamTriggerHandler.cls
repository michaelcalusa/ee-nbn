/*------------------------------------------------------------------------
Author:        Andrew Zhang
Company:       NBNco
Description:   Manage trigger actions from the Customer_Service_Team__c object 
               Responsible for:
               1. After Insert: Add a Public Group in same name, Add a Queue in same name, add Public Group to Queue
               2. After Delete: Remove Public Group and Queue in same name
               3. After Update: Rename Public Group and Queue
Test Class:    CustomerServiceTeamTriggerHandler_Test
History
<Date>            <Authors Name>    <Brief Description of Change> 
--------------------------------------------------------------------------*/

public without sharing class CustomerServiceTeamTriggerHandler{
     
    public static void afterInsertRules() {        
        List<String> teamNames = new List<String>();
        List<String> teamEmails = new List<String>();
        
        for (Id id : Trigger.newMap.keySet()){     
            Customer_Service_Team__c team = (Customer_Service_Team__c)Trigger.newMap.get(id);            
            teamNames.add(team.Name);  
            teamEmails.add(team.Email__c);           
        }        
        
        addGroupsAndQueues(teamNames, teamEmails);
    } 
    
    public static void afterDeleteRules() {        
        Set<String> teamNameSet = new Set<String>();
        
        for (Id id : Trigger.oldMap.keySet()){     
            Customer_Service_Team__c team = (Customer_Service_Team__c)Trigger.oldMap.get(id);            
            teamNameSet.add(team.Name);            
        }        
        
        removeGroupsAndQueues(teamNameSet);                  
    } 

    public static void afterUpdateRules() {   
        //change Name     
        Map<String, String> oldNameNewNameMap = new Map<String, String>();
        Map<String, String> oldNameNewEmailMap = new Map<String, String>();
                
        for (Id id : Trigger.newMap.keySet()){     
            Customer_Service_Team__c teamOld = (Customer_Service_Team__c)Trigger.oldMap.get(id); 
            Customer_Service_Team__c teamNew = (Customer_Service_Team__c)Trigger.newMap.get(id); 
            
            if (teamNew.Name != teamOld.Name || teamNew.Email__c != teamOld.Email__c){
                oldNameNewNameMap.put(teamOld.Name, teamNew.Name);
                oldNameNewEmailMap.put(teamOld.Name, teamNew.Email__c);
            }

        }                
        updateGroupsAndQueues(oldNameNewNameMap, oldNameNewEmailMap);           
    }     

    
    @future
    public static void addGroupsAndQueues(List<String> names, List<String> emails){
        
        //existing groups
        Map<String, Group> groupMap = new Map<String,Group>();
        for (Group g : [SELECT Id, Name FROM Group WHERE TYpe = 'Regular']){
            groupMap.put(g.Name, g);
        }
        
        //Insert Groups
        Map<String, Group> groupMapToInsert = new Map<String,Group>();
        for (String groupName : names){ 
            if (!groupMap.containsKey(groupName))
                groupMapToInsert.put(groupName, new Group(Type = 'Regular', Name = groupName));
        }
        insert groupMapToInsert.values();

        //existing queues
        Map<String, Group> queueMap = new Map<String,Group>();
        for (Group g : [SELECT Id, Name FROM Group WHERE TYpe = 'Queue']){
            queueMap.put(g.Name, g);
        }
        
        //Insert Queues
        List<Group> queuesToInsert = new List<Group>();
        for (Integer i=0; i < names.size(); i++){ 
            if (!queueMap.containsKey(names[i]))
                queuesToInsert.add(new Group(Type = 'Queue', Name = names[i], DoesSendEmailToMembers = true, Email = emails[i]));
        }
        insert queuesToInsert;
        
        //insert QueueSobject
        List<QueueSobject> queueSupportedObjects = new List<QueueSobject>();
        for (Group q : queuesToInsert){ 
            queueSupportedObjects.add(new QueueSobject(QueueId = q.Id, SobjectType = 'Case'));
            queueSupportedObjects.add(new QueueSobject(QueueId = q.Id, SobjectType = 'Case_Assignment_Matrix__c'));
            queueSupportedObjects.add(new QueueSobject(QueueId = q.Id, SobjectType = 'Case_Matrix_Detail__c'));
        }        
        insert queueSupportedObjects;
        
        //add group To queue
        List<GroupMember> members = new List<GroupMember>();
        for (Group q : queuesToInsert){ 
            Group g = groupMapToInsert.get(q.Name);
            
            if (g == null)
                g = groupMap.get(q.Name);
                
            if (g != null)
                members.add(new GroupMember(GroupId = q.Id, UserOrGroupId = g.Id));
        }
        insert members;
    }

    @future
    public static void removeGroupsAndQueues(Set<String> names){    
        delete [SELECT Id FROM Group WHERE Name IN :names AND Type IN ('Regular','Queue')];  
    }
    
    @future
    public static void updateGroupsAndQueues(Map<String, String> oldNameNewNameMap, Map<String, String> oldNameNewEmailMap){
        
        List<Group> groupsQueues = [SELECT Id, Name, Type FROM Group WHERE Name IN :oldNameNewNameMap.keySet() AND Type IN ('Regular','Queue')];
        
        for (Group g : groupsQueues ){ 
            if (oldNameNewEmailMap.containsKey(g.Name) && g.Type == 'Queue') 
                g.Email = oldNameNewEmailMap.get(g.Name);
                
            if (oldNameNewNameMap.containsKey(g.Name))
                g.Name = oldNameNewNameMap.get(g.Name);
        }
        update groupsQueues;
    }
        
}