/*------------------------------------------------------------------------
Author:        Andrew Zhang
Company:       NBNco
Description:   Test class for CustomerServiceTeamTriggerHandler
History
<Date>      <Authors Name>     <Brief Description of Change>
----------------------------------------------------------------------------*/

@isTest
private class CustomerServiceTeamTriggerHandler_Test {

    static testMethod void testInsertUpdate(){
        
        test.startTest();   
        
        //test insert
        Customer_Service_Team__c team = new Customer_Service_Team__c(
            Name = 'Test SME Team',
            Email__c = 'testsme@example.com',
            Type__c= 'SME'
        );
        insert team;

        //test update
        team.Name = 'Test SME Team 2';
        update team;
        
        test.stopTest();   
        
        List<Group> groupsQueues = [SELECT Id, Name FROM Group WHERE Name = :team.Name  AND Type IN ('Regular','Queue')];
        System.assertEquals(2, groupsQueues.size());             
        
    }


    static testMethod void testDelete(){
        
        test.startTest();   
        
        //test insert
        Customer_Service_Team__c team = new Customer_Service_Team__c(
            Name = 'Test SME Team',
            Email__c = 'testsme@example.com',
            Type__c= 'SME'
        );
        insert team;
        
        //test delete
        delete team;
        
        test.stopTest();   
        
        List<Group> groupsQueues = [SELECT Id, Name FROM Group WHERE Name = :team.Name  AND Type IN ('Regular','Queue')];
        System.assertEquals(0, groupsQueues.size());             
        
    }
}