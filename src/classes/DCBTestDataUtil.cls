/************************************************************************************************************
Version                 : 1.0 
Created Date            : 06-04-2018
Description/Function    : This class contains base methods for creating test data. Extend test record creation using init methods.
=============================================================================================================
Modification Log        :
-------------------------------------------------------------------------------------------------------------
* Developer                   Date                   Description
* ------------------------------------------------------------------------- ---------------------------------               
* Swetha Potla                06-04-2018             Created the Class
* Swetha Potla                17-04-2018            Added methods for creating customer approvals test data.
**************************************************************************************************************/

public class DCBTestDataUtil {    
    
    public static Account getNewAccount(){ 
        // Create Account record
        Account acc=initAccount();
        insert acc;
        return acc;
    }
    
    public static Account initAccount(){ 
        // Init Account record
        Account accRec = new Account(name = 'Test Account',on_demand_id__c = 'Account1');
        return accRec;
    }
    
    public static Contact getNewContact(){ 
        // Create Contact record
        Contact con=initContact();
        insert con;
        return con;
    }
    
    public static Contact initContact(){        
        // Init Contact record
        Contact contactRec = new Contact(on_demand_id_P2P__c = 'Contact1',On_Demand_Id__c  = 'Contact1',LastName = 'Test Contact',Email = 'TestEmail@nbnco.com.au');
        return contactRec;       
    }
    
    public static Development__c getNewDevelopment(Id accountId, Id contactId){
        //Create Development record
        Development__c devObj = initDevelopment(accountId,contactId);
        //devObj.Sales_Support_Assessment__c = 'Completed';
        insert devObj;
        return devObj;
    }
    
    public static Development__c initDevelopment(Id accountId, Id contactId){
        // Init Development record
        Development__c Development1 = new Development__c ();
        Development1.Name = 'Test';
        Development1.Development_ID__c = 'Development1';
        Development1.Account__c = accountId;
        Development1.Primary_Contact__c = contactId;
        Development1.Suburb__c = 'North Sydney';
        return Development1;        
    }
    
    public static Stage_Application__c getNewStageApplication(Id devId){
        //Create Stage Application record
        Stage_Application__c stageApp = initStageApplication(devId);
        insert stageApp;
        return stageApp;
    }
    
    public static Stage_Application__c initStageApplication(Id devObjId){
         // Init Stage Application record
        Stage_Application__c StageApplication1 = new Stage_Application__c ();
        StageApplication1.Request_ID__c = 'Test';
        StageApplication1.Dwelling_Type__c = 'SDU Only';
        StageApplication1.Development__c = devObjId;
        StageApplication1.Active_Status__c = 'Active';
        StageApplication1.name = 'Test';
        //StageApplication1.Request_Id__c = 'StageApplicationInt1';
        return StageApplication1;
    }
    
    public static Opportunity getNewOpportunity(Id stageAppId){
        //Create opportutnity
        Opportunity opp= initOpportunity(stageAppId);
        insert opp;
        return opp;
    }
    
    public static Opportunity initOpportunity(Id stageAppId)
    {
        //init opportunity
        Opportunity opp = new Opportunity(
        Name = 'Test Opp', 
        CloseDate = System.today(),
        StageName = 'Invoicing',
        Stage_Application__c = stageAppId);
        
        return opp;
    }
    
     public static Invoice__c getNewInvoice(Id oppid){
         //Create invoice
        Invoice__c inv= initInvoice(oppId);
        insert inv;
        return inv;
    }
    
    public static Invoice__c initInvoice(Id oppid)
    {
        //Init invoice
        Invoice__c inv= new Invoice__c(
        status__c = 'New',
        EBS_Number__c= 'To Be issued',
        Transaction_Type__c='NewDevs Class 1-2',
        Unpaid_Balance__c=26000,
        opportunity__c = oppId);        
        return inv;
    }
    

    
    public static Task  getNewTask(){
        Task t = initTask();
        insert t; 
        return t;
    }  
    
    public static Task  initTask(){
        Task t = new Task(status='New');         
        return t;
    } 
    
    public static cscfga__Product_Basket__c getNewProdBasketData(Id devId)
        //Create product basket
    {
        cscfga__Product_Basket__c objBasket = initProdBasketData(devId);
        insert objBasket;
        return objBasket;
    }
 
    
    public static cscfga__Product_Basket__c initProdBasketData(Id devId)
    {
        //Init product basket
        cscfga__Product_Basket__c objBasket = new cscfga__Product_Basket__c(Name = 'Test basket');
       
        return objBasket;
    }
    
     public static cscfga__Configuration_Offer__c getNewConfigOffer(){
         //Create Configuration Offer
        cscfga__Configuration_Offer__c configOffer = initConfigOffer();
        insert configOffer;
        return configOffer;
    }
    
    public static cscfga__Configuration_Offer__c initConfigOffer(){
        //init configuration offer
        cscfga__Configuration_Offer__c configOffer = new cscfga__Configuration_Offer__c();
        configOffer.Name = 'Backhaul Charge';
        configOffer.cscfga__Template__c = false;
        configOffer.cscfga__Active__c = true;
        return configOffer;
     }

    
    public static cscfga__Product_Configuration__c getNewProdConfig(Id ProdBasket, Id configOffer){
        //Create Product Configuration
       cscfga__Product_Configuration__c  testProdConfig = initProdConfig(ProdBasket, configOffer);
       insert testProdConfig;
       return testProdConfig ;
    }    
    
    public static cscfga__Product_Configuration__c initProdConfig(Id  prodDefId,Id configOffer){
        //init product configuration
       cscfga__Product_Configuration__c  testProdConfig = new cscfga__Product_Configuration__c();
       testProdConfig.cscfga__Configuration_Offer__c = configOffer;
       testProdConfig.cscfga__Product_Definition__c = prodDefId;
       testProdConfig.Name = 'New Development ';        
       return testProdConfig ;
    }
    
       /*method to create a test product category*/
    public static cscfga__Product_Category__c createProdCateg(){
        //init Product category
        cscfga__Product_Category__c testProdCateg = new cscfga__Product_Category__c(); 
        testProdCateg.Name = 'New Developments';
        testProdCateg.cscfga__Active__c = true;       
        return testProdCateg ;        
    }
    
    /*method to create a test producyt Definition by Name*/
    public static cscfga__Product_Definition__c createProDefByName(String Name){ 
        cscfga__Product_Definition__c proDefn = createProDef();
        proDefn.Name = Name;
        proDefn.csordtelcoa__Product_Type__c = 'Delivery';
        insert proDefn;
        return proDefn;
    }

    
    /*method to create a test product Definition*/
    public static cscfga__Product_Definition__c createProDef(){
        //Init Product Definition
        cscfga__Product_Definition__c testProDef = new cscfga__Product_Definition__c();
        testProDef.cscfga__Product_Category__c= createProdCateg().Id;
        testProDef.cscfga__Description__c= 'testDescription';
        testProDef.csordtelcoa__Product_Type__c = 'Delivery';
        testProDef.cscfga__Active__c = true;
        return testProDef ;        
    }
    
    /*method to create a test Attribute Definition by Name*/
    public static cscfga__Attribute_Definition__c createAttrDefByOtherParams(String Name,Id proDefId){ 
        cscfga__Attribute_Definition__c attr = createAttrDef();
        attr.Name = Name;
        attr.cscfga__Product_Definition__c = proDefId;
        insert attr;
        return attr;
    }

    /*method to create a test Attribute Definition by Name , and type*/
    public static cscfga__Attribute_Definition__c createAttrDefByOtherParams(String Name, Id proDefId, String attType, String dataType){ 
        cscfga__Attribute_Definition__c attr = createAttrDef();
        attr.Name = Name;
        attr.cscfga__Product_Definition__c = proDefId;
        attr.cscfga__Type__c = attType;
        attr.cscfga__Data_Type__c = dataType;
        insert attr;
        return attr;
    }

    /*method to create a test Attribute Definition*/
    public static cscfga__Attribute_Definition__c createAttrDef(){ 
        //Init Attribute Defnition
        cscfga__Attribute_Definition__c testAttrDef = new cscfga__Attribute_Definition__c();
        testAttrDef.Name= 'Option';
        testAttrDef.cscfga__Is_Line_Item__c = false;
        testAttrDef.cscfga__Data_Type__c = 'Decimal';
        testAttrDef.cscfga__Enable_null_option__c = true;
        testAttrDef.csordtelcoa__Calculate_Delta__c = true;
        return testAttrDef ;        
    }
    
     /*method to create a test Attribute by Name*/
    public static cscfga__Attribute__c createAttrByName(Id proConfigId,Id proDefId,String Name, String Price){ 
        cscfga__Attribute__c attr = initAttribute(proConfigId, proDefId);
        attr.Name = 'FibreTV';
        attr.cscfga__Price__c = decimal.valueOf(price);   
        insert attr;
        return attr;
    }


    public static cscfga__Attribute__c getNewAttribute(Id prodConfigId, Id atrrDefId){
        // Create Attribute
         cscfga__Attribute__c attr = initAttribute(prodConfigId, atrrDefId);
         insert attr;
         return attr;
    }
    
    public static cscfga__Attribute__c initAttribute(Id prodConfigId, Id atrrDefId){
        //Init attribute
        cscfga__Attribute__c testAttr = new cscfga__Attribute__c();
        testAttr.cscfga__Product_Configuration__c= prodConfigId;         
        testAttr.cscfga__Attribute_Definition__c = atrrDefId;
        testAttr.Name = 'Price';
        testAttr.cscfga__Line_Item_Description__c = 'New Development '; // same as product configuration name
        testAttr.cscfga__is_active__c = true;
        testAttr.cscfga__Is_Line_Item__c = true;
        testAttr.cscfga__Discount_Type__c = 'amount';
        testAttr.cscfga__Price__c = decimal.valueOf('1322.50');
        return testAttr;
    }  
    
    public static CSCAP__Customer_Approval__c getNewCustomerApproval(Id oppId){
        CSCAP__Customer_Approval__c custAppr = initCustomerApproval(oppId);
        insert custAppr;
        return custAppr;
    }
    
    public static CSCAP__Customer_Approval__c initCustomerApproval(Id oppId){
        CSCAP__Customer_Approval__c custAppr = new CSCAP__Customer_Approval__c();
        custAppr.CSCAP__Opportunity__c = oppId;
        custAppr.CSCAP__Status__c = 'Active';
        custAppr.CSCAP__Expiry_Date__c = System.Today().addDays(10);
        return custAppr;
    }

        
   
}