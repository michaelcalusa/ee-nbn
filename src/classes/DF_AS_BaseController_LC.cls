/*------------------------------------------------------------  
  Author:        Kai-Fan Hsieh
  Description:   A global utility class for Assurance (Enterprise Ethernet) related components and classes
  Test Class:    DF_AS_BaseController_Test
  History
  <Date>      <Authors Name>     <Brief Description of Change>
  ------------------------------------------------------------*/

public class DF_AS_BaseController_LC {

	private static final String INCIDENT_TYPE_SERVICE_INCIDENT = 'Service Incident';
	private static final String INCIDENT_TYPE_NETWORK_INCIDENT = 'Network Incident';
	private static final String INCIDENT_TYPE_SERVICE_ALERT = 'Service Alert';
	private static final String INCIDENT_TYPE_CHANGE_REQUEST = 'Change Request';

	private static final String INCIDENT_CLOSED_STATUS_SERVICE_INCIDENT = '\'Rejected\',\'Resolved\',\'Closed\'';
	private static final String INCIDENT_CLOSED_STATUS_NETWORK_INCIDENT = '\'Incident Closed\',\'Incident Resolved\'';
	private static final String INCIDENT_CLOSED_STATUS_SERVICE_ALERT = '\'Incident Closed\',\'Incident Resolved\'';
	private static final String INCIDENT_CLOSED_STATUS_CHANGE_REQUEST = '\'Cancelled\',\'Complete\'';

	@AuraEnabled
	public static DF_SvcCacheSearchResults searchServiceCache(String searchString) {
		final String END_POINT;
		final Integer TIMEOUT_LIMIT;
		final String NAMED_CREDENTIAL;

		Http http = new Http();
		HttpRequest req;
		HTTPResponse res;
		String requestStr;
		String responseStr;

		String referenceId = '';
		String referenceInfo = '';
		String logMessage = '';
		String payload = '';

		DF_SvcCacheSearchResults response = new DF_SvcCacheSearchResults();

		try {
			if (searchString != null
			    && (searchString.toLowerCase().startsWithIgnoreCase('bpi')
			        || searchString.toLowerCase().startsWithIgnoreCase('ovc'))) {
				DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
				                                             Timeout__c
				                                             FROM DF_Integration_Setting__mdt
				                                             WHERE DeveloperName = :DF_SvcCacheServiceUtils.INTEGRATION_SETTING_NAME];

				NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
				TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
				referenceId = searchString;
				payload = 'Search String ' + searchString;

				system.debug('@@@ NAMED_CREDENTIAL: ' + NAMED_CREDENTIAL);
				system.debug('@@@ TIMEOUT_LIMIT: ' + TIMEOUT_LIMIT);

				END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL + '/' + searchString;
				system.debug('@@@ END_POINT: ' + END_POINT);

				// Generate request 
				req = DF_AS_GlobalUtility.getHttpRequest(END_POINT, TIMEOUT_LIMIT);
				system.debug('@@@ req: ' + req);

				// Send and get response               
				res = http.send(req);

				system.debug('@@@ Response Status Code: ' + res.getStatusCode());
				system.debug('@@@ Response Body: ' + res.getBody());

				if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {
					responseStr = res.getBody();

					if (String.isNotEmpty(responseStr)) {
						// Get Response Body - Status
						response = DF_SvcCacheServiceUtils.getServiceCacheResponse(responseStr);

					} else {
						throw new CustomException(DF_SvcCacheServiceUtils.ERR_RESP_BODY_MISSING);
					}
				} else if (res.getStatusCode() == DF_Constants.HTTP_STATUS_404) {
					system.debug('@@@ Handling 404 response');
					responseStr = res.getBody();
					response = DF_SvcCacheServiceUtils.processNotFound(responseStr);
					logMessage = 'Response 404 from Service Cache';
					GlobalUtility.logMessage(DF_Constants.ERROR, DF_AS_BaseController_LC.class.getName(), 'searchServiceCache', referenceId, referenceInfo, logMessage, payload, null, 0);
				}
				else {
					system.debug('@@@ send failure');
					response = DF_SvcCacheServiceUtils.processError();
					logMessage = 'Unable to process reponse ' + res.getStatusCode();
					GlobalUtility.logMessage(DF_Constants.ERROR, DF_AS_BaseController_LC.class.getName(), 'searchServiceCache', referenceId, referenceInfo, logMessage, payload, null, 0);
					throw new AuraHandledException('Service Cache is down');
				}
			}
		} catch(Exception e) {
			logMessage = 'Exception occured while accessing Service Cache';
			GlobalUtility.logMessage(DF_Constants.ERROR, DF_AS_BaseController_LC.class.getName(), 'searchServiceCache', referenceId, referenceInfo, logMessage, payload, e, 0);
			throw new AuraHandledException('Service Cache is down');
		}
		if (searchString != null && searchString.startsWithIgnoreCase('OVC') && res.getStatusCode() == 200) {
			system.debug('!!! get uniId from OVC response: ' + response.uniId);
			response = searchServiceCache(response.uniId);
		}
		system.debug('@@@ response: ' + response);

		return response;
	}

	@AuraEnabled
	public static Map<String, List<Incident_Management__c>> searchOpenIncidents(String bpi) {

		Map<String, List<Incident_Management__c>> searchResponse = new Map<String, List<Incident_Management__c>> ();

		List<Incident_Management__c> serviceIncidents = searchOpenServiceIncidents(bpi, INCIDENT_TYPE_SERVICE_INCIDENT, INCIDENT_CLOSED_STATUS_SERVICE_INCIDENT);
		List<Incident_Management__c> networkIncidents = searchOpenServiceIncidents(bpi, INCIDENT_TYPE_NETWORK_INCIDENT, INCIDENT_CLOSED_STATUS_NETWORK_INCIDENT);
		List<Incident_Management__c> serviceAlerts = searchOpenServiceIncidents(bpi, INCIDENT_TYPE_SERVICE_ALERT, INCIDENT_CLOSED_STATUS_SERVICE_ALERT);
		List<Incident_Management__c> changeRequests = searchOpenServiceIncidents(bpi, INCIDENT_TYPE_CHANGE_REQUEST, INCIDENT_CLOSED_STATUS_CHANGE_REQUEST);

		if (serviceIncidents != null && serviceIncidents.size() > 0)
		searchResponse.put(INCIDENT_TYPE_SERVICE_INCIDENT, serviceIncidents);

		if (networkIncidents != null && networkIncidents.size() > 0)
		searchResponse.put(INCIDENT_TYPE_NETWORK_INCIDENT, networkIncidents);

		if (serviceAlerts != null && serviceAlerts.size() > 0)
		searchResponse.put(INCIDENT_TYPE_SERVICE_ALERT, serviceAlerts);

		if (changeRequests != null && changeRequests.size() > 0)
		searchResponse.put(INCIDENT_TYPE_CHANGE_REQUEST, changeRequests);

		return searchResponse;
	}

	private static List<Incident_Management__c> searchOpenServiceIncidents(String bpi, String incidentType, String closedIncidentStatus) {

		List<Incident_Management__c> incidents = new List<Incident_Management__c> ();
		List<User> currentUser = [SELECT Account.Access_seeker_id__c, AccountId, ContactId FROM User where id = :UserInfo.getUserId()];
		String userAccountId = currentUser.get(0).AccountId;

		String searchQuery = '';

		if (incidentType.equals(INCIDENT_TYPE_SERVICE_INCIDENT)) {

			searchQuery = 'SELECT Id, PRI_ID__c, Incident_Identifier__c, Incident_Number__c, Incident_Type__c, RecordType.Name, Industry_Status__c FROM Incident_Management__c';

			//construct filters
			List<String> filters = new List<String> ();

			if (userAccountId != null)
			filters.add('DF_Order__r.Opportunity_Bundle__r.Account__c = \'' + userAccountId + '\'');

			if (bpi != null && bpi != '') {
				filters.add('PRI_ID__c = \'' + bpi + '\'');
			}

			if (closedIncidentStatus != null && closedIncidentStatus != '') {
				filters.add('Industry_Status__c NOT IN (' + closedIncidentStatus + ')');
			}

			if (incidentType != null && incidentType != '') {
				filters.add('RecordType.Name = \'' + incidentType + '\'');
			}

			filters.add('RecordTypeId != null');

			//apply filters
			if (filters.size() > 0) {
				searchQuery = searchQuery + ' WHERE';
				for (Integer f = 0; f<filters.size(); f++) {
					searchQuery = searchQuery + ' ' + filters.get(f);
					if (f != filters.size() - 1) {
						searchQuery = searchQuery + ' AND';
					}
				}
			}

			//apply ordering
			searchQuery = searchQuery + ' ORDER BY CreatedDate DESC';

		} else if (incidentType.equals(INCIDENT_TYPE_CHANGE_REQUEST) || incidentType.equals(INCIDENT_TYPE_NETWORK_INCIDENT) || incidentType.equals(INCIDENT_TYPE_SERVICE_ALERT)) {

			List<Incident_Management__c> searchResponse = new List<Incident_Management__c> ();
			List<EE_AS_Incident_Account_Relationship__c> filterResponse = new List<EE_AS_Incident_Account_Relationship__c> ();

			String filterQuery = 'SELECT Id, Related_Incident__c FROM EE_AS_Incident_Account_Relationship__c';
			String accessSeekerId = currentUser.get(0).Account.Access_seeker_id__c;
			//construct filters
			List<String> filters = new List<String> ();

			if (bpi != null && bpi != '') {
				filters.add('BPI_Id__c = \'' + bpi + '\'');
			}

			if (accessSeekerId != null && accessSeekerId != '') {
				filters.add('Access_Seeker_ID__c = \'' + accessSeekerId + '\'');
			}

			if (closedIncidentStatus != null && closedIncidentStatus != '') {
				filters.add('Related_Incident__r.Industry_Status__c NOT IN (' + closedIncidentStatus + ')');
			}

			if (incidentType != null && incidentType != '') {
				filters.add('Related_Incident__r.RecordType.Name = \'' + incidentType + '\'');
			}

			filters.add('Related_Incident__r.RecordTypeId != null');

			//apply filters
			if (filters.size()> 0) {
				filterQuery = filterQuery + ' WHERE';
				for (Integer f = 0; f<filters.size(); f++) {
					filterQuery = filterQuery + ' ' + filters.get(f);
					if (f != filters.size() - 1) {
						filterQuery = filterQuery + ' AND';
					}
				}
			}

			//apply limits
			//filterQuery = filterQuery + ' LIMIT ' + INCIDENT_RESULT_SIZE;

			System.debug('!@#$% Other Incident Filter Query : ' + filterQuery);

			//execute query
			filterResponse = Database.query(filterQuery);

			Set<String> filterByIncidentIds = new Set<String> ();
			if (filterResponse != null && !filterResponse.isEmpty()) {
				for (EE_AS_Incident_Account_Relationship__c incidentAccRel : filterResponse) {
					filterByIncidentIds.add((String) incidentAccRel.Related_Incident__c);
				}
			}
			System.debug(' **%%^^   filterByIncidentIds   ' + filterByIncidentIds);
			if (!filterByIncidentIds.isEmpty()) {
				searchQuery = 'SELECT Id, PRI_ID__c, Incident_Identifier__c, Incident_Number__c, Incident_Type__c, RecordType.Name, Industry_Status__c  FROM Incident_Management__c WHERE Id IN :filterByIncidentIds ORDER BY CreatedDate DESC';
			}
		}

		System.debug('@@@ searchQuery -->' + searchQuery);
		//execute query
		if (String.isNotBlank(searchQuery)) {
			incidents = Database.query(searchQuery);
			System.debug('@@@ Incidents found --> ' + incidents);
		}

		return incidents;
	}

	@AuraEnabled
	public static String getCurrentUserAccountName() {
		User user = [SELECT Account.Name FROM User where id = :UserInfo.getUserId() LIMIT 1];
		System.debug('@@@@user.Account.Name -->' + user.Account.Name);
		return user.Account.Name;
	}

	@AuraEnabled
	public static String getCurrentUserAccountAccessSeekerId() {
		User user = [SELECT Account.Access_seeker_id__c FROM User where id = :UserInfo.getUserId() LIMIT 1];
		System.debug('@@@@Access_seeker_id__c -->' + user.Account.Access_seeker_id__c);
		return user.Account.Access_seeker_id__c;
	}

	@AuraEnabled
	public static List<EE_AS_Incident_Account_Relationship__c> getImpactedServicesByIncidentId(String incNumber) {

		User user = [SELECT Account.Access_seeker_id__c FROM User where id = :UserInfo.getUserId() LIMIT 1];
		String accessSeekerId = user.Account.Access_seeker_id__c;
		List<EE_AS_Incident_Account_Relationship__c> impactedServices = [select Access_Seeker_ID__c, BPI_Id__c, NNI_Id__c, OVC_Id__c from EE_AS_Incident_Account_Relationship__c where Access_Seeker_ID__c = :accessSeekerId AND Related_Incident__r.Incident_Number__c = :incNumber];

		return impactedServices;
	}

	@AuraEnabled
	public static String getASPickListValuesIntoList(String objectApiName, String fieldApiName) {

		String[] pickListValuesList = DF_AS_GlobalUtility.getPicklistValues(objectApiName, fieldApiName);

		return JSON.serialize(pickListValuesList);
	}

	@AuraEnabled
	public static void watchMe(String incidentId) {

		if (String.isNotBlank(incidentId)) {
			EE_AS_Incident_Watch__c watch = new EE_AS_Incident_Watch__c(Incident__c = incidentId, User__c = UserInfo.getUserId());
			insert watch;
		}
	}


	@AuraEnabled
	public static Boolean isWatching(String incidentId) {
		Boolean iswatching = false;
		List<EE_AS_Incident_Watch__c> watchList = [select ID from EE_AS_Incident_Watch__c where Incident__c = :incidentId AND User__c = :UserInfo.getUserId()];

		if (watchList != null && watchList.size()> 0) {
			iswatching = true;
		}

		return iswatching;
	}

	@AuraEnabled
	public static Id getRecordTypeId(String objectApiName, String recordTypeDeveloperName) {
		Schema.SObjectType objectType = Schema.getGlobalDescribe().get(objectApiName);
		Id recordTypeId = objectType.getDescribe().getRecordTypeInfosByDeveloperName().get(recordTypeDeveloperName).getRecordTypeId();
		return recordTypeId;
	}

	@AuraEnabled
	//Call UI API to get picklist values by record type 
	public static String getPicklistValueByRecordType(String objectApiName, String recordTypeName, String fieldApiName) {
		//String sfdcURL = 'https://dfdev2-nbnco-customer.cs6.force.com/enterpriseEthernet';
		String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm();

		String REST_ENDPOINT = '/services/data/v42.0/ui-api/object-info/';
		String valueType = '/picklist-values/';
		/*
		  String objectApiName =  'Incident_Management__c';
		  String recordTypeName = 'Service Incident';
		  String fieldApiName = 'Incident_Status__c' ; 
		 */

		Schema.SObjectType objectType = Schema.getGlobalDescribe().get(objectApiName);
		Id recordTypeId = objectType.getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();

		String restAPIURL = sfdcURL + REST_ENDPOINT + objectApiName + valueType + recordTypeId + '/' + fieldApiName;
		System.debug('@@restAPIURL  ---> ' + restAPIURL);

		HTTP http = new HTTP();
		HttpRequest httpRequest = new HttpRequest();
		httpRequest.setMethod('GET');

		System.debug('@@getPicklistValueByRecordType - getSessionId ---> ' + UserInfo.getOrganizationId().substring(0, 15) + ' ' + UserInfo.getSessionId().substring(15));
		httpRequest.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());

		httpRequest.setEndpoint(restAPIURL);

		HttpResponse response = http.send(httpRequest);
		System.debug('@@getPicklistValueByRecordType - response.getBody() ---> ' + response.getBody());
		return response.getBody();
	}	

	@AuraEnabled
	//Get EE email custom settings
	public static EE_Email_Settings__c getEEEmailCustomSettings() {
		return EE_Email_Settings__c.getOrgDefaults();
	}

	@AuraEnabled
	//Get EE Assurance pick list from custom settings by pick list name
	public static List<Map<String, String>> getASPickListCustomSettings(String picklistName) {

		List<Map<String, String>> results = new List<Map<String, String>> ();

		try
		{
			for (DF_AS_Picklist_Values__c setting : DF_AS_Picklist_Values__c.getAll().values()) {
				if (setting.Picklist__c == picklistName && setting.IsEnabled__c)
				{
					results.add(new Map<String, String> { 'label' => setting.Label__c, 'value' => setting.Value__c });
				}
			}
		}
		catch(Exception ex)
		{
			System.debug(ex);
		}

		return results;
	}

	@AuraEnabled
	//Get EE Assurance dependant pick list from custom settings by pick list name
	public static List<DF_AS_Picklist_Values__c> getASDependantPickListFromCustomSettings(String picklistName) {

		List<DF_AS_Picklist_Values__c> results = new List<DF_AS_Picklist_Values__c> ();

		try
		{
			for (DF_AS_Picklist_Values__c setting : DF_AS_Picklist_Values__c.getAll().values()) {
				if (setting.Picklist__c == picklistName && setting.IsEnabled__c)
				{
					results.add(setting);
				}
			}
		}
		catch(Exception ex)
		{
			System.debug(ex);
		}

		return results;
	}

	@AuraEnabled
	public static String getProfileName() {
		return GlobalUtility.getProfileName();
	}

	@AuraEnabled
	public static String getRoleName() {
		return GlobalUtility.getRoleName();
	}
	
	@AuraEnabled
    public static List<EE_AS_WorkLog__c> getIncidentNotes(string incId) {
        return EE_AS_TnDController.getIncidentNotes(incId);
    }
	
	@AuraEnabled
    public static List<EE_AS_WorkLog__c> getNetworkIncidentNotes(string incId) {
        return EE_AS_TnDController.getNetworkIncidentNotes(incId);
    }
	
	@AuraEnabled
    public static string createIncidentNotes(string incId, string noteDetail, string noteType)
    {
        return EE_AS_TnDController.createIncidentNotes(incId, noteDetail, noteType);
    }
}