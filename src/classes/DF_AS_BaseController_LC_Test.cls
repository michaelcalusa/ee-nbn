@isTest 
public class DF_AS_BaseController_LC_Test {

	@isTest
	Public static void test_getRecordTypeId() {
		
		Id recordTypeId;
		DF_AS_TestService.createPicklistCustomSettingsForAS();
    	User commUser = DF_TestData.createDFCommUser();
		
		system.debug('@@getRecordTypeId - user details --->'+commUser);

        system.runAs(commUser) {
	    	test.startTest();
			recordTypeId = DF_AS_BaseController_LC.getRecordTypeId('Incident_Management__c', 'Service_Request');
			system.debug('@@getRecordTypeId - recordTypeId --->'+recordTypeId);
			test.stopTest();
        }
		            
        // Assertions
		system.assertNotEquals(null, recordTypeId);
	}

	@isTest
	Public static void test_getASDependantPickListFromCustomSettings() {
		
		List<DF_AS_Picklist_Values__c> results;
		DF_AS_TestService.createPicklistCustomSettingsForAS();
    	User commUser = DF_TestData.createDFCommUser();
		
		system.debug('@@getASDependantPickListFromCustomSettings - user details --->'+commUser);

        system.runAs(commUser) {
	    	test.startTest();
			results = DF_AS_BaseController_LC.getASDependantPickListFromCustomSettings('ServiceRequestTypeNIssue');
			system.debug('@@test_getASDependantPickListFromCustomSettings - results --->'+results);		
			test.stopTest();
        }
		            
        // Assertions
		system.assertNotEquals(0, results.size());
	}

	@isTest
	Public static void test_searchOpenIncidents() {
		
		User commUser = DF_TestData.createDFCommUser();
		User user = [Select contact.accountid from User where ID=:commUser.ID Limit 1];

		system.runAs(commUser){
			DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(user.contact.accountid);
	        insert opptyBundle;

	    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
	    	String latitude = '-33.840213';
	    	String longitude = '151.207368';

			DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
			dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
			insert dfQuote;

			// Create DF Order
			DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
			dfOrder.BPI_Id__c = 'BPI000000123';
			insert dfOrder;

			String OrderID = dfOrder.id;
			
			List<Incident_Management__c> incidents = new List<Incident_Management__c>();
			// create service incident record
	        Incident_Management__c serviceIncident1 = new Incident_Management__c();
	        serviceIncident1.PRI_ID__c = 'BPI000000123';
	        serviceIncident1.Incident_Number__c = 'SI67890'; 
	        serviceIncident1.Incident_Type__c = 'Service Incident'; 
	        serviceIncident1.Industry_Status__c = 'Acknowledged';
	        serviceIncident1.serviceRestorationSLA__c = 'Standard';
	        serviceIncident1.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Service Incident').getRecordTypeId();
	        serviceIncident1.Correlation_Id__c = '8ebe7f93-56dc-4145-951c-99999';
	        serviceIncident1.DF_Order__c = dfOrder.Id;
	        incidents.add(serviceIncident1);

	        // create service alert record
	        Incident_Management__c serviceAlert1 = new Incident_Management__c();
	        serviceAlert1.PRI_ID__c = 'BPI000000123';
	        serviceAlert1.Incident_Number__c = 'SA67890'; 
	        serviceAlert1.Incident_Type__c = 'Service Alert'; 
	        serviceAlert1.Industry_Status__c = EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_INCIDENT_CREATED;
	        serviceAlert1.serviceRestorationSLA__c = 'Standard';
	        serviceAlert1.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Service Alert').getRecordTypeId();
	        incidents.add(serviceAlert1);
	        insert incidents;

			system.runAs(new User(ID=commUser.Id)) {
		        List<EE_AS_Incident_Account_Relationship__c> incidentAccRels = new List<EE_AS_Incident_Account_Relationship__c>();
				EE_AS_Incident_Account_Relationship__c incidentAccRel1 = new EE_AS_Incident_Account_Relationship__c();
		    	incidentAccRel1.Related_Incident__c = serviceIncident1.Id;
		    	incidentAccRel1.Related_Account__c = commUser.AccountId;
		    	incidentAccRel1.Access_Seeker_ID__c = commUser.Account.Access_seeker_id__c;
		    	incidentAccRel1.BPI_Id__c = 'BPI000000123';
		    	incidentAccRels.add(incidentAccRel1);

		    	EE_AS_Incident_Account_Relationship__c incidentAccRel2 = new EE_AS_Incident_Account_Relationship__c();
		    	incidentAccRel2.Access_Seeker_ID__c = commUser.Account.Access_seeker_id__c;
		    	incidentAccRel2.Related_Account__c = commUser.AccountId;
		    	incidentAccRel2.Related_Incident__c = serviceAlert1.Id;
		    	incidentAccRel2.BPI_Id__c = 'BPI000000123';
		    	incidentAccRels.add(incidentAccRel2);
		    	insert incidentAccRels;
	    	}

	    	List<TND_Result__c> tndRecs2 = new  List<TND_Result__c>();
	    	TND_Result__c tndResult3 = new  TND_Result__c();
			tndResult3.Reference_Id__c = 'REF4567890565777';
			tndResult3.Test_Id__c = 'TST_0000777';
			tndResult3.Access_Technology__c = 'Enterprise Ethernet';
			tndResult3.Channel__c = 'SALESFORCE';
			tndResult3.Correlation_Id__c = '3c11a419-c552-4d0d-aae45545488777';
			tndResult3.Req_Class_Of_Service__c = 'HIGH;MEDIUM';
			tndResult3.Req_OVC_Id__c = 'OVC123456789077';
			tndResult3.Req_Packet_Size__c = 65;
			tndResult3.Requested_Date__c = Datetime.now();
			tndResult3.Requested_By__c = UserInfo.getUserId();
			tndResult3.Status__c = 'Complete';
			tndResult3.Completion_Date__c = Datetime.now().addMinutes(-30);
			tndResult3.Related_Order__c = OrderID;
			tndRecs2.add(tndResult3);
			insert tndRecs2;
    	}
		Map<String, List<Incident_Management__c>> results = new Map<String, List<Incident_Management__c>>();
		String testID = 'TST_0000777';
		TND_Result__c existingTNDRec = EE_AS_TnDController.getTestResultsForTestID(testID);
		

		system.runAs(new User(ID=existingTNDRec.createdbyId)){
	    	test.startTest();
			results = DF_AS_BaseController_LC.searchOpenIncidents('');
			system.debug('@@test_searchOpenIncidents - results --->'+results);
			system.assertEquals(1, results.size());
			results = DF_AS_BaseController_LC.searchOpenIncidents('BPI000000123');
			system.debug('@@test_searchOpenIncidents - results --->'+results);	
			system.assertEquals(1, results.size());
			test.stopTest();                
        }
	}

	@isTest
	Public static void test_getImpactedServicesByIncidentId() {
		
		User commUser = DF_TestData.createDFCommUser();
		List<EE_AS_Incident_Account_Relationship__c> results = new List<EE_AS_Incident_Account_Relationship__c>();

		system.runAs(commUser) {
	    	test.startTest();
			results = DF_AS_BaseController_LC.getImpactedServicesByIncidentId('');
			system.debug('@@test_getImpactedServicesByIncidentId - results --->'+results);		
			test.stopTest();                
        }
		            
        // Assertions
		system.assertEquals(0, results.size());
	}

	@isTest
	Public static void test_getCurrentUserAccountAccessSeekerId() {
		
		User commUser = DF_TestData.createDFCommUser();
		String results = '';

		system.runAs(commUser) {
	    	test.startTest();
			results = DF_AS_BaseController_LC.getCurrentUserAccountAccessSeekerId();
			system.debug('@@test_getCurrentUserAccountAccessSeekerId - results --->'+results);		
			test.stopTest();                
        }
		            
        // Assertions
		system.assertEquals('ASI500050005000', results);
	}

	@isTest
	Public static void test_getCurrentUserAccountName() {
		
		User commUser = DF_TestData.createDFCommUser();
		String results = '';

		system.runAs(commUser) {
	    	test.startTest();
			results = DF_AS_BaseController_LC.getCurrentUserAccountName();
			system.debug('@@test_getCurrentUserAccountName - results --->'+results);		
			test.stopTest();                
        }
		            
        // Assertions
		system.assertEquals('RSPTest Acct', results);
	}

	@isTest
	Public static void test_getASPickListValuesIntoList() {
		
		User commUser = DF_TestData.createDFCommUser();
		String results = '';

		system.runAs(commUser) {
	    	test.startTest();
			results = DF_AS_BaseController_LC.getASPickListValuesIntoList('Incident_Management__c', 'Site_Induction_Essentials__c');
			system.debug('@@test_getASPickListValuesIntoList - results --->'+results);		
			test.stopTest();                
        }
		            
        // Assertions
		system.assertNotEquals(0, results.length());
	}

	@isTest
	Public static void test_getEEEmailCustomSettings() {
		
		EE_Email_Settings__c result;
		DF_AS_TestService.createEmailCustomSettings();
    	User commUser = DF_TestData.createDFCommUser();
		
		system.debug('@@test_getEEEmailCustomSettings - user details --->'+commUser);

        system.runAs(commUser) {
	    	test.startTest();
			result = DF_AS_BaseController_LC.getEEEmailCustomSettings();
			system.debug('@@test_getEEEmailCustomSettings - results --->'+result);		
			test.stopTest();
        }
		            
        // Assertions
		system.assertNotEquals(null, result);
	}


	@isTest
	Public static void test_getPicklistValueByRecordType() {
		
		String results;		
    	User commUser = DF_TestData.createDFCommUser();
		
		system.debug('@@test_getPicklistValueByRecordType - user details --->'+commUser);

        system.runAs(commUser) {
			// Set mock callout class 
	        DF_AS_MockHttpResponseGenerator mockResp = new DF_AS_MockHttpResponseGenerator();
        
	        // Verify the success call out
			mockResp.responseType = 'PicklistSuccess';
			Test.setMock(HttpCalloutMock.class, mockResp);
			
	    	test.startTest();
			results = DF_AS_BaseController_LC.getPicklistValueByRecordType('Incident_Management__c', 'Service Incident', 'Incident_Status__c');
			system.debug('@@test_getPicklistValueByRecordType - results --->'+results);		
			test.stopTest();
        }
		            
        // Assertions
		system.assertNotEquals(0, results.length());
	}	

	@isTest
	Public static void test_getASPicklistCustomSettings() {
		
		List<Map<String, String>> results;
		DF_AS_TestService.createPicklistCustomSettingsForAS();
    	User commUser = DF_TestData.createDFCommUser();
		
		system.debug('@@test_getASPicklistCustomSettings - user details --->'+commUser);

        system.runAs(commUser) {
	    	test.startTest();
			results = DF_AS_BaseController_LC.getASPicklistCustomSettings('TicketType');
			system.debug('@@test_getASPicklistCustomSettings - results --->'+results);		
			test.stopTest();
        }
		            
        // Assertions
		system.assertNotEquals(0, results.size());
	}
	
	@isTest
	Public static void test_getProfileName() {
		
		String profileName;
		DF_AS_TestService.createCustomSettingsForAS();
    	User commUser = DF_TestData.createDFCommUser();
		
		system.debug('@@test_getProfileName - user details --->'+commUser);
		
        system.runAs(commUser) {
	    	test.startTest();
			profileName = DF_AS_BaseController_LC.getProfileName();
			system.debug('@@test_getProfileName - results --->'+profileName);			
			test.stopTest();                
        }
		            
        // Assertions
		system.assertEquals(true, String.isNotBlank(profileName));
	}

	@isTest
	Public static void test_getRoleName() {
		
		String roleName;
		DF_AS_TestService.createCustomSettingsForAS();
    	User commUser = DF_TestData.createDFCommUser();
		
		system.debug('@@test_getRoleName - user details --->'+commUser);
		
        system.runAs(commUser) {
	    	test.startTest();
			roleName = DF_AS_BaseController_LC.getRoleName();
			system.debug('@@test_getRoleName - results'+roleName);			
			test.stopTest();                
        }
		            
        // Assertions
		system.assertEquals(true, String.isNotBlank(roleName));
	}
    
    @isTest static void testSearchServiceCache200() {  
        String response;
        User commUser;
        DF_SvcCacheSearchResults result;
        
        response = ' [{"data":{"orderItemId":"212","nni":{"csa":"CSA180000002222","nni_mode":"B","nasId":"ASI500050005000","subscriber_id":"246","nni_lag_id":"104","poi":"1-MEL-MELBOURNE-MP","id":"NNI090000016341","type":"nni","access_seeker_id":"ASI500050005000"},"poi":"1-MEL-MELBOURNE-MP","type":"OVC","cosMappingMode":"PCP","inniSvlanId":"2764","samInstanceId":"SAM","uni":{"interfaceType":"Copper (Auto-negotiate)","tpId":"0x9100","transientId":"1","id":"UNI900000002986","type":"UNIE","portId":"4","ovcType":"Access EVPL","interfaceBandwidth":"2GB","samInstanceId":"SAM"},"cosLowBandwidth":"10Mbps","ePipeSites":[{"tunnels":[{"toIpAddress":"192.168.4.67","id":"1","toHostName":"SWEAS0000001DV"},{"toIpAddress":"192.168.4.68","id":"3","toHostName":"SWEAS0000002DV"},{"toIpAddress":"192.168.4.73","id":"15","toHostName":"SWEAS0000005DV"},{"toIpAddress":"192.168.4.74","id":"16","toHostName":"SWEAS0000006DV"}],"nni_mode":"B","role":"EFS","sourceMacAddress":"B8-E7-79-00-00-01","ipAddress":"192.168.4.69","id":"SWEFS0000001DV","mepId":"1","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.67","id":"1","toHostName":"SWEAS0000001DV"},{"toIpAddress":"192.168.4.68","id":"3","toHostName":"SWEAS0000002DV"},{"toIpAddress":"192.168.4.73","id":"15","toHostName":"SWEAS0000005DV"},{"toIpAddress":"192.168.4.74","id":"16","toHostName":"SWEAS0000006DV"}],"nni_mode":"B","role":"EFS","sourceMacAddress":"B8-E7-79-00-00-02","ipAddress":"192.168.4.70","id":"SWEFS0000002DV","mepId":"2","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"1","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"2","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-00-00-01","ipAddress":"192.168.4.67","id":"SWEAS0000001DV","mepId":"3","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"1","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"2","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-00-00-02","ipAddress":"192.168.4.68","id":"SWEAS0000002DV","mepId":"4","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"15","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"16","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-01-00-01","ipAddress":"192.168.4.73","id":"SWEAS0000005DV","mepId":"5","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"15","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"16","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-01-00-02","ipAddress":"192.168.4.74","id":"SWEAS0000006DV","mepId":"6","samInstanceId":"SAM"}],"connectionProfileId":"2","enni_lag":[{"hostName":"SWEFS0000001DV","nni_lag_name":"ALG-02047","id":"ALG-02047","ports":[{"port":"7","daughterCard":"1","shelf":"1","cardSlot":"5"}],"type":"lag"},{"hostName":"SWEFS0000002DV","nni_lag_name":"ALG-02048","id":"ALG-02048","ports":[{"port":"7","daughterCard":"1","shelf":"1","cardSlot":"5"}],"type":"lag"}],"sVLanId":"2740","routeType":"State","uniVLanId":"20-22","priId":"BPI000000011551","maximumFrameSize":"Standard"},"id":"OVC000000012822","type":"enterpriseService","relationShips":{"id":"BPI000000011551","data":{"csaId":"CSA180000002222","accessSeekerId":"ASI500050005000","locationId":"LOC123456799999","samId":"2ARM-01","enterpriseEthernetServiceLevelRegion":"Regional Centre","term":"12 months","templateVersion":"1.0","accessTechnologyType":"Fibre","templateId":"TPL900000000001","productType":"EEAS","serviceRestorationSLA":"Enhanced - 12 (24/7)"},"type":"eeasProductInstance","status":"active"},"connectedTo":[{"data":{"portNo":"4"},"id":"SWBTD0009200-1_4","type":"btdPort"},{"data":{"btdType":"Standard (CSAS)","btdMounting":"Rack","physicalName":"3VRB-00-00-BTD-0012","locationId":"LOC123456799999","ipAddress":"10.0.0.1","powerSupply2":"Not Required","powerSupply1":"DC(-d8V to -22V)","subnetMask":"255.255.255.240","type":"BTD","status":"planned"},"id":"SWBTD0009200","type":"btd"},{"data":{"resources":[{"lagId":3,"lagDescription":"ALG-00010","id":"SWAAS9009998","ports":[{"port":3,"slot":4}]},{"lagId":1,"lagDescription":"ALG-00004","id":"SWBTD0009200","ports":[{"port":1,"slot":1}]}]},"id":"LAG000000002695","type":"lag"},{"data":{"physicalName":"3VRB-00-00-AAS-0092","ipAddress":"192.168.99.18"},"id":"SWAAS9009998","type":"aas"}],"status":"active"}]';
 
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

        // Set up commUser to run test as
        commUser = DF_TestData.createDFCommUser();      
        system.runAs(commUser) { 
            test.startTest();           
            result = DF_AS_BaseController_LC.searchServiceCache('BPI900000002986');                            
            test.stopTest();                                             
        }   
       System.assertEquals(result.ovcs[0].ovcId,'OVC000000012822');
    }
    
    @isTest static void testSearchServiceCache404() {  
        String response;
        User commUser;
        DF_SvcCacheSearchResults result;

        // Generate mock response
        response = '{"status":"NOT_FOUND","message":"Resource not found in Service Cache","debugMessage":"Requested resource(s) not found","timestamp":"2018-09-05T10:47:16.741","errorCode":"004040"}';
 
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(404, response, null));

        // Set up commUser to run test as
        commUser = DF_TestData.createDFCommUser();      

        system.runAs(commUser) { 
            test.startTest();           
            result = DF_AS_BaseController_LC.searchServiceCache('BPI900000002986');                            
            test.stopTest();                                            
        }   
        	system.assertEquals(result.message, 'Resource not found in Service Cache');

    }  

    
    @isTest static void testWatchMe() {  

    	Incident_Management__c inc = new  Incident_Management__c();
		inc.Incident_Number__c = 'INC09988665545';
		inc.Access_Seeker_ID__c = 'ASI000000892811';
		inc.Correlation_Id__c = '3c11a419-c552-4d0d-aae4554534344343';

		Boolean isWatching = false;
		Insert inc;
		Test.startTest();
		DF_AS_BaseController_LC.watchMe(inc.id);
		isWatching = DF_AS_BaseController_LC.isWatching(inc.id);
		Test.stopTest();

		System.assertEquals(true, isWatching);

    }   

}