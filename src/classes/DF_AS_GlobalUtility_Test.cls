/*------------------------------------------------------------------------
Author:        Kai-Fan Hsieh
Company:       Contracotor
Description:   A test class created to test DF_AS_GlobalUtility methods               

History
<Date>      <Authors Name>     <Brief Description of Change>
--------------------------------------------------------------------------*/
@isTest
private class DF_AS_GlobalUtility_Test {

    @testSetup
    static void setupTestData() {

        String EE_AS_CONTACT_MATRIX_ROLE = 'Operational - Assurance';
        String STATUS_ACTIVE = 'Active';

        // create account1 record
        Account account1 = new Account();
        account1.Name = 'Telstra';
        account1.Access_Seeker_ID__c = 'AS123';
        insert account1;

        Account account2 = new Account();
        account2.Name = 'Optus';
        account2.Access_Seeker_ID__c = 'AS456';
        insert account2;

        Account account3 = new Account();
        account3.Name = 'Vodafone';
        account3.Access_Seeker_ID__c = 'AS789';
        insert account3;

        Account account4 = new Account();
        account4.Name = 'nbn';
        account4.Access_Seeker_ID__c = 'AS7098';
        insert account4;

        Contact contact1 = new Contact();
        contact1.FirstName = 'FN1';
        contact1.LastName = 'LN1';
        contact1.Account = account1;
        contact1.Email = '123@mailinator.com';
        contact1.Status__c = STATUS_ACTIVE;
        contact1.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact1.AccountId=account1.Id;
        insert contact1;

        Contact contact2 = new Contact();
        contact2.FirstName = 'NBN FN1';
        contact2.LastName = 'NBN LN1';
        contact2.Account = account4;
        contact2.Email = '456@mailinator.com';
        contact2.Status__c = STATUS_ACTIVE;
        contact2.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact2.AccountId=account4.Id;
        insert contact2;

        Contact contact3 = new Contact();
        contact3.FirstName = 'FN2';
        contact3.LastName = 'LN2';
        contact3.Account = account2;
        contact3.Email = 'a@mailinator.com';
        contact3.Status__c = STATUS_ACTIVE;
        contact3.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact3.AccountId=account2.Id;
        insert contact3;

        Contact contact4 = new Contact();
        contact4.FirstName = 'NBN FN2';
        contact4.LastName = 'NBN LN2';
        contact4.Account = account4;
        contact4.Email = 'b@mailinator.com';
        contact4.Status__c = STATUS_ACTIVE;
        contact4.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact4.AccountId=account4.Id;
        insert contact4;

        Contact contact5 = new Contact();
        contact5.FirstName = 'FN3';
        contact5.LastName = 'LN3';
        contact5.Account = account3;
        contact5.Email = 'c@mailinator.com';
        contact5.Status__c = STATUS_ACTIVE;
        contact5.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact5.AccountId=account3.Id;
        insert contact5;

        Contact contact6 = new Contact();
        contact6.FirstName = 'FN3';
        contact6.LastName = 'LN3';
        contact6.Account = account1;
        contact6.Email = '6@mailinator.com';
        contact6.Status__c = STATUS_ACTIVE;
        contact6.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact6.AccountId=account1.Id;
        insert contact6;

        Contact contact7 = new Contact();
        contact7.FirstName = 'FN4';
        contact7.LastName = 'LN4';
        contact7.Account = account2;
        contact7.Email = '7@mailinator.com';
        contact7.Status__c = STATUS_ACTIVE;
        contact7.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact7.AccountId=account2.Id;
        insert contact7;

        Profile commProfile = [SELECT Id 
                               FROM Profile 
                               WHERE Name = 'DF Partner User'];

        User commUser1 = new User();
        commUser1.Alias = 'test123';
        commUser1.Email = 'test123@noemail.com';
        commUser1.Emailencodingkey = 'ISO-8859-1';
        commUser1.Lastname = 'Testing';
        commUser1.Languagelocalekey = 'en_US';
        commUser1.Localesidkey = 'en_AU';
        commUser1.Profileid = commProfile.Id;
        commUser1.IsActive = true;
        commUser1.ContactId = contact1.Id;
        commUser1.Timezonesidkey = 'Australia/Sydney';
        commUser1.Username = 'tester@noemail.com.test';
        insert commUser1;

        User commUser2 = new User();
        commUser2.Alias = 'test1234';
        commUser2.Email = 'test1234@noemail.com';
        commUser2.Emailencodingkey = 'ISO-8859-1';
        commUser2.Lastname = 'Testing';
        commUser2.Languagelocalekey = 'en_US';
        commUser2.Localesidkey = 'en_AU';
        commUser2.Profileid = commProfile.Id;
        commUser2.IsActive = true;
        commUser2.ContactId = contact6.Id;
        commUser2.Timezonesidkey = 'Australia/Sydney';
        commUser2.Username = 'tester12@noemail.com.test';
        insert commUser2;

        Team_Member__c teamMember1 = new Team_Member__c();
        teamMember1.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember1.Account__c = account1.Id;
        teamMember1.Customer_Contact__c = contact1.Id;
        teamMember1.Customer_Inbox__c = null;
        teamMember1.NBN_Contact__c = contact2.Id;
        teamMember1.NBN_Inbox__c = null;
        teamMember1.Role__c = EE_AS_CONTACT_MATRIX_ROLE;
        teamMember1.Role_Type__c = 'Tickets - Service Incident';
        insert teamMember1;

        Team_Member__c teamMember2 = new Team_Member__c();
        teamMember2.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember2.Account__c = account1.Id;
        teamMember2.Customer_Contact__c = null;
        teamMember2.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember2.NBN_Contact__c = null;
        teamMember2.NBN_Inbox__c = 'f@mailinator.com';
        teamMember2.Role__c = EE_AS_CONTACT_MATRIX_ROLE;
        teamMember2.Role_Type__c = 'Tickets - Service Alert';
        insert teamMember2;

        Team_Member__c teamMember3 = new Team_Member__c();
        teamMember3.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember3.Account__c = account1.Id;
        teamMember3.Customer_Contact__c = null;
        teamMember3.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember3.NBN_Contact__c = null;
        teamMember3.NBN_Inbox__c = 'f@mailinator.com';
        teamMember3.Role__c = EE_AS_CONTACT_MATRIX_ROLE;
        teamMember3.Role_Type__c = 'Tickets - Infrastructure Restoration';
        insert teamMember3;

        Team_Member__c teamMember4 = new Team_Member__c();
        teamMember4.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember4.Account__c = account1.Id;
        teamMember4.Customer_Contact__c = null;
        teamMember4.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember4.NBN_Contact__c = null;
        teamMember4.NBN_Inbox__c = 'f@mailinator.com';
        teamMember4.Role__c = EE_AS_CONTACT_MATRIX_ROLE;
        teamMember4.Role_Type__c = 'Tickets - Change Request';
        insert teamMember4;

        Team_Member__c teamMember5 = new Team_Member__c();
        teamMember5.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember5.Account__c = account2.Id;
        teamMember5.Customer_Contact__c = contact3.Id;
        teamMember5.Customer_Inbox__c = null;
        teamMember5.NBN_Contact__c = contact4.Id;
        teamMember5.NBN_Inbox__c = null;
        teamMember5.Role__c = EE_AS_CONTACT_MATRIX_ROLE;
        teamMember5.Role_Type__c = 'Tickets - Service Incident';
        insert teamMember5;

        Team_Member__c teamMember6 = new Team_Member__c();
        teamMember6.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember6.Account__c = account2.Id;
        teamMember6.Customer_Contact__c = null;
        teamMember6.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember6.NBN_Contact__c = null;
        teamMember6.NBN_Inbox__c = 'f@mailinator.com';
        teamMember6.Role__c = EE_AS_CONTACT_MATRIX_ROLE;
        teamMember6.Role_Type__c = 'Tickets - Service Alert';
        insert teamMember6;

        Team_Member__c teamMember7 = new Team_Member__c();
        teamMember7.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember7.Account__c = account2.Id;
        teamMember7.Customer_Contact__c = null;
        teamMember7.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember7.NBN_Contact__c = null;
        teamMember7.NBN_Inbox__c = 'f@mailinator.com';
        teamMember7.Role__c = EE_AS_CONTACT_MATRIX_ROLE;
        teamMember7.Role_Type__c = 'Tickets - Infrastructure Restoration';
        insert teamMember7;

        Team_Member__c teamMember8 = new Team_Member__c();
        teamMember8.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember8.Account__c = account2.Id;
        teamMember8.Customer_Contact__c = null;
        teamMember8.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember8.NBN_Contact__c = null;
        teamMember8.NBN_Inbox__c = 'f@mailinator.com';
        teamMember8.Role__c = EE_AS_CONTACT_MATRIX_ROLE;
        teamMember8.Role_Type__c = 'Tickets - Change Request';
        insert teamMember8;

        Team_Member__c teamMember9 = new Team_Member__c();
        teamMember9.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember9.Account__c = account3.Id;
        teamMember9.Customer_Contact__c = contact5.Id;
        teamMember9.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember9.NBN_Contact__c = contact4.Id;
        teamMember9.NBN_Inbox__c = 'f@mailinator.com';
        teamMember9.Role__c = EE_AS_CONTACT_MATRIX_ROLE;
        teamMember9.Role_Type__c = 'Tickets - Change Request';
        insert teamMember9;

        Team_Member__c teamMember10 = new Team_Member__c();
        teamMember10.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember10.Account__c = account1.Id;
        teamMember10.Customer_Contact__c = contact6.Id;
        teamMember10.Customer_Inbox__c = null;
        teamMember10.NBN_Contact__c = contact4.Id;
        teamMember10.NBN_Inbox__c = null;
        teamMember10.Role__c = EE_AS_CONTACT_MATRIX_ROLE;
        teamMember10.Role_Type__c = 'Tickets - Service Incident';
        insert teamMember10;

        Team_Member__c teamMember11 = new Team_Member__c();
        teamMember11.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember11.Account__c = account2.Id;
        teamMember11.Customer_Contact__c = contact7.Id;
        teamMember11.Customer_Inbox__c = null;
        teamMember11.NBN_Contact__c = contact4.Id;
        teamMember11.NBN_Inbox__c = null;
        teamMember11.Role__c = EE_AS_CONTACT_MATRIX_ROLE;
        teamMember11.Role_Type__c = 'Tickets - Service Incident';
        insert teamMember11;

        // create opportunity bundle record
        DF_Opportunity_Bundle__c oppBundle = new DF_Opportunity_Bundle__c();
        oppBundle.Account__c = account1.Id;
        insert oppBundle;
        
        // create quote record
        DF_Quote__c quote = new DF_Quote__c();
        quote.Opportunity_Bundle__c = oppBundle.Id;
        quote.Location_Id__c = 'LOC112233441';
        quote.Address__c = '380 Dockloands Drive, Docklands, VIC 3000';
        insert quote;

        // create order record
        DF_Order__c order = new DF_Order__c();
        order.Opportunity_Bundle__c = oppBundle.Id;
        order.BPI_Id__c = '12345';
        order.Order_Submitted_Date__c = System.today();
        order.DF_Quote__c = quote.Id;
        insert order;

        Incident_Management__c serviceIncident = new Incident_Management__c();
        Incident_Management__c serviceAlert = new Incident_Management__c();
        Incident_Management__c networkIncident = new Incident_Management__c();
        Incident_Management__c changeRequest = new Incident_Management__c();

        System.runAs(commUser1) {
        
            // create service incident record
            serviceIncident.PRI_ID__c = 'SI12345';
            serviceIncident.Incident_Number__c = 'SI123'; 
            serviceIncident.Incident_Type__c = 'Service Incident'; 
            serviceIncident.Industry_Status__c = 'Acknowledged';
            serviceIncident.serviceRestorationSLA__c = 'Standard';
            serviceIncident.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Service Incident').getRecordTypeId();
            serviceIncident.Access_Seeker__c = account1.Id;
            insert serviceIncident;

            // create service alert record
            serviceAlert.PRI_ID__c = 'SA12345';
            serviceAlert.Incident_Number__c = 'SA123'; 
            serviceAlert.Incident_Type__c = 'Service Alert'; 
            serviceAlert.Industry_Status__c = 'Acknowledged';
            serviceAlert.serviceRestorationSLA__c = 'Standard';
            serviceAlert.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Service Alert').getRecordTypeId();
            insert serviceAlert;

            // create network incident record
            networkIncident.PRI_ID__c = 'NI123';
            networkIncident.Incident_Number__c = 'NI123'; 
            networkIncident.Incident_Type__c = 'Network Incident'; 
            networkIncident.Industry_Status__c = 'Acknowledged';
            networkIncident.serviceRestorationSLA__c = 'Standard';
            networkIncident.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Network Incident').getRecordTypeId();
            insert networkIncident;

            // create change request record
            changeRequest.PRI_ID__c = 'CRQ12345';
            changeRequest.Incident_Number__c = 'CRQ123'; 
            changeRequest.Incident_Type__c = 'Change Request'; 
            changeRequest.Industry_Status__c = 'Acknowledged';
            changeRequest.serviceRestorationSLA__c = 'Standard';
            changeRequest.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Change Request').getRecordTypeId();
            insert changeRequest;
        }

        EE_AS_Incident_Account_Relationship__c incidentAccountServAl = new EE_AS_Incident_Account_Relationship__c();
        incidentAccountServAl.Related_Incident__c = serviceAlert.Id;
        incidentAccountServAl.Related_Account__c = account1.Id;
        insert incidentAccountServAl;

        EE_AS_Incident_Account_Relationship__c incidentAccountNwInc = new EE_AS_Incident_Account_Relationship__c();
        incidentAccountNwInc.Related_Incident__c = networkIncident.Id;
        incidentAccountNwInc.Related_Account__c = account1.Id;
        insert incidentAccountNwInc;

        EE_AS_Incident_Account_Relationship__c incidentAccountChngReq = new EE_AS_Incident_Account_Relationship__c();
        incidentAccountChngReq.Related_Incident__c = changeRequest.Id;
        incidentAccountChngReq.Related_Account__c = account1.Id;
        insert incidentAccountChngReq;

        EE_AS_Incident_Account_Relationship__c incidentAccountServAl2 = new EE_AS_Incident_Account_Relationship__c();
        incidentAccountServAl2.Related_Incident__c = serviceAlert.Id;
        incidentAccountServAl2.Related_Account__c = account2.Id;
        insert incidentAccountServAl2;

        EE_AS_Incident_Watch__c watchUser1 = new EE_AS_Incident_Watch__c();
        watchUser1.Incident__c = serviceIncident.Id;
        watchUser1.User__c = commUser1.Id;
        insert watchUser1;

        EE_AS_Incident_Watch__c watchUser2 = new EE_AS_Incident_Watch__c();
        watchUser2.Incident__c = serviceIncident.Id;
        watchUser2.User__c = commUser2.Id;
        insert watchUser2;

        DF_AS_Custom_Settings__c watchMeSetting = new DF_AS_Custom_Settings__c();
        watchMeSetting.Name = 'EE_AS_Inc_Watch_Email_Notification';
        watchMeSetting.Value__c = 'true';
        insert watchMeSetting;

        DF_AS_Custom_Settings__c businessSetting = new DF_AS_Custom_Settings__c();
        businessSetting.Name = 'EE_AS_Inc_Busi_Email_Notification';
        businessSetting.Value__c = 'true';
        insert businessSetting;

    }

    @isTest
    public static void testEmailsToSend() {
        Test.startTest();

        DF_Email_Setting__mdt assuranceEmailSetting = DF_AS_GlobalUtility.getDFEmailSetting('DF_Assurance');
        System.assertNotEquals(assuranceEmailSetting, null);

        OrgWideEmailAddress emailAddress = DF_AS_GlobalUtility.getOrgWideEmailAddress(assuranceEmailSetting.Sender_Address__c);
        System.assertNotEquals(emailAddress, null);

        DF_AS_GlobalUtility.orgWideEmailAddress = emailAddress;
        
        List<Incident_Management__c> incidents = DF_AS_GlobalUtility.getIncidents(getIncidentIds());
        System.assertNotEquals(incidents, null);
        System.assertEquals(4, incidents.size());

        Map<String, String> incidentRoleTypeMap = DF_AS_GlobalUtility.getIncidentRoleType(incidents);
        System.assertNotEquals(incidentRoleTypeMap, null);
        System.assertEquals(4, incidentRoleTypeMap.size());

        Set<String> roleTypes = DF_AS_GlobalUtility.getRoleTypes(incidentRoleTypeMap);
        System.assertNotEquals(roleTypes, null);
        System.assertEquals(4, roleTypes.size());

        Map<String, Set<String>> incidentAccountsMap = DF_AS_GlobalUtility.getIncidentAccountsMap(incidents);
        System.assertNotEquals(incidentAccountsMap, null);
        System.assertEquals(4, incidentAccountsMap.size());

        Set<String> accountIds = DF_AS_GlobalUtility.getAccounts(incidentAccountsMap);
        System.assertNotEquals(accountIds, null);
        System.assertEquals(2, accountIds.size());
        
        Map<String,List<Team_Member__c>> accountTeamMembersMap = DF_AS_GlobalUtility.getAccountTeamMembersMap(accountIds, roleTypes, assuranceEmailSetting);
        System.assertNotEquals(accountTeamMembersMap, null);
        System.assertEquals(2, accountTeamMembersMap.size());

        List<List<Team_Member__c>> teamMembersList = accountTeamMembersMap.values();
        Map<String, Team_Member__c> contactMatrix = new Map<String, Team_Member__c>();
        for(List<Team_Member__c> teamMembers : teamMembersList) {
            for(Team_Member__c teamMember : teamMembers) {
                contactMatrix.put(teamMember.Id, teamMember);
            }
        }
        System.assertEquals(10, contactMatrix.size());

        Map<String, String> incidentToTemplateMap = getIncidentToTemplateMap();
        
        Map<String, String> templateMap = DF_AS_GlobalUtility.getEmailTemplates(incidentToTemplateMap);
        System.assertNotEquals(templateMap, null);
        System.assertEquals(1, templateMap.size());

        Set<String> incidentIds = incidentToTemplateMap.keySet();
        List<Messaging.SingleEmailMessage> emailsToSend = DF_AS_GlobalUtility.getEmailsToSend(incidentIds, incidentAccountsMap, 
                                            accountTeamMembersMap, incidentRoleTypeMap, 
                                            incidentToTemplateMap, templateMap, assuranceEmailSetting);
        System.assertNotEquals(emailsToSend, null);
        System.assertEquals(7, emailsToSend.size());

        List<Messaging.SingleEmailMessage> incidentCreatedByEmailToSend = DF_AS_GlobalUtility.getIncidentCreatedByEmailToSend(incidentToTemplateMap, templateMap, assuranceEmailSetting);
        System.assertNotEquals(incidentCreatedByEmailToSend, null);
        System.assertEquals(4, incidentCreatedByEmailToSend.size());

        List<Messaging.SingleEmailMessage> notificationEmailsToSend = DF_AS_GlobalUtility.getNotificationEmailsToSend(incidentToTemplateMap, templateMap, assuranceEmailSetting);
        System.assertNotEquals(notificationEmailsToSend, null);
        System.assertEquals(7, notificationEmailsToSend.size());

        List<Messaging.SingleEmailMessage> watchMeEmailsToSend = DF_AS_GlobalUtility.getWatchMeEmailsToSend(incidentToTemplateMap, templateMap, assuranceEmailSetting);
        System.assertNotEquals(watchMeEmailsToSend, null);
        System.assertEquals(1, watchMeEmailsToSend.size());

        List<Messaging.SingleEmailMessage> allEmailsToSend = DF_AS_GlobalUtility.getAllEmailsToSend(incidentToTemplateMap, assuranceEmailSetting);
        System.assertNotEquals(allEmailsToSend, null);
        System.assertEquals(12, allEmailsToSend.size());

        Test.stopTest();
    }

    @isTest static void testGetAddresses() {
        String address = '1@gmail.com, 2@gmail.com,3@gmail.com';
        List<String> toAddresses = DF_AS_GlobalUtility.getAddresses(address);
        System.assertNotEquals(toAddresses, null);
        System.assertEquals(toAddresses.size(), 3);
    }

    @isTest static void testIsEmailValid() {
        String email = '1@gmail.com';
        Boolean isValid = DF_AS_GlobalUtility.isEmailValid(email);
        System.assertEquals(isValid, true);
    }

    public static Set<String> getIncidentIds() {
        Set<String> incidentNumbers = new Set<String>{'SI123', 'SA123', 'NI123', 'CRQ123'};
        List<Incident_Management__c> incidents = [SELECT Id FROM Incident_Management__c WHERE Incident_Number__c IN :incidentNumbers];
        Set<String> incidentIds = new Set<String>();
        for(Incident_Management__c incident : incidents) {
            incidentIds.add(incident.Id);
        }
        return incidentIds;
    }

    public static Map<String, String> getIncidentRoleTypes() {
        return new Map<String, String>{ 'Service_Incident' => 'Service Incident',
                                        'Service_Alert' => 'Service Alert',
                                        'Network_Incident' => 'Network Incident',
                                        'Change_Request' => 'Change Request'};
    }

    public static Map<String, String> getIncidentToTemplateMap() {
        List<Incident_Management__c> incidents = DF_AS_GlobalUtility.getIncidents(getIncidentIds());
        Map<String, String> incidentToTemplateMap = new Map<String, String>();
        String TEMPLATE_NETWORK_INCIDENT_STATUS_NEW = 'EE AS Unplanned Outage New';
        for(Incident_Management__c incident : incidents) {
            incidentToTemplateMap.put(incident.Id, TEMPLATE_NETWORK_INCIDENT_STATUS_NEW);
        }
        return incidentToTemplateMap;
    }
    
    @isTest static void testGetEmailTemplateName() {
        Test.startTest();
        
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName('','',''),'');
                  
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_NETWORK_INCIDENT,EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_INCIDENT_CREATED,''),DF_Constants.TEMPLATE_NETWORK_INCIDENT_STATUS_NEW);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_NETWORK_INCIDENT,EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_TICKET_UPDATED_NBNCO,''),DF_Constants.TEMPLATE_NETWORK_INCIDENT_STATUS_IN_PROGRESS);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_NETWORK_INCIDENT,EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_INCIDENT_RESOLVED,''),DF_Constants.TEMPLATE_NETWORK_INCIDENT_STATUS_RESOLVED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_NETWORK_INCIDENT,EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_INCIDENT_CLOSED,''),DF_Constants.TEMPLATE_NETWORK_INCIDENT_STATUS_CLOSED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_ALERT,EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_INCIDENT_CREATED,''),DF_Constants.TEMPLATE_SERVICE_ALERT_STATUS_NEW);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_ALERT,EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_TICKET_UPDATED_NBNCO,''),DF_Constants.TEMPLATE_SERVICE_ALERT_STATUS_IN_PROGRESS);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_ALERT,EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_INCIDENT_RESOLVED,''),DF_Constants.TEMPLATE_SERVICE_ALERT_STATUS_RESOLVED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_ALERT,EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_INCIDENT_CLOSED,''),DF_Constants.TEMPLATE_SERVICE_ALERT_STATUS_CLOSED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_CHANGE_REQUEST,EE_AS_Remedy_To_SF_TriggerHandler.INCIDENT_STATUS_NEW,''),DF_Constants.TEMPLATE_CHANGE_REQUEST_STATUS_NEW);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_CHANGE_REQUEST,EE_AS_Remedy_To_SF_TriggerHandler.INCIDENT_STATUS_IN_PROGRESS,''),DF_Constants.TEMPLATE_CHANGE_REQUEST_STATUS_IN_PROGRESS);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_CHANGE_REQUEST,EE_AS_Remedy_To_SF_TriggerHandler.INCIDENT_STATUS_PAUSED,''),DF_Constants.TEMPLATE_CHANGE_REQUEST_STATUS_IN_PROGRESS);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_CHANGE_REQUEST,EE_AS_Remedy_To_SF_TriggerHandler.INCIDENT_STATUS_COMPLETE,''),DF_Constants.TEMPLATE_CHANGE_REQUEST_STATUS_CLOSED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_CHANGE_REQUEST,EE_AS_Remedy_To_SF_TriggerHandler.INCIDENT_STATUS_CANCELLED,''),DF_Constants.TEMPLATE_CHANGE_REQUEST_STATUS_CLOSED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_INCIDENT,'',DF_Constants.SERVICE_INCIDENT_NOTIFIC_TYPE_ACKNOWLEDGED),DF_Constants.TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_ACKNOWLEDGED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_INCIDENT,'',DF_Constants.SERVICE_INCIDENT_NOTIFIC_TYPE_ACCEPTED),DF_Constants.TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_ACCEPTED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_INCIDENT,'',DF_Constants.SERVICE_INCIDENT_NOTIFIC_TYPE_CREATED),DF_Constants.TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_CREATED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_INCIDENT,'',DF_Constants.SERVICE_INCIDENT_NOTIFIC_TYPE_NBN_ACTION_REQUIRED),DF_Constants.TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_NBN_ACTION_REQUIRED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_INCIDENT,'',DF_Constants.SERVICE_INCIDENT_NOTIFIC_TYPE_NBN_ACTION_COMPLETED),DF_Constants.TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_NBN_ACTION_COMPLETED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_INCIDENT,'',DF_Constants.SERVICE_INCIDENT_NOTIFIC_TYPE_TICKET_UPDATED_NBNCO),DF_Constants.TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_TICKET_UPDATED_NBNCO);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_INCIDENT,'',DF_Constants.SERVICE_INCIDENT_NOTIFIC_TYPE_RESOLVED),DF_Constants.TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_RESOLVED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_INCIDENT,'',DF_Constants.SERVICE_INCIDENT_NOTIFIC_TYPE_CLOSED),DF_Constants.TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_CLOSED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_INCIDENT,'',DF_Constants.SERVICE_INCIDENT_NOTIFIC_TYPE_REJECTED),DF_Constants.TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_REJECTED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_INCIDENT,'',DF_Constants.SERVICE_INCIDENT_NOTIFIC_TYPE_ADDITIONAL_INFO),DF_Constants.TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_ADDITIONAL_INFO);
        
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_REQUEST,'',DF_Constants.SERVICE_REQUEST_NOTIFIC_TYPE_ACKNOWLEDGED),DF_Constants.TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_ACKNOWLEDGED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_REQUEST,'',DF_Constants.SERVICE_REQUEST_NOTIFIC_TYPE_ACCEPTED),DF_Constants.TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_ACCEPTED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_REQUEST,'',DF_Constants.SERVICE_REQUEST_NOTIFIC_TYPE_CREATED),DF_Constants.TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_CREATED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_REQUEST,'',DF_Constants.SERVICE_REQUEST_NOTIFIC_TYPE_NBN_ACTION_REQUIRED),DF_Constants.TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_NBN_ACTION_REQUIRED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_REQUEST,'',DF_Constants.SERVICE_REQUEST_NOTIFIC_TYPE_NBN_ACTION_COMPLETED),DF_Constants.TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_NBN_ACTION_COMPLETED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_REQUEST,'',DF_Constants.SERVICE_REQUEST_NOTIFIC_TYPE_TICKET_UPDATED_NBNCO),DF_Constants.TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_TICKET_UPDATED_NBNCO);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_REQUEST,'',DF_Constants.SERVICE_REQUEST_NOTIFIC_TYPE_RESOLVED),DF_Constants.TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_RESOLVED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_REQUEST,'',DF_Constants.SERVICE_REQUEST_NOTIFIC_TYPE_CLOSED),DF_Constants.TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_CLOSED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_REQUEST,'',DF_Constants.SERVICE_REQUEST_NOTIFIC_TYPE_REJECTED),DF_Constants.TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_REJECTED);
                
        system.assertEquals(DF_AS_GlobalUtility.getEmailTemplateName(DF_Constants.RECORD_TYPE_SERVICE_REQUEST,'',DF_Constants.SERVICE_REQUEST_NOTIFIC_TYPE_ADDITIONAL_INFO),DF_Constants.TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_ADDITIONAL_INFO);
        
        Test.stopTest();
    }
}