/*----------------------------------------------------------------------------------------------------------
Author:        Vineeth Reddy
Description:   This class provides incident and order search capability to lightning components
Test Class:    DF_AS_Incident_SearchController_LC_Test

History
<Date>      <Authors Name>     <Brief Description of Change>
----------------------------------------------------------------------------------------------------------*/

public with sharing class DF_AS_Incident_SearchController_LC {

    private static final Integer INCIDENT_RESULT_SIZE = 200;
    private static final String INCIDENT_TYPE_SERVICE_INCIDENT ='Service Incident';
    private static final String INCIDENT_TYPE_NETWORK_INCIDENT ='Network Incident';
    private static final String INCIDENT_TYPE_SERVICE_ALERT ='Service Alert';
    private static final String INCIDENT_TYPE_CHANGE_REQUEST ='Change Request';
	private static final String INCIDENT_TYPE_SERVICE_REQUEST ='Service Request';
    private static final String INCIDENT_TYPE_ALL ='ALL';

    private static final List<User> currentUser = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];

    @AuraEnabled
    public static List<DF_Order__c> searchOrders(String productIdentifier) {

        String userAccountId = currentUser.get(0).AccountId;
        List<DF_Order__c> orders = new List<DF_Order__c>();
        
        if(productIdentifier != null && productIdentifier != '') {
            System.debug('!@#$% Searching orders with : ' + productIdentifier);
            orders = [SELECT Id,
                         BPI_Id__c,
                         Order_Id__c,
                         Order_Submitted_Date__c,
                         DF_Quote__r.Location_Id__c,
                         DF_Quote__r.Address__c
                  FROM
                         DF_Order__c
                  WHERE
                         BPI_Id__c != null AND
                         BPI_Id__c != '' AND
                         BPI_Id__c = :productIdentifier AND						 
						 Opportunity_Bundle__r.Account__c = :userAccountId AND 
                         Order_Status__c = 'Complete' 
                  ORDER BY
                         CreatedDate DESC];    
        }
        
        System.debug('!@#$% Orders found : ' + orders);
        return orders;
    }

    @AuraEnabled
    public static DF_SvcCacheSearchResults searchServiceCache(String productIdentifier) {

        DF_SvcCacheSearchResults svCacheResult = null;
        if(productIdentifier != null 
                && productIdentifier.toLowerCase().startsWithIgnoreCase('bpi')) {
            svCacheResult = DF_AS_BaseController_LC.searchServiceCache(productIdentifier);
        }
        
        System.debug('svCacheResult >>> ' + svCacheResult);
        
        return svCacheResult;
    }

    @AuraEnabled
    public static List<Incident_Management__c> searchIncidents(String identifier, String incidentType, String incidentStatus, String dateRaisedFrom, String dateRaisedTo, String state, String serviceRestorationSla) {
        
        List<Incident_Management__c> searchResponse = new List<Incident_Management__c>();

        if(incidentType == null || incidentType == '' || incidentType == INCIDENT_TYPE_ALL) {
            
            List<Incident_Management__c> serviceIncidents = searchServiceIncidents(identifier, INCIDENT_TYPE_SERVICE_INCIDENT, incidentStatus, dateRaisedFrom, dateRaisedTo, state, serviceRestorationSla);       
            List<Incident_Management__c> networkIncidents = searchOtherIncidents(identifier, INCIDENT_TYPE_NETWORK_INCIDENT, incidentStatus, dateRaisedFrom, dateRaisedTo, state);
            List<Incident_Management__c> serviceAlerts = searchOtherIncidents(identifier, INCIDENT_TYPE_SERVICE_ALERT, incidentStatus, dateRaisedFrom, dateRaisedTo, state);
            List<Incident_Management__c> changeRequests = searchOtherIncidents(identifier, INCIDENT_TYPE_CHANGE_REQUEST, incidentStatus, dateRaisedFrom, dateRaisedTo, state);
			List<Incident_Management__c> serviceRequests = searchServiceRequests(identifier, INCIDENT_TYPE_SERVICE_REQUEST, incidentStatus, dateRaisedFrom, dateRaisedTo, state);

            if(serviceIncidents != null && serviceIncidents.size()>0)
                searchResponse.addAll(serviceIncidents);
            if(networkIncidents != null && networkIncidents.size()>0)
                searchResponse.addAll(networkIncidents);
            if(serviceAlerts != null && serviceAlerts.size()>0)
                searchResponse.addAll(serviceAlerts);
            if(changeRequests != null && changeRequests.size()>0)
                searchResponse.addAll(changeRequests);
			if(serviceRequests != null && serviceRequests.size()>0)
                searchResponse.addAll(serviceRequests);
        }

        else if(incidentType.equalsIgnoreCase(INCIDENT_TYPE_SERVICE_INCIDENT)) {
            searchResponse = searchServiceIncidents(identifier, incidentType, incidentStatus, dateRaisedFrom, dateRaisedTo, state, serviceRestorationSla);
        }

        else if(incidentType.equalsIgnoreCase(INCIDENT_TYPE_NETWORK_INCIDENT)) {
            searchResponse = searchOtherIncidents(identifier, incidentType, incidentStatus, dateRaisedFrom, dateRaisedTo, state);
        }

        else if(incidentType.equalsIgnoreCase(INCIDENT_TYPE_SERVICE_ALERT)) {
            searchResponse = searchOtherIncidents(identifier, incidentType, incidentStatus, dateRaisedFrom, dateRaisedTo, state);
        }

        else if(incidentType.equalsIgnoreCase(INCIDENT_TYPE_CHANGE_REQUEST)) {
            searchResponse = searchOtherIncidents(identifier, incidentType, incidentStatus, dateRaisedFrom, dateRaisedTo, state);
        }

		else if(incidentType.equalsIgnoreCase(INCIDENT_TYPE_SERVICE_REQUEST)) {
            searchResponse = searchServiceRequests(identifier, incidentType, incidentStatus, dateRaisedFrom, dateRaisedTo, state);
        }

        return searchResponse;

    }

    private static List<Incident_Management__c> searchServiceIncidents(String identifier, String incidentType, String incidentStatus, String dateRaisedFrom, String dateRaisedTo, String state, String serviceRestorationSla) {
        
        String userAccountId = currentUser.get(0).AccountId;

        List<Incident_Management__c> incidents = new List<Incident_Management__c>();
        List<Incident_Management__c> searchResponse = new List<Incident_Management__c>();

        String searchQuery = 'SELECT Id, PRI_ID__c, Incident_Identifier__c, Incident_Number__c, Incident_Type__c, RecordType.Name, CreatedDate, toLabel(Industry_Status__c), DF_Order__c, DF_Order__r.Opportunity_Bundle__r.Account__c FROM Incident_Management__c';

        //construct filters
        List<String> filters = new List<String>();

        if(userAccountId != null)
            filters.add('DF_Order__r.Opportunity_Bundle__r.Account__c = \'' + userAccountId+'\'');

        if(identifier != null && identifier != '') {
            filters.add('(PRI_ID__c = \'' +identifier+ '\' OR Incident_Number__c = \'' +identifier+ '\')');
        }

        if(incidentType != null && incidentType != '') {
            filters.add('RecordType.Name = \'' + incidentType+'\'');
        }

        if(incidentStatus != null && incidentStatus != '') {
            filters.add('Industry_Status__c = \'' + incidentStatus+'\'');
        }

        System.debug('!@#$% dateRaisedFrom : ' + dateRaisedFrom);
        if(dateRaisedFrom != null && dateRaisedFrom != '') {
            DateTime utcDateTimeRaisedFrom = DateTime.newInstance(Integer.valueOf(dateRaisedFrom.split('-')[0]),Integer.valueOf(dateRaisedFrom.split('-')[1]),Integer.valueOf(dateRaisedFrom.split('-')[2]),0,0,0);
            String utcDateTimeRaisedFromGmt = utcDateTimeRaisedFrom.formatGmt('YYYY-MM-dd') +'T'+ utcDateTimeRaisedFrom.timeGmt();
            System.debug('!@#$% utcDateTimeRaisedFrom Date : ' + utcDateTimeRaisedFrom);
            System.debug('!@#$% utcDateTimeRaisedFromGmt Date : ' + utcDateTimeRaisedFromGmt);

            filters.add('CreatedDate >= ' + utcDateTimeRaisedFromGmt);
        }

        System.debug('!@#$% dateRaisedTo : ' + dateRaisedTo);
        if(dateRaisedTo != null && dateRaisedTo != '') {
            DateTime utcDateTimeRaisedTo = DateTime.newInstance(Integer.valueOf(dateRaisedTo.split('-')[0]),Integer.valueOf(dateRaisedTo.split('-')[1]),Integer.valueOf(dateRaisedTo.split('-')[2]),23,59,59);
            String utcDateTimeRaisedToGmt = utcDateTimeRaisedTo.formatGmt('YYYY-MM-dd') +'T'+ utcDateTimeRaisedTo.timeGmt();
            System.debug('!@#$% utcDateTimeRaisedTo Date : ' + utcDateTimeRaisedTo);
            System.debug('!@#$% utcDateTimeRaisedToGmt Date : ' + utcDateTimeRaisedToGmt);

            filters.add('CreatedDate <= ' + utcDateTimeRaisedToGmt);    
        }

        if(serviceRestorationSla != null && serviceRestorationSla != '') {
            filters.add('serviceRestorationSLA__c = \'' + serviceRestorationSla+'\'');
        }
        filters.add('RecordTypeId != null');

        //apply filters
        if(filters.size() > 0) {
            searchQuery = searchQuery+' WHERE';
            for(Integer f=0; f<filters.size(); f++) {
                searchQuery = searchQuery + ' ' + filters.get(f);
                if(f != filters.size()-1) {
                    searchQuery = searchQuery + ' AND';
                }
            }
        }

        //apply ordering
        searchQuery = searchQuery + ' ORDER BY CreatedDate DESC';

        //apply limits
        searchQuery = searchQuery + ' LIMIT ' + INCIDENT_RESULT_SIZE;

        System.debug('!@#$% Incident Search Query : ' + searchQuery);
        
        //execute query
        searchResponse = Database.query(searchQuery);
        
        incidents = searchResponse;
        System.debug('!@#$% Incidents found : ' + incidents);
        return incidents;
    }

	private static List<Incident_Management__c> searchServiceRequests(String identifier, String incidentType, String incidentStatus, String dateRaisedFrom, String dateRaisedTo, String state) {
        
		List<Incident_Management__c> incidents = new List<Incident_Management__c>();
        List<Incident_Management__c> searchResponse = new List<Incident_Management__c>();

		//First check if search service request feature is toggled off
		DF_AS_Custom_Settings__c customSettingVal = DF_AS_Custom_Settings__c.getValues('Feature_Search_Service_Request');
        if(customSettingVal != null) {
            if(!customSettingVal.IsEnabled__c) return incidents;
        }

		User user = [SELECT Account.Access_seeker_id__c FROM User where id = :UserInfo.getUserId() LIMIT 1];
        String accessSeekerId = user.Account.Access_seeker_id__c;

        
        String searchQuery = 'SELECT Id, PRI_ID__c, Incident_Identifier__c, Incident_Number__c, Incident_Type__c, RecordType.Name, CreatedDate, toLabel(Industry_Status__c) FROM Incident_Management__c';

        //construct filters
        List<String> filters = new List<String>();

        if(accessSeekerId != null)
            filters.add('Access_Seeker_ID__c = \'' + accessSeekerId+'\'');

        if(identifier != null && identifier != '') {
            filters.add('(PRI_ID__c = \'' +identifier+ '\' OR Incident_Number__c = \'' +identifier+ '\')');
        }

        if(incidentType != null && incidentType != '') {
            filters.add('RecordType.Name = \'' + incidentType+'\'');
        }

        if(incidentStatus != null && incidentStatus != '') {
            filters.add('Industry_Status__c = \'' + incidentStatus+'\'');
        }

        System.debug('!@#$% dateRaisedFrom : ' + dateRaisedFrom);
        if(dateRaisedFrom != null && dateRaisedFrom != '') {
            DateTime utcDateTimeRaisedFrom = DateTime.newInstance(Integer.valueOf(dateRaisedFrom.split('-')[0]),Integer.valueOf(dateRaisedFrom.split('-')[1]),Integer.valueOf(dateRaisedFrom.split('-')[2]),0,0,0);
            String utcDateTimeRaisedFromGmt = utcDateTimeRaisedFrom.formatGmt('YYYY-MM-dd') +'T'+ utcDateTimeRaisedFrom.timeGmt();
            System.debug('!@#$% utcDateTimeRaisedFrom Date : ' + utcDateTimeRaisedFrom);
            System.debug('!@#$% utcDateTimeRaisedFromGmt Date : ' + utcDateTimeRaisedFromGmt);

            filters.add('CreatedDate >= ' + utcDateTimeRaisedFromGmt);
        }

        System.debug('!@#$% dateRaisedTo : ' + dateRaisedTo);
        if(dateRaisedTo != null && dateRaisedTo != '') {
            DateTime utcDateTimeRaisedTo = DateTime.newInstance(Integer.valueOf(dateRaisedTo.split('-')[0]),Integer.valueOf(dateRaisedTo.split('-')[1]),Integer.valueOf(dateRaisedTo.split('-')[2]),23,59,59);
            String utcDateTimeRaisedToGmt = utcDateTimeRaisedTo.formatGmt('YYYY-MM-dd') +'T'+ utcDateTimeRaisedTo.timeGmt();
            System.debug('!@#$% utcDateTimeRaisedTo Date : ' + utcDateTimeRaisedTo);
            System.debug('!@#$% utcDateTimeRaisedToGmt Date : ' + utcDateTimeRaisedToGmt);

            filters.add('CreatedDate <= ' + utcDateTimeRaisedToGmt);    
        }
        
        filters.add('RecordTypeId != null');

        //apply filters
        if(filters.size() > 0) {
            searchQuery = searchQuery+' WHERE';
            for(Integer f=0; f<filters.size(); f++) {
                searchQuery = searchQuery + ' ' + filters.get(f);
                if(f != filters.size()-1) {
                    searchQuery = searchQuery + ' AND';
                }
            }
        }

        //apply ordering
        searchQuery = searchQuery + ' ORDER BY CreatedDate DESC';

        //apply limits
        searchQuery = searchQuery + ' LIMIT ' + INCIDENT_RESULT_SIZE;

        System.debug('!@#$% Service Request Search Query : ' + searchQuery);
        
        //execute query
        searchResponse = Database.query(searchQuery);
        
        incidents = searchResponse;
        System.debug('!@#$% Service Requests found : ' + incidents);
        return incidents;
    }

    private static List<Incident_Management__c> searchOtherIncidents(String identifier, String incidentType, String incidentStatus, String dateRaisedFrom, String dateRaisedTo, String state) {

        List<Incident_Management__c> incidents = new List<Incident_Management__c>();
        List<Incident_Management__c> searchResponse = new List<Incident_Management__c>();
        List<EE_AS_Incident_Account_Relationship__c> filterResponse = new List<EE_AS_Incident_Account_Relationship__c>();

        User user = [SELECT Account.Access_seeker_id__c FROM User where id = :UserInfo.getUserId() LIMIT 1];        
        String accessSeekerId = user.Account.Access_seeker_id__c;

        String filterQuery = 'SELECT Id, Related_Incident__c FROM EE_AS_Incident_Account_Relationship__c';

        //construct filters
        List<String> filters = new List<String>();

        if(identifier != null && identifier != '') {
            filters.add('(BPI_Id__c = \'' +identifier+ '\' OR Related_Incident__r.Incident_Number__c = \'' +identifier+ '\')');
        }

        if(incidentType != null && incidentType != '') {
            filters.add('Related_Incident__r.RecordType.Name = \'' + incidentType+'\'');
        }

        if(accessSeekerId != null && accessSeekerId != '') {
            filters.add('Access_Seeker_ID__c = \'' + accessSeekerId+'\'');
        }


        if(incidentStatus != null && incidentStatus != '') {
            filters.add('Related_Incident__r.Industry_Status__c = \'' + incidentStatus+'\'');
        }

        System.debug('!@#$% dateRaisedFrom : ' + dateRaisedFrom);
        if(dateRaisedFrom != null && dateRaisedFrom != '') {
            DateTime utcDateTimeRaisedFrom = DateTime.newInstance(Integer.valueOf(dateRaisedFrom.split('-')[0]),Integer.valueOf(dateRaisedFrom.split('-')[1]),Integer.valueOf(dateRaisedFrom.split('-')[2]),0,0,0);
            String utcDateTimeRaisedFromGmt = utcDateTimeRaisedFrom.formatGmt('YYYY-MM-dd') +'T'+ utcDateTimeRaisedFrom.timeGmt();
            System.debug('!@#$% utcDateTimeRaisedFrom Date : ' + utcDateTimeRaisedFrom);
            System.debug('!@#$% utcDateTimeRaisedFromGmt Date : ' + utcDateTimeRaisedFromGmt);

            filters.add('Related_Incident__r.CreatedDate >= ' + utcDateTimeRaisedFromGmt);
        }

        System.debug('!@#$% dateRaisedTo : ' + dateRaisedTo);
        if(dateRaisedTo != null && dateRaisedTo != '') {
            DateTime utcDateTimeRaisedTo = DateTime.newInstance(Integer.valueOf(dateRaisedTo.split('-')[0]),Integer.valueOf(dateRaisedTo.split('-')[1]),Integer.valueOf(dateRaisedTo.split('-')[2]),23,59,59);
            String utcDateTimeRaisedToGmt = utcDateTimeRaisedTo.formatGmt('YYYY-MM-dd') +'T'+ utcDateTimeRaisedTo.timeGmt();
            System.debug('!@#$% utcDateTimeRaisedTo Date : ' + utcDateTimeRaisedTo);
            System.debug('!@#$% utcDateTimeRaisedToGmt Date : ' + utcDateTimeRaisedToGmt);

            filters.add('Related_Incident__r.CreatedDate <= ' + utcDateTimeRaisedToGmt);    
        }

        filters.add('Related_Incident__r.RecordTypeId != null');

        //apply filters
        if(filters.size() > 0) {
            filterQuery = filterQuery+' WHERE';
            for(Integer f=0; f<filters.size(); f++) {
                filterQuery = filterQuery + ' ' + filters.get(f);
                if(f != filters.size()-1) {
                    filterQuery = filterQuery + ' AND';
                }
            }
        }

        //apply limits
        filterQuery = filterQuery + ' LIMIT ' + INCIDENT_RESULT_SIZE;

        System.debug('!@#$% Incident Filter Query : ' + filterQuery);

        //execute query
        filterResponse = Database.query(filterQuery);
        filters = new List<String>();
        Set<String> filterByIncidentIds = new Set<String>();
        if(filterResponse != null && !filterResponse.isEmpty()) {
            for(EE_AS_Incident_Account_Relationship__c incidentAccRel : filterResponse) {
                filterByIncidentIds.add((String)incidentAccRel.Related_Incident__c);
            }
        }
        System.debug(' **%%^^   filterByIncidentIds   ' + filterByIncidentIds);
        
        String searchQuery = 'SELECT Id, (SELECT BPI_Id__c FROM Incident_Account_Relationships__r  where Access_Seeker_ID__c = :accessSeekerId), Incident_Identifier__c, Incident_Number__c, Incident_Type__c, RecordType.Name, CreatedDate, toLabel(Industry_Status__c), DF_Order__c, DF_Order__r.Opportunity_Bundle__r.Account__c, Impacted_Services__c FROM Incident_Management__c';
        
        if(!filterByIncidentIds.isEmpty()) {
            searchQuery = searchQuery+' WHERE Id IN :filterByIncidentIds AND RecordType.Name =:incidentType ORDER BY CreatedDate DESC' ;
            //execute query
            searchResponse = Database.query(searchQuery);
        }
        incidents = searchResponse;
        System.debug('!@#$% Incidents found : ' + incidents);
        return incidents;
    }
}