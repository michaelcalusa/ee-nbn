@isTest
private class DF_AS_Incident_SearchController_LC_Test {
    
    @testSetup static void setupTestData () {

        // create account record
        Account account = new Account();
        account.Name = 'Incident Search Account';
        insert account;

        // create opportunity bundle record
        DF_Opportunity_Bundle__c oppBundle = new DF_Opportunity_Bundle__c();
        oppBundle.Account__c = account.Id;
        insert oppBundle;
        
        // create quote record
        DF_Quote__c quote = new DF_Quote__c();
        quote.Opportunity_Bundle__c = oppBundle.Id;
        quote.Location_Id__c = 'LOC112233441';
        quote.Address__c = '380 Dockloands Drive, Docklands, VIC 3000';
        insert quote;

        // create order record
        DF_Order__c order = new DF_Order__c();
        order.Opportunity_Bundle__c = oppBundle.Id;
        order.BPI_Id__c = '12345';
        order.Order_Submitted_Date__c = System.today();
        order.DF_Quote__c = quote.Id;
        insert order;

        // create service incident record
        Incident_Management__c serviceIncident = new Incident_Management__c();
        serviceIncident.PRI_ID__c = 'SI12345';
        serviceIncident.Incident_Number__c = 'SI67890'; 
        serviceIncident.Incident_Type__c = 'Service Incident'; 
        serviceIncident.Industry_Status__c = 'Acknowledged';
        serviceIncident.serviceRestorationSLA__c = 'Standard';
        serviceIncident.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Service Incident').getRecordTypeId();
        insert serviceIncident;

        // create service alert record
        Incident_Management__c serviceAlert = new Incident_Management__c();
        serviceAlert.PRI_ID__c = 'SA12345';
        serviceAlert.Incident_Number__c = 'SA67890'; 
        serviceAlert.Incident_Type__c = 'Service Alert'; 
        serviceAlert.Industry_Status__c = 'Acknowledged';
        serviceAlert.serviceRestorationSLA__c = 'Standard';
        serviceAlert.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Service Alert').getRecordTypeId();
        insert serviceAlert;

        // create network incident record
        Incident_Management__c networkIncident = new Incident_Management__c();
        networkIncident.PRI_ID__c = 'NI12345';
        networkIncident.Incident_Number__c = 'NI67890'; 
        networkIncident.Incident_Type__c = 'Network Incident'; 
        networkIncident.Industry_Status__c = 'Acknowledged';
        networkIncident.serviceRestorationSLA__c = 'Standard';
        networkIncident.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Network Incident').getRecordTypeId();
        insert networkIncident;

        // create change request record
        Incident_Management__c changeRequest = new Incident_Management__c();
        changeRequest.PRI_ID__c = 'CRQ12345';
        changeRequest.Incident_Number__c = 'CRQ67890'; 
        changeRequest.Incident_Type__c = 'Change Request'; 
        changeRequest.Industry_Status__c = 'Acknowledged';
        changeRequest.serviceRestorationSLA__c = 'Standard';
        changeRequest.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Change Request').getRecordTypeId();
        insert changeRequest;

        EE_AS_Incident_Account_Relationship__c incidentAccountServAl = new EE_AS_Incident_Account_Relationship__c();
        incidentAccountServAl.Related_Incident__c = serviceAlert.Id;
        incidentAccountServAl.Related_Account__c = account.Id;
        insert incidentAccountServAl;

        EE_AS_Incident_Account_Relationship__c incidentAccountNwInc = new EE_AS_Incident_Account_Relationship__c();
        incidentAccountNwInc.Related_Incident__c = networkIncident.Id;
        incidentAccountNwInc.Related_Account__c = account.Id;
        insert incidentAccountNwInc;

        EE_AS_Incident_Account_Relationship__c incidentAccountChngReq = new EE_AS_Incident_Account_Relationship__c();
        incidentAccountChngReq.Related_Incident__c = changeRequest.Id;
        incidentAccountChngReq.Related_Account__c = account.Id;
        insert incidentAccountChngReq;

    }

    @isTest static void searchOrders_noResult() {
        Test.startTest();
        String productIdentifier = '67890';
        List<DF_Order__c> orders = DF_AS_Incident_SearchController_LC.searchOrders(productIdentifier);
        Test.stopTest();

        System.assertEquals(0, orders.size());
    }

    @isTest static void searchOrders_noResultAccessSeekerNotMatch() {
        Test.startTest();
        String productIdentifier = '12345';
        List<DF_Order__c> orders = DF_AS_Incident_SearchController_LC.searchOrders(productIdentifier);
        Test.stopTest();

        System.assertEquals(0, orders.size());
    }

    @isTest static void searchIncidents_noResult() {
        Test.startTest();
        String productIdentifier = '99999';
        List<Incident_Management__c> incidents = DF_AS_Incident_SearchController_LC.searchIncidents(productIdentifier,null,null,null,null,null,null);
        Test.stopTest();

        System.assertEquals(0, incidents.size());
    }

    @isTest static void searchIncidents_byProductIdentifier() {
        Test.startTest();
        String productIdentifier = 'SI12345';
        List<Incident_Management__c> incidents = DF_AS_Incident_SearchController_LC.searchIncidents(productIdentifier,null,null,null,null,null,null);
        Test.stopTest();

        System.assertEquals(1, incidents.size());
    }

    @isTest static void searchIncidents_byIncidentNumber() {
        Test.startTest();
        String incidentNumber = 'SI67890';
        List<Incident_Management__c> incidents = DF_AS_Incident_SearchController_LC.searchIncidents(incidentNumber,null,null,null,null,null,null);
        Test.stopTest();

        System.assertEquals(1, incidents.size());
    }

    @isTest static void searchIncidents_bydateRange() {
        Test.startTest();
        String incidentNumber = 'SI67890';
        String fromDate = '2018-03-31';
        String toDate = '2050-03-31';
        List<Incident_Management__c> incidents = DF_AS_Incident_SearchController_LC.searchIncidents(incidentNumber,null,null,fromDate,toDate,null,null);
        Test.stopTest();

        System.assertEquals(1, incidents.size());
    }

    @isTest static void searchIncidents_byServiceSLA() {
        Test.startTest();
        String serviceRestorationSLA = 'Standard';
        String incidentNumber = 'SI67890';
        List<Incident_Management__c> incidents = DF_AS_Incident_SearchController_LC.searchIncidents(incidentNumber,null,null,null,null,null,serviceRestorationSLA);
        Test.stopTest();

        System.assertEquals(1, incidents.size());
    }  

    @isTest static void searchIncidents_byIncidentStatus() {
        Test.startTest();
        String incidentNumber = 'SI67890';
        String incidentStatus = 'Acknowledged';
        List<Incident_Management__c> incidents = DF_AS_Incident_SearchController_LC.searchIncidents(incidentNumber,null,incidentStatus,null,null,null,null);
        Test.stopTest();

        System.assertEquals(1, incidents.size());
    }

    @isTest static void searchIncidents_byIncidentType_1() {
        Test.startTest();
        String incidentType = 'Service Incident';
        List<Incident_Management__c> incidents = DF_AS_Incident_SearchController_LC.searchIncidents(null,incidentType,null,null,null,null,null);
        Test.stopTest();

        System.assertEquals(1, incidents.size());
    }

    @isTest static void searchIncidents_byIncidentType_2() {
        Test.startTest();
        String incidentType = 'Service Alert';
        List<Incident_Management__c> incidents = DF_AS_Incident_SearchController_LC.searchIncidents(null,incidentType,null,null,null,null,null);
        Test.stopTest();

        System.assertEquals(1, incidents.size());
    }

    @isTest static void searchIncidents_byIncidentType_3() {
        Test.startTest();
        String incidentType = 'Network Incident';
        List<Incident_Management__c> incidents = DF_AS_Incident_SearchController_LC.searchIncidents(null,incidentType,null,null,null,null,null);
        Test.stopTest();

        System.assertEquals(1, incidents.size());
    }

    @isTest static void searchIncidents_byIncidentType_4() {
        Test.startTest();
        String incidentType = 'Change Request';
        List<Incident_Management__c> incidents = DF_AS_Incident_SearchController_LC.searchIncidents(null,incidentType,null,null,null,null,null);
        Test.stopTest();

        System.assertEquals(1, incidents.size());
    }
    
}