/*------------------------------------------------------------  
Author:        Kai-Fan Hsieh
Description:   This class contains test responses for EE Assurance call outs
History
<Date>      <Authors Name>     <Brief Description of Change>
------------------------------------------------------------*/
@isTest
public class DF_AS_MockHttpResponseGenerator implements HttpCalloutMock {
    public string responseType { get; set; }
    
    public HTTPResponse respond(HTTPRequest req) {
		String response;		
		
		System.debug('@@respond - req.getEndpoint ---> '+req.getEndpoint());
		System.debug('@@respond - responseType ---> '+responseType);

		if(responseType == 'PicklistSuccess')
		{
			response = '[{ "label" : "Assigned", "value" : "Assigned" }, { "label" : "Cancelled", "value" : "Cancelled"	}]';
		}		

        // Create a test response
        HttpResponse res = new HttpResponse();
        res.setBody(response);
        res.setStatusCode(200);
        return res;
    }
}