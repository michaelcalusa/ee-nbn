@isTest
public class DF_AS_TestService {

	@isTest
	//Creates values for EE email custom settings
	public static void createNBNIntegrationInputsCustomSettings() {
		List<NBNIntegrationInputs__c> csList = DF_AS_TestData.createNBNIntegrationInputs();
		if (!csList.isEmpty())
		{
			insert csList;
		}
	}

	@isTest
	//Creates values for EE email custom settings
	public static void createEmailCustomSettings() {
		List<EE_Email_Settings__c> csList = DF_AS_TestData.createEmailCustomSettings();
		if (!csList.isEmpty())
		{
			insert csList;
		}
	}

	@isTest
	//Creates values for EE Assurance custom settings
	public static void createCustomSettingsForAS() {
		List<DF_AS_Custom_Settings__c> csList = DF_AS_TestData.createCustomSettingsForAS();
		if (!csList.isEmpty())
		{
			insert csList;
		}
	}

	@isTest
	//Creates values for EE Assurance custom settings
	public static void createPicklistCustomSettingsForAS() {
		List<DF_AS_Picklist_Values__c> csList = DF_AS_TestData.createPicklistCustomSettingsForAS();
		if (!csList.isEmpty())
		{
			insert csList;
		}
	}
	
}