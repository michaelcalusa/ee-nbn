public class DF_ActiveRequests {
	// Wrapper class to map values to Active Requests in Home screen
	
    @AuraEnabled
    public String userType {get;set;}
    @AuraEnabled
    public Integer sfCountByUser {get;set;}
    @AuraEnabled
    public Integer orderCountByUser {get;set;}
    @AuraEnabled
    public Integer ticketCountByUser {get;set;}
    @AuraEnabled
    public Integer sfCountByAccount {get;set;}
    @AuraEnabled
    public Integer orderCountByAccount {get;set;}
    @AuraEnabled
    public Integer ticketCountByAccount {get;set;}
}