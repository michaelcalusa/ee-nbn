/*----------------------------------------------------------------------------------------------------------
Author:        Michael Calusa
Description:   This class provides service search capability to lightning components
Test Class:    DF_Active_Service_SearchController_Test

History
<Date>      <Authors Name>     <Brief Description of Change>
----------------------------------------------------------------------------------------------------------*/
public with sharing class DF_Active_Service_SearchController {
    
    public static final String BPI_ID = 'BPI';
    public static final String OVC_ID = 'OVC';
    public static final String UNI_ID = 'UNI';
    public static final String USER_NOT_FOUND = 'User record with the RSP account not found';
    
    /**
    * Search service cache controller trough rest call out.
    * @param: String searchTerm
    */
    @AuraEnabled 
    public static DF_SvcCacheSearchResults searchServiceCache(String searchTerm) {
        System.debug('@!# Search Term value is : ' + searchTerm);
        DF_SvcCacheSearchResults svcCacheActiveList = new DF_SvcCacheSearchResults();
        String searchProduct;
        try {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            
            if(userLst.size()>0 && userLst.get(0).AccountId!=null) {
                searchProduct = searchProductBy(searchTerm);
                system.debug('searchProduct:: ' + searchProduct);
                if (String.isNotBlank(searchProduct)) {
                    //get service summary details and ovc details
                    svcCacheActiveList = DF_SvcCacheService.searchServiceCache(searchProduct);
                    system.debug('@!#svcCacheActiveList::: ' + svcCacheActiveList); 
                }
            }else{
                throw new CustomException(USER_NOT_FOUND);
            }
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return svcCacheActiveList;
    }
    
    /**
    * Search service or product by BPI, UNI or OVC
    * @param: String searchTerm
    * @return: String type
    */
    private static String searchProductBy(String searchTerm) {
        String searchString = '';
        if(String.isNotBlank(searchTerm)) {
            if(searchTerm.trim().startsWithIgnoreCase(BPI_ID)){
                searchString = searchTerm;
            } else if (searchTerm.trim().startsWithIgnoreCase(OVC_ID)) {
                searchString = searchTerm;
            } else if (searchTerm.trim().startsWithIgnoreCase(UNI_ID)) {
                searchString = searchTerm;
            }   
        }
        return searchString;
    }
    
    /**
    * Check if there is an in flight modification for BPI ID.
    * @param: String BPIId
    * @return: Boolean true if subscription order status not yet complete or completed
    */
    @AuraEnabled
    public static Boolean isBPIOnModify(String bpiID) {
        Boolean retVal = false;
        if(String.isNotBlank(bpiId)) {
            retVal = EE_OrderUtility.IsModifyInProgress(bpiId); 
        }
        return retVal;
    } 
    
    @AuraEnabled
    public static List<DF_Modify_Order_Attribute_Settings__mdt> getModifyActionList() {
        return [SELECT DeveloperName, MasterLabel,Affected_Component__c,Toggle_Modify_Option__c from DF_Modify_Order_Attribute_Settings__mdt];
    }
     /**
    * Check if modification is allowed for this particular RSP then enable modify button.
    * @return: Boolean true if modification is allowed
    */
    @AuraEnabled
    public static Boolean isModifyAllowed() {
        Boolean retVal=false;
                
        Id AccountId=DF_SF_CaptureController.getCommUserAccountId();
        Map<String,String> accessSeekerList=new Map<String,String>();
        System.debug('##Account ID'+AccountId);
        if(AccountId <> null) {
            Account acc=[select id,access_seeker_Id__c from account where id=:AccountId AND access_seeker_Id__c!=null LIMIT 1];
            System.debug('##Account'+acc);

            if(acc != null) {
                
                if(EE_RSPAllowedToModify__c.getAll().size() > 0)
                {
                    for(EE_RSPAllowedToModify__c cs:EE_RSPAllowedToModify__c.getAll().values())
                    {
                        accessSeekerList.put(cs.name,cs.name);
                    }
                }
                
               
                
                if(accessSeekerList.containsKey(acc.access_seeker_id__c)) {
                    System.debug('##Condition Mtched');
                    retVal=true;
                }
            }
        }
        return retVal;
        
        } 
    
}