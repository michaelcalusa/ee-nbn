@isTest 
private class DF_Active_Service_SearchController_Test {
    
    private static String buildServiceCacheRespSuccess() {
        String responseBody = '[{"data":{"orderItemId":"211","nni":{"csa":"CSA180000002222","nni_mode":"B","nasId":"ASI000000151582","subscriber_id":"246","nni_lag_id":"104","poi":"1-MEL-MELBOURNE-MP","id":"NNI090000016341","type":"nni","access_seeker_id":"ASI000000151646"},"poi":"1-MEL-MELBOURNE-MP","type":"OVC","cosMappingMode":"PCP","inniSvlanId":"2763","samInstanceId":"SAM","uni":{"interfaceType":"Copper (Auto-negotiate)","tpId":"0x9100","transientId":"1","id":"UNI900000002986","type":"UNIE","portId":"4","ovcType":"Access EVPL","interfaceBandwidth":"2GB","samInstanceId":"SAM"},"cosLowBandwidth":"10Mbps","ePipeSites":[{"tunnels":[{"toIpAddress":"192.168.4.67","id":"1","toHostName":"SWEAS0000001DV"},{"toIpAddress":"192.168.4.68","id":"3","toHostName":"SWEAS0000002DV"},{"toIpAddress":"192.168.4.73","id":"15","toHostName":"SWEAS0000005DV"},{"toIpAddress":"192.168.4.74","id":"16","toHostName":"SWEAS0000006DV"}],"nni_mode":"B","role":"EFS","sourceMacAddress":"B8-E7-79-00-00-01","ipAddress":"192.168.4.69","id":"SWEFS0000001DV","mepId":"1","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.67","id":"1","toHostName":"SWEAS0000001DV"},{"toIpAddress":"192.168.4.68","id":"3","toHostName":"SWEAS0000002DV"},{"toIpAddress":"192.168.4.73","id":"15","toHostName":"SWEAS0000005DV"},{"toIpAddress":"192.168.4.74","id":"16","toHostName":"SWEAS0000006DV"}],"nni_mode":"B","role":"EFS","sourceMacAddress":"B8-E7-79-00-00-02","ipAddress":"192.168.4.70","id":"SWEFS0000002DV","mepId":"2","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"1","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"2","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-00-00-01","ipAddress":"192.168.4.67","id":"SWEAS0000001DV","mepId":"3","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"1","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"2","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-00-00-02","ipAddress":"192.168.4.68","id":"SWEAS0000002DV","mepId":"4","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"15","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"16","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-01-00-01","ipAddress":"192.168.4.73","id":"SWEAS0000005DV","mepId":"5","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"15","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"16","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-01-00-02","ipAddress":"192.168.4.74","id":"SWEAS0000006DV","mepId":"6","samInstanceId":"SAM"}],"connectionProfileId":"1","enni_lag":[{"hostName":"SWEFS0000001DV","nni_lag_name":"ALG-02047","id":"ALG-02047","ports":[{"port":"7","daughterCard":"1","shelf":"1","cardSlot":"5"}],"type":"lag"},{"hostName":"SWEFS0000002DV","nni_lag_name":"ALG-02048","id":"ALG-02048","ports":[{"port":"7","daughterCard":"1","shelf":"1","cardSlot":"5"}],"type":"lag"}],"sVLanId":"2739","routeType":"State","uniVLanId":"15-18","priId":"BPI000000011551","maximumFrameSize":"Jumbo (9000 Bytes)"},"id":"OVC000000012814","type":"enterpriseService","relationShips":{"id":"BPI000000011551","data":{"csaId":"CSA180000002222","accessSeekerId":"ASI000000999999","locationId":"LOC123456799999","samId":"2ARM-01","enterpriseEthernetServiceLevelRegion":"Regional Centre","term":"12 months","templateVersion":"1.0","accessTechnologyType":"Fibre","templateId":"TPL900000000001","productType":"EEAS","serviceRestorationSLA":"Enhanced - 12 (24/7)"},"type":"eeasProductInstance","status":"active"},"connectedTo":[{"data":{"portNo":"4"},"id":"SWBTD0009200-1_4","type":"btdPort"},{"data":{"btdType":"Standard (CSAS)","btdMounting":"Rack","physicalName":"3VRB-00-00-BTD-0012","locationId":"LOC123456799999","ipAddress":"10.0.0.1","powerSupply2":"Not Required","powerSupply1":"DC(-d8V to -22V)","subnetMask":"255.255.255.240","type":"BTD","status":"planned"},"id":"SWBTD0009200","type":"btd"},{"data":{"resources":[{"lagId":3,"lagDescription":"ALG-00010","id":"SWAAS9009998","ports":[{"port":3,"slot":4}]},{"lagId":1,"lagDescription":"ALG-00004","id":"SWBTD0009200","ports":[{"port":1,"slot":1}]}]},"id":"LAG000000002695","type":"lag"},{"data":{"physicalName":"3VRB-00-00-AAS-0092","ipAddress":"192.168.99.18"},"id":"SWAAS9009998","type":"aas"},{"data":{"portNo":"4"},"id":"SWBTD0009200-1_4","type":"btdPort"},{"data":{"btdType":"Standard (CSAS)","btdMounting":"Rack","physicalName":"3VRB-00-00-BTD-0012","locationId":"LOC123456799999","ipAddress":"10.0.0.1","powerSupply2":"Not Required","powerSupply1":"DC(-d8V to -22V)","subnetMask":"255.255.255.240","type":"BTD","status":"planned"},"id":"SWBTD0009200","type":"btd"},{"data":{"resources":[{"lagId":3,"lagDescription":"ALG-00010","id":"SWAAS9009998","ports":[{"port":3,"slot":4}]},{"lagId":1,"lagDescription":"ALG-00004","id":"SWBTD0009200","ports":[{"port":1,"slot":1}]}]},"id":"LAG000000002695","type":"lag"},{"data":{"physicalName":"3VRB-00-00-AAS-0092","ipAddress":"192.168.99.18"},"id":"SWAAS9009998","type":"aas"}],"status":"active"},{"data":{"orderItemId":"212","nni":{"csa":"CSA180000002222","nni_mode":"B","nasId":"ASI000000151582","subscriber_id":"246","nni_lag_id":"104","poi":"1-MEL-MELBOURNE-MP","id":"NNI090000016341","type":"nni","access_seeker_id":"ASI000000151646"},"poi":"1-MEL-MELBOURNE-MP","type":"OVC","cosMappingMode":"PCP","inniSvlanId":"2764","samInstanceId":"SAM","uni":{"interfaceType":"Copper (Auto-negotiate)","tpId":"0x9100","transientId":"1","id":"UNI900000002986","type":"UNIE","portId":"4","ovcType":"Access EVPL","interfaceBandwidth":"2GB","samInstanceId":"SAM"},"cosLowBandwidth":"10Mbps","ePipeSites":[{"tunnels":[{"toIpAddress":"192.168.4.67","id":"1","toHostName":"SWEAS0000001DV"},{"toIpAddress":"192.168.4.68","id":"3","toHostName":"SWEAS0000002DV"},{"toIpAddress":"192.168.4.73","id":"15","toHostName":"SWEAS0000005DV"},{"toIpAddress":"192.168.4.74","id":"16","toHostName":"SWEAS0000006DV"}],"nni_mode":"B","role":"EFS","sourceMacAddress":"B8-E7-79-00-00-01","ipAddress":"192.168.4.69","id":"SWEFS0000001DV","mepId":"1","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.67","id":"1","toHostName":"SWEAS0000001DV"},{"toIpAddress":"192.168.4.68","id":"3","toHostName":"SWEAS0000002DV"},{"toIpAddress":"192.168.4.73","id":"15","toHostName":"SWEAS0000005DV"},{"toIpAddress":"192.168.4.74","id":"16","toHostName":"SWEAS0000006DV"}],"nni_mode":"B","role":"EFS","sourceMacAddress":"B8-E7-79-00-00-02","ipAddress":"192.168.4.70","id":"SWEFS0000002DV","mepId":"2","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"1","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"2","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-00-00-01","ipAddress":"192.168.4.67","id":"SWEAS0000001DV","mepId":"3","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"1","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"2","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-00-00-02","ipAddress":"192.168.4.68","id":"SWEAS0000002DV","mepId":"4","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"15","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"16","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-01-00-01","ipAddress":"192.168.4.73","id":"SWEAS0000005DV","mepId":"5","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"15","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"16","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-01-00-02","ipAddress":"192.168.4.74","id":"SWEAS0000006DV","mepId":"6","samInstanceId":"SAM"}],"connectionProfileId":"2","enni_lag":[{"hostName":"SWEFS0000001DV","nni_lag_name":"ALG-02047","id":"ALG-02047","ports":[{"port":"7","daughterCard":"1","shelf":"1","cardSlot":"5"}],"type":"lag"},{"hostName":"SWEFS0000002DV","nni_lag_name":"ALG-02048","id":"ALG-02048","ports":[{"port":"7","daughterCard":"1","shelf":"1","cardSlot":"5"}],"type":"lag"}],"sVLanId":"2740","routeType":"State","uniVLanId":"20-22","priId":"BPI000000011551","maximumFrameSize":"Standard"},"id":"OVC000000012822","type":"enterpriseService","relationShips":{"id":"BPI000000011551","data":{"csaId":"CSA180000002222","accessSeekerId":"ASI000000999999","locationId":"LOC123456799999","samId":"2ARM-01","enterpriseEthernetServiceLevelRegion":"Regional Centre","term":"12 months","templateVersion":"1.0","accessTechnologyType":"Fibre","templateId":"TPL900000000001","productType":"EEAS","serviceRestorationSLA":"Enhanced - 12 (24/7)"},"type":"eeasProductInstance","status":"active"},"connectedTo":[{"data":{"portNo":"4"},"id":"SWBTD0009200-1_4","type":"btdPort"},{"data":{"btdType":"Standard (CSAS)","btdMounting":"Rack","physicalName":"3VRB-00-00-BTD-0012","locationId":"LOC123456799999","ipAddress":"10.0.0.1","powerSupply2":"Not Required","powerSupply1":"DC(-d8V to -22V)","subnetMask":"255.255.255.240","type":"BTD","status":"planned"},"id":"SWBTD0009200","type":"btd"},{"data":{"resources":[{"lagId":3,"lagDescription":"ALG-00010","id":"SWAAS9009998","ports":[{"port":3,"slot":4}]},{"lagId":1,"lagDescription":"ALG-00004","id":"SWBTD0009200","ports":[{"port":1,"slot":1}]}]},"id":"LAG000000002695","type":"lag"},{"data":{"physicalName":"3VRB-00-00-AAS-0092","ipAddress":"192.168.99.18"},"id":"SWAAS9009998","type":"aas"},{"data":{"portNo":"4"},"id":"SWBTD0009200-1_4","type":"btdPort"},{"data":{"btdType":"Standard (CSAS)","btdMounting":"Rack","physicalName":"3VRB-00-00-BTD-0012","locationId":"LOC123456799999","ipAddress":"10.0.0.1","powerSupply2":"Not Required","powerSupply1":"DC(-d8V to -22V)","subnetMask":"255.255.255.240","type":"BTD","status":"planned"},"id":"SWBTD0009200","type":"btd"},{"data":{"resources":[{"lagId":3,"lagDescription":"ALG-00010","id":"SWAAS9009998","ports":[{"port":3,"slot":4}]},{"lagId":1,"lagDescription":"ALG-00004","id":"SWBTD0009200","ports":[{"port":1,"slot":1}]}]},"id":"LAG000000002695","type":"lag"},{"data":{"physicalName":"3VRB-00-00-AAS-0092","ipAddress":"192.168.99.18"},"id":"SWAAS9009998","type":"aas"}],"status":"active"}]';      
        system.debug('responseBody' + responseBody);
        return responseBody;
    }
    
    @isTest 
    private static void test_searchServiceCacheWithUNI() {
        
        String searchTerm = 'UNI900000002986';
        DF_SvcCacheSearchResults result = new DF_SvcCacheSearchResults();
        String response;
        User commUser;
        commUser = DF_TestData.createDFCommUser();
        
        // Generate mock response
        response = buildServiceCacheRespSuccess();
 
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
        
        system.runAs(commUser) {
            Test.startTest();
                result = DF_Active_Service_SearchController.searchServiceCache(searchTerm);
            Test.stopTest();                
        }                                     
        
        system.assertEquals(result.uniId,searchTerm);
    }
    
    @isTest 
    private static void test_searchServiceCacheWithBPI() {
        
        String searchTerm = 'BPI000000011551';
        DF_SvcCacheSearchResults result = new DF_SvcCacheSearchResults();
        String response;
        User commUser;
        commUser = DF_TestData.createDFCommUser();
        
        // Generate mock response
        response = buildServiceCacheRespSuccess();
        
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
        
        system.runAs(commUser) {
            Test.startTest();
                result = DF_Active_Service_SearchController.searchServiceCache(searchTerm);
            Test.stopTest();                
        }
        
        system.assertEquals(result.bpiId, searchTerm);                                    
    }
    
    @isTest 
    private static void test_searchServiceCacheWithOVC() {
        
        String searchTerm = 'OVC000000012814';
        DF_SvcCacheSearchResults result = new DF_SvcCacheSearchResults();
        String response;
        User commUser;
        commUser = DF_TestData.createDFCommUser();
        
        // Generate mock response
        response = buildServiceCacheRespSuccess();
 
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
        
        system.runAs(commUser) {
            Test.startTest();
                result = DF_Active_Service_SearchController.searchServiceCache(searchTerm);
            Test.stopTest();                
        }             
        system.assertEquals(result.ovcs[0].ovcId, searchTerm);                          
    }
    
    @isTest static void IsModifyInProgressMethodTest(){
    
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        
        Opportunity oppo = DF_TestService.getOppRecord(acc.Id);
        
        csord__Subscription__c sub = DF_TestService.createCSSubscriptionRecord(acc.Id, oppo);
        
        Test.startTest();
        Boolean result=DF_Active_Service_SearchController.isBPIOnModify('BPI000000012911');
        System.assertEquals(true, result);
        Test.stopTest();  
    
    }
    @isTest 
    private static void test_getModifyActionList(){
        
        Test.startTest();
        List<DF_Modify_Order_Attribute_Settings__mdt> result = DF_Active_Service_SearchController.getModifyActionList();
        
        Test.stopTest(); 
    
    }
	    @isTest
    private static void test_IsModifiedAllowed(){

        
         User commUser;
        commUser = DF_TestData.createDFCommUser();
        system.runAs(commUser) {
            Test.startTest();
            DF_Active_Service_SearchController.isModifyAllowed();
            Test.stopTest();
        }
    }
}