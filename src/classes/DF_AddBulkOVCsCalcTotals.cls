global class DF_AddBulkOVCsCalcTotals implements Database.Batchable<EE_Bulk_OVCWrapper> {

  List<EE_Bulk_OVCWrapper> ovcDetailList= new List<EE_Bulk_OVCWrapper>();
   public static String pcDefId = DF_CS_API_Util.getCustomSettingValue('SF_PRODUCT_CHARGE_DEFINITION_ID');
  public static String ovcDefId = DF_CS_API_Util.getCustomSettingValue('SF_OVC_DEFINITION_ID');

  public static List<String> ovcDefnLst = new List<String>{'OVC1', 'OVC2', 'OVC3', 'OVC4', 'OVC5', 'OVC6', 'OVC7', 'OVC8'};
//    public Static Map<String, List<String>> nbAttrValMap = new Map<String, List<String>>();
public Static Map<String, Map<String,String>> nbAttrValMap = new Map<String, Map<String,String>>();
public static Map<String, String> nbAttrMap = new Map<String, String>{'OVCId'=>'','CSA'=>''
,'OVCName'=>'','routeType'=>'','coSHighBandwidth'=>'0','coSMediumBandwidth'=>'0'
,'coSLowBandwidth'=>'0','routeRecurring'=>'0','routeRecurring'=>'0','coSRecurring'=>'0','POI'=>''
,'status'=>'Incomplete','mappingMode'=>'PCP','NNIGroupId'=>'','sTag'=>'0','ceVlanId'=>'0'
,'ovcMaxFrameSize'=>'Jumbo (9000 Bytes)'};
public static Integer ovcCntPV = 0;

public DF_AddBulkOVCsCalcTotals(List<EE_Bulk_OVCWrapper> ovcDetailList){
  this.ovcDetailList = ovcDetailList;
}

global Iterable<EE_Bulk_OVCWrapper> start(Database.BatchableContext BC) {
  system.debug('!!! Start time:'+system.now());
  return ovcDetailList;
}

global void execute(Database.BatchableContext BC, List<object> scope) {

  List<EE_Bulk_OVCWrapper> ovcDetail= (List<EE_Bulk_OVCWrapper>)scope;
  try{

    if(ovcDetail[0].ovcNumber ==0){
    	//deleteFirstOVC(ovcDetail[0]);
      deleteAllOVCs(ovcDetail[0].basketId);
      setTermOnUni(ovcDetail[0]);
    }
     //setTermOnUni(ovcDetail[0]);
    EE_Bulk_OVCWrapper ovc = cleanOVCCoSNulls(ovcDetail[0]);
    addOVCs(ovc); 
    calculateTotal(ovc);
  }
  catch(Exception ex){
    updateQuoteStatusToError(ovcDetail[0].quoteId,'Error creating order');
  }
}

@TestVisible private static EE_Bulk_OVCWrapper cleanOVCCoSNulls(EE_Bulk_OVCWrapper ovc){
  if(String.isEmpty(ovc.coSHighBandwidth)){
    ovc.coSHighBandwidth = '0';
  }
  if(String.isEmpty(ovc.coSMediumBandwidth)){
    ovc.coSMediumBandwidth = '0';
  }
  if(String.isEmpty(ovc.coSLowBandwidth)){
    ovc.coSLowBandwidth = '0';
  }

  if(String.isEmpty(ovc.mappingMode)){

  }

  return ovc;
}

global void finish(Database.BatchableContext BC) {
  try {
    if(ovcDetailList.size() > 0) {
     DF_OrderEmailService.sendBulkUploadCompletedEmailToAccountContactMatrix(ovcDetailList[0].quoteId);
   }
 }
 catch(Exception ex) { 
  GlobalUtility.logMessage('Error', DF_AddBulkOVCsCalcTotals.class.getName(), 'finish', null, null, ex.getMessage(), ex.getStackTraceString(), ex, 0);
  system.debug('!!! Error sending email notification:' + ex.getStackTraceString());
}

system.debug('!!! Finish time:'+system.now());

}

public static void setTermOnUni(EE_Bulk_OVCWrapper ovcDetail){
  cscfga__Product_Basket__c basket = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c where id = :ovcDetail.basketId];
  cscfga.API_1.ApiSession apiSessionNew = DF_CS_API_Util.createApiSession(basket);
  apiSessionNew.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = ovcDetail.uniProdConfigId, cscfga__Product_Basket__c = basket.Id));
  cscfga.ProductConfiguration currConfig = apiSessionNew.getConfiguration();

  for(cscfga.Attribute att : currConfig.getAttributes()) {
              if(att.getName() == 'Term'){
                att.setValue(ovcDetail.uniTerm);           
                att.setDisplayValue(ovcDetail.uniTerm);
              }
            }

  apiSessionNew.executeRules();
  cscfga.ValidationResult vr = apiSessionNew.validateConfiguration();
  apiSessionNew.persistConfiguration();

}

public static void addOVCs(EE_Bulk_OVCWrapper ovcDetail){

  cscfga__Product_Basket__c basket = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c where id = :ovcDetail.basketId];
  cscfga.API_1.ApiSession apiSessionNew = DF_CS_API_Util.createApiSession(basket);
  apiSessionNew.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = ovcDetail.uniProdConfigId, cscfga__Product_Basket__c = basket.Id));
  cscfga.ProductConfiguration currConfig = apiSessionNew.getConfiguration();
  
  cscfga.ProductConfiguration relatedConfig =  apiSessionNew.AddRelatedProduct('OVC', ovcDefId);

  for(cscfga.Attribute att : relatedConfig.getAttributes()) {
    if(att.getName() == 'CoS High'){
      att.setValue(ovcDetail.coSHighBandwidth);            
      att.setDisplayValue(ovcDetail.coSHighBandwidth);
    }
    if(att.getName() == 'CoS Medium'){
      att.setValue(ovcDetail.coSMediumBandwidth);            
      att.setDisplayValue(ovcDetail.coSMediumBandwidth); 
    }
    if(att.getName() == 'CoS Low'){
      att.setValue(ovcDetail.coSLowBandwidth);            
      att.setDisplayValue(ovcDetail.coSLowBandwidth);
    }
    if(att.getName() == 'Route Type'){
      att.setValue(ovcDetail.routeType);            
      att.setDisplayValue(ovcDetail.routeType);
    }
    if(att.getName() == 'CSA'){
      att.setValue(ovcDetail.CSA);            
      att.setDisplayValue(ovcDetail.CSA);
    }

  }
//    }
apiSessionNew.executeRules();
cscfga.ValidationResult vr = apiSessionNew.validateConfiguration();
apiSessionNew.persistConfiguration();

      //add ovc JSON

    }

   // public static void deleteFirstOVC(EE_Bulk_OVCWrapper ovcDetail){
   //  List<cscfga__Product_Configuration__c>  pcToBeDeleted = new List<cscfga__Product_Configuration__c>();
   //  List<cscfga__Product_Configuration__c> pcList= [SELECT Id, Name, cscfga__Configuration_Status__c, (SELECT cscfga__Value__c, Name,cscfga__Is_Line_Item__c
   //  FROM cscfga__Attributes__r WHERE NAME IN ('Hidden_TotalCos_Values', 'Total_Routing_Charge_afterDiscount', 'Hidden_TotalCoS_Charges', 
   //  'Hidden_Zone_Charges_AfterDiscount', 'Hidden_DBP_Zone_Discount', 'Zone_Term_Discount_Recurring')) 
   //  FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c =: ovcDetail.basketId
   //  AND (cscfga__Parent_Configuration__c =: ovcDetail.uniProdConfigId OR Id =: ovcDetail.uniProdConfigId)];
   //  for(cscfga__Product_Configuration__c pc :pcList){
   //    if(pc.Name.containsIgnorecase('OVC')){

   //      Integer cosTotalValue = 1;
   //      for(cscfga__Attribute__c attr : pc.cscfga__Attributes__r) {
   //        if(attr.Name.equalsIgnoreCase('Hidden_TotalCos_Values')){
   //          cosTotalValue = Integer.valueOf(attr.cscfga__Value__c);
   //          if(cosTotalValue == 0){
   //            pcToBeDeleted.add(pc);

   //          }
   //        }
   //      }
   //    }
   //  }
   //  if(pcToBeDeleted.size() > 0){
   //    DF_ProductController.removeOVC(ovcDetail.basketId, pcToBeDeleted[0].Id, 'OVC');
   //  }
   //}

   public static void calculateTotal(EE_Bulk_OVCWrapper ovcDetail){
     system.debug('!!! calculateTotal Start');
     
     Decimal coSValues = 0.0; 
     Decimal ovcrecurring  = 0.0; 
     Decimal routerecurring  = 0.0;
     Decimal totalCoSCharges = 0.0;
     Decimal totalOVCSCR = 0.0;
    Decimal zoneCharge = 0.0; //= CS.getAttributeValue('Hidden_Zone_Charges_AfterDiscount_0');
    Decimal zoneDBPDis = 0.0;//CS.getAttributeValue('Hidden_DBP_Zone_Discount_0');
    Decimal zoneTermDis = 0.0;//CS.getAttributeValue('Zone_Term_Discount_Recurring_0');
    Decimal totalOVCCosCharges = 0.0;
    List<cscfga__Product_Configuration__c>  pcToBeDeleted = new List<cscfga__Product_Configuration__c>();


    List<cscfga__Product_Configuration__c> pcList= [SELECT Id, Name, cscfga__Configuration_Status__c, (SELECT cscfga__Value__c, Name,cscfga__Is_Line_Item__c
    FROM cscfga__Attributes__r WHERE NAME IN ('Hidden_TotalCos_Values', 'Total_Routing_Charge_afterDiscount', 'Hidden_TotalCoS_Charges', 
    'Hidden_Zone_Charges_AfterDiscount', 'Hidden_DBP_Zone_Discount', 'Zone_Term_Discount_Recurring')) 
    FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c =: ovcDetail.basketId
    AND (cscfga__Parent_Configuration__c =: ovcDetail.uniProdConfigId OR Id =: ovcDetail.uniProdConfigId)];
    
    system.debug('!!! calculateTotal pcList.size() '+pcList.size() );
    system.debug('!!! calculateTotal pcList '+pcList );
    //if(pcList.size() == ovcDetail.numberOfOVCsForQuote){
      for(cscfga__Product_Configuration__c postValConfigs: pcList){

        if(postValConfigs.Name.containsIgnorecase('OVC')){
                  //get aggregate of OVC Charges for OVC's
                  for(cscfga__Attribute__c postvalAttr : postValConfigs.cscfga__Attributes__r) {
                    if(postvalAttr.Name.equalsIgnoreCase('Hidden_TotalCos_Values')){
                      System.debug('<><>OVC');
                      coSValues += Decimal.valueOf(postvalAttr.cscfga__Value__c);
                    }
                  //  routerecurring =0.0;
                    if(postvalAttr.Name.equalsIgnoreCase('Total_Routing_Charge_afterDiscount')){
                      routerecurring += Decimal.valueOf(postvalAttr.cscfga__Value__c);
                      system.debug('!!!routerecurring '+routerecurring);
                    }

                    if(postvalAttr.Name.equalsIgnoreCase('Hidden_TotalCoS_Charges')){
                      totalCoSCharges += Decimal.valueOf(postvalAttr.cscfga__Value__c);
                    //  routerecurring += Decimal.valueOf(postvalAttr.cscfga__Value__c);
                    // ovcrecurring +=Decimal.valueOf(postvalAttr.cscfga__Value__c)+routerecurring;
                   } 
                 }
               }

               else if(postValConfigs.Name.containsIgnorecase('Direct Fibre - Product Charges')){
                //Get disocounts 
                System.debug('<><>Direct Fibre - Product Charges');
                for(cscfga__Attribute__c postvalAttr : postValConfigs.cscfga__Attributes__r){
                  if(postvalAttr.Name.equalsIgnoreCase('Hidden_Zone_Charges') && postvalAttr.cscfga__Is_Line_Item__c ){
                    if(postvalAttr.cscfga__Value__c!=null ) {
                      System.debug('<><>Hidden_Zone_charges');
                      zoneCharge = Decimal.valueOf(postvalAttr.cscfga__Value__c);
                    }
                  }

                  if(postvalAttr.Name.equalsIgnoreCase('Hidden_DBP_Zone_Discount') && postvalAttr.cscfga__Is_Line_Item__c ){
                    if(postvalAttr.cscfga__Value__c!=null ) {
                      zoneDBPDis = Decimal.valueOf(postvalAttr.cscfga__Value__c);
                    }
                  }
                  system.debug('!!! postvalAttr.Name ' +postvalAttr.Name);
                  if(postvalAttr.Name.equalsIgnoreCase('Zone_Term_Discount_Recurring') && postvalAttr.cscfga__Is_Line_Item__c ){
                   if(postvalAttr.cscfga__Value__c!=null) {
                     zoneTermDis = Decimal.valueOf(postvalAttr.cscfga__Value__c);
                     system.debug('postvalAttr.cscfga__Value__c: '+postvalAttr.cscfga__Value__c);
                   }
                 }
               }
             }
             

           }
           System.debug('!!! OVC_ovcrecurring'+ovcrecurring);
           System.debug('!!! zoneCharge'+zoneCharge);
           System.debug('!!! zoneDBPDis'+zoneDBPDis);
           System.debug('!!! zoneTermDis'+zoneTermDis);
           totalOVCCosCharges = totalCoSCharges + zoneCharge - zoneDBPDis - zoneTermDis;
           totalOVCSCR = routerecurring + totalCoSCharges;
           System.debug('<><>totalOVCCosCharges'+totalOVCCosCharges);

           if(String.isNotBlank(ovcDetail.basketId) && String.isNotBlank(ovcDetail.uniProdConfigId)){
            cscfga__Product_Basket__c basket = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c where id = :ovcDetail.basketId];
            cscfga.API_1.ApiSession apiSession = DF_CS_API_Util.createApiSession(basket);
            apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = ovcDetail.uniProdConfigId, cscfga__Product_Basket__c = basket.Id));        
            cscfga.ProductConfiguration currConfig = apiSession.getConfiguration();
            System.debug('<><><><><><>'+'I am running');
            for(cscfga.Attribute att : currConfig.getAttributes()) {
              if(att.getName() == 'Hidden_OVC_Charges'){
                att.setValue(String.valueOf(totalOVCSCR)); 
                att.setDisplayValue(String.valueOf(totalOVCSCR));
              
              //  att.setValue(String.valueOf(ovcrecurring)); 
              //  att.setDisplayValue(String.valueOf(ovcrecurring));
              }
              if(att.getName() == 'Hidden_Total_OVC_BWs'){
                att.setValue(String.valueOf(coSValues));
                att.setDisplayValue(String.valueOf(coSValues));
              }
              if(att.getName() == 'TotalOVCCoSChargesNew'){
                att.setValue(String.valueOf(totalOVCCosCharges));
                att.setDisplayValue(String.valueOf(totalOVCCosCharges));
              }
              if(att.getName() == 'After Hours Site Visit'){
                att.setValue(ovcDetail.uniAfterHours);            
                att.setDisplayValue(ovcDetail.uniAfterHours);
              }
              if(att.getName() == 'eSLA'){
                att.setValue(ovcDetail.unieSLA);          
                att.setDisplayValue(ovcDetail.unieSLA);
              }
              if(att.getName() == 'Term'){
                att.setValue(ovcDetail.uniTerm);           
                att.setDisplayValue(ovcDetail.uniTerm);
              }
            }
            apiSession.executeRules(); 
            apiSession.validateConfiguration();
            apiSession.persistConfiguration();
          }
          system.debug('!!!!ovcDetail.ovcNumber: '+ovcDetail.ovcNumber+' ovcDetail.numberOfOVCsForQuote: '+ovcDetail.numberOfOVCsForQuote);

          if(ovcDetail.ovcNumber == ovcDetail.numberOfOVCsForQuote-1 ){
            if(!String.isBlank(ovcDetail.errorStringForQuote)){
                updateQuoteStatusToError(ovcDetail.quoteId,ovcDetail.errorStringForQuote);
              }else{
                updateQuoteStatus(ovcDetail.quoteId);
              }
          }
         system.debug('!!! ovcDetail '+ovcDetail);
         for(cscfga__Product_Configuration__c postValConfigs: [Select id, Name, cscfga__Configuration_Status__c, (select cscfga__Value__c, Name,cscfga__Is_Line_Item__c from cscfga__Attributes__r) from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :ovcDetail.basketId and cscfga__Product_Definition__c = :ovcDefId]){
          if(postValConfigs.Name.containsIgnorecase('OVC')){

           if(postValConfigs.Name.equalsIgnoreCase(ovcDetail.OVCName)){
            system.debug('!!! postValConfigs '+postValConfigs);
            Map<String, String> nbAttrValMapOVC = new Map<String, String>();
            ovcDetail.OVCId = postValConfigs.Id;
            ovcDetail.status = postValConfigs.cscfga__Configuration_Status__c;

            for(cscfga__Attribute__c postvalAttr : postValConfigs.cscfga__Attributes__r) {
             if(postvalAttr.Name.equalsIgnoreCase('Hidden_TotalCoS_Charges')){
              ovcDetail.coSRecurring = postvalAttr.cscfga__Value__c;
            }
            if(postvalAttr.Name.equalsIgnoreCase('Total_Routing_Charge_afterDiscount')){
             ovcDetail.routeRecurring =  postvalAttr.cscfga__Value__c;
           }
           if(postvalAttr.Name.equalsIgnoreCase('POI')){
            ovcDetail.POI = postvalAttr.cscfga__Value__c;
          }
          if(postvalAttr.Name.equalsIgnoreCase('CSA')){
           ovcDetail.CSA =  postvalAttr.cscfga__Value__c;
         }
       }

     }

   }
 }

    updateNBValues(ovcDetail);

 system.debug('!!! calculateTotal Finish');
} 

public static void updateNBValues(EE_Bulk_OVCWrapper ovcDetail){
 DF_Order__c ordObj = [SELECT Id, OVC_NonBillable__c FROM DF_Order__c WHERE DF_Quote__c = :ovcDetail.quoteId];

 String jOrdOVC;
 Boolean ovcExistsInCollection = false;
 DF_OVCWrapper jOrdResp = new DF_OVCWrapper();
 DF_OVCWrapper.OVC ovcToAdd = new DF_OVCWrapper.OVC();

 if(!String.isEmpty(ordObj.OVC_NonBillable__c)){
  jOrdOVC = '{"OVC":' + String.valueOf(ordObj.OVC_NonBillable__c) + '}';
  jOrdResp= DF_OVCWrapper.parse(jOrdOVC);
  
  for(DF_OVCWrapper.OVC ordOVC : jOrdResp.OVC){
    system.debug('!!! updateNBValues ordOVC :'+ordOVC);
    if(ordOVC.OVCName.equalsIgnoreCase(ovcDetail.OVCName)){
					ordOVC.CSA = ovcDetail.CSA; 	//CSA180000002222
					ordOVC.NNIGroupId = ovcDetail.NNIGroupId; 	//NNI123456789123
					ordOVC.routeType = ovcDetail.routeType;	//Local
					ordOVC.coSHighBandwidth = ovcDetail.coSHighBandwidth; 	//250
					ordOVC.coSMediumBandwidth = ovcDetail.coSMediumBandwidth; 	//100
					ordOVC.coSLowBandwidth = ovcDetail.coSLowBandwidth; 	//0
					ordOVC.routeRecurring = ovcDetail.routeRecurring; 	//0.00
					ordOVC.coSRecurring = ovcDetail.coSRecurring; 	//0.00
					ordOVC.ovcMaxFrameSize = ovcDetail.ovcMaxFrameSize; 	//Jumbo (9000 Bytes)
					ordOVC.status = ovcDetail.status; 	//Incomplete
					ordOVC.sTag = ovcDetail.sTag; 	//12
					ordOVC.ceVlanId = ovcDetail.ceVlanId;	//2
					ordOVC.mappingMode = ovcDetail.mappingMode; 	//DSCP
					ordOVC.ovcSystemId = ovcDetail.ovcSystemId;  //OVC000000123456
					ordOVC.POI = ovcDetail.POI;  //3EXH - EXHIBITION
					ovcExistsInCollection = true;
				}
				else{
					
					ovcToAdd.OVCId = ovcDetail.OVCId;	//a1h5D0000006i8YQAQ
					ovcToAdd.OVCName = ovcDetail.OVCName; //OVC 1
					ovcToAdd.CSA = ovcDetail.CSA; 	//CSA180000002222
					ovcToAdd.NNIGroupId = ovcDetail.NNIGroupId; 	//NNI123456789123
					ovcToAdd.routeType = ovcDetail.routeType;	//Local
					ovcToAdd.coSHighBandwidth = ovcDetail.coSHighBandwidth; 	//250
					ovcToAdd.coSMediumBandwidth = ovcDetail.coSMediumBandwidth; 	//100
					ovcToAdd.coSLowBandwidth = ovcDetail.coSLowBandwidth; 	//0
					ovcToAdd.routeRecurring = ovcDetail.routeRecurring; 	//0.00
					ovcToAdd.coSRecurring = ovcDetail.coSRecurring; 	//0.00
					ovcToAdd.ovcMaxFrameSize = ovcDetail.ovcMaxFrameSize; 	//Jumbo (9000 Bytes)
					ovcToAdd.status = ovcDetail.status; 	//Incomplete
					ovcToAdd.sTag = ovcDetail.sTag; 	//12
					ovcToAdd.ceVlanId = ovcDetail.ceVlanId;	//2
					ovcToAdd.mappingMode = ovcDetail.mappingMode; 	//DSCP
					ovcToAdd.ovcSystemId = ovcDetail.ovcSystemId;  //OVC000000123456
					ovcToAdd.POI = ovcDetail.POI;  //3EXH - EXHIBITION
					system.debug('!!! updateNBValues ovcToAdd :'+ovcToAdd);
					ovcExistsInCollection = true;
					
				}
			}
			jOrdResp.OVC.add(ovcToAdd);
   }
   if(!ovcExistsInCollection && !String.isEmpty(ovcDetail.OVCId)){
    jOrdResp.OVC  = new List<DF_OVCWrapper.OVC>();
		ovcToAdd.OVCId = ovcDetail.OVCId;	//a1h5D0000006i8YQAQ
		ovcToAdd.OVCName = ovcDetail.OVCName; //OVC 1
		ovcToAdd.CSA = ovcDetail.CSA; 	//CSA180000002222
		ovcToAdd.NNIGroupId = ovcDetail.NNIGroupId; 	//NNI123456789123
		ovcToAdd.routeType = ovcDetail.routeType;	//Local
		ovcToAdd.coSHighBandwidth = ovcDetail.coSHighBandwidth; 	//250
		ovcToAdd.coSMediumBandwidth = ovcDetail.coSMediumBandwidth; 	//100
		ovcToAdd.coSLowBandwidth = ovcDetail.coSLowBandwidth; 	//0
		ovcToAdd.routeRecurring = ovcDetail.routeRecurring; 	//0.00
		ovcToAdd.coSRecurring = ovcDetail.coSRecurring; 	//0.00
		ovcToAdd.ovcMaxFrameSize = ovcDetail.ovcMaxFrameSize; 	//Jumbo (9000 Bytes)
		ovcToAdd.status = ovcDetail.status; 	//Incomplete
		ovcToAdd.sTag = ovcDetail.sTag; 	//12
		ovcToAdd.ceVlanId = ovcDetail.ceVlanId;	//2
		ovcToAdd.mappingMode = ovcDetail.mappingMode; 	//DSCP
		ovcToAdd.ovcSystemId = ovcDetail.ovcSystemId;  //OVC000000123456
		ovcToAdd.POI = ovcDetail.POI;  //3EXH - EXHIBITION
		system.debug('!!! updateNBValues ovcToAdd :'+ovcToAdd);
		jOrdResp.OVC.add(ovcToAdd);

	}
	String ovcNonBillableNew = JSON.serialize(jOrdResp);
	ovcNonBillableNew = ovcNonBillableNew.removeStart('{"OVC":').removeEnd('}');

	String invalidOVCs = '';
	for(DF_OVCWrapper.OVC ordOVC : jOrdResp.OVC){
    if(ordOVC.status != null){
    system.debug('!!! ordOVC ' +ordOVC);
  		if(!ordOVC.status.equalsIgnoreCase('valid')){
  			invalidOVCs = invalidOVCs+' '+ordOVC.OVCName;
  		}
    }
	}
 if(invalidOVCs.length()>0){
  updateQuoteStatusToError(ovcDetail.quoteId,invalidOVCs);
  
}  

ordObj.OVC_NonBillable__c = ovcNonBillableNew;
update ordObj;
}

public static void updateQuoteStatus(String quoteId){
 if(quoteId!=null){
    DF_Quote__c quoteObjUpd = [SELECT Id, Name, Status__c, Bulk_Order_Error__c FROM DF_Quote__c WHERE Id = :quoteId FOR UPDATE];
    quoteObjUpd.Status__c = 'Ready for Order';
    quoteObjUpd.Bulk_Order_Error__c = '';
    update quoteObjUpd;
  }
}
public static void updateBasketLastTime(set<Id> prodConfigs){
  if(prodConfigs.size()>0){
   // cscfga.ProductConfigurationBulkActions.revalidateConfigurations(prodConfigs);
  }
}
public static void updateQuoteStatusToError(String quoteId, String message){
   if(quoteId!=null){
    DF_Quote__c quoteObjUpd = [SELECT Id, Name, Status__c, Bulk_Order_Error__c FROM DF_Quote__c WHERE Id = :quoteId FOR UPDATE];
    quoteObjUpd.Status__c = 'Error';
    quoteObjUpd.Bulk_Order_Error__c = 'Error: '+ message ;
    update quoteObjUpd;
    }
}

public static void deleteAllOVCs(String basketId){
  if(String.isNotBlank(basketId)){

      Set<Id> parentConfigIds = new Set<Id>();
      List <cscfga__Product_Configuration__c> configs = [SELECT Id,Name, cscfga__Configuration_Offer__r.cscfga__Template__c, cscfga__Product_Basket__c, 
                                                        cscfga__Parent_Configuration__c, cscfga__Configuration_Offer__c, cscfga__Configuration_Offer__r.Name 
                                                        FROM cscfga__Product_Configuration__c 
                                                        WHERE cscfga__Parent_Configuration__c <> NULL 
                                                        AND cscfga__Product_Definition__r.Name = 'OVC'
                                                        AND cscfga__Product_Basket__c =: basketId];
      for(cscfga__Product_Configuration__c config : configs){
        parentConfigIds.add(config.cscfga__Parent_Configuration__c);
      }

      System.debug('PPPP config: '+configs);
      if(parentConfigIds != null){
          //method to remove related product from basket
          cscfga.ProductBasketManager.removeProductsFromBasket(configs, new cscfga__Product_Basket__c(Id = basketId));
              List<cscfga__attribute__c> att = [Select id, cscfga__value__c from cscfga__attribute__c where name =:'OVC' and cscfga__Product_Configuration__c IN: parentConfigIds];
              if(att != null){
                  removeFromRelatedProduct(att);
                  update att;
                  //Set<Id> idList = new Set<Id>();
                  //idList.add(config.cscfga__Parent_Configuration__c);
                  cscfga.ProductConfigurationBulkActions.revalidateConfigurations(parentConfigIds);
          }

      }

  }
} 
public static void removeFromRelatedProduct(List<cscfga__Attribute__c> atts) {
   if(!atts.isEmpty()) {
          for(cscfga__Attribute__c att: atts){
              Id value = att.Id;
              att.cscfga__value__c = att.cscfga__value__c.replace(value + ',', '');
              att.cscfga__value__c = att.cscfga__value__c.replace(',' + value, '');
              att.cscfga__value__c = att.cscfga__value__c.replace(value, '');
            }
          }       
    }


}