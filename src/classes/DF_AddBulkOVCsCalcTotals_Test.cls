@isTest
private class DF_AddBulkOVCsCalcTotals_Test {
    
    @testSetup static void createQuote() {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        DF_Quote__c quote = [Select Id from DF_Quote__c where Opportunity_Bundle__c =:oppId LIMIT 1];
        
        DF_Order__c testOrder1 = new DF_Order__c(DF_Quote__c = quote.Id, Opportunity_Bundle__c = oppId );
        testOrder1.Heritage_Site__c = 'Yes';
        testOrder1.Induction_Required__c = 'Yes';
        testOrder1.Security_Required__c = 'Yes';
        testOrder1.Site_Notes__c = 'Site Notes';
        testOrder1.Trading_Name__c = 'Trader Joes TEST SITE';
        testOrder1.OrderType__c = 'Connect';
        //string ovcNonBill = '[{"OVCId":"'+prodConfigOvc.Id+'","CSA":"CSA180000002222","OVCName":"OVC 1","NNIGroupId":"NNI090000015859","routeType":"Local","coSHighBandwidth":"50","coSMediumBandwidth":"100","coSLowBandwidth":"60","routeRecurring":"0.00","coSRecurring":"0.00","ovcMaxFrameSize":"Jumbo (9000 Bytes)","status":"Incomplete","sTag":"0","ceVlanId":"3421","mappingMode":"DSCP"}]';
        string ovcNonBill = '[{"OVCId":"'+null+'","CSA":"CSA180000002222","OVCName":"OVC 1","NNIGroupId":"NNI090000015859","routeType":"Local","coSHighBandwidth":"50","coSMediumBandwidth":"100","coSLowBandwidth":"60","routeRecurring":"0.00","coSRecurring":"0.00","ovcMaxFrameSize":"Jumbo (9000 Bytes)","status":"Incomplete","sTag":"0","ceVlanId":"3421","mappingMode":"DSCP"}]';
        testOrder1.OVC_NonBillable__c = ovcNonBill;
        Insert testOrder1;
    }
    
    @isTest static void test_cleanOVCCoSNulls_method() {
        // Implement test code
        DF_Quote__c quote=[SELECT Id FROM DF_Quote__c LIMIT 1];
        DF_Order__c order=[SELECT Id , DF_Quote__C FROM DF_Order__c ];
        system.debug('!!! quioite '+ quote +' order '+order);
        cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1  ];
        cscfga__Product_Configuration__c uniProdConfigId = [SELECT Id,Name FROM cscfga__Product_Configuration__c WHERE Name !='OVC' LIMIT 1];
        system.debug('!!! uniProdConfigId' +uniProdConfigId);
        String ovsWrapperString = '{"CSA":"CSA180000001111", "NNIGroupId":"NNI090000015449", "OVCId":"", "OVCName":"OVC 1", "POI":"", "basketId":"'+basket.Id+'", "ceVlanId":"1002", "coSHighBandwidth":"20", "coSLowBandwidth":"100", "coSMediumBandwidth":"50", "coSRecurring":"", "errorStringForQuote":"Invalid Format for SiteCon1Email: x,Invalid Date Format for CRD", "mappingMode":"PCP", "numberOfOVCsForQuote":"1", "ovcMaxFrameSize":"Jumbo (9000 Bytes)", "ovcNumber":"0", "ovcSystemId":"", "quoteId":"'+quote.Id+'", "routeRecurring":"", "routeType":"State", "sTag":"0", "status":"Incomplete", "uniAfterHours":"No", "uniProdConfigId":"'+uniProdConfigId.Id+'", "uniTerm":"36", "unieSLA":"Premium - 4 (24/7)"}';
        EE_Bulk_OVCWrapper ovc  = (EE_Bulk_OVCWrapper)JSON.deserialize(ovsWrapperString,EE_Bulk_OVCWrapper.class);
        List<EE_Bulk_OVCWrapper> ovcDetailList = new List<EE_Bulk_OVCWrapper>();
        ovcDetailList.add(ovc);
        test.startTest();
        EE_Bulk_OVCWrapper resOVC = DF_AddBulkOVCsCalcTotals.cleanOVCCoSNulls(ovc);
        test.stopTest();
    }
    
    @isTest static void test_batch_method() {
        // Implement test code
        DF_Quote__c quote=[SELECT Id FROM DF_Quote__c LIMIT 1];
        DF_Order__c order=[SELECT Id , DF_Quote__C FROM DF_Order__c ];
        system.debug('!!! quioite '+ quote +' order '+order);
        cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1  ];
        cscfga__Product_Configuration__c uniProdConfigId = [SELECT Id,Name FROM cscfga__Product_Configuration__c WHERE Name !='OVC' LIMIT 1];
        system.debug('!!! uniProdConfigId' +uniProdConfigId);
        String ovsWrapperString = '{"CSA":"CSA180000001111", "NNIGroupId":"NNI090000015449", "OVCId":"", "OVCName":"OVC 1", "POI":"", "basketId":"'+basket.Id+'", "ceVlanId":"1002", "coSHighBandwidth":"20", "coSLowBandwidth":"100", "coSMediumBandwidth":"50", "coSRecurring":"", "errorStringForQuote":"Invalid Format for SiteCon1Email: x,Invalid Date Format for CRD", "mappingMode":"PCP", "numberOfOVCsForQuote":"1", "ovcMaxFrameSize":"Jumbo (9000 Bytes)", "ovcNumber":"0", "ovcSystemId":"", "quoteId":"'+quote.Id+'", "routeRecurring":"", "routeType":"State", "sTag":"0", "status":"Incomplete", "uniAfterHours":"No", "uniProdConfigId":"'+uniProdConfigId.Id+'", "uniTerm":"36", "unieSLA":"Premium - 4 (24/7)"}';
        EE_Bulk_OVCWrapper ovc  = (EE_Bulk_OVCWrapper)JSON.deserialize(ovsWrapperString,EE_Bulk_OVCWrapper.class);
        List<EE_Bulk_OVCWrapper> ovcDetailList = new List<EE_Bulk_OVCWrapper>();
        ovcDetailList.add(ovc);
        DF_AddBulkOVCsCalcTotals e = new DF_AddBulkOVCsCalcTotals(ovcDetailList);
        test.startTest();
        Database.executeBatch(e);
        test.stopTest();
    }
    
    @isTest static void test_method_one() {
        // Implement test code
        DF_Quote__c quote=[SELECT Id FROM DF_Quote__c LIMIT 1];
        DF_Order__c order=[SELECT Id , DF_Quote__C FROM DF_Order__c ];
        system.debug('!!! quioite '+ quote +' order '+order);
        cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1  ];
        cscfga__Product_Configuration__c uniProdConfigId = [SELECT Id,Name FROM cscfga__Product_Configuration__c WHERE Name !='OVC' LIMIT 1];
        system.debug('!!! uniProdConfigId' +uniProdConfigId);
        String ovsWrapperString = '{"CSA":"CSA180000001111", "NNIGroupId":"NNI090000015449", "OVCId":"", "OVCName":"OVC 1", "POI":"", "basketId":"'+basket.Id+'", "ceVlanId":"1002", "coSHighBandwidth":"20", "coSLowBandwidth":"100", "coSMediumBandwidth":"50", "coSRecurring":"", "errorStringForQuote":"Invalid Format for SiteCon1Email: x,Invalid Date Format for CRD", "mappingMode":"PCP", "numberOfOVCsForQuote":"1", "ovcMaxFrameSize":"Jumbo (9000 Bytes)", "ovcNumber":"0", "ovcSystemId":"", "quoteId":"'+quote.Id+'", "routeRecurring":"", "routeType":"State", "sTag":"0", "status":"Incomplete", "uniAfterHours":"No", "uniProdConfigId":"'+uniProdConfigId.Id+'", "uniTerm":"36", "unieSLA":"Premium - 4 (24/7)"}';
        EE_Bulk_OVCWrapper ovc  = (EE_Bulk_OVCWrapper)JSON.deserialize(ovsWrapperString,EE_Bulk_OVCWrapper.class);
        test.startTest();
        DF_AddBulkOVCsCalcTotals.addOVCs(ovc);
        test.stopTest();
    }
    
    @isTest static void test_method_two() {
        DF_Quote__c quote=[SELECT Id FROM DF_Quote__c LIMIT 1];
        cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1  ];
        cscfga__Product_Configuration__c uniProdConfigId = [SELECT Id FROM cscfga__Product_Configuration__c WHERE Name  !='OVC'  LIMIT 1];
        String ovsWrapperString = '{"CSA":"CSA180000001111", "NNIGroupId":"NNI090000015449", "OVCId":"", "OVCName":"OVC 1", "POI":"", "basketId":"'+basket.Id+'", "ceVlanId":"1002", "coSHighBandwidth":"20", "coSLowBandwidth":"100", "coSMediumBandwidth":"50", "coSRecurring":"", "errorStringForQuote":"Invalid Format for SiteCon1Email: x,Invalid Date Format for CRD", "mappingMode":"PCP", "numberOfOVCsForQuote":"1", "ovcMaxFrameSize":"Jumbo (9000 Bytes)", "ovcNumber":"0", "ovcSystemId":"", "quoteId":"'+quote.Id+'", "routeRecurring":"", "routeType":"State", "sTag":"0", "status":"Incomplete", "uniAfterHours":"No", "uniProdConfigId":"'+uniProdConfigId.Id+'", "uniTerm":"36", "unieSLA":"Premium - 4 (24/7)"}';
        EE_Bulk_OVCWrapper ovc  = (EE_Bulk_OVCWrapper)JSON.deserialize(ovsWrapperString,EE_Bulk_OVCWrapper.class);
        test.startTest();
        DF_AddBulkOVCsCalcTotals.setTermOnUni(ovc);
        test.stopTest();
        
    }
    @isTest static void test_method_3() {
        DF_Quote__c quote=[SELECT Id FROM DF_Quote__c LIMIT 1];
        cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1  ];
        cscfga__Product_Configuration__c uniProdConfigId = [SELECT Id FROM cscfga__Product_Configuration__c WHERE Name  !='OVC'  LIMIT 1];
        String ovsWrapperString = '{"CSA":"CSA180000001111", "NNIGroupId":"NNI090000015449", "OVCId":"", "OVCName":"OVC 1", "POI":"", "basketId":"'+basket.Id+'", "ceVlanId":"1002", "coSHighBandwidth":"20", "coSLowBandwidth":"100", "coSMediumBandwidth":"50", "coSRecurring":"", "errorStringForQuote":"", "mappingMode":"PCP", "numberOfOVCsForQuote":"1", "ovcMaxFrameSize":"Jumbo (9000 Bytes)", "ovcNumber":"0", "ovcSystemId":"", "quoteId":"'+quote.Id+'", "routeRecurring":"", "routeType":"State", "sTag":"0", "status":"Valid", "uniAfterHours":"No", "uniProdConfigId":"'+uniProdConfigId.Id+'", "uniTerm":"36", "unieSLA":"Premium - 4 (24/7)"}';
        EE_Bulk_OVCWrapper ovc  = (EE_Bulk_OVCWrapper)JSON.deserialize(ovsWrapperString,EE_Bulk_OVCWrapper.class);
        test.startTest();
        DF_AddBulkOVCsCalcTotals.calculateTotal(ovc);
        test.stopTest();
        
    }
    
    @isTest static void test_method_4() {
        DF_Quote__c quote=[SELECT Id FROM DF_Quote__c LIMIT 1];
        cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1  ];
        cscfga__Product_Configuration__c uniProdConfigId = [SELECT Id FROM cscfga__Product_Configuration__c WHERE Name  !='OVC' LIMIT 1];
        String ovsWrapperString = '{"CSA":"CSA180000001111", "NNIGroupId":"NNI090000015449", "OVCId":"", "OVCName":"OVC 1", "POI":"", "basketId":"'+basket.Id+'", "ceVlanId":"1002", "coSHighBandwidth":"20", "coSLowBandwidth":"100", "coSMediumBandwidth":"50", "coSRecurring":"", "errorStringForQuote":"", "mappingMode":"PCP", "numberOfOVCsForQuote":"1", "ovcMaxFrameSize":"Jumbo (9000 Bytes)", "ovcNumber":"0", "ovcSystemId":"", "quoteId":"'+quote.Id+'", "routeRecurring":"", "routeType":"State", "sTag":"0", "status":"Valid", "uniAfterHours":"No", "uniProdConfigId":"'+uniProdConfigId.Id+'", "uniTerm":"36", "unieSLA":"Premium - 4 (24/7)"}';
        EE_Bulk_OVCWrapper ovc  = (EE_Bulk_OVCWrapper)JSON.deserialize(ovsWrapperString,EE_Bulk_OVCWrapper.class);
        test.startTest();
        DF_AddBulkOVCsCalcTotals.calculateTotal(ovc);
        test.stopTest();
        
    }
    
}