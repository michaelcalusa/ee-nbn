public with sharing class DF_AppainToSFEventHandler implements Queueable{
    
    private List<BusinessEvent__e> lstToProcess;

    public DF_AppainToSFEventHandler(List<BusinessEvent__e> evtLst) {       
        lstToProcess = evtLst;
    }

    public void execute(QueueableContext context) {

        Business_Platform_Events__c customSettingValues = Business_Platform_Events__c.getOrgDefaults();       
        
        for(BusinessEvent__e evt : lstToProcess){
            if((evt.Event_Id__c==customSettingValues.SFC_Desktop_Assessment_completed__c)){
                system.debug('Appain Update event received for the Event ID '+evt.Event_Id__c);
                updateProductConfigFBCValue(evt.Message_Body__c);
            }               
        }
    }

    @future(callout=true)
    public static void updateProductConfigFBCValue(String evtBody){
      system.debug('@@updateProductConfigFBCValue==='+evtBody);   

      try{          
            System.debug('Appain Message Body : ' + evtBody);
            Map<String,String> evtBodyMap = new Map<String,String>(); 
            List<String> bodyList = evtBody.split(',');
            
            for(String s : bodyList){
               List<String> strVal = s.split(':');
               evtBodyMap.put(strVal[0],strVal[1]);
            }
            
            if(evtBodyMap.containskey('opportunityId') && evtBodyMap.containskey('cost') && evtBodyMap.containskey('salesforceQuoteId')){
                //String oppId = evtBodyMap.get('opportunityId');
                                
                String cost =  evtBodyMap.get('cost');
                String quoteId = evtBodyMap.get('salesforceQuoteId');
                if(String.isNotBlank(quoteId)){                    
                
                    List<DF_Quote__c> quoteList = [SELECT Id, Name, Location_Id__c, Address__c, Opportunity__c, RAG__c, Opportunity__r.Commercial_Deal__r.name,
                                                    Opportunity__r.Commercial_Deal__r.End_Date__c,Opportunity__r.Commercial_Deal__c,
                                                    LAPI_Response__c, Opportunity_Bundle__c,Fibre_Build_Category__c, Fibre_Build_Cost__c, GUID__c
                                                    FROM DF_Quote__c WHERE Id = :quoteId];
                    DF_Quote__c quoteObj = !quoteList.isEmpty() ? quoteList.get(0) : null;                    
                    System.debug('PP Start of DA - quoteObj: '+quoteObj);
                    System.debug('PP Start of DA - RAG: '+quoteObj.RAG__c);
                    Id recTypeId = DF_CS_API_Util.getRecordType(DF_Constants.ENTERPRISE_ETHERNET_NAME,DF_Constants.ORDER_OBJECT);
                    if(quoteObj != null) {
                        string oppId = quoteObj.Opportunity__c;                        
                        updateFBC(oppId,quoteId,cost);
                        System.debug('Updating product basket with the Desktop Assesment Cost : ' + cost);
                        System.debug('PP Start of DA - oppId: '+oppId);
                        System.debug('PP Start of DA - quoteId: '+quoteId);
                        if((quoteObj.Fibre_Build_Category__c).equalsIgnorecase(DF_Constants.FB_CATEGORY_C) && recTypeId != null){
                            
                            
                            List<DF_Opportunity_Bundle__c> oppBundleList = [SELECT Id,name, Account__c FROM DF_Opportunity_Bundle__c where Id = :quoteObj.Opportunity_Bundle__c];
                            DF_Opportunity_Bundle__c oppBundle = !oppBundleList.isEmpty() ? oppBundleList.get(0) : null;
                            List<Account> accList = [Select Id,name, Access_Seeker_ID__c, Billing_ID__c FROM Account where Id = :oppBundle.Account__c];
                            Account acc = !accList.isEmpty() ? accList.get(0) : null;
                            DF_Order__c orderObj = new DF_Order__c();
                            orderObj.DF_Quote__c = quoteId;
                            orderObj.Opportunity_Bundle__c = oppBundle.Id;
                            orderObj.RecordTypeId = recTypeId;
                            insert orderObj;
                            String priceItemCode = '';
                            System.debug('PP Start of DA - orderObj: '+orderObj);
                            String childOppId = generateOrderForDekstopAssessment(oppId);
                            if(String.isNotBlank(childOppId)){
                                List<csord__Order__c> csOrder = [SELECT Id, CreatedDate,csordtelcoa__Opportunity__c, csord__Total_One_Off_Charges__c FROM csord__Order__c where csordtelcoa__Opportunity__c =:childOppId AND csord__Primary_Order__c != null];
                                if(csOrder.size() > 0 && csOrder.get(0) != null && acc != null){
                                    List<csord__Order_Line_Item__c> csOLI = [SELECT Name,csord__Total_Price__c from csord__Order_Line_Item__c where csord__Order__c =:csOrder.get(0).Id];
                                    for(csord__Order_Line_Item__c oli :csOLI){
                                      if(oli.csord__Total_Price__c > 0){
                                        priceItemCode = oli.name;
                                      }
                                    }
                                    if(String.isNotBlank(priceItemCode)){
                                    List<String> priceItemCodebits = priceItemCode.split('-');
                                    priceItemCode = (priceItemCodebits[priceItemCodebits.size() - 1]).trim();
                                    }
                                    orderObj.IsSiteSurveyChargesOrderCompleted__c = true;
                                    orderObj.Site_Survey_Charges_JSON__c = buildJSON(csOrder.get(0),priceItemCode,orderObj,quoteObj,acc);
                                    if(string.isNotBlank(orderObj.Site_Survey_Charges_JSON__c)){
                                      orderObj.Site_Survey_Charges_JSON__c= orderObj.Site_Survey_Charges_JSON__c.replace('"discount" : null,','');
                          orderObj.Site_Survey_Charges_JSON__c=orderObj.Site_Survey_Charges_JSON__c.replace('"discount":null,','');
                          orderObj.Site_Survey_Charges_JSON__c = orderObj.Site_Survey_Charges_JSON__c.replace(',{"type":"Term","amount":null}','');
                          orderObj.Site_Survey_Charges_JSON__c = orderObj.Site_Survey_Charges_JSON__c.replace('{"type":"PSD","amount":null}','');
                          orderObj.Site_Survey_Charges_JSON__c = orderObj.Site_Survey_Charges_JSON__c.replace(',{"type":null,"amount":null}','');
                          orderObj.Site_Survey_Charges_JSON__c = orderObj.Site_Survey_Charges_JSON__c.replace('{"type":null,"amount":null}','');
                          orderObj.Site_Survey_Charges_JSON__c = orderObj.Site_Survey_Charges_JSON__c.replace('"discount":[],','');
                          orderObj.Site_Survey_Charges_JSON__c = orderObj.Site_Survey_Charges_JSON__c.replace('"discount":[{"type":"Term","amount":null}],','');
                          orderObj.Site_Survey_Charges_JSON__c = orderObj.Site_Survey_Charges_JSON__c.replace('"discount":[{"type":"PSD","amount":null}],','');
                          orderObj.Site_Survey_Charges_JSON__c = orderObj.Site_Survey_Charges_JSON__c.replace('[,{','[{');
                                    }
                                    update orderObj;
                                }
                                
                            }
                        }
                    }    
                }
            }
        }catch(System.CalloutException ex) {
            GlobalUtility.logMessage('Error', 'AppianResponseHandlingError', 'updateProductConfigFBCValue', ' ', '', '', '', ex, 0);
        }         
    }
    
    //Method to build JSON that is stored in DF Order 
    public static String buildJSON(csord__Order__c csOrder, String priceItemCode, DF_Order__c orderObj, DF_Quote__c quoteObj, Account acc){
        DF_SOAOrderDataJSONWrapper ssSOAClass = new DF_SOAOrderDataJSONWrapper();
        DF_SOAOrderDataJSONWrapper.ManageBillingEventRequest ssMR = new DF_SOAOrderDataJSONWrapper.ManageBillingEventRequest();
        ssMR.notificationType = DF_Constants.NOTIFICATION_TYPE;
        DF_SOAOrderDataJSONWrapper.ProductOrder ssPO = new DF_SOAOrderDataJSONWrapper.ProductOrder();
        ssPO.id = orderObj.Id;
        ssPO.SFid = orderObj.Id;
        DF_SOAOrderDataJSONWrapper.AccessSeekerInteraction ssASI = new DF_SOAOrderDataJSONWrapper.AccessSeekerInteraction();
        ssASI.billingAccountID = acc.Billing_ID__c;//to be checked
        ssPO.accessSeekerInteraction = ssASI;
        ssPO.interactionStatus = DF_Constants.INTERACTION_STATUS;
        ssPO.orderType = DF_Constants.ORDER_TYPE;
        ssPO.interactionDateComplete = String.valueOfGmt(csOrder.CreatedDate);
        if(quoteObj.Opportunity__c <> null && quoteObj.Opportunity__r.Commercial_Deal__c <> null){
          ssPO.commercialDealId = quoteObj.Opportunity__r.Commercial_Deal__r.name;
          ssPO.commercialDealName = acc.name;  
          ssPO.commercialDealEndDate = string.valueOf(quoteObj.Opportunity__r.Commercial_Deal__r.End_Date__c); 
        }
        DF_SOAOrderDataJSONWrapper.ProductOrderComprisedOf ssPC = new DF_SOAOrderDataJSONWrapper.ProductOrderComprisedOf();
        ssPC.action = DF_Constants.ACTION;
        DF_SOAOrderDataJSONWrapper.ItemInvolvesLocation ssIIA = new DF_SOAOrderDataJSONWrapper.ItemInvolvesLocation();
        ssIIA.id = quoteObj.Location_Id__c;
        ssPC.itemInvolvesLocation = ssIIA;
        DF_SOAOrderDataJSONWrapper.ItemInvolvesProduct ssIIP = new DF_SOAOrderDataJSONWrapper.ItemInvolvesProduct();
        ssIIP.id = quoteObj.Name;
        ssIIP.accessServiceTechnologyType = DF_Constants.SITE_SURVEY_PRODUCT_TYPE;
        ssPC.itemInvolvesProduct = ssIIP;
        ssPO.productOrderComprisedOf = ssPC;
        ssMR.productOrder = ssPO;
        DF_SOAOrderDataJSONWrapper.ProductOrderInvolvesCharges ssPOC = new DF_SOAOrderDataJSONWrapper.ProductOrderInvolvesCharges();
        List<DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge> ssOCList = new List<DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge>();
        DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge ssOC = new DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge();
        DF_SOAOrderDataJSONWrapper.DiscountType abc = new DF_SOAOrderDataJSONWrapper.DiscountType();
        map<string,string> siteSurveyChargePriceMap = new map<string,string>();
        for(csord__Order_Line_Item__c oli : [Select id,name,csord__Order__c, csord__Total_Price__c from csord__Order_Line_Item__c where csord__Order__c =:csOrder.id]){
          if(oli.name.containsIgnoreCase(DF_Constants.siteSurveyCharges)){
            siteSurveyChargePriceMap.put(oli.name,string.valueOf(oli.csord__Total_Price__c));
          }
        }
        if(siteSurveyChargePriceMap != null){
          ssOC.ID = priceItemCode;
          ssOC.name = DF_Constants.SITE_SURVEY_NAME;
          ssOC.chargeReferences = quoteObj.Name;
          ssOC.quantity = '1.0';
          ssOC.amount = String.valueOf(siteSurveyChargePriceMap.get(DF_Constants.SITE_SURVEY_NAME+' - '+priceItemCode));
          if(siteSurveyChargePriceMap.get(DF_Constants.siteSurveyChargesCommercialDiscount) != null){
            abc.Type = 'PSD';
              abc.amount =  String.valueOf(siteSurveyChargePriceMap.get(DF_Constants.siteSurveyChargesCommercialDiscount));
          }
            if(abc != null){
                ssOC.discount = new List<DF_SOAOrderDataJSONWrapper.DiscountType>{abc};
            }
            /*if(siteSurveyChargePriceMap.get(DF_Constants.siteSurveyChargesCommercialDiscount) != null){
                ssOC.amount = String.valueOf(decimal.valueOf((siteSurveyChargePriceMap.get(DF_Constants.SITE_SURVEY_NAME+' - '+priceItemCode))) + decimal.valueOf(siteSurveyChargePriceMap.get(DF_Constants.siteSurveyChargesCommercialDiscount)));
            }else{
                ssOC.amount = String.valueOf(decimal.valueOf(siteSurveyChargePriceMap.get(DF_Constants.SITE_SURVEY_NAME+' - '+priceItemCode)));
            }*/
            ssOCList.add(ssOC);
        }
        ssPOC.OneTimeChargeProdPriceCharge = ssOCList;
        ssMR.ProductOrderInvolvesCharges = ssPOC;
        return json.serialize(ssMR);
    }
    
   

    //Method to create order for Desktop Assessment
    public static String generateOrderForDekstopAssessment(String parentOppId){
        try{
            if(String.isNotEmpty(parentOppId)){
                String oppId = DF_DesktopAssessmentController.processForDA(parentOppId);
                return oppId;
            }
            else
                return null;
        }catch(Exception ex) {
            GlobalUtility.logMessage('Error', 'AppianResponseHandlingError', 'generateOrderForDekstopAssessment', ' ', '', '', '', ex, 0);
            return null;
        }         
    }

    public static void updateFBC(String oppId, String quoteId,String cost){
            String bcDefId = DF_CS_API_Util.getCustomSettingValue('SF_BUILD_CONTRIBUTION_DEFINITION_ID');
            List<DF_Quote__c> quoteList = [SELECT Id, Name, Location_Id__c, Address__c, Opportunity__c, LAPI_Response__c, Fibre_Build_Cost__c FROM DF_Quote__c WHERE Id = :quoteId];
            DF_Quote__c quoteObj = !quoteList.isEmpty() ? quoteList.get(0) : null;
            List<cscfga__Product_Basket__c> baskets = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :oppId];
            if(!baskets.isEmpty()) {
                String basketId = !baskets.isEmpty() ? baskets.get(0) != null ? baskets.get(0).Id : null : null;
                List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c>([Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and cscfga__Product_Definition__c = :bcDefId]);
                
                cscfga.API_1.ApiSession apiSession = DF_CS_API_Util.createApiSession(baskets.get(0));
                apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = configs.get(0).id, cscfga__Product_Basket__c = basketId));        
                cscfga.ProductConfiguration currConfig = apiSession.getConfiguration();
    
                for(cscfga.Attribute att : currConfig.getAttributes()) {
                    System.debug('related Attribute name is : ' + att.getName());
                    if(att.getName() == 'Initial Build Cost'){
                        att.setValue(cost);            
                        att.setDisplayValue(cost);
                    }
                }
                
                apiSession.executeRules(); // run the rules after setting the attribute values
                apiSession.validateConfiguration();
                apiSession.persistConfiguration(true);
    
                currConfig = apiSession.getConfiguration();
    
                for(cscfga.Attribute att : currConfig.getAttributes()) {
                    if(att.getName() == 'Fibre Build Contribution'){
                        quoteObj.Fibre_Build_Cost__c = Decimal.valueOf(att.getValue());
                    }
                    if(att.getName().equalsIgnoreCase('DBP FBC discount') && att.getPrice() !=null){
                        quoteObj.Fibre_Build_Cost__c  = quoteObj.Fibre_Build_Cost__c + att.getPrice();
                    }               
                }
                update quoteObj;
            }
    }
}