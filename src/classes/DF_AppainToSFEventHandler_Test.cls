@isTest
private class DF_AppainToSFEventHandler_Test {

    
    @isTest
    static void testExecuteMethod()
    {
        test.startTest(); 
		Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigSiteSurvey(acc);
        //Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        //opp.Opportunity_Bundle__c = oppId;
        //insert opp;
        //basketObj.cscfga__Opportunity__c = opp.Id;
        //system.debug('::oppty id for basket::'+opp.Id);
        //update basketObj;
        Business_Platform_Events__c settings = DF_TestData.createCustomSettingsforBusinessPlatformEvents();
        insert settings;
        
        List<DF_Quote__c> dfQuoteList = [SELECT Id,Address__c,LAPI_Response__c,Fibre_Build_Category__c,RAG__c,Fibre_Build_Cost__c,Opportunity_Bundle__c,Opportunity__c
                                         FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppid LIMIT 1];
        system.debug('::oppty for quote::'+dfQuoteList[0].Opportunity__c);
        basketObj.cscfga__Opportunity__c = dfQuoteList[0].Opportunity__c;
        //system.debug('::oppty id for basket::'+opp.Id);
        update basketObj;
        List<BusinessEvent__e> toProcess = new List<BusinessEvent__e>();
        for(Integer i=0;i<1;i++){
            String msg = 'opportunityId:' + dfQuoteList[0].Opportunity__c + ',cost:' + 5555 + ',salesforceQuoteId:' + dfQuoteList.get(0).id +'';
            BusinessEvent__e b = new BusinessEvent__e();
            b.Event_Id__c = 'AP-SF-01000';
            b.Event_Record_Id__c = String.valueOf(i);
            b.Event_Type__c = 'Desktop assesment';
            b.Source__c = 'Appian';
            b.Message_Body__c = msg;
            toProcess.add(b);
        }           
        
        Id jobId = System.enqueueJob(new DF_AppainToSFEventHandler(toProcess));
        test.stopTest(); 
    }
    
    @isTest
    static void testExecuteMethodDBP()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecordsDBP();
        Account acc = DF_TestData.createAccount('Test Account');
        acc.Access_Seeker_ID__c = 'AS12345';
        acc.Billing_ID__c = 'BL12345';
        insert acc;
        
        Commercial_Deal__c commercialDeal = DF_TestData.commercialDeal(acc.id);
        insert commercialDeal;
        
        List<Opportunity> oppList = new List<Opportunity>();
        
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigSiteSurvey(acc);
        //Opportunity opp = DF_TestData.createOpportunity('Test Opp');
       // opp.Opportunity_Bundle__c = oppId;
        //oppList.add(opp);
        
       // Opportunity childOpp = DF_TestData.createOpportunity('Test Child Opp');
       // childOpp.Parent_Opportunity__c = opp.Id;
       // childOpp.Commercial_Deal__c = commercialDeal.id;
       // oppList.add(childOpp);
        
      //  insert oppList;
                
        //basketObj.cscfga__Opportunity__c = opp.Id;
      //  update basketObj;
        Business_Platform_Events__c settings = DF_TestData.createCustomSettingsforBusinessPlatformEvents();
        insert settings;
        
        List<DF_Quote__c> dfQuoteList = [SELECT Id, Name, Location_Id__c, Address__c, Opportunity__c, RAG__c, Opportunity__r.Commercial_Deal__r.name,
                                                    Opportunity__r.Commercial_Deal__r.End_Date__c,Opportunity__r.Commercial_Deal__c,
                                                    LAPI_Response__c, Opportunity_Bundle__c,Fibre_Build_Category__c, Fibre_Build_Cost__c, GUID__c
                                                    FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppid LIMIT 1];
        
        String priceItemCode = '';
        
        DF_Order__c ordObj = new DF_Order__c();
        ordObj.DF_Quote__c = dfQuoteList.get(0).Id;
        ordObj.Opportunity_Bundle__c = oppId;
        insert ordObj;
        
        
        csord__Order__c csOrder = new csord__Order__c();
        csOrder.csordtelcoa__Opportunity__c = dfQuoteList[0].Opportunity__c;
        csOrder.csord__Identification__c = 'Order_a1hN0000001UNpfIAG_0';
        insert csOrder;
        
        csord__Order_Line_Item__c csOLI = new csord__Order_Line_Item__c();
        csOLI.csord__Order__c = csOrder.Id;
        csOLI.name = 'Site Survey Charges - Commercial Deal Discount';
        csOLI.csord__Total_Price__c = 1500;
        csOLI.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        insert csOLI;
        
        basketObj.cscfga__Opportunity__c = dfQuoteList[0].Opportunity__c;
        //system.debug('::oppty id for basket::'+opp.Id);
        update basketObj;
        List<BusinessEvent__e> toProcess = new List<BusinessEvent__e>();
        for(Integer i=0;i<1;i++){
            String msg = 'opportunityId:' + dfQuoteList[0].Opportunity__c + ',cost:' + 5555 + ',salesforceQuoteId:' + dfQuoteList.get(0).id +'';
            BusinessEvent__e b = new BusinessEvent__e();
            b.Event_Id__c = 'AP-SF-01000';
            b.Event_Record_Id__c = String.valueOf(i);
            b.Event_Type__c = 'Desktop assesment';
            b.Source__c = 'Appian';
            b.Message_Body__c = msg;
            toProcess.add(b);
        }           
        test.startTest(); 
        Id jobId = System.enqueueJob(new DF_AppainToSFEventHandler(toProcess));
        test.stopTest(); 
    }
    
@isTest
    static void testBuildJSONMethod()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        acc.Access_Seeker_ID__c = 'AS12345';
        acc.Billing_ID__c = 'BL12345';
        insert acc;
        
        test.startTest(); 
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigSiteSurvey(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basketObj.cscfga__Opportunity__c = opp.Id;
        update basketObj;
        Business_Platform_Events__c settings = DF_TestData.createCustomSettingsforBusinessPlatformEvents();
        insert settings;
        
        List<DF_Quote__c> dfQuoteList = [SELECT Id, Name, Location_Id__c, Address__c, Opportunity__c, RAG__c, Opportunity__r.Commercial_Deal__r.name,
                                                    Opportunity__r.Commercial_Deal__r.End_Date__c,Opportunity__r.Commercial_Deal__c,
                                                    LAPI_Response__c, Opportunity_Bundle__c,Fibre_Build_Category__c, Fibre_Build_Cost__c, GUID__c
                                                    FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppid LIMIT 1];
        dfQuoteList[0].Fibre_Build_Category__c = DF_Constants.FB_CATEGORY_C;
       
        String priceItemCode = '';
        DF_Order__c ordObj = new DF_Order__c();
        ordObj.DF_Quote__c = dfQuoteList.get(0).Id;
        ordObj.Opportunity_Bundle__c = oppId;
        insert ordObj;
        Opportunity childOpp = DF_TestData.createOpportunity('Test Child Opp');
        childOpp.Parent_Opportunity__c = opp.Id;
        insert childOpp;
        csord__Order__c csOrder = new csord__Order__c();
        csOrder.csordtelcoa__Opportunity__c = childOpp.Id;
        csOrder.csord__Identification__c = 'Order_a1hN0000001UNpfIAG_0';
        insert csOrder;
        csord__Order_Line_Item__c csOLI = new csord__Order_Line_Item__c();
        csOLI.csord__Order__c = csOrder.Id;
        csOLI.name = 'Site Survey Charges - EEAS_NRC_SF';
        csOLI.csord__Total_Price__c = 1500;
        csOLI.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        insert csOLI;
        priceItemCode = 'EEAS_NRC_SF';
        
        //test.startTest(); 
        String jsonStr = DF_AppainToSFEventHandler.buildJSON(csOrder,priceItemCode,ordObj,dfQuoteList.get(0),acc);
        test.stopTest(); 
    }
    
    @isTest
    static void testBuildJSONMethodDBP()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecordsDBP();
        Account acc = DF_TestData.createAccount('Test Account');
        acc.Access_Seeker_ID__c = 'AS12345';
        acc.Billing_ID__c = 'BL12345';
        insert acc;
        
        Commercial_Deal__c commercialDeal = DF_TestData.commercialDeal(acc.id);
        insert commercialDeal;
        
        test.startTest(); 
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigSiteSurvey(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        
        Opportunity childOpp = DF_TestData.createOpportunity('Test Child Opp');
        childOpp.Parent_Opportunity__c = opp.Id;
        childOpp.Commercial_Deal__c = commercialDeal.id;
        insert childOpp;
                
        basketObj.cscfga__Opportunity__c = opp.Id;
        update basketObj;
        Business_Platform_Events__c settings = DF_TestData.createCustomSettingsforBusinessPlatformEvents();
        insert settings;
        
        List<DF_Quote__c> dfQuoteList = [SELECT Id, Name, Location_Id__c, Address__c, Opportunity__c, RAG__c, Opportunity__r.Commercial_Deal__r.name,
                                                    Opportunity__r.Commercial_Deal__r.End_Date__c,Opportunity__r.Commercial_Deal__c,
                                                    LAPI_Response__c, Opportunity_Bundle__c,Fibre_Build_Category__c, Fibre_Build_Cost__c, GUID__c
                                                    FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppid LIMIT 1];
        
        String priceItemCode = '';
        
        DF_Order__c ordObj = new DF_Order__c();
        ordObj.DF_Quote__c = dfQuoteList.get(0).Id;
        ordObj.Opportunity_Bundle__c = oppId;
        insert ordObj;
        
        csord__Order__c csOrder = new csord__Order__c();
        csOrder.csordtelcoa__Opportunity__c = childOpp.Id;
        csOrder.csord__Identification__c = 'Order_a1hN0000001UNpfIAG_0';
        insert csOrder;
        
        csord__Order_Line_Item__c csOLI = new csord__Order_Line_Item__c();
        csOLI.csord__Order__c = csOrder.Id;
        csOLI.name = 'Site Survey Charges - Commercial Deal Discount';
        csOLI.csord__Total_Price__c = 1500;
        csOLI.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        insert csOLI;
        
        priceItemCode = 'Commercial Deal Discount';
        
        //test.startTest(); 
        String jsonStr = DF_AppainToSFEventHandler.buildJSON(csOrder,priceItemCode,ordObj,dfQuoteList.get(0),acc);
        test.stopTest(); 
    }
}