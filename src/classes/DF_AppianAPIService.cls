public class DF_AppianAPIService {

    public static Id dfOrderId;
    public static DF_Order__c dfOrder;
    public static DF_Quote__c dfQuote;
    public static DF_Order_Settings__mdt dfOrderSettings;
  
    public static Boolean stubPNI = false;    
    
    @future(callout=true)
    public static void submitOrderByOrderId(String param) {  
        
        system.debug('param: ' + param);

        final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;   
        // Headers        
        
        // Errors             
        final String ERROR_MESSAGE = 'A HTTP error occurred in class method DF_AppianAPIService.submitOrderByOrderId';        

        Http http = new Http();     
        HttpRequest req;              
        HTTPResponse res; 
        String orderJson = '';
        String requestStr;        
        String responseBodyStatus;        
        
        Map<String, String> headerMap = new Map<String, String>();

        try {           
             
            // Get dfOrder and its details
            dfOrder = DF_AppianAPIUtils.getDFOrder(param);
            dfOrderSettings = DF_AppianAPIUtils.getOrderMetadata('DFOrderSettings');
            NAMED_CREDENTIAL = 'Appian';
            TIMEOUT_LIMIT = 60000;
            // Generate Payload   
            
            if(dfOrder.OrderType__c == DF_Constants.MODIFY) {
                orderJson = dfOrder.Modify_Order_JSON__c;
            }
            else {
                orderJson = dfOrder.Order_JSON__c;
            }
                          
            requestStr =  '{"eventIdentifier":"'+dfOrderSettings.OrderCreateCode__c+'","externalSystemCorrelationId":"'+dfOrder.DF_Quote__r.GUID__c+'","eventBody":'+orderJson+'}';
            //system.debug('Payload request (before stripping json nulls): ' + requestStr);
            system.debug('Payload request: ' + requestStr);

            if (String.isEmpty(requestStr)) {
                throw new CustomException(DF_IntegrationUtils.ERR_REQUEST_BODY_EMPTY);
            }

            // Build endpoint
            END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL;
            system.debug('END_POINT: ' + END_POINT);     

            DF_AppianAPIUtils.getHeaders(dfOrder, headerMap);

            // Generate request 
            req = DF_AppianAPIUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, requestStr, headerMap, 'OrderSubmit');
            system.debug('req: ' + req); 

            // Send and get response               
            res = http.send(req);

            system.debug('Response Status Code: ' + res.getStatusCode());
            system.debug('Response Body: ' + res.getBody());
        
            if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {                           
                system.debug('send success');
                DF_AppianAPIUtils.orderSubmitPostProcessing(dfOrder);
            
            } else {
                system.debug('send failure');                                                                   
                
                List<String> paramList = new List<String>{param};                  
                AsyncQueueableUtils.retry(DF_AppianOrderSubmitHandler.HANDLER_NAME, paramList, ERROR_MESSAGE + '\nResponse error: '+res.getBody());                          
            }                           
        } catch (Exception e) {
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_AppianAPIService.class.getName(), 'orderSubmitEvent', '', '', '', '', e, 0);
            
            List<String> paramList = new List<String>{param};              
            AsyncQueueableUtils.retry(DF_AppianOrderSubmitHandler.HANDLER_NAME, paramList, (e.getMessage() + '\n\n' + e.getStackTraceString()));       
        }        
    }
    
    
    @future(callout=true)
    public static void sfRequestByQuoteId(String param) {  
        
        system.debug('param: ' + param);

        final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;   
        // Headers        
        
        // Errors             
        final String ERROR_MESSAGE = 'A HTTP error occurred in class method DF_AppianAPIService.sfRequestByQuoteId';        

        Http http = new Http();     
        HttpRequest req;              
        HTTPResponse res; 

        String requestStr;        
        String responseBodyStatus;        
        
        //Map<String, String> headerMap = new Map<String, String>();

        try {           
             
            // Get dfOrder and its details
            dfQuote = DF_AppianAPIUtils.getDFQuote(param);
            Business_Platform_Events__c customSettingValues = Business_Platform_Events__c.getOrgDefaults();
            NAMED_CREDENTIAL = 'Appian';
            TIMEOUT_LIMIT = 60000;
            // Generate Payload                 
            requestStr =  '{"eventIdentifier":"'+customSettingValues.DF_SFC_Desktop_Assessment__c+'","externalSystemCorrelationId":"'+dfQuote.GUID__c+'","eventBody":'+dfQuote.SF_Request_Json__c+'}';
            //system.debug('Payload request (before stripping json nulls): ' + requestStr);
            system.debug('Payload request: ' + requestStr);

            if (String.isEmpty(requestStr)) {
                throw new CustomException(DF_IntegrationUtils.ERR_REQUEST_BODY_EMPTY);
            }

            // Build endpoint
            END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL;
            system.debug('END_POINT: ' + END_POINT);     

            // Generate request 
            req = DF_AppianAPIUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, requestStr, null, '');
            system.debug('req: ' + req); 

            // Send and get response               
            res = http.send(req);

            system.debug('Response Status Code: ' + res.getStatusCode());
            system.debug('Response Body: ' + res.getBody());
        
            if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {                           
                system.debug('send success');
                //DF_AppianAPIUtils.orderSubmitPostProcessing(dfOrder);
            
            } else {
                system.debug('send failure');                                                                   
                
                List<String> paramList = new List<String>{param};                  
                AsyncQueueableUtils.retry(DF_AppianSFRequestHandler.HANDLER_NAME, paramList, ERROR_MESSAGE + '\nResponse error: '+res.getBody());                          
            }                           
        } catch (Exception e) {
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_AppianAPIService.class.getName(), 'SFRequestEvent', '', '', '', '', e, 0);
            
            List<String> paramList = new List<String>{param};              
            AsyncQueueableUtils.retry(DF_AppianSFRequestHandler.HANDLER_NAME, paramList, (e.getMessage() + '\n\n' + e.getStackTraceString()));       
        }        
    }
  
}