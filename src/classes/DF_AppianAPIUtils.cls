public class DF_AppianAPIUtils {
    public static void getHeaders(DF_Order__c dfOrder, Map<String, String> headerMap) {     
        // Headers
        final String TIMESTAMP_VAL;  
        final String CORRELATION_ID_VAL;
        final String ACCESS_SEEKER_ID_VAL;
        
        try {               
            TIMESTAMP_VAL = DateTime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');

            headerMap.put(DF_IntegrationUtils.TIMESTAMP, TIMESTAMP_VAL);

                        
            CORRELATION_ID_VAL = dfOrder.DF_Quote__r.GUID__c;               
            
            ACCESS_SEEKER_ID_VAL = dfOrder.Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c;            
            headerMap.put(DF_IntegrationUtils.CORRELATION_ID, CORRELATION_ID_VAL);                          
            headerMap.put(DF_IntegrationUtils.ACCESS_SEEKER_ID, ACCESS_SEEKER_ID_VAL);
            headerMap.put(DF_Constants.CONTENT_TYPE, DF_Constants.CONTENT_TYPE_JSON);           
            headerMap.put(DF_Constants.HTTP_HEADER_ACCEPT, DF_Constants.CONTENT_TYPE_JSON);    
            
            system.debug('::header::'+headerMap);                                      
        } catch (Exception e) {  
            throw new CustomException(e.getMessage());
        }
    }

    public static HttpRequest getHttpRequest(String endPoint, Integer timeoutLimit, String requestStr, Map<String, String> headerMap, string apiCallType) {                                                      
        HttpRequest req = new HttpRequest();        
        
        try {               
            req.setEndpoint(endpoint);      
            req.setMethod(DF_Constants.HTTP_METHOD_POST);             
            req.setTimeout(timeoutLimit);
            
            // Set all headers
              
            req.setHeader(DF_Constants.CONTENT_TYPE, DF_Constants.CONTENT_TYPE_JSON);
            if(apiCallType == 'OrderSubmit') {
                req.setHeader('activityName','createOrder');
                req.setHeader('msgName','ManageProductOrdercreateOrderRequest');
                req.setHeader('businessServiceName','ManageProductOrder');
                req.setHeader('businessServiceVersion','V2.0');
                req.setHeader('msgType','Request'); 
                req.setHeader('security','Placeholder Security');
                req.setHeader('communicationPattern','SubmitNotification');
                req.setHeader('orderPriority', '6');
                if (String.isNotEmpty(headerMap.get(DF_IntegrationUtils.CORRELATION_ID))) {
                    system.debug('::GUID::'+headerMap.get(DF_IntegrationUtils.CORRELATION_ID));             
                    req.setHeader(DF_IntegrationUtils.CORRELATION_ID, headerMap.get(DF_IntegrationUtils.CORRELATION_ID));           
                } else {                
                    throw new CustomException(DF_IntegrationUtils.ERR_CORRELATION_ID_NOT_FOUND);
                }            
                
                req.setHeader(DF_IntegrationUtils.TIMESTAMP, headerMap.get(DF_IntegrationUtils.TIMESTAMP));
    
                if (String.isNotEmpty(headerMap.get(DF_IntegrationUtils.ACCESS_SEEKER_ID))) {               
                    req.setHeader(DF_IntegrationUtils.ACCESS_SEEKER_ID, headerMap.get(DF_IntegrationUtils.ACCESS_SEEKER_ID));               
                } else {                
                    throw new CustomException(DF_IntegrationUtils.ERR_ACCESS_SEEKER_ID_NOT_FOUND);
                }
            }
            req.setBody(requestStr);                                 
        } catch (Exception e) {  
            throw new CustomException(e.getMessage());
        }
               
        return req;
    }
    
    public static void orderSubmitPostProcessing(DF_Order__c dfOrder) {
        try {  
            if (dfOrder != null) {              
                dfOrder.Order_Status__c = DF_Constants.QUOTE_STATUS_SUBMITTED;  
                update dfOrder;             
            }
            
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }        
    }
    
    public static DF_Order__c getDFOrder(Id dfOrderId) {                                             
        DF_Order__c dfOrder;
        List<DF_Order__c> dfOrderList = new List<DF_Order__c>();

        try {
            if (String.isNotEmpty(dfOrderId)) {                                                                        
                dfOrderList = [SELECT DF_Quote__r.GUID__c, DF_Quote__r.Order_GUID__c, Modify_Order_JSON__c,
                                      Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c,
                                      Order_Json__c, DF_Quote__r.Opportunity__c, OrderType__c
                               FROM   DF_Order__c            
                               WHERE  Id = :dfOrderId];     

                if (!dfOrderList.isEmpty()) {
                    dfOrder = dfOrderList[0];
                }                                                          
            }                                                     
        } catch (Exception e) {    
            throw new CustomException(e.getMessage());
        }                     
                            
        return dfOrder;
    }
    
    public static DF_Quote__c getDFQuote(Id dfQuoteId) {                                             
        DF_Quote__c dfQuote;
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        try {
            if (String.isNotEmpty(dfQuoteId)) {                                                                        
                dfQuoteList = [SELECT GUID__c, Order_GUID__c,
                                      Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c,
                                      SF_Request_Json__c, Opportunity__c
                               FROM   DF_Quote__c
                               WHERE  Id = :dfQuoteId];     

                if (!dfQuoteList.isEmpty()) {
                    dfQuote = dfQuoteList[0];
                }                                                          
            }                                                     
        } catch (Exception e) {    
            throw new CustomException(e.getMessage());
        }                     
                            
        return dfQuote;
    }
    
    public static DF_Order_Settings__mdt getOrderMetadata(string name) {
        return [SELECT OrderAckEventCode__c,OrderAckSource__c,OrderAckBatchSize__c,
                                            OrderCreateBatchSize__c,OrderCreateCode__c,OrderCreateSource__c,
                                            OrderAccBatchSize__c,OrderAccEventCode__c,OrderAccSource__c,
                                            OrderCompleteBatchSize__c,OrderCompleteEventCode__c,
                                            OrderCompleteSource__c
                                                         FROM   DF_Order_Settings__mdt
                                                         WHERE  DeveloperName =:name];
    }
}