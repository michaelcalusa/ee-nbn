public class DF_AppianOrderSubmitHandler extends AsyncQueueableHandler {

    public static final String HANDLER_NAME = 'AppianOrderSubmitHandler';

    public DF_AppianOrderSubmitHandler() {
      // No of params must be 1
        super(DF_AppianOrderSubmitHandler.HANDLER_NAME, 1);     
    }

    public override void executeWork(List<String> paramList) {      
      // Enforce max of 1 input parameter
      if (paramList.size() == 1) {
        invokeProcess(paramList[0]);
      } else {
        throw new CustomException(AsyncQueueableUtils.ERR_INVALID_PARAMS);
      }
    }

    private void invokeProcess(String param) {
        DF_AppianAPIService.submitOrderByOrderId(param);
    }
}