public class DF_AppianSFRequestHandler extends AsyncQueueableHandler {

    public static final String HANDLER_NAME = 'AppianSFRequestHandler';

    public DF_AppianSFRequestHandler() {
      // No of params must be 1
        super(DF_AppianSFRequestHandler.HANDLER_NAME, 1);     
    }

    public override void executeWork(List<String> paramList) {      
      // Enforce max of 1 input parameter
      if (paramList.size() == 1) {
        invokeProcess(paramList[0]);
      } else {
        throw new CustomException(AsyncQueueableUtils.ERR_INVALID_PARAMS);
      }
    }

    private void invokeProcess(String param) {
        DF_AppianAPIService.sfRequestByQuoteId(param);
    }
}