@isTest
private class DF_AppianSFRequestHandler_Test {

    @isTest static void test_executeWork() {            
      /** Setup data  **/
      String response;      
      User commUser;
             
      String locationId = 'LOC000035375038';
      
    // Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        // Create sfreq
        DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, DF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);    
        insert sfReq;
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.SF_Request_JSON__c = '{"field":"value"}';
        insert dfQuote;
    
        
        String param = dfQuote.Id;

        List<String> paramList = new List<String>();
        paramList.add(param); 

        // Generate mock response
        response = '{"Ack":"true"}';
 
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

        // Set up commUser to run test as
        commUser = DF_TestData.createDFCommUser();

        system.runAs(commUser) { 
            test.startTest();   
    
            DF_AppianSFRequestHandler handler = new DF_AppianSFRequestHandler();
            
            // Invoke Call
            handler.executeWork(paramList);      
          
            test.stopTest();           
        }                                                   
    }
    
    @isTest static void test_executeWork_negative() {            
      /** Setup data  **/
      String response;      
      User commUser;
             
      String locationId = 'LOC000035375038';
      
    // Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        // Create sfreq
        DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, DF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);    
        insert sfReq;
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        //dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.SF_Request_JSON__c = '{"field":"value"}';
        insert dfQuote;
    
        
        String param = dfQuote.Id;

        List<String> paramList = new List<String>();
        paramList.add(param); 

        // Generate mock response
        response = '{"Ack":"true"}';
 
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(400, response, null));

        // Set up commUser to run test as
        commUser = DF_TestData.createDFCommUser();

        system.runAs(commUser) { 
            test.startTest();   
    
            DF_AppianSFRequestHandler handler = new DF_AppianSFRequestHandler();
            
            // Invoke Call
            handler.executeWork(paramList);      
          
            test.stopTest();           
        }                                                   
    }
    
     /** Data Creation **/
    @testSetup static void setup() {
      try {
        createCustomSettings();                  
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }       
    }
    
    @isTest static void createCustomSettings() {                
        try {
          Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
          asrConfigSettings.Max_No_of_Future_Calls__c = 50;
          asrConfigSettings.Max_No_of_Parallels__c = 10;
          asrConfigSettings.Max_No_of_Retries__c = 3;
          asrConfigSettings.Max_No_of_Rows__c = 1;      
          upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }                
    }  
}