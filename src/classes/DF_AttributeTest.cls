@isTest
public class DF_AttributeTest {
	
    //define attribute properties for Cloudsense
    public String name;
    public String value;
    public Boolean isLineItem;
    public Decimal price;
    
    public DF_AttributeTest(String n, String v, Boolean isLI, Decimal p) {
        name =  n;
        value = v;
        isLineItem = isLI;
        price = p; 
    }
}