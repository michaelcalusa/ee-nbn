public class DF_BaseController  {
	
	@AuraEnabled
    public static Boolean isBasketZoneMappingUpdated(String basketId) {
		Boolean result = false;
		try
		{			
			String sam = 
			[
				SELECT cscfga__Value__c
				FROM cscfga__Attribute__c 
				WHERE Name = 'SAM ID' AND cscfga__Product_Configuration__r.cscfga__Product_Basket__c =: basketId LIMIT 1
			].cscfga__Value__c;

			System.debug('@@@ sam: '+ sam);	

			List<cscfga__Attribute__c> zoneAttr =  
			[
				SELECT cscfga__Value__c
				FROM cscfga__Attribute__c 
				WHERE Name = 'Zone' AND cscfga__Product_Configuration__r.cscfga__Product_Basket__c =: basketId LIMIT 1
			];
			
			System.debug('@@@ zoneAttr: ' + zoneAttr); 

			String savedZone = zoneAttr.size() == 0 ? '' : zoneAttr[0].cscfga__Value__c;

			System.debug('@@@ savedZone: ' + savedZone);

			List<DF_SAMZone_Mapping_Values__c> zoneMapping = 
			[
				SELECT Zone__c 					
				FROM DF_SAMZone_Mapping_Values__c 
				WHERE SAM__c =: sam LIMIT 1
			];

			String latestZone = zoneMapping.size() == 0 ? '' : zoneMapping[0].Zone__c;

			System.debug('@@@ latestZone: ' + latestZone);

			if(!savedZone.equalsIgnoreCase(latestZone)) result = true;
		}
		catch(Exception ex)
		{
			System.debug(ex);
			System.debug(basketId);
		}
		return result;
	}

	@AuraEnabled
    public static String getCustomSetting(String settingName) {
		String result = '';
		try
		{
			result = DF_CS_API_Util.getCustomSettingValue(settingName);
		}
		catch(Exception ex)
		{
			System.debug(ex);
			System.debug(settingName);
		}
		return result;
	}
	
	@AuraEnabled
	//Get EE Assurance custom settings by component name
    public static Map<String, String> getASCustomSettings(String componentName) {

		Map<String, String> results = new Map<String, String>();
		
		try
		{			
			for (DF_AS_Custom_Settings__c setting : DF_AS_Custom_Settings__c.getAll().values()){
				if(setting.Component__c == componentName && setting.IsEnabled__c)
				{
					results.put(setting.Name, setting.Value__c);
				}
			}	
		}
		catch(Exception ex)
		{
			System.debug(ex);
		}		
		
		return results;
	}	
}