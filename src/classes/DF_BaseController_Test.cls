@isTest 
private class DF_BaseController_Test {

	@isTest 
	private static void test_isBasketZoneMappingUpdated() {
		Id oppId = DF_TestService.getServiceFeasibilityRequest();
      oppId = DF_TestService.getQuoteRecords();
      Account acc = DF_TestData.createAccount('Test Account');
      insert acc;
      cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
      Opportunity opp = DF_TestData.createOpportunity('Test Opp');
      opp.Opportunity_Bundle__c = oppId;
      insert opp;
		System.debug('@@@ basketId: ' +  basketObj.Id);

      Test.startTest();      		
		Boolean result = DF_BaseController.isBasketZoneMappingUpdated(basketObj.Id);
		System.debug('@@@ result: ' +  result);
      System.assertEquals(true, result);
      Test.stopTest();
	}

	@isTest
	private static void test_getCustomSetting() {
		
		String result1;
		String result2;
    	User commUser;
		commUser = DF_TestData.createDFCommUser();
        system.runAs(commUser) {
	    	test.startTest();    

			String customSettingName1 = 'PAGE_URL_SF';
			result1 = DF_BaseController.getCustomSetting(customSettingName1);
			String customSettingName2 = 'PAGE_URL_ORDER';
			result2 = DF_BaseController.getCustomSetting(customSettingName2);
	        
			test.stopTest();                
        }	        
		            
        // Assertions
        system.assertNotEquals('', result1);
		system.assertNotEquals('', result2);
	}

	@isTest
	Public static void test_getASCustomSettings() {
		
		Map<String, String> results;
		DF_AS_TestService.createCustomSettingsForAS();
    	User commUser = DF_TestData.createDFCommUser();
		
		system.runAs(commUser) {
	    	test.startTest();
			results = DF_BaseController.getASCustomSettings('DF_AS_Service_Incident_Search');
			system.debug('@@test_getASCustomSettings - results --->'+results);		
			test.stopTest();                
        }
		            
        // Assertions
		system.assertNotEquals(0, results.size());
	}
}