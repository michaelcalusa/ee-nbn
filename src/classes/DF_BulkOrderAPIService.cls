/**
 * Created by michael.calusa on 21/02/2019.
 */

public class DF_BulkOrderAPIService {
    @future(callout=true)
    public static void addNewOVC(String param) {
        List<String> params = param.split(',');
        String basketId = params[0];
        String configId = params[1];
        String orderId = params[2];
        system.debug('basketId: ' + basketId);
        system.debug('configId: ' + configId);
        system.debug('orderId: ' + orderId);
        try {
            String result = DF_ProductController.addOVC(basketId,configId,null);
            system.debug('results:: ' + result);
            if(result == 'Success') {
                DF_SF_BulkOrderUtils.updateOVCDetails(param);
            } else {
                DF_SF_BulkOrderUtils.updateQuoteStatusInError(orderId, result + ' inserting new ovc.');
            }
        } catch (Exception e) {
            DF_SF_BulkOrderUtils.updateQuoteStatusInError(orderId, e.getMessage());
        }
    }
}