/**
 * Created by michael.calusa on 22/02/2019.
 */

@IsTest
private class DF_BulkOrderAPIService_Test {
    @IsTest
    static void testAddOVC_method() {
        /** Setup data  **/
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        insert opp;
        List<cscfga__Product_Configuration__c> pcConfigList = [Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketObj.Id and name = 'Direct Fibre - Product Charges'];
        cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
        String configId = configObj != null ? configObj.Id : null;
        Test.startTest();
        String param = basketObj.Id + ',' + configId + ',' + 'a7I5D0000008UvNUAU' + ',' + '2';
            DF_BulkOrderAPIService.addNewOVC(param);
            System.assert(param != null);
        Test.stopTest();
    }

    @IsTest
    static void testAddOVC_negative_method() {
        /** Setup data  **/
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        insert opp;
        List<cscfga__Product_Configuration__c> pcConfigList = [Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketObj.Id and name = 'Direct Fibre - Product Charges'];
        cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
        String configId = configObj != null ? configObj.Id : null;
        Test.startTest();
        String param = '' + ',' + configId + ',' + 'a7I5D0000008UvNUAU' + ',' + '2';
        DF_BulkOrderAPIService.addNewOVC(param);
        System.assert(param != null);
        Test.stopTest();
    }
}