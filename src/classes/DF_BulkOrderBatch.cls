//************************************************************
// Author: Rajaramachandran Sethu
// Date: 21 MAR 2019
// Purpose: To process the bulk order upload records.
//************************************************************
global class DF_BulkOrderBatch implements Database.Batchable<sObject>,Database.Stateful  {


    //this is a single job batch
    public static String ovcName = '';
    public static String errorStringForQuote = '';
    public static String ovcId = '';
    public static String CSA = '';
    public static String cosRecurring = '';
    public static String routeRecurring = '';
    public static String jsonOVCNB = '';
    public static Integer ovcCnt = 0;
    public static Integer ovcCntPV = 0;
    public static String ovcAddResult = '';
    public static Set<Id> configIds = new Set<Id>();
//    public Static List<String> nbAttrValLst = new List<String>();
    public static List<String> ovcDefnLst = new List<String>{'OVC1', 'OVC2', 'OVC3', 'OVC4', 'OVC5', 'OVC6', 'OVC7', 'OVC8'};
//    public Static Map<String, List<String>> nbAttrValMap = new Map<String, List<String>>();
    public Static Map<String, Map<String,String>> nbAttrValMap = new Map<String, Map<String,String>>();
    public static Map<String, String> nbAttrMap = new Map<String, String>{'OVCId'=>'','CSA'=>''
            ,'OVCName'=>'','routeType'=>'','coSHighBandwidth'=>'0','coSMediumBandwidth'=>'0'
            ,'coSLowBandwidth'=>'0','routeRecurring'=>'0','routeRecurring'=>'0','coSRecurring'=>'0','POI'=>''
            ,'status'=>'Incomplete','mappingMode'=>'PCP','NNIGroupId'=>'','sTag'=>'0','ceVlanId'=>'0'
            ,'ovcMaxFrameSize'=>'Jumbo (9000 Bytes)'};

    //Global Map<String, Map<String, Map<String, EE_Bulk_OVCWrapper>>> quoteProdConfIdBasketOVCsMap = new Map<String, Map<String, Map<String, EE_Bulk_OVCWrapper>>>(); //Map<quote Id, Map<product config Id, Map<basket Id, OVC List >>> 
    Global List<EE_Bulk_OVCWrapper> allBulkOVCs = new List<EE_Bulk_OVCWrapper>();
   // GLobal allBulkOVCs.OVC  = new List<EE_Bulk_OVCWrapper.OVC>();
    
    List<ID> quoteIdLst;
    List<ID> quoteToProcess = new List<ID>();
    Map<Id, Map<String, Map<String, String>>> inceptionMap;
   // Map<Id, Id> quoteToOrderMap = new Map<Id, Id>();
    Map<Id, Id> quoteToOppMap = new Map<Id, Id>();
    Map<Id, String> quoteErrMap = new Map<Id, String>();
    Map<Id, List<String>> missingReqFields = new Map<Id, List<String>> ();
    Map<Id, Integer> bulkOVCCntMap;
    public static  List<DF_Quote__c> quoteLst = new List<DF_Quote__c>();
    public static  List<String> csAPIParams = new List<String>();

    public static final String READY = 'Ready for Order';
    public static List<String> dontProcess = new List<String>{'Accepted', 'Ready for Order', 'Submitted', 'Order Draft','Feasibility Expired'};
    String query='SELECT Id, Opportunity_Bundle__c, Status__c FROM DF_Quote__c WHERE Id IN :quoteIdLst'; 

    
    global DF_BulkOrderBatch(List<ID> quoteIdLst, Map<Id, Map<String, Map<String, String>>> inceptionMap, Map<Id, Integer> bulkOVCCntMap, Map<Id, List<String>> missingReqFields) {
        this.quoteIdLst = quoteIdLst;
        this.inceptionMap = inceptionMap;
        this.bulkOVCCntMap = bulkOVCCntMap;
        this.missingReqFields =missingReqFields;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<DF_Quote__c> scope) {
        List<DF_Order__c> dfOrderLst = new List<DF_Order__c>();
        Map<Id, String> qProdConfMap = new Map<Id, String>();
        Map<Id, String> qProdAttrMap = new Map<Id, String>();
        Map<Id, Id> quoteToOrderMap = new Map<Id, Id>();

        Map<Id,Id> orderMap = new Map<Id,Id>();
        for(DF_Order__c order:[SELECT Id,DF_Quote__c From DF_Order__c WHERE DF_Quote__c IN:quoteIdLst]){
            if(order!=null){
                orderMap.put(order.DF_Quote__c,order.id);
            }
        }
system.debug('!!! orderMap '+orderMap);
        
        string orderRTId = DF_CS_API_Util.getRecordType(DF_Constants.ENTERPRISE_ETHERNET_NAME, DF_Constants.ORDER_OBJECT);  
    
        system.debug('@@@ quoteIdLstBatch : '+ JSON.serialize(this.quoteIdLst));
        system.debug('@@@ inceptionMapBatch : '+ JSON.serialize(this.inceptionMap));

        for(DF_Quote__c quoteRec : scope){
            if(!dontProcess.contains(quoteRec.Status__c)){
                quoteToProcess.add(quoteRec.Id);
                
                DF_Order__c dfOrderRec = new DF_Order__c();
                if(orderMap.get(quoteRec.Id)==null){
                     dfOrderRec.Opportunity_Bundle__c = quoteRec.Opportunity_Bundle__c;
                }else{
                     dfOrderRec.id = orderMap.get(quoteRec.Id);
                }
                dfOrderRec.DF_Quote__c = quoteRec.Id;
               // dfOrderRec.Opportunity_Bundle__c = quoteRec.Opportunity_Bundle__c;
                dfOrderRec.recordTypeId = orderRTId;
                dfOrderRec.Interface_Type__c = inceptionMap.get(quoteRec.Id).get('Order').get('UNIInterfaceTypes');
                dfOrderRec.OVC_Type__c = inceptionMap.get(quoteRec.Id).get('Order').get('UNIOVCType');
                if(inceptionMap.get(quoteRec.Id).get('Order').get('UNIOVCType')=='Access EPL'){
                    dfOrderRec.TPID__c = '0x8100';
                }else{
                    dfOrderRec.TPID__c = inceptionMap.get(quoteRec.Id).get('Order').get('UNITPID'); 
                }
                dfOrderRec.Heritage_Site__c = inceptionMap.get(quoteRec.Id).get('Order').get('HeritageSite');
                dfOrderRec.Induction_Required__c = inceptionMap.get(quoteRec.Id).get('Order').get('InductionRequired');
                dfOrderRec.Security_Required__c = inceptionMap.get(quoteRec.Id).get('Order').get('SecurityRequired');
                dfOrderRec.Site_Notes__c = inceptionMap.get(quoteRec.Id).get('Order').get('SiteNotes');
                dfOrderRec.Trading_Name__c = inceptionMap.get(quoteRec.Id).get('Order').get('TradingName');
                if(inceptionMap.get(quoteRec.Id).get('Order').get('CustomerRequiredDate')!=null && inceptionMap.get(quoteRec.Id).get('Order').get('CustomerRequiredDate') != ''){
                    system.debug('!!!!! Date' + Date.parse(inceptionMap.get(quoteRec.Id).get('Order').get('CustomerRequiredDate')));
                    dfOrderRec.Customer_Required_Date__c = Date.parse(inceptionMap.get(quoteRec.Id).get('Order').get('CustomerRequiredDate'));
                }else if(inceptionMap.get(quoteRec.Id).get('Order').get('CustomerRequiredDate') == ''){
                    dfOrderRec.Customer_Required_Date__c = null;
                }
                dfOrderRec.NTD_Mounting__c= inceptionMap.get(quoteRec.Id).get('Order').get('NTDMounting');
                dfOrderRec.Power_Supply_1__c = inceptionMap.get(quoteRec.Id).get('Order').get('PowerSupply1');
                dfOrderRec.Power_Supply_2__c = inceptionMap.get(quoteRec.Id).get('Order').get('PowerSupply2');
                dfOrderRec.Installation_Notes__c = inceptionMap.get(quoteRec.Id).get('Order').get('InstallationNotes');
                dfOrderRec.OVC_NonBillable__c = '';
                dfOrderLst.add(dfOrderRec);
            }
        }
        if(!dfOrderLst.isEmpty()){
            Database.UpsertResult[] srOrdLst = Database.upsert(dfOrderLst, false);
            system.debug('New Order Id: '+ dfOrderLst[0].Id);
            
            for(integer i =0; i<dfOrderLst.size();i++){
                String msg='';
                If(!srOrdLst[i].isSuccess()){       
                    for(Database.Error err: srOrdLst[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    }
                    quoteErrMap.put(dfOrderLst[i].DF_Quote__c, msg);
                    system.debug('!!! quoteErrMap'+quoteErrMap);
                }else{
                    quoteToOrderMap.put(dfOrderLst[i].DF_Quote__c, srOrdLst[i].getId());
                    system.debug('!!! quoteToOrderMap'+quoteToOrderMap);
                }
            } 
            system.debug('!!! quoteToOrderMap '+quoteToOrderMap);
            for(DF_Quote__c quoteObjRec: [SELECT Id, Status__c, BulkOrderJSON__c, BulkOVCCount__c FROM DF_Quote__c WHERE Id IN :quoteToOrderMap.keySet()]){
                Map<Id, Map<String, Map<String,String>>> interimMap = new Map<Id, Map<String, Map<String,String>>>();
                interimMap.put(quoteObjRec.Id, inceptionMap.get(quoteObjRec.Id));
                quoteObjRec.BulkOVCCount__c = bulkOVCCntMap.get(quoteObjRec.Id);
                quoteObjRec.BulkOrderJSON__c = JSON.serialize(interimMap);
                quoteLst.add(quoteObjRec);
                csAPIParams.add(quoteObjRec.Id);
                associateContacts(quoteObjRec.Id, inceptionMap.get(quoteObjRec.Id));

                system.debug('!!! quoteToOrderMap (quoteObjRec.Id)'+quoteObjRec.Id );
               
            }

            if(!quoteErrMap.isEmpty()){
                for(DF_Quote__c quoteErrRec: [SELECT Id, Status__c, Bulk_Order_Error__c FROM DF_Quote__c WHERE Id IN :quoteErrMap.keySet()]){
                    quoteErrRec.Bulk_Order_Error__c = quoteErrMap.get(quoteErrRec.Id);
                    quoteErrRec.Status__c = 'Error';
                    quoteLst.add(quoteErrRec);
                }    
            }
        }
        if(!quoteLst.isEmpty()){
            Database.SaveResult[] srQuoteLst = Database.update(quoteLst, false);
        }

        for(DF_Quote__c quoteObjRec: [SELECT Id, Status__c, BulkOrderJSON__c, BulkOVCCount__c FROM DF_Quote__c WHERE Id IN :quoteToOrderMap.keySet()]){
            //moved for flow control
             updateProdConfig(quoteObjRec.Id);
        }
        
        if(!csAPIParams.isEmpty()){
            //AsyncQueueableUtils.createRequests(DF_SF_BulkOrderHandler.HANDLER_NAME, csAPIParams);            
        }
    
    }
    
    global void finish(Database.BatchableContext BC) {//Map<String, Map<String, Map<String, EE_Bulk_OVCWrapper>>> <quote Id, Map<product config Id, Map<basket Id, OVC List >>> 
        system.debug('!!! Inside Finish');

        for(EE_Bulk_OVCWrapper ovc: allBulkOVCs){
            System.debug(ovc);
        }
        Database.executeBatch(new DF_AddBulkOVCsCalcTotals(allBulkOVCs),1);

    }

    public static void associateContacts(String quoteId, Map<String, Map<String, String>> deepMap){
         //Map<String, String> interimBusCon1Map = new Map<String, String>(deepMap.get('BusinessCon1'));
        Map<String, String> interimBusCon1Map = deepMap.containsKey('BusinessCon1')  ? new Map<String, String>(deepMap.get('BusinessCon1')): new Map<String, String>();
        if(returnMapSize(interimBusCon1Map)>0){
            interimBusCon1Map.put('busContStreetAddress', deepMap.get('BusinessCon1').get('BusinessCon1StreetNumber') +' '+
                                  deepMap.get('BusinessCon1').get('BusinessCon1StreetName')+ ' '+
                                  deepMap.get('BusinessCon1').get('BusinessCon1StreetType'));
            interimBusCon1Map.remove('BusinessCon1StreetNumber');
            interimBusCon1Map.remove('BusinessCon1StreetName');
            interimBusCon1Map.remove('BusinessCon1StreetType');
            if(!(deepMap.get('BusinessCon1').get('BusinessCon1Email') == '' || deepMap.get('BusinessCon1').get('BusinessCon1Number') == ''))
            createContactDetails(interimBusCon1Map, 'BusinessCon1', 'busCont', quoteId);
        }
        //Map<String, String> interimBusCon2Map = new Map<String, String>(deepMap.get('BusinessCon2'));
         Map<String, String> interimBusCon2Map = deepMap.containsKey('BusinessCon2') ? new Map<String, String>(deepMap.get('BusinessCon2')): new Map<String, String>();
        if(returnMapSize(interimBusCon2Map)>0){
            interimBusCon2Map.put('busContStreetAddress', deepMap.get('BusinessCon2').get('BusinessCon2StreetNumber') +' '+
                                  deepMap.get('BusinessCon2').get('BusinessCon2StreetName')+ ' '+
                                  deepMap.get('BusinessCon2').get('BusinessCon2StreetType'));
            interimBusCon2Map.remove('BusinessCon2StreetNumber');
            interimBusCon2Map.remove('BusinessCon2StreetName');
            interimBusCon2Map.remove('BusinessCon2StreetType');
            if(!(deepMap.get('BusinessCon2').get('BusinessCon2Email') == '' || deepMap.get('BusinessCon2').get('BusinessCon2Number') == ''))
            createContactDetails(interimBusCon2Map, 'BusinessCon2', 'busCont', quoteId);
        }
        //Map<String, String> interimSiteCon1Map = new Map<String, String>(deepMap.get('SiteCon1'));
        Map<String, String> interimSiteCon1Map = deepMap.containsKey('SiteCon1') ? new Map<String, String>(deepMap.get('SiteCon1')): new Map<String, String>();

        if(returnMapSize(interimSiteCon1Map)>0){
            if(!(deepMap.get('SiteCon1').get('SiteCon1Email') == '' || deepMap.get('SiteCon1').get('SiteCon1Number') == ''))
            createContactDetails(interimSiteCon1Map, 'SiteCon1', 'siteCont', quoteId);
        }
        //Map<String, String> interimSiteCon2Map = new Map<String, String>(deepMap.get('SiteCon2'));
         Map<String, String> interimSiteCon2Map = deepMap.containsKey('SiteCon2') ? new Map<String, String>(deepMap.get('SiteCon2')): new Map<String, String>();
        if(returnMapSize(interimSiteCon2Map)>0){
            if(!(deepMap.get('SiteCon2').get('SiteCon2Email') == '' || deepMap.get('SiteCon2').get('SiteCon2Number') == ''))
            createContactDetails(interimSiteCon2Map, 'SiteCon2', 'siteCont', quoteId);
        }
    }
    
    private static void createContactDetails(Map<String,String> contactMap, String replaceFrom, String replaceTo, Id qId){
        String contactJson = JSON.serialize(contactMap);
        contactJson = contactJson.replace(replaceFrom, replaceTo);
        system.debug('@@@ Contact JSON : '+ contactJson);
        system.debug('@@@ Quote Id : '+ qId);

        if(replaceFrom.containsIgnorecase('BusinessCon')){
            String bConId = DF_ContactDetailsController.addBusinessContactToContactsAndOppContactRole('Business Contact', contactJson, qId);
        }
        if(replaceFrom.containsIgnorecase('SiteCon')){
            String sConId = DF_ContactDetailsController.addSiteContactToContactsAndOppContactRole('Site Contact', contactJson, qId);
        }
    }
    private static Integer returnMapSize(Map<String, String> inputMap){
        List<String> ContactValueLst = new List<String>();
        ContactValueLst = inputMap.values();
        Set<String> ContactValueSet = new Set<String>(ContactValueLst);
        removeNullVal(ContactValueSet);

        return ContactValueSet.size();
    }

    private static void removeNullVal(Set<String> ipSet){
        Set<String> nullSet = new Set<String>();
        nullSet.add('');
        nullSet.add(null);
        ipSet.removeAll(nullSet);
    }

    public void updateProdConfig(String quoteId){
        system.debug('!!! Start updateProdConfig');
        String pcDefId = DF_CS_API_Util.getCustomSettingValue('SF_PRODUCT_CHARGE_DEFINITION_ID');
        String ovcDefId = DF_CS_API_Util.getCustomSettingValue('SF_OVC_DEFINITION_ID');
        List<EE_Bulk_OVCWrapper> oVCs = new List<EE_Bulk_OVCWrapper>(); // add this to the quoteProdConfIdBasketOVCsMap
        //oVCs.OVC  = new List<EE_Bulk_OVCWrapper.OVC>();
        
        String pcConfigId;
        String basketId;
        
        try{    
            DF_Quote__c quoteObj = [SELECT Id, Name, Location_Id__c, LAPI_Response__c, Opportunity__c, Status__c, Bulk_Order_Error__c, BulkOrderJSON__c, BulkOVCCount__c FROM DF_Quote__c WHERE Id = :quoteId];
            DF_ServiceFeasibilityResponse sfResp = (DF_ServiceFeasibilityResponse)System.JSON.deserialize(quoteObj.LAPI_Response__c, DF_ServiceFeasibilityResponse.class);
            CSA = sfResp.csaId;
            system.debug('!!! quoteObj.BulkOrderJSON__c,' +quoteObj.BulkOrderJSON__c);
            Map<Id, Map<String, Map<String, String>>> inceptionMap = (Map<Id, Map<String, Map<String, String>>>) JSON.deserialize(quoteObj.BulkOrderJSON__c, Map<Id, Map<String, Map<String, String>>>.class);
            List<cscfga__Product_Basket__c> baskets = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :quoteObj.Opportunity__c];

            Integer totalOVC = Integer.valueOf(quoteObj.BulkOVCCount__c);

            system.debug('@@@ totalOVC : '+ totalOVC);
            if(!baskets.isEmpty()) {
                basketId = !baskets.isEmpty() ? baskets.get(0) != null ? baskets.get(0).Id : null : null;    
                
                cscfga__Product_Configuration__c pcConfigObj = [Select Id, Name from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and cscfga__Product_Definition__c = :pcDefId LIMIT 1];
                pcConfigId = pcConfigObj.Id;
                
                sObject ovcObject = new cscfga__Product_Definition__c();
                String queryString = DF_CS_API_Util.getQuery(ovcObject, ' WHERE Id = '+ '\'' + ovcDefId + '\'');
                List<cscfga__Product_Definition__c> ovcList = (List<cscfga__Product_Definition__c>)DF_CS_API_Util.getQueryRecords(queryString); 
                cscfga__Product_Definition__c ovc = !ovcList.isEmpty() ? ovcList.get(0) : null;
                System.debug('PPPP ovc: '+ovc);
                
                if(missingReqFields.containsKey(quoteId)){
                    List<String> errorStrings =  missingReqFields.get(quoteId);
                    //errorStrings.remove(null);
                    errorStringForQuote = String.join(errorStrings, ',');
                    if(totalOVC == 0 && !missingReqFields.isEmpty()){//added for error handling
                        String additionalMsg = 'Missing values in CSV Columns:';
                        DF_SF_QuoteController.updateQuoteStatusInError(missingReqFields, additionalMsg);
                    }

                }else{
                    errorStringForQuote = '';
                }


                    for(Integer i=0; i<totalOVC; i++){
                        EE_Bulk_OVCWrapper ovcToAdd= new EE_Bulk_OVCWrapper();
                        ovcToAdd.quoteId = quoteId;
                        ovcToAdd.basketId = basketId;
                        ovcToAdd.uniProdConfigId = pcConfigId;
                        ovcToAdd.numberOfOVCsForQuote = quoteObj.BulkOVCCount__c;
                        ovcToAdd.OVCId = '';    //a1h5D0000006i8YQAQ
                        ovcToAdd.OVCName='OVC '+String.ValueOF(i+1); //OVC 1
                        ovcToAdd.CSA =CSA;  //CSA180000002222
                        ovcToAdd.NNIGroupId=inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'NNIGroupId'); //NNI123456789123
                        ovcToAdd.routeType = inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'RouteType'); //Local
                        ovcToAdd.coSHighBandwidth = inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'CoSHigh');    //250
                        ovcToAdd.coSMediumBandwidth = inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'CoSMedium');    //100
                        ovcToAdd.coSLowBandwidth = inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'CoSLow');  //0
                        ovcToAdd.routeRecurring ='';    //0.00
                        ovcToAdd.coSRecurring = ''; //0.00
                        ovcToAdd.ovcMaxFrameSize = 'Jumbo (9000 Bytes)';    //Jumbo (9000 Bytes)
                        ovcToAdd.status = 'Incomplete'; //Incomplete
                        ovcToAdd.sTag = inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'SVLANId');    //12
                        ovcToAdd.ceVlanId=inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'UNIVLANId');    //2
                        ovcToAdd.mappingMode=inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'MappingMode'); //DSCP
                        ovcToAdd.ovcSystemId=''; //OVC000000123456
                        ovcToAdd.POI = ''; //3EXH - EXHIBITION
                        ovcToAdd.uniAfterHours = inceptionMap.get(quoteId).get('Order').get('AfterHoursSiteVisit') ;
                        ovcToAdd.unieSLA = inceptionMap.get(quoteId).get('Order').get('UNIServiceRestorationSLA');
                        ovcToAdd.uniTerm = inceptionMap.get(quoteId).get('Order').get('UNITerm');
                        ovcToAdd.errorStringForQuote = errorStringForQuote;
                        ovcToAdd.ovcNumber = i;
                        oVCs.add(ovcToAdd);
                        system.debug('!!! ovcToAdd '+ovcToAdd);
                        ovcCnt++;
                    }


                
                //DF_Quote__c quoteObjUpd = [SELECT Id, Name, Status__c, Bulk_Order_Error__c FROM DF_Quote__c WHERE Id = :quoteId FOR UPDATE];
                //quoteObjUpd.Status__c = 'Ready for Order';
                //quoteObjUpd.Bulk_Order_Error__c = '';
                //update quoteObjUpd;

                if(quoteObj.Id != null){

                    allBulkOVCs.addAll(oVCs);
                    system.debug('!!! allBulkOVCs '+allBulkOVCs);

                }
            }// Basket isEmpty
        }//try
        catch(Exception e){
            DF_Quote__c dfQuotObj = [SELECT Id, Status__c, Bulk_Order_Error__c FROM DF_Quote__c WHERE Id = :quoteId FOR UPDATE];
            dfQuotObj.Status__c = 'Error';
            dfQuotObj.Bulk_Order_Error__c = 'Error updating product configurations';
            update dfQuotObj;//logMessage(String logLevel, String sourceClass, String sourceFunction, String referenceId, String referenceInfo, String logMessage, String payLoad, Exception ex, long timeTaken) 
           
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_BulkOrder_UpdateBasket.class.getName(), 'updateProdConfig', '', '', '', '', e, 0);
        }
    }


    
}