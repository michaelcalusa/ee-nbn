@isTest
public class DF_BulkOrderBatch_Test {
	@isTest
    static void test_BulkOrderBatch(){



        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = DF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;
        // Create Product Definition
        cscfga__Product_Definition__c prodDef = DF_TestData.buildProductDefinition('Direct Fibre - Product Charges', '');
        insert prodDef;

        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig = DF_TestData.buildProductConfig(prodDef.Id);
        prodConfig.Name = 'Direct Fibre - Product Charges';
        prodConfig.cscfga__Product_Basket__c = prodBasket.Id;
        insert prodConfig;

        DF_Custom_Options__c setting = new DF_Custom_Options__c();
		setting.Name = 'SF_PRODUCT_CHARGE_DEFINITION_ID';
		setting.Value__c = prodDef.id;
		insert setting;

        // Create Oppty
        Opportunity oppty1 = DF_TestData.createOpportunity('LOC111111111111');
        oppty1.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty1;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket1 = DF_TestData.buildBasket();
        prodBasket1.cscfga__Opportunity__c = oppty1.Id;
        insert prodBasket1;
        
        // Create Oppty
        Opportunity oppty2 = DF_TestData.createOpportunity('LOC111111111111');
        oppty2.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty2;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket2 = DF_TestData.buildBasket();
        prodBasket2.cscfga__Opportunity__c = oppty2.Id;
        insert prodBasket2;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'A');  
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuote1.Fibre_Build_Category__c = 'A';
        dfQuote1.QuoteType__c = 'Connect';
        dfQuote1.BulkOVCCount__c = 1;
        dfQuote1.LAPI_Response__c = '{"systemId":"AzGAVIC420702950","structuredAddressList":[],"Status":"Valid","sfSearchType":"SearchByLocationID","sfRequestId":"a6b5D0000002JmtQAE","serviceType":"Fibre to the building","samID":"2BLT-03","RAG":null,"primaryTechnology":"Copper","pniException":null,"OLT_ID":null,"OLT_Exists":null,"Longitude":"11.111111","locLatLong":"1.111, 2.111","LocId":"LOC000000000001","Latitude":"1.111111","isNoFibreJointFound":null,"id":"LOC000000000001","fsaID":"2BLT","fibreJointTypeDescription":null,"fibreJointTypeCode":"TYCO_OFDC_A4","fibreJointStatus":"INSERVICE","fibreJointLatLong":"1.111111, 11.111111","fibreJointId":"3CBR-25-01-BJL-016","Distance":1,"derivedTechnology":"FTTB","csaId":"CSA180000001111","assetOwner":"NBNCO","assetLong":"11.111111","assetLat":"1.111111","AddressList":["TELSTRA BUILDING  UNIT unit001 1 road001 ST NSW 22001"],"AccessTechnology":null}';
        dfQuoteList.add(dfQuote1);

        DF_Quote__c dfQuote2 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty1.Id, opptyBundle.Id, 1000, 'A');        
        dfQuote2.Proceed_to_Pricing__c = true;
        dfQuote2.Fibre_Build_Category__c = 'A';
        dfQuote2.QuoteType__c = 'Connect';
        dfQuote2.BulkOVCCount__c = 1;
        dfQuote2.LAPI_Response__c = '{"systemId":"AzGAVIC420702950","structuredAddressList":[],"Status":"Valid","sfSearchType":"SearchByLocationID","sfRequestId":"a6b5D0000002JmtQAE","serviceType":"Fibre to the building","samID":"2BLT-03","RAG":null,"primaryTechnology":"Copper","pniException":null,"OLT_ID":null,"OLT_Exists":null,"Longitude":"11.111111","locLatLong":"1.111, 2.111","LocId":"LOC000000000001","Latitude":"1.111111","isNoFibreJointFound":null,"id":"LOC000000000001","fsaID":"2BLT","fibreJointTypeDescription":null,"fibreJointTypeCode":"TYCO_OFDC_A4","fibreJointStatus":"INSERVICE","fibreJointLatLong":"1.111111, 11.111111","fibreJointId":"3CBR-25-01-BJL-016","Distance":1,"derivedTechnology":"FTTB","csaId":"CSA180000001111","assetOwner":"NBNCO","assetLong":"11.111111","assetLat":"1.111111","AddressList":["TELSTRA BUILDING  UNIT unit001 1 road001 ST NSW 22001"],"AccessTechnology":null}';
        dfQuoteList.add(dfQuote2);
        
        DF_Quote__c dfQuote3 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty2.Id, opptyBundle.Id, 1000, 'A');        
        dfQuote3.Proceed_to_Pricing__c = true;
        dfQuote3.Fibre_Build_Category__c = 'A';
        dfQuote3.QuoteType__c = 'Connect';
        dfQuote3.BulkOVCCount__c = 1;
        dfQuote3.LAPI_Response__c = '{"systemId":"AzGAVIC420702950","structuredAddressList":[],"Status":"Valid","sfSearchType":"SearchByLocationID","sfRequestId":"a6b5D0000002JmtQAE","serviceType":"Fibre to the building","samID":"2BLT-03","RAG":null,"primaryTechnology":"Copper","pniException":null,"OLT_ID":null,"OLT_Exists":null,"Longitude":"11.111111","locLatLong":"1.111, 2.111","LocId":"LOC000000000001","Latitude":"1.111111","isNoFibreJointFound":null,"id":"LOC000000000001","fsaID":"2BLT","fibreJointTypeDescription":null,"fibreJointTypeCode":"TYCO_OFDC_A4","fibreJointStatus":"INSERVICE","fibreJointLatLong":"1.111111, 11.111111","fibreJointId":"3CBR-25-01-BJL-016","Distance":1,"derivedTechnology":"FTTB","csaId":"CSA180000001111","assetOwner":"NBNCO","assetLong":"11.111111","assetLat":"1.111111","AddressList":["TELSTRA BUILDING  UNIT unit001 1 road001 ST NSW 22001"],"AccessTechnology":null}';
        dfQuoteList.add(dfQuote3);
        
        insert dfQuoteList; 

		List<Id> quoteIdLst = new List<Id>();

        for(DF_Quote__c quoteObj: dfQuoteList){
        	quoteIdLst.add(quoteObj.Id);
        } 
		system.debug(JSON.serialize(quoteIdLst));
		
        String inception1 = '{"a6aO00000005qOhIAI":{"SiteCon2":{"SiteCon2Number":"412345691","SiteCon2Email":"kam12@email.com","SiteCon2SurName":"kam12","SiteCon2FirstName":"Bri"},"SiteCon1":{"SiteCon1Number":"412345902","SiteCon1Email":"brij11@email.com","SiteCon1SurName":"Pandey1","SiteCon1FirstName":"Brijmohan1"},"BusinessCon2":{"BusinessCon2PostCode":"3000","BusinessCon2State":"VIC","BusinessCon2StreetType":"Melb","BusinessCon2Suburb":"Mel","BusinessCon2StreetName":"Queen","BusinessCon2StreetNumber":"14","BusinessCon2Number":"412345678","BusinessCon2Email":"mathewhayden123@email.com","BusinessCon2Role":"businesscontact","BusinessCon2SurName":"hayden6","BusinessCon2FirstName":"mathew3"},"BusinessCon1":{"BusinessCon1PostCode":"3000","BusinessCon1State":"VIC","BusinessCon1Suburb":"Melb","BusinessCon1StreetType":"Apartment","BusinessCon1Number":"412345703","BusinessCon1StreetName":"Queen","BusinessCon1StreetNumber":"12","BusinessCon1Email":"kane1@email.com","BusinessCon1Role":"business contact","BusinessCon1SurName":"pointing1","BusinessCon1FirstName":"Kane1"},"OVC8":{"OVC8MappingMode":"","OVC8CoSLow":"","OVC8CoSMedium":"","OVC8CoSHigh":"","OVC8UNIVLANId":"","OVC8SVLANId":"","OVC8NNIGroupId":"","OVC8RouteType":""},"OVC7":{"OVC7MappingMode":"","OVC7CoSLow":"","OVC7CoSMedium":"","OVC7CoSHigh":"","OVC7UNIVLANId":"","OVC7SVLANId":"","OVC7NNIGroupId":"","OVC7RouteType":""},"OVC6":{"OVC6MappingMode":"","OVC6CoSLow":"","OVC6CoSMedium":"","OVC6CoSHigh":"","OVC6UNIVLANId":"","OVC6SVLANId":"","OVC6NNIGroupId":"","OVC6RouteType":""},"OVC5":{"OVC5MappingMode":"","OVC5CoSLow":"","OVC5CoSMedium":"","OVC5CoSHigh":"","OVC5UNIVLANId":"","OVC5SVLANId":"","OVC5NNIGroupId":"","OVC5RouteType":""},"OVC4":{"OVC4MappingMode":"","OVC4CoSLow":"","OVC4CoSMedium":"","OVC4CoSHigh":"","OVC4UNIVLANId":"","OVC4SVLANId":"","OVC4NNIGroupId":"","OVC4RouteType":""},"OVC3":{"OVC3MappingMode":"","OVC3CoSLow":"","OVC3CoSMedium":"","OVC3CoSHigh":"","OVC3UNIVLANId":"","OVC3SVLANId":"","OVC3NNIGroupId":"","OVC3RouteType":""},"OVC2":{"OVC2MappingMode":"DSCP","OVC2CoSLow":"10","OVC2CoSMedium":"300","OVC2CoSHigh":"500","OVC2UNIVLANId":"1004","OVC2SVLANId":"0","OVC2NNIGroupId":"NNI090000015449","OVC2RouteType":"Local"},"OVC1":{"OVC1MappingMode":"PCP","OVC1CoSLow":"100","OVC1CoSMedium":"300","OVC1CoSHigh":"200","OVC1UNIVLANId":"1002","OVC1SVLANId":"0","OVC1NNIGroupId":"NNI090000015449","OVC1RouteType":"State"},"Order":{"SalesforceDFQId":"a6aO00000005qOhIAI","InstallationNotes":"","PowerSupply2":"Not Required (NR)","PowerSupply1":"AC (240V)","NTDMounting":"Wall","CustomerRequiredDate":"30/04/2019","SiteNotes":"site Notes","SecurityRequired":"No","InductionRequired":"No","HeritageSite":"Yes","TradingName":"test","UNIServiceRestorationSLA":"Premium - 4 (24/7)","UNITerm":"12","UNITPID":"0x88a8","UNIOVCType":"Access EVPL","UNIInterfaceTypes":"Optical (Single mode)","AfterHoursSiteVisit":"Yes","Address":"TELSTRA BUILDING  UNIT unit93 93 road93 ST NSW 2293","QuoteId":"EEQ-0000007033","LocationId":"LOC000000000093"}}}';
	//	inception1.replace('a6aO00000005qOhIAI', quoteIdLst[0]);

		String inception2 = '{"a6aO00000005qOkIAI":{"SiteCon2":{"SiteCon2Number":"412345691","SiteCon2Email":"kam12@email.com","SiteCon2SurName":"kam12","SiteCon2FirstName":"Bri"},"SiteCon1":{"SiteCon1Number":"412345902","SiteCon1Email":"brij11@email.com","SiteCon1SurName":"Pandey1","SiteCon1FirstName":"Brijmohan1"},"BusinessCon2":{"BusinessCon2PostCode":"3000","BusinessCon2State":"VIC","BusinessCon2StreetType":"Melb","BusinessCon2Suburb":"Mel","BusinessCon2StreetName":"Queen","BusinessCon2StreetNumber":"14","BusinessCon2Number":"412345678","BusinessCon2Email":"mathewhayden123@email.com","BusinessCon2Role":"businesscontact","BusinessCon2SurName":"hayden6","BusinessCon2FirstName":"mathew3"},"BusinessCon1":{"BusinessCon1PostCode":"3000","BusinessCon1State":"VIC","BusinessCon1Suburb":"Melb","BusinessCon1StreetType":"Apartment","BusinessCon1Number":"412345703","BusinessCon1StreetName":"Queen","BusinessCon1StreetNumber":"12","BusinessCon1Email":"kane1@email.com","BusinessCon1Role":"business contact","BusinessCon1SurName":"pointing1","BusinessCon1FirstName":"Kane1"},"OVC8":{"OVC8MappingMode":"","OVC8CoSLow":"","OVC8CoSMedium":"","OVC8CoSHigh":"","OVC8UNIVLANId":"","OVC8SVLANId":"","OVC8NNIGroupId":"","OVC8RouteType":""},"OVC7":{"OVC7MappingMode":"","OVC7CoSLow":"","OVC7CoSMedium":"","OVC7CoSHigh":"","OVC7UNIVLANId":"","OVC7SVLANId":"","OVC7NNIGroupId":"","OVC7RouteType":""},"OVC6":{"OVC6MappingMode":"","OVC6CoSLow":"","OVC6CoSMedium":"","OVC6CoSHigh":"","OVC6UNIVLANId":"","OVC6SVLANId":"","OVC6NNIGroupId":"","OVC6RouteType":""},"OVC5":{"OVC5MappingMode":"","OVC5CoSLow":"","OVC5CoSMedium":"","OVC5CoSHigh":"","OVC5UNIVLANId":"","OVC5SVLANId":"","OVC5NNIGroupId":"","OVC5RouteType":""},"OVC4":{"OVC4MappingMode":"","OVC4CoSLow":"","OVC4CoSMedium":"","OVC4CoSHigh":"","OVC4UNIVLANId":"","OVC4SVLANId":"","OVC4NNIGroupId":"","OVC4RouteType":""},"OVC3":{"OVC3MappingMode":"","OVC3CoSLow":"","OVC3CoSMedium":"","OVC3CoSHigh":"","OVC3UNIVLANId":"","OVC3SVLANId":"","OVC3NNIGroupId":"","OVC3RouteType":""},"OVC2":{"OVC2MappingMode":"DSCP","OVC2CoSLow":"10","OVC2CoSMedium":"300","OVC2CoSHigh":"500","OVC2UNIVLANId":"1004","OVC2SVLANId":"0","OVC2NNIGroupId":"NNI090000015449","OVC2RouteType":"Local"},"OVC1":{"OVC1MappingMode":"PCP","OVC1CoSLow":"100","OVC1CoSMedium":"300","OVC1CoSHigh":"200","OVC1UNIVLANId":"1002","OVC1SVLANId":"0","OVC1NNIGroupId":"NNI090000015449","OVC1RouteType":"State"},"Order":{"SalesforceDFQId":"a6aO00000005qOkIAI","InstallationNotes":"","PowerSupply2":"Not Required (NR)","PowerSupply1":"AC (240V)","NTDMounting":"Wall","CustomerRequiredDate":"30/04/2019","SiteNotes":"site Notes","SecurityRequired":"No","InductionRequired":"No","HeritageSite":"Yes","TradingName":"test","UNIServiceRestorationSLA":"Premium - 4 (24/7)","UNITerm":"12","UNITPID":"0x88a8","UNIOVCType":"Access EVPL","UNIInterfaceTypes":"Optical (Single mode)","AfterHoursSiteVisit":"Yes","Address":"TELSTRA BUILDING  UNIT unit96 96 road96 ST NSW 2296","QuoteId":"EEQ-0000007036","LocationId":"LOC000000000096"}}}';
	//	inception2.replace('a6aO00000005qOhIAI', quoteIdLst[1]);

        String inception3 = '{"a6aO00000005qOkIAI":{"SiteCon2":{"SiteCon2Number":"412345691","SiteCon2Email":"kam12@email.com","SiteCon2SurName":"kam12","SiteCon2FirstName":"Bri"},"SiteCon1":{"SiteCon1Number":"412345902","SiteCon1Email":"brij11@email.com","SiteCon1SurName":"Pandey1","SiteCon1FirstName":"Brijmohan1"},"BusinessCon2":{"BusinessCon2PostCode":"3000","BusinessCon2State":"VIC","BusinessCon2StreetType":"Melb","BusinessCon2Suburb":"Mel","BusinessCon2StreetName":"Queen","BusinessCon2StreetNumber":"14","BusinessCon2Number":"412345678","BusinessCon2Email":"mathewhayden123@email.com","BusinessCon2Role":"businesscontact","BusinessCon2SurName":"hayden6","BusinessCon2FirstName":"mathew3"},"BusinessCon1":{"BusinessCon1PostCode":"3000","BusinessCon1State":"VIC","BusinessCon1Suburb":"Melb","BusinessCon1StreetType":"Apartment","BusinessCon1Number":"412345703","BusinessCon1StreetName":"Queen","BusinessCon1StreetNumber":"12","BusinessCon1Email":"kane1@email.com","BusinessCon1Role":"business contact","BusinessCon1SurName":"pointing1","BusinessCon1FirstName":"Kane1"},"OVC8":{"OVC8MappingMode":"","OVC8CoSLow":"","OVC8CoSMedium":"","OVC8CoSHigh":"","OVC8UNIVLANId":"","OVC8SVLANId":"","OVC8NNIGroupId":"","OVC8RouteType":""},"OVC7":{"OVC7MappingMode":"","OVC7CoSLow":"","OVC7CoSMedium":"","OVC7CoSHigh":"","OVC7UNIVLANId":"","OVC7SVLANId":"","OVC7NNIGroupId":"","OVC7RouteType":""},"OVC6":{"OVC6MappingMode":"","OVC6CoSLow":"","OVC6CoSMedium":"","OVC6CoSHigh":"","OVC6UNIVLANId":"","OVC6SVLANId":"","OVC6NNIGroupId":"","OVC6RouteType":""},"OVC5":{"OVC5MappingMode":"","OVC5CoSLow":"","OVC5CoSMedium":"","OVC5CoSHigh":"","OVC5UNIVLANId":"","OVC5SVLANId":"","OVC5NNIGroupId":"","OVC5RouteType":""},"OVC4":{"OVC4MappingMode":"","OVC4CoSLow":"","OVC4CoSMedium":"","OVC4CoSHigh":"","OVC4UNIVLANId":"","OVC4SVLANId":"","OVC4NNIGroupId":"","OVC4RouteType":""},"OVC3":{"OVC3MappingMode":"","OVC3CoSLow":"","OVC3CoSMedium":"","OVC3CoSHigh":"","OVC3UNIVLANId":"","OVC3SVLANId":"","OVC3NNIGroupId":"","OVC3RouteType":""},"OVC2":{"OVC2MappingMode":"","OVC2CoSLow":"","OVC2CoSMedium":"","OVC2CoSHigh":"","OVC2UNIVLANId":"","OVC2SVLANId":"","OVC2NNIGroupId":"","OVC2RouteType":""},"OVC1":{"OVC1MappingMode":"PCP","OVC1CoSLow":"100","OVC1CoSMedium":"300","OVC1CoSHigh":"200","OVC1UNIVLANId":"1002","OVC1SVLANId":"0","OVC1NNIGroupId":"NNI090000015449","OVC1RouteType":"State"},"Order":{"SalesforceDFQId":"a6aO00000005qOkIAI","InstallationNotes":"","PowerSupply2":"Not Required (NR)","PowerSupply1":"AC (240V)","NTDMounting":"Wall","CustomerRequiredDate":"","SiteNotes":"site Notes","SecurityRequired":"No","InductionRequired":"No","HeritageSite":"Yes","TradingName":"test","UNIServiceRestorationSLA":"Premium - 4 (24/7)","UNITerm":"12","UNITPID":"0x88a8","UNIOVCType":"Access EPL","UNIInterfaceTypes":"OpticalSingle mode)","AfterHoursSiteVisit":"Yes","Address":"TELSTRA BUILDING  UNIT unit96 96 road96 ST NSW 2296","QuoteId":"EEQ-0000007036","LocationId":"LOC000000000096"}}}';
        
        Map<Id, Map<String, Map<String, String>>> in1Map = (Map<Id, Map<String, Map<String, String>>>) JSON.deserialize(inception1, Map<Id, Map<String, Map<String, String>>>.class);
		Map<Id, Map<String, Map<String, String>>> in2Map = (Map<Id, Map<String, Map<String, String>>>) JSON.deserialize(inception2, Map<Id, Map<String, Map<String, String>>>.class);
        Map<Id, Map<String, Map<String, String>>> in3Map = (Map<Id, Map<String, Map<String, String>>>) JSON.deserialize(inception3, Map<Id, Map<String, Map<String, String>>>.class);
        
		Map<Id, Map<String, Map<String, String>>> inceptionMap = new Map<Id, Map<String, Map<String, String>>>();
        inceptionMap.put(quoteIdLst[0], in1Map.get('a6aO00000005qOhIAI'));
        inceptionMap.put(quoteIdLst[1], in2Map.get('a6aO00000005qOkIAI'));
        inceptionMap.put(quoteIdLst[2], in3Map.get('a6aO00000005qOkIAI'));
        
        List<DF_Quote__c> quoteToUpdLst = new List<DF_Quote__c>();
		for(DF_Quote__c quoteObjUpd: [SELECT Id, Status__c, BulkOrderJSON__c, BulkOVCCount__c FROM DF_Quote__c WHERE Id IN :quoteIdLst]){
			if (quoteObjUpd.Id == quoteIdLst[0]){
                quoteObjUpd.BulkOrderJSON__c = inception1;
                quoteObjUpd.BulkOVCCount__c = 2;
                quoteObjUpd.Status__c = 'Processing';
                quoteToUpdLst.add(quoteObjUpd);
            }
            if (quoteObjUpd.Id == quoteIdLst[1]){
                quoteObjUpd.BulkOrderJSON__c = inception2;
                quoteObjUpd.BulkOVCCount__c = 2;
                quoteObjUpd.Status__c = 'Processing';
                quoteToUpdLst.add(quoteObjUpd);
            }
		} 

		update quoteToUpdLst;
        
        Map<Id, Integer> bulkOVCCntMap = new Map<Id, Integer>();
        bulkOVCCntMap.put(quoteIdLst[0], 2);
        bulkOVCCntMap.put(quoteIdLst[1], 2);
        Map<Id, List<String>> missingReqFields = new Map<Id, List<String>>();

        DF_BulkOrderBatch batchBulkOrder = new DF_BulkOrderBatch(quoteIdLst, inceptionMap, bulkOVCCntMap,missingReqFields);
        test.startTest();
        Database.executeBatch(batchBulkOrder);
        test.stopTest();
    }
}