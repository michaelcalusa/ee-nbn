@isTest
public class DF_BulkOrder_FormatValidations_Test {

    @isTest public static void testBulkOrder_Format(){
    // Setup data
        String csvContent = 'LocationId,QuoteId,Address,AfterHoursSiteVisit,UNIInterfaceTypes,UNIOVCType,UNITPID,UNITerm,UNIServiceRestorationSLA,OVC1RouteType,OVC1NNIGroupId,OVC1SVLANId,OVC1UNIVLANId,OVC1CoSHigh,OVC1CoSMedium,OVC1CoSLow,OVC1MappingMode,OVC2RouteType,OVC2NNIGroupId,OVC2SVLANId,OVC2UNIVLANId,OVC2CoSHigh,OVC2CoSMedium,OVC2CoSLow,OVC2MappingMode,OVC3RouteType,OVC3NNIGroupId,OVC3SVLANId,OVC3UNIVLANId,OVC3CoSHigh,OVC3CoSMedium,OVC3CoSLow,OVC3MappingMode,OVC4RouteType,OVC4NNIGroupId,OVC4SVLANId,OVC4UNIVLANId,OVC4CoSHigh,OVC4CoSMedium,OVC4CoSLow,OVC4MappingMode,OVC5RouteType,OVC5NNIGroupId,OVC5SVLANId,OVC5UNIVLANId,OVC5CoSHigh,OVC5CoSMedium,OVC5CoSLow,OVC5MappingMode,OVC6RouteType,OVC6NNIGroupId,OVC6SVLANId,OVC6UNIVLANId,OVC6CoSHigh,OVC6CoSMedium,OVC6CoSLow,OVC6MappingMode,OVC7RouteType,OVC7NNIGroupId,OVC7SVLANId,OVC7UNIVLANId,OVC7CoSHigh,OVC7CoSMedium,OVC7CoSLow,OVC7MappingMode,OVC8RouteType,OVC8NNIGroupId,OVC8SVLANId,OVC8UNIVLANId,OVC8CoSHigh,OVC8CoSMedium,OVC8CoSLow,OVC8MappingMode,BusinessCon1FirstName,BusinessCon1SurName,BusinessCon1Role,BusinessCon1Email,BusinessCon1StreetNumber,BusinessCon1StreetName,BusinessCon1Number,BusinessCon1StreetType,BusinessCon1Suburb,BusinessCon1State,BusinessCon1PostCode,SiteCon1FirstName,SiteCon1SurName,SiteCon1Email,SiteCon1Number,BusinessCon2FirstName,BusinessCon2SurName,BusinessCon2Role,BusinessCon2Email,BusinessCon2Number,BusinessCon2StreetNumber,BusinessCon2StreetName,BusinessCon2Suburb,BusinessCon2StreetType,BusinessCon2State,BusinessCon2PostCode,SiteCon2FirstName,SiteCon2SurName,SiteCon2Email,SiteCon2Number,TradingName,HeritageSite,InductionRequired,SecurityRequired,SiteNotes,CustomerRequiredDate,NTDMounting,PowerSupply1,PowerSupply2,InstallationNotes,SalesforceDFQId';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000000000001,EEQ-0000001227,HILLSIDE NURSING HOME  UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290,No,Optical (Single mode),Access EPL,0x88a8,12,Premium - 12 (24/7),Local,NNI123456789123,4,12,500,100,,DSCP,Local,NNI123456789,4,12,500,100,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,fname,surname,business contact,test@email.com,12,Queen,412345678,Apartment,Melb,vic,3000,test,test,test@email.com,412345678,fname,surname2,businesscontact,test@email.com,412345678,12,Queen,Mel,Melb,VIC,3000,test,test,test@email.com,412345678,test,test,No,No,,5/03/2019,Rack,AC (240V),DC (-48V),,a6a5D0000004OEdQAM';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000000000002,EEQ-0000001228,HILLSIDE NURSING HOME  UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290,OUI,Optical (Single mode),Access EVPL,0x88a8,12,Premium - 12 (24/7),Local,NNI123456789123,6,34,200,,,,Local,NNI123456789,9999,9999,200,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,ram,kum,business contact,asdemail.com,12,King,345678,Apartment,Melb,TN,30002,Kum,raj,kmsdemail.com,412345678,,,,,,,,,,,,,,,,test,test,No,No,,7/03/2020,Rack,AC (240V),DC(-48V),,a6a5D0000004OEeQAM';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000000000003,EEQ-0000001229,HILLSIDE NURSING HOME  UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290,,Optical Single mode,Access IPL,0x88a11,13 months,Premium - 12 (48/7),National,NNI123456789,6,34,220,12,14,,Local,NNI123456789,9999,9999,200,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,ram,kum,business contact,asdemail.com,12,King,345678,Apartment,Melb,TN,30002,Kum,raj,kmsdemail.com,412345678,,,,,,,,,,,,,,,,test,test,No,No,,17/13/2020,Rack,AC (240V),DC(-48V to -72V),,a6a5D0000004OEfQAM';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000000000004,EEQ-0000001230,HILLSIDE NURSING HOME  UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290,,Optical (Single mode),Access EVPL,,12,Premium - 12 (24/7),Local,NNI123456789123,6,,200,,,iMap,Local,NNI123456789,9999,9999,200,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,ram,kum,business contact,asdemail.com,12,King,345678,Apartment,Melb,TN,30002,Kum,raj,kmsdemail.com,412345678,,,,,,,,,,,,,,,,test,test,No,No,,7/03/2020,Rack,AC (240V),DC(-48V),,a6a5D0000004OEeQAM';
        
        
        Blob blobContents = Blob.valueOf(csvContent);
        String	strBase64Contents = EncodingUtil.base64Encode(blobContents);
        String base64Data = EncodingUtil.urlDecode(strBase64Contents, 'UTF-8');
        List<List<String>> csvContents = CSVFileUtil.parseCSV(EncodingUtil.base64Decode(base64Data),false);
        test.startTest();
        DF_BulkOrder_FormatValidations.innerWrapper mapWrap = new DF_BulkOrder_FormatValidations.innerWrapper();
        mapWrap = DF_BulkOrder_FormatValidations.processCSVHeaders(csvContents);
        test.stopTest();                
        system.AssertEquals(3, mapWrap.quoteErrMapRet.keySet().size());	
        system.AssertEquals(3, mapWrap.inceptionMapRet.keySet().size());
    } 

    @isTest public static void test_calculateTotalOVCBW(){
        Map<String, Map<String, String>> OVCMap = new Map<String, Map<String, String>> ();
        Map<String, String> ovcInstance = new Map<String, String> ();
        String ovcInstString= 'OVC1CoSHigh=1000, OVC1CoSLow=, OVC1CoSMedium=1000, OVC1MappingMode=PCP, OVC1NNIGroupId=NNI000000072445, OVC1RouteType=Local, OVC1SVLANId=0, OVC1UNIVLANId=2346';
        ovcInstance.put('OVC1CoSHigh', '1000');
        ovcInstance.put('OVC1CoSMedium', '1000');
        ovcInstance.put('OVC1CoSLow', '0');
        OVCMap.put('OVC1', ovcInstance);
        Test.startTest();
        Integer totalBW = DF_BulkOrder_FormatValidations.calculateTotalOVCBW(OVCMap,'OVC1');
        Test.stopTest();
        System.assertEquals(1250,totalBW);
    }
}