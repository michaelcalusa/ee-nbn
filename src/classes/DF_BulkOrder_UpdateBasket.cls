//**************************************************************
// Author: Rajaramachandran Sethu
// Date: 21 MAR 2019
// Purpose: To update CS Basket info with Bulk Order attributes
//**************************************************************
public class DF_BulkOrder_UpdateBasket {
    public static String ovcName = '';
    public static String ovcId = '';
    public static String CSA = '';
    public static String cosRecurring = '';
    public static String routeRecurring = '';
    public static String jsonOVCNB = '';
    public static Integer ovcCnt = 0;
    public static Integer ovcCntPV = 0;
    public static String ovcAddResult = '';
    public static Set<Id> configIds = new Set<Id>();
//    public Static List<String> nbAttrValLst = new List<String>();
    public static List<String> ovcDefnLst = new List<String>{'OVC1', 'OVC2', 'OVC3', 'OVC4', 'OVC5', 'OVC6', 'OVC7', 'OVC8'};
//    public Static Map<String, List<String>> nbAttrValMap = new Map<String, List<String>>();
    public Static Map<String, Map<String,String>> nbAttrValMap = new Map<String, Map<String,String>>();
    public static Map<String, String> nbAttrMap = new Map<String, String>{'OVCId'=>'','CSA'=>''
            ,'OVCName'=>'','routeType'=>'','coSHighBandwidth'=>'0','coSMediumBandwidth'=>'0'
            ,'coSLowBandwidth'=>'0','routeRecurring'=>'0','routeRecurring'=>'0','coSRecurring'=>'0','POI'=>''
            ,'status'=>'Incomplete','mappingMode'=>'PCP','NNIGroupId'=>'','sTag'=>'0','ceVlanId'=>'0'
            ,'ovcMaxFrameSize'=>'Jumbo (9000 Bytes)'};

    @future(callout=true)            
	public static void updateProdConfig(String quoteId){
        String pcDefId = DF_CS_API_Util.getCustomSettingValue('SF_PRODUCT_CHARGE_DEFINITION_ID');
        String ovcDefId = DF_CS_API_Util.getCustomSettingValue('SF_OVC_DEFINITION_ID');
        
        try{    
            DF_Quote__c quoteObj = [SELECT Id, Name, Location_Id__c, LAPI_Response__c, Opportunity__c, Status__c, Bulk_Order_Error__c, BulkOrderJSON__c, BulkOVCCount__c FROM DF_Quote__c WHERE Id = :quoteId];
            DF_ServiceFeasibilityResponse sfResp = (DF_ServiceFeasibilityResponse)System.JSON.deserialize(quoteObj.LAPI_Response__c, DF_ServiceFeasibilityResponse.class);
            CSA = sfResp.csaId;
            
            Map<Id, Map<String, Map<String, String>>> inceptionMap = (Map<Id, Map<String, Map<String, String>>>) JSON.deserialize(quoteObj.BulkOrderJSON__c, Map<Id, Map<String, Map<String, String>>>.class);
            List<cscfga__Product_Basket__c> baskets = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :quoteObj.Opportunity__c];

            Integer totalOVC = Integer.valueOf(quoteObj.BulkOVCCount__c);
            String custLabelOVC = Label.DF_BulkOrder_OVC;
         	totalOVC = totalOVC> Integer.valueOf(custLabelOVC) ? Integer.valueOf(custLabelOVC):totalOVC;
            system.debug('@@@ totalOVC : '+ totalOVC);
            if(!baskets.isEmpty()) {
                String basketId = !baskets.isEmpty() ? baskets.get(0) != null ? baskets.get(0).Id : null : null;    
                
                cscfga__Product_Configuration__c pcConfigObj = [Select Id, Name from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and cscfga__Product_Definition__c = :pcDefId LIMIT 1];
                String pcConfigId = pcConfigObj.Id;
                
                sObject ovcObject = new cscfga__Product_Definition__c();
                String queryString = DF_CS_API_Util.getQuery(ovcObject, ' WHERE Id = '+ '\'' + ovcDefId + '\'');
                List<cscfga__Product_Definition__c> ovcList = (List<cscfga__Product_Definition__c>)DF_CS_API_Util.getQueryRecords(queryString); 
                cscfga__Product_Definition__c ovc = !ovcList.isEmpty() ? ovcList.get(0) : null;
                System.debug('PPPP ovc: '+ovc);
                
            //    cscfga.API_1.ApiSession apiSession = DF_CS_API_Util.createApiSession(baskets.get(0));  
                
                if(totalOVC>1 && ovc != null){
                    cscfga.API_1.ApiSession apiSessionNew = DF_CS_API_Util.createApiSession(baskets.get(0));
                    for(Integer i=2; i<=totalOVC; i++){
                        apiSessionNew.setProductToConfigure(new cscfga__Product_Definition__c(Id = ovcDefId), new Map<String, String> {'containerType' => 'basket', 'linkedId' => basketId, 'configId' => pcConfigId});
                        cscfga.ProductConfiguration currConfig = apiSessionNew.getRootConfiguration();
                        cscfga.ProductConfiguration relatedConfig =  apiSessionNew.AddRelatedProduct(ovc.Name, ovcDefId);
                        if(String.isNotBlank(CSA)){
                            for(cscfga.Attribute att : relatedConfig.getAttributes()) {
                                if(att.getName() == 'CSA'){
                                    att.setValue(CSA);            
                                    att.setDisplayValue(CSA);
                                }
                            }
                        }
                        apiSessionNew.executeRules();
                        cscfga.ValidationResult vr = apiSessionNew.validateConfiguration();
                        apiSessionNew.persistConfiguration(true);
                        system.debug('@@@ Configs Added Result : '+ vr);
                    }
                    apiSessionNew.close();
                }
                
                cscfga.API_1.ApiSession apiSession = DF_CS_API_Util.createApiSession(baskets.get(0));
           //     apiSession.setProductToConfigure(new cscfga__Product_Definition__c(Id = ovcDefId), new Map<String, String> {'containerType' => 'basket', 'linkedId' => basketId, 'configId' => pcConfigId});
                
                for(cscfga__Product_Configuration__c configs: [Select id, Name from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and (cscfga__Product_Definition__c = :pcDefId OR cscfga__Product_Definition__c = :ovcDefId)]){
                    if(!configs.Name.containsIgnorecase('OVC')){
                        configIds.add(configs.Id);
                        apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = configs.Id, cscfga__Product_Basket__c = basketId));        
                        cscfga.ProductConfiguration currConfig = apiSession.getConfiguration();
            
                        for(cscfga.Attribute pcAttr : currConfig.getAttributes()) {
                            System.debug('related Attribute name is : ' + pcAttr.getName());
                            if(pcAttr.getName() == 'After Hours Site Visit'){
                                pcAttr.setValue(inceptionMap.get(quoteId).get('Order').get('AfterHoursSiteVisit'));            
                                pcAttr.setDisplayValue(inceptionMap.get(quoteId).get('Order').get('AfterHoursSiteVisit'));
                            }
                            if(pcAttr.getName() == 'eSLA'){
                                pcAttr.setValue(inceptionMap.get(quoteId).get('Order').get('UNIServiceRestorationSLA'));          
                                pcAttr.setDisplayValue(inceptionMap.get(quoteId).get('Order').get('UNIServiceRestorationSLA'));
                            }
                            if(pcAttr.getName() == 'Term'){
                                pcAttr.setValue(inceptionMap.get(quoteId).get('Order').get('UNITerm'));           
                                pcAttr.setDisplayValue(inceptionMap.get(quoteId).get('Order').get('UNITerm'));
                            }
                        }
                    }else if(configs.Name.containsIgnorecase('OVC')){
                        apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = configs.Id, cscfga__Product_Basket__c = basketId));        
                        cscfga.ProductConfiguration currConfig = apiSession.getConfiguration();
                    	system.debug('@@@ OVCName : '+ configs.Name);
                        configIds.add(configs.Id);
                        for(cscfga.Attribute ovcAttr : currConfig.getAttributes()) {
                            System.debug('related Attribute name is : ' + ovcAttr.getName());
                            if(ovcAttr.getName() == 'CoS High'){
                                if(!String.isBlank(inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'CoSHigh'))){
                                    ovcAttr.setValue(inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'CoSHigh'));            
                                    ovcAttr.setDisplayValue(inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'CoSHigh'));
                                }else{
                                    ovcAttr.setValue('0');            
                                    ovcAttr.setDisplayValue('0');    
                                }
                            }
                            if(ovcAttr.getName() == 'CoS Medium'){
                                if(!String.isBlank(inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'CoSMedium'))){
                                    ovcAttr.setValue(inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'CoSMedium'));            
                                    ovcAttr.setDisplayValue(inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'CoSMedium'));
                                }else{
                                    ovcAttr.setValue('0');            
                                    ovcAttr.setDisplayValue('0');    
                                }
                            }
                            if(ovcAttr.getName() == 'CoS Low'){
                                if(!String.isBlank(inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'CoSLow'))){
                                    ovcAttr.setValue(inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'CoSLow'));            
                                    ovcAttr.setDisplayValue(inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'CoSLow'));
                                }else{
                                    ovcAttr.setValue('0');            
                                    ovcAttr.setDisplayValue('0');    
                                }
                            }
                            if(ovcAttr.getName() == 'Route Type'){
                                ovcAttr.setValue(inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'RouteType'));            
                                ovcAttr.setDisplayValue(inceptionMap.get(quoteId).get(ovcDefnLst[ovcCnt]).get(ovcDefnLst[ovcCnt]+'RouteType'));
                            }
                            if(ovcAttr.getName() == 'CSA'){
								ovcAttr.setValue(CSA);
                                ovcAttr.setDisplayValue(CSA);
                            }
                        }
                        ovcCnt++;
                    }
                    apiSession.executeRules(); // run the rules after setting the attribute values
                    apiSession.validateConfiguration();
                    apiSession.persistConfiguration(true);
                }//For configs
                
           //     apiSession.executeRules(); // run the rules after setting the attribute values
           //     apiSession.validateConfiguration();
           //     apiSession.persistConfiguration(true);
                apiSession.close();
           
                cscfga.ProductConfigurationBulkActions.revalidateConfigurations(configIds);
                
                for(cscfga__Product_Configuration__c postValConfigs: [Select id, Name, cscfga__Configuration_Status__c, (select cscfga__Value__c, Name from cscfga__Attributes__r) from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and cscfga__Product_Definition__c = :ovcDefId]){
                    if(postValConfigs.Name.containsIgnorecase('OVC')){
                    	Map<String, String> nbAttrValMapOVC = new Map<String, String>();
                        nbAttrValMapOVC.put('Id', postValConfigs.Id);
                        nbAttrValMapOVC.put('Name', postValConfigs.Name);
                        nbAttrValMapOVC.put('ConfigStat', postValConfigs.cscfga__Configuration_Status__c); 
                        for(cscfga__Attribute__c postvalAttr : postValConfigs.cscfga__Attributes__r) {
                            if(postvalAttr.Name.equalsIgnoreCase('Hidden_TotalCoS_Charges')){
                                nbAttrValMapOVC.put('CoSRec', postvalAttr.cscfga__Value__c);
                            }
                            if(postvalAttr.Name.equalsIgnoreCase('Total_Routing_Charge_afterDiscount')){
                                nbAttrValMapOVC.put('RouteRec', postvalAttr.cscfga__Value__c);
                            }
                            if(postvalAttr.Name.equalsIgnoreCase('POI')){
                                nbAttrValMapOVC.put('POI', postvalAttr.cscfga__Value__c);
                            }
                        }
                        nbAttrValMap.put(ovcDefnLst[ovcCntPV], nbAttrValMapOVC);
                        ovcCntPV++;
                    }
                }
                
                DF_Quote__c quoteObjUpd = [SELECT Id, Name, Status__c, Bulk_Order_Error__c FROM DF_Quote__c WHERE Id = :quoteId FOR UPDATE];
                quoteObjUpd.Status__c = 'Ready for Order';
                quoteObjUpd.Bulk_Order_Error__c = '';
                update quoteObjUpd;

                if(quoteObj.Id != null){
                    updateNBValues(quoteId, inceptionMap.get(quoteId), nbAttrValMap, ovcCntPV);
                    associateContacts(quoteId, inceptionMap.get(quoteId));
                }
            }// Basket isEmpty
        }//try
        catch(Exception e){
            DF_Quote__c dfQuotObj = [SELECT Id, Status__c, Bulk_Order_Error__c FROM DF_Quote__c WHERE Id = :quoteId FOR UPDATE];
            dfQuotObj.Status__c = 'Error';
            dfQuotObj.Bulk_Order_Error__c = 'Error updating product configurations';
            update dfQuotObj;
        }
    }

    public static void updateNBValues(String quoteId, Map<String, Map<String, String>> deepMap, Map<String, Map<String, String>> nbAttrValMap, Integer totalOVC){
        Integer nbCnt=0;
        List<String> ovcNBLst = new List<String>();
        for(nbCnt=0; nbCnt<totalOVC; nbCnt++){
            nbAttrMap.put('OVCId',nbAttrValMap.get(ovcDefnLst[nbCnt]).get('Id')!=null?nbAttrValMap.get(ovcDefnLst[nbCnt]).get('Id'):'');
            nbAttrMap.put('OVCName', nbAttrValMap.get(ovcDefnLst[nbCnt]).get('Name')!=null?nbAttrValMap.get(ovcDefnLst[nbCnt]).get('Name'):'');
            nbAttrMap.put('status', nbAttrValMap.get(ovcDefnLst[nbCnt]).get('ConfigStat')!=null?nbAttrValMap.get(ovcDefnLst[nbCnt]).get('ConfigStat'):'');            
            nbAttrMap.put('coSRecurring',nbAttrValMap.get(ovcDefnLst[nbCnt]).get('CoSRec')!=null?nbAttrValMap.get(ovcDefnLst[nbCnt]).get('CoSRec'):'0');
            nbAttrMap.put('routeRecurring',nbAttrValMap.get(ovcDefnLst[nbCnt]).get('RouteRec')!=null?nbAttrValMap.get(ovcDefnLst[nbCnt]).get('RouteRec'):'0');
            nbAttrMap.put('POI',nbAttrValMap.get(ovcDefnLst[nbCnt]).get('POI')!=null?nbAttrValMap.get(ovcDefnLst[nbCnt]).get('POI'):'');
            nbAttrMap.put('coSHighBandwidth', deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'CoSHigh')!=null?deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'CoSHigh'):'0');
            nbAttrMap.put('coSMediumBandwidth', deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'CoSMedium')!=null?deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'CoSMedium'):'0');
            nbAttrMap.put('coSLowBandwidth', deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'CoSLow')!=null?deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'CoSLow'):'0');
            nbAttrMap.put('routeType', deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'RouteType')!=null?deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'RouteType'):'');
            nbAttrMap.put('NNIGroupId', deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'NNIGroupId')!=null?deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'NNIGroupId'):'');
            nbAttrMap.put('ceVlanId', deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'UNIVLANId')!=null?deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'UNIVLANId'):'0');
            nbAttrMap.put('sTag', deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'SVLANId')!=null?deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'SVLANId'):'0');
            nbAttrMap.put('mappingMode', deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'MappingMode')!=null?deepMap.get(ovcDefnLst[nbCnt]).get(ovcDefnLst[nbCnt]+'MappingMode'):'');
            nbAttrMap.put('CSA', CSA);
            if(JSON.serialize(nbAttrMap)!=null) {
                jsonOVCNB =  JSON.serialize(nbAttrMap);
                ovcNBLst.add(jsonOVCNB);
            }
        }
        if(!ovcNBLst.isEmpty()){
            jsonOVCNB = String.valueOf(ovcNBLst).removeStart('(');
            jsonOVCNB = jsonOVCNB.removeEnd(')');
        }
        
        DF_Order__c ordObj = [SELECT Id, OVC_NonBillable__c FROM DF_Order__c WHERE DF_Quote__c = :quoteId];
        ordObj.OVC_NonBillable__c = '['+jsonOVCNB+']';
        update ordObj;
    }
    
    public static void associateContacts(String quoteId, Map<String, Map<String, String>> deepMap){
        
        Map<String, String> interimBusCon1Map = new Map<String, String>(deepMap.get('BusinessCon1'));
        if(returnMapSize(interimBusCon1Map)>0){
            interimBusCon1Map.put('busContStreetAddress', deepMap.get('BusinessCon1').get('BusinessCon1StreetNumber') +' '+
                                  deepMap.get('BusinessCon1').get('BusinessCon1StreetName')+ ' '+
                                  deepMap.get('BusinessCon1').get('BusinessCon1StreetType'));
            interimBusCon1Map.remove('BusinessCon1StreetNumber');
            interimBusCon1Map.remove('BusinessCon1StreetName');
            interimBusCon1Map.remove('BusinessCon1StreetType');
            createContactDetails(interimBusCon1Map, 'BusinessCon1', 'busCont', quoteId);
        }
        Map<String, String> interimBusCon2Map = new Map<String, String>(deepMap.get('BusinessCon2'));
        if(returnMapSize(interimBusCon2Map)>0){
            interimBusCon2Map.put('busContStreetAddress', deepMap.get('BusinessCon2').get('BusinessCon2StreetNumber') +' '+
                                  deepMap.get('BusinessCon2').get('BusinessCon2StreetName')+ ' '+
                                  deepMap.get('BusinessCon2').get('BusinessCon2StreetType'));
            interimBusCon2Map.remove('BusinessCon2StreetNumber');
            interimBusCon2Map.remove('BusinessCon2StreetName');
            interimBusCon2Map.remove('BusinessCon2StreetType');
            createContactDetails(interimBusCon2Map, 'BusinessCon2', 'busCont', quoteId);
        }
        Map<String, String> interimSiteCon1Map = new Map<String, String>(deepMap.get('SiteCon1'));
        if(returnMapSize(interimSiteCon1Map)>0){
            createContactDetails(interimSiteCon1Map, 'SiteCon1', 'siteCont', quoteId);
        }
        Map<String, String> interimSiteCon2Map = new Map<String, String>(deepMap.get('SiteCon2'));
        if(returnMapSize(interimSiteCon2Map)>0){
            createContactDetails(interimSiteCon2Map, 'SiteCon2', 'siteCont', quoteId);
        }
    }
    
    private static void createContactDetails(Map<String,String> contactMap, String replaceFrom, String replaceTo, Id qId){
        String contactJson = JSON.serialize(contactMap);
        contactJson = contactJson.replace(replaceFrom, replaceTo);
        system.debug('@@@ Contact JSON : '+ contactJson);
        system.debug('@@@ Quote Id : '+ qId);

        if(replaceFrom.containsIgnorecase('BusinessCon')){
            String bConId = DF_ContactDetailsController.addBusinessContactToContactsAndOppContactRole('Business Contact', contactJson, qId);
        }
        if(replaceFrom.containsIgnorecase('SiteCon')){
            String sConId = DF_ContactDetailsController.addSiteContactToContactsAndOppContactRole('Site Contact', contactJson, qId);
        }
    }

    private static Integer returnMapSize(Map<String, String> inputMap){
        List<String> ContactValueLst = new List<String>();
        ContactValueLst = inputMap.values();
        Set<String> ContactValueSet = new Set<String>(ContactValueLst);
        removeNullVal(ContactValueSet);

        return ContactValueSet.size();
    }

    private static void removeNullVal(Set<String> ipSet){
        Set<String> nullSet = new Set<String>();
        nullSet.add('');
        nullSet.add(null);
        ipSet.removeAll(nullSet);
    }
}