public with sharing class DF_BusinessContactDetails{
//wrapper class for the site details screen in the
            //'busContId' : maxbusContId + 1,     
            //'busContFirstName': 'Ben',
            //'busContSurname': 'Simmons',
            //'busContRole': 'CEO',
            //'busContEmail': 'bsimmons@test.com',
            //'busContNumber': '(03) 9366 9988',
            //'busContStreetAddress': '17 NICHOLSON ST',
            //'busContSuburb': 'NEW NORFOLK',
            //'busContPostcode': '7140',
            //'busContState': 'Tasmania'
    
    @AuraEnabled
    public String busContId {get;set;}
    @AuraEnabled
    public String busContFirstName {get;set;}
    @AuraEnabled
    public String busContSurname {get;set;}
    @AuraEnabled
    public String busContRole {get;set;}
    @AuraEnabled
    public String busContEmail {get;set;}
    @AuraEnabled
    public String busContNumber {get;set;}
    @AuraEnabled
    public String busContStreetAddress {get;set;}
    @AuraEnabled
    public String busContSuburb {get;set;}
    @AuraEnabled
    public String busContPostcode {get;set;}
    @AuraEnabled
    public String busContState {get;set;}

     public DF_BusinessContactDetails(String id, String contactFirstName, String contactSurname,  String contactRole, 
     									String contactEmail, String contactPhone, String streetAddress, String suburb, String postCode, String state ){

        busContId = id;
        busContFirstName = contactFirstName;
        busContSurname = contactSurname;
        busContRole = contactRole;
        busContNumber = contactPhone;
        busContEmail = contactEmail;
        busContRole = contactRole;
        busContStreetAddress = streetAddress;
        busContSuburb = suburb;
        busContPostcode = postCode;
        busContState = state;

     }
     public DF_BusinessContactDetails(){}

}