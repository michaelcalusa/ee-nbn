/**
* Class for testing Cloudsense API Util methods.
*/
@isTest
public class DF_CS_API_Util_Test {

    /**
	* Tests createApiSession method
	*/
    /*@isTest static void createApiSessionTest()
    {
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        
        Test.startTest();
        cscfga.API_1.ApiSession apiSession = CS_API_Util.createApiSession(basket);
        System.debug('PP apiSession:'+apiSession);
        Test.stopTest();
    }
    
    /**
	* Tests createBasket method
	*/
    /*@isTest static void createBasketTest()
    {
		Test.startTest();
        cscfga__Product_Basket__c basket = CS_API_Util.createBasket(null);
        System.debug('PP basket:'+basket);
        Test.stopTest();
    }
    
    /**
	* Tests addProductConfigToBasket method
	*/
    /*@isTest static void addProductConfigToBasketTest()
    {
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        String pdtDefinitionId;
        List<cscfga__Product_Configuration__c> productConfigList = [SELECT cscfga__Product_Definition__c FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basket.Id];
        if(!productConfigList.isEmpty()){
            pdtDefinitionId = productConfigList.get(0).cscfga__Product_Definition__c;
        
            Test.startTest();
        
        cscfga.ProductConfiguration pdtConfig = CS_API_Util.addProductConfigToBasket(pdtDefinitionId, null);
        System.debug('PP pdtConfig:'+pdtConfig);
        Test.stopTest();
        }
    }
    */
    /**
	* Tests findProdDefinition method
	*/
    @isTest static void findProdDefinitionTest()
    {
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        Id pdtDefinitionId;
        List<cscfga__Product_Configuration__c> productConfigList = [SELECT cscfga__Product_Definition__c FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basket.Id];
        if(!productConfigList.isEmpty()){
            pdtDefinitionId = productConfigList.get(0).cscfga__Product_Definition__c;
        
        Test.startTest();
        
        List<cscfga__Product_Definition__c> pdtDefinitionList = DF_CS_API_Util.findProdDefinition(pdtDefinitionId);
        System.debug('PP pdtDefinitionList:'+pdtDefinitionList);
        Test.stopTest();
        }
    }
    
    /**
	* Tests getProductBasketConfigs method
	*/
    @isTest static void getProductBasketConfigsTest()
    {
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        
        Test.startTest();
        
        List<cscfga__Product_Configuration__c> prodConfigList = DF_CS_API_Util.getProductBasketConfigs(basket.Id);
        System.debug('PP prodConfigList:'+prodConfigList);
        Test.stopTest();
    }
}