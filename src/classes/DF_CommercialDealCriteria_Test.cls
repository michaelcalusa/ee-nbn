@IsTest
public class DF_CommercialDealCriteria_Test {
    
    @isTest static void testCommercialDealTest(){
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        Commercial_Deal__c cmdeal = DF_TestData.commercialDeal(acc.id);
        insert cmdeal;
        Commercial_Deal__c cmdeal2 = DF_TestData.commercialDeal(acc.id);
        cmdeal2.Active__c = False;
        insert cmdeal2;
        Commercial_Deal_Criteria__c cmdealcriteria = DF_TestData.commercialDealCriteria(cmdeal.Id);
        cmdealcriteria.Criteria_Value__c = 'LOC000006231845';
        insert cmdealcriteria;
        Commercial_Deal_Criteria__c cmdealcriteria2 = DF_TestData.commercialDealCriteria(cmdeal2.Id);
        cmdealcriteria2.Criteria_Value__c = 'LOC000006231845';
        insert cmdealcriteria2;
        cmdeal2.Active__c = True;
        try{
            update cmdeal2;
        }catch(DMLException e) {
            System.Assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }
        
    }

}