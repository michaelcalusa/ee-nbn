/*================================================
    * @Class Name : DF_CommercialDealCriterialTrigger_Util
    * @author : Gagan
    * @Purpose: This is a generic class to call methods used in Commercial Deal Criteria
    * @created date:October 09 2018
    * @Last modified date:22/10/2018
    * @Last modified by : Suman Gunaganti
==================================================*/
Public with sharing Class DF_CommercialDealCriterialTrigger_Util {
    
    public static void commercialDealCriteria(List<Commercial_Deal_Criteria__c> newCriterialist, Map<Id, Commercial_Deal__c> commercialDealMap, Boolean isCommercialDealContext){
        Set<Id> commercialDealIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();
        if(!isCommercialDealContext){
            for(Commercial_Deal_Criteria__c commdealCriteria: newCriterialist){
                commercialDealIds.add(commdealCriteria.Commercial_Deal__c);
        	}
        	commercialDealMap = New Map <Id, Commercial_Deal__c>([Select Id, Start_Date__c, End_Date__c, Account__c From Commercial_Deal__c
                                                         Where Id IN:commercialDealIds And
                                                         Active__c = True]);
        }
        for(Commercial_Deal__c cmd: commercialDealMap.values()){
            if(cmd.Account__c <> Null) accountIds.add(cmd.Account__c);
        }
        List <Commercial_Deal_Criteria__c> dealCriteriaList = [Select id,Name,Commercial_Deal__c,Criteria__c, Criteria_Value__c,Start_Date__c,End_Date__c,
                                                                  Commercial_Deal__r.Name,Commercial_Deal__r.Active__c, Commercial_Deal__r.Account__c,Commercial_Deal__r.End_Date__c,Commercial_Deal__r.Start_Date__c
                                                                  from Commercial_Deal_Criteria__c
                                                                  where Commercial_Deal__r.Account__c IN:accountIds and 
                                                                  Commercial_Deal__r.Active__c = True limit:Limits.getLimitQueryRows()];                                                          
        for(Commercial_Deal_Criteria__c dealCriteria: newCriterialist){
            if(commercialDealMap.containsKey(dealCriteria.Commercial_Deal__c)){
                for(Commercial_Deal_Criteria__c existingDealCriteria: dealCriteriaList){
                    if(dealCriteria.Start_Date__c <= existingDealCriteria.End_Date__c &&
                       dealCriteria.End_Date__c >= existingDealCriteria.Start_Date__c &&
                       commercialDealMap.get(dealCriteria.Commercial_Deal__c).Account__c == existingDealCriteria.Commercial_Deal__r.Account__c &&
                       dealCriteria.Criteria__c ==  existingDealCriteria.Criteria__c &&
                       dealCriteria.Criteria_Value__c == existingDealCriteria.Criteria_Value__c &&
                       existingDealCriteria.Commercial_Deal__r.Active__c == True &&
                       dealCriteria.Id != existingDealCriteria.Id &&
                       commercialDealMap.get(dealCriteria.Commercial_Deal__c).Start_Date__c <= existingDealCriteria.Commercial_Deal__r.End_Date__c &&
                       commercialDealMap.get(dealCriteria.Commercial_Deal__c).End_Date__c >= existingDealCriteria.Commercial_Deal__r.Start_Date__c){
                           if(!isCommercialDealContext)
                           	   dealCriteria.adderror('This ' + dealCriteria.Criteria__c + ' is already assigned to deal '+ existingDealCriteria.Commercial_Deal__r.Name +' for the same timeframe');
                           else
                               commercialDealMap.get(dealCriteria.Commercial_Deal__c).adderror(existingDealCriteria.Criteria_Value__c + ' is already assigned to deal '+ existingDealCriteria.Commercial_Deal__r.Name +' for the same timeframe. Please Contact BSM Programs to assist');
                               
                       }
                }
            }
        }
    }
}