public with sharing class DF_Constants {
    
    public static final String RAG_STATUS_RED = 'RED'; 
    public static final String FB_CATEGORY_C = 'C';
    public static final String FB_CATEGORY_A = 'A'; 
    public static final String FB_CATEGORY_B = 'B';
    public static final String FEASIBILITY_EXPIRED = 'Feasibility Expired';
    public static final String FIBRE_BUILD_COST = 'Fibre_Build_Cost__c';
    public static final String EXPIRED = 'Expired';
    public static final String CLOSED_LOST = 'Closed Lost';
    public static final String BUSINESS_HOURS_EXPIRE_SF = 'EE Expire Service Feasibility Organization hours';
    public static final String BUSINESS_DAYS = ' Business Days';
    public static final String APPAIN_DESKTOP_ASSESSMENT_STATUS_PENDING = 'PENDING';
    public static final String APPAIN_DESKTOP_ASSESSMENT_STATUS_IN_PROCESS = 'IN PROGRESS';
    public static final String APPAIN_DESKTOP_ASSESSMENT_STATUS_COMPLETED = 'COMPLETED';
    public static final String APPIAN_REQUEST_HANDLER = 'AppianSFRequestHandler';
    public static final String ASYNC_REQUEST_COMPLETE = 'Processed';

    public static final String QUOTE_STATUS_QUICK_QUOTE = 'Quick Quote';
    public static final String QUOTE_STATUS_ORDER_DRAFT = 'Order Draft';
    public static final String ORDER_DRAFT = 'In Draft';
    public static final String QUOTE_STATUS_PENDING_SUBMISSION = 'Pending Submission';
    public static final String QUOTE_STATUS_SUBMITTED = 'Submitted';
    public static final String QUOTE_STATUS_ACCEPTED = 'Accepted';

    public static final String API_NAME_LAPI = 'LAPI'; 
    public static final String API_NAME_CIS = 'CIS'; 

    public static final String DF_ORDER_STATUS_SUBMITTED = 'Submitted';
    public static final String DF_ORDER_STATUS_IN_PROGRESS = 'InProgress';
    public static final String DF_ORDER_STATUS_ACKNOWLEDGED = 'Acknowledged';
    public static final String DF_ORDER_STATUS_COMPLETE = 'Complete';
    public static final String DF_ORDER_STATUS_CANCEL_INITIATED = 'Cancel Initiated';
    public static final String DF_ORDER_STATUS_CANCELLED = 'Cancelled';

    public static final String DF_ORDER_SUB_STATUS_ORDER_SCHEDULED = 'OrderScheduled';
    public static final String DF_ORDER_SUB_STATUS_CONSTRUCTION_STARTED = 'ConstructionStarted';
    public static final String DF_ORDER_SUB_STATUS_SERVICE_TEST_COMPLETED = 'ServiceTestCompleted';
    public static final String DF_ORDER_SUB_STATUS_ORDER_ACCEPTED = 'OrderAccepted';
    public static final String ORDER_TYPE_CONNECT = 'Connect'; 

    
    public static final String ORDER_STATUS_COMPLETE = 'Complete';

    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_UNDEFINED = 'Undefined';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_NULL = 'Null';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_SATELLITE = 'Satellite';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_WIRELESS = 'Wireless';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FIBRE = 'Fibre';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_HFC = 'HFC';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_COPPER = 'Copper';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_HFC_PILOT = 'HFC Pilot';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FTTC = 'FTTC';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FIBRE_TO_THE_CURB = 'Fibre to the curb';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FTTB = 'FTTB';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FIBRE_TO_THE_BUILDING = 'Fibre to the building';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FIXED_WIRELESS = 'Fixed Wireless';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FIBRE_TO_THE_NODE = 'Fibre to the node';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FTTN = 'FTTN';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FTTP = 'FTTP';
    
    public static final String NAMED_CRED_PREFIX = 'callout:';
    public static final String HTTP_METHOD_GET = 'GET';
    public static final String HTTP_METHOD_POST = 'POST';
    public static final String HTTP_HEADER_ACCEPT = 'ACCEPT';    
    public static final String CONTENT_TYPE = 'Content-Type';
    public static final String CONTENT_TYPE_JSON = 'application/json';     
    public static final String NBN_DOMAIN = 'https://www.nbnco.com.au';
    
    public static final Integer HTTP_STATUS_200 = 200;
    public static final Integer HTTP_STATUS_201 = 201;
    public static final Integer HTTP_STATUS_202 = 202;
    public static final Integer HTTP_STATUS_203 = 203;
    public static final Integer HTTP_STATUS_204 = 204;
    public static final Integer HTTP_STATUS_205 = 205;
    public static final Integer HTTP_STATUS_206 = 206;
    public static final Integer HTTP_STATUS_404 = 404;
    public static final Integer HTTP_STATUS_500 = 500;
    
    public static final String ERROR = 'Error';   
    public static final String INFO = 'Info';  

    public static final String ACTIVITY_NAME = 'createBillingEvent';
    public static final String MSG_NAME = 'ManageBillingEventcreateBillingEventRequest';
    public static final String BUSINESS_SERVICE_NAME = 'ManageBillingEvent';
    public static final String BUSINESS_SERVICE_VERSION = 'V2.0';
    public static final String MSG_TYPE = 'Request';
    public static final String SECURITY = 'Placeholder Security';
    public static final String COMMUNICATION_PATTERN = 'RequestReply';
    public static final String BUSINESS_CHANNEL = 'Enterprise Fulfilment';
    public static final String ORDER_PRIORITY = '6';
    public static final String NOTIFICATION_TYPE = 'BillingEvent';
    public static final String INTERACTION_STATUS = 'Complete';
    public static final String ORDER_TYPE = 'Connect'; 
    public static final String ACTION = 'ADD';
    public static final String SITE_SURVEY_NAME = 'Site Survey Charges';
    public static final String SITE_SURVEY_PRODUCT_TYPE = 'Fibre';
    
    public static final String BUSINESS_PERMISSION_SET = 'DF_BC_Customer_Permission';
    public static final String BUSINESS_PLUS_PERMISSION_SET = 'DF_BC_Customer_Plus_Permission';
    public static final String ENTERPRISE_ETHERNET_NAME = 'Enterprise Ethernet';
    public static final String ENTERPRISE_ETHERNET_RECORDTYPE_NAME = 'Enterprise Ethernet';
    public static final String NBN_SELECT_RECORDTYPE_NAME = 'NBN Select';
    public static final String ENTERPRISE_ETHERNET_GROUP_NAME = 'BSM_EE_';
    public static final String NBN_SELECT_GROUP_NAME = 'BSM_NS_';
    public static final String OPPORTUNITY_BUNDLE_OBJECT = 'DF_Opportunity_Bundle__c';
    public static final String QUOTE_OBJECT = 'DF_Quote__c';
    public static final String ORDER_OBJECT = 'DF_Order__c';
    public static final String FBC_RULE_OBJECT = 'DF_FBC_Rule__c';

    //Order Line Item/Service Line Item Name
    public static final String FibreBuildContribution = 'Fibre Build Contribution';
    public static final String FibreBuildContributionEEAS = 'Fibre Build Contribution - EEAS';
    public static final string FBCCommercialDiscount = 'Fibre Build Contribution - Commercial Deal Discount';
    public static final string SiteSurveyRebate = 'Site Survey Rebate';
    public static final string AfterHoursAppointment = 'After Hours Appointment';
    public static final string ZoneCharges = 'Zone Charges';
    public static final string cosHighCharges = 'Cos High Charges';
    public static final string cosHighChargesEEAS = 'Cos High Charges - EEAS';
    public static final string cosHighChargesCommercialDiscount = 'CoS High Charges - Commercial Deal Discount';
    public static final string cosHighChargesTermDiscount = 'CoS High Charges - Term Discount';
    public static final string cosMediumCharges = 'Cos Medium Charges' ;
    public static final string cosMediumChargesEEAS= 'Cos Medium Charges - EEAS';
    public static final string cosMediumChargesCommercialDiscount = 'CoS Medium Charges - Commercial Deal Discount';
    public static final string cosMediumChargesTermDiscount = 'CoS Medium Charges - Term Discount';
    public static final string cosLowCharges = 'Cos Low Charges' ;
    public static final string cosLowChargesEEAS= 'Cos Low Charges - EEAS';
    public static final string cosLowChargesCommercialDiscount = 'CoS Low Charges - Commercial Deal Discount';
    public static final string cosLowChargesTermDiscount = 'CoS Low Charges - Term Discount';
    public static final string uniSetUpFee = 'Uni Setup Fee' ;
    public static final string uniSetUpFeeEEAS = 'Uni Setup Fee - EEAS' ;
    public static final string uniSetUpFeeCommercialDiscount = 'UNI Setup Fee - Commercial Deal Discount' ;
    public static final string uniSetUpFeeTermDiscount = 'UNI Setup Fee - Term Discount' ;
    public static final string routeTypeCharges = 'Route Type Charges';
    public static final string routeTypeChargesEEAS = 'Route Type Charges - EEAS';
    public static final string routeTypeChargesTermDiscount = 'Route Type Charges - Term Discount';
    public static final string eSLAchargesEEAS = 'eSLA Charges - EEAS';
    public static final string zoneChargesEEAS = 'Zone Charges - EEAS';
    public static final string zoneChargesCommercialDiscount = 'Zone Charges - Commercial Deal Discount';
    public static final string zoneChargesTermDiscount = 'Zone Charges - Term Discount';
    public static final string siteSurveyCharges = 'Site Survey Charges';
    public static final string siteSurveyChargesCommercialDiscount = 'Site Survey Charges - Commercial Deal Discount';
    public static final string CANCEL_OPPORTUNITY_NAME = 'Cancellation Charge';

    public static final String DIRECT_FIBRE_PRODUCT_CHARGES_NAME = 'Direct Fibre - Product Charges';
    public static final String DIRECT_FIBRE_BUILD_CONTRIBUTION_NAME = 'Direct Fibre - Build Contribution';
    public static final String OPPORTUNITY_OBJECT = 'Opportunity';
    public static final String Product_Name = 'productName';
    public static final String EE_Product_Name = 'EEAS';
    public static final String MODIFY = 'Modify';
    public static final String SERVICE_MODIFICATION_CHARGE_EEAS = 'Service Modification Charges - EEAS';
    public static final String QUOTE_TYPE_CONNECT = 'Connect'; 
    public static final String UNI_VLAN_ID_MODIFY_SUBTYPE = 'Change UNI VLAN ID'; 

    public static final String TEMPLATE_ORDER_ACCEPTED = 'EE Order Accepted';
    public static final String TEMPLATE_ORDER_ACKNOWLEDGED = 'EE Order Acknowledged';
    public static final String TEMPLATE_ORDER_COMPLETED = 'EE Order Completed';
    public static final String TEMPLATE_ORDER_CONSTRUCTION_STARTED = 'EE Order Construction Started';
    public static final String TEMPLATE_ORDER_SCHEDULED = 'EE Order Scheduled';
    public static final String TEMPLATE_ORDER_SERVICE_TEST_COMPLETED = 'EE Order Service Test Completed';
    public static final String TEMPLATE_ORDER_CANCEL_INITIATED = 'EE Order Cancel Initiated';
    public static final String TEMPLATE_ORDER_CANCELLED = 'EE Order Cancelled';
    public static final String TEMPLATE_ORDER_PENDING = 'EE Order Pending';
    public static final String TEMPLATE_ORDER_PENDING_COMPLETED = 'EE Order Pending Completed';
    public static final String TEMPLATE_ORDER_BULK_UPLOAD_PROCESSING_COMPLETE = 'EE Order Bulk Upload Processing Complete';
    //CPST-7870 | michaelcalusa@nbnco.com.au | Send email (RDD)
    public static final String TEMPLATE_ORDER_RESCHEDULED = 'EE Order Rescheduled';
    public static final String TEMPLATE_ORDER_ON_SCHEDULE = 'EE Order On Schedule';
    
    public static final String MODIFY_OVC_BANDWIDTH = 'Change CoS Profile & Bandwidth';
    public static final String MODIFY_ADD_OVC = 'Add new OVC';

    public static final String RECORD_TYPE_SERVICE_INCIDENT = 'Service Incident';
    public static final String RECORD_TYPE_SERVICE_REQUEST = 'Service Request';
    public static final String RECORD_TYPE_NETWORK_INCIDENT = 'Network Incident';
    public static final String RECORD_TYPE_SERVICE_ALERT = 'Service Alert';
    public static final String RECORD_TYPE_CHANGE_REQUEST = 'Change Request';

    // Template Types
    public static final String TEMPLATE_NETWORK_INCIDENT_STATUS_NEW = 'EE AS Unplanned Outage New';
    public static final String TEMPLATE_NETWORK_INCIDENT_STATUS_IN_PROGRESS = 'EE AS Unplanned Outage In Progress';
    public static final String TEMPLATE_NETWORK_INCIDENT_STATUS_CLOSED = 'EE AS Unplanned Outage Closed';
    public static final String TEMPLATE_NETWORK_INCIDENT_STATUS_RESOLVED = 'EE AS Unplanned Outage Resolved';

    public static final String TEMPLATE_SERVICE_ALERT_STATUS_NEW = 'EE AS Service Alert New';
    public static final String TEMPLATE_SERVICE_ALERT_STATUS_IN_PROGRESS = 'EE AS Service Alert In Progress';
    public static final String TEMPLATE_SERVICE_ALERT_STATUS_CLOSED = 'EE AS Service Alert Closed';
    public static final String TEMPLATE_SERVICE_ALERT_STATUS_RESOLVED = 'EE AS Service Alert Resolved';
    
    public static final String TEMPLATE_CHANGE_REQUEST_STATUS_NEW = 'EE AS Change Request New';
    public static final String TEMPLATE_CHANGE_REQUEST_STATUS_IN_PROGRESS = 'EE AS Change Request In Progress';
    public static final String TEMPLATE_CHANGE_REQUEST_STATUS_CLOSED = 'EE AS Change Request Closed';

    //Service Incident specific templates
    public static final String TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_ACKNOWLEDGED = 'EE AS Service Incident Acknowledged';
    public static final String TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_ACCEPTED = 'EE AS Service Incident Accepted';
    public static final String TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_CREATED = 'EE AS Service Incident Created';
    public static final String TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_NBN_ACTION_REQUIRED = 'EE AS Service Incident NBN Action Required';
    public static final String TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_NBN_ACTION_COMPLETED = 'EE AS Service Incident NBN Action Completed';
    public static final String TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_TICKET_UPDATED_NBNCO = 'EE AS Service Incident Ticket Updated NBNCo';
    public static final String TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_RESOLVED = 'EE AS Service Incident Resolved';
    public static final String TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_CLOSED= 'EE AS Service Incident Closed';
    public static final String TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_REJECTED= 'EE AS Service Incident Rejected';
    public static final String TEMPLATE_SERVICE_INCIDENT_NOTIFIC_TYPE_ADDITIONAL_INFO= 'EE AS Service Incident Additional Information Required';
    
    //Service Incident specific Notification Types
    public static final String SERVICE_INCIDENT_NOTIFIC_TYPE_ACKNOWLEDGED = 'IncidentAcknowledged';
    public static final String SERVICE_INCIDENT_NOTIFIC_TYPE_ACCEPTED = 'IncidentAccepted';
    public static final String SERVICE_INCIDENT_NOTIFIC_TYPE_CREATED = 'IncidentCreated';
    public static final String SERVICE_INCIDENT_NOTIFIC_TYPE_NBN_ACTION_REQUIRED = 'NBNActionRequired';
    public static final String SERVICE_INCIDENT_NOTIFIC_TYPE_NBN_ACTION_COMPLETED = 'NBNActionCompleted';
    public static final String SERVICE_INCIDENT_NOTIFIC_TYPE_TICKET_UPDATED_NBNCO = 'TicketUpdatedNBNCo';
    public static final String SERVICE_INCIDENT_NOTIFIC_TYPE_RESOLVED = 'IncidentResolved';
    public static final String SERVICE_INCIDENT_NOTIFIC_TYPE_CLOSED= 'IncidentClosed';
    public static final String SERVICE_INCIDENT_NOTIFIC_TYPE_REJECTED= 'IncidentRejected';
    public static final String SERVICE_INCIDENT_NOTIFIC_TYPE_ADDITIONAL_INFO= 'AdditionalInformationRequired';
 
    //Service Request specific templates
    public static final String TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_ACKNOWLEDGED = 'EE AS Service Request Acknowledged';
    public static final String TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_ACCEPTED = 'EE AS Service Request Accepted';
    public static final String TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_CREATED = 'EE AS Service Request Created';
    public static final String TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_NBN_ACTION_REQUIRED = 'EE AS Service Request NBN Action Required';
    public static final String TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_NBN_ACTION_COMPLETED = 'EE AS Service Request NBN Action Completed';
    public static final String TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_TICKET_UPDATED_NBNCO = 'EE AS Service Request Ticket Updated NBNCo';
    public static final String TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_RESOLVED = 'EE AS Service Request Resolved';
    public static final String TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_CLOSED= 'EE AS Service Request Closed';
    public static final String TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_REJECTED= 'EE AS Service Request Rejected';
    public static final String TEMPLATE_SERVICE_REQUEST_NOTIFIC_TYPE_ADDITIONAL_INFO= 'EE AS Service Request Additional Information Required';

    //Service Request specific Notification Types
    public static final String SERVICE_REQUEST_NOTIFIC_TYPE_ACKNOWLEDGED = 'IncidentAcknowledged';
    public static final String SERVICE_REQUEST_NOTIFIC_TYPE_ACCEPTED = 'IncidentAccepted';
    public static final String SERVICE_REQUEST_NOTIFIC_TYPE_CREATED = 'IncidentCreated';
    public static final String SERVICE_REQUEST_NOTIFIC_TYPE_NBN_ACTION_REQUIRED = 'NBNActionRequired';
    public static final String SERVICE_REQUEST_NOTIFIC_TYPE_NBN_ACTION_COMPLETED = 'NBNActionCompleted';
    public static final String SERVICE_REQUEST_NOTIFIC_TYPE_TICKET_UPDATED_NBNCO = 'TicketUpdatedNBNCo';
    public static final String SERVICE_REQUEST_NOTIFIC_TYPE_RESOLVED = 'IncidentResolved';
    public static final String SERVICE_REQUEST_NOTIFIC_TYPE_CLOSED = 'IncidentClosed';
    public static final String SERVICE_REQUEST_NOTIFIC_TYPE_REJECTED = 'IncidentRejected';
    public static final String SERVICE_REQUEST_NOTIFIC_TYPE_ADDITIONAL_INFO = 'AdditionalInformationRequired';
    public static final String EMAIL_WORKLOG_REPLACEMENT_TOKEN = '{#Note}';
    
    public static final Map<String,RecordType> INCIDENT_RECORD_TYPES = getIncidentRecordTypes();

    private static Map<String,RecordType> getIncidentRecordTypes() {

        List<RecordType> rtypes = [SELECT Id, Name 
                                   FROM RecordType 
                                   WHERE 
                                   sObjectType = 'Incident_Management__c' and 
                                   isActive=true];
     
        Map<String,RecordType> recordTypes = new Map<String,RecordType>{};
        for(RecordType rt: rtypes)
            recordTypes.put(rt.Id, rt);
     
        return recordTypes;
    }
}