public with sharing class DF_ContactDetailsController{



    @AuraEnabled
    //Add contact - handles both insert and updates
    public static String addSiteContactToContactsAndOppContactRole(String contactRoleType, String jsonContactDetails, Id quoteId ){
        Id contactId;
        try{
                DF_ContactDetails contactDetails = (DF_ContactDetails)JSON.deserialize(jsonContactDetails, DF_ContactDetails.class);
                DF_Quote__c dfQuote = [SELECT Opportunity__c FROM DF_Quote__c WHERE Id =:quoteId  LIMIT 1];
                //create new contact 
              //  Id accountId = DF_SF_CaptureController.getCommUserAccountId();
               // Account accountTier = [SELECT Tier__c From Account WHERE Id =:accountId LIMIT 1];
                Id accountRecordTypeId;

                

               // If(accountTier.Tier__c.equals('1')){
                    accountRecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId();
               // } 
               // else{
               //     accountRecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('Downstream RSP Contact').getRecordTypeId();
               // }
                Contact c = new Contact();
               List<Contact> existingContact = new List<Contact>();
               existingContact=[SELECT Id, FirstName,LastName,Email,Phone FROM Contact WHERE FirstName =:contactDetails.siteContFirstName 
                                            AND LastName =:contactDetails.siteContSurname 
                                            AND (Phone =:contactDetails.siteContNumber OR Email =:contactDetails.siteContEmail) AND recordtypeid=:accountRecordTypeId LIMIT 1];
                                            

                if(existingContact.size() == 0){

                
                    
       
                    //else if(existingContact.size()>0){
                    //  c.Id = contactId;
                    //}
                    if(!validateEmail(contactDetails.siteContEmail)){ 
                        DmlException e = new DmlException();
                        e.setMessage('INVALID_EMAIL_ADDRESS');
                        throw e;}
                    c.LastName = contactDetails.siteContSurname;
                    c.FirstName = contactDetails.siteContFirstName;
                    c.RecordTypeId = accountRecordTypeId;
                    c.Email = contactDetails.siteContEmail;
                    c.Phone= contactDetails.siteContNumber;
                   // c.AccountId = DF_SF_CaptureController.getCommUserAccountId();
                    System.debug('!!! '+contactDetails.siteContId );
                    

                    //insert c;   
                    Database.DMLOptions dml = new Database.DMLOptions();
                    dml.DuplicateRuleHeader.AllowSave = true; 
                    Database.SaveResult sr = Database.insert(c,dml);
                    }
                    else 
                    {c = existingContact[0];}
                    
                

                OpportunityContactRole[] ocr = [SELECT Id FROM OpportunityContactRole WHERE ContactId=:c.Id AND  OpportunityId =:dfQuote.Opportunity__c AND  Role =:contactRoleType LIMIT 1];
                

                OpportunityContactRole oppContRole = new OpportunityContactRole();
                if(ocr.size()>0){
                    oppContRole.Id = ocr[0].Id;
                }

                oppContRole.ContactId = c.Id;
                
                if(ocr.size()==0){
                    oppContRole.OpportunityId = dfQuote.Opportunity__c;
                }
                oppContRole.Role = contactRoleType;
                oppContRole.IsPrimary = false;
                upsert oppContRole;

        }
        catch(Exception e){
                System.debug('Error in AddContactToContactsAndOppContactRole: ' + e);
                throw new AuraHandledException(e.getMessage());
        }
        return contactId;

    }
        @AuraEnabled
        //Add contact - handles both insert and updates
    public static String addBusinessContactToContactsAndOppContactRole(String contactRoleType, String jsonContactDetails, Id quoteId){
        Id contactId;
        try{    


                //create new contact 
                DF_Quote__c dfQuote = [SELECT Opportunity__c FROM DF_Quote__c WHERE Id =:quoteId  LIMIT 1];
                DF_BusinessContactDetails contactDetails = (DF_BusinessContactDetails)JSON.deserialize(jsonContactDetails, DF_BusinessContactDetails.class);    
              //  Id accountId = DF_SF_CaptureController.getCommUserAccountId();
                //Account accountTier = [SELECT Tier__c From Account WHERE Id =:accountId LIMIT 1];
                Id accountRecordTypeId;
                accountRecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId();

                Contact c = new Contact();
               List<Contact> existingContact = new List<Contact>();
               existingContact=[SELECT Id, FirstName,LastName,Email,Phone,Title,MailingState,MailingStreet,MailingPostalCode,MailingCity FROM Contact WHERE FirstName =:contactDetails.busContFirstName 
                                            AND LastName =:contactDetails.busContSurname 
                                            AND (Phone =:contactDetails.busContNumber OR Email =:contactDetails.busContEmail) LIMIT 1];
                                            

                
                if(existingContact.size()==0){
                
                if(!validateEmail(contactDetails.busContEmail)){ 
                        DmlException e = new DmlException();
                        e.setMessage('INVALID_EMAIL_ADDRESS');
                        throw e;}
                

                        
                    c.LastName = contactDetails.busContSurname;
                    c.FirstName = contactDetails.busContFirstName;
                    c.RecordTypeId = accountRecordTypeId;
                    c.Email = contactDetails.busContEmail;
                    c.Phone= contactDetails.busContNumber;
                    c.MailingStreet = contactDetails.busContStreetAddress;
                    c.MailingPostalCode = contactDetails.busContPostcode;
                    c.MailingCity = contactDetails.busContSuburb;
                    c.MailingState = contactDetails.busContState;
                   // c.AccountId = DF_SF_CaptureController.getCommUserAccountId();
                    c.Title = contactDetails.busContRole;
                    //insert c;
                    Database.DMLOptions dml = new Database.DMLOptions();
                    dml.DuplicateRuleHeader.AllowSave = true; 
                    Database.SaveResult sr = Database.insert(c,dml);
                     
                     }
                       else 
                       {
                       c=existingContact[0];
                       }    

                OpportunityContactRole[] ocr = [SELECT Id FROM OpportunityContactRole WHERE ContactId=:c.id AND  OpportunityId =:dfQuote.Opportunity__c AND Role =:contactRoleType LIMIT 1 ];
                //Add contact to opportunity

                OpportunityContactRole oppContRole = new OpportunityContactRole();
                
                if(ocr.size() > 0){
                    oppContRole.Id = ocr[0].Id;
                }
                oppContRole.ContactId = c.id;
                if(ocr.size()==0){
                    oppContRole.OpportunityId = dfQuote.Opportunity__c;
                }
                oppContRole.Role = contactRoleType;
                oppContRole.IsPrimary = false;
                upsert oppContRole;
                
                dfQuote.Business_Contact_Name__c = contactDetails.busContFirstName+' '+contactDetails.busContSurname;
                update dfQuote;

        }
        catch(Exception e){
                System.debug('Error in DF_ContactDetailsController.AddContactToContactsAndOppContactRole: ' + e);
                throw new AuraHandledException(e.getMessage());
        }
        return contactId;

    }

    /*  getBusinessContactsAndOppContactRole based on Opp Id and Role
    *
    *
    */
    @AuraEnabled
    public static String getBusinessContactsAndOppContactRole( Id quoteId,String oppName ){
    
        try{
                List<OpportunityContactRole> opptyContactList  = [SELECT ContactId, Contact.FirstName, Contact.LastName, 
                                                                Contact.Email ,Contact.Phone, Contact.MailingStreet, 
                                                                Contact.MailingCity, Contact.MailingPostalCode, Contact.MailingState ,Contact.Title 
                                                                FROM OpportunityContactRole WHERE Role = 'Business Contact' 
                                                                AND Opportunity.Name=:oppName AND OpportunityId IN (SELECT Opportunity__c FROM DF_Quote__c WHERE Id =:quoteId)];

                List<DF_BusinessContactDetails> lstBusinessContactDetails = new List<DF_BusinessContactDetails>();
                // Process List    
                    if (!opptyContactList.isEmpty()) {
                        for(OpportunityContactRole opc :opptyContactList){
                            DF_BusinessContactDetails bContact = new DF_BusinessContactDetails(opc.ContactId,opc.Contact.FirstName,opc.Contact.LastName,opc.Contact.Title,
                                                                                opc.Contact.Email, opc.Contact.Phone,opc.Contact.MailingStreet,
                                                                                opc.Contact.MailingCity,opc.Contact.MailingPostalCode, opc.Contact.MailingState);
                            lstBusinessContactDetails.add(bContact);
                        }
                    }
                String serializedData = json.serialize(lstBusinessContactDetails);
                return serializedData;
        }
        catch(Exception e){
            System.debug('Error in DF_ContactDetailsController.getBusinessContactsAndOppContactRole: ' + e);
            throw new AuraHandledException(e.getMessage());
        }

    }

    @AuraEnabled
    public static String getSiteContactsAndOppContactRole( Id quoteId, String oppName ){
        try{
            List<OpportunityContactRole> opptyContactList = [SELECT ContactId,  Contact.FirstName, Contact.LastName, 
                                                            Role, Contact.Email ,Contact.Phone
                                                            FROM OpportunityContactRole WHERE Role = 'Site Contact' 
                                                            AND Opportunity.Name=:oppName AND OpportunityId IN (SELECT Opportunity__c FROM DF_Quote__c WHERE Id =:quoteId)];

            List<DF_ContactDetails> lstBusinessContactsData = new List<DF_ContactDetails>();

            If(!opptyContactList.isEmpty()){
                for(OpportunityContactRole cd : opptyContactList){
                    DF_ContactDetails sContact = new DF_ContactDetails(cd.ContactId, cd.Contact.FirstName, cd.Contact.LastName,cd.Contact.Phone,cd.Contact.Email);
                    lstBusinessContactsData.add(sContact);
                }
            }


            String serializedData = json.serialize(lstBusinessContactsData);
            System.debug('!!! getSiteContactsAndOppContactRole '+ serializedData);
            return serializedData;
        }
        catch(Exception e){
            System.debug('Error in DF_ContactDetailsController.getSiteContactsAndOppContactRole: ' + e);
            throw new AuraHandledException(e.getMessage());
        }

    }


    @AuraEnabled
    public static void deleteSiteOrBusinessContact(Id contactId){
        Contact c = [SELECT Id FROM Contact WHERE Id =:contactId];
        delete c;
    }

public static Boolean validateEmail(String email) {
    Boolean res = true;
        
    
    String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: <a href="http://www.regular-expressions.info/email.html" target="_blank" rel="nofollow">http://www.regular-expressions.info/email.html</a>
    Pattern MyPattern = Pattern.compile(emailRegex);
    Matcher MyMatcher = MyPattern.matcher(email);

    if (!MyMatcher.matches()) 
        res = false;
    return res; 
    }
    
    @AuraEnabled
    public static String getBundleId(String quoteId) {
        return DF_ProductController.getBundleId(quoteId);
    }
}