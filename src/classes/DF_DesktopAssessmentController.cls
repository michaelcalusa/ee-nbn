public class DF_DesktopAssessmentController {
    
    /*
	* Method to create child opportunity and product basket with product configurations in it based on the offer - Site Survey Charge.
	* The child opportunity created is mapped to the opportunity that is passed as parameter  
	* Parameters : parent opportunity id to which the child opportunity has to be associated.
	* @Return : void
	*/
    public static String processForDA(String parentOppId){
        Opportunity oppo = new Opportunity();
        if(String.isNotEmpty(parentOppId)){
            System.debug('PP parentOppId: '+parentOppId);
            sObject oppObject = new Opportunity();
            String queryStr = DF_CS_API_Util.getQuery(oppObject, ' WHERE Id = '+ '\'' + parentOppId + '\'');
            List<Opportunity> parentOppList = (List<Opportunity>)DF_CS_API_Util.getQueryRecords(queryStr); 
            Opportunity parentOpp = !parentOppList.isEmpty() ? parentOppList.get(0) : null;
            //Create child opportunity
            oppo.Name = parentOpp.Name;
            oppo.StageName = parentOpp.StageName;
            oppo.CloseDate = parentOpp.CloseDate;
            oppo.Parent_Opportunity__c = parentOpp.Id;            
            oppo.RecordTypeId = parentOpp.RecordTypeId;
            insert oppo;
            //Create basket, product configuration, sync with opportunity and change the opportunity to closed won
            cscfga__Product_Basket__c basket = createProductBasket(oppo.Id);
            String syncResults = DF_CS_API_Util.syncWithOpportunity(basket.Id);
            if(syncResults.equalsIgnoreCase('sychronised with opportunity')){
                updateOppClosedWon(oppo,basket);
            }
        }
        return oppo.Id;
    }
    
    //Method to create product basket
    public static cscfga__Product_Basket__c createProductBasket(String oppId){
        if(String.isNotEmpty(oppId)){
            cscfga__Product_Basket__c basket;
            cscfga.API_1.ApiSession apiSession;
            //create api session
            apiSession = DF_CS_API_Util.createApiSession(basket);
            //create basket for the session
            basket = DF_CS_API_Util.createBasket(apiSession);
            basket.cscfga__opportunity__c = oppId;
            update basket;
            createProductDefinition(apiSession,basket);
            return basket;
        }
        else
            return null;
    }
    
    //Method to create product definition
    public static void createProductDefinition(cscfga.API_1.ApiSession apiSession, cscfga__Product_Basket__c basket){
        apiSession = DF_CS_API_Util.createApiSession(basket);
        
        //fetch custom setting values for site survey charges
        String sscOfferId = DF_CS_API_Util.getCustomSettingValue('SF_SITE_SURVEY_CHARGE_OFFER_ID');
        String sscDefId = DF_CS_API_Util.getCustomSettingValue('SF_SITE_SURVEY_CHARGE_DEFINITION_ID');
        String categoryId = DF_CS_API_Util.getCustomSettingValue('SF_CATEGORY_ID');
        sObject categoryObject = new cscfga__Product_Category__c();
        String queryStrng = DF_CS_API_Util.getQuery(categoryObject, ' WHERE Id = '+ '\'' + categoryId + '\'');
        List<cscfga__Product_Category__c> categoryList = (List<cscfga__Product_Category__c>)DF_CS_API_Util.getQueryRecords(queryStrng); 
        cscfga__Product_Category__c category = !categoryList.isEmpty() ? categoryList.get(0) : null;
        List<csbb__Product_Configuration_Request__c> pcrList = new List<csbb__Product_Configuration_Request__c>();   
        
        try{
            // adding Site survey charges config
            apiSession.setProductToConfigure(new cscfga__Product_Definition__c(Id = sscDefId), new Map<String, String> {'containerType' => 'basket', 'linkedId' => basket.Id});
            apiSession.executeRules();
            cscfga.ValidationResult vr = apiSession.validateConfiguration();
            apiSession.persistConfiguration(true);
        }catch(Exception ex){
            GlobalUtility.logMessage('Error', 'AppianResponseHandlingError', 'createProductDefinition', ' ', '', '', '', ex, 0);
        }  
        cscfga.ProductConfiguration config1 = apiSession.getRootConfiguration(); 
        pcrList.add(DF_CS_API_Util.createProductConfigurationRequest(basket, config1.getSObject(), sscOfferId, category));
        if(!pcrList.isEmpty()){
            insert pcrList;
        }
    }
    
    //Method to update Opportunity Stage to Closed Won for creation of orders
    public static void updateOppClosedWon(Opportunity opp, cscfga__Product_Basket__c basket){
        String stageName = DF_CS_API_Util.getCustomSettingValue('SF_OPPORTUNITY_STAGE');
        if(opp != null && basket != null){
            opp.StageName = stageName;
            update opp;
        }
    }
}