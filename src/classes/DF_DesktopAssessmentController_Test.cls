@IsTest
public class DF_DesktopAssessmentController_Test {

    /**
	* Tests processForDA method
	*/
    @isTest static void processForDATest()
    {
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        Opportunity parentOpp = DF_TestData.createOpportunity('Test Parent Opp');
        insert parentOpp;
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Parent_Opportunity__c = parentOpp.Id;
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        
        Test.startTest();
        String oppId = DF_DesktopAssessmentController.processForDA(opp.Id);
        Test.stopTest();
        System.assertNotEquals('null',oppId);
    }
}