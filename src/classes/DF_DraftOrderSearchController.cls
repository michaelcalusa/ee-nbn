public with sharing class DF_DraftOrderSearchController {
    
    /*
    * Fetches order records for the given search parameters
    * Parameters : searchTerm, serachDate, searchStatus, isTriggerBySearch
    * @Return : Returns a serialized string of list of DF Order records
    */
    
    public static final String LOC_ID = 'LOC'; 
    public static final String BUNDLE_ID = 'EEB-'; 
    //public static final String ORDER_ID = 'EEO-'; 
    public static final String QUOTE_Id = 'EEQ-';
    //public static final String QUOTE_STATUS = 'Submitted';
    public static list<string> status = new List<string>();
    
    @AuraEnabled
    public static String getRecords(String searchTerm, Boolean isTriggerBySearch) {
        System.debug('PPPP Search Term value is : ' + searchTerm);  
        status.add('Quick Quote');
        status.add('Order Draft');
        //get the EE recordTypeId for DF_Order__c
        string quoteRTId = DF_CS_API_Util.getRecordType(DF_Constants.ENTERPRISE_ETHERNET_NAME, DF_Constants.QUOTE_OBJECT);
        
        List<DF_Quote__c> quoteList = new List<DF_Quote__c>();
        try {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            System.debug('PPPP User details ' + userLst);
            
            if(userLst.size()>0 && userLst.get(0).AccountId!=null){
                if(String.isBlank(searchTerm) && !isTriggerBySearch){
                    String queryStr = 'Select id, Name, Location_Id__c, Opportunity_Bundle__c, Opportunity_Bundle__r.Name, LastModifiedDate, Proceed_to_Pricing__c, Status__c FROM DF_Quote__c WHERE Status__c in (\'Order Draft\') AND QuoteType__c <> \'Modify\' AND recordTypeId = ' + '\'' + quoteRTId + '\' AND Opportunity_Bundle__r.Account__c = ' + '\'' + userLst.get(0).AccountId + '\' ORDER BY Name DESC';
                    quoteList = (List<DF_Quote__c>)DF_CS_API_Util.getQueryRecords(queryStr);
                    
                }
                else if(String.isNotBlank(searchTerm) && isTriggerBySearch){
                    String locId,ordId,bundleId, quoteId;
                    String queryStr = 'Select id, Name, Location_Id__c, Opportunity_Bundle__c, Opportunity_Bundle__r.Name, LastModifiedDate, Proceed_to_Pricing__c, Status__c FROM DF_Quote__c WHERE Status__c in (\'Order Draft\') AND QuoteType__c <> \'Modify\' AND recordTypeId = ' + '\'' + quoteRTId + '\' AND Opportunity_Bundle__r.Account__c = ' + '\'' + userLst.get(0).AccountId + '\' ';
                    if(String.isNotBlank(searchTerm)){
                        if(searchTerm.startsWithIgnoreCase(LOC_ID)){
                            locId = searchTerm;
                            queryStr += ' AND Location_Id__c = ' + '\'' + locId + '\'';
                        }
                        else if(searchTerm.startsWithIgnoreCase(BUNDLE_ID)){
                            bundleId = searchTerm;
                            queryStr += ' AND Opportunity_Bundle__r.Name = ' + '\'' + bundleId + '\'';
                        }
                        else if(searchTerm.startsWithIgnoreCase(QUOTE_ID)){
                            quoteId = searchTerm;
                            queryStr += ' AND Name = ' + '\'' + quoteId + '\'';
                        }
                        else{
                            List<DF_Quote__c> orderEmptyList = new List<DF_Quote__c>();
                            return JSON.serialize(orderEmptyList);
                        }
                    }
                    
                    quoteList = (List<DF_Quote__c>)DF_CS_API_Util.getQueryRecords(queryStr);
                    System.debug('PPPP quoteList: '+quoteList);
                }
            }else{
                throw new CustomException('User record with the RSP account not found');
            }
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }
        return JSON.serialize(quoteList);
    }
    
    @AuraEnabled
    public static string getOrderSummaryData(string bundleId) {
        return DF_SF_QuoteController.getOrderSummaryData(bundleId);
    }
    
    @AuraEnabled
    public static string createDF_Order(string dfQuoteId) {
        return DF_SF_QuoteController.createDF_Order(dfQuoteId);
    }
}