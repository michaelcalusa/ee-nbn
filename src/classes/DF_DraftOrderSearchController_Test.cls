@isTest 
private class DF_DraftOrderSearchController_Test {
   
    @isTest
    private static void test_getRecordsWithLoc() {
       
        String result = '';
        List<DF_Order__c> deserializedResult = new List<DF_Order__c>();
    
        User commUser;
        commUser = DF_TestData.createDFCommUser();
            
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            DF_TestService.createOrderRecord(userLst.get(0).AccountId);
            test.startTest();
            result = DF_DraftOrderSearchController.getRecords('LOC000035375038',true);
            deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
            test.stopTest();                
        }                                    
            
    }
    
    @isTest
    private static void test_getRecordsNull() {
       
        String result = '';
        List<DF_Order__c> deserializedResult = new List<DF_Order__c>();
    
        User commUser;
        commUser = DF_TestData.createDFCommUser();
            
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            DF_TestService.createOrderRecord(userLst.get(0).AccountId);
            test.startTest();
            result = DF_DraftOrderSearchController.getRecords('',false);
            deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
            test.stopTest();                
        }                                    
            
    }
    
    @isTest
    private static void test_getRecordsOppBundle() {
       
        String result = '';
        List<DF_Order__c> deserializedResult = new List<DF_Order__c>();
    
        User commUser;
        commUser = DF_TestData.createDFCommUser();
            
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            DF_TestService.createOrderRecord(userLst.get(0).AccountId);
            test.startTest();
            List<DF_Opportunity_Bundle__c> oppBundle = [SELECT ID, Name FROM DF_Opportunity_Bundle__c WHERE Account__c = :userLst.get(0).AccountId];
            
            result = DF_DraftOrderSearchController.getRecords(oppBundle[0].Name,true);
            //DF_DraftOrderSearchController.getOrderSummaryData(oppBundle[0].id);
            deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
            test.stopTest();                
        }                                    
            
    }
    
    @isTest
    private static void test_getRecordsOppQuote() {
       
        String result = '';
        List<DF_Order__c> deserializedResult = new List<DF_Order__c>();
    
        User commUser;
        commUser = DF_TestData.createDFCommUser();
            
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            DF_TestService.createOrderRecord(userLst.get(0).AccountId);
            test.startTest();
            List<DF_Quote__c> oppQuote = [SELECT ID, Name FROM DF_Quote__c WHERE Opportunity_Bundle__r.Account__c = :userLst.get(0).AccountId];
            
            result = DF_DraftOrderSearchController.getRecords(oppQuote[0].Name,true);
            DF_DraftOrderSearchController.createDF_Order(oppQuote[0].id);
            deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
            test.stopTest();                
        }                                    
            
    }
}