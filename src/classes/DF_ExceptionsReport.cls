/**
* Class Name: DF_ExceptionsReport
* Description: This class used to generate the exceptions report.
* */
public with sharing class DF_ExceptionsReport{
    
    public List<DF_Quote__c> lstQuote {set;get;}
    public set<Id> optyIds {set;get;}
    public DF_ExceptionsReport(){
         //optyIds = new set<id>();
         lstQuote = [Select Id,Address__c,Business_Contact_Name__c,Location_Id__c,Opportunity__c,Opportunity__r.Name,
                    Opportunity__r.Account.Name,Opportunity__r.Commercial_Deal__c,Opportunity__r.Reference_Number__c,Opportunity__r.Commercial_Deal__r.name,
                    Opportunity__r.Parent_Opportunity_s_Commercial_Deal__c,QuoteType__c,
                    Opportunity__r.Follow_Up_Action__c,Quote_Submitted_Date__c
                    From DF_Quote__c 
                    where Opportunity__c <> null 
                    and Opportunity__r.recordtype.developerName = 'Enterprise_Ethernet'
                    and Opportunity__r.StageName = 'Closed Won' 
                    and Opportunity__r.Account.Commercial_Deal__c = true
                    and (Opportunity__r.Commercial_Deal__c = null OR Opportunity__r.Commercial_Deal__r.is_Default__c = true)
                    and Opportunity__r.Follow_Up_Action__c = false order by createdDate
                    limit:Limits.getLimitQueryRows()];
        
         
    }
    public PageReference approve() {

        lstQuote = [Select Id,Address__c,Business_Contact_Name__c,Location_Id__c,Opportunity__c,Opportunity__r.Name,
                    Opportunity__r.Account.Name,Opportunity__r.Commercial_Deal__c,Opportunity__r.Reference_Number__c,Opportunity__r.Commercial_Deal__r.name,
                    Opportunity__r.Parent_Opportunity_s_Commercial_Deal__c,QuoteType__c,
                    Opportunity__r.Follow_Up_Action__c,Quote_Submitted_Date__c
                    From DF_Quote__c 
                    where Opportunity__c <> null 
                    and Opportunity__r.recordtype.developerName = 'Enterprise_Ethernet'
                    and Opportunity__r.StageName = 'Closed Won' 
                    and Opportunity__r.Account.Commercial_Deal__c = true
                    and (Opportunity__r.Commercial_Deal__c = null OR Opportunity__r.Commercial_Deal__r.is_Default__c = true)
                    and Opportunity__r.Follow_Up_Action__c = false order by createdDate
                    limit:Limits.getLimitQueryRows()];
        return ApexPages.currentPage();
    }
    public PageReference exportToExcel(){
        PageReference retURLExcel = new PageReference('/apex/DF_ExceptionsReportToExcel');
        return retURLExcel;
    }

}