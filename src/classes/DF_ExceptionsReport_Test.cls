@isTest
private class DF_ExceptionsReport_Test {    
    
    @isTest
    static void testDF_ExceptionsReport()
    {
        PageReference pageRef = Page.DF_ExceptionsReportToExcel;
        
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        
        Account acc = DF_TestData.createAccount('Test Account');
        acc.Access_Seeker_ID__c = 'AS12345';
        acc.Billing_ID__c = 'BL12345';
        insert acc;
        
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        opp.accountid = acc.id;
        insert opp;
        
        Test.setCurrentPage(pageRef);
        
        test.startTest(); 
        List<DF_Quote__c> dfQuoteList = [SELECT Id,Name,Address__c,LAPI_Response__c,RAG__c,Location_Id__c,Fibre_Build_Cost__c,Opportunity_Bundle__c,Opportunity__c
                                         FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppid LIMIT 1];
                                         
        Opportunity childOpp = DF_TestData.createOpportunity('Test Child Opp');
        childOpp.Parent_Opportunity__c = dfQuoteList.get(0).Opportunity__c;
        insert childOpp;
        
        DF_ExceptionsReport controller = new DF_ExceptionsReport() ;
        String excelPage = controller.exportToExcel().getUrl();

        System.assertEquals('/apex/DF_ExceptionsReportToExcel', excelPage);
        test.stopTest(); 
    }
}