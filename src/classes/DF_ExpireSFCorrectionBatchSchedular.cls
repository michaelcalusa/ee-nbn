global class DF_ExpireSFCorrectionBatchSchedular implements Schedulable{
    //Schedular to schedule DF_ExpireServiceFeasibilityBatch class
    global void execute(SchedulableContext sc)
    {
        
        // We now call the batch class to be scheduled
        DF_ExpireSFCorrectionBatch e = new DF_ExpireSFCorrectionBatch ();
        
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(e,20);
    }
}