@isTest
public class DF_ExpireSFCorrectionBatchSchedular_Test {

    //Tests DF_ExpireSFCorrectionBatchSchedular methods
    @isTest
    static void test_ExpireServiceFeasibilitySchedular(){
        
        Test.StartTest();
        DF_ExpireSFCorrectionBatchSchedular sh1 = new DF_ExpireSFCorrectionBatchSchedular();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Expire Service Feasibility correction Schedular', sch, sh1); 
        Test.stopTest(); 
    }
}