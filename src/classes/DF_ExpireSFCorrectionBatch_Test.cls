@isTest
public class DF_ExpireSFCorrectionBatch_Test {
	
    //Tests DF_ExpireSFCorrectionBatch methods
    @isTest
    static void test_ExpireServiceFeasibilityBatch(){
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        Account acc = DF_TestData.createAccount('Test Account');
        Id oppId = DF_TestService.getServiceFeasibilityRequestForAccount(acc);
        
        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = oppId;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = DF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        // Create Oppty
        Opportunity oppty1 = DF_TestData.createOpportunity('LOC111111111111');
        oppty1.Opportunity_Bundle__c = oppId;
        insert oppty1;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket1 = DF_TestData.buildBasket();
        prodBasket1.cscfga__Opportunity__c = oppty1.Id;
        insert prodBasket1;
        
        // Create Oppty
        Opportunity oppty2 = DF_TestData.createOpportunity('LOC111111111111');
        oppty2.Opportunity_Bundle__c = oppId;
        insert oppty2;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket2 = DF_TestData.buildBasket();
        prodBasket2.cscfga__Opportunity__c = oppty2.Id;
        insert prodBasket2;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC000035375038', oppty.Id, oppId, 1000, 'A');  
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuote1.Fibre_Build_Category__c = 'A';
        dfQuote1.QuoteType__c = 'Connect';
        dfQuote1.Status__c = '';
        dfQuote1.Fibre_Build_Cost__c = 0.0;
        dfQuoteList.add(dfQuote1);

        DF_Quote__c dfQuote2 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC000035375037', oppty1.Id, oppId, 1000, 'A');        
        dfQuote2.Proceed_to_Pricing__c = true;
        dfQuote2.Fibre_Build_Category__c = 'B';
        dfQuote2.QuoteType__c = 'Connect';
        dfQuote2.Status__c = 'Quick Quote';
        dfQuoteList.add(dfQuote2);
        
        DF_Quote__c dfQuote3 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC000035375039', oppty2.Id, oppId, 1000, 'A');        
        dfQuote3.Proceed_to_Pricing__c = true;
        dfQuote3.Fibre_Build_Category__c = 'A';
        dfQuote3.QuoteType__c = 'Connect';
        dfQuote3.Status__c = 'Ready for Order';
        dfQuoteList.add(dfQuote3);
        
        insert dfQuoteList;
        
        dfQuote2.Fibre_Build_Cost__c = 1000.0;
        update dfQuote2;
        
        DF_Order__c ordObj = DF_TestData.createDFOrder(dfQuote1.Id,oppId,'In Draft');
        insert ordObj;
        
        DF_ExpireSFCorrectionBatch e = new DF_ExpireSFCorrectionBatch();
        test.startTest();
        Database.executeBatch(e);
        test.stopTest();
    }
}