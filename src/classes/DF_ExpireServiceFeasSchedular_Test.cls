@isTest
public class DF_ExpireServiceFeasSchedular_Test {

    //Tests DF_ExpireServiceFeasibilitySchedular methods
    @isTest
    static void test_ExpireServiceFeasibilitySchedular(){
        
        Test.StartTest();
        DF_ExpireServiceFeasibilitySchedular sh1 = new DF_ExpireServiceFeasibilitySchedular();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Expire Service Feasibility Schedular', sch, sh1); 
        Test.stopTest(); 
    }
}