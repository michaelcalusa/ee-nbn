global class DF_ExpireServiceFeasibilityBatch implements Database.Batchable<sObject>{
    //Class that is used to expire DF Quote, DF Order, Opportunity and Service Feasibility records at the end of 60 business days
    
    List<String> ORDER_STATUS = new List<String>{'In Draft'};
    List<String> Quote_STATUS = new List<String>{'Submitted','Accepted','Feasibility Expired'};
    String query;
    
    global DF_ExpireServiceFeasibilityBatch(){
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        String quoteStatus = '';
        for(String s : Quote_STATUS){
            quoteStatus += '\'' + s + '\',';
        }
        quoteStatus = quoteStatus.lastIndexOf(',') > 0 ? '(' + quoteStatus.substring(0,quoteStatus.lastIndexOf(',')) + ')' : quoteStatus ;
        
        //Query all quote records that are yet to be submitted
        String quoteRTId = DF_ExpireServiceFeasibilityUtil.getRecordType(DF_ExpireServiceFeasibilityUtil.ENTERPRISE_ETHERNET_NAME, DF_ExpireServiceFeasibilityUtil.QUOTE_OBJECT); 
        query = 'SELECT Id, Status__c, Fibre_Build_Category__c, CreatedDate, Opportunity_Bundle__c, Opportunity__c, Location_Id__c, QuoteType__c, RecordTypeId FROM DF_Quote__c WHERE RecordTypeId = ' + '\'' + quoteRTId + '\'' + ' AND (QuoteType__c = ' + '\'' + DF_ExpireServiceFeasibilityUtil.QUOTE_TYPE_CONNECT + '\'' + ' OR QuoteType__c = null) AND Status__c NOT IN ' + quoteStatus + ' ';
        System.debug('AZ quoteRTId : '+quoteRTId);
        System.debug('AZ query : '+query);
        return Database.getQueryLocator(query); 
    } 
    
    global void execute(Database.BatchableContext bc, List<DF_Quote__c> scope){
        Datetime currentDate;
        List<DF_Quote__c> quotesToUpdate = new List<DF_Quote__c>();
        List<DF_Order__c> ordersToUpdate = new List<DF_Order__c>();
        List<String> oppIdsToUpdate = new List<String>();
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        List<DF_SF_Request__c> sfrToUpdateList = new List<DF_SF_Request__c>();
        Set<Id> bundleIdSet = new Set<Id>();
        Set<Id> quoteIdSet = new Set<Id>();
        Map<String, Map<String, String>> quoteToBundleLocIdMap = new Map<String, Map<String, String>>();
        
        //Calculate the current time to determine business days
        currentDate = Datetime.now();
        Integer offset = UserInfo.getTimezone().getOffset(currentDate);
        Datetime toDate = currentDate.addSeconds(offset/1000);
        //Query the confiurable expiry duration from custom settings
        Integer expiryDuration = Integer.valueOf(DF_ExpireServiceFeasibilityUtil.getCustomSettingValue('SF_EXPIRY_DURATION'));
        //Query the business hours setup
        Id businessHourId = [SELECT Id FROM BusinessHours WHERE IsActive = true AND Name = :DF_ExpireServiceFeasibilityUtil.BUSINESS_HOURS_EXPIRE_SF].Id;
        System.debug('AZ scope : '+scope);
        for(DF_Quote__c q : scope){
            quoteIdSet.add(q.Id);
        }
        //Query all associated orders and history records that are not yet submitted by RSP
        List<DF_Quote__c> qList = [SELECT Id,Status__c,Fibre_Build_Category__c, CreatedDate, Opportunity_Bundle__c,Opportunity__c,Location_Id__c,
                                   (SELECT Id, DF_Quote__c, Order_Status__c, OrderType__c
                                    FROM DF_Orders__r 
                                    WHERE Order_Status__c IN :ORDER_STATUS AND OrderType__c = :DF_ExpireServiceFeasibilityUtil.ORDER_TYPE_CONNECT),
                                   (select Id, Field, CreatedDate, NewValue, OldValue, ParentId
                                    FROM Histories)
                                   FROM DF_Quote__c
                                   WHERE Id IN :quoteIdSet];
        
        System.debug('AZ qList in batch :'+qList);
        for(DF_Quote__c q: qList){
            Map<String, String> oppBundleToLocIdMap = new Map<String, String>();
            Date fromDate;
        	//DF Quote for Category  A records would always be created with $0. The timer for expiry kicks in when they are created
            if(String.isNotBlank(q.Fibre_Build_Category__c)){
                if(q.Fibre_Build_Category__c.equalsIgnoreCase(DF_ExpireServiceFeasibilityUtil.FB_CATEGORY_A)){
                    fromDate = date.newinstance(q.CreatedDate.year(), q.CreatedDate.month(), q.CreatedDate.day());
                }
                //Build contribution for Category  B and C records would be displayed to the RSP after it is received from Appian and threshold is subtracted
                else if (q.Fibre_Build_Category__c.equalsIgnoreCase(DF_ExpireServiceFeasibilityUtil.FB_CATEGORY_B) || q.Fibre_Build_Category__c.equalsIgnoreCase(DF_ExpireServiceFeasibilityUtil.FB_CATEGORY_C)){
                    for(DF_Quote__History h : q.Histories){
                        if(String.isNotBlank(h.Field)){
                            if(h.Field.equalsIgnoreCase(DF_ExpireServiceFeasibilityUtil.FIBRE_BUILD_COST) && h.OldValue == null && h.NewValue != null){
                                fromDate = date.newinstance(h.CreatedDate.year(), h.CreatedDate.month(), h.CreatedDate.day());
                            }
                        }
                    }
                }
                if(fromDate != null){
                    Long differenceMilliSecs = BusinessHours.diff(businessHourId, fromDate, toDate);
                    Long diffInDays = ((Decimal)differenceMilliSecs/86400000).round(System.RoundingMode.UP);
                    
                    System.debug('AZ diffInDays: '+diffInDays);
                    //Expire records if they exceed the duration
                    if(diffInDays > expiryDuration){ 
                        for(DF_Order__c ord : q.DF_Orders__r){
                            if(ord != null){
                                ord.Order_Status__c = DF_ExpireServiceFeasibilityUtil.FEASIBILITY_EXPIRED;//set the status to 'Feasibility Expired'
                                ordersToUpdate.add(ord);
                            }
                        }
                        q.Status__c = DF_ExpireServiceFeasibilityUtil.FEASIBILITY_EXPIRED;//set the status to 'Feasibility Expired'
                        quotesToUpdate.add(q);
                        oppIdsToUpdate.add(q.Opportunity__c);
                        bundleIdSet.add(q.Opportunity_Bundle__c);
                        oppBundleToLocIdMap.put(q.Opportunity_Bundle__c,q.Location_Id__c);
                        quoteToBundleLocIdMap.put(q.Id,oppBundleToLocIdMap);
                    }
                }
            }
        }
        //Set the associated Opportunities to Closed Lost
        List<Opportunity> oppList = [select Id, StageName from Opportunity where Id IN :oppIdsToUpdate];
        for(Opportunity opp: oppList){
            opp.StageName = DF_ExpireServiceFeasibilityUtil.CLOSED_LOST;
            oppsToUpdate.add(opp);
        }
        System.debug('AZ quotesToUpdate : '+quotesToUpdate);
        System.debug('AZ ordersToUpdate : '+ordersToUpdate);
        System.debug('AZ oppsToUpdate : '+oppsToUpdate);
        if(!quotesToUpdate.isEmpty()){
            Database.SaveResult[] qResultList = Database.update(quotesToUpdate, false);
            for(integer i =0; i<quotesToUpdate.size();i++){
                String msg='';
                If(!qResultList[i].isSuccess()){       
                    for(Database.Error err: qResultList[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    }
                    GlobalUtility.logMessage(DF_ExpireServiceFeasibilityUtil.ERROR, DF_ExpireServiceFeasibilityBatch.class.getName(), msg, null, '', '', '', null, 0);
                }
            }
        }
        if(!ordersToUpdate.isEmpty()){
            Database.SaveResult[] oResultList = Database.update(ordersToUpdate, false);
            for(integer i =0; i<ordersToUpdate.size();i++){
                String msg='';
                If(!oResultList[i].isSuccess()){       
                    for(Database.Error err: oResultList[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    }
                    GlobalUtility.logMessage(DF_ExpireServiceFeasibilityUtil.ERROR, DF_ExpireServiceFeasibilityBatch.class.getName(), msg, null, '', '', '', null, 0);
                }
            }
        }
        if(!oppsToUpdate.isEmpty()){
            Database.SaveResult[] opResultList = Database.update(oppsToUpdate, false);
            for(integer i =0; i<oppsToUpdate.size();i++){
                String msg='';
                If(!opResultList[i].isSuccess()){       
                    for(Database.Error err: opResultList[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    }
                    GlobalUtility.logMessage(DF_ExpireServiceFeasibilityUtil.ERROR, DF_ExpireServiceFeasibilityBatch.class.getName(), msg, null, '', '', '', null, 0);
                }
            }
        }
        //Query the associated service feasibility request records
        List<DF_SF_Request__c> sfrList = [select Id, Location_Id__c, Status__c, Opportunity_Bundle__c from DF_SF_Request__c WHERE Opportunity_Bundle__c IN :bundleIdSet];
        System.debug('AZ quoteToBundleLocIdMap : '+quoteToBundleLocIdMap);
        System.debug('AZ sfrList : '+sfrList);
        for(String outerKey : quoteToBundleLocIdMap.keySet()){
            for(String obId : quoteToBundleLocIdMap.get(outerKey).keySet()){
                for(DF_SF_Request__c sfr : sfrList){
                    if(sfr != null && sfr.Opportunity_Bundle__c != null && String.isNotBlank(sfr.Location_Id__c)){
                        Id bundleFromSFR = sfr.Opportunity_Bundle__c;
                        Id bundleFromMap = obId;
                        if(bundleFromSFR.equals(bundleFromMap) && sfr.Location_Id__c.equalsIgnorecase(quoteToBundleLocIdMap.get(outerKey).get(obId))){
                            sfr.Status__c = DF_ExpireServiceFeasibilityUtil.EXPIRED;//set the status to 'Expired'
                            if(!sfrToUpdateList.contains(sfr))
                                sfrToUpdateList.add(sfr);
                        }
                    }
                }
            }
        }
        System.debug('AZ sfrToUpdateList : '+sfrToUpdateList);
        if(!sfrToUpdateList.isEmpty()){
            Database.SaveResult[] sResultList = Database.update(sfrToUpdateList, false);
            for(integer i =0; i<sfrToUpdateList.size();i++){
                String msg='';
                If(!sResultList[i].isSuccess()){       
                    for(Database.Error err: sResultList[i].getErrors()){  
                        msg += err.getmessage()+'"\n\n';
                    }
                    GlobalUtility.logMessage(DF_ExpireServiceFeasibilityUtil.ERROR, DF_ExpireServiceFeasibilityBatch.class.getName(), msg, null, '', '', '', null, 0);
                }
            }
        }
    }
    
    global void finish(Database.BatchableContext bc){
        AsyncApexJob job = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :bc.getJobId()]; 
        System.debug('>>>> finish ' + job.Status);
        if(job.Status == 'Failed')
            GlobalUtility.logMessage(DF_ExpireServiceFeasibilityUtil.ERROR, DF_ExpireServiceFeasibilityBatch.class.getName(), 'expirebatch', job.Id, '', '', '', null, 0);
    }
}