global class DF_ExpireServiceFeasibilitySchedular implements Schedulable{
    //Schedular to schedule DF_ExpireServiceFeasibilityBatch class
    global void execute(SchedulableContext sc)
    {
        
        // We now call the batch class to be scheduled
        DF_ExpireServiceFeasibilityBatch e = new DF_ExpireServiceFeasibilityBatch ();
        
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(e,20);
    }
}