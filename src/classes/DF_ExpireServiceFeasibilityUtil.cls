public class DF_ExpireServiceFeasibilityUtil {

    public static final String FB_CATEGORY_C = 'C';
    public static final String FB_CATEGORY_A = 'A'; 
    public static final String FB_CATEGORY_B = 'B';
    public static final String FEASIBILITY_EXPIRED = 'Feasibility Expired';
    public static final String FIBRE_BUILD_COST = 'Fibre_Build_Cost__c';
    public static final String EXPIRED = 'Expired';
    public static final String CLOSED_LOST = 'Closed Lost';
    public static final String BUSINESS_HOURS_EXPIRE_SF = 'EE Expire Service Feasibility Organization hours';
    public static final String ENTERPRISE_ETHERNET_NAME = 'Enterprise Ethernet';
    public static final String QUOTE_OBJECT = 'DF_Quote__c';
    public static final String QUOTE_TYPE_CONNECT = 'Connect'; 
    public static final String ORDER_TYPE_CONNECT = 'Connect';
    public static final String ERROR = 'Error'; 
    
    /*
	* Method to get RecordType Id for all Direct Fibre custom objects      
	* Parameters : recordtype developer name, sObject type
	* @Return : Id of the recordtype
	*/
    public static Id getRecordType(String RTNAME, String SobjectType){
        Id recTypeId = null;
        if(String.isNotBlank(RTNAME) && String.isNotBlank(SobjectType)){
             recTypeId= Schema.getGlobalDescribe().get(SobjectType).getDescribe().getRecordTypeInfosByName().get(RTNAME).getRecordTypeId();
        }
        return recTypeId;
    }
    
    /*
        * Method to get custom setting value    
        * Parameters : custom setting name.
        * @Return : String which contains the custom setting value
    */
    public static String getCustomSettingValue(String csName){
        String value = null;
        if(String.isNotBlank(csName)){
            DF_Custom_Options__c customSettingVal = DF_Custom_Options__c.getValues(csName);
            if(customSettingVal != null){
                value = customSettingVal.Value__c;
            }
        }
        return value;
    }
}