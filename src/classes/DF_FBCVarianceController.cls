public class DF_FBCVarianceController {
    
    public static final String INITIAL_BUILD_COST = 'Initial Build Cost';
    public static final String FIBRE_BUILD_CONTRIBUTION = 'Fibre Build Contribution';
    public static final String TOTAL_FBC_PRICE = 'Total FBC Price';
    public static final String ERROR = 'Error';
    
    public static void manageFBCRequest(Decimal costVariance, String basketId) {        
        try {  
            String errorMsg = null;
            if(String.isNotBlank(basketId) && costVariance != null){
                //Query Fibre Build Contribution product configuration
                List<cscfga__Product_Configuration__c> fbcPCList = [SELECT Id, Name, cscfga__total_contract_value__c from cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c =:basketId AND Name = :DF_Constants.DIRECT_FIBRE_BUILD_CONTRIBUTION_NAME];
                cscfga__Product_Configuration__c fbcPC = !fbcPCList.isEmpty() ? fbcPCList.get(0) : null;
                System.debug('AP fbcPC: '+fbcPC);
                
                if(fbcPC != null){
                    //Query for 'Initial build cost' attribute and update its value
                    List<cscfga__Attribute__c> attList = [select Id, Name, cscfga__Value__c, cscfga__Display_Value__c from cscfga__Attribute__c where Name =:INITIAL_BUILD_COST and cscfga__Product_Configuration__c =:fbcPC.Id];
                    cscfga__Attribute__c att = !attList.isEmpty() ? attList.get(0) : null;
                    System.debug('AP att: '+att);
                    if(att != null){
                        att.cscfga__Value__c = String.valueOf(costVariance);
                        att.cscfga__Display_Value__c = String.valueOf(costVariance);
                        update att;
                        
                        //execute rules and persist configuration
                        cscfga.API_1.ApiSession apiSession;
                        List<cscfga__Product_Basket__c> basketList = [select Id, cscfga__Opportunity__c, cscfga__User_Session__c from cscfga__Product_Basket__c where Id =:basketId];
                        cscfga__Product_Basket__c basket = !basketList.isEmpty() ? basketList.get(0) : null;
                        System.debug('AP basket: '+basket);
                        apiSession = DF_CS_API_Util.createApiSession(basket);
                        apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = fbcPC.Id, cscfga__Product_Basket__c = basketId));
                        apiSession.executeRules(); 
                        apiSession.validateConfiguration();
                        apiSession.persistConfiguration(); 
                        cscfga.ProductConfiguration productConfig = apiSession.getConfiguration();
                        //query for Fibre Build Contribution to get the final fbc 
                        Decimal UpdatedFBC = 0;
                        for(cscfga.LineItem li: productConfig.getAllLineItems(False)){
                            System.debug('li.getPrice(): '+li.getPrice());
                            UpdatedFBC = UpdatedFBC + li.getPrice();
                        }
                        System.debug('AP updatedFBC: '+updatedFBC);
                        if(updatedFBC != null){
                            //update total contract value on product configuration
                            fbcPC.cscfga__total_contract_value__c = UpdatedFBC;
                            update fbcPC;
                            
                            //update Order Line item
                            List<csord__Order__c> ordList = [SELECT Id, Name, (SELECT Id, csord__Total_Price__c, csord__Line_Description__c, csord__Identification__c, csord__Order__c FROM csord__Order_Line_Items__r), csordtelcoa__Opportunity__c FROM csord__Order__c WHERE csordtelcoa__Opportunity__c =:basket.cscfga__Opportunity__c AND Name =:DF_Constants.DIRECT_FIBRE_BUILD_CONTRIBUTION_NAME];
                            csord__Order__c ord = !ordList.isEmpty() ? ordList.get(0) : null;
                            System.debug('AP ord: '+ord);
                            if(ord != null){
                                    DF_CS_API_Util.upgradeordersfromProductConfiguration(productConfig, ord);
                                    //update DF Quote with Fibre build contribution
                                    List<DF_Quote__c> quoteList = [SELECT Id, Fibre_Build_Cost__c, Opportunity__c FROM DF_Quote__c WHERE Opportunity__c =:basket.cscfga__Opportunity__c];
                                    DF_Quote__c quoteRec = !quoteList.isEmpty() ? quoteList.get(0) : null;
                                    System.debug('AP quoteRec: '+quoteRec);
                                    if(quoteRec != null){
                                        quoteRec.Fibre_Build_Cost__c = updatedFBC;
                                        update quoteRec;
                                    }
                                    else
                                        errorMsg = 'DF Quote does not exists';
                            }
                            else
                                errorMsg = 'CloudSense order does not exists';
                        }
                        else
                           errorMsg = 'Fibre Build Contribution attribute in Product Configuration is null'; 
                    }
                    else
                        errorMsg = 'Initial Build Cost attribute in Product Configuration is null';
                }
                else
                    errorMsg = 'Fibre Build Contribution Product Configuration is null';
            }
            else
                errorMsg = 'Cost variance/ Bakset is null';
            if(errorMsg != null)
                GlobalUtility.logMessage(DF_FBCVarianceController.ERROR, DF_FBCVarianceController.class.getName(), errorMsg, '', '', '', '', null, 0);
        } catch (Exception e) {  
            GlobalUtility.logMessage(DF_FBCVarianceController.ERROR, DF_FBCVarianceController.class.getName(), 'Handling FBC cost variance', '', '', '', '', e, 0);
        }
    }
}