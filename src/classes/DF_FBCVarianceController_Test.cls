@IsTest
public class DF_FBCVarianceController_Test {

    /**
    * Tests manageFBCRequest method
    */
    @isTest static void manageFBCRequestMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords(); 
        Test.startTest();
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfigFBC(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        opp.StageName = 'New';
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        DF_Quote__c quote1 = new DF_Quote__c();
        quote1.Opportunity__c = opp.Id;
        quote1.Opportunity_Bundle__c = oppId;
        insert quote1;
        String syncResults = DF_CS_API_Util.syncWithOpportunity(basket.Id);
        if(syncResults.equalsIgnoreCase('sychronised with opportunity')){
            opp.StageName = 'Closed Won';
            update opp;
        }
        Decimal costVariance = 50000.00;
        csord__Order__c ord = new csord__Order__c();
        ord.csordtelcoa__Opportunity__c = opp.Id;
        ord.Name = 'Direct Fibre - Build Contribution';
        ord.csord__Identification__c = 'Order_a1h5D000000HUDPQA4_0';
        insert ord;
        csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c();
        oli.csord__Order__c = ord.Id;
        oli.csord__Total_Price__c = 0.00;
        oli.csord__Identification__c = 'Order_a1h5D000000HUDPQA4_0';
        insert oli;
        //Test.startTest();
        DF_FBCVarianceController.manageFBCRequest(costVariance, basket.Id);
        Test.stopTest();
    }
    
    /**
    * Tests manageFBCRequestNoQuote method
    */
    @isTest static void manageFBCRequestNoQuoteMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Test.startTest();
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfigFBC(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        opp.StageName = 'New';
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        String syncResults = DF_CS_API_Util.syncWithOpportunity(basket.Id);
        if(syncResults.equalsIgnoreCase('sychronised with opportunity')){
            opp.StageName = 'Closed Won';
            update opp;
        }
        Decimal costVariance = 50000.00;
        csord__Order__c ord = new csord__Order__c();
        ord.csordtelcoa__Opportunity__c = opp.Id;
        ord.Name = 'Direct Fibre - Build Contribution';
        ord.csord__Identification__c = 'Order_a1h5D000000HUDPQA4_0';
        insert ord;
        csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c();
        oli.csord__Order__c = ord.Id;
        oli.csord__Total_Price__c = 0.00;
        oli.csord__Identification__c = 'Order_a1h5D000000HUDPQA4_0';
        insert oli;
        
        DF_FBCVarianceController.manageFBCRequest(costVariance, basket.Id);
        Test.stopTest();
    }
    
    /**
    * Tests manageFBCRequestNoCSOrder method
    */
    @isTest static void manageFBCRequestNoCSOrderMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
         Test.startTest();
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfigFBC(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        opp.StageName = 'New';
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        String syncResults = DF_CS_API_Util.syncWithOpportunity(basket.Id);
        if(syncResults.equalsIgnoreCase('sychronised with opportunity')){
            opp.StageName = 'Closed Won';
            update opp;
        }
        Decimal costVariance = 50000.00;
       // Test.startTest();
        DF_FBCVarianceController.manageFBCRequest(costVariance, basket.Id);
        Test.stopTest();
    }
    
    /**
    * Tests manageFBCRequestNoCSOrderLineItem method
    */
    @isTest static void manageFBCRequestNoCSOrderLineItemMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Test.startTest();
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfigFBC(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        opp.StageName = 'New';
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        String syncResults = DF_CS_API_Util.syncWithOpportunity(basket.Id);
        if(syncResults.equalsIgnoreCase('sychronised with opportunity')){
            opp.StageName = 'Closed Won';
            update opp;
        }
        Decimal costVariance = 50000.00;
        csord__Order__c ord = new csord__Order__c();
        ord.csordtelcoa__Opportunity__c = opp.Id;
        ord.Name = 'Direct Fibre - Build Contribution';
        ord.csord__Identification__c = 'Order_a1h5D000000HUDPQA4_0';
        insert ord;
        //Test.startTest();
        DF_FBCVarianceController.manageFBCRequest(costVariance, basket.Id);
        Test.stopTest();
    }
    
    /**
    * Tests manageFBCRequestNoCS method
    */
    @isTest static void manageFBCRequestNoCSMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Test.startTest();
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        opp.StageName = 'New';
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        
        DF_FBCVarianceController.manageFBCRequest(100.00, basket.Id);
        Test.stopTest();
    }
    
    /**
    * Tests manageFBCRequestNoCost method
    */
    @isTest static void manageFBCRequestNoCostMethodTest()
    {
        Test.startTest();
        DF_FBCVarianceController.manageFBCRequest(null, null);
        Test.stopTest();
    }
}