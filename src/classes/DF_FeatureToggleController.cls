/**
 * Created by philipstafford-jones on 25/1/19.
 */

public with sharing class DF_FeatureToggleController {

    @AuraEnabled
    public static Boolean isFeatureToggleEnabled(String featureName) {
        ReleaseToggle releaseToggle = (ReleaseToggle)ObjectFactory.getInstance(ReleaseToggle.class);
        return releaseToggle.isActive(String.escapeSingleQuotes(featureName));
    }

}