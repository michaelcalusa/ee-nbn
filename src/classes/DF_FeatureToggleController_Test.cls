/**
 * Created by philipstafford-jones on 29/1/19.
 */

@IsTest
private class DF_FeatureToggleController_Test {
    @IsTest
    static void testBehavior() {
        Boolean ft = DF_FeatureToggleController.isFeatureToggleEnabled('dummy');
        system.AssertEquals(false, ft);

        ft = DF_FeatureToggleController.isFeatureToggleEnabled('MR_Argo_EE_Enhancements');
        system.AssertEquals(true, ft);

    }
}