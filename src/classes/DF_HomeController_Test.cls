@isTest 
private class DF_HomeController_Test {

    @isTest
	private static void test_getBusinessUserDetails() {
		String result = null;
        User commUser;
		commUser = DF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = :DF_Constants.BUSINESS_PERMISSION_SET];
            insert new PermissionSetAssignment(AssigneeId = commUser.id, PermissionSetId = ps.Id );
            test.startTest();    
            result = DF_HomeController.getUserDetails();
            System.debug(result);
            test.stopTest();                
        }	                    
        // Assertions
        system.AssertEquals('business', result);
	}
    
    @isTest
	private static void test_getBusinessPlusUserDetails() {
		String result = null;
        User commUser;
		commUser = DF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = :DF_Constants.BUSINESS_PLUS_PERMISSION_SET];
            insert new PermissionSetAssignment(AssigneeId = commUser.id, PermissionSetId = ps.Id );
            test.startTest();    
            result = DF_HomeController.getUserDetails();
            System.debug(result);
            test.stopTest();                
        }	                    
        // Assertions
        system.AssertEquals('businessPlus', result);
	}
}