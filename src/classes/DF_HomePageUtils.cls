public with sharing class DF_HomePageUtils {
    //Includes all the utility methods for Home (Landing page)

    /*
    * Gets the logged in user details based on the permission sets assigned to the user
    * Parameters : N/A
    * @Return : Returns a string which says the type of user
    */
    
    public static String getLoggedInUserDetails() {
        String userDetails = null;
        try {
            decimal currentLevel = 0;
            map<string, decimal> mapPermLevel = new map<string, decimal>();
            
            List<PermissionSetAssignment> lstcurrentUserPerSet = [SELECT Id, PermissionSet.Name,AssigneeId
                                                                  FROM PermissionSetAssignment
                                                                  WHERE AssigneeId = :Userinfo.getUserId()];
            
            
            for(EE_User_Permission_Level__mdt permissionData : [Select DeveloperName, MasterLabel, Level_of_Permission__c from EE_User_Permission_Level__mdt order by Level_of_Permission__c desc] ) {
                mapPermLevel.put(permissionData.DeveloperName, permissionData.Level_of_Permission__c);
            }
            
            for (PermissionSetAssignment psa: lstcurrentUserPerSet)
            {
                decimal level=0;
                if(psa.PermissionSet.Name.equalsIgnoreCase(DF_Constants.BUSINESS_PERMISSION_SET)) {
                    //userDetails = 'business';
                    level = mapPermLevel.get(DF_Constants.BUSINESS_PERMISSION_SET);
                }    
                else if(psa.PermissionSet.Name.equalsIgnoreCase(DF_Constants.EE_Pre_Order_Plus_Permission)) {
                    //userDetails = 'business';
                    level = mapPermLevel.get(DF_Constants.EE_Pre_Order_Plus_Permission);
                }    
                else if(psa.PermissionSet.Name.equalsIgnoreCase(DF_Constants.EE_Pre_Order_Basic_Permission)) {
                    //userDetails = 'business'; 
                    level = mapPermLevel.get(DF_Constants.EE_Pre_Order_Basic_Permission);  
                }    
                else if(psa.PermissionSet.Name.equalsIgnoreCase(DF_Constants.BUSINESS_PLUS_PERMISSION_SET)) {
                    //userDetails = 'businessPlus';
                    level = mapPermLevel.get(DF_Constants.BUSINESS_PLUS_PERMISSION_SET);
                } 
                
                if(level > currentLevel) {
                    currentLevel = level;
                }      
                //else if(psa.PermissionSet.Name.equalsIgnoreCase(DF_Constants.))
                    //userDetails = 'assurance';
            }
            
            switch on integer.valueOf(currentLevel) {
               when 1, 2, 3 {
                   userDetails = 'business';
               }               
               when 4 {
                   userDetails = 'businessPlus';
               }
            }
            
            
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }
        return userDetails;
    }
}