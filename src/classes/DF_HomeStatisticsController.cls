public with sharing class DF_HomeStatisticsController {
	//Includes the methods for displaying active requests on Home page
	
    /*
	* Gets the logged in user details based on the permission sets assigned to the user
	* Parameters : N/A
	* @Return : Returns a string which says the type of user along with the count of records for Service Feasibility, Order, Ticket
	*/
    @AuraEnabled
    public static String getActiveRequestDetails() {
        String activeRequest = null;
        try {
            String userDetails = DF_HomePageUtils.getLoggedInUserDetails();
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            Integer sfTotalByUser = 0;
            Integer orderTotalByUser = 0;
            Integer ticketTotalByUser = 0;
            Integer sfTotalByAccount = 0;
            Integer orderTotalByAccount = 0;
            Integer ticketTotalByAccount = 0;
            List<DF_Opportunity_Bundle__c> bundleByAccountList = new List<DF_Opportunity_Bundle__c>();
            List<DF_Opportunity_Bundle__c> bundleByUserList = new List<DF_Opportunity_Bundle__c>();
            Id recTypeId = DF_CS_API_Util.getRecordType(DF_Constants.ENTERPRISE_ETHERNET_NAME,DF_Constants.OPPORTUNITY_BUNDLE_OBJECT);
            
            if(userLst.size()>0 && userLst.get(0).AccountId!=null && recTypeId != null){
                bundleByAccountList = [SELECT Id,Name,Account__c,DF_Quote_Count__c,DF_SF_Request_Count__c,DF_Order_Count__c FROM DF_Opportunity_Bundle__c where 
                              Account__c = :userLst.get(0).AccountId AND RecordTypeId = :recTypeId];
                bundleByUserList = [SELECT Id, Name,CreatedById,DF_Quote_Count__c,DF_SF_Request_Count__c,DF_Order_Count__c FROM DF_Opportunity_Bundle__c where 
                              CreatedById = :UserInfo.getUserId() AND RecordTypeId = :recTypeId];
            }
            for(DF_Opportunity_Bundle__c bundle : bundleByAccountList){
                sfTotalByAccount += Integer.valueof(bundle.DF_SF_Request_Count__c);
                orderTotalByAccount += Integer.valueof(bundle.DF_Order_Count__c);
            }
            for(DF_Opportunity_Bundle__c bundle : bundleByUserList){
                sfTotalByUser += Integer.valueof(bundle.DF_SF_Request_Count__c);
                orderTotalByUser += Integer.valueof(bundle.DF_Order_Count__c);
            }
            DF_ActiveRequests ar = new DF_ActiveRequests();
            ar.userType = userDetails;
            ar.sfCountByAccount = sfTotalByAccount;
            ar.orderCountByAccount = orderTotalByAccount;
            ar.ticketCountByAccount = ticketTotalByAccount;
            ar.sfCountByUser = sfTotalByUser;
            ar.orderCountByUser = orderTotalByUser;
            ar.ticketCountByUser = ticketTotalByUser;
            activeRequest = JSON.serialize(ar);

        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }
        return activeRequest;
    }
}