@isTest 
private class DF_HomeStatisticsController_Test {
    
    @isTest
    private static void test_getBusinessUserDetails() {
        String result = null;
        User commUser;
        commUser = DF_TestData.createDFCommUser();
        system.runAs(commUser) {
            PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = :DF_Constants.BUSINESS_PERMISSION_SET];
            insert new PermissionSetAssignment(AssigneeId = commUser.id, PermissionSetId = ps.Id );
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            DF_Opportunity_Bundle__c oppBundle = DF_TestData.createOpportunityBundle(userLst.get(0).AccountId);
            insert oppBundle;
            test.startTest();    
            result = DF_HomeStatisticsController.getActiveRequestDetails();
            System.debug(result);
            test.stopTest();                
        }
        // Assertions
        system.AssertNotEquals(null, result);
    }
}