public class DF_IntegrationUtils {

	public static final String ACCESS_SEEKER_ID = 'accessSeekerID';
	public static final String CORRELATION_ID = 'correlationId';
	public static final String TIMESTAMP = 'timestamp';
	public static final String NBN_ENV_OVRD = 'NBN-Environment-Override';

	public static final String ERR_ACCESS_SEEKER_ID_NOT_FOUND = 'accessSeekerId for RSP Account was not found.';
	public static final String ERR_CORRELATION_ID_NOT_FOUND = 'correlationId was not found.';	
	public static final String ERR_REQUEST_BODY_EMPTY = 'Request body is empty.';
	public static final String ERR_RESP_BODY_MISSING = 'Response Body is missing.';
	public static final String ERR_PROD_URL_CONFIGURED = 'Production EndPoint is configured in Sandbox for SOA Named Credentials';

    public static String stripJsonNulls(String jsonString) {
    	try {
	    	if (jsonString != null) {
				jsonString = jsonString.replaceAll('\"[^\"]*\":null',''); //basic removal of null values
				jsonString = jsonString.replaceAll(',{2,}', ','); //remove duplicate/multiple commas
				jsonString = jsonString.replace('{,', '{'); //prevent opening brace from having a comma after it
				jsonString = jsonString.replace(',}', '}'); //prevent closing brace from having a comma before it
				jsonString = jsonString.replace('[,', '['); //prevent opening bracket from having a comma after it
				jsonString = jsonString.replace(',]', ']'); //prevent closing bracket from having a comma before it
	    	}    		
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

		return jsonString;
    }    
    
    public static String generateGUID() {    	
		String kHexChars = '0123456789abcdef';
        String guId = '';
        Integer nextByte = 0;

		try {
	        for (Integer i = 0; i < 16; i++) {
	            if (i == 4 || i == 6 || i == 8 || i == 10) {
	            	guId+= '-';
	            }
	
	            nextByte = (Math.round(Math.random() * 255)-128) & 255;
	
	            if (i == 6) {
	                nextByte = nextByte & 15;
	                nextByte = nextByte | (4 << 4);
	            }
	
	            if (i == 8) {
	                nextByte = nextByte & 63;
	                nextByte = nextByte | 128;
	            }
	
	            guId+= getCharAtIndex(kHexChars, nextByte >> 4);
	            guId+= getCharAtIndex(kHexChars, nextByte & 15);
	        }
	
			guId = trimStringToSize(guId, 36);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return guId;
    }       

    @TestVisible private static String getCharAtIndex(String str, Integer index) {    	
		try {
	        if (str == null) {
	        	return null;
	        } 
	
	        if (str.length() <= 0) {
	        	return str;
	        } 
	
	        if (index == str.length()) {
				return null;
	        }	
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return str.substring(index, index + 1);
    }    

    public static String trimStringToSize(String inputString, Integer maxLength) {   
		String outputString;

        try {        	
        	if (String.isNotEmpty(inputString) && maxLength != null && maxLength > 0) {
				if (inputString.length() > maxLength) {													
					outputString = inputString.substring(0, maxLength);
				} else {
					outputString = inputString;
				}
        	}
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return outputString;
    }    
         
}