public class DF_LAPIPlacesAPIService {

	public static final String ERR_INVALID_SEARCH_BY_LOCID_INPUTS = 'Invalid SearchByLocationId Inputs.';
	public static final String ERR_INVALID_SEARCH_BY_LATLONG_INPUTS = 'Invalid SearchByLatLong Inputs.';
	public static final String ERR_INVALID_SEARCH_BY_ADDR_INPUTS = 'Invalid SearchByAddress Inputs.';
	public static final String ERR_LOCATIONID_NOT_FOUND = 'LocationId was not found.';
	public static final String ERR_LATLONG_NOT_FOUND = 'Latitide or longitude was not found.';
	public static final String ERR_SFREQ_NOT_FOUND = 'Service Feasibility Request record was not found.';
	public static final String ERR_ADDR_API_ENDPOINT_PARAMS_EMPTY = 'Address API endpoint parameters are empty.';
	
	public static Boolean stubPNI = false;

	@future(callout=true)
	public static void searchLocationByLocationId(String param) {	
        final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;
		// Headers        
        final String NBN_ENV_OVRD_VAL;		
		// Errors             
		final String ERROR_MESSAGE = 'A HTTP error occurred in class method DF_LAPIPlacesAPIService.searchLocationByLocationId'; 

        Map<String, String> paramMap = new Map<String, String>();
        Map<String, String> headerMap = new Map<String, String>();

        Http http = new Http();     
        HttpRequest req;              
        HTTPResponse res;  

		Id sfReqId;
        DF_SF_Request__c sfReq;
        String locationId;
        String response;

        try {             	   	
        	sfReqId = param;        	
        	system.debug('sfReqId: ' + sfReqId);        	
        	
            // Get SF Req data
            sfReq = DF_LAPI_APIServiceUtils.getSFReqDetails(sfReqId);

            if (sfReq != null) {
		        // Validate data inputs
		        if (DF_LAPI_APIServiceUtils.hasValidSearchByLocationIdInputs(sfReq)) {
					locationId = sfReq.Location_Id__c;
		        } else {
					throw new CustomException(DF_LAPIPlacesAPIService.ERR_INVALID_SEARCH_BY_LOCID_INPUTS);		        	
		        }                
            } else {
            	throw new CustomException(DF_LAPIPlacesAPIService.ERR_SFREQ_NOT_FOUND);
            }                       
	    	
            system.debug('locationId: ' + locationId);            	
            
			if (String.isEmpty(locationId)) {
				throw new CustomException(DF_LAPIPlacesAPIService.ERR_LOCATIONID_NOT_FOUND);
			}

			DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
																Timeout__c,
																Enable_Stubbing_PNI__c,
																DP_Environment_Override__c
														 FROM   DF_Integration_Setting__mdt
														 WHERE  DeveloperName = :DF_LAPI_APIServiceUtils.INTEGR_SETTING_NAME_LOCATION_DISTANCE_API];

			NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
			TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
			stubPNI = integrSetting.Enable_Stubbing_PNI__c;
			NBN_ENV_OVRD_VAL = integrSetting.DP_Environment_Override__c;
			
			system.debug('NAMED_CREDENTIAL: ' + NAMED_CREDENTIAL); 
			system.debug('TIMEOUT_LIMIT: ' + TIMEOUT_LIMIT);
			system.debug('stubPNI: ' + stubPNI);
			system.debug('NBN_ENV_OVRD_VAL: ' + NBN_ENV_OVRD_VAL); 

            // Build endpoint
            END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL + '/' + locationId;
            system.debug('END_POINT: ' + END_POINT);     

			headerMap.put(DF_IntegrationUtils.NBN_ENV_OVRD, NBN_ENV_OVRD_VAL);
			DF_LAPI_APIServiceUtils.getHeaders(headerMap);		

            // Generate request 
            req = DF_LAPI_APIServiceUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, headerMap);
            //req = DF_LAPI_APIServiceUtils.getHttpRequest(END_POINT);
            system.debug('req: ' + req); 

            // Send and get response               
            res = http.send(req);

            system.debug('Response Status Code: ' + res.getStatusCode());
            system.debug('Response Body: ' + res.getBody());

			if (stubPNI) {								
	            if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200 || res.getStatusCode() == DF_Constants.HTTP_STATUS_201
	            		|| res.getStatusCode() == DF_Constants.HTTP_STATUS_202 || res.getStatusCode() == DF_Constants.HTTP_STATUS_203
	            		|| res.getStatusCode() == DF_Constants.HTTP_STATUS_204 || res.getStatusCode() == DF_Constants.HTTP_STATUS_205
	            		|| res.getStatusCode() == DF_Constants.HTTP_STATUS_206) { 	 	         
	                response = res.getBody();  
	
					if (String.isNotEmpty(response)) {												
		                // Update sfReq.Status to "Completed"
		                // Update sfReq.Response with data from response                                                    
		                DF_LAPI_APIServiceUtils.getLocationByLocationIdSuccessPostProcessing(response, locationId, sfReq);   					
					} else {
						throw new CustomException(DF_IntegrationUtils.ERR_RESP_BODY_MISSING);
					}
	            } else {		            	            	
	            	// Update sfReq.Status to "Completed" or "In Error"
	                DF_LAPI_APIServiceUtils.getLocationByLocationIdFailurePostProcessing(response, res.getStatusCode(), sfReq);//Raja: Added response    
	                
	                List<String> paramList = new List<String>{param};                  
	                AsyncQueueableUtils.retry(DF_LAPISearchByLocationHandler.HANDLER_NAME, paramList, ERROR_MESSAGE + '\n\n' + 'Response Status Code: ' + res.getStatusCode());    	                                           
	            }				
			} else {	
                response = res.getBody();
	            if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) { 	    	            
		
					if (String.isNotEmpty(response)) {												
		                // Update sfReq.Status to "Completed"
		                // Update sfReq.Response with data from response                                                    
		                DF_LAPI_APIServiceUtils.getLocationByLocationIdSuccessPostProcessing(response, locationId, sfReq);   					
					} else {
						throw new CustomException(DF_IntegrationUtils.ERR_RESP_BODY_MISSING);
					}
	            } else {	        
	            	// Update sfReq.Status to "Completed" or "In Error"
	                DF_LAPI_APIServiceUtils.getLocationByLocationIdFailurePostProcessing(response, res.getStatusCode(), sfReq);//Raja: Added response    
	                
	                List<String> paramList = new List<String>{param};                  
	                AsyncQueueableUtils.retry(DF_LAPISearchByLocationHandler.HANDLER_NAME, paramList, ERROR_MESSAGE + '\n\n' + 'Response Status Code: ' + res.getStatusCode());    	                                           
	            }				
			}                    				
        } catch (Exception e) {        	
        	DF_LAPI_APIServiceUtils.getLocationByLocationIdFailurePostProcessing(response, null, sfReq);//Raja: Added response

            GlobalUtility.logMessage(DF_Constants.ERROR, DF_LAPIPlacesAPIService.class.getName(), 'searchLocationByLocationId', '', '', '', response, e, 0);
            
            List<String> paramList = new List<String>{param};              
            AsyncQueueableUtils.retry(DF_LAPISearchByLocationHandler.HANDLER_NAME, paramList, (e.getMessage() + '\n\n' + e.getStackTraceString()));       
        }      
    }
    
    public static String editLocationSync(DF_SF_Request__c sfReq) {
        String searchType = sfReq.Search_Type__c;
        String response = '';

        try {
			DF_Integration_Setting__mdt integrSetting = getIntegrationSettingsByType(searchType);
            stubPNI = integrSetting.Enable_Stubbing_PNI__c;              
            
            HTTPResponse res = makeCalloutByType(integrSetting, sfReq);
            response = res.getBody();
            
			if (stubPNI && DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID.equals(searchType)) {
	            if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200 || res.getStatusCode() == DF_Constants.HTTP_STATUS_201
	            		|| res.getStatusCode() == DF_Constants.HTTP_STATUS_202 || res.getStatusCode() == DF_Constants.HTTP_STATUS_203
	            		|| res.getStatusCode() == DF_Constants.HTTP_STATUS_204 || res.getStatusCode() == DF_Constants.HTTP_STATUS_205
	            		|| res.getStatusCode() == DF_Constants.HTTP_STATUS_206) {
					return handleSuccessPostProcessing(response, sfReq);
	            } else {
	                return handleFailurePostProcessing(response, res.getStatusCode(), sfReq);
	            }
			} else {	
	            if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {
                    return handleSuccessPostProcessing(response, sfReq);
	            } else {
                    return handleFailurePostProcessing(response, res.getStatusCode(), sfReq);
	            }
			}
        } catch (Exception e) {
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_LAPIPlacesAPIService.class.getName(), 'editLocationSync', '', '', '', response, e, 0);
            return handleFailurePostProcessing(response, null, sfReq);
        }
    }
    
    private static DF_Integration_Setting__mdt getIntegrationSettingsByType(String searchType) {
        String developName;
        if (DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG.equals(searchType)) {
            developName = DF_LAPI_APIServiceUtils.INTEGR_SETTING_NAME_LOCATION_API;
        } else if (DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS.equals(searchType)) {
            developName = DF_LAPI_APIServiceUtils.INTEGR_SETTING_NAME_ADDRESS_API;
        } else {
            // default to search by location id
            developName = DF_LAPI_APIServiceUtils.INTEGR_SETTING_NAME_LOCATION_DISTANCE_API;
        }
        
        return [SELECT 	Named_Credential__c,
                		Timeout__c,
                		Enable_Stubbing_PNI__c,
                		DP_Environment_Override__c
                FROM   DF_Integration_Setting__mdt
                WHERE  DeveloperName = :developName];
    }
    
    private static HTTPResponse makeCalloutByType(DF_Integration_Setting__mdt integrSetting, DF_SF_Request__c sfReq) {
        String searchType = sfReq.Search_Type__c;
        
        stubPNI = integrSetting.Enable_Stubbing_PNI__c;
        final String NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
        final Integer TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
        final String NBN_ENV_OVRD_VAL = integrSetting.DP_Environment_Override__c;
        
        system.debug('stubPNI: ' + stubPNI);
        system.debug('NAMED_CREDENTIAL: ' + NAMED_CREDENTIAL); 
        system.debug('TIMEOUT_LIMIT: ' + TIMEOUT_LIMIT);
        system.debug('NBN_ENV_OVRD_VAL: ' + NBN_ENV_OVRD_VAL); 
        
        final String END_POINT;
        if (DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG.equals(searchType)) {
            final String LATLONG_PARAMS = DF_LAPI_APIServiceUtils.URL_FILTER_PREFIX + DF_LAPI_APIServiceUtils.LATITUDE + DF_LAPI_APIServiceUtils.URL_PARAM_ASSIGN_OPR + sfReq.Latitude__c 
                + DF_LAPI_APIServiceUtils.URL_PARAM_DELIMITER + DF_LAPI_APIServiceUtils.LONGITUDE + DF_LAPI_APIServiceUtils.URL_PARAM_ASSIGN_OPR + sfReq.Longitude__c;      
                
            END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL + LATLONG_PARAMS;                
        } else if (DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS.equals(searchType)) {
            final String INPUT_ADDR_QUERY_FILTER = DF_LAPI_APIServiceUtils.getInputAddressQueryFilter(sfReq);
            final String SEARCH_PARAMS = DF_LAPI_APIServiceUtils.convertToURLEncodedStr(INPUT_ADDR_QUERY_FILTER);
            
            END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL + DF_LAPI_APIServiceUtils.URL_FILTER_PREFIX + SEARCH_PARAMS; 
        } else {
            // default to search by location id
            END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL + '/' + sfReq.Location_Id__c;
        }
        
        // Headers
        Map<String, String> headerMap = new Map<String, String>();
        headerMap.put(DF_IntegrationUtils.NBN_ENV_OVRD, NBN_ENV_OVRD_VAL);
		DF_LAPI_APIServiceUtils.getHeaders(headerMap); 
        
        system.debug('END_POINT: ' + END_POINT);
        system.debug('headerMap: ' + headerMap);
        
        // Generate request
        HttpRequest req = DF_LAPI_APIServiceUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, headerMap);
        system.debug('req: ' + req);
        
        // Send and get response               
        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        system.debug('Response Status Code: ' + res.getStatusCode());
        system.debug('Response Body: ' + res.getBody());
        
        return res;
    }
    
    private static String handleFailurePostProcessing(String response, Integer responseStatusCode, DF_SF_Request__c sfReq) {
        String searchType = sfReq.Search_Type__c;
        if (DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG.equals(searchType)) {
            DF_LAPI_APIServiceUtils.getLocationByLatLongFailurePostProcessing(responseStatusCode, sfReq);
        } else if (DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS.equals(searchType)) {
            DF_LAPI_APIServiceUtils.getLocationByAddressFailurePostProcessing(sfReq);
        } else {
            // default to search by location id
            DF_LAPI_APIServiceUtils.getLocationByLocationIdFailurePostProcessing(response, responseStatusCode, sfReq);
        }
        return 'KO';
    }
    
    private static String handleSuccessPostProcessing(String response, DF_SF_Request__c sfReq) {
        if (String.isEmpty(response)) {
            throw new CustomException(DF_IntegrationUtils.ERR_RESP_BODY_MISSING);
        }
        
        String searchType = sfReq.Search_Type__c;
        if (DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG.equals(searchType)) {
            DF_LAPI_APIServiceUtils.getLocationByLatLongSuccessPostProcessing(response, sfReq);
        } else if (DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS.equals(searchType)) {
            DF_LAPI_APIServiceUtils.getLocationByAddressSuccessPostProcessing(response, sfReq);
        } else {
        	// default to search by location id
            DF_LAPI_APIServiceUtils.getLocationByLocationIdSuccessPostProcessing(response, sfReq.Location_Id__c, sfReq);
        }
        
        return 'OK';
    }
    
	@future(callout=true)
	public static void searchLocationByLatLong(String param) {	
        final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;
		// Headers        
        final String NBN_ENV_OVRD_VAL;
        final String LATLONG_PARAMS;		
		// Errors             
		final String ERROR_MESSAGE = 'A HTTP error occurred in class method DF_LAPIPlacesAPIService.searchLocationByLatLong';        

        Map<String, String> headerMap = new Map<String, String>();

        Http http = new Http();     
        HttpRequest req;              
        HTTPResponse res; 
        DF_SF_Request__c sfReq;

		Id sfReqId;
        String response;        
        String longitude;
        String latitude;

        try {        	
        	sfReqId = param;
        	system.debug('sfReqId: ' + sfReqId);
        	
            // Get SF Req data
            sfReq = DF_LAPI_APIServiceUtils.getSFReqDetails(sfReqId);
            
            if (sfReq != null) {
	            // Validate data inputs
	            if (DF_LAPI_APIServiceUtils.hasValidSearchByLatLongInputs(sfReq)) {	            	
	                latitude = sfReq.Latitude__c;
	                longitude = sfReq.Longitude__c;  
	            } else {
					throw new CustomException(DF_LAPIPlacesAPIService.ERR_INVALID_SEARCH_BY_LATLONG_INPUTS);	            	
	            }
            } else {
            	throw new CustomException(DF_LAPIPlacesAPIService.ERR_SFREQ_NOT_FOUND);
            } 

            system.debug('longitude: ' + longitude);
            system.debug('latitude: ' + latitude);

			if (String.isEmpty(latitude) || String.isEmpty(longitude)) {
				throw new CustomException(DF_LAPIPlacesAPIService.ERR_LATLONG_NOT_FOUND);
			}

			DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
																Timeout__c,
																DP_Environment_Override__c
														 FROM   DF_Integration_Setting__mdt
														 WHERE  DeveloperName = :DF_LAPI_APIServiceUtils.INTEGR_SETTING_NAME_LOCATION_API];

			NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
			TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
			NBN_ENV_OVRD_VAL = integrSetting.DP_Environment_Override__c;
			
			system.debug('NAMED_CREDENTIAL: ' + NAMED_CREDENTIAL); 
			system.debug('TIMEOUT_LIMIT: ' + TIMEOUT_LIMIT);
			system.debug('NBN_ENV_OVRD_VAL: ' + NBN_ENV_OVRD_VAL); 

            // Build endpoint
            LATLONG_PARAMS = DF_LAPI_APIServiceUtils.URL_FILTER_PREFIX + DF_LAPI_APIServiceUtils.LATITUDE + DF_LAPI_APIServiceUtils.URL_PARAM_ASSIGN_OPR + latitude + DF_LAPI_APIServiceUtils.URL_PARAM_DELIMITER + DF_LAPI_APIServiceUtils.LONGITUDE + DF_LAPI_APIServiceUtils.URL_PARAM_ASSIGN_OPR + longitude;
            system.debug('LATLONG_PARAMS: ' + LATLONG_PARAMS);        
                
            END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL + LATLONG_PARAMS;
            system.debug('END_POINT: ' + END_POINT);     

			headerMap.put(DF_IntegrationUtils.NBN_ENV_OVRD, NBN_ENV_OVRD_VAL);
			DF_LAPI_APIServiceUtils.getHeaders(headerMap);

            // Generate request 
            req = DF_LAPI_APIServiceUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, headerMap);
            //req = DF_LAPI_APIServiceUtils.getHttpRequest(END_POINT);
            system.debug('req: ' + req); 

            // Send and get response               
            res = http.send(req);

            system.debug('Response Status Code: ' + res.getStatusCode());
            system.debug('Response Body: ' + res.getBody());

            if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {            	            	            	
                // Response Status 200:
                //   Results would be one of the following outcomes:
                // - Results returned
                // - 0 results returned                           
                response = res.getBody();  

				if (String.isNotEmpty(response)) {										
	                // Update sfReq.Status to "Completed"
	                // Update sfReq.Response with data from response
	                DF_LAPI_APIServiceUtils.getLocationByLatLongSuccessPostProcessing(response, sfReq);    					
				} else {					
					throw new CustomException(DF_IntegrationUtils.ERR_RESP_BODY_MISSING);
				}                         
            } else {                  	            	      	
                // Response Status 400 / 404 etc
                // Update sfReq.Status to "In Error"                
                // Update sfReq.Response.Status to "Invalid"
                DF_LAPI_APIServiceUtils.getLocationByLatLongFailurePostProcessing(res.getStatusCode(), sfReq);
                
                List<String> paramList = new List<String>{param};                  
                AsyncQueueableUtils.retry(DF_LAPISearchByLatLongHandler.HANDLER_NAME, paramList, ERROR_MESSAGE + '\n\n' + 'Response Status Code: ' + res.getStatusCode());                 
            }           				
        } catch (Exception e) {        	
        	DF_LAPI_APIServiceUtils.getLocationByLatLongFailurePostProcessing(null, sfReq);
        	
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_LAPIPlacesAPIService.class.getName(), 'searchLocationByLatLong', '', '', '', response, e, 0);
            
            List<String> paramList = new List<String>{param};              
            AsyncQueueableUtils.retry(DF_LAPISearchByLatLongHandler.HANDLER_NAME, paramList, (e.getMessage() + '\n\n' + e.getStackTraceString()));   
        } 
    }
    
	@future(callout=true)
	public static void searchLocationByAddress(String param) {	
        final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;
		// Headers        
        final String NBN_ENV_OVRD_VAL;
        final String INPUT_ADDR_QUERY_FILTER;
        final String SEARCH_PARAMS;		
		// Errors             
		final String ERROR_MESSAGE = 'A HTTP error occurred in class method DF_LAPIPlacesAPIService.searchLocationByAddress';  

        Map<String, String> headerMap = new Map<String, String>();

        Http http = new Http();     
        HttpRequest req;              
        HTTPResponse res; 
        DF_SF_Request__c sfReq;

		Id sfReqId;
        String response;

        try {        	
        	sfReqId = param;
        	system.debug('sfReqId: ' + sfReqId);
        	
            // Get SF Req data
            sfReq = DF_LAPI_APIServiceUtils.getSFReqDetails(sfReqId);
            
            if (sfReq != null) {
                // Validate data inputs
                if (!DF_LAPI_APIServiceUtils.hasValidSearchByAddressInputs(sfReq)) {					
					throw new CustomException(DF_LAPIPlacesAPIService.ERR_INVALID_SEARCH_BY_ADDR_INPUTS);                	
                }
            } else {            	
            	throw new CustomException(DF_LAPIPlacesAPIService.ERR_SFREQ_NOT_FOUND);
            }

			DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
																Timeout__c,
																DP_Environment_Override__c
														 FROM   DF_Integration_Setting__mdt
														 WHERE  DeveloperName = :DF_LAPI_APIServiceUtils.INTEGR_SETTING_NAME_ADDRESS_API];

			NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
			TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
			NBN_ENV_OVRD_VAL = integrSetting.DP_Environment_Override__c;
			
			system.debug('NAMED_CREDENTIAL: ' + NAMED_CREDENTIAL); 
			system.debug('TIMEOUT_LIMIT: ' + TIMEOUT_LIMIT);
			system.debug('NBN_ENV_OVRD_VAL: ' + NBN_ENV_OVRD_VAL); 

            // Build address params for endpoint
            INPUT_ADDR_QUERY_FILTER = DF_LAPI_APIServiceUtils.getInputAddressQueryFilter(sfReq);
			system.debug('INPUT_ADDR_QUERY_FILTER: ' + INPUT_ADDR_QUERY_FILTER);

            // Convert to urlEndcoded           
            SEARCH_PARAMS = DF_LAPI_APIServiceUtils.convertToURLEncodedStr(INPUT_ADDR_QUERY_FILTER);
            system.debug('SEARCH_PARAMS: ' + SEARCH_PARAMS);

            if (String.isEmpty(SEARCH_PARAMS)) {                 	       	
                throw new CustomException(DF_LAPIPlacesAPIService.ERR_ADDR_API_ENDPOINT_PARAMS_EMPTY); 
            }

            // Build endpoint
            END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL + DF_LAPI_APIServiceUtils.URL_FILTER_PREFIX + SEARCH_PARAMS;
            system.debug('END_POINT: ' + END_POINT);     

			headerMap.put(DF_IntegrationUtils.NBN_ENV_OVRD, NBN_ENV_OVRD_VAL);
			DF_LAPI_APIServiceUtils.getHeaders(headerMap);

            // Generate request 
            req = DF_LAPI_APIServiceUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, headerMap);
            //req = DF_LAPI_APIServiceUtils.getHttpRequest(END_POINT);
            system.debug('req: ' + req); 

            // Send and get response               
            res = http.send(req);

            system.debug('Response Status Code: ' + res.getStatusCode());
            system.debug('Response Body: ' + res.getBody());     

            if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {                	      	
                // Includes:
                // - Multiple results returned
                // - 0 results returned             
                response = res.getBody();  

				if (String.isNotEmpty(response)) {										
	                // Update sfReq.Status to "Completed"
	                // Update sfReq.Response with data from response
	                DF_LAPI_APIServiceUtils.getLocationByAddressSuccessPostProcessing(response, sfReq);     					
				} else {					
					throw new CustomException(DF_IntegrationUtils.ERR_RESP_BODY_MISSING);
				}
            } else {                 	            	                       	
                // Update sfReq.Status to "In Error"
                // Update sfReq.Response.Status to "Invalid"
                DF_LAPI_APIServiceUtils.getLocationByAddressFailurePostProcessing(sfReq);
                
                List<String> paramList = new List<String>{param};                  
                AsyncQueueableUtils.retry(DF_LAPISearchByAddressHandler.HANDLER_NAME, paramList, ERROR_MESSAGE + '\n\n' + 'Response Status Code: ' + res.getStatusCode());                    
            }     
        } catch (Exception e) {        	
        	DF_LAPI_APIServiceUtils.getLocationByAddressFailurePostProcessing(sfReq);
        	
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_LAPIPlacesAPIService.class.getName(), 'searchLocationByAddress', '', '', '', response, e, 0);
            
            List<String> paramList = new List<String>{param};              
            AsyncQueueableUtils.retry(DF_LAPISearchByAddressHandler.HANDLER_NAME, paramList, (e.getMessage() + '\n\n' + e.getStackTraceString()));       
        }  
    }     
}