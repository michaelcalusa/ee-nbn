@isTest
private class DF_LAPIPlacesAPIService_Test {

    @isTest static void test_searchLocationByLocationId_Response_200() {         	
    	/** Setup data  **/
    	String response;    	
		User commUser;
    	       
    	String locationId = 'LOC000035375038';
    	
		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		Map<String, String> addressMap = new Map<String, String>();

		// Create sfreq
		DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, DF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);		
		insert sfReq;
		
		String param = sfReq.Id;

   		// Generate mock response
		response = DF_IntegrationTestData.buildLocationDistanceAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

		// Set up commUser to run test as
 		commUser = DF_TestData.createDFCommUser();

		system.runAs(commUser) { 
	    	test.startTest(); 	
			
			DF_LAPIPlacesAPIService.searchLocationByLocationId(param);				
			
	        test.stopTest();     			
		}                                                  

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id 
                           					FROM   DF_SF_Request__c 
                           					WHERE  Opportunity_Bundle__c = :opptyBundle.Id
                           					AND    Status__c = :DF_LAPI_APIServiceUtils.STATUS_COMPLETED];

		system.AssertEquals(false, sfReqList.isEmpty());   	 	 	    	

    }
    
    @isTest static void shouldGetSuccessfulResponseOnEditLocationSyncForLocId() {
        String locationId = 'LOC000005972574';
        String response = null;
        
        // Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Create sfreq
        DF_SF_Request__c request = EE_CISLocationAPIUtils.createServiceFeasibilityRequest(
            DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, locationId, null, null, opptyBundle.Id, response, new Map<String, String>());
        insert request;
        
        response = DF_IntegrationTestData.buildLocationDistanceAPIRespSuccessful();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
        
        test.startTest(); 	
        
        System.assertEquals(DF_LAPI_APIServiceUtils.STATUS_PENDING, request.Status__c);
        
        String result = DF_LAPIPlacesAPIService.editLocationSync(request);
        System.assertEquals('OK', result);
        
        System.assertEquals(DF_LAPI_APIServiceUtils.STATUS_COMPLETED, request.Status__c);
        System.assertNotEquals('', request.Response__c);
        DF_ServiceFeasibilityResponse sfResponse = (DF_ServiceFeasibilityResponse) System.JSON.deserialize(request.Response__c, DF_ServiceFeasibilityResponse.class);
        System.assertEquals(EE_CISLocationAPIUtils.STATUS_VALID, sfResponse.status);
        
        test.stopTest();
    }
    
    @isTest static void shouldGetFailedResponseOnEditLocationSyncForLocIdWhenStatusCodeIs200AndBodyIsEmpty() {
        String locationId = 'LOC000005972574';
        String response = null;
        
        // Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Create sfreq
        DF_SF_Request__c request = EE_CISLocationAPIUtils.createServiceFeasibilityRequest(
            DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, locationId, null, null, opptyBundle.Id, response, new Map<String, String>());
        insert request;
        
        response = '';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
        
        test.startTest(); 	
        
        System.assertEquals(DF_LAPI_APIServiceUtils.STATUS_PENDING, request.Status__c);
        
        String result = DF_LAPIPlacesAPIService.editLocationSync(request);
        System.assertEquals('KO', result);
        
        test.stopTest();
    }
    
    @isTest static void shouldGetFailedResponseOnEditLocationSyncForLocIdWhenStatusCodeIs500() {
        String locationId = 'LOC000005972574';
        String response = null;
        
        // Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Create sfreq
        DF_SF_Request__c request = EE_CISLocationAPIUtils.createServiceFeasibilityRequest(
            DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, locationId, null, null, opptyBundle.Id, response, new Map<String, String>());
        insert request;
        
        response = DF_IntegrationTestData.buildLocationDistanceAPIRespSuccessful();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(500, response, null));
        
        test.startTest(); 	
        
        System.assertEquals(DF_LAPI_APIServiceUtils.STATUS_PENDING, request.Status__c);
        
        String result = DF_LAPIPlacesAPIService.editLocationSync(request);
        System.assertEquals('KO', result);
        
        test.stopTest();
    }
    
    @isTest static void shouldGetSuccessfulResponseOnEditLocationSyncForLatLong() {
        String lati = '-42.789312';
        String longi = '147.05578';
        String response = null;
        
        // Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Create sfreq
        DF_SF_Request__c request = EE_CISLocationAPIUtils.createServiceFeasibilityRequest(
            DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG, null, lati, longi, opptyBundle.Id, response, new Map<String, String>());
        insert request;
        
        response = DF_IntegrationTestData.buildLocationAPIRespSuccessful();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
        
        test.startTest(); 	
        
        System.assertEquals(DF_LAPI_APIServiceUtils.STATUS_PENDING, request.Status__c);
        
        String result = DF_LAPIPlacesAPIService.editLocationSync(request);
        System.assertEquals('OK', result);
        
        System.assertNotEquals('', request.Response__c);
        
        test.stopTest();
    }
    
    @isTest static void shouldGetFailedResponseOnEditLocationSyncForLatLongWhenStatusCodeIs500() {
        String lati = '-42.789312';
        String longi = '147.05578';
        String response = null;
        
        // Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Create sfreq
        DF_SF_Request__c request = EE_CISLocationAPIUtils.createServiceFeasibilityRequest(
            DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG, null, lati, longi, opptyBundle.Id, response, new Map<String, String>());
        insert request;
        
        response = DF_IntegrationTestData.buildLocationAPIRespSuccessful();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(500, response, null));
        
        test.startTest(); 	
        
        System.assertEquals(DF_LAPI_APIServiceUtils.STATUS_PENDING, request.Status__c);
        
        String result = DF_LAPIPlacesAPIService.editLocationSync(request);
        System.assertEquals('KO', result);
        
        System.assertNotEquals('', request.Response__c);
        
        test.stopTest();
    }
    
    @isTest static void shouldGetSuccessfulResponseOnEditLocationSyncForAddress() {
        String response = null;
        
        // Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		// Address values
		String state = 'VIC';
		String postcode = '3000';
		String suburbLocality = 'Melbournje';
		String streetName = 'Jump';
		String streetType = 'ST';
		String streetLotNumber = '21';
		String unitType = 'UNIT';
		String unitNumber = '1';

		String level;
		String complexSiteName;
		String complexBuildingName;
		String complexStreetName;
		String complexStreetType;
		String complexStreetNumber;						

		// Build address map
		Map<String, String> addressMap = new Map<String, String>();
		addressMap.put('state', state);
		addressMap.put('postcode', postcode);
		addressMap.put('suburbLocality', suburbLocality);
		addressMap.put('streetName', streetName);
		addressMap.put('streetType', streetType);
		addressMap.put('streetLotNumber', streetLotNumber);
		addressMap.put('unitType', unitType);
		addressMap.put('unitNumber', unitNumber);
		addressMap.put('level', level);
		addressMap.put('complexSiteName', complexSiteName);
		addressMap.put('complexBuildingName', complexBuildingName);
		addressMap.put('complexStreetName', complexStreetName);
		addressMap.put('complexStreetType', complexStreetType);
		addressMap.put('complexStreetNumber', complexStreetNumber);

		// Create sfreq
		DF_SF_Request__c request = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, 
                                                             DF_LAPI_APIServiceUtils.STATUS_PENDING, null, null, null, 
                                                             opptyBundle.Id, response, addressMap);		
		insert request;
        
        test.startTest(); 	
        
        // Generate mock response
		response = DF_IntegrationTestData.buildAddressAPIRespSuccessfulForMutipleMatches();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
        
        System.assertEquals(DF_LAPI_APIServiceUtils.STATUS_PENDING, request.Status__c);
        
        String result = DF_LAPIPlacesAPIService.editLocationSync(request);
        System.assertEquals('OK', result);
        
        System.assertNotEquals('', request.Response__c);
        
        test.stopTest();
    }
    
    @isTest static void shouldGetFailedResponseOnEditLocationSyncForAddressWhenStatusCodeIs500() {
        String response = null;
        
        // Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		// Address values
		String state = 'VIC';
		String postcode = '3000';
		String suburbLocality = 'Melbournje';
		String streetName = 'Jump';
		String streetType = 'ST';
		String streetLotNumber = '21';
		String unitType = 'UNIT';
		String unitNumber = '1';

		String level;
		String complexSiteName;
		String complexBuildingName;
		String complexStreetName;
		String complexStreetType;
		String complexStreetNumber;						

		// Build address map
		Map<String, String> addressMap = new Map<String, String>();
		addressMap.put('state', state);
		addressMap.put('postcode', postcode);
		addressMap.put('suburbLocality', suburbLocality);
		addressMap.put('streetName', streetName);
		addressMap.put('streetType', streetType);
		addressMap.put('streetLotNumber', streetLotNumber);
		addressMap.put('unitType', unitType);
		addressMap.put('unitNumber', unitNumber);
		addressMap.put('level', level);
		addressMap.put('complexSiteName', complexSiteName);
		addressMap.put('complexBuildingName', complexBuildingName);
		addressMap.put('complexStreetName', complexStreetName);
		addressMap.put('complexStreetType', complexStreetType);
		addressMap.put('complexStreetNumber', complexStreetNumber);

		// Create sfreq
		DF_SF_Request__c request = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, 
                                                             DF_LAPI_APIServiceUtils.STATUS_PENDING, null, null, null, 
                                                             opptyBundle.Id, response, addressMap);		
		insert request;
        
        test.startTest(); 	
        
        // Generate mock response
		response = DF_IntegrationTestData.buildAddressAPIRespSuccessfulForMutipleMatches();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(500, response, null));
        
        System.assertEquals(DF_LAPI_APIServiceUtils.STATUS_PENDING, request.Status__c);
        
        String result = DF_LAPIPlacesAPIService.editLocationSync(request);
        System.assertEquals('KO', result);
        
        System.assertNotEquals('', request.Response__c);
        
        test.stopTest();
    }
    
    @isTest static void test_searchLocationByLocationId_Response_400() {         	
    	/** Setup data  **/
    	String response;    	
		User commUser;
    	       
    	String locationId = 'LOC000035375038';
    	
		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		Map<String, String> addressMap = new Map<String, String>();

		// Create sfreq
		DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, DF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);		
		insert sfReq;
		
		String param = sfReq.Id;

   		// Generate mock response
		response = DF_IntegrationTestData.buildLocationDistanceAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(400, response, null));

		// Set up commUser to run test as
 		commUser = DF_TestData.createDFCommUser();

		system.runAs(commUser) { 
	    	test.startTest(); 	
			
			DF_LAPIPlacesAPIService.searchLocationByLocationId(param);				
			
	        test.stopTest();     			
		}   
		
        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id 
                           					FROM   DF_SF_Request__c 
                           					WHERE  Opportunity_Bundle__c = :opptyBundle.Id
                           					AND    Status__c = :DF_LAPI_APIServiceUtils.STATUS_COMPLETED];

		system.AssertEquals(true, sfReqList.isEmpty());
    }    
    
    @isTest static void test_searchLocationByLatLong_Response_200() {         	
    	/** Setup data  **/
    	String response;    	
		User commUser;
	
	    String latitude = '-42.78931';
	    String longitude = '147.05578'; 

		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		Map<String, String> addressMap = new Map<String, String>();

		// Create sfreq
		DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG, DF_LAPI_APIServiceUtils.STATUS_PENDING, null, latitude, longitude, opptyBundle.Id, response, addressMap);		
		insert sfReq;

		String param = sfReq.Id;

   		// Generate mock response
		response = DF_IntegrationTestData.buildLocationAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

		// Set up commUser to run test as
 		commUser = DF_TestData.createDFCommUser();

		system.runAs(commUser) { 
	    	test.startTest(); 	
			
			DF_LAPIPlacesAPIService.searchLocationByLatLong(param);				
			
	        test.stopTest();     			
		}                                                  
    }  
    
    @isTest static void test_searchLocationByLatLong_Response_400() {         	
    	/** Setup data  **/
    	String response;    	
		User commUser;
	
	    String latitude = '-42.78931';
	    String longitude = '147.05578'; 

		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		Map<String, String> addressMap = new Map<String, String>();

		// Create sfreq
		DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG, DF_LAPI_APIServiceUtils.STATUS_PENDING, null, latitude, longitude, opptyBundle.Id, response, addressMap);		
		insert sfReq;

		String param = sfReq.Id;

   		// Generate mock response
		response = DF_IntegrationTestData.buildLocationAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(400, response, null));

		// Set up commUser to run test as
 		commUser = DF_TestData.createDFCommUser();

		system.runAs(commUser) { 
	    	test.startTest(); 	
			
			DF_LAPIPlacesAPIService.searchLocationByLatLong(param);				
			
	        test.stopTest();     			
		}                                                  
    }    
    
    @isTest static void test_searchLocationByAddressResponse_200() {         	
    	/** Setup data  **/
    	String response;    	
		User commUser;
	
		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		// Address values
		String state = 'VIC';
		String postcode = '3000';
		String suburbLocality = 'Melbournje';
		String streetName = 'Jump';
		String streetType = 'ST';
		String streetLotNumber = '21';
		String unitType = 'UNIT';
		String unitNumber = '1';

		String level;
		String complexSiteName;
		String complexBuildingName;
		String complexStreetName;
		String complexStreetType;
		String complexStreetNumber;						

		// Build address map
		Map<String, String> addressMap = new Map<String, String>();
		addressMap.put('state', state);
		addressMap.put('postcode', postcode);
		addressMap.put('suburbLocality', suburbLocality);
		addressMap.put('streetName', streetName);
		addressMap.put('streetType', streetType);
		addressMap.put('streetLotNumber', streetLotNumber);
		addressMap.put('unitType', unitType);
		addressMap.put('unitNumber', unitNumber);
		addressMap.put('level', level);
		addressMap.put('complexSiteName', complexSiteName);
		addressMap.put('complexBuildingName', complexBuildingName);
		addressMap.put('complexStreetName', complexStreetName);
		addressMap.put('complexStreetType', complexStreetType);
		addressMap.put('complexStreetNumber', complexStreetNumber);

		// Create sfreq
		DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, DF_LAPI_APIServiceUtils.STATUS_PENDING, null, null, null, opptyBundle.Id, response, addressMap);		
		insert sfReq;
 
		// Generate mock response
		response = DF_IntegrationTestData.buildAddressAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

		String param = sfReq.Id;

   		// Generate mock response
		response = DF_IntegrationTestData.buildLocationAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

		// Set up commUser to run test as
 		commUser = DF_TestData.createDFCommUser();

		system.runAs(commUser) { 
	    	test.startTest(); 	
			
			DF_LAPIPlacesAPIService.searchLocationByAddress(param);				
			
	        test.stopTest();     			
		}                                                  	 	 	    	
    }     
    
    @isTest static void test_searchLocationByAddressResponse_400() {         	
    	/** Setup data  **/
    	String response;    	
		User commUser;
	
		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		// Address values
		String state = 'VIC';
		String postcode = '3000';
		String suburbLocality = 'Melbournje';
		String streetName = 'Jump';
		String streetType = 'ST';
		String streetLotNumber = '21';
		String unitType = 'UNIT';
		String unitNumber = '1';

		String level;
		String complexSiteName;
		String complexBuildingName;
		String complexStreetName;
		String complexStreetType;
		String complexStreetNumber;						

		// Build address map
		Map<String, String> addressMap = new Map<String, String>();
		addressMap.put('state', state);
		addressMap.put('postcode', postcode);
		addressMap.put('suburbLocality', suburbLocality);
		addressMap.put('streetName', streetName);
		addressMap.put('streetType', streetType);
		addressMap.put('streetLotNumber', streetLotNumber);
		addressMap.put('unitType', unitType);
		addressMap.put('unitNumber', unitNumber);
		addressMap.put('level', level);
		addressMap.put('complexSiteName', complexSiteName);
		addressMap.put('complexBuildingName', complexBuildingName);
		addressMap.put('complexStreetName', complexStreetName);
		addressMap.put('complexStreetType', complexStreetType);
		addressMap.put('complexStreetNumber', complexStreetNumber);

		// Create sfreq
		DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, DF_LAPI_APIServiceUtils.STATUS_PENDING, null, null, null, opptyBundle.Id, response, addressMap);		
		insert sfReq;
 
		// Generate mock response
		response = DF_IntegrationTestData.buildAddressAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

		String param = sfReq.Id;

   		// Generate mock response
		response = DF_IntegrationTestData.buildLocationAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(400, response, null));

		// Set up commUser to run test as
 		commUser = DF_TestData.createDFCommUser();

		system.runAs(commUser) { 
	    	test.startTest(); 	
			
			DF_LAPIPlacesAPIService.searchLocationByAddress(param);				
			
	        test.stopTest();     			
		}                                                     	 	 	    	
    }    
    
	/** Data Creation **/
    @testSetup static void setup() {
    	try {
    		createCustomSettings();  				        
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }     	
    }
    
    @isTest static void createCustomSettings() {                
        try {
	        Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
			asrConfigSettings.Max_No_of_Future_Calls__c = 50;
			asrConfigSettings.Max_No_of_Parallels__c = 10;
			asrConfigSettings.Max_No_of_Retries__c = 3;
			asrConfigSettings.Max_No_of_Rows__c = 1;			
	        upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }                
    }        
  
}