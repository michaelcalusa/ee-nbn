@isTest
private class DF_LAPISearchByAddressHandler_Test {

    @isTest static void test_executeWork() {            
    	/** Setup data  **/
    	String response;    	
		User commUser;
	
		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		// Address values
		String state = 'VIC';
		String postcode = '3000';
		String suburbLocality = 'Melbournje';
		String streetName = 'Jump';
		String streetType = 'ST';
		String streetLotNumber = '21';
		String unitType = 'UNIT';
		String unitNumber = '1';

		String level;
		String complexSiteName;
		String complexBuildingName;
		String complexStreetName;
		String complexStreetType;
		String complexStreetNumber;						

		// Build address map
		Map<String, String> addressMap = new Map<String, String>();
		addressMap.put('state', state);
		addressMap.put('postcode', postcode);
		addressMap.put('suburbLocality', suburbLocality);
		addressMap.put('streetName', streetName);
		addressMap.put('streetType', streetType);
		addressMap.put('streetLotNumber', streetLotNumber);
		addressMap.put('unitType', unitType);
		addressMap.put('unitNumber', unitNumber);
		addressMap.put('level', level);
		addressMap.put('complexSiteName', complexSiteName);
		addressMap.put('complexBuildingName', complexBuildingName);
		addressMap.put('complexStreetName', complexStreetName);
		addressMap.put('complexStreetType', complexStreetType);
		addressMap.put('complexStreetNumber', complexStreetNumber);

		// Create sfreq
		DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, DF_LAPI_APIServiceUtils.STATUS_PENDING, null, null, null, opptyBundle.Id, response, addressMap);		
		insert sfReq;
 
		// Generate mock response
		response = DF_IntegrationTestData.buildAddressAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

		String param = sfReq.Id;

        List<String> paramList = new List<String>();
        paramList.add(param); 

   		// Generate mock response
		response = DF_IntegrationTestData.buildLocationAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

		// Set up commUser to run test as
 		commUser = DF_TestData.createDFCommUser();

		system.runAs(commUser) { 
	    	test.startTest(); 	
 
			DF_LAPISearchByAddressHandler handler = new DF_LAPISearchByAddressHandler();
    		
	        // Invoke Call
	        handler.executeWork(paramList);			
			
	        test.stopTest();     			
		}                                                  
    }



	/** Data Creation **/
    @testSetup static void setup() {
    	try {
    		createCustomSettings();  				        
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }     	
    }
    
    @isTest static void createCustomSettings() {                
        try {
	        Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
			asrConfigSettings.Max_No_of_Future_Calls__c = 50;
			asrConfigSettings.Max_No_of_Parallels__c = 10;
			asrConfigSettings.Max_No_of_Retries__c = 3;
			asrConfigSettings.Max_No_of_Rows__c = 1;			
	        upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }                
    }  
 
}