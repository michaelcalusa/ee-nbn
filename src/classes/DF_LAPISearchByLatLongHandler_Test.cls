@isTest
private class DF_LAPISearchByLatLongHandler_Test {

    @isTest static void test_executeWork() {            
    	/** Setup data  **/
    	String response;    	
		User commUser;
	
	    String latitude = '-42.78931';
	    String longitude = '147.05578'; 

		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		Map<String, String> addressMap = new Map<String, String>();

		// Create sfreq
		DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG, DF_LAPI_APIServiceUtils.STATUS_PENDING, null, latitude, longitude, opptyBundle.Id, response, addressMap);		
		insert sfReq;

		String param = sfReq.Id;

        List<String> paramList = new List<String>();
        paramList.add(param); 

   		// Generate mock response
		response = DF_IntegrationTestData.buildLocationAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

		// Set up commUser to run test as
 		commUser = DF_TestData.createDFCommUser();

		system.runAs(commUser) { 
	    	test.startTest(); 	
 
			DF_LAPISearchByLatLongHandler handler = new DF_LAPISearchByLatLongHandler();
    		
	        // Invoke Call
	        handler.executeWork(paramList);			
			
	        test.stopTest();     			
		}                                                  
    }



	/** Data Creation **/
    @testSetup static void setup() {
    	try {
    		createCustomSettings();  				        
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }     	
    }
    
    @isTest static void createCustomSettings() {                
        try {
	        Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
			asrConfigSettings.Max_No_of_Future_Calls__c = 50;
			asrConfigSettings.Max_No_of_Parallels__c = 10;
			asrConfigSettings.Max_No_of_Retries__c = 3;
			asrConfigSettings.Max_No_of_Rows__c = 1;			
	        upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }                
    }  
 
}