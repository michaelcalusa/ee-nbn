public class DF_LAPISearchByLocationHandler extends AsyncQueueableHandler {

	public static final String HANDLER_NAME = 'LAPISearchByLocationHandler';

    public DF_LAPISearchByLocationHandler() {
    	// No of params must be 1
        super(DF_LAPISearchByLocationHandler.HANDLER_NAME, 1);     
    }

    public override void executeWork(List<String> paramList) {    	
    	// Enforce max of 1 input parameter
    	if (paramList.size() == 1) {
    		invokeProcess(paramList[0]);
    	} else {
    		throw new CustomException(AsyncQueueableUtils.ERR_INVALID_PARAMS);
    	}
    }

    private void invokeProcess(String param) {
        DF_LAPIPlacesAPIService.searchLocationByLocationId(param);
    }
}