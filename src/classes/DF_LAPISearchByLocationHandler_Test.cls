@isTest
private class DF_LAPISearchByLocationHandler_Test {

    @isTest static void test_executeWork() {            
    	/** Setup data  **/
    	String response;    	
		User commUser;
    	       
    	String locationId = 'LOC000035375038';
    	
		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		Map<String, String> addressMap = new Map<String, String>();

		// Create sfreq
		DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, DF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);		
		insert sfReq;
		
		String param = sfReq.Id;

        List<String> paramList = new List<String>();
        paramList.add(param); 

   		// Generate mock response
		response = DF_IntegrationTestData.buildLocationDistanceAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

		// Set up commUser to run test as
 		commUser = DF_TestData.createDFCommUser();

		system.runAs(commUser) { 
	    	test.startTest(); 	

			DF_LAPISearchByLocationHandler handler = new DF_LAPISearchByLocationHandler();
    		
	        // Invoke Call
	        handler.executeWork(paramList);			
			
	        test.stopTest();     			
		}                                                  

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id 
                           					FROM   DF_SF_Request__c 
                           					WHERE  Opportunity_Bundle__c = :opptyBundle.Id
                           					AND    Status__c = :DF_LAPI_APIServiceUtils.STATUS_COMPLETED];

		system.AssertEquals(false, sfReqList.isEmpty());  
    }

	/** Data Creation **/
    @testSetup static void setup() {
    	try {
    		createCustomSettings();  				        
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }     	
    }
    
    @isTest static void createCustomSettings() {                
        try {
	        Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
			asrConfigSettings.Max_No_of_Future_Calls__c = 50;
			asrConfigSettings.Max_No_of_Parallels__c = 10;
			asrConfigSettings.Max_No_of_Retries__c = 3;
			asrConfigSettings.Max_No_of_Rows__c = 1;			
	        upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }                
    }  
 
}