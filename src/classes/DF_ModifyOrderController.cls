public class DF_ModifyOrderController {
    
    public static String modifyOrderDetails;
    public static DF_Order__c dfOrderObj;
    public static DF_Quote__c dfQuoteObj;
        
    @AuraEnabled
    public static DF_Modify_Order_Attribute_Settings__mdt getDFModifyOrderAttribute(string modifyType) {
        return [SELECT DeveloperName, BTD_Type__c,Cos_High__c,Cos_Low__c,Cos_Medium__c,Id,Interface_Bandwidth__c,Interface_Type__c,
                Mapping_Mode__c,NNI__c,OVC_Type__c,Route_Type__c,Service_Restoration_SLA__c,SVLAN_Id__c,Term__c,
                TPID__c,UNI_VLAN_Id__c,Zone__c, After_Business_Hours__c, MasterLabel 
                from DF_Modify_Order_Attribute_Settings__mdt Where DeveloperName = :modifyType limit 1];
    }

    @AuraEnabled          
    public static void invokeSave(String dfQuoteId, String dfOrderId, String location, String afterBH) {          
        try {   
            DF_SF_Order_ItemController.resetOVFields(dfOrderId);       
            DF_SF_Order_ItemController.processUpdateDFQuoteStatus(dfQuoteId);
            updateDF_OrderNonBillable(dfOrderId, afterBH);
            DF_SF_Order_ItemController.updateOrderJSON(dfOrderId, location);
            invokeOrderValidatorCallout(dfOrderId); 
            //String x = invokeOrderValidatorSynchronousCallout(dfOrderId);                             
        } catch(Exception e) {
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_ModifyOrderController.class.getName(), 'invokeSave', '', '', '', '', e, 0);
            
            throw new AuraHandledException(e.getMessage());         
        }
    }

    @AuraEnabled
    public static void updateDF_OrderNonBillable(String dfOrderId, String afterBH) {
        DF_Order__c orderToUpdate;
        List<DF_Order__c> orderList;
        system.debug('!!!!!!!' + afterBH);
        try {       
            if (String.isNotEmpty(dfOrderId)) {               
                orderList = [SELECT Id, After_Business_Hours__c 
                                        FROM   DF_Order__c
                                        WHERE  Id = :dfOrderId];                                                   
            }

            // Perform update       
            if (!orderList.isEmpty()) {
                orderToUpdate = orderList[0];  

                if (orderToUpdate != null) {
                    orderToUpdate.After_Business_Hours__c = afterBH=='Select'?'':afterBH;                            
                    update orderToUpdate;                 
                }
            }                          
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        } 
    }
    //CPST-7018
    @AuraEnabled
    public static String getCustomSetting(String settingName) {
		String result = '';
		try
		{
			result = DF_CS_API_Util.getCustomSettingValue(settingName);
		}
		catch(Exception ex)
		{
			System.debug(ex);
			System.debug(settingName);
		}
		return result;
	}

    @AuraEnabled
    public static void submitOrder(String dfOrderId){
        
         List<DF_Order__c> dfOrder = [SELECT Id,Order_Status__c, DF_Quote__r.Opportunity__c, BPI_Id__c, Order_Submitted_Date__c FROM DF_Order__c WHERE Id =:dfOrderId];
         if(!dfOrder.isEmpty()){
            DF_OrderSubmitUtility.processDFOrder(dfOrder[0]);
            update dfOrder[0];
            //post order submit, deleting any draft/pending modification related to same BPI Id
            EE_OrderUtility.modifyOrderPostProcessing(dfOrder[0].BPI_Id__c, dfOrder[0].Id);
        }
        
    }

    @AuraEnabled
    public static String invokeOrderValidatorSynchronousCallout(String dfOrderId) {
        String validationResponse;
        try {       
            if (String.isNotEmpty(dfOrderId)) {                         
                validationResponse = DF_OrderValidatorService.validateOrderSyncronous(dfOrderId);                             
            }      
        } catch(Exception e) {
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_ModifyOrderController.class.getName(), 'invokeOrderValidatorSynchronousCallout', '', '', '', '', e, 0);
            throw new AuraHandledException(e.getMessage());         
        }
        return validationResponse;
    }

     @AuraEnabled
    public static void invokeOrderValidatorCallout(String dfOrderId) {
        DF_SF_Order_ItemController.invokeOrderValidatorCallout( dfOrderId);
    }

    
    @AuraEnabled
    public static String getModifyOrderDetails(String bpiId, String modifyType)
    {
      //  String bpiId = 'BPI000000012355';
        if(String.IsNotBlank(bpiId))
        {
            dfOrderObj=EE_OrderUtility.getDfOrder(bpiId, modifyType);
            system.debug('!!!! dfOrderObj '+dfOrderObj);
        
            If(dfOrderObj == null)
                { system.debug('!!!! call mac d dfOrderObj '+dfOrderObj);
                    modifyOrderDetails=EE_OrderUtility.createMACDBasket(bpiId, modifyType);
                }
            else
                {   system.debug('!!!! dfOrderObj fetch exist '+dfOrderObj);
                    dfQuoteObj=EE_OrderUtility.getDfQuote(dfOrderObj.df_quote__c);
                    List<cscfga__Product_Basket__c> basketList = [Select Id, Name, cscfga__User_Session__c, cscfga__Opportunity__c from cscfga__Product_Basket__c where cscfga__Opportunity__c = : dfQuoteObj.Opportunity__c];
                    cscfga__Product_Basket__c basket = !basketList.isEmpty() ? basketList.get(0) : null;
                    cscfga__Product_Configuration__c pConfig= [Select Id, cscfga__Recurring_Charge__c, csordtelcoa__Replaced_Product_Configuration__r.cscfga__Recurring_Charge__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basket.Id and Name = :DF_Constants.DIRECT_FIBRE_PRODUCT_CHARGES_NAME limit 1];
                    if(pConfig.cscfga__Recurring_Charge__c == pconfig.csordtelcoa__Replaced_Product_Configuration__r.cscfga__Recurring_Charge__c) {
                        EE_OrderUtility.updateOldESLAValueAfterMacDCreation(pConfig.Id);
                    }
                    modifyOrderDetails=EE_OrderUtility.getProductData(basket.Id, dfQuoteObj);
                    
                }
        }
        
        return modifyOrderDetails;
    }

    @AuraEnabled
    public static DF_Order__c getCurrentOrder(String bpiId, string modifyType){
        dfOrderObj=EE_OrderUtility.getDfOrder(bpiId, modifyType);
        return dfOrderObj;
    }
    
    @AuraEnabled
    public static Boolean doesCSADefaultToLocal(string cSAId){
        Boolean retVal = false;
        Integer counter = 0;
        if(String.isNotEmpty(CSAId)){
            DF_POI_Route_Type_Value__mdt[] localRoutes = [SELECT POI__c,Route_Type__c FROM DF_POI_Route_Type_Value__mdt WHERE CSA__c =:cSAId];
            if(localRoutes.size() > 1){
                String poiName = localRoutes[0].POI__c;
                for(DF_POI_Route_Type_Value__mdt poi : localRoutes){
                    if(poi.POI__c == poiName && counter>0){
                        retVal = true;
                    }
                    counter++;
                }
            }
        }
        return retVal;
    }

    /*
    * Includes the logic to update Hidden_OVC_Charges as JS rules do not execute in custom UI.
    * This method is executed to set the Hidden OVC charges in the parent configuration which is otherwise set using javascript rules. Javascript rules do not run in CS API calls
    * Parameters : basket id, product configuration id, total recurring charge of all OVCs.
    * @Return : String that says if the execution of functionality was successful. If so, Recuuring charge value in the parent is returned
    */
    @AuraEnabled
    public static String updateHiddenOVCChargesinParent(String basketId,String configId, Decimal totalOVC) {
        return EE_OrderUtility.updateHiddenOVCCharges(basketId, configId, totalOVC);
    }
    
    /*
    * Logic to Add OVC to the basket for Modify Orders.
    * Parameters : basket id, product configuration id and csa
    * @Return : String that says if the execution of functionality was successful
    */
    @AuraEnabled
    public static String addOVC(String basketId,String configId, String csa){
        return EE_OrderUtility.addOVC( basketId, configId,  csa);
    }

    /* Includes the logic to get the product configuration for Product charges and OVC and other DF Quote related information as a serialized string.
    * Parameters : quote id.
    * @Return : String that says if the execution of functionality was successful. If so, a serialized string is returned
    */
    @AuraEnabled
    public static String getExistingQuickQuoteData(String quoteId) {
        return EE_OrderUtility.getExistingQuickQuoteData(quoteId);
    }

    /*
    * Includes the logic to remove OVC from the basket and from Product charges product configuration.
    * Parameters : basket id, product configuration id from which ovc is removed, related product attribute name in parent product definition.
    * @Return : String that says if the execution of functionality was successful
    */
    @AuraEnabled
    public static String removeOVCFromParent(Id basketId, Id configId, String relatedProductAtt) {
        return EE_OrderUtility.removeOVC(basketId, configId, relatedProductAtt);
    }
    
     @AuraEnabled 
    public static String getFullOptionsList(List<String>  attNameList) {

       String optionsResponse = 'Error';
       List<cscfga__Attribute_Definition__c> options = [SELECT Name ,( SELECT cscfga__Value__c, Name 
                                                                        FROM cscfga__Select_Options__r 
                                                                        ORDER BY cscfga__Sequence__c ASC ) 
                                                        FROM cscfga__Attribute_Definition__c 
                                                        WHERE cscfga__Product_Definition__r.Name 
                                                        IN ( 'OVC','Direct Fibre - Product Charges') AND
                                                        Name IN: attNameList ];
                    system.debug('!!! qry result options'+ options);
        //cleanup result - remove any results with no picklist values
       if(options.size() > 0 ){
            Integer j = 0;
            while (j < options.size())
            {
              if(options.get(j).cscfga__Select_Options__r == null){
                options.remove(j);
              }else{
                j++;
              }
            }

            Map<String, List<cscfga__Select_Option__c>> optionsMap = new Map<String, List<cscfga__Select_Option__c>>();
            for(cscfga__Attribute_Definition__c op:options){
                if(op.cscfga__Select_Options__r.size() > 0){
                    optionsMap.put(op.Name, op.cscfga__Select_Options__r);
                }
            }

            system.debug('!!! optionsMap ' +optionsMap);



        
            optionsResponse =  JSON.serialize(optionsMap);
        }
        System.debug('!!! optionsResponse '+ optionsResponse);
        return optionsResponse;

    }   

    @AuraEnabled 
    public static String getOptionsList(String basketId, Boolean pcConfig, List<String> attNameList) {     
        
        return DF_SF_Order_ItemController.getOptionsList( basketId,  pcConfig, attNameList) ;
    }
    @AuraEnabled 
    public static String getNonBillableOptionsValues(String dfOrderId){
        return DF_SF_Order_ItemController.getNonBillableOptionsValues(dfOrderId);
    }

    @AuraEnabled 
    public static String getNonBillableOVCValues(String dfOrderId){
        return DF_SF_Order_ItemController.getNonBillableOVCValues(dfOrderId);
    }

    @AuraEnabled 
    public static String getNonBillableOptionsList(){
        return DF_SF_Order_ItemController.getNonBillableOptionsList();
    }
    //@AuraEnabled
    //public static String updateHiddenOVCCharges(String basketId,String configId, Decimal totalOVC, Decimal totalCoSBw, Decimal totalOVCCosCharges) {
    //    return DF_SF_Order_ItemController.updateHiddenOVCCharges(basketId, configId, totalOVC,totalCoSBw,totalOVCCosCharges);

    //}
        @AuraEnabled
    public static String updateHiddenOVCCharges(String basketId,String configId, Decimal totalOVC, Decimal totalCoSBw, Decimal totalOVCCosCharges) {
        String result;
        if(String.isNotBlank(basketId) && String.isNotBlank(configId) && totalOVC != null){
            cscfga__Product_Basket__c basket = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c where id = :basketId];
            cscfga.API_1.ApiSession apiSession = DF_CS_API_Util.createApiSession(basket);
            apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = configId, cscfga__Product_Basket__c = basketId));        
            cscfga.ProductConfiguration currConfig = apiSession.getConfiguration();


            Id previousProdConfigId =  [SELECT csordtelcoa__Replaced_Product_Configuration__c 
                                        FROM cscfga__Product_Configuration__c 
                                        WHERE Id =: configId][0].csordtelcoa__Replaced_Product_Configuration__c;
                                        system.debug('previousProdConfigId ' + previousProdConfigId);

            String totalOVCCoSChargesNewFromPreviousOrder = [SELECT cscfga__Value__c 
                                                            FROM cscfga__Attribute__c 
                                                            WHERE cscfga__Product_Configuration__c=:previousProdConfigId
                                                            AND Name = 'TotalOVCCoSChargesNew'][0].cscfga__Value__c;
                                                            system.debug('totalOVCCoSChargesNewFromPreviousOrder ' +totalOVCCoSChargesNewFromPreviousOrder);



            
            for(cscfga.Attribute att : currConfig.getAttributes()) {
                //System.debug('Attribute name is : ' + att);
                if(att.getName() == 'Hidden_OVC_Charges'){
                    att.setValue(String.valueOf(totalOVC)); 
                }
                if(att.getName() == 'Hidden_Total_OVC_BWs'){
                    system.debug('bandwith Attribute--'+totalCoSBw);
                    att.setValue(String.valueOf(totalCoSBw));
                }
                if(att.getName() == 'TotalOVCCoSChargesNew'){
                    system.debug('TotalOVCCoSChargesNew-'+totalOVCCosCharges);
                    att.setValue(String.valueOf(totalOVCCosCharges));
                }
                if(att.getName() == 'TotalOVCCoSChargesOld'){
                    system.debug('TotalOVCCoSChargesOld-'+totalOVCCoSChargesNewFromPreviousOrder);
                    att.setValue(String.valueOf(totalOVCCoSChargesNewFromPreviousOrder));
                }
                //rules don't appear to fire for Is_CoS_Downgrade
                if(att.getName()=='Is_CoS_Downgrade'){
                    If(totalOVCCosCharges<decimal.valueOf(totalOVCCoSChargesNewFromPreviousOrder)){
                        att.setValue(String.valueOf(true));
                    }else{
                        att.setValue(String.valueOf(false));
                    }
                }
            }
            apiSession.executeRules(); 
            apiSession.validateConfiguration();
            apiSession.persistConfiguration();
            
 // return the final recurring charge
            String rc;
            String mc;
            for(cscfga.Attribute att : currConfig.getAttributes()) {
                //System.debug('AP Attribute name is : ' + att);
                if(att.getName() == 'Recurring charge'){
                    rc = att.getValue(); 
                }
                else if(att.getName() == 'Hidden_SM_Charge'){
                    mc = att.getValue(); 
                }
            }
            result = '{"rc":"'+rc+'", "mc":"'+mc+'"}';
            system.debug('<><><>'+result);
        }
        else
            result = 'Error';

        return result;   
    }
    @AuraEnabled
    public static void setNonBillableOVCValues(String  dfOrderId, String jsonOVCnonBillable) {
         DF_SF_Order_ItemController.setNonBillableOVCValues(  dfOrderId,  jsonOVCnonBillable);
    }
    @AuraEnabled
    public static void processUpdateDFQuoteStatus(String quoteId) {   
         DF_SF_Order_ItemController.processUpdateDFQuoteStatus(quoteId);
    }
    
    /*
    * Includes the logic to update product information
    * Parameters : basket id, serialized string of configurations and attributes, configuration Id.
    * @Return : String that says if the execution of functionality was successful
    */
    @AuraEnabled
    public static String updateProductInParent(String basketId, String configs, String attributes, String configId) {
        return EE_OrderUtility.updateProduct(basketId, configs, attributes, configId);
    }
    /*
    Get Previous Orders OVC Non Billable JSON and update Id's


    */

    @AuraEnabled
    public static String getPreviousOrderNonBillableOVC(String bpiId ) {
        String prevOVCNonBillable;
        try{
                DF_Order__c prevOrder = [SELECT OVC_NonBillable__c
                                        FROM DF_Order__c 
                                        WHERE BPI_Id__c =:bpiId AND Order_Status__c = 'Complete' 
                                        ORDER BY CreatedDate DESC LIMIT 1 ][0];
                //call generateOVCNonBillableJSON to get the updated OVC systemId 
                prevOVCNonBillable = EE_OrderUtility.generateOVCNonBillableJSON(prevOrder);

            }
            catch(Exception e) {
                GlobalUtility.logMessage(DF_Constants.ERROR, DF_ModifyOrderController.class.getName(), 'invokeOrderValidatorSynchronousCallout', '', '', '', '', e, 0);
                throw new AuraHandledException(e.getMessage());         
            }

            return prevOVCNonBillable;

    }
    
    @AuraEnabled
    public static String getPreviousOrderESLA(String bpiId ) {
        String prevESLA = '';
        try{
                DF_Order__c prevOrder = [SELECT Id, OVC_NonBillable__c , Order_JSON__c, BTD_Id__c, UNI_Id__c, UNI_Port__c, BPI_Id__c 
                                        FROM DF_Order__c 
                                        WHERE BPI_Id__c =:bpiId AND Order_Status__c = 'Complete' 
                                        ORDER BY CreatedDate DESC LIMIT 1 ][0];
                
                prevESLA = DF_ModifyOrderJSONDataHelper.getESLA(prevOrder.Order_JSON__c, prevOrder);

            }
            catch(Exception e) {
                GlobalUtility.logMessage(DF_Constants.ERROR, DF_ModifyOrderController.class.getName(), 'getPreviousOrderESLA', '', '', '', '', e, 0);
                throw new AuraHandledException(e.getMessage());         
            }

            return prevESLA;

    }
	
    //@AuraEnabled
    //public static Decimal getTotalCosRecurring(String orderId, String methodType) {   
    //    Decimal totalCosRec = 0;
    //    string strOVCJSON = '';
        
    //    DF_Order__c newOrd = [Select Id, Previous_Order__c, OVC_NonBillable__c from DF_Order__c Where Id = :orderId];
        
    //    if(newOrd != null) {
    //        if(methodType == 'OnLoad') {
    //            List<DF_Order__c> prevOrd = [select Id, OVC_NonBillable__c from DF_Order__c where id = :newOrd.Previous_Order__c];
    //            if(!prevOrd.isEmpty()) {
    //                totalCosRec = getSumOfOVCChargeFromCS(prevOrd[0].Id);
    //                //totalCosRec = getSumOfOVCCharge(prevOrd[0].OVC_NonBillable__c);
    //            }
    //        }
    //        else if(methodType == 'OnSave') {
    //            //totalCosRec = getSumOfOVCCharge(newOrd.OVC_NonBillable__c);
    //            totalCosRec = getSumOfOVCChargeFromCS(orderId);
    //        }
    //    }        
        
    //    return totalCosRec;         
    //}
    
    //public Static Decimal getSumOfOVCCharge(string ovcJSON) {

    //    Decimal sumOfOVCCharge = 0;
    //    List<DF_OVCWrapper.OVC> ovcData = (List<DF_OVCWrapper.OVC>)Json.deserialize(ovcJSON, List<DF_OVCWrapper.OVC>.class);
    //    system.debug('::::'+ovcData);
    //    if(!ovcData.isEmpty()) {
    //        for(integer count = 0; count < ovcData.size(); count++) {
    //            sumOfOVCCharge += Decimal.valueOf(ovcData[count].coSRecurring);
    //        }
    //    }
    //    return sumOfOVCCharge;
    //} 
    //public Static Decimal getSumOfOVCChargeFromCS(String dfOrderId){

    //    Decimal totalCosChargeForOrder = 0.0;
        
    //    DF_Order__c dfORder = [SELECT DF_Quote__r.Opportunity__c 
    //                           FROM DF_Order__c 
    //                           WHERE Id =: dfOrderId][0];
    //    String OppId = dfORder.DF_Quote__r.Opportunity__c;
    //    system.debug(OppId);
        
    //    String basketId = [SELECT Id 
    //                        FROM cscfga__Product_Basket__c 
    //                        WHERE cscfga__Opportunity__c =:OppId][0].Id;
    //    system.debug(basketId);
        
    //    Set<cscfga__Product_Configuration__c> configSet = new Set<cscfga__Product_Configuration__c> ([SELECT Id 
    //                                                                                                 FROM cscfga__Product_Configuration__c 
    //                                                                                                 WHERE cscfga__Product_Basket__c =:basketId 
    //                                                                                                 AND Name Like 'OVC%']);
    //    system.debug(configSet);
        
    //    List<cscfga__Attribute__c> AttrList = new List<cscfga__Attribute__c>([SELECT cscfga__Value__c 
    //                                                                          FROM cscfga__Attribute__c 
    //                                                                          WHERE cscfga__Product_Configuration__c IN:configSet AND 
    //                                                                          NAME = 'Hidden_TotalCoS_Charges'] );
        
    //    for(cscfga__Attribute__c attrTemp: AttrList){
    //        totalCosChargeForOrder += Decimal.ValueOf(attrTemp.cscfga__Value__c);
    //    }
    //    system.debug(totalCosChargeForOrder);

    //    return totalCosChargeForOrder;
    //}

}