@IsTest
public class DF_ModifyOrderController_Test {

    /**
    * Tests getModifyOrderDetails method when an DF Order doesn't exist
    */
    @isTest static void getModifyOrderDetailsMethodWithNoDFOrderTest()
    {
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        Opportunity oppo = DF_TestService.getOppRecord(acc.Id);
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        basketObj.cscfga__Opportunity__c = oppo.Id;
        update basketObj;
        List<DF_Custom_Options__c> csList = new List<DF_Custom_Options__c>();
        DF_Custom_Options__c cs1 = new DF_Custom_Options__c(name = 'SF_OPPORTUNITY_STAGE', Value__c = 'Closed Won');
        DF_Custom_Options__c cs2 = new DF_Custom_Options__c(name = 'SF_STAGENAME', Value__c = 'New');
        csList.add(cs1);
        csList.add(cs2);
        insert csList;
        csord__Subscription__c sub = DF_TestService.createCSSubscriptionRecord(acc.Id, oppo);
        
        List<string> attList = new List<String>();
        attList.add('Test');
        
        Test.startTest();
        String result = DF_ModifyOrderController.getModifyOrderDetails('BPI000000012911', '');
        cscfga__Attribute_Definition__c objCsAtt = DF_TestData.buildAttributeDefinition('Test', [select id from cscfga__Product_Definition__c Where Name = 'OVC' limit 1].Id);
        insert objCsAtt;
        DF_ModifyOrderController.getFullOptionsList(attList);
        Test.stopTest();
    }
    
    /**
    * Tests updateHiddenOVCChargesinParent method
    */
    @isTest static void updateHiddenOVCChargesinParentMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        List<cscfga__Product_Configuration__c> pcConfigList = [Select id,cscfga__Configuration_Offer__r.cscfga__Template__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketObj.Id and name = 'Direct Fibre - Product Charges'];
        cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
        String configId = configObj != null ? configObj.Id : null;
        Test.startTest();
        String result = DF_ModifyOrderController.updateHiddenOVCChargesinParent(basketObj.Id, configId, 353.40);
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
    * Tests Modify Order Lightning methods test 
    */
    @isTest static void modifyOrderAuraMethodTest()
    {    
        List<DF_Custom_Options__c> csList = new List<DF_Custom_Options__c>();
        DF_Custom_Options__c cs1 = new DF_Custom_Options__c(name = 'SF_OPPORTUNITY_STAGE', Value__c = 'Closed Won');
        DF_Custom_Options__c cs2 = new DF_Custom_Options__c(name = 'SF_STAGENAME', Value__c = 'New');
        csList.add(cs1);
        csList.add(cs2);
        insert csList;
        
        // Create OpptyBundle
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acc.ID);
        insert opptyBundle;
        
        Opportunity oppty = DF_TestData.createOpportunity('Test Opp');
        oppty.Opportunity_Bundle__c = opptyBundle.id;
        insert oppty;
                    
        csord__Subscription__c subs = DF_TestService.createCSSubscriptionRecord(acc.Id, oppty);
        
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        basketObj.cscfga__Opportunity__c = oppty.Id;
        update basketObj;
        
        List<cscfga__Product_Configuration__c> prodConfigList = DF_CS_API_Util.getProductBasketConfigs(basketObj.Id);
        prodConfigList[0].csordtelcoa__Replaced_Subscription__c = subs.Id;
        update prodConfigList[0];
        
        DF_Opportunity_Bundle__c opptyBundle2 = DF_TestData.createOpportunityBundle(acc.ID);
        insert opptyBundle2;
        
        Opportunity oppty2 = DF_TestData.createOpportunity('Test Opp2');
        oppty2.Opportunity_Bundle__c = opptyBundle2.id;
        insert oppty2;
                    
        //csord__Subscription__c subs2 = DF_TestService.createCSSubscriptionRecord(acc.Id, oppty2);
        
        cscfga__Product_Basket__c basketObj2 = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        basketObj2.cscfga__Opportunity__c = oppty2.Id;
        update basketObj2;
        List<cscfga__Product_Configuration__c> prodConfigList2 = DF_CS_API_Util.getProductBasketConfigs(basketObj2.Id);
        //prodConfigList2[0].csordtelcoa__Replaced_Subscription__c = subs2.Id;
        prodConfigList2[0].csordtelcoa__Replaced_Product_Configuration__c = prodConfigList[0].Id;
        update prodConfigList2[0];
        
        csord__Service__c service = new csord__Service__c();
        service.Name = 'OVC 1';
        service.csordtelcoa__Product_Basket__c = basketObj.Id;
        service.csord__Subscription__c = subs.Id;
        //service.csord__Order__c = csOrd.Id;
        service.csord__Identification__c = 'xxx';
        service.csordtelcoa__Product_Configuration__c = prodConfigList[0].Id;
        insert service;
        
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        
        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');         
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;       
          
        DF_Order__c testOrder = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle.Id );
        testOrder.Heritage_Site__c = 'Yes';
        testOrder.Induction_Required__c = 'Yes';
        testOrder.Security_Required__c = 'Yes';
        testOrder.Site_Notes__c = 'Site Notes';
        testOrder.BPI_Id__c = 'BPI123123123123';
        testOrder.Order_Status__c = 'Complete';
        testOrder.Order_Json__c = DF_IntegrationTestData.buildDFOrderJSONString();
        testOrder.Trading_Name__c = 'Trader Joes TEST SITE';
        testOrder.Order_Notifier_Response_JSON__c = '{"tradingName":null,"securityClearance":null,"productOrderComprisedOf":{"referencesProductOrderItem":[{"referencesProductOrderItem":[{"itemInvolvesProduct":{"type":"UNIE","transientId":"1","tpId":"0x8100","portId":"3","ovcType":"Access EVPL","interfaceType":"Optical (Single mode)","interfaceBandwidth":"1Gbps","id":"UNI900000002940"},"action":"NO CHANGE"}],"itemInvolvesProduct":{"uniVLanId":null,"type":"BTD","sVLanId":null,"routeType":null,"powerSupply2":"DC(-48V to -72V)","powerSupply1":"DC(-48V to -72V)","poi":null,"ovcTransientId":null,"nni":null,"maximumFrameSize":null,"id":"SWBTD0000221","cosMediumBandwidth":null,"cosMappingMode":null,"cosLowBandwidth":null,"cosHighBandwidth":null,"connectedTo":null,"btdType":"Standard","btdMounting":"Wall"},"action":"NO CHANGE"},{"referencesProductOrderItem":null,"itemInvolvesProduct":{"uniVLanId":"2020","type":"OVC","sVLanId":"1010","routeType":"State","powerSupply2":null,"powerSupply1":null,"poi":"4NTF - NSW MEDIUM POI","ovcTransientId":"OVC 1","nni":"NNI000022017828","maximumFrameSize":"Jumbo (9000 Bytes)","id":"OVC000000003174","cosMediumBandwidth":"","cosMappingMode":"","cosLowBandwidth":"","cosHighBandwidth":"100Mbps","connectedTo":null,"btdType":null,"btdMounting":null},"action":"MODIFY"}],"itemInvolvesProduct":{"zone":"Zone 1","term":"12 months","templateVersion":"1.0","templateId":"TPL","serviceRestorationSLA":"Premium - 12 (24/7)","productType":"EEAS","id":"BPI000000002197","accessAvailabilityTarget":null},"itemInvolvesLocation":{"id":"LOC000016828316","enterpriseEthernetServiceLevelRegion":"Metropolitan"},"action":"NO CHANGE"},"plannedCompletionDate":"2019-02-07T00:00:00Z","orderType":"Modify","notes":null,"interactionStatus":"InProgress","interactionDateComplete":null,"inductionRequired":null,"id":"BCO000000002871","heritageSite":null,"customerRequiredDate":null,"afterHoursAppointment":null,"accessSeekerInteraction":{"billingAccountID":"BAN000000001018"}}';
        testOrder.OVC_NonBillable__c = '[{"OVCId":"'+prodConfigList[0].Id+'","CSA":"CSA180000002222","NNIGroupId":"NNI090000015859","routeType":"Local","coSHighBandwidth":"50","coSMediumBandwidth":"150","coSLowBandwidth":"60","routeRecurring":"0.00","coSRecurring":"0.00","ovcMaxFrameSize":"Jumbo (9000 Bytes)","status":"Incomplete","sTag":"0","ceVlanId":"3421","mappingMode":"DSCP"}]';
        Insert testOrder;
        
        DF_Order__c testOrder1 = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle.Id );
        testOrder1.Heritage_Site__c = 'Yes';
        testOrder1.Induction_Required__c = 'Yes';
        testOrder1.Security_Required__c = 'Yes';
        testOrder1.Site_Notes__c = 'Site Notes';
        testOrder1.BPI_Id__c = 'BPI123123123123';
        testOrder1.Trading_Name__c = 'Trader Joes TEST SITE';
        testOrder1.Previous_Order__c = testOrder.Id;
        testOrder1.OrderType__c = 'Modify';
        testOrder1.Order_Notifier_Response_JSON__c = '{"tradingName":null,"securityClearance":null,"productOrderComprisedOf":{"referencesProductOrderItem":[{"referencesProductOrderItem":[{"itemInvolvesProduct":{"type":"UNIE","transientId":"1","tpId":"0x8100","portId":"3","ovcType":"Access EVPL","interfaceType":"Optical (Single mode)","interfaceBandwidth":"1Gbps","id":"UNI900000002940"},"action":"NO CHANGE"}],"itemInvolvesProduct":{"uniVLanId":null,"type":"BTD","sVLanId":null,"routeType":null,"powerSupply2":"DC(-48V to -72V)","powerSupply1":"DC(-48V to -72V)","poi":null,"ovcTransientId":null,"nni":null,"maximumFrameSize":null,"id":"SWBTD0000221","cosMediumBandwidth":null,"cosMappingMode":null,"cosLowBandwidth":null,"cosHighBandwidth":null,"connectedTo":null,"btdType":"Standard","btdMounting":"Wall"},"action":"NO CHANGE"},{"referencesProductOrderItem":null,"itemInvolvesProduct":{"uniVLanId":"2020","type":"OVC","sVLanId":"1010","routeType":"State","powerSupply2":null,"powerSupply1":null,"poi":"4NTF - NSW MEDIUM POI","ovcTransientId":"OVC 1","nni":"NNI000022017828","maximumFrameSize":"Jumbo (9000 Bytes)","id":"OVC000000003174","cosMediumBandwidth":"","cosMappingMode":"","cosLowBandwidth":"","cosHighBandwidth":"100Mbps","connectedTo":null,"btdType":null,"btdMounting":null},"action":"MODIFY"}],"itemInvolvesProduct":{"zone":"Zone 1","term":"12 months","templateVersion":"1.0","templateId":"TPL","serviceRestorationSLA":"Premium - 12 (24/7)","productType":"EEAS","id":"BPI000000002197","accessAvailabilityTarget":null},"itemInvolvesLocation":{"id":"LOC000016828316","enterpriseEthernetServiceLevelRegion":"Metropolitan"},"action":"NO CHANGE"},"plannedCompletionDate":"2019-02-07T00:00:00Z","orderType":"Modify","notes":null,"interactionStatus":"InProgress","interactionDateComplete":null,"inductionRequired":null,"id":"BCO000000002871","heritageSite":null,"customerRequiredDate":null,"afterHoursAppointment":null,"accessSeekerInteraction":{"billingAccountID":"BAN000000001018"}}';
        testOrder1.Order_Json__c = DF_IntegrationTestData.buildDFOrderJSONString();
        testOrder1.Order_SubType__c = 'Add new OVC';
        string ovcNonBill = '[{"OVCId":"'+prodConfigList2[0].Id+'","CSA":"CSA180000002222","NNIGroupId":"NNI090000015859","routeType":"Local","coSHighBandwidth":"50","coSMediumBandwidth":"100","coSLowBandwidth":"60","routeRecurring":"0.00","coSRecurring":"0.00","ovcMaxFrameSize":"Jumbo (9000 Bytes)","status":"Incomplete","sTag":"0","ceVlanId":"3421","mappingMode":"DSCP"}]';
        testOrder1.OVC_NonBillable__c = ovcNonBill;
        
        Insert testOrder1;

        //creating a draft modify order
        DF_Quote__c dfQuoteDraft1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');         
        dfQuoteDraft1.Proceed_to_Pricing__c = true;
        
        insert dfQuoteDraft1;
        
        DF_Order__c testDraftOrder1 = new DF_Order__c(DF_Quote__c = dfQuoteDraft1.Id, Opportunity_Bundle__c = opptyBundle.Id );
        testDraftOrder1.Heritage_Site__c = 'Yes';
        testDraftOrder1.Induction_Required__c = 'Yes';
        testDraftOrder1.Security_Required__c = 'Yes';
        testDraftOrder1.Site_Notes__c = 'Site Notes';
        testDraftOrder1.Trading_Name__c = 'Trader Joes TEST SITE';
        testDraftOrder1.Previous_Order__c = testOrder.Id;
        testDraftOrder1.OrderType__c = 'Modify';
        testDraftOrder1.BPI_Id__c = 'BPI123123123123';
        testDraftOrder1.Order_Status__c = 'In Draft';
        testDraftOrder1.Order_Json__c = DF_IntegrationTestData.buildDFOrderJSONString();
        testDraftOrder1.Order_SubType__c = 'Change UNI VLAN ID';
        string ovcNonBillDraft = '[{"OVCId":"'+prodConfigList2[0].Id+'","CSA":"CSA180000002222","NNIGroupId":"NNI090000015859","routeType":"Local","coSHighBandwidth":"50","coSMediumBandwidth":"100","coSLowBandwidth":"60","routeRecurring":"0.00","coSRecurring":"0.00","ovcMaxFrameSize":"Jumbo (9000 Bytes)","status":"Incomplete","sTag":"0","ceVlanId":"3421","mappingMode":"DSCP"}]';
        testDraftOrder1.OVC_NonBillable__c = ovcNonBillDraft;
        testDraftOrder1.Order_Notifier_Response_JSON__c = '{"tradingName":null,"securityClearance":null,"productOrderComprisedOf":{"referencesProductOrderItem":[{"referencesProductOrderItem":[{"itemInvolvesProduct":{"type":"UNIE","transientId":"1","tpId":"0x8100","portId":"3","ovcType":"Access EVPL","interfaceType":"Optical (Single mode)","interfaceBandwidth":"1Gbps","id":"UNI900000002940"},"action":"NO CHANGE"}],"itemInvolvesProduct":{"uniVLanId":null,"type":"BTD","sVLanId":null,"routeType":null,"powerSupply2":"DC(-48V to -72V)","powerSupply1":"DC(-48V to -72V)","poi":null,"ovcTransientId":null,"nni":null,"maximumFrameSize":null,"id":"SWBTD0000221","cosMediumBandwidth":null,"cosMappingMode":null,"cosLowBandwidth":null,"cosHighBandwidth":null,"connectedTo":null,"btdType":"Standard","btdMounting":"Wall"},"action":"NO CHANGE"},{"referencesProductOrderItem":null,"itemInvolvesProduct":{"uniVLanId":"2020","type":"OVC","sVLanId":"1010","routeType":"State","powerSupply2":null,"powerSupply1":null,"poi":"4NTF - NSW MEDIUM POI","ovcTransientId":"OVC 1","nni":"NNI000022017828","maximumFrameSize":"Jumbo (9000 Bytes)","id":"OVC000000003174","cosMediumBandwidth":"","cosMappingMode":"","cosLowBandwidth":"","cosHighBandwidth":"100Mbps","connectedTo":null,"btdType":null,"btdMounting":null},"action":"MODIFY"}],"itemInvolvesProduct":{"zone":"Zone 1","term":"12 months","templateVersion":"1.0","templateId":"TPL","serviceRestorationSLA":"Premium - 12 (24/7)","productType":"EEAS","id":"BPI000000002197","accessAvailabilityTarget":null},"itemInvolvesLocation":{"id":"LOC000016828316","enterpriseEthernetServiceLevelRegion":"Metropolitan"},"action":"NO CHANGE"},"plannedCompletionDate":"2019-02-07T00:00:00Z","orderType":"Modify","notes":null,"interactionStatus":"InProgress","interactionDateComplete":null,"inductionRequired":null,"id":"BCO000000002871","heritageSite":null,"customerRequiredDate":null,"afterHoursAppointment":null,"accessSeekerInteraction":{"billingAccountID":"BAN000000001018"}}';
        Insert testDraftOrder1;
        
        string location = '{"status":"Order Draft","SCR":"1297.0000","SCNR":"5000.0000","quoteId":"EEQ-0000000324","orderId":"a7EN000000092gQMAQ","locId":"LOC000006231845","id":"a6aN00000000k5iIAA","FBC":0,"address":"6 LENNOX ST RAVENSWOOD TAS 7250 Australia","OrderId":"a7EN000000092gQMAQ","basketId":"a1eN0000000z2amIAA","UNI":[{"BTDType":"Standard (CSAS)","interfaceBandwidth":"1GB","AAT":"","SLA":"Premium - 12 (24/7)","term":"12","zone":"Zone 3","AHA":"No","coSRecurring":"0","oVCType":"Access EVPL","interfaceTypes":"Copper (Auto-negotiate)","tPID":"0x8100","Id":"a1hN0000001TOJTIA4"}],"OVCs":[{"OVCId":"a1hN0000001TOJUIA4","OVCName":"OVC 1","CSA":"CSA180000002222","monthlyCombinedCharges":"497.0000","routeType":"Local","coSHighBandwidth":"20","coSMediumBandwidth":"10","coSLowBandwidth":"0","routeRecurring":"0.0000","coSRecurring":"497.0000","POI":"1-MEL-MELBOURNE-MP","status":"Incomplete","mappingMode":"DSCP","NNIGroupId":"NNI090000015859","sTag":"0","ceVlanId":"12","ovcMaxFrameSize":"Jumbo (9000 Bytes)"}],"prodChargeConfigId":"a1hN0000001TOJTIA4","ovcConfigId":"a1hN0000001TOJUIA4","businessContactList":[{"busContSurname":"asf","busContSuburb":"sdf","busContStreetAddress":"asf","busContState":"VIC","busContRole":"asdf","busContPostcode":"2000","busContNumber":"0412123123","busContId":"003N000001DhSVkIAN","busContFirstName":"asd","busContEmail":"asdf@asd.com","busContIndex":1}],"siteContactList":[{"siteContSurname":"asdf","siteContNumber":"0412123123","siteContId":"003N000001DhSWsIAN","siteContFirstName":"asd","siteContEmail":"asdf@asdf.com","siteContIndex":1}],"SiteDetails":{"Trading Name":[""],"Heritage Site":["Yes"],"Induction Required":["Yes"],"Security Required":["Yes"],"Site Notes":[""]}}';

        Test.startTest();
        DF_IntegrationUtils.stripJsonNulls(location);
        DF_ModifyOrderController.invokeSave(dfQuoteList[0].id, testOrder1.Id, location, 'Yes'); //CPST-5509
        DF_ModifyOrderController.submitOrder(testOrder1.Id);
        DF_ModifyOrderController.invokeOrderValidatorSynchronousCallout(testOrder1.Id);
        //DF_ModifyOrderController.getTotalCosRecurring(testOrder1.Id, 'OnLoad');
        //DF_ModifyOrderController.getTotalCosRecurring(testOrder1.Id, 'OnSave');
        DF_ModifyOrderController.getCurrentOrder('BPI123123123123','Add new OVC');
        DF_ModifyOrderController.getNonBillableOVCValues(testOrder1.Id);
        DF_ModifyOrderController.getNonBillableOptionsValues(testOrder1.Id);
        
        DF_ModifyOrderController.getNonBillableOptionsList();
        DF_ModifyOrderController.processUpdateDFQuoteStatus(dfQuoteList[0].id);
        DF_ModifyOrderController.setNonBillableOVCValues(testOrder1.Id, ovcNonBill);

        DF_ModifyOrderController.getPreviousOrderNonBillableOVC('BPI123123123123');
        
        DF_ModifyOrderController.getPreviousOrderESLA('BPI123123123123');
        
        List<string> attList = new List<String>();
        attList.add('Test');
        DF_ModifyOrderController.getOptionsList(basketObj2.Id, true, attList);
        DF_ModifyOrderController.updateHiddenOVCCharges(basketObj2.Id, prodConfigList2[0].Id, 0.0, 0.0, 0.0);  
        
        DF_ModifyOrderController.doesCSADefaultToLocal('CSA200000000153');
        
        DF_ModifyOrderController.getExistingQuickQuoteData(dfQuoteList[0].Id);
              
        DF_ModifyOrderController.addOVC(basketObj2.Id,prodConfigList2[0].Id,'CSA200000000153');      
        Test.stopTest();
    }
    
    /**
    * Tests updateHiddenOVCChargesinParent method error condition
    */
    @isTest static void updateHiddenOVCChargesinParentMethodErrorTest()
    {
        Test.startTest();
        String result = DF_ModifyOrderController.updateHiddenOVCChargesinParent('', '', 0.00);
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
    /**
    * Tests getOrderModifyMetadataTest to get the configsettings for modifyOrder layout fields
    */
    @isTest static void getOrderModifyMetadataTest()
    {
        Test.startTest();
        DF_Modify_Order_Attribute_Settings__mdt modifyOrderSetting = DF_ModifyOrderController.getDFModifyOrderAttribute('Add_new_OVC');
        Test.stopTest();
    }

    @isTest static void getCustomSetting()
    {
        Test.startTest();
        String toggleAfterBH = DF_ModifyOrderController.getCustomSetting('TOGGLE_AFTERBH_ON');
        Test.stopTest();
    }
    
    /**
    * Tests updateProductInParent method
    */
    @isTest static void updateProductInParentMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Map<Id, cscfga__Product_Configuration__c> configs1 = new Map<Id, cscfga__Product_Configuration__c>([Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketObj.Id]);
        List<Id> configIdList = new List<Id>(configs1.keySet());
        Map<String,Object> configResponse = cscfga.API_1.getProductConfigurations(new List<Id>(configs1.keySet()));
        Map<String, cscfga__Product_Configuration__c> cfgsOVC = new Map<String, cscfga__Product_Configuration__c>();
        Map<String, Object> attsOVC = new Map<String, Object>();
        for(String o : configResponse.keySet()){
            if(o.contains('-attributes'))
            {
                attsOVC.put(o, configResponse.get(o));
            }
            else
                cfgsOVC.put(o, (cscfga__Product_Configuration__c)configResponse.get(o));
        }

        String jsonStr = json.serialize(attsOVC);
        String jsonStr1 = json.serialize(cfgsOVC);
        
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        Test.startTest();
        String result = DF_ModifyOrderController.updateProductInParent(basketObj.Id, jsonStr1, jsonStr, configIdList.get(0));
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
    * Tests updateProductInParent method error condition
    */
    @isTest static void updateProductInParentMethodErrorTest()
    {
        Test.startTest();
        String result = DF_ModifyOrderController.updateProductInParent('', '', '','');
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
    /**
    * Tests removeOVCFromParent method
    */
    @isTest static void removeOVCFromParentMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        
        List<cscfga__Product_Configuration__c> pcConfigList = [Select id,cscfga__Configuration_Offer__r.cscfga__Template__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basket.Id and name = 'OVC'];
        cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
        String configId = configObj != null ? configObj.Id : null;
        Test.startTest();
        String result = DF_ModifyOrderController.removeOVCFromParent(basket.Id, configId, 'OVC');
        System.assertNotEquals('Error', result);
        Test.stopTest();
    }
}