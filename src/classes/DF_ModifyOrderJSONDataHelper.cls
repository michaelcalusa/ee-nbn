public with sharing class DF_ModifyOrderJSONDataHelper {
 public static String generateJSONStringForOrder(String  dfOrderId, String location) {
        String jsonRetVal = '';
        string csaId = '';
        String jOrdOVC = '';
        String jPrevOrdOVC = '';
        Boolean chkModify = false;
        Set<Id> connectOpportunityIds= new Set<Id>();
 try{
        DF_Order__c objObject = new DF_Order__c();  
        DF_Order__c order;
        cscfga__Product_Configuration__c prodConfig;
        
        List<DF_Order__c> orderList;
        if (String.isNotEmpty(dfOrderId)) {               
            orderList = [SELECT Id, Trading_Name__c, Heritage_Site__c,Induction_Required__c, Security_Required__c,Site_Notes__c, 
                        Customer_Required_Date__c,NTD_Mounting__c,Power_Supply_1__c,Power_Supply_2__c,Installation_Notes__c,
                        DF_Quote__c, Modify_Order_JSON__c, Order_JSON__c, Previous_Order__c, OVC_NonBillable__c,
                        Order_SubType__c, After_Business_Hours__c 
                        FROM DF_Order__c
                        WHERE  Id = :dfOrderId];                                                   
        }
        if (!orderList.isEmpty()) {
            order = orderList[0];
            system.debug('!!!!' + order);
        }
        DF_Order__c prevOrder = [SELECT Id, OVC_NonBillable__c , Order_JSON__c, BTD_Id__c, UNI_Id__c, UNI_Port__c, BPI_Id__c FROM DF_Order__c WHERE Id = :order.Previous_Order__c LIMIT 1];
        DF_Quote__c quote = [SELECT Quote_Name__c, Opportunity_Bundle__c,Opportunity__c,LAPI_Response__c, Location_Id__c FROM DF_Quote__c WHERE Id =: order.DF_Quote__c LIMIT 1];
        Id modifyOpty = quote.Opportunity__c;
        DF_Opportunity_Bundle__c opBundle = [SELECT Name, Account__c FROM DF_Opportunity_Bundle__c WHERE Id =: quote.Opportunity_Bundle__c LIMIT 1 ];
        Account account = [SELECT Billing_ID__c FROM Account WHERE Id =: opBundle.Account__c LIMIT 1]; 
        
        for(DF_Quote__c quoteObj:[Select id,QuoteType__c,Opportunity__c from DF_Quote__c where Opportunity_Bundle__c=:opBundle.id]){
            if(quoteObj.Opportunity__c != null && string.isNotBlank(quoteObj.QuoteType__c) && quoteObj.QuoteType__c.equalsIgnoreCase(DF_Constants.ORDER_TYPE_CONNECT)){
                connectOpportunityIds.add(quoteObj.Opportunity__c);
            }
        }
        csord__Order__c csOrderConnect = [Select id from csord__Order__c where csordtelcoa__Opportunity__c=:connectOpportunityIds and name=:DF_Constants.DIRECT_FIBRE_PRODUCT_CHARGES_NAME limit 1];
        system.debug('quote.Opportunity__c --- '+quote.Opportunity__c);
        cscfga__Product_Configuration__c modifyProductConfig;
        csord__Service__c connectService;
        cscfga__Product_Basket__c modifyBasket = [Select id from cscfga__Product_Basket__c where cscfga__Opportunity__c=:quote.Opportunity__c];
        if(modifyBasket !=null){
           modifyProductConfig  = [Select id,cscfga__Recurring_Charge__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c=:modifyBasket.id
                                                                    and Name=:DF_Constants.DIRECT_FIBRE_PRODUCT_CHARGES_NAME];
        }
        if(!Test.IsRunningTest()){
            connectService = [Select id,csord__Total_Recurring_Charges__c,csord__Order__c from csord__Service__c
                                             where csord__Order__c=:csOrderConnect.id and name=:DF_Constants.DIRECT_FIBRE_PRODUCT_CHARGES_NAME limit 1];
        }
        jOrdOVC = '{"OVC":' + order.OVC_NonBillable__c + '}';
        jPrevOrdOVC = '{"OVC":' + prevOrder.OVC_NonBillable__c + '}';
        
    
        System.debug('!!!!jOrdOVC ' + jOrdOVC );
        System.debug('!!!!jPrevOrdOVC ' + jPrevOrdOVC );
        //get the csaId   
        if(!string.isEmpty(quote.LAPI_Response__c)) {             
            DF_ServiceFeasibilityResponse df_sfJson = (DF_ServiceFeasibilityResponse)Json.deserialize(quote.LAPI_Response__c, DF_ServiceFeasibilityResponse.class);
            if(df_sfJson != null) {
                csaId = df_sfJson.csaId;
            }   
        }
        system.debug('::csaId::'+csaId); 
        System.debug('!!!!' + JSON.serializePretty(location));
        Map<String, Object> orderJSON = (Map<String, Object>) JSON.deserializeUntyped(location);
        System.debug('!!!!' + orderJSON);
        

        List<Object> uniList = (List<Object>)orderJSON.get('UNI');
        system.debug('!!! uniList'+uniList);
        Map<String, Object> uni = (Map<String, Object>)uniList[0]; //only one UNI

        List<Object> ovcList = (List<Object>)orderJSON.get('OVCs');
        system.debug('!!! ovcList'+ovcList);


        DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot productOrderRoot = new DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot();

        //get acces seeker
        DF_OrderDataJSONWrapper.AccessSeekerInteraction accessSeeker = new DF_OrderDataJSONWrapper.AccessSeekerInteraction();
            accessSeeker.billingAccountID = account.Billing_ID__c;

        DF_OrderDataJSONWrapper.ProductOrder productOrder = new DF_OrderDataJSONWrapper.ProductOrder();

        productOrderRoot.productOrder = productOrder;
        //added for appian payload
        if(quote.Quote_Name__c != null) {
            productOrder.associatedReferenceId = quote.Quote_Name__c;
        }

        productOrder.accessSeekerInteraction = accessSeeker;
        productOrder.orderType = 'Modify';
		productOrder.afterHoursProcessing = order.After_Business_Hours__c;  //CPST-5509
        productOrder.csaId = csaId;

        //generate product order comprised of
        DF_OrderDataJSONWrapper.ProductOrderComprisedOf productOrderComprisedOf = new DF_OrderDataJSONWrapper.ProductOrderComprisedOf();
        if(order.Order_SubType__c == 'Change Service Restoration SLA') {
            productOrderComprisedOf.action = 'MODIFY';
        }
        else {
            productOrderComprisedOf.action = 'NO CHANGE';
        }
        
        system.debug('!!! uni'+uni);
        system.debug('!!! uniSLA'+(String)uni.get('SLA'));
        //itemInvolvesLocation
        DF_OrderDataJSONWrapper.ItemInvolvesLocation itemInvolvesLocation = new DF_OrderDataJSONWrapper.ItemInvolvesLocation();
        itemInvolvesLocation.id = (String)orderJSON.get('locId');
        productOrderComprisedOf.itemInvolvesLocation = itemInvolvesLocation;
        //itemInvolvesProductL1 Wors
        DF_OrderDataJSONWrapper.ItemInvolvesProductL1 itemInvolvesProductL1 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL1();
        //if(ProdConfig != null) {
        itemInvolvesProductL1.productType = 'EEAS';
        itemInvolvesProductL1.templateId = 'TPL';
        itemInvolvesProductL1.templateVersion = '1.0';
        itemInvolvesProductL1.id = prevOrder.BPI_Id__c;
        string prevSLA = getESLA(prevOrder.Order_JSON__c, prevOrder);
        if((String)uni.get('SLA') != null) {
            if(order.Order_SubType__c == 'Change Service Restoration SLA' && prevSLA != (String)uni.get('SLA'))
                itemInvolvesProductL1.serviceRestorationSLA = (String)uni.get('SLA');
        }          
        //}
        productOrderComprisedOf.itemInvolvesProduct = itemInvolvesProductL1;

        List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1> referencesProductOrderItemListL1 = new List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1>();
        //referencesProductOrderItemL1
        DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 referencesProductOrderItemL1 = new DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1();
        referencesProductOrderItemL1.action = 'NO CHANGE';
        
        //Add BTD 
        DF_OrderDataJSONWrapper.ItemInvolvesProductL2 itemInvolvesProductL2 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL2();
         // NTD / BTD fields
        itemInvolvesProductL2.type = 'BTD';
        itemInvolvesProductL2.id = prevOrder.BTD_Id__c;

        referencesProductOrderItemL1.itemInvolvesProduct = itemInvolvesProductL2;
        referencesProductOrderItemListL1.Add(referencesProductOrderItemL1);

        List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1>  referencesProductOrderItemL1List = new List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1> ();
        

        DF_OVCWrapper jOrdResp = DF_OVCWrapper.parse(jOrdOVC);   
                //get current OVC Ids
                Set<String> currentOrderOVCIds = new Set<String>();
        system.debug('jOrdResp.OVC'+jOrdResp);        
        for(DF_OVCWrapper.OVC currentOrdOVC : jOrdResp.OVC){
            currentOrderOVCIds.add(currentOrdOVC.OVCId);
        } 
        DF_OVCWrapper jPrevOrdResp = DF_OVCWrapper.parse(jPrevOrdOVC);
        Set<String> PrevOrderOVCIds = new Set<String>();
        Map<String,DF_OVCWrapper.OVC> oldOVCData = new  map<string,DF_OVCWrapper.OVC>();
        //get Previous OVC Ids
        for(DF_OVCWrapper.OVC PrevOrdOVC : jPrevOrdResp.OVC){
            PrevOrderOVCIds.add(PrevOrdOVC.OVCId);
            oldOVCData.put(PrevOrdOVC.OVCId,PrevOrdOVC );
        }
        
        system.debug('PrevOrderOVCIds '+PrevOrderOVCIds);
        system.debug('oldOVCData '+oldOVCData);

        //create new map current prodcinfigId to old Prodconfig id
        Map<String,String> newConfigIdToOldConfigId = new Map<String,String>();

        for(cscfga__Product_Configuration__c pc : [SELECT Id,   csordtelcoa__Replaced_Product_Configuration__c FROM cscfga__Product_Configuration__c WHERE Id IN:currentOrderOVCIds]){
            newConfigIdToOldConfigId.put(pc.Id, pc.csordtelcoa__Replaced_Product_Configuration__c);
        } 
        system.debug('newConfigIdToOldConfigId '+ newConfigIdToOldConfigId);

        //create Previous OVC prod config Map
        Map<ID,cscfga__Product_Configuration__c> ProdConfigMap = new Map<ID,cscfga__Product_Configuration__c>([SELECT Id, Name, csordtelcoa__Replaced_Service__r.OVC_Id__c, csordtelcoa__Replaced_Subscription__r.UNI_Id__c,
                          csordtelcoa__Replaced_Subscription__r.BPI_Id__c  FROM cscfga__Product_Configuration__c WHERE ID IN: PrevOrderOVCIds]);
        system.debug('ProdConfigMap '+ ProdConfigMap);
        //need to get the ovc_id__c from service 
        Map<String,csord__Service__c> configToServiceMap = new Map<String,csord__Service__c>();

        for(csord__Service__c serv : [SELECT csordtelcoa__Product_Configuration__c, OVC_Id__c, Name FROM csord__Service__c WHERE csordtelcoa__Product_Configuration__c IN:PrevOrderOVCIds]){
            configToServiceMap.put(serv.csordtelcoa__Product_Configuration__c, serv);
        } 
        system.debug('configToServiceMap '+ configToServiceMap);

        
        
        system.debug('jOrdResp.OVC '+ jOrdResp.OVC);
        system.debug('!!!!!!jPrevOrdResp.OVC '+jPrevOrdResp.OVC);
        for(DF_OVCWrapper.OVC OrdOVC : jOrdResp.OVC){
            DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 referencesProductOrderItemL1ovc = new DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1();
            DF_OrderDataJSONWrapper.ItemInvolvesProductL2 prodL2 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL2();//OVCs
            prodL2.type = 'OVC' ;

  
                ProdConfig = ProdConfigMap.get(newConfigIdToOldConfigId.get(OrdOVC.OVCId));
                system.debug('!!!!!!ProdConfig' +ProdConfig );
                 system.debug('!!!!!!OrdOVC' +OrdOVC );
                 //system.debug('!!!!!!PrevOrdOVC' +PrevOrdOVC );
                 DF_OVCWrapper.OVC PrevOrdOVC = oldOVCData.get(newConfigIdToOldConfigId.get(OrdOVC.OVCId));
                 system.debug('!!!!!!PrevOrdOVC' +PrevOrdOVC );

                if(ProdConfig != null && PrevOrdOVC != null && !order.Order_SubType__c.equalsIgnoreCase(DF_Constants.UNI_VLAN_ID_MODIFY_SUBTYPE)) {
                    system.debug('ProdConfig.Id ' +ProdConfig.Id);
                    system.debug('PrevOrdOVC.OVCId ' +PrevOrdOVC.OVCId);
                    if(ProdConfig.Id==PrevOrdOVC.OVCId){

                        if(OrdOVC.coSHighBandwidth != PrevOrdOVC.coSHighBandwidth){
                            //prodL2.cosHighBandwidth = OrdOVC.coSHighBandwidth+'Mbps';
                            prodL2.cosHighBandwidth  = (String)OrdOVC.coSHighBandwidth== '0' ? '':(String)OrdOVC.coSHighBandwidth+'Mbps';
                            chkModify = true;
                        }
                        if(OrdOVC.coSMediumBandwidth != PrevOrdOVC.coSMediumBandwidth){
                            //prodL2.coSMediumBandwidth = OrdOVC.coSMediumBandwidth+'Mbps';
                            prodL2.cosMediumBandwidth = (String)OrdOVC.coSMediumBandwidth== '0' ? '':(String)OrdOVC.coSMediumBandwidth+'Mbps';
                            chkModify = true;
                        }
                        if(OrdOVC.coSLowBandwidth != PrevOrdOVC.coSLowBandwidth){
                            //prodL2.coSLowBandwidth = OrdOVC.coSLowBandwidth+'Mbps';
                            prodL2.cosLowBandwidth = (String)OrdOVC.coSLowBandwidth== '0' ? '':(String)OrdOVC.coSLowBandwidth+'Mbps';
                            chkModify = true;
                        }
                        //nullity check
                        if(String.isNotBlank(OrdOVC.mappingMode) || String.isNotBlank(PrevOrdOVC.mappingMode)) {
                            if(OrdOVC.mappingMode != PrevOrdOVC.mappingMode){
                                prodL2.cosMappingMode = OrdOVC.mappingMode;
                                chkModify = true;
                            }
                        }

                        if(chkModify){
                            referencesProductOrderItemL1ovc.action = 'MODIFY';

                        }else{
                            referencesProductOrderItemL1ovc.action = 'NO CHANGE';
                        }
                        //prodL2.id = ProdConfig.csordtelcoa__Replaced_Service__r.OVC_Id__c;
                        csord__Service__c tempServ =  configToServiceMap.get(PrevOrdOVC.OVCId);
                        prodL2.ovcTransientId = tempServ.Name;
                        prodL2.id = tempServ.OVC_Id__c;//ProdConfig.csordtelcoa__Replaced_Service__r.OVC_Id__c;
                        //referencesProductOrderItemL1ovc.itemInvolvesProduct = prodL2;
                        //referencesProductOrderItemListL1.Add(referencesProductOrderItemL1ovc);

                    }
                    chkModify = false;
                }  else {
                    //START: ADD OVC json
                    //perform the population of below attributes if order is for add ovc modify
                    system.debug('Check if ovc sub type is add ovc');
                    if(order.Order_SubType__c != null
                            && order.Order_SubType__c.equalsIgnoreCase(DF_Constants.MODIFY_ADD_OVC)) {
                        system.debug('@!# PrevOrdOVC:: ' + PrevOrdOVC);
                        if(PrevOrdOVC == null){
                            referencesProductOrderItemL1ovc.action = 'ADD';
                            prodL2.type = 'OVC' ;
                            // OVC fields for add
                            prodL2.poi = OrdOVC.POI == null ? null: OrdOVC.POI;
                            prodL2.nni = OrdOVC.NNIGroupId == null ? null:OrdOVC.NNIGroupId;
                            prodL2.routeType  = OrdOVC.routeType == null ? null:OrdOVC.routeType ;
                            prodL2.sVLanId  = OrdOVC.sTag == null ? null:Integer.valueOf(OrdOVC.sTag);
                            prodL2.maximumFrameSize  = OrdOVC.ovcMaxFrameSize;

                            prodL2.cosHighBandwidth  = OrdOVC.coSHighBandwidth == '0' ? '':OrdOVC.coSHighBandwidth+'Mbps';
                            prodL2.cosMappingMode = OrdOVC.mappingMode == null ? null:OrdOVC.mappingMode ;
                            prodL2.cosMediumBandwidth = OrdOVC.coSMediumBandwidth == '0' ? '':OrdOVC.coSMediumBandwidth+'Mbps';
                            prodL2.cosLowBandwidth = OrdOVC.coSLowBandwidth == '0' ? '':OrdOVC.coSLowBandwidth+'Mbps';
                            prodL2.uniVLanId = OrdOVC.ceVlanId == null ? null:OrdOVC.ceVlanId;
                            prodL2.connectedTo    =  prevOrder.UNI_Id__c;//'1';// ovc.get('NNI');
                            prodL2.ovcTransientId = OrdOVC.OVCName == null ? null:OrdOVC.OVCName;
                        }
                    } //MODIFY UNI VLAN ID 

                    else if(order.Order_SubType__c != null && order.Order_SubType__c.equalsIgnoreCase(DF_Constants.UNI_VLAN_ID_MODIFY_SUBTYPE)) {
                        system.debug('!!! PrevOrdOVC:: ' + PrevOrdOVC);
                        if(PrevOrdOVC.ceVlanId != OrdOVC.ceVlanId){
                            referencesProductOrderItemL1ovc.action = 'MODIFY';
                            prodL2.type = 'OVC' ;
                            // OVC fields for Modify UNI VLAN Id
                            prodL2.uniVLanId = OrdOVC.ceVlanId == null ? null:OrdOVC.ceVlanId;
                            prodL2.ovcTransientId = OrdOVC.OVCName == null ? null:OrdOVC.OVCName;
                            csord__Service__c tempServ =  configToServiceMap.get(PrevOrdOVC.OVCId);
                            prodL2.id = tempServ.OVC_Id__c;
                        }else{
                            referencesProductOrderItemL1ovc.action = 'NO CHANGE';
                            prodL2.type = 'OVC' ;
                            csord__Service__c tempServ =  configToServiceMap.get(PrevOrdOVC.OVCId);
                            prodL2.ovcTransientId = tempServ.Name;
                            prodL2.id = tempServ.OVC_Id__c;
                        }
                    }
                    //END: ADD OVC json
                }
                referencesProductOrderItemL1ovc.itemInvolvesProduct = prodL2;
                referencesProductOrderItemListL1.Add(referencesProductOrderItemL1ovc);
          //  }

        }

        //referencesProductOrderItemL1.referencesProductOrderItem = referencesProductOrderItemL1List;
        productOrderComprisedOf.referencesProductOrderItem = referencesProductOrderItemListL1;

        List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2> referencesProductOrderItemListL2 = new List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2>();
        DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2 referencesProductOrderItemL2 = new DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2();
        if(connectService != null && modifyProductConfig !=null ){
            referencesProductOrderItemL2.action = 'NO CHANGE';
        }
        else{
            referencesProductOrderItemL2.action = 'NO CHANGE';
        }
        //referencesProductOrderItemL2.action = 'NO CHANGE';


        DF_OrderDataJSONWrapper.ItemInvolvesProductL3 itemInvolvesProductL3 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL3();
        itemInvolvesProductL3.type = 'UNI';
        itemInvolvesProductL3.id = prevOrder.UNI_Id__c;
        //add this attribute if add ovc : not needed
        /*if(order.Order_SubType__c != null && order.Order_SubType__c == 'Add new OVC') {
            itemInvolvesProductL3.transientId = '1';
        }*/
        system.debug('!!!!itemInvolvesProductL3 '+itemInvolvesProductL3);
        referencesProductOrderItemL2.itemInvolvesProduct = itemInvolvesProductL3;
        referencesProductOrderItemListL2.add(referencesProductOrderItemL2);

        referencesProductOrderItemL1.referencesProductOrderItem = referencesProductOrderItemListL2;

        productOrderComprisedOf.referencesProductOrderItem = referencesProductOrderItemListL1;
        productOrder.productOrderComprisedOf = productOrderComprisedOf;
        system.debug('!!!!!!! productOrderComprisedOf ' +productOrderComprisedOf);


        DF_OrderDataJSONWrapper.AccessSeekerContact accessSeekerContact = new DF_OrderDataJSONWrapper.AccessSeekerContact();
        accessSeekerContact.contactName = null;
        accessSeekerContact.contactPhone = null;
        DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot root = new DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot();

        productOrder.accessSeekerContact = accessSeekerContact;
        root.productOrder = productOrder;
        system.debug(JSON.serialize(root));
        jsonRetVal = JSON.serialize(root);
        return jsonRetVal;
        }
        catch(Exception e)
        {
            system.debug('Exception throw--- '+e.getMessage()+e.getLineNumber());
            throw e;
        }

    }

    public static string getESLA(string strJson, DF_Order__c dfOrder) {
        DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot orderJson = (DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot)JSON.deserialize(strJson, DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot.class);
        DF_OrderSubmittedSummaryController.UNIData uData = new DF_OrderSubmittedSummaryController.UNIData();
        uData = DF_OrderSubmittedSummaryHelper.getUNIHelper(uData, dfOrder, orderJson);
        return uData.uniESLA;
    }

    @TestVisible private static String nullToEmpty(String input) {
        return input == null ? '' : input;
    }
}
