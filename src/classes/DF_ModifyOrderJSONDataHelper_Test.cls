@isTest
private class DF_ModifyOrderJSONDataHelper_Test {
     
    @isTest static void generateJSONStringForOrderTest() {
        test.startTest(); 
            // Create OpptyBundle
            Account acc = DF_TestData.createAccount('Test Account');
            insert acc;
            
            DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acc.ID);
            insert opptyBundle;
            
            Opportunity oppty = DF_TestData.createOpportunity('Test Opp');
            oppty.Opportunity_Bundle__c = opptyBundle.id;
            insert oppty;
                        
            csord__Subscription__c subs = DF_TestService.createCSSubscriptionRecord(acc.Id, oppty);
            
            cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
            basketObj.cscfga__Opportunity__c = oppty.Id;
            update basketObj;
            
            List<cscfga__Product_Configuration__c> prodConfigList = DF_CS_API_Util.getProductBasketConfigs(basketObj.Id);
            prodConfigList[0].csordtelcoa__Replaced_Subscription__c = subs.Id;
            update prodConfigList[0];
            
            DF_Opportunity_Bundle__c opptyBundle2 = DF_TestData.createOpportunityBundle(acc.ID);
            insert opptyBundle2;
            
            Opportunity oppty2 = DF_TestData.createOpportunity('Test Opp2');
            oppty2.Opportunity_Bundle__c = opptyBundle2.id;
            insert oppty2;
                        
            //csord__Subscription__c subs2 = DF_TestService.createCSSubscriptionRecord(acc.Id, oppty2);
            
            cscfga__Product_Basket__c basketObj2 = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
            List<cscfga__Product_Configuration__c> prodConfigList2 = DF_CS_API_Util.getProductBasketConfigs(basketObj2.Id);
            //prodConfigList2[0].csordtelcoa__Replaced_Subscription__c = subs2.Id;
            prodConfigList2[0].csordtelcoa__Replaced_Product_Configuration__c = prodConfigList[0].Id;
            update prodConfigList2[0];
            
            csord__Order__c csOrder = DF_TestData.csOrder(oppty.id,acc.id);
            csOrder.name = DF_Constants.DIRECT_FIBRE_PRODUCT_CHARGES_NAME;
            insert csOrder;
        
            csord__Service__c service = new csord__Service__c();
            service.Name = 'OVC 1';
            service.csordtelcoa__Product_Basket__c = basketObj.Id;
            service.csord__Subscription__c = subs.Id;
            //service.csord__Order__c = csOrd.Id;
            service.csord__Identification__c = 'xxx';
            service.csordtelcoa__Product_Configuration__c = prodConfigList[0].Id;
            insert service;
            
            List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
            
            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';

            
            // Create DFQuote recs
            DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');         
            dfQuote1.Proceed_to_Pricing__c = true;
            dfQuoteList.add(dfQuote1);
            insert dfQuoteList;       
        
            csord__Service__c service1 = new csord__Service__c();
            service1.Name = DF_Constants.DIRECT_FIBRE_PRODUCT_CHARGES_NAME;
            service1.csordtelcoa__Product_Basket__c = basketObj.Id;
            service1.csord__Subscription__c = subs.Id;
            service1.csord__Order__c = csOrder.id;
            service1.csord__Identification__c = 'xxx';
            service1.csordtelcoa__Product_Configuration__c = prodConfigList[0].Id;
            insert service1;
               
            DF_Order__c testOrder = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle.Id );
            testOrder.Heritage_Site__c = 'Yes';
            testOrder.Induction_Required__c = 'Yes';
            testOrder.Security_Required__c = 'Yes';
            testOrder.Site_Notes__c = 'Site Notes';
            testOrder.Trading_Name__c = 'Trader Joes TEST SITE';
            testOrder.OVC_NonBillable__c = '[{"OVCId":"'+prodConfigList[0].Id+'","CSA":"CSA180000002222","NNIGroupId":"NNI090000015859","routeType":"Local","coSHighBandwidth":"50","coSMediumBandwidth":"150","coSLowBandwidth":"60","routeRecurring":"0.00","coSRecurring":"0.00","ovcMaxFrameSize":"Jumbo (9000 Bytes)","status":"Incomplete","sTag":"0","ceVlanId":"3421","mappingMode":"DSCP"}]';
            testOrder.Order_Json__c = DF_IntegrationTestData.buildDFOrderJSONString();
            Insert testOrder;
            
            DF_Order__c testOrder1 = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle.Id );
            testOrder1.Heritage_Site__c = 'Yes';
            testOrder1.Induction_Required__c = 'Yes';
            testOrder1.Security_Required__c = 'Yes';
            testOrder1.Site_Notes__c = 'Site Notes';
            testOrder1.Trading_Name__c = 'Trader Joes TEST SITE';
            testOrder1.Previous_Order__c = testOrder.Id;
            testOrder1.OrderType__c = 'Modify';
            testOrder1.After_Business_Hours__c = 'Yes'; //CPST-5509
            testOrder1.Order_SubType__c = 'Change Service Restoration SLA';
            testOrder1.Order_Json__c = DF_IntegrationTestData.buildDFOrderJSONString();
            testOrder1.OVC_NonBillable__c = '[{"OVCId":"'+prodConfigList2[0].Id+'","CSA":"CSA180000002222","NNIGroupId":"NNI090000015859","routeType":"Local","coSHighBandwidth":"50","coSMediumBandwidth":"100","coSLowBandwidth":"60","routeRecurring":"0.00","coSRecurring":"0.00","ovcMaxFrameSize":"Jumbo (9000 Bytes)","status":"Incomplete","sTag":"0","ceVlanId":"3421","mappingMode":"DSCP"}]';
            
            Insert testOrder1;

            
            string location = '{"status":"Order Draft","SCR":"1297.0000","SCNR":"5000.0000","quoteId":"EEQ-0000000324","orderId":"a7EN000000092gQMAQ","locId":"LOC000006231845","id":"a6aN00000000k5iIAA","FBC":0,"address":"6 LENNOX ST RAVENSWOOD TAS 7250 Australia","OrderId":"a7EN000000092gQMAQ","basketId":"a1eN0000000z2amIAA","UNI":[{"BTDType":"Standard (CSAS)","interfaceBandwidth":"1GB","AAT":"","SLA":"Premium - 12 (24/7)","term":"12","zone":"Zone 3","AHA":"No","coSRecurring":"0","oVCType":"Access EVPL","interfaceTypes":"Copper (Auto-negotiate)","tPID":"0x8100","Id":"a1hN0000001TOJTIA4"}],"OVCs":[{"OVCId":"a1hN0000001TOJUIA4","OVCName":"OVC 1","CSA":"CSA180000002222","monthlyCombinedCharges":"497.0000","routeType":"Local","coSHighBandwidth":"20","coSMediumBandwidth":"10","coSLowBandwidth":"0","routeRecurring":"0.0000","coSRecurring":"497.0000","POI":"1-MEL-MELBOURNE-MP","status":"Incomplete","mappingMode":"","NNIGroupId":"NNI090000015859","sTag":"0","ceVlanId":"12","ovcMaxFrameSize":"Jumbo (9000 Bytes)"}],"prodChargeConfigId":"a1hN0000001TOJTIA4","ovcConfigId":"a1hN0000001TOJUIA4","businessContactList":[{"busContSurname":"asf","busContSuburb":"sdf","busContStreetAddress":"asf","busContState":"VIC","busContRole":"asdf","busContPostcode":"2000","busContNumber":"0412123123","busContId":"003N000001DhSVkIAN","busContFirstName":"asd","busContEmail":"asdf@asd.com","busContIndex":1}],"siteContactList":[{"siteContSurname":"asdf","siteContNumber":"0412123123","siteContId":"003N000001DhSWsIAN","siteContFirstName":"asd","siteContEmail":"asdf@asdf.com","siteContIndex":1}],"SiteDetails":{"Trading Name":[""],"Heritage Site":["Yes"],"Induction Required":["Yes"],"Security Required":["Yes"],"Site Notes":[""]}}';

            String jsonRetval = DF_ModifyOrderJSONDataHelper.generateJSONStringForOrder(testOrder1.Id,location);
            system.debug('@!#jsonRetval modify cos:: ' + jsonRetVal);

            test.stopTest();  
        
    }

    @isTest static void generateJSONStringForOrderAddOVC_Test() {
        test.startTest();
        // Create OpptyBundle
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;

        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acc.ID);
        insert opptyBundle;

        Opportunity oppty = DF_TestData.createOpportunity('Test Opp');
        oppty.Opportunity_Bundle__c = opptyBundle.id;
        insert oppty;

        csord__Subscription__c subs = DF_TestService.createCSSubscriptionRecord(acc.Id, oppty);

        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        basketObj.cscfga__Opportunity__c = oppty.Id;
        update basketObj;

        List<cscfga__Product_Configuration__c> prodConfigList = DF_CS_API_Util.getProductBasketConfigs(basketObj.Id);
        prodConfigList[0].csordtelcoa__Replaced_Subscription__c = subs.Id;
        update prodConfigList[0];

        DF_Opportunity_Bundle__c opptyBundle2 = DF_TestData.createOpportunityBundle(acc.ID);
        insert opptyBundle2;

        Opportunity oppty2 = DF_TestData.createOpportunity('Test Opp2');
        oppty2.Opportunity_Bundle__c = opptyBundle2.id;
        insert oppty2;

        cscfga__Product_Basket__c basketObj2 = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        List<cscfga__Product_Configuration__c> prodConfigList2 = DF_CS_API_Util.getProductBasketConfigs(basketObj2.Id);
        system.debug('@!prodConfigList2:: ' + prodConfigList2);
        prodConfigList2[0].csordtelcoa__Replaced_Product_Configuration__c = prodConfigList[0].Id;
        update prodConfigList2[0];
        
        csord__Order__c csOrder = DF_TestData.csOrder(oppty.id,acc.id);
        csOrder.name = DF_Constants.DIRECT_FIBRE_PRODUCT_CHARGES_NAME;
        insert csOrder;
        
        csord__Service__c service = new csord__Service__c();
        service.Name = 'OVC 1';
        service.csordtelcoa__Product_Basket__c = basketObj.Id;
        service.csord__Subscription__c = subs.Id;
        service.csord__Identification__c = 'xxx';
        service.csordtelcoa__Product_Configuration__c = prodConfigList[0].Id;
        insert service;

        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;
        
        csord__Service__c service1 = new csord__Service__c();
        service1.Name = DF_Constants.DIRECT_FIBRE_PRODUCT_CHARGES_NAME;
        service1.csordtelcoa__Product_Basket__c = basketObj.Id;
        service1.csord__Subscription__c = subs.Id;
        service1.csord__Order__c = csOrder.id;
        service1.csord__Identification__c = 'xxx';
        service1.csordtelcoa__Product_Configuration__c = prodConfigList[0].Id;
        insert service1;
        system.debug('service1---'+service1);
        
        DF_Order__c testOrder = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle.Id );
        testOrder.Heritage_Site__c = 'Yes';
        testOrder.Induction_Required__c = 'Yes';
        testOrder.Security_Required__c = 'Yes';
        testOrder.Site_Notes__c = 'Site Notes';
        testOrder.Trading_Name__c = 'Trader Joes TEST SITE';
        testOrder.UNI_Id__c = 'UNI01111111111';
        testOrder.Order_Json__c = DF_IntegrationTestData.buildDFOrderJSONString();
        testOrder.OVC_NonBillable__c = '[{"OVCId":"'+prodConfigList[0].Id+'","CSA":"CSA180000002222","NNIGroupId":"NNI090000015859","routeType":"Local","coSHighBandwidth":"10","coSMediumBandwidth":"50","coSLowBandwidth":"50","routeRecurring":"0.00","coSRecurring":"0.00","ovcMaxFrameSize":"Jumbo (9000 Bytes)","status":"Incomplete","sTag":"0","ceVlanId":"3421","mappingMode":"DSCP"}]';
        Insert testOrder;

        DF_Order__c testOrder1 = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle2.Id );
        testOrder1.Heritage_Site__c = 'Yes';
        testOrder1.Induction_Required__c = 'Yes';
        testOrder1.Security_Required__c = 'Yes';
        testOrder1.Site_Notes__c = 'Site Notes';
        testOrder1.Trading_Name__c = 'Trader Joes TEST SITE 2';
        testOrder1.Previous_Order__c = testOrder.Id;
        testOrder1.OrderType__c = 'Modify';
        testOrder1.Order_SubType__c = 'Add new OVC';
        testOrder1.Order_Json__c = DF_IntegrationTestData.buildDFOrderJSONString();
        testOrder1.OVC_NonBillable__c = '[{"OVCId":"'+prodConfigList2[0].Id+'","OVCName":"OVC 1","CSA":"CSA180000002222","routeType":"Local","coSHighBandwidth":"10","coSMediumBandwidth":"50","coSLowBandwidth":"50","routeRecurring":"0.00","coSRecurring":"750.00","POI":"1-MEL-MELBOURNE-MP","status":"Incomplete","mappingMode":"DSCP","NNIGroupId":"NNI123123123123","sTag":"0","ceVlanId":"123","ovcMaxFrameSize":"Jumbo (9000 Bytes)"},{"OVCId":"a1h5D00000071q0QAA","CSA":"CSA180000002222","NNIGroupId":"NNI123456789123","routeType":"Local","coSHighBandwidth":"40","coSMediumBandwidth":"0","coSLowBandwidth":"0","routeRecurring":"0.00","coSRecurring":"0","ovcMaxFrameSize":"Jumbo (9000 Bytes)","status":"Incomplete","sTag":"7","ceVlanId":"8","mappingMode":"DSCP"}]';
        Insert testOrder1;

        string location = '{"status":"Order Draft","SCR":"1297.0000","SCNR":"5000.0000","quoteId":"EEQ-0000000324","orderId":"a7EN000000092gQMAQ","locId":"LOC000006231845","id":"a6aN00000000k5iIAA","FBC":0,"address":"6 LENNOX ST RAVENSWOOD TAS 7250 Australia","OrderId":"a7EN000000092gQMAQ","basketId":"a1eN0000000z2amIAA","UNI":[{"BTDType":"Standard (CSAS)","interfaceBandwidth":"1GB","AAT":"","SLA":"Premium - 12 (24/7)","term":"12","zone":"Zone 3","AHA":"No","coSRecurring":"0","oVCType":"Access EVPL","interfaceTypes":"Copper (Auto-negotiate)","tPID":"0x8100","Id":"a1hN0000001TOJTIA4"}],"OVCs":[{"OVCId":"a1hN0000001TOJUIA4","OVCName":"OVC 1","CSA":"CSA180000002222","monthlyCombinedCharges":"497.0000","routeType":"Local","coSHighBandwidth":"20","coSMediumBandwidth":"10","coSLowBandwidth":"0","routeRecurring":"0.0000","coSRecurring":"497.0000","POI":"1-MEL-MELBOURNE-MP","status":"Incomplete","mappingMode":"","NNIGroupId":"NNI090000015859","sTag":"0","ceVlanId":"12","ovcMaxFrameSize":"Jumbo (9000 Bytes)"}],"prodChargeConfigId":"a1hN0000001TOJTIA4","ovcConfigId":"a1hN0000001TOJUIA4","businessContactList":[{"busContSurname":"asf","busContSuburb":"sdf","busContStreetAddress":"asf","busContState":"VIC","busContRole":"asdf","busContPostcode":"2000","busContNumber":"0412123123","busContId":"003N000001DhSVkIAN","busContFirstName":"asd","busContEmail":"asdf@asd.com","busContIndex":1}],"siteContactList":[{"siteContSurname":"asdf","siteContNumber":"0412123123","siteContId":"003N000001DhSWsIAN","siteContFirstName":"asd","siteContEmail":"asdf@asdf.com","siteContIndex":1}],"SiteDetails":{"Trading Name":[""],"Heritage Site":["Yes"],"Induction Required":["Yes"],"Security Required":["Yes"],"Site Notes":[""]}}';

        String jsonRetVal = DF_ModifyOrderJSONDataHelper.generateJSONStringForOrder(testOrder1.Id,location);
        test.stopTest();

        system.assert(jsonRetVal != null);


    }
}