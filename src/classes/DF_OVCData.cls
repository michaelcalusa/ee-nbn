public virtual  class DF_OVCData{
	// Wrapper class to map values to the quick quote screen
    @AuraEnabled
    public String OVCId {get;set;}    
    @AuraEnabled
    public String NNI {get;set;}    
	@AuraEnabled
    public String RouteType {get;set;}    
	@AuraEnabled
    public String CoSHighBandwidth {get;set;}    
	@AuraEnabled
    public String CoSMediumBandwidth {get;set;}    
	@AuraEnabled
    public String CoSLowBandwidth {get;set;}    
	@AuraEnabled
    public String RouteRecurring {get;set;}    
	@AuraEnabled
    public String CoSRecurring {get;set;}
    @AuraEnabled
    public String POI {get;set;}
	@AuraEnabled
    public String Status {get;set;}
	
	public DF_OVCData() {
		OVCId = '';
        NNI = '';
        RouteType = '';
        CoSHighBandwidth = '';
        CoSMediumBandwidth = '';
        CoSLowBandwidth = '';
        RouteRecurring = '';
        CoSRecurring = '';
		POI = '';
		Status = 'Incomplete';
    }

    public DF_OVCData(
		String OVCId,
		String NNI, 
		String RouteType, 
		String HighBandwidth, 
		String MediumBandwidth, 
		String LowBandwidth, 
		String RouteRecurring,
		String Recurring,
		String POI,
		String Status) {

		OVCId = OVCId;
        NNI = NNI;
        RouteType = RouteType;
        CoSHighBandwidth = HighBandwidth;
        CoSMediumBandwidth = MediumBandwidth;
        CoSLowBandwidth = LowBandwidth;
        RouteRecurring = RouteRecurring;
        CoSRecurring = Recurring;
		POI = POI;
		Status = Status;
    }
}