public class DF_OVCWrapper {
	public List<OVC> OVC;
    public class OVC {
		public String OVCId;	//a1h5D0000006i8YQAQ
		public String OVCName; //OVC 1
		public String CSA;	//CSA180000002222
		public String NNIGroupId;	//NNI123456789123
		public String routeType;	//Local
		public String coSHighBandwidth;	//250
		public String coSMediumBandwidth;	//100
		public String coSLowBandwidth;	//0
		public String routeRecurring;	//0.00
		public String coSRecurring;	//0.00
		public String ovcMaxFrameSize;	//Jumbo (9000 Bytes)
		public String status;	//Incomplete
		public String sTag;	//12
		public String ceVlanId;	//2
		public String mappingMode;	//DSCP
		public String ovcSystemId; //OVC000000123456
		public String POI; //3EXH - EXHIBITION
	}
	public static DF_OVCWrapper parse(String json){
		return (DF_OVCWrapper) System.JSON.deserialize(json, DF_OVCWrapper.class);
	}
}