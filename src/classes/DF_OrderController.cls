public with sharing class DF_OrderController {

	@AuraEnabled
    public static String getBasketId(String quoteId) {
    	String basketId = DF_ProductController.getBasketId(quoteId);
    	return basketId;
    }
    
	@AuraEnabled
    public static void processUpdateDFQuoteStatus(String quoteId) {    	      
		DF_Quote__c dfQuoteToUpdate;
		List<DF_Quote__c> dfQuotesToUpdateList;

		try {		
	        if (String.isNotEmpty(quoteId)) {	        	
	        	dfQuotesToUpdateList = [SELECT Status__c
	        	 				   		FROM   DF_Quote__c
	        	 				   		WHERE  Id = :quoteId
	        	 				   		AND    Status__c <> :DF_Constants.QUOTE_STATUS_ORDER_DRAFT];					        	 				   
	        }

	        // Perform update       
	        if (!dfQuotesToUpdateList.isEmpty()) {
				dfQuoteToUpdate = dfQuotesToUpdateList[0];	

				if (dfQuoteToUpdate != null) {
					dfQuoteToUpdate.Status__c = DF_Constants.QUOTE_STATUS_ORDER_DRAFT;										
					update dfQuoteToUpdate;					
				}
	        }	 					   
		} catch(Exception e) {
			throw new AuraHandledException(e.getMessage());			
        } 
    }     
}