@isTest
private class DF_OrderController_Test {	

    @isTest static void test_getBasketId() {
    	// Setup data   	   	    	    	
    	List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
    	
    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		// Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

		// Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		// Create Oppty
		Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
		oppty.Opportunity_Bundle__c = opptyBundle.Id;
		insert oppty;

		// Create DFQuote recs
    	DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');	    	
    	dfQuote1.Proceed_to_Pricing__c = true;
    	dfQuoteList.add(dfQuote1);
    	insert dfQuoteList;                       
        
        Test.startTest();

        String basketId = DF_OrderController.getBasketId(dfQuote1.Id);
        
        Test.stopTest();             
    }  

    @isTest static void test_processUpdateDFQuoteStatus() {
    	// Setup data   	   	    	    	
    	List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
    	
    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		// Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

		// Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		// Create Oppty
		Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
		oppty.Opportunity_Bundle__c = opptyBundle.Id;
		insert oppty;

		// Create DFQuote recs
    	DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');	    	
    	dfQuote1.Proceed_to_Pricing__c = true;
    	dfQuoteList.add(dfQuote1);
    	insert dfQuoteList;                       
        
        Test.startTest();

        DF_OrderController.processUpdateDFQuoteStatus(String.valueOf(dfQuote1.Id));
        
        Test.stopTest();
        
		// Assertions
		List<DF_Quote__c> dfQuoteVerifyList = [SELECT Id
									     		FROM   DF_Quote__c 
									     		WHERE  Status__c = :DF_Constants.QUOTE_STATUS_ORDER_DRAFT];
									 
        system.AssertEquals(false, dfQuoteVerifyList.isEmpty());        
    }    
}