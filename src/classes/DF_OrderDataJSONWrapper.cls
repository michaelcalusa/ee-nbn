public class DF_OrderDataJSONWrapper {
 
   public class DF_OrderDataJSONRoot {
             public ProductOrder productOrder { get; set; }
    }
 
    public class ProductOrder {
        public String associatedReferenceId { get; set; } //added for appian payload
             public AccessSeekerInteraction accessSeekerInteraction { get; set; }
        public String orderType { get; set; }
        public String afterHoursProcessing { get; set; } //CPST-5509
        public String salesforceQuoteId { get; set; }
        public String salesforceBundleId { get; set; }
        public String customerRequiredDate { get; set; }       
             public String afterHoursAppointment { get; set; }     
             public String inductionRequired { get; set; } 
             public String securityClearance { get; set; } 
             public String notes { get; set; }     
             public String tradingName { get; set; }
             public String heritageSite { get; set; }
             public String csaId { get; set; } //added  by Vishal
             public List<ItemInvolvesContact> itemInvolvesContact { get; set; }     
             public ProductOrderComprisedOf productOrderComprisedOf { get; set; } 
             public AccessSeekerContact accessSeekerContact { get; set; }
    }
 
    public class AccessSeekerInteraction {
             public String billingAccountID { get; set; }
    }
 
    public class ItemInvolvesContact {
             public String type { get; set; }
             public String contactName { get; set; }
             public String role { get; set; }
             public String emailAddress { get; set; }
             public String phoneNumber { get; set; }
             public UnstructuredAddress unstructuredAddress { get; set; }
    }
 
    public class ProductOrderComprisedOf {
             public String action { get; set; }
             public ItemInvolvesLocation itemInvolvesLocation { get; set; }
             public ItemInvolvesProductL1 itemInvolvesProduct { get; set; }
             public List<ReferencesProductOrderItemL1> referencesProductOrderItem { get; set; }
    }
 
    public class UnstructuredAddress {
             public String addressLine1 { get; set; }
             public String addressLine2 { get; set; }
             public String addressLine3 { get; set; }
             public String localityName { get; set; }
             public String postcode { get; set; }
             public String stateTerritoryCode { get; set; } 
    }
 
    public class ItemInvolvesLocation {
             public String id { get; set; }
    }
 
    public class ItemInvolvesProductL1 {
        	 public String id {get; set; } //Raja: CPST-673
             public String term { get; set; }
             public String serviceRestorationSLA { get; set; }
             public String accessAvailabilityTarget { get; set; }       
             public String zone { get; set; }
             public String productType { get; set; }
             public String templateId { get; set; }
             public String templateVersion { get; set; }                           
    }
 
    public class ReferencesProductOrderItemL1 {
             public String action { get; set; }
             public ItemInvolvesProductL2 itemInvolvesProduct { get; set; }            
             public List<ReferencesProductOrderItemL2> referencesProductOrderItem { get; set; }                                                  
    }
 
    public class ItemInvolvesProductL2 {     
             public String type { get; set; }
             // NTD / BTD fields
             public String btdType { get; set; }     
             public String btdMounting { get; set; }      
             public String powerSupply1 { get; set; }
             public String powerSupply2 { get; set; }
             // OVC fields
        	 public String id {get; set; } //Raja: CPST-673
        	 public String poi { get; set; } 
             public String nni { get; set; }       
             public String routeType { get; set; }    
             public Integer sVLanId { get; set; }            
             public String maximumFrameSize { get; set; } 
             public String cosHighBandwidth { get; set; } 
             public String cosMediumBandwidth { get; set; } 
             public String cosLowBandwidth { get; set; } 
             public String uniVLanId { get; set; }    
             public String connectedTo { get; set; }  
             public String ovcTransientId {get; set;}    
             public String cosMappingMode { get; set; }                                                                                                   
    }
 
    public class ItemInvolvesProductL3 {
      // UNI fields
        	 public String id {get; set; } //Raja: CPST-673
             public String type { get; set; }
             public String interfaceBandwidth { get; set; } 
             public String interfaceType { get; set; }      
             public String ovcType { get; set; }     
             public String tpId { get; set; }                                                   
             //public String cosMappingMode { get; set; }
             public String transientId { get; set; }
    }
 
    public class ReferencesProductOrderItemL2 {
             public String action { get; set; }         
             public ItemInvolvesProductL3 itemInvolvesProduct { get; set; }                                                         
    }
 
    public class AccessSeekerContact {
             public String contactName { get; set; }
             public String contactPhone { get; set; }
    }


//    public static String generateJSONStringForOrder(String  dfOrderId, String location) {
//        String jsonRetVal = '';
//try{
//        DF_Order__c objObject = new DF_Order__c();  
//        DF_Order__c order;
//        List<DF_Order__c> orderList;
//        if (String.isNotEmpty(dfOrderId)) {               
//                orderList = [SELECT Id, Trading_Name__c, Heritage_Site__c,Induction_Required__c, Security_Required__c,Site_Notes__c, 
//                                    Customer_Required_Date__c,NTD_Mounting__c,Power_Supply_1__c,Power_Supply_2__c,Installation_Notes__c,
//                                    DF_Quote__c
//                                    FROM DF_Order__c
//                                    WHERE  Id = :dfOrderId];                                                   
//            }
//            if (!orderList.isEmpty()) {
//                order = orderList[0];
//                system.debug('!!!!' + order);
//               // order.Order_JSON__c = jsonOVCnonBillable;
//               // update order;
//            }
////r.Opportunity_Bundle__r.Account__r.Billing_ID__c

//        DF_Quote__c quote = [SELECT Opportunity_Bundle__c FROM DF_Quote__c WHERE Id =: order.DF_Quote__c LIMIT 1];
//        DF_Opportunity_Bundle__c opBundle = [SELECT Name, Account__c FROM DF_Opportunity_Bundle__c WHERE Id =: quote.Opportunity_Bundle__c LIMIT 1 ];
//        Account account = [SELECT Billing_ID__c FROM Account WHERE Id =: opBundle.Account__c LIMIT 1]; 

        
//        //DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 refProdOrderL1 = new DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1();
////
//       // DF_OrderDataJSONWrapper.ItemInvolvesProductL3 prodL3 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL3();//UNI
//       // DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2 refProdOrderL2 = new DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2();
//        //DF_OrderDataJSONWrapper.AccessSeekerContact accessSeekerContact = new DF_OrderDataJSONWrapper.AccessSeekerContact();






//        System.debug('!!!!' + JSON.serializePretty(location));
//        Map<String, Object> orderJSON = (Map<String, Object>) JSON.deserializeUntyped(location);
//        System.debug('!!!!' + orderJSON);
        

//        List<Object> uniList = (List<Object>)orderJSON.get('UNI');
//        system.debug('!!! uniList'+uniList);
//        Map<String, Object> uni = (Map<String, Object>)uniList[0]; //only one UNI

//        List<Object> ovcList = (List<Object>)orderJSON.get('OVCs');
//        system.debug('!!! ovcList'+ovcList);


//        DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot productOrderRoot = new DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot();

//        //get acces seeker
//        DF_OrderDataJSONWrapper.AccessSeekerInteraction accessSeeker = new DF_OrderDataJSONWrapper.AccessSeekerInteraction();
//            accessSeeker.billingAccountID = account.Billing_ID__c;
//        //
//        //create datevar

//        DF_OrderDataJSONWrapper.ProductOrder productOrder = new DF_OrderDataJSONWrapper.ProductOrder();
   
//        system.debug('!!!! order.Customer_Required_Date__c @186' +order.Customer_Required_Date__c  );
//        If(order.Customer_Required_Date__c != null){
//            Date dToday = order.Customer_Required_Date__c;
//            Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
//            productOrder.customerRequiredDate = dt.format('yyyy-MM-dd');
//        }
//        //create productOrder

//        productOrderRoot.productOrder = productOrder;
//        productOrder.accessSeekerInteraction = accessSeeker;
//        productOrder.orderType = 'Connect';
//        productOrder.salesforceQuoteId = (String)orderJSON.get('quoteId');
//        productOrder.salesforceBundleId = opBundle.Name;
       
//        productOrder.afterHoursAppointment =  (String)uni.get('AHA');
//        productOrder.securityClearance = order.Security_Required__c;
//        productOrder.inductionRequired = order.Induction_Required__c;
//        productOrder.notes = order.Site_Notes__c;
//        productOrder.heritageSite = order.Heritage_Site__c;
//        productOrder.tradingName = order.Trading_Name__c;
        
//        List<ItemInvolvesContact> itemInvolvesContact = new List<ItemInvolvesContact>();
//        //set Site businessContList
//        List<Object> businessContList = (List<Object>)orderJSON.get('businessContactLiist');
//        system.debug('!!! businessContList'+businessContList);


//        if(businessContList !=null ){
//            for(Integer i = 0; i<businessContList.size(); i++){
//                DF_OrderDataJSONWrapper.ItemInvolvesContact itemInvBusContact = new DF_OrderDataJSONWrapper.ItemInvolvesContact();
//                Map<String, Object> bContact = (Map<String, Object>)businessContList[i];
//                itemInvBusContact.type = 'Business';
//                itemInvBusContact.contactName = (String)bContact.get('busContFirstName') + ' ' + (String)bContact.get('busContSurname');
//                itemInvBusContact.role = (String)bContact.get('busContRole');
//                itemInvBusContact.emailAddress = (String)bContact.get('busContEmail');
//                itemInvBusContact.phoneNumber = (String)bContact.get('busContNumber');
                
//                DF_OrderDataJSONWrapper.UnstructuredAddress busContAddress = new DF_OrderDataJSONWrapper.UnstructuredAddress();
//                busContAddress.addressLine1 = (String)bContact.get('busContStreetAddress');
//                busContAddress.addressLine2 = '';
//                busContAddress.addressLine3 = '';
//                busContAddress.localityName =  (String)bContact.get('busContSuburb');
//                busContAddress.postcode = (String)bContact.get('busContPostcode');
//                busContAddress.stateTerritoryCode = (String)bContact.get('busContState');
//                itemInvBusContact.unstructuredAddress = busContAddress;
//                System.debug('!!! itemInvBusContact'  +itemInvBusContact);
//                System.debug('!!! busContAddress'  +busContAddress);
//                itemInvolvesContact.add(itemInvBusContact);

//            }
//        }

//        List<Object> siteContactList= (List<Object>)orderJSON.get('siteContactList');
//        system.debug('!!! siteContactList'+siteContactList);    
////Site Contact List
//        if(siteContactList != null ){
//            for(Integer i = 0; i<siteContactList.size(); i++){
//                DF_OrderDataJSONWrapper.ItemInvolvesContact itemInvSiteContact = new DF_OrderDataJSONWrapper.ItemInvolvesContact();
//                Map<String, Object> sContact = (Map<String, Object>)siteContactList[i];
//                itemInvSiteContact.type = 'Site';
//                itemInvSiteContact.contactName = (String)sContact.get('siteContFirstName') + ' ' + (String)sContact.get('siteContSurname');
//                itemInvSiteContact.role = '' ;
//                itemInvSiteContact.emailAddress = (String)sContact.get('siteContEmail');
//                itemInvSiteContact.phoneNumber = (String)sContact.get('siteContNumber');
                
//                DF_OrderDataJSONWrapper.UnstructuredAddress siteContAddress = new DF_OrderDataJSONWrapper.UnstructuredAddress();
//                siteContAddress.addressLine1 ='';
//                siteContAddress.addressLine2 = '';
//                siteContAddress.addressLine3 = '';
//                siteContAddress.localityName = '';
//                siteContAddress.postcode = '';
//                siteContAddress.stateTerritoryCode = '';
//                itemInvSiteContact.unstructuredAddress = siteContAddress;
//                System.debug('!!! itemInvSiteContact'  +itemInvSiteContact);
//                System.debug('!!! siteContAddress'  +siteContAddress);
//                itemInvolvesContact.add(itemInvSiteContact);

//            }
//        }

//        //add item involves contact site and business generated above
//        productOrder.itemInvolvesContact = itemInvolvesContact;

//        //generate product order comprised of
//        DF_OrderDataJSONWrapper.ProductOrderComprisedOf productOrderComprisedOf = new DF_OrderDataJSONWrapper.ProductOrderComprisedOf();
//        productOrderComprisedOf.action = 'ADD';
//        //itemInvolvesLocation
//        DF_OrderDataJSONWrapper.ItemInvolvesLocation itemInvolvesLocation = new DF_OrderDataJSONWrapper.ItemInvolvesLocation();
//        itemInvolvesLocation.id = (String)orderJSON.get('locId');
//        productOrderComprisedOf.itemInvolvesLocation = itemInvolvesLocation;
//        //itemInvolvesProductL1 Wors
//        DF_OrderDataJSONWrapper.ItemInvolvesProductL1 itemInvolvesProductL1 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL1();
//        itemInvolvesProductL1.term = (String)uni.get('term');
//        itemInvolvesProductL1.serviceRestorationSLA = (String)uni.get('SLA');
//        itemInvolvesProductL1.accessAvailabilityTarget = (String)uni.get('AAT');
//        itemInvolvesProductL1.zone = (String)uni.get('zone');
//        itemInvolvesProductL1.productType = 'EE';//add custom setting ?
//        itemInvolvesProductL1.templateId = 'TPL';//add custom setting ?
//        itemInvolvesProductL1.templateVersion = '1.0';//add custom setting ?
//        productOrderComprisedOf.itemInvolvesProduct = itemInvolvesProductL1;


//        List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1> referencesProductOrderItemListL1 = new List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1>();
//        //referencesProductOrderItemL1
//        DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 referencesProductOrderItemL1 = new DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1();
//        referencesProductOrderItemL1.action = 'ADD';
//        //Add BTD 
//        DF_OrderDataJSONWrapper.ItemInvolvesProductL2 itemInvolvesProductL2 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL2();
//         // NTD / BTD fields
//        itemInvolvesProductL2.type = 'BTD';
//        itemInvolvesProductL2.btdType = 'Standard';
//        itemInvolvesProductL2.btdMounting = order.NTD_Mounting__c;
//        itemInvolvesProductL2.powerSupply1 = order.Power_Supply_1__c;
//        itemInvolvesProductL2.powerSupply2 = order.Power_Supply_2__c;

//        referencesProductOrderItemL1.itemInvolvesProduct = itemInvolvesProductL2;
//        referencesProductOrderItemListL1.Add(referencesProductOrderItemL1);
//       // referencesProductOrderItemL1.referencesProductOrderItem
//        //BTD CORRECT

//        //SET OVCs
//        //creating OVCs ItemInvolvesProductL2
//        //adding to the 
//        List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1>  referencesProductOrderItemL1List = new List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1> ();

//        for (Integer i =0 ; i< ovcList.size();i++){
//            DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 referencesProductOrderItemL1ovc = new DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1();
//            DF_OrderDataJSONWrapper.ItemInvolvesProductL2 prodL2 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL2();//OVCs
//            system.debug('!!!! ovcListf i ' + ovcList[i]);
//            Map<String, Object> ovc = (Map<String, Object>)ovcList[i];
//            referencesProductOrderItemL1ovc.action = 'ADD';
//            prodL2.type = 'OVC' ;
//             // OVC fields
//             prodL2.poi = (String)ovc.get('POI')== null ? null:(String)ovc.get('POI');
//             prodL2.nni = (String)ovc.get('NNI')== null ? null:(String)ovc.get('NNI');    
//             prodL2.routeType  = (String)ovc.get('routeType') == null ? null:(String)ovc.get('routeType') ;   
//             prodL2.sVLanId  = (String)ovc.get('sTag') == null ? null:Integer.valueOf((String)ovc.get('sTag'));      
//             prodL2.maximumFrameSize  = null;
//             System.debug('!!! line 325');
//             prodL2.cosHighBandwidth  = (String)ovc.get('cosHighBandwidth');
//             System.debug('!!! line 327');
//             prodL2.cosMediumBandwidth = (String)ovc.get('cosMediumBandwidth');
//             prodL2.cosLowBandwidth = (String)ovc.get('cosLowBandwidth');
//             prodL2.uniVLanId = (String)ovc.get('ceVlanId') == null ? null:(String)ovc.get('ceVlanId');
//             prodL2.connectedTo    = '1';// (String)ovc.get('NNI');  
//             referencesProductOrderItemL1ovc.itemInvolvesProduct = prodL2;     
//             //refProdOrderL1.itemInvolvesProduct = prodL2;
//             //prodOrderCompOf.referencesProductOrderItem.Push(refProdOrderL1.itemInvolvesProduct);
//             //referencesProductOrderItemL1List.Add(referencesProductOrderItemL2ovc);
//             referencesProductOrderItemListL1.Add(referencesProductOrderItemL1ovc);

//              }
//        //referencesProductOrderItemL1.referencesProductOrderItem = referencesProductOrderItemL1List;
//        productOrderComprisedOf.referencesProductOrderItem = referencesProductOrderItemListL1;

//        List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2> referencesProductOrderItemListL2 = new List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2>();
//        DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2 referencesProductOrderItemL2 = new DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2();
//        referencesProductOrderItemL2.action = 'ADD';


//        DF_OrderDataJSONWrapper.ItemInvolvesProductL3 itemInvolvesProductL3 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL3();  
//        itemInvolvesProductL3.type = 'UNI';
//        itemInvolvesProductL3.interfaceBandwidth = (String)uni.get('interfaceBandwidth');
//        itemInvolvesProductL3.interfaceType = (String)uni.get('interfaceTypes');     
//        itemInvolvesProductL3.ovcType = (String)uni.get('oVCType');    
//        itemInvolvesProductL3.tpId = (String)uni.get('tPID');                                                 
//        itemInvolvesProductL3.cosMappingMode = (String)uni.get('mappingMode');
//        itemInvolvesProductL3.transientId = '1';
//        system.debug('!!!!itemInvolvesProductL3 '+itemInvolvesProductL3);
//        referencesProductOrderItemL2.itemInvolvesProduct = itemInvolvesProductL3;
//        referencesProductOrderItemListL2.add(referencesProductOrderItemL2);
        
//        referencesProductOrderItemL1.referencesProductOrderItem = referencesProductOrderItemListL2;






//        productOrderComprisedOf.referencesProductOrderItem = referencesProductOrderItemListL1;
//        productOrder.productOrderComprisedOf = productOrderComprisedOf;
//        system.debug('!!!!!!! productOrderComprisedOf ' +productOrderComprisedOf);


//        DF_OrderDataJSONWrapper.AccessSeekerContact accessSeekerContact = new DF_OrderDataJSONWrapper.AccessSeekerContact();
//        accessSeekerContact.contactName = null;
//        accessSeekerContact.contactPhone = null;
//        DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot root = new DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot();

//        productOrder.accessSeekerContact = accessSeekerContact;
//        root.productOrder = productOrder;
//        system.debug(JSON.serialize(root));
//        jsonRetVal = JSON.serialize(root);
//        return jsonRetVal;
//        }
//        catch(Exception e)
//        {
//            throw e;
//        }

//    }

//    @TestVisible private static String nullToEmpty(String input) {
//    return input == null ? '' : input;
//}


   
}