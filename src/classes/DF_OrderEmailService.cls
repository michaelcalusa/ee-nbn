/*------------------------------------------------------------  
  Author:        Bharath Kumar
  Company:       Contractor
  Description:   An email utility class for DF Order
 
  Test Class:    DF_OrderEmailServiceTest
  History
  19/10/2018      Bharath Kumar     Creation
  ------------------------------------------------------------*/
public without sharing class DF_OrderEmailService {

	private static final string ERROR = 'Error';
	private static final String AUTOMATED_PROCESS_USER_TYPE = 'AutomatedProcess';
	private static final String DF_ORDER = 'DF_Order';
	private static final String NS_ORDER = 'NS_Order';
	public static DF_Email_Setting__mdt ORDER_EMAIL_SETTING = getDFEmailSetting(DF_ORDER);
	private static Contact randomContact = null;
	public static OrgWideEmailAddress orgWideEmailAddress { get; set; }

	
	public static Integer sendEmail(Map<String, String> orderToTemplateMap) {
	    return sendEmail(orderToTemplateMap, DF_ORDER);
	}

    public static void sendNSOrderEmail(Map<String, String> orderToTemplateMap) {
        sendEmail(orderToTemplateMap, NS_ORDER);
    }

	public void sendNsOrderMail(Map<String, String> orderToTemplateMap) {
		sendEmail(orderToTemplateMap, NS_ORDER);
	}

    public static Integer sendEmail(Map<String, String> orderToTemplateMap, String emailSettingName) {
        System.debug('DF_OrderEmailService.sendEmail() with setting name: ' + emailSettingName);
		Integer emailCount = 0;	
		List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage> ();
		List<Messaging.SingleEmailMessage> msgListToBeSend = new List<Messaging.SingleEmailMessage> ();
		String orderEmailToggle = '';
		DF_Email_Setting__mdt dfOrderEmailSetting = null;
		String replyTo = null;
		String senderName = null;

		try {
			if (orderToTemplateMap != null && !orderToTemplateMap.isEmpty()) {
				dfOrderEmailSetting = getDFEmailSetting(emailSettingName);
				if (dfOrderEmailSetting != null) {

					emailsToSend = getEmailsToSend(orderToTemplateMap, dfOrderEmailSetting);

					for (Messaging.SingleEmailMessage email : emailsToSend) {
						Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
						if (email.getTargetObjectId() != null
						    && randomContact != null
						    && randomContact.Id == email.getTargetObjectId()) {
							emailToSend.setOrgWideEmailAddressId(email.getOrgWideEmailAddressId());
							emailToSend.setToAddresses(email.getToAddresses());
							emailToSend.setCcAddresses(email.getCcAddresses());
							emailToSend.setPlainTextBody(email.getPlainTextBody());
							emailToSend.setHTMLBody(email.getHTMLBody());
							emailToSend.setSubject(email.getSubject());
						} else {
							emailToSend = email;
						}
						msgListToBeSend.add(emailToSend);
					}
					emailCount = msgListToBeSend.size();

					if (!msgListToBeSend.isEmpty()) {
						Messaging.SendEmailResult[] results = Messaging.sendEmail(msgListToBeSend, false);
					}
				} else {
					GlobalUtility.logMessage(DF_OrderEmailService.ERROR, DF_OrderEmailService.class.getName(), 'sendEmail', null, null, 'Order email disabled', null, null, 0);
				}
			}
		} catch(Exception e) {
			GlobalUtility.logMessage(DF_OrderEmailService.ERROR, DF_OrderEmailService.class.getName(), 'sendEmail', null, null, e.getMessage(), null, e, 0);
		} finally {
			return emailCount;
		}

		return emailCount;
	}

	/**
	 * Send Email
	 *
	 * @param      dfOrderToTemplateMap         Map of Order and Template names
	 *
	 * @return     void
	 */
	public static List<Messaging.SingleEmailMessage> getEmailsToSend(Map<String, String> dfOrderToTemplateMap,
	                                                                 DF_Email_Setting__mdt dfOrderEmailSetting) {
		List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage> ();
		Map<String, String> templateMap = new Map<String, String> ();
		String payload = '';

		try {
			if (dfOrderEmailSetting != null) {

				orgWideEmailAddress = getOrgWideEmailAddress(dfOrderEmailSetting.Sender_Address__c);

				templateMap = getEmailTemplates(dfOrderToTemplateMap);

				System.debug('@@@ templateMap: ' + templateMap);
				System.debug('@@@ dfOrderEmailSetting: ' + dfOrderEmailSetting);

				// Get the email to be sent to DF Order creator
				if (!dfOrderEmailSetting.Turn_Off_Creator_Emails__c) {
					emailsToSend.addAll(getDFOrderCreatorEmailToSend(dfOrderToTemplateMap, templateMap, dfOrderEmailSetting));
				} else {
					payload = 'Email delivery to Creator is disabled for EE Orders. No email is being sent.';
					GlobalUtility.logMessage(DF_OrderEmailService.ERROR, DF_OrderEmailService.class.getName(), 'getAllEmailsToSend', null, null, null, payload, null, 0);
				}

				// Get the emails to be sent to others
				if (!dfOrderEmailSetting.Turn_Off_Notification_Emails__c) {
					emailsToSend.addAll(getNotificationEmailsToSend(dfOrderToTemplateMap, templateMap, dfOrderEmailSetting));
				} else {
					payload = 'Email delivery to Contact Matrix is disabled for EE Orders. No email is being sent.';
					GlobalUtility.logMessage(DF_OrderEmailService.ERROR, DF_OrderEmailService.class.getName(), 'getAllEmailsToSend', null, null, null, payload, null, 0);
				}

			}

		} catch(Exception e) {
			GlobalUtility.logMessage(ERROR, DF_OrderEmailService.class.getName(), 'sendEmail', null, null, e.getMessage(), null, e, 0);
		}
		return emailsToSend;
	}


	/**
	 * Get the list of emails to be sent
	 *
	 * @param      dfOrderToTemplateMap         Map of Order and Template names
	 * @param      templateMap                   Map of all Template Named and template
	 *
	 * @return     List<Messaging.SingleEmailMessage>
	 */
	public static List<Messaging.SingleEmailMessage> getDFOrderCreatorEmailToSend(Map<String, String> dfOrderToTemplateMap,
	                                                                              Map<String, String> templateMap,
	                                                                              DF_Email_Setting__mdt dfOrderEmailSetting) {
		List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage> ();

		Set<String> dfOrderIds = dfOrderToTemplateMap.keySet();

		List<DF_Order__c> dfOrders = getDFOrders(dfOrderIds);

		for (DF_Order__c dfOrder : dfOrders) {
			String dfOrderId = dfOrder.Id;
			Messaging.SingleEmailMessage mail = null;
			if (!dfOrder.CreatedBy.UserType.equalsIgnoreCase(AUTOMATED_PROCESS_USER_TYPE)) {
				mail = Messaging.renderStoredEmailTemplate(templateMap.get(dfOrderToTemplateMap.get(dfOrderId)), dfOrder.CreatedBy.Id, dfOrderId);
				if (orgWideEmailAddress != null) {
					mail.setOrgWideEmailAddressId(orgWideEmailAddress.Id);
				}
				mail.setSaveAsActivity(false);
			}
			emailsToSend.add(mail);
		}
		return emailsToSend;
	}

	/**
	 * Send emails to account's contact matirix for bulk upload completed
	 *
	 * @param      recordId  => urrent supported SObject type is DF_Quote__c. Can add suppor to more SObject types when it is needed	 
	 * @return     Integer => Count of email being sent
	 */
	public static Integer sendBulkUploadCompletedEmailToAccountContactMatrix(Id recordId) {

		String accountId;
		String bundleId;
		List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage> ();
		orgWideEmailAddress = getOrgWideEmailAddress(ORDER_EMAIL_SETTING.Sender_Address__c);

		if (String.isNotBlank(recordId))
		{
			// Use generic record ID parameter then detect the SObject type
			if (recordId.getSObjectType().getDescribe().getName().equals(DF_Constants.QUOTE_OBJECT)) {
				DF_Quote__c dfQuote =
				[
				SELECT Id, Opportunity_Bundle__c, Opportunity_Bundle__r.Account__c
				FROM DF_Quote__c
				WHERE Id = : recordId
				];

				bundleId = dfQuote.Opportunity_Bundle__c;
				accountId = dfQuote.Opportunity_Bundle__r.Account__c;
			}
		}

		emailsToSend = getEmailsToBeSentToAccountContactMatrix(accountId, bundleId, DF_Constants.TEMPLATE_ORDER_BULK_UPLOAD_PROCESSING_COMPLETE);
		DF_AS_GlobalUtility.sendEmailList(emailsToSend);
		return emailsToSend.size();
	}

	/**
	 * Send emails to account's contact matirix
	 *
	 * @param      accountId       => To get contact matrix
	 * @param      targetObjectId  => The record from which the Email Template will probably draw most of its merge fields
	 * @return     List<Messaging.SingleEmailMessage> => List of email objects
	 */
	public static List<Messaging.SingleEmailMessage> getEmailsToBeSentToAccountContactMatrix(String accountId, String targetObjectId, String emailTemplateName) {
		String methodName = 'getEmailsToBeSentToAccountContactMatrix';
		List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage> ();
		System.debug('@@@ ORDER_EMAIL_SETTING: ' + ORDER_EMAIL_SETTING);

		// Check if contact matrix email notification function is turned off
		if (ORDER_EMAIL_SETTING.Turn_Off_Notification_Emails__c) {
			String payload = 'Email delivery to Contact Matrix is disabled for EE Orders. No email is being sent.';
			GlobalUtility.logMessage(ERROR, DF_OrderEmailService.class.getName(), methodName, null, null, null, payload, null, 0);
			System.debug(payload);
			return emailsToSend;
		}

		// Only send notofication emails when accountId and bundleId are both not blank
		if (String.isBlank(accountId) || String.isBlank(targetObjectId)) {
			String payload = 'Bundle ID or Account ID not found. No email is being sent.';
			GlobalUtility.logMessage(ERROR, DF_OrderEmailService.class.getName(), methodName, null, null, null, payload, null, 0);
			System.debug(payload);
		}
		else {
			String emailTemplateId = DF_AS_GlobalUtility.getEmailTemplateId(emailTemplateName);

			try {
				randomContact = getRandomContact();

				if (String.isNotBlank(accountId)) {
					List<String> rspAddresses = new List<String> ();
					List<String> nbnAddresses = new List<String> ();
					Messaging.SingleEmailMessage nbnEmailToSend = new Messaging.SingleEmailMessage();
					Messaging.SingleEmailMessage rspEmailToSend = new Messaging.SingleEmailMessage();

					// For the Account >> Get the contact matrix
					DF_AS_GlobalUtility.getAccountTeamMembersEmailList(accountId, ORDER_EMAIL_SETTING, rspAddresses, nbnAddresses);
					System.debug('@@@ rspAddresses: ' + rspAddresses);
					System.debug('@@@ nbnAddresses: ' + nbnAddresses);

					if (rspAddresses != null && !rspAddresses.isEmpty()) {
						rspEmailToSend = buildEmail(rspAddresses, emailTemplateId, randomContact.id, targetObjectId);
						rspEmailToSend.setTreatTargetObjectAsRecipient(false);
						emailsToSend.add(rspEmailToSend);
					}

					if (nbnAddresses != null && !nbnAddresses.isEmpty()) {
						nbnEmailToSend = buildEmail(nbnAddresses, emailTemplateId, randomContact.id, targetObjectId);
						nbnEmailToSend.setTreatTargetObjectAsRecipient(false);
						emailsToSend.add(nbnEmailToSend);
					}
				}
			} catch(Exception e) {
				GlobalUtility.logMessage(ERROR, DF_OrderEmailService.class.getName(), methodName, null, null, e.getMessage(), null, e, 0);
			}
		}
		return emailsToSend;
	}

	/**
	 * Get the list of emails to be sent
	 *
	 * @param      dfOrderToTemplateMap         Map of Order and Template names
	 *
	 * @return     List<Messaging.SingleEmailMessage>
	 */
	public static List<Messaging.SingleEmailMessage> getNotificationEmailsToSend(Map<String, String> dfOrderToTemplateMap,
	                                                                             Map<String, String> templateMap,
	                                                                             DF_Email_Setting__mdt dfOrderEmailSetting) {
		List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage> ();
		Map<String, String> dfOrderAccountMap = null;
		Set<String> dfOrderIds = new Set<String> ();
		Set<String> accountIds = new Set<String> ();
		Map<String, List<Team_Member__c>> accountTeamMembers = new Map<String, List<Team_Member__c>> ();
		List<Messaging.SingleEmailMessage> rspEmailsToSend = new List<Messaging.SingleEmailMessage> ();
		Map<String, Messaging.SingleEmailMessage> accountIdNbnEmails = new Map<String, Messaging.SingleEmailMessage> ();
		Map<String, String> accountIdRspEmails = new Map<String, String> ();
		Map<String, String> accountIdName = new Map<String, String> ();
		Map<String, String> rspEmailIdAccountName = new Map<String, String> ();
		Map<String, String> nbnRspEmailIds = new Map<String, String> ();
		String accountName = null;
		String rspEmailIds = null;
		String nbnEmailIds = null;

		try {

			if (dfOrderEmailSetting != null) {

				randomContact = getRandomContact();

				// Get DF Order - Account Map
				dfOrderAccountMap = getDFOrderAccountMap(dfOrderToTemplateMap.keySet());
				System.debug('@@@ dfOrderAccountMap: ' + dfOrderAccountMap);

				// Get DF Order Ids
				dfOrderIds = dfOrderAccountMap.keySet();

				// Get Account Ids
				accountIds.addAll(dfOrderAccountMap.values());

				// Prepare Account - Contact Matrix map
				accountTeamMembers = getAccountContactMatrixMap(accountIds, dfOrderEmailSetting);
				System.debug('@@@ accountTeamMembers: ' + accountTeamMembers);

				// Loop over every order
				for (String dfOrderId : dfOrderIds) {

					// Get the corresponding Account Id
					String accountId = dfOrderAccountMap.get(dfOrderId);

					if (String.isNotBlank(accountId)) {

						// For the Account >> Get the contact matrix
						List<Team_Member__c> teamMembers = accountTeamMembers.get(accountId);

						if (teamMembers != null && !teamMembers.isEmpty()) {

							// Get all the Contact matrix details
							Set<String> uniqueRspAddresses = new Set<String> ();
							Set<String> uniqueNbnAddresses = new Set<String> ();
							List<String> rspAddresses = new List<String> ();
							List<String> nbnAddresses = new List<String> ();
							Messaging.SingleEmailMessage nbnEmailToSend = new Messaging.SingleEmailMessage();

							for (Team_Member__c teamMember : teamMembers) {

								Id customerContact = teamMember.Customer_Contact__c;
								Id nbnContact = teamMember.NBN_Contact__c;
								String customerInbox = teamMember.Customer_Inbox__c;
								String nbnInbox = teamMember.NBN_Inbox__c;
								String customerEmail = teamMember.Customer_Contact_Email__c;
								String nbnEmail = teamMember.NBN_Contact_Email__c;
								Boolean customerOptedOut = teamMember.Customer_Contact__r.HasOptedOutOfEmail;
								Boolean nbnOptedOut = teamMember.NBN_Contact__r.HasOptedOutOfEmail;
								accountName = teamMember.Account_Name__c;

								// If any email is present, then continue
								if (customerContact != null
								    || String.isNotBlank(customerInbox)
								    || nbnContact != null
								    || String.isNotBlank(nbnInbox)) {
									if (customerContact != null && !customerOptedOut) {
										if (customerEmail != null && isEmailValid(customerEmail)) {
											uniqueRspAddresses.add(customerEmail);
										}
									}

									if (customerInbox != null) {
										rspAddresses = getAddresses(customerInbox);
									}

									if (!rspAddresses.isEmpty()) {
										uniqueRspAddresses.addAll(rspAddresses);
									}

									if (nbnContact != null && !nbnOptedOut) {
										if (nbnEmail != null && isEmailValid(nbnEmail)) {
											uniqueNbnAddresses.add(nbnEmail);
										}
									}

									if (nbnInbox != null) {
										nbnAddresses = getAddresses(nbnInbox);
									}

									if (!nbnAddresses.isEmpty()) {
										uniqueNbnAddresses.addAll(nbnAddresses);
									}

									rspAddresses = new List<String> ();
									if (!uniqueRspAddresses.isEmpty()) {
										rspAddresses.addAll(uniqueRspAddresses);
									}

									nbnAddresses = new List<String> ();
									if (!uniqueNbnAddresses.isEmpty()) {
										nbnAddresses.addAll(uniqueNbnAddresses);
									}
								}
							}

							if (rspAddresses != null && !rspAddresses.isEmpty()) {
								rspEmailsToSend.add(buildEmail(rspAddresses, templateMap.get(dfOrderToTemplateMap.get(dfOrderId)), randomContact.id, dfOrderId));
								rspEmailIds = String.join(rspAddresses, '; ');
								accountIdRspEmails.put(accountId, rspEmailIds);
							}

							if (nbnAddresses != null && !nbnAddresses.isEmpty()) {
								nbnEmailToSend = buildEmail(nbnAddresses, templateMap.get(dfOrderToTemplateMap.get(dfOrderId)), randomContact.id, dfOrderId);
								nbnEmailIds = String.join(nbnAddresses, '; ');
								if (rspAddresses != null && !rspAddresses.isEmpty()) {
									accountIdNbnEmails.put(accountId, nbnEmailToSend);
									accountIdName.put(accountId, accountName);
								}
							}
						}
					}
				}
			}
			if (rspEmailsToSend != null && !rspEmailsToSend.isEmpty()) {
				// Send the emails in a transaction, then roll it back
				Savepoint sp = Database.setSavepoint();
				Messaging.sendEmail(rspEmailsToSend); // Dummy email send
				Database.rollback(sp); // Email will not send as it is rolled Back
				emailsToSend.addAll(rspEmailsToSend);
			}

			if (accountIdNbnEmails != null && !accountIdNbnEmails.isEmpty()) {
				accountIds = accountIdNbnEmails.keySet();
				List<Messaging.SingleEmailMessage> nbnEmailsToSend = accountIdNbnEmails.values();
				if (nbnEmailsToSend != null && !nbnEmailsToSend.isEmpty()) {
					// Send the emails in a transaction, then roll it back
					Savepoint sp = Database.setSavepoint();
					Messaging.sendEmail(nbnEmailsToSend); // Dummy email send
					Database.rollback(sp); // Email will not send as it is rolled Back
				}

				List<Messaging.SingleEmailMessage> modifiedNbnEmailsToSend = new List<Messaging.SingleEmailMessage> ();
				for (String accountId : accountIds) {
					if (accountIdRspEmails.containsKey(accountId)) {
						Messaging.SingleEmailMessage nbnEmail = accountIdNbnEmails.get(accountId);
						String htmlBody = nbnEmail.getHTMLBody();
						String plainBody = nbnEmail.getPlainTextBody();
						String additionalText = String.format(Label.DF_NBN_Email, new List<String> { accountIdName.get(accountId), accountIdRspEmails.get(accountId) });
						String htmlAdditionalText = '<b>Note: </b>' + additionalText + '<br><br>';
						String plainAdditionalText = additionalText + '\n\n';
						htmlBody = htmlAdditionalText + htmlBody;
						plainBody = plainAdditionalText + plainBody;
						nbnEmail.setHTMLBody(htmlBody);
						nbnEmail.setPlainTextBody(plainBody);
						modifiedNbnEmailsToSend.add(nbnEmail);
					}
				}
				emailsToSend.addAll(modifiedNbnEmailsToSend);
			}
		} catch(Exception e) {
			GlobalUtility.logMessage(ERROR, DF_OrderEmailService.class.getName(), 'sendEmail', null, null, e.getMessage(), null, e, 0);
		}

		return emailsToSend;
	}

	/**
	 * Build the email
	 *
	 * @param      toAddresses    List of toAddresses
	 * @param      templateId    templateId
	 * @param      contactId    Contact Id
	 * @param      whatId    What Id
	 *
	 * @return     Email
	 */
	public static Messaging.SingleEmailMessage buildEmail(List<String> toAddresses, String templateId, String contactId, String whatId) {
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		// Set From
		if (orgWideEmailAddress != null) {
			mail.setOrgWideEmailAddressId(orgWideEmailAddress.Id);
		}

		// Set To
		mail.setToAddresses(toAddresses);		
		// Set Body
		mail.setTemplateID(templateId);
		mail.setTargetObjectId(contactId);
		mail.setWhatId(whatId);
		mail.setSaveAsActivity(false);

		return mail;
	}

	/**
	 * Get the Incident details
	 *
	 * @param      dfOrderIds    Set of Orders
	 *
	 * @return     List of Order details
	 */
	public static List<DF_Order__c> getDFOrders(Set<String> dfOrderIds) {
		List<DF_Order__c> dfOrders = null;
		if (dfOrderIds != null && !dfOrderIds.isEmpty()) {
			dfOrders = [SELECT Id, RecordType.DeveloperName, CreatedBy.Id, CreatedBy.UserType
			            FROM DF_Order__c WHERE Id IN :dfOrderIds];
		}
		return dfOrders;
	}


	/**
	 * Get the DF Order - Account Map
	 *
	 * @param      orderIds    Set of Order Ids
	 *
	 * @return     Map of Df Order Id - Account Id
	 */
	public static Map<String, String> getDFOrderAccountMap(Set<String> orderIds) {
		List<DF_Order__c> dfOrders = null;
		Map<String, String> orderAccountMap = null;
		if (orderIds != null && !orderIds.isEmpty()) {
			dfOrders = [SELECT Id, Opportunity_Bundle__r.Account__c FROM DF_Order__c WHERE Id IN :orderIds];
			if (!dfOrders.isEmpty()) {
				orderAccountMap = new Map<String, String> ();
				for (DF_Order__c dfOrder : dfOrders) {
					orderAccountMap.put(dfOrder.Id, dfOrder.Opportunity_Bundle__r.Account__c);
				}
			}
		}

		return orderAccountMap;
	}

	/**
	 * Get the Account - Contact Matrix Map
	 *
	 * @param      accountIds    Set of Account Ids
	 *
	 * @return     Map of Account Id - Contact Matrix Id
	 */
	public static Map<String, List<Team_Member__c>> getAccountContactMatrixMap(Set<String> accountIds, DF_Email_Setting__mdt dfOrderEmailSetting) {
		Map<String, List<Team_Member__c>> accountContactMatrix = null;
		List<Team_Member__c> contactMatrix = new List<Team_Member__c> ();

		if (dfOrderEmailSetting != null) {
			if (accountIds != null && !accountIds.isEmpty()) {
				List<Team_Member__c> teamMembers = [SELECT Id, Account__c,
				                                    Account_Name__c,
				                                    Customer_Contact__c,
				                                    Customer_Contact__r.HasOptedOutOfEmail,
				                                    Customer_Contact_Email__c,
				                                    Customer_Inbox__c,
				                                    NBN_Contact__c,
				                                    NBN_Contact__r.HasOptedOutOfEmail,
				                                    NBN_Contact_Email__c,
				                                    NBN_Inbox__c
				                                    FROM Team_Member__c
				                                    WHERE Account__c
				                                    IN :accountIds
				                                    AND Role__c = :dfOrderEmailSetting.Contact_Matrix_Role__c];
				if (teamMembers != null && !teamMembers.isEmpty()) {
					accountContactMatrix = new Map<String, List<Team_Member__c>> ();

					for (Team_Member__c teamMember : teamMembers) {
						String accountId = teamMember.Account__c;
						if (accountContactMatrix.containsKey(accountId)) {
							contactMatrix = accountContactMatrix.get(accountId);
						} else {
							contactMatrix = new List<Team_Member__c> ();
						}
						contactMatrix.add(teamMember);
						accountContactMatrix.put(accountId, contactMatrix);
					}
				}
			}
		}
		return accountContactMatrix;
	}

	/**
	 * Validate the input email for format
	 *
	 * @param      accountIds    Set of Account Ids
	 *
	 * @return     Boolean
	 */
	public static Boolean isEmailValid(String email) {
		Boolean isValid = true;
		String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
		Pattern MyPattern = Pattern.compile(emailRegex);
		Matcher MyMatcher = MyPattern.matcher(email);

		if (!MyMatcher.matches())
		isValid = false;
		return isValid;
	}

	/**
	 * Validate the input and split it into separate email addresses
	 *
	 * @param      input    Email Text
	 *
	 * @return     List of email addresses
	 */
	public static List<String> getAddresses(String input) {
		List<String> addresses = new List<String> ();
		Set<String> uniqueAddresses = null;
		if (String.isNotBlank(input)) {
			uniqueAddresses = new Set<String> ();
			if (input.containsWhitespace()) {
				input = input.replaceAll('\\s+', '');
			}
			if (input.contains(',')) {
				List<String> emailIds = input.split(',');
				for (String emailId : emailIds) {
					if (isEmailValid(emailId)) {
						uniqueAddresses.add(emailId);
					}
				}
			} else {
				if (isEmailValid(input)) {
					uniqueAddresses.add(input);
				}
			}
		}
		if (uniqueAddresses != null && !uniqueAddresses.isEmpty()) {
			addresses = new List<String> ();
			addresses.addAll(uniqueAddresses);
		}
		return addresses;
	}

	/**
	 * Get the Email Templates
	 *
	 * @param      dfOrderToTemplateMap
	 *
	 * @return     Map of email templates
	 */
	public static Map<String, String> getEmailTemplates(Map<String, String> dfOrderToTemplateMap) {
		Set<String> templateNames = new Set<String> ();
		Map<String, String> templateMap = new Map<String, String> ();
		List<EmailTemplate> templates = new List<EmailTemplate> ();
		templateNames.addAll(dfOrderToTemplateMap.values());
		templates = [SELECT Id, Name FROM EmailTemplate WHERE Name IN :templateNames];
		for (EmailTemplate template : templates) {
			templateMap.put(template.Name, template.Id);
		}
		return templateMap;
	}

	/**
	 * Get a random contact
	 *
	 * @return     any random contact
	 */
	public static Contact getRandomContact() {
		Contact contact = null;
		contact = [SELECT Id, Email FROM Contact WHERE email != null LIMIT 1];
		return contact;
	}

	/**
	 * Validate the input and split it into separate email addresses
	 *
	 * @param      name
	 *
	 * @return     Custom Metadata
	 */
	public static DF_Email_Setting__mdt getDFEmailSetting(String name) {
		List<DF_Email_Setting__mdt> dfEmailSettings =
		[
		SELECT
		Contact_Matrix_Role__c,
		Sender_Address__c,
		Turn_Off_Notification_Emails__c,
		Turn_Off_Watch_Me_Emails__c,
		Turn_Off_Creator_Emails__c
		FROM DF_Email_Setting__mdt
		WHERE DeveloperName = : name
		];

		DF_Email_Setting__mdt dfOrderEmailSetting = null;

		if (dfEmailSettings != null && dfEmailSettings.size() == 1) {
			dfOrderEmailSetting = dfEmailSettings.get(0);
		}

		return dfOrderEmailSetting;
	}

	/**
	 * Get the OrgwideEmailAddress
	 *
	 * @param      address
	 *
	 * @return     OrgwideEmailAddress
	 */
	public static OrgWideEmailAddress getOrgWideEmailAddress(String address) {

		List<OrgWideEmailAddress> emailAddresses = null;
		OrgWideEmailAddress emailAddress = null;

		if (String.isNotEmpty(address)) {
			emailAddresses = [SELECT Id FROM OrgWideEmailAddress WHERE Address = :address];
		}

		if (emailAddresses != null && !emailAddresses.isEmpty()) {
			emailAddress = emailAddresses.get(0);
		}

		return emailAddress;
	}
}