/*------------------------------------------------------------------------
Author:        Bharath Kumar
Company:       Contractor
Description:   A test class created to test DF_OrderEmailService methods               

History
22/10/2018      Bharath Kumar     Creation
--------------------------------------------------------------------------*/

@isTest
public class DF_OrderEmailServiceTest {
	
	@testSetup
	static void setupTestData() {

		String DF_ORDER_CONTACT_MATRIX_ROLE = 'Operational - Activations';
		String STATUS_ACTIVE = 'Active';

		// create account1 record
        Account account1 = new Account();
        account1.Name = 'Telstra';
        account1.Access_Seeker_ID__c = 'AS123';
        insert account1;

        Account account2 = new Account();
        account2.Name = 'Optus';
        account2.Access_Seeker_ID__c = 'AS456';
        insert account2;

        Account account3 = new Account();
        account3.Name = 'Vodafone';
        account3.Access_Seeker_ID__c = 'AS789';
        insert account3;

        Account account4 = new Account();
        account4.Name = 'nbn';
        account4.Access_Seeker_ID__c = 'AS7098';
        insert account4;

        Contact contact1 = new Contact();
        contact1.FirstName = 'FN1';
        contact1.LastName = 'LN1';
        contact1.Account = account1;
        contact1.Email = '123@mailinator.com';
        contact1.Status__c = STATUS_ACTIVE;
        contact1.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact1.AccountId=account1.Id;
        insert contact1;

        Contact contact2 = new Contact();
        contact2.FirstName = 'NBN FN1';
        contact2.LastName = 'NBN LN1';
        contact2.Account = account4;
        contact2.Email = '456@mailinator.com';
        contact2.Status__c = STATUS_ACTIVE;
        contact2.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact2.AccountId=account4.Id;
        insert contact2;

        Contact contact3 = new Contact();
        contact3.FirstName = 'FN2';
        contact3.LastName = 'LN2';
        contact3.Account = account2;
        contact3.Email = 'a@mailinator.com';
        contact3.Status__c = STATUS_ACTIVE;
        contact3.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact3.AccountId=account2.Id;
        insert contact3;

        Contact contact4 = new Contact();
        contact4.FirstName = 'NBN FN2';
        contact4.LastName = 'NBN LN2';
        contact4.Account = account4;
        contact4.Email = 'b@mailinator.com';
        contact4.Status__c = STATUS_ACTIVE;
        contact4.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact4.AccountId=account4.Id;
        insert contact4;

        Contact contact5 = new Contact();
        contact5.FirstName = 'FN3';
        contact5.LastName = 'LN3';
        contact5.Account = account3;
        contact5.Email = 'c@mailinator.com';
        contact5.Status__c = STATUS_ACTIVE;
        contact5.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact5.AccountId=account3.Id;
        insert contact5;

        Contact contact6 = new Contact();
        contact6.FirstName = 'FN3';
        contact6.LastName = 'LN3';
        contact6.Account = account1;
        contact6.Email = '6@mailinator.com';
        contact6.Status__c = STATUS_ACTIVE;
        contact6.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact6.AccountId=account1.Id;
        insert contact6;

        Contact contact7 = new Contact();
        contact7.FirstName = 'FN4';
        contact7.LastName = 'LN4';
        contact7.Account = account2;
        contact7.Email = '7@mailinator.com';
        contact7.Status__c = STATUS_ACTIVE;
        contact7.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact7.AccountId=account2.Id;
        insert contact7;

        Profile commProfile = [SELECT Id FROM Profile WHERE Name = 'DF Partner User'];

        User dfUser = new User();
        dfUser.Alias = 'test123';
        dfUser.Email = 'test123@noemail.com';
        dfUser.Emailencodingkey = 'ISO-8859-1';
        dfUser.Lastname = 'Testing';
        dfUser.Languagelocalekey = 'en_US';
        dfUser.Localesidkey = 'en_AU';
        dfUser.Profileid = commProfile.Id;
        dfUser.IsActive = true;
        dfUser.ContactId = contact1.Id;
        dfUser.Timezonesidkey = 'Australia/Sydney';
        dfUser.Username = 'tester@noemail.com.test';
        dfUser.Contact = contact1;
        insert dfUser;

        Team_Member__c teamMember1 = new Team_Member__c();
        teamMember1.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember1.Account__c = account1.Id;
        teamMember1.Customer_Contact__c = contact1.Id;
        teamMember1.Customer_Inbox__c = null;
        teamMember1.NBN_Contact__c = contact2.Id;
        teamMember1.NBN_Inbox__c = null;
        teamMember1.Role__c = DF_ORDER_CONTACT_MATRIX_ROLE;        
        insert teamMember1;

        Team_Member__c teamMember2 = new Team_Member__c();
        teamMember2.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember2.Account__c = account1.Id;
        teamMember2.Customer_Contact__c = null;
        teamMember2.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember2.NBN_Contact__c = null;
        teamMember2.NBN_Inbox__c = 'f@mailinator.com';
        teamMember2.Role__c = DF_ORDER_CONTACT_MATRIX_ROLE;        
        insert teamMember2;

        Team_Member__c teamMember3 = new Team_Member__c();
        teamMember3.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember3.Account__c = account1.Id;
        teamMember3.Customer_Contact__c = null;
        teamMember3.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember3.NBN_Contact__c = null;
        teamMember3.NBN_Inbox__c = 'f@mailinator.com';
        teamMember3.Role__c = DF_ORDER_CONTACT_MATRIX_ROLE;        
        insert teamMember3;

        Team_Member__c teamMember4 = new Team_Member__c();
        teamMember4.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember4.Account__c = account1.Id;
        teamMember4.Customer_Contact__c = null;
        teamMember4.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember4.NBN_Contact__c = null;
        teamMember4.NBN_Inbox__c = 'f@mailinator.com';
        teamMember4.Role__c = DF_ORDER_CONTACT_MATRIX_ROLE;        
        insert teamMember4;

        Team_Member__c teamMember5 = new Team_Member__c();
        teamMember5.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember5.Account__c = account2.Id;
        teamMember5.Customer_Contact__c = contact3.Id;
        teamMember5.Customer_Inbox__c = null;
        teamMember5.NBN_Contact__c = contact4.Id;
        teamMember5.NBN_Inbox__c = null;
        teamMember5.Role__c = DF_ORDER_CONTACT_MATRIX_ROLE;        
        insert teamMember5;

        Team_Member__c teamMember6 = new Team_Member__c();
        teamMember6.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember6.Account__c = account2.Id;
        teamMember6.Customer_Contact__c = null;
        teamMember6.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember6.NBN_Contact__c = null;
        teamMember6.NBN_Inbox__c = 'f@mailinator.com';
        teamMember6.Role__c = DF_ORDER_CONTACT_MATRIX_ROLE;        
        insert teamMember6;

        Team_Member__c teamMember7 = new Team_Member__c();
        teamMember7.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember7.Account__c = account2.Id;
        teamMember7.Customer_Contact__c = null;
        teamMember7.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember7.NBN_Contact__c = null;
        teamMember7.NBN_Inbox__c = 'f@mailinator.com';
        teamMember7.Role__c = DF_ORDER_CONTACT_MATRIX_ROLE;        
        insert teamMember7;

        Team_Member__c teamMember8 = new Team_Member__c();
        teamMember8.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember8.Account__c = account2.Id;
        teamMember8.Customer_Contact__c = null;
        teamMember8.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember8.NBN_Contact__c = null;
        teamMember8.NBN_Inbox__c = 'f@mailinator.com';
        teamMember8.Role__c = DF_ORDER_CONTACT_MATRIX_ROLE;        
        insert teamMember8;

        Team_Member__c teamMember9 = new Team_Member__c();
        teamMember9.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember9.Account__c = account3.Id;
        teamMember9.Customer_Contact__c = contact5.Id;
        teamMember9.Customer_Inbox__c = 'abc@mailinator.com, d@mailinator.com,e@mailinator.com';
        teamMember9.NBN_Contact__c = contact4.Id;
        teamMember9.NBN_Inbox__c = 'f@mailinator.com';
        teamMember9.Role__c = DF_ORDER_CONTACT_MATRIX_ROLE;        
        insert teamMember9;

        Team_Member__c teamMember10 = new Team_Member__c();
        teamMember10.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember10.Account__c = account1.Id;
        teamMember10.Customer_Contact__c = contact6.Id;
        teamMember10.Customer_Inbox__c = null;
        teamMember10.NBN_Contact__c = contact4.Id;
        teamMember10.NBN_Inbox__c = null;
        teamMember10.Role__c = DF_ORDER_CONTACT_MATRIX_ROLE;        
        insert teamMember10;

        Team_Member__c teamMember11 = new Team_Member__c();
        teamMember11.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember11.Account__c = account2.Id;
        teamMember11.Customer_Contact__c = contact7.Id;
        teamMember11.Customer_Inbox__c = null;
        teamMember11.NBN_Contact__c = contact4.Id;
        teamMember11.NBN_Inbox__c = null;
        teamMember11.Role__c = DF_ORDER_CONTACT_MATRIX_ROLE;        
        insert teamMember11;

        System.runAs(dfUser) {
        	PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'DF_BC_Customer_Plus_Permission'];
            insert new PermissionSetAssignment(AssigneeId = dfUser.id, PermissionSetId = ps.Id );   
            
            List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
            
            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';

            // Create OpptyBundle
            DF_Opportunity_Bundle__c opptyBundle1 = DF_TestData.createOpportunityBundle(dfUser.Contact.AccountId);
            insert opptyBundle1;

            // Create Oppty
            Opportunity oppty1 = DF_TestData.createOpportunity('LOC111111111111');
            oppty1.Opportunity_Bundle__c = opptyBundle1.Id;
            insert oppty1;

            // Create DFQuote recs
            DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty1.Id, opptyBundle1.Id, 1000, 'Green');         
            dfQuote1.Proceed_to_Pricing__c = true;
            dfQuoteList.add(dfQuote1);

            insert dfQuoteList;         

            DF_Order__c dfOrder1 = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle1.Id );
            dfOrder1.Heritage_Site__c = 'Yes';
            dfOrder1.Induction_Required__c = 'Yes';
            dfOrder1.Security_Required__c = 'Yes';
            dfOrder1.Site_Notes__c = 'Site Notes';
            dfOrder1.Trading_Name__c = 'Trader Joes TEST SITE';
            dfOrder1.BPI_Id__c = '123';
            Insert dfOrder1;

            DF_Order__c dfOrder2 = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle1.Id );
            dfOrder2.Heritage_Site__c = 'Yes';
            dfOrder2.Induction_Required__c = 'Yes';
            dfOrder2.Security_Required__c = 'Yes';
            dfOrder2.Site_Notes__c = 'Site Notes';
            dfOrder2.Trading_Name__c = 'Trader Joes TEST SITE';
            dfOrder2.BPI_Id__c = '456';
            Insert dfOrder2;

            DF_Order__c dfOrder3 = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle1.Id );
            dfOrder3.Heritage_Site__c = 'Yes';
            dfOrder3.Induction_Required__c = 'Yes';
            dfOrder3.Security_Required__c = 'Yes';
            dfOrder3.Site_Notes__c = 'Site Notes';
            dfOrder3.Trading_Name__c = 'Trader Joes TEST SITE';
            dfOrder3.BPI_Id__c = '123';
            Insert dfOrder3;

            DF_Order__c dfOrder4 = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle1.Id );
            dfOrder4.Heritage_Site__c = 'Yes';
            dfOrder4.Induction_Required__c = 'Yes';
            dfOrder4.Security_Required__c = 'Yes';
            dfOrder4.Site_Notes__c = 'Site Notes';
            dfOrder4.Trading_Name__c = 'Trader Joes TEST SITE';
            dfOrder4.BPI_Id__c = '456';
            Insert dfOrder4;
    	}
    }

	@isTest static void testsendBulkUploadCompletedEmailToAccountContactMatrix() {
		Test.startTest();		
		DF_Quote__c dfQuote = [SELECT Id FROM DF_Quote__c LIMIT 1];
		
        DF_Email_Setting__mdt dfOrderEmailSetting = DF_OrderEmailService.getDFEmailSetting('DF_Order');
        System.assertNotEquals(dfOrderEmailSetting, null);
		
        OrgWideEmailAddress emailAddress = DF_OrderEmailService.getOrgWideEmailAddress(dfOrderEmailSetting.Sender_Address__c);
        System.assertNotEquals(emailAddress, null);

        DF_OrderEmailService.orgWideEmailAddress = emailAddress;
		Integer emailCount = DF_OrderEmailService.sendBulkUploadCompletedEmailToAccountContactMatrix(dfQuote.Id);
		System.assertEquals(2, emailCount);
		System.debug('@@@ emailCount:' + emailCount);

		Test.stopTest();	
	}
	
	@isTest
	public static void testSendEmail() {
		Test.startTest();

        DF_Email_Setting__mdt dfOrderEmailSetting = DF_OrderEmailService.getDFEmailSetting('DF_Order');
        System.assertNotEquals(dfOrderEmailSetting, null);

        OrgWideEmailAddress emailAddress = DF_OrderEmailService.getOrgWideEmailAddress(dfOrderEmailSetting.Sender_Address__c);
        System.assertNotEquals(emailAddress, null);

        DF_OrderEmailService.orgWideEmailAddress = emailAddress;

		List<DF_Order__c> dfOrders = DF_OrderEmailService.getDFOrders(getDFOrders().keySet());
		System.assertNotEquals(dfOrders, null);
		System.assertEquals(4, dfOrders.size());

		Integer emailCount = DF_OrderEmailService.sendEmail(getDFOrderToTemplateMap());
		System.assertEquals(9, emailCount);
		System.debug('@@@ emailCount:' + emailCount);
		
		Test.stopTest();
	}
			
    @isTest
	public static void testEmailsToSend() {
		Test.startTest();

        DF_Email_Setting__mdt dfOrderEmailSetting = DF_OrderEmailService.getDFEmailSetting('DF_Order');
        System.assertNotEquals(dfOrderEmailSetting, null);

        OrgWideEmailAddress emailAddress = DF_OrderEmailService.getOrgWideEmailAddress(dfOrderEmailSetting.Sender_Address__c);
        System.assertNotEquals(emailAddress, null);

        DF_OrderEmailService.orgWideEmailAddress = emailAddress;

		List<DF_Order__c> dfOrders = DF_OrderEmailService.getDFOrders(getDFOrders().keySet());
		System.assertNotEquals(dfOrders, null);
		System.assertEquals(4, dfOrders.size());

		List<Messaging.SingleEmailMessage> emailsToSend = DF_OrderEmailService.getEmailsToSend(getDFOrderToTemplateMap(), dfOrderEmailSetting);
		System.assertNotEquals(emailsToSend, null);
		System.assertEquals(9, emailsToSend.size());
		Test.stopTest();
	}

    @isTest static void testGetAddresses() {
		String address = '1@gmail.com, 2@gmail.com,3@gmail.com';
		List<String> toAddresses = DF_OrderEmailService.getAddresses(address);
		System.assertNotEquals(toAddresses, null);
		System.assertEquals(3, toAddresses.size());
	}

	@isTest static void testIsEmailValid() {
		String email = '1@gmail.com';
		Boolean isValid = DF_OrderEmailService.isEmailValid(email);
		System.assertEquals(isValid, true);
	}

	public static Map<String, DF_Order__c> getDFOrders() {
		Set<String> products = new Set<String>{'123', '456'};
		List<DF_Order__c> dfOrders = [SELECT Id, Opportunity_Bundle__r.Account__c FROM DF_Order__c WHERE BPI_Id__c IN :products];
		Map<String, DF_Order__c> dfOrderMap = new Map<String, DF_Order__c>();
		for(DF_Order__c dfOrder : dfOrders) {
			dfOrderMap.put(dfOrder.Id, dfOrder);
		}
		return dfOrderMap;
	}

	public static Map<String, String> getDFOrderToTemplateMap() {
		List<DF_Order__c> dfOrders = DF_OrderEmailService.getDFOrders(getDFOrders().keySet());
		Map<String, String> dfOrderToTemplateMap = new Map<String, String>();
		String TEMPLATE_ORDER_STATUS_ACCEPTED = 'EE Order Accepted';
		for(DF_Order__c dfOrder : dfOrders) {
			dfOrderToTemplateMap.put(dfOrder.Id, TEMPLATE_ORDER_STATUS_ACCEPTED);
		}
		System.debug('@@@ dfOrderToTemplateMap: ' + dfOrderToTemplateMap);
		return dfOrderToTemplateMap;
	}
}