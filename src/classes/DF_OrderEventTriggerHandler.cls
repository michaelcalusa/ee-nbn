public class DF_OrderEventTriggerHandler {
    
    public static void eventTypesIdentification(list<DF_Order_Event__e> ordEvents){     
     
       List<DF_Order_Event__e> appianCalloutList = new List<DF_Order_Event__e>();
       List<DF_Order_Event__e> sfProcessingList = new List<DF_Order_Event__e>();
       List<DF_Order_Event__e> ordAccProcessingList = new List<DF_Order_Event__e>();
       List<DF_Order_Event__e> ordCompleteProcessingList = new List<DF_Order_Event__e>();
       List<DF_Order_Event__e> ordSchProcessingList = new List<DF_Order_Event__e>();
       List<DF_Order_Event__e> ordConProcessingList = new List<DF_Order_Event__e>();
       List<DF_Order_Event__e> ordSVCProcessingList = new List<DF_Order_Event__e>();
       List<DF_Order_Event__e> ordCancelProcessingList = new List<DF_Order_Event__e>();
        //CPST-5595 able to track cause delays
        List<DF_Order_Event__e> ordActionReqProcessingList = new List<DF_Order_Event__e>();
        List<DF_Order_Event__e> ordActionCompleteProcessingList = new List<DF_Order_Event__e>();
        //CPST-7849 | michaelcalusa@nbnco.com.au | Receive new notification from Appian for Revised Delivery Date
        List<DF_Order_Event__e> ordReschedProcessingList = new List<DF_Order_Event__e>();
        List<DF_Order_Event__e> ordOnSchedProcessingList = new List<DF_Order_Event__e>();

        //CPST-7849 | michaelcalusa@nbnco.com.au | Receive new notification from Appian for Revised Delivery Date
       DF_Order_Settings__mdt ordSetting = [SELECT OrderAckEventCode__c,OrderAckSource__c,OrderAckBatchSize__c,
                                            OrderCreateBatchSize__c,OrderCreateCode__c,OrderCreateSource__c,
                                            OrderAccBatchSize__c,OrderAccEventCode__c,OrderAccSource__c,
                                            OrderCancelBatchSize__c, OrderCancelEventCode__c, OrderCancelSource__c, 
                                            OrderCompleteBatchSize__c,OrderCompleteEventCode__c,
                                            OrderCompleteSource__c, OrderSchEventCode__c, OrderSchBatchSize__c,
                                            OrderSchSource__c, OrderConstructionCode__c, OrderConstructionBatchSize__c,
                                            OrderConstructionSource__c, OrderSVCBatchSize__c, OrderSVCCOMPCode__c,
                                            OrderReschedBatchSize__c,OrderReschedEventCode__c,OrderOnSchedBatchSize__c,
                                            OrderOnSchedEventCode__c,
                                            OrderSVCCOMPSource__c,OrderRspActionCompBatchSize__c,OrderRspActionCompEventCode__c,
                                            OrderRspActionReqdEventCode__c
                                                         FROM   DF_Order_Settings__mdt
                                                         WHERE  DeveloperName = 'DFOrderSettings'];
                 
        for(DF_Order_Event__e oEvent: ordEvents){
          System.debug('Actual event is :' + oEvent);
          System.debug('ordSetting value  is :' + ordSetting);

            if(ordSetting.OrderCreateCode__c.equalsIgnoreCase(oEvent.Event_Id__c)){                 
                appianCalloutList.add(oEvent);
            }
            else if(ordSetting.OrderAckEventCode__c.equalsIgnoreCase(oEvent.Event_Id__c)){
                sfProcessingList.add(oEvent);
            }
          else if(ordSetting.OrderAccEventCode__c.equalsIgnoreCase(oEvent.Event_Id__c)){
            ordAccProcessingList.add(oEvent);
          }
          else if(ordSetting.OrderCompleteEventCode__c.equalsIgnoreCase(oEvent.Event_Id__c)){
            ordCompleteProcessingList.add(oEvent);
          }
          else if(ordSetting.OrderSchEventCode__c.equalsIgnoreCase(oEvent.Event_Id__c)){
            ordSchProcessingList.add(oEvent);
          }
          else if(ordSetting.OrderConstructionCode__c.equalsIgnoreCase(oEvent.Event_Id__c)){
            ordConProcessingList.add(oEvent);
          }
          else if(ordSetting.OrderSVCCOMPCode__c.equalsIgnoreCase(oEvent.Event_Id__c)){
            ordSVCProcessingList.add(oEvent);
          }
          else if(ordSetting.OrderCancelEventCode__c.equalsIgnoreCase(oEvent.Event_Id__c)) {
            ordCancelProcessingList.add(oEvent);
          //START: CPST-7849 | michaelcalusa@nbnco.com.au | Receive new notification from Appian for Revised Delivery Date
          } else if (ordSetting.OrderReschedEventCode__c.equalsIgnoreCase(oEvent.Event_Id__c)) {
            ordReschedProcessingList.add(oEvent);
          } else if (ordSetting.OrderOnSchedEventCode__c.equalsIgnoreCase(oEvent.Event_Id__c)) {
            ordOnSchedProcessingList.add(oEvent);
          //END: CPST-7849 | michaelcalusa@nbnco.com.au | Receive new notification from Appian for Revised Delivery Date
          } else if(ordSetting.OrderRspActionReqdEventCode__c.equalsIgnoreCase(oEvent.Event_Id__c)) {
              ordActionReqProcessingList.add(oEvent);
          } else if(ordSetting.OrderRspActionCompEventCode__c.equalsIgnoreCase(oEvent.Event_Id__c)) {
              ordActionCompleteProcessingList.add(oEvent);
          }
        }

        if(appianCalloutList.size()>0){
            List<List<DF_Order_Event__e>> resultList = (List<List<DF_Order_Event__e>>)DF_CS_API_Util.spliceBy(appianCalloutList,Integer.valueOf(ordSetting.OrderCreateBatchSize__c));
            system.debug('Salesforce to Appain Event batches size  : ' + resultList.size());      
            for(List<DF_Order_Event__e> evtLst : resultList){
                System.enqueueJob(new DF_OrdertoAppainEventHandler(evtLst));
            }
        }

        else if(sfProcessingList.size()>0){
            List<List<DF_Order_Event__e>> resultList = (List<List<DF_Order_Event__e>>)DF_CS_API_Util.spliceBy(sfProcessingList,Integer.valueOf(ordSetting.OrderAckBatchSize__c));
            system.debug('Appain to Salesforce Event batches size : ' + resultList.size());
            for(List<DF_Order_Event__e> evtLst : resultList){
                System.enqueueJob(new DF_OrderInboundEventHandler(evtLst,ordSetting.OrderAckEventCode__c));
            }
        }
        else if(ordAccProcessingList.size()>0){
          List<List<DF_Order_Event__e>> resultList = (List<List<DF_Order_Event__e>>)DF_CS_API_Util.spliceBy(ordAccProcessingList,Integer.valueOf(ordSetting.OrderAccBatchSize__c));
          system.debug('Appain to Salesforce Event batches size : ' + resultList.size());
          for(List<DF_Order_Event__e> evtLst : resultList){
            System.enqueueJob(new DF_OrderInboundEventHandler(evtLst,ordSetting.OrderAccEventCode__c));
          }
        }
        else if(ordCompleteProcessingList.size()>0){
          List<List<DF_Order_Event__e>> resultList = (List<List<DF_Order_Event__e>>)DF_CS_API_Util.spliceBy(ordCompleteProcessingList,Integer.valueOf(ordSetting.OrderCompleteBatchSize__c));
          system.debug('Appain to Salesforce Event batches size : ' + resultList.size());
          for(List<DF_Order_Event__e> evtLst : resultList){
            System.enqueueJob(new DF_OrderInboundEventHandler(evtLst,ordSetting.OrderCompleteEventCode__c));
          }
        }
        else if(ordSchProcessingList.size()>0){
          List<List<DF_Order_Event__e>> resultList = (List<List<DF_Order_Event__e>>)DF_CS_API_Util.spliceBy(ordSchProcessingList,Integer.valueOf(ordSetting.OrderSchBatchSize__c));
          system.debug('Appain to Salesforce Event batches size : ' + resultList.size());
          for(List<DF_Order_Event__e> evtLst : resultList){
            System.enqueueJob(new DF_OrderInboundEventHandler(evtLst,ordSetting.OrderSchEventCode__c));
          }
        }
        else if(ordConProcessingList.size()>0){
          List<List<DF_Order_Event__e>> resultList = (List<List<DF_Order_Event__e>>)DF_CS_API_Util.spliceBy(ordConProcessingList,Integer.valueOf(ordSetting.OrderConstructionBatchSize__c));
          system.debug('Appain to Salesforce Event batches size : ' + resultList.size());
          for(List<DF_Order_Event__e> evtLst : resultList){
            System.enqueueJob(new DF_OrderInboundEventHandler(evtLst,ordSetting.OrderConstructionCode__c));
          }
        }
        else if(ordSVCProcessingList.size()>0){
          List<List<DF_Order_Event__e>> resultList = (List<List<DF_Order_Event__e>>)DF_CS_API_Util.spliceBy(ordSVCProcessingList,Integer.valueOf(ordSetting.OrderSVCBatchSize__c));
          system.debug('Appain to Salesforce Event batches size : ' + resultList.size());
          for(List<DF_Order_Event__e> evtLst : resultList){
            System.enqueueJob(new DF_OrderInboundEventHandler(evtLst,ordSetting.OrderSVCCOMPCode__c));
          }
        }
        else if(ordCancelProcessingList.size() > 0) {
          List<List<DF_Order_Event__e>> resultList = (List<List<DF_Order_Event__e>>)DF_CS_API_Util.spliceBy(ordCancelProcessingList,Integer.valueOf(ordSetting.OrderCancelBatchSize__c));
          system.debug('Appain to Salesforce Event batches size : ' + resultList.size());
          for(List<DF_Order_Event__e> evtLst : resultList){
            System.enqueueJob(new EE_OrderInflightCancelHandler(evtLst));
          }
        //START: CPST-7849 | michaelcalusa@nbnco.com.au | Receive new notification from Appian for Revised Delivery Date
        } else if(ordReschedProcessingList.size() > 0) {
            List<List<DF_Order_Event__e>> resultList
                = (List<List<DF_Order_Event__e>>)DF_CS_API_Util.spliceBy(ordReschedProcessingList,Integer.valueOf(ordSetting.OrderReschedBatchSize__c));
            system.debug('Appain to Salesforce RDD Event batches size : ' + resultList.size());
            for(List<DF_Order_Event__e> evtLst : resultList){
                System.enqueueJob(new DF_OrderInboundEventHandler(evtLst,ordSetting.OrderReschedEventCode__c));
            }
        } else if(ordOnSchedProcessingList.size() > 0) {
            List<List<DF_Order_Event__e>> resultList
                = (List<List<DF_Order_Event__e>>)DF_CS_API_Util.spliceBy(ordOnSchedProcessingList,Integer.valueOf(ordSetting.OrderOnSchedBatchSize__c));
            system.debug('Appain to Salesforce On Sched Event batches size : ' + resultList.size());
            for(List<DF_Order_Event__e> evtLst : resultList){
                System.enqueueJob(new DF_OrderInboundEventHandler(evtLst,ordSetting.OrderOnSchedEventCode__c));
            }
        //END: CPST-7849 | michaelcalusa@nbnco.com.au | Receive new notification from Appian for Revised Delivery Date
        } else if(ordActionReqProcessingList.size() > 0) {
            List<List<DF_Order_Event__e>> resultList = (List<List<DF_Order_Event__e>>)DF_CS_API_Util.spliceBy(ordActionReqProcessingList,Integer.valueOf(ordSetting.OrderRspActionCompBatchSize__c));
            system.debug('Appain to Salesforce Event batches size : ' + resultList.size());
            for(List<DF_Order_Event__e> evtLst : resultList){
                System.enqueueJob(new DF_OrderInboundEventHandler(evtLst,ordSetting.OrderRspActionReqdEventCode__c));
            }
        } else if(ordActionCompleteProcessingList.size() > 0) {
            List<List<DF_Order_Event__e>> resultList = (List<List<DF_Order_Event__e>>)DF_CS_API_Util.spliceBy(ordActionCompleteProcessingList,Integer.valueOf(ordSetting.OrderRspActionCompBatchSize__c));
            system.debug('Appain to Salesforce Event batches size : ' + resultList.size());
            for(List<DF_Order_Event__e> evtLst : resultList){
                System.enqueueJob(new DF_OrderInboundEventHandler(evtLst,ordSetting.OrderRspActionCompEventCode__c));
            }
        }
    }
    

}