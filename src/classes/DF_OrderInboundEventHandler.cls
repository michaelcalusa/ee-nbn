public class DF_OrderInboundEventHandler implements Queueable{
    //SELECT BPI__c,Name,OVC_Id__c,UNI__c FROM csord__Service__c WHERE  csordtelcoa__Product_Basket__r.cscfga__Opportunity__c = 
    private List<DF_Order_Event__e> lstToProcess;
    private String eventType;


    public DF_OrderInboundEventHandler(List<DF_Order_Event__e> evtLst,String eventTp) {     
        lstToProcess = evtLst;
        eventType = eventTp;
    }

    public void execute(QueueableContext context) {

        //try{

            System.debug('Event Type is : ' + eventType);
            //new fields added
            //CPST-7849 | michaelcalusa@nbnco.com.au | Receive new notification from Appian for Revised Delivery Date
            DF_Order_Settings__mdt ordSetting = [SELECT OrderAckEventCode__c,OrderAckSource__c,OrderAckBatchSize__c,
                                            OrderCreateBatchSize__c,OrderCreateCode__c,OrderCreateSource__c,
                                            OrderAccBatchSize__c,OrderAccEventCode__c,OrderAccSource__c,
                                            OrderCompleteBatchSize__c,OrderCompleteEventCode__c,
                                            OrderCompleteSource__c, OrderSchEventCode__c, OrderSchBatchSize__c,
                                            OrderSchSource__c, OrderConstructionCode__c, OrderConstructionBatchSize__c,
                                            OrderConstructionSource__c, OrderSVCBatchSize__c, OrderSVCCOMPCode__c,
                                            OrderReschedBatchSize__c,OrderReschedEventCode__c,OrderOnSchedBatchSize__c,
                                            OrderOnSchedEventCode__c,
                                            OrderSVCCOMPSource__c,OrderRspActionCompBatchSize__c,OrderRspActionCompEventCode__c,
                                            OrderRspActionReqdEventCode__c
                                            FROM   DF_Order_Settings__mdt
                                            WHERE  DeveloperName = 'DFOrderSettings'];

            set<string> guids = new set<string>();
            

            for(DF_Order_Event__e evt : lstToProcess){
                system.debug('::guid::'+evt.Event_Record_Id__c);
                guids.add(evt.Event_Record_Id__c);
            }
            system.debug('::guids.size()::'+guids.size());
            if(guids.size()>0){
                //New fields added
                //CPST-7849 | michaelcalusa@nbnco.com.au | Receive new notification from Appian for Revised Delivery Date
                List<DF_Order__c> ordLst = [Select id,Order_Id__c,DF_Quote__c,Order_Status__c,Product_Charges_JSON__c,BPI_Id__c,
                                            Site_Survey_Charges_JSON__c,NBN_Commitment_Date__c, UNI_Port__c, Order_Sub_Status__c,
                                            Appian_Notification_Date__c, DF_Quote__r.Opportunity__c, Service_Region__c,Order_SubType__c,
                                            BTD_Id__c, UNI_Id__c, DF_Quote__r.GUID__c,OrderType__c,Revised_Delivery_Date__c,
                                            Notification_Reason__c,Reason_Code__c,Order_Notification_Sub_Status__c
                                            from DF_Order__c
                                            where DF_Quote__r.GUID__c in :guids];


                set<Id> oppIds = new set<Id>(); //used for updating csord__Service__c  items                           
                Map<string, DF_Order__c> guidToOrderMap = new Map<string, DF_Order__c>();

                for(DF_Order__c ord : ordLst){
                    guidToOrderMap.put(ord.DF_Quote__r.GUID__c, ord);
                    oppIds.add(ord.DF_Quote__r.Opportunity__c);
                }
                System.debug('oppIds '+ oppIds);

                //get List of csord_Service__c OVC items to update
                List<csord__Service__c> servicesList = [SELECT id,Name,OVC_Id__c,csordtelcoa__Product_Basket__r.cscfga__Opportunity__c,
                                                                csord__Order__c, csord__Subscription__c,csordtelcoa__Product_Basket__c
                                                        FROM csord__Service__c 
                                                        WHERE  csordtelcoa__Product_Basket__r.cscfga__Opportunity__c in :oppIds
                                                        AND  Name LIKE 'OVC%'];


                system.debug('servicesList '+servicesList);
            

                Map<string, List<csord__Service__c>> opptyToServiceMap = new Map<string, List<csord__Service__c>>();
                set<Id> subscriptionIds = new set<Id>(); //used for updating csord__Service__c  items  

                for(csord__Service__c serv: servicesList){
                    List<csord__Service__c> temp = opptyToServiceMap.get(serv.csordtelcoa__Product_Basket__r.cscfga__Opportunity__c);

                    if(temp == null){
                        opptyToServiceMap.put( serv.csordtelcoa__Product_Basket__r.cscfga__Opportunity__c,new List<csord__Service__c>{serv});
                        subscriptionIds.add(serv.csord__Subscription__c);
                    }else{
                        temp.add(serv);
                    }  
                }
                system.debug('!!! opptyToServiceMap: '+opptyToServiceMap);
                //get List of csord__Subscription__c UNI BPI items to update
                List<csord__Subscription__c> subsList = [SELECT id,Name,BPI_Id__c,UNI_Id__c,csord__Order__r.csordtelcoa__Opportunity__c
                                                        FROM csord__Subscription__c 
                                                        WHERE  Id in :subscriptionIds
                                                        AND Name LIKE 'Direct Fibre - Product Charges'];

                Map<string, List<csord__Subscription__c>> opptyToSubscriptionMap = new Map<string, List<csord__Subscription__c>>();

                for(csord__Subscription__c sub : subsList){
                    List<csord__Subscription__c> temp = opptyToSubscriptionMap.get(sub.csord__Order__r.csordtelcoa__Opportunity__c); // there should be a 1 to 1 for opps and subscriptions but handling multiple
                    if(temp==null){
                        opptyToSubscriptionMap.put(sub.csord__Order__r.csordtelcoa__Opportunity__c,new List<csord__Subscription__c>{sub});
                    }else{
                        temp.add(sub);
                    }
                }

                    system.debug('guidToOrderMap: ' + guidToOrderMap);
                    system.debug('opptyToServiceMap: ' + opptyToServiceMap);
                    system.debug('opptyToSubscriptionMap: ' + opptyToSubscriptionMap);

                //CPST-42 | michaelcalusa@nbnco.com.au | received notification rspactionrequired and rspactioncompleted
                //CPST-7849 | michaelcalusa@nbnco.com.au | Receive new notification from Appian for Revised Delivery Date
                if(eventType.equalsIgnoreCase(ordSetting.OrderAckEventCode__c) )
                    processOrderAckRequest(guidToOrderMap,ordSetting);
                else if (eventType.equalsIgnoreCase(ordSetting.OrderAccEventCode__c)     ||
                         eventType.equalsIgnoreCase(ordSetting.OrderConstructionCode__c) ||
                         eventType.equalsIgnoreCase(ordSetting.OrderSVCCOMPCode__c) ||
                         eventType.equalsIgnoreCase(ordSetting.OrderReschedEventCode__c) ||
                         eventType.equalsIgnoreCase(ordSetting.OrderOnSchedEventCode__c) ||
                         eventType.equalsIgnoreCase(ordSetting.OrderRspActionReqdEventCode__c) ||
                         eventType.equalsIgnoreCase(ordSetting.OrderRspActionCompEventCode__c)) {
                    processOrderProgressRequest(guidToOrderMap,ordSetting);

                    If(opptyToSubscriptionMap.size()>0 && opptyToServiceMap.size()>0){
                        updateCSServiceAndSubscriptionItems(guidToOrderMap, opptyToServiceMap, opptyToSubscriptionMap);
                    }
                }
                else if (eventType.equalsIgnoreCase(ordSetting.OrderCompleteEventCode__c)){
                    processOrderCompleteRequest(guidToOrderMap,ordSetting); 

                    If(opptyToSubscriptionMap.size()>0 && opptyToServiceMap.size()>0){
                        updateCSServiceAndSubscriptionItems(guidToOrderMap, opptyToServiceMap, opptyToSubscriptionMap);
                    }          
                }
                else if(eventType.equalsIgnoreCase(ordSetting.OrderSchEventCode__c)){
                     string fbcToggle = 'true';
                     //check if FBC delta function is on/off in custom setting
                     DF_Custom_Options__c option = DF_Custom_Options__c.getValues('TOGGLE_FBC');
                     if(option != null) {
                         fbcToggle = option.value__c;
                     }
                     processOrderProgressRequest(guidToOrderMap,ordSetting); 
                     
                     If(opptyToSubscriptionMap.size()>0 && opptyToServiceMap.size()>0){
                        system.debug('handling  order scheduled event');

                        for(DF_Order_Event__e evt : lstToProcess){
                            OrderResponse ordRes = (OrderResponse)JSON.deserialize(evt.Message_Body__c, OrderResponse.class);                            
                                                         
                            if(ordRes.body.manageProductOrderNotification.productOrder.newFibreBuildCost!=null && fbcToggle == 'true'){
                                DF_Order__c ordObj = guidToOrderMap.get(evt.Event_Record_Id__c);
                                csord__Service__c serviceInstance = opptyToServiceMap.get(ordObj.DF_Quote__r.Opportunity__c)[0];
                                system.debug('!!! serviceInstance.csordtelcoa__Product_Basket__c' +serviceInstance.csordtelcoa__Product_Basket__c);
                                if(serviceInstance.csordtelcoa__Product_Basket__c!=null){
                                    DF_FBCVarianceController.manageFBCRequest(ordRes.body.manageProductOrderNotification.productOrder.newFibreBuildCost,serviceInstance.csordtelcoa__Product_Basket__c);
                                }
                            }
                        }
                     }
                }

            }

        //}catch(Exception e){
        //    GlobalUtility.logMessage('Error', 'OrderNotifier', 'DF_OrderInboundEventHandler', ' ', '', '', '', e, 0);
        //}
       
    }

    public class OrderResponse {
        public Body body;
        public Headers headers;
    }
    
    public class Headers {
        public string timestamp;
    }
    
    public class Body
    {
        public ManageProdOrderNotification manageProductOrderNotification;
    }

    public class ManageProdOrderNotification {
        public ProductOrd productOrder;
        public string notificationType;
    }

    public class ProductOrd {
        public String id {get;set;}
        public Note note {get;set;}
        public String interactionStatus {get;set;}
        public String interactionSubStatus {get;set;}
        public String orderType {get;set;}
        public ProductOrderComprisedOf productOrderComprisedOf { get; set; }
        public String plannedCompletionDate {get;set;}
        //CPST-7849 | michaelcalusa@nbnco.com.au | Receive new notification from Appian for Revised Delivery Date
        public String revisedCompletionDate {get;set;}
        public Decimal newFibreBuildCost {get; set;}
        public string customerRequiredDate {get; set;}
        public string interactionDateComplete {get; set;}
        public string afterHoursAppointment {get; set;}
        public string inductionRequired {get; set;}
        public string securityClearance {get; set;}
        public string tradingName {get; set;}
        public string notes {get; set;}
        public string heritageSite {get; set;}
        public AccessSeekerInteraction accessSeekerInteraction {get; set;}
    }

    //added for pending delay reasons
    public class Note {
        public String id {get;set;}
        public String description {get;set;}
    }

    public class AccessSeekerInteraction {
        public string billingAccountID {get; set;}
    }
    
    public class ItemInvolvesLocation
    {
        public string id { get; set; }
        public string enterpriseEthernetServiceLevelRegion {get; set;}
    }

    public class ItemInvolvesProduct
    {
        public string id {get ; set;}
        public string term { get; set; }
        public string serviceRestorationSLA { get; set; }
        public string accessAvailabilityTarget { get; set; }
        public string zone { get; set; }
        public string productType { get; set; }
        public string templateId { get; set; }
        public string templateVersion { get; set; }
    }

    public class ItemInvolvesProduct2
    {
        public string id { get; set; }
        public string type { get; set; }
        public string btdType { get; set; }
        public string btdMounting { get; set; }
        public string powerSupply1 { get; set; }
        public string powerSupply2 { get; set; }
        public string ovcTransientId { get; set; }
        public string poi { get; set; }
        public string nni { get; set; }
        public string routeType { get; set; }
        public string sVLanId { get; set; }
        public string maximumFrameSize { get; set; }
        public string cosHighBandwidth { get; set; }
        public string cosMediumBandwidth { get; set; }
        public string cosLowBandwidth { get; set; }
        public string uniVLanId { get; set; }
        public string connectedTo { get; set; }
        public string cosMappingMode { get; set; }
    }

    public class ItemInvolvesProduct3
    {
        public string id { get; set; }
        public string type { get; set; }
        public string interfaceBandwidth { get; set; }
        public string portId { get; set; }
        public string interfaceType { get; set; }
        public string ovcType { get; set; }
        public string tpId { get; set; }
        //public string cosMappingMode { get; set; }
        public string transientId { get; set; }
    }

    public class ReferencesProductOrderItem2
    {
        public string action { get; set; }
        public ItemInvolvesProduct3 itemInvolvesProduct { get; set; }
    }

    public class ReferencesProductOrderItem
    {
        public string action { get; set; }
        public ItemInvolvesProduct2 itemInvolvesProduct { get; set; }
        public List<ReferencesProductOrderItem2> referencesProductOrderItem { get; set; }
    }

    public class ProductOrderComprisedOf
    {
        public string action { get; set; }
        public ItemInvolvesLocation itemInvolvesLocation { get; set; }
        public ItemInvolvesProduct itemInvolvesProduct { get; set; }
        public List<ReferencesProductOrderItem> referencesProductOrderItem { get; set; }
    }

    private void processOrderAckRequest(Map<String,DF_Order__c> guidToOrderMap,DF_Order_Settings__mdt ordSetting){

        List<DF_Order__c> modOrderLst = new List<DF_Order__c>();
        Map<String, String> dfOrderToTemplateMap = new Map<String, String>();

        //getting the list of notification type
        set<String> notifyType = new set<String>();
        Schema.DescribeFieldResult fieldResultNotify = DF_Order__c.Order_Sub_Status__c.getDescribe();
        notifyType = getOrderPickListByField(fieldResultNotify);
        system.debug('notifyType acknowledge:: ' + notifyType);

        for(DF_Order_Event__e evt : lstToProcess){
            DF_Order__c ordObj = guidToOrderMap.get(evt.Event_Record_Id__c);
            if(ordObj!=null){
                if((evt.Event_Id__c==ordSetting.OrderAckEventCode__c)){
                    system.debug('Order notifier Update event received for the Event ID '+evt.Event_Id__c);
                    system.debug('Order notifier message is '+evt.Message_Body__c);
                    OrderResponse ordAckRes = (OrderResponse)JSON.deserialize(evt.Message_Body__c, OrderResponse.class);
                    System.debug('temp class is ' + ordAckRes);
                    ordObj.Order_Notifier_Response_JSON__c = json.serialize(ordAckRes.body.manageProductOrderNotification.productOrder);
                    ordObj.Order_Id__c = ordAckRes.body.manageProductOrderNotification.productOrder.id;
                    ordObj.BPI_Id__c = ordAckRes.body.manageProductOrderNotification.productOrder.productOrderComprisedOf.itemInvolvesProduct.id;
                    ordObj.Order_Status__c = ordAckRes.body.manageProductOrderNotification.productOrder.interactionStatus;
                    //added for notification type tracking
                    setDFOrderNotificationType(notifyType, ordObj, ordAckRes);
                    //for unpending substatus notifications
                    ordObj.Order_Notification_Sub_Status__c = null;
                    ordObj.Appian_Notification_Date__c = Datetime.valueOfGMT(ordAckRes.headers.timestamp.replace('T', ' ').replace('Z',''));//convertGMTtoLocal(Datetime.valueOfGMT(ordAckRes.headers.timestamp.replace('T', ' ').replace('Z','')));
                    modOrderLst.add(ordObj);
                    dfOrderToTemplateMap.put(ordObj.id, DF_Constants.TEMPLATE_ORDER_ACKNOWLEDGED);
                } 
            }
                          
        }

        if(modOrderLst.size()>0){
            update modOrderLst;
        }
        if(!dfOrderToTemplateMap.isEmpty()) {
            System.debug('DF_OrderInboundEventHandler >>>> dfOrderToTemplateMap >>> ' + dfOrderToTemplateMap);
            DF_OrderEmailService.sendEmail(dfOrderToTemplateMap);
        }

        system.debug('!!!! modOrderLst: '+modOrderLst);
    }

    private void processOrderProgressRequest(Map<String,DF_Order__c> guidToOrderMap,DF_Order_Settings__mdt ordSetting){

        List<DF_Order__c> modOrderLst = new List<DF_Order__c>();
        Map<String, String> dfOrderToTemplateMap = new Map<String, String>();

        //getting the list of notification type
        set<String> notifyType = new set<String>();
        Schema.DescribeFieldResult fieldResultNotify = DF_Order__c.Order_Sub_Status__c.getDescribe();
        notifyType = getOrderPickListByField(fieldResultNotify);
        system.debug('notifyType processing:: ' + notifyType);

        //getting the list of notification sub status
        //CPST-42 | michaelcalusa@nbnco.com.au | received notification rspactionrequired and rspactioncompleted
        set<String> notifySubStatus = new set<String>();
        Schema.DescribeFieldResult fieldResultSub = DF_Order__c.Order_Notification_Sub_Status__c.getDescribe();
        notifySubStatus = getOrderPickListByField(fieldResultSub);
        system.debug('notifySubStatus:: ' + notifySubStatus);

        for(DF_Order_Event__e evt : lstToProcess){
            DF_Order__c ordObj = guidToOrderMap.get(evt.Event_Record_Id__c);
            if(ordObj!=null){
                //CPST-42 | michaelcalusa@nbnco.com.au | received notification rspactionrequired and rspactioncompleted
                //CPST-7849 | michaelcalusa@nbnco.com.au | Receive new notification from Appian for Revised Delivery Date
                if((evt.Event_Id__c== ordSetting.OrderAccEventCode__c)     ||
                   (evt.Event_Id__c== ordSetting.OrderSchEventCode__c)     ||
                   (evt.Event_Id__c== ordSetting.OrderConstructionCode__c)||
                   (evt.Event_Id__c== ordSetting.OrderSVCCOMPCode__c) ||
                   (evt.Event_Id__c== ordSetting.OrderReschedEventCode__c) ||
                   (evt.Event_Id__c== ordSetting.OrderOnSchedEventCode__c) ||
                   (evt.Event_Id__c== ordSetting.OrderRspActionReqdEventCode__c) ||
                   (evt.Event_Id__c== ordSetting.OrderRspActionCompEventCode__c)) {
                    system.debug('processOrderProgressRequest Order notifier Update event received for the Event ID '+evt.Event_Id__c);
                    system.debug('processOrderProgressRequest Order notifier message is '+evt.Message_Body__c);
                    OrderResponse ordRes = (OrderResponse)JSON.deserialize(evt.Message_Body__c, OrderResponse.class);
                    ordObj.Order_Status__c = ordRes.body.manageProductOrderNotification.productOrder.interactionStatus;
                    ordObj.BPI_Id__c = ordRes.body.manageProductOrderNotification.productOrder.productOrderComprisedOf.itemInvolvesProduct.id;
                    ordObj.Service_Region__c = ordRes.body.manageProductOrderNotification.productOrder.productOrderComprisedOf.itemInvolvesLocation.enterpriseEthernetServiceLevelRegion;
                    ordObj.Order_Notifier_Response_JSON__c = json.serialize(ordRes.body.manageProductOrderNotification.productOrder);
                    ordObj.Appian_Notification_Date__c = Datetime.valueOfGMT(ordRes.headers.timestamp.replace('T', ' ').replace('Z',''));//convertGMTtoLocal(Datetime.valueOfGMT(ordRes.headers.timestamp.replace('T', ' ').replace('Z','')));
                    //set notification type
                    setDFOrderNotificationType(notifyType, ordObj, ordRes);
                    //for unpending substatus notifications
                    ordObj.Order_Notification_Sub_Status__c = null;
                    //change for unie port id
                    DF_OrderInboundEventHandler.productOrd objPrdOrd = ordRes.body.manageProductOrderNotification.productOrder;
                    if(objPrdOrd != null) {
                              system.debug('!!! ordRes.body.manageProductOrderNotification.productOrder.newFibreBuildCost' + ordRes.body.manageProductOrderNotification.productOrder.newFibreBuildCost);          
                        DF_OrderInboundEventHandler.ProductOrderComprisedOf objPrdComp = objPrdOrd.productOrderComprisedOf;
                        if(objPrdComp != null) {
                            
                            List<DF_OrderInboundEventHandler.ReferencesProductOrderItem> lstRefPrdItem = objPrdComp.referencesProductOrderItem;
                            System.debug('::RefPRD::'+lstRefPrdItem);
                            if(lstRefPrdItem != null) {
                                for(DF_OrderInboundEventHandler.ReferencesProductOrderItem lstRef : lstRefPrdItem) {
                                    DF_OrderInboundEventHandler.ItemInvolvesProduct2 itemPrd = lstRef.itemInvolvesProduct;
                                    if(itemPrd.type != 'OVC')
                                        ordObj.BTD_Id__c = itemPrd.Id;                                
                                    if(lstRef.referencesProductOrderItem != null) {
                                        //UNI data from ItemInvolvesProduct3 node
                                        List<DF_OrderInboundEventHandler.ReferencesProductOrderItem2> lstUni = lstRef.referencesProductOrderItem;
                                        if(lstUni != null && lstUni.size() > 0) {
                                            DF_OrderInboundEventHandler.ItemInvolvesProduct3 objUni = lstUni[0].itemInvolvesProduct;
                                            if(objUni != null) {
                                                if(string.IsNotBlank(objUni.portId))
                                                    ordObj.UNI_Port__c = objUni.portId;
                                                ordObj.UNI_Id__c = objUni.Id;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if(String.isNotBlank(ordRes.body.manageProductOrderNotification.productOrder.plannedCompletionDate)) {                        
                        ordObj.NBN_Commitment_Date__c = date.valueof(ordRes.body.manageProductOrderNotification.productOrder.plannedCompletionDate);
                    }
                    //CPST-42 | michaelcalusa@nbnco.com.au | received notification rspactionrequired and rspactioncompleted
                    //CPST-7849 | michaelcalusa@nbnco.com.au | Receive new notification from Appian for Revised Delivery Date
                    processOrderNotificationsWithReason(ordObj,ordRes,ordSetting,evt,notifySubStatus);
                    modOrderLst.add(ordObj);

                    // Email notifications
                    //CPST-7870 | michaelcalusa@nbnco.com.au | Send email (RDD)
                    if((evt.Event_Id__c == ordSetting.OrderAccEventCode__c)) {
                        dfOrderToTemplateMap.put(ordObj.Id, DF_Constants.TEMPLATE_ORDER_ACCEPTED);
                    } else if((evt.Event_Id__c == ordSetting.OrderConstructionCode__c)) {
                        dfOrderToTemplateMap.put(ordObj.Id, DF_Constants.TEMPLATE_ORDER_CONSTRUCTION_STARTED);
                    } else if((evt.Event_Id__c == ordSetting.OrderSchEventCode__c)) {
                        dfOrderToTemplateMap.put(ordObj.Id, DF_Constants.TEMPLATE_ORDER_SCHEDULED);
                    } else if((evt.Event_Id__c == ordSetting.OrderSVCCOMPCode__c)) {
                        dfOrderToTemplateMap.put(ordObj.Id, DF_Constants.TEMPLATE_ORDER_SERVICE_TEST_COMPLETED);
                    } else if((evt.Event_Id__c == ordSetting.OrderRspActionReqdEventCode__c)) {
                        dfOrderToTemplateMap.put(ordObj.Id, DF_Constants.TEMPLATE_ORDER_PENDING);
                    } else if((evt.Event_Id__c == ordSetting.OrderRspActionCompEventCode__c)) {
                        dfOrderToTemplateMap.put(ordObj.Id, DF_Constants.TEMPLATE_ORDER_PENDING_COMPLETED);
                    } else if((evt.Event_Id__c == ordSetting.OrderReschedEventCode__c)) {
                        dfOrderToTemplateMap.put(ordObj.Id, DF_Constants.TEMPLATE_ORDER_RESCHEDULED);
                    } else if((evt.Event_Id__c == ordSetting.OrderOnSchedEventCode__c)) {
                        dfOrderToTemplateMap.put(ordObj.Id, DF_Constants.TEMPLATE_ORDER_ON_SCHEDULE);
                    }
                } 
            }                          
        }

        if(modOrderLst.size()>0){
            update modOrderLst;
        }
        system.debug('!!!! modOrderLst: '+modOrderLst);

        if(!dfOrderToTemplateMap.isEmpty()) {
            System.debug('DF_OrderInboundEventHandler >>>  dfOrderToTemplateMap >>> ' + dfOrderToTemplateMap);
            DF_OrderEmailService.sendEmail(dfOrderToTemplateMap);
        }
    }
    
    private void processOrderCompleteRequest(Map<String,DF_Order__c> guidToOrderMap,DF_Order_Settings__mdt ordSetting){
        //getting the list of notification type
        set<String> notifyType = new set<String>();
        Schema.DescribeFieldResult fieldResultNotify = DF_Order__c.Order_Sub_Status__c.getDescribe();
        notifyType = getOrderPickListByField(fieldResultNotify);
        system.debug('notifyType completed:: ' + notifyType);

        for(DF_Order_Event__e evt : lstToProcess){
            DF_Order__c ordObj = guidToOrderMap.get(evt.Event_Record_Id__c);
            if(ordObj!=null){
                if((evt.Event_Id__c == ordSetting.OrderCompleteEventCode__c)){
                    OrderResponse ordCompRes = (OrderResponse)JSON.deserialize(evt.Message_Body__c, OrderResponse.class);
                    System.debug('ord comp is : ' + ordCompRes);
                    string notifierResponse = json.serialize(ordCompRes.body.manageProductOrderNotification.productOrder);
                    string status = ordCompRes.body.manageProductOrderNotification.productOrder.interactionStatus;
                    string appianDate = ordCompRes.headers.timestamp;
                    system.debug('ordObj.Id, notifierResponse, status, appianDate,ordObj.Order_SubType__c ' + ordObj.Id+', '+notifierResponse +', '+status+', '+ appianDate+', '+ordObj.Order_SubType__c);
		    //added for notification type tracking
                    setDFOrderNotificationType(notifyType, ordObj, ordCompRes);
                    //for unpending substatus notifications
                    ordObj.Order_Notification_Sub_Status__c = null;
                    if(ordObj.OrderType__c==DF_Constants.ORDER_TYPE_CONNECT ||ordObj.Order_SubType__c==null || !ordObj.Order_SubType__c.equalsIgnoreCase(DF_Constants.UNI_VLAN_ID_MODIFY_SUBTYPE)){
                        DF_SFToSOAProductCharges.updateProductCharges(ordObj.Id, notifierResponse, status, appianDate);     
                    } else{
                        system.debug('!!! Not sending Values SOA');
                        ordObj.Order_Status__c = status;
                        ordObj.Order_Sub_Status__c = ''; 
                        if(!string.isEmpty(appianDate))
                            ordObj.Appian_Notification_Date__c = DateTime.valueOfGMT(appianDate.replace('T', ' ').replace('Z',''));//DF_OrderInboundEventHandler.convertGMTtoLocal(DateTime.valueOfGMT(appianDate.replace('T', ' ').replace('Z','')));      
                        update ordObj;
                    }           
                } 
            }                 
        }
    }

    private void updateCSServiceAndSubscriptionItems(Map<String,DF_Order__c> guidToOrderMap,Map<string, List<csord__Service__c>> opptyToServiceMap,Map<string, List<csord__Subscription__c>> opptyToSubscriptionMap ){
        List<csord__Service__c> servicesToUpdate = new List<csord__Service__c>();
        List<csord__Subscription__c> subscriptionsToUpdate = new List<csord__Subscription__c>();
        try{
            for(DF_Order_Event__e evt : lstToProcess){
                DF_Order__c ordObj = guidToOrderMap.get(evt.Event_Record_Id__c);
                Id oppId = ordObj.DF_Quote__r.Opportunity__c; //get opp id for updating CS objects
                if(ordObj!=null){
                    OrderResponse ordRes = (OrderResponse)JSON.deserialize(evt.Message_Body__c, OrderResponse.class);

                    DF_OrderInboundEventHandler.productOrd objPrdOrd = ordRes.body.manageProductOrderNotification.productOrder;
                        if(objPrdOrd != null) {
                                            
                            DF_OrderInboundEventHandler.ProductOrderComprisedOf objPrdComp = objPrdOrd.productOrderComprisedOf;
                            if(objPrdComp != null) {
                                
                                List<DF_OrderInboundEventHandler.ReferencesProductOrderItem> lstRefPrdItem = objPrdComp.referencesProductOrderItem;
                                System.debug('::RefPRD::'+lstRefPrdItem);
                                if(lstRefPrdItem != null) {
                                    for(DF_OrderInboundEventHandler.ReferencesProductOrderItem lstRef : lstRefPrdItem) {
                                        DF_OrderInboundEventHandler.ItemInvolvesProduct2 itemPrd = lstRef.itemInvolvesProduct;
                                        system.debug('itemPrd '+itemPrd);
                                        system.debug('itemPrd.type '+itemPrd.type);
                                        if(itemPrd.type == 'OVC'){
                                            //get OVC with ovcTransient Id from subscriptions to update csord__Service__c
                                            List<csord__Service__c> tempServs = opptyToServiceMap.get(oppId);
                                            if(tempServs!=null && tempServs.size()>0){
                                                for(csord__Service__c serv: tempServs){
                                                    if(serv.Name == itemPrd.ovcTransientId){
                                                        serv.OVC_Id__c = itemPrd.Id;
                                                        servicesToUpdate.add(serv);
                                                    }
                                                }
                                            }                          
                                        }
                                        if(lstRef.referencesProductOrderItem != null) {
                                            //UNI data from ItemInvolvesProduct3 node
                                            List<DF_OrderInboundEventHandler.ReferencesProductOrderItem2> lstUni = lstRef.referencesProductOrderItem;
                                            if(lstUni != null && lstUni.size() > 0) {
                                                DF_OrderInboundEventHandler.ItemInvolvesProduct3 objUni = lstUni[0].itemInvolvesProduct;
                                                if(objUni != null) {
                                                     List<csord__Subscription__c> tempSubs = opptyToSubscriptionMap.get(oppId);
                                                     if(tempSubs != null && tempSubs.size()>0){
                                                         for(csord__Subscription__c sub: tempSubs){ //there should be only one
                                                            sub.UNI_Id__c = objUni.Id;
                                                            sub.BPI_Id__c = ordRes.body.manageProductOrderNotification.productOrder.productOrderComprisedOf.itemInvolvesProduct.id;
                                                            subscriptionsToUpdate.add(sub);
                                                         }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }


                }
            }
       
            system.debug('servicesToUpdate subscriptionsToUpdate'+ servicesToUpdate+' '+subscriptionsToUpdate);
            if(servicesToUpdate.size()>0){
                update servicesToUpdate;
            }
            if(subscriptionsToUpdate.size()>0){
                update subscriptionsToUpdate;
            }
         }catch(Exception e){
            GlobalUtility.logMessage('Error', 'OrderNotifier', 'updateCSServiceAndSubscriptionItems', ' ', '', '', '', e, 0);
        }
    }
    
    public static Datetime convertGMTtoLocal(Datetime gmtTime) {
        Integer offset = UserInfo.getTimezone().getOffset(gmtTime);
        Datetime local = gmtTime.addSeconds(offset/1000);
        return local;
    }

    private static Set<String> getOrderPickListByField(Schema.DescribeFieldResult fieldResult) {
        set<String> subStatus = new set<String>();
        if(fieldResult != null) {
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                subStatus.add(pickListVal.getValue());
            }
        }
        return subStatus;
    }

    /**
    * Process different order notifications that has reason code and description.
    * CPST-7849 | michaelcalusa@nbnco.com.au | Receive new notification from Appian for Revised Delivery Date
    */
    private static void processOrderNotificationsWithReason(DF_Order__c ordObj, OrderResponse ordRes,
        DF_Order_Settings__mdt ordSetting,DF_Order_Event__e evt,set<String> notifySubStatus) {
        //additional conditions may apply for future notifications with reason code and description
        if(evt.Event_Id__c==ordSetting.OrderOnSchedEventCode__c) {
            ordObj.Revised_Delivery_Date__c	= null;
        } else if(evt.Event_Id__c==ordSetting.OrderReschedEventCode__c) {
            if(String.isNotBlank(ordRes.body.manageProductOrderNotification.productOrder.revisedCompletionDate)) {
                ordObj.Revised_Delivery_Date__c	= date.valueof(ordRes.body.manageProductOrderNotification.productOrder.revisedCompletionDate);
            }
        } else if((evt.Event_Id__c== ordSetting.OrderRspActionReqdEventCode__c)
            || (evt.Event_Id__c== ordSetting.OrderRspActionCompEventCode__c)) {
            setDFOrderNotificationSubStatus(notifySubStatus, ordObj, ordRes);
        }
        setDFOrderReasonNotifications(ordObj, ordRes);
    }

    /**
    * add reason code and description for pending, resume notification and for future notifications
    */
    private static void setDFOrderReasonNotifications(DF_Order__c ordObj, OrderResponse ordRes) {
        if(ordRes.body.manageProductOrderNotification.productOrder.note != null
                && ordRes.body.manageProductOrderNotification.productOrder.note.id != null
                && ordRes.body.manageProductOrderNotification.productOrder.note.description != null) {
            ordObj.Reason_Code__c = ordRes.body.manageProductOrderNotification.productOrder.note.id;
            ordObj.Notification_Reason__c = ordRes.body.manageProductOrderNotification.productOrder.note.description;
        }
    }

    /**
    *  if interaction sub status matches sf api, set sub status to pending or equivalent
    *  else set to empty
    */
    private static void setDFOrderNotificationSubStatus(set<String> notifySubStatus, DF_Order__c ordObj, OrderResponse ordRes) {
        ordObj.Order_Notification_Sub_Status__c = null;
        if(notifySubStatus.contains(ordRes.body.manageProductOrderNotification.productOrder.interactionSubStatus)) {
            ordObj.Order_Notification_Sub_Status__c = ordRes.body.manageProductOrderNotification.productOrder.interactionSubStatus;
        }
    }

    /**
    * if notificationType matches sf api, set notification type on sub status field in ee
    */
    private static void setDFOrderNotificationType(set<String> notifyType, DF_Order__c ordObj, OrderResponse ordRes) {
        if(notifyType.contains(ordRes.body.manageProductOrderNotification.notificationType)) {
            ordObj.Order_Sub_Status__c = ordRes.body.manageProductOrderNotification.notificationType;
        }
    }
}