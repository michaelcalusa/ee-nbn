public with sharing class DF_OrderJSONDataHelper {
 public static String generateJSONStringForOrder(String  dfOrderId, String location) {
        String jsonRetVal = '';
        string csaId = '';
try{
        DF_Order__c objObject = new DF_Order__c();  
        DF_Order__c order;
        List<DF_Order__c> orderList;
        if (String.isNotEmpty(dfOrderId)) {               
                orderList = [SELECT Id, Trading_Name__c, Heritage_Site__c,Induction_Required__c, Security_Required__c,Site_Notes__c, 
                                    Customer_Required_Date__c,NTD_Mounting__c,Power_Supply_1__c,Power_Supply_2__c,Installation_Notes__c,
                                    DF_Quote__c
                                    FROM DF_Order__c
                                    WHERE  Id = :dfOrderId];                                                   
            }
            if (!orderList.isEmpty()) {
                order = orderList[0];
                system.debug('!!!!' + order);
            }

        DF_Quote__c quote = [SELECT Opportunity_Bundle__c, LAPI_Response__c FROM DF_Quote__c WHERE Id =: order.DF_Quote__c LIMIT 1];
        DF_Opportunity_Bundle__c opBundle = [SELECT Name, Account__c FROM DF_Opportunity_Bundle__c WHERE Id =: quote.Opportunity_Bundle__c LIMIT 1 ];
        Account account = [SELECT Billing_ID__c FROM Account WHERE Id =: opBundle.Account__c LIMIT 1]; 

        //get the csaId   
        if(!string.isEmpty(quote.LAPI_Response__c)) {             
            DF_ServiceFeasibilityResponse df_sfJson = (DF_ServiceFeasibilityResponse)Json.deserialize(quote.LAPI_Response__c, DF_ServiceFeasibilityResponse.class);
            if(df_sfJson != null) {
                csaId = df_sfJson.csaId;
            }   
        }
        system.debug('::csaId::'+csaId); 
        System.debug('!!!!' + JSON.serializePretty(location));
        Map<String, Object> orderJSON = (Map<String, Object>) JSON.deserializeUntyped(location);
        System.debug('!!!!' + orderJSON);
        

        List<Object> uniList = (List<Object>)orderJSON.get('UNI');
        system.debug('!!! uniList'+uniList);
        Map<String, Object> uni = (Map<String, Object>)uniList[0]; //only one UNI

        List<Object> ovcList = (List<Object>)orderJSON.get('OVCs');
        system.debug('!!! ovcList'+ovcList);


        DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot productOrderRoot = new DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot();

        //get acces seeker
        DF_OrderDataJSONWrapper.AccessSeekerInteraction accessSeeker = new DF_OrderDataJSONWrapper.AccessSeekerInteraction();
            accessSeeker.billingAccountID = account.Billing_ID__c;
        //
        //create datevar

        DF_OrderDataJSONWrapper.ProductOrder productOrder = new DF_OrderDataJSONWrapper.ProductOrder();
   
        system.debug('!!!! order.Customer_Required_Date__c @186' +order.Customer_Required_Date__c  );
        If(order.Customer_Required_Date__c != null){
            Date dToday = order.Customer_Required_Date__c;
            Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
            productOrder.customerRequiredDate = dt.format('yyyy-MM-dd');
        }
        //create productOrder

        productOrderRoot.productOrder = productOrder;
        productOrder.accessSeekerInteraction = accessSeeker;
        productOrder.orderType = 'Connect';
        productOrder.salesforceQuoteId = (String)orderJSON.get('quoteId');
        productOrder.salesforceBundleId = opBundle.Name;
       
        productOrder.afterHoursAppointment =  (String)uni.get('AHA');
        productOrder.csaId = csaId;
        productOrder.securityClearance = order.Security_Required__c;
        productOrder.inductionRequired = order.Induction_Required__c;
        productOrder.notes = order.Site_Notes__c;
        productOrder.heritageSite = order.Heritage_Site__c;
        productOrder.tradingName = order.Trading_Name__c;
        
        List<DF_OrderDataJSONWrapper.ItemInvolvesContact> itemInvolvesContact = new List<DF_OrderDataJSONWrapper.ItemInvolvesContact>();
        //set Site businessContList
        List<Object> businessContList = (List<Object>)orderJSON.get('businessContactList');
        system.debug('!!! businessContList'+businessContList);


        if(businessContList !=null ){
            for(Integer i = 0; i<businessContList.size(); i++){
                DF_OrderDataJSONWrapper.ItemInvolvesContact itemInvBusContact = new DF_OrderDataJSONWrapper.ItemInvolvesContact();
                Map<String, Object> bContact = (Map<String, Object>)businessContList[i];
                itemInvBusContact.type = 'Business';
                itemInvBusContact.contactName = (String)bContact.get('busContFirstName') + ' ' + (String)bContact.get('busContSurname');
                itemInvBusContact.role = (String)bContact.get('busContRole');
                itemInvBusContact.emailAddress = (String)bContact.get('busContEmail');
                itemInvBusContact.phoneNumber = (String)bContact.get('busContNumber');
                
                DF_OrderDataJSONWrapper.UnstructuredAddress busContAddress = new DF_OrderDataJSONWrapper.UnstructuredAddress();
                busContAddress.addressLine1 = (String)bContact.get('busContStreetAddress');
                busContAddress.addressLine2 = '';
                busContAddress.addressLine3 = '';
                busContAddress.localityName =  (String)bContact.get('busContSuburb');
                busContAddress.postcode = (String)bContact.get('busContPostcode');
                busContAddress.stateTerritoryCode = (String)bContact.get('busContState');
                itemInvBusContact.unstructuredAddress = busContAddress;
                System.debug('!!! itemInvBusContact'  +itemInvBusContact);
                System.debug('!!! busContAddress'  +busContAddress);
                itemInvolvesContact.add(itemInvBusContact);

            }
        }

        List<Object> siteContactList= (List<Object>)orderJSON.get('siteContactList');
        system.debug('!!! siteContactList'+siteContactList);    
//Site Contact List
        if(siteContactList != null ){
            for(Integer i = 0; i<siteContactList.size(); i++){
                DF_OrderDataJSONWrapper.ItemInvolvesContact itemInvSiteContact = new DF_OrderDataJSONWrapper.ItemInvolvesContact();
                Map<String, Object> sContact = (Map<String, Object>)siteContactList[i];
                itemInvSiteContact.type = 'Site';
                itemInvSiteContact.contactName = (String)sContact.get('siteContFirstName') + ' ' + (String)sContact.get('siteContSurname');
                itemInvSiteContact.role = '' ;
                itemInvSiteContact.emailAddress = (String)sContact.get('siteContEmail');
                itemInvSiteContact.phoneNumber = (String)sContact.get('siteContNumber');
                
                DF_OrderDataJSONWrapper.UnstructuredAddress siteContAddress = new DF_OrderDataJSONWrapper.UnstructuredAddress();
                siteContAddress.addressLine1 ='';
                siteContAddress.addressLine2 = '';
                siteContAddress.addressLine3 = '';
                siteContAddress.localityName = '';
                siteContAddress.postcode = '';
                siteContAddress.stateTerritoryCode = '';
                itemInvSiteContact.unstructuredAddress = siteContAddress;
                System.debug('!!! itemInvSiteContact'  +itemInvSiteContact);
                System.debug('!!! siteContAddress'  +siteContAddress);
                itemInvolvesContact.add(itemInvSiteContact);

            }
        }

        //add item involves contact site and business generated above
        productOrder.itemInvolvesContact = itemInvolvesContact;

        //generate product order comprised of
        DF_OrderDataJSONWrapper.ProductOrderComprisedOf productOrderComprisedOf = new DF_OrderDataJSONWrapper.ProductOrderComprisedOf();
        productOrderComprisedOf.action = 'ADD';
        //itemInvolvesLocation
        DF_OrderDataJSONWrapper.ItemInvolvesLocation itemInvolvesLocation = new DF_OrderDataJSONWrapper.ItemInvolvesLocation();
        itemInvolvesLocation.id = (String)orderJSON.get('locId');
        productOrderComprisedOf.itemInvolvesLocation = itemInvolvesLocation;
        //itemInvolvesProductL1 Wors
        DF_OrderDataJSONWrapper.ItemInvolvesProductL1 itemInvolvesProductL1 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL1();
        itemInvolvesProductL1.term = (String)uni.get('term')+' months';
        itemInvolvesProductL1.serviceRestorationSLA = (String)uni.get('SLA');
        itemInvolvesProductL1.accessAvailabilityTarget = (String)uni.get('AAT');
        itemInvolvesProductL1.zone = (String)uni.get('zone');
        itemInvolvesProductL1.productType = 'EEAS';//add custom setting ?
        itemInvolvesProductL1.templateId = 'TPL';//add custom setting ?
        itemInvolvesProductL1.templateVersion = '1.0';//add custom setting ?
        productOrderComprisedOf.itemInvolvesProduct = itemInvolvesProductL1;


        List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1> referencesProductOrderItemListL1 = new List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1>();
        //referencesProductOrderItemL1
        DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 referencesProductOrderItemL1 = new DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1();
        referencesProductOrderItemL1.action = 'ADD';
        //Add BTD 
        DF_OrderDataJSONWrapper.ItemInvolvesProductL2 itemInvolvesProductL2 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL2();
         // NTD / BTD fields
        itemInvolvesProductL2.type = 'BTD';
        itemInvolvesProductL2.btdType = 'Standard';
        itemInvolvesProductL2.btdMounting = order.NTD_Mounting__c;
        itemInvolvesProductL2.powerSupply1 = order.Power_Supply_1__c;
        itemInvolvesProductL2.powerSupply2 = order.Power_Supply_2__c;

        referencesProductOrderItemL1.itemInvolvesProduct = itemInvolvesProductL2;
        referencesProductOrderItemListL1.Add(referencesProductOrderItemL1);
       // referencesProductOrderItemL1.referencesProductOrderItem
        //BTD CORRECT

        //SET OVCs
        //creating OVCs ItemInvolvesProductL2
        //adding to the 
        List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1>  referencesProductOrderItemL1List = new List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1> ();

        for (Integer i =0 ; i< ovcList.size();i++){
            DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 referencesProductOrderItemL1ovc = new DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1();
            DF_OrderDataJSONWrapper.ItemInvolvesProductL2 prodL2 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL2();//OVCs
            system.debug('!!!! ovcListf i ' + ovcList[i]);
            Map<String, Object> ovc = (Map<String, Object>)ovcList[i];
            referencesProductOrderItemL1ovc.action = 'ADD';
            prodL2.type = 'OVC' ;
             // OVC fields
             prodL2.poi = (String)ovc.get('POI')== null ? null:(String)ovc.get('POI');
             prodL2.nni = (String)ovc.get('NNIGroupId')== null ? null:(String)ovc.get('NNIGroupId');    
             prodL2.routeType  = (String)ovc.get('routeType') == null ? null:(String)ovc.get('routeType') ;   
             prodL2.sVLanId  = (String)ovc.get('sTag') == null ? null:Integer.valueOf((String)ovc.get('sTag'));      
             prodL2.maximumFrameSize  = (String)ovc.get('ovcMaxFrameSize');
     
             prodL2.cosHighBandwidth  = (String)ovc.get('coSHighBandwidth')== '0' ? '':(String)ovc.get('coSHighBandwidth')+'Mbps';
             prodL2.cosMappingMode = (String)ovc.get('mappingMode') == null ? null:(String)ovc.get('mappingMode') ;   
             prodL2.cosMediumBandwidth = (String)ovc.get('coSMediumBandwidth')== '0' ? '':(String)ovc.get('coSMediumBandwidth')+'Mbps';
             prodL2.cosLowBandwidth = (String)ovc.get('coSLowBandwidth')== '0' ? '':(String)ovc.get('coSLowBandwidth')+'Mbps';
             prodL2.uniVLanId = (String)ovc.get('ceVlanId') == null ? null:(String)ovc.get('ceVlanId');
             prodL2.connectedTo    = '1';// (String)ovc.get('NNI');  
             prodL2.ovcTransientId = (String)ovc.get('OVCName') == null ? null:(String)ovc.get('OVCName');
             referencesProductOrderItemL1ovc.itemInvolvesProduct = prodL2;     
             //refProdOrderL1.itemInvolvesProduct = prodL2;
             //prodOrderCompOf.referencesProductOrderItem.Push(refProdOrderL1.itemInvolvesProduct);
             //referencesProductOrderItemL1List.Add(referencesProductOrderItemL2ovc);
             referencesProductOrderItemListL1.Add(referencesProductOrderItemL1ovc);

              }
        //referencesProductOrderItemL1.referencesProductOrderItem = referencesProductOrderItemL1List;
        productOrderComprisedOf.referencesProductOrderItem = referencesProductOrderItemListL1;

        List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2> referencesProductOrderItemListL2 = new List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2>();
        DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2 referencesProductOrderItemL2 = new DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2();
        referencesProductOrderItemL2.action = 'ADD';


        DF_OrderDataJSONWrapper.ItemInvolvesProductL3 itemInvolvesProductL3 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL3();  
        itemInvolvesProductL3.type = 'UNI';
        itemInvolvesProductL3.interfaceBandwidth = (String)uni.get('interfaceBandwidth');
        itemInvolvesProductL3.interfaceType = (String)uni.get('interfaceTypes');     
        itemInvolvesProductL3.ovcType = (String)uni.get('oVCType');    
        itemInvolvesProductL3.tpId = (String)uni.get('tPID')=='Select'?null:(String)uni.get('tPID');                                                 
        //itemInvolvesProductL3.cosMappingMode = (String)uni.get('mappingMode');
        itemInvolvesProductL3.transientId = '1';
        system.debug('!!!!itemInvolvesProductL3 '+itemInvolvesProductL3);
        referencesProductOrderItemL2.itemInvolvesProduct = itemInvolvesProductL3;
        referencesProductOrderItemListL2.add(referencesProductOrderItemL2);
        
        referencesProductOrderItemL1.referencesProductOrderItem = referencesProductOrderItemListL2;






        productOrderComprisedOf.referencesProductOrderItem = referencesProductOrderItemListL1;
        productOrder.productOrderComprisedOf = productOrderComprisedOf;
        system.debug('!!!!!!! productOrderComprisedOf ' +productOrderComprisedOf);


        DF_OrderDataJSONWrapper.AccessSeekerContact accessSeekerContact = new DF_OrderDataJSONWrapper.AccessSeekerContact();
        accessSeekerContact.contactName = null;
        accessSeekerContact.contactPhone = null;
        DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot root = new DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot();

        productOrder.accessSeekerContact = accessSeekerContact;
        root.productOrder = productOrder;
        system.debug(JSON.serialize(root));
        jsonRetVal = JSON.serialize(root);
        return jsonRetVal;
        }
        catch(Exception e)
        {
            throw e;
        }

    }

    @TestVisible private static String nullToEmpty(String input) {
    return input == null ? '' : input;
    }
}