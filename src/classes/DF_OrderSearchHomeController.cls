public with sharing class DF_OrderSearchHomeController {

    /*
	* Fetches order records for the given search parameters
	* Parameters : searchTerm (the id to be searched), searchDate, searchStatus, isTriggerBySearch (if the user pressed Enter key)
	* @Return : Returns a serialized string of list of DF Order records
	*/
    
    @AuraEnabled
    public static String getRecords(String searchTerm, String searchDate, String searchStatus, Boolean isTriggerBySearch){
        return DF_OrderSubmittedSearchController.getRecords(searchTerm,searchDate,searchStatus,isTriggerBySearch);
    }
}