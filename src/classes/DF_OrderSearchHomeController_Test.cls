@isTest 
private class DF_OrderSearchHomeController_Test {

    @isTest
	private static void test_getRecordsWithEmptySearchTerm() {

		String result = '';
		List<DF_Order__c> deserializedResult = new List<DF_Order__c>();
        
    	User commUser;
		commUser = DF_TestData.createDFCommUser();
        system.runAs(commUser) {
	    	test.startTest();    
			result = DF_OrderSearchHomeController.getRecords('',null,'',false);
			System.debug(result);
		    deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
	        System.debug(deserializedResult);                                   
	        test.stopTest();                
        }	                    
        // Assertions
        system.AssertEquals(0, deserializedResult.size());
	}

	@isTest
	private static void test_getRecordsWithInvalidSearchTerm() {
		// Setup data    	
		String searchTerm = 'This search term will cause no record to return';
		String result = '';
		List<DF_Order__c> deserializedResult = new List<DF_Order__c>();

    	User commUser;
		commUser = DF_TestData.createDFCommUser();

        system.runAs(commUser) {
	    	test.startTest();
			result = DF_OrderSearchHomeController.getRecords(searchTerm,null,'',true);
			deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
			test.stopTest();
        }	                                  
        
        // Assertions
        system.AssertEquals(0, deserializedResult.size());
	}

	@isTest
	private static void test_getRecordsWithValidSearchTerm() {
		
        String searchTerm = 'ORD000000000641';
		String result = '';
		List<DF_Order__c> deserializedResult = new List<DF_Order__c>();

    	User commUser;
		commUser = DF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            DF_TestService.createOrderRecord(userLst.get(0).AccountId);
            test.startTest();
			result = DF_OrderSearchHomeController.getRecords(searchTerm,'','',true);
			deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
			test.stopTest();                
        }	                                  
        
	}
    
    @isTest
	private static void test_getRecordsWithValidSearchTermLOCID() {
		
        String searchTerm = 'LOC000035375038';
		String result = '';
		List<DF_Order__c> deserializedResult = new List<DF_Order__c>();

    	User commUser;
		commUser = DF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            DF_TestService.createOrderRecord(userLst.get(0).AccountId);
            test.startTest();
			result = DF_OrderSearchHomeController.getRecords(searchTerm,'','',true);
			deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
			test.stopTest();                
        }	                                  
        
	}
    
    @isTest
	private static void test_getRecordsWithValidSearchTermBundleID() {
		
        String result = '';
		List<DF_Order__c> deserializedResult = new List<DF_Order__c>();

    	User commUser;
		commUser = DF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            DF_TestService.createOrderRecord(userLst.get(0).AccountId);
            List<DF_Opportunity_Bundle__c> oppBundle = [SELECT ID, Name FROM DF_Opportunity_Bundle__c WHERE Account__c = :userLst.get(0).AccountId];
            DF_Opportunity_Bundle__c oB = oppBundle.size()>0 ? oppBundle.get(0) : null;
            test.startTest();
			result = DF_OrderSearchHomeController.getRecords(oB.Name,'','',true);
			deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
			test.stopTest();                
        }	                                  
        
	}
    
    @isTest
	private static void test_getRecordsWithValidSearchStatus() {
		
        String searchStatus = 'In Draft';
		String result = '';
		List<DF_Order__c> deserializedResult = new List<DF_Order__c>();

    	User commUser;
		commUser = DF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            DF_TestService.createOrderRecord(userLst.get(0).AccountId);
            test.startTest();
			result = DF_OrderSearchHomeController.getRecords('','',searchStatus,true);
			deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
			test.stopTest();                
        }	                                  
        
	}
    
    @isTest
	private static void test_getRecordsWithValidSearchDate() {
		
		String result = '';
		List<DF_Order__c> deserializedResult = new List<DF_Order__c>();

    	User commUser;
		commUser = DF_TestData.createDFCommUser();

        system.runAs(commUser) {
	    	List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            DF_TestService.createOrderRecord(userLst.get(0).AccountId);
            test.startTest();
			result = DF_OrderSearchHomeController.getRecords('','2018-04-24','',true);
			deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
			test.stopTest();                
        }	                                  
        
	}
}