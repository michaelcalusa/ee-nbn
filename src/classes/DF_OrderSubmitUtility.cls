/**
* Class Name: DF_OrderSubmitUtility
* Description: This class used to perform generic
* operations for Connect and Modify Order
* */
public with sharing class DF_OrderSubmitUtility {
    
    public static void processDFOrder(DF_Order__c dfOrder){
        try{            
            List<DF_Order_Event__e> eventList = new List<DF_Order_Event__e>();
            //string csaId = '';
            Id oppId = dfOrder.DF_Quote__r.Opportunity__c; 
            system.debug('::oppId::'+oppId);               
            //DF_ServiceFeasibilityResponse df_sfJson = (DF_ServiceFeasibilityResponse)Json.deserialize(dfOrder.DF_Quote__r.LAPI_Response__c, DF_ServiceFeasibilityResponse.class);
            //if(df_sfJson != null) {
            //    csaId = df_sfJson.csaId;
            //}
            
            //DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot orderJson =  (DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot)JSON.deserialize(jsonInput, DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot.class);
            //orderJson.productOrder.csaId = csaId;
            
            
            eventList.add(createDFOrderEvent(dfOrder.Id));
            
            updateOppClosedWon(oppId);
            processUpdateDFQuoteStatus(oppId);        
            
            if(eventList.size()>0) {
                List<Database.SaveResult> results = EventBus.publish(eventList);
                for (Database.SaveResult sr : results) {
                    if (sr.isSuccess()) {
                        dfOrder.Order_Status__c = DF_Constants.QUOTE_STATUS_PENDING_SUBMISSION;
                        dfOrder.Order_Submitted_Date__c = System.today();
                        
                        System.debug('Successfully published event.');
                    } else {
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('Error returned: ' + err.getStatusCode() +   ' - ' + err.getMessage());                         
                        }    
                    }
                } 
            }
            
        }
        catch(AsyncException e){
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_OrderSubmitUtility.class.getName(), 'processDFOrder', '', '', '', '', e, 0);
        }
    }
    
    public static DF_Order_Event__e createDFOrderEvent(string orderid){
        DF_Order_Settings__mdt ordSetting = [SELECT OrderAckEventCode__c,OrderAckSource__c,OrderAckBatchSize__c,
                                            OrderCreateBatchSize__c,OrderCreateCode__c,OrderCreateSource__c,
                                            OrderAccBatchSize__c,OrderAccEventCode__c,OrderAccSource__c,
                                            OrderCompleteBatchSize__c,OrderCompleteEventCode__c,
                                            OrderCompleteSource__c
                                                         FROM   DF_Order_Settings__mdt
                                                         WHERE  DeveloperName = 'DFOrderSettings'];
                                                         
        DF_Order_Event__e evt = new DF_Order_Event__e();
        evt.Event_Id__c = ordSetting.OrderCreateCode__c ;
        evt.Event_Record_Id__c = orderid;
        evt.Source__c = ordSetting.OrderCreateSource__c ;        
        //evt.Message_Body__c = dfOrderJSON;
        system.debug('::msg body::'+evt.Message_Body__c);
        return evt;
    }



    public static void updateOppClosedWon(String oppId) {
        //add   String syncResults = DF_CS_API_Util.syncWithOpportunity(basket.Id);
        //add sync check 
        Opportunity op = [SELECT Id FROM Opportunity WHERE Id =:oppId LIMIT 1];
        List<cscfga__Product_Basket__c> baskets = [SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :oppId];
         
         if(!baskets.isEmpty()){
                cscfga__Product_Basket__c basket = baskets[0];
                String syncResults = DF_CS_API_Util.syncWithOpportunity(basket.Id);
                if(syncResults.equalsIgnoreCase('sychronised with opportunity')){
                    DF_DesktopAssessmentController.updateOppClosedWon(op,basket);
                }
            }
         //DF_DesktopAssessmentController.updateOppClosedWon(op, basket) ;  
    }  


    public static void processUpdateDFQuoteStatus(String oppId) {           
        DF_Quote__c dfQuoteToUpdate;
        List<DF_Quote__c> dfQuotesToUpdateList;        

        try {       
            if (String.isNotEmpty(oppId)) {               
                dfQuotesToUpdateList = [SELECT Status__c,Quote_Submitted_Date__c
                                        FROM   DF_Quote__c
                                        WHERE  Opportunity__c = :oppId
                                        AND    Status__c <> :DF_Constants.QUOTE_STATUS_SUBMITTED];                                                   
            }

            // Perform update       
            if (!dfQuotesToUpdateList.isEmpty()) {
                dfQuoteToUpdate = dfQuotesToUpdateList[0];  

                if (dfQuoteToUpdate != null) {
                    dfQuoteToUpdate.Status__c = DF_Constants.QUOTE_STATUS_SUBMITTED;       
                    dfQuoteToUpdate.Quote_Submitted_Date__c = System.today();                                                            
                    update dfQuoteToUpdate;                 
                }
            }                          
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        } 
    }
}