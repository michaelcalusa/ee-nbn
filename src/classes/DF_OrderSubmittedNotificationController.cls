public class DF_OrderSubmittedNotificationController {
    //@AuraEnabled public static List<OpportunityContactRole> lstBizContacts {get; set;}
    
    @AuraEnabled
    public static  List<OrderHistory> getOrderNotifications(string orderId) {
        List<OrderHistory> ohList = new List<OrderHistory>();
        Map<String,List<String>> pickValueMap = PickListUtils.getDependentOptions('DF_Order__c','Order_Status__c','Order_Sub_Status__c');
        //map for timestamp for order history updated and history values
        map<string, List<OrderHistory>> mapActivity = new map<string, List<OrderHistory>>();
        //maplist for dependent picklis values for Substatus
        map<string, string> mapList = new map<string, string>();
        for(string sList : pickValueMap.keySet()){
            
            if(pickValueMap.get(sList).isEmpty()) {
                mapList.put(sList, sList);
            }
            else{
                for(string str : pickValueMap.get(sList)) {
                    mapList.put(str, sList);
                }
            }
        }
        
        List<DF_Order__History> lstHistory = [SELECT toLabel(NewValue), CreatedDate, Field FROM DF_Order__History where parentId =:orderId and 
                     (Field = 'Order_Status__c' OR Field = 'Order_Sub_Status__c' OR Field = 'Appian_Notification_Date__c'
                     OR Field = 'Order_Notification_Sub_Status__c' OR Field = 'Notification_Reason__c' OR Field = 'Reason_Code__c')
                     Order by CreatedDate];
        mapActivity = mapHistory(lstHistory);
        //once we have mapActivity, arranging it to tabular format to showcase on notification UI
        ohList = arrangeHistory(mapActivity, mapList);
        return ohList;        
    }
    
    public static List<OrderHistory> arrangeHistory(map<string, List<OrderHistory>> mapActivityHistory, map<string, string> mapDependentList) {
        List<OrderHistory> orderHistory = new List<OrderHistory>();
        for(string dateOfActivity : mapActivityHistory.KeySet()) {
            List<OrderHistory> ohl = mapActivityHistory.get(dateOfActivity);
            OrderHistory o = new OrderHistory();
            if(dateOfActivity != null)
                o.appianDate = Datetime.valueOf(dateOfActivity);
            for(OrderHistory oh : ohl) {
                
                if(oh.appianDate != null) {
                    o.appianDate = oh.appianDate;
                }
                else if(oh.status != null) {
                    o.status = oh.status;
                }
                else if(oh.subStatus != null) {
                    o.subStatus = oh.subStatus;
                    if(oh.subStatus != null && oh.status == null) {
                        if(mapDependentList!= null && !mapDependentList.isEmpty())
                            o.status = mapDependentList.get(o.subStatus);
                    }
                } else if(oh.notifySubStatus != null) {
                    o.notifySubStatus = oh.notifySubStatus;
                } else if(oh.reasonCode != null) {
                    o.reasonCode = oh.reasonCode;
                } else if(oh.reasonDesc != null) {
                    o.reasonDesc = oh.reasonDesc;
                }
                
            }
            orderHistory.add(o);   
        }
        system.debug('::'+orderHistory);
        return orderHistory;
    }
    
    public static map<string, List<OrderHistory>> mapHistory(List<DF_Order__History> lstHistory) {
        map<string, List<OrderHistory>> mapActivityHistory = new map<string, List<OrderHistory>>();
        //putting history datetimestamp and history values into mapActivity               
        for(DF_Order__History dfh : lstHistory) {
            OrderHistory oh = new OrderHistory();   
            if(dfh.Field == 'Appian_Notification_Date__c') {
                oh.appianDate = DateTime.valueof(dfh.NewValue);           
            }
            else if(dfh.Field == 'Order_Status__c') {
                oh.status = string.valueof(dfh.NewValue);           
            }
            else if(dfh.Field == 'Order_Sub_Status__c') {
                oh.subStatus = string.valueof(dfh.NewValue);
            } else if(dfh.Field == 'Order_Notification_Sub_Status__c') {
                oh.notifySubStatus = string.valueof(dfh.NewValue);
            } else if(dfh.Field == 'Reason_Code__c') {
                oh.reasonCode = string.valueof(dfh.NewValue);
            } else if(dfh.Field == 'Notification_Reason__c') {
                oh.reasonDesc = string.valueof(dfh.NewValue);
            }
            string dateStr = string.valueof(dfh.CreatedDate);
            if(mapActivityHistory.get(dateStr) != null && !mapActivityHistory.get(dateStr).isEmpty()){
                mapActivityHistory.get(dateStr).add(oh);
            }
            else {
                mapActivityHistory.put(dateStr, new List<OrderHistory> { oh });
            }
        }
        return mapActivityHistory;
    }
    
    public class OrderHistory {
        @AuraEnabled public string status {get; set;}
        //subStatus is equivalent to notificationType
        @AuraEnabled public string subStatus {get; set;}
        @AuraEnabled public DateTime appianDate {get; set;}
        @AuraEnabled public string notifySubStatus {get; set;}
        @AuraEnabled public string reasonCode {get; set;}
        @AuraEnabled public string reasonDesc {get; set;}
    }

}