@isTest
private class DF_OrderSubmittedNotification_Test{
    
    static final String PENDING_REASON = 'New Appointment Required - Access Seeker or End User was unable to provide access to the network boundary point';
    static final String UNPENDING_REASON = 'Reschedule Required - NBN Co network infrastructure Fault has been resolved';
    static final String RESCHED_REASON = 'Reschedule Required';
    static final String ONSCHED_REASON = 'On Schedule';
    
    @testSetup static void testDataSetup() {
        Id oppId = DF_TestService.getQuoteRecords();
        List<Opportunity> oppList = [SELECT Id FROM OPPORTUNITY WHERE Opportunity_Bundle__c = :oppId];
        System.debug('PP oppList:'+oppList);
        Opportunity opp = oppList.get(0);
        
        List<DF_Quote__c> dfQuoteList = [SELECT Id,Address__c,LAPI_Response__c,RAG__c,Fibre_Build_Cost__c,Opportunity_Bundle__c,Opportunity__c
                                     FROM DF_Quote__c WHERE Opportunity__c = :oppList.get(0).id limit 1];

        //List<DF_Order__c> dfOrderLst = new List<DF_Order__c>();
        
        //for(DF_Quote__c qtItem : dfQuoteList){
            DF_Order__c ord = new DF_Order__c();
            ord.DF_Quote__c = dfQuoteList[0].id;
            
            ord.Order_Status__c = 'Submitted';
            ord.appian_notification_date__c = DateTime.Now();
            ord.Opportunity_Bundle__c = [Select id from DF_Opportunity_Bundle__c limit 1].Id;
        //    dfOrderLst.add(ord);
        //}

        insert ord;
        
        
    }
    
    @isTest static void getNotificationTest() {
        DF_Order__c ordr = [select Order_Status__c, Order_Sub_Status__c from DF_Order__c limit 1];
        ordr.Order_Sub_Status__c = 'ConstructionStarted';
        ordr.Order_Status__c = 'InProgress';
        ordr.appian_notification_date__c = DateTime.Now();
        update ordr;
        List<DF_Order__c> orders = [select Order_Status__c, Order_Sub_Status__c , appian_notification_date__c  from DF_Order__c];
        //DF_Order__History dfo = [SELECT toLabel(NewValue), CreatedDate, Field FROM DF_Order__History where parentId =:ordr.Id and (Field = 'Order_Status__c' OR Field = 'Order_Sub_Status__c' OR Field = 'Appian_Notification_Date__c')];
        system.debug('DFO'+orders);
        
        List<DF_Order__History> lstHistory = new List<DF_Order__History>();
        DF_Order__History h1 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Status__c');
        DF_Order__History h2 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Sub_Status__c');
        DF_Order__History h3 = new DF_Order__History(parentId =ordr.Id, Field = 'Appian_Notification_Date__c');
        
        lstHistory.add(h1);
        lstHistory.add(h2);
        lstHistory.add(h3);
        map<string, List<DF_OrderSubmittedNotificationController.OrderHistory>> mapDateHistory = DF_OrderSubmittedNotificationController.mapHistory(lstHistory);
        DF_OrderSubmittedNotificationController.arrangeHistory(mapDateHistory, null);
        DF_OrderSubmittedNotificationController.getOrderNotifications(ordr.Id);
    }
    
    @isTest static void getNotificationPendingTest() {
        DF_Order__c ordr = [select Order_Status__c,Order_Notification_Sub_Status__c,Order_Sub_Status__c,Reason_Code__c, Notification_Reason__c from DF_Order__c limit 1];
        ordr.Order_Sub_Status__c = 'RSPActionRequired';
        ordr.Order_Status__c = 'InProgress';
        ordr.Order_Notification_Sub_Status__c = 'Pending';
        ordr.Reason_Code__c = 'EACS1560';
        ordr.Notification_Reason__c = PENDING_REASON;
        ordr.appian_notification_date__c = DateTime.Now();
        update ordr;
        system.debug('DFO for Pending'+ordr);
        Test.startTest();
        List<DF_Order__History> lstHistory = new List<DF_Order__History>();
        DF_Order__History h1 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Status__c');
        DF_Order__History h2 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Sub_Status__c');
        DF_Order__History h3 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Notification_Sub_Status__c');
        DF_Order__History h4 = new DF_Order__History(parentId =ordr.Id, Field = 'Reason_Code__c');
        DF_Order__History h5 = new DF_Order__History(parentId =ordr.Id, Field = 'Notification_Reason__c');
        DF_Order__History h6 = new DF_Order__History(parentId =ordr.Id, Field = 'Appian_Notification_Date__c');
        lstHistory.add(h1);
        lstHistory.add(h2);
        lstHistory.add(h3);
        lstHistory.add(h4);
        lstHistory.add(h5);
        lstHistory.add(h6);
        insert lstHistory;
        map<string, List<DF_OrderSubmittedNotificationController.OrderHistory>> mapDateHistory = DF_OrderSubmittedNotificationController.mapHistory(lstHistory);
        DF_OrderSubmittedNotificationController.arrangeHistory(mapDateHistory, null);
        DF_OrderSubmittedNotificationController.getOrderNotifications(ordr.Id);
        Test.stopTest();
        system.assert(lstHistory.size() > 0);
        system.assert(mapDateHistory.size() > 0);
    }

    @isTest static void getNotificationUnPendingTest() {
        DF_Order__c ordr = [select Order_Status__c,Order_Notification_Sub_Status__c,Order_Sub_Status__c,Reason_Code__c, Notification_Reason__c from DF_Order__c limit 1];
        ordr.Order_Sub_Status__c = 'RSPActionCompleted';
        ordr.Order_Status__c = 'InProgress';
        ordr.Order_Notification_Sub_Status__c = null;
        ordr.Reason_Code__c = 'XEACS1560';
        ordr.Notification_Reason__c = UNPENDING_REASON;
        ordr.appian_notification_date__c = DateTime.Now();
        update ordr;
        system.debug('DFO for UnPending'+ordr);
        Test.startTest();
        List<DF_Order__History> lstHistory = new List<DF_Order__History>();
        DF_Order__History h1 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Status__c');
        DF_Order__History h2 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Sub_Status__c');
        DF_Order__History h3 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Notification_Sub_Status__c');
        DF_Order__History h4 = new DF_Order__History(parentId =ordr.Id, Field = 'Reason_Code__c');
        DF_Order__History h5 = new DF_Order__History(parentId =ordr.Id, Field = 'Notification_Reason__c');
        DF_Order__History h6 = new DF_Order__History(parentId =ordr.Id, Field = 'Appian_Notification_Date__c');
        lstHistory.add(h1);
        lstHistory.add(h2);
        lstHistory.add(h3);
        lstHistory.add(h4);
        lstHistory.add(h5);
        lstHistory.add(h6);
        insert lstHistory;
        map<string, List<DF_OrderSubmittedNotificationController.OrderHistory>> mapDateHistory = DF_OrderSubmittedNotificationController.mapHistory(lstHistory);
        DF_OrderSubmittedNotificationController.arrangeHistory(mapDateHistory, null);
        DF_OrderSubmittedNotificationController.getOrderNotifications(ordr.Id);
        Test.stopTest();
        system.assert(lstHistory.size() > 0);
        system.assert(mapDateHistory.size() > 0);
    }

    @isTest static void getNotificationReschedTest() {
        DF_Order__c ordr = [select Order_Status__c,Order_Notification_Sub_Status__c,Order_Sub_Status__c,Reason_Code__c, Notification_Reason__c from DF_Order__c limit 1];
        ordr.Order_Sub_Status__c = 'OrderRescheduled';
        ordr.Order_Status__c = 'InProgress';
        ordr.Reason_Code__c = 'EACS1562';
        ordr.Notification_Reason__c = RESCHED_REASON;
        ordr.appian_notification_date__c = DateTime.Now();
        update ordr;
        Test.startTest();
        List<DF_Order__History> lstHistory = new List<DF_Order__History>();
        DF_Order__History h1 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Status__c');
        DF_Order__History h2 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Sub_Status__c');
        DF_Order__History h3 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Notification_Sub_Status__c');
        DF_Order__History h4 = new DF_Order__History(parentId =ordr.Id, Field = 'Reason_Code__c');
        DF_Order__History h5 = new DF_Order__History(parentId =ordr.Id, Field = 'Notification_Reason__c');
        DF_Order__History h6 = new DF_Order__History(parentId =ordr.Id, Field = 'Appian_Notification_Date__c');
        lstHistory.add(h1);
        lstHistory.add(h2);
        lstHistory.add(h3);
        lstHistory.add(h4);
        lstHistory.add(h5);
        lstHistory.add(h6);
        insert lstHistory;
        map<string, List<DF_OrderSubmittedNotificationController.OrderHistory>> mapDateHistory = DF_OrderSubmittedNotificationController.mapHistory(lstHistory);
        DF_OrderSubmittedNotificationController.arrangeHistory(mapDateHistory, null);
        DF_OrderSubmittedNotificationController.getOrderNotifications(ordr.Id);
        Test.stopTest();
        system.assert(lstHistory.size() > 0);
        system.assert(mapDateHistory.size() > 0);
    }

    @isTest static void getNotificationOnschedTest() {
        DF_Order__c ordr = [select Order_Status__c,Order_Notification_Sub_Status__c,Order_Sub_Status__c,Reason_Code__c, Notification_Reason__c from DF_Order__c limit 1];
        ordr.Order_Sub_Status__c = 'OrderOnSchedule';
        ordr.Order_Status__c = 'InProgress';
        ordr.Reason_Code__c = 'EACS1563';
        ordr.Notification_Reason__c = ONSCHED_REASON;
        ordr.appian_notification_date__c = DateTime.Now();
        update ordr;
        Test.startTest();
        List<DF_Order__History> lstHistory = new List<DF_Order__History>();
        DF_Order__History h1 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Status__c');
        DF_Order__History h2 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Sub_Status__c');
        DF_Order__History h3 = new DF_Order__History(parentId =ordr.Id, Field = 'Order_Notification_Sub_Status__c');
        DF_Order__History h4 = new DF_Order__History(parentId =ordr.Id, Field = 'Reason_Code__c');
        DF_Order__History h5 = new DF_Order__History(parentId =ordr.Id, Field = 'Notification_Reason__c');
        DF_Order__History h6 = new DF_Order__History(parentId =ordr.Id, Field = 'Appian_Notification_Date__c');
        lstHistory.add(h1);
        lstHistory.add(h2);
        lstHistory.add(h3);
        lstHistory.add(h4);
        lstHistory.add(h5);
        lstHistory.add(h6);
        insert lstHistory;
        map<string, List<DF_OrderSubmittedNotificationController.OrderHistory>> mapDateHistory = DF_OrderSubmittedNotificationController.mapHistory(lstHistory);
        DF_OrderSubmittedNotificationController.arrangeHistory(mapDateHistory, null);
        DF_OrderSubmittedNotificationController.getOrderNotifications(ordr.Id);
        Test.stopTest();
        system.assert(lstHistory.size() > 0);
        system.assert(mapDateHistory.size() > 0);
    }
    
}