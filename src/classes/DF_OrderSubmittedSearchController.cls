public with sharing class DF_OrderSubmittedSearchController {
    
    /*
    * Fetches order records for the given search parameters
    * Parameters : searchTerm, serachDate, searchStatus, isTriggerBySearch
    * @Return : Returns a serialized string of list of DF Order records
    */
    
    public static final String LOC_ID = 'LOC'; 
    public static final String BUNDLE_ID = 'EEB-'; 
    public static final String ORDER_ID = 'BCO'; 
    public static final String ORDER_STATUS_DRAFT = 'In Draft';
    public static final String ORDER_STATUS_FEXP = 'Feasibility Expired';
    public static final String PROD_ID = 'BPI';
    public static List<String> dontProcess = new List<String>{'In Draft', 'Feasibility Expired'};

    @AuraEnabled
    public static String getRecords(String searchTerm, String searchDate, String searchStatus, Boolean isTriggerBySearch) {
        System.debug('PPPP Search Term value is : ' + searchTerm);  
        System.debug('PPPP Search Date value is : ' + searchDate);
        System.debug('PPPP Search Status value is : ' + searchStatus);
        System.debug('PPPP isTriggerBySearch : ' + isTriggerBySearch);
        
        //get the EE recordTypeId for DF_Order__c
        string orderRTId = DF_CS_API_Util.getRecordType(DF_Constants.ENTERPRISE_ETHERNET_NAME, DF_Constants.ORDER_OBJECT);
        
        List<DF_Order__c> orderList = new List<DF_Order__c>();
        try {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            System.debug('PPPP User details ' + userLst);
            
            if(userLst.size()>0 && userLst.get(0).AccountId!=null){
                if(String.isBlank(searchTerm) && String.isBlank(searchDate) && String.isBlank(searchStatus) && !isTriggerBySearch){
                //  String queryStr = 'SELECT Id, Name, Order_Id__c, OrderType__c, Order_SubType__c, Order_Submitted_Date__c, toLabel(Order_Status__c), BPI_Id__c, DF_Quote__r.Location_Id__c, DF_Quote__r.Address__c, Opportunity_Bundle__r.Name FROM DF_Order__c WHERE toLabel(Order_Status__c) != ' + '\'' + ORDER_STATUS_DRAFT + '\' AND recordTypeId = ' + '\'' + orderRTId + '\' AND Opportunity_Bundle__r.Account__c = ' + '\'' + userLst.get(0).AccountId + '\' ORDER BY Order_Submitted_Date__c DESC NULLS FIRST';
                    String queryStr = 'SELECT Id, Name, Order_Id__c, OrderType__c, Order_SubType__c, Order_Submitted_Date__c, toLabel(Order_Status__c), BPI_Id__c, DF_Quote__r.Location_Id__c, DF_Quote__r.Address__c, Opportunity_Bundle__r.Name FROM DF_Order__c WHERE (toLabel(Order_Status__c) != ' + '\'' + ORDER_STATUS_DRAFT + '\' AND toLabel(Order_Status__c) != ' + '\'' + ORDER_STATUS_FEXP + '\') AND recordTypeId = ' + '\'' + orderRTId + '\' AND Opportunity_Bundle__r.Account__c = ' + '\'' + userLst.get(0).AccountId + '\' ORDER BY Order_Submitted_Date__c DESC NULLS FIRST';
                    orderList = (List<DF_Order__c>)DF_CS_API_Util.getQueryRecords(queryStr);
                }
                else if((String.isNotBlank(searchTerm) || String.isNotBlank(searchDate) || String.isNotBlank(searchStatus)) && isTriggerBySearch){
                    String locId,ordId,bundleId,prodId;
                //  String queryStr = 'SELECT Id, Name, Order_Id__c, OrderType__c, Order_SubType__c, Order_Submitted_Date__c, toLabel(Order_Status__c), BPI_Id__c, DF_Quote__r.Location_Id__c, DF_Quote__r.Address__c, Opportunity_Bundle__r.Name FROM DF_Order__c WHERE toLabel(Order_Status__c) != ' + '\'' + ORDER_STATUS_DRAFT + '\' AND recordTypeId = ' + '\'' + orderRTId + '\' AND Opportunity_Bundle__r.Account__c = ' + '\'' + userLst.get(0).AccountId + '\'';
                    String queryStr = 'SELECT Id, Name, Order_Id__c, OrderType__c, Order_SubType__c, Order_Submitted_Date__c, toLabel(Order_Status__c), BPI_Id__c, DF_Quote__r.Location_Id__c, DF_Quote__r.Address__c, Opportunity_Bundle__r.Name FROM DF_Order__c WHERE (toLabel(Order_Status__c) != ' + '\'' + ORDER_STATUS_DRAFT + '\' AND toLabel(Order_Status__c) != ' + '\'' + ORDER_STATUS_FEXP + '\') AND recordTypeId = ' + '\'' + orderRTId + '\' AND Opportunity_Bundle__r.Account__c = ' + '\'' + userLst.get(0).AccountId + '\'';
                    if(String.isNotBlank(searchTerm)){
                        if(searchTerm.startsWithIgnoreCase(LOC_ID)){
                            locId = searchTerm;
                            queryStr += ' AND DF_Quote__r.Location_Id__c = ' + '\'' + locId + '\'';
                        }
                        else if(searchTerm.startsWithIgnoreCase(BUNDLE_ID)){
                            bundleId = searchTerm;
                            queryStr += ' AND Opportunity_Bundle__r.Name = ' + '\'' + bundleId + '\'';
                        }
                        else if(searchTerm.startsWithIgnoreCase(ORDER_ID)){
                            ordId = searchTerm;
                            queryStr += ' AND Order_Id__c = ' + '\'' + ordId + '\'';
                        } else if(searchTerm.startsWithIgnoreCase(PROD_ID)) {
                            prodId = searchTerm;
                            queryStr += ' AND BPI_Id__c = ' + '\'' + prodId + '\'';
                        }
                        else{
                            List<DF_Order__c> orderEmptyList = new List<DF_Order__c>();
                            return JSON.serialize(orderEmptyList);
                        }
                    }
                    if(String.isNotBlank(searchDate))
                        queryStr += ' AND Order_Submitted_Date__c = ' + String.escapeSingleQuotes(searchDate);
                    if(String.isNotBlank(searchStatus))
                        queryStr += ' AND toLabel(Order_Status__c) = ' + '\'' + searchStatus + '\'';
                    System.debug('PPPP queryStr: '+queryStr);
                    queryStr += ' ORDER BY Order_Submitted_Date__c DESC NULLS FIRST';
                    orderList = (List<DF_Order__c>)DF_CS_API_Util.getQueryRecords(queryStr);
                    System.debug('PPPP orderList: '+orderList);
                }
            }else{
                throw new CustomException('User record with the RSP account not found');
            }
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }
        return JSON.serialize(orderList);
    }
    
    /*
    * Fetches picklist values for Order status field
    * Parameters : N/A
    * @Return : Returns a serialized string of picklist values
    */
    @AuraEnabled
    public static String getPickListValuesIntoList(){
       List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = DF_Order__c.Order_Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
        //    if(!pickListVal.getLabel().equalsIgnoreCase('In Draft'))      
            if(!dontProcess.contains(pickListVal.getLabel()))
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return JSON.serialize(pickListValuesList);
    }
}