/**
* Class Name: DF_OrderSubmittedSummaryController
* Description: This class used to show the DF Order submitted 
* Summary on partner community.
*
* */
public with sharing class DF_OrderSubmittedSummaryController {
    public static List<DF_Order__c> dfOrder;
    public static DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot orderJson;
	//Made it test visible so that test class can set different today date
	@TestVisible static Date todayDate = Date.today();
    //@AuraEnabled public static List<OpportunityContactRole> lstBizContacts {get; set;}
    //@AuraEnabled public static List<OpportunityContactRole> lstSiteContacts {get; set;}
    @AuraEnabled public static OrderSummaryData osData {get; set;}
    @AuraEnabled public static LocationData lData {get; set;}
    @AuraEnabled public static NTDData nData {get; set;}
    @AuraEnabled public static UNIData uData {get; set;}
    @AuraEnabled public static List<OVCData> oDataList {get; set;}
    @AuraEnabled public static List<RelatedContacts> lstBizContacts {get; set;}
    @AuraEnabled public static List<RelatedContacts> lstSiteContacts {get; set;}
    
    public static void getOrderData(string orderId) {   
        lstBizContacts = new List<RelatedContacts>();
        lstSiteContacts = new List<RelatedContacts>();   
        List<RelatedContacts> lstRoles = new List<RelatedContacts>();
        //cpst-1888 added order type
        //cpst-5484 added Opportunity_Bundle__r.Name,Opportunity_Bundle__r.Account__r.Name
        //cpst-64 added Order_Notification_Sub_Status__c
        //CPST-5509 added After_Business_Hours__c
        //PI8 added Revised_Delivery_Date__c
        dfOrder =  [SELECT Name, Order_JSON__c, DF_Quote__r.Opportunity__r.Name, DF_Quote__r.Latitude__c, UNI_Port__c, Order_Id__c,BPI_Id__c,
                            DF_Quote__r.Longitude__c, DF_Quote__r.Opportunity__c, DF_Quote__r.Address__c,  Trading_Name__c,
                            Customer_Required_Date__c, Heritage_Site__c, Induction_Required__c, Security_Required__c, toLabel(Order_Sub_Status__c),
                            Order_Notifier_Response_JSON__c, toLabel(Order_Status__c), DF_Quote__r.Location_Id__c, Service_Region__c,
                            NBN_Commitment_Date__c, UNI_Id__c, BTD_Id__c, DF_Quote__r.Fibre_Build_Cost__c,OrderType__c,Order_SubType__c, Modify_Order_JSON__c,
                            Opportunity_Bundle__r.Name,Opportunity_Bundle__r.Account__r.Name,Order_Notification_Sub_Status__c, After_Business_Hours__c, Revised_Delivery_Date__c
                            from DF_Order__c WHERE Id=:orderId];                                
        if(!dfOrder.isEmpty()) {
            
            if(!string.isEmpty(dfOrder[0].Order_JSON__c)) {
                DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot orderJson = (DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot)JSON.deserialize(dfOrder[0].Order_JSON__c, DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot.class);  
          
                 osData = DF_OrderSubmittedSummaryHelper.getOrderSummaryHelper(osData, dfOrder[0], orderJson);
                
                 lData = DF_OrderSubmittedSummaryHelper.getLocationHelper(lData, dfOrder[0], orderJson);
                
                 nData = DF_OrderSubmittedSummaryHelper.getNTDHelper(nData, dfOrder[0], orderJson);
                 
                 uData = DF_OrderSubmittedSummaryHelper.getUNIHelper(uData, dfOrder[0], orderJson);
                 
                 oDataList = DF_OrderSubmittedSummaryHelper.getOVCHelper(oDataList, dfOrder[0], orderJson);
                 
                 //get biz / site contacts
                 lstRoles = DF_OrderSubmittedSummaryHelper.getContactsHelper(lstRoles, orderJson);
                 for(RelatedContacts opptyRoles : lstRoles) {   
                     if(opptyRoles.contactType == 'Business')
                         lstBizContacts.add(opptyRoles);
                     else if(opptyRoles.contactType == 'Site')  
                         lstSiteContacts.add(opptyRoles);               
                }
            }                                  
                                                           
        }
    }
    
    @AuraEnabled
    public static OrderSummaryData getOrderSummaryDetails(string orderId){
        getOrderData(orderId);
        return osData;
    }                                                          
                                                              
    @AuraEnabled 
    public static LocationData getLocationDetails(string orderId) {
        system.debug('::orderId::'+orderId);
        getOrderData(orderId);
        system.debug('::ord::'+dfOrder);
        
        return lData;  
    }
    
    @AuraEnabled
    public static List<RelatedContacts> getBizContacts(string orderId) {
        if(lstBizContacts == null) {
            getOrderData(orderId);
        }
        return lstBizContacts; 
    }
    
    @AuraEnabled
    public static List<RelatedContacts> getSiteContacts(string orderId) {
        if(lstSiteContacts == null) {
            getOrderData(orderId);
        }
        return lstSiteContacts; 
    }

	/**
	* @description Check If the Order is Cancellable
	* @param orderId - Salesforce record ID of an DF_Order Record
	* @return 
	*/ 
	@AuraEnabled
    public static Boolean isCancellableOrder(string orderId) {

		return EE_OrderInflightCancelService.isCancellableOrder(orderId,todayDate);

    }

	/**
	* @description Trigger the cancellation process on eligble order
	* @param orderId - Salesforce record ID of an DF_Order Record
	* @return void
	*/ 
	@AuraEnabled
    public static void cancelOrder(string orderId) {
		try{
	     EE_OrderInflightCancelService.cancelEEOrderByOrderId(orderId);
	    } catch(Exception e) {
        throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static NTDData getNTDDetails(string orderId) {
        if(nData == null) {
            getOrderData(orderId);
        }
       
        return nData; 
    }
    
    @AuraEnabled
    public static UNIData getUNIDetails(string orderId) {
        if(uData == null) {
            getOrderData(orderId);
        }
       
        return uData; 
    }
    
    @AuraEnabled
    public static List<OVCData> getOVCDetails(string orderId) {
        if(oDataList == null) {
            getOrderData(orderId);
        }       
        return oDataList; 
    }
    //Raja: Added below CPST-1012    
    public class OrderSummaryData{
        @AuraEnabled public string FBC {get; set;}
        @AuraEnabled public string NRC {get; set;}
        @AuraEnabled public string MC {get; set;}    
        
        @AuraEnabled public string SM {get; set;}
        @AuraEnabled public string orderType {get; set;}
        @AuraEnabled public string orderSubType {get; set;}
        @AuraEnabled public string status {get; set;}
        @AuraEnabled public string subStatus {get; set;}
        //CPST-5509
        @AuraEnabled public string afterBH {get; set;}
        //CPST-7074: Michael: added below for updated summary screen
        @AuraEnabled public string bpiId {get; set;}
        @AuraEnabled public string orderId {get; set;}
        @AuraEnabled public string nbnCommitDate {get; set;}
        @AuraEnabled public string customerRequiredDate {get; set;}
        //PI8
        @AuraEnabled public string revisedDeliveryDate {get; set;}
    }
                                                              
    public class LocationData {
        @AuraEnabled public string locId {get; set;}
        @AuraEnabled public string orderId {get; set;}
        @AuraEnabled public string latitude {get; set;}
        @AuraEnabled public string longitude {get; set;}
        @AuraEnabled public string address {get; set;}
        @AuraEnabled public string customerRequiredDate {get; set;}
        @AuraEnabled public string security {get; set;}
        @AuraEnabled public string notes {get; set;}
        @AuraEnabled public string tradingName {get; set;}
        @AuraEnabled public string induction {get; set;}
        @AuraEnabled public string AHA {get; set;}
        @AuraEnabled public string heritageSite {get; set;}
        @AuraEnabled public string bpiId {get; set;}
        @AuraEnabled public string serviceRegion {get; set;}
        @AuraEnabled public string status {get; set;}
        @AuraEnabled public string subStatus {get; set;}
        @AuraEnabled public string nbnCommitDate {get; set;}
        //cpst-1888 added order type
        @AuraEnabled public string orderType {get; set;}
        @AuraEnabled public string orderSubType {get; set;}
        //cpst-5484 added bundleId,access seeker
        @AuraEnabled public string bundleId {get; set;}
        @AuraEnabled public string accessSeeker {get; set;}
    }
    
    public class RelatedContacts {
        public string contactType {get; set;}
        @AuraEnabled public string name {get; set;}
        @AuraEnabled public string role {get; set;}
        @AuraEnabled public string email {get; set;}
        @AuraEnabled public string phone {get; set;}
        @AuraEnabled public string street {get; set;}
        @AuraEnabled public string suburb {get; set;}
        @AuraEnabled public string postcode {get; set;}
        @AuraEnabled public string state {get; set;}
    }
    
    public class NTDData {
        @AuraEnabled public string ntdType {get; set;}
        @AuraEnabled public string powerSupply1 {get; set;}
        @AuraEnabled public string powerSupply2 {get; set;}
        @AuraEnabled public string ntdMounting {get; set;}
        @AuraEnabled public string btdId {get; set;}
    }
    
    public class UNIData {
        @AuraEnabled public string ovcType {get; set;}
        @AuraEnabled public string tpid {get; set;}
        @AuraEnabled public string interfaceBandwidth {get; set;}
        @AuraEnabled public string interfaceTypes {get; set;}
        //@AuraEnabled public string mappingMode {get; set;}
        @AuraEnabled public string uniPort {get; set;}
        @AuraEnabled public string uniTerm {get; set;}
        @AuraEnabled public string AAT {get; set;}
        @AuraEnabled public string uniESLA {get; set;}
        @AuraEnabled public string zone {get; set;}  
        @AuraEnabled public string uniId {get; set;}      
    }
    
    public class OVCData {
        @AuraEnabled public string id {get; set;}
        @AuraEnabled public string POI {get; set;}
        @AuraEnabled public string sTag {get; set;}
        @AuraEnabled public string coSHighBandwidth {get; set;}
        @AuraEnabled public string coSMediumBandwidth {get; set;}
        @AuraEnabled public string coSLowBandwidth {get; set;}
        @AuraEnabled public string NNI {get; set;}
        @AuraEnabled public string routeType {get; set;}
        @AuraEnabled public string ceVlanId {get; set;}
        @AuraEnabled public string ovcMaxFrameSize {get; set;}
        @AuraEnabled public string ovcId {get; set;}
        @AuraEnabled public string mappingMode {get; set;}
    }
}