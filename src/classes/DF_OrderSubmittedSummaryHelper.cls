public class DF_OrderSubmittedSummaryHelper {
    
    public static DF_OrderSubmittedSummaryController.OrderSummaryData getOrderSummaryHelper(DF_OrderSubmittedSummaryController.OrderSummaryData osData, DF_Order__c dfOrder, DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot orderJson){
        Map<String, Decimal> recurringCostsMap = new Map<String, Decimal>();
        Map<String, Decimal> nonRecurringCostsMap = new Map<String, Decimal>();
        Map<String, Decimal> modificationCostsMap = new Map<String, Decimal>();
        
        Set<String> dfQuoteIdSet = new Set<String>();
        dfQuoteIdSet.add(dfOrder.DF_Quote__r.Id);
        //DF_SF_QuoteController.getQuoteCosts(dfQuoteIdSet, recurringCostsMap, nonRecurringCostsMap);
        DF_SF_QuoteController.getQuoteCostsForModify(dfQuoteIdSet, recurringCostsMap, nonRecurringCostsMap, modificationCostsMap);
        
        osData = new DF_OrderSubmittedSummaryController.OrderSummaryData();
        osData.FBC = string.valueOf(dfOrder.DF_Quote__r.Fibre_Build_Cost__c);
        osData.MC = string.valueOf(recurringCostsMap.get(dfOrder.DF_Quote__r.Id));
        osData.NRC = string.valueOf(nonRecurringCostsMap.get(dfOrder.DF_Quote__r.Id));
        osData.SM = string.valueOf(modificationCostsMap.get(dfOrder.DF_Quote__r.Id));
        
        osData.orderType = dfOrder.OrderType__c;
        osData.orderSubType = dfOrder.Order_SubType__c;
        osData.status = dfOrder.Order_status__c;
        //updated sub status to order notification sub status
        osData.subStatus = dfOrder.Order_Notification_Sub_Status__c;
        //CPST-5509
        osData.afterBH = dfOrder.After_Business_Hours__c;
        //CPST-7074: Michael: added below for updated summary screen
        osData.orderId = dfOrder.order_Id__c;
        osData.bpiId = dfOrder.BPI_Id__c;
        if(dfOrder.Customer_Required_Date__c != null)
            osData.customerRequiredDate = string.valueof(dfOrder.Customer_Required_Date__c);
        if(dfOrder.NBN_Commitment_Date__c != null)
            osData.nbnCommitDate = String.valueOf(dfOrder.NBN_Commitment_Date__c);
        if(dfOrder.Revised_Delivery_Date__c != null)
            osData.revisedDeliveryDate = String.valueOf(dfOrder.Revised_Delivery_Date__c);

        return osData;
    }    
    
    public static DF_OrderSubmittedSummaryController.LocationData getLocationHelper(DF_OrderSubmittedSummaryController.LocationData lData, DF_Order__c dfOrder, DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot orderJson) {   
                
        //adding the location data to show on detail screen
        lData = new DF_OrderSubmittedSummaryController.LocationData();
        lData.locId = dfOrder.DF_Quote__r.Location_Id__c;
        lData.orderId = dfOrder.order_Id__c; 
        lData.latitude = dfOrder.DF_Quote__r.Latitude__c;
        lData.longitude = dfOrder.DF_Quote__r.Longitude__c;
        lData.address = dfOrder.DF_Quote__r.Address__c;
        lData.bpiId = dfOrder.BPI_Id__c;
        lData.tradingName = dfOrder.Trading_Name__c;
        lData.heritageSite = dfOrder.Heritage_Site__c; 
        if(dfOrder.Customer_Required_Date__c != null)
            lData.customerRequiredDate = string.valueof(dfOrder.Customer_Required_Date__c);
        lData.induction = dfOrder.Induction_Required__c;
        lData.security = dfOrder.Security_Required__c;
        lData.serviceRegion = dfOrder.Service_Region__c;
        lData.status = dfOrder.Order_status__c;
        //updated sub status to order notification sub status
        lData.subStatus = dfOrder.Order_Notification_Sub_Status__c;
        //cpst-1888 added order type
        lData.orderType = dfOrder.OrderType__c;
        lData.orderSubType = dfOrder.Order_SubType__c;
        //cpst-5484 added Opportunity_Bundle__r.Name
        lData.bundleId = dfOrder.Opportunity_Bundle__r.Name;
        lData.accessSeeker = dfOrder.Opportunity_Bundle__r.Account__r.Name;
        if(dfOrder.NBN_Commitment_Date__c != null)
            lData.nbnCommitDate = String.valueOf(dfOrder.NBN_Commitment_Date__c);
        if(orderJson != null) { //getting the OrderJson from getOrderData(orderId) method
                                          
            DF_OrderDataJSONWrapper.productOrder objPrdOrd = orderJson.productOrder;
            if(objPrdOrd != null) {
                lData.AHA = objPrdOrd.afterHoursAppointment;                
                lData.notes = objPrdOrd.notes;
            }
        }
        
        return lData;    
    }
    
    public static List<DF_OrderSubmittedSummaryController.RelatedContacts> getContactsHelper(List<DF_OrderSubmittedSummaryController.RelatedContacts> lstContacts, DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot orderJson) {
       /* return [SELECT Contact.Name, Contact.Email, Role, Contact.MailingCity, 
                            Contact.Phone, Contact.MailingState, Contact.MailingPostalCode,
                            Contact.MailingStreet
                            from OpportunityContactRole Where OpportunityId = :opptyId]; */
        DF_OrderDataJSONWrapper.productOrder objPrdOrd = orderJson.productOrder;
        if(objPrdOrd != null) {
            List<DF_OrderDataJSONWrapper.ItemInvolvesContact> contacts = objPrdOrd.itemInvolvesContact;
            if(contacts != null && !contacts.isEmpty()) {
                for(DF_OrderDataJSONWrapper.ItemInvolvesContact con : contacts) {
                    DF_OrderSubmittedSummaryController.RelatedContacts relContact = new DF_OrderSubmittedSummaryController.RelatedContacts();
                    relContact.contactType = con.Type;
                    relContact.name = con.contactName;
                    relContact.role = con.role;
                    relContact.email = con.emailAddress;
                    relContact.phone = con.phoneNumber;
                    if(con.unstructuredAddress != null) {
                        DF_OrderDataJSONWrapper.UnstructuredAddress addressNode = con.unstructuredAddress;
                        relContact.street = addressNode.addressLine1 +' '+ addressNode.addressLine2 +' '+addressNode.addressLine3;
                        relContact.suburb = addressNode.localityName;
                        relContact.postcode = addressNode.postcode;
                        relContact.state = addressNode.stateTerritoryCode;
                    }
                    lstContacts.add(relContact);
                }
            }
        }                    
        return lstContacts;                                   
    } 
    
    public static DF_OrderSubmittedSummaryController.NTDData getNTDHelper(DF_OrderSubmittedSummaryController.NTDData nData, DF_Order__c dfOrder, DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot orderJson) {   
        if(dfOrder != null) {
            
            if(orderJson != null) { //getting the OrderJson from getOrderData(orderId) method
                                              
                DF_OrderDataJSONWrapper.productOrder objPrdOrd = orderJson.productOrder;
                //adding the NTD details to show on UI   
                if(objPrdOrd != null) {              
                    DF_OrderDataJSONWrapper.ProductOrderComprisedOf objPrdComp = objPrdOrd.productOrderComprisedOf;
                    if(objPrdComp != null) {
                        
                        List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1> lstRefPrdItem = objPrdComp.referencesProductOrderItem;
                        System.debug('::RefPRD::'+lstRefPrdItem);
                        if(lstRefPrdItem != null) {
                            integer countOvc = 0;
                            
                            for(DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 lstRef : lstRefPrdItem) {
                                DF_OrderDataJSONWrapper.ItemInvolvesProductL2 itemPrd = lstRef.itemInvolvesProduct;
                                
                                //NTD/BTD                                
                                if(lstRef.referencesProductOrderItem != null) {                                
                                    System.debug('::itemNTD::'+lstRef.itemInvolvesProduct);
                                                                        
                                    nData = new DF_OrderSubmittedSummaryController.NTDData();
                                    nData.btdId = dfOrder.BTD_Id__c;
                                    nData.ntdType = itemPrd.btdType;
                                    nData.ntdMounting = itemPrd.btdMounting;
                                    nData.powerSupply1 = itemPrd.powerSupply1;
                                    nData.powerSupply2 = itemPrd.powerSupply2;
                                }                                
                            }
                        }
                    }
                }
            }
                                                        
        }            
        return ndata;
    }
    
    public static DF_OrderSubmittedSummaryController.UNIData getUNIHelper(DF_OrderSubmittedSummaryController.UNIData uData, DF_Order__c dfOrder, DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot orderJson) {
        if(dfOrder != null) {
                
            if(orderJson != null) { //getting the OrderJson from getOrderData(orderId) method
                                              
                DF_OrderDataJSONWrapper.productOrder objPrdOrd = orderJson.productOrder;
                if(objPrdOrd != null) {                
                    DF_OrderDataJSONWrapper.ProductOrderComprisedOf objPrdComp = objPrdOrd.productOrderComprisedOf;
                    if(objPrdComp != null) {
                        //getting the UNI data from ItemInvolvesProduct 1st node
                        uData = new DF_OrderSubmittedSummaryController.UNIData();
                        uData.uniPort = dfOrder.UNI_Port__c;
                        uData.uniId = dfOrder.UNI_Id__c;
                        DF_OrderDataJSONWrapper.ItemInvolvesProductL1 objItemUni = objPrdComp.itemInvolvesProduct;
                        if(objItemUni != null) {
                            uData.uniESLA = objItemUni.serviceRestorationSLA;
                            uData.zone = objItemUni.zone;                            
                            uData.uniTerm = objItemUni.term; 
                            uData.AAT = objItemUni.accessAvailabilityTarget;        
                        }
                        
                        List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1> lstRefPrdItem = objPrdComp.referencesProductOrderItem;
                        System.debug('::RefPRD::'+lstRefPrdItem);
                        if(lstRefPrdItem != null) {
                            integer countOvc = 0;
                            
                            for(DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 lstRef : lstRefPrdItem) {
                                DF_OrderDataJSONWrapper.ItemInvolvesProductL2 itemPrd = lstRef.itemInvolvesProduct;
                                                                
                                if(lstRef.referencesProductOrderItem != null) {
                                    //UNI data from ItemInvolvesProductL3 node
                                    List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL2> lstUni = lstRef.referencesProductOrderItem;
                                    if(lstUni != null && lstUni.size() > 0) {
                                        DF_OrderDataJSONWrapper.ItemInvolvesProductL3 objUni = lstUni[0].itemInvolvesProduct;
                                        if(objUni != null) {
                                            uData.ovcType = objUni.ovcType;
                                            uData.tpid = objUni.tpId;
                                            uData.interfaceBandwidth = objUni.interfaceBandwidth ;
                                            uData.interfaceTypes = objUni.interfaceType;
                                            //uData.mappingMode = objUni.cosMappingMode;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
                                                         
        }
        return uData;
    }
    
    public static List<DF_OrderSubmittedSummaryController.OVCData> getOvcHelper(List<DF_OrderSubmittedSummaryController.OVCData> oDataList, DF_Order__c dfOrder, DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot orderJson) {
         oDataList = new List<DF_OrderSubmittedSummaryController.OVCData>();
         if(dfOrder != null) {
            if(dfOrder.Order_Status__c != 'In Draft' && dfOrder.Order_Status__c != 'Pending Submission' && dfOrder.Order_Status__c != 'Submitted' && !string.isEmpty(dfOrder.Order_Notifier_Response_JSON__c) && dfOrder.OrderType__c == 'Connect') {
                oDataList = getOVCDetailsWithId(oDataList, dfOrder.Order_Notifier_Response_JSON__c, dfOrder);
            }
            else if(dfOrder.Order_Status__c != 'In Draft' && dfOrder.Order_Status__c != 'Pending Submission' && dfOrder.Order_Status__c != 'Submitted' && dfOrder.Order_Status__c != 'Acknowledged' && !string.isEmpty(dfOrder.Order_Notifier_Response_JSON__c) && dfOrder.OrderType__c == 'Modify') {
                if(dfOrder.Order_SubType__c=='Change Service Restoration SLA'){
                    oDataList = getOVCDetailsFromOrderJSON(orderJson, dfOrder);
                }else{
                    oDataList = getOVCDetailsWithId(oDataList, dfOrder.Order_Notifier_Response_JSON__c, dfOrder);     
                }
            }
            if(oDataList.isEmpty()) {
                if(orderJson != null) {
                    oDataList = getOVCDetailsFromOrderJSON(orderJson, dfOrder);
                    /*
                    DF_OrderDataJSONWrapper.productOrder objPrdOrd = orderJson.productOrder;
                    if(objPrdOrd != null) {
                        DF_OrderDataJSONWrapper.ProductOrderComprisedOf objPrdComp = objPrdOrd.productOrderComprisedOf;
                        if(objPrdComp != null) {
                            List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1> lstRefPrdItem = objPrdComp.referencesProductOrderItem;
                            System.debug('::RefPRD::'+lstRefPrdItem);
                            if(lstRefPrdItem != null) {
                                integer countOvc = 0;
                                
                                for(DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 lstRef : lstRefPrdItem) {
                                    DF_OrderDataJSONWrapper.ItemInvolvesProductL2 itemPrd = lstRef.itemInvolvesProduct;
                                    
                                    //OVC                            
                                    if(lstRef.referencesProductOrderItem == null) {
                                        if(itemPrd.type == 'OVC') {
                                            countOvc++;
                                            DF_OrderSubmittedSummaryController.OVCData oData = new DF_OrderSubmittedSummaryController.OVCData();
                                            oData.ovcId = itemPrd.id;
                                            oData.id = string.valueOf(countOvc);
                                            oData.POI = itemPrd.poi;
                                            oData.sTag = string.valueOf(itemPrd.sVLanId);
                                            oData.coSHighBandwidth = itemPrd.cosHighBandwidth;
                                            oData.coSMediumBandwidth = itemPrd.coSMediumBandwidth;
                                            oData.coSLowBandwidth = itemPrd.coSLowBandwidth;
                                            oData.NNI = itemPrd.nni;
                                            oData.routeType = itemPrd.routeType;
                                            oData.ceVlanId = itemPrd.uniVLanId;
                                            oData.mappingMode = itemPrd.cosMappingMode;
                                            oData.ovcMaxFrameSize = itemPrd.maximumFrameSize;     
                                            oDataList.add(oData);                                        
                                        }
                                    }
                                }
                            }
                        }
                    }*/
                }
            }                                                             
        }
         
        return oDataList;
    } 
    
    public static List<DF_OrderSubmittedSummaryController.OVCData> getOVCDetailsFromOrderJSON(DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot orderJson, DF_Order__c dfOrder){
        List<DF_OrderSubmittedSummaryController.OVCData> ovcList = new List<DF_OrderSubmittedSummaryController.OVCData>();
        DF_OrderDataJSONWrapper.productOrder objPrdOrd = orderJson.productOrder;
        if(objPrdOrd != null) {
            DF_OrderDataJSONWrapper.ProductOrderComprisedOf objPrdComp = objPrdOrd.productOrderComprisedOf;
            if(objPrdComp != null) {
                List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1> lstRefPrdItem = objPrdComp.referencesProductOrderItem;
                System.debug('::RefPRD::'+lstRefPrdItem);
                if(lstRefPrdItem != null) {
                    integer countOvc = 0;
                    
                    for(DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 lstRef : lstRefPrdItem) {
                        DF_OrderDataJSONWrapper.ItemInvolvesProductL2 itemPrd = lstRef.itemInvolvesProduct;
                        
                        //OVC                            
                        if(lstRef.referencesProductOrderItem == null) {
                            if(itemPrd.type == 'OVC') {
                                countOvc++;
                                DF_OrderSubmittedSummaryController.OVCData oData = new DF_OrderSubmittedSummaryController.OVCData();
                                oData.ovcId = itemPrd.id;
                                oData.id = string.valueOf(countOvc);
                                oData.POI = itemPrd.poi;
                                oData.sTag = string.valueOf(itemPrd.sVLanId);
                                oData.coSHighBandwidth = itemPrd.cosHighBandwidth;
                                oData.coSMediumBandwidth = itemPrd.coSMediumBandwidth;
                                oData.coSLowBandwidth = itemPrd.coSLowBandwidth;
                                oData.NNI = itemPrd.nni;
                                oData.routeType = itemPrd.routeType;
                                oData.ceVlanId = itemPrd.uniVLanId;
                                oData.mappingMode = itemPrd.cosMappingMode;
                                oData.ovcMaxFrameSize = itemPrd.maximumFrameSize;     
                                ovcList.add(oData);                                        
                            }
                        }
                    }
                }
            }
        }
        return ovcList;        
    }

    public static List<DF_OrderSubmittedSummaryController.OVCData> getOVCDetailsWithId(List<DF_OrderSubmittedSummaryController.OVCData> ovcList, string orderNotifier, DF_Order__c dfOrder) {
        //adding the code to get latest values from Modify order
        /*map<string, ModifyOrderOVC> mapModifyData = new map<string, ModifyOrderOVC>();
        if(dfOrder.OrderType__c == 'Modify' && !string.isEmpty(dfOrder.Modify_Order_JSON__c)) {
            mapModifyData = getModifyOrderOVCDetails(dfOrder.Modify_Order_JSON__c, mapModifyData);   
        }*/
        
        DF_OrderInboundEventHandler.ProductOrd objPrdOrd = (DF_OrderInboundEventHandler.ProductOrd)JSON.deserialize(orderNotifier, DF_OrderInboundEventHandler.ProductOrd.class);  
        if(objPrdOrd != null) {
            DF_OrderInboundEventHandler.ProductOrderComprisedOf objPrdComp = objPrdOrd.productOrderComprisedOf;
            if(objPrdComp != null) {
                List<DF_OrderInboundEventHandler.ReferencesProductOrderItem> lstRefPrdItem = objPrdComp.referencesProductOrderItem;
                System.debug('::RefPRD Id::'+lstRefPrdItem);
                if(lstRefPrdItem != null) {
                    integer countOvc = 0;
                    
                    for(DF_OrderInboundEventHandler.ReferencesProductOrderItem lstRef : lstRefPrdItem) {
                        DF_OrderInboundEventHandler.ItemInvolvesProduct2 itemPrd = lstRef.itemInvolvesProduct;
                        
                        //OVC                    
                        if(lstRef.referencesProductOrderItem == null) {
                            if(itemPrd.type == 'OVC') {                                
                                countOvc++;
                                DF_OrderSubmittedSummaryController.OVCData oData = new DF_OrderSubmittedSummaryController.OVCData();  
                                oData.id = string.valueOf(countOvc);
                                oData.POI = itemPrd.poi;
                                oData.sTag = string.valueOf(itemPrd.sVLanId);
                                /*if(!mapModifyData.isEmpty() && mapModifyData.get(itemPrd.Id) != null) {
                                    //checking if the value is changed from connect or not
                                    
                                    oData.coSHighBandwidth = mapModifyData.get(itemPrd.Id).high;
                                    if(oData.coSHighBandwidth==null){
                                        oData.coSHighBandwidth=itemPrd.coSHighBandwidth;
                                    }
                                    oData.coSMediumBandwidth =  mapModifyData.get(itemPrd.Id).medium;
                                    if(oData.coSMediumBandwidth==null){
                                        oData.coSMediumBandwidth=itemPrd.coSMediumBandwidth;
                                    }
                                    oData.coSLowBandwidth = mapModifyData.get(itemPrd.Id).low;
                                    if(oData.coSLowBandwidth==null){
                                        oData.coSLowBandwidth=itemPrd.coSLowBandwidth;
                                    }
                                    
                                    oData.mappingMode = mapModifyData.get(itemPrd.Id).mappingMode;
                                    if(oData.mappingMode==null){
                                        oData.mappingMode=itemPrd.cosMappingMode;
                                    }
                                    
                                }
                                else {*/
                                oData.coSHighBandwidth = itemPrd.cosHighBandwidth;
                                oData.coSMediumBandwidth = itemPrd.coSMediumBandwidth;
                                oData.coSLowBandwidth = itemPrd.coSLowBandwidth;
                                oData.mappingMode = itemPrd.cosMappingMode;
                                //}
                                oData.NNI = itemPrd.nni;
                                oData.routeType = itemPrd.routeType;
                                oData.ceVlanId = itemPrd.uniVLanId;                                
                                oData.ovcMaxFrameSize = itemPrd.maximumFrameSize;
                                oData.ovcId = itemPrd.Id;                                        
                                ovcList.add(oData);                                        
                            }
                            
                        }
                    }
                }
            }
        }  
        return ovcList;
    } 
    
    /*public static map<string, ModifyOrderOVC> getModifyOrderOVCDetails(string modifyOrderJson, map<string, ModifyOrderOVC> mapModifyOrderData) {
        DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot orderJson = (DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot)JSON.deserialize(modifyOrderJson, DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot.class);        
        List<DF_OrderSubmittedSummaryController.OVCData> oDataList = new List<DF_OrderSubmittedSummaryController.OVCData>();
        if(orderJson != null) { 
            DF_OrderDataJSONWrapper.productOrder objPrdOrd = orderJson.productOrder;
            if(objPrdOrd != null) {
                DF_OrderDataJSONWrapper.ProductOrderComprisedOf objPrdComp = objPrdOrd.productOrderComprisedOf;
                if(objPrdComp != null) {
                    List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1> lstRefPrdItem = objPrdComp.referencesProductOrderItem;
                    System.debug('::RefPRD::'+lstRefPrdItem);
                    if(lstRefPrdItem != null) {
                        for(DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 lstRef : lstRefPrdItem) {
                            DF_OrderDataJSONWrapper.ItemInvolvesProductL2 itemPrd = lstRef.itemInvolvesProduct;
                            
                            //OVC                            
                            if(lstRef.referencesProductOrderItem == null) {
                                if(itemPrd.type == 'OVC' && lstRef.Action == 'MODIFY') {
                                    ModifyOrderOVC objModifyData = new ModifyOrderOVC();
                                    //if(!string.isEmpty(itemPrd.cosHighBandwidth))
                                        objModifyData.high = itemPrd.cosHighBandwidth;
                                    //if(!string.isEmpty(itemPrd.coSLowBandwidth))    
                                        objModifyData.low = itemPrd.coSLowBandwidth;
                                    //if(!string.isEmpty(itemPrd.coSMediumBandwidth))
                                        objModifyData.medium = itemPrd.coSMediumBandwidth;
                                    //if(!string.isEmpty(itemPrd.cosMappingMode))    
                                        objModifyData.mappingMode = itemPrd.cosMappingMode;
                                    mapModifyOrderData.put(itemPrd.Id, objModifyData);                                                                            
                                }
                            }
                        }
                    }
                }
            }
        }
        return mapModifyOrderData;
    }
    
    public class ModifyOrderOVC {
        public string high {get; set;}
        public string low {get; set;}
        public string medium {get; set;}
        public string mappingMode {get; set;}
    } */             
}