/**
* Class for testing DF_OrderSubmittedSummaryController and Helper methods.
*/
@isTest
public class DF_OrderSubmittedSummaryTest {
    public static final String orderSubmitStr = '{ "productOrder":{ "accessSeekerInteraction":{ "billingAccountID":"BAN000000892801" }, "orderType":"Connect", "customerRequiredDate":"2017-02-25", "afterHoursAppointment":"Yes", "inductionRequired":"No", "securityClearance":"No", "notes":"Order Note Added", "tradingName":"Lacrosse", "heritageSite":"No", "csaId":"VIC", "itemInvolvesContact":[ { "type":"Business", "contactName":"Bob Tompson", "role":"Manager", "emailAddress":"bobt@example.com", "phoneNumber":"0412 345 443", "unstructuredAddress":{ "addressLine1":"Suite 2", "addressLine2":"Level 5", "addressLine3":"380 Doclands Drive", "localityName":"Melbourne CBD", "postcode":"3000", "stateTerritoryCode":"VIC" } }, { "type":"Site", "contactName":"Samantha Greg", "role":"", "emailAddress":"sgreg@example.com", "phoneNumber":"0412 345 443", "unstructuredAddress":{ "addressLine1":"Suite 2", "addressLine2":"Level 6", "addressLine3":"652 Latrobe St", "localityName":"Melbourne CBD", "postcode":"3000", "stateTerritoryCode":"VIC" } } ], "productOrderComprisedOf":{ "action":"ADD", "itemInvolvesLocation":{ "id":"LOC000000160801" }, "itemInvolvesProduct":{ "term":"12 Months", "serviceRestorationSLA":"Enhanced-6 (24/7)", "accessAvailabilityTarget":"99.95% (Dual Uplink/same Network Aggregation element)", "zone":"CBD", "productType":"EE", "templateId":"TPL", "templateVersion":"1.0" }, "referencesProductOrderItem":[ { "action":"ADD", "itemInvolvesProduct":{ "type":"BTD", "ntdType":"Standard (CSAS)", "ntdMounting":"Rack", "powerSupply1":"DC(-d8V to -22V)", "powerSupply2":"Not Required" }, "referencesProductOrderItem":[ { "action":"ADD", "itemInvolvesProduct":{ "type":"UNIE", "interfaceBandwidth":"5 GB", "portId":null, "interfaceType":"100Base-10Base-T (Cat-3) ", "ovcType":"EVPL", "tpId":"0x9100", "cosMappingMode":"PCP", "transientId":"1" } } ] }, { "action":"ADD", "itemInvolvesProduct":{ "type":"OVC", "id":"ovctest", "poi":"N/A", "nni":"NNI000000601417", "routeType":"Local", "sVLanId":"21", "maximumFrameSize":"Jumbo (9000 Bytes)", "cosHighBandwidth":"50Mbps ", "cosMediumBandwidth":"500Mbps ", "cosLowBandwidth":"500Mbps ", "uniVLanId":"25", "connectedTo":"1" } } ] }, "accessSeekerContact":{ "contactName":"John Doe", "contactPhone":"+61401123456" } } }';
    public static final String orderNotifierStr = '{ "orderType":"Connect", "customerRequiredDate":"2017-02-25", "afterHoursAppointment":"Yes", "inductionRequired":"No", "securityClearance":"No", "notes":"Order Note Added", "tradingName":"Lacrosse", "heritageSite":"No", "csaId":"VIC", "itemInvolvesContact":[ { "type":"Business", "contactName":"Bob Tompson", "role":"Manager", "emailAddress":"bobt@example.com", "phoneNumber":"0412 345 443", "unstructuredAddress":{ "addressLine1":"Suite 2", "addressLine2":"Level 5", "addressLine3":"380 Doclands Drive", "localityName":"Melbourne CBD", "postcode":"3000", "stateTerritoryCode":"VIC" } }, { "type":"Site", "contactName":"Samantha Greg", "role":"", "emailAddress":"sgreg@example.com", "phoneNumber":"0412 345 443", "unstructuredAddress":{ "addressLine1":"Suite 2", "addressLine2":"Level 6", "addressLine3":"652 Latrobe St", "localityName":"Melbourne CBD", "postcode":"3000", "stateTerritoryCode":"VIC" } } ], "productOrderComprisedOf":{ "action":"ADD", "itemInvolvesLocation":{ "id":"LOC000000160801" }, "itemInvolvesProduct":{ "term":"12 Months", "serviceRestorationSLA":"Enhanced-6 (24/7)", "accessAvailabilityTarget":"99.95% (Dual Uplink/same Network Aggregation element)", "zone":"CBD", "productType":"EE", "templateId":"TPL", "templateVersion":"1.0" }, "referencesProductOrderItem":[ { "action":"ADD", "itemInvolvesProduct":{ "type":"BTD", "ntdType":"Standard (CSAS)", "ntdMounting":"Rack", "powerSupply1":"DC(-d8V to -22V)", "powerSupply2":"Not Required" }, "referencesProductOrderItem":[ { "action":"ADD", "itemInvolvesProduct":{ "type":"UNIE", "interfaceBandwidth":"5 GB", "portId":null, "interfaceType":"100Base-10Base-T (Cat-3) ", "ovcType":"EVPL", "tpId":"0x9100", "cosMappingMode":"PCP", "transientId":"1" } } ] }, { "action":"ADD", "itemInvolvesProduct":{ "type":"OVC", "id":"ovctest", "poi":"N/A", "nni":"NNI000000601417", "routeType":"Local", "sVLanId":"21", "maximumFrameSize":"Jumbo (9000 Bytes)", "cosHighBandwidth":"50Mbps ", "cosMediumBandwidth":"500Mbps ", "cosLowBandwidth":"500Mbps ", "uniVLanId":"25", "connectedTo":"1" } } ] }, "accessSeekerContact":{ "contactName":"John Doe", "contactPhone":"+61401123456" } }';
    public static final String orderModifyStr = '{"productOrder":{"productOrderComprisedOf":{"referencesProductOrderItem":[{"referencesProductOrderItem":[{"itemInvolvesProduct":{"type":"UNI"},"action":"NO CHANGE"}],"itemInvolvesProduct":{"type":"BTD", "cosMediumBandwidth":"150"},"action":"Modify"},{"itemInvolvesProduct":{"type":"OVC","id":"ovctest","cosLowBandwidth":"150","cosHighBandwidth":"10"},"action":"MODIFY"}],"itemInvolvesProduct":{},"itemInvolvesLocation":{"id":"LOC000140610964"},"action":"NO CHANGE"},"orderType":"Modify","itemInvolvesContact":[],"csaId":"CSA300000010316","accessSeekerInteraction":{"billingAccountID":"BAN000000892802"},"accessSeekerContact":{}}}';
    @testSetup static void testDataSetup() {
        Id oppId = DF_TestService.getQuoteRecords();
        List<Opportunity> oppList = [SELECT Id FROM OPPORTUNITY WHERE Opportunity_Bundle__c = :oppId];
        System.debug('PP oppList:'+oppList);
        Opportunity opp = oppList.get(0);
        
        List<DF_Quote__c> dfQuoteList = [SELECT Id,Address__c,LAPI_Response__c,RAG__c,Fibre_Build_Cost__c,Opportunity_Bundle__c,Opportunity__c
                                     FROM DF_Quote__c WHERE Opportunity__c = :oppList.get(0).id limit 1];
        List<DF_Order__c> lstOrd = new List<DF_Order__c>();
        DF_Order__c ordConnect = new DF_Order__c();
        ordConnect.DF_Quote__c = dfQuoteList[0].id;
        ordConnect.Order_Json__c = orderSubmitStr;
        ordConnect.OrderType__c = 'Connect';
        ordConnect.Order_Notifier_Response_JSON__c = orderNotifierStr;
        ordConnect.Opportunity_Bundle__c = [Select id from DF_Opportunity_Bundle__c limit 1].Id;
        
        DF_Order__c ordModify = new DF_Order__c();
        ordModify.DF_Quote__c = dfQuoteList[0].id;
        ordModify.Order_Json__c = orderSubmitStr;
        ordModify.OrderType__c = 'Modify';
        ordModify.Order_Notifier_Response_JSON__c = orderNotifierStr;
        ordModify.Modify_Order_JSON__c = orderModifyStr;
        ordModify.Opportunity_Bundle__c = [Select id from DF_Opportunity_Bundle__c limit 1].Id;
        
        DF_Order__c ord3 = new DF_Order__c();
        ord3.DF_Quote__c = dfQuoteList[0].id;
        ord3.Order_Json__c = orderSubmitStr;
        ord3.OrderType__c = 'Connect'; 
        ord3.Order_Id__c = 'BCOTest';       
        ord3.Opportunity_Bundle__c = [Select id from DF_Opportunity_Bundle__c limit 1].Id;
        
        lstOrd.add(ordConnect);
        lstOrd.add(ordModify);
        lstOrd.add(ord3);
        
        insert lstOrd;

		//Record added for Order Cancel Test cases
		Account testAcc = new Account();
        testAcc.Name = 'Cancel Test Acct';
        testAcc.Access_Seeker_ID__c = 'ASI500050005000';
        insert testAcc; 

		DF_Opportunity_Bundle__c bundle = [Select id from DF_Opportunity_Bundle__c limit 1];
		bundle.Account__c = testAcc.Id;
		Update bundle;
		DF_Order__c ord4 = new DF_Order__c();
        ord4.DF_Quote__c = dfQuoteList[0].id;
        ord4.Order_Json__c = orderSubmitStr;
        ord4.OrderType__c = 'Connect'; 
        ord4.Order_Id__c = 'BCO_CANCEL_001';       
        ord4.Opportunity_Bundle__c = bundle.Id;
        
		insert ord4;


		//Insert required custom settings
        DF_AS_Custom_Settings__c settings  = new DF_AS_Custom_Settings__c(Name='Order_Cancel_Business_Days', Value__c='1');
	    insert settings;

    }
    
    @isTest
    static void testExecuteForInboundCallConnect() {
        id ordId = [select id from df_order__c where orderType__c = 'Connect' and Order_Id__c = null].id;
        Test.startTest();
        DF_OrderSubmittedSummaryController.getLocationDetails(ordId);
        DF_OrderSubmittedSummaryController.getNTDDetails(ordId);
        DF_OrderSubmittedSummaryController.getUNIDetails(ordId); 
        DF_OrderSubmittedSummaryController.getOVCDetails(ordId);        
         Test.stopTest();
    }
    
    @isTest
    static void testExecuteForInboundCallModify() {
        id ordId = [select id from df_order__c where orderType__c = 'Modify'].id;
        Test.startTest();
        DF_OrderSubmittedSummaryController.getLocationDetails(ordId);
        DF_OrderSubmittedSummaryController.getNTDDetails(ordId);
        DF_OrderSubmittedSummaryController.getUNIDetails(ordId); 
        DF_OrderSubmittedSummaryController.getOVCDetails(ordId);        
        Test.stopTest();
    }
    
    @isTest
    static void testExecuteForInboundCallWithOrderId() {
        id ordId = [select id from df_order__c where order_Id__c = 'BCOTest'].id;
        Test.startTest();
        DF_OrderSubmittedSummaryController.getLocationDetails(ordId);
        DF_OrderSubmittedSummaryController.getNTDDetails(ordId);
        DF_OrderSubmittedSummaryController.getUNIDetails(ordId); 
        DF_OrderSubmittedSummaryController.getOVCDetails(ordId);        
        Test.stopTest();
    }

    @isTest
    static void testIsCancellable_ACK_Order() {
        df_order__c order  = [select id, Order_Status__c from df_order__c where orderType__c = 'Connect' and Order_Id__c = null LIMIT 1];
		order.Order_Status__c = DF_Constants.DF_ORDER_STATUS_ACKNOWLEDGED;
		Update order;
        Test.startTest();
		Boolean isCancellable = DF_OrderSubmittedSummaryController.isCancellableOrder(order.id);
		Test.stopTest();
     
        System.assertEquals(true,isCancellable);
    }

    @isTest
    static void testIsCancellable_In_Progress_Scheduled_Order_Not_cancellable() {
        df_order__c order  = [select id, Order_Status__c, Order_Sub_Status__c, NBN_Commitment_Date__c from df_order__c where orderType__c = 'Connect' and Order_Id__c = null LIMIT 1];
		order.Order_Status__c = DF_Constants.DF_ORDER_STATUS_IN_PROGRESS;
		order.Order_Sub_Status__c = DF_Constants.DF_ORDER_SUB_STATUS_ORDER_SCHEDULED;
		//Set NBN COmmited Date AS 19th Nov 2018 - Monday
		order.NBN_Commitment_Date__c = Date.newInstance(2018, 11, 19);
		Update order;
		//Set today date as  16th Nov 2018 - Friday (Should not allow this as there is not 1 Business day Gap)
		DF_OrderSubmittedSummaryController.todayDate = Date.newInstance(2018, 11, 16);
        Test.startTest();
		Boolean isCancellable = DF_OrderSubmittedSummaryController.isCancellableOrder(order.id);
		Test.stopTest();
        System.assertEquals(false,isCancellable);
    }


    @isTest
    static void testIsCancellable_In_Progress_Scheduled_Order_Not_Cancellable_Holiday() {
        df_order__c order  = [select id, Order_Status__c, Order_Sub_Status__c, NBN_Commitment_Date__c from df_order__c where orderType__c = 'Connect' and Order_Id__c = null LIMIT 1];
		order.Order_Status__c = DF_Constants.DF_ORDER_STATUS_IN_PROGRESS;
		order.Order_Sub_Status__c = DF_Constants.DF_ORDER_SUB_STATUS_ORDER_SCHEDULED;
		//Set NBN COmmited Date AS 19th Nov 2018 - Monday
		order.NBN_Commitment_Date__c = Date.newInstance(2018, 11, 19);
		Update order;
		//Set today date as  15th Nov 2018 - Thursday - Being 16th is public Holiday (should NOT allow this date)
		DF_OrderSubmittedSummaryController.todayDate = Date.newInstance(2018, 11, 15);
		//Insert One Holiday for Testing -Thursday - 16th is public Holiday
		//To avoid mixed DML Error
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		System.runAs(thisUser){
		Holiday holiday = new Holiday(Name='Test Holiday', ActivityDate = Date.newInstance(2018, 11, 16));
		insert holiday;
		}

        Test.startTest();
		Boolean isCancellable = DF_OrderSubmittedSummaryController.isCancellableOrder(order.id);
		Test.stopTest();
        System.assertEquals(false,isCancellable);
    }

    @isTest
    static void testIsCancellable_In_Progress_Scheduled_Order_Cancellable() {
        df_order__c order  = [select id, Order_Status__c, Order_Sub_Status__c, NBN_Commitment_Date__c from df_order__c where orderType__c = 'Connect' and Order_Id__c = null LIMIT 1];
		order.Order_Status__c = DF_Constants.DF_ORDER_STATUS_IN_PROGRESS;
		order.Order_Sub_Status__c = DF_Constants.DF_ORDER_SUB_STATUS_ORDER_SCHEDULED;
		//Set NBN COmmited Date AS 19th Nov 2018 - Monday
		order.NBN_Commitment_Date__c = Date.newInstance(2018, 11, 19);
		Update order;
		//Set today date as  14th Nov 2018 - Thursday - 16th is public Holiday and 15th is not holiday (should allow this date)
		DF_OrderSubmittedSummaryController.todayDate = Date.newInstance(2018, 11, 14);
		//Insert One Holiday for Testing -Thursday - 16th is public Holiday
		//To avoid mixed DML Error
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		System.runAs(thisUser){
		Holiday holiday = new Holiday(Name='Test Holiday', ActivityDate = Date.newInstance(2018, 11, 16));
		insert holiday;
		}

        Test.startTest();
		Boolean isCancellable = DF_OrderSubmittedSummaryController.isCancellableOrder(order.id);
		Test.stopTest();
        System.assertEquals(true,isCancellable);
    }

    @isTest
    static void testSendCancelRequest() {
        df_order__c order  = [select id, Order_Status__c, Order_Sub_Status__c, NBN_Commitment_Date__c from df_order__c where orderType__c = 'Connect' and Order_Id__c = 'BCO_CANCEL_001' LIMIT 1];
		order.Order_Status__c = DF_Constants.DF_ORDER_STATUS_IN_PROGRESS;
		order.Order_Sub_Status__c = DF_Constants.DF_ORDER_SUB_STATUS_ORDER_SCHEDULED;
		Update order;
		Map<string, string> headerMap = new  Map<string, string>();
		headerMap.put('Content-Type', 'application/json');
		//Set Mock for Cancel Appian Interface
		EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', '{}', headerMap);
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, mockObj);
		DF_OrderSubmittedSummaryController.cancelOrder(order.id);
		Test.stopTest();
		//Query Order
        order  = [select id, Order_Status__c, Order_Sub_Status__c, NBN_Commitment_Date__c from df_order__c where orderType__c = 'Connect' and Order_Id__c = 'BCO_CANCEL_001' LIMIT 1];
        System.assertEquals(DF_Constants.DF_ORDER_STATUS_CANCEL_INITIATED, order.Order_Status__c);
    }

}