public class DF_OrderSummaryData {
    // Wrapper class to display Order Summary data
    @AuraEnabled
    public String locId {get;set;}
    @AuraEnabled
    public String quoteId {get;set;}    
    @AuraEnabled
    public String address {get;set;}
    @AuraEnabled
    public Decimal FBC {get;set;}
    @AuraEnabled
    public Decimal SCNR {get;set;}
    @AuraEnabled
    public Decimal SCR {get;set;}    
    @AuraEnabled
    public String status {get;set;}     
    @AuraEnabled 
    public String id {get;set;}
    @AuraEnabled 
    public String orderId {get;set;}  
    @AuraEnabled
    public String oppBundleName {get;set;}
    @AuraEnabled
    public String oppBundleId {get;set;}
    @AuraEnabled
    public String ovcNonBillable {get;set;}
    @AuraEnabled
    public String bulkOrderError {get;set;}
    
    public DF_OrderSummaryData(String inputId, String inputLocId, String inputQuoteId, String inputAddress, Decimal inputFBC, Decimal inputSCNR, Decimal inputSCR, String inputStatus) {
        id = inputId;
        locId = inputLocId;
        quoteId = inputQuoteId;
        address = inputAddress;
        FBC = inputFBC;
        SCNR = inputSCNR;
        SCR = inputSCR;
        status = inputStatus; 
    } 
}