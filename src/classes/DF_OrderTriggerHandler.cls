public without sharing class DF_OrderTriggerHandler extends TriggerHandler {
    
    public DF_OrderTriggerHandler() {       
        if (Test.isRunningTest()) {
            this.setMaxLoopCount(10);
        } else {
            this.setMaxLoopCount(8);
        }
    }
    
    protected override void afterUpdate(Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {   
        createSOACreateBillingEventHandlerRequests(trigger.new, oldMap);
        updateCSServiceStatus(trigger.new, oldMap);
    }    
     
    public static void createSOACreateBillingEventHandlerRequests(List<SObject> newDfOrderList, Map<Id, SObject> oldDfOrderMap) {
        final String DELIMITER = ',';
        final String COMPLETED = 'Complete';

        List<String> paramList = new List<String>();

        SObject oldSObject;
        DF_Order__c newDfOrder;
        DF_Order__c oldDfOrder;
    
        Boolean oldIsSiteSurveyChargesOrderCompletedFlg;
        Boolean newIsSiteSurveyChargesOrderCompletedFlg;        
        Boolean oldIsProductChargesOrderCompletedFlg;
        Boolean newIsProductChargesOrderCompletedFlg;   

        try {                                   
            // NS Order Record Type
            Id nsOrderRecType = SF_CS_API_Util.getRecordType(SF_Constants.NBN_SELECT_NAME, SF_Constants.ORDER_OBJECT);
            
            for (SObject contextRec : newDfOrderList) {        
                    
                // Cast to DF Order object
                newDfOrder = (DF_Order__c) contextRec;
                oldSObject = oldDfOrderMap.get(newDfOrder.Id);
                            
                if (oldSObject != null) {                   
                    // Cast to DF Order object
                    oldDfOrder = (DF_Order__c) oldSObject;

                    // Get old DF Order Flg values  
                    oldIsSiteSurveyChargesOrderCompletedFlg = oldDfOrder.IsSiteSurveyChargesOrderCompleted__c;                  
                    oldIsProductChargesOrderCompletedFlg = oldDfOrder.IsProductChargesOrderCompleted__c;
                }               

                // Get new DF Order DF Order Flg values 
                newIsSiteSurveyChargesOrderCompletedFlg = newDfOrder.IsSiteSurveyChargesOrderCompleted__c;  
                newIsProductChargesOrderCompletedFlg = newDfOrder.IsProductChargesOrderCompleted__c;

                // Check Site Survey Charges to be sent to SOA          
                if (oldIsSiteSurveyChargesOrderCompletedFlg == false && newIsSiteSurveyChargesOrderCompletedFlg == true) {
                    if (String.isNotEmpty(newDfOrder.Site_Survey_Charges_JSON__c)) {                        
                        String param = newDfOrder.Id + DELIMITER + DF_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY;
                        
                        // Add to Async Req - creation list
                        paramList.add(param);               
                    }                                       
                }
                
                // Check Product Charges to be sent to SOA          
                if (oldIsProductChargesOrderCompletedFlg == false && newIsProductChargesOrderCompletedFlg == true) {
                    if (newDfOrder.Order_Status__c.equalsIgnoreCase(COMPLETED)) {               
                        if (String.isNotEmpty(newDfOrder.Product_Charges_JSON__c)) {                            
                            String param = newDfOrder.Id + DELIMITER + DF_SOABillingEventUtils.CHARGE_TYPE_PRODUCT;                         
                            // Add to Async Req - creation list
                            paramList.add(param);                   
                        }
                    }
                }
				
				System.debug('$$$ Changes for In-flight cancel - START');

                // Changes for In-flight cancel - START
                if(oldDfOrder != null && newDfOrder != null) {
                    String oldDfOrderStatus = oldDfOrder.Order_Status__c;
                    String newDfOrderStatus = newDfOrder.Order_Status__c;
                    Boolean soaOrderCancelChargesSent = newDfOrder.SOA_Order_Cancel_Charges_Sent__c;
                    String orderCancelChargesJSON = newDfOrder.Order_Cancel_Charges_JSON__c;
					System.debug('$$$ oldDfOrderStatus : ' + oldDfOrderStatus);

                    if(String.isNotBlank(oldDfOrderStatus) 
                            && String.isNotBlank(newDfOrderStatus)
                            && !oldDfOrderStatus.equals(newDfOrderStatus)
                            && newDfOrderStatus.equals('Cancelled')
                            && !soaOrderCancelChargesSent
                            && String.isNotBlank(orderCancelChargesJSON)
                            && !orderCancelChargesJSON.equalsIgnoreCase(Label.DF_Order_Cancel_No_Charge)) {
                        
                        if (newDfOrder.RecordTypeId != null && newDfOrder.RecordTypeId.equals(nsOrderRecType)){
                            String param = newDfOrder.Id + NS_SOABillingEventUtils.PARAMETER_DELIMITER + NS_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER;
                            // NS_SOACreateBillingEventHandler only takes one Order at a time
                            AsyncQueueableUtils.createRequests(NS_SOACreateBillingEventHandler.HANDLER_NAME, new List<String>{param});
                        } else {
                            String param = newDfOrder.Id + DELIMITER + DF_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER;
                            paramList.add(param); 
                        }
                    }
                }
                // Changes for In-flight cancel - END

                oldDfOrder = null;
                newDfOrder = null;
                oldIsSiteSurveyChargesOrderCompletedFlg = null;
                newIsSiteSurveyChargesOrderCompletedFlg = null;
                oldSObject = null;
            }
			
			System.debug('$$$ paramList : ' + paramList);

            if (!paramList.isEmpty()) {                                     
                AsyncQueueableUtils.createRequests(DF_SOACreateBillingEventHandler.HANDLER_NAME, paramList);                                
            }
        } catch (Exception e) {          
            throw new CustomException(e.getMessage());
        }
    }

    public static void updateCSServiceStatus(List<SObject> newDfOrderList, Map<Id, SObject> oldDfOrderMap) {
        
        SObject oldSObject;
        DF_Order__c newDfOrder;
        DF_Order__c oldDfOrder;

        List<csord__Subscription__c> subscriptionsToUpdate = new List<csord__Subscription__c>();
        List<csord__Service__c> servicesToUpdate = new List<csord__Service__c>();
        List<csord__Order__c> ordersToUpdate = new List<csord__Order__c>();

        List<Id> oppIds = new List<Id>();
        List<DF_Order__c> orderList = new List<DF_Order__c>();
        //Get list of df_orders
        try {
            for(SObject updatedOrder:newDfOrderList){
                newDfOrder = (DF_Order__c) updatedOrder;
                orderList.add(newDfOrder);
            }
            newDfOrder = null;
            //Get list of oppId's from dfOrder

            for(DF_Order__c order:[Select DF_Quote__r.Opportunity__c from DF_Order__c where Id in :oldDfOrderMap.keyset()]){
                oppIds.add(order.DF_Quote__r.Opportunity__c);
            }
            /*for(DF_Order__c order:orderList){
                oppIds.add(order.DF_Quote__r.Opportunity__c);
            }*/

                    //Map<Id,csord__Subscription__c> subMap = new Map<Id,csord__Subscription__c>();
            //List<csord__Subscription__c> subsList = [SELECT id,csord__Status__c,csord__Order__r.csordtelcoa__Opportunity__c
      //                                                      FROM csord__Subscription__c 
      //                                                      WHERE  csord__Order__r.csordtelcoa__Opportunity__c in :oppIds];
      //      for(csord__Subscription__c sub:subsList){
      //        subMap.put(sub.csord__Order__r.csordtelcoa__Opportunity__c,sub);
      //      }
      //      system.debug('!!! subMap '+subMap);
        //      Map<Id,csord__Service__c> serviceMap = new Map<Id,csord__Service__c>();
      //      for(csord__Service__c service:[SELECT id,csord__Status__c,csord__Subscription__r.csord__Order__r.csordtelcoa__Opportunity__c
      //                                                      FROM csord__Service__c 
      //                                                      WHERE  csord__Subscription__r.csord__Order__r.csordtelcoa__Opportunity__c  in :oppIds]){
      //        serviceMap.put(service.csord__Subscription__r.csord__Order__r.csordtelcoa__Opportunity__c,service);
      //      }
      //      system.debug('!!! serviceMap '+serviceMap);

      //      Map<Id,csord__Order__c> orderMap = new Map<Id,csord__Order__c>();
      //      for(csord__Order__c order:[SELECT id,csord__Status__c,csordtelcoa__Opportunity__c
      //                                                      FROM csord__Order__c 
      //                                                      WHERE  csordtelcoa__Opportunity__c in :oppIds]){
      //        orderMap.put(order.csordtelcoa__Opportunity__c,order);
      //      }

      //        system.debug('!!! orderMap '+orderMap);
        List<csord__Subscription__c> subscriptionList = new List<csord__Subscription__c>();
        subscriptionList= [SELECT id,csord__Status__c,csord__Order__r.csordtelcoa__Opportunity__c
                        FROM csord__Subscription__c 
                        WHERE  csord__Order__r.csordtelcoa__Opportunity__c in :oppIds];


        List<csord__Service__c> serviceList = new List<csord__Service__c>();
        serviceList = [SELECT id,csord__Status__c,csord__Subscription__r.csord__Order__r.csordtelcoa__Opportunity__c
                       FROM csord__Service__c 
                       WHERE  csord__Subscription__r.csord__Order__r.csordtelcoa__Opportunity__c     in :oppIds];

        List<csord__Order__c> csordersList = new List<csord__Order__c> ();      
        csordersList = [SELECT id,csord__Status2__c,csordtelcoa__Opportunity__c
                                                        FROM csord__Order__c 
                                                        WHERE  csordtelcoa__Opportunity__c in :oppIds 
                                                        AND csord__Primary_Order__c != null];
        system.debug('!!! scriptionList '+subscriptionList);
        system.debug('!!! serviceList '+serviceList);        
        system.debug('!!! csordersList '+csordersList);                                             

                //loop through DF orders and update  CS subs servs and orders                           
            for (SObject contextRec : newDfOrderList) {        
                    
                // Cast to DF Order object
                newDfOrder = (DF_Order__c) contextRec;
                oldSObject = oldDfOrderMap.get(newDfOrder.Id);
                            
                if (oldSObject != null) {                   
                    // Cast to DF Order object
                    oldDfOrder = (DF_Order__c) oldSObject;
                    if(newDfOrder.Order_Status__c != oldDfOrder.Order_Status__c 
                        // TODO: Order Cancel Inflight change - To be updated once approach is confirmed with Cloudsense - START
                        && !('Cancel Initiated'.equalsIgnoreCase(newDfOrder.Order_Status__c)
                            || 'Cancelled'.equalsIgnoreCase(newDfOrder.Order_Status__c))
                        // TODO - In-flight change - END
                    ){
                        //update subs
                        for(csord__Subscription__c sub:subscriptionList){
                            if(sub.csord__Status__c!=newDfOrder.Order_Status__c){
                                sub.csord__Status__c=newDfOrder.Order_Status__c;
                                subscriptionsToUpdate.add(sub);
                            }
                        }
                        //update services
                        for(csord__Service__c service:serviceList){
                            if(service.csord__Status__c!=newDfOrder.Order_Status__c){
                                service.csord__Status__c=newDfOrder.Order_Status__c;
                                servicesToUpdate.add(service);
                            }
                        }
                        //update orders
                        for(csord__Order__c order:csordersList){
                            if(order.csord__Status2__c!=newDfOrder.Order_Status__c){
                                order.csord__Status2__c=newDfOrder.Order_Status__c;
                                ordersToUpdate.add(order);
                            }
                        }
                    }
                }
            }               

            oldDfOrder = null;
            newDfOrder = null;
            oldSObject = null;
            
             if(servicesToUpdate.size()>0){
                update servicesToUpdate;
             }
             if(subscriptionsToUpdate.size()>0){
                update subscriptionsToUpdate;
             }
             if(ordersToUpdate.size()>0){
                update ordersToUpdate;
             }

        } catch (Exception e) {          
            throw new CustomException(e.getMessage());
        }

    }

}