@isTest
private class DF_OrderTriggerHandler_Test {

    @isTest static void test_createSOACreateBillingEventHandlerRequests_SiteSurveyCharge() {            
    	/** Setup data  **/
    	String response;    	
    	       
		// Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
		dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
		insert dfQuote;
	
		// Create DF Order
		DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
		dfOrder.Site_Survey_Charges_JSON__c = '{"field":"value"}';
		insert dfOrder;	
    	   
   		// Generate mock response
		response = '{"Ack":"true"}';
 
		// Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

		test.startTest(); 	

		dfOrder.IsSiteSurveyChargesOrderCompleted__c = true;	
		update dfOrder;
			
		test.stopTest();     			                                            
    }
    
    @isTest static void test_createSOACreateBillingEventHandlerRequests_ProductCharge() {            
    	/** Setup data  **/
    	String response;    	
    	       
		// Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
		dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
		insert dfQuote;
	
		// Create DF Order
		DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
		dfOrder.Product_Charges_JSON__c = '{"field":"value"}';
		insert dfOrder;	
    	   
   		// Generate mock response
		response = '{"Ack":"true"}';
 
		// Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

		test.startTest(); 	

		dfOrder.Order_Status__c = 'Completed';
		dfOrder.IsProductChargesOrderCompleted__c = true;	
		update dfOrder;
			
		test.stopTest();     			                                            
    }    

	/** Data Creation **/
    @testSetup static void setup() {
    	try {
    		createCustomSettings();
            createNSCustomSettings();
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

	@isTest static void createCustomSettings() {
        try {
	        Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
			asrConfigSettings.Max_No_of_Future_Calls__c = 50;
			asrConfigSettings.Max_No_of_Parallels__c = 10;
			asrConfigSettings.Max_No_of_Retries__c = 3;
			asrConfigSettings.Max_No_of_Rows__c = 1;
	        upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    @isTest static void test_updateCSServiceStatus(){
              Id oppId = DF_TestService.getQuoteRecords();
        Account account = DF_TestData.createAccount('My Account');
        List<Opportunity> oppList = [SELECT Id, AccountId FROM OPPORTUNITY WHERE Opportunity_Bundle__c = :oppId];
        System.debug('PP oppList:'+oppList);
        Opportunity opp = oppList.get(0);


        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(account);
        basketObj.cscfga__Opportunity__c = opp.Id;
        update basketObj;

        List<cscfga__Product_Configuration__c> pcConfigList = [Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketObj.Id and name = 'Direct Fibre - Product Charges'];
        cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
        String configId = configObj != null ? configObj.Id : null;



         csord__Order__c csOrd = DF_TestData.createCSOrder('Test Order',account.Id,opp);
         csOrd.csordtelcoa__Opportunity__c = opp.Id;
         update csOrd;
         system.debug('csOrd.csordtelcoa__Opportunity__c) '+csOrd.csordtelcoa__Opportunity__c);
         system.debug('csOrd ' + csOrd);


        csord__Subscription__c subscription = new csord__Subscription__c();
        subscription.Name = 'Direct Fibre - Product Charges';
        subscription.csord__Order__c = csOrd.Id;
        subscription.csord__Identification__c = 'xxx';
        insert subscription;
        system.debug('subscription.csord__Orders__r.csordtelcoa__Opportunity__c '+subscription.csord__Order__r.csordtelcoa__Opportunity__c);
        system.debug('subscription.csord__Order__c '+subscription.csord__Order__c);

        csord__Service__c service = new csord__Service__c();
        service.Name = 'OVC 1';
        service.csordtelcoa__Product_Basket__c = basketObj.Id;
        service.csord__Subscription__c = subscription.Id;
        service.csord__Order__c = csOrd.Id;
        service.csord__Identification__c = 'xxx';
        insert service;
        system.debug('service.csord__Orders__r.csordtelcoa__Opportunity__c '+ service.csord__Order__r.csordtelcoa__Opportunity__c);
         system.debug('service.csord__Order__c '+ service.csord__Order__c);
        List<DF_Quote__c> dfQuoteList = [SELECT Id,Address__c,LAPI_Response__c,RAG__c,Fibre_Build_Cost__c,Opportunity_Bundle__c,Opportunity__c
                                     FROM DF_Quote__c WHERE Opportunity__c = :opp.id];

        system.debug('dfQuoteList '+dfQuoteList);
        // Create DF Order

        //for(DF_Quote__c qtItem : dfQuoteList){
            DF_Order__c ord = new DF_Order__c();
            ord.DF_Quote__c = dfQuoteList[0].id;
            ord.Opportunity_Bundle__c = [Select id from DF_Opportunity_Bundle__c limit 1].Id;
            ord.Order_Status__c = 'In Draft';
            ord.recordTypeId = DF_CS_API_Util.getRecordType(DF_Constants.ENTERPRISE_ETHERNET_NAME, DF_Constants.ORDER_OBJECT);
            insert ord;
            system.debug('ord '+ord);

            test.startTest();
            //DF_Order__c ordToTest = new DF_Order__c();
            //ordToTest = ord;
            ord.Order_Status__c = 'Test Status';
            update ord;
            test.stopTest();


    }


    @isTest static void test_createSOACreateBillingEventHandlerRequests_NS_CancelCharge() {
        // Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;

        // ensure the group is created for sharing
        EE_DataSharingUtility.findGroup(DF_Constants.NBN_SELECT_GROUP_NAME + acct.Access_Seeker_ID__c);

        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        opptyBundle.Account__c = acct.Id;
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        insert dfQuote;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        dfOrder.Order_Cancel_Charges_JSON__c = '{"field":"value"}';
        dfOrder.SOA_Order_Cancel_Charges_Sent__c = false;
        insert dfOrder;

        // Generate mock response
        String response = '{"Ack":"true"}';

        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

        test.startTest();

        dfOrder.Order_Status__c = 'Cancelled';
        update dfOrder;

        test.stopTest();

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT Order_Status__c, SOA_Order_Cancel_Charges_Sent__c
                                         FROM   DF_Order__c
                                         WHERE  Id = :dfOrder.Id];

        system.AssertEquals('Cancelled', dfOrderList[0].Order_Status__c);

        // https://salesforce.stackexchange.com/questions/61239/testing-scheduled-apex-containing-future-method/70707
        // unfortunately we cannot assert the event was sent due to the limitation of testing environment
        // system.AssertEquals(true, dfOrderList[0].SOA_Order_Cancel_Charges_Sent__c);
    }

    @isTest static void createNSCustomSettings() {
        try {
            NS_Custom_Options__c nsOption = NS_Custom_Options__c.getValues('NS_SOA_PRODUCT_TYPE');
            if (nsOption == null) {
                nsOption = new NS_Custom_Options__c();
            }
            nsOption.Name = 'NS_SOA_PRODUCT_TYPE';
            nsOption.Value__c = 'NS';

            upsert nsOption;
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }
}