public class DF_OrderValidatorService { 
    @AuraEnabled
    public static string validateOrderSyncronous(String dfOrderId){
                
        final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;
        final String DATAPOWER_OVERRIDE;

        OrderValidatorRequest ovRequest = new OrderValidatorRequest();
        

        Http http = new Http();     
        HttpRequest req;              
        HTTPResponse res; 

        String requestStr;
        String responseStr;
        
        String responseBodyStatus;
         Map<String, String> returnDataMap = new Map<String, String>();
          String returnDataJson;
        String orderValidationStatus;   
        String orderValidatorResponseJSON;  

        try {                               
            DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
                                                                Timeout__c,
                                                                DP_Environment_Override__c
                                                         FROM   DF_Integration_Setting__mdt
                                                         WHERE  DeveloperName = :DF_OrderValidatorServiceUtils.INTEGRATION_SETTING_NAME];

            NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
            TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
            DATAPOWER_OVERRIDE = integrSetting.DP_Environment_Override__c;
            
            system.debug('NAMED_CREDENTIAL: ' + NAMED_CREDENTIAL); 
            system.debug('TIMEOUT_LIMIT: ' + TIMEOUT_LIMIT); 
            system.debug('DATAPOWER_OVERRIDE: ' + DATAPOWER_OVERRIDE);
            //query for df order id an retrieve modify order valoator jspn
            List<DF_Order__c> modifyOrder = [SELECT Id, OrderType__c,Modify_Order_JSON__c FROM DF_Order__c WHERE Id =: dfOrderId AND OrderType__c = 'Modify'];

            if(!modifyOrder.isEmpty()){
                requestStr = modifyOrder[0].Modify_Order_JSON__c;   
            }else{
            // Generate Payload
                ovRequest.productOrder = DF_OrderValidatorTransformer.generateRequest(dfOrderId);   
                 // Serialise object for sending  
                      
                requestStr = JSON.serialize(ovRequest);   
            }        

                         
            system.debug('payload request (before stripping json nulls): ' + requestStr);

            requestStr = DF_OrderValidatorServiceUtils.stripJsonNulls(requestStr);
            system.debug('payload request (after stripping json nulls): ' + requestStr);

            // Build endpoint
            END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL;
            system.debug('END_POINT: ' + END_POINT);     

            // Generate request 
            req = DF_OrderValidatorServiceUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, requestStr, dfOrderId, DATAPOWER_OVERRIDE);
            system.debug('req: ' + req); 

            // Send and get response               
            res = http.send(req);

            system.debug('Response Status Code: ' + res.getStatusCode());
            system.debug('Response Body: ' + res.getBody());
        
            if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {
                responseStr = res.getBody();  

                system.debug('send success');

                if (String.isNotEmpty(responseStr)) {
                    // Get Response Body - Status
                    responseBodyStatus = DF_OrderValidatorServiceUtils.getOrderValidatorResponseStatus(responseStr);    
                    system.debug('responseBodyStatus: ' + responseBodyStatus);
// Get Response Body - Status
                    responseBodyStatus = DF_OrderValidatorServiceUtils.getOrderValidatorResponseStatus(responseStr);    
                    system.debug('responseBodyStatus: ' + responseBodyStatus);

                    returnDataMap.put('orderValidationStatus', responseBodyStatus);                  
                    returnDataMap.put('orderValidatorResponseJSON', responseStr);                        
                    returnDataJson = JSON.serialize(returnDataMap); 
                    return returnDataJson;                                                                        
                }
             }else{
                    system.debug('send failure');                                   
                    returnDataMap.put('orderValidationStatus', DF_OrderValidatorServiceUtils.ORDER_VALIDATION_STATUS_IN_ERROR);                  
                    returnDataMap.put('orderValidatorResponseJSON', '');                           
                    returnDataJson = JSON.serialize(returnDataMap);                            
            }   
        
        }
        catch (Exception e) {
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_OrderValidatorService.class.getName(), 'validateOrder', '', '', '', '', e, 0);
            DF_OrderValidatorServiceUtils.validateOrderErrorPostProcessing(e.getMessage(), dfOrderId);
        }
         return returnDataJson;       

    }

    @future(callout=true)
    public static void validateOrder(String dfOrderId) {                   
        final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;
        final String DATAPOWER_OVERRIDE;

        OrderValidatorRequest ovRequest = new OrderValidatorRequest();

        Http http = new Http();     
        HttpRequest req;              
        HTTPResponse res; 

        String requestStr;
        String responseStr;
        
        String responseBodyStatus;


        try {                               
            DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
                                                                Timeout__c,
                                                                DP_Environment_Override__c
                                                         FROM   DF_Integration_Setting__mdt
                                                         WHERE  DeveloperName = :DF_OrderValidatorServiceUtils.INTEGRATION_SETTING_NAME];

            NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
            TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
            DATAPOWER_OVERRIDE = integrSetting.DP_Environment_Override__c;
            
            system.debug('NAMED_CREDENTIAL: ' + NAMED_CREDENTIAL); 
            system.debug('TIMEOUT_LIMIT: ' + TIMEOUT_LIMIT); 
            system.debug('DATAPOWER_OVERRIDE: ' + DATAPOWER_OVERRIDE);

            List<DF_Order__c> modifyOrder = [SELECT Id, OrderType__c,Modify_Order_JSON__c FROM DF_Order__c WHERE Id =: dfOrderId AND OrderType__c = 'Modify'];

            if(!modifyOrder.isEmpty()){
                requestStr = modifyOrder[0].Modify_Order_JSON__c;   
            }else{
            // Generate Payload
                ovRequest.productOrder = DF_OrderValidatorTransformer.generateRequest(dfOrderId);   
                 // Serialise object for sending        
                requestStr = JSON.serialize(ovRequest);   
            }               
            system.debug('payload request (before stripping json nulls): ' + requestStr);

            requestStr = DF_OrderValidatorServiceUtils.stripJsonNulls(requestStr);
            system.debug('payload request (after stripping json nulls): ' + requestStr);

            // Build endpoint
            END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL;
            system.debug('END_POINT: ' + END_POINT);     

            // Generate request 
            req = DF_OrderValidatorServiceUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, requestStr, dfOrderId, DATAPOWER_OVERRIDE);
            system.debug('req: ' + req); 

            // Send and get response               
            res = http.send(req);

            system.debug('Response Status Code: ' + res.getStatusCode());
            system.debug('Response Body: ' + res.getBody());
        
            if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {                           
                responseStr = res.getBody();  

                system.debug('send success');

                if (String.isNotEmpty(responseStr)) {
                    // Get Response Body - Status
                    responseBodyStatus = DF_OrderValidatorServiceUtils.getOrderValidatorResponseStatus(responseStr);    
                    system.debug('responseBodyStatus: ' + responseBodyStatus);

                    if (responseBodyStatus.equalsIgnoreCase(DF_OrderValidatorServiceUtils.RESP_BODY_STATUS_SUCCESS)) {                      
                        DF_OrderValidatorServiceUtils.validateOrderSuccessPostProcessing(responseStr, dfOrderId);                                               
                    } else if (responseBodyStatus.equalsIgnoreCase(DF_OrderValidatorServiceUtils.RESP_BODY_STATUS_FAILED)) {                        
                        DF_OrderValidatorServiceUtils.validateOrderFailurePostProcessing(responseStr, dfOrderId);                       
                    }                                   
                } else {
                    throw new CustomException(DF_OrderValidatorServiceUtils.ERR_RESP_BODY_MISSING);
                }
            } else {
                system.debug('send failure');                                   
                DF_OrderValidatorServiceUtils.validateOrderErrorPostProcessing(res.getBody(), dfOrderId);                               
            }               
        } catch (Exception e) {
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_OrderValidatorService.class.getName(), 'validateOrder', '', '', '', '', e, 0);
            DF_OrderValidatorServiceUtils.validateOrderErrorPostProcessing(e.getMessage(), dfOrderId);
        }
    } 
    
}