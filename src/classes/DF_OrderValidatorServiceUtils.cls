public class DF_OrderValidatorServiceUtils {

    public static final String INTEGRATION_SETTING_NAME = 'Order_Validator';
    public static final String ACTION_ADD = 'ADD';

    public static final String ERR_ACCESS_SEEKER_ID_NOT_FOUND = 'accessSeekerId for RSP Account was not found.';    
    public static final String ERR_RESP_BODY_MISSING = 'Response Body is missing.'; 
    public static final String ERR_RESP_BODY_STATUS_NOT_FOUND = 'Response - Status not found.';
    public static final String ERR_DFORDER_ORDER_JSON_NOT_FOUND = 'DF Order - Order JSON not found.';
    
    public static final String RESP_BODY_STATUS_SUCCESS = 'SUCCESS';
    public static final String RESP_BODY_STATUS_FAILED = 'FAILED';

    public static final String ORDER_VALIDATION_STATUS_PENDING = 'Pending';
    public static final String ORDER_VALIDATION_STATUS_SUCCESS = 'Success'; 
    public static final String ORDER_VALIDATION_STATUS_FAILED = 'Failed';
    public static final String ORDER_VALIDATION_STATUS_IN_ERROR = 'In Error';
    
    public static final String ACCESS_SEEKER_ID = 'accessSeekerID';
    public static final String CORRELATION_ID = 'correlationId';

    public static HttpRequest getHttpRequest(String endPoint, Integer timeoutLimit, String requestStr, String dfOrderId, String datapowerOverride) {                                         
        HttpRequest req = new HttpRequest();        
        String accessSeekerId;
        String correlationId;
        
        try {    
            // Get access seeker id
            accessSeekerId = getAccessSeekerId(); // Can only test via Community
  
            // Generate correlationid
            DF_Order__c ord = getDFOrder(dfOrderId);
            if(ord <> null && string.isNotEmpty(ord.DF_Quote__r.Order_GUID__c))
                correlationId = ord.DF_Quote__r.Order_GUID__c;
            else 
                correlationId = generateGUID();
                
            req.setEndpoint(endpoint);      
            req.setMethod(DF_Constants.HTTP_METHOD_POST);             
            req.setTimeout(timeoutLimit);                
            
            if (accessSeekerId != null) {
                req.setHeader(DF_OrderValidatorServiceUtils.ACCESS_SEEKER_ID, accessSeekerId);              
            } else {
                throw new CustomException(DF_OrderValidatorServiceUtils.ERR_ACCESS_SEEKER_ID_NOT_FOUND);
            }      
              
            req.setHeader(DF_OrderValidatorServiceUtils.CORRELATION_ID, correlationId); 
            req.setHeader(DF_Constants.CONTENT_TYPE, DF_Constants.CONTENT_TYPE_JSON);  
            req.setHeader(DF_Constants.HTTP_HEADER_ACCEPT, DF_Constants.CONTENT_TYPE_JSON);
            if(String.isNotEmpty(datapowerOverride)){
                req.setHeader(DF_IntegrationUtils.NBN_ENV_OVRD, datapowerOverride);
            }   

            req.setBody(requestStr);                                 
        } catch (Exception e) {  
            throw new CustomException(e.getMessage());
        }
               
        return req;
    }   
   
    public static void validateOrderSuccessPostProcessing(String response, String dfOrderId) {                        
        DF_Order__c dfOrder;

        try {       
            dfOrder = getDFOrder(dfOrderId);            
            dfOrder.Order_Validation_Status__c = DF_OrderValidatorServiceUtils.ORDER_VALIDATION_STATUS_SUCCESS;
            dfOrder.Last_Successful_Order_Validation_Date__c = DateTime.now();
            dfOrder.Order_Validator_Response_JSON__c = response;
            update dfOrder;
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }    

    public static void validateOrderFailurePostProcessing(String response, String dfOrderId) {  
        DF_Order__c dfOrder;

        try {       
            dfOrder = getDFOrder(dfOrderId);            
            dfOrder.Order_Validation_Status__c = DF_OrderValidatorServiceUtils.ORDER_VALIDATION_STATUS_FAILED;
            dfOrder.Order_Validator_Response_JSON__c = response;
            update dfOrder;                  
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }
    
    public static void validateOrderErrorPostProcessing(String response, String dfOrderId) {
        DF_Order__c dfOrder;
                   
        try {       
            dfOrder = getDFOrder(dfOrderId);            
            dfOrder.Order_Validation_Status__c = DF_OrderValidatorServiceUtils.ORDER_VALIDATION_STATUS_IN_ERROR;
            dfOrder.Order_Validator_Response_JSON__c = response;
            update dfOrder;           
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    public static String stripJsonNulls(String jsonString) {
        try {
            if (jsonString != null) {
                jsonString = jsonString.replaceAll('\"[^\"]*\":null',''); //basic removal of null values
                jsonString = jsonString.replaceAll(',{2,}', ','); //remove duplicate/multiple commas
                jsonString = jsonString.replace('{,', '{'); //prevent opening brace from having a comma after it
                jsonString = jsonString.replace(',}', '}'); //prevent closing brace from having a comma before it
                jsonString = jsonString.replace('[,', '['); //prevent opening bracket from having a comma after it
                jsonString = jsonString.replace(',]', ']'); //prevent closing bracket from having a comma before it
            }           
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return jsonString;
    }    

    public static String stripSpecificJsonNull(String jsonString) {
        final String assocAttribute = '"associatedReferenceId":null';
        final String itemIdAttribute = '"id":null';
        try {
            String regexInvolvedProdId = '^.*"id":null.*$';
            String regexAssociatedRef = '^.*"associatedReferenceId":null.*$';
            if (jsonString != null) {
                Matcher matchRegexId = Pattern.compile(regexInvolvedProdId).matcher(jsonString);
                Matcher matchRegexAssoc = Pattern.compile(regexAssociatedRef).matcher(jsonString);

                if(matchRegexAssoc.matches()) {
                    jsonString = jsonString.replace(assocAttribute, '');
                }
                if(matchRegexId.matches()) {
                    jsonString = jsonString.replace(itemIdAttribute, '');
                }

                if(matchRegexAssoc.matches() || matchRegexId.matches()) {
                    jsonString = jsonString.replaceAll(',{2,}', ',');
                    jsonString = jsonString.replace('{,', '{');
                    jsonString = jsonString.replace(',}', '}');
                    jsonString = jsonString.replace('[,', '[');
                    jsonString = jsonString.replace(',]', ']');
                }
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return jsonString;
    }

    public static String getAccessSeekerId() {
        Account acct;                                
        Id commUserAccountId;
        String accessSeekerId;

        try {
            commUserAccountId = DF_SF_CaptureController.getCommUserAccountId();      

            if (commUserAccountId != null) {                                
                acct = [SELECT Access_Seeker_ID__c
                        FROM   Account 
                        WHERE  Id = :commUserAccountId];

                if (acct != null) {                                 
                    if (acct.Access_Seeker_ID__c != null) {         
                        accessSeekerId = acct.Access_Seeker_ID__c;
                    }
                }                       
            }                                                                      
        } catch (Exception e) {    
            throw new CustomException(e.getMessage());
        }                     
                    
        return accessSeekerId;
    }

    public static String getOrderValidatorResponseStatus(String response) {                      
        String status;
        
        try {
            // Get root node of response        
            OrderValidatorResponse.OrderValidatorResponseRoot rootNode = (OrderValidatorResponse.OrderValidatorResponseRoot)JSON.deserialize(response, OrderValidatorResponse.OrderValidatorResponseRoot.Class);
            
            if (rootNode != null) {     
                status = rootNode.status;
            } else {                
                throw new CustomException(DF_OrderValidatorServiceUtils.ERR_RESP_BODY_STATUS_NOT_FOUND);
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return status;
    }            

    @TestVisible private static DF_Order__c getDFOrder(String dfOrderId) {                               
        DF_Order__c dfOrder;

        try {
            if (String.isNotEmpty(dfOrderId)) {                                        
                dfOrder = [SELECT Id, DF_Quote__r.Order_GUID__c, DF_Quote__c
                           FROM   DF_Order__c            
                           WHERE  Id = :dfOrderId];                       
            }                                                          
        } catch (Exception e) {    
            throw new CustomException(e.getMessage());
        }                     
                            
        return dfOrder;
    }       

    public static String generateGUID() {       
        String kHexChars = '0123456789abcdef';
        String guId = '';
        Integer nextByte = 0;

        try {
            for (Integer i = 0; i < 16; i++) {
                if (i == 4 || i == 6 || i == 8 || i == 10) {
                    guId+= '-';
                }
    
                nextByte = (Math.round(Math.random() * 255)-128) & 255;
    
                if (i == 6) {
                    nextByte = nextByte & 15;
                    nextByte = nextByte | (4 << 4);
                }
    
                if (i == 8) {
                    nextByte = nextByte & 63;
                    nextByte = nextByte | 128;
                }
    
                guId+= getCharAtIndex(kHexChars, nextByte >> 4);
                guId+= getCharAtIndex(kHexChars, nextByte & 15);
            }
    
            guId = trimStringToSize(guId, 36);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return guId;
    }       
    
    @TestVisible private static String getCharAtIndex(String str, Integer index) {      
        try {
            if (str == null) {
                return null;
            } 
    
            if (str.length() <= 0) {
                return str;
            } 
    
            if (index == str.length()) {
                return null;
            }   
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return str.substring(index, index + 1);
    }    

    public static String trimStringToSize(String inputString, Integer maxLength) {   
        String outputString;

        try {           
            if (String.isNotEmpty(inputString) && maxLength != null && maxLength > 0) {
                if (inputString.length() > maxLength) {                                                 
                    outputString = inputString.substring(0, maxLength);
                } else {
                    outputString = inputString;
                }
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return outputString;
    }    
}