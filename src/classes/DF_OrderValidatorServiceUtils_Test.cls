@isTest
private class DF_OrderValidatorServiceUtils_Test {

    
    @isTest static void test_getHttpRequest() {                     
        // Setup data   
        final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;
        String requestStr = 'requestStr';
        HttpRequest req;
        User commUser;

        DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
                                                            Timeout__c
                                                     FROM   DF_Integration_Setting__mdt
                                                     WHERE  DeveloperName = :DF_OrderValidatorServiceUtils.INTEGRATION_SETTING_NAME];

        NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
        TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);

        END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL;
         
        Account acc = new Account(recordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId(), Tier__c = '1', Name='test nbn', Access_Seeker_ID__c = 'AS00000011');
        insert acc;
        Group gr =EE_DataSharingUtility.createNewGroup(DF_Constants.NBN_SELECT_GROUP_NAME+acc.Id);

        Map<String, String> mapAttributes = new Map<String, String>();
        mapAttributes.put('User.UserRoleId','AS00000011');
        mapAttributes.put('User.FederationIdentifier','testBSM_1');
        mapAttributes.put('User.LastName','nbnRSPTest_1');
        mapAttributes.put('User.FirstName','nbnRSPTest_1');
        mapAttributes.put('User.Email','testuser_1@nbnco.com.au.nbnRsp');
        mapAttributes.put('User.ProfileId','Enterprise Ethernet Plus,Access Seeker nbnSelect Feasibility');
        
        JITHandler handler = new JITHandler();
        //Create User
        
        commUser = handler.createUser(null,null,null,'testSAML',mapAttributes,null);


        system.runAs(commUser) {
            // Statements to be executed by this test user

        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acc.Id);//createOpportunityBundle(acct.Id);
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.Order_GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55bada';
        insert dfQuote;
    
        // Create DF Order
        DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        insert dfOrder;

            test.startTest();           
    
            req = DF_OrderValidatorServiceUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, requestStr, dfOrder.Id,'');
                                            
            test.stopTest();                
        }                    

        // Assertions
        system.AssertNotEquals(null, req);
    }    
    
    @isTest static void test_generateGUID() {                   
        // Setup data   
        String correlationId;  

        test.startTest();           

        correlationId = DF_OrderValidatorServiceUtils.generateGUID();
                                        
        test.stopTest();               

        // Assertions
        system.AssertNotEquals(null, correlationId);
    }     
    
    @isTest static void test_stripJsonNulls() {                     
        // Setup data   
        String jsonStr;
        Map<String, String> dataMap = new Map<String, String>();  
        
        dataMap.put('key1', 'value1');
        dataMap.put('key2', 'value2');
        dataMap.put('key3', 'value3');
        dataMap.put('key4', null);

        jsonStr = JSON.serialize(dataMap);

        test.startTest();           
        
        jsonStr = DF_OrderValidatorServiceUtils.stripJsonNulls(jsonStr);
                                        
        test.stopTest();               

        // Assertions
        system.AssertNotEquals(null, jsonStr);
    }

    @isTest static void test_stripJsonSpecificNulls() {
        // Setup data
        String jsonStr;
        Map<String, String> dataMap = new Map<String, String>();

        dataMap.put('key1', 'value1');
        dataMap.put('key2', 'value2');
        dataMap.put('associatedReferenceId', null);
        dataMap.put('id', null);

        jsonStr = JSON.serialize(dataMap);

        test.startTest();

        jsonStr = DF_OrderValidatorServiceUtils.stripSpecificJsonNull(jsonStr);

        test.stopTest();

        // Assertions
        system.AssertNotEquals(null, jsonStr);
    }

    @isTest static void test_trimStringToSize() {                   
        // Setup data   
        String string1 = 'stringtokeepSTRINGTOEXCLUDE'; 
        String string2;
        
        test.startTest();           
        
        string2 = DF_OrderValidatorServiceUtils.trimStringToSize(string1, 12);
                                        
        test.stopTest();               

        // Assertions
        system.AssertEquals('stringtokeep', string2);
    }    
    
    @isTest static void test_getDFOrder() {                     
        // Setup data   
        DF_Order__c dfOrder;

        // Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Create DF Order
        DF_Order__c dfOrderToCreate = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        insert dfOrderToCreate;

        test.startTest();           
        
        dfOrder = DF_OrderValidatorServiceUtils.getDFOrder(dfOrderToCreate.Id);
                                        
        test.stopTest();               

        // Assertions
        system.AssertNotEquals(null, dfOrder);
    }          
    
    @isTest static void test_validateOrderSuccessPostProcessing() {                     
        // Setup data   
        final String response = '{"field":"value"}';

        // Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Create DF Order
        DF_Order__c dfOrderToCreate = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        insert dfOrderToCreate;

        test.startTest();           
        
        DF_OrderValidatorServiceUtils.validateOrderSuccessPostProcessing(response, dfOrderToCreate.Id);
                                        
        test.stopTest();               

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT Id
                                         FROM   DF_Order__c 
                                         WHERE  Id = :dfOrderToCreate.Id
                                         AND    Order_Validation_Status__c = :DF_OrderValidatorServiceUtils.ORDER_VALIDATION_STATUS_SUCCESS];

        system.AssertEquals(false, dfOrderList.isEmpty());
    }       
    
    @isTest static void test_validateOrderFailurePostProcessing() {                     
        // Setup data   
        final String response = '{"field":"value"}';

        // Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Create DF Order
        DF_Order__c dfOrderToCreate = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        insert dfOrderToCreate;

        test.startTest();           
        
        DF_OrderValidatorServiceUtils.validateOrderFailurePostProcessing(response, dfOrderToCreate.Id);
                                        
        test.stopTest();               

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT Id
                                         FROM   DF_Order__c 
                                         WHERE  Id = :dfOrderToCreate.Id
                                         AND    Order_Validation_Status__c = :DF_OrderValidatorServiceUtils.ORDER_VALIDATION_STATUS_FAILED];

        system.AssertEquals(false, dfOrderList.isEmpty());
    }     
    
    @isTest static void test_validateOrderErrorPostProcessing() {                   
        // Setup data   
        final String response = '{"field":"value"}';

        // Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Create DF Order
        DF_Order__c dfOrderToCreate = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        insert dfOrderToCreate;

        test.startTest();           
        
        DF_OrderValidatorServiceUtils.validateOrderErrorPostProcessing('', dfOrderToCreate.Id);
                                        
        test.stopTest();               

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT Id
                                         FROM   DF_Order__c 
                                         WHERE  Id = :dfOrderToCreate.Id
                                         AND    Order_Validation_Status__c = :DF_OrderValidatorServiceUtils.ORDER_VALIDATION_STATUS_IN_ERROR];

        system.AssertEquals(false, dfOrderList.isEmpty());
    }    
        
    @isTest static void test_getOrderValidatorResponseStatus() {                    
        // Setup data   
        String response;
        String status;

        // Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Create DF Order
        DF_Order__c dfOrderToCreate = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        insert dfOrderToCreate;

        // Generate mock response
        response = DF_IntegrationTestData.buildOrderValidatorRespSuccess();

        test.startTest();           
        
        status = DF_OrderValidatorServiceUtils.getOrderValidatorResponseStatus(response);
                                        
        test.stopTest();               

        // Assertions
        system.AssertEquals('SUCCESS', status);
    }       
            
}