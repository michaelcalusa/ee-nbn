@isTest
private class DF_OrderValidatorService_Test {

    @isTest static void test_validateOrder_200_success() {  
        String response;
        User commUser;
        
        // Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.Order_GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55bada';
        insert dfQuote;
        
        // Build order json
        String dfOrderJSONString = DF_IntegrationTestData.buildDFOrderJSONString();
        
        // Create DF Order
        DF_Order__c dfOrder = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        dfOrder.Order_JSON__c = dfOrderJSONString;
        dfOrder.DF_Quote__c = dfQuote.Id;
        insert dfOrder;
        
        // Generate mock response
        response = DF_IntegrationTestData.buildOrderValidatorRespSuccess();
 
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

        // Set up commUser to run test as
        commUser = DF_TestData.createDFCommUser();      

        system.runAs(commUser) { 
            test.startTest();           
            
            DF_OrderValidatorService.validateOrder(dfOrder.Id);
                                            
            test.stopTest();                                             
        }   
        
        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT Id
                                         FROM   DF_Order__c 
                                         WHERE  Id = :dfOrder.Id
                                         AND    Order_Validation_Status__c = :DF_OrderValidatorServiceUtils.ORDER_VALIDATION_STATUS_SUCCESS];

        system.AssertEquals(false, dfOrderList.isEmpty());
    }    
    
    @isTest static void test_validateOrder_200_failed() {       
        String response;
        User commUser;
        
        // Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.Order_GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55bada';
        insert dfQuote;
        
        // Build order json
        String dfOrderJSONString = DF_IntegrationTestData.buildDFOrderJSONString();
        
        // Create DF Order
        DF_Order__c dfOrder = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        dfOrder.Order_JSON__c = dfOrderJSONString;
        dfOrder.DF_Quote__c = dfQuote.Id;
        insert dfOrder;
        
        // Generate mock response
        response = DF_IntegrationTestData.buildOrderValidatorRespFailed();
 
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

        // Set up commUser to run test as
        commUser = DF_TestData.createDFCommUser();      

        system.runAs(commUser) { 
            test.startTest();           
            
            DF_OrderValidatorService.validateOrder(dfOrder.Id);
                                            
            test.stopTest();                                             
        }   
        
        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT Id
                                         FROM   DF_Order__c 
                                         WHERE  Id = :dfOrder.Id
                                         AND    Order_Validation_Status__c = :DF_OrderValidatorServiceUtils.ORDER_VALIDATION_STATUS_FAILED];

        system.AssertEquals(false, dfOrderList.isEmpty());
    }    
    
    @isTest static void test_validateOrder_unsuccessful_400() {         
        String response = '';
        User commUser;
        
        // Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Build order json
        String dfOrderJSONString = DF_IntegrationTestData.buildDFOrderJSONString();
        
        // Create DF Order
        DF_Order__c dfOrder = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        dfOrder.Order_JSON__c = dfOrderJSONString;
        insert dfOrder;
 
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(400, response, null));

        // Set up commUser to run test as
        commUser = DF_TestData.createDFCommUser();      

        system.runAs(commUser) { 
            test.startTest();           
            
            DF_OrderValidatorService.validateOrder(dfOrder.Id);
                                            
            test.stopTest();                                             
        }   
        
        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT Id
                                         FROM   DF_Order__c 
                                         WHERE  Id = :dfOrder.Id
                                         AND    Order_Validation_Status__c = :DF_OrderValidatorServiceUtils.ORDER_VALIDATION_STATUS_IN_ERROR];

        system.AssertEquals(false, dfOrderList.isEmpty());
    }    
    
}