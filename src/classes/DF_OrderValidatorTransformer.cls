public class DF_OrderValidatorTransformer {
    
    public static OrderValidatorRequest.ProductOrder generateRequest(String dfOrderId) {
        OrderValidatorRequest.ProductOrder productOrderInst = new OrderValidatorRequest.ProductOrder();
        
        String orderJSON;       
        Integer ovcTotal = 0;
        string csaId = '';
        try {
            // Get DF Order data
            DF_Order__c dfOrder = getDFOrderDetails(dfOrderId);
            system.debug('DF_OrderValidatorTransformer.generateRequest - dfOrder.Id: ' + dfOrder.Id);
            
            if(dfOrder.DF_Quote__r.LAPI_Response__c != null && dfOrder.DF_Quote__r.LAPI_Response__c != '') {
                DF_ServiceFeasibilityResponse df_sfJson = (DF_ServiceFeasibilityResponse)Json.deserialize(dfOrder.DF_Quote__r.LAPI_Response__c, DF_ServiceFeasibilityResponse.class);
                if(df_sfJson != null) {
                    csaId = df_sfJson.csaId;
                }
            }
            
            if (dfOrder != null) {
                orderJSON = dfOrder.Order_JSON__c;  
                
                if (String.isEmpty(orderJSON)) {
                    throw new CustomException(DF_OrderValidatorServiceUtils.ERR_DFORDER_ORDER_JSON_NOT_FOUND);
                }                   
            }

            system.debug('DF_OrderValidatorTransformer.generateRequest - orderJSON: ' + orderJSON);

            DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot dfOrderDataJSONRootInst = getDFOrderDataJSONRoot(orderJSON);

            /*** Extract Data from Order JSON  ***/
            String afterHoursAppointment;
            
            // Service data
            String term;
            String serviceRestorationSLA;
            String accessAvailabilityTarget;
            String zone;
            String productType;
            String orderType;
            
            // BTD data
            String btdType;
            
            // UNI data
            String interfaceBandwidth;
            String interfaceType;
            String ovcType;
            String tpId;
            //String cosMappingMode;
            
            // Location data
            String locationId;
            
            // MAPS to store OVC info for OVC - Key: OVC[n] / Value: fieldvalue
            Map<Integer, String> ovcPOIMap = new Map<Integer, String>();
            Map<Integer, String> ovcNNIMap = new Map<Integer, String>();
            Map<Integer, String> ovcTransientIdMap = new Map<Integer, String>();
            Map<Integer, String> ovcRouteTypeMap = new Map<Integer, String>();
            Map<Integer, Integer> ovcSVLanIdMap = new Map<Integer, Integer>();
            Map<Integer, String> ovcMaximumFrameSizeMap = new Map<Integer, String>();
            Map<Integer, String> ovcCosHighBandwidthMap = new Map<Integer, String>();
            Map<Integer, String> ovcCosMediumBandwidthMap = new Map<Integer, String>();
            Map<Integer, String> ovcCosLowBandwidthMap = new Map<Integer, String>();
            Map<Integer, String> ovcUniVLanIdMap = new Map<Integer, String>();
            Map<Integer, String> ovcCosMappingModeMap = new Map<Integer, String>();
            
            DF_OrderDataJSONWrapper.ProductOrder odJSONWrapperProductOrderInst = new DF_OrderDataJSONWrapper.ProductOrder();
            odJSONWrapperProductOrderInst = dfOrderDataJSONRootInst.productOrder;
            
            // Get afterHoursAppointment
            afterHoursAppointment = odJSONWrapperProductOrderInst.afterHoursAppointment;
            
            DF_OrderDataJSONWrapper.ProductOrderComprisedOf odJSONWrapperProductOrderComprisedOfInst = new DF_OrderDataJSONWrapper.ProductOrderComprisedOf();
            odJSONWrapperProductOrderComprisedOfInst = odJSONWrapperProductOrderInst.productOrderComprisedOf;
            
            // Get locationId
            DF_OrderDataJSONWrapper.ItemInvolvesLocation odJSONWrapperItemInvolvesLocationInst = new DF_OrderDataJSONWrapper.ItemInvolvesLocation();
            odJSONWrapperItemInvolvesLocationInst = odJSONWrapperProductOrderComprisedOfInst.itemInvolvesLocation;
            locationId = odJSONWrapperItemInvolvesLocationInst.id;
            
            DF_OrderDataJSONWrapper.ItemInvolvesProductL1 itemInvolvesProductL1Inst = new DF_OrderDataJSONWrapper.ItemInvolvesProductL1();
            itemInvolvesProductL1Inst = odJSONWrapperProductOrderComprisedOfInst.itemInvolvesProduct;
            
            term = itemInvolvesProductL1Inst.term;
            serviceRestorationSLA = itemInvolvesProductL1Inst.serviceRestorationSLA;
            accessAvailabilityTarget = itemInvolvesProductL1Inst.accessAvailabilityTarget;
            zone = itemInvolvesProductL1Inst.zone;
            productType = 'EE';
            orderType = 'Connect';
            List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1> referencesProductOrderItemList = new List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1>();
            referencesProductOrderItemList = odJSONWrapperProductOrderComprisedOfInst.referencesProductOrderItem;
            
            // Stores referencesProductOrderItem/itemInvolvesProduct node to get UNI info later
            DF_OrderDataJSONWrapper.ItemInvolvesProductL3 itemInvolvesProductL3UNIInst = new DF_OrderDataJSONWrapper.ItemInvolvesProductL3();
            
            // List is used to store all OVC nodes for processing later
            List<DF_OrderDataJSONWrapper.ItemInvolvesProductL2> itemInvolvesProductL2OVCList = new List<DF_OrderDataJSONWrapper.ItemInvolvesProductL2>();
                        
            // Get BTD info
            // Loop thru referencesProductOrderItemList and search for type = BTD
            for (DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 referencesProductOrderItemL1Iterator : referencesProductOrderItemList) {              
                // Get itemInvolvesProduct node
                DF_OrderDataJSONWrapper.ItemInvolvesProductL2 itemInvolvesProductL2Inst = referencesProductOrderItemL1Iterator.itemInvolvesProduct;
                
                String type = itemInvolvesProductL2Inst.type;
                
                if (type.equalsIgnoreCase('BTD')) {
                    // Get BTD field data
                    btdType = itemInvolvesProductL2Inst.btdType;
                    
                    // Also get referencesProductOrderItem/itemInvolvesProduct node to get UNI info later
                    itemInvolvesProductL3UNIInst = referencesProductOrderItemL1Iterator.referencesProductOrderItem[0].itemInvolvesProduct;                  
                } else if (type.equalsIgnoreCase('OVC')) {
                    // Add OVC node to list
                    itemInvolvesProductL2OVCList.add(itemInvolvesProductL2Inst);
                }                               
            }
                        
            // Get UNI info
            interfaceBandwidth = itemInvolvesProductL3UNIInst.interfaceBandwidth;
            interfaceType = itemInvolvesProductL3UNIInst.interfaceType;
            ovcType = itemInvolvesProductL3UNIInst.ovcType;
            tpId = itemInvolvesProductL3UNIInst.tpId;
            //cosMappingMode = itemInvolvesProductL3UNIInst.cosMappingMode; 
            
            if (!itemInvolvesProductL2OVCList.isEmpty()) {
                Integer ovcNo = 1;                  
                
                // Loop thru list of OVCs and store all the field info in local MAPS
                for (DF_OrderDataJSONWrapper.ItemInvolvesProductL2 itemInvolvesProductL2OVCIterator : itemInvolvesProductL2OVCList) {
                    ovcTotal++;
                                                            
                    String ovcPOI = itemInvolvesProductL2OVCIterator.poi;       
                    ovcPOIMap.put(ovcNo, ovcPOI);                    
                    
                    String ovcTransientId = itemInvolvesProductL2OVCIterator.ovcTransientId;       
                    ovcTransientIdMap.put(ovcNo, ovcTransientId);
                                                            
                    String ovcNNI = itemInvolvesProductL2OVCIterator.nni;       
                    ovcNNIMap.put(ovcNo, ovcNNI);
                    
                    String ovcRouteType = itemInvolvesProductL2OVCIterator.routeType;           
                    ovcRouteTypeMap.put(ovcNo, ovcRouteType);
                    
                    Integer ovcSVLanId = itemInvolvesProductL2OVCIterator.sVLanId;      
                    ovcSVLanIdMap.put(ovcNo, ovcSVLanId);
                    
                    String ovcMaximumFrameSize = itemInvolvesProductL2OVCIterator.maximumFrameSize;         
                    ovcMaximumFrameSizeMap.put(ovcNo, ovcMaximumFrameSize);
                    
                    String ovcCosHighBandwidth = itemInvolvesProductL2OVCIterator.cosHighBandwidth;         
                    ovcCosHighBandwidthMap.put(ovcNo, ovcCosHighBandwidth);         

                    String ovcCosMediumBandwidth = itemInvolvesProductL2OVCIterator.cosMediumBandwidth;     
                    ovcCosMediumBandwidthMap.put(ovcNo, ovcCosMediumBandwidth); 
                    
                    String ovcCosLowBandwidth = itemInvolvesProductL2OVCIterator.cosLowBandwidth;           
                    ovcCosLowBandwidthMap.put(ovcNo, ovcCosLowBandwidth);               

                    String ovcUniVLanId = itemInvolvesProductL2OVCIterator.uniVLanId;           
                    ovcUniVLanIdMap.put(ovcNo, ovcUniVLanId);   

                    String ovcCosMappingMode = itemInvolvesProductL2OVCIterator.cosMappingMode;
                    ovcCosMappingModeMap.put(ovcNo, ovcCosMappingMode);                                                                         

                    // Increment ovcNo
                    ovcNo++;
                }
            }                       
            
            /*** Build OrderValidatorRequest instance  ***/

            // Empty nodes
            OrderValidatorRequest.AccessSeekerInteraction accessSeekerInteractionInst = new OrderValidatorRequest.AccessSeekerInteraction();
            
            // Empty nodes
            OrderValidatorRequest.AccessSeekerContact accessSeekerContactInst = new OrderValidatorRequest.AccessSeekerContact();
    
            // Empty nodes
            OrderValidatorRequest.UnstructuredAddress unstructuredAddressInst = new OrderValidatorRequest.UnstructuredAddress();
            
            // Empty nodes
            OrderValidatorRequest.ItemInvolvesContact itemInvolvesContactInst = new OrderValidatorRequest.ItemInvolvesContact();
            itemInvolvesContactInst.unstructuredAddress = unstructuredAddressInst;
            
            List<OrderValidatorRequest.ItemInvolvesContact> itemInvolvesContactInstList = new List<OrderValidatorRequest.ItemInvolvesContact>();
            itemInvolvesContactInstList.add(itemInvolvesContactInst);
    
            // Empty nodes
            OrderValidatorRequest.ItemInvolvesLocation itemInvolvesLocationInst = new OrderValidatorRequest.ItemInvolvesLocation();
            itemInvolvesLocationInst.id = locationId;
    
            // ItemInvolvesProductL1
            OrderValidatorRequest.ItemInvolvesProductL1 itemInvolvesProductL1InstSVC = new OrderValidatorRequest.ItemInvolvesProductL1();
            itemInvolvesProductL1InstSVC.term = term;
            itemInvolvesProductL1InstSVC.serviceRestorationSLA = serviceRestorationSLA;
            itemInvolvesProductL1InstSVC.accessAvailabilityTarget = accessAvailabilityTarget;
            itemInvolvesProductL1InstSVC.zone = zone;
            itemInvolvesProductL1InstSVC.productType = productType;

            // ItemInvolvesProductL3 - UNI
            OrderValidatorRequest.ItemInvolvesProductL3 itemInvolvesProductL3InstUNI = new OrderValidatorRequest.ItemInvolvesProductL3();
            itemInvolvesProductL3InstUNI.type = 'UNI';
            itemInvolvesProductL3InstUNI.interfaceBandwidth = interfaceBandwidth;
            itemInvolvesProductL3InstUNI.interfaceType = interfaceType;
            itemInvolvesProductL3InstUNI.ovcType = ovcType;
            itemInvolvesProductL3InstUNI.tpId = tpId;
            //itemInvolvesProductL3InstUNI.cosMappingMode = cosMappingMode;
    
            // ReferencesProductOrderItemL2
            OrderValidatorRequest.ReferencesProductOrderItemL2 referencesProductOrderItemL2Inst = new OrderValidatorRequest.ReferencesProductOrderItemL2();
            referencesProductOrderItemL2Inst.action = DF_OrderValidatorServiceUtils.ACTION_ADD;
            referencesProductOrderItemL2Inst.itemInvolvesProduct = itemInvolvesProductL3InstUNI;
    
            List<OrderValidatorRequest.ReferencesProductOrderItemL2> referencesProductOrderItemL2InstUNIList = new List<OrderValidatorRequest.ReferencesProductOrderItemL2>();
            referencesProductOrderItemL2InstUNIList.add(referencesProductOrderItemL2Inst);

            // ItemInvolvesProductL2 - BTD
            OrderValidatorRequest.ItemInvolvesProductL2 itemInvolvesProductL2InstBTD = new OrderValidatorRequest.ItemInvolvesProductL2();
            itemInvolvesProductL2InstBTD.type = 'BTD';
            itemInvolvesProductL2InstBTD.btdType = btdType;

            // Declare here - holds: BTDs, OVCs
            List<OrderValidatorRequest.ReferencesProductOrderItemL1> referencesProductOrderItemL1InstList = new List<OrderValidatorRequest.ReferencesProductOrderItemL1>();

            // Loop through all OVC maps and build up OVC list for outbound
            for (Integer ovcNo = 1; ovcNo <= ovcTotal; ovcNo++) {               
                String poi;
                String nni;
                String oTransientId; 
                String routeType;
                Integer sVLanId;
                String maximumFrameSize;
                String cosHighBandwidth;
                String cosMediumBandwidth;
                String cosLowBandwidth;
                String uniVLanId;
                String cosMappingMode;
                
                // Get values from maps
                poi = ovcPOIMap.get(ovcNo);  
                oTransientId = ovcTransientIdMap.get(ovcNo);       
                nni = ovcNNIMap.get(ovcNo);             
                routeType = ovcRouteTypeMap.get(ovcNo);             
                sVLanId = ovcSVLanIdMap.get(ovcNo);                 
                maximumFrameSize = ovcMaximumFrameSizeMap.get(ovcNo);               
                cosHighBandwidth = ovcCosHighBandwidthMap.get(ovcNo);               
                cosMediumBandwidth = ovcCosMediumBandwidthMap.get(ovcNo);               
                cosLowBandwidth = ovcCosLowBandwidthMap.get(ovcNo);                 
                uniVLanId = ovcUniVLanIdMap.get(ovcNo);     
                cosMappingMode = ovcCosMappingModeMap.get(ovcNo);       
                
                // ItemInvolvesProductL2 - OVC
                OrderValidatorRequest.ItemInvolvesProductL2 itemInvolvesProductL2InstOVC = new OrderValidatorRequest.ItemInvolvesProductL2();
                itemInvolvesProductL2InstOVC.type = 'OVC';
                itemInvolvesProductL2InstOVC.poi = poi;
                itemInvolvesProductL2InstOVC.ovcTransientId = oTransientId;
                itemInvolvesProductL2InstOVC.nni = nni;
                itemInvolvesProductL2InstOVC.routeType = routeType;
                itemInvolvesProductL2InstOVC.sVLanId = sVLanId;
                itemInvolvesProductL2InstOVC.maximumFrameSize = maximumFrameSize;
                itemInvolvesProductL2InstOVC.cosHighBandwidth = cosHighBandwidth;
                itemInvolvesProductL2InstOVC.cosMediumBandwidth = cosMediumBandwidth;
                itemInvolvesProductL2InstOVC.cosLowBandwidth = cosLowBandwidth;
                itemInvolvesProductL2InstOVC.uniVLanId = uniVLanId;         
                itemInvolvesProductL2InstOVC.cosMappingMode = cosMappingMode ;
                
                // Create an OVC entry for referencesProductOrderItem
                OrderValidatorRequest.ReferencesProductOrderItemL1 referencesProductOrderItemL1InstOVC = new OrderValidatorRequest.ReferencesProductOrderItemL1();
                referencesProductOrderItemL1InstOVC.action = DF_OrderValidatorServiceUtils.ACTION_ADD;
                referencesProductOrderItemL1InstOVC.itemInvolvesProduct = itemInvolvesProductL2InstOVC;
                
                // Add OVC entry for referencesProductOrderItem to parent referencesProductOrderItem list
                referencesProductOrderItemL1InstList.add(referencesProductOrderItemL1InstOVC); // add OVC node                      
            }

            // ReferencesProductOrderItemL1 node - BTD
            OrderValidatorRequest.ReferencesProductOrderItemL1 referencesProductOrderItemL1InstBTD = new OrderValidatorRequest.ReferencesProductOrderItemL1();
            referencesProductOrderItemL1InstBTD.action = DF_OrderValidatorServiceUtils.ACTION_ADD;
            referencesProductOrderItemL1InstBTD.itemInvolvesProduct = itemInvolvesProductL2InstBTD;     
            referencesProductOrderItemL1InstBTD.referencesProductOrderItem = referencesProductOrderItemL2InstUNIList;   

            // Add BTD entry for referencesProductOrderItem to parent referencesProductOrderItem list
            referencesProductOrderItemL1InstList.add(referencesProductOrderItemL1InstBTD); // add BTD node
            
            // ProductOrderComprisedOf node
            OrderValidatorRequest.ProductOrderComprisedOf productOrderComprisedOfInst = new OrderValidatorRequest.ProductOrderComprisedOf();
            productOrderComprisedOfInst.action = DF_OrderValidatorServiceUtils.ACTION_ADD;
            productOrderComprisedOfInst.itemInvolvesLocation = itemInvolvesLocationInst;
            productOrderComprisedOfInst.itemInvolvesProduct = itemInvolvesProductL1InstSVC;
            productOrderComprisedOfInst.referencesProductOrderItem = referencesProductOrderItemL1InstList;
    
            // ProductOrder node (root node)
            productOrderInst.accessSeekerInteraction = accessSeekerInteractionInst;
            productOrderInst.afterHoursAppointment = afterHoursAppointment;
            productOrderInst.csaId = csaId;
            productOrderInst.itemInvolvesContact = itemInvolvesContactInstList;
            productOrderInst.productOrderComprisedOf = productOrderComprisedOfInst;
            productOrderInst.accessSeekerContact = accessSeekerContactInst;
            productOrderInst.orderType = orderType;
    
        } catch (Exception e) {
            throw new CustomException(e.getMessage());            
        }       
                
        return productOrderInst;
    }    

    @TestVisible private static DF_Order__c getDFOrderDetails(String dfOrderId) {                                
        DF_Order__c dfOrder;

        try {
            if (String.isNotEmpty(dfOrderId)) {                                        
                dfOrder = [SELECT Order_JSON__c, DF_Quote__r.LAPI_Response__c
                           FROM   DF_Order__c            
                           WHERE  Id = :dfOrderId];                                                    
            }                                                          
        } catch (Exception e) {    
            throw new CustomException(e.getMessage());
        }                     
                    
        return dfOrder;
    }      

    @TestVisible private static DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot getDFOrderDataJSONRoot(String orderJSON) {                              
        DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot dfOrderDataJSONRootInst = new DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot();

        try {
            // Serialise orderJSON string into dfOrderDataJSONRootInst
            dfOrderDataJSONRootInst = (DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot)JSON.deserialize(orderJSON, DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot.Class);                                                                             
        } catch (Exception e) {    
            throw new CustomException(e.getMessage());
        }                     
                    
        return dfOrderDataJSONRootInst;
    }
}