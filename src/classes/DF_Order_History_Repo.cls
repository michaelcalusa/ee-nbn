/**
 * Created by alan on 2019-03-08.
 */

public without sharing class DF_Order_History_Repo {

    public List<DF_Order_History_Wrapper> getNsNotificationHistory(String orderId){
        //CPST-7850 | michaelcalusa@nbnco.com.au | ability to show revised completion date in NS
        List<DF_Order__History> orderHistoryList = [
                SELECT toLabel(NewValue),
                    CreatedDate,
                    Field,
                    Parent.Order_Notifier_Response_JSON__c,
                        CreatedBy.Name
                FROM DF_Order__History
                WHERE parentId =:orderId AND
        (
                Field = 'Order_Status__c' OR
                Field = 'Order_Sub_Status__c' OR
                Field = 'Appian_Notification_Date__c' OR
                Field = 'Notification_Type__c' OR
                Field = 'Notification_Reason__c' OR
                Field = 'Reason_Code__c'
        )
        Order by CreatedDate];

        List<DF_Order_History_Wrapper> orderHistoryWrapperList = new List<DF_Order_History_Wrapper>();
        for(DF_Order__History orderHistory : orderHistoryList){
            DF_Order_History_Wrapper wrapper = new DF_Order_History_Wrapper(orderHistory.Field, orderHistory.NewValue, orderHistory.CreatedDate, orderHistory.CreatedBy.Name);
            orderHistoryWrapperList.add(wrapper);
        }

        return orderHistoryWrapperList;

    }


}