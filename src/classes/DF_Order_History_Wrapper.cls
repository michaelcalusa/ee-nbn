/**
 * Created by alan on 2019-03-11.
 */

public class DF_Order_History_Wrapper {

    private String field {get;set;}
    private Object newValue {get;set;}
    private Datetime modifiedDate {get;set;}
    private String lastModifiedBy {get;set;}

    public DF_Order_History_Wrapper(String field, Object newValue, Datetime modifiedDate, String lastModifiedBy){
        this.field = field;
        this.newValue = newValue;
        this.modifiedDate = modifiedDate;
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getField(){
        return field;
    }

    public Object getNewValue(){
        return newValue;
    }

    public Datetime getModifiedDate(){
        return modifiedDate;
    }

    public String getLastModifiedBy(){
        return lastModifiedBy;
    }

}