@isTest
private class DF_Order_Installation_DetailsCntlr_Test {
    
@isTest static void test1(){
            test.startTest(); 

            User dfuser =createDFCommUser();


            system.runAs(dfuser) {
                
            PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'DF_BC_Customer_Plus_Permission'];
            insert new PermissionSetAssignment(AssigneeId = dfuser.id, PermissionSetId = ps.Id );   
            system.debug('!!!!! user AccountId' + dfuser.AccountId);
            system.debug('!!!!! user ContactId' + dfuser.ContactId);
            ID contactId = [Select contactid from User where id =: dfuser.id].contactId;
            system.debug('!!!!contactId ' +contactId); 

            ID AccID  = [Select AccountID from Contact where id =: contactId].AccountId;
            system.debug('!!!!AccID ' +AccID);

             //       PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'DF_BC_Customer_Plus_Permission'];
            //insert new PermissionSetAssignment(AssigneeId = dfuser.id, PermissionSetId = ps.Id );
            List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
            
            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';

            // Create OpptyBundle
            DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(AccID);
            insert opptyBundle;

            // Create Oppty
            Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
            oppty.Opportunity_Bundle__c = opptyBundle.Id;
            insert oppty;

            // Create DFQuote recs
            DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');         
            dfQuote1.Proceed_to_Pricing__c = true;
            dfQuoteList.add(dfQuote1);

            insert dfQuoteList;         
            DF_Order__c testOrder = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle.Id );
            testOrder.Heritage_Site__c = 'Yes';
            testOrder.Induction_Required__c = 'Yes';
            testOrder.Security_Required__c = 'Yes';
            testOrder.Site_Notes__c = 'Site Notes';
            testOrder.Trading_Name__c = 'Trader Joes TEST SITE';
            Insert testOrder;

            DF_Order__c  testOrder1 = [SELECT Id, Opportunity_Bundle__c,DF_Quote__c FROM DF_Order__c WHERE Trading_Name__c = 'Trader Joes TEST SITE' LIMIT 1];
            Opportunity oppt = [SELECT Id, Name From Opportunity WHERE Opportunity_Bundle__c =: testOrder1.Opportunity_Bundle__c LIMIT 1];
            DF_Quote__c quote = [SELECT Id From DF_Quote__c WHERE Opportunity__c =: oppty.Id];
            DF_Opportunity_Bundle__c ob = [SELECT id FROM DF_Opportunity_Bundle__c WHERE Id =: testOrder1.Opportunity_Bundle__c];
            
            

            String test1Result = DF_Order_Installation_DetailsController.loadDFOrderInstallationDetails(testOrder1.Id);

            DF_Order_Installation_DetailsController.setDFOrderInstallationDetails(testOrder1.Id, '2001-12-12', 'Wall', 'AC (240V)', 'AC (240V)', 'Notes');

            //DF_Order_Installation_DetailsController.updateOppClosedWon(oppt.Id);

            //DF_Order_Installation_DetailsController.processUpdateDFQuoteStatus(oppt.Id);

            string location = '{"status":"Order Draft","SCR":"1297.0000","SCNR":"5000.0000","quoteId":"EEQ-0000000324","orderId":"a7EN000000092gQMAQ","locId":"LOC000006231845","id":"a6aN00000000k5iIAA","FBC":0,"address":"6 LENNOX ST RAVENSWOOD TAS 7250 Australia","OrderId":"a7EN000000092gQMAQ","basketId":"a1eN0000000z2amIAA","UNI":[{"BTDType":"Standard (CSAS)","interfaceBandwidth":"1GB","AAT":"","SLA":"Premium - 12 (24/7)","term":"12","zone":"Zone 3","AHA":"No","coSRecurring":"0","oVCType":"Access EVPL","interfaceTypes":"Copper (Auto-negotiate)","tPID":"0x8100","Id":"a1hN0000001TOJTIA4"}],"OVCs":[{"OVCId":"a1hN0000001TOJUIA4","OVCName":"OVC 1","CSA":"CSA180000002222","monthlyCombinedCharges":"497.0000","routeType":"Local","coSHighBandwidth":"20","coSMediumBandwidth":"10","coSLowBandwidth":"0","routeRecurring":"0.0000","coSRecurring":"497.0000","POI":"1-MEL-MELBOURNE-MP","status":"Incomplete","mappingMode":"DSCP","NNIGroupId":"NNI090000015859","sTag":"0","ceVlanId":"12","ovcMaxFrameSize":"Jumbo (9000 Bytes)"}],"prodChargeConfigId":"a1hN0000001TOJTIA4","ovcConfigId":"a1hN0000001TOJUIA4","businessContactList":[{"busContSurname":"asf","busContSuburb":"sdf","busContStreetAddress":"asf","busContState":"VIC","busContRole":"asdf","busContPostcode":"2000","busContNumber":"0412123123","busContId":"003N000001DhSVkIAN","busContFirstName":"asd","busContEmail":"asdf@asd.com","busContIndex":1}],"siteContactList":[{"siteContSurname":"asdf","siteContNumber":"0412123123","siteContId":"003N000001DhSWsIAN","siteContFirstName":"asd","siteContEmail":"asdf@asdf.com","siteContIndex":1}],"SiteDetails":{"Trading Name":[""],"Heritage Site":["Yes"],"Induction Required":["Yes"],"Security Required":["Yes"],"Site Notes":[""]}}';

            DF_Order_Installation_DetailsController.updateOrderJSON(testOrder1.Id,location);

            test.stopTest();  
            }

    }
    
    public static User createDFCommUser() {
        Profile commProfile = [SELECT Id 
                               FROM Profile 
                               WHERE Name = 'DF Partner User'];
        
        system.debug('createDFCommUser - commProfile: ' + commProfile);
        
        Account commAcct = new Account();
        commAcct.Name = 'My account';
        commAcct.Tier__c = '1';
        commAcct.Billing_ID__c = '123';
        //commAcct.Number_of_Employees_for_ICT__c = '1';
        insert commAcct; 
        system.debug('createDFCommUser - test account inserted...: account id: ' + commAcct.Id);
        
        Contact commContact = new Contact();
        commContact.LastName ='testContact';
        commContact.AccountId = commAcct.Id;
        insert commContact;  
        system.debug('createDFCommUser - test contact inserted...: contact id: ' + commContact.Id);
        
        User commUser = new User();
        commUser.Alias = 'test123';
        commUser.Email = 'test123@noemail.com';
        commUser.Emailencodingkey = 'ISO-8859-1';
        commUser.Lastname = 'Testing';
        commUser.Languagelocalekey = 'en_US';
        commUser.Localesidkey = 'en_AU';
        commUser.Profileid = commProfile.Id;
        commUser.IsActive = true;
        commUser.ContactId = commContact.Id;
        commUser.Timezonesidkey = 'Australia/Sydney';
        commUser.Username = 'tester@noemail.com.test';
        
        insert commUser;
        system.debug('createDFCommUser - test user inserted...: user id: ' + commUser);

        
        return commUser;
    }
    
}