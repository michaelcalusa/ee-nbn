public with sharing class DF_Order_Installation_DetailsController {
    @AuraEnabled 
    public static String loadDFOrderInstallationDetails(String dfOrderId){
        String jsonRetVal = '';

        DF_Order__c objObject = new DF_Order__c();  
        DF_Order__c order;
        List<DF_Order__c> orderList;
        

        try {       
            if (String.isNotEmpty(dfOrderId)) {               
                orderList = [SELECT     Id,     Customer_Required_Date__c,NTD_Mounting__c,Power_Supply_1__c,Power_Supply_2__c, Installation_Notes__c
                                        FROM   DF_Order__c
                                        WHERE  Id = :dfOrderId];                                                   
            }
            if (!orderList.isEmpty() && orderList.size()>0) {
                order = orderList[0];  
                Datetime d =  order.Customer_Required_Date__c;
                System.debug('!!!!!!'+ d);
                string retDate = order.Customer_Required_Date__c != null ? d.format('YYYY-MM-dd'): '';
                jsonRetVal = '{ "Customer Required Date":["'+  retDate +'"], "NTD Mounting":["'+nullToEmpty(order.NTD_Mounting__c)+'"],'  
                +'"Power Supply 1":["'+nullToEmpty(order.Power_Supply_1__c) +'"], "Power Supply 2":["'+ nullToEmpty(order.Power_Supply_2__c )+'"],' 
                +'"Installation Notes":["'+nullToEmpty(order.Installation_Notes__c) +'"]}'; 
            }


        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        } 
        return jsonRetVal;

    }

    @AuraEnabled 
    public static void setDFOrderInstallationDetails(String dfOrderId, String custReqDate, String ntdMounting, String powerSupp1, String powerSupp2, String installationNotes){
        system.debug('!!! dfOrderId, String custReqDate, String ntdMounting, String powerSupp1, String powerSupp2, String installationNotes !!! ' +dfOrderId+' '+custReqDate+' '+ntdMounting+' '+ powerSupp1+' '+ powerSupp2+' '+installationNotes);

        DF_Order__c objObject = new DF_Order__c();  
        DF_Order__c order;
        List<DF_Order__c> orderList;
        System.debug('custReqDate !!! '+ custReqDate);

        try {       
            if (String.isNotEmpty(dfOrderId)) {       

                orderList = [SELECT   Id, Customer_Required_Date__c,NTD_Mounting__c,Power_Supply_1__c,Power_Supply_2__c, Installation_Notes__c
                                        FROM   DF_Order__c
                                        WHERE  Id = :dfOrderId];                                                   
            }
            if (!orderList.isEmpty()) {
                order = orderList[0];
                if(custReqDate!=null && custReqDate != ''){
                    system.debug('!!!!! Date' + Date.valueOf(custReqDate));
                    order.Customer_Required_Date__c = Date.valueOf(custReqDate);
                }else if(custReqDate == ''){
                    order.Customer_Required_Date__c = null;
                }
                order.NTD_Mounting__c= ntdMounting;
                order.Power_Supply_1__c = powerSupp1;
                order.Power_Supply_2__c = powerSupp2;
                order.Installation_Notes__c = installationNotes;
                update order;
            }


        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        } 
    
    }
    
    @AuraEnabled 
    public static void updateOrderJSON(String dfOrderId, String location)
    {      
        system.debug('!!!!!!!! updateOrderJSON ' + dfOrderId +' '+ location);

        DF_Order__c order;
        List<DF_Order__c> orderList;
        String jsonOutput;
        

        try {       
            if (String.isNotEmpty(dfOrderId)) {       

                orderList = [SELECT   Id, Heritage_Site__c,Induction_Required__c,Security_Required__c,Site_Notes__c,Trading_Name__c, DF_Quote__r.GUID__c,
                                DF_Quote__r.Opportunity__c, DF_Quote__r.LAPI_Response__c, Order_JSON__c, Order_Status__c,Order_Submitted_Date__c
                                        FROM   DF_Order__c
                                        WHERE  Id = :dfOrderId];                                                   
            }
            if (!orderList.isEmpty()) {
                order = orderList[0];
                jsonOutput = DF_OrderJSONDataHelper.generateJSONStringForOrder(dfOrderId,location);
                order.Order_JSON__c = jsonOutput;
                //order.Order_Status__c = 'Pending';
                order.Order_Submitted_Date__c = System.today();
                //sending order to appian
                DF_OrderSubmitUtility.processDFOrder(order);
                
                update order;
            }

        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        } 

    }
 
    @TestVisible private static String nullToEmpty(String input) {
        return input == null ? '' : input;
    }
    
    @AuraEnabled
    public static String getBundleId(String quoteId) {
        return DF_ProductController.getBundleId(quoteId);
    }
}