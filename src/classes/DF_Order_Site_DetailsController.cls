public with sharing class DF_Order_Site_DetailsController {
@AuraEnabled 
 public static String loadDFOrderSiteDetails(String dfOrderId){
 	String jsonRetVal = '';

        DF_Order__c objObject = new DF_Order__c();  
        DF_Order__c order;
        List<DF_Order__c> orderList;
        

        try {       
            if (String.isNotEmpty(dfOrderId)) {               
                orderList = [SELECT     Id, Heritage_Site__c,Induction_Required__c,Security_Required__c,Site_Notes__c,Trading_Name__c
                                        FROM   DF_Order__c
                                        WHERE  Id = :dfOrderId];                                                   
            }
            if (!orderList.isEmpty()) {
                order = orderList[0];  
                jsonRetVal = '{ "Trading Name":["'+  nullToEmpty(order.Trading_Name__c ) +'"], "Heritage Site":["'+nullToEmpty(order.Heritage_Site__c)+'"],'  
                +'"Induction Required":["'+nullToEmpty(order.Induction_Required__c) +'"], "Security Required":["'+ nullToEmpty(order.Security_Required__c) +'"],' 
                +'"Site Notes":["'+nullToEmpty(order.Site_Notes__c) +'"]}'; 
            }


        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        } 
        return jsonRetVal;

 }

 @AuraEnabled 
 public static void setDFOrderSiteDetails(String dfOrderId, String tradingName, String heritageSite, String inductionReqd, String securityReqd, String siteNotes){
 	String jsonRetVal = '';

        DF_Order__c objObject = new DF_Order__c();  
        DF_Order__c order;
        List<DF_Order__c> orderList;
        

        try {       
            if (String.isNotEmpty(dfOrderId)) {       

                orderList = [SELECT   Id, Heritage_Site__c,Induction_Required__c,Security_Required__c,Site_Notes__c,Trading_Name__c
                                        FROM   DF_Order__c
                                        WHERE  Id = :dfOrderId];                                                   
            }
            if (!orderList.isEmpty()) {
                order = orderList[0];
                order.Heritage_Site__c = heritageSite;
                order.Induction_Required__c = inductionReqd;
                order.Security_Required__c = securityReqd;
                order.Site_Notes__c = siteNotes;
                order.Trading_Name__c = tradingName;
                update order;
            }


        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        } 
 	
 }
    @TestVisible private static String nullToEmpty(String input) {
    return input == null ? '' : input;
}

	@AuraEnabled
    public static String getBundleId(String quoteId) {
        return DF_ProductController.getBundleId(quoteId);
    }


}