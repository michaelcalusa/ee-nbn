@isTest
private class DF_Order_Site_DetailsController_Test {
    
@isTest static void test1(){
            test.startTest(); 

            User dfuser =createDFCommUser();


            system.runAs(dfuser) {
                
            PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'DF_BC_Customer_Plus_Permission'];
            insert new PermissionSetAssignment(AssigneeId = dfuser.id, PermissionSetId = ps.Id );   
            system.debug('!!!!! user AccountId' + dfuser.AccountId);
            system.debug('!!!!! user ContactId' + dfuser.ContactId);


             //       PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'DF_BC_Customer_Plus_Permission'];
            //insert new PermissionSetAssignment(AssigneeId = dfuser.id, PermissionSetId = ps.Id );
            List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
            
            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';

            // Create OpptyBundle
            DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(dfuser.AccountId);
            insert opptyBundle;

            // Create Oppty
            Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
            oppty.Opportunity_Bundle__c = opptyBundle.Id;
            insert oppty;

            // Create DFQuote recs
            DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');         
            dfQuote1.Proceed_to_Pricing__c = true;
            dfQuoteList.add(dfQuote1);

            insert dfQuoteList;         
            DF_Order__c testOrder = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle.Id );
            testOrder.Heritage_Site__c = 'Yes';
            testOrder.Induction_Required__c = 'Yes';
            testOrder.Security_Required__c = 'Yes';
            testOrder.Site_Notes__c = 'Site Notes';
            testOrder.Trading_Name__c = 'Trader Joes TEST SITE';
            Insert testOrder;

            DF_Order__c  testOrder1 = [SELECT Id, Opportunity_Bundle__c,DF_Quote__c FROM DF_Order__c WHERE Trading_Name__c = 'Trader Joes TEST SITE' LIMIT 1];
            Opportunity oppt = [SELECT Id, Name From Opportunity WHERE Opportunity_Bundle__c =: testOrder1.Opportunity_Bundle__c LIMIT 1];
            DF_Quote__c quote = [SELECT Id From DF_Quote__c WHERE Opportunity__c =: oppty.Id];
            DF_Opportunity_Bundle__c ob = [SELECT id FROM DF_Opportunity_Bundle__c WHERE Id =: testOrder1.Opportunity_Bundle__c];
            
            

            String test1 = DF_Order_Site_DetailsController.loadDFOrderSiteDetails(testOrder1.Id);
            DF_Order_Site_DetailsController.setDFOrderSiteDetails(testOrder1.Id, 'testName', 'No', 'No',  'No', 'The Cat Sat on the mat');
  



            test.stopTest();  
            }

        }
    
    @isTest static void test_method_two() {
        // Implement test code
    }

    public static User createDFCommUser() {
        Profile commProfile = [SELECT Id 
                               FROM Profile 
                               WHERE Name = 'DF Partner User'];
        
        system.debug('createDFCommUser - commProfile: ' + commProfile);
        
        Account commAcct = new Account();
        commAcct.Name = 'My account';
        commAcct.Tier__c = '1';
        //commAcct.Number_of_Employees_for_ICT__c = '1';
        insert commAcct; 
        system.debug('createDFCommUser - test account inserted...: account id: ' + commAcct.Id);
        
        Contact commContact = new Contact();
        commContact.LastName ='testContact';
        commContact.AccountId = commAcct.Id;
        insert commContact;  
        system.debug('createDFCommUser - test contact inserted...: contact id: ' + commContact.Id);
        
        User commUser = new User();
        commUser.Alias = 'test123';
        commUser.Email = 'test123@noemail.com';
        commUser.Emailencodingkey = 'ISO-8859-1';
        commUser.Lastname = 'Testing';
        commUser.Languagelocalekey = 'en_US';
        commUser.Localesidkey = 'en_AU';
        commUser.Profileid = commProfile.Id;
        commUser.IsActive = true;
        commUser.ContactId = commContact.Id;
        commUser.Timezonesidkey = 'Australia/Sydney';
        commUser.Username = 'tester@noemail.com.test';
        
        insert commUser;
        system.debug('createDFCommUser - test user inserted...: user id: ' + commUser);

        
        return commUser;
    }
    

    
}