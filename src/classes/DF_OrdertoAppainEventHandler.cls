public with sharing class DF_OrdertoAppainEventHandler implements Queueable{
  
  private List<DF_Order_Event__e> lstToProcess;

  public DF_OrdertoAppainEventHandler(List<DF_Order_Event__e> evtLst) {      
      lstToProcess = evtLst;
    }

    public void execute(QueueableContext context) {

      //DF_Order_Event__e customSettingValues = Business_Platform_Events__c.getOrgDefaults();      
      DF_Order_Settings__mdt ordSetting = [SELECT OrderAckEventCode__c,OrderAckSource__c,OrderAckBatchSize__c,
                                            OrderCreateBatchSize__c,OrderCreateCode__c,OrderCreateSource__c,
                                            OrderAccBatchSize__c,OrderAccEventCode__c,OrderAccSource__c,
                                            OrderCompleteBatchSize__c,OrderCompleteEventCode__c,
                                            OrderCompleteSource__c
                                                         FROM   DF_Order_Settings__mdt
                                                         WHERE  DeveloperName = 'DFOrderSettings'];
                                                         
                                                         
      for(DF_Order_Event__e evt : lstToProcess){
        if(evt.Event_Id__c == ordSetting.OrderCreateCode__c) {
          List<String> paramList = new List<String>();
          paramList.add(evt.Event_Record_Id__c);
          system.debug('Salesforce to Appain event received for the Event ID '+evt.Event_Id__c);
          AsyncQueueableUtils.createRequests(DF_AppianOrderSubmitHandler.HANDLER_NAME, paramList);  
          //String evtBody = '{"eventIdentifier":"'+evt.Event_Id__c+'","externalSystemCorrelationId":"'+evt.Event_Record_Id__c+'","eventBody":'+evt.Message_Body__c+'}';
          //AppianCallout.SendInformationToAppian(evtBody, evt.Event_Id__c+';'+evt.Event_Record_Id__c ,null);              
            
        }                  
      }
  }
}