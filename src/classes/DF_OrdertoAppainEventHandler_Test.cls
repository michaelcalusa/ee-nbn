@isTest
public class DF_OrdertoAppainEventHandler_Test {

    @isTest
    static void testExecuteMethod()
    {
        Id oppId = DF_TestService.getQuoteRecords();
        List<Opportunity> oppList = [SELECT Id FROM OPPORTUNITY WHERE Opportunity_Bundle__c = :oppId];
        System.debug('PP oppList:'+oppList);
        Account acc = DF_TestData.createAccount('Test Account');
        Business_Platform_Events__c settings = DF_TestData.createCustomSettingsforBusinessPlatformEvents();
        insert settings;
        
        List<DF_Order_Event__e> toProcess = new List<DF_Order_Event__e>();
        //for(Integer i=0;i<21;i++){
            String msg = '{ opportunityId:' + oppId +'}';
            DF_Order_Event__e o = new DF_Order_Event__e();
            o.Event_Id__c = 'SF-DF-000002';
            o.Event_Record_Id__c = oppId;
            o.Event_Type__c = 'Desktop assesment';
            o.Source__c = 'Salesforce';
            o.Message_Body__c = msg;
            toProcess.add(o);
        //}
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, '', null));
          
        try{
            test.startTest();
            Id jobId = System.enqueueJob(new DF_OrdertoAppainEventHandler(toProcess));
            test.stopTest();
        }
        catch(Exception e){ 
        }
          
    }
}