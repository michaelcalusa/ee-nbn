public class DF_ProcessBasketQueueHandler implements Queueable {

    public List<DF_Quote__c> quoteList;
    // Constructor
    public DF_ProcessBasketQueueHandler(List<DF_Quote__c> quotesList) {    	
        // Set quote var
        quoteList = quotesList;
    }
    
    public void execute(QueueableContext context) {    	  
	    //Invoke the method for creation of Product Basket
		for(DF_Quote__c q: quoteList){
			DF_ProductController.createProductBasketQueue(JSON.serialize(q));
		}		
    }
}