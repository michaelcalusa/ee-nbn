global class DF_ProcessOrderObserverImpl implements csordcb.ObserverApi.IObserver {
    
    global DF_ProcessOrderObserverImpl() {
        system.debug('Calling DF_ProcessOrderObserverImpl constructor');
    }
    
    global void execute(csordcb.ObserverApi.Observable o, Object arg){
        csordtelcoa.OrderGenerationObservable observable = (csordtelcoa.OrderGenerationObservable) o;
        System.debug('PP observable: '+observable);
        list<Id> orderIds = observable.getOrderIds();
        System.debug('PP orderIds: '+orderIds);
        list<Id> orderRequestIds = observable.getOrderRequestIds();
        System.debug('PP orderRequestIds: '+orderRequestIds);
        Id oppRecordTypeId = DF_CS_API_Util.getDFRecordType();
        
        list<csord__Order_Line_Item__c> oliList = [
            SELECT id, name, csord__Order_Request__c, csord__Order__c, 
            csord__Order__r.csordtelcoa__Opportunity__c, csord__Order__r.csordtelcoa__Opportunity__r.ContractId, 
            csord__Order__r.csord__Identification__c, csord__Order__r.csordtelcoa__Product_Configuration__c, 
            csord__Line_Number__c, csord__Line_Description__c, Quantity__c, csord__Total_Price__c, Memo_Line__c, 
            Memo_Line_Id__c, GST__c, Solution__c, Invoice__c, Invoice_Line_Item__c, Unit_Price__c, Cost__c, Margin__c, OLI_Type__c                
            from csord__Order_Line_Item__c where csord__Order_Request__c in :orderRequestIds
        ];
        csord__Order_Line_Item__c orderLineItemObj = !oliList.isEmpty() ? oliList.get(0) : null;
        List<csord__Order__c> orderList = [SELECT csordtelcoa__Opportunity__c
                                           FROM csord__Order__c
                                           WHERE Id IN :orderIds];
        csord__Order__c orderObj = !orderList.isEmpty() ? orderList.get(0) : null;
        System.debug('PP oliList: '+oliList);
        System.debug('PP orderList: '+orderList);
        List<Opportunity> oppList = new List<Opportunity>();
        if(orderObj != null && orderLineItemObj != null){
            sObject oppObject = new Opportunity();
            String queryStr = DF_CS_API_Util.getQuery(oppObject, ' WHERE Id = '+ '\'' + orderObj.csordtelcoa__Opportunity__c + '\'');
            oppList = (List<Opportunity>)DF_CS_API_Util.getQueryRecords(queryStr); 
            Opportunity oppObj = !oppList.isEmpty() ? oppList.get(0) : null;
            
            //please include Site Survey Charge in custom settings if that is used in IF condition
            //Logic to send 1500.00 order to billing system to be developed here
            if(oppObj.RecordTypeId == oppRecordTypeId && (orderLineItemObj.Name).equalsIgnoreCase('Site Survey Charge') && oppObj.Parent_Opportunity__c != null){
                
            }
        }
        
    }
}