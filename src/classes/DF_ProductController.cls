public class DF_ProductController {
    
    public static final String FB_CATEGORY_A = 'A';
    /*
    * Includes the logic to create product basket.
    * A product basket is created for every opportunity. This product basket includes two product configurations - Product charges and Build contribution. Product charges has one related product OVC added by default     
    * Parameters : quoteObjectStr.
    * @Return : void
    */
    
    @future(callout=true)
    public static void createProductBasketQueue(String quoteObjectStr){
        DF_Quote__c quoteObject = (DF_Quote__c)System.JSON.deserialize(quoteObjectStr, DF_Quote__c.class);
        if(quoteObject != null){
            //get the opportunity id from quote
            String oppId = quoteObject.Opportunity__c;
            
            //get the technology value, csa from lapi response
            //technology value is stored in  Fibre build contribution product 
            //csa is stored in OVC product
            DF_ServiceFeasibilityResponse sfr = (DF_ServiceFeasibilityResponse)System.JSON.deserialize(quoteObject.LAPI_Response__c, DF_ServiceFeasibilityResponse.class);
            String technologyVal = sfr.derivedTechnology;
            String csa = sfr.csaId;
            String sam = sfr.samID;
            cscfga__Product_Basket__c basket;
            cscfga.API_1.ApiSession apiSession;
            
            //create api session
            apiSession = DF_CS_API_Util.createApiSession(basket);
            
            //create basket for the session and associate it with the opportunity
            basket = DF_CS_API_Util.createBasket(apiSession);
            basket.cscfga__opportunity__c = oppId;
            update basket;
            
            //add product configurations and related product to basket
            String result = createProductBasketWithDefinitions(apiSession,basket,quoteObject,technologyVal,csa,sam);
            
        }
    }
    
    
    /*
    * Includes the logic to create product basket with definition (Product charges (with a related product OVC), build contribution).
    * Parameters : apiSession, basket, quote object, technology, csa, sam.
    * @Return : String that says if the creation of products was successful
    */
    public static String createProductBasketWithDefinitions(cscfga.API_1.ApiSession apiSession, cscfga__Product_Basket__c basket, DF_Quote__c quote, String technologyValue, String csa, String sam){
        
        String result;
        apiSession = DF_CS_API_Util.createApiSession(basket);
        List<csbb__Product_Configuration_Request__c> pcrList = new List<csbb__Product_Configuration_Request__c>();
        
        DF_ServiceFeasibilityResponse sfr = (DF_ServiceFeasibilityResponse)System.JSON.deserialize(quote.LAPI_Response__c, DF_ServiceFeasibilityResponse.class);
        String fsaID = sfr.fsaID; 
        
        //fetch custom setting values for product charges, build contribution, ovc and category
        String pcOfferId = DF_CS_API_Util.getCustomSettingValue('SF_PRODUCT_CHARGE_OFFER_ID');
        String bcOfferId = DF_CS_API_Util.getCustomSettingValue('SF_BUILD_CONTRIBUTION_OFFER_ID');
        String pcDefId = DF_CS_API_Util.getCustomSettingValue('SF_PRODUCT_CHARGE_DEFINITION_ID');
        String bcDefId = DF_CS_API_Util.getCustomSettingValue('SF_BUILD_CONTRIBUTION_DEFINITION_ID');
        String ovcDefId = DF_CS_API_Util.getCustomSettingValue('SF_OVC_DEFINITION_ID');
        String categoryId = DF_CS_API_Util.getCustomSettingValue('SF_CATEGORY_ID');
        Set<Id> idList = new Set<Id>();
        
        if(String.isNotBlank(pcOfferId) && String.isNotBlank(bcOfferId) && String.isNotBlank(categoryId) && String.isNotBlank(pcDefId) && String.isNotBlank(bcDefId) && String.isNotBlank(ovcDefId)){
            
            //query ovc name
            sObject ovcObject = new cscfga__Product_Definition__c();
            String queryString = DF_CS_API_Util.getQuery(ovcObject, ' WHERE Id = '+ '\'' + ovcDefId + '\'');
            List<cscfga__Product_Definition__c> ovcList = (List<cscfga__Product_Definition__c>)DF_CS_API_Util.getQueryRecords(queryString); 
            cscfga__Product_Definition__c ovc = !ovcList.isEmpty() ? ovcList.get(0) : null;
            
            //query category value
            sObject categoryObject = new cscfga__Product_Category__c();
            String queryStrng = DF_CS_API_Util.getQuery(categoryObject, ' WHERE Id = '+ '\'' + categoryId + '\'');
            List<cscfga__Product_Category__c> categoryList = (List<cscfga__Product_Category__c>)DF_CS_API_Util.getQueryRecords(queryStrng); 
            cscfga__Product_Category__c category = !categoryList.isEmpty() ? categoryList.get(0) : null;
            
            if(ovc != null && category != null){
                // adding first config
                apiSession.setProductToConfigure(new cscfga__Product_Definition__c(Id = pcDefId), new Map<String, String> {'containerType' => 'basket', 'linkedId' => basket.Id});
                cscfga.ProductConfiguration config1 = apiSession.getRootConfiguration();
                for(cscfga.Attribute att : config1.getAttributes()) {
                    if(att.getName() == 'SAM ID'){
                        att.setValue(sam);            
                        att.setDisplayValue(sam);
                    }
                    else if(att.getName() == 'FSA'){
                        att.setValue(fsaID);            
                        att.setDisplayValue(fsaID);
                    } 
                }
                //apiSession.executeRules();
                //cscfga.ValidationResult vresult = apiSession.validateConfiguration();
                //apiSession.persistConfiguration();
                
                //adding ovc attribute definition as related product
                cscfga.ProductConfiguration relatedConfig =  apiSession.AddRelatedProduct(ovc.Name, ovcDefId);
                for(cscfga.Attribute att : relatedConfig.getAttributes()) {
                    if(att.getName() == 'CSA'){
                        att.setValue(csa);            
                        att.setDisplayValue(csa);
                    }
                }
                apiSession.executeRules();
                cscfga.ValidationResult vr = apiSession.validateConfiguration();
                apiSession.persistConfiguration();
                
                
                pcrList.add(DF_CS_API_Util.createProductConfigurationRequest(basket, config1.getSObject(), pcOfferId, category));
                idList.add(config1.getSObject().Id);
                
                // adding second config
                apiSession.setProductToConfigure(new cscfga__Product_Definition__c(Id = bcDefId), new Map<String, String> {'containerType' => 'basket', 'linkedId' => basket.Id});
                cscfga.ProductConfiguration config2 = apiSession.getRootConfiguration();
                for(cscfga.Attribute att : config2.getAttributes()) {
                    if(att.getName() == 'Access Technology'){
                        att.setValue(technologyValue);            
                        att.setDisplayValue(technologyValue);
                    }
                }
                apiSession.executeRules(); 
                cscfga.ValidationResult vr1 = apiSession.validateConfiguration();
                apiSession.persistConfiguration();
                
                
                pcrList.add(DF_CS_API_Util.createProductConfigurationRequest(basket, config2.getSObject(), bcOfferId, category));
                
                idList.add(config2.getSObject().Id);
                //revalidate configurations to get the final totals
                cscfga.ProductConfigurationBulkActions.revalidateConfigurations(idList);
                //create product configuration request for displaying in telecom sales console
                if(!pcrList.isEmpty()){
                    insert pcrList;
                    result = 'Success';
                }
                else
                    result = 'Product configuration request records do not exist';
            }
            else
                result = 'OVC product definition or category does not exist';
        }
        else
            result = 'Custom settings does not exist';
        return result;
    }
    
    /*
    * Includes the logic to get basket id.
    * Parameters : quote id.
    * @Return : String that says if the execution of functionality was successful. If so basket id is returned
    */
    @AuraEnabled
    public static String getBasketId(String quoteId) {
        String basketId;
        if(String.isNotBlank(quoteId)){
            List<DF_Quote__c> quoteList = [SELECT Id, Name, RAG__c, Location_Id__c, Address__c, Opportunity__c, LAPI_Response__c, Fibre_Build_Cost__c FROM DF_Quote__c WHERE Id = :quoteId];
            DF_Quote__c quoteObj = !quoteList.isEmpty() ? quoteList.get(0) : null;
            String oppId = quoteObj != null ? quoteObj.Opportunity__c : null;
            if(String.isNotBlank(oppId)){
                List<cscfga__Product_Basket__c> baskets = [SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :oppId];
                basketId = !baskets.isEmpty() ? baskets.get(0) != null ? baskets.get(0).Id : null : null;
                if(String.isEmpty(basketId))
                    basketId = 'Error';
            }
            else
                basketId = 'Error';
        }
        else
            basketId = 'Error';
        return basketId;
    }
    
    /*
    * Includes the logic to get the product configuration for Product charges and OVC and other DF Quote related information as a serialized string.
    * Parameters : quote id.
    * @Return : String that says if the execution of functionality was successful. If so, a serialized string is returned
    */
    @AuraEnabled
    public static String getQuickQuoteData(String quoteId) {
        String serializedResponse;
        String bcDefId = DF_CS_API_Util.getCustomSettingValue('SF_BUILD_CONTRIBUTION_DEFINITION_ID');
        if(String.isNotBlank(quoteId) && String.isNotBlank(bcDefId)){
            List<DF_Quote__c> quoteList = [SELECT Id, Name, RAG__c, Location_Id__c, Address__c, Opportunity__c,
                                            Opportunity__r.Commercial_Deal__c,Opportunity__r.Commercial_Deal__r.Deal_Module_Reference__c,Opportunity__r.Commercial_Deal__r.ETP_Applies__c,
                                            LAPI_Response__c, Fibre_Build_Cost__c 
                                            FROM DF_Quote__c WHERE Id = :quoteId];
            DF_Quote__c quoteObj = !quoteList.isEmpty() ? quoteList.get(0) : null;
            String oppId = quoteObj != null ? quoteObj.Opportunity__c : null;
            String DealDescription = (quoteObj != null && quoteObj.Opportunity__c !=null && quoteObj.Opportunity__r.Commercial_Deal__c !=null) ? quoteObj.Opportunity__r.Commercial_Deal__r.Deal_Module_Reference__c : null;
            String EtpApplies = (quoteObj != null && quoteObj.Opportunity__c !=null && quoteObj.Opportunity__r.Commercial_Deal__c !=null) ? quoteObj.Opportunity__r.Commercial_Deal__r.ETP_Applies__c : null;
            List<cscfga__Product_Basket__c> baskets = [SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :oppId];
            String basketId = !baskets.isEmpty() ? baskets.get(0) != null ? baskets.get(0).Id : null : null;
            Map<Id, cscfga__Product_Configuration__c> configs = new Map<Id, cscfga__Product_Configuration__c>([Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and cscfga__Product_Definition__c != :bcDefId]);
            if(!configs.isEmpty()){
                //fetches configurations from the product basket
                Map<String,Object> configResponse = cscfga.API_1.getProductConfigurations(new List<Id>(configs.keySet()));
                System.debug('PPPP configResponse: '+configResponse);
                if(quoteObj != null && configResponse != null && String.isNotBlank(basketId))
                    //serialize the information required for quick quote screen
                    //serializedResponse = json.serialize( new DF_QuickQuoteData(quoteObj.Name, quoteObj.Location_Id__c, quoteObj.Address__c, configResponse, basketId, quoteObj.Fibre_Build_Cost__c));
                    serializedResponse = json.serialize( new DF_QuickQuoteData(quoteObj.Name, quoteObj.Location_Id__c, quoteObj.Address__c, configResponse, basketId, quoteObj.Fibre_Build_Cost__c,DealDescription,EtpApplies));
                else
                    serializedResponse = 'Error';
            }
            else
                serializedResponse = 'Error';
        }
        else
            serializedResponse = 'Error';
        System.debug('PPPP serializedResponse: '+serializedResponse);
        return serializedResponse;
    }
    
    /*
    * Modified by Suman Gunaganti(10/10/2018)
    * Includes the logic to fetch select options values for attributes displayed in quick quote screen.
    * Parameters : basket id, boolean which says if its Product charges/OVC, picklist names list.
    * @Return : String that says if the execution of functionality was successful. If so, all picklist values are returned as a serialized string
    */
    @AuraEnabled
    public static String getOptionsList(String basketId, List<String> attNameList, List<String> ovcAttrList) {     
        
        String configId;
        String OvcconfigId;
        map<string, list<String>> attriMaptoProd = new Map<string, list<String>>();
        String pcDefId = DF_CS_API_Util.getCustomSettingValue('SF_PRODUCT_CHARGE_DEFINITION_ID');
        String ovcDefId = DF_CS_API_Util.getCustomSettingValue('SF_OVC_DEFINITION_ID');
        String optionsResponse;
        Map<String, List<cscfga__Select_Option__c>> optionsMap = new Map<String, List<cscfga__Select_Option__c>>();
        List<cscfga__Select_Option__c> optionsList;
        if(String.isNotBlank(basketId) && !attNameList.isEmpty() && String.isNotBlank(pcDefId) && String.isNotBlank(ovcDefId)){
            List<cscfga__Product_Basket__c> baskets = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c where id = :basketId];
            cscfga__Product_Basket__c basket = !baskets.isEmpty() ? baskets.get(0) : null;
            List<cscfga__Product_Configuration__c> pcConfigList = [Select id, cscfga__Product_Definition__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and (cscfga__Product_Definition__c = :pcDefId OR cscfga__Product_Definition__c = :ovcDefId)];
                for(cscfga__Product_Configuration__c pc : pcConfigList){
                    if(pc.cscfga__Product_Definition__c == pcDefId && attNameList.size() > 0){
                        configId = pc.Id;
                        attriMaptoProd.put(configId, attNameList);
                    }
                    if(pc.cscfga__Product_Definition__c == ovcDefId && ovcAttrList.size() > 0){
                        OvcconfigId = pc.Id;
                        attriMaptoProd.put(OvcconfigId, ovcAttrList);
                    }
                }
            
            if(attriMaptoProd.size() > 0){
                cscfga.API_1.ApiSession apiSession = DF_CS_API_Util.createApiSession(basket);
                for(String productId: attriMaptoProd.keySet()){
                    apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = productId, cscfga__Product_Basket__c = basketId));        
                    cscfga.ProductConfiguration currConfig = apiSession.getConfiguration();
                    for(String attName: attriMaptoProd.get(productId)){
                        cscfga.Attribute att = apiSession.getAttributeForCurrentConfig(attName);
                        //method to fetch select options values
                        optionsList = att != null ? att.getAvailableOptions() : null;
                        if(optionsList != null)
                            optionsMap.put(attName, optionsList);
                    }
                }                
                if(optionsMap != null)
                    optionsResponse = JSON.serialize(optionsMap);
                else
                    optionsResponse = 'Error';
            }
            else
                optionsResponse = 'Error';
        }
        else
            optionsResponse = 'Error';
        return optionsResponse;
    }     
    /*
    * Includes the logic to add OVC to Product charges product configuration.
    * Parameters : basket id, product configuration id to which ovc is added as a related product, csa value for the new OVC.
    * @Return : String that says if the execution of functionality was successful
    */
    @AuraEnabled
    public static String addOVC(String basketId,String configId, String csa){
        String result;
        String ovcDefId = DF_CS_API_Util.getCustomSettingValue('SF_OVC_DEFINITION_ID');
        if(String.isNotBlank(basketId) && String.isNotBlank(configId) && String.isNotBlank(ovcDefId)){
            sObject ovcObject = new cscfga__Product_Definition__c();
            String queryString = DF_CS_API_Util.getQuery(ovcObject, ' WHERE Id = '+ '\'' + ovcDefId + '\'');
            List<cscfga__Product_Definition__c> ovcList = (List<cscfga__Product_Definition__c>)DF_CS_API_Util.getQueryRecords(queryString); 
            cscfga__Product_Definition__c ovc = !ovcList.isEmpty() ? ovcList.get(0) : null;
            System.debug('PPPP ovc: '+ovc);
            
            List<cscfga__Product_Basket__c> baskets = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c where id = :basketId];
            cscfga__Product_Basket__c basket = !baskets.isEmpty() ? baskets.get(0) : null;
            System.debug('PPPP basket: '+basket);
            
            //logic to fetch product configuration and add a new related product OVC
            cscfga.API_1.ApiSession apiSession = DF_CS_API_Util.createApiSession(basket);
            apiSession.setProductToConfigure(new cscfga__Product_Definition__c(Id = ovcDefId), new Map<String, String> {'containerType' => 'basket', 'linkedId' => basketId, 'configId' => configId});
            if(ovc != null){
                cscfga.ProductConfiguration currConfig = apiSession.getRootConfiguration(); 
                cscfga.ProductConfiguration relatedConfig =  apiSession.AddRelatedProduct(ovc.Name, ovcDefId); //adding ovc attribute definition where OVC is related attribute name and ovcdefId is the definition id of new product to be added
                
                if(String.isNotBlank(csa)){
                    for(cscfga.Attribute att : relatedConfig.getAttributes()) {
                        if(att.getName() == 'CSA'){
                            att.setValue(csa);            
                            att.setDisplayValue(csa);
                        }
                    }
                }
                apiSession.executeRules();
                cscfga.ValidationResult vr = apiSession.validateConfiguration();
                apiSession.persistConfiguration();
                result = 'Success';
            }
            else
                result = 'Error';
        }
        else
            result = 'Error';
        return result;
    }
    
    /*
    * Includes the logic to remove OVC from Product charges product configuration.
    * Parameters : basket id, product configuration id from which ovc is removed, related product attribute name in parent product definition.
    * @Return : String that says if the execution of functionality was successful
    */
    @AuraEnabled
    public static String removeOVC(Id basketId, Id configId, String relatedProductAtt) {
        String result;
        if(String.isNotBlank(basketId) && String.isNotBlank(configId) && String.isNotBlank(relatedProductAtt)){
            cscfga__Product_Configuration__c config = [Select Id, cscfga__Configuration_Offer__r.cscfga__Template__c, cscfga__Product_Basket__c, cscfga__Parent_Configuration__c, cscfga__Configuration_Offer__c, cscfga__Configuration_Offer__r.Name from cscfga__Product_Configuration__c where id = :configId];
            System.debug('PPPP config: '+config);
            if(config != null){
                //method to remove related product from basket
                cscfga.ProductBasketManager.removeProductsFromBasket(new List<cscfga__Product_Configuration__c>{config}, new cscfga__Product_Basket__c(Id = basketId));
                if(String.isNotBlank(config.cscfga__Parent_Configuration__c)) {
                    cscfga__Attribute__c att = [Select id, cscfga__value__c from cscfga__attribute__c where name = :relatedProductAtt and cscfga__Product_Configuration__c = :config.cscfga__Parent_Configuration__c];
                    if(att != null){
                        removeFromRelatedProduct(att, config.Id);
                        update att;
                        Set<Id> idList = new Set<Id>();
                        idList.add(config.cscfga__Parent_Configuration__c);
                        cscfga.ProductConfigurationBulkActions.revalidateConfigurations(idList);
                        result = 'Success';
                    }
                    else
                        result = 'Error';
                }
                else
                    result = 'Error';
            }
            else
                result = 'Error';
        }
        else
            result = 'Error';   
        return result;
    }
    
    /*
    * Includes the logic to update related product attribute in the parent product configuration when a related product is removed
    * Parameters : cscfga__Attribute__c attribute and value.
    * @Return : void
    */
    @AuraEnabled
    public static void removeFromRelatedProduct(cscfga__Attribute__c att, Id value) {
        if(String.isNotBlank(att.cscfga__Value__c)) {
            att.cscfga__value__c = att.cscfga__value__c.replace(value + ',', '');
            att.cscfga__value__c = att.cscfga__value__c.replace(',' + value, '');
            att.cscfga__value__c = att.cscfga__value__c.replace(value, '');
        }       
    }
    
    /*
    * Includes the logic to update product information
    * Parameters : basket id, serialized string of configurations and attributes.
    * @Return : String that says if the execution of functionality was successful
    */
    @AuraEnabled
    public static String updateProduct(String basketId, String configs, String attributes) {
        String result;
        String serializedResponse;
        if(String.isNotBlank(basketId) && String.isNotBlank(configs) && String.isNotBlank(attributes)){
            Map<String, cscfga__Product_Configuration__c> cfgs = (Map<String, cscfga__Product_Configuration__c>)JSON.deserialize(configs, Map<String, cscfga__Product_Configuration__c>.class);
            Map<String, List<cscfga__Attribute__c>> atts = (Map<String, List<cscfga__Attribute__c>>)JSON.deserialize(attributes, Map<String, List<cscfga__Attribute__c>>.class);
            
            //update all attributes in the product and revalidate configurations
            Map<String, Object> allConfigData = new Map<String, Object>();
            allConfigData.putAll((Map<String, Object>) cfgs);
            allConfigData.putAll((Map<String, Object>) atts);
            cscfga.API_1.updateProductConfigurations(allConfigData);
            
            Set<Id> configIds = new Set<Id>();
            for(String key : cfgs.keySet()) {
                configIds.add(key);
            }
            cscfga.ProductConfigurationBulkActions.revalidateConfigurations(configIds);
            
            Boolean validationFlag = false;
            String validationMsg;
            for(cscfga__Product_Configuration__c config : cfgs.values()){
                if(config.cscfga__Configuration_Status__c != 'Requires update' || config.cscfga__Configuration_Status__c != 'Valid' || config.cscfga__Configuration_Status__c != 'Is Valid'){
                    validationFlag = true;
                    validationMsg = config.cscfga__Validation_Message__c;
                    break;
                }
            }
            if(validationFlag)
                result = validationMsg;
            else
                result = 'Success';
        }
        else
            result = 'Error';
        return result;
    }

    /*
    * Includes the logic to update Hidden_OVC_Charges as JS rules do not execute in custom UI.
    * Parameters : basket id, product configuration id, total recurring charge of all OVCs.
    * @Return : String that says if the execution of functionality was successful. If so, Recuuring charge value in the parent is returned
    */
    @AuraEnabled
    public static String updateHiddenOVCCharges(String basketId,String configId, Decimal totalOVC, Decimal TotalCoSBw) {
        String result;
        if(String.isNotBlank(basketId) && String.isNotBlank(configId) && totalOVC != null){
            cscfga__Product_Basket__c basket = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c where id = :basketId];
            cscfga.API_1.ApiSession apiSession = DF_CS_API_Util.createApiSession(basket);
            apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = configId, cscfga__Product_Basket__c = basketId));        
            cscfga.ProductConfiguration currConfig = apiSession.getConfiguration();
            
            for(cscfga.Attribute att : currConfig.getAttributes()) {
                System.debug('Attribute name is : ' + att);
                if(att.getName() == 'Hidden_OVC_Charges'){
                    att.setValue(String.valueOf(totalOVC)); 
                }
                if(att.getName() == 'Hidden_Total_OVC_BWs'){
                    system.debug('bandwith Attribute--'+TotalCoSBw);
                    att.setValue(String.valueOf(TotalCoSBw));
                }
            }
            apiSession.executeRules(); 
            apiSession.validateConfiguration();
            apiSession.persistConfiguration();
            
            //return the final recurring charge 
            for(cscfga.Attribute att : currConfig.getAttributes()) {
                System.debug('Attribute name is : ' + att);
                if(att.getName() == 'Recurring charge'){
                    result = att.getValue(); 
                }
            }
        }
        else
            result = 'Error';
        return result;   
    }
    @AuraEnabled
    public static String updateHiddenOVCCharges(String basketId,String configId, Decimal totalOVC) {
        String result;
        if(String.isNotBlank(basketId) && String.isNotBlank(configId) && totalOVC != null){
            cscfga__Product_Basket__c basket = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c where id = :basketId];
            cscfga.API_1.ApiSession apiSession = DF_CS_API_Util.createApiSession(basket);
            apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = configId, cscfga__Product_Basket__c = basketId));        
            cscfga.ProductConfiguration currConfig = apiSession.getConfiguration();
            
            for(cscfga.Attribute att : currConfig.getAttributes()) {
                System.debug('Attribute name is : ' + att);
                if(att.getName() == 'Hidden_OVC_Charges'){
                    att.setValue(String.valueOf(totalOVC)); 
                }
            }
            apiSession.executeRules(); 
            apiSession.validateConfiguration();
            apiSession.persistConfiguration();
            
            //return the final recurring charge 
            for(cscfga.Attribute att : currConfig.getAttributes()) {
                System.debug('Attribute name is : ' + att);
                if(att.getName() == 'Recurring charge'){
                    result = att.getValue(); 
                }
            }
        }
        else
            result = 'Error';
        return result;   
    }
    
    @AuraEnabled
    public static void processUpdateDFQuoteStatus(String quoteId) {     
        DF_Quote__c dfQuoteToUpdate;
        List<DF_Quote__c> dfQuotesToUpdateList;

        try {       
            if (String.isNotEmpty(quoteId)) {               
                dfQuotesToUpdateList = [SELECT Status__c
                                        FROM   DF_Quote__c
                                        WHERE  Id = :quoteId
                                        AND    Status__c <> :DF_Constants.QUOTE_STATUS_QUICK_QUOTE];                                                   
            }

            // Perform update       
            if (!dfQuotesToUpdateList.isEmpty()) {
                dfQuoteToUpdate = dfQuotesToUpdateList[0];  

                if (dfQuoteToUpdate != null) {
                    dfQuoteToUpdate.Status__c = DF_Constants.QUOTE_STATUS_QUICK_QUOTE;                                      
                    update dfQuoteToUpdate;             
                }
            }                          
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        }
    }

       @AuraEnabled   
    public static String createDF_Order(String dfQuoteId){
        return DF_SF_QuoteController.createDF_Order(dfQuoteId);
    }
    @AuraEnabled 
    public static Boolean doesCSADefaultToLocal(string cSAId){
        return DF_SF_Order_ItemController.doesCSADefaultToLocal( cSAId);
    }
    
    @AuraEnabled
    public static String getBundleId(String quoteId) {
        String opptyBundleId = '';
        List<DF_Quote__c> quote = [Select Opportunity_Bundle__c from DF_Quote__c where id=:quoteId];
        if(!quote.isEmpty()) {
            opptyBundleId = quote[0].Opportunity_Bundle__c;
        }
        return opptyBundleId;
    }
    
    @AuraEnabled
    public static Decimal getEEUserPermissionLevel(){
        return DF_SF_RAGController.getEEUserPermissionLevel();
    }
}