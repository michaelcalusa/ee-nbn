@IsTest
public class DF_ProductController_Test {

    /**
    * Tests queueable method
    */
    @isTest static void queueableMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        List<DF_Quote__c> quoteList = [SELECT Id, RAG__c, Fibre_Build_Category__c ,Opportunity_Bundle__c, Opportunity__c, LAPI_Response__c FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppId LIMIT 1];
        Test.startTest();
        Id jobId = System.enqueueJob(new DF_ProcessBasketQueueHandler(quoteList));
        Test.stopTest();
    
    }
    
    /**
    * Tests getBasketID method
    */
    @isTest static void getBasketIDMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        Test.startTest();
        List<DF_Quote__c> quoteList = [SELECT Id, Fibre_Build_Category__c ,RAG__c, Opportunity_Bundle__c, Opportunity__c, LAPI_Response__c FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppId LIMIT 1];
        String result = DF_ProductController.getBasketId(quoteList.get(0).Id);
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
    * Tests getBasketID method for error condition
    */
    @isTest static void getBasketIDMethodErrorTest()
    {
        Test.startTest();
        String result = DF_ProductController.getBasketId('');
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
    /**
    * Tests getQuickQuoteData method
    */
    @isTest static void getQuickQuoteDataMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        Test.startTest();
        List<DF_Quote__c> quoteList = [SELECT Id, RAG__c, Fibre_Build_Category__c ,Opportunity_Bundle__c, Opportunity__c, LAPI_Response__c FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppId LIMIT 1];
        String result = DF_ProductController.getQuickQuoteData(quoteList.get(0).Id);
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
    * Tests getQuickQuoteData method error condition
    */
    @isTest static void getQuickQuoteDataMethodErrorTest()
    {
        Test.startTest();
        String result = DF_ProductController.getQuickQuoteData('');
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
    /**
    * Tests getOptionsList method
    */
    @isTest static void getOptionsListMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        List<String> attNameList = new List<String>{'Term'};
        List<String> OvcAttNameList = new List<String>();
        Test.startTest();
        String result = DF_ProductController.getOptionsList(basketObj.Id, attNameList, OvcAttNameList);
        Test.stopTest();
    }
    
    /**
    * Tests getOptionsList method error condition
    */
    @isTest static void getOptionsListMethodErrorTest()
    {
        List<String> attNameList = new List<String>{'Term'};
        List<String> OvcAttNameList = new List<String>();
        Test.startTest();
        String result = DF_ProductController.getOptionsList('', attNameList, OvcAttNameList);
        Test.stopTest();
    }
    
    /**
    * Tests addOVC method
    */
    @isTest static void addOVCMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        List<cscfga__Product_Configuration__c> pcConfigList = [Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketObj.Id and name = 'Direct Fibre - Product Charges'];
        cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
        String configId = configObj != null ? configObj.Id : null;
        Test.startTest();
        String result = DF_ProductController.addOVC(basketObj.Id, configId, 'CSA400000010875');
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
    * Tests addOVC method error condition
    */
    @isTest static void addOVCMethodErrorTest()
    {
        Test.startTest();
        String result = DF_ProductController.addOVC('', '', 'CSA400000010875');
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
    /**
    * Tests removeOVC method
    */
    @isTest static void removeOVCMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        List<cscfga__Product_Configuration__c> pcConfigList = [Select id,cscfga__Configuration_Offer__r.cscfga__Template__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketObj.Id and name = 'OVC'];
        cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
        String configId = configObj != null ? configObj.Id : null;
        Test.startTest();
        String result = DF_ProductController.removeOVC(basketObj.Id, configId, 'OVC');
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
    * Tests removeOVC method error condition
    */
    @isTest static void removeOVCMethodErrorTest()
    {
        Test.startTest();
        String result = DF_ProductController.removeOVC(null, null, 'OVC');
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
    /**
    * Tests updateProduct method
    */
    @isTest static void updateProductMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Map<Id, cscfga__Product_Configuration__c> configs1 = new Map<Id, cscfga__Product_Configuration__c>([Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketObj.Id]);
        Map<String,Object> configResponse = cscfga.API_1.getProductConfigurations(new List<Id>(configs1.keySet()));
        Map<String, cscfga__Product_Configuration__c> cfgsOVC = new Map<String, cscfga__Product_Configuration__c>();
        Map<String, Object> attsOVC = new Map<String, Object>();
        for(String o : configResponse.keySet()){
            if(o.contains('-attributes'))
            {
                attsOVC.put(o, configResponse.get(o));
            }
            else
                cfgsOVC.put(o, (cscfga__Product_Configuration__c)configResponse.get(o));
        }

        String jsonStr = json.serialize(attsOVC);
        String jsonStr1 = json.serialize(cfgsOVC);
        
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        Test.startTest();
        String result = DF_ProductController.updateProduct(basketObj.Id, jsonStr1, jsonStr);
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
    * Tests updateProduct method error condition
    */
    @isTest static void updateProductMethodErrorTest()
    {
        Test.startTest();
        String result = DF_ProductController.updateProduct('', '', '');
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
    /**
    * Tests updateHiddenOVCCharges method
    */
    @isTest static void updateHiddenOVCChargesMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        List<cscfga__Product_Configuration__c> pcConfigList = [Select id,cscfga__Configuration_Offer__r.cscfga__Template__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketObj.Id and name = 'Direct Fibre - Product Charges'];
        cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
        String configId = configObj != null ? configObj.Id : null;
        Test.startTest();
        String result = DF_ProductController.updateHiddenOVCCharges(basketObj.Id, configId, 353.40, 250);
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
    * Tests updateHiddenOVCCharges method error condition
    */
    @isTest static void updateHiddenOVCChargesMethodErrorTest()
    {
        Test.startTest();
        String result = DF_ProductController.updateHiddenOVCCharges('', '', 0.00,250);
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
    /**
    * Tests DF_OVCData
    */
    @isTest static void DF_OVCDataTest()
    {
        Test.startTest();
        DF_OVCData ovcObj = new DF_OVCData('OVC',
        'NNI', 
        'Local', 
        '1000', 
        '500', 
        '10', 
        '23',
        '45',
        '4SYLB',
        'Complete');
        Test.stopTest();
    }
    
    /**
    * Tests DF_OVCData
    */
    @isTest static void DF_OVCDataEmptyTest()
    {
        Test.startTest();
        DF_OVCData ovcObj = new DF_OVCData();
        Test.stopTest();
    }
    
    /**
    * Tests DF_QuickQuoteData
    */
    @isTest static void DF_QuickQuoteDataTest()
    {
        Test.startTest();
        DF_QuickQuoteData qqObj = new DF_QuickQuoteData('EEQ-1234567891', 'LOC123456789', 'My address', null, 'Id', 375.64);
        Test.stopTest();
    }
    
    @isTest static void test_processUpdateDFQuoteStatus() {
        // Setup data                           
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');         
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;                       
        
        Test.startTest();

        DF_ProductController.processUpdateDFQuoteStatus(String.valueOf(dfQuote1.Id));
        
        Test.stopTest();
        
        // Assertions
        List<DF_Quote__c> dfQuoteVerifyList = [SELECT Id
                                               FROM   DF_Quote__c 
                                               WHERE  Status__c = :DF_Constants.QUOTE_STATUS_QUICK_QUOTE];
                                     
        system.AssertEquals(false, dfQuoteVerifyList.isEmpty());        
    }    
    
    @isTest static void getEEUserPermissionLevelTest(){
        DF_ProductController.getEEUserPermissionLevel();
    }
}