public class DF_QuickQuoteData {

    //wrapper class to display quick quote data
    @AuraEnabled
    public String QuoteId {get;set;}
    @AuraEnabled
    public String LocId {get;set;}
    @AuraEnabled
    public String Address {get;set;}
    @AuraEnabled
    public String BasketId {get;set;}
    @AuraEnabled
    public Decimal Fbc {get;set;}
    @AuraEnabled
    public Map<String,Object> ProductConfigMap {get;set;}
    @AuraEnabled
    public String DealDescription {get;set;}
    @AuraEnabled
    public String EtpApplies {get;set;}
    
    public DF_QuickQuoteData(String quoteName, String locationId, String add, Map<String,Object> configMap, String basketIdValue, Decimal fbContibution){
        QuoteId = quoteName;
        LocId = locationId;
        Address = add;
        ProductConfigMap = configMap;
        BasketId = basketIdValue;
        Fbc = fbContibution;
    }
    public DF_QuickQuoteData(String quoteName, String locationId, String add, Map<String,Object> configMap, String basketIdValue, Decimal fbContibution, String Description, String EtpAppliesFlag){
        QuoteId = quoteName;
        LocId = locationId;
        Address = add;
        ProductConfigMap = configMap;
        BasketId = basketIdValue;
        Fbc = fbContibution;
        DealDescription = Description;
        EtpApplies = EtpAppliesFlag;
    }
}