@isTest
private class DF_SFToSOAProductCharges_Test {    
    @isTest
    static void testSendProductCharges()
    {
        string orderNotifier = '{"tradingName":"BUNNINGS PTY LTD","securityClearance":"No","productOrderComprisedOf":{"referencesProductOrderItem":[{"referencesProductOrderItem":[{"itemInvolvesProduct":{"type":"UNIE","transientId":"1","tpId":"0x9100","portId":"3","ovcType":"EVPL","interfaceType":"100Base-10Base-T (Cat-3) ","interfaceBandwidth":"5 GB","id":"UNI112233445566","cosMappingMode":"PCP"},"action":"ADD"}],"itemInvolvesProduct":{"uniVLanId":null,"type":"BTD","sVLanId":null,"salesforceOvcId":null,"routeType":null,"powerSupply2":"Not Required","powerSupply1":"DC(-d8V to -22V)","poi":null,"nni":null,"maximumFrameSize":null,"id":"BTD112233445566","cosMediumBandwidth":null,"cosLowBandwidth":null,"cosHighBandwidth":null,"connectedTo":null,"btdType":"Standard (CSAS)","btdMounting":"Rack"},"action":"ADD"},{"referencesProductOrderItem":null,"itemInvolvesProduct":{"uniVLanId":"25","type":"OVC","ovcTransientID":"OVC 1","sVLanId":"21","salesforceOvcId":null,"routeType":"Local","powerSupply2":null,"powerSupply1":null,"poi":"N/A","nni":"NNI000000601417","maximumFrameSize":"Jumbo (9000 Bytes)","id":"OVC112233445588","cosMediumBandwidth":"500Mbps ","cosLowBandwidth":"500Mbps ","cosHighBandwidth":"50Mbps ","connectedTo":"1","btdType":null,"btdMounting":null},"action":"ADD"},{"referencesProductOrderItem":null,"itemInvolvesProduct":{"uniVLanId":"25","type":"OVC","ovcTransientID":"OVC 2","sVLanId":"21","salesforceOvcId":null,"routeType":"Local","powerSupply2":null,"powerSupply1":null,"poi":"N/A","nni":"NNI000000601417","maximumFrameSize":"Jumbo (9000 Bytes)","id":"OVC112233445589","cosMediumBandwidth":"500Mbps ","cosLowBandwidth":"500Mbps ","cosHighBandwidth":"50Mbps ","connectedTo":"1","btdType":null,"btdMounting":null},"action":"ADD"}],"itemInvolvesProduct":{"zone":"CBD","term":"12 Months","templateVersion":"1.0","templateId":"TPL","serviceRestorationSLA":"Enhanced-6 (24/7)","productType":"EE","id":"PRI000000000969","accessAvailabilityTarget":"99.95% (Dual Uplink/same Network Aggregation element)"},"itemInvolvesLocation":{"id":"LOC000233782131"},"action":"ADD"},"plannedCompletionDate":"2018-01-01T12:00:00Z","orderType":"Connect","notes":"Lore Ipsum","interactionStatus":"Complete","interactionDateComplete":"2018-02-01T12:00:00Z","inductionRequired":"No","id":"ORD000000000878","heritageSite":"No","customerRequiredDate":"2017-02-25T00:00:00Z","afterHoursAppointment":"Yes","accessSeekerInteraction":{"billingAccountID":"BAN000000892801"}}';
        
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        test.startTest(); 
        Account acc = DF_TestData.createAccount('Test Account');
        acc.Access_Seeker_ID__c = 'AS12345';
        acc.Billing_ID__c = 'BL12345';
        insert acc;
        
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigSiteSurvey(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        
        basketObj.cscfga__Opportunity__c = opp.Id;
        update basketObj;
        //Business_Platform_Events__c settings = DF_TestData.createCustomSettingsforBusinessPlatformEvents();
        //insert settings;
        
        List<DF_Quote__c> dfQuoteList = [SELECT Id,Name,Address__c,LAPI_Response__c,RAG__c,Location_Id__c,Fibre_Build_Cost__c,Opportunity_Bundle__c,Opportunity__c
                                         FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppid LIMIT 1];
        
        String priceItemCode = '';
        DF_Order__c ordObj = new DF_Order__c();
        ordObj.DF_Quote__c = dfQuoteList.get(0).Id;
        ordObj.Opportunity_Bundle__c = oppId;
        ordObj.Order_Notifier_Response_JSON__c = orderNotifier;
        ordObj.BPI_Id__c = 'TestPRI';
        insert ordObj;


        
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity childOpp = DF_TestData.createOpportunity('Test Child Opp');
        childOpp.Parent_Opportunity__c = dfQuoteList.get(0).Opportunity__c;
        insert childOpp;
                  
        csord__Order__c primaryOrder = new csord__Order__c(Name='so sad',csord__Identification__c='Order_a1hN0000001UNpfIAG_0');
        insert primaryOrder;
        
        csord__Order__c order = DF_TestData.csOrder(childOpp.id,acc.id);
        order.csord__Primary_Order__c = primaryOrder.id;
        insert order;
        
        csord__Order__c csOrder = new csord__Order__c();
        csOrder.csordtelcoa__Opportunity__c = dfQuoteList.get(0).Opportunity__c;
        csOrder.name = 'Direct Fibre - Product Charges';
        csOrder.csord__Primary_Order__c = primaryOrder.id;
        //csOrder.csordtelcoa__Opportunity__c = childOpp.Id;         
        csOrder.csord__Identification__c = 'Order_a1hN0000001UNpfIAG_0';
        insert csOrder;





        
        List<csord__Order_Line_Item__c> oliList = new List<csord__Order_Line_Item__c>();
        csord__Order_Line_Item__c csOLI = new csord__Order_Line_Item__c();
        csOLI.csord__Order__c = csOrder.Id;
        csOLI.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        csOLI.name = 'Fibre Build Contribution - EEAS';
        csOLI.csord__Total_Price__c = 200;
        oliList.add(csOLI);
        
        csord__Order_Line_Item__c csOLI1 = new csord__Order_Line_Item__c();
        csOLI1.csord__Order__c = csOrder.Id;
        csOLI1.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        csOLI1.name = 'Fibre Build Contribution - Commercial Deal Discount';
        csOLI1.csord__Total_Price__c = -200;
        oliList.add(csOLI1);
        
        csord__Order_Line_Item__c csOLI2 = new csord__Order_Line_Item__c();
        csOLI2.csord__Order__c = csOrder.Id;
        csOLI2.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        csOLI2.name = 'Site Survey Rebate - EEAS';
        csOLI2.csord__Total_Price__c = -200;
        oliList.add(csOLI2);
        
        insert oliList;
        
        csord__Subscription__c objSubs = new csord__Subscription__c();
        objSubs.name = 'test';
        objSubs.csord__Order__c = csOrder.Id;
        objSubs.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        insert objSubs;
        
        //        List<cscfga__Product_Configuration__c> prodConfigList = DF_CS_API_Util.getProductBasketConfigs(basketObj.Id);
        //prodConfigList[0].csordtelcoa__Replaced_Subscription__c = objSubs.Id;
        //update prodConfigList[0];

        //csord__Service__c service = new csord__Service__c();
        //service.Name = 'OVC 1';
        //service.csordtelcoa__Product_Basket__c = basketObj.Id;
        //service.csord__Subscription__c = objSubs.Id;
        ////service.csord__Order__c = csOrd.Id;
        //service.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        //service.csordtelcoa__Product_Configuration__c = prodConfigList[0].Id;
        //insert service;

        csord__Service__c objServ = new csord__Service__c();
        objServ.Name = 'OVC 1';
        objServ.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objServ.csord__Subscription__c = objSubs.Id;
        objServ.csord__Order__c = csOrder.Id;
        insert objServ;
        
        List<csord__Service_Line_Item__c> lineItemList = new List<csord__Service_Line_Item__c>();
        
        csord__Service_Line_Item__c objSLI1 = new csord__Service_Line_Item__c();
        objSLI1.name = 'Zone Charges - EEAS';
        objSLI1.csord__Is_Recurring__c = true;
        objSLI1.csord__Service__c = objServ.Id;
        objSLI1.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI1.csord__Total_Price__c = 200;
        lineItemList.add(objSLI1);
        
        csord__Service_Line_Item__c objSLI2 = new csord__Service_Line_Item__c();
        objSLI2.name = 'Zone Charges - Commercial Deal Discount';
        objSLI2.csord__Service__c = objServ.Id;
        objSLI2.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI2.csord__Total_Price__c = -200;
        lineItemList.add(objSLI2);
        
        csord__Service_Line_Item__c objSLI3 = new csord__Service_Line_Item__c();
        objSLI3.name = 'Zone Charges - Term Discount';
        objSLI3.csord__Service__c = objServ.Id;
        objSLI3.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI3.csord__Total_Price__c = -100;
        lineItemList.add(objSLI3);
        
        csord__Service_Line_Item__c objSLI4 = new csord__Service_Line_Item__c();
        objSLI4.name = 'Cos High Charges - EEAS';
        objSLI4.csord__Is_Recurring__c = true;
        objSLI4.csord__Service__c = objServ.Id;
        objSLI4.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI4.csord__Total_Price__c = 200;
        lineItemList.add(objSLI4);
        
        csord__Service_Line_Item__c objSLI5 = new csord__Service_Line_Item__c();
        objSLI5.name = 'CoS High Charges - Commercial Deal Discount';
        objSLI5.csord__Service__c = objServ.Id;
        objSLI5.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI5.csord__Total_Price__c = -200;
        lineItemList.add(objSLI5);
        
        csord__Service_Line_Item__c objSLI6 = new csord__Service_Line_Item__c();
        objSLI6.name = 'CoS High Charges - Term Discount';
        objSLI6.csord__Service__c = objServ.Id;
        objSLI6.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI6.csord__Total_Price__c = -100;
        lineItemList.add(objSLI6);
        
        csord__Service_Line_Item__c objSLI7 = new csord__Service_Line_Item__c();
        objSLI7.name = 'eSLA Charges - EEAS';
        objSLI7.csord__Is_Recurring__c = true;
        objSLI7.csord__Service__c = objServ.Id;
        objSLI7.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI7.csord__Total_Price__c = 200;
        lineItemList.add(objSLI7);
        
        csord__Service_Line_Item__c objSLI8 = new csord__Service_Line_Item__c();
        objSLI8.name = 'Cos Medium Charges - EEAS';
        objSLI8.csord__Is_Recurring__c = true;
        objSLI8.csord__Service__c = objServ.Id;
        objSLI8.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI8.csord__Total_Price__c = 200;
        lineItemList.add(objSLI8);
        
        csord__Service_Line_Item__c objSLI9 = new csord__Service_Line_Item__c();
        objSLI9.name = 'CoS Medium Charges - Commercial Deal Discount';
        objSLI9.csord__Service__c = objServ.Id;
        objSLI9.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI9.csord__Total_Price__c = -200;
        lineItemList.add(objSLI9);
        
        csord__Service_Line_Item__c objSLI10 = new csord__Service_Line_Item__c();
        objSLI10.name = 'CoS Medium Charges - Term Discount';
        objSLI10.csord__Service__c = objServ.Id;
        objSLI10.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI10.csord__Total_Price__c = -100;
        lineItemList.add(objSLI10);
        
        csord__Service_Line_Item__c objSLI11 = new csord__Service_Line_Item__c();
        objSLI11.name = 'Cos Low Charges - EEAS';
        objSLI11.csord__Is_Recurring__c = true;
        objSLI11.csord__Service__c = objServ.Id;
        objSLI11.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI11.csord__Total_Price__c = 200;
        lineItemList.add(objSLI11);
        
        csord__Service_Line_Item__c objSLI12 = new csord__Service_Line_Item__c();
        objSLI12.name = 'CoS Low Charges - Commercial Deal Discount';
        objSLI12.csord__Service__c = objServ.Id;
        objSLI12.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI12.csord__Total_Price__c = -200;
        lineItemList.add(objSLI12);
        
        csord__Service_Line_Item__c objSLI13 = new csord__Service_Line_Item__c();
        objSLI13.name = 'CoS Low Charges - Term Discount';
        objSLI13.csord__Service__c = objServ.Id;
        objSLI13.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI13.csord__Total_Price__c = -100;
        lineItemList.add(objSLI13);
        
        csord__Service_Line_Item__c objSLI14 = new csord__Service_Line_Item__c();
        objSLI14.name = 'Route Type Charges - EEAS';
        objSLI14.csord__Is_Recurring__c = true;
        objSLI14.csord__Service__c = objServ.Id;
        objSLI14.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI14.csord__Total_Price__c = 200;
        lineItemList.add(objSLI14);
        
        csord__Service_Line_Item__c objSLI15 = new csord__Service_Line_Item__c();
        objSLI15.name = 'Route Type Charges - Term Discount';
        objSLI15.csord__Service__c = objServ.Id;
        objSLI15.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI15.csord__Total_Price__c = -200;
        lineItemList.add(objSLI15);
        
        csord__Service_Line_Item__c objSLI16 = new csord__Service_Line_Item__c();
        objSLI16.name = 'Uni Setup Fee - EEAS';
        objSLI16.csord__Is_Recurring__c = false;
        objSLI16.csord__Service__c = objServ.Id;
        objSLI16.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI16.csord__Total_Price__c = 200;
        lineItemList.add(objSLI16);
        
        csord__Service_Line_Item__c objSLI17 = new csord__Service_Line_Item__c();
        objSLI17.name = 'UNI Setup Fee - Commercial Deal Discount';
        objSLI17.csord__Service__c = objServ.Id;
        objSLI17.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI17.csord__Total_Price__c = -200;
        lineItemList.add(objSLI17);
        
        
        csord__Service_Line_Item__c objSLI18 = new csord__Service_Line_Item__c();
        objSLI18.name = 'UNI Setup Fee - Term Deal Discount';
        objSLI18.csord__Service__c = objServ.Id;
        objSLI18.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI18.csord__Total_Price__c = -200;
        lineItemList.add(objSLI18);
        
        insert lineItemList;
        
        DF_SFToSOAProductCharges.updateProductCharges(ordObj.Id, orderNotifier, 'Complete', string.valueOf(DateTime.Now()));
        test.stopTest(); 
    }
      
    @isTest
    static void testSendProductCharges1()
    {
        string orderNotifier = '{"tradingName":"BUNNINGS PTY LTD","securityClearance":"No","productOrderComprisedOf":{"referencesProductOrderItem":[{"referencesProductOrderItem":[{"itemInvolvesProduct":{"type":"UNIE","transientId":"1","tpId":"0x9100","portId":"3","ovcType":"EVPL","interfaceType":"100Base-10Base-T (Cat-3) ","interfaceBandwidth":"5 GB","id":"UNI112233445566","cosMappingMode":"PCP"},"action":"ADD"}],"itemInvolvesProduct":{"uniVLanId":null,"type":"BTD","sVLanId":null,"salesforceOvcId":null,"routeType":null,"powerSupply2":"Not Required","powerSupply1":"DC(-d8V to -22V)","poi":null,"nni":null,"maximumFrameSize":null,"id":"BTD112233445566","cosMediumBandwidth":null,"cosLowBandwidth":null,"cosHighBandwidth":null,"connectedTo":null,"btdType":"Standard (CSAS)","btdMounting":"Rack"},"action":"ADD"},{"referencesProductOrderItem":null,"itemInvolvesProduct":{"uniVLanId":"25","type":"OVC","ovcTransientID":"OVC 1","sVLanId":"21","salesforceOvcId":null,"routeType":"Local","powerSupply2":null,"powerSupply1":null,"poi":"N/A","nni":"NNI000000601417","maximumFrameSize":"Jumbo (9000 Bytes)","id":"OVC112233445588","cosMediumBandwidth":"500Mbps ","cosLowBandwidth":"500Mbps ","cosHighBandwidth":"50Mbps ","connectedTo":"1","btdType":null,"btdMounting":null},"action":"ADD"},{"referencesProductOrderItem":null,"itemInvolvesProduct":{"uniVLanId":"25","type":"OVC","ovcTransientID":"OVC 2","sVLanId":"21","salesforceOvcId":null,"routeType":"Local","powerSupply2":null,"powerSupply1":null,"poi":"N/A","nni":"NNI000000601417","maximumFrameSize":"Jumbo (9000 Bytes)","id":"OVC112233445589","cosMediumBandwidth":"500Mbps ","cosLowBandwidth":"500Mbps ","cosHighBandwidth":"50Mbps ","connectedTo":"1","btdType":null,"btdMounting":null},"action":"ADD"}],"itemInvolvesProduct":{"zone":"CBD","term":"12 Months","templateVersion":"1.0","templateId":"TPL","serviceRestorationSLA":"Enhanced-6 (24/7)","productType":"EE","id":"PRI000000000969","accessAvailabilityTarget":"99.95% (Dual Uplink/same Network Aggregation element)"},"itemInvolvesLocation":{"id":"LOC000233782131"},"action":"ADD"},"plannedCompletionDate":"2018-01-01T12:00:00Z","orderType":"Connect","notes":"Lore Ipsum","interactionStatus":"Complete","interactionDateComplete":"2018-02-01T12:00:00Z","inductionRequired":"No","id":"ORD000000000878","heritageSite":"No","customerRequiredDate":"2017-02-25T00:00:00Z","afterHoursAppointment":"Yes","accessSeekerInteraction":{"billingAccountID":"BAN000000892801"}}';
        
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        test.startTest(); 
        Account acc = DF_TestData.createAccount('Test Account');
        acc.Access_Seeker_ID__c = 'AS12345';
        acc.Billing_ID__c = 'BL12345';
        insert acc;
        
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigSiteSurvey(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        
        basketObj.cscfga__Opportunity__c = opp.Id;
        update basketObj;
        //Business_Platform_Events__c settings = DF_TestData.createCustomSettingsforBusinessPlatformEvents();
        //insert settings;
        
        List<DF_Quote__c> dfQuoteList = [SELECT Id,Name,Address__c,LAPI_Response__c,RAG__c,Location_Id__c,Fibre_Build_Cost__c,Opportunity_Bundle__c,Opportunity__c
                                         FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppid LIMIT 1];
        
        String priceItemCode = '';
        DF_Order__c ordObj = new DF_Order__c();
        ordObj.DF_Quote__c = dfQuoteList.get(0).Id;
        ordObj.Opportunity_Bundle__c = oppId;
        ordObj.Order_Notifier_Response_JSON__c = orderNotifier;
        ordObj.BPI_Id__c = 'TestPRI';
        insert ordObj;
        
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity childOpp = DF_TestData.createOpportunity('Test Child Opp');
        childOpp.Parent_Opportunity__c = dfQuoteList.get(0).Opportunity__c;
        insert childOpp;
        
        csord__Order__c order = DF_TestData.csOrder(childOpp.id,acc.id);
        insert order;
        
        csord__Order__c csOrder = new csord__Order__c();
        csOrder.csordtelcoa__Opportunity__c = dfQuoteList.get(0).Opportunity__c;
        csOrder.name = 'Direct Fibre - Build Contribution';
        //csOrder.csordtelcoa__Opportunity__c = childOpp.Id;         
        csOrder.csord__Identification__c = 'Order_a1hN0000001UNpfIAG_0';
        insert csOrder;
        
        List<csord__Order_Line_Item__c> oliList = new List<csord__Order_Line_Item__c>();
        csord__Order_Line_Item__c csOLI = new csord__Order_Line_Item__c();
        csOLI.csord__Order__c = csOrder.Id;
        csOLI.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        csOLI.name = 'Fibre Build Contribution - EEAS';
        csOLI.csord__Total_Price__c = 200;
        oliList.add(csOLI);
        
        csord__Order_Line_Item__c csOLI1 = new csord__Order_Line_Item__c();
        csOLI1.csord__Order__c = csOrder.Id;
        csOLI1.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        csOLI1.name = 'Fibre Build Contribution - Commercial Deal Discount';
        csOLI1.csord__Total_Price__c = -200;
        oliList.add(csOLI1);
        
        csord__Order_Line_Item__c csOLI2 = new csord__Order_Line_Item__c();
        csOLI2.csord__Order__c = csOrder.Id;
        csOLI2.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        csOLI2.name = 'Site Survey Rebate - EEAS';
        csOLI2.csord__Total_Price__c = -200;
        oliList.add(csOLI2);
        
        insert oliList;
        
        csord__Subscription__c objSubs = new csord__Subscription__c();
        objSubs.name = 'test';
        objSubs.csord__Order__c = csOrder.Id;
        objSubs.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        insert objSubs;
        
        csord__Service__c objServ = new csord__Service__c();
        objServ.Name = 'ABC Test';
        objServ.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objServ.csord__Subscription__c = objSubs.Id;
        objServ.csord__Order__c = csOrder.Id;
        insert objServ;
        
        List<csord__Service_Line_Item__c> lineItemList = new List<csord__Service_Line_Item__c>();
        
        csord__Service_Line_Item__c objSLI1 = new csord__Service_Line_Item__c();
        objSLI1.name = 'Zone Charges - EEAS';
        objSLI1.csord__Is_Recurring__c = true;
        objSLI1.csord__Service__c = objServ.Id;
        objSLI1.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI1.csord__Total_Price__c = 200;
        lineItemList.add(objSLI1);
        
        csord__Service_Line_Item__c objSLI2 = new csord__Service_Line_Item__c();
        objSLI2.name = 'Zone Charges - Commercial Deal Discount';
        objSLI2.csord__Service__c = objServ.Id;
        objSLI2.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI2.csord__Total_Price__c = -200;
        lineItemList.add(objSLI2);
        
        /*csord__Service_Line_Item__c objSLI3 = new csord__Service_Line_Item__c();
        objSLI3.name = 'Zone Charges - Term Discount';
        objSLI3.csord__Service__c = objServ.Id;
        objSLI3.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI3.csord__Total_Price__c = -100;
        lineItemList.add(objSLI3);*/
        
        csord__Service_Line_Item__c objSLI4 = new csord__Service_Line_Item__c();
        objSLI4.name = 'Cos High Charges - EEAS';
        objSLI4.csord__Is_Recurring__c = true;
        objSLI4.csord__Service__c = objServ.Id;
        objSLI4.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI4.csord__Total_Price__c = 200;
        lineItemList.add(objSLI4);
        
        csord__Service_Line_Item__c objSLI5 = new csord__Service_Line_Item__c();
        objSLI5.name = 'CoS High Charges - Commercial Deal Discount';
        objSLI5.csord__Service__c = objServ.Id;
        objSLI5.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI5.csord__Total_Price__c = -200;
        lineItemList.add(objSLI5);
        
        /*csord__Service_Line_Item__c objSLI6 = new csord__Service_Line_Item__c();
        objSLI6.name = 'CoS High Charges - Term Discount';
        objSLI6.csord__Service__c = objServ.Id;
        objSLI6.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI6.csord__Total_Price__c = -100;
        lineItemList.add(objSLI6);*/
        
        csord__Service_Line_Item__c objSLI7 = new csord__Service_Line_Item__c();
        objSLI7.name = 'eSLA Charges - EEAS';
        objSLI7.csord__Is_Recurring__c = true;
        objSLI7.csord__Service__c = objServ.Id;
        objSLI7.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI7.csord__Total_Price__c = 200;
        lineItemList.add(objSLI7);
        
        csord__Service_Line_Item__c objSLI8 = new csord__Service_Line_Item__c();
        objSLI8.name = 'Cos Medium Charges - EEAS';
        objSLI8.csord__Is_Recurring__c = true;
        objSLI8.csord__Service__c = objServ.Id;
        objSLI8.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI8.csord__Total_Price__c = 200;
        lineItemList.add(objSLI8);
        
        csord__Service_Line_Item__c objSLI9 = new csord__Service_Line_Item__c();
        objSLI9.name = 'CoS Medium Charges - Commercial Deal Discount';
        objSLI9.csord__Service__c = objServ.Id;
        objSLI9.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI9.csord__Total_Price__c = -200;
        lineItemList.add(objSLI9);
        
        /*csord__Service_Line_Item__c objSLI10 = new csord__Service_Line_Item__c();
        objSLI10.name = 'CoS Medium Charges - Term Discount';
        objSLI10.csord__Service__c = objServ.Id;
        objSLI10.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI10.csord__Total_Price__c = -100;
        lineItemList.add(objSLI10);*/
        
        csord__Service_Line_Item__c objSLI11 = new csord__Service_Line_Item__c();
        objSLI11.name = 'Cos Low Charges - EEAS';
        objSLI11.csord__Is_Recurring__c = true;
        objSLI11.csord__Service__c = objServ.Id;
        objSLI11.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI11.csord__Total_Price__c = 200;
        lineItemList.add(objSLI11);
        
        csord__Service_Line_Item__c objSLI12 = new csord__Service_Line_Item__c();
        objSLI12.name = 'CoS Low Charges - Commercial Deal Discount';
        objSLI12.csord__Service__c = objServ.Id;
        objSLI12.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI12.csord__Total_Price__c = -200;
        lineItemList.add(objSLI12);
        
        /*csord__Service_Line_Item__c objSLI13 = new csord__Service_Line_Item__c();
        objSLI13.name = 'CoS Low Charges - Term Discount';
        objSLI13.csord__Service__c = objServ.Id;
        objSLI13.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI13.csord__Total_Price__c = -100;
        lineItemList.add(objSLI13);*/
        
        csord__Service_Line_Item__c objSLI16 = new csord__Service_Line_Item__c();
        objSLI16.name = 'Uni Setup Fee - EEAS';
        objSLI16.csord__Is_Recurring__c = false;
        objSLI16.csord__Service__c = objServ.Id;
        objSLI16.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI16.csord__Total_Price__c = 200;
        lineItemList.add(objSLI16);
        
        csord__Service_Line_Item__c objSLI17 = new csord__Service_Line_Item__c();
        objSLI17.name = 'UNI Setup Fee - Commercial Deal Discount';
        objSLI17.csord__Service__c = objServ.Id;
        objSLI17.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI17.csord__Total_Price__c = -200;
        lineItemList.add(objSLI17);
        
        insert lineItemList;
        
        DF_SFToSOAProductCharges.updateProductCharges(ordObj.Id, orderNotifier, 'Complete', string.valueOf(DateTime.Now()));
        test.stopTest(); 
    }
    
    
    @isTest
    static void testSendProductCharges_Modify()
    {
        string orderNotifier = '{"tradingName":"BUNNINGS PTY LTD","securityClearance":"No","productOrderComprisedOf":{"referencesProductOrderItem":[{"referencesProductOrderItem":[{"itemInvolvesProduct":{"type":"UNIE","transientId":"1","tpId":"0x9100","portId":"3","ovcType":"EVPL","interfaceType":"100Base-10Base-T (Cat-3) ","interfaceBandwidth":"5 GB","id":"UNI112233445566","cosMappingMode":"PCP"},"action":"ADD"}],"itemInvolvesProduct":{"uniVLanId":null,"type":"BTD","sVLanId":null,"salesforceOvcId":null,"routeType":null,"powerSupply2":"Not Required","powerSupply1":"DC(-d8V to -22V)","poi":null,"nni":null,"maximumFrameSize":null,"id":"BTD112233445566","cosMediumBandwidth":null,"cosLowBandwidth":null,"cosHighBandwidth":null,"connectedTo":null,"btdType":"Standard (CSAS)","btdMounting":"Rack"},"action":"ADD"},{"referencesProductOrderItem":null,"itemInvolvesProduct":{"uniVLanId":"25","type":"OVC","ovcTransientID":"OVC 1","sVLanId":"21","salesforceOvcId":null,"routeType":"Local","powerSupply2":null,"powerSupply1":null,"poi":"N/A","nni":"NNI000000601417","maximumFrameSize":"Jumbo (9000 Bytes)","id":"OVC112233445588","cosMediumBandwidth":"500Mbps ","cosLowBandwidth":"500Mbps ","cosHighBandwidth":"50Mbps ","connectedTo":"1","btdType":null,"btdMounting":null},"action":"ADD"},{"referencesProductOrderItem":null,"itemInvolvesProduct":{"uniVLanId":"25","type":"OVC","ovcTransientID":"OVC 2","sVLanId":"21","salesforceOvcId":null,"routeType":"Local","powerSupply2":null,"powerSupply1":null,"poi":"N/A","nni":"NNI000000601417","maximumFrameSize":"Jumbo (9000 Bytes)","id":"OVC112233445589","cosMediumBandwidth":"500Mbps ","cosLowBandwidth":"500Mbps ","cosHighBandwidth":"50Mbps ","connectedTo":"1","btdType":null,"btdMounting":null},"action":"ADD"}],"itemInvolvesProduct":{"zone":"CBD","term":"12 Months","templateVersion":"1.0","templateId":"TPL","serviceRestorationSLA":"Enhanced-6 (24/7)","productType":"EE","id":"PRI000000000969","accessAvailabilityTarget":"99.95% (Dual Uplink/same Network Aggregation element)"},"itemInvolvesLocation":{"id":"LOC000233782131"},"action":"ADD"},"plannedCompletionDate":"2018-01-01T12:00:00Z","orderType":"Connect","notes":"Lore Ipsum","interactionStatus":"Complete","interactionDateComplete":"2018-02-01T12:00:00Z","inductionRequired":"No","id":"ORD000000000878","heritageSite":"No","customerRequiredDate":"2017-02-25T00:00:00Z","afterHoursAppointment":"Yes","accessSeekerInteraction":{"billingAccountID":"BAN000000892801"}}';
        
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        test.startTest(); 
        Account acc = DF_TestData.createAccount('Test Account');
        acc.Access_Seeker_ID__c = 'AS12345';
        acc.Billing_ID__c = 'BL12345';
        insert acc;
        
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigSiteSurvey(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        
        basketObj.cscfga__Opportunity__c = opp.Id;
        update basketObj;
        //Business_Platform_Events__c settings = DF_TestData.createCustomSettingsforBusinessPlatformEvents();
        //insert settings;
        
        List<DF_Quote__c> dfQuoteList = [SELECT Id,Name,Address__c,LAPI_Response__c,RAG__c,Location_Id__c,Fibre_Build_Cost__c,Opportunity_Bundle__c,Opportunity__c
                                         FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppid LIMIT 1];
        
        String priceItemCode = '';
        DF_Order__c ordObj = new DF_Order__c();
        ordObj.DF_Quote__c = dfQuoteList.get(0).Id;
        ordObj.Opportunity_Bundle__c = oppId;
        ordObj.Order_Notifier_Response_JSON__c = orderNotifier;
        ordObj.BPI_Id__c = 'TestPRI';
        ordObj.OrderType__c = 'Modify';
        insert ordObj;
        
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity childOpp = DF_TestData.createOpportunity('Test Child Opp');
        childOpp.Parent_Opportunity__c = dfQuoteList.get(0).Opportunity__c;
        insert childOpp;
        
        csord__Order__c order = DF_TestData.csOrder(childOpp.id,acc.id);
        insert order;
        
        csord__Order__c csOrder = new csord__Order__c();
        csOrder.csordtelcoa__Opportunity__c = dfQuoteList.get(0).Opportunity__c;
        csOrder.name = 'Direct Fibre - Build Contribution';
        //csOrder.csordtelcoa__Opportunity__c = childOpp.Id;         
        csOrder.csord__Identification__c = 'Order_a1hN0000001UNpfIAG_0';
        insert csOrder;
        
        List<csord__Order_Line_Item__c> oliList = new List<csord__Order_Line_Item__c>();
        csord__Order_Line_Item__c csOLI = new csord__Order_Line_Item__c();
        csOLI.csord__Order__c = csOrder.Id;
        csOLI.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        csOLI.name = 'Fibre Build Contribution - EEAS';
        csOLI.csord__Total_Price__c = 200;
        oliList.add(csOLI);
        
        insert oliList;
        
        csord__Subscription__c objSubs = new csord__Subscription__c();
        objSubs.name = 'test';
        objSubs.csord__Order__c = csOrder.Id;
        objSubs.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        insert objSubs;
        
        csord__Service__c objServ = new csord__Service__c();
        objServ.Name = 'ABC Test';
        objServ.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objServ.csord__Subscription__c = objSubs.Id;
        objServ.csord__Order__c = csOrder.Id;
        insert objServ;
        
        List<csord__Service_Line_Item__c> lineItemList = new List<csord__Service_Line_Item__c>();
        
        csord__Service_Line_Item__c objSLI1 = new csord__Service_Line_Item__c();
        objSLI1.name = 'Zone Charges - EEAS';
        objSLI1.csord__Is_Recurring__c = true;
        objSLI1.csord__Service__c = objServ.Id;
        objSLI1.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI1.csord__Total_Price__c = 200;
        lineItemList.add(objSLI1);
        
        csord__Service_Line_Item__c objSLI2 = new csord__Service_Line_Item__c();
        objSLI2.name = DF_Constants.SERVICE_MODIFICATION_CHARGE_EEAS;
        objSLI2.csord__Service__c = objServ.Id;
        objSLI2.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        objSLI2.csord__Total_Price__c = 250;
        lineItemList.add(objSLI2);        
        
        
        insert lineItemList;
        
        DF_SFToSOAProductCharges.updateProductCharges(ordObj.Id, orderNotifier, 'Complete', string.valueOf(DateTime.Now()));
        test.stopTest(); 
    }

}