/**
 * Created by michael.calusa on 21/02/2019.
 */

public class DF_SF_BulkOrderHandler extends AsyncQueueableHandler {

    public static final String HANDLER_NAME = 'DF_SF_BulkOrderHandler';

    public DF_SF_BulkOrderHandler() {
        // No of params must be 1
        super(DF_SF_BulkOrderHandler.HANDLER_NAME, 1);
    }

    public override void executeWork(List<String> paramList) {
        // Enforce max of 1 input parameter
        if (paramList.size() == 1) {
            invokeProcess(paramList[0]);
        } else {
            throw new CustomException(AsyncQueueableUtils.ERR_INVALID_PARAMS);
        }
    }

    private void invokeProcess(String param) {
        DF_BulkOrderAPIService.addNewOVC(param);
    }
}