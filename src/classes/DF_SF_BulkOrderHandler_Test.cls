/**
 * Created by michael.calusa on 22/02/2019.
 */

@IsTest
private class DF_SF_BulkOrderHandler_Test {
    @isTest static void test_executeWork() {
        /** Setup data  **/
        User commUser;
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = DF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        // Create Product Definition
        cscfga__Product_Definition__c prodDef = DF_TestData.buildProductDefinition('Direct Fibre - Product Charges', '');
        insert prodDef;

        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig = DF_TestData.buildProductConfig(prodDef.Id);
        prodConfig.Name = 'Direct Fibre - Product Charges';
        prodConfig.cscfga__Product_Basket__c = prodBasket.Id;
        insert prodConfig;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'B');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuote1.Fibre_Build_Category__c = 'B';
        dfQuote1.QuoteType__c = 'Connect';
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;

        string orderID= DF_SF_QuoteController.createDF_Order(dfQuoteList[0].Id);

        String param = prodBasket.Id + ',' + prodConfig.Id + ',' + orderID + ',' + '1';
        List<String> paramList = new List<String>();
        paramList.add(param);
        // Set up commUser to run test as
        commUser = DF_TestData.createDFCommUser();
        system.runAs(commUser) {
            test.startTest();
            DF_SF_BulkOrderHandler handler = new DF_SF_BulkOrderHandler();
            // Invoke Call
            handler.executeWork(paramList);
            test.stopTest();
        }
        system.assert(paramList.size() > 0);
    }

    /** Data Creation **/
    @testSetup static void setup() {
        try {
            createCustomSettings();
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    @isTest static void createCustomSettings() {
        try {
            Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
            asrConfigSettings.Max_No_of_Future_Calls__c = 50;
            asrConfigSettings.Max_No_of_Parallels__c = 10;
            asrConfigSettings.Max_No_of_Retries__c = 3;
            asrConfigSettings.Max_No_of_Rows__c = 1;
            upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }
}