/**
 * Created by michael.calusa on 22/02/2019.
 */

@IsTest
private class DF_SF_BulkOrderUtils_Test {
    @IsTest
    static void testValidateRequiredFields() {
        // Setup data
        String csvContent = 'LocationId,QuoteId,Address,FibreBuildContribution,NonRecurringCharge,MonthlyCharge,AfterHoursSiteVisit,BTDType,UNIInterfaceBandwidth,UNIInterfaceTypes,UNIOVCType,UNITPID,UNITerm,UNIZone,UNIServiceRestorationSLA,UNICIR,OVC1Id,OVC1RouteRecurring,OVC1ServiceRecurring,OVC1RouteType,OVC1POIName,OVC1NNIGroupId,OVC1MaxFrameSize,OVC1SVLANId,OVC1UNIVLANId,OVC1OVCCIR,OVC1CoSHigh,OVC1CoSMedium,OVC1CoSLow,OVC1MappingMode,OVC1Status,OVC1ValidationErrors,OVC2Id,OVC2RouteRecurring,OVC2ServiceRecurring,OVC2RouteType,OVC2POIName,OVC2NNIGroupId,OVC2MaxFrameSize,OVC2SVLANId,OVC2UNIVLANId,OVC2OVCCIR,OVC2CoSHigh,OVC2CoSMedium,OVC2CoSLow,OVC2MappingMode,OVC2Status,OVC2ValidationErrors,OVC3Id,OVC3RouteRecurring,OVC3ServiceRecurring,OVC3RouteType,OVC3POIName,OVC3NNIGroupId,OVC3MaxFrameSize,OVC3SVLANId,OVC3UNIVLANId,OVC3OVCCIR,OVC3CoSHigh,OVC3CoSMedium,OVC3CoSLow,OVC3MappingMode,OVC3Status,OVC3ValidationErrors,OVC4Id,OVC4RouteRecurring,OVC4ServiceRecurring,OVC4RouteType,OVC4POIName,OVC4NNIGroupId,OVC4MaxFrameSize,OVC4SVLANId,OVC4UNIVLANId,OVC4OVCCIR,OVC4CoSHigh,OVC4CoSMedium,OVC4CoSLow,OVC4MappingMode,OVC4Status,OVC4ValidationErrors,OVC5Id,OVC5RouteRecurring,OVC5ServiceRecurring,OVC5RouteType,OVC5POIName,OVC5NNIGroupId,OVC5MaxFrameSize,OVC5SVLANId,OVC5UNIVLANId,OVC5OVCCIR,OVC5CoSHigh,OVC5CoSMedium,OVC5CoSLow,OVC5MappingMode,OVC5Status,OVC5ValidationErrors,OVC6Id,OVC6RouteRecurring,OVC6ServiceRecurring,OVC6RouteType,OVC6POIName,OVC6NNIGroupId,OVC6MaxFrameSize,OVC6SVLANId,OVC6UNIVLANId,OVC6OVCCIR,OVC6CoSHigh,OVC6CoSMedium,OVC6CoSLow,OVC6MappingMode,OVC6Status,OVC6ValidationErrors,OVC7Id,OVC7RouteRecurring,OVC7ServiceRecurring,OVC7RouteType,OVC7POIName,OVC7NNIGroupId,OVC7MaxFrameSize,OVC7SVLANId,OVC7UNIVLANId,OVC7OVCCIR,OVC7CoSHigh,OVC7CoSMedium,OVC7CoSLow,OVC7MappingMode,OVC7Status,OVC7ValidationErrors,OVC8Id,OVC8RouteRecurring,OVC8ServiceRecurring,OVC8RouteType,OVC8POIName,OVC8NNIGroupId,OVC8MaxFrameSize,OVC8SVLANId,OVC8UNIVLANId,OVC8OVCCIR,OVC8CoSHigh,OVC8CoSMedium,OVC8CoSLow,OVC8MappingMode,OVC8Status,OVC8ValidationErrors,BusinessCon1FirstName,BusinessCon1SurName,BusinessCon1Role,BusinessCon1Email,BusinessCon1StreetNumber,BusinessCon1StreetName,BusinessCon1Number,BusinessCon1StreetType,BusinessCon1Suburb,BusinessCon1State,BusinessCon1PostCode,SiteCon1FirstName,SiteCon1SurName,SiteCon1Role,SiteCon1Email,SiteCon1Number,BusinessCon2FirstName,BusinessCon2SurName,BusinessCon2Role,BusinessCon2Email,BusinessCon2Number,BusinessCon2StreetNumber,BusinessCon2StreetName,BusinessCon2Suburb,BusinessCon2StreetType,BusinessCon2State,BusinessCon2PostCode,SiteCon2FirstName,SiteCon2SurName,SiteCon2Role,SiteCon2Email,SiteCon2Number,TradingName,HeritageSite,InductionRequired,SecurityRequired,SiteNotes,CustomerRequiredDate,NTDMounting,PowerSupply1,PowerSupply2,InstallationNotes,ErrorDetails,SalesforceDFQId,SalesforceDFOId';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000000000001,EEQ-0000005004,LOT 1 56 BALLOW ST COOLANGATTA QLD 4225 Australia,,,,No,,1Gpbs,Optical (Single mode),Access EPL,0x88a8,12 months,,Premium - 12 (24/7),,,,,Local,,NNI123456789123,,5000,12245,,500,500,,,,,,,,Local,,NNI123456789123,,6,18,,300,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,fname,surname,business contact,test@email.com,12,Queen,412345678,Apartment,Melb,vi,30002,test,test,test,email@email.com,412345678,fname,surname2,businesscontact,test@email.com,12312,12,Queen,Mel,Melb,VIC,3000,test,test,test,test@email.com,412345678,test,test,No,No,,5/03/2019,Rack,AC (240V),DC (-48V),,,a6aO000000051MxIAI,';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000000000002,EEQ-0000005005,LOT 15 30 POPE ST AITKENVALE QLD 4814 Australia,,,,,,,Optical (Single mode),Access EVPL,0x88a8,12 months,,Premium - 12 (24/7),,,,,Local,,NNI123456789123,,6000,3423434,,,,,,,,,,,Local,,NNI123456789,,6,18,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,ram,kum,business contact,sdfkd,12,King,123213,Apartment,Melb,vic,3000,Kum,raj,test,tewsdks,192838,,,,,,,,,,,,,,,,,test,test,No,No,,5/09/2019,Rack,AC (240V),DC (-48V),,,a6aO000000051MyIAI,';
        Map<Id, Map<String, String>> allFieldsInCSV = new Map<Id, Map<String, String>>();
        Map<Id, Set<String>> completedFields = new Map<Id, Set<String>>();
        Map<Id, List<String>> missingReqFields = new Map<Id, List<String>>();
        Blob blobContents = Blob.valueOf(csvContent);
        String  strBase64Contents = EncodingUtil.base64Encode(blobContents);
        String base64Data = EncodingUtil.urlDecode(strBase64Contents, 'UTF-8');
        List<List<String>> csvContents = CSVFileUtil.parseCSV(EncodingUtil.base64Decode(base64Data),false);
        test.startTest();
            missingReqFields.putAll(DF_SF_BulkOrderUtils.validateRequiredFieldsInCSV(csvContents,completedFields,allFieldsInCSV));
        test.stopTest();
        system.AssertEquals(1, missingReqFields.size());
        system.AssertEquals(2, allFieldsInCSV.size());
        system.AssertEquals(1, completedFields.size());
    }

    @IsTest
    static void testProcessBulkOrderRecords() {
        // Setup data
        List<DF_Quote__c> dfQuotes = [SELECT Id from DF_Quote__c];
        System.debug('dfQuotes:' + dfQuotes);
        String csvContent = 'LocationId,QuoteId,Address,FibreBuildContribution,NonRecurringCharge,MonthlyCharge,AfterHoursSiteVisit,BTDType,UNIInterfaceBandwidth,UNIInterfaceTypes,UNIOVCType,UNITPID,UNITerm,UNIZone,UNIServiceRestorationSLA,UNICIR,OVC1Id,OVC1RouteRecurring,OVC1ServiceRecurring,OVC1RouteType,OVC1POIName,OVC1NNIGroupId,OVC1MaxFrameSize,OVC1SVLANId,OVC1UNIVLANId,OVC1OVCCIR,OVC1CoSHigh,OVC1CoSMedium,OVC1CoSLow,OVC1MappingMode,OVC1Status,OVC1ValidationErrors,OVC2Id,OVC2RouteRecurring,OVC2ServiceRecurring,OVC2RouteType,OVC2POIName,OVC2NNIGroupId,OVC2MaxFrameSize,OVC2SVLANId,OVC2UNIVLANId,OVC2OVCCIR,OVC2CoSHigh,OVC2CoSMedium,OVC2CoSLow,OVC2MappingMode,OVC2Status,OVC2ValidationErrors,OVC3Id,OVC3RouteRecurring,OVC3ServiceRecurring,OVC3RouteType,OVC3POIName,OVC3NNIGroupId,OVC3MaxFrameSize,OVC3SVLANId,OVC3UNIVLANId,OVC3OVCCIR,OVC3CoSHigh,OVC3CoSMedium,OVC3CoSLow,OVC3MappingMode,OVC3Status,OVC3ValidationErrors,OVC4Id,OVC4RouteRecurring,OVC4ServiceRecurring,OVC4RouteType,OVC4POIName,OVC4NNIGroupId,OVC4MaxFrameSize,OVC4SVLANId,OVC4UNIVLANId,OVC4OVCCIR,OVC4CoSHigh,OVC4CoSMedium,OVC4CoSLow,OVC4MappingMode,OVC4Status,OVC4ValidationErrors,OVC5Id,OVC5RouteRecurring,OVC5ServiceRecurring,OVC5RouteType,OVC5POIName,OVC5NNIGroupId,OVC5MaxFrameSize,OVC5SVLANId,OVC5UNIVLANId,OVC5OVCCIR,OVC5CoSHigh,OVC5CoSMedium,OVC5CoSLow,OVC5MappingMode,OVC5Status,OVC5ValidationErrors,OVC6Id,OVC6RouteRecurring,OVC6ServiceRecurring,OVC6RouteType,OVC6POIName,OVC6NNIGroupId,OVC6MaxFrameSize,OVC6SVLANId,OVC6UNIVLANId,OVC6OVCCIR,OVC6CoSHigh,OVC6CoSMedium,OVC6CoSLow,OVC6MappingMode,OVC6Status,OVC6ValidationErrors,OVC7Id,OVC7RouteRecurring,OVC7ServiceRecurring,OVC7RouteType,OVC7POIName,OVC7NNIGroupId,OVC7MaxFrameSize,OVC7SVLANId,OVC7UNIVLANId,OVC7OVCCIR,OVC7CoSHigh,OVC7CoSMedium,OVC7CoSLow,OVC7MappingMode,OVC7Status,OVC7ValidationErrors,OVC8Id,OVC8RouteRecurring,OVC8ServiceRecurring,OVC8RouteType,OVC8POIName,OVC8NNIGroupId,OVC8MaxFrameSize,OVC8SVLANId,OVC8UNIVLANId,OVC8OVCCIR,OVC8CoSHigh,OVC8CoSMedium,OVC8CoSLow,OVC8MappingMode,OVC8Status,OVC8ValidationErrors,BusinessCon1FirstName,BusinessCon1SurName,BusinessCon1Role,BusinessCon1Email,BusinessCon1StreetNumber,BusinessCon1StreetName,BusinessCon1Number,BusinessCon1StreetType,BusinessCon1Suburb,BusinessCon1State,BusinessCon1PostCode,SiteCon1FirstName,SiteCon1SurName,SiteCon1Role,SiteCon1Email,SiteCon1Number,BusinessCon2FirstName,BusinessCon2SurName,BusinessCon2Role,BusinessCon2Email,BusinessCon2Number,BusinessCon2StreetNumber,BusinessCon2StreetName,BusinessCon2Suburb,BusinessCon2StreetType,BusinessCon2State,BusinessCon2PostCode,SiteCon2FirstName,SiteCon2SurName,SiteCon2Role,SiteCon2Email,SiteCon2Number,TradingName,HeritageSite,InductionRequired,SecurityRequired,SiteNotes,CustomerRequiredDate,NTDMounting,PowerSupply1,PowerSupply2,InstallationNotes,ErrorDetails,SalesforceDFQId,SalesforceDFOId';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000000000001,EEQ-0000005004,LOT 1 56 BALLOW ST COOLANGATTA QLD 4225 Australia,,,,No,,1Gpbs,Optical (Single mode),Access EPL,0x88a8,12 months,,Premium - 12 (24/7),,,,,Local,,NNI123456789123,,5000,12245,,500,500,,,,,,,,Local,,NNI123456789123,,6,18,,300,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,fname,surname,business contact,test@email.com,12,Queen,412345678,Apartment,Melb,vi,30002,test,test,test,email@email.com,412345678,fname,surname2,businesscontact,test@email.com,12312,12,Queen,Mel,Melb,VIC,3000,test,test,test,test@email.com,412345678,test,test,No,No,,5/03/2019,Rack,AC (240V),DC (-48V),,,'+dfQuotes[0].Id+',';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000000000002,EEQ-0000005005,LOT 15 30 POPE ST AITKENVALE QLD 4814 Australia,,,,,,,Optical (Single mode),Access EVPL,0x88a8,12 months,,Premium - 12 (24/7),,,,,Local,,NNI123456789123,,6000,3423434,,,,,,,,,,,Local,,NNI123456789,,6,18,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,ram,kum,business contact,sdfkd,12,King,123213,Apartment,Melb,vic,3000,Kum,raj,test,tewsdks,192838,,,,,,,,,,,,,,,,,test,test,No,No,,5/09/2019,Rack,AC (240V),DC (-48V),,,'+dfQuotes[1].Id+',';
        Map<Id, Map<String, String>> allFieldsInCSV = new Map<Id, Map<String, String>>();
        Map<Id, Set<String>> completedFields = new Map<Id, Set<String>>();
        Map<Id, List<String>> missingReqFields = new Map<Id, List<String>>();
        Blob blobContents = Blob.valueOf(csvContent);
        String  strBase64Contents = EncodingUtil.base64Encode(blobContents);
        String base64Data = EncodingUtil.urlDecode(strBase64Contents, 'UTF-8');
        List<List<String>> csvContents = CSVFileUtil.parseCSV(EncodingUtil.base64Decode(base64Data),false);
        test.startTest();
        missingReqFields.putAll(DF_SF_BulkOrderUtils.validateRequiredFieldsInCSV(csvContents,completedFields,allFieldsInCSV));
        if(!completedFields.isEmpty()) {
            DF_SF_BulkOrderUtils.processBulkOrderRecords(completedFields,allFieldsInCSV,missingReqFields);
        }
        test.stopTest();
        system.AssertEquals(1, missingReqFields.size());
        system.AssertEquals(2, allFieldsInCSV.size());
        system.AssertEquals(1, completedFields.size());
    }

    @IsTest
    static void testUpdateQuoteStatusError() {
        // Setup data
        List<DF_Quote__c> dfQuotes = [SELECT Id from DF_Quote__c];
        System.debug('dfQuotes:' + dfQuotes);
        String csvContent = 'LocationId,QuoteId,Address,FibreBuildContribution,NonRecurringCharge,MonthlyCharge,AfterHoursSiteVisit,BTDType,UNIInterfaceBandwidth,UNIInterfaceTypes,UNIOVCType,UNITPID,UNITerm,UNIZone,UNIServiceRestorationSLA,UNICIR,OVC1Id,OVC1RouteRecurring,OVC1ServiceRecurring,OVC1RouteType,OVC1POIName,OVC1NNIGroupId,OVC1MaxFrameSize,OVC1SVLANId,OVC1UNIVLANId,OVC1OVCCIR,OVC1CoSHigh,OVC1CoSMedium,OVC1CoSLow,OVC1MappingMode,OVC1Status,OVC1ValidationErrors,OVC2Id,OVC2RouteRecurring,OVC2ServiceRecurring,OVC2RouteType,OVC2POIName,OVC2NNIGroupId,OVC2MaxFrameSize,OVC2SVLANId,OVC2UNIVLANId,OVC2OVCCIR,OVC2CoSHigh,OVC2CoSMedium,OVC2CoSLow,OVC2MappingMode,OVC2Status,OVC2ValidationErrors,OVC3Id,OVC3RouteRecurring,OVC3ServiceRecurring,OVC3RouteType,OVC3POIName,OVC3NNIGroupId,OVC3MaxFrameSize,OVC3SVLANId,OVC3UNIVLANId,OVC3OVCCIR,OVC3CoSHigh,OVC3CoSMedium,OVC3CoSLow,OVC3MappingMode,OVC3Status,OVC3ValidationErrors,OVC4Id,OVC4RouteRecurring,OVC4ServiceRecurring,OVC4RouteType,OVC4POIName,OVC4NNIGroupId,OVC4MaxFrameSize,OVC4SVLANId,OVC4UNIVLANId,OVC4OVCCIR,OVC4CoSHigh,OVC4CoSMedium,OVC4CoSLow,OVC4MappingMode,OVC4Status,OVC4ValidationErrors,OVC5Id,OVC5RouteRecurring,OVC5ServiceRecurring,OVC5RouteType,OVC5POIName,OVC5NNIGroupId,OVC5MaxFrameSize,OVC5SVLANId,OVC5UNIVLANId,OVC5OVCCIR,OVC5CoSHigh,OVC5CoSMedium,OVC5CoSLow,OVC5MappingMode,OVC5Status,OVC5ValidationErrors,OVC6Id,OVC6RouteRecurring,OVC6ServiceRecurring,OVC6RouteType,OVC6POIName,OVC6NNIGroupId,OVC6MaxFrameSize,OVC6SVLANId,OVC6UNIVLANId,OVC6OVCCIR,OVC6CoSHigh,OVC6CoSMedium,OVC6CoSLow,OVC6MappingMode,OVC6Status,OVC6ValidationErrors,OVC7Id,OVC7RouteRecurring,OVC7ServiceRecurring,OVC7RouteType,OVC7POIName,OVC7NNIGroupId,OVC7MaxFrameSize,OVC7SVLANId,OVC7UNIVLANId,OVC7OVCCIR,OVC7CoSHigh,OVC7CoSMedium,OVC7CoSLow,OVC7MappingMode,OVC7Status,OVC7ValidationErrors,OVC8Id,OVC8RouteRecurring,OVC8ServiceRecurring,OVC8RouteType,OVC8POIName,OVC8NNIGroupId,OVC8MaxFrameSize,OVC8SVLANId,OVC8UNIVLANId,OVC8OVCCIR,OVC8CoSHigh,OVC8CoSMedium,OVC8CoSLow,OVC8MappingMode,OVC8Status,OVC8ValidationErrors,BusinessCon1FirstName,BusinessCon1SurName,BusinessCon1Role,BusinessCon1Email,BusinessCon1StreetNumber,BusinessCon1StreetName,BusinessCon1Number,BusinessCon1StreetType,BusinessCon1Suburb,BusinessCon1State,BusinessCon1PostCode,SiteCon1FirstName,SiteCon1SurName,SiteCon1Role,SiteCon1Email,SiteCon1Number,BusinessCon2FirstName,BusinessCon2SurName,BusinessCon2Role,BusinessCon2Email,BusinessCon2Number,BusinessCon2StreetNumber,BusinessCon2StreetName,BusinessCon2Suburb,BusinessCon2StreetType,BusinessCon2State,BusinessCon2PostCode,SiteCon2FirstName,SiteCon2SurName,SiteCon2Role,SiteCon2Email,SiteCon2Number,TradingName,HeritageSite,InductionRequired,SecurityRequired,SiteNotes,CustomerRequiredDate,NTDMounting,PowerSupply1,PowerSupply2,InstallationNotes,ErrorDetails,SalesforceDFQId,SalesforceDFOId';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000000000001,EEQ-0000005004,LOT 1 56 BALLOW ST COOLANGATTA QLD 4225 Australia,,,,No,,1Gpbs,Optical (Single mode),Access EPL,0x88a8,12 months,,Premium - 12 (24/7),,,,,Local,,NNI123456789123,,5000,12245,,500,500,,,,,,,,Local,,NNI123456789123,,6,18,,300,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,fname,surname,business contact,test@email.com,12,Queen,412345678,Apartment,Melb,vi,30002,test,test,test,email@email.com,412345678,fname,surname2,businesscontact,test@email.com,12312,12,Queen,Mel,Melb,VIC,3000,test,test,test,test@email.com,412345678,test,test,No,No,,5/03/2019,Rack,AC (240V),DC (-48V),,,'+dfQuotes[0].Id+',';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000000000002,EEQ-0000005005,LOT 15 30 POPE ST AITKENVALE QLD 4814 Australia,,,,,,,Optical (Single mode),Access EVPL,0x88a8,12 months,,Premium - 12 (24/7),,,,,Local,,NNI123456789123,,6000,3423434,,,,,,,,,,,Local,,NNI123456789,,6,18,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,ram,kum,business contact,sdfkd,12,King,123213,Apartment,Melb,vic,3000,Kum,raj,test,tewsdks,192838,,,,,,,,,,,,,,,,,test,test,No,No,,5/09/2019,Rack,AC (240V),DC (-48V),,,'+dfQuotes[1].Id+',';
        Map<Id, Map<String, String>> allFieldsInCSV = new Map<Id, Map<String, String>>();
        Map<Id, Set<String>> completedFields = new Map<Id, Set<String>>();
        Map<Id, List<String>> missingReqFields = new Map<Id, List<String>>();
        Blob blobContents = Blob.valueOf(csvContent);
        String  strBase64Contents = EncodingUtil.base64Encode(blobContents);
        String base64Data = EncodingUtil.urlDecode(strBase64Contents, 'UTF-8');
        List<List<String>> csvContents = CSVFileUtil.parseCSV(EncodingUtil.base64Decode(base64Data),false);
        test.startTest();
        missingReqFields.putAll(DF_SF_BulkOrderUtils.validateRequiredFieldsInCSV(csvContents,completedFields,allFieldsInCSV));
        if(!completedFields.isEmpty()) {
            DF_SF_BulkOrderUtils.updateQuoteStatusInError(completedFields, 'System Error');
        }
        test.stopTest();
        system.AssertEquals(1, missingReqFields.size());
        system.AssertEquals(2, allFieldsInCSV.size());
        system.AssertEquals(1, completedFields.size());
    }

    @IsTest
    static void testUpdateOVCDetailsCountOne() {
        DF_Order__c[] dfOrderId = [SELECT Id FROM DF_Order__c LIMIT 1];
        List<cscfga__Product_Basket__c> basketObj = [SELECT Id From cscfga__Product_Basket__c LIMIT 1];
        List<cscfga__Product_Configuration__c> pcConfigList = [Select id from cscfga__Product_Configuration__c
        where cscfga__Product_Basket__c = :basketObj[0].Id and name = 'Direct Fibre - Product Charges'];
        cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
        String configId = configObj != null ? configObj.Id : null;
        Test.startTest();
            String param = basketObj[0].Id + ',' + configId + ',' + dfOrderId[0].Id + ',' + '1';
            DF_SF_BulkOrderUtils.updateOVCDetails(param);
            System.assert(param != null);
        test.stopTest();
    }

    @IsTest
    static void testUpdateOVCDetailsCountTwo() {
        DF_Order__c[] dfOrderId = [SELECT Id FROM DF_Order__c LIMIT 1];
        List<cscfga__Product_Basket__c> basketObj = [SELECT Id From cscfga__Product_Basket__c LIMIT 1];
        List<cscfga__Product_Configuration__c> pcConfigList = [Select id from cscfga__Product_Configuration__c
        where cscfga__Product_Basket__c = :basketObj[0].Id and name = 'Direct Fibre - Product Charges'];
        cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
        String configId = configObj != null ? configObj.Id : null;
        Test.startTest();
        String param = basketObj[0].Id + ',' + configId + ',' + dfOrderId[0].Id + ',' + '2';
        DF_SF_BulkOrderUtils.updateOVCDetails(param);
        System.assert(param != null);
        test.stopTest();
    }

    @testSetup static void createQuote() {
        // Setup data
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create sfreq
        String sfReq = '{"systemId":"AzGAVIC420702950","structuredAddressList":[],"Status":"Valid","sfSearchType":"SearchByLocationID","sfRequestId":"a6b5D000000926KQAQ","serviceType":"Fibre To The Building","samID":"2BLT-03","RAG":null,"primaryTechnology":"Copper","pniException":null,"OLT_ID":null,"OLT_Exists":null,"Longitude":"151.67177704","locLatLong":"-32.9873616, 151.67177704","LocId":"LOC000000000001","Latitude":"-32.9873616","isNoFibreJointFound":null,"id":"LOC000000000001","fsaID":"2BLT","fibreJointTypeDescription":null,"fibreJointTypeCode":"TYCO_OFDC_A4","fibreJointStatus":"INSERVICE","fibreJointLatLong":null,"fibreJointId":"3CBR-25-01-BJL-016","Distance":17,"derivedTechnology":"FTTB","csaId":"CSA200000010384","assetOwner":"NBNCO","assetLong":null,"assetLat":null,"AddressList":["HILLSIDE NURSING HOME UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290"],"AccessTechnology":null}';

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        Opportunity oppty1 = DF_TestData.createOpportunity('LOC111111111111');
        oppty1.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty1;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = DF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        cscfga__Product_Basket__c prodBasket1 = DF_TestData.buildBasket();
        prodBasket1.cscfga__Opportunity__c = oppty1.Id;
        insert prodBasket1;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'B');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuote1.Fibre_Build_Category__c = 'B';
        dfQuote1.QuoteType__c = 'Connect';
        dfQuote1.LAPI_Response__c = sfReq;
        dfQuoteList.add(dfQuote1);

        DF_Quote__c dfQuote2 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty1.Id, opptyBundle.Id, 1000, 'B');
        dfQuote2.Proceed_to_Pricing__c = true;
        dfQuote2.Fibre_Build_Category__c = 'B';
        dfQuote2.QuoteType__c = 'Connect';
        dfQuote2.LAPI_Response__c = sfReq;
        dfQuoteList.add(dfQuote2);
        insert dfQuoteList;

        cscfga__Product_Category__c  pc = DF_TestData.buildProductCategory();
        insert pc;

        String pdtName = 'Direct Fibre - Product Charges';
        String ovcName = 'OVC 1';
        cscfga__Product_Definition__c prodDef = DF_TestData.buildProductDefinition(pdtName, '');
        prodDef.cscfga__Active__c = true;
        insert prodDef;

        cscfga__Product_Definition__c ovcDef = DF_TestData.buildProductDefinition('OVC', '');
        ovcDef.cscfga__Active__c = true;
        insert ovcDef;

        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig = DF_TestData.buildProductConfig(prodDef.Id);
        prodConfig.Name = pdtName;
        prodConfig.cscfga__Product_Basket__c = prodBasket.Id;
        insert prodConfig;

        cscfga__Product_Configuration__c prodConfigOvc = DF_TestData.buildProductConfig(ovcDef.Id);
        prodConfigOvc.Name = ovcName;
        prodConfigOvc.cscfga__Product_Basket__c = prodBasket.Id;
        prodConfigOvc.cscfga__Parent_Configuration__c = prodConfig.Id;
        insert prodConfigOvc;

        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig1 = DF_TestData.buildProductConfig(prodDef.Id);
        prodConfig1.Name = pdtName;
        prodConfig1.cscfga__Product_Basket__c = prodBasket1.Id;
        insert prodConfig1;

        cscfga__Configuration_Offer__c  offer = DF_TestData.createOffer(pdtName);
        insert offer;

        DF_Order__c testOrder1 = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle.Id );
        testOrder1.Heritage_Site__c = 'Yes';
        testOrder1.Induction_Required__c = 'Yes';
        testOrder1.Security_Required__c = 'Yes';
        testOrder1.Site_Notes__c = 'Site Notes';
        testOrder1.Trading_Name__c = 'Trader Joes TEST SITE';
        testOrder1.OrderType__c = 'Connect';
        string ovcNonBill = '[{"OVCId":"'+prodConfigOvc.Id+'","CSA":"CSA180000002222","NNIGroupId":"NNI090000015859","routeType":"Local","coSHighBandwidth":"50","coSMediumBandwidth":"100","coSLowBandwidth":"60","routeRecurring":"0.00","coSRecurring":"0.00","ovcMaxFrameSize":"Jumbo (9000 Bytes)","status":"Incomplete","sTag":"0","ceVlanId":"3421","mappingMode":"DSCP"}]';
        testOrder1.OVC_NonBillable__c = ovcNonBill;
        Insert testOrder1;

        DF_AttributeTest testName1 = new DF_AttributeTest('CSA', 'CSA400000010875', false, null);
        DF_AttributeTest testName2 = new DF_AttributeTest('Cos High', '300', false, null);
        DF_AttributeTest testName3 = new DF_AttributeTest('Cos Medium', '100', false, null);
        DF_AttributeTest testName4 = new DF_AttributeTest('Cos Low', '200', false, null);
        DF_AttributeTest testName5 = new DF_AttributeTest('OVC', 'a1eN0000000iClM', false, null);
        DF_AttributeTest testName6 = new DF_AttributeTest('Route Type', 'Local', false, null);

        List<DF_AttributeTest> attributeTests1 = new List<DF_AttributeTest>{testName1,testName4,testName2,testName3,testName5,testName6};

        List<cscfga__Attribute_Definition__c> attDefList =  new List<cscfga__Attribute_Definition__c>();
        String dt;
        for(DF_AttributeTest attTest : attributeTests1) {
            if(attTest.Name =='CSA' || attTest.Name =='OVC' || attTest.Name =='Route Type')
                dt = 'String';
            else if(attTest.Name =='Cos High' || attTest.Name =='Cos Medium' || attTest.Name =='Cos Low')
                dt = 'Decimal';
            cscfga__Attribute_Definition__c attDef = DF_TestData.buildAttributeDefinitionQQ(attTest.name, ovcDef.id,dt);
            attDefList.add(attDef);
        }
        insert attDefList;

        List<cscfga__Attribute__c> attsList = new List<cscfga__Attribute__c>();
        for(DF_AttributeTest attTest : attributeTests1) {
            cscfga__Attribute__c att = DF_TestData.buildAttribute(attTest.name,attTest.value,prodConfigOvc.Id);
            att.cscfga__is_active__c = true;
            att.cscfga__Is_Line_Item__c = true;
            attsList.add(att);
        }
        insert attsList;

        List<DF_Custom_Options__c> csListProd = new List<DF_Custom_Options__c>();
        List<DF_Custom_Options__c> csListOvc = new List<DF_Custom_Options__c>();
        if(pdtName.equalsIgnoreCase('Direct Fibre - Product Charges')){
            csListProd = DF_TestData.createCustomSettingsforProdCharge(pc.ID,offer.Id,prodDef.Id);
        }
        if(ovcName.equalsIgnoreCase('OVC 1')){
            csListOvc = DF_TestData.createCustomSettingsforOVC(pc.ID,offer.Id,ovcDef.Id);
        }
        if(!csListProd.isEmpty())
            insert csListProd;
        if(!csListOvc.isEmpty())
            insert csListOvc;
    }
}