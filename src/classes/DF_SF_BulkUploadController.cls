public class DF_SF_BulkUploadController {
    //Class that creates an opportunity bundle and attaches the csv file to that record. It also processes the contents of the csv file uploaded
    //name for the csv file to be attached
    public static final String uploadFileName = 'Bulk Upload.csv';
    
    @AuraEnabled
    public static Id saveChunk(String fileName, String base64Data, String contentType, String fileId) {
        // Create an opportunity bundle record whose id is returned after processing  
        Id parentId = DF_SF_CaptureController.createOpptyBundle();
        Id attachmentId;
        System.debug('PP base64Data:'+base64Data);
        // check if fileId id ''(Always blank in first chunk), then call the saveTheFile method for larger files call appendtofile in future
        if(String.isNotEmpty(parentId)){
            if (fileId == '') {
                fileId = saveTheFile(parentId, fileName, base64Data, contentType);
            } 
            attachmentId = Id.valueOf(fileId);
            if(String.isNotEmpty(attachmentId)){
                DF_SF_BulkUploadUtils.processBulkUploadFile(parentId);
            }
        }
        return parentId;
    }
 
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
 
        Attachment oAttachment = new Attachment();
        oAttachment.parentId = parentId;
 
        oAttachment.Body = EncodingUtil.base64Decode(base64Data);
        oAttachment.Name = uploadFileName;
        oAttachment.ContentType = contentType;
 
        insert oAttachment;
 
        return oAttachment.Id;
    }

    @AuraEnabled
    public static String downloadAttachment(){
        
        String BULK_UPLOAD_TEMPLATE = DF_CS_API_Util.getCustomSettingValue('BULK_UPLOAD_TEMPLATE');
        return BULK_UPLOAD_TEMPLATE;
    }
}