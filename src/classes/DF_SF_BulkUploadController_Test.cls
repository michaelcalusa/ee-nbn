@IsTest
public class DF_SF_BulkUploadController_Test {

     /**
	* Tests saveChunk method
	*/
    @isTest static void saveChunkTest()
    {
        String base64Data = 'U2VhcmNoVHlwZSxMb2NhdGlvbklkLExhdGl0dWRlLExvbmdpdHVkZSxVbml0TnVtYmVyLFVuaXRUeXBlLFN0cmVldG9yTG90TnVtYmVyLFN0cmVldE5hbWUsU3RyZWV0VHlwZSxTdWJ1cmJvckxvY2FsaXR5LFN0YXRlLFBvc3Rjb2RlDQpTZWFyY2hCeUxvY2F0aW9uSUQsTE9DMDAwMDAyNjk2NzI0LCwsLCwsLCwsLA0KU2VhcmNoQnlMYXRMb25nLCwtNDIuNzg5MzEsMTQ3LjA1NTc4LCwsLCwsLCwNClNlYXJjaEJ5QWRkcmVzcywsLCwxLFVOSVQsMTEsUEFDSUZJQyxXQVksQkVMRE9OLFdBLDYwMjc';
        String contentType = 'application/vnd.ms-excel';
        User commUser;
        commUser = DF_TestData.createDFCommUser();
        system.runAs(commUser) {
            Test.startTest();
            Id oppBundleId = DF_SF_BulkUploadController.saveChunk('', base64Data, contentType, '');
            System.assertNotEquals('', oppBundleId);
        	Test.stopTest();
        }
    }
    
    /**
	* Tests downloadAttachment method
	*/
    @isTest static void downloadAttachmentTest()
    {
        List<DF_Custom_Options__c> csList = DF_TestData.createCustomSettings();
        insert csList;
        Test.startTest();
        String baseUrl = DF_SF_BulkUploadController.downloadAttachment();
        System.assertNotEquals('', baseUrl);
        Test.stopTest();
    }
}