public class DF_SF_BulkUploadUtils {

    public static final String ATTACHMNT_NAME = 'Bulk Upload.csv';
    public static final String ERR_TOO_MANY_FILE_ATTACHMNTS_FOUND = 'Too many CSV file attachments found. ';
    public static final String ERR_FILE_ATTACHMNT_NOT_FOUND = 'CSV file attachment was not found. ';
    public static final String ERR_UNABLE_TO_DETERMINE_SEARCH_TYPE = 'Unable to determine search type. ';
    public static final String OVER_SIZE_LOCID_PREFIX = 'LOC...';  

    public static void processBulkUploadFile(Id opptyBundleId) { 
    	system.debug('opptyBundleId: ' + opptyBundleId);
    	   	
        final String ENCODING_SCHEME = 'ISO-8859-1';
		final String NEWLINE = '\n';
        final String FIELD_DELIMITER = ',';

        Blob fileContentBlob; // Store attachment file content (blob)
        String fileContentStr; // Store attachment file blob data as a string
        String[] csvFileRowsArray = new String[]{}; // Store array of each CSV row
    
        List<DF_SF_Request__c> sfReqToInsertList = new List<DF_SF_Request__c>(); // List of sfReqs to insert in bulk        
		List<String> searchByLocationHandlerParamList = new List<String>();
		List<String> searchByAddressHandlerParamList = new List<String>();
		List<String> searchByLatLongHandlerParamList = new List<String>();		

        try {
            // Get attachment file content (blob)
            fileContentBlob = getAttachmentFile(opptyBundleId);
            
            // Convert the uploaded file content from BLOB format into a string
            fileContentStr = blobToString(fileContentBlob, ENCODING_SCHEME);
            
            // Seperate every row of the excel file
            csvFileRowsArray = fileContentStr.split(NEWLINE);
            
            system.debug('csvFileRowsArray.size: ' + csvFileRowsArray.size());
            
            Integer validCount = 0;
            
            // Start from second line (skip header)
            for (Integer i = 1; i < csvFileRowsArray.size(); i++) {            	  
            	system.debug('CSV row no ' + i + ' of ' + (csvFileRowsArray.size() - 1));
            	              
				List<String> addressList = new List<String>();                

                // Get csv file row
                String csvFileRow = csvFileRowsArray[i];

				// Strip string off all newline chars
				csvFileRow = csvFileRow.replaceAll('(\\r|\\n)+', '');

                List<String> csvFileRowValuesList = new List<String>();
                // Split into seperate fields
                csvFileRowValuesList = csvFileRow.split(FIELD_DELIMITER); 
                                   
                DF_SF_Request__c sfReq = new DF_SF_Request__c();
                sfReq.Opportunity_Bundle__c = opptyBundleId;

				String locationId;
				String latitude;
				String longitude;
				String unitType;
				String unitNumber;
				String streetLotNumber;
				String streetName;
				String streetType;
				String suburbLocality;
				String state;
				String postcode;
                		String level;

				// Get all fields
				for (Integer x = 0; x < csvFileRowValuesList.size(); x++) {					
					if (x == 0) {
						locationId = csvFileRowValuesList[0];
					} else if (x == 1) {
						latitude = csvFileRowValuesList[1];
					} else if (x == 2) {
						longitude = csvFileRowValuesList[2];
					} else if (x == 3) {
						unitType = csvFileRowValuesList[3];
					} else if (x == 4) {
						level = csvFileRowValuesList[4];
					} else if (x == 5) {
						unitNumber = csvFileRowValuesList[5];
					} else if (x == 6) {
						streetLotNumber = csvFileRowValuesList[6];
					} else if (x == 7) {
						streetName = csvFileRowValuesList[7];
					} else if (x == 8) {
						streetType = csvFileRowValuesList[8];
					} else if (x == 9) {
						suburbLocality = csvFileRowValuesList[9];
					} else if (x == 10) {
						state = csvFileRowValuesList[10];
					} else if (x == 11) {
						postcode = csvFileRowValuesList[11];
					}
				}				

				// Check all fields to determine the search type		
				String searchType = deriveSearchType(locationId, latitude, longitude, streetLotNumber, streetName, suburbLocality, state, postcode);
				system.debug('searchType: ' + searchType);

				String status;

				if (String.isNotEmpty(searchType)) {					
	                // Set default status for sfReq and sfReq.Response
	                status = DF_LAPI_APIServiceUtils.STATUS_PENDING;
	                sfReq.Status__c = status;
	
					Boolean validSearchRequest;
	
	                // Search By Location ID
	                if (searchType.equalsIgnoreCase(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID)) {		                	
	                	sfReq.Search_Type__c = searchType;
	
						// Perform validation of user inputs
						validSearchRequest = validateLocationIdSearchInput(locationId);
	
						if (validSearchRequest) {							
							validCount++;
						} else {														
							// Set status to 'Invalid'
							status = DF_LAPI_APIServiceUtils.STATUS_INVALID_LOCID;
							
							// Set sfReq.Status to 'Completed'
							sfReq.Status__c = DF_LAPI_APIServiceUtils.STATUS_COMPLETED;																			
						}					
	                	
	                    locationId = getValidSizedLocationId(locationId);
	                    sfReq.Location_Id__c = locationId;
	                    
						// Flush all other csv input vars
						latitude = null;
						longitude = null;
						unitType = null;
                        			level = null;
						unitNumber = null;
						streetLotNumber = null;
						streetName = null;
						streetType = null;
						suburbLocality = null;
						state = null;
						postcode = null;					
						                    
	                // Search By LatLong    
	                } else if (searchType.equalsIgnoreCase(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG)) {	                		
	                	sfReq.Search_Type__c = searchType;
	
						// Perform validation of user inputs
						validSearchRequest = validateLatLongSearchInput(latitude, longitude);
						system.debug('validSearchRequest: ' + validSearchRequest);
	
						if (validSearchRequest) {							
							validCount++;
						} else {														
							// Set status to 'Invalid'
							status = DF_LAPI_APIServiceUtils.STATUS_INVALID_LATLONG;	
							
							// Set sfReq.Status to 'Completed'
							sfReq.Status__c = DF_LAPI_APIServiceUtils.STATUS_COMPLETED;												
						}

						latitude = getValidSizedLatitude(latitude);
						sfReq.Latitude__c = latitude;

	                    longitude = getValidSizedLongitude(longitude);
	                    sfReq.Longitude__c = longitude;
	
						// Flush all other csv input vars
						locationId = null;
						unitType = null;
                        			level = null;
						unitNumber = null;
						streetLotNumber = null;
						streetName = null;
						streetType = null;
						suburbLocality = null;
						state = null;
						postcode = null;	                    
	                    
	                // Search By Address                    
	                } else if (searchType.equalsIgnoreCase(DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS)) {		                	
	                	sfReq.Search_Type__c = searchType;
	
						// Perform validation of user inputs
						validSearchRequest = validateAddressSearchInput(unitType, unitNumber, streetLotNumber, streetName, streetType, suburbLocality, state, postcode);
						system.debug('validSearchRequest: ' + validSearchRequest);
	
						if (validSearchRequest) {														
							validCount++;
						} else {														
							// Set status to 'Invalid'
							status = DF_LAPI_APIServiceUtils.STATUS_INVALID_ADDRESS;
							
							// Set sfReq.Status to 'Completed'
							sfReq.Status__c = DF_LAPI_APIServiceUtils.STATUS_COMPLETED;						
						}
	                	                                      
	                    // Update sfReq field inputs                                
	                    sfReq.Unit_Type__c = getUnitTypeAPIName(unitType);
	                    sfReq.Unit_Number__c = unitNumber;
                            sfReq.Level__c = level;
	                    sfReq.Street_or_Lot_Number__c = streetLotNumber;
	                    sfReq.Street_Name__c = streetName;
	                    sfReq.Street_Type__c = getStreetTypeAPIName(streetType);
	                    sfReq.Suburb_Locality__c = suburbLocality;
	                    sfReq.State__c = getStateAPIName(state);
	                    sfReq.Postcode__c = postcode;
	
	                    // Vars to populate DTO Response values
	                    String address = DF_LAPI_APIServiceUtils.getInputAddressFullText(sfReq);                   
	
	                    if (String.isNotEmpty(address)) {          	                       
	                        addressList.add(address);
	                    }
	                    
						// Flush all other csv input vars
						locationId = null;
						latitude = null;
						longitude = null;
	                }		
				} else {					
					// No Search Type determined					
					// Set status to 'Invalid'
					status = DF_LAPI_APIServiceUtils.STATUS_INVALID_UNKNOWN;
					
					// Set sfReq.Status to 'Completed'
					sfReq.Status__c = DF_LAPI_APIServiceUtils.STATUS_COMPLETED;											
				}	

				// Always create sfReq				
                // Generate a default sfReq.Response
                DF_ServiceFeasibilityResponse sfResponse = new DF_ServiceFeasibilityResponse(locationId, latitude, longitude, addressList, null, null, status, null, null, null, null, null, null, null);   
                sfResponse.level = level;
                String dfSFRespDTO = JSON.serialize(sfResponse); 
    
                // Update sfReq.Response
                sfReq.Response__c = dfSFRespDTO;    
                
                // Add to list of sfreq recs for bulk update later
                sfReqToInsertList.add(sfReq);	                
            }
			
			system.debug('sfReqToInsertList.size:' + sfReqToInsertList.size());

            if (!sfReqToInsertList.isEmpty()) {            	            	            	
                insert sfReqToInsertList;

	            // Only kick off jobs if there were valid csv records
	            if (validCount > 0) {				
	            	DF_Order_Settings__mdt settings  = EE_CISLocationAPIUtils.getDfOrderSettings();
					String apiToUse = settings.Location_API__c;
					if (DF_Constants.API_NAME_LAPI.equalsIgnoreCase(apiToUse)) {	
					// Loop thru and get all sfReq ids
					for (DF_SF_Request__c sfReq : sfReqToInsertList) {	
		                // Add to handler specific params list for ASR processing	
						String searchType = sfReq.Search_Type__c;
						system.debug('searchType: ' + searchType);		
						
						if(searchType!=null) {
				 			if (searchType.equalsIgnoreCase(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID)) { 				
				 				searchByLocationHandlerParamList.add(sfReq.Id);
				 			} else if (searchType.equalsIgnoreCase(DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS)) {
								searchByAddressHandlerParamList.add(sfReq.Id);
				 			} else if (searchType.equalsIgnoreCase(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG)) {				
								searchByLatLongHandlerParamList.add(sfReq.Id);					
				 			} else {
				 			}	
						}							
					}	
		   
					system.debug('searchByLocationHandlerParamList.size: ' + searchByLocationHandlerParamList.size());
	
		            if (!searchByLocationHandlerParamList.isEmpty()) {                                    
		                AsyncQueueableUtils.createRequests(DF_LAPISearchByLocationHandler.HANDLER_NAME, searchByLocationHandlerParamList);                                
		            }	   
		            
		            system.debug('searchByAddressHandlerParamList.size: ' + searchByAddressHandlerParamList.size());
	
		            if (!searchByAddressHandlerParamList.isEmpty()) {                                    
		                AsyncQueueableUtils.createRequests(DF_LAPISearchByAddressHandler.HANDLER_NAME, searchByAddressHandlerParamList);                                
		            }		
		            
		            system.debug('searchByLatLongHandlerParamList.size: ' + searchByLatLongHandlerParamList.size());
	
		            if (!searchByLatLongHandlerParamList.isEmpty()) {                                    
		                AsyncQueueableUtils.createRequests(DF_LAPISearchByLatLongHandler.HANDLER_NAME, searchByLatLongHandlerParamList);                                
		            }
		            } else {
		            	System.debug('Create Async bulk request >>> ');
		            	if(sfReqToInsertList != null && !sfReqToInsertList.isEmpty()) {
		            		Set<String> sfReqs = new Set<String>();
		            		for(DF_SF_Request__c sfReq : sfReqToInsertList) {
		            			sfReqs.add(sfReq.Id);
		            		}
		            		List<String> params = EE_CISLocationAPIUtils.getParamsInBulk(sfReqs, EE_CISLocationAPIUtils.API_CIS);
		            		for(String param : params) {
		            			AsyncQueueableUtils.createRequests(EE_CISLocationSearchHandler.HANDLER_NAME, new List<String>{param});
		            		}
		            	}
		            }
	            }
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    @TestVisible private static String blobToString(Blob input, String inCharset) {     
        String hex = EncodingUtil.convertToHex(input);
        System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        
        String blobAsString;

        try {
            for (Integer i = 0; i < bytesCount; ++i) {
                bytes[i] = hex.mid(i << 1, 2);
            }

            blobAsString = EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return blobAsString;
    }

    @TestVisible private static Blob getAttachmentFile(String opptyBundleId) {
        Blob attachmentBlobFile;
        List<Attachment> attachmentList;
        Attachment attachmt;
        
        try {
            attachmentList = [SELECT Body 
                              FROM   Attachment 
                              WHERE  ParentId = :opptyBundleId
                              AND    Name = :DF_SF_BulkUploadUtils.ATTACHMNT_NAME];

            if (!attachmentList.isEmpty()) {                
                if (attachmentList.size() == 1) {                   
                    attachmt = attachmentList[0];
                    attachmentBlobFile = attachmt.Body;
                } else {
                    throw new CustomException(DF_SF_BulkUploadUtils.ERR_TOO_MANY_FILE_ATTACHMNTS_FOUND);
                }
            } else {
                throw new CustomException(DF_SF_BulkUploadUtils.ERR_FILE_ATTACHMNT_NOT_FOUND);
            }
                
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return attachmentBlobFile;
    }
    
    @TestVisible private static String deriveSearchType(String locationId, String latitude, String longitude,  
    													String streetLotNumber, String streetName, String suburbLocality, 
    													String state, String postcode) {			
		String searchType;
        
        try {
 			if (String.isNotEmpty(locationId)) { 				
 				searchType = DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID;
 			} else if (String.isNotEmpty(streetLotNumber) && String.isNotEmpty(streetName) && String.isNotEmpty(suburbLocality) 
 							|| (String.isNotEmpty(state) || String.isNotEmpty(postcode)) ) {					
 				searchType = DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS;
 			} else if (String.isNotEmpty(latitude) && String.isNotEmpty(longitude)) { 				
 				searchType = DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG;
 			} else {
 			}
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return searchType;
    }
    
    @TestVisible private static Boolean validateLocationIdSearchInput(String locationId) {    													
        final String LOC_PREFIX = 'LOC';
		Boolean isValid = true;
        
        try {        	       	
	        if (locationId.length() != 15) {	        	
				isValid = false;
	        } else {	        	
	            // Get prefix from input - search for LOC prefix
	            String locPrefix = locationId.left(3);

	            if (!locationId.startsWithIgnoreCase(LOC_PREFIX)) {	            	
	            	isValid = false;
	            } else {	            	               
	                // Get last 12 chars of LOCID
	                String locDigits = locationId.right(12);
	                
	                // Validate if last 12 chars of LOCID are numeric
	                if (!locDigits.isNumeric()) {	                	                   
	                    isValid = false;                 
	                }           
	            }            
	        }
 			
 			system.debug('isValid: ' + isValid);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return isValid;
    }

    @TestVisible private static Boolean validateLatLongSearchInput(String latitude, String longitude) {    	
		Boolean isValid = true;

        try {        	
			if (!validateLatitude(latitude) || !validateLongitude(longitude)) {
				isValid = false;
			}

 			system.debug('isValid: ' + isValid);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return isValid;
    }

    @TestVisible private static Boolean validateLatitude(String latitude) {        															
		Boolean isValid = true;

        try {        	        		
			if (!isNumeric(latitude)) {						
				isValid = false;
			} else {				
				Decimal latitudeDec = Decimal.valueOf(latitude);						
				
				// Validate latitude must be between -90 and 90
				if (latitudeDec < -90 || latitudeDec > 90) {
					isValid = false;
				}
			}

 			system.debug('isValid: ' + isValid);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return isValid;
    }

    @TestVisible private static Boolean validateLongitude(String longitude) {      	  														
		Boolean isValid = true;

        try {        	
			if (!isNumeric(longitude)) {				
				isValid = false;
			} else {				
				Decimal longitudeDec = Decimal.valueOf(longitude);						
				
				// Validate longitude must be between -180 and 180
				if (longitudeDec < -180 || longitudeDec > 180) {
					isValid = false;
				}
			}			

 			system.debug('isValid: ' + isValid);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return isValid;
    }

    @TestVisible private static Boolean validateAddressSearchInput(String unitType, String unitNumber, String streetLotNumber, String streetName, String streetType, String suburbLocality, String state, String postcode) {    																
		Boolean isValid = true;

        try {
        	Boolean hasValidMandatoryFields = validateAddressSearchInputMandatoryFields(streetLotNumber, streetName, suburbLocality, state, postcode);
			system.debug('hasValidMandatoryFields: ' + hasValidMandatoryFields);
        	        	
			if (!hasValidMandatoryFields) {				
				isValid = false;				   
			} else {
				Boolean hasValidAddressSearchInputFields = validateAddressSearchInputFields(unitType, unitNumber, streetLotNumber, streetName, streetType, suburbLocality, state, postcode);
				system.debug('hasValidAddressSearchInputFields: ' + hasValidAddressSearchInputFields);

				// Validate all address fields 
				if (!hasValidAddressSearchInputFields) {
					isValid = false;
				}   				     
			}

 			system.debug('isValid: ' + isValid);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return isValid;
    } 

    @TestVisible private static Boolean validateAddressSearchInputMandatoryFields(String streetLotNumber, String streetName, String suburbLocality, String state, String postcode) {    																
		Boolean isValid = true;

        try {
        	// GFAST-5048 fixed null pointer exceptions
	        if (String.isEmpty(streetLotNumber) || String.isEmpty(streetName) || String.isEmpty(suburbLocality)) {
	            isValid = false;                    
	        } else {	        	            
	            if (String.isEmpty(state) || String.isEmpty(postcode)) {
					isValid = false;           
	            }            
	        }           	        	        	
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return isValid;
    } 

    @TestVisible private static Boolean validateAddressSearchInputFields(String unitType, String unitNumber, String streetLotNumber, String streetName, String streetType, String suburbLocality, String state, String postcode) {    																
		Boolean isValid = true;

        try {
            // Perform detailed validation checks for each field
            if (String.isNotEmpty(unitType)) {
	            if (!validateUnitType(unitType)) {
	                isValid = false;
	            }            	
            }

			if (String.isNotEmpty(streetType)) {
	            if (!validateStreetType(streetType)) {
	                isValid = false;
	            }
			}

			if (String.isNotEmpty(state)) {
	            if (!validateState(state)) {
	                isValid = false;
	            }
			}

			if (String.isNotEmpty(postcode)) {             
	            if (!validatePostcode(postcode)) {
	                isValid = false;
	            }
			}
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return isValid;
    }

    @TestVisible private static Boolean validateUnitType(String unitType) {    																
		Boolean isValid = true;

        try {
			String unitTypeAPIName = getUnitTypeAPIName(unitType);

			if (String.isEmpty(unitTypeAPIName)) {
				isValid = false;
			} 			          	        	

 			system.debug('validateUnitType - isValid: ' + isValid);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return isValid;
    }

    @TestVisible private static Boolean validateStreetType(String streetType) {    																
		Boolean isValid = true;

        try {
			String streetTypeAPIName = getStreetTypeAPIName(streetType);

			if (String.isEmpty(streetTypeAPIName)) {
				isValid = false;
			}                   	        	

 			system.debug('validateStreetType - isValid: ' + isValid);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return isValid;
    }
 
    @TestVisible private static Boolean validateState(String state) {    	    														
		Boolean isValid = true;

        try {     	        	
			String stateAPIName = getStateAPIName(state);

			if (String.isEmpty(stateAPIName)) {
				isValid = false;
			}

 			system.debug('validateState - isValid: ' + isValid);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return isValid;
    }
    
    @TestVisible private static Boolean validatePostcode(String postcode) {    														    	
		Boolean isValid = true;

        try {        	        	
	        if (postcode.length() != 4) {
				isValid = false;        	
	        } else {	        	
				if (!postcode.isNumeric()) {				
					isValid = false;
				} 	        	
	        }   	

 			system.debug('validatePostcode - isValid: ' + isValid);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return isValid;
    }                        
           
    public static Boolean isNumeric(String inputString) {        
        Boolean isNumeric;        
        
        try {
            Decimal.valueOf(inputString);
            isNumeric = true; 
        } catch (Exception e) {
            isNumeric = false;
        }

        return isNumeric;
    }

	private static String getValueFromMap( Map<String,String> lookup, String value ) {
		String result;
		if (String.isNotEmpty(value)) {
			value = value.trim().replaceAll('[\r\n\t\f ]{1,}', ' ').toUpperCase();
			result = lookup.get(value);
			if (result == null &&
					DF_FeatureToggleController.isFeatureToggleEnabled('MR_Argo_EE_Enhancements') &&
					lookup.values().contains(value)) {
				result = value;
			}
		}
		return result;
	}

	private static Map<String,String> unitTypeMap = new Map<String,String> {
			'APARTMENT'		=> 'APT',
			'COTTAGE'		=> 'CTGE',
			'DUPLEX'		=> 'DUPL',
			'FACTORY'		=> 'FCTY',
			'FLAT'			=> 'FLAT',
			'HOUSE'			=> 'HSE',
			'KIOSK'			=> 'KSK',
			'MAISONETTE'	=> 'MSNT',
			'MARINE BERTH'	=> 'MBTH',
			'OFFICE'		=> 'OFFC',
			'PENTHOUSE'		=> 'PTHS',
			'ROOM'			=> 'ROOM',
			'SHED'			=> 'SHED',
			'SHOP'			=> 'SHOP',
			'SITE'			=> 'SITE',
			'STALL'			=> 'STLL',
			'STUDIO'		=> 'STU',
			'SUITE'			=> 'Suite', // dodgy
			'TOWNHOUSE'		=> 'TNHS',
			'UNIT'			=> 'UNIT',
			'VILLA'			=> 'VLLA',
			'WARD'			=> 'WARD',
			'WAREHOUSE'		=> 'WHSE'
	};

	public static String getUnitTypeAPIName(String unitType) {
		return getValueFromMap(unitTypeMap, unitType);
    }

	private static Map<String, String> streetTypeMap = new Map<String, String>{
			'ALLEY' 	=> 'ALLY',
			'AMBLE' 	=> 'AMBL',
			'APPROACH'	=> 'APP',
			'ARCADE'	=> 'ARC',
			'ARTERIAL'	=> 'ARTL',
			'AVENUE'	=> 'AV',
			'BAY'		=> 'BAY',
			'BEND'		=> 'BEND',
			'BRAE'		=> 'BRAE',
			'BREAK'		=> 'BRK',
			'BOULEVARD'	=> 'BVD',
			'BOARDWALK'	=> 'BWLK',
			'BOWL'		=> 'BOWL',
			'BYPASS'	=> 'BYPA',
			'CIRCLE'	=> 'CIR',
			'CIRCUS'	=> 'CRCS',
			'CIRCUIT'	=> 'CCT',
			'CHASE'		=> 'CH',
			'CLOSE'		=> 'CL',
			'CORNER'	=> 'CNR',
			'COMMON'	=> 'CMMN',
			'CONCOURSE'	=> 'CON',
			'CRESCENT'	=> 'CR',
			'CROSS'		=> 'CRSS',
			'COURSE'	=> 'CRSE',
			'CREST'		=> 'CRST',
			'CRUISEWAY'	=> 'CSWY',
			'COURT'		=> 'CT',
			'COURTS'	=> 'CT',
            'COURT/S'	=> 'CT',
			'COVE'		=> 'COVE',
			'DALE'		=> 'DALE',
			'DELL'		=> 'DELL',
			'DENE'		=> 'DE',
			'DIVIDE'	=> 'DIV',
			'DOMAIN'	=> 'DOM',
			'DRIVE'		=> 'DR',
			'EAST'		=> 'EST',
			'EDGE'		=> 'EDGE',
			'ENTRANCE'	=> 'ENT',
			'ESPLANADE'	=> 'ESP',
			'EXTENSION'	=> 'EXTN',
			'FLATS'		=> 'FLAT',
			'FORD'		=> 'FORD',
			'FREEWAY'	=> 'FWY',
			'GATE'		=> 'GTE',
			'GARDEN'	=> 'GDN',
			'GARDENS'	=> 'GDNS',
			'GLADE'		=> 'GLDE',
			'GLADES'	=> 'GLDE',
            'GLADE/S'	=> 'GLDE',
            'GLEN'		=> 'GLEN',
			'GULLY'		=> 'GLY',
			'GRANGE'	=> 'GRA',
			'GREEN'		=> 'GRN',
			'GROVE'		=> 'GR',
			'GATEWAY'	=> 'GWY',
			'HILL'		=> 'HILL',
			'HOLLOW'	=> 'HLLW',
			'HEATH'		=> 'HTH',
			'HEIGHTS'	=> 'HTS',
			'HUB'		=> 'HUB',
			'HIGHWAY'	=> 'HWY',
			'ISLAND'	=> 'ID',
			'JUNCTION'	=> 'JNC',
			'LANE'		=> 'LANE',
			'LINK'		=> 'LINK',
			'LOOP'		=> 'LOOP',
			'LOWER'		=> 'LWR',
			'LANEWAY'	=> 'LNWY',
			'MALL'		=> 'MALL',
			'MEW'		=> 'MEW',
			'MEWS'		=> 'MEWS',
			'NOOK'		=> 'NOOK',
			'NORTH'		=> 'NTH',
			'OUTLOOK'	=> 'OTLK',
			'PATH'		=> 'PATH',
			'PARADE'	=> 'PDE',
			'POCKET'	=> 'PKT',
			'PARKWAY'	=> 'PWAY',
			'PLACE'		=> 'PL',
			'PLAZA'		=> 'PLZA',
			'PROMENADE'	=> 'PROM',
			'PASS'		=> 'PASS',
			'PASSAGE'	=> 'PSGE',
			'POINT'		=> 'PNT',
			'PURSUIT'	=> 'PRST',
			'PATHWAY'	=> 'PWY',
			'QUADRANT'	=> 'QDRT',
			'QUAY'		=> 'QY',
			'REACH'		=> 'RCH',
			'ROAD'		=> 'RD',
			'RIDGE'		=> 'RDGE',
			'RESERVE'	=> 'RES',
			'REST'		=> 'REST',
			'RETREAT'	=> 'RTT',
			'RIDE'		=> 'RIDE',
			'RISE'		=> 'RISE',
			'ROUND'		=> 'RND',
			'ROW'		=> 'ROW',
			'RISING'	=> 'RSNG',
			'RETURN'	=> 'RTN',
			'RUN'		=> 'RUN',
			'SLOPE'		=> 'SLPE',
			'SQUARE'	=> 'SQ',
			'STREET'	=> 'ST',
			'SOUTH'		=> 'SOUTH',
			'STRIP'		=> 'STRP',
			'STEPS'		=> 'STPS',
			'SUBWAY'	=> 'SBWY',
			'TERRACE'	=> 'TCE',
			'THROUGHWAY'=> 'THRU',
			'TOR'		=> 'TOR',
			'TRACK'		=> 'TRK',
			'TRAIL'		=> 'TRL',
			'TURN'		=> 'TURN',
			'TOLLWAY'	=> 'TLWY',
			'UPPER'		=> 'UPR',
			'VALLEY'	=> 'VLLY',
			'VISTA'		=> 'VSTA',
			'VIEW'		=> 'VIEW',
			'VIEWS'		=> 'VIEW',
            'VIEW/S'	=> 'VIEW',
            'WAY'		=> 'WAY',
			'WOOD'		=> 'WD',
			'WEST'		=> 'WEST',
			'WALK'		=> 'WALK',
			'WALKWAY'	=> 'WKWY',
			'WATERS'	=> 'WTRS',
			'WATERWAY'	=> 'WATERWAY',
			'WYND'		=> 'WYND'
	};

	public static String getStreetTypeAPIName(String streetType) {
		return getValueFromMap(streetTypeMap, streetType);
    }

	private static Map<String, String> stateMap = new Map<String, String>{
		'NEW SOUTH WALES'				=>	'NSW',
		'VICTORIA'						=>	'VIC',
		'QUEENSLAND'					=>	'QLD',
		'WESTERN AUSTRALIA'				=>	'WA',
		'SOUTH AUSTRALIA'				=>	'SA',
		'TASMANIA'						=>	'TAS',
		'AUSTRALIAN CAPITAL TERRITORY'	=>	'ACT',
		'NORTHERN TERRITORY'			=>	'NT'
	};

    public static String getStateAPIName(String state) {
		return getValueFromMap(stateMap, state);
    }
    
    @TestVisible private static String getValidSizedLocationId(String locationId) {
        String validSizedLocationId;        
        
        try {
	        if (locationId.length() <= 15) {
				validSizedLocationId = locationId;
	        } else {	        	
	        	// Trim to fit field
	        	// Build following string: LOC...[last 9 chars]
	        	// eg. LOC...111111789
	        	Integer startIndex = locationId.length() - 9;
	        	Integer endIndex = locationId.length();
	        	validSizedLocationId = DF_SF_BulkUploadUtils.OVER_SIZE_LOCID_PREFIX + locationId.substring(startIndex, endIndex);
	        }        
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return validSizedLocationId;
    }
    
    @TestVisible private static String getValidSizedLatitude(String latitude) {    	        
        String validSizedLatitude;        
        
        try {
	        if (latitude.length() <= 20) {
	        	validSizedLatitude = latitude;
	        } else {	        	
	        	// Trim to fit field
	        	// Build following string: ...[last 17 chars]
	        	// eg. ...11111111111111111
	        	Integer startIndex = latitude.length() - 17;
	        	Integer endIndex = latitude.length();
	        	validSizedLatitude = '...' + latitude.substring(startIndex, endIndex);	                    	
	        }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return validSizedLatitude;
    }      
    
    @TestVisible private static String getValidSizedLongitude(String longitude) {    	        
        String validSizedLongitude;        
        
        try {
	        if (longitude.length() <= 20) {	        	
				validSizedLongitude = longitude;
	        } else {	        	
	        	// Trim to fit field
	        	// Build following string: ...[last 17 chars]
	        	// eg. ...11111111111111111
	        	Integer startIndex = longitude.length() - 17;
	        	Integer endIndex = longitude.length();
	        	validSizedLongitude = '...' + longitude.substring(startIndex, endIndex);	                    	
	        }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return validSizedLongitude;
    }       
}