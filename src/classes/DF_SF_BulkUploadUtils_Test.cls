@isTest
private class DF_SF_BulkUploadUtils_Test {

    @isTest static void test_processBulkUploadFile_locationidSearch() {      	 	    	
    	// Setup data
		final String ENCODING_SCHEME = 'ISO-8859-1';
		String csvContent = 'LocationId,Latitude,Longitude,UnitType,Level,UnitNumber,StreetorLotNumber,StreetName,StreetType,SuburborLocality,State,Postcode';
		csvContent = csvContent + '\n';
		csvContent = csvContent + 'LOC000002696724,-42.78931,147.05578,UNIT,5,1,11,PACIFIC,Way,BELDON,Western Australia,6027';

		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf(csvContent);
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    
		
    	test.startTest();           

		DF_SF_BulkUploadUtils.processBulkUploadFile(opptyBundle.Id);			
                                        
        test.stopTest();                

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id
											FROM   DF_SF_Request__c];
											
		system.AssertEquals(1, sfReqList.size());											
    }

    @isTest static void test_processBulkUploadFile_locationidSearch_overSizeLocationId() {      	 	    	
    	// Setup data
		final String ENCODING_SCHEME = 'ISO-8859-1';
		String csvContent = 'LocationId,Latitude,Longitude,UnitType,Level,UnitNumber,StreetorLotNumber,StreetName,StreetType,SuburborLocality,State,Postcode';
		csvContent = csvContent + '\n';
		csvContent = csvContent + 'LOC00000269672499999,-42.78931,147.05578,UNIT,5,1,11,PACIFIC,Way,BELDON,Western Australia,6027';

		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf(csvContent);
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    
		
    	test.startTest();           

		DF_SF_BulkUploadUtils.processBulkUploadFile(opptyBundle.Id);			
                                        
        test.stopTest();                

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id
											FROM   DF_SF_Request__c];
											
		system.AssertEquals(1, sfReqList.size());											
    }
    
    @isTest static void test_processBulkUploadFile_latLongSearch() {      	 	    	
    	// Setup data
		final String ENCODING_SCHEME = 'ISO-8859-1';
		String csvContent = 'LocationId,Latitude,Longitude,UnitType,Level,UnitNumber,StreetorLotNumber,StreetName,StreetType,SuburborLocality,State,Postcode';
		csvContent = csvContent + '\n';
		csvContent = csvContent + ',-42.78931,147.05578,UNIT,5,1,11,,Way,,Western Australia,6027';

		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf(csvContent);
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    
		
    	test.startTest();           

		DF_SF_BulkUploadUtils.processBulkUploadFile(opptyBundle.Id);
                                        
        test.stopTest();                

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id
											FROM   DF_SF_Request__c];
											
		system.AssertEquals(1, sfReqList.size());	
    }

    @isTest static void test_processBulkUploadFile_latLongSearch_overSizeLatLong() {      	 	    	
    	// Setup data
		final String ENCODING_SCHEME = 'ISO-8859-1';
		String csvContent = 'LocationId,Latitude,Longitude,UnitType,Level,UnitNumber,StreetorLotNumber,StreetName,StreetType,SuburborLocality,State,Postcode';
		csvContent = csvContent + '\n';
		csvContent = csvContent + ',-42.7893111111111111111111111,147.0557811111111111111111111,UNIT,5,1,11,,Way,,Western Australia,6027';

		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf(csvContent);
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    
		
    	test.startTest();           

		DF_SF_BulkUploadUtils.processBulkUploadFile(opptyBundle.Id);
                                        
        test.stopTest();                

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id
											FROM   DF_SF_Request__c];
											
		system.AssertEquals(1, sfReqList.size());	
    }
    
    @isTest static void test_processBulkUploadFile_addressSearch() {      	 	    	
    	// Setup data
		final String ENCODING_SCHEME = 'ISO-8859-1';
		String csvContent = 'LocationId,Latitude,Longitude,UnitType,Level,UnitNumber,StreetorLotNumber,StreetName,StreetType,SuburborLocality,State,Postcode';
		csvContent = csvContent + '\n';
		csvContent = csvContent + ',,,UNIT,5,1,11,PACIFIC,Way,BELDON,Western Australia,6027';

		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf(csvContent);
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    
		
    	test.startTest();           

		DF_SF_BulkUploadUtils.processBulkUploadFile(opptyBundle.Id);
                                        
        test.stopTest();                

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id
											FROM   DF_SF_Request__c];
											
		system.AssertEquals(1, sfReqList.size());	
    }        
    
    @isTest static void test_blobToString() {      	 	    	
    	// Setup data
		final String ENCODING_SCHEME = 'ISO-8859-1';

		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf('LocationId,Latitude,Longitude');
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    	
    	
		Blob fileContentBlob;

    	test.startTest();           

		fileContentBlob = DF_SF_BulkUploadUtils.getAttachmentFile(opptyBundle.Id);
		String fileContentStr = DF_SF_BulkUploadUtils.blobToString(fileContentBlob, ENCODING_SCHEME);
                                        
        test.stopTest();                

        // Assertions
		system.AssertNotEquals(null, fileContentStr);
    }
    
    @isTest static void test_getAttachmentFile() {      	 	    	
    	// Setup data

		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf('LocationId,Latitude,Longitude');
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    	
    	
		Blob fileContentBlob;

    	test.startTest();           

		fileContentBlob = DF_SF_BulkUploadUtils.getAttachmentFile(opptyBundle.Id);
                                        
        test.stopTest();                

        // Assertions
		system.AssertNotEquals(null, fileContentBlob);
    }        
}