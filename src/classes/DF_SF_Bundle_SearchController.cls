public with sharing class DF_SF_Bundle_SearchController {
    
    @AuraEnabled
    public static Boolean isArgoEnhancementsEnabled() {
        return GlobalUtility.getReleaseToggle('Release_Toggles__mdt', 'MR_Argo_EE_Enhancements', 'IsActive__c');
    }
    
	/*
        * Fetch bundle recent records for the given max count 
        * Parameters : maxCount.
        * @Return : Returns a list of DF Bundle search result records
    */
	@AuraEnabled
    public static String getRecords(String searchTerm,Boolean isTriggerBySearch) {
    	System.debug('Search Term values is : ' + searchTerm);	
    	System.debug('isTriggerBySearch : ' + isTriggerBySearch);
		
		List<DF_Opportunity_Bundle__c> bundleList = new List<DF_Opportunity_Bundle__c>();

		try {
				//if(String.isEmpty(searchTerm)){

				List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
				System.debug('User details ' + userLst);
				Id recTypeId = DF_CS_API_Util.getRecordType(DF_Constants.ENTERPRISE_ETHERNET_NAME,DF_Constants.OPPORTUNITY_BUNDLE_OBJECT);
            				
				if(userLst.size()>0 && userLst.get(0).AccountId!=null && recTypeId != null){
					if(String.isEmpty(searchTerm) && !isTriggerBySearch){
						bundleList = [SELECT Id, Name, CreatedDate,DF_Quote_Count__c,DF_SF_Request_Count__c FROM DF_Opportunity_Bundle__c where 
									  Account__c = :userLst.get(0).AccountId AND RecordTypeId = :recTypeId ORDER BY Name DESC NULLS FIRST ];
					}
					else if(!String.isEmpty(searchTerm)){
                        Boolean isEnhancementEnabled = isArgoEnhancementsEnabled();
                        if (isEnhancementEnabled) {
                            bundleList = doEnhancedSearch(searchTerm, userLst.get(0).AccountId, recTypeId);
                        } else {
                            bundleList = doSearchByBundleId(searchTerm, userLst.get(0).AccountId, recTypeId);
                        }
					}
				}else{
					throw new CustomException('User record with the RSP account not found');
				}

		} catch(Exception e) {
			throw new CustomException(e.getMessage());
		}
		
		return JSON.serialize(bundleList);
    }	

	private static List<DF_Opportunity_Bundle__c> doEnhancedSearch(String searchTerm, Id userAcctId, Id recTypeId) {
        List<DF_Opportunity_Bundle__c> bundleList = new List<DF_Opportunity_Bundle__c>();

        if (searchTerm.startsWithIgnoreCase('LOC')) {
            // search by loc id
            Set<Id> bundleIdSet = findBundleIdsByLocId(searchTerm, userAcctId, recTypeId);

            bundleList = [SELECT Id, Name, CreatedDate, DF_Quote_Count__c, DF_SF_Request_Count__c
                          FROM DF_Opportunity_Bundle__c
                          WHERE Account__c = :userAcctId
                          AND RecordTypeId = :recTypeId
                          AND isDeleted = false
                          AND Id IN :bundleIdSet
                          ORDER BY Name DESC NULLS FIRST];

        } else if (searchTerm.startsWithIgnoreCase('EEQ')) {
            // search by quote id
            bundleList = [SELECT Id, Name, CreatedDate, DF_Quote_Count__c, DF_SF_Request_Count__c
                          FROM DF_Opportunity_Bundle__c
                          WHERE Account__c = :userAcctId
                          AND RecordTypeId = :recTypeId
                          AND isDeleted = false
                          AND DF_Quote_Count__c > 0
                          AND Id IN (
                              SELECT Opportunity_Bundle__c
                              FROM DF_Quote__c
                              WHERE name = :searchTerm
                              AND isDeleted = false
                          )
                          ORDER BY Name DESC NULLS FIRST];
        } else {
            // fallback to search by BundleId
            bundleList = doSearchByBundleId(searchTerm, userAcctId, recTypeId);
        }
        return bundleList;
    }

    private static List<DF_Opportunity_Bundle__c> doSearchByBundleId(String searchTerm, Id userAcctId, Id recTypeId) {
        return [SELECT Id, Name, CreatedDate,DF_Quote_Count__c,DF_SF_Request_Count__c FROM DF_Opportunity_Bundle__c where
									  Account__c = :userAcctId AND RecordTypeId = :recTypeId and name = :searchTerm ORDER BY Name DESC NULLS FIRST];
    }

    private static Set<Id> findBundleIdsByLocId(String searchTerm, Id userAcctId, Id recTypeId) {
        Set<Id> result = new Set<Id>();

        AggregateResult[] bundleIdsFromSFRequests = [SELECT Opportunity_Bundle__c
                                                     FROM DF_SF_Request__c
                                                     WHERE Location_Id__c = :searchTerm
                                                     AND isDeleted = false
                                                     AND Opportunity_Bundle__r.Account__c = :userAcctId
                                                     AND Opportunity_Bundle__r.RecordTypeId = :recTypeId
                                                     GROUP BY Opportunity_Bundle__c];
        for (AggregateResult ar : bundleIdsFromSFRequests) {
            result.add((Id) ar.get('Opportunity_Bundle__c'));
        }

        AggregateResult[] bundleIdsFromQuotes = [SELECT Opportunity_Bundle__c
                                                 FROM DF_Quote__c
                                                 WHERE Location_Id__c = :searchTerm
                                                 AND isDeleted = false
                                                 AND Opportunity_Bundle__r.Account__c = :userAcctId
                                                 AND Opportunity_Bundle__r.RecordTypeId = :recTypeId
                                                 GROUP BY Opportunity_Bundle__c];
        for (AggregateResult ar : bundleIdsFromQuotes) {
            result.add((Id) ar.get('Opportunity_Bundle__c'));
        }

        return result;
    }
}