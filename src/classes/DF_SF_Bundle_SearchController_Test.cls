@isTest 
private class DF_SF_Bundle_SearchController_Test {

	@isTest
	private static void test_getRecordsWithEmptySearchTerm() {

		Id oppId = DF_TestService.getQuoteRecords();
		String searchTerm = null;
		String result = '';
		List<DF_Opportunity_Bundle__c> deserializedResult = new List<DF_Opportunity_Bundle__c>();
		
    	User commUser;
		commUser = DF_TestData.createDFCommUser();
        system.runAs(commUser) {
	    	test.startTest();    
			result = DF_SF_Bundle_SearchController.getRecords(searchTerm,false);
			System.debug(result);
		    deserializedResult = (List<DF_Opportunity_Bundle__c>)JSON.deserialize(result, List<DF_Opportunity_Bundle__c>.Class);
	        System.debug(deserializedResult);                                   
	        test.stopTest();                
        }	                    
        // Assertions
        system.AssertEquals(0, deserializedResult.size());
	}

	@isTest
	private static void test_getRecordsWithInvalidSearchTerm() {
		// Setup data    	
		Integer maxCount = 10;
		String searchTerm = 'This search term will cause no record to return';
		String result = '';
		List<DF_Opportunity_Bundle__c> deserializedResult = new List<DF_Opportunity_Bundle__c>();

		Id oppId = DF_TestService.getQuoteRecords();

    	User commUser;
		commUser = DF_TestData.createDFCommUser();

        system.runAs(commUser) {
	    	test.startTest();
			result = DF_SF_Bundle_SearchController.getRecords(searchTerm,false);
			deserializedResult = (List<DF_Opportunity_Bundle__c>)JSON.deserialize(result, List<DF_Opportunity_Bundle__c>.Class);
			test.stopTest();
        }	                                  
        
        // Assertions
        system.AssertEquals(0, deserializedResult.size());
	}

	@isTest
	private static void test_getRecordsWithValidSearchTerm() {
		// Setup data    	
		Integer maxCount = 10;
		String searchTerm = '';
		String result = '';
		List<DF_Opportunity_Bundle__c> deserializedResult = new List<DF_Opportunity_Bundle__c>();

		Id oppId = DF_TestService.getQuoteRecords();

		DF_Opportunity_Bundle__c bundle = [
					SELECT
						Id,
						Name						
					FROM
						DF_Opportunity_Bundle__c
					WHERE Id =: oppId
				];
		
		searchTerm = bundle.Name;

    	User commUser;
		commUser = DF_TestData.createDFCommUser();

        system.runAs(commUser) {
	    	test.startTest();
			result = DF_SF_Bundle_SearchController.getRecords(searchTerm,true);
			deserializedResult = (List<DF_Opportunity_Bundle__c>)JSON.deserialize(result, List<DF_Opportunity_Bundle__c>.Class);
			test.stopTest();                
        }	                                  
        
        // Assertions
        //system.AssertEquals(1, deserializedResult.size());
	}
    
    @isTest
    private static void should_search_by_bundle_id_regardless_toggle() {
        // Setup data
        User commUser = DF_TestData.createDFCommUser();
        List<DF_Opportunity_Bundle__c> deserializedResult;
        Boolean isEnabled = GlobalUtility.getReleaseToggle('Release_Toggles__mdt', 'MR_Argo_EE_Enhancements', 'IsActive__c');
        
        // Run test method
        system.runAs(commUser) {
            Account acct = DF_TestData.getAccount(commUser);
            Id oppId = DF_TestService.getQuoteRecords(acct);
            DF_Opportunity_Bundle__c bundle = [SELECT Id, Name						
                                               FROM DF_Opportunity_Bundle__c
                                               WHERE Id = :oppId];
            String searchTerm = bundle.Name;
            
            test.startTest();
            String result = DF_SF_Bundle_SearchController.getRecords(searchTerm, true);
            deserializedResult = (List<DF_Opportunity_Bundle__c>) JSON.deserialize(result, List<DF_Opportunity_Bundle__c>.Class);
            test.stopTest();                
        }	                                  
        
        // Assertions
        if (isEnabled) {
            system.AssertEquals(1, deserializedResult.size());
        } else {
            system.AssertEquals(1, deserializedResult.size());
        }  
    }
    
    @isTest
    private static void should_search_by_location_id_when_toggle_on() {
        // Setup data
        User commUser = DF_TestData.createDFCommUser();
        List<DF_Opportunity_Bundle__c> deserializedResult;
        Boolean isEnabled = GlobalUtility.getReleaseToggle('Release_Toggles__mdt', 'MR_Argo_EE_Enhancements', 'IsActive__c');
        
        // Run test method
        system.runAs(commUser) {
            Account acct = DF_TestData.getAccount(commUser);
            Id oppId = DF_TestService.getQuoteRecords(acct);
            String searchTerm = 'LOC000035375038';
            
            test.startTest();
            String result = DF_SF_Bundle_SearchController.getRecords(searchTerm, true);
            deserializedResult = (List<DF_Opportunity_Bundle__c>) JSON.deserialize(result, List<DF_Opportunity_Bundle__c>.Class);
            test.stopTest();                
        }	                                  
        
        // Assertions
        if (isEnabled) {
            system.AssertEquals(1, deserializedResult.size());
        } else {
            system.AssertEquals(0, deserializedResult.size());
        }  
    }
    
    @isTest
    private static void should_search_by_quote_id_when_toggle_on() {
        // Setup data
        User commUser = DF_TestData.createDFCommUser();
        List<DF_Opportunity_Bundle__c> deserializedResult;
        Boolean isEnabled = GlobalUtility.getReleaseToggle('Release_Toggles__mdt', 'MR_Argo_EE_Enhancements', 'IsActive__c');
        
        // Run test method
        system.runAs(commUser) {
            Account acct = DF_TestData.getAccount(commUser);
            Id oppId = DF_TestService.getQuoteRecords(acct);
            
            List<DF_Quote__c> quotes = [SELECT Id, Name
                                 FROM DF_Quote__c
                                 WHERE Opportunity_Bundle__c = :oppId
                                 AND isDeleted = false];
            String searchTerm = quotes[0].Name;
            
            test.startTest();
            String result = DF_SF_Bundle_SearchController.getRecords(searchTerm, true);
            deserializedResult = (List<DF_Opportunity_Bundle__c>) JSON.deserialize(result, List<DF_Opportunity_Bundle__c>.Class);
            test.stopTest();                
        }	                                  
        
        // Assertions
        if (isEnabled) {
            system.AssertEquals(1, deserializedResult.size());
        } else {
            system.AssertEquals(0, deserializedResult.size());
        }  
    }
}