public class DF_SF_CaptureController {

	// Errors
	public static final String ERR_OPPTY_BUNDLE_ID_NOT_FOUND = 'Opportunity Bundle Id was not found.';
	public static final String ERR_COMM_USR_ACCT_ID_NOT_FOUND = 'Community User AccountId was not found.';
	public static final String ERR_COMM_USR_ID_NOT_FOUND = 'Community UserId was not found.';
	public static final String ERR_COMM_USR_NOT_FOUND = 'Community User was not found.';
	
	@TestVisible static String apiToUse  = EE_CISLocationAPIUtils.getDfOrderSettings().Location_API__c;
	


    @AuraEnabled
    public static String searchByLocationID(String locId) {    	
		Map<String, String> addressInputsMap = new Map<String, String>();
		List<String> paramList = new List<String>();
    	
		Id opptyBundleId;
		Id sfReqId;
		try {
		if (DF_Constants.API_NAME_LAPI.equalsIgnoreCase(apiToUse)) {
	        // Create Oppty Bundle rec	        
	        opptyBundleId = createOpptyBundle();
	        system.debug('opptyBundleId: ' + opptyBundleId);			
		         
	        if (opptyBundleId != null) {	        		        	        			        				        		        
		        sfReqId = DF_LAPI_APIServiceUtils.createServiceFeasibilityRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, locId, null, null, opptyBundleId, null, addressInputsMap);
				system.debug('sfReqId: ' + sfReqId);
      
      			if (sfReqId != null) {	         				                                 
	                // Add to Async Req - creation list
	                paramList.add(sfReqId);          				
      			}

	            if (!paramList.isEmpty()) {	            	                                   
	                AsyncQueueableUtils.createRequests(DF_LAPISearchByLocationHandler.HANDLER_NAME, paramList);                                
	            }   	        	
	        } else {
				throw new CustomException(DF_SF_CaptureController.ERR_OPPTY_BUNDLE_ID_NOT_FOUND);
				}
			} else {
				DF_SF_Request__c sfReq = EE_CISLocationAPIUtils.createServiceFeasibilityRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, locId, null, null, opptyBundleId, null, addressInputsMap);
				opptyBundleId = EE_CISLocationAPIService.searchLocationSync(sfReq);
			}
		} catch(Exception e) {
			throw new AuraHandledException(e.getMessage());
        }
        
        return opptyBundleId;
    }

    @AuraEnabled
    public static String searchByAddress(String state, String postcode, String suburbLocality, String streetName, String streetType,
    									 String streetLotNumber, String unitType, String unitNumber, String level, String complexSiteName,
    									 String complexBuildingName, String complexStreetName, String complexStreetType, String complexStreetNumber) {							 	    									 	
		Map<String, String> addressInputsMap = new Map<String, String>();
		List<String> paramList = new List<String>();

		Id opptyBundleId;	
		Id sfReqId;	

		try {
			// Build address map
			addressInputsMap.put('state', state);
			addressInputsMap.put('postcode', postcode);
			addressInputsMap.put('suburbLocality', suburbLocality);
			addressInputsMap.put('streetName', streetName);
			addressInputsMap.put('streetType', streetType);
			addressInputsMap.put('streetLotNumber', streetLotNumber);
			addressInputsMap.put('unitType', unitType);
			addressInputsMap.put('unitNumber', unitNumber);
			addressInputsMap.put('level', level);
			addressInputsMap.put('complexSiteName', complexSiteName);
			addressInputsMap.put('complexBuildingName', complexBuildingName);
			addressInputsMap.put('complexStreetName', complexStreetName);
			addressInputsMap.put('complexStreetType', complexStreetType);
			addressInputsMap.put('complexStreetNumber', complexStreetNumber);
			if (DF_Constants.API_NAME_LAPI.equalsIgnoreCase(apiToUse)) {
	        // Create Oppty Bundle rec	        
	        opptyBundleId = createOpptyBundle();
	        system.debug('opptyBundleId: ' + opptyBundleId);			
		         
	        if (opptyBundleId != null) {		        	        	        			        				                        		        
				sfReqId = DF_LAPI_APIServiceUtils.createServiceFeasibilityRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, null, null, null, opptyBundleId, null, addressInputsMap);
				system.debug('sfReqId: ' + sfReqId);

      			if (sfReqId != null) {	         				                                 
	                // Add to Async Req - creation list
	                paramList.add(sfReqId);         				
      			}

	            if (!paramList.isEmpty()) {	            	                                    
	                AsyncQueueableUtils.createRequests(DF_LAPISearchByAddressHandler.HANDLER_NAME, paramList);                                
	            }    
	        } else {
				throw new CustomException(DF_SF_CaptureController.ERR_OPPTY_BUNDLE_ID_NOT_FOUND);
				}
			} else {
				DF_SF_Request__c sfReq = EE_CISLocationAPIUtils.createServiceFeasibilityRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, null, null, null, opptyBundleId, null, addressInputsMap);
				opptyBundleId = EE_CISLocationAPIService.searchLocationSync(sfReq);
			}
		} catch(Exception e) {
			throw new AuraHandledException(e.getMessage());
        }
        
        return opptyBundleId;
    }

    @AuraEnabled
    public static String searchByLatLong(String latitude, String longitude) {		
		Map<String, String> addressInputsMap = new Map<String, String>();
		List<String> paramList = new List<String>();

		Id opptyBundleId;
		Id sfReqId;

		try {	        						
			if (DF_Constants.API_NAME_LAPI.equalsIgnoreCase(apiToUse)) {     						
	        // Create Oppty Bundle rec	        
	        opptyBundleId = createOpptyBundle();
	        system.debug('opptyBundleId: ' + opptyBundleId);			
		         
	        if (opptyBundleId != null) {		        	        	        			        				        
		        sfReqId = DF_LAPI_APIServiceUtils.createServiceFeasibilityRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG, null, latitude, longitude, opptyBundleId, null, addressInputsMap);			        
		        system.debug('sfReqId: ' + sfReqId);
		        
      			if (sfReqId != null) {	                                    
	                // Add to Async Req - creation list
	                paramList.add(sfReqId);         				
      			}

	            if (!paramList.isEmpty()) {	            	                                  
	                AsyncQueueableUtils.createRequests(DF_LAPISearchByLatLongHandler.HANDLER_NAME, paramList);                                
	            }		        		        
	        } else {
				throw new CustomException(DF_SF_CaptureController.ERR_OPPTY_BUNDLE_ID_NOT_FOUND);
				}
			} else {
				DF_SF_Request__c sfReq = EE_CISLocationAPIUtils.createServiceFeasibilityRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG, null, latitude, longitude, opptyBundleId, null, addressInputsMap);
				opptyBundleId = EE_CISLocationAPIService.searchLocationSync(sfReq);
			}
		} catch(Exception e) {
			throw new AuraHandledException(e.getMessage());
        }
        
        return opptyBundleId;
    }

    public static Id createOpptyBundle() {
		Id commUserAccountId;			
		DF_Opportunity_Bundle__c opptyBundle = new DF_Opportunity_Bundle__c();

        try {        	        	
        	// Get account id from Partner Community user
			commUserAccountId = getCommUserAccountId();
        	Id recTypeId = DF_CS_API_Util.getRecordType(DF_Constants.ENTERPRISE_ETHERNET_NAME, DF_Constants.OPPORTUNITY_BUNDLE_OBJECT);
            
        	if (commUserAccountId != null && recTypeId != null) {
				opptyBundle.Account__c = commUserAccountId; 
                opptyBundle.RecordTypeId = recTypeId;
        	} else {
        		throw new CustomException(DF_SF_CaptureController.ERR_COMM_USR_ACCT_ID_NOT_FOUND);        		
        	}
			
			insert opptyBundle;			 
        } catch(Exception e) {
			throw new CustomException(e.getMessage());
        }
        
        return opptyBundle.Id; 
    }
     
    public static Id getCommUserAccountId() {		
		Id commUserAccountId;
		Id commUserId;
		User commUser;

        try {        	        	
        	// Get Community user AccountId
			commUserId = UserInfo.getUserId();

			if (commUserId != null) {				
				commUser = [SELECT AccountId
							FROM User 
							WHERE Id = :commUserId];
			} else {
				throw new CustomException(ERR_COMM_USR_ID_NOT_FOUND); 
			}

			if (commUser != null) {				
				if (commUser.AccountId != null) {					
					commUserAccountId = commUser.AccountId;
				} else {
					throw new CustomException(DF_SF_CaptureController.ERR_COMM_USR_ACCT_ID_NOT_FOUND);    
				}
			} else {
				throw new CustomException(DF_SF_CaptureController.ERR_COMM_USR_NOT_FOUND);
			}
        } catch(Exception e) {
			throw new CustomException(e.getMessage());
        }     
        
        return commUserAccountId; 
    }                       
}