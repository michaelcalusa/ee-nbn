@isTest
private class DF_SF_CaptureController_Test
{
	@testSetup static void testDataSetup() {
		DF_AS_TestService.createCustomSettingsForAS();		
		DF_AS_TestService.createNBNIntegrationInputsCustomSettings();
	}

    @isTest static void test_searchByLocationID() {                     
        // Setup data       
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'LAPI';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response
        String response = DF_IntegrationTestData.buildLocationAPIRespSuccessful();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
 
        system.runAs(commUser) {
            test.startTest();           
    
            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);
                                            
            test.stopTest();                
        }                                     
        
        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

    @isTest static void test_searchByAddress() {                    

        DF_SF_CaptureController.apiToUse = 'LAPI';              
        // Setup data    
        String opptyBundleId;           
        String state = 'VIC';
        String postcode = '3000';
        String suburbLocality = 'Melbourne';
        String streetName = 'Flinders';
        String streetType = 'ST';
        String streetLotNumber = '21';
        String unitType = '';
        String unitNumber = '';
        String level = '';
        String complexSiteName = '';
        String complexBuildingName = '';
        String complexStreetName = '';
        String complexStreetType = '';
        String complexStreetNumber = '';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response
        String response = DF_IntegrationTestData.buildAddressAPIRespSuccessful();
  
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

        system.runAs(commUser) {
            test.startTest();           

            opptyBundleId = DF_SF_CaptureController.searchByAddress(state, postcode, suburbLocality, streetName, streetType, streetLotNumber, unitType, unitNumber, level, complexSiteName, complexBuildingName, complexStreetName, complexStreetType, complexStreetNumber);
                                            
            test.stopTest();                
        }                                                         

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

    @isTest static void test_searchByLatLong() {                    

        DF_SF_CaptureController.apiToUse = 'LAPI';                  
        // Setup data       
        Id opptyBundleId;
        String latitude = '-33.840213';
        String longitude = '151.207368';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response
        String response = DF_IntegrationTestData.buildLocationAPIRespSuccessful();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
 
        system.runAs(commUser) {
            test.startTest();           

            opptyBundleId = DF_SF_CaptureController.searchByLatLong(latitude, longitude);
                                            
            test.stopTest();                
        }                                                               
        
        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }    

    @isTest static void test_createOpptyBundle() {                          
        // Setup data
        Id opptyBundleId;
        User commUser;
        
        commUser = DF_TestData.createDFCommUser();
  
        system.runAs(commUser) {
            // Statements to be executed by this test user
            test.startTest();           
    
            opptyBundleId = DF_SF_CaptureController.createOpptyBundle();
                                            
            test.stopTest();                
        }       
               
        // Assertions                                                   
        system.AssertNotEquals(null, opptyBundleId);
    }         
    @isTest static void test_getCommUserAccountId() {                   
        // Setup data  
        Id commUserAccountId;
        User commUser = DF_TestData.createDFCommUser();

        system.runAs(commUser) {
            test.startTest();           
            
            commUserAccountId = DF_SF_CaptureController.getCommUserAccountId();
                                    
            test.stopTest();               
        }                               
        
        // Assertions
        system.AssertNotEquals(null, commUserAccountId);
    }         

    @isTest static void test_searchByLocationID_CIS() {
        // Setup data
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'CIS';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response

        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISPositiveJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }
	
	@isTest static void test_searchByLocationID_CIS_Fail_Wireless() {
        // Setup data
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'CIS';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response

        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISWirelessNegativeJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }
	
	@isTest static void test_searchByLocationID_CIS_Fail_Fibre() {
        // Setup data
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'CIS';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response

        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISFibreNegativeJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

	@isTest static void test_searchByLocationID_CIS_Fail_Undefined() {
        // Setup data
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'CIS';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response

        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISUndefinedNegativeJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }
	
	@isTest static void test_searchByLocationID_CIS_Fail_BlankTechnologyType() {
        // Setup data
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'CIS';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response

        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISBlankTechnologyTypeNegativeJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }
	
	@isTest static void test_searchByLocationID_CIS_Fail_EmptyIncluded() {
        // Setup data
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'CIS';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response

        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISEmptyIncludedNegativeJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

	@isTest static void test_searchByLocationID_CIS_Fail_Building() {
        // Setup data
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'CIS';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response

        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISBuildingNegativeJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

	@isTest static void test_searchByLocationID_CIS_Fail_Other() {
        // Setup data
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'CIS';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response

        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISOtherNegativeJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

	@isTest static void test_searchByLocationID_CIS_Fail_Crub() {
        // Setup data
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'CIS';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response

        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISCrubNegativeJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

	@isTest static void test_searchByLocationID_CIS_Fail_HFC() {
        // Setup data
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'CIS';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response

        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISHFCNegativeJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

	@isTest static void test_searchByLocationID_CIS_Fail_Satellite() {
        // Setup data
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'CIS';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response

        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISSatelliteNegativeJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

    @isTest static void test_searchByLocationID_CIS_Async() {
        // Setup data
        Map<String, String> addressInputsMap = new Map<String, String>();
        String sfReqId;
        String locId = 'LOC000035375038';


        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response
        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISPositiveJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {

            String opptyBundleId = DF_SF_CaptureController.createOpptyBundle();
            sfReqId = DF_LAPI_APIServiceUtils.createServiceFeasibilityRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, locId, null, null, opptyBundleId, null, addressInputsMap);
            test.startTest();
            EE_CISLocationAPIService.searchLocationAsync(sfReqId);
            test.stopTest();
        }

        // Assertions
        DF_SF_Request__c reqRec = [Select ID, status__c from DF_SF_Request__c where ID = :sfReqId ];
        system.AssertEquals('Pending', reqRec.status__c);
    }


    @isTest static void test_searchByLocationID_CIS_Empty_Resp() {
        // Setup data
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'CIS';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response

        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', '', headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

    @isTest static void test_searchByLocationID_CIS_400_Response() {
        // Setup data
        String opptyBundleId;
        String locId = 'LOC000035375038';
        DF_SF_CaptureController.apiToUse = 'CIS';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response
        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(400, 'error', '{}', headerMap);

        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLocationID(locId);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

    @isTest static void test_searchByAddress_CIS() {

        DF_SF_CaptureController.apiToUse = 'CIS';
        // Setup data
        String opptyBundleId;
        String state = 'VIC';
        String postcode = '3000';
        String suburbLocality = 'Melbourne';
        String streetName = 'Flinders';
        String streetType = 'ST';
        String streetLotNumber = '21';
        String unitType = '';
        String unitNumber = '';
        String level = '';
        String complexSiteName = '';
        String complexBuildingName = '';
        String complexStreetName = '';
        String complexStreetType = '';
        String complexStreetNumber = '';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response
        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISPositiveJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByAddress(state, postcode, suburbLocality, streetName, streetType, streetLotNumber, unitType, unitNumber, level, complexSiteName, complexBuildingName, complexStreetName, complexStreetType, complexStreetNumber);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

	@isTest static void test_searchByAddress_CIS_Invalid_Address() {

        DF_SF_CaptureController.apiToUse = 'CIS';
        // Setup data
        String opptyBundleId;
        String state = 'VIC';
        String postcode = '3000';
        String suburbLocality = 'Melbourne';
        String streetName = 'Flinders';
        String streetType = 'ST';
        String streetLotNumber = '21';
        String unitType = '';
        String unitNumber = '';
        String level = '';
        String complexSiteName = '';
        String complexBuildingName = '';
        String complexStreetName = '';
        String complexStreetType = '';
        String complexStreetNumber = '';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response
        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json'); 

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISInvalidAddressNegativeJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByAddress(state, postcode, suburbLocality, streetName, streetType, streetLotNumber, unitType, unitNumber, level, complexSiteName, complexBuildingName, complexStreetName, complexStreetType, complexStreetNumber);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }
	

	@isTest static void test_searchByAddress_CIS_400_LotNumber_StreetName_Locality_StreetType() {

        DF_SF_CaptureController.apiToUse = 'CIS';
        // Setup data
        String opptyBundleId;
        String state = '';
        String postcode = '';
        String suburbLocality = 'Melbourne';
        String streetName = 'Flinders';
        String streetType = 'ST';
        String streetLotNumber = '21';
        String unitType = '';
        String unitNumber = '';
        String level = '';
        String complexSiteName = '';
        String complexBuildingName = '';
        String complexStreetName = '';
        String complexStreetType = '';
        String complexStreetNumber = '';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response
        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(400, 'Error', '{}', headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByAddress(state, postcode, suburbLocality, streetName, streetType, streetLotNumber, unitType, unitNumber, level, complexSiteName, complexBuildingName, complexStreetName, complexStreetType, complexStreetNumber);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }


	@isTest static void test_searchByAddress_CIS_400_LotNumber_StreetName_Locality() {

        DF_SF_CaptureController.apiToUse = 'CIS';
        // Setup data
        String opptyBundleId;
        String state = '';
        String postcode = '';
        String suburbLocality = 'Melbourne';
        String streetName = 'Flinders';
        String streetType = '';
        String streetLotNumber = '21';
        String unitType = '';
        String unitNumber = '';
        String level = '';
        String complexSiteName = '';
        String complexBuildingName = '';
        String complexStreetName = '';
        String complexStreetType = '';
        String complexStreetNumber = '';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response
        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(400, 'Error', '{}', headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByAddress(state, postcode, suburbLocality, streetName, streetType, streetLotNumber, unitType, unitNumber, level, complexSiteName, complexBuildingName, complexStreetName, complexStreetType, complexStreetNumber);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

    @isTest static void test_searchByAddress_CIS_400() {

        DF_SF_CaptureController.apiToUse = 'CIS';
        // Setup data
        String opptyBundleId;
        String state = 'VIC';
        String postcode = '3000';
        String suburbLocality = 'Melbourne';
        String streetName = 'Flinders';
        String streetType = 'ST';
        String streetLotNumber = '21';
        String unitType = '';
        String unitNumber = '';
        String level = '';
        String complexSiteName = '';
        String complexBuildingName = '';
        String complexStreetName = '';
        String complexStreetType = '';
        String complexStreetNumber = '';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response
        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(400, 'Error', '{}', headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByAddress(state, postcode, suburbLocality, streetName, streetType, streetLotNumber, unitType, unitNumber, level, complexSiteName, complexBuildingName, complexStreetName, complexStreetType, complexStreetNumber);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

    @isTest static void test_searchByLatLong_CIS() {

        DF_SF_CaptureController.apiToUse = 'CIS';
        // Setup data
        Id opptyBundleId;
        String latitude = '-33.840213';
        String longitude = '151.207368';

        User commUser;
        commUser = DF_TestData.createDFCommUser();

        // Generate mock response
        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISPositiveJSON(), headerMap);
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, mockObj);

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = DF_SF_CaptureController.searchByLatLong(latitude, longitude);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }  

}