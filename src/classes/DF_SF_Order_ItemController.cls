public with sharing class DF_SF_Order_ItemController {

    @AuraEnabled          
    public static void invokeSave(String dfQuoteId, String dfOrderId, String interfaceType, String ovcType,  String tpidx, String location) {       
        try {   
            resetOVFields(dfOrderId);       
            processUpdateDFQuoteStatus(dfQuoteId);
            updateDF_OrderNonBillable(dfOrderId, interfaceType, ovcType,  tpidx);
            updateOrderJSON(dfOrderId, location);
            invokeOrderValidatorCallout(dfOrderId); 
            //String x = invokeOrderValidatorSynchronousCallout(dfOrderId);                             
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        }
    } 

    public static void resetOVFields(String dfOrderId){
        List<DF_Order__c> orderList = new List<DF_Order__c>();

        orderList = [SELECT   Id, Order_Validation_Status__c,Order_Validator_Response_JSON__c
                                            FROM   DF_Order__c
                                            WHERE  Id = :dfOrderId];  

        if(!orderList.isEmpty()){
            DF_Order__c order = orderList[0];
            order.Order_Validation_Status__c = null;
            order.Order_Validator_Response_JSON__c = null;
            update order;
        }
    } 
 
    @AuraEnabled          
    public static void invokeQuickSave(String dfQuoteId, String dfOrderId, String interfaceType, String ovcType,  String tpidx) {                   
        try {       
            processUpdateDFQuoteStatus(dfQuoteId);
            updateDF_OrderNonBillable(dfOrderId, interfaceType, ovcType,  tpidx);          
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        }
    }    
     
    public static void invokeOrderValidatorCallout(String dfOrderId) {
        try {       
            if (String.isNotEmpty(dfOrderId)) {                         
                DF_OrderValidatorService.validateOrder(dfOrderId);                             
            }      
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        }
    }
    @AuraEnabled
    public static String invokeOrderValidatorSynchronousCallout(String dfOrderId) {
        String validationResponse;
        try {       
            if (String.isNotEmpty(dfOrderId)) {                         
                validationResponse = DF_OrderValidatorService.validateOrderSyncronous(dfOrderId);                             
            }      
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        }
        return validationResponse;
    }

    @AuraEnabled          
    public static String getOrderValidatorResponseData(String dfOrderId) {
        Map<String, String> returnDataMap = new Map<String, String>();

        DF_Order__c dfOrder;
        List<DF_Order__c> dfOrderList;

        String returnDataJson;
        String orderValidationStatus;   
        String orderValidatorResponseJSON;  
        
        try {
            if (String.isNotEmpty(dfOrderId)) {             
                dfOrderList = [SELECT Order_Validation_Status__c,
                                      Order_Validator_Response_JSON__c  
                               FROM   DF_Order__c
                               WHERE  Id = :dfOrderId];                                                
            }
    
            if (!dfOrderList.isEmpty()) {
                dfOrder = dfOrderList[0];   

                if (dfOrder != null) {
                    orderValidationStatus = dfOrder.Order_Validation_Status__c;
                    orderValidatorResponseJSON = dfOrder.Order_Validator_Response_JSON__c;                  
                    returnDataMap.put('orderValidationStatus', orderValidationStatus);                  
                    returnDataMap.put('orderValidatorResponseJSON', orderValidatorResponseJSON);                                                    
                    returnDataJson = JSON.serialize(returnDataMap); 
                }
            }               
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        }
        
        return returnDataJson;
    }   

    @AuraEnabled
    public static void processUpdateDFQuoteStatus(String quoteId) {           
        DF_Quote__c dfQuoteToUpdate;
        List<DF_Quote__c> dfQuotesToUpdateList;

        try {       
            if (String.isNotEmpty(quoteId)) {               
                dfQuotesToUpdateList = [SELECT Status__c
                                        FROM   DF_Quote__c
                                        WHERE  Id = :quoteId
                                        AND    Status__c <> :DF_Constants.QUOTE_STATUS_ORDER_DRAFT];                                                   
            }

            // Perform update       
            if (!dfQuotesToUpdateList.isEmpty()) {
                dfQuoteToUpdate = dfQuotesToUpdateList[0];  

                if (dfQuoteToUpdate != null) {
                    dfQuoteToUpdate.Status__c = DF_Constants.QUOTE_STATUS_ORDER_DRAFT;                                      
                    update dfQuoteToUpdate;                 
                }
            }                          
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        } 
    }    

     @AuraEnabled
    public static String getBasketId(String quoteId) {
        String basketId = DF_ProductController.getBasketId(quoteId);
        return basketId;
    }

    @AuraEnabled
    public static String addOVC(String basketId,String configId, String csa){

        return DF_ProductController.addOVC( basketId, configId,  csa);
    }

    @AuraEnabled
    public static String removeOVC(Id basketId, Id configId, String relatedProductAtt) {
        system.debug('!!!!! removeOVC '+basketId+ ' ' +configId+' ' +relatedProductAtt );
        return DF_ProductController.removeOVC(basketId, configId, relatedProductAtt);
    }


        /*
    }
    * Includes the logic to get the product configuration for Product charges and OVC and other DF Quote related information as a serialized string.
    * Parameters : quote id.
    * @Return : String that says if the execution of functionality was successful. If so, a serialized string is returned
    */
    @AuraEnabled
    public static String getExistingQuickQuoteData(String quoteId) {
        String serializedResponse;
        String bcDefId = DF_CS_API_Util.getCustomSettingValue('SF_BUILD_CONTRIBUTION_DEFINITION_ID');
        if(String.isNotBlank(quoteId) && String.isNotBlank(bcDefId)){
            List<DF_Quote__c> quoteList = [SELECT Id, Name, Fibre_Build_Category__c,RAG__c, Location_Id__c, Address__c, Opportunity__c, LAPI_Response__c, Fibre_Build_Cost__c,
                                            Opportunity__r.Commercial_Deal__c,Opportunity__r.Commercial_Deal__r.Deal_Module_Reference__c,Opportunity__r.Commercial_Deal__r.ETP_Applies__c
                                             FROM DF_Quote__c WHERE Id = :quoteId];
            DF_Quote__c quoteObj = !quoteList.isEmpty() ? quoteList.get(0) : null;
            String oppId = quoteObj != null ? quoteObj.Opportunity__c : null;
            String DealDescription = (quoteObj != null && quoteObj.Opportunity__c !=null && quoteObj.Opportunity__r.Commercial_Deal__c !=null) ? quoteObj.Opportunity__r.Commercial_Deal__r.Deal_Module_Reference__c : null;
            String EtpApplies = (quoteObj != null && quoteObj.Opportunity__c !=null && quoteObj.Opportunity__r.Commercial_Deal__c !=null) ? quoteObj.Opportunity__r.Commercial_Deal__r.ETP_Applies__c : null;
            List<cscfga__Product_Basket__c> baskets = [SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :oppId];
            String basketId = !baskets.isEmpty() ? baskets.get(0) != null ? baskets.get(0).Id : null : null;
            Map<Id, cscfga__Product_Configuration__c> configs = new Map<Id, cscfga__Product_Configuration__c>([Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and cscfga__Product_Definition__c != :bcDefId]);
            if(!configs.isEmpty()){
                //fetches configurations from the product basket
                Map<String,Object> configResponse = cscfga.API_1.getProductConfigurations(new List<Id>(configs.keySet()));
                System.debug('PPPP configResponse: '+configResponse);
                if(quoteObj != null && configResponse != null && String.isNotBlank(basketId))
                    //serialize the information required for quick quote screen
                    //serializedResponse = json.serialize( new DF_QuickQuoteData(quoteObj.Name, quoteObj.Location_Id__c, quoteObj.Address__c, configResponse, basketId, quoteObj.Fibre_Build_Cost__c));
                    serializedResponse = json.serialize( new DF_QuickQuoteData(quoteObj.Name, quoteObj.Location_Id__c, quoteObj.Address__c, configResponse, basketId, quoteObj.Fibre_Build_Cost__c,DealDescription,EtpApplies));
                else
                    serializedResponse = 'Error';
            }
            else
                serializedResponse = 'Error';
        }
        else
            serializedResponse = 'Error';
        System.debug('PPPP serializedResponse: '+serializedResponse);
        return serializedResponse;
    }
        /*
    * Includes the logic to fetch select options values for attributes displayed in quick quote screen.
    * Parameters : basket id, boolean which says if its Product charges/OVC, picklist names list.
    * @Return : String that says if the execution of functionality was successful. If so, all picklist values are returned as a serialized string
    */
    @AuraEnabled
    public static String getOptionsList(String basketId, Boolean pcConfig, List<String> attNameList) {     
        
        String configId;
        String pcDefId = DF_CS_API_Util.getCustomSettingValue('SF_PRODUCT_CHARGE_DEFINITION_ID');
        String ovcDefId = DF_CS_API_Util.getCustomSettingValue('SF_OVC_DEFINITION_ID');
        String optionsResponse;
        Map<String, List<cscfga__Select_Option__c>> optionsMap = new Map<String, List<cscfga__Select_Option__c>>();
        List<cscfga__Select_Option__c> optionsList;
        system.debug('basketId:: ' + basketId);
        system.debug('attNameList:: ' + attNameList);
        system.debug('pcDefId:: ' + pcDefId);
        system.debug('ovcDefId:: ' + ovcDefId);
        if(String.isNotBlank(basketId) && !attNameList.isEmpty() && String.isNotBlank(pcDefId) && String.isNotBlank(ovcDefId)){
            List<cscfga__Product_Basket__c> baskets = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c where id = :basketId];
            cscfga__Product_Basket__c basket = !baskets.isEmpty() ? baskets.get(0) : null;
            //fetch relevant configuration id
            if(pcConfig){
                List<cscfga__Product_Configuration__c> pcConfigList = [Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and cscfga__Product_Definition__c = :pcDefId];
                cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
                configId = configObj != null ? configObj.Id : null;
            }
            else{
                List<cscfga__Product_Configuration__c> ovcConfigList = [Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and cscfga__Product_Definition__c = :ovcDefId];
                cscfga__Product_Configuration__c configObj = !ovcConfigList.isEmpty() ? ovcConfigList.get(0) : null;
                configId = configObj != null ? configObj.Id : null;
            }

            System.debug('YYYY configId:'+configId);
            if(String.isNotBlank(configId)){
                cscfga.API_1.ApiSession apiSession = DF_CS_API_Util.createApiSession(basket);
                apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = configId, cscfga__Product_Basket__c = basketId));        
                cscfga.ProductConfiguration currConfig = apiSession.getConfiguration();
                for(String attName: attNameList){
                    cscfga.Attribute att = apiSession.getAttributeForCurrentConfig(attName);
                    //method to fetch select options values
                    optionsList = att != null ? att.getAvailableOptions() : null;
                    if(optionsList != null)
                        optionsMap.put(attName, optionsList);
                }
                if(optionsMap != null)
                    optionsResponse = JSON.serialize(optionsMap);
                else
                    optionsResponse = 'Error';
            }
            else
                optionsResponse = 'Error';
        }
        else
            optionsResponse = 'Error';
        System.debug('YYYY optionsResponse:'+optionsResponse);
        System.debug('!!!!!!!!! getOptionsList ' + optionsResponse);
        return optionsResponse;
    }
    @AuraEnabled
    public static String getOptionsList(String basketId, List<String> attNameList, List<String> ovcAttrList) { 
        return DF_ProductController.getOptionsList(basketId, attNameList, ovcAttrList);
    }

    @AuraEnabled
     public static String getNonBillableOptionsList() {
        String nonBillableOptionsResponse;
            DF_Order__c objObject = new DF_Order__c();
           
            List<List<String>> pickLists = new List<List<String>> { new List<String>{'Interface Type','Interface_Type__c'},
                                                                    new List<String>{'TPID','TPID__c'},
                                                                    new List<String>{'OVC Type','OVC_Type__c'},
                                                                    new List<String>{'mappingMode','Mapping_Mode__c'},
                                                                    new List<String>{'After BH', 'After_Business_Hours__c'}                                                  
                                                                };
            List < String > allOpts = new list < String > ();
            Schema.sObjectType objType = objObject.getSObjectType();
            Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
            map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();

            Map<String, List<String>> optionsMap = new Map<String, List<String>> ();
            for(Integer i=0; i < 5; i++){
                for(Schema.PicklistEntry pv : fieldMap.get(pickLists[i][1]).getDescribe().getPickListValues()){
                    system.debug('!!!! pv' + pv.getValue());
                    if(optionsMap.containsKey(pickLists[i][0])){
                            List<String> name = optionsMap.get(pickLists[i][0]);
                            name.add(pv.getValue());
                            optionsMap.put(pickLists[i][0],name);
                        } 
                        else{
                            optionsMap.put(pickLists[i][0],new List<String>{pv.getValue()});
                        }
                    }
                }

            if(optionsMap.size()==0){
                nonBillableOptionsResponse = 'Error';
            }
            else{
                nonBillableOptionsResponse = JSON.serialize(optionsMap);
            }

            System.debug('!!!!!!! ' + optionsMap);
            System.debug('!!!!!!! nonBillableOptionsResponse '+nonBillableOptionsResponse);
            return nonBillableOptionsResponse;
     }
/*
    * Includes the logic to update Hidden_OVC_Charges as JS rules do not execute in custom UI.
    * Parameters : basket id, product configuration id, total recurring charge of all OVCs.
    * @Return : String that says if the execution of functionality was successful. If so, Recuuring charge value in the parent is returned
    */
    @AuraEnabled
    public static String updateHiddenOVCCharges(String basketId,String configId, Decimal totalOVC, Decimal totalCoSBw, Decimal totalOVCCosCharges) {
        String result;
        if(String.isNotBlank(basketId) && String.isNotBlank(configId) && totalOVC != null){
            cscfga__Product_Basket__c basket = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c where id = :basketId];
            cscfga.API_1.ApiSession apiSession = DF_CS_API_Util.createApiSession(basket);
            apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = configId, cscfga__Product_Basket__c = basketId));        
            cscfga.ProductConfiguration currConfig = apiSession.getConfiguration();
            
            for(cscfga.Attribute att : currConfig.getAttributes()) {
                System.debug('Attribute name is : ' + att);
                if(att.getName() == 'Hidden_OVC_Charges'){
                    att.setValue(String.valueOf(totalOVC)); 
                }
                if(att.getName() == 'Hidden_Total_OVC_BWs'){
                    system.debug('bandwith Attribute--'+totalCoSBw);
                    att.setValue(String.valueOf(totalCoSBw));
                }
                if(att.getName() == 'TotalOVCCoSChargesNew'){
                    system.debug('TotalOVCCoSChargesNew-'+totalOVCCosCharges);
                    att.setValue(String.valueOf(totalOVCCosCharges));
                }
            }
            apiSession.executeRules(); 
            apiSession.validateConfiguration();
            apiSession.persistConfiguration();
            
            //return the final recurring charge 
            for(cscfga.Attribute att : currConfig.getAttributes()) {
                System.debug('Attribute name is : ' + att);
                if(att.getName() == 'Recurring charge'){
                    result = att.getValue(); 
                }
            }
        }
        else
            result = 'Error';
        return result;   
    }

    @AuraEnabled
    public static String updateProduct(String basketId, String configs, String attributes) {
        return DF_ProductController.updateProduct(basketId, configs, attributes);
    }

    @AuraEnabled /*,*/
    public static void updateDF_OrderNonBillable(String dfOrderId, String interfaceType, String ovcType,  String tpidx ) {
        DF_Order__c orderToUpdate;
        List<DF_Order__c> orderList;
        system.debug('!!!!!!!' + tpidx);
        try {       
            if (String.isNotEmpty(dfOrderId)) {               
                orderList = [SELECT Id, Interface_Type__c,OVC_Type__c,TPID__c
                                        FROM   DF_Order__c
                                        WHERE  Id = :dfOrderId];                                                   
            }

            // Perform update       
            if (!orderList.isEmpty()) {
                orderToUpdate = orderList[0];  

                if (orderToUpdate != null) {
                    orderToUpdate.Interface_Type__c = interfaceType=='Select'?'':interfaceType;
                    orderToUpdate.OVC_Type__c = ovcType =='Select'?'':ovcType;
                    orderToUpdate.TPID__c = tpidx  =='Select'?'':''+tpidx;                                 
                    update orderToUpdate;                 
                }
            }                          
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        } 

    }
    @AuraEnabled
     public static String getNonBillableOptionsValues(String dfOrderId) {
        String nonBillableOptionsResponse;
        DF_Order__c objObject = new DF_Order__c();  
        DF_Order__c order;
        List<DF_Order__c> orderList;
        String retVal = '';

        try {       
            if (String.isNotEmpty(dfOrderId)) {               
                orderList = [SELECT Interface_Type__c,OVC_Type__c,TPID__c,After_Business_Hours__c 
                                        FROM   DF_Order__c
                                        WHERE  Id = :dfOrderId];                                                   
            }
            if (!orderList.isEmpty()) {
                order = orderList[0];  
                retVal = '{ "Interface Type":["' +order.Interface_Type__c + '"], "TPID":["'+order.TPID__c+'"],"OVC Type":["'+order.OVC_Type__c+'"], "After BH":["'+order.After_Business_Hours__c+'"]}' ;
            }


        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        } 
        return retVal; 
    }

    @AuraEnabled
     public static String getNonBillableOVCValues(String  dfOrderId) {
        String jsonRetVal = '';

        DF_Order__c objObject = new DF_Order__c();  
        DF_Order__c order;
        List<DF_Order__c> orderList;
        

        try {       
            if (String.isNotEmpty(dfOrderId)) {               
                orderList = [SELECT     OVC_NonBillable__c
                                        FROM   DF_Order__c
                                        WHERE  Id = :dfOrderId];                                                   
            }
            if (!orderList.isEmpty()) {
                order = orderList[0];  
                jsonRetVal = order.OVC_NonBillable__c ;
            }


        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        } 
        return jsonRetVal;
    }
    @AuraEnabled
     public static void setNonBillableOVCValues(String  dfOrderId, String jsonOVCnonBillable) {
        String jsonRetVal = '';

        DF_Order__c objObject = new DF_Order__c();  
        DF_Order__c order;
        List<DF_Order__c> orderList;
        

        try {       
            if (String.isNotEmpty(dfOrderId)) {               
                orderList = [SELECT   Id, OVC_NonBillable__c
                                        FROM   DF_Order__c
                                        WHERE  Id = :dfOrderId];                                                   
            }
            if (!orderList.isEmpty()) {
                order = orderList[0];
                order.OVC_NonBillable__c = jsonOVCnonBillable;
                update order;
            }


        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        } 
    }
    
    @AuraEnabled 
     public static void updateOrderJSON(String dfOrderId, String location)
     {      
            system.debug('!!!!!!!! updateOrderJSON ' + dfOrderId +' '+ location);
    
            DF_Order__c order;
            List<DF_Order__c> orderList;
            String jSONOrder;
            String jSONModOrder;
            
            try {       
                if (String.isNotEmpty(dfOrderId)) {       
    
                    orderList = [SELECT   Id, Heritage_Site__c, Induction_Required__c, Security_Required__c,
                                 Site_Notes__c, Trading_Name__c, OrderType__c, Order_Status__c, Order_SubType__c,
                                 DF_Quote__r.Opportunity__c 
                                            FROM   DF_Order__c
                                            WHERE  Id = :dfOrderId];     
                    if(orderList[0].OrderType__c=='Connect') {
                        jSONOrder = DF_OrderJSONDataHelper.generateJSONStringForOrder(dfOrderId,location);
                    }else if(orderList[0].OrderType__c=='Modify'){
                        jSONModOrder = DF_ModifyOrderJSONDataHelper.generateJSONStringForOrder(dfOrderId,location);                        
                    }
                    system.debug('!!!! jSONOrder ' +jSONOrder);
                                                          
                }
                if (!orderList.isEmpty()) {
                    order = orderList[0];
                    system.debug('!!!! order' +order);
                    if(orderList[0].OrderType__c=='Connect') {
                        order.Order_JSON__c = jSONOrder;
                    }else if(order.OrderType__c=='Modify'){
                        jSONModOrder = DF_OrderValidatorServiceUtils.stripJsonNulls(jSONModOrder);
                        system.debug('jSONModOrder:: ' + jSONModOrder);
                        if(jSONModOrder!='' && jSONModOrder!=null){
                            jSONOrder = DF_UpdateOrderJSON.updateOrderJSONForModifyOrder(dfOrderId, order.Order_SubType__c, location);
                            order.Modify_Order_JSON__c = jSONModOrder;
                            order.Order_JSON__c = jSONOrder;                            
                        }
					}
                    update order;
                }
    
    
            } catch(Exception e) {
                GlobalUtility.logMessage(DF_Constants.ERROR, DF_SF_Order_ItemController.class.getName(), 'updateOrderJSON', '', '', '', '', e, 0);
                throw new AuraHandledException(e.getMessage());         
            } 
    
     }
     @AuraEnabled
     public static Boolean doesCSADefaultToLocal(string cSAId){
        Boolean retVal = false;
        Integer counter = 0;
        if(String.isNotEmpty(CSAId)){
            DF_POI_Route_Type_Value__mdt[] localRoutes = [SELECT POI__c,Route_Type__c FROM DF_POI_Route_Type_Value__mdt WHERE CSA__c =:cSAId];
            if(localRoutes.size() > 1){
                String poiName = localRoutes[0].POI__c;
                for(DF_POI_Route_Type_Value__mdt poi : localRoutes){
                    if(poi.POI__c == poiName && counter>0){
                        retVal = true;
                    }
                    counter++;
                }
            }
        }
        return retVal;
     }

    @AuraEnabled
    public static String getBundleId(String quoteId) {
        return DF_ProductController.getBundleId(quoteId);
    }
}