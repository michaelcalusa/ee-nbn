@isTest
private class DF_SF_Order_ItemController_Test {
      /**
  * Tests getBasketID method
  */
    @isTest static void getBasketIDMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        Test.startTest();
        List<DF_Quote__c> quoteList = [SELECT Id, RAG__c, Opportunity_Bundle__c, Opportunity__c, LAPI_Response__c FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppId LIMIT 1];
        String result = DF_SF_Order_ItemController.getBasketId(quoteList.get(0).Id);
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
  * Tests getBasketID method for error condition
  */
    @isTest static void getBasketIDMethodErrorTest()
    {
        Test.startTest();
        String result = DF_SF_Order_ItemController.getBasketId('');
        System.assertEquals('Error', result);
        Test.stopTest();
    }

        /**
  * Tests getQuickQuoteData method
  */
    @isTest static void getQuickQuoteDataMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        Test.startTest();
        List<DF_Quote__c> quoteList = [SELECT Id, RAG__c, Opportunity_Bundle__c, Opportunity__c, LAPI_Response__c FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppId LIMIT 1];
        String result = DF_SF_Order_ItemController.getExistingQuickQuoteData(quoteList.get(0).Id);
        System.assertNotEquals('', result);
        Test.stopTest();
    }

        /**
  * Tests getOptionsList method
  */
    @isTest static void getOptionsListMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        List<String> attNameList = new List<String>{'Term'};
        Test.startTest();
        String result = DF_SF_Order_ItemController.getOptionsList(basketObj.Id, true, attNameList);
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    //@isTest static void getOptionsListMethodTest2()
    //{
    //    Id oppId = DF_TestService.getServiceFeasibilityRequest();
    //    oppId = DF_TestService.getQuoteRecords();
    //    Account acc = DF_TestData.createAccount('Test Account');
    //    insert acc;
    //    cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
    //    Opportunity opp = DF_TestData.createOpportunity('Test Opp');
    //    opp.Opportunity_Bundle__c = oppId;
    //    insert opp;
    //    List<String> attNameList = new List<String>{'Term'};
    //    Test.startTest();
    //    String result = DF_SF_Order_ItemController.getOptionsList(basketObj.Id,attNameList);
    //    System.assertNotEquals('', result);
    //    Test.stopTest();
    //}
        /**
  * Tests getOptionsList method error condition
  */
    @isTest static void getOptionsListMethodErrorTest()
    {
        List<String> attNameList = new List<String>{'Term'};
        Test.startTest();
        String result = DF_SF_Order_ItemController.getOptionsList('', true, attNameList);
        System.assertEquals('Error', result);
        Test.stopTest();
    }
  @isTest static void getNonBillableOptionsListMethodTest()
    {
      Test.startTest();
      String result = DF_SF_Order_ItemController.getNonBillableOptionsList();
      System.assertNotEquals('', result);
      Test.stopTest();

    }
  
    @isTest static void test_Invoke() {                 
      String response;
        User commUser;
        String res ; 
        Account acc = new Account(recordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId(), Tier__c = '1', Name='test nbn', Access_Seeker_ID__c = 'AS00000011');
        insert acc;

        Group gr = new Group();
        gr=EE_DataSharingUtility.createNewGroup(DF_Constants.NBN_SELECT_GROUP_NAME+acc.Id);
        system.debug('!!!!group: '+gr);


        
        Map<String, String> mapAttributes = new Map<String, String>();
        mapAttributes.put('User.UserRoleId','AS00000011');
        mapAttributes.put('User.FederationIdentifier','testBSM_1');
        mapAttributes.put('User.LastName','nbnRSPTest_1');
        mapAttributes.put('User.FirstName','nbnRSPTest_1');
        mapAttributes.put('User.Email','testuser_1@nbnco.com.au.nbnRsp');
        mapAttributes.put('User.ProfileId','Enterprise Ethernet Plus,Access Seeker nbnSelect Feasibility');
        
        JITHandler handler = new JITHandler();
        //Create User
        
        commUser = handler.createUser(null,null,null,'testSAML',mapAttributes,null);
        
        //// Create acct
        //Account acct = DF_TestData.createAccount('Test Account');
        //insert acct;
                system.runAs(commUser) { 
            test.startTest(); 
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acc.Id);
        insert opptyBundle;
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC000233782131', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.Order_GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55bada';
        insert dfQuote;
        String loc = '{"status":"Order Draft","SCR":"1316.0000","SCNR":"5000.0000","quoteId":"EEQ-0000000123","orderId":"a7I5D000000CawIUAS","oppBundleName":"EEB-0000000195","locId":"LOC000006231845","id":"a6a5D000000CcKkQAK","FBC":0,"address":"6 LENNOX ST RAVENSWOOD TAS 7250 Australia","OrderId":"a7I5D000000CawIUAS","basketId":"a1e5D000000GPE4QAO","UNI":[{"BTDType":"Standard","interfaceBandwidth":"1Gbps","AAT":"","SLA":"Premium - 12 (24/7)","term":"12","zone":"Zone 3","AHA":"No","coSRecurring":"0","oVCType":"Access EVPL","interfaceTypes":"Optical (Single mode)","tPID":"0x8100","Id":"a1h5D000000DPlnQAG"}],"OVCs":[{"OVCId":"a1h5D000000DPloQAG","OVCName":"OVC 1","CSA":"CSA180000002222","monthlyCombinedCharges":"516.0000","routeType":"Local","coSHighBandwidth":"70","coSMediumBandwidth":"0","coSLowBandwidth":"0","routeRecurring":"0.0000","coSRecurring":"516.0000","POI":"1-MEL-MELBOURNE-MP","status":"Incomplete","mappingMode":"","NNIGroupId":"NNI123456789123","sTag":"8","ceVlanId":"8","ovcMaxFrameSize":"Jumbo (9000 Bytes)"}],"prodChargeConfigId":"a1h5D000000DPlnQAG","ovcConfigId":"a1h5D000000DPloQAG"}';
        // Build order json
        String dfOrderJSONString = DF_IntegrationTestData.buildDFOrderJSONString();
        
        // Create DF Order
        DF_Order__c dfOrder = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        dfOrder.Order_JSON__c = dfOrderJSONString;
        dfOrder.DF_Quote__c = dfQuote.Id;
        insert dfOrder;
         DF_SF_Order_ItemController.invokeSave(dfQuote.Id,dfOrder.id,'Optical (Multi mode)','Access EPL','0x8100',loc);

         DF_SF_Order_ItemController.invokeQuickSave(dfQuote.Id,dfOrder.id,'Optical (Multi mode)','Access EPL','0x8100');

            test.stopTest();                                             
        } 

    }   

    @isTest static void test_getNonBillableOptionsValues(){

    	   User commUser;
        String res ; 
        Account acc = new Account(recordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId(), Tier__c = '1', Name='test nbn', Access_Seeker_ID__c = 'AS00000011');
        insert acc;

        Group gr = new Group();
        gr=EE_DataSharingUtility.createNewGroup(DF_Constants.NBN_SELECT_GROUP_NAME+acc.Id);
        system.debug('!!!!group: '+gr);


        
        Map<String, String> mapAttributes = new Map<String, String>();
        mapAttributes.put('User.UserRoleId','AS00000011');
        mapAttributes.put('User.FederationIdentifier','testBSM_1');
        mapAttributes.put('User.LastName','nbnRSPTest_1');
        mapAttributes.put('User.FirstName','nbnRSPTest_1');
        mapAttributes.put('User.Email','testuser_1@nbnco.com.au.nbnRsp');
        mapAttributes.put('User.ProfileId','Enterprise Ethernet Plus,Access Seeker nbnSelect Feasibility');
        
        JITHandler handler = new JITHandler();
        //Create User
        
        commUser = handler.createUser(null,null,null,'testSAML',mapAttributes,null);
        
        //// Create acct
        //Account acct = DF_TestData.createAccount('Test Account');
        //insert acct;
                system.runAs(commUser) { 
            test.startTest(); 
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acc.Id);
        insert opptyBundle;
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC000233782131', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.Order_GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55bada';
        insert dfQuote;
        
        // Create DF Order
        DF_Order__c dfOrder = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');

        dfOrder.OVC_NonBillable__c = 'xxx';
        dfOrder.Interface_Type__c = 'Optical (Multi mode)';
        dfOrder.OVC_Type__c = 'Access EPL';
        dfOrder.TPID__c = '0x8100';
        dfOrder.After_Business_Hours__c = 'Yes'; //CPST-5509
        dfOrder.Order_Validation_Status__c ='Success';
        dfOrder.Order_Validator_Response_JSON__c = '{"validationErrors": [],"status": "success"}';
        dfOrder.DF_Quote__c = dfQuote.Id;
        insert dfOrder;
         DF_SF_Order_ItemController.getNonBillableOptionsValues(dfOrder.Id);
         DF_SF_Order_ItemController.getNonBillableOVCValues(dfOrder.Id);
     	DF_SF_Order_ItemController.getOrderValidatorResponseData(dfOrder.Id);
     	DF_SF_Order_ItemController.doesCSADefaultToLocal('CSA200000000153');
     	DF_SF_Order_ItemController.setNonBillableOVCValues(dfOrder.Id, '[{"OVCId":"a1h5D0000006dPsQAI","CSA":"CSA180000002222","NNIGroupId":"NNI123123123123","routeType":"Local","coSHighBandwidth":"10","coSMediumBandwidth":"20","coSLowBandwidth":"30","routeRecurring":"0.00","coSRecurring":"0.00","ovcMaxFrameSize":"Jumbo (9000 Bytes)","status":"Incomplete","sTag":"0","ceVlanId":"8","mappingMode":"DSCP"}]');
            test.stopTest();                                             
        } 

    }
        
}