@isTest
private class DF_SF_QuoteController_Test {

    @isTest static void test_getQuoteRecords() {
        // Setup data                           
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = DF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'B');         
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuote1.Fibre_Build_Category__c = 'B';
        dfQuote1.QuoteType__c = 'Connect';
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;               
        
        // Create Product Definition
        cscfga__Product_Definition__c prodDef = DF_TestData.buildProductDefinition('Direct Fibre - Product Charges', '');
        insert prodDef;
        
        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig = DF_TestData.buildProductConfig(prodDef.Id);
        prodConfig.Name = 'Direct Fibre - Product Charges';
        prodConfig.cscfga__Product_Basket__c = prodBasket.Id;
        insert prodConfig;
        
        Test.startTest();
        List<DF_Quote__c> dfQuoteList2 = DF_SF_QuoteController.getQuoteRecords(opptyBundle.Id);
        
        Test.stopTest();
        
        // Assertions
        system.AssertEquals(false, dfQuoteList2.isEmpty());        
    }

    @isTest static void test_buildDFQuoteIdSet() {
        // Setup data                           
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = DF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'B');     
        dfQuote1.Proceed_to_Pricing__c = true;  
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;               
        
        // Create Product Definition
        cscfga__Product_Definition__c prodDef = DF_TestData.buildProductDefinition('Direct Fibre - Product Charges', '');
        insert prodDef;
        
        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig = DF_TestData.buildProductConfig(prodDef.Id);
        prodConfig.Name = 'Direct Fibre - Product Charges';
        prodConfig.cscfga__Product_Basket__c = prodBasket.Id;
        insert prodConfig;
        
        Test.startTest();

        Set<String> dfQuoteIdSet = DF_SF_QuoteController.buildDFQuoteIdSet(dfQuoteList);
        
        Test.stopTest();
        
        // Assertions
        system.AssertEquals(false, dfQuoteIdSet.isEmpty());        
    }

    @isTest static void test_getOrderSummaryData() {
        // Setup data               
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = DF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'B');  
        dfQuote1.Proceed_to_Pricing__c = true;      
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;               
        
        // Create Product Definition
        cscfga__Product_Definition__c prodDef = DF_TestData.buildProductDefinition('Direct Fibre - Product Charges', '');
        insert prodDef;
        
        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig = DF_TestData.buildProductConfig(prodDef.Id);
        prodConfig.Name = 'Direct Fibre - Product Charges';
        prodConfig.cscfga__Product_Basket__c = prodBasket.Id;
        insert prodConfig;
        
        Test.startTest();
        
        String serializedList = DF_SF_QuoteController.getOrderSummaryData(opptyBundle.Id);
        
        Test.stopTest();

         // Assertions
        system.AssertEquals(false, String.isEmpty(serializedList));
    }
    
    @isTest static void test_getQuoteCosts() {
        // Setup data       
        Set<String> dfQuoteIdSet = new Set<String>();
        Map<String, Decimal> recurringCostsMap = new Map<String, Decimal>();
        Map<String, Decimal> nonRecurringCostsMap = new Map<String, Decimal>();             
                
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = DF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'B');     
        dfQuote1.Proceed_to_Pricing__c = true;  
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;               
        
        // Create Product Definition
        cscfga__Product_Definition__c prodDef = DF_TestData.buildProductDefinition('Direct Fibre - Product Charges', '');
        insert prodDef;
        
        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig = DF_TestData.buildProductConfig(prodDef.Id);
        prodConfig.Name = 'Direct Fibre - Product Charges';
        prodConfig.cscfga__Product_Basket__c = prodBasket.Id;
        insert prodConfig;
        
        // Create attribute
        cscfga__Attribute__c prodAttr = DF_TestData.buildAttribute('Recurring charge','570.8',prodConfig.Id);
        insert prodAttr;
        
        dfQuoteIdSet.add(dfQuote1.Id);
        
        Test.startTest();

        DF_SF_QuoteController.getQuoteCosts(dfQuoteIdSet, recurringCostsMap, nonRecurringCostsMap);
        
        Test.stopTest();
    } 
    
    @isTest static void test_getQuoteCostsForModify() {
        // Setup data       
        Set<String> dfQuoteIdSet = new Set<String>();
        Map<String, Decimal> recurringCostsMap = new Map<String, Decimal>();
        Map<String, Decimal> nonRecurringCostsMap = new Map<String, Decimal>();
        Map<String, Decimal> modificationCostsMap = new Map<String, Decimal>();             
                
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = DF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'B');     
        dfQuote1.Proceed_to_Pricing__c = true;  
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;               
        
        // Create Product Definition
        cscfga__Product_Definition__c prodDef = DF_TestData.buildProductDefinition('Direct Fibre - Product Charges', '');
        insert prodDef;
        
        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig = DF_TestData.buildProductConfig(prodDef.Id);
        prodConfig.Name = 'Direct Fibre - Product Charges';
        prodConfig.cscfga__Product_Basket__c = prodBasket.Id;
        insert prodConfig;
        
        // Create attribute
        cscfga__Attribute__c prodAttr = DF_TestData.buildAttribute('Recurring charge','570.8',prodConfig.Id);
        insert prodAttr;
        
        dfQuoteIdSet.add(dfQuote1.Id);
        
        Test.startTest();

        DF_SF_QuoteController.getQuoteCostsForModify(dfQuoteIdSet, recurringCostsMap, nonRecurringCostsMap, modificationCostsMap);
        
        Test.stopTest();
    }
    @isTest
    static void test_stripOffReplaceComma(){
       
        Test.startTest();
        String result = DF_SF_QuoteController.stripOffReplaceComma('VlanId','asdf,asdf,asdf');
         System.assertEquals(result, 'asdf asdf asdf');
        Test.stopTest();
    }

    @isTest
    static void test_bulkCol(){
                // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;
        //User commUser;
       // commUser = DF_TestData.createDFCommUserMyAccount();
        // Setup data                           
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

         Test.startTest();
            List<String> columns = DF_SF_QuoteController.bulkCol( opptyBundle.Id);
            //System.assertNotEquals('', oppBundleId);
            Test.stopTest();

    }
    @isTest
    static void test_createDFORder(){
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = DF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'B');         
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuote1.Fibre_Build_Category__c = 'B';
        dfQuote1.QuoteType__c = 'Connect';
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;       

         Test.startTest();
           string orderID= DF_SF_QuoteController.createDF_Order( dfQuoteList[0].Id);
        System.assertNotEquals(null, orderID);
        Test.stopTest();

    }
    
    @isTest static void test_searchOrderSummaryData() {
        // Setup data
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('EEQ-000000001');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = DF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'B');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;

        // Create Product Definition
        cscfga__Product_Definition__c prodDef = DF_TestData.buildProductDefinition('Direct Fibre - Product Charges', '');
        insert prodDef;

        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig = DF_TestData.buildProductConfig(prodDef.Id);
        prodConfig.Name = 'Direct Fibre - Product Charges';
        prodConfig.cscfga__Product_Basket__c = prodBasket.Id;
        insert prodConfig;

        Test.startTest();

        // bundle name
        String serializedList = DF_SF_QuoteController.searchOrderSummaryData(opptyBundle.Name);

        // quote name
        String serializedList2 = DF_SF_QuoteController.searchOrderSummaryData(oppty.Name);

        Test.stopTest();
        //debug
        System.debug('Bundle Name: ' + opptyBundle.Name);
        System.debug('Bundle id: ' + opptyBundle.id);
        System.debug(serializedList);
        System.debug('Quote Id: ' + oppty.id);
        System.debug('Quote Name: ' + oppty.Name);
        System.debug(serializedList2);

        // Assertions
        system.AssertEquals(false, String.isEmpty(serializedList));
        system.AssertEquals(false, String.isEmpty(serializedList2));
    }

    @IsTest
    static void testSaveCSV() {
        // Setup data
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('EEQ-000000001');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'B');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuote1.Status__c = 'Error';
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;

        String csvContent = 'LocationId,QuoteId,Address,AfterHoursSiteVisit,UNIInterfaceTypes,UNIOVCType,UNITPID,UNITerm,UNIServiceRestorationSLA,OVC1RouteType,OVC1NNIGroupId,OVC1SVLANId,OVC1UNIVLANId,OVC1CoSHigh,OVC1CoSMedium,OVC1CoSLow,OVC1MappingMode,OVC2RouteType,OVC2NNIGroupId,OVC2SVLANId,OVC2UNIVLANId,OVC2CoSHigh,OVC2CoSMedium,OVC2CoSLow,OVC2MappingMode,OVC3RouteType,OVC3NNIGroupId,OVC3SVLANId,OVC3UNIVLANId,OVC3CoSHigh,OVC3CoSMedium,OVC3CoSLow,OVC3MappingMode,OVC4RouteType,OVC4NNIGroupId,OVC4SVLANId,OVC4UNIVLANId,OVC4CoSHigh,OVC4CoSMedium,OVC4CoSLow,OVC4MappingMode,OVC5RouteType,OVC5NNIGroupId,OVC5SVLANId,OVC5UNIVLANId,OVC5CoSHigh,OVC5CoSMedium,OVC5CoSLow,OVC5MappingMode,OVC6RouteType,OVC6NNIGroupId,OVC6SVLANId,OVC6UNIVLANId,OVC6CoSHigh,OVC6CoSMedium,OVC6CoSLow,OVC6MappingMode,OVC7RouteType,OVC7NNIGroupId,OVC7SVLANId,OVC7UNIVLANId,OVC7CoSHigh,OVC7CoSMedium,OVC7CoSLow,OVC7MappingMode,OVC8RouteType,OVC8NNIGroupId,OVC8SVLANId,OVC8UNIVLANId,OVC8CoSHigh,OVC8CoSMedium,OVC8CoSLow,OVC8MappingMode,BusinessCon1FirstName,BusinessCon1SurName,BusinessCon1Role,BusinessCon1Email,BusinessCon1StreetNumber,BusinessCon1StreetName,BusinessCon1Number,BusinessCon1StreetType,BusinessCon1Suburb,BusinessCon1State,BusinessCon1PostCode,SiteCon1FirstName,SiteCon1SurName,SiteCon1Email,SiteCon1Number,BusinessCon2FirstName,BusinessCon2SurName,BusinessCon2Role,BusinessCon2Email,BusinessCon2Number,BusinessCon2StreetNumber,BusinessCon2StreetName,BusinessCon2Suburb,BusinessCon2StreetType,BusinessCon2State,BusinessCon2PostCode,SiteCon2FirstName,SiteCon2SurName,SiteCon2Email,SiteCon2Number,TradingName,HeritageSite,InductionRequired,SecurityRequired,SiteNotes,CustomerRequiredDate,NTDMounting,PowerSupply1,PowerSupply2,InstallationNotes,SalesforceDFQId';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000005972574,EEQ-0000001241,HILLSIDE NURSING HOME  UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290,Yes,Optical (Single mode),Access EVPL,0x88a8,12 months,Premium - 12 (24/7),Local,NNI123456789123,4,12,500,100,300,DSCP,Local,NNI123456789123,4,16,300,100,,PCP,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,fname1,surname1,business contact,test@email.com,12,Queen,412345678,Apartment,Melb,VIC,3000,test,test1,test@email.com,412345678,fname,surname11,business contact11,test@email.com,412345678,12,Queen,Mel,Unit,VIC,3000,test,test,test@email.com,234534545,trade,herit,yes,yes,,5/04/2019,Rack,AC (240V),DC(-48V to -72V),,a6a5D0000004PGkQAM';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000096895622,EEQ-0000001242,HILLSIDE NURSING HOME  UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290,No,Optical (Single mode),,0x88a8,12 months,Premium - 12 (24/7),Local,NNI123456789123,6,34,300,,,,Local,NNI123456789123,4,16,300,,,,Local,NNI123456789123,4,16,300,,,,Local,NNI123456789123,4,16,300,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,fname2,surname2,business contact,,12,Queen,412345678,Apartment,Melb,VIC,3000,test,test,test@email.com,412345678,fname,surname,business contact,test@email.com,412345678,12,Queen,Mel,Unit,VIC,3000,test2,test2,,,test,test,No,No,,5/04/2019,Rack,AC (240V),DC(-48V to -72V),,'+dfQuote1.Id;
        Blob blobContents = Blob.valueOf(csvContent);
        String  strBase64Contents = EncodingUtil.base64Encode(blobContents);
        String base64Data = EncodingUtil.urlDecode(strBase64Contents, 'UTF-8');
        test.startTest();
        String saveCSVString = DF_SF_QuoteController.saveCSV(opptyBundle.Id, 'BulkOrderUpload_EEB-0000002425', base64Data, 'application/vnd.ms-excel', '');
        test.stopTest();
        system.assert(saveCSVString != null);
    }

    @IsTest
    static void testAppendToFile() {
        // Setup data
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('EEQ-000000001');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'B');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuote1.Status__c = 'Error';
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;

        String csvContent = 'LocationId,QuoteId,Address,AfterHoursSiteVisit,UNIInterfaceTypes,UNIOVCType,UNITPID,UNITerm,UNIServiceRestorationSLA,OVC1RouteType,OVC1NNIGroupId,OVC1SVLANId,OVC1UNIVLANId,OVC1CoSHigh,OVC1CoSMedium,OVC1CoSLow,OVC1MappingMode,OVC2RouteType,OVC2NNIGroupId,OVC2SVLANId,OVC2UNIVLANId,OVC2CoSHigh,OVC2CoSMedium,OVC2CoSLow,OVC2MappingMode,OVC3RouteType,OVC3NNIGroupId,OVC3SVLANId,OVC3UNIVLANId,OVC3CoSHigh,OVC3CoSMedium,OVC3CoSLow,OVC3MappingMode,OVC4RouteType,OVC4NNIGroupId,OVC4SVLANId,OVC4UNIVLANId,OVC4CoSHigh,OVC4CoSMedium,OVC4CoSLow,OVC4MappingMode,OVC5RouteType,OVC5NNIGroupId,OVC5SVLANId,OVC5UNIVLANId,OVC5CoSHigh,OVC5CoSMedium,OVC5CoSLow,OVC5MappingMode,OVC6RouteType,OVC6NNIGroupId,OVC6SVLANId,OVC6UNIVLANId,OVC6CoSHigh,OVC6CoSMedium,OVC6CoSLow,OVC6MappingMode,OVC7RouteType,OVC7NNIGroupId,OVC7SVLANId,OVC7UNIVLANId,OVC7CoSHigh,OVC7CoSMedium,OVC7CoSLow,OVC7MappingMode,OVC8RouteType,OVC8NNIGroupId,OVC8SVLANId,OVC8UNIVLANId,OVC8CoSHigh,OVC8CoSMedium,OVC8CoSLow,OVC8MappingMode,BusinessCon1FirstName,BusinessCon1SurName,BusinessCon1Role,BusinessCon1Email,BusinessCon1StreetNumber,BusinessCon1StreetName,BusinessCon1Number,BusinessCon1StreetType,BusinessCon1Suburb,BusinessCon1State,BusinessCon1PostCode,SiteCon1FirstName,SiteCon1SurName,SiteCon1Email,SiteCon1Number,BusinessCon2FirstName,BusinessCon2SurName,BusinessCon2Role,BusinessCon2Email,BusinessCon2Number,BusinessCon2StreetNumber,BusinessCon2StreetName,BusinessCon2Suburb,BusinessCon2StreetType,BusinessCon2State,BusinessCon2PostCode,SiteCon2FirstName,SiteCon2SurName,SiteCon2Email,SiteCon2Number,TradingName,HeritageSite,InductionRequired,SecurityRequired,SiteNotes,CustomerRequiredDate,NTDMounting,PowerSupply1,PowerSupply2,InstallationNotes,SalesforceDFQId';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000005972574,EEQ-0000001241,HILLSIDE NURSING HOME  UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290,Yes,Optical (Single mode),Access EVPL,0x88a8,12 months,Premium - 12 (24/7),Local,NNI123456789123,4,12,500,100,300,DSCP,Local,NNI123456789123,4,16,300,100,,PCP,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,fname1,surname1,business contact,test@email.com,12,Queen,412345678,Apartment,Melb,VIC,3000,test,test1,test@email.com,412345678,fname,surname11,business contact11,test@email.com,412345678,12,Queen,Mel,Unit,VIC,3000,test,test,test@email.com,234534545,trade,herit,yes,yes,,5/04/2019,Rack,AC (240V),DC(-48V to -72V),,a6a5D0000004PGkQAM';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'LOC000096895622,EEQ-0000001242,HILLSIDE NURSING HOME  UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290,No,Optical (Single mode),,0x88a8,12 months,Premium - 12 (24/7),Local,NNI123456789123,6,34,300,,,,Local,NNI123456789123,4,16,300,,,,Local,NNI123456789123,4,16,300,,,,Local,NNI123456789123,4,16,300,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,fname2,surname2,business contact,,12,Queen,412345678,Apartment,Melb,VIC,3000,test,test,test@email.com,412345678,fname,surname,business contact,test@email.com,412345678,12,Queen,Mel,Unit,VIC,3000,test2,test2,,,test,test,No,No,,5/04/2019,Rack,AC (240V),DC(-48V to -72V),,'+dfQuote1.Id;
        Blob blobContents = Blob.valueOf(csvContent);
        String  strBase64Contents = EncodingUtil.base64Encode(blobContents);
        String base64Data = EncodingUtil.urlDecode(strBase64Contents, 'UTF-8');
        String saveCSVString = DF_SF_QuoteController.saveCSV(opptyBundle.Id, 'BulkOrderUpload_EEB-0000002425', base64Data, 'application/vnd.ms-excel', '');
        Map<string,Object> saveres =  (Map<string,Object>) JSON.deserializeUntyped(saveCSVString);
        String fileId =(String)saveres.get('fileId');
        test.startTest();
        try{
            String fileIdString = DF_SF_QuoteController.saveCSV(opptyBundle.Id, 'BulkOrderUpload_EEB-0000002425', base64Data, 'application/vnd.ms-excel', fileId);
            system.assert(fileIdString == null);
        } catch (Exception e) {
            system.debug('eee: ' + e.getStackTraceString());
        }

        test.stopTest();
        system.assert(fileId != null);
    }

    @IsTest
    static void testUpdateQuoteStatusInProcess() {
        // Setup data
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('EEQ-000000001');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create DFQuote recs
        List<String> quoteIdList = new List<String>();
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'B');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuote1.Status__c = 'Error';
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;
        quoteIdList.add(dfQuote1.Id);

        test.startTest();
            DF_SF_QuoteController.updateQuoteStatusInProcess(opptyBundle.Id, quoteIdList);
        test.stopTest();
        DF_Quote__c statusProcessing = [Select Id, Status__c from DF_Quote__c LIMIT 1];
        system.assertEquals('Processing', statusProcessing.Status__c);
    }

    @IsTest
    static void testResetQuoteStatus() {
        // Setup data
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        // Create Account
        Account acct = DF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = DF_TestData.createOpportunity('EEQ-000000001');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'B');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuote1.Status__c = 'Processing';
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;

        test.startTest();
        DF_SF_QuoteController.resetDFQuoteErrors(opptyBundle.Id);
        test.stopTest();
        DF_Quote__c statusProcessing = [Select Id, Status__c from DF_Quote__c LIMIT 1];
        system.assertEquals(null, statusProcessing.Status__c);
    }
    
    @isTest static void getEEUserPermissionLevelTest(){
        DF_SF_QuoteController.getEEUserPermissionLevel();
    }
}