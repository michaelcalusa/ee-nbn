public class DF_SF_RAGController {

    static public final String FB_CATEGORY_A = 'A';
    static public final String FB_CATEGORY_B = 'B';
    static public final String FB_CATEGORY_C = 'C';
    
    static public final String READY = 'Ready'; 
    static public final String PENDING = 'Pending';
    static public final String INPROGRESS = 'In Progress';

    @AuraEnabled
    public static void processProceedToPricing(List<String> dfQuoteIdList) {        
        List<DF_Quote__c> dfQuoteToUpdateList = new List<DF_Quote__c>();

        try {               
            // Build dfQuote list to update
            if (!dfQuoteIdList.isEmpty()) {                         
                dfQuoteToUpdateList = [SELECT Proceed_To_Pricing__c
                                       FROM   DF_Quote__c
                                       WHERE  Name IN :dfQuoteIdList
                                       AND    Proceed_To_Pricing__c <> true];
            }

            // Perform updates        
            if (!dfQuoteToUpdateList.isEmpty()) {                               
                for (DF_Quote__c dfQuote : dfQuoteToUpdateList) {           
                    dfQuote.Proceed_To_Pricing__c = true;
                }        

                update dfQuoteToUpdateList;             
            }                          
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());         
        }
    }
    
    @AuraEnabled
    public static Decimal getDbpDiscountValueforSFC(List<String> dfQuoteIdList) {
        List<String> CommercialDealList = new List<String>();
        Decimal SfcOneOffChargeAmount = 0;
        Decimal DiscountedSfcValue = 0;
        Decimal SfcDbpDiscount = 0;
        try{
                system.debug('Enter the DBP Method---'+dfQuoteIdList);
                // Build dfQuote list to update
                if (!dfQuoteIdList.isEmpty()) {  
                    for(DF_Quote__c dfQuote:[Select id,Opportunity__c,Opportunity__r.Commercial_Deal__c,Opportunity__r.Commercial_Deal__r.Name from DF_Quote__c where Name IN: dfQuoteIdList
                                             AND Opportunity__c <> null limit:Limits.getLimitQueryRows()]){
                        
                        if(dfQuote.Opportunity__r.Commercial_Deal__c !=null && dfQuote.Opportunity__r.Commercial_Deal__r.Name !=null){
                            CommercialDealList.add(dfQuote.Opportunity__r.Commercial_Deal__r.Name);
                        }
                    }
                    system.debug('CommercialDealList----'+CommercialDealList);
                    for(cspmb__Price_Item__c priceItem:[Select id,Name,cspmb__One_Off_Charge__c,cspmb__Price_Item_Code__c,Discount_Reference_ID__c,Discount_Value__c
                                                            from cspmb__Price_Item__c limit:Limits.getLimitQueryRows()]){
                            
                        if(string.isNotBlank(priceItem.cspmb__Price_Item_Code__c) && priceItem.cspmb__Price_Item_Code__c.equalsIgnoreCase('Site Feasibility') && priceItem.cspmb__One_Off_Charge__c !=null){
                            SfcOneOffChargeAmount = priceItem.cspmb__One_Off_Charge__c;
                        }
                        if(!CommercialDealList.isEmpty() && CommercialDealList.contains(priceItem.Discount_Reference_ID__c) && priceItem.Discount_Value__c !=null && priceItem.cspmb__Price_Item_Code__c.equalsIgnoreCase('SFC DBP Discount') ){
                            SfcDbpDiscount = Decimal.ValueOf(priceItem.Discount_Value__c);
                        }
                    }
                    if(SfcOneOffChargeAmount >= 0 && SfcDbpDiscount >=0){
                        system.debug('SfcOneOffChargeAmount----'+SfcOneOffChargeAmount);
                        system.debug('SfcDbpDiscount----'+SfcDbpDiscount);
                        DiscountedSfcValue = (SfcOneOffChargeAmount - (SfcOneOffChargeAmount * (SfcDbpDiscount/100)));
                    }   
                    system.debug('DiscountedSfcValue----'+DiscountedSfcValue);
                }
           }catch(Exception e) {
                throw new AuraHandledException(e.getMessage());         
           }
           
           return DiscountedSfcValue;
    }
    
    /*
        * Fetch Opportunity Bundle name for the given parent Opportunity Bundle Id    
        * Parameters : Opportunity Bundle Id.
        * @Return : Returns Opportunity Bundle name
    */
    @AuraEnabled
    public static String getOppBundleName(String oppBundleId) {
        String oppName;
        List<DF_Opportunity_Bundle__c> bundleList = new List<DF_Opportunity_Bundle__c>();
        if(String.isNotEmpty(oppBundleId)){
            bundleList = [SELECT Id, Name FROM DF_Opportunity_Bundle__c where 
                          Id = :oppBundleId ];
            if(!bundleList.isEmpty()){
                oppName = bundleList.get(0) != null ? bundleList.get(0).Name : null;
            }
        }
        return oppName;
    }
    
    /*
        * Processes RAG information and serializes the result to a string      
        * Parameters : Opportunity Bundle Id.
        * @Return : Serialized string for displaying in the lightning UI
    */
    // Builds data for all lists
    @AuraEnabled
    public static String getRAGData(String oppId) {
        List<DF_Quote__c> readyDFQuoteList;
        List<DF_Quote__c> pendingDFQuoteList;
        List<DF_Quote__c> inProgressDFQuoteList;
        List<DF_RAGClass> ragDataList = new List<DF_RAGClass>();
        
        String parentOppBundleName = getOppBundleName(oppId);

        String serializedData;
        Boolean edtTogOff = true;

        try {           
            // List 1
            readyDFQuoteList = getReadyQuoteRecords(oppId);
            // List 2
            pendingDFQuoteList = getPendingQuoteRecords(oppId); 
            // List 3
            inProgressDFQuoteList = getInProgressQuoteRecords(oppId);
            String edtToggle = DF_CS_API_Util.getCustomSettingValue('TOGGLE_OFF_ESTIMATED_DELIVERY_TIME');
            if(String.isNotBlank(edtToggle) && (edtToggle).equalsIgnoreCase('false')){
                edtTogOff = false;
            }

            // Process List 1: Ready List
            if (!readyDFQuoteList.isEmpty()) {              
                for (DF_Quote__c dfQuote : readyDFQuoteList) {                  
                    // Add new entry to the output list
                    DF_RAGClass rc = new DF_RAGClass(dfQuote.Name, dfQuote.Location_Id__c, dfQuote.Address__c, dfQuote.Fibre_Build_Category__c, dfQuote.Fibre_Build_Cost__c, DF_SF_RAGController.READY);
                    rc.oppBundleName = parentOppBundleName;
                    rc.EDT = dfQuote.Estimated_Delivery_Time_Business_Days__c;
                    rc.edtToggleOff = edtTogOff;
                    ragDataList.add(rc);
                }
            }
    
            // Process List 2: Pending List
            if (!pendingDFQuoteList.isEmpty()) {                
                for (DF_Quote__c dfQuote : pendingDFQuoteList) {                    
                    // Add new entry to the output list
                    DF_RAGClass rc = new DF_RAGClass(dfQuote.Name, dfQuote.Location_Id__c, dfQuote.Address__c, dfQuote.Fibre_Build_Category__c, dfQuote.Fibre_Build_Cost__c, DF_SF_RAGController.PENDING);
                    rc.oppBundleName = parentOppBundleName;
                    rc.EDT = dfQuote.Estimated_Delivery_Time_Business_Days__c;
                    rc.edtToggleOff = edtTogOff;
                    ragDataList.add(rc);
                }   
            }
            
            // Process List 3: In Progress List
            if (!inProgressDFQuoteList.isEmpty()) {             
                for (DF_Quote__c dfQuote : inProgressDFQuoteList) {                 
                    // Add new entry to the output list
                    DF_RAGClass rc = new DF_RAGClass(dfQuote.Name, dfQuote.Location_Id__c, dfQuote.Address__c, dfQuote.Fibre_Build_Category__c, dfQuote.Fibre_Build_Cost__c, DF_SF_RAGController.INPROGRESS);
                    rc.oppBundleName = parentOppBundleName;
                    rc.EDT = dfQuote.Estimated_Delivery_Time_Business_Days__c;
                    rc.edtToggleOff = edtTogOff;
                    ragDataList.add(rc);
                }       
            }        
            
            serializedData = json.serialize(ragDataList);  
            System.debug('PPPPP EDT : '+serializedData);
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        
        return serializedData;
    }

    /*
        * Fetch Quote records for the given Opportunity Bundle Id    
        * Parameters : Opportunity Bundle Id.
        * @Return : Returns a list of DF Quote (custom Quote object for Direct Fibre) records
    */
    // Get DF Quote records for list 1
    public static List<DF_Quote__c> getReadyQuoteRecords(String oppId) {        
        String queryCondition;
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        try {
            if (!String.isEmpty(oppId)) {
                sObject qObject = new DF_Quote__c();
                
                Set<Id> oppIds = new Set<Id>();
                Set<Id> bundleOppIds = new Set<Id>();
                 for(DF_Quote__c q:[SELECT Opportunity__c FROM DF_Quote__c WHERE Opportunity_Bundle__c =:oppId]){
                    bundleOppIds.add(q.Opportunity__c);

                 }

                for(cscfga__Product_Basket__c basket : [SELECT cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c IN :bundleOppIds]){
                    oppIds.add(basket.cscfga__Opportunity__c);
                }

                queryCondition = ' WHERE ((Fibre_Build_Category__c = ' + '\'' + DF_SF_RAGController.FB_CATEGORY_A + '\'' + ') OR (Fibre_Build_Category__c = ' + '\'' + DF_SF_RAGController.FB_CATEGORY_B + '\'' 
                + ' AND Fibre_Build_Cost__c != NULL) OR (Fibre_Build_Category__c = ' + '\'' + DF_SF_RAGController.FB_CATEGORY_C + '\''
                + ' AND Approved__c = true AND Fibre_Build_Cost__c != NULL))'             
                + ' AND Opportunity_Bundle__c = ' + '\'' + oppId + '\'' 
                + ' AND QuoteType__c = ' + '\'' + DF_Constants.QUOTE_TYPE_CONNECT + '\'' 
                + ' AND Opportunity__c != NULL AND Opportunity__c IN :oppIds '
                + ' ORDER BY Name Asc';  
                
                String queryStr = DF_CS_API_Util.getQuery(qObject, queryCondition);                         
                dfQuoteList = Database.query(queryStr);
            }           
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }
        
        return dfQuoteList;
    }
       
    // Get DF Quote records for list 2
    public static List<DF_Quote__c> getPendingQuoteRecords(String oppId) {      
        String queryCondition;
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        try {
            if (!String.isEmpty(oppId)) {
                sObject qObject = new DF_Quote__c();
                
                queryCondition = ' WHERE Fibre_Build_Category__c = ' + '\'' + DF_SF_RAGController.FB_CATEGORY_C + '\'' + ' AND Approved__c = false'
                + ' AND Opportunity_Bundle__c = ' + '\'' + oppId + '\'' + + ' AND QuoteType__c = ' + '\'' + DF_Constants.QUOTE_TYPE_CONNECT + '\'' + ' ORDER BY Name Asc';            
                system.debug('DF_SF_RAGController.getPendingQuoteRecords - queryCondition: ' + queryCondition);
                
                String queryStr = DF_CS_API_Util.getQuery(qObject, queryCondition);             
                dfQuoteList = Database.query(queryStr);  
            }           
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }
        
        return dfQuoteList;
    }       
    
    // Get DF Quote records for list 3
    public static List<DF_Quote__c> getInProgressQuoteRecords(String oppId) {       
        String queryCondition;
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>(); 

        try {
            if (!String.isEmpty(oppId)) {
                sObject qObject = new DF_Quote__c();   

                Set<Id> bundleOppIds = new Set<Id>();
                 for(DF_Quote__c q:[SELECT Opportunity__c FROM DF_Quote__c WHERE Opportunity_Bundle__c =:oppId]){
                    bundleOppIds.add(q.Opportunity__c);

                 }
                
                Set<Id> oppIds = new Set<Id>();
                for(cscfga__Product_Basket__c basket : [SELECT cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c IN :bundleOppIds]){
                    oppIds.add(basket.cscfga__Opportunity__c);
                }//OR Opportunity__c NOT IN :oppIds

                queryCondition = ' WHERE ((Fibre_Build_Category__c = ' + '\'' + DF_SF_RAGController.FB_CATEGORY_A + '\'' + ' AND (Opportunity__c = NULL OR Opportunity__c NOT IN :oppIds)) OR (Fibre_Build_Category__c = ' + '\'' + DF_SF_RAGController.FB_CATEGORY_B + '\'' 
                + ' AND Fibre_Build_Cost__c = NULL) OR (Fibre_Build_Category__c = ' + '\'' + DF_SF_RAGController.FB_CATEGORY_C + '\''
                + ' AND Approved__c = true AND Fibre_Build_Cost__c = NULL))'             
                + ' AND Opportunity_Bundle__c = ' + '\'' + oppId + '\'' 
                + ' AND QuoteType__c = ' + '\'' + DF_Constants.QUOTE_TYPE_CONNECT + '\'' 
                + ' ORDER BY Name Asc';
                
                String queryStr = DF_CS_API_Util.getQuery(qObject, queryCondition);
                dfQuoteList = Database.query(queryStr);  
            }           
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }
        
        return dfQuoteList;
    }
    
    @AuraEnabled
    public static void updateDFQuotesToApprovedAndPublishEvent(List<String> dfQuoteIdList) {
		List<DF_Quote__c> dfQuoteToUpdateList = new List<DF_Quote__c>();
		List<BusinessEvent__e> eventList = new List<BusinessEvent__e>();

        try {               
            // Build dfQuote list to update
            if (!dfQuoteIdList.isEmpty()) {             
                dfQuoteToUpdateList = [SELECT Approved__c,name, Location_Id__c,Address__c,Latitude__c,Longitude__c,
                                       Opportunity__c,Opportunity_Bundle__c, GUID__c,
                                       Opportunity__r.Name,RAG__c,Fibre_Build_Category__c,Opportunity_Bundle__r.name,
                                       Fibre_Build_Cost__c,LAPI_Response__c
                                       FROM   DF_Quote__c
                                       WHERE  Name IN :dfQuoteIdList
                                       AND    Approved__c <> true];
            }

	        // Perform updates        
	        if (!dfQuoteToUpdateList.isEmpty()) {	        	
		        for (DF_Quote__c dfQuote : dfQuoteToUpdateList) {		        	
					dfQuote.Approved__c = true;

					eventList.add(DF_SF_ValidationController.createBusinessEvent((DF_ServiceFeasibilityResponse)JSON.deserialize(dfQuote.LAPI_Response__c, DF_ServiceFeasibilityResponse.class),
								dfQuote,false));
		        }        

		        update dfQuoteToUpdateList;	
                
		        if(eventList.size()>0){
		            List<Database.SaveResult> results = EventBus.publish(eventList);
		            for (Database.SaveResult sr : results) {
		                 if (sr.isSuccess()) {
		                    System.debug('Successfully published event.');
		                } else {
		                       for(Database.Error err : sr.getErrors()) {
		                         System.debug('Error returned: ' + err.getStatusCode() +   ' - ' + err.getMessage());                         
		                       }    
		                }
		            } 
		        }       
	        }	 					   
		} catch(Exception e) {
			throw new AuraHandledException(e.getMessage());			
        }
    }
    
    @AuraEnabled
    public static Boolean hasValidPermissions() {
		String CUSTOM_PERM_NAME = 'DF_Create_Quote';

		Boolean hasValidPerms = false;
		ID userId;

		Map<ID, User> validUserMap = new Map<ID, User>();	
	    Set<Id> permissionSetIdSet = new Set<Id>();

		try {		
			// Get users id
			userId = UserInfo.getUserId();
			
			// Get the list of permission sets that the DF_Create_Quote CustomPermission is associated with
			for (SetupEntityAccess access : [SELECT ParentId 
											 FROM   SetupEntityAccess 
											 WHERE  SetupEntityId IN 
											 (SELECT Id 
											  FROM   CustomPermission 
											  WHERE  DeveloperName = :CUSTOM_PERM_NAME)]) {
				// Add the id of the permission set that the DF_Create_Quote CustomPermission is associated with
				permissionSetIdSet.add(access.ParentId);
			}
			
			if (!permissionSetIdSet.isEmpty()) {
				// Build user map of users who have a Permission Set assigned to them (that is associated to the DF_Create_Quote Custom Permission)
				validUserMap = new Map<ID, User>([SELECT Id, 
								  					     Username 
						   					 	  FROM User 
						   					 	  WHERE Id IN (SELECT AssigneeId 
									    	 			  	   FROM   PermissionSetAssignment
														  	   WHERE  PermissionSetId IN :permissionSetIdSet)]);
			}      

			if (!validUserMap.keySet().isEmpty()) {				
				// Check if users id is found
				if (validUserMap.keySet().contains(userId)) {					
					hasValidPerms = true;
				}
			}	   
		} catch(Exception e) {
			throw new AuraHandledException(e.getMessage());			
        }
               
        return hasValidPerms;
    }

      @AuraEnabled
      public static Boolean hasAppianProcssedRequests(String oppBundleId){
          Boolean allQuotesProcessed = False;
          
          //pre check, if user has permission to proceed to approve charges
          if(getEEUserPermissionLevel() == 1) {
              return false;    
          }
      
        
        System.debug('!!! oppBundleId: ' +oppBundleId);
        //we only want red pending sites - reuse getPendingQuoteRecords
        List<DF_Quote__c> pendingDFQuoteList = getPendingQuoteRecords(oppBundleId); 


      	Set<Id> quoteIds = (new Map<Id,DF_Quote__c>(pendingDFQuoteList)).keySet();
		System.debug('!!! quoteIds: ' +quoteIds);
      	//get quotes in list that were created in the last 14 days
      	List<DF_Quote__c> currentQuotes = [SELECT Id, CreatedDate 
      								FROM DF_Quote__c 
      								WHERE Id IN:quoteIds
      								AND CreatedDate = LAST_N_DAYS:14];
      								System.debug('!!! currentQuotes: ' +currentQuotes);
      	//Set<Id> quoteIds = (new Map<Id,DF_Quote__c>(currentQuotes)).keySet();
      	//Assume a response has been received if count is zero and return true - this will handle cases where items are deleted form Async_Request__c
      	if(currentQuotes.size()==0){
      		allQuotesProcessed = True;
      		return allQuotesProcessed;
      	}

      	List<Async_Request__c> appianCallouts = [SELECT Handler__c,Id,Status__c 
      											FROM Async_Request__c WHERE Params__c IN:quoteIds
      											AND Handler__c =: DF_Constants.APPIAN_REQUEST_HANDLER
      											AND Status__c = : DF_Constants.ASYNC_REQUEST_COMPLETE];
      	System.debug('!!! appianCallouts: ' +appianCallouts);
      	If(appianCallouts.size() > 0){
      		allQuotesProcessed = True;
      	}


      	return allQuotesProcessed;
      }
      
    @AuraEnabled
    public static Decimal getEEUserPermissionLevel() {
        Id userId = UserInfo.getUserId();
        set<String> psName = new set<String>();
        Decimal permissionLevel = 1; //default 1
        for(PermissionSetAssignment psa : [SELECT Id, PermissionSet.Name, AssigneeId FROM PermissionSetAssignment WHERE AssigneeId = :userId]) {
            psName.add(psa.PermissionSet.Name); 
        }
        system.debug('::psName::'+psName);
        for(EE_User_Permission_Level__mdt permissionData : [Select DeveloperName, MasterLabel, Level_of_Permission__c from EE_User_Permission_Level__mdt order by Level_of_Permission__c desc] ) {
           
           if(psName.contains(permissionData.DeveloperName)) {
                permissionLevel = permissionData.Level_of_Permission__c;
                return permissionLevel;
           } 
        }        
        return permissionLevel;
    }      
}