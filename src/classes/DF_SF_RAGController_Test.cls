@isTest
private class DF_SF_RAGController_Test
{

  /**
  * Tests getOppBundleName method
  */
    @isTest static void getOppBundleNameTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        
        Test.startTest();
        String oppName = DF_SF_RAGController.getOppBundleName(oppId);
        System.assertNotEquals('', oppName);
        Test.stopTest();
    }
    
    @isTest static void test_getRAGData() {
        // Setup data               
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        List<DF_Custom_Options__c> csList = DF_TestData.createCustomSettings();
        if(!csList.isEmpty()) {
            insert csList;
        }
        Account acc = DF_TestData.createAccount('My account');
        insert acc;
        DF_Opportunity_Bundle__c oppBundle = DF_TestData.createOpportunityBundle(acc.Id);
        insert oppBundle;

        // Create Ready list recs
        //DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, oppBundle.Id, null, 'Green');
        //DF_Quote__c dfQuote2 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111112', null, oppBundle.Id, 1000, 'Amber');
        
        // Create Ready list recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, oppBundle.Id, null, 'A');
        DF_Quote__c dfQuote2 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111112', null, oppBundle.Id, 1000, 'B');
        
        // Create Pending list recs
        //DF_Quote__c dfQuote3 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111113', null, oppBundle.Id, null, 'Red');
        //DF_Quote__c dfQuote4 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111114', null, oppBundle.Id, null, 'Red');
        
        // Create Pending list recs
        DF_Quote__c dfQuote3 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111113', null, oppBundle.Id, null, 'C');
        DF_Quote__c dfQuote4 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111114', null, oppBundle.Id, null, 'C');

        // Create InProgress list recs
        //DF_Quote__c dfQuote5 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111115', null, oppBundle.Id, null, 'Amber');
        //DF_Quote__c dfQuote6 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111116', null, oppBundle.Id, null, 'Red');    
        
        // Create InProgress list recs
        DF_Quote__c dfQuote5 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111115', null, oppBundle.Id, null, 'B');
        DF_Quote__c dfQuote6 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111116', null, oppBundle.Id, null, 'C'); 
         
        dfQuote4.Approved__c = true;
        
        dfQuoteList.add(dfQuote1);
        dfQuoteList.add(dfQuote2);
        dfQuoteList.add(dfQuote3);
        dfQuoteList.add(dfQuote4);      
        dfQuoteList.add(dfQuote5);
        dfQuoteList.add(dfQuote6);          
        insert dfQuoteList;
        
        Test.startTest();
        
        String serializedList = DF_SF_RAGController.getRAGData(oppBundle.Id);
        
        Test.stopTest();

         // Assertions
        system.AssertEquals(false, String.isEmpty(serializedList));         
    }
    
    @isTest static void test_updateDFQuotesToApprovedAndPublishEvent() {                       
      // Setup data           
      List<String> dfQuoteIdList = new List<String>();
      
      String locId = 'LOC111111111111';
      String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
      String latitude = '-33.840213';
      String longitude = '151.207368';
      Decimal cost = 5555.55;
      String rag = 'Red';

        Account acc = DF_TestData.createAccount('My account');
        insert acc;
        DF_Opportunity_Bundle__c oppBundle = DF_TestData.createOpportunityBundle(acc.Id);
        insert oppBundle;

    Opportunity opp = DF_TestData.createOpportunity('Opp 1');
    insert opp;

       // Generate mock response
    List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp2 = DF_TestData.buildResponseString('LOC000002696724', '42.78931', '147.05578', addressList , 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);
        
      DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, locId, opp.Id, oppBundle.Id, cost, rag, resp2);     
      insert dfQuote1;

        dfQuote1 = [SELECT Name
          FROM   DF_Quote__c
          WHERE  Id = :dfQuote1.Id];
      
      dfQuoteIdList.add(dfQuote1.Name);
      
      test.startTest();           

    DF_SF_RAGController.updateDFQuotesToApprovedAndPublishEvent(dfQuoteIdList);
                                        
        test.stopTest();                                    
        
        // Assertions
        List<DF_Quote__c> existingDFQuoteList = [SELECT Id
                         FROM   DF_Quote__c];

        system.AssertEquals(false, existingDFQuoteList.isEmpty());
    }
    
    @isTest static void test_hasValidPermissions() {                     
      // Setup data           
    Boolean hasValidPerms = false;
      
      test.startTest();           

    hasValidPerms = DF_SF_RAGController.hasValidPermissions();
                                        
        test.stopTest();                                    
        
        // Assertions
        system.AssertEquals(false, hasValidPerms);
    }
    
    @isTest static void test_processProceedToPricing() {                       
      // Setup data           
      List<String> dfQuoteIdList = new List<String>();
      
      String locId = 'LOC111111111111';
      String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
      String latitude = '-33.840213';
      String longitude = '151.207368';
      Decimal cost = 5555.55;
      String rag = 'Red';

        Account acc = DF_TestData.createAccount('My account');
        insert acc;
        DF_Opportunity_Bundle__c oppBundle = DF_TestData.createOpportunityBundle(acc.Id);
        insert oppBundle;

    Opportunity opp = DF_TestData.createOpportunity('Opp 1');
    insert opp;

       // Generate mock response
    List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp2 = DF_TestData.buildResponseString('LOC000002696724', '42.78931', '147.05578', addressList , 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);
        
      DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, locId, opp.Id, oppBundle.Id, cost, rag, resp2);     
      insert dfQuote1;

        dfQuote1 = [SELECT Name
          FROM   DF_Quote__c
          WHERE  Id = :dfQuote1.Id];
      
      dfQuoteIdList.add(dfQuote1.Name);
      
      test.startTest();           

    DF_SF_RAGController.processProceedToPricing(dfQuoteIdList);
                                        
        test.stopTest();                                    
        
        // Assertions
        List<DF_Quote__c> existingDFQuoteList = [SELECT Id
                         FROM   DF_Quote__c
                            WHERE  Proceed_To_Pricing__c = true];                         

        system.AssertEquals(false, existingDFQuoteList.isEmpty());
    }   
    
    @isTest static void test_getDbpDiscountValueforSFC() {                            
        // Setup data               
        List<String> dfQuoteIdList = new List<String>();
        
        String locId = 'LOC111111111111';
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        Decimal cost = 5555.55;
        String rag = 'Red';

        Account acc = DF_TestData.createAccount('My account');
        insert acc;
        DF_Opportunity_Bundle__c oppBundle = DF_TestData.createOpportunityBundle(acc.Id);
        insert oppBundle;
        
        Commercial_Deal__c commercialDeal = DF_TestData.commercialDeal(acc.id);
        insert commercialDeal;

        Opportunity opp = DF_TestData.createOpportunity('Opp 1');
        opp.Commercial_Deal__c = commercialDeal.id;
        insert opp;
         
        cspmb__Price_Item__c pItem = DF_TestData.priceItemRecordSiteFeasibility(); 
        insert pItem;
        
        cspmb__Price_Item__c pItem1 = DF_TestData.priceItemRecordDBP(); 
        pItem1.Discount_Reference_ID__c = commercialDeal.name;
        insert pItem1;   

        // Generate mock response
        List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp2 = DF_TestData.buildResponseString('LOC000002696724', '42.78931', '147.05578', addressList , 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);
        
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, locId, opp.Id, oppBundle.Id, cost, rag, resp2);      
        insert dfQuote1;

        dfQuote1 = [SELECT Name
                    FROM   DF_Quote__c
                    WHERE  Id = :dfQuote1.Id];
        
        dfQuoteIdList.add(dfQuote1.Name);
        
        test.startTest();           

        DF_SF_RAGController.getDbpDiscountValueforSFC(dfQuoteIdList);
                                        
        test.stopTest();                                    
        
        // Assertions
        
        system.AssertEquals(true, pItem1.Id !=null);
    }  

    @isTest static void test_hasAppianProcssedRequests() {   
      // Setup data               
        List<String> dfQuoteIdList = new List<String>();
        
        String locId = 'LOC111111111111';
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        Decimal cost = 5555.55;
        String rag = 'Red';

        Account acc = DF_TestData.createAccount('My account');
        insert acc;
        DF_Opportunity_Bundle__c oppBundle = DF_TestData.createOpportunityBundle(acc.Id);
        insert oppBundle;
        
        Commercial_Deal__c commercialDeal = DF_TestData.commercialDeal(acc.id);
        insert commercialDeal;

        Opportunity opp = DF_TestData.createOpportunity('Opp 1');
        opp.Commercial_Deal__c = commercialDeal.id;
        insert opp;

        // Generate mock response
        List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp2 = DF_TestData.buildResponseString('LOC000002696724', '42.78931', '147.05578', addressList , 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);
        
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, locId, opp.Id, oppBundle.Id, cost, rag, resp2);      
        insert dfQuote1;
            // Create asyncReq
        Async_Request__c asyncReq = DF_TestData.createAsyncRequest(DF_SOACreateBillingEventHandler.HANDLER_NAME, 'Processed', dfQuote1.Id, null, null, 0, 0);
        insert asyncReq; 
        
        Test.setCreatedDate(dfQuote1.Id,DateTime.now());
        test.startTest();           

          Boolean retVal1 =  DF_SF_RAGController.hasAppianProcssedRequests(oppBundle.Id);
                                    
        test.stopTest();        
        system.assertEquals(retVal1,True);



    }
    
    @isTest static void getEEUserPermissionLevelTest(){
        DF_SF_RAGController.getEEUserPermissionLevel();
    }

}