public with sharing class DF_SFtoAppainEventHandler implements Queueable{
    
    private List<BusinessEvent__e> lstToProcess;

    public DF_SFtoAppainEventHandler(List<BusinessEvent__e> evtLst) {       
        lstToProcess = evtLst;
    }

    public void execute(QueueableContext context) {

        Business_Platform_Events__c customSettingValues = Business_Platform_Events__c.getOrgDefaults();       
        
        for(BusinessEvent__e evt : lstToProcess){
            if((evt.Event_Id__c==customSettingValues.DF_SFC_Desktop_Assessment__c)){
                List<String> paramList = new List<String>();
                paramList.add(evt.Event_Record_Id__c);
                system.debug('Salesforce to Appain event received for the Event ID '+evt.Event_Id__c);
                AsyncQueueableUtils.createRequests(DF_AppianSFRequestHandler.HANDLER_NAME, paramList);
                //String evtBody = '{"eventIdentifier":"'+evt.Event_Id__c+'","externalSystemCorrelationId":"'+evt.Event_Record_Id__c+'","eventBody":'+evt.Message_Body__c+'}';
                
                //AppianCallout.SendInformationToAppian(evtBody, '', '');             
            }                       
        }
    }
}