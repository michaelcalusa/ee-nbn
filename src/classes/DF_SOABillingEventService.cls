public class DF_SOABillingEventService {

    public static Id dfOrderId;
    public static DF_Order__c dfOrder;
    public static String chargeType;

    @future(callout=true)
    public static void createBillingEvent(String param) {           
        system.debug('param: ' + param);

        final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;   
        // Headers        
        final String NBN_ENV_OVRD_VAL;
        
        // Errors             
        final String ERROR_MESSAGE = 'A HTTP error occurred in class method DF_SOABillingEventService.createBillingEvent';        

        Http http = new Http();     
        HttpRequest req;              
        HTTPResponse res; 

        String requestStr;        
        String responseBodyStatus;        
        
        Map<String, String> headerMap = new Map<String, String>();

        try {           
            getParamInputs(param);
            system.debug('DF_SOABillingEventService.dfOrderId: ' + DF_SOABillingEventService.dfOrderId);

            // Get dfOrder and its details
            DF_SOABillingEventService.dfOrder = DF_SOABillingEventUtils.getDFOrder(dfOrderId);

            DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
                                                                Timeout__c,
                                                                Prod_EndPoint__c,
                                                                DP_Environment_Override__c
                                                         FROM   DF_Integration_Setting__mdt
                                                         WHERE  DeveloperName = :DF_SOABillingEventUtils.INTEGR_SETTING_NAME_CREATE_BILL_EVT];

            NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
            TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
            NBN_ENV_OVRD_VAL = integrSetting.DP_Environment_Override__c;
            
            system.debug('NAMED_CREDENTIAL: ' + NAMED_CREDENTIAL); 
            system.debug('TIMEOUT_LIMIT: ' + TIMEOUT_LIMIT);
            system.debug('NBN_ENV_OVRD_VAL: ' + NBN_ENV_OVRD_VAL); 
            //
            if(runningInSandbox())
            {
                NamedCredential nc=[select endpoint from NamedCredential where developerName=:NAMED_CREDENTIAL LIMIT 1];
                if(nc.endpoint.tolowercase()==integrSetting.Prod_EndPoint__c.tolowercase())
                {
                    throw new CustomException(DF_IntegrationUtils.ERR_PROD_URL_CONFIGURED);
                }
                
            }
            // Generate Payload                 
            requestStr = DF_SOABillingEventTransformer.generateCreateBillingEventRequest(DF_SOABillingEventService.dfOrder, chargeType);
            //system.debug('Payload request (before stripping json nulls): ' + requestStr);
            system.debug('Payload request: ' + requestStr);

            if (String.isEmpty(requestStr)) {
                throw new CustomException(DF_IntegrationUtils.ERR_REQUEST_BODY_EMPTY);
            }

            //requestStr = DF_IntegrationUtils.stripJsonNulls(requestStr);
            //system.debug('Payload request (after stripping json nulls): ' + requestStr);

            // Build endpoint
            END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL;
            system.debug('END_POINT: ' + END_POINT);     

            headerMap.put(DF_IntegrationUtils.NBN_ENV_OVRD, NBN_ENV_OVRD_VAL);

            DF_SOABillingEventUtils.getHeaders(DF_SOABillingEventService.dfOrder, headerMap, chargeType);

            // Generate request 
            req = DF_SOABillingEventUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, requestStr, headerMap);
            system.debug('req: ' + req); 

            // Send and get response               
            res = http.send(req);

            system.debug('Response Status Code: ' + res.getStatusCode());
            system.debug('Response Body: ' + res.getBody());
        
            if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {                           
                system.debug('send success');
                DF_SOABillingEventUtils.SOACreateBillingEventSuccessPostProcessing(DF_SOABillingEventService.dfOrder, chargeType);
            
            } else {
                system.debug('send failure');                                                                   
                
                List<String> paramList = new List<String>{param};                  
                AsyncQueueableUtils.retry(DF_SOACreateBillingEventHandler.HANDLER_NAME, paramList, ERROR_MESSAGE);                          
            }                           
        } catch (Exception e) {
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_SOABillingEventService.class.getName(), 'createBillingEvent', '', '', '', '', e, 0);
            
            List<String> paramList = new List<String>{param};              
            AsyncQueueableUtils.retry(DF_SOACreateBillingEventHandler.HANDLER_NAME, paramList, (e.getMessage() + '\n\n' + e.getStackTraceString()));       
        }        
    }
    
    public static boolean runningInSandbox()
    {
    Organization org=[SELECT IsSandbox FROM Organization LIMIT 1];
    return org.isSandbox;
    }

    @TestVisible private static void getParamInputs(String param) {             
        final String DELIMITER = ',';           
        List<String> paramInputList;
        
        String dfOrderId;
  
        try {           
            if (String.isNotEmpty(param)) {             
                paramInputList = param.split(DELIMITER, 2);
                
                if (!paramInputList.isEmpty()) {                                                            
                    if (paramInputList.size() == 2) {                       
                        dfOrderId = paramInputList[0];
                        
                        if (String.isNotEmpty(dfOrderId)) {
                            DF_SOABillingEventService.dfOrderId = dfOrderId;
                        }                       
                        
                        DF_SOABillingEventService.chargeType = paramInputList[1];                               
                    }
                }
            }
        } catch (Exception e) {    
            throw new CustomException(e.getMessage());
        }         
    }  

}