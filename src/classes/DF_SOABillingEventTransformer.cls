public class DF_SOABillingEventTransformer {  

    public static String generateCreateBillingEventRequest(DF_Order__c dfOrder, String chargeType) {	 	    		
		String createBillingEventRequestStr;

		try {	
			if (dfOrder != null) {												
            	if (chargeType.equalsIgnoreCase(DF_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY)) {               		         		
         			createBillingEventRequestStr = dfOrder.Site_Survey_Charges_JSON__c;
         			
					if (String.isEmpty(createBillingEventRequestStr)) {												
						throw new CustomException(DF_SOABillingEventUtils.ERR_DFORDER_SITE_SURVEY_CHARGES_JSON_NOT_FOUND);
					}         			
            	} else if (chargeType.equalsIgnoreCase(DF_SOABillingEventUtils.CHARGE_TYPE_PRODUCT)) {            		            		
          			createBillingEventRequestStr = dfOrder.Product_Charges_JSON__c;
          			
					if (String.isEmpty(createBillingEventRequestStr)) {				
						throw new CustomException(DF_SOABillingEventUtils.ERR_DFORDER_SITE_PRODUCT_CHARGES_JSON_NOT_FOUND);
					}          			
            	}            		
				// Changes for In-flight cancel - START
            	else if (chargeType.equalsIgnoreCase(DF_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER)) {
          			createBillingEventRequestStr = dfOrder.Order_Cancel_Charges_JSON__c;
          			
					if (String.isEmpty(createBillingEventRequestStr)) {
						throw new CustomException(DF_SOABillingEventUtils.ERR_DFORDER_ORDER_CANCEL_CHARGES_JSON_NOT_FOUND);
					}
            	}
            	// Changes for In-flight cancel - END
			}
        } catch (Exception e) {
            throw new CustomException(e.getMessage());            
        }		
				
		return createBillingEventRequestStr;
    }

}