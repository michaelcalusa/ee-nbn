@isTest
private class DF_SOABillingEventTransformer_Test {
	
    @isTest static void test_generateCreateBillingEventRequest_SiteSurveyCharge() {          	 	    	
    	/** Setup data  **/   	       
		// Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
		dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
		insert dfQuote;
	
		// Create DF Order
		DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
		dfOrder.Site_Survey_Charges_JSON__c = '{"field":"value"}';
		insert dfOrder;

    	test.startTest(); 	
		
		String request = DF_SOABillingEventTransformer.generateCreateBillingEventRequest(dfOrder, DF_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY);
		
        test.stopTest();                              

        // Assertions
        system.AssertEquals(false, String.isEmpty(request));
    }    
    
    @isTest static void test_generateCreateBillingEventRequest_ProductCharge() {          	 	    	
    	/** Setup data  **/   	       
		// Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
		dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
		insert dfQuote;
	
		// Create DF Order
		DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
		dfOrder.Product_Charges_JSON__c = '{"field":"value"}';
		insert dfOrder;

    	test.startTest(); 	
		
		String request = DF_SOABillingEventTransformer.generateCreateBillingEventRequest(dfOrder, DF_SOABillingEventUtils.CHARGE_TYPE_PRODUCT);
		
        test.stopTest();                              

        // Assertions
        system.AssertEquals(false, String.isEmpty(request));
    }
 
     @isTest static void test_generateCreateBillingEventRequest_withSiteSurveyChargeError() {          	 	    	
    	/** Setup data  **/   	       
		// Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
		dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
		insert dfQuote;
	
		// Create DF Order
		DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
		dfOrder.Site_Survey_Charges_JSON__c = null;
		insert dfOrder;

		String request;

    	test.startTest(); 	
		
		try {
			request = DF_SOABillingEventTransformer.generateCreateBillingEventRequest(dfOrder, DF_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY);		
        } catch (Exception e) {
            // suppress error         
        }		
		
        test.stopTest();                              

        // Assertions
        system.AssertEquals(true, String.isEmpty(request));
    }

     @isTest static void test_generateCreateBillingEventRequest_withProductChargeError() {          	 	    	
    	/** Setup data  **/   	       
		// Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
		dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
		insert dfQuote;
	
		// Create DF Order
		DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
		dfOrder.Product_Charges_JSON__c = null;
		insert dfOrder;

		String request;

    	test.startTest(); 	
		
		try {
			request = DF_SOABillingEventTransformer.generateCreateBillingEventRequest(dfOrder, DF_SOABillingEventUtils.CHARGE_TYPE_PRODUCT);		
        } catch (Exception e) {
            // suppress error         
        }		
		
        test.stopTest();                              

        // Assertions
        system.AssertEquals(true, String.isEmpty(request));
    }
 
}