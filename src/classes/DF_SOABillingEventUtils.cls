public class DF_SOABillingEventUtils {

    public static final String INTEGR_SETTING_NAME_CREATE_BILL_EVT = 'SOA_Create_Billing_Event';
    public static final String CHARGE_TYPE_SITE_SURVEY = 'SITE SURVEY';
    public static final String CHARGE_TYPE_PRODUCT = 'PRODUCT'; 
	public static final String CHARGE_TYPE_CANCEL_ORDER = 'CANCEL ORDER';
    public static final String ERR_DFORDER_SITE_SURVEY_CHARGES_JSON_NOT_FOUND = 'DF Order - Site Survey Charges JSON not found.';
    public static final String ERR_DFORDER_SITE_PRODUCT_CHARGES_JSON_NOT_FOUND = 'DF Order - Product Charges JSON not found.';
    public static final String ERR_DFORDER_ORDER_CANCEL_CHARGES_JSON_NOT_FOUND = 'DF Order - Cancel Charges JSON not found.';

    public static void getHeaders(DF_Order__c dfOrder, Map<String, String> headerMap, String chargeType) {     
        // Headers
        final String TIMESTAMP_VAL;  
        final String CORRELATION_ID_VAL;
        final String ACCESS_SEEKER_ID_VAL;
        
        try {               
            TIMESTAMP_VAL = DateTime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');

            headerMap.put(DF_IntegrationUtils.TIMESTAMP, TIMESTAMP_VAL);

            if (dfOrder != null) {
                if(chargeType == DF_SOABillingEventUtils.CHARGE_TYPE_PRODUCT)              
                    CORRELATION_ID_VAL = dfOrder.DF_Quote__r.Order_GUID__c; 
                else 
                    CORRELATION_ID_VAL = dfOrder.DF_Quote__r.GUID__c;                   
                
                ACCESS_SEEKER_ID_VAL = dfOrder.Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c;
                
                headerMap.put(DF_IntegrationUtils.CORRELATION_ID, CORRELATION_ID_VAL);                          
                headerMap.put(DF_IntegrationUtils.ACCESS_SEEKER_ID, ACCESS_SEEKER_ID_VAL);
            }

            headerMap.put(DF_Constants.CONTENT_TYPE, DF_Constants.CONTENT_TYPE_JSON);           
            headerMap.put(DF_Constants.HTTP_HEADER_ACCEPT, DF_Constants.CONTENT_TYPE_JSON);
            headerMap.put(DF_Constants.Product_Name, DF_Constants.EE_Product_Name);                                          
        } catch (Exception e) {  
            throw new CustomException(e.getMessage());
        }
    }

    public static HttpRequest getHttpRequest(String endPoint, Integer timeoutLimit, String requestStr, Map<String, String> headerMap) {                                                      
        HttpRequest req = new HttpRequest();        
        
        try {               
            req.setEndpoint(endpoint);      
            req.setMethod(DF_Constants.HTTP_METHOD_POST);             
            req.setTimeout(timeoutLimit);
            
            // Set all headers
            if (String.isNotEmpty(headerMap.get(DF_IntegrationUtils.CORRELATION_ID))) {
                system.debug('::GUID::'+headerMap.get(DF_IntegrationUtils.CORRELATION_ID));             
                req.setHeader(DF_IntegrationUtils.CORRELATION_ID, headerMap.get(DF_IntegrationUtils.CORRELATION_ID));           
            } else {                
                throw new CustomException(DF_IntegrationUtils.ERR_CORRELATION_ID_NOT_FOUND);
            }            
            
            req.setHeader(DF_IntegrationUtils.TIMESTAMP, headerMap.get(DF_IntegrationUtils.TIMESTAMP));

            if (String.isNotEmpty(headerMap.get(DF_IntegrationUtils.ACCESS_SEEKER_ID))) {               
                req.setHeader(DF_IntegrationUtils.ACCESS_SEEKER_ID, headerMap.get(DF_IntegrationUtils.ACCESS_SEEKER_ID));               
            } else {                
                throw new CustomException(DF_IntegrationUtils.ERR_ACCESS_SEEKER_ID_NOT_FOUND);
            }   

            // Only set NBN ENV Override header if Data Power NBN ENV Override - Integration settings val was set
            if (String.isNotEmpty(headerMap.get(DF_IntegrationUtils.NBN_ENV_OVRD))) {               
                req.setHeader(DF_IntegrationUtils.NBN_ENV_OVRD, headerMap.get(DF_IntegrationUtils.NBN_ENV_OVRD));               
            }               
            
            req.setHeader(DF_Constants.CONTENT_TYPE, headerMap.get(DF_Constants.CONTENT_TYPE));
            req.setHeader(DF_Constants.HTTP_HEADER_ACCEPT, headerMap.get(DF_Constants.CONTENT_TYPE));
            req.setHeader(DF_Constants.Product_Name, headerMap.get(DF_Constants.Product_Name));

            req.setBody(requestStr);                                 
        } catch (Exception e) {  
            throw new CustomException(e.getMessage());
        }
               
        return req;
    }   
              
    public static void SOACreateBillingEventSuccessPostProcessing(DF_Order__c dfOrder, String chargeType) {
        try {    
            
            if (dfOrder != null) {              
                if (chargeType.equalsIgnoreCase(DF_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY)) {
                    dfOrder.SOA_SiteSurvey_Charges_Sent__c = true;                                                                              
                } else if (chargeType.equalsIgnoreCase(DF_SOABillingEventUtils.CHARGE_TYPE_PRODUCT)) {
                    dfOrder.SOA_Product_Charges_Sent__c = true;
                }               
                // Changes for In-flight cancel order - START
                else if (chargeType.equalsIgnoreCase(DF_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER)) {
                    dfOrder.SOA_Order_Cancel_Charges_Sent__c = true;
                }
                // Changes for In-flight cancel order - END
            }
            
            update dfOrder;
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }        
    }    

    public static DF_Order__c getDFOrder(Id dfOrderId) {                                             
        DF_Order__c dfOrder;
        List<DF_Order__c> dfOrderList = new List<DF_Order__c>();

        try {
            if (String.isNotEmpty(dfOrderId)) {                                                                        
                dfOrderList = [SELECT DF_Quote__r.GUID__c, DF_Quote__r.Order_GUID__c,
                                      Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c,
                                      Site_Survey_Charges_JSON__c,
                                      Product_Charges_JSON__c,
                                      Order_Cancel_Charges_JSON__c,
                                      SOA_Order_Cancel_Charges_Sent__c
                               FROM   DF_Order__c            
                               WHERE  Id = :dfOrderId];     

                if (!dfOrderList.isEmpty()) {
                    dfOrder = dfOrderList[0];
                }                                                          
            }                                                     
        } catch (Exception e) {    
            throw new CustomException(e.getMessage());
        }                     
                            
        return dfOrder;
    } 

}