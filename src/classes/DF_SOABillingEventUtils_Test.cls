@isTest
private class DF_SOABillingEventUtils_Test {
    
    @isTest static void test_getHttpRequest() {                     
        /** Setup data **/   
        final String END_POINT;
        final String NAMED_CREDENTIAL;        
        final Integer TIMEOUT_LIMIT;
        
        final String NBN_ENV_OVRD_VAL;
        final String TIMESTAMP_VAL;  
        final String CORRELATION_ID_VAL;
        final String ACCESS_SEEKER_ID_VAL;        
        
        final String CORRELATION_ID;
        final String ACCESS_SEEKER_ID;
        String requestStr = 'requestStr';
        
        HttpRequest req;
        User commUser;

        Map<String, String> headerMap = new Map<String, String>();

        DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
                                                            Timeout__c,
                                                            DP_Environment_Override__c
                                                     FROM   DF_Integration_Setting__mdt
                                                     WHERE  DeveloperName = :DF_SOABillingEventUtils.INTEGR_SETTING_NAME_CREATE_BILL_EVT];

        NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
        TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
        NBN_ENV_OVRD_VAL = integrSetting.DP_Environment_Override__c;        
        END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL;
        
        TIMESTAMP_VAL = DateTime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        CORRELATION_ID_VAL = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        ACCESS_SEEKER_ID_VAL = 'ASI500050005000';

        // Setup headerMap
        headerMap.put(DF_IntegrationUtils.NBN_ENV_OVRD, NBN_ENV_OVRD_VAL);  
        headerMap.put(DF_IntegrationUtils.TIMESTAMP, TIMESTAMP_VAL);
        headerMap.put(DF_IntegrationUtils.CORRELATION_ID, CORRELATION_ID_VAL);                          
        headerMap.put(DF_IntegrationUtils.ACCESS_SEEKER_ID, ACCESS_SEEKER_ID_VAL);      
        headerMap.put(DF_Constants.CONTENT_TYPE, DF_Constants.CONTENT_TYPE_JSON);           
        headerMap.put(DF_Constants.HTTP_HEADER_ACCEPT, DF_Constants.CONTENT_TYPE_JSON);
        headerMap.put(DF_Constants.Product_Name, DF_Constants.EE_Product_Name);
          
        commUser = DF_TestData.createDFCommUser();          
        
        system.runAs(commUser) {
            // Statements to be executed by this test user
            test.startTest();           
    
            req = DF_SOABillingEventUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, requestStr, headerMap);
                                            
            test.stopTest();                
        }                    

        // Assertions
        system.AssertNotEquals(null, req);
    }    

    @isTest static void test_getHeaders() {                     
        /** Setup data **/           
        final String NBN_ENV_OVRD_VAL;
        Map<String, String> headerMap = new Map<String, String>();

        // Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.Order_GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55bada';
        insert dfQuote;
    
        // Create DF Order
        DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        insert dfOrder;

        // Setup headerMap
        headerMap.put(DF_IntegrationUtils.NBN_ENV_OVRD, NBN_ENV_OVRD_VAL);              

        test.startTest();           

        DF_SOABillingEventUtils.getHeaders(dfOrder, headerMap, 'PRODUCT');
                                        
        test.stopTest();                        

        // Assertions
        system.AssertEquals(7, headerMap.size());
    }    

    @isTest static void test_getDFOrder() {                     
        /** Setup data **/   
        DF_Order__c dfOrder;

        // Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Create DF Order
        DF_Order__c dfOrderToCreate = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        insert dfOrderToCreate;

        test.startTest();           
        
        dfOrder = DF_SOABillingEventUtils.getDFOrder(dfOrderToCreate.Id);
                                        
        test.stopTest();               

        // Assertions
        system.AssertNotEquals(null, dfOrder);
    }   

    @isTest static void test_SOACreateBillingEventSuccessPostProcessing_Site_Survey_Charge() {                      
        /** Setup data **/ 
        // Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Create DF Order
        DF_Order__c dfOrder = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        insert dfOrder;

        test.startTest();           
        
        DF_SOABillingEventUtils.SOACreateBillingEventSuccessPostProcessing(dfOrder, DF_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY);
                                        
        test.stopTest();               

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT SOA_SiteSurvey_Charges_Sent__c
                                         FROM   DF_Order__c 
                                         WHERE  Id = :dfOrder.Id];

        system.AssertEquals(true, dfOrderList[0].SOA_SiteSurvey_Charges_Sent__c);
    } 
    
    @isTest static void test_SOACreateBillingEventSuccessPostProcessing_Product_Charge() {                      
        /** Setup data **/  
        // Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        // Create DF Order
        DF_Order__c dfOrder = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        insert dfOrder;

        test.startTest();           
        
        DF_SOABillingEventUtils.SOACreateBillingEventSuccessPostProcessing(dfOrder, DF_SOABillingEventUtils.CHARGE_TYPE_PRODUCT);
                                        
        test.stopTest();               

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT SOA_Product_Charges_Sent__c
                                         FROM   DF_Order__c 
                                         WHERE  Id = :dfOrder.Id];

        system.AssertEquals(true, dfOrderList[0].SOA_Product_Charges_Sent__c);
    }                
    
}