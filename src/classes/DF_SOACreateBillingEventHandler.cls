public class DF_SOACreateBillingEventHandler extends AsyncQueueableHandler {

	public static final String HANDLER_NAME = 'SOACreateBillingEventHandler';

    public DF_SOACreateBillingEventHandler() {
    	// No of params must be 1
        super(DF_SOACreateBillingEventHandler.HANDLER_NAME, 1);     
    }

    public override void executeWork(List<String> paramsList) {    	
    	// Enforce max of 1 input parameter
    	if (paramsList.size() == 1) {
    		invokeProcess(paramsList[0]);
    	} else {
    		throw new CustomException(AsyncQueueableUtils.ERR_INVALID_PARAMS);
    	}
    }

    private void invokeProcess(String param) {
        DF_SOABillingEventService.createBillingEvent(param);
    }
}