@isTest
private class DF_SOACreateBillingEventHandler_Test {

    @isTest static void test_executeWork() {            
    	/** Setup data  **/
    	String response;    	
		User commUser;
    	       
		// Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
		dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
		insert dfQuote;
	
		// Create DF Order
		DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
		dfOrder.Site_Survey_Charges_JSON__c = '{"field":"value"}';
		insert dfOrder;	
    	   
		final String DELIMITER = ',';
		final String CHARGE_TYPE = DF_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY;
		String param = dfOrder.Id + DELIMITER + CHARGE_TYPE;

        List<String> paramsList = new List<String>();
        paramsList.add(param); 

   		// Generate mock response
		response = '{"Ack":"true"}';
 
		// Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

		// Set up commUser to run test as
 		commUser = DF_TestData.createDFCommUser();

		system.runAs(commUser) { 
	    	test.startTest(); 	

			DF_SOACreateBillingEventHandler handler = new DF_SOACreateBillingEventHandler();       
    		
	        // Invoke Call
	        handler.executeWork(paramsList);			
			
	        test.stopTest();     			
		}                                                  

        // Assertions
		List<DF_Order__c> dfOrderList = [SELECT SOA_SiteSurvey_Charges_Sent__c
                           				 FROM   DF_Order__c 
                           				 WHERE  Id = :dfOrder.Id];

        system.AssertEquals(true, dfOrderList[0].SOA_SiteSurvey_Charges_Sent__c);     
    }

    @isTest static void test_executeWork_withInvalidParamsError() {            
    	/** Setup data  **/
    	String response;    	
		User commUser;
    	       
		// Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
		dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
		insert dfQuote;
	
		// Create DF Order
		DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
		dfOrder.Site_Survey_Charges_JSON__c = '{"field":"value"}';
		insert dfOrder;	
    	   
		final String DELIMITER = ',';
		String param1 = dfOrder.Id + DELIMITER + DF_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY;
		String param2 = dfOrder.Id + DELIMITER + DF_SOABillingEventUtils.CHARGE_TYPE_PRODUCT;

        List<String> paramsList = new List<String>();
        paramsList.add(param1);
        paramsList.add(param2); 

   		// Generate mock response
		response = '{"Ack":"true"}';
 
		// Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

		// Set up commUser to run test as
 		commUser = DF_TestData.createDFCommUser();

		system.runAs(commUser) { 
	    	test.startTest(); 	

			try {
				DF_SOACreateBillingEventHandler handler = new DF_SOACreateBillingEventHandler();       
	    		
		        // Invoke Call
		        handler.executeWork(paramsList);			
	        } catch (Exception e) {
	            // suppress error         
	        }
			
	        test.stopTest();     			
		}                                                  

        // Assertions
		List<DF_Order__c> dfOrderList = [SELECT SOA_SiteSurvey_Charges_Sent__c
                           				 FROM   DF_Order__c 
                           				 WHERE  Id = :dfOrder.Id];

        system.AssertEquals(false, dfOrderList[0].SOA_SiteSurvey_Charges_Sent__c);     
    }

	/** Data Creation **/
    @testSetup static void setup() {
    	try {
    		createCustomSettings();  				        
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }     	
    }
    
    @isTest static void createCustomSettings() {                
        try {
	        Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
			asrConfigSettings.Max_No_of_Future_Calls__c = 50;
			asrConfigSettings.Max_No_of_Parallels__c = 10;
			asrConfigSettings.Max_No_of_Retries__c = 3;
			asrConfigSettings.Max_No_of_Rows__c = 1;			
	        upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }                
    }  
 
}