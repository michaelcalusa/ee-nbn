@IsTest
public class DF_SOAOrderDataJSONWrapper_Test {

    static testMethod void testParse() {
        String json = '{'+
        '   \"headers\":{'+
        '      \"timestamp\":\"2018-05-09T04:21:57.285Z\",'+
        '      \"security\":\"Placeholder Security\",'+
        '      \"orderPriority\":\"6\",'+
        '      \"msgType\":\"Request\",'+
        '      \"msgName\":\"ManageBillingEventcreateBillingEventRequest\",'+
        '      \"correlationId\":\"4eef4405-606e-46db-8c5c-af78e2874077\",'+
        '      \"communicationPattern\":\"RequestReply\",'+
        '      \"businessServiceVersion\":\"V2.0\",'+
        '      \"businessServiceName\":\"ManageBillingEvent\",'+
        '      \"businessChannel\":\"Enterprise Fulfilment\",'+
        '      \"activityName\":\"createBillingEvent\",'+
        '      \"accessSeekerID\":\"123456\"'+
        '   },'+
        '   \"body\":{'+
        '      \"ManageBillingEventRequest\":{'+
        '         \"notificationType\":\"BillingEvent\",'+
        '         \"productOrder\":{'+
        '            \"productOrderComprisedOf\":{'+
        '               \"referencesProductOrderItem\":['+
        '                  {'+
        '                     \"referencesProductOrderItem\":['+
        '                        {'+
        '                           \"itemInvolvesProduct\":{'+
        '                              \"type\":\"UNIE\",'+
        '                              \"transientId\":\"1\",'+
        '                              \"tpId\":\"0x9100\",'+
        '                              \"portId\":\"3\",'+
        '                              \"ovcType\":\"EVPL\",'+
        '                              \"interfaceType\":\"100Base-10Base-T (Cat-3) \",'+
        '                              \"interfaceBandwidth\":\"5 GB\",'+
        '                              \"id\":\"UNI112233445566\",'+
        '                              \"cosMappingMode\":\"PCP\"'+
        '                           },'+
        '                           \"action\":\"ADD\"'+
        '                        }'+
        '                     ],'+
        '                     \"itemInvolvesProduct\":{'+
        '                        \"uniVLanId\":null,'+
        '                        \"type\":\"BTD\",'+
        '                        \"sVLanId\":null,'+
        '                        \"routeType\":null,'+
        '                        \"powerSupply2\":\"Not Required\",'+
        '                        \"powerSupply1\":\"DC(-d8V to -22V)\",'+
        '                        \"poi\":null,'+
        '                        \"nni\":null,'+
        '                        \"maximumFrameSize\":null,'+
        '                        \"id\":\"BTD112233445566\",'+
        '                        \"cosMediumBandwidth\":null,'+
        '                        \"cosLowBandwidth\":null,'+
        '                        \"cosHighBandwidth\":null,'+
        '                        \"connectedTo\":null,'+
        '                        \"btdType\":\"Standard (CSAS)\",'+
        '                        \"btdMounting\":\"Rack\"'+
        '                     },'+
        '                     \"action\":\"ADD\"'+
        '                  },'+
        '                  {'+
        '                     \"referencesProductOrderItem\":null,'+
        '                     \"itemInvolvesProduct\":{'+
        '                        \"uniVLanId\":\"25\",'+
        '                        \"type\":\"OVC\",'+
        '                        \"sVLanId\":\"21\",'+
        '                        \"routeType\":\"Local\",'+
        '                        \"powerSupply2\":null,'+
        '                        \"powerSupply1\":null,'+
        '                        \"poi\":\"N/A\",'+
        '                        \"nni\":\"NNI000000601417\",'+
        '                        \"maximumFrameSize\":\"Jumbo (9000 Bytes)\",'+
        '                        \"id\":\"OVC112233445588\",'+
        '                        \"cosMediumBandwidth\":\"500Mbps \",'+
        '                        \"cosLowBandwidth\":\"500Mbps \",'+
        '                        \"cosHighBandwidth\":\"50Mbps \",'+
        '                        \"connectedTo\":\"1\",'+
        '                        \"btdType\":null,'+
        '                        \"btdMounting\":null'+
        '                     },'+
        '                     \"action\":\"ADD\"'+
        '                  },'+
        '                  {'+
        '                     \"referencesProductOrderItem\":null,'+
        '                     \"itemInvolvesProduct\":{'+
        '                        \"uniVLanId\":\"25\",'+
        '                        \"type\":\"OVC\",'+
        '                        \"sVLanId\":\"21\",'+
        '                        \"routeType\":\"Local\",'+
        '                        \"powerSupply2\":null,'+
        '                        \"powerSupply1\":null,'+
        '                        \"poi\":\"N/A\",'+
        '                        \"nni\":\"NNI000000601417\",'+
        '                        \"maximumFrameSize\":\"Jumbo (9000 Bytes)\",'+
        '                        \"id\":\"OVC112233445589\",'+
        '                        \"cosMediumBandwidth\":\"500Mbps \",'+
        '                        \"cosLowBandwidth\":\"500Mbps \",'+
        '                        \"cosHighBandwidth\":\"50Mbps \",'+
        '                        \"connectedTo\":\"1\",'+
        '                        \"btdType\":null,'+
        '                        \"btdMounting\":null'+
        '                     },'+
        '                     \"action\":\"ADD\"'+
        '                  }'+
        '               ],'+
        '               \"itemInvolvesProduct\":{'+
        '                  \"zone\":\"CBD\",'+
        '                  \"term\":\"12 Months\",'+
        '                  \"templateId\":\"TPL\",'+
        '                  \"serviceRestorationSLA\":\"Enhanced-6 (24/7)\",'+
        '                  \"id\":\"PRI000000000969\",'+
        '                  \"accessServiceTechnologyType\":\"EE\",'+
        '                  \"accessAvailabilityTarget\":\"99.95% (Dual Uplink/same Network Aggregation element)\"'+
        '               },'+
        '               \"itemInvolvesLocation\":{'+
        '                  \"id\":\"LOC000233782131\"'+
        '               },'+
        '               \"action\":\"ADD\"'+
        '            },'+
        '            \"plannedCompletionDate\":\"2018-01-01T12:00:00Z\",'+
        '            \"orderType\":\"Connect\",'+
        '            \"interactionStatus\":\"Complete\",'+
        '            \"interactionDateComplete\":\"2018-02-01T12:00:00Z\",'+
        '            \"id\":\"ORD000000000878\",'+
        '            \"customerRequiredDate\":\"2017-02-25T00:00:00Z\",'+
        '            \"afterHoursAppointment\":\"Yes\",'+
        '            \"accessSeekerInteraction\":{'+
        '               \"billingAccountID\":\"BAN000000892801\"'+
        '            }'+
        '         },'+
        '         \"ProductOrderInvolvesCharges\":{'+
        '            \"ProdPriceCharge\":['+
        '               {'+
        '                  \"type\":\"PRI\",'+
        '                  \"quantity\":\"1.0\",'+
        '                  \"name\":\"Zone Charges\",'+
        '                  \"ID\":\"EEAS_RC_1GB_ZONE_1\",'+
        '                  \"chargeReferences\":\"PRI000000000969\",'+
        '                  \"amount\":\"200.00\"'+
        '               },'+
        '               {'+
        '                  \"type\":\"PRI\",'+
        '                  \"quantity\":\"1.0\",'+
        '                  \"name\":\"eSLA Charges\",'+
        '                  \"ID\":\"EEAS_RC_ESLA_6_24_7\",'+
        '                  \"chargeReferences\":\"PRI000000000969\",'+
        '                  \"amount\":\"65.00\"'+
        '               },'+
        '               {'+
        '                  \"type\":\"OVC\",'+
        '                  \"quantity\":\"1.0\",'+
        '                  \"name\":\"Route Type Charges\",'+
        '                  \"ID\":\"EEAS_RC_STATE_ZONE_2\",'+
        '                  \"chargeReferences\":\"OVC112233445588\",'+
        '                  \"amount\":\"846.00\"'+
        '               },'+
        '               {'+
        '                  \"type\":\"OVC\",'+
        '                  \"quantity\":\"1.0\",'+
        '                  \"name\":\"CoS Low Charges\",'+
        '                  \"ID\":\"EEAS_RC_COS_LOW\",'+
        '                  \"chargeReferences\":\"OVC112233445588\",'+
        '                  \"amount\":\"232.00\"'+
        '               },'+
        '               {'+
        '                  \"type\":\"OVC\",'+
        '                  \"quantity\":\"1.0\",'+
        '                  \"name\":\"CoS High Charges\",'+
        '                  \"ID\":\"EEAS_RC_COS_HIGH\",'+
        '                  \"chargeReferences\":\"OVC112233445588\",'+
        '                  \"amount\":\"380.00\"'+
        '               },'+
        '               {'+
        '                  \"type\":\"OVC\",'+
        '                  \"quantity\":\"1.0\",'+
        '                  \"name\":\"CoS Medium Charges\",'+
        '                  \"ID\":\"EEAS_RC_COS_MEDIUM\",'+
        '                  \"chargeReferences\":\"OVC112233445588\",'+
        '                  \"amount\":\"293.00\"'+
        '               },'+
        '               {'+
        '                  \"type\":\"OVC\",'+
        '                  \"quantity\":\"1.0\",'+
        '                  \"name\":\"Route Type Charges\",'+
        '                  \"ID\":\"EEAS_RC_STATE_ZONE_2\",'+
        '                  \"chargeReferences\":\"OVC112233445589\",'+
        '                  \"amount\":\"2585.00\"'+
        '               },'+
        '               {'+
        '                  \"type\":\"OVC\",'+
        '                  \"quantity\":\"1.0\",'+
        '                  \"name\":\"CoS Medium Charges\",'+
        '                  \"ID\":\"EEAS_RC_COS_MEDIUM\",'+
        '                  \"chargeReferences\":\"OVC112233445589\",'+
        '                  \"amount\":\"573.00\"'+
        '               },'+
        '               {'+
        '                  \"type\":\"OVC\",'+
        '                  \"quantity\":\"1.0\",'+
        '                  \"name\":\"CoS Low Charges\",'+
        '                  \"ID\":\"EEAS_RC_COS_LOW\",'+
        '                  \"chargeReferences\":\"OVC112233445589\",'+
        '                  \"amount\":\"382.00\"'+
        '               },'+
        '               {'+
        '                  \"type\":\"OVC\",'+
        '                  \"quantity\":\"1.0\",'+
        '                  \"name\":\"CoS High Charges\",'+
        '                  \"ID\":\"EEAS_RC_COS_HIGH\",'+
        '                  \"chargeReferences\":\"OVC112233445589\",'+
        '                  \"amount\":\"426.00\"'+
        '               }'+
        '            ],'+
        '            \"OneTimeChargeProdPriceCharge\":['+
        '               {'+
        '                  \"type\":\"PRI\",'+
        '                  \"quantity\":\"1.0\",'+
        '                  \"name\":\"Fibre Build Contribution\",'+
        '                  \"ID\":\"EEAS_NRC_FBC\",'+
        '                  \"chargeReferences\":\"PRI000000000969\",'+
        '                  \"amount\":\"25000.00\"'+
        '               },'+
        '               {'+
        '                  \"type\":\"PRI\",'+
        '                  \"quantity\":\"1.0\",'+
        '                  \"name\":\"UNI Setup Fee\",'+
        '                  \"ID\":\"EEAS_NRC_1G_SETUP_FEE\",'+
        '                  \"chargeReferences\":\"PRI000000000969\",'+
        '                  \"amount\":\"5000.00\"'+
        '               },'+
        '               {'+
        '                  \"type\":\"PRI\",'+
        '                  \"quantity\":\"1.0\",'+
        '                  \"name\":\"After Hours Appointment\",'+
        '                  \"ID\":\"EEAS_NRC_AHA_MISSED\",'+
        '                  \"chargeReferences\":\"PRI000000000969\",'+
        '                  \"amount\":\"150.00\"'+
        '               }'+
        '            ]'+
        '         }'+
        '      }'+
        '   }'+
        '}';
        DF_SOAOrderDataJSONWrapper obj = DF_SOAOrderDataJSONWrapper.parse(json);
        System.assert(obj != null);
    }
}