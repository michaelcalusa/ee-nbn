public class DF_SendLocationRequestQueueHandler implements Queueable {  
    /* 
        Salesforce Limits:
        -50 future calls per batch transaction
        -100 callouts per transaction
        
        QueueHandler Limits:
        -Max 50 invocations (of getLocation future method)
         
        This equates to:
        -> 50 future calls (per transaction)
        -> Max 2 callouts (per transaction) - 1 Address API callout / 1 Location API callout
        
        -This SendLocationRequestQueueHandler will fire off max 50 SFReqs (future calls) in 1 txn (1 iteration of the Queuehandler)
         If there are more than 50 SFReqs to be sent, then this job (txn) will spawn again for the next 50 SFReqs and so on 
         till all pending SFReqs have been processed for an Opportunity Bundle.
    */

    public static final Integer MAX_NO_PARALLELS = 50; // Max number of future calls to be made in 1 txn
    private String opportunityBundleId;
    
    // Constructor
    public DF_SendLocationRequestQueueHandler(String opptyBundleId) {       
        system.debug('opptyBundleId: ' + opptyBundleId);

        // Set class vars
        opportunityBundleId = opptyBundleId;
    }

    public void execute(QueueableContext context) {    	
    	Id existingJobId;
    	List<String> sfReqIdList;
    	
    	try {
	        // JobId is the AsyncApexJob Id
	        existingJobId = context.getJobId();
	        system.debug('existingJobId: ' + existingJobId);
	        
	        // Get recs from SFReq table for only the one opptyBundleId
	        sfReqIdList = getSFReqIdList(opportunityBundleId);
	        system.debug('sfReqIdList size: ' + sfReqIdList.size()); 
	        
	        if (!sfReqIdList.isEmpty()) {           
	            for (String sfReqId : sfReqIdList) {                
	                makeCallout(sfReqId);
	            }
	        }
	
	        // if needed more than 50 SFRequests in 1 txn (50 seperate callouts) - re-invoke queueable job          
	        // check if anything else to run
	        if (isAnythingPendingToProcess(opportunityBundleId)) {
	        	
	        	// Only chain jobs if not within context of running test class (chaining not permitted via test class)
	        	if (!Test.isRunningTest()) {
		            Id jobId = System.enqueueJob(new DF_SendLocationRequestQueueHandler(opportunityBundleId));
		   
		            if (jobId != null) {
		                system.debug('DF_SendLocationRequestQueueHandler queueable job started - jobId: ' + jobId);
		            }        		
	        	}
	        }    		
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }
 
    @TestVisible private static void makeCallout(String sfReqId) {
        DF_LocationAPIService.getLocation(sfReqId);
    }
    
    @TestVisible private static List<String> getSFReqIdList(String opptyBundleId) {        
        Map<String, DF_SF_Request__c> sfReqMap = new Map<String, DF_SF_Request__c>();              
        List<String> sfReqIdList;        
        List<DF_SF_Request__c> sfReqUpdateList;
        
        try {                               
            sfReqMap = new Map<String, DF_SF_Request__c>([SELECT   Id 
                                                          FROM     DF_SF_Request__c 
                                                          WHERE    Opportunity_Bundle__c = :opptyBundleId 
                                                          AND      Status__c = :DF_LAPI_APIServiceUtils.STATUS_PENDING
                                                          ORDER BY Id
                                                          LIMIT :DF_SendLocationRequestQueueHandler.MAX_NO_PARALLELS]);

            system.debug('sfReqMap size: ' + sfReqMap.size());
            
            // Returns ids from keyset
            if (!sfReqMap.keySet().isEmpty()) {             
                // Create new list from a set
                sfReqIdList = new List<String>(sfReqMap.keySet());
            }

            // Get sfReq recs to update
            sfReqUpdateList = new List<DF_SF_Request__c>(sfReqMap.values());
            system.debug('sfReqUpdateList size: ' + sfReqUpdateList.size());

            if (!sfReqUpdateList.isEmpty()) {           
                // Loop through and set each rec to Status = "Processed"
                for (DF_SF_Request__c sfReq : sfReqUpdateList) {
                    sfReq.Status__c = DF_LAPI_APIServiceUtils.STATUS_PROCESSED;
                }
                
                update sfReqUpdateList;
            }

            system.debug('sfReqIdList size: ' + sfReqIdList.size());
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }                      
                
        return sfReqIdList;
    }
    
    @TestVisible private static Boolean isAnythingPendingToProcess(String opptyBundleId) {                          
        List<DF_SF_Request__c> sfReqIdList;
        Boolean anythingPendingToProcess = false;

        try {
            sfReqIdList = [SELECT Id 
                           FROM   DF_SF_Request__c 
                           WHERE  Opportunity_Bundle__c = :opptyBundleId
                           AND    Status__c = :DF_LAPI_APIServiceUtils.STATUS_PENDING LIMIT 1];

            system.debug('sfReqIdList.size: ' + sfReqIdList.size());
                        
            if (!sfReqIdList.isEmpty()) { 
                anythingPendingToProcess = true;
            }
                                   
            system.debug('anythingPendingToProcess: ' + anythingPendingToProcess);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }       

        return anythingPendingToProcess;           
    }    
}