@isTest
private class DF_SendLocationRequestQueueHandler_Test {

    @isTest static void test_execute() {      	 	    	
    	// Setup data   
    	String locationId = 'LOC000035375038';
		String response;    	
    	
		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		Map<String, String> addressMap = new Map<String, String>();

		// Create sfreq
		DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, DF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);		
		insert sfReq;
  
    	test.startTest();           

		Id jobId = System.enqueueJob(new DF_SendLocationRequestQueueHandler(opptyBundle.Id));
                                        
        test.stopTest();                

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id 
                           					FROM   DF_SF_Request__c 
                           					WHERE  Opportunity_Bundle__c = :opptyBundle.Id
                           					AND    Status__c = :DF_LAPI_APIServiceUtils.STATUS_PENDING];

		system.AssertEquals(0, sfReqList.size());
    }

    @isTest static void test_makeCallout() {      	 	    	
    	// Setup data   
    	String locationId = 'LOC000035375038';
		String response;

		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		Map<String, String> addressMap = new Map<String, String>();

		// Create sfreq
		DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, DF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);		
		insert sfReq;
  
    	test.startTest();           

		DF_SendLocationRequestQueueHandler.makeCallout(sfReq.Id);
                                        
        test.stopTest();                

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id 
                           					FROM   DF_SF_Request__c 
                           					WHERE  Opportunity_Bundle__c = :opptyBundle.Id
                           					AND    Status__c = :DF_LAPI_APIServiceUtils.STATUS_PENDING];

		system.AssertEquals(0, sfReqList.size());
    }

    @isTest static void test_getSFReqIdList() {      	 	    	
    	// Setup data   
  		List<String> sfReqIdList;

    	String locationId = 'LOC000035375038';
		String response;

		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		Map<String, String> addressMap = new Map<String, String>();

		// Create sfreq
		DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, DF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);		
		insert sfReq;
  
    	test.startTest();           

		sfReqIdList = DF_SendLocationRequestQueueHandler.getSFReqIdList(opptyBundle.Id);
                                        
        test.stopTest();                

        // Assertions
		system.Assert(sfReqIdList.size() > 0);
    }
    
    @isTest static void test_isAnythingPendingToProcess() {      	 	    	
    	// Setup data   
  		Boolean anythingPendingToProcess = false;

    	String locationId = 'LOC000035375038';
		String response;

		// Create acct
        Account acct = DF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		Map<String, String> addressMap = new Map<String, String>();

		// Create sfreq
		DF_SF_Request__c sfReq = DF_TestData.createSFRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, DF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);		
		insert sfReq;
  
    	test.startTest();           

		anythingPendingToProcess = DF_SendLocationRequestQueueHandler.isAnythingPendingToProcess(opptyBundle.Id);
                                        
        test.stopTest();                

        // Assertions
		system.AssertEquals(true, anythingPendingToProcess);
    }
}