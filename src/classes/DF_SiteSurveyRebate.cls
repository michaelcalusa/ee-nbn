public class DF_SiteSurveyRebate {
    
    /*
  * Method to create child opportunity and product basket with product configurations in it based on the offer - Site Survey Rebate.
  * The child opportunity created is mapped to the opportunity that is passed as parameter  
  * Parameters : parent opportunity id to which the child opportunity has to be associated.
  * @Return : void
  */
    public static String addSiteSurveyRebate(String parentOppId){
        Opportunity opp = new Opportunity();
        Boolean stopCreationOfRebateOppty = false;
        csord__Order__c csOrder;
        if(String.isNotEmpty(parentOppId)){
            System.debug('PP parentOppId: '+parentOppId);
            sObject oppObject = new Opportunity();
            Opportunity parentOpp;
            String queryStr = DF_CS_API_Util.getQuery(oppObject, ' WHERE Id = '+ '\'' + parentOppId + '\'');
            List<Opportunity> parentOppList = (List<Opportunity>)DF_CS_API_Util.getQueryRecords(queryStr); 
            //Opportunity parentOpp = !parentOppList.isEmpty() ? parentOppList.get(0) : null;
            if(!parentOppList.isEmpty()){
               csOrder = [Select id,name,csordtelcoa__Opportunity__c,csordtelcoa__Opportunity__r.Parent_Opportunity__c,csordtelcoa__Opportunity__r.Parent_Opportunity__r.id,
               			  csordtelcoa__Opportunity__r.Parent_Opportunity__r.name,
               			  csordtelcoa__Opportunity__r.Parent_Opportunity__r.RecordTypeId , csordtelcoa__Opportunity__r.Parent_Opportunity__r.CloseDate,csordtelcoa__Opportunity__r.amount
           				  from csord__Order__c where csordtelcoa__Opportunity__c <> null and csordtelcoa__Opportunity__r.Parent_Opportunity__c <> null
               			  and name = 'Direct Fibre - Site Survey Charges' and csordtelcoa__Opportunity__r.Parent_Opportunity__c=:parentOppList.get(0).id];	
               			 
               //parentOpp = [Select id,name,RecordTypeId,CloseDate,Commercial_Deal__c,accountid,account.Commercial_Deal__c from Opportunity where id=:parentOppList.get(0).id];
            } 
            if(csOrder <> null && csOrder.csordtelcoa__Opportunity__r.amount == 0 ){
                stopCreationOfRebateOppty = true;
            }
            system.debug('stopCreationOfRebateOppty----'+stopCreationOfRebateOppty);
            if(!stopCreationOfRebateOppty){
                //Create child opportunity
                opp.Name = csOrder.csordtelcoa__Opportunity__r.Parent_Opportunity__r.name;
                opp.StageName = 'New';
                opp.CloseDate = csOrder.csordtelcoa__Opportunity__r.Parent_Opportunity__r.CloseDate;
                opp.Parent_Opportunity__c = csOrder.csordtelcoa__Opportunity__r.Parent_Opportunity__r.id;            
                opp.RecordTypeId = csOrder.csordtelcoa__Opportunity__r.Parent_Opportunity__r.RecordTypeId;
                insert opp;
                //Create basket, product configuration, sync with opportunity and change the opportunity to closed won
                cscfga__Product_Basket__c basket = createProductBasket(opp.Id);
                String syncResults = DF_CS_API_Util.syncWithOpportunity(basket.Id);
                if(syncResults.equalsIgnoreCase('sychronised with opportunity')){
                    updateOppClosedWon(opp,basket);
                }
            }

        }
        return opp.id;
    }
    
    //Method to create product basket
    public static cscfga__Product_Basket__c createProductBasket(String oppId){
        if(String.isNotEmpty(oppId)){
            cscfga__Product_Basket__c basket;
            cscfga.API_1.ApiSession apiSession;
            //create api session
            apiSession = DF_CS_API_Util.createApiSession(basket);
            //create basket for the session
            basket = DF_CS_API_Util.createBasket(apiSession);
            basket.cscfga__opportunity__c = oppId;
            update basket;
            createProductDefinition(apiSession,basket);
            return basket;
        }
        else
            return null;
    }
    
    //Method to create product definition
    public static void createProductDefinition(cscfga.API_1.ApiSession apiSession, cscfga__Product_Basket__c basket){
        apiSession = DF_CS_API_Util.createApiSession(basket);
        
        //fetch custom setting values for site survey charges
        String sscOfferId = DF_CS_API_Util.getCustomSettingValue('SF_SITE_SURVEY_REBATE_OFFER_ID');
        String sscDefId = DF_CS_API_Util.getCustomSettingValue('SF_SITE_SURVEY_REBATE_DEFINITION_ID');
        String categoryId = DF_CS_API_Util.getCustomSettingValue('SF_CATEGORY_ID');
        sObject categoryObject = new cscfga__Product_Category__c();
        String queryStrng = DF_CS_API_Util.getQuery(categoryObject, ' WHERE Id = '+ '\'' + categoryId + '\'');
        List<cscfga__Product_Category__c> categoryList = (List<cscfga__Product_Category__c>)DF_CS_API_Util.getQueryRecords(queryStrng); 
        cscfga__Product_Category__c category = !categoryList.isEmpty() ? categoryList.get(0) : null;
        List<csbb__Product_Configuration_Request__c> pcrList = new List<csbb__Product_Configuration_Request__c>();   
        
        try{
            // adding Site survey charges config
            apiSession.setProductToConfigure(new cscfga__Product_Definition__c(Id = sscDefId), new Map<String, String> {'containerType' => 'basket', 'linkedId' => basket.Id});
            apiSession.executeRules();
            cscfga.ValidationResult vr = apiSession.validateConfiguration();
            apiSession.persistConfiguration(true);
        }catch(Exception ex){
            GlobalUtility.logMessage('Error', 'SiteSurveyRebate', 'createProductDefinition', ' ', '', '', '', ex, 0);
        }  
        cscfga.ProductConfiguration config1 = apiSession.getRootConfiguration(); 
        if(config1 != null)
            pcrList.add(DF_CS_API_Util.createProductConfigurationRequest(basket, config1.getSObject(), sscOfferId, category));
        if(!pcrList.isEmpty()){
            insert pcrList;
        }
    }
    //Method to update Opportunity Stage to Closed Won for creation of orders
    public static void updateOppClosedWon(Opportunity opp, cscfga__Product_Basket__c basket){
        String stageName = DF_CS_API_Util.getCustomSettingValue('SF_OPPORTUNITY_STAGE');
        if(opp != null && basket != null){
            opp.StageName = stageName;
            update opp;
        }
    }
    
}