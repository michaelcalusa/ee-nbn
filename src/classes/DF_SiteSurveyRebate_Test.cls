@IsTest
public class DF_SiteSurveyRebate_Test {

    /**
  * Tests addSiteSurveyRebate method
  */
    @isTest static void addSiteSurveyRebateTest()
    {
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        Opportunity parentOpp = DF_TestData.createOpportunity('Test Parent Opp');
        insert parentOpp;
        Opportunity childOpp = DF_TestData.createOpportunity('Test Child Opp');
        childOpp.Parent_Opportunity__c =parentOpp.id;
        insert childOpp;
        csord__Order__c order = DF_TestData.csOrder(childOpp.id,acc.id);
        insert order;
        Test.startTest();
        String oppId = DF_SiteSurveyRebate.addSiteSurveyRebate(parentOpp.Id);
        Test.stopTest();
        System.assertNotEquals('null',oppId);
    }
    
    @isTest static void addSiteSurveyRebateNegativeTest()
    {
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        Opportunity parentOpp = DF_TestData.createOpportunity('Test Parent Opp');
        insert parentOpp;
        Opportunity childOpp = DF_TestData.createOpportunity('Test Child Opp');
        childOpp.Parent_Opportunity__c =parentOpp.id;
        childOpp.Amount = 0;
        insert childOpp;
        csord__Order__c order = DF_TestData.csOrder(childOpp.id,acc.id);
        insert order;
        Test.startTest();
        String oppId = DF_SiteSurveyRebate.addSiteSurveyRebate(parentOpp.Id);
        Test.stopTest();
        System.assertNotEquals('null',oppId);
    }
}