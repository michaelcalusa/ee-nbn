public  class DF_SvcCacheSearchResults {
    	@AuraEnabled public String locationId {get;set;}  // From SF
    	@AuraEnabled public String address {get;set;}  // From SF
    	@AuraEnabled public String bpiId {get;set;}  //From Service Cache
    	@AuraEnabled public String btdId {get;set;}  //From Service Cache
    	@AuraEnabled public String btdType {get;set;}  //From Service Cache
    	@AuraEnabled public String uniId {get;set;}  //From Service Cache
    	@AuraEnabled public String interfacetype {get;set;}  //From Service Cache
    	@AuraEnabled public String tpid {get;set;}  //From Service Cache
    	@AuraEnabled public String term {get;set;}  // From SF
    	@AuraEnabled public String ovcType {get;set;}  //From Service Cache
    	@AuraEnabled public String zone {get;set;}  //From Service Cache
        @AuraEnabled public String enterpriseEthernetServiceLevelRegion {get;set;}  //From Service Cache
    	@AuraEnabled public String interfaceBandwidth {get;set;}  //From Service Cache
    	@AuraEnabled public String sla {get;set;}  // From SF
    	@AuraEnabled public List<OVC> ovcs {get;set;}
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public String errorMessage {get;set;}
        @AuraEnabled public String debugMessage {get;set;}
        @AuraEnabled public String message {get;set;}
        @AuraEnabled public Id df_QuoteId {get;set;}
        @AuraEnabled public Id df_OrderId {get;set;}
        @AuraEnabled public Date df_OrderDateCreated {get;set;}
		@AuraEnabled public String df_OrderName {get;set;}
        @AuraEnabled public String df_ExternalOrderId {get;set;}
		@AuraEnabled public String accessSeekerId {get;set;}

    public class OVC{
    	@AuraEnabled public String ovcId {get;set;} 
		@AuraEnabled public String routeType {get;set;} 
		@AuraEnabled public String nni {get;set;} 
		@AuraEnabled public String sVLanId {get;set;} 
		@AuraEnabled public String cosLowBandwidth {get;set;} 
    	@AuraEnabled public String cosMediumBandwidth {get;set;} 
    	@AuraEnabled public String cosHighBandwidth {get;set;} 
		@AuraEnabled public String poi {get;set;} 
		@AuraEnabled public String maximumFrameSize {get;set;} 
		@AuraEnabled public String uniVLanId {get;set;} 
		@AuraEnabled public String cosMappingMode {get;set;} 

    }
}