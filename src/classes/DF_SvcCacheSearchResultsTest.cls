@isTest
private class DF_SvcCacheSearchResultsTest {
	

	    @isTest static void  returnTestData(){
	    	test.startTest();
    	DF_SvcCacheSearchResults searchResult = new DF_SvcCacheSearchResults();
    	DF_SvcCacheSearchResults.OVC tempOVC = new DF_SvcCacheSearchResults.OVC();
    	List<DF_SvcCacheSearchResults.OVC> tempOVCList = new List<DF_SvcCacheSearchResults.OVC>();

    			searchResult.bpiId = 'BPI000123123123';
        		searchResult.btdId = 'SWBTD0009200';
        		searchResult.btdType = 'Standard (CSAS)';
        		searchResult.locationId = 'LOC123456799999';
        		searchResult.interfaceBandwidth = '5GB';
            	searchResult.uniId = 'UNI900000002986';
            	searchResult.interfacetype = 'Copper (Auto-negotiate)';
            	searchResult.tpid = '0x9100';
            	searchResult.term = '12 months';
            	searchResult.ovcType = 'Access EVPL';
            	searchResult.zone = 'Missing from SvcCache will be added later';
            	searchResult.sla = 'Enhanced - 12 (24/7)';
            	
            	searchResult.status = 'Whose House?';
            	searchResult.address = 'Runs House';
            	searchResult.errorMessage = 'Whose House? 2';
            	searchResult.debugMessage = 'Runs House 2';
            	searchResult.message = 'run DMC';
            	system.debug('searchResult' + searchResult);

            	tempOVC.ovcId = 'OVC000000012814';
            	tempOVC.routeType =  'State';
            	tempOVC.sVLanId = '2763';
            	tempOVC.cosLowBandwidth = '10Mbps';
            	tempOVC.cosMediumBandwidth = '20Mbps';
            	tempOVC.cosHighBandwidth = '30Mbps';
            	tempOVC.poi = '1-MEL-MELBOURNE-MP';
            	tempOVC.maximumFrameSize = 'Jumbo (9000 Bytes)';
            	tempOVC.uniVLanId = '55';
            	tempOVC.nni = '555';
            	tempOVC.cosMappingMode = 'PCP';
            	system.debug('tempOVC'+tempOVC);
            	tempOVCList.add(tempOVC);
            	tempOVC = new DF_SvcCacheSearchResults.OVC();
            	tempOVC.ovcId = 'OVC000000012822';
            	tempOVC.routeType =  'Local';
            	tempOVC.sVLanId = '2220';
            	tempOVC.cosLowBandwidth = '10Mbps';
            	tempOVC.cosMediumBandwidth = '20Mbps';
            	tempOVC.cosHighBandwidth = '30Mbps';
            	tempOVC.poi = '1-MEL-MELBOURNE-MP';
            	tempOVC.maximumFrameSize = 'Jumbo (9000 Bytes)';
            	tempOVC.uniVLanId = '66';
            	tempOVC.cosMappingMode = 'PCP';
            	tempOVCList.add(tempOVC);
            	system.debug('tempOVC'+tempOVC);
            	searchResult.ovcs = tempOVCList;

            	system.debug(searchResult);

            test.stopTest();     
    	
	
	}
}