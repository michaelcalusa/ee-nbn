public with sharing class DF_SvcCacheService {
	//@future(callout=true)
    public static DF_SvcCacheSearchResults searchServiceCache(String searchString) {                   
        final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;

        //Servi ovRequest = new ServiceCacheRequest();

        Http http = new Http();     
        HttpRequest req;              
        HTTPResponse res; 

        String requestStr;
        String responseStr;

        if(searchString == 'Test') {
        	return DF_SvcCacheServiceUtils.returnTestData();

        }


        
        DF_SvcCacheSearchResults response = new DF_SvcCacheSearchResults();

        try {                               
            DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
                                                                Timeout__c
                                                         FROM   DF_Integration_Setting__mdt
                                                         WHERE  DeveloperName = :DF_SvcCacheServiceUtils.INTEGRATION_SETTING_NAME];

            NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
            TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
            
            system.debug('NAMED_CREDENTIAL: ' + NAMED_CREDENTIAL); 
            system.debug('TIMEOUT_LIMIT: ' + TIMEOUT_LIMIT); 

            END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL+'/'+searchString;
            system.debug('END_POINT: ' + END_POINT);     

            // Generate request 
            req = DF_SvcCacheServiceUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT);
            system.debug('req: ' + req); 

            // Send and get response               
            res = http.send(req);

            system.debug('Response Status Code: ' + res.getStatusCode());
            system.debug('Response Body: ' + res.getBody());
        
            if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {                           
                responseStr = res.getBody();  


                if (String.isNotEmpty(responseStr)) {
                    // Get Response Body - Status
                    response = DF_SvcCacheServiceUtils.getServiceCacheResponse(responseStr);    
                    

                } else {
                    throw new CustomException(DF_SvcCacheServiceUtils.ERR_RESP_BODY_MISSING);
                }
            } else if(res.getStatusCode() == DF_Constants.HTTP_STATUS_404){
            	system.debug('Handling 404 response');
            	responseStr = res.getBody();  
            	response = DF_SvcCacheServiceUtils.processNotFound(responseStr);
            }
            else

            {
                system.debug('send failure');  
				response = DF_SvcCacheServiceUtils.processError();                               
            }               
        } catch (Exception e) {
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_SvcCacheService.class.getName(), 'searchServiceCache', '', '', '', '', e, 0);
            //DF_SvcCacheServiceUtils.validateOrderErrorPostProcessing(dfOrderId);
        }
       

        if(searchString.startsWithIgnoreCase('OVC') && res.getStatusCode()==200){
        	system.debug('!!! get uniId from OVC response: ' + response.uniId);
        	response = searchServiceCache(response.uniId);
        }

 		system.debug('!!! response: ' + response);
        return response;
    } 

   

}