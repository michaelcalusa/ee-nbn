@isTest
private class DF_SvcCacheServiceTest {
    
    @isTest static void test_SVCCache_200_success() {  
        String response;
        User commUser;
        DF_SvcCacheSearchResults result;
         
        response = ' [{"data":{"orderItemId":"212","nni":{"csa":"CSA180000002222","nni_mode":"B","nasId":"ASI500050005000","subscriber_id":"246","nni_lag_id":"104","poi":"1-MEL-MELBOURNE-MP","id":"NNI090000016341","type":"nni","access_seeker_id":"ASI500050005000"},"poi":"1-MEL-MELBOURNE-MP","type":"OVC","cosMappingMode":"PCP","inniSvlanId":"2764","samInstanceId":"SAM","uni":{"interfaceType":"Copper (Auto-negotiate)","tpId":"0x9100","transientId":"1","id":"UNI900000002986","type":"UNIE","portId":"4","ovcType":"Access EVPL","interfaceBandwidth":"2GB","samInstanceId":"SAM"},"cosLowBandwidth":"10Mbps","ePipeSites":[{"tunnels":[{"toIpAddress":"192.168.4.67","id":"1","toHostName":"SWEAS0000001DV"},{"toIpAddress":"192.168.4.68","id":"3","toHostName":"SWEAS0000002DV"},{"toIpAddress":"192.168.4.73","id":"15","toHostName":"SWEAS0000005DV"},{"toIpAddress":"192.168.4.74","id":"16","toHostName":"SWEAS0000006DV"}],"nni_mode":"B","role":"EFS","sourceMacAddress":"B8-E7-79-00-00-01","ipAddress":"192.168.4.69","id":"SWEFS0000001DV","mepId":"1","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.67","id":"1","toHostName":"SWEAS0000001DV"},{"toIpAddress":"192.168.4.68","id":"3","toHostName":"SWEAS0000002DV"},{"toIpAddress":"192.168.4.73","id":"15","toHostName":"SWEAS0000005DV"},{"toIpAddress":"192.168.4.74","id":"16","toHostName":"SWEAS0000006DV"}],"nni_mode":"B","role":"EFS","sourceMacAddress":"B8-E7-79-00-00-02","ipAddress":"192.168.4.70","id":"SWEFS0000002DV","mepId":"2","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"1","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"2","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-00-00-01","ipAddress":"192.168.4.67","id":"SWEAS0000001DV","mepId":"3","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"1","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"2","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-00-00-02","ipAddress":"192.168.4.68","id":"SWEAS0000002DV","mepId":"4","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"15","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"16","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-01-00-01","ipAddress":"192.168.4.73","id":"SWEAS0000005DV","mepId":"5","samInstanceId":"SAM"},{"tunnels":[{"toIpAddress":"192.168.4.69","id":"15","toHostName":"SWEFS0000001DV"},{"toIpAddress":"192.168.4.70","id":"16","toHostName":"SWEFS0000002DV"}],"role":"EAS","sourceMacAddress":"AC-DE-48-01-00-02","ipAddress":"192.168.4.74","id":"SWEAS0000006DV","mepId":"6","samInstanceId":"SAM"}],"connectionProfileId":"2","enni_lag":[{"hostName":"SWEFS0000001DV","nni_lag_name":"ALG-02047","id":"ALG-02047","ports":[{"port":"7","daughterCard":"1","shelf":"1","cardSlot":"5"}],"type":"lag"},{"hostName":"SWEFS0000002DV","nni_lag_name":"ALG-02048","id":"ALG-02048","ports":[{"port":"7","daughterCard":"1","shelf":"1","cardSlot":"5"}],"type":"lag"}],"sVLanId":"2740","routeType":"State","uniVLanId":"20-22","priId":"BPI000000011551","maximumFrameSize":"Standard"},"id":"OVC000000012822","type":"enterpriseService","relationShips":{"id":"BPI000000011551","data":{"csaId":"CSA180000002222","accessSeekerId":"ASI500050005000","locationId":"LOC123456799999","samId":"2ARM-01","enterpriseEthernetServiceLevelRegion":"Regional Centre","term":"12 months","templateVersion":"1.0","accessTechnologyType":"Fibre","templateId":"TPL900000000001","productType":"EEAS","serviceRestorationSLA":"Enhanced - 12 (24/7)"},"type":"eeasProductInstance","status":"active"},"connectedTo":[{"data":{"portNo":"4"},"id":"SWBTD0009200-1_4","type":"btdPort"},{"data":{"btdType":"Standard (CSAS)","btdMounting":"Rack","physicalName":"3VRB-00-00-BTD-0012","locationId":"LOC123456799999","ipAddress":"10.0.0.1","powerSupply2":"Not Required","powerSupply1":"DC(-d8V to -22V)","subnetMask":"255.255.255.240","type":"BTD","status":"planned"},"id":"SWBTD0009200","type":"btd"},{"data":{"resources":[{"lagId":3,"lagDescription":"ALG-00010","id":"SWAAS9009998","ports":[{"port":3,"slot":4}]},{"lagId":1,"lagDescription":"ALG-00004","id":"SWBTD0009200","ports":[{"port":1,"slot":1}]}]},"id":"LAG000000002695","type":"lag"},{"data":{"physicalName":"3VRB-00-00-AAS-0092","ipAddress":"192.168.99.18"},"id":"SWAAS9009998","type":"aas"}],"status":"active"}]';
 
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

        // Set up commUser to run test as
        commUser = DF_TestData.createDFCommUser();      

        system.runAs(commUser) { 
            test.startTest();           
            
            result = DF_SvcCacheService.searchServiceCache('UNI900000002986');
            

                                            
            test.stopTest();                                             
        }   
       //system.assertEquals(result.ovcs[0].ovcId,'OVC000000012822');
        
        // Assertions
        //List<DF_Order__c> dfOrderList = [SELECT Id
        //                                 FROM   DF_Order__c 
        //                                 WHERE  Id = :dfOrder.Id
        //                                 AND    Order_Validation_Status__c = :DF_SvcCacheServiceUtils.ORDER_VALIDATION_STATUS_SUCCESS];

        //system.AssertEquals(false, dfOrderList.isEmpty());
    }    

     @isTest static void test_SVCCache_404() {  
        String response;
        User commUser;
        DF_SvcCacheSearchResults result;

        // Generate mock response
        response = '{"status":"NOT_FOUND","message":"Resource not found in Service Cache","debugMessage":"Requested resource(s) not found","timestamp":"2018-09-05T10:47:16.741","errorCode":"004040"}';
 
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(404, response, null));

        // Set up commUser to run test as
        commUser = DF_TestData.createDFCommUser();      

        system.runAs(commUser) { 
            test.startTest();           
            
            result = DF_SvcCacheService.searchServiceCache('OVC000000000000');
            system.debug('!!! result '+ result);

             DF_SvcCacheService.searchServiceCache('Test');                               
            test.stopTest();                                             
        }   
        	system.assertEquals(result.message, 'Resource not found in Service Cache');

    }   


     @isTest static void test_SVCCache_nullResp() {  
        String response;
        User commUser;

        // Generate mock response
        response = '';
 
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(500, response, null));

        // Set up commUser to run test as
        commUser = DF_TestData.createDFCommUser();      

        system.runAs(commUser) { 
            test.startTest();           
            
            DF_SvcCacheService.searchServiceCache('OVC000000001635');
                                            
            test.stopTest();                                             
        }   
        

    }  
    
    

    

    
}