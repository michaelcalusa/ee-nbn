public with sharing class DF_SvcCacheServiceUtils{
	
	public static final String INTEGRATION_SETTING_NAME = 'Service_Cache';
    public static final String ACTION_ADD = 'GET';

    public static final String ERR_ACCESS_SEEKER_ID_NOT_FOUND = 'accessSeekerId for RSP Account was not found.';    
    public static final String ERR_RESP_BODY_MISSING = 'Response Body is missing.'; 
    public static final String ERR_RESP_BODY_STATUS_NOT_FOUND = 'Response - Status not found.';

    
    public static final String RESP_BODY_STATUS_SUCCESS = 'SUCCESS';
    public static final String RESP_BODY_STATUS_FAILED = 'FAILED';

    
    public static final String ACCESS_SEEKER_ID = 'accessSeekerId';
    public static final String CORRELATION_ID = 'correlationId';
    public static final String ACTIVE_SERVICES = 'status=active';
    public static final String PRODUCT_TYPE = 'type=enterpriseService';
    public static final String CONNECTED_TO = 'connectedTo=true';


    public static HttpRequest getHttpRequest(String endPoint, Integer timeoutLimit) {      

		DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
													Timeout__c,
													DP_Environment_Override__c
											 FROM   DF_Integration_Setting__mdt
											 WHERE  DeveloperName = :INTEGRATION_SETTING_NAME LIMIT 1];             

        HttpRequest req = new HttpRequest();        
        String environment = integrSetting.DP_Environment_Override__c;
        String accessSeekerId;
        String correlationId;

        try {    
            // Get access seeker id
            accessSeekerId = getAccessSeekerId(); // Can only test via Community
            // Generate correlationid
			correlationId = generateGUID();

            //construct endpoint
            endpoint = endpoint+'?'+CONNECTED_TO+'&'+ACTIVE_SERVICES+'&'+PRODUCT_TYPE+'&'+ACCESS_SEEKER_ID+'='+accessSeekerId;


            req.setEndpoint(endpoint);      
            req.setMethod(DF_Constants.HTTP_METHOD_GET);            
            req.setTimeout(timeoutLimit);                
                                
            //req.setHeader(DF_IntegrationUtils.NBN_ENV_OVRD, environment); 
            req.setHeader(DF_SvcCacheServiceUtils.CORRELATION_ID, correlationId); 
            req.setHeader(DF_Constants.CONTENT_TYPE, DF_Constants.CONTENT_TYPE_JSON);  
            req.setHeader(DF_Constants.HTTP_HEADER_ACCEPT, DF_Constants.CONTENT_TYPE_JSON);
                     
        } catch (Exception e) {  
            throw new CustomException(e.getMessage());
        }
         	//system.debug('!!!!! req' + req);
        return req;
    } 


    public static String getAccessSeekerId() {
    Account acct;                                
    Id commUserAccountId;
    String accessSeekerId;

        try {
            commUserAccountId = DF_SF_CaptureController.getCommUserAccountId();      

            if (commUserAccountId != null) {                                
                acct = [SELECT Access_Seeker_ID__c
                        FROM   Account 
                        WHERE  Id = :commUserAccountId];

                if (acct != null) {                                 
                    if (acct.Access_Seeker_ID__c != null) {         
                        accessSeekerId = acct.Access_Seeker_ID__c;
                    }
                }                       
            }                                                                      
        } catch (Exception e) {    
            throw new CustomException(e.getMessage());
        }                     
                    
        return accessSeekerId;
    }

     public static String generateGUID() {       
        String kHexChars = '0123456789abcdef';
        String guId = '';
        Integer nextByte = 0;

        try {
            for (Integer i = 0; i < 16; i++) {
                if (i == 4 || i == 6 || i == 8 || i == 10) {
                    guId+= '-';
                }
    
                nextByte = (Math.round(Math.random() * 255)-128) & 255;
    
                if (i == 6) {
                    nextByte = nextByte & 15;
                    nextByte = nextByte | (4 << 4);
                }
    
                if (i == 8) {
                    nextByte = nextByte & 63;
                    nextByte = nextByte | 128;
                }
    
                guId+= getCharAtIndex(kHexChars, nextByte >> 4);
                guId+= getCharAtIndex(kHexChars, nextByte & 15);
            }
    
            guId = trimStringToSize(guId, 36);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return guId;
    }       

     public static String trimStringToSize(String inputString, Integer maxLength) {   
        String outputString;

        try {           
            if (String.isNotEmpty(inputString) && maxLength != null && maxLength > 0) {
                if (inputString.length() > maxLength) {                                                 
                    outputString = inputString.substring(0, maxLength);
                } else {
                    outputString = inputString;
                }
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return outputString;
    }      
    @TestVisible private static String getCharAtIndex(String str, Integer index) {      
        try {
            if (str == null) {
                return null;
            } 
    
            if (str.length() <= 0) {
                return str;
            } 
    
            if (index == str.length()) {
                return null;
            }   
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return str.substring(index, index + 1);
    } 
    public static String stripJsonNulls(String jsonString) {
        try {
            if (jsonString != null) {
                jsonString = jsonString.replaceAll('\"[^\"]*\":null',''); //basic removal of null values
                jsonString = jsonString.replaceAll(',{2,}', ','); //remove duplicate/multiple commas
                jsonString = jsonString.replace('{,', '{'); //prevent opening brace from having a comma after it
                jsonString = jsonString.replace(',}', '}'); //prevent closing brace from having a comma before it
                jsonString = jsonString.replace('[,', '['); //prevent opening bracket from having a comma after it
                jsonString = jsonString.replace(',]', ']'); //prevent closing bracket from having a comma before it
            }           
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return jsonString;
    }     
    //todo Handle bad requests 
    public static DF_SvcCacheSearchResults getServiceCacheResponse(String response) {                      
        String status;
        List<DF_SvcCacheActiveSvcsRespJSON> products = new List<DF_SvcCacheActiveSvcsRespJSON> ();
        DF_SvcCacheSearchResults searchResult =  new DF_SvcCacheSearchResults();
        try {
        	response = stripJsonNulls(response);
        	system.debug('!!! response'+response);
            // Get root node of response        
            products = (List<DF_SvcCacheActiveSvcsRespJSON>)JSON.deserialize(response, List<DF_SvcCacheActiveSvcsRespJSON>.Class);

            //DEBUG dev code clean up
            for(DF_SvcCacheActiveSvcsRespJSON prod : products){

            	system.debug('!!! getServiceCacheResponse prod ' + prod);
            }
           	searchResult =  searchResultCleanUp(products);
            //DEBUG dev code clean up
            if (products != null) {    
            system.debug('!!! + products '+products) ;
                status = products[0].status;
            } else {                
                throw new CustomException(DF_SvcCacheServiceUtils.ERR_RESP_BODY_STATUS_NOT_FOUND);
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return searchResult;
    }  

    public static DF_SvcCacheSearchResults searchResultCleanUp(List<DF_SvcCacheActiveSvcsRespJSON> searchResults){
    	DF_SvcCacheSearchResults searchResult =  new DF_SvcCacheSearchResults();
    	List<DF_SvcCacheSearchResults.OVC> tempOVCs = new List<DF_SvcCacheSearchResults.OVC>();

        String bpiId = searchResults[0].data.priId;
        if(bpiId!=null){
            List<DF_Order__c> orderList = getOrderFromBpiId(bpiId);
            if(!orderList.isEmpty()){
                searchResult.address = orderList[0].DF_Quote__r.Address__c;
                searchResult.df_OrderId = orderList[0].Id;
                searchResult.df_QuoteId = orderList[0].DF_Quote__c;
				searchResult.df_OrderName = orderList[0].Name;
                searchResult.df_OrderDateCreated = orderList[0].Order_Submitted_Date__c;
                searchResult.df_ExternalOrderId = orderList[0].Order_Id__c;
            }
        }

    	for(DF_SvcCacheActiveSvcsRespJSON prod : searchResults){
    			DF_SvcCacheSearchResults.OVC tempOVC = new DF_SvcCacheSearchResults.OVC();
            	searchResult.bpiId = prod.data.priId;

            	for(DF_SvcCacheActiveSvcsRespJSON.connectedTo btd:prod.connectedTo){
            	//only select BTD from connectTo list
	            	if(btd.type =='BTD'){
	            		searchResult.btdId = btd.id;
	            		searchResult.btdType = btd.data.btdType;
	            		searchResult.locationId = btd.data.locationId;
	            	}
	            
            	}
            	searchResult.uniId = prod.data.uni.id;
            	searchResult.interfacetype = prod.data.uni.interfaceType;
            	searchResult.tpid = prod.data.uni.tpId;
            	searchResult.term = prod.RelationShips.data.term;
            	searchResult.ovcType = prod.data.uni.ovcType;
            	searchResult.zone = prod.RelationShips.data.zone;
                searchResult.enterpriseEthernetServiceLevelRegion = prod.RelationShips.data.enterpriseEthernetServiceLevelRegion;
                searchResult.interfaceBandwidth = prod.data.uni.interfaceBandwidth;
            	searchResult.sla = prod.RelationShips.data.serviceRestorationSLA;
				searchResult.accessSeekerId = prod.RelationShips.data.accessSeekerId;
            	system.debug('searchResult' + searchResult);
            	tempOVC.ovcId = prod.id;
            	tempOVC.nni = prod.data.Nni.id;
            	tempOVC.routeType =  prod.Data.routeType;
            	tempOVC.sVLanId = prod.Data.sVLanId;
            	tempOVC.uniVLanId = prod.Data.uniVLanId;
            	tempOVC.cosLowBandwidth = prod.Data.cosLowBandwidth;
            	tempOVC.cosMediumBandwidth = prod.Data.cosMediumBandwidth;
            	tempOVC.cosHighBandwidth = prod.Data.cosHighBandwidth;
            	tempOVC.poi = prod.Data.poi;
            	tempOVC.maximumFrameSize = prod.Data.maximumFrameSize;
            	tempOVC.cosMappingMode = prod.Data.cosMappingMode;
            	system.debug('tempOVC'+tempOVC);
            	tempOVCs.add(tempOVC);
				
            	



        }

		searchResult.ovcs = tempOVCs;
		system.debug('searchResult' + searchResult);

        return searchResult;
    }

    public static DF_SvcCacheSearchResults processError(){
		DF_SvcCacheSearchResults searchResult = new DF_SvcCacheSearchResults();
		searchResult.status = 'Error';
    	return searchResult;
    }

    public static DF_SvcCacheSearchResults processNotFound(string response){
		DF_SvcCacheSearchResults searchResult = new DF_SvcCacheSearchResults();
		List<DF_SvcCacheActiveSvcsRespJSON> products = new List<DF_SvcCacheActiveSvcsRespJSON> ();
		products = (List<DF_SvcCacheActiveSvcsRespJSON>)JSON.deserialize('['+response+']', List<DF_SvcCacheActiveSvcsRespJSON>.Class);
		for(DF_SvcCacheActiveSvcsRespJSON prod : products){
			searchResult.status = prod.status;
			searchResult.message = prod.message;
			searchResult.debugMessage =  prod.debugMessage;
			searchResult.errorMessage = prod.debugMessage;

		}

    	return searchResult;
    }
//probably should remove this
    public static DF_SvcCacheSearchResults returnTestData(){
    	DF_SvcCacheSearchResults searchResult = new DF_SvcCacheSearchResults();
    	DF_SvcCacheSearchResults.OVC tempOVC = new DF_SvcCacheSearchResults.OVC();
    	List<DF_SvcCacheSearchResults.OVC> tempOVCList = new List<DF_SvcCacheSearchResults.OVC>();

    			searchResult.bpiId = 'BPI000123123123';
        		searchResult.btdId = 'SWBTD0009200';
        		searchResult.btdType = 'Standard (CSAS)';
        		searchResult.locationId = 'LOC123456799999';
        		searchResult.interfaceBandwidth = '5GB';
            	searchResult.uniId = 'UNI900000002986';
            	searchResult.interfacetype = 'Copper (Auto-negotiate)';
            	searchResult.tpid = '0x9100';
            	searchResult.term = '12 months';
            	searchResult.ovcType = 'Access EVPL';
            	searchResult.zone = 'Missing from SvcCache will be added later';
            	searchResult.sla = 'Enhanced - 12 (24/7)';
            	system.debug('searchResult' + searchResult);

            	tempOVC.ovcId = 'OVC000000012814';
            	tempOVC.routeType =  'State';
            	tempOVC.sVLanId = '2763';
            	tempOVC.cosLowBandwidth = '10Mbps';
            	tempOVC.cosMediumBandwidth = '20Mbps';
            	tempOVC.cosHighBandwidth = '30Mbps';
            	tempOVC.poi = '1-MEL-MELBOURNE-MP';
            	tempOVC.maximumFrameSize = 'Jumbo (9000 Bytes)';
            	tempOVC.uniVLanId = '55';
            	tempOVC.cosMappingMode = 'PCP';
            	system.debug('tempOVC'+tempOVC);
            	tempOVCList.add(tempOVC);
            	tempOVC = new DF_SvcCacheSearchResults.OVC();
            	tempOVC.ovcId = 'OVC000000012822';
            	tempOVC.routeType =  'Local';
            	tempOVC.sVLanId = '2220';
            	tempOVC.cosLowBandwidth = '10Mbps';
            	tempOVC.cosMediumBandwidth = '20Mbps';
            	tempOVC.cosHighBandwidth = '30Mbps';
            	tempOVC.poi = '1-MEL-MELBOURNE-MP';
            	tempOVC.maximumFrameSize = 'Jumbo (9000 Bytes)';
            	tempOVC.uniVLanId = '66';
            	tempOVC.cosMappingMode = 'PCP';
            	tempOVCList.add(tempOVC);
            	system.debug('tempOVC'+tempOVC);
            	searchResult.ovcs = tempOVCList;

            	system.debug(searchResult);

    	return searchResult;



    }
    public static List<DF_Order__c> getOrderFromBpiId(String bpiId){
        // Process only Complete orders. 
        List<DF_Order__c> order = new List<DF_Order__c>();
        order = [SELECT Id, Name, DF_Quote__c,DF_Quote__r.Address__c, Order_Submitted_Date__c, Order_Id__c FROM DF_Order__c WHERE BPI_Id__c =: bpiId AND Order_Status__c = 'Complete' LIMIT 1] ;
        return order;
    }
   
}