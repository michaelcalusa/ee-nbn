@isTest
private class DF_SvcCacheServiceUtilsTest {
	
    @isTest static void test_getHttpRequest() {                     
        // Setup data   
        final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;
        String requestStr = 'requestStr';
        HttpRequest req;
        User commUser;

        DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
                                                            Timeout__c
                                                     FROM   DF_Integration_Setting__mdt
                                                     WHERE  DeveloperName = :DF_SvcCacheServiceUtils.INTEGRATION_SETTING_NAME];

        NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
        TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);

        END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL;
        
        // Create Acct
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.Order_GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55bada';
        insert dfQuote;
        
        // Build order json
        String dfOrderJSONString = DF_IntegrationTestData.buildDFOrderJSONString();
        
        // Create DF Order
        DF_Order__c dfOrder = DF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        dfOrder.Order_JSON__c = dfOrderJSONString;
        dfOrder.DF_Quote__c = dfQuote.Id;
        insert dfOrder;
        
        commUser = DF_TestData.createDFCommUser();          
        
        system.runAs(commUser) {
            // Statements to be executed by this test user
            test.startTest();           
    
            req = DF_SvcCacheServiceUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT);
                                            
            test.stopTest();                
        }                    

        // Assertions
        system.AssertNotEquals(null, req);
    }  

        @isTest static void test_generateGUID() {                   
        // Setup data   
        String correlationId;  

        test.startTest();           

        correlationId = DF_SvcCacheServiceUtils.generateGUID();
                                        
        test.stopTest();               

        // Assertions
        system.AssertNotEquals(null, correlationId);
    }     
    
    @isTest static void test_stripJsonNulls() {                     
        // Setup data   
        String jsonStr;
        Map<String, String> dataMap = new Map<String, String>();  
        
        dataMap.put('key1', 'value1');
        dataMap.put('key2', 'value2');
        dataMap.put('key3', 'value3');
        dataMap.put('key4', null);

        jsonStr = JSON.serialize(dataMap);

        test.startTest();           
        
        jsonStr = DF_SvcCacheServiceUtils.stripJsonNulls(jsonStr);
                                        
        test.stopTest();               

        // Assertions
        system.AssertNotEquals(null, jsonStr);
    }  
        @isTest static void test_trimStringToSize() {                   
        // Setup data   
        String string1 = 'stringtokeepSTRINGTOEXCLUDE'; 
        String string2;
        
        test.startTest();           
        
        string2 = DF_SvcCacheServiceUtils.trimStringToSize(string1, 12);
                                        
        test.stopTest();               

        // Assertions
        system.AssertEquals('stringtokeep', string2);
    }   

            @isTest static void test_processError() {                   
        // Setup data   
        
        test.startTest();           
        DF_SvcCacheSearchResults r = DF_SvcCacheServiceUtils.returnTestData();
        DF_SvcCacheSearchResults string2 = DF_SvcCacheServiceUtils.processError();
                                        
        test.stopTest();               

        // Assertions
        
    }   
    @isTest static void getCharAtIndex_test(){
    	String one = null;
    	String two = '';
    	String three = 'asdf';
    	String result;

    	Test.startTest();
    	result = DF_SvcCacheServiceUtils.getCharAtIndex(one,0);
    	System.assertEquals(result,null);

    	result = DF_SvcCacheServiceUtils.getCharAtIndex(two,0);
    	System.assertEquals(result,result);

    	result = DF_SvcCacheServiceUtils.getCharAtIndex(three,0);
    	System.assertEquals(result,'a');
    	Test.stopTest();


    }
	
}