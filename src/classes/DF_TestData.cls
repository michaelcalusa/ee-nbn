@IsTest
public class DF_TestData {

    // Create Account
    public Static Account createAccount(String name) {
        Account a = new Account();
        a.Name = name;
        a.Commercial_Deal__c= true;
        
        return a;
    }
    
    //Create Commercial Deal record
    public static Commercial_Deal__c commercialDeal(Id accountId){
        Commercial_Deal__c deal= new Commercial_Deal__c();
        deal.Account__c = accountId;
        deal.Active__c = true;
        deal.Start_Date__c = system.today().addDays(-2);
        deal.End_Date__c = system.today().addDays(6);
        deal.Deal_Module_Reference__c = 'Test';
        deal.ETP_Applies__c = 'Applicable';
        return deal;
    }
    
    //Create Commercial Deal Criteria Record
    public static Commercial_Deal_Criteria__c commercialDealCriteria(Id dealId){
        Commercial_Deal_Criteria__c dealCriteria= new Commercial_Deal_Criteria__c();
        dealCriteria.Commercial_Deal__c = dealId;
        dealCriteria.Criteria__c = 'Loc Id';
        //dealCriteria.Criteria_Value__c = 'LOC000006231845';
        dealCriteria.Start_Date__c = system.today().addDays(-2);
        dealCriteria.End_Date__c = system.today().addDays(6);
        return dealCriteria;
    }
    
    //Create csord__Order__c record
    public static csord__Order__c csOrder(Id OppId,Id accountId){
        csord__Order__c order = new csord__Order__c();
        order.csord__Account__c = accountId;
        order.csordtelcoa__Opportunity__c= OppId;
        order.name = 'Direct Fibre - Site Survey Charges';
        order.csord__Identification__c = 'Order_a1hN0000001UNpfIAG_0';
        return order;
    }
    
    //Create cspmb__Price_Item__c record
    public static cspmb__Price_Item__c priceItemRecordSiteFeasibility(){
        cspmb__Price_Item__c pItem = new cspmb__Price_Item__c();
        pItem.cspmb__One_Off_Charge__c= 1500;
        pItem.cspmb__Price_Item_Code__c = 'Site Feasibility';
        return pItem;
    }
    
    //Create cspmb__Price_Item__c record
    public static cspmb__Price_Item__c priceItemRecordDBP(){
        cspmb__Price_Item__c pItem = new cspmb__Price_Item__c();
        pItem.cspmb__Price_Item_Code__c = 'SFC DBP Discount';
        pItem.Discount_Value__c = '100';
        return pItem;
    }
     
    // Retrieve Account
    public Static Account getAccount(Id accountId){
        List<Account> accounts = [SELECT Id, Name FROM Account WHERE Id = :accountId];
        if(!accounts.isEmpty()){
            return accounts[0];
        } else {
            return null;
        }
        
    }
    
    //Create empty basket
    public static cscfga__Product_Basket__c buildEmptyBasket() {
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        return basket;
    }
    
    //Create basket
    public static cscfga__Product_Basket__c buildBasket() {
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.cscfga__Basket_Status__c = 'Valid';
        basket.csbb__Synchronised_with_Opportunity__c = false;
        basket.Name = 'TestBasket';
        cscfga__User_Session__c s = buildUserSession();
        insert s;
        basket.cscfga__User_Session__c = s.Id;
        return basket;
    }
    
    //Create user session
    public static cscfga__User_Session__c buildUserSession() {
        cscfga__User_Session__c session = new cscfga__User_Session__c();
        session.Name = 'User Session';
        session.cscfga__PC_Return_URL__c = '/';
        return session;
    }
    
    //Create product definition
    public static cscfga__Product_Definition__c buildProductDefinition(String productName, String planName) {
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.Name = productName;
        pd.cscfga__Description__c = productName;
        return pd;
    }
    
    //create attribute
    public static cscfga__Attribute__c buildAttribute(String name, String value, Id productConfigId) {
       cscfga__Attribute__c att = new cscfga__Attribute__c();
       att.name = name;
       att.cscfga__Value__c = value;
       att.cscfga__Product_Configuration__c = productConfigId;
       return att;
    }
    
    //create product configuration
    public static cscfga__Product_Configuration__c buildProductConfig(Id productDefId) {
        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c();
        config.name = 'test config';
        config.cscfga__Product_Definition__c = productDefId;
        return config;
    }

    //create product category
    public static cscfga__Product_Category__c   buildProductCategory() {
        cscfga__Product_Category__c   pc =  new cscfga__Product_Category__c  ();
        pc.name = 'Business';
        pc.cscfga__Active__c  = true;
        pc.cscfga__Browseable__c  = true;
        return pc;
    }
    
    //create offer
    public static cscfga__Configuration_Offer__c createOffer(String name) {
        cscfga__Configuration_Offer__c  offer =  new cscfga__Configuration_Offer__c ();
        offer.cscfga__Active__c = true;
        offer.Name = name;
        return offer;
    }
    
    //create callout results
    public static csbb__Callout_Result__c  buildCallOutResults() {
        csbb__Callout_Result__c coResults = new csbb__Callout_Result__c ();
        coResults.csbb__SOAP_Request_Message__c = '<?xml version="1.0" encoding="UTF-8"?><env:Envelope><env:Body><sqCheck xmlns="http://soap.sforce.com/schemas/class/SQAvailabilityCheck"><addressId>112233</addressId></sqCheck ></env:Body></env:Envelope>';
        return coResults;
    }
    
    //Create callout product results
    public static csbb__Callout_Product_Result__c  buildCalloutProductResults(csbb__Product_Configuration_Request__c  pcr, csbb__Callout_Result__c cor) {
        csbb__Callout_Product_Result__c  productRes = new csbb__Callout_Product_Result__c ();
        productRes.csbb__Product_Configuration_Request__c = pcr.id;
        productRes.csbb__Callout_Result__c = cor.id;
        return productRes;
    }
    
    //create product definition
    public static cscfga__Attribute_Definition__c buildAttributeDefinition(String name, Id productDefinitionId) {
        cscfga__Attribute_Definition__c attDef = new cscfga__Attribute_Definition__c();
        attDef.name = name;
        attDef.cscfga__Label__c = name;
        attDef.cscfga__Product_Definition__c = productDefinitionId;
        return attDef;
    }
    
    //create product definition
    public static cscfga__Attribute_Definition__c buildAttributeDefinitionQQ(String name, Id productDefinitionId, String dt) {
        cscfga__Attribute_Definition__c attDef = new cscfga__Attribute_Definition__c();
        attDef.name = name;
        attDef.cscfga__Label__c = name;
        attDef.cscfga__Product_Definition__c = productDefinitionId;
        attDef.cscfga__Is_Line_Item__c = true;
        attDef.cscfga__Data_Type__c = dt;
        attDef.cscfga__Enable_null_option__c = true;
        attDef.csordtelcoa__Calculate_Delta__c = true;
        return attDef;
    }
    
    //create product definition
    public static cscfga__Select_Option__c buildSelectOptions(String name, cscfga__Attribute_Definition__c ad) {
        cscfga__Select_Option__c so = new cscfga__Select_Option__c();
        so.name = name;
        so.cscfga__Value__c = name;
        so.cscfga__Attribute_Definition__c = ad.Id;
        return so;
    }
    
    //create product definition
    public static cscfga__Attribute_Definition__c buildAttributeDefinitionSelectOptions(String name, Id productDefinitionId, String dt) {
        cscfga__Attribute_Definition__c attDef = new cscfga__Attribute_Definition__c();
        attDef.name = name;
        attDef.cscfga__Label__c = name;
        attDef.cscfga__Product_Definition__c = productDefinitionId;
        attDef.cscfga__Is_Line_Item__c = true;
        attDef.cscfga__Data_Type__c = dt;
        attDef.cscfga__Type__c = 'Select List';
        attDef.cscfga__Enable_null_option__c = true;
        attDef.csordtelcoa__Calculate_Delta__c = true;
        return attDef;
    }

    //create attribute field definition
    public static cscfga__Attribute_Field_Definition__c buildAttributeFieldDefinition(String name, Id attrDefinitionId,String defVal) {
        cscfga__Attribute_Field_Definition__c attFieldDef = new cscfga__Attribute_Field_Definition__c();
        attFieldDef.name = name;
        attFieldDef.cscfga__Default_Value__c = defVal;
        attFieldDef.cscfga__Attribute_Definition__c = attrDefinitionId;
        return attFieldDef;
    }
    
    //create price attribute
    public static cscfga__Attribute__c buildPriceAttribute(String name, String value, Id productConfigId, Boolean isRC, Decimal price) {
       cscfga__Attribute__c att = new cscfga__Attribute__c();
       att.name = name;
       att.cscfga__Value__c = value;
       att.cscfga__Product_Configuration__c = productConfigId;
       att.cscfga__Is_Line_Item__c = isRC;
       att.cscfga__Price__c = price;
       return att;
    }
    
    //create screen flow
    public static cscfga__Screen_Flow__c  buildScreenFlow(String sfName) {
        cscfga__Screen_Flow__c sf = new cscfga__Screen_Flow__c(name = sfName);
        sf.cscfga__Template_Reference__c = 'Test screen flow Ref';
        return sf;
    }
    
    //create screen flow product association
    public static cscfga__Screen_Flow_Product_Association__c buildSFAssociation(Id prodDefId, Id sfId) {
        cscfga__Screen_Flow_Product_Association__c sfA = new cscfga__Screen_Flow_Product_Association__c();
        sfA.cscfga__Product_Definition__c = prodDefId;
        sfA.cscfga__Screen_Flow__c = sfId;
        return sfA;
    }
    
    //create opportunity
    public static Opportunity createOpportunity(String name){
        DF_Custom_Options__c cs = new DF_Custom_Options__c(name = 'SF_RECORDTYPE_DEVELOPER_NAME', Value__c = 'Enterprise_Ethernet');
        insert cs;
        Opportunity opp = new Opportunity();
        opp.Name = name;
        opp.StageName = 'New';
        opp.CloseDate = System.today();
        opp.RecordTypeId = DF_CS_API_Util.getDFRecordType();
        return opp;
    }
    
    //create opportunity bundle record
    public static DF_Opportunity_Bundle__c createOpportunityBundle(Id acct){
        DF_Opportunity_Bundle__c opp = new DF_Opportunity_Bundle__c();
        opp.Account__c = acct;
        opp.recordTypeId = DF_CS_API_Util.getRecordType(DF_Constants.ENTERPRISE_ETHERNET_NAME, DF_Constants.OPPORTUNITY_BUNDLE_OBJECT);
        return opp;
    }
    
    //create rules in the Fibre build category rule object
    public static DF_FBC_Rule__c createRagRuleTable (String tech, Decimal min, Decimal max, String val){
        DF_FBC_Rule__c fbc = new DF_FBC_Rule__c();
        fbc.Access_Technology__c = tech;
        fbc.Minimum_Distance__c = min;
        fbc.Maximum_Distance__c = max;
        fbc.RAG_Status__c = val;
        fbc.recordTypeId = DF_CS_API_Util.getRecordType(DF_Constants.ENTERPRISE_ETHERNET_NAME, DF_Constants.FBC_RULE_OBJECT);
        return fbc;
    }
    
    //create service feasibility request records
    public static DF_SF_Request__c createSFRequest(String add, String lat, String longit, String locId, Id oppId, String resp){
        DF_SF_Request__c sfr = new DF_SF_Request__c();
        sfr.Address__c = add;
        sfr.Latitude__c = lat;
        sfr.Longitude__c = longit;
        sfr.Location_Id__c = locId;
        sfr.Opportunity_Bundle__c = oppId;
        sfr.Response__c = resp;
        return sfr;
    }

    //create service feasibility request records (version 2)
    public static DF_SF_Request__c createSFRequest(String searchType, String status, String locId, String latitude, String longitude, Id opptyBundleId, String response, Map<String, String> addressMap) {
        DF_SF_Request__c sfReq = new DF_SF_Request__c();
        sfReq.Search_Type__c = searchType;
        sfReq.Status__c = status;
        sfReq.Location_Id__c = locId;
        sfReq.Latitude__c = latitude;       
        sfReq.Longitude__c = longitude;
        sfReq.Opportunity_Bundle__c = opptyBundleId;            
        sfReq.Response__c = response;       

        if (!addressMap.isEmpty()) {                    
            sfReq.State__c = addressMap.get('state');
            sfReq.Postcode__c = addressMap.get('postcode');
            sfReq.Suburb_Locality__c = addressMap.get('suburbLocality');
            sfReq.Street_Name__c = addressMap.get('streetName');
            sfReq.Street_Type__c = addressMap.get('streetType');
            sfReq.Street_or_Lot_Number__c = addressMap.get('streetLotNumber');
            sfReq.Unit_Type__c = addressMap.get('unitType');
            sfReq.Unit_Number__c = addressMap.get('unitNumber');
            sfReq.Level__c = addressMap.get('level');
            sfReq.Complex_or_Site_Name__c = addressMap.get('complexSiteName');
            sfReq.Building_Name_in_a_Complex__c = addressMap.get('complexBuildingName');
            sfReq.Street_Name_in_a_Complex__c = addressMap.get('complexStreetName');
            sfReq.Street_Type_in_a_Complex__c = addressMap.get('complexStreetType');
            sfReq.Street_Number_in_a_Complex__c = addressMap.get('complexStreetNumber');                                
        }   

        return sfReq;
    }
    
    //create df quote records
    public static DF_Quote__c createDFQuote(String add, String lat, String longit, String locId, Id oppId, Id parentOppId, Decimal cost, String rag){
        DF_Quote__c q = new DF_Quote__c();
        q.Address__c = add;
        q.Latitude__c = lat;
        q.Longitude__c = longit;
        q.Location_Id__c = locId;
        q.GUID__c = 'testGUID';
        q.Opportunity__c = oppId;
        q.Opportunity_Bundle__c = parentOppId;
        q.Fibre_Build_Cost__c = cost;
        //q.RAG__c = rag;
        q.Fibre_Build_Category__c= rag;
        q.recordTypeId = DF_CS_API_Util.getRecordType(DF_Constants.ENTERPRISE_ETHERNET_NAME, DF_Constants.QUOTE_OBJECT);
        return q;
    }
    
    //create df quote records
    public static DF_Quote__c createDFQuote(String add, String lat, String longit, String locId, Id oppId, Id parentOppId, Decimal cost, String rag, String response){
        DF_Quote__c q = new DF_Quote__c();
        q.Address__c = add;
        q.Latitude__c = lat;
        q.Longitude__c = longit;
        q.Location_Id__c = locId;
        q.Opportunity__c = oppId;
        q.GUID__c = 'testGUID';
        q.Opportunity_Bundle__c = parentOppId;
        q.Fibre_Build_Cost__c = cost;
        //q.RAG__c = rag;
        q.Fibre_Build_Category__c = rag;
        q.LAPI_Response__c = response;
        q.recordTypeId = DF_CS_API_Util.getRecordType(DF_Constants.ENTERPRISE_ETHERNET_NAME, DF_Constants.QUOTE_OBJECT);
        return q;
    }

    // Create DF Order record
    public static DF_Order__c createDFOrder(Id dfQuoteId, Id opptyBundleId, String orderStatus) {
        DF_Order__c dfOrder = new DF_Order__c();
        
        dfOrder.DF_Quote__c = dfQuoteId;
        dfOrder.Opportunity_Bundle__c = opptyBundleId;
        dfOrder.Order_Status__c = orderStatus;
        dfOrder.BPI_Id__c = 'BPI000000000000';
        dfOrder.Order_Version__c=1;
        dfOrder.OrderType__c='Connect';
        dfOrder.recordTypeId = DF_CS_API_Util.getRecordType(DF_Constants.ENTERPRISE_ETHERNET_NAME, DF_Constants.ORDER_OBJECT);
        return dfOrder;
    }
    
    //creates values for custom settings
    public static List<DF_Custom_Options__c> createCustomSettings(){
        List<DF_Custom_Options__c> csList = new List<DF_Custom_Options__c>();
        DF_Custom_Options__c cs1 = new DF_Custom_Options__c(name = 'SF_RECORDTYPE_DEVELOPER_NAME', Value__c = 'Enterprise_Ethernet');
        DF_Custom_Options__c cs2 = new DF_Custom_Options__c(name = 'SF_SITESTATUS', Value__c = 'Valid');
        DF_Custom_Options__c cs3 = new DF_Custom_Options__c(name = 'SF_STAGENAME', Value__c = 'New');
        DF_Custom_Options__c cs4 = new DF_Custom_Options__c(name = 'SF_UNAVAILABLE', Value__c = 'Unavailable');
        DF_Custom_Options__c cs5 = new DF_Custom_Options__c(name = 'BULK_UPLOAD_TEMPLATE', Value__c = 'https://nbn--DFDEVSB.cs72.my.salesforce.com/sfc/p/5D0000004iSY/a/5D00000000Wb/iz3b9Q5G_iNMUNsLnKuftIKJUk.RmIvGCJxNPYabapc');
        DF_Custom_Options__c cs6 = new DF_Custom_Options__c(name = 'PRODUCT_BASKET_PARALLEL_QUEUE_SIZE', Value__c = '1');
        DF_Custom_Options__c cs7 = new DF_Custom_Options__c(name = 'SF_DISTANCE_MULTIPLIER', Value__c = '1');
        DF_Custom_Options__c cs8 = new DF_Custom_Options__c(name = 'TOGGLE_OFF_ESTIMATED_DELIVERY_TIME', Value__c = 'false');
        DF_Custom_Options__c cs9 = new DF_Custom_Options__c(name = 'SF_EXPIRY_DURATION', Value__c = '0');
        csList.add(cs1);
        csList.add(cs2);
        csList.add(cs3);
        csList.add(cs4);
        csList.add(cs5);
        csList.add(cs6);
        csList.add(cs7);
        csList.add(cs8);
        csList.add(cs9);
        return csList;
    }

    //create custom settings for product charge
    public static List<DF_Custom_Options__c> createCustomSettingsforProdCharge(String category, String offer, String def){
        List<DF_Custom_Options__c> csList = new List<DF_Custom_Options__c>();
        DF_Custom_Options__c cs1 = new DF_Custom_Options__c(name = 'SF_CATEGORY_ID', Value__c = category);
        DF_Custom_Options__c cs2 = new DF_Custom_Options__c(name = 'SF_PRODUCT_CHARGE_OFFER_ID', Value__c = offer);
        DF_Custom_Options__c cs3 = new DF_Custom_Options__c(name = 'SF_PRODUCT_CHARGE_DEFINITION_ID', Value__c = def);
        csList.add(cs1);
        csList.add(cs2);
        csList.add(cs3);
        return csList;
    }
    
    //create custom settings for build contribution
    public static List<DF_Custom_Options__c> createCustomSettingsforBuildContribution(String category, String offer, String def){
        List<DF_Custom_Options__c> csList = new List<DF_Custom_Options__c>();
        DF_Custom_Options__c cs1 = new DF_Custom_Options__c(name = 'SF_CATEGORY_ID', Value__c = category);
        DF_Custom_Options__c cs2 = new DF_Custom_Options__c(name = 'SF_BUILD_CONTRIBUTION_OFFER_ID', Value__c = offer);
        DF_Custom_Options__c cs3 = new DF_Custom_Options__c(name = 'SF_BUILD_CONTRIBUTION_DEFINITION_ID', Value__c = def);
        csList.add(cs1);
        csList.add(cs2);
        csList.add(cs3);
        return csList;
    }
    
    //create custom settings for build contribution
    public static List<DF_Custom_Options__c> createCustomSettingsforOVC(String category, String offer, String def){
        List<DF_Custom_Options__c> csList = new List<DF_Custom_Options__c>();
        DF_Custom_Options__c cs1 = new DF_Custom_Options__c(name = 'SF_CATEGORY_ID', Value__c = category);
        DF_Custom_Options__c cs2 = new DF_Custom_Options__c(name = 'SF_OVC_DEFINITION_ID', Value__c = def);
        csList.add(cs1);
        csList.add(cs2);
        return csList;
    }
	
	//create custom settings for Order Cancellation Charges
    public static List<DF_AS_Custom_Settings__c> createCustomSettingsforCancellation(String category, String offer, String def){
        List<DF_AS_Custom_Settings__c> csList = new List<DF_AS_Custom_Settings__c>();
        DF_AS_Custom_Settings__c cs1 = new DF_AS_Custom_Settings__c(name = 'SF_INFLIGHT_CANCELLATION_DEFINITION_ID', Value__c = def);
        DF_AS_Custom_Settings__c cs2 = new DF_AS_Custom_Settings__c(name = 'SF_INFLIGHT_CANCELLATION_OFFER_ID', Value__c = offer);
        DF_AS_Custom_Settings__c cs3 = new DF_AS_Custom_Settings__c(name = 'SF_CATEGORY_ID', Value__c = category);
        csList.add(cs1);
        csList.add(cs2);
        csList.add(cs3);
        return csList;
    }
    
    //Create service feasibility response string
    public static String buildResponseString(String locId, String latitude, String longitude, List<String> addressList, Integer distance, String technology, String status, String rag, String fibreJointId, String fibreJointStatus, String fibreJointLatLong, String fibreJointTypeCode, String fibreJointTypeDescription, String locLatLong, String primaryTechnology, String derivedTechnology){
        String dtoResponse = '';
        
        DF_ServiceFeasibilityResponse sfResponse = new DF_ServiceFeasibilityResponse(locId, latitude, longitude, addressList, distance, technology, status, rag, fibreJointId, fibreJointStatus, fibreJointLatLong, fibreJointTypeCode, fibreJointTypeDescription, locLatLong);
        sfResponse.primaryTechnology = primaryTechnology;
        sfResponse.derivedTechnology = derivedTechnology;                                     
        dtoResponse = JSON.serialize(sfResponse);
        return dtoResponse;
    }
    
    //create custom settings for site survey charge
    public static List<DF_Custom_Options__c> createCustomSettingsforSiteSurveyCharge(String category, String offer, String def){
        List<DF_Custom_Options__c> csList = new List<DF_Custom_Options__c>();
        DF_Custom_Options__c cs1 = new DF_Custom_Options__c(name = 'SF_CATEGORY_ID', Value__c = category);
        DF_Custom_Options__c cs2 = new DF_Custom_Options__c(name = 'SF_SITE_SURVEY_CHARGE_OFFER_ID', Value__c = offer);
        DF_Custom_Options__c cs3 = new DF_Custom_Options__c(name = 'SF_SITE_SURVEY_CHARGE_DEFINITION_ID', Value__c = def);
        DF_Custom_Options__c cs4 = new DF_Custom_Options__c(name = 'SF_OPPORTUNITY_STAGE', Value__c = 'Closed Won');
        csList.add(cs1);
        csList.add(cs2);
        csList.add(cs3);
        csList.add(cs4);
        return csList;
    }
    
    //create custom settings for site survey charge
    public static Business_Platform_Events__c createCustomSettingsforBusinessPlatformEvents(){
        Business_Platform_Events__c settings = new Business_Platform_Events__c();
        settings.AppainToSFSource__c = 'Appian';
        settings.Batch_Size__c = 23;
        settings.DF_SFC_Desktop_Assessment__c = 'SF-DF-000001';
        settings.SFC_Desktop_Assessment_completed__c = 'AP-SF-01000';
        settings.SFToAppainSource__c = 'Salesforce';
        return settings;
    }
    
    //Create an attachment
    public static Attachment createAttachment(Id parentId){
        final String ENCODING_SCHEME = 'ISO-8859-1';
        String csvContent = 'SearchType,LocationId,Latitude,Longitude,UnitNumber,UnitType,StreetorLotNumber,StreetName,StreetType,SuburborLocality,State,Postcode';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'SearchByLocationID,LOC000002696724,,,,,,,,,,';
        String base64Data = 'TG9jIElkLEFkZHJlc3MsTGF0aXR1ZGUsTG9uZ2l0dWRlDQo';
        String contentType = 'application/vnd.ms-excel';
        Attachment a = new Attachment();
        a.parentId = parentId;
        Blob bodyBlob = Blob.valueOf(csvContent);
        a.body = bodyBlob;
        a.Name = 'Bulk Upload.csv';
        a.ContentType = 'application/vnd.ms-excel';
        return a;
    }
    
    // Create df community user and related records
    public static User createDFCommUser() {
        Profile commProfile = [SELECT Id 
                               FROM Profile 
                               WHERE Name = 'DF Partner User'];
                
        Account commAcct = new Account();
        commAcct.Name = 'RSPTest Acct';
        commAcct.Access_Seeker_ID__c = 'ASI500050005000';
        commAcct.Commercial_Deal__c = true;
        insert commAcct;
        
        Contact commContact = new Contact();
        commContact.LastName ='testContact';
        commContact.AccountId = commAcct.Id;
        insert commContact;  
        
        User commUser = new User();
        commUser.Alias = 'test123';
        commUser.Email = 'test123@noemail.com';
        commUser.Emailencodingkey = 'ISO-8859-1';
        commUser.Lastname = 'Testing';
        commUser.Languagelocalekey = 'en_US';
        commUser.Localesidkey = 'en_AU';
        commUser.Profileid = commProfile.Id;
        commUser.IsActive = true;
        commUser.ContactId = commContact.Id;
        commUser.Timezonesidkey = 'Australia/Sydney';
        commUser.Username = 'tester@noemail.com.test';

        insert commUser;

        return commUser;
    }


    // Create df community user and related records
    public static User createDFCommUserMyAccount() {
        Profile commProfile = [SELECT Id 
                               FROM Profile 
                               WHERE Name = 'DF Partner User'];

        Account commAcct = new Account();
        commAcct.Name = 'My Account';
        commAcct.Access_Seeker_ID__c = 'ASI500050005000';
        insert commAcct;

        Contact commContact = new Contact();
        commContact.LastName ='testContact';
        commContact.AccountId = commAcct.Id;
        insert commContact;

        User commUser = new User();
        commUser.Alias = 'test123';
        commUser.Email = 'test123@noemail.com';
        commUser.Emailencodingkey = 'ISO-8859-1';
        commUser.Lastname = 'Testing';
        commUser.Languagelocalekey = 'en_US';
        commUser.Localesidkey = 'en_AU';
        commUser.Profileid = commProfile.Id;
        commUser.IsActive = true;
        commUser.ContactId = commContact.Id;
        commUser.Timezonesidkey = 'Australia/Sydney';
        commUser.Username = 'tester@noemail.com.test';

        insert commUser;

        return commUser;
    }

    public static Account getAccount(User user) {

        if(user.Account != null){
            return user.Account;
        } else {
            String contactId = user.ContactId;
            if (contactId != null) {
                List<Contact> contacts = [SELECT Id, LastName, AccountId FROM Contact WHERE Id = :contactId];
                if (!contacts.isEmpty()) {
                    String accountId = contacts[0].AccountId;
                    if(accountId != null) {
                        List<Account> accounts = [SELECT Id, Name FROM Account WHERE Id = :accountId];
                        if (!accounts.isEmpty()) {
                            return accounts[0];
                        }
                    }
                }
            }
            return null;
        }
    }

    // Create AsyncRequest record
    public static Async_Request__c createAsyncRequest(String handlerName, String status, String params, String errorMsg, Decimal priority, Decimal noOfRetries, Decimal noOfResubmissions) {
        Async_Request__c asyncReq = new Async_Request__c();
        
        asyncReq.Handler__c = handlerName;
        asyncReq.Status__c = status;
        asyncReq.Params__c = params;
        asyncReq.Error_Message__c = errorMsg;
        asyncReq.Priority__c = priority;
        asyncReq.No_of_Retries__c = noOfRetries;
        asyncReq.No_of_Resubmissions__c = noOfResubmissions;
        
        return asyncReq;
    }
    
    //Create CS subscription record
    public static csord__Subscription__c createSubscription(String nameVal, String bpiID, String changeType, Id accId, csord__Order__c ord) {
        csord__Subscription__c sub = new csord__Subscription__c();
        sub.Name = nameVal;
        sub.BPI_Id__c = bpiID;
        sub.csordtelcoa__Change_Type__c = changeType;
        sub.csord__Account__c = accId;
        sub.csord__Order__c = ord.Id;
        sub.csord__Identification__c = 'Subscription Id';
        insert sub;
        return sub;
    }
    
    //Create CS Order record
    public static csord__Order__c createCSOrder(String nameVal, Id accId, Opportunity opp) {
        csord__Order__c ord = new csord__Order__c();
        ord.Name = nameVal;
        ord.csord__Account__c = accID;
        ord.csordtelcoa__Opportunity__c = opp.Id;
        ord.csord__Identification__c = 'Order ID';
        insert ord;
        return ord;
    }
    
    //create custom settings for CS change type
    public static csordtelcoa__Change_Types__c createCustomSettingsforCSChangeType(String changeType){
        csordtelcoa__Change_Types__c changeTypeVal = new csordtelcoa__Change_Types__c();
        changeTypeVal.Name = changeType;
        changeTypeVal.csordtelcoa__Add_To_Existing_Subscription__c = true;
        insert changeTypeVal;
        return changeTypeVal;
    }
    
}