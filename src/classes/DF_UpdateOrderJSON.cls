public with sharing class DF_UpdateOrderJSON {
    public static String updateOrderJSONForModifyOrder(String  dfOrderId, String modifyType, String location) {
        try{
            DF_Order__c dfOrder = [SELECT Order_JSON__c, Order_Notifier_Response_JSON__c,OVC_NonBillable__c, Modify_Order_JSON__c FROM DF_Order__c WHERE Id =:dfOrderId LIMIT 1];
            DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot dfOrdJSON = (DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot)JSON.deserialize(dfOrder.Order_JSON__c, DF_OrderDataJSONWrapper.DF_OrderDataJSONRoot.class);
            String ovcNBModOrd = '{"OVC":' + dfOrder.OVC_NonBillable__c + '}';
            DF_OVCWrapper ovcNBList = DF_OVCWrapper.parse(ovcNBModOrd);
            System.debug(JSON.serialize(dfOrdJSON.ProductOrder.ProductOrderComprisedOf.referencesProductOrderItem[0]));
            System.debug('OVC NonBill: '+ ovcNBModOrd);
        //    Integer modOVCLen = dfModOrdJSON.ProductOrder.ProductOrderComprisedOf.referencesProductOrderItem.size(); 
        //    Integer ordOVCLen = dfOrdJSON.ProductOrder.ProductOrderComprisedOf.referencesProductOrderItem.size();
            DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 referencesProductOrderItemL1btd = new DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1();
            referencesProductOrderItemL1btd = dfOrdJSON.ProductOrder.ProductOrderComprisedOf.referencesProductOrderItem[0];
            
            List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1> referencesProductOrderItemListL1 = new List<DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1>();
            referencesProductOrderItemListL1.Add(referencesProductOrderItemL1btd);
            
            List<DF_OrderSubmittedSummaryController.OVCData> oDataList = new List<DF_OrderSubmittedSummaryController.OVCData>();
            Map<String, DF_OrderSubmittedSummaryController.OVCData> ovcNFDataMap = new Map<String, DF_OrderSubmittedSummaryController.OVCData>();
            oDataList = DF_OrderSubmittedSummaryHelper.getOVCDetailsWithId(oDataList, dfOrder.Order_Notifier_Response_JSON__c, dfOrder);
            system.debug('@!#oDataList: ' + oDataList);
            for(DF_OrderSubmittedSummaryController.OVCData oData : oDataList) {
                if(oData.ovcId != null)
                ovcNFDataMap.put(oData.ovcId, oData);
            }

            for (DF_OVCWrapper.OVC ovcNBData : ovcNBList.OVC){
                DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1 referencesProductOrderItemL1ovc = new DF_OrderDataJSONWrapper.ReferencesProductOrderItemL1();
                DF_OrderDataJSONWrapper.ItemInvolvesProductL2 prodL2 = new DF_OrderDataJSONWrapper.ItemInvolvesProductL2();//OVCs

                referencesProductOrderItemL1ovc.action = 'ADD';
                prodL2.type = 'OVC' ;
                // OVC fields
                prodL2.id = (String)ovcNBData.ovcSystemId;
                prodL2.poi = (String)ovcNBData.POI == null ? null:(String)ovcNBData.POI;
                prodL2.nni = (String)ovcNBData.NNIGroupId == null ? null:(String)ovcNBData.NNIGroupId;    
                prodL2.routeType  = (String)ovcNBData.routeType == null ? null:(String)ovcNBData.routeType;   
                prodL2.sVLanId  = (String)ovcNBData.sTag == null ? null:Integer.valueOf((String)ovcNBData.sTag);
                if(!ovcNFDataMap.isEmpty()) {
                    if(ovcNFDataMap.containsKey(ovcNBData.ovcSystemId) && ovcNFDataMap.get(ovcNBData.ovcSystemId).sTag!=null) {
                        prodL2.sVLanId  = Integer.valueOf(ovcNFDataMap.get(ovcNBData.ovcSystemId).sTag);
                    }
                }
                prodL2.maximumFrameSize  = (String)ovcNBData.ovcMaxFrameSize;
                prodL2.cosHighBandwidth  = (String)ovcNBData.coSHighBandwidth == '0' ? '':(String)ovcNBData.coSHighBandwidth +'Mbps';
                prodL2.cosMappingMode = (String)ovcNBData.mappingMode == null ? null:(String)ovcNBData.mappingMode;   
                prodL2.cosMediumBandwidth = (String)ovcNBData.coSMediumBandwidth == '0' ? '':(String)ovcNBData.coSMediumBandwidth +'Mbps';
                prodL2.cosLowBandwidth = (String)ovcNBData.coSLowBandwidth == '0' ? '':(String)ovcNBData.coSLowBandwidth +'Mbps';
                prodL2.uniVLanId = (String)ovcNBData.ceVlanId == null ? null:(String)ovcNBData.ceVlanId;
                prodL2.connectedTo    = '1';// (String)ovc.get('NNI');  
                prodL2.ovcTransientId = (String)ovcNBData.OVCName;
                referencesProductOrderItemL1ovc.itemInvolvesProduct = prodL2;     
                referencesProductOrderItemListL1.Add(referencesProductOrderItemL1ovc);
                }
            dfOrdJSON.ProductOrder.ProductOrderComprisedOf.referencesProductOrderItem = referencesProductOrderItemListL1;
            if(modifyType=='Change Service Restoration SLA'){
                Map<String, Object> orderJSON = (Map<String, Object>) JSON.deserializeUntyped(location);
                List<Object> uniList = (List<Object>)orderJSON.get('UNI');
                Map<String, Object> uni = (Map<String, Object>)uniList[0]; //only one UNI
                dfOrdJSON.ProductOrder.ProductOrderComprisedOf.itemInvolvesProduct.serviceRestorationSLA=(String)uni.get('SLA');
            }
        //    System.debug('Prod L2: ' + JSON.serialize(prodL2));
            System.debug('Order JSON: ' + JSON.serialize(dfOrdJSON));
            String updOrderJSON = JSON.serialize(dfOrdJSON);
            return updOrderJSON;
        }
        catch(Exception e)
        {
            throw e;
        }
    }
}