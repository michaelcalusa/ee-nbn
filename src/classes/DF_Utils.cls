public class DF_Utils {

	public static final String ERR_COMM_USR_ACCT_ID_NOT_FOUND = 'Community User AccountId was not found.';
	public static final String ERR_COMM_USR_ID_NOT_FOUND = 'Community UserId was not found.';
	public static final String ERR_COMM_USR_NOT_FOUND = 'Community User was not found.';

    public static Id getCommUserAccountId() {		
		Id commUserAccountId;
		Id commUserId;
		User commUser;

        try {        	        	
        	// Get Community user AccountId
			commUserId = UserInfo.getUserId();

			if (commUserId != null) {				
				commUser = [SELECT AccountId
							FROM   User 
							WHERE  Id = :commUserId];
			} else {
				throw new CustomException(DF_Utils.ERR_COMM_USR_ID_NOT_FOUND); 
			}

			if (commUser != null) {				
				if (commUser.AccountId != null) {					
					commUserAccountId = commUser.AccountId;
				} else {
					throw new CustomException(DF_Utils.ERR_COMM_USR_ACCT_ID_NOT_FOUND);    
				}
			} else {
				throw new CustomException(DF_Utils.ERR_COMM_USR_NOT_FOUND);
			}
        } catch(Exception e) {
			throw new CustomException(e.getMessage());
        }     
        
        return commUserAccountId; 
    } 
        
}