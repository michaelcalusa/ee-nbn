/***************************************************************************************************
    Class Name          : DeSerialiseContactUpsertRequest
    Version             : 1.0
    Created Date        : 03-Jul-2018
    Author              : Arpit Narain
    Description         :  Class to parse contact update or insert request body to Apex class
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/

public class DeSerialiseContactUpsertRequest {

    public String firstName { get {return String.isNotEmpty(firstName) ? firstName.trim() : firstName;} set; }
    public String lastName { get {return String.isNotEmpty(lastName) ? lastName.trim() : lastName;} set; }
    public String email { get {return String.isNotEmpty(email) ? email.trim().toLowerCase() : email;} set; }
    public String phoneNo { get {return String.isNotEmpty(phoneNo) ? phoneNo.trim() : phoneNo;} set; }
    public String alternatePhoneNo { get {return String.isNotEmpty(alternatePhoneNo) ? alternatePhoneNo.trim() : alternatePhoneNo;} set; }
    public String index { get {return String.isNotEmpty(index) ? index.trim() : index;} set; }
    public Boolean updateContact { get {return updateContact == null ? false:updateContact;}set;}


    public static List<DeSerialiseContactUpsertRequest> parse(String json) {

        return (List<DeSerialiseContactUpsertRequest>) System.JSON.deserialize(json, List<DeSerialiseContactUpsertRequest>.class);
    }
}