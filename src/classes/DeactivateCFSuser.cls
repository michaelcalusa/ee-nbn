////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*  Name    : DeactivateCFSuser
 *  Purpose : This is a class to deactivate user for deactivation user after 30 days and not having profile ICT Customer Community Plus User
 *  Author  : Nancy Gupta
 *  Date    : 2017-11-08
 *  Version : 1.0
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public class DeactivateCFSuser{
public static void RunLoop( )
{

        List<lmscons__Training_Path_Assignment_Progress__c> listftrnngpth =[Select Id, Name, lmscons__StatusPicklist__c from lmscons__Training_Path_Assignment_Progress__c where lmscons__StatusPicklist__c = 'Completed' LIMIT 1000];              
        List<User> listUser =[Select Id, CFS_Activation_Date__c from user WHERE lmscons__CFS_Status__c = 'Active' AND Profile.Name = 'ICT Customer Community Plus User'];
        Integer deactivateIn = 30;
        for (User unot: listUser) {          
                  Date date1 = date.Today()-deactivateIn ;
            if (unot.CFS_Activation_Date__c < date1 || listftrnngpth !=null ) {
               unot.lmscons__CFS_Status__c = 'Deactivated';
            }
            //update unot;
        }
       
}
}