/*
Class Description
Creator: v-dileepathley
Purpose: This class will be used to transform and transfer Development Contact records from Junction_Object__c table.
Modifications:
*/
public class DevContactTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
    public String dcQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {
        
    }
    
    public DevContactTransformation(String ejId)
    {
    	 exJobId = ejId;
    	 Batch_Job__c bj = new Batch_Job__c();
    	 bj.Name = 'DevContactTransform'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
    	dcQuery = 'Select Child_Id__c,Child_Name__c,Event_Name__c,Object_Id__c,Object_Name__c from Junction_Object_Int__c where Transformation_Status__c = \'Exported from CRM\' and Object_Name__c = \'CustomObject2\' order by createddate asc';
    	return Database.getQueryLocator(dcQuery);   
    }
    
    public void execute(Database.BatchableContext BC,List<Junction_Object_Int__c> junList)
    {
        try
        {
        	list<Development_Contact__c> dcAddList = new list<Development_Contact__c>();
        	map<String, Development_Contact__c> dcAddMap = new map<String, Development_Contact__c>();
        	list<Development_Contact__c> dcDelList = new list<Development_Contact__c>();
        	
        	Database.DMLOptions dml = new Database.DMLOptions();
			dml.allowFieldTruncation = true;
				
			list <Junction_Object_Int__c> aDev = new list<Junction_Object_Int__c>();
			list <Junction_Object_Int__c> dDev = new list<Junction_Object_Int__c>();
			
			String devConUnique;
			list<string> devConUniqueId =  new list<string>();
			List<String> listOfDevIds = new List<String>();
			List<String> listOfConIds = new List<String>(); 
			Map<String, Id> mapListOfCon = new Map<String, Id>();
			Map<String, Id> mapListOfDev = new Map<String, Id>();   
			
			for(Junction_Object_Int__c devi : junList)
            {                                
                listOfConIds.add(devi.Child_Id__c);     
                listOfDevIds.add(devi.Object_Id__c);
            }
            
            for(Contact con : [select On_Demand_Id_P2P__c, Id from Contact where On_Demand_Id_P2P__c in :listOfConIds FOR UPDATE])
            {
                mapListOfCon.put(con.On_Demand_Id_P2P__c, con.id);            
            }  
                 
            for(Development__c dev : [select Development_Id__c, Id from Development__c where Development_Id__c in :listOfDevIds FOR UPDATE])
            {
                mapListOfDev.put(dev.Development_Id__c, dev.id);            
            }
            
			for(Junction_Object_Int__c jun: junList)
			{
				Development_Contact__c devCon = new Development_Contact__c();
				devCon.setOptions(dml);
				
				if(jun.Event_Name__c == 'Associate')
				{
					//Contact con = new Contact(On_Demand_Id_P2P__c = jun.Child_Id__c);
					//devCon.Contact__r = con;
					//Development__c dev = new Development__c(Development_ID__c = jun.Object_Id__c);
					//devCon.Development__r = dev;
					devCon.Contact__c = mapListOfCon.get(jun.Child_Id__c);
					devCon.Development__c = mapListOfDev.get(jun.Object_Id__c);
					devCon.Dev_Contact_Unique__c = mapListOfDev.get(jun.Object_Id__c)+' '+mapListOfCon.get(jun.Child_Id__c);
					aDev.add(jun);
					//dcAddList.add(devCon);
					dcAddMap.put(mapListOfDev.get(jun.Object_Id__c)+' '+mapListOfCon.get(jun.Child_Id__c),devCon);
				}
				
				if(jun.Event_Name__c == 'Dissociate')
				{
					devConUnique = mapListOfDev.get(jun.Object_Id__c)+' '+mapListOfCon.get(jun.Child_Id__c);
					dDev.add(jun);
					devConUniqueId.add(devConUnique);
				}
			}
			if(!devConUniqueId.isEmpty())
			{
				dcDelList = [select Id from Development_Contact__c where Dev_Contact_Unique__c in: devConUniqueId];
			}
			dcAddList = dcAddMap.values();
			Schema.SObjectField dcUnique = Development_Contact__c.Fields.Dev_Contact_Unique__c;
			List<Database.upsertResult> uResults = Database.upsert(dcAddList,dcUnique,false);
			List<Database.deleteResult> dResults = Database.delete(dcDelList,false);
			list<Database.Error> err;
			String msg, fAffected;
			String[]fAff;
			StatusCode sCode;
			list<Error_Logging__c> errList = new List<Error_Logging__c>();
			for(Integer idx = 0; idx < uResults.size(); idx++)
			{	
				if(!uResults[idx].isSuccess())
				{
					err=uResults[idx].getErrors();
					for (Database.Error er: err)
					{
						sCode = er.getStatusCode();
						msg=er.getMessage();
	            		fAff = er.getFields();
					}
					fAffected = string.join(fAff,',');
					errList.add(new Error_Logging__c(Name='DevelopmentContact_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),
					isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=aDev[idx].Id, Schedule_Job__c=bjId));
					aDev[idx].Transformation_Status__c = 'Error';
					aDev[idx].Schedule_Job_Transformation__c = bjId;
				}
				else
				{
						aDev[idx].Transformation_Status__c = 'Copied to Base Table';
						aDev[idx].Schedule_Job_Transformation__c = bjId;
				}
			}
			
			for(Integer idx = 0; idx < dResults.size(); idx++)
			{	
				if(!dResults[idx].isSuccess())
				{
					err=dResults[idx].getErrors();
					for (Database.Error er: err)
					{
						sCode = er.getStatusCode();
						msg=er.getMessage();
	            		fAff = er.getFields();
					}
					fAffected = string.join(fAff,',');
					errList.add(new Error_Logging__c(Name='DevelopmentContact_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,
					isSuccess__c=dResults[idx].isSuccess(),Record_Row_Id__c=dResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=dDev[idx].Id,Schedule_Job__c=bjId));
					dDev[idx].Transformation_Status__c = 'Error';
					dDev[idx].Schedule_Job_Transformation__c = bjId;
				}
				else
				{
						dDev[idx].Transformation_Status__c = 'Copied to Base Table';
						dDev[idx].Schedule_Job_Transformation__c = bjId;
				}
			}
			aDev.addAll(dDev);
            system.debug('errList==>'+errList);
			Update aDev;
			Insert errList;
			if(errList.isEmpty())      
            {
            	status = 'Completed';
            } 
            else
            {
            	status = 'Error';
            }  
             recordCount = recordCount+ junList.size();  
        }
        catch (Exception e)
        {
        	status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
			errList.add(new Error_Logging__c(Name='DevelopmentContact_Tranformation',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),Schedule_Job__c=bjId));
			Insert errList;
        }
        
    }
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob devConJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
       	{
       		bj.Status__c= devConJob.Status;
       	}
       	else
       	{
       		 bj.Status__c=status;
       	}
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = devConJob.id;
        bj.End_Time__c  = devConJob.CompletedDate;
        upsert bj;
    } 
}