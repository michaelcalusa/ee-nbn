/***************************************************************************************************
Class Name:  DevContactTransformation_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 31-10-2016
Function    : This class contains unit test scenarios for DevContactTransformation apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       31-10-2016                Created
****************************************************************************************************/
@isTest
private class DevContactTransformation_Test {
    static date CRM_Modified_Date = date.newinstance(1963, 01,24);
    static List<Account> listOfAccounts = new List<Account> (); 
    static void getRecords (){
        // Custom Setting record creation
        TestDataUtility.getListOfCRMTransformationJobRecords(true);
        // Create Account record
        Account accRec = new Account();
        accRec.name = 'Test Account';
        accRec.on_demand_id__c = 'Account6';
        listOfAccounts.add(accRec);
        Account accRec2 = new Account();
        accRec2.name = 'Test Account';
        accRec2.on_demand_id__c = 'Account4';
        listOfAccounts.add(accRec2);
        insert listOfAccounts;
        // Create Contact record
        List<Contact> ListOfContacts = new List<Contact> ();
        Contact contactRec = new Contact ();
        contactRec.on_demand_id_P2P__c = 'Contact123';
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        ListOfContacts.add(contactRec);
        Contact contactRec2 = new Contact ();
        contactRec2.on_demand_id_P2P__c = 'Contact12345';
        contactRec2.LastName = 'Test Contact';
        contactRec2.Email = 'TestEmail@nbnco.com.au';
        contactRec2.On_Demand_Id__c  = 'Contact12345';
        ListOfContacts.add(contactRec2);
        insert ListOfContacts;
        // Create Development record
        Development__c Development = new Development__c ();
        Development.Name = 'Test';
        Development.Development_ID__c = '123456789';
        Development.Account__c = listOfAccounts.get(0).Id;
        Development.Primary_Contact__c = ListOfContacts.get(0).Id;
        Development.Suburb__c = 'North Sydney';
        insert Development;
        Development__c Development2 = new Development__c ();
        Development2.Name = 'Test';
        Development2.Development_ID__c = '0123456789';
        Development2.Account__c = listOfAccounts.get(0).Id;
        Development2.Primary_Contact__c = ListOfContacts.get(0).Id;
        Development2.Suburb__c = 'North Sydney';
        insert Development2;
        // Create Junction_Object_Int__c record
        List<Junction_Object_Int__c> actintList = new List<Junction_Object_Int__c>();
        Junction_Object_Int__c actint1 = new Junction_Object_Int__c();
        actint1.Name='TestName';
        actint1.CRM_Modified_Date__c=CRM_Modified_Date;
        actint1.Transformation_Status__c = 'Exported from CRM';
        actint1.Object_Name__c = 'Account';
        actint1.Child_Name__c = 'Contact';
        actint1.Event_Name__c = 'Associate';
        actint1.Object_Id__c = 'Account123';
        actint1.Child_Id__c = 'Contact123';
        actintList.add(actint1);
        Junction_Object_Int__c actint2 = new Junction_Object_Int__c();
        actint2.Name='TestName';
        actint2.CRM_Modified_Date__c=CRM_Modified_Date;
        actint2.Transformation_Status__c = 'Exported from CRM';
        actint2.Object_Name__c = 'CustomObject2';
        actint2.Child_Id__c = 'Contact123';
        actint2.Object_Id__c = '123456789';
        actint2.Event_Name__c = 'Associate';
        actintList.add(actint2);
        Junction_Object_Int__c actint3 = new Junction_Object_Int__c();
        actint3.Name='TestName';
        actint3.CRM_Modified_Date__c=CRM_Modified_Date;
        actint3.Transformation_Status__c = 'Exported from CRM';
        actint3.Object_Name__c = 'CustomObject2';
        actint3.Child_Id__c = '11223344';
        actint3.Object_Id__c = 'aaassss';
        actint3.Event_Name__c = 'Associate';
        actintList.add(actint3);
        Junction_Object_Int__c actint4 = new Junction_Object_Int__c();
        actint4.Name='TestName';
        actint4.CRM_Modified_Date__c=CRM_Modified_Date;
        actint4.Transformation_Status__c = 'Exported from CRM';
        actint4.Object_Name__c = 'CustomObject2';
        actint4.Child_Id__c = 'Contact123';
        actint4.Object_Id__c = '0123456789';
        actint4.Event_Name__c = 'Dissociate';
        actintList.add(actint4);
        Junction_Object_Int__c actint5 = new Junction_Object_Int__c();
        actint5.Name='TestName';
        actint5.CRM_Modified_Date__c=CRM_Modified_Date;
        actint5.Transformation_Status__c = 'Exported from CRM';
        actint5.Object_Name__c = 'CustomObject2';
        actint5.Child_Id__c = 'Contact12345';
        actint5.Object_Id__c = '0123456789';
        actint5.Event_Name__c = 'Dissociate';
        actintList.add(actint5);
        insert actintList;
        // insert Development Contact Records
        Development_Contact__c DevContact = new Development_Contact__c ();
        DevContact.Contact__c = ListOfContacts.get(1).Id;
        DevContact.Development__c = Development2.Id;
        DevContact.Dev_Contact_Unique__c = Development2.Id +' '+ListOfContacts.get(1).Id;
        insert DevContact;
    }
    static testMethod void DevContactTransformation_Test1(){
        getRecords();
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());   
        Test.stopTest();
    }
}