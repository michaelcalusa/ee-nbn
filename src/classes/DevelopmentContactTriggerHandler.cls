/***************************************************************************************************
Version     : 1.0 
Created Date: 19-09-2016 
Description/Function    : 
Used in     : 
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN        19-09-2016           Created the Class
****************************************************************************************************/
public class DevelopmentContactTriggerHandler{
    //Class level variables that are commonly used
    private boolean isExecuting = false;
    private integer batchSize;
    private List<Development_Contact__c> trgOldList = new List<Development_Contact__c> ();
    private List<Development_Contact__c> trgNewList = new List<Development_Contact__c> ();
    private Map<id,Development_Contact__c> trgOldMap = new Map<id,Development_Contact__c> ();
    private Map<id,Development_Contact__c> trgNewMap = new Map<id,Development_Contact__c> ();
    private static boolean isAsync;
    // Below 7 boolean variables are used to Prevent recursion
    public static boolean isBeforeInsertFirstRun = true;
    public static boolean isBeforeUpdateFirstRun = true;
    public static boolean isBeforeDeleteFirstRun = true;
    public static boolean isAfterInsertFirstRun = true;
    public static boolean isAfterUpdateFirstRun = true;
    public static boolean isAfterDeleteFirstRun = true;
    public static boolean isAfterUndeleteFirstRun = true;
    /***************************************************************************************************
    Method Name:  DevelopmentContactTriggerHandler
    Method Type: Constructor
    Version     : 1.0 
    Created Date: 19-09-2016  
    Function    : used to assign the Trigger values to the Class variables
    Input Parameters: Trigger Context Variables (6 parameters - 1 boolean, 1 integer, 2 List and 2 Map) 
    Output Parameters: None
    Description:   
    Used in     : "DevelopmentContactTrigger" Trigger
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       19-09-2016              Created
    ****************************************************************************************************/  
    public DevelopmentContactTriggerHandler(boolean isExecuting, integer batchSize, List<Development_Contact__c> trgOldList, List<Development_Contact__c> trgNewList, Map<id,Development_Contact__c> trgOldMap, Map<id,Development_Contact__c> trgNewMap){
        this.isExecuting = isExecuting;
        this.BatchSize = batchSize;
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
        DevelopmentContactTriggerHandler.isAsync = System.isBatch() || System.isFuture();
    }
    public void OnBeforeInsert(){}
    public void OnBeforeUpdate(){}
    public void OnBeforeDelete(){
        if(!HelperUtility.isTriggerMethodExecutionDisabled('validateDevContsDelete')){
            if(isBeforeDeleteFirstRun){
                validateDevContsDelete(trgOldList, trgOldMap);
                isBeforeDeleteFirstRun = false;
            }
        }
    }
    public void OnAfterInsert(){}
    public void OnAfterUpdate(){}
    public void OnAfterDelete(){}
    public void OnUndelete(){}
    /*---------------------------------------------------------------------------------------------------------------------------------------------------*/
    // Business Logic Method
    public void validateDevContsDelete(List<Development_Contact__c> trgOldList, map<ID, Development_Contact__c > trgOldMap){
        set<id> contactIdSet = new set<id> ();
        for(Development_Contact__c devContact : trgOldMap.values()){
            if(devContact.Contact__c <> null)
                contactIdSet.add(devContact.Contact__c);
        }
        Map<Id,contact> mapOfContacts = new Map<Id,contact> ([SELECT On_Demand_Id__c FROM Contact WHERE ID IN: contactIdSet AND On_Demand_Id__c <> null]);
        for(Development_Contact__c devContact : trgOldMap.values()){
            if(devContact.Contact__c <> null && mapOfContacts.get(devContact.Contact__c) <> null)
                devContact.addError(HelperUtility.getErrorMessage('006'));
        }
    }
}