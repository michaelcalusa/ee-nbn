/***************************************************************************************************
Class Name:  DevelopmentContactTriggerHandler_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 03-11-2016
Function    : This class contains unit test scenarios for DevelopmentContactTriggerHandler apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN         03-11-2016                Created
****************************************************************************************************/
@isTest
private class DevelopmentContactTriggerHandler_Test{
        static User testRunningUser = new User ();
    static Contact conObj = new Contact();
    static Development__c devObj = new Development__c();
    /***************************************************************************************************
    Method Name:  getRecords
    Method Type: Constructor
    Version     : 1.0 
    Created Date: 13 July 2016
    Description:  Common Test data used for all test methods
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       13/07/2015                Created
    ****************************************************************************************************/
    static void getRecords (){
        // Custom Setting - "Error Message" Records
        List<Error_Message__c> ListofErrorMessage = TestDataUtility.getListOfErrorMessage(true);
        // Add all the Profile names which are required for testing.
        List<string> listOfProfileName = new List<string> {'System Administrator'};
        // Query Profiles
        list<Profile> listOfProfiles = TestDataUtility.getListOfProfiles(listOfProfileName);
        // Instantiate User and assign values. Not creating User
        testRunningUser = TestDataUtility.createTestUser(true,listOfProfiles.get(0).id);
        // Create Account
        Account actObj = new Account();
        actObj.name = 'Test Account';
        insert actObj;
        // Create Contact
        conObj.LastName = 'testLastName';
        insert conObj;
        // Create Development
        devObj.Name='testName';
        devObj.Development_ID__c='AKHS833';
        devObj.Account__c=actObj.Id;
        devObj.Primary_Contact__c=conObj.Id;
        devObj.Suburb__c='testsuburd';
        insert devObj;
    }
    
    static testMethod void DevelopmentContactTrigHandlr_Test1(){
        getRecords();
        // Test Trigger Functionality
        Test.startTest();
        System.runAs(testRunningUser){
            // Create Development Contact
            Development_Contact__c devConObj = new Development_Contact__c();
            devConObj.Contact__c=conObj.Id;
            devConObj.Development__c=devObj.Id;
            // DML Operations
            insert devConObj;
            update devConObj;
            delete devConObj;
            undelete devConObj;
        }
        Test.stopTest();
    }
    static testMethod void DevelopmentContactTrigHandlr_Test2(){
        getRecords();
        conObj.On_Demand_Id__c = 'test123';
        update conObj;
        // Test Trigger Functionality
        Test.startTest();
        System.runAs(testRunningUser){
            /// Create Development Contact
            Development_Contact__c devConObj = new Development_Contact__c();
            devConObj.Contact__c=conObj.Id;
            devConObj.Development__c=devObj.Id;
            // DML Operations
            insert devConObj;
            try{
                delete devConObj;
            }
            catch(Exception Ex){
                boolean expectedExceptionThrown =  Ex.getMessage().contains('ErrorMessage5') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
        }
        Test.stopTest();
    }
}