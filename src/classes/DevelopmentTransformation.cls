/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class will be used to transform and transfer Development records from Development_Int to Development
Modifications:
*/
public class DevelopmentTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
    public String deveIntQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {            
    }
    
     public DevelopmentTransformation(String ejId)
    {
         exJobId = ejId;
         Batch_Job__c bj = new Batch_Job__c();
         bj.Name = 'DevelopmentTransform'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        deveIntQuery = 'select Id, Name,FSA__c,CRM_Development_Id__c, Account__c, Account_Id__c, Primary_Contact__c, Primary_Contact_Id__c, Active_Status__c, Address__c, Suburb__c, State__c, Post_Code__c, SAM__c, Local_Technology__c, Latitude__c, Longitude__c, CRMOD_Owner__c, Estimated_First_Service_Connection_Date__c, Development_Type__c, Dwelling_Type__c,Small_Development_Status__c,Developer_Contract_Status__c from Development_Int__c where Transformation_Status__c = \'Extracted from CRM\' order by CreatedDate'; 
        return Database.getQueryLocator(deveIntQuery);    
        
    }
    public void execute(Database.BatchableContext BC,List<Development_Int__C> listOfDevInt)
    {
      try
        {            

            Boolean assError = false;
            List<String> listOfConIds = new List<String>();  
            List<String> listOfAccIds = new List<String>(); 
            
            Map<String, Id> mapListOfCon = new Map<String, Id>();
            for(Development_Int__C devi : listOfDevInt)
            { 
                if(!String.isBlank(devi.Primary_Contact_Id__c) && devi.Primary_Contact_Id__c != 'No Match Row Id')
                {                	 
                    listOfConIds.add(devi.Primary_Contact_Id__c);
                }
                if(!String.isBlank(devi.Account_Id__c) && devi.Account_Id__c != 'No Match Row Id')
                {                	
                    listOfAccIds.add(devi.Account_Id__c);
                }                                                     
            } 
            for(Contact con : [select On_Demand_Id_P2P__c, Id from Contact where On_Demand_Id_P2P__c in :listOfConIds FOR UPDATE])
            {
                mapListOfCon.put(con.On_Demand_Id_P2P__c, con.id);            
            }       
            
            Map<String, Id> mapListOfAcc = new Map<String, Id>();
            for(Account acc : [select on_demand_id__c, Id from Account where on_demand_Id__c in :listOfAccIds FOR UPDATE])
            {
                mapListOfAcc.put(acc.on_demand_id__c, acc.id);            
            }                        
            
            List<Development__c> listOfDev = new List<Development__c>();
            
            String rTypeIdMediumLarge = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Development__c'  and name = 'Medium/Large'].Id; 
            String rTypeIdSmall = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Development__c'  and name = 'Small'].Id;                                
            map <String, Development__c> devMap = new map<String, Development__c>();
            for(Development_Int__C di : listOfDevInt)
            {
                
                Development__c dev = new Development__c();
                
                dev.Development_ID__c = di.CRM_Development_Id__c;
                dev.Name = di.Name;
                if(!String.isBlank(di.Account_Id__c))
                {                        
                    //Account a = new Account(On_Demand_Id__c = di.Account_Id__c);                        
                    //dev.Account__r =a;
                    dev.Account__c = mapListOfAcc.get(di.Account_Id__c);
                }
                
                if(!String.isBlank(di.Primary_Contact_Id__c))
                {
                    //Contact c = new Contact(On_Demand_Id_P2P__c = di.Primary_Contact_Id__c);                        
                    //dev.Primary_Contact__r =c;
                    dev.Primary_Contact__c = mapListOfCon.get(di.Primary_Contact_Id__c);                    
                }     
               
                dev.Active_Status__c = di.Active_Status__c;
                dev.Address__c = di.Address__c;
                dev.Suburb__c = di.Suburb__c;
                dev.State__c = di.State__c;
                dev.Post_Code__c = di.Post_Code__c;
                dev.FSA__c = di.FSA__c;
                dev.SAM__c = di.SAM__c;
                dev.Local_Technology__c = di.Local_Technology__c;
                String strLatitude = di.Latitude__c;
                if(!String.isBlank(strLatitude))
                {
                  dev.Location__Latitude__s = Decimal.valueOf(strLatitude);    
                }                
                String strLongitude = di.Longitude__c;
                if(!String.isBlank(strLongitude))
                {
                  dev.Location__Longitude__s = Decimal.valueOf(strLongitude);    
                }                
                dev.CRMOD_Owner__c = di.CRMOD_Owner__c;
                String EFSCD = di.Estimated_First_Service_Connection_Date__c;                
                if(!String.isBlank(EFSCD))
                {
                    //String EFSCD2 = EFSCD.substringBefore(' ');
                  	//dev.Estimated_First_Service_Connection_Date__c = date.parse(EFSCD2.replace('-','/'));     
                  	dev.Estimated_First_Service_Connection_Date__c = StringToDate.strDate(EFSCD);
                }
                
                dev.Development_Type__c = di.Development_Type__c;
                if(di.Development_Type__c == 'Medium/Large')
                {
                    dev.RecordTypeId = rTypeIdMediumLarge;
                }
                else
                {
                    dev.RecordTypeId = rTypeIdSmall;
                }
                //dev.Dwelling_Type__c = di.Dwelling_Type__c;
               	dev.Small_Development_Status__c = di.Small_Development_Status__c;
                dev.Developer_Contract_Status__c = di.Developer_Contract_Status__c;
                dev.Ownership__c = 'CRMOD';
                
                //listOfDev.add(dev);
                devMap.put(di.CRM_Development_Id__c, dev);
            }
            listOfDev = devMap.values();
            List<Database.UpsertResult> upsertDevelopmentResults = Database.Upsert(listOfDev,Development__c.Fields.Development_ID__c,false);
            List<Database.Error> upsertDevelopmentError;
            String errorMessage, fieldsAffected;
            String[] listOfAffectedFields;
            StatusCode sCode;
            List<Error_Logging__c> errDevList = new List<Error_Logging__c>();
            List<Development_Int__c> updDevIntRes = new List<Development_Int__c>();
            for(Integer j=0; j<upsertDevelopmentResults.size();j++)
            {
                //Development_Int__c updDevInt = new Development_Int__C();
                if(!upsertDevelopmentResults[j].isSuccess())
                {
                    upsertDevelopmentError = upsertDevelopmentResults[j].getErrors();
                    for(Database.Error er: upsertDevelopmentError)
                    {
                        sCode = er.getStatusCode();
                        errorMessage = er.getMessage();
                        listOfAffectedFields = er.getFields();
                    }
                    fieldsAffected = String.join(listOfAffectedFields,',');
                    errDevList.add(new Error_Logging__c(Name='Development Transformation',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isCreated__c=upsertDevelopmentResults[j].isCreated(),isSuccess__c=upsertDevelopmentResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfDev[j].Development_ID__c,Schedule_Job__c=bjId));
                    //updDevInt.Transformation_Status__c = 'Error';
                    //updDevInt.CRM_Development_Id__c = listOfDev[j].Development_ID__c;
                    //updDevInt.Schedule_Job_Transformation__c = bjId;
                    //updDevIntRes.add(updDevInt);
                    listOfDevInt[j].Transformation_Status__c = 'Error';
                    listOfDevInt[j].Schedule_Job_Transformation__c = bjId;
                }
                else
                {
                    if(String.isBlank(listOfDevInt[j].Account_Id__c) || listOfDevInt[j].Account_Id__c == 'No Match Row Id' || String.isNotBlank(mapListOfAcc.get(listOfDevInt[j].Account_Id__c)))
					{
                        //updDevInt.Transformation_Status__c = 'Copied to Base Table';
                        listOfDevInt[j].Transformation_Status__c = 'Copied to Base Table';                        
					}
					else
					{
						//updDevInt.Transformation_Status__c = 'Association Error';
                        //listOfDevInt[j].Transformation_Status__c = 'Association Error';
                        assError = true;
						errDevList.add(new Error_Logging__c(Name='Development Tranformation',Error_Message__c='Account Record is not associated',Fields_Afftected__c=fieldsAffected,isCreated__c=upsertDevelopmentResults[j].isCreated(),isSuccess__c=upsertDevelopmentResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfDev[j].Development_Id__c,Schedule_Job__c=bjId));
					}
					
					if(String.isBlank(listOfDevInt[j].Primary_Contact_Id__c) || listOfDevInt[j].Primary_Contact_Id__c == 'No Match Row Id' || String.isNotBlank(mapListOfCon.get(listOfDevInt[j].Primary_Contact_Id__c)))
					{
                        //updDevInt.Transformation_Status__c = 'Copied to Base Table';
                        listOfDevInt[j].Transformation_Status__c = 'Copied to Base Table';                            	
                        
                        
					}
					else
					{
						//updDevInt.Transformation_Status__c = 'Association Error';
                        //listOfDevInt[j].Transformation_Status__c = 'Association Error';                        
                        assError = true;
						errDevList.add(new Error_Logging__c(Name='Development Tranformation',Error_Message__c='Contact Record is not associated',Fields_Afftected__c=fieldsAffected,isCreated__c=upsertDevelopmentResults[j].isCreated(),isSuccess__c=upsertDevelopmentResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfDev[j].Development_Id__c,Schedule_Job__c=bjId));
					}                    
                    //updDevInt.Schedule_Job_Transformation__c = bjId;
                    //updDevInt.CRM_Development_Id__c = listOfDev[j].Development_ID__c;
                    //updDevIntRes.add(updDevInt);  
                    if(assError)        
                    {
						listOfDevInt[j].Transformation_Status__c = 'Association Error';                        
                    }
                    listOfDevInt[j].Schedule_Job_Transformation__c = bjId;          
                }
            }
            Insert errDevList;
            //Upsert updDevIntRes CRM_Development_Id__c;
            update listOfDevInt;
            if(errDevList.isEmpty())      
            {
                status = 'Completed';
            } 
            else
            {
                status = 'Error';
            }  
            recordCount = recordCount+listOfDevInt.size();  
        }
        catch(Exception e)
        {            
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='Development Transformation',Error_Message__c= e.getMessage()+' Line Number: '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }
    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob devJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
       	{
       		bj.Status__c= devJob.Status;
       	}
       	else
       	{
       		 bj.Status__c=status;
       	}
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = devJob.id;
        bj.End_Time__c  = devJob.CompletedDate;
        upsert bj;
       if (CRM_Transformation_Job__c.getinstance('DevContactTransformation') <> null && CRM_Transformation_Job__c.getinstance('DevContactTransformation').on_off__c)
   	   { 
        	Database.executeBatch(new DevContactTransformation(exJobId));
       }
       if (CRM_Transformation_Job__c.getinstance('StageApplicationTransformation') <> null && CRM_Transformation_Job__c.getinstance('StageApplicationTransformation').on_off__c)
   	   {
        	Database.executeBatch(new StageApplicationTransformation(exJobId));
       }
    }
}