/**
 * Created by alan on 2019-02-26.
 */

public with sharing class DummyController {

    @AuraEnabled
    public static String processRequest(String arg1, String arg2){

        //get singleton instance of service
        DummyService dummyService = (DummyService)ObjectFactory.getInstance(DummyService.class);

        return dummyService.method1(arg1, arg2);

    }

    @AuraEnabled
    public static String processRequest2(String value){

        //get singleton instance of service
        DummyService dummyService = (DummyService)ObjectFactory.getInstance(DummyService.class);
        Map<String, String> arg = new Map<String, String>();
        arg.put('key', value);
        return dummyService.method2(arg);

    }

    @AuraEnabled
    public static String processRequest3(String value){

        //get singleton instance of service
        DummyService dummyService = (DummyService)ObjectFactory.getInstance(DummyService.class);
        //call void method
        dummyService.method3(value);
        return 'ok';
    }

    @AuraEnabled
    public static String processRequest4(){

        //get singleton instance of service
        DummyService dummyService = (DummyService)ObjectFactory.getInstance(DummyService.class);
        //call void method
        return dummyService.method4('1', 2, true, null, new List<String>());
    }

    @AuraEnabled
    public static String processRequest5(boolean callService){

        //get singleton instance of service
        DummyService dummyService = (DummyService)ObjectFactory.getInstance(DummyService.class);
        //call void method
        if(callService) {
            return dummyService.method4('1', 2, true, null, new List<String>());
        }else{
            return 'nothing to do';
        }
    }

    @AuraEnabled
    public static String processRequest6(){

        ReleaseToggle releaseToggle = (ReleaseToggle)ObjectFactory.getInstance(ReleaseToggle.class);
        if(releaseToggle.isActive('MarCaseAdditionalTrailHistory')) {
            return 'Active';
        }else{
            return 'not Active';
        }
    }

}