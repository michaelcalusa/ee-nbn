/**
 * Created by alan on 2019-02-26.
 */

@IsTest
private class DummyControllerTest {

    @IsTest
    static void testWithoutMock() {

        Test.startTest();

        String result = DummyController.processRequest('1', '2');

        //assert result
        Assert.equals('ok', result);

        Test.stopTest();

    }

    @IsTest
    static void testIsolated() {

        RecordingStubProvider stubProvider = new RecordingStubProvider(DummyService.class);
        ArgumentMatcher argMmatcher1 = new StringArgumentMatcher('1');
        ArgumentMatcher argMmatcher2 = new StringArgumentMatcher('2');
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>();
        argMatcherList.add(argMmatcher1);
        argMatcherList.add(argMmatcher2);
        stubProvider.given('method1').when(argMatcherList).thenReturn('not ok');
        ObjectFactory.setStubProvider(DummyService.class, stubProvider);

        Test.startTest();

        String result = DummyController.processRequest('1', '2');

        //assert result
        Assert.equals('not ok', result);
        //verify interaction
//        stubProvider.verify('method1', 1, argMatcherList);

        Test.stopTest();

    }

    @IsTest
    static void testIsolatedWithMultipleArgTypes() {

        RecordingStubProvider stubProvider = new RecordingStubProvider(DummyService.class);
        ArgumentMatcher argMmatcher1 = new StringArgumentMatcher('1');
        ArgumentMatcher argMmatcher2 = new IntegerArgumentMatcher(2);
        ArgumentMatcher argMmatcher3 = new BooleanArgumentMatcher(true);
        ArgumentMatcher argMmatcher4 = new NullArgumentMatcher();
        ArgumentMatcher argMmatcher5 = new GenericObjectArgumentCaptor();
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>();
        argMatcherList.add(argMmatcher1);
        argMatcherList.add(argMmatcher2);
        argMatcherList.add(argMmatcher3);
        argMatcherList.add(argMmatcher4);
        argMatcherList.add(argMmatcher5);
        stubProvider.given('method4').when(argMatcherList).thenReturn('not ok');
        ObjectFactory.setStubProvider(DummyService.class, stubProvider);

        Test.startTest();

        String result = DummyController.processRequest4();

        //assert result
        Assert.equals('not ok', result);
        //verify interaction
//        stubProvider.verify('method1', 1, argMatcherList);

        Test.stopTest();

    }

    @IsTest
    static void testIsolatedWithVerify() {

        RecordingStubProvider stubProvider = new RecordingStubProvider(DummyService.class);

        ObjectFactory.setStubProvider(DummyService.class, stubProvider);

        Test.startTest();

        String result = DummyController.processRequest3('1');

        //assert result
        Assert.equals('ok', result);

        //verify interaction
        ArgumentMatcher argMmatcher1 = new StringArgumentMatcher('1');
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>();
        argMatcherList.add(argMmatcher1);
        //verify first invocation of DummyService method3 with expected args
        stubProvider.verify('method3', 1, argMatcherList);

        Test.stopTest();

    }

    @IsTest
    static void testIsolatedWithException() {

        RecordingStubProvider stubProvider = new RecordingStubProvider(DummyService.class);
        ArgumentMatcher argMmatcher1 = new StringArgumentMatcher('1');
        ArgumentMatcher argMmatcher2 = new StringArgumentMatcher('2');
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>();
        argMatcherList.add(argMmatcher1);
        argMatcherList.add(argMmatcher2);
        stubProvider.given('method1').when(argMatcherList).thenThrow(new CustomException('test generated exception'));
        ObjectFactory.setStubProvider(DummyService.class, stubProvider);

        Test.startTest();

        try {
            DummyController.processRequest('1', '2');
            System.assert(false, 'expected CustomException');
        }catch(CustomException e){
            //verify interaction
            Assert.equals('test generated exception', e.getMessage());
//            stubProvider.verify('method1', 1, argMatcherList);
        }

        Test.stopTest();
    }

    @IsTest
    static void testIsolatedWithArgumentCaptor() {

        RecordingStubProvider stubProvider = new RecordingStubProvider(DummyService.class);
        //an ArgumentCaptor matches all non null object s but captures the arg and allows you to inspect it later
        ArgumentCaptor argCaptor1 = new GenericObjectArgumentCaptor();
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>();
        argMatcherList.add(argCaptor1);
        stubProvider.given('method2').when(argMatcherList).thenReturn('mock value');
        ObjectFactory.setStubProvider(DummyService.class, stubProvider);

        Test.startTest();

        String result = DummyController.processRequest2('value');
        //assert result
        System.assertEquals('mock value', result);
        //verify interaction
//        stubProvider.verify('method2', 1, argMatcherList);
        //inspect argument captor
        Map<String, String> capturedArg = (Map<String, String>)argCaptor1.getCaptured();
        Assert.equals('value', capturedArg.get('key'));

        Test.stopTest();
    }

    @IsTest
    static void testIsolatedWithMultipleInteractions() {

        RecordingStubProvider stubProvider = new RecordingStubProvider(DummyService.class);

        ArgumentMatcher argMmatcher1 = new StringArgumentMatcher('1');
        ArgumentMatcher argMmatcher2 = new StringArgumentMatcher('2');
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>();
        argMatcherList.add(argMmatcher1);
        argMatcherList.add(argMmatcher2);
        stubProvider.given('method1').when(argMatcherList).thenReturn('not ok');

        ArgumentCaptor argCaptor1 = new GenericObjectArgumentCaptor();
        List<ArgumentMatcher> argMatcherList2 = new List<ArgumentMatcher>();
        argMatcherList2.add(argCaptor1);
        stubProvider.given('method2').when(argMatcherList2).thenReturn('mock value');

        ObjectFactory.setStubProvider(DummyService.class, stubProvider);

        Test.startTest();

        String result = DummyController.processRequest('1', '2');

        //assert result
        Assert.equals('not ok', result);
        //verify interaction
//        stubProvider.verify('method1', 1 , argMatcherList);


        result = DummyController.processRequest2('value');
        //assert result
        Assert.equals('mock value', result);
        //verify interaction
//        stubProvider.verify('method2', 1, argMatcherList2);
        //inspect argument captor
        Map<String, String> capturedArg = (Map<String, String>)argCaptor1.getCaptured();
        Assert.equals('value', capturedArg.get('key'));


        Test.stopTest();

    }

    @IsTest
    static void testZeroInteractions() {

        RecordingStubProvider stubProvider = new RecordingStubProvider(DummyService.class);

        ObjectFactory.setStubProvider(DummyService.class, stubProvider);

        Test.startTest();

        String result = DummyController.processRequest5(false);

        //assert result
        Assert.equals('nothing to do', result);
        //verify 0 invocations of the given method
        stubProvider.verify('method4', RecordingStubProvider.NEVER);
        //verify no interactions at all with this service
        stubProvider.verifyZeroInteractions();

        Test.stopTest();

    }

    @IsTest
    static void testReleaseToggle(){

        Test.startTest();

        RecordingStubProvider stubProvider = new RecordingStubProvider(ReleaseToggle.class);
        ObjectFactory.setStubProvider(ReleaseToggle.class, stubProvider);

        List<ArgumentMatcher> argMatchers = new List<ArgumentMatcher>{new StringArgumentMatcher('MarCaseAdditionalTrailHistory')};

        stubProvider.given('isActive')
                .when(argMatchers)
                .thenReturn(true);

        Assert.equals('Active', DummyController.processRequest6());

        stubProvider.given('isActive')
                .when(argMatchers)
                .thenReturn(false);

        Assert.equals('not Active', DummyController.processRequest6());

        Test.stopTest();

    }
}