/**
 * Created by alan on 2019-02-26.
 */

public without sharing class DummyService {

    public String method1(String arg1, String arg2){
        return 'ok';
    }

    public String method2(Map<String, String> arg1){
        return arg1.get('key');
    }

    public void method3(String arg1){

    }

    public String method4(String arg1, Integer arg2, Boolean arg3, Object obj, List<Object> arg4){
        return 'ok';
    }
}