public with sharing class EE_AS_IncidentManagementTriggerHandler {

    private static final String CHANNEL_ID = 'SALESFORCE';
    private static final String DEFAULT_SEVERITY = 'LOW';
    private static final String OPERATION_INSERT =  'Insert';

    private static final String BUSINESS_PROCESS_VERSION = 'V7.0';
    private static final String BUSINESS_SERVICE_VERSION = 'V7.0';
    
    public static void setCorrelationId(List<Incident_Management__c> incidents) {
        for(Incident_Management__c incident : incidents) {
            incident.Correlation_Id__c = DF_IntegrationUtils.generateGUID();
        }
    }

    public static void generatePlatformEvents(List<Incident_Management__c> incidents) {
        
        List<EE_AS_SF_To_Remedy__e> eventsToPublish = new List<EE_AS_SF_To_Remedy__e>();
        Map<ID,Schema.RecordTypeInfo> recordTypeMap = Incident_Management__c.sObjectType.getDescribe().getRecordTypeInfosById();
        
        for(Incident_Management__c incident : incidents) {
            // Only publish platform event message for specific record types
            if(recordTypeMap.get(incident.recordTypeID).getName().equalsIgnoreCase(DF_Constants.RECORD_TYPE_SERVICE_INCIDENT) ||
                recordTypeMap.get(incident.recordTypeID).getName().equalsIgnoreCase(DF_Constants.RECORD_TYPE_SERVICE_REQUEST)
            ) {
                EE_AS_SF_To_Remedy__e eventMsg = new EE_AS_SF_To_Remedy__e();
                eventMsg.OutboundJSONMessage__c = generateCreateIncidentMessage(incident);
                
                eventsToPublish.add(eventMsg);
                System.debug('!@#$% Publishing platform event messages to EE_AS_SF_To_Remedy__e : ' + eventsToPublish);
            }
        }

        if(eventsToPublish.size() > 0)
            EventBus.publish(eventsToPublish);

    }

    private static String generateCreateIncidentMessage(Incident_Management__c incident) {
        
        EE_AS_Remedy_Dto request = new EE_AS_Remedy_Dto();
        // Root
        request.correlationId = incident.Correlation_Id__c;
        request.incidentType = DF_Constants.INCIDENT_RECORD_TYPES.get(incident.RecordTypeId).Name;
        request.operation = OPERATION_INSERT;

        // Incident Data
        request.incidentData = new EE_AS_Remedy_Dto.IncidentData();
        request.incidentData.reportedDate = String.valueOf(incident.CreatedDate.getTime());
        request.incidentData.summary = incident.summary__c;
        request.incidentData.channelId = CHANNEL_ID;
        request.incidentData.severity = DEFAULT_SEVERITY;
        request.incidentData.businessProcessVersion = BUSINESS_PROCESS_VERSION;
        request.incidentData.businessServiceVersion = BUSINESS_SERVICE_VERSION;
        request.incidentData.faultType = incident.Fault_Type__c;

        // RequestDetailsContainer
        request.requestDetailsContainer = new EE_AS_Remedy_Dto.RequestDetailsContainer();

        // categoryContainer
        if ( request.incidentType.equals(DF_Constants.RECORD_TYPE_SERVICE_REQUEST) && String.isNotBlank(incident.Service_Request_Type__c) ){
            request.categoryListContainer = new List<EE_AS_Remedy_Dto.CategoryListContainer>();
            EE_AS_Remedy_Dto.CategoryListContainer categoryContainer = new EE_AS_Remedy_Dto.CategoryListContainer();
            categoryContainer.opCat2 = incident.Service_Request_Type__c;
            categoryContainer.opCat3 = incident.Service_Request_Issue_Type__c;
            request.categoryListContainer.add(categoryContainer);

            //AccessSeeker
            request.accessSeekerId = incident.Access_Seeker_ID__c;
            // BPI/PRI Id 
            request.incidentData.priId = incident.PRI_ID__c;

        }
        else {
            List<TND_Result__c> tnds = [SELECT Id, Req_OVC_Id__c, Req_Class_Of_Service__c, RecordType.Name, Related_Order__r.BPI_Id__c, Related_Order__r.Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c FROM TND_Result__c WHERE Id = :incident.Test_Reference__c];
            if(tnds != null && tnds.size()>0) {
                TND_Result__c tndResult = tnds.get(0);

                // Test Results
                request.testResults = new List<EE_AS_Remedy_Dto.TestResult>();
                EE_AS_Remedy_Dto.TestResult testResult = new EE_AS_Remedy_Dto.TestResult();
                testResult.testReferenceId = incident.Test_Reference__c;
                testResult.testType = tndResult.RecordType.Name;
                request.testResults.add(testResult);

                // Service List Container
                request.serviceListContainer = new List<String>();
                String impactedOVC = incident.Impacted_OVC__c;
                if(String.isNotBlank(impactedOVC)) {
                    impactedOVC= impactedOVC.replaceAll( '\\s+', '');
                }
                if(impactedOVC != null && impactedOVC.contains(',')) {
                    request.serviceListContainer = impactedOVC.split(',');
                } else {
                    request.serviceListContainer.add(impactedOVC);
                }

                // Access Seeker Info
                request.accessSeekerId = tnds.get(0).Related_Order__r.Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c;
            
                // BPI/PRI Id
                request.incidentData.priId = tnds.get(0).Related_Order__r.BPI_Id__c;
            
                // Class Of Service parsing
                request.requestDetailsContainer.cos = '';
                List<String> cosValues = new List<String>();
                if(tndResult.Req_Class_Of_Service__c != null) {
                    if(tndResult.Req_Class_Of_Service__c.contains(';'))
                        cosValues = tndResult.Req_Class_Of_Service__c.split(';');
                    else
                        cosValues.add(tndResult.Req_Class_Of_Service__c);    
                }
                for(String cosValue : cosValues) {
                    request.requestDetailsContainer.cos = request.requestDetailsContainer.cos + cosValue;
                    request.requestDetailsContainer.cos = request.requestDetailsContainer.cos + ',';
                }
                if(String.isNotBlank(request.requestDetailsContainer.cos)) {
                    request.requestDetailsContainer.cos = request.requestDetailsContainer.cos.substring(0, request.requestDetailsContainer.cos.length()-2);
                    request.requestDetailsContainer.cos = '['+request.requestDetailsContainer.cos+']';
                }
            }

        }
        
        request.requestDetailsContainer.descriptionOfProblem = incident.Incident_Notes__c;
        request.requestDetailsContainer.technicalContactName = incident.Technical_Contact_Name__c;
        request.requestDetailsContainer.technicalContactNumber = incident.Technical_Contact_Mobile_Number__c;
        request.requestDetailsContainer.siteContactName = incident.Site_Contact_Name__c;
        request.requestDetailsContainer.siteContactNumber = incident.Site_Contact_Mobile_Number__c;

        request.requestDetailsContainer.businessName = incident.Business_Name__c;
        request.requestDetailsContainer.siteInductionEssentials = incident.Site_Induction_Essentials__c;
        request.requestDetailsContainer.securityRequired = incident.Security_Required__c;
        request.requestDetailsContainer.currentWhiteCard = incident.Current_White_Card__c;
        request.requestDetailsContainer.securityClearance = incident.Security_Clearance__c;
        request.requestDetailsContainer.siteAccessClearance = incident.Site_Access_Clearance__c;
        request.requestDetailsContainer.other = incident.summary__c;
        request.requestDetailsContainer.locationId = incident.locId__c;
        request.requestDetailsContainer.siteAvailability = incident.Site_Operating_Hours__c;
        request.requestDetailsContainer.additionalComments = incident.Additional_Comments__c;

        if(incident.Availability_Date__c != null) {
            if(incident.Availability_Start_Time__c != null) {

                String timeString =  String.valueOf(incident.Availability_Start_Time__c);
                DateTime availabilityStartDateTime = DateTime.newInstance(incident.Availability_Date__c,Time.newInstance(
                                                Integer.valueOf(timeString.split(':')[0]),
                                                Integer.valueof(timeString.split(':')[1]), 
                                                0, 0));
                request.incidentData.appAvailStartDatetime = String.valueOf(availabilityStartDateTime.getTime());    
            }
            if(incident.Availability_End_Time__c != null) {

                String timeString =  String.valueOf(incident.Availability_End_Time__c);
                DateTime availabilityEndDateTime = DateTime.newInstance(incident.Availability_Date__c,Time.newInstance(
                                                Integer.valueOf(timeString.split(':')[0]),
                                                Integer.valueof(timeString.split(':')[1]), 
                                                0, 0));
                request.incidentData.appAvailEndDatetime = String.valueOf(availabilityEndDateTime.getTime());    
            }      
        }
        
        String serializedRequest = System.JSON.serializePretty(request);
        System.debug('Inside generateCreateIncidentMessage:: serializedRequest : ' + serializedRequest);
        return serializedRequest;
        
    }

    public static void deliverEmail(List<Incident_Management__c> incidents) {

        Map<String, String> incidentToTemplateMap = new Map<String, String>();

        for(Incident_Management__c incident : incidents) {
            String templateName = '';
            
            String currentRecordType = '';
            if(incident.RecordTypeId != null)
                currentRecordType = DF_Constants.INCIDENT_RECORD_TYPES.get(incident.RecordTypeId).Name;
            
            System.debug('!@#$% Current Record Type : ' + currentRecordType);
            System.debug('!@#$% Current Status : ' + incident.Industry_Status__c);

            templateName = DF_AS_GlobalUtility.getEmailTemplateName(currentRecordType, incident.Industry_Status__c, incident.Notification_Type__c);

            System.debug('!@#$% Email template being used : ' + templateName);

            if(String.isNotBlank(templateName)) {
                incidentToTemplateMap.put(incident.id, templateName);
            }
        }

        DF_AS_GlobalUtility.sendEmail(incidentToTemplateMap);
    }
    
    

}