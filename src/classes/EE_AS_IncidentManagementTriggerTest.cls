@IsTest private class EE_AS_IncidentManagementTriggerTest
{

    static void setUp(){
        Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;

        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        insert dfQuote;
    
        // Create DF Order
        DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        dfOrder.BPI_Id__c = 'BPI0000000011';
        insert dfOrder;

    }
	@IsTest static void testCorrelationId()
	{

		Account acct = DF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;

		DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
		dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
		insert dfQuote;
	
		// Create DF Order
		DF_Order__c dfOrder = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
		dfOrder.BPI_Id__c = 'BPI0000000011';
		insert dfOrder;

		ID loopbackRecTypeId = Schema.SObjectType.TND_Result__c.getRecordTypeInfosByName().get(EE_AS_TnDAPIUtils.TND_TEST_TYPE_LOOPBACK).getRecordTypeId();
		
		List<TND_Result__c> tndRecs = new  List<TND_Result__c>();

		TND_Result__c tndResult1 = new  TND_Result__c();
		tndResult1.Reference_Id__c = 'REF4567890565656';
		tndResult1.recordtypeid = loopbackRecTypeId;
		tndResult1.Test_Id__c = 'TST_0000277';
		tndResult1.Access_Technology__c = 'Enterprise Ethernet';
		tndResult1.Channel__c = 'SALESFORCE';
		tndResult1.Correlation_Id__c = '3c11a419-c552-4d0d-aae45545435353';
		tndResult1.Req_Class_Of_Service__c = 'HIGH;MEDIUM';
		tndResult1.Req_OVC_Id__c = 'OVC123456789033';
		tndResult1.Req_Packet_Size__c = 65;
		tndResult1.Requested_Date__c = Datetime.now();
		tndResult1.Requested_By__c = UserInfo.getUserId();
		tndResult1.Status__c = 'Pending';
		tndResult1.Completion_Date__c = Datetime.now();
		
		tndRecs.add(tndResult1);

		TND_Result__c tndResult2 = new  TND_Result__c();
		tndResult2.Reference_Id__c = 'REF4567890';
		tndResult2.recordtypeid = loopbackRecTypeId;
		tndResult2.Test_Id__c = 'TST_00002';
		tndResult2.Access_Technology__c = 'Enterprise Ethernet';
		tndResult2.Channel__c = 'SALESFORCE';
		tndResult2.Correlation_Id__c = '3c11a419-c552-4d0d-aae455454545454';
		tndResult2.Req_Class_Of_Service__c = 'HIGH';
		tndResult2.Req_OVC_Id__c = 'OVC123456789012';
		tndResult2.Req_Packet_Size__c = 65;
		tndResult2.Requested_Date__c = Datetime.now();
		tndResult2.Requested_By__c = UserInfo.getUserId();
		tndResult2.Status__c = 'Complete';
		tndResult2.Completion_Date__c = Datetime.now().addMinutes(-30);

		tndRecs.add(tndResult2);
		
		Insert tndRecs;

		DateTime dt = System.today();
		Time myTime = Time.newInstance(dt.hour(), dt.minute(), dt.second(), dt.millisecond());
		
		List<Incident_Management__c> incidents = new List<Incident_Management__c>();
		// create service incident record
        Incident_Management__c serviceIncident1 = new Incident_Management__c();
        serviceIncident1.PRI_ID__c = 'SI12345';
        serviceIncident1.Incident_Number__c = ''; 
        serviceIncident1.Incident_Type__c = 'Service Incident'; 
        serviceIncident1.Industry_Status__c = 'Acknowledged';
        serviceIncident1.serviceRestorationSLA__c = 'Standard';
        serviceIncident1.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Service Incident').getRecordTypeId();
        serviceIncident1.Correlation_Id__c = '8ebe7f93-56dc-4145-951c-99999';
        serviceIncident1.summary__c='Summary';
        serviceIncident1.Fault_Type__c = 'Fault Type';
        serviceIncident1.Incident_Notes__c = 'Note 1';
        serviceIncident1.Technical_Contact_Name__c = 'Test Contact 1';
        serviceIncident1.Availability_Date__c = System.today();
        serviceIncident1.Availability_Start_Time__c = myTime;
        serviceIncident1.Availability_End_Time__c = myTime;
        serviceIncident1.CreatedDate = System.now();
        serviceIncident1.Test_Reference__c = tndResult1.Id;
        incidents.add(serviceIncident1);

        Incident_Management__c serviceIncident2 = new Incident_Management__c();
        serviceIncident2.PRI_ID__c = 'SI56789';
        serviceIncident2.Incident_Number__c = ''; 
        serviceIncident2.Incident_Type__c = 'Service Incident'; 
        serviceIncident2.Industry_Status__c = 'Acknowledged';
        serviceIncident2.serviceRestorationSLA__c = 'Standard';
        serviceIncident2.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Service Incident').getRecordTypeId();
        serviceIncident2.Correlation_Id__c = '8ebe7f93-56dc-4145-951c-99799';
        serviceIncident2.summary__c='Summary';
        serviceIncident2.Fault_Type__c = 'Fault Type';
        serviceIncident2.Incident_Notes__c = 'Note 1';
        serviceIncident2.Technical_Contact_Name__c = 'Test Contact 1';
        serviceIncident2.Availability_Date__c = System.today();
        serviceIncident2.Availability_Start_Time__c = myTime;
        serviceIncident2.Availability_End_Time__c = myTime;
        serviceIncident2.CreatedDate = System.now();
        serviceIncident2.Test_Reference__c = tndResult2.Id;
        incidents.add(serviceIncident2);

        // create service alert record
        Incident_Management__c serviceAlert1 = new Incident_Management__c();
        serviceAlert1.PRI_ID__c = 'SA12345';
        serviceAlert1.Incident_Number__c = 'SA67890'; 
        serviceAlert1.Incident_Type__c = 'Service Alert'; 
        serviceAlert1.Industry_Status__c = EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_INCIDENT_CLOSED;
        serviceAlert1.serviceRestorationSLA__c = 'Standard';
        serviceAlert1.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Service Alert').getRecordTypeId();
        incidents.add(serviceAlert1);

        Incident_Management__c serviceAlert2 = new Incident_Management__c();
        serviceAlert2.PRI_ID__c = 'SA12';
        serviceAlert2.Incident_Number__c = 'SA67'; 
        serviceAlert2.Incident_Type__c = 'Service Alert'; 
        serviceAlert2.Industry_Status__c = EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_INCIDENT_RESOLVED;
        serviceAlert2.serviceRestorationSLA__c = 'Standard';
        serviceAlert2.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Service Alert').getRecordTypeId();
        incidents.add(serviceAlert2);

        Incident_Management__c serviceAlert3 = new Incident_Management__c();
        serviceAlert3.PRI_ID__c = 'SA1290';
        serviceAlert3.Incident_Number__c = 'SA6712'; 
        serviceAlert3.Incident_Type__c = 'Service Alert'; 
        serviceAlert3.Industry_Status__c = EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_TICKET_UPDATED_NBNCO;
        serviceAlert3.serviceRestorationSLA__c = 'Standard';
        serviceAlert3.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Service Alert').getRecordTypeId();
        incidents.add(serviceAlert3);

        Incident_Management__c serviceAlert4 = new Incident_Management__c();
        serviceAlert4.PRI_ID__c = 'SA1294';
        serviceAlert4.Incident_Number__c = 'SA67121'; 
        serviceAlert4.Incident_Type__c = 'Service Alert'; 
        serviceAlert4.Industry_Status__c = EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_INCIDENT_CREATED;
        serviceAlert4.serviceRestorationSLA__c = 'Standard';
        serviceAlert4.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Service Alert').getRecordTypeId();
        incidents.add(serviceAlert4);

        // create network incident record
        Incident_Management__c networkIncident1 = new Incident_Management__c();
        networkIncident1.PRI_ID__c = 'NI12345';
        networkIncident1.Incident_Number__c = 'NI67890'; 
        networkIncident1.Incident_Type__c = 'Network Incident'; 
        networkIncident1.Industry_Status__c = EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_INCIDENT_CLOSED;
        networkIncident1.serviceRestorationSLA__c = 'Standard';
        networkIncident1.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Network Incident').getRecordTypeId();
        incidents.add(networkIncident1);

        Incident_Management__c networkIncident2 = new Incident_Management__c();
        networkIncident2.PRI_ID__c = 'NI1234';
        networkIncident2.Incident_Number__c = 'NI6789'; 
        networkIncident2.Incident_Type__c = 'Network Incident'; 
        networkIncident2.Industry_Status__c = EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_INCIDENT_RESOLVED;
        networkIncident2.serviceRestorationSLA__c = 'Standard';
        networkIncident2.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Network Incident').getRecordTypeId();
        incidents.add(networkIncident2);

        Incident_Management__c networkIncident3 = new Incident_Management__c();
        networkIncident3.PRI_ID__c = 'NI12341';
        networkIncident3.Incident_Number__c = 'NI67891'; 
        networkIncident3.Incident_Type__c = 'Network Incident'; 
        networkIncident3.Industry_Status__c = EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_TICKET_UPDATED_NBNCO;
        networkIncident3.serviceRestorationSLA__c = 'Standard';
        networkIncident3.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Network Incident').getRecordTypeId();
        incidents.add(networkIncident3);

        Incident_Management__c networkIncident4 = new Incident_Management__c();
        networkIncident4.PRI_ID__c = 'NI12342';
        networkIncident4.Incident_Number__c = 'NI67892'; 
        networkIncident4.Incident_Type__c = 'Network Incident'; 
        networkIncident4.Industry_Status__c = EE_AS_Remedy_To_SF_TriggerHandler.NOTIFICATION_STATUS_INCIDENT_CREATED;
        networkIncident4.serviceRestorationSLA__c = 'Standard';
        networkIncident4.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Network Incident').getRecordTypeId();
        incidents.add(networkIncident4);

        // create change request record
        Incident_Management__c changeRequest1 = new Incident_Management__c();
        changeRequest1.PRI_ID__c = 'CRQ12345';
        changeRequest1.Incident_Number__c = 'CRQ67890'; 
        changeRequest1.Incident_Type__c = 'Change Request'; 
        changeRequest1.Industry_Status__c = EE_AS_Remedy_To_SF_TriggerHandler.INCIDENT_STATUS_CANCELLED;
        changeRequest1.serviceRestorationSLA__c = 'Standard';
        changeRequest1.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Change Request').getRecordTypeId();
        incidents.add(changeRequest1);

        Incident_Management__c changeRequest2 = new Incident_Management__c();
        changeRequest2.PRI_ID__c = 'CRQ123454';
        changeRequest2.Incident_Number__c = 'CRQ678904'; 
        changeRequest2.Incident_Type__c = 'Change Request'; 
        changeRequest2.Industry_Status__c = EE_AS_Remedy_To_SF_TriggerHandler.INCIDENT_STATUS_COMPLETE;
        changeRequest2.serviceRestorationSLA__c = 'Standard';
        changeRequest2.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Change Request').getRecordTypeId();
        incidents.add(changeRequest2);

        Incident_Management__c changeRequest3 = new Incident_Management__c();
        changeRequest3.PRI_ID__c = 'CRQ123455';
        changeRequest3.Incident_Number__c = 'CRQ678905'; 
        changeRequest3.Incident_Type__c = 'Change Request'; 
        changeRequest3.Industry_Status__c = EE_AS_Remedy_To_SF_TriggerHandler.INCIDENT_STATUS_PAUSED;
        changeRequest3.serviceRestorationSLA__c = 'Standard';
        changeRequest3.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Change Request').getRecordTypeId();
        incidents.add(changeRequest3);

        Test.startTest();
        EE_AS_IncidentManagementTriggerHandler.setCorrelationId(incidents);
        insert incidents;
        Test.stopTest();
        for(Incident_Management__c incident : incidents) {
            System.assertNotEquals(incident.Correlation_Id__c, null);
        }
	}

    @IsTest static void testEmailNotificationForServiceIncident(){

    String notificationJSON = '{\"correlationId\":\"e6b358ad-439c-4311-b263-fd266baa2e9c\",\"accessSeekerId\":\"ASI000000000019\",\"incidentId\":\"INC01109180130\",\"incidentType\":\"User Service Restoration\",\"incidentData\":{\"summary\":\"Automation Incident at2018-06-20T10:12:32Z\",\"notifyRequestType\":\"notification\",\"currentStatus\":\"New\",\"reportedDate\":\"1529489552000\",\"impact\":\"4\",\"descriptionOfProblem\":\"Description of Problem :null Technical Contact Name :TECHNICAL CONTACT FIVE Technical Contact Number :0433433433 Site Contact Name :SITE CONTACT FIVE Site Contact Number :0456789123 Business Name :GGG Site Induction Essentials :No Security Required :No Current WhiteCard :No Security Clearance :No Site Access Clearance :No Other Comments :null Additional Comments :Some comments\",\"industryStatus\":\"Acknowledged\",\"industrySubStatus\":\"\",\"notificationType\":\"TicketUpdatedNBNCo\",\"priId\":\"BPI000000000377\",\"category\":\"Other\",\"notifyRequestSubType\":\"incident\",\"industryReasonCode\":\"\",\"resolutionRejectionDeclineCode\":\"\",\"industryResolutionCode\":\"\",\"nbnProblemId\":\"\",\"impactedLocation\":\"\",\"urgency\":\"\",\"severity\":\"\"},\"serviceListContainer\":[\"OVC148510000001\"],\"requestDetailsContainer\":{\"SiteContactName\":\"SITE CONTACT FIVE\",\"BusinessName\":\"GGG\",\"CoS\":\"[HIGH, MEDIUM, LOW]\",\"SiteContactNumber\":\"0456789123\",\"SecurityClearance\":\"No\",\"SecurityRequired\":\"No\",\"SiteInductionEssentials\":\"No\",\"TechnicalContactName\":\"TECHNICAL CONTACT FIVE\",\"AdditionalComments\":\"Some comments\",\"SiteAccessClearance\":\"No\",\"SiteAvailability\":\"Other\",\"TechnicalContactNumber\":\"0433433433\",\"CurrentWhiteCard\":\"No\"},\"testResults\":[{\"testType\":\" BTD Status\",\"id\":\"05886d5a-9f44-41b3-9987-f64dfa09c5d9\"}],\"categoryListContainer\":[{\"OpCat1\":\"Customer Incident\",\"ProdCat1\":\"EEAS\"}]}';

    EE_AS_Remedy_To_SF__e event = new EE_AS_Remedy_To_SF__e();
    event.InboundJSONMessage__c = notificationJSON;

    Test.startTest();
    EventBus.publish(event);
    Test.stopTest();

    Incident_Management__c inc = [select notification_Type__c from Incident_Management__c where incident_number__c ='INC01109180130' Limit 1];

    system.assertEquals('TicketUpdatedNBNCo',inc.notification_Type__c);

    }

    @IsTest static void testCreateServiceRequest() {
        setUp();
            
        DateTime dt = System.today();
        Time myTime = Time.newInstance(dt.hour(), dt.minute(), dt.second(), dt.millisecond());
        
        List<Incident_Management__c> incidents = new List<Incident_Management__c>();
        // create service incident record
        Incident_Management__c serviceRequest = new Incident_Management__c();
        serviceRequest.PRI_ID__c = 'BPI000000000019';
        serviceRequest.Industry_Status__c = 'Acknowledged';
        serviceRequest.serviceRestorationSLA__c = 'Standard';
        serviceRequest.RecordTypeId = Schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
        serviceRequest.summary__c='Summary';
        serviceRequest.Incident_Notes__c = 'Note 1';
        serviceRequest.Service_Request_Issue_Type__c = 'BTD redundent power supply';
        serviceRequest.Service_Request_Type__c = 'Equipment';
        serviceRequest.Technical_Contact_Name__c = 'Test Contact 1';
        serviceRequest.CreatedDate = System.now();
        incidents.add(serviceRequest);
        Test.startTest();
        EE_AS_IncidentManagementTriggerHandler.setCorrelationId(incidents);
        insert incidents;
        Test.stopTest();
        for(Incident_Management__c incident : incidents) {
            Incident_Management__c incidentRec = [SELECT Id,Correlation_Id__c, Incident_Number__c FROM Incident_Management__c WHERE Correlation_Id__c=:incident.Correlation_Id__c LIMIT 1];
            System.assertNotEquals(incidentRec.Correlation_Id__c, null);
        }
    }

}