public without sharing class EE_AS_IncidentNoteAccessSeekerTgrHandler {
        
    public static void callIncidentNoteEmailToContactMatrix(List<EE_AS_IncidentNoteAccessSeeker__c> newList) {   
        for (EE_AS_IncidentNoteAccessSeeker__c incident : newList) {
            DF_AS_GlobalUtility.sendIncidentNoteEmailToContactMatrix(incident.Id);
        }
    }  
}