@isTest
private class EE_AS_IncidentNoteAccessSeekerTgrTest
{
    @testSetup
    static void setupTestData() {

        String EE_AS_CONTACT_MATRIX_ROLE = 'Operational - Assurance';
        String STATUS_ACTIVE = 'Active';
        
        DF_AS_TestService.createEmailCustomSettings();
        
        List<Account> newAccountList = new List<Account>();
        // create account1 record
        Account account1 = new Account();
        account1.Name = 'Telstra';
        account1.Access_Seeker_ID__c = 'AS123';
        //insert account1;
        newAccountList.add(account1);
        
        Account account2 = new Account();
        account2.Name = 'nbn';
        account2.Access_Seeker_ID__c = 'AS7098';
        //insert account2;
        newAccountList.add(account2);
        insert newAccountList;
        
        List<Contact> newContactList = new List<Contact>();
        Contact contact1 = new Contact();
        contact1.FirstName = 'FN1';
        contact1.LastName = 'LN1';
        contact1.Account = account1;
        contact1.Email = '123@mailinator.com';
        contact1.Status__c = STATUS_ACTIVE;
        contact1.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact1.AccountId=account1.Id;
        //insert contact1;
        newContactList.add(contact1);

        Contact contact2 = new Contact();
        contact2.FirstName = 'NBN FN1';
        contact2.LastName = 'NBN LN1';
        contact2.Account = account2;
        contact2.Email = '456@mailinator.com';
        contact2.Status__c = STATUS_ACTIVE;
        contact2.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact2.AccountId=account2.Id;
        //insert contact2;
        newContactList.add(contact2);
        
        insert newContactList;
        
        List<Team_Member__c> newTeamMemberList = new List<Team_Member__c>();
        Team_Member__c teamMember1 = new Team_Member__c();
        teamMember1.RecordTypeId = Schema.SObjectType.Team_Member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        teamMember1.Account__c = account1.Id;
        teamMember1.Customer_Contact__c = contact1.Id;
        teamMember1.Customer_Inbox__c = null;
        teamMember1.NBN_Contact__c = contact2.Id;
        teamMember1.NBN_Inbox__c = null;
        teamMember1.Role__c = EE_AS_CONTACT_MATRIX_ROLE;
        teamMember1.Role_Type__c = 'Tickets - Service Request';
        //insert teamMember1;
        newTeamMemberList.add(teamMember1);

        insert newTeamMemberList;
        
        //Create incident
        List<Incident_Management__c> newIncMgmtList = new List<Incident_Management__c>();
        Incident_Management__c inc1 = getIncidentanagementValues('INC0123', 'Service Request', DF_Constants.SERVICE_REQUEST_NOTIFIC_TYPE_ACKNOWLEDGED, account1.Access_Seeker_ID__c);
        newIncMgmtList.add(inc1);
        
        insert newIncMgmtList;
        
        List<EE_AS_WorkLog__c> newWorkLogList = new List<EE_AS_WorkLog__c>();
        EE_AS_WorkLog__c workLog1 = getWorklogValues(inc1.Id);        
        newWorkLogList.add(workLog1);
               
        insert newWorkLogList;
    }
    
    static Incident_Management__c getIncidentanagementValues(string incNum, string recType, string notType, string accessSeekerId) {
        Incident_Management__c inc = new  Incident_Management__c();
        inc.Incident_Number__c = incNum;
        inc.Access_Seeker_ID__c = accessSeekerId;
        inc.recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get(recType).getRecordTypeId();
        inc.Notification_Type__c = notType;  
        
        return inc;
    }
    
    static EE_AS_WorkLog__c getWorklogValues(Id incId) {
        EE_AS_WorkLog__c workLog = new  EE_AS_WorkLog__c();
        workLog.Work_Log_Type__c = 'Detail Clarification';
        workLog.Note_Summary__c = '  Summary';
        workLog.Note_Source__c = 'Web';
        workLog.Provided_By__c = 'SALESFORCE';
        workLog.Note_Details__c = 'Some Note';
        workLog.Created_By_SFDC__c = true;
        workLog.Parent_Incident__c = incId; 
        return workLog;
    }
    
    @isTest
    static void testInsertIncidentNotes()
    {
        
        //Create incident notes
        Account acc = [select id, Access_Seeker_ID__c from account where Access_Seeker_ID__c = 'AS123' limit 1];
        EE_AS_IncidentNoteAccessSeeker__c objNote = new EE_AS_IncidentNoteAccessSeeker__c();
        objNote.Incident_Note__c = [select id from EE_AS_WorkLog__c where Parent_Incident__r.Incident_Number__c = 'INC0123'].Id;
        objNote.Account__c = acc.Id;
        objNote.Access_Seeker__c = acc.Access_Seeker_ID__c;
        
        Test.startTest();
        Integer emailsCountSentBefore = Limits.getEmailInvocations();
        insert objNote;
        System.assertNotEquals(1,Limits.getEmailInvocations() - emailsCountSentBefore,'Should be increased by 1 or other value how many emails have been sent');
        Test.stopTest();
    }
}