/**
* Class for EE AS incident note trigger handler.
*/
public with sharing class EE_AS_IncidentNoteTriggerHandler
{
  // This should be used in conjunction with the ApexTriggerComprehensive.trigger template
  // The origin of this pattern is http://www.embracingthecloud.com/2010/07/08/ASimpleTriggerTemplateForSalesforce.aspx
  
  private boolean m_isExecuting = false;
  private integer batchSize = 0;
  private static final string CHANNEL_ID = 'SALESFORCE';
  private static final string BUSINESS_PROCESS_VERSION = 'V7.0';
  private static final string BUSINESS_SERVICE_VERSION = 'V7.0';
  private static final string CON_EVENT_TROUBLE_TICKET_AMEND = 'troubleTicketAmend';
  private static final string CON_EVENT_TROUBLE_MORE_INFO = 'provideMoreInformationResponse';
  private static final string NOTE_TYPE_GEN_INFO = 'General Information';
  private static final string NOTE_TYPE_ADDITIONAL_INFO = 'Detail Clarification';
  private static final string NOTE_SYNC_STATUS_REMEDY_PENDING = 'Pending';
  @testVisible
  public static integer eventPublishedCount = 0;

  /**
  * Constructs the object.
  *
  * @param      isExecuting  Indicates if executing
  * @param      size         The size
  */
  public EE_AS_IncidentNoteTriggerHandler(boolean isExecuting, integer size)
  {
    m_isExecuting = isExecuting;
    BatchSize = size;
  }

  /**
  * { function_description }
  *
  * @param      newRecords  The new records
  */
  public void OnAfterInsert(EE_AS_WorkLog__c[] newRecords)
  {
    generatePlatformEvent(newRecords);
  }
  
  /**
  * Generate Platform Event
  *
  * @param      incidentNotes  The incident notes
  */
 
  public static void generatePlatformEvent(List<EE_AS_WorkLog__c> incidentNotes)
  {
    try
    {
      List<EE_AS_SF_To_Remedy__e> eventsToPublish = new  List<EE_AS_SF_To_Remedy__e>();
      List<EE_AS_WorkLog__c> noteToFireEvent = new  List<EE_AS_WorkLog__c>();
      Set<Id> incidentIds = new  Set<Id>();
      for(EE_AS_WorkLog__c note: incidentNotes)
      {
        if(note.Created_By_SFDC__c)
        {
          //Collect all the Note created by SFDC so that we can fire event only for those notes
          noteToFireEvent.add(note);
          if(note.parent_incident__c != null)
          {
            incidentIds.add(note.parent_incident__c);
          }
        }
      }
      Map<Id, Incident_Management__c> incidentEntries = new  Map<Id, Incident_Management__c>([SELECT
        Incident_Number__c, 
        DF_Order__r.Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c,
		PRI_ID__c
      FROM
        Incident_Management__c
      WHERE
        id in: incidentIds]);
      List<EE_AS_WorkLog__c> workLogToUpdate = new  List<EE_AS_WorkLog__c>();
      for(EE_AS_WorkLog__c note: noteToFireEvent)
      {
        EE_AS_SF_To_Remedy__e eventMsg = new  EE_AS_SF_To_Remedy__e();
        string correlationId = DF_IntegrationUtils.generateGUID();
        workLogToUpdate.add(new  EE_AS_WorkLog__c(id = note.id, Correlation_Id__c = correlationId, Sync_Status__c=NOTE_SYNC_STATUS_REMEDY_PENDING));
        if(incidentEntries != null && incidentEntries.get(note.parent_incident__c) != null)
        {
          string incidentNumber = incidentEntries.get(note.parent_incident__c).Incident_Number__c;
          string accessSeekerId = incidentEntries.get(note.parent_incident__c).DF_Order__r.Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c;
          if(accessSeekerId == null) {
              List<DF_Order__c> lstOrder = [select Access_Seeker_ID__c from DF_Order__c where BPI_Id__c = :incidentEntries.get(note.parent_incident__c).PRI_ID__c limit 1];
              if(!lstOrder.isEmpty()) {
                  accessSeekerId = lstOrder[0].Access_Seeker_ID__c;
              }
          }
          system.debug('::accessSeekerId ::'+accessSeekerId);
		  eventMsg.OutboundJSONMessage__c = generateUpdateIncidentMessage(note, incidentNumber, accessSeekerId, correlationId);
        }
        eventsToPublish.add(eventMsg);
      }
      if(workLogToUpdate.size() > 0)
      {
        Update workLogToUpdate;
      }
      if(eventsToPublish.size() > 0)
      {
        eventPublishedCount = eventsToPublish.size();
        EventBus.publish(eventsToPublish);
      }
    }
    catch(Exception e)
    {
      System.debug(e.getMessage());
      System.debug(e.getStackTraceString());
      GlobalUtility.logMessage(EE_AS_TnDAPIUtils.ERROR, EE_AS_IncidentNoteTriggerHandler.class.getName(), 'generatePlatformEvent', '', '', e.getMessage(), JSON.serialize(incidentNotes), e, 0);
    }
  }

  /**
  * Generate Update incident Message
  *
  * @param      note            The note
  * @param      incidentNumber  The incident number
  * @param      accessSeekerId  The access seeker identifier
  *
  * @return     String JSON
  */
  private static string generateUpdateIncidentMessage(EE_AS_WorkLog__c note, string incidentNumber, string accessSeekerId, string correlationId)
  {
    EE_AS_Remedy_Dto remedyDTO = new  EE_AS_Remedy_Dto();
    remedyDTO.correlationId = correlationId;
    remedyDTO.incidentId = incidentNumber;
    //TODO externalise using custom settings
    remedyDTO.operation = 'Update';
    remedyDTO.accessSeekerId = accessSeekerId;
    EE_AS_Remedy_Dto.IncidentData incidentData = new  EE_AS_Remedy_Dto.IncidentData();
    if(NOTE_TYPE_GEN_INFO.equalsIgnoreCase(note.Work_Log_Type__c))
    {
      incidentData.accessSeekerConversationEvent = CON_EVENT_TROUBLE_TICKET_AMEND;
    }
    else if(NOTE_TYPE_ADDITIONAL_INFO.equalsIgnoreCase(note.Work_Log_Type__c))
    {
      incidentData.accessSeekerConversationEvent = CON_EVENT_TROUBLE_MORE_INFO;
    }
    incidentData.businessProcessVersion = BUSINESS_PROCESS_VERSION;
    incidentData.businessServiceVersion = BUSINESS_SERVICE_VERSION;
    incidentData.channelId = CHANNEL_ID;
    remedyDTO.incidentData = incidentData;
    EE_AS_Remedy_Dto.WorkInfoContainer workinfo = new  EE_AS_Remedy_Dto.WorkInfoContainer();
    workinfo.channelId = CHANNEL_ID;
    workinfo.type = note.Work_Log_Type__c;
    workinfo.summary = note.Note_Summary__c;
    workinfo.source = note.Note_Source__c;
    workinfo.providedBy = note.Provided_By__c;
    workinfo.notes = note.Note_Details__c;
    remedyDTO.workInfoContainer = new List<EE_AS_Remedy_Dto.WorkInfoContainer>{workinfo};
    string serializedRequest = System.JSON.serialize(remedyDTO, true);
    return serializedRequest;
  }

}