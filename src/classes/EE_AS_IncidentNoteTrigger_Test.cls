@isTest
private class EE_AS_IncidentNoteTrigger_Test
{
	@isTest
	static void testUpdateIncidentPositive()
	{
		//Create incident
		Incident_Management__c inc = new  Incident_Management__c();
		inc.Incident_Number__c = 'INC09988665545';
		inc.Access_Seeker_ID__c = 'ASI000000892811';
		Insert inc;
		EE_AS_WorkLog__c workLog = new  EE_AS_WorkLog__c();
		workLog.Work_Log_Type__c = 'Detail Clarification';
		workLog.Note_Summary__c = '	Summary';
		workLog.Note_Source__c = 'Web';
		workLog.Provided_By__c = 'SALESFORCE';
		workLog.Note_Details__c = 'Some Note';
		workLog.Created_By_SFDC__c = true;
		workLog.Parent_Incident__c = inc.id;
		Test.startTest();
		Insert workLog;
		Test.stopTest();
		System.assertEquals(1, EE_AS_IncidentNoteTriggerHandler.eventPublishedCount);
	}
	
	@isTest
	static void testUpdateIncidentNegative()
	{
		Incident_Management__c inc = new  Incident_Management__c();
		inc.Incident_Number__c = 'INC09988665545';
		inc.Access_Seeker_ID__c = 'ASI000000892811';
		Insert inc;
		EE_AS_WorkLog__c workLog = new  EE_AS_WorkLog__c();
		workLog.Work_Log_Type__c = 'Detail Clarification';
		workLog.Note_Summary__c = '	Summary';
		workLog.Note_Source__c = 'Web';
		workLog.Provided_By__c = 'SALESFORCE';
		workLog.Note_Details__c = 'Some Note';
		workLog.Created_By_SFDC__c = false;
		workLog.Parent_Incident__c = inc.id;
		Test.startTest();
		Insert workLog;
		Test.stopTest();
		System.assertEquals(0, EE_AS_IncidentNoteTriggerHandler.eventPublishedCount);
	}
}