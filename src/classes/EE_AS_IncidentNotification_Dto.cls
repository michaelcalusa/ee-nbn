public with sharing class EE_AS_IncidentNotification_Dto {
	
	public Notification notification;

	public Class Notification {
		public MessageHeader messageHeader;
    	public NotifyRequest notifyRequest;
	}

	public class MessageHeader {
	    public String activityName;
	    public String msgName;
	    public String businessServiceName;
	    public String businessServiceVersion;
	    public String msgType;
	    public String accessSeekerID;
	    public String correlationId;
	    public String security;
	    public String priority;
	    public String communicationPattern;
	    public String timestamp;
	    public String businessChannel;
	    public String businessProcessVersion;
	}

	public class NotifyRequest {
	    public String id;
	    public String type;
	    public String subType;
	    public String code;
	    public String message;
	    public String referenceId;
	    public Incident incident;
	    public String notificationType;
	}

	public class Incident {
	    public String id;
	    public String targetResolutionDate;
	    public String targetCommitmentDate;
	    public String impact;
	    public String severity;
	    public String priority;
	    public String resolution;
	    public String currentStatus;
	    public String statusReason;
	    public String closedDate;
	    public String resolvedDate;
	    public String plannedResolutionDate;
	    public String plannedExpiryDate;
	    public String plannedRemediationDate;
	    public String csg;
	    public String industryStatus;
	    public String industrySubStatus;
	    public String industryReasonCode;
	    public String industryReasonDescription;
	    public String industryResolutionCode;
	    public String industryResolutionDescription;
	    public String summary;
	    public String reportedSource;
	    public String type;
	    public String notificationEventTypes;
	    public String channelId;
	    public String incidentnotes;
	    public String category;
	    public String subCategory;
	    public String reportedDate;
	    public String appavailstartdatetime;
	    public String appavailenddatetime;
	    public String priorityAssist;
	    public String nbnproblemid;
	    public String nbnimpactedlocations;
	    public AccessSeeker accessSeeker;
	    public AccessSeekerInteraction accessSeekerInteraction;
	    
	    /*
	    public Appointment appointment;
	    public Assignee assignee;
	    public Product product;
	    public CategoryListContainer categoryListContainer;
	    public ServiceListContainer serviceListContainer;
	    public RequestDetailsContainer requestDetailsContainer;
	    public Order order;
	    public TestResults testResults;
	    public Place place;
	    public WorkInfoList workInfoList;
	    public String endUserEngagementType;
	    public IncidentTester incidentTester;
	    public GlobalResolution globalResolution;
	    public Service service;
	    public String submitter;
	    public String lastUpdatedBy;
	    public Owner Owner;
	    */
	}


	public class AccessSeekerContact {
	    public String firstName;
	    public String lastName;
	}

	public class AddressInfo {
	    public String state;
	    public String region;
	    public String addressSiteName;
	    public String localityName;
	}

	public class Address {
	    public AddressInfo address;
	}

	public class AccessSeeker {
	    public String id;
	    public String name;
	    public AccessSeekerContact accessSeekerContact;
	    public Address address;
	}

	public class AccessSeekerInteraction {
	    public String id;
	}

	public class Appointment {
	    public String id;
	}

	public class Assignee {
	    public String assignee;
	    public String assigneeLoginId;
	    public String assignedGroup;
	    public String assignedSupportCompany;
	    public String assignedSupportOrganization;
	}

	public class ProductInfo {
	    public String id;
	    public String type;
	}

	public class Product {
	    public ProductInfo product;
	}

	public class Category {
	    public String name;
	    public String value;
	}

	public class IncidentCategory {
	    public String categoryType;
	    public Category category;
	}

	public class IncidentCategoryList {
	    public List<IncidentCategory> incidentCategory;
	}

	public class CategoryListContainer {
	    public IncidentCategoryList incidentCategoryList;
	}

	public class ServiceList {
	    public List<String> id;
	}

	public class ServiceListContainer {
	    public ServiceList serviceList;
	}

	public class RequestDetail {
	    public String question;
	    public String answer;
	}

	public class RequestDetailsList {
	    public List<RequestDetail> requestDetail;
	}

	public class RequestDetailsContainer {
	    public RequestDetailsList requestDetailsList;
	}

	public class Order {
	    public String id;
	}

	public class TestResult {
		public String id;
		public String testType;
		public String testResult;
	}

	public class TestResults {
	    public TestResult testResult;
	}

	public class PlaceInfo {
	    public String id;
	    public String type;
	    public String address;
	}

	public class Place {
	    public PlaceInfo place;
	}

	public class Attachment {
	    public String id;
	    public String name;
	}

	public class AttachmentList {
	    public Attachment attachment;
	}

	public class WorkInfo {
	    public String workInfoid;
	    public String datee;
	    public String workInfoType;
	    public String notes;
	    public String providedBy;
	    public String source;
	    public String summary;
	    public AttachmentList attachmentList;
	    public String noteType;
	}

	public class WorkInfoList {
	    public List<WorkInfo> WorkInfo;
	}

	public class ContactMedium {
	    public String contactNumber;
	}

	public class IncidentTesterContactableVia {
	    public ContactMedium contactMedium;
	}

	public class IncidentTester {
	    public IncidentTesterContactableVia incidentTesterContactableVia;
	}

	public class GlobalResolutionOutcomes {
	    public String globalResolutionOutcomeId;
	    public String globalResolutionOutcome;
	}

	public class WorkfFlowDetails {
	    public String workFlowReferenceId;
	    public String CSCworkFlowResult;
	    public String deducedFaultType;
	    public String appointmentIndicator;
	    public String commitmentIndicator;
	}

	public class GlobalResolution {
	    public String id;
	    public String type;
	    public String result;
	    public String globalResolutionStatus;
	    public String globalResolutionDescription;
	    public GlobalResolutionOutcomes globalResolutionOutcomes;
	    public String remediationDate;
	    public String remediationFlag;
	    public String copperPathId;
	    public WorkfFlowDetails workfFlowDetails;
	}

	public class Service {
	    public String serviceNeverWorked;
	}

	public class Owner {
	    public String ownerGroup;
	}

}