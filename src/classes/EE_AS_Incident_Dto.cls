public with sharing class EE_AS_Incident_Dto {

	public String correlationId;
	public String typeOfService;
	public String dateIncidentRaised;
	public String summary;
	public String priId;
	public String accessSeekerId;
	public String channelId;
	public String severity;
	public String referenceId;
	public String testReferenceId;
	public String testType;
	public String ovcSelected;
	public List<String> cos;
	public String descriptionOfProblem;
	public String technicalContactName;
	public String technicalContactNumber;
	public String siteContactName;
	public String businessName;
	public String siteContactNumber;
	public String siteInductionEssentials;
	public String securityRequired;
	public String currentWhiteCard;
	public String securityClearance;
	public String siteAccessClearance;
	public String other;
	public String locationId;
	public String siteAvailability;
	public String appAvailStartDatetime;
	public String appAvailEndDatetime;
	public String additionalComments;
	public String businessProcessVersion;
	public String businessServiceVersion;
}