/**
    This DTO is used to :

    1. Update Service Incident received from Remedy in SF
    2. Create Network Incident received from Remedy in SF
    3. Create Service Alert received from Remedy in SF
    4. Create Change Request received from Remedy in SF
    5. Create Incident in Remedy from SF
    6. Update Incident in Remedy from SF
*/

public with sharing class EE_AS_Remedy_Dto {
    
    public String correlationId;
    public String accessSeekerId;
    public String referenceId;
    public String incidentId;
    public String changeRequestId;
    public String operation;
    public String incidentType;
    public IncidentData incidentData;
    public List<String> serviceListContainer;
    public List<WorkInfoContainer> workInfoContainer;
    public List<TestResult> testResults;
    public RequestDetailsContainer requestDetailsContainer;
    public List<String> impactedServices;
    public InterruptionWindows interruptionWindows;
    public ChangeRequestData changeRequestData;
    public List<CategoryListContainer> categoryListContainer;
    public List<EeasImpactedServiceLists> eeasImpactedServiceLists;
    public TransactionData transactionData;

    //for Update incident acknowledgement
    public String wlgId;
    public String statusCode;
    public String statusMessage;

    public class IncidentData {
        public String priId;
        public String summary;
        public String status;  
        public String impact; 
        public String severity;
        public String raisedBy;
        public String channelId;
        public String category;
        public String subCategory;
        public String reportedDate;
        public String closedDate;
        public String currentStatus;
        public String typeOfService;
        public String industryStatus;
        public String industrySubStatus;
        public String notificationType;
        public String targetResolutionDate;
        public String impactedLocation;
        public String notifyRequestType;
        public String notifyRequestSubType;
        public String descriptionOfProblem;
        public String businessProcessVersion;
        public String businessServiceVersion;
        public String accessSeekerConversationEvent;
        public String faultType;
        public String industryReasonCode;
        public String industryReasonDescription;
        public String industryResolutionCode;
        public String industryResolutionDescription;
        public String appAvailStartDatetime;
        public String appAvailEndDatetime;
    }

    public class ChangeRequestData {
        public String changeCategory;
        public String changeDuration;  
        public String createDate; 
        public String description;
        public String endDate;
        public String impactedLocation;
        public String startDate;
        public String status;
        public String type;
    }

    public class InterruptionWindows {
        public String interruption1;
        public String interruption2;
        public String interruption3;
        public String interruption4;
        public String interruption5;
    }

    public class WorkInfoContainer {    
        public String channelId;
        public String wlgId;
        public String type;
        public String summary;
        public String source;
        public String notes;
        public String providedBy;

    }

    public class RequestDetailsContainer {
        public String descriptionOfProblem;
        public String siteContactName;
        public String businessName;
        public String cos;
        public String siteContactNumber;
        public String securityClearance;
        public String securityRequired;
        public String siteInductionEssentials;
        public String technicalContactName;
        public String additionalComments;
        public String siteAccessClearance;
        public String siteAvailability;
        public String technicalContactNumber;
        public String currentWhiteCard;
        public String other;
        public String locationId;
    }

    public class TestResult {
        public String testReferenceId;
        public String testType;
    }

    public class CategoryListContainer {
        public String opCat1;
        public String opCat2;
        public String opCat3;
        public String prodCat1;
        public String prodCat2;
        public String prodCat3;
    }

    public class EeasImpactedServiceLists {
        public String ovcId;
        public String bpiId;
        public String nniId;
    }

    public class TransactionData {
        public String errorCode;
        public String errorMessage;
    }
}