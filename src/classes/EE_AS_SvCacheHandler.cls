public with sharing class EE_AS_SvCacheHandler extends AsyncQueueableHandler {
	public static final String HANDLER_NAME = 'EE_AS_SvCacheHandler';

    public EE_AS_SvCacheHandler() {
    	// No of params must be 1
        super(EE_AS_SvCacheHandler.HANDLER_NAME, 1);     
    }

    public override void executeWork(List<String> paramList) {    	
    	// Enforce max of 1 input parameter
    	if (paramList != null && paramList.size() == 1) {
    		invokeProcess(paramList[0]);
    	} else {
    		throw new CustomException(AsyncQueueableUtils.ERR_INVALID_PARAMS);
    	}
    }

    private void invokeProcess(String param) {
        EE_AS_SvCacheService.updateIncidentWithSvCacheDetails(param);
    }
}