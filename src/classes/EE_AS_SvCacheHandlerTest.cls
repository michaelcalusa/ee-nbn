@isTest
public class EE_AS_SvCacheHandlerTest {
	
	@isTest 
    static void test_EE_AS_SvCacheHandlerTestSuccess() {    	
    	/** Data setup **/
    	List<Async_Request__c> asyncReqList;
    	
    	String HANDLER_NAME = EE_AS_SvCacheHandler.HANDLER_NAME;
  		
        Incident_Management__c inc = new  Incident_Management__c();
        inc.Incident_Number__c = 'INC09988665545';
        inc.Access_Seeker_ID__c = 'ASI000000892811';
        inc.Correlation_Id__c = '3c11a419-c552-4d0d-aae4554534344343';
        inc.PRI_ID__c = 'BPI900000002986';
        Insert inc;

        // Dummy params
        String PARAM_1 = inc.Id;
        
		// Data Setup
		createASRCustomSettings(HANDLER_NAME);
		
		// Create asyncReq
		Async_Request__c asyncReq1 = DF_TestData.createAsyncRequest(HANDLER_NAME, 'Pending', PARAM_1, null, null, 0, 0);
		insert asyncReq1;  					

   		// Generate mock response
		String response = '{"Ack":"true"}';
 
		// Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
        
        test.startTest();
        
        EE_AS_SvCacheHandler handler = new EE_AS_SvCacheHandler();
        handler.run(null);
        handler.executeWork(new List<String>{PARAM_1});
        
        test.stopTest();
    }

    private static void createASRCustomSettings(String handler) {  
		Async_Request_Settings__c asrSettings = new Async_Request_Settings__c();
    	              
        try {	        
	        asrSettings.Name = handler;
	        asrSettings.Max_No_of_Rows__c = 1;
	        asrSettings.Max_No_of_Parallels__c = 50;
	        insert asrSettings;
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }  
    }    

	/** Data Creation **/
    @testSetup static void setup() {
    	try {
    		createASRConfigCustomSettings();  				        
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }     	
    }
    
    public static void createASRConfigCustomSettings() {                
        try {
	        Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
			asrConfigSettings.Max_No_of_Future_Calls__c = 50;
			asrConfigSettings.Max_No_of_Parallels__c = 10;
			asrConfigSettings.Max_No_of_Retries__c = 3;
			asrConfigSettings.Max_No_of_Rows__c = 1;			
	        upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }                
    }
}