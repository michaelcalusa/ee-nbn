public with sharing class EE_AS_SvCacheJob implements Queueable {
	
	List<String> incidents;
	
	public EE_AS_SvCacheJob(List<String> incidentList) {
		incidents = incidentList;
	}

	public void execute(QueueableContext context) {
		if(incidents != null) {
			for(String incidentId : incidents) {
				AsyncQueueableUtils.createRequests(EE_AS_SvCacheHandler.HANDLER_NAME, new List<String>{incidentId});		
			}
		}
	}
}