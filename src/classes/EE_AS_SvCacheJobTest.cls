@isTest
public class EE_AS_SvCacheJobTest {
	@isTest
    static void testExecuteMethod()
    {
    	List<String> toProcess = new List<String>();
    	Incident_Management__c inc = new  Incident_Management__c();
		inc.Incident_Number__c = 'INC09988665545';
		inc.Access_Seeker_ID__c = 'ASI000000892811';
		inc.Correlation_Id__c = '3c11a419-c552-4d0d-aae4554534344343';
		insert inc;
		toProcess.add(inc.Id);
    	
    	Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, '', null));
    	Test.startTest();
        Id jobId = System.enqueueJob(new EE_AS_SvCacheJob(toProcess));
        Test.stopTest();
    }
}