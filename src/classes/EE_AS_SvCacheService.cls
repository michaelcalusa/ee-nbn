public class EE_AS_SvCacheService {


	@future(callout=true)
    public static void updateIncidentWithSvCacheDetails(String incidentId) {
        System.debug('EE_AS_SvCacheService updateIncidentWithSvCacheDetails Async call START>>> ');
		final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;
        
        // Errors             
        final String ERROR_MESSAGE = 'A HTTP error occurred in class method EE_AS_SvCacheService.updateIncidentWithSvCacheDetails'; 

        Http http = new Http();
        HttpRequest req;       
        HTTPResponse res;
        String requestStr;
        String responseStr;
        String incidentNumber = '';
        String referenceId = '';
        String referenceInfo = '';
        String logMessage = '';
        String payload = '';
        String bpiId = '';

        DF_SvcCacheSearchResults response = new DF_SvcCacheSearchResults();
        List<Incident_Management__c> incidentsToUpdate = new List<Incident_Management__c>();

        try {
        	List<Incident_Management__c> incidents = [SELECT Id, PRI_ID__c, Incident_Number__c, serviceRestorationSLA__c, serviceRegion__c, locId__c FROM Incident_Management__c WHERE Id = :incidentId];
        	Incident_Management__c incident = null;
        	if(incidents != null && !incidents.isEmpty()) {
        		incident = incidents.get(0);
        	}
        	
            DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
                                                                Timeout__c
                                                         FROM   DF_Integration_Setting__mdt
                                                         WHERE  DeveloperName = :DF_SvcCacheServiceUtils.INTEGRATION_SETTING_NAME];

            NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
            TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
            
            system.debug('@@@ NAMED_CREDENTIAL: ' + NAMED_CREDENTIAL); 
            system.debug('@@@ TIMEOUT_LIMIT: ' + TIMEOUT_LIMIT);

            if(incident != null) {
                incidentNumber = incident.Incident_Number__c;
                bpiId = incident.PRI_ID__c;
                referenceId = incidentNumber;
                referenceInfo = 'BPI Id: ' + bpiId;
                payload = 'Incident Number ' + incidentNumber + '' + referenceInfo;
                END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL + '/' + bpiId;
                system.debug('@@@ END_POINT: ' + END_POINT);

                // Generate request 
                req = DF_AS_GlobalUtility.getHttpRequest(END_POINT, TIMEOUT_LIMIT);
                system.debug('@@@ req: ' + req); 

                // Send and get response               
                res = http.send(req);

                system.debug('@@@ Response Status Code: ' + res.getStatusCode());
                system.debug('@@@ Response Body: ' + res.getBody());
            
                if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {
                    responseStr = res.getBody();

                    if (String.isNotEmpty(responseStr)) {
                        // Get Response Body - Status
                        response = DF_SvcCacheServiceUtils.getServiceCacheResponse(responseStr);
                        if(response != null) {
                            incident.serviceRestorationSLA__c = response.sla;
                            incident.serviceRegion__c = response.enterpriseEthernetServiceLevelRegion;
                            incident.locId__c = response.locationId;
                            UPDATE incident;
                        }

                    } else {
                        logMessage = 'Response was empty from Service Cache';
                        GlobalUtility.logMessage(DF_Constants.ERROR, EE_AS_SvCacheService.class.getName(), 'searchServiceCache', referenceId, referenceInfo, logMessage, payload, null, 0);
                        AsyncQueueableUtils.retry(EE_AS_SvCacheHandler.HANDLER_NAME, new List<String>{incidentId}, ERROR_MESSAGE + '\n\n' + 'Response Status Code: ' + res.getStatusCode());
                    }
                } else {
                    logMessage = 'Response Code is ' + res.getStatusCode();
                    GlobalUtility.logMessage(DF_Constants.ERROR, EE_AS_SvCacheService.class.getName(), 'searchServiceCache', referenceId, referenceInfo, logMessage, payload, null, 0);
                    AsyncQueueableUtils.retry(EE_AS_SvCacheHandler.HANDLER_NAME, new List<String>{incidentId}, ERROR_MESSAGE + '\n\n' + 'Response Status Code: ' + res.getStatusCode());
                }
            } else {
                logMessage = 'Cannot find the incident';
                GlobalUtility.logMessage(DF_Constants.ERROR, EE_AS_SvCacheService.class.getName(), 'searchServiceCache', referenceId, referenceInfo, logMessage, payload, null, 0);
                AsyncQueueableUtils.retry(EE_AS_SvCacheHandler.HANDLER_NAME, new List<String>{incidentId}, ERROR_MESSAGE + '\n\n' + 'Response Status Code: ' + res.getStatusCode());
            }
            System.debug('EE_AS_SvCacheService updateIncidentWithSvCacheDetails END>>> ');
        } catch (Exception e) {
            logMessage = 'Exception occured while accessing Service Cache';
            GlobalUtility.logMessage(DF_Constants.ERROR, EE_AS_SvCacheService.class.getName(), 'searchServiceCache', referenceId, referenceInfo, logMessage, payload, e, 0);
            AsyncQueueableUtils.retry(EE_AS_SvCacheHandler.HANDLER_NAME, new List<String>{incidentId}, ERROR_MESSAGE + '\n\n' + 'Response Status Code: ' + res.getStatusCode());
        }
	}


    @future(callout=true)
    public static void updateIncidentWithSvCacheDetails(List<String> incidentIds) {
        System.debug('EE_AS_SvCacheService updateIncidentWithSvCacheDetails START>>> ');
        final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;
        
        // Errors             
        final String ERROR_MESSAGE = 'A HTTP error occurred in class method EE_AS_SvCacheService.updateIncidentWithSvCacheDetails'; 

        Http http = new Http();
        HttpRequest req;       
        HTTPResponse res;
        String requestStr;
        String responseStr;
        String incidentNumber = '';
        String referenceId = '';
        String referenceInfo = '';
        String logMessage = '';
        String payload = '';
        String bpiId = '';

        DF_SvcCacheSearchResults response = new DF_SvcCacheSearchResults();
        List<Incident_Management__c> incidentsToUpdate = new List<Incident_Management__c>();

        try {
            List<Incident_Management__c> incidents = [SELECT Id, PRI_ID__c, Incident_Number__c, serviceRestorationSLA__c, serviceRegion__c, locId__c FROM Incident_Management__c WHERE Id IN :incidentIds];
            DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
                                                                Timeout__c
                                                         FROM   DF_Integration_Setting__mdt
                                                         WHERE  DeveloperName = :DF_SvcCacheServiceUtils.INTEGRATION_SETTING_NAME];

            NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
            TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
            
            system.debug('@@@ NAMED_CREDENTIAL: ' + NAMED_CREDENTIAL); 
            system.debug('@@@ TIMEOUT_LIMIT: ' + TIMEOUT_LIMIT);

            for(Incident_Management__c incident : incidents) {
                incidentNumber = incident.Incident_Number__c;
                bpiId = incident.PRI_ID__c;
                referenceId = incidentNumber;
                referenceInfo = 'BPI Id: ' + bpiId;
                payload = 'Incident Number ' + incidentNumber + '' + referenceInfo;
                END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL + '/' + bpiId;
                system.debug('@@@ END_POINT: ' + END_POINT);

                // Generate request 
                req = DF_AS_GlobalUtility.getHttpRequest(END_POINT, TIMEOUT_LIMIT);
                system.debug('@@@ req: ' + req); 

                // Send and get response               
                res = http.send(req);

                system.debug('@@@ Response Status Code: ' + res.getStatusCode());
                system.debug('@@@ Response Body: ' + res.getBody());
            
                if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {                           
                    responseStr = res.getBody();

                    if (String.isNotEmpty(responseStr)) {
                        // Get Response Body - Status
                        response = DF_SvcCacheServiceUtils.getServiceCacheResponse(responseStr);
                        if(response != null) {
                            incident.serviceRestorationSLA__c = response.sla;
                            incident.serviceRegion__c = response.enterpriseEthernetServiceLevelRegion;
                            incident.locId__c = response.locationId;
                            UPDATE incident;
                        }

                    } else {
                        logMessage = 'Response was empty from Service Cache';
                        GlobalUtility.logMessage(DF_Constants.ERROR, EE_AS_SvCacheService.class.getName(), 'searchServiceCache', referenceId, referenceInfo, logMessage, payload, null, 0);
                    }
                } else {
                    logMessage = 'Response Code is ' + res.getStatusCode();
                    GlobalUtility.logMessage(DF_Constants.ERROR, EE_AS_SvCacheService.class.getName(), 'searchServiceCache', referenceId, referenceInfo, logMessage, payload, null, 0);
                }
            }
            System.debug('EE_AS_SvCacheService updateIncidentWithSvCacheDetails END>>> ');
        } catch (Exception e) {
            logMessage = 'Exception occured while accessing Service Cache';
            GlobalUtility.logMessage(DF_Constants.ERROR, EE_AS_SvCacheService.class.getName(), 'searchServiceCache', referenceId, referenceInfo, logMessage, payload, e, 0);
        }
    }
}