/**
 * Class for ee as tnd http callout mock.
 */
@isTest
public class EE_AS_TNDHttpCalloutMock implements HttpCalloutMock {

    protected Integer code;
    protected String status;
    protected String body;
    protected Map<String, String> responseHeaders;

    /**
     * Constructs the object.
     *
     * @param      code             The code
     * @param      status           The status
     * @param      body             The body
     * @param      responseHeaders  The response headers
     */
    public EE_AS_TNDHttpCalloutMock(Integer code, String status, String body, Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
    }

    /**
     * Send Callout Response
     *
     * @param      req   The request
     *
     * @return    returns  HTTPResponse obj
     */
    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        for (String key : this.responseHeaders.keySet()) {
            res.setHeader(key, this.responseHeaders.get(key));
        }
        res.setBody(this.body);
        res.setStatusCode(this.code);
        res.setStatus(this.status);
        return res;
    }

}