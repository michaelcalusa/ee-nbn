public with sharing class EE_AS_TND_Result_Dto {
    
    public String testName;
    public String nbnCorrelationId;
    public String subscriberId;
    public String timestamp;
    public String status;
    public String testId;
    public String result;
    public TestDetails testDetails;

    public class TestDetails {
        public List<Exceptions> exceptions;
        public TestAttributes testAttributes;
    }

    public class Exceptions {
        public String errorCode;
        public String errorMessage;
    }

    public class TestAttributes {

        //For Loopback and Link Trace
        public String PACKET_SIZE;
        public String ACCESS_SEEKER_ID;
        public String FROM_NODE_ID;
        public String NODEIDENNI;
        public String SVLANENNI;
        public String CVLANENNI;
        public String LAGIDENNI;
        public String NODEIDINNI;
        public String SVLANINNI;
        public String CVLANINNI;
        public String LAGIDINNI;
        public String MEP_TARGET_MAC_ADDRESS;
        public String MAINTENANCE_DOMAIN_ID;
        public String SVCID;
        public String BTDID;
        public String INNIACCESSINTERFACEOBJNAME;
        public List<CosResult> cosResults;
        //For LinkTrace
        public String LOGICAL_NAME_ENNI;
        public String PHYSICAL_NAME_ENNI;
        public String LOGICAL_NAME_INNI;
        public String PHYSICAL_NAME_INNI;
        public String AAS_LOGICAL_NAME;
        public String AAS_PHYSICAL_NAME;
        public String BNTD_LOGICAL_NAME;
        public String BNTD_PHYSICAL_NAME;
        //For BTD Status
        public String service_profile {get;set;} 
        public String inni_svlan {get;set;} 
        public String enni_svlan {get;set;} 
        public String btd_operational_state {get;set;} 
        public String btd_active_software_version {get;set;} 
        public String btd_last_change_time {get;set;} 
        //For AAS Port Status
        public String aas_port_admin_state {get;set;} 
        public String aas_port_operational_state {get;set;} 
        public String aas_port_link_state {get;set;} 
        public String aas_port_last_change_date {get;set;} 
        public String aas_card_operational_state {get;set;} 
        public String aas_card_admin_state {get;set;} 
        //For UNI_E Status
        public String ethernet_port_admin_state {get;set;} 
        public String ethernet_port_operational_state {get;set;} 
        public String ethernet_port_link_state {get;set;} 
        public String ethernet_port_last_change_date {get;set;} 

        //For NPT Statistical
        public List<CosResultsAll> cosResultsAll {get; set;}
        public CosResultsAveraged cosResultsAveraged {get; set;}
        public String routeType {get; set;}

        //For Check SAPs Test
        public String enni_ip_address; //ENNI Active
        public String enni_sv_lan;
        public String enni_cv_lan;
        public String enni_lag_id;
        public List<String> enni_config; // ENNI Configuration
        public String enni_service_cache_policer_egr; //ENNI Service Cache
        public String enni_sam_policer_egr; //ENNI SAM Detected
        public String enni_sam_policer_egr_flag;
        public String enni_service_cache_policer_igr; //Service Cache
        public String enni_sam_policer_igr; //ENNI SAM Detected
        public String enni_sam_policer_igr_flag;
        public String uni_ip_address;//UNI Active
        public String uni_sv_lan;
        public String uni_cv_lan;
        public String uni_lag_id;
        public List<String> uni_config; // UNI Configuration
        public String uni_service_cache_policer_egr; //UNI Service Cache
        public String uni_sam_policer_egr;//UNI SAM Detected
        public String uni_sam_policer_egr_flag;//UNI Access Policer EGR Result
        public String uni_service_cache_policer_igr; //UNI Service Cache
        public String uni_sam_policer_igr;//UNI SAM Detected
        public String uni_sam_policer_igr_flag;//UNI Access Policer IGR Result
        public String find_epipe_state_flag;
        public String active_enni_uni_verification_flag;
        public String show_serviceid_flag;


    }

    public class CosResult {
        public String PACKETS_SENT;
        public String PACKETS_RECEIVED;
        public String cosProfile;
        public String result;
        public String TRACES_SENT;
        public String TRACES_RECEIVED;
        public String BNTD_TO_AAS;
        public String AAS_TO_EAS;
        public String EAS_TO_EFS;
        public String AAS_TO_ECS;
        public String AAS_TO_ECS2;
        public String ECS2_TO_ECS1;
        public String EAS_TO_ECS;
        
        //Added for Octet Test
        public String cos;
        
        //changed by vishal
        public UpstreamDownstream upstream;
        public UpstreamDownstream downstream;
        public UpstreamDownstream upstream_cir;
        public UpstreamDownstream downstream_cir;
        public UpstreamDownstream upstream_eir;
        public UpstreamDownstream downstream_eir;
    }

    //Added for Octet Test
    public class OfferedOctets {
    
        public String t1;
        public String t2;
        public String delta;
    }
    
    public class DroppedOctets {
    
        public String t1;
        public String t2;
        public String delta;
    }
    
    public class UpstreamDownstream {
    
        public OfferedOctets offeredOctets;
        public OfferedOctets forwardedOctets;
        public DroppedOctets droppedOctets;
        //public OfferedOctets offered_inProfile_Octets;
        //public OfferedOctets offered_outProfile_Octets;
        //public OfferedOctets forwarded_inProfile_Octets;
        //public OfferedOctets forwarded_outProfile_Octets;
        //public DroppedOctets dropped_inProfile_Octets;
        //public DroppedOctets dropped_outProfile_Octets;
    }

    // NPT Statistical
    public class CosResultsAll {
        public String timeline;
        public String timestamp;
        public String average_frame_delay;
        public String average_inter_frame_delay_variation;
        public String average_frame_loss_ratio;

        //ONDemand NPT specific attributes
        public String frame_delay_min;
        public String frame_delay_max;
        public String inter_frame_delay_variation_min;
        public String inter_frame_delay_variation_max;
        public String frame_loss_ratio_min;
        public String frame_loss_ratio_max;
        public String frame_delay_bin_two_way_count_average;
        public String inter_frame_delay_variation_bin_two_way_count_average;
        public String frame_delay_suspectFlag;
        public String inter_frame_delay_variation_suspectFlag;
        public String frame_loss_ratio_suspectFlag;

        public List<BinStats> binStats {get; set;}
    }

    // NPT OnDemand
    public class BinStats {

        public String bin_timeline;
        public String frame_delay_bin_two_way_count;
        public String inter_frame_delay_variation_bin_two_way_count;
        public String frame_delay_bin_lower_bound;
        public String inter_frame_delay_variation_delayDmmBinLowerBound;
        public String frame_delay_bin_suspectFlag;
        public String inter_frame_delay_variation_bin_suspectFlag;

    }

    public class CosResultsAveraged {
        public NPTAveraged average_frame_delay;
        public NPTAveraged average_inter_frame_delay_variation;
        public NPTAveraged average_frame_loss_ratio;
    }

    public class NPTAveraged {
        public String value;
        public String threshold;
        public String indicator;
    }

}