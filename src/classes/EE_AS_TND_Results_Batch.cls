/*------------------------------------------------------------
Author:        Bharath Kumar
Company:       Di Data
Description:   Generic batch class for TnD_Result__c
Test Class:    EE_AS_TND_Results_BatchTest
------------------------------------------------------------*/ 
global class EE_AS_TND_Results_Batch implements Database.Batchable<sObject> {
	
	global final String query;
	
	global EE_AS_TND_Results_Batch(String q) {
		query = q;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<TND_Result__c> scope) {
   		System.debug('TnD Results scope >>> ' + scope.size());
		DELETE scope;
	}
	
	global void finish(Database.BatchableContext BC) {}
	
}