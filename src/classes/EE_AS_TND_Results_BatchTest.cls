/*------------------------------------------------------------
Author:        Bharath Kumar
Company:       Di Data
Description:   Test class for TnD_Result__c
------------------------------------------------------------*/ 

@isTest
private class EE_AS_TND_Results_BatchTest 
{

	/**
     * Setup Test Data
     */
    @testSetup static void setupTestData()
    {
    	List<TND_Result__c> tndRecs = new  List<TND_Result__c>();
        TND_Result__c tndResult = new  TND_Result__c();
        tndResult.Reference_Id__c = 'REF4567890';
        tndResult.Access_Technology__c = 'Enterprise Ethernet';
        tndResult.Channel__c = 'SALESFORCE';
        tndResult.Correlation_Id__c = '3c11a419-c552-4d0d-aae455454545454';
        tndResult.Req_Class_Of_Service__c = 'HIGH;MEDIUM';
        tndResult.Req_OVC_Id__c = 'OVC123456789012';
        tndResult.Req_Packet_Size__c = 65;
        tndResult.Requested_Date__c = Datetime.now();
        tndResult.Requested_By__c = UserInfo.getUserId();
        tndResult.Status__c = 'Pending';
        tndResult.Result__c = 'Fail';
        tndResult.Error_Internal__c = 'UNI-E Port Reset Unsuccessful';
        tndResult.Error_Code__c = 'UNI-E_RESET_FAILED';


        tndRecs.add(tndResult);

        TND_Result__c tndResult1 = new  TND_Result__c();
        tndResult1.Reference_Id__c = 'REF4567890565656';
        tndResult1.Test_Id__c = 'TST_0000277';
        tndResult1.Access_Technology__c = 'Enterprise Ethernet';
        tndResult1.Channel__c = 'SALESFORCE';
        tndResult1.Correlation_Id__c = '3c11a419-c552-4d0d-aae45545435353';
        tndResult1.Req_Class_Of_Service__c = 'HIGH;MEDIUM;LOW';
        tndResult1.Req_OVC_Id__c = 'OVC123456789033';
        tndResult1.CoS_High_Result__c = 'Pending';
        tndResult1.CoS_Medium_Result__c = 'Pending';
        tndResult1.CoS_Low_Result__c = 'Pending';
        tndResult1.Req_Packet_Size__c = 65;
        tndResult1.Requested_Date__c = Datetime.now();
        tndResult1.Requested_By__c = UserInfo.getUserId();
        tndResult1.Status__c = 'Pending';

        tndRecs.add(tndResult1);


        TND_Result__c tndResult2 = new  TND_Result__c();
        tndResult2.Reference_Id__c = 'REF4567890565688';
        tndResult2.Access_Technology__c = 'Enterprise Ethernet';
        tndResult2.Channel__c = 'SALESFORCE';
        tndResult2.Correlation_Id__c = '3c11a419-c552-4d0d-aae45545438888';
        tndResult2.Req_Class_Of_Service__c = 'HIGH;MEDIUM;LOW';
        tndResult2.Req_OVC_Id__c = 'OVC123456789033';
        tndResult2.Req_Packet_Size__c = 65;
        tndResult2.Requested_Date__c = Datetime.now();
        tndResult2.Requested_By__c = UserInfo.getUserId();
        tndResult2.Status__c = 'User Cancelled';

        tndRecs.add(tndResult2);

        Insert tndRecs;
    }
	
	@isTest static void testTnDResultsBatch() 
	{
		String query = 'SELECT Id FROM TND_Result__c';
		Test.startTest();
		EE_AS_TND_Results_Batch tndBatch = new EE_AS_TND_Results_Batch(query);
		DataBase.executeBatch(tndBatch);
		Test.stopTest();
	}

}