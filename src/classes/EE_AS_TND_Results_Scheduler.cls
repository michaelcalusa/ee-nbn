/*------------------------------------------------------------
Author:        Bharath Kumar
Company:       Di Data
Description:   Generic scheduler for TnD_Result__c
Test Class:    EE_AS_TND_Results_SchedulerTest
------------------------------------------------------------*/ 
global class EE_AS_TND_Results_Scheduler implements Schedulable {
	
	private static final string EE_AS_TND_RESULTS_PURGE_DAYS = 'EE_AS_TnD_Results_Purge_Days';
	private static final string ERROR = 'Error';
	private static final String ERROR_MESSAGE = EE_AS_TND_RESULTS_PURGE_DAYS + ' not configured in Custom Settings';

	global void execute(SchedulableContext sc) {
		String days = DF_AS_GlobalUtility.getDFAssuranceCustomSettingValue(EE_AS_TND_RESULTS_PURGE_DAYS);
		if(String.isNotEmpty(days))
		{
			System.debug('days >>> ' + days);
			String query = 'SELECT Id FROM TND_Result__c WHERE LastModifiedDate != LAST_N_DAYS:' + days;
			System.debug('query >>> ' + query);
			EE_AS_TND_Results_Batch tndResultsBatch = new EE_AS_TND_Results_Batch(query);
			Id batchProcessId = Database.ExecuteBatch(tndResultsBatch);
		}
		else 
		{
			GlobalUtility.logMessage(ERROR, DF_AS_GlobalUtility.class.getName(), 'sendEmail', null, null, ERROR_MESSAGE, null, null, 0);
		}
	}
}