@isTest
private class EE_AS_TND_To_SF_Trigger_Test {

    /**
     * Setup Test Data
     */
    @testSetup
    static void setupTestData()
    {
 
        List<TND_Result__c> tndRecs = new  List<TND_Result__c>();
        TND_Result__c tndResult = new  TND_Result__c();
        tndResult.Reference_Id__c = 'REF4567890';
        tndResult.Access_Technology__c = 'Enterprise Ethernet';
        tndResult.Channel__c = 'SALESFORCE';
        tndResult.Correlation_Id__c = '3c11a419-c552-4d0d-aae455454545454';
        tndResult.Req_Class_Of_Service__c = 'HIGH;MEDIUM';
        tndResult.Req_OVC_Id__c = 'OVC123456789012';
        tndResult.Req_Packet_Size__c = 65;
        tndResult.Requested_Date__c = Datetime.now();
        tndResult.Requested_By__c = UserInfo.getUserId();
        tndResult.Status__c = 'Pending';
        tndResult.Result__c = 'Fail';
        tndResult.Error_Internal__c = 'UNI-E Port Reset Unsuccessful';
        tndResult.Error_Code__c = 'UNI-E_RESET_FAILED';


        tndRecs.add(tndResult);

        TND_Result__c tndResult1 = new  TND_Result__c();
        tndResult1.Reference_Id__c = 'REF4567890565656';
        tndResult1.Test_Id__c = 'TST_0000277';
        tndResult1.Access_Technology__c = 'Enterprise Ethernet';
        tndResult1.Channel__c = 'SALESFORCE';
        tndResult1.Correlation_Id__c = '3c11a419-c552-4d0d-aae45545435353';
        tndResult1.Req_Class_Of_Service__c = 'HIGH;MEDIUM;LOW';
        tndResult1.Req_OVC_Id__c = 'OVC123456789033';
        tndResult1.CoS_High_Result__c = 'Pending';
        tndResult1.CoS_Medium_Result__c = 'Pending';
        tndResult1.CoS_Low_Result__c = 'Pending';
        tndResult1.Req_Packet_Size__c = 65;
        tndResult1.Requested_Date__c = Datetime.now();
        tndResult1.Requested_By__c = UserInfo.getUserId();
        tndResult1.Status__c = 'Pending';

        tndRecs.add(tndResult1);


        TND_Result__c tndResult2 = new  TND_Result__c();
        tndResult2.Reference_Id__c = 'REF4567890565688';
        tndResult2.Access_Technology__c = 'Enterprise Ethernet';
        tndResult2.Channel__c = 'SALESFORCE';
        tndResult2.Correlation_Id__c = '3c11a419-c552-4d0d-aae45545438888';
        tndResult2.Req_Class_Of_Service__c = 'HIGH;MEDIUM;LOW';
        tndResult2.Req_OVC_Id__c = 'OVC123456789033';
        tndResult2.Req_Packet_Size__c = 65;
        tndResult2.Requested_Date__c = Datetime.now();
        tndResult2.Requested_By__c = UserInfo.getUserId();
        tndResult2.Status__c = 'User Cancelled';

        tndRecs.add(tndResult2);

        ID statisticalRecTypeId = Schema.SObjectType.TND_Result__c.getRecordTypeInfosByName().get(EE_AS_TnDAPIUtils.TND_TEST_TYPE_NPT_STATISTICAL).getRecordTypeId();
        ID proactiveRecTypeId = Schema.SObjectType.TND_Result__c.getRecordTypeInfosByName().get(EE_AS_TnDAPIUtils.TND_TEST_TYPE_NPT_PROACTIVE).getRecordTypeId();
        TND_Result__c tndResult3 = new  TND_Result__c();
        tndResult3.Reference_Id__c = 'REF4567890565656';
        tndResult3.Test_Id__c = 'TST_0000555';
        tndResult3.Access_Technology__c = 'Enterprise Ethernet';
        tndResult3.Channel__c = 'SALESFORCE';
        tndResult3.Correlation_Id__c = '3c11a419-c552-4d0d-aae45545556677';
        tndResult3.Req_Class_Of_Service__c = 'HIGH';
        tndResult3.Req_OVC_Id__c = 'OVC000000000602';
        tndResult3.Req_Packet_Size__c = 65;
        tndResult3.Requested_Date__c = Datetime.now();
        tndResult3.Requested_By__c = UserInfo.getUserId();
        tndResult3.Status__c = 'Pending';
        tndResult3.recordTypeId=statisticalRecTypeId;

        tndRecs.add(tndResult3);

        TND_Result__c tndResult4 = new  TND_Result__c();
        tndResult4.Reference_Id__c = 'REF4567890565656';
        tndResult4.Test_Id__c = 'TST_0000444';
        tndResult4.Access_Technology__c = 'Enterprise Ethernet';
        tndResult4.Channel__c = 'SALESFORCE';
        tndResult4.Correlation_Id__c = '3c11a419-c552-4d0d-aae455678998';
        tndResult4.Req_Class_Of_Service__c = 'HIGH';
        tndResult4.Req_OVC_Id__c = 'OVC123456789033';
        tndResult4.Req_Packet_Size__c = 65;
        tndResult4.Req_MTU_Size__c = 1000;
        tndResult4.Requested_Date__c = Datetime.now();
        tndResult4.Requested_By__c = UserInfo.getUserId();
        tndResult4.Status__c = 'Pending';
        tndResult4.recordTypeId = proactiveRecTypeId;

        tndRecs.add(tndResult4);


        Insert tndRecs;

    }

    @isTest static void testEventLoopBackCosHighLow() {


        List<EE_AS_TND_To_SF__e> tndEvents = new List<EE_AS_TND_To_SF__e>();

        EE_AS_TND_To_SF__e event1 = new EE_AS_TND_To_SF__e();
        event1.InboundJSONMessage__c = '{"testName":"Loopback Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae455454545454","subscriberId":"","timestamp":"2018-06-05 05:31:59","status":"","testId":"","result":"","testDetails":{"exceptions":[{"message":""}],"testAttributes":{"PACKET_SIZE":"1200","ACCESS_SEEKER_ID":"ASI000000000035","FROM_NODE_ID":"10.128.3.228","NODEIDENNI":"123","SVLANENNI":"270","CVLANENNI":"3207","LAGIDENNI":"105","NODEIDINNI":"SWEAS0000117","SVLANINNI":"2007","CVLANINNI":"587","LAGIDINNI":"103","MEP_TARGET_MAC_ADDRESS":"0A:63:8C:C2:92:00","MAINTENANCE_DOMAIN_ID":"2","cosResults":[{"PACKETS_SENT":"5","PACKETS_RECEIVED":"5","cosProfile":"High","result":"Passed"}]}}}';
        tndEvents.add(event1);
        EE_AS_TND_To_SF__e event2 = new EE_AS_TND_To_SF__e();
        event2.InboundJSONMessage__c = '{"testName":"Loopback Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae455454545454","subscriberId":"","timestamp":"2018-06-05 05:31:59","status":"","testId":"","result":"","testDetails":{"exceptions":[{"message":""}],"testAttributes":{"PACKET_SIZE":"1200","ACCESS_SEEKER_ID":"ASI000000000035","FROM_NODE_ID":"10.128.3.228","NODEIDENNI":"123","SVLANENNI":"270","CVLANENNI":"3207","LAGIDENNI":"105","NODEIDINNI":"SWEAS0000117","SVLANINNI":"2007","CVLANINNI":"587","LAGIDINNI":"103","MEP_TARGET_MAC_ADDRESS":"0A:63:8C:C2:92:00","MAINTENANCE_DOMAIN_ID":"2","cosResults":[{"PACKETS_SENT":"5","PACKETS_RECEIVED":"5","cosProfile":"Low","result":"Passed"}]}}}';
        tndEvents.add(event2);
        
        Test.startTest();
        EventBus.publish(tndEvents);
        Test.stopTest();

        TND_Result__c tndResult =[select ID, CoS_High_Result__c, CoS_Low_Result__c from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae455454545454'];
        
        System.assertEquals('Passed', tndResult.CoS_High_Result__c);
        System.assertEquals('Passed', tndResult.CoS_Low_Result__c);

    }

    @isTest static void testEventLoopBackCosMedium() {

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"Loopback Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545435353","subscriberId":"","timestamp":"","status":"","testId":"","result":"","testDetails":{"exceptions":[{"errorMessage":""}],"testAttributes":{"PACKET_SIZE":"1200","ACCESS_SEEKER_ID":"ASI000000000035","FROM_NODE_ID":"10.128.3.228","NODEIDENNI":"123","SVLANENNI":"270","CVLANENNI":"3207","LAGIDENNI":"105","NODEIDINNI":"SWEAS0000117","SVLANINNI":"2007","CVLANINNI":"587","LAGIDINNI":"103","MEP_TARGET_MAC_ADDRESS":"0A:63:8C:C2:92:00","MAINTENANCE_DOMAIN_ID":"2","SVCID":"360","BTDID":"SWBTD0006909","INNIACCESSINTERFACEOBJNAME":"svc-mgr:service-188303:192.168.58.208","cosResults":[{"PACKETS_SENT":"5","PACKETS_RECEIVED":"5","cosProfile":"Medium","result":"Passed"}]}}}';
        
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        TND_Result__c tndResult =[select ID, CoS_Medium_Result__c, BTDID__c from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];
        System.assertEquals('Passed', tndResult.CoS_Medium_Result__c);
        System.assertEquals('SWBTD0006909', tndResult.BTDID__c);
    }

    @isTest static void testEventLoopBackUserCancelled() {

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"Loopback Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545438888","subscriberId":"","timestamp":"2018-06-05 05:31:59","status":"","testId":"","result":"","testDetails":{"exceptions":[{"message":""}],"testAttributes":{"PACKET_SIZE":"1200","ACCESS_SEEKER_ID":"ASI000000000035","FROM_NODE_ID":"10.128.3.228","NODEIDENNI":"123","SVLANENNI":"270","CVLANENNI":"3207","LAGIDENNI":"105","NODEIDINNI":"SWEAS0000117","SVLANINNI":"2007","CVLANINNI":"587","LAGIDINNI":"103","MEP_TARGET_MAC_ADDRESS":"0A:63:8C:C2:92:00","MAINTENANCE_DOMAIN_ID":"2","cosResults":[{"PACKETS_SENT":"5","PACKETS_RECEIVED":"5","cosProfile":"Low","result":"Passed"}]}}}';
        
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        TND_Result__c tndResult =[select ID, CoS_Medium_Result__c from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];
        System.assertNotEquals('Passed', tndResult.CoS_Medium_Result__c);
    }

    @isTest static void testEventLinkTraceCosLow() {

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"LinkTrace Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545435353","subscriberId":"","timestamp":"","status":"Complete","testId":"","result":"","testDetails":{"exceptions":[{"errorMessage":""}],"testAttributes":{"PACKET_SIZE":"1200","ACCESS_SEEKER_ID":"ASI000000000035","FROM_NODE_ID":"10.128.3.228","NODEIDENNI":"123","SVLANENNI":"270","CVLANENNI":"3207","LAGIDENNI":"105","LOGICAL_NAME_ENNI":"","PHYSICAL_NAME_ENNI":"","NODEIDINNI":"SWEAS0000117","SVLANINNI":"2007","CVLANINNI":"587","LAGIDINNI":"103","LOGICAL_NAME_INNI":"","PHYSICAL_NAME_INNI":"","AAS_LOGICAL_NAME":"","AAS_PHYSICAL_NAME":"","BNTD_LOGICAL_NAME":"","BNTD_PHYSICAL_NAME":"","MEP_TARGET_MAC_ADDRESS":"0A:63:8C:C2:92:00","MAINTENANCE_DOMAIN_ID":"2","cosResults":[{"TRACES_SENT":"5","TRACES_RECEIVED":"5","cosProfile":"Low","result":"Passed","BNTD_TO_AAS":"Green","AAS_TO_EAS":"Green","EAS_TO_EFS":"Green"}]}}}';
        
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        TND_Result__c tndResult =[select ID, CoS_Low_Result__c, CoS_Low_AAS_to_EAS__c from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];
        System.assertEquals('Passed', tndResult.CoS_Low_Result__c);
        System.assertEquals('Green', tndResult.CoS_Low_AAS_to_EAS__c);
    }

    @isTest static void testEventNegative(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"Loopback Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545435353","subscriberId":"","timestamp":"","status":"Cancelled","testId":"","result":"","testDetails":{"exceptions":[{"errorMessage":"Exception occured in MS"}],"testAttributes":{"PACKET_SIZE":"1200","ACCESS_SEEKER_ID":"ASI000000000035","FROM_NODE_ID":"10.128.3.228","NODEIDENNI":"123","SVLANENNI":"270","CVLANENNI":"3207","LAGIDENNI":"105","NODEIDINNI":"SWEAS0000117","SVLANINNI":"2007","CVLANINNI":"587","LAGIDINNI":"103","MEP_TARGET_MAC_ADDRESS":"0A:63:8C:C2:92:00","MAINTENANCE_DOMAIN_ID":"2","cosResults":[{"PACKETS_SENT":"5","PACKETS_RECEIVED":"5","cosProfile":"Low","result":"Passed"}]}}}';
        
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        TND_Result__c tndResult =[select ID, Error_Description__c from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];
        System.assert(TND_Result__c.Error_Description__c!=null);
    }

    @isTest static void testEventExceptionLog(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"Loopback Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545435353","subscriberId":"","timestamp":"2018-06-05T05:31:59Z","status":"Complete","testId":"","result":"","testDetails":{"exceptions":[{"message":""}],"testAttributes":{"PACKET_SIZE":"1200","ACCESS_SEEKER_ID":"ASI000000000035", "FROM_NODE_ID":"10.128.3.228","NODEIDENNI":"123","SVLANENNI":"270","CVLANENNI":"3207","LAGIDENNI":"105","NODEIDINNI":"SWEAS0000117","SVLANINNI":"2007","CVLANINNI":"587","LAGIDINNI":"103","MEP_TARGET_MAC_ADDRESS":"0A:63:8C:C2:92:00","MAINTENANCE_DOMAIN_ID":"2","cosResults":[{"PACKETS_SENT":"5","PACKETS_RECEIVED":"5","cosProfile":"Low","result":"Passed"}]}}}';
        
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        TND_Result__c tndResult =[select ID, Timestamp__c from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];
        System.assert(tndResult.Timestamp__c==null);
    }

    @isTest static void testEventUNIeStatus(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"UNI-E Status Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545435353","subscriberId":"","timestamp":"2018-06-05 05:31:59","status":"Complete","testId":"","result":"","testDetails":{"exceptions":[{"message":""}],"testAttributes":{"ACCESS_SEEKER_ID":"ASI000000000035","ethernet_port_admin_state":"","ethernet_port_operational_state":"","ethernet_port_link_state":"","ethernet_port_last_change_date":"2018-06-05 05:31:59"}}}';
        
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        TND_Result__c tndResult =[select ID, Ethernet_Port_Last_Change_Date__c from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];
        System.assert(tndResult.Ethernet_Port_Last_Change_Date__c!=null);
    }

    @isTest static void testEventBTDStatus(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"BTD Status Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545435353","subscriberId":"","timestamp":"2018-06-05 05:31:59","status":"Complete","testId":"","result":"","testDetails":{"exceptions":[{"message":""}],"testAttributes":{"ACCESS_SEEKER_ID":"ASI000000000035","service_Profile":"","inni_cvlan":"","enni_cvlan":"","btd_operational_state":"","btd_active_software_version":"","btd_last_change_time":"2018-06-05 05:31:59"}}}';
        
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        TND_Result__c tndResult =[select ID, BTD_System_Up_Time__c from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];
        System.assert(tndResult.BTD_System_Up_Time__c!=null);
    }
    

    @isTest static void testEventAASPortStatus(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"AAS Port Status Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545435353","subscriberId":"","timestamp":"2018-06-05 05:31:59","status":"Complete","testId":"","result":"","testDetails":{"exceptions":[{"message":""}],"testAttributes":{"ACCESS_SEEKER_ID":"ASI000000000035","aas_port_admin_state":"Yes","aas_port_operational_state":"","aas_port_link_state":"","aas_port_last_change_date":"2018-06-05 05:31:59","aas_card_operational_state":"Yes","aas_card_admin_state":""}}}';
        
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        TND_Result__c tndResult =[select ID, AAS_Port_Last_Change_Date__c from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];
        System.assert(tndResult.AAS_Port_Last_Change_Date__c!=null);
    }

    @isTest static void testEventUNIeResetPositive(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"UNI-E Reset Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545435353","subscriberId":"","timestamp":"","status":"Complete", "testId":"","result":"Passed","testDetails":{ "exceptions":[{ "errorCode": "","errorMessage": ""}],"testAttributes":{}}';
      
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        TND_Result__c tndResult =[select ID, LastModifiedDate, Result__c  from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];
        System.assert(tndResult.LastModifiedDate!=null);
        System.assert(tndResult.Result__c !='');
        
    }

    @isTest static void testEventUNIeResetError(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"UNI-E Port Reset","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545435353","subscriberId":"","timestamp":"","status":"Complete","testId":"","result":"Fail","testDetails":{"exceptions":[{"errorCode":"UNI-E_RESET_FAILED","errorMessage":""}],"testAttributes":{}}}';
      
        Test.startTest();
        EventBus.publish(event);
        Test.getEventBus().deliver();

        Test.stopTest();

        TND_Result__c tndResult =[select ID, Error_Code__c, Error_Internal__c, LastModifiedDate, Result__c  from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];
        system.debug('tndResult@resetError'+tndResult);
        System.assert(tndResult.LastModifiedDate!=null);
        System.assert(tndResult.Result__c !='');
        System.assert(tndResult.Error_Internal__c!='');
    }

   @isTest static void testEventOctetTestPositive(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{ "testName":"Octet Count Test", "nbnCorrelationId":"3c11a419-c552-4d0d-aae45545435353", "subscriberId":"subs1234", "timestamp":"", "status":"Complete", "testId":"test763472", "result":"test result", "testDetails":{ "exceptions":[ { "errorCode":"", "errorMessage" : "" } ], "testAttributes":{ "access_seeker_id" : "ASI000000000019", "cosResults" : [ { "cos" : "High", "upstream" : { "offeredOctets" :{ "t1" : "100", "t2" : "30", "delta" : "23" }, "forwardedOctets" : { "t1" : "234", "t2" : "342", "delta" : "23" }, "droppedOctets" : { "t1" :"234", "t2" : "322", "delta" : "223" } }, "downstream" : { "offeredOctets" :{ "t1" : "23", "t2" : "42", "delta" : "45" }, "forwardedOctets" : { "t1" : "76", "t2" : "543", "delta" : "34" }, "droppedOctets" : { "t1" :"21", "t2" : "213", "delta" : "23" } } }, { "cos" : "Medium", "upstream_cir" : { "offeredOctets" :{ "t1" : "231", "t2" : "23", "delta" : "232" }, "forwardedOctets" : { "t1" : "233", "t2" : "23", "delta" : "2" }, "droppedOctets" : { "t1" :"342", "t2" : "23", "delta" : "53" } }, "downstream_cir" : { "offeredOctets" :{ "t1" : "234", "t2" : "53", "delta" : "34" }, "forwardedOctets" : { "t1" : "324", "t2" : "32", "delta" : "42" }, "droppedOctets" : { "t1" :"45", "t2" : "46", "delta" : "23" } }, "upstream_eir" : { "offeredOctets" :{ "t1" : "234", "t2" : "34", "delta" : "4" }, "forwardedOctets" : { "t1" : "34", "t2" : "38", "delta" : "4" }, "droppedOctets" : { "t1" :"34", "t2" : "3", "delta" : "3" } }, "downstream_eir" : { "offeredOctets" :{ "t1" : "23", "t2" : "34", "delta" : "43" }, "forwardedOctets" : { "t1" : "23", "t2" : "213", "delta" : "123" }, "droppedOctets" : { "t1" :"78", "t2" : "56", "delta" : "54" } } }, { "cos" : "Low", "upstream" : { "offeredOctets" :{ "t1" : "34", "t2" : "54", "delta" : "3" }, "forwardedOctets" : { "t1" : "23", "t2" : "21", "delta" : "5" }, "droppedOctets" : { "t1" :"43", "t2" : "21", "delta" : "13" } }, "downstream" : { "offeredOctets" :{ "t1" : "56", "t2" : "54", "delta" : "3" }, "forwardedOctets" : { "t1" : "32", "t2" : "22", "delta" : "1" }, "droppedOctets" : { "t1" :"345", "t2" : "65", "delta" : "43" } } }] } } }';
      
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();
        TND_Result__c tndResult =[select ID, High_Ingress_Forwarded_Octets_T1__c from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];
        System.assertEquals('234',tndResult.High_Ingress_Forwarded_Octets_T1__c);
        
    }

    @isTest static void testEventOctetTestError(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"Octet Count Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545435353","subscriberId":"","timestamp":"","status":"Complete","testId":"","result":"Fail","testDetails":{"exceptions":[{"errorCode":"NO_ACTIVE_ENNI","errorMessage":""}],"testAttributes":{}}}';
      
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        TND_Result__c tndResult =[select ID, Error_Code__c, Error_Internal__c, LastModifiedDate, Result__c  from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];

        System.assert(tndResult.LastModifiedDate!=null);
        System.assert(tndResult.Result__c !='');
        System.assert(tndResult.Error_Internal__c!='');
    }

    @isTest static void testEventCheckSAPTestPositive(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"Check SAPs Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545435353","subscriberId":"","timestamp":"","status":"","testId":"","result":"Passed","testDetails":{"exceptions":[{"errorCode":"","errorMessage":""}],"testAttributes":{"access_seeker_id":"ASI000000000035","enni_ip_address":"192.168.8.134","enni_sv_lan":"1210","enni_cv_lan":"4095","enni_lag_id":"144","enni_config":["svc-mgr:service-71839:192.168.8.134:interface-lag_144-inner-tag-4095-outer-tag-1210 = serviceUp","svc-mgr:service-71839:192.168.8.135:interface-lag_144-inner-tag-4095-outer-tag-1210 = serviceDown"],"enni_service_cache_policer_egr":"21","enni_sam_policer_egr":"21","enni_sam_policer_egr_flag":"green","enni_service_cache_policer_igr":"21","enni_sam_policer_igr":"21","enni_sam_policer_igr_flag":"green","uni_ip_address":"192.176.8.154","uni_sv_lan":"","uni_cv_lan":"","uni_lag_id":"","uni_config":["NA","NA"],"uni_service_cache_policer_egr":"700","uni_sam_policer_egr":"700","uni_sam_policer_egr_flag":"green","uni_service_cache_policer_igr":"710","uni_sam_policer_igr":"710","uni_sam_policer_igr_flag":"green"}}}';
      
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        TND_Result__c tndResult =[select ID, UNI_SAM_Policer_IGR__c,ENNI_Service_Cache_Policer_EGR__c from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];
        
        System.assertEquals('710',tndResult.UNI_SAM_Policer_IGR__c);
        System.assertEquals('21',tndResult.ENNI_Service_Cache_Policer_EGR__c);
        
    }

    @isTest static void testEventCheckSAPTestError(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"Check SAPs Test","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545435353","subscriberId":"","timestamp":"","status":"Cancelled","testId":"","result":"Cancelled","testDetails":{"exceptions":[{"errorCode":"POLICER_MISMATCH","errorMessage":"Access Policy Mismatch"}],"testAttributes":{}}}';
      
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        TND_Result__c tndResult =[select ID, Error_Code__c, Error_Internal__c, LastModifiedDate, Result__c  from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];

        System.assert(tndResult.LastModifiedDate!=null);
        System.assert(tndResult.Result__c !='');
        System.assert(tndResult.Error_Internal__c!='');
    }

    @isTest static void testEventNPTStatisticalTestPositive(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"NPT – Statistical","nbnCorrelationId":"3c11a419-c552-4d0d-aae45545556677","subscriberId":"OVC000000000602","timestamp":"2018-08-01 07:37:08","testId":"WRI500000012198","testDetails":{"testAttributes":{"access_seeker_id":"ASI000000220810","routeType":"Local","cosResultsAll":[{"timeline":"t5","timestamp":"2018-08-01 16:05:00","average_frame_delay":"1","average_inter_frame_delay_variation":"1","average_frame_loss_ratio":"2"}],"cosResultsAveraged":{"average_inter_frame_delay_variation":{"value":"1.00","threshold":"1","indicator":"Green"},"average_frame_delay":{"value":"1.00","threshold":"1","indicator":"Green"},"average_frame_loss_ratio":{"value":"1.00","threshold":"1","indicator":"Green"}}}},"status":"Complete","result":"Passed"}';
      
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        EE_AS_NPT_Data__c nptData =[select ID, Timeline__c from EE_AS_NPT_Data__c where Test_Reference__r.Correlation_Id__c='3c11a419-c552-4d0d-aae45545556677'];
        
        System.assertEquals('t5',nptData.Timeline__c);

        
    }

    @isTest static void testEventNPTProactiveTestPositive(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"NPT - On Demand","nbnCorrelationId":"3c11a419-c552-4d0d-aae455678998","subscriberId":"OVC123456789033","timestamp":"2018-08-01 07:37:08","testId":"WRI500000012198","testDetails":{"testAttributes":{"access_seeker_id":"ASI000000220810","routeType":"Local","cosResultsAll":[{"timeline":"t5","timestamp":"2018-08-01 16:05:00","average_frame_delay":"1","average_inter_frame_delay_variation":"1","average_frame_loss_ratio":"2","frame_delay_min":"2","frame_delay_max":"2","inter_frame_delay_variation_min":"2","inter_frame_delay_variation_max":"2","frame_loss_ratio_min":"2","frame_loss_ratio_max":"2","frame_delay_bin_two_way_count_average":"2","inter_frame_delay_variation_bin_two_way_count_average":"2","binStats":[{"bin_timeline":"1","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"2","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"3","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"4","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"timeline":"5","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"6","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"7","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"8","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"9","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"10","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""}]},{"timeline":"t6","timestamp":"2018-08-01 16:10:00","average_frame_delay":"1","average_inter_frame_delay_variation":"1","average_frame_loss_ratio":"2","frame_delay_min":"2","frame_delay_max":"2","inter_frame_delay_variation_min":"2","inter_frame_delay_variation_max":"2","frame_loss_ratio_min":"2","frame_loss_ratio_max":"2","frame_delay_bin_two_way_count_average":"2","inter_frame_delay_variation_bin_two_way_count_average":"2","binStats":[{"bin_timeline":"1","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"2","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"3","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"4","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"timeline":"5","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"6","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"7","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"8","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"9","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"10","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""}]}],"cosResultsAveraged":{"average_inter_frame_delay_variation":{"value":"1.00","threshold":"1","indicator":"Green"},"average_frame_delay":{"value":"1.00","threshold":"1","indicator":"Green"},"average_frame_loss_ratio":{"value":"1.00","threshold":"1","indicator":"Green"}}}},"status":"Complete","result":"Passed"}';
      
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        EE_AS_NPT_Data__c nptData =[select ID, Delay_Bin_Two_Way_Count_Average__c from EE_AS_NPT_Data__c where Test_Reference__r.Correlation_Id__c='3c11a419-c552-4d0d-aae455678998' AND Timeline__c = 't5' AND Delay_Bin_Two_Way_Count_Average__c!=null];
        
        System.assertEquals(2,nptData.Delay_Bin_Two_Way_Count_Average__c);

        
    }

    @isTest static void testEventNPTProactiveTestNegative(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"NPT - On Demand","nbnCorrelationId":"3c11a419-c552-4d0d-aae455678998","subscriberId":"","timestamp":"","status":"Cancelled","testId":"","result":"Cancelled","testDetails":{"exceptions":[{"errorCode":"SNPM_COMM_FAIL","errorMessage":"SNPM Communication Failure"}],"testAttributes":{}}}';
      
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        List<EE_AS_NPT_Data__c> nptDataList =[select ID, Timeline__c from EE_AS_NPT_Data__c where Test_Reference__r.Correlation_Id__c='3c11a419-c552-4d0d-aae455678998'];
        

        TND_Result__c tndResult =[select ID, Error_Code__c, Error_Internal__c, LastModifiedDate, Result__c  from TND_Result__c where Correlation_Id__c='3c11a419-c552-4d0d-aae45545435353'];

        System.assertEquals(0,nptDataList.size());
        System.assert(tndResult.LastModifiedDate!=null);
        System.assert(tndResult.Result__c !='');
        System.assert(tndResult.Error_Internal__c!='');
       
    }

    @isTest static void testEventNPTProactiveTestSuspectFlag(){

        EE_AS_TND_To_SF__e event = new EE_AS_TND_To_SF__e();
        event.InboundJSONMessage__c = '{"testName":"NPT - On Demand","nbnCorrelationId":"3c11a419-c552-4d0d-aae455678998","subscriberId":"OVC123456789033","timestamp":"2018-08-01 07:37:08","testId":"WRI500000012198","testDetails":{"testAttributes":{"access_seeker_id":"ASI000000220810","routeType":"Local","cosResultsAll":[{"timeline":"t5","timestamp":"2018-08-01 16:05:00","average_frame_delay":"1","average_inter_frame_delay_variation":"1","average_frame_loss_ratio":"2","frame_delay_min":"2","frame_delay_max":"2","inter_frame_delay_variation_min":"2","inter_frame_delay_variation_max":"2","frame_loss_ratio_min":"2","frame_loss_ratio_max":"2","frame_delay_bin_two_way_count_average":"2","inter_frame_delay_variation_bin_two_way_count_average":"2","frame_delay_suspectFlag":"True", "binStats":[{"bin_timeline":"1","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"2","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"3","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"4","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"timeline":"5","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"6","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"7","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"8","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"9","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"10","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""}]},{"timeline":"t6","timestamp":"2018-08-01 16:10:00","average_frame_delay":"1","average_inter_frame_delay_variation":"1","average_frame_loss_ratio":"2","frame_delay_min":"2","frame_delay_max":"2","inter_frame_delay_variation_min":"2","inter_frame_delay_variation_max":"2","frame_loss_ratio_min":"2","frame_loss_ratio_max":"2","frame_delay_bin_two_way_count_average":"2","inter_frame_delay_variation_bin_two_way_count_average":"2","binStats":[{"bin_timeline":"1","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"2","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"3","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"4","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"timeline":"5","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"6","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"7","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"8","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"9","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""},{"bin_timeline":"10","frame_delay_bin_two_way_count":"","inter_frame_delay_variation_bin_two_way_count":"","bin_lower_bound":""}]}],"cosResultsAveraged":{"average_inter_frame_delay_variation":{"value":"1.00","threshold":"1","indicator":"Green"},"average_frame_delay":{"value":"1.00","threshold":"1","indicator":"Green"},"average_frame_loss_ratio":{"value":"1.00","threshold":"1","indicator":"Green"}}}},"status":"Complete","result":"Passed"}';
      
        Test.startTest();
        EventBus.publish(event);
        Test.stopTest();

        EE_AS_NPT_Data__c nptData =[select ID, One_Way_Frame_Delay_Suspect_Flag__c from EE_AS_NPT_Data__c where Test_Reference__r.Correlation_Id__c='3c11a419-c552-4d0d-aae455678998' AND Timeline__c = 't5' AND Delay_Bin_Two_Way_Count_Average__c!=null];
        
        System.assertEquals('True',nptData.One_Way_Frame_Delay_Suspect_Flag__c);

        
    }

}