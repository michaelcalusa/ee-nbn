/**
 * Class for TND API Service
 */
public with sharing class EE_AS_TnDAPIService
{

	public static String bpiId{get;set;}
	/**
	* Run Loopback Test
	*
	* @param      ovcId           The ovc identifier
	* @param      classOfService  The class of service
	* @param      packetSize      The packet size
	*/
	public static EE_AS_TNDResponseWrapper runLoopbackTest(string reqType, string ovcId, string[] classOfService, integer packetSize, string refID, String orderID)
	{
		string requestStr;
		String integrationSetting;

			// Serialise object for sending  
			Map<string, object> reqObj = new  Map<string, object>();
			reqObj.put('ovcId', ovcId);
			reqObj.put('classOfService', classOfService);
			reqObj.put('packetSize', packetSize);
			integrationSetting = EE_AS_TnDAPIUtils.LOOPBACK_TEST_SETTING_NAME;

		return runTest(reqObj, reqType, integrationSetting, refID, orderID);

	}


	/**
	 * Run LinkTrace Test
	 *
	 * @param      reqType         The request type
	 * @param      ovcId           The ovc identifier
	 * @param      classOfService  The class of service
	 * @param      packetSize      The packet size
	 * @param      refID           The reference id
	 *
	 * @return     EE_AS_TNDResponseWrapper
	 */
	public static EE_AS_TNDResponseWrapper runLinkTraceTest(string reqType, string ovcId, string[] classOfService, integer packetSize, string refID, String orderID)
	{
		string requestStr;
		String integrationSetting;

			// Serialise object for sending  
			Map<string, object> reqObj = new  Map<string, object>();
			reqObj.put('ovcId', ovcId);
			reqObj.put('classOfService', classOfService);
			reqObj.put('packetSize', packetSize);
			integrationSetting = EE_AS_TnDAPIUtils.LINKTRACE_TEST_SETTING_NAME;

		return runTest(reqObj, reqType, integrationSetting, refID, orderID);

	}

	/**
	 * run AAS Port Status Test
	 *
	 * @param      reqType         The request type
	 * @param      ovcId           The ovc identifier
	 * @param      classOfService  The class of service
	 * @param      packetSize      The packet size
	 * @param      refID           The reference id
	 *
	 * @return     EE_AS_TNDResponseWrapper
	 */
	public static EE_AS_TNDResponseWrapper runAASPortStatusTest(string reqType, string ovcId, string[] classOfService, integer packetSize, string refID, String orderID)
	{
		string requestStr;
		String integrationSetting;

			// Serialise object for sending  
			Map<string, object> reqObj = new  Map<string, object>();
			reqObj.put('ovcId', ovcId);
			integrationSetting = EE_AS_TnDAPIUtils.AAS_PORT_STATUS_TEST_SETTING_NAME;

		return runTest(reqObj, reqType, integrationSetting, refID, orderID);

	}

	/**
	 * run BTD Status Test
	 *
	 * @param      reqType         The request type
	 * @param      ovcId           The ovc identifier
	 * @param      classOfService  The class of service
	 * @param      packetSize      The packet size
	 * @param      refID           The reference id
	 *
	 * @return     EE_AS_TNDResponseWrapper
	 */
	public static EE_AS_TNDResponseWrapper runBTDStatusTest(string reqType, string ovcId, string[] classOfService, integer packetSize, string refID, String orderID)
	{
		string requestStr;
		String integrationSetting;

			// Serialise object for sending  
			Map<string, object> reqObj = new  Map<string, object>();
			reqObj.put('ovcId', ovcId);
			integrationSetting = EE_AS_TnDAPIUtils.BTD_STATUS_TEST_SETTING_NAME;

		return runTest(reqObj, reqType, integrationSetting, refID, orderID);

	}

	/**
	 * run Check SAPs Test
	 *
	 * @param      reqType         The request type
	 * @param      ovcId           The ovc identifier
	 * @param      classOfService  The class of service
	 * @param      packetSize      The packet size
	 * @param      refID           The reference id
	 *
	 * @return     EE_AS_TNDResponseWrapper
	 */
	public static EE_AS_TNDResponseWrapper runCheckSAPsTest(string reqType, string ovcId, string[] classOfService, integer packetSize, string refID, String orderID)
	{
		string requestStr;
		String integrationSetting;

			// Serialise object for sending  
			Map<string, object> reqObj = new  Map<string, object>();
			reqObj.put('ovcId', ovcId);
			integrationSetting = EE_AS_TnDAPIUtils.CHECK_SAPS_TEST_SETTING_NAME;

		return runTest(reqObj, reqType, integrationSetting, refID, orderID);

	}

	/**
	 * run NPT Statistical Test
	 *
	 * @param      reqType            The request type
	 * @param      ovcId              The ovc identifier
	 * @param      classOfService     The class of service
	 * @param      refID              The reference id
	 * @param      orderID            The order id
	 * @param      durationStartDate  The duration start date
	 * @param      durationEndDate    The duration end date
	 *
	 * @return     EE_AS_TNDResponseWrapper
	 */
	public static EE_AS_TNDResponseWrapper runNPTStatisticalTest(string reqType, string ovcId, string[] classOfService, string refID, String orderID, String durationStartDate, String durationEndDate)
	{
		string requestStr;
		String integrationSetting;
		String cos = (classOfService!=null && classOfService.size()>0)?classOfService[0]:'';
		System.debug('durationStartDate Before is: '+durationStartDate);
		durationStartDate = durationStartDate.replace('T',' ').replace('Z', '');
		durationStartDate = (DateTime.valueOfGMT(durationStartDate)).format('YYYY-MM-dd HH:mm:ss');
		System.debug('durationStartDate After is: '+durationStartDate);
		durationEndDate = durationEndDate.replace('T',' ').replace('Z', '');
		durationEndDate = (DateTime.valueOfGMT(durationEndDate)).format('YYYY-MM-dd HH:mm:ss');


			// Serialise object for sending  
			Map<string, object> reqObj = new  Map<string, object>();
			reqObj.put('ovcId', ovcId);
			reqObj.put('classOfService', cos);
			reqObj.put('startDateTime', durationStartDate);
			reqObj.put('endDateTime', durationEndDate);
			
			integrationSetting = EE_AS_TnDAPIUtils.NPT_STATISTICAL_TEST_SETTING_NAME;

		return runTest(reqObj, reqType, integrationSetting, refID, orderID);

	}

	/**
	 * run NPT Proactive Test
	 *
	 * @param      reqType            The request type
	 * @param      ovcId              The ovc identifier
	 * @param      classOfService     The class of service
	 * @param      packetSize         The packet size
	 * @param      refID              The reference id
	 * @param      orderID            The order id
	 * @param      durationStartDate  The duration start date
	 * @param      durationEndDate    The duration end date
	 *
	 * @return     EE_AS_TNDResponseWrapper
	 */
	public static EE_AS_TNDResponseWrapper runNPTProactiveTest(string reqType, string ovcId, string[] classOfService, integer packetSize, string refID, String orderID, String durationStartDate, String durationEndDate, integer mtuSize)
	{
		string requestStr;
		String integrationSetting;
		String cos = (classOfService!=null && classOfService.size()>0)?classOfService[0]:'';

		durationStartDate = durationStartDate.replace('T',' ').replace('Z', '');
		durationStartDate = (DateTime.valueOfGMT(durationStartDate)).format('YYYY-MM-dd HH:mm:ss');
		durationEndDate = durationEndDate.replace('T',' ').replace('Z', '');
		durationEndDate = (DateTime.valueOfGMT(durationEndDate)).format('YYYY-MM-dd HH:mm:ss');

			// Serialise object for sending  
			Map<string, object> reqObj = new  Map<string, object>();
			reqObj.put('ovcId', ovcId);
			reqObj.put('classOfService', cos);
			reqObj.put('startDateTime', durationStartDate);
			reqObj.put('endDateTime', durationEndDate);
			reqObj.put('packetSize', packetSize);
			reqObj.put('mtuSize', mtuSize);

			integrationSetting = EE_AS_TnDAPIUtils.NPT_PROACTIVE_TEST_SETTING_NAME;

		return runTest(reqObj, reqType, integrationSetting, refID, orderID);

	}


	/**
	 * run NPT Proactive Future Test
	 *
	 * @param      recId              The record identifier
	 * @param      Name               The name
	 * @param      ovcId              The ovc identifier
	 * @param      classOfService     The class of service
	 * @param      packetSize         The packet size
	 * @param      durationStartDate  The duration start date
	 * @param      durationEndDate    The duration end date
	 * @param      mtuSize            The mtu size
	 */
	@Future (Callout=true)
	public static void runNPTProactiveFutureTest(ID recId, String Name, string ovcId, List<String> classOfService, integer packetSize, Datetime durationStartDate, Datetime durationEndDate, integer mtuSize)
	{
		string requestStr;
		String integrationSetting;
		String correlationId;
		EE_AS_TNDResponseWrapper resWrapper = new EE_AS_TNDResponseWrapper();
		string responseStr;
		String cos = (classOfService!=null && classOfService.size()>0)?classOfService.get(0):'';
		HttpResponse res;
		string error;
		try{
		// Serialise object for sending  
		Map<string, object> reqObj = new  Map<string, object>();
		reqObj.put('ovcId', ovcId);
		reqObj.put('classOfService', cos);
		reqObj.put('startDateTime', durationStartDate.format('YYYY-MM-dd HH:mm:ss'));
		reqObj.put('endDateTime', durationEndDate.format('YYYY-MM-dd HH:mm:ss'));
		reqObj.put('packetSize', packetSize);
		reqObj.put('mtuSize', mtuSize);

		integrationSetting = EE_AS_TnDAPIUtils.NPT_PROACTIVE_TEST_SETTING_NAME;

		requestStr = JSON.serialize(reqObj);
		correlationId = DF_IntegrationUtils.generateGUID();

		//Make Callout
			res = EE_AS_TnDAPIUtils.callout(integrationSetting, requestStr, correlationId);
			System.debug('HTTP RES : '+res);
			responseStr = res!=null?res.getBody():'';
			
			System.debug('RES BODY IS'+res.getBody());

			resWrapper = (EE_AS_TNDResponseWrapper)JSON.deserialize(responseStr, EE_AS_TNDResponseWrapper.class);
			
			
			if(res!=null && res.getStatusCode() == (DF_Constants.HTTP_STATUS_200)){
			TND_Result__c tndResult = new TND_Result__c(Id=recId,Status__c = EE_AS_TnDAPIUtils.TND_PEND_STATUS);
			Update tndResult;
			}
			else if(res.getStatusCode() != null && string.valueOf(res.getStatusCode()).startsWith(EE_AS_TnDAPIUtils.HTTP_STATUS_STARTS_40))
			{
					string message = (resWrapper.errors != null && resWrapper.errors.size() > 0) ? resWrapper.errors.get(0).defaultMessage : '';
					message = String.isBlank(message)?(res.getStatusCode() +'-'+res.getStatus()): message;
					throw new  CustomException(message);
			}
			else
			{
				error = EE_AS_TnDAPIUtils.SERVER_ERROR;
				throw new  CustomException(error);
			}
		}
		catch(Exception e)
		{
			error = e.getMessage();
			TND_Result__c tndResult = new TND_Result__c(Id=recId,Status__c = EE_AS_TnDAPIUtils.TND_FAILED_STATUS,Error_Internal__c=error);
			Update tndResult;
			GlobalUtility.logMessage(EE_AS_TnDAPIUtils.ERROR, EE_AS_TnDAPIService.class.getName(), 'runTest: OnDemandNPT-Future' , ovcId, correlationId, e.getMessage(), requestStr, e, 0);
		}

	}

	/**
	 * run Octet Count Test
	 *
	 * @param      reqType         The request type
	 * @param      ovcId           The ovc identifier
	 * @param      classOfService  The class of service
	 * @param      packetSize      The packet size
	 * @param      refID           The reference id
	 *
	 * @return     EE_AS_TNDResponseWrapper
	 */
	public static EE_AS_TNDResponseWrapper runOctetCountTest(string reqType, string ovcId, string[] classOfService, integer packetSize, string refID, String orderID, String duration)
	{
		string requestStr;
		String integrationSetting;

			// Serialise object for sending  
			Map<string, object> reqObj = new  Map<string, object>();
			reqObj.put('ovcId', ovcId);
			reqObj.put('duration', duration);
			integrationSetting = EE_AS_TnDAPIUtils.OCTET_COUNT_TEST_SETTING_NAME;

		return runTest(reqObj, reqType, integrationSetting, refID, orderID);

	}

	/**
	 * run UNI e Port Reset Test
	 *
	 * @param      reqType         The request type
	 * @param      ovcId           The ovc identifier
	 * @param      classOfService  The class of service
	 * @param      packetSize      The packet size
	 * @param      refID           The reference id
	 *
	 * @return     EE_AS_TNDResponseWrapper
	 */
	public static EE_AS_TNDResponseWrapper runUNIePortResetTest(string reqType, string ovcId, string[] classOfService, integer packetSize, string refID, String orderID)
	{
		string requestStr;
		String integrationSetting;

			// Serialise object for sending  
			Map<string, object> reqObj = new  Map<string, object>();
			reqObj.put('ovcId', ovcId);
			integrationSetting = EE_AS_TnDAPIUtils.UNI_E_PORT_RESET_TEST_SETTING_NAME;

		return runTest(reqObj, reqType, integrationSetting, refID, orderID);

	}

	/**
	 * run UNI e Status Test
	 *
	 * @param      reqType         The request type
	 * @param      ovcId           The ovc identifier
	 * @param      classOfService  The class of service
	 * @param      packetSize      The packet size
	 * @param      refID           The reference id
	 *
	 * @return     EE_AS_TNDResponseWrapper
	 */
	public static EE_AS_TNDResponseWrapper runUNIeStatusTest(string reqType, string ovcId, string refID, String orderID)
	{
		string requestStr;
		String integrationSetting;

			// Serialise object for sending  
			Map<string, object> reqObj = new  Map<string, object>();
			reqObj.put('ovcId', ovcId);
			integrationSetting = EE_AS_TnDAPIUtils.UNI_E_STATUS_TEST_SETTING_NAME;

		return runTest(reqObj, reqType, integrationSetting, refID, orderID);

	}


	/**
	 * Cancel Test
	 *
	 * @param      ovcId           The ovc identifier
	 * @param      testType  	   The type of test
	 * @param      existingCorrelationId  	   The existing correlation Id
	 *
	 * @return     EE_AS_TNDResponseWrapper
	 */
	public static EE_AS_TNDResponseWrapper cancelTest(Id id, string ovcId, string testType, string existingCorrelationId)
	{
		String requestStr;
		string responseStr;
		String integrationSetting;
		HttpResponse res;
		string error;
		Map<string, object> reqObj = new  Map<string, object>();
		EE_AS_TNDResponseWrapper resWrapper = new EE_AS_TNDResponseWrapper();
		String correlationId;
		
		try 
		{
			Map<String, String> testTypeNameMap = new Map<String, String>();
			testTypeNameMap.put('Loop Back', 'Loopback Test');
			testTypeNameMap.put('Link Trace', 'LinkTrace Test');
			integrationSetting = EE_AS_TnDAPIUtils.CANCEL_TEST_SETTING_NAME;
			correlationId = DF_IntegrationUtils.generateGUID();
			System.debug('ovcId >>> ' + ovcId);
			System.debug('testType >>> ' + testTypeNameMap.get(testType));
			System.debug('existingCorrelationId >>> ' + existingCorrelationId);
			reqObj.put('ovcId', ovcId);
			reqObj.put('testType', testTypeNameMap.get(testType));
			reqObj.put('correlationId', existingCorrelationId);
			requestStr = JSON.serialize(reqObj);
			System.debug('********  requestStr>>>>     ' + requestStr);
			res = EE_AS_TnDAPIUtils.callout(integrationSetting, requestStr, correlationId);
			responseStr = res!=null?res.getBody():'';
			if(res.getStatusCode() == (DF_Constants.HTTP_STATUS_200))
			{
				TND_Result__c tndResult = new  TND_Result__c(Id = id);
				tndResult.Status__c = EE_AS_TnDAPIUtils.TND_USR_CNCLLD_STATUS;
				tndResult.Completion_Date__c = System.now();
				Update tndResult;
				GlobalUtility.logMessage(EE_AS_TnDAPIUtils.INFO, EE_AS_TnDAPIService.class.getName(), EE_AS_TnDAPIUtils.TND_TEST_TYPE_CANCEL, ovcId, correlationId, EE_AS_TnDAPIUtils.SUCCESS_RESP, requestStr, null, 0);
			} 
			else
			{
				resWrapper = (EE_AS_TNDResponseWrapper)JSON.deserialize(responseStr, EE_AS_TNDResponseWrapper.class);
				if(res.getStatusCode() != null && string.valueOf(res.getStatusCode()).startsWith(EE_AS_TnDAPIUtils.HTTP_STATUS_STARTS_40))
				{
					string message = (resWrapper.errors != null && resWrapper.errors.size() > 0) ? resWrapper.errors.get(0).defaultMessage : '';
					message = String.isBlank(message)?(res.getStatusCode() +'-'+res.getStatus()): message;
					throw new  CustomException(message);
				}
				else
				{
					error = EE_AS_TnDAPIUtils.SERVER_ERROR;
					throw new  CustomException(error);
				}
			}
		}
		catch(Exception e)
		{
			error = e.getMessage();
			System.debug(e.getStackTraceString());
			GlobalUtility.logMessage(EE_AS_TnDAPIUtils.ERROR, EE_AS_TnDAPIService.class.getName(), EE_AS_TnDAPIUtils.TND_TEST_TYPE_CANCEL, ovcId, correlationId, e.getMessage(), requestStr, e, 0);
		}
		finally
		{
			if(error != null && (resWrapper.errors == null || (resWrapper.errors.size() == 1 && String.isBlank(resWrapper.errors[0].defaultMessage))))
			{
				EE_AS_TNDResponseWrapper.errors errorObj = new  EE_AS_TNDResponseWrapper.errors();
				errorObj.defaultMessage = error;
				errorObj.field='';
				resWrapper.errors = new  List<EE_AS_TNDResponseWrapper.errors>();
				resWrapper.errors.add(errorObj);
				resWrapper.exceptionMsg='';
				resWrapper.error=error;
				resWrapper.status=500;
			}
		}
		return resWrapper;
	}


	/**
	 * Fetches a result for npt proactive.
	 *
	 * @param      testRecID              The test record id
	 * @param      existingCorrelationId  The existing correlation identifier
	 *
	 * @return     Response Wrapper
	 */
	public static EE_AS_TNDResponseWrapper fetchResultForNPTProactive(Id testRecID, string existingCorrelationId)
	{
		String requestStr;
		string responseStr;
		String integrationSetting;
		HttpResponse res;
		string error;
		Map<string, object> reqObj = new  Map<string, object>();
		EE_AS_TNDResponseWrapper resWrapper = new EE_AS_TNDResponseWrapper();
		String correlationId;
		
		try 
		{
			integrationSetting = EE_AS_TnDAPIUtils.FETCH_NPT_PROACTIVE_TEST_SETTING_NAME;
			correlationId = DF_IntegrationUtils.generateGUID();
			reqObj.put('correlationId', existingCorrelationId);
			requestStr = JSON.serialize(reqObj);
			res = EE_AS_TnDAPIUtils.callout(integrationSetting, requestStr, correlationId);
			responseStr = res!=null?res.getBody():'';
			if(res.getStatusCode() == (DF_Constants.HTTP_STATUS_200))
			{
				GlobalUtility.logMessage(EE_AS_TnDAPIUtils.INFO, EE_AS_TnDAPIService.class.getName(), EE_AS_TnDAPIUtils.TND_TEST_TYPE_FETCH_RESULT, testRecID, correlationId, EE_AS_TnDAPIUtils.SUCCESS_RESP, requestStr, null, 0);
			} 
			else
			{
				resWrapper = (EE_AS_TNDResponseWrapper)JSON.deserialize(responseStr, EE_AS_TNDResponseWrapper.class);
				if(res.getStatusCode() != null && string.valueOf(res.getStatusCode()).startsWith(EE_AS_TnDAPIUtils.HTTP_STATUS_STARTS_40))
				{
					string message = (resWrapper.errors != null && resWrapper.errors.size() > 0) ? resWrapper.errors.get(0).defaultMessage : '';
					message = String.isBlank(message)?(res.getStatusCode() +'-'+res.getStatus()): message;
					throw new  CustomException(message);
				}
				else
				{
					error = EE_AS_TnDAPIUtils.SERVER_ERROR;
					throw new  CustomException(error);
				}
			}
		}
		catch(Exception e)
		{
			error = e.getMessage();
			System.debug(e.getStackTraceString());
			GlobalUtility.logMessage(EE_AS_TnDAPIUtils.ERROR, EE_AS_TnDAPIService.class.getName(), EE_AS_TnDAPIUtils.TND_TEST_TYPE_FETCH_RESULT, testRecID, correlationId, e.getMessage(), requestStr, e, 0);
		}
		finally
		{
			if(error != null && (resWrapper.errors == null || (resWrapper.errors.size() == 1 && String.isBlank(resWrapper.errors[0].defaultMessage))))
			{
				EE_AS_TNDResponseWrapper.errors errorObj = new  EE_AS_TNDResponseWrapper.errors();
				errorObj.defaultMessage = error;
				errorObj.field='';
				resWrapper.errors = new  List<EE_AS_TNDResponseWrapper.errors>();
				resWrapper.errors.add(errorObj);
				resWrapper.exceptionMsg='';
				resWrapper.error=error;
				resWrapper.status=500;
			}
		}
		return resWrapper;
	}

	
	/**
	 * Cancel a Scheduled npt proactive test.
	 *
	 * @param      testRecID              The test record id
	 * @param      existingCorrelationId  The existing correlation identifier
	 *
	 * @return     EE_AS_TNDResponseWrapper
	 */
	public static EE_AS_TNDResponseWrapper cancelScheduledNPTProactiveTest(Id testRecID, string scheduledJobId)
	{
		string error;
		EE_AS_TNDResponseWrapper resWrapper = new EE_AS_TNDResponseWrapper();

		
		try 
		{
			//abort the scheduled job
			system.abortJob(scheduledJobId);
			//Change status to user cancelled
			TND_Result__c tndResult = new  TND_Result__c(Id = testRecID);
			tndResult.Status__c = EE_AS_TnDAPIUtils.TND_USR_CNCLLD_STATUS;
			tndResult.Completion_Date__c = System.now();
			Update tndResult;

		}
		catch(Exception e)
		{
			error = e.getMessage();
			System.debug(e.getStackTraceString());
			GlobalUtility.logMessage(EE_AS_TnDAPIUtils.ERROR, EE_AS_TnDAPIService.class.getName(), EE_AS_TnDAPIUtils.TND_TEST_TYPE_STOP_TEST, testRecID, scheduledJobId, e.getMessage(), null, e, 0);
		}
		finally
		{
			if(error != null )
			{
				EE_AS_TNDResponseWrapper.errors errorObj = new  EE_AS_TNDResponseWrapper.errors();
				errorObj.defaultMessage = error;
				errorObj.field='';
				resWrapper.errors = new  List<EE_AS_TNDResponseWrapper.errors>();
				resWrapper.errors.add(errorObj);
				resWrapper.exceptionMsg='';
				resWrapper.error=error;
				resWrapper.status=500;
			}
		}
		return resWrapper;
	}

	/**
	 * Stops a npt proactive test.
	 *
	 * @param      testRecID              The test record id
	 * @param      existingCorrelationId  The existing correlation identifier
	 *
	 * @return     EE_AS_TNDResponseWrapper
	 */
	public static EE_AS_TNDResponseWrapper stopNPTProactiveTest(Id testRecID, string existingCorrelationId)
	{
		String requestStr;
		string responseStr;
		String integrationSetting;
		HttpResponse res;
		string error;
		Map<string, object> reqObj = new  Map<string, object>();
		EE_AS_TNDResponseWrapper resWrapper = new EE_AS_TNDResponseWrapper();
		String correlationId;

		String durationEndDate = (System.now()).format('YYYY-MM-dd HH:mm:ss');
		
		try 
		{


			integrationSetting = EE_AS_TnDAPIUtils.STOP_NPT_PROACTIVE_TEST_SETTING_NAME;
			correlationId = DF_IntegrationUtils.generateGUID();
			reqObj.put('correlationId', existingCorrelationId);
			reqObj.put('endDateTime', durationEndDate);
			requestStr = JSON.serialize(reqObj);
			res = EE_AS_TnDAPIUtils.callout(integrationSetting, requestStr, correlationId);
			responseStr = res!=null?res.getBody():'';
			if(res.getStatusCode() == (DF_Constants.HTTP_STATUS_200))
			{
				GlobalUtility.logMessage(EE_AS_TnDAPIUtils.INFO, EE_AS_TnDAPIService.class.getName(), EE_AS_TnDAPIUtils.TND_TEST_TYPE_STOP_TEST, testRecID, correlationId, EE_AS_TnDAPIUtils.SUCCESS_RESP, requestStr, null, 0);
			} 
			else
			{
				
				if(res.getStatusCode() != null && string.valueOf(res.getStatusCode()).startsWith(EE_AS_TnDAPIUtils.HTTP_STATUS_STARTS_40))
				{
					if(String.isNotBlank(responseStr)){
					resWrapper = (EE_AS_TNDResponseWrapper)JSON.deserialize(responseStr, EE_AS_TNDResponseWrapper.class);
					}
					string message = (resWrapper.errors != null && resWrapper.errors.size() > 0) ? resWrapper.errors.get(0).defaultMessage : '';
					message = String.isBlank(message)?(res.getStatusCode() +'-'+res.getStatus()): message;
					throw new  CustomException(message);
				}
				else
				{
					error = EE_AS_TnDAPIUtils.SERVER_ERROR;
					throw new  CustomException(error);
				}
			}
		}
		catch(Exception e)
		{
			error = e.getMessage();
			System.debug(e.getStackTraceString());
			GlobalUtility.logMessage(EE_AS_TnDAPIUtils.ERROR, EE_AS_TnDAPIService.class.getName(), EE_AS_TnDAPIUtils.TND_TEST_TYPE_STOP_TEST, testRecID, correlationId, e.getMessage(), requestStr, e, 0);
		}
		finally
		{
			if(error != null && (resWrapper.errors == null || (resWrapper.errors.size() == 1 && String.isBlank(resWrapper.errors[0].defaultMessage))))
			{
				EE_AS_TNDResponseWrapper.errors errorObj = new  EE_AS_TNDResponseWrapper.errors();
				errorObj.defaultMessage = error;
				errorObj.field='';
				resWrapper.errors = new  List<EE_AS_TNDResponseWrapper.errors>();
				resWrapper.errors.add(errorObj);
				resWrapper.exceptionMsg='';
				resWrapper.error=error;
				resWrapper.status=500;
			}
		}
		return resWrapper;
	}

	/**
	 * run Test for all Tests
	 *
	 * @param      reqObj              The request object
	 * @param      reqType             The request type
	 * @param      integrationSetting  The integration setting
	 * @param      refId               The reference identifier
	 *
	 * @return     EE_AS_TNDResponseWrapper
	 */
	public static EE_AS_TNDResponseWrapper runTest(Map<string, object> reqObj, string reqType, String integrationSetting, string refID, String orderID)
	{

		string responseStr;
		Boolean isFuture;
		string requestStr;
		HttpResponse res;
		string responseBodyStatus;
		string correlationId;
		EE_AS_TNDResponseWrapper resWrapper = new EE_AS_TNDResponseWrapper();
		string error;
		Set<String> cosSet =new Set<String>();
		String[] classOfService = new String[]{};
		String ovcId ='';
		Integer packetSize ;
		Integer mtuSize ;
		Datetime endDateTime ;
		Datetime startDateTime ;



		try
		{
			requestStr = JSON.serialize(reqObj);
			correlationId = DF_IntegrationUtils.generateGUID();
			if(reqObj.get('classOfService')!=null){
			try{
			classOfService = (string[])reqObj.get('classOfService');
			}catch(System.TypeException te){
			classOfService = new String[]{(string)reqObj.get('classOfService')};
			}

			cosSet.addAll(classOfService);
			}
			if(reqObj.get('ovcId')!=null){
			ovcId= (string)reqObj.get('ovcId');
			}
			if(reqObj.get('packetSize')!=null){
			packetSize = Integer.valueOf(reqObj.get('packetSize'));
			}

			if(reqObj.get('mtuSize')!=null){
			mtuSize = Integer.valueOf(reqObj.get('mtuSize'));
			}

			if(reqObj.get('startDateTime')!=null){
			String startDate = (string)reqObj.get('startDateTime');
			startDateTime = DateTime.valueOf(startDate);
			}

			if(reqObj.get('endDateTime')!=null){
			String endDate = (string)reqObj.get('endDateTime');
			endDateTime = DateTime.valueOf(endDate);
			}
			
			//Find if the test to be run in the future
			if((startDateTime==null || startDateTime <= System.now())){
			isFuture = false;
			}else{
			isFuture = true;
			}

			if(!isFuture){
			//Make Callout
			res = EE_AS_TnDAPIUtils.callout(integrationSetting, requestStr, correlationId);
			System.debug('HTTP RES : '+res);
			responseStr = res!=null?res.getBody():'';
			
			System.debug('RES BODY IS'+res.getBody());

			resWrapper = (EE_AS_TNDResponseWrapper)JSON.deserialize(responseStr, EE_AS_TNDResponseWrapper.class);
			}
			//Check the response and handle errors
			if((res!=null && res.getStatusCode() == (DF_Constants.HTTP_STATUS_200)) || isFuture)
			{

				Id tndRecTypeId = Schema.SObjectType.TND_Result__c.getRecordTypeInfosByName().get(reqType).getRecordTypeId();
				TND_Result__c tndResult = new  TND_Result__c();
				tndResult.Reference_Id__c = refId;
				tndResult.Access_Technology__c = EE_AS_TnDAPIUtils.TECH_TYPE_ENT_ETHERNET;
				tndResult.Channel__c = EE_AS_TnDAPIUtils.CHANNEL_SFDC;
				tndResult.Correlation_Id__c = correlationId;
				tndResult.Req_Class_Of_Service__c = string.join(classOfService, ';');
				tndResult.Req_NPT_Duration_Start_Time__c = startDateTime;
				tndResult.Req_NPT_Duration_End_Date_Time__c = endDateTime;
				tndResult.Req_Duration__c = calculateTestDuration(startDateTime, endDateTime);
				tndResult.Req_OVC_Id__c = ovcId;
				tndResult.Req_Packet_Size__c = packetSize ;
				tndResult.Req_MTU_Size__c = mtuSize ;
				tndResult.Requested_Date__c = Datetime.now();
				tndResult.Requested_By__c = UserInfo.getUserId();
				if(isFuture){
				tndResult.Status__c = EE_AS_TnDAPIUtils.TND_SCHED_STATUS;
				}else{
				tndResult.Status__c = EE_AS_TnDAPIUtils.TND_PEND_STATUS;
				}

				tndResult.Related_Order__c = orderID;
				tndResult.Error_Internal__c = EE_AS_TnDAPIUtils.TND_NA_STATUS;
				System.debug('EE_AS_TnDAPIService.bpiId >>> ' + EE_AS_TnDAPIService.bpiId);
				tndResult.BPI_Id__c = EE_AS_TnDAPIService.bpiId;
				tndResult.recordtypeid=tndRecTypeId;

				Boolean isNPT = reqType != null && (reqType.contains(EE_AS_TnDAPIUtils.TND_TEST_TYPE_NPT_STATISTICAL) ||reqType.contains(EE_AS_TnDAPIUtils.TND_TEST_TYPE_NPT_PROACTIVE));

				if(cosSet.contains(EE_AS_TnDAPIUtils.TND_COS_HIGH) && !isNPT ){
					tndResult.CoS_High_Result__c=EE_AS_TnDAPIUtils.TND_PEND_STATUS;
				}else{
					tndResult.CoS_High_Result__c=EE_AS_TnDAPIUtils.TND_NA_STATUS;
				}

				if(cosSet.contains(EE_AS_TnDAPIUtils.TND_COS_MEDIUM) && !isNPT ){
					tndResult.CoS_Medium_Result__c=EE_AS_TnDAPIUtils.TND_PEND_STATUS;
				}else{
					tndResult.CoS_Medium_Result__c=EE_AS_TnDAPIUtils.TND_NA_STATUS;
				}

				if(cosSet.contains(EE_AS_TnDAPIUtils.TND_COS_LOW)  && !isNPT ){
					tndResult.CoS_Low_Result__c=EE_AS_TnDAPIUtils.TND_PEND_STATUS;
				}else{
					tndResult.CoS_Low_Result__c=EE_AS_TnDAPIUtils.TND_NA_STATUS;
				}

				Insert tndResult;
				resWrapper.recordId = tndResult.Id;
				tndResult = [select Id, Name from TND_Result__c where ID = :tndResult.Id];
				resWrapper.recordName = tndResult.Name;

				GlobalUtility.logMessage(EE_AS_TnDAPIUtils.INFO, EE_AS_TnDAPIService.class.getName(), 'runTest: ' +reqType , ovcId, correlationId, EE_AS_TnDAPIUtils.SUCCESS_RESP, requestStr, null, 0);
				if(isFuture){
				//Schedule the Future Job for TND ONdemand Test
				string acron = startDateTime.second() +' '+(startDateTime.minute()+1)+' '+startDateTime.hour()+' '+startDateTime.day()+' '+startDateTime.month()+' ? '+startDateTime.year();
				System.debug('Cron Expression is:'+acron);
				String jobName = 'EE TnD Scheduler - '+tndResult.Name;//jobs must be uniquely named
				String jobId = system.schedule(jobName, acron, new EE_TnDFutureSchedule(tndResult.id));
				tndResult.Job_Id__c = jobId;
				update tndResult;
				}

			}
			else if(res.getStatusCode() != null && string.valueOf(res.getStatusCode()).startsWith(EE_AS_TnDAPIUtils.HTTP_STATUS_STARTS_40))
			{
					string message = (resWrapper.errors != null && resWrapper.errors.size() > 0) ? resWrapper.errors.get(0).defaultMessage : '';
					message = String.isBlank(message)?(res.getStatusCode() +'-'+res.getStatus()): message;
					throw new  CustomException(message);
			}
			else
			{
				error = EE_AS_TnDAPIUtils.SERVER_ERROR;
				throw new  CustomException(error);
			}
		}
		catch(Exception e)
		{
			error = e.getMessage();
			System.debug(e.getStackTraceString());
			GlobalUtility.logMessage(EE_AS_TnDAPIUtils.ERROR, EE_AS_TnDAPIService.class.getName(), 'runTest: ' +reqType, ovcId, correlationId, e.getMessage(), requestStr, e, 0);
		}
		finally
		{

			if(error != null && (resWrapper.errors == null || (resWrapper.errors.size() == 1 && String.isBlank(resWrapper.errors[0].defaultMessage))))
			{
				EE_AS_TNDResponseWrapper.errors errorObj = new  EE_AS_TNDResponseWrapper.errors();
				errorObj.defaultMessage = error;
				errorObj.field='';
				resWrapper.errors = new  List<EE_AS_TNDResponseWrapper.errors>();
				resWrapper.errors.add(errorObj);
				resWrapper.exceptionMsg='';
				resWrapper.error=error;
				resWrapper.status=500;
			}
		}

		return resWrapper;
	}

	/**
	 * Calculate difference between 2 times
	 *
	 * @param      startDateTime   Start Date Time
	 * @param      endDateTime     End Date Time
	 *
	 * @return     String
	 */
	public static String calculateTestDuration(DateTime startDateTime, DateTime endDateTime) {
		String duration = null;
		Long startDateTimeLng = null;
		Long endDateTimeLng = null;
		Long milliseconds = null;
		Long seconds = null;
		Long minutes = null;
		Long hours = null;
		String strMin = '';
		String strHr = '';

		if(startDateTime != null && endDateTime != null) {
			duration = '';
			startDateTimeLng = startDateTime.getTime();
			endDateTimeLng = endDateTime.getTime();
			milliseconds = endDateTimeLng - startDateTimeLng;
			if(milliseconds > 0) {
				seconds = milliseconds / 1000;
				if(seconds > 0) {
					minutes = seconds / 60;	
				}
				if(minutes > 0) {
					hours = minutes / 60;
					minutes = Math.mod(minutes, 60);
					strMin = (minutes > 1) ? minutes + EE_AS_TnDAPIUtils.STR_MINUTES : minutes + EE_AS_TnDAPIUtils.STR_MINUTE;
				}
				if(hours > 0) {
					strHr = (hours > 1) ? hours + EE_AS_TnDAPIUtils.STR_HOURS : hours + EE_AS_TnDAPIUtils.STR_HOUR;
				}
				if(String.isNotBlank(strHr)) {
					duration = strHr + EE_AS_TnDAPIUtils.STR_SPACE;
				} else {
					duration = EE_AS_TnDAPIUtils.DEFAULT_HOUR + EE_AS_TnDAPIUtils.STR_SPACE;
				}
				if(String.isNotBlank(strMin)) {
					duration += strMin;
				} else {
					duration += EE_AS_TnDAPIUtils.DEFAULT_MINUTE;
				}
			} else {
				duration = EE_AS_TnDAPIUtils.DEFAULT_DURATION;
			}
		}
		return duration;
	}
}