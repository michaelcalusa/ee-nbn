/**
 * Test Class for TND API Service.
 */
@isTest
private class EE_AS_TnDAPIService_Test {
	
	@isTest
	static void testCalculateDurationNull()
	{
		String expected = null;
        String actual = EE_AS_TnDAPIService.calculateTestDuration(null, null);
        System.assertEquals(expected, actual);
	}

	@isTest
	static void testCalculateDurationDefault()
	{
		Datetime startDateTime = DateTime.parse('10/09/2018 11:46 AM');
        Datetime endDateTime = DateTime.parse('10/09/2018 11:46 AM');
        String actual = EE_AS_TnDAPIService.calculateTestDuration(startDateTime, endDateTime);
        String expected = EE_AS_TnDAPIUtils.DEFAULT_DURATION;
        System.assertEquals(expected, actual);
	}

	@isTest
	static void testCalculateDurationPositive1()
	{
		Datetime startDateTime = DateTime.parse('08/09/2018 11:46 AM');
        Datetime endDateTime = DateTime.parse('09/09/2018 11:46 AM');
        String actual = EE_AS_TnDAPIService.calculateTestDuration(startDateTime, endDateTime);
        String expected = '24hours 0minute';
        System.assertEquals(expected, actual);
	}

	@isTest
	static void testCalculateDurationPositive2()
	{
		Datetime startDateTime = DateTime.parse('08/09/2018 11:46 AM');
        Datetime endDateTime = DateTime.parse('09/09/2018 11:45 AM');
        String actual = EE_AS_TnDAPIService.calculateTestDuration(startDateTime, endDateTime);
        String expected = '23hours 59minutes';
        System.assertEquals(expected, actual);
	}

	@isTest
	static void testCalculateDurationPositive3()
	{
		Datetime startDateTime = DateTime.parse('07/09/2018 11:46 AM');
        Datetime endDateTime = DateTime.parse('09/09/2018 11:45 AM');
        String actual = EE_AS_TnDAPIService.calculateTestDuration(startDateTime, endDateTime);
        String expected = '47hours 59minutes';
        System.assertEquals(expected, actual);
	}

}