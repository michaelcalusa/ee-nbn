/**
 * Class for TND Util
 */
public with sharing class EE_AS_TnDAPIUtils
{
	
	public static final string CORRELATION_ID = 'nbn-correlation-id';
	public static final string CHANNEL_SFDC = 'SALESFORCE';
	public static final string CHANNEL = 'CHANNEL';
	public static final string ERROR = 'Error';
	public static final string INFO = 'Info';
	public static final string SERVER_ERROR = 'Internal Server Error';
	public static final string SUCCESS_RESP = 'Success Response';
	public static final String HTTP_STATUS_STARTS_40 = '40';
	public static final String TECH_TYPE_ENT_ETHERNET = 'Enterprise Ethernet';
	public static final String TND_PEND_STATUS = 'Pending';
	public static final String TND_SCHED_STATUS = 'Scheduled';
	public static final String TND_FAILED_STATUS = 'Failed';
	public static final string TND_USR_CNCLLD_STATUS = 'User Cancelled';
	public static final String TND_NA_STATUS = 'NA';
	public static final String TND_COS_HIGH = 'HIGH';
	public static final String TND_COS_MEDIUM = 'MEDIUM';
	public static final String TND_COS_LOW = 'LOW';

	public static final String TND_TEST_TYPE_CANCEL = 'Cancel Test';

	public static final String TND_TEST_TYPE_FETCH_RESULT = 'Fetch Result';
	public static final String TND_TEST_TYPE_STOP_TEST = 'Stop Test';

	public static final String TND_TEST_TYPE_LOOPBACK = 'Loop Back';
	public static final String TND_TEST_TYPE_LINKTRACE = 'Link Trace';
	public static final String TND_TEST_TYPE_AAS_PORT_STATUS = 'AAS Port Status';
	public static final String TND_TEST_TYPE_BTD_STATUS = 'BTD Status';
	public static final String TND_TEST_TYPE_CHECK_SAPS = 'Check SAP\'s';
	public static final String TND_TEST_TYPE_NPT_STATISTICAL = 'NPT – Statistical';
	public static final String TND_TEST_TYPE_NPT_PROACTIVE = 'NPT - On Demand';
	public static final String TND_TEST_TYPE_OCTET_COUNT = 'Octet Count';
	public static final String TND_TEST_TYPE_UNI_E_PORT_RESET = 'UNI-E Port Reset';
	public static final String TND_TEST_TYPE_UNI_E_STATUS = 'UNI-E Status';

	public static final string CANCEL_TEST_SETTING_NAME = 'Cancel_Test_API';
	public static final string FETCH_NPT_PROACTIVE_TEST_SETTING_NAME = 'Fetch_NPT_Proactive_Test_API';
	public static final string STOP_NPT_PROACTIVE_TEST_SETTING_NAME = 'Stop_NPT_Proactive_Test_API';
	public static final string LOOPBACK_TEST_SETTING_NAME = 'Loopback_Test_API';
	public static final string LINKTRACE_TEST_SETTING_NAME = 'LinkTrace_Test_API';
	public static final string AAS_PORT_STATUS_TEST_SETTING_NAME = 'AAS_Port_Status_Test_API';
	public static final string BTD_STATUS_TEST_SETTING_NAME = 'BTD_Status_Test_API';
	public static final string CHECK_SAPS_TEST_SETTING_NAME = 'Check_SAPs_Test_API';
	public static final string NPT_STATISTICAL_TEST_SETTING_NAME = 'NPT_Statistical_Test_API';
	public static final string NPT_PROACTIVE_TEST_SETTING_NAME = 'NPT_Proactive_Test_API';
	public static final string OCTET_COUNT_TEST_SETTING_NAME = 'Octet_Count_Test_API';
	public static final string UNI_E_PORT_RESET_TEST_SETTING_NAME = 'UNI_E_Port_Reset_Test_API';
	public static final string UNI_E_STATUS_TEST_SETTING_NAME = 'UNI_E_Status_Test_API';
	public static final String NBN_ENV_OVRD = 'NBN-Environment-Override';
	public static final String NBN_ROLE = 'ROLE';
	public static final String USER_TYPE_STANDARD = 'Standard';
	public static final String NBN_ROLE_VAL_NOC = 'NOC';
	public static final String NBN_ROLE_VAL_RSP = 'RSP';

	public static final Integer TND_TIME_THRESHOLD = 30;
	public static final Integer TND_DAY_THRESHOLD = 30;

	public static string END_POINT;
	public static Integer TIMEOUT_LIMIT;
	public static string NAMED_CREDENTIAL;
	public static string CONTEXT_PATH;
	public static String NBN_ENV_OVRD_VAL;
	public static String NBN_ROLE_VAL;

	public static final String STR_HOURS = 'hours';
	public static final String STR_HOUR = 'hour';
	public static final String STR_MINUTES = 'minutes';
	public static final String STR_MINUTE = 'minute';
	public static final String STR_SPACE = ' ';
	public static final String DEFAULT_DURATION = '0hour 0minute';
	public static final String DEFAULT_HOUR = '0hour';
	public static final String DEFAULT_MINUTE = '0minute';


	/**
	 * Makes HTTP callout based on the provided input
	 *
	 * @param      integSettingName  The integ setting name
	 * @param      requestStr        The request string
	 * @param      correlationId     The correlation identifier
	 *
	 * @return     HttpResponse response
	 */
	public static HttpResponse callout(string integSettingName, string requestStr, String correlationId)
	{
		Http http = new  Http();
		HTTPResponse res;
		HttpRequest req;

			DF_Integration_Setting__mdt integrationSetting = EE_AS_TnDAPIUtils.getIntegrationSettings(integSettingName);
			String userType = UserInfo.getUserType();
			NAMED_CREDENTIAL = integrationSetting.Named_Credential__c;
			TIMEOUT_LIMIT = Integer.valueOf(integrationSetting.Timeout__c);
			CONTEXT_PATH = integrationSetting.Context_Path__c;
			NBN_ENV_OVRD_VAL = integrationSetting.DP_Environment_Override__c;

			if(USER_TYPE_STANDARD.equalsIgnoreCase(userType)){
				NBN_ROLE_VAL = NBN_ROLE_VAL_NOC;
			} else{
				NBN_ROLE_VAL = NBN_ROLE_VAL_RSP;
			}

			// Build endpoint
			END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL + CONTEXT_PATH;
			System.debug('END_POINT: ' + END_POINT);
			// Generate request 
			req = new  HttpRequest();

			//Set Endpoint, Method and Timeout    	
			req.setEndpoint(END_POINT);
			req.setMethod(DF_Constants.HTTP_METHOD_POST);
			req.setTimeout(TIMEOUT_LIMIT);
			// Set all headers
			req.setHeader(CORRELATION_ID, correlationId);
			req.setHeader(DF_Constants.CONTENT_TYPE, DF_Constants.CONTENT_TYPE_JSON);
			req.setHeader(DF_Constants.HTTP_HEADER_ACCEPT, DF_Constants.CONTENT_TYPE_JSON);
			req.setHeader(CHANNEL, CHANNEL_SFDC);
			if(NBN_ENV_OVRD_VAL!=null)
			req.setHeader(NBN_ENV_OVRD, NBN_ENV_OVRD_VAL);
			req.setHeader(NBN_ROLE, NBN_ROLE_VAL);
			//Set JSOON request body
			req.setBody(requestStr);

			System.debug('requestStr  >>>>>    ' + requestStr);
			System.debug('req body  >>>>>    ' + req.getBody());

			System.debug('req: ' + req);
			// Send and get response               
			res = http.send(req);

		return res;
	}


	/**
	* Gets the integration settings.
	*
	* @param      settingName  The setting name
	*
	* @return     The integration settings.
	*/
	public static DF_Integration_Setting__mdt getIntegrationSettings(string settingName)
	{
		DF_Integration_Setting__mdt integrSetting;

			integrSetting = [SELECT
				Named_Credential__c, 
				Timeout__c, 
				Context_Path__c,
				DP_Environment_Override__c
			FROM
				DF_Integration_Setting__mdt
			WHERE
				DeveloperName = : settingName];

		return integrSetting;
	}

	/**
	 * Gets the access seeker name.
	 *
	 * @param      seekerID  The seeker id
	 *
	 * @return     The access seeker name.
	 */
	public static Map<String,String> getAccessSeekerName(Set<String> seekerIdSet)
	{
		Map<String,String> accessSeekerMap = new Map<String,String>();

		List<Account>  accs = [SELECT 
				Name,
				Access_Seeker_ID__c
			FROM
				Account
			WHERE
			    recordtype.Name='Customer'
			AND	Access_Seeker_ID__c IN : seekerIdSet];

		if(accs!=null && accs.size()>0){
			for(Account acc:accs)
			{
				accessSeekerMap.put(acc.Access_Seeker_ID__c,acc.Name);
			}
		}

		return accessSeekerMap;
	}

	/**
	* Gets the tnd error custom metadata.
	*
	* @param      errorCodes  List of error codes
	*
	* @return     map of errorCode:Messages
	*/
	public static Map<String, String> getErrorConfiguration(List<String> errorCodes) {

        List<TND_Errors__mdt> tndErrors = [SELECT Error_Code__c, 
    									   Internal_Error_Message__c, 
    									   External_Error_Message__c 
    									   FROM TND_Errors__mdt 
        							       WHERE
        							       Error_Code__c IN : errorCodes];  
	
        Map<String, String> configuration = new Map<String, String>();
        if(tndErrors != null) {
        	for(TND_Errors__mdt tndError : tndErrors) {

        		String errorCode = tndError.Error_Code__c;
        		if(errorCode != null)
        			errorCode = errorCode.toUpperCase();

        		configuration.put(errorCode+'_INTERNAL', tndError.Internal_Error_Message__c);
        		configuration.put(errorCode+'_EXTERNAL', tndError.External_Error_Message__c);
        	}
        }

        System.debug('!@#$% Error Configuration : ' + configuration);
        return configuration;
    }
}