/**
* Controls the data flow into an TND Test and updates the view whenever data changes.
*/
public with sharing class EE_AS_TnDController
{
	public static final string PENDING = 'Pending';
	public static final string COMPLETE = 'Complete';
	public static final string CANCELLED = 'Cancelled';
	public static final string USER_STD_TYPE = 'Standard';
	public static final string STATUS_USER_CANCELLED = 'User Cancelled';

	/**
	* Run TNDTest
	*
	* @param      reqType         The request type
	* @param      ovcId           The ovc identifier
	* @param      classOfService  The class of service
	* @param      packetSize      The packet size
	* @param      refID           The reference id
	*
	* @return     String array of errors. If any errors
	*/
	@AuraEnabled
	public static EE_AS_TNDResponseWrapper runTndTest(string reqType, string ovcId, string[] classOfService, integer packetSize, string refID, string orderID, string duration, string durationStartDate, string durationEndDate, integer mtuSize, string productID)
	{
		string[] errorMsgs = new string[]{};
		EE_AS_TNDResponseWrapper respWrap;
		EE_AS_TnDAPIService.bpiId = productID;

		if(reqType.contains(EE_AS_TnDAPIUtils.TND_TEST_TYPE_LOOPBACK))
		{
			respWrap = EE_AS_TnDAPIService.runLoopbackTest(reqType, ovcId, classOfService, packetSize, refID, orderID);
		}
		else if(reqType.equalsIgnoreCase(EE_AS_TnDAPIUtils.TND_TEST_TYPE_LINKTRACE))
		{
			respWrap = EE_AS_TnDAPIService.runLinkTraceTest(reqType, ovcId, classOfService, packetSize, refID, orderID);
		}
		else if(reqType.equalsIgnoreCase(EE_AS_TnDAPIUtils.TND_TEST_TYPE_AAS_PORT_STATUS))
		{
			respWrap = EE_AS_TnDAPIService.runAASPortStatusTest(reqType, ovcId, classOfService, packetSize, refID, orderID);
		}
		else if(reqType.equalsIgnoreCase(EE_AS_TnDAPIUtils.TND_TEST_TYPE_BTD_STATUS))
		{
			respWrap = EE_AS_TnDAPIService.runBTDStatusTest(reqType, ovcId, classOfService, packetSize, refID, orderID);
		}
		else if(reqType.equalsIgnoreCase(EE_AS_TnDAPIUtils.TND_TEST_TYPE_CHECK_SAPS))
		{
			respWrap = EE_AS_TnDAPIService.runCheckSAPsTest(reqType, ovcId, classOfService, packetSize, refID, orderID);
		}
		else if(reqType.equalsIgnoreCase(EE_AS_TnDAPIUtils.TND_TEST_TYPE_NPT_STATISTICAL))
		{
			respWrap = EE_AS_TnDAPIService.runNPTStatisticalTest(reqType, ovcId, classOfService, refID, orderID, durationStartDate, durationEndDate);
		}
		else if(reqType.equalsIgnoreCase(EE_AS_TnDAPIUtils.TND_TEST_TYPE_NPT_PROACTIVE))
		{
			respWrap = EE_AS_TnDAPIService.runNPTProactiveTest(reqType, ovcId, classOfService, packetSize, refID, orderID, durationStartDate, durationEndDate, mtuSize);
		}
		else if(reqType.equalsIgnoreCase(EE_AS_TnDAPIUtils.TND_TEST_TYPE_OCTET_COUNT))
		{
			respWrap = EE_AS_TnDAPIService.runOctetCountTest(reqType, ovcId, classOfService, packetSize, refID, orderID, duration);
		}
		else if(reqType.equalsIgnoreCase(EE_AS_TnDAPIUtils.TND_TEST_TYPE_UNI_E_PORT_RESET))
		{
			respWrap = EE_AS_TnDAPIService.runUNIePortResetTest(reqType, ovcId, classOfService, packetSize, refID, orderID);
		}
		else if(reqType.equalsIgnoreCase(EE_AS_TnDAPIUtils.TND_TEST_TYPE_UNI_E_STATUS))
		{
			respWrap = EE_AS_TnDAPIService.runUNIeStatusTest(reqType, ovcId, refID, orderID);
		}
		
		return respWrap;
	}

	/**
	* Gets the pending test results for user.
	*
	* @return     The pending test results for user.
	*/
	@AuraEnabled
	public static List<TND_Result__c> getCurrentTestResultsForUser()
	{
		List<TND_Result__c> testRecords = new  List<TND_Result__c>();
		List<TND_Result__c> updateTNDRecords = new  List<TND_Result__c>();
		Map<String, Decimal> testTimeOutMap = new Map<String, Decimal>();
		Long createDate; Long now; Long milliseconds ; Decimal seconds;
		
		User loggedInUser = [SELECT
			AccountId
		FROM
			User
		WHERE
			id = : UserInfo.getUserId()
		Limit 1];

		testRecords = [SELECT
			Id, 
			Name, 
			Requested_Date__c, 
			Completion_Date__c, 
			Requested_By__r.Name, 
			Status__c, 
			recordType.Name, 
			Test_Id__c,
			Createddate, 
			Reference_Id__c, 
			CoS_High_Result__c, 
			CoS_Low_Result__c, 
			CoS_Medium_Result__c,
			Is_Cancelable__c,
			TimedOut__c
		FROM
			TND_Result__c
		WHERE
			(Status__c = : PENDING OR ((Status__c = : COMPLETE OR Status__c = : CANCELLED OR Status__c = : STATUS_USER_CANCELLED) AND Completion_Date__c >: Datetime.now().addMinutes(-EE_AS_TnDAPIUtils.TND_TIME_THRESHOLD))) AND createdby.UserType !=: USER_STD_TYPE AND createdby.UserType != null AND Related_Order__r.Opportunity_Bundle__r.Account__c = : loggedInUser.AccountId
		ORDER BY
			lastmodifieddate desc];
		for(TND_Test_TimeOut_Time__mdt tnd: [SELECT id, DeveloperName, MasterLabel, TimeOut_Time__c FROM TND_Test_TimeOut_Time__mdt LIMIT 100]){
			if(tnd.MasterLabel != null & tnd.TimeOut_Time__c != null){
				testTimeOutMap.put(tnd.MasterLabel, tnd.TimeOut_Time__c);
			}
		}


		for(TND_Result__c tnd : testRecords){
			createDate = tnd.CreatedDate.getTime();
			now = DateTime.now().getTime();
			milliseconds = now - createDate;
			seconds = milliseconds / 1000;
			if(tnd.Status__c == PENDING && tnd.TimedOut__c == false){
				if(testTimeOutMap.containsKey(tnd.recordType.Name) && seconds >=  testTimeOutMap.get(tnd.recordType.Name)){
					tnd.TimedOut__c = true;
					updateTNDRecords.add(tnd);
				}
			}
		}

		if(updateTNDRecords.size() > 0){
			try{
				update updateTNDRecords;
			}
			catch(Exception ex){
				System.debug(ex);
			}			
		}	
			
		return testRecords;
	}

	/**
	* Cancel TND test for Long running Tests
	*
	* @param      id    The SFDC identifier
	*/
	@AuraEnabled
	public static string[] cancelTndTest(string id)
	{
		List<TND_Result__c> testRecords = new  List<TND_Result__c>();
		EE_AS_TNDResponseWrapper respWrap;
		string[] errorMsgs = new string[]{};
		testRecords = [SELECT
			Id, 
			Status__c,
			RecordType.Name,
			Req_OVC_Id__c,
			Job_Id__c,
			Correlation_Id__c
		FROM
			TND_Result__c
		WHERE
			Id = : id];
		if(testRecords != null && testRecords.size() > 0)
		{
			TND_Result__c testRec = testRecords.get(0);
			if(testRec.RecordType.Name.equalsIgnoreCase(EE_AS_TnDAPIUtils.TND_TEST_TYPE_NPT_PROACTIVE)){
				if(EE_AS_TnDAPIUtils.TND_SCHED_STATUS.equals(testRec.Status__c) && String.isNotBlank(testRec.Job_Id__c)){
				respWrap = EE_AS_TnDAPIService.cancelScheduledNPTProactiveTest(testRec.Id,  (String)testRec.Job_Id__c);
				}else{
				respWrap = EE_AS_TnDAPIService.stopNPTProactiveTest(testRec.Id,  (String)testRec.Correlation_Id__c);
				}
			}else{
				respWrap = EE_AS_TnDAPIService.cancelTest(testRec.Id, (String)testRec.Req_OVC_Id__c, (String)testRec.RecordType.Name, (String)testRec.Correlation_Id__c);
			}
			
		}
		System.debug(' respWrap  size' + respWrap);
		if(respWrap != null && respWrap.errors != null && respWrap.errors.size() > 0)
		{
			for(EE_AS_TNDResponseWrapper.errors errorObj: respWrap.errors)
			{
				if(!string.isBlank(errorObj.defaultMessage))
				{
					System.debug('Error Message  ' + errorObj.defaultMessage);
					errorMsgs.add(errorObj.defaultMessage);
				}
			}
		}
		return errorMsgs;
	}

	/**
	 * Fetches a result for tnd proactive.
	 *
	 * @param      id    The identifier
	 *
	 * @return     Array of String- erorr messages
	 */
	@AuraEnabled
	public static string[] fetchResultForTndProactive(string id)
	{
		List<TND_Result__c> testRecords = new  List<TND_Result__c>();
		EE_AS_TNDResponseWrapper respWrap;
		string[] errorMsgs = new string[]{};
		testRecords = [SELECT
			Id, 
			Status__c,
			Correlation_Id__c
		FROM
			TND_Result__c
		WHERE
			Id = : id];
		if(testRecords != null && testRecords.size() > 0)
		{
			TND_Result__c testRec = testRecords.get(0);
			respWrap = EE_AS_TnDAPIService.fetchResultForNPTProactive(testRec.Id,  (String)testRec.Correlation_Id__c);
		}
		System.debug(' respWrap  size' + respWrap);
		if(respWrap != null && respWrap.errors != null && respWrap.errors.size() > 0)
		{
			for(EE_AS_TNDResponseWrapper.errors errorObj: respWrap.errors)
			{
				if(!string.isBlank(errorObj.defaultMessage))
				{
					System.debug('Error Message  ' + errorObj.defaultMessage);
					errorMsgs.add(errorObj.defaultMessage);
				}
			}
		}
		return errorMsgs;		
	}

	/**
	* Gets the completed test results for user.
	*
	* @return     The completed test results for user.
	*/
	@AuraEnabled
	public static List<TND_Result__c> getCompletedTestResultsForUser()
	{
		List<TND_Result__c> testRecords = new  List<TND_Result__c>();

		User loggedInUser = [SELECT
			AccountId
		FROM
			User
		WHERE
			id = : UserInfo.getUserId()
		Limit 1];

		testRecords = [SELECT
			Id, 
			Name, 
			Requested_Date__c, 
			Completion_Date__c, 
			Requested_By__r.Name, 
			Status__c, 
			recordType.Name, 
			Test_Id__c, 
			Reference_Id__c, 
			CoS_High_Result__c, 
			CoS_Low_Result__c, 
			CoS_Medium_Result__c
		FROM
			TND_Result__c
		WHERE
			(Status__c = : COMPLETE OR Status__c = : CANCELLED OR Status__c = : STATUS_USER_CANCELLED) AND Completion_Date__c <: Datetime.now().addMinutes(-EE_AS_TnDAPIUtils.TND_TIME_THRESHOLD) AND Completion_Date__c >: Datetime.now().addDays(-EE_AS_TnDAPIUtils.TND_DAY_THRESHOLD) AND createdby.UserType !=: USER_STD_TYPE AND createdby.UserType != null AND Related_Order__r.Opportunity_Bundle__r.Account__c = : loggedInUser.AccountId
		ORDER BY
			lastmodifieddate desc];
		return testRecords;
	}

	/**
	* Gets the test results for test id.
	*
	* @param      TestId  The test identifier
	*
	* @return     The test results for test id.
	*/
	@AuraEnabled
	public static TND_Result__c getTestResultsForTestID(string TestId)
	{
		TND_Result__c testRecord = new  TND_Result__c();
		testRecord = [SELECT
			Id, 
			Name, 
			Requested_Date__c, 
			Completion_Date__c, 
			Requested_By__r.Name, 
			Status__c, 
			recordType.Name, 
			Test_Id__c, 
			CreatedByID,
			Reference_Id__c, 
			CoS_High_Result__c, 
			CoS_Low_Result__c, 
			CoS_Medium_Result__c
		FROM
			TND_Result__c
		WHERE
			Test_Id__c = : TestId
		limit 1];
		return testRecord;
	}

	/**
	* update Test Record with Incident
	*
	* @param      testId      The test identifier
	* @param      incidentId  The incident identifier
	*/
	@AuraEnabled
	public static void updateTestRecordwithIncident(string testId, string incidentId)
	{
		TND_Result__c testRecord = new  TND_Result__c(Id = testId, Incident__c = incidentId);
		Update testRecord;
	}

	/**
	* Search Test Results
	*
	* @param      testId  The test identifier
	* @param      ovcId   The ovc identifier
	*
	* @return     List of TND_Result__c
	*/
	@AuraEnabled
	public static List<TND_Result__c> searchTestResults(string searchFilter)
	{
		User loggedInUser = [SELECT
			AccountId
		FROM
			User
		WHERE
			id = : UserInfo.getUserId()
		Limit 1];
		List<TND_Result__c> searchResults = new  List<TND_Result__c>();
		string searchQuery = 'SELECT Id, Name, Related_Order__c, Requested_Date__c,Req_OVC_Id__c, Completion_Date__c, Requested_By__r.Name, Status__c, recordType.Name,  Test_Id__c, Reference_Id__c, CoS_High_Result__c, CoS_Low_Result__c, CoS_Medium_Result__c FROM TND_Result__c';
		//construct filters
		List<string> filters = new  List<string>();
		if(loggedInUser != null && !string.isBlank(loggedInUser.AccountId))
		{
			filters.add('Related_Order__r.Opportunity_Bundle__r.Account__c = \'' + loggedInUser.AccountId + '\'');
		}
		if(!string.isBlank(searchFilter))
		{
			filters.add('(Test_Id__c = \'' + searchFilter + '\' OR Req_OVC_Id__c = \'' + searchFilter + '\')');
		}
		Datetime thresholdDate = Datetime.now().addDays(-EE_AS_TnDAPIUtils.TND_DAY_THRESHOLD);
		filters.add('Createddate > :thresholdDate');
		Set<string> externalTest = new Set<string>{EE_AS_TnDAPIUtils.TND_TEST_TYPE_LOOPBACK, EE_AS_TnDAPIUtils.TND_TEST_TYPE_NPT_STATISTICAL, EE_AS_TnDAPIUtils.TND_TEST_TYPE_UNI_E_PORT_RESET, EE_AS_TnDAPIUtils.TND_TEST_TYPE_UNI_E_STATUS, EE_AS_TnDAPIUtils.TND_TEST_TYPE_BTD_STATUS};
		filters.add('RecordType.Name IN :externalTest');
		filters.add('createdby.UserType != :USER_STD_TYPE AND createdby.UserType!=null');
		//apply filters
		if(filters.size() > 0)
		{
			searchQuery = searchQuery + ' WHERE';
			for(Integer f = 0; f < filters.size(); f++)
			{
				searchQuery = searchQuery + ' ' + filters.get(f);
				if(f != filters.size() - 1)
				{
					searchQuery = searchQuery + ' AND';
				}
			}
		}
		//apply ordering
		searchQuery = searchQuery + ' ORDER BY CreatedDate DESC';
		System.debug('Test Search Query : ' + searchQuery);
		//execute query
		searchResults = Database.query(searchQuery);
		System.debug('Test Results fount: ' + searchResults.size());
		return searchResults;
	}

	/**
	 * Gets the incident notes.
	 *
	 * @param      incId  The increment identifier
	 *
	 * @return     The incident notes.
	 */
	@AuraEnabled
	public static List<EE_AS_WorkLog__c> getIncidentNotes(string incId)
	{
		List<EE_AS_WorkLog__c> allIncNotes = [SELECT
			ID, 
			Note_Details__c, 
			Work_Log_Type__c, 
			CreatedBy.Name, 
			CreatedDate, 
			Channel_Id__c
		FROM
			EE_AS_WorkLog__c
		WHERE
			Parent_Incident__c = : incId
		ORDER BY
			createddate];
		return allIncNotes;
	}

	/**
	 * Gets the Network incident notes.
	 *
	 * @param      incId  The increment identifier
	 *
	 * @return     The incident notes.
	 */
	@AuraEnabled
	public static List<EE_AS_WorkLog__c> getNetworkIncidentNotes(string incId)
	{

		Set<Id> applicableNetIncNote = new Set<Id>();

	    User loggedInUser = [SELECT
			AccountId
		FROM
			User
		WHERE
			id = : UserInfo.getUserId()
		Limit 1];

	  //Check the junction object for applicable notes

		List<EE_AS_IncidentNoteAccessSeeker__c> applicableNotes = [SELECT
				ID, 
				Incident_Note__c
			FROM
				EE_AS_IncidentNoteAccessSeeker__c
			WHERE
				Incident_Note__r.Parent_Incident__c = : incId
			AND Account__c = :loggedInUser.AccountId
			];

		for(EE_AS_IncidentNoteAccessSeeker__c noteList : applicableNotes){
		applicableNetIncNote.add(noteList.Incident_Note__c);
		}

		//Get all the work Logs applicable
		List<EE_AS_WorkLog__c> allIncNotes = [SELECT
			ID, 
			Note_Details__c, 
			Work_Log_Type__c, 
			CreatedBy.Name, 
			CreatedDate, 
			Channel_Id__c
		FROM
			EE_AS_WorkLog__c
		WHERE
			ID IN :applicableNetIncNote
		ORDER BY
			createddate];

		return allIncNotes;
	}

	/**
	 * Creates incident notes.
	 *
	 * @param      incId       The increment identifier
	 * @param      noteDetail  The note detail
	 * @param      noteType    The note type
	 *
	 * @return     String
	 */
	@AuraEnabled
	public static string createIncidentNotes(string incId, string noteDetail, string noteType)
	{
		string summary;
		if(noteDetail != null && noteDetail.length() > 100)
		{
			summary = noteDetail.substring(0, 100);
		}
		else
		{
			summary = noteDetail;
		}
		EE_AS_WorkLog__c workLog = new  EE_AS_WorkLog__c();
		workLog.Work_Log_Type__c = noteType;
		workLog.Note_Summary__c = summary;
		workLog.Note_Source__c = 'Web';
		workLog.Provided_By__c = 'SALESFORCE';
		workLog.Note_Details__c = noteDetail;
		workLog.Created_By_SFDC__c = true;
		workLog.Parent_Incident__c = incId;
		Insert workLog;
		return workLog.ID;
	}
}