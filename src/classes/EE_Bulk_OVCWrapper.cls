public with sharing class EE_Bulk_OVCWrapper {
			public String quoteId;
			public String basketId;
			public String uniProdConfigId;
			public String OVCId;	//a1h5D0000006i8YQAQ
			public String OVCName; //OVC 1
			public String CSA;	//CSA180000002222
			public String NNIGroupId;	//NNI123456789123
			public String routeType;	//Local
			public String coSHighBandwidth;	//250
			public String coSMediumBandwidth;	//100
			public String coSLowBandwidth;	//0
			public String routeRecurring;	//0.00
			public String coSRecurring;	//0.00
			public String ovcMaxFrameSize;	//Jumbo (9000 Bytes)
			public String status;	//Incomplete
			public String sTag;	//12
			public String ceVlanId;	//2
			public String mappingMode;	//DSCP
			public String ovcSystemId; //OVC000000123456
			public String POI; //3EXH - EXHIBITIOn
			public Decimal numberOfOVCsForQuote;
			public string uniAfterHours;
			public string unieSLA;
			public string uniTerm;
			public Integer ovcNumber;
			public String errorStringForQuote;
}