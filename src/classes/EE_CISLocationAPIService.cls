public class EE_CISLocationAPIService  {

	public static final String ERR_INVALID_SEARCH_BY_LOCID_INPUTS = 'Invalid SearchByLocationId Inputs.';
	public static final String ERR_INVALID_SEARCH_BY_LATLONG_INPUTS = 'Invalid SearchByLatLong Inputs.';
	public static final String ERR_INVALID_SEARCH_BY_ADDR_INPUTS = 'Invalid SearchByAddress Inputs.';
	public static final String ERR_LOCATIONID_NOT_FOUND = 'LocationId was not found.';
	public static final String ERR_LATLONG_NOT_FOUND = 'Latitide or longitude was not found.';
	public static final String ERR_SFREQ_NOT_FOUND = 'Service Feasibility Request record was not found.';
	public static final String ERR_ADDR_API_ENDPOINT_PARAMS_EMPTY = 'Address API endpoint parameters are empty.';
	public static final String CIS_PLACES_API_SETTING = 'EE-CISPlacesAPI';

	/**
	 * Search Location Sync Call
	 *
	 * @param      param  The parameter SF Req ID
	 */
	public static String searchLocationSync(DF_SF_Request__c sfReq) {
		return searchLocation(sfReq, null, false, false);
	}

    public static String editLocationSync(DF_SF_Request__c sfReq) {
		return searchLocation(sfReq, null, false, true);
	}

	/**
	 * Search Location Async Call
	 *
	 * @param      param  The parameter SF Req ID
	 */
	@future(callout = true)
	public static void searchLocationAsync(String param) {
		searchLocation(null, param, true, false);
	}


	/**
	 * Search Location
	 *
	 * @param      param  The parameter SF Req ID
	 */

	public static String searchLocation(DF_SF_Request__c sfRequest, String param, Boolean isAsync, Boolean isEdit) {

		String response;
		Http http = new Http();
		HttpRequest req;
		HTTPResponse res;
		String queryString;
		DF_Integration_Setting__mdt integrSetting ;
		String  bundleID = null;
		List<DF_SF_Request__c> sfReqList = new List<DF_SF_Request__c>();
		List<DF_SF_Request__c> bulkPNIList = new List<DF_SF_Request__c>();
		List<DF_SF_Request__c> reTryList = new List<DF_SF_Request__c>();
		List<DF_SF_Request__c> noPNIList = new List<DF_SF_Request__c>();
		List<DF_SF_Request__c> sfErrorList = new List<DF_SF_Request__c>();
		List<ApplicationLogWrapper> logMsgList = new List<ApplicationLogWrapper>();

		// Errors
		final String ERROR_MESSAGE = 'A HTTP error occurred in class method EE_CISLocationAPIService.searchLocation';

		// Get SF Request Details
		if (sfRequest != null) {
			sfReqList.add(sfRequest);
		} else if (String.isNotBlank(param)) {
			sfReqList.addAll(EE_CISLocationAPIUtils.getSFReqDetails(param.split(EE_CISLocationAPIUtils.STR_COMMA)));
		}

		integrSetting = EE_CISLocationAPIUtils.getIntegrationSettings(EE_CISLocationAPIUtils.INTEGR_SETTING_NAME_CIS_LOCATION_API);

		for (DF_SF_Request__c sfReq : sfReqList) {
			try {
				if (sfReq != null) {
					queryString = validatePrepareQueryString(sfReq);
				} else {
					throw new CustomException(ERR_SFREQ_NOT_FOUND);
				}

				//Make callout
				res = makeCIScallout(integrSetting, queryString);

				response = res.getBody();

				if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {

					if (String.isNotEmpty(response)) {
						DF_ServiceFeasibilityResponse sfResponse = EE_CISLocationAPIUtils.getDFSFReqDTOFromLocationIdResults(response, sfReq);

						//If Sync call for only one loc Id, Then make PNI call here
						if (!isAsync) {

							//Call PNI.
							//TODO Hard coded PNI values to be removed
							if (EE_CISLocationAPIUtils.STATUS_PENDING.equalsIgnoreCase(sfResponse.status)) {

								// TODO: Call out to PNI and process the response
								EE_PNIDistanceAPIService.getDistanceSync(sfResponse);

								//Only if the PNI returned valid value
								if ((String.isNotBlank(sfResponse.fibreJointId) && sfResponse.Distance != null) || (sfResponse.isNoFibreJointFound!=null && sfResponse.isNoFibreJointFound)) {
									sfResponse.status = EE_CISLocationAPIUtils.STATUS_VALID;
								} else {
									sfResponse.status = EE_CISLocationAPIUtils.STATUS_ERROR;
								}

							}
							sfReq.Status__c = EE_CISLocationAPIUtils.STATUS_COMPLETED;
							sfReq.Response__c = JSON.serialize(sfResponse);

                            if(isEdit) {
                            	return 'OK';
                            }

							DF_Opportunity_Bundle__c bundle = EE_CISLocationAPIUtils.createOpptyBundle();
							insert bundle;
							bundleID = bundle.id;
							sfReq.Opportunity_Bundle__c  = bundleID;
							insert sfReq;
							return bundleID;
						} else {
							sfReq.Response__c = JSON.serialize(sfResponse);
							if (EE_CISLocationAPIUtils.STATUS_PENDING.equals(sfResponse.status)) {
								bulkPNIList.add(sfReq);
							} else {
								sfReq.Status__c = EE_CISLocationAPIUtils.STATUS_COMPLETED;
								noPNIList.add(sfReq);
							}
						}

					} else {
						throw new CustomException(DF_IntegrationUtils.ERR_RESP_BODY_MISSING);
					}
				} else {
					List<String> addressList = new List<String>();
					DF_ServiceFeasibilityResponse sfResponse = new DF_ServiceFeasibilityResponse();

					if (res.getStatusCode() == DF_Constants.HTTP_STATUS_404) {
						sfResponse.status = EE_CISLocationAPIUtils.STATUS_INVALID_LOCID;
					} else {
						sfResponse.status = EE_CISLocationAPIUtils.STATUS_ERROR;
					}

					sfResponse.LocId = sfReq.Location_Id__c;
					sfResponse.latitude = sfReq.Latitude__c;
					sfResponse.Longitude =  sfReq.Longitude__c;

					if (EE_CISLocationAPIUtils.SEARCH_TYPE_ADDRESS.equals(sfReq.Search_Type__c)) {
						addressList.add(EE_CISLocationAPIUtils.getInputAddressFullText(sfReq));
					}
					sfResponse.AddressList =  new List<String>(addressList);
					sfReq.Status__c = EE_CISLocationAPIUtils.STATUS_COMPLETED;
					sfReq.Response__c = JSON.serialize(sfResponse);
					if (isAsync) {
						ApplicationLogWrapper appLogMsg = new ApplicationLogWrapper();
						appLogMsg.debugLevel = DF_Constants.ERROR;
						appLogMsg.source  = EE_CISLocationAPIService.class.getName();
						appLogMsg.sourceFunction = 'searchLocation';
						appLogMsg.referenceId = sfReq.Id;
						appLogMsg.referenceInfo = integrSetting.Context_Path__c + queryString;
						appLogMsg.logMessage = String.valueOf(res.getStatusCode());
						appLogMsg.payload = response;
						appLogMsg.timer = 0;
						logMsgList.add(appLogMsg);
						sfErrorList.add(sfReq);
						if (EE_CISLocationAPIUtils.STATUS_ERROR.equals(sfResponse.status)) {
							retryList.add(sfReq);
						}

					} else {
						GlobalUtility.logMessage(DF_Constants.ERROR, EE_CISLocationAPIService.class.getName(), 'searchLocation', sfReq.Id, integrSetting.Context_Path__c + queryString, String.valueOf(res.getStatusCode()), response, null, 0);
                        if(isEdit) {
                        	return 'KO';
                        }
						DF_Opportunity_Bundle__c bundle = EE_CISLocationAPIUtils.createOpptyBundle();
						insert bundle;
						bundleID = bundle.id;
						sfReq.Opportunity_Bundle__c  = bundleID;
						insert sfReq;
						return bundleID;
					}

				}

			} catch (Exception e) {
				if (sfReq != null) {
					sfReq.Status__c = EE_CISLocationAPIUtils.STATUS_ERROR;

					if (isAsync) {
						ApplicationLogWrapper appLogMsg = new ApplicationLogWrapper();
						appLogMsg.debugLevel = DF_Constants.ERROR;
						appLogMsg.source  = EE_CISLocationAPIService.class.getName();
						appLogMsg.sourceFunction = 'searchLocation';
						appLogMsg.referenceId = sfReq.Id;
						appLogMsg.referenceInfo = integrSetting.Context_Path__c + queryString;
						appLogMsg.logMessage = String.valueOf(res.getStatusCode());
						appLogMsg.payload = response;
						appLogMsg.timer = 0;
						logMsgList.add(appLogMsg);
						sfErrorList.add(sfReq);
						retryList.add(sfReq);
					} else {
						GlobalUtility.logMessage(DF_Constants.ERROR, EE_CISLocationAPIService.class.getName(), 'searchLocationByLocationId', sfReq.Id, integrSetting.Context_Path__c + queryString, e.getMessage(), response, e, 0);
                        if(isEdit) {
                        	return 'KO';
                        }
						DF_Opportunity_Bundle__c bundle = EE_CISLocationAPIUtils.createOpptyBundle();
						insert bundle;
						bundleID = bundle.id;
						sfReq.Opportunity_Bundle__c  = bundle.id;
						insert sfReq;
						return bundleID;
					}
				} else {
					throw new CustomException(e.getMessage());
				}
			}
		}

		//end of bigger for loop
		//Make PNI call for Bulk
		//get the Fibre Joint details
		//Update the SF Response DTO
		if (isAsync) {

			GlobalUtility.logMessage(logMsgList);
			UPDATE sfErrorList;

			for (DF_SF_Request__c sfRequestForReTry : reTryList) {
				List<String> paramList = new List<String>();
				paramList.add(sfRequestForReTry.id);
				AsyncQueueableUtils.retry(EE_CISLocationSearchHandler.HANDLER_NAME, paramList, ERROR_MESSAGE + '\n\n' + 'Response Status Code: ' + res.getStatusCode());
			}

			// Update the SF object with the response from CIS
			UPDATE noPNIList;

			if(bulkPNIList != null && !bulkPNIList.isEmpty()) {

				UPDATE bulkPNIList;

				Set<String> sfReqs = new Set<String>();
				for(DF_SF_Request__c sfReq : bulkPNIList) {
					sfReqs.add(sfReq.Id);
				}

				List<String> paramsList = EE_CISLocationAPIUtils.getParamsInBulk(sfReqs, EE_CISLocationAPIUtils.API_PNI);
				if(paramsList != null && !paramsList.isEmpty()) {
					for(String params : paramsList) {
						AsyncQueueableUtils.createRequests(EE_PNIDistanceHandler.HANDLER_NAME, new List<String>{params});
					}
				}
			}

			
			
		}

		return bundleID;
	}

	/**
	 * Validate and Prepare QueryString
	 *
	 * @param      sfReq  The sf request
	 *
	 * @return     String Query String
	 */
	public static String validatePrepareQueryString(DF_SF_Request__c sfReq) {

		String queryString = '';
		// Validate data inputs
		if (EE_CISLocationAPIUtils.SEARCH_TYPE_LOCATION_ID.equals(sfReq.Search_Type__c)) {
			if (EE_CISLocationAPIUtils.hasValidSearchByLocationIdInputs(sfReq)) {
				String locationId = sfReq.Location_Id__c;
				queryString = '/' + locationId + '?include=containmentBoundaries';
			} else {
				throw new CustomException(ERR_INVALID_SEARCH_BY_LOCID_INPUTS);
			}
		} else if (EE_CISLocationAPIUtils.SEARCH_TYPE_LAT_LONG.equals(sfReq.Search_Type__c)) {
			if (EE_CISLocationAPIUtils.hasValidSearchByLatLongInputs(sfReq)) {
				String latitude = sfReq.Latitude__c;
				String longitude = sfReq.Longitude__c;
				queryString = EE_CISLocationAPIUtils.URL_FILTER_PREFIX + EE_CISLocationAPIUtils.LATITUDE + EE_CISLocationAPIUtils.URL_PARAM_ASSIGN_OPR + latitude + EE_CISLocationAPIUtils.URL_PARAM_DELIMITER + EE_CISLocationAPIUtils.LONGITUDE + EE_CISLocationAPIUtils.URL_PARAM_ASSIGN_OPR + longitude + '&include=containmentBoundaries';
			} else {
				throw new CustomException(ERR_INVALID_SEARCH_BY_LATLONG_INPUTS);
			}
		} else if (EE_CISLocationAPIUtils.SEARCH_TYPE_ADDRESS.equals(sfReq.Search_Type__c)) {
			if (EE_CISLocationAPIUtils.hasValidSearchByAddressInputs(sfReq)) {
				String addrQueryFilter = EE_CISLocationAPIUtils.getInputAddressQueryFilter(sfReq);
				// Convert to urlEndcoded
				String searchParam = EE_CISLocationAPIUtils.convertToURLEncodedStr(addrQueryFilter);
				if (String.isEmpty(searchParam)) {
					throw new CustomException(ERR_ADDR_API_ENDPOINT_PARAMS_EMPTY);
				}
				queryString = EE_CISLocationAPIUtils.URL_FILTER_PREFIX + searchParam + '&include=containmentBoundaries';

			} else {
				throw new CustomException(ERR_INVALID_SEARCH_BY_ADDR_INPUTS);
			}
		}

		return queryString;

	}

	/**
	 * Makes a CIS callout.
	 *
	 * @param      integrSetting  The integr setting
	 * @param      queryString    The query string
	 *
	 * @return     HTTPResponse Response fields
	 */
	public static HTTPResponse  makeCISCallout(DF_Integration_Setting__mdt integrSetting, String queryString) {

		HTTPResponse res;
		HttpRequest req;
		Http http = new  Http();
		final String END_POINT;
		final Integer TIMEOUT_LIMIT;
		final String NAMED_CREDENTIAL;
		final String NBN_ENV_OVRD_VAL;
		final String CONTEXT_PATH;

		// get custom setting data for Integration Input details
        NBNIntegrationInputs__c nbnCISPlacesInstaces = NBNIntegrationInputs__c.getInstance(CIS_PLACES_API_SETTING);

		if(nbnCISPlacesInstaces!=null){
		NAMED_CREDENTIAL = nbnCISPlacesInstaces.Is_Api_Gateway__c ? nbnCISPlacesInstaces.API_GW_Callout_URL__c : nbnCISPlacesInstaces.Call_Out_URL__c ;
		}else{
		NAMED_CREDENTIAL = DF_Constants.NAMED_CRED_PREFIX + integrSetting.Named_Credential__c;
		}

		Map<String, String> headerMap = new Map<String, String>();


		TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
		NBN_ENV_OVRD_VAL = integrSetting.DP_Environment_Override__c;
		CONTEXT_PATH = integrSetting.Context_Path__c;

		system.debug('NAMED_CREDENTIAL: ' + NAMED_CREDENTIAL);
		system.debug('TIMEOUT_LIMIT: ' + TIMEOUT_LIMIT);
		system.debug('NBN_ENV_OVRD_VAL: ' + NBN_ENV_OVRD_VAL);

		// Build endpoint
		END_POINT = NAMED_CREDENTIAL + CONTEXT_PATH + queryString;
		system.debug('END_POINT: ' + END_POINT);

        if (nbnCISPlacesInstaces!=null) {
            headerMap = integrationUtility.prepareHttpReqHeaderMap(nbnCISPlacesInstaces,null,null);
        }

		//Commented unwanted header parameter for APP Gateway
		//headerMap.put(DF_IntegrationUtils.NBN_ENV_OVRD, NBN_ENV_OVRD_VAL);
		headerMap.put(DF_Constants.CONTENT_TYPE, DF_Constants.CONTENT_TYPE_JSON);
		//headerMap.put(EE_CISLocationAPIUtils.REQ_HDR_REFERER, DF_Constants.NBN_DOMAIN);

		// Generate request commented to use existing IntegrationUtil
		//req = EE_CISLocationAPIUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, headerMap);

		// Send and get response
		//res = http.send(req);

		res = integrationUtility.doHttpCallout(DF_Constants.HTTP_METHOD_GET, END_POINT, headerMap , TIMEOUT_LIMIT, null, null, 'makeCISCallout');

		system.debug('Response Status Code: ' + res.getStatusCode());
		system.debug('Response Body: ' + res.getBody());

		return res;
	}

}