/*------------------------------------------------------------------------
Description:   	A test class created to test EE_CISLocationAPIService methods

--------------------------------------------------------------------------*/

@isTest
public class EE_CISLocationAPIServiceTest {

	public static final String LOC_ID_0 = '';
	public static final String LOC_ID_1 = 'LOC000035375038';
	public static final String LATITUDE_0 = '';
	public static final String LONGITUDE_0 = '';
	public static final String LATITUDE_1 = '44.968046';
	public static final String LONGITUDE_1 = '-94.420307';
	public static final String STATE_1 = 'VIC';
	public static final String POSTCODE_1 = '3000';
	public static final String SUBURB_LOCALITY_1 = 'Melbourne';
	public static final String UNIT_NUMBER_1 = '1';
	public static final String STREET_NAME_1 = 'Collins';
	public static final String STREET_LOT_NUMBER_1 = '123';
	public static final String STATUS_VALID = 'Valid';

	@testSetup static void testDataSetup() {
		DF_AS_TestService.createNBNIntegrationInputsCustomSettings();
		Account acct = DF_TestData.createAccount('Test Account');
		acct.Access_Seeker_Id__c = 'abctest';
		insert acct;

		String response;
		
		Map<String, String> addressMap1 = new Map<String, String>();
		addressMap1.put('state', STATE_1);
		addressMap1.put('postcode', POSTCODE_1);
		addressMap1.put('suburbLocality', SUBURB_LOCALITY_1);
		addressMap1.put('streetName', STREET_NAME_1);
		addressMap1.put('streetLotNumber', STREET_LOT_NUMBER_1);
		addressMap1.put('unitNumber', UNIT_NUMBER_1);
		
		Map<String, String> addressMap0 = new Map<String, String>();

		User commUser = DF_TestData.createDFCommUser();
		
		System.runAs(commUser) {

			// Create Oppty Bundle
			String opptyBundleId = DF_SF_CaptureController.createOpptyBundle();

			// Create sfreq
			DF_SF_Request__c sfReq1 = DF_TestData.createSFRequest(EE_CISLocationAPIUtils.SEARCH_TYPE_LOCATION_ID, EE_CISLocationAPIUtils.STATUS_PENDING, LOC_ID_1, null, null, opptyBundleId, response, addressMap0);
			insert sfReq1;

			DF_SF_Request__c sfReq2 = DF_TestData.createSFRequest(EE_CISLocationAPIUtils.SEARCH_TYPE_LAT_LONG, EE_CISLocationAPIUtils.STATUS_PENDING, null, LATITUDE_1, LONGITUDE_1, opptyBundleId, response, addressMap0);
			insert sfReq2;

			DF_SF_Request__c sfReq3 = DF_TestData.createSFRequest(EE_CISLocationAPIUtils.SEARCH_TYPE_LOCATION_ID, EE_CISLocationAPIUtils.STATUS_PENDING, LOC_ID_0, null, null, opptyBundleId, response, addressMap0);
			insert sfReq3;

			DF_SF_Request__c sfReq4 = DF_TestData.createSFRequest(EE_CISLocationAPIUtils.SEARCH_TYPE_LAT_LONG, EE_CISLocationAPIUtils.STATUS_PENDING, null, LATITUDE_0, LONGITUDE_0, opptyBundleId, response, addressMap0);
			insert sfReq4;

			DF_SF_Request__c sfReq5 = DF_TestData.createSFRequest(EE_CISLocationAPIUtils.SEARCH_TYPE_ADDRESS, EE_CISLocationAPIUtils.STATUS_PENDING, null, null, null, opptyBundleId, response, addressMap1);
			insert sfReq5;

			DF_SF_Request__c sfReq6 = DF_TestData.createSFRequest(EE_CISLocationAPIUtils.SEARCH_TYPE_ADDRESS, EE_CISLocationAPIUtils.STATUS_PENDING, null, null, null, opptyBundleId, response, addressMap0);
			insert sfReq6;
		}
	}

	@isTest static void test_validatePrepareQueryString_locId_success() {
		String queryString = '';
		Test.startTest();
		User commUser = getCommUserByEmail('test123@noemail.com');
		System.runAs(commUser) {
			DF_SF_Request__c sfReq = getSFRequestByLocId(LOC_ID_1);
			queryString = EE_CISLocationAPIService.validatePrepareQueryString(sfReq);
		}
		Test.stopTest();
		System.assertNotEquals(queryString, '');
		Boolean result = queryString.contains(LOC_ID_1);
		System.assert(result);
	}

	@isTest static void test_validatePrepareQueryString_locId_fail() {
		String queryString = '';
		Test.startTest();
		try {
			User commUser = getCommUserByEmail('test123@noemail.com');
			System.runAs(commUser) {
				DF_SF_Request__c sfReq = getSFRequestByLocId(LOC_ID_0);
				queryString = EE_CISLocationAPIService.validatePrepareQueryString(sfReq);
			}
		} catch (Exception e) {
			Boolean expectedExceptionThrown =  e.getMessage().contains(EE_CISLocationAPIService.ERR_INVALID_SEARCH_BY_LOCID_INPUTS);
			System.AssertEquals(expectedExceptionThrown, true);
		}
		Test.stopTest();
	}

	@isTest static void test_validatePrepareQueryString_latlong_success() {
		String queryString = '';
		Test.startTest();
		User commUser = getCommUserByEmail('test123@noemail.com');
		System.runAs(commUser) {
			DF_SF_Request__c sfReq = getSFRequestByLatLong(LATITUDE_1, LONGITUDE_1);
			queryString = EE_CISLocationAPIService.validatePrepareQueryString(sfReq);
		}
		Test.stopTest();
		System.assertNotEquals(queryString, '');
		Boolean result = queryString.contains(LATITUDE_1) && queryString.contains(LONGITUDE_1);
		System.assert(result);
	}

	@isTest static void test_validatePrepareQueryString_latlong_fail() {
		String queryString = '';
		Test.startTest();
		try {
			User commUser = getCommUserByEmail('test123@noemail.com');
			System.runAs(commUser) {
				DF_SF_Request__c sfReq = getSFRequestByLatLong(LATITUDE_0, LONGITUDE_0);
				queryString = EE_CISLocationAPIService.validatePrepareQueryString(sfReq);
			}
		} catch (Exception e) {
			Boolean expectedExceptionThrown =  e.getMessage().contains(EE_CISLocationAPIService.ERR_INVALID_SEARCH_BY_LATLONG_INPUTS);
			System.AssertEquals(expectedExceptionThrown, true);
		}
		Test.stopTest();
	}

	@isTest static void test_validatePrepareQueryString_addr_success() {
		String queryString = '';
		Test.startTest();
		User commUser = getCommUserByEmail('test123@noemail.com');
		System.runAs(commUser) {
			DF_SF_Request__c sfReq = getSFRequestByAddr(STATE_1, POSTCODE_1, SUBURB_LOCALITY_1, STREET_NAME_1, STREET_LOT_NUMBER_1);
			queryString = EE_CISLocationAPIService.validatePrepareQueryString(sfReq);
		}
		Test.stopTest();
		System.assertNotEquals(queryString, '');
		Boolean result = queryString.contains(STATE_1) && queryString.contains(POSTCODE_1);
		System.assert(result);
	}

	@isTest static void test_validatePrepareQueryString_addr_fail_1() {
		String queryString = '';
		Test.startTest();
		try {
			User commUser = getCommUserByEmail('test123@noemail.com');
			System.runAs(commUser) {
				DF_SF_Request__c sfReq = getSFRequestByAddr(null, null, null, null, null);
				queryString = EE_CISLocationAPIService.validatePrepareQueryString(sfReq);
			}
		} catch (Exception e) {
			Boolean expectedExceptionThrown =  e.getMessage().contains(EE_CISLocationAPIService.ERR_INVALID_SEARCH_BY_ADDR_INPUTS);
			System.AssertEquals(expectedExceptionThrown, true);
		}
		Test.stopTest();
	}

    @isTest static void shouldGetSuccessResponseOnEditLocationSync() {
        String locationId = 'LOC000005972574';
        String response = null;
        
        // Create sfreq
        DF_SF_Request__c request = EE_CISLocationAPIUtils.createServiceFeasibilityRequest(
            DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, locationId, null, null, null, response, new Map<String, String>());
        
        // Generate mock response
        response = getCISPositiveJSON();
        
        // stub cis
        Map<string, string> headerMap = new Map<string, string>();
        headerMap.put('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, new EE_HttpCalloutMock(200, 'ok', response, headerMap));
        // stub pni 
        Test.setMock(WebServiceMock.class, new EE_WebServiceCalloutMock(DF_IntegrationTestData.getPNIPositive()));
        
        test.startTest();
        
        String result = EE_CISLocationAPIService.editLocationSync(request);
        System.assertEquals('OK', result);
        
        test.stopTest();
    }
    
    @isTest static void shouldGetFailResponseOnEditLocationSyncWhenStatusCodeIsNot200() {
        String locationId = 'LOC000005972574';
        String response = null;
        
        // Create sfreq
        DF_SF_Request__c request = EE_CISLocationAPIUtils.createServiceFeasibilityRequest(
            DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, locationId, null, null, null, response, new Map<String, String>());
        
        // Generate mock response
        response = getCISPositiveJSON();
        
        // stub cis
        Map<string, string> headerMap = new Map<string, string>();
        headerMap.put('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, new EE_HttpCalloutMock(500, 'fail', response, headerMap));
        // stub pni 
        Test.setMock(WebServiceMock.class, new EE_WebServiceCalloutMock(DF_IntegrationTestData.getPNIPositive()));
        
        test.startTest();
        
        String result = EE_CISLocationAPIService.editLocationSync(request);
        System.assertEquals('KO', result);
        
        test.stopTest();
    }
    
    class EE_HttpCalloutExceptionMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
    		throw new CustomException('something bad happened');
        }
    }
        
	@isTest static void test_makeCISCallout_positive() {
		DF_Integration_Setting__mdt integrSetting = EE_CISLocationAPIUtils.getIntegrationSettings(EE_CISLocationAPIUtils.INTEGR_SETTING_NAME_CIS_LOCATION_API);
		String queryString = '';
		User commUser = getCommUserByEmail('test123@noemail.com');
		System.runAs(commUser) {
			DF_SF_Request__c sfReq = getSFRequestByLocId(LOC_ID_1);
			queryString = EE_CISLocationAPIService.validatePrepareQueryString(sfReq);

			Map<string, string> headerMap = new  Map<string, string>();
			headerMap.put('Content-Type', 'application/json');
			
			EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', getCISPositiveJSON(), headerMap);
			
			Test.startTest();
			
			Test.setMock(HttpCalloutMock.class, mockObj);
			HTTPResponse res = EE_CISLocationAPIService.makeCIScallout(integrSetting, queryString);
			
			Test.stopTest();
			
			//System.assertEquals(null,res.errors);
		}
		
	}

	@isTest static void testRunLoopbackTestNegative_500_Error() {
		DF_Integration_Setting__mdt integrSetting = EE_CISLocationAPIUtils.getIntegrationSettings(EE_CISLocationAPIUtils.INTEGR_SETTING_NAME_CIS_LOCATION_API);
		String queryString = '';
		User commUser = getCommUserByEmail('test123@noemail.com');
		System.runAs(commUser) {
			DF_SF_Request__c sfReq = getSFRequestByLocId(LOC_ID_1);
			queryString = EE_CISLocationAPIService.validatePrepareQueryString(sfReq);

			Map<string, string> headerMap = new  Map<string, string>();
			headerMap.put('Content-Type', 'application/json');
			
			EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(500, 'Internal Server Error', getEmptyJSON(), headerMap);
			
			Test.startTest();
			
			Test.setMock(HttpCalloutMock.class, mockObj);
			HTTPResponse res = EE_CISLocationAPIService.makeCIScallout(integrSetting, queryString);
			
			Test.stopTest();
		}
	}

	static DF_SF_Request__c getSFRequestByLocId(String locId) {
		List<DF_SF_Request__c> sfReqs = [SELECT Id, Search_Type__c, Location_Id__c FROM DF_SF_Request__c WHERE Location_Id__c = :locId AND Search_Type__c = :EE_CISLocationAPIUtils.SEARCH_TYPE_LOCATION_ID];
		DF_SF_Request__c sfReq = null;
		if(sfReqs != null && !sfReqs.isEmpty()) {
			sfReq = sfReqs.get(0);
		}
		return sfReq;
	}

	static DF_SF_Request__c getSFRequestByLatLong(String latitude, String longitude) {
		List<DF_SF_Request__c> sfReqs = [SELECT Id, Search_Type__c, Latitude__c, Longitude__c, Location_Id__c FROM DF_SF_Request__c WHERE Latitude__c = :latitude AND Longitude__c = :longitude AND Search_Type__c = :EE_CISLocationAPIUtils.SEARCH_TYPE_LAT_LONG];
		DF_SF_Request__c sfReq = null;
		if(sfReqs != null && !sfReqs.isEmpty()) {
			sfReq = sfReqs.get(0);
		}
		return sfReq;
	}
	
	static DF_SF_Request__c getSFRequestByAddr(String state, String postcode, String suburbLocality, String streetName, String streetLotNumber) {
		List<DF_SF_Request__c> sfReqs = [SELECT 
												Id, Location_Id__c, Latitude__c, Longitude__c,State__c,Postcode__c,Suburb_Locality__c,
												Street_Name__c,Street_Type__c,Street_or_Lot_Number__c,Unit_Type__c,
												Unit_Number__c,Level__c,Complex_or_Site_Name__c,Building_Name_in_a_Complex__c,
												Street_Name_in_a_Complex__c,Street_Type_in_a_Complex__c,Street_Number_in_a_Complex__c,
												Address__c,Status__c,Search_Type__c
											FROM 
												DF_SF_Request__c 
											WHERE 
												State__c = :state AND 
												Postcode__c = :postcode AND
												Suburb_Locality__c = :suburbLocality AND
												Street_Name__c = :streetName AND
												Street_or_Lot_Number__c = :streetLotNumber AND
												Search_Type__c = :EE_CISLocationAPIUtils.SEARCH_TYPE_ADDRESS];
		DF_SF_Request__c sfReq = null;
		if(sfReqs != null && !sfReqs.isEmpty()) {
			sfReq = sfReqs.get(0);
		}
		return sfReq;
	}

	static String getDFPartnerProfile() {
		return (String)([SELECT Id FROM Profile WHERE Name = 'DF Partner User'].get(0).Id);
	}

	static User getCommUserByProfile(String profileId) {
		User commUser = null;
		List<User> users = [SELECT Id, Lastname, Alias FROM User WHERE Profileid = :profileId AND IsActive = true];
		if(users != null && !users.isEmpty()) {
			commUser = users.get(0);
		} else {
			commUser = DF_TestData.createDFCommUser();
		}
		return commUser;
	}

	static User getCommUserByEmail(String emailId) {
		User commUser = null;
		List<User> users = [SELECT Id, Lastname, Alias FROM User WHERE Email = :emailId AND IsActive = true];
		if(users != null && !users.isEmpty()) {
			commUser = users.get(0);
		} else {
			commUser = DF_TestData.createDFCommUser();
		}
		return commUser;
	}


	private static string getCISNegJSON() {
		string json = '';
		return json;
	}


	private static string getCISPositiveJSON() {
		string json = '{ 	"data": [{	"type": "location",	"id": "LOC000035375038",	"attributes": {	"dwellingType": "MDU",	"region": "Urban",	"regionValueDefault": "FALSE",	"externalTerritory": "FALSE",	"address": {	"unstructured": "HILLSIDE NURSING HOME  UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290",	"roadNumber1": "3",	"roadName": "VIOLET TOWN",	"roadTypeCode": "RD",	"locality": "MOUNT HUTTON",	"postCode": "2290",	"unitNumber": "212",	"unitType": "UNIT",	"state": "NSW",	"siteName": "HILLSIDE NURSING HOME"	},	"primaryAccessTechnology": {	"technologyType": "Fibre to the node",	"serviceClass": "13",	"rolloutType": "Brownfields"	},	"csaId": "CSA200000010384",	"serviceabilityMessage": null,	"serviceabilityDate": null,	"serviceabilityClassReason": null,	"geoCode": {	"latitude": "-32.9873616",	"longitude": "151.67177704",	"geographicDatum": "GDA94",	"srid": "4283"	},	"landUse": "RESIDENTIAL"	},	"relationships": {	"containmentBoundaries": {	"data": [{	"id": "2BLT-03-11",	"type": "NetworkBoundary",	"boundaryType": "ADA"	},	{	"id": "2BLT-03",	"type": "NetworkBoundary",	"boundaryType": "SAM"	},	{	"id": "S2 - Mount Hutton - Windale",	"type": "NetworkBoundary",	"boundaryType": "SSA"	},	{	"id": "2BLT",	"type": "NetworkBoundary",	"boundaryType": "FSA"	},	{	"id": "2HAM",	"type": "NetworkBoundary",	"boundaryType": "AAR"	}]	},	"MDU": {	"data": {	"id": "LOC100062870710",	"type": "location"	}	}	} 	}], 	"included": [{	"type": "NetworkBoundary",	"id": "2BLT-03-11",	"attributes": {	"boundaryType": "ADA",	"status": "INSERVICE",	"technologyType": "Copper",	"technologySubType": "FTTN"	} 	}, 	{	"type": "NetworkBoundary",	"id": "2BLT-03",	"attributes": {	"boundaryType": "SAM",	"status": "INSERVICE",	"technologyType": "Multiple"	} 	}, 	{	"type": "NetworkBoundary",	"id": "S2 - Mount Hutton - Windale",	"attributes": {	"boundaryType": "SSA",	"status": "PLANNED",	"technologyType": "Satellite"	} 	}, 	{	"type": "NetworkBoundary",	"id": "2BLT",	"attributes": {	"boundaryType": "FSA",	"status": "INSERVICE",	"technologyType": "Multiple"	} 	}, 	{	"type": "NetworkBoundary",	"id": "2HAM",	"attributes": {	"boundaryType": "AAR",	"status": "PLANNED",	"technologyType": "Multiple"	} 	}] }';
		return json;
	}


	private static string getEmptyJSON() {
		string json = '{}';
		return json;
	}
}