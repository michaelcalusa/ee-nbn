public class EE_CISLocationResponseParsar  {

	public List<Data> data; 
	public List<Included> included; 

	public class Address {

        public string unstructured ;
        public string siteName ;
        public string locationDescriptor ;
        public string levelType ;
        public string levelNumber ;
        public string unitType ;
        public string unitNumber ;
        public string lotNumber ;
        public string planNumber ;
        public string roadNumber1 ;
        public string roadNumber2 ;
        public string roadName ;
        public string roadTypeCode ;
        public string roadSuffixCode ;
        public string locality ;
        public string postCode ;
        public string state ;
        public ComplexAddress complexAddress ;
        public string type ;

	}

    public class ComplexAddress
    {
        public string siteName ;
        public string roadNumber1 ;
        public string roadNumber2 ;
        public string roadName ;
        public string roadTypeCode ;
        public string roadSuffixCode ;

    }

	public class Included {
		public String type;
		public String id; 
		public Attributes attributes; 
	}
	
	public class Attributes {
	
		public String dwellingType; 
		public String region; 
		public String regionValueDefault; 
		public String externalTerritory; 
		public Address address; 
		public PrimaryAccessTechnology primaryAccessTechnology; 
		public String csaId; 
		public String serviceabilityMessage; 
		public String serviceabilityDate; 
		public String serviceabilityClassReason; 
		public GeoCode geoCode; 
		public String landUse; 
		public String boundaryType; 
		public String status; 
		public String technologyType; 
		public String technologySubType; 
	}
	
	public class Data {
		public String id; 
		public String type; 
		public String boundaryType; 
		public Attributes attributes; 
		public Relationships relationships; 

	}
	
	public class PrimaryAccessTechnology {
		public String technologyType; 
		public String serviceClass; 
		public String rolloutType; 

	}
	
	public class ContainmentBoundaries {
		public List<Data> data; 

	}
	
	public class GeoCode {
		public String latitude; 
		public String longitude; 
		public String geographicDatum; 
		public String srid; 

	}
	
	public class Relationships {
		public ContainmentBoundaries containmentBoundaries; 
		public MDU MDU; 

	}

	public class MDU {
		public Data data; 
	}
}