public class EE_CISLocationSearchHandler extends AsyncQueueableHandler {

	public static final String HANDLER_NAME = 'EE_CISLocationSearchHandler';

    public EE_CISLocationSearchHandler() {
    	// No of params must be 1
        super(EE_CISLocationSearchHandler.HANDLER_NAME, 1);     
    }

    public override void executeWork(List<String> paramList) {    	
    	// Enforce max of 1 input parameter
    	if (paramList.size() == 1) {
    		invokeProcess(paramList[0]);
    	} else {
    		throw new CustomException(AsyncQueueableUtils.ERR_INVALID_PARAMS);
    	}
    }

    private void invokeProcess(String param) {
        EE_CISLocationAPIService.searchLocationAsync(param);
    }

}