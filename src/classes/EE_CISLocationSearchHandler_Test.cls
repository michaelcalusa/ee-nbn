@isTest
public class EE_CISLocationSearchHandler_Test {

	@isTest
	static void test_EE_CIS_LocationHandlerSuccess() {
		/** Data setup **/
		List<Async_Request__c> asyncReqList;
		Map<String, String> addressInputsMap = new Map<String, String>();
		String HANDLER_NAME = EE_CISLocationSearchHandler.HANDLER_NAME;
		String locId = 'LOC000035375038';

		Account acc = DF_TestData.createAccount('Test Acc CIS');
		insert acc;

		DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acc.id);
		insert opptyBundle;
		String sfReqId = DF_LAPI_APIServiceUtils.createServiceFeasibilityRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, locId, null, null, opptyBundle.id, null, addressInputsMap);

		// Dummy params
		String PARAM_1 = sfReqId;

		// Data Setup
		createASRCustomSettings(HANDLER_NAME);

		// Create asyncReq
		Async_Request__c asyncReq1 = DF_TestData.createAsyncRequest(HANDLER_NAME, 'Pending', PARAM_1, null, null, 0, 0);
		insert asyncReq1;

        Map<string, string> headerMap = new  Map<string, string>();
        headerMap.put('Content-Type', 'application/json');

        EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', DF_IntegrationTestData.getCISPositiveJSON(), headerMap);
        
		// Set mock callout class
		Test.setMock(HttpCalloutMock.class, mockObj);

		test.startTest();
		EE_CISLocationSearchHandler handler = new EE_CISLocationSearchHandler();
		handler.run(null);
		handler.executeWork(new List<String> {PARAM_1});
		test.stopTest();

		// Assertions
		DF_SF_Request__c reqRec = [Select ID, status__c from DF_SF_Request__c where ID = :sfReqId ];
		system.AssertEquals('Pending', reqRec.status__c);

	}

	private static void createASRCustomSettings(String handler) {
		Async_Request_Settings__c asrSettings = new Async_Request_Settings__c();

		try {
			asrSettings.Name = handler;
			asrSettings.Max_No_of_Rows__c = 1;
			asrSettings.Max_No_of_Parallels__c = 50;
			insert asrSettings;
		} catch (Exception e) {
			throw new CustomException(e.getMessage());
		}
	}

	/** Data Creation **/
	@testSetup static void setup() {
		try {
			DF_AS_TestService.createNBNIntegrationInputsCustomSettings();
			createASRConfigCustomSettings();
		} catch (Exception e) {
			throw new CustomException(e.getMessage());
		}
	}

	public static void createASRConfigCustomSettings() {
		try {
			Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
			asrConfigSettings.Max_No_of_Future_Calls__c = 50;
			asrConfigSettings.Max_No_of_Parallels__c = 10;
			asrConfigSettings.Max_No_of_Retries__c = 3;
			asrConfigSettings.Max_No_of_Rows__c = 1;
			upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
		} catch (Exception e) {
			throw new CustomException(e.getMessage());
		}
	}
}