global class EE_CSUpgraderUtil implements Database.Batchable<sObject> {


global String query;
global Boolean isUpgrade;
global  Boolean isValidate;
//Consutructor
global EE_CSUpgraderUtil(String query, Boolean isUpgrade, Boolean isValidate) {
this.query = query;
this.isUpgrade = isUpgrade;
this.isValidate = isValidate;
}

global Database.QueryLocator start(Database.BatchableContext bc) {
System.debug('##Query : '+query);
return Database.getQueryLocator(query);

}
global void execute(Database.BatchableContext bc, List<cscfga__Product_Configuration__c> scope) {

System.debug('##BC'+bc);
System.debug('##Scope'+scope);

for(cscfga__Product_Configuration__c pc:scope)
{
System.debug('##Batch Executing for '+pc.Id);
List<Id> configList=new List<Id>();
Set<Id> configSet=new Set<Id>();
configList.add(pc.id);
configSet.add(pc.id);
if(!Test.isRunningTest())
{
    if(isUpgrade)cfgug1.ProductConfigurationUpgrader.upgradeConfigurations(configList);
    if(isValidate)cscfga.ProductConfigurationBulkActions.revalidateConfigurations(configSet);
}
}

}
global void finish(Database.BatchableContext bc) {
}
}