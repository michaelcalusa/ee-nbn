/*************************************************
- Developed by: Ashwani Kaushik
- Date Created: 17/11/2018 (dd/MM/yyyy)
- Description: This class contains test methods to test CloudSense upgrader utility
- Version History:
- v1.0 - 17/11/2018, RS: Created
*/
@isTest(SeeAllData=true) 
public class EE_CSUpgraderUtilTest
{
    static testMethod void testBatchClass() 
    {
       String query = 'select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__r.cscfga__Opportunity__r.RecordType.Name=\'Enterprise Ethernet\' LIMIT 10';
		
       Test.startTest();

            EE_CSUpgraderUtil obj=new EE_CSUpgraderUtil(query,True, True);
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
}