/*************************************************
- Developed by: Ashwani Kaushik
- Date Created: 27/08/2018 (dd/MM/yyyy)
- Description: This class contains key methods for Partner data sharing functionality. At the moment sharing functionality is written for Opportunity 
and DF_Opportunity_Bundle__c objects.
- Version History:
- v1.0 - 27/08/2018, RS: Created
*/
public class EE_DataSharingUtility
{
    public static void addUserToGroup(Id usrId,String grpName)
    {
        GroupMember gm=new GroupMember();
        gm.userorgroupid=usrId;
        Group grp=findGroup(grpName);
        gm.groupid=grp.id;
        insert gm;      
    }
    public static Group findGroup(String grpName)
    {
        List<Group> lstGrp =[SELECT id FROM Group WHERE name=:grpName LIMIT 1];
        Group bsmGroup;
        if(lstGrp.isEmpty()){
            bsmGroup=createNewGroup(grpName);
        }else{
            bsmGroup = lstGrp[0];
        }
        return bsmGroup;
    }
    public static Group createNewGroup(String grpName)
    {
        Group grp=new Group();
        grp.name=grpName;
        insert grp;
        return grp;
    }
    public static void processOppBundleSharingRules(List<DF_Opportunity_Bundle__c> newOppBundleList)
    {
        Set<Id> eeRecordTypeSet=getEERecordTypeSet(DF_Constants.OPPORTUNITY_BUNDLE_OBJECT);
        Set<String> eegroupList=new Set<String>();
        Set<Id> nsRecordTypeSet=getNSRecordTypeSet(DF_Constants.OPPORTUNITY_BUNDLE_OBJECT);
        Map<Id,String> accountMap=getAccountAccessSeeker(newOppBundleList);
        Map<Id,Id> oppBundleWithGroupMap=new Map<Id,Id>();
        Map<String,Id> groupMap=groupMap();
        String groupName;
        
        for(DF_Opportunity_Bundle__c opb:newOppBundleList)
        {
            if(eeRecordTypeSet.contains(opb.recordtypeid))
            {
                groupName=DF_Constants.ENTERPRISE_ETHERNET_GROUP_NAME+accountMap.get(opb.Account__c);
                oppBundleWithGroupMap.put(opb.id,groupMap.get(groupName));
            }else if(nsRecordTypeSet.contains(opb.recordtypeid)){
                groupName=DF_Constants.NBN_SELECT_GROUP_NAME+accountMap.get(opb.Account__c);
                oppBundleWithGroupMap.put(opb.id,groupMap.get(groupName));
            }
        }
        
        try
        {
        if(oppBundleWithGroupMap.size()>0) shareOppBundleWithGroup(oppBundleWithGroupMap,'Edit');
        }Catch(Exception ex) {
        System.debug('EE_DataSharingUtility::processOppBundleSharingRules:EXCEPTION '+ ex.getMessage() + '\n'+ ex.getStackTraceString());
        }
    }
    //@future
    public static void shareOppBundleWithGroup(Map<Id,Id> oppBundleWithGroupMap,String accessType)
    {
        List<DF_Opportunity_Bundle__share> oppBundleShareList =new List<DF_Opportunity_Bundle__share>();
        for(Id oppId:oppBundleWithGroupMap.keySet())
        {
            DF_Opportunity_Bundle__share obs=new DF_Opportunity_Bundle__share();
            obs.userorgroupid=oppBundleWithGroupMap.get(oppId);
            obs.accesslevel=accessType;
            obs.parentid=oppId;
            oppBundleShareList.add(obs);
            System.debug('Sharing Record'+obs);
        }
        System.debug('List'+oppBundleShareList);  

        if(oppBundleShareList.size()>0){
            try{
                Database.insert(oppBundleShareList);
            }catch(Exception ex){
                System.debug('EE_DataSharingUtility::shareOppBundleWithGroup:EXCEPTION: '+ ex.getMessage() + '\n'+ ex.getStackTraceString());
            }
        }
    }
    public static void processOpportunitySharingRules(List<Opportunity> newOppList)
    {
        Set<Id> eeRecordTypeSet=getEERecordTypeSet('Opportunity');
        Set<Id> nsRecordTypeSet=getNSRecordTypeSet('Opportunity');
        Set<String> eegroupList=new Set<String>();
        Map<Id,String> accountMap=getAccountAccessSeeker(newOppList);
        Map<Id,Id> oppWithGroupMap=new Map<Id,Id>();
        Map<String,Id> groupMap=groupMap();
        String groupName;
        
        for(Opportunity opp:newOppList)
        {
            if(eeRecordTypeSet.contains(opp.recordtypeid))
            {
                groupName=DF_Constants.ENTERPRISE_ETHERNET_GROUP_NAME+accountMap.get(opp.accountId);
                oppWithGroupMap.put(opp.id,groupMap.get(groupName));
            }else if(nsRecordTypeSet.contains(opp.recordtypeid)){
                groupName=DF_Constants.NBN_SELECT_GROUP_NAME+accountMap.get(opp.accountId);
                oppWithGroupMap.put(opp.id,groupMap.get(groupName));
            }
        }
        try{
        if(oppWithGroupMap.size()>0) shareOpportunityWithGroup(oppWithGroupMap,'Edit');
        }Catch(Exception ex) {
        System.debug('EE_DataSharingUtility::processOpportunitySharingRules:EXCEPTION '+ ex.getMessage() + '\n'+ ex.getStackTraceString());
        }
    }
    //@future
    public static void shareOpportunityWithGroup(Map<Id,Id> oppWithGroupMap,String accessType)
    {
        List<OpportunityShare> oppShareList =new List<OpportunityShare>();
        for(Id oppId:oppWithGroupMap.keySet())
        {
            OpportunityShare oppshare=new OpportunityShare();
            oppshare.userorgroupid=oppWithGroupMap.get(oppId);
            oppshare.opportunityaccesslevel=accessType;
            oppshare.opportunityid=oppId;
            oppShareList.add(oppshare);
        }
        
        if(oppShareList.size()>0){
            try{
                Database.insert(oppShareList);
            }catch(Exception ex){
                System.debug('EE_DataSharingUtility::shareOpportunityWithGroup:EXCEPTION: '+ ex.getMessage() + '\n'+ ex.getStackTraceString());
            }
        }
    }   
    public static Map<String,Id> groupMap()
    {
        Map<String,Id> groupMap=new Map<String,Id>();
        for(Group grp:[select id,name from group where name like 'BSM_%'])
        {
            groupMap.put(grp.name,grp.id);
        }
        return groupMap;
    }
    public static Set<Id> getEERecordTypeSet(String ObjectType)
    {
        Set<Id> recordTypeSet=new Set<Id>();
        recordTypeSet.add(Schema.getGlobalDescribe().get(ObjectType).getDescribe().getRecordTypeInfosByName().get(DF_Constants.ENTERPRISE_ETHERNET_RECORDTYPE_NAME).getRecordTypeId());
        
        return recordTypeSet;      
        
    }
    public static Set<Id> getNSRecordTypeSet(String strSobjectType){
        Set<Id> recordTypeSet=new Set<Id>();
        recordTypeSet.add(Schema.getGlobalDescribe().get(strSobjectType).getDescribe().getRecordTypeInfosByName().get(DF_Constants.NBN_SELECT_RECORDTYPE_NAME).getRecordTypeId());
        return recordTypeSet;
    }
    public static Map<Id,String> getAccountAccessSeeker(List<DF_Opportunity_Bundle__c> oppBundleList)
    {
        Set<Id> accountIdSet=new Set<Id>();
        Map<Id,String> accountMap=new Map<Id,String>();
        
        for(DF_Opportunity_Bundle__c opb:oppBundleList)
        {
            accountIdSet.add(opb.Account__c);
        }
        for(Account acc:[select id, Access_Seeker_ID__c from account where id IN : accountIdSet])
        {
            accountMap.put(acc.id,acc.Access_Seeker_ID__c);
        }
        return accountMap;       
    }
    public static Map<Id,String> getAccountAccessSeeker(List<Opportunity> oppList)
    {
        Set<Id> accountIdSet=new Set<Id>();
        Map<Id,String> accountMap=new Map<Id,String>();
        
        for(Opportunity opp:oppList)
        {
            accountIdSet.add(opp.Accountid);
        }
        for(Account acc:[select id, Access_Seeker_ID__c from account where id IN : accountIdSet])
        {
            accountMap.put(acc.id,acc.Access_Seeker_ID__c);
        }
        return accountMap;       
    }
    @future
    public static void modifyBsmPublicGroup(String strAccessSeekerId, Id userId){
        String strEE_GroupName, strNS_GroupName;
        User bsmUser = [SELECT Id, Has_EE_Community_Permissions__c, Has_NS_Community_Permissions__c FROM User WHERE Id =:userId LIMIT 1];
        try{
            strEE_GroupName = DF_Constants.ENTERPRISE_ETHERNET_GROUP_NAME+ strAccessSeekerId.trim();
            strNS_GroupName = DF_Constants.NBN_SELECT_GROUP_NAME+ strAccessSeekerId.trim();
            
            if(bsmUser.Has_EE_Community_Permissions__c){
                addUserToGroup(userId, strEE_GroupName);
            }else{
                removeUserFromGroup(userId, strEE_GroupName);
            }
            
            if(bsmUser.Has_NS_Community_Permissions__c){
                addUserToGroup(userId, strNS_GroupName);
            }else{
                removeUserFromGroup(userId, strNS_GroupName);
            }
        }catch(Exception ex){
            System.debug('EE_DataSharingUtility::modifyBsmPublicGroup:EXCEPTION: '+ ex.getMessage() + '\n'+ ex.getStackTraceString());
        }
    }
    public static void removeUserFromGroup(Id usrId, String strGroupName){
        List<GroupMember> lstGrpMem = [SELECT Id, 
                                       GroupId, 
                                       UserOrGroupId 
                                       FROM GroupMember 
                                       WHERE 
                                       (Group.Name = :strGroupName OR Group.DeveloperName = :strGroupName) 
                                       AND UserOrGroupId = :usrId];
        if(!lstGrpMem.isEmpty()){
            Database.delete(lstGrpMem);
        }
    }
}