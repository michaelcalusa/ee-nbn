/*************************************************
- Developed by: Ashwani Kaushik
- Date Created: 07/09/2018 (dd/MM/yyyy)
- Description: This class contains test methods to test Partner data sharing functionality.
- Version History:
- v1.0 - 07/09/2018, RS: Created
*/
@isTest
private class EE_DataSharingUtility_Test
{
    public static Account acc, acc2;
    @testSetup
    static void setupTestData()
    {
        Test.StartTest();
        acc = new Account(recordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId(), Tier__c = '1', Name='test nbn', Access_Seeker_ID__c = 'AS00000011');
        insert acc;
        acc2 = new Account(recordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId(), Tier__c = '1', Name='test nbn', Access_Seeker_ID__c = 'AS00000021');
        insert acc2;
        
        Map<String, String> mapAttributes = new Map<String, String>();
        mapAttributes.put('User.UserRoleId','AS00000011');
        mapAttributes.put('User.FederationIdentifier','testBSM_1');
        mapAttributes.put('User.LastName','nbnRSPTest_1');
        mapAttributes.put('User.FirstName','nbnRSPTest_1');
        mapAttributes.put('User.Email','testuser_1@nbnco.com.au.nbnRsp');
        mapAttributes.put('User.ProfileId','Enterprise Ethernet Plus,Access Seeker nbnSelect Feasibility');
        
        JITHandler handler = new JITHandler();
        //Create User
        
        User u = handler.createUser(null,null,null,'testSAML',mapAttributes,null);
        
        //create another user within the same Access Seeker
        mapAttributes = new Map<String, String>();
        mapAttributes.put('User.UserRoleId','AS00000011');
        mapAttributes.put('User.FederationIdentifier','testBSM_2');
        mapAttributes.put('User.LastName','nbnRSPTest_2');
        mapAttributes.put('User.FirstName','nbnRSPTest_2');
        mapAttributes.put('User.Email','testuser_2@nbnco.com.au.nbnRsp');
        mapAttributes.put('User.ProfileId','Enterprise Ethernet Plus,Access Seeker nbnSelect Feasibility');
        
        handler = new JITHandler();
        //Create User
        
        u = handler.createUser(null,null,null,'testSAML',mapAttributes,null);
        
        //create another user with another Access Seeker and has access to on EE
        mapAttributes = new Map<String, String>();
        mapAttributes.put('User.UserRoleId','AS00000021');
        mapAttributes.put('User.FederationIdentifier','testBSM_3');
        mapAttributes.put('User.LastName','nbnRSPTest_3');
        mapAttributes.put('User.FirstName','nbnRSPTest_3');
        mapAttributes.put('User.Email','testuser_3@nbnco.com.au.nbnRsp');
        mapAttributes.put('User.ProfileId','Enterprise Ethernet Plus');
        
        handler = new JITHandler();
        //Create User
        
        u = handler.createUser(null,null,null,'testSAML',mapAttributes,null);
        Test.StopTest();
        
    }
    @isTest
    static void testShareOpportunityWithGroupPositiveTest()
    {
        Account acc1 = [SELECT Id FROM Account WHERE Access_Seeker_ID__c = 'AS00000011' LIMIT 1];
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_1' LIMIT 1];
        User rspUser2 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_2' LIMIT 1];
        Opportunity opp= DF_TestData.createOpportunity('TestOpportunity');
        opp.accountid=acc1.id;
        System.runAs(rspUser1){
            Test.startTest();
                insert opp;
            Test.stopTest();
        }
        System.runAs(rspUser2){
            List<Opportunity> lstOpp = [SELECT Id FROM Opportunity WHERE Id= :opp.Id];
            System.assertEquals(1, lstOpp.size());
        }
    }
    
    
    
    @isTest
    static void testShareOppBundleWithGroupPositiveTest()
    {
        Account acc1 = [SELECT Id FROM Account WHERE Access_Seeker_ID__c = 'AS00000011' LIMIT 1];
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_1' LIMIT 1];
        User rspUser2 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_2' LIMIT 1];
        DF_Opportunity_Bundle__c oppBundle = DF_TestData.createOpportunityBundle(acc1.Id);
        System.runAs(rspUser1){
            Test.startTest();
                insert oppBundle;
            Test.stopTest();
        }
        System.runAs(rspUser2){
            List<DF_Opportunity_Bundle__c> lstOppBundle = [SELECT Id FROM DF_Opportunity_Bundle__c WHERE Id= :oppBundle.Id];
            System.assertEquals(1, lstOppBundle.size());
        }
    }
    
    @isTest
    static void testShareNSOpportunityWithGroupPositiveTest()
    {
        Account acc1 = [SELECT Id FROM Account WHERE Access_Seeker_ID__c = 'AS00000011' LIMIT 1];
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_1' LIMIT 1];
        User rspUser2 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_2' LIMIT 1];
        Opportunity opp= DF_TestData.createOpportunity('TestOpportunity');
        opp.accountid=acc1.id;
        opp.RecordTypeId = DF_CS_API_Util.getRecordType('NBN Select','Opportunity');
        System.runAs(rspUser1){
            Test.startTest();
                insert opp;
            Test.stopTest();
        }
        System.runAs(rspUser2){
            List<Opportunity> lstOpp = [SELECT Id FROM Opportunity WHERE Id= :opp.Id];
            System.assertEquals(1, lstOpp.size());
        }
    }
    
    @isTest
    static void testShareNSOppBundleWithGroupPositiveTest()
    {
        Account acc1 = [SELECT Id FROM Account WHERE Access_Seeker_ID__c = 'AS00000011' LIMIT 1];
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_1' LIMIT 1];
        User rspUser2 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_2' LIMIT 1];
        DF_Opportunity_Bundle__c oppBundle = DF_TestData.createOpportunityBundle(acc1.Id);
        oppBundle.RecordTypeId = DF_CS_API_Util.getRecordType('NBN Select',DF_Constants.OPPORTUNITY_BUNDLE_OBJECT);
        System.runAs(rspUser1){
            Test.startTest();
                insert oppBundle;
            Test.stopTest();
        }
        System.runAs(rspUser2){
            List<DF_Opportunity_Bundle__c> lstOppBundle = [SELECT Id FROM DF_Opportunity_Bundle__c WHERE Id= :oppBundle.Id];
            System.assertEquals(1, lstOppBundle.size());
        }
    }
    
    /****/
    @isTest
    static void testShareOpportunityWithGroupNegativeTest()
    {
        Account acc1 = [SELECT Id FROM Account WHERE Access_Seeker_ID__c = 'AS00000011' LIMIT 1];
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_1' LIMIT 1];
        User rspUser3 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_3' LIMIT 1];
        Opportunity opp= DF_TestData.createOpportunity('TestOpportunity');
        opp.accountid=acc1.id;
        System.runAs(rspUser1){
            Test.startTest();
                insert opp;
            Test.stopTest();
        }
        System.runAs(rspUser3){
            List<Opportunity> lstOpp = [SELECT Id FROM Opportunity WHERE Id= :opp.Id];
            System.assertEquals(0, lstOpp.size());
        }
    }
    
    
    
    @isTest
    static void testShareOppBundleWithGroupNegativeTest()
    {
        Account acc1 = [SELECT Id FROM Account WHERE Access_Seeker_ID__c = 'AS00000011' LIMIT 1];
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_1' LIMIT 1];
        User rspUser3 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_3' LIMIT 1];
        DF_Opportunity_Bundle__c oppBundle = DF_TestData.createOpportunityBundle(acc1.Id);
        System.runAs(rspUser1){
            Test.startTest();
                insert oppBundle;
            Test.stopTest();
        }
        System.runAs(rspUser3){
            List<DF_Opportunity_Bundle__c> lstOppBundle = [SELECT Id FROM DF_Opportunity_Bundle__c WHERE Id= :oppBundle.Id];
            System.assertEquals(0, lstOppBundle.size());
        }
    }
    
    @isTest
    static void testShareNSOpportunityWithGroupNegativeTest()
    {
        Account acc1 = [SELECT Id FROM Account WHERE Access_Seeker_ID__c = 'AS00000011' LIMIT 1];
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_1' LIMIT 1];
        User rspUser3 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_3' LIMIT 1];
        Opportunity opp= DF_TestData.createOpportunity('TestOpportunity');
        opp.accountid=acc1.id;
        opp.RecordTypeId = DF_CS_API_Util.getRecordType('NBN Select','Opportunity');
        System.runAs(rspUser1){
            Test.startTest();
                insert opp;
            Test.stopTest();
        }
        System.runAs(rspUser3){
            List<Opportunity> lstOpp = [SELECT Id FROM Opportunity WHERE Id= :opp.Id];
            System.assertEquals(0, lstOpp.size());
        }
    }
    
    @isTest
    static void testShareNSOppBundleWithGroupNegativeTest()
    {
        Account acc1 = [SELECT Id FROM Account WHERE Access_Seeker_ID__c = 'AS00000011' LIMIT 1];
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_1' LIMIT 1];
        User rspUser3 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_3' LIMIT 1];
        DF_Opportunity_Bundle__c oppBundle = DF_TestData.createOpportunityBundle(acc1.Id);
        oppBundle.RecordTypeId = DF_CS_API_Util.getRecordType('NBN Select',DF_Constants.OPPORTUNITY_BUNDLE_OBJECT);
        System.runAs(rspUser1){
            Test.startTest();
                insert oppBundle;
            Test.stopTest();
        }
        System.runAs(rspUser3){
            List<DF_Opportunity_Bundle__c> lstOppBundle = [SELECT Id FROM DF_Opportunity_Bundle__c WHERE Id= :oppBundle.Id];
            System.assertEquals(0, lstOppBundle.size());
        }
    }
}