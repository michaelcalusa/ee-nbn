/***************************************************
- Developed by: Ashwani Kaushik
- Date Created: 03/10/2018 (dd/MM/yyyy)
- Description: This class contains method to disable user if licence change scenario occurs in JIT Handler
- Version History:
- v1.0 - 03/10/2018, RS: Created
*/
global class EE_JITHandlerUtility
{
   
    public static void disableUser(String userId)
    {
        
        User u=[select IsActive,IsPortalEnabled,federationIdentifier,communityNickName,alias,contactId from user where id=:userId LIMIT 1];
        
        if(u.IsActive && u.IsPortalEnabled )
            {
                u.IsActive = false;
                u.IsPortalEnabled = false;
                u.federationIdentifier='';
                u.communityNickName=String.ValueOf(Crypto.getRandomInteger());
                update u;
                  
            }
        

    
    }
    
    public static void disableUserEvent(String userId)
    {
        List<JIT_Handler_Events__e> EventListToPublish=new List<JIT_Handler_Events__e>();
        JIT_Handler_Events__e e=new JIT_Handler_Events__e();
        e.userId__c=userId;
        EventListToPublish.add(e);
        
        if(EventListToPublish.size()>0){
            List<Database.SaveResult> results = EventBus.publish(eventListToPublish);
            for(Database.SaveResult sr : results) {
              if (sr.isSuccess()) {
              System.debug('Successfully published event.');
              } 
              else {
              for(Database.Error err : sr.getErrors()) {
              System.debug('Error returned: ' +  err.getStatusCode() +  ' - ' + err.getMessage());
              }
             }
          } 
      } 
    }
    
    public static boolean isLicenceChanged(String oldProfile, String newProfile)
    {
        boolean isLicenceChange=false;
        Set<String> fromLicence=new Set<String>();
        Set<String> toLicence=new Set<String>();
        
        //Add profile ids to list to query profile records
        List<String> profileList=new List<String>();
        profileList.add(oldProfile);
        profileList.add(newProfile);
        Map<Id,String> profileLicenceMap=new Map<Id,String>();
        
        for(Profile p:[select id, UserLicense.Name from profile where id IN : profileList])
        {
            profileLicenceMap.put(p.id,p.UserLicense.Name);
        }
        
        if(profileLicenceMap.get(oldProfile) != profileLicenceMap.get(newProfile))
        {
            for(JITLicenceMapping__mdt m:[select fromLicence__c,tolicence__c from JITLicenceMapping__mdt])
            {
                fromLicence.add(m.fromLicence__c);
                toLicence.add(m.tolicence__c);
            }
            
            if(fromLicence.contains(profileLicenceMap.get(oldProfile)) && toLicence.contains(profileLicenceMap.get(newProfile)))
            {
                isLicenceChange=true;
            }
        }
        return isLicenceChange;
    }
 
 }