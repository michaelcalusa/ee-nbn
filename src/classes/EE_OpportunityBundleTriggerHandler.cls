/*************************************************
- Developed by: Ashwani Kaushik
- Date Created: 27/08/2018 (dd/MM/yyyy)
- Description: Trigger Handler to handle all the events by DF_Opportunity__c trigger.
- Version History:
- v1.0 - 27/08/2018, RS: Created
*/

public class EE_OpportunityBundleTriggerHandler
{
public Boolean TriggerDisabled = false;
 
    /*
        Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        EE_DisableTriggers__c cs = EE_DisableTriggers__c.getOrgDefaults();
        
        return cs.DisableOpportunityBundleTrigger__c;
    }
    public void run()
    {
        if(Trigger.IsAfter)
            {
                if(Trigger.IsInsert)    afterInsertHandler(Trigger.new);
                
            } 
    }
    public void afterInsertHandler(List<DF_Opportunity_Bundle__c> newOppBundleList)
    {
        EE_DataSharingUtility.processOppBundleSharingRules(newOppBundleList);
        
    }
}