/*------------------------------------------------------------  
  Description:   	Queue handler class for Order Inflight Cancellation 

  Test Class:		EE_OrderInflightCancelHandlerTest

  ------------------------------------------------------------*/

public class EE_OrderInflightCancelHandler implements Queueable {

	private List<DF_Order_Event__e> eeOrderEvents;

	public EE_OrderInflightCancelHandler(List<DF_Order_Event__e> events) {     
		eeOrderEvents = events;
	}

	public void execute(QueueableContext context) {
		if(eeOrderEvents != null && !eeOrderEvents.isEmpty()) {
			EE_OrderInflightCancelService.processEEOrderInflightCancellation(eeOrderEvents);
		}
	}
}