/*------------------------------------------------------------  
  Description:   	Service class for Order Inflight Cancellation 

  Test Class:		EE_OrderInflightCancelServiceTest

  ------------------------------------------------------------*/
public class EE_OrderInflightCancelService {

	private static final String STR_ERROR = 'Error';
	private static final String STR_EMPTY = '';
	
	private static String payload = STR_EMPTY;
	private static String message = STR_EMPTY;
	private static String sourceFunction = STR_EMPTY;
	private static final String logLevel = STR_ERROR;
	private static final String sourceClass = EE_OrderInflightCancelService.class.getName();
	
	private static final String STR_FIBRE = 'Fibre';
	private static final String STR_CANCELLED = 'Cancelled';
	private static final String STR_QTY_1 = '1.0';
	private static final String STR_ORDER_CANCELLATION_CHARGES = 'Order Cancellation Charges';
	private static final String FIELD_EVENT_RECORD_ID = 'Event_Record_Id__c';
	private static final String FIELD_DF_QUOTE_GUID = 'DF_Quote__c.GUID__c';
	private static final String FIELD_ID = 'Id';
	private static final String FORMAT_INTERACTION_DATE_TIME = 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'';

	public static final String ORDER_STATUS_CANCELLED = 'Cancelled';

	private static Map<String, DF_Order_Event__e> dfOrderEventsByRecordId {get; set;}
	private static Map<Id, String> eeOrderDFQuoteGuid {get; set;}
	private static Map<String, DF_Order__c> dfQuoteGuidEEOrders {get; set;}


	/**
	 * Process Order Inflight Cancel
	 *
	 * @param	  String 	eeOrdersId - SFDC record ID
	 *
	 * @return	 void
	*/
	public static void cancelEEOrderByOrderId(String eeOrderId) {  
		EE_OrderInflightCancelUtil.sendCancelRequest(eeOrderId);
	}


	/**
	* @description Check If the Order is Cancellable
	* @param orderId - Salesforce record ID of an DF_Order Record, today date
	* @return 
	*/ 

	public static Boolean isCancellableOrder(string orderId, Date todayDate) {

		Boolean isCancellable =false;
		List<String> eeOrdersId = new List<String>();
		List<DF_Order__c> orderList = new List<DF_Order__c>();
		Integer businessDaysInt = 0;

		if(String.isNotBlank(orderId)) {
			eeOrdersId.add(orderId);
			orderList = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);
		}
		
		if(orderList.size() >0) {

			DF_Order__c orderRec = orderList.get(0);
			String orderStatus = orderRec.Order_Status__c;
			String orderSubStatus = orderRec.Order_Sub_Status__c;
			Date nbnCommitedDate = orderRec.NBN_Commitment_Date__c;
			Date maxCancelDate = nbnCommitedDate;
		
			if(maxCancelDate!=null) {
				
		
			Date ccdEndDate = nbnCommitedDate.addDays(-1);
			DF_AS_Custom_Settings__c settings  = DF_AS_Custom_Settings__c.getValues('Order_Cancel_Business_Days');

			if(settings!=null) {
				businessDaysInt= Integer.valueOf(settings.Value__c);
			}

			do {
				maxCancelDate = maxCancelDate.addDays(-1);
			} while (!(DF_AS_GlobalUtility.getNumberOfBusinessDaysBetween(maxCancelDate,ccdEndDate)>=businessDaysInt));

			System.debug('maxCancelDate is' + maxCancelDate);
			System.debug('todayDate is' + todayDate);
		}
		
		if(DF_Constants.DF_ORDER_STATUS_ACKNOWLEDGED.equals(orderStatus)) {
			isCancellable = true;
		} else if(DF_Constants.DF_ORDER_STATUS_IN_PROGRESS.equals(orderStatus)) {
			if(DF_Constants.DF_ORDER_SUB_STATUS_ORDER_SCHEDULED.equals(orderSubStatus) 
				|| DF_Constants.DF_ORDER_SUB_STATUS_CONSTRUCTION_STARTED.equals(orderSubStatus) 
				|| DF_Constants.DF_ORDER_SUB_STATUS_SERVICE_TEST_COMPLETED.equals(orderSubStatus)) {
					if(maxCancelDate!=null && todayDate < maxCancelDate ) {
						isCancellable = true;
					} else if (nbnCommitedDate==null) {
						sourceFunction = 'isCancellableOrder';
						message = 'NBN Commited Date not available for Order which is scheduled for Delivery';
						payload = null;
						GlobalUtility.logMessage(logLevel, sourceClass, sourceFunction, orderId, orderStatus, message, payload, null, 0);
					}
				} else {
					isCancellable = true;
				}
			}
		}
		return isCancellable;
	}

	/**
	 * Process cancel order notification from Appian
	 *
	 * @param	  List<DF_Order_Event__e> eeOrderEvents
	 *
	 * @return	 void
	*/
	public static void processEEOrderInflightCancellation(List<DF_Order_Event__e> eeOrderEvents) {

		try {

			if(eeOrderEvents != null && !eeOrderEvents.isEmpty()) {
				
				EE_OrderInflightCancelService.dfOrderEventsByRecordId = new Map<String, DF_Order_Event__e>();

				for(DF_Order_Event__e eeOrderEvent : eeOrderEvents) {
					String dfQuoteGuid = eeOrderEvent.Event_Record_Id__c;
					EE_OrderInflightCancelService.dfOrderEventsByRecordId.put(dfQuoteGuid, eeOrderEvent);
				}

				Map<Id, DF_Order__c> eeOrdersByIdAll = (EE_OrderInflightCancelService.dfOrderEventsByRecordId != null && !EE_OrderInflightCancelService.dfOrderEventsByRecordId.isEmpty()) 
														? EE_OrderInflightCancelUtil.getEEOrdersByQuoteGUID(new List<String>(EE_OrderInflightCancelService.dfOrderEventsByRecordId.keySet()))
														: null;
				Map<Id, DF_Order__c> eeOrdersById = (eeOrdersByIdAll != null && !eeOrdersByIdAll.isEmpty())
														? EE_OrderInflightCancelUtil.getDfOrdersToChargeById(eeOrdersByIdAll.values())
														: null;
                
                List<String> opportunityIds = !eeOrdersByIdAll.isEmpty()? EE_OrderInflightCancelUtil.getFieldValues(eeOrdersByIdAll.values(), 'Opportunity__c', 'DF_Quote__r') : null;
                cancellOpportunitiesServicesSubscriptions(opportunityIds);
				if(eeOrdersById != null && !eeOrdersById.isEmpty()) {
					EE_OrderInflightCancelService.dfQuoteGuidEEOrders = new Map<String, DF_Order__c>();
					EE_OrderInflightCancelService.eeOrderDFQuoteGuid = new Map<Id, String>();

					for(DF_Order__c eeOrder : eeOrdersById.values()) {
						String dfQuoteGUID = eeOrder.DF_Quote__r.GUID__c;
						if(String.isNotBlank(dfQuoteGUID)) {
							EE_OrderInflightCancelService.dfQuoteGuidEEOrders.put(dfQuoteGUID, eeOrder);
							EE_OrderInflightCancelService.eeOrderDFQuoteGuid.put(eeOrder.Id, dfQuoteGUID);
						}
					}
				}
				processEEOrdersInflightCancel(eeOrderEvents, eeOrdersById);
				processEEOrdersInflightCancelNoCharge(eeOrdersByIdAll, eeOrdersById);
			}
		} catch (Exception e) {
			System.debug('Exception   ' + e.getMessage());
			sourceFunction = 'processEEOrderInflightCancellation';
			message = 'Error in processing notification from Appian';
			payload = 'TODO';
			GlobalUtility.logMessage(logLevel, sourceClass, sourceFunction, null, null, message, payload, e, 0);
		}
	}

	/**
	 * 
	 * 
	 */
	@future(callout=true)
	public static void processCancellationOpportunity(Id eeOrderId, Id opportunityId, 
														String inflightStage, Decimal fbcCharge,
														String orderId, String interactionStatus, 
														String interactionDateTime, String billingId) {

		createCancellationProductBasket(opportunityId, inflightStage, fbcCharge);
		
		closeCancellationChargeOpportunity(opportunityId);
		
		EE_OrderInflightCancel eeOrderInflightCancel = getEEOrdersInflightCancelDetails(opportunityId, (String)eeOrderId, orderId, interactionStatus, interactionDateTime, billingId);
		String soaCancelOrderRequest = getSOACancelOrderRequest(eeOrderInflightCancel);
		updateEEOrder(eeOrderId, soaCancelOrderRequest);
		
		sendEmail(eeOrderId, DF_Constants.TEMPLATE_ORDER_CANCELLED);
	}
	
	public static void processEEOrdersInflightCancelNoCharge(Map<Id, DF_Order__c> eeOrdersByIdAll, Map<Id, DF_Order__c> eeOrdersById) {

		Map<Id, DF_Order__c> eeOrdersNoChargeById = (eeOrdersByIdAll != null && !eeOrdersByIdAll.isEmpty() && eeOrdersById != null)
																? EE_OrderInflightCancelUtil.getEEOrdersToNotChargeById(eeOrdersByIdAll, eeOrdersById)
																: null;
		
		if(eeOrdersNoChargeById != null && !eeOrdersNoChargeById.isEmpty()) {
			List<DF_Order__c> eeOrders = eeOrdersNoChargeById.values();
			for(DF_Order__c eeOrder : eeOrders) {
				eeOrder.Order_Cancel_Charges_JSON__c = Label.DF_Order_Cancel_No_Charge;
				eeOrder.Order_Status__c = ORDER_STATUS_CANCELLED;
			}
			UPDATE eeOrders;

			sendEmail(eeOrders, DF_Constants.TEMPLATE_ORDER_CANCELLED);
		}
	}


	/**
	 * Process EE Order Inflight cancellation
	 *
	 * @param	 List<DF_Order_Event__e> eeOrderEvents
	 * @param	 Map<String,DF_Order__c> eeOrdersById
	 *
	*/
	public static void processEEOrdersInflightCancel(List<DF_Order_Event__e> eeOrderEvents, Map<Id, DF_Order__c> eeOrdersById) {

		if(eeOrdersById != null && !eeOrdersById.isEmpty()) {
			List<DF_Order__c> eeOrders = eeOrdersById.values();
			Map<Id, Id> orderIdToOpportunityId = createCancelOpportunityForOrder(eeOrders);
			if(orderIdToOpportunityId != null && !orderIdToOpportunityId.isEmpty()) {
				Set<Id> eeOrdersId = orderIdToOpportunityId.keySet();
				for(Id eeOrderId : eeOrdersId) {
					if(eeOrdersById.containsKey(eeOrderId) 
						&& EE_OrderInflightCancelService.eeOrderDFQuoteGuid.containsKey(eeOrderId)) {
						DF_Order__c eeOrder = eeOrdersById.get(eeOrderId);
						String dfQuoteGUID = EE_OrderInflightCancelService.eeOrderDFQuoteGuid.get(eeOrderId);
						DF_Order_Event__e eeOrderEvent = EE_OrderInflightCancelService.dfOrderEventsByRecordId.get(dfQuoteGUID);
						
						DF_OrderInboundEventHandler.OrderResponse appianNotification = (DF_OrderInboundEventHandler.OrderResponse)JSON.deserialize(eeOrderEvent.Message_Body__c, DF_OrderInboundEventHandler.OrderResponse.class);
                    	String interactionStatus = appianNotification.body.manageProductOrderNotification.productOrder.interactionStatus;
                    	String interactionDateTime = appianNotification.body.manageProductOrderNotification.productOrder.interactionDateComplete;
						
						interactionDateTime = (String.isNotBlank(interactionDateTime)) ? interactionDateTime : Datetime.now().formatGmt(FORMAT_INTERACTION_DATE_TIME); 
                    
                    	// If interactionStatus is null, set to cancelled
                    	interactionStatus = (String.isNotBlank(interactionStatus)) ? interactionStatus: STR_CANCELLED;

						if(String.isNotBlank(dfQuoteGUID)) {

							processCancellationOpportunity(eeOrderId, 
																orderIdToOpportunityId.get(eeOrderId), 
																eeOrder.Cancel_Initiated_Stage__c, 
																eeOrder.DF_Quote__r.Fibre_Build_Cost__c, 
																eeOrder.Order_Id__c,
																interactionStatus, interactionDateTime,
																eeOrder.Opportunity_Bundle__r.Account__r.Billing_ID__c);
						} else {
							// TODO :::: LOG
						}
					} else {
						// TODO: LOG
					}
				}
			}
		}
	}

	/**
	 * Get the Interaction Date, Interaction Status, Order Id and SF Id
	 *
	 * @param	 List<DF_Order_Event__e> eeOrderEvents
	 * @param	 Map<String,sObject> dfQuoteGuidEEOrders
	 *
	 * @return	 Map<String,EE_OrderInflightCancel>
	*/
	public static EE_OrderInflightCancel getEEOrdersInflightCancelDetails(Id opportunityId, String eeOrderId, String orderId, 
																			String interactionStatus, String interactionDateTime,
																			String billingId) {
		
		System.debug('### opportunityId: ' + opportunityId);

		EE_OrderInflightCancel eeOrderInflightCancel = null;

		try {
			
			if(opportunityId != null) {
                
                // Create a new Cancel notification
                eeOrderInflightCancel = new EE_OrderInflightCancel(eeOrderId, orderId, interactionDateTime, interactionStatus, billingId);
                
                List<csord__Order__c> cancellationOrders = 
				[
					SELECT 
						Name, 
						csordtelcoa__Opportunity__r.Commercial_Deal__r.Name, 
						csordtelcoa__Opportunity__r.Commercial_Deal__r.End_Date__c, 
						csordtelcoa__Opportunity__r.Account.Name,
						(
							SELECT 
								Name,
								csord__Total_Price__c, 
								csord__Line_Description__c 
							FROM csord__Order_Line_Items__r
						) 
                    FROM csord__Order__c 
                    WHERE csordtelcoa__Opportunity__c = :opportunityId and
					csord__Primary_Order__c!= NULL and
					csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.Name='Direct Fibre - Inflight Build Cancellation Charge'
					LIMIT 1
                ];

				if(!cancellationOrders.isEmpty() && !cancellationOrders.get(0).csord__Order_Line_Items__r.isEmpty()){
                    
                    for(csord__Order_Line_Item__c oli: cancellationOrders.get(0).csord__Order_Line_Items__r){
						System.debug('### oli: ' + oli);
						System.debug('### oli.csord__Line_Description__c: ' + oli.csord__Line_Description__c);

                        if(oli.csord__Line_Description__c.containsIgnoreCase('Cancellation Charge')){
                            String[] arrStr = oli.csord__Line_Description__c.split('-');
                            eeOrderInflightCancel.cancellationCode = !arrStr.isEmpty()? arrStr.get(1).trim() : Null;
                            eeOrderInflightCancel.cancellationFee = oli.csord__Total_Price__c;
                        }
						else if(oli.csord__Line_Description__c.containsIgnoreCase('Fibre Build Charge')){
                            String[] arrStr = oli.csord__Line_Description__c.split('-');
                            eeOrderInflightCancel.fibreBuildCode = !arrStr.isEmpty()? arrStr.get(1).trim() : Null;
                            eeOrderInflightCancel.fibreBuildCost = oli.csord__Total_Price__c;
                        }
						else if(oli.csord__Line_Description__c.containsIgnoreCase('DBP Cancellation Discount')){
                            eeOrderInflightCancel.cancellationDiscount = oli.csord__Total_Price__c;
                        }
						else if(oli.csord__Line_Description__c.containsIgnoreCase('FBC Discount')){
                            eeOrderInflightCancel.fibreBuildDiscount = oli.csord__Total_Price__c;
                        } 
                    }
                    eeOrderInflightCancel.dealId = cancellationOrders.get(0).csordtelcoa__Opportunity__r.Commercial_Deal__c <> Null? cancellationOrders.get(0).csordtelcoa__Opportunity__r.Commercial_Deal__r.Name: Null;
                    eeOrderInflightCancel.dealName = cancellationOrders.get(0).csordtelcoa__Opportunity__r.Commercial_Deal__c <> Null? cancellationOrders.get(0).csordtelcoa__Opportunity__r.Account.Name: Null;
                    eeOrderInflightCancel.dealEndDate = cancellationOrders.get(0).csordtelcoa__Opportunity__r.Commercial_Deal__c <> Null? string.valueOf(cancellationOrders.get(0).csordtelcoa__Opportunity__r.Commercial_Deal__r.End_Date__c) : Null;
                }
			}
		} catch (Exception e) {
			sourceFunction = 'createEEOrdersInflightCancelDetails';
			message = 'Error in processing notification from Appian';
			payload = 'TODO';
			GlobalUtility.logMessage(logLevel, sourceClass, sourceFunction, null, null, message, payload, e, 0);			
		}

		return eeOrderInflightCancel;
	}

	/**
	 * Method to create Cancellation Opportunity
	 * Returns True
	 */
	public static Map<Id, Id> createCancelOpportunityForOrder(List<DF_Order__c> dfOrders){
		List<Opportunity> cancellationOpportunities = new List<Opportunity>();
		Map<Id, Id> orderOpportunityIDMap = new Map<Id, Id>();
		Integer index = 0;
		for(DF_Order__c dfOrder :dfOrders){
            Opportunity cancellationOpp = new Opportunity(Name = 'Cancellation Charge',
					CloseDate = Date.today().addDays(20),
					StageName = 'New',
					RecordTypeId = DF_CS_API_Util.getRecordType('Enterprise Ethernet', 'Opportunity'),
					AccountId = dfOrder.Opportunity_Bundle__r.Account__c,
					Parent_Opportunity__c = dfOrder.DF_Quote__r.Opportunity__c,
					Commercial_Deal__c  = dfOrder.DF_Quote__r.Opportunity__r.Commercial_Deal__c);
			cancellationOpportunities.add(cancellationOpp);
		}
		Try{
			Database.SaveResult[] srList = Database.insert(cancellationOpportunities, false);
			for (Database.SaveResult sr : srList) {				
				if (sr.isSuccess()) {
					orderOpportunityIDMap.put(dfOrders.get(index).Id, sr.getId());				
				}
				index++;
			}			
		}catch (Exception e) {  
			GlobalUtility.logMessage(DF_Constants.ERROR, EE_OrderInflightCancelUtil.class.getName(), '', '', '', e.getMessage(), '', e, 0);
			throw new CustomException(e.getMessage());
		}
		return orderOpportunityIDMap;
	}

	


	public static void createCancellationProductBasket(Id opportunityId, String inflightStage, Decimal fbcCharge) {
		String prodDefId = DF_AS_Custom_Settings__c.getValues('SF_INFLIGHT_CANCELLATION_DEFINITION_ID').Value__c;
		String offerId =  DF_AS_Custom_Settings__c.getValues('SF_INFLIGHT_CANCELLATION_OFFER_ID').Value__c;
		String categoryID = DF_CS_API_Util.getCustomSettingValue('SF_CATEGORY_ID');
		
		System.debug('@@@ prodDefId:' + prodDefId);
		System.debug('@@@ offerId:' + offerId);
		System.debug('@@@ categoryID:' + categoryID);

		cscfga.API_1.ApiSession apisession = DF_CS_API_Util.createApiSession(NULL);
		System.debug('@@@ apisession:' + apisession);
		cscfga__Product_Basket__c basket = apiSession.getBasket();
		System.debug('@@@ basket:' + basket);

		DF_CS_API_Util.associateBasketWithOppty(basket.Id, opportunityId);
		apiSession.setProductToConfigure(new cscfga__Product_Definition__c(Id = prodDefId), new Map<String, String> {'containerType' => 'basket', 'linkedId' => basket.Id});
		cscfga.ProductConfiguration productConfig = apiSession.getConfiguration();

		System.debug('@@@ productConfig:' + productConfig);
		
		//Get and set inflight Stage attribute
		cscfga.Attribute inflightstageattribute = productConfig.getAttribute('InflightStage');
		inflightstageattribute.setValue(inflightStage);
		
		System.debug('@@@ inflightstageattribute:' + inflightstageattribute); 

		//Get and Set FBC charge attribute
		if(fbcCharge > 0){
			cscfga.Attribute fbcChargeAttribute = productConfig.getAttribute('FBC_Charge');
			fbcChargeAttribute.setValue(String.valueOf(fbcCharge));			
			System.debug('@@@ fbcChargeAttribute:' + fbcChargeAttribute); 
		}

		apiSession.executeRules();
		cscfga.ValidationResult vr = apiSession.validateConfiguration();
		System.debug('@@@ vr:' + vr); 

		cscfga.Attribute totalChargeAttribute = productConfig.getAttribute('Total_Cancellation_Charge');
		System.debug('@@@ totalChargeAttribute:' + totalChargeAttribute); 

		String CancellationCharge = totalChargeAttribute.getValue();
		basket = apiSession.persistConfiguration(true);
		insert new csbb__Product_Configuration_Request__c(csbb__Product_Basket__c = basket.Id,
														  csbb__Product_Configuration__c = productConfig.getId(),
														  csbb__Offer__c = offerId,
													   	  csbb__Optionals__c = '{"selectedAddressDisplay":null}',
														  csbb__Product_Category__c = categoryID,
														  csbb__Status__c='finalized');
        DF_CS_API_Util.syncWithOpportunity(basket.Id);
	}
	
	


	/**
	 * Method to build JSON that is sent to BRM
	 *
	 * @param	 Map<String,EE_OrderInflightCancel> eeOrdersInflightCancel
	 * 
	 * @return	 Map<String, String> eeOrdersSOACancelOrderRequest
	*/
	public static String getSOACancelOrderRequest(EE_OrderInflightCancel eeOrderInflightCancel) {
		
		String soaCancelOrderRequest = null;
				
		try {

			if(eeOrderInflightCancel != null) {

				if(proceedToBuildCharges(eeOrderInflightCancel)) {

					String orderId = (eeOrderInflightCancel != null && String.isNotBlank(eeOrderInflightCancel.orderId)) ? eeOrderInflightCancel.orderId : null;
					String sfId = (eeOrderInflightCancel != null && String.isNotBlank(eeOrderInflightCancel.sfId)) ? eeOrderInflightCancel.sfId : null;
					String interactionStatus = (eeOrderInflightCancel != null && String.isNotBlank(eeOrderInflightCancel.interactionStatus)) 
												? eeOrderInflightCancel.interactionStatus
												: STR_CANCELLED;
					String interactionDateComplete = (eeOrderInflightCancel != null && String.isNotBlank(eeOrderInflightCancel.interactionDateTime))
													? eeOrderInflightCancel.interactionDateTime 
													: Datetime.now().formatGmt(FORMAT_INTERACTION_DATE_TIME);
					
					DF_SOAOrderDataJSONWrapper ssSOAClass = new DF_SOAOrderDataJSONWrapper();
				
					DF_SOAOrderDataJSONWrapper.ManageBillingEventRequest ssMR = new DF_SOAOrderDataJSONWrapper.ManageBillingEventRequest();
					ssMR.notificationType = DF_Constants.NOTIFICATION_TYPE;

					DF_SOAOrderDataJSONWrapper.ItemInvolvesProduct ssIIP = new DF_SOAOrderDataJSONWrapper.ItemInvolvesProduct();
					ssIIP.id = orderId;
					ssIIP.accessServiceTechnologyType = STR_FIBRE;

					DF_SOAOrderDataJSONWrapper.ProductOrderComprisedOf ssPC = new DF_SOAOrderDataJSONWrapper.ProductOrderComprisedOf();
					ssPC.itemInvolvesProduct = ssIIP;
					
					DF_SOAOrderDataJSONWrapper.ProductOrder ssPO = new DF_SOAOrderDataJSONWrapper.ProductOrder();
					ssPO.orderType = DF_Constants.ORDER_TYPE_CONNECT;					
					ssPO.interactionStatus = interactionStatus;
					ssPO.interactionDateComplete = interactionDateComplete;
					ssPO.id = orderId;
					ssPO.SFid = sfId;
					ssPO.productOrderComprisedOf = ssPC;
                    if(eeOrderInflightCancel.dealId <> Null){
                        ssPO.commercialDealId = eeOrderInflightCancel.dealId;
                        ssPO.commercialDealName = eeOrderInflightCancel.dealName;
                        ssPO.commercialDealEndDate = eeOrderInflightCancel.dealEndDate;
                    }
					String billingAccountId = eeOrderInflightCancel != null ? eeOrderInflightCancel.billingAccountId : null;

					DF_SOAOrderDataJSONWrapper.AccessSeekerInteraction ssASI = new DF_SOAOrderDataJSONWrapper.AccessSeekerInteraction();
					ssASI.billingAccountId = billingAccountId;
					ssPO.accessSeekerInteraction = ssASI;

					List<DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge> ssOCList = new List<DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge>();
					
					String eeOrderCancellationCode = (eeOrderInflightCancel != null && String.isNotBlank(eeOrderInflightCancel.cancellationCode))
														? eeOrderInflightCancel.cancellationCode 
														: null;
					Decimal eeOrderCancellationFee = (eeOrderInflightCancel != null && eeOrderInflightCancel.cancellationFee != null)
														? eeOrderInflightCancel.cancellationFee 
														: null;
                    DF_SOAOrderDataJSONWrapper.DiscountType cancelDiscount;
                    if(eeOrderInflightCancel.cancellationDiscount < 0){
                        cancelDiscount = new DF_SOAOrderDataJSONWrapper.DiscountType();
						cancelDiscount.type = 'PSD';
                        cancelDiscount.amount = string.valueOf(eeOrderInflightCancel.cancellationDiscount);
                    }
					                  
					DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge cancellationData = (String.isNotBlank(eeOrderCancellationCode) && eeOrderCancellationFee != null)
																									? getData(orderId, eeOrderCancellationCode, eeOrderCancellationFee, cancelDiscount)
																									: null;

					String eeOrderFibreBuildCode = (eeOrderInflightCancel != null && String.isNotBlank(eeOrderInflightCancel.fibreBuildCode))
														? eeOrderInflightCancel.fibreBuildCode 
														: null;

					Decimal eeOrderFibreBuildCost = (eeOrderInflightCancel != null && eeOrderInflightCancel.fibreBuildCost != null)
														? eeOrderInflightCancel.fibreBuildCost 
														: null;
                    DF_SOAOrderDataJSONWrapper.DiscountType fBiscount;
                    if(eeOrderInflightCancel.fibreBuildDiscount < 0){
                        fBiscount = new DF_SOAOrderDataJSONWrapper.DiscountType();
						fBiscount.type = 'PSD';
                        fBiscount.amount = string.valueOf(eeOrderInflightCancel.fibreBuildDiscount);
                    }
					DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge fibreBuildData = (String.isNotBlank(eeOrderFibreBuildCode) && eeOrderFibreBuildCost != null)
																								? getData(orderId, eeOrderFibreBuildCode, eeOrderFibreBuildCost, fBiscount)
																								: null;

					if(cancellationData != null) {
						ssOCList.add(cancellationData);
					}

					if(fibreBuildData != null) {
						ssOCList.add(fibreBuildData);
					}

					DF_SOAOrderDataJSONWrapper.ProductOrderInvolvesCharges ssPOC = new DF_SOAOrderDataJSONWrapper.ProductOrderInvolvesCharges();
					ssPOC.OneTimeChargeProdPriceCharge = ssOCList;

					ssMR.ProductOrder = ssPO;
					ssMR.ProductOrderInvolvesCharges = ssPOC;
					
					if(ssMR != null) {
						soaCancelOrderRequest = json.serialize(ssMR, true);
						soaCancelOrderRequest = removeDiscountFromSoaJSON(soaCancelOrderRequest);
					}
				}
			}
		} catch (Exception e) {
			sourceFunction = 'getEEOrdersSOACancelOrderRequest';
			message = 'Error is SOA request';
			payload = 'Error is constructing JSON for SOA	' + e.getMessage();
			GlobalUtility.logMessage(logLevel, sourceClass, sourceFunction, null, null, message, payload, e, 0);
		}
		
		return soaCancelOrderRequest;
	}

	public static DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge getData(String chargeReference, 
																					String code, 
																					Decimal amount,
                                                                                    DF_SOAOrderDataJSONWrapper.DiscountType dtype) {
		DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge otcppc = null;

		if(String.isNotBlank(code) && String.isNotBlank(chargeReference) && amount != null) {
			otcppc = new DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge();
			otcppc.quantity = STR_QTY_1;
			otcppc.name = STR_ORDER_CANCELLATION_CHARGES;
			otcppc.ID = code;
			otcppc.chargeReferences = chargeReference;
			otcppc.amount = String.valueOf(amount);
            if(dtype != Null)
                otcppc.discount = new List<DF_SOAOrderDataJSONWrapper.DiscountType>{dtype};
		}
		
		return otcppc;
	}

	public static Boolean proceedToBuildCharges(EE_OrderInflightCancel eeOrderInflightCancel) {
		String eeOrderCancellationCode = (eeOrderInflightCancel != null && String.isNotBlank(eeOrderInflightCancel.cancellationCode))
														? eeOrderInflightCancel.cancellationCode 
														: null;
		Decimal eeOrderCancellationFee = (eeOrderInflightCancel != null && eeOrderInflightCancel.cancellationFee != null)
											? eeOrderInflightCancel.cancellationFee 
											: null;
		
		System.debug('@@@ eeOrderInflightCancel : ' + eeOrderInflightCancel);
		System.debug('@@@ eeOrderInflightCancel.cancellationCode : ' + eeOrderInflightCancel.cancellationCode);
		System.debug('@@@ eeOrderInflightCancel.cancellationFee : ' + eeOrderInflightCancel.cancellationFee);

		return (String.isNotBlank(eeOrderCancellationCode) && eeOrderCancellationFee != null && eeOrderCancellationFee > 0);
	}


	/**
	 * Update EE Orders
	 *
	 * @param	  Map<String,DF_Order__c> eeOrdersById
	 * @param	  Map<String,String> eeOrdersSOACancelOrderRequest
	 *
	 * @return	 void
	*/
	public static void updateEEOrder(Id eeOrderId, 
										String eeOrderSOACancelOrderRequest) {

		DF_Order__c eeOrder = new DF_Order__c(Id = eeOrderId);
		eeOrder.Order_Cancel_Charges_JSON__c = (String.isNotBlank(eeOrderSOACancelOrderRequest))
											? eeOrderSOACancelOrderRequest
											: Label.DF_Order_Cancel_No_Charge;
		eeOrder.Order_Status__c = ORDER_STATUS_CANCELLED;
		//eeOrder.Appian_Notification_Date__c = DF_OrderInboundEventHandler.convertGMTtoLocal(DateTime.valueOfGMT(appianDate.replace('T', ' ').replace('Z','')));
		UPDATE eeOrder;
	}

	/**
	 * Remove discount related params from SOA JSON
	 *
	 * @param	  Map<String,String> eeOrdersSoaJson
	 *
	 * @return	 Map<String,String>
	*/
	public static String removeDiscountFromSoaJSON(String soaJson) {

		String updatedSoaJson = null;

		if(String.isNotBlank(soaJson)) {
			soaJson = soaJson.replace('"discount" : null,','');
			soaJson = soaJson.replace('"discount":null,','');
			soaJson = soaJson.replace(',{"type":"TERM","amount":null}','');
			soaJson = soaJson.replace('{"type":"PSD","amount":null}','');
			soaJson = soaJson.replace(',{"type":null,"amount":null}','');
			soaJson = soaJson.replace('{"type":null,"amount":null}','');
			soaJson = soaJson.replace('"discount":[],','');
			soaJson = soaJson.replace('"discount":[{"type":"TERM","amount":null}],','');
			soaJson = soaJson.replace('"discount":[{"type":"PSD","amount":null}],','');
			soaJson = soaJson.replace('[,{','[{');
			soaJson = soaJson.replace(',"discount":[{"type":"TERM","amount":"0.00"}]','');
			soaJson = soaJson.replace(',"discount":[{"type":"PSD","amount":"0.00"}]','');
			updatedSoaJson = soaJson;
		}
		
		return updatedSoaJson;
	}

	/**
	 * Send the emails out
	 *
	 * @param	 Set<String> eeOrdersId
	 * @param	 String TEMPLATE_NAME
	 *
	 * @return	 void
	*/
	public static void sendEmail(Id eeOrderId, String TEMPLATE_NAME) {

		Map<String, String> orderToTemplateMap = new Map<String, String>();
		if(String.isNotBlank(eeOrderId) && String.isNotBlank(TEMPLATE_NAME)) {
			orderToTemplateMap.put((String)eeOrderId, TEMPLATE_NAME);
		}

		if(orderToTemplateMap != null && !orderToTemplateMap.isEmpty()) {
			DF_OrderEmailService.sendEmail(orderToTemplateMap);
		}
	}

	public static void sendEmail(List<DF_Order__c> eeOrders, String TEMPLATE_NAME) {

		Map<String, String> orderToTemplateMap = new Map<String, String>();
		if(eeOrders != null && !eeOrders.isEmpty() && String.isNotBlank(TEMPLATE_NAME)) {
			for(DF_Order__c eeOrder : eeOrders) {
				orderToTemplateMap.put((String)eeOrder.Id, TEMPLATE_NAME);
			}
		}

		if(orderToTemplateMap != null && !orderToTemplateMap.isEmpty()) {
			DF_OrderEmailService.sendEmail(orderToTemplateMap);
		}
	}

    /* *
     * Close cancellation charge opportunity
     * 
    */
    
    public static void closeCancellationChargeOpportunity(Id opptyId){
		opportunity opportunityToClose = new Opportunity(Id = opptyId);
		opportunityToClose.StageName = 'Closed Won';
        try{
            Update opportunityToClose;
        }catch (Exception e){
            sourceFunction = 'closeCancellationChargeOpportunity';
            message = 'Unable to close cancellation charge opportunity';
            GlobalUtility.logMessage(logLevel, sourceClass, sourceFunction, null, null, message , Null, e, 0);
        }        
	}
    
    /**
     * Method to set opportunity stage to Cancelled Lost
     * @params opportunity id list
     */
    public static void cancellOpportunitiesServicesSubscriptions(list<string> opportunityIds){
        List<sobject> objectsToUpdate = new List<sobject>();
        List<csord__Order__c> cancelledOrders = [Select csordtelcoa__Opportunity__c,
                                                    (select csord__Status__c From csord__Subscriptions__r),
                                                	(Select csord__Status__c From csord__Services__r)
                                                 	From csord__Order__c 
                                                 	Where csordtelcoa__Opportunity__c IN :opportunityIds];
        for(Id OpportunityId:opportunityIds){
			objectsToUpdate.add((sobject)new opportunity(Id = OpportunityId, StageName ='Cancelled Lost'));            
        }

        for(csord__Order__c csorder :cancelledOrders){
            for(csord__Subscription__c cssub:csorder.csord__Subscriptions__r){
                cssub.csord__Status__c = ORDER_STATUS_CANCELLED;
                objectsToUpdate.add((sobject)cssub);
            }
            for(csord__Service__c csservice:csorder.csord__Services__r){
                csservice.csord__Status__c = ORDER_STATUS_CANCELLED;
                objectsToUpdate.add((sobject)csservice);
            }
        }
        try{
            update objectsToUpdate;
        } Catch(Exception e){
            sourceFunction = 'cancellOpportunitiesServicesSubscriptions';
            message = 'Unable to update opportunity or service or subscription';
            GlobalUtility.logMessage(logLevel, sourceClass, sourceFunction, null, null, message , Null, e, 0);
        }
		
    }
	
	/**
	 * Class to hold the details to be sent to SOA
	 */
	private class EE_OrderInflightCancel {
		String interactionDateTime {get; set;}
		String interactionStatus {get; set;}
		String cancellationCode {get; set;}
		Decimal cancellationFee {get; set;}
		String fibreBuildCode {get; set;}
		Decimal fibreBuildCost {get; set;}
		String orderId {get; set;}
		String sfId {get; set;}
		String billingAccountId {get; set;}
        Decimal fibreBuildDiscount {get; set;}
        Decimal cancellationDiscount {get; set;}
        String dealId {get; set;}
        String dealName {get; set;}
        String dealEndDate {get; set;}
		
        public EE_OrderInflightCancel(String salesforceId, String ordId, String interactionDtTime, String interactionSts, String billingId) {
			sfId = salesforceId;
			orderId = ordId;
			interactionDateTime = interactionDtTime;
			interactionStatus = interactionSts;
			billingAccountId = billingId;
		}
	}
		
}