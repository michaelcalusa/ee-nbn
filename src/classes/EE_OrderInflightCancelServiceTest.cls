/*------------------------------------------------------------------------
Author:         Bharath Kumar
Company:        Contractor
Description:    A test class created to test EE_OrderInflightCancelService methods             

History
27/11/2018    Bharath Kumar  Creation
--------------------------------------------------------------------------*/

@isTest
public class EE_OrderInflightCancelServiceTest {
    public static final String orderSubmitStr = '{ "productOrder":{ "accessSeekerInteraction":{ "billingAccountID":"BAN000000892801" }, "orderType":"Connect", "customerRequiredDate":"2017-02-25", "afterHoursAppointment":"Yes", "inductionRequired":"No", "securityClearance":"No", "notes":"Order Note Added", "tradingName":"Lacrosse", "heritageSite":"No", "csaId":"VIC", "itemInvolvesContact":[ { "type":"Business", "contactName":"Bob Tompson", "role":"Manager", "emailAddress":"bobt@example.com", "phoneNumber":"0412 345 443", "unstructuredAddress":{ "addressLine1":"Suite 2", "addressLine2":"Level 5", "addressLine3":"380 Doclands Drive", "localityName":"Melbourne CBD", "postcode":"3000", "stateTerritoryCode":"VIC" } }, { "type":"Site", "contactName":"Samantha Greg", "role":"", "emailAddress":"sgreg@example.com", "phoneNumber":"0412 345 443", "unstructuredAddress":{ "addressLine1":"Suite 2", "addressLine2":"Level 6", "addressLine3":"652 Latrobe St", "localityName":"Melbourne CBD", "postcode":"3000", "stateTerritoryCode":"VIC" } } ], "productOrderComprisedOf":{ "action":"ADD", "itemInvolvesLocation":{ "Id":"LOC000000160801" }, "itemInvolvesProduct":{ "term":"12 Months", "serviceRestorationSLA":"Enhanced-6 (24/7)", "accessAvailabilityTarget":"99.95% (Dual Uplink/same Network Aggregation element)", "zone":"CBD", "productType":"EE", "templateId":"TPL", "templateVersion":"1.0" }, "referencesProductOrderItem":[ { "action":"ADD", "itemInvolvesProduct":{ "type":"BTD", "ntdType":"Standard (CSAS)", "ntdMounting":"Rack", "powerSupply1":"DC(-d8V to -22V)", "powerSupply2":"Not Required" }, "referencesProductOrderItem":[ { "action":"ADD", "itemInvolvesProduct":{ "type":"UNIE", "interfaceBandwidth":"5 GB", "portId":null, "interfaceType":"100Base-10Base-T (Cat-3) ", "ovcType":"EVPL", "tpId":"0x9100", "cosMappingMode":"PCP", "transientId":"1" } } ] }, { "action":"ADD", "itemInvolvesProduct":{ "type":"OVC", "Id":"ovctest", "poi":"N/A", "nni":"NNI000000601417", "routeType":"Local", "sVLanId":"21", "maximumFrameSize":"Jumbo (9000 Bytes)", "cosHighBandwidth":"50Mbps ", "cosMediumBandwidth":"500Mbps ", "cosLowBandwidth":"500Mbps ", "uniVLanId":"25", "connectedTo":"1" } } ] }, "accessSeekerContact":{ "contactName":"John Doe", "contactPhone":"+61401123456" } } }';
    public static final String orderNotifierStr = '{ "orderType":"Connect", "customerRequiredDate":"2017-02-25", "afterHoursAppointment":"Yes", "inductionRequired":"No", "securityClearance":"No", "notes":"Order Note Added", "tradingName":"Lacrosse", "heritageSite":"No", "csaId":"VIC", "itemInvolvesContact":[ { "type":"Business", "contactName":"Bob Tompson", "role":"Manager", "emailAddress":"bobt@example.com", "phoneNumber":"0412 345 443", "unstructuredAddress":{ "addressLine1":"Suite 2", "addressLine2":"Level 5", "addressLine3":"380 Doclands Drive", "localityName":"Melbourne CBD", "postcode":"3000", "stateTerritoryCode":"VIC" } }, { "type":"Site", "contactName":"Samantha Greg", "role":"", "emailAddress":"sgreg@example.com", "phoneNumber":"0412 345 443", "unstructuredAddress":{ "addressLine1":"Suite 2", "addressLine2":"Level 6", "addressLine3":"652 Latrobe St", "localityName":"Melbourne CBD", "postcode":"3000", "stateTerritoryCode":"VIC" } } ], "productOrderComprisedOf":{ "action":"ADD", "itemInvolvesLocation":{ "Id":"LOC000000160801" }, "itemInvolvesProduct":{ "term":"12 Months", "serviceRestorationSLA":"Enhanced-6 (24/7)", "accessAvailabilityTarget":"99.95% (Dual Uplink/same Network Aggregation element)", "zone":"CBD", "productType":"EE", "templateId":"TPL", "templateVersion":"1.0" }, "referencesProductOrderItem":[ { "action":"ADD", "itemInvolvesProduct":{ "type":"BTD", "ntdType":"Standard (CSAS)", "ntdMounting":"Rack", "powerSupply1":"DC(-d8V to -22V)", "powerSupply2":"Not Required" }, "referencesProductOrderItem":[ { "action":"ADD", "itemInvolvesProduct":{ "type":"UNIE", "interfaceBandwidth":"5 GB", "portId":null, "interfaceType":"100Base-10Base-T (Cat-3) ", "ovcType":"EVPL", "tpId":"0x9100", "cosMappingMode":"PCP", "transientId":"1" } } ] }, { "action":"ADD", "itemInvolvesProduct":{ "type":"OVC", "Id":"ovctest", "poi":"N/A", "nni":"NNI000000601417", "routeType":"Local", "sVLanId":"21", "maximumFrameSize":"Jumbo (9000 Bytes)", "cosHighBandwidth":"50Mbps ", "cosMediumBandwidth":"500Mbps ", "cosLowBandwidth":"500Mbps ", "uniVLanId":"25", "connectedTo":"1" } } ] }, "accessSeekerContact":{ "contactName":"John Doe", "contactPhone":"+61401123456" } }';
    public static final String RECORD_TYPE_NAME_EE = 'Enterprise Ethernet';
    public static final String OBJECT_NAME_DF_ORDER = 'DF_Order__c';

    @testSetup static void testDataSetup() {
        String STATUS_ACTIVE = 'Active';
        final Id eeOrderRecordTypeId = Schema.getGlobalDescribe().get(OBJECT_NAME_DF_ORDER).getDescribe().getRecordTypeInfosByName().get(RECORD_TYPE_NAME_EE).getRecordTypeId();

        // create account1 record
        Account account1 = new Account();
        account1.Name = 'Telstra';
        account1.Access_Seeker_ID__c = 'AS123';
        insert account1;


        Contact contact1 = new Contact();
        contact1.FirstName = 'FN1';
        contact1.LastName = 'LN1';
        contact1.Account = account1;
        contact1.Email = '123@mailinator.com';
        contact1.Status__c = STATUS_ACTIVE;
        contact1.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        contact1.AccountId=account1.Id;
        insert contact1;


        Profile commProfile = [SELECT Id 
                               FROM Profile 
                               WHERE Name = 'DF Partner User'];

        User dfUser = new User();
        dfUser.Alias = 'test123';
        dfUser.Email = 'test123@noemail.com';
        dfUser.Emailencodingkey = 'ISO-8859-1';
        dfUser.Lastname = 'Testing';
        dfUser.Languagelocalekey = 'en_US';
        dfUser.Localesidkey = 'en_AU';
        dfUser.Profileid = commProfile.Id;
        dfUser.IsActive = true;
        dfUser.ContactId = contact1.Id;
        dfUser.Timezonesidkey = 'Australia/Sydney';
        dfUser.Username = 'tester@noemail.com.test';
        dfUser.Contact = contact1;
        insert dfUser;
        

        System.runAs(dfUser) {
            PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'DF_BC_Customer_Plus_Permission'];
            insert new PermissionSetAssignment(AssigneeId = dfUser.id, PermissionSetId = ps.Id );
            
            List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';

            // Create OpptyBundle
            DF_Opportunity_Bundle__c opptyBundle1 = DF_TestData.createOpportunityBundle(account1.Id);
            insert opptyBundle1;

            // Create Oppty
            Opportunity oppty1 = DF_TestData.createOpportunity('LOC111111111111');
            oppty1.Opportunity_Bundle__c = opptyBundle1.Id;
            insert oppty1;

            // Create DFQuote recs
            DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty1.Id, opptyBundle1.Id, 1000, 'Green');        
            dfQuote1.Proceed_to_Pricing__c = true;
            dfQuote1.GUID__c = '123456789';
            dfQuoteList.add(dfQuote1);
            insert dfQuoteList; 


            List<DF_Order__c> eeOrders = new List<DF_Order__c>();       
            DF_Order__c eeOrder1 = new DF_Order__c();
            eeOrder1.DF_Quote__c = dfQuote1.Id;
            eeOrder1.Order_Json__c = orderSubmitStr;
            eeOrder1.OrderType__c = 'Connect';
            eeOrder1.Order_Notifier_Response_JSON__c = orderNotifierStr;
            eeOrder1.Opportunity_Bundle__c = opptyBundle1.Id;
            eeOrder1.Order_Id__c = 'BCO-12345';
            eeOrder1.RecordTypeId = eeOrderRecordTypeId;
            eeOrder1.Order_Status__c = DF_Constants.DF_ORDER_STATUS_IN_PROGRESS;
            eeOrder1.Order_Sub_Status__c = DF_Constants.DF_ORDER_SUB_STATUS_CONSTRUCTION_STARTED;
            eeOrders.add(eeOrder1);
            
            INSERT eeOrders;

            //Insert required custom settings
            DF_AS_Custom_Settings__c settings  = new DF_AS_Custom_Settings__c(Name='Order_Cancel_Business_Days', Value__c='1');
            INSERT settings;

            INSERT EE_OrderInflightCancelUtil.createEEOrderCancelCustomSettings();

            createASRConfigCustomSettings();
        }
    }

    @isTest
    static void testIsCancellable_ACK_Order() {
        
        Df_order__c eeOrder  = [SELECT Id, Order_Status__c FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1];
        eeOrder.Order_Status__c = DF_Constants.DF_ORDER_STATUS_ACKNOWLEDGED;
        UPDATE eeOrder;
        
        Test.startTest();
        
        Boolean isCancellable = EE_OrderInflightCancelService.isCancellableOrder(eeOrder.Id, Date.newInstance(2018, 11, 16));
        
        Test.stopTest();
     
        System.assertEquals(true,isCancellable);
    }

    @isTest
    static void testIsCancellable_In_Progress_Scheduled_Order_Not_cancellable() {
        df_order__c eeOrder  = [SELECT Id, Order_Status__c, Order_Sub_Status__c, NBN_Commitment_Date__c FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1];
        eeOrder.Order_Status__c = DF_Constants.DF_ORDER_STATUS_IN_PROGRESS;
        eeOrder.Order_Sub_Status__c = DF_Constants.DF_ORDER_SUB_STATUS_ORDER_SCHEDULED;
        //Set NBN COmmited Date AS 19th Nov 2018 - Monday
        eeOrder.NBN_Commitment_Date__c = Date.newInstance(2018, 11, 19);
        UPDATE eeOrder;

        Test.startTest();
        Boolean isCancellable = EE_OrderInflightCancelService.isCancellableOrder(eeOrder.Id,Date.newInstance(2018, 11, 16));
        Test.stopTest();
        System.assertEquals(false,isCancellable);
    }

    @isTest
    static void testIsCancellable_In_Progress_Scheduled_Order_Not_Cancellable_Holiday() {
        df_order__c eeOrder  = [SELECT Id, Order_Status__c, Order_Sub_Status__c, NBN_Commitment_Date__c FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1];
        eeOrder.Order_Status__c = DF_Constants.DF_ORDER_STATUS_IN_PROGRESS;
        eeOrder.Order_Sub_Status__c = DF_Constants.DF_ORDER_SUB_STATUS_ORDER_SCHEDULED;
        //Set NBN COmmited Date AS 19th Nov 2018 - Monday
        eeOrder.NBN_Commitment_Date__c = Date.newInstance(2018, 11, 19);
        UPDATE eeOrder;
        //Set today date as  15th Nov 2018 - Thursday - Being 16th is public Holiday (should NOT allow this date)
        //Insert One Holiday for Testing -Thursday - 16th is public Holiday
        //To avoid mixed DML Error
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
        Holiday holiday = new Holiday(Name='Test Holiday', ActivityDate = Date.newInstance(2018, 11, 16));
        INSERT holiday;
        }

        Test.startTest();
        Boolean isCancellable = EE_OrderInflightCancelService.isCancellableOrder(eeOrder.Id,Date.newInstance(2018, 11, 15));
        Test.stopTest();
        System.assertEquals(false,isCancellable);
    }

    @isTest
    static void testIsCancellable_OrderSubStatus_Accepted() {
        df_order__c eeOrder  = [SELECT Id, Order_Status__c, Order_Sub_Status__c, NBN_Commitment_Date__c FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1];
        eeOrder.Order_Status__c = DF_Constants.DF_ORDER_STATUS_IN_PROGRESS;
        eeOrder.Order_Sub_Status__c = DF_Constants.DF_ORDER_SUB_STATUS_ORDER_ACCEPTED;      
        UPDATE eeOrder;     
        
        Test.startTest();
        Boolean isCancellable = EE_OrderInflightCancelService.isCancellableOrder(eeOrder.Id,Date.newInstance(2018, 11, 15));
        Test.stopTest();
        System.assertEquals(true,isCancellable);
    }

    @IsTest 
    static void testCancellOpportunitiesServicesSubscriptions()
    {   
        Account acc = [ SELECT Id, Name FROM Account LIMIT 1 ];
        Opportunity opp = [ SELECT Id, Name FROM Opportunity LIMIT 1];
        List<String> opportunityIds = new List<String>();
        opportunityIds.add(opp.Id);

        DF_TestService.createCSSubscriptionRecord(acc.Id, opp);

        Test.startTest();
        EE_OrderInflightCancelService.CancellOpportunitiesServicesSubscriptions(opportunityIds);
        Test.stopTest();

        csord__Order__c csOrder = [ SELECT Id, Name, csord__Status__c FROM csord__Order__c LIMIT 1 ];       
        System.assertNotEquals(EE_OrderInflightCancelService.ORDER_STATUS_CANCELLED, csOrder.csord__Status__c);
    }   

    @IsTest 
    static void testSendMail()
    {
        DF_Order__c eeOrder = [ SELECT Id, Name FROM DF_Order__c LIMIT 1 ];
        List<DF_Order__c> eeOrders = [ SELECT Id, Name FROM DF_Order__c LIMIT 1 ];
        Test.startTest();
        EE_OrderInflightCancelService.sendEmail(eeOrder.Id, DF_Constants.TEMPLATE_ORDER_CANCELLED);
        EE_OrderInflightCancelService.sendEmail(eeOrders, DF_Constants.TEMPLATE_ORDER_CANCELLED);
        Test.stopTest();
    }

    @IsTest 
    static void testGetData()
    {
        DF_SOAOrderDataJSONWrapper.DiscountType cancelDiscount = new DF_SOAOrderDataJSONWrapper.DiscountType();
        cancelDiscount.type = 'PSD';
        cancelDiscount.amount = '1';
        DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge otpp;
        Test.startTest();
        otpp = EE_OrderInflightCancelService.getData('a1h5D000000HUDPQA4_0', 'EEAS', 500, cancelDiscount);
        Test.stopTest();
        System.assertNotEquals(null, otpp);
    }

    @isTest
    static void testIsCancellable_Null_NBN_Commitment_Date() {
        df_order__c eeOrder  = [SELECT Id, Order_Status__c, Order_Sub_Status__c, NBN_Commitment_Date__c FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1];
        eeOrder.Order_Status__c = DF_Constants.DF_ORDER_STATUS_IN_PROGRESS;
        eeOrder.Order_Sub_Status__c = DF_Constants.DF_ORDER_SUB_STATUS_ORDER_SCHEDULED;     
        eeOrder.NBN_Commitment_Date__c = null;
        UPDATE eeOrder;     
        
        Test.startTest();
        Boolean isCancellable = EE_OrderInflightCancelService.isCancellableOrder(eeOrder.Id,Date.newInstance(2018, 11, 14));
        Test.stopTest();
        System.assertEquals(false,isCancellable);
    }

    @isTest
    static void testIsCancellable_In_Progress_Scheduled_Order_Cancellable() {
        df_order__c eeOrder = [SELECT Id, Order_Status__c, Order_Sub_Status__c, NBN_Commitment_Date__c FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1];
        eeOrder.Order_Status__c = DF_Constants.DF_ORDER_STATUS_IN_PROGRESS;
        eeOrder.Order_Sub_Status__c = DF_Constants.DF_ORDER_SUB_STATUS_ORDER_SCHEDULED;
        //Set NBN COmmited Date AS 19th Nov 2018 - Monday
        eeOrder.NBN_Commitment_Date__c = Date.newInstance(2018, 11, 19);
        UPDATE eeOrder;
        //Set today date as 14th Nov 2018 - Thursday - 16th is public Holiday AND 15th is not holiday (should allow this date)
        //Insert One Holiday for Testing -Thursday - 16th is public Holiday
        //To avoid mixed DML Error
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(thisUser){
        Holiday holiday = new Holiday(Name='Test Holiday', ActivityDate = Date.newInstance(2018, 11, 16));
        INSERT holiday;
        }

        Test.startTest();
        Boolean isCancellable = EE_OrderInflightCancelService.isCancellableOrder(eeOrder.Id,  Date.newInstance(2018, 11, 14));
        Test.stopTest();
        System.assertEquals(true,isCancellable);
    }

    @isTest
    static void test_NotificationFromAppian() {
        Test.startTest();
        final String appianNotification1 = '{"headers":{"timestamp": "2011-06-23T12:00:00Z" }, "body": { "manageProductOrderNotification": { "notificationType": "OrderCancelled", "productOrder": { "id": "ORD000000000214", "accessSeekerInteraction": { "billingAccountID": "BAN000000892801" }, "interactionStatus": "Cancelled", "interactionDateComplete": "2018-11-12T01:23:57Z", "orderType": "Connect", "customerRequiredDate": "2017-02-25T00:00:00Z", "afterHoursAppointment": "Yes", "inductionRequired": "No", "securityClearance": "No", "tradingName": "BUNNINGS PTY LTD", "notes": "Lore Ipsum", "heritageSite": "No", "itemInvolvesContact": [ { "type": "Business", "contactName": "Bob Tompson", "role": "Manager", "emailAddress": "bobt@example.com", "phoneNumber": "0412 345 443", "unstructuredAddress": { "addressLine1": "Suite 2", "addressLine2": "Level 5", "addressLine3": "380 Doclands Drive", "localityName": "Melbourne CBD", "postcode": "3000", "stateTerritoryCode": "VIC" } } ], "businessInteractionRelationship": [ "xyz" ], "plannedCompletionDate": "2017-02-25T00:00:00Z", "productOrderComprisedOf": { "action": "ADD", "itemInvolvesLocation": { "id": "LOC000006040786" }, "itemInvolvesProduct": { "id": "PRI000023422232","term": "12 Months", "serviceRestorationSLA": "Enhanced-6 (24/7)", "accessAvailabilityTarget": "99.95% (Dual Uplink/same Network Aggregation element)", "zone": "CBD", "productType": "EE", "templateId": "TPL", "templateVersion": "1.0" }, "referencesProductOrderItem": [ { "action": "ADD", "itemInvolvesProduct": { "type": "BTD", "ntdType": "Standard (CSAS)", "ntdMounting": "Rack", "powerSupply1": "DC(-d8V to -22V)", "powerSupply2": "Not Required" }, "referencesProductOrderItem": [ { "action": "ADD", "itemInvolvesProduct": { "type": "UNIE", "interfaceBandwidth": "5 GB", "portId": "", "interfaceType": "100Base-10Base-T (Cat-3) ", "ovcType": "EVPL", "tpId": "0x9100", "cosMappingMode": "PCP", "transientId": "1" } } ] }, { "action": "ADD", "itemInvolvesProduct": { "type": "OVC", "poi": "N/A", "nni": "NNI000000601417", "routeType": "Local", "sVLanId": "21", "maximumFrameSize": "Jumbo (9000 Bytes)", "cosHighBandwidth": "50Mbps ", "cosMediumBandwidth": "500Mbps ", "cosLowBandwidth": "500Mbps ", "uniVLanId": "25", "connectedTo": "1", "ovcTransientId":"OVC 1" } } ] }, "accessSeekerContact": { "contactName": "John Doe", "contactPhone": "+61401123456" } } } } }';
        
        DF_Order_Settings__mdt dfOrderSettings = EE_OrderInflightCancelUtil.getDFOrderSettings();

        DF_Order__c eeOrder  = [SELECT Id, Cancel_Initiated_Stage__c, Order_Status__c, Order_Sub_Status__c, NBN_Commitment_Date__c FROM DF_Order__c WHERE OrderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1];
        eeOrder.Order_Status__c = DF_Constants.DF_ORDER_STATUS_CANCEL_INITIATED;
        eeOrder.Order_Sub_Status__c = '';
        eeOrder.Cancel_Initiated_Stage__c = 'Build';
        UPDATE eeOrder;

        List<DF_Order_Event__e> dfOrderEvents = new List<DF_Order_Event__e>();
        
        DF_Order_Event__e dfOrderEvent1 = new DF_Order_Event__e();
        dfOrderEvent1.Event_Id__c = 'ORDCANCELLED101';
        dfOrderEvent1.Event_Record_Id__c ='123456789';
        dfOrderEvent1.Event_Type__c = 'Application Cancel Process';
        dfOrderEvent1.Source__c = 'OrderNotifier';
        dfOrderEvent1.Message_Body__c = appianNotification1;
        dfOrderEvents.add(dfOrderEvent1);
        
        
        System.assertNotEquals(dfOrderSettings, null);
        System.assertEquals(dfOrderSettings.OrderCancelEventCode__c, 'ORDCANCELLED101');
        EventBus.publish(dfOrderEvents);
        Test.stopTest();
    }
    
    @isTest
    static void test_ProcessCancellationOpportunity() {
        
        DF_Order_Settings__mdt dfOrderSettings = EE_OrderInflightCancelUtil.getDFOrderSettings();

        Account acc = [ SELECT Id, Name FROM Account LIMIT 1 ];     

        DF_Order__c eeOrder = 
        [
            SELECT Id, Cancel_Initiated_Stage__c, Order_Status__c, Order_Sub_Status__c, NBN_Commitment_Date__c 
            FROM DF_Order__c 
            WHERE OrderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1
        ];

        eeOrder.Order_Status__c = DF_Constants.DF_ORDER_STATUS_CANCEL_INITIATED;
        eeOrder.Order_Sub_Status__c = '';
        eeOrder.Cancel_Initiated_Stage__c = 'Build';
        UPDATE eeOrder;

        DF_AttributeTest testName1 = new DF_AttributeTest('Cancellation_Charge', '50000', true, null);
        DF_AttributeTest testName2 = new DF_AttributeTest('InflightStage', 'Design', false, null);
        DF_AttributeTest testName3 = new DF_AttributeTest('FBC_Charge', '1000', false, null);
        DF_AttributeTest testName4 = new DF_AttributeTest('Total_Cancellation_Charge', '60000', false, null);
        
        List<DF_AttributeTest> attributeTests1 = new List<DF_AttributeTest>{testName1, testName2, testName3, testName4};
                
        //Create Basket
        cscfga__Product_Basket__c basket = DF_TestData.buildBasket();
        basket.csbb__Account__c = acc.id;
        insert basket;
        
        //Create pcr data
        DF_TestService.createProductConfigData('Direct Fibre - Inflight Build Cancellation Charge', basket, attributeTests1);
        
        cscfga__Product_Definition__c cspd = [ SELECT Id, Name FROM cscfga__Product_Definition__c LIMIT 1 ];
        
        cscfga__Product_Configuration__c cspc = [ SELECT Id, Name FROM cscfga__Product_Configuration__c LIMIT 1 ];
        cspc.cscfga__Product_Definition__c=cspd.Id;
        update cspc;

        DF_AS_Custom_Settings__c opt1 = [ SELECT Id, Name FROM DF_AS_Custom_Settings__c WHERE Name = 'SF_INFLIGHT_CANCELLATION_DEFINITION_ID' LIMIT 1 ]; 
        opt1.Value__c = cspd.Id;
        update opt1;

        Test.startTest();
        Map<Id, Id> orderIdToOpportunityId = EE_OrderInflightCancelService.createCancelOpportunityForOrder(new List<DF_Order__c>{eeOrder});
        System.assertNotEquals(null, orderIdToOpportunityId);
        System.assertEquals(1, orderIdToOpportunityId.size());
        List<Id> eeOrderIds = new List<Id>();
        eeOrderIds.addAll(orderIdToOpportunityId.keySet());
        List<Id> csOppIds =orderIdToOpportunityId.values();

        System.debug('@@@ csOppIds.get(0) : ' + csOppIds.get(0));

        csord__Order__c dummyOrder = new csord__Order__c(Name = 'Cancellation Charge'
        , csord__Identification__c = 'Order_a1h5D000000HUDPQA7_1');
        insert dummyOrder;

        csord__Order__c testOrder = new csord__Order__c
            ( Name = 'Direct Fibre - Inflight Build Cancellation Charge'
            , csordtelcoa__Opportunity__c = csOppIds.get(0)
            , csord__Primary_Order__c = dummyOrder.id
            , csordtelcoa__Product_Configuration__c = cspc.id
            , csord__Status2__c = 'Order Submitted'
            , csord__Order_Type__c = 'Product Delivery'
            , csord__Product_Type__c = 'Recoverable Works'
            , csord__Identification__c = 'Order_a1h5D000000HUDPQA4_0'
            );
        
        insert testOrder;
        
        /*
        Invoice__c testInvoice = new Invoice__c
                ( Status__c = 'Requested'
                , Name = 'To be issued'
                , Opportunity__c = csOppIds.get(0) 
                , Unpaid_Balance__c = decimal.valueOf('4025.00')
                , Payment_Status__c = 'UnPaid'
                );  
        
        insert testInvoice;
        

        Invoice_Line_Item__c testInvoiceLineItem = new Invoice_Line_Item__c
                ( Quantity__c = decimal.valueOf('1')
                , Unit_Price__c = decimal.valueOf('1150.00')
                , Memo_Line__c = ''
                , Inc_GST__c = true
                , Tax_Code__c = 'GST 10% (NON-CAPITAL)'
                , Invoice__c = testInvoice.Id
                , Order__c = testOrder.Id
                , ILI_Type__c = 'Initial Basket'
                );
                
        insert testInvoiceLineItem;
        */

        List<csord__Order_Line_Item__c> testOLIList = new List<csord__Order_Line_Item__c>();

        testOLIList.add(new csord__Order_Line_Item__c
            ( Name = 'PCD Relocation'
            , csord__Order__c = testOrder.Id
            , csord__Line_Description__c = 'Cancellation Charge-EEAS_NRC_CANX_DESIGN_FEE'           
            , OLI_Type__c = 'Initial Basket'
            , Memo_Line__c = ''
            , csord__Total_Price__c = decimal.valueOf('60000.00')
            , GST__c = 'GST (Inclusive)'
            , Cost__c = decimal.valueOf('1000.00')
            , Margin__c = decimal.valueOf('15.00')
            , Quantity__c = decimal.valueOf('1')
            , Unit_Price__c = decimal.valueOf('1150.00')
            , csord__Identification__c = 'Order_a1h5D000000HUDPQA4_0'
            )); 

        testOLIList.add(new csord__Order_Line_Item__c
            ( Name = 'PCD Relocation'
            , csord__Order__c = testOrder.Id
            , csord__Line_Description__c = 'Fibre Build Charge-EEAS_FBC'
            , OLI_Type__c = 'Initial Basket'
            , Memo_Line__c = ''
            , GST__c = 'GST (Inclusive)'
            , Cost__c = decimal.valueOf('1000.00')
            , Margin__c = decimal.valueOf('15.00')
            , Quantity__c = decimal.valueOf('1')
            , Unit_Price__c = decimal.valueOf('1150.00')
            , csord__Identification__c = 'Order_a1h5D000000HUDPQA4_0'
            )); 

        testOLIList.add(new csord__Order_Line_Item__c
            ( Name = 'PCD Relocation'
            , csord__Order__c = testOrder.Id
            , csord__Line_Description__c = 'DBP Cancellation Discount-EEAS_DBCD'
            , OLI_Type__c = 'Initial Basket'
            , Memo_Line__c = ''
            , GST__c = 'GST (Inclusive)'
            , Cost__c = decimal.valueOf('1000.00')
            , Margin__c = decimal.valueOf('15.00')
            , Quantity__c = decimal.valueOf('1')
            , Unit_Price__c = decimal.valueOf('1150.00')
            , csord__Identification__c = 'Order_a1h5D000000HUDPQA4_0'
            )); 

        testOLIList.add(new csord__Order_Line_Item__c
            ( Name = 'PCD Relocation'
            , csord__Order__c = testOrder.Id
            , csord__Line_Description__c = 'FBC Discount-EEAS_FBCD'
            , OLI_Type__c = 'Initial Basket'
            , Memo_Line__c = ''
            , GST__c = 'GST (Inclusive)'
            , Cost__c = decimal.valueOf('1000.00')
            , Margin__c = decimal.valueOf('15.00')
            , Quantity__c = decimal.valueOf('1')
            , Unit_Price__c = decimal.valueOf('1150.00')
            , csord__Identification__c = 'Order_a1h5D000000HUDPQA4_0'
            ));         

        insert testOLIList;

        EE_OrderInflightCancelService.processCancellationOpportunity(eeOrderIds.get(0), csOppIds.get(0), 'Design', 1000.00, null, null, null, null);
        Test.stopTest();
        
        DF_Order__c eeOrderAfterCancelled = [ SELECT Id, Order_Status__c FROM DF_Order__c WHERE OrderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1 ];
        
        System.assertEquals(EE_OrderInflightCancelService.ORDER_STATUS_CANCELLED, eeOrderAfterCancelled.Order_Status__c); 
        
        List<csord__Order__c> cancellationOrders = 
                [
                    SELECT 
                        Name, 
                        csordtelcoa__Opportunity__r.Commercial_Deal__r.Name, 
                        csordtelcoa__Opportunity__r.Commercial_Deal__r.End_Date__c, 
                        csordtelcoa__Opportunity__r.Account.Name,
                        (
                            SELECT 
                                Name,
                                csord__Total_Price__c, 
                                csord__Line_Description__c 
                            FROM csord__Order_Line_Items__r
                        ) 
                    FROM csord__Order__c LIMIT 1
                ];

        System.debug('###!!! cancellationOrders: ' + cancellationOrders);
    }

    public static void createASRConfigCustomSettings() {
        try {
            Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
            asrConfigSettings.Max_No_of_Future_Calls__c = 50;
            asrConfigSettings.Max_No_of_Parallels__c = 10;
            asrConfigSettings.Max_No_of_Retries__c = 3;
            asrConfigSettings.Max_No_of_Rows__c = 1;            
            upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {        
            throw new CustomException(e.getMessage());
        }               
    }
}