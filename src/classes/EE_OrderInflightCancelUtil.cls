/*------------------------------------------------------------  
  Description:   	Utility class for Order Inflight Cancellation 
 
  Test Class:		EE_OrderInflightCancelUtilTest
  ------------------------------------------------------------*/
public class EE_OrderInflightCancelUtil {

	private static final String STR_ERROR = 'Error';
	private static final String STR_EMPTY = '';
	
	private static String payload = STR_EMPTY;
	private static String message = STR_EMPTY;
	private static String sourceFunction = STR_EMPTY;
	private static final String logLevel = STR_ERROR;
	private static final String sourceClass = EE_OrderInflightCancelUtil.class.getName();

	private static final String RECORD_TYPE_NAME_EE = 'Enterprise Ethernet';
	private static final String OBJECT_NAME_DF_ORDER = 'DF_Order__c';
	private static final String RECORD_TYPE_DEVELOPER_NAME_EE = 'Enterprise_Ethernet';
	private static final String BUSINESS_CHANNEL_HEADER_NAME = 'businessChannel';
	private static final String ACTIVITY_NAME_HEADER_NAME ='activityName';
	private static final String ACTIVITY_NAME_HEADER_VALUE = 'cancelOrder';
	private static final String MSGNAME_HEADER_NAME = 'msgName';
	private static final String MSGNAME_HEADER_VALUE = 'ManageProductOrdercancelOrderRequest';
	private static final String BUSINESSSERVICENAME_HEADER_NAME = 'businessServiceName';
	private static final String BUSINESSSERVICENAME_HEADER_VALUE = 'ManageProductOrder';
	private static final String BUSINESSPROCESSVERSION_HEADER_NAME = 'businessProcessVersion';
	private static final String BUSINESSPROCESSVERSION_HEADER_VALUE = 'V13.0';
	private static final String BUSINESSSERVICEVERSION_HEADER_NAME = 'businessServiceVersion';
	private static final String BUSINESSSERVICEVERSION_HEADER_VALUE = 'V2.0';
	private static final String MSGTYPE_HEADER_NAME = 'msgType';
	private static final String MSGTYPE_HEADER_VALUE = 'Request';
	private static final String SECURITY_HEADER_NAME = 'security';
	private static final String SECURITY_HEADER_VALUE = 'Placeholder Security';
	private static final String COMMUNICATIONPATTERN_HEADER_NAME = 'communicationPattern';
	private static final String COMMUNICATIONPATTERN_HEADER_VALUE = 'RequestReply';
	private static final String ORDER_PRIORITY_NAME_HEADER_NAME = 'orderPriority';
	private static final String ORDER_PRIORITY_NAME_HEADER_VALUE = '6';
	private static final String ORDER_CANCEL_REQUEST = 'OrderCancelRequest';
	private static final String SUCCESS_RESP = 'Success Response';
	public static final String NO_CHARGE = 'No Charge';
	public static final String DESIGN_NO_CHARGE = 'Design - No Charge';
	public static final String DESIGN = 'Design';

	private static Id eeOrderRecordTypeId {get; set;}
	public static DF_Order__c dfOrder;
	public static DF_Order_Settings__mdt dfOrderSettings;
	public static List<DF_Order_Cancel_Settings__c> dfOrderCancelSettings;
	private static String CORRELATION_ID_VAL;
	private static String ACCESS_SEEKER_ID_VAL;

	/**
	 * Get the EE Order Record Type ID
	 *
	 * @return	 Id
	*/
	public static Id getEEOrderRecordTypeId() {
		
		if(EE_OrderInflightCancelUtil.eeOrderRecordTypeId == null) {
			EE_OrderInflightCancelUtil.eeOrderRecordTypeId = EE_OrderInflightCancelUtil.getRecordTypeId(EE_OrderInflightCancelUtil.RECORD_TYPE_NAME_EE, EE_OrderInflightCancelUtil.OBJECT_NAME_DF_ORDER);
		}
		return EE_OrderInflightCancelUtil.eeOrderRecordTypeId;
	}

	/**
	 * Get List of DF Orders using DF Order Ids
	 *
	 * @param	  List<String> eeOrdersId
	 *
	 * @return	 List<DF_Order__c>
	*/
	public static List<DF_Order__c> getEEOrdersById(List<String> eeOrdersId) {
		
		List<DF_Order__c> eeOrders = null;

		if(eeOrdersId != null && !eeOrdersId.isEmpty()) {
			eeOrders = [SELECT 
							Id,
							Cancel_Initiated_Stage__c,
							Customer_Required_Date__c,
							DF_Quote__r.GUID__c,
							NBN_Commitment_Date__c,
							Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c,
                        	Opportunity_Bundle__r.Account__c,
                        	DF_Quote__r.Opportunity__c,
                        	DF_Quote__r.Opportunity__r.Commercial_Deal__c,
                        	DF_Quote__r.Fibre_Build_Cost__c,
							Order_Id__c,
							Order_Status__c,
							Order_Sub_Status__c,
							RecordType.DeveloperName,
							(
								SELECT 
									CreatedDate,
									NewValue,
									OldValue 
								FROM 
									Histories
								WHERE 
									Field IN ('Order_Sub_Status__c')
								ORDER BY 
									CreatedDate DESC
							)
						FROM 
							DF_Order__c 
						WHERE 
							Id IN :eeOrdersId
						AND 
							OrderType__c = :DF_Constants.ORDER_TYPE_CONNECT
						AND 
							RecordTypeId = :EE_OrderInflightCancelUtil.getEEOrderRecordTypeId()];
		}
		return eeOrders;
	}


	/**
	 * Get List of DF Orders using DF Quote GUID
	 *
	 * @param	  List<String> eeQuoteGUID
	 *
	 * @return	 List<DF_Order__c>
	*/
	public static Map<Id, DF_Order__c> getEEOrdersByQuoteGUID(List<String> eeQuoteGUID) {

		Map<Id, DF_Order__c> eeOrdersById = null;
		if(eeQuoteGUID != null && !eeQuoteGUID.isEmpty()) {
			eeOrdersById = new Map<Id, DF_Order__c>([SELECT 
														Id,
														Cancel_Initiated_Stage__c,
														DF_Quote__r.Fibre_Build_Cost__c,
														DF_Quote__r.GUID__c,
														NBN_Commitment_Date__c,
														Opportunity_Bundle__r.Account__r.Billing_ID__c,
							                        	Opportunity_Bundle__r.Account__c,
							                        	DF_Quote__r.Opportunity__c,
							                        	DF_Quote__r.Opportunity__r.Commercial_Deal__c,
														Order_Cancel_Charges_JSON__c,
														Order_Id__c,
														Order_Status__c,
														Order_Sub_Status__c,
														SOA_Order_Cancel_Charges_Sent__c
													FROM 
														DF_Order__c 
													WHERE 
														DF_Quote__r.GUID__c IN :eeQuoteGUID
													AND 
														OrderType__c = :DF_Constants.ORDER_TYPE_CONNECT
													AND 
														RecordTypeId = :EE_OrderInflightCancelUtil.getEEOrderRecordTypeId()]);
		}
		return eeOrdersById;
	}

	/**
	 * Method to get RecordType Id
	 *
	 * @param 	String RECORD_TYPE_NAME
	 * @param 	String SobjectType
	 * @return 	Id recTypeId
	*/
	public static Id getRecordTypeId(final String RECORD_TYPE_NAME, final String SobjectType) {
		
		Id recTypeId = null;
		
		if(String.isNotBlank(RECORD_TYPE_NAME) && String.isNotBlank(SobjectType)) {
			
			recTypeId = Schema.getGlobalDescribe().get(SobjectType).getDescribe().getRecordTypeInfosByName().get(RECORD_TYPE_NAME).getRecordTypeId();
		}
		
		return recTypeId;
	}

	/**
	 * Method to get DF Order Settings custom metadata
	 *
	 * @return 	DF_Order_Settings__mdt dfOrderSettings
	*/
	public static DF_Order_Settings__mdt getDfOrderSettings() {
		DF_Order_Settings__mdt dfOrderSettings = [SELECT 
													OrderCancelBatchSize__c, 
													OrderCancelEventCode__c, 
													OrderCancelSource__c
												FROM 
													DF_Order_Settings__mdt
												WHERE 
													DeveloperName = 'DFOrderSettings'];
		return dfOrderSettings;
	}


	/**
	 * Method to get values from sObjects
	 *
	 *  @return 	List<String> fieldValues
	*/
	public static List<String> getFieldValues(List<sObject> sObjects, String fieldName, string relationShip) {
		List<String> fieldValues = new List<String>();
		Set<String> uniqueValues = new Set<String>();
		if(sObjects != null && !sObjects.isEmpty() && String.isNotBlank(fieldName)) {
			for(sObject thisObject : sObjects) {
				String value = string.isEmpty(relationShip)? (String)thisObject.get(fieldName): (thisObject.getSObject(relationShip) <> Null? (string)thisObject.getSObject(relationShip).get(fieldName): Null);
				if(String.isNotBlank(value)) {
					uniqueValues.add(value);
				}
			}
		}
		if(!uniqueValues.isEmpty()) {
			fieldValues.addAll(uniqueValues);
		}

		return fieldValues;
	}

	/**
	 * Method to get values from sObjects
	 *
	 * @return 	List<String> fieldValues
	*/
	public static Map<String, sObject> getObjectByField(List<sObject> sObjects, String fieldName) {
		
		Map<String, sObject> objectByField = new Map<String, sObject>();
		
		if(sObjects != null && !sObjects.isEmpty() && String.isNotBlank(fieldName)) {
			for(sObject thisObject : sObjects) {
				String key = (String)thisObject.get(fieldName);
				if(String.isNotBlank(key)) {
					objectByField.put(key, thisObject);
				}
			}
		}

		return objectByField;
	}

	/**
	 * Method to send Cancel Request to Appian
	 *
	 * @return 
	*/
	public static void sendCancelRequest(String orderId) {  
		system.debug('orderId: ' + orderId);

		final String END_POINT;
		final Integer TIMEOUT_LIMIT;
		final String NAMED_CREDENTIAL;   
		final String GUID;   
		final String ORDER_ID;   

		// Headers		
		
		Http http = new Http();	 
		HttpRequest req;			  
		HTTPResponse res; 
		String orderJson = '';
		String requestStr;		
		String responseBodyStatus;	
		List<String> eeOrdersId = new List<String>();	
		List<DF_Order__c> orderList = new List<DF_Order__c>();

		Map<String, String> headerMap = new Map<String, String>();

		try {		   

			// Get dfOrder and its details

			if(String.isNotBlank(orderId)) {
			eeOrdersId.add(orderId);
			orderList = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);
			}
		
			if(orderList.size() >0) {
			dfOrder = orderList.get(0);
			}

			dfOrderSettings = getDfOrderSettings();
			NAMED_CREDENTIAL = 'Appian';
			TIMEOUT_LIMIT = 60000;
			GUID = String.isNotBlank(dfOrder.DF_Quote__r.GUID__c)?dfOrder.DF_Quote__r.GUID__c:'';
			ORDER_ID = String.isNotBlank(dfOrder.Order_Id__c)?dfOrder.Order_Id__c:'';
			// Generate Payload   
						 
			requestStr =  '{"eventIdentifier":"'+dfOrderSettings.OrderCancelSource__c+'","externalSystemCorrelationId":"'+GUID+'","eventBody":{"productOrder": {"id": "'+ORDER_ID+'"}}}';
			//system.debug('Payload request (before stripping json nulls): ' + requestStr);
			system.debug('Payload request: ' + requestStr);

			if (String.isEmpty(requestStr)) {
				throw new CustomException(DF_IntegrationUtils.ERR_REQUEST_BODY_EMPTY);
			}

			// Build endpoint
			END_POINT = DF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL;
			system.debug('END_POINT: ' + END_POINT);	 

			getHeaders(dfOrder, headerMap);

			// Generate request 
			req = getHttpRequest(END_POINT, TIMEOUT_LIMIT, requestStr, headerMap);
			system.debug('req: ' + req); 

			// Send and get response			   
			res = http.send(req);

			system.debug('Response Status Code: ' + res.getStatusCode());
			system.debug('Response Body: ' + res.getBody());
		
			if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {						   
				dfOrder.Cancel_Initiated_Stage__c = getDfOrderStage(dfOrder);
				dfOrder.Order_Status__c = DF_Constants.DF_ORDER_STATUS_CANCEL_INITIATED; 
				dfOrder.Order_Sub_Status__c  = '';
				update dfOrder;	
				GlobalUtility.logMessage(DF_Constants.INFO, EE_OrderInflightCancelUtil.class.getName(), ORDER_CANCEL_REQUEST, dfOrder.Order_Id__c, CORRELATION_ID_VAL, SUCCESS_RESP, requestStr, null, 0);
				Map<String, String> orderToTemplateMap = new Map<String, String>();
				orderToTemplateMap.put(dfOrder.Id, DF_Constants.TEMPLATE_ORDER_CANCEL_INITIATED);
				DF_OrderEmailService.sendEmail(orderToTemplateMap);
			} else {
				throw new CustomException('Error occured during callout. Status Code:' +res.getStatusCode());
			}	 
								 
		} catch (Exception e) {
			GlobalUtility.logMessage(DF_Constants.ERROR, EE_OrderInflightCancelUtil.class.getName(), ORDER_CANCEL_REQUEST, dfOrder.Order_Id__c, CORRELATION_ID_VAL, e.getMessage(), requestStr, e, 0);
			throw new CustomException(e.getMessage());
	   }		
	}

	public static void getHeaders(DF_Order__c dfOrder, Map<String, String> headerMap) {	 
		// Headers
		final String TIMESTAMP_VAL;  
		
		try {			   
			TIMESTAMP_VAL = DateTime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
			CORRELATION_ID_VAL = dfOrder.DF_Quote__r.GUID__c;
			System.debug('@@@ dfOrder.Opportunity_Bundle__r.Account__r: ' + dfOrder.Opportunity_Bundle__r.Account__r);
			ACCESS_SEEKER_ID_VAL = dfOrder.Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c;			

			if(String.isNotBlank(CORRELATION_ID_VAL)){
			headerMap.put(DF_IntegrationUtils.CORRELATION_ID, CORRELATION_ID_VAL);						  
 			}else{				
			throw new CustomException(DF_IntegrationUtils.ERR_CORRELATION_ID_NOT_FOUND);
			}   
			if(String.isNotBlank(ACCESS_SEEKER_ID_VAL)){
			headerMap.put(DF_IntegrationUtils.ACCESS_SEEKER_ID, ACCESS_SEEKER_ID_VAL);
 			}else {				
			throw new CustomException(DF_IntegrationUtils.ERR_ACCESS_SEEKER_ID_NOT_FOUND);
			}
			headerMap.put(DF_IntegrationUtils.TIMESTAMP, TIMESTAMP_VAL);
			headerMap.put(DF_Constants.CONTENT_TYPE, DF_Constants.CONTENT_TYPE_JSON);		   
			headerMap.put(DF_Constants.HTTP_HEADER_ACCEPT, DF_Constants.CONTENT_TYPE_JSON);	
			headerMap.put(BUSINESS_CHANNEL_HEADER_NAME,DF_Constants.BUSINESS_CHANNEL);
			headerMap.put(ACTIVITY_NAME_HEADER_NAME,ACTIVITY_NAME_HEADER_VALUE);
			headerMap.put(MSGNAME_HEADER_NAME,MSGNAME_HEADER_VALUE);
			headerMap.put(BUSINESSSERVICENAME_HEADER_NAME,BUSINESSSERVICENAME_HEADER_VALUE);
			headerMap.put(BUSINESSPROCESSVERSION_HEADER_NAME,BUSINESSPROCESSVERSION_HEADER_VALUE);
			headerMap.put(BUSINESSSERVICEVERSION_HEADER_NAME,BUSINESSSERVICEVERSION_HEADER_VALUE);
			headerMap.put(MSGTYPE_HEADER_NAME,MSGTYPE_HEADER_VALUE); 
			headerMap.put(SECURITY_HEADER_NAME,SECURITY_HEADER_VALUE);
			headerMap.put(COMMUNICATIONPATTERN_HEADER_NAME,COMMUNICATIONPATTERN_HEADER_VALUE);
			headerMap.put(ORDER_PRIORITY_NAME_HEADER_NAME, ORDER_PRIORITY_NAME_HEADER_VALUE);

			system.debug('::header::'+headerMap); 
												 
		} catch (Exception e) {  
			GlobalUtility.logMessage(DF_Constants.ERROR, EE_OrderInflightCancelUtil.class.getName(), ORDER_CANCEL_REQUEST+':getHeaders', dfOrder.Order_Id__c, CORRELATION_ID_VAL, e.getMessage(), String.valueOf(headerMap), e, 0);
			throw new CustomException(e.getMessage());
		}
	}

	public static HttpRequest getHttpRequest(String endPoint, Integer timeoutLimit, String requestStr, Map<String, String> headerMap) {													  
		HttpRequest req = new HttpRequest();		
		
		try {			   
			req.setEndpoint(endpoint);	  
			req.setMethod(DF_Constants.HTTP_METHOD_POST);			 
			req.setTimeout(timeoutLimit);
			
			// Set all headers
			for (String key : headerMap.keySet()){
			req.setHeader(key,headerMap.get(key));
			}
			req.setBody(requestStr);								 
		} catch (Exception e) {  
			throw new CustomException(e.getMessage());
		}
			   
		return req;
	}

	/**
	 * Create the necessary custom settings for DF Order Cancel
	 *
	 * @param	 void
	 *
	 * @return	 List<DF_Order_Cancel_Settings__c> eeOrderCancelCustomSettings
	*/
	public static List<DF_Order_Cancel_Settings__c> createEEOrderCancelCustomSettings() {
		
		List<DF_Order_Cancel_Settings__c> deleteList = new List<DF_Order_Cancel_Settings__c>();
		deleteList = [SELECT Id FROM DF_Order_Cancel_Settings__c WHERE Order_Record_Type__c = 'Enterprise_Ethernet'];
		DELETE deleteList;
		
		List<DF_Order_Cancel_Settings__c> eeOrderCancelCustomSettings = new List<DF_Order_Cancel_Settings__c> ();	
		eeOrderCancelCustomSettings.add(new DF_Order_Cancel_Settings__c(Name = 'Fee_001', Order_Record_Type__c = 'Enterprise_Ethernet', Order_Status__c = 'InProgress', Order_Sub_Status__c = 'OrderAccepted', Order_Stage__c = 'Plan', Fee__c = 5000.00));
		eeOrderCancelCustomSettings.add(new DF_Order_Cancel_Settings__c(Name = 'Fee_002', Order_Record_Type__c = 'Enterprise_Ethernet', Order_Status__c = 'InProgress', Order_Sub_Status__c = 'OrderScheduled', Order_Stage__c = 'Design', Fee__c = 10000.00));
		eeOrderCancelCustomSettings.add(new DF_Order_Cancel_Settings__c(Name = 'Fee_003', Order_Record_Type__c = 'Enterprise_Ethernet', Order_Status__c = 'InProgress', Order_Sub_Status__c = 'ConstructionStarted', Order_Stage__c = 'Build', Fee__c = 15000.00));
		eeOrderCancelCustomSettings.add(new DF_Order_Cancel_Settings__c(Name = 'Fee_004', Order_Record_Type__c = 'Enterprise_Ethernet', Order_Status__c = 'InProgress', Order_Sub_Status__c = 'ServiceTestCompleted', Order_Stage__c = 'Build', Fee__c = 15000.00));

		return eeOrderCancelCustomSettings;
	}


	/**
	 * Get the DF Order stage based on recordtype, status and substatus
	 *
	 * @param	 String recordType
	 * @param	 String status
	 * @param	 String subStatus
	 * 
	 * @return	 String
	*/
	public static String getDfOrderStage(DF_Order__c dfOrder) {

		String recordType = dfOrder.RecordType.DeveloperName;
		String status = dfOrder.Order_Status__c;
		String subStatus = dfOrder.Order_Sub_Status__c;
		String orderStage = NO_CHARGE;

		List<DF_Order_Cancel_Settings__c> dfOrderCancelSettings = getDfOrderCancelSettings();

		for(DF_Order_Cancel_Settings__c dfOrderCancelSetting : dfOrderCancelSettings) {
			String orderRecordType = dfOrderCancelSetting.Order_Record_Type__c;
			String orderStatus = dfOrderCancelSetting.Order_Status__c;
			String orderSubStatus = dfOrderCancelSetting.Order_Sub_Status__c;

			if(recordType.equals(orderRecordType) 
					&& status.equals(orderStatus)
					&& subStatus.equals(orderSubStatus)) {
				orderStage = dfOrderCancelSetting.Order_Stage__c;
				if(orderStage.equals(DESIGN) && isChargeNotApplicable(getDfOrderInfo(dfOrder))) {
					orderStage = DESIGN_NO_CHARGE;
					break; 
				}				
			}
		}

		return orderStage;
	}


	/**
	 * Get the cancellation fee based on Order Stage
	 *
	 * @param	 String cancelInitiatedStage
	 *
	 * @return	 Decimal
	*/
	public static Decimal getEEOrderCancellationFee(String cancelInitiatedStage) {
		Decimal fee = null;

		List<DF_Order_Cancel_Settings__c> dfOrderCancelSettings = getDfOrderCancelSettings();
		for(DF_Order_Cancel_Settings__c dfOrderCancelSetting : dfOrderCancelSettings) {
			String orderRecordType = dfOrderCancelSetting.Order_Record_Type__c;
			String orderStage = dfOrderCancelSetting.Order_Stage__c;
			if(RECORD_TYPE_DEVELOPER_NAME_EE.equalsIgnoreCase(orderRecordType) 
				&& cancelInitiatedStage.equalsIgnoreCase(orderStage)) {
				fee = dfOrderCancelSetting.Fee__c;
				break;
			} 
		}

		return fee; 
	}


	/**
	 * Get the DF Order Cancel custom settings
	 *
	 * @return	 List<DF_Order_Cancel_Settings__c>
	*/
	public static List<DF_Order_Cancel_Settings__c> getDfOrderCancelSettings() {
		if(EE_OrderInflightCancelUtil.dfOrderCancelSettings == null 
				|| EE_OrderInflightCancelUtil.dfOrderCancelSettings.isEmpty()) {
			EE_OrderInflightCancelUtil.dfOrderCancelSettings = DF_Order_Cancel_Settings__c.getAll().values();
		}

		return EE_OrderInflightCancelUtil.dfOrderCancelSettings;
	}

	/**
	 * Returns the list of EE Orders to be charged
	 *
	 * @param	 List<DF_Order__c> eeOrders
	 *
	 * @return	 List<DF_Order__c>
	*/
	public static List<DF_Order__c> getEEOrdersToCharge(List<DF_Order__c> eeOrders) {

		Map<String, DF_Order__c> eeOrdersToCharge = new Map<String, DF_Order__c>();
		
		if(eeOrders != null && !eeOrders.isEmpty()) {
			for(DF_Order__c eeOrder : eeOrders) {
				if(String.isNotBlank(eeOrder.Cancel_Initiated_Stage__c) 
						&& !(NO_CHARGE.equalsIgnoreCase(eeOrder.Cancel_Initiated_Stage__c)
							|| DESIGN_NO_CHARGE.equalsIgnoreCase(eeOrder.Cancel_Initiated_Stage__c))) {
					eeOrdersToCharge.put(eeOrder.Id, eeOrder);
				}
			}
		}

		return eeOrdersToCharge.values();
	}

	/**
	 * Process the EE Orders and get the Accepted & Scheduled date from history
	 *
	 * @param	 DF_Order__c eeOrder
	 *
	 * @return	 EE_OrderInfo
	*/
	public static EE_OrderInfo getDfOrderInfo(DF_Order__c eeOrder) {
		
		EE_OrderInfo eeOrderInfo = null;
		
		System.debug('@@@ eeOrder.Histories: '+ eeOrder.Histories);

		if(eeOrder != null && eeOrder.Histories != null && !eeOrder.Histories.isEmpty()) {
			List<DF_Order__History> eeOrderHistories = eeOrder.Histories;
			DateTime eeOrderAcceptedDate = null;
			DateTime eeOrderScheduledDate = null;
			DateTime eeOrderCancelInitiatedDate = null;
			for(DF_Order__History eeOrderHistory : eeOrderHistories) {
				String oldValue = (String) eeOrderHistory.OldValue;
				String newValue = (String) eeOrderHistory.NewValue;

				eeOrderAcceptedDate = eeOrderAcceptedDate == null 
										&& (oldValue == null || STR_EMPTY.equalsIgnoreCase(oldValue))
										&& DF_Constants.DF_ORDER_SUB_STATUS_ORDER_ACCEPTED.equalsIgnoreCase(newValue) 
											? eeOrderHistory.CreatedDate 
											: eeOrderAcceptedDate;

				eeOrderScheduledDate = eeOrderScheduledDate == null 
										&& DF_Constants.DF_ORDER_SUB_STATUS_ORDER_ACCEPTED.equalsIgnoreCase(oldValue) 
										&& DF_Constants.DF_ORDER_SUB_STATUS_ORDER_SCHEDULED.equalsIgnoreCase(newValue)
											? eeOrderScheduledDate = eeOrderHistory.CreatedDate
											: eeOrderScheduledDate;
				
				eeOrderCancelInitiatedDate = Date.today();
			}
			eeOrderInfo = new EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		}

		return eeOrderInfo;
	}

	/**
	 * Check if the charge is not applicable
	 *
	 * @param	 DF_Order__c eeOrder
	 *
	 * @return	 Boolean
	*/
	public static Boolean isChargeNotApplicable(EE_OrderInfo eeOrderInfo) {

		// Get the Custom settings
		Integer nbnCommitmentDaysSetting  = DF_AS_Custom_Settings__c.getValues('NBN_Commitment_Days') != null ? Integer.valueOf(DF_AS_Custom_Settings__c.getValues('NBN_Commitment_Days').Value__c) : 0;
		Integer nbnConsiderationDaysSetting  = DF_AS_Custom_Settings__c.getValues('NBN_Consideration_Days') != null ? Integer.valueOf(DF_AS_Custom_Settings__c.getValues('NBN_Consideration_Days').Value__c) : 0;

		if(eeOrderInfo == null 
				|| eeOrderInfo.scheduledDate == null 
				|| eeOrderInfo.acceptedDate == null 
				|| eeOrderInfo.cancelInitiatedDate == null 
				|| eeOrderInfo.dfOrder == null 
				|| eeOrderInfo.dfOrder.NBN_Commitment_Date__c == null 
				|| nbnCommitmentDaysSetting <= 0 
				|| nbnConsiderationDaysSetting <= 0) {
			 return false;
		}

		// Get the EE Order accepted date
		Date eeOrderAcceptedDate = eeOrderInfo.acceptedDate.date();
		Integer nbnCommitmentDays = eeOrderAcceptedDate.daysBetween(DF_AS_GlobalUtility.addBussinessDays(eeOrderAcceptedDate, nbnCommitmentDaysSetting)); 
		Integer nbnConsiderationDays = eeOrderAcceptedDate.daysBetween(DF_AS_GlobalUtility.addBussinessDays(eeOrderAcceptedDate, nbnConsiderationDaysSetting));

		// Get the EE Order Cancel Initiated Date
		Date eeOrderCancelInitiatedDate =  eeOrderInfo.cancelInitiatedDate.date();
		// Get the EE Order Scheduled date
		Date eeOrderScheduledDate = eeOrderInfo.scheduledDate.date();
		// Check if the cancel is initiated after the nbnConsiderationDays
		if(eeOrderScheduledDate.daysBetween(eeOrderCancelInitiatedDate) > nbnConsiderationDays) {
			return false;
		}
		// Get the NBN Allowed delivery date
		Date stdCommitmentDate =  eeOrderAcceptedDate.addDays(nbnCommitmentDays - 1);
		// Get the Committed delivery date
		Date committedDeliveryDate = eeOrderInfo.dfOrder.NBN_Commitment_Date__c;
		// Get the Customer requested date. If customer requested date is null, then set to EE Order accepted date + NBN Commitment days
		Date customerRequestedDate = eeOrderInfo.dfOrder.Customer_Required_Date__c;	
		// Check if NBN has committed early
		Boolean hasNBNCommittedEarly = committedDeliveryDate.daysBetween(stdCommitmentDate) >= 0;

		// Check the number of days between Order Accepted date and Customer Requested Date
		if (customerRequestedDate != null) {
			// Calculate customed requested as days from Order accepted date
			Integer customerRequestedDays = eeOrderAcceptedDate.daysBetween(customerRequestedDate);
			// Check if Customer has requested before standard NBN commitment days
			Boolean hasCustomerRequestedEarly = customerRequestedDays - nbnCommitmentDays < 0;
			// Check if NBN has committed beyond standard NBN commitment days
			Boolean hasNbnCommittedLaterThanStd = committedDeliveryDate > stdCommitmentDate;
			// Check if NBN has committed beyond custmomer requested days
			Boolean hasNbnCommittedLaterThanCustomer = committedDeliveryDate > customerRequestedDate;

			if(hasCustomerRequestedEarly ){
				return (!hasNBNCommittedEarly && hasNbnCommittedLaterThanStd ) ? true: false;
			}
			else{
				return hasNbnCommittedLaterThanCustomer ? true: false; // false =>charge customer
			}
		}
		else{
			return hasNBNCommittedEarly ? false: true; //true => do not charge customer
		}

	}

	public static Map<Id, DF_Order__c> getDfOrdersToChargeById(List<DF_Order__c> eeOrdersAll) {
		Map<Id, DF_Order__c> eeOrdersById = new Map<Id, DF_Order__c>();

		if(eeOrdersAll != null && !eeOrdersAll.isEmpty()) {
			for(DF_Order__c eeOrder : eeOrdersAll) {
				String cancelInitiatedStage = eeOrder.Cancel_Initiated_Stage__c;
				if(String.isNotBlank(cancelInitiatedStage) 
					&& !NO_CHARGE.equalsIgnoreCase(cancelInitiatedStage) 
					&& !DESIGN_NO_CHARGE.equalsIgnoreCase(cancelInitiatedStage)) {
					eeOrdersById.put(eeOrder.Id, eeOrder);
				}
			}
		}
		return eeOrdersById;
	}

	public static Map<Id, DF_Order__c> getEEOrdersToNotChargeById(Map<Id, DF_Order__c> eeOrdersAllById, Map<Id, DF_Order__c> eeOrdersToChargeById) {
		
		Map<Id, DF_Order__c> eeOrdersById = new Map<Id, DF_Order__c>();
		
		Set<Id> eeOrdersAll = new Set<Id>();
		Set<Id> eeOrdersToCharge = new Set<Id>();
		Set<Id> eeOrdersNoCharge = new Set<Id>();
		
		if(eeOrdersAllById != null && eeOrdersToChargeById != null && !eeOrdersAllById.isEmpty()) {
			eeOrdersAll = eeOrdersAllById.keySet();
			eeOrdersToCharge = eeOrdersToChargeById.keySet();
		}

		if(!eeOrdersAll.isEmpty()) {
			eeOrdersNoCharge = eeOrdersAll;
			eeOrdersNoCharge.removeAll(eeOrdersToCharge);
		}

		if(!eeOrdersNoCharge.isEmpty()) {
			for(Id eeOrderId : eeOrdersNoCharge) {
				eeOrdersById.put(eeOrderId, eeOrdersAllById.get(eeOrderId));
			}
		}

		return eeOrdersById;
	}


	/**
	 * Class to hold additional details for in-flight cancel processing
	 */
	public class EE_OrderInfo {
		DF_Order__c dfOrder {get; set;}
		DateTime acceptedDate {get; set;}
		DateTime scheduledDate {get; set;}
		DateTime cancelInitiatedDate {get; set;}

		public EE_OrderInfo(DF_Order__c eeOrder, DateTime eeOrderAcceptedDate, DateTime eeOrderScheduledDate, DateTime eeOrderCancelInitiatedDate) {
			dfOrder = eeOrder;
			acceptedDate = eeOrderAcceptedDate;
			scheduledDate = eeOrderScheduledDate;
			cancelInitiatedDate = eeOrderCancelInitiatedDate;
		}
	}
}