/*------------------------------------------------------------------------
Author:			Bharath Kumar
Company:	   	Contractor
Description:   	A test class created to test EE_OrderInflightCancelUtil methods			   

History
27/11/2018	  Bharath Kumar	 Creation
--------------------------------------------------------------------------*/

@IsTest
public class EE_OrderInflightCancelUtilTest {

	public static final String RECORD_TYPE_NAME_EE = 'Enterprise Ethernet';
	public static final String OBJECT_NAME_DF_ORDER = 'DF_Order__c';

	@testSetup static void testDataSetup() {

		final Id eeOrderRecordTypeId = Schema.getGlobalDescribe().get(OBJECT_NAME_DF_ORDER).getDescribe().getRecordTypeInfosByName().get(RECORD_TYPE_NAME_EE).getRecordTypeId();
		
		DF_TestData.createDFCommUser();
		List<Account> accList = [SELECT Id FROM Account LIMIT 1];

		System.debug('@@@ accList[0]: ' + accList[0]);

		Id oppId = DF_TestService.getQuoteRecords(accList[0]);
		
		List<Opportunity> oppList = [SELECT Id FROM OPPORTUNITY WHERE Opportunity_Bundle__c = :oppId];
		
		Opportunity opp = oppList.get(0);
		
		List<DF_Quote__c> dfQuoteList = [SELECT 
											Id, Address__c, LAPI_Response__c, RAG__c, Fibre_Build_Cost__c, Opportunity_Bundle__c, Opportunity__c
									 	FROM 
									 		DF_Quote__c 
									 	WHERE 
									 		Opportunity__c = :oppList.get(0).Id 
									 	LIMIT 1];

		DF_Quote__c dfQuote1 = dfQuoteList.get(0);
		dfQuote1.GUID__c = '123456789';
		UPDATE dfQuote1;
		
		List<DF_Order__c> eeOrders = new List<DF_Order__c>();
		
		DF_Order__c eeOrder = new DF_Order__c();
		eeOrder.DF_Quote__c = dfQuoteList[0].Id;
		eeOrder.OrderType__c = 'Connect';
		eeOrder.Opportunity_Bundle__c = [SELECT Id FROM DF_Opportunity_Bundle__c LIMIT 1].Id;
		eeOrder.Order_Id__c = 'BCO-12345';
		eeOrder.RecordTypeId = eeOrderRecordTypeId;
		eeOrders.add(eeOrder);
		
		INSERT eeOrders;

		INSERT EE_OrderInflightCancelUtil.createEEOrderCancelCustomSettings();

		DF_AS_Custom_Settings__c nbnCommitmentDaysSetting  = new DF_AS_Custom_Settings__c(Name='NBN_Commitment_Days', Value__c='65');
		INSERT nbnCommitmentDaysSetting;

		DF_AS_Custom_Settings__c nbnConsiderationDaysSetting  = new DF_AS_Custom_Settings__c(Name='NBN_Consideration_Days', Value__c='5');
		INSERT nbnConsiderationDaysSetting;

	}

	@isTest
	static void test_getEEOrderRecordTypeId() {
		Test.startTest();
		Id eeOrderRecordTypeId = EE_OrderInflightCancelUtil.getEEOrderRecordTypeId();
		System.assertNotEquals(eeOrderRecordTypeId, null);
		Test.stopTest();
	}

	@isTest
	static void test_getDfOrderInfo() {
		List<DF_Order__c> eeOrders = getEEOrders();
		DF_Order__History dfOrderHistory = new DF_Order__History(ParentId = eeOrders[0].Id, Field = 'Order_Sub_Status__c');
        insert dfOrderHistory;

		Test.startTest();
		List<DF_Order__c> eeOrdersWithHistory = EE_OrderInflightCancelUtil.getEEOrdersById(getEEOrdersId());
		System.debug('@@@ eeOrdersWithHistory: '+ eeOrdersWithHistory);
				
		EE_OrderInflightCancelUtil.EE_OrderInfo eeOrderInfo = EE_OrderInflightCancelUtil.getDfOrderInfo(eeOrdersWithHistory[0]);
		System.assertNotEquals(eeOrderInfo, null);
		Test.stopTest();
	}

	@isTest
	static void test_sendCancelRequest() {
		List<String> eeOrdersId = getEEOrdersId();

		Map<string, string> headerMap = new  Map<string, string>();
		headerMap.put('Content-Type', 'application/json');
			
		EE_HttpCalloutMock mockObj = new  EE_HttpCalloutMock(200, 'ok', '{}', headerMap);		

		Test.startTest();
			Test.setMock(HttpCalloutMock.class, mockObj);
			EE_OrderInflightCancelUtil.sendCancelRequest(eeOrdersId[0]);
			List<DF_Order__c> eeOrders = getEEOrders();
			System.assertEquals(eeOrders[0].Order_Status__c, DF_Constants.DF_ORDER_STATUS_CANCEL_INITIATED);
		Test.stopTest();
	}

	@isTest
	static void test_getEEOrdersToChargeFound() {
		List<DF_Order__c> eeOrders = getEEOrders();		
		System.debug('@@@ eeOrders before: ' + eeOrders);
		eeOrders.get(0).Cancel_Initiated_Stage__c = EE_OrderInflightCancelUtil.DESIGN;
		update eeOrders.get(0);
		System.debug('@@@ eeOrders after: ' + eeOrders);

		Test.startTest();
		List<DF_Order__c> eeOrdersToCharge = EE_OrderInflightCancelUtil.getEEOrdersToCharge(eeOrders);
		System.assertNotEquals(eeOrdersToCharge, null);
		System.assertEquals(eeOrdersToCharge.size(), 1);
		Test.stopTest();
	}

	@isTest
	static void test_getEEOrdersToChargeNotFound() {
		List<DF_Order__c> eeOrders = getEEOrders();
		Test.startTest();
		List<DF_Order__c> eeOrdersToCharge = EE_OrderInflightCancelUtil.getEEOrdersToCharge(eeOrders);
		System.debug('@@@ eeOrdersToCharge: ' + eeOrdersToCharge);
		System.assertEquals(eeOrdersToCharge.size(), 0);
		Test.stopTest();
	}

	@isTest
	static void test_getEEOrdersById() {
		List<String> eeOrdersId = getEEOrdersId();
		Test.startTest();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);
		System.assertNotEquals(eeOrders, null);
		System.assertEquals(eeOrders.size(), 1);
		Test.stopTest();
	}

	@isTest
	static void test_getEEOrdersByQuoteGUID() {
		List<String> dfQuotesGuid = getDFQuotesGUID();
		Test.startTest();
		Map<Id, DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersByQuoteGUID(dfQuotesGuid);
		System.assertNotEquals(eeOrders, null);
		System.assertEquals(eeOrders.size(), 1);
		Test.stopTest();
	}

	@isTest
	static void test_getRecordTypeId() {
		Test.startTest();
		Id eeOrderRecordTypeId = EE_OrderInflightCancelUtil.getRecordTypeId(RECORD_TYPE_NAME_EE, OBJECT_NAME_DF_ORDER);
		System.assertNotEquals(eeOrderRecordTypeId, null);
		Test.stopTest();
	}
	
	@isTest
	static void test_getDFOrderSettings() {
		Test.startTest();
		DF_Order_Settings__mdt dfOrderSettings = EE_OrderInflightCancelUtil.getDFOrderSettings();
		System.assertNotEquals(dfOrderSettings, null);
		System.assertEquals(dfOrderSettings.OrderCancelEventCode__c, 'ORDCANCELLED101');
		Test.stopTest();
	}

	@isTest
	static void test_getFieldValues() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);
		List<String> fieldValues = EE_OrderInflightCancelUtil.getFieldValues(eeOrders, 'Order_Id__c', Null);
		System.assertNotEquals(fieldValues, null);
		System.assertNotEquals(fieldValues.size(), 0);
		System.assertEquals(fieldValues.size(), 1);
		System.assertEquals(fieldValues.get(0), 'BCO-12345');
		Test.stopTest();
	}

	@isTest
	static void test_getObjectByField() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);
		Map<String, sObject> objectByField = EE_OrderInflightCancelUtil.getObjectByField(eeOrders, 'Id');
		System.assertNotEquals(objectByField, null);
		System.assertNotEquals(objectByField.size(), 0);
		System.assertEquals(objectByField.size(), 1);
		List<String> keys = new List<String>();
		keys.addAll(objectByField.keySet());
		String key = keys.get(0);
		System.assertEquals(((DF_Order__c)objectByField.get(key)).Order_Id__c, 'BCO-12345');
		Test.stopTest();
	}

	@isTest
	static void test_getEEOrderCancellationFee() {

		Test.startTest();
		Decimal fee = EE_OrderInflightCancelUtil.getEEOrderCancellationFee('plan');
		System.assertNotEquals(fee, null);
		System.assertEquals(fee, 5000.00);
		Test.stopTest();
	}
	@isTest
	static void test_getDfOrderStage() {

		Test.startTest();
		DF_Order__c order =  [SELECT Id,RecordType.DeveloperName FROM DF_Order__c WHERE Order_Id__c IN ('BCO-12345') LIMIT 1];
		// Id recordTypeId = Schema.SObjectType.DF_Order__c.getRecordTypeInfosByName()
        //           .get('Enterprise_Ethernet').getRecordTypeId();
		// order.RecordType.DeveloperName = 'Enterprise_Ethernet';
		order.Order_Status__c = 'InProgress';
		order.Order_Sub_Status__c = 'OrderAccepted';
		Test.createStub(EE_OrderInflightCancelUtil.class, new EE_OrderInFlightCancelUtilMockProvider());
		System.assertEquals('Plan', EE_OrderInflightCancelUtil.getDfOrderStage(order));
		Test.stopTest();
	}



	@isTest
	static void test_isChargeNotApplicable_CancelInit_before_5d() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 14);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 03, 20);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2018, 12, 25);
		//Cancel initiated before 5 days and  NBN committed to deliver after SLA , and after CRD, so its NOT chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(true, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}

	@isTest
	static void test_isChargeNotApplicable_CancelInit_on_5d() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 15);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 03, 20);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2018, 12, 25);
		//Cancel initiated on 5th days and NBN committed to deliver after SLA , and after CRD, its NOT chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(true, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}

	@isTest
	static void test_isChargeNotApplicable_CancelInit_after_5d() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 18);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 03, 20);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2018, 12, 25);
		//Cancel initiated after 5 days so even if NBN committed to deliver after SLA , and after CRD, its chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(false, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}

//(Customer want before SLA)
	//CASE1 - NBN will deliver  after  customer want and  after SLA 
	@isTest
	static void test_isChargeNotApplicable_CDD_after_CRD_c1() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 03, 20);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2018, 12, 25);
		//NBN committed to deliver after SLA , and after CRD, so its NOT chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(true, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}

	//CASE2 - NBN will deliver  after  customer want and before SLA 
	@isTest
	static void test_isChargeNotApplicable_CRD_b4_CDD_c2() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2018, 12, 25);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2018, 12, 20);
		//NBN committed to deliver before SLA , but after CRD, so its chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(false, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}

	//CASE3 - NBN will deliver  after  customer want and on  SLA
	@isTest
	static void test_isChargeNotApplicable_CDD_after_CRD_c3() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 2, 12);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2018, 12, 25);
		//NBN committed to deliver before SLA , but before CRD, so its chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(false, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}

	//CASE4 - NBN will deliver  before customer want 
	@isTest
	static void test_isChargeNotApplicable_CDD_b4_CRD_c4() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2018, 12, 20);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2018, 12, 25);
		//NBN committed to deliver before SLA , but before CRD, so its chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(false, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}
	//CASE5 - NBN will deliver  on  customer want 
	@isTest
	static void test_isChargeNotApplicable_CDD_on_CRD_c5() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2018, 12, 25);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2018, 12, 25);
		//NBN committed to deliver before SLA , but before CRD, so its chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(false, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}


//(Customer want after SLA)

	//CASE6 - NBN will deliver  before customer want and after SLA 
	@isTest
	static void test_isChargeNotApplicable_CDD_b4_CRD_after_SLA_c6() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 3, 2);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2019, 3, 20);
		//NBN committed to deliver before SLA , but before CRD, so its chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(false, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}

	//CASE7 - NBN will deliver  on customer want and after SLA 
	@isTest
	static void test_isChargeNotApplicable_CDD_on_CRD_after_SLA_c7() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 3, 20);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2019, 3, 20);
		//NBN committed to deliver before SLA , but before CRD, so its chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(false, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}

	//CASE8 - NBN will deliver  after customer want and after SLA 
	@isTest
	static void test_isChargeNotApplicable_CDD_after_CRD_after_SLA_c8() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 3, 25);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2019, 3, 20);
		//NBN committed to deliver before SLA , but before CRD, so its chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(true, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}


//(Customer want on SLA)

	//CASE9 - NBN will deliver before customer want and on SLA  
	@isTest
	static void test_isChargeNotApplicable_CDD_b4_CRD_on_SLA_c9() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2018, 12, 25);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2019, 2, 12);
		//NBN committed to deliver before SLA , but before CRD, so its chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(false, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}
	//CASE10 - NBN will deliver  on customer want and on SLA 
	@isTest
	static void test_isChargeNotApplicable_CDD_on_CRD_on_SLA_c10() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 2, 12);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2019, 2, 12);
		//NBN committed to deliver before SLA , but before CRD, so its chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(false, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}

	//CASE11 - NBN will delivering  after  customer want and on SLA  
	@isTest
	static void test_isChargeNotApplicable_CDD_after_CRD_on_SLA_c11() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 03, 20);
		eeOrder.Customer_Required_Date__c = Date.newInstance(2019, 2, 12);
		//NBN committed to deliver before SLA , but before CRD, so its chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(true, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}

// (Customer not specified a CRD)
	@isTest
	static void test_isChargeNotApplicable_CRD_notGiven_CDD_after_SLA() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 03, 20);		
		//NBN committed to deliver after SLA , and CRD not given, so its NOT chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(true, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}

	@isTest
	static void test_isChargeNotApplicable_CRD_notGiven_CDD_on_SLA() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 03, 15);		
		//NBN committed to deliver on SLA , and CRD not given, so its NOT chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(true, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}

	@isTest
	static void test_isChargeNotApplicable_CRD_notGiven_CDD_b4_SLA() {
		Test.startTest();
		List<String> eeOrdersId = getEEOrdersId();
		List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

		DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
		DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
		DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 11);
		DF_Order__c eeOrder = eeOrders.get(0);
		eeOrder.NBN_Commitment_Date__c = Date.newInstance(2018, 12, 20);		
		//NBN committed to deliver on SLA , and CRD not given, so its chargable
		EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
		System.assertEquals(false, EE_OrderInflightCancelUtil.isChargeNotApplicable(orderInfo));
		Test.stopTest();
	}

	private static List<String> getEEOrdersId() {
		List<String> eeOrderIds = new List<String>{'BCO-12345'};
		List<DF_Order__c> dfOrders =  [SELECT Id FROM DF_Order__c WHERE Order_Id__c IN :eeOrderIds];
		List<String> eeOrdersId = new List<String>();
		for(DF_Order__c dfOrder : dfOrders) {
			eeOrdersId.add(dfOrder.Id);
		}
		return eeOrdersId;
	}

	private static List<DF_Order__c> getEEOrders() {
		List<String> eeOrderIds = new List<String>{'BCO-12345'};
		List<DF_Order__c> dfOrders =  [SELECT Id, Cancel_Initiated_Stage__c, Order_Status__c FROM DF_Order__c WHERE Order_Id__c IN :eeOrderIds];		
		return dfOrders;
	}

	private static List<String> getDFQuotesGUID() {
		List<String> dfQuotesGuid = new List<String>{'123456789'};
		return dfQuotesGuid;
	}
		
	public class EE_OrderInFlightCancelUtilMockProvider implements System.StubProvider {
		private List<String> getEEOrdersId() {
			List<String> eeOrderIds = new List<String>{'BCO-12345'};
			List<DF_Order__c> dfOrders =  [SELECT Id FROM DF_Order__c WHERE Order_Id__c IN :eeOrderIds];
			List<String> eeOrdersId = new List<String>();
			for(DF_Order__c dfOrder : dfOrders) {
				eeOrdersId.add(dfOrder.Id);
			}
			return eeOrdersId;
		}
		public Object handleMethodCall(Object stubbedObject, String stubbedMethodName, 
			Type returnType, List<Type> listOfParamTypes, List<String> listOfParamNames, 
			List<Object> listOfArgs) {
			
			// The following debug statements show an example of logging 
			// the invocation of a mocked method.
		
			// You can use the method name and return type to determine which method was called.
			System.debug('Name of stubbed method: ' + stubbedMethodName);
			System.debug('Return type of stubbed method: ' + returnType.getName());
			
			// You can also use the parameter names and types to determine which method 
			// was called.
			for (integer i =0; i < listOfParamNames.size(); i++) {
				System.debug('parameter name: ' + listOfParamNames.get(i));
				System.debug('  parameter type: ' + listOfParamTypes.get(i).getName());
			}
			
			// This shows the actual parameter values passed into the stubbed method at runtime.
			System.debug('number of parameters passed into the mocked call: ' + 
				listOfArgs.size());
			System.debug('parameter(s) sent into the mocked call: ' + listOfArgs);
			
			// This is a very simple mock provider that returns a hard-coded value 
			// based on the return type of the invoked.
			if (stubbedObject == 'EE_OrderInflightCancelUtil' && stubbedMethodName == 'getDfOrderInfo' ){
				List<String> eeOrdersId = getEEOrdersId();
				List<DF_Order__c> eeOrders = EE_OrderInflightCancelUtil.getEEOrdersById(eeOrdersId);

				DateTime eeOrderAcceptedDate = DateTime.newInstance(2018, 12, 9);
				DateTime eeOrderScheduledDate = DateTime.newInstance(2018, 12, 10);
				DateTime eeOrderCancelInitiatedDate = DateTime.newInstance(2018, 12, 14);
				DF_Order__c eeOrder = eeOrders.get(0);
				eeOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 03, 20);
				eeOrder.Customer_Required_Date__c = Date.newInstance(2018, 12, 25);
				//Cancel initiated before 5 days and  NBN committed to deliver after SLA , and after CRD, so its NOT chargable
				EE_OrderInflightCancelUtil.EE_OrderInfo orderInfo = new EE_OrderInflightCancelUtil.EE_OrderInfo(eeOrder, eeOrderAcceptedDate, eeOrderScheduledDate, eeOrderCancelInitiatedDate);
				return orderInfo;
				
			}
			else if (stubbedObject == 'EE_OrderInflightCancelUtil' && stubbedMethodName =='getDfOrderCancelSettings'){
				List<DF_Order_Cancel_Settings__c> cancelSettings = new List<DF_Order_Cancel_Settings__c>();
				DF_Order_Cancel_Settings__c cancelSetting = new DF_Order_Cancel_Settings__c();
				cancelSetting.Order_Record_Type__c = 'Enterprise_Ethernet';
				cancelSetting.Order_Status__c = 'InProgress';
				cancelSetting.Order_Sub_Status__c = 'OrderAccepted';
				cancelSettings.add(cancelSetting);
				return cancelSettings;

			}
			else 
				return stubbedObject;
		}
	}
}