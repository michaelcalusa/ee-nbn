@IsTest
public class EE_OrderUtility_Test {

    
    /**
    * Tests IsModifyInProgress method
    */
    @isTest static void IsModifyInProgressMethodTest(){
    
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        
        Opportunity oppo = DF_TestService.getOppRecord(acc.Id);
        
        csord__Subscription__c sub = DF_TestService.createCSSubscriptionRecord(acc.Id, oppo);
        
        Test.startTest();
        Boolean result=EE_OrderUtility.IsModifyInProgress('BPI000000012911');
        System.assertEquals(true, result);
        Test.stopTest();  
    
    }
    /**
    * Tests clonedfQuote method
    */
    @isTest static void clonedfQuoteMethod()
    {
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        
        DF_Opportunity_Bundle__c oppBundle = DF_TestData.createOpportunityBundle(acc.id);
        insert oppBundle;
        
        Opportunity opp = DF_TestService.getOppRecord(acc.Id);
        
        DF_Quote__c quote1 = DF_TestData.createDFQuote('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia', '-15.451568', '145.178216', 'LOC000035375038', opp.id, oppBundle.id, 1500.00, 'Red', null);
        insert quote1;
        
        DF_Quote__c newQuote=EE_OrderUtility.clonedfQuote(quote1, 'Add new OVC');
        System.assertNotEquals(null, newQuote);
        
        //Test to find quote object
        DF_Quote__c quoteObj=EE_OrderUtility.getDfQuote(newQuote.id);
        System.assertNotEquals(null, quoteObj);
    }
    /**
    * Tests clonedfOrder method
    */
    @isTest static void clonedfOrderMethod()
    {
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        
        DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acc.ID);
        insert opptyBundle;
        
        Opportunity oppty = DF_TestData.createOpportunity('Test Opp');
        oppty.Opportunity_Bundle__c = opptyBundle.id;
        insert oppty;
                    
        csord__Subscription__c subs = DF_TestService.createCSSubscriptionRecord(acc.Id, oppty);
        
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        
        List<cscfga__Product_Configuration__c> prodConfigList = DF_CS_API_Util.getProductBasketConfigs(basketObj.Id);
        prodConfigList[0].csordtelcoa__Replaced_Subscription__c = subs.Id;
        update prodConfigList[0];
        
        DF_Opportunity_Bundle__c opptyBundle2 = DF_TestData.createOpportunityBundle(acc.ID);
        insert opptyBundle2;
        
        Opportunity oppty2 = DF_TestData.createOpportunity('Test Opp2');
        oppty2.Opportunity_Bundle__c = opptyBundle2.id;
        insert oppty2;
                    
        //csord__Subscription__c subs2 = DF_TestService.createCSSubscriptionRecord(acc.Id, oppty2);
        
        cscfga__Product_Basket__c basketObj2 = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        List<cscfga__Product_Configuration__c> prodConfigList2 = DF_CS_API_Util.getProductBasketConfigs(basketObj2.Id);
        //prodConfigList2[0].csordtelcoa__Replaced_Subscription__c = subs2.Id;
        prodConfigList2[0].csordtelcoa__Replaced_Product_Configuration__c = prodConfigList[0].Id;
        update prodConfigList2[0];
        
        csord__Service__c service = new csord__Service__c();
        service.Name = 'OVC 1';
        service.csordtelcoa__Product_Basket__c = basketObj.Id;
        service.csord__Subscription__c = subs.Id;
        //service.csord__Order__c = csOrd.Id;
        service.csord__Identification__c = 'xxx';
        service.csordtelcoa__Product_Configuration__c = prodConfigList[0].Id;
        insert service;
        
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        
        // Create DFQuote recs
        DF_Quote__c dfQuote1 = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');         
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;       
          
        DF_Order__c testOrder = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle.Id );
        testOrder.Heritage_Site__c = 'Yes';
        testOrder.Induction_Required__c = 'Yes';
        testOrder.Security_Required__c = 'Yes';
        testOrder.Site_Notes__c = 'Site Notes';
        testOrder.Trading_Name__c = 'Trader Joes TEST SITE';
        testOrder.Order_Version__c = 1;
        testOrder.BPI_Id__c = 'BPI000000000000';
        testOrder.OVC_NonBillable__c = '[{"OVCId":"'+prodConfigList[0].Id+'","CSA":"CSA180000002222","NNIGroupId":"NNI090000015859","routeType":"Local","coSHighBandwidth":"50","coSMediumBandwidth":"150","coSLowBandwidth":"60","routeRecurring":"0.00","coSRecurring":"0.00","ovcMaxFrameSize":"Jumbo (9000 Bytes)","status":"Incomplete","sTag":"0","ceVlanId":"3421","mappingMode":"DSCP"}]';
        Insert testOrder;
        
        DF_Order__c testOrder1 = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle.Id );
        testOrder1.Heritage_Site__c = 'Yes';
        testOrder1.Induction_Required__c = 'Yes';
        testOrder1.Security_Required__c = 'Yes';
        testOrder1.Site_Notes__c = 'Site Notes';
        testOrder1.Trading_Name__c = 'Trader Joes TEST SITE';
        testOrder1.Previous_Order__c = testOrder.Id;
        testOrder1.OrderType__c = 'Modify';
        testOrder1.Order_SubType__c = 'Add new OVC';
        string ovcNonBill = '[{"OVCId":"'+prodConfigList2[0].Id+'","CSA":"CSA180000002222","NNIGroupId":"NNI090000015859","routeType":"Local","coSHighBandwidth":"50","coSMediumBandwidth":"100","coSLowBandwidth":"60","routeRecurring":"0.00","coSRecurring":"0.00","ovcMaxFrameSize":"Jumbo (9000 Bytes)","status":"Incomplete","sTag":"0","ceVlanId":"3421","mappingMode":"DSCP"}]';
        testOrder1.OVC_NonBillable__c = ovcNonBill;
        
        Insert testOrder1;

        
        string location = '{"status":"Order Draft","SCR":"1297.0000","SCNR":"5000.0000","quoteId":"EEQ-0000000324","orderId":"a7EN000000092gQMAQ","locId":"LOC000006231845","id":"a6aN00000000k5iIAA","FBC":0,"address":"6 LENNOX ST RAVENSWOOD TAS 7250 Australia","OrderId":"a7EN000000092gQMAQ","basketId":"a1eN0000000z2amIAA","UNI":[{"BTDType":"Standard (CSAS)","interfaceBandwidth":"1GB","AAT":"","SLA":"Premium - 12 (24/7)","term":"12","zone":"Zone 3","AHA":"No","coSRecurring":"0","oVCType":"Access EVPL","interfaceTypes":"Copper (Auto-negotiate)","tPID":"0x8100","Id":"a1hN0000001TOJTIA4"}],"OVCs":[{"OVCId":"a1hN0000001TOJUIA4","OVCName":"OVC 1","CSA":"CSA180000002222","monthlyCombinedCharges":"497.0000","routeType":"Local","coSHighBandwidth":"20","coSMediumBandwidth":"10","coSLowBandwidth":"0","routeRecurring":"0.0000","coSRecurring":"497.0000","POI":"1-MEL-MELBOURNE-MP","status":"Incomplete","mappingMode":"DSCP","NNIGroupId":"NNI090000015859","sTag":"0","ceVlanId":"12","ovcMaxFrameSize":"Jumbo (9000 Bytes)"}],"prodChargeConfigId":"a1hN0000001TOJTIA4","ovcConfigId":"a1hN0000001TOJUIA4","businessContactList":[{"busContSurname":"asf","busContSuburb":"sdf","busContStreetAddress":"asf","busContState":"VIC","busContRole":"asdf","busContPostcode":"2000","busContNumber":"0412123123","busContId":"003N000001DhSVkIAN","busContFirstName":"asd","busContEmail":"asdf@asd.com","busContIndex":1}],"siteContactList":[{"siteContSurname":"asdf","siteContNumber":"0412123123","siteContId":"003N000001DhSWsIAN","siteContFirstName":"asd","siteContEmail":"asdf@asdf.com","siteContIndex":1}],"SiteDetails":{"Trading Name":[""],"Heritage Site":["Yes"],"Induction Required":["Yes"],"Security Required":["Yes"],"Site Notes":[""]}}';

        Test.startTest();
        DF_Order__c newOrder = EE_OrderUtility.cloneDfOrder(dfQuote1,testOrder, 'Add new OVC');
        System.assertNotEquals(null, newOrder);
        
        
        //Test to find dfOrder object
        DF_Order__c orderObj=EE_OrderUtility.getDfOrder('BPI000000000000','Add new OVC');
        System.assertNotEquals(null, orderObj);
         Test.stopTest();
        
        
        
    }
    /**
    * Tests getSubscriptionRecord method
    */
    @isTest static void getSubscriptionRecordMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        Opportunity oppo = DF_TestService.getOppRecord(acc.Id);
        csord__Subscription__c sub = DF_TestService.createCSSubscriptionRecord(acc.Id, oppo);
        Test.startTest();
        csord__Subscription__c subResult = EE_OrderUtility.getSubscriptionRecord('BPI000000012911');
        System.assertNotEquals(null, subResult);
        Test.stopTest();
    }
    
    /**
    * Tests getSubscriptionRecord method error condition
    */
    @isTest static void getSubscriptionRecordMethodErrorTest()
    {
        Test.startTest();
        csord__Subscription__c subResult = EE_OrderUtility.getSubscriptionRecord('');
        System.assertEquals(null, subResult);
        Test.stopTest();
    }
    
    /**
    * Tests getOpportunityRecord method
    */
    @isTest static void getOpportunityRecordMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        Opportunity oppo = DF_TestService.getOppRecord(acc.Id);
        csord__Subscription__c sub = DF_TestService.createCSSubscriptionRecord(acc.Id, oppo);
        Test.startTest();
        Opportunity opp = EE_OrderUtility.getOpportunityRecord(sub);
        System.assertNotEquals(null, opp);
        Test.stopTest();
    }
    
    /**
    * Tests getOpportunityRecord method error condition
    */
    @isTest static void getOpportunityRecordMethodErrorTest()
    {
        Test.startTest();
        Opportunity opp = EE_OrderUtility.getOpportunityRecord(null);
        System.assertEquals(null, opp);
        Test.stopTest();
    }
    
    /**
    * Tests getOpportunityRecord method error condition
    */
    @isTest static void getOpportunityRecordMethodOrderErrorTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        Opportunity oppo = DF_TestService.getOppRecord(acc.Id);
        csord__Subscription__c sub = DF_TestService.createCSSubscriptionRecord(acc.Id, oppo);
        sub.csord__Order__c = null;
        update sub;
        Test.startTest();
        Opportunity opp = EE_OrderUtility.getOpportunityRecord(sub);
        System.assertEquals(null, opp);
        Test.stopTest();
    }
    
    /**
    * Tests createMACDOpp method
    */
    @isTest static void createMACDOppMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        Opportunity oppo = DF_TestService.getOppRecord(acc.Id);
        List<Opportunity> oppoList = new List<Opportunity>();
        oppoList.add(oppo);
        csord__Subscription__c sub = DF_TestService.createCSSubscriptionRecord(acc.Id, oppo);
        Test.startTest();
        List<Opportunity> oppList = EE_OrderUtility.createMACDOpp(oppoList);
        System.assertNotEquals(0, oppList.size());
        Test.stopTest();
    }
    
    /**
    * Tests getProductData method
    */
    @isTest static void getProductDataMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfig(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        Test.startTest();
        List<DF_Quote__c> quoteList = [SELECT Id, Name,Location_Id__c, Opportunity__r.Commercial_Deal__c, Opportunity__r.Commercial_Deal__r.Deal_Module_Reference__c, 
                                       Opportunity__r.Commercial_Deal__r.ETP_Applies__c, Address__c, RAG__c, Fibre_Build_Category__c ,Opportunity_Bundle__c, Opportunity__c, LAPI_Response__c FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppId LIMIT 1];
        String result = EE_OrderUtility.getProductData(basket.Id,quoteList.get(0));
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
    * Tests getProductData method error condition
    */
    @isTest static void getProductDataMethodErrorTest()
    {
        Test.startTest();
        String result = EE_OrderUtility.getProductData('',null);
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
    /**
    * Tests updateHiddenOVCCharges method
    */
    @isTest static void updateHiddenOVCChargesMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        List<cscfga__Product_Configuration__c> pcConfigList = [Select id,cscfga__Configuration_Offer__r.cscfga__Template__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketObj.Id and name = 'Direct Fibre - Product Charges'];
        cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
        String configId = configObj != null ? configObj.Id : null;
        Test.startTest();
        String result = EE_OrderUtility.updateHiddenOVCCharges(basketObj.Id, configId, 353.40);
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
    * Tests updateHiddenOVCCharges method error condition
    */
    @isTest static void updateHiddenOVCChargesMethodErrorTest()
    {
        Test.startTest();
        String result = EE_OrderUtility.updateHiddenOVCCharges('', '', 0.00);
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
     /**
    * Tests createMACDBasket method
    */
    @isTest static void createMACDBasketMethodTest()
    {
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        Opportunity oppo = DF_TestService.getOppRecord(acc.Id);
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        basketObj.cscfga__Opportunity__c = oppo.Id;
        update basketObj;
        List<DF_Custom_Options__c> csList = new List<DF_Custom_Options__c>();
        DF_Custom_Options__c cs1 = new DF_Custom_Options__c(name = 'SF_OPPORTUNITY_STAGE', Value__c = 'Closed Won');
        DF_Custom_Options__c cs2 = new DF_Custom_Options__c(name = 'SF_STAGENAME', Value__c = 'New');
        csList.add(cs1);
        csList.add(cs2);
        insert csList;
        //String syncResults = DF_CS_API_Util.syncWithOpportunity(basketObj.Id);
        //DF_DesktopAssessmentController.updateOppClosedWon(oppo,basketObj);
        //csord__Order__c ord = [select Id, csordtelcoa__Opportunity__c from csord__Order__c where csordtelcoa__Opportunity__c = :oppo.Id];
        //csord__Subscription__c sub = [select Id, Name, BPI_Id__c, csordtelcoa__Change_Type__c, csord__Account__c, csord__Order__c, csord__Status__c, csordtelcoa__Replacement_Subscription__c from csord__Subscription__c where csord__Order__c = :ord.Id];
        csord__Subscription__c sub = DF_TestService.createCSSubscriptionRecord(acc.Id, oppo);
        
        /*cscfga__Product_Category__c  pc = DF_TestData.buildProductCategory();
        insert pc;
        
        cscfga__Configuration_Offer__c  offer = DF_TestData.createOffer('Direct Fibre - Product Charges');
        insert offer;
        
        List<DF_Custom_Options__c> csList = new List<DF_Custom_Options__c>();
        DF_Custom_Options__c cs1 = new DF_Custom_Options__c(name = 'SF_CATEGORY_ID', Value__c = pc.Id);
        DF_Custom_Options__c cs2 = new DF_Custom_Options__c(name = 'SF_PRODUCT_CHARGE_OFFER_ID', Value__c = offer.Id);
        csList.add(cs1);
        csList.add(cs2);
        insert csList;*/
        
        Test.startTest();
        String result = EE_OrderUtility.createMACDBasket('BPI000000012911','Add new OVC');
        Test.stopTest();
    }
    
        /**
    * Tests createMACDBasket method error condition
    */
    @isTest static void createMACDBasketMethodErrorTest()
    {
        Test.startTest();
        String result = EE_OrderUtility.createMACDBasket('','');
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
    /**
    * Tests updateProduct method
    */
    @isTest static void updateProductMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Map<Id, cscfga__Product_Configuration__c> configs1 = new Map<Id, cscfga__Product_Configuration__c>([Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketObj.Id]);
        List<Id> configIdList = new List<Id>(configs1.keySet());
        Map<String,Object> configResponse = cscfga.API_1.getProductConfigurations(new List<Id>(configs1.keySet()));
        Map<String, cscfga__Product_Configuration__c> cfgsOVC = new Map<String, cscfga__Product_Configuration__c>();
        Map<String, Object> attsOVC = new Map<String, Object>();
        for(String o : configResponse.keySet()){
            if(o.contains('-attributes'))
            {
                attsOVC.put(o, configResponse.get(o));
            }
            else
                cfgsOVC.put(o, (cscfga__Product_Configuration__c)configResponse.get(o));
        }

        String jsonStr = json.serialize(attsOVC);
        String jsonStr1 = json.serialize(cfgsOVC);
        
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        Test.startTest();
        String result = EE_OrderUtility.updateProduct(basketObj.Id, jsonStr1, jsonStr, configIdList.get(0));
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
    * Tests updateProduct method error condition
    */
    @isTest static void updateProductMethodErrorTest()
    {
        Test.startTest();
        String result = EE_OrderUtility.updateProduct('', '', '','');
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
    /**
    * Tests removeOVC method
    */
    @isTest static void removeOVCMethodTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        oppId = DF_TestService.getQuoteRecords();
        Account acc = DF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = DF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Opportunity opp = DF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        
        List<cscfga__Product_Configuration__c> pcConfigList = [Select id,cscfga__Configuration_Offer__r.cscfga__Template__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basket.Id and name = 'OVC'];
        cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
        String configId = configObj != null ? configObj.Id : null;
        Test.startTest();
        String result = EE_OrderUtility.removeOVC(basket.Id, configId, 'OVC');
        System.assertNotEquals('Error', result);
        Test.stopTest();
    }
    
    /**
    * Tests removeOVC method error condition
    */
    @isTest static void removeOVCMethodErrorTest()
    {
        Test.startTest();
        String result = EE_OrderUtility.removeOVC(null, null, 'OVC');
        System.assertEquals('Error', result);
        Test.stopTest();
    }
}