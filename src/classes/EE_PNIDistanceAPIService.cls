public class EE_PNIDistanceAPIService {

	/**
	 * Gets the distance asynchronous.
	 *
	 * @param      sfReqId  The sf request identifier
	 */
	@future(callout = true)
	public static void getDistanceAsync(String sfrIds) {
		getDistance(sfrIds);
	}

	/**
	 * Gets the distance synchronize.
	 *
	 * @param      sfReqId  The sf request identifier
	 */
	public static void getDistanceSync(DF_ServiceFeasibilityResponse sfResp) {
		getDistance(sfResp);
	}


	/**
	 * Gets the distance.
	 *
	 * @param      sfReqId  The sf request identifier Set
	 */
	public static void getDistance(String sfrIds) {

		String oppBundleName;
		List<DF_SF_request__c> lstSfr = new List<DF_SF_Request__c>(), lstSfrToUpdate = new List<DF_SF_Request__c>();
		DF_Integration_Setting__mdt integrSetting = EE_CISLocationAPIUtils.getIntegrationSettings(EE_CISLocationAPIUtils.INTEGR_SETTING_NAME_PNI_DISTANCE_API);
		final Integer TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
		final String NBN_ENV_OVRD_VAL = integrSetting.DP_Environment_Override__c;
		String strQueryOptions = '';


		if(String.isNotBlank(sfrIds)) {
		List<String> sfrIDArray = sfrIds.split(EE_CISLocationAPIUtils.STR_COMMA);
		Set<String> setSfrId = new Set<String>();
		setSfrId.addAll(sfrIDArray);
		DF_AS_Custom_Settings__c settings  = DF_AS_Custom_Settings__c.getValues('DF_Order_PNI_API_Query_Options');

		if (settings != null) {
			strQueryOptions = settings.Value__c;
		}

		DF_ServiceFeasibilityResponse sfrJSRec;

		NS_PNI_QueryService pniCalloutObj = new NS_PNI_QueryService ();
		NS_PNI_QueryService.queryServicePort queryServiceObj = new NS_PNI_QueryService.queryServicePort();
		queryServiceObj.timeout_x = TIMEOUT_LIMIT;
		NS_PNI_QueryService.queryInputList_element lstPniInput = new NS_PNI_QueryService.queryInputList_element();
		NS_PNI_QueryService.queryInput_element pniInputRec;
		/*QueryOptions Begin*/

		NS_PNI_QueryService.queryOptions_element pniQueryOptionsRec = new NS_PNI_QueryService.queryOptions_element();
		if (String.isNotBlank(strQueryOptions)) {
			pniQueryOptionsRec.describedby = new List<NS_PNI_QueryService.describedby_element>();
			NS_PNI_QueryService.describedby_element descOptions;// = new NS_PNI_QueryService.describedby_element();
			if (strQueryOptions.containsIgnoreCase(';')) {
				for (String strJointDetails : strQueryOptions.split(';')) {
					descOptions = new NS_PNI_QueryService.describedby_element();
					descOptions.characteristic = new NS_PNI_QueryService.characteristic_element();
					descOptions.value = strJointDetails.split(',')[1];
					descOptions.characteristic.ID = strJointDetails.split(',')[0];
					pniQueryOptionsRec.describedby.add(descOptions);
				}
			} else {
				descOptions.value = strQueryOptions.split(',')[0];
				descOptions.characteristic.ID = strQueryOptions.split(',')[1];
				pniQueryOptionsRec.describedby.add(descOptions);
			}
		}
		/*QueryOptions End*/
		NS_PNI_QueryService.queryResponse_element pniResponse = new NS_PNI_QueryService.queryResponse_element();
		lstPniInput.queryInput = new List<NS_PNI_QueryService.queryInput_element>();
		if (String.isNotBlank(NBN_ENV_OVRD_VAL)) {
			queryServiceObj.inputHttpHeaders_x = new Map<String, String>();
			queryServiceObj.inputHttpHeaders_x.put('NBN-Environment-Override', NBN_ENV_OVRD_VAL);
		}

		try {
			//lstSfr= Database.query(SF_CS_API_Util.getQuery(new DF_SF_Request__c(), ' WHERE Id IN :setSfrId'));
			//we cannot use the above method as we need Opportunity_Bundle_Name__c
			lstSfr = [SELECT Id,
			          Name,
			          Location_Id__c,
			          Opportunity_Bundle__r.Opportunity_Bundle_Name__c,
			          Response__c
			          FROM DF_SF_Request__c
			          WHERE Id IN :setSfrId];
			//loop through SFR records, create reuest for PNI
			for (DF_SF_Request__c sfrRec : lstSfr) {
				oppBundleName = sfrRec.Opportunity_Bundle__r.Opportunity_Bundle_Name__c;
				if (String.isNotBlank(sfrRec.Location_Id__c)) {
					pniInputRec = new NS_PNI_QueryService.queryInput_element();
					pniInputRec.type_x = 'Location';
					//pniInput.subtype=;
					pniInputRec.id = sfrRec.Location_Id__c;
					//pniInput.systemId=;
				}
				lstPniInput.queryInput.add(pniInputRec);
			}
			//callout PNI
			pniResponse = queryServiceObj.getNearestAsset(oppBundleName, pniQueryOptionsRec, lstPniInput);
			//upadte PNI response
			lstSfrToUpdate.addAll(updatePniResponse(pniResponse, lstSfr));
			Database.update(lstSfrToUpdate);
		} catch (Exception ex) {
			System.debug('NS_PNI_SendLocationCalloutQueueHandler::makePNICallout EXCEPTION: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
			// Errors
			final String ERROR_MESSAGE = 'A HTTP error occurred in class method EE_PNIDistanceAPIService.getDistance';
			AsyncQueueableUtils.retry(EE_PNIDistanceHandler.HANDLER_NAME, new List<String>{sfrIds}, ERROR_MESSAGE);

			for (DF_SF_Request__c sfrRec : lstSfr) {
				sfrJSRec = (DF_ServiceFeasibilityResponse)System.JSON.deserialize(sfrRec.Response__c, DF_ServiceFeasibilityResponse.class);
				sfrJSRec.pniException =  ex.getTypeName();
				sfrJSRec.Status = EE_CISLocationAPIUtils.STATUS_ERROR;
				sfrRec.Response__c = JSON.serialize(sfrJSRec);
				sfrRec.Status__c = EE_CISLocationAPIUtils.STATUS_ERROR;
			}
			Database.update(lstSfr);
		}
		}

	}

	/**
	 * Gets the distance.
	 *
	 * @param      sfReqId  The sf request identifier Set
	 */
	public static void getDistance(DF_ServiceFeasibilityResponse sfResp) {


		DF_Integration_Setting__mdt integrSetting = EE_CISLocationAPIUtils.getIntegrationSettings(EE_CISLocationAPIUtils.INTEGR_SETTING_NAME_PNI_DISTANCE_API);
		final Integer TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
		final String NBN_ENV_OVRD_VAL = integrSetting.DP_Environment_Override__c;
		String strQueryOptions = '';

		DF_AS_Custom_Settings__c settings  = DF_AS_Custom_Settings__c.getValues('DF_Order_PNI_API_Query_Options');

		if (settings != null) {
			strQueryOptions = settings.Value__c;
		}


		NS_PNI_QueryService pniCalloutObj = new NS_PNI_QueryService ();
		NS_PNI_QueryService.queryServicePort queryServiceObj = new NS_PNI_QueryService.queryServicePort();
		queryServiceObj.timeout_x = TIMEOUT_LIMIT;
		NS_PNI_QueryService.queryInputList_element lstPniInput = new NS_PNI_QueryService.queryInputList_element();
		NS_PNI_QueryService.queryInput_element pniInputRec;
		/*QueryOptions Begin*/

		NS_PNI_QueryService.queryOptions_element pniQueryOptionsRec = new NS_PNI_QueryService.queryOptions_element();
		if (String.isNotBlank(strQueryOptions)) {
			pniQueryOptionsRec.describedby = new List<NS_PNI_QueryService.describedby_element>();
			NS_PNI_QueryService.describedby_element descOptions;// = new NS_PNI_QueryService.describedby_element();
			if (strQueryOptions.containsIgnoreCase(';')) {
				for (String strJointDetails : strQueryOptions.split(';')) {
					descOptions = new NS_PNI_QueryService.describedby_element();
					descOptions.characteristic = new NS_PNI_QueryService.characteristic_element();
					descOptions.value = strJointDetails.split(',')[1];
					descOptions.characteristic.ID = strJointDetails.split(',')[0];
					pniQueryOptionsRec.describedby.add(descOptions);
				}
			} else {
				descOptions.value = strQueryOptions.split(',')[0];
				descOptions.characteristic.ID = strQueryOptions.split(',')[1];
				pniQueryOptionsRec.describedby.add(descOptions);
			}
		}
		/*QueryOptions End*/
		NS_PNI_QueryService.queryResponse_element pniResponse = new NS_PNI_QueryService.queryResponse_element();
		lstPniInput.queryInput = new List<NS_PNI_QueryService.queryInput_element>();
		if (String.isNotBlank(NBN_ENV_OVRD_VAL)) {
			queryServiceObj.inputHttpHeaders_x = new Map<String, String>();
			queryServiceObj.inputHttpHeaders_x.put('NBN-Environment-Override', NBN_ENV_OVRD_VAL);
		}

		try {
			if (String.isNotBlank(sfResp.LocId)) {
				pniInputRec = new NS_PNI_QueryService.queryInput_element();
				pniInputRec.type_x = 'Location';
				//pniInput.subtype=;
				pniInputRec.id = sfResp.LocId ;
				//pniInput.systemId=;
			}
			lstPniInput.queryInput.add(pniInputRec);

			//callout PNI
			pniResponse = queryServiceObj.getNearestAsset(sfResp.LocId, pniQueryOptionsRec, lstPniInput);
			//upadte PNI response
			updatePniResponse(pniResponse, sfResp);

		} catch (Exception ex) {
			System.debug('NS_PNI_SendLocationCalloutQueueHandler::makePNICallout EXCEPTION: ' + ex.getMessage() + '\n' + ex.getStackTraceString());

			sfResp.pniException =  ex.getTypeName();
			sfResp.Status = EE_CISLocationAPIUtils.STATUS_ERROR;
		}

	}

	/**
	 * Update SF Request using PNI Response
	 *
	 * @param      pniResponse  The pni response
	 * @param      lstSFR       The list sfr
	 *
	 * @return     { description_of_the_return_value }
	 */
	public static List<DF_SF_Request__c> updatePniResponse(NS_PNI_QueryService.queryResponse_element pniResponse, List<DF_SF_Request__c> lstSFR) {
		List<DF_SF_Request__c> lstSfrUpdated = new List<DF_SF_Request__c>();

		try {

			DF_ServiceFeasibilityResponse sfrJSRec;
			Map<String, NS_PNI_QueryService.queryResult_element> mapQueryResponse;
			NS_PNI_QueryService.queryResult_element resElement;

			String noFibreJointErrorCode = '';


			DF_AS_Custom_Settings__c settings  = DF_AS_Custom_Settings__c.getValues('DF_Order_PNI_No_Fibre_Joint_Error_Code');

			if (settings != null) {
				noFibreJointErrorCode = settings.Value__c;
			}


			//check if there are any callout errors
			if (pniResponse.exception_x == null) {
				//if no errors, create a map of locationId & response record
				mapQueryResponse = new Map<String, NS_PNI_QueryService.queryResult_element>();
				for (NS_PNI_QueryService.queryResult_element responseElement : pniResponse.queryResultList.queryResult) {
					System.debug('adding ' + responseElement.Id + ' to pni mapQueryResponse');
					mapQueryResponse.put(responseElement.Id, responseElement);
				}
				//iterate through SFR records and update the PNI response on the sfr record
				for (DF_SF_Request__c sfrRec : lstSFR) {
					sfrJSRec = (DF_ServiceFeasibilityResponse)System.JSON.deserialize(sfrRec.Response__c, DF_ServiceFeasibilityResponse.class);
					System.debug('CIS loc API response locId is ' + sfrJSRec.LocId);
					resElement = mapQueryResponse.get(sfrJSRec.LocId);
					if(resElement!=null){
					if (resElement.exception_x == null) {
						sfrJSRec.systemId = resElement.systemId;
						sfrJSRec.id = resElement.Id;
						for (NS_PNI_QueryService.describedby_element descElement : resElement.describedby) {
							if (descElement.characteristic.ID.equalsIgnoreCase('AssetStatus')) {
								sfrJSRec.fibreJointStatus = descElement.value;
							} else if (descElement.characteristic.ID.equalsIgnoreCase('AssetId')) {
								sfrJSRec.fibreJointId = descElement.value;
							} else if (descElement.characteristic.ID.equalsIgnoreCase('AssetOwner')) {
								sfrJSRec.assetOwner = descElement.value;
							} else if (descElement.characteristic.ID.equalsIgnoreCase('AssetDistance')) {
								sfrJSRec.Distance = Decimal.valueOf(descElement.value).round(System.RoundingMode.CEILING).intValue();
							} else if (descElement.characteristic.ID.equalsIgnoreCase('AssetLat')) {
								sfrJSRec.assetLat = descElement.value;
								sfrJSRec.Latitude = descElement.value;
							} else if (descElement.characteristic.ID.equalsIgnoreCase('AssetLong')) {
								sfrJSRec.assetLong = descElement.value;
								sfrJSRec.Longitude = descElement.value;
							} else if (descElement.characteristic.ID.equalsIgnoreCase('OLT_Exists')) {
								sfrJSRec.OLT_Exists = descElement.value;
							} else if (descElement.characteristic.ID.equalsIgnoreCase('AssetTypeCode')) {
								sfrJSRec.fibreJointTypeCode = descElement.value;
							} else if (descElement.characteristic.ID.equalsIgnoreCase('OLT_ID')) {
								sfrJSRec.OLT_ID = descElement.value;
							}
						}
						if (String.isNotBlank(sfrJSRec.assetLong) && String.isNotBlank(sfrJSRec.assetLat)) {
							sfrJSRec.fibreJointLatLong = sfrJSRec.assetLat + ', ' + sfrJSRec.assetLong;
						}
						sfrJSRec.pniException = null;
						sfrJSRec.Status = EE_CISLocationAPIUtils.STATUS_VALID;
						sfrRec.Response__c = JSON.serialize(sfrJSRec);
						sfrRec.Status__c = EE_CISLocationAPIUtils.STATUS_COMPLETED;
						System.debug('after successful PNI response for request with sfReq locId ' + sfrRec.Location_Id__c + ' , latlong ' + sfrRec.Latitude__c + ',' + sfrRec.Longitude__c + ' , status ' + sfrRec.Status__c + ' and sfResponse locId ' + sfrJSRec.LocId + ' , latlong ' + sfrJSRec.locLatLong + ' , status ' + sfrJSRec.status);


					} else {

						if (resElement.exception_x.exceptionCode != null && noFibreJointErrorCode.equals(resElement.exception_x.exceptionCode)) {
							sfrJSRec.Status = EE_CISLocationAPIUtils.STATUS_VALID;
							sfrJSRec.isNoFibreJointFound = true;
							sfrRec.Response__c = JSON.serialize(sfrJSRec);
							sfrRec.Status__c = EE_CISLocationAPIUtils.STATUS_COMPLETED;
						} else {

							sfrJSRec.id = resElement.Id;
							sfrJSRec.pniException = 'Type= ' + resElement.exception_x.exceptionType +
							                        ', Code= ' + resElement.exception_x.exceptionCode +
							                        ', Description= ' + resElement.exception_x.exceptionDescription;
							sfrJSRec.Status = EE_CISLocationAPIUtils.STATUS_ERROR;
							sfrRec.Response__c = JSON.serialize(sfrJSRec);
							sfrRec.Status__c = EE_CISLocationAPIUtils.STATUS_ERROR;
						}

					}
					} else {
							sfrJSRec.pniException = sfrJSRec.LocId+' Not Returned in PNI Response';
							sfrJSRec.Status = EE_CISLocationAPIUtils.STATUS_ERROR;
							sfrRec.Response__c = JSON.serialize(sfrJSRec);
							sfrRec.Status__c = EE_CISLocationAPIUtils.STATUS_ERROR;
					}

					lstSfrUpdated.add(sfrRec);
				}
			} else {
				//if there were any callout errors update the same on SFR records.
				for (DF_SF_Request__c sfrRec : lstSFR) {

					sfrJSRec = (DF_ServiceFeasibilityResponse)System.JSON.deserialize(sfrRec.Response__c, DF_ServiceFeasibilityResponse.class);


					sfrJSRec.pniException = 'Type= ' + pniResponse.exception_x.exceptionType +
					                        ', Code= ' + pniResponse.exception_x.exceptionCode +
					                        ', Description= ' + pniResponse.exception_x.exceptionDescription;
					sfrJSRec.Status = EE_CISLocationAPIUtils.STATUS_ERROR;
					sfrRec.Response__c = JSON.serialize(sfrJSRec);
					sfrRec.Status__c = EE_CISLocationAPIUtils.STATUS_ERROR;
					lstSfrUpdated.add(sfrRec);

				}
			}
		} catch (Exception ex) {
			System.debug('NS_PNI_Util::updatePniResponse EXCEPTION: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
			throw new CustomException(ex.getMessage());
		}

		return lstSfrUpdated;
	}


	/**
	 * Update SF Request using PNI Response
	 *
	 * @param      pniResponse  The pni response
	 * @param      sfResp       The sf resp
	 */
	public static void updatePniResponse(NS_PNI_QueryService.queryResponse_element pniResponse, DF_ServiceFeasibilityResponse sfrJSRec) {

		try {

			Map<String, NS_PNI_QueryService.queryResult_element> mapQueryResponse;
			NS_PNI_QueryService.queryResult_element resElement;

			String noFibreJointErrorCode = '';


			DF_AS_Custom_Settings__c settings  = DF_AS_Custom_Settings__c.getValues('DF_Order_PNI_No_Fibre_Joint_Error_Code');

			if (settings != null) {
				noFibreJointErrorCode = settings.Value__c;
			}


			//check if there are any callout errors
			if (pniResponse.exception_x == null) {
				//if no errors, create a map of locationId & response record
				mapQueryResponse = new Map<String, NS_PNI_QueryService.queryResult_element>();
				for (NS_PNI_QueryService.queryResult_element responseElement : pniResponse.queryResultList.queryResult) {
					mapQueryResponse.put(responseElement.Id, responseElement);
				}
				resElement = mapQueryResponse.get(sfrJSRec.LocId);
				if (resElement.exception_x == null) {
					sfrJSRec.systemId = resElement.systemId;
					sfrJSRec.id = resElement.Id;
					for (NS_PNI_QueryService.describedby_element descElement : resElement.describedby) {
						if (descElement.characteristic.ID.equalsIgnoreCase('AssetStatus')) {
							sfrJSRec.fibreJointStatus = descElement.value;
						} else if (descElement.characteristic.ID.equalsIgnoreCase('AssetId')) {
							sfrJSRec.fibreJointId = descElement.value;
						} else if (descElement.characteristic.ID.equalsIgnoreCase('AssetOwner')) {
							sfrJSRec.assetOwner = descElement.value;
						} else if (descElement.characteristic.ID.equalsIgnoreCase('AssetDistance')) {
							sfrJSRec.Distance = Decimal.valueOf(descElement.value).round(System.RoundingMode.CEILING).intValue();
						} else if (descElement.characteristic.ID.equalsIgnoreCase('AssetLat')) {
							sfrJSRec.assetLat = descElement.value;
							sfrJSRec.Latitude = descElement.value;
							System.debug('Updated DF_ServiceFeasibilityResponse.Latitude with PNI response '+ descElement.value);
						} else if (descElement.characteristic.ID.equalsIgnoreCase('AssetLong')) {
							sfrJSRec.assetLong = descElement.value;
							sfrJSRec.Longitude = descElement.value;
							System.debug('Updated DF_ServiceFeasibilityResponse.Longitude with PNI response '+ descElement.value);
						} else if (descElement.characteristic.ID.equalsIgnoreCase('OLT_Exists')) {
							sfrJSRec.OLT_Exists = descElement.value;
						} else if (descElement.characteristic.ID.equalsIgnoreCase('AssetTypeCode')) {
							sfrJSRec.fibreJointTypeCode = descElement.value;
						} else if (descElement.characteristic.ID.equalsIgnoreCase('OLT_ID')) {
							sfrJSRec.OLT_ID = descElement.value;
						}
					}
					if (String.isNotBlank(sfrJSRec.assetLong) && String.isNotBlank(sfrJSRec.assetLat)) {
						sfrJSRec.fibreJointLatLong = sfrJSRec.assetLat + ', ' + sfrJSRec.assetLong;
					}
					sfrJSRec.pniException = null;
					sfrJSRec.Status = EE_CISLocationAPIUtils.STATUS_VALID;

				} else {

					if (resElement.exception_x.exceptionCode != null && noFibreJointErrorCode.equals(resElement.exception_x.exceptionCode)) {
						sfrJSRec.Status = EE_CISLocationAPIUtils.STATUS_VALID;
						sfrJSRec.isNoFibreJointFound = true;
					} else {

						sfrJSRec.id = resElement.Id;
						sfrJSRec.pniException = 'Type= ' + resElement.exception_x.exceptionType +
						                        ', Code= ' + resElement.exception_x.exceptionCode +
						                        ', Description= ' + resElement.exception_x.exceptionDescription;
						sfrJSRec.Status = EE_CISLocationAPIUtils.STATUS_ERROR;
					}

				}


			} else {

				sfrJSRec.pniException = 'Type= ' + pniResponse.exception_x.exceptionType +
				                        ', Code= ' + pniResponse.exception_x.exceptionCode +
				                        ', Description= ' + pniResponse.exception_x.exceptionDescription;
				sfrJSRec.Status = EE_CISLocationAPIUtils.STATUS_ERROR;

			}
		} catch (Exception ex) {
			System.debug('NS_PNI_Util::updatePniResponse EXCEPTION: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
			throw new CustomException(ex.getMessage());
		}

	}

}