/*------------------------------------------------------------------------
Description:   	A test class created to test EE_PNIDistanceAPIService methods


--------------------------------------------------------------------------*/
@IsTest
public class EE_PNIDistanceAPIServiceTest {
	
	public static final String LOC_ID_0 = '';
	public static final String LOC_ID_1 = 'LOC000035375038';
	public static final String LATITUDE_0 = '';
	public static final String LONGITUDE_0 = '';
	public static final String LATITUDE_1 = '44.968046';
	public static final String LONGITUDE_1 = '-94.420307';
	public static final String STATE_1 = 'VIC';
	public static final String POSTCODE_1 = '3000';
	public static final String SUBURB_LOCALITY_1 = 'Melbourne';
	public static final String UNIT_NUMBER_1 = '1';
	public static final String UNIT_TYPE_1 = 'UNIT';
	public static final String STREET_NAME_1 = 'Collins';
	public static final String STREET_TYPE_1 = 'ST';
	public static final String STREET_LOT_NUMBER_1 = '123';
	
	@testSetup static void testDataSetup() {
		DF_AS_TestService.createCustomSettingsForAS();
		DF_AS_TestService.createNBNIntegrationInputsCustomSettings();
		Account acct = DF_TestData.createAccount('Test Account');
		acct.Access_Seeker_Id__c = 'abctest';
		insert acct;

		String response;
		
		Map<String, String> addressMap1 = new Map<String, String>();
		addressMap1.put('state', STATE_1);
		addressMap1.put('postcode', POSTCODE_1);
		addressMap1.put('suburbLocality', SUBURB_LOCALITY_1);
		addressMap1.put('streetName', STREET_NAME_1);
		addressMap1.put('streetLotNumber', STREET_LOT_NUMBER_1);
		addressMap1.put('unitNumber', UNIT_NUMBER_1);
		
		Map<String, String> addressMap0 = new Map<String, String>();
	}

	@isTest static void test_getDistancePositive() {		
		Map<String, String> addressMap = new Map<String, String>();			
		addressMap.put('unitNumber', UNIT_NUMBER_1);
		addressMap.put('streetLotNumber', STREET_LOT_NUMBER_1);
		addressMap.put('streetName', STREET_NAME_1);
		addressMap.put('suburbLocality', SUBURB_LOCALITY_1);
		addressMap.put('state', STATE_1);
		addressMap.put('postcode', POSTCODE_1);

		User commUser = DF_TestData.createDFCommUser();
		
		EE_WebServiceCalloutMock mockObj = new EE_WebServiceCalloutMock(DF_IntegrationTestData.getPNIPositive());        
        Test.setMock(WebServiceMock.class, mockObj);
		String sfrIds; 

		System.runAs(commUser) {

			String opptyBundleId = DF_SF_CaptureController.createOpptyBundle();
			// Create sfreq
			List<String> addressList = new List<String>();
			addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
			String resp1 = DF_TestData.buildResponseString('LOC000005972574', '42.78931', '147.05578', addressList , 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);

			DF_SF_Request__c sfReq1 = DF_TestData.createSFRequest(EE_CISLocationAPIUtils.SEARCH_TYPE_LOCATION_ID, EE_CISLocationAPIUtils.STATUS_PENDING, LOC_ID_1, null, null, opptyBundleId, resp1, addressMap);
			insert sfReq1;

			String resp2 = DF_TestData.buildResponseString('LOC000005972574', '-15.451568', '145.178216', addressList , 500, 'Wireless', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Wireless', null);

			DF_SF_Request__c sfReq2 = DF_TestData.createSFRequest(EE_CISLocationAPIUtils.SEARCH_TYPE_LAT_LONG, EE_CISLocationAPIUtils.STATUS_PENDING, null, LATITUDE_1, LONGITUDE_1, opptyBundleId, resp2, addressMap);
			insert sfReq2;

			sfrIds = sfReq1.Id + EE_CISLocationAPIUtils.STR_COMMA + sfReq2.Id;
		}			

		Test.startTest();
			EE_PNIDistanceAPIService.getDistance(sfrIds);
			List<DF_SF_Request__c> sfReqList = [SELECT Id, Status__c FROM DF_SF_Request__c LIMIT 2];
		Test.stopTest();

		System.assertEquals(EE_CISLocationAPIUtils.STATUS_COMPLETED, sfReqList.get(0).Status__c); 
	}	


	@isTest static void test_getDistanceNegative() {		
		Map<String, String> addressMap = new Map<String, String>();			
		addressMap.put('unitNumber', UNIT_NUMBER_1);
		addressMap.put('streetLotNumber', STREET_LOT_NUMBER_1);
		addressMap.put('streetName', STREET_NAME_1);
		addressMap.put('suburbLocality', SUBURB_LOCALITY_1);
		addressMap.put('state', STATE_1);
		addressMap.put('postcode', POSTCODE_1);

		User commUser = DF_TestData.createDFCommUser();
		
		EE_WebServiceCalloutMock mockObj = new EE_WebServiceCalloutMock(DF_IntegrationTestData.getPNINegative());        
        Test.setMock(WebServiceMock.class, mockObj);
		String sfrIds; 

		System.runAs(commUser) {

			String opptyBundleId = DF_SF_CaptureController.createOpptyBundle();
			// Create sfreq
			List<String> addressList = new List<String>();
			addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
			String resp1 = DF_TestData.buildResponseString('LOC000005972574', '42.78931', '147.05578', addressList , 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);

			DF_SF_Request__c sfReq1 = DF_TestData.createSFRequest(EE_CISLocationAPIUtils.SEARCH_TYPE_LOCATION_ID, EE_CISLocationAPIUtils.STATUS_PENDING, LOC_ID_1, null, null, opptyBundleId, resp1, addressMap);
			insert sfReq1;

			sfrIds = sfReq1.Id;
		}			

		Test.startTest();
			EE_PNIDistanceAPIService.getDistance(sfrIds);
			List<DF_SF_Request__c> sfReqList = [SELECT Id, Status__c FROM DF_SF_Request__c LIMIT 1];
		Test.stopTest();

		System.assertEquals(EE_CISLocationAPIUtils.STATUS_ERROR, sfReqList.get(0).Status__c); 
	}

	@isTest static void test_getDistanceNegative_PNI_001() {		
		Map<String, String> addressMap = new Map<String, String>();			
		addressMap.put('unitNumber', UNIT_NUMBER_1);
		addressMap.put('streetLotNumber', STREET_LOT_NUMBER_1);
		addressMap.put('streetName', STREET_NAME_1);
		addressMap.put('suburbLocality', SUBURB_LOCALITY_1);
		addressMap.put('state', STATE_1);
		addressMap.put('postcode', POSTCODE_1);

		User commUser = DF_TestData.createDFCommUser();
		
		EE_WebServiceCalloutMock mockObj = new EE_WebServiceCalloutMock(DF_IntegrationTestData.getPNINegative_PNI_001());        
        Test.setMock(WebServiceMock.class, mockObj);
		String sfrIds; 

		System.runAs(commUser) {

			String opptyBundleId = DF_SF_CaptureController.createOpptyBundle();
			// Create sfreq
			List<String> addressList = new List<String>();
			addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
			String resp1 = DF_TestData.buildResponseString('LOC000005972574', '42.78931', '147.05578', addressList , 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);

			DF_SF_Request__c sfReq1 = DF_TestData.createSFRequest(EE_CISLocationAPIUtils.SEARCH_TYPE_LOCATION_ID, EE_CISLocationAPIUtils.STATUS_PENDING, LOC_ID_1, null, null, opptyBundleId, resp1, addressMap);
			insert sfReq1;

			sfrIds = sfReq1.Id;
		}			

		Test.startTest();
			EE_PNIDistanceAPIService.getDistance(sfrIds);
			List<DF_SF_Request__c> sfReqList = [SELECT Id, Status__c FROM DF_SF_Request__c LIMIT 1];
		Test.stopTest();

		System.assertEquals(EE_CISLocationAPIUtils.STATUS_ERROR, sfReqList.get(0).Status__c); 
	}

	@isTest static void test_getDistancePositive_PNI_0012() {		
		Map<String, String> addressMap = new Map<String, String>();			
		addressMap.put('unitNumber', UNIT_NUMBER_1);
		addressMap.put('streetLotNumber', STREET_LOT_NUMBER_1);
		addressMap.put('streetName', STREET_NAME_1);
		addressMap.put('suburbLocality', SUBURB_LOCALITY_1);
		addressMap.put('state', STATE_1);
		addressMap.put('postcode', POSTCODE_1);

		User commUser = DF_TestData.createDFCommUser();
		
		EE_WebServiceCalloutMock mockObj = new EE_WebServiceCalloutMock(DF_IntegrationTestData.getPNINegative_PNI_APP_0012());        
        Test.setMock(WebServiceMock.class, mockObj);
		String sfrIds; 

		System.runAs(commUser) {

			String opptyBundleId = DF_SF_CaptureController.createOpptyBundle();
			// Create sfreq
			List<String> addressList = new List<String>();
			addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
			String resp1 = DF_TestData.buildResponseString('LOC000005972574', '42.78931', '147.05578', addressList , 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);

			DF_SF_Request__c sfReq1 = DF_TestData.createSFRequest(EE_CISLocationAPIUtils.SEARCH_TYPE_LOCATION_ID, EE_CISLocationAPIUtils.STATUS_PENDING, LOC_ID_1, null, null, opptyBundleId, resp1, addressMap);
			insert sfReq1;

			sfrIds = sfReq1.Id;
		}			

		Test.startTest();
			EE_PNIDistanceAPIService.getDistance(sfrIds);
			List<DF_SF_Request__c> sfReqList = [SELECT Id, Status__c FROM DF_SF_Request__c LIMIT 1];
		Test.stopTest();

		System.assertEquals(EE_CISLocationAPIUtils.STATUS_COMPLETED, sfReqList.get(0).Status__c); 
	}

	@isTest static void test_getDistanceSyncPositive() {		
		Map<String, String> addressMap = new Map<String, String>();			
		addressMap.put('unitNumber', UNIT_NUMBER_1);
		addressMap.put('streetLotNumber', STREET_LOT_NUMBER_1);
		addressMap.put('streetName', STREET_NAME_1);
		addressMap.put('suburbLocality', SUBURB_LOCALITY_1);
		addressMap.put('state', STATE_1);
		addressMap.put('postcode', POSTCODE_1);

		User commUser = DF_TestData.createDFCommUser();
							
		EE_WebServiceCalloutMock mockObj = new EE_WebServiceCalloutMock(DF_IntegrationTestData.getPNIPositive());      
        Test.setMock(WebServiceMock.class, mockObj);
		String jsResp; 

		System.runAs(commUser) {

			String opptyBundleId = DF_SF_CaptureController.createOpptyBundle();
			// Create sfreq
			List<String> addressList = new List<String>();
			addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
			String resp1 = DF_TestData.buildResponseString('LOC000005972574', '42.78931', '147.05578', addressList , 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);

			DF_SF_Request__c sfReq1 = DF_TestData.createSFRequest(EE_CISLocationAPIUtils.SEARCH_TYPE_LOCATION_ID, EE_CISLocationAPIUtils.STATUS_PENDING, LOC_ID_1, null, null, opptyBundleId, resp1, addressMap);
			insert sfReq1;

			jsResp = resp1;
		
		}			
		
		DF_ServiceFeasibilityResponse resp = (DF_ServiceFeasibilityResponse)System.JSON.deserialize(jsResp, DF_ServiceFeasibilityResponse.class);
		System.debug('@@@ resp before:' + resp);
		Test.startTest();
			EE_PNIDistanceAPIService.getDistance(resp);
			List<DF_SF_Request__c> sfReqList = [SELECT Id, Status__c FROM DF_SF_Request__c LIMIT 1];
		Test.stopTest();
		System.debug('@@@ resp after:' + resp);
		System.assertEquals(EE_CISLocationAPIUtils.STATUS_VALID, resp.Status); 
	}

	@isTest static void test_getDistanceSyncPositive_PNI_0012() {		
		Map<String, String> addressMap = new Map<String, String>();			
		addressMap.put('unitNumber', UNIT_NUMBER_1);
		addressMap.put('streetLotNumber', STREET_LOT_NUMBER_1);
		addressMap.put('streetName', STREET_NAME_1);
		addressMap.put('suburbLocality', SUBURB_LOCALITY_1);
		addressMap.put('state', STATE_1);
		addressMap.put('postcode', POSTCODE_1);

		User commUser = DF_TestData.createDFCommUser();
		
		EE_WebServiceCalloutMock mockObj = new EE_WebServiceCalloutMock(DF_IntegrationTestData.getPNINegative_PNI_APP_0012());        
        Test.setMock(WebServiceMock.class, mockObj);
		String jsResp; 

		System.runAs(commUser) {

			String opptyBundleId = DF_SF_CaptureController.createOpptyBundle();
			// Create sfreq
			List<String> addressList = new List<String>();
			addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
			String resp1 = DF_TestData.buildResponseString('LOC000005972574', '42.78931', '147.05578', addressList , 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);

			DF_SF_Request__c sfReq1 = DF_TestData.createSFRequest(EE_CISLocationAPIUtils.SEARCH_TYPE_LOCATION_ID, EE_CISLocationAPIUtils.STATUS_PENDING, LOC_ID_1, null, null, opptyBundleId, resp1, addressMap);
			insert sfReq1;

			jsResp = resp1;
		
		}			
		
		DF_ServiceFeasibilityResponse resp = (DF_ServiceFeasibilityResponse)System.JSON.deserialize(jsResp, DF_ServiceFeasibilityResponse.class);
		System.debug('@@@ resp before:' + resp);
		Test.startTest();
			EE_PNIDistanceAPIService.getDistance(resp);
			List<DF_SF_Request__c> sfReqList = [SELECT Id, Status__c FROM DF_SF_Request__c LIMIT 1];
		Test.stopTest();
		System.debug('@@@ resp after:' + resp);
		System.assertEquals(EE_CISLocationAPIUtils.STATUS_VALID, resp.Status); 
	}
}