public with sharing class EE_PNIDistanceHandler extends AsyncQueueableHandler {

	public static final String HANDLER_NAME = 'EE_PNIDistanceHandler';

    public EE_PNIDistanceHandler() {
    	// No of params must be 1
        super(EE_PNIDistanceHandler.HANDLER_NAME, 1);     
    }

    public override void executeWork(List<String> paramList) {    	
    	// Enforce max of 1 input parameter
    	if (paramList.size() == 1) {
    		invokeProcess(paramList[0]);
    	} else {
    		throw new CustomException(AsyncQueueableUtils.ERR_INVALID_PARAMS);
    	}
    }

    private void invokeProcess(String param) {
        EE_PNIDistanceAPIService.getDistanceAsync(param);
    }

}