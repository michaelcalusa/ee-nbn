@isTest
public class EE_PNIDistanceHandler_Test  {
@isTest
	static void PNIDistanceHandlerSuccess() {
		/** Data setup **/
		List<Async_Request__c> asyncReqList;
		Map<String, String> addressInputsMap = new Map<String, String>();
		String HANDLER_NAME = EE_PNIDistanceHandler.HANDLER_NAME;
		String locId = 'LOC000005972574';

		Account acc = DF_TestData.createAccount('Test Acc CIS');
		insert acc;

		DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(acc.id);
		insert opptyBundle;
		String sfReqId = DF_LAPI_APIServiceUtils.createServiceFeasibilityRequest(DF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, locId, null, null, opptyBundle.id, null, addressInputsMap);

		String PARAM_1 = sfReqId;

		// Data Setup
		createASRCustomSettings(HANDLER_NAME);

		// Create asyncReq
		Async_Request__c asyncReq1 = DF_TestData.createAsyncRequest(HANDLER_NAME, 'Pending', PARAM_1, null, null, 0, 0);
		insert asyncReq1;

		EE_WebServiceCalloutMock mockObj = new EE_WebServiceCalloutMock(DF_IntegrationTestData.getPNIPositive());        
        Test.setMock(WebServiceMock.class, mockObj);

		test.startTest();
		EE_PNIDistanceHandler handler = new EE_PNIDistanceHandler();
		handler.run(null);
		handler.executeWork(new List<String> {PARAM_1});
		test.stopTest();

		// Assertions			
		List<DF_SF_Request__c> sfReqList = [SELECT Id, Status__c FROM DF_SF_Request__c LIMIT 2];
		System.assertEquals(EE_CISLocationAPIUtils.STATUS_COMPLETED, sfReqList.get(0).Status__c); 

	}

	private static void createASRCustomSettings(String handler) {
		Async_Request_Settings__c asrSettings = new Async_Request_Settings__c();

		try {
			asrSettings.Name = handler;
			asrSettings.Max_No_of_Rows__c = 1;
			asrSettings.Max_No_of_Parallels__c = 50;
			insert asrSettings;
		} catch (Exception e) {
			throw new CustomException(e.getMessage());
		}
	}

	/** Data Creation **/
	@testSetup static void setup() {
		try {
			createASRConfigCustomSettings();
		} catch (Exception e) {
			throw new CustomException(e.getMessage());
		}
	}

	public static void createASRConfigCustomSettings() {
		try {
			Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
			asrConfigSettings.Max_No_of_Future_Calls__c = 50;
			asrConfigSettings.Max_No_of_Parallels__c = 10;
			asrConfigSettings.Max_No_of_Retries__c = 3;
			asrConfigSettings.Max_No_of_Rows__c = 1;
			upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
		} catch (Exception e) {
			throw new CustomException(e.getMessage());
		}
	}
}