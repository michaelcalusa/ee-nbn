/**
 * Class for ee tn d future schedule.
 */
global class EE_TnDFutureSchedule implements Schedulable {

    String recordId;
    public EE_TnDFutureSchedule(String testId){
    recordId = testId;

    }
    
    /**
     * Execute to tun the future method with callout
     *
     * @param      ctx   The context
     *
     * @return     void
     */
    global void execute(SchedulableContext ctx) {
	List<TND_Result__c> tndResults = [select Id,Name,Req_OVC_Id__c,Req_Class_Of_Service__c,Req_Packet_Size__c,Req_NPT_Duration_Start_Time__c,Req_NPT_Duration_End_Date_Time__c,Req_MTU_Size__c from TND_Result__c where id = :recordId];
		if(tndResults!=null && tndResults.size()>0){
			TND_Result__c resultRec = tndResults.get(0);
			EE_AS_TnDAPIService.runNPTProactiveFutureTest(resultRec.Id,resultRec.Name,resultRec.Req_OVC_Id__c,resultRec.Req_Class_Of_Service__c.split(';'),Integer.valueOf(resultRec.Req_Packet_Size__c),resultRec.Req_NPT_Duration_Start_Time__c,resultRec.Req_NPT_Duration_End_Date_Time__c,Integer.valueOf(resultRec.Req_MTU_Size__c));
		}
	
	//Call the finish method
	finish(ctx);
    }
    
    /**
     * Finish method to abort the job
     *
     * @param      ctx   The context
     */
    void finish(SchedulableContext ctx)
    {
    String jobId = ctx.getTriggerId();
	System.debug('Scheduled Apex Id is: '+jobId);
    system.abortJob(jobId );
    }
    
    
}