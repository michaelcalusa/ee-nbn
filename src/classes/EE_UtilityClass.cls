/* =====================================================================================
* @Class Name :  EE_UtilityClass
* @author : GAgarwal
* @Purpose: This is a generic class to contain methods related to Deal Based Pricing Solution
* @created date: August 23,2018
=======================================================================================*/
public with sharing class EE_UtilityClass {

    /*
        * Returning Commercial Deal based on Account, Criteria and Criteria Value    
        * Parameters : Criteria,CriteriaValue, oppBundleId.
        * @Return : Returns a Commercial_Deal__c record
    */
    @AuraEnabled
    public static string commercialDeal(String Criteria,String CriteriaValue,String oppBundleId){
        String CommercialDealId = '';
        set<Id> accountIds = new set<Id>();
        List<Commercial_Deal_Criteria__c> criteriaList = new List<Commercial_Deal_Criteria__c>();
        if(string.isNotBlank(Criteria) && string.isNotBlank(CriteriaValue) && string.isNotBlank(oppBundleId)){
            for(DF_Opportunity_Bundle__c oppBundleObj:[SELECT Id, Account__c,Account__r.Commercial_Deal__c 
                                                      FROM DF_Opportunity_Bundle__c 
                                                      WHERE Id =: oppBundleId and Account__r.Commercial_Deal__c = true limit:Limits.getLimitQueryRows()]){
                accountIds.add(oppBundleObj.Account__c);
            }
            if(accountIds.size() >0){
                criteriaList = [Select id,name,Commercial_Deal__c,End_Date__c,Criteria_Value__c,Start_Date__c 
                                                                 FROM Commercial_Deal_Criteria__c
                                                                 WHERE Criteria_Value__c =:CriteriaValue and Commercial_Deal__c !=null 
                                                                 and Commercial_Deal__r.Account__c in:accountIds 
                                                                 and Criteria__c =:Criteria and Start_Date__c <=Today 
                                                                 and End_Date__c >=Today 
                                                                 and Commercial_Deal__r.Active__c = true
                                                                 and Commercial_Deal__r.Start_Date__c <= Today 
                                                                 and Commercial_Deal__r.End_Date__c >=Today
                                                                 limit:Limits.getLimitQueryRows()];
                if(!criteriaList.isEmpty()){                                             
                    for(Commercial_Deal_Criteria__c dealCriteria:criteriaList){
                       CommercialDealId=dealCriteria.Commercial_Deal__c;
                    } 
                }else{
                    List<Commercial_Deal__c> dealList = [Select id,Is_Default__c from Commercial_Deal__c where account__c in:accountIds and Is_Default__c = true limit 1] ;
                    if(!dealList.isEmpty()){
                        CommercialDealId = dealList[0].id;
                    }
                }
            }
        }
        return CommercialDealId;
    }

}