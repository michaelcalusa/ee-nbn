/* =====================================================================================
* @Class Name :  EE_UtilityClass_Test
* @author : GAgarwal
* @Purpose: This is a generic class to test methods related to EE_UtilityClass 
* @created date: August 23,2018
=======================================================================================*/
@IsTest
public class EE_UtilityClass_Test {

    /**
    * Tests commercialDeal method
    */
    @isTest static void commercialDealTest()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        Commercial_Deal__c commercialDeal = DF_TestData.commercialDeal(acc.id);
        commercialDeal.Active__c = true;
        commercialDeal.Start_Date__c = system.today().addDays(-2);
        commercialDeal.End_Date__c = system.today().addDays(6);
        insert commercialDeal;
        Commercial_Deal_Criteria__c commercialDealCriteria = DF_TestData.commercialDealCriteria(commercialDeal.id);
        commercialDealCriteria.Criteria_Value__c = 'LOC000035375038';
        commercialDealCriteria.Criteria__c = 'Loc Id';
        insert commercialDealCriteria;
        Test.startTest();
        EE_UtilityClass.commercialDeal('Loc id','LOC000035375038',string.valueOf(oppId));
        Test.stopTest();
    }
    @isTest static void commercialDealTestNegative()
    {
        Id oppId = DF_TestService.getServiceFeasibilityRequest();
        Account acc = DF_TestData.createAccount('Test Account');
        insert acc;
        Commercial_Deal__c commercialDeal = DF_TestData.commercialDeal(acc.id);
        insert commercialDeal;
        Commercial_Deal_Criteria__c commercialDealCriteria = DF_TestData.commercialDealCriteria(commercialDeal.id);
        commercialDealCriteria.Criteria_Value__c = 'LOC000006231845';
        commercialDealCriteria.Criteria__c = 'Loc Id';
        insert commercialDealCriteria;
        Test.startTest();
        EE_UtilityClass.commercialDeal('Loc id','LOC000006231845',string.valueOf(oppId));
        Test.stopTest();
    }
}