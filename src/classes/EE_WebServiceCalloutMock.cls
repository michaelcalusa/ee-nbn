@istest
global class EE_WebServiceCalloutMock implements WebServiceMock {
    private NS_PNI_QueryService.queryResponse_element pniResponse;

	public EE_WebServiceCalloutMock(NS_PNI_QueryService.queryResponse_element pniRes){
        this.pniResponse = pniRes;
    }

    public void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {                        
            response.put('response_x', this.pniResponse);
        }
}