@isTest
public class EmailToCaseService_Test {
    
    
    static testMethod void createCaseForEmailToCase() {
    
         Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        /* Account newAcc = new Account();
        newAcc.name='test';
        insert newAcc;
        
        Contact con=new Contact();
        con.lastname='Testing';
        con.email='test@test.com';
        insert con;*/
        
        email.fromName = 'testtest test';
        email.fromAddress = 'industrycapabilitytest@nbnco.com.au';
         Contact contact = new Contact();
        contact.FirstName = email.fromName.substring(0,email.fromName.indexOf(' '));
        contact.LastName = email.fromName.substring(email.fromName.indexOf(' '));
        contact.Email = email.fromAddress;
        insert contact;
        
         Id formulComplaintRecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId();
         Case caseRecord = new Case();
         caseRecord.Status = 'Open';
         caseRecord.Origin = 'Email';
         caseRecord.Priority = '3-General';    
         caseRecord.Subject = 'testtesttesttesttesttest For Testging';
         caseRecord.Description = 'testtesttesttesttesttest';
         caseRecord.ContactId = contact.id; 
          caseRecord.Primary_Contact_Role__c = 'Contractor';
          caseRecord.Anonymous_Reason__c = 'asdfasdfad';
         insert caseRecord;
         
          Case caseRed = [select id,caseNumber,Subject from case where Subject ='testtesttesttesttesttest For Testging'];
         List<Case> caseList = new List<Case>();
         caseList.add(caseRecord);
        System.debug('**************'+caseRed.caseNumber);
        
        //envelope.fromAddress = 'industrycapabilitytest@nbnco.com.au';

        email.subject = caseRed.CaseNumber;
        
        email.plainTextBody = 'Hello, this a test email body. for testing purposes only.Phone:123456 Bye';
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = new Messaging.InboundEmail.BinaryAttachment[1];  
        Messaging.InboundEmail.BinaryAttachment binaryAttachment = new Messaging.InboundEmail.BinaryAttachment();
        binaryAttachment.Filename = 'test.txt';
        String algorithmName = 'HMacSHA1';
        Blob b = Crypto.generateMac(algorithmName, Blob.valueOf('test'),
        Blob.valueOf('test_key'));
        binaryAttachment.Body = b;
        binaryAttachments[0] =  binaryAttachment ;
        email.binaryAttachments = binaryAttachments ;
        envelope.fromAddress = 'newdevelopmentstest@nbn.com.au';
        


        // setup controller object
        InBoundEmailToCaseServiceHandler catcher = new InBoundEmailToCaseServiceHandler ();
        Messaging.InboundEmailResult result = catcher.handleInboundEmail(email, envelope);
        System.assertEquals( result.success  ,true);
         
        catcher.sendReplyEmail(email,caseRed);
         
         //System.assertEquals( caseRed.Subject ,'testtesttesttesttesttest For Testging');
         
    }
 
}