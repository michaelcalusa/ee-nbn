/**************************************************************************************************************
Class Name				: EnG_TransitionOverrideController
Version                 : 1.0 
Created Date            : 26-04-2018 
Description/Function    : This is used in the lightning component controller to check whether there is any existing
						  transition engagement record associated to the opportunity
Used in                 : Lightning Component - EnG_TransitionOverrideComponent
Modification Log        :
*-------------------------------------------------------------------------------------------------------------
* Developer                   Date                   Description
* -----------------------------------------------------------------------------------------------------------                
* Mohith                   26-04-2018              Created the Class
**************************************************************************************************************/
public class EnG_TransitionOverrideController {	
    @AuraEnabled
    public static String getTransitionRecord(String opportunityId) {        
        if(opportunityId != null) {
            for(Transition_Engagement__c existingEngagement : [SELECT Id FROM Transition_Engagement__c WHERE Opportunity__c =:opportunityId ]) {
                return existingEngagement.Id;
            }       
        }         
        return null;
    }
}