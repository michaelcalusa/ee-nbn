@isTest
public class EnG_TransitionOverrideController_Test {
    
    public static testmethod void test_TransitionOverideController() {
        String strAccRecordTypeID;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Partner Account').getRecordTypeId();  
        
        Account acc = new Account(Name = 'testPartner Account', RecordTypeID = strAccRecordTypeID, OwnerId = UserInfo.getUserId());
        insert acc;
        
        Opportunity opp = new Opportunity();
        opp.Name= 'testopp';
        opp.AccountId = acc.id;
        opp.StageName='Initial Engagement';
        opp.CloseDate = system.today();
        insert opp;
        
        Transition_Engagement__c te = new Transition_Engagement__c();
        te.Opportunity__c = opp.id;
        te.Assigned_To__c = null;
        insert te;
        
        Test.startTest();        
        String TeID;
        TeID = EnG_TransitionOverrideController.getTransitionRecord(opp.Id);
        system.assertEquals(TeID, te.id);
        Test.stopTest();
    }

}