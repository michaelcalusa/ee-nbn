/*
@Auther : Anjani Tiwari
@Date : 07/Nov/2017
@Description : Plateform Event Trigger retry and error handler
*/
public class EventRetryAndErrorHandler{
    public static void RetryOrthrowExMessage(){
        System.debug('Retries Count :'+EventBus.TriggerContext.currentContext().retries);
        if(EventBus.TriggerContext.currentContext().retries < 2){    
            system.debug('@@retries@@'+EventBus.TriggerContext.currentContext().retries);
            throw new EventBus.RetryableException('Condition is not met, so retrying the trigger again.'); 
        }
        else{
            system.debug('Not getting success response after multiple try');
            throw new System.CalloutException('Not getting success response after multiple try');     
        }
    }
}