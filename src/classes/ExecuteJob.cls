public class ExecuteJob
{
	public static void execute()
	{
		String cronExp = '0 00 * * * ?';
		String cronExpHalf = '0 30 * * * ?';
		
		String aJobId = System.schedule('CRMExtract',cronExp,new ScheduleCRMExt());
	    String hJobId = System.schedule('CRMExtractHalfHour',cronExpHalf,new ScheduleCRMExt());
	}
}