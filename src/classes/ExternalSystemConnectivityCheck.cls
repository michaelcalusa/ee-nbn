/***************************************************************************************************
Class Name:  ExternalSystemConnectivityCheck
Class Type: Implements Schedulable Class 
Version     : 1.0 
Created Date: 10 Nov 2016
Function    : This class test the External System connectivity
Description:   This class test the External System connectivity
Used in     :
Modification Log :
* Developer                   Date          Description
* ----------------------------------------------------------------------------                 
* Dilip Athley
* Syed Moosa Nazir TN       10 Nov 2016      Created - Version 1.0
****************************************************************************************************/
public class ExternalSystemConnectivityCheck implements Schedulable{
    static List<Integration_Connectivity_Test__c> listofIntegrationConnectivity = new List<Integration_Connectivity_Test__c>();
    /***************************************************************************************************
    Method Name:  CRMod_Connectivity
    Method Type: void
    Version     : 1.0 
    Created Date: 10 Nov 2016
    Function / Description : This class will be used to test CRM On Demand is up and Running for CRMOD - SF P2P Integration.
    Input Parameters:None
    Output Parameters: None
    Used in   :
    Modification Log :
    * Developer                             Date                   Description
    * ----------------------------------------------------------------------------                 
    * Dilip Athley (v-dileepathley)       10 Nov 2016             Created - Version 1.0 
    ****************************************************************************************************/
    public static void CRMod_Connectivity(){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setEndpoint('callout:CRMOD_Connection');
        DateTime beforeReq = System.now();
        HttpResponse response = http.send(request);
        DateTime afterRes = System.now();
        
        Integration_Connectivity_Test__c icTest = new Integration_Connectivity_Test__c();
        icTest.Application_Name__c = 'CRM On Demand';
        icTest.Status__c = response.getStatusCode()==200 ? 'Successful' : 'Failed';
        icTest.Status_Code__c = string.valueOf(response.getStatusCode());
        icTest.Request__c = String.valueOf(request);
        icTest.Response__c = String.valueOf(response) + 'Body: ' + response.getBody();
        icTest.Response_Time__c = afterRes.getTime() - beforeReq.getTime();
        listofIntegrationConnectivity.add(icTest);
    }
    /***************************************************************************************************
    Method Name:  CIS_Connectivity
    Method Type: void
    Version     : 1.0 
    Created Date: 10 Nov 2016
    Function / Description : This class is used to test CIS connectivity. Testing with "Unstructured Address search"
    Input Parameters:None
    Output Parameters: None
    Used in   :
    Modification Log :
    * Developer                         Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN              10 Nov 2016             Created - Version 1.0 
    ****************************************************************************************************/
    public static void CIS_Connectivity(){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        string typedInAddress = EncodingUtil.urlEncode('100 Arthur', 'utf-8');
        String URLEndPoint = '?filter=address.unstructured='+ typedInAddress;
        request.setMethod('GET');
        request.setEndpoint('callout:CISURL'+URLEndPoint);
        DateTime beforeReq = System.now();
        HttpResponse response = http.send(request);
        DateTime afterRes = System.now();
        
        Integration_Connectivity_Test__c icTest = new Integration_Connectivity_Test__c();
        icTest.Application_Name__c = 'CIS';
        icTest.Status__c = response.getStatusCode()==200 ? 'Successful' : 'Failed';
        icTest.Status_Code__c = string.valueOf(response.getStatusCode());
        icTest.Request__c = String.valueOf(request);
        icTest.Response__c = String.valueOf(response) + 'Body: ' + response.getBody();
        icTest.Response_Time__c = afterRes.getTime() - beforeReq.getTime();
        listofIntegrationConnectivity.add(icTest);
    }
    public static void execute(SchedulableContext ctx){
        executeCallouts();
    }
    @future(callout=true)
    public static void executeCallouts(){
        listofIntegrationConnectivity = new List<Integration_Connectivity_Test__c>();
        CRMod_Connectivity();
        CIS_Connectivity();
        if(!listofIntegrationConnectivity.isEmpty())
            insert listofIntegrationConnectivity;
    }
}