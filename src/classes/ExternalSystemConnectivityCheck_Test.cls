/***************************************************************************************************
Class Name:  ExternalSystemConnectivityCheck_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 31-10-2016
Function    : This class contains unit test scenarios for ExternalSystemConnectivityCheck schedule class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       10 Nov 2016               Created
****************************************************************************************************/
@isTest
public class ExternalSystemConnectivityCheck_Test {
    static testMethod void ExternalSystemConnectivityCheck_Test(){
        // Test the schedule class
        Test.StartTest();
        string body = '{'+
        '   \"CustomObjects2\":['+
        '    {'+
        '         \"Id\": \"AYCA-3QA3EI\",'+
        '         \"Name\": \"60 & 70 Queen Street\",'+
        '         \"Type\": \"Medium/Large\",'+
        '         \"CustomPickList26\": \"\",'+
        '         \"AccountName\": \"Darazzo Pty Ltd ATF The Queen Street Unit Trust\",'+
        '         \"AccountId\": \"AYCA-3Q9SGG\",'+
        '         \"ContactId\": \"AYCA-2AI5J3\",'+
        '         \"Owner\": \"Claire McGregor\",'+
        '         \"CustomText36\": \"-34.932064\",'+
        '         \"CustomText32\": \"138.604627\",'+
        '         \"CustomText46\": \"\",'+
        '         \"ContactFullName\": \"Claire Mcgregor\",'+
        '         \"CustomPickList3\": \"MDA\",'+
        '         \"CustomPickList10\": \"Closed\",'+
        '         \"CustomText0\": \"60-70 Queen Street\",'+
        '         \"CustomText34\": \"Adelaide\",'+
        '         \"CustomPickList8\": \"SA\",'+
        '         \"CustomText37\": \"5000\",'+
        '         \"CustomText4\": \"60 - 70 Queen Street, Adelaide SA\",'+
        '         \"CustomText1\": \"\",'+
        '         \"CustomObject1Name\": \"\",'+
        '         \"CustomObject4Name\": \"\",'+
        '         \"CustomPickList31\": \"\",'+
        '         \"IndexedPick1\": \"Development Withdrawn\",'+
        '         \"IndexedPick0\": \"Not Started\",'+
        '         \"CustomNumber4\": 0,'+
        '         \"CustomInteger2\": 50,'+
        '         \"IndexedPick2\": \"\",'+
        '         \"CustomDate33\": \"\",'+
        '         \"CustomInteger7\": 0,'+
        '         \"CustomPickList22\": \"Yes\",'+
        '         \"ModifiedDate\": \"2016-09-16T17:20:03Z\"'+
        '    }'+
        '   ],'+
        '   \"_contextInfo\":    {'+
        '      \"limit\": 100,'+
        '      \"offset\": 0,'+
        '      \"lastpage\": true'+
        '   }'+
        '}';
        integer statusCode = 200;
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test CRModFakeResponse = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, CRModFakeResponse);
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        System.schedule('ExternalSystemConnectivityCheck_Test',CRON_EXP, new ExternalSystemConnectivityCheck());
        Test.stopTest();        
    }
}