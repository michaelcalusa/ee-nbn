public class ExternalSystemRequestJsonToApex {
    public static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.START_OBJECT || 
                curr == JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == JSONToken.END_OBJECT ||
                curr == JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    public class CaseOwner {
        public String firstName {get;set;} 
        public String lastName {get;set;} 
        public String phoneNumber {get;set;} 
        public String emailAddress {get;set;} 
        public String preferredContactMethod {get;set;} 

        public CaseOwner(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'firstName') {
                            firstName = parser.getText();
                        } else if (text == 'lastName') {
                            lastName = parser.getText();
                        } else if (text == 'phoneNumber') {
                            phoneNumber = parser.getText();
                        } else if (text == 'emailAddress') {
                            emailAddress = parser.getText();
                        } else if (text == 'preferredContactMethod') {
                            preferredContactMethod = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'CaseOwner consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    public class NbnReferenceIds {
        public String id {get;set;} 
        public String type_Z {get;set;} // in json: type

        public NbnReferenceIds(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'id') {
                            id = parser.getText();
                        } else if (text == 'type') {
                            type_Z = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'NbnReferenceIds consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    public class CaseRequester {
        public String firstName {get;set;} 
        public String lastName {get;set;} 
        public String phoneNumber {get;set;} 
        public String emailAddress {get;set;} 

        public CaseRequester(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'firstName') {
                            firstName = parser.getText();
                        } else if (text == 'lastName') {
                            lastName = parser.getText();
                        } else if (text == 'phoneNumber') {
                            phoneNumber = parser.getText();
                        } else if (text == 'emailAddress') {
                            emailAddress = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'CaseRequester consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    public class ContactDetails {
        public CaseOwner caseOwner {get;set;} 
        public CaseRequester caseRequester {get;set;} 

        public ContactDetails(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'caseOwner') {
                            caseOwner = new CaseOwner(parser);
                        } else if (text == 'caseRequester') {
                            caseRequester = new CaseRequester(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'ContactDetails consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    public CreateCaseRequestDetails createCaseRequestDetails {get;set;} 
    public ExternalSystemRequestJsonToApex(JSONParser parser) {
        while (parser.nextToken() != JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != JSONToken.VALUE_NULL) {
                    if (text == 'createCaseRequestDetails') {
                        createCaseRequestDetails = new CreateCaseRequestDetails(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'Root consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    public class CaseDetails {
        public String isNewCase {get;set;}
        public String caseNumber {get;set;}
        public String caseType {get;set;}
        public String dateofComplaintevent {get;set;}
        public String isTio {get;set;}
        public String tioReferenceId {get;set;}
        public String tioLevel {get;set;}
        public String description {get;set;}
        public String desiredOutcome {get;set;}
        public List<NbnReferenceIds> nbnReferenceIds {get;set;}
        public String nbnServiceId {get;set;}
        public String whatDamaged {get;set;}
        public String whereDamaged {get;set;}
        public String whenDamaged {get;set;}
        public String howDamaged {get;set;}
        public String whoDamaged {get;set;}
        
        
        //changed here added by Wayne Ni
        public String campaignName {get;set;}

        public CaseDetails(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'isNewCase') {
                            isNewCase = parser.getText();
                        } else if (text == 'caseNumber') {
                            caseNumber = parser.getText();
                        } else if (text == 'caseType') {
                            caseType = parser.getText();
                        } else if (text == 'dateofComplaintevent') {
                            dateofComplaintevent = parser.getText();
                        } else if (text == 'isTio') {
                            isTio = parser.getText();
                        } else if (text == 'tioReferenceId') {
                            tioReferenceId = parser.getText();
                        } else if (text == 'tioLevel') {
                            tioLevel = parser.getText();
                        } else if (text == 'description') {
                            description = parser.getText();
                        } else if (text == 'desiredOutcome') {
                            desiredOutcome = parser.getText();
                        } else if (text == 'nbnReferenceIds') {
                            nbnReferenceIds = new List<NbnReferenceIds>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                nbnReferenceIds.add(new NbnReferenceIds(parser));
                            }
                        } else if (text == 'nbnServiceId') {
                            nbnServiceId = parser.getText();
                        } else if (text == 'whatDamaged') {
                            whatDamaged = parser.getText();
                        } else if (text == 'whereDamaged') {
                            whereDamaged = parser.getText();
                        } else if (text == 'whenDamaged') {
                            whenDamaged = parser.getText();
                        } else if (text == 'howDamaged') {
                            howDamaged = parser.getText();
                        } else if (text == 'whoDamaged') {
                            whoDamaged = parser.getText();
                        } 
                        //Added by Wayne here
                        else if (text == 'campaignName'){
                            campaignName = parser.getText();
                        }
                        //End here
                        else {
                            System.debug(LoggingLevel.WARN, 'CaseDetails consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    public class AttachmentFailureInfo {
        public String fileName {get;set;} 
        public String fileSize {get;set;} 
        public String failureMessage {get;set;} 

        public AttachmentFailureInfo(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'fileName') {
                            fileName = parser.getText();
                        } else if (text == 'fileSize') {
                            fileSize = parser.getText();
                        } else if (text == 'failureMessage') {
                            failureMessage = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'AttachmentFailureInfo consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    public class ContactAuthorisation {
        public String contactInstructions {get;set;} 

        public ContactAuthorisation(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'contactInstructions') {
                            contactInstructions = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'ContactAuthorisation consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    public class LocationAddress {
        public String premisetype {get;set;} 
        public String unitNumber {get;set;} 
        public String streetOrLotnumber {get;set;} 
        public String streetName {get;set;} 
        public String streetType {get;set;} 
        public String suburbOrlocality {get;set;} 
        public String state {get;set;} 
        public String postCode {get;set;}
        public String country {get;set;} 

        public LocationAddress(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'premisetype') {
                            premisetype = parser.getText();
                        } else if (text == 'unitNumber') {
                            unitNumber = parser.getText();
                        } else if (text == 'streetOrLotnumber') {
                            streetOrLotnumber = parser.getText();
                        } else if (text == 'streetName') {
                            streetName = parser.getText();
                        } else if (text == 'streetType') {
                            streetType = parser.getText();
                        } else if (text == 'suburbOrlocality') {
                            suburbOrlocality = parser.getText();
                        } else if (text == 'state') {
                            state = parser.getText();
                        } else if (text == 'postCode') {
                            postCode = parser.getText();
                        } else if (text == 'country') {
                            country = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'LocationAddress consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    public class AccessSeekerDetails {
        public String yourReferenceId  {get;set;} 
        public String accessSeekerId {get;set;} 
        public String organisationName {get;set;} 
        public AccessSeekerDetails(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'yourReferenceId') {
                            yourReferenceId  = parser.getText();
                        } else if (text == 'accessSeekerId') {
                            accessSeekerId = parser.getText();
                        } else if (text == 'organisationName') {
                            organisationName = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'AccessSeekerDetails consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    } 
    public class CreateCaseRequestDetails {
        public String businessChannel {get;set;} 
        public String correlationId {get;set;}
        public String isAttachmentRequest {get;set;} 
        public String isAttachmentAvailable {get;set;} 
        public String timestamp {get;set;} 
        public AccessSeekerDetails accessSeekerDetails {get;set;} 
        public CaseDetails caseDetails {get;set;} 
        public ContactAuthorisation contactAuthorisation {get;set;} 
        public LocationAddress locationAddress {get;set;} 
        public ContactDetails contactDetails {get;set;}
        public AttachmentDetails attachmentDetails {get;set;}
        public CreateCaseRequestDetails(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'businessChannel') {
                            businessChannel = parser.getText();
                        } else if (text == 'isAttachmentRequest') {
                            isAttachmentRequest = parser.getText();
                        } else if (text == 'isAttachmentAvailable') {
                            isAttachmentAvailable = parser.getText();
                        } else if (text == 'correlationId') {
                            correlationId = parser.getText();
                        } else if (text == 'timestamp') {
                            timestamp = parser.getText();
                        } else if (text == 'accessSeekerDetails') {
                            accessSeekerDetails = new AccessSeekerDetails(parser);
                        } else if (text == 'caseDetails') {
                            caseDetails = new CaseDetails(parser);
                        } else if (text == 'contactAuthorisation') {
                            contactAuthorisation = new ContactAuthorisation(parser);
                        } else if (text == 'locationAddress') {
                            locationAddress = new LocationAddress(parser);
                        } else if (text == 'contactDetails') {
                            contactDetails = new ContactDetails(parser);
                        } else if (text == 'attachmentDetails') {
                            attachmentDetails = new AttachmentDetails(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'CreateCaseRequestDetails consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    public class AttachmentDetails {
        public String success {get;set;} 
        public List<AttachmentFailureInfo> attachmentFailureInfo {get;set;} 

        public AttachmentDetails(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'success') {
                            success = parser.getText();
                        } else if (text == 'attachmentFailureInfo') {
                            attachmentFailureInfo = new List<AttachmentFailureInfo>();
                            while (parser.nextToken() != JSONToken.END_ARRAY) {
                                attachmentFailureInfo.add(new AttachmentFailureInfo(parser));
                            }
                        } else {
                            System.debug(LoggingLevel.WARN, 'AttachmentDetails consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    public static ExternalSystemRequestJsonToApex parse(String json) {
        return new ExternalSystemRequestJsonToApex(System.JSON.createParser(json));
    }
}