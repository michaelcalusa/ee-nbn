/***************************************************************************************************
Class Name:  ExternalSystemRequestJsonToApex_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : This class contains unit test scenarios for  ExternalSystemRequestJsonToApex apex class which converts the JSON to APEX (Both Service Portal and Contact Us Form)
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       26/10/2015                 Created
****************************************************************************************************/
@IsTest
public class ExternalSystemRequestJsonToApex_Test {
    /***************************************************************************************************
    Method Name:  testParse
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  Testing JSON to apex Conversion.
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void testParse() {
        String json = '{'+
        '  \"createCaseRequestDetails\": {'+
        '    \"businessChannel\": \"Service Portal\",'+
        '       \"isAttachmentRequest\": \"true\",'+
        '       \"isAttachmentAvailable\": \"true\",'+
        '    \"correlationId\": \"4eef4405-606e-46db-8c5c-af78e2874077\",'+
        '    \"timeStamp\": \"2016-08-23T12:00:00Z\",'+
        '    \"accessSeekerDetails\": {'+
        '      \"yourReferenceId \": \"1-8526607052\",'+
        '      \"accessSeekerId\": \"ASI000000000035\",'+
        '   \"organisationName\": \"Test Access Seeker\"'+
        '    },'+
        '    \"caseDetails\": {'+
        '     \"isNewCase\": \"false\",'+
        '     \"caseNumber\": \"00001234\",'+
        '      \"caseType\": \"Formal Complaint\",'+
        '      \"dateofComplaintevent\": \"2016-07-11T14:00:01Z\",'+
        '      \"isTio\": \"Y\",'+
        '      \"tioReferenceId\": \"178378-8912894-X\",'+
        '      \"tioLevel\": \"TIO Level\",'+
        '      \"description\": \"Annoying sound from NTD / please prioritize cx is 70 years old\",'+
        '      \"desiredOutcome\": \"Customer is requesting to have technician visit\",'+
        '      \"nbnReferenceIds\": ['+
        '        {'+
        '          \"id\": \"ORD123456789012\",'+
        '          \"type\": \"Order\"'+
        '        },'+
        '        {'+
        '          \"id\": \"INC123456789012\",'+
        '          \"type\": \"Incident\"'+
        '        },'+
        '        {'+
        '          \"id\": \"INC123456789023\",'+
        '          \"type\": \"Incident\"'+
        '        }'+
        '      ],'+
        '       \"nbnServiceId\": \"AVC901280915\",'+
        '       \"WhatDamaged\": \"My House compound wall Damaged\",'+
        '       \"WhereDamaged\": \"My House entrance\",'+
        '       \"WhenDamaged\": \"05-10-2016\",'+
        '       \"HowDamaged\": \"Some reason\",'+
        '       \"WhoDamaged\": \"NBN Damaged\"'+
        '    },'+
        '    \"contactAuthorisation\": {'+
        '      \"contactInstructions\": \"Both Parties\"'+
        '    },'+
        '    \"locationAddress\": {'+
        '      \"premisetype\": \"Multi Dwelling Unit (MDU)\",'+
        '      \"unitNumber\": \"L5\",'+
        '      \"streetOrLotnumber\": \"50\",'+
        '      \"streetName\": \"MILLER\",'+
        '      \"streetType\": \"STREET\",'+
        '      \"suburbOrlocality\": \"NORTH SYDNEY\",'+
        '      \"state\": \"NSW\",'+
        '      \"postCode\": \"2600\",'+
        '       \"country\": \"Australia\"'+
        '    },'+
        '    \"contactDetails\": {'+
        '      \"caseOwner\": {'+
        '        \"firstName\": \"Bugs\",'+
        '        \"lastName\": \"Bunny\",'+
        '        \"phoneNumber\": \"0212346789\",'+
        '        \"emailAddress\": \"bugs@looneytunes.com\",'+
        '       \"preferredContactMethod\": \"Preferred Phone\"'+
        '      },'+
        '      \"caseRequester\": {'+
        '        \"firstName\": \"Yosemite\",'+
        '        \"lastName\": \"Sam\",'+
        '        \"phoneNumber\": \"0212346788\",'+
        '        \"emailAddress\": \"angrysam@looneytunes.com\"'+
        '      }'+
        '    },'+
        '       \"attachmentDetails\": {'+
        '           \"success\": \"false\",'+
        '           \"attachmentFailureInfo\": [{'+
        '               \"fileName\": \"FileName1.pdf\",'+
        '               \"fileSize\": \"17.07 MB\",'+
        '               \"failureMessage\": \"Uploading failed due to A reason.\"'+
        '           }, {'+
        '               \"fileName\": \"FileName2.txt\",'+
        '               \"fileSize\": \"2.10 kB\",'+
        '               \"failureMessage\": \"Uploading failed due to B reason.\"'+
        '           }, {'+
        '               \"fileName\": \"FileName3.txt\",'+
        '               \"fileSize\": \"24.10 kB\",'+
        '               \"failureMessage\": \"Uploading failed due to C reason.\"'+
        '           }, {'+
        '               \"fileName\": \"FileName4.txt\",'+
        '               \"fileSize\": \"12.10 MB\",'+
        '               \"failureMessage\": \"Uploading failed due to D reason.\"'+
        '           }, {'+
        '               \"fileName\": \"FileName5.txt\",'+
        '               \"fileSize\": \"21.10 MB\",'+
        '               \"failureMessage\": \"Uploading failed due to E reason.\"'+
        '           }]'+
        '       }'+
        '  }'+
        '}';
        ExternalSystemRequestJsonToApex r = ExternalSystemRequestJsonToApex.parse(json);
        System.assert(r != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        ExternalSystemRequestJsonToApex.CaseOwner objCaseOwner = new ExternalSystemRequestJsonToApex.CaseOwner(System.JSON.createParser(json));
        System.assert(objCaseOwner != null);
        System.assert(objCaseOwner.firstName == null);
        System.assert(objCaseOwner.lastName == null);
        System.assert(objCaseOwner.phoneNumber == null);
        System.assert(objCaseOwner.emailAddress == null);
        System.assert(objCaseOwner.preferredContactMethod == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        ExternalSystemRequestJsonToApex.NbnReferenceIds objNbnReferenceIds = new ExternalSystemRequestJsonToApex.NbnReferenceIds(System.JSON.createParser(json));
        System.assert(objNbnReferenceIds != null);
        System.assert(objNbnReferenceIds.id == null);
        System.assert(objNbnReferenceIds.type_Z == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        ExternalSystemRequestJsonToApex.CaseRequester objCaseRequester = new ExternalSystemRequestJsonToApex.CaseRequester(System.JSON.createParser(json));
        System.assert(objCaseRequester != null);
        System.assert(objCaseRequester.firstName == null);
        System.assert(objCaseRequester.lastName == null);
        System.assert(objCaseRequester.phoneNumber == null);
        System.assert(objCaseRequester.emailAddress == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        ExternalSystemRequestJsonToApex.ContactDetails objContactDetails = new ExternalSystemRequestJsonToApex.ContactDetails(System.JSON.createParser(json));
        System.assert(objContactDetails != null);
        System.assert(objContactDetails.caseOwner == null);
        System.assert(objContactDetails.caseRequester == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        ExternalSystemRequestJsonToApex objRoot = new ExternalSystemRequestJsonToApex(System.JSON.createParser(json));
        System.assert(objRoot != null);
        System.assert(objRoot.createCaseRequestDetails == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        ExternalSystemRequestJsonToApex.CaseDetails objCaseDetails = new ExternalSystemRequestJsonToApex.CaseDetails(System.JSON.createParser(json));
        System.assert(objCaseDetails != null);
        System.assert(objCaseDetails.isNewCase == null);
        System.assert(objCaseDetails.caseNumber == null);
        System.assert(objCaseDetails.caseType == null);
        System.assert(objCaseDetails.dateofComplaintevent == null);
        System.assert(objCaseDetails.isTio == null);
        System.assert(objCaseDetails.tioReferenceId == null);
        System.assert(objCaseDetails.tioLevel == null);
        System.assert(objCaseDetails.description == null);
        System.assert(objCaseDetails.desiredOutcome == null);
        System.assert(objCaseDetails.nbnReferenceIds == null);
        System.assert(objCaseDetails.nbnServiceId == null);
        System.assert(objCaseDetails.WhatDamaged == null);
        System.assert(objCaseDetails.WhereDamaged == null);
        System.assert(objCaseDetails.WhenDamaged == null);
        System.assert(objCaseDetails.HowDamaged == null);
        System.assert(objCaseDetails.WhoDamaged == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        ExternalSystemRequestJsonToApex.ContactAuthorisation objContactAuthorisation = new ExternalSystemRequestJsonToApex.ContactAuthorisation(System.JSON.createParser(json));
        System.assert(objContactAuthorisation != null);
        System.assert(objContactAuthorisation.contactInstructions == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        ExternalSystemRequestJsonToApex.LocationAddress objLocationAddress = new ExternalSystemRequestJsonToApex.LocationAddress(System.JSON.createParser(json));
        System.assert(objLocationAddress != null);
        System.assert(objLocationAddress.premisetype == null);
        System.assert(objLocationAddress.unitNumber == null);
        System.assert(objLocationAddress.streetOrLotnumber == null);
        System.assert(objLocationAddress.streetName == null);
        System.assert(objLocationAddress.streetType == null);
        System.assert(objLocationAddress.suburbOrlocality == null);
        System.assert(objLocationAddress.state == null);
        System.assert(objLocationAddress.postCode == null);
        System.assert(objLocationAddress.country == null);
        
        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        ExternalSystemRequestJsonToApex.AttachmentFailureInfo objAttachmentFailureInfo = new ExternalSystemRequestJsonToApex.AttachmentFailureInfo(System.JSON.createParser(json));
        System.assert(objAttachmentFailureInfo != null);
        System.assert(objAttachmentFailureInfo.fileName == null);
        System.assert(objAttachmentFailureInfo.fileSize == null);
        System.assert(objAttachmentFailureInfo.failureMessage == null);
        
        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        ExternalSystemRequestJsonToApex.AccessSeekerDetails objAccessSeekerDetails = new ExternalSystemRequestJsonToApex.AccessSeekerDetails(System.JSON.createParser(json));
        System.assert(objAccessSeekerDetails != null);
        System.assert(objAccessSeekerDetails.yourReferenceId  == null);
        System.assert(objAccessSeekerDetails.accessSeekerId == null);
        System.assert(objAccessSeekerDetails.organisationName == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        ExternalSystemRequestJsonToApex.CreateCaseRequestDetails objCreateCaseRequestDetails = new ExternalSystemRequestJsonToApex.CreateCaseRequestDetails(System.JSON.createParser(json));
        System.assert(objCreateCaseRequestDetails != null);
        System.assert(objCreateCaseRequestDetails.businessChannel == null);
        System.assert(objCreateCaseRequestDetails.isAttachmentRequest == null);
        System.assert(objCreateCaseRequestDetails.isAttachmentAvailable == null);
        System.assert(objCreateCaseRequestDetails.correlationId == null);
        System.assert(objCreateCaseRequestDetails.timeStamp == null);
        System.assert(objCreateCaseRequestDetails.accessSeekerDetails == null);
        System.assert(objCreateCaseRequestDetails.caseDetails == null);
        System.assert(objCreateCaseRequestDetails.contactAuthorisation == null);
        System.assert(objCreateCaseRequestDetails.locationAddress == null);
        System.assert(objCreateCaseRequestDetails.contactDetails == null);
        System.assert(objCreateCaseRequestDetails.attachmentDetails == null);
        
        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        ExternalSystemRequestJsonToApex.AttachmentDetails objAttachmentDetails = new ExternalSystemRequestJsonToApex.AttachmentDetails(System.JSON.createParser(json));
        System.assert(objAttachmentDetails != null);
        System.assert(objAttachmentDetails.success == null);
        System.assert(objAttachmentDetails.attachmentFailureInfo == null);
    }
}