public class FALPhoneNumber_CX{
/*------------------------------------------------------------------------
Author:        Rohit Kumar
Company:       Salesforce
Description:   A class created to display all Fire & Lift Phone number records based on matching either of Contact Numbers or Email Address

Test Class:    FALPhoneNumber_Test
History
<Date>            <Authors Name>    <Brief Description of Change>
--------------------------------------------------------------------------*/   
    private FAL_Phone_Number__c falPhoneNumber ;
    private string whereClause ;
    private string soqlQuery;
    private boolean conditionsFound;
    private final integer PAGINATION_SIZE = 5;
    
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    
    public FALPhoneNumber_CX(ApexPages.StandardController stdController){
        this.falPhoneNumber = (FAL_Phone_Number__c) stdController.getRecord();
        
        falPhoneNumber = [select Id,Contact_Number_1__c,Contact_Number_2__c,Email_Address__c from FAL_Phone_Number__c where Id = :falPhoneNumber.id ];
        
        whereClause = 'Id != \'' + falPhoneNumber.id + '\' AND ( ';
        
        if(falPhoneNumber.Contact_Number_1__c != null){ // prepare the where condition for SOQL if Contact Number 1 is not null
            whereClause += 'Contact_Number_1__c = \'' + falPhoneNumber.Contact_Number_1__c + '\' OR ';
            conditionsFound = true;
        }
        if(falPhoneNumber.Contact_Number_2__c != null){ // prepare the where condition for SOQL if Contact Number 2 is not null
            whereClause += 'Contact_Number_2__c = \'' + falPhoneNumber.Contact_Number_2__c +'\' OR ';
            conditionsFound = true;
        }
        if(falPhoneNumber.Email_Address__c != null){ // prepare the where condition for SOQL if Email Address is not null
            whereClause += 'Email_Address__c = \'' +falPhoneNumber.Email_Address__c +'\' OR ';
            conditionsFound = true;
        }
        //Remove the last OR Clause 
        if(conditionsFound){
            whereClause = whereClause.substring(0,whereClause.length()-3);
            whereClause += ' ) ';
        }else{
            whereClause = whereClause.substring(0,whereClause.length()-6); // remove last bracket
        }
        
        System.Debug( 'NBN: -> Dynamic whereClause ' + whereClause );
        //Preapre the SOQL Query
        soqlQuery = fetchFALPhoneNumberSOQL( whereClause ); 
        
        System.Debug( 'NBN: -> Dynamic SOQL QUERY ' + soqlQuery );
        
    }
    
    /**
     *  Prepare the SOQL Query and pull fields from Field Set FALPhoneNumber
     *  @Params : Pass the where Clause to be added in SOQL Query
     */
    private static string fetchFALPhoneNumberSOQL( string whereClause){
        
        String query = 'SELECT ';
        try{
            for(Schema.FieldSetMember f : SObjectType.FAL_Phone_Number__c.FieldSets.FALPhoneNumber.getFields() ) {
                query += f.getFieldPath() + ', ';
             }
             query += 'Id FROM FAL_Phone_Number__c Where ' + whereClause;
        }catch(Exception ex){
            GlobalUtility.logMessage('Error','FALPhoneNumberExtension','fetchFALPhoneNumberSOQL','','','','',ex,null);
        }
        
        return query; 
     }
    
    /**
     * Standard set controller
     */
     public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {
                size = PAGINATION_SIZE;
                try{
                 setCon = new ApexPages.StandardSetController(Database.getQueryLocator( soqlQuery ));   
                }catch(Exception ex){
                    GlobalUtility.logMessage('Error','FALPhoneNumberExtension','SetCon','','','','',ex,null);
                }
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
            }
            return setCon;
        }
        set;
    }
    
    /**
     * method to return list of records
     */
    public List<FAL_Phone_Number__c> getfalPhoneNumberRecords() {
        
        System.Debug( 'NBN: -> FAL Return List ' + (List<FAL_Phone_Number__c>) setCon.getRecords() );
        return (List<FAL_Phone_Number__c>) setCon.getRecords();
    }
    
    /**
     *  Pagination method to point to first record
     */ 
    public void first() {
        setCon.first();
    }
   
    /**
     *  Pagination method to point to last record
     */ 
    public void last() {
        setCon.last();
    }
   
    /**
     *  Pagination method to point to previous record
     */ 
    public void previous() {
        setCon.previous();
    }
   
    /**
     *  Pagination method to point to next record
     */ 
    public void next() {
        setCon.next();
    }
    
    /*
     * indicates whether there are more records after the current page set.
     */
    public Boolean hasNext {
        get {
            return setcon.getHasNext();
        }
        set;
    }

    /* 
     * indicates whether there are more records before the current page set.
     */
    public Boolean hasPrevious {
        get {
            return setcon.getHasPrevious();
        }
        set;
    }

     /*
     * returns the page number of the current page set
     */
    public Integer pageNumber {
        get {
            return setcon.getPageNumber();
        }
        set;
    }

     
}