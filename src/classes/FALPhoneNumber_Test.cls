@isTest
private class FALPhoneNumber_Test{
/*------------------------------------------------------------------------
Author:        Rohit Kumar
Company:       Salesforce
Description:   A test class created to test FalPhoneNumberExtension
History
<Date>            <Authors Name>    <Brief Description of Change>
--------------------------------------------------------------------------*/   
    private static Site__c siteRecord ;
    private static list<FAL_Phone_Number__c> falPhoneNumberRecord;
    
    static void setupTestData(){ // Method to initialize the test data
        siteRecord = TestDataUtility.createSiteTestRecords(1,true)[0];
        falPhoneNumberRecord = TestDataUtility.createFALPhoneNumberTestRecords(50,true,siteRecord.Id);
    }
    
    static testMethod void testFALPhoneNumberExtension(){
        // set up test data
        setupTestData();
        // Initialize Controller Extension
        ApexPages.StandardController con = new ApexPages.StandardController(falPhoneNumberRecord.get(0));
        FALPhoneNumber_CX ext = new FALPhoneNumber_CX(con);
        ext.getfalPhoneNumberRecords();
        
        system.assertEquals(ext.hasPrevious,false);
        
        if(ext.hasNext){
            ext.next();
            system.assertEquals(ext.pageNumber,2);
        }
        
        if(ext.hasPrevious){
            ext.previous();
            system.assertEquals(ext.pageNumber,1);
        }
        
        ext.first();
        ext.last();
    }

}