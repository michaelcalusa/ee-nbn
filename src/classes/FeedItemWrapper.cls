public class FeedItemWrapper{
        @AuraEnabled public String Id{get;set;}
        @AuraEnabled public String ParentId{get;set;}
        @AuraEnabled public String Type{get;set;}
        @AuraEnabled public String CreatedById{get;set;}
        @AuraEnabled public DateTime CreatedDate{get;set;}
        @AuraEnabled public String Title{get;set;}
        @AuraEnabled public String BodyHeader{get;set;}
        @AuraEnabled public String Body{get;set;}
        @AuraEnabled public String NetworkScope{get;set;}
        @AuraEnabled public String Visibility{get;set;}
        @AuraEnabled public boolean isLongPost{get;set;}
        @AuraEnabled public String strRSPName{get;set;}
        @AuraEnabled public String strUsersName{get;set;}
        @AuraEnabled public String strPhotoURL{get;set;} 
     
        public FeedItemWrapper(String xId, String xParentId, String xType, String xCreatedById, DateTime xCreatedDate, String xTitle, String xBodyHeader, String xBody, String xNetworkScope, String xVisibility,Boolean xIsLongPost,String xstrRSPName,String xstrUsersName,String xstrPhotoURL){
            Id = xId;
            ParentId = xParentId;
            Type = xType;
            CreatedById = xCreatedById;
            CreatedDate = xCreatedDate;
            Title = xTitle;
            Body = xBody;
            BodyHeader = xBodyHeader;
            NetworkScope = xNetworkScope;
            Visibility = xVisibility;
            isLongPost = xIsLongPost;
            strRSPName = xstrRSPName;
            strUsersName = xstrUsersName;
            strPhotoURL = xstrPhotoURL;
        }
    }