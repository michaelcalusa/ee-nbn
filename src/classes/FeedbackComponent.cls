/*
 *@Author:       Allanah Mae San Miguel
 *@Created Date: 2017-04-17
 *@Description:  Controller for rsp community feedback component
 */

public class FeedbackComponent {
	@AuraEnabled
    public static boolean sendFeedback(String feedback, Id articleId) {
        System.debug('submitFeedback');
        //System.debug('feedback: ' + feedback);
        //System.debug('articleId: '+ articleId);
        
        List<KnowledgeArticleVersion> articles = [SELECT ArticleCreatedDate,ArticleNumber,ArticleType,CreatedById,CreatedDate,
                 							FirstPublishedDate,Id, KnowledgeArticleId,LastModifiedById,LastPublishedDate,
                                          	Title,UrlName,VersionNumber
                                           FROM KnowledgeArticleVersion
                                           WHERE Id=:articleId
                                          	AND PublishStatus = 'Online'];
        
        if (!articles.isEmpty() && !String.isBlank(feedback)) {
            KnowledgeArticleVersion article = articles[0];
            //write to article feedback obj
            aft__Article_Feedback__c articleFeedback = new aft__Article_Feedback__c();
            
            //articleFeedback.aft__Action_Date__c = 
            //articleFeedback.aft__Action_Taken__c = 
            articleFeedback.aft__Article_Created_Date__c = article.ArticleCreatedDate;
            //articleFeedback.aft__Article_Feed_Update__c = 
            //
            String link = System.URL.getSalesforceBaseUrl().getHost();
                link += '/articles/';
            	link += Schema.getGlobalDescribe().get(article.ArticleType).getDescribe().getLabel();
            	link += '/'+article.Title;
            articleFeedback.aft__Article_Link__c = link.replaceAll(' ', '-');
            articleFeedback.aft__Article_Number__c = article.ArticleNumber;
            articleFeedback.aft__Article_Title__c = article.Title;
            articleFeedback.aft__Article_Type__c = article.ArticleType;
            articleFeedback.aft__Article_Version__c = article.VersionNumber;
            //articleFeedback.aft__Assigned_To__c = 
            articleFeedback.aft__Community_Name__c = Site.getMasterLabel();
            
            articleFeedback.aft__Feedback__c = feedback;
            articleFeedback.aft__Feedback_Source__c = 'Communities';
            
            //default to new
            articleFeedback.aft__Feedback_Status__c = 'New';
            //articleFeedback.aft__Internal_Notes__c = 
            //articleFeedback.aft__Is_Current_User__c = 
            //articleFeedback.aft__Language__c = 
            articleFeedback.aft__Last_Published_By__c = article.LastModifiedById;
            articleFeedback.aft__Last_Published_Date__c = article.LastPublishedDate;
            //articleFeedback.aft__Parent_FeedItem__c = 
            
            System.debug(articleFeedback);
            
            try {
                insert articleFeedback;
            } catch (Exception e) {
                System.debug(e.getMessage());
            }
        }
        else {
            if (String.isBlank(feedback)) {
                System.debug(logginglevel.error, 'No feedback provided');
            }
            else {
            	System.debug(logginglevel.error, 'Article not found.');
            }
            return false;
        }
        return true;
    }
}