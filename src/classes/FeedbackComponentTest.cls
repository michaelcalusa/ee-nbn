/*
 *@Author:       Allanah Mae San Miguel
 *@Created Date: 2017-05-04
 *@Description:  Test class for feedback component
 */

@isTest
private class FeedbackComponentTest {
	@testSetup static void createTestData() {
        //KnowledgeArticleVersion

        //articles
        List<FAQ__kav> equipmentArticles = new List<FAQ__kav>();
        equipmentArticles.add(new FAQ__kav (Title='1 Unit Test', UrlName='12345'));
        equipmentArticles.add(new FAQ__kav (Title='2 Bluewolf Test', UrlName='123456'));
        equipmentArticles.add(new FAQ__kav (Title='3 Broadband', UrlName='123457'));

        insert equipmentArticles;

        equipmentArticles = [SELECT Id, KnowledgeArticleId FROM FAQ__kav WHERE PublishStatus = 'Draft' AND Language = 'en_US' ORDER BY Title];
        for (FAQ__kav article : equipmentArticles) {
            KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
        }
    }

    @isTest static void sendFeedbackTest() {
    	FAQ__kav article = [SELECT Id, Title, ArticleType, KnowledgeArticleId FROM FAQ__kav WHERE PublishStatus = 'Online' AND Language = 'en_US' LIMIT 1];

    	Test.startTest();
    		//String feedback, Id articleId
    		String feedback = 'Testing feedback component';
    		Boolean result = FeedbackComponent.sendFeedback(feedback, article.Id);
    		System.assert(result);

    		List<aft__Article_Feedback__c> articleFeedbacks = [SELECT Id, aft__Feedback__c, aft__Article_Title__c, aft__Article_Type__c FROM aft__Article_Feedback__c];
    		System.assert(!articleFeedbacks.isEmpty());

    		aft__Article_Feedback__c articleFeedback = articleFeedbacks[0];
    		System.assertEquals(feedback, articleFeedback.aft__Feedback__c);
    		System.assertEquals(article.Title, articleFeedback.aft__Article_Title__c);
    		System.assertEquals(article.ArticleType, articleFeedback.aft__Article_Type__c);

    		//test blank string
    		result = FeedbackComponent.sendFeedback(null, article.Id);
    		System.assert(!result);

    		//test invalid article
    		result = FeedbackComponent.sendFeedback(feedback, article.KnowledgeArticleId);
    		System.assert(!result);

    	Test.stopTest();
    }
}