/***************************************************************************************************
    Class Name          : FileUploadControllers
    Version             : 1.0 
    Created Date        : 13-Nov-2017
    Author              : Shuo Li
    Description         : Logic to call FileUpload 
    Input Parameters    : Master Record Id 
    Output Parameters   : List Files
****************************************************************************************************/ 
public with sharing class FileUploadController {
    @AuraEnabled
    public static List<ContentDocumentWrap> getListContentDocument(String myRecordId) {
        List<ContentDocumentWrap> ContentDocumentWraps  = new list<ContentDocumentWrap>();
        for (ContentDocumentLink cdl: [ SELECT ContentDocument.Id, ContentDocument.Title, ContentDocument.ParentId, ContentDocument.FileType, ContentDocument.ContentSize, ContentDocument.Description, ContentDocument.FileExtension 
                                   FROM ContentDocumentLink WHERE LinkedEntityId  = :myRecordId ]) {
              ContentDocumentWrap ContentDocumentWrap = new ContentDocumentWrap(false,
              																	cdl.ContentDocument.Id ,
              																	cdl.ContentDocument.Title,
              																	cdl.ContentDocument.FileType,
              																	cdl.ContentDocument.ContentSize,
              																	cdl.ContentDocument.Description,
              																	cdl.ContentDocument.FileExtension
              																	);
              ContentDocumentWraps.add(ContentDocumentWrap);
        }
        return ContentDocumentWraps;
    }
    
    @AuraEnabled
    public static List<ContentDocumentWrap> removeContentDocument(String contentDocumentId,String myRecordId) {
        List<Id> removeContentDocumentList  = new list<Id>();  
        removeContentDocumentList.add(contentDocumentId);
            
        if (removeContentDocumentList.size()>0) {
	        Database.DeleteResult[] deleteResults = Database.delete(removeContentDocumentList, true);
			for(Database.DeleteResult dr : deleteResults) {                   
			    if (!dr.isSuccess()) {
			        for(Database.Error err : dr.getErrors()) {
			            System.debug(LoggingLevel.Error, 'The following error has occurred.'
			                + '\n' + err.getStatusCode() + ': ' + err.getMessage()
			                + '\n fields that affected this error: ' + err.getFields());
			            // Plus further error handling as required
			        }
			    }
			}
        } 
        
        return getListContentDocument(myRecordId);
        
    }
     
    
    public class ContentDocumentWrap{
        @AuraEnabled
        public Boolean checkbox {get;set;}
        @AuraEnabled
        public Id ContentDocumentId {get;set;}  
          @AuraEnabled
        public String Title {get;set;}   
          @AuraEnabled
        public String FileType {get;set;}   
          @AuraEnabled
        public integer ContentSize {get;set;}   
          @AuraEnabled
        public String Description {get;set;}   
          @AuraEnabled
        public String FileExtension {get;set;}  
             
        public ContentDocumentWrap(Boolean checkbox, Id ContentDocumentId, String Title, String FileType, integer ContentSize, String Description, String FileExtension){
            this.checkbox = checkbox;
            this.ContentDocumentId = ContentDocumentId;    
            this.Title = Title;         
            this.FileType = FileType;         
            this.ContentSize = ContentSize;         
            this.Description = Description;         
            this.FileExtension = FileExtension;                
        }
    } 
}