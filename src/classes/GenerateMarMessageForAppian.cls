/***************************************************************************************************
    Class Name          : GenerateMarMessageForAppian
    Version             : 1.0
    Created Date        : 03-Jul-2018
    Author              : Arpit Narain
    Description         : Class to generate MAR message to send to Appian
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/

public class GenerateMarMessageForAppian {

    private Id id;
    private String marNumber;
    private Id primaryContactId { get; set; }
    private Id secondaryContactId { get; set; }
    private Site site { get; set; }
    private Alarm alarm { get; set; }
    private String providedDate;
    private String howDidYouHear;
    private String additionalInformation;

    public GenerateMarMessageForAppian(MAR__c mar) {

        Site site = new site(mar);
        Alarm alarm = new alarm(mar);

        this.id = mar.Id;
        this.marNumber = mar.Name;
        this.primaryContactId = mar.Contact__r.Id;
        this.secondaryContactId = mar.Alternate_Contact__r.Id;
        this.site = site;
        this.alarm = alarm;
        this.providedDate = String.valueOf(mar.CreatedDate);
        this.howDidYouHear = mar.How_did_you_hear__c;
        this.additionalInformation = mar.Additional_Information_Text__c;
    }

    /**
     * Site: Class to generate Site details
     *
     */
    public class Site {

        private String id { get; set;}
        private String locationId { get; set; }
        private String unitNumber { get; set; }
        private String unitType { get; set; }
        private String levelNumber { get; set; }
        private String levelType { get; set; }
        private String roadNumber1 { get; set; }
        private String roadNumber2 { get; set; }
        private String roadName { get; set; }
        private String roadSuffix { get; set; }
        private String roadType { get; set; }
        private String suburb { get; set; }
        private String state { get; set; }
        private String postcode { get; set; }
        private String siteAddress { get; set;}


        public site (MAR__c mar) {

            this.id = mar.Site__r.Id;
            this.locationId = mar.Site__r.Location_Id__c;
            this.unitNumber = mar.Site__r.Unit_Number__c;
            this.unitType = mar.Site__r.Unit_Type_Code__c;
            this.levelNumber = mar.Site__r.Level_Number__c;
            this.levelType = mar.Site__r.Level_Type_Code__c;
            this.roadNumber1 = mar.Site__r.Road_Number_1__c;
            this.roadNumber2 = mar.Site__r.Road_Number_2__c;
            this.roadName = mar.Site__r.Road_Name__c;
            this.roadType = mar.Site__r.Road_Suffix_Code__c;
            this.roadSuffix = mar.Site__r.Road_Type_Code__c;
            this.suburb = mar.Site__r.Locality_Name__c;
            this.state = mar.Site__r.State_Territory_Code__c;
            this.postcode = mar.Site__r.Post_Code__c;
            this.siteAddress = mar.Site__r.Site_Address__c;

        }
    }

    /**
    * Primary Contact Details: Class to generate primary contact details
    *
    */
    public class Alarm {

        private String brand { get; set;}
        private String brandOther { get; set;}
        private String whoDoesAlarmCall { get; set;}
        private String type { get; set;}

        public alarm (MAR__c mar) {

            this.brand = mar.Medical_Alarm_Brand__c;
            this.brandOther = mar.Medical_Alarm_Brand_Other__c;
            this.whoDoesAlarmCall = mar.Who_Does_Alarm_Call__c;
            this.type = mar.Medical_Alarm_Type__c;
        }
    }

}