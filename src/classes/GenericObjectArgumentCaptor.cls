/**
 * Created by alan on 2019-02-26.
 */

@isTest
public class GenericObjectArgumentCaptor extends ArgumentCaptor{

    public override Boolean isMatch(Object arg){
        return arg != null;
    }

}