public without sharing class GlobalCache {
/*------------------------------------------------------------
Author:        Dave Norris
Company:       Salesforce
Description:   A global utility class for caching data. Limits the SOQL and
               describe calls to reduce the chance of hitting governor limits
               Supports:
               1. Getting Record Types by Id
               2. Getting Profile Names by Id
Test Class:    GlobalCache_Test
History
<Date>      <Authors Name>     <Brief Description of Change> 
------------------------------------------------------------*/

    //maps to hold the record type info
    private static Map<String, Schema.SObjectType> gd;
    private static Map<String,Map<Id,Schema.RecordTypeInfo>> recordTypesById = new Map<String,Map<Id,Schema.RecordTypeInfo>>();
    private static Map<String,Map<String,Schema.RecordTypeInfo>> recordTypesByName = new Map<String,Map<String,Schema.RecordTypeInfo>>();
    //private static Map<String, String> profileMap = new Map<String,String>();

    private static void fillMapsForRecordTypeObject(string objectName) {
    /*------------------------------------------------------------
    Author:        Dave Norris
    Company:       Salesforce
    Description:   Function to fill record type map for objects not in cache
    Inputs:        objectName - The name of the sObject
    Returns:       Nothing
    History
    <Date>      <Authors Name>     <Brief Description of Change> 
    ------------------------------------------------------------*/
        // get the object map the first time
        if (gd==null) gd = Schema.getGlobalDescribe();

        // get the object description
        if (gd.containsKey(objectName)) {
            Schema.DescribeSObjectResult d = gd.get(objectName).getDescribe();
            recordTypesByName.put(objectName, d.getRecordTypeInfosByName());
            recordTypesById.put(objectName, d.getRecordTypeInfosById());
        }
    }

    public static Id getRecordTypeId(String objectName, String recordTypeName) {
    /*------------------------------------------------------------
    Author:        Dave Norris
    Company:       Salesforce
    Description:   Gives record type id from a given sObject and record type label
    Inputs:        objectName - The sObject
                   recordTypeName - The name of the record type (NOT the API Name)
    Returns:       The specified record types id value
    History
    <Date>      <Authors Name>     <Brief Description of Change> 
    ------------------------------------------------------------*/
        // make sure we have this object's record types mapped
        if (!recordTypesByName.containsKey(objectName)) 
            fillMapsForRecordTypeObject(objectName);

        // now grab and return the requested id 
        Map<String,Schema.RecordTypeInfo> rtMap = recordTypesByName.get(objectName);
        if (rtMap != null && rtMap.containsKey(recordTypeName)) {
            return rtMap.get(recordTypeName).getRecordTypeId();
        } else {
            return null;
        }
    }
    
    /*------------------------------------------------------------
    Author:        Beau Anderson
    Company:       Contractor
    Description:   Gives record type id from a given sObject and record type developername. If label is used you can't change the label for a recordtype without changing code
    Inputs:        objectName - The sObject
                   recordTypeName - The devlopername of the record type (NOT the label Name)
    Returns:       The specified record types id value
    History
    <Date>      <Authors Name>     <Brief Description of Change> 
    ------------------------------------------------------------*/

    // Create Cache
    private static Map<Schema.SObjectType,Map<String,Id>> rtypesCache;

    static{
        rtypesCache = new Map<Schema.SObjectType,Map<String,Id>>();
    }
 
    public static Map<String, Id> GetRecordTypeIdsByDeveloperName(Schema.SObjectType token){
        Map<String, Id> mapRecordTypes = rtypesCache.get(token);
            if(mapRecordTypes == null){
                mapRecordTypes = new Map<String, Id>();
            rtypesCache.put(token,mapRecordTypes);
        }else{
             return mapRecordTypes;
        }

        Schema.DescribeSObjectResult obj = token.getDescribe();
        String soql = 'SELECT Id, Name, DeveloperName '+ 'FROM RecordType ' + 'WHERE SObjectType = \'' + String.escapeSingleQuotes(obj.getName()) + '\' '+ 'AND IsActive = TRUE';
        List<SObject> results;
        try{
            results = Database.query(soql);
        }catch(Exception ex){
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'GlobalCache','GetRecordTypeIdsByDeveloperName','','','','',ex,0);
             results = new List<SObject>();
        }

        Map<Id,Schema.RecordTypeInfo> recordTypeInfos = obj.getRecordTypeInfosByID();

        for(SObject rt : results){  
            if(recordTypeInfos.get(rt.Id).isAvailable() || (UserInfo.getName() == 'Automated Process')){
                    System.debug('Available ---->'+recordTypeInfos.get(rt.Id).isAvailable());
                mapRecordTypes.put(String.valueOf(rt.get('DeveloperName')),rt.Id);
            }
        }
        System.debug('mapRecordTypes ---->'+mapRecordTypes);
        return mapRecordTypes;
    }
    
    /*------------------------------------------------------------
    Author:        Beau Anderson
    Company:       Contractor
    Description:   Gives record id for an account record which is common accross the Alternate Continuity solution. Aim is to cache the value to reduce queries
    Inputs:        Account name 

    Returns:       The record id for the account
    History
    <Date>      <Authors Name>     <Brief Description of Change> 
    ------------------------------------------------------------*/
    // Create Cache
    private static Map<String,Map<String,Id>> accCache;

    static{
        accCache = new Map<String,Map<String,Id>>();
    }
    
    
    public static Map<String,Id> GetAccountId(String accName){
        Map<String,Id> mapAccount = accCache.get(accName);
        Map<String,Id> accountRecordTypes = GetRecordTypeIdsByDeveloperName(Account.SObjectType);
        Id accountrecTypeID = Id.valueOf(accountRecordTypes.get('Company'));
        //List<Account> lstAcc = [select id,name from Account];
            if(mapAccount == null || mapAccount.isEmpty()){
                 //for(Account acc:lstAcc)
                 //{
                     //if(acc.name == accName){
                         mapAccount.put(accName,Id.valueOf('0015D000003E27K'));
                         accCache.put(accName,mapAccount);
                  //   }
                 //}
              return mapAccount;
            }
            else{
              return mapAccount;
            }
    }
    /*public static String getProfileName(String userProfileId) {
    
        if (!profileMap.containsKey(userProfileId)) {
            Profile profileName = [Select Id, Name from Profile where Id=:userProfileId];
            profileMap.put(userProfileId, profileName.Name);
        }
        return profileMap.get(userProfileId);
    }*/
    
}