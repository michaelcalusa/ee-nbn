/*------------------------------------------------------------------------
Author:        Dave Norris
Company:       Salesforce
Description:   A test class created to test GlobalCache methods
               Test executed:
               1 - Verify record type id lookup works
               2 - Verify record type name lookup works
               3 - Verify profile name lookup works

History
<Date>      <Authors Name>     <Brief Description of Change>
--------------------------------------------------------------------------*/
@isTest
private class GlobalCache_Test {

    static testMethod void lookupRecordTypeByName() {
    
        
        
    }
    
    static testMethod void lookupRecordTypeById() {
    
    }
    
    static testMethod void lookupProfileNameById() {
    
    }
    
}