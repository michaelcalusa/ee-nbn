/*------------------------------------------------------------  
Author:        Dave Norris
Company:       Salesforce
Description:   A global constants class for common variables
Test Class:    Doesn't need an associated test class
History
<Date>      <Authors Name>     <Brief Description of Change>
------------------------------------------------------------*/
public without sharing class GlobalConstants {
    public static final string DATE_TIME_FORMAT = 'yyyy-MM-dd\'T\'hh:mm:ss\'Z\'';
    //CONTACT RELATED
    //=========================================================================
    //Record Type Labels
    public static final String END_USER_RECTYPE_NAME = 'External Contact'; //record type label for the individual Contact records
    public static final String EXTERNAL_CONTACT_RECTYPE_NAME = 'External Contact'; //record type label for the individual Contact records
    public static final String COMPANY_CONTACT_RECTYPE_NAME = 'Company Contact'; //record type label for the Company Contact records
    //ACCOUNT RELATED
    //=========================================================================
    //Record Type Labels  
    public static final String HOUSEHOLD_ACCOUNT_RECTYPE_NAME = 'Household'; //record type for HouseHold Account records
    public static final String RSP_RECTYPE_NAME = 'Retail Service Provider'; //record type for RSP Account records
    public static final String COMPANY_RECTYPE_NAME = 'Company'; // record type for Company account records
    public static final String PROPERTY_DEVELOPER_RECTYPE_NAME = 'Property Developer'; // record type for Property Developer account records
    public static final String RESIDENTIAL_RECTYPE_NAME = 'Residential'; // record type for Residential account records
    //ACCOUNT ADDRESS RELATED(CONTRACT)
    public static final String TECHNOLOGY_CHOICE = 'Technology Choice'; //record type for tech choice
    public static final String ACC_ADDRESS_NEWDEV_CLASS_3_4 = 'NewDevs Class 3-4'; //record type for tech choice
    public static final String ACC_ADDRESS_NEWDEV_CLASS_1_2 = 'NewDevs Class 1-2'; //record type for tech choice
    // case Record Type
    public static final String FORMAL_COMPLAINT_RECTYPE_NAME = 'Formal Complaint'; //record type for Formal Complaint Record Type
    public static final String QUERY_RECTYPE_NAME = 'Query';
    //Opportunity Related
    public static final String NEW_DEVELOPMENTS = 'New Developments';
     public static final String NEW_DEV_LCO = 'New Development LCO';
    public static final String NEW_DEV_SCO = 'New Development SCO';
    // Development Related
    public static final String DEV_SMALL_RECTYPE_NAME = 'Small';
    public static final String DEV_MEDIUMLARGE_RECTYPE_NAME = 'Medium/Large';
    //TASK RELATED
    //=========================================================================
    public static final String TASK_INTERACTION_RECTYPE_NAME = 'Interaction'; //record type for Interaction Task records
    public static final String TASK_TYPE_CUSTOM_EU = 'EU';
    public static final String TASK_TYPE_CUSTOM_INTERNAL = 'Internal';
    public static final String TASK_TYPE_CUSTOM_EXTERNAL = 'External';
    public static final String TASK_SUB_TYPE_OUTBOUND = 'Outbound';
    public static final String TASK_CATEGORY_EMAIL = 'Email';
    public static final String TASK_PRIORITY_3_GENERAL = '3-General';
    public static final String TASK_COMPLETED_OUTCOME_COMPLETED = 'Completed';
    public static final String SENIOR_OPS_ENGAGEMENT_TASK_RECTYPE_NAME = 'Senior Operational Engagement';
    public static final String TASK_PRI_API_CONNECTION_FAILED = 'Network communitcation problem. Could not connect to NBNCo onpremise system';
    public static final String TASK_PRI_API_INCLUDE_VALUE = '?include=address';
    public static final String TASK_PRI_FORMAT_ERROR = 'PRI format needs to start with capital letters PRI and be followed by 12 digits from 0 to 9. Please check the PRI you have entered';
    public static final String TASK_PRI_NOT_FOUND_ERROR = 'WARNING! The PRI number has not been found on NBNCo systems for the copper technology type and associated NBNCo delivery partner. Please check the number,confirm the technology type is copper and the delivery partner is correct.';
    public static final String TASK_PRI_EMPTY_ERROR = 'Please enter a PRI before validating'; 
    public static final String TASK_REQUIRED_FIELD = 'You must enter a value, Please enter a PRI and validate';
    public static final String TASK_REQUIRED_FIELD_NOT_COMPLETED = 'Either Location ID,Address or CPI ID have not been retrieved. Please enter a valid PRI and validate';
    public static final String TASK_SAVE_AND_CLOSE_MESSAGE = 'Thank you for your service '+UserInfo.getFirstName()+' . We have recorded the following information. If needed please update the interaction.';
    public static final String TASK_BYPASSED_PORT_NUMBER = 'Bypassed port numbers can only be numeric';
    public static final String TASK_TELSTRA_TECH_NUMBER_FORMAT = 'Phone number must be format 04 followed by 8 numeric digits. Please check the number entered';
    public static final String TASK_DESIRED_PAIR_ID_FORMAT = 'Incorrect Desired Pair ID Format (Eg: P12:34 or P12:345)';
    public static final String TASK_COPPER_PATH_REQUIRED = 'A copper path is required please validate PRI first';
    public static final String TASK_SPECIAL_CHARACTER_CHECK = 'Telstra tech name cannot contain numbers or special charaters e.g %,&,$';
    public static final String TASK_SPECIAL_CHAR_CHECK = 'This field cannot contain special charaters e.g %,&,$';
    
    //APPLICATION LOG RELATED
    //=========================================================================
    //Record Type Labels
    public static final String ERROR_RECTYPE_NAME = 'Error';
    public static final String DEBUG_RECTYPE_NAME = 'Debug';
    public static final String INFO_RECTYPE_NAME = 'Info';
    public static final String WARNING_RECTYPE_NAME = 'Warning';
    
    /*******************************************************************************************/
    // "Authorisation for Contact" VALUE FROM SERVICE PORTAL TO SALESFORCE CASE
    public static final String ACCESS_SEEKER_FIRST  = 'RSP/ISP First';
    public static final String PRIMARY_FIRST  = 'Primary First';
    public static final String ACCESS_SEEKER_ONLY  = 'RSP/ISP Only';
    public static final String PRIMARY_ONLY  = 'Primary Only';
    public static final String END_USER_DIRECT  = 'End User Direct';
    public static final String SECONDARY_ONLY  = 'Secondary Only';
    public static final String BOTH_PARTIES  = 'Both Parties';
    public static final String ALL_PARTIES  = 'All Parties';
    /*******************************************************************************************/
    // Case Type from Web Form
    public static final String ENQUIRY  = 'Enquiry';
    public static final String COMPLAINT  = 'Complaint';
    public static final String DAMAGE_REPORT  = 'Damage Report';
    public static final String RSP_ISP_AS_PY_ROLE  = 'RSP/ISP/Access Seeker';
    public static final String END_USER_CONSUMER_ROLE  = 'End User Residential'; // ICRM-1280
    public static final String SERVICE_PORTAL_ORIGIN  = 'Portal';
    public static final String SERVICE_PORTAL_CASE_ORIGIN  = 'Service Portal';
    public static final String WEB_PUBLIC_WEBSITE_ORIGIN  = 'Web'; // ICRM-1418
    public static final String BUSINESS_CHANNEL_PUBLIC_WEBSITE  = 'Public Website';
    public static final String DATE_FORMAT  = 'dd/MM/yyyy';
    public static final String SYDNEY_TIME_ZONE  = 'Australia/Sydney';
    public static final String ONLINE_PREFERRED_CONTACT_METHOD_PHONE  = 'Phone';
    public static final String PREFERRED_CONTACT_METHOD_PHONE  = 'Preferred Phone';
    //SITE OBJECT RELATED CONSTANTS
    //=========================================================================
    public static final String SITE_VERIFIED = 'Verified';
    public static final String SITE_UNVERIFIED = 'Unverified';
    public static final String SITE_MDUCP = 'MDU/CP';//https://jira-cc.slb.nbndc.local/browse/FY17-306 
    
    //CIS INTEGRATION RELATED CONSTANTS
    //=========================================================================
     public static final String CIS_CONNECTEDTO= '?include=connectedTo';
     
    //DISPATCH TECHNICIAN RELATED CONSTANTS
    //=========================================================================
     public static final String CURRENT_STATUS_UPDATING= 'Waiting for dispatch technician to complete';
}