/****************************************************************************************************************************
Class Name:         GlobalFormAPI_UtilityClass.
Class Type:         Apex Web service utility class. 
Version:            1.0 
Created Date:       8th August,2018
Function:           This class will set up the response codes.
Description:        This class will set up the response codes.
Modification Log:
* Developer             Date             Description
* --------------------------------------------------------------------------------------------------                 
* Sridevi      08/08/2018      Created - Version 1.0 Refer Form API epic description.- MSEU 13650
*****************************************************************************************************************************/
public class GlobalFormAPI_UtilityClass {
    public static Integer STATUS_OK = 200;
    public static Integer STATUS_BAD = 400;
    public static Integer STATUS_NOTFOUND = 404;
    public static Integer STATUS_DMLERROR = 405;  
  
    public static RestResponse displayErrorCode(RestResponse res,Global_Form_Staging_WebService.GlobalFormStagingDetail Detail){  
       boolean greenflag = true;
        
       String emptyFields = ' ';
       if(String.IsBlank(Detail.FormData)){
           emptyFields = emptyFields  + 'FormData,';
       }
       if(String.IsBlank(Detail.FormStatus)){
           emptyFields = emptyFields  + 'FormStatus,';
       }
       if(String.IsBlank(Detail.FormType)){
           emptyFields = emptyFields  + 'FormType,';
       }
       if(String.IsBlank(Detail.FormContentType)){
           emptyFields = emptyFields  + 'FormContentType,';
       }
       
       res.statusCode = STATUS_BAD;
       res.addHeader('Content-Type', 'application/json');
       FailureResponseBase CustomResponse = new FailureResponseBase(false,'Following attributes are empty:' + emptyFields.removeEnd(','));
       res.responseBody = blob.valueOf(JSON.serializePretty(CustomResponse,true));  
       GlobalFormEventTriggerHandlerUtil.logMessage('Error','FormAPI_WebService','Following attributes are empty: ' + emptyFields.removeEnd(',') + ' doPost() method','','','','',null,0,'');
       return(res);
        
    }
    public static RestResponse getResponseCode(RestResponse res,Global_Form_Staging__c GlobalRec){
        res.statusCode = STATUS_OK;
        res.addHeader('Content-Type', 'application/json');
        SuccessResponseBase CustomResponse = new SuccessResponseBase(true,'Form staging record insert successful!',GlobalRec.Id,GlobalRec.Name); 
        res.responseBody = blob.valueOf(JSON.serializePretty(CustomResponse,true)); 
        return(res);
    }
    public static RestResponse displayDMLError(RestResponse res){     
        res.statusCode = STATUS_DMLERROR;
        res.addHeader('Content-Type', 'application/json');        
        FailureResponseBase CustomResponse = new FailureResponseBase(false,'Error during DML insert'); 
        res.responseBody = blob.valueOf(JSON.serializePretty(CustomResponse,true)); 
        GlobalFormEventTriggerHandlerUtil.logMessage('Error','FormAPI_WebService DML Insert failed','DML insert failed','','','','',null,0,'');
        return(res);
    }
    public static RestResponse displayStatusNotFoundError(RestResponse res, Exception e){
        res.statusCode = STATUS_NOTFOUND;
        res.addHeader('Content-Type', 'application/json');
        if(e!= null && e.getMessage().contains('INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST')){
            res.responseBody = blob.valueOf('Invalid Form Status value.'); 
        }else if(e!= null && (e.getMessage().contains('INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST')) == false){
            res.responseBody = blob.valueOf('Problem with APEX REST Webservice: '+e.getMessage()); 
        }else{
            res.responseBody = blob.valueOf('Problem with APEX REST Webservice: Status not found'); 
        }
        GlobalFormEventTriggerHandlerUtil.logMessage('Error','Problem with APEX REST Webservice','StatusNotFoundError','','','','',null,0,'');
        return(res);
    }
    public static RestResponse displayInvalidFormStatusError(RestResponse res){     
        res.statusCode = STATUS_BAD;
        res.addHeader('Content-Type', 'application/json');        
        FailureResponseBase CustomResponse = new FailureResponseBase(false,'Invalid Form Status value'); 
        res.responseBody = blob.valueOf(JSON.serializePretty(CustomResponse,true)); 
        GlobalFormEventTriggerHandlerUtil.logMessage('Error','FormAPI_WebService DML Insert failed','Invalid form status','','','','',null,0,'');
        return(res);
    }
    
    public static RestResponse AllFieldsLengthCheck(RestResponse res,Global_Form_Staging_WebService.GlobalFormStagingDetail Detail){ 
        String exceedFields = ' ';
        
        if(Detail.FormData.length() > 131072) {
            exceedFields = exceedFields + 'FormData,';            
        }   
        
        if(Detail.FormStatus.length() > 100) {
            exceedFields = exceedFields + 'FormStatus,';
        } 
        
        if(Detail.FormType.length() > 255) {
            exceedFields = exceedFields + 'FormType,';            
        }               
        
        if(Detail.FormContentType.length() > 255) {
            exceedFields = exceedFields + 'FormContentType,';            
        }        
        
       res.statusCode = STATUS_BAD;
       res.addHeader('Content-Type', 'application/json');
       FailureResponseBase CustomResponse = new FailureResponseBase(false,'Following attributes are exceeding the length limits:' + exceedFields.removeEnd(','));
       res.responseBody = blob.valueOf(JSON.serializePretty(CustomResponse,true));  
       GlobalFormEventTriggerHandlerUtil.logMessage('Error','FormAPI_WebService','Following attributes are exceeding the length limits:: ' + exceedFields.removeEnd(',') + 'doPost() method','','','','',null,0,'');
       return(res);
        
    }

    public class SuccessResponseBase {
        public Boolean Status {get;set;}
        public String Message {get;set;}
        public String GlobalFormId {get;set;}
        public String GlobalFormName {get;set;}         
        public SuccessResponseBase(Boolean Status, String Message, String GlobalFormId, String GlobalFormName) {
            this.Status = Status;
            this.Message = Message;
            this.GlobalFormId = GlobalFormId;
            this.GlobalFormName = GlobalFormName;
        }  
    } 
    public class FailureResponseBase {
        public Boolean Status {get;set;}
        public String Message {get;set;}       
        public FailureResponseBase(Boolean Status, String Message) {
            this.Status = Status;
            this.Message = Message;
        }  
    }     
}