/****************************************************************************************************************************
Class Name:         GlobalFormEventTriggerHandler.
Class Type:         Global form platform event trigger handler. 
Version:            1.0 
Created Date:       3rd August,2018.
Function:           
Description:        This class will take the input from Web Integration and insert the records to Global_Form_Staging__c object.
Modification Log:
* Developer             Date             Description
* --------------------------------------------------------------------------------------------------                 
* Wayne Ni     03/08/2018      			Created this Apex class
*****************************************************************************************************************************/

public class GlobalFormEventTriggerHandler {
    //Class variables
    public static final string Pre_Qua_Resource_Type = 'pre-qualification questionaire';
    public id LeadAccessseekerRecordTypeId = GlobalCache.getRecordTypeId('Lead','Access Seeker');
    public static boolean InsertLeadFlag = true;
    
    //Class Constructor
    private List<Global_Form_Event__e> trgNewList = new List<Global_Form_Event__e> ();
    public GlobalFormEventTriggerHandler(List<Global_Form_Event__e> trgNewList){    
        this.trgNewList = trgNewList;      
    }
    
    
    //Class methods
    public void OnAfterInsert(){
 		WebToLeadProcessing(trgNewList);        
    }
    
    Public static Id getGlobalFormRecordId(string CallbackURL){        
        String recordIdString = CallbackURL.substringAfterLast('/'); 
        Id recordid = Id.valueOf(recordIdString);
        system.debug('record id is '+recordId);
        
        return recordId; 
    }
    
    public static void WebToLeadProcessing(List<Global_Form_Event__e> trgNewList){
        //Get all the realted Global form staging record id
        set<Id> GlobalStagingFormRecordIdSet = new set<Id>();
        system.debug('trigger new list is '+trgNewList);
        
        for(Global_Form_Event__e singleGlobalFormEvent : trgNewList){
            if(singleGlobalFormEvent.ResourceType__c.tolowercase() == Pre_Qua_Resource_Type){
                if(singleGlobalFormEvent.Source__c == GlobalFormStagingTriggerHandler.EVENT_SOURCE && getGlobalFormRecordId(singleGlobalFormEvent.URL__c) != null
                  && singleGlobalFormEvent.Event_Type__c == 'Completed'){
                    system.debug('*** the passed global form event for web to lead is '+singleGlobalFormEvent);
                    system.debug('*** the passed global form event for web to lead event type is '+singleGlobalFormEvent.Event_Type__c);
                    system.debug('*** the passed global form event for web to lead  resource type is '+singleGlobalFormEvent.ResourceType__c);
                    system.debug('*** the passed global form event for web to lead  source type is '+singleGlobalFormEvent.Source__c);
                    system.debug('*** the passed global form event for web to lead  call back url is '+singleGlobalFormEvent.URL__c);
                    GlobalStagingFormRecordIdSet.add(getGlobalFormRecordId(singleGlobalFormEvent.URL__c));            
                    
                    //for future development
                    /*
                    if(singleGlobalFormEvent.Event_Type__c == 'In Progress'){
                    
                    }else if(singleGlobalFormEvent.Event_Type__c == 'Completed'){
                    
                    }else if(singleGlobalFormEvent.Event_Type__c == 'Cancelled'){
                    
                    }*/
                }
            }else{
                //what to do now?
            }
        }
        
        List<Global_Form_Staging__c> TargetGlobalFormStagingRecords = [Select Name, Id, Status__c , Data__c , Type__c , Content_Type__c from Global_Form_Staging__c where id in: GlobalStagingFormRecordIdSet];
        
        if(TargetGlobalFormStagingRecords!= null && TargetGlobalFormStagingRecords.size() > 0){
            
            for(Global_Form_Staging__c stagingRecord: TargetGlobalFormStagingRecords){
                system.debug('the query Global form staging record is '+ stagingRecord);
                system.debug('the query Global form staging record Name is '+ stagingRecord.Name);
                system.debug('the query Global form staging record Id is '+ stagingRecord.Id);
                system.debug('the query Global form staging record status is '+ stagingRecord.Status__c);
                system.debug('the query Global form staging record Data is '+ stagingRecord.Data__c);
                system.debug('the query Global form staging record type is '+ stagingRecord.Type__c);
                system.debug('the query Global form staging record content type is '+ stagingRecord.Content_Type__c);
            }
            
            //Web-to-Lead form processing, to be continued
            //WebToLeadProcessingTriggerHandlerUtil.WebToLeadProcessing(TargetGlobalFormStagingRecords);      
            
            //Building Registeration form processing, to be continued, comment it out for the time being
            //BuildingRegisterationTriggerHandlerUtil.BuildingRegistrationFormProcessing(TargetGlobalFormStagingRecords);            
        }
        
        
        
    }
    
}