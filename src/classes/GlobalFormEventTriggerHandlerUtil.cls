/*------------------------------------------------------------  
Author:        Wayne Ni
Description:   A global utility class for Form API Trigger Handler
1 - Application Logging
2 - Get current record id from url
Test Class:    --   --
History
<Date>      <Authors Name>     <Brief Description of Change>
------------------------------------------------------------*/

public class GlobalFormEventTriggerHandlerUtil {

    
    Public static void retryPlatformEventAndLogError(string errorMsg, string methodName, Exception e, string referenceInfo){
        
        //logMessage('Error',errorMsg,MethodName,'','','','',e,0,'Pre-qualification questionaire');
        
        //Retry the Platform event for the exception 
        if(EventBus.TriggerContext.currentContext().retries < 2){    
            system.debug('@@Global Form event retries@@'+EventBus.TriggerContext.currentContext().retries);
            throw new EventBus.RetryableException('Condition is not met, so retrying the Global Form event trigger.'); 
        }else{
            //throw new Exception('Throw exception when insert the leads after multi 2 tries');   
            logMessage('Error','Global Form Event trigger handler failed after 2 retries',MethodName,'',referenceInfo,'','', new AsyncException('Exception when inserting record'),0,'Global Form Event Trigger');  
        }
        
    }   
    
           
    Public static Id getGlobalFormRecordId(string CallbackURL){        
        String recordIdString = CallbackURL.substringAfterLast('/'); 
        Id recordid = Id.valueOf(recordIdString);
        system.debug('record id is '+recordId);
        
        return recordId; 
    }
    
        
    public static void logMessage(String logLevel, String sourceClass, String sourceFunction, String referenceId, String referenceInfo, String logMessage, String payLoad, Exception ex, long timeTaken,  String fromType) {        
        GlobalFormErrorLogWrapper msg = new GlobalFormErrorLogWrapper();
        
        msg.source = sourceClass;
        msg.logMessage = logMessage;
        msg.sourceFunction = sourceFunction;
        msg.referenceId = referenceId;
        msg.referenceInfo = referenceInfo;
        msg.payload = payLoad;
        msg.debugLevel = logLevel;
        msg.ex = ex;
        msg.Timer = timeTaken;
        msg.FormType = fromType;
              
        logMessage( msg );
        
    }
    
    
    public static void logMessage(GlobalFormErrorLogWrapper appLog){        
        List<GlobalFormErrorLogWrapper> appLogs = new List<GlobalFormErrorLogWrapper>();       
        appLogs.add ( appLog );        
        logMessage ( appLogs );       
    }
    
    
    public static void logMessage(List<GlobalFormErrorLogWrapper> appLogs){
        Set<Id> InsertedErrorLogIds = new set<Id>();
        List<Global_Form_Error_Log__c> insertAppLogs = new List<Global_Form_Error_Log__c >();
        
        System.Debug('@@@AppMsg 3');
        
        for(GlobalFormErrorLogWrapper appLog : appLogs){
            
            Global_Form_Error_Log__c  log = new Global_Form_Error_Log__c ();
            
            log.Source__c = appLog.source;
            log.Source_Function__c = appLog.sourceFunction;
            log.Reference_Id__c = appLog.referenceId;
            log.Reference_Information__c = appLog.referenceInfo;
            log.Message__c = appLog.logMessage;
            log.Integration_Payload__c = appLog.payload;
            log.FormType__c = appLog.FormType;
            
            if(appLog.ex != null){
                log.Stack_Trace__c = appLog.ex.getStackTraceString();
                log.Message__c = applog.ex.getMessage();
            }    
            
            log.Code__c = appLog.logCode;
            log.Timer__c = appLog.timer;

            insertAppLogs.add(log);
            system.debug('inserted app log is '+insertAppLogs.size());
            System.Debug( 'Inserted Global Form Error Log from ' + log.source__c + ' debug level: ' + appLog.debugLevel );
            
        }
        
        system.debug('inserted app log is '+insertAppLogs.size());
        if ( insertAppLogs.size() > 0 ){                       
            List<Database.SaveResult> insertRecord = database.insert(insertAppLogs);             
            for(Database.SaveResult singleLog : insertRecord){
                if(singleLog.isSuccess()){
                    InsertedErrorLogIds.add(singleLog.getId());
                }else if(singleLog.isSuccess() == false){
                    system.debug(singleLog.getErrors());
                }
            }                
            system.debug('after insert of debug log');
            system.debug('InsertedErrorLogIds is '+InsertedErrorLogIds);
        }
        
        //Send email to production support team for further investigation
        sendLogEmailToProduction(InsertedErrorLogIds);
    }
    
    public static void sendLogEmailToProduction(Set<Id> GlobalFormErrorLogIds){
        //get the target staging error record id:
        system.debug('GlobalFormErrorLogIds is '+GlobalFormErrorLogIds);
        OrgWideEmailAddress[] NoReplyNBNOwea = [select Id,Address,DisplayName from OrgWideEmailAddress where DisplayName = 'NoReplyNBN'];
        List<Global_Form_Error_Log__c> insertedErrorLogs = [select id ,Source__c,Source_Function__c,Reference_Id__c,
                                                            Reference_Information__c,Integration_Payload__c,FormType__c,Stack_Trace__c,
                                                            Message__c,Code__c,Timer__c From Global_Form_Error_Log__c 
                                                            where id in: GlobalFormErrorLogIds];
        system.debug('insertedErrorLogs is '+insertedErrorLogs);
		List<Messaging.Email> FinalSendEmails = new List<Messaging.Email>();
        Id emailTemplateId = [select id, name from EmailTemplate where developername = 'Global_Form_Error_Log_Email_Template_To_Support_Team'].id;
        system.debug('emailTemplateId is '+emailTemplateId);
               
        Messaging.reserveSingleEmailCapacity(insertedErrorLogs.size());
        
        system.debug('inserted error logs is '+insertedErrorLogs);
        if(insertedErrorLogs != null && insertedErrorLogs.size() > 0){
            for(Global_Form_Error_Log__c singleErrorLog:insertedErrorLogs){
                if(string.isNotBlank(singleErrorLog.Reference_Information__c) && (singleErrorLog.FormType__c == 'pre-qualification questionnaire' || singleErrorLog.FormType__c == 'Single Building Registration') ){
                    //constructing the email
                    system.debug('send email method returns '+SendEmail(singleErrorLog,emailTemplateId,NoReplyNBNOwea));
    				FinalSendEmails.add(SendEmail(singleErrorLog,emailTemplateId,NoReplyNBNOwea));
                }
                //for future development
                /*
                else if(string.isNotBlank(singleErrorLog.Reference_Information__c) && singleErrorLog.FormType__c.contains('Building registration form')){
                    
                }*/
            }          
        }
        
        system.debug('before final send');
        if(FinalSendEmails != null && FinalSendEmails.size()> 0 && !(test.isRunningTest())){
            system.debug('FinalSendEmails are '+FinalSendEmails);
            Messaging.sendEmail(FinalSendEmails);
        }
        
    }
    
    public static Messaging.SingleEmailMessage SendEmail(Global_Form_Error_Log__c singleErrorLog,string emailTemplateId,List<OrgWideEmailAddress> orgWideEmail){
        List<String> Emails = new List<String>();
        Map<string,Global_Form_Support_Teams__c> resultMap = Global_Form_Support_Teams__c.getAll(); 
        
        //system.debug('Send email methods form type is '+singleErrorLog.FormType__c);
        Global_Form_Support_Teams__c mc = Global_Form_Support_Teams__c.getValues(singleErrorLog.FormType__c);
        system.debug('mc is '+mc);
        if(mc != null){
            Emails =  mc.Support_Team_Emails__c.split(';');                          
        }
        
        //system.debug('Emails are '+Emails);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(Emails);
        
        if(orgWideEmail != null){
            mail.setOrgWideEmailAddressId(orgWideEmail[0].id);
        }
        
        system.debug('before rendering result from email template');        
        Messaging.SingleEmailMessage result = Messaging.renderStoredEmailTemplate(emailTemplateId,null,singleErrorLog.id);
        system.debug('After rendering result from email template');
                
        mail.setSubject(result.getSubject());                        
        mail.setHtmlBody(result.getHtmlBody());
        mail.setPlainTextBody(result.getPlainTextBody());
        //Messaging.sendEmail(new Messaging.Email[]{mail});
        //system.debug('sending email successfully');
        return mail;
    }
    
   
    public class GlobalFormErrorLogWrapper {        
        public string source {get;set;}
        public string sourceFunction {get;set;}
        public string referenceId {get;set;}
        public string referenceInfo{get;set;}
        public string logMessage {get;set;}
        public string payload {get;set;}
        public Exception ex {get;set;}
        public string debugLevel {get;set;}
        public string logCode {get;set;}
        public string FormType {get;set;}
        public long timer {get;set;}                
    }
 
    
}