@IsTest
public class GlobalFormEventTriggerHandlerUtil_Test {
    
    @testSetup static void createSetup(){
        List<Global_Form_Staging__c> gfslist = new List<Global_Form_Staging__c>();
        Global_Form_Staging__c gfs1 = new Global_Form_Staging__c();
        gfs1.Content_Type__c = 'application/json';
        gfs1.Data__c = '{"FirstName":"Test","MiddleName":"User","LastName":"LI","Phone":"0323232323","Email":"test@test.com","ABN":"86136533741","Company":"NBN CO LIMITED","TradingName":"test trading","Website":"nbnco.com.au","LeadRole":"Technical","leadsource":"NBN Website","recordTypeId":"01228000000ycCL","Business_ID":"","Distributor_ID":"","Vendor_ID":"","sf_skip_reference_number_check":true,"Partner_Record":true,"T_Cs_Accepted":true}';
        gfs1.Status__c = 'Completed';
        gfs1.Type__c = 'Pre-qualification questionnaire'; 
        gfslist.add(gfs1);
        
        Global_Form_Staging__c gfs2 = new Global_Form_Staging__c();
        gfs2.Content_Type__c = 'application/json';
        gfs2.Data__c = '{"FirstName":"Test","MiddleName":"User2","LastName":"Testing","Phone":"0323232323","Email":"test@t.com","ABN":"86136533741","Company":"NBN CO LIMITED","Website":"nbnco.com.au","LeadRole":"Technical","leadsource":"NBN Website","recordTypeId":"01228000000ycCL","Business_ID":"","Distributor_ID":"","Vendor_ID":"","sf_skip_reference_number_check":true,"Partner_Record":true,"T_Cs_Accepted":true}';
        gfs2.Status__c = 'Completed';
        gfs2.Type__c = 'Pre-qualification questionnaire'; 
        gfslist.add(gfs2);
        
        insert gfslist;
    }     
    
    @isTest static void getGlobalFormRecordId_Test(){
        List<Global_Form_Staging__c> testrecords = [select id from Global_Form_Staging__c];
        string url = 'www.my.salesforce.com/'+testrecords[0].id;
 		Id result = GlobalFormEventTriggerHandlerUtil.getGlobalFormRecordId(url);
        system.assertEquals(result, testrecords[0].id);
    }


    @isTest static void logMessage_Test(){          
        GlobalFormEventTriggerHandlerUtil.logMessage('Error','GlobalFormTriggerHandler','Execute','','','','',null,0,'Pre-Qualification Questionnaire');
    	List<Global_Form_Error_Log__c> testErrorLogRecords = [select id from Global_Form_Error_Log__c];
        system.assertNotEquals(testErrorLogRecords, null);     
    }
     
}