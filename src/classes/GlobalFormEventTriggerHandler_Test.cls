@isTest
public class GlobalFormEventTriggerHandler_Test {
    Public static string JsonPayload = '{"FirstName":"JayAsdfsdTest","MiddleName":"sddgdgd","LastName":"LI","Phone":"0323232323","Email":"jayli@nbnco.com.au","ABN":"86136533741","Company":"NBN CO LIMITED","TradingName":"test trading","Website":"nbnco.com.au","LeadRole":"Technical","leadsource":"NBN Website","recordTypeId":"01228000000ycCL","Business_ID":"","Distributor_ID":"","Vendor_ID":"","sf_skip_reference_number_check":true,"Partner_Record":true,"T_Cs_Accepted":true}';
    Public static string JsonPayloadTwo = '{"FirstName":"JayAsdfsdTest","MiddleName":"sddgdgd","LastName":"","Phone":"0323232323","Email":"jayli@nbnco.com.au","ABN":"86136533741","Company":"NBN CO LIMITED","TradingName":"test trading","Website":"nbnco.com.au","LeadRole":"Technical","leadsource":"NBN Invalid picklist value test","recordTypeId":"01228000000ycCL","Business_ID":"","Distributor_ID":"","Vendor_ID":"","sf_skip_reference_number_check":true,"Partner_Record":true,"T_Cs_Accepted":true}';
       
    @testSetup static void setupMethod() {
        //create test records of Global form staging 
        List<Global_Form_Staging__c>  testRecordsList = new List<Global_Form_Staging__c>();  
        
        Global_Form_Staging__c GlobalStagingTestRecordWebToLead = new Global_Form_Staging__c();
        GlobalStagingTestRecordWebToLead.Type__c = 'pre-qualification questionaire';
        GlobalStagingTestRecordWebToLead.Status__c = 'Completed';
        GlobalStagingTestRecordWebToLead.Content_Type__c = 'JSON';
        GlobalStagingTestRecordWebToLead.Data__c = JsonPayload;
        GlobalStagingTestRecordWebToLead.Description__c  = 'TestRecordOne';
        testRecordsList.add(GlobalStagingTestRecordWebToLead);
        //system.debug(GlobalStagingTestRecordWebToLead.Id);
        
        Global_Form_Staging__c GlobalStagingTestRecordWebToLeadTwo = new Global_Form_Staging__c();
        GlobalStagingTestRecordWebToLeadTwo.Type__c = 'pre-qualification questionaire';
        GlobalStagingTestRecordWebToLeadTwo.Status__c = 'Completed';
        GlobalStagingTestRecordWebToLeadTwo.Content_Type__c = 'JSON';
        GlobalStagingTestRecordWebToLeadTwo.Data__c = JsonPayloadTwo;
        GlobalStagingTestRecordWebToLeadTwo.Description__c  = 'TestRecordTwo';
        testRecordsList.add(GlobalStagingTestRecordWebToLeadTwo);
        //system.debug(GlobalStagingTestRecordWebToLead.Id);
        //
        Global_Form_Staging__c GlobalStagingTestRecordWebToLeadThree = new Global_Form_Staging__c();
        GlobalStagingTestRecordWebToLeadThree.Type__c = 'pre-qualification questionaire';
        GlobalStagingTestRecordWebToLeadThree.Status__c = 'In progress';
        GlobalStagingTestRecordWebToLeadThree.Content_Type__c = 'JSON';
        GlobalStagingTestRecordWebToLeadThree.Data__c = JsonPayloadTwo;
        GlobalStagingTestRecordWebToLeadThree.Description__c  = 'TestRecordhree';
        testRecordsList.add(GlobalStagingTestRecordWebToLeadThree);
        //system.debug(GlobalStagingTestRecordWebToLead.Id);
        
        insert testRecordsList;
		
    }
    
    @istest static void testMethodOne() {        
        Global_Form_Staging__c testRecord = [select id,Name,Type__c, Status__c, Content_Type__c, Data__c, Description__c from Global_Form_Staging__c where Description__c = 'TestRecordOne' limit 1];
        system.debug('test record id is '+testRecord.Id);
        
        Global_Form_Event__e GlobalFormPlatformEvent = new Global_Form_Event__e();
        GlobalFormPlatformEvent.ResourceType__c  = testRecord.Type__c;
        GlobalFormPlatformEvent.URL__c = 'https://nbn.my.salesforce.com/services/data/v41.0/sobjects/Web_Form__c/'+testRecord.Id;
        GlobalFormPlatformEvent.Event_Type__c = testRecord.Status__c;
        GlobalFormPlatformEvent.Source__c = 'Salesforce.GlobalFormAPI';  
        
        Test.startTest();        
        List<Database.SaveResult> results = EventBus.publish(new List<Global_Form_Event__e>{GlobalFormPlatformEvent});
        Test.stopTest();
    }

    
    @istest static void testMethodTwo() {      
        Global_Form_Staging__c testRecord = [select id,Name,Type__c, Status__c, Content_Type__c, Data__c, Description__c from Global_Form_Staging__c where Description__c = 'TestRecordTwo' limit 1];
        system.debug('test record id is '+testRecord.Id);
        
        Global_Form_Event__e GlobalFormPlatformEvent = new Global_Form_Event__e();
        //GlobalFormPlatformEvent.GlobalFormName__c = testRecord.Name;
        //GlobalFormPlatformEvent.GlobalFormRecordId__c = testRecord.Id;
        GlobalFormPlatformEvent.ResourceType__c  = testRecord.Type__c;
        GlobalFormPlatformEvent.URL__c = 'https://nbn.my.salesforce.com/services/data/v41.0/sobjects/Web_Form__c/'+testRecord.Id;
        GlobalFormPlatformEvent.Event_Type__c = testRecord.Status__c;
        GlobalFormPlatformEvent.Source__c = 'Salesforce.GlobalFormAPI';  
                
        Test.startTest();     
        List<Database.SaveResult> results = EventBus.publish(new List<Global_Form_Event__e>{GlobalFormPlatformEvent});
        Test.stopTest();
        
    }
    
    @istest static void testMethodThree() {      
        Global_Form_Staging__c testRecord = [select id,Name,Type__c, Status__c, Content_Type__c, Data__c, Description__c from Global_Form_Staging__c where Description__c = 'TestRecordhree' limit 1];
        system.debug('test record id is '+testRecord.Id);
        
        Global_Form_Event__e GlobalFormPlatformEvent = new Global_Form_Event__e();
        GlobalFormPlatformEvent.ResourceType__c  = testRecord.Type__c;
        GlobalFormPlatformEvent.URL__c = 'https://nbn.my.salesforce.com/services/data/v41.0/sobjects/Web_Form__c/'+testRecord.Id;
        GlobalFormPlatformEvent.Event_Type__c = testRecord.Status__c;
        GlobalFormPlatformEvent.Source__c = 'Salesforce.GlobalFormAPI';  
                
        Test.startTest();     
        List<Database.SaveResult> results = EventBus.publish(new List<Global_Form_Event__e>{GlobalFormPlatformEvent});
        //Id returnId = GlobalFormEventTriggerHandler.getGlobalFormRecordId(GlobalFormPlatformEvent.URL__c);
        Test.stopTest();
        
        
    }
}