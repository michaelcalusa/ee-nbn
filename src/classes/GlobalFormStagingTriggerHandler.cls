/*------------------------------------------------------------------------
Description:   A class created to manage trigger actions from the Global From Staging object 
               Responsible for:
               1 - Publishing the Global From Staging Event JIRA: MSEU 13650 
Test Class:    GlobalFormStaging_Test (GlobalFormStagingTriggerHandler_Test?)
				
Created   :    03/08/2018    
History
<Date>            <Authors Name>    <Brief Description of Change> 
--------------------------------------------------------------------------*/

public class GlobalFormStagingTriggerHandler {
    
    public static final string PROCESS_STATUS_NEW = 'New';
    public static final string EVENT_TYPE = 'Global Web Form';
    public static final string ICT_RESOURCE_TYPE = 'ICT Lead';    
    public static final string EVENT_SOURCE = 'Salesforce.GlobalFormAPI'; 
    
    private List<Global_Form_Staging__c> trgOldList = new List<Global_Form_Staging__c> ();
    private List<Global_Form_Staging__c> trgNewList = new List<Global_Form_Staging__c> ();
    private Map<id,Global_Form_Staging__c> trgOldMap = new Map<id,Global_Form_Staging__c> ();
    private Map<id,Global_Form_Staging__c> trgNewMap = new Map<id,Global_Form_Staging__c> (); 
    
    
    public GlobalFormStagingTriggerHandler(List<Global_Form_Staging__c> trgOldList, List<Global_Form_Staging__c> trgNewList, Map<id,Global_Form_Staging__c> trgOldMap, Map<id,Global_Form_Staging__c> trgNewMap){
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
    }
    
    public void OnAfterInsert(){
        if(!HelperUtility.isTriggerMethodExecutionDisabled('PublishGlobalFormEvent')){
            system.debug('OnAfterInsert - trgOldMap....'+trgNewMap); 
            PublishGlobalFormEvent(trgNewList, new Map<id,Global_Form_Staging__c>());
        }
    }
    
    /*
    Function to public event for new status of Global From Staging records
    */
    public void PublishGlobalFormEvent(List<Global_Form_Staging__c> trgNewList,Map<id,Global_Form_Staging__c> trgOldMap){  
        list<Global_Form_Staging__c>EventList = new list<Global_Form_Staging__c>();
        if (trgOldMap.isEmpty()) {
            EventList.addAll(trgNewList);
        } 
        //System.debug(EventList);
        
        //Publish Event         
        List<Global_Form_Event__e> GlobalFormEvents = new List<Global_Form_Event__e>();
        for (Global_Form_Staging__c gfs: EventList) {
            Global_Form_Event__e gfevent = new Global_Form_Event__e();   
            gfevent.URL__c = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+gfs.Id; 
            gfevent.ResourceType__c = gfs.Type__c;          
            gfevent.Event_Type__c = gfs.Status__c;
            gfevent.Source__c = EVENT_SOURCE;
            gfevent.SalesforceRecordId__c = gfs.Id;
            GlobalFormEvents.add(gfevent);           
        }
        
        System.debug('Event publish Size....'+GlobalFormEvents.size());     
        // Call method to publish events
        // 
        List<Database.SaveResult> results = EventBus.publish(GlobalFormEvents);
        
        // Inspect publishing result for each event
        for (Database.SaveResult sr : results) {
            system.debug('the save result is '+sr.isSuccess());
            if (sr.isSuccess()) {
                System.debug('Successfully published event.');
            } else {
                for(Database.Error err : sr.getErrors()) {
                    System.debug('Error returned: ' +err.getStatusCode() +' - ' +err.getMessage()); 
                    GlobalFormEventTriggerHandlerUtil.logMessage('Error','GlobalFormStagingTriggerHandler','Execute','','','','',null,0,'');   
                }
            }
        }

    } 
    
    
}