/*------------------------------------------------------------
Author:        Krishna Kanth
Company:       IBM
Description:   Test class for GlobalFormTriggerHandler
History:
------------------------------------------------------------*/ 
@isTest
public class GlobalFormStagingTriggerHandler_Test {
    
    @testSetup static void createSetup(){
        List<Global_Form_Staging__c> gfslist = new List<Global_Form_Staging__c>();
        Global_Form_Staging__c gfs1 = new Global_Form_Staging__c();
        gfs1.Content_Type__c = 'JSON';
        gfs1.Data__c = '{"givenName":"JayAsdfsdTest","familyName":"LI","Phone":"0323232323","Email":"jayli@nbnco.com.au","ABN":"86136533741","regName":"NBN CO LIMITED","TradingName":"test trading","Website":"nbnco.com.au","LeadRole":"Technical","leadsource":"NBN Website","Technologies":"FTTP;FTTC;FTTB","Business_ID":"","Distributor_ID":"","Vendor_ID":"","sf_skip_reference_number_check":true,"Partner_Record":true,"T_Cs_Accepted":true,"newServiceProvider":true,"siteAddress":"15 Mill St, Test Suburb, Test City, Australia","layer2Network":true,"technologyCategories":["Fixed Line","Fixed Wireless","Satellite"],"address":{"formattedAddress":"132 Westbury Cl, Balaclava, VIC","id":"LocXXXXXXXXXXXXX","source":"LAPI"}}';
        gfs1.Status__c = 'Start';
        gfs1.Type__c = 'pre-qualification questionnaire'; 
        gfslist.add(gfs1);
        
        Global_Form_Staging__c gfs2 = new Global_Form_Staging__c();
        gfs2.Content_Type__c = 'JSON';
        gfs2.Data__c = '{"givenName":"JayAsdfsdTest","familyName":"","Phone":"0323232323","Email":"jayli@nbnco.com.au","ABN":"86136533741","regName":"NBN CO LIMITED","TradingName":"test trading","Website":"nbnco.com.au","LeadRole":"Technical","leadsource":"NBN Invalid picklist value test","Technologies":"FTTP;FTTC","Business_ID":"","Distributor_ID":"","Vendor_ID":"","sf_skip_reference_number_check":true,"Partner_Record":true,"T_Cs_Accepted":true,"newServiceProvider":true,"siteAddress":"15 Mill St, Test Suburb, Test City, Australia","layer2Network":true,"technologyCategories":["Fixed Line","Fixed Wireless","Satellite"],"address":{"formattedAddress":"143 Pitts Street, Zuccoli NT, Australia","id":"LocXXXXXXXXXXXXX","source":"Google"}}';
        gfs2.Status__c = 'Completed';
        gfs2.Type__c = 'pre-qualification questionnaire'; 
        gfslist.add(gfs2);
        
        insert gfslist;
    }     
    
    @isTest static void createGlobalFormStaging(){     
        // Creation of test event instances
        List<Global_Form_Event__e> GlobalFormEvents = new List<Global_Form_Event__e>();
        List<Global_Form_Staging__c> gfslist = [SELECT Id,Name FROM Global_Form_Staging__c];
        
        for (Global_Form_Staging__c gf: gfslist) {
            Global_Form_Event__e gfevent = new Global_Form_Event__e();
            //gfevent.GlobalFormRecordId__c = gf.Id;
            //gfevent.GlobalFormName__c = gf.Name;	
            gfevent.URL__c = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+gf.Id; 
            gfevent.ResourceType__c = 'ICT Lead'; 	 
            gfevent.Event_Type__c = 'Global Web Form';
            gfevent.Source__c = 'Web Integration';
            GlobalFormEvents.add(gfevent);	         
        }
        
        Test.startTest();
        List<Database.SaveResult> results = EventBus.publish(GlobalFormEvents);
        Test.stopTest();                
        
        // Inspect publishing result for each event
        for (Database.SaveResult sr : results) {
            System.assertEquals(true, sr.isSuccess());
        }
        
        //List<Lead> leads = [SELECT Id FROM Lead];
        //System.assertEquals(2, leads.size()); 
    }
    
    
    @isTest static void createInvalidFormEvent() {
        
        //Global_Form_Event__e gfse = new Global_Form_Event__e(GlobalFormRecordId__c = '12345678901234567890');
        Global_Form_Event__e gfse = new Global_Form_Event__e();
        Test.startTest();
        Database.SaveResult sr = EventBus.publish(gfse);  
        Test.stopTest();
        //system.debug('the created invalid form event method result is '+sr.isSuccess());
        System.assertEquals(false, sr.isSuccess());
        
        // Log the error message
        for(Database.Error err : sr.getErrors()) {
            System.debug('Error returned: ' +
                         err.getStatusCode() +
                         ' - ' +
                         err.getMessage()+' - '+err.getFields());
        } 
        //List<Lead> leads = [SELECT Id FROM Lead];
        //System.assertEquals(0,leads.size());
    } 

}