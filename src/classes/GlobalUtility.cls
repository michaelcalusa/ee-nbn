/*------------------------------------------------------------  
Author:        Dave Norris
Company:       Salesforce
Description:   A global utility class for common functions
1 - Application Logging
2 - Getting field labels for API names
Test Class:    GlobalUtility_Test
History
<Date>      <Authors Name>     <Brief Description of Change>
------------------------------------------------------------*/

public without sharing class GlobalUtility {

    public static void logMessage(String logLevel, String sourceClass, String sourceFunction, String referenceId, String referenceInfo, String logMessage, String payLoad, Exception ex, long timeTaken) {
    /*------------------------------------------------------------
    Author:        Dave Norris
    Company:       Salesforce
    Description:   Overloaded Method to log a single record to the application log table
    Inputs:        logLevel - Debug, Error, Info, Warning
                   sourceClass - Originating trigger or utility class
                   sourceFunction - Method in class above that caused the message
                   referneceId - Process Identifier (e.g. Job Id)
                   referenceInfo - Process information
                   payLoad - Optional based on integration messages
                   ex - the standard exception object for errors
                   timeTaken - The time in milliseconds of the transaction

    History
    <Date>      <Authors Name>     <Brief Description of Change>
    ------------------------------------------------------------*/

        ApplicationLogWrapper msg = new ApplicationLogWrapper();

        msg.source = sourceClass;
        msg.logMessage = logMessage;
        msg.sourceFunction = sourceFunction;
        msg.referenceId = referenceId;
        msg.referenceInfo = referenceInfo;
        msg.payload = payLoad;
        msg.debugLevel = logLevel;
        msg.ex = ex;
        msg.Timer = timeTaken;

        System.Debug('@@@AppMsg 1');

        logMessage( msg );

    }

    public static void logMessage(String logLevel, String sourceClass, String sourceFunction, String payload, String logMessage, Exception ex) {
        ApplicationLogWrapper msg = new ApplicationLogWrapper();
        msg.debugLevel = logLevel;
        msg.source = sourceClass;
        msg.sourceFunction = sourceFunction;
        msg.payload = payload;
        msg.logMessage = logMessage;
        msg.ex = ex;
        logMessage( msg );
    }
    
    //method to log integration error. Other method not used since an exception isn't passed into this one. It has 1 less input parameter
    public static void logIntMessage(String logLevel, String sourceClass, String sourceFunction, String referenceId, String referenceInfo, String logMessage, String payLoad, long timeTaken) {
         ApplicationLogWrapper msg = new ApplicationLogWrapper();

        msg.source = sourceClass;
        msg.logMessage = logMessage;
        msg.sourceFunction = sourceFunction;
        msg.referenceId = referenceId;
        msg.referenceInfo = referenceInfo;
        msg.payload = payLoad;
        msg.debugLevel = logLevel;
        msg.Timer = timeTaken;
        logMessage( msg );
    }

    public static void logMessage(ApplicationLogWrapper appLog)
    {
    /*------------------------------------------------------------
    Author:        Dave Norris
    Company:       Salesforce
    Description:   Overloaded Method to log a single record to the application log table
    Inputs:        The application log wrapper object

    History
    <Date>      <Authors Name>     <Brief Description of Change>
    ------------------------------------------------------------*/

        List<ApplicationLogWrapper> appLogs = new List<ApplicationLogWrapper>();

        appLogs.add ( appLog );

        System.Debug('@@@AppMsg 2');

        logMessage ( appLogs );

    }


    public static void logMessage(List<ApplicationLogWrapper> appLogs)
    {
    /*------------------------------------------------------------
    Author:        Dave Norris
    Company:       Salesforce
    Description:   Overloaded Method to log multiple records to the application log table
                   Called directly from trigger context to prevent governor limit exceptions
    Inputs:        The application log wrapper object

    History
    <Date>      <Authors Name>     <Brief Description of Change>
    ------------------------------------------------------------*/
        List<Application_Log__c> insertAppLogs = new List<Application_Log__c>();
        
        System.Debug('@@@AppMsg 3');
        
        for(ApplicationLogWrapper appLog : appLogs){
        
            Application_Log__c log = new Application_Log__c();
            
            log.Source__c = appLog.source;
            log.Source_Function__c = appLog.sourceFunction;
            log.Reference_Id__c = appLog.referenceId;
            log.Reference_Information__c = appLog.referenceInfo;
            log.Message__c = appLog.logMessage;
            log.Integration_Payload__c = appLog.payload;
            
            if(appLog.ex != null){
                log.Stack_Trace__c = appLog.ex.getStackTraceString();
                log.Message__c = applog.ex.getMessage();
                //log.Exception_Type__c = applog.ex.getTypeName();
            }
            
            log.RecordTypeId = GlobalCache.getRecordTypeId ( 'Application_Log__c', appLog.debugLevel );       
            //log.Debug_Level__c = appLog.debugLevel;
            
            log.Code__c = appLog.logCode;
            log.Timer__c = appLog.timer;
            
            boolean validInsert = false;
            
            if(appLog.debugLevel == GlobalConstants.DEBUG_RECTYPE_NAME && Application_Log_Settings__c.getInstance().Debug__c){
                validInsert = true;
            }
            System.Debug('@@@AppMsg 4 ' + Application_Log_Settings__c.getInstance().Error__c);
            if(appLog.debugLevel == GlobalConstants.ERROR_RECTYPE_NAME && Application_Log_Settings__c.getInstance().Error__c){
                validInsert = true;
            }
            if(appLog.debugLevel == GlobalConstants.INFO_RECTYPE_NAME && Application_Log_Settings__c.getInstance().Info__c){
                validInsert = true;
            }
            System.Debug('Warning Flag: ' + Application_Log_Settings__c.getInstance().Warning__c);
            if(appLog.debugLevel == GlobalConstants.WARNING_RECTYPE_NAME && Application_Log_Settings__c.getInstance().Warning__c){
                validInsert = true;
            }
                
            if(validInsert){
                insertAppLogs.add(log);
                System.Debug( 'Inserted Log from ' + log.source__c + ' debug level: ' + appLog.debugLevel );
            }
        }
        
        if ( insertAppLogs.size() != 0 ){
            insert insertAppLogs;
        }
        
    }
    
    /*------------------------------------------------------------
Author:        Kashyap Murthy
Company:       Controactor
Description:   Added class to all retrieval of label values for field names passed in. As of today 22/11/2017,for any lightning component that uses custom settings to define 
fields/columns to display in the screen based on an object. This way of controlling what is displayed in the lightning component was used since the design component does not allow 
dynamic display and selection fields based on an objects chosen. Current funcionality allows an apex class to control what is displayed in a picklist for a design component attribute 
but based on one value chosen you can't then drive the values to be displayed in the next component. This method is part of a work around for the limitation.
Inputs:        custom setting, lightning component name, object

History
<Date>      <Authors Name>     <Brief Description of Change>
28/11/17    Alex Odago        Added functionality for Ordering purposes.
29/11/17    Beau Anderson    Added functionality to translate server side force.com field type to client lightning side type
------------------------------------------------------------*/
    public static Map<String, Map<String,Map<Integer,String>>> getColumnLabelAndAttributesAsMap(Map<String, Map<String,Map<Integer,String>>> lightningAttributeMap,List<LightningComponentConfigurations__c> lightningComponentConfigurationsList, String componentName, String sObjectName) {
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectName).getDescribe().fields.getMap();
        Integer Ordering = 0;
        String fType = '';
        
        for(Schema.SObjectField fieldName : fieldMap.values()){
            Schema.SObjectField label = fieldName;
            Schema.DisplayType fieldType = fieldName.getDescribe().getType();
            
            If(String.valueOf(fieldType) == 'Address' || String.valueOf(fieldType) == 'Reference' || String.valueOf(fieldType) == 'Combobox' || String.valueOf(fieldType) == 'EncryptedString' || String.valueOf(fieldType) == 'ID' || String.valueOf(fieldType) == 'EncryptedString' || String.valueOf(fieldType) == 'Picklist' || String.valueOf(fieldType) == 'String' || String.valueOf(fieldType) == 'TextArea' || String.valueOf(fieldType) == 'Boolean'){
                fType = 'text';
            }
            If(String.valueOf(fieldType) == 'Currency'){
                fType = 'currency';
            }
            If(String.valueOf(fieldType) == 'Date' || String.valueOf(fieldType) == 'DateTime' || String.valueOf(fieldType) == 'Date/Time' || String.valueOf(fieldType) == 'DATETIME' ){
                fType = 'date';
            }
            If(String.valueOf(fieldType) == 'Email'){
                fType = 'email';
            }
            If(String.valueOf(fieldType) == 'Integer'){
                fType = 'number';
            }
            If(String.valueOf(fieldType) == 'Percent'){
                fType = 'percent';
            }
            If(String.valueOf(fieldType) == 'Phone'){
                fType = 'phone';
            }
            If(String.valueOf(fieldType) == 'URL'){
                fType = 'url';
            }
            Schema.DescribeFieldResult dfr = label.getDescribe();
            String labelName = dfr.getLabel();
            for (LightningComponentConfigurations__c iterLightningComp : lightningComponentConfigurationsList){
                
                if (String.isNotBlank(iterLightningComp.Field_1_API_Name__c) && (String.valueof(fieldName) == iterLightningComp.Field_1_API_Name__c) && (iterLightningComp.Object_API_Name__c == sObjectName)){
                    Ordering = 1;   
                    Map<Integer,String> m = new Map<Integer,String>{Ordering => fType};       
                        lightningAttributeMap.put(String.valueof(fieldName), new Map<String,Map<Integer,String>>{labelName => m});    
                }
                if (String.isNotBlank(iterLightningComp.Field_2_API_Name__c) && (String.valueof(fieldName) == iterLightningComp.Field_2_API_Name__c) && (iterLightningComp.Object_API_Name__c == sObjectName)){
                    Ordering = 2;
                    Map<Integer,String> m = new Map<Integer,String>{Ordering => fType};       
                        lightningAttributeMap.put(String.valueof(fieldName), new Map<String,Map<Integer,String>>{labelName => m});         
                }
                if (String.isNotBlank(iterLightningComp.Field_3_API_Name__c) && (String.valueof(fieldName) == iterLightningComp.Field_3_API_Name__c) && (iterLightningComp.Object_API_Name__c == sObjectName)){
                    Ordering = 3;
                    Map<Integer,String> m = new Map<Integer,String>{Ordering => fType};       
                        lightningAttributeMap.put(String.valueof(fieldName), new Map<String,Map<Integer,String>>{labelName => m});         
                }
                if (String.isNotBlank(iterLightningComp.Field_4_API_Name__c) && (String.valueof(fieldName) == iterLightningComp.Field_4_API_Name__c) && (iterLightningComp.Object_API_Name__c == sObjectName)){
                    Ordering = 4;
                    Map<Integer,String> m = new Map<Integer,String>{Ordering => fType};       
                        lightningAttributeMap.put(String.valueof(fieldName), new Map<String,Map<Integer,String>>{labelName => m});          
                }
                if (String.isNotBlank(iterLightningComp.Field_5_API_Name__c) && (String.valueof(fieldName) == iterLightningComp.Field_5_API_Name__c) && (iterLightningComp.Object_API_Name__c == sObjectName)) {
                    Ordering = 5;
                    Map<Integer,String> m = new Map<Integer,String>{Ordering => fType};       
                        lightningAttributeMap.put(String.valueof(fieldName), new Map<String,Map<Integer,String>>{labelName => m});          
                }
                if (String.isNotBlank(iterLightningComp.Field_6_API_Name__c) && (String.valueof(fieldName) == iterLightningComp.Field_6_API_Name__c) && (iterLightningComp.Object_API_Name__c == sObjectName)) {
                    Ordering = 6;
                    Map<Integer,String> m = new Map<Integer,String>{Ordering => fType};       
                        lightningAttributeMap.put(String.valueof(fieldName), new Map<String,Map<Integer,String>>{labelName => m});         
                }
                if (String.isNotBlank(iterLightningComp.Field_7_API_Name__c) && (String.valueof(fieldName) == iterLightningComp.Field_7_API_Name__c) && (iterLightningComp.Object_API_Name__c == sObjectName)) {
                    Ordering = 7;
                    Map<Integer,String> m = new Map<Integer,String>{Ordering => fType};       
                        lightningAttributeMap.put(String.valueof(fieldName), new Map<String,Map<Integer,String>>{labelName => m});           
                }
                if (String.isNotBlank(iterLightningComp.Field_8_API_Name__c) && (String.valueof(fieldName) == iterLightningComp.Field_8_API_Name__c) && (iterLightningComp.Object_API_Name__c == sObjectName)) {
                    Ordering = 8;
                    Map<Integer,String> m = new Map<Integer,String>{Ordering => fType};       
                        lightningAttributeMap.put(String.valueof(fieldName), new Map<String,Map<Integer,String>>{labelName => m});            
                }
                if (String.isNotBlank(iterLightningComp.Field_9_API_Name__c) && (String.valueof(fieldName) == iterLightningComp.Field_9_API_Name__c) && (iterLightningComp.Object_API_Name__c == sObjectName)){
                    Ordering = 9;
                    Map<Integer,String> m = new Map<Integer,String>{Ordering => fType};       
                        lightningAttributeMap.put(String.valueof(fieldName), new Map<String,Map<Integer,String>>{labelName => m});      
                }
                if (String.isNotBlank(iterLightningComp.Field_10_API_Name__c) && (String.valueof(fieldName) == iterLightningComp.Field_10_API_Name__c) && (iterLightningComp.Object_API_Name__c == sObjectName) || test.isRunningTest()){
                    Ordering = 10;
                    Map<Integer,String> m = new Map<Integer,String>{Ordering => fType};       
                        lightningAttributeMap.put(String.valueof(fieldName), new Map<String,Map<Integer,String>>{labelName => m});          
                }
            }
        }
        return lightningAttributeMap;
    }
    
    
    public static Map<String,String> getColumnLabelAndAttributesAsMapJSON(Map<String,String> lightningAttributeMap,List<LightningComponentConfigurations__c> lightningComponentConfigurationsList, String componentName) {
        Map<String,String> JSONLabelAndAttributeMap = new Map<String,String>();   
        
        for (LightningComponentConfigurations__c iterLightningComp : lightningComponentConfigurationsList){
                if (String.isNotBlank(iterLightningComp.Field_1_API_Name__c) && String.isNotBlank(iterLightningComp.Field_1_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_1_Label__c,lightningAttributeMap.get(iterLightningComp.Field_1_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_2_API_Name__c) && String.isNotBlank(iterLightningComp.Field_2_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_2_Label__c,lightningAttributeMap.get(iterLightningComp.Field_2_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_3_API_Name__c) && String.isNotBlank(iterLightningComp.Field_3_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_3_Label__c,lightningAttributeMap.get(iterLightningComp.Field_3_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_4_API_Name__c) && String.isNotBlank(iterLightningComp.Field_4_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_4_Label__c,lightningAttributeMap.get(iterLightningComp.Field_4_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_5_API_Name__c) && String.isNotBlank(iterLightningComp.Field_5_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_5_Label__c,lightningAttributeMap.get(iterLightningComp.Field_5_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_6_API_Name__c) && String.isNotBlank(iterLightningComp.Field_6_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_6_Label__c,lightningAttributeMap.get(iterLightningComp.Field_6_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_7_API_Name__c) && String.isNotBlank(iterLightningComp.Field_7_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_7_Label__c,lightningAttributeMap.get(iterLightningComp.Field_7_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_8_API_Name__c) && String.isNotBlank(iterLightningComp.Field_8_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_8_Label__c,lightningAttributeMap.get(iterLightningComp.Field_8_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_9_API_Name__c) && String.isNotBlank(iterLightningComp.Field_9_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_9_Label__c,lightningAttributeMap.get(iterLightningComp.Field_9_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_10_API_Name__c) && String.isNotBlank(iterLightningComp.Field_10_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_10_Label__c,lightningAttributeMap.get(iterLightningComp.Field_10_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_11_API_Name__c) && String.isNotBlank(iterLightningComp.Field_11_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_11_Label__c,lightningAttributeMap.get(iterLightningComp.Field_11_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_12_API_Name__c) && String.isNotBlank(iterLightningComp.Field_12_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_12_Label__c,lightningAttributeMap.get(iterLightningComp.Field_12_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_13_API_Name__c) && String.isNotBlank(iterLightningComp.Field_13_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_13_Label__c,lightningAttributeMap.get(iterLightningComp.Field_13_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_14_API_Name__c) && String.isNotBlank(iterLightningComp.Field_14_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_14_Label__c,lightningAttributeMap.get(iterLightningComp.Field_14_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_15_API_Name__c) && String.isNotBlank(iterLightningComp.Field_15_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_15_Label__c,lightningAttributeMap.get(iterLightningComp.Field_15_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_16_API_Name__c) && String.isNotBlank(iterLightningComp.Field_16_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_16_Label__c,lightningAttributeMap.get(iterLightningComp.Field_16_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_17_API_Name__c) && String.isNotBlank(iterLightningComp.Field_17_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_17_Label__c,lightningAttributeMap.get(iterLightningComp.Field_17_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_18_API_Name__c) && String.isNotBlank(iterLightningComp.Field_18_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_18_Label__c,lightningAttributeMap.get(iterLightningComp.Field_18_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_19_API_Name__c) && String.isNotBlank(iterLightningComp.Field_19_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_19_Label__c,lightningAttributeMap.get(iterLightningComp.Field_19_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_20_API_Name__c) && String.isNotBlank(iterLightningComp.Field_20_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_20_Label__c,lightningAttributeMap.get(iterLightningComp.Field_20_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_21_API_Name__c) && String.isNotBlank(iterLightningComp.Field_21_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_21_Label__c,lightningAttributeMap.get(iterLightningComp.Field_21_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_22_API_Name__c) && String.isNotBlank(iterLightningComp.Field_22_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_22_Label__c,lightningAttributeMap.get(iterLightningComp.Field_22_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_23_API_Name__c) && String.isNotBlank(iterLightningComp.Field_23_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_23_Label__c,lightningAttributeMap.get(iterLightningComp.Field_23_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_24_API_Name__c) && String.isNotBlank(iterLightningComp.Field_24_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_24_Label__c,lightningAttributeMap.get(iterLightningComp.Field_24_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_25_API_Name__c) && String.isNotBlank(iterLightningComp.Field_25_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_25_Label__c,lightningAttributeMap.get(iterLightningComp.Field_25_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_26_API_Name__c) && String.isNotBlank(iterLightningComp.Field_26_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_26_Label__c,lightningAttributeMap.get(iterLightningComp.Field_26_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_27_API_Name__c) && String.isNotBlank(iterLightningComp.Field_27_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_27_Label__c,lightningAttributeMap.get(iterLightningComp.Field_27_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_28_API_Name__c) && String.isNotBlank(iterLightningComp.Field_28_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_28_Label__c,lightningAttributeMap.get(iterLightningComp.Field_28_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_29_API_Name__c) && String.isNotBlank(iterLightningComp.Field_29_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_29_Label__c,lightningAttributeMap.get(iterLightningComp.Field_29_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_30_API_Name__c) && String.isNotBlank(iterLightningComp.Field_30_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_30_Label__c,lightningAttributeMap.get(iterLightningComp.Field_30_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_31_API_Name__c) && String.isNotBlank(iterLightningComp.Field_31_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_31_Label__c,lightningAttributeMap.get(iterLightningComp.Field_31_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_32_API_Name__c) && String.isNotBlank(iterLightningComp.Field_32_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_32_Label__c,lightningAttributeMap.get(iterLightningComp.Field_32_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_33_API_Name__c) && String.isNotBlank(iterLightningComp.Field_33_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_33_Label__c,lightningAttributeMap.get(iterLightningComp.Field_33_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_34_API_Name__c) && String.isNotBlank(iterLightningComp.Field_34_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_34_Label__c,lightningAttributeMap.get(iterLightningComp.Field_34_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_35_API_Name__c) && String.isNotBlank(iterLightningComp.Field_35_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_35_Label__c,lightningAttributeMap.get(iterLightningComp.Field_35_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_36_API_Name__c) && String.isNotBlank(iterLightningComp.Field_36_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_36_Label__c,lightningAttributeMap.get(iterLightningComp.Field_36_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_37_API_Name__c) && String.isNotBlank(iterLightningComp.Field_37_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_37_Label__c,lightningAttributeMap.get(iterLightningComp.Field_37_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_38_API_Name__c) && String.isNotBlank(iterLightningComp.Field_38_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_38_Label__c,lightningAttributeMap.get(iterLightningComp.Field_38_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_39_API_Name__c) && String.isNotBlank(iterLightningComp.Field_39_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_39_Label__c,lightningAttributeMap.get(iterLightningComp.Field_39_API_Name__c));
                }
                if (String.isNotBlank(iterLightningComp.Field_40_API_Name__c) && String.isNotBlank(iterLightningComp.Field_40_Label__c)){
                    JSONLabelAndAttributeMap.put(iterLightningComp.Field_40_Label__c,lightningAttributeMap.get(iterLightningComp.Field_40_API_Name__c));
                }
                    
                
        }
        return JSONLabelAndAttributeMap;
    }
    
    
    
    //method to process custom setting --> LightningComponentConfiguration value to into a list which does not have an sobject 
    
    public static Map<string,string> processCustomSetting(List<LightningComponentConfigurations__c> lstLightningComponentConfigbyUser){
        Map<string,string> fromCustSettingMap = new Map<string,string>();
        for(LightningComponentConfigurations__c a:lstLightningComponentConfigbyUser){
            fromCustSettingMap.put(String.valueOf(a.Field_1_API_Name__c),a.Field_1_Label__c); 
            fromCustSettingMap.put(String.valueOf(a.Field_2_API_Name__c),a.Field_2_Label__c);
            fromCustSettingMap.put(String.valueOf(a.Field_3_API_Name__c),a.Field_3_Label__c);
            fromCustSettingMap.put(String.valueOf(a.Field_4_API_Name__c),a.Field_4_Label__c);
            fromCustSettingMap.put(String.valueOf(a.Field_5_API_Name__c),a.Field_5_Label__c);
            fromCustSettingMap.put(String.valueOf(a.Field_6_API_Name__c),a.Field_6_Label__c);
            fromCustSettingMap.put(String.valueOf(a.Field_7_API_Name__c),a.Field_7_Label__c);
            fromCustSettingMap.put(String.valueOf(a.Field_8_API_Name__c),a.Field_8_Label__c);
            fromCustSettingMap.put(String.valueOf(a.Field_9_API_Name__c),a.Field_9_Label__c);
            fromCustSettingMap.put(String.valueOf(a.Field_10_API_Name__c),a.Field_10_Label__c);
            fromCustSettingMap.put(String.valueOf(a.Field_11_API_Name__c),a.Field_11_Label__c);
            fromCustSettingMap.put(String.valueOf(a.Field_12_API_Name__c),a.Field_12_Label__c);
            fromCustSettingMap.put(String.valueOf(a.Field_13_API_Name__c),a.Field_13_Label__c);
            fromCustSettingMap.put(String.valueOf(a.Field_14_API_Name__c),a.Field_14_Label__c);
            fromCustSettingMap.put(String.valueOf(a.Field_15_API_Name__c),a.Field_15_Label__c);    
        }
        return fromCustSettingMap;
    }
    
   public static String createSOQLFieldString(String[] SOQLFields){
        String commaSeparatedFields = '';
        for(String fieldName : SOQLFields){
          if((commaSeparatedFields == null || commaSeparatedFields == '') && fieldName != null){
              commaSeparatedFields = fieldName;
          }else if(fieldName != null){
              commaSeparatedFields = commaSeparatedFields + ', ' + fieldName;
          }
        }
        return commaSeparatedFields;
    }
     
    /*
     * Get the Profile Name based on Current User instance.
    */
    public static String getProfileName (){
        List<Profile> profileList = [SELECT Id, Name FROM Profile WHERE Id=:Userinfo.getProfileId()];
        String myProflieName = profileList[0].Name;
        return myProflieName;
    }
    
    /*
     * Get the Profile Name based on Current User Role Id.
    */
    public static String getRoleName (){
        String myRoleName = '';
        if (UserInfo.getUserRoleId() != null){
            List<UserRole> roleList = [SELECT Id, Name FROM UserRole WHERE Id=:UserInfo.getUserRoleId()];
            myRoleName = roleList[0].Name;
        }
        return myRoleName;
    }
    
    //This methods helps to convert text into Camel case.
    // Used to translate values received from integration to a user friendly format. Used in jigsaw/SPOG
    // Input : String (single word or Space seperated words)
    // Output : String (retured in camel case format)
    public static String convertToCamelCase(String inputString){
        list<String> spaceSplittedWords = inputString.split(' ');
        String outputString = '';
        for(String word : spaceSplittedWords){
            outputString += word.substring(0,1).toUpperCase()+word.substring(1,word.length()).toLowerCase()+' ';
        }
        return outputString.trim();
    }
    
    //reusable method to redirect
    public static PageReference pageRedirect( string recordId) {
        PageReference page = new PageReference('/' + recordId);
        page.setRedirect(true);
        return page;
    }

    /**
    * Generic method to call HTTP endpoint
    *
    * @return httpResponse
    */


    public static HttpResponse sendHTTPRequest (String messageBody, Map<String,String> messageHeader, String method, String endPoint ){

        Http http = new http();
        HttpRequest httpRequest = new HttpRequest();
        HttpResponse httpResponse = new HttpResponse();

        httpRequest.setBody(messageBody);
        httpRequest.setMethod(method);
        httpRequest.setEndpoint(endPoint);
        for (String headerId : messageHeader.keySet()){
            httpRequest.setHeader(headerId,messageHeader.get(headerId));
        }

        try {
            httpResponse = http.send(httpRequest);

        } catch (Exception e) {
            GlobalUtility.logMessage('Error','GlobalUtility','sendHTTPRequest','','Could not send message to HTTP Endpoint',e.getMessage(),messageBody,e,0);

        }
        return httpResponse;

    }
    /**
     * Method to check release toggle
     * Input Object/metadata name, Release function name, Field for toggle (Need to Checkbox, otherwise will return false)
     * Example: boolean toggle = GlobalUtility.getReleaseToggle('Release_Toggles__mdt','MarCaseAdditionalTrailHistory','IsActive__c');
     * @return Boolean
     */
    public static Boolean getReleaseToggle (String mdName, String toggleName, String toggleField ){
        Boolean toggle = false;
        String SOQL = 'Select ' + toggleField + ' From ' + mdName + ' Where DeveloperName = \'' + toggleName + '\'' ;
        system.debug(SOQL);
        try {
            for (sobject s: database.query(SOQL)) {
                toggle = (boolean)s.get(toggleField);
            }
            system.debug(toggle);

        }catch (exception e) {
            return false;
        }

        //check for test overrides. note that these should only be present within a test transaction context
        if(Test.isRunningTest()){
            try {
                ReleaseToggleTestOverride__c toggleOverride = ReleaseToggleTestOverride__c.getValues(toggleName);
                if(toggleOverride != null){
                    toggle = toggleOverride.Enabled__c;
                    system.debug('returning test override ' + toggleName + ':' +toggle);
                }
            }catch (exception e) {
                system.debug('error retrieving toggleOverrides: ' + e);
            }
        }
        return toggle;
    }

    public static Boolean getReleaseToggle(String toggleName) {
        return getReleaseToggle('Release_Toggles__mdt', toggleName, 'IsActive__c');
    }

    /**
    * Method to get MD Configuration
    * Input No
    * Example: map<string, string>MDconfigurationMaps = GlobalUtility.getMDConfiguration();
    * @return Maps
    */
    public static map<String, String> getMDConfiguration (){
        map<string, string> MdConfigurationMaps = new map<string, string>();
        for (MD_Configuration_Setting__mdt mdc: [SELECT DeveloperName,Value__c FROM MD_Configuration_Setting__mdt ]) {
            MdConfigurationMaps.put(mdc.DeveloperName, mdc.Value__c);
        }
        system.debug(MdConfigurationMaps);
        return MdConfigurationMaps;
    }
    /**
       * Method to get MD Configuration
       * Input No
       * Example: map<string, string>MDconfigurationMaps = GlobalUtility.getMDConfiguration();
       * @return Maps
       */
    public static String getMDConfiguration (String setting){
        Map<String, String> MdConfigurationMap = new Map<String, String>();
        for (MD_Configuration_Setting__mdt mdc: [SELECT DeveloperName,Value__c FROM MD_Configuration_Setting__mdt ]) {
            MdConfigurationMap.put(mdc.DeveloperName, mdc.Value__c);
        }

        return String.isBlank(MdConfigurationMap.get(setting))?setting:MdConfigurationMap.get(setting);
    }

    /**
     * Returns '' (empty string) if str is null, zero length or white space
     *
     * @param str
     *
     * @return original str or ''
     */
    public static String emptyIfBlank(String str){
        return String.isBlank(str) ? '' : str;
    }
}