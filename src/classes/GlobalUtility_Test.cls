/*------------------------------------------------------------------------
Author:        Dave Norris
Company:       Salesforce
Description:   A test class created to test GlobalUtility methods
               Test executed:
               1 - 

History
<Date>      <Authors Name>     <Brief Description of Change>
--------------------------------------------------------------------------*/
@isTest
private class GlobalUtility_Test {
   
    static testMethod void logMessage() {      
    test.startTest();
    LightningComponentConfigurations__c lc = new LightningComponentConfigurations__c();
    lc.name='test';
    lc.Role_or_Profile_Based__c = 'test';
    lc.Role_or_Profile_Name__c ='test';
    insert lc;
    list<LightningComponentConfigurations__c> lcs = new list<LightningComponentConfigurations__c>();
    lcs.add(lc);
    GlobalUtility.processCustomSetting(lcs);
    string[] fields = new string[]{};
    fields.add('test');
    fields.add(null);
    GlobalUtility.createSOQLFieldString(fields);
    GlobalUtility.getProfileName();
    GlobalUtility.getRoleName();
    
    lc.Field_1_API_Name__c = 'test';
    lc.Field_1_Label__c = 'test';
    lc.Field_2_API_Name__c = 'test';
    lc.Field_2_Label__c = 'test';
     lc.Field_3_API_Name__c = 'test';
    lc.Field_3_Label__c = 'test';
    lc.Field_4_API_Name__c = 'test';
    lc.Field_4_Label__c = 'test';
    
    lc.Field_5_API_Name__c = 'test';
    lc.Field_5_Label__c = 'test';
    lc.Field_6_API_Name__c = 'test';
    lc.Field_6_Label__c = 'test';
     lc.Field_7_API_Name__c = 'test';
    lc.Field_7_Label__c = 'test';
    lc.Field_8_API_Name__c = 'test';
    lc.Field_8_Label__c = 'test';

    lc.Field_9_API_Name__c = 'test';

    lc.Field_9_Label__c = 'test';

    lc.Field_10_API_Name__c = 'test';

    lc.Field_10_Label__c = 'test';

     lc.Field_11_API_Name__c = 'test';

    lc.Field_11_Label__c = 'test';

    lc.Field_12_API_Name__c = 'test';

    lc.Field_12_Label__c = 'test';

    

    lc.Field_13_API_Name__c = 'test';

    lc.Field_13_Label__c = 'test';

    lc.Field_14_API_Name__c = 'test';

    lc.Field_14_Label__c = 'test';

     lc.Field_15_API_Name__c = 'test';

    lc.Field_15_Label__c = 'test';

    lc.Field_16_API_Name__c = 'test';

    lc.Field_16_Label__c = 'test';

      lc.Field_17_API_Name__c = 'test';

    lc.Field_17_Label__c = 'test';

    lc.Field_18_API_Name__c = 'test';

    lc.Field_18_Label__c = 'test';

     lc.Field_19_API_Name__c = 'test';

    lc.Field_19_Label__c = 'test';

    lc.Field_20_API_Name__c = 'test';

    lc.Field_20_Label__c = 'test';

    

    lc.Field_21_API_Name__c = 'test';

    lc.Field_21_Label__c = 'test';

    lc.Field_22_API_Name__c = 'test';

    lc.Field_22_Label__c = 'test';

     lc.Field_23_API_Name__c = 'test';

    lc.Field_23_Label__c = 'test';

    

    list<LightningComponentConfigurations__c> newlcs = new list<LightningComponentConfigurations__c>();

    newlcs.add(lc);

    map<String,String> vals = new map<String,String>();

    vals.put('test','test');

    GlobalUtility.getColumnLabelAndAttributesAsMapJSON(vals,newlcs ,'incidentManagement');

    try{

    GlobalUtility.getColumnLabelAndAttributesAsMap(null,newlcs,'incidentManagement','incident_Management__c');

    }catch(Exception ex){}

    try{

     Test.setMock(HttpCalloutMock.class, new IntUtilityMockHttpRespGen());                     

     HttpResponse resPri = integrationUtility.getHttpResponse('','');

    }

    catch(Exception e){

    Long Dt=Long.valueOf('4294967296');   

    GlobalUtility.logMessage( 'logLevel',  'sourceClass',  'sourceFunction',  'referenceId',  'referenceInfo', 'logMessage', ' payLoad',e, Long.valueOf('4294967296'));

    } 

    test.stopTest();

    }

    

   static testMethod void  logIntMessage(){

   test.startTest();

    GlobalUtility.logIntMessage( 'logLevel',  'sourceClass',  'sourceFunction',  'referenceId',  'referenceInfo', 'logMessage', ' payLoad', Long.valueOf('4294967296'));

    test.stopTest();

   }

    @isTest static void shouldOverrideReleaseToggle(){

        Test.startTest();

        String toggleName = 'MarCaseAdditionalTrailHistory';

        //first test default mdt setting
        system.AssertEquals(true, GlobalUtility.getReleaseToggle('Release_Toggles__mdt', toggleName, 'IsActive__c'));

        //insert custom settings
        insert new ReleaseToggleTestOverride__c(Name = toggleName, Enabled__c = false);

        //test toggle override
        system.AssertEquals(false, GlobalUtility.getReleaseToggle('Release_Toggles__mdt', toggleName, 'IsActive__c'));

        Test.stopTest();
    }

}