@RestResource(urlMapping='/GlobalFormAPI/v1/*')
global class Global_Form_Staging_WebService {
/****************************************************************************************************************************
Class Name:         Global_Form_Staging_WebService.
Class Type:         Apex Web service Class. 
Version:            1.0 
Created Date:       8th August,2018
Function:           This class will take the input from Web Integration and insert the records to Global_Form_Staging__c object.
Input Parameters:   Data,Type,Status,Content Type.
Output Parameters:  Status,Message,Global Form Id, Global Form Name
Description:        This class will take the input from Web Form Integration and insert the record to Global_Form_Staging__c object.
Modification Log:
* Developer             Date             Description
* --------------------------------------------------------------------------------------------------                 
* Sridevi      08/08/2018      Created - Version 1.0 Refer Form API epic description.- MSEU 13650
*****************************************************************************************************************************/   
    @HttpPost 
    global static void doPost(GlobalFormStagingDetail Detail) {     
        // Instantiate  Global_Form_Staging__c object.    
        Global_Form_Staging__c Fobj = new Global_Form_Staging__c();  
        Boolean insertFlag = true;
        //Instantiate the RestResponse to hold the response of POST Method.
        RestResponse standardResponse = RestContext.response;             
        try{            
            if(String.IsBlank(Detail.FormData) || String.IsBlank(Detail.FormStatus) || String.IsBlank(Detail.FormType) || String.IsBlank(Detail.FormContentType) ) {
               standardResponse = GlobalFormAPI_UtilityClass.displayErrorCode(standardResponse,Detail); 
			   insertFlag = false;                
            } else if(String.IsBlank(Detail.FormStatus) == false && Detail.FormStatus.tolowercase() != 'completed'){
               standardResponse = GlobalFormAPI_UtilityClass.displayInvalidFormStatusError(standardResponse);
               insertFlag = false;
            } else if(Detail.FormData.length() > 131072 || Detail.FormStatus.length() > 255 || Detail.FormType.length() > 255
                     || Detail.FormContentType.length() > 255){                
               standardResponse = GlobalFormAPI_UtilityClass.AllFieldsLengthCheck(standardResponse,Detail);
               insertFlag = false;               
            }
            
            if(insertFlag){
                //insert the data to Global Form Staging Object.
                Fobj.Data__c = Detail.FormData.trim();      
                Fobj.Status__c = Detail.FormStatus.trim();
                Fobj.Type__c = Detail.FormType.trim();
                Fobj.Content_Type__c = Detail.FormContentType.trim();                 
                Database.SaveResult insertRecord = Database.insert(Fobj); 
                
                if (insertRecord.isSuccess()) { 
                    //Select the record which is inserted.
                    Global_Form_Staging__c formRec = [select Id, Name from Global_Form_Staging__c where id =: Fobj.id];
                    standardResponse = GlobalFormAPI_UtilityClass.getResponseCode(standardResponse,formRec);
                } else {
                    standardResponse = GlobalFormAPI_UtilityClass.displayDMLError(standardResponse);  
                } 
            }
        } catch(Exception e) {    
            standardResponse = GlobalFormAPI_UtilityClass.displayStatusNotFoundError(standardResponse,e);
            GlobalFormEventTriggerHandlerUtil.logMessage('Error','FormAPI_WebService failed','doPost() method','','','','',e,0,'');                   
        }        
    }    
    //Wrapper to hold the FormObject data from Web Form Integration.
    global class GlobalFormStagingDetail {
        global String FormData {get;set;}
        global String FormStatus {get;set;}
        global String FormType {get;set;}
        global String FormContentType {get;set;}               
        public GlobalFormStagingDetail(){}
    }
}