@isTest
public class Global_Form_Staging_WebService_Test {
    
    @isTest static void testdoPostMissingMandatoryFields() {
        
        //set up test input payload        
        Global_Form_Staging_WebService.GlobalFormStagingDetail testJsonLoad = new Global_Form_Staging_WebService.GlobalFormStagingDetail();
        testJsonLoad.FormStatus = null;
        testJsonLoad.FormType = 'Pre-qualification questionnaire';
        testJsonLoad.FormContentType = 'application/json';
        testJsonLoad.FormData = '{"FirstName":"JayAsdfsdTest","MiddleName":"sddgdgd","LastName":"LI","Phone":"0323232323","Email":"jayli@nbnco.com.au","ABN":"86136533741","Company":"NBN CO LIMITED","TradingName":"test trading","Website":"nbnco.com.au","LeadRole":"Technical","leadsource":"NBN Website","recordTypeId":"01228000000ycCL","Business_ID":"","Distributor_ID":"","Vendor_ID":"","sf_skip_reference_number_check":true,"Partner_Record":true,"T_Cs_Accepted":true}';
        string SerlizaedJsonPayLoad = Json.serialize(testJsonLoad);

        RestRequest req = new RestRequest();        
        req.requestURI = '/services/apexrest/createGlobalFormStagingRecord/v1';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type       
        RestContext.request = req;

        RestContext.response= new RestResponse();
		       
        Test.startTest();
        Global_Form_Staging_WebService.doPost(testJsonLoad);
        Test.StopTest();  
        
        system.assertEquals(RestContext.response.statusCode,400);     
    }
    
    @isTest static void testdoPostTwoWithAllFields(){
        RestRequest req = new RestRequest();
        req.requestURI = '/services/apexrest/GlobalFormAPI/v1'; //Request URL
        req.httpMethod = 'POST';        
        RestContext.request = req;
        
        RestContext.response= new RestResponse();
        
        //set up test input payload
        Global_Form_Staging_WebService.GlobalFormStagingDetail testJsonLoad = new Global_Form_Staging_WebService.GlobalFormStagingDetail();
        testJsonLoad.FormStatus = 'Completed';
        testJsonLoad.FormType = 'Pre-qualification questionnaire';
        testJsonLoad.FormContentType = 'application/json';
        testJsonLoad.FormData = '{"layer2Network":true,"newServiceProvider":true,"wholesaleProviderName":"Wholesaler test provider test","technologyCategories": ["Fixed Wireless","Fixed Line","Satellite"],"enquiryType":"Business","abn":"11112222","regName":"Test Reg Company","givenName":"Robin","familyName":"Hood","phoneNumber":"0232342323","emailAddress":"wayneni@nbnco.com.au","website":"www.nbnco.com.au","carrierNumber":"11111111","jobTitle":"CEO","address":{"formattedAddress":"132 Westbury Cl, Balaclava, VIC","addressId":"LocXXXXXXXXXXXXX","source":"lapi"}}';
    	
        Test.startTest();
        Global_Form_Staging_WebService.doPost(testJsonLoad);
        Test.StopTest();
        
        system.assertEquals(RestContext.response.statusCode,200);      
    }
    
    @isTest static void testdisplayDMLErrorMethod() {
        
        //set up test input payload        
        Global_Form_Staging_WebService.GlobalFormStagingDetail testJsonLoad = new Global_Form_Staging_WebService.GlobalFormStagingDetail();
        string SerlizaedJsonPayLoad = Json.serialize(testJsonLoad);

        RestRequest req = new RestRequest();        
        req.requestURI = '/services/apexrest/createGlobalFormStagingRecord/v1';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type       
        RestContext.request = req;

        RestContext.response= new RestResponse();
		       
        Test.startTest();
        Global_Form_Staging_WebService.doPost(testJsonLoad);
        RestResponse testresult = GlobalFormAPI_UtilityClass.displayDMLError(RestContext.response);
        Test.StopTest();  
        
        system.assertEquals(405,testresult.statusCode);   
    }
    
    @isTest static void displayStatusNotFoundErrorMethod() {
        
        //set up test input payload  
        

        RestRequest req = new RestRequest();        
        req.requestURI = '/services/apexrest/createGlobalFormStagingRecord/v1';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type       
        RestContext.request = req;

        RestContext.response= new RestResponse();
		       
        Test.startTest();
        RestResponse testresult = GlobalFormAPI_UtilityClass.displayStatusNotFoundError(RestContext.response,null);
        Test.StopTest();  
        
        system.assertEquals(404,testresult.statusCode);   
    }
    
    @isTest static void testOverLimitCharacterScenario() {
        
        //set up test input payload        
        Global_Form_Staging_WebService.GlobalFormStagingDetail testJsonLoad = new Global_Form_Staging_WebService.GlobalFormStagingDetail();

        RestRequest req = new RestRequest();        
        req.requestURI = '/services/apexrest/createGlobalFormStagingRecord/v1';  //Request URL
        req.httpMethod = 'POST';//HTTP Request Type       
        RestContext.request = req;

        RestContext.response= new RestResponse();
        
        //set up test input payload
        testJsonLoad.FormStatus = 'Completed';
        testJsonLoad.FormType = 'Pre-qualification questionnaire test 255 chacaters Pre-qualification questionnaire test 255 chacaters Pre-qualification questionnaire test 255 chacaters Pre-qualification questionnaire test 255 chacaters Pre-qualification questionnaire test 255 chacaters Pre-qualification questionnaire test 255 chacaters';
        testJsonLoad.FormContentType = 'application/json application/json application/json application/json application/json application/json application/json application/json application/json application/json application/json application/json application/json application/json application/json application/json application/json application/json application/json';
        testJsonLoad.FormData = '{"FirstName":"JayAsdfsdTest","MiddleName":"sddgdgd","LastName":"LI","Phone":"0323232323","Email":"jayli@nbnco.com.au","ABN":"86136533741","Company":"NBN CO LIMITED","TradingName":"test trading","Website":"nbnco.com.au","LeadRole":"Technical","leadsource":"NBN Website","recordTypeId":"01228000000ycCL","Business_ID":"","Distributor_ID":"","Vendor_ID":"","sf_skip_reference_number_check":true,"Partner_Record":true,"T_Cs_Accepted":true}';
		       
        Test.startTest();
        Global_Form_Staging_WebService.doPost(testJsonLoad);
        Test.StopTest();  
        
        system.assertEquals(RestContext.response.statusCode,400);     
    }


}