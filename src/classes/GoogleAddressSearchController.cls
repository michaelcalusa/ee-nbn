/***************************************************************************************************
    Class Name          : GoogleAddressSearchController
    Version             : 1.0 
    Created Date        : 26-Aug-2017
    Author              : Rupendra Kumar Vats
    Description         : Logic to call Google Map API to return address
    Input Parameters    : Address Search Term 
    Output Parameters   : Addresses returned by the Google API

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats       26-Aug-2017       Logic to call Google Map API to return address
****************************************************************************************************/ 
public class GoogleAddressSearchController{
    
    // Method will search the given address term using Google Place API and will return matching results
    @AuraEnabled
    public static List<AddressWrapper> searchGoogleMap(String strAddressTerm){
        List<AddressWrapper> lstAddressWrapper = new List<AddressWrapper>();
        try{
            String strEndPoint = '?key='+ System.Label.GoogleAPIKey + '&components=country:AU&input=';

            // Create a HTTP request
            Http h = new Http(); 
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            //req.setEndpoint(strEndPoint + EncodingUtil.urlEncode(strAddressTerm, 'UTF-8'));
            req.setEndpoint('callout:Google_Place_API' + strEndPoint + EncodingUtil.urlEncode(strAddressTerm, 'UTF-8'));
            req.setTimeout(integer.valueof(Label.Location_TimeOut_In_MilliSeconds));
            
            HttpResponse res = h.send(req); 
            
            system.debug('---response---' + res);
            system.debug('---responseBody---' + res.getBody());  
            
            MapResponse mapAddress = (MapResponse) JSON.deserialize(res.getBody(), GoogleAddressSearchController.MapResponse.class);
            
            system.debug('---mapAddress---' + mapAddress);

            if(mapAddress.status.toLowerCase() == 'ok'){
                if(!mapAddress.predictions.isEmpty()){
                    
                    // Set five results to display
                    List<GoogleAddressSearchController.MapComponents> lstAddress = new List<GoogleAddressSearchController.MapComponents>();
                    if(mapAddress.predictions.size() <= 5){
                        lstAddress.addAll(mapAddress.predictions);
                    }
                    else{
                        for(integer i=0; i<5; i++){
                            lstAddress.add(mapAddress.predictions[i]);
                        }
                    }
                    
                    // Iterate over the addresses
                    for(GoogleAddressSearchController.MapComponents result : lstAddress){
                        lstAddressWrapper.add(new AddressWrapper(result.description, result.place_id));
                    }
                }
            } 

            system.debug('---lstFormattedAddress---' + lstAddressWrapper);
        }catch(Exception ex){
            system.debug('---Exception - GoogleAddressSearchController Class - searchGoogleMap() ---' + ex.getMessage());
        }
        return lstAddressWrapper;
    }
    
    // Method will call the  Google Place Details API using the selected address's Place ID and will return the address details 
    @AuraEnabled
    public static AddressDetailsWrapper getAddressDetails(String strPlaceID){
        AddressDetailsWrapper AddressWrapper;
        String strSubPremise, strStreet, strStreetName, strSuburb, strState, strPostalCode, strSt, latitude, longitude;
        try{
            if(!string.isEmpty(strPlaceID)){
                String strEndPoint = '?key='+ System.Label.GoogleAPIKey + '&placeid=' + strPlaceID;

                // Create a HTTP request
                Http h = new Http(); 
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setEndpoint('callout:Google_Place_Details_API' + strEndPoint);
                req.setTimeout(integer.valueof(Label.Location_TimeOut_In_MilliSeconds));
                
                HttpResponse res = h.send(req); 
                
                system.debug('---response---' + res);
                system.debug('---responseBody---' +string.valueOf(res.getBody()));  
                
                MapDetailsResponse mapAddressDetails = (MapDetailsResponse) JSON.deserialize(res.getBody(), GoogleAddressSearchController.MapDetailsResponse.class);
                
                system.debug('---mapAddressDetails---' + mapAddressDetails);

                if(mapAddressDetails.status.toLowerCase() == 'ok'){
                    strSt = '';
                    strSubPremise = '';
                    strStreet = ''; 
                    strStreetName = ''; 
                    strSuburb = ''; 
                    strState = ''; 
                    strPostalCode = '';
                    latitude = mapAddressDetails.result.geometry.location.lat;
					longitude = mapAddressDetails.result.geometry.location.lng;
                    // Iterate over the address information
                    for(GoogleAddressSearchController.MapInformation cmp : mapAddressDetails.result.address_components){
                         
                        system.debug('---address component---' + cmp);
                        if(cmp.types[0] == 'subpremise'){
                            strSubPremise = cmp.short_name;
                        }
                        if(cmp.types[0] == 'street_number'){
                            strStreet = cmp.short_name;
                        }
                        else if(cmp.types[0] == 'route'){
                            strStreetName = cmp.short_name;
                        }
                        else if(cmp.types[0] == 'locality'){
                            strSuburb = cmp.short_name;
                        }
                        else if(cmp.types[0] == 'administrative_area_level_1'){
                            strState = cmp.short_name;
                        }
                        else if(cmp.types[0] == 'postal_code'){
                            strPostalCode = cmp.short_name;
                        }
                    }
                    
                    if(!string.isEmpty(strSubPremise)){
                        strSt = strSubPremise + '/' + strStreet + ' ' + strStreetName;
                    }
                    else{
                        strSt = strStreet + ' ' + strStreetName;
                    }
                }
                    
                AddressWrapper = new AddressDetailsWrapper( strSt, strSuburb, strState, strPostalCode, latitude, longitude);
                
                system.debug('---AddressWrapper---' + AddressWrapper);
            }
        }catch(Exception ex){
            system.debug('---Exception - GoogleAddressSearchController Class - getAddressDetails() ---' + ex.getMessage());
            AddressWrapper = new AddressDetailsWrapper( strSt, strSuburb, strState, strPostalCode, latitude, longitude);
        }
        return AddressWrapper;
    }
    
    /* JSON classes to parse the Google Place API response start */
    
    public class MapResponse{
        public String status;
        public List<MapComponents> predictions;
    }
    
    public class MapComponents{
       public String description;
       public String place_id;
    }
    
    /* JSON classes to parse the Google Place API response end */
    
    /* JSON classes to parse the Google Place Details API response start */
    
    public class MapDetailsResponse{
        public String status;
        @AuraEnabled public MapDetailsComponents result;
    }
    
    public class MapDetailsComponents{
        public List<MapInformation> address_components;
        @AuraEnabled public geometry geometry;
        @AuraEnabled public string formatted_address;
    }
    
    public class MapInformation {
        public String long_name;
        public String short_name;
        public List<String> types;
    }
    public class geometry{
        @AuraEnabled public location location;
    }
    public class location{
        @AuraEnabled public string lat;
        @AuraEnabled public string lng;
    }
    /* JSON classes to parse the Google Place Details API response end */
    
    /* Wrapper class to pass address information */
    public class AddressWrapper implements NewDevelopmentsControllerInterface{
        @AuraEnabled
        public String strAddress { get; set; }
        
        @AuraEnabled
        public String strPlaceId { get; set; }
        
        public AddressWrapper(String strAddress, String strPlaceId){
            this.strAddress = strAddress;
            this.strPlaceId = strPlaceId;
        }
    }
    
    /* Wrapper class to pass address details information */
    public class AddressDetailsWrapper{
        @AuraEnabled
        public String strStreet { get; set; }
        
        @AuraEnabled
        public String strSuburb { get; set; }
        
        @AuraEnabled
        public String strState { get; set; }
        
        @AuraEnabled
        public String strPostalCode { get; set; }
        
        @AuraEnabled
        public string latitude {get; set;}
        
        @AuraEnabled
        public string longitude {get; set;}
        
        public AddressDetailsWrapper(String strStreet, String strSuburb, String strState, String strPostalCode, string latitude, string longitude){
            this.strStreet = strStreet;
            this.strSuburb = strSuburb;
            this.strState = strState;
            this.strPostalCode = strPostalCode;
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }
}