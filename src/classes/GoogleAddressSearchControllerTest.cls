/***************************************************************************************************
    Class Name  : GoogleAddressSearchControllerTest
    Class Type  : Test Class 
    Version     : 1.0 
    Created Date: 07-September-2017
    Function    : This class contains unit test scenarios for GoogleAddressSearchController apex class which handles Google APIs for Tech Choice
    Used in     : None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            06/09/2017                 Created
****************************************************************************************************/
@isTest(seeAllData = false)
public class GoogleAddressSearchControllerTest{
    static testMethod void TestMethod_searchGoogleMap(){
        // Verify system call out exception
        GoogleAddressSearchController.searchGoogleMap('177');
        
        // Verify Responses using Mock classes for Success and Failure
        MockHttpResponse mockResp = new MockHttpResponse();
        
        // Verify the success call out
        mockResp.strResponseType = 'GooglePlaceSearch';
        Test.setMock(HttpCalloutMock.class, mockResp);
        GoogleAddressSearchController.searchGoogleMap('177');
        
        mockResp.strResponseType = 'GooglePlaceDetails';
        GoogleAddressSearchController.getAddressDetails('177');
    }
}