global class HelperIntegrationUtility{
    public static string getcurrentDateTime(dateTime givenDateTime){
        if(givenDateTime <> null)
            return givenDateTime.format(GlobalConstants.DATE_TIME_FORMAT);
        else
            return null;
    }
    // used by Service Portal
    public static Case findCase(string SfOrCrmOdCaseNumber, Id primaryContactid){
        if(string.isNotBlank(SfOrCrmOdCaseNumber) && primaryContactid <> null){
            List<Case> caseList = new List<Case> ();
            caseList = [SELECT Id, On_Demand_Case_Number__c, CaseNumber, ContactId, contact.FirstName, contact.LastName, contact.Preferred_Phone__c, contact.Alternate_Phone__c, contact.Email, Contact.Name, Contact.IsEmailBounced, createdDate, Website_Attachment_URL__c, Website_Attachment_Base_URL__c, RecordType.Name FROM Case WHERE ((CaseNumber =: SfOrCrmOdCaseNumber OR On_Demand_Case_Number__c =: SfOrCrmOdCaseNumber) AND ContactId =: primaryContactid) order by lastmodifieddate DESC];
            if(!caseList.isEmpty()){
                return caseList.get(0);
            }
        }
        return null;
    }
    // used by Public Website
    public static Case findCaseByCaseNumber(string SfOrCrmOdCaseNumber){
        if(string.isNotBlank(SfOrCrmOdCaseNumber)){
            List<Case> caseList = new List<Case> ();
            caseList = [SELECT Id, On_Demand_Case_Number__c, CaseNumber, ContactId, contact.FirstName, contact.LastName, contact.Preferred_Phone__c, contact.Alternate_Phone__c, contact.Email, Contact.Name, Contact.IsEmailBounced, createdDate, Website_Attachment_URL__c, Website_Attachment_Base_URL__c, RecordType.Name FROM Case WHERE (CaseNumber =: SfOrCrmOdCaseNumber OR On_Demand_Case_Number__c =: SfOrCrmOdCaseNumber) order by lastmodifieddate DESC];
            if(!caseList.isEmpty()){
                return caseList.get(0);
            }
        }
        return null;
    }
    public static Account findRspAccount(string accessSeekerid){
        if(string.isNotBlank(accessSeekerid)){
            List<Account> accountList = new List<Account> ();
            Id RspRecordTypeId = GlobalCache.getRecordTypeId('Account', GlobalConstants.RSP_RECTYPE_NAME);
            accountList = [SELECT Id, Access_Seeker_ID__c, Linked_End_User_Id__c FROM Account WHERE (Access_Seeker_ID__c =: accessSeekerid AND RecordTypeId =: RspRecordTypeId)];
            if(!accountList.isEmpty()){
                return accountList.get(0);
            }
        }
        return null;
    }
    
    public static Map<String, String> findRspAccountList(List<String> accessSeekerIdList){
        Map<String, String> accessSeekerIdAndAccMap = new Map<String, String>();
        if(!accessSeekerIdList.isEmpty()){
            List<Account> accountList = new List<Account> ();
            Id RspRecordTypeId = GlobalCache.getRecordTypeId('Account', GlobalConstants.RSP_RECTYPE_NAME);
            accountList = [SELECT Id, Access_Seeker_ID__c, Linked_End_User_Id__c FROM Account WHERE (Access_Seeker_ID__c IN: accessSeekerIdList AND RecordTypeId =: RspRecordTypeId)];
            for (Account iterAccount : accountList){
            	accessSeekerIdAndAccMap.put(iterAccount.Access_Seeker_ID__c, iterAccount.Id);    
            }
        }
        return accessSeekerIdAndAccMap;
    }
    
    public static Contact findContact(string firstName,string lastName,string phoneNumber,string emailAddress){
        if((string.isNotBlank(firstName)) && (string.isNotBlank(lastName)) && ((string.isNotBlank(phoneNumber)) || (string.isNotBlank(emailAddress)))){
            List<Contact> contactList = new List<Contact>  ();
            Id ExtContactRecordTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME);
            String contactQuery = 'SELECT Id, FirstName, LastName, Email, Preferred_Phone__c, Alternate_Phone__c, AccountId, IsEmailBounced FROM contact WHERE (RecordTypeId =: ExtContactRecordTypeId AND FirstName =: firstName AND LastName =: lastName) order by lastmodifieddate DESC';
            if(string.isNotBlank(contactQuery)){
                system.debug('contactQuery==>'+contactQuery);
                contactList = database.query(contactQuery);
                if(!contactList.isEmpty()){
                    system.debug('contactList Size==>'+contactList.size());
                    Map<string,contact> mapofcontact = new Map<string,contact> ();
                    for(Contact contactRec : contactList){
                        if(string.isNotBlank(emailAddress) && contactRec.Email == emailAddress){
                            if(!mapofcontact.containsKey(contactRec.Email))
                                mapofcontact.put(contactRec.Email,contactRec);
                        }
                        if(string.isNotBlank(phoneNumber) && contactRec.Preferred_Phone__c == phoneNumber){
                            if(!mapofcontact.containsKey('PREFERRED PHONE - '+contactRec.Preferred_Phone__c))
                                mapofcontact.put('PREFERRED PHONE - '+contactRec.Preferred_Phone__c,contactRec);
                        }
                        if(string.isNotBlank(phoneNumber) && contactRec.Alternate_Phone__c == phoneNumber){
                            if(!mapofcontact.containsKey('ALTERNATE PHONE - '+contactRec.Alternate_Phone__c))
                                mapofcontact.put('ALTERNATE PHONE - '+contactRec.Alternate_Phone__c,contactRec);
                        }
                    }
                    if(!mapofcontact.isEmpty()){
                        if(string.isNotBlank(emailAddress)){
                            if(mapofcontact.containsKey(emailAddress.toLowerCase()))
                                return mapofcontact.get(emailAddress.toLowerCase());
                        }
                        if(mapofcontact.containsKey('PREFERRED PHONE - '+phoneNumber))
                            return mapofcontact.get('PREFERRED PHONE - '+phoneNumber);
                        if(mapofcontact.containsKey('ALTERNATE PHONE - '+phoneNumber))
                            return mapofcontact.get('ALTERNATE PHONE - '+phoneNumber);
                    }
                }
            }
        }
        return null;
    }
    // Note : Assuming External system will handle the Email format for emailAddress attribute. Else. system will not create Contact record.
    public static Contact createContact(string firstName, string lastName, string phoneNumber, string emailAddress, boolean isEnduser, Account accountRec, string preferredContactMethod){
        Contact contactRec = new Contact();

        contactRec.RecordTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME);
        if(string.isNotBlank(firstName))
            contactRec.FirstName = firstName;
        if(string.isNotBlank(lastName))
            contactRec.LastName = lastName;
        if(string.isNotBlank(phoneNumber))
            contactRec.Preferred_Phone__c = phoneNumber;
        if(string.isNotBlank(emailAddress)){
            // No_Email_provided__c is made false. As Validation rule exists in Contact object
            contactRec.Email = emailAddress;
            contactRec.No_Email_provided__c = false;
        }
        else{
            contactRec.No_Email_provided__c = true;
        }
        if(string.isNotBlank(preferredContactMethod)){
            if(preferredContactMethod.equalsIgnoreCase(GlobalConstants.ONLINE_PREFERRED_CONTACT_METHOD_PHONE))
                contactRec.Preferred_Contact_Method__c = GlobalConstants.PREFERRED_CONTACT_METHOD_PHONE;
            else
                contactRec.Preferred_Contact_Method__c = preferredContactMethod;
        }
        if(!isEnduser){
            // Creating Access Seeker Contact
            if(accountRec <> null){
                contactRec.AccountId = accountRec.id;
            }
        }

        return contactRec;
    }
    public static Case createCase(ExternalSystemRequestJsonToApex.caseDetails caseDetails, Contact accessSeeker, Contact endUser, string contactInstructions, ExternalSystemRequestJsonToApex.LocationAddress siteDetails, string accessSeekerReferenceid, boolean isFromServicePortal, Id RecordTypeId, Account rspAccount){
        Case caseRecord = new Case ();
        if(string.isNotBlank(accessSeekerReferenceid))
            caseRecord.RSP_ISP_Reference_Id__c = accessSeekerReferenceid;
        if(string.isNotBlank(caseDetails.isTio) && caseDetails.isTio.equalsIgnoreCase('Y'))
            caseRecord.TIO__c = caseDetails.tioLevel;
        if(string.isNotBlank(caseDetails.tioReferenceId) && string.isNotBlank(caseDetails.isTio) && caseDetails.isTio.equalsIgnoreCase('Y'))
            caseRecord.TIO_Reference_Number__c = caseDetails.tioReferenceId;
        //added by Wayne
        if(string.isNotBlank(caseDetails.campaignName)){
            
                 caseRecord.Campaign_Name__c = caseDetails.campaignName;
           
        }
        //End here
        if(string.isNotBlank(caseDetails.dateofComplaintevent) && caseDetails.dateofComplaintevent.contains('T')){
            // format given -- 2016-07-11T14:00:01Z
            string dateTimeOfComplaint = caseDetails.dateofComplaintevent;
            if(dateTimeOfComplaint.containsIgnoreCase('Z')){
                dateTimeOfComplaint = dateTimeOfComplaint.Substring(0,dateTimeOfComplaint.length()-1);
                String[] splitDateAndTime = dateTimeOfComplaint.split('T');
                if(splitDateAndTime <> null){
                    // Separate the Time
                    String[] timeVal = splitDateAndTime[1].split(':');
                    // Separate the Date
                    String[] dateVal = splitDateAndTime[0].split('-');
                    // Set the Date and Time
                    Datetime GMTDate = Datetime.newInstanceGmt(Integer.valueOf(dateVal[0]), Integer.valueOf(dateVal[1]), Integer.valueOf(dateVal[2]),Integer.valueOf(timeVal[0]), Integer.valueOf(timeVal[1]), Integer.valueOf(timeVal[2]));
                    // Format to Local time zone
                    String strConvertedDate = GMTDate.format(GlobalConstants.DATE_FORMAT, GlobalConstants.SYDNEY_TIME_ZONE);
                    date mydate = date.parse(strConvertedDate);
                    caseRecord.Date_of_Complaint_Event__c = myDate;
                }
            }
        }
        if(string.isNotBlank(contactInstructions)){
            if(contactInstructions.equalsIgnoreCase(GlobalConstants.ACCESS_SEEKER_FIRST))
                caseRecord.Authorisation_for_Contact__c = GlobalConstants.PRIMARY_FIRST;
            if(contactInstructions.equalsIgnoreCase(GlobalConstants.ACCESS_SEEKER_ONLY))
                caseRecord.Authorisation_for_Contact__c = GlobalConstants.PRIMARY_ONLY;
            if(contactInstructions.equalsIgnoreCase(GlobalConstants.END_USER_DIRECT))
                caseRecord.Authorisation_for_Contact__c = GlobalConstants.SECONDARY_ONLY;
            if(contactInstructions.equalsIgnoreCase(GlobalConstants.BOTH_PARTIES))
                caseRecord.Authorisation_for_Contact__c = GlobalConstants.ALL_PARTIES;            
        }
        if(caseDetails.nbnReferenceIds <> null && !caseDetails.nbnReferenceIds.isEmpty()){
            caseRecord.description = 'NBN Co Reference Ids : ';
            integer counter = 0;
            for(ExternalSystemRequestJsonToApex.nbnReferenceIds var : caseDetails.nbnReferenceIds){
                if(string.isNotBlank(var.id)){
                    counter++;
                    if(counter < caseDetails.nbnReferenceIds.size())
                        caseRecord.description += var.id+', ';
                    else
                        caseRecord.description += var.id;
                }
            }
        }
        if(string.isNotBlank(caseDetails.nbnServiceId)){
            if(string.isNotBlank(caseRecord.description))
                caseRecord.description += '\r\n NBN Co Service Id : '+caseDetails.nbnServiceId;
            else
                caseRecord.description = 'NBN Co Service Id : '+caseDetails.nbnServiceId;
        }
        if(isFromServicePortal){
            if(string.isNotBlank(caseDetails.description)){
                if(string.isNotBlank(caseRecord.description))
                    caseRecord.description += '\r\n Description : '+caseDetails.description;
                else
                    caseRecord.description = 'Description : '+caseDetails.description;
            }
        }
        else{
            if(string.isNotBlank(caseDetails.description))
                caseRecord.description = caseDetails.description;
        }
        if(string.isNotBlank(caseDetails.desiredOutcome)){
            if(string.isNotBlank(caseRecord.description))
                caseRecord.description += '\r\n Desired Outcome : '+caseDetails.desiredOutcome;
            else
                caseRecord.description = 'Desired Outcome : '+caseDetails.desiredOutcome;
        }
        
        if(isFromServicePortal){
            caseRecord.RecordTypeId = RecordTypeId;
            caseRecord.Origin = GlobalConstants.SERVICE_PORTAL_CASE_ORIGIN;
            if(rspAccount <> null && rspAccount.Id <> null){
                caseRecord.RSP_ISP__c = rspAccount.Id;
            }
            if(accessSeeker <> null && accessSeeker.Id <> null){
                caseRecord.ContactId = accessSeeker.Id;
                caseRecord.Primary_Contact_Role__c = GlobalConstants.RSP_ISP_AS_PY_ROLE;
            }
            if(endUser <> null && endUser.Id <> null){
                caseRecord.Secondary_Contact__c = endUser.id;
                caseRecord.Secondary_Contact_Role__c = GlobalConstants.END_USER_CONSUMER_ROLE;
            }
        }
        else{
            caseRecord.RecordTypeId = RecordTypeId;
            caseRecord.Origin = GlobalConstants.WEB_PUBLIC_WEBSITE_ORIGIN;
            if(endUser <> null && endUser.Id <> null){
                caseRecord.ContactId = endUser.id;
                caseRecord.Primary_Contact_Role__c = GlobalConstants.END_USER_CONSUMER_ROLE;
            }
        }
        // Associate Site Address
        if(siteDetails <> null){
            caseRecord.Site_Address__c = '';
            if(string.isNotBlank(siteDetails.premisetype))
                caseRecord.Site_Address__c = siteDetails.premisetype+', ';
            if(string.isNotBlank(siteDetails.unitNumber))
                caseRecord.Site_Address__c += siteDetails.unitNumber+', ';
            if(string.isNotBlank(siteDetails.streetOrLotnumber))
                caseRecord.Site_Address__c += siteDetails.streetOrLotnumber+', ';
            if(string.isNotBlank(siteDetails.streetName))
                caseRecord.Site_Address__c += siteDetails.streetName+', ';
            if(string.isNotBlank(siteDetails.streetType))
                caseRecord.Site_Address__c += siteDetails.streetType+', ';
            if(string.isNotBlank(siteDetails.suburbOrlocality))
                caseRecord.Site_Address__c += siteDetails.suburbOrlocality+', ';
            if(string.isNotBlank(siteDetails.state))
                caseRecord.Site_Address__c += siteDetails.state+', ';
            if(string.isNotBlank(siteDetails.postCode))
                caseRecord.Site_Address__c += siteDetails.postCode+' ';
            if(string.isNotBlank(siteDetails.country))
                caseRecord.Site_Address__c += siteDetails.country;
        }
        system.debug('caseRecord===>'+caseRecord);
        insert caseRecord;
        if(caseRecord.id <> null){
            Case createdCase = [SELECT Id, On_Demand_Case_Number__c, CaseNumber, ContactId, contact.FirstName, contact.LastName, contact.Preferred_Phone__c, contact.Alternate_Phone__c, contact.Email, Contact.Name, Contact.IsEmailBounced, createdDate, Website_Attachment_URL__c, Website_Attachment_Base_URL__c, RecordType.Name, RSP_ISP_Reference_Id__c, Date_of_Complaint_Event__c, Origin, Account.Name, Site_Address__c, Authorisation_for_Contact__c, RSP_ISP__c FROM Case WHERE Id =: caseRecord.id];
            return createdCase;
        }
        return caseRecord;
    }
    
    public static Case createNBNGoCase(NBNGoRequestJsonToApex.caseDetails caseDetails, Contact primaryContact, Contact secondaryContact, NBNGoRequestJsonToApex.LocationAddress siteDetails, Id RecordTypeId, Account rspAccount, String NBNGoUserEmail, String notifyEmail){
        Case caseRecord = new Case ();

        if(string.isNotBlank(caseDetails.dateofComplaintevent) && caseDetails.dateofComplaintevent.contains('T')){
            // format given -- 2016-07-11T14:00:01Z
            string dateTimeOfComplaint = caseDetails.dateofComplaintevent;
            if(dateTimeOfComplaint.containsIgnoreCase('Z')){
                dateTimeOfComplaint = dateTimeOfComplaint.Substring(0,dateTimeOfComplaint.length()-1);
                String[] splitDateAndTime = dateTimeOfComplaint.split('T');
                if(splitDateAndTime <> null){
                    // Separate the Time
                    String[] timeVal = splitDateAndTime[1].split(':');
                    // Separate the Date
                    String[] dateVal = splitDateAndTime[0].split('-');
                    // Set the Date and Time
                    Datetime GMTDate = Datetime.newInstanceGmt(Integer.valueOf(dateVal[0]), Integer.valueOf(dateVal[1]), Integer.valueOf(dateVal[2]),Integer.valueOf(timeVal[0]), Integer.valueOf(timeVal[1]), Integer.valueOf(timeVal[2]));
                    // Format to Local time zone
                    String strConvertedDate = GMTDate.format(GlobalConstants.DATE_FORMAT, GlobalConstants.SYDNEY_TIME_ZONE);
                    date mydate = date.parse(strConvertedDate);
                    caseRecord.Date_of_Complaint_Event__c = myDate;
                }
            }
        }
        
        caseRecord.RecordTypeId = RecordTypeId;
        caseRecord.Origin = 'App - nbn go';
        caseRecord.Priority = '3-General';
        caseRecord.OwnerId = UserInfo.getUserId();
        
        if(string.isNotBlank(NBNGoUserEmail)){
            caseRecord.nbn_go_App_User__c = NBNGoUserEmail;
        }
        
        if(string.isNotBlank(caseDetails.caseType)){
            caseRecord.nbn_go_Case_Type__c = caseDetails.caseType;
        }
        
        if(string.isNotBlank(notifyEmail)){
            if(notifyEmail.toLowerCase() == 'false'){
                caseRecord.Do_you_want_to_get_nbn_go_notifications__c = false;
            }else if(notifyEmail.toLowerCase() == 'true'){
                caseRecord.Do_you_want_to_get_nbn_go_notifications__c = true;
            }
        }

        if(string.isNotBlank(caseDetails.description)){
            caseRecord.description = caseDetails.description;
        }
        else{
            caseRecord.description = '';
        }
        // Case Type is Enquiry
        if(string.isNotBlank(caseDetails.desiredOutcome)){
            if(string.isNotBlank(caseRecord.description))
                caseRecord.description += '\r\n Desired Outcome : '+caseDetails.desiredOutcome;
            else
                caseRecord.description = 'Desired Outcome : '+caseDetails.desiredOutcome;
        }
        
        // Primary Contact
        if(primaryContact <> null && primaryContact.Id <> null){
            caseRecord.ContactId = primaryContact.id;
            caseRecord.Primary_Contact_Role__c = GlobalConstants.END_USER_CONSUMER_ROLE;
        }
        
        // Secondary Contact
        if(secondaryContact <> null && secondaryContact.Id <> null){
            caseRecord.Secondary_Contact__c = secondaryContact.id;
            caseRecord.Secondary_Contact_Role__c = GlobalConstants.END_USER_CONSUMER_ROLE;
        }
        
        // Assign Damage details to Case description
        if(caseDetails.whatDamaged != Null && string.isNotBlank(caseDetails.whatDamaged)){
            caseRecord.description = caseRecord.description + '\n\n Damaged item : ' + caseDetails.whatDamaged;
        }
        
        if(caseDetails.whereDamaged != Null && string.isNotBlank(caseDetails.whereDamaged)){
            caseRecord.description = caseRecord.description + '\n\n Damaged location : ' + caseDetails.whereDamaged;
        }
        
        if(caseDetails.whenDamaged != Null && string.isNotBlank(caseDetails.whenDamaged)){
            caseRecord.description = caseRecord.description + '\n\n Damaged date : ' + caseDetails.whenDamaged;
        }
        
        if(caseDetails.howDamaged != Null && string.isNotBlank(caseDetails.howDamaged)){
            caseRecord.description = caseRecord.description + '\n\n How Damaged : ' + caseDetails.howDamaged;
        }
        
        if(caseDetails.whoDamaged != Null && string.isNotBlank(caseDetails.whoDamaged)){
            caseRecord.description = caseRecord.description + '\n\n Damaged by : ' + caseDetails.whoDamaged;
        }   
      
        // Associate Site Address
        if(siteDetails <> null){
            caseRecord.Site_Address__c = '';
            if(string.isNotBlank(siteDetails.premisetype))
                caseRecord.Site_Address__c = siteDetails.premisetype+', ';
            if(string.isNotBlank(siteDetails.unitNumber))
                caseRecord.Site_Address__c += siteDetails.unitNumber+', ';
            if(string.isNotBlank(siteDetails.streetOrLotnumber))
                caseRecord.Site_Address__c += siteDetails.streetOrLotnumber+', ';
            if(string.isNotBlank(siteDetails.streetName))
                caseRecord.Site_Address__c += siteDetails.streetName+', ';
            if(string.isNotBlank(siteDetails.streetType))
                caseRecord.Site_Address__c += siteDetails.streetType+', ';
            if(string.isNotBlank(siteDetails.suburbOrlocality))
                caseRecord.Site_Address__c += siteDetails.suburbOrlocality+', ';
            if(string.isNotBlank(siteDetails.state))
                caseRecord.Site_Address__c += siteDetails.state+', ';
            if(string.isNotBlank(siteDetails.postCode))
                caseRecord.Site_Address__c += siteDetails.postCode+' ';
            if(string.isNotBlank(siteDetails.country))
                caseRecord.Site_Address__c += siteDetails.country;
        }

        system.debug('--NBNGo caseRecord--' + caseRecord);
        insert caseRecord;
        
        if(caseRecord.id <> null){
            Case createdCase = [SELECT Id, On_Demand_Case_Number__c, CaseNumber, ContactId, contact.FirstName, contact.LastName, contact.Preferred_Phone__c, contact.Alternate_Phone__c, contact.Email, Contact.Name, Contact.IsEmailBounced, createdDate, Website_Attachment_URL__c, Website_Attachment_Base_URL__c, RecordType.Name, RSP_ISP_Reference_Id__c, Date_of_Complaint_Event__c, Origin, Account.Name, Site_Address__c, Authorisation_for_Contact__c, RSP_ISP__c FROM Case WHERE Id =: caseRecord.id];
            return createdCase;
        }
        
        return caseRecord;
    }
    
    public static responseStatus generateResponse(
        string activityName,
        boolean success,
        string correlationId,
        string id,
        string code,
        string message,
        string timestamp){
            responseStatus responseToSendBack = new responseStatus();
            responseToSendBack.activityName = activityName;
            responseToSendBack.success = success;
            responseToSendBack.correlationId = correlationId;
            responseToSendBack.id = id;
            responseToSendBack.code = code;
            responseToSendBack.message = message;
            responseToSendBack.timestamp = timestamp;
            return responseToSendBack;
    }
    public static responseStatus generatePublicWebsiteResponse(
        string activityName,
        boolean success,
        string id,
        string code,
        string message,
        string sharePointURL,
        string sharePointBaseURL,
        string timestamp){
            responseStatus responseToSendBack = new responseStatus();
            responseToSendBack.activityName = activityName;
            responseToSendBack.success = success;
            if(String.isNotBlank(id))
                responseToSendBack.id = id;
            responseToSendBack.code = code;
            responseToSendBack.message = message;
            if(String.isNotBlank(sharePointURL))
                responseToSendBack.sharePointURL = sharePointURL;
            if(String.isNotBlank(sharePointBaseURL))
                responseToSendBack.sharePointBaseURL = sharePointBaseURL;
            responseToSendBack.timestamp = timestamp;
            return responseToSendBack;
    }
    public static responseStatus generateNBNGoResponse(
        string activityName,
        boolean success,
        string id,
        string code,
        string message,
        string timestamp){
            responseStatus responseToSendBack = new responseStatus();
            responseToSendBack.activityName = activityName;
            responseToSendBack.success = success;
            if(String.isNotBlank(id))
                responseToSendBack.id = id;
            responseToSendBack.code = code;
            responseToSendBack.message = message;
            responseToSendBack.timestamp = timestamp;
            return responseToSendBack;
    }
    
    global class responseStatus{
        public string activityName;
        public boolean success;
        public string correlationId;
        public string id;
        public string code;
        public string message;
        public string timestamp;
        public string sharePointURL;
        public string sharePointBaseURL;
    }
     
    public class NBNGoCaseInfo{
        public String CaseNumber {get;set;} 
        public String PrimaryContact {get;set;}
        public String CaseType {get;set;}
        public String Status {get;set;} 
        public String CreateDate {get;set;}
        public String ClosedDate {get;set;}
        public List<CaseRelatedActivities> CaseActivities {get; set;}
        
        public NBNGoCaseInfo(String casenumber, String name, String type, String status, String createddate, String closeddate, List<CaseRelatedActivities> lstCaseActivites){
            this.CaseNumber = casenumber;
            this.PrimaryContact = name;
            this.CaseType = type;
            this.Status = status;
            this.CreateDate = createddate;
            this.ClosedDate = closeddate;
            this.CaseActivities = lstCaseActivites;
        }       
    }
    
    public class GetNBNGoCaseInfo {
        public List<NBNGoCaseInfo> Cases {get;set;} 
        public GetNBNGoCaseInfo(List<NBNGoCaseInfo> lstCase){
            this.Cases = lstCase;
        }
    }
	
    public class CaseRelatedActivities{
        public String ActivityType {get;set;} 
        public String ActivityMessage {get;set;}
        public DateTime ActivityDate {get;set;} 
        
        public CaseRelatedActivities(String AT, String AM, DateTime AD){
            this.ActivityType = AT;
            this.ActivityMessage = AM;
            this.ActivityDate = AD;
        }       
    }
    
    public class NBNGoArticleInfo{
        public String Title {get;set;} 
        public String LastPublishedDate {get;set;}
        public String ArticleContent {get;set;} 
        
        public NBNGoArticleInfo(){}
        public NBNGoArticleInfo(String title, String summary, String publisheddate){
            this.Title = title;
            this.ArticleContent = summary;
            this.LastPublishedDate = publisheddate;
        }       
    }
    
    public static getResponseStatus generateGETNBNGoResponse(
        string activityName,
        boolean success,
        string id,
        string code,
        string message,
        string timestamp,
        GetNBNGoCaseInfo NBNGoCase){
            getResponseStatus responseToSendBack = new getResponseStatus();
            responseToSendBack.activityName = activityName;
            responseToSendBack.success = success;
            if(String.isNotBlank(id))
                responseToSendBack.id = id;
            responseToSendBack.code = code;
            responseToSendBack.message = message;
            responseToSendBack.timestamp = timestamp;
            if(NBNGoCase == null){
                responseToSendBack.getCaseInformation = new GetNBNGoCaseInfo(new List<NBNGoCaseInfo>());
            }else{
                responseToSendBack.getCaseInformation = NBNGoCase;
            }
            return responseToSendBack;
    }
    
    global class getResponseStatus{
        public string activityName;
        public boolean success;
        public string id;
        public string code;
        public string message;
        public string timestamp;
        public GetNBNGoCaseInfo getCaseInformation;
    }
    
    public static getArticleResponse generateArticleResponse(
        string activityName,
        boolean success,
        string id,
        string code,
        string message,
        string timestamp,
        NBNGoArticleInfo NBNGoarticle){
            getArticleResponse responseToSendBack = new getArticleResponse();
            responseToSendBack.activityName = activityName;
            responseToSendBack.success = success;
            if(String.isNotBlank(id))
                responseToSendBack.id = id;
            responseToSendBack.code = code;
            responseToSendBack.message = message;
            responseToSendBack.timestamp = timestamp;
            if(NBNGoarticle == null){
                responseToSendBack.articleDetails = new NBNGoArticleInfo();
            }else{
                responseToSendBack.articleDetails = NBNGoarticle;
            }
            return responseToSendBack;
    }
    
    global class getArticleResponse{
        public string activityName;
        public boolean success;
        public string id;
        public string code;
        public string message;
        public string timestamp;
        public NBNGoArticleInfo articleDetails;
    }
}