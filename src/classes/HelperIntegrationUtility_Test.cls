/***************************************************************************************************
Class Name:  HelperIntegrationUtility_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : This class contains unit test scenarios for  HelperIntegrationUtility apex class which handles integration business logic (Both Service Portal and Contact Us Form)
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       26/10/2015                 Created
****************************************************************************************************/@isTest
private class HelperIntegrationUtility_Test {
    /***************************************************************************************************
    Method Name:  getcurrentDateTime_Test
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void getcurrentDateTime_Test() {
        Test.startTest();
        HelperIntegrationUtility.getcurrentDateTime(null);
        HelperIntegrationUtility.getcurrentDateTime(system.now());
        Test.stopTest();
    }
    /***************************************************************************************************
    Method Name:  findCase_Test
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void findCase_Test() {
        Test.startTest();
        Contact conRec = new  Contact();
        conRec.RecordTypeId = GlobalCache.getRecordTypeId('Contact','External Contact');
        conRec.LastName = 'tn';
        conRec.FirstName = 'syedmoosanazir';
        conRec.email = '1syedmoosanazir@nbnco.com.au';
        conRec.Preferred_Phone__c = '0450297361';
        conRec.Alternate_Phone__c = '0450297367';
        insert conRec;
        Case caseRec = new Case ();
        caseRec.Contactid = conRec.Id;
        caseRec.On_Demand_Case_Number__c = '1234567890';
        insert caseRec;
        string SfOrCrmOdCaseNumber = '00000000';
        List<Case> listOfCase = [SELECT id, CaseNumber, On_Demand_Case_Number__c from Case where Id=:caseRec.Id];
        if(listOfCase<>null && !listOfCase.isEmpty())
            SfOrCrmOdCaseNumber = listOfCase.get(0).CaseNumber;
        HelperIntegrationUtility.findCase(null, null);
        HelperIntegrationUtility.findCase(SfOrCrmOdCaseNumber, conRec.Id);
        Test.stopTest();
    }
    /***************************************************************************************************
    Method Name:  findCaseByCaseNumber_Test
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void findCaseByCaseNumber_Test() {
        Test.startTest();
        Contact conRec = new  Contact();
        conRec.RecordTypeId = GlobalCache.getRecordTypeId('Contact','External Contact');
        conRec.LastName = 'tn';
        conRec.FirstName = 'syedmoosanazir';
        conRec.email = '1syedmoosanazir@nbnco.com.au';
        conRec.Preferred_Phone__c = '0450297361';
        conRec.Alternate_Phone__c = '0450297367';
        insert conRec;
        Case caseRec = new Case ();
        caseRec.Contactid = conRec.Id;
        caseRec.On_Demand_Case_Number__c = '1234567890';
        insert caseRec;
        string SfOrCrmOdCaseNumber = '00000000';
        List<Case> listOfCase = [SELECT id, CaseNumber, On_Demand_Case_Number__c from Case where Id=:caseRec.Id];
        if(listOfCase<>null && !listOfCase.isEmpty())
            SfOrCrmOdCaseNumber = listOfCase.get(0).CaseNumber;
        HelperIntegrationUtility.findCaseByCaseNumber(null);
        HelperIntegrationUtility.findCaseByCaseNumber(SfOrCrmOdCaseNumber);
        Test.stopTest();
    }
    /***************************************************************************************************
    Method Name:  findRspAccount_Test
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void findRspAccount_Test() {
        Test.startTest();
        Id RspRecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Retail Service Provider').getRecordTypeId();
        Account accountRecord = new Account ();
        accountRecord.Name = 'Test Account';
        accountRecord.recordTypeId = RspRecordTypeId;
        accountRecord.Access_Seeker_ID__c = 'ASI000000000035';
        insert accountRecord;
        HelperIntegrationUtility.findRspAccount(null);
        HelperIntegrationUtility.findRspAccount(accountRecord.Access_Seeker_ID__c);
        Test.stopTest();
    }
    /***************************************************************************************************
    Method Name:  findContact_Test
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void findContact_Test() {
        Test.startTest();
        Id ExtContRecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId();
        Contact conRec = new  Contact();
        conRec.LastName = 'tn';
        conRec.FirstName = 'syedmoosanazir';
        conRec.email = 'syedmoosanazir@nbnco.com.au';
        conRec.Preferred_Phone__c = '0450297367';
        conRec.Alternate_Phone__c = '0450297367';
        conRec.recordTypeId = ExtContRecordTypeId;
        insert conRec;
        HelperIntegrationUtility.findContact(null,null,null,null);
        HelperIntegrationUtility.findContact(conRec.FirstName,conRec.LastName,conRec.Preferred_Phone__c,conRec.email);
        conRec.email = '123syedmoosanazir@nbnco.com.au';
        conRec.Alternate_Phone__c = '1450297367';
        update conRec;
        HelperIntegrationUtility.findContact(conRec.FirstName,conRec.LastName,conRec.Preferred_Phone__c,'syedmoosanazir@nbnco.com.au');
        HelperIntegrationUtility.findContact(conRec.FirstName,conRec.LastName,'1450297367','syedmoosanazir@nbnco.com.au');
        Test.stopTest();
    }
    /***************************************************************************************************
    Method Name:  createContact_Test
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void createContact_Test() {
        Test.startTest();
        Id RspRecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Retail Service Provider').getRecordTypeId();
        Account accountRecord = new Account ();
        accountRecord.Name = 'Test Account';
        accountRecord.recordTypeId = RspRecordTypeId;
        accountRecord.Access_Seeker_ID__c = 'ASI000000000035';
        insert accountRecord;
        HelperIntegrationUtility.createContact('syedmoosanazir','tn','0450297367','syedmoosanazir@nbnco.com.au',true,null,'Preferred Phone');
        HelperIntegrationUtility.createContact('syed moosa nazir','t n','0450297367',null,false,accountRecord,'Phone');
        Test.stopTest();
    }
    /***************************************************************************************************
    Method Name:  createCase_Test
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void createCase_Test() {
        Test.startTest();
        String jsonInput = '{'+
            '\"createCaseRequestDetails\": {'+
                '\"businessChannel\": \"Portal\",'+
                '\"correlationId\": \"4eef4405-606e-46db-8c5c-af78e2874077\",'+
                '\"timeStamp\": \"2016-08-23T12:00:00Z\",'+
                '\"accessSeekerDetails\": {'+
                    '\"yourReferenceId \": \"1-8526607052\",'+
                    '\"accessSeekerId\": \"ASI000000000035\",'+
                    '\"organisationName\": \"Test Access Seeker\"'+
                '},'+
                '\"caseDetails\": {'+
                    '\"caseType\": \"Formal Complaint\",'+
                    '\"isTio\": \"Y\",'+
                    '\"tioReferenceId\": \"178378-8912894-X\",'+
                    '\"tioLevel\": \"TIO Level 1\"'+
                '},'+
                '\"contactAuthorisation\": {'+
                '\"contactInstructions\": \"Both Parties\"'+
                '},'+
                '\"locationAddress\": {'+
                    '\"premisetype\": \"Multi Dwelling Unit (MDU)\",'+
                    '\"unitNumber\": \"L5\",'+
                    '\"streetOrLotnumber\": \"50\",'+
                    '\"streetName\": \"MILLER\",'+
                    '\"streetType\": \"STREET\",'+
                    '\"suburbOrlocality\": \"NORTH SYDNEY\",'+
                    '\"state\": \"NSW\",'+
                    '\"postCode\": \"2600\",'+
                    '\"country\": \"Australia\"'+
                '},'+
                '\"contactDetails\": {'+
                    '\"caseOwner\": {'+
                        '\"firstName\": \"syedmoosanazir\",'+
                        '\"lastName\": \"tn\",'+
                        '\"phoneNumber\": \"0450297367\",'+
                        '\"emailAddress\": \"syedmoosanazir@nbnco.com.au\",'+
                        '\"preferredContactMethod\": \"Preferred Phone\"'+
                    '},'+
                    '\"caseRequester\": {'+
                        '\"firstName\": \"syed moosa nazir\",'+
                        '\"lastName\": \"t n\",'+
                        '\"phoneNumber\": \"0450297368\",'+
                        '\"emailAddress\": \"123syedmoosanazir@nbnco.com.au\"'+
                    '}'+
                '}'+
            '}'+
        '}';
        ExternalSystemRequestJsonToApex.CaseDetails caseDetails_Class = new ExternalSystemRequestJsonToApex.CaseDetails(System.JSON.createParser(jsonInput));
        List<ExternalSystemRequestJsonToApex.NbnReferenceIds> ListOfNbnReferenceIds = new List<ExternalSystemRequestJsonToApex.NbnReferenceIds> ();
        ExternalSystemRequestJsonToApex.NbnReferenceIds NbnReferenceIds_Class = new ExternalSystemRequestJsonToApex.NbnReferenceIds(System.JSON.createParser(jsonInput));
        NbnReferenceIds_Class.id = 'ORD123456789012';
        NbnReferenceIds_Class.type_Z = 'Order';
        ListOfNbnReferenceIds.add(NbnReferenceIds_Class);
        //CaseDetails_Class.caseType = 'Formal Complaint';
        CaseDetails_Class.caseType = 'LiFD';        
        CaseDetails_Class.isTio = 'Y';
        CaseDetails_Class.tioLevel = 'TIO Level 1';
        CaseDetails_Class.tioReferenceId = '178378-8912894-X';
        CaseDetails_Class.dateofComplaintevent = '2016-10-23T23:00:00Z';
        CaseDetails_Class.NbnReferenceIds = ListOfNbnReferenceIds;
        CaseDetails_Class.nbnServiceId = 'AVC901280915';
        CaseDetails_Class.description = 'test description';
        CaseDetails_Class.desiredOutcome = 'test desiredOutcome';
        ExternalSystemRequestJsonToApex.LocationAddress LocationAddress_Class = new ExternalSystemRequestJsonToApex.LocationAddress(System.JSON.createParser(jsonInput));
        LocationAddress_Class.country = 'Australia';
        LocationAddress_Class.postCode = '2600';
        LocationAddress_Class.premisetype = 'Multi Dwelling Unit (MDU)';
        LocationAddress_Class.state = 'NSW';
        LocationAddress_Class.streetName = 'MILLER';
        LocationAddress_Class.streetOrLotnumber = '50';
        LocationAddress_Class.streetType = 'STREET';
        LocationAddress_Class.suburbOrlocality = 'NORTH SYDNEY';
        LocationAddress_Class.unitNumber = 'L5';
        // Create RSP Account
        Id RspRecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Retail Service Provider').getRecordTypeId();
        Account accountRecord = new Account ();
        accountRecord.Name = 'Test Account';
        accountRecord.recordTypeId = RspRecordTypeId;
        accountRecord.Access_Seeker_ID__c = 'ASI000000000035';
        insert accountRecord;
        // Create Contact
        List<Contact> listOfContact = new List<Contact> ();
        Id ExtContRecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId();
        Contact conRec = new  Contact();
        conRec.accountId = accountRecord.id;
        conRec.LastName = 'tn';
        conRec.FirstName = 'syedmoosanazir';
        conRec.email = 'syedmoosanazir@nbnco.com.au';
        conRec.Preferred_Phone__c = '0450297367';
        conRec.Alternate_Phone__c = '0450297367';
        conRec.recordTypeId = ExtContRecordTypeId;
        listOfContact.add(conRec);
        Contact conRec2 = new  Contact();
        conRec2.LastName = 't n';
        conRec.FirstName = 'syed moosa nazir';
        conRec2.email = '123syedmoosanazir@nbnco.com.au';
        conRec2.Preferred_Phone__c = '0450297368';
        conRec2.Alternate_Phone__c = '0450297368';
        conRec2.recordTypeId = ExtContRecordTypeId;
        listOfContact.add(conRec2);
        insert listOfContact;
        //Id FromalCompRecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Formal Complaint').getRecordTypeId();       
        Id FromalCompRecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('LiFD').getRecordTypeId();       
        HelperIntegrationUtility.createCase(caseDetails_Class,listOfContact.get(0),listOfContact.get(1),'RSP/ISP First',LocationAddress_Class,'123456789',true,FromalCompRecordTypeId, accountRecord);
        HelperIntegrationUtility.createCase(caseDetails_Class,listOfContact.get(0),listOfContact.get(1),'RSP/ISP Only',LocationAddress_Class,'123456789',true,FromalCompRecordTypeId, accountRecord);
        HelperIntegrationUtility.createCase(caseDetails_Class,listOfContact.get(0),listOfContact.get(1),'End User Direct',LocationAddress_Class,'123456789',true,FromalCompRecordTypeId, accountRecord);
        Test.stopTest();
    }
    /***************************************************************************************************
    Method Name:  createCase_Test2
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void createCase_Test2() {
        Test.startTest();
        String jsonInput = '{'+
            '\"createCaseRequestDetails\": {'+
                '\"businessChannel\": \"Portal\",'+
                '\"correlationId\": \"4eef4405-606e-46db-8c5c-af78e2874077\",'+
                '\"timeStamp\": \"2016-08-23T12:00:00Z\",'+
                '\"accessSeekerDetails\": {'+
                    '\"yourReferenceId \": \"1-8526607052\",'+
                    '\"accessSeekerId\": \"ASI000000000035\",'+
                    '\"organisationName\": \"Test Access Seeker\"'+
                '},'+
                '\"caseDetails\": {'+
                    '\"caseType\": \"Formal Complaint\",'+
                    '\"isTio\": \"Y\",'+
                    '\"tioReferenceId\": \"178378-8912894-X\",'+
                    '\"tioLevel\": \"TIO Level 1\"'+
                '},'+
                '\"contactAuthorisation\": {'+
                '\"contactInstructions\": \"Both Parties\"'+
                '},'+
                '\"locationAddress\": {'+
                    '\"premisetype\": \"Multi Dwelling Unit (MDU)\",'+
                    '\"unitNumber\": \"L5\",'+
                    '\"streetOrLotnumber\": \"50\",'+
                    '\"streetName\": \"MILLER\",'+
                    '\"streetType\": \"STREET\",'+
                    '\"suburbOrlocality\": \"NORTH SYDNEY\",'+
                    '\"state\": \"NSW\",'+
                    '\"postCode\": \"2600\",'+
                    '\"country\": \"Australia\"'+
                '},'+
                '\"contactDetails\": {'+
                    '\"caseOwner\": {'+
                        '\"firstName\": \"syedmoosanazir\",'+
                        '\"lastName\": \"tn\",'+
                        '\"phoneNumber\": \"0450297367\",'+
                        '\"emailAddress\": \"syedmoosanazir@nbnco.com.au\",'+
                        '\"preferredContactMethod\": \"Preferred Phone\"'+
                    '},'+
                    '\"caseRequester\": {'+
                        '\"firstName\": \"syed moosa nazir\",'+
                        '\"lastName\": \"t n\",'+
                        '\"phoneNumber\": \"0450297368\",'+
                        '\"emailAddress\": \"123syedmoosanazir@nbnco.com.au\"'+
                    '}'+
                '}'+
            '}'+
        '}';
        ExternalSystemRequestJsonToApex.CaseDetails caseDetails_Class = new ExternalSystemRequestJsonToApex.CaseDetails(System.JSON.createParser(jsonInput));
        List<ExternalSystemRequestJsonToApex.NbnReferenceIds> ListOfNbnReferenceIds = new List<ExternalSystemRequestJsonToApex.NbnReferenceIds> ();
        ExternalSystemRequestJsonToApex.NbnReferenceIds NbnReferenceIds_Class = new ExternalSystemRequestJsonToApex.NbnReferenceIds(System.JSON.createParser(jsonInput));
        NbnReferenceIds_Class.id = 'ORD123456789012';
        NbnReferenceIds_Class.type_Z = 'Order';
        ListOfNbnReferenceIds.add(NbnReferenceIds_Class);
       // CaseDetails_Class.caseType = 'Formal Complaint';
        CaseDetails_Class.caseType = 'LiFD';
          
        CaseDetails_Class.isTio = 'Y';
        CaseDetails_Class.tioLevel = 'TIO Level 1';
        CaseDetails_Class.tioReferenceId = '178378-8912894-X';
        CaseDetails_Class.dateofComplaintevent = '2016-10-23T23:00:00Z';
        CaseDetails_Class.NbnReferenceIds = ListOfNbnReferenceIds;
        CaseDetails_Class.nbnServiceId = 'AVC901280915';
        CaseDetails_Class.description = 'test description';
        CaseDetails_Class.desiredOutcome = 'test desiredOutcome';
        ExternalSystemRequestJsonToApex.LocationAddress LocationAddress_Class = new ExternalSystemRequestJsonToApex.LocationAddress(System.JSON.createParser(jsonInput));
        LocationAddress_Class.country = 'Australia';
        LocationAddress_Class.postCode = '2600';
        LocationAddress_Class.premisetype = 'Multi Dwelling Unit (MDU)';
        LocationAddress_Class.state = 'NSW';
        LocationAddress_Class.streetName = 'MILLER';
        LocationAddress_Class.streetOrLotnumber = '50';
        LocationAddress_Class.streetType = 'STREET';
        LocationAddress_Class.suburbOrlocality = 'NORTH SYDNEY';
        LocationAddress_Class.unitNumber = 'L5';
        // Create RSP Account
        Id RspRecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Retail Service Provider').getRecordTypeId();
        Account accountRecord = new Account ();
        accountRecord.Name = 'Test Account';
        accountRecord.recordTypeId = RspRecordTypeId;
        accountRecord.Access_Seeker_ID__c = 'ASI000000000035';
        insert accountRecord;
        // Create Contact
        List<Contact> listOfContact = new List<Contact> ();
        Id ExtContRecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId();
        Contact conRec = new  Contact();
        conRec.accountId = accountRecord.id;
        conRec.LastName = 'tn';
        conRec.FirstName = 'syedmoosanazir';
        conRec.email = 'syedmoosanazir@nbnco.com.au';
        conRec.Preferred_Phone__c = '0450297367';
        conRec.Alternate_Phone__c = '0450297367';
        conRec.recordTypeId = ExtContRecordTypeId;
        listOfContact.add(conRec);
        Contact conRec2 = new  Contact();
        conRec2.LastName = 't n';
        conRec.FirstName = 'syed moosa nazir';
        conRec2.email = '123syedmoosanazir@nbnco.com.au';
        conRec2.Preferred_Phone__c = '0450297368';
        conRec2.Alternate_Phone__c = '0450297368';
        conRec2.recordTypeId = ExtContRecordTypeId;
        listOfContact.add(conRec2);
        insert listOfContact;
//        Id FromalCompRecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Formal Complaint').getRecordTypeId();       
          Id FromalCompRecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('LiFD').getRecordTypeId();       
        
        HelperIntegrationUtility.createCase(caseDetails_Class,listOfContact.get(0),listOfContact.get(1),'Both Parties',LocationAddress_Class,'123456789',true,FromalCompRecordTypeId, accountRecord);
        HelperIntegrationUtility.createCase(caseDetails_Class,listOfContact.get(0),listOfContact.get(1),'Both Parties',LocationAddress_Class,'123456789',false,FromalCompRecordTypeId, accountRecord);
        Test.stopTest();
    }
    /***************************************************************************************************
    Method Name:  generateResponse_Test
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void generateResponse_Test() {
        HelperIntegrationUtility.generateResponse('activityName',true,'correlationId','id','code','message','timestamp');
    }
    /***************************************************************************************************
    Method Name:  generatePublicWebsiteResponse_Test
    Method Type: testmethod
    Version     : 1.0 
    Created Date: 26/10/2015
    Description:  
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       26/10/2015                Created
    ****************************************************************************************************/
    static testMethod void generatePublicWebsiteResponse_Test() {
        HelperIntegrationUtility.generatePublicWebsiteResponse('activityName',true,'id','code','message','sharePointURL','sharePointBaseURL','timestamp');
    }
}