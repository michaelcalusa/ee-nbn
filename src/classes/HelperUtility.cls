/***************************************************************************************************
Class Name:  HelperUtility
Class Type: Test Class 
Version     : 1.0 
Created Date: 02-11-2016
Function    : This class contains utility methods for Case and Site objects
Used in     : Site Qualification, Search Wizard. 
Modification Log :
* Developer  Ganesh Sawant    Date   25-08-2017
	Description - Nullify Existing address fields before Requalification from CIS and Fix for 
                  avoiding Conversion of MDU/CP Site to Verified recordType
* Developer Ganesh Sawant     Date 31-01-2018
    Description - Fixing the mapping of CIS data for Road Type and Road Suffix field
                  MSEU - 9003 & MSEU - 4338                  
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN         02-11-2016                Created
****************************************************************************************************/

public class HelperUtility{
    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName){
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
        if(ObjectName <> null && !String.isEmpty(ObjectName)){
            Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
            if(SObjectTypeObj <> null){
                Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
                if(fieldSetName <> null &&  !String.isEmpty(fieldSetName)){
                    Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
                    if(fieldSetObj <> null){
                        system.debug('Field Information===> '+fieldSetObj.getFields());
                        return fieldSetObj.getFields();
                    }
                    else{return null;}
                }
                else{return null;}
            }
            else{return null;}
        }
        else{return null;}
    }
    public static List<Profile> getProfile(List<Id> listOfProfileId){
        List<Profile> profileRec = [select id, name from Profile where ID IN: listOfProfileId];
        return profileRec;
    }
    
    public static List<Profile> getProfileByName(List<String> listOfProfileName){
        List<Profile> profileRec = [select id, name from Profile where Name IN: listOfProfileName];
        return profileRec;
    }
    
    public static List<Profile> getProfileByName(String profileName){
        List<Profile> profileRec = [select id, name from Profile where Name=:profileName];
        return profileRec;
    }
    public static List<PermissionSet> getPermissionSetByName(List<String> listOfPermissionSetName){
        List<PermissionSet> permissionSetRec = [select id, name from PermissionSet where Name IN: listOfPermissionSetName];
        return permissionSetRec ;
    }
    
    public static String generateQueryFromFieldSets(
    String sObjectName,
    List<Schema.FieldSetMember> fieldSetMemberList,
    String whereClause){
        string queryString = 'SELECT Id';
        for(Schema.FieldSetMember fld :fieldSetMemberList) {
            queryString += ', ' + fld.getFieldPath();
        }
        queryString = (whereClause == '' || whereClause ==null)   
         ? (queryString + ' FROM '+sObjectName)   
         : (queryString + ' FROM '+sObjectName + ' WHERE ' + whereClause);  
        return queryString;
    }
    public static string getErrorMessage(string errorCode){
        string DefaultErrorMessage = 'Error Occurred';
        if(Error_Message__c.getinstance(errorCode)!=null){
            if(Error_Message__c.getinstance(errorCode).Error_Message__c!=null){
                DefaultErrorMessage =Error_Message__c.getinstance(errorCode).Error_Message__c; 
            }
        }
        return DefaultErrorMessage;
    }
    /*  The following characters are reserved:
        ? & | ! { } [ ] ( ) ^ ~ * : \ " ' + -
        Reference : https://developer.salesforce.com/docs/atlas.en-us.soql_sosl.meta/soql_sosl/sforce_api_calls_sosl_find.htm
    */
    public static string reservedCharacterHandler(string unHandledQueryParam){
        string HandledQueryParam;
        if(unHandledQueryParam != null){
            String[] unHandledQueryCharacterArray = unHandledQueryParam.split('');
            for(string character : unHandledQueryCharacterArray){
                if(character.equalsIgnoreCase('?')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('&')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('|')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('!')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('{')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('}')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('[')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase(']')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('(')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase(')')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('^')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('~')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('*')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase(':')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('\\')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('"')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('\'')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('+')){
                HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else if(character.equalsIgnoreCase('-')){
                    HandledQueryParam = formHandledQueryParam(HandledQueryParam,character);
                }
                else{
                    if(HandledQueryParam == null){
                        HandledQueryParam = character;
                    }
                    else{
                        HandledQueryParam += character;
                    }
                }
            }
        }
        return HandledQueryParam;
    }
    public static string formHandledQueryParam(string unHandledQueryParam, string character){
        if(unHandledQueryParam == null){
            unHandledQueryParam = '\\'+character;
        }
        else{
            unHandledQueryParam += '\\'+character;
        }
        return unHandledQueryParam;
    }
    
    public Static map<String, Id> pullAllRecordTypes(String objectAPIName)
    {
        Map<String, Id> mapofCaseRecordTypeNameandId = new Map<String,Id>();
        Schema.DescribeSObjectResult sobjectResult = Schema.getGlobalDescribe().get(objectAPIName).getDescribe();
        List<Schema.RecordTypeInfo> recordTypeInfo = sobjectResult.getRecordTypeInfos();
        mapofCaseRecordTypeNameandId = new Map<String,Id>();
        for(Schema.RecordTypeInfo info : recordTypeInfo){
             mapofCaseRecordTypeNameandId.put(info.getName(),info.getRecordTypeId());
        }       
        return mapofCaseRecordTypeNameandId;
    }
    
    public Static Boolean checkNullValue(String strValue)
    {
        if(!String.isBlank(strValue)){
            return true;
        }
        return false; 
    }
    public static Id createUnStructuredVerifiedSite (AddressSearch_CX.addressWrapper selectedAddress){
        Id siteRecordId;
        if(selectedAddress != null){
            if(!String.isBlank(selectedAddress.sfRecordId)){
                siteRecordId = selectedAddress.sfRecordId;               
            }
            else if(!String.isBlank(selectedAddress.locationId)){
                List<Site__c> siteRecords = [SELECT Id, Name, Site_Address__c, isSiteCreationThroughCode__c, Location_Id__c, Asset_Number__c FROM site__c WHERE Location_Id__c =: selectedAddress.locationId LIMIT 1];
                if(siteRecords <> null && !siteRecords.isEmpty()){
                    siteRecordId = siteRecords.get(0).Id;                    
                }
            }
            if(siteRecordId == null){
                 
                Map<String, Id> mapOfSiteRecordTypeNamesAndId = new Map<String, Id>();
                mapOfSiteRecordTypeNamesAndId = HelperUtility.pullAllRecordTypes('site__c');
                Site__c siteRecord = new Site__c();
                siteRecord.isSiteCreationThroughCode__c = true;
                siteRecord.Location_Id__c = selectedAddress.locationId;
                siteRecord.Site_Address__c = selectedAddress.address;
                siteRecord.Name = truncateString(selectedAddress.address, 79);
                siteRecord.recordTypeId = mapOfSiteRecordTypeNamesAndId.get('Verified');
                AddressSearch_CX.sWrapper qDtls = new AddressSearch_CX.sWrapper();
                qDtls = AddressSearch_CX.getQualificationDetailsfromExtSystem(siteRecord.Location_Id__c);
                if(qDtls!=null){
                    siteRecord.Technology_Type__c =  qDtls.technologyType;
                    siteRecord.Serviceability_Class__c = Integer.valueof(qDtls.serviceClass);
                    siteRecord.Rollout_Type__c = qDtls.rolloutType;
                    if(qDtls.isSiteAvailable <> null && qDtls.isSiteAvailable)
                        siteRecord.Qualification_Date__c = qDtls.qualificationDate;
                    insert siteRecord;
                    siteRecordId = siteRecord.Id;
                } 
            }            
        }
        return siteRecordId;
    }
    public static Id createStructuredUnVerifiedSite (AddressSearch_CX.StructuredAddress selectedAddress){
        Id siteRecordId;        
        if(selectedAddress != null){
            Map<String, Id> mapOfSiteRecordTypeNamesAndId = new Map<String, Id>();
            mapOfSiteRecordTypeNamesAndId = HelperUtility.pullAllRecordTypes('site__c');
            Site__c siteRecord = new Site__c();
            if(string.isNotEmpty(selectedAddress.unitNumber))
                siteRecord.Unit_Number__c = selectedAddress.unitNumber;
            if(string.isNotEmpty(selectedAddress.levelNumber))
                siteRecord.Level_Number__c = selectedAddress.levelNumber;
            if(string.isNotEmpty(selectedAddress.lotNumber))
                siteRecord.Lot_Number__c = selectedAddress.lotNumber;
            if(string.isNotEmpty(selectedAddress.roadNumber1))
                siteRecord.Road_Number_1__c = selectedAddress.roadNumber1;
            if(string.isNotEmpty(selectedAddress.roadName))
                siteRecord.Road_Name__c = selectedAddress.roadName;
            if(string.isNotEmpty(selectedAddress.roadSuffixCode))
                siteRecord.Road_Suffix_Code__c = selectedAddress.roadSuffixCode;
            if(string.isNotEmpty(selectedAddress.localityName))
                siteRecord.Locality_Name__c = selectedAddress.localityName;
            if(string.isNotEmpty(selectedAddress.postCode))
                siteRecord.Post_Code__c = selectedAddress.postCode;
                siteRecord.State_Territory_Code__c = selectedAddress.stateTerritoryCode;
            siteRecord.recordTypeId = mapOfSiteRecordTypeNamesAndId.get('Unverified');
            // Form the unstructured Address
            siteRecord.Site_Address__c = '';
            if(string.isNotEmpty(selectedAddress.lotNumber))  
                siteRecord.Site_Address__c += selectedAddress.lotNumber + ' ';
            if(string.isNotEmpty(selectedAddress.levelNumber))  
                siteRecord.Site_Address__c += selectedAddress.levelNumber + ' ';
            if(string.isNotEmpty(selectedAddress.unitNumber))   
                siteRecord.Site_Address__c += selectedAddress.unitNumber + ' ';
            if(string.isNotEmpty(selectedAddress.roadName)) 
                siteRecord.Site_Address__c += selectedAddress.roadName + ' ';
            if(string.isNotEmpty(selectedAddress.roadNumber1)) 
                siteRecord.Road_Number_1__c = selectedAddress.roadNumber1;
            if(string.isNotEmpty(selectedAddress.roadSuffixCode))   
                siteRecord.Site_Address__c += selectedAddress.roadSuffixCode + ', ';
            if(string.isNotEmpty(selectedAddress.localityName)) 
                siteRecord.Site_Address__c += selectedAddress.localityName + ', ';
            if(string.isNotEmpty(selectedAddress.stateTerritoryCode))   
                siteRecord.Site_Address__c += selectedAddress.stateTerritoryCode + ' ';
            if(string.isNotEmpty(selectedAddress.postCode)) 
                siteRecord.Site_Address__c += selectedAddress.postCode;
            siteRecord.isSiteCreationThroughCode__c = true;
            insert siteRecord;
            siteRecordId = siteRecord.Id;
        }
        return siteRecordId;
    }
    public static Site__c instantiateUnStructuredVerifiedSite (AddressSearch_CX.addressWrapper selectedAddress){
        //Id siteRecordId;
        boolean isSiteAvailableInSF = false;
        Site__c siteRecord = new Site__c();
        if(selectedAddress != null){
            system.debug('selectedAddress......'+selectedAddress);
            system.debug('selectedAddress......'+selectedAddress.sfRecordId);
            if(String.isNotEmpty(selectedAddress.sfRecordId)){
                List<Site__c> siteRecords = [SELECT Id, isSiteCreationThroughCode__c, Name, Site_Address__c, Technology_Type__c, Serviceability_Class__c, recordtype.Name, recordtypeId, Rollout_Type__c, Location_Id__c, Asset_Number__c,Unit_Number__c,Level_Number__c,Post_Code__c,Locality_Name__c,Road_Type_Code__c,Road_Name__c,Road_Number_1__c,Unit_Type_Code__c FROM site__c WHERE id =: selectedAddress.sfRecordId LIMIT 1];
                if(siteRecords <> null && !siteRecords.isEmpty()){
                    siteRecord = siteRecords.get(0);
                    // sukumar 29.11.2016
                    if(String.isNotEmpty(selectedAddress.roadNumber1))
                        siteRecord.Road_Number_1__c = selectedAddress.roadNumber1;
                    if(String.isNotEmpty(selectedAddress.roadName))
                        siteRecord.Road_Name__c = selectedAddress.roadName;
                    if(String.isNotEmpty(selectedAddress.roadTypeCode)) 
                        siteRecord.Road_Type_Code__c = selectedAddress.roadTypeCode;
                    if(String.isNotEmpty(selectedAddress.locality))
                        siteRecord.Locality_Name__c = selectedAddress.locality;
                    if(String.isNotEmpty(selectedAddress.postcode))
                        siteRecord.Post_Code__c = selectedAddress.postcode;
                    if(String.isNotEmpty(selectedAddress.state))
                        siteRecord.State_Territory_Code__c = selectedAddress.state;
                    if(String.isNotEmpty(selectedAddress.lotnumber))
                        siteRecord.Lot_Number__c = selectedAddress.lotnumber;
                    // sukumar 29.11.2016 END   
                    system.debug('By SF ID in SF siteRecord===>'+siteRecord);
                    isSiteAvailableInSF = true;
                    if(String.isNotEmpty(selectedAddress.locationId)){
                        siteRecord.Location_Id__c = selectedAddress.locationId; // sukumar 29.11.2016
                        siteRecord = executeServiceQualification(siteRecord, selectedAddress.locationId);
                    }
                    system.debug('siteRecord===>'+siteRecord);
                    return siteRecord;
                }
            }
            else if(String.isNotEmpty(selectedAddress.locationId)){
              List<Site__c> siteRecords = [SELECT Id, isSiteCreationThroughCode__c, Name, Site_Address__c, Technology_Type__c, 
              Serviceability_Class__c, recordtype.Name,recordtypeId, Rollout_Type__c, Location_Id__c, Asset_Number__c,Unit_Number__c,Level_Number__c,
              Post_Code__c,Locality_Name__c,Road_Type_Code__c,Road_Name__c,Road_Number_1__c,Unit_Type_Code__c FROM site__c 
              WHERE Location_Id__c =: selectedAddress.locationId AND (recordtype.name =: 'Unverified' OR recordtype.name =: 'Verified') LIMIT 1];
               if(siteRecords <> null && !siteRecords.isEmpty()){
                    siteRecord = siteRecords.get(0);
                    // sukumar 29.11.2016
                    if(String.isNotEmpty(selectedAddress.roadNumber1))
                        siteRecord.Road_Number_1__c = selectedAddress.roadNumber1;
                    if(String.isNotEmpty(selectedAddress.roadName))
                        siteRecord.Road_Name__c = selectedAddress.roadName;
                    if(String.isNotEmpty(selectedAddress.roadTypeCode)) 
                        siteRecord.Road_Type_Code__c = selectedAddress.roadTypeCode;
                    if(String.isNotEmpty(selectedAddress.locality))
                        siteRecord.Locality_Name__c = selectedAddress.locality;
                    if(String.isNotEmpty(selectedAddress.postcode))
                        siteRecord.Post_Code__c = selectedAddress.postcode;
                    if(String.isNotEmpty(selectedAddress.state))
                        siteRecord.State_Territory_Code__c = selectedAddress.state;
                    if(String.isNotEmpty(selectedAddress.lotnumber))
                        siteRecord.Lot_Number__c = selectedAddress.lotnumber;
                    // sukumar 29.11.2016 - END
                    system.debug('By Loc ID in SF siteRecord===>'+siteRecord);
                    isSiteAvailableInSF = true;
                    if(String.isNotEmpty(selectedAddress.locationId)){
                        siteRecord.Location_Id__c = selectedAddress.locationId; // sukumar 29.11.2016
                       siteRecord = executeServiceQualification(siteRecord, selectedAddress.locationId);
                    }
                    system.debug('siteRecord===>'+siteRecord);
                    return siteRecord;                    
                }
            }
            if(!isSiteAvailableInSF && String.isNotEmpty(selectedAddress.locationId)){ 
                Map<String, Id> mapOfSiteRecordTypeNamesAndId = new Map<String, Id>();
                mapOfSiteRecordTypeNamesAndId = HelperUtility.pullAllRecordTypes('site__c');
                    // sukumar 29.11.2016
                    siteRecord.Location_Id__c = selectedAddress.locationId;
                    siteRecord.Site_Address__c = selectedAddress.address;
                    siteRecord.Name = truncateString(selectedAddress.address, 79);
                    siteRecord.isSiteCreationThroughCode__c = true;
                    siteRecord.recordtypeId = mapOfSiteRecordTypeNamesAndId.get('Verified');
                    if(String.isNotEmpty(selectedAddress.roadNumber1))
                        siteRecord.Road_Number_1__c = selectedAddress.roadNumber1;
                    if(String.isNotEmpty(selectedAddress.roadName))
                        siteRecord.Road_Name__c = selectedAddress.roadName;
                    if(String.isNotEmpty(selectedAddress.roadTypeCode)) 
                        siteRecord.Road_Type_Code__c = selectedAddress.roadTypeCode;
                    if(String.isNotEmpty(selectedAddress.locality))
                        siteRecord.Locality_Name__c = selectedAddress.locality;
                    if(String.isNotEmpty(selectedAddress.postcode))
                        siteRecord.Post_Code__c = selectedAddress.postcode;
                    if(String.isNotEmpty(selectedAddress.state))
                        siteRecord.State_Territory_Code__c = selectedAddress.state;
                    if(String.isNotEmpty(selectedAddress.lotnumber))
                        siteRecord.Lot_Number__c = selectedAddress.lotnumber;
                    // sukumar 29.11.2016 END
                    //siteRecord.recordtype.Name = 'Verified';
                    system.debug('siteRecord===>'+siteRecord);
                    if(String.isNotEmpty(selectedAddress.locationId)){
                       siteRecord = executeServiceQualification(siteRecord, selectedAddress.locationId);
                    }
                    system.debug('siteRecord===>'+siteRecord);
                    return siteRecord;
            }           
        }
        return siteRecord;
    }
    public static Site__c executeServiceQualification(site__c siteRecord, string locationId){
        system.debug('executeServiceQualification.locationId==>'+locationId);
        //siteRecord = new site__c();
        if(String.isNotEmpty(locationId)){
            Map<String, Id> mapOfSiteRecordTypeNamesAndId = new Map<String, Id>();
            mapOfSiteRecordTypeNamesAndId = HelperUtility.pullAllRecordTypes('site__c');
            AddressSearch_CX.sWrapper qDtls = new AddressSearch_CX.sWrapper();
            AddressSearch_CX.boundaryWrapper bDtls = new AddressSearch_CX.boundaryWrapper();
            qDtls = AddressSearch_CX.getQualificationDetailsfromExtSystem(locationId);
            system.debug('qDtls..................'+qDtls);
            if(qDtls!=null){
                //system.debug('siteRecord.....before1.............'+siteRecord);
                // Address fields Mapping
                if(qDtls.unstructured  <> null && qDtls.unstructured  <> ''){
                    siteRecord.Site_Address__c = qDtls.unstructured ;
                    siteRecord.Name = truncateString(qDtls.unstructured,79);
                    siteRecord.isSiteCreationThroughCode__c = true;
                }
                /******BAU Story 4040 blank out the existing values before requalify if not null
					   Changed By Ganesh Sawant Dated 25th Aug 2017*****/   
                	siteRecord.Level_Number__c = null;    
                	siteRecord.Level_Type_Code__c = null;
                	siteRecord.Unit_Type_Code__c = null;
                	siteRecord.Unit_Number__c = null;
                	siteRecord.Lot_Number__c = null;
                	siteRecord.Plan_Number__c = null;
                	siteRecord.Road_Number_1__c = null;
                	siteRecord.Road_Number_2__c = null;
                	siteRecord.Road_Type_Code__c = null;
                	siteRecord.Road_Suffix_Code__c = null;    
                	siteRecord.Locality_Name__c = null; 
                	siteRecord.State_Territory_Code__c = null;                
                	siteRecord.Complex_Road_Number_1__c = null;
                	siteRecord.Complex_Road_Number_2__c = null;
                	siteRecord.Complex_Road_Name__c = null;
                	siteRecord.Complex_Road_Type_Code__c = null;
                	siteRecord.Complex_Road_Suffix_Code__c = null;
               /*************************End of Change*********************/ 	                	                	                	                	                	                	            	            	                
                if(qDtls.siteName <> null && qDtls.siteName <> '')
                    siteRecord.Site_Name__c =  qDtls.siteName;
                if(qDtls.locationDescriptor <> null && qDtls.locationDescriptor <> '')
                    siteRecord.Description__c =  qDtls.locationDescriptor;                      
                if(qDtls.levelType <> null && qDtls.levelType <> '')
                    siteRecord.Level_Type_Code__c =  qDtls.levelType;
                if(qDtls.levelNumber <> null && qDtls.levelNumber <> '')
                    siteRecord.Level_Number__c = qDtls.levelNumber;
                if(qDtls.unitType <> null && qDtls.unitType <> '')
                    siteRecord.Unit_Type_Code__c = qDtls.unitType;  
                if(qDtls.unitNumber <> null && qDtls.unitNumber <> '')
                    siteRecord.Unit_Number__c = qDtls.unitNumber;   
                if(qDtls.lotNumber <> null && qDtls.lotNumber <> '')
                    siteRecord.Lot_Number__c = qDtls.lotNumber; 
                if(qDtls.planNumber <> null && qDtls.planNumber <> '')
                    siteRecord.Plan_Number__c = qDtls.planNumber;   
                if(qDtls.roadNumber1 <> null && qDtls.roadNumber1 <> '')
                    siteRecord.Road_Number_1__c = qDtls.roadNumber1;    
                if(qDtls.roadNumber2 <> null && qDtls.roadNumber2 <> '')
                    siteRecord.Road_Number_2__c = qDtls.roadNumber2;
                if(qDtls.roadName <> null && qDtls.roadName <> '')
                    siteRecord.Road_Name__c = qDtls.roadName;
            /**********************************************************************************
             * Site Data Cleanup Process mapping the CIS roadType value to RoadSuffix picklist
             * In salesforce and mapping CIS roadSuffix value to Road Type Text field
             * MSEU - 9003 & MSEU - 4338
             **********************************************************************************/        
                /*if(qDtls.roadTypeCode <> null && qDtls.roadTypeCode <> '')
                    siteRecord.Road_Type_Code__c = qDtls.roadTypeCode;
                if(qDtls.roadSuffixCode <> null && qDtls.roadSuffixCode <> '')
                    siteRecord.Road_Suffix_Code__c = qDtls.roadSuffixCode;*/
                if(qDtls.roadTypeCode <> null && qDtls.roadTypeCode <> '')
                    siteRecord.Road_Suffix_Code__c = qDtls.roadTypeCode;
                if(qDtls.roadSuffixCode <> null && qDtls.roadSuffixCode <> '')
                    siteRecord.Road_Type_Code__c = qDtls.roadSuffixCode;                              
            /*******************************End Of Change*************************************/        
                if(qDtls.locality <> null && qDtls.locality <> '')
                    siteRecord.Locality_Name__c = qDtls.locality;
                if(qDtls.postCode <> null && qDtls.postCode <> '')
                    siteRecord.Post_Code__c = qDtls.postCode;
                if(qDtls.state <> null && qDtls.state <> '')
                    siteRecord.State_Territory_Code__c = qDtls.state;
                 // Complex Address fields Mapping
                 // Complex Address Site Name needs to be mapped
                if(qDtls.complexAddress_roadNumber1 <> null && qDtls.complexAddress_roadNumber1 <> '')
                    siteRecord.Complex_Road_Number_1__c = qDtls.complexAddress_roadNumber1; 
                if(qDtls.complexAddress_roadNumber2 <> null && qDtls.complexAddress_roadNumber2 <> '')
                    siteRecord.Complex_Road_Number_2__c = qDtls.complexAddress_roadNumber2; 
                if(qDtls.complexAddress_roadName <> null && qDtls.complexAddress_roadName <> '')
                    siteRecord.Complex_Road_Name__c = qDtls.complexAddress_roadName; 
                if(qDtls.complexAddress_roadTypeCode <> null && qDtls.complexAddress_roadTypeCode <> '')
                    siteRecord.Complex_Road_Type_Code__c = qDtls.complexAddress_roadTypeCode;
                if(qDtls.complexAddress_roadSuffixCode <> null && qDtls.complexAddress_roadSuffixCode <> '')
                    siteRecord.Complex_Road_Suffix_Code__c = qDtls.complexAddress_roadSuffixCode;
                // Geo Location fields mapping
                if(qDtls.latitude  <> null && qDtls.latitude  <> '')
                    siteRecord.Latitude__c = qDtls.latitude ;
                if(qDtls.longitude  <> null && qDtls.longitude  <> '')
                    siteRecord.Longitude__c = qDtls.longitude ;
                // Gerographinc Datum field needs to be mapped
                // srid field needs to be mapped
                
                // Primary Access Technology field mapping (Qualification details)  
                if(qDtls.technologyType <> null && qDtls.technologyType <> '')
                    siteRecord.Technology_Type__c =  qDtls.technologyType;
                if(qDtls.serviceClass <> null)
                    siteRecord.Serviceability_Class__c = Integer.valueof(qDtls.serviceClass);
                if(qDtls.rolloutType <> null && qDtls.rolloutType <> '')
                    siteRecord.Rollout_Type__c = qDtls.rolloutType;
                // Relationship field Mappings
                system.debug('**** qDtls.bWrapper ==>'+ qDtls.bWrapper);
                if(qDtls.bWrapper <> null)
                 {
                    for(AddressSearch_CX.boundaryWrapper b: qDtls.bWrapper)
                    {
                       system.debug('*** b.containmentBoundary_type'+b.containmentBoundary_type);
                       if(b.containmentBoundary_boundaryType == 'FSA')
                            siteRecord.FSA__c = b.containmentBoundary_id;
                        if(b.containmentBoundary_boundaryType == 'SAM')
                            siteRecord.SAM__c = b.containmentBoundary_id;
                        if(b.containmentBoundary_boundaryType == 'MPS')
                            siteRecord.MPS__c = b.containmentBoundary_id;
                    }
                 }               
                 // MDU  Field mappings
                if(qDtls.MDU_id <> null && qDtls.MDU_id <> '')
                    siteRecord.MDU_LocId__c = qDtls.MDU_id; 
                 // MDU type field mapping needs to be done.
                 
                // Map Other attributes
                if(locationId <> null && locationId <> '')
                    siteRecord.Location_Id__c = locationId; 
                if(qDtls.dwellingType <> null && qDtls.dwellingType <> '')
                    siteRecord.Premises_Type__c = qDtls.dwellingType ; 
                if(qDtls.landUse <> null && qDtls.landUse <> '')
                    siteRecord.Land_Use__c = qDtls.landUse; 
                if(qDtls.isSiteAvailable <> null && qDtls.isSiteAvailable) {
                    //https://jira-cc.slb.nbndc.local/browse/FY17-307 - Added by Naga -  MDU/CP re qualification
                    if(siteRecord.RecordTypeId == mapOfSiteRecordTypeNamesAndId.get('MDU/CP'))
                        siteRecord.RecordTypeId = mapOfSiteRecordTypeNamesAndId.get('MDU/CP');
                    else
                        siteRecord.recordtypeId = mapOfSiteRecordTypeNamesAndId.get('Verified');                
                }
                //siteRecord.recordtypeId = mapOfSiteRecordTypeNamesAndId.get('Verified');                    
                if(qDtls.isSiteAvailable <> null && qDtls.isSiteAvailable)
                    siteRecord.Qualification_Date__c = qDtls.qualificationDate;
            }
        }
        system.debug('siteRecord.....before3.............'+siteRecord);
        return siteRecord;
    }
    public static Site__c instantiateStructuredUnVerifiedSite (AddressSearch_CX.StructuredAddress selectedAddress){
        Site__c SiteRecord = new Site__c ();
        if(selectedAddress != null){
            Map<String, Id> mapOfSiteRecordTypeNamesAndId = new Map<String, Id>();
            mapOfSiteRecordTypeNamesAndId = HelperUtility.pullAllRecordTypes('site__c');
            if(string.isNotEmpty(selectedAddress.unitNumber))  
                siteRecord.Unit_Number__c = selectedAddress.unitNumber;
            if(string.isNotEmpty(selectedAddress.levelNumber))  
                siteRecord.Level_Number__c = selectedAddress.levelNumber;
            if(string.isNotEmpty(selectedAddress.lotNumber))  
                siteRecord.Lot_Number__c = selectedAddress.lotNumber;
            if(string.isNotEmpty(selectedAddress.roadName))  
                siteRecord.Road_Name__c = selectedAddress.roadName;
            if(string.isNotEmpty(selectedAddress.roadNumber1))  
                siteRecord.Road_Number_1__c = selectedAddress.roadNumber1;
            if(string.isNotEmpty(selectedAddress.roadSuffixCode))  
                siteRecord.Road_Suffix_Code__c = selectedAddress.roadSuffixCode;
            if(string.isNotEmpty(selectedAddress.localityName))  
                siteRecord.Locality_Name__c = selectedAddress.localityName;
            if(string.isNotEmpty(selectedAddress.postCode))  
                siteRecord.Post_Code__c = selectedAddress.postCode;
            if(string.isNotEmpty(selectedAddress.stateTerritoryCode))  
                siteRecord.State_Territory_Code__c = selectedAddress.stateTerritoryCode;
            siteRecord.recordTypeId = mapOfSiteRecordTypeNamesAndId.get('Unverified');
            // Form the unstructured Address
            siteRecord.Site_Address__c = '';
            if(string.isNotEmpty(selectedAddress.lotNumber))  
                siteRecord.Site_Address__c += selectedAddress.lotNumber + ' ';
            if(string.isNotEmpty(selectedAddress.levelNumber))  
                siteRecord.Site_Address__c += selectedAddress.levelNumber + ' ';
            if(string.isNotEmpty(selectedAddress.unitNumber))   
                siteRecord.Site_Address__c += selectedAddress.unitNumber + ' ';
            if(string.isNotEmpty(selectedAddress.roadNumber1)) 
                siteRecord.Site_Address__c += selectedAddress.roadNumber1 + ' ';
            if(string.isNotEmpty(selectedAddress.roadName)) 
                siteRecord.Site_Address__c += selectedAddress.roadName + ' ';
            if(string.isNotEmpty(selectedAddress.roadSuffixCode))   
                siteRecord.Site_Address__c += selectedAddress.roadSuffixCode + ', ';
            if(string.isNotEmpty(selectedAddress.localityName)) 
                siteRecord.Site_Address__c += selectedAddress.localityName + ', ';
            if(string.isNotEmpty(selectedAddress.stateTerritoryCode))   
                siteRecord.Site_Address__c += selectedAddress.stateTerritoryCode + ' ';
            if(string.isNotEmpty(selectedAddress.postCode)) 
                siteRecord.Site_Address__c += selectedAddress.postCode; 
            siteRecord.Name = truncateString(siteRecord.Site_Address__c,79) ;
            siteRecord.isSiteCreationThroughCode__c = true;
            //insert siteRecord;
            //siteRecordId = siteRecord.Id;
        }
        system.debug('UNVERIFIED siteRecord==>'+siteRecord);
        return siteRecord;
    }
    // This method is called when user selects the SITE in the RADIO button for existing SITE Contact
    public static Site__c getSiteQualificationForExistingSite (string selectedSiteSFId){
        Site__c siteRecord = new Site__c();
        if(string.isNotEmpty(selectedSiteSFId)){
            List<Site__c> siteRecords = [SELECT Id, Name, Site_Address__c, Technology_Type__c, Serviceability_Class__c, recordtype.Name, recordtypeId, Rollout_Type__c, Location_Id__c, Asset_Number__c,Unit_Number__c,Level_Number__c,Post_Code__c,Locality_Name__c,Road_Type_Code__c,Road_Name__c,Road_Number_1__c,Unit_Type_Code__c,State_Territory_Code__c, Asset_Type__c, Road_Suffix_Code__c, Lot_Number__c FROM site__c WHERE id =: selectedSiteSFId LIMIT 1];
            if(siteRecords <> null && !siteRecords.isEmpty()){
                siteRecord = siteRecords.get(0);
                if(String.isNotEmpty(siteRecord.Location_Id__c)){
                    siteRecord = executeServiceQualification(siteRecord, siteRecord.Location_Id__c);
                    system.debug('AFTER site qualification==>'+siteRecord);
                }
                return siteRecord;
            }
        }
        return siteRecord;
    }
    // Custom Setting for Trigger method execution - trigger switch
    public static boolean isTriggerMethodExecutionDisabled(String methodName){
        Trigger_Execution__c TriggerExecution = Trigger_Execution__c.getValues(methodName);
        if(TriggerExecution !=null && TriggerExecution.isDisabled__c)
            return true;
        else
            return false;
    }
    
    // Truncate string 
    public static string truncateString(String stringValue, Integer stringLengthToTruncate)
    {
        String truncatedStringValue;
        if(string.isNotBlank(stringValue))
        {
            Integer strLength = stringValue.length();           
            if(strLength > stringLengthToTruncate){
               truncatedStringValue = stringValue.substring(0,stringLengthToTruncate); 
            }
            else if(strLength < stringLengthToTruncate){
                truncatedStringValue = stringValue.substring(0,strLength); 
            }
        }       
        return truncatedStringValue;        
    }
    
    
      // Dynamic Field value
    
    public static Object recursiveGet(sObject record, String field) {
    if(field.contains('.')) {
      Integer firstPeriod = field.indexOf('.');
      String nextObjectName = field.subString(0, firstPeriod);
      String remainingfieldName = field.subString(firstPeriod + 1, field.length());
      sObject nextObject = record.getSObject(nextObjectName);
      if(nextObject == null) {
        return null;
      } else {
        return recursiveGet(nextObject, remainingfieldName);
      }
    } else {
      return record.get(field);  
    }
  }
    
    public static sObject recursivePut(sObject record, String field, String Value) {
    if(field.contains('.')) {
      Integer firstPeriod = field.indexOf('.');
      String nextObjectName = field.subString(0, firstPeriod);
      String remainingfieldName = field.subString(firstPeriod + 1, field.length());
      sObject nextObject = record.getSObject(nextObjectName);
      if(nextObject == null) {
        return null;
      } else {
        return recursivePut(nextObject, remainingfieldName, Value);
      }
    } else {
            //system.debug('** sObject Type ==>'+ record.getSObjectType());
            record.put(field, Value);
      return record;  
    }
  }
}