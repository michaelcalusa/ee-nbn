/***************************************************************************************************
Class Name:  HelperUtility_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 02-11-2016
Function    : This class contains unit test scenarios for HelperUtility apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN         02-11-2016                Created
****************************************************************************************************/
@isTest
private class HelperUtility_Test{
    static integer statusCode = 200;
    static Map<String, String> responseHeaders = new Map<String, String> ();
    static String json = '{'+
        '  \"data\": ['+
        '    {'+
        '      \"type\": \"location\",'+
        '      \"id\": \"LOC111234567890\",'+
        '      \"attributes\": {'+
        '        \"gnafId\": \"G8932387982982\",'+
        '        \"dwellingType\": \"SDU\",'+
        '        \"region\": \"Urban\",'+
        '        \"regionValueDefaulted\": false,'+
        '        \"externalTerritory\": false,'+
        '        \"landUse\": \"Commercial\",'+
        '        \"landSubUse\": \"Retail\",'+
        '        \"address\": {'+
        '          \"unstructured\": \"Building A 1-2 Park LANE N Parkview 12345 APT 1 F 1 15B-16 Trevenar ST N Ashbury NSW 2193\",'+
        '          \"siteName\": \"Parkview\",'+
        '          \"locationDescriptor\": \"Near the school\",'+
        '          \"levelType\": \"F\",'+
        '          \"levelNumber\": \"1\",'+
        '          \"unitType\": \"APT\",'+
        '          \"unitNumber\": \"1\",'+
        '          \"lotNumber\": \"12345\",'+
        '          \"planNumber\": \"N-78238\",'+
        '          \"roadNumber1\": \"15B\",'+
        '          \"roadNumber2\": \"16\",'+
        '          \"roadName\": \"Trevenar\",'+
        '          \"roadTypeCode\": \"ST\",'+
        '          \"roadSuffixCode\": \"N\",'+
        '          \"locality\": \"Ashbury\",'+
        '          \"postCode\": \"2193\",'+
        '          \"state\": \"NSW\",'+
        '          \"complexAddress\": {'+
        '            \"siteName\": \"Building A\",'+
        '            \"roadNumber1\": \"1\",'+
        '            \"roadNumber2\": \"2\",'+
        '            \"roadName\": \"Park\",'+
        '            \"roadTypeCode\": \"LANE\",'+
        '            \"roadSuffixCode\": \"N\"'+
        '          }'+
        '        },'+
        '        \"aliasAddresses\": ['+
        '          {'+
        '            \"unstructured\": \"Building A 1-2 Park LANE N Parkview 12345 APT 1 F 1 13A-16 Trevenar ST N Ashbury NSW 2193\",'+
        '            \"siteName\": \"Parkview\",'+
        '            \"locationDescriptor\": \"Near the school\",'+
        '            \"levelType\": \"F\",'+
        '            \"levelNumber\": \"1\",'+
        '            \"unitType\": \"APT\",'+
        '            \"unitNumber\": \"1\",'+
        '            \"lotNumber\": \"12345\",'+
        '            \"planNumber\": \"N-78238\",'+
        '            \"roadNumber1\": \"15\",'+
        '            \"roadNumber2\": \"16\",'+
        '            \"roadName\": \"Trevenar\",'+
        '            \"roadTypeCode\": \"ST\",'+
        '            \"roadSuffixCode\": \"N\",'+
        '            \"locality\": \"Ashbury\",'+
        '            \"postCode\": \"2193\",'+
        '            \"state\": \"NSW\",'+
        '            \"complexAddress\": {'+
        '              \"siteName\": \"Building A\",'+
        '              \"roadNumber1\": \"1\",'+
        '              \"roadNumber2\": \"2\",'+
        '              \"roadName\": \"Park\",'+
        '              \"roadTypeCode\": \"LANE\",'+
        '              \"roadSuffixCode\": \"N\"'+
        '            }'+
        '          }'+
        '        ],'+
        '        \"geocode\": {'+
        '          \"latitude\": \"37.8136\",'+
        '          \"longitude\": \"144.9631\",'+
        '          \"geographicDatum\": \"GDA94\",'+
        '          \"srid\": \"4283\"'+
        '        },'+
        '        \"primaryAccessTechnology\": {'+
        '          \"technologyType\": \"Fibre\",'+
        '          \"serviceClass\": 3,'+
        '          \"rolloutType\": \"Brownfield\"'+
        '        }'+
        '      },'+
        '      \"relationships\": {'+
        '        \"containmentBoundaries\": {'+
        '          \"data\": ['+
        '            {'+
        '              \"id\": \"ADA-001\",'+
        '              \"type\": \"networkBoundary\"'+
        '            },'+
        '            {'+
        '              \"id\": \"MPS-001\",'+
        '              \"type\": \"networkBoundary\"'+
        '            }'+
        '          ]'+
        '        },'+
        '        \"MDU\": {'+
        '          \"data\": {'+
        '            \"id\": \"LOC212345678901\",'+
        '            \"type\": \"location\"'+
        '          }'+
        '        }'+
        '      }'+
        '    }'+
        '  ],'+
        '  \"included\": ['+
        '    {'+
        '      \"type\": \"networkBoundary\",'+
        '      \"id\": \"ADA-001\",'+
        '      \"attributes\": {'+
        '        \"boundaryType\": \"ADA\",'+
        '        \"status\": \"INSERVICE\",'+
        '        \"technologyType\": \"Fibre\"'+
        '      }'+
        '    },'+
        '    {'+
        '      \"type\": \"networkBoundary\",'+
        '      \"id\": \"MPS-001\",'+
        '      \"attributes\": {'+
        '        \"boundaryType\": \"MPS\",'+
        '        \"status\": \"INSERVICE\",'+
        '        \"technologyType\": \"Fibre\"'+
        '      }'+
        '    }'+
        '  ]'+
        '}';
    static testMethod void readFieldSet_Test(){
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get('Case');
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        HelperUtility.readFieldSet(DescribeSObjectResultObj.FieldSets.getMap().values().get(0).getName(),'Case');
        HelperUtility.readFieldSet(DescribeSObjectResultObj.FieldSets.getMap().values().get(0).getName(),null);
        HelperUtility.readFieldSet(DescribeSObjectResultObj.FieldSets.getMap().values().get(0).getName(),'abc__c');
        HelperUtility.readFieldSet(null,'Case');
    }
    static testMethod void getProfile_Test(){
        List<Id> listOfId = new List<Id> ();
        listOfId.add(UserInfo.getProfileId());
        HelperUtility.getProfile(listOfId);
    }
    static testMethod void generateQueryFromFieldSets_Test(){
        string whereClause = 'Name = \''+'Test'+ '\' ORDER BY createdDate DESC';
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get('Case');
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        HelperUtility.generateQueryFromFieldSets('Case',HelperUtility.readFieldSet(DescribeSObjectResultObj.FieldSets.getMap().values().get(0).getName(),'Case'),whereClause);
        HelperUtility.generateQueryFromFieldSets('Case',HelperUtility.readFieldSet(DescribeSObjectResultObj.FieldSets.getMap().values().get(0).getName(),'Case'),null);
    }
    static testMethod void getErrorMessage_Test(){
        HelperUtility.getErrorMessage('001');
        List<Error_Message__c> ListofErrorMessage = TestDataUtility.getListOfErrorMessage(true);
        HelperUtility.getErrorMessage('001');
    }
    static testMethod void reservedCharacterHandler_Test(){
        HelperUtility.reservedCharacterHandler('t!{d}es|t[d]s^~*:\\(s)c_a+E"hr\'eqw-r&or?');
    }
    static testMethod void formHandledQueryParam_Test(){
        HelperUtility.formHandledQueryParam(null,'a');
        HelperUtility.formHandledQueryParam('a','a');
    }
    static testMethod void pullAllRecordTypes_Test(){
        HelperUtility.pullAllRecordTypes('Case');
    }
    static testMethod void checkNullValue_Test(){
        HelperUtility.checkNullValue(null);
        HelperUtility.checkNullValue('Case');
    }
    static testMethod void createUnStructuredVerifiedSite_Test(){
        List<Site__c> listOfSites = new List<Site__c> ();
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        listOfSites = TestDataUtility.createSiteTestRecords(1, false);
        integer counter = 0;
        for(Site__c Site : listOfSites){
            Site.Location_Id__c = 'LOC11123456789'+counter;
            Site.MDU_CP_On_Demand_Id__c = 'LOC11123456789'+counter;
            Site.Asset_Number__c = '12345';
            Site.RecordTypeId = recordTypeId;
            Site.Site_Address__c = '100 CHATSWOOD, NSW 2150';
            Site.Name = '100 CHATSWOOD, NSW 2150';
            Site.Locality_Name__c = 'CHATSWOOD';
            Site.Road_Name__c = 'CHATSWOOD';
            counter++;
        }
        insert listOfSites;
        AddressSearch_CX.addressWrapper wrap = new AddressSearch_CX.addressWrapper();
        wrap.address = listOfSites.get(0).Site_Address__c;
        wrap.locationId = listOfSites.get(0).Location_Id__c;
        wrap.addressFrom = 'UnVerified';
        wrap.sfRecordId = listOfSites.get(0).Id;
        wrap.assetNumber = listOfSites.get(0).Asset_Number__c;
        wrap.SiteSfRecordName = listOfSites.get(0).Name;
        HelperUtility.createUnStructuredVerifiedSite(null);
        HelperUtility.createUnStructuredVerifiedSite(wrap);
        wrap.sfRecordId = null;
        HelperUtility.createUnStructuredVerifiedSite(wrap);
        wrap.locationId = 'LOC111234567890';
        HelperUtility.createUnStructuredVerifiedSite(wrap);
        delete listOfSites;
        Test.startTest();
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,json,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        Test.stopTest();
        HelperUtility.createUnStructuredVerifiedSite(wrap);
        
    }
    static testMethod void createStructuredUnVerifiedSite_Test(){
        HelperUtility.createStructuredUnVerifiedSite(null);
        AddressSearch_CX.StructuredAddress StructuredAddress_Class = new AddressSearch_CX.StructuredAddress ();
        StructuredAddress_Class.unitNumber = '100';
        StructuredAddress_Class.levelNumber = '100';
        StructuredAddress_Class.lotNumber = '100';
        StructuredAddress_Class.roadNumber1 = '100';
        StructuredAddress_Class.roadName = 'test';
        StructuredAddress_Class.roadSuffixCode = 'SA';
        StructuredAddress_Class.localityName = 'test';
        StructuredAddress_Class.postCode = '2150';
        StructuredAddress_Class.stateTerritoryCode = 'NSW';
        StructuredAddress_Class.AssetNumber = '12345';
        StructuredAddress_Class.AssetType = 'Cabinet';
        HelperUtility.createStructuredUnVerifiedSite(StructuredAddress_Class);
    }
    static testMethod void instantiateUnStructuredVerifiedSite_Test(){
        List<Site__c> listOfSites = new List<Site__c> ();
        responseHeaders.put('Content-Type','application/JSON');
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        listOfSites = TestDataUtility.createSiteTestRecords(1, false);
        integer counter = 0;
        for(Site__c Site : listOfSites){
            Site.Location_Id__c = 'LOC11123456789'+counter;
            Site.MDU_CP_On_Demand_Id__c = 'LOC11123456789'+counter;
            Site.Asset_Number__c = '12345';
            Site.RecordTypeId = recordTypeId;
            Site.Site_Address__c = '100 CHATSWOOD, NSW 2150';
            Site.Name = '100 CHATSWOOD, NSW 2150';
            Site.Locality_Name__c = 'CHATSWOOD';
            Site.Road_Name__c = 'CHATSWOOD';
            counter++;
        }
        insert listOfSites;
        Test.startTest();
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,json,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        Test.stopTest();
        AddressSearch_CX.addressWrapper wrap = new AddressSearch_CX.addressWrapper();
        wrap.address = listOfSites.get(0).Site_Address__c;
        wrap.locationId = listOfSites.get(0).Location_Id__c;
        wrap.addressFrom = 'UnVerified';
        wrap.sfRecordId = listOfSites.get(0).Id;
        wrap.assetNumber = listOfSites.get(0).Asset_Number__c;
        wrap.SiteSfRecordName = listOfSites.get(0).Name;
        HelperUtility.instantiateUnStructuredVerifiedSite(null);
        HelperUtility.instantiateUnStructuredVerifiedSite(wrap);
        wrap.sfRecordId = null;
        HelperUtility.instantiateUnStructuredVerifiedSite(wrap);
        wrap.locationId = 'LOC111123456789';
        HelperUtility.instantiateUnStructuredVerifiedSite(wrap);
    }
    static testMethod void executeServiceQualification_Test(){
        List<Site__c> listOfSites = new List<Site__c> ();
        responseHeaders.put('Content-Type','application/JSON');
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        listOfSites = TestDataUtility.createSiteTestRecords(1, false);
        integer counter = 0;
        for(Site__c Site : listOfSites){
            Site.Location_Id__c = 'LOC11123456789'+counter;
            Site.MDU_CP_On_Demand_Id__c = 'LOC11123456789'+counter;
            Site.Asset_Number__c = '12345';
            Site.RecordTypeId = recordTypeId;
            Site.Site_Address__c = '100 CHATSWOOD, NSW 2150';
            Site.Name = '100 CHATSWOOD, NSW 2150';
            Site.Locality_Name__c = 'CHATSWOOD';
            Site.Road_Name__c = 'CHATSWOOD';
            counter++;
        }
        insert listOfSites;
        Test.startTest();
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,json,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        Test.stopTest();
        HelperUtility.executeServiceQualification(null,null);
        HelperUtility.executeServiceQualification(listOfSites.get(0),'LOC111234567890');
    }
    static testMethod void instantiateStructuredUnVerifiedSite_Test(){
        HelperUtility.instantiateStructuredUnVerifiedSite(null);
        AddressSearch_CX.StructuredAddress StructuredAddress_Class = new AddressSearch_CX.StructuredAddress ();
        StructuredAddress_Class.unitNumber = '100';
        StructuredAddress_Class.levelNumber = '100';
        StructuredAddress_Class.lotNumber = '100';
        StructuredAddress_Class.roadNumber1 = '100';
        StructuredAddress_Class.roadName = 'test';
        StructuredAddress_Class.roadSuffixCode = 'SA';
        StructuredAddress_Class.localityName = 'test';
        StructuredAddress_Class.postCode = '2150';
        StructuredAddress_Class.stateTerritoryCode = 'NSW';
        StructuredAddress_Class.AssetNumber = '12345';
        StructuredAddress_Class.AssetType = 'Cabinet';
        HelperUtility.instantiateStructuredUnVerifiedSite(StructuredAddress_Class);
    }
    static testMethod void getSiteQualificationForExistingSite_Test(){
        HelperUtility.getSiteQualificationForExistingSite(null);
        List<Site__c> listOfSites = new List<Site__c> ();
        responseHeaders.put('Content-Type','application/JSON');
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        listOfSites = TestDataUtility.createSiteTestRecords(1, false);
        integer counter = 0;
        for(Site__c Site : listOfSites){
            Site.Location_Id__c = 'LOC11123456789'+counter;
            Site.MDU_CP_On_Demand_Id__c = 'LOC11123456789'+counter;
            Site.Asset_Number__c = '12345';
            Site.RecordTypeId = recordTypeId;
            Site.Site_Address__c = '100 CHATSWOOD, NSW 2150';
            Site.Name = '100 CHATSWOOD, NSW 2150';
            Site.Locality_Name__c = 'CHATSWOOD';
            Site.Road_Name__c = 'CHATSWOOD';
            counter++;
        }
        insert listOfSites;
        Test.startTest();
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,json,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        Test.stopTest();
        HelperUtility.getSiteQualificationForExistingSite(string.valueOf(listOfSites.get(0).Id));
    }
    static testMethod void isTriggerMethodExecutionDisabled_Test(){
        HelperUtility.isTriggerMethodExecutionDisabled('logicMethod');
    }
    static testMethod void truncateString_Test(){
        HelperUtility.truncateString('truncateString',4);
        HelperUtility.truncateString('truncateString',40);
        HelperUtility.truncateString(null,4);
    }
}