/***************************************************************************************************
    Class Name          : HttpMarCallOutImpl
    Version             : 1.0
    Created Date        : 03-Jul-2018
    Author              : Arpit Narain
    Description         : Class to mock HTTP callout to Appian and generate mock response
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/

@isTest
public Class HttpMarCallOutImpl implements HttpCalloutMock {

    private Integer responseStatusCode;
    private String responseMsgBody;

    public HttpMarCallOutImpl(Integer statusCode, String msgBody) {

        this.responseStatusCode = statusCode;
        this.responseMsgBody = msgBody;

    }



    public HttpResponse respond(HttpRequest httpRequest) {

        HttpResponse httpResponse = new HttpResponse();
        httpResponse.setStatusCode(responseStatusCode);
        httpResponse.setBody(responseMsgBody);

        return httpResponse;
    }
}