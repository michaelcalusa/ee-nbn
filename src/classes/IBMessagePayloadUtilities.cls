/*------------------------------------------------------------------------
Author:        Asif Khan
Company:       NBN
Description:   Utility class to transform the inbound object into sObject

Test Class: IBMessagePayloadUtilitiesTest

History
<Date>      <Authors Name>      <Action>      <Brief Description of Change>
20.03.2018   Asif Khan           Created
--------------------------------------------------------------------------*/
public class IBMessagePayloadUtilities
{
    Public static Boolean createOutBoundMessage = true;
    Public static Map<string,string> doNotCreateOBMsgFor = new Map<string,string>();
    public IBMessagePayloadUtilities()
    {
        
    }
    
    Public static void synchSFDCDataFromIB(List<Inbound_Staging__c> ibLst)
    {
        try
        {   
            List<ApplicationLogWrapper> msgs = new List<ApplicationLogWrapper>();            
            List<sObject> insertList = new List<sObject>();
            Map<string, sObject> insertMap = new Map<string, sObject>();
            Map<string, sObject> inboundStagingMap = new Map<string, sObject>();
            List<sObject> updateList = new List<sObject>();        
            List<sObject> deleteList = new List<sObject>();        
            
            if(StagingObjConfigUtilities.mapIBStagingConfiguration != null)        
            {            
                
                for(Inbound_Staging__c ibSB : ibLst)
                {
                    if(StagingObjConfigUtilities.mapIBStagingConfiguration.containsKey(ibSB.Object__c))                
                    {                        
                        system.debug('ibSB.Payload__c>>>>>'+ibSB.Payload__c);
                        try
                        {
                            sObject obj = getSobject(ibSB);                    
                            if(obj != null)
                            {                            
                                if(string.isBlank(obj.Id) && ibSB.Event__c == 'Insert')
                                {                                
                                    insertList.add(obj);
                                    doNotCreateOBMsgFor.put(ibSB.External_Id__c, ibSB.Source__c);
                                }
                                else if(!string.isBlank(obj.Id) && ibSB.Event__c == 'Update')
                                {
                                    updateList.add(obj);
                                    system.debug('Updating >>>>>>>> '+JSON.Serialize(obj));
                                    doNotCreateOBMsgFor.put(obj.Id, ibSB.Source__c);
                                }
                                else if(!string.isBlank(obj.Id) && ibSB.Event__c == 'Delete')
                                {
                                    deleteList.add(obj);
                                    doNotCreateOBMsgFor.put(obj.Id, ibSB.Source__c);
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            ApplicationLogWrapper msg = new ApplicationLogWrapper();
                            msg.source = 'IBMessagePayloadUtilities';
                            msg.logMessage = ex.getMessage();
                            msg.sourceFunction = 'synchSFDCDataFromIB';
                            msg.referenceId = ibSB.SFDC_Record_Id__c;
                            msg.referenceInfo = 'Unable to get sObject from given payload';
                            msg.payload = JSON.Serialize(ibSB);
                            msg.debugLevel = 'Error';
                            msg.Timer = 0;                                                                                    
                            msgs.add(msg);
                        }                        
                    }                
                }
                createOutBoundMessage = false;            
                if(insertList.size() > 0)
                {                           
                    insert insertList;                    
                }                        
                
                if(updateList.size() > 0)
                {
                    system.debug('Updating >>>>>>>> '+JSON.Serialize(updateList));
                    Database.SaveResult[] srList = Database.Update(updateList, false);                                        
                    List<ApplicationLogWrapper> logMsgs = getApplicationLogs(srList, null, updateList);
                    if(logMsgs.size() > 0)
                        msgs.addAll(logMsgs);
                                                            
                }
                
                if(deleteList.size() > 0)
                {
                    //Delete deleteList; 
                    Database.DeleteResult[] srList = Database.delete(updateList, false);
                    List<ApplicationLogWrapper> logMsgs = getApplicationLogs(null, srList, updateList);
                    if(logMsgs.size() > 0)
                        msgs.addAll(logMsgs);
                }
                
                if(msgs.size() > 0)
                {
                    GlobalUtility.logMessage(msgs);
                }
                
                system.debug('doNotCreateOBMsgFor>>>>>'+doNotCreateOBMsgFor.values());
            }
        }
        catch(Exception ex)
        {
            GlobalUtility.logMessage('Error','IBMessagePayloadUtilities', 'synchSFDCDataFromIB','',' synch SFDC Data From Inbound Object failed ','','',ex,0);
        }
       
                
    }
    
    public static List<ApplicationLogWrapper> getApplicationLogs(Database.SaveResult[] srList, Database.DeleteResult[] srDeleteList, List<sObject> sObjectList)
    {
        List<ApplicationLogWrapper> msgs = new List<ApplicationLogWrapper>();
        
        if(srList!=null)
        {
            for(Integer i=0;i<srList.size();i++)
            {
                Database.SaveResult sr = srList.get(i);
                system.debug('Database.SaveResult >>>>>>>> '+JSON.Serialize(sr));
                sObject recordFailed = sObjectList.get(i);
                string recordId = recordFailed.id;//sr.getId();                            
                
                system.debug('mapwithTargetIds.containsKey >>>>>>>> '+recordId);
                if (!sr.isSuccess())                                 
                {                                    
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) 
                    {                                        
                        system.debug('!sr.isSuccess() >>>>>>>> '+err.getStatusCode() + ': ' + err.getMessage());
                        ApplicationLogWrapper msg = new ApplicationLogWrapper();
                        msg.source = 'IBMessagePayloadUtilities';
                        msg.logMessage = err.getStatusCode() + ': ' + err.getMessage();
                        msg.sourceFunction = 'synchSFDCDataFromIB';
                        msg.referenceId = recordId;
                        msg.referenceInfo = err.getFields() !=null ? JSON.Serialize(err.getFields()) : '';
                        msg.payload = JSON.Serialize(recordFailed);
                        msg.debugLevel = 'Error';
                        msg.Timer = 0;                                                                                    
                        msgs.add(msg);
                        
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Faield to synch the record from Inbound message error: ' + err.getFields());
                    }
                }
        	}
        
        }
        
        if(srDeleteList!=null)
        {
            for(Integer j=0;j<srDeleteList.size();j++)
            {
                Database.DeleteResult srDelete = srDeleteList.get(j);
                system.debug('Database.DeleteResult >>>>>>>> '+JSON.Serialize(srDelete));
                sObject deleteRecordFailed = sObjectList.get(j);
                string deleteRecordId = deleteRecordFailed.id;//srDelete.getId();                            
                
                system.debug('mapwithTargetIds.containsKey >>>>>>>> '+deleteRecordId);
                if (!srDelete.isSuccess())                                 
                {                                    
                    // Operation failed, so get all errors                
                    for(Database.Error err : srDelete.getErrors()) 
                    {                                        
                        system.debug('!srDelete.isSuccess() >>>>>>>> '+err.getStatusCode() + ': ' + err.getMessage());
                        ApplicationLogWrapper msg = new ApplicationLogWrapper();
                        msg.source = 'IBMessagePayloadUtilities';
                        msg.logMessage = err.getStatusCode() + ': ' + err.getMessage();
                        msg.sourceFunction = 'synchSFDCDataFromIB';
                        msg.referenceId = deleteRecordId;
                        msg.referenceInfo = err.getFields() !=null ? JSON.Serialize(err.getFields()) : '';
                        msg.payload = JSON.Serialize(deleteRecordFailed);
                        msg.debugLevel = 'Error';
                        msg.Timer = 0;                                                                                    
                        msgs.add(msg);
                        
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Faield to synch the record from Inbound message error: ' + err.getFields());
                    }
                }
            }
        }            
        
        return msgs;
    }
    
    //Public static sObject getSobject(string sObjectApiName, Map<string, string> targetMapping, String jSONSObject)
    Public static sObject getSobject(Inbound_Staging__c ibSB)//Map<string, string> targetMapping, String jSONSObject)
    {    
        string sObjectApiName = ibSB.Object__C;
        String jSONSObject = ibSB.Payload__c;
        string source = ibSB.source__c; 
        SObject newSobject;
        if((ibSB != null && !string.IsBlank(sObjectApiName) && !string.IsBlank(source)))
        {        
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get(sObjectApiName);
            if(targetType != null)
            {
                List<StagingObjConfigUtilities.StagingObjConfig> sOCList = StagingObjConfigUtilities.mapIBStagingConfiguration.get(ibSB.Object__c);
                if(sOCList != null && sOCList.size() > 0)
                {
                    for(StagingObjConfigUtilities.StagingObjConfig sOC : sOCList)
                    {
                        boolean actionRequired = false;
                        if(sOC.configFor == source)
                        {
                            newSobject = targetType.newSObject();
                            newSobject.Id = ibSB.SFDC_Record_Id__c;
                            //system.debug('Got RecordType ID >>>>>'+newSobject.get('recordTypeId'));
                            if(!string.isBlank(jSONSObject)  && ibSB.Event__c != 'Delete' && (
                              (ibSB.Event__c == 'Update' && sOC.isModificationAllowed) ||
                              (ibSB.Event__c == 'Insert' && sOC.isCreationAllowed)))
                            {
                             	Map<String, Object> fieldMap = (Map<String, Object>)JSON.deserializeUntyped(jSONSObject);
                                if(fieldMap.containsKey('Payload'))
                                {
                                    system.debug('fieldMap Contains Payload>>>>>'+fieldMap);
                                    system.debug('fieldMap Payload>>>>>'+fieldMap.get('Payload'));
                                    fieldMap = (Map<String, Object>)fieldMap.get('Payload');
                                    
                                    system.debug('Payload JSON>>>>>'+fieldMap);                        
                                }
                                                            
                                Map<String, Schema.sObjectField> targetFields = targetType.getDescribe().fields.getMap();
                                                                
                                for (String key : fieldMap.keySet())
                                {                            
                                    if(sOC.mapObjectFieldsMapping.containsKey(key))
                                    {                                
                                        String sfdcFieldApiName = sOC.mapObjectFieldsMapping.get(key);
                                        Object value = fieldMap.get(key);
                                        Schema.DisplayType valueType = targetFields.get(sOC.mapObjectFieldsMapping.get(key)).getDescribe().getType();
                                        Schema.DescribeFieldResult sObjectDescribe = targetFields.get(sOC.mapObjectFieldsMapping.get(key)).getDescribe();
                                        system.debug('sObjectDescribe>>>>>'+sObjectDescribe);
                                        system.debug('valueType>>>>>'+valueType+' ++++ '+'value>>>>>'+value);
                                        
                                        if(sObjectDescribe.isUpdateable())
                                        {
                                            if (value instanceof String && valueType != Schema.DisplayType.String)
                                            {
                                                String svalue = (String)value;
                                                system.debug('value>>>>>'+svalue);
                                                if (valueType == Schema.DisplayType.Date)
                                                    newSobject.put(sOC.mapObjectFieldsMapping.get(key), Date.valueOf(svalue));                                        
                                                else if(valueType == Schema.DisplayType.DateTime)
                                                    newSobject.put(sOC.mapObjectFieldsMapping.get(key), DateTime.valueOfGmt(svalue));                                        
                                                else if (valueType == Schema.DisplayType.Percent || valueType == Schema.DisplayType.Currency)
                                                    newSobject.put(sOC.mapObjectFieldsMapping.get(key), svalue == '' ? null : Decimal.valueOf(svalue));                                        
                                                else if (valueType == Schema.DisplayType.Double)
                                                    newSobject.put(sOC.mapObjectFieldsMapping.get(key), svalue == '' ? null : Double.valueOf(svalue));                                        
                                                else if (valueType == Schema.DisplayType.Integer)
                                                    newSobject.put(sOC.mapObjectFieldsMapping.get(key), Integer.valueOf(svalue));
                                                else if (valueType == Schema.DisplayType.Boolean)
                                                    newSobject.put(sOC.mapObjectFieldsMapping.get(key), Boolean.valueOf(svalue));
                                                else if (valueType == Schema.DisplayType.Base64)
                                                    newSobject.put(sOC.mapObjectFieldsMapping.get(key), Blob.valueOf(svalue));                                        
                                                else
                                                    newSobject.put(sOC.mapObjectFieldsMapping.get(key), svalue);                                        
                                            }
                                            else
                                                newSobject.put(sOC.mapObjectFieldsMapping.get(key), value);
                                            
                                            actionRequired = true;
                                        }
                                    }                
                                }   
                            }
                            else
                            {                                
                                if(!string.isBlank(ibSB.SFDC_Record_Id__c) && ibSB.Event__c == 'Delete')
                                {
                                    if(sOC.isDeletionAllowed)
                                    {                                        
                                     	actionRequired = true;
                                    }                                    
                                }
                            }
                            
                            if(!actionRequired)
                            {
                                newSobject = null;
                            }
                        }
                    }
                }                
            }                                    			                        
        }
        return newSobject;
    }

}