/***************************************************************************************************
    Class Name  :  OBMessagePayloadUtilitiesTest
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  March 26,2019
    Function    :  This class contains unit test scenarios for OBMessagePayloadUtilities
	Used in     :  None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            March 26,2019               Created
****************************************************************************************************/
@isTest(seeAllData = false)
public class IBMessagePayloadUtilities_Test {
    static final string[] ABN_Nos = new string[]{'67099422723','74070625850', '52115553341', '38843605157', '69477029568', 	'79051193065' , '91118703665' };
    static final string externalID =  '11223491505';
    static final string action = 'Update';
    static final string Source = 'MDM';

    //Insert Inbound Records for Account Updates in Salesforce - Account 
    public static testmethod void testInsert_InboundStagingRecs(){
        Map<Id,Account> accMap = new Map<Id,Account>();
        List<Account> accLst = new List<Account>();
        Map<Id,Inbound_Staging__c> inboundRecsMap;
        string payload = '{ \n'+
            ' "Payload" : { \n' + 
            ' "ChangeEventHeader" : { \n' +
            ' "entityName" : "Account", \n' +
            ' "SFDCRecordId" :"recID" , \n' +
            ' "ExternalId" : "11223491505",  \n' +
            ' "Event" : "Update"  \n' +
            ' },  \n' +
            ' "ACN" : "12113113" \n' +
            ' } \n' +
            '}' ; 
        Test.startTest();
        accMap = TestDataFactory_BSM.createAccTestRecords(1, true, 'Business_End_Customer' , ABN_Nos);
        for(Id recId: accMap.keySet()){
             payload =  payload.replace('recID', recId);
            inboundRecsMap = TestDataFactory_BSM.createInboundStagingRecords(1, true, recId, 'Account', action, Source, payload, externalID);}
        accLst = [Select Id, ACN__c from Account where Id IN: accMap.keySet()];
                Test.stopTest();
        for(Account a: accLst)
        system.assertEquals('12113113', a.ACN__c);
    }
    
    //Insert Inbound Records for Contact Updates in Salesforce - Contact 
    public static testmethod void testInsert_InboundStagingRecs_Contact(){
        Map<Id,Contact> conMap = new Map<Id,Contact>();
        List<Contact> conLst = new List<Contact>();
        Map<Id,Inbound_Staging__c> inboundRecsMap;
        string payload = '{ \n'+
            ' "Payload" : { \n' + 
            ' "ChangeEventHeader" : { \n' +
            ' "entityName" : "Contact", \n' +
            ' "SFDCRecordId" :"recID" , \n' +
            ' "ExternalId" : "11223491505",  \n' +
            ' "Event" : "Update"  \n' +
            ' },  \n' +
            ' "firstName" : "subName" \n' +
            ' } \n' +
            '}' ; 
        Test.startTest();
        conMap = TestDataFactory_BSM.createConTestRecords(1, true, 'Business_End_Customer' , ABN_Nos);
        for(Id recId: conMap.keySet()){
            string Name = conMap.get(recId).FirstName;
            system.debug(Name);
             payload =  payload.replace('recID', recId);
             payload =  payload.replace('subName', Name);
            system.debug(payload);
            inboundRecsMap = TestDataFactory_BSM.createInboundStagingRecords(1, true, recId, 'Account', action, Source, payload, externalID);}
        conLst = [Select Id, FirstName from Contact where Id IN: conMap.keySet()];
                Test.stopTest();
        for(Contact c: conLst)
        system.assertEquals('BSMTest', c.FirstName);
    }
   
    //Insert Inbound Records with invalid ABN on Account
    public static testmethod void testInsert_InboundStagingRecs_InvalidABN(){
        Map<Id,Account> accMap = new Map<Id,Account>();
        List<Account> accLst = new List<Account>();
        Map<Id,Inbound_Staging__c> inboundRecsMap;
        string payload = '{ \n'+
            ' "Payload" : { \n' + 
            ' "ChangeEventHeader" : { \n' +
            ' "entityName" : "Account", \n' +
            ' "SFDCRecordId" :"recID" , \n' +
            ' "ExternalId" : "11223491505",  \n' +
            ' "Event" : "Update"  \n' +
            ' },  \n' +
            ' "ACN" : "12113113", \n' +
            ' "ABN" : "12113113" \n' +
            ' } \n' +
            '}' ; 
        Test.startTest();
        accMap = TestDataFactory_BSM.createAccTestRecords(1, true, 'Business_End_Customer' , ABN_Nos);
        
        for(Id recId: accMap.keySet()){
            try{
                payload =  payload.replace('recID', recId);
                system.debug(payload);
                inboundRecsMap = TestDataFactory_BSM.createInboundStagingRecords(1, true, recId, 'Account', action, Source, payload, externalID);}
            catch(Exception e)
            {
                Boolean expectedExceptionThrown =  e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION: ABN is not a valid format. Check you have a 11 digit numeric value that conforms to Australian standards.') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            } 
        }
        system.debug(inboundRecsMap.Size());
        system.debug(inboundRecsMap.values());
        accLst = [Select Id, ACN__c from Account where Id IN: accMap.keySet()];
                Test.stopTest();
        for(Account a: accLst){
            system.assertEquals(null, a.ACN__c);}
    } 
    //Insert Inbound Records with restricted standard fields like LastModifiedDate on Account
    public static testmethod void testInsert_InboundStagingRecs_restrictedFields1(){
        Map<Id,Account> accMap = new Map<Id,Account>();
        List<Account> accLst = new List<Account>();
        Map<Id,Inbound_Staging__c> inboundRecsMap;
        string payload = '{ \n'+
            ' "Payload" : { \n' + 
            ' "ChangeEventHeader" : { \n' +
            ' "entityName" : "Account", \n' +
            ' "SFDCRecordId" :"recID" , \n' +
            ' "ExternalId" : "11223491505",  \n' +
            ' "Event" : "Update"  \n' +
            ' },  \n' +
            ' "ACN" : "12113113", \n' +
            ' "LastModifiedDate" : "20190816" \n' +
            ' } \n' +
            '}' ; 
        Test.startTest();
        accMap = TestDataFactory_BSM.createAccTestRecords(1, true, 'Business_End_Customer' , ABN_Nos);
        
        for(Id recId: accMap.keySet()){
            try{
                payload =  payload.replace('recID', recId);
                system.debug(payload);
                inboundRecsMap = TestDataFactory_BSM.createInboundStagingRecords(1, true, recId, 'Account', action, Source, payload, externalID);}
            catch(Exception e)
            {
                Boolean expectedExceptionThrown =  e.getMessage().contains('Field LastModifiedDate is not editable') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
                system.debug('I am here1');
            } 
        }
        system.debug(inboundRecsMap.Size());
        system.debug(inboundRecsMap.values());
        accLst = [Select Id, ACN__c from Account where Id IN: accMap.keySet()];
                Test.stopTest();
        for(Account a: accLst){
            system.assertEquals('12113113', a.ACN__c);
        }
    }    
    //Insert Inbound Records with fields (which MDM is not allowed to) on Account
    public static testmethod void testInsert_InboundStagingRecs_restrictedFields2(){
        Map<Id,Account> accMap = new Map<Id,Account>();
        List<Account> accLst = new List<Account>();
        Map<Id,Inbound_Staging__c> inboundRecsMap;
        string payload = '{ \n'+
            ' "Payload" : { \n' + 
            ' "ChangeEventHeader" : { \n' +
            ' "entityName" : "Account", \n' +
            ' "SFDCRecordId" :"recID" , \n' +
            ' "ExternalId" : "11223491505",  \n' +
            ' "Event" : "Update"  \n' +
            ' },  \n' +
            ' "RevenueBand" : "something", \n' +
            ' "ACN" : "12113113" \n' +
            ' } \n' +
            '}' ; 
        Test.startTest();
        accMap = TestDataFactory_BSM.createAccTestRecords(1, true, 'Business_End_Customer' , ABN_Nos);
        
        for(Id recId: accMap.keySet()){
            try{
                payload =  payload.replace('recID', recId);
                system.debug(payload);
                inboundRecsMap = TestDataFactory_BSM.createInboundStagingRecords(1, true, recId, 'Account', action, Source, payload, externalID);}
            catch(Exception e)
            {
                Boolean expectedExceptionThrown =  e.getMessage().contains('INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST: Revenue Band: bad value for restricted picklist field: something') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
                system.debug('I am here2');
            } 
        }
        system.debug(inboundRecsMap.Size());
        system.debug(inboundRecsMap.values());
        accLst = [Select Id, ACN__c from Account where Id IN: accMap.keySet()];
                Test.stopTest();
        for(Account a: accLst){
            system.assertEquals(null, a.ACN__c);
        }
    }   
    //Coverage
    public static testmethod void test_IBMessagePayloadUtilities(){
        IBMessagePayloadUtilities something = new IBMessagePayloadUtilities();
    }
    
    //Insert Inbound Records for Account in Salesforce - Account Delete
    public static testmethod void testInsert_InboundStagingRecs_AccDelete(){
        Map<Id,Account> accMap = new Map<Id,Account>();
        Staging_Object_Config__mdt configOutbnd = new Staging_Object_Config__mdt();
        List<Account> accLst = new List<Account>();
        Map<Id,Inbound_Staging__c> inboundRecsMap;
        string payload = '{ \n'+
            ' "Payload" : { \n' + 
            ' "ChangeEventHeader" : { \n' +
            ' "entityName" : "Account", \n' +
            ' "SFDCRecordId" :"recID" , \n' +
            ' "ExternalId" : "11223491505",  \n' +
            ' "Event" : "Delete"  \n' +
            ' },  \n' +
            ' "ABN" : "12113113" \n' +
            ' } \n' +
            '}' ; 
        Test.startTest();
        accMap = TestDataFactory_BSM.createAccTestRecords(1, true, 'Business_End_Customer' , ABN_Nos);
        for(Id recId: accMap.keySet()){
             payload =  payload.replace('recID', recId);
            inboundRecsMap = TestDataFactory_BSM.createInboundStagingRecords(1, true, recId, 'Account', 'Delete', Source, payload, externalID);}
        accLst = [Select Id, ACN__c from Account where Id IN: accMap.keySet()];
                Test.stopTest();
		configOutbnd = TestDataFactory_BSM.getMetadataTestRecords('Inbound', 'Account', 'MDMAccountIB');
        if(configOutbnd.is_Deletion_Allowed__c == false){
            system.assertEquals(1, accLst.Size());}
    }   
    //Insert Inbound Records for Account in Salesforce - Account Insert
    public static testmethod void testInsert_InboundStagingRecs_AccInsert(){
        Map<Id,Account> accMap = new Map<Id,Account>();
        Staging_Object_Config__mdt configOutbnd = new Staging_Object_Config__mdt();
        List<Account> accLst = new List<Account>();
        List<Id> sfdcIDs = new  List<Id>();
        Map<Id,Inbound_Staging__c> inboundRecsMap;
        string payload = '{ \n'+
            ' "Payload" : { \n' + 
            ' "ChangeEventHeader" : { \n' +
            ' "entityName" : "Account", \n' +
            ' "SFDCRecordId" :"recID" , \n' +
            ' "ExternalId" : "11223491505",  \n' +
            ' "Event" : "Insert"  \n' +
            ' },  \n' +
            ' "ABN" : "12113113" \n' +
            ' } \n' +
            '}' ; 
        Test.startTest();
            inboundRecsMap = TestDataFactory_BSM.createInboundStagingRecords(1, true, null, 'Account', 'Insert', Source, payload, externalID);
        for(Inbound_Staging__c i : inboundRecsMap.values()){
            sfdcIDs.add(i.SFDC_Record_Id__c);
        }
        accLst = [Select Id, ACN__c from Account where Id IN: sfdcIDs];
                Test.stopTest();
		configOutbnd = TestDataFactory_BSM.getMetadataTestRecords('Inbound', 'Account', 'MDMAccountIB');
        if(configOutbnd.is_Creation_Allowed__c == false){
            system.assertEquals(0, accLst.Size());}
    }     
}