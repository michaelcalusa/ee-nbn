/***************************************************************************************************
    Class Name          : ICTAdditionalInformationController
    Version             : 1.0 
    Created Date        : 08-Nov-2017
    Author              : Rupendra Kumar Vats
    Description         : Logic to get additional Contact details for ICT community
    Test Class          : ICTAdditionalInformationControllerTest

    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Rupendra Kumar Vats       
    * Mohith                23/03/2018      Added additional fields to be retrieved MSEU-9621
    * Jairaj Jadhav         19-07-2018      US#15186: Added Certified_Count__c field of Account in the contact query of getContactInformation method.
****************************************************************************************************/ 
global without sharing class ICTAdditionalInformationController{
    @AuraEnabled
    public static Contact getContactInformation() {
        Contact c;
        try{
            User u = getCurrentUser();
            if(u != null && u.ContactId != null){
                c = [SELECT FirstName, LastName, Email, Phone, Lead_Role__c, Job_Title__c , 
                        Account.Name, Contact_ID__c, Account.Account_ID__c, ABN__c, Trading_Name__c, 
                        Number_of_Employees_in_your_company__c, MailingStreet, MailingCity, ICT_Contact_Role__c, 
                        MailingState, MailingPostalCode, Account.Website, Account.Engagement_Status__c, Account.Certified_Count__c,
                     (SELECT Id FROM Accreditation__r WHERE Case__c != null Order BY CreatedDate DESC LIMIT 1)
                    FROM Contact 
                    WHERE Id =: u.ContactId];
            }
        }
        Catch(Exception ex){
            system.debug('--ICTAdditionalInformationController--getContactInformation--' + ex.getMessage());
            c = new Contact(FirstName = '',LastName = '',Email = '',Phone='', Lead_Role__c='', Job_Title__c='', ABN__c='', Number_of_Employees_in_your_company__c='',
            MailingStreet = '', MailingCity = '', MailingState = '', MailingPostalCode = '', Trading_Name__c = '');
        }
        return c;
    }
    
    
    @AuraEnabled 
    public static List<string> roleOptions(){
        List<string> roleOptList = new List<string>();
        roleOptList.add('Please choose');
        Schema.DescribeFieldResult fieldResult = Contact.Lead_Role__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            roleOptList.add(f.getValue());
        }  
        system.debug('--roleOptList--' + roleOptList);
        return roleOptList;
        
    }
    
    @AuraEnabled 
    public static List<string> businessOptions(){
        List<string> businessOptList = new List<string>();
        businessOptList.add('Please choose');
        Schema.DescribeFieldResult fieldResult = Contact.Number_of_Employees_in_your_company__c.getDescribe();
        List<Schema.PicklistEntry> ple1 = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f1 : ple1){
            businessOptList.add(f1.getValue());
        }  
        system.debug('--businessOptList--' + businessOptList);
        return businessOptList;
        
    }
    
    @AuraEnabled
    public static Boolean processAdditionalInformation(String strPhone, String strABNNumber, String strEmployeeCount, String strStreetName, String strSuburb, String strState, String strPostalcode, String strRoleType, String strJobTitle, String strTradingName) {
        Boolean isProcessed = false;
        system.debug('--strPhone--' + strPhone);
        system.debug('--strABNNumber--' + strABNNumber);
        system.debug('--strEmployeeCount--' + strEmployeeCount);
        system.debug('--strStreetName--' + strStreetName);
        system.debug('--strSuburb--' + strSuburb);
        system.debug('--strState--' + strState);
        system.debug('--strPostalcode--' + strPostalcode);
        //MSEU-9621
        system.debug('--strRoleType--' + strRoleType);
        system.debug('--strJobTitle--' + strJobTitle);
        
        try{
            User u = getCurrentUser();
            if(u != null && u.ContactId != null){
                Contact c = [ SELECT Phone, Number_of_Employees_in_your_company__c, MailingStreet, MailingCity, MailingState, MailingPostalCode, Lead_Role__c, Job_Title__c, Trading_Name__c FROM Contact WHERE Id =: u.ContactId ];
                
                c.Phone = strPhone;
                c.MailingStreet = strStreetName;
                c.MailingCity = strSuburb;
                c.ABN__c = strABNNumber;
                c.MailingState = strState;
                c.MailingPostalCode = strPostalcode;
                c.Number_of_Employees_in_your_company__c = strEmployeeCount;
                //MSEU-9621
                c.Job_Title__c = strJobTitle;
                c.Lead_Role__c = strRoleType;
                c.Trading_Name__c = strTradingName;
                c.Additional_Information_Provided__c = true;

                update c;
                
                isProcessed = true;
            }
        }Catch(Exception ex){
            GlobalUtility.logMessage('Error','ICT Partner Program Community','Additional Information','','Additional Information','Additional Information method error','',ex,0);
            system.debug('--ICTAdditionalInformationController--' + ex.getMessage());        
        }
        return isProcessed;
    }
    
    private static User getCurrentUser(){
        User u;
        List<User> lstUser = [SELECT FirstName, LastName, Email, ContactId FROM User WHERE Id =: UserInfo.getUserId()];
        if(!lstUser.isEmpty()){
            u = lstUser[0];
        }
        return u;
    }
    /*------------------------------------------------------------------------
    Description         :   This method will send the individual preferences
    Input Parameters    :   IndividualModel Object
    Output Parameters   :   Individual
    // Preferences model and details 
    // MSEU-21704 - Update existing Pref code for Email opt-out for ICT Partners
    --------------------------------------------------------------------------   */

    public class PartnerIndividualModel {
        //preference variables
        @AuraEnabled
        public String pID {get;set;}
        @AuraEnabled
        public Boolean newsUpdates {get;set;}
        @AuraEnabled
        public Boolean productInfo {get;set;}
        @AuraEnabled
        public Boolean ictPrograms {get;set;}
        
        public PartnerIndividualModel(String prefID, Boolean isnewsUpdates, Boolean isproductInfo, Boolean isictPrograms){
            this.pID = prefID;
            this.newsUpdates = isnewsUpdates;
            this.productInfo = isproductInfo;
            this.ictPrograms = isictPrograms;
            //this.LeaglAndPrivacy = isLeaglAndPrivacy;
        }
    }
    
    /*------------------------------------------------------------------------
    Description         :   This method will send the individual preferences
    Input Parameters    :   IndividualModel Object
    Output Parameters   :   Individual
    --------------------------------------------------------------------------   */
    @AuraEnabled
    public static PartnerIndividualModel getICTPreferences() {  
        list<Contact> contactList = new list<Contact>();
        PartnerIndividualModel partnerPref;
        Contact C;
        try{
            User u = getCurrentUser();
            if(u !=null && u.ContactId !=null){
                c = [SELECT Id, FirstName,LastName,Email,Phone,ICT_nbn_Business_News__c,ICT_nbn_Business_Products_Services__c,ICT_Channel_Program_Updates__c,
                     NBN_Products__c,nbn_updates__c, telesales__c, IndividualId, HasOptedOutOfEmail
                     FROM Contact WHERE Id=:u.ContactId];
                
                List<Individual> lstIndividuals = [SELECT Id, FirstName,LastName,Email__c,
                                    nbn_updates__c,NBN_Products__c,telesales__c,
                                    ICT_nbn_Business_News__c,ICT_nbn_Business_Products_Services__c,ICT_Channel_Program_Updates__c,
                                    PID__c,Phone__c 
                                    FROM Individual WHERE Id =: c.IndividualId];
                
                if(!lstIndividuals.isEmpty()){
                    Individual ind = lstIndividuals[0];
                    partnerPref = new PartnerIndividualModel(ind.PID__c, ind.ICT_nbn_Business_News__c, ind.ICT_nbn_Business_Products_Services__c, ind.ICT_Channel_Program_Updates__c);
                }

            }
        }
        Catch(Exception ex){
            system.debug('--ICTAdditionalInformationController--getICTPreferences--' + ex.getMessage());
            c = new Contact(FirstName = '',LastName = '',Email = '',Phone='');
            partnerPref = new PartnerIndividualModel(null, false,false,false);
        }
        return partnerPref;
    }

    /*------------------------------------------------------------------------
    Description         :   This method will send the individual preferences
    Input Parameters    :   IndividualModel Object
    Output Parameters   :   Individual
    --------------------------------------------------------------------------   */
   @AuraEnabled
   public static Boolean processICTPreferences(String strPID, Boolean strNewsUpdates, Boolean strProductInfo, Boolean strICTPrograms){
       try{
           List<Individual> ind = new List<Individual>();  
           system.debug('--strPID--' + strPID);
           system.debug('--strNewsUpdates--' + strNewsUpdates);
           system.debug('--strProductInfo--' + strProductInfo);
           system.debug('--strICTPrograms--' + strICTPrograms);
           PartnerIndividualModel savePartnerPref= new PartnerIndividualModel(strPID, strNewsUpdates, strProductInfo, strICTPrograms);
           
           ICTPreferencesController.updateICTPrefIndividualByModel(savePartnerPref);
           
           
       }catch(exception ex){
           GlobalUtility.logMessage('Error','ICT Partner Program Community','processICTPreferences','','processICTPreferences','processICTPreferences method error','',ex,0);
           system.debug('--ICTAdditionalInformationController--' + ex.getMessage());  
       }
        return true;
   }
    
   
}