/***************************************************************************************************
    Class Name  :  ICTAdditionalInformationControllerTest
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  Nov 12,2017 
    Function    :  This class contains unit test scenarios for ICTAdditionalInformationController
    Used in     :  None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            Nov 12,2017                 Created
****************************************************************************************************/

@isTest(seeAllData = false)
public Class ICTAdditionalInformationControllerTest{
    static testMethod void TestMethod_ICTAdditionalInformation() {
        String strConRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRecordTypeID = rtMapByName.get('Partner Contact').getRecordTypeId();    
        
        String strAccRecordTypeID;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Partner Account').getRecordTypeId();  
        
        Account acc = new Account(Name = 'test ICT Comm', RecordTypeID = strAccRecordTypeID);
        insert acc;
        
        Contact con = new Contact(LastName = 'LastName', Email = 'test@force.com', RecordTypeID = strConRecordTypeID, AccountId = acc.Id);
        insert con;

        lmscons__Training_Path__c  testCourse = new lmscons__Training_Path__c();
        testCourse.Name = 'ICT Channel Partner Program';
        testCourse.lmscons__Enable_Certificate__c = true;
        insert testCourse;
        
        ICTAdditionalInformationController.roleOptions();  
        ICTAdditionalInformationController.businessOptions();
        
        Test.startTest();
        // Get the community user profile
        List<Profile> lstProfile = [ SELECT Id FROM Profile WHERE Name = 'ICT Customer Community Plus User'];
        if(!lstProfile.isEmpty()){
            // Create community user
            User commUser = new User();
            commUser.Username = 'test@force.com.ICT';
            commUser.Email = 'test@force.com';
            commUser.FirstName = 'firstname';
            commUser.LastName = 'lastname';
            commUser.CommunityNickname = 'ictcomm8848';
            commUser.ContactId = con.ID; 
            commUser.ProfileId = lstProfile[0].Id;
            commUser.Alias = 'tict'; 
            commUser.TimeZoneSidKey = 'Australia/Sydney'; 
            commUser.LocaleSidKey = 'en_US'; 
            commUser.EmailEncodingKey = 'UTF-8'; 
            commUser.LanguageLocaleKey = 'en_US'; 
            insert commUser;

            system.runAs(commUser){
                Contact c = ICTAdditionalInformationController.getContactInformation();
                
                Boolean isContactUpdatedA = ICTAdditionalInformationController.processAdditionalInformation('0123456789','23','56','177','pacific hwy','NSW','2060','Sales','Dev','trading');
                
                Boolean isContactUpdatedB = ICTAdditionalInformationController.processAdditionalInformation('0123456789','23','1','177','pacific hwy','NSW','2060','Sales','Dev','trading');
                
                Boolean isContactUpdatedC = ICTAdditionalInformationController.processAdditionalInformation('0123456789','23','3','177','pacific hwy','NSW','2060','Sales','Dev','trading');
                
                Boolean isContactUpdatedD = ICTAdditionalInformationController.processAdditionalInformation('0123456789','23','8','177','pacific hwy','NSW','2060','Sales','Dev','trading');
                
                Boolean isContactUpdatedE = ICTAdditionalInformationController.processAdditionalInformation('0123456789','23','12','177','pacific hwy','NSW','2060','Sales','Dev','trading');
                
                Boolean isContactUpdatedF = ICTAdditionalInformationController.processAdditionalInformation('0123456789','23','22','177','pacific hwy','NSW','2060','Sales','Dev','trading');
                
                Boolean isContactUpdatedG = ICTAdditionalInformationController.processAdditionalInformation('0123456789','23','110','177','pacific hwy','NSW','2060','Sales','Dev','trading');
                
                ICTAdditionalInformationController.getICTPreferences();
                
                Boolean isIndividualUpdatedA =  ICTAdditionalInformationController.processICTPreferences(null, false, false, false);
                
                Individual i = new Individual(FirstName = 'first',LastName = 'last',Email__c = 'testind@test.com');
                insert i;
                
                Individual testIndv = [SELECT Id,Salutation,FirstName,LastName,Email__c,TriggerSwitch__c, NBN_Products__c,nbn_updates__c, telesales__c, Email_Opt_Out_Date__c, HasOptedOutOfEmail__c, PID__c, 
                                        Phone__c FROM Individual WHERE Id =:i.Id limit 1];
             
                Boolean isIndividualUpdatedB =  ICTAdditionalInformationController.processICTPreferences(testIndv.PID__c, false, false, false);                                   
            }
            
            Test.stopTest();
        }      
    }
}