/***************************************************************************************************
    Class Name  : ICTAddressSearchControllerTest
    Class Type  : Test Class 
    Version     : 1.0 
    Created Date: 26-March-2018
    Function    : This class contains unit test scenarios for ICTAddressSearchController apex class which handles Google APIs for ICT
    Used in     : None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            26/03/2018                 Created
****************************************************************************************************/
@isTest(seeAllData = false)
public class ICTAddressSearchControllerTest{
    static testMethod void TestMethod_searchGoogleMap(){
        // Verify system call out exception
        ICTAddressSearchController.searchGoogleMap('177');
        
        // Verify Responses using Mock classes for Success and Failure
        MockHttpResponse mockResp = new MockHttpResponse();
        
        // Verify the success call out
        mockResp.strResponseType = 'GooglePlaceSearch';
        Test.setMock(HttpCalloutMock.class, mockResp);
        ICTAddressSearchController.searchGoogleMap('177');
        
        mockResp.strResponseType = 'GooglePlaceDetails';
        ICTAddressSearchController.getAddressDetails('177');
    }
}