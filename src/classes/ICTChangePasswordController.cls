/***************************************************************************************************
    Class Name          : ICTChangePasswordController
    Version             : 1.0 
    Created Date        : 20-Nov-2017
    Author              : Rupendra Kumar Vats
    Description         : Logic to 'Change Password' for ICT community
    Test Class          : ICTChangePasswordControllerTest

    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------
    * Rupendra Kumar Vats
    * Jairaj Jadhav
****************************************************************************************************/
public with sharing class ICTChangePasswordController {
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}
    public String useremail {get; set;}
    public boolean invalidPassword {get; set;}
    public boolean emptyPassword {get; set;}
    public boolean mismatchPassword {get; set;}
    
    public PageReference changePassword() {
        /**
         * Custom Password Check Logic to force Password Complexity of minimum 10 charater Length
         * One Uppercase letter, one Lower Case Letter and Password Should not contain the 
         * Username or FirstName/LastName
        **/
        invalidPassword = false;
        emptyPassword = false;
        mismatchPassword = false;
        
        if ((newPassword == null || String.isEmpty(newPassword)) || (verifyNewPassword == null || String.isEmpty(verifyNewPassword))) {
            emptyPassword = true;
            return null;
        }  
        
        if (!isValidPassword(newPassword, verifyNewPassword) || (newPassword == null || String.isEmpty(newPassword)) || (verifyNewPassword == null || String.isEmpty(verifyNewPassword))) {
            mismatchPassword = true;
            return null;
        }
        Pattern MyPattern = Pattern.compile('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d!_$%@#£€*?&]{10,}$');
        // Then instantiate a new Matcher object "MyMatcher"
        Matcher passwordMyMatcher = MyPattern.matcher(newPassword);
        Matcher confirmpasswordMyMatcher = MyPattern.matcher(verifyNewPassword);
        if(!passwordMyMatcher.matches() || !confirmpasswordMyMatcher.matches()) {
            invalidPassword = true;
            return null;
        }else{
            system.debug('==>'+newPassword);
            return Site.changePassword(newPassword, verifyNewPassword);
        }
        //return null;
    }
    
    public void populateUsername(){
        useremail = userinfo.getuseremail();
    }
    
    @TestVisible 
    private static boolean isValidPassword(String password, String confirmPassword) {
        return password == confirmPassword;
    }
    
    public ICTChangePasswordController() {
        newPassword = '';
        verifyNewPassword = '';
    }
}