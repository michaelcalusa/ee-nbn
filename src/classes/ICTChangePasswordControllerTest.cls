/***************************************************************************************************
    Class Name  :  ICTChangePasswordControllerTest
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  Nov 22,2017 
    Function    :  This class contains unit test scenarios for ICTChangePasswordController
    Used in     :  None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            Nov 22,2017                 Created
****************************************************************************************************/

@isTest(seeAllData = false)
public Class ICTChangePasswordControllerTest{
    static testMethod void TestMethod_ICTChangePassword() {
        ICTChangePasswordController controller = new ICTChangePasswordController();
        controller.oldPassword = '123456';
        controller.newPassword = 'qwerty1'; 
        controller.verifyNewPassword = 'qwerty1';
        System.assertEquals(controller.changePassword(),null);
        controller.populateUsername();
    }
}