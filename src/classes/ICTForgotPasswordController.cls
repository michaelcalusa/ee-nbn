/***************************************************************************************************
    Class Name          : ICTForgotPasswordController 
    Version             : 1.0 
    Created Date        : 03-April-2018
    Author              : Rupendra Kumar Vats
    Description         : Logic to 'Forgot Password' for ICT community
    Test Class          : ICTForgotPasswordControllerTest

    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Rupendra Kumar Vats       
****************************************************************************************************/ 
public class ICTForgotPasswordController {

    public ICTForgotPasswordController() {

    }

    @AuraEnabled
    public static String forgotPassowrd(String username, String checkEmailUrl) {
        try {
            String strUserName = username + Label.ICT_User_Suffix;
            String strLoginUserName;
            List<User> lstUser = [ SELECT Id, UserName FROM User WHERE (UserName =: strUserName OR  UserName =: username) AND Profile.Name IN ('ICT Partner Community User', 'ICT Customer Community Plus User') AND isPortalEnabled = true];
            if(lstUser.size() > 0){
                strLoginUserName = lstUser[0].UserName;
                Site.forgotPassword(strLoginUserName);
                ApexPages.PageReference checkEmailRef = new PageReference(checkEmailUrl);
                if(!Site.isValidUsername(strLoginUserName)) {
                    return Label.Site.invalid_email;
                }
                if(!Test.isRunningTest()){
                    aura.redirect(checkEmailRef);
                }
            }
            else{
                return Label.Site.invalid_email;
            }
            
            return null;
        }
        catch (Exception ex) { return ex.getMessage();}
    }
}