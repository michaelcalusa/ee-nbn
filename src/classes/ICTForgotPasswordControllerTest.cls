/***************************************************************************************************
    Class Name          : ICTForgotPasswordControllerTest 
    Version             : 1.0 
    Created Date        : 03-April-2018
    Author              : Rupendra Kumar Vats
    Description         : Test class for ICTForgotPasswordController

    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Rupendra Kumar Vats       
****************************************************************************************************/ 

@isTest(seeAllData = false)
public Class ICTForgotPasswordControllerTest{
    static testMethod void TestMethod_ICTChangePassword() {
        String strConRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRecordTypeID = rtMapByName.get('Partner Contact').getRecordTypeId();    
        
        String strAccRecordTypeID;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Partner Account').getRecordTypeId();  
        
        Account acc = new Account(Name = 'test ICT Comm', RecordTypeID = strAccRecordTypeID);
        insert acc;
        
        Contact con = new Contact(LastName = 'LastName', Email = 'test@force.com', RecordTypeID = strConRecordTypeID, AccountId = acc.Id);
        insert con;
  
        // Get the community user profile
        List<Profile> lstProfile = [ SELECT Id FROM Profile WHERE Name = 'ICT Customer Community Plus User'];
        if(!lstProfile.isEmpty()){
            // Create community user
            User commUser = new User();
            commUser.Username = 'test@force.com.ict';
            commUser.Email = 'test@force.com';
            commUser.FirstName = 'firstname';
            commUser.LastName = 'lastname';
            commUser.CommunityNickname = 'ictlogincomm';
            commUser.ContactId = con.ID; 
            commUser.ProfileId = lstProfile[0].Id;
            commUser.Alias = 'tictfp'; 
            commUser.TimeZoneSidKey = 'Australia/Sydney'; 
            commUser.LocaleSidKey = 'en_US'; 
            commUser.EmailEncodingKey = 'UTF-8'; 
            commUser.LanguageLocaleKey = 'en_US'; 
            insert commUser;
        }
        ICTForgotPasswordController login = new ICTForgotPasswordController();
        ICTForgotPasswordController.forgotPassowrd('test@force.com','/ForgotPassword');
        ICTForgotPasswordController.forgotPassowrd('test2@force.com','/ForgotPassword');             
    }
}