/*------------------------------------------------------------  
Author:        Jairaj Jadhav
Company:       Wipro Technologies
Description:   This class is service class of ICTMyCertificate lightning component.
Test Class:    
History
<Date>      <Authors Name>     <Brief Description of Change>
12-03-2018    Jairaj Jadhav    Replaced Course Name with Course lookup and removed some code which is not of use.
------------------------------------------------------------*/
public without sharing class ICTGenerateCertificateController{
    
    /**************************************************************************************************
        Description: This method is responsible for querying and returning the details related to obtained certificate for the logged-in user.
        Return Type: List<lmscons__Training_Path_Assignment_Progress__c>
        Created By: Jairaj Jadhav
    **************************************************************************************************/
    @AuraEnabled
    public static List<lrngRcrdCrsAssnmntWrapper> retCertificates(){
        try{
            user loggdUsr = [select id, contactID from user where id= :userInfo.getUserID()];
            list<lrngRcrdCrsAssnmntWrapper> wrapperList = new list<lrngRcrdCrsAssnmntWrapper>();
            map<string, lmscons__Training_Path_Assignment_Progress__c> crsAssnmntIDMap = new map<string, lmscons__Training_Path_Assignment_Progress__c>();
            List<Learning_Record__c> lrngRcrdList = [select id, Course__r.name, Course_Status__c, Contact__r.Contact_ID__c,
                Contact__r.name from Learning_Record__c where Contact__r.id=: loggdUsr.contactID AND Course_Status__c='Certified' AND
                Course__c!=null AND Contact__r.Additional_Information_Provided__c=true];
            List<lmscons__Training_Path_Assignment_Progress__c> courseAssignList = [select id, lmscons__Completion_Date__c,
                lmscons__Training_Path__r.name from lmscons__Training_Path_Assignment_Progress__c where lmscons__Completion_Date__c!=null
                AND lmscons__Transcript__r.lmscons__Trainee__c != null AND lmscons__Transcript__r.lmscons__Trainee__c =: loggdUsr.id];
            if(!courseAssignList.isEmpty() && courseAssignList.size() > 0){
                for(lmscons__Training_Path_Assignment_Progress__c crsAssnmnt: courseAssignList){
                    crsAssnmntIDMap.put(crsAssnmnt.lmscons__Training_Path__r.name,crsAssnmnt);
                }
            }
            if(!lrngRcrdList.isEmpty() && lrngRcrdList.size() > 0){
                for(Learning_Record__c lrngRcrd: lrngRcrdList){
                    if(crsAssnmntIDMap.containsKey(lrngRcrd.Course__r.name)){
                        wrapperList.add(new lrngRcrdCrsAssnmntWrapper(lrngRcrd, crsAssnmntIDMap.get(lrngRcrd.Course__r.name)));
                    }
                }
            }
            return (!wrapperList.isEmpty() && wrapperList.size() > 0) ? wrapperList: null;
        }catch(exception ex){
            system.debug('Unhandled DMLException: '+ex);
            return null;
        }
    }
    
    public class lrngRcrdCrsAssnmntWrapper {
        @AuraEnabled public string crseName {get; set;}
        @AuraEnabled public string cntctID {get; set;}
        @AuraEnabled public string cntctName {get; set;}
        @AuraEnabled public datetime cmpltnDate {get; set;}
        
        public lrngRcrdCrsAssnmntWrapper(Learning_Record__c lrngRcrd, lmscons__Training_Path_Assignment_Progress__c crsAssnmnt) {
            crseName = lrngRcrd.Course__r.name;
            cntctID = lrngRcrd.Contact__r.Contact_ID__c;
            cntctName = lrngRcrd.Contact__r.name;
            cmpltnDate = crsAssnmnt.lmscons__Completion_Date__c;
        }
    }
}