@isTest
public class ICTGenerateCertificateControllerTest {
   static testmethod void testMethod1(){
       try{
            account acc = new account(name='test account');
            insert acc;
            contact con = new contact(Additional_Information_Provided__c=true,accountid=acc.id,firstName = 'test first', lastName = 'test last',recordtypeid=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Partner Contact').getRecordTypeId());
            insert con;
            lmscons__Training_Path__c course = new lmscons__Training_Path__c(name='test');
            insert course;
            Learning_Record__c lrec = new Learning_Record__c(Contact__c=con.id,Course_Name__c='test', Course_Status__c='Certified',course__c=course.id);
            insert lrec;
            profile commProfile = [select id from profile where name='Customer Community Plus Login User'];
            //userrole commRole = [select id from userrole where PortalType='CustomerPortal' limit 1];
            user communityUsr = new user();
            System.runAs (new User(Id = UserInfo.getUserId())){
                UserRole r = new UserRole(name = 'TEST ROLE', PortalType='CustomerPortal', PortalAccountId=acc.Id);
                communityUsr = new user(PortalRole = 'Manager',userroleid=r.id,contactid=con.id,LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',LanguageLocaleKey='en_US',TimeZoneSidKey='Australia/Sydney',CommunityNickname='testtt',Alias='tttt',Email='test@test.com',firstName = 'test first', lastName = 'test last', username='testclassuser@test.com', profileid=commProfile.id);
                insert communityUsr;
            }
            lmscons__Transcript__c transcript = new lmscons__Transcript__c(lmscons__Trainee__c=communityUsr.id);
            insert transcript;
            lmscons__Training_Path_Assignment_Progress__c courseAssignment = new lmscons__Training_Path_Assignment_Progress__c(Learning_Record__c=lrec.id,lmscons__Training_Path__c=course.id, lmscons__Transcript__c=transcript.id, lmscons__Completion_Date__c=system.now());
            insert courseAssignment;
            lrec.Course_Status__c='Certified';
            update lrec;
            system.runAs(communityUsr){
                test.startTest();
                    ICTGenerateCertificateController.retCertificates();
                test.stopTest();
            }
        }Catch(Exception ex){
            system.debug('---Exception--UserTriggerHandler_Test--enrollCFSUserTest()--' + ex.getMessage());
        }
   }
}