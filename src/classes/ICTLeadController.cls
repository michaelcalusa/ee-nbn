/********************************************************
* Class Name    : ICTLeadController
* Description   : Controller class for ICT_Lead_Component Lightning component
* Created By    : Rupendra Kumar Vats
* Date          : 23 November, 2018
* Change History :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Rupendra Vats           23 November, 2018           Created
* ----------------------------------------------------------------------------
*/
 
public With Sharing class ICTLeadController{

    /*
        Method  : getLeadRecord
        Return  : Lead record inforamtion
        Params  : Lead Record Id
    */
    @AuraEnabled
    public static Lead getLeadRecord(String strRecordId) {
        Lead leadRecord = new Lead();
        try{
            List<Lead> lstLead = [ SELECT Id, OwnerId, Salutation, FirstName, LastName, Email, Company, Title__c, Phone, MobilePhone, Preferred_Contact_Method__c, ABN__c, Trading_Name__c, Company_Size__c, Street, City, State, PostalCode FROM Lead WHERE ID =: strRecordId ];
            
            if(!lstLead.isEmpty()){
                leadRecord = lstLead[0];
            }
        }
        Catch(Exception ex){
            GlobalUtility.logMessage('Error','ICT Partner Program Community','Get lead record','','Get lead record','getLeadRecord method error','',ex,0);
        }
        return leadRecord;
    }
    
    /*
        Method  : processAcceptance
        Return  : Acceptance status
        Params  : Lead Record Id
    */    
    @AuraEnabled
    public static String processAcceptance(String strRecordId) {
        String strResponse;
        try{
            Lead leadRec = new Lead(Id = strRecordId);
            leadRec.OwnerId = UserInfo.getUserId();
            leadRec.Partner_Accepted_Date__c = DateTime.now();
            leadRec.Reset_SLA__c = false;
            update leadRec;
            strResponse = 'success';
            
            // Share the Lead with the Partner queue users
            List<User> lstUser = [ SELECT Contact.Account.Name, Contact.Account.Account_ID__c FROM User WHERE Id =: UserInfo.getUserId() ];
            if(lstUser.size() > 0){
                User prtnrUsr = lstUser[0];
                String prtnrAccID;
                // Create a queue name
                if(prtnrUsr.Contact.account.Name.Length() > 30){
                    prtnrAccID = prtnrUsr.Contact.account.Name.trim().substring(0,29).replace(' ', '_') + '_' + prtnrUsr.Contact.account.Account_ID__c.replace('-', '').replace('ACCT','');
                }else{
                    prtnrAccID = prtnrUsr.Contact.account.Name.trim().replace(' ', '_') + '_' + prtnrUsr.Contact.account.Account_ID__c.replace('-', '').replace('ACCT','');
                }

                // Create Lead Sharing rule
                List<group> lstGroup = [ SELECT id, developername FROM group WHERE type = 'Regular' AND developername =: prtnrAccID ];
                if(lstGroup.size() > 0){
                    LeadShare shareRec = new LeadShare();
                    shareRec.LeadId = strRecordId;
                    shareRec.UserOrGroupId = lstGroup[0].id;
                    shareRec.LeadAccessLevel = 'edit';
                    insert shareRec;                
                }
            }
        }
        Catch(Exception ex){
            strResponse = 'error';
            GlobalUtility.logMessage('Error','ICT Partner Program Community','Set Lead Owner','','Set Lead Owner','processAcceptance method error','',ex,0);
        }       
        return strResponse;
    }

    /*
        Method  : processAcceptance
        Return  : Acceptance status
        Params  : Lead Record Id
    */    
    @AuraEnabled
    public static String processDecline(String strRecordId, String strDeclineReason) {
        String strResponse;
        try{
			List<Lead> lstLead = [ SELECT Id, Qualification__c FROM Lead WHERE Id =: strRecordId];
            system.debug('-----lstLead---' + lstLead);
            Lead leadRec = new Lead(Id = strRecordId);
            leadRec.OwnerId = Label.ICTSMBQueue;
            leadRec.Partner_Declined_Date__c = DateTime.now();
            leadRec.Partner_Accepted_Date__c = null;
            leadRec.Decline_Reason__c = strDeclineReason;
            if(!lstLead.isEmpty()){
                system.debug('-----Record---' + lstLead[0].Qualification__c);
                if(string.isBlank(lstLead[0].Qualification__c)){
                    leadRec.Status = 'Open';
                }else{
                    leadRec.Status = lstLead[0].Qualification__c;
                }
            }
            else{
                leadRec.Status = 'Open';
            }
            leadRec.Reset_SLA__c = false;
            update leadRec;
            strResponse = 'success';
        }
        Catch(Exception ex){
            strResponse = 'error';
            GlobalUtility.logMessage('Error','ICT Partner Program Community','Set Lead Owner','','Set Lead Owner','processDecline method error','',ex,0);
        }       
        return strResponse;
    }
    
    /*
        Method  : getSalutationPicklistValues
        Return  : List of string containing picklist values of Salutaion on Lead Object
        Params  : None
    */    
    @AuraEnabled
    public static List<String> getSalutationPicklistValues() {
        List<String> salutationOptions = new List <String> ();
        salutationOptions.add('Please choose');
        List<String> lstSalutations = System.Label.ICTLeadSalutation.split(',');
        for(String s : lstSalutations){
            salutationOptions.add(s);
        }  
        
        return salutationOptions;
    }

    /*
        Method  : getPreferredContactPicklistValues
        Return  : List of string containing picklist values of Preferred Contact on Lead Object
        Params  : None
    */    
    @AuraEnabled
    public static List<String> getPreferredContactPicklistValues() {
        List<String> preferredOptions = new List <String> ();
        
        List<String> lstPreferredMethod = System.Label.ICTPreferredMethod.split(',');
        for(String s : lstPreferredMethod){
            preferredOptions.add(s);
        } 
        
        return preferredOptions;
    }
    
    /*
        Method  : getCompanySizePicklistValues
        Return  : List of string containing picklist values of Company Size on Lead Object
        Params  : None
    */    
    @AuraEnabled
    public static List<String> getCompanySizePicklistValues() {
        
        List<String> companysizeOptions = new List <String> ();
        Schema.sObjectType objType = Lead.getSObjectType();
        
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();        
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        companysizeOptions.add('Please choose');
        List<Schema.PicklistEntry> values = fieldMap.get('Company_Size__c').getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
            companysizeOptions.add(a.getValue());
        }
        
        return companysizeOptions;
    }
    
    /*
        Method  : getDeclineReasonPicklistValues
        Return  : List of string containing picklist values of Decline_Reason__c on Lead Object
        Params  : None
    */    
    @AuraEnabled
    public static List<String> getDeclineReasonPicklistValues() {
        List<String> declineOptions = new List <String> ();

        Schema.sObjectType objType = Lead.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();        
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        declineOptions.add('Please select decline reason');
        List<Schema.PicklistEntry> values = fieldMap.get('Decline_Reason__c').getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
            declineOptions.add(a.getValue());
        }
        
        return declineOptions;
    }
    
    /*
        Method  : saveLeadRecord
        Return  : response as 'success' or 'error'
        Params  : Lead fields update
    */
    @AuraEnabled
    public static String saveLeadRecord(String strRecordId, String strSalutation, String strFirstName, String strLastName, String strEmail, String strCompany, String strTitle, String strPhone, String strMobile, String strPreferredContact, String strTradingName, String strStreet, String strCity, String strState, String strPostCode) {
        String strResponse;
        try{
            Lead leadRec = new Lead(Id = strRecordId);
            leadRec.Salutation = strSalutation;
            leadRec.FirstName = strFirstName;
            leadRec.LastName = strLastName;
            leadRec.Email = strEmail;
            leadRec.Company = strCompany;
            leadRec.Title__c = strTitle;
            leadRec.Phone = strPhone;
            leadRec.MobilePhone = strMobile;
            leadRec.Preferred_Contact_Method__c = strPreferredContact;
            leadRec.Trading_Name__c = strTradingName;
            leadRec.Street = strStreet;
            leadRec.City = strCity;
            leadRec.State = strState;
            leadRec.PostalCode = strPostCode;
            
            update leadRec;
            strResponse = 'success';
        }Catch(Exception ex){
            GlobalUtility.logMessage('Error','ICT Partner Program Community','Lead Save','','Lead Save','saveLeadRecord method error','',ex,0);
            strResponse = 'error';
        }
        return strResponse;
    }
    
    @AuraEnabled 
    public static List<LeadViewWrapper> leadViewOptions(){
        List<LeadViewWrapper> leadViewList = new List<LeadViewWrapper>();
        leadViewList.add(new LeadViewWrapper('All_End_Customer_Leads','All End Customer Leads'));
        leadViewList.add(new LeadViewWrapper('My_End_Customer_Leads','My End Customer Leads')); 
        return leadViewList;
    }
    
    public class LeadViewWrapper {
        @AuraEnabled
        public String strViewValue {get;set;}
        @AuraEnabled
        public String strViewLabel {get;set;}
        
        public LeadViewWrapper(String val, String label){
            this.strViewValue = val;
            this.strViewLabel = label;
        }
    }    
}