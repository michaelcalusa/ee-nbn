/***************************************************************************************************
    Class Name  :  ICTLeadControllerTest
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  Dec 04,2018 
    Function    :  This class contains unit test scenarios for ICTLeadController
    Used in     :  None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            Dec 04,2018                Created
****************************************************************************************************/

@isTest(seeAllData = false)
public Class ICTLeadControllerTest{
    static testMethod void TestMethod_ICTLeadController() {
        Test.startTest();
        
        String strConRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRecordTypeID = rtMapByName.get('Partner Contact').getRecordTypeId();    
        
        String strAccRecordTypeID;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Partner Account').getRecordTypeId();  
        
        String strLeadRecordTypeID;
        Schema.DescribeSObjectResult resultC = Schema.SObjectType.Lead; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameC = resultC.getRecordTypeInfosByName();
        strLeadRecordTypeID = rtMapByNameC.get('Business End Customer').getRecordTypeId();  
        
        Account acc = new Account(Name = 'test ICT Comm', RecordTypeID = strAccRecordTypeID);
        insert acc;
        
        Contact con = new Contact(LastName = 'LastName123', Email = 'test123@force.com', RecordTypeID = strConRecordTypeID, AccountId = acc.Id);
        insert con;
        
        List<Profile> lstProfile = [ SELECT Id FROM Profile WHERE Name = 'ICT Partner Community User'];
        
        
        if(!lstProfile.isEmpty()){
            // Create community user
            User commUser = new User();
            commUser.Username = 'test123@force.com.ICT';
            commUser.Email = 'test123@force.com';
            commUser.FirstName = 'firstname12';
            commUser.LastName = 'lastname123';
            commUser.CommunityNickname = '123ictcomm8848';
            commUser.ContactId = con.ID; 
            commUser.ProfileId = lstProfile[0].Id;
            commUser.Alias = 'tict1'; 
            commUser.TimeZoneSidKey = 'Australia/Sydney'; 
            commUser.LocaleSidKey = 'en_US'; 
            commUser.EmailEncodingKey = 'UTF-8'; 
            commUser.LanguageLocaleKey = 'en_US'; 
            insert commUser;
            
            Lead leadRec = new Lead();
            leadRec.FirstName = 'test';
            leadRec.LastName = 'last';
            leadRec.Email = 'test@gmail.com';
            leadRec.Company = 'ÁBC';
            leadRec.Phone = '0123456789';
            leadRec.Preferred_Contact_Method__c = 'Email';
            leadRec.Status = 'Ópen';
            leadRec.LeadSource = 'Conference';
            leadRec.OwnerId = commUser.Id;
            insert leadRec;

            system.runAs(commUser){
                
                
                String strStatus = ICTLeadController.saveLeadRecord(leadRec.Id, 'Mr', 'fn', 'ln', 'test@gmail.com', 'Telstra', 'test', '0123456789', '', 'Émail', '', '', '', '', '');
                
                Lead l = ICTLeadController.getLeadRecord(leadRec.Id);
                ICTLeadController.getSalutationPicklistValues();
                ICTLeadController.getPreferredContactPicklistValues();
                ICTLeadController.getCompanySizePicklistValues();
                ICTLeadController.leadViewOptions();
                ICTLeadController.processAcceptance(leadRec.Id);
                ICTLeadController.processDecline(leadRec.Id, 'Not Interested');
                ICTLeadController.getDeclineReasonPicklistValues();
            }
            
            Test.stoptest();
        }
    }
}