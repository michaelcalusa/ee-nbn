/***************************************************************************************************
    Class Name          : ICTLoginController
    Version             : 1.0 
    Created Date        : 22-March-2018
    Author              : Rupendra Kumar Vats
    Description         : Logic to 'Sign in' for ICT community
    Test Class          : ICTLoginControllerTest

    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Rupendra Kumar Vats       
****************************************************************************************************/ 
public without sharing class ICTLoginController {

    public ICTLoginController() {
        
    }

    @AuraEnabled
    public static String login(String username, String password, String startUrl) {
        try{
            String strUserName = username + Label.ICT_User_Suffix;
            String strLoginUserName;
            List<User> lstUser = [ SELECT Id, IsActive, UserName FROM User WHERE (UserName =: strUserName OR  UserName =: username) AND Profile.Name IN ('ICT Partner Community User', 'ICT Customer Community Plus User') AND isPortalEnabled = true];
            if(lstUser.size() > 0){
                if(lstUser[0].IsActive){
                    strLoginUserName = lstUser[0].UserName;
                    ApexPages.PageReference lgn = Site.login(strLoginUserName, password, startUrl);
                    aura.redirect(lgn); 
                }
                else{
                    List<ICT_Error_Messages__mdt> lstErrorMsg = [ SELECT Error_Code__c, Error_Message__c FROM ICT_Error_Messages__mdt WHERE Error_Code__c = '001'];
                    
                    if(lstErrorMsg.size() > 0){
                        return lstErrorMsg[0].Error_Message__c;
                    }
                }
            }
            else{
                ApexPages.PageReference lgn = Site.login(username, password, startUrl);
                aura.redirect(lgn);
            }
            return null;
        }
        catch (Exception ex) {
            return ex.getMessage();            
        }
    }
    
    @AuraEnabled
    public static Boolean getIsUsernamePasswordEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getUsernamePasswordEnabled();
    }

    @AuraEnabled
    public static Boolean getIsSelfRegistrationEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getSelfRegistrationEnabled();
    }

    @AuraEnabled
    public static String getSelfRegistrationUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        if (authConfig.getSelfRegistrationEnabled()) {
            return authConfig.getSelfRegistrationUrl();
        }
        return null;
    }

    @AuraEnabled
    public static String getForgotPasswordUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getForgotPasswordUrl();
    }
    
    private static Auth.AuthConfiguration getAuthConfig(){
        Id networkId = Network.getNetworkId();
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(networkId,'');
        return authConfig;
    }
}