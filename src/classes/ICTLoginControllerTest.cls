/***************************************************************************************************
    Class Name  :  ICTLoginControllerTest
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  March 24,2018
    Function    :  This class contains unit test scenarios for ICTLoginController
    Used in     :  None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            March 24,2018                 Created
****************************************************************************************************/


//Replace by Release branch


@isTest(seeAllData = false)
public Class ICTLoginControllerTest{
    static testMethod void TestMethod_Login() {
        String strConRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRecordTypeID = rtMapByName.get('Partner Contact').getRecordTypeId();    
        
        String strAccRecordTypeID;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Partner Account').getRecordTypeId();  
        
        Account acc = new Account(Name = 'test ICT Comm', RecordTypeID = strAccRecordTypeID);
        insert acc;
        
        Contact con = new Contact(LastName = 'LastName', Email = 'test@force.com', RecordTypeID = strConRecordTypeID, AccountId = acc.Id);
        insert con;
        
        /*
        lmscons__Training_Path__c  testCourse = new lmscons__Training_Path__c();
        testCourse.Name = 'ICT Channel Partner Program';
        testCourse.lmscons__Enable_Certificate__c = true;
        insert testCourse;
        
        ICTAdditionalInformationController.roleOptions();  
        ICTAdditionalInformationController.businessOptions();  */
        // Get the community user profile
        List<Profile> lstProfile = [ SELECT Id FROM Profile WHERE Name = 'ICT Customer Community Plus User'];
        if(!lstProfile.isEmpty()){
            // Create community user
            User commUser = new User();
            commUser.Username = 'test@force.com.ict';
            commUser.Email = 'test@force.com';
            commUser.FirstName = 'firstname';
            commUser.LastName = 'lastname';
            commUser.CommunityNickname = 'ictlogincomm';
            commUser.ContactId = con.ID; 
            commUser.ProfileId = lstProfile[0].Id;
            commUser.Alias = 'tict'; 
            commUser.TimeZoneSidKey = 'Australia/Sydney'; 
            commUser.LocaleSidKey = 'en_US'; 
            commUser.EmailEncodingKey = 'UTF-8'; 
            commUser.LanguageLocaleKey = 'en_US'; 
            insert commUser;
        }
        ICTLoginController login = new ICTLoginController();
        ICTLoginController.login('test@force.com.ict','login123','/home');
        ICTLoginController.login('test@force.com.rsp','','/home');
        ICTLoginController.getIsUsernamePasswordEnabled();
        ICTLoginController.getIsSelfRegistrationEnabled();
        ICTLoginController.getSelfRegistrationUrl();
        ICTLoginController.getForgotPasswordUrl();
    }
}