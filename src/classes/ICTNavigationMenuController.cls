/***************************************************************************************************
    Class Name          : ICTNavigationMenuController
    Version             : 1.0 
    Created Date        : 25-03-2018
    Author              : Rupendra Kumar Vats
    Description         : Dynamic navigation menu based on ICT community user role
    Test Class          : ICTNavigationMenuControllerTest

    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Rupendra Kumar Vats  25-03-2018
    * Jairaj jadhav        24-08-2018       Changed returntype to map to check Contact Role and License type as part of US#13526.
****************************************************************************************************/
public without sharing  class ICTNavigationMenuController{
@AuraEnabled
    public static map<string, boolean> checkCertifiedUser() {
        map<string, boolean> certifiedPrtnrCommUsrMap = new map<string, boolean>();
        Boolean isCertifiedUser = false;
        try{
            List<User> lstUser = [SELECT ContactId,contact.ICT_Contact_Role__c,User_License__c FROM User WHERE Id =: UserInfo.getUserId() AND ContactId != NULL];
            
            if(!lstUser.isEmpty()){
                // User is a community user so give visibility according to role
                //List<Learning_Record__c> lstRecs = [ SELECT Course_Status__c FROM Learning_Record__c WHERE Contact__c =: lstUser[0].ContactId AND Course_Status__c = 'Certified' ];
                List<Contact> lstCon = [ SELECT ICT_Home_View__c, ICT_Resource_View__c FROM Contact WHERE ID =: lstUser[0].ContactId ];
                
                if(!lstCon.isEmpty()){
                    if(lstCon[0].ICT_Home_View__c == 'Non Training' && (lstCon[0].ICT_Resource_View__c == 'Certified' || lstCon[0].ICT_Resource_View__c == 'Marketing' || lstCon[0].ICT_Resource_View__c == 'All')){
                        isCertifiedUser = true;
                    }
                    else{
                        isCertifiedUser = false;
                    }
                }
                certifiedPrtnrCommUsrMap.put('isPartnerCommunityUser', (lstUser[0].User_License__c == 'Partner Community') ? true: false);
                certifiedPrtnrCommUsrMap.put('isSalesDelegateUser', lstUser[0].contact.ICT_Contact_Role__c.contains('Sales Delegate') ? true: false);
            }else{
                // User is a Salesforce user so show all tabs
                isCertifiedUser = true;
                certifiedPrtnrCommUsrMap.put('isPartnerCommunityUser', false);
                certifiedPrtnrCommUsrMap.put('isSalesDelegateUser', false);
            }
            certifiedPrtnrCommUsrMap.put('isCertifiedUser', isCertifiedUser);
            
        }Catch(Exception ex){
            system.debug('----Exception: ICTNavigationMenuController---' + ex.getMessage());
        }
        return certifiedPrtnrCommUsrMap;
    }
    
    @AuraEnabled
    public static Boolean identifyAccessLevel(String strURL){
        Boolean isUserAllowed = true;
        string url = strURL;
        try{
            if(string.isNotBlank(url) && string.isNotBlank(url.substringAfterLast('/'))){
                boolean validPage = label.ICTPagesForCertifiedOrPartner.contains(url.substringAfterLast('/'));
                user commUser = [select ContactID, id, Contact.ICT_Contact_Role__c,Contact.ICT_Home_View__c,Contact.ICT_Resource_View__c, User_License__c from user where id =: userInfo.getUserID()];
                
                boolean nonOpptyPage = (url.substringAfterLast('/') == 'resources' || url.substringAfterLast('/') == 'pre-sales-support' || url.substringAfterLast('/') == 'mycertificates');
                
                boolean certifiedUser = commUser.Contact.ICT_Home_View__c == 'Non Training' && (commUser.Contact.ICT_Resource_View__c == 'Certified' || commUser.Contact.ICT_Resource_View__c == 'Marketing' || commUser.Contact.ICT_Resource_View__c == 'All');
                
                boolean opptyPageAccess = ((url.substringAfterLast('/') == 'opportunities' || url.substringAfterLast('/') == 'opportunity-view') && commUser.Contact.ICT_Contact_Role__c.contains('Sales Delegate') && commUser.User_License__c == 'Partner Community');
                isUserAllowed = validPage? (opptyPageAccess? true: (nonOpptyPage && certifiedUser)? true: false): true;             
            }
            else{
                isUserAllowed = true;
            }
        }catch(exception ex){
            GlobalUtility.logMessage('Error','ICT Partner Program Community','Page Access Check','','Community Page Access Check','identifyAccessLevel Error','',ex,0);
            isUserAllowed = false;
        }
        return isUserAllowed;
    }
}