/***************************************************************************************************
    Class Name  :  ICTNavigationMenuControllerTest
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  Feb 28,2018 
    Function    :  This class contains unit test scenarios for ICTNavigationMenuController
    Used in     :  None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            Feb 28,2018                 Created
****************************************************************************************************/

@isTest(seeAllData = false)
public Class ICTNavigationMenuControllerTest{
    static testMethod void TestMethod_ICTNavigation() {
        String strConRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRecordTypeID = rtMapByName.get('Partner Contact').getRecordTypeId();    
        
        String strAccRecordTypeID;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Partner Account').getRecordTypeId();  
        
        Account acc = new Account(Name = 'test ICT Comm', RecordTypeID = strAccRecordTypeID);
        insert acc;
        
        Contact con = new Contact(LastName = 'LastName', Email = 'test@force.com', RecordTypeID = strConRecordTypeID, AccountId = acc.Id);
        insert con;

        lmscons__Training_Path__c  testCourse = new lmscons__Training_Path__c();
        testCourse.Name = 'ICT Channel Partner Program';
        testCourse.lmscons__Enable_Certificate__c = true;
        insert testCourse;
        
        Learning_Record__c lr = new Learning_Record__c(Contact__c = con.Id, Account__c = acc.Id, Course_Status__c = 'Certified');
        insert lr;
        
        ICTNavigationMenuController.checkCertifiedUser();
        
        // Get the community user profile
        List<Profile> lstProfile = [ SELECT Id FROM Profile WHERE Name = 'ICT Customer Community Plus User'];
        if(!lstProfile.isEmpty()){
            // Create community user
            User commUser = new User();
            commUser.Username = 'test@force.com.ICT';
            commUser.Email = 'test@force.com';
            commUser.FirstName = 'firstname';
            commUser.LastName = 'lastname';
            commUser.CommunityNickname = 'ictcomm8848';
            commUser.ContactId = con.ID; 
            commUser.ProfileId = lstProfile[0].Id;
            commUser.Alias = 'tict'; 
            commUser.TimeZoneSidKey = 'Australia/Sydney'; 
            commUser.LocaleSidKey = 'en_US'; 
            commUser.EmailEncodingKey = 'UTF-8'; 
            commUser.LanguageLocaleKey = 'en_US'; 
            insert commUser;

            system.runAs(commUser){
                ICTNavigationMenuController.checkCertifiedUser();
                ICTNavigationMenuController.identifyAccessLevel('ictpartnerprogram/s/opportunities');
                ICTNavigationMenuController.identifyAccessLevel('ictpartnerprogram/s/');
            }
        }
    }
}