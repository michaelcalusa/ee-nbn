/***************************************************************************************************
    Class Name          : ICTNotificationsController
    Version             : 1.0 
    Created Date        : 14-Nov-2017
    Author              : Rupendra Kumar Vats
    Description         : Logic to display notifications for community user's pending tasks
    Test Class          : ICTNotificationsControllerTest

    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Rupendra Kumar Vats       
****************************************************************************************************/ 
global without sharing class ICTNotificationsController{
    @AuraEnabled
    public static Boolean userNotifications() {
        Boolean hasNotification = false;
        try{
            List<User> lstUser = [ SELECT Id, ContactId FROM User WHERE Id =: UserInfo.getUserId() ];
            system.debug('--lstUser--' + lstUser);
            if(!lstUser.isEmpty()){
                List<Learning_Record__c> lstLearningRecords = [ SELECT id, Course_Status__c from Learning_Record__c where Contact__r.id =: lstUser[0].contactID AND Contact__r.Additional_Information_Provided__c = false AND Course_Status__c = 'Completed' ];
                system.debug('--lstLearningRecords--' + lstLearningRecords);
                if(!lstLearningRecords.isEmpty()){
                    hasNotification = true;
                }
            }   
        }
        Catch(Exception ex){
            system.debug('--ICTNotificationsController--userNotifications--' + ex.getMessage());
        }
        return hasNotification;
    }
    
    @AuraEnabled
    public static Boolean userResourceNotifications() {
        Boolean hasNotification = false;
        try{
            List<User> lstUser = [ SELECT Id, ContactId FROM User WHERE Id =: UserInfo.getUserId() ];
            system.debug('--lstUser--' + lstUser);
            if(!lstUser.isEmpty()){
                List<Learning_Record__c> lstLearningRecords = [ SELECT id, Course_Status__c from Learning_Record__c where Contact__r.id =: lstUser[0].contactID AND Contact__r.Additional_Information_Provided__c = true AND Course_Status__c = 'Certified' ];
                system.debug('--lstLearningRecords--' + lstLearningRecords);
                if(lstLearningRecords.isEmpty()){
                    hasNotification = true;
                }
            }   
        }
        Catch(Exception ex){
            system.debug('--ICTNotificationsController--userResourceNotifications--' + ex.getMessage());
        }
        return hasNotification;
    }
}