/***************************************************************************************************
    Class Name  :  ICTNotificationsControllerTest
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  Nov 14,2017 
    Function    :  This class contains unit test scenarios for ICTNotificationsController
    Used in     :  None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            Nov 12,2017                 Created
****************************************************************************************************/

@isTest(seeAllData = false)
public Class ICTNotificationsControllerTest{
    static testMethod void TestMethod_ICTNotifications() {
        try{
            String strConRecordTypeID;
            Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
            Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
            strConRecordTypeID = rtMapByName.get('Partner Contact').getRecordTypeId();    
            
            String strAccRecordTypeID;
            Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
            Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
            strAccRecordTypeID = rtMapByNameB.get('Partner Account').getRecordTypeId();  
            
            Account acc = new Account(Name = 'test ICT Comm', RecordTypeID = strAccRecordTypeID);
            insert acc;
            
            Contact con = new Contact(LastName = 'LastName', Email = 'test@force.com', RecordTypeID = strConRecordTypeID, AccountId = acc.Id);
            insert con;
            
            Learning_Record__c lr = new Learning_Record__c(Course_Status__c = 'Completed', Contact__c = con.Id);
            insert lr;
            
            lmscons__Training_Path__c  testCourse = new lmscons__Training_Path__c();
            testCourse.Name = 'ICT Channel Partner Program';
            testCourse.lmscons__Enable_Certificate__c = true;
            insert testCourse;
                
            // Get the community user profile
            List<Profile> lstProfile = [ SELECT Id FROM Profile WHERE Name = 'ICT Customer Community Plus User'];
            if(!lstProfile.isEmpty()){
                // Create community user
                UserRole r = new UserRole(name = 'TEST ROLE', PortalType='CustomerPortal', PortalAccountId=acc.Id);
                User commUser = new User();
                commUser.Username = 'test@force.com.ICT';
                commUser.Email = 'test@force.com';
                commUser.FirstName = 'firstname';
                commUser.LastName = 'lastname';
                commUser.userroleid = r.id;
                commUser.CommunityNickname = 'ictcomm8848';
                commUser.ContactId = con.ID; 
                commUser.ProfileId = lstProfile[0].Id;
                commUser.Alias = 'tict'; 
                commUser.TimeZoneSidKey = 'Australia/Sydney'; 
                commUser.LocaleSidKey = 'en_US'; 
                commUser.EmailEncodingKey = 'UTF-8'; 
                commUser.LanguageLocaleKey = 'en_US'; 
                insert commUser;

                system.runAs(commUser){
                    Contact c = ICTAdditionalInformationController.getContactInformation();
                    
                    Boolean hasNotification = ICTNotificationsController.userNotifications();
                }
                
                lr.Course_Status__c = 'Certified';
                update lr;
                
                con.Additional_Information_Provided__c = true;
                update con;
                
                system.runAs(commUser){
                    Boolean hasUserNotification = ICTNotificationsController.userResourceNotifications();
                }
            }   
        }Catch(Exception ex){
            system.debug('---Exception--UserTriggerHandler_Test--TestMethod_ICTNotifications()--' + ex.getMessage());
        }
    }
}