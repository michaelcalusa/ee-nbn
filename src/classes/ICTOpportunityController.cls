/********************************************************
* Class Name    : ICTOpportunityController
* Description   : Controller class for ICT_Opportunity_Creation Lightning component
* Created By    : Rupendra Kumar Vats
* Date          : 28th August, 2018
* Change History :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Rupendra Vats           August 28, 2018                 Created
* Jairaj Jadhav           October 22, 2018                Defect#17996 - Modified it with logic to return the user access on opportunity based on contact role.
* ----------------------------------------------------------------------------
*/
 
public With Sharing class ICTOpportunityController{

    /*
        Method  : getOpportunityRecord
        Return  : A wrapper object contatining info on opportunity. Method is invoked on INIT
        Params  : Opportunity Record Id
    */
    @AuraEnabled
    public static OpportunityWrapper getOpportunityRecord(String strRecordId) {
        OpportunityWrapper oppwrp;
        boolean hasOpptyAccess = true;
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Business Segment ICT').getRecordTypeId();
        user loggdInUser = [select id, contact.Is_Sales_Delegate_User__c from user where contactID!=null AND id=: userInfo.getUserID()];
        if(loggdInUser != null){
            hasOpptyAccess = loggdInUser.contact.Is_Sales_Delegate_User__c;
        }
        try{
            List<Opportunity> lstOpp = [ SELECT Id, Name, Account.Name, Account.ABN__c, Total_Sites__c, Comments__c, StageName, CloseDate, Site_Information__c, Trading_Name__c, Number_of_services_TC2_5_10Mbps__c, Number_of_services_TC2_20_100Mbps__c, Number_of_services_TC2_100Mbps__c, Number_of_services_TC4__c, Number_of_services_TC1__c, Number_of_services_eSLA__c FROM Opportunity WHERE ID =: strRecordId ];
            
            if(lstOpp.size() > 0){
                List<OpportunityContactRole> lstOptConRoles = [ SELECT ContactId FROM OpportunityContactRole WHERE OpportunityId =: strRecordId AND IsPrimary = true ];
                if(lstOptConRoles.size() > 0){
                    List<Contact> lstContact = [ SELECT Id, Salutation, FirstName, LastName, RecordTypeId, Email, AccountId, Job_Title__c, Phone, MobilePhone, Preferred_Contact_Method__c, MailingStreet, MailingCity, MailingState, MailingPostalCode FROM Contact WHERE Id =: lstOptConRoles[0].ContactId Order by LastModifiedDate DESC ];
                    
                    oppwrp = new OpportunityWrapper(oppRecordTypeId, lstOpp[0], lstContact[0], hasOpptyAccess);
                }
                else{
                    oppwrp = new OpportunityWrapper(oppRecordTypeId, lstOpp[0], new Contact(), hasOpptyAccess);
                }
            }
            else{
                oppwrp = new OpportunityWrapper(oppRecordTypeId, new Opportunity(), new Contact(), hasOpptyAccess);
            }
        }
        Catch(Exception ex){
            oppwrp = new OpportunityWrapper(oppRecordTypeId, new Opportunity(), new Contact(), hasOpptyAccess);
        }
        return oppwrp;
    }

    /*
        Method  : getLeadRecord
        Return  : Lead record inforamtion
        Params  : Lead Record Id
    */
    @AuraEnabled
    public static LeadWrapper getLeadRecord(String strRecordId) {
        LeadWrapper wrapper;
        Lead leadRecord;
        ABNSearchController.ABNInformation abnInformation;
        try{
            List<Lead> lstLead = [ SELECT Id, OwnerId, Salutation, FirstName, LastName, Email, Company, Title__c, Phone, MobilePhone, Preferred_Contact_Method__c, ABN__c, Trading_Name__c, Company_Size__c, Street, City, State, PostalCode FROM Lead WHERE ID =: strRecordId ];
            
            if(!lstLead.isEmpty()){
                leadRecord = lstLead[0];
                if(!string.isEmpty(leadRecord.ABN__c)){
                    abnInformation =  ABNSearchController.doABNSearch(leadRecord.ABN__c);
                    wrapper = new LeadWrapper(abnInformation.lstBusinessName[0], leadRecord);                   
                }
                else{
                    wrapper = new LeadWrapper('', leadRecord);
                }
            }
            else{
                wrapper = new LeadWrapper('', leadRecord);
            }
        }
        Catch(Exception ex){
            wrapper = new LeadWrapper('', leadRecord);
            GlobalUtility.logMessage('Error','ICT Partner Program Community','Get lead record','','Get lead record','getLeadRecord method error','',ex,0);
        }
        return wrapper;
    }
    
    @AuraEnabled 
    public static List<string> phaseOptions(){
        List<string> phaseOptList = new List<string>();
        phaseOptList.add('Please choose');
        List<String> lstAvailableStages = System.Label.ICTOppStages.split(',');
        for(String stageval : lstAvailableStages){
            phaseOptList.add(stageval);
        }  
        system.debug('--phaseOptList--' + phaseOptList);
        return phaseOptList;
    }

    @AuraEnabled 
    public static List<OpportunityViewWrapper> oppViewOptions(){
        List<OpportunityViewWrapper> optViewList = new List<OpportunityViewWrapper>();
        optViewList.add(new OpportunityViewWrapper('All_End_Customer_Opportunities','All End Customer Opportunities'));
        optViewList.add(new OpportunityViewWrapper('My_End_Customer_Opportunities','My End Customer Opportunities')); 
        system.debug('--optViewList--' + optViewList);
        return optViewList;
    }
    
    /*
        Method  : saveOpportunityRecord
        Return  : Opportunity Record Id
        Params  : Opportunity fileds for creation and update
    */
    @AuraEnabled
    public static String saveOpportunityRecord(String strRecordId, String strName, String strPhase, String strCloseDate, String strABNNumber, String strEntityName, String strTradingName, String strComments, String strTotalSites, String strSiteDescription, String strNSTC5, String strNSTC20, String strNSTC2, String strNSTC4, String strNSTC1, String strNSSLA, String strSalutation, String strFirstName, String strLastName, String strEmail, String strTitle, String strPhone, String strMobile, String strPreferredContact, String strStreet, String strCity, String strState, String strPostCode, String strLeadId) {
        String strOpportunityID;
        Boolean isAccountInsert = false, isOppInsert = false, isContactProcessed = false;
        try{
            // Find the End Customer Account
            Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business End Customer').getRecordTypeId();
            String strAccountId;
            strABNNumber= strABNNumber.replaceAll( '\\s+', '');
            System.debug('---strABNNumber---' + strABNNumber);
            
            if(string.isEmpty(strTotalSites.trim())){
                strTotalSites = '0';
            }

            if(string.isEmpty(strNSTC5.trim())){
                strNSTC5 = '0';
            }

            if(string.isEmpty(strNSTC20.trim())){
                strNSTC20 = '0';
            }

            if(string.isEmpty(strNSTC2.trim())){
                strNSTC2 = '0';
            }

            if(string.isEmpty(strNSTC4.trim())){
                strNSTC4 = '0';
            }

            if(string.isEmpty(strNSTC1.trim())){
                strNSTC1 = '0';
            }

            if(string.isEmpty(strNSSLA.trim())){
                strNSSLA = '0';
            }
            
            strAccountId = ICTUtilityHelper.getAccount(accRecordTypeId, strABNNumber, strEntityName);
            if(String.isBlank(strAccountId)){
                Account accRec = new Account();
                accRec.Name = strEntityName;
                accRec.ABN__c = strABNNumber;
                accRec.RecordTypeId = accRecordTypeId;
                accRec.Account_Types__c = 'Medium';
                insert accRec;
                
                strAccountId = accRec.Id;
                isAccountInsert = true;
            }
            
            // Find the End Customer Contact
            Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business End Customer').getRecordTypeId();
            String strContactId;
             
            
            if(string.isEmpty(strRecordId)){
                strContactId = ICTUtilityHelper.getContact(conRecordTypeId, strAccountId, strLastName, strEmail);
            }else{
                List<OpportunityContactRole> lstOptConRoles = [ SELECT ContactId FROM OpportunityContactRole WHERE OpportunityId =: strRecordId AND IsPrimary = true ];
                if(lstOptConRoles.size() > 0){
                    strContactId = lstOptConRoles[0].ContactId;
                }                
            }
            if(String.isBlank(strContactId)){
                Contact conRec = new Contact();
                if(strSalutation != 'Please choose'){
                    conRec.Salutation = strSalutation;
                }
                conRec.FirstName = strFirstName;
                conRec.LastName = strLastName;
                conRec.RecordTypeId = conRecordTypeId;
                conRec.Email = strEmail;
                conRec.AccountId = strAccountId;
                conRec.Job_Title__c = strTitle;
                conRec.Phone = strPhone;
                conRec.MobilePhone = strMobile;
                if(strPreferredContact != 'Please choose'){
                    conRec.Preferred_Contact_Method__c = strPreferredContact;
                }
                conRec.MailingStreet = strStreet;
                conRec.MailingCity = strCity;
                conRec.MailingState = strState;
                conRec.MailingPostalCode = strPostCode;
                
                strContactId = ICTUtilityHelper.processContact(conRec, true);
                isContactProcessed = true;
            }
            else{
                Contact conRec = new Contact(Id = strContactId);
                if(strSalutation != 'Please choose'){
                    conRec.Salutation = strSalutation;
                }
                conRec.FirstName = strFirstName;
                conRec.LastName = strLastName;
                conRec.RecordTypeId = conRecordTypeId;
                conRec.Email = strEmail;
                conRec.AccountId = strAccountId;
                conRec.Job_Title__c = strTitle;
                conRec.Phone = strPhone;
                conRec.MobilePhone = strMobile ;
                if(strPreferredContact != 'Please choose'){
                    conRec.Preferred_Contact_Method__c = strPreferredContact;
                }
                conRec.MailingStreet = strStreet;
                conRec.MailingCity = strCity;
                conRec.MailingState = strState;
                conRec.MailingPostalCode = strPostCode;
                
                strContactId = ICTUtilityHelper.processContact(conRec, false);

                isContactProcessed = true;
            }
            
            // Process the Opportunity
            Opportunity opp;
            if(string.isEmpty(strRecordId)){
                opp = new Opportunity();
                opp.Name = strName;
                opp.StageName = strPhase;
                opp.CloseDate = date.valueOf(strCloseDate);
                opp.AccountId = strAccountId; 
                opp.Comments__c = strComments;
                opp.Total_Sites__c = decimal.valueOf(strTotalSites);
                opp.Site_Information__c = strSiteDescription;
                opp.Trading_Name__c = strTradingName;
                opp.OwnerId = UserInfo.getUserId();
                opp.Number_of_services_TC2_5_10Mbps__c = decimal.valueOf(strNSTC5);
                opp.Number_of_services_TC2_20_100Mbps__c = decimal.valueOf(strNSTC20);
                opp.Number_of_services_TC2_100Mbps__c = decimal.valueOf(strNSTC2);
                opp.Number_of_services_TC4__c = decimal.valueOf(strNSTC4);
                opp.Number_of_services_TC1__c = decimal.valueOf(strNSTC1);
                opp.Number_of_services_eSLA__c = decimal.valueOf(strNSSLA);

                strOpportunityID = ICTUtilityHelper.createOpportunity(opp, true);
                isOppInsert = true;
            }
            else{
                opp = new Opportunity(Id = strRecordId);
                opp.AccountId = strAccountId;
                opp.Name = strName;
                opp.StageName = strPhase;
                opp.CloseDate = date.valueOf(strCloseDate);
                opp.Comments__c = strComments;
                opp.Total_Sites__c = decimal.valueOf(strTotalSites);
                opp.Site_Information__c = strSiteDescription;
                opp.Trading_Name__c = strTradingName;
                opp.Number_of_services_TC2_5_10Mbps__c = decimal.valueOf(strNSTC5);
                opp.Number_of_services_TC2_20_100Mbps__c = decimal.valueOf(strNSTC20);
                opp.Number_of_services_TC2_100Mbps__c = decimal.valueOf(strNSTC2);
                opp.Number_of_services_TC4__c = decimal.valueOf(strNSTC4);
                opp.Number_of_services_TC1__c = decimal.valueOf(strNSTC1);
                opp.Number_of_services_eSLA__c = decimal.valueOf(strNSSLA);

                strOpportunityID = ICTUtilityHelper.createOpportunity(opp, false);
            }
            
            if(isAccountInsert || isOppInsert){
                List<User> lstUser = [SELECT Id, Contact.AccountId FROM User WHERE Id =: UserInfo.getUserId()];
                String strPartnerAccId = lstUser[0].Contact.AccountId;
                ICTUtilityHelper.processOpportunityRecord(strAccountId, strPartnerAccId, strOpportunityID, isAccountInsert);              
            }
            
            // Execute only while Lead Conversion
            if(isOppInsert && isContactProcessed && !string.isEmpty(strLeadId)){
                Lead l = new Lead(Id = strLeadId);
                l.Reset_SLA__c = false;
                l.OwnerId = userInfo.getUserID();
                l.ABN__c = strABNNumber;
                update l;
                
                ICTUtilityHelper.processLeadConvert(strAccountId, strContactId, strOpportunityID, strLeadId);
            }
        }Catch(Exception ex){
            GlobalUtility.logMessage('Error','ICT Partner Program Community','Opportunity Save','','Opportunity Save','saveOpportunityRecord method error','',ex,0);
        }

        return strOpportunityID;
    }

    @AuraEnabled 
    public static List<String> showOpportunityAttachments(String strOpportunityID){
        List<String> lstFildIds = new List<String>();
        try{
            if(string.isNotBlank(strOpportunityID)){
                List<ContentDocumentLink> lstDocLinks = [ SELECT ContentDocumentId FROM ContentDocumentLink where LinkedEntityId =: strOpportunityID];
                if(!lstDocLinks.isEmpty()){
                    for(ContentDocumentLink doc: lstDocLinks){
                        lstFildIds.add(doc.ContentDocumentId);
                    }
                }
            }
        }Catch(Exception ex){
            GlobalUtility.logMessage('Error','ICT Partner Program Community','Opportunity Attachments','','Opportunity Attachments','showOpportunityAttachments method error','',ex,0);
        }
        system.debug('---lstFildIds---' + lstFildIds);
        return lstFildIds;
    }
    
        /*
        Method  : getSalutationPicklistValues
        Return  : List of string containing picklist values of Salutaion on Lead Object
        Params  : None
    */    
    @AuraEnabled
    public static List<String> getSalutationPicklistValues() {
        List<String> salutationOptions = new List <String> ();
        salutationOptions.add('Please choose');
        List<String> lstSalutations = System.Label.ICTLeadSalutation.split(',');
        for(String s : lstSalutations){
            salutationOptions.add(s);
        }  
        
        return salutationOptions;
    }

    /*
        Method  : getPreferredContactPicklistValues
        Return  : List of string containing picklist values of Preferred Contact on Lead Object
        Params  : None
    */    
    @AuraEnabled
    public static List<String> getPreferredContactPicklistValues() {
        List<String> preferredOptions = new List <String> ();
        
        List<String> lstPreferredMethod = System.Label.ICTPreferredMethod.split(',');
        for(String s : lstPreferredMethod){
            preferredOptions.add(s);
        } 
        
        return preferredOptions;
    }
    
    /*
        Method  : getCompanySizePicklistValues
        Return  : List of string containing picklist values of Company Size on Lead Object
        Params  : None
    */    
    @AuraEnabled
    public static List<String> getCompanySizePicklistValues() {
        
        List<String> companysizeOptions = new List <String> ();
        Schema.sObjectType objType = Lead.getSObjectType();
        
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();        
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        companysizeOptions.add('Please choose');
        List<Schema.PicklistEntry> values = fieldMap.get('Company_Size__c').getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
            companysizeOptions.add(a.getValue());
        }
        
        return companysizeOptions;
    }
    
    /*
        Opportunity Wrapper Class to encapsulate ICT Record Type and Record Information
    */
 
    public class OpportunityWrapper {
        @AuraEnabled
        public String strRecordTypeId {get;set;}
        @AuraEnabled
        public Opportunity oppRecord {get;set;}
        @AuraEnabled
        public Contact conRecord {get;set;}
        @AuraEnabled
        public boolean hasOpptyAccess {get;set;}
        
        public OpportunityWrapper(String ICTOppRecordTypeId, Opportunity opp, Contact con, boolean hasAccess){
            this.strRecordTypeId = ICTOppRecordTypeId;
            this.oppRecord = opp;
            this.conRecord = con;
            this.hasOpptyAccess = hasAccess;
        }
    }
    
    public class OpportunityViewWrapper {
        @AuraEnabled
        public String strViewValue {get;set;}
        @AuraEnabled
        public String strViewLabel {get;set;}
        
        public OpportunityViewWrapper(String val, String label){
            this.strViewValue = val;
            this.strViewLabel = label;
        }
    }
    
    public class LeadWrapper {
        @AuraEnabled
        public String strEntityName {get;set;}
        @AuraEnabled
        public Lead leadRecord {get;set;}
        
        public LeadWrapper(String entityname, Lead l){
            this.strEntityName = entityname;
            this.leadRecord = l;
        }
    }
}