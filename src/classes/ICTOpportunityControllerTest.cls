/***************************************************************************************************
    Class Name  :  ICTOpportunityControllerTest
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  Sep 12,2018 
    Function    :  This class contains unit test scenarios for ICTOpportunityController
    Used in     :  None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            Sep 12,2018                Created
****************************************************************************************************/

@isTest(seeAllData = false)
public Class ICTOpportunityControllerTest{
    static testMethod void TestMethod_ICTOpportunityController() {
        Test.startTest();
        
        String strConRecordTypeID, strConRTEndCust;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRecordTypeID = rtMapByName.get('Partner Contact').getRecordTypeId();    
        strConRTEndCust = rtMapByName.get('Business End Customer').getRecordTypeId();  
        
        String strAccRecordTypeID, strAccRTEndCust;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Partner Account').getRecordTypeId();  
        strAccRTEndCust = rtMapByNameB.get('Business End Customer').getRecordTypeId();  
        
        Account acc = new Account(Name = 'test ICT Comm', RecordTypeID = strAccRecordTypeID, ABN__c = '33051775556');
        insert acc;
        
        Contact con = new Contact(LastName = 'LastName123', Email = 'test123@force.com', RecordTypeID = strConRecordTypeID, AccountId = acc.Id);
        insert con;

        Account accA = new Account(Name = 'Test End', RecordTypeID = strAccRTEndCust, ABN__c = '33051775556');
        insert accA;
        
        Contact conA = new Contact(LastName = 'LastLead', Email = 'test@force.com', RecordTypeID = strConRTEndCust, AccountId = accA.Id);
        insert conA;
        
        List<Profile> lstProfile = [ SELECT Id FROM Profile WHERE Name = 'ICT Partner Community User'];
        
        
        if(!lstProfile.isEmpty()){
            // Create community user
            User commUser = new User();
            commUser.Username = 'test123@force.com.ICT';
            commUser.Email = 'test123@force.com';
            commUser.FirstName = 'firstname12';
            commUser.LastName = 'lastname123';
            commUser.CommunityNickname = '123ictcomm8848';
            commUser.ContactId = con.ID; 
            commUser.ProfileId = lstProfile[0].Id;
            commUser.Alias = 'tict1'; 
            commUser.TimeZoneSidKey = 'Australia/Sydney'; 
            commUser.LocaleSidKey = 'en_US'; 
            commUser.EmailEncodingKey = 'UTF-8'; 
            commUser.LanguageLocaleKey = 'en_US'; 
            insert commUser;
            
            Lead leadRec = new Lead();
            leadRec.FirstName = 'test';
            leadRec.LastName = 'last';
            leadRec.Email = 'test@gmail.com';
            leadRec.Company = 'ABC';
            leadRec.Phone = '0123456789';
            leadRec.Preferred_Contact_Method__c = 'Email';
            leadRec.Status = 'Open';
            leadRec.LeadSource = 'Conference';
            leadRec.OwnerId = commUser.Id;
            insert leadRec;
            /*
            Group g1 = new Group(Name='ICT Channel Team', type='Queue');
                insert g1;
                QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
                insert q1;
             */
            system.runAs(commUser){
                String strOppIdA = ICTOpportunityController.saveOpportunityRecord(null, 'test opp', 'Account Allocated', string.valueOf(Date.today()), '33051775556', 'TELSTRA CORPORATION LIMITED', 'Telstra', 'test comments', '24', 'test data','2', '2', '2', '2', '2', '2', 'Dr', 'strFirstName', 'last', 'test@gmail.com', 'Doctor', '0123456789', '0123456789', 'Email', '320 pitt st', 'sydney', 'nsw', '2009', leadRec.Id);
                String strOppIdB = ICTOpportunityController.saveOpportunityRecord(strOppIdA, 'test opp', 'Account Allocated', string.valueOf(Date.today()), '33051775556', 'TELSTRA CORPORATION LIMITED', 'Telstra', 'test comments', '24', 'test data','2', '2', '2', '2', '2', '2', 'Dr', 'strFirstName', 'last', 'test@gmail.com', 'Doctor', '0123456789', '0123456789', 'Email', '320 pitt st', 'sydney', 'nsw', '2009', null);
                String strOppIdC = ICTOpportunityController.saveOpportunityRecord(strOppIdA, 'test opp', 'Account Allocated', '27/10/2019', '33051775556', 'TELSTRA CORPORATION LIMITED', 'Telstra', 'test comments', '24', 'test data','2', '2', '2', '2', '2', '2', 'Dr', 'strFirstName', 'last', 'test@gmail.com', 'Doctor', '0123456789', '0123456789', 'Email', '320 pitt st', 'sydney', 'nsw', '2009', null);
                String strOppIdE = ICTOpportunityController.saveOpportunityRecord(strOppIdA, 'test oppD', 'Account Allocated', string.valueOf(Date.today()), '33051775556', 'Test End', 'Test End', 'test comments', '24', 'test data','2', '2', '2', '2', '2', '2', 'Dr', 'strFirstName', 'LastLead', 'test@force.com', 'Doctor', '0123456789', '0123456789', 'Email', '320 pitt st', 'sydney', 'nsw', '2009', null);
                String strOppIdD = ICTOpportunityController.saveOpportunityRecord(null, 'test oppD', 'Account Allocated', string.valueOf(Date.today()), '33051775556', 'Test End', 'Test End', 'test comments', '24', 'test data','2', '2', '2', '2', '2', '2', 'Dr', 'strFirstName', 'LastLead', 'test@force.com', 'Doctor', '0123456789', '0123456789', 'Email', '320 pitt st', 'sydney', 'nsw', '2009', null);

                
                ICTOpportunityController.getOpportunityRecord(strOppIdA);
                ICTOpportunityController.phaseOptions();
                ICTOpportunityController.oppViewOptions();
                ICTOpportunityController.showOpportunityAttachments(strOppIdC);
                ICTOpportunityController.getSalutationPicklistValues();
                ICTOpportunityController.getPreferredContactPicklistValues();
                ICTOpportunityController.getCompanySizePicklistValues();
                ICTOpportunityController.getLeadRecord(leadRec.Id);
                ICTUtilityHelper.logException('Error','ICT Partner Program Community','Public Queue Membership','','Public Queue Membership','createICTPartnerEntities Error','', 'test', 'test',0);
            }
            
            Test.stoptest();
        }
    }
}