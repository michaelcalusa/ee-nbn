public class ICTPdfViewerController {
  
    @AuraEnabled
    public static String getLatestContentVersion() {
        ContentWorkspace pdfWorkspace;
        ContentVersion pdfLatestVersion;
        
        for(ContentWorkspace conwosp : [SELECT Id, Name FROM ContentWorkspace WHERE Name = 'Pre-Sales Support PDF']) {
           pdfWorkspace = conwosp;
        }
        system.debug('==>'+pdfWorkspace);
        if(pdfWorkspace != null) {
            
            for(ContentVersion conVersion : [SELECT Id, ContentDocumentId, Title FROM ContentVersion 
                                             WHERE ContentDocument.ParentId = :pdfWorkspace.Id  
                                             AND isLatest = true order by ContentDocument.CreatedDate ASC limit 1]) {
                pdfLatestVersion = conVersion;
            }
            
            if(pdfLatestVersion != null) {
                return pdfLatestVersion.Id + '-' + pdfLatestVersion.ContentDocumentId + '-' + pdfLatestVersion.Title;
            }else{
                return null;
            }
            
        }
        
        return null;
        
    }
}