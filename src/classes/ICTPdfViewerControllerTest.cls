@isTest
public class ICTPdfViewerControllerTest {
  
    @TestSetup
    static void setup() {
        
        ContentWorkspace myWorkspace = new ContentWorkspace();
        for(ContentWorkspace Conworkspace : [SELECT Id FROM ContentWorkspace WHERE Name = 'Pre-Sales Support PDF']) {
            myWorkspace = Conworkspace;
        }
        if(myWorkspace == null) {
            myWorkspace = new ContentWorkspace();
            myWorkspace.Name = 'Pre-Sales Support PDF';
          insert myWorkspace;
        }
        
        
        ContentVersion newConentVersion = new ContentVersion(
          Title = 'Test Document',
          FirstPublishLocationId = myWorkspace.Id,
          PathOnClient = 'test.jpg',
          VersionData = Blob.valueOf('Test Content'),
          IsMajorVersion = true
        );
        insert newConentVersion;
        
        
    }
    
    static testMethod void TestMethod_ICTPdfViewerController() {
        Test.startTest();
        String returnedId = ICTPdfViewerController.getLatestContentVersion();
        system.assert(true, returnedId != null);
        Test.stopTest();
    }
    
    
}