/********************************************************
* Class Name    : ICTPreferencesController
* Description   : Controller class for ICT_Preferences Lightning component
* Date          : 19 February, 2019
* Change History :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Mohith Ramachandraiah    19 February, 2019           Created
* ----------------------------------------------------------------------------
*/
 
public class ICTPreferencesController{ 
    /*------------------------------------------------------------------------
        Description         :   This method will populate Individual records
        Input Parameters    :   IndividualModelList Object
        Output Parameters   :   Individual List
    --------------------------------------------------------------------------*/ 
    @AuraEnabled
    public static void updateICTPrefIndividualByModel(ICTAdditionalInformationController.PartnerIndividualModel indModel){          
        list<Individual> indList = new list<Individual>();
        system.debug('---indModel---' + indModel);
        if(!string.IsBlank(indModel.pID)){
            indList = [SELECT Id, telesales__c, FirstName,LastName,Email__c,TriggerSwitch__c,nbn_updates__c,NBN_Products__c,
                       ICT_nbn_Business_News__c,ICT_nbn_Business_Products_Services__c,ICT_Channel_Program_Updates__c,
                       		PID__c,Phone__c, HasOptedOutOfEmail__c
                            FROM Individual 
                            WHERE Pid__c =: indModel.pID Limit 1];     
            system.debug('---indList-initial--' + indList);
            if(indList.size() > 0){
                if(indList[0].TriggerSwitch__c)
                    indList[0].TriggerSwitch__c = false;
                else
                    indList[0].TriggerSwitch__c = true;
                    
                indList[0].ICT_nbn_Business_Products_Services__c = indModel.productInfo;
                indList[0].ICT_nbn_Business_News__c = indModel.newsUpdates;  
                indList[0].ICT_Channel_Program_Updates__c = indModel.ictPrograms;
                //indList[0].Legal_And_Privacy_Updates_Email__c = indModel.LeaglAndPrivacy;
                                
            }
            
            update indList;            
            ManualContactAndLeadICTPrefByIndividual(indList);
        }                
        //return indList;          
    }
    
    /*------------------------------------------------------------------------
    Description         :   This method will populate Individual records
    Input Parameters    :   IndividualModel Object
    Output Parameters   :   Individual
    --------------------------------------------------------------------------*/    
    public static list<Individual> ManualContactAndLeadICTPrefByIndividual(list<Individual> indList){  
        //Whenever ict-preferences on Individual is updated manually, it needs to be cascaded to the associated lead and contact
        try{
            list<Contact> updatedContacts = IndividualUtilities_Bulk.syncContactPref(indList);
            list<Lead> updatedLeads = IndividualUtilities_Bulk.syncLeadPref(indList);            
        }
        catch(Exception ex){
            System.debug('ICTPreferencesController.ManualContactAndLeadICTPrefByIndividual - Exception....'+ex);
            System.debug('ICTPreferencesController.ManualContactAndLeadICTPrefByIndividual - getMessage :'+ex.getMessage());
            System.debug('ICTPreferencesController.ManualContactAndLeadICTPrefByIndividual - getStackTraceString :'+ex.getStackTraceString());
            System.debug('ICTPreferencesController.ManualContactAndLeadICTPrefByIndividual - getLineNumber :'+ex.getLineNumber());
        }
        return indList;
    }
    
}