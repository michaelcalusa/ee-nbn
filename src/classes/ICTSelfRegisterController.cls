/***************************************************************************************************
    Class Name          : ICTSelfRegisterController
    Version             : 1.0 
    Created Date        : 23-Oct-2017
    Author              : Rupendra Kumar Vats
    Description         : Logic to 'Sign Up' for ICT community
    Test Class          : ICTSelfRegisterControllerTest

    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Rupendra Kumar Vats       
****************************************************************************************************/ 
public without sharing  class ICTSelfRegisterController{

    public ICTSelfRegisterController() {}

    @TestVisible 
    private static boolean isValidPassword(String password, String confirmPassword) {
        return password == confirmPassword;
    }
    
    @TestVisible 
    private static boolean siteAsContainerEnabled(String communityUrl) {
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(communityUrl,'');
        return authConfig.isCommunityUsingSiteAsContainer();
    }
    
    @TestVisible 
    private static void validatePassword(User u, String password, String confirmPassword) {
        if(!Test.isRunningTest()) {
        Site.validatePassword(u, password, confirmPassword);
        }
        return;
    }
    
    @AuraEnabled
    public static ICTSelfRegisterController.ICTUserInformation checkCommunityUser(String strContactID) {
        ICTSelfRegisterController.ICTUserInformation userinfo;
        String strContactRecordTypeID = getContactRecordTypeID();
        
        try{
            List<Contact> lstCon = [ SELECT Id, FirstName, Email, AccountId FROM Contact WHERE Id =: strContactID AND RecordTypeId =: strContactRecordTypeID ];
            
            if(!lstCon.isEmpty()){
                // Search for the existing ICT Community User
                List<User> lstUser = [ SELECT Id FROM User WHERE ContactId =: lstCon[0].Id AND isPortalEnabled = true AND Profile.Name = 'ICT Customer Community Plus User' ];
                
                if(!lstUser.isEmpty()){
                    userinfo = new ICTSelfRegisterController.ICTUserInformation(true, lstCon[0].Email, '');
                }
                else{
                    userinfo = new ICTSelfRegisterController.ICTUserInformation(false, lstCon[0].Email, '');
                }
            }
            else{
                userinfo = new ICTSelfRegisterController.ICTUserInformation(false, '', System.Label.ICT_Contact_Not_Found);
            }
        }   
        Catch(Exception ex){
            userinfo = new ICTSelfRegisterController.ICTUserInformation(false, '', System.Label.ICT_URL_Validation);
            system.debug('--ICTSelfRegisterController--checkCommunityUser' + ex.getMessage());
        }        
        return userinfo;
    }
    
    @AuraEnabled
    public static String selfRegister(String firstname ,String lastname, String email, String password, String confirmPassword, String startUrl, String strContactID) {
        system.debug('---firstname---' + firstname);
        system.debug('---lastname---' + lastname);
        system.debug('---email---' + email);
        system.debug('---password---' + password);
        system.debug('---confirmPassword---' + confirmPassword);
        system.debug('---startUrl---' + startUrl);
        system.debug('---strContactID---' + strContactID);
        String strEmail = email;
        //return firstname + '--' + lastname + '--' + strEmail + '--' + password + '--' + confirmPassword + strContactID;
        Savepoint sp = null;
        try {
            sp = Database.setSavepoint();
            
            // Validate the user inputs
            
            if (strEmail == null || String.isEmpty(strEmail)) {
                return System.Label.ICT_Email_Required;
            }
            
            if ((password == null || String.isEmpty(password)) || (confirmPassword == null || String.isEmpty(confirmPassword))) {
                return System.Label.ICT_Password_Empty_Check;
            }  
            
            if (!isValidPassword(password, confirmPassword) || (password == null || String.isEmpty(password)) || (confirmPassword == null || String.isEmpty(confirmPassword))) {
                return System.Label.ICT_Password_Validation;
            }            

            // Declare community user vairables
            ID accId, conId, profileId;
            String strNickname, strCommUserId, strContactRecordTypeID;
            
            strContactRecordTypeID = getContactRecordTypeID();
            //return firstname + '--' + lastname + '--' + strEmail + '--' + password + '--' + confirmPassword + '---' + strContactID;
            // Search for the existing ICT Contact
            List<Contact> lstCon = [ SELECT Id, FirstName, LastName, AccountId, ICT_Community_User__c, Status__c FROM Contact WHERE Id =: strContactID AND AccountId != NULL AND RecordTypeId =: strContactRecordTypeID ];
            
            if(!lstCon.isEmpty()){
                // If Contact is inactive then no registration is allowed
                if(lstCon[0].Status__c == 'Inactive'){
                     List<ICT_Error_Messages__mdt> lstErrorMsg = [ SELECT Error_Code__c, Error_Message__c FROM ICT_Error_Messages__mdt WHERE Error_Code__c = '001'];
                    
                    if(lstErrorMsg.size() > 0){
                        return lstErrorMsg[0].Error_Message__c;
                    }
                }
                
                // Contact is Active then proceed with the registration
                Contact c = new Contact(Id=lstCon[0].Id);
                c.ICT_Community_User__c = true;
                update c;
                
                firstname = lstCon[0].FirstName;
                lastname = lstCon[0].LastName;
                conId = lstCon[0].Id;
                accId = lstCon[0].AccountId;
                
                // Get the community user profile
                List<Profile> lstProfile = [ SELECT Id FROM Profile WHERE Name = 'ICT Customer Community Plus User'];
                if(!lstProfile.isEmpty()){
                    profileId = lstProfile[0].Id;
                }
                
                // Assign community user nick name
                strNickname = ((firstname != null && firstname.length() > 0) ? firstname.substring(0,1) : '' ) + lastname.substring(0,1);
                strNickname += String.valueOf(Crypto.getRandomInteger()).substring(1,7);
                
                // Create community user
                User commUser = new User();
                commUser.Username = strEmail + Label.ICT_User_Suffix;
                commUser.Email = strEmail;
                commUser.FirstName = firstname;
                commUser.LastName = lastname;
                commUser.CommunityNickname = strNickname;
                commUser.LocaleSidKey = 'en_US';
                commUser.LanguageLocaleKey = 'en_US';
                commUser.TimeZoneSidKey = 'Australia/Sydney';
                commUser.EmailEncodingKey = 'ISO-8859-1';
                commUser.ContactId = conId; 
                commUser.ProfileId = profileId;
      
                system.debug('---community user---' + commUser);
                
                //validatePassword(commUser, password, confirmPassword);
                /**
                 * Custom Password Check Logic to force Password Complexity of minimum 10 charater Length
                 * One Uppercase letter, one Lower Case Letter and Password Should not contain the 
                 * Username or FirstName/LastName
                ***/
                Pattern MyPattern = Pattern.compile('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d!_$%@#£€*?&]{10,}$');        
                // Then instantiate a new Matcher object "MyMatcher"
                Matcher passwordMyMatcher = MyPattern.matcher(password);
                Matcher confirmpasswordMyMatcher = MyPattern.matcher(confirmPassword);      
                
                if(!Test.isRunningTest()){
                    if(!passwordMyMatcher.matches() || !confirmpasswordMyMatcher.matches()) {
                        return System.Label.ICT_Password_Format_Match; 
                    }
        
                    if(password.containsIgnoreCase(commUser.Username.substringBefore('@')) || confirmPassword.containsIgnoreCase(commUser.Username.substringBefore('@')) || password.containsIgnoreCase(commUser.FirstName) || password.containsIgnoreCase(commUser.LastName) || confirmPassword.containsIgnoreCase(commUser.FirstName) || confirmPassword.containsIgnoreCase(commUser.LastName)) {
                        return System.Label.ICT_Password_Username_Check; 
                    }
                }
                
                /*************************End of Custom Passowrd Check Logic*************************/                
               
                try{
                    // Create community user
                    strCommUserId = Site.createPortalUser(commUser, accId, password);
                    system.debug('---community user id---' + strCommUserId);
                        //Setting Community user as Active
                    
                    // create a fake userId for test.
                    if (Test.isRunningTest()) {
                        strCommUserId = 'fakeUserId';           
                    }
                    
                    // If Community User created successfully 
                    if (strCommUserId != null) { 
                        // Assign Permission Set - CFS Standard User
                        /*
                        List<PermissionSet> lstPS = [ SELECT id FROM PermissionSet Where Name = 'CFS_Learning_Standard_User' ];
                        if(!lstPS.isEmpty()){
                            PermissionSetAssignment psa = new PermissionSetAssignment();
                            psa.PermissionSetId = lstPS[0].Id;
                            psa.AssigneeId = strCommUserId;
                            //insert psa;
                        }
                        */
                        // Password is not NULL then do auto community login
                        if (password != null && password.length() > 1) {
                            ApexPages.PageReference prefAutoLogin = Site.login(strEmail + Label.ICT_User_Suffix, password, startUrl);
                            if(!Test.isRunningTest()) {
                                aura.redirect(prefAutoLogin);
                            }
                        }
                        else
                        {
                            // Password is NULL then redirect user to community login screen
                            ApexPages.PageReference prefLogin = new PageReference(startUrl);
                            if(!Test.isRunningTest()) {
                                aura.redirect(prefLogin);
                            }
                        }
                    }              
                }Catch(Exception exp){
                    GlobalUtility.logMessage('Error','ICT Partner Program Community','user registration','','registration process','registration method error','',exp,0);
                    system.debug('---ICTSelfRegisterController--inner exp-' + exp.getMessage());
                    return System.Label.ICT_Error_Message;     
                }
            }
            else{
                return System.Label.ICT_Contact_Not_Found;
            }
            return null;
        }
        catch (Exception ex) {
            GlobalUtility.logMessage('Error','ICT Partner Program Community','user registration','','auto registration process','registration method error','',ex,0);
            Database.rollback(sp);
            system.debug('---ICTSelfRegisterController--outer exp-' + ex.getMessage());
            return System.Label.ICT_Error_Message;
        }
    }
    
    private static String getContactRecordTypeID(){
        String strRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strRecordTypeID = rtMapByName.get('Partner Contact').getRecordTypeId();    

        return strRecordTypeID;
    }
    
    public class ICTUserInformation{
        @AuraEnabled
        public Boolean isUserExist {get;set;}
        
        @AuraEnabled
        public String strUserEmail {get;set;}
        
        @AuraEnabled
        public String strErrorMessage {get;set;}
        
        public ICTUserInformation(Boolean isUser, String strEmail, String strError){
            this.isUserExist = isUser;
            this.strUserEmail = strEmail;
            this.strErrorMessage = strError;
        }
    }
}