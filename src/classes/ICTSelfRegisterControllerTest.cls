/***************************************************************************************************
    Class Name  :  ICTSelfRegisterControllerTest
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  Nov05,2017 
    Function    :  This class contains unit test scenarios for ICTSelfRegisterController
    Used in     :  None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            Nov05,2017                 Created
****************************************************************************************************/

@isTest(seeAllData = false)
public Class ICTSelfRegisterControllerTest{
    static testMethod void TestMethod_SelfRegistration() {
        ICTSelfRegisterController obj = new ICTSelfRegisterController();
        String strConRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRecordTypeID = rtMapByName.get('Partner Contact').getRecordTypeId();    
        
        String strAccRecordTypeID;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Partner Account').getRecordTypeId();  
        
        Account acc = new Account(Name = 'test ICT Comm', RecordTypeID = strAccRecordTypeID);
        insert acc;
        
        ICTSelfRegisterController.selfRegister('FirstName', 'LastName', 'test@force.com', '', '', '/', '');
        ICTSelfRegisterController.ICTUserInformation objAB = ICTSelfRegisterController.checkCommunityUser('');
        
        Contact con = new Contact(LastName = 'LastName', Email = 'test@force.com', RecordTypeID = strConRecordTypeID, AccountId = acc.Id);
        insert con;

        Contact conA = new Contact(LastName = 'LastName', Status__c = 'Inactive', Email = 'test1@force.com', RecordTypeID = strConRecordTypeID, AccountId = acc.Id);
        insert conA;
        
        ICTSelfRegisterController.selfRegister('FirstName', 'LastName', '', '', '', '/', con.Id);
        
        ICTSelfRegisterController.selfRegister('FirstName', '', 'FirstName@force.com', '', '', '/', con.Id);
        
        ICTSelfRegisterController.selfRegister('FirstName', 'LastName', 'FirstName@force.com', '', '', '/', con.Id);
        
        ICTSelfRegisterController.selfRegister('FirstName', 'LastName', 'FirstName@force.com', 'Abcdefg1234', 'Abcdefg1234', '/', con.Id);
        
        ICTSelfRegisterController.selfRegister('FirstName', 'LastName', 'test@force.com', 'Abcdefg1234', 'Abcdefg1234', '/', con.Id);
        ICTSelfRegisterController.selfRegister('FirstName', 'LastName', 'test@force.com', 'Abcdefg1234', 'Abcdefg123', '/', con.Id);
        
        ICTSelfRegisterController.ICTUserInformation objA = ICTSelfRegisterController.checkCommunityUser(con.Id);
        
        ICTSelfRegisterController.selfRegister('FirstName', 'LastName', 'test1@force.com', 'Abcdefg1234', 'Abcdefg1234', '/', conA.Id);
        ICTSelfRegisterController.selfRegister('FirstName', 'LastName', 'test1@force.com', 'Abcdefg1234', 'Abcdefg123', '/', conA.Id);
        
        Boolean boolCommURL = ICTSelfRegisterController.siteAsContainerEnabled('/');
        ICTSelfRegisterController.validatePassword(new User(),'test','test');
    }
}