/***************************************************************************************************
    Class Name  :  ICTUserMigrationController_Test
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  April 08,2019 
    Function    :  This class contains unit test scenarios for ICTUserMigrationController
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats             April 08,2019              Created
****************************************************************************************************/
@isTest(seeAllData = false)
public class ICTUserMigrationController_Test {
    static Contact conA, conB, conC;
    static User ictUserA, ictUserB, commUser, commUserB;
    
    /*
        create test data for the user migration scenarios
    */
    public static void createTestRecords() {
        String strConRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRecordTypeID = rtMapByName.get('Partner Contact').getRecordTypeId();    
        
        String strAccRecordTypeID;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Partner Account').getRecordTypeId();  

        Account acc = new Account(Name = 'test ICT Comm', RecordTypeID = strAccRecordTypeID);
        insert acc;
        
        List<Contact> lstCon = new List<Contact>();
        conA = new Contact(LastName = 'LastNameA', Email = 'testA@force.com', RecordTypeID = strConRecordTypeID, AccountId = acc.Id);
        conB = new Contact(LastName = 'LastNameB', Email = 'testB@force.com', RecordTypeID = strConRecordTypeID, AccountId = acc.Id);
        conC = new Contact(LastName = 'LastNameC', Email = 'testc@force.com', RecordTypeID = strConRecordTypeID, AccountId = acc.Id);
        lstCon.add(conA);
        lstCon.add(conB);
        lstCon.add(conC);
        insert lstCon;
        
        List<Profile> lstProfile = [ SELECT Id, Name FROM Profile WHERE Name = 'BSM ICT' OR Name = 'ICT Customer Community Plus User' OR Name = 'ICT Partner Community User' Order By Name ASC];
        
        List<User> lstUser = new List<User>();
        ictUserA = new User();
        ictUserA.Username = 'testbsmuserA@nbnco.com.au.test';
        ictUserA.Email = 'testbsmuserA@nbnco.com.au';
        ictUserA.FirstName = 'testbsmA';
        ictUserA.LastName = 'userA';
        ictUserA.ProfileId = lstProfile[0].Id;
        ictUserA.Alias = 'tbsmA'; 
        ictUserA.TimeZoneSidKey = 'Australia/Sydney'; 
        ictUserA.LocaleSidKey = 'en_US'; 
        ictUserA.EmailEncodingKey = 'UTF-8'; 
        ictUserA.LanguageLocaleKey = 'en_US'; 
        
        ictUserB = new User();
        ictUserB.Username = 'testbsmuserB@nbnco.com.au.test';
        ictUserB.Email = 'testbsmuserB@nbnco.com.au';
        ictUserB.FirstName = 'testbsmB';
        ictUserB.LastName = 'userB';
        ictUserB.ProfileId = lstProfile[0].Id;
        ictUserB.Alias = 'tbsmB'; 
        ictUserB.TimeZoneSidKey = 'Australia/Sydney'; 
        ictUserB.LocaleSidKey = 'en_US'; 
        ictUserB.EmailEncodingKey = 'UTF-8'; 
        ictUserB.LanguageLocaleKey = 'en_US'; 

        commUserB = new User();
        commUserB.Username = 'test123@forceB.com.ict';
        commUserB.Email = 'test123@forceB.com';
        commUserB.FirstName = 'firstname12B';
        commUserB.LastName = 'lastname123B';
        commUserB.CommunityNickname = '123ictcommB8848';
        commUserB.ContactId = conC.ID; 
        commUserB.ProfileId = lstProfile[2].Id;
        commUserB.Alias = 'tict1B'; 
        commUserB.TimeZoneSidKey = 'Australia/Sydney'; 
        commUserB.LocaleSidKey = 'en_US'; 
        commUserB.EmailEncodingKey = 'UTF-8'; 
        commUserB.LanguageLocaleKey = 'en_US';
        commUserB.Previous_Contact_Id__c = conC.ID;
        commUserB.User_Migration_Payload__c = '{"userName":"test123@force.com.ict","status":"UserDisabled","oldUserId":null,"nextStep":"CreateUser","newUserId":null,"contactId":"' + conC.ID + '","cfsId":"u9obym49OAghDlN4yyoxTJes8gpWsQvzAaK8hFKSyLGPkE8Ls5Cqxcs2rCR1m2tng3cJGwmjNqqAwk5DS9H+iw=="}';
        
        lstUser.add(ictUserA);
        lstUser.add(ictUserB);
        lstUser.add(commUserB);
        insert lstUser;
        
        commUser = new User();
        commUser.Username = 'test123@force.com.ict';
        commUser.Email = 'test123@force.com';
        commUser.FirstName = 'firstname12';
        commUser.LastName = 'lastname123';
        commUser.CommunityNickname = '123ictcomm8848';
        commUser.ContactId = conB.ID; 
        commUser.ProfileId = lstProfile[1].Id;
        commUser.Alias = 'tict1'; 
        commUser.TimeZoneSidKey = 'Australia/Sydney'; 
        commUser.LocaleSidKey = 'en_US'; 
        commUser.EmailEncodingKey = 'UTF-8'; 
        commUser.LanguageLocaleKey = 'en_US';
        commUser.Previous_Contact_Id__c = conB.ID;
        commUser.User_Migration_Payload__c = '{"userName":"test123@force.com.ict","status":"UserDisabled","oldUserId":null,"nextStep":"CreateUser","newUserId":"' + commUserB.ID + '","contactId":"' + conB.ID + '","cfsId":"u9obym49OAghDlN4yyoxTJes8gpWsQvzAaK8hFKSyLGPkE8Ls5Cqxcs2rCR1m2tng3cJGwmjNqqAwk5DS9H+iw=="}';
        insert commUser;
        
        // Create CFS training data
        lmscons__Training_Path__c course = new lmscons__Training_Path__c(name='test');
        insert course;
        
        Learning_Record__c lrec = new Learning_Record__c(Contact__c=conB.id,Course_Name__c='test', Course_Status__c='Certified',course__c=course.id);
        insert lrec;
        
        lmscons__Transcript__c transcript = new lmscons__Transcript__c(lmscons__Trainee__c=commUser.id);
        insert transcript;
        
        
        lmscons__Training_Content__c testModule = new lmscons__Training_Content__c(lmscons__Title__c = 'Test Module');
        insert testModule;
        
        lmscons__Training_Content_License__c tCL = new lmscons__Training_Content_License__c(lmscons__Training_Content__c = testModule.Id);
        insert tCL;
        
        lmscons__Training_User_License__c tUL = new lmscons__Training_User_License__c(lmscons__User__c = commUser.id, lmscons__Content_License__c = tCL.Id);
        insert tUL;
        
        lmscons__Training_Path_Assignment_Progress__c courseAssignment = new lmscons__Training_Path_Assignment_Progress__c(Learning_Record__c=lrec.id,lmscons__Training_Path__c=course.id, lmscons__Transcript__c=transcript.id, lmscons__Completion_Date__c=system.now());
        insert courseAssignment;
        
        lrec.Course_Status__c='Certified';
        update lrec;
    }

    /*
        Test Scenario - When ICT user does not have permission to execute process
    */    
    static testMethod void TestMethod_UserWithNoPermission() {
        createTestRecords();        
        System.runAs(ictUserA){
            ICTUserMigrationController.MigrateUserModel result = ICTUserMigrationController.getMigrateUserModel(conA.ID); 
            system.debug('---UserWithNoPermission---result----' + result);
            System.assertEquals(false, result.hasPermission, 'user does not have permission');
        }
    }

    /*
        Test Scenario - When Customer Community Plus user is not found
    */  
    static testMethod void TestMethod_UserNotFound() {
        createTestRecords(); 

        System.runAs(ictUserB){
            Test.startTest();
            List<PermissionSet> lstPS = [ SELECT Id FROM PermissionSet WHERE Name = 'Migrate_CCP_to_PC'];
            PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = lstPS[0].Id, AssigneeId = ictUserB.Id);
            insert psa;
            Test.stopTest();
            ICTUserMigrationController.MigrateUserModel result = ICTUserMigrationController.getMigrateUserModel(conA.ID); 
            system.debug('---UserNotFound---result----' + result);
            System.assertEquals(false, result.isUserFound, 'user is not found');
        }
    }    

    /*
        Test Scenario - Single execution of user migration to create Partner Community user
    */  
    static testMethod void TestMethod_UserMigration() {
        createTestRecords(); 

        System.runAs(ictUserB){

            List<PermissionSet> lstPS = [ SELECT Id FROM PermissionSet WHERE Name = 'Migrate_CCP_to_PC'];
            PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = lstPS[0].Id, AssigneeId = ictUserB.Id);
            insert psa;
        }

        System.runAs(ictUserB){ 
        
            ICTUserMigrationController.MigrateUserModel result = ICTUserMigrationController.getMigrateUserModel(conB.ID); 
            system.debug('---UserNotFound---result----' + result);
            System.assertEquals(true, result.isUserFound, 'user found');
            
            ICTUserMigrationController.ResponseModel resultA = ICTUserMigrationController.disableUserByContactId(conB.ID); 
            system.debug('---DisabledCCPUser---result----' + resultA);
            System.assertEquals(true, resultA.success, 'Successfully disabled the given user');
            
            ICTUserMigrationController.ResponseModel resultB = ICTUserMigrationController.createUserByContactId(conB.ID); 
            system.debug('---CreatePartnerUser---result----' + resultB);
        
            ICTUserMigrationController.ResponseModel resultC = ICTUserMigrationController.migrateTranscriptsWithOldUserId(commUser.ID); 
            system.debug('---MoveTrainingHistory---result----' + resultC);
        }
    } 

    /*
        Test Scenario - Community user is already Partner Community user
    */     
    static testMethod void TestMethod_UserAlreadyMigrated() {
        createTestRecords(); 

        System.runAs(ictUserB){
            Test.startTest();
            List<PermissionSet> lstPS = [ SELECT Id FROM PermissionSet WHERE Name = 'Migrate_CCP_to_PC'];
            PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = lstPS[0].Id, AssigneeId = ictUserB.Id);
            insert psa;
            Test.stopTest();
            ICTUserMigrationController.MigrateUserModel result = ICTUserMigrationController.getMigrateUserModel(conC.ID); 
            system.debug('---UserAlreadyMigrated---result----' + result);
            System.assertEquals(true, result.isActivePCUserExist, 'Partner user already exist');
        }
    }   

    /*
        Test Scenario - Resume process after Customer Community Plus user is disabled and Partner user is not created
    */     
    static testMethod void TestMethod_CreatePartnerUser() {
        createTestRecords(); 
        System.runAs(ictUserB){
            Test.startTest();
            List<PermissionSet> lstPS = [ SELECT Id FROM PermissionSet WHERE Name = 'Migrate_CCP_to_PC'];
            PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = lstPS[0].Id, AssigneeId = ictUserB.Id);
            insert psa;
            Test.stopTest();
            
            ICTUserMigrationController.ResponseModel resultB = ICTUserMigrationController.createUserByContactId(conB.ID); 
            system.debug('---CreatePartnerUser---result----' + resultB);
        }
    }

    /*
        Test Scenario - Resume process after Partner Community user is created and CFS data was not migrated
    */ 
    static testMethod void TestMethod_MoveTrainingHistory() {
        createTestRecords(); 
        System.runAs(ictUserB){
            Test.startTest();
            List<PermissionSet> lstPS = [ SELECT Id FROM PermissionSet WHERE Name = 'Migrate_CCP_to_PC'];
            PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = lstPS[0].Id, AssigneeId = ictUserB.Id);
            insert psa;
            Test.stopTest();
            
            ICTUserMigrationController.ResponseModel result = ICTUserMigrationController.migrateTranscriptsWithOldUserId(commUser.ID); 
            system.debug('---MoveTrainingHistory---result----' + result);
            System.assertEquals(true, result.success, 'Successfully migrated the old user related records');
        }
    }
    
    /*
        Test Scenario - When Contact information is not sent
    */     
    static testMethod void TestMethod_MirationWithoutContactInfo() {
        createTestRecords(); 
        System.runAs(ictUserB){
            ICTUserMigrationController.MigrateUserModel result = ICTUserMigrationController.getMigrateUserModel(null); 
            
            ICTUserMigrationController.ResponseModel resultA = ICTUserMigrationController.disableUserByContactId(null); 
            
            ICTUserMigrationController.ResponseModel resultB = ICTUserMigrationController.createUserByContactId(null); 
            
            ICTUserMigrationController.migrateTranscriptsWithOldUserId(null); 
            ICTUserMigrationController.migrateTranscriptsToNewuser(null,null);
            ICTUserMigrationController.migrateTranscriptsToNewuser(commUserB.ID,commUserB.ID);
            ICTUserMigrationController.migrateTrainingUserLicenseToNewuser(null,null);
            ICTUserMigrationController.migrateTrainingUserLicenseToNewuser(commUserB.ID,commUserB.ID);
            
            ICTUserMigrationController.PortalUser puser = new ICTUserMigrationController.PortalUser(); 
            puser.FirstName = commUserB.FirstName;
            puser.Username = commUserB.Username;
            puser.TimeZoneSidKey = commUserB.TimeZoneSidKey;
            puser.ProfileId = commUserB.ProfileId;
            puser.LocaleSidKey = commUserB.LocaleSidKey;
            puser.LastName = commUserB.LastName;
            puser.LanguageLocaleKey = commUserB.LanguageLocaleKey;
            puser.EmailEncodingKey = commUserB.EmailEncodingKey;
            puser.Email = commUserB.Email;
            puser.ContactId = commUserB.ContactId;
            puser.Alias = commUserB.Alias;
            puser.IsActive = true;
            puser.IsPortalEnabled = true;
            puser.AccountId = conB.AccountId;
        }
    }
}