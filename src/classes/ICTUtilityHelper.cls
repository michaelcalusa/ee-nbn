/********************************************************
* Class Name    : ICTUtilityHelper
* Description   : Utility class for ICT Lightning components
* Created By    : Rupendra Kumar Vats
* Date          : 17th Sep, 2018
* Change History :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Rupendra Vats           September 17, 2018                 Created
* Rupendra Vats           December 19, 2018                  Updated to include methods - getContact and processLeadConvert
* ----------------------------------------------------------------------------
*/
 
public Without Sharing class ICTUtilityHelper{
    /*
        Method  : getAccount
        Return  : Account Record Id
        Params  : Account params
    */
    public static String getAccount(String strRecordTypeId, String strABNNumber, String strEntityName){
        String strAccountID = '';
        List<Account> lstAccount = [ SELECT Id FROM Account WHERE RecordTypeId =: strRecordTypeId AND ABN__c =: strABNNumber AND Name =: strEntityName Order by LastModifiedDate DESC ];
        if(!lstAccount.isEmpty()){
            strAccountID = lstAccount[0].Id;
        }
        return strAccountID;
    }

    /*
        Method  : getContact
        Return  : contact Record Id
        Params  : Contact params
    */
    public static String getContact(String strRecordTypeId, String strAccountId, String strLastName, String strEmail){
        String strContactID = '';
        List<Contact> lstContact = [ SELECT Id, Salutation, FirstName, LastName, RecordTypeId, Email, AccountId, Job_Title__c, Phone, MobilePhone, Preferred_Contact_Method__c, MailingStreet, MailingCity, MailingState, MailingPostalCode FROM Contact WHERE RecordTypeId =: strRecordTypeId AND AccountId =: strAccountId AND LastName =: strLastName AND Email =: strEmail Order by LastModifiedDate DESC ];
        if(!lstContact.isEmpty()){
            strContactID = lstContact[0].Id;
        }
        return strContactID;
    }

    /*
        Method  : createOpportunity
        Return  : Opportunity Record Id
        Params  : Opportunity fileds for creation and update
    */
    public static String createOpportunity(Opportunity opp, Boolean isInsert){
        String strOpportunityID = '';
        if(isInsert){
            insert opp;
        }
        else{
            update opp;
        }
        strOpportunityID = opp.Id;
        return strOpportunityID;
    }

    /*
        Method  : createOpportunity
        Return  : Opportunity Record Id
        Params  : Opportunity fileds for creation and update
    */
    public static String processContact(Contact con, Boolean isInsert){
        String strContactID = '';
        if(isInsert){
            insert con;
        }
        else{
            update con;
        }
        strContactID = con.Id;
        return strContactID;
    }
    
    /*
        Method  : processLeadConvert
        Return  : Opportunity Record Id
        Params  : Record Ids for Account, Contact, Opportunity and Lead
    */
    public static String processLeadConvert(String strAccountId, String strContactId, String strOpportunityId, String strLeadId){
        String strResult = '';
        Database.LeadConvert Leadconvert = new Database.LeadConvert();
        Leadconvert.setLeadId(strLeadId);
        Leadconvert.setAccountId(strAccountId);
        Leadconvert.setContactId(strContactId);
        Leadconvert.setOpportunityId(strOpportunityId);
        Leadconvert.setConvertedStatus('Qualified');
        Database.LeadConvertResult LeadconvertResult = Database.convertLead(Leadconvert);
        system.debug('--LeadconvertResult--' + LeadconvertResult);
        
        List<OpportunityContactRole> lstOCRoles = [ select Id,IsPrimary,Role from OpportunityContactRole where OpportunityId =: strOpportunityId ];
        if(!lstOCRoles.isEmpty()){
            OpportunityContactRole ocRole = new OpportunityContactRole(Id = lstOCRoles[0].Id);
            ocRole.IsPrimary = true;
            ocRole.Role = 'Óther';
            update ocRole;          
        }

        return strResult;
    }
    
    @future(callout=false)
    public static void processOpportunityRecord(String strEndCustAccId, String strPartnerAccId, String strOpportunityId, Boolean isAccountInsert){
        try{
            // Link 'End Customer Account' with Partner Account
            if(isAccountInsert){
                List<String> lstEmail = System.Label.ICTPortalAccountOwner.split(',');
                List<User> lstUser = [ SELECT ID FROM User WHERE Email In: lstEmail Order by FirstName DESC ];
                Account acc = new Account(Id = strEndCustAccId);
                acc.OwnerId = lstUser[0].Id;
                update acc;
            }

            // Link End Customer Opportunity with Partner Account
            List<OpportunityPartner> lstOppPartner = [ SELECT Id FROM OpportunityPartner WHERE OpportunityId =: strOpportunityId AND AccountToId =: strPartnerAccId ];
            if(lstOppPartner.isEmpty()){
                Partner p = new Partner();
                p.AccountToId = strPartnerAccId;
                p.OpportunityId = strOpportunityID;
                p.Role = 'Channel Partner';
                p.IsPrimary = true;
                insert p;           
                
                system.debug('---partner record--' + p);
            }           
        }Catch(Exception ex){
            GlobalUtility.logMessage('Error','ICTUtilityHelper','Opportunity Update and Partner linking','','Opportunity linking','processOpportunityRecord method error','',ex,0);
        }
    }
    
     /*
        Method  : logException
        Return  : none
        Params  : exception details input
    */
    
    @future(callout=false)
    public static void logException(String logLevel, String sourceClass, String sourceFunction, String referenceId, String referenceInfo, String logMessage, String payLoad, String msg, String tracemsg, long timeTaken){
        Application_Log__c log = new Application_Log__c();
            
        log.Source__c = sourceClass;
        log.Source_Function__c = sourceFunction;
        log.Reference_Id__c = referenceId;
        log.Reference_Information__c = referenceInfo;
        log.Message__c = logMessage;
        log.Integration_Payload__c = payload;
        log.Stack_Trace__c = tracemsg;
        log.Message__c = msg;
        log.RecordTypeId = GlobalCache.getRecordTypeId ( 'Application_Log__c', logLevel );       
        
        insert log;
    }
}