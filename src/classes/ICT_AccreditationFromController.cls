/********************************************************
* Class Name    : ICT_AccreditationFromController
* Description   : Controller class for ICT_AccreditationForm Lightning component
* Created By    : Arvind Thakur (ICT Team NBN)
* Date          : 15th May, 2018
* Change History :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Arvind Thakur           May 15, 2018                 Created
 * *******************************************************/

public without sharing class ICT_AccreditationFromController {
    
     /*
    Method  : getAccreditationRecord
    Return  : A wrapper object contatining info on contact.
                Method is invoked on INIT
    Params  : None
    */
    @AuraEnabled
    public static string getAccreditationRecord() {
        
        User loggedinUser;
        for(User u : [SELECT ContactId, AccountId, Account.Engagement_Status__c, Contact.FirstName, Contact.LastName, Contact.Email, Contact.Phone, Contact.Job_Title__c 
                      FROM User 
                      WHERE Id =: UserInfo.getUserId()]) {
            loggedInUser = u;
        }
        
        ContactAccreditationWrapper newWrapper = new ContactAccreditationWrapper();
        
        if(loggedinUser != null && loggedinUser.ContactId != null) {
            
            newWrapper.contactId = loggedInUser.ContactId;
            newWrapper.accountId = loggedInUser.AccountId;
            newWrapper.accountAccreditationStatus = loggedInUser.Account.Engagement_Status__c;
            newWrapper.loggedInUserDetails = loggedInUser;
            
            for(Accreditation_Request__c existingRequest : [SELECT Id 
                                                            FROM Accreditation_Request__c 
                                                            WHERE Contact__c =: newWrapper.contactId AND Status__c='']) {
                                                                newWrapper.accreditationRequestId = existingRequest.Id;
                                                            }
            
        }
        
        return JSON.serialize(newWrapper);
    }
    
    
    
    
    /*
    Method  : getAccountStatus
    Return  : Return the account's accreditation status
    Description : We want to detect if account is changed at the backend on submit on form
    Params  : Account Id
    */
    @AuraEnabled
    public static String getAccountStatus(String accountId) {
        for(Account userAccount : [SELECT Id, Engagement_Status__c FROM Account WHERE Id=: accountId] ) {
            return userAccount.Engagement_Status__c;
        }
        return null;
    }
    
    
    
    
    
    /*
    Method  : getContactRolePicklistValues
    Return  : List of string containing picklist values
                of Contact Role on Accreditation Object
    Params  : None
    */    
    @AuraEnabled
    public static List<String> getContactRolePicklistValues() {
        
        List<String> contactRoleOptions = new List <String> ();
        Schema.sObjectType objType = Accreditation_Request__c.getSObjectType();
        
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();        
        Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
        
        List<Schema.PicklistEntry> values = fieldMap.get('Business_Principal_Contact_Role__c').getDescribe().getPickListValues();
        for (Schema.PicklistEntry a: values) {
            contactRoleOptions.add(a.getValue());
        }
        
        contactRoleOptions.sort();
        return contactRoleOptions;
    }
    
    
    
    
    
    /*
    For ICT Channel Partner Record Type - 
    -Origin is defaulted to ICT Partner Portal
    -Phase is defaulted to ICT Partner Accreditation
    -Status is defaulted to OPEN via the support process
    */
    @AuraEnabled
    public static String createNewAccreditationCase(String accountId, String contactId) {
        
        Id queueId;
        Case newAccreditationCase = new Case();
        
        for(Group caseQueueObject : [SELECT Id , Name
                                     FROM Group 
                                     WHERE Name = 'ICT Channel Team' 
                                     AND Type = 'Queue']) {
                                         queueId = caseQueueObject.Id;                                  
                                     }
        
        if(queueId != null) {
            
            newAccreditationCase.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('ICT Channel Partner').getRecordTypeId();
            newAccreditationCase.OwnerId = queueId;
            newAccreditationCase.AccountId = accountId;
            newAccreditationCase.ContactId = contactId;
            
            
            try{
                
                Database.DMLOptions dmo = new Database.DMLOptions();
                dmo.assignmentRuleHeader.useDefaultRule= true;
                dmo.EmailHeader.TriggerUserEmail = true;
                newAccreditationCase.setOptions(dmo);

                insert newAccreditationCase;
                
                List<Accreditation_Request__c> lstReq = [ SELECT Id FROM Accreditation_Request__c WHERE Contact__c =: newAccreditationCase.ContactId LIMIT 1];
                if(!lstReq.isEmpty()){
                    lstReq[0].Case__c = newAccreditationCase.Id;
                    update lstReq;
                }

            }catch(Exception ex) {
                GlobalUtility.logMessage('Error','ICT Partner Program Community','Accreditation Information','','Accreditation Information','Accreditation case creation method error','',ex,0);
                return 'Error';
                system.debug('Error on Accreditation Submission >> ' + accountId + ex.getMessage());
            }
        }               
        
        return 'Error';
    }
    
    
    
    
    /*
    Wrapper Class for sending out info to Lightning component
    */
 
    public class ContactAccreditationWrapper {
        public String contactId {get;set;}
        public String accountId {get;set;}
        public String accountAccreditationStatus {get;set;}
        public String accreditationRequestId {get;set;}
        public User loggedInUserDetails {get;set;}
    }
    
}