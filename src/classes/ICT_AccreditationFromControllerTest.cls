/***************************************************************************************************
Class Name  :  ICTAdditionalInformationControllerTest
Class Type  :  Test Class 
Created Date:  May 15, 2018 
Function    :  This class contains unit test scenarios for ICT_AccreditationFromController
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Arvind Thakur           May 15, 2018                 Created
****************************************************************************************************/
@isTest(seeAllData = false)
public class ICT_AccreditationFromControllerTest {
    
    static testMethod void Test_AccreditationFromController() {
        test.startTest();
        String strConRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRecordTypeID = rtMapByName.get('Partner Contact').getRecordTypeId();    
        
        String strAccRecordTypeID;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Partner Account').getRecordTypeId();  
        
        Account acc = new Account(Name = 'test ICT Comm', RecordTypeID = strAccRecordTypeID);
        insert acc;
        
        Contact con = new Contact(LastName = 'LastName123', Email = 'test123@force.com', RecordTypeID = strConRecordTypeID, AccountId = acc.Id);
        insert con;
        
        List<Profile> lstProfile = [ SELECT Id FROM Profile WHERE Name = 'ICT Customer Community Plus User'];
        
        
        if(!lstProfile.isEmpty()){
            // Create community user
            User commUser = new User();
            commUser.Username = 'test123@force.com.ICT';
            commUser.Email = 'test123@force.com';
            commUser.FirstName = 'firstname12';
            commUser.LastName = 'lastname123';
            commUser.CommunityNickname = '123ictcomm8848';
            commUser.ContactId = con.ID; 
            commUser.ProfileId = lstProfile[0].Id;
            commUser.Alias = 'tict1'; 
            commUser.TimeZoneSidKey = 'Australia/Sydney'; 
            commUser.LocaleSidKey = 'en_US'; 
            commUser.EmailEncodingKey = 'UTF-8'; 
            commUser.LanguageLocaleKey = 'en_US'; 
            insert commUser;
            
            
            /*
            Group g1 = new Group(Name='ICT Channel Team', type='Queue');
                insert g1;
                QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
                insert q1;
             */
            system.runAs(commUser){
                
                ICT_AccreditationFromController.getContactRolePicklistValues();               
                String returnWrapper = ICT_AccreditationFromController.getAccreditationRecord();
                ICT_AccreditationFromController.ContactAccreditationWrapper  deserializedWrapper = (ICT_AccreditationFromController.ContactAccreditationWrapper) System.JSON.deserialize(returnWrapper, ICT_AccreditationFromController.ContactAccreditationWrapper.class);
                ICT_AccreditationFromController.getAccountStatus(deserializedWrapper.accountId);
                Accreditation_Request__c newRecord = new Accreditation_Request__c();
                newRecord.Contact__c = deserializedWrapper.contactId;
                insert newRecord;
                
                String returnValue = ICT_AccreditationFromController.createNewAccreditationCase(deserializedWrapper.contactId, deserializedWrapper.accountId);
                //system.assert([Select Id FROM Case Where contactId =:deserializedWrapper.contactId].size() > 0);
                
                
            }
            
            Test.stoptest();
        }
        
    }
}