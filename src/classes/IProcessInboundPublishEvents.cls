/*------------------------------------------------------------
    Author:        Kashyap Murthy
    Company:       Contractor
    Description:   Interface to process the InboundPublishEvents.
    Returns:       The record id for the account
    History
    <Date>      <Authors Name>     <Brief Description of Change> 
    ------------------------------------------------------------*/
public Interface IProcessInboundPublishEvents{

   void processInboundPublishEvents (List<String> inboundJSONMessageList, String integrationType);
}