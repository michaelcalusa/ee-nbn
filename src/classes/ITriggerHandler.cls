public interface ITriggerHandler 
{
    void BeforeInsert(List<SObject> newItems, Map<Id, SObject> newItemMap);
 
    void BeforeUpdate(List<SObject> newItems, List<SObject> oldItems, Map<Id, SObject> newItemMap, Map<Id, SObject> oldItemMap);
 
    void BeforeDelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap);
 
    void AfterInsert(List<SObject> newItems, Map<Id, SObject> newItemMap);
 
    void AfterUpdate(List<SObject> newItems, List<SObject> oldItems, Map<Id, SObject> newItemMap, Map<Id, SObject> oldItemMap);
 
    void AfterDelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap);
 
    void AfterUndelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap);
 
    Boolean IsDisabled();
    
}