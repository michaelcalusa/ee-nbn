global class InBoundEmailToCaseServiceHandler implements Messaging.InboundEmailHandler 
{
  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope)  
    {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();               
        String emailBody = ''; 
        String where_clause;    
        String where_clause_with_recordType;  
        System.debug('email1 = ' + email.fromAddress);
        System.debug('email2 = ' + envelope.fromAddress);  
        try {         
            List<Contact> contactList = createContact(email);
            System.debug('Email Subject = ' + email.subject);   
            String caseNumber;  
            String refNumber ;  
            String senderEmail;         
            if(String.isNotBlank(email.subject)) {                    
                String subjectStr = email.subject;
                String[] tokenizeSubject = subjectStr.split(' ');
                 System.debug('********tokenizeSubject ************'+tokenizeSubject );
               
                for(String st:tokenizeSubject) {
                    String caseFormat = '[a-zA-Z]*[0-9]{7,8}[a-zA-Z]*';                  
                    Pattern p = Pattern.compile(caseFormat);
                    Matcher m = p.matcher(st);
                    if(m.find()) {  
                        caseNumber = st;
                        if(!caseNumber.startsWith('0')) {
                            caseNumber = '0'+caseNumber;
                        }
                        where_clause = ' caseNumber ='+ '\'' + caseNumber + '\'';
                    }else{
                        if(st.contains('ref:_')) {
                            Integer refIndex = st.indexOf('ref:_');
                            Integer lastRefIndex = st.lastIndexOf(':ref');
                            refNumber = st.substring(refIndex,lastRefIndex+4);
                            where_clause = ' Thread_Id__c ='+ '\'' + refNumber+ '\'';
                        }
                    }
                     System.debug('********caseNumber ************'+caseNumber);
                }
                
                if(String.isEmpty(caseNumber)){
                    if(!String.isEmpty(email.plainTextBody)) {
                        String description = email.plainTextBody;
                        String[] splitString = description.split('\n');
                        
                       
               
                        for(String str:splitString ) {
                            String[] tokenizeSub = str.split(' ');
                            for(String st:tokenizeSub) {
                                String caseFormat = '[a-zA-Z]*[0-9]{7,8}[a-zA-Z]*';                  
                                Pattern p = Pattern.compile(caseFormat);
                                Matcher m = p.matcher(st);
                                if(m.find()) { 
                                 System.debug('********side body************'+st ); 
                                    caseNumber = st;
                                     if(!caseNumber.startsWith('0')) {
                                       caseNumber = '0'+caseNumber;
                                     }
                                    where_clause = ' caseNumber ='+ '\'' + caseNumber + '\'';
                                    break;
                                }
                            }
                        } 
                        if(String.isEmpty(caseNumber) && description.contains('ref:_')) {
                            Integer refIndex = description.indexOf('ref:_');
                            Integer lastRefIndex = description.lastIndexOf(':ref');
                            refNumber = description.substring(refIndex,lastRefIndex+4);
                            where_clause = ' Thread_Id__c ='+ '\'' + refNumber+ '\'';
                            System.debug('********refNumber************'+refNumber);
                        }
                    }
                }
                System.debug('********caseNumber************'+caseNumber);
               System.debug('********refNumber************'+refNumber);
               
                if(String.isEmpty(refNumber) && String.isEmpty(caseNumber)){
                    senderEmail = email.fromAddress;
                     System.debug('********senderEmail ************'+senderEmail);
                    where_clause = ' contact.Email ='+ '\'' + senderEmail + '\'';
                      
                }
                
            }
            
            Id recordTypeStrId; 
            String fromAdd = envelope.fromAddress;
            System.debug('fromAdd::'+fromAdd);
            String  tokenizeStr = fromAdd.split('=')[2];
            System.debug('tokenizeStr::'+tokenizeStr);
            EmailToCaseMailBox__c myCS1 = EmailToCaseMailBox__c.getValues(tokenizeStr);           

            if(myCS1.Case_RecordType__c == 'Query') {
                    recordTypeStrId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId();
            }else if(myCS1.Case_RecordType__c == 'Formal Complaint') {
                    recordTypeStrId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Formal Complaint').getRecordTypeId();
            }
            /*if(envelope.fromAddress.contains('newdevelopmentstest@nbn.com.au')) {
                    recordTypeStrId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId();
            }else if(envelope.fromAddress.contains('industrycapabilitytest@nbnco.com.au')) {
                    recordTypeStrId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Formal Complaint').getRecordTypeId();
            }*/

            where_clause_with_recordType = where_clause+' and RecordTypeId ='+ '\'' + recordTypeStrId + '\'';           
            
            String caseQuery = 'SELECT Id, caseNumber,Status, RecordTypeId FROM CASE WHERE ' + where_clause_with_recordType;
            System.debug('********caseQuery ************'+caseQuery );

            List<Case> caseList = Database.query(caseQuery); 
    
            if(!caseList.isEmpty() ) {
                 Case caseObject;
                 //case size one and status is open
                if(caseList.size() == 1 && (!String.isEmpty(refNumber) || !String.isEmpty(caseNumber)) ) {
                    
                    if((caseList.get(0).status == 'Open' || caseList.get(0).status == 'Re-Opened' || caseList.get(0).status == 'Closed')) {
                        caseObject = updateCase(email,caseList);    
                    }                
                } 
                /*else if( (!String.isEmpty(refNumber) || !String.isEmpty(caseNumber)) && caseList.size() == 1 && caseList.get(0).status == 'Closed') {
                    caseList.get(0).status = 'Re-Opened';
                    caseObject = updateCase(email,caseList);                    
                } */
                else if(String.isEmpty(refNumber) && String.isEmpty(caseNumber)) {
                    List<Case> openStatusCaseList = new List<Case>();
                    List<Case> reOpenStatusCaseList = new List<Case>();
                    for(case caseRecord:caseList){
                        if(caseRecord.status == 'Open') {
                            openStatusCaseList.add(caseRecord);
                        }
                                               
                    }
                    /*if(openStatusCaseList.size() > 1 ){
                        caseObject = createNewCase(email,contactList,recordTypeStrId,tokenizeStr); 
                           
                    }else*/
                    
                     if((openStatusCaseList.size() == 1 ) && caseList.get(0).status == 'Open') {
                        caseObject = updateCase(email,openStatusCaseList);                  
                    }else {
                        caseObject = createNewCase(email,contactList,recordTypeStrId,tokenizeStr); 
                    }
                     System.debug('Case Updated');
                }
                Task taskObj = createTask(email,contactList,caseObject);
                //This loop will process text attacchments like txt files
                createAttachments(email,taskObj);
              //sendAckEmail(email.subject, email.fromAddress);
            } else {
               Case caseObject = createNewCase(email,contactList,recordTypeStrId,tokenizeStr);
               Task taskObj = createTask(email,contactList,caseObject);
                //This loop will process text attacchments like txt files
                createAttachments(email,taskObj);                              
            }
        }               
        catch(Exception e)
        {
            System.debug('Exception' + e);   
            emailBody = emailBody + 'error::'+e.getStackTraceString();          
        }
        sendDebugEmail('Debug Email','narasimhabinkam@nbnco.com.au',emailBody);
        result.success = true;
        return result;           
    }
    
    
    public void sendDebugEmail(String subject, String email, String emailBody) {
        List<Messaging.SingleEmailMessage>  sendEmails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage sendEmail = new Messaging.SingleEmailMessage();
        sendEmail.setPlainTextBody(emailBody);
        sendEmail.setSubject('RE: '+subject);
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply@nbnco.com.au'];
        if ( owea.size() > 0 ) {
            sendEmail.setOrgWideEmailAddressId(owea.get(0).Id) ;
        }        
        sendEmail.setToAddresses(new String[] {email});
        sendEmails.add(sendEmail);
        Messaging.sendEmail(sendEmails);        
    }
    
    
    public void sendAckEmail(String subject, String email,String caseNumber,String reffernceId,String senderName) {
        System.debug('Sending Acknowledgement Email');                
        List<Messaging.SingleEmailMessage>  sendEmails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage sendEmail = new Messaging.SingleEmailMessage();
        
            
        String emailBody = 'Hello '+senderName +','+ '\n' + '\n';
        emailBody = emailBody + 'Thank you for your enquiry to NBN. The reference case for your enquiry is:'+caseNumber + '\n' + '\n';
        emailBody = emailBody + 'Your answer may already be available at our website nbnco.com.au. We recommend that you access our website in the first instance or discuss your enquiry with your retail service provider.'  + '\n';
        emailBody = emailBody + 'If it is related to a Safety concern please call us immediately on 1800 OUR NBN (1800 687 626), quoting the above reference number.  '  + '\n' + '\n';
        
        emailBody = emailBody + 'Regards'   + '\n' + '\n';
        emailBody = emailBody + 'nbnco'   + '\n' + '\n';
        
        emailBody = emailBody + reffernceId   + '\n' + '\n';
        
        sendEmail.setPlainTextBody(emailBody);
        sendEmail.setSubject('RE: '+subject + '  '+caseNumber);
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply@nbnco.com.au'];
        if ( owea.size() > 0 ) {
            sendEmail.setOrgWideEmailAddressId(owea.get(0).Id) ;
        }        
        sendEmail.setToAddresses(new String[] {email});
        sendEmails.add(sendEmail);
        Messaging.sendEmail(sendEmails);        
    }
    
    
    private List<Contact> createContact(Messaging.InboundEmail email) {
        List<Contact> contList = new List<Contact>();
        system.debug('**********email.fromAddress***********'+email.fromAddress);
        system.debug('**********email.fromName***********'+email.fromName);
        system.debug('**********email***********'+email);
        if(String.isEmpty(email.fromAddress)) {
            return contList;
        }
         contList = [SELECT Id, Name, Email, AccountId FROM CONTACT WHERE Email =: email.fromAddress limit 1];       
        String ContactId, AccountId = '0010k00000X4hlo';                 
        //emailBody = emailBody + 'cont:'  + cont + '\n' + '\n';
         
       
        if(contList.isEmpty()) {        
            Contact contact = new Contact();
            if(email.fromName != null) {
                List<String> fromNameValue = email.fromName.trim().split(' ');
                
                
                if(fromNameValue.size() > 1) {
                    contact.FirstName = email.fromName.substring(0,email.fromName.indexOf(' '));
                    contact.LastName = email.fromName.substring(email.fromName.indexOf(' '));
                } else {
                    contact.LastName = email.fromName.trim();
                }
            }
            contact.Email = email.fromAddress;
            id retailSerypeStrId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId();
            List<Account> accountsList = [select id, RecordTypeId, name from account where name='accreditation' and RecordTypeId =: retailSerypeStrId];
            contact.AccountId = accountsList.get(0).id;
           
            Database.SaveResult contactResult = Database.Insert(contact , false );
            // for (Integer i = 0; i < contactResult.size(); i++) {

                if (!contactResult.isSuccess()) {

                    //SVMXC__Service_Contract_Products__c errorCP = coveredProductsListToInsert.get(i);

                    Database.Error error = contactResult.getErrors().get(0);
                    System.debug(LoggingLevel.INFO,'******contact error****'+error.getMessage());
                }
             //}
            
            contList = [SELECT Id, Name, Email, AccountId FROM CONTACT WHERE Email =: email.fromAddress order by createdDate DESC limit 1];           
            System.debug('New Contact Created');
        }
        
        return contList;
        
    }

 
 private Task createTask(Messaging.InboundEmail email,List<Contact> contactList,Case casesRecord){
    Task taskRecord = new Task();                                    
    taskRecord.Type = 'EU';
    taskRecord.Priority = '3-General';
    taskRecord.Subject = email.subject;
    taskRecord.Description = email.plainTextBody;
    taskRecord.Status = 'Open';
    taskRecord.Due_Date__c = Date.today()+1;
    taskRecord.Outcome__c = 'Open';
    taskRecord.WhatId = casesRecord.Id;
    taskRecord.Type_Custom__c = 'External';
    taskRecord.Sub_Type__c = 'Outbound';
    taskRecord.Category__c = 'Email';
    if(!contactList.isEmpty()) {
      taskRecord.WhoId = contactList.get(0).Id;   
    }
    insert taskRecord;
    return taskRecord;
    
 }
 
 private void createAttachments(Messaging.InboundEmail email,Task taskObj){
    if (email.textAttachments != null && email.textAttachments.size() > 0)
    {
     
      for(Messaging.InboundEmail.textAttachment tAttachment : email.textAttachments)
      {
          Attachment att = new Attachment();
          att.name = tAttachment.filename;
          att.body = Blob.valueOf(tAttachment.body);
          att.parentid = taskObj.id;
          insert att;
      }                              
    }else if(email.binaryAttachments != null && email.binaryAttachments.size() > 0)
    {
        for(Messaging.InboundEmail.binaryAttachment bAttachment : email.binaryAttachments)
        {
          Attachment att = new Attachment();
          att.name = bAttachment.filename;
          att.body = bAttachment.body;
          att.parentid = taskObj.id;
          insert att;
        }                              
    }
     
 }
 
 private Case createNewCase(Messaging.InboundEmail email,List<Contact> contactList,Id recordTypeStrId,String caseCreatingFrom){
    List<Group> groupList = [select Id from Group where  Type = 'Queue' AND NAME = 'EUE Enquiries And Complaints'];
    Case caseRecord = new Case(); 
   
    
    //Id formulComplaintRecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Formal Complaint').getRecordTypeId();
    
    caseRecord.RecordTypeId = recordTypeStrId;
    /*if(formulComplaintRecordTypeId == recordTypeStrId) {
        caseRecord.Phase__c = 'Use';
        caseRecord.Category__c = 'Use';
        caseRecord.Sub_Category__c = 'Connection/Service Performance Issues';        
    }*/
    
    caseRecord.LastModifiedChannel__c = 'Email';
    caseRecord.Case_Created_From__c = caseCreatingFrom;
    caseRecord.Status = 'Open';
    caseRecord.Origin = 'Email';
    caseRecord.Priority = '3-General';    
    caseRecord.Subject = email.subject;
    caseRecord.Description = email.plainTextBody;
    caseRecord.OwnerId = groupList.get(0).id;
    caseRecord.Primary_Contact_Role__c = 'RSP/ISP/Access Seeker';
    System.debug('*******************'+contactList);
    //User userRecord = [select id,name from user where name='EUEEnquiriesComplaints'];
     //caseRecord.CreatedById = userRecord.id;
    //caseRecord.LastModifiedById = '0050k000001XNQH';
    if(!contactList.isEmpty()) {
       caseRecord.ContactId = contactList[0].id; 
    }

    //insert caseRecord;
    Database.SaveResult MySaveResult = Database.Insert(caseRecord, false );
    if (!MySaveResult.isSuccess()) {
        //SVMXC__Service_Contract_Products__c errorCP = coveredProductsListToInsert.get(i);

        Database.Error error = MySaveResult.getErrors().get(0);
        System.debug(LoggingLevel.INFO,'******case error****'+error.getMessage());
    }
    Id caseId = MySaveResult .getId();
    //}
    List<Case> caseList = [select id,caseNumber,Thread_Id__c from case where id=:caseId];
    System.debug('Email Subject '+ email.subject + ' doest not have Case No.');
    //sendAutoReply(email.subject,email.fromAddress);    
    if(caseList != null && !caseList.isEmpty()) {   
        sendAckEmail(email.subject, email.fromAddress,caseList[0].caseNumber,caseList[0].Thread_Id__c,email.fromName);
    } 
    return caseRecord;
 }
 
 private Case updateCase(Messaging.InboundEmail email,List<Case> caseList){
    Case updCase = new Case(); 
    updCase.Id = caseList.get(0).Id;
    updCase.status = caseList.get(0).status;
    //updCase.Description = email.plainTextBody;
    //updCase.Primary_Contact_Role__c = 'RSP/ISP/Access Seeker';
    updCase.LastModifiedChannel__c = 'Email';
   
    //if(!contactList.isEmpty()) {
    //updCase.LastModifiedById = contactList.get(0).Id;   
    //}
    update updCase;
    sendReplyEmail(email,caseList.get(0));
    return updCase;
 }
 
@TestVisible private void sendReplyEmail(Messaging.InboundEmail email,Case caseObj){
   System.debug('Sending Auto Response'+ caseObj);                          
    List<Messaging.SingleEmailMessage>  sendEmails = new List<Messaging.SingleEmailMessage>();
    Messaging.SingleEmailMessage sendEmail = new Messaging.SingleEmailMessage();
    String senderName = email.fromName;

    String emailBody = '';  
    emailBody += 'Hello '+senderName+',\n\n';
    emailBody += 'Thank you for your enquiry to NBN. The reference number for your enquiry is: '+ caseObj.CaseNumber+'\n\n';
    emailBody += 'An update has been made to your case. If you require more information please email nbn quoting the above reference number.'+'\n\n';
    emailBody += 'Regards\n';
    emailBody += 'nbnco\n';
    
    sendEmail.setPlainTextBody(emailBody);
    sendEmail.setSubject('RE: '+email.subject);
    OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply@nbnco.com.au'];
    if ( owea.size() > 0 ) {
        sendEmail.setOrgWideEmailAddressId(owea.get(0).Id) ;
    }        
    sendEmail.setToAddresses(new String[] {email.fromAddress});
    sendEmails.add(sendEmail);
    Messaging.sendEmail(sendEmails);        
}
 
}