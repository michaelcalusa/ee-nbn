/************************************************************************************************************
Version                 : 1.0 
Created Date            : 22-09-2016
Description/Function    :   1.When a new Inbound_Staging__c is created synch with internal Salesforce Data using the Payload
Used in                 : 
Modification Log        :
* Developer                   Date                   Description
* -------------------------------------------------------------------------                
* Asif Khan             01-04-2019              Created the Class
**************************************************************************************************************/
public class InboundStagingTriggerHandler{
    //Class level variables that are commonly used
    private boolean isExecuting = false;
    private integer batchSize;
    private List<Inbound_Staging__c> trgOldList = new List<Inbound_Staging__c> ();
    private List<Inbound_Staging__c> trgNewList = new List<Inbound_Staging__c> ();
    private Map<id,Inbound_Staging__c> trgOldMap = new Map<id,Inbound_Staging__c> ();
    private Map<id,Inbound_Staging__c> trgNewMap = new Map<id,Inbound_Staging__c> ();
    private static boolean isAsync;
    // Below 7 boolean variables are used to Prevent recursion
    public static boolean isBeforeInsertFirstRun = true;
    public static boolean isBeforeUpdateFirstRun = true;
    public static boolean isBeforeDeleteFirstRun = true;
    public static boolean isAfterInsertFirstRun = true;
    public static boolean isAfterUpdateFirstRun = true;
    public static boolean isAfterDeleteFirstRun = true;
    public static boolean isAfterUndeleteFirstRun = true;
    /***************************************************************************************************
    Method Name         : InboundStagingTriggerHandler
    Method Type         : Constructor
    Version             : 1.0 
    Created Date        : 22-09-2016 
    Function            : used to assign the Trigger values to the Class variables
    Input Parameters    : Trigger Context Variables (6 parameters - 1 boolean, 1 integer, 2 List and 2 Map) 
    Output Parameters   : None
    Description         :  
    Used in             : "Inbound_Staging__c Trigger"
    Modification Log    :
    * Developer                   Date                   Description
    * --------------------------------------------------------------------------------------------------                 
    * Asif Khan              01-04-2019                Created
    ****************************************************************************************************/  
    public InboundStagingTriggerHandler(boolean isExecuting, integer batchSize, List<Inbound_Staging__c> trgOldList, List<Inbound_Staging__c> trgNewList, Map<id,Inbound_Staging__c> trgOldMap, Map<id,Inbound_Staging__c> trgNewMap){
        this.isExecuting = isExecuting;
        this.BatchSize = batchSize;
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
        InboundStagingTriggerHandler.isAsync = System.isBatch() || System.isFuture();
    }
    public void OnBeforeInsert(){}
    public void OnBeforeUpdate(){}
    public void OnBeforeDelete(){}
    public void OnAfterInsert()
    {
        if(!HelperUtility.isTriggerMethodExecutionDisabled('SyncFromInBoundMsg'))
        {
            IBMessagePayloadUtilities.synchSFDCDataFromIB(trgNewList);
        }
    }
    public void OnAfterUpdate(){}
    public void OnAfterDelete(){}
    public void OnUndelete(){}
    /*---------------------------------------------------------------------------------------------------------------------------------------------------*/
        
}