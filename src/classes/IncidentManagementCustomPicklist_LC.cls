public class IncidentManagementCustomPicklist_LC extends VisualEditor.DynamicPickList {
    
/*------------------------------------------------------------
Author:      Kashyap Murthy
Company:     Contractor
Description: Class to get all field names for an object
Inputs:      None
Test Class:    
History
<Date>      <Author>     <Description>
------------------------------------------------------------*/

    public override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('', '');
        return defaultValue;
    }
    
    public override VisualEditor.DynamicPickListRows getValues(){
	   SObject mySObj = new Incident_Management__c();
       Map<String,Schema.DescribeFieldResult> fieldsMap = getFieldMetadata(mySObj);
       VisualEditor.DynamicPickListRows  custPickVals = new VisualEditor.DynamicPickListRows();
        VisualEditor.DataRow pickVal1 = new VisualEditor.DataRow('','');
        custPickVals.addRow(pickVal1);
      	for (String key: fieldsMap.keyset())
        {
            VisualEditor.DataRow pickVal2 = new VisualEditor.DataRow( fieldsmap.get(key).getLabel().toUpperCase(), fieldsmap.get(key).getName().toUpperCase());
            custPickVals.addRow(pickVal2);
        	System.debug('Test');
        }
        return custPickVals;
    }
    
    public static Map<String, Schema.DescribeFieldResult> getFieldMetadata(sObject mySObject){
        Schema.DescribeSObjectResult dsor = mySObject.getSObjectType().getDescribe();
        // the map to be returned with the final data
  		Map<String,Schema.DescribeFieldResult> finalMap = new Map<String, Schema.DescribeFieldResult>();
  		// map of all fields in the object
  		Map<String, Schema.SObjectField> objectFields = dsor.fields.getMap();
        // iterate over the requested fields and get the describe info for each one. 
     	// add it to a map with field name as key
        for(String key: objectFields.keyset()){
        	Schema.DescribeFieldResult dr = objectFields.get(key).getDescribe();
            // add the results to the map to be returned
            finalMap.put(key, dr); 
        }
        return finalMap;
    }

}