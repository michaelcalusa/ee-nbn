/*------------------------------------------------------------------------------------------------------
<Date>        <Authors Name>        <Brief Description of Change> 
12-12-2018  	 Mohith R        	MSEU-18932 : Handle update preferences from formAPI to Individual object
-------------------------------------------------------------------------------------------------------*/
public class IndividualPreferences {
    //Class variables
    public static boolean InsertIndividual = true;
    
    //Method to convert JSON Data to Individual object 
    public static void IndividualPreferenceProcessing(List<Global_form_staging__c> input){               
        List<Global_Form_Staging__c> PreferenceStagingRecords = input;
        
        system.debug('PreferenceStagingRecords size is '+ PreferenceStagingRecords.size());
        if(PreferenceStagingRecords.size() > 0 ){
            StagingIndividualConversion(PreferenceStagingRecords);
        }
    }
    
    //Method to update individual preference records
    public static void StagingIndividualConversion(List<Global_form_staging__c> stagingIndividuals){
        List<Individual> insertIndividual = new List<Individual>();
        Set<string> GobalFormStagingNameSet = new Set<string>();
        List<GlobalFormEventTriggerHandlerUtil.GlobalFormErrorLogWrapper> GlobalErrorLogs = new List<GlobalFormEventTriggerHandlerUtil.GlobalFormErrorLogWrapper>();
		
        List<IndividualUtilities_Bulk.IndividualModel> listInd = new List<IndividualUtilities_Bulk.IndividualModel>();
        
        system.debug('stagingIndividuals********'+JSON.serialize(stagingIndividuals));
        try{
            Database.DMLOptions dmo = new Database.DMLOptions();
            //Deserialise the JSON object and update the individual object
            for(Global_form_staging__c stagingRecord:stagingIndividuals)
            {
                IndividualUtilities_Bulk.IndividualModel  vResponse  = new IndividualUtilities_Bulk.IndividualModel();
                system.debug('stagingRecord********'+stagingRecord.Data__c);
                vResponse = (IndividualUtilities_Bulk.IndividualModel)system.JSON.deserialize(stagingRecord.Data__c,IndividualUtilities_Bulk.IndividualModel.class);            	
                listInd.add(vResponse);
                system.debug('listInd********'+JSON.serialize(listInd));            	
            }
            //update the preferences for the record 
            IndividualUtilities_Bulk.updateIndividualByModel(listInd);
            
        }catch(Exception e){
            if(!(test.isRunningTest())){
                GlobalFormEventTriggerHandlerUtil.retryPlatformEventAndLogError('Global Form Event trigger handler general exception thrown', 'StagingIndividualConversion() method', e, null);                                        
            }
        }
    }
}