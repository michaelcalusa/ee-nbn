/*------------------------------------------------------------------------------------------------------
<Date>        <Authors Name>        <Brief Description of Change> 
12-12-2018  	 Mohith R        	MSEU-18932 : Handle update preferences from formAPI to Individual object
-------------------------------------------------------------------------------------------------------*/

public class IndividualPreferencesProcessing {
    //Class variables
    public static final string Form_Resource_Type = 'Preference Center';
    public static boolean updateIndividualPreferences = true;
    
    @InvocableMethod
    public static void ProcessBuilderInvokeMethod(List<Global_Form_Staging__c> trgNewList){
        system.debug('proces builder invoked the process');
        Id recordid = trgNewList[0].id;
        
        List<Id> recordIds = new List<Id> ();
        if(trgNewList != null && trgNewList.size() > 0){
            for(Global_Form_Staging__c singleRecord:trgNewList){
                recordIds.add(singleRecord.id);
            }            
        }
        
        //Processing the records that need to be updated on the Individual      
        
        List<Global_form_staging__c> toBeUpdatedIndRecords = [select id, Name, Status__c, Type__c, Data__c, Content_Type__c from Global_Form_Staging__c  where id in: recordIds];  
        List<Global_form_staging__c> CompletedPrefRecords = new List<Global_form_staging__c>();
        try{
            for(Global_form_staging__c singleGlobalFormStagingRecord: toBeUpdatedIndRecords){
                if(singleGlobalFormStagingRecord.status__c == 'Completed'){
                    CompletedPrefRecords.add(singleGlobalFormStagingRecord);
                }
            }
            
            if(CompletedPrefRecords != null && CompletedPrefRecords.size() > 0 ){
                IndividualPreferences.IndividualPreferenceProcessing(CompletedPrefRecords);
            }
            
        }catch(Exception e){
            if(!(test.isRunningTest())){
                GlobalFormEventTriggerHandlerUtil.retryPlatformEventAndLogError('Global Form Event general exception thrown', 'IndividualPreferenceProcessing() method', e, null);                                        
            }
        }  
        
    }
}