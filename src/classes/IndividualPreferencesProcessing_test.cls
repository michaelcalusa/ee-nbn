@isTest
public class IndividualPreferencesProcessing_test {  
  @testsetup static void testRecordSetup(){     
        List<Global_Form_Staging__c> gfslist = new List<Global_Form_Staging__c>();
        Global_Form_Staging__c gfs1 = new Global_Form_Staging__c();
        gfs1.Content_Type__c = 'Test Preference Record';
        gfs1.Data__c = '{"firstName":"Geoff","lastName":"Saxby","email":"geoff.saxby@wollies.com","phoneNumber":"0404220000", "pID":"0PK6F000-000f-fxU2-0PK6-00000fxU2WAI","nbnProducts":true,"nbnUpdates":true,"telesales":false, "hasOptedOutOfEmail":false}';
        gfs1.Status__c = 'Completed';
        gfs1.Type__c = 'Preference Center'; 
        gfslist.add(gfs1);
        insert gfslist;
    }
    
    @isTest static void ProcessBuilderInvokeMethod_Test(){

 		List<Global_Form_Staging__c> testrecords = [select id from Global_Form_Staging__c];
        system.debug('testrecords is '+testrecords);
 		IndividualPreferencesProcessing.ProcessBuilderInvokeMethod(testrecords);
        
       // List<Lead> results = [select id,lastname,firstname from lead where LastName = 'Test Wayne 18'];
        //system.assertNotEquals(null, results);        
    }

}