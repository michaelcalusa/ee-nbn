@isTest
public class IndividualPreferences_Test {
    @testSetup static void createSetup(){
        List<id> stagingRecordIds = new list<id>();
        List<Global_Form_Staging__c> gfslist = new List<Global_Form_Staging__c>();
        Global_Form_Staging__c gfs1 = new Global_Form_Staging__c();
        gfs1.Content_Type__c = 'Test Preference Record';
        gfs1.Data__c = '{"firstName":"Geoff","lastName":"Saxby","email":"geoff.saxby@wollies.com","phoneNumber":"0404220000", "pID":"0PK6F000-000f-fxU2-0PK6-00000fxU2WAI","nbnProducts":true,"nbnUpdates":true,"telesales":false, "hasOptedOutOfEmail":false}';
        gfs1.Status__c = 'Completed';
        gfs1.Type__c = 'Preference Center'; 
        gfslist.add(gfs1);
        insert gfslist;
    }  
    @isTest static void testStagingIndividualConversion(){
        List<Global_Form_Staging__c> testRecordOne =  [select Name,Content_Type__c,Data__c,Status__c,Type__c from Global_Form_Staging__c where Content_Type__c = 'Test Preference Record' limit 1];
        test.startTest();
        IndividualPreferences.StagingIndividualConversion(testRecordOne);
        test.stopTest();
    }
}