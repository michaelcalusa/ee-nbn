/*------------------------------------------------------------------------------------------------------
    Class Name          : IndividualTriggerHandler
    Version             : 1.0 
    Created Date        : 12-12-2018
    Author              : Mohith

    <Date>        <Authors Name>        <Brief Description of Change> 
    12-12-2018       Mohith R           MSEU-19986 : Reflect updated preference page information to Lead object
                                        MSEU-19988 : Sync subscription pref & Channel info to Contact object
-------------------------------------------------------------------------------------------------------*/

public class IndividualTriggerHandler {
    private boolean isExecuting = false;
    private integer batchSize;
    private List<Individual> trgOldList = new List<Individual> ();
    private List<Individual> trgNewList = new List<Individual> ();
    private Map<id,Individual> trgOldMap = new Map<id,Individual> ();
    private Map<id,Individual> trgNewMap = new Map<id,Individual> ();
    public static boolean isBeforeUpdateFirstRun = true;
    public static boolean isAfterUpdateFirstRun = true;
   
    public IndividualTriggerHandler(boolean isExecuting, integer batchSize, List<Individual> trgOldList, List<Individual> trgNewList, Map<id,Individual> trgOldMap, Map<id,Individual> trgNewMap){
        this.isExecuting = isExecuting;
        this.BatchSize = batchSize;
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;

    }

    public void OnBeforeUpdate()
    {
        if(!HelperUtility.isTriggerMethodExecutionDisabled('SetEmailOptOutDate')){
            if(isBeforeUpdateFirstRun)
            {                                
                processIndividuals();
                isBeforeUpdateFirstRun = false;
            }
        }
    }

    public void OnBeforeInsert()
    {
        if(!HelperUtility.isTriggerMethodExecutionDisabled('SetEmailOptOutDate')){
                processIndividuals();
        }
    }
    
    //The scenario is update the contact & lead object after the Individual is updated
    public void OnAfterUpdate()
    {
        if(!HelperUtility.isTriggerMethodExecutionDisabled('UpdateContatAndLeadPrefByIndividual'))
        {
            if(isAfterUpdateFirstRun)
            {
                List<Individual> lstInd = new List<Individual>();
                for(Individual indVal : trgNewList)
                {
                    if(trgOldMap.get(indVal.id).TriggerSwitch__c == indVal.TriggerSwitch__c)
                        lstInd.add(indVal);
                }

                if(lstInd.size() > 0)
                {
                    system.debug('OnAfterUpdate - trgNewList....'+trgNewList); 
                    IndividualUtilities_Bulk.UpdateContatAndLeadPrefByIndividual(trgNewList);       
                    isAfterUpdateFirstRun = false;
                }                
            }
        }       
    }
    
    public void processIndividuals()
    {
        if(trgNewList.size() > 0)
        {
            for(Individual ind : trgNewList)
            {
                if(!ind.nbn_updates__c && !ind.NBN_Products__c && !ind.telesales__c &&
                    (!ind.Unsubscribe_all__c && ind.Unsubscribed_date__c == null) &&
                    (!ind.HasOptedOutOfEmail__c && ind.Email_Opt_Out_Date__c == null))
                {
                    ind.HasOptedOutOfEmail__c = true;
                    ind.Email_Opt_Out_Date__c = Date.today();
                    ind.Unsubscribe_all__c = true;
                    ind.Unsubscribed_date__c = Date.today();
                    
                }
                else if(!ind.nbn_updates__c && !ind.NBN_Products__c && !ind.telesales__c &&
                    (!ind.Unsubscribe_all__c && ind.Unsubscribed_date__c == null))                  
                {                    
                    ind.Unsubscribe_all__c = true;
                    ind.Unsubscribed_date__c = Date.today();
                }                      
                else if(!ind.nbn_updates__c && !ind.NBN_Products__c && ind.telesales__c)
                {
                    if(!ind.HasOptedOutOfEmail__c && ind.Email_Opt_Out_Date__c == null)
                    {
                        ind.HasOptedOutOfEmail__c = true;
                        ind.Email_Opt_Out_Date__c = Date.today();
                        ind.Unsubscribe_all__c = false;
                        ind.Unsubscribed_date__c = null;
                    }
                    else
                    {                        
                        ind.Unsubscribe_all__c = false;
                        ind.Unsubscribed_date__c = null;
                    }
                }
                else
                {
                    if((ind.Unsubscribe_all__c) &&
                        (ind.nbn_updates__c || ind.NBN_Products__c || ind.telesales__c))
                    {
                        ind.Unsubscribe_all__c = false;
                        ind.Unsubscribed_date__c = null;
                        
                        if(ind.nbn_updates__c || ind.NBN_Products__c)
                        {
                            
                            ind.HasOptedOutOfEmail__c = false;
                            ind.Email_Opt_Out_Date__c = null;
                        }
                    }
                    if((ind.HasOptedOutOfEmail__c) &&
                        (ind.nbn_updates__c || ind.NBN_Products__c))
                    {
                        
                        ind.HasOptedOutOfEmail__c = false;
                        ind.Email_Opt_Out_Date__c = null;
                    }
                    
                }          
            }   
        }       
    }
}