@isTest
public class IndividualTrigger_Test {
    @testSetup static void createSetup()
    {
        
    }  
    @isTest static void testIndividualTrigger(){
        Individual ind = new Individual();
        ind.FirstName = 'con.firstName';
        ind.LastName = 'con.lastName';
        ind.Email__c = 'con@emailm.com';
        ind.Phone__c = '0479852274';
        
        ind.NBN_Products__c = true;
        ind.nbn_updates__c = true;
        ind.telesales__c = true;
        insert ind;
        ind.HasOptedOutOfEmail__c = false;
        test.startTest();
        ind.NBN_Products__c = true;
        update ind;
        test.stopTest();
    }
}