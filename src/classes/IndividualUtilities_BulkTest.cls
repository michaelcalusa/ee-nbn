/***************************************************************************************************
    Class Name  :  IndividualUtilities_BulkTest
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  Jan 29,2019 
    Function    :  This class contains unit test scenarios for IndividualUtilities_Bulk
    Used in     :  None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            Jan 29,2019                 Created
    * Rupendra Vats            March 12,2019               Updated
****************************************************************************************************/

@isTest(seeAllData = false)
public Class IndividualUtilities_BulkTest{
    static testMethod void TestMethod_SetIndividualsToEndCustomerContacts() {
         
        List<Global_Form_Staging__c> gfslist = new List<Global_Form_Staging__c>();
        Global_Form_Staging__c gfs = new Global_Form_Staging__c();
        gfs.Content_Type__c = 'Test Preference Record';
        gfs.Data__c = '{"firstName":"Geoff","lastName":"Saxby","email":"geoff.saxby@wollies.com","phoneNumber":"0404220000", "pID":"0PK6F000-000f-fxU2-0PK6-00000fxU2WAI","nbnProducts":true,"nbnUpdates":true,"telesales":false, "hasOptedOutOfEmail":false}';
        gfs.Status__c = 'Completed';
        gfs.Type__c = 'Preference Center'; 
        gfslist.add(gfs);
        insert gfslist;
        
       /*
        Salutation,FirstName,LastName,Email__c,
                                                NBN_Products__c,nbn_updates__c,
                                                telesales__c, Email_Opt_Out_Date__c, 
                                                HasOptedOutOfEmail__c, PID__c, Phone__c
                                                FROM Individual*/
        List<Individual> lstIndv = new List<Individual>();
        Individual i = new Individual(FirstName = 'first',LastName = 'last',Email__c = 'testind@test.com');
        lstIndv.add(i);
        insert lstIndv;
        Individual testIndv = [SELECT Id,Salutation,FirstName,LastName,Email__c,TriggerSwitch__c,
                                                NBN_Products__c,nbn_updates__c,
                                                telesales__c, Email_Opt_Out_Date__c, 
                                                HasOptedOutOfEmail__c, PID__c, Phone__c
                                                FROM Individual WHERE Id =:i.Id limit 1];
        
        IndividualUtilities_Bulk.UpdateContatAndLeadPrefByIndividual(lstIndv);
        
        IndividualUtilities_Bulk.IndividualModel  vResponse  = new IndividualUtilities_Bulk.IndividualModel();
        List<IndividualUtilities_Bulk.IndividualModel> listInd = new List<IndividualUtilities_Bulk.IndividualModel>();
        listInd.add(vResponse);
        IndividualUtilities_Bulk.updateIndividualByModel(listInd);
            
        String strConRTEndCust;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRTEndCust = rtMapByName.get('Business End Customer').getRecordTypeId();  
        
        String strAccRTEndCust;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRTEndCust = rtMapByNameB.get('Business End Customer').getRecordTypeId();  
        
        String strLeadRTEndCust;
        Schema.DescribeSObjectResult resultC = Schema.SObjectType.Lead; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameC = resultC.getRecordTypeInfosByName();
        strLeadRTEndCust = rtMapByNameC.get('Business End Customer').getRecordTypeId();
        
        Account accA = new Account(Name = 'Test End', RecordTypeID = strAccRTEndCust, ABN__c = '33051775556');
        insert accA;
        
        List<Contact> lstCon = new List<Contact>();
        Contact conA = new Contact(LastName = 'ConLastA', RecordTypeID = strConRTEndCust, AccountId = accA.Id, Email = 'test@gmail.com');
        lstCon.add(conA);

        Contact conB = new Contact(LastName = 'ConLastB', RecordTypeID = strConRTEndCust, AccountId = accA.Id);
        lstCon.add(conB);
                
        insert lstCon;
        
        conA.Email = 'testind@test.com';
        conA.LastName = 'Con Last New';
        conA.IndividualId = i.Id;
        update conA;
        
        List<Lead> lstLead =  new List<Lead>();
        Lead leadRec = new Lead();
        leadRec.FirstName = 'test';
        leadRec.LastName = 'last';
        leadRec.Email = 'testind@test.com';
        leadRec.RecordTypeID = strLeadRTEndCust;
        leadRec.Company = 'ÁBC';
        leadRec.Phone = '0123456789';
        leadRec.Preferred_Contact_Method__c = 'Email';
        leadRec.Status = 'Ópen';
        leadRec.LeadSource = 'Conference';
        leadRec.ABN__c = '53004085616';
        lstLead.add(leadRec);
        
        Lead leadRecB = new Lead();
        leadRecB.FirstName = 'test';
        leadRecB.LastName = 'last';
        leadRecB.Email = 'testindB@test.com';
        leadRecB.RecordTypeID = strLeadRTEndCust;
        leadRecB.Company = 'ÁBC';
        leadRecB.Phone = '0123456789';
        leadRecB.Preferred_Contact_Method__c = 'Email';
        leadRecB.Status = 'Ópen';
        leadRecB.LeadSource = 'Conference';
        leadRecB.ABN__c = '53004085616';
        lstLead.add(leadRecB);
        
        insert lstLead;
        
        Lead l = [SELECT Id, FirstName,LastName,Email,Phone,
                                    NBN_Products__c,nbn_updates__c,
                                    telesales__c, IndividualId, HasOptedOutOfEmail
                                    FROM Lead WHERE Id =:leadRecB.Id limit 1];
        l.FirstName = 'testnew';
        l.LastName = 'testnew';
        l.Phone = '0987654321';
        l.Email = 'testindnew@test.com';
        l.Salutation = 'Mrs.';
        l.IndividualId = testIndv.Id;

        update l;
        
        IndividualUtilities_Bulk.UpdateIndividualsByLeads(lstLead, true);
        IndividualUtilities_Bulk.UpdateIndividualsByLeads(lstLead, false);
        
        map<string, Individual> pidMapNewInd = new map<string, Individual>();
        pidMapNewInd.put(testIndv.Id, testIndv);
        
        IndividualUtilities_Bulk.UpdateContatAndLeadByIndividual(lstIndv, pidMapNewInd);
        IndividualUtilities_Bulk.SetIndividualsToLead(lstLead);
        IndividualUtilities_Bulk.SetIndividualsToContacts(lstCon);
        
        IndividualUtilities_Bulk.IndividualModel  indmodel  = new IndividualUtilities_Bulk.IndividualModel();
        indmodel.firstName = 'test name 12';
        indmodel.lastName = 'test last 12' ;
        indmodel.email = 'test7689@gmail.com';
        indmodel.nbnProducts = false;
        indmodel.nbnUpdates = false;
        indmodel.telesales = false;
        indmodel.unsubscribeAll = true;
        indmodel.hasOptedOutOfEmail = false;
        indmodel.optedOutOfEmailDate = Date.toDay();
        indmodel.phoneNumber = '0123456789';
        indmodel.events = false;
        indmodel.productInfo = false;
        indmodel.nbnIndustryEvents = false;
        indmodel.nbnWholeSaleSolution = false;
        indmodel.pID = testIndv.PiD__c; //'0PKN0000-0004-4CHl-0PKN-000004CHlOAM';
        
        List<IndividualUtilities_Bulk.IndividualModel> lstIndMd = new List<IndividualUtilities_Bulk.IndividualModel>();
        lstIndMd.add(indmodel);
        IndividualUtilities_Bulk.updateIndividualByModel(lstIndMd);
        IndividualUtilities_Bulk obj = new IndividualUtilities_Bulk();
    }
    
    
    static testMethod void TestMethod_SetIndividualsToPartnerContacts() {
         
        List<Global_Form_Staging__c> gfslist = new List<Global_Form_Staging__c>();
        Global_Form_Staging__c gfs = new Global_Form_Staging__c();
        gfs.Content_Type__c = 'Test Preference Record';
        gfs.Data__c = '{"firstName":"Geoff","lastName":"Saxby","email":"geoff.saxby@wollies.com","phoneNumber":"0404220000", "pID":"0PK6F000-000f-fxU2-0PK6-00000fxU2WAI","nbnProducts":true,"nbnUpdates":true,"telesales":false, "hasOptedOutOfEmail":false}';
        gfs.Status__c = 'Completed';
        gfs.Type__c = 'Preference Center'; 
        gfslist.add(gfs);
        insert gfslist;
        
        List<Individual> lstIndv = new List<Individual>();
        Individual i = new Individual(FirstName = 'first',LastName = 'last',Email__c = 'testind@test.com',Phone__c = '0479852274',NBN_Products__c = true,nbn_updates__c = true,telesales__c = true);

        lstIndv.add(i);
        insert lstIndv;
        
        Individual testIndv = [SELECT Id,Salutation,FirstName,LastName,Email__c,TriggerSwitch__c,
                                                NBN_Products__c,nbn_updates__c,
                                                telesales__c, Email_Opt_Out_Date__c, 
                                                HasOptedOutOfEmail__c, PID__c, Phone__c
                                                FROM Individual WHERE Id =:i.Id limit 1];
        
        IndividualUtilities_Bulk.UpdateContatAndLeadPrefByIndividual(lstIndv);
        
        IndividualUtilities_Bulk.IndividualModel  vResponse  = new IndividualUtilities_Bulk.IndividualModel();
        List<IndividualUtilities_Bulk.IndividualModel> listInd = new List<IndividualUtilities_Bulk.IndividualModel>();
        listInd.add(vResponse);
        IndividualUtilities_Bulk.updateIndividualByModel(listInd);
            
        String strConRTEndCust;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRTEndCust = rtMapByName.get('Partner Contact').getRecordTypeId();  
        
        String strAccRTEndCust;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRTEndCust = rtMapByNameB.get('Partner Account').getRecordTypeId();  
        
        String strLeadRTEndCust;
        Schema.DescribeSObjectResult resultC = Schema.SObjectType.Lead; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameC = resultC.getRecordTypeInfosByName();
        strLeadRTEndCust = rtMapByNameC.get('Business Segment ICT').getRecordTypeId();
        
        Account accA = new Account(Name = 'Test End', RecordTypeID = strAccRTEndCust, ABN__c = '33051775556');
        insert accA;
        
        List<Contact> lstCon = new List<Contact>();
        Contact conA = new Contact(LastName = 'ConLastA', RecordTypeID = strConRTEndCust, AccountId = accA.Id, Email = 'test@gmail.com');
        lstCon.add(conA);

        Contact conB = new Contact(LastName = 'ConLastB', RecordTypeID = strConRTEndCust, AccountId = accA.Id);
        lstCon.add(conB);
                
        insert lstCon;
        
        conA.Email = 'testind@test.com';
        conA.LastName = 'Con Last New';
        conA.IndividualId = i.Id;
        update conA;
        
        List<Lead> lstLead =  new List<Lead>();
        Lead leadRec = new Lead();
        leadRec.FirstName = 'test';
        leadRec.LastName = 'last';
        leadRec.Email = 'testind@test.com';
        leadRec.RecordTypeID = strLeadRTEndCust;
        leadRec.Company = 'ÁBC';
        leadRec.Phone = '0123456789';
        leadRec.Preferred_Contact_Method__c = 'Email';
        leadRec.Status = 'Ópen';
        leadRec.LeadSource = 'Conference';
        leadRec.ABN__c = '53004085616';
        lstLead.add(leadRec);
        
        Lead leadRecB = new Lead();
        leadRecB.FirstName = 'test';
        leadRecB.LastName = 'last';
        leadRecB.Email = 'testindB@test.com';
        leadRecB.RecordTypeID = strLeadRTEndCust;
        leadRecB.Company = 'ÁBC';
        leadRecB.Phone = '0123456789';
        leadRecB.Preferred_Contact_Method__c = 'Email';
        leadRecB.Status = 'Ópen';
        leadRecB.LeadSource = 'Conference';
        leadRecB.ABN__c = '53004085616';
        lstLead.add(leadRecB);
        
        insert lstLead;
        
        Lead l = [SELECT Id, FirstName,LastName,Email,Phone,
                                    NBN_Products__c,nbn_updates__c,
                                    telesales__c, IndividualId, HasOptedOutOfEmail
                                    FROM Lead WHERE Id =:leadRecB.Id limit 1];
        l.FirstName = 'testnew';
        l.LastName = 'testnew';
        l.Phone = '0987654321';
        l.Email = 'testindnew@test.com';
        l.Salutation = 'Mrs.';
        l.IndividualId = testIndv.Id;

        update l;
        
        IndividualUtilities_Bulk.UpdateIndividualsByLeads(lstLead, true);
        IndividualUtilities_Bulk.UpdateIndividualsByLeads(lstLead, false);
        
        map<string, Individual> pidMapNewInd = new map<string, Individual>();
        pidMapNewInd.put(testIndv.Id, testIndv);
        
        IndividualUtilities_Bulk.UpdateContatAndLeadByIndividual(lstIndv, pidMapNewInd);
        IndividualUtilities_Bulk.SetIndividualsToLead(lstLead);
        IndividualUtilities_Bulk.SetIndividualsToContacts(lstCon);
        
        IndividualUtilities_Bulk.IndividualModel  indmodel  = new IndividualUtilities_Bulk.IndividualModel();
        indmodel.firstName = 'test name 12';
        indmodel.lastName = 'test last 12' ;
        indmodel.email = 'test7689@gmail.com';
        indmodel.nbnProducts = false;
        indmodel.nbnUpdates = false;
        indmodel.telesales = false;
        indmodel.unsubscribeAll = true;
        indmodel.hasOptedOutOfEmail = false;
        indmodel.optedOutOfEmailDate = Date.toDay();
        indmodel.phoneNumber = '0123456789';
        indmodel.events = false;
        indmodel.productInfo = false;
        indmodel.nbnIndustryEvents = false;
        indmodel.nbnWholeSaleSolution = false;
        indmodel.pID = testIndv.PiD__c; //'0PKN0000-0004-4CHl-0PKN-000004CHlOAM';
        
        List<IndividualUtilities_Bulk.IndividualModel> lstIndMd = new List<IndividualUtilities_Bulk.IndividualModel>();
        lstIndMd.add(indmodel);
        IndividualUtilities_Bulk.updateIndividualByModel(lstIndMd);
        IndividualUtilities_Bulk obj = new IndividualUtilities_Bulk();
        
        List<Individual> lstIndividuals = new List<Individual>();
        Individual indA = new Individual();
        indA.FirstName = 'first1';
        indA.LastName = 'last1';
        indA.Email__c = 'testindA1@test.com';
        indA.Phone__c = '0479852274';
        indA.nbn_updates__c = false;
        indA.NBN_Products__c = false;
        indA.telesales__c = false;
        indA.Unsubscribe_all__c = false;
        indA.Unsubscribed_date__c = null;
        indA.HasOptedOutOfEmail__c = true;
        indA.Email_Opt_Out_Date__c = Date.toDay();
        
        Individual indB = new Individual();
        indB.FirstName = 'first1';
        indB.LastName = 'last1';
        indB.Email__c = 'testindB1@test.com';
        indB.Phone__c = '0479852274';
        indB.nbn_updates__c = false;
        indB.NBN_Products__c = false;
        indB.telesales__c = true;
        indB.Unsubscribe_all__c = true;
        indB.Unsubscribed_date__c = null;
        indB.HasOptedOutOfEmail__c = true;
        indB.Email_Opt_Out_Date__c = null;

        Individual indC = new Individual();
        indC.FirstName = 'first1';
        indC.LastName = 'last1';
        indC.Email__c = 'testindC1@test.com';
        indC.Phone__c = '0479852274';
        indC.nbn_updates__c = false;
        indC.NBN_Products__c = true;
        indC.telesales__c = false;
        indC.Unsubscribe_all__c = true;
        indC.Unsubscribed_date__c = null;
        indC.HasOptedOutOfEmail__c = true;
        indC.Email_Opt_Out_Date__c = null;

        Individual indD = new Individual();
        indD.FirstName = 'first1';
        indD.LastName = 'last1';
        indD.Email__c = 'testindD1@test.com';
        indD.Phone__c = '0479852274';
        indD.nbn_updates__c = false;
        indD.NBN_Products__c = false;
        indD.telesales__c = true;
        indD.Unsubscribe_all__c = true;
        indD.Unsubscribed_date__c = null;
        indD.HasOptedOutOfEmail__c = false;
        indD.Email_Opt_Out_Date__c = null;
        
        lstIndividuals.add(indA);
        lstIndividuals.add(indB);
        lstIndividuals.add(indC);
        lstIndividuals.add(indD);
        
        insert lstIndividuals;
        
        IndividualTriggerHandler indObj = new IndividualTriggerHandler(true, 50, null, lstIndividuals, null, null);
        indObj.processIndividuals();
    }
}