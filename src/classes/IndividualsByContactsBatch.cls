/*------------------------------------------------------------
Author:         Asif A Khan
Company:        NBN
Description:    Reads the data loaded from Contact
                Can retrieve 50,000,000 record but execute method will only process
                max 2000 record at a time if max limit not set than 200 records at at time                
                Using Database.Stateful to maintain the list of all records return by Database.getQueryLocator
                
Test Class:     
History
<Date>          <Authors Name>      <Brief Description of Change>
Dec 12 2018      Asif A Khan            Created
------------------------------------------------------------*/
global class IndividualsByContactsBatch implements Database.Batchable<sobject>, Database.Stateful, Database.AllowsCallouts,Schedulable 
{
    
    public list<Contact> contactList;    

    global IndividualsByContactsBatch() 
    {        
        contactList = new list<Contact>();        
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Integer maxRecordForProcess = 5000000;        
        
        String strQuery = 'SELECT Id, Salutation, FirstName,LastName,Email,RecordTypeId, NBN_Products__c,nbn_updates__c,telesales__c,Phone,HasOptedOutOfEmail,ICT_nbn_Business_News__c,ICT_nbn_Business_Products_Services__c,ICT_Channel_Program_Updates__c, IndividualId,AccountId FROM Contact WHERE IndividualId = null AND Email != null AND (RecordType.Name = \'Business End Customer\' OR RecordType.Name = \'Partner Contact\') ORDER BY Email, AccountId DESC limit '+maxRecordForProcess;
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<Contact> scope)
    {       
        list<Contact> response = new list<Contact>();
        string childBatchId = BC.getChildJobId();
        string parentBatchId = BC.getJobId();

        System.debug('Scope Size ---->>>>'+scope.size());
        try
        {            
            response = IndividualUtilities_Bulk.SetIndividualsToContacts(scope);

            if(response.size() > 0)
            {
                contactList.addAll(response);
                System.debug('Contact Synced Size ---->>>>'+response.size());
                update contactList;
            }            
        }
        catch(Exception ex)
        {
            System.debug('Exception in Class IndividualsByContactsBatch function Execute ---->>>>'+ex.getMessage()+'---'+ex.getStackTraceString());
            
        }               
    }

    global void finish(Database.BatchableContext BC)
    {        
        System.debug('Finishing IndividualsByContactsBatch Updated '+contactList.size()+' Contact records with IndividualId');
        try
        {
                      
        }
        catch(Exception ex)
        {
            System.debug('Exception in Class IndividualsByContactsBatch function finish ---->>>>'+ex.getMessage()+'---'+ex.getStackTraceString());
            
        }     
    }

    global void execute(SchedulableContext sc) 
    {
        Integer maxRecordForChuck = 100;        

        IndividualsByContactsBatch b = new IndividualsByContactsBatch(); 
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,maxRecordForChuck);        
    }
}