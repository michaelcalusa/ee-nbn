/*------------------------------------------------------------
Author:         Asif A Khan
Company:        NBN
Description:    Reads the data loaded from Lead
                Can retrieve 50,000,000 record but execute method will only process
                max 2000 record at a time if max limit not set than 200 records at at time                
                Using Database.Stateful to maintain the list of all records return by Database.getQueryLocator
                
Test Class:     
History
<Date>          <Authors Name>      <Brief Description of Change>
Dec 12 2018      Asif A Khan            Created
------------------------------------------------------------*/
global class IndividualsByLeadBatch implements Database.Batchable<sobject>, Database.Stateful, Database.AllowsCallouts,Schedulable 
{
    
    public list<Lead> leadList;    

    global IndividualsByLeadBatch() 
    {        
        leadList = new list<Lead>();        
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Integer maxRecordForProcess = 5000000;        
        
        String strQuery = 'SELECT Id,Salutation, FirstName,RecordTypeId,LastName,Email,NBN_Products__c,nbn_updates__c,telesales__c,Phone,HasOptedOutOfEmail,ICT_nbn_Business_News__c,ICT_nbn_Business_Products_Services__c,ICT_Channel_Program_Updates__c, IndividualId FROM Lead WHERE IndividualId = null AND Email != null AND IsConverted = false AND (RecordType.Name = \'Business End Customer\' OR RecordType.Name = \'Business Segment ICT\') ORDER BY LastModifiedDate DESC limit '+maxRecordForProcess;
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<Lead> scope)
    {       
        list<Lead> response = new list<Lead>();
        string childBatchId = BC.getChildJobId();
        string parentBatchId = BC.getJobId();

        System.debug('Scope Size ---->>>>'+scope.size());
        try
        {            
            response = IndividualUtilities_Bulk.SetIndividualsToLead(scope);

            if(response.size() > 0)
            {
                leadList.addAll(response);
                System.debug('Lead Synced Size ---->>>>'+response.size());
                update leadList;
            }            
        }
        catch(Exception ex)
        {
            System.debug('Exception in Class IndividualsByLeadBatch function Execute ---->>>>'+ex.getMessage()+'---'+ex.getStackTraceString());
            
        }               
    }

    global void finish(Database.BatchableContext BC)
    {        
        System.debug('Finishing IndividualsByLeadBatch Updated '+leadList.size()+' Lead records with IndividualId');
        try
        {
                      
        }
        catch(Exception ex)
        {
            System.debug('Exception in Class IndividualsByLeadBatch function finish ---->>>>'+ex.getMessage()+'---'+ex.getStackTraceString());
            
        }     
    }

    global void execute(SchedulableContext sc) 
    {
        Integer maxRecordForChuck = 1000;        

        IndividualsByLeadBatch b = new IndividualsByLeadBatch(); 
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,maxRecordForChuck);        
    }
}