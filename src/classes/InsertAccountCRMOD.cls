/***************************************************************************************************
    Class Name          : InsertAccountCRMOD
    Test Class          :
    Version             : 1.0 
    Created Date        : 14-Nov-2017
    Author              : Krishna Sai
    Description         : Class to Insert Account Records in CRMOD.
    Input Parameters    : NewDev Application Object

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 

****************************************************************************************************/ 
public class InsertAccountCRMOD{
    @future(callout=true)
    public static void InsertaccCRMOD(string newDevAppFormAccountId){
        try{
            
        NewDev_Application__c newDevAppFormAccount = [SELECT Id, OwnerId, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, 
                                              LastViewedDate, LastReferencedDate, ABN__c, ACN__c, Accept_T_C__c, Accounts_Payable_Email__c, Accounts_Payable_First_Name__c, 
                                              Accounts_Payable_Last_Name__c, Accounts_Payable_Phone__c, Accounts_Payable_same_as_Applicant__c, Applicant_ABN__c, 
                                              Applicant_Company__c, Applicant_Email__c, Applicant_First_Name__c, Applicant_Last_Name__c, Applicant_Password__c,
                                              Applicant_Phone__c, Applicant_Role__c, Application_Country__c, Application_Postcode__c, Application_Start_Time__c, Application_State__c,
                                              Application_Street_1__c, Application_Street_2__c, Status__c, Application_Suburb__c, Billing_Contact_same_as_Applicant__c, Billing_Country__c, 
                                              Billing_Email__c, Billing_First_Name__c, Billing_Last_Name__c, Billing_Phone__c, Accounts_Payable_Mobile_Number__c, Billing_State__c, 
                                              Billing_Street_1__c, Billing_Street_2__c, Billing_Suburb__c, Business_Name__c, Confirm_no_other_Telco__c,
                                              Council_Reference_Number__c, Development_Name__c, Development_Type__c, Documents_Uploaded__c, EFSCD__c, 
                                              Entity_Name__c, Entity_Type__c, Existing_LIC__c, First_Name__c, General_Comments__c, Last_Name__c, 
                                              Latitude__c, Lead_in_Conduit__c, Longitude__c, Number_of_Essential_Services__c, Number_of_MDUs__c, 
                                              Number_of_SDUs__c, Number_of_Stages__c, Pathways__c, Payment_Method__c, Pit_and_Pipe__c, Site_Type__c, 
                                              Total_number_of_Premises__c, Trustee_ABN__c, Trustee_Name__c, Application_Submit_Time__c, Customer_Type__c, 
                                              Location_ID__c, Site_Name__c, Technical_Assessment__c, Service_Delivery_Type__c, Reference_Number__c, Application_Name__c, 
                                              Application_Reference_Number__c, Billing_Postcode__c, Contract_Signatory_Additional_Phone__c, Contract_Signatory_Country__c,
                                              Contract_Signatory_Email__c, Contract_Signatory_First_Name__c, Contract_Signatory_Last_Name__c, Contract_Signatory_Phone__c,
                                              Contract_Signatory_Post_Code__c, Contract_Signatory_State__c, Contract_Signatory_Street_1__c, Contract_Signatory_Street_2__c,
                                              Contract_Signatory_Suburb__c, Development_Work_Type__c, What_are_you_Building__c, Applicant_Confirm_Email__c, 
                                              Application_Terms_and_Conditions__c, Billing_Mobile_Number__c, Building_Type__c, Essential_Service__c, 
                                              Received_nbn_conditional_approval__c, Total_number_of_commercial_premises__c, Total_number_of_residential_premises__c, 
                                              Will_this_outbuilding_have_a_new_address__c, worked_with_nbn__c, Accounts_Payable_Confirm_Email__c, Any_External_Utility_Work_Being_Planned__c, 
                                              Applicant_additional_phone__c, Are_You_Producing_Development_Lots__c, Are_you_building_new_road_infrastructure__c, Billing_Confirm_Email__c, 
                                              CRMoD_AP_Contact_ID__c, CRMoD_Account_ID__c, CRMoD_Applicant_Contact_ID__c, CRMoD_Billing_Contact_ID__c, CRMoD_Contract_Signatory_Contact_ID__c, 
                                              CRMoD_Development_ID__c, CRMoD_Stage_ID__c, Cancelled_phone_and_internet__c, Contract_Signatory_Same_As_Applicant__c, Cost_For_Premises__c, 
                                              Cost_for_Essential_Services__c, Customer_Class__c, Date_of_Completion_of_PnP_Or_Pathways__c, Do_You_Have_an_ABN__c, Document_Type__c, Dwelling_Type__c, 
                                              Estimated_Occupancy_Date__c, Estimated_Trench_Start_Date__c, Fixed_Line_Boundary__c, Is_Pit_and_Pipe_Private__c, Is_Property_Owner__c, 
                                              LIC_Installed__c, LIC_Type__c, Number_Of_Premises_Already_Developed__c, Old_application_reference_number__c, Pit_And_Pipe_Or_Pathways_Developed__c, 
                                              Premises_To_Be_Serviced_By_NBN__c, Property_Owner_Email__c, Property_Owner_First_name__c, Property_Owner_Last_name__c, Property_Owner_Phone__c, 
                                              Property_Owner_mobile__c, RFS_Date__c, Real_Property_Description__c, Rejection_reason__c, Relevant_Type_Of_Works__c, SF_AP_Contact_ID__c, 
                                              SF_Account_ID__c, SF_Applicant_Contact_ID__c, SF_Billing_Contact_ID__c, SF_Contract_Signatory_Contact_ID__c, SF_Development_ID__c, 
                                              SF_Stage_ID__c, Service_Status__c, Small_Development_Developer_T_C__c, Stage_Name__c, Technology_Planned__c, Total_Cost_for_Premises__c, 
                                              Total_Number_of_Lots__c,TotalNumberOfPremises__c,Total_Number_Of_Premises_In_CurrentStage__c,Current_Stage__c,samId__c,fsaId__c FROM NewDev_Application__c where Id =: newDevAppFormAccountId];

            
        Boolean isAccountAvailable = False;
        string ABN,AccountName,ACN,RegisteredEntityName,TrusteeName,TrusteeACN,EntityType,Address1,Address2,City,State,Country,AccConcat,TrustConcat;
        string PostCode;
        string jsessionId;
	    string AccountToken;
        string serviceEndpoint;
        string crmondemandId;
      	string Contact;
      	string Accountstr;
      	string A1;
      	Integer Acc1;
      	Integer Acc2;
      	String dte,accData;
      	Integer num = 0;
      	Integer session =0;
      	Integer invalidCode =0;
      	Long sleepTime;
      	String accTypes;
      	Boolean invalid =false, invalidScode = false;
        Map<String, String> entityTypeMap = new Map<String, String>();
            
        for (NewDev_Billing_Entity_Mapping__mdt ndBE: [select Class__c,id,Entity__c,Entity_Code__c,CRMOD_Entity_Type__c  from NewDev_Billing_Entity_Mapping__mdt])
        {
            entityTypeMap.put(ndBE.Entity_Code__c, ndBE.CRMOD_Entity_Type__c);
        }
         
         system.debug('newDevAppFormAccount ==>'+newDevAppFormAccount.Business_Name__c);   
        //storing the data in variables 
       if(string.isNotBlank(newDevAppFormAccount.ABN__c))
        {
            if(string.isNotBlank(newDevAppFormAccount.Trustee_Name__c))
            {
                AccountName = newDevAppFormAccount.Trustee_Name__c+' '+'ATF'+' '+newDevAppFormAccount.Business_Name__c;
            }
            else
            {
                AccountName = newDevAppFormAccount.Business_Name__c;
            }
            
            EntityType =  entityTypeMap.get(newDevAppFormAccount.Entity_Type__c);
            ABN=newDevAppFormAccount.ABN__c;
        }
        else
        {
            AccountName = newDevAppFormAccount.Billing_First_Name__c+ ' '+newDevAppFormAccount.Billing_Last_Name__c ;
            EntityType =  'Person';
            ABN='00000000000';
        }
        ACN = newDevAppFormAccount.ACN__c ;
        RegisteredEntityName= newDevAppFormAccount.Entity_Name__c;
        TrusteeName = newDevAppFormAccount.Trustee_Name__c;
        TrusteeACN =  newDevAppFormAccount.Trustee_ABN__c;
        
        Address1 =  newDevAppFormAccount.Application_Street_1__c;
        Address2 =  newDevAppFormAccount.Application_Street_2__c;
        City = newDevAppFormAccount.Application_Suburb__c;
        State = newDevAppFormAccount.Application_State__c;
        PostCode = newDevAppFormAccount.Application_Postcode__c;
        Country = newDevAppFormAccount.Application_Country__c;
        list<Account> Acclist = [SELECT On_Demand_Id__c,ABN__c,Name from Account
                                where Name =:AccountName
                                AND ABN__c =:ABN
                                 AND On_Demand_Id__c != null LIMIT 1];
         
            if ( AccountName != null && AccountName.length()>=100)
         {
           AccConcat   =    AccountName.substring(0,99);
         }
            else {
                AccConcat = AccountName;
            }
            
          if ( TrusteeName != null && TrusteeName.length()>=40)
         {
           TrustConcat   =    TrusteeName.substring(0,39);
         }
            else {
                TrusteeName = TrustConcat;
            }
            
                       
        // If the Account is not existing in Salesforce insert in CRMOD
        if(Acclist.isEmpty()){
             String AccJSONString = '{'+
                        '\"Accounts\": '+'[ {'+
                        '\"AccountName\": \"'+ (AccConcat == null ?'':AccConcat) + '\"'+ ',' +
                          '\"CustomText1\": \"'+ (RegisteredEntityName== null ?'':RegisteredEntityName) + '\"'+ ',' +
                          '\"CustomText39\": \"'+ (TrustConcat == null ?'':TrustConcat) + '\"'+ ',' +
                           '\"CustomText40\": \"'+ (TrusteeACN == null ?'':TrusteeACN) + '\"'+ ',' +
                            '\"CustomText30\": \"'+ (ABN == null ?'': ABN) + '\"'+ ',' +
                              '\"CustomText31\": \"'+ (ACN == null ?'' : ACN) + '\"'+ ',' +
                               '\"CustomMultiSelectPickList1\": \"Developer\"' +',' +
                              '\"CustomPickList2\": \"'+ (EntityType == null ?'' :EntityType) + '\"'+ ',' +  
                               '\"PrimaryShipToStreetAddress\": \"'+ (Address1 == null ?'' :Address1) + '\"'+ ',' +
                                '\"PrimaryShipToStreetAddress2\": \"'+ (Address2 == null ?'' :Address2) + '\"'+ ',' +
                                 '\"PrimaryShipToCity\": \"'+ (City == null ?'' :City) + '\"'+ ',' +
                                  '\"PrimaryShipToProvince\": \"'+ (State == null ?'' :State) + '\"'+ ',' +
                                   '\"PrimaryShipToPostalCode\": \"'+ (PostCode == null ?'' :PostCode) + '\"'+ ',' +
                                   '\"PrimaryShipToCounty\": \"'+ (Country == null ?'' :Country) + '\"'+ 
                         '}]'+
                     '}';
            System.debug('JSON format: ' + AccJSONString);
            
            // Process POST request
            
                  Http http = new Http();
      			  HttpRequest req;
                  HttpResponse AccountRes;
                  req = new HttpRequest();
                  req.setMethod('POST');
                  req.setTimeout(120000);
                  req.setHeader('content-type', 'application/vnd.oracle.adf.resource+json');
      			  jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                  req.setHeader('Cookie', jsessionId);
                  req.setBody(AccJSONString);
                  req.setEndpoint('callout:Insert_CRMOD_Account');
                  AccountRes = http.send(req);
             	  System.debug(AccountRes.getStatus());
                  System.debug(AccountRes.getStatusCode());
                  System.debug(AccountRes.getBody());              
                if(AccountRes.getStatusCode()!=500)
                {
                    JSONParser parser;
                    parser = JSON.createParser(AccountRes.getBody());            	
                    system.debug('nextToken ==>' + parser.nextToken());
                    while (parser.nextToken() != null) {
                        if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'href')) {
                            parser.nextToken();
                            if(parser.getCurrentToken() == JSONToken.VALUE_STRING){
                                AccountToken = parser.getText();
                                A1 = AccountToken;
                                Acc1 = A1.lastIndexOf('/');
                                Acc2 = A1.length();
                                Accountstr = AccountToken.substring(Acc1+1,Acc2);
                                system.debug('Account ==>' + Accountstr);  
                            }
                            break;
                        } 
                    }
                    
                }
                else
                {
                    //GlobalUtility.logMessage('Error', 'InsertAccountCRMOD', 'InsertaccCRMOD', ' ', '', '', '', 't', 0);
                    Application_Log__c error = new Application_Log__c(Source__c='InsertAccountCRMOD',Source_Function__c='InsertaccCRMOD',Message__c= AccountRes.getBody(),Stack_Trace__c=newDevAppFormAccount.Id);
                    insert error;
                }
                
                             
      //Parsing the Json Response to get AccountId from JSON
      


        }
            else {
                 
                
                //Account existingaccount = Acclist.get(0);  
                Accountstr = Acclist.get(0).On_Demand_Id__c;
                system.debug('Account ==>' + Accountstr);  
                isAccountAvailable= true;
                }
            
      if(Accountstr!=null)
      {
          InsertContactCRMOD.InsertContactsCRMOD(newDevAppFormAccount,Accountstr);
      }
            
               
     }
        
        catch(Exception ex)
        {
            GlobalUtility.logMessage('Error', 'InsertAccountCRMOD', 'InsertaccCRMOD', ' ', '', '', '', ex, 0);
        }
       
      
    }
 }