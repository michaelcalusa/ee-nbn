@isTest
private class InsertAccountCRMOD_Test {
    
/***************************************************************************************************
    Class Name          : InsertAccountCRMOD_Test
    Test Class          :
    Version             : 1.0 
    Created Date        : 6 - feb - 2018
    Author              : Dilip Athley	
    Description         : Test class for InsertAccountCRMOD
    Input Parameters    : 

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 

****************************************************************************************************/ 
    

	static testMethod void InsertAccount_SuccessScenario() {
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        NewDev_Application__c nda1 = new NewDev_Application__c();
        nda1 = TestDataUtility.createNewDevApp();
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        string body = '{"Accounts":[{"CustomMultiSelectPickList1":"Developer","PrimaryShipToProvince":"","CustomText1":"NewDevelopmentTestEntityName","CustomText40":"","CustomText31":"","PrimaryShipToPostalCode":"","CustomText30":"33051775556","CustomPickList2":"","PrimaryShipToCounty":"Australia","PrimaryShipToStreetAddress":"","CustomText39":"","AccountName":"NewDevelopmentBusinessName","PrimaryShipToCity":"","PrimaryShipToStreetAddress2":"","links":{"self":{"rel":"self","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU"},"canonical":{"rel":"canonical","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU"},"AccountAddresses":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/AccountAddresses"},"AccountBooks":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/AccountBooks"},"AccountContactRoles":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/AccountContactRoles"},"AccountContacts":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/AccountContacts"},"AccountNotes":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/AccountNotes"},"AccountTeams":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/AccountTeams"},"Accounts":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/Accounts"},"Activities":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/Activities"},"Assets":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/Assets"},"BusinessPlans":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/BusinessPlans"},"CustomObjects13":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/CustomObjects13"},"CustomObjects14":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/CustomObjects14"},"CustomObjects15":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/CustomObjects15"},"CustomObjects7":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/CustomObjects7"},"CustomObjects9":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/CustomObjects9"},"Objectives":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/Objectives"},"Opportunities":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/Opportunities"},"PlanAccounts":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/PlanAccounts"},"Policies":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/Policies"},"ServiceRequests":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/ServiceRequests"}}}]}';
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
       	QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        InsertAccountCRMOD.InsertaccCRMOD(nda1.Id);
        Test.stopTest(); 
    }
    
    static testMethod void InsertAccount_ExceptionScenario() {
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        NewDev_Application__c nda1 = new NewDev_Application__c();
        nda1 = TestDataUtility.createNewDevApp();
        // Test the functionality
        Test.startTest();
        integer statusCode = 500;
        string body = '{"Accounts":[{"CustomMultiSelectPickList1":"Developer","PrimaryShipToProvince":"","CustomText1":"NewDevelopmentTestEntityName","CustomText40":"","CustomText31":"","PrimaryShipToPostalCode":"","CustomText30":"33051775556","CustomPickList2":"","PrimaryShipToCounty":"Australia","PrimaryShipToStreetAddress":"","CustomText39":"","AccountName":"NewDevelopmentBusinessName","PrimaryShipToCity":"","PrimaryShipToStreetAddress2":"","links":{"self":{"rel":"self","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU"},"canonical":{"rel":"canonical","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU"},"AccountAddresses":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/AccountAddresses"},"AccountBooks":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/AccountBooks"},"AccountContactRoles":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/AccountContactRoles"},"AccountContacts":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/AccountContacts"},"AccountNotes":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/AccountNotes"},"AccountTeams":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/AccountTeams"},"Accounts":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/Accounts"},"Activities":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/Activities"},"Assets":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/Assets"},"BusinessPlans":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/BusinessPlans"},"CustomObjects13":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/CustomObjects13"},"CustomObjects14":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/CustomObjects14"},"CustomObjects15":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/CustomObjects15"},"CustomObjects7":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/CustomObjects7"},"CustomObjects9":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/CustomObjects9"},"Objectives":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/Objectives"},"Opportunities":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/Opportunities"},"PlanAccounts":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/PlanAccounts"},"Policies":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/Policies"},"ServiceRequests":{"rel":"child","href":"/OnDemand/user/Rest/034/Accounts/AYSA-4M2YAU/child/ServiceRequests"}}}]}';
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
       	QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        InsertAccountCRMOD.InsertaccCRMOD(nda1.Id);
        Test.stopTest(); 
    }
}