/***************************************************************************************************
    Class Name          : InsertContactCRMOD
    Version             : 1.0 
    Created Date        : 14-Nov-2017
    Author              : Krishna Sai
    Description         : Class to Insert Contact Records in CRMOD.
    Input Parameters    : NewDev Application Object

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 
	Naseer					10/20/2018		Included PORTAL UNIQUE CODE field on Contact in SOQL
****************************************************************************************************/ 
public class InsertContactCRMOD {
	public static void InsertContactsCRMOD(NewDev_Application__c newDevAppFormContact, string Accountstr){
  		try{
      		Boolean isContactAvailable = False,BillingContactsameAsApplicant = False,AccPayablesameAsApplicant = False,ApplicantsameasSignatory = False;
            string FirstName,LastName,ApplicantEmail,ApplicantPhone,Address1,Address2,City,State,Country;
            string jsessionId,BillingFirstName,BillingLastName,BillingEmail,BillingPhone,AccPayableFirstName,AccPayableLastName,AccPayableEmail,AccPayablePhone,billingConId,accConId;
            string ContractSignatoryFirstName,ContractSignatoryLastName,ContractSignatoryEmail,ContractSignatoryPhone,ContractSignatoryStreet1,ContractSignatoryStreet2,ContractSignatoryCity,ContractSignatoryState,ContractSignatoryCountry;
            string ContactToken,ApplicantFirstName,ApplicantLastName;
            string Contact1,ContractSignatoryConId,ApplicantStreet1,ApplicantStreet2,ApplicantCity,ApplicationPostCode,ApplicationCountry,ApplicantState;
            Integer Con1;
            Integer Con2;
            string Contactstr,ApplicantJSONString,ApplicantConId;
            string AcpayableMobile,BillingMobile,ConsignAddlPhone,AppAddnlPhone;
            Http http = new Http();
            HttpRequest req;
            HttpResponse ContactRes;
            JSONParser parser;
            String BillingContactJSONString,AccPayableContactJSONString,ContractSignatoryJSONString,PostCode,ContractSignatoryPostCode;
            list<Contact> ConSignatoryConList = new List<Contact>();
            list<Contact> billingConList = new List<Contact>();
            list<Contact> accConList = new List<Contact>();
            list<Contact> ApplicantConList = new List<Contact>();      
      		//storing the data in variables
            Address1 = newDevAppFormContact.Billing_Street_1__c;
            Address2 =  newDevAppFormContact.Billing_Street_2__c;
            City = newDevAppFormContact.Billing_Suburb__c;
            State = newDevAppFormContact.Billing_State__c;
            PostCode = newDevAppFormContact.Billing_Postcode__c;
            Country = newDevAppFormContact.Billing_Country__c;
            BillingContactsameAsApplicant = newDevAppFormContact.Billing_Contact_same_as_Applicant__c;
            AccPayablesameAsApplicant = newDevAppFormContact.Accounts_Payable_same_as_Applicant__c;
            ContractSignatoryFirstName = newDevAppFormContact.Contract_Signatory_First_Name__c;
            ContractSignatoryLastName = newDevAppFormContact.Contract_Signatory_Last_Name__c;
            ContractSignatoryEmail = newDevAppFormContact.Contract_Signatory_Email__c;
            ContractSignatoryPhone = newDevAppFormContact.Contract_Signatory_Phone__c;
            ContractSignatoryStreet1 = newDevAppFormContact.Contract_Signatory_Street_1__c;
            ContractSignatoryStreet2 = newDevAppFormContact.Contract_Signatory_Street_2__c;
            ContractSignatoryCity = newDevAppFormContact.Contract_Signatory_Suburb__c;
            ContractSignatoryState = newDevAppFormContact.Contract_Signatory_State__c;
            ContractSignatoryPostCode = newDevAppFormContact.Contract_Signatory_Post_Code__c;
            ContractSignatoryCountry = newDevAppFormContact.Contract_Signatory_Country__c;
            ApplicantStreet1 = newDevAppFormContact.Application_Street_1__c;
            ApplicantStreet2 = newDevAppFormContact.Application_Street_2__c;
            ApplicantCity = newDevAppFormContact.Application_Suburb__c;
            ApplicantState = newDevAppFormContact.Application_State__c;
            ApplicationPostCode = newDevAppFormContact.Application_Postcode__c;
            ApplicationCountry = newDevAppFormContact.Application_Country__c;
            ApplicantsameasSignatory = newDevAppFormContact.Contract_Signatory_Same_As_Applicant__c;
            ApplicantFirstName = newDevAppFormContact.Applicant_First_Name__c;
            ApplicantLastName = newDevAppFormContact.Applicant_Last_Name__c;
            ApplicantEmail = newDevAppFormContact.Applicant_Email__c;
            ApplicantPhone = newDevAppFormContact.Applicant_Phone__c;
            AccPayableFirstName = newDevAppFormContact.Accounts_Payable_First_Name__c;
            AccPayableLastName = newDevAppFormContact.Accounts_Payable_Last_Name__c;
            AccPayableEmail = newDevAppFormContact.Accounts_Payable_Email__c;
            AccPayablePhone = newDevAppFormContact.Accounts_Payable_Phone__c;
            BillingFirstName = newDevAppFormContact.Billing_First_Name__c;
            BillingLastName = newDevAppFormContact.Billing_Last_Name__c;
            BillingEmail = newDevAppFormContact.Billing_Email__c;
            BillingPhone = newDevAppFormContact.Billing_Phone__c;
            AcpayableMobile = newDevAppFormContact.Accounts_Payable_Mobile_Number__c;
            BillingMobile = newDevAppFormContact.Billing_Mobile_Number__c;
            ConsignAddlPhone = newDevAppFormContact.Contract_Signatory_Additional_Phone__c;
            AppAddnlPhone = newDevAppFormContact.Applicant_additional_phone__c;
      
            // Applicant Details
            if(ApplicantLastName!= null){
                ApplicantConList = [SELECT On_Demand_Id_P2P__c, Portal_Unique_Code__c, FirstName,LastName,Email from Contact
                where FirstName = :ApplicantFirstName
                AND LastName = :ApplicantLastName 
                AND Email = :ApplicantEmail
                AND On_Demand_Id_P2P__c != null];     
                if(ApplicantConList.isEmpty()){
                    ApplicantJSONString = '{'+
                        '\"Contacts\": '+'[ {'+
                        '\"ContactFirstName\": \"'+ (ApplicantFirstName == null ?'' :ApplicantFirstName) + '\"'+ ',' +
                        '\"ContactLastName\": \"'+ (ApplicantLastName == null ?'' :ApplicantLastName) + '\"'+ ',' +
                        '\"ContactEmail\": \"'+ (ApplicantEmail == null ?'' :ApplicantEmail) + '\"'+ ',' +
                        '\"WorkPhone\": \"'+ (ApplicantPhone == null ?'' :ApplicantPhone) + '\"'+ ',' +
                        '\"HomePhone\": \"'+ (AppAddnlPhone == null ?'' :AppAddnlPhone) + '\"'+ ',' +
                        '\"AlternateAddress1\": \"'+ (ApplicantStreet1 == null ?'' :ApplicantStreet1) + '\"'+ ',' +
                        '\"AlternateAddress2\": \"'+ (ApplicantStreet2 == null ?'' :ApplicantStreet2) + '\"'+ ',' +
                        '\"AlternateCity\": \"'+ (ApplicantCity == null ?'' :ApplicantCity) + '\"'+ ',' +
                        '\"AlternateProvince\": \"'+ (ApplicantState == null ?'' :ApplicantState) + '\"'+ ',' +
                        '\"AlternateZipCode\": \"'+ (ApplicationPostCode == null ?'' :ApplicationPostCode) + '\"'+ ',' +
                        '\"CustomMultiSelectPickList5\": \"Consultant/Applicant\"' +',' +
                        '\"AlternateCountry\": \"'+ (ApplicationCountry == null ?'' :ApplicationCountry) + '\"'+ 
                        '}]'+
                        '}'; 
                    // Process POST request
                    req = new HttpRequest();
                    req.setMethod('POST');
                    req.setTimeout(120000);
                    req.setHeader('content-type', 'application/vnd.oracle.adf.resource+json');
                    jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                    req.setHeader('Cookie', jsessionId);
                    req.setBody(ApplicantJSONString);
                    req.setEndpoint('callout:Insert_CRMOD_Contact');
                    ContactRes = http.send(req);
                    System.debug(ContactRes.getBody());
                    if(ContactRes.getStatusCode()!=500){
                        parser = JSON.createParser(ContactRes.getBody());
                        while (parser.nextToken() != null){
                            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'href')){
                                parser.nextToken();
                                if(parser.getCurrentToken() == JSONToken.VALUE_STRING){
                                    ContactToken = parser.getText();
                                    Contact1 = ContactToken;
                                    Con1 = Contact1.lastIndexOf('/');
                                    Con2 = Contact1.length();
                                    ApplicantConId = ContactToken.substring(Con1+1,Con2);
                                    system.debug('ContactId ==>' + ApplicantConId);  
                                }
                                break;
                            }
                        }
                    }
                    else{
                        Application_Log__c error = new Application_Log__c(Source__c='InsertContactCRMOD',Source_Function__c='InsertContactsCRMOD',Message__c= ContactRes.getBody(),Stack_Trace__c=newDevAppFormContact.Id);
                        insert error;
                    }
                }
                else{
                    //pick up the contact with PUC on it, else first one in the list
                    if (ApplicantConList.size() > 1){
                        for (Contact con : ApplicantConList){
                            if (con.Portal_Unique_Code__c <> '' && con.Portal_Unique_Code__c <> null){
                                ApplicantConId = con.On_Demand_Id_P2P__c;
                                break;
                            }
                        }
                    }
	                ApplicantConId = (ApplicantConId == '' || ApplicantConId == null) ? ApplicantConList.get(0).On_Demand_Id_P2P__c : ApplicantConId;
                } 
            }
            
      		if(!ApplicantsameasSignatory){
        		ConSignatoryConList = [SELECT On_Demand_Id_P2P__c,FirstName,LastName,Email from Contact
                          where FirstName = :ContractSignatoryFirstName
                          AND LastName = :ContractSignatoryLastName 
                          AND Email = :ContractSignatoryEmail
                          AND On_Demand_Id_P2P__c != null limit 1];
                if(ConSignatoryConList.isEmpty()){
                    ContractSignatoryJSONString = '{'+
                        '\"Contacts\": '+'[ {'+
                        '\"ContactFirstName\": \"'+ (ContractSignatoryFirstName == null ?'' :ContractSignatoryFirstName) + '\"'+ ',' +
                        '\"ContactLastName\": \"'+ (ContractSignatoryLastName == null ?'' :ContractSignatoryLastName) + '\"'+ ',' +
                        '\"ContactEmail\": \"'+ (ContractSignatoryEmail == null ?'' :ContractSignatoryEmail) + '\"'+ ',' +
                        '\"WorkPhone\": \"'+ (ContractSignatoryPhone == null ?'' :ContractSignatoryPhone) + '\"'+ ',' +
                        '\"HomePhone\": \"'+ (ConsignAddlPhone == null ?'' :ConsignAddlPhone) + '\"'+ ',' +
                        '\"AlternateAddress1\": \"'+ (ContractSignatoryStreet1 == null ?'' :ContractSignatoryStreet1) + '\"'+ ',' +
                        '\"AlternateAddress2\": \"'+ (ContractSignatoryStreet2 == null ?'' :ContractSignatoryStreet2) + '\"'+ ',' +
                        '\"AlternateCity\": \"'+ (ContractSignatoryCity == null ?'' :ContractSignatoryCity) + '\"'+ ',' +
                        '\"AlternateProvince\": \"'+ (ContractSignatoryState == null ?'' :ContractSignatoryState) + '\"'+ ',' +
                        '\"AlternateZipCode\": \"'+ (ContractSignatoryPostCode == null ?'' :ContractSignatoryPostCode) + '\"'+ ',' +
                        '\"CustomMultiSelectPickList5\": \"Developer\"' +',' +
                        '\"AlternateCountry\": \"'+ (ContractSignatoryCountry == null ?'' :ContractSignatoryCountry) + '\"'+ 
                        '}]'+
                        '}'; 
                    // Process POST request
                    req = new HttpRequest();
                    req.setMethod('POST');
                    req.setTimeout(120000);
                    req.setHeader('content-type', 'application/vnd.oracle.adf.resource+json');
                    jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                    req.setHeader('Cookie', jsessionId);
                    req.setBody(ContractSignatoryJSONString);
                    req.setEndpoint('callout:Insert_CRMOD_Contact');
                    ContactRes = http.send(req);
                    System.debug(ContactRes.getBody());
                    if(ContactRes.getStatusCode()!=500){
                        parser = JSON.createParser(ContactRes.getBody());
                        while (parser.nextToken() != null){
                            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'href')){
                                parser.nextToken();
                                if(parser.getCurrentToken() == JSONToken.VALUE_STRING){
                                    ContactToken = parser.getText();
                                    Contact1 = ContactToken;
                                    Con1 = Contact1.lastIndexOf('/');
                                    Con2 = Contact1.length();
                                    ContractSignatoryConId = ContactToken.substring(Con1+1,Con2);
                                    system.debug('ContactId ==>' + ContractSignatoryConId);  
                                }
                                break;
                            }
                        }
                    }
                    else{
                        Application_Log__c error = new Application_Log__c(Source__c='InsertContactCRMOD',Source_Function__c='InsertContactsCRMOD',Message__c= ContactRes.getBody(),Stack_Trace__c=newDevAppFormContact.Id);
                        insert error;
                    }
                }
                else{
                    ContractSignatoryConId = ConSignatoryConList.get(0).On_Demand_Id_P2P__c;
                }
            }
  
      
      		if(!BillingContactsameAsApplicant){                       
                billingConList = [SELECT On_Demand_Id_P2P__c,FirstName,LastName,Email from Contact
                                  where FirstName =:BillingFirstName
                                  AND LastName =:BillingLastName AND
                                  Email =:BillingEmail
                                  AND On_Demand_Id_P2P__c != null limit 1];
                if(billingConList.isEmpty()){
                    BillingContactJSONString = '{'+
                        '\"Contacts\": '+'[ {'+
                        '\"ContactFirstName\": \"'+ (BillingFirstName == null ?'' :BillingFirstName) + '\"'+ ',' +
                        '\"ContactLastName\": \"'+ (BillingLastName == null ?'' :BillingLastName) + '\"'+ ',' +
                        '\"ContactEmail\": \"'+ (BillingEmail == null ?'' :BillingEmail) + '\"'+ ',' +
                        '\"WorkPhone\": \"'+ (BillingPhone == null ?'' :BillingPhone) + '\"'+ ',' +
                        '\"CellularPhone\": \"'+ (BillingMobile == null ?'' :BillingMobile) + '\"'+ ',' +
                        '\"AlternateAddress1\": \"'+ (Address1 == null ?'' :Address1) + '\"'+ ',' +
                        '\"AlternateAddress2\": \"'+ (Address2 == null ?'' :Address2) + '\"'+ ',' +
                        '\"AlternateCity\": \"'+ (City == null ?'' :City) + '\"'+ ',' +
                        '\"AlternateProvince\": \"'+ (State == null ?'' :State) + '\"'+ ',' +
                        '\"AlternateZipCode\": \"'+ (PostCode == null ?'' :PostCode) + '\"'+ ',' +
                        '\"CustomMultiSelectPickList5\": \"Consultant/Applicant\"' +',' +
                        '\"AlternateCountry\": \"'+ (Country == null ?'' :Country) + '\"'+ 
                        '}]'+
                        '}'; 
                    // Process POST request
                    req = new HttpRequest();
                    req.setMethod('POST');
                    req.setTimeout(120000);
                    req.setHeader('content-type', 'application/vnd.oracle.adf.resource+json');
                    jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                    req.setHeader('Cookie', jsessionId);
                    req.setBody(BillingContactJSONString);
                    req.setEndpoint('callout:Insert_CRMOD_Contact');
                    ContactRes = http.send(req);
                    System.debug(ContactRes.getBody());
                    parser = JSON.createParser(ContactRes.getBody());
                    //system.debug('nextToken ==>' + parser.nextToken());
                    if(ContactRes.getStatusCode()!=500){
                        while (parser.nextToken() != null){
                            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'href')){
                                parser.nextToken();
                                if(parser.getCurrentToken() == JSONToken.VALUE_STRING){
                                    ContactToken = parser.getText();
                                    Contact1 = ContactToken;
                                    Con1 = Contact1.lastIndexOf('/');
                                    Con2 = Contact1.length();
                                    billingConId = ContactToken.substring(Con1+1,Con2);
                                    system.debug('ContactId ==>' + billingConId);  
                                }
                                break;
                            }
                        }
                    }
                    else{
                        //GlobalUtility.logMessage('Error', 'InsertAccountCRMOD', 'InsertaccCRMOD', ' ', '', '', '', 't', 0);
                        Application_Log__c error = new Application_Log__c(Source__c='InsertContactCRMOD',Source_Function__c='InsertContactsCRMOD',Message__c= ContactRes.getBody(),Stack_Trace__c=newDevAppFormContact.Id);
                        insert error;
                    }
                }
                else{
                    billingConId = billingConList.get(0).On_Demand_Id_P2P__c;
                }
            }
    
            if(!AccPayablesameAsApplicant){
                accConList = [SELECT On_Demand_Id_P2P__c,FirstName,LastName,Email from Contact
                              where FirstName =:AccPayableFirstName
                              AND LastName =:AccPayableLastName AND
                              Email =:AccPayableEmail
                              AND On_Demand_Id_P2P__c != null limit 1];   
                if(accConList.isEmpty()){
                    AccPayableContactJSONString = '{'+
                        '\"Contacts\": '+'[ {'+
                        '\"ContactFirstName\": \"'+ (AccPayableFirstName == null ?'' :AccPayableFirstName) + '\"'+ ',' +
                        '\"ContactLastName\": \"'+ (AccPayableLastName == null ?'' :AccPayableLastName) + '\"'+ ',' +
                        '\"ContactEmail\": \"'+ (AccPayableEmail == null ?'': AccPayableEmail) + '\"'+ ',' +
                        '\"CellularPhone\": \"'+ (AcpayableMobile == null ?'' :AcpayableMobile) + '\"'+ ',' +
                        '\"CustomMultiSelectPickList5\": \"Accounts Payable\"' +',' +
                        '\"WorkPhone\": \"'+ (AccPayablePhone == null ?'' :AccPayablePhone) + '\"'+ 
                        '}]'+
                        '}';
                    System.debug('Contact JSON format: ' + AccPayableContactJSONString);
                    // Process POST request
                    req = new HttpRequest();
                    req.setMethod('POST');
                    req.setTimeout(120000);
                    req.setHeader('content-type', 'application/vnd.oracle.adf.resource+json');
                    jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                    req.setHeader('Cookie', jsessionId);
                    req.setBody(AccPayableContactJSONString);
                    req.setEndpoint('callout:Insert_CRMOD_Contact');
                    ContactRes = http.send(req);
                    System.debug(ContactRes.getBody());
                    parser = JSON.createParser(ContactRes.getBody());
                    system.debug('nextToken ==>' + parser.nextToken());
                    if(ContactRes.getStatusCode()!=500){
                        while (parser.nextToken() != null){
                            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'href')){
                                parser.nextToken();
                                if(parser.getCurrentToken() == JSONToken.VALUE_STRING){
                                    ContactToken = parser.getText();
                                    Contact1 = ContactToken;
                                    Con1 = Contact1.lastIndexOf('/');
                                    Con2 = Contact1.length();
                                    accConId = ContactToken.substring(Con1+1,Con2);
                                    system.debug('ContactId ==>' + accConId);  
                                }
                                break;
                            }
                        } 
                    }
                    else{    
                        Application_Log__c error = new Application_Log__c(Source__c='InsertContactCRMOD',Source_Function__c='InsertContactsCRMOD',Message__c= ContactRes.getBody(),Stack_Trace__c=newDevAppFormContact.Id);
                        insert error;
                    }
                }
                else{
                    accConId = accConList.get(0).On_Demand_Id_P2P__c;
                }
            }
            
            if(Accountstr!=null){
                AccountContactRelCRMOD.InsertAccountContactRelCRMOD(newDevAppFormContact,billingConId,accConId,Accountstr,ContractSignatoryConId,ApplicantConId);
            }
        }
        catch(Exception ex){
            GlobalUtility.logMessage('Error', 'InsertContactCRMOD', 'InsertContactsCRMOD', ' ', '', '', '', ex, 0);
        }
    }     
}