@isTest
private class InsertContactCRMOD_Test {
    
/***************************************************************************************************
    Class Name          : InsertContactCRMOD_Test
    Test Class          :
    Version             : 1.0 
    Created Date        : 6 - feb - 2018
    Author              : Dilip Athley	
    Description         : Test class for InsertContactCRMOD
    Input Parameters    : 

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 

****************************************************************************************************/ 
    

	static testMethod void InsertContact_SuccessScenario() {
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        NewDev_Application__c nda1 = new NewDev_Application__c();
        nda1 = TestDataUtility.createNewDevApp();
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        string body = '{"Contacts":[{"ContactLastName":"applicant-LastName","ContactFirstName":"applicant-FirstName111","AlternateAddress2":"","WorkPhone":"(04) 2074 7871","AlternateCountry":"Australia","ContactEmail":"applicant@test.com","CustomMultiSelectPickList5":"Consultant/Applicant","AlternateCity":"","AlternateZipCode":"","AlternateAddress1":"","AlternateProvince":"","HomePhone":"","links":{"self":{"rel":"self","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1"},"canonical":{"rel":"canonical","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1"},"AccountContactRoles":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/AccountContactRoles"},"AccountContacts":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/AccountContacts"},"CampaignRecipients":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/CampaignRecipients"},"ContactBooks":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ContactBooks"},"ContactNotes":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ContactNotes"},"ContactTeams":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ContactTeams"},"FinancialAccounts":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/FinancialAccounts"},"PlanContacts":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/PlanContacts"},"ServiceRequests":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ServiceRequests"}}}]}';
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
       	QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        InsertContactCRMOD.InsertContactsCRMOD(nda1,'AYSA-12345');
        Test.stopTest(); 
    }
    
    static testMethod void InsertContact_ExceptionScenario() {
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        NewDev_Application__c nda1 = new NewDev_Application__c();
        nda1 = TestDataUtility.createNewDevApp();
        // Test the functionality
        Test.startTest();
        integer statusCode = 500;
        string body = '{"Contacts":[{"ContactLastName":"applicant-LastName","ContactFirstName":"applicant-FirstName111","AlternateAddress2":"","WorkPhone":"(04) 2074 7871","AlternateCountry":"Australia","ContactEmail":"applicant@test.com","CustomMultiSelectPickList5":"Consultant/Applicant","AlternateCity":"","AlternateZipCode":"","AlternateAddress1":"","AlternateProvince":"","HomePhone":"","links":{"self":{"rel":"self","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1"},"canonical":{"rel":"canonical","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1"},"AccountContactRoles":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/AccountContactRoles"},"AccountContacts":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/AccountContacts"},"CampaignRecipients":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/CampaignRecipients"},"ContactBooks":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ContactBooks"},"ContactNotes":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ContactNotes"},"ContactTeams":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ContactTeams"},"FinancialAccounts":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/FinancialAccounts"},"PlanContacts":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/PlanContacts"},"ServiceRequests":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ServiceRequests"}}}]}';
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
       	QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        InsertContactCRMOD.InsertContactsCRMOD(nda1,'AYSA-12345');
        Test.stopTest(); 
    }
    
     static testMethod void InsertContact_SuccessScenario2() {
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        NewDev_Application__c nda1 = new NewDev_Application__c();
        nda1 = TestDataUtility.createNewDevApp();
         
         nda1.Accounts_Payable_same_as_Applicant__c = false;
         nda1.Billing_Contact_same_as_Applicant__c = true;
         nda1.Contract_Signatory_Same_As_Applicant__c = true;
         nda1.What_are_you_Building__c = 'Lead-In Conduit';
         nda1.Site_Name__c = 'this is a 50 character testthis is a 50 character testthis is a 50 character testthis is a 50 character testthis is a 50 character test';
         nda1.worked_with_nbn__c = true;
         nda1.Any_External_Utility_Work_Being_Planned__c = true;
         nda1.Technology_Planned__c = 'FTTP';
         nda1.RFS_Date__c= system.today();
         nda1.Estimated_Trench_Start_Date__c = system.today();
         nda1.Number_of_Premises_In_Current_Stage__c = 6.0;
         nda1.Number_of_Non_Premises_In_Current_Stage__c = 6.0;
         nda1.Building_Type__c = 'Residential use only';
         nda1.Dwelling_Type__c = 'Single Dwelling Unit (SDU)';
         nda1.Technology_Planned__c = 'Fixed Wireless / Satellite';
         update nda1;
         nda1 = [SELECT Id, OwnerId, IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, 
                                              LastViewedDate, LastReferencedDate, ABN__c, ACN__c, Accept_T_C__c, Accounts_Payable_Email__c, Accounts_Payable_First_Name__c, 
                                              Accounts_Payable_Last_Name__c, Accounts_Payable_Phone__c, Accounts_Payable_same_as_Applicant__c, Applicant_ABN__c, 
                                              Applicant_Company__c, Applicant_Email__c, Applicant_First_Name__c, Applicant_Last_Name__c, Applicant_Password__c,
                                              Applicant_Phone__c, Applicant_Role__c, Application_Country__c, Application_Postcode__c, Application_Start_Time__c, Application_State__c,
                                              Application_Street_1__c, Application_Street_2__c, Status__c, Application_Suburb__c, Billing_Contact_same_as_Applicant__c, Billing_Country__c, 
                                              Billing_Email__c, Billing_First_Name__c, Billing_Last_Name__c, Billing_Phone__c, Accounts_Payable_Mobile_Number__c, Billing_State__c, 
                                              Billing_Street_1__c, Billing_Street_2__c, Billing_Suburb__c, Business_Name__c, Confirm_no_other_Telco__c,
                                              Council_Reference_Number__c, Development_Name__c, Development_Type__c, Documents_Uploaded__c, EFSCD__c, 
                                              Entity_Name__c, Entity_Type__c, Existing_LIC__c, First_Name__c, General_Comments__c, Last_Name__c, 
                                              Latitude__c, Lead_in_Conduit__c, Longitude__c, Number_of_Essential_Services__c, Number_of_MDUs__c, 
                                              Number_of_SDUs__c, Number_of_Stages__c, Pathways__c, Payment_Method__c, Pit_and_Pipe__c, Site_Type__c, 
                                              Total_number_of_Premises__c, Trustee_ABN__c, Trustee_Name__c, Application_Submit_Time__c, Customer_Type__c, 
                                              Location_ID__c, Site_Name__c, Technical_Assessment__c, Service_Delivery_Type__c, Reference_Number__c, Application_Name__c, 
                                              Application_Reference_Number__c, Billing_Postcode__c, Contract_Signatory_Additional_Phone__c, Contract_Signatory_Country__c,
                                              Contract_Signatory_Email__c, Contract_Signatory_First_Name__c, Contract_Signatory_Last_Name__c, Contract_Signatory_Phone__c,
                                              Contract_Signatory_Post_Code__c, Contract_Signatory_State__c, Contract_Signatory_Street_1__c, Contract_Signatory_Street_2__c,
                                              Contract_Signatory_Suburb__c, Development_Work_Type__c, What_are_you_Building__c, Applicant_Confirm_Email__c, 
                                              Application_Terms_and_Conditions__c, Billing_Mobile_Number__c, Building_Type__c, Essential_Service__c, 
                                              Received_nbn_conditional_approval__c, Total_number_of_commercial_premises__c, Total_number_of_residential_premises__c, 
                                              Will_this_outbuilding_have_a_new_address__c, worked_with_nbn__c, Accounts_Payable_Confirm_Email__c, Any_External_Utility_Work_Being_Planned__c, 
                                              Applicant_additional_phone__c, Are_You_Producing_Development_Lots__c, Are_you_building_new_road_infrastructure__c, Billing_Confirm_Email__c, 
                                              CRMoD_AP_Contact_ID__c, CRMoD_Account_ID__c, CRMoD_Applicant_Contact_ID__c, CRMoD_Billing_Contact_ID__c, CRMoD_Contract_Signatory_Contact_ID__c, 
                                              CRMoD_Development_ID__c, CRMoD_Stage_ID__c, Cancelled_phone_and_internet__c, Contract_Signatory_Same_As_Applicant__c, Cost_For_Premises__c, 
                                              Cost_for_Essential_Services__c, Customer_Class__c, Date_of_Completion_of_PnP_Or_Pathways__c, Do_You_Have_an_ABN__c, Document_Type__c, Dwelling_Type__c, 
                                              Estimated_Occupancy_Date__c, Estimated_Trench_Start_Date__c, Fixed_Line_Boundary__c, Is_Pit_and_Pipe_Private__c, Is_Property_Owner__c, 
                                              LIC_Installed__c, LIC_Type__c, Number_Of_Premises_Already_Developed__c, Old_application_reference_number__c, Pit_And_Pipe_Or_Pathways_Developed__c, 
                                              Premises_To_Be_Serviced_By_NBN__c, Property_Owner_Email__c, Property_Owner_First_name__c, Property_Owner_Last_name__c, Property_Owner_Phone__c, 
                                              Property_Owner_mobile__c, RFS_Date__c, Real_Property_Description__c, Rejection_reason__c, Relevant_Type_Of_Works__c, SF_AP_Contact_ID__c, 
                                              SF_Account_ID__c, SF_Applicant_Contact_ID__c, SF_Billing_Contact_ID__c, SF_Contract_Signatory_Contact_ID__c, SF_Development_ID__c, 
                                              SF_Stage_ID__c, Service_Status__c, Small_Development_Developer_T_C__c, Stage_Name__c, Technology_Planned__c, Total_Cost_for_Premises__c, 
                                              Total_Number_of_Lots__c,TotalNumberOfPremises__c,Total_Number_Of_Premises_In_CurrentStage__c,Current_Stage__c,samId__c,fsaId__c FROM NewDev_Application__c where Id =: nda1.Id];
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        string body = '{"Contacts":[{"ContactLastName":"applicant-LastName","ContactFirstName":"applicant-FirstName111","AlternateAddress2":"","WorkPhone":"(04) 2074 7871","AlternateCountry":"Australia","ContactEmail":"applicant@test.com","CustomMultiSelectPickList5":"Consultant/Applicant","AlternateCity":"","AlternateZipCode":"","AlternateAddress1":"","AlternateProvince":"","HomePhone":"","links":{"self":{"rel":"self","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1"},"canonical":{"rel":"canonical","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1"},"AccountContactRoles":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/AccountContactRoles"},"AccountContacts":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/AccountContacts"},"CampaignRecipients":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/CampaignRecipients"},"ContactBooks":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ContactBooks"},"ContactNotes":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ContactNotes"},"ContactTeams":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ContactTeams"},"FinancialAccounts":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/FinancialAccounts"},"PlanContacts":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/PlanContacts"},"ServiceRequests":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ServiceRequests"}}}]}';
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
       	QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        InsertContactCRMOD.InsertContactsCRMOD(nda1,'AYSA-12345');
        Test.stopTest(); 
    }
}