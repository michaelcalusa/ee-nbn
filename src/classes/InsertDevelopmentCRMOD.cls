/***************************************************************************************************
Class Name          : InsertDevelopmentCRMOD
Version             : 1.0 
Created Date        : 14-Nov-2017
Author              : Krishna Sai
Description         : Class to Insert Development Records in CRMOD.
Input Parameters    : NewDev Application Object

Modification Log    :
* Developer             Date            Description
* ----------------------------------------------------------------------------                 

****************************************************************************************************/
public class InsertDevelopmentCRMOD {
    public static void InsertDevCRMOD(NewDev_Application__c newDevAppFormDevelopment,String billingConId,string Accountstr,string accConId,string ContractSignatoryConId, string ApplicantConId){
        try{
            String DevelopmentName,DevelopmentType,CouncilReferenceNo,jsessionId,DevelopmentToken,D1,Developmentstr,serviceEndpoint,ApplicationStreet1,ApplicationStreet2,ApplicationSuburb,ApplicationState;
            decimal NoOfStages,TotalNoOfPremises,NoofResidentialPremises,NoofPreCoveredbyAgreement,NoofPtremisesConnected,NoofNonResiPremises;
            date EFSCD;
            boolean AcceptTC,ConfirmNoOtherTelco,DocumentsUploaded,ExistingLIC,PreviouslybuiltwithNbn,UtilityWorks,essentialService;
            integer Dev1,Dev2;
            String DevelopmentJsonString,ApplicationPostCode,techPlanned,whatareyoubuilding,BuildingType,DwellingType;
            String EFSCDModified,RealPropertyDesc,PreviouslyBuiltstr,utilityworksstr,techplannedstr,Latitude,Longitude,Sam,Fsa,Address,SmallDevJsonString;
            
            //DevelopmentName = newDevAppFormDevelopment.Development_Name__c;
            
            DevelopmentName = newDevAppFormDevelopment.Site_Name__c;
            if(DevelopmentName.length()>50) {
                DevelopmentName = DevelopmentName.substring(0, 49);                   
            }         
            NoOfStages = newDevAppFormDevelopment.Number_of_Stages__c;
            TotalNoOfPremises = newDevAppFormDevelopment.TotalNumberOfPremises__c;
            DevelopmentType = newDevAppFormDevelopment.Development_Type__c;
            CouncilReferenceNo = newDevAppFormDevelopment.Council_Reference_Number__c;
            AcceptTC = newDevAppFormDevelopment.Accept_T_C__c;
            ConfirmNoOtherTelco = newDevAppFormDevelopment.Confirm_no_other_Telco__c;
            DocumentsUploaded = newDevAppFormDevelopment.Documents_Uploaded__c;
            ExistingLIC = newDevAppFormDevelopment.Existing_LIC__c;
            ApplicationStreet1 = newDevAppFormDevelopment.Application_Street_1__c;
            ApplicationStreet2 = newDevAppFormDevelopment.Application_Street_2__c;
            ApplicationSuburb = newDevAppFormDevelopment.Application_Suburb__c;
            ApplicationState = newDevAppFormDevelopment.Application_State__c;
            ApplicationPostCode = newDevAppFormDevelopment.Application_Postcode__c;
            RealPropertyDesc = newDevAppFormDevelopment.Real_Property_Description__c;
            PreviouslybuiltwithNbn = newDevAppFormDevelopment.worked_with_nbn__c;
            NoofResidentialPremises = newDevAppFormDevelopment.Total_number_of_residential_premises__c;
            NoofPreCoveredbyAgreement = newDevAppFormDevelopment.Total_Number_Of_Premises_In_CurrentStage__c;
            techPlanned = newDevAppFormDevelopment.Technology_Planned__c ;     
            NoofPtremisesConnected = newDevAppFormDevelopment.Number_Of_Premises_Already_Developed__c;
            UtilityWorks = newDevAppFormDevelopment.Any_External_Utility_Work_Being_Planned__c;
            whatareyoubuilding = newDevAppFormDevelopment.What_are_you_Building__c;
            EFSCD = newDevAppFormDevelopment.Estimated_Occupancy_Date__c;
            NoofNonResiPremises = newDevAppFormDevelopment.Total_number_of_commercial_premises__c;
            Latitude = newDevAppFormDevelopment.Latitude__c;
            Longitude = newDevAppFormDevelopment.Longitude__c;
            Sam = newDevAppFormDevelopment.samId__c;
            Fsa = newDevAppFormDevelopment.fsaId__c;  
            essentialService = newDevAppFormDevelopment.Essential_Service__c;
            if(newDevAppFormDevelopment.Building_Type__c != null){
                if(newDevAppFormDevelopment.Building_Type__c == 'Residential use only'){
                    BuildingType = 'Residential';  
                    NoofResidentialPremises = TotalNoOfPremises;
                }
                else if(newDevAppFormDevelopment.Building_Type__c == 'Commercial use only'){  
                    BuildingType = 'Commercial';
                    NoofNonResiPremises = TotalNoOfPremises;
                }
                else if(newDevAppFormDevelopment.Building_Type__c == 'Mixed residential and commercial use'){  
                    BuildingType = 'Mixed Use';
                }
                else{  
                    BuildingType = 'Other';
                }
            }
            if(newDevAppFormDevelopment.Dwelling_Type__c != null){
                DwellingType = newDevAppFormDevelopment.Dwelling_Type__c == 'Single Dwelling Unit (SDU)' ? 'SDU' : 'MDU';
            }
            String datestr = efscd.format();
            Integer iYear= EFSCD.year();
            Integer iMonth= EFSCD.month();
            Integer iDate= EFSCD.day();
            String strDay;
            String strMonth;
            if(iDate<10){
                strDay = '0' + iDate;
            }
            else{
                strDay =string.valueOf(iDate);
            }
            if(iMonth<10){
                strMonth = '0' + string.valueOf(iMonth);
            }
            else{
                strMonth = string.valueOf(iMonth);
            }
            datestr       = iYear + '-' + strMonth + '-' + strDay;
            
            
            if(PreviouslybuiltwithNbn)
            {
                PreviouslyBuiltstr = 'Y';
            }
            else{
                PreviouslyBuiltstr = 'N';
            }
            
            if(UtilityWorks)
            {
                utilityworksstr = 'Y';
            }
            else{
                utilityworksstr = 'N';
            }
            if(techPlanned!= null)
            {
                if(techPlanned.toLowerCase().contains('wireless') || techPlanned.toLowerCase().contains('satellite')){
                    techplannedstr = 'Fixed Wireless / Satellite';              
                }
                else if(techPlanned == 'FTTP')
                {
                    techplannedstr = 'FTTP – Type 3 (MT-LFN)';
                }
                else{
                    techplannedstr = techPlanned;
                }
                if(techplannedstr == 'FTTN' || techplannedstr == 'FTTP – Type 2' || techplannedstr == 'FTTP – Type 3 (MT-LFN)' || techplannedstr == 'FTTC' || 
                   techplannedstr == 'FTTB'|| techplannedstr == 'HFC'|| techplannedstr == 'Fixed Wireless / Satellite'){
                       techplannedstr = techplannedstr;                
                   }
            }
            else{
                techplannedstr = '';
            }
            
            if (ApplicationStreet2!= null){
                Address = ApplicationStreet1 + ' ' + ApplicationStreet2;
            }
            else{
                
                Address = ApplicationStreet1;
            }
            
            if (ApplicationStreet1 == null){
                
                Address = ApplicationStreet2;
            }
            if (Sam!= null)
            {
                if ( Sam.contains('|')){
                    Sam = Sam.substringBefore('|');
                    
                }
            }
            system.debug('NoofPreCoveredbyAgreement' +NoofPreCoveredbyAgreement);
            if(NoofPreCoveredbyAgreement>3 || essentialService)
            {                                        
                DevelopmentJsonString = '{'+
                    '\"CustomObjects2\": '+'[ {'+
                    '\"Name\": \"'+ (DevelopmentName == null ?'' :DevelopmentName) + '\"'+ ',' +
                    '\"CustomInteger1\": \"'+ (NoOfStages == null ? 0 :NoOfStages) + '\"'+ ',' +
                    '\"CustomInteger2\": \"'+ (TotalNoOfPremises == null ? 0 :TotalNoOfPremises) + '\"'+ ',' +
                    '\"Type\": \"Medium/Large\"'+ ',' +
                    '\"CustomText12\": \"'+ (CouncilReferenceNo == null ?'' :CouncilReferenceNo) + '\"'+ ',' +
                    '\"AccountId\": \"'+ (Accountstr == null ?'' :Accountstr) + '\"'+ ',' +
                    '\"IndexedPick1\": \"New\"'+ ',' +
                    '\"IndexedPick0\": \"Not Started\"'+ ',' +
                    '\"CustomText0\": \"'+ (Address == null ?'' :Address) + '\"'+ ',' +
                    '\"CustomText34\": \"'+ (ApplicationSuburb == null ?'' :ApplicationSuburb)  + '\"'+ ',' +
                    '\"CustomPickList8\":\"'+ (ApplicationState == null ?'' :ApplicationState) + '\"'+ ',' +
                    '\"CustomText4\":\"'+ (RealPropertyDesc == null ?'' :RealPropertyDesc) + '\"'+ ',' +
                    '\"CustomText36\":\"'+ (Latitude == null ?'' :Latitude) + '\"'+ ',' +
                    '\"CustomText32\":\"'+ (Longitude == null ?'' :Longitude) + '\"'+ ',' +
                    '\"CustomBoolean14\":\"'+ PreviouslyBuiltstr + '\"'+ ',' +
                    '\"CustomInteger9\":\"'+ (NoofResidentialPremises == null ? 0 :NoofResidentialPremises) + '\"'+ ',' +
                    '\"CustomInteger6\":\"'+ (NoofPreCoveredbyAgreement == null ? 0 :NoofPreCoveredbyAgreement) + '\"'+ ',' +
                    '\"CustomInteger0\":\"'+ (NoofPtremisesConnected == null ? 0 :NoofPtremisesConnected) + '\"'+ ',' +
                    '\"CustomBoolean4\":\"'+ utilityworksstr + '\"'+ ',' +
                    '\"CustomPickList32\":\"'+ (techplannedstr == null ?'' :techplannedstr) + '\"'+ ',' +
                    '\"CustomText46\":\"'+ (techplannedstr == null ?'' :techplannedstr) + '\"'+ ',' +
                    '\"CustomInteger11\":\"'+ (NoofNonResiPremises == null ? 0 :NoofNonResiPremises) + '\"'+ ',' +
                    '\"CustomObject4Name\":\"'+ (Sam == null ? '' :Sam) + '\"'+ ',' +
                    '\"CustomObject1Name\":\"'+ (Fsa == null ? '' :Fsa) + '\"'+ ',' +
                    '\"CustomPickList26\":\"'+ (DwellingType == null ? '' :DwellingType) + '\"'+ ',' +
                    '\"CustomText37\":\"'+ (ApplicationPostCode == null ?'' :ApplicationPostCode) + '\"'+                                 
                    
                    '}]'+
                    '}';
                System.debug('Development JSON format: ' + DevelopmentJsonString);
                
                // Process POST request
                
                Http http = new Http();
                HttpRequest req;
                HttpResponse DevelopmentRes;
                req = new HttpRequest();
                req.setMethod('POST');
                req.setTimeout(120000);
                req.setHeader('content-type', 'application/vnd.oracle.adf.resource+json');
                jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                req.setHeader('Cookie', jsessionId);
                req.setBody(DevelopmentJsonString);
                req.setEndpoint('callout:Insert_CRMOD_Development');
                DevelopmentRes = http.send(req);
                System.debug(DevelopmentRes.getBody());   
                
                
                //Parsing the Json Response
                if(DevelopmentRes.getStatusCode()!=500)
                {
                    JSONParser parser;
                    parser = JSON.createParser(DevelopmentRes.getBody());
                    system.debug('nextToken ==>' + parser.nextToken());
                    while (parser.nextToken() != null) {
                        if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'href')) {
                            parser.nextToken();
                            if(parser.getCurrentToken() == JSONToken.VALUE_STRING){
                                DevelopmentToken = parser.getText();
                                D1 = DevelopmentToken;
                                Dev1 = D1.lastIndexOf('/');
                                System.debug('Dev1 ' + Dev1);
                                Dev2 = D1.length();
                                System.debug('Dev2 ' + Dev2);
                                Developmentstr = DevelopmentToken.substring(Dev1+1,Dev2);
                                system.debug('Development ==>' + Developmentstr);  
                                
                            }
                            break;
                        }
                    }
                }
                
                else
                {
                    Application_Log__c error = new Application_Log__c(Source__c='InsertDevelopmentCRMOD',Source_Function__c='InsertDevCRMOD',Message__c= DevelopmentRes.getBody(),Stack_Trace__c=newDevAppFormDevelopment.Id);
                    insert error;
                }
                
                
                if(string.isNotBlank(billingConId) || string.isNotBlank(accConId) || string.isNotBlank(ContractSignatoryConId) || string.isNotBlank(ApplicantConId) )
                {
                    InsertDevelopmentContactCRMOD.sendRequest(Developmentstr, billingConId, accConId,ContractSignatoryConId,ApplicantConId);
                    
                }
            }
            else{
                SmallDevJsonString = '{'+
                    '\"CustomObjects2\": '+'[ {'+
                    '\"Name\": \"'+ (DevelopmentName == null ?'' :DevelopmentName) + '\"'+ ',' +
                    '\"Type\": \"Small\"'+ ',' +
                    '\"CustomText12\": \"'+ (CouncilReferenceNo == null ?'' :CouncilReferenceNo) + '\"'+ ',' +
                    '\"AccountId\": \"'+ (Accountstr == null ?'' :Accountstr) + '\"'+ ',' +
                    '\"IndexedPick2\": \"New\"'+ ',' +
                    '\"CustomText0\": \"'+ (Address == null ?'' :Address) + '\"'+ ',' +
                    '\"CustomText34\": \"'+ (ApplicationSuburb == null ?'' :ApplicationSuburb)  + '\"'+ ',' +
                    '\"CustomPickList8\":\"'+ (ApplicationState == null ?'' :ApplicationState) + '\"'+ ',' +
                    '\"CustomText37\":\"'+ (ApplicationPostCode == null ?'' :ApplicationPostCode) + '\"'+ ',' +
                    '\"CustomText36\":\"'+ (Latitude == null ?'' :Latitude) + '\"'+ ',' +
                    '\"CustomText32\":\"'+ (Longitude == null ?'' :Longitude) + '\"'+ ',' +
                    '\"CustomBoolean14\":\"'+ PreviouslyBuiltstr + '\"'+ ',' +
                    '\"CustomInteger7\":\"'+ (NoofPreCoveredbyAgreement == null ? 0 :NoofPreCoveredbyAgreement) + '\"'+ ',' +
                    '\"CustomBoolean4\":\"'+ utilityworksstr + '\"'+ ',' +
                    '\"CustomPickList32\":\"'+ (techplannedstr == null ?'' :techplannedstr) + '\"'+ ',' +
                    '\"CustomText46\":\"'+ (techplannedstr == null ?'' :techplannedstr) + '\"'+ ',' +
                    '\"CustomObject4Name\":\"'+ (Sam == null ? '' :Sam) + '\"'+ ',' +
                    '\"CustomObject1Name\":\"'+ (Fsa == null ? '' :Fsa) + '\"'+ ',' +
                    '\"CustomPickList26\":\"'+ (DwellingType == null ? '' :DwellingType) + '\"'+ ',' +
                    '\"CustomDate33\":\"'+ (datestr == null ? '' :datestr) + '\"'+ ',' +
                    '\"CustomPickList2\":\"'+ (BuildingType == null ?'' :BuildingType) + '\"'+                    
                    
                    '}]'+
                    '}';
                
                // Process POST request
                
                Http http = new Http();
                HttpRequest req;
                HttpResponse DevelopmentRes;
                req = new HttpRequest();
                req.setMethod('POST');
                req.setTimeout(120000);
                req.setHeader('content-type', 'application/vnd.oracle.adf.resource+json');
                jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                req.setHeader('Cookie', jsessionId);
                req.setBody(SmallDevJsonString);
                req.setEndpoint('callout:Insert_CRMOD_Development');
                DevelopmentRes = http.send(req);
                System.debug(DevelopmentRes.getBody());   
                
                
                //Parsing the Json Response
                if(DevelopmentRes.getStatusCode()!=500)
                {
                    JSONParser parser;
                    parser = JSON.createParser(DevelopmentRes.getBody());
                    system.debug('nextToken ==>' + parser.nextToken());
                    while (parser.nextToken() != null) {
                        if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'href')) {
                            parser.nextToken();
                            if(parser.getCurrentToken() == JSONToken.VALUE_STRING){
                                DevelopmentToken = parser.getText();
                                D1 = DevelopmentToken;
                                Dev1 = D1.lastIndexOf('/');
                                System.debug('Dev1 ' + Dev1);
                                Dev2 = D1.length();
                                System.debug('Dev2 ' + Dev2);
                                Developmentstr = DevelopmentToken.substring(Dev1+1,Dev2);
                                system.debug('Development ==>' + Developmentstr);  
                                
                            }
                            break;
                        }
                    }
                }
                
                else
                {
                    Application_Log__c error = new Application_Log__c(Source__c='InsertDevelopmentCRMOD',Source_Function__c='InsertDevCRMOD',Message__c= DevelopmentRes.getBody(),Stack_Trace__c=newDevAppFormDevelopment.Id);
                    insert error;
                }
                
                
                if(string.isNotBlank(billingConId) || string.isNotBlank(accConId) || string.isNotBlank(ContractSignatoryConId) || string.isNotBlank(ApplicantConId) )
                {
                    InsertDevelopmentContactCRMOD.sendRequest(Developmentstr, billingConId, accConId,ContractSignatoryConId,ApplicantConId);
                    
                }
                
                if(Developmentstr!='' && Developmentstr!=null)
                {
                    newDevAppFormDevelopment.CRMoD_Account_ID__c = Accountstr;
                    newDevAppFormDevelopment.CRMoD_Development_ID__c =  Developmentstr;
                    newDevAppFormDevelopment.CRMoD_Billing_Contact_ID__c =  billingConId == null ?ApplicantConId :billingConId;
                    newDevAppFormDevelopment.CRMoD_AP_Contact_ID__c =  accConId == null ?ApplicantConId :accConId;
                    newDevAppFormDevelopment.CRMoD_Contract_Signatory_Contact_ID__c = ContractSignatoryConId == null ?ApplicantConId :ContractSignatoryConId;
                    newDevAppFormDevelopment.CRMoD_Applicant_Contact_ID__c =  ApplicantConId;
                    newDevAppFormDevelopment.Status__c = 'CRMOD Record Creation Completed';
                }
                
                update newDevAppFormDevelopment;                 
                
            }
            
            if(Accountstr!=null && Developmentstr !=null && (NoofPreCoveredbyAgreement >3 || essentialService))
            {
                InsertStageApplicationCRMOD.InsertStageAppCRMOD(newDevAppFormDevelopment,Accountstr,Developmentstr,billingConId,accConId,ContractSignatoryConId,ApplicantConId);
            }
            
            
        }
        
        catch(Exception ex)
        {
            GlobalUtility.logMessage('Error', 'InsertDevelopmentCRMOD', 'InsertDevCRMOD', ' ', '', '', '', ex, 0);
        }
        
    }
}