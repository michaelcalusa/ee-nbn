/***************************************************************************************************
    Class Name          : InsertDevelopmentContactCRMOD
    Version             : 1.0 
    Created Date        : 21-Nov-2017
    Author              : Krishna Sai
    Description         : Class to Insert Development related COntact Records in CRMOD.
    Input Parameters    : NewDev Application Object

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 

****************************************************************************************************/
public class InsertDevelopmentContactCRMOD {
	public static void sendRequest(string Developmentstr,string billingConId,string accConId,string ContractSignatoryConId, string ApplicantConId)
	{
        try{
	    DOM.Document doc = new DOM.Document();
	    String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
	    String cus = 'urn:crmondemand/ws/customobject2/';
	    String cus1='urn:/crmondemand/xml/customobject2';
	    String wsse = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        string wsu = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';
	    Http http = new Http();
		HttpResponse res;
		HttpRequest req = new HttpRequest();
	    dom.XmlNode listOfAccOut,listOfDevOut,listOfSAOut,listOfMAROut,listOfMDUOut,AccOut,DevOut,SAOut,MAROut,MDUOut;
	    dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');
		envelope.setNamespace('cus', cus);
		envelope.setNamespace('cus1', cus1);
		String accLastPage,devLastPage,saLastPage,marLastPage,mduLastPage;
		dom.xmlNode[] accList, devList, saList, marList, mduList;
		Integer cCount,aCount,dCount,sCount,mCount,mdCount;
		//Header Part
		dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
		dom.XmlNode security= header.addChildElement('Security', wsse, 'wsse');
		dom.XmlNode usernameToken = security.addChildElement('UsernameToken',wsse,null);
        usernameToken.addChildElement('Username',wsse,null).addTextNode(CRMOD_Credentials__c.getinstance('User Name').Value__c);
		dom.XmlNode password = usernameToken.addChildElement('Password',wsse,null);
		password.setAttribute('Type','http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');
		password.addTextNode(CRMOD_Credentials__c.getinstance('Password').Value__c);
		
		//body part
		dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
		dom.XmlNode devInsertInput = body.addChildElement('CustomObject2WS_CustomObject2InsertChild_Input',cus,null);
		dom.XmlNode listOfDev = devInsertInput.addChildElement('ListOfCustomObject2',cus1,null);
		
		dom.XmlNode dev = listOfDev.addChildElement('CustomObject2',cus1,null);
		dom.XmlNode devId = dev.addChildElement('CustomObject2Id',cus1,null);
        devId.addTextNode(Developmentstr);
		
		
		//Contact Part
		dom.XmlNode listOfCon = dev.addChildElement('ListOfContact',cus1,null);
        if(string.isNotBlank(ContractSignatoryConId))
        {
            dom.XmlNode contactSignCon = listOfCon.addChildElement('Contact',cus1,null);
            dom.XmlNode contractSignConId = contactSignCon.addChildElement('ContactId',cus1,null);
            contractSignConId.addTextNode(ContractSignatoryConId);
        }
            if(string.isNotBlank(ApplicantConId))
        {
            dom.XmlNode applicantContact = listOfCon.addChildElement('Contact',cus1,null);
            dom.XmlNode applicantContactId = applicantContact.addChildElement('ContactId',cus1,null);
            applicantContactId.addTextNode(ApplicantConId);
        }
          
      
		if(string.isNotBlank(billingConId))
        {
            
            dom.XmlNode billingCon = listOfCon.addChildElement('Contact',cus1,null);
            dom.XmlNode billConId = billingCon.addChildElement('ContactId',cus1,null);
            billConId.addTextNode(billingConId);
        }
        if(string.isNotBlank(accConId))
        {
            dom.XmlNode accCon = listOfCon.addChildElement('Contact',cus1,null);
            dom.XmlNode acConId = accCon.addChildElement('ContactId',cus1,null);
            acConId.addTextNode(accConId);
        }
        
        req.setMethod('POST');
		req.setEndpoint(CRMOD_Credentials__c.getinstance('url').Value__c);
		req.setHeader('Content-Type', 'text/xml');
		req.setHeader('SOAPAction','"document/urn:crmondemand/ws/customobject2/:CustomObject2InsertChild"');
		
		req.setBodyDocument(doc);
		listOfCon.setAttribute('startrownum','0');
		res = http.send(req);
		System.debug(doc.toXmlString());
		System.debug(res.getBodyDocument().toXmlString());
        }
         catch(Exception ex)
        {
            GlobalUtility.logMessage('Error', 'InsertDevelopmentContactCRMOD', 'sendRequest', ' ', '', '', '', ex, 0);
        }   
             
     }
}