/***************************************************************************************************
    Class Name          : InsertStageAppContactCRMOD
    Version             : 1.0 
    Created Date        : 22-Nov-2017
    Author              : Krishna Sai
    Description         : Class to Insert StageApplication related COntact Records in CRMOD.
    Input Parameters    : NewDev Application Object

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 

****************************************************************************************************/
public class InsertStageAppContactCRMOD {
public static void sendStageRequest(string stagestr,string billingConId,string accConId,string ContractSignatoryConId, string ApplicantConId)
	{
        try{
	    DOM.Document doc = new DOM.Document();
	    String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
	    String ns = 'urn:crmondemand/ws/customobject3/10/2004';
	    String cus ='urn:/crmondemand/xml/customObject3';
	    String wsse = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        string wsu = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';
	    Http http = new Http();
		HttpResponse res;
		HttpRequest req = new HttpRequest();
	    //dom.XmlNode listOfSAOut,listOfMAROut,listOfMDUOut,AccOut,DevOut,SAOut,MAROut,MDUOut;
	    dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');
		envelope.setNamespace('ns', ns);
		envelope.setNamespace('cus', cus);
		String accLastPage,devLastPage,saLastPage,marLastPage,mduLastPage;
		//dom.xmlNode[] accList, devList, saList, marList, mduList;
		//Integer cCount,aCount,dCount,sCount,mCount,mdCount;
		//Header Part
		dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
		dom.XmlNode security= header.addChildElement('Security', wsse, 'wsse');
		dom.XmlNode usernameToken = security.addChildElement('UsernameToken',wsse,null);
        usernameToken.addChildElement('Username',wsse,null).addTextNode(CRMOD_Credentials__c.getinstance('User Name').Value__c);
		dom.XmlNode password = usernameToken.addChildElement('Password',wsse,null);
		password.setAttribute('Type','http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');
		password.addTextNode(CRMOD_Credentials__c.getinstance('Password').Value__c);
		
		//body part
		dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
		dom.XmlNode StageInsertInput = body.addChildElement('CustomObject3WS_CustomObject3InsertChild_Input',ns,null);
		dom.XmlNode listOfStage = StageInsertInput.addChildElement('ListOfCustomObject3',cus,null);
		
		dom.XmlNode stage = listOfStage.addChildElement('CustomObject3',cus,null);
		dom.XmlNode stageId = stage.addChildElement('CustomObject3Id',cus,null);
        stageId.addTextNode(stagestr);
		
		
		//Contact Part
		dom.XmlNode listOfCon = stage.addChildElement('ListOfContact',cus,null);
		if(string.isNotBlank(ContractSignatoryConId))
        {
            dom.XmlNode consignCon = listOfCon.addChildElement('Contact',cus,null);
            dom.XmlNode consignConId = consignCon.addChildElement('ContactId',cus,null);
            consignConId.addTextNode(ContractSignatoryConId);
        }
         
            if(string.isNotBlank(ApplicantConId))
        {
            dom.XmlNode applicantContact = listOfCon.addChildElement('Contact',cus,null);
            dom.XmlNode applicantContactId = applicantContact.addChildElement('ContactId',cus,null);
            applicantContactId.addTextNode(ApplicantConId);
        }
            
        if(string.isNotBlank(billingConId))
        {
            
            dom.XmlNode billingCon = listOfCon.addChildElement('Contact',cus,null);
            dom.XmlNode billConId = billingCon.addChildElement('ContactId',cus,null);
            billConId.addTextNode(billingConId);
        }
        if(string.isNotBlank(accConId))
        {
            dom.XmlNode accCon = listOfCon.addChildElement('Contact',cus,null);
            dom.XmlNode acConId = accCon.addChildElement('ContactId',cus,null);
            acConId.addTextNode(accConId);
        }
        
        req.setMethod('POST');
		req.setEndpoint(CRMOD_Credentials__c.getinstance('url').Value__c);
		req.setHeader('Content-Type', 'text/xml');
		req.setHeader('SOAPAction','"document/urn:crmondemand/ws/customobject3/10/2004:CustomObject3InsertChild"');
		
		req.setBodyDocument(doc);
		listOfCon.setAttribute('startrownum','0');
		res = http.send(req);
		System.debug(doc.toXmlString());
		System.debug(res.getBodyDocument().toXmlString());
        }
         catch(Exception ex)
        {
            GlobalUtility.logMessage('Error', 'InsertDevelopmentContactCRMOD', 'sendRequest', ' ', '', '', '', ex, 0);
        } 
    
}
}