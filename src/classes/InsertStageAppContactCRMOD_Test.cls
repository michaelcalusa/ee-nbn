@isTest
private class InsertStageAppContactCRMOD_Test {
/***************************************************************************************************
    Class Name          : InsertDevelopmentContactCRMOD_Test
    Version             : 1.0 
    Created Date        : 06-Feb-2018
    Author              : Dilip Athley
    Description         : Test class for InsertDevelopmentContactCRMOD
    Input Parameters    : NewDev Application Object

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 

****************************************************************************************************/
    
    static testMethod void insertStageAppContactTest() {
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        TestDataUtility.getListOfCRMODCredentialsRecords(true);
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        string body = '{"Contacts":[{"ContactLastName":"applicant-LastName","ContactFirstName":"applicant-FirstName111","AlternateAddress2":"","WorkPhone":"(04) 2074 7871","AlternateCountry":"Australia","ContactEmail":"applicant@test.com","CustomMultiSelectPickList5":"Consultant/Applicant","AlternateCity":"","AlternateZipCode":"","AlternateAddress1":"","AlternateProvince":"","HomePhone":"","links":{"self":{"rel":"self","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1"},"canonical":{"rel":"canonical","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1"},"AccountContactRoles":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/AccountContactRoles"},"AccountContacts":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/AccountContacts"},"CampaignRecipients":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/CampaignRecipients"},"ContactBooks":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ContactBooks"},"ContactNotes":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ContactNotes"},"ContactTeams":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ContactTeams"},"FinancialAccounts":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/FinancialAccounts"},"PlanContacts":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/PlanContacts"},"ServiceRequests":{"rel":"child","href":"/OnDemand/user/Rest/034/Contacts/AYSA-4M2YB1/child/ServiceRequests"}}}]}';
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
       	QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
		
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        InsertStageAppContactCRMOD.sendStageRequest('AYSA-1244444','AYSA-12344','AYSA-123444','AYSA-123444','AYSA-12345');
        Test.stopTest(); 
    }

}