/***************************************************************************************************
Class Name          : InsertStageApplicationCRMOD
Version             : 1.0 
Created Date        : 15-Nov-2017
Author              : Krishna Sai
Description         : Class to Insert Stage Application Records in CRMOD.
Input Parameters    : NewDev Application Object

Modification Log    :
* Developer             Date            Description
* ----------------------------------------------------------------------------                 

****************************************************************************************************/
public class InsertStageApplicationCRMOD {
    public static void InsertStageAppCRMOD(NewDev_Application__c newDevAppFormStageApplication,string Accountstr,string Developmentstr,String billingConId,string accConId,string ContractSignatoryConId, string ApplicantConId){
        try{
            String PaymentMethod,WhatareyouBuilding,GeneralComments,ApplicationPassword,ApplicationStreet1,ApplicationStreet2,ApplicationSuburb,StageApplicationToken,S1,Stagestr,sam,fsa,BuildingType,DwellingType,servicestatus,Address;
            string ApplicationState,ApplicationCountry,Latitude,Longitude,ApplicationId,ApplicationName,jsessionId,ApplicationPostcode,techplannedstr,techPlanned;
            decimal NumberofSDU,NumberofMDU,NumberofEssentialServices,NoofLots,NoofPremises,Stages,TotalNoOfPremises,NoofNonResiPremises,NoofResidentialPremises,CurrentStage;
            integer Stage1,Stage2;
            date EFSCD,RFS,TrenchwSTDate;
            boolean TechnicalAssessment;
            PaymentMethod = newDevAppFormStageApplication.Payment_Method__c;
            NumberofSDU = newDevAppFormStageApplication.Number_of_SDUs__c;
            NumberofMDU = newDevAppFormStageApplication.Number_of_MDUs__c;
            NumberofEssentialServices= newDevAppFormStageApplication.Number_of_Essential_Services__c;
            WhatareyouBuilding = newDevAppFormStageApplication.What_are_you_Building__c;
            GeneralComments = newDevAppFormStageApplication.General_Comments__c;
            ApplicationPassword = newDevAppFormStageApplication.Applicant_Password__c;
            ApplicationStreet1 = newDevAppFormStageApplication.Application_Street_1__c;
            ApplicationStreet2 = newDevAppFormStageApplication.Application_Street_2__c;
            ApplicationSuburb = newDevAppFormStageApplication.Application_Suburb__c;
            ApplicationPostcode = newDevAppFormStageApplication.Application_Postcode__c;
            ApplicationState = newDevAppFormStageApplication.Application_State__c;
            ApplicationCountry = newDevAppFormStageApplication.Application_Country__c;
            Latitude = newDevAppFormStageApplication.Latitude__c;
            Longitude = newDevAppFormStageApplication.Longitude__c;
            ApplicationId = newDevAppFormStageApplication.Name;
            ApplicationName = newDevAppFormStageApplication.Application_Name__c;
            techPlanned = newDevAppFormStageApplication.Technology_Planned__c; 
            TechnicalAssessment = newDevAppFormStageApplication.Technical_Assessment__c;
            EFSCD = newDevAppFormStageApplication.Estimated_Occupancy_Date__c;
            RFS = newDevAppFormStageApplication.RFS_Date__c;
            TrenchwSTDate = newDevAppFormStageApplication.Estimated_Trench_Start_Date__c;
            NoofLots = newDevAppFormStageApplication.Total_Number_of_Lots__c;
            NoofPremises = newDevAppFormStageApplication.Total_Number_Of_Premises_In_CurrentStage__c;
            Stages = newDevAppFormStageApplication.Current_Stage__c;
            sam = newDevAppFormStageApplication.samId__c;
            fsa = newDevAppFormStageApplication.fsaId__c;
            TotalNoOfPremises = newDevAppFormStageApplication.Total_Number_Of_Premises_In_CurrentStage__c;
            NoofResidentialPremises = newDevAppFormStageApplication.Total_number_of_residential_premises__c;
            NoofNonResiPremises = newDevAppFormStageApplication.Total_number_of_commercial_premises__c;
            CurrentStage = newDevAppFormStageApplication.Current_Stage__c;
            servicestatus = newDevAppFormStageApplication.Service_Status__c;
            String datestr;
            String RFSdatestr;
            String Trenchdatestr;
            
            if(EFSCD!=null){
                //String datestr = efscd.format();
                Integer iYear= EFSCD.year();
                Integer iMonth= EFSCD.month();
                Integer iDate= EFSCD.day();
                String strDay;
                String strMonth;
                if(iDate<10){
                    strDay = '0' + iDate;
                }
                else{
                    strDay =string.valueOf(iDate);
                }
                if(iMonth<10){
                    strMonth = '0' + string.valueOf(iMonth);
                }
                else{
                    strMonth = string.valueOf(iMonth);
                }
                datestr       = iYear + '-' + strMonth + '-' + strDay;
            }
           
            if(RFS!=null){
                //String RFSdatestr = RFS.format();
                Integer iRFSYear= RFS.year();
                Integer iRFSMonth= RFS.month();
                Integer iRFSDate= RFS.day();
                String RFSstrDay;
                String RFSstrMonth;
                if(iRFSDate<10){
                    RFSstrDay = '0' + iRFSDate;
                }
                else{
                    RFSstrDay =string.valueOf(iRFSDate);
                }
                if(iRFSMonth<10){
                    RFSstrMonth = '0' + string.valueOf(iRFSMonth);
                }
                else{
                    RFSstrMonth = string.valueOf(iRFSMonth);
                }
                RFSdatestr = iRFSYear + '-' + RFSstrMonth + '-' + RFSstrDay; 
            }
           
            
            if(TrenchwSTDate!=null){
                // String Trenchdatestr = TrenchwSTDate.format();
                Integer iTrenchYear= TrenchwSTDate.year();
                Integer iTrenchMonth= TrenchwSTDate.month();
                Integer iTrenchDate= TrenchwSTDate.day();
                String TrenchstrDay;
                String TrenchstrMonth;
                if(iTrenchDate<10){
                    TrenchstrDay = '0' + iTrenchDate;
                }
                else{
                    TrenchstrDay =string.valueOf(iTrenchDate);
                }
                if(iTrenchMonth<10){
                    TrenchstrMonth = '0' + string.valueOf(iTrenchMonth);
                }
                else{
                    TrenchstrMonth = string.valueOf(iTrenchMonth);
                }
                Trenchdatestr = iTrenchYear + '-' + TrenchstrMonth + '-' + TrenchstrDay;                 
            }
            if(techPlanned!=null)
            {
            if(techPlanned.toLowerCase().contains('wireless') || techPlanned.toLowerCase().contains('satellite')){
                techplannedstr = 'Fixed Wireless / Satellite';              
            }
            else if(techPlanned == 'FTTP')
            {
                techplannedstr = 'FTTP – Type 3 (MT-LFN)';
            }
            else{
                techplannedstr = techPlanned;
            }
            if(techplannedstr == 'FTTN' || techplannedstr == 'FTTP – Type 2' || techplannedstr == 'FTTP – Type 3 (MT-LFN)' || techplannedstr == 'FTTC' || 
               techplannedstr == 'FTTB'|| techplannedstr == 'HFC'|| techplannedstr == 'Fixed Wireless / Satellite'){
                   techplannedstr = techplannedstr;                
               }
            }
            else{
                techplannedstr = '';
            }
            
            if(newDevAppFormStageApplication.Building_Type__c != null){
               if(newDevAppFormStageApplication.Building_Type__c == 'Residential use only'){
                   BuildingType = 'Residential';  
                   NoofResidentialPremises = TotalNoOfPremises;
               }
               else if(newDevAppFormStageApplication.Building_Type__c == 'Commercial use only'){  
                   BuildingType = 'Commercial';
                   NoofNonResiPremises = TotalNoOfPremises;
               }
               else if(newDevAppFormStageApplication.Building_Type__c == 'Mixed residential and commercial use'){  
                   BuildingType = 'Mixed Use';
               }
               else{  
                   BuildingType = 'Other';
               }
           }
            
            if(newDevAppFormStageApplication.Service_Status__c !=null){
              if(newDevAppFormStageApplication.Service_Status__c == 'proposed' || newDevAppFormStageApplication.Service_Status__c == null ){
                   servicestatus = 'No Existing Estate/SAM'; 
              }
                  else if(newDevAppFormStageApplication.Service_Status__c == 'in_construction'){  
                   servicestatus = 'SAM Under Construction';
                   
               }
                   else if(newDevAppFormStageApplication.Service_Status__c == 'available'){  
                   servicestatus = 'SAM In Service';
                   
               }
                
           }
            
              if(newDevAppFormStageApplication.Dwelling_Type__c != null){
               DwellingType = newDevAppFormStageApplication.Dwelling_Type__c == 'Single Dwelling Unit (SDU)' ? 'SDU Only' : 'MDU Only';
           }
            
             if (ApplicationStreet2!= null){
               Address = ApplicationStreet1+ ' ' + ApplicationStreet2;
           }
           else{
               
               Address = ApplicationStreet1;
           }
              if (ApplicationStreet1 == null){
               
               Address = ApplicationStreet2;
           }
                
               if (Sam!= null)
           {
               if ( Sam.contains('|')){
               Sam = Sam.substringBefore('|');
               
           	}
           }
            
            string jsonStageApplicationInput = '{'+
                '\"CustomObjects3\": '+'[ {'+
                '\"Name\":'+'\"'+ ApplicationName +'\"'+ ',' +
                '\"AccountId\":'+'\"'+ (Accountstr == null ?'' :Accountstr) +'\"'+ ',' +
                '\"CustomObject2Id\":'+'\"'+ (Developmentstr == null ?'' :Developmentstr) +'\"'+ ',' +
                '\"CustomDate31\":'+'\"'+ (datestr == null ?'' :datestr) +'\"'+ ',' +  
                '\"CustomDate38\":'+'\"'+ (RFSdatestr == null ?'' :RFSdatestr) +'\"'+ ',' +   
                '\"CustomDate35\":'+'\"'+  (Trenchdatestr == null ?'' :Trenchdatestr) +'\"'+ ',' + 
                '\"CustomInteger27\":'+'\"'+ (NoofLots == null ? 0 :NoofLots) +'\"'+ ',' + 
                '\"CustomInteger9\":'+'\"'+ (NoofPremises == null ? 0 :NoofPremises) +'\"'+ ',' +
                '\"CustomInteger2\":'+'\"'+ (NoofPremises == null ? 0 :NoofPremises) +'\"'+ ',' +  
                '\"CustomInteger20\":'+'\"'+ (NoofResidentialPremises == null ? 0 :NoofResidentialPremises) +'\"'+ ',' +
                '\"CustomInteger21\":'+'\"'+ (NoofNonResiPremises == null ? 0 :NoofNonResiPremises) +'\"'+ ',' + 
                '\"CustomPickList0\": \"Active\"'+ ',' +
                '\"CustomPickList36\":'+'\"'+ (servicestatus == null ?'' :servicestatus) +'\"'+ ',' +
                '\"IndexedPick0\": \"New\"'+ ',' +
                '\"CustomPickList28\": \"1-Normal\"'+ ',' +
                '\"CustomPickList4\": \"Not Started\"'+ ',' +
                '\"CustomPickList30\": \"Not Started\"'+ ',' +
                '\"CustomPickList8\": \"Not Started\"'+ ',' +
                '\"CustomPickList28\": \"1-Normal\"'+ ',' +
                '\"Type\":'+'\"'+ (DwellingType == null ?'' :DwellingType) +'\"'+ ',' +
                '\"CustomInteger14\": \"0\"' + ',' +
                '\"CustomText0\":'+'\"'+ (Address == null ?'' :Address) +'\"'+ ',' +
                '\"CustomText46\":'+'\"'+ (ApplicationSuburb == null ?'' :ApplicationSuburb)+'\"'+ ',' +
                '\"CustomPickList6\":'+'\"'+ (ApplicationState == null ?'' :ApplicationState) +'\"'+ ',' +
                '\"CustomText37\":'+'\"'+ (ApplicationPostcode == null ?'' :ApplicationPostcode) +'\"'+ ',' +
                '\"CustomText40\":'+'\"'+ (Latitude == null ?'' :Latitude) +'\"'+ ',' +
                '\"CustomPickList31\":\"'+ (techplannedstr == null ?'' :techplannedstr) + '\"'+ ',' +
                '\"CustomText58\":\"'+ (techplannedstr == null ?'' :techplannedstr) + '\"'+ ',' +
                 '\"CustomObject4Name\":\"'+ (Sam == null ? '' :Sam) + '\"'+ ',' +
                 '\"CustomObject1Name\":\"'+ (Fsa == null ? '' :Fsa) + '\"'+ ',' +
                 '\"CustomText33\":\"'+ (CurrentStage == null ? 0 :CurrentStage) + '\"'+ ',' +
                '\"CustomText39\":'+'\"'+ (Longitude == null ?'' :Longitude) +'\"'+ 
                
                '}]'+
                '}';
            System.debug('StageApp JSON format: ' + jsonStageApplicationInput);
            
            // Process POST request
            
            Http http = new Http();
            HttpRequest req;
            HttpResponse StageAppRes;
            req = new HttpRequest();
            req.setMethod('POST');
            req.setTimeout(120000);
            req.setHeader('content-type', 'application/vnd.oracle.adf.resource+json');
            jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
            req.setHeader('Cookie', jsessionId);
            req.setBody(jsonStageApplicationInput);
            req.setEndpoint('callout:Insert_CRMOD_Stage_Application');
            StageAppRes = http.send(req);
            System.debug(StageAppRes.getBody()); 
            
            //Parsing the Json Response
            if(StageAppRes.getStatusCode()!=500)
            {
                JSONParser parser;
                parser = JSON.createParser(StageAppRes.getBody());
                system.debug('nextToken ==>' + parser.nextToken());
                while (parser.nextToken() != null) {
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'href')) {
                        parser.nextToken();
                        if(parser.getCurrentToken() == JSONToken.VALUE_STRING){
                            StageApplicationToken = parser.getText();
                            S1 = StageApplicationToken;
                            Stage1 = S1.lastIndexOf('/');
                            Stage2 = S1.length();
                            Stagestr = StageApplicationToken.substring(Stage1+1,Stage2);
                            system.debug('Stage Application ==>' + Stagestr);  
                            
                        }
                        break;
                    }
                }
            }
            else{
                Application_Log__c error = new Application_Log__c(Source__c='InsertStageApplicationCRMOD',Source_Function__c='InsertStageAppCRMOD',Message__c= StageAppRes.getBody(),Stack_Trace__c=newDevAppFormStageApplication.Id);
                insert error;
            }
            
            InsertStageAppContactCRMOD.sendStageRequest(Stagestr,billingConId,accConId,ContractSignatoryConId,ApplicantConId);
            
            // Logic to update CRMoD Ids on to NewDev Application rec.
            if(Stagestr!='' && Stagestr!=null){
                newDevAppFormStageApplication.CRMoD_Account_ID__c = Accountstr;
            	newDevAppFormStageApplication.CRMoD_Development_ID__c =  Developmentstr;
            	newDevAppFormStageApplication.CRMoD_Billing_Contact_ID__c =  billingConId == null ?ApplicantConId :billingConId;
            	newDevAppFormStageApplication.CRMoD_AP_Contact_ID__c =  accConId == null ?ApplicantConId :accConId;
            	newDevAppFormStageApplication.CRMoD_Contract_Signatory_Contact_ID__c = ContractSignatoryConId == null ?ApplicantConId :ContractSignatoryConId;
            	newDevAppFormStageApplication.CRMoD_Applicant_Contact_ID__c =  ApplicantConId;
            	newDevAppFormStageApplication.CRMoD_Stage_ID__c = Stagestr;
                
            	if(TechnicalAssessment)
                {
                   
                    newDevAppFormStageApplication.Status__c = 'Tech Assessment send to CRMOD Completed';
                }
                else{
                    newDevAppFormStageApplication.Status__c = 'CRMOD Record Creation Completed';
                }
            	
                update newDevAppFormStageApplication;              
            }
        }
        catch(Exception ex)
        {
            GlobalUtility.logMessage('Error', 'InsertStageApplicationCRMOD', 'InsertStageAppCRMOD', ' ', '', '', '', ex, 0);
        }
        
    }
}