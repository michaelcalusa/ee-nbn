/***************************************************************************************************
    Class Name  : IntegrationUtilityMockHttpResponseGenerator
    Class Type  : Test Class 
    Version     : 1.0 
    Created Date: 17-January-2018
    Function    : This class contains test responses for call outs
    Used in     : None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Sanghita Das            17/01/2018                 Created
    * Sethu                   27/03/2018                 Added a new mock response for TOW http request and created a new statusCode var 
                                                         to hold the failure codes
    * Krishna Kanth           15/04/2019                 Updated mock response based on new JSON structure 
****************************************************************************************************/
@isTest
public class IntUtilityMockHttpRespGen implements HttpCalloutMock { 
    public string strResponseType {get;set;}
    public string Status {get;set;}
    public Integer TOWStatusCode;
    public Integer StatusCode;
    
    
    public string strResponseCIS = '{' +
            '"data":{' +
            '"type":"avc-d",' +
            '"id":"AVC000022249851",' +
            '"attributes":{' +
            '"elementManager":"SAM",' +
            '"bandwidthProfile":"D25-100_U5-40_Mbps_TC4_P",' +
            '"avcType":"1:1",' +
            '"avcResourceId":"ALK000000000006",' +
            '"primaryAccessTechnology":"FibreToTheBuilding/FibreToTheNode",' +
            '"poiName":"2MAI-MAITLAND",' +
            '"avcConfig":{' +
            '"minLay1UpBitRate":"5100",' +
            '"minLay1DownBitRate":"25900",' +
            '"insertDslLineRate":"True",' +
            '"icsServiceId":"ILK000000414065"' +
            '},' +
            '"aas":{' +
            '"aasName":"SWAAS0000102",' +
            '"svlan":"3000",' +
            '"ipAddress":"1.1.1.1",' +
            '"primaryPort":"1/1/1",' +
            '"secondaryPort":"1/1/2",' +
            '"connectedtoPort":"1/1/3"' +
            '},' +
            '"cvc":{' +
            '"cvcId":"CVC000000190598",' +
            '"cvcResourceId":"CLK000000000006",' +
            '"tc1BandwidthProfile":"D200_U200_Mbps_TC1_C",' +
            '"tc2BandwidthProfile":"D200_U200_Mbps_TC2_C",' +
            '"tc4BandwidthProfile":"D2000_U2000_Mbps_TC4_C",' +
            '"mcBandwidthProfile":"D2000_U2000_Mbps_TC4_C"' +
            '},' +
            '"uniDsl":{' +
            '"uniId":"UNI000000773681",' +
            '"uniDslId":"CPI300000418831",' +
            '"uniDslVlanMode":"Tagged",' +
            '"dslStabilityProfile":"Standard",' +
            '"potsInterconnect":"0245456780",' +
            '"uniDslMode":"VDSL2",' +
            '"exchangePairConnection":"No",' +
            '"csll":{' +
            '"id":"PRI000004401447",' +
            '"type":"USL",' +
            '"productStatus":"Terminated"' +
            '}' +
            '},' +
            '"nniNodes":[{' +
            '"nniId":"SWEAS0000105",' +
            '"nniType":"i-nni",' +
            '"nniCvlanId":"1",' +
            '"nniSvlanId":"1",' +
            '"nniLagId":"102",' +
            '"nniIpAddress":"1.1.1.1"' +
            '},' +
            '{' +
            '"nniId":"SWEAS0000105",' +
            '"nniType":"i-nni",' +
            '"nniCvlanId":"1",' +
            '"nniSvlanId":"1",' +
            '"nniLagId":"103",' +
            '"nniIpAddress":"3.3.3.3"' +
            '},' +
            '{' +
            '"nniId":"SWEAS0000105",' +
            '"nniType":"i-nni",' +
            '"nniCvlanId":"1",' +
            '"nniSvlanId":"1",' +
            '"nniLagId":"104",' +
            '"nniIpAddress":"4.4.4.4"' +
            '}]' +
            '},' +
            '"relationships":{' +
            '"associatedWith":{' +
            '"data":{' +
            '"id":"PRI000057309003",' +
            '"type":"ncasProductInstance"' +
            '}' +
            '},' +
            '"servicedBy":{' +
            '"data":{' +
            '"id":"SWOLT0000112",' +
            '"type":"dslam"' +
            '}' +
            '}' +
            '}' +
            '},' +
            '"included":[{' +
            '"type":"ncasProductInstance",' +
            '"id":"PRI000057309003",' +
            '"attributes":{' +
            '"locationId":"LOC000142502552",' +
            '"accessSeekerId":"ASI0000002209977",' +
            '"priorityAssit":"No",' +
            '"enhancedSla":"Enhanced-8"' +
            '}' +
            '},{' +
            '"type":"dslam",' +
            '"id":"SWOLT0000112",' +
            '"attributes":{' +
            '"rack":"1",' +
            '"shelf":"1",' +
            '"slot":"LT3",' +
            '"port":"7",' +
            '"dslamType":"LNEU",' +
            '"ontIpAddress":"12.34.56.78",' +
            '"dpboProfile":"ADP-VDSL_12dB"' +
            '}' +
            '}]' +
            '}';
    //Added as part of CCT-2247
    //Updated by Krishna Kanth - 15/4/19
    public String sdm_strResponse =  '{  '+
		'   \"data\":{  '+
		'      \"locationid\":\"LOC000033466416\",'+
		'      \"accessseekerdetails\":{  '+
		'         \"accessseekerid\":\"ASI000000221967\",'+
		'         \"accessseekername\":\"Exetel Pty Ltd\"'+
		'      },'+
		'      \"orderdetails\":[  '+
		'         {  '+
		'            \"nbnorderid\":\"ORD410016340008\",'+
		'            \"asreferenceid\":\"Test\",'+
		'            \"orderstatus\":\"Complete\",'+
		'            \"dateacknowledged\":\"2017-10-20T04:40:16Z\",'+
		'            \"dateaccepted\":\"2017-10-20T04:42:56Z\",'+
		'            \"datecompleted\":\"2017-10-20T04:53:11Z\",'+
		'            \"orderserviceabilityclass\":\"20\",'+
		'            \"sla\":\"Standard\",'+
		'            \"appdetails\":[  '+
		'               {  '+
		'                  \"appointmentid\":\"APT700000163234\",'+
		'                  \"nbnslottype\":\"AM\",'+
		'                  \"nbndemandtype\":\"STANDARD INSTALL\",'+
		'                  \"status\":\"BOOKED\",'+
		'                  \"nbnappstart\":\"2017-10-26T09:00:00\",'+
		'                  \"nbnappstartlocal\":\"2017-10-26T00:00:00\",'+
		'                  \"nbnappstartlocaltime\":\"1970-01-01T08:00:00\",'+
		'                  \"nbnappend\":\"2017-10-26T13:00:00\",'+
		'                  \"nbnappendlocal\":\"2017-10-26T00:00:00\",'+
		'                  \"nbnappendlocaltime\":\"1970-01-01T12:00:00\",'+
		'                  \"wodetails\":[  '+
		'                     {  '+
		'                        \"woid\":\"WOR700000187311\",'+
		'                        \"nbnworkforce\":\"DP1019\",'+
		'                        \"woparent\":null,'+
		'                        \"wostatus\":\"APPR\",'+
		'                        \"woreasoncode\":\"ASSIGNED-TO-SDP\",'+
		'                        \"woprd\":null,'+
		'                        \"wopriority\":null'+
		'                     },'+
		'                     {  '+
		'                        \"woid\":\"WOR700000187324\",'+
		'                        \"nbnworkforce\":\"DP1019\",'+
		'                        \"woparent\":\"WOR700000187311\",'+
		'                        \"wostatus\":\"APPR\",'+
		'                        \"woreasoncode\":\"ASSIGNED-TO-SDP\",'+
		'                        \"woprd\":null,'+
		'                        \"wopriority\":null'+
		'                     }'+
		'                  ]'+
		'               }'+
		'            ]'+
		'         },'+
		'         {  '+
		'            \"nbnorderid\":\"ORD410020090076\",'+
		'            \"asreferenceid\":\"auto_Order\",'+
		'            \"orderstatus\":\"InProgress\",'+
		'            \"dateacknowledged\":\"2018-04-25T09:19:25Z\",'+
		'            \"dateaccepted\":\"2018-04-25T09:21:32Z\",'+
		'            \"datecompleted\":null,'+
		'            \"orderserviceabilityclass\":\"20\",'+
		'            \"sla\":\"Standard\",'+
		'            \"appdetails\":[  '+
		''+
		'            ]'+
		'         },'+
		'         {  '+
		'            \"nbnorderid\":\"ORD410020090082\",'+
		'            \"asreferenceid\":\"auto_Order\",'+
		'            \"orderstatus\":\"InProgress\",'+
		'            \"dateacknowledged\":\"2018-04-25T09:22:06Z\",'+
		'            \"dateaccepted\":\"2018-04-25T10:24:12Z\",'+
		'            \"datecompleted\":null,'+
		'            \"orderserviceabilityclass\":\"20\",'+
		'            \"sla\":\"Standard\",'+
		'            \"appdetails\":[  '+
		''+
		'            ]'+
		'         }'+
		'      ]'+
		'   }'+
		'}';  
        
        
    public HTTPResponse respond(HTTPRequest req) {
        String strResponse;
        if(strResponseType == 'product'){
            strResponse = '{"data":[{"id":"PRI003000710422","type":"ncasProductInstance","attributes":{"locationId":"LOC000020377685","primaryAccessTechnology":"Fibre To The Node","priorityAssist":"No","productStatus":"Active","orderCompletionDate":"19/03/2018 15:01:56"},"relationships":{"accessSeeker":{"data":{"id":"ASI000000220997","type":"accessSeeker"},"links":{"related":"http://10.11.43.23:8484/accessseeker/v1/info/ASI000000220997"}},"services":{"data":[{"id":"AVC000022352577","type":"avc-d"}],"links":{"related":"https://cis-api-sit3.slb.nbndc.local/rest/services/v1/avcs/"}}}}]}';
        }
        if(strResponseType == 'CISPRI'){
            strResponse= '{"data":[{"id":"PRI003000710422","type":"csllProductInstance","attributes":{"copperPathId":"CPI300006462521"},"relationships":{"address":{"data":{"id":"LOC000080833411","type":"location"},"links":{"related":"https://cis-api-sit3.slb.nbndc.local/rest/places/v1/locations/LOC000080833411"}}}}],"included":[{"type":"location","id":"LOC000080833411","attributes":{"dwellingType":"SDU","region":"Urban","regionValueDefault":"FALSE","externalTerritory":"FALSE","address":{"unstructured":"LOT 9 10 YURUGA RD DURAL NSW 2158","roadNumber1":"10","roadName":"YURUGA","roadTypeCode":"RD","locality":"DURAL","postCode":"2158","state":"NSW","lotNumber":"9","planNumber":"9//DP735652"},"primaryAccessTechnology":{"technologyType":"Fibre To The Node","serviceClass":"12","rolloutType":"Brownfields"},"landUse":"AGRICULTURAL","geoCode":{"latitude":"-33.695061","longitude":"151.012085","geographicDatum":"GDA94","srid":"4283"}},"relationships":{"containmentBoundaries":{"data":[{"id":"2DUR","type":"NetworkBoundary","boundaryType":"FSA"},{"id":"S2 -Dural-Khurst-Wisemans Fery","type":"NetworkBoundary","boundaryType":"SSA"},{"id":"2CAS","type":"NetworkBoundary","boundaryType":"AAR"},{"id":"2DUR-20-13","type":"NetworkBoundary","boundaryType":"ADA"},{"id":"2DUR-20","type":"NetworkBoundary","boundaryType":"SAM"}]}}}]}';
        }
        else if(strResponseType == 'CISOrderHistoryAPI'){
            strResponse= '{"data":[{"id":"PRI003000710422","type":"csllProductInstance","attributes":{"orderStatus":"Complete"},"relationships":{"address":{"data":{"id":"LOC000080833411","type":"location"},"links":{"related":"https://cis-api-sit3.slb.nbndc.local/rest/places/v1/locations/LOC000080833411"}}}}],"included":[{"type":"location","id":"LOC000080833411","attributes":{"orderStatus":"Complete","dwellingType":"SDU","region":"Urban","regionValueDefault":"FALSE","externalTerritory":"FALSE","address":{"unstructured":"LOT 9 10 YURUGA RD DURAL NSW 2158","roadNumber1":"10","roadName":"YURUGA","roadTypeCode":"RD","locality":"DURAL","postCode":"2158","state":"NSW","lotNumber":"9","planNumber":"9//DP735652"},"primaryAccessTechnology":{"technologyType":"Fibre To The Node","serviceClass":"12","rolloutType":"Brownfields"},"landUse":"AGRICULTURAL","geoCode":{"latitude":"-33.695061","longitude":"151.012085","geographicDatum":"GDA94","srid":"4283"}},"relationships":{"containmentBoundaries":{"data":[{"id":"2DUR","type":"NetworkBoundary","boundaryType":"FSA"},{"id":"S2 -Dural-Khurst-Wisemans Fery","type":"NetworkBoundary","boundaryType":"SSA"},{"id":"2CAS","type":"NetworkBoundary","boundaryType":"AAR"},{"id":"2DUR-20-13","type":"NetworkBoundary","boundaryType":"ADA"},{"id":"2DUR-20","type":"NetworkBoundary","boundaryType":"SAM"}]}}}]}';
        }
        else if(strResponseType == 'PNI'){
            strResponse = '{"transactionId":"aeb8a1007f60271d","metadata":{"nbnCorrelationId":"0e8cedd0-ad98-11e6-bf2e-47644ada7c0f","tokenId":"3c08d723a26f1234","copperPathId":"CPI300000403245","changeReason":"DATA-CORRECTION","requestedPair":"P:12"},"copperPathId":"CPI300000403245","status":"Success","pairConnected":"abcde:123"}';
        }
        else if(strResponseType == 'DiagnosticTests'){
            strResponse = '{"transactionId":"aeb8a1007f60271d","testReferenceId":"WRI900000664825","status":"Completed","result":"Passed","startDateTime":"2017-09-07T01:34:48Z","endDateTime""2017-09-07T01:34:52Z","metadata":{"nbnCorrelationId":"0e8cedd0-ad98-11e6-bf2e-47644ada7c0f","tokenId":"3c08d723a26f1234","requestorId""ASI000000000035","serviceId":"AVC000028726705"},"diagnosticDetails":[{"type":"LSD Test","status":"Completed","result":"Passed","lineId""SWDSL0200175:1-1-1-4","userLabel":"UNI000022118321","lineOperationalStatus":"Up","lastStatusChange":"2017-10-17T05:40:57Z", "serviceStability:"STABLE",systemVendorModelHex""41327076364630333976320000000000","systemVendorModelAscii":"A2pv6F039v4","serialNumber":"CP1632TAEHG 799vac17.2","actualCpeVectoringType:"g.vector capable","vectoringStatus":"Enabled","estimatedDeltDistance":"465","electricalLength":"9.5","rtxActualNetDataRateUpstrea":"6400.0","rtxActualNetDataRateDownstream":"28000.0","rtxActualNetDataRateUnitType":"kbps",  "rtxActualExpectedThroughputUpstream":"6335.0",rtxActualExpectedThroughputDownstream":"27717.0","rtxActualExpectedThroughputUnitType":"kbps",   "rtxAttainableNetDataRateUpstream":"43725.0", "rtxAttainableNetDataRateDownstream":"100164.0","rtxAttainableNetDataRateUnitType":"kbps","rtxAttainableExpectedThroughputUpstream":"43212.0", "rtxAttainableExpectedThroughputDownstream":"98990.0","rtxAttainableExpectedThroughputUnitType":"kbps",            "tc2ExistingWbaThresholdUpstream":"38,200.0", "tc2ExistingWbaThresholdDownstream":"38,200.0","tc2ExistingWbaThresholdUnitType":"kbps",              "tc4ExistingWbaThresholdUpstream":"26,100.0", "tc4ExistingWbaThresholdDownstream":"5,100.0","tc4ExistingWbaThresholdUnitType":"kbps",       "higherCalculatedRateUpstream":"38,200.0", "higherCalculatedRateDownstream":"38,200.0","higherCalculatedRateUnitType":"kbps",       "fecActualBitrateUpstream":"6400.0",fecActualBitrateDownstream""28000.0","fecActualBitrateUnitType":"kbps","fecAttainableBitrateUpstream":"6400.0","fecAttainableBitrateDownstream":"28000.0","fecAttainableBitrateUnitType":"kbp",     "tc2OrderedSpeedUpstream":"30,000.0","tc2OrderedSpeedDownstream":"30,000.0","tc2OrderedSpeedUnitType":"kbps",   "tc2CapableDownstreamResult":"PASS","tc2CapableUpstreamResult":"PASS",  "tc4OrderedSpeedUpstream":"40,000.0","tc4OrderedSpeedDownstream":"40,000.0","tc4OrderedSpeedUnitType":"kbps",   "tc4CapableDownstreamResult":"PASS","tc4CapableUpstreamResult":"PASS",  "noiseMarginAverageUpstream":"22.8","noiseMarginAverageDownstream":"20.7","noiseMarginAverageUnitType":"dB",            "attenuationAverage":{"unitType":"dB","bands":[{"bandId":"0","downstream":"127.0","upstream":"4.5"},{"bandId":"1","downstream":"15.2","upstream":"18.5"},{"bandId":"2", "downstream":"25.2","upstream":"18.5"},{"bandId":"3","downstream":"36.2","upstream":"18.5"}]},      "loopAttenuationAverage":{"unitType":"dB","bands":[ {"bandId":"0","downstream":"127.0","upstream":"4.5"},{"bandId":"1","downstream":"15.2","upstream":"18.5"},{ "bandId":"2","downstream":"25.2","upstream":"18.5"},{"bandId":"3","downstream":"36.2", "upstream":"18.5"}]},                "relativeCapacityOccupationUpstream":"43.0","relativeCapacityOccupationDownstream":"46.0", "relativeCapacityOccupationUnitType":"%",        "actualPsdUpstream":"-71.0","actualPsdDownstream":"-66.0","actualPsdUnitType":"dBm/Hz",          "actualRtxModeUpstream":"In Use","actualRtxModeDownstream":"In Use",       "userTrafficUpstream":"0",  "userTrafficDownstream":"0", "userTrafficUnitType:"octets","outputPowerUpstream":"10.7","outputPowerDownstream":"6.3","outputPowerUnitType":"dBm",  "supportedCpeVectoringType":[legacy","g.vector capable"],   "layer2BitrateUpstream":"15009","layer2BitrateDownstream":"4341","layer2BitrateUnitType":"kbps",    "physicalProfile":"12/1 Standard 6dB","coexistence":"No","fallbackState":"NotActive"}]}';
        }
        else if(strResponseType == 'Service'){
            strResponse = strResponseCIS;
        }
        else if(strResponseType == 'CISServicesAPI'){
            strResponse = strResponseCIS;
            
        }
        else if(strResponseType == 'Location'){
            strResponse = '{ "data":[ {"type": "location","id": "LOC123456789012","attributes": {"gnafId": "G8932387982982","dwellingType": "MDU","region": "Urban","regionValueDefaulted": "FALSE","externalTerritory": "FALSE", "address": {"unstructured": "FL 1 OFFC 1 50 Miller ST North Sydney NSW 2060","roadNumber1": "50","roadName": "Miller","roadTypeCode": "ST","locality": "North Sydney","postCode": "2060","levelNumber": "1","levelType": "FL", "unitNumber": "1","unitType": "OFFC", "state":"NSW"},"geocode": {"latitude": "37.8136","longitude": "144.9631", "geographicDatum": "GDA94","srid": "4283"},"primaryAccessTechnology": {"technologyType": "Fibre", "serviceClass": 3,"rolloutType": "Brownfield"}}, "relationships": {"containmentBoundaries": {"data": [{"id": "ADA-001","type": "networkBoundary","boundaryType": "ADA"},{"id": "MPS-001","type": "networkBoundary","boundaryType": "MPS"}]},"MDU": {"data": {"id": "LOC212345678901","type": "location"}}}}]}';
        }
        else if(strResponseType == 'TOW'){
            strResponse = '{"transactionId":"aeb8a1007f60271d","ticketOfWorkStatus":"Requested"}';
        }else if(strResponseType == 'LocationOrders'){//Added by Ramya as part of CCT-2247
        	strResponse = sdm_strResponse;
        }  
        // Create a test response
        HttpResponse res = new HttpResponse();
        res.setBody(strResponse);
	if(strResponseType != 'LocationOrders'){//Added by Ramya as part of CCT-2247
    		res.setStatusCode(integer.valueOf(Status));
        } 
        if(strResponseType == 'TOW') res.setHeader('nbn-correlation-id', '0e8cedd0-ad98-11e6-bf2e-47644ada7c0f');
        if(Status==Label.success)
        {
          if(strResponseType == 'TOW') res.setStatusCode(202);
          else res.setStatusCode(200);
        }
        if(Status==Label.failure)
        {
            if(strResponseType == 'TOW' && TOWStatusCode == 400) res.setStatusCode(400);
            else if(strResponseType == 'TOW' && TOWStatusCode == 404) res.setStatusCode(404);
            else if(strResponseType == 'TOW' && TOWStatusCode == 401) res.setStatusCode(401);
            else if(strResponseType == 'TOW' && TOWStatusCode == 500) res.setStatusCode(500);
            else if(strResponseType == 'TOW' && TOWStatusCode == 502) res.setStatusCode(502);
            //Added by Ramya as part of CCT-2247
            else if(strResponseType == 'LocationOrders' && StatusCode == 404) res.setStatusCode(404);
            else if(strResponseType == 'LocationOrders' && StatusCode == 429) res.setStatusCode(429);
            else if(strResponseType == 'LocationOrders' && StatusCode == 500) res.setStatusCode(500);
            //Added by Ramya as part of CCT-2247
            else res.setStatusCode(400);            
            
            if(strResponseType != 'LocationOrders'){
                CalloutException e = (CalloutException)CalloutException.class.newInstance();
                e.setMessage('Unauthorized endpoint, please check Setup->Security->Remote site settings.');
                throw e;
            }else if(strResponseType == 'LocationOrders' && StatusCode == 502){
                CalloutException e = (CalloutException)CalloutException.class.newInstance();
                e.setMessage('Unauthorized endpoint, please check Setup->Security->Remote site settings.');
                throw e;
            }
           
        }
        return res;
    }
}