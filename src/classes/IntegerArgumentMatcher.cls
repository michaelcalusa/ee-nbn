/**
 * Created by alan on 2019-02-26.
 */

@isTest
public class IntegerArgumentMatcher extends ArgumentMatcher {

    private Integer expectedVal;

    public IntegerArgumentMatcher(Integer expectedVal){
        super();
        this.expectedVal = expectedVal;
    }

    public override Boolean isMatch(Object arg){
        try{
            return expectedVal == Integer.valueOf(arg);
        }catch(Exception e){
            return false;
        }
    }

}