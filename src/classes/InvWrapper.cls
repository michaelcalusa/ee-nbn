public Class InvWrapper  implements Comparable
{
    public string TransactionDate{get; set;}
     public static String sortOrder='desc';  
     public static String sortableField='TransactionDate';

    public Integer compareTo(Object compareTo) 
    {
        invwrapper  compWrap = (invwrapper)compareTo;
         if(sortOrder == 'desc'){
          if(sortableField == 'TransactionDate'){
        if (TransactionDate == compWrap.TransactionDate) 
            return 0;
        if (TransactionDate > compWrap.TransactionDate) 
            return 1;
        return -1;    
        }
       }   
       
       return null;     
    }    
}