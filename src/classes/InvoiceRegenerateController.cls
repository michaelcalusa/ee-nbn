/*
Class Description
Creator: Shalini Tripathi
Purpose: This class set the value of Last_Invoice_Regeneration_Date__c field on the basis of status__c value to trigger the workflow for outbound message
Modifications: Added creation of task logic for user story 3235
*/
public class InvoiceRegenerateController {
  public Invoice__c inv {get; set;}
  public Invoice__c inv1 {get; set;}
  //public boolean IsNotRegenerate {get; set;}
  public boolean IsRegenerate {get; set;}
  public String invoiceId;
  String InvReg;
  public InvoiceRegenerateController(ApexPages.StandardController controller) {
    invoiceId = controller.getId();
    inv1 = [select Id, Account__c, Amount__c, Opportunity__c, Solution__c, Unpaid_Balance__c, name, Status__c, Previous_Status__c, Reason_Code__c
            from Invoice__c where id = :invoiceId];
    Map<String, String> mapVariable =  (Map<String, String>)JSON.deserialize(System.label.INVOICE_REGENERATE_SENDOUTBOUND_VARIABLES, Map<String, String>.class);
    if (inv1.Status__c.equalsIgnoreCase(mapVariable.get('validstatus')) || inv1.Status__c.equalsIgnoreCase(mapVariable.get('validstatus2') )) {
      IsRegenerate = true;
    }
  }

  public PageReference pageRedirect() {
    return GlobalUtility.pageRedirect(invoiceId);
  }
  //Method sendoutbound() Created to change the date of Last_Invoice_Regeneration_Date__c to current date
  // Changing date will trigger the workflow on Invoice object
  public PageReference sendoutbound() {
    PageReference page;
    System.debug('invoiceId' + invoiceId);
    Map<String, String> mapVariable =  (Map<String, String>)JSON.deserialize(System.label.INVOICE_REGENERATE_SENDOUTBOUND_VARIABLES, Map<String, String>.class);
    try {
      inv = [select Id, Record_Type_Name__c, Status__c
             from Invoice__c
             where id = :invoiceId];
      // if (inv.Record_Type_Name__c.equalsIgnoreCase(mapVariable.get('recordType'))) {
      if (inv.Status__c.equalsIgnoreCase(mapVariable.get('validstatus')) || inv.Status__c.equalsIgnoreCase(mapVariable.get('validstatus2') )) {
        // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, mapVariable.get('confirmMessage')));
        inv.Last_Invoice_Regeneration_Date__c = datetime.now();
        //Log the Regeneration of Invoice log by creating closed task
        Task newTask = new Task(Description = 'Invoice Regeneration',
                                Priority = 'Normal',
                                Status = 'Closed',
                                Subject = 'Invoice Regeneration request submitted',
                                IsReminderSet = true,
                                Requested_Date__c = System.Now(),
                                Due_Date__c = System.today()
                               );
        newTask.WhatId = inv.id;
        newTask.OwnerId = UserInfo.getUserId();
        insert newTask;
        update inv;
        return GlobalUtility.pageRedirect(invoiceId);
      }
    } catch ( Exception e ) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, mapVariable.get('exceptionMessage')));
      System.debug(e.getStackTraceString() + 'stacktrace');
      String errorMsg = 'Failed to Regenerate in Invoice__c object. ' + '/n' + e.getMessage();
      GlobalUtility.logMessage( 'Error',
                                'InvoiceRegenerateController',
                                'sendoutbound',
                                invoiceId,
                                'invoiceId Id',
                                errorMsg,
                                '',
                                null,
                                null );
    }
    return page;
  }
}