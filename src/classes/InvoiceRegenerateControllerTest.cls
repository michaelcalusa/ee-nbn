/***************************************************************************************************
    Class Name  : InvoiceRegenerateControllerTest
    Class Type  : Test Class
    Version     : 1.0
    Created Date: 09-April-2018
    Function    : This class contains unit test scenarios for InvoiceRegenerateController apex class which handles regenerationof invoice records
    Used in     : None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------
    * Shalini Tripathi            09/04/2018               Created for user story 1887
    * Shalini Tripathi            17/04/2018               Added for user story 3620,3033,3235
****************************************************************************************************/
@istest
public class InvoiceRegenerateControllerTest {
	//sendOutboundTestdata() Method to create testdata
	@testsetup
	public static void sendOutboundTestdata() {
		Account objAccount = TestDataClass.CreateTestAccountData();
		Opportunity testOpty = new Opportunity (Name = 'Test Opp', CloseDate = System.today(), StageName = 'Invoicing', Approving_Status__c = 'Internal Approval Received'
		                                        , Invoicing_Status__c = 'Pending');
		insert testOpty;
		RecordType InvoiceRecType  = [SELECT Id
		                              FROM RecordType
		                              WHERE SobjectType = 'Invoice__c'
		                                      AND DeveloperName = 'Invoice'];
		ObjectCommonSettings__c invSet =
		    new ObjectCommonSettings__c(Name = 'Invoice Statuses', New__c = 'New', Requested__c = 'Requested', Cancelled__c = 'Cancelled', Pending_Approval__c = 'Pending Approval',
		                                Cancellation_Requested__c = 'Cancellation Requested', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval',
		                                Payment_Status_Paid__c = 'Paid', GST_Exclusive__c = 'GST (Exclusive)', GST_Inclusive__c = 'GST (Inclusive)',
		                                Discount_Type_Amount__c = 'Amount', ILI_Type_Cancel_Inv__c = 'Cancel Inv', Rejected__c = 'Rejected', Refer_to_Credit__c = 'Refer to Credit');

		insert invSet;
		//create invoice
		Invoice__c inv = new Invoice__c(Name = 'test inv', Unpaid_Balance__c = 100, Account__c = objAccount.Id, RecordTypeId = InvoiceRecType.Id,  Opportunity__c = testOpty.Id);
		inv.status__c = 'Issued';
		inv.EBS_Number__c = 'To be issued';
		insert inv;
		Invoice__c inv2 = new Invoice__c(Name = 'test inv', Unpaid_Balance__c = 100, Account__c = objAccount.Id, RecordTypeId = InvoiceRecType.Id,  Opportunity__c = testOpty.Id);
		inv2.status__c = 'Requested';
		inv2.EBS_Number__c = 'To be issued';
		insert inv2;
		//create task
		Task newTask = new Task(Description = 'Invoice Regeneration',
		                        Priority = 'Normal',
		                        Status = 'Closed',
		                        Subject = 'Invoice Regeneration request has submitted',
		                        IsReminderSet = true,
		                        Requested_Date__c = System.Now(),
		                        Due_Date__c = System.today()
		                       );
		newTask.WhatId = inv.id;
		insert newTask;
		system.debug('invoice*********8' + inv);


	}
	//test1Postive() Method to test the postive cases of sendoutbound()
	@isTest static void test1Postive() {
		Invoice__c inv1 = [select id, status__c, Last_Invoice_Regeneration_Date__c
		                   from Invoice__c
		                   where status__c = 'Issued'];
		Task taskTest = [select Due_Date__c from task where WhatId = :inv1.id];
		ApexPages.StandardController sc = new ApexPages.StandardController(inv1);
		InvoiceRegenerateController invController = new InvoiceRegenerateController(sc);
		Test.startTest();
		PageReference pageRef = Page.InvoiceRegeneratePage;
		Test.setCurrentPage(pageRef);
		invController.sendoutbound();
		inv1 = [select id, status__c, Last_Invoice_Regeneration_Date__c
		        from Invoice__c
		        where id = :inv1.id];
		System.assertEquals(inv1.Last_Invoice_Regeneration_Date__c.date(), Date.today());
		System.assertEquals(taskTest.Due_Date__c, System.today());
		Test.stopTest();
	}
	//test2Negative() Method to test the negative cases of sendoutbound()
	@isTest static void test2Negative() {
		Invoice__c inv1 = [select id, status__c, Last_Invoice_Regeneration_Date__c
		                   from Invoice__c
		                   where status__c = 'Requested'];
		Map<String, String> mapVariable =  (Map<String, String>)JSON.deserialize(System.label.INVOICE_REGENERATE_SENDOUTBOUND_VARIABLES, Map<String, String>.class);
		ApexPages.StandardController sc = new ApexPages.StandardController(inv1);
		InvoiceRegenerateController invController = new InvoiceRegenerateController(sc);
		Test.startTest();
		PageReference pageRef = Page.InvoiceRegeneratePage;
		Test.setCurrentPage(pageRef);
		invController.sendoutbound();
		invController.pageRedirect();
		system.assertEquals(inv1.Last_Invoice_Regeneration_Date__c, NULL);
		Test.stopTest();
	}
	//test2Negative() Method to test the exceptional cases of sendoutbound()
	@isTest static void test3Negative() {
		Invoice__c inv1 = [select id, status__c
		                   from Invoice__c
		                   where status__c = 'Issued'];
		system.debug('status******' + inv1);
		ApexPages.StandardController sc = new ApexPages.StandardController(inv1);
		system.debug('controller******' + sc);
		InvoiceRegenerateController invController = new InvoiceRegenerateController(sc);
		Test.startTest();
		PageReference pageRef = Page.InvoiceRegeneratePage;
		Test.setCurrentPage(pageRef);
		//deliberately setting invoice ID attribute as null to cover exception block
		invController.invoiceId = null;
		invController.sendoutbound();
		//Due to exception, there  will be one apex error message
		system.assertEquals(ApexPages.getMessages().size(), 1);
		Test.stopTest();
	}
}