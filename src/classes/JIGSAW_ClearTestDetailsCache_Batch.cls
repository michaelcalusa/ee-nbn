/***************************************************************************************************
Class Name:         JIGSAW_ClearTestDetailsCache_Batch
Class Type:         Batch Class With Scheduable interface
Version:            1.0 
Created Date:       March 01, 2019
Function:           This Batch class is for clear the Test manager result records from JIGSAW_Test_Details__c object
Input Parameters:   None 
Output Parameters:  None

Modification Log:
* Developer          Date             Description
* Rohit M            March 01, 2019   CUSTSA-28521 :Added the logic to make the deletion of records dynamic from JIGSAW_Test_Details__c based on integration type.
* --------------------------------------------------------------------------------------------------                 
* 
****************************************************************************************************/ 

global class JIGSAW_ClearTestDetailsCache_Batch implements Database.Batchable<sObject>, Schedulable{
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        JIGSAW_Cache_Config__mdt intRecord =  [SELECT Jigsaw_Integration_Type__c,Jigsaw_Number_Of_Days__c  FROM JIGSAW_Cache_Config__mdt where Label = 'TestDiagnostics' AND Jigsaw_Integration_Type__c = 'Test and Diagnostics' Limit 1];
        if(intRecord != null){
            String qry = 'SELECT Id FROM JIGSAW_Test_Details__c  WHERE CreatedDate < LAST_N_DAYS:'+intRecord.Jigsaw_Number_Of_Days__c.intValue();
            system.debug('query is***** ' + qry);
            return Database.getQuerylocator(qry);
        }
        return null;
    } 
    
    global void execute(Database.BatchableContext BC, list<sObject> scope){
        delete scope;
    }
    
    global void finish(Database.BatchableContext BC){
    }
    
    global void execute(SchedulableContext sc){
        // Implement any logic to be scheduled
        // We now call the batch class to be scheduled
        JIGSAW_ClearTestDetailsCache_Batch b = new JIGSAW_ClearTestDetailsCache_Batch ();
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b, 200);
    }
}