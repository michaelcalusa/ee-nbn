@isTest
public class JIGSAW_ClearTestDetailsCache_Batch_Test {

    // Testmethod for checking the cache object is clear with records older than 7 days.
    static testMethod void method1() {
        Datetime olderThan7 = Datetime.now().addDays(-10);
        List < JIGSAW_Test_Details__c > lstCache = new List < JIGSAW_Test_Details__c > ();
        for (Integer i = 0; i < 10; i++) {
            JIGSAW_Test_Details__c objCache = new JIGSAW_Test_Details__c();
            objCache.worfklowReferenceId__c = 'Test worfklowReferenceId ' + i;
            lstCache.add(objCache);
        }

        insert lstCache;
        for (JIGSAW_Test_Details__c obj: lstCache) {
            Test.setCreatedDate(obj.Id, olderThan7);
        }

        Test.startTest();
        JIGSAW_ClearTestDetailsCache_Batch obj = new JIGSAW_ClearTestDetailsCache_Batch();
        DataBase.executeBatch(obj);
        // Schedule the test job
        String jobId = System.schedule('testBasicScheduledApex', '0 0 0 3 9 ? 2022', new JIGSAW_ClearTestDetailsCache_Batch());
        Test.stopTest();

        list<JIGSAW_Test_Details__c> countRecords = [SELECT Id FROM JIGSAW_Test_Details__c];
        System.assertEquals(0, countRecords.size());

    }
}