/*------------------------------------------------------------
Author:        Gaurav Saraswat
Company:       Contractor
Description:   Constant Class for JIGSAW
History
<Date>      <Author>     <Description>
15/02/2019    GS        Added Constants for Operator Actions Visibility    */
public class JIGSAW_Constants {
    
    public final Static String LITERAL_METHODNAME = 'MethodName';
    public final Static String LITERAL_ADDNOTEOPACTION = 'AddNote_OperatorAction';
    public final Static String LITERAN_ASSIGNANDACCEPT = 'AssignAndAccept_OperatorAction';
    public final static String LITERAL_TESTMANAGER = 'Test_Manager_Link_Module';
    public final Static String LITERAL_UPDATEOPSCAT = 'Update_Op_Cats';
    
}