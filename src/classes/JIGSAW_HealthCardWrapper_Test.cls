@isTest
private class JIGSAW_HealthCardWrapper_Test{
	/*
    public static String jsonHealthCard(){ 
		// Exlicit Change
        String json =		'{'+
		'	"serviceId": "AVC000000000010",'+
		'	"notes": [],'+
		'	"transactionTimestamp": "2019-02-06T02:36:42.723Z",'+
		'	"basedOnDataUpdatedEvery15mins": {'+
		'		"modemOperationalStateForPast24Hours": [{'+
		'				"value": "up",'+
		'				"timestamp": "2018-08-13T13:42:47.798Z"'+
		'			},'+
		'			{'+
		'				"value": "up",'+
		'				"timestamp": "2018-08-13T13:27:47.798Z"'+
		'			},'+
		'			{'+
		'				"value": "up",'+
		'				"timestamp": "2018-08-13T13:12:47.798Z"'+
		'			}'+
		'		],'+
		'		"modemOperationalStateForPast48Hours": [{'+
		'				"value": "up",'+
		'				"timestamp": "2018-08-13T13:42:47.798Z"'+
		'			},'+
		'			{'+
		'				"value": "up",'+
		'				"timestamp": "2018-08-13T13:27:47.798Z"'+
		'			},'+
		'			{'+
		'				"value": "up",'+
		'				"timestamp": "2018-08-13T13:12:47.798Z"'+
		'			}'+
		'		],'+
		'		"sumUnavailableSecondCountersForPast24Hours": 185,'+
		'		"unavailableSecondCountersForPast24Hours": [{'+
		'				"value": 60,'+
		'				"timestamp": "2018-08-13T13:42:47.798Z"'+
		'			},'+
		'			{'+
		''+
		'				"value": 60,'+
		'				"timestamp": "2018-08-13T13:27:47.798Z"'+
		'			},'+
		'			{'+
		''+
		'				"value": 65,'+
		'				"timestamp": "2018-08-13T13:12:47.798Z"'+
		'			}'+
		'		],'+
		'		"sumSeverelyErroredSecondsDownstreamForPast24Hours": 215,'+
		'		"sumSeverelyErroredSecondsUpstreamForPast24Hours": 95,'+
		'		"severelyErroredSeconds": {'+
		'			"downstreamForPast24Hours": [{'+
		'					"timestamp": "2018-08-13T13:42:47.798Z",'+
		'					"value": 70'+
		'				},'+
		'				{'+
		'					"timestamp": "2018-08-13T13:27:47.798Z",'+
		'					"value": 70'+
		'				},'+
		'				{'+
		'					"timestamp": "2018-08-13T13:12:47.798Z",'+
		'					"value": 75'+
		'				}'+
		'			],'+
		'			"upstreamForPast24Hours": [{'+
		'					"timestamp": "2018-08-13T13:42:47.798Z",'+
		'					"value": 30'+
		'				},'+
		'				{'+
		'					"timestamp": "2018-08-13T13:27:47.798Z",'+
		'					"value": 30'+
		'				},'+
		'				{'+
		'					"timestamp": "2018-08-13T13:12:47.798Z",'+
		'					"value": 35'+
		'				}'+
		'			]'+
		'		},'+
		'		"correctedAndr": {'+
		'			"downstreamForPast24Hours": [{'+
		'					"timestamp": "2018-08-13T13:42:47.798Z",'+
		'					"value": 40562'+
		'				}, {'+
		'					"timestamp": "2018-08-13T13:27:47.798Z",'+
		'					"value": 42153'+
		'				},'+
		'				{'+
		'					"timestamp": "2018-08-13T13:12:47.798Z",'+
		'					"value": 52325'+
		'				}'+
		'			],'+
		'			"upstreamForPast24Hours": [{'+
		'					"timestamp": "2018-08-13T13:42:47.798Z",'+
		'					"value": 12156'+
		'				}, {'+
		'					"timestamp": "2018-08-13T13:27:47.798Z",'+
		'					"value": 13256'+
		'				},'+
		'				{'+
		'					"timestamp": "2018-08-13T13:12:47.798Z",'+
		'					"value": 12025'+
		'				}'+
		'			]'+
		'		},'+
		'		"macAddressesForPast48Hours": [{'+
		'				"value": "0A-C3-52-C1-81-C0",'+
		'				"timestamp": "2018-08-13T13:42:47.798Z"'+
		'			},'+
		'			{'+
		'				"value": "3C-A3-B2-C2-12-AB",'+
		'				"timestamp": "2018-08-11T13:56:07.798Z"'+
		'			}'+
		'		],'+
		'		"dropouts": {'+
		'			"dropoutFault": true,'+
		'			"dropout24HoursFault": true,'+
		'			"dropoutsPast24Hours": 116,'+
		'			"sumReInitialisationCountersForPast24Hours": 150,'+
		'			"sumLossOfPowerCountersForPast24Hours": 34,'+
		'			"dropout48HoursFault": true,'+
		'			"dropoutsPast48Hours": 116,'+
		'			"sumReInitialisationCountersForPast48Hours": 150,'+
		'			"sumLossOfPowerCountersForPast48Hours": 34,'+
		'			"sumUnavailableSecondCountersForPast48Hours": 185,'+
		'			"sumSeverelyErroredSecondsDownstreamForPast48Hours": 215,'+
		'			"sumSeverelyErroredSecondsUpstreamForPast48Hours": 95,'+
		'			"reInitialisationCounterPast48Hours": [{'+
		'					"value": 50,'+
		'					"timestamp": "2018-08-13T13:42:47.798Z"'+
		'				},'+
		'				{'+
		'					"value": 50,'+
		'					"timestamp": "2018-08-13T13:27:47.798Z"'+
		'				},'+
		'				{'+
		'					"value": 50,'+
		'					"timestamp": "2018-08-13T13:12:47.798Z"'+
		'				}'+
		'			],'+
		'			"lossOfPowerCounterPast48Hours": [{'+
		'					"value": 9,'+
		'					"timestamp": "2018-08-13T13:42:47.798Z"'+
		'				},'+
		'				{'+
		'					"value": 10,'+
		'					"timestamp": "2018-08-13T13:27:47.798Z"'+
		'				},'+
		'				{'+
		'					"value": 15,'+
		'					"timestamp": "2018-08-13T13:12:47.798Z"'+
		'				}'+
		'			]'+
		'		},'+
		'		"loopAttenuation": {'+
		'			"loopAttenuationWarning": true,'+
		'			"loopAttenuationVariationForPast24Hours": 15,'+
		'			"loopAttenuationUnitType": "dB",'+
		'			"loopAttenuationDownstreamForPast24Hours": [{'+
		'					"value": 10,'+
		'					"timestamp": "2018-08-13T13:42:47.798Z"'+
		'				},'+
		'				{'+
		'					"value": 20,'+
		'					"timestamp": "2018-08-13T13:27:47.798Z"'+
		'				},'+
		'				{'+
		'					"value": 25,'+
		'					"timestamp": "2018-08-13T13:12:47.798Z"'+
		'				}'+
		'			]'+
		'		}'+
		'	},'+
		'	"basedOnDataUpdatedEvery24Hours": {'+
		'		"cpeListFromPast30Days": [{'+
		'				"make": "Huawei",'+
		'				"serialNumber": "AA:AA:AA:AA:AA:AA",'+
		'				"model": "Hi9jguj",'+
		'				"firmwareVersion": "2.3.565.1152212",'+
		'				"timestamp": "2018-08-12T13:41:07.798Z"'+
		'			},'+
		'			{'+
		'				"make": "Apple",'+
		'				"serialNumber": "AA:AA:AA:AA:AA:AA",'+
		'				"model": "AABBVENDORID",'+
		'				"firmwareVersion": "2.3.565.1152212",'+
		'				"timestamp": "2018-08-11T13:41:07.798Z"'+
		'			}'+
		'		],'+
		'		"failedReInitialisations": 3,'+
		'		"failedReInitialisationsForPast48Hours": 6,'+
		'		"failedReInitialisationsTimestamp": "2018-08-13T13:41:07.798Z",'+
		'		"actualRateAdaptationModeDownstream": "Automatic at Startup",'+
		'		"actualRateAdaptationModeUpstream": "Operator Controlled",'+
		'		"actualRateAdaptationModeTimestamp": "2018-08-13T13:41:07.798Z",'+
		'		"noiseMarginDownstream": 2379,'+
		'		"noiseMarginUpstream": 2379,'+
		'		"noiseMarginTimestamp": "2018-08-13T13:41:07.798Z",'+
		'		"noiseMarginUnitType": "dB",'+
		'		"outputPowerDownstream": 9999,'+
		'		"outputPowerUpstream": 100,'+
		'		"outputPowerTimestamp": "2018-08-13T13:41:07.798Z",'+
		'		"outputPowerUnitType": "dBm",'+
		'		"impulseNoiseProtectionDownstream": 123,'+
		'		"impulseNoiseProtectionUpstream": 27071,'+
		'		"impulseNoiseProtectionTimestamp": "2018-08-13T13:41:07.798Z",'+
		'		"impulseNoiseProtectionUnitType": "symbols"'+
		'	},'+
		'	"healthCardSummary": {'+
		'		"stabilityRagStatus": "Red",'+
		'		"connectivityRagStatus": "Green",'+
		'		"serviceStateLastChangeTimestamp": "2019-01-31T00:00:52Z",'+
		'		"serviceState": "Up",'+
		'		"serviceStateTimestamp": "2018-08-13T13:42:47.798Z",'+
		'		"correctedAttainableRateDownstream": 40562,'+
		'		"correctedAttainableRateUpstream": 12156,'+
		'		"correctedAttainableRateTimestamp": "2018-08-13T13:42:47.798Z",'+
		'		"dropoutFault": true,'+
		'		"dropoutFaultTimestamp": "2018-08-13T13:42:47.798Z",'+
		'		"dropout24HoursFault": true,'+
		'		"dropout48HoursFault": true,'+
		'		"dropoutsPast24Hours": 116,'+
		'		"dropoutsPast48Hours": 116,'+
		'		"sumReInitialisationCountersForPast24Hours": 150,'+
		'		"sumReInitialisationCountersForPast48Hours": 150,'+
		'		"reinitialisationCountersTimestamp ": "2018-08-13T13:42:47.798Z",'+
		'		"sumLossOfPowerCountersForPast24Hours": 34,'+
		'		"sumLossOfPowerCountersForPast48Hours": 34,'+
		'		"lossOfPowerCountersTimestamp": "2018-08-13T13:42:47.798Z",'+
		'		"sumUnavailableSecondCountersForPast24Hours": 185,'+
		'		"sumUnavailableSecondCountersForPast48Hours": 185,'+
		'		"unavailableSecondCountersTimestamp": "2018-08-13T13:42:47.798Z",'+
		'		"sumSeverelyErroredSecondsDownstreamForPast48Hours": 215,'+
		'		"sumSeverelyErroredSecondsUpstreamForPast48Hours": 95,'+
		'		"sumSeverelyErroredSecondsUpstreamForPast24Hours": 95,'+
		'		"sumSeverelyErroredSecondsDownstreamForPast24Hours": 215,'+
		'		"severelyErroredSecondsTimestamp": "2018-08-13T13:42:47.798Z",'+
		'		"loopAttenuationWarning": true,'+
		'		"loopAttenuationVariationForPast24Hours": "15dB",'+
		'		"loopAttenuationDownstream": "10dB",'+
		'		"loopAttenuationDownstreamTimestamp": "2018-08-13T13:42:47.798Z",'+
		'		"sumFailedReinitialisationsForPast24Hours": 3,'+
		'		"sumFailedReinitialisationsForPast48Hours": 6,'+
		'		"failedReinitialisationsTimestamp": "2018-08-13T13:42:47.798Z",'+
		'		"noiseMarginDownstream": "2379dB",'+
		'		"noiseMarginUpstream": "2379dB",'+
		'		"noiseMarginTimestamp": "2018-08-13T13:41:07.798Z",'+
		'		"actualRateAdaptationModeUpstream": "Operator Controlled",'+
		'		"actualRateAdaptationModeDownstream": "Automatic at Startup",'+
		'		"actualRateAdaptationModeTimestamp": "2018-08-13T13:41:07.798Z",'+
		'		"outputPowerUpstream": "100dBm",'+
		'		"outputPowerDownstream": "9999dBm",'+
		'		"outputPowerTimestamp": "2018-08-13T13:41:07.798Z",'+
		'		"cpeMake": "Huawei",'+
		'		"cpeModel": "Hi9jguj",'+
		'		"cpeSerialNumber": "B392KD89239KD93L",'+
		'		"cpeFirmwareVersion": "2.3.565.1152212",'+
		'		"cpeCompatibility": "Compatible",'+
		'		"cpeTimestamp": "2018-08-12T13:41:07.798Z",'+
		'		"cpeType": "Netcomm",'+
		'		"cpeMacAddress": "0A:C3:52:C1:81:C0",'+
		'		"cpeMacAddressTimestamp": "2018-08-13T13:42:47.798Z",'+
		'		"cpeTerminationType": "UNKNOWN",'+
		'		"cpeTerminationTypeTimestamp": "2019-02-06T02:14:11.673Z",'+
		'		"numOfCpeChangesInLast30Days": 1,'+
		'		"cpeChangeInLast30Days": true,'+
		'		"potsServicePresent": "YES",'+
		'		"potsServicePresentTimestamp": "2019-02-06T02:14:11.548Z",'+
		'		"foreignDcAbVoltage": "-0.1V",'+
		'		"foreignDcAbVoltageRagStatus": "Green",'+
		'		"foreignDcAEarthVoltage": "-0.2V",'+
		'		"foreignDcAEarthVoltageRagStatus": "Green",'+
		'		"foreignDcBEarthVoltage": "-0.1V",'+
		'		"foreignDcBEarthVoltageRagStatus": "Green",'+
		'		"foreignVoltageTimestamp": "2019-02-06T02:14:11.548Z",'+
		'		"foreignAcAbVoltage": "0.0V",'+
		'		"foreignAcAbVoltageRagStatus": "Green",'+
		'		"foreignAcAEarthVoltage": "0.6V",'+
		'		"foreignAcAEarthVoltageRagStatus": "Green",'+
		'		"foreignAcBEarthVoltage": "0.6V",'+
		'		"foreignAcBEarthVoltageRagStatus": "Green",'+
		'		"abResistance": "5000+kohms",'+
		'		"abResistanceRagStatus": "Green",'+
		'		"aEarthResistance": "2854.7kohms",'+
		'		"aEarthResistanceRagStatus": "Red",'+
		'		"bEarthResistance": "5000+kohms",'+
		'		"bEarthResistanceRagStatus": "Green",'+
		'		"resistanceTimestamp": "2019-02-06T02:14:11.548Z",'+
		'		"abCapacitance": "58.5nF",'+
		'		"aEarthCapacitance": "63.3nF",'+
		'		"bEarthCapacitance": "51.0nF",'+
		'		"capacitiveBalance": "81%",'+
		'		"capacitiveBalanceRagStatus": "Green",'+
		'		"capacitanceTimestamp": "2019-02-06T02:14:11.548Z",'+
		'		"lineLengthFromSelt": "640m",'+
		'		"lineLengthAccuracyFromSelt": "[+/-] 70m",'+
		'		"lineLengthFrom375": "620m",'+
		'		"lineLengthTimestamp": "2019-02-15T02:22:01.599Z",'+
		'		"tooltips": {'+
		'			"stabilityRagStatus": "Stability Red/Amber/Green Status for the given AVC",'+
		'			"connectivityRagStatus": "Connectivity Red/Amber/Green Status for the given AVC",'+
		'			"serviceState": "Latest modem operational state. Indicates if the modem is up or down",'+
		'			"serviceStateLastChangeTimestamp": "Some Text About This Field",'+
		'			"sumUnavailableSecondCounters": "Some Text About This Field",'+
		'			"sumSeverelyErroredSeconds": "Some Text About This Field",'+
		'			"correctedAttainableRate": "Some Text About This Field",'+
		'			"calculated375Attenuation": "Some Text About This Field",'+
		'			"dropoutFault": "Some Text About This Field",'+
		'			"sumReInitialisationCounters": "Some Text About This Field",'+
		'			"sumLossOfPowerCounters": "Some Text About This Field",'+
		'			"loopAttenuation": "Some Text About This Field",'+
		'			"sumFailedReinitialisations": "Some Text About This Field",'+
		'			"noiseMargin": "Some Text About This Field",'+
		'			"actualRateAdaptationMode": "Some Text About This Field",'+
		'			"outputPower": "Some Text About This Field",'+
		'			"cpeMake": "Some Text About This Field",'+
		'			"cpeModel": "Some Text About This Field",'+
		'			"cpeSerialNumber": "Some Text About This Field",'+
		'			"cpeFirmwareVersion": "Some Text About This Field",'+
		'			"cpeMacAddress": "Some Text About This Field",'+
		'			"cpeCompatibility": "Some Text About This Field",'+
		'			"cpeType": "Some Text About This Field",'+
		'			"cpeTerminationType": "Some Text About This Field",'+
		'			"potsServicePresent": "Some Text About This Field",'+
		'			"foreignDcVoltage": "Some Text About This Field",'+
		'			"foreignAcVoltage": "Some Text About This Field",'+
		'			"resistance": "Some Text About This Field",'+
		'			"capacitance": "Some Text About This Field",'+
		'			"capacitiveBalance": "Some Text About This Field",'+
		'			"lineLengthFromSelt": "Some Text About This Field",'+
		'			"lineLengthFrom375": "Some Text About This Field"'+
		'		}'+
		'	},'+
		'	"cpeListFromPast30Days": [{'+
		'			"make": "Huawei",'+
		'			"serialNumber": "B392KD89239KD93L",'+
		'			"model": "Hi9jguj",'+
		'			"firmwareVersion": "2.3.565.1152212",'+
		'			"compatibility": "Incompatible",'+
		'			"timestamp": "2018-08-12T13:41:07.798Z"'+
		'		},'+
		'		{'+
		'			"make": "Apple",'+
		'			"serialNumber": "ACL93KADWXC",'+
		'			"model": "AABBVENDORID",'+
		'			"firmwareVersion": "2.3.565.1152212",'+
		'			"compatibility": "Compatible",'+
		'			"timestamp": "2018-08-11T13:41:07.798Z"'+
		'		}'+
		'	],'+
		'	"macAddressesForPast48Hours": [{'+
		'			"value": "0A-C3-52-C1-81-C0",'+
		'			"timestamp": "2018-08-13T13:42:47.798Z"'+
		'		},'+
		'		{'+
		'			"value": "3C-A3-B2-C2-12-AB",'+
		'			"timestamp": "2018-08-11T13:56:07.798Z"'+
		'		}'+
		'	]'+
		'}';
		return json;
	}
    
    @isTest static void testMethod1() {
        Test.startTest();
        String jsonHealthCard = jsonHealthCard();
        JIGSAW_HealthCardWrapper obj = JIGSAW_HealthCardWrapper.parse(jsonHealthCard);
		System.assert(obj != null);
        
        
        Test.stopTest();
    }
    */
	
}