/***************************************************************************************************
Class Name:         JIGSAW_HealthCard_CX
Created Date:       30 August 2018
Created By:         Sunil Gupta
Description:        Lightning Component Controller for JIGSAW_HealthCard Epic# 14298

Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Sunil Gupta      03/09/2018        
****************************************************************************************************/
public class JIGSAW_HealthCard_CX {



    // Calling from Lightning component JIGSAW_HealthCard
    @AuraEnabled
    public static HealthCardResponse requestHealthCard(String incidentId, String snapShotId) {
        HealthCardResponse objResponse = new HealthCardResponse();
        String techType;
        String avcId;
        String accessSeekerId;

        try {
           
            objResponse.refreshTimeInMinutes = Integer.valueOf(System.Label.JIGSAW_HealthCard_Refresh_Health_Card);
            snapshotObject snapObject = getSnapshotMap(incidentId);
            Map < Id, NBNIntegrationCache__c > mapSnapshots = snapObject.mapSnapshots;
            objResponse.snapShotMap = snapObject.snapShotMap;

            // If SnapShot Id Exists.
            if (!String.isBlank(snapShotId) && mapSnapshots.containsKey(snapShotId.split('###')[0])  && mapSnapshots.containsKey(snapShotId.split('###')[1])) {
                for(String snapId : snapShotId.split('###')){
                    if( mapSnapshots.get(snapId).IntegrationType__c == 'ServiceHealthCard'){
                        objResponse.healthCardPayload = mapSnapshots.get(snapId).JSONPayload__c;
                    }else if(mapSnapshots.get(snapId).IntegrationType__c == 'SpeedAssurance'){
                        objResponse.speedPayload = mapSnapshots.get(snapId).JSONPayload__c;
                    }
                }
            }

            if (String.isBlank(snapShotId)) {
                // Query other attributs of Incident.
                List < Incident_Management__c > lstIM = [SELECT Id, AVC_Id__c, Prod_Cat__c, Access_Seeker_ID__c FROM Incident_Management__c WHERE Id =: incidentId];
                avcId = lstIM.get(0).AVC_Id__c;
                accessSeekerId = lstIM.get(0).Access_Seeker_ID__c;


                if (lstIM.get(0).Prod_Cat__c == 'NCAS-FTTN') {
                    techType = 'FTTN';
                } else if (lstIM.get(0).Prod_Cat__c == 'NCAS-FTTB') {
                    techType = 'FTTB';
                }

                if (String.isBlank(avcId) || avcId == 'null') {
                    objResponse.errorDesc = 'AVC Id is not present';
                    objResponse.errorCode = 'UNKNOWN';
                    return objResponse;
                }

                if (String.isBlank(accessSeekerId)) {
                    objResponse.errorDesc = 'Access Seeker Id is not present';
                    objResponse.errorCode = 'UNKNOWN';
                    return objResponse;
                }

                // Health Card API:
                HttpResponse httpResponse = new HttpResponse();
                httpResponse = integrationUtility.getServiceHealthCard(avcId);

                if (httpResponse.getStatusCode() == 200 && !String.isBlank(httpResponse.getBody())) {
                    String jsonString = httpResponse.getBody().replaceAll('\n', '').replaceAll('\r', '');
                    System.debug('@@@ DO NOT REMOVE THIS DEBUG LINE: This is Health Card API Response:: ' + jsonString);
                    //JIGSAW_HealthCardWrapper objWrapper = (JIGSAW_HealthCardWrapper)JSON.deserialize(jsonString, JIGSAW_HealthCardWrapper.Class);
                    objResponse.healthCardPayload = jsonString;

                    objResponse.connectivity = 'na_grey'; // usimng this for calling speed assurance


                } else {
                    objResponse.errorDesc = 'Health Card API: ' + httpResponse.getStatus();
                    objResponse.errorCode = String.valueOf(httpResponse.getStatusCode());
                    objResponse.addValues('Error', 'Health Card API: A technical error has occurred during the processing of the request.');
                }
            }






            // Connectivity Status
            String connectivityRagStatus;
            JSONParser parser = JSON.createParser(objResponse.healthCardPayload);
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    if (parser.getText() == 'healthCardSummary') {
                        while (parser.nextToken() != null) {
                            if (parser.getText() == 'connectivityRagStatus') {
                                parser.nextToken();
                                connectivityRagStatus = parser.getText();
                                break;
                            }
                        }
                    }
                }
            }

            System.debug('@@@' + connectivityRagStatus);
            if (!String.isEmpty(connectivityRagStatus)) {
                objResponse.connectivity = connectivityRagStatus;
            }




            // Get Notes
            Map < String, Object > deserializedJsonRecord = (Map < String, Object > ) JSON.deserializeUntyped(objResponse.healthCardPayload);
            Object objNotes = (Object) deserializedJsonRecord.get('notes');
            if (objNotes != null) {
                List < cls_note > lstNotes = (List < cls_note > ) JSON.deserialize(JSON.serialize(objNotes), List < cls_note > .class);
                for (cls_note note: lstNotes) {
                    if (!String.isEmpty(note.type) && !String.isEmpty(note.description)) {
                        objResponse.addValues(note.type, note.description);
                    }
                }
            }


        } catch (Exception ex) {
            System.debug('@@@' + ' ' + ex.getMessage() + ' ' + ex.getStackTraceString());
            objResponse.errorDesc = 'Health Card API: A technical error has occurred during the processing of the request.';
            objResponse.errorCode = 'UNKNOWN';
            objResponse.addValues('Error', 'Health Card API: A technical error has occurred during the processing of the request.');
        }
        system.debug('@@@techType' + techType);
        if (techType != 'FTTN' && techType != 'FTTB') {
            system.debug('@@@### Speed assurance API is not available for Incidents other than FTTN & FTTB');
        }
        if (objResponse.connectivity == 'na_grey') {
            system.debug('@@@### Not calling Speed assurance API when Connectivity is NA.');
        }


        // Traffic Light API:
        if (String.isBlank(snapShotId) && (techType == 'FTTN' || techType == 'FTTB') && (objResponse.connectivity == 'Green' || objResponse.connectivity == 'Red')) {
            try {
                HttpResponse responseTL = new HttpResponse();
                responseTL = integrationUtility.getSpeedPerformance(avcId, accessSeekerId);
                if (responseTL.getStatusCode() == 200 && !String.isBlank(responseTL.getBody())) {
                    String jsonString = responseTL.getBody().replaceAll('\n', '').replaceAll('\r', '');
                    jsonString = jsonString.replaceFirst('exception', 'exception_x');
                    System.debug('@@@ Speed Assurance API Response:: ' + jsonString);
                    objResponse.speedPayload = jsonString;
                }
                // When 404 then display the message from response body as per requirement (see spacs)
                else if (responseTL.getStatusCode() == 404 && !String.isBlank(responseTL.getBody())) {
                    String jsonString = responseTL.getBody().replaceAll('\n', '').replaceAll('\r', '');
                    jsonString = jsonString.replaceFirst('exception', 'exception_x');
                    System.debug('@@@ Speed Assurance API Response:: ' + jsonString);
                    JSONParser parser = JSON.createParser(jsonString);
                    while (parser.nextToken() != null) {
                        if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                            if (parser.getText() == 'message') {
                                parser.nextToken();
                                objResponse.addValues('Error', parser.getText());
                            }
                        }
                    }
                    objResponse.speedPayload = jsonString;
                } else {
                    // Display generic message if other than 200 & 404
                    System.debug('@@@');
                    objResponse.addValues('Error', 'Speed Assurance API: A technical error has occurred during the processing of the request.');
                }
            } catch (Exception ex) {
                objResponse.addValues('Error', 'Speed Assurance API: A technical error has occurred during the processing of the request.');
                System.debug('@@@' + ' ' + ex.getMessage() + ' ' + ex.getStackTraceString());
            }
        }



        return objResponse;
    }


    public class cls_note {
        public String id; //000004
        public String type; //Info
        public String description; //No recently executed LSD test found for the past 24 hours for the given AVC ID - service state, noise margins and cpe type data reflective of telemetry-based data only
    }
    // Wrapper class for result
    public class HealthCardResponse {
        @AuraEnabled public Map < String, String > snapShotMap {
            get;
            set;
        }
        @AuraEnabled public String healthCardPayload {
            get;
            set;
        }
        @AuraEnabled public String speedPayload {
            get;
            set;
        }
        @AuraEnabled public String connectivity {
            get;
            set;
        }
        @AuraEnabled public String errorDesc {
            get;
            set;
        }
        @AuraEnabled public String errorCode {
            get;
            set;
        }
        @AuraEnabled public Integer refreshTimeInMinutes {
            get;
            set;
        }

        @AuraEnabled public Map < String, String > mapErrorStrips {
            get;
            set;
        }
        private Integer iErrorStrip = 0;
        public void addValues(String key, String val) {
            iErrorStrip = iErrorStrip + 1;
            Map < String, String > m;
            if (mapErrorStrips == null) {
                m = new Map < String, String > ();
            } else {
                m = mapErrorStrips;
            }
            m.put(key + ':' + iErrorStrip, val);

            mapErrorStrips = m;
        }
    }

    @AuraEnabled
    public static snapshotObject saveSnapshotAction(String healthCardPayload, String speedPayload, String recordId) {
        
        snapshotObject returnObject = new snapshotObject();
        
        if (String.isNotBlank(recordId) && String.isNotBlank(healthCardPayload) && String.isNotBlank(speedPayload)) {

            try {
                list < NBNIntegrationCache__c > snapshotList = new list < NBNIntegrationCache__c > ();

                NBNIntegrationCache__c healthCardSnapshot = new NBNIntegrationCache__c();
                healthCardSnapshot.JSONPayload__c = healthCardPayload;
                healthCardSnapshot.IntegrationType__c = 'ServiceHealthCard';
                healthCardSnapshot.Transaction_Id__c = recordId;

                snapshotList.add(healthCardSnapshot);

                NBNIntegrationCache__c speedSnapshot = new NBNIntegrationCache__c();
                speedSnapshot.JSONPayload__c = speedPayload;
                speedSnapshot.IntegrationType__c = 'SpeedAssurance';
                speedSnapshot.Transaction_Id__c = recordId;

                snapshotList.add(speedSnapshot);

                insert(snapshotList);

                //create feed item for Recent History module
                FeedItem feedObj = new FeedItem();
                feedObj.body = System.label.JIGSAW_HealthCardSnapshotFeedText;
                feedObj.NetworkScope = 'AllNetworks';
                feedObj.ParentId = recordId;
                feedObj.Status = 'Published';
                feedObj.Visibility = 'InternalUsers';
                feedObj.Type = 'TextPost';
                insert feedObj;
                
                returnObject = getSnapshotMap(recordId);
                returnObject.errorOccured = false;
                
                return returnObject;
            } catch (Exception ex) {
                system.debug('Exception :: ' + ex.getStackTraceString());
                GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME, 'JIGSAW_HealthCard_CX', 'saveSnapshotAction', recordId, '', Ex.getMessage(), Ex.getStackTraceString(), Ex, 0);
                returnObject.errorOccured = true;
                return returnObject;
            }
        }
        
        returnObject.errorOccured = true;
        return returnObject;
    }
    
    public class snapshotObject{
        @AuraEnabled public Map < Id, NBNIntegrationCache__c > mapSnapshots {
            get;
            set;
        }
        
        @AuraEnabled public Map < String, String > snapShotMap {
            get;
            set;
        }
        
        @AuraEnabled public Boolean errorOccured {
            get;
            set;
        }
    }
    
    public static snapshotObject getSnapshotMap (String incidentId){
        
        snapshotObject returnObject = new snapshotObject();
        
        // Query all existing snapshots
        Map < Id, NBNIntegrationCache__c > mapSnapshots = new Map < Id, NBNIntegrationCache__c > ([SELECT Id, CreatedDate, CreatedBy.Name, SystemModstamp,
            JSONPayload__c,Transaction_Id__c,IntegrationType__c
            FROM NBNIntegrationCache__c
            WHERE(IntegrationType__c = 'ServiceHealthCard'
                OR IntegrationType__c = 'SpeedAssurance')
            AND Transaction_Id__c =: incidentId 
            ORDER By CreatedDate DESC
        ]);


        Map < String , String > snapShotMap = new Map < String, String > ();
        for (NBNIntegrationCache__c objCache: mapSnapshots.values()) {
            String mapKey = objCache.CreatedDate.format('dd/MM/yyyy HH:mm') + ' by ' + objCache.CreatedBy.Name;
            if(snapShotMap.containsKey(mapKey)){
                String concatId = snapShotMap.get(mapKey)+'###'+objCache.Id;
                 snapShotMap.put(mapKey, concatId);
            }else{
                snapShotMap.put(mapKey, objCache.Id);
            }
            
        }
        
        returnObject.mapSnapshots = mapSnapshots;
        returnObject.snapShotMap = snapShotMap;
        
        return returnObject;
        
    }
}