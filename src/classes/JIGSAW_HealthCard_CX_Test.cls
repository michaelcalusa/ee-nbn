@isTest
private class JIGSAW_HealthCard_CX_Test{
    private static StaticResource resource = [SELECT Body FROM StaticResource WHERE Name ='Jigsaw_HealthCard_1_2_0']; 
    private static Blob listOfJsons = resource.Body;
    private static List<String> listOfrecords = listOfJsons.toString().split(';');
    
    private static String speed_1 = listOfrecords.get(0);
    private static String speed_2 = listOfrecords.get(1);
    private static String speed_3 = listOfrecords.get(2);
    
    private static String healthCard_1 = listOfrecords.get(3);
    private static String healthCard_2 = listOfrecords.get(4);
    private static String healthCard_3 = listOfrecords.get(5);
    
    
    // Test Data 
    @testSetup static void setup() {
        // create custom setting records
        List<SObject> lstToInsert = new List<SObject>();
        
        NBNIntegrationInputs__c objCS1 = new NBNIntegrationInputs__c();        
        objCS1.Name = 'HealthCard';
        objCS1.Call_Out_URL__c = 'callout:NBNAPPGATEWAY_RSVT';
        objCS1.EndpointURL__c='/rest/services/v1/avcs/service-health-card/';
        objCS1.AccessSeekerId__c = 'testId';
        objCS1.Call_TimeOut_In_MilliSeconds__c = '60000';
        objCS1.NBN_Environment_Override__c='PSVT';
        objCS1.nbn_Role__c='NBN';
        objCS1.Service_Name__c='test serviec';
        objCS1.Token__c='8e73b7e33b22ad58';
        objCS1.XNBNBusinessChannel__c='Salesforce';
        objCS1.UI_TimeOut__c = 6000;
        objCS1.Header_Keys__c = 'nbn-correlation-id';     
        lstToInsert.add(objCS1);
        
        NBNIntegrationInputs__c objCS2 = new NBNIntegrationInputs__c();        
        objCS2.Name = 'SpeedAssurance';
        objCS2.Call_Out_URL__c = 'callout:NBNAPPGATEWAY_RSVT';
        objCS2.EndpointURL__c='/v1/speed-assurance-traffic-light';
        objCS2.AccessSeekerId__c = 'testId';
        objCS2.Call_TimeOut_In_MilliSeconds__c = '60000';
        objCS2.NBN_Environment_Override__c='SIT1';
        objCS2.nbn_Role__c='NBN';
        objCS2.Service_Name__c='test serviec';
        objCS2.Token__c='8e73b7e33b22ad58';
        objCS2.XNBNBusinessChannel__c='Salesforce';
        objCS2.UI_TimeOut__c = 6000;
        objCS2.Header_Keys__c = 'NBN-TransactionID,NBN-PartyID,NBN-Channel,x-api-key'; 
        objCS2.x_api_key__c = 'testtest'; 
        lstToInsert.add(objCS2);
        
        Incident_Management__c obj1 = new Incident_Management__c();
        obj1.Name = 'INC000000000001';
        obj1.AVC_Id__c = 'AVC000000000001';
        obj1.Access_Seeker_ID__c ='ASI000000000001';
        obj1.Prod_Cat__c='NCAS-FTTN';
        lstToInsert.add(obj1);
                
        
        
       	insert lstToInsert;
    }
        
    // FTTN Incident Health Card
    @isTest static void testMethod1() {
        Test.startTest();
        String incidenId = [SELECT Id FROM Incident_Management__c WHERE Name = 'INC000000000001'].get(0).Id;
        // Set Mock
        MockHttpResponseGenerator objMock = new MockHttpResponseGenerator();
        System.debug('@@@' + healthCard_1);
        objMock.dummyResponse = healthCard_1;
        objMock.dummyStatusCode = 200;
        objMock.dummyStatus = 'OK';
        
        objMock.dummyResponseForTrafficLight = speed_1;
        objMock.dummyStatusCodeForTrafficLight = 200;
        objMock.dummyStatusForTrafficLight = 'OK';
        
        Test.setMock(HttpCalloutMock.class, objMock);
        JIGSAW_HealthCard_CX.HealthCardResponse result = JIGSAW_HealthCard_CX.requestHealthCard(incidenId,null);
        
        //boolean displayHealthcard = JIGSAW_HealthCard_CX.isHealthCardAvailable();
        Test.stopTest();
    }
    
    // FTTB Incident when accesSeeker is NULL
    @isTest static void testMethod2() {
        Test.startTest();
        Incident_Management__c inc = [SELECT Id, AVC_Id__c, Access_Seeker_ID__c FROM Incident_Management__c WHERE Name = 'INC000000000001'];
        inc.Access_Seeker_ID__c = null;
        inc.Prod_Cat__c = 'NCAS-FTTB';
        update inc;
            
        JIGSAW_HealthCard_CX.HealthCardResponse result = JIGSAW_HealthCard_CX.requestHealthCard(inc.Id,null);
        Test.stopTest();
        System.assertNotEquals(result.errorCode, null);
        System.assertNotEquals(result.errorDesc, null);
        
    }
    
    // FTTN Incident when AVC is NULL
    @isTest static void testMethod3() {
        Test.startTest();
        Incident_Management__c inc = [SELECT Id, AVC_Id__c, Access_Seeker_ID__c FROM Incident_Management__c WHERE Name = 'INC000000000001'];
        inc.AVC_Id__c = null;
        update inc;
            
        JIGSAW_HealthCard_CX.HealthCardResponse result = JIGSAW_HealthCard_CX.requestHealthCard(inc.Id,null);
        Test.stopTest();
        System.assertNotEquals(result.errorCode, null);
        System.assertNotEquals(result.errorDesc, null);
    }
    
    
    // FTTN Incident Speed API
    @isTest static void testMethod4() {
        Test.startTest();
        Incident_Management__c inc = [SELECT Id FROM Incident_Management__c WHERE Name = 'INC000000000001'].get(0);
        
        
        // Set Mock
        MockHttpResponseGenerator objMock = new MockHttpResponseGenerator();
        System.debug('@@@' + healthCard_1);
        objMock.dummyResponse = healthCard_1;
        objMock.dummyStatusCode = 200;
        objMock.dummyStatus = 'OK';
        
        objMock.dummyResponseForTrafficLight = speed_1;
        objMock.dummyStatusCodeForTrafficLight = 200;
        objMock.dummyStatusForTrafficLight = 'OK';
        
        Test.setMock(HttpCalloutMock.class, objMock); 
        JIGSAW_HealthCard_CX.HealthCardResponse result = JIGSAW_HealthCard_CX.requestHealthCard(inc.Id,null);
        
        
        
        
        
        
            
        //boolean displayHealthcard = JIGSAW_HealthCard_CX.isHealthCardAvailable();
        Test.stopTest();
    }
    
    
    // FTTN Incident Speed API with status code 404 for traffic light
    @isTest static void testMethod44() {
        Test.startTest();
        Incident_Management__c inc = [SELECT Id FROM Incident_Management__c WHERE Name = 'INC000000000001'].get(0);
        
        
        // Set Mock
        MockHttpResponseGenerator objMock = new MockHttpResponseGenerator();
        System.debug('@@@' + healthCard_1);
        objMock.dummyResponse = healthCard_1;
        objMock.dummyStatusCode = 200;
        objMock.dummyStatus = 'OK';
        
        objMock.dummyResponseForTrafficLight = speed_1;
        objMock.dummyStatusCodeForTrafficLight = 404;
        objMock.dummyStatusForTrafficLight = 'Not Found';
        
        Test.setMock(HttpCalloutMock.class, objMock); 
        JIGSAW_HealthCard_CX.HealthCardResponse result = JIGSAW_HealthCard_CX.requestHealthCard(inc.Id,null);
        
        
        
        
        
        
            
        //boolean displayHealthcard = JIGSAW_HealthCard_CX.isHealthCardAvailable();
        Test.stopTest();
    }
    
    @isTest static void testMethod45() {
        Test.startTest();
        Incident_Management__c inc = [SELECT Id FROM Incident_Management__c WHERE Name = 'INC000000000001'].get(0);
        
        
        // Set Mock
        MockHttpResponseGenerator objMock = new MockHttpResponseGenerator();
        System.debug('@@@' + healthCard_1);
        objMock.dummyResponse = healthCard_1;
        objMock.dummyStatusCode = 200;
        objMock.dummyStatus = 'OK';
        
        objMock.dummyResponseForTrafficLight = '{ "metadata":{ "accessSeekerId":"ASI000000000000", "serviceId":"AVC000000000001" }, "exception":{ "code":"000001", "message":"Access seeker is not part of the trial." } }';
        objMock.dummyStatusCodeForTrafficLight = 404;
        objMock.dummyStatusForTrafficLight = 'Not Found';
        
        Test.setMock(HttpCalloutMock.class, objMock); 
        JIGSAW_HealthCard_CX.HealthCardResponse result = JIGSAW_HealthCard_CX.requestHealthCard(inc.Id,null);
        
        
        
        
        
        
            
        //boolean displayHealthcard = JIGSAW_HealthCard_CX.isHealthCardAvailable();
        Test.stopTest();
    }
        
    // FTTN Incident with Notes section in payload
    @isTest static void testMethod5() {
        Test.startTest();
        String incidenId = [SELECT Id FROM Incident_Management__c WHERE Name = 'INC000000000001'].get(0).Id;
        // Set Mock
        MockHttpResponseGenerator objMock = new MockHttpResponseGenerator();
        System.debug('@@@' + healthCard_1);
        objMock.dummyResponse = healthCard_2;
        objMock.dummyStatusCode = 200;
        objMock.dummyStatus = 'OK';
        Test.setMock(HttpCalloutMock.class, objMock);
        JIGSAW_HealthCard_CX.HealthCardResponse result = JIGSAW_HealthCard_CX.requestHealthCard(incidenId,null);
        
        //boolean displayHealthcard = JIGSAW_HealthCard_CX.isHealthCardAvailable();
        Test.stopTest();
    }
    
    // test saveSnapshotAction method
    @isTest static void testMethod6() {
        Test.startTest();
        String incidenId = [SELECT Id FROM Incident_Management__c WHERE Name = 'INC000000000001'].get(0).Id;
        String healthCardPayload = '{  "serviceId": "AVC000000000010",  "notes": [],  "transactionTimestamp": "2019-02-06T02:36:42.723Z",  "basedOnDataUpdatedEvery15mins": {    "modemOperationalStateForPast24Hours": [      {        "value": "up",        "timestamp": "2018-08-13T13:42:47.798Z"      },      {        "value": "up",        "timestamp": "2018-08-13T13:27:47.798Z"      },      {        "value": "up",        "timestamp": "2018-08-13T13:12:47.798Z"      }    ],    "modemOperationalStateForPast48Hours": [      {        "value": "up",        "timestamp": "2018-08-13T13:42:47.798Z"      },      {        "value": "up",        "timestamp": "2018-08-13T13:27:47.798Z"      },      {        "value": "up",        "timestamp": "2018-08-13T13:12:47.798Z"      }    ],    "sumUnavailableSecondCountersForPast24Hours": 185,    "unavailableSecondCountersForPast24Hours": [      {        "value": 60,        "timestamp": "2018-08-13T13:42:47.798Z"      },      {        "value": 60,        "timestamp": "2018-08-13T13:27:47.798Z"      },      {        "value": 65,        "timestamp": "2018-08-13T13:12:47.798Z"      }    ],    "sumSeverelyErroredSecondsDownstreamForPast24Hours": 215,    "sumSeverelyErroredSecondsUpstreamForPast24Hours": 95,    "severelyErroredSeconds": {      "downstreamForPast24Hours": [        {          "timestamp": "2018-08-13T13:42:47.798Z",          "value": 70        },        {          "timestamp": "2018-08-13T13:27:47.798Z",          "value": 70        },        {          "timestamp": "2018-08-13T13:12:47.798Z",          "value": 75        }      ],      "upstreamForPast24Hours": [        {          "timestamp": "2018-08-13T13:42:47.798Z",          "value": 30        },        {          "timestamp": "2018-08-13T13:27:47.798Z",          "value": 30        },        {          "timestamp": "2018-08-13T13:12:47.798Z",          "value": 35        }      ]    },    "correctedAndr": {      "downstreamForPast24Hours": [        {          "timestamp": "2018-08-13T13:42:47.798Z",          "value": 40562        },        {          "timestamp": "2018-08-13T13:27:47.798Z",          "value": 42153        },        {          "timestamp": "2018-08-13T13:12:47.798Z",          "value": 52325        }      ],      "upstreamForPast24Hours": [        {          "timestamp": "2018-08-13T13:42:47.798Z",          "value": 12156        },        {          "timestamp": "2018-08-13T13:27:47.798Z",          "value": 13256        },        {          "timestamp": "2018-08-13T13:12:47.798Z",          "value": 12025        }      ]    },    "macAddressesForPast48Hours": [      {        "value": "0A-C3-52-C1-81-C0",        "timestamp": "2018-08-13T13:42:47.798Z"      },      {        "value": "3C-A3-B2-C2-12-AB",        "timestamp": "2018-08-11T13:56:07.798Z"      }    ],    "dropouts": {      "dropoutFault": true,      "dropout24HoursFault": true,      "dropoutsPast24Hours": 116,      "sumReInitialisationCountersForPast24Hours": 150,      "sumLossOfPowerCountersForPast24Hours": 34,      "dropout48HoursFault": true,      "dropoutsPast48Hours": 116,      "sumReInitialisationCountersForPast48Hours": 150,      "sumLossOfPowerCountersForPast48Hours": 34,      "sumUnavailableSecondCountersForPast48Hours": 185,      "sumSeverelyErroredSecondsDownstreamForPast48Hours": 215,      "sumSeverelyErroredSecondsUpstreamForPast48Hours": 95,      "reInitialisationCounterPast48Hours": [        {          "value": 50,          "timestamp": "2018-08-13T13:42:47.798Z"        },        {          "value": 50,          "timestamp": "2018-08-13T13:27:47.798Z"        },        {          "value": 50,          "timestamp": "2018-08-13T13:12:47.798Z"        }      ],      "lossOfPowerCounterPast48Hours": [        {          "value": 9,          "timestamp": "2018-08-13T13:42:47.798Z"        },        {          "value": 10,          "timestamp": "2018-08-13T13:27:47.798Z"        },        {          "value": 15,          "timestamp": "2018-08-13T13:12:47.798Z"        }      ]    },    "loopAttenuation": {      "loopAttenuationWarning": true,      "loopAttenuationVariationForPast24Hours": 15,      "loopAttenuationUnitType": "dB",      "loopAttenuationDownstreamForPast24Hours": [        {          "value": 10,          "timestamp": "2018-08-13T13:42:47.798Z"        },        {          "value": 20,          "timestamp": "2018-08-13T13:27:47.798Z"        },        {          "value": 25,          "timestamp": "2018-08-13T13:12:47.798Z"        }      ]    }  },  "basedOnDataUpdatedEvery24Hours": {    "cpeListFromPast30Days": [      {        "make": "Huawei",        "serialNumber": "AA:AA:AA:AA:AA:AA",        "model": "Hi9jguj",        "firmwareVersion": "2.3.565.1152212",        "compatibility": "Compatible",        "compatibilityRagStatus": "Green",        "timestamp": "2018-08-12T13:41:07.798Z"      },      {        "make": "Apple",        "serialNumber": "AA:AA:AA:AA:AA:AA",        "model": "AABBVENDORID",        "firmwareVersion": "2.3.565.1152212",        "compatibility": "Compatible",        "compatibilityRagStatus": "Green",        "timestamp": "2018-08-11T13:41:07.798Z"      }    ],    "failedReInitialisations": 3,    "failedReInitialisationsForPast48Hours": 6,    "failedReInitialisationsTimestamp": "2018-08-13T13:41:07.798Z",    "actualRateAdaptationModeDownstream": "Automatic at Startup",    "actualRateAdaptationModeUpstream": "Operator Controlled",    "actualRateAdaptationModeTimestamp": "2018-08-13T13:41:07.798Z",    "noiseMarginDownstream": 2379,    "noiseMarginUpstream": 2379,    "noiseMarginTimestamp": "2018-08-13T13:41:07.798Z",    "noiseMarginUnitType": "dB",    "outputPowerDownstream": 9999,    "outputPowerUpstream": 100,    "outputPowerTimestamp": "2018-08-13T13:41:07.798Z",    "outputPowerUnitType": "dBm",    "calculated375Attenuation": 50,    "calculated375AttenuationUnitType": "dB",    "impulseNoiseProtectionDownstream": 123,    "impulseNoiseProtectionUpstream": 27071,    "impulseNoiseProtectionTimestamp": "2018-08-13T13:41:07.798Z",    "impulseNoiseProtectionUnitType": "symbols",    "lineLengthFrom375": 620,    "lineLengthAccuracyFrom375": "60",    "lineLengthUnitType": "m"  },  "healthCardSummary": {    "stabilityRagStatus": "Red",    "connectivityRagStatus": "Red",    "serviceStateLastChangeTimestamp": "2019-01-31T00:00:52Z",    "serviceState": "Down",    "serviceStateTimestamp": "2018-08-13T13:42:47.798Z",    "correctedAttainableRateDownstream": "40562 kbps",    "correctedAttainableRateUpstream": "12156 kbps",    "correctedAttainableRateTimestamp": "2018-08-13T13:42:47.798Z",    "actualNetDataRateDownstream": "35025 kbps",    "actualNetDataRateUpstream": "10352 kbps",    "actualNetDataRateTimestamp": "2018-08-13T13:42:47.798Z",    "calculated375Attenuation": "50 dB",    "calculated375AttenuationRagStatus": "Red",    "calculated375AtteuationTimestamp": "2018-08-13T13:41:07.798Z",    "dropoutFault": true,    "dropoutFaultTimestamp": "2018-08-13T13:42:47.798Z",    "dropout24HoursFault": true,    "dropout48HoursFault": true,    "dropoutsPast24Hours": 116,    "dropoutsPast48Hours": 116,    "sumReInitialisationCountersForPast24Hours": 150,    "sumReInitialisationCountersForPast48Hours": 150,    "reinitialisationCountersTimestamp ": "2018-08-13T13:42:47.798Z",    "sumLossOfPowerCountersForPast24Hours": 34,    "sumLossOfPowerCountersForPast48Hours": 34,    "lossOfPowerCountersTimestamp": "2018-08-13T13:42:47.798Z",    "sumUnavailableSecondCountersForPast24Hours": 185,    "sumUnavailableSecondCountersForPast48Hours": 185,    "unavailableSecondCountersTimestamp": "2018-08-13T13:42:47.798Z",    "sumSeverelyErroredSecondsDownstreamForPast48Hours": 215,    "sumSeverelyErroredSecondsUpstreamForPast48Hours": 95,    "sumSeverelyErroredSecondsUpstreamForPast24Hours": 95,    "sumSeverelyErroredSecondsDownstreamForPast24Hours": 215,    "severelyErroredSecondsTimestamp": "2018-08-13T13:42:47.798Z",    "loopAttenuationWarning": true,    "loopAttenuationVariationForPast24Hours": "15 dB",    "loopAttenuationDownstream": "10 dB",    "loopAttenuationDownstreamTimestamp": "2018-08-13T13:42:47.798Z",    "loopAttenuationUpstream": "5 dB",    "loopAttenuationUpstreamTimestamp": "2018-08-12T13:41:07.798Z",    "sumFailedReinitialisationsForPast24Hours": 3,    "sumFailedReinitialisationsForPast48Hours": 6,    "failedReinitialisationsTimestamp": "2018-08-13T13:42:47.798Z",    "noiseMarginDownstream": "2379 dB",    "noiseMarginUpstream": "2379 dB",    "noiseMarginTimestamp": "2018-08-13T13:41:07.798Z",    "actualRateAdaptationModeUpstream": "Operator Controlled",    "actualRateAdaptationModeUpstreamRagStatus": "Red",    "actualRateAdaptationModeDownstream": "Automatic at Startup",    "actualRateAdaptationModeDownstreamRagStatus": "Green",    "actualRateAdaptationModeTimestamp": "2018-08-13T13:41:07.798Z",    "cpeMake": "Huawei",    "cpeModel": "Hi9jguj",    "cpeSerialNumber": "B392KD89239KD93L",    "cpeFirmwareVersion": "2.3.565.1152212",    "cpeCompatibility": "Compatible",    "cpeCompatibilityRagStatus": "Green",    "cpeTimestamp": "2018-08-12T13:41:07.798Z",    "cpeType": "Registered",    "cpeTypeRagStatus": "Green",    "cpeMacAddress": "0A:C3:52:C1:81:C0",    "cpeMacAddressTimestamp": "2018-08-13T13:42:47.798Z",    "cpeTerminationType": "OPEN",    "cpeTerminationTypeTimestamp": "2019-02-06T02:14:11.673Z",    "numOfCpeChangesInLast30Days": 1,    "cpeChangeInLast30Days": true,    "potsServicePresent": "YES",    "electricalMeasurementOfLoopTimestamp": "2019-02-06T02:14:11.548Z",    "foreignDcAbVoltage": "-0.1 V",    "foreignDcAbVoltageRagStatus": "Green",    "foreignDcAEarthVoltage": "-0.2 V",    "foreignDcAEarthVoltageRagStatus": "Green",    "foreignDcBEarthVoltage": "-0.1 V",    "foreignDcBEarthVoltageRagStatus": "Green",    "foreignAcAbVoltage": "0.0 V",    "foreignAcAbVoltageRagStatus": "Green",    "foreignAcAEarthVoltage": "0.6 V",    "foreignAcAEarthVoltageRagStatus": "Green",    "foreignAcBEarthVoltage": "0.6 V",    "foreignAcBEarthVoltageRagStatus": "Green",    "abResistance": "5000+ kohms",    "abResistanceRagStatus": "Green",    "aEarthResistance": "2854.7 kohms",    "aEarthResistanceRagStatus": "Red",    "bEarthResistance": "5000+ kohms",    "bEarthResistanceRagStatus": "Green",    "abCapacitance": "58.5 nF",    "aEarthCapacitance": "63.3 nF",    "bEarthCapacitance": "51.0 nF",    "capacitiveBalance": "81%",    "capacitiveBalanceRagStatus": "Green",    "lineLengthFromSelt": "640m",    "lineLengthAccuracyFromSelt": "[+/-] 70m",    "lineLengthFromSeltTimestamp": "2019-02-15T02:22:01.599Z",    "lineLengthFrom375": "620m",    "lineLengthAccuracyFrom375": "[+/-] 60m",    "lineLengthFrom375Timestamp": "2018-08-12T13:41:07.798Z",    "dlmStatus": "Enabled",    "tooltips": {      "stabilityRagStatus": "Stability Red/Amber/Green Status for the given AVC",      "connectivityRagStatus": "Connectivity Red/Amber/Green Status for the given AVC",      "serviceState": "Latest modem operational state. Indicates if the modem is up or down",      "serviceStateLastChangeTimestamp": "Some Text About This Field",      "sumUnavailableSecondCounters": "Some Text About This Field",      "sumSeverelyErroredSeconds": "Some Text About This Field",      "correctedAttainableRate": "Some Text About This Field",      "calculated375Attenuation": "Some Text About This Field",      "dropoutFault": "Some Text About This Field",      "sumReInitialisationCounters": "Some Text About This Field",      "sumLossOfPowerCounters": "Some Text About This Field",      "loopAttenuation": "Some Text About This Field",      "sumFailedReinitialisations": "Some Text About This Field",      "noiseMargin": "Some Text About This Field",      "actualRateAdaptationMode": "Some Text About This Field",      "cpeMake": "Some Text About This Field",      "cpeModel": "Some Text About This Field",      "cpeSerialNumber": "Some Text About This Field",      "cpeFirmwareVersion": "Some Text About This Field",      "cpeMacAddress": "Some Text About This Field",      "cpeCompatibility": "Some Text About This Field",      "cpeType": "Some Text About This Field",      "cpeTerminationType": "Some Text About This Field",      "potsServicePresent": "Some Text About This Field",      "foreignDcVoltageRagStatus": "Some Text About This Field",      "foreignAcVoltageRagStatus": "Some Text About This Field",      "resistanceRagStatus": "Some Text About This Field",      "capacitiveBalanceRagStatus": "Some Text About This Field",      "lineLengthFromSelt": "Some Text About This Field",      "lineLengthFrom375": "Some Text About This Field",      "numOfCpeChangesInLast30Days": "Some Text About This Field",      "cpeChangeInLast30Days": "Some Text About This Field",      "dlmStatus": "Some Text About This Field",      "actualNetDataRate": "Some Text About This Field",      "currentSpeed": "Some Text About This Field",      "averageSpeed7Days": "Some Text About This Field",      "averageSpeed2Hours": "Some Text About This Field"    }  },  "cpeListFromPast30Days": [    {      "make": "Huawei",      "serialNumber": "B392KD89239KD93L",      "model": "Hi9jguj",      "firmwareVersion": "2.3.565.1152212",      "compatibility": "Incompatible",      "compatibilityRagStatus": "Red",      "timestamp": "2018-08-12T13:41:07.798Z"    },    {      "make": "Apple",      "serialNumber": "ACL93KADWXC",      "model": "AABBVENDORID",      "firmwareVersion": "2.3.565.1152212",      "compatibility": "Compatible",      "compatibilityRagStatus": "Green",      "timestamp": "2018-08-11T13:41:07.798Z"    }  ],  "macAddressesForPast48Hours": [    {      "value": "0A-C3-52-C1-81-C0",      "timestamp": "2018-08-13T13:42:47.798Z"    },    {      "value": "3C-A3-B2-C2-12-AB",      "timestamp": "2018-08-11T13:56:07.798Z"    }  ]}';
        String speedPayload = '{    "metadata": {		"accessSeekerId": "ASI000000000001",		"serviceId": "AVC000000000001"	},	"unitType": "Mbps",	"averageNetDataRateUpstream": 13.452,	"averageNetDataRateDownstream": 25.125,	"attainableNetDataRateUpstream": 18.999,	"attainableNetDataRateDownstream": 25.999,	"attainableNetDataRateTimestamp": "2018-06-27T20:25:02.725Z",	"sdcAverageNetDataRateUpstream": 14.125,	"sdcAverageNetDataRateDownstream": 26.452,	"thresholdWbaUpstream": 0.845,	"thresholdWbaDownstream": 18.845,	"thresholdUc1Upstream": 0.912,	"thresholdUc1Downstream": 22.125,	"thresholdUc2Upstream": 13.532,	"thresholdUc2Downstream": 25.425,	"thresholdBaselineDownstream": 17.044,	"thresholdBaselineUpstream": 20.755,	"thresholdTc2Upstream": 7.447,	"thresholdTc2Downstream": 7.445,	"tc2UpstreamCapable": true,	"tc2DownstreamCapable": true,	"indicator": "Green",	"indicatorCode": "MAX_WBA_UC1_UC2_MET",	"indicatorDescription": "Service is performing within specification. Performance Trouble Tickets should not be lodged for this service.",	"incidentEligibility": "No"}';
        JIGSAW_HealthCard_CX.saveSnapshotAction(healthCardPayload,speedPayload,incidenId);
        Test.stopTest();
    }

    
    class MockHttpResponseGenerator implements HttpCalloutMock {
        public String dummyResponse;
        public Integer dummyStatusCode;
        public String dummyStatus;
        public String dummyResponseForTrafficLight;
        public Integer dummyStatusCodeForTrafficLight;
        public String dummyStatusForTrafficLight;
        
        // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            if (req.getEndpoint().endsWith('speed-assurance-traffic-light') && !String.isBlank(dummyResponseForTrafficLight)) {
                // Create a fake response
                res.setHeader('Content-Type', 'application/json');
                res.setBody(dummyResponseForTrafficLight);
                res.setStatusCode(dummyStatusCodeForTrafficLight);
                res.setStatus(dummyStatusForTrafficLight);
                return res;
            } 
            else{
                // Create a fake response
                res.setHeader('Content-Type', 'application/json');
                res.setBody(dummyResponse);
                res.setStatusCode(dummyStatusCode);
                res.setStatus(dummyStatus);
                return res;
            }
        }
    }
}