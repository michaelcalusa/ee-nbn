/***************************************************************************************************
Class Name:  JIGSAW_IncidentManagementCon_LC_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 30/05/2018
Function    : Test script used to cover JIGSAW_IncidentManagementController_LC class
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
  Murali Krishna              17/01/2019             test class used to cover unit test of
                                                     JIGSAW_IncidentManagementController_LC class
****************************************************************************************************/
@isTest()
private class JIGSAW_IncidentManagementCon_LC_Test 
{
    @testSetup static void loadTestDataJIGSAW_IncidentManagementCon() 
    {
        insert new Incident_Management__c (Name = 'INC000111211',Prod_Cat__c = 'NCAS-FTTN',Access_Seeker_ID__c = 'ACCESSSEEKERID',repeatIncidentPriority__c = 'RepeatIncidentPriority',PRI_ID__c = 'PRIID',AVC_Id__c ='AVC000028840963',Fault_Type__c = 'Performance latency', recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Incident Management FTTN/B').getRecordTypeId(), Appointment_Start_Date__c=system.now(),Appointment_End_Date__c=system.now(), Target_Commitment_Date__c=system.now(),Reported_Date__c=system.now());
        insert new Incident_Management__c (Fault_Type__c = 'Intermittent service/dropouts', recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Incident Management FTTN/B').getRecordTypeId(), Appointment_Start_Date__c=system.now(),Appointment_End_Date__c=system.now(), Target_Commitment_Date__c=system.now(),Reported_Date__c=system.now());
        insert new Incident_Management__c (Fault_Type__c = 'VOIP fault', recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Incident Management FTTN/B').getRecordTypeId(), Appointment_Start_Date__c=system.now(),Appointment_End_Date__c=system.now(), Target_Commitment_Date__c=system.now(),Reported_Date__c=system.now());
        insert new Incident_Management__c (Fault_Type__c = 'No sync', recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Incident Management FTTN/B').getRecordTypeId(), Appointment_Start_Date__c=system.now(),Appointment_End_Date__c=system.now(), Target_Commitment_Date__c=system.now(),Reported_Date__c=system.now());
        insert new Incident_Management__c (Fault_Type__c = 'No data connection', recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Incident Management FTTN/B').getRecordTypeId(), Appointment_Start_Date__c=system.now(),Appointment_End_Date__c=system.now(), Target_Commitment_Date__c=system.now(),Reported_Date__c=system.now());
    }
    /*test Method for linkNetworkIncidents Method of loadTestDataJIGSAW_IncidentManagementCon Class */
    Static TestMethod void linkNetworkIncidentsTest()
    {
         Test.StartTest();
         User u = new User(firstname= 'test',
                          lastname='IncidentMmtCtrl',
                          Alias='tinci',
                          email = 'tinci@email.com',
                          username= 'test.IncidentMmtCtrl@nbnco.com.au', 
                          profileId= [SELECT id, Name FROM Profile WHERE Name = 'System Administrator'][0].Id, 
                          emailencodingkey='UTF-8',
                          languagelocalekey='en_US',
                          localesidkey='en_US',
                          timezonesidkey='Australia/Sydney',
                          FederationIdentifier='n102466'
                          );
        System.runAs(u)
        {
            Incident_Management__c im = [SELECT Id FROM Incident_Management__c LIMIT 1];
            JIGSAW_IncidentManagementController_LC.linkNetworkIncidents(im.Id,'INC00011222331','Test Message Seeker');   
        }
        Test.StopTest();
    }
    /*test Method for linkNetworkIncidents Method of JIGSAW_IncidentManagementController_LC Class in Exception Case */
    Static TestMethod void linkNetworkIncidentsException()
    {
         Test.StartTest();
         User u = new User(firstname= 'test',
                          lastname='IncidentMmtCtrl',
                          Alias='tinci',
                          email = 'tinci@email.com',
                          username= 'test.IncidentMmtCtrl@nbnco.com.au', 
                          profileId= [SELECT id, Name FROM Profile WHERE Name = 'System Administrator'][0].Id, 
                          emailencodingkey='UTF-8',
                          languagelocalekey='en_US',
                          localesidkey='en_US',
                          timezonesidkey='Australia/Sydney',
                          FederationIdentifier='n102466'
                          );
        System.runAs(u)
        {
            Incident_Management__c im = [SELECT Id FROM Incident_Management__c LIMIT 1];
            JIGSAW_IncidentManagementController_LC.linkNetworkIncidents('00617125152121','INC00011222331','Test Message Seeker');   
        }
        Test.StopTest();
    }
    
    Static testMethod void getAppointmentDetailsFromCISTestSuccess() {
        List<sObject> lstNbnintegrationinputs = Test.loadData(NBNIntegrationInputs__c.sObjectType, 'NBNIntegrationInputsTestData');
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('getAppointmentDetailsResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        Test.StartTest();
        	Incident_Management__c im = [SELECT Id FROM Incident_Management__c LIMIT 1];
        	JIGSAW_IncidentManagementController_LC.getAppointmentValidateFromCIS('getAppointmentDetailsCIS','ASC123','APT123');
        Test.StopTest();
    }
}