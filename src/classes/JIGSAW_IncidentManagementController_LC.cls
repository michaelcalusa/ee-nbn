/*------------------------------------------------------------ 
Author:    Rohit Mathur 
Description:   Server-side controller which will handle a number of custom lightning components
History
<Date>      <Author>     <Description>
11/12/2018  Rohit M      Created tthis controller class that will be used as direct controller class for multiple lightning components.
------------------------------------------------------------*/
public class JIGSAW_IncidentManagementController_LC 
{
    @AuraEnabled
    public static String getPendingReasonType(String reasonCode,String asRequestDescription)
    {
        String pendingReason;
        if(String.isNotBlank(reasonCode) && String.isNotBlank(asRequestDescription))
        {
            list<RemedyPendingReasonCodes__mdt> reasonList = [select Id,AS_Request_Description__c,DeveloperName,Group_Name__c,Label,MasterLabel,
                                 Reason_Code__c,Request_Description__c,Substatus__c 
                          From   RemedyPendingReasonCodes__mdt 
                          where  Reason_Code__c =: reasonCode AND Substatus__c = 'Pending'];
            if(reasonList != null && !reasonList.isEmpty())
            {
                for(RemedyPendingReasonCodes__mdt remedyPendingReasoncode : reasonList)
                {
                    if(String.isNotBlank(remedyPendingReasoncode.AS_Request_Description__c) && remedyPendingReasoncode.AS_Request_Description__c.Equals(asRequestDescription))
                    {
                        //In case of More Information required
                        if(remedyPendingReasoncode.Group_Name__c == 'Additional Information Required')
                        {
                            pendingReason = 'MoreInfoResponse';
                        }
                        else if(remedyPendingReasoncode.Group_Name__c == 'Appointment Reschedule Required')
                        {
                            pendingReason = 'AppointmentReschedule';
                        }
                        else if(remedyPendingReasoncode.Group_Name__c == 'Appointment Required')
                        {
                            pendingReason = 'NewAppointment';
                        }
                    }
                }
            }
        }
        return pendingReason;
    }
    /* Method to update Op Cat (Jigsaw_Op_Cat)
    *  Created By : Sunil
    *  Date : 17 January 2019
    */
    @AuraEnabled
    public static Incident_Management__c submitOpCat(String incidentID, String Tier1, String Tier2, String oldTier1, String oldTier2, String Tier3, String oldTier3, String techType) {
        if((Tier1 != oldTier1 || Tier2 != oldTier2 || Tier3 != oldTier3) && String.isNotBlank(incidentID) && String.isNotBlank(Tier1) && String.isNotBlank(Tier2)){
            try{
                System.debug('techType '+techType);
                Incident_Management__c inm = new Incident_Management__c();
                inm.id = incidentID;
                inm.User_Action__c = 'updateOperationCategorisation';
                //inm.Op_Cat__c = Tier1;
                //inm.Fault_Type__c = Tier2;
                // CUSTSA-31682
                if((techType == 'NHAS' || techType == 'NHUR') && String.isNotBlank(Tier3)){
                	inm.Op_Cat_Inflight_Changes__c = Tier1 + ';'+ Tier2+ ';'+ Tier3;
                }else{
                    inm.Op_Cat_Inflight_Changes__c = Tier1 + ';'+ Tier2;
                }
                // Show Spinner under SLA if Incident Type is changed.
                if(oldTier1 !=  Tier1){
                    inm.Awaiting_Current_SLA_Status__c = true;
                }
                
                // to do: need to confirm the role of below field
                inm.RandomAction__c = Math.round(Math.random()*1000);
                
                
                update inm;
                
                
                String body;
                body = 'Operation Categorisation updated by ' + UserInfo.getName() + '\r\n';
                body += 'PREVIOUS \r\n';
                if(Tier1 != oldTier1){
                	body += 'Tier 1: ' + oldTier1 + '\r\n';
                }
                if(Tier2 != oldTier2){
                	body += 'Tier 2: ' + oldTier2 + '\r\n';
                }
                // CUSTSA-31682
                if((techType == 'NHAS' || techType == 'NHUR') && String.isNotBlank(oldTier3)){
                	body += 'Tier 3: ' + oldTier3 + '\r\n';
                }
                
                
                body += '\r\nUPDATE \r\n';
                
                if(Tier1 != oldTier1){
                	body += 'Tier 1: ' + Tier1 + '\r\n';
                }
                if(Tier2 != oldTier2){
                	body += 'Tier 2: ' + Tier2 + '\r\n';
                }
                // CUSTSA-31682
                if((techType == 'NHAS' || techType == 'NHUR') && String.isNotBlank(Tier3)){
                    body += 'Tier 3: ' + Tier3 + '\r\n';
                }
                
                FeedItem feedObj = new FeedItem();
                String s = String.isBlank(oldTier2) ? 'NULL' : oldTier2;
                feedObj.body = body;
                feedObj.NetworkScope = 'AllNetworks';
                feedObj.ParentId = incidentID;
                feedObj.Status = 'Published';
                feedObj.Visibility = 'AllUsers';
                feedObj.Type = 'TextPost';
                insert feedObj;
                
                
                return inm;
            }catch(Exception ex){
                system.debug('@@OpCat: ' + ex.getStackTraceString());
                return null;
            }
        }
        system.debug('@@@inm: ');  
        return null;
    }
    
    
    private static List<JIGSAW_OpCategories__mdt> lstMetaData;
    private static List<JIGSAW_OpCategories__mdt> fetchMetaData(String t){
        
        if(lstMetaData == null){
            lstMetaData = [SELECT Id, MasterLabel, Technology__c, Tier1__c , Tier2__c, Tier3__c 
                           FROM JIGSAW_OpCategories__mdt 
                           WHERE Technology__c = :t ORDER BY Tier2__c];
        }
        return lstMetaData;
        
    }    
    
    @AuraEnabled 
    public static Map<String, List<String>> getDependentMap(String techType) {
        Map<String, List<String>> objResults = new Map<String, List<String>>();
        
        if(techType == 'NCAS-FTTN' || techType == 'NCAS-FTTB'){
            techType = 'FTTN/B';
        }
        
        List<JIGSAW_OpCategories__mdt> lstValues = fetchMetaData(techType);
        
        for (JIGSAW_OpCategories__mdt cat : lstValues) {
            objResults.put(cat.Tier1__c, new List<String>());
        }
        System.debug('@@@' + objResults);
        
        Set<String> uniqueValues;
        for(String s: objResults.keySet()){
            uniqueValues = new Set<String>();
            for(JIGSAW_OpCategories__mdt cat: lstValues){
                if(cat.Tier1__c == s && String.isNotBlank(cat.Tier2__c)){
                    if(uniqueValues.add(cat.Tier2__c)){
                        objResults.get(s).add(cat.Tier2__c);
                        uniqueValues.add(cat.Tier2__c);
                    }
                    
                }
                
            }
        }
        System.debug('@@@' + objResults);
        return objResults;
    }
    
    @AuraEnabled 
    public static Map<String, List<String>> getDependentMapT3(String techType, String T1, String T2) {
        Map<String, List<String>> objResults = new Map<String, List<String>>();
        
        if(techType == 'NCAS-FTTN' || techType == 'NCAS-FTTB'){
            techType = 'FTTN/B';
        }
        
        List<JIGSAW_OpCategories__mdt> lstValues = fetchMetaData(techType);
        
        for (JIGSAW_OpCategories__mdt cat : lstValues) {
            if(cat.Tier1__c == T1 && cat.Tier2__c == T2){
                if(!objResults.containsKey(cat.Tier2__c)){
                    objResults.put(cat.Tier2__c, new List<String>());
                }
                objResults.get(cat.Tier2__c).add(cat.Tier3__c);
            }
            
        }
        return objResults;
    } 
    /* Method to update incident Record CUSTSA - 24872
    *  Created By : Murali Krishna
    *  Date : 09 January 2019
    */
    @AuraEnabled
    public static Incident_Management__c linkNetworkIncidents(String strRecordId,String strNetworkIncidents,String strNbnInternalNote)
    {
        Incident_Management__c updatedIncidentRecord;
        try
        {
            Incident_Management__c updateIncidentManagementRecord = new Incident_Management__c(Id = strRecordId);
            //Feeditem variable to intialize chatter feed to insert based on the action by user.
            FeedItem feedObj;
            updateIncidentManagementRecord.User_Action__c ='relateToIncident';
            if(String.isNotBlank(strNetworkIncidents))
            {
                updateIncidentManagementRecord.DisableOperatorActions__c = true; 
                updateIncidentManagementRecord.Send_SINI_Request__c = True;
                updateIncidentManagementRecord.Network_Incidents_For_Linking__c = strNetworkIncidents;
            }
            if(String.IsNotBlank(strNbnInternalNote))
            {
                //Add Chatter Post.
                feedObj = new FeedItem();
                feedObj.body = strNbnInternalNote;
                feedObj.NetworkScope = 'AllNetworks';
                feedObj.ParentId = strRecordId;
                feedObj.Status = 'Published';
                feedObj.Visibility = 'AllUsers';
                feedObj.Type = 'TextPost';
            }
            //Update Incident Management Record
            Update updateIncidentManagementRecord;
            //Insert FeedItems;
            if(feedObj != Null )
                Insert feedObj;
            updatedIncidentRecord = incidentManagementController_LC.getUpdatedIncidentRecord(strRecordId);
        }
        catch(Exception ex)
        {
            System.debug('@@ Exception Message' + ex.getMessage());  
            //Added below code as per part of CUSTSA-17093 - Nishank Tandon.
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'JIGSAW_IncidentManagementController_LC','linkNetworkIncidents',strRecordId,'',Ex.getMessage(), Ex.getStackTraceString(),Ex, 0);
            updatedIncidentRecord = NULL;
        }
        return updatedIncidentRecord;
    }
	
	// CUSTSA-27232 - Get Appointment details from CIS to validate AppointmentID
    @AuraEnabled
    public Static Map<String, Object> getAppointmentValidateFromCIS(String integrationType,String accessSeekerId ,String appointmentId)
    {
        return JIGSAW_SummaryTableController.getAppointmentDetailsFromCIS(integrationType,accessSeekerId,appointmentId);
        
    }
}