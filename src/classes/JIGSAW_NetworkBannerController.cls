/*------------------------------------------------------------
Author: Nishank Tandon Jan 21, 2019
Company: IBM
Description: Server-side controller for Network Banner Lightning component used to display NI and CRQ Details.
Story : CUSTSA-26697, CUSTSA-6629
History
<Date>      <Author>     <Description>
------------------------------------------------------------*/
public class JIGSAW_NetworkBannerController {
    
    @AuraEnabled   
    public static SummaryWrapper fetchNetworkIncidents(String incId) {
        SummaryWrapper objWrapper = new SummaryWrapper();
        SummaryWrapper objSummaryWrapperNI = new SummaryWrapper();
        SummaryWrapper objSummaryWrapperCRQ = new SummaryWrapper();
        List < SummaryWrapper > lstSobjectDataNI = new List < SummaryWrapper > ();
        List < SummaryWrapper > lstSobjectDataCRQ = new List < SummaryWrapper > ();
        List < String > strFields = new List < String > ();
        HttpResponse response;
        String avcId;
        String incidentNumber;
        String jsonString;
        Boolean validNI;
        Boolean validCRQ;
        System.debug('Incident Id-->' + incId);
        List < Incident_Management__c > incList = [select id, AVC_Id__c,Incident_Number__c from Incident_Management__c where id =: incId];
        if (!incList.isEmpty()){
            avcId = incList.get(0).AVC_Id__c;
            incidentNumber = incList.get(0).Incident_Number__c;
        }
        System.debug('Incident Number-->' + incidentNumber);
        System.debug('AVC Id-->' + avcId);
        
        //Do call out to fetch the network banner details.
        try{
            response = doNetworkBannerCallout('JIGSAW_NetworkBanner',avcId, incidentNumber);
            //Set Sample JSON for testing purpose.
            //JIGSAW_NetworkBannerWrapper objNetworkBannerWrapper = new JIGSAW_NetworkBannerWrapper();
            //response.setBody(objNetworkBannerWrapper.json);
            //response.setStatusCode(200);
            jsonString = response.getBody();
            System.debug('Sample Response-->'+ response.getBody());
            System.debug('Sample jsonString-->'+ jsonString);
            if(response.getBody()!='{}'&& response.getBody()!='') {
                System.debug('In Response if');
                JIGSAW_NetworkBannerWrapper objResponseWrapper = (JIGSAW_NetworkBannerWrapper)JSON.deserialize(jsonString,JIGSAW_NetworkBannerWrapper.Class);
                if(response.getStatusCode() == 200) {
                    System.debug('Response Wrapper-->' + objResponseWrapper);
                    System.debug('Network Banner Status Code-->' + response.getStatusCode());
                    System.debug('Network Banner Response Body-->' + response.getBody());
                    if (objResponseWrapper.unplannedOutages != null && !objResponseWrapper.unplannedOutages.isEmpty() && objResponseWrapper.plannedOutages != null && !objResponseWrapper.plannedOutages.isEmpty()){
                        //There is a planned & unplanned outage i.e. NI and CRQ is available.
                        //Fill the Summry Wrapper for NI details.
                        for (JIGSAW_NetworkBannerWrapper.cls_unplannedOutages unplannedOutagesData : objResponseWrapper.unplannedOutages){
                            System.debug('unplannedOutagesData.incidentId-->'+ unplannedOutagesData.incidentNumber);
                            objSummaryWrapperNI = new SummaryWrapper();
                            objSummaryWrapperNI.incidentId = unplannedOutagesData.incidentNumber;
                            objSummaryWrapperNI.statusNI = unplannedOutagesData.status;
                            objSummaryWrapperNI.priority = unplannedOutagesData.priority;
                            objSummaryWrapperNI.summaryNI = unplannedOutagesData.summary;
                            if (unplannedOutagesData.reportedDateTime != null){
                                objSummaryWrapperNI.raisedNI = formatDate(unplannedOutagesData.reportedDateTime);
                            }
                            if (unplannedOutagesData.requiredResolutionDateTime != null){
                                objSummaryWrapperNI.slaDueBy = formatDate(unplannedOutagesData.requiredResolutionDateTime);
                            }
                            if (unplannedOutagesData.lastResolutionDateTime != null){
                                objSummaryWrapperNI.resolutionDate = formatDate(unplannedOutagesData.lastResolutionDateTime);
                            }
                            if (unplannedOutagesData.closedDateTime != null){
                                objSummaryWrapperNI.closedDate = formatDate(unplannedOutagesData.closedDateTime);
                            }
                            if (unplannedOutagesData.estimatedResolutionDateTime != null){
                                objSummaryWrapperNI.estimatedResolutionDateTime  = formatDate(unplannedOutagesData.estimatedResolutionDateTime);
                            }
                            if (unplannedOutagesData.actualResolutionDateTime != null){
                                objSummaryWrapperNI.actualResolutionDateTime = formatDate(unplannedOutagesData.actualResolutionDateTime);
                            }
                            if (unplannedOutagesData.relatedToSI == 'Yes'){
                                objSummaryWrapperNI.relatedToSI = 'Yes';
                            }
                            else
                                objSummaryWrapperNI.relatedToSI = 'No';
                            objSummaryWrapperNI.notes = unplannedOutagesData.detailedDescription;
                            validNI = true;
                            lstSobjectDataNI.add(objSummaryWrapperNI);
                            System.debug('NI Data-->' + JSON.serialize(lstSobjectDataNI));
                        }
                        //Fill the Summry Wrapper for CRQ details.
                        for (JIGSAW_NetworkBannerWrapper.cls_plannedOutages plannedOutagesData : objResponseWrapper.plannedOutages){
                             objSummaryWrapperCRQ = new SummaryWrapper();
                            objSummaryWrapperCRQ.CRQNumber = plannedOutagesData.infrastructureChangeId;
                            objSummaryWrapperCRQ.statusCRQ = plannedOutagesData.status;
                            objSummaryWrapperCRQ.summaryCRQ = plannedOutagesData.summary;
                            if (plannedOutagesData.submitDate != null){
                                objSummaryWrapperCRQ.raisedCRQ = formatDate(plannedOutagesData.submitDate);
                            }
                            if (plannedOutagesData.scheduledStartDate != null){
                                objSummaryWrapperCRQ.scheduledStart = formatDate(plannedOutagesData.scheduledStartDate);
                            }
                            if (plannedOutagesData.scheduledEndDate != null){
                                objSummaryWrapperCRQ.scheduledEnd = formatDate(plannedOutagesData.scheduledEndDate);
                            }
                            if (plannedOutagesData.requestedStartDate != null){
                                objSummaryWrapperCRQ.requestedStart = formatDate(plannedOutagesData.requestedStartDate);
                            }
                            if (plannedOutagesData.requestedEndDate != null){
                                objSummaryWrapperCRQ.requestedEnd = formatDate(plannedOutagesData.requestedEndDate);
                            }
                            if (plannedOutagesData.actualStartDate != null){
                                objSummaryWrapperCRQ.actualStartDate = formatDate(plannedOutagesData.actualStartDate);
                            }
                            if (plannedOutagesData.actualEndDate != null){
                                objSummaryWrapperCRQ.actualEndDate = formatDate(plannedOutagesData.actualEndDate);
                            }
                            validCRQ = true;
                            lstSobjectDataCRQ.add(objSummaryWrapperCRQ);
                            System.debug('CRQ Data-->' + lstSobjectDataCRQ);
                        }
                        
                        system.debug('Debug in the first condition for a valid NI-->' + objResponseWrapper.unplannedOutages);
                        system.debug('Debug for valid NI-->' + objResponseWrapper.unplannedOutages[0].incidentNumber);
                    }
                    else if (objResponseWrapper.unplannedOutages != null && !objResponseWrapper.unplannedOutages.isEmpty() && (objResponseWrapper.plannedOutages == null || objResponseWrapper.plannedOutages.isEmpty())){
                        //There is a unplanned outage i.e. NI is available.
                        //Fill the Summry Wrapper for NI details.
                        for (JIGSAW_NetworkBannerWrapper.cls_unplannedOutages unplannedOutagesData : objResponseWrapper.unplannedOutages){
                            objSummaryWrapperNI = new SummaryWrapper();
                            objSummaryWrapperNI.incidentId = unplannedOutagesData.incidentNumber;
                            objSummaryWrapperNI.statusNI = unplannedOutagesData.status;
                            objSummaryWrapperNI.priority = unplannedOutagesData.priority;
                            objSummaryWrapperNI.summaryNI = unplannedOutagesData.summary;
                            if (unplannedOutagesData.reportedDateTime != null){
                                objSummaryWrapperNI.raisedNI = formatDate(unplannedOutagesData.reportedDateTime);
                            }
                            if (unplannedOutagesData.requiredResolutionDateTime != null){
                                objSummaryWrapperNI.slaDueBy = formatDate(unplannedOutagesData.requiredResolutionDateTime);
                            }
                            if (unplannedOutagesData.lastResolutionDateTime != null){
                                objSummaryWrapperNI.resolutionDate = formatDate(unplannedOutagesData.lastResolutionDateTime);
                            }
                            if (unplannedOutagesData.closedDateTime != null){
                                objSummaryWrapperNI.closedDate = formatDate(unplannedOutagesData.closedDateTime);
                            }
                            if (unplannedOutagesData.estimatedResolutionDateTime != null){
                                objSummaryWrapperNI.estimatedResolutionDateTime  = formatDate(unplannedOutagesData.estimatedResolutionDateTime);
                            }
                            if (unplannedOutagesData.actualResolutionDateTime != null){
                                objSummaryWrapperNI.actualResolutionDateTime = formatDate(unplannedOutagesData.actualResolutionDateTime);
                            }
                            if (unplannedOutagesData.relatedToSI == 'Yes'){
                                objSummaryWrapperNI.relatedToSI = 'Yes';
                            }
                            else
                                objSummaryWrapperNI.relatedToSI = 'No';
                            objSummaryWrapperNI.notes = unplannedOutagesData.detailedDescription;
                            validNI = true;
                            lstSobjectDataNI.add(objSummaryWrapperNI);
                            System.debug('NI Data-->' + objWrapper);
                        }
                    }
                    else if (objResponseWrapper.plannedOutages != null && !objResponseWrapper.plannedOutages.isEmpty() && (objResponseWrapper.unplannedOutages == null || !objResponseWrapper.plannedOutages.isEmpty())){
                        //There is a planned outage i.e. CRQ is available.
                        //Fill the Summry Wrapper for CRQ details.
                        for (JIGSAW_NetworkBannerWrapper.cls_plannedOutages plannedOutagesData : objResponseWrapper.plannedOutages){
                            objSummaryWrapperCRQ.CRQNumber = plannedOutagesData.infrastructureChangeId;
                            objSummaryWrapperCRQ.statusCRQ = plannedOutagesData.status;
                            objSummaryWrapperCRQ.summaryCRQ = plannedOutagesData.summary;
                            if (plannedOutagesData.submitDate != null){
                                objSummaryWrapperCRQ.raisedCRQ = formatDate(plannedOutagesData.submitDate);
                            }
                            if (plannedOutagesData.scheduledStartDate != null){
                                objSummaryWrapperCRQ.scheduledStart = formatDate(plannedOutagesData.scheduledStartDate);
                            }
                            if (plannedOutagesData.scheduledEndDate != null){
                                objSummaryWrapperCRQ.scheduledEnd = formatDate(plannedOutagesData.scheduledEndDate);
                            }
                            if (plannedOutagesData.requestedStartDate != null){
                                objSummaryWrapperCRQ.requestedStart = formatDate(plannedOutagesData.requestedStartDate);
                            }
                            if (plannedOutagesData.requestedEndDate != null){
                                objSummaryWrapperCRQ.requestedEnd = formatDate(plannedOutagesData.requestedEndDate);
                            }
                            if (plannedOutagesData.actualStartDate != null){
                                objSummaryWrapperCRQ.actualStartDate = formatDate(plannedOutagesData.actualStartDate);
                            }
                            if (plannedOutagesData.actualEndDate != null){
                                objSummaryWrapperCRQ.actualEndDate = formatDate(plannedOutagesData.actualEndDate);
                            }
                            validCRQ = true;
                            lstSobjectDataCRQ.add(objSummaryWrapperCRQ);
                            System.debug('CRQ Data-->' + lstSobjectDataCRQ);
                        }
                    }
                }
                else if(response.getStatusCode() == 500) {
                    
                    objWrapper.errorCode = objResponseWrapper.errorCode; 
                    objWrapper.errorMessage = objResponseWrapper.errorMessage;
                }
                else{
                    objWrapper.errorCode = String.valueOf(response.getStatusCode());
                    objWrapper.errorMessage = 'Something went wrong';
                }
            }
            else{
                throw new AurahandledException('Error in callout networkBannerDetails: No Response Body ');
            }
        }catch(Exception ex){
            objWrapper.errorMessage = 'Error in callout networkBannerDetails: ' + ex.getMessage();
            objWrapper.errorCode = 'UNKNOWN';
        }
        
        // Fill Wrapper
        objWrapper.validNI = validNI;
        objWrapper.validCRQ = validCRQ;
        objWrapper.lstSobjectDataNI = lstSobjectDataNI;
        objWrapper.lstSobjectDataCRQ = lstSobjectDataCRQ;
        return objWrapper;
    }
    
    // Helper Method to change date format
    private static string formatDate(String epochTimeStamp){
        Long dateValueInEpochFormat;
        dateValueInEpochFormat = Long.valueOf(epochTimeStamp + '000');
        DateTime tempDate = DateTime.newInstance(dateValueInEpochFormat);
        String formattedDate = tempDate.format('dd/MM/YYYY hh:mm a');
        return formattedDate;
    }
    
    // Helper method to make callout
    public static HTTPResponse doNetworkBannerCallout(String integrationType,String avcId, String incidentNumber) {
        //Do the call out for the network banner details.
        HttpResponse networkBannerResponse = new HttpResponse();
        networkBannerResponse = integrationUtility.getNetworkBanner ('JIGSAW_NetworkBanner',avcId,incidentNumber);
        // check response returned by Callout .
        System.Debug('Network Banner Response-->' + networkBannerResponse.getBody());
        return networkBannerResponse;
    }
    
    //Wrapper class to hold Columns with headers
    public class DataTableColumns {
        
        @AuraEnabled
        public String label {
            get;
            set;
        }
        
        @AuraEnabled
        public Boolean sortable {
            get;
            set;
        }
        
        @AuraEnabled
        public String type {
            get;
            set;
        }
        
        public DataTableColumns(String label, String dataType, Boolean sortable) {
            this.label = label;
            this.type = dataType;
            this.sortable = sortable;
        }
    }
    
    public class SummaryWrapper {
        
        @AuraEnabled
        public Integer totalRecords {
            get;
            set;
        }
        
        @AuraEnabled
        public List < Object > lstSobjectDataNI {
            get;
            set;
        }
        
        @AuraEnabled
        public List < Object > lstSobjectDataCRQ {
            get;
            set;
        }
        
        @AuraEnabled
        public String errorMessage {
            get;
            set;
        }
        
        @AuraEnabled
        public String errorCode {
            get;
            set;
        }
        
        @AuraEnabled
        public String statusNI {
            get;
            set;
        }
        
        @AuraEnabled
        public String statusCRQ {
            get;
            set;
        }
        
        @AuraEnabled
        public String priority {
            get;
            set;
        }   
        
        @AuraEnabled
        public String raisedNI {
            get;
            set;
        }
        
        @AuraEnabled
        public String slaDueBy {
            get;
            set;
        }
        
        @AuraEnabled
        public String resolutionDate {
            get;
            set;
        }
        
        @AuraEnabled
        public String closedDate {
            get;
            set;
        }
        
        @AuraEnabled
        public String notes {
            get;
            set;
        }
        
        @AuraEnabled
        public String raisedCRQ {
            get;
            set;
        }
        
        @AuraEnabled
        public String scheduledStart {
            get;
            set;
        }
        
        @AuraEnabled
        public String scheduledEnd {
            get;
            set;
        }
        
        @AuraEnabled
        public String requestedStart {
            get;
            set;
        }
        
        @AuraEnabled
        public String requestedEnd {
            get;
            set;
        }
        
        @AuraEnabled
        public Boolean validNI {
            get;
            set;
        }
        
        @AuraEnabled
        public Boolean validCRQ {
            get;
            set;
        }
        
        @AuraEnabled
        public String incidentId {
            get;
            set;
        }
        
        @AuraEnabled
        public String CRQNumber {
            get;
            set;
        }
        
        @AuraEnabled
        public String relatedToSI {
            get;
            set;
        }
        
        @AuraEnabled
        public String estimatedResolutionDateTime{
            get;
            set;
        }
        
        @AuraEnabled
        public String actualResolutionDateTime{
            get;
            set;
        }
        
        @AuraEnabled
        public String summaryNI {
            get;
            set;
        }
        
        @AuraEnabled
        public String summaryCRQ {
            get;
            set;
        }
        
        @AuraEnabled
        public String actualStartDate {
            get;
            set;
        }
        
        @AuraEnabled
        public String actualEndDate {
            get;
            set;
        }
    }
}