@isTest
public class JIGSAW_NetworkBannerControllerTest {
    
    // Test Data
    @testSetup static void setup() {
        
        // create data for Incident Management custom object.
        Incident_Management__c obj = new Incident_Management__c();
        obj.Appointment_Start_Date__c = System.now();
        obj.Appointment_End_Date__c = System.now();
        obj.Target_Commitment_Date__c = System.now();
        obj.Appointment_Start_Date__c = System.now();
        obj.Appointment_End_Date__c = System.now();
        obj.Target_Commitment_Date__c = System.now();
        obj.PRI_ID__c = 'PRI003000710422';
        obj.AVC_ID__C = 'AVC003000710422';
        insert obj;
        
        Incident_Management__c obj1 = new Incident_Management__c();
        obj1.Appointment_Start_Date__c = System.now();
        obj1.Appointment_End_Date__c = System.now();
        obj1.Target_Commitment_Date__c = System.now();
        obj1.Appointment_Start_Date__c = System.now();
        obj1.Appointment_End_Date__c = System.now();
        obj1.Target_Commitment_Date__c = System.now();
        obj1.PRI_ID__c = 'PRI003000710422';
        obj1.AVC_ID__C = 'AVC003000710423';
        insert obj1;
        
        //Create data for Custom Setting.
        List<NBNIntegrationInputs__c> integratioInputList = new List<NBNIntegrationInputs__c>();
		integratioInputList.add(new NBNIntegrationInputs__c(AccessSeekerId__c='', API_GW_Callout_URL__c='callout:Api_Gateway', Call_Out_URL__c='callout:NBNAPPGATEWAY', Call_TimeOut_In_MilliSeconds__c='60000', EndpointURL__c='/v1/assn-network-outages/outage/{0}?serviceIncidentNumber={1}&outageType=all', Is_Api_Gateway__c=TRUE, Name='JIGSAW_NetworkBanner', NBN_Activity_Name__c='', NBN_Environment_Override__c='SIT1', nbn_Role__c='NBN', NBNBreadcrumbId__c='', Service_Name__c='Salesforce', Token__c='', UI_TimeOut__c=null, x_api_key__c='pnqPLGG8Y52i4enle3C924EnZECXfAFw5BjCSazc', XNBNBusinessChannel__c='Salesforce', Header_Keys__c='X-NBN-CorrelationID,X-NBN-APIVersion,X-NBN-MessageID,X-NBN-Channel,NBN-Business-Channel,NBN-Environment-Override,NBN-Activity-Name,x-api-key',XNBNAPIVersion__c='1.0.0'));
		insert integratioInputList;
    }
    
    static testMethod void testMethod_NICRQAvailable() {
     
        // Set Mock
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('getNetworkBannerResponseNICRQ');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        Incident_Management__c inc = [SELECT Id, AVC_Id__c, Name FROM Incident_Management__c where AVC_ID__C = 'AVC003000710422'].get(0);
        Test.startTest();       
        	JIGSAW_NetworkBannerController.SummaryWrapper wrap1 = JIGSAW_NetworkBannerController.fetchNetworkIncidents(inc.Id);
        Test.stopTest();
    }
    
    static testMethod void testMethod_CRQPlanned() {
        
        // Set Mock
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('getNetworkBannerResponseCRQ');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        Incident_Management__c inc1 = [SELECT Id, AVC_Id__c, Name FROM Incident_Management__c where AVC_ID__C = 'AVC003000710423'].get(0);
        Test.startTest();
        	JIGSAW_NetworkBannerController.SummaryWrapper wrap2 = JIGSAW_NetworkBannerController.fetchNetworkIncidents(inc1.Id);
        Test.stopTest();
    }
    
    static testMethod void testMethod_NIPlanned() {
        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('getNetworkBannerResponseNI');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        Incident_Management__c inc1 = [SELECT Id, AVC_Id__c, Name FROM Incident_Management__c where AVC_ID__C = 'AVC003000710423'].get(0);
        Test.startTest();
        	JIGSAW_NetworkBannerController.SummaryWrapper wrap3 = JIGSAW_NetworkBannerController.fetchNetworkIncidents(inc1.Id);
        Test.stopTest();
    }
}