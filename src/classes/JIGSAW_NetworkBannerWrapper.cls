//
// Apex Wrapper class generated for Network Banner API Response.
//

@isTest
public class JIGSAW_NetworkBannerWrapper{
    public String avcId;    //AVC000012345678
    public String errorCode;
    public String errorMessage;
    public cls_unplannedOutages[] unplannedOutages;
    public cls_plannedOutages[] plannedOutages;
    public class cls_unplannedOutages {
        public String incidentNumber;   //INC000000324567
        public String status;   //In Progress
        public String summary;  //NCAS-FTTN || NI || 4EDG-06-02-FNO-001 || Vectoring problem
        public String priority; //Medium
        public String reportedDateTime; //1543556414
        public String requiredResolutionDateTime;   //1543642814
        public String relatedToSI;  //Yes
        public String closedDateTime;
        public String lastResolutionDateTime;
        public String detailedDescription;
        public String estimatedResolutionDateTime;
        public String actualResolutionDateTime;
    }
    public class cls_plannedOutages {
        public String infrastructureChangeId;   //CRQ000002424763
        public String coordinatorGroup; //NBNCo Operations
        public String changeCoordinator;    //Hoi Kung
        public String changeLocation;   //1010 LaTrobe St
        public String summary;  //NWAS: 2SYA/Asquith Sample Summary
        public String notes;    //This activity has been pre-approved at NOPS CAB.
        public String changeReason; //Other
        public String targetDate;   //1540332000
        public String impact;   //4-Minor/Localized
        public String urgency;  //3-Medium
        public String priority; //Medium
        public String riskLevel;    //Risk Level 2
        public String status;   //Implementation In Progress
        public String managerGroup; //NBNCo Ops Change Management
        public String changeManager;    //Nicholas Kane
        public String changeManagerLoginId; //nickkane
        public String interruption1;    //90
        public String interruption2;    //0
        public String interruption3;    //0
        public String interruption4;    //0
        public String interruption5;    //0
        public String changeDuration;   //10
        public String impactedLocation; //Redland Bay, QLD
        public String changeDescription;    //This notification is to let you know that we will be performing network maintenance work.
        public String opCatTier1;   //NBNCo Ops Change Request
        public String opCatTier2;   //Non-Impacting
        public String opCatTier3;
        public String prodCatTier1; //NCAS-FTTN
        public String prodCatTier2;
        public String prodCatTier3;
        public String scheduledStartDate;   //1540332000
        public String scheduledEndDate; //1540533600
        public String requestedStartDate;   //1540332000
        public String requestedEndDate; //1540533600
        public String submitDate;   //1539907154
        public String actualStartDate;   //1539907154
        public String actualEndDate;   //1539907154
    }
    public static JIGSAW_NetworkBannerWrapper parse(String json){
        return (JIGSAW_NetworkBannerWrapper) System.JSON.deserialize(json, JIGSAW_NetworkBannerWrapper.class);
    }
    
    public String json=        '{'+
        '   "avcId": "AVC000012345678",'+
        '          "unplannedOutages": [{'+
		'                    "incidentNumber": "INC000000324567",'+
		'                    "status": "In Progress",'+
		'                    "summary": "NCAS-FTTN || NI || 4EDG-06-02-FNO-001 || Vectoring problem",'+
		'                    "priority": "Medium",'+
		'                    "reportedDateTime": "1543556414",'+
		'                    "requiredResolutionDateTime": "1543642814",'+
        '                    "lastResolutionDateTime": "1543642814",'+
        '                    "closedDate": "1543642814",'+
        '                    "estimatedResolutionDateTime": "1543642814",'+
        '                    "actualResolutionDateTime": "1543642814",'+
		'                    "relatedToSI": "Yes"'+
		'          },'+
		'          {'+
		'                    "incidentNumber": "INC000000324566",'+
		'                    "status": "In Progress",'+
		'                    "summary": "NCAS-FTTN || NI || 4EDG-06-02-FNO-001 || Vectoring problem",'+
		'                    "priority": "Medium",'+
		'                    "reportedDateTime": "1543556414",'+
		'                    "requiredResolutionDateTime": "1543642814",'+
        '                    "lastResolutionDateTime": "1543642814",'+
        '                    "closedDate": "1543642814",'+
		'                    "relatedToSI": ""'+
		'          }],'+
        '		"plannedOutages": [{'+
        '       "infrastructureChangeId": "CRQ000002424763",'+
        '       "coordinatorGroup": "NBNCo Operations",'+
        '       "changeCoordinator": "Hoi Kung",'+
        '       "changeLocation": "1010 LaTrobe St",'+
        '       "summary": "NWAS: 2SYA/Asquith Sample Summary",'+
        '       "notes": "This activity has been pre-approved at NOPS CAB.",'+
        '       "class": "Standard",'+
        '       "changeReason": "Other",'+
        '       "targetDate": "1540332000",'+
        '       "impact": "4-Minor/Localized",'+
        '       "urgency": "3-Medium",'+
        '       "priority": "Medium",'+
        '       "riskLevel": "Risk Level 2",'+
        '       "status": "Implementation In Progress",'+
        '       "managerGroup": "NBNCo Ops Change Management",'+
        '       "changeManager": "Nicholas Kane",'+
        '       "changeManagerLoginId": "nickkane",'+
        '       "interruption1": "90",'+
        '       "interruption2": "0",'+
        '       "interruption3": "0",'+
        '       "interruption4": "0",'+
        '       "interruption5": "0",'+
        '       "changeDuration": "10",'+
        '       "impactedLocation": "Redland Bay, QLD",'+
        '       "changeDescription": "This notification is to let you know that we will be performing network maintenance work.",'+
        '       "opCatTier1": "NBNCo Ops Change Request",'+
        '       "opCatTier2": "Non-Impacting",'+
        '       "opCatTier3": null,'+
        '       "prodCatTier1": "NCAS-FTTN",'+
        '       "prodCatTier2": null,'+
        '       "prodCatTier3": null,'+
        '       "scheduledStartDate": "1540332000",'+
        '       "scheduledEndDate": "1540533600",'+
        '       "requestedStartDate": "1540332000",'+
        '       "requestedEndDate": "1540533600",'+
        '       "submitDate": "1539907154",'+
        '       "actualStartDate": "1539907154",'+
        '       "actualEndDate": "1539907154"'+
        '          },'+
		'          {'+
		'       "infrastructureChangeId": "CRQ000002424765",'+
        '       "coordinatorGroup": "NBNCo Operations",'+
        '       "changeCoordinator": "Hoi Kung",'+
        '       "changeLocation": "1010 LaTrobe St",'+
        '       "summary": "NWAS: 2SYA/Asquith Sample Summary",'+
        '       "notes": "This activity has been pre-approved at NOPS CAB.",'+
        '       "class": "Standard",'+
        '       "changeReason": "Other",'+
        '       "targetDate": "1540332000",'+
        '       "impact": "4-Minor/Localized",'+
        '       "urgency": "3-Medium",'+
        '       "priority": "Medium",'+
        '       "riskLevel": "Risk Level 2",'+
        '       "status": "Implementation In Progress",'+
        '       "managerGroup": "NBNCo Ops Change Management",'+
        '       "changeManager": "Nicholas Kane",'+
        '       "changeManagerLoginId": "nickkane",'+
        '       "interruption1": "90",'+
        '       "interruption2": "0",'+
        '       "interruption3": "0",'+
        '       "interruption4": "0",'+
        '       "interruption5": "0",'+
        '       "changeDuration": "10",'+
        '       "impactedLocation": "Redland Bay, QLD",'+
        '       "changeDescription": "This notification is to let you know that we will be performing network maintenance work.",'+
        '       "opCatTier1": "NBNCo Ops Change Request",'+
        '       "opCatTier2": "Non-Impacting",'+
        '       "opCatTier3": null,'+
        '       "prodCatTier1": "NCAS-FTTN",'+
        '       "prodCatTier2": null,'+
        '       "prodCatTier3": null,'+
        '       "scheduledStartDate": "1540332000",'+
        '       "scheduledEndDate": "1540533600",'+
        '       "requestedStartDate": "1540332000",'+
        '       "requestedEndDate": "1540533600",'+
        '       "submitDate": "1539907154"'+
		'   }]'+
        '}';
    
    
    static testMethod void testParse() {
        String json=        '{'+
        '   "avcId": "AVC000012345678",'+
        '   "unplannedOutages": [{'+
        '       "incidentNumber": "INC000000324567",'+
        '       "status": "In Progress",'+
        '       "summary": "NCAS-FTTN || NI || 4EDG-06-02-FNO-001 || Vectoring problem",'+
        '       "priority": "Medium",'+
        '       "reportedDateTime": "1543556414",'+
        '       "requiredResolutionDateTime": "1543642814",'+
        '       "relatedToSI": "Yes"'+
        '   }],'+
        '   "plannedOutages": [{'+
        '       "infrastructureChangeId": "CRQ000002424763",'+
        '       "coordinatorGroup": "NBNCo Operations",'+
        '       "changeCoordinator": "Hoi Kung",'+
        '       "changeLocation": "1010 LaTrobe St",'+
        '       "summary": "NWAS: 2SYA/Asquith Sample Summary",'+
        '       "notes": "This activity has been pre-approved at NOPS CAB.",'+
        '       "class": "Standard",'+
        '       "changeReason": "Other",'+
        '       "targetDate": "1540332000",'+
        '       "impact": "4-Minor/Localized",'+
        '       "urgency": "3-Medium",'+
        '       "priority": "Medium",'+
        '       "riskLevel": "Risk Level 2",'+
        '       "status": "Implementation In Progress",'+
        '       "managerGroup": "NBNCo Ops Change Management",'+
        '       "changeManager": "Nicholas Kane",'+
        '       "changeManagerLoginId": "nickkane",'+
        '       "interruption1": "90",'+
        '       "interruption2": "0",'+
        '       "interruption3": "0",'+
        '       "interruption4": "0",'+
        '       "interruption5": "0",'+
        '       "changeDuration": "10",'+
        '       "impactedLocation": "Redland Bay, QLD",'+
        '       "changeDescription": "This notification is to let you know that we will be performing network maintenance work.",'+
        '       "opCatTier1": "NBNCo Ops Change Request",'+
        '       "opCatTier2": "Non-Impacting",'+
        '       "opCatTier3": null,'+
        '       "prodCatTier1": "NCAS-FTTN",'+
        '       "prodCatTier2": null,'+
        '       "prodCatTier3": null,'+
        '       "scheduledStartDate": "1540332000",'+
        '       "scheduledEndDate": "1540533600",'+
        '       "requestedStartDate": "1540332000",'+
        '       "requestedEndDate": "1540533600",'+
        '       "submitDate": "1539907154"'+
        '   }]'+
        '}';
        
        JIGSAW_NetworkBannerWrapper obj = parse(json);
        System.assert(obj != null);
    }
}