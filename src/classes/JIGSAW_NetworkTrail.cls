/***************************************************************************************************
Class Name:         JIGSAW_NetworkTrail 
Created Date:       27 June 2018
Created By:         Marc Bazin
Description:        Controller for JIGSAW_NetworkTrail component 

Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Sunil Gupta      03/08/2018      
****************************************************************************************************/
public class JIGSAW_NetworkTrail {
    
    private static Integer timeoutInMilliSec = 60000;
   
  // Calling from Lightning component JIGSAW_NetworkTrail 
  
  /*  @AuraEnabled 
    public static WrapperResponse  requestNetworkTrail(String cpiId, String technologyType, String incidentId){
        
        WrapperResponse objResponse = checkCache(cpiId, technologyType);
        
        // Do callout if Cache not found.
        if(objResponse.isCacheFound == false || !String.isBlank(objResponse.errorDesc)){
            try{
                HttpResponse mResp = new HttpResponse();
                //Added by Chitra 
           	 	//mResp = doCallout(cpiid, technologyType, incidentId);
                if(technologyType == 'NCAS-FTTB'){
                    technologyType =  'Fibre To The Building';
                }else if(technologyType == 'NCAS-FTTN'){
                    technologyType =  'Fibre To The Node';
                }

                //making callout using Integration Utility class methods
                mResp = integrationUtility.postReqTMS(cpiId, technologyType, incidentId, 'csll', 'NetworkTrail');
                //code change ends
           	 	String transactionId = '';
           	 	
           	 	if(mResp != null){
                    system.debug('mRESP value - ' + mResp);
               	 	if(mResp.getStatusCode() == 202 || mResp.getStatusCode() == 200){
               	 	    String jsonString = mResp.getBody();
                    	Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
        				transactionId = (String)responseMap.get('transactionId');
        				System.debug('@@@' + transactionId);
        				// Update Incident Record to record the transaction Id.
        				if(String.isBlank(transactionId) == false){
        				    List<Incident_Management__c> lstIM = [SELECT Id, Network_Trail_Transaction_Id__c FROM incident_management__C WHERE Name = :incidentId];
        				    
        				    if(lstIM.size() > 0){
        				        lstIM.get(0).Network_Trail_Transaction_Id__c = transactionId;
        				    }
        				    System.debug('@@@' + lstIM.get(0));
                            update lstIM;
                            System.debug('@@@' + lstIM.get(0));
        				}
        				objResponse.transactionId = transactionId;
        		  	}
               	 	else{
               	 	    objResponse.errorDesc = 'Error in callout cableRecordEnquiry: ' + mResp.getBody();
               	 	    objResponse.errorCode = String.valueOf(mResp.getStatusCode());
        		  	}
       	 	    }
       	 	    else{
       	 	        objResponse.errorDesc = 'Error in callout cableRecordEnquiry';
               	 	objResponse.errorCode = 'UNKNOWN';
       	 	    }
            }
            catch(Exception ex){
                objResponse.errorDesc = 'Error in callout cableRecordEnquiry: ' + ex.getMessage();
               	objResponse.errorCode = 'UNKNOWN';
            }
        }
        objResponse.timeoutInMilliSec = timeoutInMilliSec;
        
        // To Do: temp remove below code;
        //objResponse.transactionId = 'aqe8j72iuw2380298';
        //objResponse.strPayload = '';
        //objResponse.errorCode = '404';
        //objResponse.timeoutInMilliSec = 100000;
        // End To DO
        
        
        System.debug('@@@objResponse: ' + objResponse);
        return objResponse;
        
    }  */
    
     
    // Calling from Lightning component JIGSAW_NetworkTrail  waiting or response from PNI
   /* @AuraEnabled 
    public static WrapperResponse  requestPNIResponse(String cpiId, String incidentId, String technologyType){
        WrapperResponse objResponse = checkCache(cpiId, technologyType);
        return objResponse;
    } */
    
    
    
    // Helper method to check Cache Object
   /* private static WrapperResponse checkCache(String cpiId, String technologyType){
        WrapperResponse objResponse = new WrapperResponse();
        
        List<NBNIntegrationCache__c> lstCache = [SELECT Id, JSONPayload__c, Transaction_Id__c, CPI_Id__c FROM NBNIntegrationCache__c 
                                            WHERE CPI_Id__c =:cpiId AND integrationtype__c = 'NetworkTrail'  limit 1];
                                            
        System.debug('@@@' + lstCache);
        if(lstCache.size() > 0 && !String.isBlank(lstCache.get(0).JSONPayload__c)){
            String jsonString = lstCache.get(0).JSONPayload__c;
            System.debug('@@@' + jsonString);
            objResponse.isCacheFound = true;
            
            // Check if there is a parsing error.
            try{
                jsonString = jsonString.replaceAll('\n', '').replaceAll('\r', '');
                jsonString = jsonString.replaceFirst('exception', 'exception_x');
                

                if(technologyType == 'NCAS-FTTN' || technologyType == 'NCAS-FTTB'){
                	JIGSAW_WrapperNetworkTrail objWrapper = (JIGSAW_WrapperNetworkTrail)JSON.deserialize(jsonString, JIGSAW_WrapperNetworkTrail.Class);
                    if(objWrapper.status == 'Failure' || objWrapper.exception_x != null){
                        if(String.isBlank(objWrapper.exception_x.message)){
                            objResponse.errorDesc = objWrapper.exception_x.message;
                        }else{
                            objResponse.errorDesc = 'Failed status found with no details';
                        }
                        
                        if(!String.isBlank(objWrapper.exception_x.code)){
                            objResponse.errorCode = objWrapper.exception_x.code;
                        }
                        else{
                            objResponse.errorCode = 'UNKNOWN';
                        }
                	}
                    else{
                        objResponse.strPayload = jsonString; 
                    }
                }
                
                else if(technologyType == 'FTTB'){
                    JIGSAW_WrapperNetworkTrailFTTB objWrapper_FTTB = (JIGSAW_WrapperNetworkTrailFTTB)JSON.deserialize(jsonString, JIGSAW_WrapperNetworkTrailFTTB.Class);
                    
                    if(objWrapper_FTTB.fttbSite == null){
                        objResponse.errorDesc = 'Invalid JSON, FTTB Not found.';
               			objResponse.errorCode = 'UNKNOWN';
                    }
                    
                    else if(objWrapper_FTTB.status == 'Failure' || objWrapper_FTTB.exception_x != null){
                        if(!String.isBlank(objWrapper_FTTB.exception_x.message)){
                            objResponse.errorDesc = objWrapper_FTTB.exception_x.message;
                        }else{
                            objResponse.errorDesc = 'Failed status found with no details';
                        }
                        
                        if(!String.isBlank(objWrapper_FTTB.exception_x.code)){
                            objResponse.errorCode = objWrapper_FTTB.exception_x.code;
                        }
                        else{
                            objResponse.errorCode = 'UNKNOWN';
                        }
                	}
                    else{
                        objResponse.strPayload = jsonString; 
                    }
                }
            }
            catch(Exception ex){
                objResponse.errorDesc = 'Invalid JSON, ' + ex.getMessage();
               	objResponse.errorCode = 'UNKNOWN';
            }
        }
        else{
            objResponse.isCacheFound = false;
        }
		 return objResponse;        
    } 
      */
    
  
    
    
    // Wrapper class for result
   /* public class WrapperResponse{
        @AuraEnabled
        public Boolean isCacheFound {
            get;
            set;
        }
        
      
        
        @AuraEnabled
        public String strPayload {
            get;
            set;
        }
        
        @AuraEnabled
        public String errorDesc {
            get;
            set;
        }
        
        @AuraEnabled
        public String errorCode {
            get;
            set;
        }
        
        @AuraEnabled
        public String transactionId {
            get;
            set;
        }
        
        @AuraEnabled
        public Integer timeoutInMilliSec {
            get;
            set;
        }
    } */
    
    /*
    // Helper method to make callout
    public static HTTPResponse doCallout(String copperPathId, String techType, String incidentId) {
        HttpResponse response = new HttpResponse();
        Http rh = new Http();
        HttpRequest req = new HttpRequest();
        String strnbnCorrelationId = integrationUtility.newGuid('');
        
        NBNIntegrationInputs__c cType  = NBNIntegrationInputs__c.getInstance('NetworkTrail');
        
        if(cType.UI_TimeOut__c != null && cType.UI_TimeOut__c > 0){
            timeoutInMilliSec = Integer.valueOf(cType.UI_TimeOut__c);
        }
        
        req.setMethod('POST');
        req.setHeader('nbn-role', cType.nbn_Role__c);
        req.setHeader('nbn-requestor-platform', cType.XNBNBusinessChannel__c);
        req.setHeader('nbn-correlation-id', strnbnCorrelationId);
        req.setHeader('Content-Type', 'application/json');
        req.setTimeOut(integer.valueof(cType.Call_TimeOut_In_MilliSeconds__c));
        req.setHeader('NBN-Environment-Override',String.valueOf(cType.NBN_Environment_Override__c)); 
        req.setEndpoint(cType.Call_Out_URL__c + cType.EndpointURL__c);
        
        Map<String, String> body = new Map<String, String>();
        body.put('tokenId', cType.Token__c);
        body.put('copperPathId', copperPathId);
        
        if(techType == 'NCAS-FTTB'){
        	body.put('accessServiceTechnologyType', 'Fibre To The Building');
        }else if(techType == 'NCAS-FTTN'){
            body.put('accessServiceTechnologyType', 'Fibre To The Node');
        }
        
        body.put('csll', 'false');
        
        if(!String.isBlank(incidentId)){
            body.put('incidentId', incidentId);
        }
        
        System.debug('@@@request::' + req.toString());
        
        String sbody = JSON.serialize(body);
        req.setBody(sbody);
        response = rh.send(req);
        
        System.debug('@@@' + response);
        
        if(response != null){
            System.debug('@@@' + response.getStatus());
            System.debug('@@@' + response.getStatusCode());
            System.debug('@@@' + response.getBody());
        }
        return response;
    }*/
    
   // START - Add demo code under Refactoring process - Lokesh  
    @AuraEnabled 
    public static String requestNetworkTrailResponse(String cpiId, String technologyType, String incidentId, String incRecId){
        
        Map<String,String> networkResponse = new Map<String,String>();
        List<NBNIntegrationCache__c> lstCache = checkRecordInCache(cpiId, technologyType);
        // Do callout if Cache not found.
        if(lstCache.isEmpty()){
            try{
                HttpResponse mResp = new HttpResponse();
                /*Added by Chitra **/
           	 	//mResp = doCallout(cpiid, technologyType, incidentId);
                if(technologyType == 'NCAS-FTTB'){
                    technologyType =  'Fibre To The Building';
                }else if(technologyType == 'NCAS-FTTN'){
                    technologyType =  'Fibre To The Node';
                }

                //making callout using Integration Utility class methods
                mResp = integrationUtility.postReqTMS(cpiId, technologyType, incidentId, 'csll', 'NetworkTrail');
                String transactionId = '';
           	 	system.debug('mRESP value - ' + mResp);
           	 	if(mResp != null){
                  // test check for Error  
                 /* mResp.setStatusCode(502);
                    mResp.setBody('Error- Server connection error. Try later');
                 */   
               	 	if(mResp.getStatusCode() == 202 || mResp.getStatusCode() == 200){
               	 	    String jsonString = mResp.getBody();
                    	Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
        				transactionId = (String)responseMap.get('transactionId');
        				System.debug('@@@ transactionId' + transactionId);
        				// Update Incident Record to record the transaction Id.
        				if(String.isBlank(transactionId) == false){
        				  
                            Incident_Management__c incRec = new Incident_Management__c();
                            incRec.id = incRecId;
                            incRec.Network_Trail_Transaction_Id__c = transactionId;
                            update incRec;
        				    System.debug(' update incRec' + incRec);
        				}
        				networkResponse.put('transactionId',transactionId);
                       // networkResponse.put('payload','');
        		  	}
               	 	else{
                        networkResponse.put('errorDesc','Error in callout cableRecordEnquiry: ' + mResp.getBody());
                        networkResponse.put('errorCode',String.valueOf(mResp.getStatusCode()));
                        //networkResponse.put('payload','');
               	 	}
       	 	    }
       	 	    else{
                    networkResponse.put('errorDesc','Error in callout cableRecordEnquiry' );
                    networkResponse.put('errorCode','UNKNOWN');
                    //networkResponse.put('payload','');
       	 	    }
            }
            catch(Exception ex){
                networkResponse.put('errorDesc','Error in callout cableRecordEnquiry' );
                networkResponse.put('errorCode','UNKNOWN');
                //networkResponse.put('payload','');
            }
        }
        else if(!String.isBlank(lstCache.get(0).JSONPayload__c)){
            
            String jsonString = lstCache.get(0).JSONPayload__c;
            networkResponse.put('payload',jsonString);
        }
        
        networkResponse.put('timeoutInMilliSec',String.valueOf(timeoutInMilliSec));
        System.debug('@@@networkResponse: ' + networkResponse);
        string networkResponseString = JSON.serialize(networkResponse);
        System.debug('@@@networkResponseString: ' + networkResponseString);
        return networkResponseString;
        
    }
    
    private static List<NBNIntegrationCache__c> checkRecordInCache(String cpiId, String technologyType){
       
        List<NBNIntegrationCache__c> lstCache = [SELECT Id, JSONPayload__c, Transaction_Id__c, CPI_Id__c FROM NBNIntegrationCache__c 
                                            WHERE CPI_Id__c =:cpiId AND integrationtype__c = 'NetworkTrail'  limit 1];
        return (lstCache);
    }
    
     // Calling from Lightning component JIGSAW_NetworkTrail  waiting or response from PNI
    @AuraEnabled 
    public static String  requestForPNIResponse(String cpiId, String incidentId, String technologyType){
        Map<String,String> networkResponse = new Map<String,String>();
        List<NBNIntegrationCache__c> lstCache = checkRecordInCache(cpiId, technologyType);
        if(!lstCache.isEmpty() && lstCache.size() > 0 ){
            String jsonString = lstCache.get(0).JSONPayload__c;
            networkResponse.put('payload',jsonString);
        }
        System.debug('@@@networkResponse: ' + networkResponse);
        string networkResponseString = JSON.serialize(networkResponse);
        System.debug('@@@networkResponseString: ' + networkResponseString);
        return networkResponseString;
    }
   // END 
  
}