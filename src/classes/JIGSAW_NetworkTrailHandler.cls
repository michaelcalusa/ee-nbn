public class JIGSAW_NetworkTrailHandler {
    
    // Save Network Trail in NBN Integration Cache
    // Update Incident Mgmt record so LDS can be triggers at JIGSAW
  /*  public static void saveNetworkTrail(List<NetworkTrail__e > lstTrails){
        integrationWrapper.NetWorkTrialResp objWrapper = new integrationWrapper.NetWorkTrialResp();
        
        for(NetworkTrail__e trail :lstTrails){
            String strJSON = String.valueOf(trail.InboundJSONMessage__c);
            System.debug('@@@' + strJSON);
            
            try{
                objWrapper = (integrationWrapper.NetWorkTrialResp)JSON.deserialize(strJSON, integrationWrapper.NetWorkTrialResp.Class);
            }
            catch(Exception ex){
                GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'JIGSAW_NetworkTrailHandler','Error in parsing response','','','','', ex, 0);
                return;
            }
            
            System.debug('@@@' + objWrapper.transactionId);
            System.debug('@@@' + objWrapper.metadata.copperPathId);
            
            // Insert Cache record
            NBNIntegrationCache__c objCache =  new NBNIntegrationCache__c();
            objCache.Transaction_Id__c = objWrapper.transactionId;
            objCache.CPI_Id__c = objWrapper.metadata.copperPathId;
            objCache.JSONPayload__c = strJSON;
            objCache.IntegrationType__c = 'NetworkTrail';
            upsert objCache;
            
            
            // Update Incident Management Record
            Incident_Management__c objIM = [SELECT Id, Network_Trail_Transaction_Id__c FROM Incident_Management__c WHERE Network_Trail_Transaction_Id__c = :objWrapper.transactionId].get(0);
            objIM.Network_Trail_Transaction_Id__c = objIM.Network_Trail_Transaction_Id__c + '_COMPLETED';
            update objIM;
            System.debug('@@@' + objIM);
            
        }
    } */
    // Commented above method. Created below updated method.
    // START - Network Trail Code Refactor - Lokesh
    public static void saveNetworkTrail(List<NetworkTrail__e > lstTrails){
        
       // integrationWrapper.NetWorkTrialResp objWrapper = new integrationWrapper.NetWorkTrialResp();
        Map<String, Object> responseMap ;
        List<NBNIntegrationCache__c> lstCache = new List<NBNIntegrationCache__c>();
        Set<String> transactionIds = new Set<String>();
        for(NetworkTrail__e trail :lstTrails){
            String strJSON = String.valueOf(trail.InboundJSONMessage__c);
            System.debug('@@@ Json - ' + strJSON);
            responseMap = new Map<String, Object>();
            try{
                responseMap = (Map<String, Object>) JSON.deserializeUntyped(strJSON);
            }
            catch(Exception ex){
                GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'JIGSAW_NetworkTrailHandler','Error in parsing response','','','','', ex, 0);
                return;
            }
            System.debug('responseMap' + responseMap);
            System.debug('responseMap trasaction' + (String)responseMap.get('transactionId'));
            System.debug('responseMap metadata' + responseMap.get('metadata'));
            //System.debug('responseMap copper' + responseMap.get('metadata').copperPathId);
            
            // Insert Cache record
            NBNIntegrationCache__c objCache =  new NBNIntegrationCache__c();
            objCache.Transaction_Id__c = (String)responseMap.get('transactionId');
            Object metadataObj = (Object) responseMap.get('metadata');
            Map<String, Object> metadataMap = (Map<String, Object>)metadataObj;
            objCache.CPI_Id__c = (String) metadataMap.get('copperPathId');
            objCache.JSONPayload__c = strJSON;
            objCache.IntegrationType__c = 'NetworkTrail';
            lstCache.add(objCache);
            
            transactionIds.add((String)responseMap.get('transactionId'));
        }
        if(!lstCache.isEmpty() && lstCache.size() > 0){
            upsert lstCache;
            if(!transactionIds.isEmpty() && transactionIds.size() >0 ){
                List<Incident_Management__c> lstIM = [SELECT Id, Network_Trail_Transaction_Id__c FROM Incident_Management__c WHERE Network_Trail_Transaction_Id__c = :transactionIds];
                if(!lstIM.isEmpty() && lstIM.size() > 0){
                    for( integer i = 0; i < lstIM.size(); i++){
                        lstIM[i].Network_Trail_Transaction_Id__c = lstIM[i].Network_Trail_Transaction_Id__c + '_COMPLETED';
                    }
                    update lstIM;
                }
            }
        }
    }
}