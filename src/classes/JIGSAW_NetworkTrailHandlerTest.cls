@isTest
private class JIGSAW_NetworkTrailHandlerTest {
    
    // Test Data
    @testSetup static void setup() {
        // create custom setting records for Network Trail
        List<SObject> lstToInsert = new List<SObject>();
        
        
        Incident_Management__c obj = new Incident_Management__c();
        obj.Appointment_Start_Date__c = System.now();
        obj.Appointment_End_Date__c = System.now();
        obj.Target_Commitment_Date__c = System.now();
        obj.Appointment_Start_Date__c = System.now();
        obj.Appointment_End_Date__c = System.now();
        obj.Target_Commitment_Date__c = System.now();
        obj.PRI_ID__c = 'PRI003000710422';
        obj.Network_Trail_Transaction_Id__c = 'test';
        lstToInsert.add(obj);
        
        
        
      
        
        insert lstToInsert;
        
    }
    
    @isTest static void testMethod1() {
        
        Test.startTest();
        
        List<NetworkTrail__e> lst = new List<NetworkTrail__e>();
        
        
        lst.add(new NetworkTrail__e(InboundJSONMessage__c = networkTrailJSON()));
        
        
        
        
        EventBus.publish(lst);
        
        Test.stopTest();
    }
    
    
    @isTest static void testMethod2() {
        
        Test.startTest();
        
        List<NetworkTrail__e> lst = new List<NetworkTrail__e>();
        
        String jsn = 'invalid json response';
        
        lst.add(new NetworkTrail__e(InboundJSONMessage__c = jsn));
        
        
        
        
        EventBus.publish(lst);
        
        Test.stopTest();
    }
    
    
   
    
    static String networkTrailJSON() {
		return		'{'+
		'	"transactionId": "test",'+
		'	"status": "Completed",'+
		'	"metadata": {'+
		'		"nbnCorrelationId": "0e8cedd0-ad98-11e6-bf2e-47644ada7c0f",'+
		'		"tokenId": "3c08d723a26f1234",'+
		'		"copperPathId": "CPI300000403245",'+
		'		"accessServiceTechnologyType": "Fibre To The Node",'+
		'		"incidentId": "INC000000012"'+
		'	},'+
		'	"tco": {'+
		'		"id": "148689111451627491",'+
		'		"name": "",'+
		'		"type": "Premise Equipment",'+
		'		"subtype": "TCO",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "1291492788",'+
		'		"locationId": "",'+
		'		"locationLatitude": "",'+
		'		"locationLongitude": ""'+
		'	},'+
		'	"mdf": {'+
		'		"id": "E0001",'+
		'		"name": "P17:194",'+
		'		"type": "Joint",'+
		'		"subtype": "TAP",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "781274058",'+
		'		"locationId": "LOC000012345679",'+
		'		"locationLatitude": "-37.6854",'+
		'		"locationLongitude": "144.565",'+
		'		"portId": "157",'+
		'		"portName": "P17:194",'+
		'		"portPlatformReferenceId": "893746589",'+
		'		"portStatus": "Designed",'+
		'		"portInstallationStatus": "Not Installed"'+
		'	},'+
		'	"copperLeadinCable": {'+
		'		"id": "182219421351754719",'+
		'		"name": "4TBA0111DPU001:4-4|182219421351754719:2-2",'+
		'		"type": "Cable",'+
		'		"subtype": "CLS",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "1291492811",'+
		'		"length": "35.16",'+
		'		"lengthType": "",'+
		'		"installationType": "Underground",'+
		'		"twistedPairId": "1",'+
		'		"twistedPairName": "4TBA0111DPU001:4",'+
		'		"twistedPairPlatformReferenceId": "1291492813",'+
		'		"twistedPairStatus": "In Service",'+
		'		"twistedPairInstallationStatus": "Installed"'+
		'	},'+
		'	"copperDistributionCables": [{'+
		'		"id": "4TBA-01-11-CDS-004",'+
		'		"name": "4TBA0111CCU001:4-4",'+
		'		"type": "Cable",'+
		'		"subtype": "CDS",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "1291492821",'+
		'		"length": "69",'+
		'		"lengthType": "",'+
		'		"twistedPairCapacity": "",'+
		'		"twistedPairId": "1",'+
		'		"twistedPairName": "4TBA0111CCU001:4",'+
		'		"twistedPairPlatformReferenceId": "1291492823",'+
		'		"twistedPairStatus": "In Service",'+
		'		"twistedPairInstallationStatus": "Installed"'+
		'	}],'+
		'	"pillars": [{'+
		'		"id": "101559892156683127",'+
		'		"name": "4TBA-01-11-CCU-001",'+
		'		"type": "Pillar",'+
		'		"subtype": "CCU",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "1291491672",'+
		'		"distributionPairCapacity": "900",'+
		'		"terminationModules": [{'+
		'				"id": "101559892156683127_O",'+
		'				"name": "4TBA0111CCU001:1-1200",'+
		'				"type": "Termination Module",'+
		'				"subtype": "O Pair",'+
		'				"status": "In Service",'+
		'				"platformReferenceId": "1291491676",'+
		'				"twistedPairId": 4,'+
		'				"twistedPairName": "4TBA0111CCU001:4",'+
		'				"twistedPairPlatformReferenceId": "713844058",'+
		'				"twistedPairStatus": "In Service",'+
		'				"twistedPairInstallationStatus": "Installed"'+
		'			},'+
		'			{'+
		'				"id": "101559892156683127_C",'+
		'				"name": "4TBA0111CCU002:1-1200",'+
		'				"type": "Termination Module",'+
		'				"subtype": "C Pair",'+
		'				"status": "In Service",'+
		'				"platformReferenceId": "1291491678",'+
		'				"twistedPairId": 4,'+
		'				"twistedPairName": "4TBA0111CCU002:4",'+
		'				"twistedPairPlatformReferenceId": "713844059",'+
		'				"twistedPairStatus": "In Service",'+
		'				"twistedPairInstallationStatus": "Installed"'+
		'			}'+
		'		]'+
		'	}],'+
		'	"joint": {'+
		'		"id": "000000015032224032",'+
		'		"type": "Joint",'+
		'		"subtype": "CJL",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "1446074012",'+
		'		"locationLatitude": "-38.02440142",'+
		'		"locationLongitude": "145.2447201",'+
		'		"supportStructureType": "Pit"'+
		'	},'+
		'	"fttnNode": {'+
		'		"id": "6CAN-08-04-FNO-001",'+
		'		"name": "",'+
		'		"type": "FTTN Node",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "727426051",'+
		'		"dslamNetworkResource": {'+
		'			"id": "3LYN-24-12-DSL-0001",'+
		'			"name": "SWDSL0217261",'+
		'			"type": "Chassis",'+
		'			"subtype": "DSL",'+
		'			"status": "In Service",'+
		'			"platformReferenceId": "1401059993"'+
		'		}'+
		'	},'+
		'	"copperMainCable": {'+
		'		"id": "M0002",'+
		'		"name": "EXCH01 M2:1201-1350",'+
		'		"type": "Cable",'+
		'		"subtype": "CMS",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "713844079",'+
		'		"length": "",'+
		'		"lengthType": "",'+
		'		"twistedPairId": "56",'+
		'		"twistedPairName": "EXCH01 M2:1256",'+
		'		"twistedPairPlatformReferenceId": "451604097",'+
		'		"twistedPairStatus": "In Service",'+
		'		"twistedPairInstallationStatus": "Installed"'+
		'	}'+
		'}'+
		'';
    }
    
    
}