/***************************************************************************************************
Class Name:         JIGSAW_WorkOrderWrapper
Class Type:         WrapperClass 
Company :           Appirio
Version:            1.0 
Created Date:       27 June 2018
Function:           
Input Parameters:   None 
Output Parameters:  None
Description:        Wrapper class to hold deserialized JSON value. This wrapper class is created to display 
work order modifications on incident management record page when user clicks on Show Serice Info link under ViewOrderHistory Component.
Created as part of CUSTSA-15944.
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Rohit Mathur      27/06/2018      Created - Version 1.0 
****************************************************************************************************/
@isTest
public class JIGSAW_ProductOrderWrapper {
    public cls_data[] data;
    public cls_included[] included;
    public class cls_data {
        public String id; //AVC000003083996
        public String type; //avc-d
        public cls_attributes attributes;
        public cls_relationships relationships;
    }
    public class cls_attributes {
        public String orderType; //Connect
        public String orderStatus; //Complete
        public String dateAccepted; //11/10/2017 10:29:00
        public String dateComplete; //19/02/2018 17:15:14
        public cls_orderAttrs orderAttrs;
        public String locationId; //LOC000090284813
        public String orderCompletionDate;
    }
    public class cls_relationships {
        public cls_accessSeeker accessSeeker;
        public cls_services services;
    }
    public class cls_accessSeeker {
        public cls_data data;
        public cls_links links;
    }
    public class cls_links {
        public String related; //https://cis-api-prod-dc2.slb.nbndc.local/rest/services/v1/avcs/
    }
    public class cls_services {
        public cls_data[] data;
        public cls_links links;
    }
    public class cls_included {
        public String type; //ProductOrder
        public String id; //ORD020025921854
        public cls_attributes attributes;
    }
    public class cls_orderAttrs {
        public String action; //ADD
        public String priorityAssist; //No
        public String serviceRestorationSla; //Standard
        public String informedConsent; //Yes
        public String batteryBackupService; //No
        public String orderSla; //Standard
        public cls_nfasNtd nfasNtd;
        public cls_uniD[] uniD;
        public cls_avcD[] avcD;
        public cls_avcM[] avcM;
    }
    public class cls_nfasNtd {
        public String action; //ADD
        public String location; //Indoor
        public String ntdType; //Internal
    }
    public class cls_uniD {
        public String action; //ADD
        public String uniType; //UNI-D
        public String vlanMode; //Default-Mapped
        public String fromPortId; //0
        public String interfaceType; //10/100/1000BASE-T
        public String portId; //0
        public String speedAndDuplex; //Auto Negotiation (10/100/1000Mbps,Half/Full Duplex)
        public String uniTransientId; //UNI-D1
    }
    public class cls_avcD {
        public String action; //ADD
        public String avcType; //1:1
        public String bandwidthProfile; //D25_U10_Mbps_TC4_P, D5_U5_Mbps_TC2_C
        public String cvcId; //CVC000000455871
        public String nniCvlanId; //1246
        public String accessLoopIdentification; //Active
        public String avcTransientId; //AVC-D1
        public String connectedToUni; //UNI-D1
    }
    public class cls_avcM {
        public String action; //ADD
        public String avcType; //Multicast
        public String bandwidthProfile; //D5_Mbps_TCMC_C
        public String cvcId; //CVC012345678904
        public String connectedToAvc; //AVC-D1
    }
    public static JIGSAW_ProductOrderWrapper parse(String json) {
        return (JIGSAW_ProductOrderWrapper) System.JSON.deserialize(json, JIGSAW_ProductOrderWrapper.class);
    }

    static testMethod void testParse() {
        String json = '{' +
            '   "data":[' +
            '      {' +
            '         "id":"PRI000018801652",' +
            '         "type":"nfasProductInstance",' +
            '         "attributes":{' +
            '            "locationId":"LOC000118847663",' +
            '            "primaryAccessTechnology":"Fibre",' +
            '            "priorityAssist":"No",' +
            '            "productStatus":"Active",' +
            '            "orderCompletionDate":"07/05/2014 11:28:29"' +
            '         },' +
            '         "relationships":{' +
            '            "accessSeeker":{' +
            '               "data":{' +
            '                  "id":"ASI000000000035",' +
            '                  "type":"accessSeeker"' +
            '               },' +
            '               "links":{' +
            '                  "related":"http://cis-enterprise-search-slb.cisc.intprod.nbn-aws.local/accessseeker/v1/info/ASI000000000035"' +
            '               }' +
            '            },' +
            '            "services":{' +
            '               "data":[' +
            '                  {' +
            '                     "id":"AVC000003083996",' +
            '                     "type":"avc-d"' +
            '                  }' +
            '               ],' +
            '               "links":{' +
            '                  "related":"https://cis-api-prod-dc2.slb.nbndc.local/rest/services/v1/avcs/"' +
            '               }' +
            '            }' +
            '         }' +
            '      }' +
            '   ],' +
            '   "included":[' +
            '      {' +
            '         "type":"ProductOrder",' +
            '         "id":"ORD020025921854",' +
            '         "attributes":{' +
            '            "orderType":"Connect",' +
            '            "orderStatus":"Complete",' +
            '            "dateAccepted":"11/10/2017 10:29:00",' +
            '            "dateCompleted":"19/02/2018 17:15:14",' +
            '            "orderAttrs":{' +
            '               "action":"ADD",' +
            '               "priorityAssist":"No",' +
            '               "serviceRestorationSla":"Standard",' +
            '               "informedConsent":"Yes",' +
            '               "batteryBackupService":"No",' +
            '               "orderSla":"Standard",' +
            '               "nfasNtd":{' +
            '                  "action":"ADD",' +
            '                  "location":"Indoor",' +
            '                  "ntdType":"Internal"' +
            '               },' +
            '               "uniD":[{' +
            '                  "action":"ADD",' +
            '                  "uniType":"UNI-D",' +
            '                  "vlanMode":"Default-Mapped",' +
            '                  "fromPortId":"0",' +
            '                  "interfaceType":"10/100/1000BASE-T",' +
            '                  "portId":"0",' +
            '                  "speedAndDuplex":"Auto Negotiation (10/100/1000Mbps,Half/Full Duplex)",' +
            '                  "uniTransientId":"UNI-D1"' +
            '               }],' +
            '               "avcD":[{' +
            '                  "action":"ADD",' +
            '                  "avcType":"1:1",' +
            '                  "bandwidthProfile":"D25_U10_Mbps_TC4_P, D5_U5_Mbps_TC2_C",' +
            '                  "cvcId":"CVC000000455871",' +
            '                  "nniCvlanId":"1246",' +
            '                  "accessLoopIdentification":"Active",' +
            '                  "avcTransientId":"AVC-D1",' +
            '                  "connectedToUni":"UNI-D1"' +
            '               }],' +
            '               "avcM":[{' +
            '                  "action":"ADD",' +
            '                  "avcType":"Multicast",' +
            '                  "bandwidthProfile":"D5_Mbps_TCMC_C",' +
            '                  "cvcId":"CVC012345678904",' +
            '                  "connectedToAvc":"AVC-D1"' +
            '               }]' +
            '            }' +
            '         }' +
            '      },' +
            '      {' +
            '         "type":"ProductOrder",' +
            '         "id":"ORD020039778026",' +
            '         "attributes":{' +
            '            "orderType":"Modify",' +
            '            "orderStatus":"In Progress",' +
            '            "dateAccepted":"22/02/2018 14:56:43",' +
            '            "orderAttrs":{' +
            '               "action":"MODIFY",' +
            '               "id":"PRI000053946957",' +
            '               "informedConsent":"Yes",' +
            '               "batteryBackupService":"Yes",' +
            '               "nfasNtd":{' +
            '                  "action":"NO CHANGE",' +
            '                  "id":"NTD000037135691"' +
            '               },' +
            '               "uniV":[' +
            '                  {' +
            '                     "action":"ADD",' +
            '                     "uniType":"UNI-V",' +
            '                     "portId":"0",' +
            '                     "uniTransientId":"UNI-V1",' +
            '                     "configurationMode":"TR-069"' +
            '                  }' +
            '               ],' +
            '               "uniD":[{' +
            '                  "action":"NO CHANGE",' +
            '                  "id":"UNI000046834467"' +
            '               }],' +
            '               "avcV":[' +
            '                  {' +
            '                     "action":"ADD",' +
            '                     "avcType":"N:1",' +
            '                     "bandwidthProfile":"D0.15_U0.15_Mbps_TC1_C",' +
            '                     "cvcId":"CVC000000299920",' +
            '                     "connectedToUni":"UNI-V1"' +
            '                  }' +
            '               ],' +
            '               "avcD":[{' +
            '                  "action":"NO CHANGE",' +
            '                  "id":"AVC000047039892"' +
            '               }]' +
            '            }' +
            '         }' +
            '      }' +
            '   ]' +
            '}';
        JIGSAW_ProductOrderWrapper obj = parse(json);
        System.assert(obj != null);
    }
}