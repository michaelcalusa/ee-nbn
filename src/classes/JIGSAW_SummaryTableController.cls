/*------------------------------------------------------------
Author: Sunil Gupta June 23, 2018
Company: Appirio
Description: Server-side controller for 3 Lightning components that used to display More details about Incidents, Service Orders and WO.
History
<Date>      <Author>     		<Description>
17-01-2019	Syed Moosa Nazir	CUSTSA-23140 - MR1902 release. Added AVC ID column in Incident History Module.
24-01-2019	Syed Moosa Nazir	CUSTSA-24874, CUSTSA-24875 - Consume CIS getAppointment inplace of MS getAppointment service. Implemented Toggle feature. 
								Method: "getAppointmentDetailsFromCIS"
------------------------------------------------------------*/
public class JIGSAW_SummaryTableController {
    
    @AuraEnabled
    public static SummaryWrapper fetchRecords(String callingComponent, String filter, String dayCount, String locId) {
        if (callingComponent == 'INCIDENT') {
            return fetchIncidents(filter, locId);
        }
        if (callingComponent == 'SERVICE') {
            return fetchServiceChanges(filter);
        }
        if (callingComponent == 'WORKORDER') {
            return fetchWorkOrder(filter, dayCount, locId);
        }
        return null;
    }
    
    private static SummaryWrapper fetchServiceChanges(String filter) {
        
        List < DataTableColumns > cols = new list < DataTableColumns > ();
        List < ServiceChangeDataWrapper > lstSobjectData = new List < ServiceChangeDataWrapper > ();
        SummaryWrapper objWrapper = new SummaryWrapper();
        String priId ;
        String jsonPayload ;
        String AccessSeekerID;
        //set columns
        cols.add(new DataTableColumns('Orderid', 'Order ID', 'button', false));
        cols.add(new DataTableColumns('orderType', 'Type', 'text', false));
        cols.add(new DataTableColumns('orderStatus', 'Status', 'text', false));
        cols.add(new DataTableColumns('dateAccepted', 'Raised', 'text', true));
        cols.add(new DataTableColumns('dateCompleted', 'Completed', 'text', true));
        
        if (String.isNotBlank(filter)) {
            List < Incident_Management__c > incList = [select id, PRI_ID__c, Access_Seeker_ID__c from Incident_Management__c where id =: filter];
            JIGSAW_ProductOrderWrapper RtPObject;
            if (!incList.isEmpty()) {
                
                priId = incList.get(0).PRI_ID__c;
                AccessSeekerID = incList.get(0).Access_Seeker_ID__c;
                
                //check in integration cache for Product order 
                List<NBNIntegrationCache__c> lstnbnInt = [SELECT id,PRIId__c,JSONPayload__c from NBNIntegrationCache__c where PRIId__c=:priId AND IntegrationType__c = 'OrderHistory'];
                if(lstnbnInt.size() > 0){
                    for(NBNIntegrationCache__c nInt:lstnbnInt){
                        if(nInt.PRIId__c == PRIid){
                            jsonPayload = nInt.JSONPayload__c;
                        }
                    }
                    if (String.isNotBlank(jsonPayload)) {
                        System.debug('Service changes cahce body :: ' + jsonPayload);
                        //deserialize the product order response to be displayed on summary table
                        RtPObject = (JIGSAW_ProductOrderWrapper) JSON.deserialize(jsonPayload, JIGSAW_ProductOrderWrapper.Class);
                        
                        //check for included to get order details
                        if (RtPObject.included != null && !RtPObject.included.isEmpty()) {
                            for (JIGSAW_ProductOrderWrapper.cls_included includedData: RtPObject.included) {
                                //set column data
                                ServiceChangeDataWrapper wrap = new ServiceChangeDataWrapper();
                                wrap.Orderid = includedData.id;
                                wrap.orderType = includedData.attributes.orderType;
                                wrap.orderStatus = includedData.attributes.orderStatus;
                                wrap.dateAccepted = includedData.attributes.dateAccepted;
                                wrap.dateCompleted = includedData.attributes.dateComplete;
                                
                                lstSobjectData.add(wrap);
                            }
                        }
                        
                        // Fill Wrapper
                        objWrapper.lstColumns = cols;
                        objWrapper.totalRecords = lstSobjectData.size();
                        objWrapper.lstSobjectData = lstSobjectData;
                        objWrapper.priId = priId;
                        objWrapper.deserializedObject = jsonPayload;
                    }
                }else{
                    //show error if no records found in integration cahce for product order history
                    objWrapper.errorOccured = true;
                }
            }
        }else{
            //show error id incident record id was not passed from lightning component
            objWrapper.errorOccured = true;
        }
        
        return objWrapper;
    }
    
    public static SummaryWrapper fetchWorkOrder(String filter, String dayCount, String locId) {
        SummaryWrapper objWrapper = new SummaryWrapper();
        List < DataTableColumns > cols = new list < DataTableColumns > ();
        List < WorkOrderDataWrapper > lstSobjectData = new List < WorkOrderDataWrapper > ();
        
        //set columns
        cols.add(new DataTableColumns('incident', 'Incident', 'url', false));
        cols.add(new DataTableColumns('workRequest', 'Work Request', 'text', false));
        cols.add(new DataTableColumns('faultType', 'Fault Type', 'text', false));
        cols.add(new DataTableColumns('parentWorkOrder', 'Parent Work Order', 'text', false));
        cols.add(new DataTableColumns('workOrder', 'Work Order', 'url', false));
        cols.add(new DataTableColumns('raised', 'Raised', 'text', true));
        cols.add(new DataTableColumns('completed', 'Completed', 'text', true));
        cols.add(new DataTableColumns('reasonCode', 'Reason Code', 'text', false));
        
        
        try{
            if (String.isNotBlank(filter)) {
                JIGSAW_WorkHistoryWrapper workHistoryObject;
                
                // Call Work History API 
                JIGSAW_WorkOrderModuleController.Wrapper_WorkHistoryResponse objResponse = JIGSAW_WorkOrderModuleController.getData_WorkHistory(filter, dayCount,locId);
                
                String errorCode = objResponse.errorCode; 
                String errorDetails = objResponse.errorDetails;
                String response = objResponse.response;
                
                
                if(String.isBlank(locId) == true){
                    List < Incident_Management__c > incList = [select id, locId__c from Incident_Management__c where id =: filter];
                    locId = incList.get(0).locId__c;
                }
                
                if(String.isBlank(locId) == true){
                 	objWrapper.totalRecords = 0;
            		objWrapper.errorDetails = 'Location Id not found.';
            		objWrapper.errorCode = '1000';
            		return objWrapper;
                }
                
                
                
                
                if (String.isBlank(response) == false){
                    
                    workHistoryObject = (JIGSAW_WorkHistoryWrapper) JSON.deserialize(response, JIGSAW_WorkHistoryWrapper.class);
                    
                    //BodyfalseJIGSAW_WorkHistoryWrapper.Class);
                    
                    Set<String> uniqueIncidentNumbers = new Set<String>();
                    Set<String> uniqueIncidentNumbers2 = new Set<String>();
                    Set<String> uniqueWRQ = new Set<String>();
                    
                    
                    List<WorkOrderDataWrapper > incidentnumbertoIdsMap = new List<WorkOrderDataWrapper>();        
                    //check for included to get order details
                    if (workHistoryObject <> null && workHistoryObject.member != null && !workHistoryObject.member.isEmpty()) {
                        for (JIGSAW_WorkHistoryWrapper.cls_member memberData: workHistoryObject.member) {
                            String nbnReferenceId = memberData.nbnreferenceid;
                            if (memberData.nbnworkhistoryview != null && !memberData.nbnworkhistoryview.isEmpty() && memberData.nbncategory == 'SERVICE RESTORATION' && nbnReferenceId.startsWith('INC')) {
                                for (JIGSAW_WorkHistoryWrapper.cls_nbnworkhistoryview historyData: memberData.nbnworkhistoryview) {
                                    //set col data
                                    if(!(historyData.status == 'CANCEL' || historyData.status == 'WITHDRAWN' || historyData.status == 'ALLOCATED' || historyData.status == 'REJECTED' || historyData.status == 'CLOSE' || historyData.status == 'CAN-INFLIGHT')){
                                        WorkOrderDataWrapper wrap = new WorkOrderDataWrapper();
                                        wrap.incident = historyData.nbnreferenceid;
                                        wrap.workRequest = historyData.nbnticketid;
                                        wrap.parentWorkOrder = historyData.parent;
                                        wrap.workOrder = historyData.wonum;
                                        wrap.reasonCode= historyData.nbnreasoncode;
                                        string dtReport = historyData.reportdate;
                                        wrap.raised = dtReport.substring(8,10)+'/'+dtReport.substring(5,7)+'/'+dtReport.substring(0,4)+' @ '+dtReport.substring(11,16) ;
                                        if(historyData.status=='COMP'){
                                            string dtComp = historyData.statusdate ;
                                            wrap.completed = dtComp.substring(8,10)+'/'+dtComp.substring(5,7)+'/'+dtComp.substring(0,4)+' @ '+dtComp.substring(11,16);
                                        }
                                        incidentnumbertoIdsMap.add(wrap);
                                        uniqueIncidentNumbers.add(wrap.workOrder);
                                        uniqueIncidentNumbers2.add(wrap.incident);
                                        uniqueWRQ.add(wrap.workRequest);
                                    }
                                }
                            }
                        }
                    }
                    
                    // prepare map for Incident Numbers;
                    Map<String, workorder> mapSFDCIds = new Map<String, workorder>();
                    
                    system.debug('Filter 2 -->' + uniqueWRQ);
                    
                    for(workorder wo :[select Id, WorkOrderNumber, wrqIdentifier__c,Incident__c, Incident__r.name,faultType__c from workorder where wrqIdentifier__c in : uniqueWRQ]){
                        mapSFDCIds.put(wo.wrqIdentifier__c, wo);
                    }
                    
                    // prepare map for Incident Numbers;
                    Map<String, Incident_Management__c> mapSFDCIds2 = new Map<String, Incident_Management__c>();
                    for(Incident_Management__c inc :[select Id, Name from Incident_Management__c where Name in :uniqueIncidentNumbers2]){
                        mapSFDCIds2.put(inc.name, inc);
                    }
                    
                    for(WorkOrderDataWrapper wrap :incidentnumbertoIdsMap){
                        if(mapSFDCIds.containsKey(wrap.workRequest) == true){
                            wrap.faultType = mapSFDCIds.get(wrap.workRequest).faultType__c;
                        }
                        if(mapSFDCIds2.containsKey(wrap.incident) == true){
                            wrap.sfdcId = mapSFDCIds2.get(wrap.incident).Id;
                        }else{
                            wrap.sfdcId = 'Not Available';
                        }
                    }
                    
                    // Fill Wrapper
                    objWrapper.lstColumns = cols;
                    objWrapper.totalRecords = incidentnumbertoIdsMap.size();
                    objWrapper.lstSobjectData = incidentnumbertoIdsMap;
                    objWrapper.locId = locId ; 
                    objWrapper.errorDetails = errorDetails;
                    objWrapper.errorCode = errorCode;
                    if(objResponse.cacheRec != null){
                        objWrapper.deserializedObject = objResponse.cacheRec;
                    }
                    return objWrapper;
                }
                else{
                    
                    objWrapper.totalRecords = 0;
                    objWrapper.errorDetails = errorDetails;
                    objWrapper.errorCode = errorCode;
                    
                }
                
            }
        }
        catch(exception ex){
            objWrapper.totalRecords = 0;
            objWrapper.errorDetails = ex.getMessage();
            objWrapper.errorCode = '1000';
            return objWrapper;
        }
        
        
        return objWrapper;
    }
    
    
    
    
    private static SummaryWrapper fetchIncidents(String filter, String locId) {
        // prepare columns
        List < DataTableColumns > lstColumns = new List < DataTableColumns > ();
        List < String > strFields = new List < String > ();
        /*
strFields = new List<String>{'Incident_Number__c', 'Fault_Type__c', 
'repeatIncResolutionGoal__c', 'ResolutionCategoryTier3__c', 'Reported_Date__c', 
'Closed_Date__c', 'Resolution_Rejection_Code__c', 'MTTR__c'};

*/
        //for (Schema.FieldSetMember field: SObjectType.Incident_Management__c.FieldSets.Incident_Summary_Table.getFields()) {
        lstColumns.add(new DataTableColumns('Incident_Number__c', 'Incident', 'url', false)); 
        lstColumns.add(new DataTableColumns('Fault_Type__c', 'Fault Type', 'text', false));
        lstColumns.add(new DataTableColumns('repeatIncResolutionGoal__c', 'Goal', 'text', false));
        lstColumns.add(new DataTableColumns('AVC_Id__c', 'AVC', 'text', true));
        lstColumns.add(new DataTableColumns('Access_Seeker_Name__c', 'RSP', 'text', false));
        lstColumns.add(new DataTableColumns('MTTR__c', 'TTR', 'text', true));
        lstColumns.add(new DataTableColumns('Reported_Date__c', 'Raised', 'text', true));
        lstColumns.add(new DataTableColumns('closedDate__c', 'Closed', 'text', true));
        lstColumns.add(new DataTableColumns('industryResolutionDescription__c', 'Resolution', 'text', false));
        
        strFields.add('Incident_Number__c');
        strFields.add('Fault_Type__c');
        strFields.add('repeatIncResolutionGoal__c');
        strFields.add('AVC_Id__c');
        strFields.add('Access_Seeker_Name__c');
        strFields.add('MTTR__c');
        strFields.add('Reported_Date__c');
        strFields.add('closedDate__c');
        strFields.add('industryResolutionDescription__c');
        //}
        System.debug('locId==>' + locId);
        if(String.isBlank(locId) == true){
            List < Incident_Management__c > incList = [select Id, locId__c, Technology__c, Reported_Date__c from Incident_Management__c where id =:filter];
            if (!incList.isEmpty()) {
                locId = incList.get(0).locId__c;
            }
        }
        System.debug('@@@' + locId);
        List <Incident_Management__c> lstIM = [SELECT Id, locId__c, Incident_Number__c, Technology__c, Fault_Type__c,repeatIncResolutionGoal__c,Access_Seeker_Name__c,
                                               MTTR__c, Reported_Date__c,closedDate__c,industryResolutionDescription__c, AVC_Id__c FROM Incident_Management__c 
                                               WHERE Reported_Date__c = LAST_N_DAYS:90 
                                               AND Industry_Status__c != 'Rejected'
                                               AND  (locId__c = :locId OR Id = :filter)];
        
        
        System.debug('@@@' + lstIM);
        // filter list by Location Id
        List<Sobject> lstRecords = new List<SObject>();                          
        for (Incident_Management__c obj: lstIM) {
            System.debug('@@@' + obj.Id );
            System.debug('@@@' + filter);
            System.debug('@@@' + obj.locId__c);
            if(obj.locId__c == locId || obj.Id == filter){
                lstRecords.add(obj);
            }
        }
        
        System.debug('@@@' + lstRecords);
        
        // Fill Wrapper
        SummaryWrapper objWrapper = new SummaryWrapper();
        objWrapper.lstColumns = lstColumns;
        objWrapper.totalRecords = lstRecords.size();
        objWrapper.lstSobjectData = lstRecords;
        objWrapper.locId = locId;
        return objWrapper;
    }
    
    
    public static list<NBNIntegrationCache__c> cacheWorkHistoryIntegration(String LocId,HttpResponse response){
        list<NBNIntegrationCache__c> cacheList = new list<NBNIntegrationCache__c>();
        JIGSAW_WorkHistoryWrapper workHistoryObject;
        String strJson = '';
        /*List < Incident_Management__c > incList = [select id, locId__c, Reported_Date__c from Incident_Management__c where id =: filter];
locId = incList.get(0).locId__c;
datetime nowdt = system.now();
String reportedDateFinal = nowdt.format('yyyy-MM-dd'); 
if(incList.get(0).Reported_Date__c != null){
reportedDateFinal = incList.get(0).Reported_Date__c.format('yyyy-MM-dd'); 
}
String recId = incList.get(0).id;*/
        
        if(response.getBody()!='{}'&& response.getBody()!='' && response.getStatusCode() == 200){
            workHistoryObject = (JIGSAW_WorkHistoryWrapper) JSON.deserialize(response.getBody(), JIGSAW_WorkHistoryWrapper.Class);
            strJson = String.valueof(response.getBody());
        }
        NBNIntegrationCache__c nbnIntcache = new NBNIntegrationCache__c();
        nbnIntcache.IntegrationType__c = 'WorkHistoryInfo';
        nbnIntcache.JSONPayload__c = String.valueOf(strJson);
        nbnIntcache.LocId__c = LocId;
        if(strJson!=null && !String.Isblank(strJson)){
            //AsyncCache updateJob = new AsyncCache(nbnIntcache);
            // enqueue the job for processing
            //ID jobID = System.enqueueJob(updateJob);
            cacheList.add(nbnIntcache);
            return cacheList;
        }
        return null;
    }
    
    public class SummaryWrapper {
        @AuraEnabled
        public List < DataTableColumns > lstColumns {
            get;
            set;
        }
        
        @AuraEnabled
        public Integer totalRecords {
            get;
            set;
        }
        
        @AuraEnabled
        public List < Object > lstSobjectData {
            get;
            set;
        }
        
        @AuraEnabled
        public String priId {get;set;}
        
        @AuraEnabled
        public String locId {get;set;}
        
        @AuraEnabled
        public Boolean errorOccured {get;set;}
        
        @AuraEnabled
        public Object deserializedObject {get;set;}
        
        @AuraEnabled
        public String errorCode {get;set;}
        
        @AuraEnabled
        public String errorDetails {get;set;}
    }
    
    
    //Wrapper class to hold Columns with headers
    public class DataTableColumns {
        
        @AuraEnabled
        public String fieldName {
            get;
            set;
        }
        
        @AuraEnabled
        public String label {
            get;
            set;
        }
        
        
        @AuraEnabled
        public Boolean sortable {
            get;
            set;
        }
        
        @AuraEnabled
        public String type {
            get;
            set;
        }
        
        @AuraEnabled
        public String url {
            get;
            set;
        }
        
        public DataTableColumns(String fieldName, String label, String dataType, Boolean sortable) {
            this.fieldName = fieldName;
            this.label = label;
            this.type = dataType;
            this.sortable = sortable;
        }
    }
	// @@getAppointmentDetails -- Controller Method. Called by LC helper.js when JIGSAW_Appointment_CIS_Request_Enable customLabel is "false"
    @AuraEnabled
    public Static Map < String, Object > getAppointmentDetails(String integrationType, String appointmentTransactionId) {
        Map <String, Object> results;
        if(appointmentTransactionId.startsWithIgnoreCase('APT')){
			// Process the existing payload cache from Salesforce. No Callouts made. 
			NBNIntegrationCache__c apptPayloadFromSfCache = fetchApptPayloadFromSfCache('AppointmentDetails', appointmentTransactionId);
			if(apptPayloadFromSfCache <> null && String.isNotBlank(apptPayloadFromSfCache.JSONPayload__c)){
				results = (Map<String, Object>) JSON.deserializeUntyped(apptPayloadFromSfCache.JSONPayload__c);
			}
        }
		// Make Callout with transaction Id
        else{
            HttpResponse getApptAPIResponse = new HttpResponse();
            getApptAPIResponse = integrationUtility.GetAppointmentRequest(integrationType, appointmentTransactionId);
            if(getApptAPIResponse <> null){
                results = (Map<String, Object>) JSON.deserializeUntyped(getApptAPIResponse.getBody());
				// Save the success paylaod in SF Integration Cache object.
                if(((String)results.get('transactionStatus')).equalsIgnoreCase('Completed')){
                    Map <String, Object> appointmentMetadata;
                    appointmentMetadata = (Map <String, Object>)results.get('metadata');
					saveApptPayloadInSfCache((String)appointmentMetadata.get('appointmentId'), getApptAPIResponse.getBody());
                }
				// Failed Callout not saved in Cache. Displayed the error in UI.
				else{
					System.debug('JIGSAW_SummaryTableController==>getAppointmentDetails==>Response Status Code==>' + getApptAPIResponse.getStatusCode());
                    System.debug('JIGSAW_SummaryTableController==>getAppointmentDetails==>Response Status==>' + getApptAPIResponse.getStatus());
                    System.debug('JIGSAW_SummaryTableController==>getAppointmentDetails==>Response Body==>' + getApptAPIResponse.getBody());
				}
            }
        }
        return results;
    }
	// @@fetchApptPayloadFromSfCache -- Controller Method. Called by LC helper.js when JIGSAW_Appointment_CIS_Request_Enable customLabel is "true"
	@AuraEnabled
    public static Map<String,Object> getAppointmentDetailsFromCIS(String integrationType, String accessSeekerId, String appointmentId){
        Map <String, Object> results;
		if(String.isNotBlank(appointmentId) && String.isNotBlank(accessSeekerId)){
			NBNIntegrationCache__c apptPayloadFromSfCache = fetchApptPayloadFromSfCache('AppointmentDetails', appointmentId);
			// Process the existing payload cache from Salesforce. No Callouts made. 
			if(apptPayloadFromSfCache <> null && String.isNotBlank(apptPayloadFromSfCache.JSONPayload__c)){
				results = (Map<String, Object>) JSON.deserializeUntyped(apptPayloadFromSfCache.JSONPayload__c);
				// pass Http Status code to LC to handle and display exception.
				results.put('httpStatusCode','200');
			}
			// Callout made to get the appointment details from CIS getAppointment api
			else{
				HttpResponse getApptAPIResponse = new HttpResponse();
				getApptAPIResponse = integrationUtility.getAppointmentDetailsFromCIS(integrationType, accessSeekerId, appointmentId);
				if(getApptAPIResponse <> null){
					if(getApptAPIResponse.getBody() <> '{}' && String.isNotBlank(getApptAPIResponse.getBody())){
						if(getApptAPIResponse.getStatusCode() == 200){
							// Save the success paylaod in SF Integration Cache object.
							saveApptPayloadInSfCache(appointmentId, getApptAPIResponse.getBody());
						}
						try{
							// Success and failure Payload is deserialized and sent to UI to display
							results = (Map<String, Object>)JSON.deserializeUntyped(getApptAPIResponse.getBody());
							// pass Http Status code to LC to handle and display exception.
							results.put('httpStatusCode',(object)getApptAPIResponse.getStatusCode());
						}
						catch(Exception ex){
							// Display Failed Callout messages in UI
							results = new Map <String, Object>();
							results.put('code',(object)getApptAPIResponse.getStatusCode());
							results.put('description',(object)getApptAPIResponse.getBody());
						}
					}
				}
			}
		}
        return results;
    }
	// @@fetchApptPayloadFromSfCache -- Helper Method. Fetch Appointment Payload from SF Integration Cache
	private static NBNIntegrationCache__c fetchApptPayloadFromSfCache(String integrationType, String apptId){
		List<NBNIntegrationCache__c> lstApptPayloadFromSfCache = [SELECT Id, Transaction_Id__c, JSONPayload__c FROM NBNIntegrationCache__c WHERE IntegrationType__c = :integrationType and Transaction_Id__c = :apptId order By createddate desc];
		if(lstApptPayloadFromSfCache <> null && !lstApptPayloadFromSfCache.isEmpty()){
			return lstApptPayloadFromSfCache.get(0);
		}
		return null;
	}
	// @@saveApptPayloadInSfCache -- Helper Method. Save Appointment Payload in SF Integration Cache
	private static void saveApptPayloadInSfCache(String appointmentId, String responseBody){
		NBNIntegrationCache__c nbnIntegrationCache = new NBNIntegrationCache__c();
		nbnIntegrationCache.IntegrationType__c = 'AppointmentDetails';
		nbnIntegrationCache.Transaction_Id__c = appointmentId;
		nbnIntegrationCache.JSONPayload__c = responseBody;
		INSERT nbnIntegrationCache;
	}
    
    public class ServiceChangeDataWrapper {
        @AuraEnabled
        public String Orderid {
            get;
            set;
        }
        @AuraEnabled
        public String orderType {
            get;
            set;
        }
        @AuraEnabled
        public String orderStatus {
            get;
            set;
        }
        @AuraEnabled
        public String dateAccepted {
            get;
            set;
        }
        @AuraEnabled
        public String dateCompleted {
            get;
            set;
        }
    }
    
    public class WorkOrderDataWrapper {
        
        @AuraEnabled
        public String SFDCId {
            get;
            set;
        }
        
        @AuraEnabled
        public String incident {
            get;
            set;
        }
        @AuraEnabled
        public String workRequest {
            get;
            set;
        }
        @AuraEnabled
        public String faultType{
            get;
            set;
        }
        @AuraEnabled
        public String parentWorkOrder {
            get;
            set;
        }
        @AuraEnabled
        public String workOrder {
            get;
            set;
        }
        @AuraEnabled
        public String raised {
            get;
            set;
        }
        @AuraEnabled
        public String completed {
            get;
            set;
        }
        @AuraEnabled
        public String reasonCode{
            get;
            set;
        }
    }
}