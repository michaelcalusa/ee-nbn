/*------------------------------------------------------------
Author: Sunil Gupta June 23, 2018
Company: Appirio
Description: Server-side controller for 3 Lightning components that used to display More details about Incidents, Service Orders and WO.
History
<Date>			<Author>				<Description>
*24-01-2019		Syed Moosa Nazir		Test class for CIS getAppointment and MS getAppointment service. CUSTSA-24874, CUSTSA-24875
------------------------------------------------------------*/
@isTest()
public class JIGSAW_SummaryTableControllerTest {
    Static testMethod void getAppointmentDetailsFromCISTestSuccess() {
        List<sObject> lstNbnintegrationinputs = Test.loadData(NBNIntegrationInputs__c.sObjectType, 'NBNIntegrationInputsTestData');
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('getAppointmentDetailsResponse');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        Test.StartTest();
        	JIGSAW_SummaryTableController.getAppointmentDetailsFromCIS('getAppointmentDetailsCIS','ASC123','APT123');
        Test.StopTest();
    }
    Static testMethod void getAppointmentDetailsTestSuccess() {
        User u = new User(firstname= 'test',
                          lastname='IncidentMmtCtrl',
                          Alias='tinci',
                          email = 'tinci@email.com',
                          username= 'test.IncidentMmtCtrl@nbnco.com.au', 
                          profileId= [SELECT id, Name FROM Profile WHERE Name = 'System Administrator'][0].Id, 
                          emailencodingkey='UTF-8',
                          languagelocalekey='en_US',
                          localesidkey='en_US',
                          timezonesidkey='Australia/Sydney',
                          FederationIdentifier='n102466'
                         );
        NBNIntegrationInputs__c nbnIntegrationInputs = new NBNIntegrationInputs__c();
        nbnIntegrationInputs.Name = 'getAppointmentDetails';
        nbnIntegrationInputs.EndpointURL__c = '/v1/get-appointment';
        nbnIntegrationInputs.Token__c = 'dba5adfae0050403';
        nbnIntegrationInputs.nbn_Role__c = 'NBN';
        nbnIntegrationInputs.Service_Name__c = 'Salesforce';
        nbnIntegrationInputs.NBN_Environment_Override__c = 'SIT3';
        nbnIntegrationInputs.Call_TimeOut_In_MilliSeconds__c = '6000';
        nbnIntegrationInputs.Is_Api_Gateway__c = false;
		nbnIntegrationInputs.Header_Keys__c = 'nbn-role,nbn-requestor-platform,NBN-Environment-Override,Content-Type,x-api-key';
        System.runAs(u){insert nbnIntegrationInputs;}
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('getAppointmentDetailsResponse');
        mock.setStatusCode(202);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        Map<String, Object> results;
        Test.StartTest();
        results = JIGSAW_SummaryTableController.getAppointmentDetails('getAppointmentDetails','aeb8a1007f60271d');
        Test.StopTest();
        System.Debug(results);
    }
    Static testMethod void getAppointmentDetailsFromCacheTestSuccess() {
        User u = new User(firstname= 'test',
                          lastname='IncidentMmtCtrl',
                          Alias='tinci',
                          email = 'tinci@email.com',
                          username= 'test.IncidentMmtCtrl@nbnco.com.au', 
                          profileId= [SELECT id, Name FROM Profile WHERE Name = 'System Administrator'][0].Id, 
                          emailencodingkey='UTF-8',
                          languagelocalekey='en_US',
                          localesidkey='en_US',
                          timezonesidkey='Australia/Sydney',
                          FederationIdentifier='n102466'
                         );
        StaticResource responseJson = [Select Id,Body From StaticResource Where Name = 'getAppointmentDetailsResponse'];
        NBNIntegrationCache__c nbnIntegrationCacheRecord = new NBNIntegrationCache__c();
        nbnIntegrationCacheRecord.IntegrationType__c = 'AppointmentDetails';
        nbnIntegrationCacheRecord.Transaction_Id__c = 'APT000000000001';
        nbnIntegrationCacheRecord.JSONPayload__c = responseJson.Body.Tostring();
        System.runAs(u){insert nbnIntegrationCacheRecord;}
        Map<String, Object> results;
        Test.StartTest();
        results = JIGSAW_SummaryTableController.getAppointmentDetails('getAppointmentDetails','APT000000000001');
        Test.StopTest();
    }
    Static testMethod void getAppointmentDetailsFromCacheTestException() {
         User u = new User(firstname= 'test',
                          lastname='IncidentMmtCtrl',
                          Alias='tinci',
                          email = 'tinci@email.com',
                          username= 'test.IncidentMmtCtrl@nbnco.com.au', 
                          profileId= [SELECT id, Name FROM Profile WHERE Name = 'System Administrator'][0].Id, 
                          emailencodingkey='UTF-8',
                          languagelocalekey='en_US',
                          localesidkey='en_US',
                          timezonesidkey='Australia/Sydney',
                          FederationIdentifier='n102466'
                         );
        NBNIntegrationInputs__c nbnIntegrationInputs = new NBNIntegrationInputs__c();
        nbnIntegrationInputs.Name = 'getAppointmentDetails';
        nbnIntegrationInputs.EndpointURL__c = '/v1/get-appointment/{0}';
        nbnIntegrationInputs.Token__c = 'dba5adfae0050403';
        nbnIntegrationInputs.Call_TimeOut_In_MilliSeconds__c = '6000';
        nbnIntegrationInputs.nbn_Role__c = 'NBN';
        nbnIntegrationInputs.Header_Keys__c = 'nbn-role,nbn-requestor-platform,NBN-Environment-Override,Content-Type,x-api-key';
        nbnIntegrationInputs.Service_Name__c = 'Salesforce';
        nbnIntegrationInputs.NBN_Environment_Override__c = 'SIT3';
        nbnIntegrationInputs.Is_Api_Gateway__c = false;
        System.runAs(u){insert nbnIntegrationInputs;}
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('getAppointmentDetailsResponse');
        mock.setStatusCode(500);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        Map<String, Object> results;
        Test.StartTest();
            results = JIGSAW_SummaryTableController.getAppointmentDetails('getAppointmentDetails','aeb8a1007f60271d');
        Test.StopTest();
    }
    Static testMethod void getWorkHistorySuccess() {
        User u = new User(firstname= 'test',
                          lastname='IncidentMmtCtrl1',
                          Alias='tinci1',
                          email = 'tinci1@email.com',
                          username= 'test.IncidentMmtCtrl1@nbnco.com.au', 
                          profileId= [SELECT id, Name FROM Profile WHERE Name = 'System Administrator'][0].Id, 
                          emailencodingkey='UTF-8',
                          languagelocalekey='en_US',
                          localesidkey='en_US',
                          timezonesidkey='Australia/Sydney',
                          FederationIdentifier='n1024661'
                         );
        NBNIntegrationInputs__c nbnIntegrationInputs = new NBNIntegrationInputs__c();
        nbnIntegrationInputs.Name = 'WorkOrderHistory';
        nbnIntegrationInputs.EndpointURL__c = '/maxrest/wwm/json/os/workhistory?nbnlocationid={0}&reportdate={1}&daysbackward={2}';
        nbnIntegrationInputs.Service_Name__c = 'Salesforce';
        nbnIntegrationInputs.NBN_Environment_Override__c = 'SIT3';
        nbnIntegrationInputs.NBN_Activity_Name__c = 'getWorkHistory';
		nbnIntegrationInputs.Header_Keys__c = 'nbn-role,nbn-requestor-platform,NBN-Environment-Override,Content-Type,x-api-key';
        System.runAs(u){insert nbnIntegrationInputs;}
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('getWorkHistoryResponse');
        mock.setStatusCode(200);
        Test.setMock(HttpCalloutMock.class, mock);
        //Map<String, Object> results;
        Test.StartTest();
        JIGSAW_SummaryTableController.SummaryWrapper wrapper= new JIGSAW_SummaryTableController.SummaryWrapper();
        //String results = wrapper.fetchRecords('WORKORDER','1212121');
        Test.StopTest();
        //System.Debug(results);
    }
    static testMethod void testFetchServiceChanges(){
        //create Incident
        Incident_Management__c inc = new Incident_Management__c(Incident_Number__c='INC000006922569',PRI_ID__c='1234');
        insert inc;
        
        // Set cache record
        String strResponse = '{"data":[{"id":"PRI003001935267","type":"ncasProductInstance","attributes":{"locationId":"LOC000070161048","primaryAccessTechnology":"FibreToTheNode","priorityAssist":"No","productStatus":"Active","orderCompletionDate":"28/05/201813:55:36"},"relationships":{"accessSeeker":{"data":{"id":"ASI000000220997","type":"accessSeeker"},"links":{"related":"http://10.11.43.23:8484/accessseeker/v1/info/ASI000000220997"}},"services":{"data":[{"id":"AVC000022374809","type":"avc-d"}],"links":{"related":"https://cis-api-sit3.slb.nbndc.local/rest/services/v1/avcs/"}}}}],"included":[{"type":"ProductOrder","id":"ORD010415110427","attributes":{"orderType":"Connect","orderStatus":"Complete","dateAccepted":"28/05/201811:55:29","dateComplete":"28/05/201813:55:36","orderAttrs":{"action":"ADD","priorityAssist":"No","serviceRestorationSla":"Standard","installationWorkforce":"NBNCoInstall","installationOptionsJumpering":"Yes","installationOptionsCentralSplitter":"No","orderSla":"Standard","avcD":[{"action":"ADD","bandwidthProfile":"D25-50_U5-20_Mbps_TC4_P","cvcId":"CVC000022033549","nniCvlanId":"0","accessLoopIdentification":"Active","insertDSLLineRate":"True"}],"uniDsl":[{"id":"CPI300006343922","action":"MODIFY","potsInterconnect":"0245456780","dslMode":"VDSL2","dslStabilityProfile":"Standard","exchangePairConnection":"Yes"}]}}}]}';
            
        NBNIntegrationCache__c nbnIntegrationCacheRecord = new NBNIntegrationCache__c();
        nbnIntegrationCacheRecord.IntegrationType__c = 'OrderHistory';
        nbnIntegrationCacheRecord.PRIId__c = '1234';
        nbnIntegrationCacheRecord.JSONPayload__c = strResponse;
        insert nbnIntegrationCacheRecord;
        
        Test.StartTest();
        JIGSAW_SummaryTableController.SummaryWrapper wrap1 = JIGSAW_SummaryTableController.fetchRecords('SERVICE',inc.id,null,null);
        system.assertEquals(wrap1.errorOccured, null);
        //delete cache record to test negative case
        delete [select id from NBNIntegrationCache__c];
        JIGSAW_SummaryTableController.SummaryWrapper wrap2 = JIGSAW_SummaryTableController.fetchRecords('SERVICE',inc.id,null,null);
        system.assertEquals(wrap2.errorOccured, true);
        //test error case when incident id is not fount
        JIGSAW_SummaryTableController.SummaryWrapper wrap3 = JIGSAW_SummaryTableController.fetchRecords('SERVICE',null,null,null);
        system.assertEquals(wrap3.errorOccured, true);
        Test.StopTest();
    }
    static testMethod void testFetchIncident(){
        //create Incident
        list<Incident_Management__c> incList = new list<Incident_Management__c>();
        for(integer i =0 ; i<100 ; i++){
            incList.add(new Incident_Management__C(Incident_Number__c='INC00000692256'+String.valueOf(i),PRI_ID__c='1234'+String.valueOf(i),locId__c='111000',Reported_Date__c=system.now(),Industry_Status__c='Resolved'));
        }
        insert incList;
        
        
        Test.StartTest();
        JIGSAW_SummaryTableController.SummaryWrapper wrap1 = JIGSAW_SummaryTableController.fetchRecords('INCIDENT',incList[0].id,null,'111000');
        //check negative case 
        JIGSAW_SummaryTableController.SummaryWrapper wrap2 = JIGSAW_SummaryTableController.fetchRecords('INCIDENT',incList[0].id,null,null);
        Test.StopTest();
    }
    
    static testMethod void testWorkOrder(){
        List<SObject> lstToInsert = new List<SObject>();
        
        Incident_Management__c objIM_1 = new Incident_Management__c();
        objIM_1.Appointment_Start_Date__c = System.now();
        objIM_1.Appointment_End_Date__c = System.now();
        objIM_1.Target_Commitment_Date__c = System.now();
        objIM_1.Appointment_Start_Date__c = System.now();
        objIM_1.Appointment_End_Date__c = System.now();
        objIM_1.Target_Commitment_Date__c = System.now();
        objIM_1.locId__c = 'LOC00001';
        objIM_1.Reported_Date__c = System.now().addDays(-10);
        objIM_1.Network_Trail_Transaction_Id__c = 'test';
        lstToInsert.add(objIM_1);
        
        
        NBNIntegrationInputs__c objCS1 = new NBNIntegrationInputs__c();
        objCS1.NBN_Activity_Name__c = 'test';
        objCS1.Name = 'WorkOrderHistory';
        objCS1.Call_Out_URL__c = 'callout:NBNAPPGATEWAY';
        objCS1.EndpointURL__c='testWorkOrder';
        objCS1.AccessSeekerId__c = 'testId';
        objCS1.Call_TimeOut_In_MilliSeconds__c = '60000';
        objCS1.NBN_Environment_Override__c='SIT1';
        objCS1.nbn_Role__c='NBN';
        objCS1.Service_Name__c='test serviec';
        objCS1.Token__c='8e73b7e33b22ad58';
        objCS1.XNBNBusinessChannel__c='Salesforce';
        objCS1.UI_TimeOut__c = 6000;
        lstToInsert.add(objCS1);
        
        insert lstToInsert;
        
        Test.StartTest();
        

        // Set Mock
        MultiStaticResourceCalloutMock mock = new MultiStaticResourceCalloutMock();
        mock.setStaticResource('testWorkOrderHistory', 'JIGSAW_WorkHistoryJSON');
        mock.setStaticResource('testWorkOrder', 'JIGSAW_WorkOrderJSON');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        
        JIGSAW_SummaryTableController.SummaryWrapper wrap1 = JIGSAW_SummaryTableController.fetchRecords('WORKORDER', objIM_1.Id, '60', 'LOC00001');
        
        Test.StopTest();
    }
    
    
    
}