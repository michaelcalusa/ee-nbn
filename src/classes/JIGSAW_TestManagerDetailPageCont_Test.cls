@isTest()
public class JIGSAW_TestManagerDetailPageCont_Test {
    @testSetup static void setUpData() {
        //create custom setting records
        Insert new JIGSAW_TestManager_Config__c(name = 'LSD1', Test_Type__c = 'LSD Test', Parent_JSON_Key__c = 'diagnosticDetails', JSON_Key__c = 'type', SF_Label_Name__c = 'Type', Is_Visible_in_Jigsaw__c = true, Table_Row_Number__c = 1, Table_Number__c = 1, Is_Array__c = false, Concatenate_With__c = '', Table_Heading__c = '');
        Insert new JIGSAW_TestManager_Config__c(name = 'Loopback1', Test_Type__c = 'Loopback', Parent_JSON_Key__c = 'diagnosticDetails', JSON_Key__c = 'type', SF_Label_Name__c = 'Type', Is_Visible_in_Jigsaw__c = true, Table_Row_Number__c = 1, Table_Number__c = 1, Is_Array__c = false, Concatenate_With__c = '', Table_Heading__c = '');
        Insert new JIGSAW_TestManager_Config__c(name = 'SELT1', Test_Type__c = 'Single End Line Test', Parent_JSON_Key__c = 'diagnosticDetails', JSON_Key__c = 'type', SF_Label_Name__c = 'Type', Is_Visible_in_Jigsaw__c = true, Table_Row_Number__c = 1, Table_Number__c = 1, Is_Array__c = false, Concatenate_With__c = '', Table_Heading__c = '');
        Insert new JIGSAW_TestManager_Config__c(name = 'LQD1', Test_Type__c = 'LQD Test', Parent_JSON_Key__c = 'diagnosticDetails', JSON_Key__c = 'type', SF_Label_Name__c = 'Type', Is_Visible_in_Jigsaw__c = true, Table_Row_Number__c = 1, Table_Number__c = 1, Is_Array__c = false, Concatenate_With__c = '', Table_Heading__c = '');
        Insert new JIGSAW_TestManager_Config__c(name = 'MLT1', Test_Type__c = 'Metallic Line Test', Parent_JSON_Key__c = 'diagnosticDetails', JSON_Key__c = 'type', SF_Label_Name__c = 'Type', Is_Visible_in_Jigsaw__c = true, Table_Row_Number__c = 1, Table_Number__c = 1, Is_Array__c = false, Concatenate_With__c = '', Table_Heading__c = '');

        insert new NBNIntegrationInputs__c(Header_Keys__c = 'nbn-correlation-id', name = 'JIGSAW_TestManagerDetail', AccessSeekerId__c = 'testId', Call_TimeOut_In_MilliSeconds__c = '60000', EndpointURL__c = '/rest/services/v1/avcs/wri/', NBNBreadcrumbId__c = 'test', NBN_Environment_Override__c = 'SIT1', nbn_Role__c = 'NBN', Service_Name__c = 'test serviec', Token__c = '8e73b7e33b22ad58', XNBNBusinessChannel__c = 'Salesforce', UI_TimeOut__c = 50);
        insert new NBNIntegrationInputs__c(Header_Keys__c = 'nbn-correlation-id', name = 'JIGSAW_TestManagerSummary', AccessSeekerId__c = 'testId', Call_TimeOut_In_MilliSeconds__c = '60000', EndpointURL__c = '/rest/services/v1/avcs/GTAS-summary/', NBNBreadcrumbId__c = 'test', NBN_Environment_Override__c = 'SIT1', nbn_Role__c = 'NBN', Service_Name__c = 'test serviec', Token__c = '8e73b7e33b22ad58', XNBNBusinessChannel__c = 'Salesforce', UI_TimeOut__c = 50);
        insert new NBNIntegrationInputs__c(Header_Keys__c = 'nbn-correlation-id', name = 'JIGSAW_TestManagerGraphs', AccessSeekerId__c = 'testId', Call_TimeOut_In_MilliSeconds__c = '60000', EndpointURL__c = '', NBNBreadcrumbId__c = 'test', NBN_Environment_Override__c = 'MOCK', nbn_Role__c = 'NBN', Service_Name__c = 'test serviec', Token__c = '8e73b7e33b22ad58', XNBNBusinessChannel__c = 'Salesforce', UI_TimeOut__c = 50);

        //create incident record
        insert new Incident_Management__c(Name = 'INC000111211', Prod_Cat__c = 'NCAS-FTTN', Access_Seeker_ID__c = 'ACCESSSEEKERID', repeatIncidentPriority__c = 'RepeatIncidentPriority', PRI_ID__c = 'PRIID', AVC_Id__c = 'AVC000028840963', Fault_Type__c = 'Performance latency', recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Incident Management FTTN/B').getRecordTypeId(), Appointment_Start_Date__c = system.now(), Appointment_End_Date__c = system.now(), Target_Commitment_Date__c = system.now(), Reported_Date__c = system.now());
        insert new Incident_Management__c(Name = 'INC000111212', Prod_Cat__c = 'NCAS-FTTN', Access_Seeker_ID__c = 'ACCESSSEEKERID', repeatIncidentPriority__c = 'RepeatIncidentPriority', PRI_ID__c = 'PRIID', AVC_Id__c = 'AVC000028840963', Fault_Type__c = 'Performance latency', recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Incident Management FTTN/B').getRecordTypeId(), Appointment_Start_Date__c = system.now(), Appointment_End_Date__c = system.now(), Target_Commitment_Date__c = system.now(), Reported_Date__c = system.now(), Industry_Status__c = 'Closed');
        
        //create test details cache records
        insert new JIGSAW_Test_Details__c(worfklowReferenceId__c='WRI000000001');
        insert new JIGSAW_Test_Details__c(worfklowReferenceId__c='WRI000000002');
        insert new JIGSAW_Test_Details__c(worfklowReferenceId__c='WRI000000005');
        
        //create test graph cahce
        Attachment graphCache = new Attachment();
        graphCache.body = Blob.valueof('Test Image Blob');
        graphCache.ParentId = [select id from JIGSAW_Test_Details__c where worfklowReferenceId__c = 'WRI000000002' Limit 1].Id;
        graphCache.contentType = 'image/png';
        graphCache.name = 'WRI000000001###graph.2.title.rtx.bitrate.ds.png';

        insert graphCache;

    }


    static testMethod void getAtomicCSListTest() {
        test.startTest();
        
        list < JIGSAW_TestManager_Config__c > lstCustSet1 = JIGSAW_TestManagerDetailPageController.getAtomicCSList('LSD Test');
        system.assertEquals(1, lstCustSet1.size());
        list < JIGSAW_TestManager_Config__c > lstCustSet2 = JIGSAW_TestManagerDetailPageController.getAtomicCSList('');
        system.assertEquals(null, lstCustSet2);
        
        test.stopTest();
    }

    static testMethod void getTestHistorySummaryTest() {
        test.startTest();
        
        Incident_Management__c inc = [select id from Incident_Management__c LIMIT 1][0];
        //Success callout
        TestManagerMockHttpRespGen mockResp1 = new TestManagerMockHttpRespGen();
        mockResp1.strResponseType = 'Initial Callout by AVC Id';
        mockResp1.status = '200';
        Test.setMock(HttpCalloutMock.class, mockResp1);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap1 = JIGSAW_TestManagerDetailPageController.getTestHistorySummary(inc.id, '1000');

        System.assertEquals(false, wrap1.errorOccured);
        
        //failure callout
        TestManagerMockHttpRespGen mockResp2 = new TestManagerMockHttpRespGen();
        mockResp2.strResponseType = 'Initial Callout by AVC Id';
        mockResp2.status = '400';
        Test.setMock(HttpCalloutMock.class, mockResp2);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap2 = JIGSAW_TestManagerDetailPageController.getTestHistorySummary(inc.id, '1000');

        System.assertEquals(true, wrap2.errorOccured);
        
        //If no Id is passed
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap3 = JIGSAW_TestManagerDetailPageController.getTestHistorySummary('XXXX', '1000');
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap4 = JIGSAW_TestManagerDetailPageController.getTestHistorySummary('', '1000');
        System.assertEquals(true, wrap3.errorOccured);
        System.assertEquals(true, wrap4.errorOccured);
        
        //Exception callout
        Delete [select Id from NBNIntegrationInputs__c];
        
        TestManagerMockHttpRespGen mockResp5 = new TestManagerMockHttpRespGen();
        mockResp5.strResponseType = 'Initial Callout by AVC Id';
        mockResp5.status = '200';
        Test.setMock(HttpCalloutMock.class, mockResp5);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap5 = JIGSAW_TestManagerDetailPageController.getTestHistorySummary(inc.id, '1000');

        System.assertEquals(true, wrap5.errorOccured);
        
        
        test.stopTest();
    }
    
    static testMethod void getTestHistorySummaryTest1() {
        test.startTest();
        
        Incident_Management__c inc = [select id from Incident_Management__c where name= 'INC000111212'  LIMIT 1][0];
        //Success callout
        TestManagerMockHttpRespGen mockResp1 = new TestManagerMockHttpRespGen();
        mockResp1.strResponseType = 'Initial Callout by AVC Id';
        mockResp1.status = '200';
        Test.setMock(HttpCalloutMock.class, mockResp1);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap1 = JIGSAW_TestManagerDetailPageController.getTestHistorySummary(inc.id, '1000');

        System.assertEquals(false, wrap1.errorOccured);
        
        //failure callout
        TestManagerMockHttpRespGen mockResp2 = new TestManagerMockHttpRespGen();
        mockResp2.strResponseType = 'Initial Callout by AVC Id';
        mockResp2.status = '400';
        Test.setMock(HttpCalloutMock.class, mockResp2);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap2 = JIGSAW_TestManagerDetailPageController.getTestHistorySummary(inc.id, '1000');

        System.assertEquals(true, wrap2.errorOccured);
        
        //If no Id is passed
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap3 = JIGSAW_TestManagerDetailPageController.getTestHistorySummary('XXXX', '1000');
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap4 = JIGSAW_TestManagerDetailPageController.getTestHistorySummary('', '1000');
        System.assertEquals(true, wrap3.errorOccured);
        System.assertEquals(true, wrap4.errorOccured);
        
        //Exception callout
        Delete [select Id from NBNIntegrationInputs__c];
        
        TestManagerMockHttpRespGen mockResp5 = new TestManagerMockHttpRespGen();
        mockResp5.strResponseType = 'Initial Callout by AVC Id';
        mockResp5.status = '200';
        Test.setMock(HttpCalloutMock.class, mockResp5);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap5 = JIGSAW_TestManagerDetailPageController.getTestHistorySummary(inc.id, '1000');

        System.assertEquals(true, wrap5.errorOccured);
        
        
        test.stopTest();
    }
    
    static testMethod void getTestDetailsAsWRITest() {
        test.startTest();
        //successfull callout
        TestManagerMockHttpRespGen mockResp1 = new TestManagerMockHttpRespGen();
        mockResp1.strResponseType = 'Second Callout for Test Details';
        mockResp1.status = '200';
        Test.setMock(HttpCalloutMock.class, mockResp1);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap1 = JIGSAW_TestManagerDetailPageController.getTestDetailsAsWRI('WRI000000000000','Atomic','NBN','Remedy','ACCESSSEEKERID','LSD Test','LSD Test');
        System.assertEquals(false, wrap1.errorOccured);
        
        //error callout
        TestManagerMockHttpRespGen mockResp2 = new TestManagerMockHttpRespGen();
        mockResp2.strResponseType = 'Second Callout for Test Details';
        mockResp2.status = '400';
        Test.setMock(HttpCalloutMock.class, mockResp2);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap2 = JIGSAW_TestManagerDetailPageController.getTestDetailsAsWRI('WRI000000000000','Atomic','NBN','Remedy','ACCESSSEEKERID','LSD Test','LSD Test');
        System.assertEquals(true, wrap2.errorOccured);
        
        //create cache record 
        JIGSAW_Test_Details__c cacheRec = new JIGSAW_Test_Details__c();
        cacheRec.worfklowReferenceId__c = 'WRI000000000000';
        cacheRec.JSON_response__c = '{"testReferenceId":"WRI000000000000","status":"Completed","role":"NBN","channel":"Remedy","workflowOutcome":{"status":"Passed","outcome":"ESCALATION","description":"Workflow Steps performed and Details on the Outcome","deducefaultType":"No Data"},"diagnosticDetails":[{"type":"LSD Test","status":"Completed","result":"Passed","lineId":"SWDSL0200175:1-1-1-4","userLabel":"UNI000022118321","lineOperationalStatus":"Up","lastStatusChange":"2017-10-17T05:40:57Z","serviceStability":"STABLE","cardType":"NDLT-F","cpeType":"Netcomm","dslType":"VDSL2","cpeMacAddress":"90:72:82:DB:24:1E","modemVendorIdHex":"B5004244434D0000","modemVendorIdAscii":"BDCM","systemVendorIdHex":"0F00544D4D420000","systemVendorIdAscii":"BDCM","systemVendorModelHex":"41327076364630333976320000000000","systemVendorModelAscii":"A2pv6F039v4","serialNumber":"CP1632TAEHG 799vac 17.2","actualCpeVectoringType":"g.vector capable","vectoringStatus":"Enabled","estimatedDeltDistance":"465","estimatedDeltDistanceUnitType":"m","electricalLength":"9.5","electricalLengthUnitType":"dB","rtxActualNetDataRateUpstream":"6400.0","rtxActualNetDataRateDownstream":"28000.0","rtxActualNetDataRateUnitType":"kbps","rtxActualExpectedThroughputUpstream":"6335.0","rtxActualExpectedThroughputDownstream":"27717.0","rtxActualExpectedThroughputUnitType":"kbps","rtxAttainableNetDataRateUpstream":"43725.0","rtxAttainableNetDataRateDownstream":"100164.0","rtxAttainableNetDataRateUnitType":"kbps","rtxAttainableExpectedThroughputUpstream":"43212.0","rtxAttainableExpectedThroughputDownstream":"98990.0","rtxAttainableExpectedThroughputUnitType":"kbps","tc2ExistingWbaThresholdUpstream":"38,200.0","tc2ExistingWbaThresholdDownstream":"38,200.0","tc2ExistingWbaThresholdUnitType":"kbps","tc4ExistingWbaThresholdUpstream":"26,100.0","tc4ExistingWbaThresholdDownstream":"5,100.0","tc4ExistingWbaThresholdUnitType":"kbps","higherCalculatedRateUpstream":"38,200.0","higherCalculatedRateDownstream":"38,200.0","higherCalculatedRateUnitType":"kbps","fecActualBitrateUpstream":"6400.0","fecActualBitrateDownstream":"28000.0","fecActualBitrateUnitType":"kbps","fecAttainableBitrateUpstream":"6400.0","fecAttainableBitrateDownstream":"28000.0","fecAttainableBitrateUnitType":"kbps","tc2OrderedSpeedUpstream":"30,000.0","tc2OrderedSpeedDownstream":"30,000.0","tc2OrderedSpeedUnitType":"kbps","tc2CapableDownstreamResult":"PASS","tc2CapableUpstreamResult":"PASS","tc4OrderedSpeedUpstream":"40,000.0","tc4OrderedSpeedDownstream":"40,000.0","tc4OrderedSpeedUnitType":"kbps","tc4CapableDownstreamResult":"PASS","tc4CapableUpstreamResult":"PASS","noiseMarginAverageUpstream":"22.8","noiseMarginAverageDownstream":"20.7","noiseMarginAverageUnitType":"dB","attenuationAverage":{"unitType":"dB","bands":[{"bandId":"0","downstream":"127.0","upstream":"4.5"},{"bandId":"1","downstream":"15.2","upstream":"18.5"},{"bandId":"2","downstream":"25.2","upstream":"18.5"},{"bandId":"3","downstream":"36.2","upstream":"18.5"}]},"loopAttenuationAverage":{"unitType":"dB","bands":[{"bandId":"0","downstream":"127.0","upstream":"4.5"},{"bandId":"1","downstream":"15.2","upstream":"18.5"},{"bandId":"2","downstream":"25.2","upstream":"18.5"},{"bandId":"3","downstream":"36.2","upstream":"18.5"}]},"relativeCapacityOccupationUpstream":"43.0","relativeCapacityOccupationDownstream":"46.0","relativeCapacityOccupationUnitType":"%","actualPsdUpstream":"-71.0","actualPsdDownstream":"-66.0","actualPsdUnitType":"dBm/Hz","actualRtxModeUpstream":"In Use","actualRtxModeDownstream":"In Use","userTrafficUpstream":"0","userTrafficDownstream":"0","userTrafficUnitType":"octets","outputPowerUpstream":"10.7","outputPowerDownstream":"6.3","outputPowerUnitType":"dBm","supportedCpeVectoringType":["legacy","g.vector capable"],"layer2BitrateUpstream":"15009","layer2BitrateDownstream":"4341","layer2BitrateUnitType":"kbps","physicalProfile":"12/1 Standard 6dB","coexistence":"No","fallbackState":"Not Active","fallbackProfile":"100/40 Standard 6dB","serviceSpeedIndicator":"Green","serviceSpeedDescription":"Service speed is performing within specification.","serviceSpeedIncidentEligibility":"No","serviceProblems":[{"description":"CONNECTION_STATUS-CPE_SILENT-parameter.far.end.line.failure-NOATUR","problemArea":"xDSL","impact":"no.connectivity.impact","confidenceLevel":"100","confidenceLevelUnitType":"%"}],"resolutions":[{"code":"SF000","message":"LSD Test Success"}]},{"type":"Reset Port","status":"Completed","result":"Passed","lineId":"SWDSL0200175:1-1-1-4","resolutions":[{"code":"SF00","message":"Reset Port Success"}]},{"type":"LQD Test","status":"Completed","result":"Passed","estimatedResponseTime":"60","monitoringPeriod":"300","cpeType":"OPTUS TR20","dslType":"VDSL2","vectoringStatus":"Enabled","actualCpeVectoringType":"g.vector capable","modemVendorIdHex":"B5004244434D0000","systemVendorIdHex":"0F00544D4D420000","systemVendorModelHex":"41327076364630333976320000000000","serialNumber":"CP1632TAEHG 799vac 17.2","serviceStability":"STABLE","estimatedDeltDistance":"465","electricalLength":"9.5","actualBitrateUpstream":"6400.0","actualBitrateDownstream":"28000.0","actualBitrateUnitType":"kbps","attainableBitrateUpstream":"6400.0","attainableBitrateDownstream":"28000.0","attainableBitrateUnitType":"kbps","layer2BitrateUpstream":"1000.9","layer2BitrateDownstream":"1000.9","layer2BitrateUnitType":"kbps","noiseMarginAverageUpstream":"22.8","noiseMarginAverageDownstream":"20.7","noiseMarginAverageUnitType":"dB","relativeCapacityOccupationUpstream":"43.0","relativeCapacityOccupationDownstream":"46.0","relativeCapacityOccupationUnitType":"%","actualPsdUpstream":"-71.0","actualPsdDownstream":"-66.0","actualPsdUnitType":"dBm/Hz","outputPowerUpstream":"-5.1","outputPowerDownstream":"3.3","outputPowerUnitType":"dBm","userTrafficUpstream":"621579167","userTrafficDownstream":"2147483647","userTrafficUnitType":"octets","physicalProfile":"25/5 Standard 6dB","fallbackState":"Not Active","imageUrls":["link1","link2"],"classificationHistoryImageUrls":["link1","link2"],"resolutions":[{"code":"SF00","message":"LQD Success"}]},{"type":"Reset Port","status":"Completed","result":"Failed","lineId":"SWDSL0200175:1-1-1-4","resolutions":[{"code":"SF01","message":"Reset Port Failed"}],"exceptions":[{"code":"000","message":"Reset Port Failed"}]}]}';
        cacheRec.result__c = 'Passed';
        cacheRec.workflowType__c = 'Atomic';
        insert cacheRec;
        
        //get details from cache callout
        TestManagerMockHttpRespGen mockResp3 = new TestManagerMockHttpRespGen();
        mockResp3.strResponseType = 'Second Callout for Test Details';
        mockResp3.status = '200';
        Test.setMock(HttpCalloutMock.class, mockResp3);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap3 = JIGSAW_TestManagerDetailPageController.getTestDetailsAsWRI('WRI000000000000','Atomic','NBN','Remedy','ACCESSSEEKERID','LSD Test','LSD Test');
        System.assertEquals(false, wrap3.errorOccured);
        System.assertEquals(true, wrap3.isFromCache);
        
        //When no workFlowReference id is provided
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap4 = JIGSAW_TestManagerDetailPageController.getTestDetailsAsWRI('','Atomic','NBN','Remedy','ACCESSSEEKERID','LSD Test','LSD Test');
        System.assertEquals(true, wrap4.errorOccured);
        
        //exception in callout
        Delete [select Id from NBNIntegrationInputs__c];
        Delete [select Id from JIGSAW_Test_Details__c];
        
        TestManagerMockHttpRespGen mockResp5 = new TestManagerMockHttpRespGen();
        mockResp5.strResponseType = 'Second Callout for Test Details';
        mockResp5.status = '200';
        Test.setMock(HttpCalloutMock.class, mockResp5);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap5 = JIGSAW_TestManagerDetailPageController.getTestDetailsAsWRI('WRI000000000000','Atomic','NBN','Remedy','ACCESSSEEKERID','LSD Test','LSD Test');
        System.assertEquals(true, wrap5.errorOccured);
        
        test.stopTest();
    }
    
    static testMethod void saveTestDetailCacheTest() {
        test.startTest();
        String wriid = 'WRI000000000000';
        String testType = 'LSD Test';
        String testResult = 'Passed';
        String testStatus = 'Completed';
        String avcID = 'AVC000028840963';
        String testName = 'LSD';
        String JSONPayload = '{"testReferenceId":"WRI000000000000","status":"Completed","role":"NBN","channel":"Remedy","workflowOutcome":{"status":"Passed","outcome":"ESCALATION","description":"Workflow Steps performed and Details on the Outcome","deducefaultType":"No Data"},"diagnosticDetails":[{"type":"LSD Test","status":"Completed","result":"Passed","lineId":"SWDSL0200175:1-1-1-4","userLabel":"UNI000022118321","lineOperationalStatus":"Up","lastStatusChange":"2017-10-17T05:40:57Z","serviceStability":"STABLE","cardType":"NDLT-F","cpeType":"Netcomm","dslType":"VDSL2","cpeMacAddress":"90:72:82:DB:24:1E","modemVendorIdHex":"B5004244434D0000","modemVendorIdAscii":"BDCM","systemVendorIdHex":"0F00544D4D420000","systemVendorIdAscii":"BDCM","systemVendorModelHex":"41327076364630333976320000000000","systemVendorModelAscii":"A2pv6F039v4","serialNumber":"CP1632TAEHG 799vac 17.2","actualCpeVectoringType":"g.vector capable","vectoringStatus":"Enabled","estimatedDeltDistance":"465","estimatedDeltDistanceUnitType":"m","electricalLength":"9.5","electricalLengthUnitType":"dB","rtxActualNetDataRateUpstream":"6400.0","rtxActualNetDataRateDownstream":"28000.0","rtxActualNetDataRateUnitType":"kbps","rtxActualExpectedThroughputUpstream":"6335.0","rtxActualExpectedThroughputDownstream":"27717.0","rtxActualExpectedThroughputUnitType":"kbps","rtxAttainableNetDataRateUpstream":"43725.0","rtxAttainableNetDataRateDownstream":"100164.0","rtxAttainableNetDataRateUnitType":"kbps","rtxAttainableExpectedThroughputUpstream":"43212.0","rtxAttainableExpectedThroughputDownstream":"98990.0","rtxAttainableExpectedThroughputUnitType":"kbps","tc2ExistingWbaThresholdUpstream":"38,200.0","tc2ExistingWbaThresholdDownstream":"38,200.0","tc2ExistingWbaThresholdUnitType":"kbps","tc4ExistingWbaThresholdUpstream":"26,100.0","tc4ExistingWbaThresholdDownstream":"5,100.0","tc4ExistingWbaThresholdUnitType":"kbps","higherCalculatedRateUpstream":"38,200.0","higherCalculatedRateDownstream":"38,200.0","higherCalculatedRateUnitType":"kbps","fecActualBitrateUpstream":"6400.0","fecActualBitrateDownstream":"28000.0","fecActualBitrateUnitType":"kbps","fecAttainableBitrateUpstream":"6400.0","fecAttainableBitrateDownstream":"28000.0","fecAttainableBitrateUnitType":"kbps","tc2OrderedSpeedUpstream":"30,000.0","tc2OrderedSpeedDownstream":"30,000.0","tc2OrderedSpeedUnitType":"kbps","tc2CapableDownstreamResult":"PASS","tc2CapableUpstreamResult":"PASS","tc4OrderedSpeedUpstream":"40,000.0","tc4OrderedSpeedDownstream":"40,000.0","tc4OrderedSpeedUnitType":"kbps","tc4CapableDownstreamResult":"PASS","tc4CapableUpstreamResult":"PASS","noiseMarginAverageUpstream":"22.8","noiseMarginAverageDownstream":"20.7","noiseMarginAverageUnitType":"dB","attenuationAverage":{"unitType":"dB","bands":[{"bandId":"0","downstream":"127.0","upstream":"4.5"},{"bandId":"1","downstream":"15.2","upstream":"18.5"},{"bandId":"2","downstream":"25.2","upstream":"18.5"},{"bandId":"3","downstream":"36.2","upstream":"18.5"}]},"loopAttenuationAverage":{"unitType":"dB","bands":[{"bandId":"0","downstream":"127.0","upstream":"4.5"},{"bandId":"1","downstream":"15.2","upstream":"18.5"},{"bandId":"2","downstream":"25.2","upstream":"18.5"},{"bandId":"3","downstream":"36.2","upstream":"18.5"}]},"relativeCapacityOccupationUpstream":"43.0","relativeCapacityOccupationDownstream":"46.0","relativeCapacityOccupationUnitType":"%","actualPsdUpstream":"-71.0","actualPsdDownstream":"-66.0","actualPsdUnitType":"dBm/Hz","actualRtxModeUpstream":"In Use","actualRtxModeDownstream":"In Use","userTrafficUpstream":"0","userTrafficDownstream":"0","userTrafficUnitType":"octets","outputPowerUpstream":"10.7","outputPowerDownstream":"6.3","outputPowerUnitType":"dBm","supportedCpeVectoringType":["legacy","g.vector capable"],"layer2BitrateUpstream":"15009","layer2BitrateDownstream":"4341","layer2BitrateUnitType":"kbps","physicalProfile":"12/1 Standard 6dB","coexistence":"No","fallbackState":"Not Active","fallbackProfile":"100/40 Standard 6dB","serviceSpeedIndicator":"Green","serviceSpeedDescription":"Service speed is performing within specification.","serviceSpeedIncidentEligibility":"No","serviceProblems":[{"description":"CONNECTION_STATUS-CPE_SILENT-parameter.far.end.line.failure-NOATUR","problemArea":"xDSL","impact":"no.connectivity.impact","confidenceLevel":"100","confidenceLevelUnitType":"%"}],"resolutions":[{"code":"SF000","message":"LSD Test Success"}]},{"type":"Reset Port","status":"Completed","result":"Passed","lineId":"SWDSL0200175:1-1-1-4","resolutions":[{"code":"SF00","message":"Reset Port Success"}]},{"type":"LQD Test","status":"Completed","result":"Passed","estimatedResponseTime":"60","monitoringPeriod":"300","cpeType":"OPTUS TR20","dslType":"VDSL2","vectoringStatus":"Enabled","actualCpeVectoringType":"g.vector capable","modemVendorIdHex":"B5004244434D0000","systemVendorIdHex":"0F00544D4D420000","systemVendorModelHex":"41327076364630333976320000000000","serialNumber":"CP1632TAEHG 799vac 17.2","serviceStability":"STABLE","estimatedDeltDistance":"465","electricalLength":"9.5","actualBitrateUpstream":"6400.0","actualBitrateDownstream":"28000.0","actualBitrateUnitType":"kbps","attainableBitrateUpstream":"6400.0","attainableBitrateDownstream":"28000.0","attainableBitrateUnitType":"kbps","layer2BitrateUpstream":"1000.9","layer2BitrateDownstream":"1000.9","layer2BitrateUnitType":"kbps","noiseMarginAverageUpstream":"22.8","noiseMarginAverageDownstream":"20.7","noiseMarginAverageUnitType":"dB","relativeCapacityOccupationUpstream":"43.0","relativeCapacityOccupationDownstream":"46.0","relativeCapacityOccupationUnitType":"%","actualPsdUpstream":"-71.0","actualPsdDownstream":"-66.0","actualPsdUnitType":"dBm/Hz","outputPowerUpstream":"-5.1","outputPowerDownstream":"3.3","outputPowerUnitType":"dBm","userTrafficUpstream":"621579167","userTrafficDownstream":"2147483647","userTrafficUnitType":"octets","physicalProfile":"25/5 Standard 6dB","fallbackState":"Not Active","imageUrls":["link1","link2"],"classificationHistoryImageUrls":["link1","link2"],"resolutions":[{"code":"SF00","message":"LQD Success"}]},{"type":"Reset Port","status":"Completed","result":"Failed","lineId":"SWDSL0200175:1-1-1-4","resolutions":[{"code":"SF01","message":"Reset Port Failed"}],"exceptions":[{"code":"000","message":"Reset Port Failed"}]}]}';
        String role = 'NBN';
        String channel = 'Remedy';
        String accessekerId = 'ACCESSSEEKERID';
        String executionTimestamp = '1536907257675';
        
        boolean result1 = JIGSAW_TestManagerDetailPageController.saveTestDetailCache(wriid, testType, testResult, testStatus, avcID, testName, JSONPayload, role, channel, accessekerId, executionTimestamp);
        system.assertEquals(true,result1);
        
        list<JIGSAW_Test_Details__c> cache = [select id from JIGSAW_Test_Details__c where worfklowReferenceId__c = 'WRI000000000000' LIMIT 1];
        system.assertEquals(1,cache.size());
        
        //To cover catch exception
        boolean result2 = JIGSAW_TestManagerDetailPageController.saveTestDetailCache(wriid, testType, testResult, testStatus, avcID, testName, JSONPayload, role, channel, accessekerId, 'abcd');
        system.assertEquals(false,result2);
        
        //if workFlowReferenceid is blank
        boolean result3 = JIGSAW_TestManagerDetailPageController.saveTestDetailCache('', testType, testResult, testStatus, avcID, testName, JSONPayload, role, channel, accessekerId, executionTimestamp);
        system.assertEquals(false,result3);
        
        test.stopTest();
    }
    
    static testMethod void getGraphsAsStringTest() {
        test.startTest();
        
        //successfully get it from cache
        TestManagerMockHttpRespGen mockResp0 = new TestManagerMockHttpRespGen();
        mockResp0.strResponseType = 'callout for graphs';
        mockResp0.status = '200';
        Test.setMock(HttpCalloutMock.class, mockResp0);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap1 = JIGSAW_TestManagerDetailPageController.getGraphsAsString('/gtas-api/graphs/view-test/assn-gtas-test-images/LQD/WRI000000001/graph.2.title.rtx.bitrate.ds.png','graph.2.title.rtx.bitrate.ds.png' , 'WRI000000001', 'WRI000000002');
        System.assertEquals(false, wrap1.errorOccured);
        
        //successfull callout
        TestManagerMockHttpRespGen mockResp1 = new TestManagerMockHttpRespGen();
        mockResp1.strResponseType = 'callout for graphs';
        mockResp1.status = '200';
        Test.setMock(HttpCalloutMock.class, mockResp1);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap0 = JIGSAW_TestManagerDetailPageController.getGraphsAsString('/gtas-api/graphs/view-test/assn-gtas-test-images/LQD/WRI000000001/graph.2.title.rtx.bitrate.ds.png','graph.2.title.rtx.bitrate.ds.png' , 'WRI000000005', null);
        System.assertEquals(false, wrap0.errorOccured);
        
        //Exception callout
        TestManagerMockHttpRespGen mockResp2 = new TestManagerMockHttpRespGen();
        mockResp2.strResponseType = 'callout for graphs';
        mockResp2.status = '503';
        Test.setMock(HttpCalloutMock.class, mockResp2);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap2 = JIGSAW_TestManagerDetailPageController.getGraphsAsString('/gtas-api/graphs/view-test/assn-gtas-test-images/LQD/WRI000000001/graph.2.title.rtx.bitrate.ds.png','graph.2.title.rtx.bitrate.ds.png' , 'wrongWRIID', null);
        System.assertEquals(true, wrap2.errorOccured);
        
        //in case of missing graph urls
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap3 = JIGSAW_TestManagerDetailPageController.getGraphsAsString(null,null,null,null);
        System.assertEquals(true, wrap3.errorOccured);
        
        //exception callout to cover null response
        Delete [select Id from NBNIntegrationInputs__c];
        
        TestManagerMockHttpRespGen mockResp4 = new TestManagerMockHttpRespGen();
        mockResp4.strResponseType = 'callout for graphs';
        mockResp4.status = '200';
        Test.setMock(HttpCalloutMock.class, mockResp4);
        
        JIGSAW_TestManagerDetailPageController.returnWrapper wrap4 = JIGSAW_TestManagerDetailPageController.getGraphsAsString('/gtas-api/graphs/view-test/assn-gtas-test-images/LQD/WRI000000001/graph.2.title.rtx.bitrate.ds.png','graph.2.title.rtx.bitrate.ds.png' , 'wrongWRIID', null);
        System.assertEquals(true, wrap4.errorOccured);
        
        test.stopTest();
    }

    //Mock callout response generation class
    public class TestManagerMockHttpRespGen implements HttpCalloutMock {

        public string strResponseType {
            get;
            set;
        }
        public string Status {
            get;
            set;
        }

        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            String strResponse;
            if (strResponseType == 'Initial Callout by AVC Id') {
                if (Status == '200') {
                    strResponse = '[{"workflowName":"Atlas Verification","worfklowReferenceId":"WRI000000000000","role":"Field tech","channel":"Atlas","workflowType":"Guided","status":"Completed","result":"Passed","technologyType":"Fibre To The Node","accessekerId":"ASI000000000000","testDetails":[{"type":"LSD Test","result":"Passed","resolutionText":["LSD Success"],"timeStamp":"1527235921398"},{"type":"Loopback","result":"Passed","resolutionText":["Loopback Executed Successfully"],"timeStamp":"1527233258920"}],"timeStamp":"1527235921398"},{"workflowName":"LSD","worfklowReferenceId":"WRI000000000000","role":"nbn","channel":"CSC","workflowType":"Atomic","technologyType":"Fibre To The Node","accessekerId":"ASI000000000000","status":"Completed","result":"Passed","testDetails":[{"type":"LSD Test","result":"Passed","resolutionText":["LSD Success"],"timeStamp":"1527235921398"}],"timeStamp":"1527235921398"}]';
                } else {
                    strResponse = 'No matching records found';
                }

            } else if (strResponseType == 'Second Callout for Test Details') {
                if (Status == '200') {
                    strResponse = '{"testReferenceId":"WRI000000000000","status":"Completed","role":"NBN","channel":"Remedy","workflowOutcome":{"status":"Passed","outcome":"ESCALATION","description":"Workflow Steps performed and Details on the Outcome","deducefaultType":"No Data"},"diagnosticDetails":[{"type":"LSD Test","status":"Completed","result":"Passed","lineId":"SWDSL0200175:1-1-1-4","userLabel":"UNI000022118321","lineOperationalStatus":"Up","lastStatusChange":"2017-10-17T05:40:57Z","serviceStability":"STABLE","cardType":"NDLT-F","cpeType":"Netcomm","dslType":"VDSL2","cpeMacAddress":"90:72:82:DB:24:1E","modemVendorIdHex":"B5004244434D0000","modemVendorIdAscii":"BDCM","systemVendorIdHex":"0F00544D4D420000","systemVendorIdAscii":"BDCM","systemVendorModelHex":"41327076364630333976320000000000","systemVendorModelAscii":"A2pv6F039v4","serialNumber":"CP1632TAEHG 799vac 17.2","actualCpeVectoringType":"g.vector capable","vectoringStatus":"Enabled","estimatedDeltDistance":"465","estimatedDeltDistanceUnitType":"m","electricalLength":"9.5","electricalLengthUnitType":"dB","rtxActualNetDataRateUpstream":"6400.0","rtxActualNetDataRateDownstream":"28000.0","rtxActualNetDataRateUnitType":"kbps","rtxActualExpectedThroughputUpstream":"6335.0","rtxActualExpectedThroughputDownstream":"27717.0","rtxActualExpectedThroughputUnitType":"kbps","rtxAttainableNetDataRateUpstream":"43725.0","rtxAttainableNetDataRateDownstream":"100164.0","rtxAttainableNetDataRateUnitType":"kbps","rtxAttainableExpectedThroughputUpstream":"43212.0","rtxAttainableExpectedThroughputDownstream":"98990.0","rtxAttainableExpectedThroughputUnitType":"kbps","tc2ExistingWbaThresholdUpstream":"38,200.0","tc2ExistingWbaThresholdDownstream":"38,200.0","tc2ExistingWbaThresholdUnitType":"kbps","tc4ExistingWbaThresholdUpstream":"26,100.0","tc4ExistingWbaThresholdDownstream":"5,100.0","tc4ExistingWbaThresholdUnitType":"kbps","higherCalculatedRateUpstream":"38,200.0","higherCalculatedRateDownstream":"38,200.0","higherCalculatedRateUnitType":"kbps","fecActualBitrateUpstream":"6400.0","fecActualBitrateDownstream":"28000.0","fecActualBitrateUnitType":"kbps","fecAttainableBitrateUpstream":"6400.0","fecAttainableBitrateDownstream":"28000.0","fecAttainableBitrateUnitType":"kbps","tc2OrderedSpeedUpstream":"30,000.0","tc2OrderedSpeedDownstream":"30,000.0","tc2OrderedSpeedUnitType":"kbps","tc2CapableDownstreamResult":"PASS","tc2CapableUpstreamResult":"PASS","tc4OrderedSpeedUpstream":"40,000.0","tc4OrderedSpeedDownstream":"40,000.0","tc4OrderedSpeedUnitType":"kbps","tc4CapableDownstreamResult":"PASS","tc4CapableUpstreamResult":"PASS","noiseMarginAverageUpstream":"22.8","noiseMarginAverageDownstream":"20.7","noiseMarginAverageUnitType":"dB","attenuationAverage":{"unitType":"dB","bands":[{"bandId":"0","downstream":"127.0","upstream":"4.5"},{"bandId":"1","downstream":"15.2","upstream":"18.5"},{"bandId":"2","downstream":"25.2","upstream":"18.5"},{"bandId":"3","downstream":"36.2","upstream":"18.5"}]},"loopAttenuationAverage":{"unitType":"dB","bands":[{"bandId":"0","downstream":"127.0","upstream":"4.5"},{"bandId":"1","downstream":"15.2","upstream":"18.5"},{"bandId":"2","downstream":"25.2","upstream":"18.5"},{"bandId":"3","downstream":"36.2","upstream":"18.5"}]},"relativeCapacityOccupationUpstream":"43.0","relativeCapacityOccupationDownstream":"46.0","relativeCapacityOccupationUnitType":"%","actualPsdUpstream":"-71.0","actualPsdDownstream":"-66.0","actualPsdUnitType":"dBm/Hz","actualRtxModeUpstream":"In Use","actualRtxModeDownstream":"In Use","userTrafficUpstream":"0","userTrafficDownstream":"0","userTrafficUnitType":"octets","outputPowerUpstream":"10.7","outputPowerDownstream":"6.3","outputPowerUnitType":"dBm","supportedCpeVectoringType":["legacy","g.vector capable"],"layer2BitrateUpstream":"15009","layer2BitrateDownstream":"4341","layer2BitrateUnitType":"kbps","physicalProfile":"12/1 Standard 6dB","coexistence":"No","fallbackState":"Not Active","fallbackProfile":"100/40 Standard 6dB","serviceSpeedIndicator":"Green","serviceSpeedDescription":"Service speed is performing within specification.","serviceSpeedIncidentEligibility":"No","serviceProblems":[{"description":"CONNECTION_STATUS-CPE_SILENT-parameter.far.end.line.failure-NOATUR","problemArea":"xDSL","impact":"no.connectivity.impact","confidenceLevel":"100","confidenceLevelUnitType":"%"}],"resolutions":[{"code":"SF000","message":"LSD Test Success"}]},{"type":"Reset Port","status":"Completed","result":"Passed","lineId":"SWDSL0200175:1-1-1-4","resolutions":[{"code":"SF00","message":"Reset Port Success"}]},{"type":"LQD Test","status":"Completed","result":"Passed","estimatedResponseTime":"60","monitoringPeriod":"300","cpeType":"OPTUS TR20","dslType":"VDSL2","vectoringStatus":"Enabled","actualCpeVectoringType":"g.vector capable","modemVendorIdHex":"B5004244434D0000","systemVendorIdHex":"0F00544D4D420000","systemVendorModelHex":"41327076364630333976320000000000","serialNumber":"CP1632TAEHG 799vac 17.2","serviceStability":"STABLE","estimatedDeltDistance":"465","electricalLength":"9.5","actualBitrateUpstream":"6400.0","actualBitrateDownstream":"28000.0","actualBitrateUnitType":"kbps","attainableBitrateUpstream":"6400.0","attainableBitrateDownstream":"28000.0","attainableBitrateUnitType":"kbps","layer2BitrateUpstream":"1000.9","layer2BitrateDownstream":"1000.9","layer2BitrateUnitType":"kbps","noiseMarginAverageUpstream":"22.8","noiseMarginAverageDownstream":"20.7","noiseMarginAverageUnitType":"dB","relativeCapacityOccupationUpstream":"43.0","relativeCapacityOccupationDownstream":"46.0","relativeCapacityOccupationUnitType":"%","actualPsdUpstream":"-71.0","actualPsdDownstream":"-66.0","actualPsdUnitType":"dBm/Hz","outputPowerUpstream":"-5.1","outputPowerDownstream":"3.3","outputPowerUnitType":"dBm","userTrafficUpstream":"621579167","userTrafficDownstream":"2147483647","userTrafficUnitType":"octets","physicalProfile":"25/5 Standard 6dB","fallbackState":"Not Active","imageUrls":["link1","link2"],"classificationHistoryImageUrls":["link1","link2"],"resolutions":[{"code":"SF00","message":"LQD Success"}]},{"type":"Reset Port","status":"Completed","result":"Failed","lineId":"SWDSL0200175:1-1-1-4","resolutions":[{"code":"SF01","message":"Reset Port Failed"}],"exceptions":[{"code":"000","message":"Reset Port Failed"}]}]}';
                } else {
                    strResponse = 'No matching records found';
                }
            }else if (strResponseType == 'callout for graphs') {
                if (Status == '200') {
                    strResponse = 'test png response';
                } else {
                    strResponse = 'No matching records found';
                }
            }

            res.setBody(strResponse);
            res.setStatusCode(integer.valueOf(Status));

            return res;
        }
    }

}