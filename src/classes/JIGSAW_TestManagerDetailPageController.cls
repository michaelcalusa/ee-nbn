/***************************************************************************************************
Class Name:         JIGSAW_TestManagerDetailPageController
Class Type:         Controller Class 
Version:            1.0 
Created Date:       9/08/2018
Description:        Used by Testmanagerdetailpage lightning component. Created as part of epic CUSTSA-17210
Modification Log:
* Developer          Date             Descriptionasdasd
* ---------------------------------------------------------getWorkOrderRequest-----------------------------------------                 
* Rohit Mathur      9/08/2018         Created - Version 1.0 Refer CUSTSA-15932 for story description
* Sridevi/Rohit		6/09/2018         Added the method getTestDetailsAsWRI() to get the test detail. Refer CUSTSA-21716 for story description.
****************************************************************************************************/
public class JIGSAW_TestManagerDetailPageController {

    /***************************************************************************************************
    Method Name: getAtomicCSList
    Method Parameters: String of test type based on which we get all configuration records
    Method Return Type: list of JIGSAW_TestManager_Config__c
    Method Description :- This method is used to get all the test manager config records for selected test type from the summary panel
    Version: 1.0
    Created Date: 9/08/2018 
    ***************************************************************************************************/
    @AuraEnabled
    public static list < JIGSAW_TestManager_Config__c > getAtomicCSList(String testType) {
        if (String.isNotBlank(testType)) {
            list < JIGSAW_TestManager_Config__c > lstCustSet = [Select Id, Name, Test_Type__c, Parent_JSON_Key__c, JSON_Key__c, SF_Label_Name__c,
                Is_Visible_in_Jigsaw__c, Table_Number__c, Table_Row_Number__c, Concatenate_With__c,
                Is_Array__c, Table_Heading__c, Is_DateTime__c, Is_Graph__c
                from JIGSAW_TestManager_Config__c
                where Test_Type__c like: testType
                order by Table_Row_Number__c
            ];
            return lstCustSet;
        } else {
            return null;
        }
    }

    /***************************************************************************************************
    Method Name: getTestHistorySummary
    Method Parameters: String of record id on which the test manager is open and the default durion from lightning design attribute
    Method Return Type: Instance of returnWrapper
    Method Description :- This method is used to do the duration calculation and making the first summary callout to GTAS to get the test manager summary
    Version: 1.0
    Created Date: 9/08/2018 
    ***************************************************************************************************/
    @AuraEnabled
    public static returnWrapper getTestHistorySummary(String recordId, String duration) {

        //initialize wrapper class to post data to lightning component
        returnWrapper wrap = new returnWrapper();


        if (String.isNotBlank(recordId)) {

            //Updated the query by RM (CUSTSA-28977) to include current status and closed date to filter the test history summary callout data in case of closed incident. 
            list < Incident_Management__c > IncList = [select id, AVC_Id__c, Incident_Number__c, Reported_Date__c, Current_Status__c, closedDate__c from Incident_Management__c where ID =: recordId];

            if (!IncList.isEmpty()) {
                //AVC id is required to make call out get Test History summary
                String AVC = IncList[0].AVC_Id__c;

                try {

                    //calculation to get the total hours count from to get all the test run within last n hrs.[In case of non terminal incident status]
                    Long reportedDate = IncList[0].Reported_Date__c.getTime();
                    Long currentTime = DateTime.now().getTime();
                    Long milliseconds = currentTime - reportedDate;

                    Long seconds = milliseconds / 1000;
                    Long minutes = seconds / 60;
                    Long hr = minutes / 60;
                    Long days = hr / 24;
                    system.debug(milliseconds + '" "' + seconds + '" "' + minutes + '" "' + hr + '" "' + days);

                    //queryDuration holds the total calculated hours (admin provided hrs + difference between current time and the time incident was raised)
                    //queryDuration is an optional parameter for callout but should be passed in as per our requirements
                    Integer queryDuration;
                    Integer calculatedHrs = (integer) hr;

                    queryDuration = calculatedHrs + integer.valueOf(String.valueOf(duration));
                    
                    DateTime startDatetime =  IncList[0].Reported_Date__c - (Integer.valueof(duration.trim())/24);
                    
                    String startDate;
                    String endDate;
                    
                    if(IncList[0].Current_Status__c != null && IncList[0].Current_Status__c == 'Closed' && IncList[0].closedDate__c != null && IncList[0].Reported_Date__c != null){
                        //create filter in case of closed incident 
                        startDate = startDatetime.format('yyyy-MM-dd HH:mm:ss');
                        endDate = IncList[0].closedDate__c.format('yyyy-MM-dd HH:mm:ss');
                    }else if(IncList[0].Reported_Date__c != null){
                        //create filter in case of non terminal incident 
                        startDate = startDatetime.format('yyyy-MM-dd HH:mm:ss');
                        endDate = Datetime.now().format('yyyy-MM-dd HH:mm:ss');
                    }
                    
                    
                    
                    HTTPResponse res;
        
                    if(System.Label.JIGSAW_toggleGetTestHistorySummary.equalsIgnoreCase('true')){
                        res = integrationUtility.getTestHistorySummary(AVC, startDate, endDate);
                    }else{
                        res = integrationUtility.getTestHistorySummary(AVC, queryDuration);
                    }
                    

                    //responseSinceDate is calculated in order to display the timestamp on UI so that operators are aware that the data which 
                    //they are viewing is since "responseSinceDate".
                    DateTime responseSinceDate = IncList[0].Reported_Date__c.addHours(-Integer.valueOf(duration));
                    String dateformat = 'dd/MM/yyyy HH:mm';
                    String responseSinceDateText = responseSinceDate.format(dateformat, '' + userinfo.getTimeZone().toString());
                    system.debug('>>>> responseSinceDateText : ' + responseSinceDateText);
                    //res will be null in case of any apex error. The error occured can be viewed in application log
                    if (res == null) {
                        //in case of apex/internal error
                        wrap.jsonResponse = null;
                        wrap.responseSinceDate = responseSinceDateText;
                        wrap.errorOccured = true;
                        wrap.errorText = 'Unable to get Service Test History due to system error. Please contact system adminstrator for further details.';
                    } else {

                        if (res.getStatusCode() != 200) {
                            //in case of callout exceptions
                            wrap.jsonResponse = res.getBody();
                            wrap.responseSinceDate = responseSinceDateText;
                            wrap.errorOccured = true;
                            wrap.errorText = res.getBody();
                            wrap.responseStatusCode = res.getStatusCode();
                            wrap.responseStatus = res.getStatus();
                            GlobalUtility.logIntMessage(GlobalConstants.ERROR_RECTYPE_NAME, 'JIGSAW_TestManagerDetailPageController', 'getTestHistorySummary', IncList[0].Incident_Number__c, '', IncList[0].Incident_Number__c, res.getBody(), 0);

                        } else {
                            //in case of successful callout
                            wrap.jsonResponse = res.getBody();
                            wrap.responseSinceDate = responseSinceDateText;
                            wrap.errorOccured = false;
                            wrap.AVC = AVC;
                        }
                    }
                    System.debug('JIGSAW_TestManagerDetailPageController==>getTestHistorySummary==>wrap:' + wrap);
                } catch (Exception ex) {
                    GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME, 'JIGSAW_TestManagerDetailPageController', 'getTestHistorySummary', IncList[0].Incident_Number__c, '', '', '', ex, 0);
                    System.debug('EXCEPTION ::: ' + ex.getStackTraceString());

                    wrap.jsonResponse = null;
                    wrap.errorOccured = true;
                    wrap.errorText = 'Unable to get Service Test History due to system error. Please contact system adminstrator for further details.';
                    return wrap;
                }
            } else {
                wrap.jsonResponse = null;
                wrap.errorOccured = true;
                wrap.errorText = 'Unable to get Service Test History due to system error. Please contact system adminstrator for further details.';
                return wrap;
            }
        } else {
            wrap.jsonResponse = null;
            wrap.errorOccured = true;
            wrap.errorText = 'Unable to get Service Test History due to system error. Please contact system adminstrator for further details.';
            return wrap;
        }

        return wrap;
    }

    /***************************************************************************************************
    Method Name: getTestDetailsAsWRI
    Method Parameters: String values of selected workflowreferenceID, test type, role,channel, accesssekerId, and workflowName
    Method Return Type: Instance of returnWrapper
    Method Description :- This method is used to make the second callout to get the test results based on selected wriId and also return the relevent date to cache the response.
    Version: 1.0
    Created Date: 9/08/2018 
    ***************************************************************************************************/
    @AuraEnabled
    public static returnWrapper getTestDetailsAsWRI(string wriId, String testType, String role, String channel, String accessekerId, String type, String workflowName) {
        //initialize wrapper class to post data to lightning component
        returnWrapper wrap = new returnWrapper();

        try {
            type = '%' + type + '%';
            wrap.lstCustSet = [Select Id, Name, Test_Type__c, Parent_JSON_Key__c, JSON_Key__c, SF_Label_Name__c,
                Is_Visible_in_Jigsaw__c, Table_Number__c, Table_Row_Number__c, Concatenate_With__c,
                Is_Array__c, Table_Heading__c, Is_DateTime__c, Is_Graph__c
                from JIGSAW_TestManager_Config__c
                where Test_Type__c like: type
                order by Table_Row_Number__c
            ];

            if (String.isNotBlank(wriId)) {

                List < JIGSAW_Test_Details__c > cacheLst = [select worfklowReferenceId__c, JSON_response__c, result__c, workflowType__c from JIGSAW_Test_Details__c where worfklowReferenceId__c =: wriId];

                if (!cacheLst.isEmpty()) {
                    //Map the wrapper to return the cache data.   
                    wrap.jsonResponse = cacheLst[0].JSON_response__c;
                    wrap.errorOccured = false;
                    wrap.isFromCache = true;

                } else {
                    HTTPResponse res = integrationUtility.getTestHistoryDetail(wriId);
                    if (res == null) {
                        wrap.jsonResponse = null;
                        wrap.errorOccured = true;
                    } else {
                        if (res.getStatusCode() != 200) {
                            //Callout Exceptions
                            wrap.jsonResponse = res.getBody();
                            wrap.errorOccured = true;
                            wrap.errorText = res.getBody();
                            wrap.responseStatusCode = res.getStatusCode();
                            wrap.responseStatus = res.getStatus();
                            GlobalUtility.logIntMessage(GlobalConstants.ERROR_RECTYPE_NAME, 'JIGSAW_TestManagerDetailPageController', 'getTestDetailsAsWRI', wriId, '', wriId, res.toString() + ' - ' + res.getBody(), 0);
                        } else {
                            //Successful callout
                            wrap.jsonResponse = res.getBody();
                            wrap.errorOccured = false;
                            wrap.isFromCache = false;
                            wrap.testType = testType;
                            wrap.role = role;
                            wrap.channel = channel;
                            wrap.accessekerId = accessekerId;
                            wrap.workflowName = workflowName;
                        }
                    }
                }
            } else {
                wrap.jsonResponse = null;
                wrap.errorOccured = true;
                wrap.errorText = 'Unable to get Service Test details due to system error. Please contact system adminstrator for further details.';
                system.debug('wriId is empty');
            }
        } catch (Exception ex) {
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME, 'JIGSAW_TestManagerDetailPageController', 'getTestDetailsAsWRI', wriId, '', '', '', ex, 0);
            System.debug('EXCEPTION ::: ' + ex.getStackTraceString());
            wrap.jsonResponse = null;
            wrap.errorOccured = true;
            wrap.errorText = 'Unable to get Service Test History due to system error. Please contact system adminstrator for further details.';
            return wrap;
        }
        return wrap;
    }

    /***************************************************************************************************
    Method Name: saveTestDetailCache
    Method Parameters: String values of selected test required for caching the test manager result 
    Method Return Type: Boolean value.
    Method Description :- This method is used to save the cache of selected test.
    Version: 1.0
    Created Date: 9/08/2018 
    ***************************************************************************************************/
    @AuraEnabled
    public static boolean saveTestDetailCache(String wriid, String testType, String testResult, String testStatus, String avcID, String testName, String JSONPayload, String role, String channel, String accessekerId, String executionTimestamp) {
        try {
            if (string.isNotBlank(wriid)) {
                JIGSAW_Test_Details__c cacheRec = new JIGSAW_Test_Details__c();
                cacheRec.worfklowReferenceId__c = wriid;
                cacheRec.workflowType__c = testType;
                cacheRec.result__c = testResult;
                cacheRec.Status__c = testStatus;
                cacheRec.AVCId__c = avcID;
                cacheRec.workflowName__c = testName;
                cacheRec.JSON_response__c = JSONPayload;
                cacheRec.role__c = role;
                cacheRec.channel__c = channel;
                cacheRec.accessekerId__c = accessekerId;

                DateTime endDT = null;

                if (String.isNotBlank(executionTimestamp)) {
                    Long endDTLong = Long.valueOf(executionTimestamp);
                    endDT = DateTime.newInstance(endDTLong);
                }

                cacheRec.executionTimestamp__c = endDT;

                insert cacheRec;
                return true;
            }
            return false;
        } catch (exception ex) {
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME, 'JIGSAW_TestManagerDetailPageController', 'saveTestDetailCache', wriid, '', '', '', ex, 0);
            System.debug('EXCEPTION ::: ' + ex.getStackTraceString());
            return false;
        }
    }


    /***************************************************************************************************
    Method Name: saveTestDetailCache
    Method Parameters: String values of selected test required for caching the test manager result 
    Method Return Type: Boolean value.
    Method Description :- This method is used to save the cache of selected test.
    Version: 1.0
    Created Date: 9/08/2018 
    ***************************************************************************************************/
    @AuraEnabled
    public static returnWrapper getGraphsAsString(string graphUrl, String graphName, String wriId, String parentWriId) {

        returnWrapper wrap = new returnWrapper();
        String cachedGraphName = '';
        String cachedWorkflowReferenceId = '';
        if (String.isNotBlank(parentWriId)) {
            //In case the graph is from guided workflow
            cachedGraphName = wriId + '###' + graphName;
            cachedWorkflowReferenceId = parentWriId;
        } else {
            //In case the graph is from atomic test
            cachedGraphName = wriId + '###' + graphName;
            cachedWorkflowReferenceId = wriId;
        }

        //CUSTSA-28521 - Added by RM - Check if the graph cache exists 
        if (String.isNotBlank(cachedGraphName) && String.isNotBlank(cachedWorkflowReferenceId)) {
            List < Attachment > cacheList = [Select id from Attachment where ParentId IN(Select id from JIGSAW_Test_Details__c where worfklowReferenceId__c =: cachedWorkflowReferenceId) AND Name =: cachedGraphName];
            //if cache exists
            if (!cacheList.isEmpty()) {
                String imageUrl = '/servlet/servlet.FileDownload?file=' + cacheList[0].id;
                system.debug(imageUrl);

                wrap.imageUrl = imageUrl;
                wrap.errorOccured = false;

            } else if (String.isNotBlank(graphUrl)) {
                //In case graph not found in cache object, Make callout to get graphs
                HTTPResponse response = integrationUtility.getGraphsIntegration(graphUrl);
                system.debug(response.getbody());
                system.debug(response.getbody());
                if (response != null) {
                    if (response.getStatusCode() == 200) {
                        //in case of success callout to get graphs
                        Blob image = response.getBodyAsBlob();
                        string imageUrl = 'data:' + response.getHeader('Content-Type') + ';base64,' + EncodingUtil.base64Encode(image);
                        system.debug(imageUrl);

                        wrap.imageUrl = imageUrl;
                        wrap.errorOccured = false;

                        //CUSTSA-28521 - Added by RM - cache logic
                        try {
                            system.debug('>>>>>GRAPH NAME : ' + graphName);
                            system.debug('>>>>>WRIID : ' + wriId);
                            system.debug('>>>>>cachedGraphName  : ' + cachedGraphName);
                            system.debug('>>>>>cachedWorkflowReferenceId : ' + cachedWorkflowReferenceId);
                            if (String.isNotBlank(wriId) && String.isNotBlank(graphName)) {
                                list < JIGSAW_Test_Details__c > chcheList = [select id from JIGSAW_Test_Details__c where worfklowReferenceId__c =: cachedWorkflowReferenceId Limit 1];
                                if (!chcheList.isEmpty()) {
                                    Attachment graphCache = new Attachment();
                                    graphCache.body = image;
                                    graphCache.ParentId = chcheList.get(0).id;
                                    graphCache.contentType = 'image/png';
                                    graphCache.name = cachedGraphName;

                                    insert graphCache;
                                }
                            }
                        } catch (Exception ex) {
                            System.debug('Exception while saving graphs : ' + ex.getStackTraceString() + ' :: Exception Message :' + ex.getMessage());
                            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME, 'JIGSAW_TestManagerDetailPageController', 'getGraphsAsString', cachedWorkflowReferenceId, '', '', '', ex, 0);
                        }


                    } else {
                        wrap.jsonResponse = response.getBody();
                        wrap.errorOccured = true;
                        wrap.errorText = response.getBody();
                        wrap.responseStatusCode = response.getStatusCode();
                        wrap.responseStatus = response.getStatus();
                    }
                } else {
                    wrap.errorOccured = true;
                    wrap.errorText = 'Unable to get graph due to system error. Please contact system adminstrator for further details.';
                }
            }
        } else {
            wrap.errorOccured = true;
            wrap.errorText = 'Unable to get graph as graph url is missing. Please contact system adminstrator for further details.';
        }

        return wrap;
    }

    /***************************************************************************************************
    Class Name: saveTestDetailCache
    Method Parameters: This class is used to wrap the JSON response with some additional values that can be used on lighting component in order to 
    handle errors and can be modified in future in order to pass some extra values.
    Version: 1.0
    Created Date: 9/08/2018 
    ***************************************************************************************************/
    public class returnWrapper {

        @AuraEnabled
        public String imageUrl {
            get;
            set;
        }
        @AuraEnabled
        public String role {
            get;
            set;
        }
        @AuraEnabled
        public String channel {
            get;
            set;
        }
        @AuraEnabled
        public String accessekerId {
            get;
            set;
        }
        @AuraEnabled
        public String testType {
            get;
            set;
        }
        @AuraEnabled
        public String AVC {
            get;
            set;
        }
        @AuraEnabled
        public Boolean isFromCache {
            get;
            set;
        }
        @AuraEnabled
        public String jsonResponse {
            get;
            set;
        }
        @AuraEnabled
        public String responseSinceDate {
            get;
            set;
        }
        @AuraEnabled
        public Boolean errorOccured {
            get;
            set;
        }
        @AuraEnabled
        public String errorText {
            get;
            set;
        }
        @AuraEnabled
        public Integer responseStatusCode {
            get;
            set;
        }
        @AuraEnabled
        public String responseStatus {
            get;
            set;
        }
        @AuraEnabled
        public String workflowName {
            get;
            set;
        }
        @AuraEnabled
        public List < JIGSAW_TestManager_Config__c > lstCustSet {
            get;
            set;
        }
    }
}