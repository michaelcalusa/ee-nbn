/* purpose: this class is created to insert remedy integration handling records for updateSalesforceRecordId action asynchronously
*            as the execution of insert triggers on incident takes a while to process
*  jira#: created as a part of CUSTSA-28544
*  team: JIGSAW
*/
public class JIGSAW_UpdateSFIdAction implements Queueable{
private List<String> outJSONList;
    
    public JIGSAW_UpdateSFIdAction(List<String> tmpJSONList){
        outJSONList = tmpJSONList;
    }
    
    public void execute(QueueableContext context) {
        if(!outJSONList.isEmpty()){
            // the following method creates RIH record to eventually publish on the bus
            remedyRPAErrorHandlingUtil.deserializeRPAErrorJSON(outJSONList,'Outbound');
        }
    }
}