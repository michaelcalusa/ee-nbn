/*------------------------------------------------------------
Author:     Gaurav Saraswat
Company:       Contractor
Description:   Utility Class for Jigsaw
History
<Date>      <Author>     <Description>
15/02/2019    GS        Adding Method evaluateModuleVisibility to implement the logic for the jigsaw module visibility    */
public class JIGSAW_Utils{
	
    
    /***************************************************************************************************
    Method Name:        evaluateModuleVisibility
    Method Parameters:  Incident Management Record, Module Names Set & current Status Value Check with Tech Type Check
    Method Return Type: List of Module Configurations Records
    Method Description: User to filter out modules visibility based on Current Status and Enabled Checkbox
    Created Date:       23 Feb 2019
    ***************************************************************************************************/
    public static List<JIGSAW_Module_Config__mdt> evaluateModuleVisibility(Incident_Management__c incObj, Set<String> moduleNames, Boolean isCurrentStatusCheck, Boolean isTechTypeCheck){
        String configQuery = 'SELECT ' + String.join(retrieveAllFieldsList('JIGSAW_Module_Config__mdt'),',') +
                                    ' FROM JIGSAW_Module_Config__mdt ';
        List<String> whereClause = new List<String>();
        if(isCurrentStatusCheck){
            whereClause.add(' Incident_Current_Status__c LIKE \'%'+(String)incObj.get('Current_Status__c')+'%\' ');
        }
        if(isTechTypeCheck){
            whereClause.add(' Technology_Type__c LIKE \'%'+(String)incObj.get('Technology__c')+'%\' ');
        }
        if(!moduleNames.isEmpty()){
            whereClause.add(' DeveloperName IN : moduleNames');
        }
        whereClause.add(' Enabled__c = true ');
        if(!whereClause.isEmpty()){
            configQuery += ' WHERE ' + String.join(whereClause, ' AND ');
        }
        List<JIGSAW_Module_Config__mdt> configMetadata = database.query(configQuery);
        for(JIGSAW_Module_Config__mdt config : configMetadata){
            /*if(String.isNotBlank(config.Visbility_Class__c) && String.isNotBlank(config.Visibility_Method__c)){
                Map<String,Object> inputParamsMap = new Map<String,Object>{JIGSAW_Constants.LITERAL_METHODNAME => config.Visibility_Method__c};
                Type t = Type.forName(config.Visbility_Class__c);
                JIGSAW_OpenInterface openInterface = (JIGSAW_OpenInterface)t.newInstance();
                inputParamsMap.put('sObjRecord',incObj);
                config.Enabled__c = openInterface.evaluateModuleValidation(inputParamsMap);
            }*/
        }
        
        return configMetadata;
    }
    
    /***************************************************************************************************
    Method Name:        retrieveAllFieldsList
    Method Parameters:  sObject name
    Method Return Type: List of all Fields Related to input sobject
    Method Description: Method can be used to get all field list exists on input sobject 
    Created Date:       23 Feb 2019
    ***************************************************************************************************/
    public static List<String> retrieveAllFieldsList(String sObjectName){
		
        Map<String, Schema.SObjectType> gdMap = Schema.getGlobalDescribe();
        Schema.Describesobjectresult dsr = gdMap.get(sObjectName).getDescribe();
        return new List<String>(dsr.fields.getMap().keySet());
    }
    
    
}