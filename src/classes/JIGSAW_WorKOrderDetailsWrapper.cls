public class JIGSAW_WorKOrderDetailsWrapper{
   
    @AuraEnabled
    public String errorCode;
    @AuraEnabled
    public String errorDesc;
    @AuraEnabled
    public String fnlconstraint;
    @AuraEnabled
    public String href; //http://wwm-int-sit1.slb.nbndc.local:2080/maxrest/wwm/json/os/workorder/TkJOQ08jI1dPUjEwMDAwMDcyNTM4MQ==
    @AuraEnabled
    public String actfinish;    //2018-05-15T09:48:00+1000
    @AuraEnabled
    public String actstart; //2018-05-15T09:46:00+1000
    @AuraEnabled
    public String nbncausecode; //DDA
    @AuraEnabled
    public String nbnremedycode;    //RDC
    @AuraEnabled
    public String nbnwospecid;  //FTTP_Service_Assurance_Work_Order_Specification
    @AuraEnabled
    public String nbnwospectype;    //WorkOrder Specification
    @AuraEnabled
    public String nbnwospecversion; //1.3.0
    @AuraEnabled
    public String orgid;    //NBN CO
    @AuraEnabled
    public String problemcode;  //DDC
    @AuraEnabled
    public String siteid;   //NBNCO
    @AuraEnabled
    public String wonum;    //WOR100000725381
    @AuraEnabled
    public String worklog_collectionref;    //
    @AuraEnabled
    public String nbnpriorityvalue; //45
    @AuraEnabled
    public String gbsstatusreasonhist_collectionref;    //http://wwm-int-dev15.slb.nbndc.local:2080/maxrest/wwm/json/os/workorder/TkJOQ08jI1dPUjEwMDAwMDIwMjY0MQ==/gbsstatusreasonhist
    @AuraEnabled
    public String workorderspec_collectionref;  //http://wwm-int-dev15.slb.nbndc.local:2080/maxrest/wwm/json/os/workorder/TkJOQ08jI1dPUjEwMDAwMDIwMjY0MQ==/workorderspec
    @AuraEnabled
    public String fnlconstaint;
    @AuraEnabled
    public String remarkdesc;
    @AuraEnabled
    public cls_wostatus[] wostatus;
    @AuraEnabled
    public cls_gbsstatusreasonhist[] gbsstatusreasonhist;
    @AuraEnabled
    public cls_workorderspec[] workorderspec; //http://wwm-int-sit1.slb.nbndc.local:2080/maxrest/wwm/json/os/workorder/TkJOQ08jI1dPUjEwMDAwMDcyNTM4MQ==/worklog
    @AuraEnabled
    public String wostatus_collectionref;   //http://wwm-int-sit1.slb.nbndc.local:2080/maxrest/wwm/json/os/workorder/TkJOQ08jI1dPUjEwMDAwMDcyNTM4MQ==/wostatus
    @AuraEnabled
    public String nbntestdetails_collectionref; //http://wwm-int-sit1.slb.nbndc.local:2080/maxrest/wwm/json/os/workorder/TkJOQ08jI1dPUjEwMDAwMDcyNTM4MQ==/nbntestdetails
    @AuraEnabled
    public List<cls_worklog> worklog;
    @AuraEnabled
    public cls_nbntestdetails[] nbntestdetails;
    
    public class cls_worklog {
        @AuraEnabled
        public String href; //http://wwm-int-sit1.slb.nbndc.local:2080/maxrest/wwm/json/os/workorder/TkJOQ08jI1dPUjEwMDAwMDcyNTM4MQ==/worklog/NDIzLDk3OQ==
        @AuraEnabled
        public boolean clientviewable;
        @AuraEnabled
        public String createby; //MXINTATLAS
        @AuraEnabled
        public String createdate;   //2018-05-15T09:46:46+1000
        @AuraEnabled
        public String description;  //Complete Job Safely - Action_Commence_Work
        @AuraEnabled
        public String description_longdescription;  //Standard Controls
        @AuraEnabled
        public String logtype;  //CLIENTNOTE
        @AuraEnabled
        public Integer worklogid;   //423979
    }
    class cls_wostatus {
        @AuraEnabled
        public String href; //http://wwm-int-sit1.slb.nbndc.local:2080/maxrest/wwm/json/os/workorder/TkJOQ08jI1dPUjEwMDAwMDcyNTM4MQ==/wostatus/MSw0MDcsNzYw
        @AuraEnabled
        public String changeby; //LACHLANFRY
        @AuraEnabled
        public String changedate;   //2018-05-15T09:49:35+1000
        @AuraEnabled
        public String status;   //COMP
        @AuraEnabled
        public Integer wostatusid;  //1407760
    }
    class cls_gbsstatusreasonhist {
        @AuraEnabled
        public String href; //http://wwm-int-sit1.slb.nbndc.local:2080/maxrest/wwm/json/os/workorder/TkJOQ08jI1dPUjEwMDAwMDcyNTM4MQ==/gbsstatusreasonhist/ODY1LDc4NA==
        @AuraEnabled
        public String changeby; //LACHLANFRY
        @AuraEnabled
        public String changedate;   //2018-05-15T09:49:35+1000
        @AuraEnabled
        public String fromstatus;   //INPRG
        @AuraEnabled
        public Integer gbsstatusreasonhistid;   //865784
        @AuraEnabled
        public String reasoncode;   //FINISHED
        @AuraEnabled
        public String status;   //COMP
        @AuraEnabled
        public String gbsreason_collectionref;  //http://wwm-int-sit1.slb.nbndc.local:2080/maxrest/wwm/json/os/workorder/TkJOQ08jI1dPUjEwMDAwMDcyNTM4MQ==/gbsstatusreasonhist/gbsreason
        @AuraEnabled
        public cls_gbsreason[] gbsreason;
    }
    class cls_gbsreason {
        @AuraEnabled
        public String href; //http://wwm-int-sit1.slb.nbndc.local:2080/maxrest/wwm/json/os/workorder/TkJOQ08jI1dPUjEwMDAwMDcyNTM4MQ==/gbsstatusreasonhist/ODY1LDc4NA==/gbsreason/RklOSVNIRUQ=
        @AuraEnabled
        public String description;  //Finished
        @AuraEnabled
        public String reasoncode;   //FINISHED
    }
    public class cls_nbntestdetails {
        @AuraEnabled
        public String href; //http://wwm-int-sit1.slb.nbndc.local:2080/maxrest/wwm/json/os/workorder/TkJOQ08jI1dPUjEwMDAwMDcyNTM4MQ==/nbntestdetails/
        @AuraEnabled
        public String createdate;   //2018-05-15
        @AuraEnabled
        public String testid;   //WRI900005429180
        @AuraEnabled
        public String testname; //Verification Test
        @AuraEnabled
        public String testoutcome;  //Completed
        @AuraEnabled
        public String testresult;   //Passed
    }
    public class cls_workorderspec {
        @AuraEnabled
        public String href; //http://wwm-int-sit1.slb.nbndc.local:2080/maxrest/wwm/json/os/workorder/TkJOQ08jI1dPUjEwMDAwMDcyNTM4MQ==/workorderspec/V09SMTAwMDAwNzI1MzgxIyNOQk5DTyMjQVZDLUQgSUQjIw==
        @AuraEnabled
        public String assetattrid;  //AVC-D ID
        @AuraEnabled
        public String gbsspecvalue; //AVC000000871713
        @AuraEnabled
        public Integer workorderspecid; //4295360
    }
    public static JIGSAW_WorKOrderDetailsWrapper parse(){
        return (JIGSAW_WorKOrderDetailsWrapper) System.JSON.deserialize(jsonString, JIGSAW_WorKOrderDetailsWrapper.class);
    }

    public static String jsonString =        '{'+
        '   "actfinish":"2018-05-15T09:48:00+1000",'+
        '   "actstart":"2018-05-15T09:46:00+1000",'+
        '   "nbncausecode":"DDA",'+
        '   "nbnpriorityvalue":"99",'+
        '   "nbnremedycode":"RDC",'+
        '   "nbnwospecid":"FTTP_Service_Assurance_Work_Order_Specification",'+
        '   "nbnwospectype":"WorkOrder Specification",'+
        '   "nbnwospecversion":"1.3.0",'+
        '   "orgid":"NBN CO",'+
        '   "problemcode":"DDC",'+
        '   "siteid":"NBNCO",'+
        '   "wonum":"WOR100000725381",'+
        '   "worklog":['+
        '      {'+
        '         "clientviewable":true,'+
        '         "createby":"MXINTATLAS",'+
        '         "createdate":"2018-05-15T09:46:46+1000",'+
        '         "description":"Complete Job Safely - Action_Commence_Work",'+
        '         "description_longdescription":"Standard Controls",'+
        '         "logtype":"CLIENTNOTE",'+
        '         "worklogid":423979'+
        '      },'+
        '      {'+
        '         "clientviewable":true,'+
        '         "createby":"LACHLANFRY",'+
        '         "createdate":"2018-05-15T09:49:34+1000",'+
        '         "description":"Complete Job Safely - Action_Finish_Activity",'+
        '         "description_longdescription":"Standard Controls",'+
        '         "logtype":"CLIENTNOTE",'+
        '         "worklogid":423934'+
        '      },'+
        '      {'+
        '         "clientviewable":false,'+
        '         "createby":"LACHLANFRY",'+
        '         "createdate":"2018-05-15T09:49:00+1000",'+
        '         "description":"The work order is Claimable",'+
        '         "description_longdescription":"The work order is Claimable",'+
        '         "logtype":"CLAIMABLE",'+
        '         "worklogid":423935'+
        '      }'+
        '   ],'+
        '   "wostatus":['+
        '      {'+
        '         "changeby":"LACHLANFRY",'+
        '         "changedate":"2018-05-15T09:49:35+1000",'+
        '         "status":"COMP",'+
        '         "wostatusid":1407760'+
        '      },'+
        '      {'+
        '         "changeby":"MXINTATLAS",'+
        '         "changedate":"2018-05-15T09:46:44+1000",'+
        '         "status":"INPRG",'+
        '         "wostatusid":1407757'+
        '      },'+
        '      {'+
        '         "changeby":"LACHLANFRY",'+
        '         "changedate":"2018-05-15T09:44:11+1000",'+
        '         "status":"ACKNOWLEDGED",'+
        '         "wostatusid":1407755'+
        '      },'+
        '      {'+
        '         "changeby":"MXINTADM",'+
        '         "changedate":"2018-05-14T15:56:11+1000",'+
        '         "status":"APPR",'+
        '         "wostatusid":1407109'+
        '      },'+
        '      {'+
        '         "changeby":"MXINTADM",'+
        '         "changedate":"2018-05-14T15:56:10+1000",'+
        '         "status":"ALLOCATED",'+
        '         "wostatusid":1407108'+
        '      },'+
        '      {'+
        '         "changeby":"MXINTADM",'+
        '         "changedate":"2018-05-14T15:56:10+1000",'+
        '         "status":"TRIAGED",'+
        '         "wostatusid":1407107'+
        '      },'+
        '      {'+
        '         "changeby":"MXINTADM",'+
        '         "changedate":"2018-05-14T15:56:07+1000",'+
        '         "status":"WAPPR",'+
        '         "wostatusid":1407102'+
        '      }'+
        '   ],'+
        '   "gbsstatusreasonhist":['+
        '      {'+
        '         "changeby":"LACHLANFRY",'+
        '         "changedate":"2018-05-15T09:49:35+1000",'+
        '         "fromstatus":"INPRG",'+
        '         "gbsstatusreasonhistid":865784,'+
        '         "reasoncode":"FINISHED",'+
        '         "status":"COMP",'+
        '         "gbsreason":['+
        '            {'+
        '               "description":"Finished",'+
        '               "reasoncode":"FINISHED"'+
        '            }'+
        '         ]'+
        '      },'+
        '      {'+
        '         "changeby":"MXINTATLAS",'+
        '         "changedate":"2018-05-15T09:46:46+1000",'+
        '         "fromstatus":"INPRG",'+
        '         "gbsstatusreasonhistid":865855,'+
        '         "reasoncode":"COMMENCE-WORK",'+
        '         "status":"INPRG",'+
        '         "gbsreason":['+
        '            {'+
        '               "description":"Commence Work",'+
        '               "reasoncode":"COMMENCE-WORK"'+
        '            }'+
        '         ]'+
        '      },'+
        '      {'+
        '         "changeby":"MXINTATLAS",'+
        '         "changedate":"2018-05-15T09:46:44+1000",'+
        '         "fromstatus":"ACKNOWLEDGED",'+
        '         "gbsstatusreasonhistid":865780,'+
        '         "reasoncode":"TECH-ON-SITE",'+
        '         "status":"INPRG",'+
        '         "gbsreason":['+
        '            {'+
        '               "description":"Tech on site",'+
        '               "reasoncode":"TECH-ON-SITE"'+
        '            }'+
        '         ]'+
        '      },'+
        '      {'+
        '         "changeby":"LACHLANFRY",'+
        '         "changedate":"2018-05-15T09:45:09+1000",'+
        '         "fromstatus":"ACKNOWLEDGED",'+
        '         "gbsstatusreasonhistid":865778,'+
        '         "reasoncode":"ON-TIME",'+
        '         "status":"ACKNOWLEDGED",'+
        '         "gbsreason":['+
        '            {'+
        '               "description":"On Time",'+
        '               "reasoncode":"ON-TIME"'+
        '            }'+
        '         ]'+
        '      },'+
        '      {'+
        '         "changeby":"LACHLANFRY",'+
        '         "changedate":"2018-05-15T09:44:12+1000",'+
        '         "fromstatus":"APPR",'+
        '         "gbsstatusreasonhistid":865775,'+
        '         "reasoncode":"ASSIGNED",'+
        '         "status":"ACKNOWLEDGED",'+
        '         "gbsreason":['+
        '            {'+
        '               "description":"Assigned",'+
        '               "reasoncode":"ASSIGNED"'+
        '            }'+
        '         ]'+
        '      },'+
        '      {'+
        '         "changeby":"MXINTADM",'+
        '         "changedate":"2018-05-14T15:56:11+1000",'+
        '         "fromstatus":"ALLOCATED",'+
        '         "gbsstatusreasonhistid":865244,'+
        '         "reasoncode":"ASSIGNED-TO-SDP",'+
        '         "status":"APPR",'+
        '         "gbsreason":['+
        '            {'+
        '               "description":"Assigned to Delivery Partner",'+
        '               "reasoncode":"ASSIGNED-TO-SDP"'+
        '            }'+
        '         ]'+
        '      },'+
        '      {'+
        '         "changeby":"MXINTADM",'+
        '         "changedate":"2018-05-14T15:56:10+1000",'+
        '         "fromstatus":"TRIAGED",'+
        '         "gbsstatusreasonhistid":865243,'+
        '         "reasoncode":"ASSIGNED-TO-SDP",'+
        '         "status":"ALLOCATED",'+
        '         "gbsreason":['+
        '            {'+
        '               "description":"Assigned to Delivery Partner",'+
        '               "reasoncode":"ASSIGNED-TO-SDP"'+
        '            }'+
        '         ]'+
        '      },'+
        '      {'+
        '         "changeby":"MXINTADM",'+
        '         "changedate":"2018-05-14T15:56:10+1000",'+
        '         "fromstatus":"WAPPR",'+
        '         "gbsstatusreasonhistid":865242,'+
        '         "reasoncode":"ASSIGNED-TO-SDP",'+
        '         "status":"TRIAGED",'+
        '         "gbsreason":['+
        '            {'+
        '               "description":"Assigned to Delivery Partner",'+
        '               "reasoncode":"ASSIGNED-TO-SDP"'+
        '            }'+
        '         ]'+
        '      }'+
        '   ],'+
        '   "nbntestdetails":['+
        '      {'+
        '         "createdate":"2018-05-15",'+
        '         "testid":"WRI900005429180",'+
        '         "testname":"Verification Test",'+
        '         "testoutcome":"Completed",'+
        '         "testresult":"Passed"'+
        '      },'+
        '      {'+
        '         "createdate":"2018-05-15",'+
        '         "testid":"WRI900005429122",'+
        '         "testname":"Verification Test",'+
        '         "testoutcome":"Cancelled",'+
        '         "testresult":"Not Applicable"'+
        '      }'+
        '   ],'+
        '   "workorderspec":['+
        '      {'+
        '         "assetattrid":"AVC-D ID",'+
        '         "gbsspecvalue":"AVC000000871713",'+
        '         "workorderspecid":4295360'+
        '      },'+
        '      {'+
        '         "assetattrid":"FAULT TYPE",'+
        '         "gbsspecvalue":"New Service Never Worked",'+
        '         "workorderspecid":4295361'+
        '      },'+
        '      {'+
        '         "assetattrid":"FTTP NTD TYPE",'+
        '         "workorderspecid":4295362'+
        '      },'+
        '      {'+
        '         "assetattrid":"INSTALLED FIBRE IN ANY PART OF TELSTRA LIC",'+
        '         "workorderspecid":4295563'+
        '      },'+
        '      {'+
        '         "assetattrid":"NTD INSTALLATION LOCATION",'+
        '         "workorderspecid":4295564'+
        '      },'+
        '      {'+
        '         "assetattrid":"PRE CALL PERFORMED",'+
        '         "workorderspecid":4295565'+
        '      },'+
        '      {'+
        '         "assetattrid":"REASON DID NOT USE TELSTRA LIC",'+
        '         "workorderspecid":4295566'+
        '      },'+
        '      {'+
        '         "assetattrid":"REQUIRED CONTROLS",'+
        '         "workorderspecid":4295567'+
        '      },'+
        '      {'+
        '         "assetattrid":"RESTRICTION TYPE",'+
        '         "workorderspecid":4295571'+
        '      },'+
        '      {'+
        '         "assetattrid":"SAFETY CHECK",'+
        '         "gbsspecvalue":"Standard Controls",'+
        '         "workorderspecid":4295568'+
        '      },'+
        '      {'+
        '         "assetattrid":"WAY LEAVE CONSENT",'+
        '         "gbsspecvalue":"No",'+
        '         "workorderspecid":4295569'+
        '      },'+
        '      {'+
        '         "assetattrid":"WIRED TV INSTALLATION REQUIRED",'+
        '         "workorderspecid":4295570'+
        '      }'+
        '   ]'+
        '}';
        }