/***************************************************************************************************
Class Name:         JIGSAW_WorkHistoryWrapper
Class Type:         WrapperClass 
Company :           IBM
Version:            1.0 
Created Date:       27 June 2018
Function:           
Input Parameters:   None 
Output Parameters:  None
Description:        Wrapper class to hold deserialized JSON value. This wrapper class is created to display 
work order history details on incident management record page when user clicks on Show Work orderInfo link under truckroll component.
created as part of CUSTSA-15925.
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Nishank Tandon      27/06/2018      Created - Version 1.0 
****************************************************************************************************/

@isTest
public class JIGSAW_WorkHistoryWrapper{
    @AuraEnabled
    public cls_member[] member;
    @AuraEnabled
    Public String errorCode;
    @AuraEnabled
    Public String errorDesc;
    public class cls_member {
        @AuraEnabled
        public String href; //http://wwm-rp-dev5.global.nbndc.local:1080/maxrest/wwm/json/os/workhistory/SR%23%23WRQ100000022774
        //public String class;    //SR
        @AuraEnabled
        Public String errorCode;
        @AuraEnabled
        Public String errorDesc;
        @AuraEnabled
        public String classstructureid; //MR1802-6735
        @AuraEnabled
        public boolean historyflag;
        @AuraEnabled
        public String nbnaccessseeker;  //ASI000000220282
        @AuraEnabled
        public String nbnaccesstech;    //FTTC
        @AuraEnabled
        public String nbnappointmentid; //APT100000045501
        @AuraEnabled
        public String nbncategory;  //DEMAND INSTALL
        @AuraEnabled
        public String nbncopperpathid;  //CPI000000000000
        @AuraEnabled
        public String nbndemandtype;    //STANDARD INSTALL
        @AuraEnabled
        public String nbnlocationid;    //LOC000009567829
        @AuraEnabled
        public String nbnreasoncode;    //COMPLETE
        @AuraEnabled
        public String nbnreferenceid;   //5879d685-bb61-4c2b-a2ed-884828340ac6
        @AuraEnabled
        public String nbnregion;    //C-CSA400000000028
        @AuraEnabled
        public String nbnregiontype;    //URBAN
        @AuraEnabled
        public String nbnserviceclass;  //32
        @AuraEnabled
        public String nbnslatime;   //2018-05-22T23:59:00+1000
        @AuraEnabled
        public String nbnslotname;  //08-12
        @AuraEnabled
        public String nbnworkforce; //DP1019
        @AuraEnabled
        public String orgid;    //NBN CO
        @AuraEnabled
        public String reportdate;   //2018-05-08T01:49:18+1000
        @AuraEnabled
        public String siteid;   //NBNCO
        @AuraEnabled
        public String status;   //COMPLETED
        @AuraEnabled
        public String ticketid; //WRQ100000022774
        @AuraEnabled
        public String nbnworkhistoryview_collectionref; //http://wwm-rp-dev5.global.nbndc.local:1080/maxrest/wwm/json/os/workhistory/SR%23%23WRQ100000022774/nbnworkhistoryview
        @AuraEnabled
        public cls_nbnworkhistoryview[] nbnworkhistoryview;
    }
        public class cls_nbnworkhistoryview {
        @AuraEnabled
        public String href; //http://wwm-rp-dev5.global.nbndc.local:1080/maxrest/wwm/json/os/workhistory/SR%23%23WRQ100000022774/nbnworkhistoryview/NBNCO%23%23WOR100000200960
        @AuraEnabled
            public String classstructureid; //MR1802-6735
        @AuraEnabled
            public boolean historyflag;
        @AuraEnabled
            public String nbnappend;    //2018-05-15T12:00:00+1000
        @AuraEnabled
            public String nbnappointmentid; //APT100000045501
        @AuraEnabled
            public String nbnappstart;  //2018-05-15T08:00:00+1000
        @AuraEnabled
            public String nbncopperpathid;  //CPI000000000000
        @AuraEnabled
            public String nbnlocationid;    //LOC000009567829 
        
        @AuraEnabled
            public String nbnplannedremediationdate;
        @AuraEnabled
            public String nbnprdupdatereason;
        
        
        @AuraEnabled
            public String nbnreasoncode;    //FINISHED
        @AuraEnabled
            public String nbnrequirement;   //CUT IN
        @AuraEnabled
            public String nbnticketid;  //WRQ100000022774
        @AuraEnabled
            public String nbntype;  //AERIAL
        @AuraEnabled
            public String nbnworkforce; //DP1019
        @AuraEnabled
            public String orgid;    //NBN CO
        @AuraEnabled
            public String parent;   //WOR100000200956
        @AuraEnabled
            public String reportdate;   //2018-05-08T01:49:28+1000
        @AuraEnabled
            public String siteid;   //NBNCO
        @AuraEnabled
            public String status;   //COMP
        @AuraEnabled
            public String statusdate;   //2018-05-08T01:50:35+1000
        @AuraEnabled
            public String woclass;  //WORKORDER
        @AuraEnabled
            public String wonum;    //WOR100000200960
        @AuraEnabled
            public String nbnreferenceid;
        @AuraEnabled
            public String classstructure_collectionref; //http://wwm-rp-dev5.global.nbndc.local:1080/maxrest/wwm/json/os/workhistory/SR%23%23WRQ100000022774/nbnworkhistoryview/classstructure
        @AuraEnabled
            public cls_classstructure[] classstructure;
    }
    class cls_classstructure {
        @AuraEnabled
        public String href; //http://wwm-rp-dev5.global.nbndc.local:1080/maxrest/wwm/json/os/workhistory/SR%23%23WRQ100000022774/nbnworkhistoryview/NBNCO%23%23WOR100000200960/classstructure/MR1802-6735
        @AuraEnabled
        public String classstructureid; //MR1802-6735
        @AuraEnabled
        public String hierarchypath;    //OPERATE \ DEMAND INSTALL \ FTTC \ CUT IN \ AERIAL
    }
    public static JIGSAW_WorkHistoryWrapper parse(String json){
        return (JIGSAW_WorkHistoryWrapper) System.JSON.deserialize(json, JIGSAW_WorkHistoryWrapper.class);
    }
    
}