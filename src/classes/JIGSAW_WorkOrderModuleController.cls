/*------------------------------------------------------------
Author: Nishank Tandon July 06, 2018
Company: IBM
Description: Server-side controller for 3 Lightning components that used to display Work Order module and work order more info.
History
<Date>      <Author>     <Description>
------------------------------------------------------------*/
public class JIGSAW_WorkOrderModuleController {
    
    // Class Variables.
    private static String response_WorkOrder; 
    private static String response_WorkHistory; 
    private static Map<String, String> mapCustomMetadataType = new Map<String, String>();
    private static Map<String, Jigsaw_PCR_Action_Filter__c> mapPCRCode = new Map<String, Jigsaw_PCR_Action_Filter__c>();
    private static String incidentTechnology;
    
    
    // Calling from Lightning component work order more info: First Tab
    @AuraEnabled
    public static Wrapper_FirstTab fetch_FirstTab (String workOrderNumber, String incidentId, String dayCount,String workRequestNumber) {
        Wrapper_FirstTab objFirstTabResult = new Wrapper_FirstTab();
        
        // Work History
        JIGSAW_WorkHistoryWrapper objWrapper_WorkHistory;
        try{
            System.debug('@@@' + workOrderNumber);
            System.debug('@@@' + incidentId);
            Wrapper_WorkHistoryResponse objResponseWorkHistory = getData_WorkHistory(incidentId, dayCount,null);
            String resultWorkHistory = objResponseWorkHistory.response;
            
            System.debug('@@@' + resultWorkHistory);
            if(String.isBlank(resultWorkHistory) == false){
                objWrapper_WorkHistory = (JIGSAW_WorkHistoryWrapper)JSON.deserialize(resultWorkHistory, JIGSAW_WorkHistoryWrapper.Class);
                System.debug('@@@' + objWrapper_WorkHistory);
            }
        }catch(exception ex){
            // add execption if parsing error
            objFirstTabResult.errorCode = 'Unknown';
            objFirstTabResult.errorDesc = ex.getMessage();
            System.debug('@@@' + ex.getMessage());
            return objFirstTabResult;
        }
        
        // Bind Work History API specs
        if(objWrapper_WorkHistory != null){
            JIGSAW_WorkHistoryWrapper.cls_nbnworkhistoryview objDetailFromHistoryAPI = getDetailsFromHistoryAPI(objWrapper_WorkHistory, workOrderNumber);
            System.debug('@@@' + objDetailFromHistoryAPI);
            
            if(objDetailFromHistoryAPI != null){
                System.debug('@@@' + objDetailFromHistoryAPI.nbnrequirement);
                if(objDetailFromHistoryAPI.nbnrequirement == 'APPOINTMENT' || objDetailFromHistoryAPI.nbnrequirement == 'COMMITMENT'){
                    objFirstTabResult.appointmentType = GlobalUtility.convertToCamelCase(objDetailFromHistoryAPI.nbnrequirement);
                    objFirstTabResult.category = 'Service Restoration';
                }
                else if(objDetailFromHistoryAPI.nbnrequirement == 'REMEDIATION'){
                    objFirstTabResult.appointmentType = 'Not Applicable';
                    objFirstTabResult.category = 'Remediation';
                }
                
                System.debug('@@@' + objFirstTabResult.appointmentType);
                
                objFirstTabResult.nbnreferenceid = objDetailFromHistoryAPI.nbnreferenceid;
                objFirstTabResult.status = objDetailFromHistoryAPI.status;
                objFirstTabResult.statusReason = objDetailFromHistoryAPI.nbnreasoncode;
                if (objDetailFromHistoryAPI.statusdate != null){
                    objFirstTabResult.lastUpdated = formatDate(objDetailFromHistoryAPI.statusdate);
                }
                
                objFirstTabResult.serviceDeliveryPartner = objDetailFromHistoryAPI.nbnworkforce;
                
                
                if(getMapCustomMetadataType.get(objDetailFromHistoryAPI.nbnworkforce) == null){
                    objFirstTabResult.serviceDeliveryPartner = objDetailFromHistoryAPI.nbnworkforce;
                }else{
                    objFirstTabResult.serviceDeliveryPartner = getMapCustomMetadataType.get(objDetailFromHistoryAPI.nbnworkforce);
                }
                
                
                objFirstTabResult.technicianId = 'Not Available';
                if (objDetailFromHistoryAPI.nbnplannedremediationdate != null){
                    objFirstTabResult.plannedRemediationDate = formatDate(objDetailFromHistoryAPI.nbnplannedremediationdate);
                }
                objFirstTabResult.reasonForplannedRemediationDate = objDetailFromHistoryAPI.nbnprdupdatereason;
                objFirstTabResult.plannedCompletionDate = 'Not Available';
            }
        }
        
        
        
        // Work Order
        JIGSAW_WorKOrderDetailsWrapper objWrapper_WorkOrder;
        try{
            Wrapper_WorkOrderResponse objResponse = getData_WorkOrder(workOrderNumber);
            
            if(objResponse != null && String.isBlank(objResponse.response) == false){
                objWrapper_WorkOrder = (JIGSAW_WorKOrderDetailsWrapper)JSON.deserialize(objResponse.response, JIGSAW_WorKOrderDetailsWrapper.Class);
                //CUSTSA-28209: fetch additional attrributes from the deserialized wrapper
                objFirstTabResult.workOrderDetailsJSON = objResponse.response;
                //End of CUSTSA-28209: fetch additional attrributes from the deserialized wrapper
                System.debug('@@@' + objWrapper_WorkOrder);
            }
            objFirstTabResult.errorCode = objResponse.errorCode;
            objFirstTabResult.errorDesc = objResponse.errorDetails;
        }
        catch(Exception ex){
            objFirstTabResult.errorCode = 'UNKNOWN';
            objFirstTabResult.errorDesc = ex.getMessage();
            
            // add exception: parsing error
            System.debug('@@@' + ex.getMessage());
        }
        // Bind Work Order API specs
        if(objWrapper_WorkOrder != null){
            
            if(getMapCustomMetadataType.get(objWrapper_WorkOrder.nbnpriorityvalue) == null){
                objFirstTabResult.priority = objWrapper_WorkOrder.nbnpriorityvalue;
            }else{
                objFirstTabResult.priority = objWrapper_WorkOrder.nbnpriorityvalue + ' - ' + getMapCustomMetadataType.get(objWrapper_WorkOrder.nbnpriorityvalue);
            }
            if (objWrapper_WorkOrder.actstart != null){
                objFirstTabResult.actualStartTime = formatDate(objWrapper_WorkOrder.actstart);
            }
            if (objWrapper_WorkOrder.actfinish != null){
                objFirstTabResult.actualFinishTime = formatDate(objWrapper_WorkOrder.actfinish);
            }
            if (objWrapper_WorkOrder.fnlconstraint != null){
                objFirstTabResult.finishNoLaterThan = formatDate(objWrapper_WorkOrder.fnlconstraint);
            }   
            objFirstTabResult.RSPPlannedRemediationDate = 'Not Available';
            
            Map<String, String> mapPCRCode = getPCRCode(objWrapper_WorkOrder.problemcode, objWrapper_WorkOrder.nbncausecode, objWrapper_WorkOrder.nbnremedycode);
            objFirstTabResult.problem = mapPCRCode.get('P');
            objFirstTabResult.Cause = mapPCRCode.get('C');
            objFirstTabResult.Remedy = mapPCRCode.get('R');
            objFirstTabResult.closureSummary = objWrapper_WorkOrder.remarkdesc;
            
            objFirstTabResult.showFirstTab = true;
            // set tab visibilities for second, third and fourth tabs.
            WorkOrder wo = queryFaultDetails(workOrderNumber, workRequestNumber);
            if(wo != null && (String.isNotBlank(wo.faultDetails__c) || String.isNotBlank(wo.additionalInformation__c))){
                
                objFirstTabResult.showSecondTab = true;
            }
            
            if(objWrapper_WorkOrder.worklog != null && objWrapper_WorkOrder.worklog.size() > 0){
                objFirstTabResult.showThirdTab = true;
            }
            
            if(objWrapper_WorkOrder.nbntestdetails != null && objWrapper_WorkOrder.nbntestdetails.size() > 0){
                objFirstTabResult.showFourthTab = true;
            }
        }
        System.debug('@@@' + objFirstTabResult);
        
        return objFirstTabResult;
    }
    
    // Calling from Lightning component work order more info: Second Tab
    @AuraEnabled
    public static WorkOrder fetch_FaultDetails (String workOrderNumber, String incidentId, String workRequestNumber) {
        //workOrderNumber = '00197887';
        return queryFaultDetails(workOrderNumber, workRequestNumber);
    }
    
    // Calling from Lightning component work order more info: Third Tab
    @AuraEnabled
    public static Wrapper_TechNotes fetch_TechNotes (String workOrderNumber, String incidentId) {
        JIGSAW_WorKOrderDetailsWrapper objWrapper_WorkOrder;
        
        Wrapper_WorkOrderResponse objResponse = getData_WorkOrder(workOrderNumber);
        try{
            if(objResponse != null && String.isBlank(objResponse.response) == false){
                objWrapper_WorkOrder = (JIGSAW_WorKOrderDetailsWrapper)JSON.deserialize(objResponse.response, JIGSAW_WorKOrderDetailsWrapper.Class);
                System.debug('@@@' + objWrapper_WorkOrder);
            }
        }
        catch(Exception ex){
            // add exception: parsing error
            System.debug('@@@' + ex.getMessage());
        }
        
        System.debug('@@@' + objWrapper_WorkOrder);
        System.debug('@@@' + objWrapper_WorkOrder.worklog);
        
        List<Wrapper_TechNoes_Data> lstSobjectData = new List<Wrapper_TechNoes_Data>();
        if(objWrapper_WorkOrder != null && objWrapper_WorkOrder.worklog != null && objWrapper_WorkOrder.worklog.size() > 0){
            for(JIGSAW_WorKOrderDetailsWrapper.cls_worklog obj :objWrapper_WorkOrder.worklog ){
                Wrapper_TechNoes_Data objData = new Wrapper_TechNoes_Data();
                
                System.debug('@@@' + obj);
                
                
                String cDate = obj.createdate;
                cDate = cDate.substring(0, cDate.lastIndexOf(':')).replace('T', ' @ ');
                System.debug('@@@@' + cDate);
                objData.createdate = cDate; //DateTime.valueOf(obj.createdate).format('dd/MM/yyyy');
                objData.logtype = obj.logtype;
                
                System.debug('@@@@' + getMapCustomMetadataType);
                
                if(getMapCustomMetadataType.get(obj.createby) == null){
                    objData.createdFrom = obj.createby;
                }else{
                    objData.createdFrom = getMapCustomMetadataType.get(obj.createby);
                }
                objData.summary = obj.description;
                objData.longSummary = obj.description_longdescription;
                lstSobjectData.add(objData);
            }
        }
        System.debug('@@@' + lstSobjectData);
        
        // Tech Notes Columns
        List <Wrapper_DataTableColumns> cols = new List <Wrapper_DataTableColumns>();
        cols.add(new Wrapper_DataTableColumns('createdate','Date', 'text', true));
        cols.add(new Wrapper_DataTableColumns('logtype','Type',  'text', false));
        cols.add(new Wrapper_DataTableColumns('createdFrom','From',  'text', false));
        cols.add(new Wrapper_DataTableColumns('summary', 'Summary', 'text', false));
        
        Wrapper_TechNotes objTechNotesData = new Wrapper_TechNotes();
        objTechNotesData.lstColumns = cols;
        objTechNotesData.lstSobjectData = lstSobjectData;
        objTechNotesData.totalRecords = lstSobjectData.size();
        System.debug('@@@' + objTechNotesData );
        return objTechNotesData;
        
    }
    
    // Calling from Lightning component work order more info: Fourth Tab
    @AuraEnabled
    public static Wrapper_TestDetails fetch_TestTab (String workOrderNumber, String incidentId) {
        JIGSAW_WorKOrderDetailsWrapper objWrapper_WorkOrder;
        
        Wrapper_WorkOrderResponse objResponse = getData_WorkOrder(workOrderNumber);
        try{
            if(objResponse != null && String.isBlank(objResponse.response) == false){
                System.debug('@@@' + objResponse.response);
                objResponse.response = objResponse.response.replaceAll('\n','').replaceAll('\r','');
                
                objWrapper_WorkOrder = (JIGSAW_WorKOrderDetailsWrapper)JSON.deserialize(objResponse.response, JIGSAW_WorKOrderDetailsWrapper.Class);
                System.debug('@@@' + objWrapper_WorkOrder);
            }
        }
        catch(Exception ex){
            // add exception: parsing error
            System.debug('@@@' + ex.getMessage());
        }
        
        System.debug('@@@' + objWrapper_WorkOrder);
        System.debug('@@@' + objWrapper_WorkOrder.nbntestdetails);
        
        List<Wrapper_Test_Data> lstSobjectData = new List<Wrapper_Test_Data>();
        if(objWrapper_WorkOrder != null && objWrapper_WorkOrder.nbntestdetails != null && objWrapper_WorkOrder.nbntestdetails.size() > 0){
            System.debug('@@@' + objWrapper_WorkOrder.nbntestdetails.size()); 
            for(JIGSAW_WorKOrderDetailsWrapper.cls_nbntestdetails obj :objWrapper_WorkOrder.nbntestdetails){
                Wrapper_Test_Data objData = new Wrapper_Test_Data();
                objData.createdate = obj.createdate;
                objData.testid = obj.testid;
                objData.testname = obj.testname;
                if(String.isBlank(obj.testresult) == true){
                    objData.testoutcome = obj.testoutcome;
                }
                else{
                    objData.testoutcome = obj.testoutcome + '-' +  obj.testresult;    
                }
                
                lstSobjectData.add(objData);
            }
        }
        System.debug('@@@' + lstSobjectData);
        
        // Tech Notes Columns
        List <Wrapper_DataTableColumns> cols = new List <Wrapper_DataTableColumns>();
        cols.add(new Wrapper_DataTableColumns('createdate','Date', 'text', true));
        cols.add(new Wrapper_DataTableColumns('testid','Test ID',  'text', false));
        cols.add(new Wrapper_DataTableColumns('testname','Name',  'text', false));
        cols.add(new Wrapper_DataTableColumns('testoutcome', 'Result', 'text', false));
        
        Wrapper_TestDetails objTestTab = new Wrapper_TestDetails();
        objTestTab.lstColumns = cols;
        objTestTab.lstSobjectData = lstSobjectData;
        objTestTab.totalRecords = lstSobjectData.size();
        System.debug('@@@' + objTestTab );
        return objTestTab;
    }
    
    // Helper method to query Fault Details
    private static WorkOrder queryFaultDetails(String workOrderNumber, String workRequestNumber){
        List<WorkOrder> lstWO = [SELECT WorkOrderNumber, faultDetails__c, additionalInformation__c, wrqIdentifier__c FROM WorkOrder WHERE wrqIdentifier__c = :workRequestNumber];
        if(lstWO.size() > 0){
            return lstWO.get(0);
        }
        return null;
    }
    
    
    // Helper method to iterate throug the API and returned only related WO details.
    private static JIGSAW_WorkHistoryWrapper.cls_nbnworkhistoryview getDetailsFromHistoryAPI(JIGSAW_WorkHistoryWrapper obj, String woNumber){
        System.debug('@@@' + woNumber);
        System.debug('@@@' + obj.member);
        
        if(obj != null && obj.member != null){
            for (JIGSAW_WorkHistoryWrapper.cls_member memberData: obj.member) {
                if(memberData.nbnworkhistoryview != null){
                    for (JIGSAW_WorkHistoryWrapper.cls_nbnworkhistoryview objWODetail: memberData.nbnworkhistoryview) {
                        System.debug('@@@' + woNumber);
                        System.debug('@@@' + objWODetail.wonum);
                        if(objWODetail.wonum == woNumber){
                            return objWODetail;
                        }
                    }
                }
            }
        }
        return null;
    }
    
    // Helper Method to get custom metadata 
    private static Map<String, String> getMapCustomMetadataType{
        get{
            if(mapCustomMetadataType.size() == 0){
                
                for (JIGSAW_WorkOrder_Details__mdt mdt :[SELECT Field_Name__c, MasterLabel, JIGSAW_Display_Field__c FROM JIGSAW_WorkOrder_Details__mdt]) {
                    mapCustomMetadataType.put(mdt.MasterLabel, mdt.JIGSAW_Display_Field__c);
                }
            }
            return mapCustomMetadataType;
        }
    }
    
    // Helper method to get PCR Code.
    private static Map<String, String> getPCRCode(String p, String c, String r){
        if(mapPCRCode.size() == 0){
            for (Jigsaw_PCR_Action_Filter__c mdtPCR :[SELECT PCR_Code_Label__c, C_Description__c, P_Description__c, R_Description__c, WO_Type_FTTN_FTTB__c FROM Jigsaw_PCR_Action_Filter__c]) {
                mapPCRCode.put(mdtPCR.PCR_Code_Label__c + '_' + mdtPCR.WO_Type_FTTN_FTTB__c, mdtPCR);
            }
        }
        System.debug('@@##' + mapPCRCode);
        System.debug('@@##' + p);
        System.debug('@@##' + c);
        System.debug('@@##' + r);
        
        Jigsaw_PCR_Action_Filter__c obj = mapPCRCode.get(p + ' _ ' + c + ' _ ' + r  +  '_' + incidentTechnology);
        
        Map<String, String> response = new Map<String, String>();
        if(obj != null){
            response.put('P', obj.P_Description__c);
            response.put('C', obj.C_Description__c);
            response.put('R', obj.R_Description__c);
            return response;
        }else{
            response.put('P', p);
            response.put('C', c);
            response.put('R', r);
            return response;
        }
        
    }
    
    // Helper Method to change date format
    private static string formatDate(String s){
        try{
            if(s.length() == 24){
                s = s.replace('T', ' ').substring(0, s.indexOf('+'));
                DateTime dt = Datetime.valueOf(s);
                s = dt.format('EEE MMM dd,yyyy \'at\' hh:mm a');
                return s;
            }
            else if(s.length() == 10){
                Integer year = Integer.valueOf(s.split('-')[0]);
                Integer month = Integer.valueOf(s.split('-')[1]);
                Integer day = Integer.valueOf(s.split('-')[2]);
                DateTime myDate = DateTime.newInstance(year, month, day);
                s = myDate.format('EEE MMM dd,yyyy');
                return s;
            }
            else{
                return 'Not Available';
            }
        }
        catch(Exception ex){
            return 'Not Available';
        }
    }
    
    // Wrapper class for first tab
    public class Wrapper_FirstTab{
        
        @AuraEnabled
        public String errorCode{
            get;
            set;
        }
        
        @AuraEnabled
        public String errorDesc{
            get;
            set;
        }
        
        @AuraEnabled
        public String appointmentType{
            get;
            set;
        }
        
        @AuraEnabled
        public String category{
            get;
            set;
        }
        @AuraEnabled
        public String priority{
            get;
            set;
        }
        @AuraEnabled
        public String nbnReferenceId{
            get;
            set;
        }
        @AuraEnabled
        public String plannedRemediationDate{
            get;
            set;
        }
        @AuraEnabled
        public String reasonForplannedRemediationDate {
            get;
            set;
        }
        @AuraEnabled
        public String plannedCompletionDate{
            get;
            set;
        }
        @AuraEnabled
        public String RSPPlannedRemediationDate{
            get;
            set;
        }
        @AuraEnabled
        public String finishNoLaterThan{
            get;
            set;
        }
        @AuraEnabled
        public String serviceDeliveryPartner{
            get;
            set;
        }
        @AuraEnabled
        public String technicianId{
            get;
            set;
        }
        @AuraEnabled
        public String actualStartTime{
            get;
            set;
        }
        @AuraEnabled
        public String actualFinishTime{
            get;
            set;
        }
        @AuraEnabled
        public String status{
            get;
            set;
        }
        @AuraEnabled
        public String statusReason{
            get;
            set;
        }
        @AuraEnabled
        public String lastUpdated{
            get;
            set;
        }
        @AuraEnabled
        public String problem{
            get;
            set;
        }
        @AuraEnabled
        public String cause{
            get;
            set;
        }
        @AuraEnabled
        public String remedy{
            get;
            set;
        }
        @AuraEnabled
        public String resolution {
            get;
            set;
        }
        @AuraEnabled
        public String closureSummary{
            get;
            set;
        }
        //CUSTSA-28209: additional attrributes on work order page
        @AuraEnabled
        public String workOrderDetailsJSON{
            get;
            set;
        }
        //End of CUSTSA-28209: additional attrributes on work order page
        @AuraEnabled
        public Boolean showFirstTab{
            get;
            set;
        }
        @AuraEnabled
        public Boolean showSecondTab{
            get;
            set;
        }
        @AuraEnabled
        public Boolean showThirdTab{
            get;
            set;
        }
        @AuraEnabled
        public Boolean showFourthTab{
            get;
            set;
        }
        
    }
    
    //Wrapper class to hold Columns with headers
    public class Wrapper_DataTableColumns {
        
        @AuraEnabled
        public String fieldName {
            get;
            set;
        }
        
        @AuraEnabled
        public String label {
            get;
            set;
        }
        
        
        @AuraEnabled
        public Boolean sortable {
            get;
            set;
        }
        
        @AuraEnabled
        public String type {
            get;
            set;
        }
        
        @AuraEnabled
        public String url {
            get;
            set;
        }
        
        public Wrapper_DataTableColumns(String fieldName, String label, String dataType, Boolean sortable) {
            this.fieldName = fieldName;
            this.label = label;
            this.type = dataType;
            this.sortable = sortable;
        }
    }
    
    // Wrapper class for Tech Notes Table Data
    public class Wrapper_TechNoes_Data {
        @AuraEnabled 
        public String createdate {
            get;
            set;
        }
        @AuraEnabled 
        public String logtype {
            get;
            set;
        }
        @AuraEnabled 
        public String createdFrom {
            get;
            set;
        }
        @AuraEnabled 
        public String summary {
            get;
            set;
        }
        
        @AuraEnabled 
        public String longSummary {
            get;
            set;
        }
        
        
    }
    
    // Wrapper class for Tech Notes Table
    public class Wrapper_TechNotes {
        @AuraEnabled
        public List <Wrapper_DataTableColumns> lstColumns {
            get;
            set;
        }
        
        @AuraEnabled
        public Integer totalRecords {
            get;
            set;
        }
        
        @AuraEnabled
        public List <Wrapper_TechNoes_Data> lstSobjectData {
            get;
            set;
        }
        
        @AuraEnabled
        public String priId {get;set;}
        
        @AuraEnabled
        public String locId {get;set;}
        
        @AuraEnabled
        public Boolean errorOccured {get;set;}
        
        @AuraEnabled
        public Object deserializedObject {get;set;}
    }
    
    // Wrapper class for Test Data
    public class Wrapper_Test_Data {
        @AuraEnabled 
        public String createdate {
            get;
            set;
        }
        @AuraEnabled 
        public String testid {
            get;
            set;
        }
        @AuraEnabled 
        public String testname {
            get;
            set;
        }
        @AuraEnabled 
        public String testoutcome {
            get;
            set;
        }
        @AuraEnabled 
        public String testresult {
            get;
            set;
        }
        
        
    }
    
    // Wrapper class Work History Callout
    public class Wrapper_WorkHistoryResponse{
        public string response;
        public string errorCode;
        public string errorDetails;
        public Integer totalRecords;
        public NBNIntegrationCache__c cacheRec;
    }
    
    // Wrapper class Work Order Callout
    public class Wrapper_WorkOrderResponse{
        public string response;
        public string errorCode;
        public string errorDetails;
    }
    
    // Wrapper class for Test Data
    public class Wrapper_TestDetails {
        @AuraEnabled
        public List <Wrapper_DataTableColumns> lstColumns {
            get;
            set;
        }
        
        @AuraEnabled
        public Integer totalRecords {
            get;
            set;
        }
        
        @AuraEnabled
        public List <Wrapper_Test_Data> lstSobjectData {
            get;
            set;
        }
        
        @AuraEnabled
        public String priId {get;set;}
        
        @AuraEnabled
        public String locId {get;set;}
        
        @AuraEnabled
        public Boolean errorOccured {get;set;}
        
        @AuraEnabled
        public Object deserializedObject {get;set;}
    }
    
    
    // Work Order Callout
    public static Wrapper_WorkOrderResponse getData_WorkOrder(String workOrderNumber){
        Wrapper_WorkOrderResponse obj = new Wrapper_WorkOrderResponse();
        
        // Set Sample JSON for testing purpose.
        //obj.response = JIGSAW_WorkOrderModuleController_Test.jsonStr;
        //return obj;
        
        
        // Check Cache Object.
        String jsonResponse = getJSONFromCache('WorkOrder', workOrderNumber, null, '');
        if(String.isBlank(jsonResponse) == false){
            obj.response = jsonResponse;
            return obj;
        }
        
        // Do callout.
        try{
            HTTPResponse res = integrationUtility.getWorkOrderRequest('NBNCO', workOrderNumber, 'WorkOrder');
            System.debug('@@@' + res);
            if(res != null){
                System.debug('@@@' + res.getStatusCode());
                System.debug('@@@' + res.getBody());
                
                if(res.getStatusCode() == 200){
                    String s = res.getBody().replaceAll('\n','').replaceAll('\r','');
                    obj.response = s;
                    obj.errorCode = String.valueOf(res.getStatusCode());
                    // fill cache here.
                    // Commented as per new logic to not to save cache for work order callout
                    //setJSONFromCache('WorkOrder', workOrderNumber, null, null, s);
                    
                }else{
                    obj.errorCode = String.valueOf(res.getStatusCode());
                    obj.errorDetails = res.getStatus();
                }
            }
            return obj;
        }
        catch(exception ex){
            System.debug('@@@' + ex.getMessage());
            obj.errorDetails = ex.getMessage();
            return obj;
        }
        
    }
    
    // Work History callout.
    public static Wrapper_WorkHistoryResponse getData_WorkHistory(String incidentId, String dayCount, String locId){
        Wrapper_WorkHistoryResponse objResponse = new Wrapper_WorkHistoryResponse();
        
        // Query Location Id and Reported Date
        
        List <Incident_Management__c> incList = [SELECT Id, Incident_Number__c, locId__c, Reported_Date__c, Technology__c FROM Incident_Management__c WHERE Id = :incidentId];
        System.debug('@@@' + incList);
        String locationId;
        if(string.isBlank(locId)){
            locationId = incList.get(0).locId__c;
        }else{
            locationId  = locId;
        }
        if(string.isNotBlank(incList.get(0).Technology__c))
        {
            incidentTechnology = incList.get(0).Technology__c;
        }
        String reportedDateFormattted;
        try{
            //Date reportedDate = Date.valueOf(incList.get(0).Reported_Date__c);
            //Changes done for the Prod Defect MRTEST-3502
            DateTime reportedDate = system.today();
            reportedDateFormattted = reportedDate.format('yyyy-MM-dd');
            //reportedDateFormattted = incList.get(0).Reported_Date__c.format('yyyy-MM-dd');
        }
        catch(exception ex){
            System.debug('@@@' + ex.getMessage());
            objResponse.errorCode = '1000';
            objResponse.errorDetails = 'Reported Date is null or invalid.';
            objResponse.totalRecords = -1;
            return objResponse;
        }
        
        
        
        // Sample JSON response for testing
        //response_WorkHistory = JIGSAW_WorKOrderDetailsWrapper.jsonString;
        
        // Check Cache Object.
        // String jsonResponse = getJSONFromCache('WorkHistory', null, reportedDate, locationId);
        String jsonResponse = getJSONFromCache('WorkHistory', null, reportedDateFormattted,locationId); // 'LOC000044048226' );// 'LOC000174611481');
        
        if(String.isBlank(jsonResponse) == false){
            objResponse.response = jsonResponse;
            objResponse.errorCode = '200';
            return objResponse;
        }
        
        // Do callout
        try{
            
            HTTPResponse res;
            
            res = integrationUtility.getWorkOrderHistoryRequest(locationId, reportedDateFormattted, dayCount,'WorkOrderHistory');            
            
            System.debug('@@@Gaurav' + res);
            if(res != null){
                System.debug('@@@Code' + res.getStatusCode());
                System.debug('@@@Body' + res.getBody());
                
                if(res.getStatusCode() == 200){
                    String s = res.getBody().replaceAll('\n','').replaceAll('\r','');
                    objResponse.response = s;
                    objResponse.errorCode = String.valueOf(res.getStatusCode());
                    objResponse.errorDetails = res.getStatus();
                    // fill cache here.
                    NBNIntegrationCache__c cacheObj = setJSONFromCache('WorkHistory', null, reportedDateFormattted, locationId, s);
                    System.debug('Work History Cache : '+cacheObj);
                    if(cacheObj != null){
                        objResponse.cacheRec = cacheObj;
                    }
                    
                }
                else{
                    objResponse.errorCode = String.valueOf(res.getStatusCode());
                    objResponse.errorDetails = res.getStatus();
                }
            }
            System.debug('@@@Response return' + objResponse);
            return objResponse;
        }
        catch(exception ex){
            System.debug('@@@' + ex.getMessage());
            objResponse.errorCode = '1000';
            objResponse.errorDetails = ex.getMessage();
            objResponse.totalRecords = -1;
            return objResponse;
        }
    }
    
    
    // Get JSON from Integration cache.
    public static String getJSONFromCache(String calloutType, String WONumber, String reportedDate, String locationId){
        String qry = 'SELECT Id, JSONPayload__c,StampTime_del1__c,CreatedDate  FROM NBNIntegrationCache__c WHERE IntegrationType__c = :calloutType';
        if(calloutType == 'WorkOrder'){
            qry += ' AND Work_Order_Number__c = :WONumber';
        }
        else if(calloutType == 'WorkHistory'){
            qry += ' AND Reported_Date__c  = :reportedDate AND locId__c = :locationId Order By CreatedDate DESC';
        }
        System.debug('@@@ query is-' + qry);
        
        List<NBNIntegrationCache__c> lstCache = Database.query(qry);
        system.debug('list of rec are -' + lstCache);
        List<NBNIntegrationCache__c> lstcachePick = new List<NBNIntegrationCache__c>();
        if(lstCache.size() > 0 && !lstCache.IsEmpty()){
            system.debug('list of rec are -' + lstCache);
            if(lstCache.size() > 1){
                Decimal stmpval = 0;
                Datetime dt = lstCache.get(0).CreatedDate;
                for(NBNIntegrationCache__c rec : lstCache ){
                    system.debug('date is --' + rec.CreatedDate );
                    if(dt != null && (dt.isSameDay(rec.CreatedDate))){
                        if(stmpval == 0 || stmpval < rec.StampTime_del1__c){
                            stmpval = rec.StampTime_del1__c ;
                            lstcachePick.clear();
                            lstcachePick.add(rec);
                        }
                    }
                    else{
                        dt = rec.CreatedDate;
                    }
                }
            }
            
            if(!lstcachePick.isEmpty() && lstcachePick.size() > 0){
                
                system.debug('pick list is - ' + lstcachePick);
                system.debug('pick list is - ' + lstcachePick.get(0).CreatedDate + 'time --' + lstcachePick.get(0).StampTime_del1__c);
                return lstcachePick.get(0).JSONPayload__c; 
            }
            else {
                system.debug('pick list original is - ' + lstCache.get(0));
                return lstCache.get(0).JSONPayload__c;  
                
            }
        }
        return null;
    }
    
    // Set Integration cache.
    public static NBNIntegrationCache__c setJSONFromCache(String calloutType, String WONumber, String reportedDate, String locationId, String response){
        try{
            response = response.replaceAll('\n','').replaceAll('\r','');
            system.debug('@@@@@response :' + response);
            NBNIntegrationCache__c objCache = new NBNIntegrationCache__c();
            objCache.JSONPayload__c = response;
            objCache.IntegrationType__c = calloutType;
            objCache.Work_Order_Number__c = WONumber;
            objCache.locId__c = locationId;
            objCache.StampTime_del1__c = Datetime.now().millisecond();
            objCache.Reported_Date__c = reportedDate;
            if(calloutType == 'WorkOrder'){
                insert objCache;
            }
            return objCache;
        }
        catch(Exception ex){
            System.debug('@@@' + ex.getMessage());
            // To do: remove uncommitted pending error.
            return null;
        }
    }    
}