/*------------------------------------------------------------
Author: Sunil Gupta August 22, 2018
Company: Appirio
Description: Test class for JIGSAW_WorkOrderModuleController
History
<Date>      <Author>     <Description>
------------------------------------------------------------*/
@isTest()
public class JIGSAW_WorkOrderModuleController_Test {
    
    // Test Data 
    @testSetup static void setup() {
        // create custom setting records for Network Trail
        List<SObject> lstToInsert = new List<SObject>();
        
        NBNIntegrationInputs__c objCS1 = new NBNIntegrationInputs__c();        
        objCS1.Name = 'WorkOrderHistory';
        objCS1.Call_Out_URL__c = 'callout:NBNAPPGATEWAY';
        objCS1.EndpointURL__c='testWorkOrderHistory';
        objCS1.AccessSeekerId__c = 'testId';
        objCS1.Call_TimeOut_In_MilliSeconds__c = '60000';
        objCS1.NBN_Environment_Override__c='SIT1';
        objCS1.nbn_Role__c='NBN';
        objCS1.Service_Name__c='test serviec';
        objCS1.Token__c='8e73b7e33b22ad58';
        objCS1.XNBNBusinessChannel__c='Salesforce';
        objCS1.UI_TimeOut__c = 6000;
        objCS1.NBN_Activity_Name__c = 'test';
        
        lstToInsert.add(objCS1);
        
        NBNIntegrationInputs__c objCS2 = new NBNIntegrationInputs__c();
        objCS2.NBN_Activity_Name__c = 'test';
        objCS2.Name = 'WorkOrder';
        objCS2.Call_Out_URL__c = 'callout:NBNAPPGATEWAY';
        objCS2.EndpointURL__c='testWorkOrder';
        objCS2.AccessSeekerId__c = 'testId';
        objCS2.Call_TimeOut_In_MilliSeconds__c = '60000';
        objCS2.NBN_Environment_Override__c='SIT1';
        objCS2.nbn_Role__c='NBN';
        objCS2.Service_Name__c='test serviec';
        objCS2.Token__c='8e73b7e33b22ad58';
        objCS2.XNBNBusinessChannel__c='Salesforce';
        objCS2.UI_TimeOut__c = 6000;
        lstToInsert.add(objCS2);
        
        
        Incident_Management__c objIM_1 = new Incident_Management__c();
        objIM_1.Appointment_Start_Date__c = System.now();
        objIM_1.Appointment_End_Date__c = System.now();
        objIM_1.Target_Commitment_Date__c = System.now();
        objIM_1.Appointment_Start_Date__c = System.now();
        objIM_1.Appointment_End_Date__c = System.now();
        objIM_1.Target_Commitment_Date__c = System.now();
        objIM_1.locId__c = 'LOC00001';
        objIM_1.Reported_Date__c = System.now();
        objIM_1.Network_Trail_Transaction_Id__c = 'test';
        lstToInsert.add(objIM_1);
        
        Incident_Management__c objIM_2 = new Incident_Management__c();
        objIM_2.Appointment_Start_Date__c = System.now();
        objIM_2.Appointment_End_Date__c = System.now();
        objIM_2.Target_Commitment_Date__c = System.now();
        objIM_2.Appointment_Start_Date__c = System.now();
        objIM_2.Appointment_End_Date__c = System.now();
        objIM_2.Target_Commitment_Date__c = System.now();
        objIM_2.locId__c = 'locationNotFound';
        objIM_2.Reported_Date__c = System.now().addDays(-10);
        objIM_2.Network_Trail_Transaction_Id__c = 'test';
        lstToInsert.add(objIM_2);
        
        
        // Work History API wokrs on basis of Location Id and  Reported Date.
        NBNIntegrationCache__c inCache = new NBNIntegrationCache__c();
        inCache.JSONPayload__c = sampleJSON_WorkHistory_1();
        inCache.IntegrationType__c = 'WorkHistory';
        inCache.locId__c = 'LOC00001';
        inCache.Reported_Date__c = System.now().format('yyyy-MM-dd');
        lstToInsert.add(inCache);
        
        
        // Work Order API wokrs on basis of work order number only.
        NBNIntegrationCache__c inCache1 = new NBNIntegrationCache__c();
        inCache1.JSONPayload__c = sampleJSON_WorkOrder_1();
        inCache1.IntegrationType__c = 'WorkOrder';
        inCache1.Work_Order_Number__c = 'WOR100000716237';
        lstToInsert.add(inCache1);
        
        insert lstToInsert;
        
    }
     
    
    
    
    
    // fetch_FirstTab coverage for callout code cache should not found
    @isTest static void testMethod1() {
        Test.startTest();
        String workOrderNumber = 'workOrderNotFound';
        String dayCount = '60';
        String workRequestNumber = 'WRQ1';
        
        Incident_Management__c inc = [SELECT Id, locId__c, Reported_Date__c, Name FROM Incident_Management__c 
                                    where locId__c = 'locationNotFound'].get(0);
        
        
        // Set Mock
        MultiStaticResourceCalloutMock mock = new MultiStaticResourceCalloutMock();
        mock.setStaticResource('testWorkOrderHistory', 'JIGSAW_WorkHistoryJSON');
        mock.setStaticResource('testWorkOrder', 'JIGSAW_WorkOrderJSON');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        
        
        JIGSAW_WorkOrderModuleController.Wrapper_FirstTab result = JIGSAW_WorkOrderModuleController.fetch_FirstTab(workOrderNumber, inc.Id, dayCount, workRequestNumber);
        
        
        Test.stopTest();
        
    }
    
    // fetch_FirstTab coverage for callout code cache should not found for Error Scenario
    @isTest static void testMethod1_1() {
        Test.startTest();
        String workOrderNumber = 'workOrderNotFound';
        String dayCount = '60';
        String workRequestNumber = 'workRequestNotFound';
        
        Incident_Management__c inc = [SELECT Id, locId__c, Reported_Date__c, Name FROM Incident_Management__c 
                                    where locId__c = 'locationNotFound'].get(0);
        
        
        // Set Mock
        MultiStaticResourceCalloutMock mock = new MultiStaticResourceCalloutMock();
        mock.setStaticResource('testWorkOrderHistory', 'JIGSAW_WorkHistoryJSON');
        mock.setStaticResource('testWorkOrder', 'JIGSAW_WorkOrderJSON');
        mock.setStatusCode(400);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        
        
        JIGSAW_WorkOrderModuleController.Wrapper_FirstTab result = JIGSAW_WorkOrderModuleController.fetch_FirstTab(workOrderNumber, inc.Id, dayCount, workRequestNumber);
        
        
        Test.stopTest();
        
    }
    
    // fetch_FirstTab no callout, Cache found
    @isTest static void testMethod1_2() {
        Test.startTest();
        String workOrderNumber = 'WOR100000716237';
        String incidentId = [SELECT Id, Name FROM Incident_Management__c].get(0).Id;
        String dayCount = '60';
        String workRequestNumber = 'WRQ1';
        
        
        JIGSAW_WorkOrderModuleController.Wrapper_FirstTab result = JIGSAW_WorkOrderModuleController.fetch_FirstTab(workOrderNumber, incidentId, dayCount, workRequestNumber);
        
        
        Test.stopTest();
        
    }
    
    
    // fetch_TechNotes
    @isTest static void testMethod2() {
        Test.startTest();
        String workOrderNumber = 'WOR100000716237';
        String incidentId = [SELECT Id, Name FROM Incident_Management__c].get(0).Id;
        String dayCount = '60';
        String workRequestNumber = 'WRQ1';
        
        
        JIGSAW_WorkOrderModuleController.Wrapper_TechNotes result2 = JIGSAW_WorkOrderModuleController.fetch_TechNotes(workOrderNumber, incidentId);
        
         
        Test.stopTest();
        
    }
    
    
    // fetch_TestTab
    @isTest static void testMethod3() {
        Test.startTest();
        String workOrderNumber = 'WOR100000716237';
        String incidentId = [SELECT Id, Name FROM Incident_Management__c].get(0).Id;
        String dayCount = '60';
        String workRequestNumber = 'WRQ1';
        
        
        JIGSAW_WorkOrderModuleController.Wrapper_TestDetails result3 = JIGSAW_WorkOrderModuleController.fetch_TestTab(workOrderNumber, incidentId);
        
         
        Test.stopTest();
        
    }
    
    
    // fetch_FaultDetails
    @isTest static void testMethod4() {
        Test.startTest();
        String workOrderNumber = 'WOR100000716237';
        String incidentId = [SELECT Id, Name FROM Incident_Management__c].get(0).Id;
        String dayCount = '60';
        String workRequestNumber = 'WRQ1';
        
        
        WorkOrder result3 = JIGSAW_WorkOrderModuleController.fetch_FaultDetails(workOrderNumber, incidentId, workRequestNumber);
        
         
        Test.stopTest();
        
    }
    
    
    static String sampleJSON_WorkHistory_1(){
        StaticResource sr = [SELECT Id, Body FROM StaticResource 
                                            WHERE Name = 'JIGSAW_WorkHistoryJSON' LIMIT 1];
        String s = sr.Body.toString();
        s = s.replaceAll('\n','').replaceAll('\r','');
        return s;
    }
    
    
    static String sampleJSON_WorkOrder_1(){
        StaticResource sr = [SELECT Id, Body FROM StaticResource 
                                            WHERE Name = 'JIGSAW_WorkOrderJSON' LIMIT 1];
        String s = sr.Body.toString();
        s = s.replaceAll('\n','').replaceAll('\r','');
        return s;
    }
}