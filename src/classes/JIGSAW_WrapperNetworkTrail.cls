@isTest
public class JIGSAW_WrapperNetworkTrail{
    public String transactionId;	//8d2ddac89d5cd930
	public String status;	//Completed
	public cls_metadata metadata;
	public cls_tco tco;
	public cls_mdf mdf;
	public cls_copperLeadinCable copperLeadinCable;
	public cls_copperDistributionCables[] copperDistributionCables;
	public cls_pillars[] pillars;
	public cls_joint joint;
	public cls_fttnNode fttnNode;
	public cls_copperMainCable copperMainCable; 
	public cls_exception_x exception_x;
    
    public class cls_metadata {
		public String nbnCorrelationId;	//0e8cedd0-ad98-11e6-bf2e-47644ada7c0f
		public String tokenId;	//3c08d723a26f1234
		public String copperPathId;	//CPI300001701040
		public String accessServiceTechnologyType;	//Fibre To The Node
		public String incidentId;	//INC000000012
    }
    
    
    class cls_tco {
		public String id;	//148689111451627491
		public String name;	//
		public String type;	//Premise Equipment
		public String subtype;	//TCO
		public String status;	//In Service
		public String platformReferenceId;	//1291492788
		public String locationId;	//
		public String locationLatitude;	//
		public String locationLongitude;	//
    }
    
    class cls_mdf {
		public String id;	//E0001
		public String name;	//P17:194
		public String type;	//Joint
		public String subtype;	//TAP
		public String status;	//In Service
		public String platformReferenceId;	//781274058
		public String locationId;	//LOC000012345679
		public String locationLatitude;	//-37.6854
		public String locationLongitude;	//144.565
		public String portId;	//157
		public String portName;	//P17:194
		public String portPlatformReferenceId;	//893746589
		public String portStatus;	//Designed
		public String portInstallationStatus;	//Not Installed
    }
    
    class cls_copperLeadinCable {
		public String id;	//182219421351754719
		public String name;	//4TBA0111DPU001:4-4|182219421351754719:2-2
		public String type;	//Cable
		public String subtype;	//CLS
		public String status;	//In Service
		public String platformReferenceId;	//1291492811
		public String length;	//35.16
		public String lengthType;	//
		public String installationType;	//Underground
		public String twistedPairId;	//1
		public String twistedPairName;	//4TBA0111DPU001:4
		public String twistedPairPlatformReferenceId;	//1291492813
		public String twistedPairStatus;	//In Service
		public String twistedPairInstallationStatus;	//Installed
    }
    
    
    class cls_copperDistributionCables {
		public String id;	//4TBA-01-11-CDS-004
		public String name;	//4TBA0111CCU001:4-4
		public String type;	//Cable
		public String subtype;	//CDS
		public String status;	//In Service
		public String platformReferenceId;	//1291492821
		public String length;	//69
		public String lengthType;	//
		public String twistedPairCapacity;	//
		public String twistedPairId;	//1
		public String twistedPairName;	//4TBA0111CCU001:4
		public String twistedPairPlatformReferenceId;	//1291492823
		public String twistedPairStatus;	//In Service
		public String twistedPairInstallationStatus;	//Installed
    }
    
    class cls_pillars {
		public String id;	//101559892156683127
		public String name;	//4TBA-01-11-CCU-001
		public String type;	//Pillar
		public String subtype;	//CCU
		public String status;	//In Service
		public String platformReferenceId;	//1291491672
		public String distributionPairCapacity;	//900
		public cls_terminationModules[] terminationModules;
    }
    
    class cls_terminationModules {
		public String id;	//101559892156683127_O
		public String name;	//4TBA0111CCU001:1-1200
		public String type;	//Termination Module
		public String subtype;	//O Pair
		public String status;	//In Service
		public String platformReferenceId;	//1291491676
		public Integer twistedPairId;	//4
		public String twistedPairName;	//4TBA0111CCU001:4
		public String twistedPairPlatformReferenceId;	//713844058
		public String twistedPairStatus;	//In Service
		public String twistedPairInstallationStatus;	//Installed
    }
    
    class cls_joint {
		public String id;	//000000015032224032
		public String type;	//Joint
		public String subtype;	//CJL
		public String status;	//In Service
		public String platformReferenceId;	//1446074012
		public String locationLatitude;	//-38.02440142
		public String locationLongitude;	//145.2447201
		public String supportStructureType;	//Pit
    }
    
    class cls_fttnNode {
		public String id;	//6CAN-08-04-FNO-001
		public String name;	//
		public String type;	//FTTN Node
		public String status;	//In Service
		public String platformReferenceId;	//727426051
		public cls_dslamNetworkResource dslamNetworkResource;
    }
    
    class cls_dslamNetworkResource {
		public String id;	//3LYN-24-12-DSL-0001
		public String name;	//SWDSL0217261
		public String type;	//Chassis
		public String subtype;	//DSL
		public String status;	//In Service
		public String platformReferenceId;	//1401059993
    }
    
    class cls_copperMainCable {
		public String id;	//M0002
		public String name;	//EXCH01 M2:1201-1350
		public String type;	//Cable
		public String subtype;	//CMS
		public String status;	//In Service
		public String platformReferenceId;	//713844079
		public String length;	//
		public String lengthType;	//
		public String twistedPairId;	//56
		public String twistedPairName;	//EXCH01 M2:1256
		public String twistedPairPlatformReferenceId;	//451604097
		public String twistedPairStatus;	//In Service
		public String twistedPairInstallationStatus;	//Installed
    }
    
    public class cls_exception_x {
		public String code;	//PNI-Test-0001
		public String message;	//This is a custom message from sunil.
    }
    
    
    public static JIGSAW_WrapperNetworkTrail parseForTestCoverage(String json){
		return (JIGSAW_WrapperNetworkTrail) System.JSON.deserialize(json, JIGSAW_WrapperNetworkTrail.class);
	}

	static testMethod void testParse() {
		String json=		'{'+
		'	"transactionId": "33333",'+
		'	"status": "Completed",'+
		'	"metadata": {'+
		'		"nbnCorrelationId": "0e8cedd0-ad98-11e6-bf2e-47644ada7c0f",'+
		'		"tokenId": "3c08d723a26f1234",'+
		'		"copperPathId": "CPI300001701040",'+
		'		"accessServiceTechnologyType": "Fibre To The Node",'+
		'		"incidentId": "INC000000012"'+
		'	},'+
		'	"tco": {'+
		'		"id": "148689111451627491",'+
		'		"name": "",'+
		'		"type": "Premise Equipment",'+
		'		"subtype": "TCO",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "1291492788",'+
		'		"locationId": "",'+
		'		"locationLatitude": "",'+
		'		"locationLongitude": ""'+
		'	},'+
		'	"mdf": {'+
		'		"id": "E0001",'+
		'		"name": "P17:194",'+
		'		"type": "Joint",'+
		'		"subtype": "TAP",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "781274058",'+
		'		"locationId": "LOC000012345679",'+
		'		"locationLatitude": "-37.6854",'+
		'		"locationLongitude": "144.565",'+
		'		"portId": "157",'+
		'		"portName": "P17:194",'+
		'		"portPlatformReferenceId": "893746589",'+
		'		"portStatus": "Designed",'+
		'		"portInstallationStatus": "Not Installed"'+
		'	},'+
		'	"copperLeadinCable": {'+
		'		"id": "182219421351754719",'+
		'		"name": "4TBA0111DPU001:4-4|182219421351754719:2-2",'+
		'		"type": "Cable",'+
		'		"subtype": "CLS",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "1291492811",'+
		'		"length": "35.16",'+
		'		"lengthType": "",'+
		'		"installationType": "Underground",'+
		'		"twistedPairId": "1",'+
		'		"twistedPairName": "4TBA0111DPU001:4",'+
		'		"twistedPairPlatformReferenceId": "1291492813",'+
		'		"twistedPairStatus": "In Service",'+
		'		"twistedPairInstallationStatus": "Installed"'+
		'	},'+
		'	"copperDistributionCables": [{'+
		'		"id": "4TBA-01-11-CDS-004",'+
		'		"name": "4TBA0111CCU001:4-4",'+
		'		"type": "Cable",'+
		'		"subtype": "CDS",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "1291492821",'+
		'		"length": "69",'+
		'		"lengthType": "",'+
		'		"twistedPairCapacity": "",'+
		'		"twistedPairId": "1",'+
		'		"twistedPairName": "4TBA0111CCU001:4",'+
		'		"twistedPairPlatformReferenceId": "1291492823",'+
		'		"twistedPairStatus": "In Service",'+
		'		"twistedPairInstallationStatus": "Installed"'+
		'	}],'+
		'	"pillars": [{'+
		'		"id": "101559892156683127",'+
		'		"name": "4TBA-01-11-CCU-001",'+
		'		"type": "Pillar",'+
		'		"subtype": "CCU",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "1291491672",'+
		'		"distributionPairCapacity": "900",'+
		'		"terminationModules": [{'+
		'				"id": "101559892156683127_O",'+
		'				"name": "4TBA0111CCU001:1-1200",'+
		'				"type": "Termination Module",'+
		'				"subtype": "O Pair",'+
		'				"status": "In Service",'+
		'				"platformReferenceId": "1291491676",'+
		'				"twistedPairId": 4,'+
		'				"twistedPairName": "4TBA0111CCU001:4",'+
		'				"twistedPairPlatformReferenceId": "713844058",'+
		'				"twistedPairStatus": "In Service",'+
		'				"twistedPairInstallationStatus": "Installed"'+
		'			},'+
		'			{'+
		'				"id": "101559892156683127_C",'+
		'				"name": "4TBA0111CCU002:1-1200",'+
		'				"type": "Termination Module",'+
		'				"subtype": "C Pair",'+
		'				"status": "In Service",'+
		'				"platformReferenceId": "1291491678",'+
		'				"twistedPairId": 4,'+
		'				"twistedPairName": "4TBA0111CCU002:4",'+
		'				"twistedPairPlatformReferenceId": "713844059",'+
		'				"twistedPairStatus": "In Service",'+
		'				"twistedPairInstallationStatus": "Installed"'+
		'			}'+
		'		]'+
		'	}],'+
		'	"joint": {'+
		'		"id": "000000015032224032",'+
		'		"type": "Joint",'+
		'		"subtype": "CJL",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "1446074012",'+
		'		"locationLatitude": "-38.02440142",'+
		'		"locationLongitude": "145.2447201",'+
		'		"supportStructureType": "Pit"'+
		'	},'+
		'	"fttnNode": {'+
		'		"id": "6CAN-08-04-FNO-001",'+
		'		"name": "",'+
		'		"type": "FTTN Node",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "727426051",'+
		'		"dslamNetworkResource": {'+
		'			"id": "3LYN-24-12-DSL-0001",'+
		'			"name": "SWDSL0217261",'+
		'			"type": "Chassis",'+
		'			"subtype": "DSL",'+
		'			"status": "In Service",'+
		'			"platformReferenceId": "1401059993"'+
		'		}'+
		'	},'+
		'	"copperMainCable": {'+
		'		"id": "M0002",'+
		'		"name": "EXCH01 M2:1201-1350",'+
		'		"type": "Cable",'+
		'		"subtype": "CMS",'+
		'		"status": "In Service",'+
		'		"platformReferenceId": "713844079",'+
		'		"length": "",'+
		'		"lengthType": "",'+
		'		"twistedPairId": "56",'+
		'		"twistedPairName": "EXCH01 M2:1256",'+
		'		"twistedPairPlatformReferenceId": "451604097",'+
		'		"twistedPairStatus": "In Service",'+
		'		"twistedPairInstallationStatus": "Installed"'+
		'	}'+
		'}'+
		'';
		JIGSAW_WrapperNetworkTrail obj = parseForTestCoverage(json);
		System.assert(obj != null);
	}
}