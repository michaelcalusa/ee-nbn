@isTest
public class JIGSAW_WrapperNetworkTrailFTTB{
	public String transactionId;	//aeb8a1007f60271d
	public String status;	//Completed
	public cls_metadata metadata;
	public cls_fttbSite fttbSite;
    public cls_exception_x exception_x;
    
	public class cls_metadata {
		public String nbnCorrelationId;	//0e8cedd0-ad98-11e6-bf2e-47644ada7c0f
		public String tokenId;	//3c08d723a26f1234
		public String copperPathId;	//CPI300000403245
		public String accessServiceTechnologyType;	//Fibre To The Building
		public String incidentId;	//INC000000012
	}
	public class cls_fttbSite {
		public String id;	//3MDC-22-002-FBU-0001
		public String type;	//Site
		public String subtype;	//FTTB
		public String status;	//In Service
		public String platformReferenceId;	//1701959229_support
		public String locationLatitude;	//-37.9527
		public String locationLongitude;	//145.0809
		public cls_dslamNetworkResource dslamNetworkResource;
		public cls_mdfs[] mdfs;
	}
	public class cls_dslamNetworkResource {
		public String id;	//2PTH-05-104-DSL-0001
		public String name;	//SWDSL0206645
		public String type;	//Chassis
		public String subtype;	//DSL
		public String status;	//In Service
		public String platformReferenceId;	//896335613
	}
	public class cls_mdfs {
		public String id;	//3MDC-22-002-FRM-0005
		public String type;	//Distribution Frame
		public String subtype;	//BUILDING_MDF
		public String status;	//In Service
		public String platformReferenceId;	//1701959264
		public String chassisId;	//3MDC-22-002-CBL-0003
		public String chassisType;	//Chassis
		public String chassisSubtype;	//MDF_C
		public String chassisStatus;	//In Service
		public String chassisPlatformReferenceId;	//1701959287
		public String portId;	//12
		public String portName;	//TBD
		public String portPlatformReferenceId;	//1701959789
		public String portStatus;	//In Service
	}
    
    public class cls_exception_x {
		public String code;	//PNI-Test-0001
		public String message;	//This is a custom message from sunil.
    }
    
	public static JIGSAW_WrapperNetworkTrailFTTB parseForTestCoverage(String json){
		return (JIGSAW_WrapperNetworkTrailFTTB) System.JSON.deserialize(json, JIGSAW_WrapperNetworkTrailFTTB.class);
	}


	static testMethod void testParse() {
		String json=		'{'+
		'  "transactionId":"aeb8a1007f60271d",'+
		'  "status":"Completed",'+
		'  "metadata":{'+
		'    "nbnCorrelationId":"0e8cedd0-ad98-11e6-bf2e-47644ada7c0f",'+
		'    "tokenId":"3c08d723a26f1234",'+
		'    "copperPathId":"CPI300000403245",'+
		'    "accessServiceTechnologyType":"Fibre To The Building",'+
		'    "incidentId":"INC000000012"'+
		'  },'+
		'  "fttbSite": {'+
		'    "id": "3MDC-22-002-FBU-0001",'+
		'    "type": "Site",'+
		'    "subtype": "FTTB",'+
		'    "status": "In Service",'+
		'    "platformReferenceId": "1701959229_support",'+
		'    "locationLatitude": "-37.9527",'+
		'    "locationLongitude": "145.0809",'+
		'    "dslamNetworkResource": {'+
		'      "id": "2PTH-05-104-DSL-0001",'+
		'      "name": "SWDSL0206645",'+
		'      "type": "Chassis",'+
		'      "subtype": "DSL",'+
		'      "status": "In Service",'+
		'      "platformReferenceId": "896335613"'+
		'    },'+
		'    "mdfs": ['+
		'      {'+
		'        "id": "3MDC-22-002-FRM-0005",'+
		'        "type": "Distribution Frame",'+
		'        "subtype": "BUILDING_MDF",'+
		'        "status": "In Service",'+
		'        "platformReferenceId": "1701959264",'+
		'        "chassisId": "3MDC-22-002-CBL-0003",'+
		'        "chassisType": "Chassis",'+
		'        "chassisSubtype": "MDF_C",'+
		'        "chassisStatus": "In Service",'+
		'        "chassisPlatformReferenceId": "1701959287",'+
		'        "portId": "12",'+
		'        "portName": "TBD",'+
		'        "portPlatformReferenceId": "1701959789",'+
		'        "portStatus": "In Service"'+
		'      },'+
		'      {'+
		'        "id": "3MDC-22-002-FRM-0006",'+
		'        "type": "Distribution Frame",'+
		'        "subtype": "BUILDING_MDF",'+
		'        "status": "In Service",'+
		'        "platformReferenceId": "1701959265",'+
		'        "chassisId": "3MDC-22-002-XBL-0003",'+
		'        "chassisType": "Chassis",'+
		'        "chassisSubtype": "MDF_X",'+
		'        "chassisStatus": "In Service",'+
		'        "chassisPlatformReferenceId": "1701959289",'+
		'        "portId": "12",'+
		'        "portName": "TBD",'+
		'        "portPlatformReferenceId": "1701961050",'+
		'        "portStatus": "In Service"'+
		'      }'+
		'    ]'+
		'  }'+
		'}';
		JIGSAW_WrapperNetworkTrailFTTB obj = parseForTestCoverage(json);
		System.assert(obj != null);
	}
}