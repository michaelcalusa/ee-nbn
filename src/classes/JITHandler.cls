/***************************************************************************************************
    Class Name          :   JITHandler
    Author              :   Rupendra Kumar Vats / Sukumar Salla
    Version             :   1.0
    Created Date        :   24-July-2017
    Function            :   This class provides logic for inbound just-in-time provisioning of single sign-on community users in Salesforce organization.
    Input Parameters    :   SAML Request
    Output Parameters   :   Success/Failure Login

    Modification Log    :
    * Developer                   Date            Description
    * ----------------------------------------------------------------------------
    * Rupendra Kumar Vats       24-July-2017      Logic for inbound just-in-time provisioning
    * Sukumar Salla             10-Nov-2017       Added Logic to handle multiple EUAP Roles
    * Ganesh Sawant             05-Feb-2018       Added logic to add federationIdentifier into Username
    *                                             to allow multiple community users per contact centre
    *                                             Users using same email address (RSP Billing MSUE - 9167)
    * Ashwani Kaushik           01-June-2018      Added logic to handle Partner community profile based on
    *                                              Precendence defined in custom metadata.
    * Gobind Khurana           27-Jul-2018        Added code to check EE and NS roles for BSM Community

****************************************************************************************************/
global class JITHandler implements Auth.SamlJitHandler {
    static roleMappingWrapper rWrapper;
    static List<Contact> lstContacts = new List<Contact>();
    static map<string, Contact> mapContactDetails = new map<string, Contact>();
    static map<string, User> mapUserDetails= new map<string, User>();

    private class JitException extends Exception{}


    // This method process community user based on EUAP Role
    private void handleUser(boolean create, User u, Map<String, String> attributes, String federationIdentifier, boolean isStandard) {
        system.debug('---handleUser--' + attributes);
        system.debug('---handleUser-- create=' + create);
        system.debug('---handleUser-- isStandard=' + isStandard);
        system.debug('---handleUser-- User=' + u);

        String strFirstName;
        String strLastName;
        String strEmail;
        String strEUAPRole;
        String communityNickName;
        Boolean isUpdatePS = false;

        mapUserDetails.put(u.id, u);

        system.debug('** attributes **'+attributes);

        if(attributes.containsKey('User.FirstName')) {
            strFirstName = attributes.get('User.FirstName');
        }
        if(attributes.containsKey('User.LastName')) {
            strLastName = attributes.get('User.LastName');
        }
        if(attributes.containsKey('User.Email')) {
            strEmail = attributes.get('User.Email');
        }
        if(attributes.containsKey('User.ProfileId')) {
            strEUAPRole = attributes.get('User.ProfileId');
        }

        rWrapper = getRoleMapping(strEUAPRole);

        system.debug('***  rWrapper ***'+ rWrapper);

   	    system.debug('** rWrapper.PermissionSetNames **'+rWrapper.PermissionSetNames);

            list<BSM_DF_Permissions__mdt> BSMdfPermissions = new list<BSM_DF_Permissions__mdt>();

            BSMdfPermissions = [select Id, DF_Permissions__c from BSM_DF_Permissions__mdt];

            list<BSM_NS_Permissions__mdt> BSMNSPermissions = new list<BSM_NS_Permissions__mdt>();

            BSMNSPermissions = [select Id, NS_Permissions__c from BSM_NS_Permissions__mdt];

            list<BSM_NE_Permissions__mdt> BSMNEPermissions = new list<BSM_NE_Permissions__mdt>();

            BSMNEPermissions = [select Id, NE_Permissions__c from BSM_NE_Permissions__mdt];

        Boolean HasEECommunityPermissions=false;
        Boolean HasNSCommunityPermissions=false;
        Boolean HasNECommunityPermissions=false;


            for(string PermissionSet: rWrapper.PermissionSetNames)
            {
                for(BSM_DF_Permissions__mdt bsmdf: BSMdfPermissions)
                {
                    if(PermissionSet.contains(bsmdf.DF_Permissions__c))
                    {
                        HasEECommunityPermissions = true;
                    }
                }
                for(BSM_NS_Permissions__mdt bsmns: BSMNSPermissions) {
                    if (PermissionSet.contains(bsmns.NS_Permissions__c)) {
                        HasNSCommunityPermissions = true;
                    }
                }
                for(BSM_NE_Permissions__mdt bsmne: BSMNEPermissions) {
                    if (PermissionSet.contains(bsmne.NE_Permissions__c)) {
                        HasNECommunityPermissions = true;
                    }
                }
            }

        // If Community User does not exist then create it.
        if(create){

            if(attributes.containsKey('User.FederationIdentifier')) {
                u.FederationIdentifier = attributes.get('User.FederationIdentifier');
            } else {
                u.FederationIdentifier = federationIdentifier;
            }

            u.Email = strEmail;
            u.FirstName = strFirstName;
            u.LastName = strLastName;
            /* Username logic changed to include federationIdentifier so mulitple community Users
             * Can be created with same email address and unique Federation Identifier
             * MSEU - 9167
            */
            u.Username = strEmail +u.FederationIdentifier+ '.' + rWrapper.postfix;
            u.LocaleSidKey = 'en_AU';
            u.LanguageLocaleKey = 'en_US';
            u.TimeZoneSidKey = 'Australia/Sydney';
            u.EmailEncodingKey = 'ISO-8859-1';
            u.ProfileId = rWrapper.sfProfileId;
            u.Has_EE_Community_Permissions__c=HasEECommunityPermissions;
            u.Has_NS_Community_Permissions__c=HasNSCommunityPermissions;
            u.Has_NE_Community_Permissions__c=HasNECommunityPermissions;


            if(string.isNotEmpty(strLastName)) {
                String alias = '';

                if(string.isNotEmpty(strFirstName)) {
                    List<user> us = [select id, CommunityNickname from user where CommunityNickname=: strFirstName limit 1];
                    system.debug('****us ==>'+ us.size());
                    if(us.size()>0){
                        alias = strLastName.charAt(0) + strLastName;
                        communityNickName = strFirstName+' '+strLastName+u.FederationIdentifier;
                    }
                    else
                    {
                        alias = strFirstName;
                        communityNickName = strFirstName+u.FederationIdentifier;
                    }
                }
                else {
                    alias = strLastName.charAt(0) + strLastName;
                    communityNickName = strFirstName+' '+strLastName+u.FederationIdentifier;
                }
                //u.CommunityNickname = alias;

                if(alias.length() > 5) {
                    alias = alias.substring(0, 5);
                }
                u.Alias = alias;
                if(communityNickName.length()<=40)
                {
                    u.communityNickName=communityNickName;
                }
                else
                {
                    communityNickName.subString(0,40);
                }
            }

           try {
                   system.debug('>>> User to be inserted: ' + u);
                    insert u;
                    system.debug('--u--' + u);
                    notifyLicenseAvaliability(string.valueof(u.profileId));
              }
              catch (System.CalloutException e)
                {
                System.debug('Error while trying to insert an account: ' + e);
                }


        }
        //  If Community User exists then verify for any updates.
        else{
            Boolean isUpdate = false;

            system.debug('**u.Has_EE_Community_Permissions__c**'+u.Has_EE_Community_Permissions__c);
            system.debug('**HasEECommunityPermissions**'+HasEECommunityPermissions);

            if(u.Has_EE_Community_Permissions__c!=HasEECommunityPermissions)
            {
                isUpdate = true;
                u.Has_EE_Community_Permissions__c = HasEECommunityPermissions;
            }

            system.debug('**u.Has_NS_Community_Permissions__c**'+u.Has_NS_Community_Permissions__c);
            system.debug('**HasNSCommunityPermissions**'+HasNSCommunityPermissions);


            if(u.Has_NS_Community_Permissions__c!=HasNSCommunityPermissions)
            {
                isUpdate = true;
                u.Has_NS_Community_Permissions__c = HasNSCommunityPermissions;
            }

            system.debug('**u.Has_NE_Community_Permissions__c**'+u.Has_NE_Community_Permissions__c);
            system.debug('**HasNECommunityPermissions**'+HasNECommunityPermissions);

            if(u.Has_NE_Community_Permissions__c!=HasNECommunityPermissions)
            {
                isUpdate = true;
                u.Has_NE_Community_Permissions__c = HasNECommunityPermissions;
            }

            if(u.Email != strEmail){
                isUpdate = true;
                u.Email = strEmail;
            }

            if(u.FirstName != strFirstName){
                isUpdate = true;
                u.FirstName = strFirstName;
            }

            if(u.LastName != strLastName){
                isUpdate = true;
                u.LastName = strLastName;
            }

            if(rWrapper.sfProfileId != u.ProfileId){
                isUpdate = true;
                isUpdatePS = true;
                u.ProfileId = rWrapper.sfProfileId;
            }

            if(!u.Username.endsWithIgnoreCase('.' + rWrapper.postfix)){
                isUpdate = true;
                isUpdatePS = true;
                u.Username = strEmail + '.' + rWrapper.postfix;
            }

            /* Username logic changed to include federationIdentifier so mulitple community Users
             * Can be created with same email address and unique Federation Identifier
             * MSEU - 9167
            */
            if(!u.Username.containsIgnoreCase(federationIdentifier)){
                isUpdate = true;
                isUpdatePS = true;
                u.Username = strEmail +federationIdentifier+ '.' + rWrapper.postfix;
            }

            if((u.FirstName != strFirstName || u.LastName != strLastName) && string.isNotEmpty(strLastName)) {
                String alias = '';
                isUpdate = true;
                if(string.isNotEmpty(strFirstName)) {
                    alias = strFirstName;
                    u.CommunityNickname = strFirstName+federationIdentifier;
                } else {
                    alias = strLastName.charAt(0) + strLastName;
                    u.CommunityNickname = strFirstName + ' ' + strLastName+federationIdentifier;
                }

                u.CommunityNickname = alias+federationIdentifier;

                if(alias.length() > 5) {
                    alias = alias.substring(0, 5);
                }
                u.Alias = alias;
            }

            if(!u.IsActive){
                 isUpdate = true;
                isUpdatePS = true;
                u.IsActive=True;
            }

            /* CPST-6736 - update the locale to en_AU - to correct currency format */
            if (u.LocaleSidKey != 'en_AU') {
                u.LocaleSidKey = 'en_AU';
                isUpdate = true;
            }

            if(isUpdate){
                update u;
                system.debug('==== License test ===');
                 notifyLicenseAvaliability(string.valueof(u.profileId));
            }

            if(!isUpdatePS){
                isUpdatePS = verifyPermissionSet(u.Id, rWrapper.permissionSetIds);
            }
        }

        system.debug('** User to be inserted or updated **'+u);

        // After User creation / Update, assign the related permission sets
        if(create){
            system.debug('userPermissionSet()' + u.ID);
            userPermissionSet(false, u.ID, rWrapper.permissionSetIds);
        }
        else{
            system.debug('update userPermissionSet()' + u.ID + isUpdatePS);

            if(isUpdatePS){
                userPermissionSet(true, u.ID, rWrapper.permissionSetIds);
            }
        }

        system.debug('User.UserRoleId' + attributes.get('User.UserRoleId'));
		 //After user insert/update, modify the BSM public group to allow access to relavant records
        if(attributes.containsKey('User.UserRoleId') && String.isNotBlank(attributes.get('User.UserRoleId'))) {
            EE_DataSharingUtility.modifyBsmPublicGroup(attributes.get('User.UserRoleId'), u.Id);
        }
        system.debug('Return from handle User');
    }

    // This method process 'Customer Contact' Contact for Community Users
    private void handleContact(String accountId, User u, Map<String, String> attributes) {
        system.debug('---handleContact--');
        Contact c;
        boolean newContact = false;
        String strFirstName, strLastName, strEmail;

        if(attributes.containsKey('User.FirstName')) {
            strFirstName = attributes.get('User.FirstName');
        }

        if(attributes.containsKey('User.LastName')) {
            strLastName = attributes.get('User.LastName');
        }

        if(attributes.containsKey('User.Email')) {
            strEmail = attributes.get('User.Email');
        }

        system.debug('---strEmail--' + strEmail);
        system.debug('---accountId--' + accountId);
        lstContacts = [ SELECT Id, Email, FirstName, LastName, AccountID FROM Contact WHERE Email =: strEmail AND AccountID =: accountId AND RecordType.Name = 'Customer Contact' ];
        for(contact con: lstContacts)
        {
            mapContactDetails.put(con.id, con);
        }

        if(lstContacts.isEmpty()){
            if(u.ContactId == null){
                c = new Contact();
                c.Email = strEmail;
                c.AccountID = accountId;
                c.FirstName = strFirstName;
                c.LastName = strLastName;
                c.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
                insert c;
                system.debug('---contact--' + c);
                u.ContactId = c.Id;
            }
            else{
                List<Contact> lstExistingContact = [ SELECT Id, Email, FirstName, LastName, AccountID FROM Contact WHERE Id =: u.ContactId AND RecordType.Name = 'Customer Contact' ];
                lstExistingContact[0].Email = strEmail;
                lstExistingContact[0].AccountID = accountId;
                lstExistingContact[0].FirstName = strFirstName;
                lstExistingContact[0].LastName = strLastName;

                update lstExistingContact[0];
            }
        }
        else{
            Boolean isUpdate = false;
            c = lstContacts[0];
            // Update Contact calling future method (To avoid mixed dml error)

            if(u.ContactId != c.Id){
                u.ContactId = c.Id;
            }
           updateContactDetails(string.valueof(c.id),strEmail,strFirstName,strLastName, string.valueof(u.ContactId), isUpdate);


        }
    }

    // This method verifies 'Customer' Account for logged in Community User
    private String handleAccount(Map<String, String> attributes) {
        system.debug('---handleAccount--');
        Account acc;

        // Logic to FIND Customer Account
        if(attributes.containsKey('User.UserRoleId')) {
            String strAccessSeekerID = attributes.get('User.UserRoleId');
            List<Account> lstAcc = [ SELECT Id FROM Account WHERE Access_Seeker_ID__c =: strAccessSeekerID and RecordType.Name = 'Customer' ];

            if(lstAcc.isEmpty()){
                throw new JitException('Could not find account');
            }
            else{
                acc = lstAcc[0];
            }
        }

        return acc.Id;
    }

    @future(Callout=false)
    private static void updateContactDetails(string conId, string strEmail, string strFirstName, string strLastName, string userId, Boolean isUpdate ){
       system.debug('**** updateContactDetails ****');
       system.debug('**** mapContactDetails****'+ mapContactDetails);

       //Contact c = mapContactDetails.get(conId);
       Contact c =  [SELECT Id, Email, FirstName, LastName FROM Contact WHERE id =: conId];

       system.debug('**** contact to be updated ****'+ c + ' == ' + strFirstName + ' == ' +strLastName);
       if(c!=null)
       {
            if(c.Email != strEmail){
                isUpdate = true;
                c.Email = strEmail;
            }

            if(c.FirstName != strFirstName){
                isUpdate = true;
                c.FirstName = strFirstName;
            }

            if(c.LastName != strLastName){
                isUpdate = true;
                c.LastName = strLastName;
            }
       }
       if(isUpdate){
          system.debug('**** updating contact ****'+c);
           update c;
       }
    }

    // This method invokes Account, Contact and User methods
    private void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion) {
        system.debug('---handleJit--');
        String account = handleAccount(attributes);
        handleContact(account, u, attributes);
        handleUser(create, u, attributes, federationIdentifier, false);
    }

    // If Salesforce user does not exist then create Community user
    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion) {
        system.debug('---createUser--');
        User u = new User();
        handleJit(true, u, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        system.debug('--- End createUser--' + u );
        return u;
    }

    // If Salesforce user exists then create or update Community user
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion) {
        system.debug('---updateUser--');
        String strFID;
        User u = new User();
        // Get the User Federation ID to get Community user
        if(attributes.containsKey('User.FederationIdentifier')) {
            strFID = attributes.get('User.FederationIdentifier');
        } else {
            strFID = federationIdentifier;
        }

        // Logic to verify Community User existance with provided Federation ID
        List<User> lstUser = [SELECT    Id,
                                        IsActive,
                                        ContactId,
                                        Email,
                                        FirstName,
                                        LastName,
                                        ProfileId,
                                        Username,
                                        CommunityNickname,
                                        alias,
                                        Community_Permissions__c,
                                        Has_EE_Community_Permissions__c,
                                        Has_NS_Community_Permissions__c,
                                        Has_NE_Community_Permissions__c,
                                        LocaleSidKey
                                        FROM User WHERE FederationIdentifier =: strFID AND IsPortalEnabled = true];

        if(!lstUser.isEmpty()){
            u = lstUser[0];
            handleJit(false, u, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        }
        else{
            handleJit(true, u, samlSsoProviderId, communityId, portalId, federationIdentifier, attributes, assertion);
        }
    }

    // getRoleMapping fetch rolemapping information from Custom Metadata type and prepare wrapper
    public roleMappingWrapper getRoleMapping(string euapRole) {

      roleMappingWrapper lstRoleWrapper;
      List<String> euapRolesList;

      List<String> permissionsetNames=new List<String>();
      String permissionsetIds='';

      String ProfileName;
      String ProfileId;

      String communityName;
      String usernamePostfix;

       if(euapRole!='' && euapRole!=null){
           euapRolesList = euapRole.split(',');
           System.debug('----EUAP Roles---'+euapRolesList);
       }

       for(JITMapping__mdt jitMappingRec: [select id, EUAP_Role__c, Profile_Precedence__c,Salesforce_Profile__c, Salesforce_Permission_Set__c, Communiity__c, User_Name_Postfix__c from JITMapping__mdt where EUAP_Role__c in: euapRolesList AND isActive__c=true ORDER BY Profile_Precedence__c ASC]) {
        System.debug('----JIT Mapping---'+jitMappingRec);
        permissionsetNames.add(jitMappingRec.Salesforce_Permission_Set__c);
        ProfileName=jitMappingRec.Salesforce_Profile__c;
        communityName=jitMappingRec.Communiity__c;
        usernamePostfix=jitMappingRec.User_Name_Postfix__c;

       }

       for(PermissionSet ps:HelperUtility.getPermissionSetByName(permissionsetNames)) {
       permissionsetIds+=ps.id+'#';
       }
       for(Profile p:HelperUtility.getProfileByName(ProfileName)) {
       ProfileId=p.id;
       }


       lstRoleWrapper = new roleMappingWrapper(ProfileId, permissionsetIds,usernamePostfix,permissionsetNames);
       return lstRoleWrapper;
    }


    // Verify if the incoming EUAP role has the same Permission Sets or changed
    private boolean verifyPermissionSet(string uId, string strPermissionIds){
        Boolean isChanged = false;
        List<String> lstPS = strPermissionIds.split('#');
        List<String> lstPSQuery = new List<String>();

        List<PermissionSetAssignment> lstPSAssignment = [ SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId =: uId];
        for(PermissionSetAssignment ps : lstPSAssignment){
            lstPSQuery.add(ps.PermissionSetId);
        }

        lstPS.sort();
        lstPSQuery.sort();
        system.debug('--lstPS--' + lstPS + '--lstPSQuery--' + lstPSQuery);
        isChanged = lstPS.equals(lstPSQuery);
        if(isChanged){
            isChanged = false;
        }
        else{
            isChanged = true;
        }
        return isChanged;
    }

    @future(Callout=false)
    private static void userPermissionSet(boolean isPSUpdated, string uId, string strPermissionIds){
        system.debug('===strPermissionIds==='+strPermissionIds);

        // Process if only need to assign Permission Sets
        if(!isPSUpdated){
            assignPermissionSet(uId, strPermissionIds);
        }
        // Process if need to remove already assignedd and then assign new Permission Sets
        else{
            // Delete the existing Permission Set first
            system.debug('===uid==='+uId);


            List<JITMapping__mdt> lstJIT = [select Salesforce_Permission_Set__c from JITMapping__mdt ];
            Set<String> setRoles = new Set<String>();
            for(JITMapping__mdt jm : lstJIT){
                setRoles.add(jm.Salesforce_Permission_Set__c);
            }

            List<PermissionSetAssignment> lstPSDelete = new List<PermissionSetAssignment>();
            List<PermissionSetAssignment> lstPSAssignment = [SELECT Id, PermissionSetId, PermissionSet.Name FROM PermissionSetAssignment WHERE AssigneeId =: uId];
             system.debug('===lstPSAssignment ==='+lstPSAssignment);
            for(PermissionSetAssignment ps : lstPSAssignment){
                system.debug('===Role==='+ps.PermissionSet.Name);
                if(setRoles.contains(ps.PermissionSet.Name)){
                    lstPSDelete.add(ps);
                }
            }
            system.debug('===lstPSDelete==='+lstPSDelete);
            if(!lstPSDelete.isEmpty()){
                delete lstPSDelete;
            }
            // Assign the new Permission Sets
           assignPermissionSet(uId, strPermissionIds);
        }

    }

    static void assignPermissionSet(string uId, string strPermissionIds){
        if(uId != null){
            // permission set assignment logic
            List<string> lstPermissionSetIds = strPermissionIds.split('#');
            if(lstPermissionSetIds.size()>0)
            {
                List<PermissionSetAssignment> lstPermissionSetsToInsert = new List<PermissionSetAssignment>();
                for(string pid: lstPermissionSetIds)
                {
                    PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = pid, AssigneeId = uId);
                    lstPermissionSetsToInsert.add(psa);
                }
                insert lstPermissionSetsToInsert;
            }
        }
    }

    // Email Notification on License count
     @future(Callout=True)
     private static void notifyLicenseAvaliability(string profileRecordId){
        string query= 'Select';
        query += ' id, name from userlicense where id in (select UserLicenseId from profile where id=\''+ profileRecordId +'\')' ;
        userlicense ul =  Database.query(query);
        string licenseName;
        if(ul!=null){ licenseName = ul.name; }
        system.debug('== ul name =='+ ul.name);
        Organization orgDetails =[ select Id, Name, Country from Organization limit 1];
         Id orgId = orgDetails.Id;
         string orgName = orgDetails.Name;
         string orgCountry = orgDetails.Country;
         string result1;

         PageReference pr=new PageReference('/'+orgId);
         //called screenscraping: get the data from the page
        String rawData;
        if(Test.isRunningTest()){
            rawData = 'test content>System Administrator<td class=" dataCell  ">Active</td><td class=" dataCell  numericalColumn">';
            result1 = '10';
        }
        else{
            rawData=pr.getContent().toString();
        }
         //locate a particular element within the raw data
         //the info after this line contains the active license count
         String licRow = '>'+licenseName+'</th><td class=" dataCell  ">Active</td><td class=" dataCell  numericalColumn">';
         Integer licLen = licRow.length();
         Integer pos=rawData.indexOf(licRow);

         if (-1!=pos)
         {
            Integer licStart = pos + licLen;
            string result=rawData.substring(licStart, licStart+10);
            string findStr = '<';
            Integer pos1 = result.indexof(findStr);
            result1=result.substring(0, pos1);
             If(result1.contains(',')) {
               result1=result1.replace(',','');
             }
            System.debug('***************** SubString: ' + result1);
         }

         Decimal lic = decimal.valueOf(result1);
         integer u = [select count() from user where profile.UserLicense.Name =: licenseName and isactive = true];
         OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply@nbnco.com.au'];
         Decimal userCount = decimal.valueOf(u);
         Integer totalUnused = (lic.intValue() - userCount.intValue());
         System.debug('**Reamining Licenses** ' + String.valueOf(totalUnused));
         if(totalUnused<=integer.valueof(Label.JIT_LicenseThresholdCountToNotify))
         {
                System.debug('Sending Email...');
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                String[] toAdd = new String[]{Label.JIT_SystemAdminEmail};
                mail.setToAddresses(toAdd);
                mail.setReplyTo(Label.JIT_SystemAdminEmail);
                if ( owea.size() > 0 ) {mail.setOrgWideEmailAddressId(owea.get(0).Id);}
                mail.setSubject('License ALERT: Remaining'+ licenseName + ' count');
                mail.setBccSender(false);
                mail.setUseSignature(false);
                mail.setPlainTextBody('License Count: ' + String.valueOf(totalUnused));
                mail.setHtmlBody('License Availability Alert:<br/>' + 'The remaining '+ licenseName +  'count is: ' + String.valueOf(totalUnused));
                List<Messaging.SendEmailResult> results = Messaging.sendEmail(new Messaging.Email[] { mail });
                System.debug('****************** Email Sent: '+results.get(0).isSuccess());
         }
     }

    // Wrapper Class to store the EUAP Role Mapping values from Custom Metadata Type.
    public class roleMappingWrapper{

        public string sfProfileId {get; set;}
        public string permissionSetIds {get; set;}
        public string postfix {get; set;}
        public list<String> PermissionSetNames{get;set;}

        public roleMappingWrapper(string profileId,string permissionSetIds,String postfix, list<string> permissionsetNames){

            this.sfProfileId = profileId;
            this.permissionSetIds = permissionSetIds;
            this.postfix = postfix;
            this.PermissionSetNames = PermissionSetNames;
        }
    }
}