/*************************************************
- Developed by: Ashwani Kaushik
- Date Created: 16/11/2018 (dd/MM/yyyy)
- Description: This class contains test methods to test community user disable functionality.
- Version History:
- v1.0 - 16/11/2018, RS: Created
*/
@isTest
private class JITHandlerTriggerTest
{

public static Account acc, acc2;
    @testSetup
    static void setupTestData()
    {
        Test.StartTest();
        acc = new Account(recordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId(), Tier__c = '1', Name='test nbn', Access_Seeker_ID__c = 'AS00000011');
        insert acc;
        Map<String, String> mapAttributes = new Map<String, String>();
        mapAttributes.put('User.UserRoleId','AS00000011');
        mapAttributes.put('User.FederationIdentifier','testBSM_1');
        mapAttributes.put('User.LastName','nbnRSPTest_1');
        mapAttributes.put('User.FirstName','nbnRSPTest_1');
        mapAttributes.put('User.Email','testuser_1@nbnco.com.au.nbnRsp');
        mapAttributes.put('User.ProfileId','Enterprise Ethernet Plus,Access Seeker nbnSelect Feasibility');
        
        JITHandler handler = new JITHandler();
        //Create User
        
        User u = handler.createUser(null,null,null,'testSAML',mapAttributes,null);
    }
    @isTest
    static void testDisableUserEventPositiveTest()
    {
    User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRSPTest_1' LIMIT 1];
    List<JIT_Handler_Events__e> EventListToPublish=new List<JIT_Handler_Events__e>();
    JIT_Handler_Events__e e=new JIT_Handler_Events__e();
    e.userId__c=rspUser1.id;
    Test.startTest();
    EventListToPublish.add(e);
         if(EventListToPublish.size()>0){
            List<Database.SaveResult> results = EventBus.publish(eventListToPublish);
            for(Database.SaveResult sr : results) {
              if (sr.isSuccess()) {
              System.debug('Successfully published event.');
              } 
              else {
              for(Database.Error err : sr.getErrors()) {
              System.debug('Error returned: ' +  err.getStatusCode() +  ' - ' + err.getMessage());
              }
             }
            }
         }
    Test.stopTest();    
    }
 }