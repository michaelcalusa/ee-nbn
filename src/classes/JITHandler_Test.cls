/***************************************************************************************************
    Class Name          : JITHandler_Test
    Version             : 1.0 
    Author              : Rupendra Vats
    Created Date        : 25-July-2017
    Description         : Test Class for JITHandler

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats       25-July-2017      Test Class for JITHandler
    * Sukumar Salla       10-Nov-2017       Updates test class to handle the Multiple EUAP roles logic
****************************************************************************************************/ 
@isTest(SeeAllData = false)
private class JITHandler_Test {
    
    static testMethod void TestMethod_CreateUpdateCommunityUserA(){
        Account a = new Account(recordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId(), Tier__c = '1', Name='test nbn', Access_Seeker_ID__c = 'ASI000000000019');
        insert a;
        
        Map<String, String> mapAttributes = new Map<String, String>();
        mapAttributes.put('User.UserRoleId','ASI000000000019');
        mapAttributes.put('User.FederationIdentifier','testSAML');
        mapAttributes.put('User.LastName','Last Test');
        mapAttributes.put('User.FirstName','First Test');
        mapAttributes.put('User.Email','testuser@nbnco.com.au');
        mapAttributes.put('User.ProfileId','B2B Collaboration Portal');
        
        JITHandler handler = new JITHandler();
        //Create User
        User u = handler.createUser(null,null,null,'testSAML',mapAttributes,null);
        
        //Update User
        handler.updateUser(u.Id,null,null,null,'testSAML',mapAttributes,null);
    }
    
    static testMethod void TestMethod_CreateUpdateCommunityUserB(){
        Account a = new Account(recordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId(), Tier__c = '1', Name='test nbn', Access_Seeker_ID__c = 'ASI000000000019');
        insert a;
        
        Map<String, String> mapAttributes = new Map<String, String>();
        mapAttributes.put('User.UserRoleId','ASI000000000019');
        mapAttributes.put('User.FederationIdentifier','testSAML');
        mapAttributes.put('User.LastName','Last Test');
        mapAttributes.put('User.FirstName','First Test');
        mapAttributes.put('User.Email','testuser@nbnco.com.au');
        mapAttributes.put('User.ProfileId','B2B Collaboration Portal');
        
        JITHandler handler = new JITHandler();
        //Create User
        User u = handler.createUser(null,null,null,'testSAML',mapAttributes,null);
        
        Test.startTest();
        //Update User with Fields
        try{
            mapAttributes.put('User.ProfileId','Notifications Sharepoint');
            handler.updateUser(u.Id,null,null,null,'testSAML',mapAttributes,null);
        }Catch(Exception ex1){}
        Test.stopTest();
        
       
        
         try{
            mapAttributes.put('User.LastName','Last Test c');
            mapAttributes.put('User.FirstName','First Test');
            handler.updateUser(u.Id,null,null,null,'testSAML',mapAttributes,null);
        }Catch(Exception ex1){}

        try{
            mapAttributes.put('User.Email','testuserB@nbnco.com.au');
            handler.updateUser(u.Id,null,null,null,'testSAML',mapAttributes,null);
        }Catch(Exception ex2){}
    }

    static testMethod void can_Create_New_NE_User(){
        Account a =
                new Account(
                        recordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId(),
                        Tier__c = '1',
                        Name='test nbn',
                        Access_Seeker_ID__c = 'ASI000000000019' );
        insert a;

        Map<String, String> mapAttributes = new Map<String, String>();
        mapAttributes.put('User.UserRoleId','ASI000000000019');
        mapAttributes.put('User.FederationIdentifier','testSAML');
        mapAttributes.put('User.LastName','Last Test');
        mapAttributes.put('User.FirstName','First Test');
        mapAttributes.put('User.Email','testuser@nbnco.com.au');
        mapAttributes.put('User.ProfileId','Access Seeker Network Extension');

        JITHandler handler = new JITHandler();
        //Create User
        User u = handler.createUser(null,null,null,'testSAML',mapAttributes,null);

    }

    static testMethod void profileIdDoesNotExistInJITMapping(){
        Account a =
                new Account(
                        recordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId(),
                        Tier__c = '1',
                        Name='test nbn',
                        Access_Seeker_ID__c = 'ASI000000000019' );
        insert a;

        Map<String, String> mapAttributes = new Map<String, String>();
        mapAttributes.put('User.UserRoleId','ASI000000000019');
        mapAttributes.put('User.FederationIdentifier','testSAML');
        mapAttributes.put('User.LastName','Last Test');
        mapAttributes.put('User.FirstName','First Test');
        mapAttributes.put('User.Email','testuser@nbnco.com.au');
        mapAttributes.put('User.ProfileId','Missing Profile');

        JITHandler handler = new JITHandler();
        //Create User
        try {
            User u = handler.createUser(null, null, null, 'testSAML', mapAttributes, null);
        } catch (System.DmlException e) {}
    }


}