/****************************************************************************************************
Class Name:         Jigsaw_BandwidthProfileParser
Test Class:         Jigsaw_BandwidthProfileParser_Test
Class Type:         Helper Class 
Version:            1.0 
Created Date:       05 March 2019
Function:           This class helps to parse the Bandwidth profile value. 
Description:        Used by CS&A Jigsaw Incident Left Panel - Service Information section
Modification Log:
* Developer          Date             Description
* Syed Moosa Nazir	05 March 2019	  Epic: CUSTSA-23897-AC09. Story: CUSTSA-26090
* Syed Moosa Nazir	05-April-2019	  Updated - CUSTSA-30132 - created logic to parse the CIS bandwidthProfile dynamically
									  instead of referring the config table. Method: formatBandwidthProfileForToW
****************************************************************************************************/
public class Jigsaw_BandwidthProfileParser{
	// @@parseBandwidthProfile - returns the list of bandwidthProfileVariables class
    public static List<bandwidthProfileVariables> parseBandwidthProfile(string rawBandwidthProfile){
        if(string.isNotBlank(rawBandwidthProfile)){
            try{
                List<bandwidthProfileVariables> listOfBandwidthProfileVariables = new List<bandwidthProfileVariables>();
                string tcType;
                string upStreamVal;
                string downStreamVal;
				string bpsVal;
				string commitmentType;
                rawBandwidthProfile = rawBandwidthProfile.trim();
                List<String> listOfBandwidthProfile = rawBandwidthProfile.split(',');
                if(listOfBandwidthProfile <> null && !listOfBandwidthProfile.isEmpty()){
                    for(string individualBandwidthProfile :listOfBandwidthProfile){
                        tcType = null;
                        upStreamVal = null;
                        downStreamVal = null;
						bpsVal = null;
						commitmentType = null;
                        individualBandwidthProfile = individualBandwidthProfile.trim();
                        List<String> listOfBandwidthVariables = individualBandwidthProfile.split('_');
                        if(listOfBandwidthVariables <> null && !listOfBandwidthVariables.isEmpty()){
                            for(string bandwidthVariables :listOfBandwidthVariables){
                                bandwidthVariables = bandwidthVariables.trim();
                                if(bandwidthVariables.startsWith('D'))
                                    downStreamVal = bandwidthVariables.removeStart('D');
                                else if(bandwidthVariables.startsWith('U'))
                                    upStreamVal = bandwidthVariables.removeStart('U');
                                else if(bandwidthVariables.contains('TC'))
                                    tcType = bandwidthVariables;
								else if(bandwidthVariables.contains('bps'))
                                    bpsVal = bandwidthVariables; 
								else if(bandwidthVariables.equals('P') || bandwidthVariables.equals('C'))
                                    commitmentType = bandwidthVariables;
                            }
                            listOfBandwidthProfileVariables.add(new bandwidthProfileVariables(tcType, upStreamVal, downStreamVal, bpsVal, commitmentType));
                        }
                    }
                }
                return listOfBandwidthProfileVariables;
            }
            catch(Exception ex){
                system.debug('parseBandwidthProfile.rawBandwidthProfile=>'+rawBandwidthProfile+'==parseBandwidthProfile.Exception=>'+ex);
            }
        }
        return null;
    }
	// @formatBandwidthProfileDisplay - returns string which is displayed in service Info section in Jigsaw Incident UI
    public static string formatBandwidthProfileDisplay(string rawBandwidthProfile){
        string displayValue;
        List<bandwidthProfileVariables> listOfBandwidthProfileVariables = Jigsaw_BandwidthProfileParser.parseBandwidthProfile(rawBandwidthProfile);
        if(listOfBandwidthProfileVariables <> null && !listOfBandwidthProfileVariables.isEmpty()){
            for(bandwidthProfileVariables var :listOfBandwidthProfileVariables){
				if(string.isBlank(displayValue))
                    displayValue = var.tcType + ' ' + getUpperThresholdValue(var.downStreamVal) + '/' + getUpperThresholdValue(var.upStreamVal) + ' ' + var.unitType;
                else
                    displayValue = displayValue + ', ' + var.tcType + ' ' + getUpperThresholdValue(var.downStreamVal) + '/' + getUpperThresholdValue(var.upStreamVal) + ' ' + var.unitType;
            }
        }
        system.debug('rawBandwidthProfile=>'+rawBandwidthProfile+'==displayValue=>'+displayValue);
        return displayValue;
    }
	// @formatBandwidthProfileForToW - returns string which is sent in during ToW creation(Dispatch Tech operator action)
    public static List<string> formatBandwidthProfileForToW(string rawBandwidthProfile){
        List<string> formatedDsUsValue = new List<string> ();
        List<bandwidthProfileVariables> listOfBandwidthProfileVariables = Jigsaw_BandwidthProfileParser.parseBandwidthProfile(rawBandwidthProfile);
        if(listOfBandwidthProfileVariables <> null && !listOfBandwidthProfileVariables.isEmpty()){
			string upStreamFormatedValue;
			string downStreamFormatedValue;
            for(bandwidthProfileVariables var :listOfBandwidthProfileVariables){
				if(string.isBlank(upStreamFormatedValue))
					upStreamFormatedValue = 'U'+var.upStreamVal+'_'+var.unitType+'_'+var.tcType+'_'+var.commitmentType;
                else
                    upStreamFormatedValue = upStreamFormatedValue + ','+'U'+var.upStreamVal+'_'+var.unitType+'_'+var.tcType+'_'+var.commitmentType;
				if(string.isBlank(downStreamFormatedValue))
					downStreamFormatedValue = 'D'+var.downStreamVal+'_'+var.unitType+'_'+var.tcType+'_'+var.commitmentType;
                else
                    downStreamFormatedValue = downStreamFormatedValue + ','+'D'+var.downStreamVal+'_'+var.unitType+'_'+var.tcType+'_'+var.commitmentType;
            }
			formatedDsUsValue.add(downStreamFormatedValue);
			formatedDsUsValue.add(upStreamFormatedValue);
        }
        system.debug('rawBandwidthProfile=>'+rawBandwidthProfile+'==formatedDsUsValue=>'+formatedDsUsValue);
        return formatedDsUsValue;
    }
    public static string getUpperThresholdValue(string thresholdWidthRange){
		if(thresholdWidthRange.contains('-')){
			string UpperThresholdValue;
			List<String> bandwidthRangeList = thresholdWidthRange.split('-');
			if(bandwidthRangeList <> null && !bandwidthRangeList.isEmpty() && bandwidthRangeList.get(1) <> null){
				UpperThresholdValue = bandwidthRangeList.get(1);
				return UpperThresholdValue;
			}
		}
		return thresholdWidthRange;
	}
	public class bandwidthProfileVariables{
        public string tcType;
        public string upStreamVal;
        public string downStreamVal;
        public string unitType;
		public string commitmentType;
        public bandwidthProfileVariables(string tcTypeValue, string upStreamValue, string downStreamValue, string unitTypeValue, string commitmentTypeValue){
            tcType = tcTypeValue;
            upStreamVal = upStreamValue;
            downStreamVal = downStreamValue;
            unitType = unitTypeValue;
			commitmentType = commitmentTypeValue;
        }
    }
}