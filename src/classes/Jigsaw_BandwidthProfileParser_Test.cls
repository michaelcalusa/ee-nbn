@isTest()
public class Jigsaw_BandwidthProfileParser_Test {
    static testMethod void formatBandwidthProfileDisplay_Test() {
        test.startTest();
        system.assertEquals(Jigsaw_BandwidthProfileParser.formatBandwidthProfileDisplay('D12_U1_Mbps_TC4_P, D0.15_U0.15_Mbps_TC1_C'), 'TC4 12/1 Mbps, TC1 0.15/0.15 Mbps');
        list<string> expectedTcResultForTow = new list<string> ();
        expectedTcResultForTow.add('D100_Mbps_TC4_P,D0.15_Mbps_TC1_C');
        expectedTcResultForTow.add('U40_Mbps_TC4_P,U0.15_Mbps_TC1_C');
        //system.assertEquals(Jigsaw_BandwidthProfileParser.formatBandwidthProfileForToW('D25-100_U5-40_Mbps_TC4_P, D0.15_U0.15_Mbps_TC1_C'), expectedTcResultForTow);
        system.assertEquals(Jigsaw_BandwidthProfileParser.formatBandwidthProfileDisplay(null), null);
        test.stopTest();
    }
}