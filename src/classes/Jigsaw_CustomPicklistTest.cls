@isTest
private class Jigsaw_CustomPicklistTest{
    // Test Data 
    @testSetup static void setup() {
          
   	}
    
    // IncidentManagementCustomPicklist_LC
    @isTest static void testMethod1() {
        Test.startTest();
        IncidentManagementCustomPicklist_LC obj = new IncidentManagementCustomPicklist_LC();
        VisualEditor.DynamicPickListRows ls = obj.getValues();
        VisualEditor.DataRow dv = obj.getDefaultValue();
        Test.stopTest();
        
    }
    
    // ObjectDynamicCustomPicklist_LC
    @isTest static void testMethod2() {
        Test.startTest();
        ObjectDynamicCustomPicklist_LC obj = new ObjectDynamicCustomPicklist_LC();
        VisualEditor.DynamicPickListRows ls = obj.getValues();
        VisualEditor.DataRow dv = obj.getDefaultValue();
        Test.stopTest();
    }
    
    // ObjectQueueListDynamicPicklist_LC
    @isTest static void testMethod3() {
        LightningComponentConfigurations__c cs = new LightningComponentConfigurations__c();
        cs.Name = 'ObjectsListByQueue';
        cs.Component__c = 'ObjectsListByQueue';
        cs.Role_or_Profile_Based__c = 'Profile';
        cs.Role_or_Profile_Name__c = 'NBN Global Contact Centre User';
        insert cs;

        Test.startTest();
        ObjectQueueListDynamicPicklist_LC obj = new ObjectQueueListDynamicPicklist_LC();
        VisualEditor.DynamicPickListRows ls = obj.getValues();
        VisualEditor.DataRow dv = obj.getDefaultValue();
        Test.stopTest();
    }
}