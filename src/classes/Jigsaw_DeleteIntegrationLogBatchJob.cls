global class Jigsaw_DeleteIntegrationLogBatchJob implements Database.Batchable<sObject>,schedulable
{
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        Integer noOfDays = Integer.ValueOf(('-' + System.Label.ExhaustedLogDurationDays));
        Date last30DaysDate = system.Now().adddays(noOfDays).Date();
        String query = 'SELECT Id FROM RemedyToSalesforceIntegrationLog__c Where CreatedDate <= :last30DaysDate';                
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<RemedyToSalesforceIntegrationLog__c> scope) 
    {
        List<RemedyToSalesforceIntegrationLog__c> lstDeleteRemedyToSalesforcIntegrationLogs = new List<RemedyToSalesforceIntegrationLog__c>();
        for(RemedyToSalesforceIntegrationLog__c remedyIntegrationLogRecord : scope)        
        {            
            lstDeleteRemedyToSalesforcIntegrationLogs.Add(remedyIntegrationLogRecord);    
        }        
        Delete lstDeleteRemedyToSalesforcIntegrationLogs;
        Database.emptyRecycleBin(lstDeleteRemedyToSalesforcIntegrationLogs);
    }
    global void finish(Database.BatchableContext BC) {    }
    global void execute(System.SchedulableContext BatchContext) 
    {
        Jigsaw_DeleteIntegrationLogBatchJob deleteRemedyintegrationLogsJob = new Jigsaw_DeleteIntegrationLogBatchJob();
        database.executebatch(deleteRemedyintegrationLogsJob,200);
    }
}