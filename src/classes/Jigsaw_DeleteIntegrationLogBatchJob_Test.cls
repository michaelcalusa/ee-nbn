/***************************************************************************************************
Class Name:  Jigsaw_DeleteIntegrationLogBatchJob_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 10/10/2018
Function    : Test script used to cover Jigsaw_DeleteIntegrationLogBatchJob class
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
  Murali Krishna              10/10/2018             test class used to cover unit test of
                                                     Jigsaw_DeleteIntegrationLogBatchJob_Test class
****************************************************************************************************/
@isTest()
private class Jigsaw_DeleteIntegrationLogBatchJob_Test
{
    @testSetup static void loadTestDataForProcessRemedyToSalesforceInt() 
    {
        List<RemedyToSalesforceIntegrationLog__c> lstRemedyToSalesforceIntegrationLog = New List<RemedyToSalesforceIntegrationLog__c>();
        RemedyToSalesforceIntegrationLog__c remedyToSalesforceIntegrationlogRecord;
        Integer noOfDays = Integer.ValueOf(('-' + System.Label.ExhaustedLogDurationDays));
        DateTime last30DaysDatetime = system.Now().adddays(noOfDays).AddDays(-10); 
        System.Debug(last30DaysDatetime);
        for(Integer i = 0;i < 200; i++)
        {
            remedyToSalesforceIntegrationlogRecord = New RemedyToSalesforceIntegrationLog__c();
            remedyToSalesforceIntegrationlogRecord.Incident_Number__c = 'INC00' + i;
            remedyToSalesforceIntegrationlogRecord.ProcessedSuccessfully__c = true;
            lstRemedyToSalesforceIntegrationLog.Add(remedyToSalesforceIntegrationlogRecord);
        }
        if(lstRemedyToSalesforceIntegrationLog != Null && !lstRemedyToSalesforceIntegrationLog.isEmpty())
            Insert lstRemedyToSalesforceIntegrationLog;
        for(RemedyToSalesforceIntegrationLog__c remedyIntegrationLogRecord : lstRemedyToSalesforceIntegrationLog)
        {
            Test.setCreatedDate(remedyIntegrationLogRecord.Id,last30DaysDatetime);
        }
    }
    Static testMethod void testDeleteIntegrationLogBatchJob()
    {
        Test.StartTest();
            System.SchedulableContext integrationLogSchedulableContext;
            Jigsaw_DeleteIntegrationLogBatchJob deleteIntlogSchedulableContext = new Jigsaw_DeleteIntegrationLogBatchJob();
            deleteIntlogSchedulableContext.execute(integrationLogSchedulableContext);
        Test.StopTest();
    }
}