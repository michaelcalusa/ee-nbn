/*------------------------------------------------------------
Author: Lokesh Agarwal Sep 24, 2018
Company: IBM
Description: Test class for Jigsaw_WO_OperatorActionController
History
<Date>      <Author>     <Description>
------------------------------------------------------------*/
@isTest()
public class Jigsaw_WO_OperatorActionController_Test {

    // Test Data 
    @testSetup static void setup() {
        
        List<Jigsaw_PCR_Action_Filter__c> lstPCR = new List<Jigsaw_PCR_Action_Filter__c>();
        //Common_Resolution_Value__c,PCR_Code__c,With_Appointment__c,With_Commitment__c,WO_Sub_Type__c,WO_Type_FTTN_FTTB__c,WO_Version__c FROM Jigsaw_PCR_Action_Filter__c WHERE Common_Resolution_Value__c != null ';
        Jigsaw_PCR_Action_Filter__c objPCR = new Jigsaw_PCR_Action_Filter__c();
        objPCR.Common_Resolution_Value__c = 'Appointment required';
        objPCR.PCR_Code__c = 'PO4 / OTH / RO1';
        objPCR.With_Appointment__c = 'Y';
        objPCR.With_Commitment__c = 'Y';
        objPCR.WO_Type_FTTN_FTTB__c = 'FTTN';
        objPCR.WO_Version__c = '4.3.0';
        objPCR.Name = '222';
        objPCR.SR_No__c = 222;
        lstPCR.add(objPCR);
        
        objPCR = new Jigsaw_PCR_Action_Filter__c();
        objPCR.Common_Resolution_Value__c = 'End user not in attendance';
        objPCR.PCR_Code__c = 'PI7 / CI8 / RI5';
        objPCR.With_Appointment__c = 'Y';
        objPCR.With_Commitment__c = 'Y';
        objPCR.WO_Type_FTTN_FTTB__c = 'FTTB';
        objPCR.WO_Version__c = '2.3.0';
         objPCR.Name = '223';
        objPCR.SR_No__c = 223;
        lstPCR.add(objPCR);
     
        insert lstPCR;
        insert new Incident_Management__c (Name = 'INC000111212',Prod_Cat__c = 'NCAS-FTTN',Access_Seeker_ID__c = 'ACCESSSEEKERID',repeatIncidentPriority__c = 'RepeatIncidentPriority',PRI_ID__c = 'PRIID',AVC_Id__c ='AVC000028840963',Fault_Type__c = 'Performance latency', recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Incident Management FTTN/B').getRecordTypeId(), Appointment_Start_Date__c=system.now(),Appointment_End_Date__c=system.now(), Target_Commitment_Date__c=system.now(),Reported_Date__c=system.now());
        Incident_Management__c imD = [SELECT Id FROM Incident_Management__c where name = 'INC000111212' LIMIT 1];
        RecordType rdWO = [Select Id , Name from RecordType where DeveloperName = 'Work_Order'];
        WorkOrder wor = new WorkOrder();
        wor.Incident__c = imD.id;
        wor.RecordTypeId = rdWO.id;
        wor.wrqIdentifier__c = 'WOR100000200960';
        insert wor;
         
        NBNIntegrationInputs__c objCS1 = new NBNIntegrationInputs__c();
        objCS1.NBN_Activity_Name__c = 'test';
        objCS1.Name = 'CreateWOOffsiteIncomplete';
        objCS1.Call_Out_URL__c = 'callout:NBNAPPGATEWAY';
        objCS1.EndpointURL__c='testWorkOrder';
        objCS1.AccessSeekerId__c = 'testId';
        objCS1.Call_TimeOut_In_MilliSeconds__c = '60000';
        objCS1.NBN_Environment_Override__c='SIT1';
        objCS1.nbn_Role__c='NBN';
        objCS1.Service_Name__c='test serviec';
        objCS1.Token__c='8e73b7e33b22ad58';
        objCS1.XNBNBusinessChannel__c='Salesforce';
        objCS1.UI_TimeOut__c = 6000;
        objCS1.Header_Keys__c = 'nbn-correlation-id'; 
        insert objCS1;
         
        NBNIntegrationInputs__c objCS2 = new NBNIntegrationInputs__c();
        objCS2.NBN_Activity_Name__c = 'test';
        objCS2.Name = 'CreateWOOffsiteComplete';
        objCS2.Call_Out_URL__c = 'callout:NBNAPPGATEWAY';
        objCS2.EndpointURL__c='testWorkOrder';
        objCS2.AccessSeekerId__c = 'testId';
        objCS2.Call_TimeOut_In_MilliSeconds__c = '60000';
        objCS2.NBN_Environment_Override__c='SIT1';
        objCS2.nbn_Role__c='NBN';
        objCS2.Service_Name__c='test serviec';
        objCS2.Token__c='8e73b7e33b22ad58';
        objCS2.XNBNBusinessChannel__c='Salesforce';
        objCS2.UI_TimeOut__c = 6000;
        objCS2.Header_Keys__c = 'nbn-correlation-id'; 
        insert objCS2;
        
        NBNIntegrationInputs__c objCS3 = new NBNIntegrationInputs__c();
        objCS3.NBN_Activity_Name__c = 'test';
        objCS3.Name = 'WorkOrder';
        objCS3.Call_Out_URL__c = 'callout:NBNAPPGATEWAY';
        objCS3.EndpointURL__c='testWorkOrder';
        objCS3.AccessSeekerId__c = 'testId';
        objCS3.Call_TimeOut_In_MilliSeconds__c = '60000';
        objCS3.NBN_Environment_Override__c='SIT1';
        objCS3.nbn_Role__c='NBN';
        objCS3.Service_Name__c='test serviec';
        objCS3.Token__c='8e73b7e33b22ad58';
        objCS3.XNBNBusinessChannel__c='Salesforce';
        objCS3.UI_TimeOut__c = 6000;
        objCS3.Header_Keys__c = 'nbn-correlation-id'; 
        insert objCS3;
        
        
    }
    
    
    static testMethod void getWOPCRValuesTest(){
        Map<String,String> mapPCResvalues = new Map<String,String>();
        List<Jigsaw_PCR_Action_Filter__c> lstPCRValues = new List<Jigsaw_PCR_Action_Filter__c>();
        Test.StartTest();
        mapPCResvalues = incidentManagementController_LC.getResolutionDropdownValues('Appointment','FTTN','4.3.0');
        lstPCRValues = incidentManagementController_LC.getPCRecords('Appointment','FTTB','2.3.0');
        Test.StopTest();
    }
    
    static testMethod void getDetailWorkOrderValuesTestException(){
        
        
        Test.StartTest();
        // Set Mock
        MultiStaticResourceCalloutMock mock = new MultiStaticResourceCalloutMock();
       // mock.setStaticResource('testWorkOrder', 'JIGSAW_WorkHistoryJSON');
        mock.setStaticResource('testWorkOrder', 'JIGSAW_WorkOrderJSON');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);
        
        Incident_Management__c imD = [SELECT Id FROM Incident_Management__c where name = 'INC000111212' LIMIT 1]; //INC000111211   INC009129384756
        WorkOrder woR = [SELECT Id,wrqIdentifier__c FROM WorkOrder where Incident__c =: imD.Id LIMIT 1];
        JIGSAW_WorKOrderDetailsWrapper obj1 = new JIGSAW_WorKOrderDetailsWrapper();
        obj1 = incidentManagementController_LC.getDetailWorkOrderValues(woR.wrqIdentifier__c);

        Test.StopTest();
    }
    
    static testMethod void getHistoryWorkOrderValuesTestException(){
        NBNIntegrationCache__c nbnIntegrationCacheRecord = new NBNIntegrationCache__c();
        nbnIntegrationCacheRecord.IntegrationType__c = 'WorkOrder';
        nbnIntegrationCacheRecord.Transaction_Id__c = 'TRA000000000001';
        nbnIntegrationCacheRecord.JSONPayload__c =      '{'+
        '   "member": ['+
        '         {'+
        '           "class": "SR",'+
        '           "classstructureid": "MR1802-6735",'+
        '           "historyflag": false,'+
        '           "nbnaccessseeker": "ASI000000220282",'+
        '           "nbnaccesstech": "FTTB",'+
        '           "nbnappointmentid": "APT100000045501",'+
        '           "nbncategory": "SERVICE RESTORATION",'+
        '           "nbncopperpathid": "CPI000000000000",'+
        '           "nbndemandtype": "STANDARD INSTALL",'+
        '           "nbnlocationid": "LOC000009567829",'+
        '           "nbnreasoncode": "COMPLETE",'+
        '           "nbnreferenceid": "INC000002595553",'+
        '           "nbnregion": "C-CSA400000000028",'+
        '           "nbnregiontype": "URBAN",'+
        '           "nbnserviceclass": "32",'+
        '           "nbnslatime": "2018-05-22T23:59:00+1000",'+
        '           "nbnslotname": "08-12",'+
        '           "nbnworkforce": "DP1019",'+
        '           "orgid": "NBN CO",'+
        '           "reportdate": "2018-05-08T01:49:18+1000",'+
        '           "siteid": "NBNCO",'+
        '           "status": "INPRG",'+
        '           "ticketid": "WRQ100000022774",'+
        '           "nbnworkhistoryview": [{'+
        '               "classstructureid": "MR1802-6735",'+
        '               "historyflag": false,'+
        '               "nbnappend": "2018-05-15T12:00:00+1000",'+
        '               "nbnappointmentid": "APT100000045501",'+
        '               "nbnappstart": "2018-05-15T08:00:00+1000",'+
        '               "nbncopperpathid": "CPI000000000000",'+
        '               "nbnlocationid": "LOC000009567829",'+
        '               "nbnreasoncode": "FINISHED",'+
        '               "nbnrequirement": "Commitment",'+
        '               "nbnticketid": "WRQ100000022774",'+
        '               "nbntype": "AERIAL",'+
        '               "nbnworkforce": "DP1019",'+
        '               "orgid": "NBN CO",'+
        '               "parent": "WOR100000200956",'+
        '               "reportdate": "2018-05-08T01:49:28+1000",'+
        '               "siteid": "NBNCO",'+
        '               "status": "INPRG",'+
        '               "statusdate": "2018-05-08T01:50:35+1000",'+
        '               "woclass": "WORKORDER",'+
        '               "wonum": "WOR100000200960",'+
        '               "classstructure": [{'+
        '                   "classstructureid": "MR1802-6735"'+
        '               }]'+
        '           }]'+
            '       }';

        Test.StartTest();
		Incident_Management__c imD = [SELECT Id,Name FROM Incident_Management__c where name = 'INC000111212' LIMIT 1]; //INC000111211   INC009129384756
        WorkOrder woR = [SELECT Id,wrqIdentifier__c FROM WorkOrder where Incident__c =: imD.Id LIMIT 1];
        JIGSAW_WorkHistoryWrapper.cls_member obj1 = new JIGSAW_WorkHistoryWrapper.cls_member();
        obj1 = incidentManagementController_LC.getHistoryWorkOrderValues(imD.Id,'60',imD.Name,'offsiteTechnician');
    }
    
    
    static testMethod void testMethod_SaveWorkOrderRecord(){
        Test.startTest();
        incidentManagementController_LC.Wrapper_IncidentManagementResponse objWrapper = new incidentManagementController_LC.Wrapper_IncidentManagementResponse();
        Incident_Management__c imD = [SELECT Id, Name FROM Incident_Management__c where name = 'INC000111212' LIMIT 1]; //INC000111211   INC009129384756
        WorkOrder woR = [SELECT Id,wrqIdentifier__c FROM WorkOrder where Incident__c =: imD.Id LIMIT 1];
        
        String woJSON = '{"woNum": "test","woEngageType" : "test","woSpecVersion": "test","FTTN Service Assurance" : "test","woPCRCode" : "test","isFault" : "false","rootCause" : "test","WoActionType" : "test","wkServiceTestPoint" : "test","addInfo" : "test","closureSummaryText" : "test","incidentId" : "INC000111212","strProdCat" : "FTTN","strAVCId" : "test","strPRI" : "test","strAccessSeekerId" : "test","parent" : "test","WRequest" : "test","WOSpecID" : "test","netBoundaryPoint" : "test","netBoundaryPointDetails" : "' + 
             'test","locationId" :"test","WorkOrderApptID" : "test","selectedPCRCode" :"test","selectedPCRDesc" :"test"}' ;
        
        incidentManagementController_LC.Wrapper_WorkOrdervalues woWrapperValues =  (incidentManagementController_LC.Wrapper_WorkOrdervalues)JSON.deserialize(woJSON.replace('\n','').replace('\r',''), incidentManagementController_LC.Wrapper_WorkOrdervalues.class); 

            
            
        Jigsaw_WO_OperatorActionController.SaveWorkOrderRecord(woR.wrqIdentifier__c, 'Appointment', woWrapperValues);
        
        
        Jigsaw_WO_OperatorActionController.SaveWorkOrderRecordOnsiteTechnician('test', 'test', woWrapperValues);
        
        Test.StopTest();
    }
    
    
    

}