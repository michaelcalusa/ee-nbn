public class LASEReportController {
    
     public Transient List<LASEReportWrapper> LASEReportWrapperList {get;set;}
    
     public LASEReportController() {
         runLASEReport();        
     }
    
    public void runLASEReport(){         
        LASEReportWrapperList = new List<LASEReportWrapper>();
        set<string> setAccountIds = new set<string>();
        map<string, Account> mapAccounts = new map<string, Account>();
        map<string, List<Contact>> mapAccountLASEContacts = new map<string, list<contact>>();
        map<string, Land_Access_Requirements__c> mapAccountLandAccessRequirement = new map<string,Land_Access_Requirements__c>();
        map<string, map<string, List<Contact>>> mapAccountLandAccessRequirementsContact = new map<string, map<string, List<contact>>>();
        // Get Account Fields by accessing Field Set on Account Object
        List<Schema.FieldSetMember> lstAccountFieldSetFields = new List<Schema.FieldSetMember>();
        lstAccountFieldSetFields = HelperUtility.readFieldSet('FS_ACCOUNT_LASEREPORT_FIELDS', 'Account'); 
        // Get Contact Fields by accessing Field Set on Contact Object
        List<Schema.FieldSetMember> lstContactFieldSetFields = new List<Schema.FieldSetMember>();
        lstContactFieldSetFields = HelperUtility.readFieldSet('FS_CONTACT_LASEREPORT_FIELDS', 'Contact'); 
        // Get Land Access Requirements Fields by accessing Field Set on Land Access Requirements Object
        List<Schema.FieldSetMember> lstLARFieldSetFields = new List<Schema.FieldSetMember>();
        lstLARFieldSetFields = HelperUtility.readFieldSet('FS_LANDACCESSREQ_LASEREPORT_FIELDS', 'Land_Access_Requirements__c'); 
        // Get Institution RecordType Id on Account
         String recordId = GlobalCache.getRecordTypeId('Account', 'Institution'); //'01228000000yYAp'
        // Prepare dynamic query looping through FieldSet fields to retrieve Account records.
        String accQuery = 'SELECT ';
        for(Schema.FieldSetMember f : lstAccountFieldSetFields) {
            accQuery += f.getFieldPath() + ', ';
        }
        accQuery += 'id FROM Account where RecordTypeId=\''+ recordId +'\'' ;
        // Perform database query on Account Object
        List<Account> accountList =  Database.query(accQuery); 
        // Prepare Account Map.
        for(Account acc: accountList){
              setAccountIds.add(acc.id);
              mapAccounts.put(acc.id,acc);
        }
        
        // Prepare dynamic query looping through FieldSet fields to retrieve Contact records.        
        String conQuery = 'SELECT ';
        for(Schema.FieldSetMember f : lstContactFieldSetFields) {
            conQuery += f.getFieldPath() + ', ';
        }
        conQuery += 'id, AccountId FROM Contact where AccountId in: setAccountIds' ;
        conQuery += ' AND Contact_for_Business_Unit__c Includes (\'' + 'LASE' + '\')';
        //Perform database query on Contact Object
        List<Contact> contactList =  Database.query(conQuery);        
        // Prepare map of Account and related LASE Contacts
        for(Contact con: contactList){
            if(mapAccountLASEContacts.containskey(con.AccountId))
            {
                mapAccountLASEContacts.get(con.AccountId).add(con);
            }
            else
            {
                mapAccountLASEContacts.put(con.AccountId, new List<Contact>());
                mapAccountLASEContacts.get(con.AccountId).add(con);
            }
        }
        
        // Prepare dynamic query looping through FieldSet fields to retrieve Land Access Requirements records. 
        String larQuery = 'SELECT ';
        for(Schema.FieldSetMember f : lstLARFieldSetFields) {
            larQuery += f.getFieldPath() + ', ';
        }
        larQuery += 'id FROM Land_Access_Requirements__c where Account__c in: setAccountIds' ;
        List<Land_Access_Requirements__c> larList =  Database.query(larQuery);    
        
        // prepare map account and related Land access requirements record
        for(Land_Access_Requirements__c lar: larList){ 
            mapAccountLandAccessRequirement.put(lar.Account__c, lar);
        }
        
        // Prepare the map Account, Land access requirements and Contacts
        for(string accId: setAccountIds){
             list<contact> lstMapContacts = new list<contact>();
             string larId = (mapAccountLandAccessRequirement.containskey(accId) ? string.valueof(mapAccountLandAccessRequirement.get(accId).id) : 'NoLAR');
             string keyString = accId + '~' + larId;
             mapAccountLandAccessRequirementsContact.put(accId, new map<string, List<contact>>());
             lstMapContacts = (mapAccountLASEContacts.containskey(accId) ? mapAccountLASEContacts.get(accId) : new List<contact>());
             mapAccountLandAccessRequirementsContact.get(accId).put(keyString, new List<contact>());
             mapAccountLandAccessRequirementsContact.get(accId).get(keyString).addall(lstMapContacts);
        }         
        system.debug('**** mapAccountLandAccessRequirementsContact ==>'+mapAccountLandAccessRequirementsContact);
        // Prepare Wrapper to display the fields in display table.
        for(string accid: mapAccountLandAccessRequirementsContact.keyset())
        {
            Account accToAdd = mapAccounts.get(accid);
            Land_Access_Requirements__c lcrToAdd = (mapAccountLandAccessRequirement.containskey(accid) ? mapAccountLandAccessRequirement.get(accid) : new Land_Access_Requirements__c());
            string keyVal = accid+'~'+ (mapAccountLandAccessRequirement.containskey(accid) ? string.valueof(mapAccountLandAccessRequirement.get(accid).id) : 'NoLAR');
            
            List<Contact> lstContacts = mapAccountLandAccessRequirementsContact.get(accid).get(keyVal);
            if(lstContacts.size()>0)
            {
                for(contact con: lstContacts)
                {
                    LASEReportWrapperList.add(new LASEReportWrapper(accToAdd, lcrToAdd, con));
                }
            }
            else
            {
                LASEReportWrapperList.add(new LASEReportWrapper(accToAdd, lcrToAdd, null));
            }
        }        
        system.debug('**** LASEReportWrapperList ==>'+LASEReportWrapperList);
    }
    
    // Wrapper class to hold the records of all three objects.
    public class LASEReportWrapper{
        public Account instutionAccount {get;set;}
        public Contact laseContact {get;set;}
        public Land_Access_Requirements__c lcrRecord {get;set;}
        public LASEReportWrapper(Account iAcc, Land_Access_Requirements__c lcr, Contact laseCon){            
            instutionAccount = iAcc;
            lcrRecord = lcr;
            laseContact = laseCon;            
        }
    }
    
    // Method to export the table data to excel.
    public PageReference exportToExcel(){return Page.LASEReportExport;}
    public void Export(){
       runLASEReport(); 
     }
     
}