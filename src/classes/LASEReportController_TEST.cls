/*****************************************************************************
Class Name  :  ContactMatrix_CX_TEST
Class Type  :  Test Class 
Version     :  1.0 
Created Date:  25/03/2017
Function    :  This class contains unit test scenarios for ContactMatrix_CX apex class
Used in     :  None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* SUKUMAR SALLA             25/03/2017                 Created
******************************************************************************/
@isTest
public class LASEReportController_TEST {
    
    static testMethod void testmethod1(){       
        ID recordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Institution').getRecordTypeId();
        Account accountRecord = new Account(
            recordTypeId = recordTypeId,
            Name = 'Test Institiution Account',               
            Tier__c = '1'
        );     
        insert accountRecord;
        
        ID recordTypeIdCON = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId();
        Contact contactRecord = new Contact(
            Lastname = 'John',
            Email = 'johntest@test.com',
            recordTypeId = recordTypeIdCON,
            AccountId = accountRecord.id
        );  
        insert contactRecord;
        
        Land_Access_Requirements__c lcrRecord = new Land_Access_Requirements__c(
            Account__c = accountRecord.id
        );
        insert lcrRecord;
        
        LASEReportController obj = new LASEReportController();
        Test.startTest();
        obj.runLASEReport();
        Test.stopTest();
        
    }
}