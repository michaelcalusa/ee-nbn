/*------------------------------------------------------------------------
Author:        Ashutosh
Company:       Salesforce
Description:   Following class is created to provide similiar functionality as standard Convert Lead Buttons. 
               Responsible for:
               1 - Check if all Customer_On_Boarding__c steps related to Lead are completed.
               2 - If all steps are completed take user to standard convert lead page
               3 - Else throw an error.
Test Class:    LeadConvert_Test
History
<Date>            <Authors Name>    <Brief Description of Change> 
--------------------------------------------------------------------------*/
public with sharing class LeadConvert_C {
    
    public Id leadId {get; set;}
        
    public LeadConvert_C (ApexPages.StandardController stdController){
        //Fetch lead id from the URL.
        leadId = ApexPages.currentPage().getParameters().get('id');                    
    }
    
    public PageReference convertLead(){
        
        Set<String> status = new Set<String>{'New', 'In Progress'};
        Id leadRecordTypeId = GlobalCache.getRecordTypeId('Lead', 'Access Seeker');
Id BusinessegeandgleadRecordTypeId = GlobalCache.getRecordTypeId('Lead', 'Business Segment E&G');
		Id BusinessegictleadRecordTypeId = GlobalCache.getRecordTypeId('Lead', 'Business Segment ICT');
        
        // Check if any qualification steps are still in progress or not started.
        List<Qualification_Step__c> qualificationSteps = [SELECT
                                                            Status__c
                                                         FROM
                                                             Qualification_Step__c
                                                         WHERE
                                                             ((Related_Lead__c=:leadId AND Related_Lead__r.RecordTypeId=:leadRecordTypeId)
                                                              AND  
                                                              Status__c In: status)
                                                         ];  
        
           
        // Throw an error if there are steps theat are not complete, skipped or not required
        if(!qualificationSteps.isEmpty()){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Please complete all qualification steps.');
            ApexPages.addMessage(myMsg);
            return null;        
        } 
        
        //Redirect Users to standard convert lead page.
        Lead lObj = [Select RecordTypeId from Lead where id=:leadId Limit 1];
        if(lObj.RecordTypeId == leadRecordTypeId || lObj.RecordTypeId == BusinessegeandgleadRecordTypeId || lObj.RecordTypeId == BusinessegictleadRecordTypeId){
            String sServerName = ApexPages.currentPage().getHeaders().get('Host');
            sServerName = 'https://' + sServerName + '/lead/leadconvert.jsp?retURL=%2F'+leadId+'&id='+leadId+'&nooverride=1'+'&nooppti=1';
            PageReference convertLeadPage =  new pagereference(sServerName);        
            convertLeadPage.setRedirect(true);
            return convertLeadPage ;
        }     
        
        return null;        
                          
    }
    
    public PageReference cancel(){
        return getPreviousPage();
    }
    
    public PageReference getPreviousPage () {
    /*------------------------------------------------------------
    Author:        Dave Norris
    Company:       Salesforce.com
    Description:   Checks the context of where the page was loaded from
                   Redirects the user back to the appropriate page
                   Used when Saving and Cancelling
    Inputs:        None
    Outputs:       A Page Reference to navigate back to
    ------------------------------------------------------------*/      
        String retURL       = ApexPages.currentPage().getParameters().get( 'retURL' );
        String id           = ApexPages.currentPage().getParameters().get( 'id' );
        String pageRef;
        String leadUrl;
                
        // Reference previous page if defined
        if( retURL != null){
            pageRef = retUrl;
        }
        // Reference detail page     
        else if( id != null ){
            pageRef = '/' + id;
        }
        // Reference shipments list by default
        else{
            leadUrl = Schema.Lead.SObjectType.getDescribe().getKeyPrefix();
            pageRef = '/' + leadUrl;
        }
        
        PageReference previousPage = new PageReference( pageRef );
        previousPage.setRedirect( true );
        
        return previousPage;
        
    }    
          
}