@isTest
private class LeadConvert_Test{
/*------------------------------------------------------------------------
Author:        Ashutosh
Company:       Salesforce
Description:   Test class for LeadConvert Class
               1 - Test for Lead Conversion Process
               
Class:           AccountTriggerHandler
History

--------------------------------------------------------------------------*/
    static list<lead> testLeadList;
    static list<Case> testCaseList;
    
    /***
        Test Lead Conversion Process
    ***/
    static testMethod void testConvertLead(){
        try{
            testLeadList = TestDataUtility.createLeadTestRecords(1,true,'John','Smith','NBNTestCompany','Qualified');
            
            List<Qualification_Step__c> qStep = [Select Status__c
                                                    From 
                                                 Qualification_Step__c
                                                     Where
                                                 Related_Lead__c=:testLeadList[0].id];
            for(Qualification_Step__c qS:qStep){
                qS.Status__c = 'Completed';
            }                                      
            
            update qStep;
            
            PageReference testPage = Page.ConvertLead;
            testPage.getParameters().put('id', testLeadList[0].id);
            
            Test.setCurrentPage(testPage);
            Test.StartTest();
          
            ApexPages.StandardController sc = new ApexPages.StandardController(testLeadList[0]);
            LeadConvert_C eUserController = new LeadConvert_C(sc);
                  
            // call the redirectEndUserDetailPage and check the results of the pageReference.
            pageReference pgRef = eUserController.convertLead();
            system.assert(pgRef.getUrl().contains(testLeadList[0].id));
            eUserController.cancel();
            
            test.stopTest();
        }Catch(Exception ex){}
    }
    
    static testMethod void testConvertLeadNonQSteps(){
        try{
            testLeadList = TestDataUtility.createLeadTestRecords(1,true,'John','Smith','NBNTestCompany','Qualified');
            
            PageReference testPage = Page.ConvertLead;
            testPage.getParameters().put('id', testLeadList[0].id);
            
            Test.setCurrentPage(testPage);
            Test.StartTest();
          
            ApexPages.StandardController sc = new ApexPages.StandardController(testLeadList[0]);
            LeadConvert_C eUserController = new LeadConvert_C(sc);
                  
            // call the redirectEndUserDetailPage and check the results of the pageReference.
            pageReference pgRef = eUserController.convertLead();
            //system.assert([Select id from Qualification_Step__c where Related_Lead__c=:testLeadList[0].id].size()>0);
            eUserController.cancel();
            
            test.stopTest();
        }Catch(Exception ex){}
    }    
 
}