/*--------------------------------------------------------------------------------------------------------------------------
<Date>          <Authors Name>     <Brief Description of Change> 
29-06-2016      Shubham Jaiswal    MSEU-16529
12-12-2018      Mohith R           MSEU-19986 : Reflect updated Individual preference information to Lead object                                    
---------------------------------------------------------------------------------------------------------------------------*/
public without sharing class LeadTriggerHandler extends TriggerHandler {
    
    public static boolean isBeforeInsertFirstRun = true;
    public static boolean isBeforeUpdateFirstRun = true;
    public static boolean isBeforeDeleteFirstRun = true;
    public static boolean isAfterInsertFirstRun = true;
    public static boolean isAfterUpdateFirstRun = true;
    public static boolean isAfterDeleteFirstRun = true;
    public static boolean isAfterUndeleteFirstRun = true;

    Set<String> status = new Set<String>{'New', 'In Progress'};
    Id leadRecordTypeId = GlobalCache.getRecordTypeId('Lead', 'Access Seeker');
    Id BusinessegeandgleadRecordTypeId = GlobalCache.getRecordTypeId('Lead', 'Business Segment E&G');
    Id BusinessegictleadRecordTypeId = GlobalCache.getRecordTypeId('Lead', 'Business Segment ICT');
    
    public LeadTriggerHandler() {
        if(Test.isRunningTest()){
            this.setMaxLoopCount(10);
        }
        else{
            this.setMaxLoopCount(2);
        }            
    }
    
    //MSEU-19986 : Reflect updated Individual preference information to Lead object 
    //Invoke the handler to update the lead details from Individual object
    public override void beforeInsert(List <SObject> leadList)
    {         
        if(!HelperUtility.isTriggerMethodExecutionDisabled('UpdateIndividualsByLeads'))
        {
            //IndividualUtilities_Bulk.SetIndividualsToLead(leadList);
            IndividualUtilities_Bulk.UpdateIndividualsByLeads(leadList, true);
        }
    }
    
    public override void beforeUpdate(map <id,SObject> oldMap, map <id,SObject> newMap) 
    {
        validateOnBoardingProcess((Map <Id,Lead>)(oldMap),(Map <Id,Lead>)(newMap));
        calculateLeadCloseDuration((Map <Id,Lead>)(oldMap),(Map <Id,Lead>)(newMap));

        if(!HelperUtility.isTriggerMethodExecutionDisabled('UpdateIndividualsByLeads') && isBeforeUpdateFirstRun)
        {            
            Boolean needsIndividualSync = false;                
            
            for(Lead leadVal : (List<Lead>)newMap.values())
            {
                Lead oldLeadObj = (Lead)(oldMap).get(leadVal.Id);

                if(String.IsBlank(oldLeadObj.Email) && !String.IsBlank(leadVal.Email) && String.IsBlank(leadVal.IndividualId))
                {
                    needsIndividualSync = true;
                } else if(oldLeadObj.Email != leadVal.Email ||
                        oldLeadObj.Salutation != leadVal.Salutation ||
                        oldLeadObj.FirstName != leadVal.FirstName ||
                        oldLeadObj.LastName != leadVal.LastName ||
                        oldLeadObj.Phone != leadVal.Phone)                
                    needsIndividualSync = true;  
                else if(!String.IsBlank(leadVal.Email) && String.IsBlank(leadVal.IndividualId))
                        needsIndividualSync = true;
            }

            if(needsIndividualSync)
            {
                system.debug('***UpdateIndividualsByLeads - Before Update***');
                IndividualUtilities_Bulk.UpdateIndividualsByLeads(newMap.values(), false);
                system.debug('***UpdateIndividualsByLeads - OnBeforeUpdate***'+newMap.values());
                isBeforeUpdateFirstRun = false;
            }
            //if(newLstLead.size() > 0)
            //{
                //system.debug('***SetIndividualsToLead - Before Update***');
                //IndividualUtilities_Bulk.SetIndividualsToLead(newLstLead);
                //system.debug('***SetIndividualsToLead - OnBeforeUpdate***'+newMap.values());
                //isBeforeUpdateFirstRun = false;
            //}           
            
        }
    }
    
    //Checks if all Customer_On_Boarding__c steps related to Lead are completed.
    public void validateOnBoardingProcess(map<id,Lead> leadOldMap,map<id,Lead> leadNewMap){ 
        for(Lead l :[Select id,isConverted,(SELECT id FROM Qualification_Steps__r WHERE Status__c In: status) from Lead where id IN :leadNewMap.keyset() AND RecordTypeId=:leadRecordTypeId]){
            if(l.Qualification_Steps__r != null && l.Qualification_Steps__r.size() > 0 && leadNewMap.get(l.id).isConverted && leadOldMap.get(l.id).isConverted != leadNewMap.get(l.id).isConverted){
                Trigger.newMap.get(l.id).addError('Please complete all qualification steps.');
            }
        }
    }

    private void calculateLeadCloseDuration(map<id,Lead> leadOldMap,map<id,Lead> leadNewMap) {
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault=true limit 1];
        for(Lead l : leadNewMap.values()){
            if(l.RecordTypeId == leadRecordTypeId && l.Status == 'Closed' && l.status!= leadOldMap.get(l.Id).status) {
                l.Lead_Closed_Date__c = datetime.now();
                if(l.Lead_Closed_Date__c!= null && bh!=null){
                    l.Lead_Open_Duration__c =  decimal.valueof(BusinessHours.diff(bh.Id, l.createdDate, l.Lead_Closed_Date__c)/(60*60*8*1000));
                }
            }
        }
    }      
}