@isTest
private class LeadTriggerHandler_Test{
/*------------------------------------------------------------------------
Author:        
Company:       Salesforce
Description:  
Class:           LeadTriggerHandler
History

--------------------------------------------------------------------------*/
    static list<lead> testLeadList;
    static list<Case> testCaseList;
    
    static testMethod void testConvertLead(){
       try{
        
            testLeadList = TestDataUtility.createLeadTestRecords(1,true,'John','Smith','NBNTestCompany','Qualified');
            
            Lead l = new Lead(id = testLeadList[0].id,Status = 'Closed',Closure_Reason__c = 'Failed finance check');
            update l;
                                               
            Test.startTest();
            
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(l.id);
            lc.setDoNotCreateOpportunity(false);
            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            Database.convertLead(lc);

            Test.stopTest();
            
        }Catch(Exception ex){}
    }  
}