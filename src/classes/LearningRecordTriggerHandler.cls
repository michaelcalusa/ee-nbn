/*------------------------------------------------------------------------
Author:        Ganesh Sawant
Company:       Cognizant
Description:   Based on Learning Record Insert/Update and the Status Value, Community Users
               are added to "Certified_Conent_Access" public Group
Test Class:    LearningRecordTriggerHandlerTest
History
<Date>      <Authors Name>     <Brief Description of Change>
13-03-2018  Jairaj Jadhav       Created method ICTcreateDltGrpMmbrship to call ICTcontactMapping method of NewContactTriggerHandler class.
----------------------------------------------------------------------------*/

public class LearningRecordTriggerHandler {
    //Class level variables that are commonly used
    private boolean isExecuting = false;
    private integer batchSize;
    private List<Learning_Record__c> trgOldList = new List<Learning_Record__c> ();
    private List<Learning_Record__c> trgNewList = new List<Learning_Record__c> ();
    private Map<id,Learning_Record__c> trgOldMap = new Map<id,Learning_Record__c> ();
    private Map<id,Learning_Record__c> trgNewMap = new Map<id,Learning_Record__c> ();
    private boolean isBefore;
    private boolean isAfter;
    private boolean isInsert;
    private boolean isUpdate;
    private boolean isDelete;
    private boolean isUnDelete;
    private static boolean isAsync;
    // Below 7 boolean variables are used to Prevent recursion
    public static boolean isBeforeInsertFirstRun = true;
    public static boolean isBeforeUpdateFirstRun = true;
    public static boolean isBeforeDeleteFirstRun = true;
    public static boolean isAfterInsertFirstRun = true;
    public static boolean isAfterUpdateFirstRun = true;
    public static boolean isAfterDeleteFirstRun = true;
    public static boolean isAfterUndeleteFirstRun = true;    
    
    public static final string LEARNING_CERTIFIED_STATUS = 'Certified';
    public static final string CERTIFIED_GROUP_NAME = 'Certified_Content_Access';

    public LearningRecordTriggerHandler(boolean isExecuting, integer batchSize, List<Learning_Record__c> trgOldList, List<Learning_Record__c> trgNewList, Map<id,Learning_Record__c> trgOldMap, Map<id,Learning_Record__c> trgNewMap, boolean isBefore, boolean isAfter, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUnDelete){
        this.isExecuting = isExecuting;
        this.BatchSize = batchSize;
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
        this.isBefore = isBefore;
        this.isAfter = isAfter;
        this.isInsert = isInsert;
        this.isUpdate = isUpdate;
        this.isDelete = isDelete;
        this.isUnDelete = isUnDelete;
        LearningRecordTriggerHandler.isAsync = System.isBatch() || System.isFuture();
    }
    public void OnAfterInsert(){
      //grantCertifiedContentGroupAccess(trgNewList, new Map<id,Learning_Record__c>());
      ICTcreateDltGrpMmbrship(null, trgNewMap);
    }
    public void OnBeforeUpdate(){}
    public void OnBeforeDelete(){}
    public void OnBeforeInsert(){}
    public void OnAfterUpdate(){
      //grantCertifiedContentGroupAccess(trgNewList, trgOldMap);
      ICTcreateDltGrpMmbrship(trgOldMap, trgNewMap);
    }
    public void OnAfterDelete(){}
    public void OnUndelete(){}
    
    /***************************************************************************************************
    Method Name         : ICTcreateDltGrpMmbrship
    Created Date        : 13-03-2018
    User Story          : 9486
    Function            : This method calls ICTcontactMapping method of NewContactTriggerHandler class for those contacts for which Learning Record status
                          was either changed to Certified or changed to something else from Certified.
    * Developer                   Date                   Description
    * --------------------------------------------------------------------------------------------------
    * Jairaj Jadhav              13-03-2018                Created
    ****************************************************************************************************/
    public void ICTcreateDltGrpMmbrship(Map<id,Learning_Record__c> trgOldMap, Map<id,Learning_Record__c> trgNewMap){
        Set<id> contIDSet = new Set<id>();
        for(Learning_Record__c lr: trgNewMap.values()){
            if(trgOldMap != null){
                if((trgOldMap.get(lr.id).Course_Status__c != 'Certified' && lr.Course_Status__c == 'Certified') || (trgOldMap.get(lr.id).Course_Status__c == 'Certified' && lr.Course_Status__c != 'Certified')){
                    contIDSet.add(lr.Contact__c);
                }
            }
        }
        if(!contIDSet.isEmpty() && contIDSet.size() > 0){
            id prtnrRcdtypID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Partner Contact').getRecordTypeId();
            Map<id,Contact> cntctIDCntctMap = new map<id,contact>([select id,recordtypeid, ICT_Community_User__c, ICT_Contact_Role__c from contact where ICT_Community_User__c = true AND recordtypeid=:prtnrRcdtypID AND ID IN: contIDSet]);
            if(!cntctIDCntctMap.keySet().isEmpty() && cntctIDCntctMap.keySet().size() > 0){
                NewContactTriggerHandler contactTriggerHandler = new NewContactTriggerHandler(true, 5, null, null, null, null);
                system.debug('before calling ICTcontactMapping method from Learning Record trigger cntctIDCntctMap==>'+cntctIDCntctMap);
                contactTriggerHandler.ICTcontactMapping(true, null, cntctIDCntctMap);
            }
        }
    }
    
    /*public static void grantCertifiedContentGroupAccess(List<Learning_Record__c> trgNewList,Map<id,Learning_Record__c> trgOldMap){
        
      if (trgOldMap.isEmpty()) {
            set<ID> conIDSet = new set<ID>();
            set<ID> userIDSet = new set<ID>();            
            for(Learning_Record__c lr : trgNewList){
                if(lr.Course_Status__c == LEARNING_CERTIFIED_STATUS && lr.Contact__c!=NULL) {
                    conIDSet.add(lr.Contact__c);
                }                
            }
            for(User u : [Select Id, ContactID From user Where IsPortalEnabled = TRUE AND ContactID IN : conIDSet]){
                userIDSet.add(u.ID);
            }
            System.debug('userIDSet-------'+userIDSet);
            //createGroupMember(userIDSet);
        } else {
            set<ID> conIDSet = new set<ID>();          
            set<ID> userIDSet = new set<ID>();            
            for(Learning_Record__c lr : trgNewList){
                if(lr.Course_Status__c == LEARNING_CERTIFIED_STATUS && lr.Contact__c!=NULL && lr.Course_Status__c != trgOldMap.get(lr.ID).Course_Status__c) {
                    conIDSet.add(lr.Contact__c);
                }                
            }
            for(User u : [Select Id, ContactID From user Where IsPortalEnabled = TRUE AND ContactID IN : conIDSet]){
                userIDSet.add(u.ID);
            }
            System.debug('userIDSet-------'+userIDSet);
            if(userIDSet.size() > 0){
                //createGroupMember(userIDSet);
            }            
        }
    }
  @future
    public static void createGroupMember(Set<Id> userIds) {
        list<GroupMember> insertListGroupMember = new list<GroupMember>();        
      ID groupID = [Select ID From Group Where DeveloperName =: CERTIFIED_GROUP_NAME].ID;
        for(ID userID : userIds) {
                GroupMember gm= new GroupMember(); 
                gm.GroupId=groupID;
                gm.UserOrGroupId = userID;            
                insertListGroupMember.add(gm);                  
        }
        System.debug('insertListGroupMember-------'+insertListGroupMember);
        if(!insertListGroupMember.isEmpty()){
            insert insertListGroupMember;
        }        
    }*/
}