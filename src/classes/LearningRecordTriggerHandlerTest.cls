/*------------------------------------------------------------------------
Author:        Ganesh Sawant
Company:       Cognizant
Description:   Test class for LearningRecordTriggerHandler class
History
<Date>      <Authors Name>     <Brief Description of Change>
----------------------------------------------------------------------------*/

@isTest(SeeAllData=false)
public class LearningRecordTriggerHandlerTest {
    
    static testmethod void testgrantCertifiedContentGroupAccess() {
        //User ownerID = [Select ID From User Where UserRoleId!=NULL LIMIT 1];
        //Create Business Account
        profile accOwnProfile = [select id from profile where Name = 'System Administrator' limit 1];
        user accOwnerUsr = new user(LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',
        LanguageLocaleKey='en_US',TimeZoneSidKey='Australia/Sydney',CommunityNickname='ictaccown',Alias='ictaccow',
        Email='test@test.com',firstName = 'Account', lastName = 'Owner', username='testclassaccown@test.com',CompanyName = 'TEST',
        FederationIdentifier='testclassaccown', profileid=accOwnProfile.id);
        System.runAs (new User(Id = UserInfo.getUserId())){
            UserRole accOwnRole = new UserRole(Name = 'Finance Approver');
            insert accOwnRole;
            accOwnerUsr.Userroleid= accOwnRole.id;
            insert accOwnerUsr;
        }
        Account businessAccount = new Account();
        businessAccount.OwnerId = accOwnerUsr.ID;
        businessAccount.Name = 'Test Business Account';
        businessAccount.RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Partner Account').getRecordTypeId();
        businessAccount.Account_Types__c = 'Other';
        insert businessAccount;
        
        //Create Business Contact
        Contact businessContact = new Contact();
        businessContact.AccountID = businessAccount.ID;
        businessContact.LastName = 'Test BS Contact';
        businessContact.Lase_State__c = 'NSW;VIC';
        businessContact.RecordTypeId  = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('Partner Contact').getRecordTypeId();   
        insert businessContact;
        
        Profile portalProfile = [SELECT Id FROM Profile Where Name ='ICT Customer Community Plus User' Limit 1];
        test.startTest();
        User user1 = new User(
        Username = System.now().millisecond() + 'test12345@test.com',
        ContactId = businessContact.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'testict12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
        insert user1;   
        
        
        //Id lerarningAssignment = [Select id From lmscons__Transcript_Line__c LIMIT 1].ID;
        list<Learning_Record__c> lrlist = new list<Learning_Record__c>();
        Learning_Record__c lr1 = new Learning_Record__c();
        lr1.Account__c = businessAccount.Id;
        lr1.Contact__c = businessContact.Id;
        //lr1.Assignment__c = lerarningAssignment;
        lr1.Course_Status__c = 'Certified';
        lrlist.add(lr1);
        Learning_Record__c lr2 = new Learning_Record__c();
        lr2.Account__c = businessAccount.Id;
        lr2.Contact__c = businessContact.Id;
        //lr2.Assignment__c = lerarningAssignment;
        lr2.Course_Status__c = 'In Progress';
        lrlist.add(lr2);   
        insert lrlist;
        lr2.Course_Status__c = 'Certified';
        update lr2;
        delete lr1;
        test.stopTest();
        /*System.debug('Learning Record>>>>>>>'+lr2);
        ID groupID = [Select ID From Group Where DeveloperName =: 'Certified_Content_Access'].ID;
        set<ID> groupMemberIDSet = new set<ID>();
        for(GroupMember gm : [Select UserOrGroupId From GroupMember Where GroupID =: groupID]) {
            groupMemberIDSet.add(gm.UserOrGroupId);
        }
        System.debug('groupMemberIDSet----------'+groupMemberIDSet);*/
        System.assertEquals(TRUE, lr2.Course_Status__c == 'Certified');
    }
}