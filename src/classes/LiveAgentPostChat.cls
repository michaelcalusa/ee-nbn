/***************************************************************************************************
Class Name:         LiveAgentPostChat
Class Type:         Apex Class 
Version:            1.0 
Created Date:       31/05/2018
Function:           This is a class to get the CaseId & ContactID from PreChat Form to PostChat Form (i.e Survey) 
Input Parameters:   None 
Output Parameters:  None
Description:        Pass the Case & Contact ID to from PreChat Form to PostChat (i.e: Survey)
Modification Log:
* Developer            Date             Description
* --------------------------------------------------------------------------------------------------                 
* Sakthivel Madesh      31/05/2018      Created - Version 1.0 Refer CUSTSA-13635 for Epic description
****************************************************************************************************/ 
public class LiveAgentPostChat {

    public String caseId {get;set;}
    public String contactId {get;set;}
    public String attachedRecords {get;set;}
    
    public LiveAgentPostChat() {
        attachedRecords = ApexPages.currentPage().getParameters().get('attachedRecords');
        if(attachedRecords != null) {
            caseId = LiveAgentPostChatJSON2Apex.parse(attachedRecords).caseId;
            contactId = LiveAgentPostChatJSON2Apex.parse(attachedRecords).contactId;
        }
    }  
    
    public PageReference getReroute() {
        attachedRecords = ApexPages.currentPage().getParameters().get('attachedRecords');
        caseId = LiveAgentPostChatJSON2Apex.parse(attachedRecords).caseId;
        contactId = LiveAgentPostChatJSON2Apex.parse(attachedRecords).contactId;        
        PageReference result = new PageReference(Label.LiveAgentSurveyURL+'&caId='+caseId+'&cId='+contactId);
        return result;
    }
    
}