/***************************************************************************************************
Class Name:         LiveAgentPostChatJSON2Apex
Class Type:         Apex Class 
Version:            1.0 
Created Date:       31/05/2018
Function:           This class is used to split the CaseId & ContactID from LiveAgentPostChat  
Input Parameters:   Json string (for case & contact parameters)
Output Parameters:  Return Case Id & Contact Id
Description:        Pass the Case & Contact ID to from PreChat Form to PostChat (i.e: Survey)
Modification Log:
* Developer            Date             Description
* --------------------------------------------------------------------------------------------------                 
* Sakthivel Madesh      31/05/2018      Created - Version 1.0 Refer CUSTSA-13635 for Epic description
****************************************************************************************************/ 
public class LiveAgentPostChatJSON2Apex {
    public static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.START_OBJECT || 
                curr == JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == JSONToken.END_OBJECT ||
                curr == JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }

    public String caseId {get;set;} // in json: Case
    public String contactId {get;set;} // in json: Contact

    public LiveAgentPostChatJSON2Apex(JSONParser parser) {
        while (parser.nextToken() != JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String text = parser.getText();
                system.debug('text:::'+text);
                if (parser.nextToken() != JSONToken.VALUE_NULL) {
                    if (text == 'ContactId') {
                        contactId = parser.getText();
                        system.debug('contactId:::'+contactId);
                    } else if (text == 'CaseId') {
                        caseId = parser.getText();
                        system.debug('caseId:::'+caseId);
                    } else {
                        System.debug(LoggingLevel.WARN, 'Root consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }


    public static LiveAgentPostChatJSON2Apex parse(String json) {
        return new LiveAgentPostChatJSON2Apex(System.JSON.createParser(json));
    }
}