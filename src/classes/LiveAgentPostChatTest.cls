/***************************************************************************************************
Class Name:         LiveAgentPostChatTest
Class Type:         Test Class 
Version:            1.0 
Created Date:       11/07/2018
Function:           This is a test class to cover LiveAgentPostChat class and Pass the Case & Contact ID to Post Chat Page (i.e Survey) 
Input Parameters:   None 
Output Parameters:  None
Modification Log:
* Developer            Date             Description
* --------------------------------------------------------------------------------------------------                 
* Sakthivel Madesh      11/07/2018      Created - Version 1.0 Refer CUSTSA-13635 for Epic description
****************************************************************************************************/ 

@isTest
private class LiveAgentPostChatTest {
    
    static testmethod void validateReroute(){
        //Setup data
        Account account = new Account(
            Name = 'Test Account',
            RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
            Tier__c = '1',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert account;

        Contact contact = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId(),
            AccountId = account.Id,
            Email = 'teste11111@example.com',
            Preferred_Phone__c = '0211111111'
        );
        insert contact;
                    
        Site__c site = new Site__c(
            Name = 'test site'
        );
        insert site;
        
        Case caseObj = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId(),
            Phase__c = 'Information',
            Category__c = '(I) Communications',
            Sub_Category__c = 'Discovery Centre',
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
            ContactId = contact.Id,
            Primary_Contact_Role__c = 'General Public',
            Site__c = site.Id
        );
        insert caseObj;
        
        test.startTest();  
        //String attachedRecords = '{\"CaseId\":\"500N000000C9j5MIAR\",\"ContactId\":\"003N000001C032AIAR\"}';
        String attachedRecords = '{\"CaseId\":\"'+caseObj.Id+'\",\"ContactId\":\"'+contact+'\",\"TestParam\":\"'+contact+'\"}';
        
        PageReference pageRef = Page.LiveAgentPostChat;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('attachedRecords',attachedRecords);
        pageRef.getParameters().put('CaseId',LiveAgentPostChatJSON2Apex.parse(attachedRecords).caseId);
        pageRef.getParameters().put('contactId',LiveAgentPostChatJSON2Apex.parse(attachedRecords).contactId);
        
        LiveAgentPostChat liveObj = new LiveAgentPostChat();
        liveObj.attachedRecords = attachedRecords;
        liveObj.caseId = LiveAgentPostChatJSON2Apex.parse(attachedRecords).caseId;
        liveObj.contactId = LiveAgentPostChatJSON2Apex.parse(attachedRecords).contactId;
        
        liveObj.getReroute();
        test.stopTest();  

    }
   
}