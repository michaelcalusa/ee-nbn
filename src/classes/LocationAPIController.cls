/***************************************************************************************************
    Class Name          : LocationAPIController
    Version             : 1.0 
    Created Date        : 12-Sep-2017
    Author              : Rupendra Kumar Vats
    Description         : Logic to call Location API to return address and it's details
    Input Parameters    : User provided address 
    Output Parameters   : Address and it's details

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats       12-Sep-2017       Logic to call Location API to return address and it's details
****************************************************************************************************/ 
public class LocationAPIController{
    
    /* 
        Input Paramter : (1) strLocation- location provided by user (2) intLocationLimit- Number of location results for display
        Output Parameter : Wrapper holding formatted adrress and it's location id
        Purpose : Get address from Location AutoComplete API and display to user for selection
    */
    @AuraEnabled
    public static LocationAPIResponse getLocations(String strLocation, Integer intLocationLimit, Boolean usePlacesGateway){
        //List<LocationWrapper> lstLocationWrapper = new List<LocationWrapper>();
        LocationAPIResponse locationAPIResponse = new LocationAPIResponse();
        LocationAPIHelper helper = new LocationAPIHelper();
        LightningComponentError__mdt mdtLCE;
        try{
            HttpRequest request = new HttpRequest();
            Http http = new Http();     
            String URLEndPoint = '';
            System.debug('use places gateway ' + usePlacesGateway);
            if(usePlacesGateway == false){	            
	            URLEndPoint = '?filter=fullText=='+ EncodingUtil.urlEncode(strLocation, 'utf-8');
	            request.setEndpoint('callout:LocationAddresses' + URLEndPoint);
            }else{
            	request.setHeader('Referer', 'https://www.nbnco.com.au');
            	URLEndPoint = 'https://places.nbnco.net.au/places/v1/autocomplete?query='+ EncodingUtil.urlEncode(strLocation, 'utf-8');
            	//request.setEndpoint('callout:Places_Gateway' + URLEndPoint);
            	system.debug(URLEndPoint);
            	request.setEndpoint(URLEndPoint);
            }
            // Testing with stubs
            //request.setEndpoint('http://putsreq.com/t190afUWSWNBobUtYboe');
            request.setMethod('GET');
            request.setTimeOut(integer.valueof(Label.Location_TimeOut_In_MilliSeconds));
            HttpResponse response = http.send(request);
            
            system.debug('---response---' + response);   
            system.debug('---response body---' + response.getBody());  
            
            if (response.getStatusCode() == 200 && string.isNotBlank(response.getBody()) && response.getBody()!='{}') {
                if(usePlacesGateway == true){
                	locationAPIResponse = parsePlacesGatewayResponse(response.getBody(), intLocationLimit);	
                }else{
                	LocationJSONResponseParsar.LocationRoot rootNode = (LocationJSONResponseParsar.LocationRoot)JSON.deserialize(response.getBody(), LocationJSONResponseParsar.LocationRoot.Class);
                    	
					List<LocationWrapper> lstLocationWrapper = new List<LocationWrapper>();
	                if(!rootNode.data.isEmpty()){
	                    // Limit the results according to intLocationLimit value
	                    system.debug('---intLocationLimit---' + intLocationLimit + '---' + rootNode.data.size());
	                    List<LocationJSONResponseParsar.Location> lstLocations = new List<LocationJSONResponseParsar.Location>();
	
	                    if(rootNode.data.size() <= integer.valueOf(intLocationLimit)){
	                        lstLocations.addAll(rootNode.data);
	                    }
	                    else{
	                        for(integer i=0; i < integer.valueOf(intLocationLimit); i++){
	                            lstLocations.add(rootNode.data[i]);
	                        }
	                    }
	                    
	                    system.debug('---lstLocations---' + lstLocations);
	                    String strLocationID, strAddress, strLotNumber;
	                    for(LocationJSONResponseParsar.Location loc : lstLocations){
	                        strLocationID = '';
	                        strAddress = '';
	                        strLotNumber = '';
	                        
	                        if(!string.isEmpty(loc.relationships.locatedAt.data.id)){
	                            strLocationID = loc.relationships.locatedAt.data.id;
	                        }
	                        
	                        if(!string.isEmpty(loc.attributes.principal.fullText)){
	                            strAddress = loc.attributes.principal.fullText;
	                        }
	                        
	                        if(!string.isEmpty(loc.attributes.principal.lotNumber)){
	                            strLotNumber = loc.attributes.principal.lotNumber;
	                        }
	                        
	                        lstLocationWrapper.add(new LocationWrapper(strAddress, strLocationID, strLotNumber, ''));
	                    }
	                    locationAPIResponse.setLocationWrappers(lstLocationWrapper);
	                }
	                else{
	                    mdtLCE =  helper.getServerError('0001');
	                    lstLocationWrapper.add(new LocationWrapper('', '', '', mdtLCE.ErrorMessage__c));
	                    locationAPIResponse.setLocationWrappers(lstLocationWrapper);
	               }
               }        
           }    
           else{
                mdtLCE =  helper.getServerError('0002');
                List<LocationWrapper> lstLocationWrapper = new List<LocationWrapper>();
                lstLocationWrapper.add(new LocationWrapper('', '', '', mdtLCE.ErrorMessage__c));
                locationAPIResponse.setLocationWrappers(lstLocationWrapper);
           }
        }Catch(Exception ex){
            system.debug('---LocationAPIController - getLocations---' + ex.getMessage());
            mdtLCE =  helper.getServerError('0002');
            List<LocationWrapper> lstLocationWrapper = new List<LocationWrapper>();
            lstLocationWrapper.add(new LocationWrapper('', '', '', mdtLCE.ErrorMessage__c));
            locationAPIResponse.setLocationWrappers(lstLocationWrapper);
        }
        return locationAPIResponse;
    }
    
    /* 
        Input Paramter : strLocationID- Location ID for the selected address by user
        Output Parameter : Service_Eligibility__mdt record for eligibility details
        Purpose : Get address from Location API and display details to user
    */
    @AuraEnabled
    public static ServiceEligibilityWrapper getLocationDetails(String strLocationID, string strLotNumber){
        ServiceEligibilityWrapper wrapperSE;
        LightningComponentError__mdt mdtLCE;
        LocationAPIHelper helper = new LocationAPIHelper();
        Map<String,String> mapAddress = new Map<String,String>();
        String strServiceClass;

        try{
            HttpRequest request = new HttpRequest();
            Http http = new Http();     
            String URLEndPoint = '/'+ strLocationID;
            request.setEndpoint('callout:LocationAPI' + URLEndPoint);
            //request.setEndpoint('callout:LocationAPI' + '/LOC000064484671');

            //request.setEndpoint('http://putsreq.com/7Dy8NIeHxutLhVTdb9oB');
            request.setMethod('GET');
            request.setTimeOut(integer.valueof(Label.Location_TimeOut_In_MilliSeconds));
            HttpResponse response = http.send(request);
            
            system.debug('---response---' + response);   
            system.debug('---response body---' + response.getBody());  
            
            if (response.getStatusCode() == 200 && string.isNotBlank(response.getBody())) {
                LocationJSONResponseParsar.LocationDetailsRoot rootNode = (LocationJSONResponseParsar.LocationDetailsRoot)JSON.deserialize(response.getBody(), LocationJSONResponseParsar.LocationDetailsRoot.Class);

                if(!rootNode.data.isEmpty()){
                    LocationJSONResponseParsar.Data locationdetails = rootNode.data[0];
                    
                    if(!string.isEmpty(locationdetails.attributes.premises.serviceClass)){
                        strServiceClass = locationdetails.attributes.premises.serviceClass;
                        
                        // Call the getServiceEligibility from helper class
                        Service_Eligibility__mdt mdtSE =  helper.getServiceEligibility(strServiceClass);
                        // use it for stubs
                        //Service_Eligibility__mdt mdtSE =  helper.getServiceEligibility('10');
                        
                        if(mdtSE != null){
                            // Capture the address details
                            mapAddress = captureLocationDetails(rootNode, strLotNumber, strLocationID, mdtSE.Technology__c);
                            wrapperSE = new ServiceEligibilityWrapper(mdtSE.Eligibility__c, mdtSE.Technology__c, mdtSE.Error_Message__c, mapAddress);
                        }
                        else{
                            mdtLCE =  helper.getServerError('0002');
                            wrapperSE = new ServiceEligibilityWrapper(System.Label.LocationNotEligible, '', mdtLCE.ErrorMessage__c, new Map<String,String>());
                        }
                    }
                    else{
                        mdtLCE =  helper.getServerError('0002');
                        wrapperSE = new ServiceEligibilityWrapper(System.Label.LocationNotEligible, '', mdtLCE.ErrorMessage__c, new Map<String,String>());
                    }
                    system.debug('---strServiceClass---' + strServiceClass);
                }
                else{
                    mdtLCE =  helper.getServerError('0002');
                    wrapperSE = new ServiceEligibilityWrapper(System.Label.LocationNotEligible, '', mdtLCE.ErrorMessage__c, new Map<String,String>());
                }
           }    
        }Catch(Exception ex){
            system.debug('---LocationAPIController - getLocationDetails---' + ex.getMessage());
            mdtLCE =  helper.getServerError('0002');
            wrapperSE = new ServiceEligibilityWrapper(System.Label.LocationNotEligible, '',  mdtLCE.ErrorMessage__c, new Map<String,String>());
        }
        return wrapperSE;
    }
    
    /* 
        Input Paramter : 
                        rootNode - Response root node to taverse values, 
                        strLotNumber - Lot Number for the location,
                        strLocationID- Location ID for location selected,
                        strTechnology- Technology at current location
        Output Parameter : Address map with all details
        Purpose : Capture address details that will be used for Tech Choice form submission
    */
    private static Map<String,String> captureLocationDetails(LocationJSONResponseParsar.LocationDetailsRoot rootNode, string strLotNumber, string strLocationID, string strTechnology){
        Map<String,String> mapAddress = new Map<String,String>();
        LocationJSONResponseParsar.Included location = rootNode.included[0];
        
        mapAddress.put('LotNumber', strLotNumber);
        mapAddress.put('LocationID', strLocationID);
        mapAddress.put('Technology', strTechnology);
        
        if(!string.isEmpty(location.attributes.principal.unitNumber)){
            mapAddress.put('UnitNumber', location.attributes.principal.unitNumber);
        }
        else{
            mapAddress.put('UnitNumber','');
        }
        
        if(!string.isEmpty(location.attributes.principal.levelNumber)){
            mapAddress.put('LevelNumber', location.attributes.principal.levelNumber);
        }
        else{
            mapAddress.put('LevelNumber','');
        }
        
        if(!string.isEmpty(location.attributes.principal.roadNumber1)){
            mapAddress.put('RoadNumber', location.attributes.principal.roadNumber1);
        }
        else{
            mapAddress.put('RoadNumber', '');
        }
        
        if(!string.isEmpty(location.attributes.principal.roadName)){
            mapAddress.put('RoadName', location.attributes.principal.roadName);
        }
        else{
            mapAddress.put('RoadName', '');
        }
        
        if(!string.isEmpty(location.attributes.principal.roadTypeCode)){
            mapAddress.put('RoadSuffix', location.attributes.principal.roadTypeCode);
        }
        else{
            mapAddress.put('RoadSuffix', '');
        }
        
        if(!string.isEmpty(location.attributes.principal.locality)){
            mapAddress.put('Suburb', location.attributes.principal.locality);
        }
        else{
            mapAddress.put('Suburb', '');
        }
        
        if(!string.isEmpty(location.attributes.principal.postcode)){
            mapAddress.put('PostCode', location.attributes.principal.postcode);
        }
        else{
            mapAddress.put('PostCode', '');
        }
        
        if(!string.isEmpty(location.attributes.principal.state)){
            mapAddress.put('State', location.attributes.principal.state);
        }
        else{
            mapAddress.put('State', '');
        }
        
        if(!string.isEmpty(location.attributes.principal.fullText)){
            mapAddress.put('SiteName', location.attributes.principal.fullText);
        }
        else{
            mapAddress.put('SiteName', '');
        }
        
        system.debug('---mapAddress---' + mapAddress);
        return mapAddress;
    }
    
    private static LocationAPIResponse parsePlacesGatewayResponse(String responseBody,
    													Integer maxResults){
    	LocationJSONResponseParsar.PlacesAutocomplete placesAutocomplete = (LocationJSONResponseParsar.PlacesAutocomplete)JSON.deserialize(responseBody, LocationJSONResponseParsar.PlacesAutocomplete.Class);
		List <LocationJSONResponseParsar.PlacesSuggestions> suggestions = placesAutocomplete.suggestions;
		LocationAPIResponse LocationAPIResponse = null; 
		List<LocationWrapper> locationList = new List<LocationWrapper>();
		if(suggestions.isEmpty()){
			LightningComponentError__mdt mdtLCE =  new LocationAPIHelper().getServerError('0001');
	        locationList.add(new LocationWrapper('', '', '', mdtLCE.ErrorMessage__c));
	        locationApiResponse = new LocationAPIResponse(locationList, false);
	        return locationApiResponse; 
		}
		
		Integer counter = 0;
       	Integer actualSize = suggestions.size();
        //system.debug('actual size is '+actualSize);                                                   
        /*for(integer i =0; i < actualSize; i++){
            system.debug('the suggestions '+i+ ' are '+suggestions[i]);                                                                 
        }  */                                                  

        if(actualSize < maxResults){
            maxResults = actualSize;
        } 
        
       // system.debug('the max results is '+maxResults);                                                    
                                                            
		while (counter < maxResults){
           // system.debug('loop counter'+counter);
			LocationJSONResponseParsar.PlacesSuggestions suggestion = suggestions.get(counter);
			locationList.add(new LocationWrapper(suggestion.formattedAddress, suggestion.id, '', ''));
			counter++;
		}
		locationAPIResponse = new LocationAPIResponse(locationList, (placesAutocomplete.source.equals('google') || placesAutocomplete.source.equals('region')));
        //system.debug('locationAPI Response is'+locationAPIResponse.locationWrappers[0].strLocation);                                                    
		return locationAPIResponse;		    	
    }
	
	/* 
		return this to the front end, containing list of LocationWrappers. It also stores if
		the search results were obtained from google.
	*/
	public class LocationAPIResponse {
		
		private List<LocationWrapper> locationWrappers;
		private Boolean googleResult;
		
		public LocationAPIResponse(){
			this.locationWrappers = new List<LocationWrapper>();
			this.googleResult = false;
		}
		
		public LocationAPIResponse(List<LocationWrapper> locationWrappers, Boolean googleResult){
            this.locationWrappers = locationWrappers;
            this.googleResult = googleResult;
        }
        
		@AuraEnabled
		public Boolean getGoogleResult() {
			return this.googleResult;
		}	
		
		public void setLocationWrappers(List<LocationWrapper> locationWrappers){
			this.locationWrappers = locationWrappers;
		}
		
		@AuraEnabled
		public List<LocationWrapper> getLocationWrappers(){
			return this.locationWrappers;
		}		
		
		

	}
	
    /* Wrapper class to pass address details information */
    public class LocationWrapper{
        @AuraEnabled
        public String strLocation { get; set; }
        
        @AuraEnabled
        public String strLocationID { get; set; }
        
        @AuraEnabled
        public String strLotNumber { get; set; }
        
        @AuraEnabled
        public String strError { get; set; }
        
        public LocationWrapper(String strLocation, String strLocationID, String strLotNumber, String strError){
            this.strLocation = strLocation;
            this.strLocationID = strLocationID;
            this.strLotNumber = strLotNumber;
            this.strError = strError;
        }
    }
    
    /* Wrapper class to pass Service Eligibility information */
    public class ServiceEligibilityWrapper{
        @AuraEnabled
        public String strEligibility { get; set; }
        
        @AuraEnabled
        public String strTechnology { get; set; }
        
        @AuraEnabled
        public String strErrorMessage { get; set; }
        
        @AuraEnabled
        public Map<String,String> mapAddress { get; set; }
        
        @AuraEnabled
        public List<data> data {get; set;}
        
        public ServiceEligibilityWrapper(String strEligibility, String strTechnology, String strError, Map<String,String> mapAddress){
            this.strEligibility = strEligibility;
            this.strTechnology = strTechnology;
            this.strErrorMessage = strError;
            this.mapAddress = mapAddress;
        }
    }
    
    public class data{
        @AuraEnabled public attributes attributes {get; set;}
    }
    
    public class attributes{
        @AuraEnabled public geopoint geopoint {get; set;}
    }
    
    public class geopoint{
        @AuraEnabled public string latitude {get; set;}
        @AuraEnabled public string longitude {get; set;}
    }
    
}