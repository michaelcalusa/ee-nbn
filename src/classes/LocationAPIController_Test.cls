/***************************************************************************************************
    Class Name  : LocationAPIController_Test
    Class Type  : Test Class 
    Version     : 1.0 
    Created Date: 22-September-2017
    Function    : This class contains unit test scenarios for LocationAPIController apex class which handles Location Search
    Used in     : None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            22/09/2017                 Created
****************************************************************************************************/
@isTest(seeAllData = false)
public class LocationAPIController_Test{

    static testMethod void TestMethod_doLocationSearch() {
        // Verify system call out exception
        LocationAPIController.LocationAPIResponse LocationAPIResponseGoogle = LocationAPIController.getLocations('177 Pacific', 5,true);
        LocationAPIController.getLocationDetails('LOC000067569295', '66');
        LocationAPIResponseGoogle.getGoogleResult();
        LocationAPIResponseGoogle.getLocationWrappers();
        
        // Verify Responses using Mock classes for Success and Failure
        MockHttpResponseGenerator mockResp = new MockHttpResponseGenerator();
        
        // Verify the success call out
        mockResp.strResponseType = 'LocationSuccess';
        Test.setMock(HttpCalloutMock.class, mockResp);
        LocationAPIController.getLocations('177 Pacific', 5,true);

        mockResp.strResponseType = 'LocationSuccess';
        Test.setMock(HttpCalloutMock.class, mockResp);
        LocationAPIController.getLocations('177 Pacific', 2,false);       
                
        // Verify the success call out
        mockResp.strResponseType = 'LocationDetailsSuccess';
        Test.setMock(HttpCalloutMock.class, mockResp);
        LocationAPIController.getLocationDetails('LOC000067569295', '66');
        
        mockResp.strResponseType = 'LocationDetailsEmpty';
        Test.setMock(HttpCalloutMock.class, mockResp);
        LocationAPIController.getLocationDetails('LOC000067569295', '66');
    }
      
    
    static testMethod void TestMethod_jsonResponse() {
        LocationJSONResponseParsar.LocationRoot root = new LocationJSONResponseParsar.LocationRoot();
        
        LocationJSONResponseParsar.Meta meta = new LocationJSONResponseParsar.Meta();
        meta.totalPages = '5';
        meta.totalMatchedResources = '6';
        
        root.meta = meta;
        
        
        LocationJSONResponseParsar.Alias alias = new LocationJSONResponseParsar.Alias();
        alias.postCode = '2009';
        alias.locality = 'THIRLMERE';
        alias.roadName = 'IBBOTSON';
        alias.roadTypeCode = 'ST';
        alias.state = 'NSW';
        alias.fullText = 'LOT 22 8 IBBOTSON ST THIRLMERE NSW 2573 Australia';
        alias.roadNumber1 = '8';
        alias.lotNumber = '66';
        alias.country = 'Australia';
        
        List<LocationJSONResponseParsar.Alias> lstAlias = new List<LocationJSONResponseParsar.Alias>();
        lstAlias.add(alias);
        
        LocationJSONResponseParsar.AttributesPrincipal attrprincipal = new LocationJSONResponseParsar.AttributesPrincipal();
        attrprincipal.postCode = '2009';
        attrprincipal.locality = 'THIRLMERE';
        attrprincipal.roadName = 'IBBOTSON';
        attrprincipal.roadTypeCode = 'ST';
        attrprincipal.state = 'NSW';
        attrprincipal.fullText = 'LOT 22 8 IBBOTSON ST THIRLMERE NSW 2573 Australia';
        attrprincipal.roadNumber1 = '8';
        attrprincipal.lotNumber = '66';
        attrprincipal.country = 'Australia';
        
        LocationJSONResponseParsar.AttributesMeta attrmeta = new LocationJSONResponseParsar.AttributesMeta();
        attrmeta.aliasMatchIndex = '2';
        attrmeta.relevanceScore = '2';
        
        LocationJSONResponseParsar.GeoPoint geopoint = new LocationJSONResponseParsar.GeoPoint();
        geopoint.latitude = '150.584054';
        geopoint.longitude = '150.584054';
        
        LocationJSONResponseParsar.Attributes attr = new LocationJSONResponseParsar.Attributes();
        attr.aliases = lstAlias;
        attr.principal = attrprincipal;
        attr.meta = attrmeta;
        attr.geopoint = geopoint;
        
        LocationJSONResponseParsar.Location loc = new LocationJSONResponseParsar.Location();
        loc.type = 'location';
        loc.id = 'LOC000067569295';
        loc.attributes = attr;
        
        LocationJSONResponseParsar.Premises premises = new LocationJSONResponseParsar.Premises();
        premises.disconnectionDate = '2009';
        premises.readyForServiceDate = 'THIRLMERE';
        premises.serviceClass = 'IBBOTSON';
        premises.isServiceable = 'ST';
        premises.isFrustrated = 'NSW';
        premises.serviceType = 'LOT 22 8 IBBOTSON ST THIRLMERE NSW 2573 Australia';
        premises.primaryTechnology = '8';
        premises.technologyType = '66';
        premises.csaId = 'Australia';
        premises.serviceClassDescription = '66';
        premises.serviceClassReason = '66';
        premises.technologyOverride = '66';
        premises.premisesFlag = '66';
        premises.disconnectionType = '66';
        premises.obsolete = '66';
        premises.marketable = '66';
        premises.gnafPersistentId = '66';
        premises.originalGnafPersistentId = '66';
        premises.serviceableLocationType = '66';
        premises.locationConnectionStatus = '66';
        premises.locationName = '66';
        premises.locationDescription = '66';
        premises.locationType = '66';
        premises.serviceLevelRegion = '66';
        premises.buildingType = '66';
        premises.propertyType = '66';
        premises.asaId = '66';
        premises.fsaId = '66';
        premises.mpsId = '66';
        premises.csaName = '66';
        premises.samId = '66';
        premises.daid = '66';
        premises.infillFlag = '66';
        premises.tlsInfillBoundary = '66';
        premises.infillRegionReadyFor = '66';
        premises.serviceContinuityRegion = '66';
        premises.polygonId = '66';
        premises.priId = '66';
        premises.mostrecentconnectionDate = '66';
        premises.poiId = '66';
        premises.poiName = '66';
        premises.transitionalPoiId = '66';
        premises.transitionalPoiName = '66';
        premises.transitionalCsaId = '66';
        premises.transitionalCsaName = '66';
        premises.landUse = '66';
        premises.landUseSubclass = '66';
        premises.newDevelopmentsChargeApplies = '66';
        premises.firstConnectionDate = '66';
        premises.firstDisconnectionDate = '66';
        premises.mostRecentDisconnectionDate = '66';
        premises.accessTechnologyType = '66';
        premises.accessSeekerId = '66';
        premises.isComplexPremise = '66';
        premises.orderId = '66';
        premises.orderStatus = '66';
        premises.orderDateReceived = '66';
        premises.bandwidthProfile = '66';
        premises.priorityAssist = '66';
        premises.deliveryPointIdentifier = '66';
        premises.isDeleted = '66';
        premises.sourceSystemC = '66';
        premises.sourceContributor = '66';
        premises.complexRoadNumber1 = '66';
        premises.complexRoadNumber2 = '66';
        premises.complexRoadName = '66';
        premises.complexRoadTypeCode = '66';
        premises.complexRoadSuffixCode = '66';
        premises.complexAddressString = '66';
        premises.listing_type = '66';
        
        LocationJSONResponseParsar.Aliases aliases = new LocationJSONResponseParsar.Aliases();
        aliases.fullText = '2009';
        aliases.levelType = 'THIRLMERE';
        aliases.levelNumber = 'IBBOTSON';
        aliases.unitType = 'ST';
        aliases.unitNumber = 'NSW';
        aliases.roadNumber1 = 'LOT 22 8 IBBOTSON ST THIRLMERE NSW 2573 Australia';
        aliases.roadName = '8';
        aliases.roadTypeCode = '66';
        aliases.locality = 'Australia';
        aliases.postcode = '66';
        aliases.state = 'Australia';
        
        LocationJSONResponseParsar.Data data = new LocationJSONResponseParsar.Data();
        data.type = '2009';
        data.id = 'THIRLMERE';
        
        LocationJSONResponseParsar.DataAttributes dataattr = new LocationJSONResponseParsar.DataAttributes();
        dataattr.rolloutRegionIdentifier = '2009';
        
        LocationJSONResponseParsar.DataDetails datadetails = new LocationJSONResponseParsar.DataDetails();
        datadetails.type = '2009';
        datadetails.id = 'THIRLMERE';
        
        LocationJSONResponseParsar.LocationDetailsMeta datameta = new LocationJSONResponseParsar.LocationDetailsMeta();
        datameta.totalMatchedResources = '2009';
        
        LocationJSONResponseParsar.Included include = new LocationJSONResponseParsar.Included();
        include.type = '2009';
        include.id = 'THIRLMERE';
        
        LocationJSONResponseParsar.AttributeMeta attmetas = new LocationJSONResponseParsar.AttributeMeta();
        attmetas.relevanceScore = '2009';
        
        LocationJSONResponseParsar.Principal pr = new LocationJSONResponseParsar.Principal();
        pr.levelType = '2009';
        pr.unitType = 'THIRLMERE';
    }
}