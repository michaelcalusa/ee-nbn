/***************************************************************************************************
    Class Name          : LocationAPIHelper
    Version             : 1.0 
    Created Date        : 18-Sep-2017
    Author              : Rupendra Kumar Vats
    Description         : Processing logic for LocationAPIController
                          Method - (A) getServiceEligibility - get address eligibility based on the location service class
                          Method - (B) getServerError - get the error details to return in any error condition
    Test Class          : LocationAPIController_Test
    
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats       18-Sep-2017       added method getServiceEligibility
****************************************************************************************************/ 
public class LocationAPIHelper{
    /* 
        Input Paramter : strServiceClass- Service Class returned by LAPI Address API
        Output Parameter : Service_Eligibility__mdt record for eligibility details
        Purpose : Get the address Eligibility
    */
    public Service_Eligibility__mdt getServiceEligibility(String strServiceClass){
        Service_Eligibility__mdt mdtSE;
        List<Service_Eligibility__mdt> lstServiceMD = [ SELECT Eligibility__c, Error_Message__c, Service_Class__c, Technology__c FROM Service_Eligibility__mdt WHERE Service_Class__c =: strServiceClass ];
        if(!lstServiceMD.isEmpty()){
            mdtSE = lstServiceMD[0];
        }
        system.debug('---mdtSE---' + mdtSE);
        return mdtSE;
    }
    
    /* 
        Input Paramter : strErrorCode- Error Code for the failure event
        Output Parameter : LightningComponentError__mdt record for user friendly error message details
        Purpose : Get the error details to return in any error condition
    */
    public LightningComponentError__mdt getServerError(String strErrorCode){
        LightningComponentError__mdt mdtLCE;
        List<LightningComponentError__mdt> lstLCErrorMD = [ SELECT ErrorCode__c, ErrorMessage__c, LightningComponent__c FROM LightningComponentError__mdt WHERE ErrorCode__c =: strErrorCode ];
        if(!lstLCErrorMD.isEmpty()){
            mdtLCE = lstLCErrorMD[0];
        }
        system.debug('---mdtLCE---' + mdtLCE);
        return mdtLCE;
    }
}