public class LocationDistanceJSONResponseParser {

    public class LocationDetailsRoot {
        public List<Data> data { get; set; }
        public Distance distance { get; set; }
        public List<Included> included { get; set; }
        public Meta meta { get; set; }
    }

    public class Distance {
        public String asset_id { get; set; }
        public String asset_status { get; set; }        
		public String asset_found { get; set; }
        public Decimal asset_distance { get; set; }
        public Decimal asset_lat { get; set; }
        public Decimal asset_long { get; set; }
        public String asset_owner { get; set; }
        public String asset_type_code { get; set; }
        public String object_name { get; set; }
    }
    
    public class Data {
        public String type { get; set; }
        public String id { get; set; }
        public DataAttributes attributes { get; set; }
        public List<DataRelationships> relationships { get; set; }
    }
    
    public class DataAttributes {
        public String rolloutRegionIdentifier { get; set; }
        public Premises premises { get; set; }
        public GeoPoint geopoint { get; set; }
    }
    
    public class Premises {
        public String accessSeekerId { get; set; }
        public String accessTechnologyType { get; set; }
        public String asaId { get; set; }
        public String bandwidthProfile { get; set; }
        public String buildingType { get; set; }
        public String complexAddressString { get; set; }
        public String complexRoadName { get; set; }      
        public String complexRoadNumber1 { get; set; }
        public String complexRoadNumber2 { get; set; }
        public String complexRoadSuffixCode { get; set; }
        public String complexRoadTypeCode { get; set; }
        public String csaId { get; set; }
        public String csaName { get; set; }        
        public String daid { get; set; } 
        public String deliveryPointIdentifier { get; set; } 
        public String disconnectionDate { get; set; } 
        public String disconnectionType { get; set; } 
        public String firstConnectionDate { get; set; } 
        public String firstDisconnectionDate { get; set; } 
        public String fsaId { get; set; } 
        public String gnafPersistentId { get; set; } 
        public Boolean infillFlag { get; set; } 
        public String infillRegionReadyFor { get; set; } 
        public String isComplexPremise { get; set; } 
        public String isDeleted { get; set; } 
        public Boolean isFrustrated { get; set; } 
        public Boolean isServiceable { get; set; }        
        public String landUse { get; set; } 
        public String landUseSubclass { get; set; } 
        public String listing_type { get; set; }        
        public String locationConnectionStatus { get; set; } 
        public String locationDescription { get; set; }        
        public String locationName { get; set; } 
        public String locationStatus { get; set; }        
        public String locationType { get; set; }        
        public Boolean marketable { get; set; }  
        public String mostRecentDisconnectionDate { get; set; }  
        public String mostrecentconnectionDate { get; set; }  
        public String mpsId { get; set; }  
        public String newDevelopmentsChargeApplies { get; set; }  
        public Boolean obsolete { get; set; }  
        public String orderDateReceived { get; set; }  
        public String orderId { get; set; }   
        public String orderStatus { get; set; } 
        public String originalGnafPersistentId { get; set; } 
        public String parcelId { get; set; } 
        public String poiId { get; set; } 
        public String poiName { get; set; } 
        public String polygonId { get; set; } 
        public Boolean premisesFlag { get; set; } 
        public String priId { get; set; }       
        public String primaryTechnology { get; set; }      
        public String priorityAssist { get; set; } 
        public String propertyType { get; set; } 
        public String readyForServiceDate { get; set; }      
        public String samId { get; set; } 
		public String secondaryComplexName { get; set; }   
        public String serviceClass { get; set; }    
        public String serviceClassDescription { get; set; } 
        public String serviceClassReason { get; set; } 
        public String serviceContinuityRegion { get; set; } 
        public String serviceLevelRegion { get; set; } 
        public String serviceType { get; set; } 
        public String serviceableLocationType { get; set; } 
        public String sourceContributor { get; set; } 
        public String sourceSystemC { get; set; } 
        public String technologyOverride { get; set; }          
        public String technologyType { get; set; }        
        public String tlsInfillBoundary { get; set; } 
        public String transitionalCsaId { get; set; } 
        public String transitionalCsaName { get; set; } 
        public String transitionalPoiId { get; set; } 
        public String transitionalPoiName { get; set; } 
    }
    
    public class DataRelationships {
        public DataAddress address { get; set; }
    }
    
    public class DataAddress {
        public List<DataDetails> data { get; set; }
    }
    
    public class DataDetails {
        public String id { get; set; }
        public String type { get; set; }
    }
    
    public class Included {
        public String id { get; set; }
        public String type { get; set; }
        public IncludedAttributes attributes { get; set; }
    }
    
    public class IncludedAttributes {
        public AttributeMeta meta { get; set; }
        public Principal principal { get; set; }
        public GeoPoint geopoint { get; set; }
        public List<Aliases> aliases { get; set; }
    }
    
    public class AttributeMeta {
        public Decimal relevanceScore { get; set; }
    }
    
    public class Principal {
        public String fullText { get; set; }
        public String levelNumber { get; set; }
        public String levelType { get; set; }
        public String locality { get; set; }
        public String postcode { get; set; }
        public String roadName { get; set; }
        public String roadNumber1 { get; set; }
		public String roadNumber2 { get; set; }
        public String roadTypeCode { get; set; }
        public String state { get; set; }
        public String unitNumber { get; set; }
        public String unitType { get; set; }
    }
    
    public class Aliases {
        public String fullText { get; set; }
        public String levelNumber { get; set; }
        public String levelType { get; set; }
        public String locality { get; set; }
        public String postcode { get; set; }
        public String roadName { get; set; }
        public String roadNumber1 { get; set; }
        public String roadNumber2 { get; set; }       
        public String roadTypeCode { get; set; }
        public String state { get; set; }
        public String unitNumber { get; set; }
        public String unitType { get; set; }
    }

    public class Meta {
        public Integer totalMatchedResources { get; set; }
        public List<Warnings> warnings { get; set; }
    }    
    
    public class Warnings {
        public String status { get; set; }
        public String code { get; set; }
        public String title { get; set; }
        public String detail { get; set; }        
    }
    
    public class GeoPoint {
        public Decimal latitude { get; set; }
        public Decimal longitude { get; set; }
    }
}