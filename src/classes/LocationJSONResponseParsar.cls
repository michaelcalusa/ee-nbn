/***************************************************************************************************
    Class Name          : LocationJSONResponseParsar
    Version             : 1.0 
    Created Date        : 14-Sep-2017
    Author              : Rupendra Kumar Vats
    Description         : JSON Response mapping of Location Addresses and Location API JSON response

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats       14-Sep-2017       JSON Response mapping of Location Addresses and Location API JSON response
****************************************************************************************************/ 
public class LocationJSONResponseParsar{
    
    /* Classes to parse JSON response for Addresses Location API */
    
    /* ****************** start here ************** */
    public class LocationRoot
    {
        public List<Location> data { get; set; }
        public Meta meta { get; set; }
    }
    
    public class Meta
    {
        public string totalPages { get; set; }
        public string totalMatchedResources { get; set; }
    }
    
    public class Location
    {
        public string type { get; set; }
        public string id { get; set; }
        public Relationships relationships { get; set; }
        public Attributes attributes { get; set; }
    }
    
    public class Attributes
    {
        public List<Alias> aliases { get; set; }
        public AttributesPrincipal principal { get; set; }
        public AttributesMeta meta { get; set; }
        public GeoPoint geopoint { get; set; }
    }
    
    public class Alias
    {
        public string postCode { get; set; }
        public string locality { get; set; }
        public string roadName { get; set; }
        public string roadTypeCode { get; set; }
        public string state { get; set; }
        public string fullText { get; set; }
        public string roadNumber1 { get; set; }
        public string lotNumber { get; set; }
        public string country { get; set; }
    }
    
    public class AttributesPrincipal
    {
        public string postCode { get; set; }
        public string locality { get; set; }
        public string roadName { get; set; }
        public string roadTypeCode { get; set; }
        public string state { get; set; }
        public string fullText { get; set; }
        public string roadNumber1 { get; set; }
        public string lotNumber { get; set; }
        public string country { get; set; }
    }
    
    public class AttributesMeta
    {
        public string aliasMatchIndex { get; set; }
        public string relevanceScore { get; set; }
    }
    
    public class GeoPoint
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
    }
    
    public class Relationships
    {
        public LocatedAt locatedAt { get; set; }
    }
    
    public class LocatedAt
    {
        public LocatedAtData data { get; set; }
    }
    
    public class LocatedAtData
    {
        public string type { get; set; }
        public string id { get; set; }
    }
    
    /* ****************** end here ************** */

    /* Classes to parse JSON response for Location API */
    
    /* ****************** start here ************** */
    public class LocationDetailsRoot
    {
        public List<Data> data { get; set; }
        public List<Included> included { get; set; }
        public Meta meta { get; set; }
    }
    
    public class Data
    {
        public string type { get; set; }
        public string id { get; set; }
        public DataAttributes attributes { get; set; }
        public List<DataRelationships> relationships { get; set; }
    }
    
    public class DataAttributes
    {
        public string rolloutRegionIdentifier { get; set; }
        public Premises premises { get; set; }
        public GeoPoint geopoint { get; set; }
    }
    
    public class Premises
    {
        public string disconnectionDate { get; set; }
        public string readyForServiceDate { get; set; }
        public string serviceClass { get; set; }
        public string isServiceable { get; set; }
        public string isFrustrated { get; set; }
        public string serviceType { get; set; }
        public string primaryTechnology { get; set; }
        public string technologyType { get; set; }
        public string csaId { get; set; }
        public string serviceClassDescription { get; set; }
        public string serviceClassReason { get; set; }
        public string technologyOverride { get; set; }
        public string premisesFlag { get; set; }
        public string disconnectionType { get; set; }
        public string obsolete { get; set; }
        public string marketable { get; set; }
        public string gnafPersistentId { get; set; }
        public string originalGnafPersistentId { get; set; }
        public string serviceableLocationType { get; set; }
        public string locationConnectionStatus { get; set; }
        public string locationName { get; set; }
        public string locationDescription { get; set; }
        public string locationType { get; set; }
        public string serviceLevelRegion { get; set; }
        public string buildingType { get; set; }
        public string propertyType { get; set; }
        public string asaId { get; set; }
        public string fsaId { get; set; }
        public string mpsId { get; set; }
        public string csaName { get; set; }
        public string samId { get; set; }
        public string daid { get; set; }
        public string infillFlag { get; set; }
        public string tlsInfillBoundary { get; set; }
        public string infillRegionReadyFor { get; set; }
        public string serviceContinuityRegion { get; set; }
        public string polygonId { get; set; }
        public string parcelId { get; set; }
        public string priId { get; set; }
        public string mostrecentconnectionDate { get; set; }
        public string poiId { get; set; }
        public string poiName { get; set; }
        public string transitionalPoiId { get; set; }
        public string transitionalPoiName { get; set; }
        public string transitionalCsaId { get; set; }
        public string transitionalCsaName { get; set; }
        public string landUse { get; set; }
        public string landUseSubclass { get; set; }
        public string newDevelopmentsChargeApplies { get; set; }
        public string firstConnectionDate { get; set; }
        public string firstDisconnectionDate { get; set; }
        public string mostRecentDisconnectionDate { get; set; }
        public string accessTechnologyType { get; set; }
        public string accessSeekerId { get; set; }
        public string isComplexPremise { get; set; }
        public string orderId { get; set; }
        public string orderStatus { get; set; }
        public string orderDateReceived { get; set; }
        public string bandwidthProfile { get; set; }
        public string priorityAssist { get; set; }
        public string deliveryPointIdentifier { get; set; }
        public string isDeleted { get; set; }
        public string sourceSystemC { get; set; }
        public string sourceContributor { get; set; }
        public string secondaryComplexName { get; set; }
        public string complexRoadNumber1 { get; set; }
        public string complexRoadNumber2 { get; set; }
        public string complexRoadName { get; set; }
        public string complexRoadTypeCode { get; set; }
        public string complexRoadSuffixCode { get; set; }
        public string complexAddressString { get; set; }
        public string listing_type { get; set; }
    }
    
    public class DataRelationships
    {
        public DataAddress address { get; set; }
    }
    
    public class DataAddress
    {
        public List<DataDetails> data { get; set; }
    }
    
    public class DataDetails
    {
        public string type { get; set; }
        public string id { get; set; }
    }
    
    public class LocationDetailsMeta
    {
        public string totalMatchedResources { get; set; }
    }
    
    public class Included
    {
        public string type { get; set; }
        public string id { get; set; }
        public IncludedAttributes attributes { get; set; }
    }
    
    public class IncludedAttributes
    {
        public AttributeMeta meta { get; set; }
        public Principal principal { get; set; }
        public GeoPoint geopoint { get; set; }
        public List<Aliases> aliases { get; set; }
    }
    
    public class AttributeMeta
    {
        public string relevanceScore { get; set; }
    }
    
    public class Principal
    {
        public string fullText { get; set; }
        public string levelType { get; set; }
        public string levelNumber { get; set; }
        public string unitType { get; set; }
        public string unitNumber { get; set; }
        public string roadNumber1 { get; set; }
        public string roadName { get; set; }
        public string roadTypeCode { get; set; }
        public string locality { get; set; }
        public string postcode { get; set; }
        public string state { get; set; }
    }
    
    public class Aliases
    {
        public string fullText { get; set; }
        public string levelType { get; set; }
        public string levelNumber { get; set; }
        public string unitType { get; set; }
        public string unitNumber { get; set; }
        public string roadNumber1 { get; set; }
        public string roadName { get; set; }
        public string roadTypeCode { get; set; }
        public string locality { get; set; }
        public string postcode { get; set; }
        public string state { get; set; }
    }
    
    public class PlacesAutocomplete
    {
    	public long timestamp { get; set; }
    	public string source { get; set; }
    	public List<PlacesSuggestions> suggestions { get; set; }
    	
    }	
    public class PlacesSuggestions
    {
    	public string id { get; set; }
    	public string formattedAddress { get; set; }
    	public Double latitude { get; set; }
    	public Double longitude { get; set; }
    	public string zoom { get; set; }
    }	
    
    /* ****************** end here ************** */
}