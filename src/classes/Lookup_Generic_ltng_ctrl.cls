public class Lookup_Generic_ltng_ctrl {

/***********************************************************************************
This controller is being used by the following Lightning components:

LKP_Generic_Main
LKP_Account_MHE_NewOpp

***********************************************************************************/

    @AuraEnabled
    public static List<sObject> getRows(String objectName, String[] selectList, String whereClause, String orderBy, Integer limitOfRecords) {
        String query = 'Select ';
        Integer j = 1;
        for (String i: selectList) {
            query = query + i;
            if(j<selectList.size())
            {
                query = query + ',';
                j=j+1;
            }
        }
        query = query + ' FROM ' + objectName;
        
        if(whereClause!='' && whereClause!=null)
            query += ' WHERE ' + whereClause;
            
        if(orderBy!='' && orderBy!=null)
            query += ' ORDER BY '+orderBy;
            
        if(limitOfRecords!=null)
            query+=' limit '+limitOfRecords;
        
        system.debug('QUERYYYYY'+query);
        
        return Database.query(query);
    }

        
    @AuraEnabled
    public static List<sObject> getContactRows(String objectName, String[] selectList, String whereClause, String orderBy, Integer limitOfRecords) {
        String conQuery = 'SELECT ' + selectList[0] + ' FROM ' + objectName + ' WHERE ' + whereClause + ' ORDER BY ' + orderBy + ' LIMIT ' + limitOfRecords;
        
        System.debug('conQuery ' + conQuery);
        return Database.query(conQuery);
    }
    
    @AuraEnabled
    public static List<String> getPickListValues(String object_name, String field_name) {
            
        List<String> options = new List<String>();
        
        List<Schema.PicklistEntry> ple = Schema.getGlobalDescribe().get(object_name).getDescribe().fields.getMap().get(field_name).getDescribe().getPickListValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            options.add(f.getValue());
        }

        return options;
    }
    
}