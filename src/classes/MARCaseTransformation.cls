/*------------------------------------------------------------
Author:        Dolly Yadav
Company:       Wipro 
Description:   To Create MAR Cases on the basis of LOC ID and Contact ID.
Test Class:    MARSiteTransformation_Test
------------------------------------------------------------
Authors Name    story       Description                         Date
Dolly Yadav     MSEU-7356   Make Rollout Group NOT mandatory  12/08/2017
Shubham Jaiswal MSEU-5631   Addition of new fields            18/12/2017
Dolly Yadav     MSEU-10164  Addition of Skybirdge fields      06/03/2018
Dolly Yadav     MSEU-10575  Null check for secondary contact  06/03/2018
Arpit Narain COMSTRUC-6991  Added filter for efficient        26/02/2019
                            pulling cases
-----------------------------------------------------------------------*/ 

public class MARCaseTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
    public String caseQuery;
    public string bjId;
    public String status = 'Completed';
    public Integer recordCount = 0;
    public Integer caseConvertCount = 0;
    public String exJobId;
    public String str = '';
    public String alarmOwner = 'Alarm Owner';
    public String preferredContact = 'Preferred Contact';
    Set<Id> MARCaseStageIdSet = new Set<Id>();
    
    /*public class GeneralException extends Exception
    {
    
    }*/
    public MarCaseTransformation(String ejId)
    {
        exJobId = ejId;
        Batch_Job__c bj = new Batch_Job__c();
        bj.Name = 'MARCaseCreation'+System.now().format(); // Extraction Job ID
        bj.Start_Time__c = System.now();
        bj.Extraction_Job_ID__c = exJobId;
        Database.SaveResult sResult = database.insert(bj,false);
        bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        caseQuery= 'select Case_status__c,Sub_Status__c,Rollout_Group__c,Primary_Contact__c,Secondary_Contact__c,Case_Type__c,Site_Id__c,Record_Provided_By__c,Date_Provided__c,Alarm_Type__c,Copper_Active__c,NBN_Active__c,Inflight_Order__c,ASP__c,Case_Concat__c,MASS_SUMMARY_STATUS__c,MASS_SUMMARY_INELIGIBILITY_REASON__c,ASP_ALARM_TECHNOLOGY__c,ASP_VILLAGE_NAME__c,ASP_VILLAGE_RESIDENT__c,Registered_in_Error__c,Service_Class__c,Case__c,MAR_Transformation_Status__c,Alarm_Brand__c,Alarm_Brand_if_other_brand__c,Who_does_the_alarm_call__c,EU_UMAP_Enquiry__c,Power_Outage_Message_Provided__c,UMA_Migration_Options_Provided__c,EU_Premises_Serviceability_Message_Provi__c,EU_UMAP_Enquiry_Timestamp__c,EU_Interest_in_UMAP__c,EU_Interest_in_UMAP_Timestamp__c,EU_Eligible__c,EU_Eligible_Timestamp__c,EU_Eligible_History__c,Reason_for_Ineligibility__c,Active_Escalation_with_nbn__c,Active_Escalation_with_nbn_Start_Timesta__c,Escalation_Notes__c,Informed_Consent_Signed__c,Consent_Timestamp__c,Successful_Installation_Validated__c,Successful_Installation_Validated_Timest__c,New_Device_Wireless_Only__c,Skybridge_UMA_Migration_Decision__c,Skybridge_UMA_Migration_Decision_Timesta__c,Skybridge_UMA_Migration_Decision_History__c from MAR_Case_Stage__c where MAR_Transformation_Status__c = \'Contact Uploaded Successfully\' order by Date_Provided__c'; 
        return Database.getQueryLocator(caseQuery);    
        
    }
    
    public void execute(Database.BatchableContext BC,List<MAR_Case_Stage__c> CaseStage)
    {       
        List<id> conidtest = new List<id>();
//        System.debug('--CaseStage--'+CaseStage);
        List<Case> MarCaseInsertList = new List<Case>();
        List<Case> MarCaseUpdateList = new List<Case>();
        Map<Id, Case> tempMarCaseUpdateMap = new Map<ID, Case>(); 
        Map<String, Case> tempMarCaseInsertMap = new Map<String, Case>(); 
        List<MAR_Case_Stage__c> marCaseStageUpdate = new List<MAR_Case_Stage__c>();
        Map<String,Case> CaseMap = new Map<String,Case> ();
//        Map<String,MAR_Case_Stage__c> CaseStageMap = new Map<String,MAR_Case_Stage__c> ();
        set<id> newcaseIDset = new set<id> () ;
        Map<String,ID> newcaseidMAP = new Map<String,ID> () ;
        List<MAR_Case_Stage__c> Newcasestageupdatelist = new List<MAR_Case_Stage__c>();
        Id marRecTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId(); 
//        System.debug('--marRecTypeId --'+marRecTypeId );

        List<String> caseContactIds = new List<String>();

        for (MAR_Case_Stage__c marCaseStage :CaseStage){
            caseContactIds.add(marCaseStage.Case_Concat__c);
        }

        for(Case cst : [SELECT Status,MAR_Sub_Status__c,Case_Concat__c,Rollout_Group__c,RecordTypeId,ContactId,
                Secondary_Contact__c,Site__c,Case_Type__c,EATLA__c,Record_Provided_By__c,Date_Provided__c,
                Alarm_Type__c,Copper_Active__c,NBN_Active__c,Inflight_Order__c,ASP__c,case.Id,
                MASS_SUMMARY_STATUS__c,MASS_SUMMARY_INELIGIBILITY_REASON__c,ASP_ALARM_TECHNOLOGY__c,
                ASP_VILLAGE_NAME__c,ASP_VILLAGE_RESIDENT__c,Primary_Contact_Role__c,Secondary_Contact_Role__c,
                Alarm_Brand__c,Alarm_Brand_if_other_brand__c,Who_does_the_alarm_call__c,EU_UMAP_Enquiry__c,
                Power_Outage_Message_Provided__c,UMA_Migration_Options_Provided__c,
                EU_Premises_Serviceability_Message_Provi__c,EU_UMAP_Enquiry_Timestamp__c,
                EU_Interest_in_UMAP__c,EU_Interest_in_UMAP_Timestamp__c,EU_Eligible__c,
                EU_Eligible_Timestamp__c,EU_Eligible_History__c,Reason_for_Ineligibility__c,
                Active_Escalation_with_nbn__c,Active_Escalation_with_nbn_Start_Timesta__c,
                Escalation_Notes__c,Informed_Consent_Signed__c,Consent_Timestamp__c,
                Successful_Installation_Validated__c,Successful_Installation_Validated_Timest__c,
                New_Device_Wireless_Only__c,Skybridge_UMA_Migration_Decision__c,
                Skybridge_UMA_Migration_Decision_Timesta__c,Skybridge_UMA_Migration_Decision_History__c
                FROM Case WHERE RecordType.DeveloperName ='MAR' AND Case_Concat__c != NULL AND Case_Concat__c IN :caseContactIds]){
            CaseMap.put(cst.Case_Concat__c,cst); // Query for the cases where the Case_Concat(Unique Field) is not NULL
//            System.debug ('existingCaseMap values'+CaseMap);
        }
        
        
        for(MAR_Case_Stage__c CaseSt : CaseStage)
        {
            
             // Check for the mandatory fields 
             MARCaseStageIdSet.add(CaseSt.id); // for reporting purpose
             
                /*if (CaseSt.Rollout_Group__c == Null ) {
                
                    str = 'Rollout Group is missing'+',' ;
                 
                }*/
                if (CaseSt.Site_ID__c == null ) {
                  
                   str = str+' Site ID is missing' +',' ;
                }
                if (CaseSt.Case_Type__c == null ) {
                  
                   str = str+' Case Type is missing' +',' ;
                }
                if (CaseSt.Record_Provided_By__c == null ) {
                  
                   str = str+' Record Provided is missing' +',' ;
                 }
                if (CaseSt.Date_Provided__c == null ) {
                  
                   str = str+' Date Provided is missing' +',' ;
                }
                if (CaseSt.Copper_Active__c == null ) {
                  
                   str = str+' Copper Active is missing' +',' ;
                                 
                }
                if (CaseSt.NBN_Active__c == null ) {
                  
                   str = str+' NBN Active is missing' +',' ;
                }
                if (CaseSt.Inflight_Order__c == null ) {
                  
                   str = str+' Inflight Order is missing' +',' ;
                }
            
                
            if(CaseMap.containsKey(CaseSt.Case_Concat__c) )
            {
               
                
                if(CaseSt.Site_ID__c != null && CaseSt.Case_Type__c != null && CaseSt.Record_Provided_By__c != null && CaseSt.Date_Provided__c != null && CaseSt.Copper_Active__c != null && CaseSt.NBN_Active__c != null && CaseSt.Inflight_Order__c != null){
                    Case existingCase = CaseMap.get(CaseSt.Case_Concat__c); //get Case values from Case map  
                    //existingCase.Status                     = CaseSt.Case_status__c;
                    //existingCase.MAR_Sub_Status__c          = CaseSt.Sub_Status__c;
                    existingCase.Rollout_Group__c           = CaseSt.Rollout_Group__c;
                    existingCase.RecordTypeID               = marRecTypeId;
                    existingCase.ContactID                  = CaseSt.Primary_Contact__c; 
                    // DOlly to update code for econdary  contacts Null check 
                    if (CaseSt.Secondary_Contact__c != null) {
                     existingCase.Secondary_Contact__c       = CaseSt.Secondary_Contact__c;
                    }
                    existingCase.Site__c                    = CaseSt.Site_ID__c;
                    existingCase.Case_Type__c               = CaseSt.Case_Type__c;
                    existingCase.Record_Provided_By__c      = CaseSt.Record_Provided_By__c;
                    existingCase.Date_Provided__c           = CaseSt.Date_Provided__c;
                   // existingCase.Alarm_Type__c              = CaseSt.Alarm_Type__c;
                    existingCase.Copper_Active__c           = CaseSt.Copper_Active__c;
                    existingCase.NBN_Active__c              = CaseSt.NBN_Active__c; 
                    existingCase.Inflight_Order__c          = CaseSt.Inflight_Order__c;
                    existingCase.ASP__c                     = CaseSt.ASP__c;
                    existingCase.Case_Concat__c             = CaseSt.Case_Concat__c;
                    existingCase.MASS_SUMMARY_STATUS__c     = CaseSt.MASS_SUMMARY_STATUS__c;
                    existingCase.MASS_SUMMARY_INELIGIBILITY_REASON__c          = CaseSt.MASS_SUMMARY_INELIGIBILITY_REASON__c;
                    existingCase.ASP_ALARM_TECHNOLOGY__c             = CaseSt.ASP_ALARM_TECHNOLOGY__c;
                    existingCase.ASP_VILLAGE_NAME__c             = CaseSt.ASP_VILLAGE_NAME__c;
                    existingCase.ASP_VILLAGE_RESIDENT__c             = CaseSt.ASP_VILLAGE_RESIDENT__c;
                    if (CaseSt.EU_UMAP_Enquiry__c != null) {
                        existingCase.EU_UMAP_Enquiry__c             = CaseSt.EU_UMAP_Enquiry__c;
                    }
                    
                    if (CaseSt.Power_Outage_Message_Provided__c != null) {
                        existingCase.Power_Outage_Message_Provided__c             = CaseSt.Power_Outage_Message_Provided__c;
                    }
                    
                    if (CaseSt.UMA_Migration_Options_Provided__c != null) {
                        existingCase.UMA_Migration_Options_Provided__c             = CaseSt.UMA_Migration_Options_Provided__c;
                    }  
                    
                    if (CaseSt.EU_Premises_Serviceability_Message_Provi__c != null) {
                        existingCase.EU_Premises_Serviceability_Message_Provi__c             = CaseSt.EU_Premises_Serviceability_Message_Provi__c;
                    }
                    
                    if (CaseSt.EU_UMAP_Enquiry_Timestamp__c != null) {
                        existingCase.EU_UMAP_Enquiry_Timestamp__c             = CaseSt.EU_UMAP_Enquiry_Timestamp__c;
                    } 
                    
                    if (CaseSt.EU_Interest_in_UMAP__c != null) {
                        existingCase.EU_Interest_in_UMAP__c             = CaseSt.EU_Interest_in_UMAP__c;
                    }   
                    
                    if (CaseSt.EU_Interest_in_UMAP_Timestamp__c != null) {
                        existingCase.EU_Interest_in_UMAP_Timestamp__c             = CaseSt.EU_Interest_in_UMAP_Timestamp__c;
                    }   
                    if (CaseSt.EU_Eligible__c != null) {
                        existingCase.EU_Eligible__c             = CaseSt.EU_Eligible__c;
                    } 
                    if (CaseSt.EU_Eligible_Timestamp__c != null) {
                        existingCase.EU_Eligible_Timestamp__c             = CaseSt.EU_Eligible_Timestamp__c;
                    } 
                    if (CaseSt.EU_Eligible_History__c != null) {
                        existingCase.EU_Eligible_History__c             = CaseSt.EU_Eligible_History__c;
                    }   
                    if (CaseSt.Reason_for_Ineligibility__c != null) {
                        existingCase.Reason_for_Ineligibility__c             = CaseSt.Reason_for_Ineligibility__c;
                    }   
                    if (CaseSt.Active_Escalation_with_nbn__c != null) {
                        existingCase.Active_Escalation_with_nbn__c             = CaseSt.Active_Escalation_with_nbn__c;
                    }   
                    if (CaseSt.Active_Escalation_with_nbn_Start_Timesta__c != null) {
                        existingCase.Active_Escalation_with_nbn_Start_Timesta__c             = CaseSt.Active_Escalation_with_nbn_Start_Timesta__c;
                    } 
                    if (CaseSt.Escalation_Notes__c != null) {
                        existingCase.Escalation_Notes__c             = CaseSt.Escalation_Notes__c;
                    }
                    if (CaseSt.Informed_Consent_Signed__c != null) {
                        existingCase.Informed_Consent_Signed__c             = CaseSt.Informed_Consent_Signed__c;
                    }  
                    if (CaseSt.Consent_Timestamp__c != null) {
                        existingCase.Consent_Timestamp__c             = CaseSt.Consent_Timestamp__c;
                    }
                    if (CaseSt.Successful_Installation_Validated__c != null) {
                        existingCase.Successful_Installation_Validated__c             = CaseSt.Successful_Installation_Validated__c;
                    }   
                    if (CaseSt.Successful_Installation_Validated_Timest__c != null) {
                        existingCase.Successful_Installation_Validated_Timest__c             = CaseSt.Successful_Installation_Validated_Timest__c;
                    }
                    if (CaseSt.New_Device_Wireless_Only__c != null) {
                        existingCase.New_Device_Wireless_Only__c             = CaseSt.New_Device_Wireless_Only__c;
                    }  
                    if (CaseSt.Skybridge_UMA_Migration_Decision__c != null) {
                        existingCase.Skybridge_UMA_Migration_Decision__c             = CaseSt.Skybridge_UMA_Migration_Decision__c;
                    }  
                    if (CaseSt.Skybridge_UMA_Migration_Decision_Timesta__c != null) {
                        existingCase.Skybridge_UMA_Migration_Decision_Timesta__c             = CaseSt.Skybridge_UMA_Migration_Decision_Timesta__c;
                    }   
                    if (CaseSt.Skybridge_UMA_Migration_Decision_History__c != null) {
                        existingCase.Skybridge_UMA_Migration_Decision_History__c             = CaseSt.Skybridge_UMA_Migration_Decision_History__c;
                    }   
                    
                    
                    if (existingCase.Primary_Contact_Role__c == null) { 
                        existingCase.Primary_Contact_Role__c             = alarmOwner;
                    } 
                    if (existingCase.Secondary_Contact_Role__c == null) { 
                        existingCase.Secondary_Contact_Role__c             = preferredContact;
                    }    
                    existingCase.EATLA__c = False;
                    
//                    system.debug('>>>>>>>>>>>>>>>>>>>BeforeexistingCase Close>>>>>>>>>>>>>>');
//                    system.debug('existingCase>>>>>>>'+existingCase.Status);
//                    system.debug('existingCase>>>>>>>'+existingCase.MAR_Sub_Status__c);
                    if (existingCase.Status != 'Closed') {
                        if (existingCase.Case_Type__c == 'Case Management (YTC)' && existingCase.NBN_Active__c== 'No' && existingCase.Copper_Active__c == 'Yes' && existingCase.Inflight_Order__c == 'No' && CaseSt.Registered_in_Error__c == '0' && (CaseSt.Service_Class__c== 3 || CaseSt.Service_Class__c== 13 || CaseSt.Service_Class__c== 24) ) {
                            existingCase.EATLA__c = True;
//                            system.debug ('----existingCase.EATLA__c---'+existingCase.EATLA__c);
                        } 
                    } else if (existingCase.Status == 'Closed' && (existingCase.MAR_Sub_Status__c !='Alarm no longer needed' && existingCase.MAR_Sub_Status__c != 'No alarm at premises' && existingCase.MAR_Sub_Status__c != 'Non-PSTN line' && existingCase.MAR_Sub_Status__c != 'Wireless alarm' && existingCase.MAR_Sub_Status__c != 'MASS completed' && existingCase.MAR_Sub_Status__c != 'Incorrect address/LOC ID' && existingCase.MAR_Sub_Status__c != 'Premises sold' && existingCase.MAR_Sub_Status__c != 'Alarm owner deceased')) 
                    {
//                        system.debug(' In closed Status and Substatus statement');
//                        system.debug('existingCase>>>>>>>'+existingCase.Status);
//                        system.debug('existingCase>>>>>>>'+existingCase.MAR_Sub_Status__c);
                        
                        if (existingCase.Case_Type__c == 'Case Management (YTC)' && existingCase.NBN_Active__c== 'No' && existingCase.Copper_Active__c == 'Yes' && existingCase.Inflight_Order__c == 'No' && CaseSt.Registered_in_Error__c == '0' && (CaseSt.Service_Class__c== 3 || CaseSt.Service_Class__c== 13 || CaseSt.Service_Class__c== 24) ) {
                            existingCase.EATLA__c = True;
                            existingCase.Status ='Re-Opened';
                            existingCase.MAR_Sub_Status__c = 'Not Yet Called';
                            
//                            system.debug ('----existingCase.EATLA__c---'+existingCase.EATLA__c);
                        } 
                        
                    } else {
                        
//                        System.debug('existingCase.Status and existingCase.MAR_Sub_Status__c'+existingCase.Status+existingCase.MAR_Sub_Status__c);
                    }
                    
//                    System.debug('--existingCase value--'+existingCase);
                    //MarCaseUpdateList.add(existingCase); 
                    tempMarCaseUpdateMap .put(existingCase.ID,existingCase) ;              
                    marCaseStageUpdate.add(CaseSt);
                    
//                    System.debug('*******if part of existing marCaseStageUpdate**'+marCaseStageUpdate);
                }
                else{
                    CaseSt.MAR_Transformation_Status__c = 'Errored';
                    CaseSt.Error_Message__c = 'The mandatory fields are missing : '+str ;
                    marCaseStageUpdate.add(CaseSt );
//                   System.debug('*******else part of existing marCaseStageUpdate**'+marCaseStageUpdate);
                }
            }           
            else 
            {
                                               
                //cs.RecordTypeID               = RecTypeId;                
              
                if(CaseSt.Site_ID__c != null && CaseSt.Case_Type__c != null && CaseSt.Record_Provided_By__c != null && CaseSt.Date_Provided__c != null && CaseSt.Copper_Active__c != null && CaseSt.NBN_Active__c != null && CaseSt.Inflight_Order__c != null){
                    Case newCase = new Case();
                    newCase.Status                     = CaseSt.Case_status__c;
                    newCase.MAR_Sub_Status__c          = CaseSt.Sub_Status__c;
                    newCase.Rollout_Group__c           = CaseSt.Rollout_Group__c;
                    newCase.RecordTypeID               = marRecTypeId;
                    newCase.ContactID              = CaseSt.Primary_Contact__c;    
                    newCase.Secondary_Contact__c       = CaseSt.Secondary_Contact__c;
                    newCase.Site__c                    = CaseSt.Site_ID__c;
                    newCase.Case_Type__c               = CaseSt.Case_Type__c;
                    newCase.Record_Provided_By__c      = CaseSt.Record_Provided_By__c;
                    newCase.Date_Provided__c           = CaseSt.Date_Provided__c;
                    newCase.Alarm_Type__c              = CaseSt.Alarm_Type__c;
                    newCase.Copper_Active__c           = CaseSt.Copper_Active__c;
                    newCase.NBN_Active__c              = CaseSt.NBN_Active__c; 
                    newCase.Inflight_Order__c          = CaseSt.Inflight_Order__c;
                    newCase.ASP__c                     = CaseSt.ASP__c;
                    newCase.Case_Concat__c             = CaseSt.Case_Concat__c;
                    newCase.MASS_SUMMARY_STATUS__c     = CaseSt.MASS_SUMMARY_STATUS__c;
                    newCase.MASS_SUMMARY_INELIGIBILITY_REASON__c          = CaseSt.MASS_SUMMARY_INELIGIBILITY_REASON__c;
                    newCase.ASP_ALARM_TECHNOLOGY__c             = CaseSt.ASP_ALARM_TECHNOLOGY__c;
                    newCase.ASP_VILLAGE_NAME__c             = CaseSt.ASP_VILLAGE_NAME__c;
                    newCase.ASP_VILLAGE_RESIDENT__c             = CaseSt.ASP_VILLAGE_RESIDENT__c;
                    newCase.Alarm_Brand__c             = CaseSt.Alarm_Brand__c;
                    newCase.Alarm_Brand_if_other_brand__c= CaseSt.Alarm_Brand_if_other_brand__c;
                    newCase.Who_does_the_alarm_call__c             = CaseSt.Who_does_the_alarm_call__c;
                    newCase.EU_UMAP_Enquiry__c             = CaseSt.EU_UMAP_Enquiry__c;
                    newCase.Power_Outage_Message_Provided__c             = CaseSt.Power_Outage_Message_Provided__c;
                    newCase.UMA_Migration_Options_Provided__c             = CaseSt.UMA_Migration_Options_Provided__c;
                    newCase.EU_Premises_Serviceability_Message_Provi__c             = CaseSt.EU_Premises_Serviceability_Message_Provi__c;
                    newCase.EU_UMAP_Enquiry_Timestamp__c             = CaseSt.EU_UMAP_Enquiry_Timestamp__c;
                    newCase.EU_Interest_in_UMAP__c             = CaseSt.EU_Interest_in_UMAP__c;
                    newCase.EU_Interest_in_UMAP_Timestamp__c             = CaseSt.EU_Interest_in_UMAP_Timestamp__c;
                    newCase.EU_Eligible__c             = CaseSt.EU_Eligible__c;
                    newCase.EU_Eligible_Timestamp__c             = CaseSt.EU_Eligible_Timestamp__c;
                    newCase.EU_Eligible_History__c             = CaseSt.EU_Eligible_History__c;
                    newCase.Reason_for_Ineligibility__c             = CaseSt.Reason_for_Ineligibility__c;
                    newCase.Active_Escalation_with_nbn__c             = CaseSt.Active_Escalation_with_nbn__c;
                    newCase.Active_Escalation_with_nbn_Start_Timesta__c             = CaseSt.Active_Escalation_with_nbn_Start_Timesta__c;
                    newCase.Escalation_Notes__c             = CaseSt.Escalation_Notes__c;
                    newCase.Informed_Consent_Signed__c             = CaseSt.Informed_Consent_Signed__c;
                    newCase.Consent_Timestamp__c             = CaseSt.Consent_Timestamp__c;
                    newCase.Successful_Installation_Validated__c             = CaseSt.Successful_Installation_Validated__c;
                    newCase.Successful_Installation_Validated_Timest__c             = CaseSt.Successful_Installation_Validated_Timest__c;
                    newCase.New_Device_Wireless_Only__c             = CaseSt.New_Device_Wireless_Only__c;
                    newCase.Skybridge_UMA_Migration_Decision__c             = CaseSt.Skybridge_UMA_Migration_Decision__c;
                    newCase.Skybridge_UMA_Migration_Decision_Timesta__c             = CaseSt.Skybridge_UMA_Migration_Decision_Timesta__c;
                    newCase.Skybridge_UMA_Migration_Decision_History__c             = CaseSt.Skybridge_UMA_Migration_Decision_History__c;
                    
                    if (newCase.Primary_Contact_Role__c == null) { 
                        newCase.Primary_Contact_Role__c         = alarmOwner;
                     } 
                    if (newCase.Secondary_Contact_Role__c == null) { 
                        newCase.Secondary_Contact_Role__c             = preferredContact;
                    } 
                                
                    
//                    System.debug('--newCase value--'+newCase);
                    if (newCase.Status != 'Closed') {
                    if (newCase.Case_Type__c == 'Case Management (YTC)' && newCase.NBN_Active__c== 'No' && newCase.Copper_Active__c == 'Yes' && newCase.Inflight_Order__c == 'No' && CaseSt.Registered_in_Error__c == '0' && (CaseSt.Service_Class__c== 3 || CaseSt.Service_Class__c== 13 || CaseSt.Service_Class__c== 24) ) {
                        newCase.EATLA__c = True;
//                        system.debug ('----newCase.EATLA__c---'+newCase.EATLA__c);
                        } 
                    }
                    
                    //MarCaseInsertList.add(newCase);  
                    tempMarCaseInsertMap.put(newCase.Case_Concat__c,newCase) ;  
                    conidtest.add(newCase.ContactId);
//                    System.debug('*******MAR_Transformation_Status__c**' +CaseSt.MAR_Transformation_Status__c);
                    marCaseStageUpdate.add(CaseSt );
//                    System.debug('*******if part of new marCaseStageUpdate**'+marCaseStageUpdate);
                } 
                else
                {
                    CaseSt.MAR_Transformation_Status__c = 'Errored';
                    CaseSt.Error_Message__c = 'The mandatory fields are missing : '+str ;
                    marCaseStageUpdate.add(CaseSt );
                    
//                    System.debug('*******else part of new marCaseStageUpdate**'+marCaseStageUpdate);
                
                  }
               }            
            
        } 

            //recordCount = recordCount + marCaseStageUpdate.size();
            list<Error_Logging__c> errCaseList = new List<Error_Logging__c>();
//            system.debug ('>>before MarCaseInsertList>>>> '+MarCaseInsertList);
            MarCaseInsertList=tempMarCaseInsertMap.values(); 
           
            if(MarCaseInsertList.size() > 0){
                //Schema.SObjectField f = Case.Fields.Id;
                Database.SaveResult[] srList = Database.insert(MarCaseInsertList,false);
//                system.debug('**********srList insert****'+srList);

                //list<Error_Logging__c> errCaseList = new List<Error_Logging__c>();
//                system.debug('**********errCaseList ****'+errCaseList);
               
                for (Database.SaveResult upsr : srList) {
                    if (!upsr.isSuccess()) {               
                        for(Database.Error err : upsr.getErrors()) {
                            errCaseList.add(new Error_Logging__c(Name='MAR - Case Insert Error',Error_Message__c=err.getMessage() + upsr.getId(),Fields_Afftected__c=String.valueof(err.getFields()),isSuccess__c=upsr.isSuccess(),Status_Code__c=String.valueof(err.getStatusCode()), isCreated__c= True ,Schedule_Job__c=bjId));
//                            system.debug ('>>>>>insert errCaseList>>>>'+errCaseList);
                        }
                    } else 
                    {
                        
                         newcaseIDset.add(upsr.getId());
                        
                    }    
                   
                }
            }
            
            MarCaseUpdateList=tempMarCaseUpdateMap.values();
            if(MarCaseUpdateList.size() > 0){
                //Schema.SObjectField f = Case.Fields.Id;
                Database.SaveResult[] srList = Database.update(MarCaseUpdateList,false);
//                system.debug('**********srList update ****'+srList);

                //list<Error_Logging__c> errCaseList = new List<Error_Logging__c>();
//                system.debug('**********errCaseList ****'+errCaseList);
                    for (Database.SaveResult upsr : srList) {
                        if (!upsr.isSuccess()) {               
                            for(Database.Error err : upsr.getErrors()) {
                                errCaseList.add(new Error_Logging__c(Name='MAR - Case Update Error',Error_Message__c=err.getMessage(),Fields_Afftected__c=String.valueof(err.getFields()),isSuccess__c=upsr.isSuccess(),Status_Code__c=String.valueof(err.getStatusCode()),isCreated__c= false,Schedule_Job__c=bjId));
//                                system.debug ('>>>>>Update errCaseList>>>>'+errCaseList);
                            }
                        } else 
                        {
                            
                             newcaseIDset.add(upsr.getId());
                            
                        }    
                    }
            }   

            if(errCaseList.size() > 0){
                insert errCaseList;
            }           
            
//         system.debug('newcaseIDset>>>>>>>>>'+newcaseIDset);
        for (case newCase : [Select Case_Concat__c,case.id from Case where ID IN : newcaseIDset] ) 
        {
            
            newcaseidMAP.put(newCase.Case_Concat__c, newCase.Id);
                                    
        }
//        system.debug('newcaseidMAP>>>>>>>>>'+newcaseidMAP);
//        system.debug('marCaseStageUpdate>>>>>>>>>'+marCaseStageUpdate);
        
        for (MAR_Case_Stage__c newMarcaseStage : marCaseStageUpdate) {
            
//            system.debug('newMarcaseStage.Case_Concat__c>>>>>>>>>'+newMarcaseStage.Case_Concat__c);
            
            if (newcaseidMAP.containsKey(newMarcaseStage.Case_Concat__c)) {
                
                newMarcaseStage.Case__c  = newcaseidMAP.get(newMarcaseStage.Case_Concat__c);
                newMarcaseStage.MAR_Transformation_Status__c = 'Completed';
                caseConvertCount = caseConvertCount + 1;
//                system.debug('newMarcaseStage>>>>>>'+newMarcaseStage);
                
            } 
           Newcasestageupdatelist.add(newMarcaseStage);
//           system.debug('Newcasestageupdatelist>>>>>>'+Newcasestageupdatelist);
            
        }
        recordCount = recordCount + marCaseStageUpdate.size();
        
             // MAR Case Stage DML statement
        if(Newcasestageupdatelist.size() > 0){
                   
            
            Database.SaveResult[] srList = Database.update(Newcasestageupdatelist, false);
            
//            System.debug ('while updating marcaseupdatelist ********'+srList );
            
            List<Error_Logging__c> errMARCaseStagage = new List<Error_Logging__c>();
            for (Database.SaveResult sr : srList){
                if (!sr.isSuccess()){ 
                    for(Database.Error err : sr.getErrors()){
                        errMARCaseStagage.add(new Error_Logging__c(Name='MAR - Case Stage Update Error After Contact' ,Error_Message__c=err.getMessage(),Fields_Afftected__c=String.valueof(err.getFields()),isSuccess__c=sr.isSuccess(),Status_Code__c=String.valueof(err.getStatusCode()),Schedule_Job__c=bjId));
                    }
                }
                 
            }  
            
            if(errMARCaseStagage.size() > 0){
                insert errMARCaseStagage;
            }
        }
           
      
    }            
    public void finish(Database.BatchableContext BC)
    {
        System.debug('MAR Case Batch Job Completed');
        
        AsyncApexJob caseJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        bj.Status__c=status;
        bj.Batch_Job_ID__c = caseJob.id;
        bj.Record_Count__c= recordCount;
        bj.End_Time__c  = caseJob.CompletedDate;
        upsert bj;
        
         // Processing Status Email logic
        
        List<MAR_Case_Stage__c> MARCaseStageList = [SELECT id,Name,MAR_Transformation_Status__c,Location_Id__c,Site_Id__r.Name,Primary_Contact__r.Name,Secondary_Contact__r.Name,Case_Type__c,Case__r.CaseNumber,Error_Message__c from MAR_Case_Stage__c where lastmodifieddate > :system.today()];
 
        String header = 'Excel Record, Processing Status , Location Id, Site Crated/Updated, Primary Contact Created/Updated, Secondary Contact Created/Updated, Case Type , Case Created/Updated, Processing Error Message\n';
        String finalstr = header ;
        for(MAR_Case_Stage__c a: MARCaseStageList)
        {
            string recordString = a.Name+','+a.MAR_Transformation_Status__c+','+a.Location_Id__c+','+a.Site_Id__r.Name+','+a.Primary_Contact__r.Name+','+a.Secondary_Contact__r.Name+','+a.Case_Type__c+','+a.Case__r.CaseNumber+','+a.Error_Message__c+'\n';
            finalstr = finalstr +recordString;
        }
        
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string csvname= 'MAR Case Upload Report.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        
        String MARReportId = '00O';
        String addresses;
        List<Report> MARReport = [SELECT id from Report where developername = 'MAR_Case_Report'];
        if(MARReport != null && MARReport.size() == 1)
        {
            MARReportId = String.valueof(MARReport[0].id);
        }
        if(System.Label.MAR_Report_Recipients != null){
            addresses = System.Label.MAR_Report_Recipients;
        }
        else{
            addresses = caseJob.CreatedBy.Email;
        }
        List<String> recipientList = addresses.split(';');
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = recipientList;
        mail.setToAddresses(toAddresses);
        mail.setSubject('MAR Cases Processing Complete');
        mail.setPlainTextBody
        ('File rows successfully converted to Case: ' + caseConvertCount + '.' + ' To view the detailed report of uploaded Cases, please click on the link : ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + MARReportId );
        mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }  
}