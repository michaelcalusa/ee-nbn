/*------------------------------------------------------------
Author:        Shubham Jaiswal
Company:       Wipro 
Description:   Create/Associate Contacts to MAR Case Stage Object...
Test Class:    MARSiteTransformation_Test
------------------------------------------------------------*/ 
public class MARContactCreation_Batch implements Database.Batchable<Sobject>, Database.Stateful
{
    public String queryMARCaseStageMAR;
    public String bjId;
    public String status = 'Completed';
    public Integer recordCount = 0;
    public String successCaseIds = '';
    public String failedCaseIds = '';
    public String schJobId;
    
    public MARContactCreation_Batch(String sjId)
    {
        schJobId = sjId;
        Batch_Job__c bj = new Batch_Job__c();
        bj.Name = 'MARContactCreation '+System.now().format();
        bj.Start_Time__c = System.now();
        bj.Extraction_Job_ID__c = sjId;
        bj.Type__c = 'MAR Contact Creation';
        Database.SaveResult sResult = database.insert(bj,false);
        bjId = sResult.getId();
        system.debug('@@@@@@@@@@@@@@sjId'+ sjId);
        system.debug('@@@@@@@@@@@@@@bjiD'+ bjId);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        queryMARCaseStageMAR = 'SELECT Id,Primary_Contact_Name__c,Primary_Contact_Email__c,Primary_Contact_Phone__c,Secondary_Contact_Name__c,Secondary_Contact_Phone__c,Secondary_Contact_Email__c,Case_Concat__c,Site_Id__c from MAR_Case_Stage__c WHERE MAR_Transformation_Status__c = \'Site Uploaded Successfully\''; 
        return Database.getQueryLocator(queryMARCaseStageMAR);    
    }
    
    public void execute(Database.BatchableContext BC,List<MAR_Case_Stage__c> queryMARCaseStage)
    {
        Set<String> contactNameSet = new Set<String>();
        Set<String> siteIdSet = new Set<String>();
        Map<String,ID>existingContactNameMap = new Map<String,ID>();
        List<Contact> existingContactList = new List<Contact>();
        Map<String,Contact> existingMARContactMap = new Map<String,Contact>();
        Map<String,Contact> existingNonMARContactMap = new Map<String,Contact>();
        Map<String,Contact> existingMARContactEmailPhoneMap = new Map<String,Contact>();
        Map<String,Contact> existingNonMARContactEmailPhoneMap = new Map<String,Contact>();
        List<Contact> upsertContactList = new List<Contact>();
        List<MAR_Case_Stage__c> marCaseStageUpdateList = new List<MAR_Case_Stage__c>();
        Map<String,Contact> duplicateContactCheckMap = new Map<String,Contact>();
        Set<Id> duplicateExistingContactSet = new Set<Id>();
        Map<String,ID> contactSiteMap = new Map<String,ID>();
        Map<ID,ID> contactMARSiteMap = new Map<ID,ID>();
        Id externalContactRecTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME);
        
        system.debug('@@@@@@@@@@@45' + queryMARCaseStage);
        
        for(MAR_Case_Stage__c cs: queryMARCaseStage)
        {
            if(cs.Primary_Contact_Name__c != null){ contactNameSet.add(cs.Primary_Contact_Name__c);}
            if(cs.Secondary_Contact_Name__c != null){ contactNameSet.add(cs.Secondary_Contact_Name__c);}
            if(cs.Site_Id__c != null){ siteIdSet.add(cs.Site_Id__c); }
        }
        
        system.debug('@@@@@@@@@@@52' + contactNameSet);
        
        for(Contact con : [Select id,Name,Email,Preferred_Phone__c,MAR__c from Contact where Name IN :contactNameSet AND RecordTypeId =:externalContactRecTypeId]){
            if(!(con.Email == null && con.Preferred_Phone__c == null)){
                if(con.Email != null && con.Preferred_Phone__c == null){
                    if(con.MAR__c == true){
                        existingMARContactMap.put(con.Name.toLowerCase() + con.Email,con);
                    }
                    else{
                        existingNonMARContactMap.put(con.Name.toLowerCase() + con.Email,con);
                    }
                }
                else if(con.Preferred_Phone__c != null && con.Email == null){
                    if(con.MAR__c == true){
                        existingMARContactMap.put(con.Name.toLowerCase() + con.Preferred_Phone__c.replaceAll('\\D',''),con);
                     }
                     else{
                         existingNonMARContactMap.put(con.Name.toLowerCase() + con.Preferred_Phone__c.replaceAll('\\D',''),con);
                     }
                }
                else if(con.Email != null && con.Preferred_Phone__c != null){
                    if(con.MAR__c == true){
                        existingMARContactEmailPhoneMap.put(con.Name.toLowerCase() + con.Email,con);
                        existingMARContactEmailPhoneMap.put(con.Name.toLowerCase() + con.Preferred_Phone__c.replaceAll('\\D',''),con);
                    }
                    else{
                        existingNonMARContactEmailPhoneMap.put(con.Name.toLowerCase() + con.Email,con);
                        existingNonMARContactEmailPhoneMap.put(con.Name.toLowerCase() + con.Preferred_Phone__c.replaceAll('\\D',''),con);
                    }
                }
                
                existingContactNameMap.put(con.Name.toLowerCase(),con.ID); 
            }
        }
        
        for(Site__c st : [SELECT id,(select id,Related_Contact__r.Name,Related_Contact__r.MAR__c from Premises_with_Contacts__r) from Site__c where id IN :siteIdSet]){
            if(st.Premises_with_Contacts__r != null){
                for(Site_Contacts__c sitCon :st.Premises_with_Contacts__r)
                {
                    if(existingContactNameMap.containsKey(sitCon.Related_Contact__r.Name.toLowerCase()) && sitCon.Related_Contact__r.MAR__c == true)
                    {
                        contactSiteMap.put(sitCon.Related_Contact__r.Name.toLowerCase(),st.id);
                    }
                    if(sitCon.Related_Contact__r.MAR__c == true)
                    {
                        contactMARSiteMap.put(st.Id,sitCon.Related_Contact__r.Id);
                    }
                }
            }
        }       
        
        //system.debug('@@@@@@@@@@@63' + existingContactMap);
        
        for(MAR_Case_Stage__c cs: queryMARCaseStage)
        {
            //Primary Contact,Secondary Contact & MAR Case Stage Objects
            Contact tempPrimCon = new Contact();
            tempPrimCon.MAR__c = true;
            tempPrimCon.RecordTypeId = externalContactRecTypeId;
            
            Contact tempSeconCon = new Contact();
            tempSeconCon.MAR__c = true;
            tempSeconCon.RecordTypeId = externalContactRecTypeId;
            
            MAR_Case_Stage__c tempMAR = new MAR_Case_Stage__c();
            tempMAR.Case_Concat__c = cs.Case_Concat__c;
            
            /*****************************************************Phone Validation Starts****************************************/
            //Primary Contact Phone Validation
            List<String> conPhone;
            if(cs.Primary_Contact_Phone__c != null){
                conPhone = cs.Primary_Contact_Phone__c.split(',',2);
                for(integer i=0; i< conPhone.size(); i++){
                    conPhone[i] = conPhone[i].replace('+61','0').replaceAll('\\D','').right(10);
                }
            } 
            if(conPhone != null && conPhone.size() == 1 && (Pattern.matches('[0-9]{10}|[0-9]{9}',conPhone[0]))){
                tempPrimCon.Preferred_Phone__c = (conPhone[0].length() == 10)?conPhone[0]:('0'+conPhone[0]);
            }
            else if(conPhone != null && conPhone.size() == 2){
                if(Pattern.matches('[0-9]{10}|[0-9]{9}',conPhone[0])){
                    tempPrimCon.Preferred_Phone__c = (conPhone[0].length() == 10)?conPhone[0]:('0'+conPhone[0]);
                    tempPrimCon.Alternate_Phone__c = conPhone[1];
                }
                else if(Pattern.matches('[0-9]{10}|[0-9]{9}',conPhone[1])){
                    tempPrimCon.Preferred_Phone__c = (conPhone[1].length() == 10)?conPhone[1]:('0'+conPhone[1]);
                }   
            }
            
            //Secondary Contact Phone Validation
            List<String> secConPhone;
            if(cs.Secondary_Contact_Phone__c != null){
                secConPhone = cs.Secondary_Contact_Phone__c.split(',',2);
                for(integer i=0; i< secConPhone.size(); i++){
                    secConPhone[i] = secConPhone[i].replace('+61','0').replaceAll('\\D','').right(10);
                }
            } 
            if(secConPhone != null && secConPhone.size() == 1 && (Pattern.matches('[0-9]{10}|[0-9]{9}',secConPhone[0]))){
                tempSeconCon.Preferred_Phone__c = (secConPhone[0].length() == 10)?secConPhone[0]:('0'+secConPhone[0]);
            }
            else if(secConPhone != null && secConPhone.size() == 2){
                if(Pattern.matches('[0-9]{10}|[0-9]{9}',secConPhone[0])){
                    tempSeconCon.Preferred_Phone__c = (secConPhone[0].length() == 10)?secConPhone[0]:('0'+secConPhone[0]);
                    tempSeconCon.Alternate_Phone__c = secConPhone[1];
                }
                else if(Pattern.matches('[0-9]{10}|[0-9]{9}',secConPhone[1])){
                    tempSeconCon.Preferred_Phone__c = (secConPhone[1].length() == 10)?secConPhone[1]:('0'+secConPhone[1]);
                }   
            }
            /***************************************************Phone Validation Ends**************************************************************/
            /***************************************************Primary Contact Creation Starts********************************************************/
            String contactEmailKey;
            String contactPhoneKey;
            
            if(cs.Primary_Contact_Email__c != null){
                tempPrimCon.Email = cs.Primary_Contact_Email__c;
                contactEmailKey = cs.Primary_Contact_Name__c.toLowerCase() +  cs.Primary_Contact_Email__c;
            } 
            
            if(tempPrimCon.Preferred_Phone__c != null){contactPhoneKey = cs.Primary_Contact_Name__c.toLowerCase() +  tempPrimCon.Preferred_Phone__c;}
            
            system.debug('##############' + contactEmailKey + '@@@@@@@@@@@@@@@@@@'+ contactPhoneKey);
           
            if(existingMARContactEmailPhoneMap.containsKey(contactEmailKey) || existingMARContactEmailPhoneMap.containsKey(contactPhoneKey) ||
                existingMARContactMap.containsKey(contactEmailKey) || existingMARContactMap.containsKey(contactPhoneKey) ||
                existingNonMARContactEmailPhoneMap.containsKey(contactEmailKey) || existingNonMARContactEmailPhoneMap.containsKey(contactPhoneKey) ||
                existingNonMARContactMap.containsKey(contactEmailKey) || existingNonMARContactMap.containsKey(contactPhoneKey))
            {
                system.debug('@@@@@@@ First if Entry' + contactEmailKey + '@@@@@@@@@@@@@@@@@@'+ contactPhoneKey);
                if(existingMARContactEmailPhoneMap.containsKey(contactEmailKey) || existingMARContactEmailPhoneMap.containsKey(contactPhoneKey)){
                    tempPrimCon.id = existingMARContactEmailPhoneMap.containsKey(contactEmailKey)?existingMARContactEmailPhoneMap.get(contactEmailKey).id:existingMARContactEmailPhoneMap.get(contactPhoneKey).id;
                }
                else if(existingMARContactMap.containsKey(contactEmailKey) || existingMARContactMap.containsKey(contactPhoneKey)){
                    tempPrimCon.id = existingMARContactMap.containsKey(contactEmailKey)?existingMARContactMap.get(contactEmailKey).id:existingMARContactMap.get(contactPhoneKey).id;
                }
                else if(existingNonMARContactEmailPhoneMap.containsKey(contactEmailKey) || existingNonMARContactEmailPhoneMap.containsKey(contactPhoneKey)){
                    tempPrimCon.id = existingNonMARContactEmailPhoneMap.containsKey(contactEmailKey)?existingNonMARContactEmailPhoneMap.get(contactEmailKey).id:existingNonMARContactEmailPhoneMap.get(contactPhoneKey).id;
                }                   
                else if(existingNonMARContactMap.containsKey(contactEmailKey) || existingNonMARContactMap.containsKey(contactPhoneKey)){
                    tempPrimCon.id = existingNonMARContactMap.containsKey(contactEmailKey)?existingNonMARContactMap.get(contactEmailKey).id:existingNonMARContactMap.get(contactPhoneKey).id;
                }
                
                if(tempPrimCon.id != null && !duplicateExistingContactSet.contains(tempPrimCon.id)){
                    upsertContactList.add(tempPrimCon);
                    duplicateExistingContactSet.add(tempPrimCon.id);
                }
                
                tempMAR.Primary_Contact__r = tempPrimCon;   
            }
            else if(contactEmailKey == null && contactPhoneKey == null && cs.Primary_Contact_Name__c != null && cs.Site_Id__c != null && contactSiteMap.containsKey(cs.Primary_Contact_Name__c.toLowerCase()) && contactSiteMap.get(cs.Primary_Contact_Name__c.toLowerCase()) == cs.Site_Id__c)
            {
                system.debug('@@@@@@@ Second if Entry' + contactEmailKey + '@@@@@@@@@@@@@@@@@@'+ contactPhoneKey);
                tempPrimCon.id = existingContactNameMap.get(cs.Primary_Contact_Name__c.toLowerCase());
                if(tempPrimCon.id != null && !duplicateExistingContactSet.contains(tempPrimCon.id)){
                    upsertContactList.add(tempPrimCon);
                    duplicateExistingContactSet.add(tempPrimCon.id);
                }
                
                tempMAR.Primary_Contact__r = tempPrimCon; 
            }
            else if(contactEmailKey == null && contactPhoneKey == null && cs.Primary_Contact_Name__c != null && cs.Site_Id__c != null && contactMARSiteMap.containsKey(cs.Site_Id__c))
            {
                system.debug('@@@@@@@ Third if Entry' + contactEmailKey + '@@@@@@@@@@@@@@@@@@'+ contactPhoneKey);
                tempPrimCon.id = contactMARSiteMap.get(cs.Site_Id__c);
                if(tempPrimCon.id != null && !duplicateExistingContactSet.contains(tempPrimCon.id)){
                    upsertContactList.add(tempPrimCon);
                    duplicateExistingContactSet.add(tempPrimCon.id);
                }
                
                tempMAR.Primary_Contact__r = tempPrimCon; 
            }
            else if(cs.Primary_Contact_Name__c != null)
            {   
                system.debug('@@@@@@@ Last if Entry' + contactEmailKey + '@@@@@@@@@@@@@@@@@@'+ contactPhoneKey );
                List<String> conFullName = cs.Primary_Contact_Name__c.split(' ',2);  
                
                if(conFullName.size() > 1 ){
                    tempPrimCon.FirstName = conFullName[0];
                    tempPrimCon.LastName = conFullName[1];
                }
                else{
                    tempPrimCon.LastName = conFullName[0];
                }
                
                system.debug('@@@@@@@' + tempPrimCon );
                if(tempPrimCon.LastName != null && (tempPrimCon.Email != null || tempPrimCon.Preferred_Phone__c != null)){
                    system.debug('@@@@@@@ inside con creation' );
                    if(!duplicateContactCheckMap.keySet().contains(cs.Primary_Contact_Name__c.toLowerCase() + tempPrimCon.Preferred_Phone__c) &&
                       !duplicateContactCheckMap.keySet().contains(cs.Primary_Contact_Name__c.toLowerCase() + cs.Primary_Contact_Email__c)) // Duplicate check in the same file.
                    {
                        upsertContactList.add(tempPrimCon);
                        if(tempPrimCon.Preferred_Phone__c != null){
                            duplicateContactCheckMap.put(cs.Primary_Contact_Name__c.toLowerCase() + tempPrimCon.Preferred_Phone__c,tempPrimCon);
                        }
                        if(cs.Primary_Contact_Email__c != null){
                            duplicateContactCheckMap.put(cs.Primary_Contact_Name__c.toLowerCase() + cs.Primary_Contact_Email__c,tempPrimCon);
                        }
                        
                        tempMAR.Primary_Contact__r = tempPrimCon;  
                    }
                    else{
                        tempMAR.Primary_Contact__r = (tempPrimCon.Email != null)? duplicateContactCheckMap.get(cs.Primary_Contact_Name__c.toLowerCase() + tempPrimCon.Email):duplicateContactCheckMap.get(cs.Primary_Contact_Name__c.toLowerCase() + tempPrimCon.Preferred_Phone__c); 
                    }
                }
            }
            /***************************************************Primary Contact Creation Ends********************************************************/
            /***************************************************Secondary Contact Creation Starts********************************************************/
            if(cs.Secondary_Contact_Name__c != null){
                String seconContactEmailKey;
                String seconContactPhoneKey;
                
                if(cs.Secondary_Contact_Email__c != null){
                    tempSeconCon.Email = cs.Secondary_Contact_Email__c;
                    seconContactEmailKey = cs.Secondary_Contact_Name__c.toLowerCase() +  cs.Secondary_Contact_Email__c;
                } 
                
                if(tempSeconCon.Preferred_Phone__c != null){seconContactPhoneKey = cs.Secondary_Contact_Name__c.toLowerCase() +  tempSeconCon.Preferred_Phone__c;}
                
                if(existingMARContactEmailPhoneMap.containsKey(seconContactEmailKey) || existingMARContactEmailPhoneMap.containsKey(seconContactPhoneKey) ||
                    existingMARContactMap.containsKey(seconContactEmailKey) || existingMARContactMap.containsKey(seconContactPhoneKey) ||
                    existingNonMARContactEmailPhoneMap.containsKey(seconContactEmailKey) || existingNonMARContactEmailPhoneMap.containsKey(seconContactPhoneKey) ||
                    existingNonMARContactMap.containsKey(seconContactEmailKey) || existingNonMARContactMap.containsKey(seconContactPhoneKey))
                {
                    if(existingMARContactEmailPhoneMap.containsKey(seconContactEmailKey) || existingMARContactEmailPhoneMap.containsKey(seconContactPhoneKey)){
                        tempSeconCon.id = existingMARContactEmailPhoneMap.containsKey(seconContactEmailKey)?existingMARContactEmailPhoneMap.get(seconContactEmailKey).id:existingMARContactEmailPhoneMap.get(seconContactPhoneKey).id;
                    }
                    else if(existingMARContactMap.containsKey(seconContactEmailKey) || existingMARContactMap.containsKey(seconContactPhoneKey)){
                        tempSeconCon.id = existingMARContactMap.containsKey(seconContactEmailKey)?existingMARContactMap.get(seconContactEmailKey).id:existingMARContactMap.get(seconContactPhoneKey).id;
                    }
                    else if(existingNonMARContactEmailPhoneMap.containsKey(seconContactEmailKey) || existingNonMARContactEmailPhoneMap.containsKey(seconContactPhoneKey)){
                        tempSeconCon.id = existingNonMARContactEmailPhoneMap.containsKey(seconContactEmailKey)?existingNonMARContactEmailPhoneMap.get(seconContactEmailKey).id:existingNonMARContactEmailPhoneMap.get(seconContactPhoneKey).id;
                    }                   
                    else if(existingNonMARContactMap.containsKey(seconContactEmailKey) || existingNonMARContactMap.containsKey(seconContactPhoneKey)){
                        tempSeconCon.id = existingNonMARContactMap.containsKey(seconContactEmailKey)?existingNonMARContactMap.get(seconContactEmailKey).id:existingNonMARContactMap.get(seconContactPhoneKey).id;
                    }
                    
                    if(tempSeconCon.id != null && !duplicateExistingContactSet.contains(tempSeconCon.id)){
                        upsertContactList.add(tempSeconCon);
                        duplicateExistingContactSet.add(tempSeconCon.id);
                    }
                    
                    tempMAR.Secondary_Contact__r = tempSeconCon;   
                }
                else if(seconContactEmailKey == null && seconContactPhoneKey == null && cs.Secondary_Contact_Name__c != null && cs.Site_Id__c != null && contactSiteMap.containsKey(cs.Secondary_Contact_Name__c.toLowerCase()) && contactSiteMap.get(cs.Secondary_Contact_Name__c.toLowerCase()) == cs.Site_Id__c)
                {
                    tempSeconCon.id = existingContactNameMap.get(cs.Secondary_Contact_Name__c.toLowerCase());
                    if(tempSeconCon.id != null && !duplicateExistingContactSet.contains(tempSeconCon.id)){
                        upsertContactList.add(tempSeconCon);
                        duplicateExistingContactSet.add(tempSeconCon.id);
                    }
                    
                    tempMAR.Secondary_Contact__r = tempSeconCon; 
                }
                else if(cs.Secondary_Contact_Name__c != null)
                {   
                    List<String> conFullName = cs.Secondary_Contact_Name__c.split(' ',2);  
                    
                    if(conFullName.size() > 1 ){
                        tempSeconCon.FirstName = conFullName[0];
                        tempSeconCon.LastName = conFullName[1];
                    }
                    else{
                        tempSeconCon.LastName = conFullName[0];
                    }
                    
                    if(tempSeconCon.LastName != null && (tempSeconCon.Email != null || tempSeconCon.Preferred_Phone__c != null)){
                        if(!duplicateContactCheckMap.keySet().contains(cs.Secondary_Contact_Name__c.toLowerCase() + tempSeconCon.Preferred_Phone__c) &&
                           !duplicateContactCheckMap.keySet().contains(cs.Secondary_Contact_Name__c.toLowerCase() + cs.Secondary_Contact_Email__c)) // Duplicate check in the same file.
                        {
                            upsertContactList.add(tempSeconCon);
                            if(tempSeconCon.Preferred_Phone__c != null){
                                duplicateContactCheckMap.put(cs.Secondary_Contact_Name__c.toLowerCase() + tempSeconCon.Preferred_Phone__c,tempSeconCon);
                            }
                            if(cs.Secondary_Contact_Email__c != null){
                                duplicateContactCheckMap.put(cs.Secondary_Contact_Name__c.toLowerCase() + cs.Secondary_Contact_Email__c,tempSeconCon);
                            }
                            
                            tempMAR.Secondary_Contact__r = tempSeconCon;  
                        }
                        else{
                            tempMAR.Secondary_Contact__r = (tempSeconCon.Email != null)? duplicateContactCheckMap.get(cs.Secondary_Contact_Name__c.toLowerCase() + tempSeconCon.Email):duplicateContactCheckMap.get(cs.Secondary_Contact_Name__c.toLowerCase() + tempSeconCon.Preferred_Phone__c); 
                        }
                    }
                }
            }
            /***************************************************Secondary Contact Creation Ends********************************************************/
            tempMAR.id = cs.id;
            marCaseStageUpdateList.add(tempMAR);
        }
        
        recordCount = recordCount + marCaseStageUpdateList.size();
        
        // Contact DML Statement
        if(upsertContactList.size() > 0){
            Schema.SObjectField f = Contact.Fields.Id;
            Database.UpsertResult[] upsrList = Database.upsert(upsertContactList,f,false);

            list<Error_Logging__c> errContactList = new List<Error_Logging__c>();
            for (Database.UpsertResult upsr : upsrList) {
                if (!upsr.isSuccess()) {               
                    for(Database.Error err : upsr.getErrors()) {
                        errContactList.add(new Error_Logging__c(Name='MAR - Contact Insert/Update Error',Error_Message__c=err.getMessage(),Fields_Afftected__c=String.valueof(err.getFields()),isSuccess__c=upsr.isSuccess(),Status_Code__c=String.valueof(err.getStatusCode()),isCreated__c= upsr.isCreated(),Schedule_Job__c=bjId));
                    }
                }
            }

            if(errContactList.size() > 0){
                insert errContactList;
            }
        }
        
        // MAR Case Stage DML statement
        if(marCaseStageUpdateList.size() > 0){
            
            for(MAR_Case_Stage__c mcs: marCaseStageUpdateList){
                
                if(mcs.Primary_Contact__r != null && mcs.Primary_Contact__r.id != null){
                    mcs.Primary_Contact__c = mcs.Primary_Contact__r.id;
                    mcs.Case_Concat__c = mcs.Case_Concat__c + String.valueOf(mcs.Primary_Contact__c);
                    mcs.MAR_Transformation_Status__c = 'Contact Uploaded Successfully';
                }
                else{
                    mcs.MAR_Transformation_Status__c = 'Errored';
                    mcs.Error_Message__c = 'Primary Contact Validation Failed or Technical Error Encountered. Please check either Preferred Phone OR Email is provided and Preferred Phone is a 10 digit number.';
                }
                
                if(mcs.Secondary_Contact__r != null && mcs.Secondary_Contact__r.id != null){
                    mcs.Secondary_Contact__c = mcs.Secondary_Contact__r.id;
                }
                else if(mcs.Secondary_Contact_Name__c != null){
                    mcs.Error_Message__c = (mcs.Error_Message__c != null)? mcs.Error_Message__c  + 'Secondary Contact Validation Failed or Technical Error Encountered. Please check either Preferred Phone OR Email is provided and Preferred Phone is a 10 digit number.' : 'Secondary Contact Validation Failed or Technical Error Encountered. Please check either Preferred Phone OR Email is provided and Preferred Phone is a 10 digit number.';
                }
            }
            
            Database.SaveResult[] srList = Database.update(marCaseStageUpdateList, false);
            
            List<Error_Logging__c> errMARCaseStagageList = new List<Error_Logging__c>();
            for (Database.SaveResult sr : srList){
                if (!sr.isSuccess()){ 
                    for(Database.Error err : sr.getErrors()){
                        errMARCaseStagageList.add(new Error_Logging__c(Name='MAR - Case Stage Update Error After Contact' ,Error_Message__c=err.getMessage(),Fields_Afftected__c=String.valueof(err.getFields()),isSuccess__c=sr.isSuccess(),Status_Code__c=String.valueof(err.getStatusCode()),Schedule_Job__c=bjId));
                    }
                }
            }  
            
            if(errMARCaseStagageList.size() > 0){
                insert errMARCaseStagageList;
            }
        }       
    }
    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('MAR Contact Batch Job Completed');
        system.debug('@@@@@@@@@@@@@@schJobId'+ schJobId);
        system.debug('@@@@@@@@@@@@@@bjiD'+ bjId);
        
        AsyncApexJob caseJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        bj.Status__c=status;
        bj.Batch_Job_ID__c = caseJob.id;
        bj.Record_Count__c= recordCount;
        bj.End_Time__c  = caseJob.CompletedDate;
        upsert bj;

        Database.executeBatch(new MarCaseTransformation(schJobId),Integer.valueOf(GlobalUtility.getMDConfiguration('SR_BR_Case_Creation_Batch_Size')));
    }    
    
}