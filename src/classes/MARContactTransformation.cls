/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class will be used to transform and transfer MAR-Contact Relationship records from Junction_Object_Int to 
MAR_Contact table
Modifications:
*/
public class MARContactTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
    public String junObjIntQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {            
    }
    public MARContactTransformation(String ejId)
    {
    	 exJobId = ejId;
    	 Batch_Job__c bj = new Batch_Job__c();
    	 bj.Name = 'MARContactTransform'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        junObjIntQuery = 'select Id,CRM_Record_Id__c,Object_Id__c,Child_Id__c,MAR_Contact_Role__c from Junction_Object_Int__c where Transformation_Status__c = \'Exported from CRM\' AND Object_Name__c = \'MAR\''; 
        return Database.getQueryLocator(junObjIntQuery);            
    }
    public void execute(Database.BatchableContext BC,List<Junction_Object_Int__c> listOfMARConInt)
    {        
      	try
        {            
            List<String> crmId = new List<String>();
            List<MAR_Contact__c> listOfMARCon = new List<MAR_Contact__c>();
            
            List<String> listOfMARIds = new List<String>();
			List<String> listOfConIds = new List<String>(); 
			Map<String, Id> mapListOfCon = new Map<String, Id>();
			Map<String, Id> mapListOfMAR = new Map<String, Id>();   
			
			for(Junction_Object_Int__c mari : listOfMARConInt)
            {                                
                listOfConIds.add(mari.Child_Id__c);     
                listOfMARIds.add(mari.Object_Id__c);
            }
            
            for(Contact con : [select On_Demand_Id_P2P__c, Id from Contact where On_Demand_Id_P2P__c in :listOfConIds FOR UPDATE])
            {
                mapListOfCon.put(con.On_Demand_Id_P2P__c, con.id);            
            }  
                 
            for(MAR__c mar : [select On_Demand_Id__c, Id from MAR__c where On_Demand_Id__c in :listOfMARIds FOR UPDATE])
            {
                mapListOfMAR.put(mar.On_Demand_Id__c, mar.id);            
            }
            
            map <String, MAR_Contact__c> marConMap = new map<String, MAR_Contact__c>();
            
            for(Junction_Object_Int__c marconInt : listOfMARConInt)
            {
                
                MAR_Contact__c marCon = new MAR_Contact__c();
                
                //crmId.add(marconInt.Id);       
                               
                //MAR__c mar = new MAR__c(On_Demand_Id__c = marconInt.Object_Id__c);
                //marCon.MAR__r = mar;
                marCon.MAR__c = mapListOfMAR.get(marconInt.Object_Id__c);
                
                //Contact c = new Contact(On_Demand_Id_P2P__c = marconInt.Child_Id__c);
                //marCon.Contact__r = c;     
                marCon.Contact__c = mapListOfCon.get(marconInt.Child_Id__c);    
               
                marCon.Alternate_Contact_Role__c = marconInt.MAR_Contact_Role__c; 
                marCon.MAR_Contact_Unique__c = marconInt.Object_Id__c + marconInt.Child_Id__c;
                
                //listOfMARCon.add(marCon);
                marConMap.put(marCon.MAR_Contact_Unique__c, marCon);

            }
            listOfMARCon = marConMap.values();
            List<Database.UpsertResult> upsertMARConResults = Database.upsert(listOfMARCon,MAR_Contact__c.Fields.MAR_Contact_Unique__c,false);
            List<Database.Error> upsertMARConError;
            String errorMessage, fieldsAffected;
      		String[] listOfAffectedFields;
            StatusCode sCode;
            List<Error_Logging__c> errMARConList = new List<Error_Logging__c>();
            List<Junction_Object_Int__c> updJunIntRes = new List<Junction_Object_Int__c>();
            for(Integer j=0; j<upsertMARConResults.size();j++)
            {
                //Junction_Object_Int__c updJunInt = new Junction_Object_Int__c();
                if(!upsertMARConResults[j].isSuccess())
                {
                    upsertMARConError = upsertMARConResults[j].getErrors();
                    for(Database.Error er: upsertMARConError)
                    {
                        sCode = er.getStatusCode();
                        errorMessage = er.getMessage();
                        listOfAffectedFields = er.getFields();
                    }
                    fieldsAffected = String.join(listOfAffectedFields,',');
                    errMARConList.add(new Error_Logging__c(Name='MAR Contact Intersection',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isSuccess__c=upsertMARConResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfMARConInt[j].Id,Schedule_Job__c=bjId));
                    listOfMARConInt[j].Transformation_Status__c = 'Error';
                    //listOfMARConInt[j].CRM_Record_Id__c = crmId[j];
                    listOfMARConInt[j].Schedule_Job_Transformation__c = bjId;
                    //listOfMARConInt[j].add(updJunInt);
                }
                else
                {
                    listOfMARConInt[j].Transformation_Status__c = 'Ready for Contact Update';
                    //listOfMARConInt[j].CRM_Record_Id__c = crmId[j];
                    listOfMARConInt[j].Schedule_Job_Transformation__c = bjId;
                                    
                }
            }
            Insert errMARConList;
            Update listOfMARConInt;     
            if(errMARConList.isEmpty())      
            {
            	status = 'Completed';
            } 
            else
            {
            	status = 'Error';
            }  
            recordCount += listOfMARConInt.size();           
        }
        catch(Exception e)
        {  
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='MAR Contact Transformation',Error_Message__c= e.getMessage()+' Line Number: '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob marConJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
       	{
       		bj.Status__c= marConJob.Status;
       	}
       	else
       	{
       		 bj.Status__c=status;
       	}
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = marConJob.id;
        bj.End_Time__c  = marConJob.CompletedDate;
        upsert bj;
        if (CRM_Transformation_Job__c.getinstance('MARContactUpdateContactMARFlag') <> null && CRM_Transformation_Job__c.getinstance('MARContactUpdateContactMARFlag').on_off__c)
   	    {
       		 Database.executeBatch(new MARContactUpdateContactMARFlag(exJobId));
        }
    }        
}