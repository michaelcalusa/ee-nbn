/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class will be used to update MAR Flag in Contact object
MAR_Contact table
Modifications:
*/
public class MARContactUpdateContactMARFlag implements Database.Batchable<Sobject>, Database.Stateful
{
    public String junObjIntQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {            
    }
    
    public MARContactUpdateContactMARFlag(String ejId)
    {
    	 exJobId = ejId;
    	 Batch_Job__c bj = new Batch_Job__c();
    	 bj.Name = 'MARContactUpdateContactMARFlag'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        junObjIntQuery = 'select Id,CRM_Record_Id__c,Object_Id__c,Child_Id__c,MAR_Contact_Role__c from Junction_Object_Int__c where Transformation_Status__c = \'Ready for Contact Update\' AND Object_Name__c = \'MAR\''; 
        return Database.getQueryLocator(junObjIntQuery);            
    }
    public void execute(Database.BatchableContext BC,List<Junction_Object_Int__c> listOfMARConInt)
    {        
      	try
        {                                    
            List<Contact> listOfCon = new List<Contact>();
            List<String> marId = new List<String>();
            for(Junction_Object_Int__c marconInt : listOfMARConInt)
            {
                marId.add(marconInt.Id);                
                Contact con = new Contact();
                con.On_Demand_Id_P2P__c = marconInt.Child_Id__c;
                con.MAR__c = true;                
                listOfCon.add(con);
            }            
            List<Database.UpsertResult> upsertMARConResults = Database.Upsert(listOfCon,Contact.Fields.On_Demand_Id_P2P__c,false);
            List<Database.Error> upsertMARConError;
            String errorMessage, fieldsAffected;
      		String[] listOfAffectedFields;
            StatusCode sCode;
            List<Error_Logging__c> errMARConList = new List<Error_Logging__c>();
            List<Junction_Object_Int__c> updJunIntRes = new List<Junction_Object_Int__c>();
            for(Integer j=0; j<upsertMARConResults.size();j++)
            {
                Junction_Object_Int__c updJunInt = new Junction_Object_Int__c();
                if(!upsertMARConResults[j].isSuccess())
                {
                    upsertMARConError = upsertMARConResults[j].getErrors();
                    for(Database.Error er: upsertMARConError)
                    {
                        sCode = er.getStatusCode();
                        errorMessage = er.getMessage();
                        listOfAffectedFields = er.getFields();
                    }
                    fieldsAffected = String.join(listOfAffectedFields,',');
                    errMARConList.add(new Error_Logging__c(Name='MAR Contact Intersection Transformation',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isSuccess__c=upsertMARConResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=marId[j],Schedule_Job__c=bjId));
                    updJunInt.Transformation_Status__c = 'Error';
                    updJunInt.Id = marId[j];
                    updJunInt.Schedule_Job_Transformation__c = bjId;
                    updJunIntRes.add(updJunInt);
                }
                else
                {
                    updJunInt.Transformation_Status__c = 'Copied to Base Table';
                    updJunInt.Id = marId[j];
                    updJunInt.Schedule_Job_Transformation__c = bjId;
                    updJunIntRes.add(updJunInt);                    
                }
            }
            Insert errMARConList;
            Update updJunIntRes;  
            if(errMARConList.isEmpty())      
            {
            	status = 'Completed';
            } 
            else
            {
            	status = 'Error';
            }  
            recordCount += listOfMARConInt.size();               
        }
        catch(Exception e)
        {            
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='MAR Contact Transformation',Error_Message__c= e.getMessage()+' Line Number: '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob marConJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
       	{
       		bj.Status__c= marConJob.Status;
       	}
       	else
       	{
       		 bj.Status__c=status;
       	}
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = marConJob.id;
        bj.End_Time__c  = marConJob.CompletedDate;
        upsert bj;
    }      
}