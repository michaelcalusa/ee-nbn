/*------------------------------------------------------------------------
Author:        Shuo Li	
Company:       NBN
Description:   A class created to manage trigger actions from the Mar From Staging object 
               Responsible for:
               1 - Publishing the Mar From Staging Event JIRA Tickets: 
Test Class:    MARSingleForm_Test
Created   :    24/10/2017    
History
<Date>            <Authors Name>    <Brief Description of Change> 
08/11/2018        Ganesh Sawant     MSEU-19383 - Error Logging in case Platform Event fails
--------------------------------------------------------------------------*/

public without sharing class MARFormTriggerHandler {
   
    public static final string MAR_PROCESS_STATUS = 'New';
    
    private List<MAR_Form_Staging__c> trgOldList = new List<MAR_Form_Staging__c> ();
    private List<MAR_Form_Staging__c> trgNewList = new List<MAR_Form_Staging__c> ();
    private Map<id,MAR_Form_Staging__c> trgOldMap = new Map<id,MAR_Form_Staging__c> ();
    private Map<id,MAR_Form_Staging__c> trgNewMap = new Map<id,MAR_Form_Staging__c> ();   
     
    
    public MARFormTriggerHandler(List<MAR_Form_Staging__c> trgOldList, List<MAR_Form_Staging__c> trgNewList, Map<id,MAR_Form_Staging__c> trgOldMap, Map<id,MAR_Form_Staging__c> trgNewMap){
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
    }
    
    public void OnAfterInsert()
    {
        if(!HelperUtility.isTriggerMethodExecutionDisabled('PublishMarFormEvent')){
            system.debug('OnAfterInsert - trgOldMap....'+trgNewMap); 
            PublishMarFormEvent(trgNewList, new Map<id,MAR_Form_Staging__c>());
        }
    }
    
    public void OnAfterUpdate()
    {
        if(!HelperUtility.isTriggerMethodExecutionDisabled('PublishMarFormEvent')){
            system.debug('OnAfterUpdate - trgOldMap....'+trgOldMap);             
            PublishMarFormEvent(trgNewList, trgOldMap);
        }
    }
     /*
    	Function to public event for new and updated status of Mar from staging records
    */
    public void PublishMarFormEvent(List<MAR_Form_Staging__c> trgNewList,Map<id,MAR_Form_Staging__c> trgOldMap)
    {  
    	list<MAR_Form_Staging__c>EventList = new list<MAR_Form_Staging__c>();
    	if (trgOldMap.isEmpty()) {
    		EventList.addAll(trgNewList);
    	} else {
    		for (MAR_Form_Staging__c mfs: trgNewList) {
				 if (mfs.status__c == MAR_PROCESS_STATUS) {
    			 	MAR_Form_Staging__c mfs_old = trgOldMap.get(mfs.id);
    			 	if (mfs_old.status__c != MAR_PROCESS_STATUS) {
    			 		EventList.add(mfs);
    			 	}
    			 	
				 }
    		}
    	}
    	
    	//Publish Event 
    	
	    List<MAR_Single_Form_Event__e> marFormEvents = new List<MAR_Single_Form_Event__e>();
		for (MAR_Form_Staging__c mfs: EventList) {
			 MAR_Single_Form_Event__e mfse = new MAR_Single_Form_Event__e();
			 mfse.Id__c = mfs.Id;
			 mfse.Name__c = mfs.name;			 
			 marFormEvents.add(mfse);			 
		}
		
		 system.debug('Event publish Size....'+marFormEvents.size());     
		// Call method to publish events
		List<Database.SaveResult> results = EventBus.publish(marFormEvents);
        //Error logging in case Platform Event fails to create MAR Registrations
        List<Error_Logging__c> errMARList = new List<Error_Logging__c>();		
		// Inspect publishing result for each event
		for (Database.SaveResult sr : results) {
			if (sr.isSuccess()) {
				System.debug('Successfully published event.');
			} else {
				for(Database.Error err : sr.getErrors()) {
					System.debug('Error returned: ' +err.getStatusCode() +' - ' +err.getMessage());
                    errMARList.add(new Error_Logging__c(Name='MAR Single Form - Platform Event Error' ,Error_Message__c=err.getMessage(),Fields_Afftected__c=String.valueof(err.getFields()),isSuccess__c=sr.isSuccess(),Status_Code__c=String.valueof(err.getStatusCode())));                    
				}
			}
		}
    	if(errMARList.size() > 0){
        	insert errMARList;
        }     	
    }
    
    
    
    
}