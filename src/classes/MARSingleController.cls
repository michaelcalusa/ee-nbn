public with sharing class MARSingleController {
    
    @AuraEnabled
    public static string saveMedAlarm(MAR_Form_Staging__c medicalAlarm) {
        // Perform isUpdatable() checking first, then
        upsert medicalAlarm;
        string recordId = medicalAlarm.id;
        string webFormID = [SELECT Name from MAR_Form_Staging__c WHERE id =: recordId].Name;
        return webFormID;
    }
    
    
    @AuraEnabled
    public static list<Map<string,string>> getStateOptions(){
        Schema.DescribeFieldResult fieldResult = MAR_Form_Staging__c.State__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<Map<string,string>> pickListValues = new List<Map<string,string>>();
        for( Schema.PicklistEntry f : ple){
            Map<string,string> entry = new Map<string, string>();
            entry.put(f.getLabel(), f.getValue());
            pickListValues.add(entry);
        }       
        return pickListValues;
    }
    
    @AuraEnabled
    public static list<Map<string,string>> getRoadTypes(){
        Schema.DescribeFieldResult fieldResult = MAR_Form_Staging__c.Road_Suffix__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<Map<string,string>> pickListValues = new List<Map<string,string>>();
        for( Schema.PicklistEntry f : ple){
            Map<string,string> entry = new Map<string, string>();
            entry.put(f.getLabel(), f.getValue());
            pickListValues.add(entry);
        }       
        return pickListValues;
    }
    
    @AuraEnabled
    public static list<Map<string,string>> getMedAlarmTypes(){
        Schema.DescribeFieldResult fieldResult = MAR_Form_Staging__c.MAR_Alarm_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<Map<string,string>> pickListValues = new List<Map<string,string>>();
        for( Schema.PicklistEntry f : ple){
            Map<string,string> entry = new Map<string, string>();
            entry.put(f.getLabel(), f.getValue());
            pickListValues.add(entry);
        }       
        return pickListValues;
    }
    
    @AuraEnabled
    public static list<Map<string,string>> getMedAlarmCall(){
        Schema.DescribeFieldResult fieldResult = MAR_Form_Staging__c.Who_Does_Alarm_Call__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<Map<string,string>> pickListValues = new List<Map<string,string>>();
        for( Schema.PicklistEntry f : ple){
            Map<string,string> entry = new Map<string, string>();
            entry.put(f.getLabel(), f.getValue());
            pickListValues.add(entry);
        }       
        return pickListValues;
    }
    
    @AuraEnabled
    public static list<Map<string,string>> getMedAlarmBrand(){
        Schema.DescribeFieldResult fieldResult = MAR_Form_Staging__c.MAR_Alarm_Brand__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<Map<string,string>> pickListValues = new List<Map<string,string>>();
        for( Schema.PicklistEntry f : ple){
            Map<string,string> entry = new Map<string, string>();
            entry.put(f.getLabel(), f.getValue());
            pickListValues.add(entry);
        }       
        return pickListValues;
    }
    
    @AuraEnabled
    public static list<Map<string,string>> getHearMethod(){
        Schema.DescribeFieldResult fieldResult = MAR_Form_Staging__c.How_did_you_hear__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        List<Map<string,string>> pickListValues = new List<Map<string,string>>();
        for( Schema.PicklistEntry f : ple){
            Map<string,string> entry = new Map<string, string>();
            entry.put(f.getLabel(), f.getValue());
            pickListValues.add(entry);
        }       
        return pickListValues;
    }
    

}