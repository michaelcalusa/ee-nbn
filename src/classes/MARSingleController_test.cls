@isTest
public class MARSingleController_test {

    public static testMethod void testSavedMethod(){
        integer MedAlarmNo = 50;
        
        Test.StartTest();
        for(integer index=0;index < MedAlarmNo; index++){
            MAR_Form_Staging__c newMed = new MAR_Form_Staging__c();
            string RecId = MARSingleController.saveMedAlarm(newMed);
            system.assertNotEquals(null, RecId);
        }
        Test.StopTest();
        //static controller requires no instantialization        
    }
    
    public static testMethod void testgetStateOptions(){
        
        Test.StartTest();

        List<Map<string,string>> stateOptions = MARSingleController.getStateOptions();
        system.assertNotEquals(null, stateOptions);
        
        Test.StopTest();      
    }
    
    public static testMethod void testgetRoadOptions(){
        
        Test.StartTest();
        
        List<Map<string,string>> RoadTypes = MARSingleController.getRoadTypes();
        system.assertNotEquals(null, RoadTypes);
        
        Test.StopTest();      
    }
    
    public static testMethod void testgetMedAlarmTypes(){
        
        Test.StartTest();
        
        List<Map<string,string>> AlarmTypes = MARSingleController.getMedAlarmTypes();
        system.assertNotEquals(null, AlarmTypes);
        
        Test.StopTest();       
    }
    
    public static testMethod void testgetMedAlarmCall(){
        
        Test.StartTest();
        
        List<Map<string,string>> AlarmCalls = MARSingleController.getMedAlarmCall();
        system.assertNotEquals(null, AlarmCalls);
        
        Test.StopTest();      
    }
    
    public static testMethod void testgetMedAlarmBrand(){
        
        Test.StartTest();
        
        List<Map<string,string>> AlarmBrands = MARSingleController.getMedAlarmBrand();
        system.assertNotEquals(null, AlarmBrands);
        
        Test.StopTest();       
    }
    
    public static testMethod void testgetHearMethod(){
        
        Test.StartTest();
        
        List<Map<string,string>> HearMethods = MARSingleController.getHearMethod();
        system.assertNotEquals(null, HearMethods);
        
        Test.StopTest();       
    }
    

    
    
    
}