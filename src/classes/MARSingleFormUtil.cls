/*------------------------------------------------------------------------
Author:        Shubham Jaiswal & Shuo Li    
Company:       NBN
Description:   util class for Mar Sigle form
               Responsible for:
               1 - Find/Create Contact based on new contact details
               2 - Find/Create Site based on site details
               3 - funtion to conver full state name to state abbr
               4 - functin to get Medical alarm type based on Who Does Alarm Call 
               
Test Class:    MARSingleForm_Test
Created   :    24/10/2017    
History
11/12/2017     Shuo Li          Remove the map between who does call and alarm type MSEU-7943
11/04/2018     Krishna Kanth    Changes made to accept unverified/unformatted Site Address from MAR form MSEU-11380  
--------------------------------------------------------------------------*/
public class MARSingleFormUtil{
  
    public static ContactWrap createExtractMARContact(ContactWrap conWrap){
        
        Id externalContactRecTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME);
        Contact tempCon = new Contact();
        tempCon.MAR__c = true;
        tempCon.RecordTypeId = externalContactRecTypeId;
        tempCon.FirstName = conWrap.firstName;
        tempCon.LastName = conWrap.lastName;
        if(conWrap.phone != null){
            tempCon.Preferred_Phone__c = conWrap.phone;
        }
        if(conWrap.email != null){
            tempCon.Email = conWrap.email;
        }
        if(conWrap.alternatePhone != null){
            tempCon.Alternate_Phone__c = conWrap.alternatePhone;
        }
       
        String fullName = tempCon.FirstName.toLowerCase() + ' ' + tempCon.LastName.toLowerCase();
        
        String contactEmailKey;
        String contactPhoneKey;
        
        if(tempCon.Email != null){
            contactEmailKey = fullName + tempCon.Email;
        } 
        
        if(tempCon.Preferred_Phone__c != null){contactPhoneKey = fullName +  tempCon.Preferred_Phone__c;}
        
        system.debug('##############' + contactEmailKey + '@@@@@@@@@@@@@@@@@@'+ contactPhoneKey);
       
        if( conWrap.existingMARContactEmailPhoneMap.containsKey(contactEmailKey) || conWrap.existingMARContactEmailPhoneMap.containsKey(contactPhoneKey) ||
            conWrap.existingMARContactMap.containsKey(contactEmailKey) || conWrap.existingMARContactMap.containsKey(contactPhoneKey) ||
            conWrap.existingNonMARContactEmailPhoneMap.containsKey(contactEmailKey) || conWrap.existingNonMARContactEmailPhoneMap.containsKey(contactPhoneKey) ||
            conWrap.existingNonMARContactMap.containsKey(contactEmailKey) || conWrap.existingNonMARContactMap.containsKey(contactPhoneKey))
        {
            system.debug('@@@@@@@ First if Entry' + contactEmailKey + '@@@@@@@@@@@@@@@@@@'+ contactPhoneKey);
            if(conWrap.existingMARContactEmailPhoneMap.containsKey(contactEmailKey) || conWrap.existingMARContactEmailPhoneMap.containsKey(contactPhoneKey)){
                tempCon.id = conWrap.existingMARContactEmailPhoneMap.containsKey(contactEmailKey)?conWrap.existingMARContactEmailPhoneMap.get(contactEmailKey).id:conWrap.existingMARContactEmailPhoneMap.get(contactPhoneKey).id;
            }
            else if(conWrap.existingMARContactMap.containsKey(contactEmailKey) || conWrap.existingMARContactMap.containsKey(contactPhoneKey)){
                tempCon.id = conWrap.existingMARContactMap.containsKey(contactEmailKey)?conWrap.existingMARContactMap.get(contactEmailKey).id:conWrap.existingMARContactMap.get(contactPhoneKey).id;
            }
            else if(conWrap.existingNonMARContactEmailPhoneMap.containsKey(contactEmailKey) || conWrap.existingNonMARContactEmailPhoneMap.containsKey(contactPhoneKey)){
                tempCon.id = conWrap.existingNonMARContactEmailPhoneMap.containsKey(contactEmailKey)?conWrap.existingNonMARContactEmailPhoneMap.get(contactEmailKey).id:conWrap.existingNonMARContactEmailPhoneMap.get(contactPhoneKey).id;
            }                   
            else if(conWrap.existingNonMARContactMap.containsKey(contactEmailKey) || conWrap.existingNonMARContactMap.containsKey(contactPhoneKey)){
                tempCon.id = conWrap.existingNonMARContactMap.containsKey(contactEmailKey)?conWrap.existingNonMARContactMap.get(contactEmailKey).id:conWrap.existingNonMARContactMap.get(contactPhoneKey).id;
            }
            
            if(tempCon.id != null && !conWrap.duplicateExistingContactSet.contains(tempCon.id)){
                conWrap.upsertContactList.add(tempCon);
                conWrap.duplicateExistingContactSet.add(tempCon.id);
            }else {
            	tempCon.OwnerId = (Id)Label.icrmprocessautomationuser;
            }
        }
        else
        {   
            system.debug('@@@@@@@ Last if Entry' + contactEmailKey + '@@@@@@@@@@@@@@@@@@'+ contactPhoneKey );
            
            system.debug('@@@@@@@' + tempCon );
            if(tempCon.Email != null || tempCon.Preferred_Phone__c != null){
                system.debug('@@@@@@@ inside con creation' );
                tempCon.OwnerId = (Id)Label.icrmprocessautomationuser;
                if(!conWrap.duplicateContactCheckMap.keySet().contains(fullName + tempCon.Preferred_Phone__c) &&
                   !conWrap.duplicateContactCheckMap.keySet().contains(fullName + tempCon.Email)) // Duplicate check in the same file.
                {
                    
                    conWrap.upsertContactList.add(tempCon);
                    if(tempCon.Preferred_Phone__c != null){
                        conWrap.duplicateContactCheckMap.put(fullName + tempCon.Preferred_Phone__c,tempCon);
                    }
                    if(tempCon.Email != null){
                        conWrap.duplicateContactCheckMap.put(fullName + tempCon.Email,tempCon);
                    }
                }
                else{
                    tempCon = (tempCon.Email != null)? conWrap.duplicateContactCheckMap.get(fullName + tempCon.Email):conWrap.duplicateContactCheckMap.get(fullName + tempCon.Preferred_Phone__c); 
                }
            }
        }
    
        conWrap.con = tempCon;
        return conWrap;

    }
    
    
    
     /*
        Functin to return Id maps based on the new address information maps
     */
     public static map<Id,Id> createExtractSite(map<id, AddressWrap>marFormAddressMaps){
         system.debug('marFormAddressMaps...'+ marFormAddressMaps);  
         
         set<Id>SiteRecordTypeIds = new set<id>();
         Map<String,Id>Recordtypemaps = HelperUtility.pullAllRecordTypes('Site__c');
         Id VerifiedRecordTypeid = Recordtypemaps.get('Verified');
         Id UnverifiedRecordTypeid = Recordtypemaps.get('Unverified');
         SiteRecordTypeIds.add(VerifiedRecordTypeid);
         SiteRecordTypeIds.add(UnverifiedRecordTypeid);
         
         
         
         map<Id,Id> marSingleFormSiteMaps = new map<Id,Id>();    
             
         set<string>locIds = new set<String>();      
         set<String>addresses = new set<String>();
         map<String,Id> locSiteMaps = new  map<String,Id> ();
         map<String,Id> addressSiteMaps = new  map<String,Id> ();
         
         map<String, set<Id>>unMatchedAddressNameMaps = new map<String, set<Id>>();
         set<Id>marFormIds = new set<Id>();
         map<String,AddressWrap>addressNameWrapMaps = new map<String,AddressWrap>();
         
         list<Site__c>newSiteList = new list<Site__c>();  
         list<Site__c>updateSiteList = new list<Site__c>();
         map<id, Site__c>updateSiteIdMaps = new map<id, Site__c>();

         for (AddressWrap aw:marFormAddressMaps.values()) {
            locIds.add(aw.LocId);
            addresses.add(aw.Address);
            addressNameWrapMaps.put(aw.Address, aw);
         }
          
         for (Site__c sit:[select id, Location_Id__c, Site_Name__c, Mar__c from Site__c where RecordTypeId in :SiteRecordTypeIds and ( (Location_Id__c  != null and Location_Id__c in :LocIds) or Name in :Addresses)]) {
              locSiteMaps.put(sit.Location_Id__c, sit.id);
              addressSiteMaps.put(sit.Site_Name__c, sit.id);
              updateSiteIdMaps.put(sit.id, sit);    
         }
         
         system.debug('Exiting Mapping for Loc...'+ locSiteMaps); 
         system.debug('Exiting Mapping for Address...'+ addressSiteMaps); 
         
         
          
         for (Id marSingleFormId: marFormAddressMaps.keyset()){
                AddressWrap add = marFormAddressMaps.get(marSingleFormId);
                if (locSiteMaps.containskey(add.LocId)) {
                    marSingleFormSiteMaps.put(marSingleFormId, locSiteMaps.get(add.LocId));
                    site__c site = updateSiteIdMaps.get(locSiteMaps.get(add.LocId));
                    system.debug('Existing site...'+ site); 
                    if (!site.Mar__c) {
                         site.Mar__c = true;
                         updateSiteList.add(site);
                    }
                }else if (addressSiteMaps.containskey(add.Address)){
                     marSingleFormSiteMaps.put(marSingleFormId, addressSiteMaps.get(add.Address));
                     site__c site = updateSiteIdMaps.get(addressSiteMaps.get(add.Address));
                     system.debug('Existing site...'+ site); 
                     if (!site.Mar__c) {
                         site.Mar__c = true;
                         updateSiteList.add(site);
                     }
                } else {
                     marFormIds = new set<Id>();
                     system.debug('AddressWrap Infor...'+ add);   
                     if (unMatchedAddressNameMaps.containsKey(add.Address)) {                        
                         marFormIds = unMatchedAddressNameMaps.get(add.Address); 
                         system.debug('Duplicate MarforAddress Found...'+ marFormIds);                      
                     } 
                     marFormIds.add(marSingleFormId);
                     unMatchedAddressNameMaps.put(add.Address, marFormIds);
                }
         }
         system.debug('Mapping Site found...'+ marSingleFormSiteMaps);
         system.debug('UnMapping Address based on Name...'+ unMatchedAddressNameMaps); 
         system.debug('Update list...'+ updateSiteList); 
         if (updateSiteList.size()>0) {
            update updateSiteList;
         }
         if (unMatchedAddressNameMaps.size()>0) {
             list<Error_Logging__c> errExcepList = new List<Error_Logging__c>();  
                 try{
                 for (String addressName: unMatchedAddressNameMaps.keyset()) {
                      AddressWrap add = addressNameWrapMaps.get(addressName);
                      list<String>splitAddress = add.Address.split(',');                
                      Site__c newSite = new Site__c();
                      String stateFull; 
                      //NBN Location Id, only update location id and Name with location Id
                      if(add.LocId != null && add.LocId.left(3) == 'LOC') {
                           newSite.Location_Id__c = add.locId;
                           newSite.name = add.locId; 
                           newSite.Site_Address__c = add.Address.left(250);
                        /*   if (splitAddress.size()>=3) {
                               stateFull= splitAddress.get(splitAddress.size()-1).trim();                          
                               newSite.Suburb__c = splitAddress.get(splitAddress.size()-2).trim();
                               newSite.Locality_Name__c = splitAddress.get(splitAddress.size()-2).trim();
                               splitAddress.remove(splitAddress.size()-1);
                               splitAddress.remove(splitAddress.size()-1);                         
                               newSite.Road_Name__c = String.valueof (splitAddress).replace('(','').Replace(')','').trim();
                           } */
                      } else {
                       //Changes made to accept unverified/unformatted Site Address from MAR form MSEU-11380                      
                        /*Google address, split full address and populate State, suburb and street.
                        if (splitAddress.size()>=3) {
                           stateFull= splitAddress.get(splitAddress.size()-2).trim();                          
                           newSite.Suburb__c = splitAddress.get(splitAddress.size()-3).trim();
                           newSite.Locality_Name__c = splitAddress.get(splitAddress.size()-3).trim();
                           splitAddress.remove(splitAddress.size()-1);
                           splitAddress.remove(splitAddress.size()-1);  
                           splitAddress.remove(splitAddress.size()-1);                       
                           newSite.Road_Name__c = String.valueof (splitAddress).replace('(','').Replace(')','').trim();
                           system.debug('State' + stateFull);
                           newSite.State_Territory_Code__c = getStageAbbr(stateFull);
                           newSite.name = add.Address.left(80);
                        } */
                       //Changes made to accept unverified/unformatted Site Address from MAR form MSEU-11380
                           newSite.name = add.Address.left(80);
                           newSite.Site_Address__c = add.Address.left(250);
                             
                      }
                      newSite.RecordTypeId = UnverifiedRecordTypeid;
                      newSite.Site_Name__c = add.Address.left(250);
                      newSite.Mar__c = true;
                      newSiteList.add(newSite);
                      system.debug('New Site' + newSite);
                 }
                 
                    insert newSiteList;  
                 
           
                 system.debug('New Sites Created...'+ newSiteList); 
                 
                 for (Site__c sit: NewSiteList) {
                      if (unMatchedAddressNameMaps.Containskey(sit.Site_Name__c)) 
                      {
                          marFormIds =  unMatchedAddressNameMaps.get(sit.Site_Name__c);
                          for (Id marFormId: marFormIds){
                                marSingleFormSiteMaps.put(marFormId, sit.Id);
                          }
                      }
                 }
             }
             catch(Exception e) {
             	errExcepList.add(new Error_Logging__c(Name='MAR Single Form - Site Creation',Error_Message__c=e.getMessage()));                    
             }
                 
             if(!errExcepList.isEmpty()){
             	insert errExcepList;
             }  
         }
          system.debug('Result Maps...'+ marSingleFormSiteMaps); 
         return marSingleFormSiteMaps;      

    }
    /*
        funtion to conver full state name to state abbr
    
    public static string getStageAbbr (String state) {
        Map<String, StateAbbrMapping__c> StateMap = StateAbbrMapping__c.getAll();
        system.debug(StateMap.get(state));
        if (StateMap.containskey(state)) {
            return StateMap.get(state).abbr__c;
        } else {
            return state;
        }
        
    } */
    
    /* 
     functin to get Medical alarm type based on Who Does Alarm Call 
    */
    /* public static string getAlarmType (String call) {
        Map<String, AlarmTypeMapping__c> MarTypeMap = AlarmTypeMapping__c.getAll();
        system.debug(MarTypeMap.get(call.left(36).trim()));
        if (MarTypeMap.containskey(call.left(36).trim())) {
            return MarTypeMap.get(call.left(36).trim()).Alarm_Type__c;
        } else {
            return null;
        }
        
    }
    */
    
    
    public class ContactWrap {
        String firstName;
        String lastName;
        String phone;
        String email;
        String alternatePhone;   
        public Contact con;
        public Set<Id> duplicateExistingContactSet;
        public List<Contact> upsertContactList;
        public Map<String,Contact> duplicateContactCheckMap;
        public Map<String,Contact> existingMARContactMap;
        public Map<String,Contact> existingNonMARContactMap;
        public Map<String,Contact> existingMARContactEmailPhoneMap;
        public Map<String,Contact> existingNonMARContactEmailPhoneMap;
        
        public ContactWrap(String FirstName,String LastName, String Phone,String Email,String AlternatePhone,Set<Id> DuplicateExistingContactSet,Map<String,Contact> DuplicateContactCheckMap,Map<String,Contact> ExistingMARContactMap,Map<String,Contact> ExistingNonMARContactMap,Map<String,Contact> ExistingMARContactEmailPhoneMap,Map<String,Contact> ExistingNonMARContactEmailPhoneMap,List<Contact> UpsertContactList){
            this.firstName = FirstName; 
            this.lastName = LastName;   
            this.phone = Phone;
            this.email = Email;
            this.alternatePhone = AlternatePhone;   
            this.duplicateExistingContactSet = DuplicateExistingContactSet;
            this.duplicateContactCheckMap = DuplicateContactCheckMap;
            this.existingMARContactMap = ExistingMARContactMap;
            this.existingNonMARContactMap = existingNonMARContactMap;
            this.existingMARContactEmailPhoneMap = existingMARContactEmailPhoneMap;
            this.existingNonMARContactEmailPhoneMap = existingNonMARContactEmailPhoneMap;
            this.upsertContactList = UpsertContactList;
        }
    }
    
    public  class AddressWrap {
       public String LocId;
       public String Address;      
    }
    
}