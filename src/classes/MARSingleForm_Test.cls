@isTest
private class MARSingleForm_Test {
/*------------------------------------------------------------
Author:        Shubham Jaiswal
Company:       Wipro
Description:   Test class for MAR Single Form
History
11/04/2018     Krishna Kanth    Changes made to accept unverified/unformatted Site Address from MAR form MSEU-11380  
------------------------------------------------------------*/    
    @isTest static void createMARSingleForm(){
        List<Contact> conrec = new List<Contact>();
        // Create Contact record
        ID conrecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId();
        Contact contactRec = new Contact ();
        contactRec.FirstName = 'Test Alt First';
        contactRec.LastName = 'Test Alt Last';
        contactRec.Email = 'test@test.com';
        contactRec.Preferred_Phone__c = '0422132123';
        contactRec.recordtypeid = conrecordTypeId;
        conrec.add(contactRec);
        
        Contact contactRec1 = new Contact ();
        contactRec1.FirstName = 'Test First';
        contactRec1.LastName = 'Test Last';
        contactRec1.Email = 't@t.com';
        contactRec1.recordtypeid = conrecordTypeId;
        conrec.add(contactRec1);
        
        Contact contactRec2 = new Contact ();
        contactRec2.FirstName = 'Test MAR First';
        contactRec2.LastName = 'Test MAR Last';
        contactRec2.Email = 'testing@testing.com';
        contactRec2.recordtypeid = conrecordTypeId;
        contactRec2.MAR__c = true;
        conrec.add(contactRec2);
        
        Contact contactRec3 = new Contact ();
        contactRec3.FirstName = 'Test Event First';
        contactRec3.LastName = 'Test Event Last';
        contactRec3.Preferred_Phone__c = '0456744445';
        contactRec3.recordtypeid = conrecordTypeId;
        contactRec3.MAR__c = true;
        conrec.add(contactRec3);
        
        Contact contactRec4 = new Contact ();
        contactRec4.FirstName = 'Test Trigger First';
        contactRec4.LastName = 'Test Trigger Last';
        contactRec4.Email = 'tt@tt.org';
        contactRec4.recordtypeid = conrecordTypeId;
        contactRec4.MAR__c = true;
        conrec.add(contactRec4);

        insert conrec;
        
        ID siterecordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();       
        Site__c SiteRec = new Site__c();
        SiteRec.Name = '300 Pitt street, Sydney, NSW 2000';
        SiteRec.recordtypeid = siterecordTypeId;
        SiteRec.Site_Address__c = '300 Pitt street, Sydney, NSW 2000';
        insert SiteRec;
        
        List<MAR_Form_Staging__c> mfslist = new List<MAR_Form_Staging__c>();
        
        MAR_Form_Staging__c mfs = new MAR_Form_Staging__c();
        mfs.Additional_Information_Text__c = 'Test';
        mfs.Additional_Phone_Number__c =  '0456789212';
       
        mfs.Alt_Additional_Phone_Number__c = '0456323234';
        mfs.Alt_Email_Address__c = 'test@test.com';
        mfs.Alt_First_Name__c = 'Test Alt First';
        mfs.Alt_Last_Name__c = 'Test Alt Last';
        mfs.Alt_Phone_Number__c = null;
        mfs.Email_Address__c = 'test1@test.com';
        mfs.First_Name__c = 'Test First';
        mfs.How_did_you_hear__c = 'Word of Mouth';
        mfs.Last_Name__c = 'Test Last';      
        mfs.Location_ID__c = 'LOC000074735774';     
        mfs.MAR_Alarm_Brand__c = 'Lifelink';
        mfs.MAR_Alarm_Brand_Other__c = 'Test Band';
        mfs.MAR_Alarm_Type__c = 'Professionally monitored alarm';
        mfs.Phone_Number__c = '0422341231';     
        mfs.Site_Name__c = '123 Beachfields Dr, Abbey, WA';     
        mfs.Status__c = 'New';      
        mfs.Who_Does_Alarm_Call__c = 'Emergency services - 000';
        mfslist.add(mfs);
        
        MAR_Form_Staging__c mfs1 = new MAR_Form_Staging__c();
        mfs1.Additional_Information_Text__c = 'Test1';
        mfs1.Additional_Phone_Number__c =  '0456729212';
        mfs1.Alt_Additional_Phone_Number__c = '0456123234';
        mfs1.Alt_Email_Address__c = 'test4@test.com';
        mfs1.Alt_First_Name__c = 'Test Alt3 First';
        mfs1.Alt_Last_Name__c = 'Test Alt3 Last';
        mfs1.Alt_Phone_Number__c = null;
        mfs1.Email_Address__c = 'test1@test.com';
        mfs1.First_Name__c = 'Test1 First';
        mfs.How_did_you_hear__c = 'Word of Mouth';
        mfs1.Last_Name__c = 'Test Last1';      
//Changes made to accept unverified/unformatted Site Address from MAR form MSEU-11380
        //mfs1.Location_ID__c = '21321244241';     
        mfs1.MAR_Alarm_Brand__c = 'Lifelink';
        mfs1.MAR_Alarm_Brand_Other__c = 'Test Band';
        mfs1.MAR_Alarm_Type__c = 'Professionally monitored alarm';
        mfs1.Phone_Number__c = '0422344231';     
        mfs1.Site_Name__c = '300 Pitt street, Sydney, NSW 2000';     
        mfs1.Status__c = 'New';      
        mfs1.Who_Does_Alarm_Call__c = 'Emergency services - 000';
        mfslist.add(mfs1); 
        
        MAR_Form_Staging__c mfs2 = new MAR_Form_Staging__c();
        mfs2.Email_Address__c = 'testing@testing.com';
        mfs2.First_Name__c = 'Test MAR First';
        mfs2.How_did_you_hear__c = 'Word of Mouth';
        mfs2.Last_Name__c = 'Test MAR Last';      
        mfs2.MAR_Alarm_Brand__c = 'Lifelink';
        mfs2.MAR_Alarm_Brand_Other__c = 'Test Band';
        mfs2.MAR_Alarm_Type__c = 'Professionally monitored alarm';  
        mfs2.Site_Name__c = '300 Market street, Sydney, NSW 2000';     
        mfs2.Status__c = 'New';      
        mfs2.Who_Does_Alarm_Call__c = 'Emergency services - 000';
        mfslist.add(mfs2); 
        
        MAR_Form_Staging__c mfs3 = new MAR_Form_Staging__c();
        mfs3.Phone_Number__c = '0456744445'; 
        mfs3.First_Name__c = 'Test Event First';
        mfs3.How_did_you_hear__c = 'Word of Mouth';
        mfs3.Last_Name__c = 'Test Event Last';      
        mfs3.MAR_Alarm_Brand__c = 'Lifelink';
        mfs3.MAR_Alarm_Brand_Other__c = 'Test Band';
        mfs3.MAR_Alarm_Type__c = 'Professionally monitored alarm';  
        mfs3.Site_Name__c = '300 Elizabeth street, Sydney, NSW 2000';     
        mfs3.Status__c = 'New';      
        mfs3.Who_Does_Alarm_Call__c = 'Emergency services - 000';
        mfslist.add(mfs3);
        
        MAR_Form_Staging__c mfs4 = new MAR_Form_Staging__c();
        mfs4.Email_Address__c = 'tt@tt.org';
        mfs4.First_Name__c = 'Test Trigger First';
        mfs4.How_did_you_hear__c = 'Word of Mouth';
        mfs4.Last_Name__c = 'Test Trigger Last';      
        mfs4.MAR_Alarm_Brand__c = 'Lifelink';
        mfs4.MAR_Alarm_Brand_Other__c = 'Test Band';
        mfs4.MAR_Alarm_Type__c = 'Professionally monitored alarm';  
        mfs4.Site_Name__c = '300 Taylor street, Sydney, NSW 2000';     
        mfs4.Status__c = 'New';      
        mfs4.Who_Does_Alarm_Call__c = 'Emergency services - 000';
        mfslist.add(mfs4);
        
        
        insert mfslist;
        
       // update mfslist;
        
        // Creation of test event instances
        List<MAR_Single_Form_Event__e> marFormEvents = new List<MAR_Single_Form_Event__e>();
        for (MAR_Form_Staging__c mf: mfsList) {
             MAR_Single_Form_Event__e mfse = new MAR_Single_Form_Event__e();
             mfse.Id__c = mf.Id;
             mfse.Name__c = mf.name;             
             marFormEvents.add(mfse);            
        }
        
        Test.startTest();
        List<Database.SaveResult> results = EventBus.publish(marFormEvents);
        Test.stopTest();                
        
        // Inspect publishing result for each event
        for (Database.SaveResult sr : results) {
                System.assertEquals(true, sr.isSuccess());
        }
        
        List<MAR__c> mars = [SELECT Id FROM MAR__c];
        List<Site__c> sites = [SELECT Id FROM Site__c];
        List<Contact> contacts = [SELECT Id FROM Contact];
        System.assertEquals(5, mars.size());
        System.assertEquals(2, sites.size());    
        System.assertEquals(8, contacts.size());  
    }
    
    @isTest static void createInvalidMARSingleForm(){ 
        List<MAR_Form_Staging__c> mfslist = new List<MAR_Form_Staging__c>();
               
        MAR_Form_Staging__c mfs1 = new MAR_Form_Staging__c();
        insert mfs1;
        
        mfslist.add(mfs1);
        
        //insert mfslist;
        
        // Creation of test event instances
        List<MAR_Single_Form_Event__e> marFormEvents = new List<MAR_Single_Form_Event__e>();
        for (MAR_Form_Staging__c mf: mfsList) {
             MAR_Single_Form_Event__e mfse = new MAR_Single_Form_Event__e();                       
             marFormEvents.add(mfse);            
        }
        
        Test.startTest();
        List<Database.SaveResult> results = EventBus.publish(marFormEvents);
        Test.stopTest();                
        
        // Inspect publishing result for each event
        for (Database.SaveResult sr : results) {
                System.assertEquals(true, sr.isSuccess());
        }
        
        List<MAR__c> mars = [SELECT Id FROM MAR__c];

        //System.assertEquals(0, mars.size());
    }
    
    @isTest static void createInvalidFormEvent() {
        
        MAR_Single_Form_Event__e mfse = new MAR_Single_Form_Event__e(Id__c = '12345678901234567890');
        
        Test.startTest();

        Database.SaveResult sr = EventBus.publish(mfse);  
        Test.stopTest();
        System.assertEquals(false, sr.isSuccess());
        
        // Log the error message
        for(Database.Error err : sr.getErrors()) {
            System.debug('Error returned: ' +
                        err.getStatusCode() +
                        ' - ' +
                        err.getMessage()+' - '+err.getFields());
        }
        
        List<MAR__c> mars = [SELECT Id FROM MAR__c];
        //System.assertEquals(0, mars.size());
    } 
    
    @isTest static void getFormHeader() {        
       String header = nbnCommunityThemeController.getNbnTopTailHeader();      
    } 
    @isTest static void getFormFooter() {        
       String header = nbnCommunityThemeController.getNbnTopTailFooter();      
    } 
}