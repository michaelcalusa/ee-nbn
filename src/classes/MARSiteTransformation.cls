/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class will be used to transform and transfer Development records from Development_Int to Development
Test Class:    MAR Site Transformation Test Class..
Modifications:
*/
public class MARSiteTransformation implements Database.Batchable<Sobject>, Database.Stateful{
    public String marSiteQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    public String verifiedRecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Site__c'  and name = 'Verified'].Id;
    public String unverifiedRecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Site__c'  and name = 'Unverified'].Id;
    
    public class GeneralException extends Exception{            
    }
    
    public MARSiteTransformation(String ejId){
         exJobId = ejId;
         Batch_Job__c bj = new Batch_Job__c();
         bj.Name = 'MARSiteTransform'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC){
        marSiteQuery = 'SELECT Id, Location_Id__c, Disconnection_Date__c,Technology_Type__c,Service_Class__c FROM MAR_Case_Stage__c WHERE MAR_Transformation_Status__c = \'New\' ORDER BY CreatedDate'; 
        return Database.getQueryLocator(marSiteQuery);            
    }
    public void execute(Database.BatchableContext BC,List<MAR_Case_Stage__c> listOfMARSite){        
        try{
            Map<String, Id> mapOfExistingSite = new Map<String, Id>(); 
			List<String> listOfSiteIds = new List<String>();  
            list <string> erroredIds = new list<string>();
            list <string> successIds = new list<string>();
			List<Site__c> listOfSiteToCreate = new List<Site__c>(); 
            List<Site__c> listOfSiteToUpdate = new List<Site__c>();
            
            map <String, Site__c> createSiteMap = new map<String, Site__c>();
            map <String, Site__c> updateSiteMap = new map<String, Site__c>();
			// In Stage Table, with one loc Id can have multiple records
			Map <String, List<MAR_Case_Stage__c>> mapOfCaseStage = new Map<String, List<MAR_Case_Stage__c>>();
			// Collect all location Id's
            for(MAR_Case_Stage__c mari : listOfMARSite){                                                 
                if(String.isNotBlank(mari.Location_Id__c)) 
                    listOfSiteIds.add(mari.Location_Id__c);
			}
			// Query existing sites. put the loc Id as key with Site Id as value
            for(Site__c site : [SELECT Id, Location_Id__c FROM Site__c WHERE Location_Id__c IN :listOfSiteIds AND (RecordType.developername = 'Unverified' OR RecordType.developername = 'Verified') FOR UPDATE]){
                mapOfExistingSite.put(site.Location_Id__c, site.id);            
            }
            
            for(MAR_Case_Stage__c marSite : listOfMARSite){
                Site__c createSite = new Site__c();
                Site__c updateSite = new Site__c();               
                if(String.isNotBlank(marSite.Location_Id__c)){
					// Collect all the stage records in Map
					if(mapOfCaseStage.containsKey(marSite.Location_Id__c))
						mapOfCaseStage.get(marSite.Location_Id__c).add(marSite);
					else
						mapOfCaseStage.put(marSite.Location_Id__c, new List<MAR_Case_Stage__c> {marSite});
					// Update Record
					if(mapOfExistingSite.get(marSite.Location_Id__c) <> null){
						if(!updateSiteMap.containsKey(marSite.Location_Id__c)){
							updateSite.Id = mapOfExistingSite.get(marSite.Location_Id__c); 
							updateSite.Location_Id__c = marSite.Location_Id__c;
							updateSite.Disconnection_Date__c = marSite.Disconnection_Date__c;
							updateSite.Technology_Type__c = marSite.Technology_Type__c;
							updateSite.Serviceability_Class__c = marSite.Service_Class__c;
							updateSite.MAR__c = true;
							listOfSiteToUpdate.add(updateSite);
							updateSiteMap.put(marSite.Location_Id__c,updateSite);
						}
					}
					// Create Record
					else{
						if(!createSiteMap.containsKey(marSite.Location_Id__c)){
							createSite.Name = marSite.Location_Id__c;
							createSite.Location_Id__c = marSite.Location_Id__c;
							createSite.Disconnection_Date__c = marSite.Disconnection_Date__c;
							createSite.Technology_Type__c = marSite.Technology_Type__c;
							createSite.Serviceability_Class__c = marSite.Service_Class__c;
							createSite.MAR__c = true;                
							createSite.RecordTypeId = unverifiedRecordTypeId;
							listOfSiteToCreate.add(createSite);
							createSiteMap.put(marSite.Location_Id__c,createSite);
						}
					}
                }                        
            }
            List<Database.SaveResult> insertSiteResults = Database.Insert(listOfSiteToCreate,false);
            List<Database.SaveResult> updateSiteResults = Database.Update(listOfSiteToUpdate,false);
            List<Database.Error> insertSiteError;
            List<Database.Error> updateSiteError;
            String errorMessage, fieldsAffected;
			List<MAR_Case_Stage__c> listOfStagingRecordsToUpdate = new List<MAR_Case_Stage__c>();
            String[] listOfAffectedFields;
            StatusCode sCode;
            List<Error_Logging__c> errSiteList = new List<Error_Logging__c>();
            List<MAR_Case_Stage__c> updMARSiteRes = new List<MAR_Case_Stage__c>();
			// Process update results
            for(Integer j=0; j<updateSiteResults.size();j++){
				// Error
                if(!updateSiteResults[j].isSuccess()){
                    updateSiteError = updateSiteResults[j].getErrors();
                    for(Database.Error er: updateSiteError){
                        sCode = er.getStatusCode();
                        errorMessage = er.getMessage();
                        listOfAffectedFields = er.getFields();
                    }
                    fieldsAffected = String.join(listOfAffectedFields,',');
                    errSiteList.add(new Error_Logging__c(Name='MARSiteTransformation',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isSuccess__c=updateSiteResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),Schedule_Job__c=bjId));
					// Get all the Staging records by providing location Id
					for(MAR_Case_Stage__c marSite : mapOfCaseStage.get(listOfSiteToUpdate[j].Location_Id__c)){
						marSite.MAR_Transformation_Status__c = 'Errored';
						listOfStagingRecordsToUpdate.add(marSite);
					}
                }
				// Success
                else{
					// Get all the Staging records by providing location Id
					for(MAR_Case_Stage__c marSite : mapOfCaseStage.get(listOfSiteToUpdate[j].Location_Id__c)){
						marSite.MAR_Transformation_Status__c = 'Site Uploaded Successfully';
						marSite.Site_Id__c = updateSiteResults[j].getId();
						marSite.Case_Concat__c = listOfSiteToUpdate[j].Location_Id__c;
						listOfStagingRecordsToUpdate.add(marSite);
					}
                }
            }
			// Process create Status
            for(Integer j=0; j<insertSiteResults.size();j++){
				// Error
                if(!insertSiteResults[j].isSuccess()){
                    insertSiteError = insertSiteResults[j].getErrors();
                    for(Database.Error er: insertSiteError){
                        sCode = er.getStatusCode();
                        errorMessage = er.getMessage();
                        listOfAffectedFields = er.getFields();
                    }
                    fieldsAffected = String.join(listOfAffectedFields,',');
                    errSiteList.add(new Error_Logging__c(Name='MARSiteTransformation',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isSuccess__c=insertSiteResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),Schedule_Job__c=bjId));
					// Get all the Staging records by providing location Id
					for(MAR_Case_Stage__c marSite : mapOfCaseStage.get(listOfSiteToCreate[j].Location_Id__c)){
						marSite.MAR_Transformation_Status__c = 'Errored';
						listOfStagingRecordsToUpdate.add(marSite);
					}
                }
				// Success
                else{
					// Get all the Staging records by providing location Id
					for(MAR_Case_Stage__c marSite : mapOfCaseStage.get(listOfSiteToCreate[j].Location_Id__c)){
						marSite.MAR_Transformation_Status__c = 'Site Uploaded Successfully';
						marSite.Site_Id__c = insertSiteResults[j].getId();
						marSite.Case_Concat__c = listOfSiteToCreate[j].Location_Id__c;
						listOfStagingRecordsToUpdate.add(marSite);
					}
                }
            }
            system.debug('errMARList==>'+errSiteList);
            Insert errSiteList;
			if(!listOfStagingRecordsToUpdate.isEmpty())
				UPDATE listOfStagingRecordsToUpdate;
            if(errSiteList.isEmpty()){
                status = 'Completed';
            } 
            else{
                status = 'Error';
            }  
            recordCount = recordCount+listOfMARSite.size();             
        }
        catch(Exception e)
        {            
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='MAR Site Transformation',Error_Message__c= e.getMessage()+' Line Number: '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob marJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
        {
            bj.Status__c= marJob.Status;
        }
        else
        {
             bj.Status__c=status;
        }
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = marJob.id;
        bj.End_Time__c  = marJob.CompletedDate;
        upsert bj;
        
        Database.executeBatch(new MARContactCreation_Batch(exJobId));
    }
}