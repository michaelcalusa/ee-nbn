@isTest
private class MARSiteTransformation_Test {
/*------------------------------------------------------------
Author:        Shubham Jaiswal
Company:       Wipro
Description:   Test class for MAR Batch Jobs.
------------------------------------------------------------*/  
    private static testMethod void scheduleMARCaseStage(){
        
        // Create Site record
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        Site__c UnverifiedSite = new Site__c();
        UnverifiedSite.RecordTypeId=recordTypeId;  
        //UnverifiedSite.MDU_CP_On_Demand_Id__c = 'NorthWest';
        UnverifiedSite.isSiteCreationThroughCode__c =true;
        insert UnverifiedSite;
        
        // Create Contact record
        List<Contact> conList = new List<Contact>(); 
        ID conrecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId();
        Contact contactRec = new Contact ();
        contactRec.LastName = 'Test Contact';
        contactRec.Preferred_Phone__c = '0123456789';
        //contactRec.On_Demand_Id_P2P__c = 'TestContactId';
        contactRec.recordtypeid = conrecordTypeId;
        conList.add(contactRec);
        
        Contact con1 = new Contact();
        con1.LastName = 'John Doe';
        con1.Email = 'johndoe@nbnco.com.au';
        con1.Preferred_Phone__c = '0123456789';
        con1.MAR__c = true;
        con1.recordtypeid = conrecordTypeId;
        conList.add(con1);
        
        Contact con2 = new Contact();
        con2.LastName = 'John Doe';
        con2.Preferred_Phone__c = '0123456789';
        con2.MAR__c = true;
        con2.recordtypeid = conrecordTypeId;
        conList.add(con2);
        
        Contact con3 = new Contact();
        con3.LastName = 'Doe John';
        con3.Email = 'doejohn@nbnco.com.au';
        con3.recordtypeid = conrecordTypeId;
        conList.add(con3);
        
        Contact con4 = new Contact();
        con4.LastName = 'Doe John';
        con4.Email = 'doejohn@nbnco.com.au';
        con4.recordtypeid = conrecordTypeId;
        conList.add(con4);
        
        Contact con5 = new Contact();
        con5.LastName = 'Doe John';
        con5.Email = 'doejohn@nbnco.com.au';
        con5.MAR__c = true;
        con5.Preferred_Phone__c = '0987654321';
        con5.recordtypeid = conrecordTypeId;
        conList.add(con5);

        Database.DMLOptions dml = new Database.DMLOptions(); 
        dml.DuplicateRuleHeader.AllowSave = true;
        List<Database.SaveResult> sr = Database.insert(conList, dml);

        
       
        List <MAR_Case_Stage__c> mcsList = new List<MAR_Case_Stage__c>();               
        
        MAR_Case_Stage__c mcs = new MAR_Case_Stage__c();
        mcs.Alarm_Type__c='Monitored';
        mcs.Case_Type__c='Courtesy Call';
        mcs.Copper_Active__c='Yes';
        mcs.Date_Provided__c=Date.valueof('2016-08-09');
        mcs.Disconnection_Date__c=Date.valueof('2016-09-09');
        mcs.Inflight_Order__c='No';
        mcs.Location_Id__c='LOC000014768895';
        mcs.NBN_Active__c='No';
        mcs.Primary_Contact_Email__c='';
        mcs.Primary_Contact_Name__c='PAMULA WELBOURN';
        mcs.Primary_Contact_Phone__c='+61 2 46262333';
        mcs.Record_Provided_By__c='Self Registered';
        mcs.Registered_in_Error__c='0';
        mcs.Rollout_Group__c='14042017 FSAMs';
        mcs.Secondary_Contact_Name__c='Test';
        mcs.Secondary_Contact_Phone__c='123456789';
        mcs.Service_Class__c=Decimal.valueof('3');
        mcs.Technology_Type__c='Brownfields Fibre';
        mcsList.add(mcs);
        
//        MAR_Case_Stage__c mcs1 = new MAR_Case_Stage__c();
//        mcs1.Alarm_Type__c='Monitored';
//        mcs1.Case_Type__c='Courtesy Call';
//        mcs1.Copper_Active__c='Yes';
//        mcs1.Date_Provided__c=Date.valueof('2016-08-09');
//        mcs1.Disconnection_Date__c=Date.valueof('2016-09-09');
//        mcs1.Inflight_Order__c='No';
//        mcs1.Location_Id__c='LOC000014768895';
//        mcs1.NBN_Active__c='No';
//        mcs1.Primary_Contact_Name__c='PAMULA WELBOURN';
//        mcs1.Primary_Contact_Phone__c='+61 2 46262333';
//        mcs1.Primary_Contact_Email__c='abcd@gmail.com';
//        mcs1.Record_Provided_By__c='Self Registered';
//        mcs1.Rollout_Group__c='17072014 FSAMs';
//        mcs1.Secondary_Contact_Name__c='Sandra Luke or Diane';
//        mcs1.Secondary_Contact_Phone__c='(02) 6650 0746,0123456789';
//        mcs1.Primary_Contact_Email__c='abcd1@gmail.com';
//        mcs1.Service_Class__c=Decimal.valueof('3');
//        mcs1.Technology_Type__c='Brownfields Fibre';
//        mcsList.add(mcs1);
//
//        MAR_Case_Stage__c mcs2 = new MAR_Case_Stage__c();
//        mcs2.Alarm_Type__c='Monitored';
//        mcs2.Case_Type__c='Courtesy Call';
//        mcs2.Copper_Active__c='Yes';
//        mcs2.Date_Provided__c=Date.valueof('2016-08-09');
//        mcs2.Disconnection_Date__c=Date.valueof('2016-09-09');
//        mcs2.Inflight_Order__c='No';
//        mcs2.Location_Id__c='LOC000014768895';
//        mcs2.NBN_Active__c='No';
//        mcs2.Primary_Contact_Name__c='PAMULA WELBOURN';
//        mcs2.Primary_Contact_Phone__c='+61 2 46262333';
//        mcs2.Record_Provided_By__c='Self Registered';
//        mcs2.Rollout_Group__c='17072014 FSAMs';
//        mcs2.Secondary_Contact_Name__c='Sandra Luke or Diane';
//        mcs2.Secondary_Contact_Phone__c='(02) 6650 0746,0123456789';
//        mcs2.Service_Class__c=Decimal.valueof('3');
//        mcs2.Technology_Type__c='Brownfields Fibre';
//        mcsList.add(mcs2);
//
//        MAR_Case_Stage__c mcs3 = new MAR_Case_Stage__c();
//        mcs3.Alarm_Type__c='Monitored';
//        mcs3.Case_Type__c='Courtesy Call';
//        mcs3.Copper_Active__c='Yes';
//        mcs3.Date_Provided__c=Date.valueof('2016-08-09');
//        mcs3.Disconnection_Date__c=Date.valueof('2016-09-09');
//        mcs3.Inflight_Order__c='No';
//        mcs3.Location_Id__c='LOC000014768895';
//        mcs3.NBN_Active__c='No';
//        mcs3.Primary_Contact_Name__c='PAMULA WELBOURN';
//        mcs3.Primary_Contact_Email__c='abcd@gmail.com';
//        mcs3.Record_Provided_By__c='Self Registered';
//        mcs3.Rollout_Group__c='17072014 FSAMs';
//        mcs3.Secondary_Contact_Name__c='Sandra Luke or Diane';
//        mcs3.Secondary_Contact_Phone__c='(02) 6650 0746,0123456789';
//        mcs3.Service_Class__c=Decimal.valueof('3');
//        mcs3.Technology_Type__c='Brownfields Fibre';
//        mcsList.add(mcs3);
//
//        MAR_Case_Stage__c mcs4 = new MAR_Case_Stage__c();
//        mcs4.Alarm_Type__c='Monitored';
//        mcs4.Case_Type__c='Courtesy Call';
//        mcs4.Copper_Active__c='Yes';
//        mcs4.Date_Provided__c=Date.valueof('2016-08-09');
//        mcs4.Disconnection_Date__c=Date.valueof('2016-09-09');
//        mcs4.Inflight_Order__c='No';
//        mcs4.Location_Id__c='LOC0000147688958';
//        mcs4.NBN_Active__c='No';
//        mcs4.Primary_Contact_Name__c='DANIEL CRAIG';
//        mcs4.Primary_Contact_Email__c='abcd@gmail.com';
//        mcs4.Record_Provided_By__c='Self Registered';
//        mcs4.Rollout_Group__c='17072014 FSAMs';
//        mcs4.Secondary_Contact_Name__c='Sandra Luke or Diane';
//        mcs4.Secondary_Contact_Phone__c='(02) 6650 0746,0123456789';
//        mcs4.Service_Class__c=Decimal.valueof('3');
//        mcs4.Technology_Type__c='Brownfields Fibre';
//        mcsList.add(mcs4);
//
//        Site__c newSite = new Site__c(Location_Id__c='LOC999999999999');
//        insert newSite;
//
//        MAR_Case_Stage__c mcs5 = new MAR_Case_Stage__c();
//        mcs5.Alarm_Type__c='Monitored';
//        mcs5.Case_Type__c='Courtesy Call';
//        mcs5.Copper_Active__c='Yes';
//        mcs5.Date_Provided__c=Date.valueof('2016-08-09');
//        mcs5.Disconnection_Date__c=Date.valueof('2016-09-09');
//        mcs5.Inflight_Order__c='No';
//        mcs5.Location_Id__c= newSite.Location_Id__c;
//        mcs5.NBN_Active__c='No';
//        mcs5.Primary_Contact_Name__c='DANIEL CRAIG';
//        mcs5.Primary_Contact_Email__c='abcd@gmail.com';
//        mcs5.Record_Provided_By__c='Self Registered';
//        mcs5.Rollout_Group__c='17072014 FSAMs';
//        mcs5.Secondary_Contact_Name__c='Sandra Luke or Diane';
//        mcs5.Secondary_Contact_Phone__c='(02) 6650 0746,0123456789';
//        mcs5.Service_Class__c=Decimal.valueof('3');
//        mcs5.Technology_Type__c='Brownfields Fibre';
//        mcsList.add(mcs5);
//
//        MAR_Case_Stage__c mcs6 = new MAR_Case_Stage__c();
//        mcs6.Alarm_Type__c='Monitored';
//        //mcs6.Case_Type__c='Courtesy Call';
//        mcs6.Copper_Active__c='Yes';
//        mcs6.Date_Provided__c=Date.valueof('2016-08-09');
//        mcs6.Disconnection_Date__c=Date.valueof('2016-09-09');
//        mcs6.Inflight_Order__c='No';
//        mcs6.Location_Id__c= 'LOC0000147688958';
//        mcs6.NBN_Active__c='No';
//        mcs6.Primary_Contact_Name__c='DANIEL CRAIG';
//        mcs6.Primary_Contact_Email__c='abcd@gmail.com';
//        mcs6.Record_Provided_By__c='Self Registered';
//        mcs6.Rollout_Group__c='17072014 FSAMs';
//        mcs6.Secondary_Contact_Name__c='Sandra Luke or Diane';
//        mcs6.Secondary_Contact_Phone__c='(02) 6650 0746,0123456789';
//        mcs6.Service_Class__c=Decimal.valueof('3');
//        mcs6.Technology_Type__c='Brownfields Fibre';
//        mcsList.add(mcs6);
//
//        MAR_Case_Stage__c mcs7 = new MAR_Case_Stage__c();
//        mcs7.Alarm_Type__c='Monitored';
//        mcs7.Case_Type__c='Courtesy Call';
//        mcs7.Copper_Active__c='Yes';
//        mcs7.Date_Provided__c=Date.valueof('2016-08-09');
//        mcs7.Disconnection_Date__c=Date.valueof('2016-09-09');
//        mcs7.Inflight_Order__c='No';
//        mcs7.Location_Id__c='LOC000014758895';
//        mcs7.NBN_Active__c='No';
//        mcs7.Primary_Contact_Name__c='John Doe';
//        mcs7.Primary_Contact_Phone__c='+61 123456789,+61 2 46262332';
//        mcs7.Primary_Contact_Email__c='johndoe@nbnco.com.au';
//        mcs7.Record_Provided_By__c='Self Registered';
//        mcs7.Rollout_Group__c='17072014 FSAMs';
//        mcs7.Secondary_Contact_Name__c='Doe John';
//        mcs7.Secondary_Contact_Phone__c='0987654321,0123456789';
//        mcs7.Secondary_Contact_Email__c='doejohn@nbnco.com.au';
//        mcs7.Service_Class__c=Decimal.valueof('3');
//        mcs7.Technology_Type__c='Brownfields Fibre';
//        mcsList.add(mcs7);
//
//        MAR_Case_Stage__c mcs8 = new MAR_Case_Stage__c();
//        mcs8.Alarm_Type__c='Monitored';
//        mcs8.Case_Type__c='Courtesy Call';
//        mcs8.Copper_Active__c='Yes';
//        mcs8.Date_Provided__c=Date.valueof('2016-08-09');
//        mcs8.Disconnection_Date__c=Date.valueof('2016-09-09');
//        mcs8.Inflight_Order__c='No';
//        mcs8.Location_Id__c='LOC000014758895';
//        mcs8.NBN_Active__c='No';
//        mcs8.Primary_Contact_Name__c='John Doe';
//        mcs8.Primary_Contact_Phone__c='+61 123456789,+61 2 46262332';
//        mcs8.Primary_Contact_Email__c='johndoe@nbnco.com.au';
//        mcs8.Record_Provided_By__c='Self Registered';
//        mcs8.Rollout_Group__c='17072014 FSAMs';
//        mcs8.Secondary_Contact_Name__c='Doe John';
//        mcs8.Secondary_Contact_Phone__c='0987654321,0123456789';
//        mcs8.Secondary_Contact_Email__c='doejohn@nbnco.com.au';
//        mcs8.Service_Class__c=Decimal.valueof('3');
//        mcs8.Technology_Type__c='Brownfields Fibre';
//        mcsList.add(mcs8);
              
        insert mcsList;            

        //Start the batch run       
        Test.startTest();
         
        String jobId = System.schedule( 'MARSiteTransformation_Schedule', 
                                        '0 0 0 3 9 ? 2022', 
                                        new MARSiteTransformation_Schedule());                               
        Test.stopTest();
    }   
    
    private static testMethod void testMARCase(){
 
        Extraction_Job__c ext = new Extraction_Job__c(Name='Test');
        insert ext; 
       
         // Create Site record
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        Site__c UnverifiedSite = new Site__c();
        UnverifiedSite.RecordTypeId=recordTypeId;  
        //UnverifiedSite.MDU_CP_On_Demand_Id__c = 'NorthWest';
        UnverifiedSite.isSiteCreationThroughCode__c =true;
        insert UnverifiedSite;
       
    
        // Create Contact record
        ID conrecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId();
        Contact contactRec = new Contact ();
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        //contactRec.On_Demand_Id_P2P__c = 'TestContactId';
        contactRec.recordtypeid = conrecordTypeId;
        insert contactRec;

        
        Id marRecTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId();
        Case marcase = new Case(RecordTypeId=marRecTypeId,Site__c =  UnverifiedSite.Id,ContactId=contactRec.id,Case_Concat__c = String.valueof(UnverifiedSite.Id)+String.valueof(contactRec.id));
        insert marcase;
             
        List <MAR_Case_Stage__c> mcsList = new List<MAR_Case_Stage__c>();      
        MAR_Case_Stage__c mcs = new MAR_Case_Stage__c();
        mcs.Alarm_Type__c='Monitored';
        mcs.Case_Type__c='Courtesy Call';
        mcs.Copper_Active__c='Yes';
        mcs.Date_Provided__c=Date.valueof('2016-08-09');
        mcs.Disconnection_Date__c=Date.valueof('2016-09-09');
        mcs.Inflight_Order__c='No';
        mcs.Location_Id__c='LOC000014768895';
        mcs.NBN_Active__c='No';
        mcs.Primary_Contact_Email__c='';
        mcs.Primary_Contact_Name__c='PAMULA WELBOURN';
        mcs.Primary_Contact_Phone__c='+61 2 46262333';
        mcs.Record_Provided_By__c='Self Registered';
        mcs.Registered_in_Error__c='0';
        mcs.Rollout_Group__c='14042017 FSAMs';
        mcs.Secondary_Contact_Name__c='Test';
        mcs.Secondary_Contact_Phone__c='123456789';
        mcs.Service_Class__c=Decimal.valueof('3');
        mcs.Technology_Type__c='Brownfields Fibre';
        mcs.MAR_Transformation_Status__c = 'Contact Uploaded Successfully';
        mcs.Site_Id__c =  UnverifiedSite.Id;
        mcs.Primary_Contact__c = String.valueof(contactRec.id);
        mcs.Case_Concat__c = String.valueof(UnverifiedSite.Id)+String.valueof(contactRec.id);
        mcsList.add(mcs);
        
        insert mcsList;
        
        Test.startTest();
        database.executeBatch(new MARCaseTransformation(ext.id));
        Test.stopTest();
    }
}