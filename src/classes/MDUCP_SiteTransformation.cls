/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class will be used to transform and transfer MDU/CP records from MDU_CP_Int to Site table of Record Type MDU/CP
Modifications:
*/
public class MDUCP_SiteTransformation // implements Database.Batchable<Sobject>, Database.Stateful
{
    /*********************************************************************
    US#4118: Below lines have been commented to decommission P2P jobs.
    Author: Jairaj Jadhav
    Company: Wipro Technologies
    
    public String mducpIntQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {            
    }
    
    public MDUCP_SiteTransformation(String ejId)
    {
         exJobId = ejId;
         Batch_Job__c bj = new Batch_Job__c();
         bj.Name = 'MDUCP_SiteTransform'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        mducpIntQuery = 'select Id,Building_ID__c,CRM_Record_Id__c,Site_Name__c,Physical_Street_Number__c,Physical_Street_Name__c,Physical_City__c,Physical_Post_Code__c,Physical_State__c,Program_Manager__c,CRMOD_Owner__c,On_Hold_Reason__c,Engagement_Progress__c,Program_Type__c,Contractor_Assigned__c,Legal_Owner_Address__c,Access_Information__c,Number_of_Levels__c,Number_of_Premises__c,No_of_ATMs__c,No_of_Lifts__c,No_of_Fire_Services__c,No_of_Emergency_Services__c,No_of_Security_Services__c,No_of_Vending_Machines__c,No_of_Other_Non_Premises__c,Plan_Number__c,Legal_Owner_Addressee__c,Crown_Land__c,Maintenance__c,Vegetation_Tree_Lopping__c,HEC_Overlay__c,FSA__c,SAM__c from MDU_CP_Int__c where Transformation_Status__c = \'Extracted from CRM\' order by CreatedDate'; 
        return Database.getQueryLocator(mducpIntQuery);            
    }
    public void execute(Database.BatchableContext BC,List<MDU_CP_Int__c> listOfMDUCPInt)
    {        
        try
        {
            List<Site__c> listOfMDUCPSite = new List<Site__c>();
            map <String, Site__c> siteMap = new map<String, Site__c>();
            String rTypeIdMDUCP = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Site__c'  and name = 'MDU/CP'].Id; 
            for(MDU_CP_Int__c mducpInt : listOfMDUCPInt)
            {
                Site__c mducpSite = new Site__c();
                
                mducpSite.RecordTypeId = rTypeIdMDUCP;
                mducpSite.MDU_CP_On_Demand_Id__c = mducpInt.CRM_Record_Id__c;
                mducpSite.Location_Id__c = mducpInt.Building_ID__c;
                mducpSite.MDU_CP_Site_Name__c = mducpInt.Site_Name__c;
                mducpSite.Road_Number_1__c = mducpInt.Physical_Street_Number__c;
                mducpSite.Road_Name__c = mducpInt.Physical_Street_Name__c;
                mducpSite.Locality_Name__c = mducpInt.Physical_City__c;
                //mducpSite.Post_Code__c = mducpInt.Physical_Post_Code__c;
                                    
                if(String.isNotBlank(mducpInt.Physical_Post_Code__c))
                {
                    if (ValidatePostCode.validate(mducpInt.Physical_Post_Code__c))
                    {
                        mducpSite.Post_Code__c=mducpInt.Physical_Post_Code__c;
                    }
                }
                mducpSite.State_Territory_Code__c = mducpInt.Physical_State__c;
                mducpSite.Site_Address__c = '';
                if(string.isNotEmpty(mducpInt.Physical_Street_Number__c))
                {
                    mducpSite.Site_Address__c += mducpInt.Physical_Street_Number__c + ' ';    
                }
                if(string.isNotEmpty(mducpInt.Physical_Street_Name__c))
                {
                    mducpSite.Site_Address__c += mducpInt.Physical_Street_Name__c + ' ';    
                }                
                if(string.isNotEmpty(mducpInt.Physical_City__c))
                {
                    mducpSite.Site_Address__c += mducpInt.Physical_City__c + ' ';    
                } 
                if(string.isNotEmpty(mducpInt.Physical_State__c))
                {
                    mducpSite.Site_Address__c += mducpInt.Physical_State__c + ' ';    
                }   
                if(string.isNotEmpty(mducpInt.Physical_Post_Code__c))
                {
                    mducpSite.Site_Address__c += mducpInt.Physical_Post_Code__c + ' ';    
                }
                mducpSite.Name =  mducpSite.Site_Address__c;
                mducpSite.Program_Manager__c = mducpInt.Program_Manager__c;
                mducpSite.MDU_CP_CRMOD_Owner__c = mducpInt.CRMOD_Owner__c;
                mducpSite.On_Hold_Reason__c = mducpInt.On_Hold_Reason__c;
                mducpSite.Engagement_Progress__c = mducpInt.Engagement_Progress__c;
                mducpSite.Program_Type__c = mducpInt.Program_Type__c;
                mducpSite.Contractor_Assigned__c = mducpInt.Contractor_Assigned__c;
                mducpSite.Legal_Owner_Address__c = mducpInt.Legal_Owner_Address__c;
                mducpSite.Access_Information__c = mducpInt.Access_Information__c;
                mducpSite.No_of_Levels__c = mducpInt.Number_of_Levels__c;
                mducpSite.No_of_Premises__c = mducpInt.Number_of_Premises__c;
                mducpSite.No_of_ATMs__c = mducpInt.No_of_ATMs__c;
                mducpSite.No_of_Lifts__c = mducpInt.No_of_Lifts__c;
                mducpSite.No_of_Fire_Services__c = mducpInt.No_of_Fire_Services__c;
                mducpSite.No_of_Emergency_Services__c = mducpInt.No_of_Emergency_Services__c;
                mducpSite.No_of_Security_Services__c = mducpInt.No_of_Security_Services__c;
                mducpSite.No_of_Vending_Machines__c = mducpInt.No_of_Vending_Machines__c;
                mducpSite.No_of_Other_Non_Premises__c = mducpInt.No_of_Other_Non_Premises__c;
                mducpSite.MDU_CP_Plan_Number__c = mducpInt.Plan_Number__c;
                mducpSite.Legal_Owner_Addressee__c = mducpInt.Legal_Owner_Addressee__c;
                mducpSite.Crown_Land__c = mducpInt.Crown_Land__c;
                mducpSite.Maintenance__c = mducpInt.Maintenance__c;
                mducpSite.Vegetation_Tree_Lopping__c = mducpInt.Vegetation_Tree_Lopping__c;
                mducpSite.HEC_Overlay__c = mducpInt.HEC_Overlay__c;
                mducpSite.FSA__c = mducpInt.FSA__c;
                mducpSite.SAM__c = mducpInt.SAM__c;
                mducpSite.MDU_CP_Ownership__c = 'CRMOD';
                mducpSite.isSiteCreationThroughCode__c = true;
                //listOfMDUCPSite.add(mducpSite);
                siteMap.put(mducpInt.CRM_Record_Id__c, mducpSite);
            }
            listOfMDUCPSite = siteMap.values();
            List<Database.UpsertResult> upsertMDUCPResults = Database.Upsert(listOfMDUCPSite,Site__c.Fields.MDU_CP_On_Demand_Id__c,false);
            List<Database.Error> upsertMDUCPError;
            String errorMessage, fieldsAffected;
            String[] listOfAffectedFields;
            StatusCode sCode;
            List<Error_Logging__c> errMDUCPList = new List<Error_Logging__c>();
            List<MDU_CP_Int__c> updMDUCPIntRes = new List<MDU_CP_Int__c>();
            for(Integer j=0; j<upsertMDUCPResults.size();j++)
            {
                //MDU_CP_Int__c updMDUCPInt = new MDU_CP_Int__c();
                if(!upsertMDUCPResults[j].isSuccess())
                {
                    upsertMDUCPError = upsertMDUCPResults[j].getErrors();
                    for(Database.Error er: upsertMDUCPError)
                    {
                        sCode = er.getStatusCode();
                        errorMessage = er.getMessage();
                        listOfAffectedFields = er.getFields();
                    }
                    fieldsAffected = String.join(listOfAffectedFields,',');
                    errMDUCPList.add(new Error_Logging__c(Name='MDU/CP Site',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isCreated__c=upsertMDUCPResults[j].isCreated(),isSuccess__c=upsertMDUCPResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfMDUCPSite[j].MDU_CP_On_Demand_Id__c,Schedule_Job__c=bjId));
                    listOfMDUCPInt[j].Transformation_Status__c = 'Error';
                    //updMDUCPInt.CRM_Record_Id__c = listOfMDUCPSite[j].MDU_CP_On_Demand_Id__c;
                    listOfMDUCPInt[j].Schedule_Job_Transformation__c = bjId;
                    //updMDUCPIntRes.add(updMDUCPInt);
                }
                else
                {
                    listOfMDUCPInt[j].Transformation_Status__c = 'Copied to Base Table';
                    //updMDUCPInt.CRM_Record_Id__c = listOfMDUCPSite[j].MDU_CP_On_Demand_Id__c;
                    listOfMDUCPInt[j].Schedule_Job_Transformation__c = bjId;
                    //updMDUCPIntRes.add(updMDUCPInt);                    
                }
            }
            Insert errMDUCPList;
            Update listOfMDUCPInt;   
            if(errMDUCPList.isEmpty())      
            {
                status = 'Completed';
            } 
            else
            {
                status = 'Error';
            }  
            recordCount = recordCount + listOfMDUCPInt.size();          
        }
        catch(Exception e)
        {
            status = 'Error';            
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='MDU/CP Site Transformation',Error_Message__c= e.getMessage()+' Line Number: '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob mduJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
        {
            bj.Status__c= mduJob.Status;
        }
        else
        {
             bj.Status__c=status;
        }
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = mduJob.id;
        bj.End_Time__c  = mduJob.CompletedDate;
        upsert bj;
        if (CRM_Transformation_Job__c.getinstance('CommunicationTransformation') <> null && CRM_Transformation_Job__c.getinstance('CommunicationTransformation').on_off__c)
        {
            Database.executeBatch(new CommunicationTransformation(exJobId));
        }
    }
    *********************************************************************/
}