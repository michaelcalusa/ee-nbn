//*************************************************
// Developed by: Viswanatha Goki
// Date Created: 25.10.2016 (dd.MM.yyyy)
// Last Update: (dd.MM.yyyy)
// Description: Custom Controller Apex class invoked from Onclick of Generate Agreement
//              button on Opportunity Page. Checks for mandatory information required for billing, and alerts
//              alerts user if any if the mandatory information is missing. If none missing re-direct to conga
//              Conga Template selection window
// Modifications
//     - 
//*************************************************
public with sharing class MandatoryBillingInfoCheck {

    public String oppId {get; set;}
    public String message {get; set;}
    public Boolean isMandatoryInfoMissing {get; set;}
    public CPQSettings__c cpqSettings;
    Public PageReference init()
    {
        //Retrieve all the  fields which are mandatory for generating Agreement/Invoicing. 
        Opportunity opp = [SELECT Id,ContractId,Account.Customer_Type__c,Account.Registered_Entity_name__c,Account.Name,
                            AccountId,Account.ABN__C,Account.ACN__c,Contract.Billing_Contact__c,
                            Contract.Billing_Contact__r.FirstName,Contract.Billing_Contact__r.LastName,
                            Contract.Billing_Contact__r.Email,Contract.Billing_Contact__r.Phone,Contract.Billing_Contact__r.MobilePhone,
                            Owner.Name,Billing_Contact_Id__c,
                            Contract.BillingStreet,Contract.BillingCity,Contract.BillingPostalCode,Contract.BillingState,
                            Contract.Dunning_Contact__c,Contract.Dunning_Contact__r.FirstName,Contract.Dunning_Contact__r.Phone,Contract.Dunning_Contact__r.MobilePhone,
                            RecordType.Name
                            FROM Opportunity where Id=:oppId];

        message = '';
        if(opp.AccountId == null)message = message + 'Opportunity Account is null\n';

        //AC-2768
        if(opp.Account.ABN__c != null && opp.Account.ABN__c !='' && opp.AccountId !=null)
        {
            if((String.valueOf(opp.Account.Name)).length() > 240)
            {
                message = message + 'Opportunity Account with ABN must have 240 or less characters\n';
            }    
            
        }
        else
        {
            if(opp.AccountId !=null && (String.valueOf(opp.Account.Name)).length() > 150)
            {
                message = message + 'Opportunity Account without ABN must have 150 or less characters\n';
            }
        }

        //if(opp.Account.ABN__c == null || opp.Account.ABN__c == '')message = message + 'Opportunity Account ABN is null\n';
        //if(opp.Account.ACN__c == null || opp.Account.ACN__c == '')message = message + 'Opportunity Account ACN is null\n';
        if(opp.Account.Customer_Type__c == null || opp.Account.Customer_Type__c == '')
        {
            message = message + 'Opportunity Account Customer Type is null\n';
        }
        else
        {
            if(opp.Account.Customer_Type__c == 'Organisation' && (opp.Account.ABN__c == null || opp.Account.ABN__c == ''))message = message + 'Opportunity Account ABN is null\n';
        }
        if(opp.ContractId == null)
        {
            message = message + 'Opportunity Account Address is null\n';
        }
        else
        {
            if(opp.Contract.BillingStreet == null || opp.Contract.BillingStreet == '')
                message = message + 'Billing Street/Address 1 is mising on Opportunity Account Address\n';
            if(opp.Contract.BillingCity == null || opp.Contract.BillingCity == '')
                message = message + 'Billing City is mising on Opportunity Account Address\n';
            if(opp.Contract.BillingState == null || opp.Contract.BillingState == '')
                message = message + 'Billing State is mising on Opportunity Account Address\n';
            if(opp.Contract.BillingPostalCode == null || opp.Contract.BillingPostalCode == '')
                message = message + 'Billing Postal Code is mising on Opportunity Account Address\n';
            if(opp.Contract.Billing_Contact__c == null)
            {
                message = message + 'Billing Contact is missing on Opportunity Account Address\n';
            }
            else
            {
                if(opp.Contract.Billing_Contact__r.FirstName == null)message = message + 'Billing Contact First Name is null\n';
                if(opp.Contract.Billing_Contact__r.LastName == null)message = message + 'Billing Contact Last Name is null\n';
                if(opp.Contract.Billing_Contact__r.Email == null)message = message + 'Billing Contact Email is null\n';
                //if(opp.Contract.Billing_Contact__r.Phone == null)message = message + 'Billing Contact Phone is null\n';
                //if(opp.Contract.Billing_Contact__r.MobilePhone == null)message = message + 'Billing Contact Mobile Phone is null\n';
                if(opp.Contract.Billing_Contact__r.MobilePhone == null && opp.Contract.Billing_Contact__r.Phone == null)
                {
                    message = message + 'At least one Phone number for Billing Contact is required.\n';
                }
            }
            if(opp.Contract.Dunning_Contact__c == null)
            {
                message = message + 'Dunning Contact is missing on Opportunity Account Address\n';
            }
            else
            {
                if(opp.Contract.Dunning_Contact__r.FirstName == null)message = message + 'Dunning Contact First Name is null\n';
                if(opp.Contract.Dunning_Contact__r.MobilePhone == null && opp.Contract.Dunning_Contact__r.Phone == null)
                {
                    message = message + 'At least one Phone number for Dunning Contact is required.\n';
                }
            }

        }


        if(message != '')isMandatoryInfoMissing = true;

        if(isMandatoryInfoMissing)
        {
            message ='Some of the mandatory information for Billing is missing. Please check below.\n\n' + message;
            return null;
        }

        //Get API.Partner URL
        //PageReference apiUrl = new PageReference('/apexcomponent/APIUrl');
        //Get API.Session_Id
        //PageReference apiSessionId = new PageReference('/apexcomponent/APISessionId');

        //Test
        PageReference apiUrl = new PageReference('/apex/getcongaapiurl');
        PageReference apiSessionId = new PageReference('/apex/getcongaapisession');

        String strSessionId = '';
        String strPartnerUrl = '';

        if(Test.isRunningTest())//Salesforce TestMethod cannot call getContent() methods
        {
            strSessionId = UserInfo.getSessionId();
            strPartnerUrl ='CONGAURL';
        }
        else
        {
            strSessionId = apiSessionId.getContent().toString();
            strSessionId = strSessionId.substringBetween('<body>', '</body>');
            system.debug('API Session Id is - ' + strSessionId);
            strPartnerUrl = apiUrl.getContent().toString();
            strPartnerUrl = strPartnerUrl.substringBetween('<body>', '</body>');
            system.debug('API URL is - ' + strPartnerUrl);
        }
        strSessionId = strSessionId.replace('</html>', '');
        strPartnerUrl = strPartnerUrl.replace('</html>', '');

        String congaUrl = cpqSettings.Conga_Composer_URL__c + strSessionId;
        congaUrl = congaUrl + '&serverUrl=' + strPartnerUrl;
        congaUrl = congaUrl + '&Id=' + oppId + '&QueryId=' + cpqSettings.Billing_Contact_Conga_Query__c + '?pv0=' + opp.ContractId + '&SC0=1&ContactId=' + opp.Billing_Contact_Id__c;
        congaUrl = congaUrl + '&CurrencyCulture=en-AU';
        if((opp.RecordType.Name).contains('Damage'))
        {
            congaUrl = congaUrl + '&TemplateGroup=' +  cpqSettings.Damage_Conga_Template_Group__c;
        }
        else
        {
           congaUrl = congaUrl + '&TemplateGroup=' +  cpqSettings.RW_Conga_Template_Group__c; 
        }
        system.debug('Conga URL- ' + congaUrl);
        return new PageReference(congaUrl);
        //return null;

    }
    public MandatoryBillingInfoCheck() {
        oppId = ApexPages.currentPage().getParameters().get('id');
        message = 'Redirecting to Conga..';
        isMandatoryInfoMissing = false;
        cpqSettings = CPQSettings__c.getvalues('CPQ Settings');
    }
}