/*------------------------------------------------------------------------
Author:        Viswanatha
Company:       CloudSense
Description:   Test class for MandatoryBillingInfoCheck Class
               1 - This class is to test the code funtionaloty implemented in MadatoryBillingInfoCheck Class
               
Class:           MandatoryBillingInfoCheck_Test
History
<Date>            <Authors Name>    <Brief Description of Change> 
--------------------------------------------------------------------------*/  
@isTest
private class MandatoryBillingInfoCheck_Test {
    
    @isTest static void test_method_one() {

        TestDataClass.CreateTestCPQSettingsData();
        // Create Test Opportunity
        Opportunity objOpp = TestDataClass.CreateTestOpportunityData();

        //Create Account Data
        Account objAccount = TestDataClass.CreateTestAccountData();
        //Create Contact Data
        Contact objContact = TestDataClass.CreateTestContactData();
        //Create Contract Data
        Contract objContract = TestDataClass.CreateTestContractData(objAccount.Id,objContact.Id);
        //Create ContractContactRole Data
        ContractContactRole objContractRole = TestDataClass.CreateTestContractContactRoleData(objContact.Id,objContract.Id);
        //Link Opp with COntract and Leave Account Blanlk
        //objOpp.AccountId = objAccount.Id;
        
        //New
        objOpp.AccountId = objAccount.Id;
        
        objOpp.ContractId = objContract.Id;
        update objOpp;

        ApexPages.currentPage().getParameters().put('id',objOpp.id);
        MandatoryBillingInfoCheck billingInfoCheck = new MandatoryBillingInfoCheck();
        //String nextPage = billingInfoCheck.init().getUrl();
        billingInfoCheck.init();
        system.assert(billingInfoCheck.message != null || billingInfoCheck.message != '');

        //Update objects with Mandatory Billing Information
        objAccount.Customer_Type__c = 'Organisation';
        objAccount.ABN__C = '51824753556';
        update objAccount;
        objContract.BillingStreet = 'Billing Street';
        objContract.BillingCity = 'Melbourne';
        objContract.BillingState = 'VIC';
        objContract.BillingPostalCode = '3000';
        update objContract;
        objContact.Email='test@mailinator.com';
        objContact.Phone = '1234567891';
        objContact.MobilePhone = '0414665478';
        update objContact;
        objOpp.AccountId = objAccount.Id;
        update objOpp;

        //ApexPages.currentPage().getParameters().put('id',objOpp.id);
        billingInfoCheck = new MandatoryBillingInfoCheck();
        //billingInfoCheck.message='';
        billingInfoCheck.init();
        system.assert(billingInfoCheck.message == '');

    }
    
}