/**
* Author:        Arpit Narain
* Company:       NBN
* Description:   schedule apex class to refresh product info for open MAR case with locID
* Test Class:    MarAutoRefreshProductInfoSchedule_Test
* Created   :    07/09/2018
* History
* <Date>        <Authors Name>    <Brief Description of Change>
*/


public class MarAutoRefreshProductInfoSchedule implements Schedulable, Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful {

    Map<String, String> refreshErrorMap = new Map<String, String>();
    List <Case> listOfOpenMarCases = new List<Case>();

    //execute schedulable
    public void execute (SchedulableContext schedulableContext) {

        MarAutoRefreshProductInfoSchedule marAutoRefreshProductInfoSchedule = new MarAutoRefreshProductInfoSchedule();

        //execute 1 at a time , more than that throws error 'You have uncommitted work pending'
        // callouts are not allowed after DML operations in the same transaction
        //refer : https://help.salesforce.com/articleView?id=000003701&type=1
        Database.executeBatch(marAutoRefreshProductInfoSchedule, 1);
    }

    //start batch processing
    public Iterable<SObject> start (Database.BatchableContext batchableContext) {
        String queryString = 'SELECT Id,CaseNumber from Case WHERE RecordType.developerName = \'MAR\' AND IsClosed= FALSE AND Site__r.Location_Id__c != NULL';
        return Database.getQueryLocator(queryString);
    }

    //execute batch process
    public void execute (Database.BatchableContext batchableContext, List<Case> cases) {

        for (Case marCase : cases) {
            listOfOpenMarCases.add(marCase);
            String errors = MarUtil.getProductInfo(marCase.Id);

            if (String.isNotBlank(errors)) {
                refreshErrorMap.put(marCase.CaseNumber, errors);
            }
        }
    }

    //finish batch process
    public void finish (Database.BatchableContext batchableContext) {

        try {
            GlobalUtility.logMessage('Info', 'MarAutoRefreshProductInfoSchedule', 'MarAutoRefreshProductInfoSchedule', '',
                    null, ('List of Errors: ' + JSON.serialize(refreshErrorMap).left(SObjectType.Application_Log__c.fields.Message__c.getLength())), 'Size of Mar Cases processed: ' + listOfOpenMarCases.size(), null, 0);

        } catch (Exception e) {
            GlobalUtility.logMessage('Info', 'MarAutoRefreshProductInfoSchedule', 'MarAutoRefreshProductInfoSchedule', '',
                    null, 'Could not update database because of error', 'Size of Mar Cases processed: ' + listOfOpenMarCases.size(), e, 0);
        }

    }
}