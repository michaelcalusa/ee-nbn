/**
 * Author:        Shuo Li
 * Company:       NBN
 * Description:   Batch apex class for Mar open case auto site re-qualification
 * Test Class:    MarAutoSiteQualification_Test
 * Created   :    06/06/2018
 * History
 * <Date>        <Authors Name>    <Brief Description of Change>
 */
global class MarAutoSiteQualification implements Database.batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    global string  query;
    global set<id> qualifiedSiteIds; 
    global set<id> errorSiteIds;
    global set<String> errorLocationIds;

    global Database.QueryLocator start(Database.BatchableContext BC){
        qualifiedSiteIds = new set<Id>();
        errorSiteIds = new set<Id>();
        errorLocationIds = new set<string>();

        if (string.isBlank(query)) {
            // MD_Service_Setting__c css = MD_Service_Setting__c.getOrgDefaults();
             map<string, string>MDconfigurationMaps = GlobalUtility.getMDConfiguration();             
             string qualifiedLimit = MDconfigurationMaps.get('Mar_Auto_Qualify_Limit');
             string recordTypeNames = string.isblank(MDconfigurationMaps.get('Mar_Auto_Qualify_Record_Type'))?'Medical Alarm':MDconfigurationMaps.get('Mar_Auto_Qualify_Record_Type');
              
             set<Id>recordTypeIds = new set<Id>();
            
             system.debug(string.valueof(recordTypeNames.split(',')));

             for (string Rec: recordTypeNames.split(',')) {
                 if (Schema.SObjectType.case.getRecordTypeInfosByName().containsKey(Rec)) {
                     recordTypeIds.add(Schema.SObjectType.case.getRecordTypeInfosByName().get(Rec).getRecordTypeId());
                 }
             }

            query =  'SELECT Id, Site__c FROM Case WHERE RecordTypeID in :recordTypeIds '+
                '  AND site__c != null and site__r.Location_Id__c!=NULL AND Status != \'Closed\' order by site__r.Qualification_Date__c ';
            if (test.isRunningTest()) {
                query += ' limit 1';
            } else {
                if (!string.isBlank(qualifiedLimit)) {
                    query += ' limit ' + string.valueOf(qualifiedLimit);
                }
            }
        }
        System.debug('Query:' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<case> scope){
        set<id>siteIds = new set<id>();
        for(case s : scope){
            if (!qualifiedSiteIds.contains(s.Site__c) && !errorSiteIds.contains(s.Site__c) ) {
                system.debug(s.Site__c);
                siteIds.add(s.Site__c);
            }
        }
        if (!siteIds.isEmpty()) {
            for (site__c s: [SELECT Id, Name,Serviceability_Class__c, Rollout_Type__c, Technology_Type__c,
                             Location_Id__c, Site_Address__c,Qualification_Date__c,RecordTypeId, Unit_Number__c,
                             Road_Name__c, Level_Number__c, Lot_Number__c, Road_Number_1__c, Road_Number_2__c,
                             Road_Suffix_Code__c,Post_Code__c, Locality_Name__c, State_Territory_Code__c,
                             Requalification_Failed_Date__c, CreatedDate FROM Site__c where id in :siteIds]) {
                SiteQualification_CX SiteQualification_CX_Con = new SiteQualification_CX(new ApexPages.StandardController(s));
                if (!test.isRunningTest()) {
                    SiteQualification_CX_Con.reQualifySiteRecord();
                }
                if (SiteQualification_CX_Con.isErr) {
                    system.debug(s.Id);
                    system.debug(s.Location_Id__c);
                    errorSiteIds.add(s.id);
                    errorLocationIds.add(s.Location_Id__c);
                }else {
                    qualifiedSiteIds.add(s.id);
                }

            }
        }
    }

    global void finish(Database.BatchableContext BC){
        sendNotification();
    }

    public void sendNotification(){
        system.debug('Total Qualified Site:' + string.valueOf(qualifiedSiteIds.size()));
        system.debug('Total Error Site:' + string.valueOf(errorSiteIds.size()));
        system.debug('Total Error Site List:' + errorLocationIds);
       
        
        map<string, string>MDconfigurationMaps = GlobalUtility.getMDConfiguration();  
        string recipients = MDconfigurationMaps.get('Mar_Auto_Qualify_Emails');
        if (string.isBlank(recipients)) {
            recipients = userinfo.getUserEmail();
        }
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = recipients.split(',');
        mail.setToAddresses(toAddresses);
        mail.setSubject('MAR Site Re-qualified Completed');
        mail.setPlainTextBody
        (  'Total Qualified Site: ' + string.valueOf(qualifiedSiteIds.size()) + '; \n\r'
        +  'Total Invalid Location Site: ' + string.valueOf(errorSiteIds.size()) + ';  \n\r'
        +  'Total Invalid Location List: ' + string.valueOf(errorLocationIds) );
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}