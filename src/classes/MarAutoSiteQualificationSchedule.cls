/**
 * Author:        Shuo Li
 * Company:       NBN
 * Description:   schedule apex class for Mar open case auto site re-qualification
 * Test Class:    MarAutoSiteQualification_Test
 * Created   :    06/06/2018
 * History
 * <Date>        <Authors Name>    <Brief Description of Change>
 */
public class MarAutoSiteQualificationSchedule  implements Schedulable{

    public static void execute(SchedulableContext ctx)
    {

        MarAutoSiteQualification MarAutoSiteQualification = new MarAutoSiteQualification();
        MarAutoSiteQualification.query = null;
        Database.executeBatch(MarAutoSiteQualification,1);
    }
}