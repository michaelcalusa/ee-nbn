/**
 * Created by shuol on 7/06/2018.
 */

@IsTest
private class MarAutoSiteQualification_Test {
    @IsTest
    static void testBehavior() {

        createTestData();
        test.startTest();
        MarAutoSiteQualification MarAutoSiteQualification = new MarAutoSiteQualification();
        MarAutoSiteQualification.query = null;
        Database.executeBatch(MarAutoSiteQualification,1);
        test.stopTest();

    }
    @IsTest
    static  void autoSiteQualificationScheduleTest(){
        createTestData();
        test.startTest();
        MarAutoSiteQualificationSchedule a = new MarAutoSiteQualificationSchedule();
        String cronStr = '0 15 * * * ?';
        System.schedule('Testing Schedule Job', cronStr, a);
        test.stopTest();
    }

    static void createTestData(){
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        Site__c UnverifiedSite = new Site__c();
        UnverifiedSite.RecordTypeId=recordTypeId;
        UnverifiedSite.Location_Id__c = 'LOC000106753648';
        UnverifiedSite.isSiteCreationThroughCode__c =true;
        insert UnverifiedSite;


        // Create Contact record
        ID conrecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId();
        Contact contactRec = new Contact ();
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        contactRec.recordtypeid = conrecordTypeId;
        insert contactRec;


        Id marRecTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId();
        Case marcase = new Case(RecordTypeId=marRecTypeId,
                Site__c =  UnverifiedSite.Id,
                ContactId=contactRec.id,
                Case_Concat__c = String.valueof(UnverifiedSite.Id)+String.valueof(contactRec.id));
        insert marcase;      
        

    }
}