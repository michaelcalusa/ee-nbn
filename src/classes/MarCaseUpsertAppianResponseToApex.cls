/***************************************************************************************************
    Class Name          : MarCaseUpsertAppianResponseToApex
    Version             : 1.0
    Created Date        : 05-Jul-2018
    Author              : Arpit Narain
    Description         : Test class for MarContactUpsertFromExternalSystem
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/

public class MarCaseUpsertAppianResponseToApex {

    public String success;
    public String registrationId;
    public String salesforceCaseId;
    public List<Error_z> error;

    public class Error_z {
        public String statusCode;
        public String message;
        public List<String> fields;
    }


    public static List<MarCaseUpsertAppianResponseToApex> parse(String json) {
        return (List<MarCaseUpsertAppianResponseToApex>) System.JSON.deserialize(json, List<MarCaseUpsertAppianResponseToApex>.class);
    }


}