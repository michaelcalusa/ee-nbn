/**
 * Created by shuol on 16/08/2018.
 */
@RestResource(UrlMapping='/marcase/upsert/*')
global with sharing class MarCaseUpsertFromExternalSystem {

    @HttpPost
    global static void marCaseUpsertFromExternalSystem() {

        Blob responseBody;
        Integer count=0;

        MarCaseUpsertFromExternalSystem MarCaseInsertFromExternalSystem = new MarCaseUpsertFromExternalSystem();

        Boolean releaseToggle = GlobalUtility.getReleaseToggle('Release_Toggles__mdt', 'MAR_Case_Upsert_Interface', 'IsActive__c');

        if (releaseToggle == null) {
            releaseToggle = false;
        }
        if (!Test.isRunningTest()) {
            if (!releaseToggle) {
                String responseMessage = 'Cannot process message as toggle is turned off in Salesforce';
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf(MarCaseInsertFromExternalSystem.setCustomErrorResponse(responseMessage));
                GlobalUtility.logMessage('Error', 'MarCaseUpsertFromExternalSystem', 'caseUpsert', '', null, responseMessage, null, null, 0);
                return;
            }
        }

        try {
            String msgBody = RestContext.request.requestBody.toString();
            if (String.isNotBlank(msgBody)) {


                // parse request into list so that each contact can be processed individually
                List<MarDeSerialiseCaseUpsertRequest> MarDeSerialiseCaseUpsertRequests = MarCaseInsertFromExternalSystem.getDeSerialiseCaseUpsertRequest(msgBody);

                if (!MarDeSerialiseCaseUpsertRequests.isEmpty()) {
                    List<MarSerialiseCaseUpsertResponse> MarSerialiseCaseUpsertResponseList = new List<MarSerialiseCaseUpsertResponse>();


                    List<Case> upsertCaseList  = MarCaseInsertFromExternalSystem.createMarCase(MarDeSerialiseCaseUpsertRequests);


                    Schema.SObjectField f = Case.Fields.Mar_External_Id__c;
                    Database.UpsertResult[] srList = Database.upsert(upsertCaseList, f, false);

                    for (Database.UpsertResult upsr : srList) {
                        MarSerialiseCaseUpsertResponseList.add(new MarSerialiseCaseUpsertResponse(upsr,MarDeSerialiseCaseUpsertRequests[count].registrationId));
                        count++;
                    }

                    if (!MarSerialiseCaseUpsertResponseList.isEmpty()) {
                        responseBody = Blob.valueOf(JSON.serialize(MarSerialiseCaseUpsertResponseList));
                        GlobalUtility.logMessage('Info', 'MarCaseUpsertFromExternalSystem', 'caseUpsert', '', null, ' Response Body: ' + String.valueOf(JSON.serialize(MarSerialiseCaseUpsertResponseList)), 'HTTP Request: ' + msgBody, null, 0);

                    } else {
                        responseBody = Blob.valueOf(MarCaseInsertFromExternalSystem.setCustomErrorResponse('Error while processing. Check salesforce logs for further details'));
                    }

                } else {
                    responseBody = Blob.valueOf(MarCaseInsertFromExternalSystem.setCustomErrorResponse('Request not in proper format. Check salesforce logs for further details'));
                    GlobalUtility.logMessage('Error', 'MarCaseUpsertFromExternalSystem', 'caseUpsert', '', null, 'Request not in proper format. Check salesforce logs for further details', 'HTTP Request: ' + msgBody, null, 0);
                }

            } else {
                responseBody = Blob.valueOf(MarCaseInsertFromExternalSystem.setCustomErrorResponse('Request body is empty'));
                GlobalUtility.logMessage('Error', 'MarCaseUpsertFromExternalSystem', 'caseUpsert', '', null, ' Request body is empty', 'HTTP Request: ' + msgBody, null, 0);
            }


        } catch (Exception e) {
            responseBody = Blob.valueOf(MarCaseInsertFromExternalSystem.setCustomErrorResponse('Error while processing message:' + e.getMessage()));
            GlobalUtility.logMessage('Error', 'MarCaseUpsertFromExternalSystem', 'caseUpsert', '', null, 'Error while processing message', 'Error', e, 0);

        }

        RestContext.response.addHeader('Content-Type', 'application/json');
        RestContext.response.responseBody = responseBody;
    }

    public String setCustomErrorResponse(String customErrorMessage) {

        //generate custom error response
        MarCaseUpsertAppianResponseToApex MarCaseUpsertAppianResponseToApex = new MarCaseUpsertAppianResponseToApex();
        MarCaseUpsertAppianResponseToApex.success = 'false';
        MarCaseUpsertAppianResponseToApex.Error_z MarCaseUpsertAppianResponseToApexErrorZ = new MarCaseUpsertAppianResponseToApex.Error_z();
        MarCaseUpsertAppianResponseToApexErrorZ.message = customErrorMessage;
        List<MarCaseUpsertAppianResponseToApex.Error_z> marCaseUpsertCustomError = new List<MarCaseUpsertAppianResponseToApex.Error_z>();
        marCaseUpsertCustomError.add(MarCaseUpsertAppianResponseToApexErrorZ);
        MarCaseUpsertAppianResponseToApex.error = marCaseUpsertCustomError;

        List<MarCaseUpsertAppianResponseToApex> marCaseUpsertAppianResponseToApexs = new List<MarCaseUpsertAppianResponseToApex>();
        MarCaseUpsertAppianResponseToApexs.add(MarCaseUpsertAppianResponseToApex);

        return JSON.serialize(marCaseUpsertAppianResponseToApexs);
    }


    public List<MarDeSerialiseCaseUpsertRequest> getDeSerialiseCaseUpsertRequest(String msgBody) {

        List<MarDeSerialiseCaseUpsertRequest> marDeSerialiseCaseUpsertRequests = new List<MarDeSerialiseCaseUpsertRequest>();

        try {

            marDeSerialiseCaseUpsertRequests = MarDeSerialiseCaseUpsertRequest.parse(msgBody);

        } catch (Exception e) {

            GlobalUtility.logMessage('Error', 'MarCaseUpsertFromExternalSystem', 'getDeSerialiseCaseUpsertRequest', '', null
                    , ' Could not de-serialise request . Request Body: ' + msgBody, '', e, 0);
        }

        return marDeSerialiseCaseUpsertRequests;
    }


    private map <String, Id> createSite (List<MarDeSerialiseCaseUpsertRequest> MarDeSerialiseCaseUpsertRequests) {
        Set<String> locationIdSet = new Set<String>();
        map<String,Id> locationIdMaps  = new map<String,Id>();

        String unverifiedRecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Site__c'  and name = 'Unverified'].Id;

        // Collect all location Id's
        for (MarDeSerialiseCaseUpsertRequest marDeSerialiseCaseUpsertRequest : MarDeSerialiseCaseUpsertRequests) {
            if (String.isNotBlank(marDeSerialiseCaseUpsertRequest.locationId))
                locationIdSet.add(marDeSerialiseCaseUpsertRequest.locationId);
        }
        // Query existing sites. put the loc Id as key with Site Id as value
        for (Site__c site : [SELECT Id, Location_Id__c FROM Site__c WHERE Location_Id__c IN :locationIdSet AND (RecordType.developername = 'Unverified' OR RecordType.developername = 'Verified') FOR UPDATE]) {
            locationIdMaps.put(site.Location_Id__c, site.id);
        }

        list<Site__c>upsertSiteList =new list<Site__c>();
        Set<String> duplicateLocationCheck = new Set<String>();
        for (MarDeSerialiseCaseUpsertRequest marDeSerialiseCaseUpsertRequest : MarDeSerialiseCaseUpsertRequests) {
            system.debug('marDeSerialiseCaseUpsertRequest Location Id:  '+ marDeSerialiseCaseUpsertRequest.locationId);
            if (String.isNotBlank(marDeSerialiseCaseUpsertRequest.locationId)) {
                if (!duplicateLocationCheck.contains(marDeSerialiseCaseUpsertRequest.locationId)) {
                    if (!locationIdMaps.containsKey(marDeSerialiseCaseUpsertRequest.locationId)){
                        Site__c createSite = new Site__c();
                        createSite.Name = marDeSerialiseCaseUpsertRequest.locationId;
                        createSite.Location_Id__c = marDeSerialiseCaseUpsertRequest.locationId;
                        createSite.Disconnection_Date__c =  MarUtil.getDate(marDeSerialiseCaseUpsertRequest.disconnectionDate);
                        createSite.MAR__c = true;
                        createSite.RecordTypeId = unverifiedRecordTypeId;
                        upsertSiteList.add(createSite);
                    } else {
                        Site__c updateSite = new Site__c();
                        updateSite.Id = locationIdMaps.get(marDeSerialiseCaseUpsertRequest.locationId);
                        updateSite.Disconnection_Date__c = MarUtil.getDate(marDeSerialiseCaseUpsertRequest.disconnectionDate) ;
                        updateSite.MAR__c = true;
                        upsertSiteList.add(updateSite);
                    }
                    duplicateLocationCheck.add(marDeSerialiseCaseUpsertRequest.locationId);
                }

            }
        }
        system.debug('upsertSiteList:  '+ upsertSiteList);
        if (!upsertSiteList.isEmpty()){
            Schema.SObjectField f = Site__c.fields.Id;
            Database.UpsertResult[] upsertResults = Database.upsert(upsertSiteList, f, false);


            for (Database.UpsertResult upsertResult : upsertResults) {
                system.debug('upsertResult:  '+ upsertResult);
                if (!upsertResult.isSuccess()) {
                    GlobalUtility.logMessage('Info', 'MarCaseUpsertFromExternalSystem', 'SiteUpsert', '', null, ' Error Body: ' + String.valueOf(upsertResult.getErrors()), null, null, 0);
                }
            }

            locationIdMaps = new  map<String,Id>();
            for (Site__c site : [SELECT Id, Location_Id__c FROM Site__c WHERE Location_Id__c IN :locationIdSet AND (RecordType.developername = 'Unverified' OR RecordType.developername = 'Verified') FOR UPDATE]) {
                locationIdMaps.put(site.Location_Id__c, site.id);
            }
        }
        return locationIdMaps;
    }


    private List<case>  createMarCase(List<MarDeSerialiseCaseUpsertRequest> MarDeSerialiseCaseUpsertRequests) {
        List<Case> upsertCaseList =  new list<Case>();
        /***** Get Location Id, Create/Update Location if Not in System******/
        map <String, Id> locationIdMaps = createSite(MarDeSerialiseCaseUpsertRequests);

        set<String> processIds = new set<String>();
        for (MarDeSerialiseCaseUpsertRequest marDeSerialiseCaseUpsertRequest : MarDeSerialiseCaseUpsertRequests) {
            processIds.add(marDeSerialiseCaseUpsertRequest.registrationId);
        }


        /**************************Case Upsert */
        Map<String, Case> CaseMap = new Map<String, Case>();
        Id marRecTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId();
        System.debug('--marRecTypeId --' + marRecTypeId);

        for (Case cst : [select Status,MAR_Sub_Status__c,Case_Concat__c,Rollout_Group__c,RecordTypeID,ContactID,Secondary_Contact__c,Site__c,
                                Case_Type__c,EATLA__c,Record_Provided_By__c,Date_Provided__c,Alarm_Type__c,Copper_Active__c,NBN_Active__c,Inflight_Order__c,
                                ASP__c,case.id,MASS_SUMMARY_STATUS__c,MASS_SUMMARY_INELIGIBILITY_REASON__c,ASP_ALARM_TECHNOLOGY__c,ASP_VILLAGE_NAME__c,
                                ASP_VILLAGE_RESIDENT__c,Primary_Contact_Role__c,Secondary_Contact_Role__c,Alarm_Brand__c,Alarm_Brand_if_other_brand__c,
                                Who_does_the_alarm_call__c,EU_UMAP_Enquiry__c,Power_Outage_Message_Provided__c,UMA_Migration_Options_Provided__c,
                                EU_Premises_Serviceability_Message_Provi__c,EU_UMAP_Enquiry_Timestamp__c,EU_Interest_in_UMAP__c,
                                EU_Interest_in_UMAP_Timestamp__c,EU_Eligible__c,EU_Eligible_Timestamp__c,EU_Eligible_History__c,
                                Reason_for_Ineligibility__c,Active_Escalation_with_nbn__c,Active_Escalation_with_nbn_Start_Timesta__c,
                                Escalation_Notes__c,Informed_Consent_Signed__c,Consent_Timestamp__c,Successful_Installation_Validated__c,
                                Successful_Installation_Validated_Timest__c,New_Device_Wireless_Only__c,Skybridge_UMA_Migration_Decision__c,
                                Skybridge_UMA_Migration_Decision_Timesta__c,Skybridge_UMA_Migration_Decision_History__c,Mar_External_Id__c,
                                Activated_Date__c,CSLL_ACTIVE__c,CSLL_Termination_Date__c,MAR_Order_Status__c,BATTERY_BACKUP_SERVICE_BBU__c,
                                Voice_RSP__c,NBN_RSP__c,Product_Info_Last_Refresh_Date__c, Priority
                                from case where RecordType.developername = 'MAR' and Mar_External_Id__c in :processIds and Mar_External_Id__c != null]) {
            CaseMap.put(cst.Mar_External_Id__c, cst); // Query for the cases where the Case_Concat(Unique Field) is not NULL
            System.debug ('existingCaseMap values' + CaseMap);
        }

        for (MarDeSerialiseCaseUpsertRequest marCaseRequest : MarDeSerialiseCaseUpsertRequests) {
            Case upsertCase = new case();
            if (CaseMap.containsKey(marCaseRequest.registrationId)) {
                upsertCase = CaseMap.get(marCaseRequest.registrationId); //get Case values from Case map
                upsertCase.RecordTypeID = marRecTypeId;
                upsertCase.ContactID = String.isNotBlank(marCaseRequest.primaryContact)?marCaseRequest.primaryContact:null;

                if (String.isNotBlank(marCaseRequest.secondaryContact)) {
                    upsertCase.Secondary_Contact__c = marCaseRequest.secondaryContact;
                }
                upsertCase.Site__c = locationIdMaps.containsKey(marCaseRequest.locationId) ? locationIdMaps.get(marCaseRequest.locationId) : null;
                upsertCase.Case_Type__c = marCaseRequest.caseType;
                upsertCase.Record_Provided_By__c = marCaseRequest.recordProvidedBy;
                upsertCase.Date_Provided__c = MarUtil.getDate(marCaseRequest.dateProvided);
                upsertCase.Copper_Active__c = marCaseRequest.copperActive;
                upsertCase.NBN_Active__c = marCaseRequest.nbnActive;
                upsertCase.Inflight_Order__c = marCaseRequest.inflightOrder;

                upsertCase.Activated_Date__c = MarUtil.getDatetimeGmt(marCaseRequest.nbnActivatedDate);
                upsertCase.CSLL_ACTIVE__c = marCaseRequest.csllStatus;
                upsertCase.CSLL_Termination_Date__c = MarUtil.getDatetimeGmt(marCaseRequest.csllTerminatedDate);
                upsertCase.MAR_Order_Status__c = marCaseRequest.orderStatus;
                upsertCase.BATTERY_BACKUP_SERVICE_BBU__c = marCaseRequest.batteryBackupService;
                upsertCase.Voice_RSP__c = marCaseRequest.rsp1;
                upsertCase.NBN_RSP__c = marCaseRequest.rsp2;
                upsertCase.Product_Info_Last_Refresh_Date__c = MarUtil.getDatetimeGmt(marCaseRequest.productInfoRefreshDate);
                upsertCase.Case_Concat__c = marCaseRequest.locationId + 'REG'  + marCaseRequest.registrationId;
                upsertCase.MASS_SUMMARY_STATUS__c = marCaseRequest.massSummaryStatus;
                upsertCase.MASS_SUMMARY_INELIGIBILITY_REASON__c = marCaseRequest.massSummaryIneligibilityReason;
                upsertCase.ASP_ALARM_TECHNOLOGY__c = marCaseRequest.aspAlarmTechnology;
                upsertCase.ASP_VILLAGE_NAME__c = marCaseRequest.aspVillageName;
                upsertCase.ASP_VILLAGE_RESIDENT__c = marCaseRequest.aspVillageResident;
                upsertCase.Priority = marCaseRequest.priority;

                if (upsertCase.Primary_Contact_Role__c == null) {
                    upsertCase.Primary_Contact_Role__c = MarUtil.alarmOwner;
                }
                if (upsertCase.Secondary_Contact_Role__c == null) {
                    upsertCase.Secondary_Contact_Role__c = MarUtil.preferredContact;
                }

                upsertCaseList.add(upsertCase);
                System.debug('*******if part of existing marCaseStageUpdate**' + upsertCase);
            } else {
                Case newCase = new Case();
                newCase.Status = 'New';
                newCase.MAR_Sub_Status__c = 'Not Yet Called';
                newCase.Mar_External_Id__c = marCaseRequest.registrationId;
                newCase.RecordTypeID = marRecTypeId;
                newCase.ContactID = String.isNotBlank(marCaseRequest.primaryContact)?marCaseRequest.primaryContact:null ;
                newCase.Secondary_Contact__c = String.isNotBlank(marCaseRequest.secondaryContact)?marCaseRequest.secondaryContact:null;
                newCase.Site__c = locationIdMaps.containsKey(marCaseRequest.locationId) ? locationIdMaps.get(marCaseRequest.locationId) : null;
                newCase.Case_Type__c = marCaseRequest.caseType;
                newCase.Record_Provided_By__c = marCaseRequest.recordProvidedBy;
                newCase.Date_Provided__c = MarUtil.getDate(marCaseRequest.dateProvided);
                newCase.Alarm_Type__c = marCaseRequest.alarmType;
                newCase.Copper_Active__c = marCaseRequest.copperActive;
                newCase.NBN_Active__c = marCaseRequest.nbnActive;
                newCase.Inflight_Order__c = marCaseRequest.inflightOrder;


                newCase.Activated_Date__c = MarUtil.getDatetimeGmt(marCaseRequest.nbnActivatedDate);
                newCase.CSLL_ACTIVE__c = marCaseRequest.csllStatus;
                newCase.CSLL_Termination_Date__c = MarUtil.getDatetimeGmt(marCaseRequest.csllTerminatedDate);
                newCase.MAR_Order_Status__c = marCaseRequest.orderStatus;
                newCase.BATTERY_BACKUP_SERVICE_BBU__c = marCaseRequest.batteryBackupService;
                newCase.Voice_RSP__c = marCaseRequest.rsp1;
                newCase.NBN_RSP__c = marCaseRequest.rsp2;
                newCase.Product_Info_Last_Refresh_Date__c = MarUtil.getDatetimeGmt(marCaseRequest.productInfoRefreshDate);
                newCase.Case_Concat__c = marCaseRequest.locationId + 'REG'  + marCaseRequest.registrationId;
                newCase.MASS_SUMMARY_STATUS__c = marCaseRequest.massSummaryStatus;
                newCase.MASS_SUMMARY_INELIGIBILITY_REASON__c = marCaseRequest.massSummaryIneligibilityReason;
                newCase.ASP_ALARM_TECHNOLOGY__c = marCaseRequest.aspAlarmTechnology;
                newCase.ASP_VILLAGE_NAME__c = marCaseRequest.aspVillageName;
                newCase.ASP_VILLAGE_RESIDENT__c = marCaseRequest.aspVillageResident;
                newCase.Alarm_Brand__c = marCaseRequest.alarmBrand;
                newCase.Alarm_Brand_if_other_brand__c = marCaseRequest.alarmBrandIfOtherBrand;
                newCase.Who_does_the_alarm_call__c = marCaseRequest.whoDoesTheAlarmCall;
                newCase.Priority = marCaseRequest.priority;

                if (newCase.Primary_Contact_Role__c == null) {
                    newCase.Primary_Contact_Role__c = MarUtil.alarmOwner;
                }
                if (newCase.Secondary_Contact_Role__c == null) {
                    newCase.Secondary_Contact_Role__c = MarUtil.preferredContact;
                }

                System.debug('--New Case value--' + newCase);
                upsertCaseList.add(newCase);
            }
        }
        return upsertCaseList;
    }

}