/***************************************************************************************************
    Class Name          : MarContactUpsertAppianResponseToApex
    Version             : 1.0
    Created Date        : 05-Jul-2018
    Author              : Arpit Narain
    Description         : Test class for MarContactUpsertFromExternalSystem
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/

public class MarContactUpsertAppianResponseToApex {

    public String success;
    public String index;
    public String id;
    public List<Error_z> error;

    public class Error_z {
        public String statusCode;
        public String message;
        public List<String> fields;
    }


    public static List<MarContactUpsertAppianResponseToApex> parse(String json) {
        return (List<MarContactUpsertAppianResponseToApex>) System.JSON.deserialize(json, List<MarContactUpsertAppianResponseToApex>.class);
    }

}