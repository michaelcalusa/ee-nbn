/***************************************************************************************************
    Class Name          : MarContactUpsertFromExternalSystem
    Version             : 1.0
    Created Date        : 03-Jul-2018
    Author              : Arpit Narain
    Description         : Class to create or update contacts from Appian
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/

//exposed resource
@RestResource(UrlMapping='/marcontactupsert/*')
global class MarContactUpsertFromExternalSystem {

    @HttpPost
    global static void contactUpsert() {

        MarContactUpsertFromExternalSystem marContactUpsertFromExternalSystem = new MarContactUpsertFromExternalSystem();
        List<SerialiseContactUpsertResponse> serialiseContactUpsertResponseList = new List<SerialiseContactUpsertResponse>();

        RestResponse restResponse = RestContext.response;
        restResponse.addHeader('Content-Type', 'application/json');

        Boolean releaseToggle = GlobalUtility.getReleaseToggle('Release_Toggles__mdt', 'MAR_Contact_Upsert_Interface', 'IsActive__c');

        if (releaseToggle == null) {
            releaseToggle = false;
        }

        if (!Test.isRunningTest()){
            if (!releaseToggle) {
                String responseMessage = 'Cannot process message as toggle is turned off in Salesforce';
                marContactUpsertFromExternalSystem.setResponse(restResponse, responseMessage, marContactUpsertFromExternalSystem, null, null);
                RestContext.response = restResponse;
                return;
            }
        }



        if (RestContext.request.requestBody == null || String.isBlank(RestContext.request.requestBody.toString())) {
            String responseMessage = 'Request body is empty';
            marContactUpsertFromExternalSystem.setResponse(restResponse, responseMessage, marContactUpsertFromExternalSystem, null, null);
            RestContext.response = restResponse;
            return;
        }

        String msgBody = RestContext.request.requestBody.toString();

        try {

            // parse request into list so that each contact can be processed individually
            List<DeSerialiseContactUpsertRequest> deSerialiseContactUpsertRequests = marContactUpsertFromExternalSystem.getDeSerialiseContactUpsertRequest(msgBody);

            if (deSerialiseContactUpsertRequests.isEmpty()) {
                String responseMessage = 'Request not in proper format. Check salesforce logs for further details';
                marContactUpsertFromExternalSystem.setResponse(restResponse, responseMessage, marContactUpsertFromExternalSystem, msgBody, null);
                RestContext.response = restResponse;
                return;

            }

            // processing can be done in loop for this case as Appian will at most send 25 requests in one batch
            // hopefully, we don't exceed DML limits
            for (DeSerialiseContactUpsertRequest contactForUpsert : deSerialiseContactUpsertRequests) {
                processContactRequest(contactForUpsert, serialiseContactUpsertResponseList);
            }

            if (serialiseContactUpsertResponseList.isEmpty()) {
                String responseMessage = 'Error while processing. Check salesforce logs for further details';
                marContactUpsertFromExternalSystem.setResponse(restResponse, responseMessage, marContactUpsertFromExternalSystem, msgBody, null);
                RestContext.response = restResponse;
                return;
            }

            restResponse.responseBody = Blob.valueOf(JSON.serialize(serialiseContactUpsertResponseList));

            GlobalUtility.logMessage('Info', 'MarContactUpsertFromExternalSystem', 'contactUpsert', '', null, ' Response Body: ' + restResponse.responseBody.toString(), 'HTTP Request: ' + msgBody, null, 0);
            RestContext.response = restResponse;

        } catch (Exception e) {
            String responseMessage = 'Error while processing message';
            marContactUpsertFromExternalSystem.setResponse(restResponse, responseMessage, marContactUpsertFromExternalSystem, msgBody, e);
            RestContext.response = restResponse;
        }
    }

    public  void setResponse (RestResponse restResponse, String responseMessage, MarContactUpsertFromExternalSystem marContactUpsertFromExternalSystem, String msgBody, Exception exceptionObj) {
        GlobalUtility.logMessage('Error', 'MarContactUpsertFromExternalSystem', 'contactUpsert', '', null, responseMessage, 'HTTP Request: ' + msgBody, exceptionObj, 0);
        restResponse.responseBody =  Blob.valueOf(marContactUpsertFromExternalSystem.setCustomErrorResponse(responseMessage));
    }


    public String setCustomErrorResponse(String customErrorMessage) {

        //generate custom error response
        MarContactUpsertAppianResponseToApex marContactUpsertAppianResponseToApex = new MarContactUpsertAppianResponseToApex();
        marContactUpsertAppianResponseToApex.success = 'false';
        MarContactUpsertAppianResponseToApex.Error_z marContactUpsertAppianResponseToApexErrorZ = new MarContactUpsertAppianResponseToApex.Error_z();
        marContactUpsertAppianResponseToApexErrorZ.statusCode = 'Error';
        marContactUpsertAppianResponseToApexErrorZ.message = customErrorMessage;
        List<MarContactUpsertAppianResponseToApex.Error_z> marContactUpsertCustomError = new List<MarContactUpsertAppianResponseToApex.Error_z>();
        marContactUpsertCustomError.add(marContactUpsertAppianResponseToApexErrorZ);
        marContactUpsertAppianResponseToApex.error = marContactUpsertCustomError;

        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = new List<MarContactUpsertAppianResponseToApex>();
        marContactUpsertAppianResponseToApexes.add(marContactUpsertAppianResponseToApex);

        return JSON.serialize(marContactUpsertAppianResponseToApexes);
    }


    public static void processContactRequest(DeSerialiseContactUpsertRequest contactForUpsert, List<SerialiseContactUpsertResponse> serialiseContactUpsertResponseList) {

        // pre-existing method returns list, so this is list
        List<Contact> upsertContactList = new List<Contact>();

        MarContactUpsertFromExternalSystem marContactUpsertFromExternalSystem = new MarContactUpsertFromExternalSystem();

        MARSingleFormUtil.ContactWrap conPack = marContactUpsertFromExternalSystem.getContactToUpsert(contactForUpsert);
        upsertContactList = conPack.upsertContactList;

        //not to be updated
        for (Contact cont: upsertContactList){
            if (cont.Id != null && !contactForUpsert.updateContact){
                SerialiseContactUpsertResponse serialiseContactUpsertResponse = new SerialiseContactUpsertResponse();
                serialiseContactUpsertResponse.success = 'true';
                serialiseContactUpsertResponse.index = contactForUpsert.index;
                serialiseContactUpsertResponse.id = cont.Id;
                serialiseContactUpsertResponse.error = new List<Database.Error>();
                serialiseContactUpsertResponseList.add(serialiseContactUpsertResponse);
                return;
            }
        }

        // Contact DML Statement
        if (upsertContactList.size() > 0) {
            Schema.SObjectField f = Contact.fields.Id;

            Database.UpsertResult[] upsertResults = Database.upsert(upsertContactList, f, false);

            for (Database.UpsertResult upsertResult : upsertResults) {
                //add to response list along with index
                serialiseContactUpsertResponseList.add(new SerialiseContactUpsertResponse(upsertResult, contactForUpsert.index));
            }
        }
    }


    public MARSingleFormUtil.ContactWrap getContactToUpsert(DeSerialiseContactUpsertRequest contactForUpsert) {

        String firstName = contactForUpsert.firstName;
        String lastName = contactForUpsert.lastName;
        Map<String, Contact> existingMARContactMap = new Map<String, Contact>();
        Map<String, Contact> existingNonMARContactMap = new Map<String, Contact>();
        Map<String, Contact> existingMARContactEmailPhoneMap = new Map<String, Contact>();
        Map<String, Contact> existingNonMARContactEmailPhoneMap = new Map<String, Contact>();
        Id externalContactRecTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME);

        String fullName = getFullName(firstName, lastName);

        System.debug('MarContactUpsertFromExternalSystem :: getContactToUpsert :: fullName is ' + fullName);

        if (String.isNotBlank(fullName)) {

            String trimmedFullName = fullName.trim();

            //get exiting contacts based on full Contact names
            for (Contact con : [SELECT Id,Name,Email,Preferred_Phone__c,MAR__c FROM Contact WHERE Name = :trimmedFullName AND RecordTypeId = :externalContactRecTypeId]) {
                System.debug('MarContactUpsertFromExternalSystem :: getContactToUpsert :: got existing contact' + con.Id);
                mapExistingContact(fullName, con, existingMARContactMap, existingNonMARContactMap, existingMARContactEmailPhoneMap, existingNonMARContactEmailPhoneMap);
            }
        }

        System.debug('MarContactUpsertFromExternalSystem :: getContactToUpsert :: existingMARContactMap is ' + existingMARContactMap);
        System.debug('MarContactUpsertFromExternalSystem :: getContactToUpsert :: existingNonMARContactMap is ' + existingNonMARContactMap);
        System.debug('MarContactUpsertFromExternalSystem :: getContactToUpsert :: existingMARContactEmailPhoneMap is ' + existingMARContactEmailPhoneMap);
        System.debug('MarContactUpsertFromExternalSystem :: getContactToUpsert :: existingNonMARContactEmailPhoneMap is ' + existingNonMARContactEmailPhoneMap);

        MARSingleFormUtil.ContactWrap conWrap = new MARSingleFormUtil.ContactWrap(firstName, lastName,
                contactForUpsert.phoneNo, contactForUpsert.email, contactForUpsert.alternatePhoneNo, new Set<Id>(), new Map<String, Contact>(),
                existingMARContactMap, existingNonMARContactMap, existingMARContactEmailPhoneMap, existingNonMARContactEmailPhoneMap, new List<Contact>());

        return MARSingleFormUtil.createExtractMARContact(conWrap);
    }


    public List<DeSerialiseContactUpsertRequest> getDeSerialiseContactUpsertRequest(String msgBody) {

        List<DeSerialiseContactUpsertRequest> deSerialiseContactUpsertRequests = new List<DeSerialiseContactUpsertRequest>();

        try {

            deSerialiseContactUpsertRequests = DeSerialiseContactUpsertRequest.parse(msgBody);

        } catch (Exception e) {

            GlobalUtility.logMessage('Error', 'MarContactUpsertFromExternalSystem', 'getDeSerialiseContactUpsertRequest', '', null
                    , ' Could not de-serialise request . Request Body: ' + msgBody, '', e, 0);

        }

        return deSerialiseContactUpsertRequests;
    }


    public void mapExistingContact(String fullName, Contact con, Map<String, Contact> existingMARContactMap, Map<String, Contact> existingNonMARContactMap, Map<String, Contact> existingMARContactEmailPhoneMap, Map<String, Contact> existingNonMARContactEmailPhoneMap) {

        Boolean isMarContact = con.MAR__c;
        //TO-DO: This can be simplified after writing unit tests covering all scenarios

        if (con.Email != null && con.Preferred_Phone__c == null) {

            if (isMarContact) {

                System.debug('MarContactUpsertFromExternalSystem :: mapExistingContact :: existingMARContactMap key is ' + fullName + con.Email);
                existingMARContactMap.put(fullName + con.Email, con);

            } else {
                System.debug('MarContactUpsertFromExternalSystem :: mapExistingContact :: existingNonMARContactMap key is ' + fullName + con.Email);
                existingNonMARContactMap.put(fullName + con.Email, con);

            }
        } else if (con.Preferred_Phone__c != null && con.Email == null) {

            if (isMarContact) {
                existingMARContactMap.put(fullName + con.Preferred_Phone__c.replaceAll('\\D', ''), con);
            } else {

                existingNonMARContactMap.put(fullName + con.Preferred_Phone__c.replaceAll('\\D', ''), con);
            }
        } else if (con.Email != null && con.Preferred_Phone__c != null) {

            if (isMarContact) {

                existingMARContactEmailPhoneMap.put(fullName + con.Email, con);
                existingMARContactEmailPhoneMap.put(fullName + con.Preferred_Phone__c.replaceAll('\\D', ''), con);

            } else {

                existingNonMARContactEmailPhoneMap.put(fullName + con.Email, con);
                existingNonMARContactEmailPhoneMap.put(fullName + con.Preferred_Phone__c.replaceAll('\\D', ''), con);
            }
        }
    }


    public String getFullName(String firstName, String lastName) {

        String fullName;

        if (String.isNotBlank(firstName)) {

            fullName = firstName.toLowerCase() + ' ' + lastName.toLowerCase();

        } else if (String.isNotBlank(lastName)) {

            //as this needs to matched with existing method MARSingleFormUtil.createExtractMARContact
            fullName =  ' ' + lastName.toLowerCase();
        }
        return fullName;
    }

}