/***************************************************************************************************
    Class Name          : MarContactUpsertFromExternalSystemTest
    Version             : 1.0
    Created Date        : 05-Jul-2018
    Author              : Arpit Narain
    Description         : Test class for AMarContactUpsertFromExternalSystem
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/
@IsTest
public class MarContactUpsertFromExternalSystemTest {

    @IsTest
    public static void checkContactUpsertForDuplicateContactsThenOnlyOneContactIsCreated() {
        String inputJson = '[\n' + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "and@tes.com",\n' + '        "firstName": "TOM",\n'
                + '        "id": "01",\n' + '        "lastName": "JERRY",\n'
                + '        "phoneNo": "0430676567",\n' + '        "salesforceId": ""\n' + '    },\n'
                + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "nd@tes.com",\n' + '        "firstName": " TOM",\n'
                + '        "id": "02",\n' + '        "lastName": "JERRY",\n'
                + '        "phoneNo": "0430676567",\n' + '        "salesforceId": ""\n' + '    }\n'
                + ']';

        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/marcontactupsert';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(inputJson);
        RestContext.request = request;
        RestContext.response = response;
        MarContactUpsertFromExternalSystem.contactUpsert();

        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = MarContactUpsertAppianResponseToApex.parse(RestContext.response.responseBody.toString());
        System.assertEquals(marContactUpsertAppianResponseToApexes[0].id, marContactUpsertAppianResponseToApexes[1].id);


    }

    @IsTest
    public static void checkContactIfNoValueTheThrowsErrorMessage() {
        String inputJson = '[\n' + '    {\n' + '        "alternatePhoneNo": "",\n'
                + '        "email": "",\n' + '        "firstName": "",\n'
                + '        "id": "",\n' + '        "lastName": "",\n'
                + '        "phoneNo": "",\n' + '        "salesforceId": ""\n' + '    },\n'
                + '    {\n' + '        "alternatePhoneNo": "",\n'
                + '        "email": "",\n' + '        "firstName": "",\n'
                + '        "id": "",\n' + '        "lastName": "",\n'
                + '        "phoneNo": "",\n' + '        "salesforceId": ""\n' + '    }\n'
                + ']';

        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/marcontactupsert';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(inputJson);
        RestContext.request = request;
        RestContext.response = response;
        MarContactUpsertFromExternalSystem.contactUpsert();

        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = MarContactUpsertAppianResponseToApex.parse(RestContext.response.responseBody.toString());
        System.assertEquals(marContactUpsertAppianResponseToApexes[0].id, marContactUpsertAppianResponseToApexes[1].id);

    }

    @IsTest
    public static void checkContactIfIncorrectJSONFormatThrowsErrorMessage() {
        String inputJson = '<<<<<>>>XML[\n' + '    {\n' + '        "alternatePhoneNo": "",\n'
                + '        "email": "",\n' + '        "firstName": "",\n'
                + '        "id": "",\n' + '        "lastName": "",\n'
                + '        "phoneNo": "",\n' + '        "salesforceId": ""\n' + '    },\n'
                + '    {\n' + '        "alternatePhoneNo": "",\n'
                + '        "email": "",\n' + '        "firstName": "",\n'
                + '        "id": "",\n' + '        "lastName": "",\n'
                + '        "phoneNo": "",\n' + '        "salesforceId": ""\n' + '    }\n'
                + ']';

        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/marcontactupsert';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(inputJson);
        RestContext.request = request;
        RestContext.response = response;
        MarContactUpsertFromExternalSystem.contactUpsert();

        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = MarContactUpsertAppianResponseToApex.parse(RestContext.response.responseBody.toString());
        System.assertEquals('Request not in proper format. Check salesforce logs for further details', marContactUpsertAppianResponseToApexes[0].error[0].message);

    }

    @IsTest
    public static void checkContactIfRequestBodyIsEmptyThenThrowsErrorMessage() {
        String inputJson = '';

        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/marcontactupsert';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(inputJson);
        RestContext.request = request;
        RestContext.response = response;
        MarContactUpsertFromExternalSystem.contactUpsert();

        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = MarContactUpsertAppianResponseToApex.parse(RestContext.response.responseBody.toString());
        System.assertEquals('Request body is empty', marContactUpsertAppianResponseToApexes[0].error[0].message);

    }

    @IsTest
    public static void checkContactUpsertForExistingMARWithEmailAndPhoneNullThenContactIsNotCreated() {

        Contact testContact = new Contact(FirstName = 'Test',
                LastName = 'User',
                Email = 'TestUser@test.com',
                MAR__c = true,
                RecordTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME));
        insert testContact;

        String inputJson = '[\n' + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "testuser@test.com",\n' + '        "firstName": "Test",\n'
                + '        "id": "01",\n' + '        "lastName": "User",\n'
                + '        "phoneNo": "",\n' + '        "salesforceId": ""\n' + '    },\n'
                + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "tEStuser@test.com",\n' + '        "firstName": " TEST",\n'
                + '        "id": "02",\n' + '        "lastName": "USER",\n'
                + '        "phoneNo": "",\n' + '        "salesforceId": ""\n' + '    }\n'
                + ']';
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/marcontactupsert';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(inputJson);
        RestContext.request = request;
        RestContext.response = response;
        MarContactUpsertFromExternalSystem.contactUpsert();

        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = MarContactUpsertAppianResponseToApex.parse(RestContext.response.responseBody.toString());
        System.assertEquals(testContact.Id, marContactUpsertAppianResponseToApexes[0].id);
        System.assertEquals(marContactUpsertAppianResponseToApexes[0].id, marContactUpsertAppianResponseToApexes[1].id);

    }

    @IsTest
    public static void checkContactUpsertForExistingNonMARWithEmailAndPhoneNullThenContactIsNotCreated() {

        Contact testContact = new Contact(FirstName = 'Test',
                LastName = 'User',
                Email = 'TestUser@test.com',
                MAR__c = false,
                RecordTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME));
        insert testContact;

        String inputJson = '[\n' + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "testuser@test.com",\n' + '        "firstName": "Test",\n'
                + '        "id": "01",\n' + '        "lastName": "User",\n'
                + '        "phoneNo": "",\n' + '        "salesforceId": ""\n' + '    },\n'
                + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "tEStuser@test.com",\n' + '        "firstName": " TEST",\n'
                + '        "id": "02",\n' + '        "lastName": "USER",\n'
                + '        "phoneNo": "",\n' + '        "salesforceId": ""\n' + '    }\n'
                + ']';
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/marcontactupsert';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(inputJson);
        RestContext.request = request;
        RestContext.response = response;
        MarContactUpsertFromExternalSystem.contactUpsert();

        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = MarContactUpsertAppianResponseToApex.parse(RestContext.response.responseBody.toString());
        System.assertEquals(testContact.Id, marContactUpsertAppianResponseToApexes[0].id);
        System.assertEquals(marContactUpsertAppianResponseToApexes[0].id, marContactUpsertAppianResponseToApexes[1].id);

    }

    @IsTest
    public static void checkContactUpsertForExistingNonMARWithEmailIsNullAndPhoneIsNotNullThenContactIsNotCreated() {

        Contact testContact = new Contact(FirstName = 'Test',
                LastName = 'User',
                Email = '',
                Preferred_Phone__c = '1234567890',
                MAR__c = false,
                RecordTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME));
        insert testContact;

        String inputJson = '[\n' + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "",\n' + '        "firstName": "Test",\n'
                + '        "id": "01",\n' + '        "lastName": "User",\n'
                + '        "phoneNo": "1234567890",\n' + '        "salesforceId": ""\n' + '    },\n'
                + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "",\n' + '        "firstName": " TEST",\n'
                + '        "id": "02",\n' + '        "lastName": "USER",\n'
                + '        "phoneNo": "1234567890",\n' + '        "salesforceId": ""\n' + '    }\n'
                + ']';
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/marcontactupsert';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(inputJson);
        RestContext.request = request;
        RestContext.response = response;
        MarContactUpsertFromExternalSystem.contactUpsert();

        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = MarContactUpsertAppianResponseToApex.parse(RestContext.response.responseBody.toString());
        System.assertEquals(testContact.Id, marContactUpsertAppianResponseToApexes[0].id);
        System.assertEquals(marContactUpsertAppianResponseToApexes[0].id, marContactUpsertAppianResponseToApexes[1].id);

    }

    @IsTest
    public static void checkContactUpsertForExistingMARWithEmailIsNullAndPhoneIsNotNullThenContactIsNotCreated() {

        Contact testContact = new Contact(FirstName = 'Test',
                LastName = 'User',
                Email = '',
                Preferred_Phone__c = '1234567890',
                MAR__c = true,
                RecordTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME));
        insert testContact;

        String inputJson = '[\n' + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "",\n' + '        "firstName": "Test",\n'
                + '        "id": "01",\n' + '        "lastName": "User",\n'
                + '        "phoneNo": "1234567890",\n' + '        "salesforceId": ""\n' + '    },\n'
                + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "",\n' + '        "firstName": " TEST",\n'
                + '        "id": "02",\n' + '        "lastName": "USER",\n'
                + '        "phoneNo": "1234567890",\n' + '        "salesforceId": ""\n' + '    }\n'
                + ']';
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/marcontactupsert';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(inputJson);
        RestContext.request = request;
        RestContext.response = response;
        MarContactUpsertFromExternalSystem.contactUpsert();

        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = MarContactUpsertAppianResponseToApex.parse(RestContext.response.responseBody.toString());
        System.assertEquals(testContact.Id, marContactUpsertAppianResponseToApexes[0].id);
        System.assertEquals(marContactUpsertAppianResponseToApexes[0].id, marContactUpsertAppianResponseToApexes[1].id);

    }

    @IsTest
    public static void checkContactUpsertForExistingMARWithEmailIsNotNullAndPhoneIsNotNullThenContactIsNotCreated() {

        Contact testContact = new Contact(FirstName = 'Test',
                LastName = 'User',
                Email = 'TESTUSER@test.com',
                Preferred_Phone__c = '1234567890',
                MAR__c = true,
                RecordTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME));
        insert testContact;

        String inputJson = '[\n' + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "TESTUSER@test.com",\n' + '        "firstName": "Test",\n'
                + '        "id": "01",\n' + '        "lastName": "User",\n'
                + '        "phoneNo": "1234567890",\n' + '        "salesforceId": ""\n' + '    },\n'
                + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "TESTUSER@test.com",\n' + '        "firstName": " TEST",\n'
                + '        "id": "02",\n' + '        "lastName": "USER",\n'
                + '        "phoneNo": "1234567890",\n' + '        "salesforceId": ""\n' + '    }\n'
                + ']';
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/marcontactupsert';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(inputJson);
        RestContext.request = request;
        RestContext.response = response;
        MarContactUpsertFromExternalSystem.contactUpsert();

        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = MarContactUpsertAppianResponseToApex.parse(RestContext.response.responseBody.toString());
        System.assertEquals(testContact.Id, marContactUpsertAppianResponseToApexes[0].id);
        System.assertEquals(marContactUpsertAppianResponseToApexes[0].id, marContactUpsertAppianResponseToApexes[1].id);

    }

    @IsTest
    public static void checkContactUpsertForExistingNonMARWithEmailIsNotNullAndPhoneIsNotNullThenContactIsNotCreated() {

        Contact testContact = new Contact(FirstName = 'Test',
                LastName = 'User',
                Email = 'TESTUSER@test.com',
                Preferred_Phone__c = '1234567890',
                MAR__c = false,
                RecordTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME));
        insert testContact;

        String inputJson = '[\n' + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "TESTUSER@test.com",\n' + '        "firstName": "Test",\n'
                + '        "id": "01",\n' + '        "lastName": "User",\n'
                + '        "phoneNo": "1234567890",\n' + '        "salesforceId": ""\n' + '    },\n'
                + '    {\n' + '        "alternatePhoneNo": "0278676545",\n'
                + '        "email": "TESTUSER@test.com",\n' + '        "firstName": " TEST",\n'
                + '        "id": "02",\n' + '        "lastName": "USER",\n'
                + '        "phoneNo": "1234567890",\n' + '        "salesforceId": ""\n' + '    }\n'
                + ']';
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/marcontactupsert';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(inputJson);
        RestContext.request = request;
        RestContext.response = response;
        MarContactUpsertFromExternalSystem.contactUpsert();

        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = MarContactUpsertAppianResponseToApex.parse(RestContext.response.responseBody.toString());
        System.assertEquals(testContact.Id, marContactUpsertAppianResponseToApexes[0].id);
        System.assertEquals(marContactUpsertAppianResponseToApexes[0].id, marContactUpsertAppianResponseToApexes[1].id);

    }

    @IsTest
    public static void checkContactIfRequestBodyIsNullThenThrowsErrorMessage() {

        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/marcontactupsert';
        request.httpMethod = 'POST';
        request.requestBody = null;
        RestContext.request = request;
        RestContext.response = response;
        MarContactUpsertFromExternalSystem.contactUpsert();

        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = MarContactUpsertAppianResponseToApex.parse(RestContext.response.responseBody.toString());
        System.assertEquals('Request body is empty', marContactUpsertAppianResponseToApexes[0].error[0].message);

    }

    @IsTest
    public static void checkIfContactNotUpdatedWhenUpdateContactIsFalse(){
        Contact testContact = new Contact(FirstName = 'Test',
                LastName = 'User',
                Email = 'testuser@test.com',
                Preferred_Phone__c = '1234567890',
                MAR__c = false,
                RecordTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME));
        insert testContact;

        String inputJson = '[{ "alternatePhoneNo": "0412345678","email": "testuser@test.com", "firstName": "TEST", "index": "01", "lastName": "USER", "phoneNo": "0987654321", "updateContact":false}]';
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/marcontactupsert';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(inputJson);
        RestContext.request = request;
        RestContext.response = response;
        Test.startTest();
        MarContactUpsertFromExternalSystem.contactUpsert();
        Test.stopTest();



        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = MarContactUpsertAppianResponseToApex.parse(RestContext.response.responseBody.toString());
        System.assertEquals(testContact.Id, marContactUpsertAppianResponseToApexes[0].id);

        Contact notUpdatedContact = [SELECT Id,FirstName,LastName,Email,Preferred_Phone__c FROM Contact WHERE Id=:marContactUpsertAppianResponseToApexes[0].id];

        System.assertEquals(testContact.LastName, notUpdatedContact.LastName);
        System.assertEquals(testContact.FirstName,notUpdatedContact.FirstName);
        System.assertEquals(testContact.Preferred_Phone__c,notUpdatedContact.Preferred_Phone__c);

    }

    @IsTest
    public static void checkIfContactIsUpdatedWhenUpdateContactIsTrue(){
        Contact testContact = new Contact(FirstName = 'Test',
                LastName = 'User',
                Email = 'testuser@test.com',
                Preferred_Phone__c = '1234567890',
                MAR__c = false,
                RecordTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME));
        insert testContact;

        String inputJson = '[{ "alternatePhoneNo": "0412345678","email": "testuser@test.com", "firstName": "TEST", "index": "01", "lastName": "USER", "phoneNo": "0987654321", "updateContact":true}]';
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = '/services/apexrest/marcontactupsert';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(inputJson);
        RestContext.request = request;
        RestContext.response = response;
        Test.startTest();
        MarContactUpsertFromExternalSystem.contactUpsert();
        Test.stopTest();



        List<MarContactUpsertAppianResponseToApex> marContactUpsertAppianResponseToApexes = MarContactUpsertAppianResponseToApex.parse(RestContext.response.responseBody.toString());
        System.assertEquals(testContact.Id, marContactUpsertAppianResponseToApexes[0].id);

        Contact updatedContact = [SELECT Id,FirstName,LastName,Email,Preferred_Phone__c FROM Contact WHERE Id=:marContactUpsertAppianResponseToApexes[0].id];

        System.assertEquals('USER', updatedContact.LastName);
        System.assertEquals('TEST',updatedContact.FirstName);
        System.assertEquals('0987654321',updatedContact.Preferred_Phone__c);

    }
}