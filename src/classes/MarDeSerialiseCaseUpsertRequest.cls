/**
 * Created by shuol on 16/08/2018.
 */

public with sharing class MarDeSerialiseCaseUpsertRequest {
    public String registrationId { get {return trimValues(registrationId);} set; }
    public String aspAlarmTechnology { get {return trimValues(aspAlarmTechnology);} set; }
    public String aspVillageName { get {return trimValues(aspVillageName);} set; }
    public String aspVillageResident { get {return trimValues(aspVillageResident);} set; }
    public String alarmBrand { get {return trimValues(alarmBrand);} set; }
    public String alarmBrandIfOtherBrand { get {return trimValues(alarmBrandIfOtherBrand);} set; }
    public String alarmType { get {return trimValues(alarmType);} set; }
    public String caseType { get {return trimValues(caseType);} set; }
    public String caseStatus { get {return trimValues(caseStatus);} set; }
    public String copperActive { get {return trimValues(copperActive);} set; }
    public String dateProvided { get {return trimValues(dateProvided);} set; }
    public String disconnectionDate { get {return trimValues(disconnectionDate);} set; }
    public String inflightOrder { get {return trimValues(inflightOrder);} set; }
    public String locationId { get {return trimValues(locationId);} set; }
    public String massSummaryIneligibilityReason { get {return trimValues(massSummaryIneligibilityReason);} set; }
    public String massSummaryStatus{ get {return trimValues(massSummaryStatus);} set; }
    public String nbnActive{ get {return trimValues(nbnActive);} set; }
    public String primaryContact{ get {return trimValues(primaryContact);} set; }
    public String recordProvidedBy{ get {return trimValues(recordProvidedBy);} set; }
    public String sam { get {return trimValues(sam);} set; }
    public String secondaryContact { get {return trimValues(secondaryContact);} set; }
    public String serviceClass{ get {return trimValues(serviceClass);} set; }
    public String technologyType{ get {return trimValues(technologyType);} set; }
    public String whoDoesTheAlarmCall{ get {return trimValues(whoDoesTheAlarmCall);} set; }
    public String nbnActivatedDate{ get {return trimValues(nbnActivatedDate);} set; }
    public String csllStatus{ get {return trimValues(csllStatus);} set; }
    public String csllTerminatedDate{ get {return trimValues(csllTerminatedDate);} set; }
    public String orderStatus{ get {return trimValues(orderStatus);} set; }
    public String batteryBackupService{ get {return trimValues(batteryBackupService);} set; }
    public String rsp1{ get {return trimValues(rsp1);} set; }
    public String rsp2{ get {return trimValues(rsp2);} set; }
    public String priority{ get {return trimValues(priority);} set; }
    public String productInfoRefreshDate{ get {return trimValues(productInfoRefreshDate);} set; }


    public static List<MarDeSerialiseCaseUpsertRequest> parse(String json) {
        return (List<MarDeSerialiseCaseUpsertRequest>) System.JSON.deserialize(json, List<MarDeSerialiseCaseUpsertRequest>.class);
    }

    private static string trimValues(String str) {
        return (String.isNotEmpty(str) ? str.trim() : str);
    }
}