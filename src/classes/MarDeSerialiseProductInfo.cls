/***************************************************************************************************
    Class Name          : MarDeSerialiseProductInfo
    Version             : 1.0
    Created Date        : 20-Aug-2018
    Author              : Arpit Narain
    Description         : Class to de-serialise Product Information response from Appian
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/


public class MarDeSerialiseProductInfo {

    public class DeSerialiseProductInfo {

        public String locationId;
        public String nbnActiveStatus;
        public String csllStatus;
        public String csllTerminationDate;
        public String inflightOrder;
        public String orderStatus;
        public String activatedDate ;
        public String batteryBackupService;
        public String rsp1;
        public String rsp2;
        public String isMultiple;
        public String dd;
        public String activeCopper;
        public String errorMessage;
    }

    public DeSerialiseProductInfo parse(String json) {
        return (DeSerialiseProductInfo) System.JSON.deserialize(json, DeSerialiseProductInfo.class);
    }
}