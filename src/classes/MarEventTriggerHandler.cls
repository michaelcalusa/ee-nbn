/***************************************************************************************************
    Class Name          : MarEventTriggerHandler
    Version             : 1.0
    Created Date        : 28-Jun-2018
    Author              : Arpit Narain
    Description         : Handler class to process Mar Events
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/

public class MarEventTriggerHandler {

    public  void processEvent(List<MAR_Event__e> marEvents) {

        for (MAR_Event__e marEvent : marEvents) {

            List<Id> listOfMarIds = new List<Id>();
            if (!String.isBlank(marEvent.ID_List__c)) {

                List<String> IDList = marEvent.ID_List__c.split('[,]{1}[\\s]?');
                {
                    if (!IDList.isEmpty()) {

                        //send Mar details to Appian
                        if (marEvent.Event_type__c.equalsIgnoreCase('SendMarDetailsToAppian')) {

                            listOfMarIds.addAll(IDList);
                            httpMarCallOut(listOfMarIds);
                        }
                    }
                }
            }
        }
    }

    @Future(Callout=true)
    public static void httpMarCallOut(List<Id> listOfMarIds) {

        List <GenerateMarMessageForAppian> generateMarMessageForAppianList = new List<GenerateMarMessageForAppian>();
        Map<String, String> httpHeaderMap = new Map<String, String>();

        //Mars which needs to be updated with process ID
        List <MAR__c> marList = new List<MAR__c>();

        for (MAR__c mar : [
                SELECT Id,Name,Contact__c,Alternate_Contact__c,Site__c,Provided_Date__c,Medical_Alarm_Brand__c,Medical_Alarm_Brand_Other__c,CreatedDate,
                        Who_Does_Alarm_Call__c,How_did_you_hear__c,Medical_Alarm_Type__c,Additional_Information_Text__c,
                        Site__r.Id,Site__r.Location_Id__c,Site__r.Unit_Number__c,Site__r.Unit_Type_Code__c,Site__r.Level_Number__c,Site__r.Level_Type_Code__c,
                        Site__r.Road_Number_1__c,Site__r.Road_Number_2__c,Site__r.Road_Name__c,Site__r.Road_Type_Code__c,Site__r.Road_Suffix_Code__c,Site__r.Locality_Name__c,
                        Site__r.State_Territory_Code__c,Site__r.Post_Code__c,Site__r.Site_Address__c,Contact__r.Id,Contact__r.FirstName,Contact__r.LastName,Contact__r.Email,Contact__r.Preferred_Phone__c,
                        Contact__r.Alternate_Phone__c,Alternate_Contact__r.Id,Alternate_Contact__r.FirstName,Alternate_Contact__r.LastName,Alternate_Contact__r.Email,
                        Alternate_Contact__r.Preferred_Phone__c,Alternate_Contact__r.Alternate_Phone__c

                FROM MAR__c
                WHERE Id IN :listOfMarIds
        ]) {

            marList.add(mar);
            generateMarMessageForAppianList.add(new GenerateMarMessageForAppian(mar));

        }

        SendMarDetailsToAppian sendMarDetailsToAppian = new SendMarDetailsToAppian();
        sendMarDetailsToAppian.sendDetailsToAppian(httpHeaderMap, generateMarMessageForAppianList, marList);

    }
}