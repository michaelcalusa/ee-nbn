/***************************************************************************************************
    Class Name          : MarEventTriggerHandlerTest
    Version             : 1.0
    Created Date        : 28-Jun-2018
    Author              : Arpit Narain
    Description         : Test class to process Mar Events
    Modification Log    :
    * Developer             Date            Description
    * Arpit Narain          11-Apr-19       Added tests for MAR Number change
    * ----------------------------------------------------------------------------

****************************************************************************************************/

@IsTest
public class MarEventTriggerHandlerTest {
    @IsTest
    public static void checkProcessEventCallOutToAppianWhenEventTypeIsSendMarDetailsToAppianAndResponseIs200() {

        String responseBody = '{"success": true, "processId": "1024594"}';

        //set Mock
        Test.setMock(HttpCalloutMock.class, new HttpMarCallOutImpl(200, responseBody));

        //create test data
        MAR__c mar = new MAR__c();
        mar.Registration_Type__c = 'WebForm';
        insert mar;

        List<MAR_Event__e> marEvents = new List<MAR_Event__e>();
        MAR_Event__e marEvent = new MAR_Event__e();
        marEvent.ID_List__c = mar.Id;
        marEvent.Event_type__c = 'SendMarDetailsToAppian';
        marEvents.add(marEvent);
        Test.startTest();
        MarEventTriggerHandler marEventTriggerHandler = new MarEventTriggerHandler();
        marEventTriggerHandler.processEvent(marEvents);
        Test.stopTest();
        MAR__c updatedMar = [SELECT Id,External_Process_ID__c FROM MAR__c WHERE Id = :mar.Id];
        System.assertEquals('1024594', updatedMar.External_Process_ID__c);

    }
    @IsTest
    public static void checkProcessEventCallOutToAppianWhenEventTypeIsSendMarDetailsToAppianAndResponseIs500() {

        String responseBody = '500 - Internal Server Error';

        //set Mock
        Test.setMock(HttpCalloutMock.class, new HttpMarCallOutImpl(500, responseBody));

        //create test data
        MAR__c mar = new MAR__c();
        mar.Registration_Type__c = 'WebForm';
        insert mar;


        List<MAR_Event__e> marEvents = new List<MAR_Event__e>();
        MAR_Event__e marEvent = new MAR_Event__e();
        marEvent.ID_List__c = mar.Id;
        marEvent.Event_type__c = 'SendMarDetailsToAppian';
        marEvents.add(marEvent);
        Test.startTest();
        MarEventTriggerHandler marEventTriggerHandler = new MarEventTriggerHandler();
        marEventTriggerHandler.processEvent(marEvents);
        Test.stopTest();
        MAR__c updatedMar = [SELECT Id,External_Process_ID__c,External_Process_Error__c FROM MAR__c WHERE Id = :mar.Id];
        System.assertEquals(null, updatedMar.External_Process_ID__c);
        System.assertEquals('500 - Internal Server Error', updatedMar.External_Process_Error__c);


    }

    @IsTest
    public static void checkProcessEventCallOutToAppianWhenEventTypeIsSendMarDetailsToAppianAndResponseIsInvalidJSON() {

        String responseBody = '<!DOCTYPE html><html lang="en" dir="ltr<head>';

        //set Mock
        Test.setMock(HttpCalloutMock.class, new HttpMarCallOutImpl(200, responseBody));

        //create test data
        MAR__c mar = new MAR__c();
        mar.Registration_Type__c = 'WebForm';
        insert mar;


        List<MAR_Event__e> marEvents = new List<MAR_Event__e>();
        MAR_Event__e marEvent = new MAR_Event__e();
        marEvent.ID_List__c = mar.Id;
        marEvent.Event_type__c = 'SendMarDetailsToAppian';
        marEvents.add(marEvent);
        Test.startTest();
        MarEventTriggerHandler marEventTriggerHandler = new MarEventTriggerHandler();
        marEventTriggerHandler.processEvent(marEvents);
        Test.stopTest();
        MAR__c updatedMar = [SELECT Id,External_Process_ID__c,External_Process_Error__c FROM MAR__c WHERE Id = :mar.Id];
        System.assertEquals(null, updatedMar.External_Process_ID__c);
        System.assertEquals('Could not parse response from Appian, Refer Application log for details', updatedMar.External_Process_Error__c);


    }

    @IsTest
    public static void checkProcessEventCallOutToAppianWhenEventTypeIsSendMarDetailsToAppianAndExceptionIsMoreThan255Character() {

        //create test data
        MAR__c mar = new MAR__c();
        mar.Registration_Type__c = 'WebForm';
        insert mar;

        List<MAR__c> mars = new List<MAR__c>();
        mars.add(mar);

        HttpResponse httpResponse = new HttpResponse();
        String msgBody = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';
        httpResponse.setBody(msgBody);
        httpResponse.setStatusCode(500);


        SendMarDetailsToAppian sendMarDetailsToAppian = new SendMarDetailsToAppian();
        sendMarDetailsToAppian.updateAppainProcessId(httpResponse, mars);

        MAR__c updatedMar = [SELECT Id,External_Process_ID__c,External_Process_Error__c FROM MAR__c WHERE Id = :mar.Id];

        System.assertEquals(msgBody, updatedMar.External_Process_Error__c);


    }

    @IsTest
    public static void checkProcessEventCallOutToAppianWhenEventTypeIsSendMarDetailsToAppianAndExceptionIsLessThan255Character() {

        //create test data
        MAR__c mar = new MAR__c();
        mar.Registration_Type__c = 'WebForm';
        insert mar;

        List<MAR__c> mars = new List<MAR__c>();
        mars.add(mar);

        HttpResponse httpResponse = new HttpResponse();
        httpResponse.setBody('Lorem ipsum');
        httpResponse.setStatusCode(500);


        SendMarDetailsToAppian sendMarDetailsToAppian = new SendMarDetailsToAppian();
        sendMarDetailsToAppian.updateAppainProcessId(httpResponse, mars);
        MAR__c updatedMar = [SELECT Id,External_Process_ID__c,External_Process_Error__c FROM MAR__c WHERE Id = :mar.Id];

        System.assertEquals('Lorem ipsum', updatedMar.External_Process_Error__c);

    }

    @IsTest
    public static void checkUpdateAppaInProcessIdWhenHTTPResponseIsNullForSendMarDetailsToAppian() {

        //create test data
        MAR__c mar = new MAR__c();
        mar.Registration_Type__c = 'WebForm';
        insert mar;

        SendMarDetailsToAppian sendMarDetailsToAppian = new SendMarDetailsToAppian();

        Test.startTest();
        sendMarDetailsToAppian.updateAppainProcessId(null, null);
        Test.stopTest();

        MAR__c updatedMar = [SELECT Id,External_Process_ID__c,External_Process_Error__c FROM MAR__c WHERE Id = :mar.Id];

        System.assertEquals(null, updatedMar.External_Process_Error__c);

    }

    @IsTest
    public static void checkAppianRequestFormatWhenMarNumberIsSent() {

        String responseBody = '{"success": true, "processId": "1024594"}';

        //set Mock
        Test.setMock(HttpCalloutMock.class, new HttpMarCallOutImpl(200, responseBody));


        // create mar form staging
        List<MAR_Form_Staging__c> marFormStagings = new List<MAR_Form_Staging__c>();

        for (Integer i = 0; i < 9; i++) {
            MAR_Form_Staging__c aMarFormStaging = new MAR_Form_Staging__c();
            aMarFormStaging.Status__c = 'New';
            aMarFormStaging.MAR_Alarm_Brand__c = 'Blueforce';
            aMarFormStaging.Who_Does_Alarm_Call__c = 'Emergency services - 000';
            aMarFormStaging.MAR_Alarm_Brand_Other__c = 'Some-Other-Alarm-Brand';
            aMarFormStaging.Additional_Information_Text__c = 'new additional info';
            aMarFormStaging.How_did_you_hear__c = 'Radio';
            aMarFormStaging.Location_ID__c = 'LOC000007251621';
            aMarFormStaging.Site_Name__c = 'Unit 23 29-45 Parramatta Rd, Concord, NSW';
            aMarFormStaging.First_Name__c = 'TestUser00' + i;
            aMarFormStaging.Last_Name__c = 'LastName00' + i;
            aMarFormStaging.Email_Address__c = 'fistnamelastname00' + i + '@test.com';
            aMarFormStaging.Phone_Number__c = '043456789' + i;
            aMarFormStaging.Additional_Phone_Number__c = '044456789' + i;
            aMarFormStaging.Alt_First_Name__c = 'AltTestUser00' + i;
            aMarFormStaging.Alt_Last_Name__c = 'AltLastName00' + i;
            aMarFormStaging.Alt_Email_Address__c = 'altfistnamelastname00' + i + '@test.com';
            aMarFormStaging.Alt_Phone_Number__c = '045456789' + i;
            aMarFormStaging.Alt_Additional_Phone_Number__c = '046456789' + i;
            marFormStagings.add(aMarFormStaging);
        }
        List <String> marStageIds = new List<String>();

        //create MAR record and send to Appian
        Test.startTest();
        List<Database.SaveResult> saveResults = Database.insert(marFormStagings);
        Test.stopTest();

        for (Database.SaveResult saveResult : saveResults) {
            if (saveResult.isSuccess()) {
                marStageIds.add(saveResult.getId());
            }
        }

        //get MAR fields
        List<String> marIds = new List<String>();
        List<String> marNumber = new List<String>();
        for (MAR_Form_Staging__c updatedMarFormStaging : [SELECT Id,MAR__c FROM MAR_Form_Staging__c WHERE Id IN :marStageIds]) {
            marIds.add(updatedMarFormStaging.MAR__c);
        }
        List <GenerateMarMessageForAppian> generateMarMessageForAppianList = new List<GenerateMarMessageForAppian>();

        for (MAR__c mar : [
                SELECT Id,Name,Contact__c,Alternate_Contact__c,Site__c,Provided_Date__c,Medical_Alarm_Brand__c,Medical_Alarm_Brand_Other__c,CreatedDate,
                        Who_Does_Alarm_Call__c,How_did_you_hear__c,Medical_Alarm_Type__c,Additional_Information_Text__c,
                        Site__r.Id,Site__r.Location_Id__c,Site__r.Unit_Number__c,Site__r.Unit_Type_Code__c,Site__r.Level_Number__c,Site__r.Level_Type_Code__c,
                        Site__r.Road_Number_1__c,Site__r.Road_Number_2__c,Site__r.Road_Name__c,Site__r.Road_Type_Code__c,Site__r.Road_Suffix_Code__c,Site__r.Locality_Name__c,
                        Site__r.State_Territory_Code__c,Site__r.Post_Code__c,Site__r.Site_Address__c,Contact__r.Id,Contact__r.FirstName,Contact__r.LastName,Contact__r.Email,Contact__r.Preferred_Phone__c,
                        Contact__r.Alternate_Phone__c,Alternate_Contact__r.Id,Alternate_Contact__r.FirstName,Alternate_Contact__r.LastName,Alternate_Contact__r.Email,
                        Alternate_Contact__r.Preferred_Phone__c,Alternate_Contact__r.Alternate_Phone__c
                FROM MAR__c
                WHERE Id IN :marIds
        ]) {
            marNumber.add(mar.Name);
            generateMarMessageForAppianList.add(new GenerateMarMessageForAppian(mar));
        }

        //parse message which needs to be sent to Appian
        JSONParser jsonParser = JSON.createParser(JSON.serialize(generateMarMessageForAppianList));

        while (jsonParser.nextToken() != null) {
            if (jsonParser.getCurrentToken() == JSONToken.FIELD_NAME) {
                jsonParser.nextValue();
                if (jsonParser.getCurrentName() == 'locationId'){
                    System.assertEquals('LOC000007251621',jsonParser.getText());
                }
                if (jsonParser.getCurrentName() == 'marNumber'){
                    System.assertEquals(true,marNumber.contains(jsonParser.getText()));
                }
            }

        }
    }
}