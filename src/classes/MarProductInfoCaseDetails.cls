/*
Author              : Shuo
Created Date        : 14 June 2018
Description         : Controller extensions for Console Product Information Details panel
Test Class          : ConsoleSiteDetailsPanelController_Test
Modification Log    :
-----------------------------------------------------------------------------------------------------------------
History <Date>      <Authors Name>              <Brief Description of Change>

*/


public with sharing class MarProductInfoCaseDetails {

    ApexPages.StandardSetController standardController;
    public Id caseId ;
    public Case displayCase { get; set; }
    public Set<String> stopRefreshProductStatus = new Set<String>();
    private String marProductInfoFieldSetName;
    public String lastRefreshDateLocalDateTime { get {return getLocalDateTime(displayCase.Product_Info_Last_Refresh_Date__c);} set ; }
    public String csllTerminationDateLocalDateTime { get {return getLocalDateTime(displayCase.CSLL_Termination_Date__c);} set ; }
    public String activatedDateLocalDateTime { get {return getLocalDateTime(displayCase.Activated_Date__c);} set ; }
    public String ddLocalDate { get {return getLocalDate(displayCase.Disconnection_date__c);} set ; }

    public MarProductInfoCaseDetails (ApexPages.StandardController stdController) {
        displayCase = (Case) stdController.getRecord();
        caseId = displayCase.id;

        if (displayCase.Id != null) {
            marProductInfoFieldSetName = MarUtil.getValueFromMDCustomMetaData('Mar_Product_Info_FS', 'FS_Case_Related_Product_Details');
            stopRefreshProductStatus = new Set<String>(MarUtil.getValueFromMDCustomMetaData('Mar_Product_Info_Exclude_Status', 'Closed').split(';'));
            displayCase = getCase(caseId);
        }
    }


    // function to get Case Site Details to the new site information after re-qualify
    public Case getCase (Id CaseId) {
        String SOQL = 'select Id, Status,site__r.Location_Id__c ' + MarUtil.getFieldNamesBasedOnFieldSet(marProductInfoFieldSetName) + ' from case where id = :CaseId limit 1';
        Case c = Database.query(SOQL);
        return c;
    }

    public void init () {
        String errors = '';
        if (displayCase.Id != null) {

            if (!stopRefreshProductStatus.contains(displayCase.Status)) {
                if (String.isBlank(displayCase.Site__r.Location_Id__c)) {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Site without Location Id. Please manually qualify the site.');
                    ApexPages.addMessage(myMsg);
                } else {
                    try {
                        errors = MarUtil.getProductInfo(displayCase.Id);

                        if (!String.isBlank(errors)) {
                            System.debug('Error:' + errors);
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, errors);
                            ApexPages.addMessage(myMsg);
                        } else {
                            displayCase = getCase(caseId);
                        }
                    } catch (Exception e) {
                        System.debug('errorMsg: ' + e.getMessage());
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Technical exception has occurred. Please raise support request with below details: ' + e.getMessage());
                        ApexPages.addMessage(myMsg);
                    }
                }
            }
            System.debug('displayCase: ' + displayCase);
        }
    }
    /*
      Return date time based on locale settings
     */
    public String getLocalDateTime (Datetime dateField) {
        if (dateField != null) {
            return dateField.format();
        }
        return '';

    }

    /*
     Return date time based on locale settings
    */
    public String getLocalDate (Date dateField) {
        if (dateField != null) {
            return dateField.format();
        }
        return '';
    }

}