/*
Author              : Arpit Narain
Created Date        : 14 Sep 2018
Description         : Test class for controller extensions in console Product Information Details panel
Modification Log    :
-----------------------------------------------------------------------------------------------------------------
History <Date>      <Authors Name>              <Brief Description of Change>

*/
@IsTest
public class MarProductInfoCaseDetailsTest {

    @IsTest
    public static void checkProductInfoInitIsCalledWithLocationID () {

        String responseBody = '{ \"id\" : \"LOC000008033779\", \"csllStatus\" : \"Terminated\", \"csllTerminationDate\" : \"2017-04-29 22:22:00\", \"orderStatus\" : \"Connect - Complete\", \"inflightOrder\" : \"No\", \"nbnActive\" : \"Yes\", \"nbnActivatedDate\" : \"2017-05-03 01:01:00\", \"batteryBackup\" : \"No\", \"rsp1\" : \"Internode Pty Ltd\", \"isMultiple\" : \"No\", \"activeCopper\" : \"Yes\" }';

        //set Mock
        Test.setMock(HttpCalloutMock.class, new HttpMarCallOutImpl(200, responseBody));


        // Create Site record
        Id recordTypeId = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId();
        Site__c UnverifiedSite = new Site__c();
        UnverifiedSite.RecordTypeId = recordTypeId;
        UnverifiedSite.Location_Id__c = 'LOC000106753648';
        UnverifiedSite.isSiteCreationThroughCode__c = true;
        insert UnverifiedSite;


        // Create Contact record
        Id conRecordTypeId = schema.SObjecttype.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId();
        Contact contactRec = new Contact ();
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        contactRec.recordTypeId = conRecordTypeId;
        insert contactRec;


        Id marRecTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId();
        Case marCase = new Case(RecordTypeId = marRecTypeId,
                Site__c = UnverifiedSite.Id,
                ContactId = contactRec.id,
                Case_Concat__c = String.valueOf(UnverifiedSite.Id) + String.valueOf(contactRec.id),
                Product_Info_Last_Refresh_Date__c = System.Now(),
                CSLL_Termination_Date__c = System.Now(),
                Activated_Date__c = System.Now(),
                Status = 'New');
        insert marCase;


        Set <String> listOfStatuses = new Set<String>();
        listOfStatuses.add('Closed');

        ApexPages.StandardController sc = new ApexPages.StandardController(marCase);
        MarProductInfoCaseDetails marProductInfoCaseDetails = new MarProductInfoCaseDetails(sc);
        marProductInfoCaseDetails.stopRefreshProductStatus = listOfStatuses;
        String lastRefreshDate = marProductInfoCaseDetails.lastRefreshDateLocalDateTime ;
        String disconnectionDate = marProductInfoCaseDetails.ddLocalDate;
        String csllTerminationDate = marProductInfoCaseDetails.csllTerminationDateLocalDateTime;
        String activatedDate = marProductInfoCaseDetails.activatedDateLocalDateTime;
        Test.startTest();
        marProductInfoCaseDetails.init();
        Test.stopTest();

    }


    @IsTest
    public static void checkProductInfoInitIsCalledWithOutLocationID () {

        // Create Site record
        Id recordTypeId = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId();
        Site__c UnverifiedSite = new Site__c();
        UnverifiedSite.RecordTypeId = recordTypeId;
        UnverifiedSite.isSiteCreationThroughCode__c = true;
        insert UnverifiedSite;


        // Create Contact record
        Id conRecordTypeId = schema.SObjecttype.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId();
        Contact contactRec = new Contact ();
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        contactRec.recordTypeId = conRecordTypeId;
        insert contactRec;


        Id marRecTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId();
        Case marCase = new Case(RecordTypeId = marRecTypeId,
                Site__c = UnverifiedSite.Id,
                ContactId = contactRec.id,
                Case_Concat__c = String.valueOf(UnverifiedSite.Id) + String.valueOf(contactRec.id),
                Product_Info_Last_Refresh_Date__c = System.Now(),
                CSLL_Termination_Date__c = System.Now(),
                Activated_Date__c = System.Now(),
                Status = 'New');
        insert marCase;


        Set <String> listOfStatuses = new Set<String>();
        listOfStatuses.add('Closed');

        ApexPages.StandardController sc = new ApexPages.StandardController(marCase);
        MarProductInfoCaseDetails marProductInfoCaseDetails = new MarProductInfoCaseDetails(sc);
        marProductInfoCaseDetails.stopRefreshProductStatus = listOfStatuses;
        marProductInfoCaseDetails.init();

    }


    @IsTest
    public static void checkGetLocalDateTimeWithNullDateTime () {
        // Create Site record
        Id recordTypeId = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId();
        Site__c UnverifiedSite = new Site__c();
        UnverifiedSite.RecordTypeId = recordTypeId;
        UnverifiedSite.isSiteCreationThroughCode__c = true;
        insert UnverifiedSite;


        // Create Contact record
        Id conRecordTypeId = schema.SObjectType.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId();
        Contact contactRec = new Contact ();
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        contactRec.recordTypeId = conRecordTypeId;
        insert contactRec;


        Id marRecTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId();
        Case marCase = new Case(RecordTypeId = marRecTypeId,
                Site__c = UnverifiedSite.Id,
                ContactId = contactRec.id,
                Case_Concat__c = String.valueOf(UnverifiedSite.Id) + String.valueOf(contactRec.id),
                Product_Info_Last_Refresh_Date__c = System.Now(),
                CSLL_Termination_Date__c = System.Now(),
                Activated_Date__c = System.Now(),
                Status = 'New');
        insert marCase;


        Set <String> listOfStatuses = new Set<String>();
        listOfStatuses.add('Closed');

        ApexPages.StandardController sc = new ApexPages.StandardController(marCase);
        MarProductInfoCaseDetails marProductInfoCaseDetails = new MarProductInfoCaseDetails(sc);
        marProductInfoCaseDetails.init();


        Test.startTest();

        String localTime = marProductInfoCaseDetails.getLocalDateTime(null);
        System.assert(String.isBlank(localTime));

    }

    @IsTest
    public static void checkGetLocalDateWithNullDate () {
        // Create Site record
        Id recordTypeId = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId();
        Site__c UnverifiedSite = new Site__c();
        UnverifiedSite.RecordTypeId = recordTypeId;
        UnverifiedSite.isSiteCreationThroughCode__c = true;
        insert UnverifiedSite;


        // Create Contact record
        Id conRecordTypeId = schema.SObjectType.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId();
        Contact contactRec = new Contact ();
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        contactRec.recordTypeId = conRecordTypeId;
        insert contactRec;


        Id marRecTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId();
        Case marCase = new Case(RecordTypeId = marRecTypeId,
                Site__c = UnverifiedSite.Id,
                ContactId = contactRec.id,
                Case_Concat__c = String.valueOf(UnverifiedSite.Id) + String.valueOf(contactRec.id),
                Product_Info_Last_Refresh_Date__c = System.Now(),
                CSLL_Termination_Date__c = System.Now(),
                Activated_Date__c = System.Now(),
                Status = 'New');
        insert marCase;


        Set <String> listOfStatuses = new Set<String>();
        listOfStatuses.add('Closed');

        ApexPages.StandardController sc = new ApexPages.StandardController(marCase);
        MarProductInfoCaseDetails marProductInfoCaseDetails = new MarProductInfoCaseDetails(sc);
        marProductInfoCaseDetails.init();


        Test.startTest();

        String localTime = marProductInfoCaseDetails.getLocalDate(null);
        System.assert(String.isBlank(localTime));

    }


}