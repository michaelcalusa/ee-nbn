/***************************************************************************************************
    Class Name          : MarUtil
    Version             : 1.0
    Created Date        : 25-Sep-2018
    Author              : Arpit Narain
    Description         : Class for custom controller product Info view only
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/


public with sharing class MarProductInfoViewOnlyController {

    ApexPages.StandardSetController standardController;
    public Id caseId ;
    public Case displayCase { get; set; }
    public Set<String> stopRefreshProductStatus = new Set<String>();
    public String lastRefreshDate { get; set; }
    public String csllStatus { get; set; }
    public String nbnActive { get; set; }
    public String csllTerminationDate { get; set; }
    public String activatedDate { get; set; }
    public String batteryBackup { get; set; }
    public String inflightOrder { get ; set ; }
    public String rsp1 { get; set; }
    public String orderStatus { get; set; }
    public String rsp2 { get; set; }
    public String disconnectionDate { get; set; }
    public String copperActive { get; set; }
    public MarProductInfoWrapper marProductInfoWrapper { get; set; }
    public String errors { get ; set; }

    public MarProductInfoViewOnlyController (ApexPages.StandardController stdController) {

        caseId = ((Case) stdController.getRecord()).Id;
        if (caseId != null) {
            stopRefreshProductStatus = new Set<String>(MarUtil.getValueFromMDCustomMetaData('Mar_Product_Info_Exclude_Status', 'Closed').split(';'));
            displayCase = getCase(caseId);
        }

    }


    // function to get Case Site Details to the new site information after re-qualify
    public Case getCase (Id CaseId) {
        return [SELECT Id, Status,Site__r.Location_Id__c FROM Case WHERE Id = :caseId LIMIT 1];
    }


    public void init () {

        if (displayCase.Id != null) {

            if (!stopRefreshProductStatus.contains(displayCase.Status)) {

                if (String.isBlank(displayCase.Site__r.Location_Id__c)) {
                    errors = 'Site without Location Id. Please manually qualify the site.';
                    displayError();
                } else {
                    processProductInfo();
                }
            }
        }
    }

    private void displayError () {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, errors);
        ApexPages.addMessage(myMsg);
    }

    public void processProductInfo () {
        try {

            marProductInfoWrapper = MarUtil.getProductInfoViewOnly(displayCase.Id);
            populateProductInfoFields(marProductInfoWrapper);

            if (!String.isBlank(errors)) {
                displayError();
            }

        } catch (Exception e) {
            errors = 'Error: Technical exception has occurred. Please raise support request with below details: ' + e.getMessage();
            displayError();
        }
    }

    public void populateProductInfoFields (MarProductInfoWrapper marProductInfoWrapper) {
        this.lastRefreshDate = MarUtil.getLocalDateTime(marProductInfoWrapper.lastRefreshDate);
        this.csllStatus = marProductInfoWrapper.csllStatus;
        this.nbnActive = marProductInfoWrapper.nbnActive;
        this.csllTerminationDate = MarUtil.getLocalDateTime(MarUtil.getDateTimeParser(marProductInfoWrapper.csllTerminationDate));
        this.activatedDate = MarUtil.getLocalDateTime(MarUtil.getDateTimeParser(marProductInfoWrapper.activatedDate));
        this.batteryBackup = marProductInfoWrapper.batteryBackup;
        this.inflightOrder = marProductInfoWrapper.inflightOrder;
        this.rsp1 = marProductInfoWrapper.rsp1;
        this.orderStatus = marProductInfoWrapper.orderStatus;
        this.rsp2 = marProductInfoWrapper.rsp2;
        this.disconnectionDate = MarUtil.getLocalDate(MarUtil.getDateParser(marProductInfoWrapper.disconnectionDate));
        this.copperActive = marProductInfoWrapper.copperActive;
        this.errors = marProductInfoWrapper.errors;
    }


    public class MarProductInfoWrapper {
        public Datetime lastRefreshDate;
        public String csllStatus;
        public String nbnActive;
        public String csllTerminationDate ;
        public String activatedDate;
        public String batteryBackup;
        public String inflightOrder;
        public String rsp1;
        public String orderStatus;
        public String rsp2;
        public String disconnectionDate ;
        public String copperActive;
        public String errors;
    }

}