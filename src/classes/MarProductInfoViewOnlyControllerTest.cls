/*
Author              : Arpit Narain
Created Date        : 25 Sep 2018
Description         : Test class for controller extensions in console Product Information Details panel (view only)
Modification Log    :
-----------------------------------------------------------------------------------------------------------------
History <Date>      <Authors Name>              <Brief Description of Change>

*/
@IsTest
public class MarProductInfoViewOnlyControllerTest {

    @IsTest
    public static void checkProductInfoInitIsCalledWithLocationID () {

        String responseBody = '{"locationId":"LOC000000563017","isMultiple":"No","nbnActiveStatus":"Yes","csllStatus":"N/A","csllTerminationDate":"","inflightOrder":"No","orderStatus":"Connect - Complete","activatedDate":"18/02/2015 03:06 AM","batteryBackupService":"No","dd":"10/02/2017","activeCopper":"","rsp1":"Telstra Corporation Ltd"}';
        //set Mock
        Test.setMock(HttpCalloutMock.class, new HttpMarCallOutImpl(200, responseBody));


        // Create Site record
        Id recordTypeId = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId();
        Site__c UnverifiedSite = new Site__c();
        UnverifiedSite.RecordTypeId = recordTypeId;
        UnverifiedSite.Location_Id__c = 'LOC000106753648';
        UnverifiedSite.isSiteCreationThroughCode__c = true;
        insert UnverifiedSite;


        // Create Contact record
        Id conRecordTypeId = schema.SObjecttype.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId();
        Contact contactRec = new Contact ();
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        contactRec.recordTypeId = conRecordTypeId;
        insert contactRec;


        Id marRecTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId();
        Case marCase = new Case(RecordTypeId = marRecTypeId,
                Site__c = UnverifiedSite.Id,
                ContactId = contactRec.id,
                Case_Concat__c = String.valueOf(UnverifiedSite.Id) + String.valueOf(contactRec.id),
                Product_Info_Last_Refresh_Date__c = System.Now(),
                CSLL_Termination_Date__c = System.Now(),
                Activated_Date__c = System.Now(),
                Status = 'New');
        insert marCase;


        Set <String> listOfStatuses = new Set<String>();
        listOfStatuses.add('Closed');

        ApexPages.StandardController sc = new ApexPages.StandardController(marCase);
        MarProductInfoViewOnlyController marProductInfoCaseDetails = new MarProductInfoViewOnlyController(sc);
        Test.startTest();
        marProductInfoCaseDetails.init();
        Test.stopTest();

        System.assertEquals(null,marProductInfoCaseDetails.errors);
        System.assertEquals(null,marProductInfoCaseDetails.marProductInfoWrapper.errors);
        System.assertEquals('18/02/2015 3:06 AM',marProductInfoCaseDetails.activatedDate);
        System.assertEquals('10/02/2017',marProductInfoCaseDetails.disconnectionDate);

    }

}