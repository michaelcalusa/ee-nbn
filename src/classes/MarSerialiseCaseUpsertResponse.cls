/**
 * Created by shuol on 16/08/2018.
 */

public with sharing class MarSerialiseCaseUpsertResponse {
    public String registrationId;
    public String salesforceCaseId;
    public String success;
    public List<Database.Error> error;
    public String subStatus;

    public MarSerialiseCaseUpsertResponse(Database.UpsertResult UpsertResult, String registrationId) {

        this.salesforceCaseId = UpsertResult.Id;
        this.success = String.valueOf(UpsertResult.isSuccess());
        this.error = UpsertResult.errors;
        this.registrationId = registrationId;

        if (UpsertResult.id == null){
            return;
        }

       Case updatedCase = [SELECT MAR_Sub_Status__c FROM Case WHERE Id=:UpsertResult.id];
        if (updatedCase != null){

            this.subStatus= updatedCase.MAR_Sub_Status__c;
        }
    }
}