/*------------------------------------------------------------------------
Author:        Shubham Jaiswal & Shuo Li    
Company:       NBN
Description:   A class created to manage trigger actions from the Mar  object 
               Responsible for:
               1 - Subscript for Mar Single form Event
               2 - Process Event Reocrd
                    1) Create/Update Contact based on the business rules
                    2) Create/Update Site based on the business rules
                    3) Create new Mar Record
                    4) link Mar with Contact and Site
Test Class:    MARSingleForm_Test
Created   :    24/10/2017    
History
07/12/2017            Shuo Li    Set Default Data Source for Mar Object(https://jira.nbnco.net.au/browse/MSEU-7279)
11/12/2017            Shuo Li    Remove the map between who does call and alarm type MSEU-7943
11/04/2018      Krishna Kanth    Changes made to accept unverified/unformatted Site Address from MAR form MSEU-11380  
08/11/2018      Ganesh Sawant    MSEU-19383 Logic added to troubleshoot MAR Incident around Query Exception by adding Special
                                 Character check before doing Contact query and adding try catch to Capture query Exception                                 
--------------------------------------------------------------------------*/

public with sharing class MarSingleFormEventTriggerHandler {
    
    private List<MAR_Single_Form_Event__e> trgNewList = new List<MAR_Single_Form_Event__e> ();
    public MarSingleFormEventTriggerHandler(List<MAR_Single_Form_Event__e> trgNewList){    
        this.trgNewList = trgNewList;      
    }
    
    public void OnAfterInsert()
    {
        if(!HelperUtility.isTriggerMethodExecutionDisabled('SubscriptMarFormEvent')){
            system.debug('OnAfterInsert - Newlist....'+trgNewList); 
            system.debug('Userinfo---' + UserInfo.getUserId());
            CreateMarRecords(trgNewList);
        }
    }
    
    /*
        Function to create Mar and link Mar with Exiting/new site and Contact.
    */
    public void CreateMarRecords(List<MAR_Single_Form_Event__e> trgNewList){
            List<Id> marFormIds = new list<Id>();
            list<MAR_Form_Staging__c> marFormUpdateList = new list<MAR_Form_Staging__c>();
            list<MAR_Form_Staging__c> marFormList = new list<MAR_Form_Staging__c>();
            
            //MAR Object
            List<MAR__c> marInsertList = new List<MAR__c>();
            
            //Contact Varibales
            Id externalContactRecTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME);
            List<Contact> upsertContactList = new List<Contact>();
            Map<String,Contact> duplicateContactCheckMap = new Map<String,Contact>();
            Set<Id> duplicateExistingContactSet = new Set<Id>();
            Set<String> contactNameSet = new Set<String>();
            Map<String,Contact> existingMARContactMap = new Map<String,Contact>();
            Map<String,Contact> existingNonMARContactMap = new Map<String,Contact>();
            Map<String,Contact> existingMARContactEmailPhoneMap = new Map<String,Contact>();
            Map<String,Contact> existingNonMARContactEmailPhoneMap = new Map<String,Contact>();
            
            //Site Variables
            map<id, MARSingleFormUtil.AddressWrap>marFormAddressMaps = new map<id, MARSingleFormUtil.AddressWrap>();
            map<id, id>marFormSiteMaps = new map<Id,Id>();
            
            //Subscript event Id
            for (MAR_Single_Form_Event__e msfe:trgNewList) {
                marFormIds.add(msfe.Id__c);
            }  
            
            //Loop to get Mar form staging records
            for (MAR_Form_Staging__c mfs : [select id,name, status__c, Location_ID__c,Alt_Additional_Phone_Number__c,
                                  Alt_Email_Address__c,Alt_First_Name__c,Alt_Last_Name__c,
                                  Alt_Phone_Number__c,Additional_Phone_Number__c,
                                  Email_Address__c,First_Name__c,Last_Name__c,
                                  Phone_Number__c,Site_Name__c,
                                  Additional_Information_Text__c, 
                                  MAR_Alarm_Type__c,How_did_you_hear__c,
                                  MAR_Alarm_Brand__c,MAR_Alarm_Brand_Other__c,
                                  Who_Does_Alarm_Call__c
                                  from MAR_Form_Staging__c where id in :marFormIds ]){
               system.debug('mfs'+ mfs);
               if (!String.isBlank(mfs.First_Name__c)) {
                   
                   //Pattern check against First Name and Last Name to verify if its string with only Special Characters
                   Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
                   Matcher firstNameStringCheck = nonAlphanumeric.matcher(mfs.First_Name__c);
                   Matcher lastNameStringCheck = nonAlphanumeric.matcher(mfs.Last_Name__c);
                   if(firstNameStringCheck.matches() && lastNameStringCheck.matches()){
                   	//Do nothing. Strings with only special Characters are not passed to Contact query
                   	//instead we will create new Contact records for all these scenarios    
                   }
                   else {                  
                   	contactNameSet.add((mfs.First_Name__c).toLowerCase() + ' ' + (mfs.Last_Name__c).toLowerCase());
                   }
                }
                if (!String.isBlank(mfs.Alt_First_Name__c)) {
                   
                   //Pattern check against First Name and Last Name to verify if its string with only Special Characters
                   Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
                   Matcher altFirstNameStringCheck = nonAlphanumeric.matcher(mfs.Alt_First_Name__c);
                   Matcher altLastNameStringCheck = nonAlphanumeric.matcher(mfs.Alt_Last_Name__c);
                   if(altFirstNameStringCheck.matches() && altLastNameStringCheck.matches()){
                   	//Do nothing. Strings with only special Characters are not passed to Contact query
                   	//instead we will create new Contact records for all these scenarios    
                   }
                   else{                    
                    	contactNameSet.add((mfs.Alt_First_Name__c).toLowerCase() + ' ' + (mfs.Alt_Last_Name__c).toLowerCase());
                   }
                }
                //Changes made to accept unverified/unformatted Site Address from MAR form MSEU-11380
                //if(!String.isBlank(mfs.Location_ID__c)) {
                     MARSingleFormUtil.AddressWrap addressWrap = new  MARSingleFormUtil.AddressWrap();
                     addressWrap.LocId = mfs.Location_ID__c;
                     addressWrap.Address = mfs.Site_Name__c;
                     marFormAddressMaps.put(mfs.Id, addressWrap);    
                //}  
                //Changes made to accept unverified/unformatted Site Address from MAR form MSEU-11380                           
                 marFormList.add(mfs);
            }
            
            if (marFormList.size()>0) {
                //Call Util function to get Site and Mar staging ID maps 
                Map<String,Id>Recordtypemaps = HelperUtility.pullAllRecordTypes('MAR__c');
                Id webFormRecordTypeid = Recordtypemaps.get('Web Form');
                marFormSiteMaps = MARSingleFormUtil.createExtractSite(marFormAddressMaps);
                list<Error_Logging__c> errQueryExcepList = new List<Error_Logging__c>();                                
                //System.debug(marFormSiteMaps);
                //Adding try Catch block to capture any query exception and error logging
                try {                
                    //Get Exiting Contacts based on Contact names
                    if(!contactNameSet.isEmpty()) {
                    for(Contact con : [Select id,Name,Email,Preferred_Phone__c,MAR__c from Contact where Name IN :contactNameSet AND RecordTypeId =:externalContactRecTypeId]){
                        if(!(con.Email == null && con.Preferred_Phone__c == null)){
                            if(con.Email != null && con.Preferred_Phone__c == null){
                                if(con.MAR__c == true){
                                    existingMARContactMap.put(con.Name.toLowerCase() + con.Email,con);
                                }
                                else{
                                    existingNonMARContactMap.put(con.Name.toLowerCase() + con.Email,con);
                                }
                            }
                            else if(con.Preferred_Phone__c != null && con.Email == null){
                                if(con.MAR__c == true){
                                    existingMARContactMap.put(con.Name.toLowerCase() + con.Preferred_Phone__c.replaceAll('\\D',''),con);
                                 }
                                 else{
                                     existingNonMARContactMap.put(con.Name.toLowerCase() + con.Preferred_Phone__c.replaceAll('\\D',''),con);
                                 }
                            }
                            else if(con.Email != null && con.Preferred_Phone__c != null){
                                if(con.MAR__c == true){
                                    existingMARContactEmailPhoneMap.put(con.Name.toLowerCase() + con.Email,con);
                                    existingMARContactEmailPhoneMap.put(con.Name.toLowerCase() + con.Preferred_Phone__c.replaceAll('\\D',''),con);
                                }
                                else{
                                    existingNonMARContactEmailPhoneMap.put(con.Name.toLowerCase() + con.Email,con);
                                    existingNonMARContactEmailPhoneMap.put(con.Name.toLowerCase() + con.Preferred_Phone__c.replaceAll('\\D',''),con);
                                }
                            }
                        }
                    }
                  }
                }
                catch(QueryException e) {
                	errQueryExcepList.add(new Error_Logging__c(Name='MAR Single Form - Contact Query Exception',Error_Message__c=e.getMessage()));                    
                }
                if(!errQueryExcepList.isEmpty()){
                    insert errQueryExcepList;
                }
                System.debug('created contact maps');
                
                //Loop with Mar form staging record to create Mar
                for (MAR_Form_Staging__c mfs :marFormList){
                
                    MAR__c mar = new MAR__c();  
                    
                    //get contact and alt Contact link for mar
                    Contact con;
                    Contact altCon;
                    
                    if (!String.isBlank(mfs.First_Name__c)) {
                        MARSingleFormUtil.ContactWrap conWrap = new MARSingleFormUtil.ContactWrap(mfs.First_Name__c,mfs.Last_Name__c,mfs.Phone_Number__c,mfs.Email_Address__c,mfs.Additional_Phone_Number__c,duplicateExistingContactSet,duplicateContactCheckMap,existingMARContactMap,existingNonMARContactMap,existingMARContactEmailPhoneMap,existingNonMARContactEmailPhoneMap,upsertContactList);
                        MARSingleFormUtil.ContactWrap conPack = MARSingleFormUtil.createExtractMARContact(conWrap);
                        con = conPack.con;
                        duplicateContactCheckMap = conPack.duplicateContactCheckMap;
                        duplicateExistingContactSet = conPack.duplicateExistingContactSet;
                        upsertContactList = conPack.upsertContactList;
                        mar.Contact__r = con;
                    }
                    
                    if (!String.isBlank(mfs.Alt_First_Name__c)) {
                        MARSingleFormUtil.ContactWrap altConWrap = new MARSingleFormUtil.ContactWrap(mfs.Alt_First_Name__c,mfs.Alt_Last_Name__c,mfs.Alt_Phone_Number__c,mfs.Alt_Email_Address__c,mfs.Alt_Additional_Phone_Number__c,duplicateExistingContactSet,duplicateContactCheckMap,existingMARContactMap,existingNonMARContactMap,existingMARContactEmailPhoneMap,existingNonMARContactEmailPhoneMap,upsertContactList);
                        
                        MARSingleFormUtil.ContactWrap altConPack = MARSingleFormUtil.createExtractMARContact(altConWrap);
                        altCon = altConPack.con;
                        duplicateContactCheckMap = altConPack.duplicateContactCheckMap;
                        duplicateExistingContactSet = altConPack.duplicateExistingContactSet;
                        upsertContactList = altConPack.upsertContactList;
                        mar.Alternate_Contact__r = altCon;
                    }
                                        
                    //get Site link for mar
                    mar.site__c = marFormSiteMaps.get(mfs.id);
                    mar.MAR_Form_Staging__c = mfs.id;
                    marInsertList.add(mar);
                    
                    //get other details
                    mar.Additional_Information_Text__c = mfs.Additional_Information_Text__c;
                   // mar.Medical_Alarm_Type__c = MARSingleFormUtil.getAlarmType(mfs.Who_Does_Alarm_Call__c);
                    mar.How_did_you_hear__c = mfs.How_did_you_hear__c;
                    mar.Medical_Alarm_Brand__c = mfs.MAR_Alarm_Brand__c;
                    mar.Medical_Alarm_Brand_Other__c = mfs.MAR_Alarm_Brand_Other__c;
                    mar.Who_Does_Alarm_Call__c = mfs.Who_Does_Alarm_Call__c;
                    mar.name = mfs.name;
                    mar.Ownership__c = 'MARSingleForm';
                    mar.Data_Source__c = 'Web - Online Registration';
                    mar.RecordTypeId = webFormRecordTypeid;
                    mar.Provided_Date__c = date.today();
                    //update Mar Form Staging data
                    mfs.MAR__r = mar;
                    mfs.status__c = 'Processed';               
                    marFormUpdateList.add(mfs);
                }
                
                system.debug('@@@@@@@@@');
                // Contact DML Statement
                if(upsertContactList.size() > 0){
                    Schema.SObjectField f = Contact.Fields.Id;
                    Database.UpsertResult[] upsrList = Database.upsert(upsertContactList,f,false);
    
                    list<Error_Logging__c> errContactList = new List<Error_Logging__c>();
                    for (Database.UpsertResult upsr : upsrList) {
                        if (!upsr.isSuccess()) {               
                            for(Database.Error err : upsr.getErrors()) {
                                errContactList.add(new Error_Logging__c(Name='MAR Single Form - Contact Insert/Update Error',Error_Message__c=err.getMessage(),Fields_Afftected__c=String.valueof(err.getFields()),isSuccess__c=upsr.isSuccess(),Status_Code__c=String.valueof(err.getStatusCode()),isCreated__c= upsr.isCreated()));
                            }
                        }
                    }
    
                    if(errContactList.size() > 0){
                        insert errContactList;
                    }
                }       
    
                // MAR DML statement
                if(marInsertList.size() > 0){
                    
                    for(MAR__c mar: marInsertList){
                        
                        if(mar.Contact__r != null && mar.Contact__r.id != null){
                            mar.Contact__c = mar.Contact__r.id;
                        }
                        else{
                            mar.Contact__r = null;
                        }
                        
                        if(mar.Alternate_Contact__r != null && mar.Alternate_Contact__r.id != null){
                            mar.Alternate_Contact__c = mar.Alternate_Contact__r.id;
                        }
                        else{
                            mar.Alternate_Contact__r = null;
                        }
                        
                        system.debug('@@@@@@@@@@@' + mar.Contact__c  + '@@@@@@@@@@@@' + mar.Alternate_Contact__c);
                    }
                    
                    Database.SaveResult[] srList = Database.insert(marInsertList, false);
                    
                    List<Error_Logging__c> errMARList = new List<Error_Logging__c>();
                    for (Database.SaveResult sr : srList){
                        if (!sr.isSuccess()){ 
                            for(Database.Error err : sr.getErrors()){
                                errMARList.add(new Error_Logging__c(Name='MAR Single Form - MAR Insert Error' ,Error_Message__c=err.getMessage(),Fields_Afftected__c=String.valueof(err.getFields()),isSuccess__c=sr.isSuccess(),Status_Code__c=String.valueof(err.getStatusCode())));
                            }
                        }
                    }  
                    
                    if(errMARList.size() > 0){
                        insert errMARList;
                    }
                }           
                
                // MAR Form Staging DML statement
                if(marFormUpdateList.size() > 0){
                    
                    for(MAR_Form_Staging__c marForm : marFormUpdateList){
                        
                        if(marForm.mar__r != null && marForm.mar__r.id != null){
                            marForm.mar__c = marForm.mar__r.id;
                        }
                        else{
                            marForm.mar__c = null;
                        }
                    }
                   
                    Database.SaveResult[] srList = Database.update(marFormUpdateList, false);
                    
                    List<Error_Logging__c> errMARList = new List<Error_Logging__c>();
                    for (Database.SaveResult sr : srList){
                        if (!sr.isSuccess()){ 
                            for(Database.Error err : sr.getErrors()){
                                errMARList.add(new Error_Logging__c(Name='MAR Single Form - MAR Staging Update Error' ,Error_Message__c=err.getMessage(),Fields_Afftected__c=String.valueof(err.getFields()),isSuccess__c=sr.isSuccess(),Status_Code__c=String.valueof(err.getStatusCode())));
                            }
                        }
                    }  
                    
                    if(errMARList.size() > 0){
                        insert errMARList;
                    }
                }   
            }   
    }
}