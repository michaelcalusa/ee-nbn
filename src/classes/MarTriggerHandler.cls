/*------------------------------------------------------------------------
Author:        Shuo Li	
Company:       NBN
Description:   A class created to manage trigger actions from the Mar  object 
               Responsible for:
               1 - Rebuild Mar Contact based on the Contact and Alternate Contact informaiton for Web Form Mar Record Only 
Test Class:    MARSingleForm_Test
Created   :    24/10/2017    
History
<Date>            <Authors Name>    <Brief Description of Change> 
21/11/2018        Ganesh Sawant     MSEU-19589 MAR Records Contact Issue where Houshold account are not being created
                                    because of order of execution issue, retriggering the contact logic with touch update
                                    to Contact so Contact trigger creates household accounts if AccountId is blank
--------------------------------------------------------------------------*/

public without sharing class MarTriggerHandler {
    
    public static final string MAR_OBJECT_API = 'Mar__c';
    public static final string MAR_WEB_FORM_RECORTDYPENAME = 'Web Form';
    private static final String sendMarDetailsToAppian = 'SendMarDetailsToAppian';

    private List<MAR__c> trgOldList = new List<MAR__c> ();
    private List<MAR__c> trgNewList = new List<MAR__c> ();
    private Map<id,MAR__c> trgOldMap = new Map<id,MAR__c> ();
    private Map<id,MAR__c> trgNewMap = new Map<id,MAR__c> ();


    public MarTriggerHandler(List<MAR__c> trgOldList, List<MAR__c> trgNewList, Map<id,MAR__c> trgOldMap, Map<id,MAR__c> trgNewMap){
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
    }

    public void OnAfterInsert()
    {
        if(!HelperUtility.isTriggerMethodExecutionDisabled('MarContactAdjustment')){
            system.debug('OnAfterInsert - trgOldMap....'+trgNewMap);
            adjustMarContact(trgNewList, new Map<id,MAR__c>());

            //re-qualify site and publish to event for posting MAR self-registrationd details to Appian
            if(GlobalUtility.getReleaseToggle('Release_Toggles__mdt','SendMARSelfRegToAppain','IsActive__c')){
                updateSiteInfoAndSendToAppian(String.valueOf(trgNewMap.keySet()).removeEnd('}').removeStart('{'));
            }
        }
    }

    public void OnAfterUpdate()
    {
        if(!HelperUtility.isTriggerMethodExecutionDisabled('MarContactAdjustment')){
            system.debug('OnAfterUpdate - trgOldMap....'+trgOldMap);
            touchUpdateMarContacts(trgNewList);
            adjustMarContact(trgNewList, trgOldMap);
        }
    }

    /*
    	Function to recreate Mar Contact records for the changes/new Contact and Alternate Contact
    */
    public void adjustMarContact (List<MAR__c> trgNewList,Map<id,MAR__c> trgOldMap){
    	Map<String,Id>Recordtypemaps = HelperUtility.pullAllRecordTypes(MAR_OBJECT_API);
        String webFormRecordTypeid = Recordtypemaps.get(MAR_WEB_FORM_RECORTDYPENAME);
        system.debug('webFormRecordTypeid...'+ webFormRecordTypeid);
        map<Id, set<Id>> marContactsMaps = new map<Id,set<Id>>();
        set<String>ExistingMarContact = new set<String>();
        set<Id>contactIds= new set<Id>();

        List<Mar_Contact__c>removeMarContactList = new list<Mar_Contact__c>();
        List<Mar_Contact__c>addMarContactList = new list<Mar_Contact__c>();

        for (MAR__c mar: trgNewList) {
        	system.debug('Mar RecordTypeId...'+ mar.recordTypeId);
        	if (mar.recordTypeId == webFormRecordTypeid) {
        		boolean valid = false;
        		if (trgOldMap.IsEmpty()) {
        			valid = true;
        		} else {
        			MAR__c m_old = trgOldMap.get(mar.id);
        			if (m_old.contact__c != mar.Contact__c || m_old.Alternate_Contact__c != mar.Alternate_Contact__c) {
        				valid = true;
        			}
        		}
        		if (valid) {
	        		contactIds = new set<id>();
	        		contactIds.add(mar.Contact__c);
	        		if (mar.Alternate_Contact__c != null) {
	        			contactIds.add(mar.Alternate_Contact__c);
	        		}
	        		marContactsMaps.put(mar.id, contactIds);
        		}
        	}
        }
        system.debug('Processed Mar Contact maps...'+ marContactsMaps);
        if (!marContactsMaps.Isempty()) {
        	//Get Existing Mar Contact maps and create remove list
        	for (Mar_Contact__c marContact: [Select Id, Mar__c, Contact__c,MAR_Contact_Unique__c from Mar_Contact__c where Mar__c in :marContactsMaps.keySet()]) {
        		 contactIds = marContactsMaps.get(marContact.Mar__c);
        		 if (contactIds.contains(marContact.Contact__c)) {
        		 	 ExistingMarContact.add(marContact.MAR_Contact_Unique__c);
        		 }else {
        		 	removeMarContactList.add(marContact);
        		 }
        	}
        	//Loop new MarContact to add new ones
        	for (Id marId: marContactsMaps.keySet()) {
        		 contactIds = marContactsMaps.get(marId);
        		 for (Id contactId:contactIds) {
        		 	 string PrimaryKey = marId +''+ contactId;
        		 	 if (!ExistingMarContact.contains(PrimaryKey) && contactId != null) {
        		 	 		Mar_Contact__c mc = new Mar_Contact__c();
        		 	 		mc.Mar__c = marId;
        		 	 		mc.Contact__c = contactId;
        		 	 		mc.MAR_Contact_Unique__c = PrimaryKey;
        		 	 		addMarContactList.add(mc);
        		 	 }
        		 }
        	}
        	system.debug('removeMarContactList Size...'+ removeMarContactList.Size());
        	system.debug('addMarContactList Size...'+ addMarContactList.Size());
        	if (removeMarContactList.Size()>0) {
        		delete removeMarContactList;
        	}
        	if (addMarContactList.Size() > 0) {
        		insert addMarContactList;
        	}

        }






    }

    /**
     * Method to re-qualify site and then publish to event for sending MAR self-registration to Appian
     */
    @future(callout=true)
    public static void updateSiteInfoAndSendToAppian(String idList) {

        try{

            if (String.isNotBlank(idList)) {
                List<Site__c> sites = [
                        Select Id, Name,Serviceability_Class__c, Rollout_Type__c, Technology_Type__c,Location_Id__c,
                                Site_Address__c,Qualification_Date__c,RecordTypeId, Unit_Number__c,Road_Name__c,
                                Level_Number__c, Lot_Number__c, Road_Number_1__c, Road_Number_2__c, Road_Suffix_Code__c,
                                Post_Code__c,Locality_Name__c,
                                State_Territory_Code__c, Requalification_Failed_Date__c, CreatedDate
                        from Site__c
                        where Id in (select Site__c from MAR__c where Id in :idList.split('[,]{1}[\\s]?'))
                ];

                //Re-qualify the site before sending to Appian
                for (Site__c site : sites) {
                    SiteQualification_CX SiteQualification_CX_Con = new SiteQualification_CX(new ApexPages.StandardController(site));
                    try {
                        SiteQualification_CX_Con.reQualifySiteRecord();
                    } catch (Exception e) {
                        GlobalUtility.logMessage('Error', 'MarTriggerHandler', 'updateSiteInfo', '', 'Error in re-qualifying site', e.getMessage(), '', e, 0);
                    }
                }

                //publish to event for sending to Appian
                Database.SaveResult dSaveResult = EventBus.publish(new MAR_Event__e(ID_List__c = idList, Event_type__c = sendMarDetailsToAppian));
                if (!dSaveResult.isSuccess()) {
                    for (Database.Error dbErr : dSaveResult.getErrors()) {
                        GlobalUtility.logMessage('Error', 'MarTriggerHandler', 'updateSiteInfo', '', 'Error in posting message to Event queue', dbErr.getMessage(), '', null, 0);
                    }
                }
            }

        } catch (Exception e){
            GlobalUtility.logMessage('Error', 'MarTriggerHandler', 'updateSiteInfo', '', 'Error in processing method updateSiteInfo', e.getMessage(), '', e, 0);
        }
    }
    
    public void touchUpdateMarContacts(list<MAR__c> trgNewList){
    	list<Contact> touchUpdateConList = new list<Contact>();
    	
    	for(MAR__c mar : [Select id, Contact__c, Contact__r.AccountId, Alternate_Contact__c, Alternate_Contact__r.AccountId From MAR__c Where ID IN : trgNewList]){
	        if(mar.Contact__c!=null && mar.Contact__r.AccountId == null){
		        Contact con = new Contact();
		        con.Id = mar.Contact__c;
		        touchUpdateConList.add(con);
	        }
	        if(mar.Alternate_Contact__c!=null && mar.Alternate_Contact__c!= mar.Contact__c && mar.Alternate_Contact__r.AccountId == null){
		        Contact con = new Contact();
		        con.Id = mar.Alternate_Contact__c;
		        touchUpdateConList.add(con);
	        }   
        }
        System.debug('Contact where Household Account was not created'+touchUpdateConList);
        if(!touchUpdateConList.isEmpty()){
        	update touchUpdateConList;
        }
    }    
}