/**
 * Created by shuol on 18/09/2018.
 */

public with sharing class MarUpdateAppianRegistration {

    @Future(Callout=true)
    public static void updateAppianRegistration_Future(Set<Id> caseIds) {
        updateAppianRegistration(caseIds);
    }

    public static void updateAppianRegistration(Set<Id> caseIds) {
        String httpRequestBody = '';
        // MarUtil.wait(400);
        List<UpdateAppianRegistration>requestLists = new List<UpdateAppianRegistration>();
        for (Case marCase : [SELECT Id, CaseNumber, ClosedDate, LastModifiedDate,
                Serviceable_Call_Completed__c, Serviceable_Call_Completed_Date__c, NBN_UMA_Migration_Decision__c,
                NBN_UMA_Migration_Decision_Timestamp__c, Status, MAR_Sub_Status__c, Mar_External_Id__c,
                Alarm_Tested_Verified_by_EU_Successfully__c, Case_Type__c, ContactId, Secondary_Contact_ID__c,
                Primary_Contact_Role__c, Secondary_Contact_Role__c
        FROM Case WHERE Id IN :caseIds]) {
            // system.debug('Processed Case: ' + marCase);
            UpdateAppianRegistration UpdateAppianRegistration = new UpdateAppianRegistration();
            UpdateAppianRegistration.salesforceCaseId = marCase.Id;
            UpdateAppianRegistration.status = marCase.Status;
            UpdateAppianRegistration.subStatus = marCase.MAR_Sub_Status__c;
            UpdateAppianRegistration.isServiceTested = marCase.Alarm_Tested_Verified_by_EU_Successfully__c;
            UpdateAppianRegistration.type = marCase.Case_Type__c;
            UpdateAppianRegistration.serviceId = marCase.Mar_External_Id__c;
            UpdateAppianRegistration.primaryContact = marCase.ContactId;
            UpdateAppianRegistration.secondaryContact = marCase.Secondary_Contact_ID__c;
            UpdateAppianRegistration.primaryContactRole = marCase.Primary_Contact_Role__c;
            UpdateAppianRegistration.secondaryContactRole = marCase.Secondary_Contact_Role__c;
            UpdateAppianRegistration.caseNumber = marCase.CaseNumber;
            UpdateAppianRegistration.lastModifiedDate = String.valueOf(marCase.LastModifiedDate);
            UpdateAppianRegistration.serviceableCallCompleted = String.valueOf(marCase.Serviceable_Call_Completed__c);
            UpdateAppianRegistration.serviceableCallCompletedDate = String.valueOf(marCase.Serviceable_Call_Completed_Date__c);
            UpdateAppianRegistration.nbnUmaMigrationDecision = marCase.NBN_UMA_Migration_Decision__c;
            UpdateAppianRegistration.nbnUmaMigrationDecisionTimestamp = String.valueOf(marCase.NBN_UMA_Migration_Decision_Timestamp__c);
            UpdateAppianRegistration.closedDate = String.valueOf(marCase.ClosedDate);
            requestLists.add(UpdateAppianRegistration);
        }

        if (requestLists.size() != 0) {
            httpRequestBody = JSON.serialize(requestLists);

            Map<String, String> httpHeaderMap = new Map<String, String>();
            httpHeaderMap.put('Content-Type','application/json');

            String httpMethod = 'POST';
            String httpEndPoint = GlobalUtility.getMDConfiguration('MarAppianEndPoint') + '/webapi/cases';

            try {
                String httpResponseBody = '';
                if (String.isBlank(httpResponseBody)) {
                    HttpResponse httpResponse = MarUtil.sendHTTPRequest(httpRequestBody, httpHeaderMap, httpMethod, httpEndPoint);
                    httpResponseBody = httpResponse.getBody();
                    GlobalUtility.logMessage('Info', 'httpUpdateAppianRegistrationCallOut', 'MarUpdateAppianRegistration.updateAppianregistion', '', 'HTTP EndPoint: ' + httpEndPoint
                            , 'HTTP EndPoint: ' + httpEndPoint + '////// Response Status Code: ' + httpResponse.getStatusCode() + '\n Response Body: ' + httpResponseBody, 'Message Sent: ' + httpRequestBody, null, 0);
                    if (httpResponse.getStatusCode() != 200 && httpResponse.getStatusCode() != 201) {
                        GlobalUtility.logMessage('Error', 'httpUpdateAppianRegistrationCallOut', 'MarUpdateAppianRegistration.updateAppianregistion', '', 'HTTP EndPoint: ' + httpEndPoint
                                , 'HTTP EndPoint: ' + httpEndPoint + '////// Response Status Code: ' + httpResponse.getStatusCode() + '\n Response Body: ' + httpResponseBody, 'Message Sent: ' + httpRequestBody, null, 0);
                    }
                }
            } catch (Exception e) {
                GlobalUtility.logMessage('Error', 'httpUpdateAppianRegistrationCallOut', 'MarUpdateAppianRegistration.updateAppianregistion', '', 'HTTP EndPoint: ' + httpEndPoint
                        , 'Error calling Appian ', 'Failed message: ' + null, e, 0);
            }
        }

    }

    public class UpdateAppianRegistration {
        public Id salesforceCaseId { get; set; }
        public String status { get; set; }
        public String subStatus { get; set; }
        public Boolean isServiceTested { get; set; }
        public String serviceId { get; set; }
        public String type { get; set;}
        public Id  primaryContact { get; set; }
        public Id  secondaryContact { get; set; }
        public String  primaryContactRole { get; set; }
        public String  secondaryContactRole { get; set; }
        public String caseNumber {get; set;}
        public String lastModifiedDate {get; set;}
        public String serviceableCallCompleted {get; set;}
        public String serviceableCallCompletedDate {get; set;}
        public String nbnUmaMigrationDecision {get; set;}
        public String nbnUmaMigrationDecisionTimestamp {get; set;}
        public String closedDate{get; set;}
    }


}