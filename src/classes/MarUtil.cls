/***************************************************************************************************
    Class Name          : MarUtil
    Version             : 1.0
    Created Date        : 20-Aug-2018
    Author              : Arpit Narain
    Description         : Mar Utility Class
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/

public class MarUtil {

    public static final String productInfoErrorMessage = GlobalUtility.getMDConfiguration('Product_Info_Error_Message');
    public static final String productInfoMultipleCsllErrorMessage = GlobalUtility.getMDConfiguration('Product_Info_Mutiple_CSLL_Message');
    public static String alarmOwner = 'Alarm Owner';
    public static String preferredContact = 'Preferred Contact';

    public static MarProductInfoViewOnlyController.MarProductInfoWrapper getProductInfoViewOnly (String caseID) {

        MarProductInfoViewOnlyController.MarProductInfoWrapper marProductInfoWrapper = new MarProductInfoViewOnlyController.MarProductInfoWrapper();
        Map<String, String> httpHeaderMap = new Map<String, String>();


        Case marCase = [
                SELECT Id,Site__c, Site__r.Location_Id__c,NBN_Active__c,CSLL_ACTIVE__c,CSLL_Termination_Date__c,Inflight_Order__c,MAR_Order_Status__c,Activated_Date__c,
                        BATTERY_BACKUP_SERVICE_BBU__c,Voice_RSP__c,NBN_RSP__c,Product_Info_Last_Refresh_Date__c
                FROM Case
                WHERE Id = :caseID
        ];

        String httpMethod = 'POST';
        String httpEndPoint = GlobalUtility.getMDConfiguration('MarAppianEndPoint') + '/webapi/clk-getproductinfo/?locationid=' + marCase.Site__r.Location_Id__c;
        try {

            String httpResponseBody = '';
            httpHeaderMap.put('Content-Type','application/json');

            HttpResponse httpResponse = sendHTTPRequest('', httpHeaderMap, httpMethod, httpEndPoint);
            httpResponseBody = httpResponse.getBody();
            GlobalUtility.logMessage('Info', 'httpProductInfoCallOut', 'MarUtil.getProductInfo', '', 'HTTP EndPoint: ' + httpEndPoint
                    , 'HTTP EndPoint: ' + httpEndPoint + '::::::: Response Status Code: ' + httpResponse.getStatusCode() + '\n Response Body: ' + httpResponseBody, 'Message Sent: ' + null, null, 0);

            if (httpResponse.getStatusCode() == 200) {
                processSuccessfulResponse(httpResponseBody, marProductInfoWrapper);

            } else {
                marProductInfoWrapper.errors = productInfoErrorMessage;
            }


        } catch (Exception e) {
            GlobalUtility.logMessage('Error', 'httpProductInfoCallOut', 'MarUtil.getProductInfo', '', 'HTTP EndPoint: ' + httpEndPoint
                    , 'Error calling Appian ', 'Failed message: ' + null, e, 0);
            marProductInfoWrapper.errors = productInfoErrorMessage;
        }
        return marProductInfoWrapper;

    }

    public static void processSuccessfulResponse (String httpResponseBody, MarProductInfoViewOnlyController.MarProductInfoWrapper marProductInfoWrapper) {


        if (String.isBlank(httpResponseBody)) {
            marProductInfoWrapper.errors = productInfoErrorMessage;
            return;
        }

        MarDeSerialiseProductInfo marDeSerialiseProductInfo = new MarDeSerialiseProductInfo();
        MarDeSerialiseProductInfo.DeSerialiseProductInfo deSerialiseProductInfo = marDeSerialiseProductInfo.parse(httpResponseBody);
        System.debug('deSerialiseProductInfo :' + deSerialiseProductInfo);

        if ('Yes'.equalsIgnoreCase(deSerialiseProductInfo.isMultiple)) {
            marProductInfoWrapper.errors = productInfoMultipleCsllErrorMessage;

        } else {

            marProductInfoWrapper.lastRefreshDate = System.now();
            marProductInfoWrapper.csllStatus = deSerialiseProductInfo.csllStatus;
            marProductInfoWrapper.nbnActive = deSerialiseProductInfo.nbnActiveStatus;
            marProductInfoWrapper.csllTerminationDate = deSerialiseProductInfo.csllTerminationDate;
            marProductInfoWrapper.activatedDate = deSerialiseProductInfo.activatedDate;
            marProductInfoWrapper.batteryBackup = deSerialiseProductInfo.batteryBackupService;
            marProductInfoWrapper.inflightOrder = deSerialiseProductInfo.inflightOrder;
            marProductInfoWrapper.rsp1 = deSerialiseProductInfo.rsp1;
            marProductInfoWrapper.orderStatus = deSerialiseProductInfo.orderStatus;
            marProductInfoWrapper.rsp2 = deSerialiseProductInfo.rsp2;
            marProductInfoWrapper.disconnectionDate = deSerialiseProductInfo.dd;
            marProductInfoWrapper.copperActive = deSerialiseProductInfo.activeCopper;
            marProductInfoWrapper.errors = deSerialiseProductInfo.errorMessage;
        }
    }

    public static String getProductInfo (String caseID) {
        String errors = '';
        return errors;
    }
    /**
       Return date time in GMT from String value
    */
    public static DateTime getDatetimeGmt (String dateString) {
        if (String.isNotBlank(dateString)) {
            return DateTime.valueOfGmt(dateString);
        } else {
            return null;
        }
    }
    /**
       Return date time from String value follows format dd/MM/yyyy hh:mm AM/PM
    */
    public static DateTime getDateTimeParser (String dateString) {
        if (String.isNotBlank(dateString)) {
            return DateTime.parse(dateString);
        } else {
            return null;
        }
    }

    /**
     Return date from String value follows format yyyy-MM-dd HH:mm:ss
    */

    public static Date getDate (String dateString) {
        if (String.isNotBlank(dateString)) {
            return Date.valueOf(dateString);
        } else {
            return null;
        }
    }

    /**
    Return date from String value follows format dd/MM/yyyy
   */

    public static Date getDateParser (String dateString) {
        if (String.isNotBlank(dateString)) {
            return Date.parse(dateString);
        } else {
            return null;
        }
    }

    /**
        Return date time based on locale settings
    */
    public static String getLocalDateTime (Datetime dateField) {
        if (dateField != null) {
            return dateField.format();
        }
        return '';

    }

    /**
     Return date based on locale settings
    */
    public static String getLocalDate (Date dateField) {
        if (dateField != null) {
            return dateField.format();
        }
        return '';
    }


    //get value from MD Configuration Setting
    public static String getValueFromMDCustomMetaData (String settingName, String defaultSettingName) {
        String marProductInfoFieldSetName = String.isBlank(GlobalUtility.getMDConfiguration().get(settingName))
                ? defaultSettingName : GlobalUtility.getMDConfiguration().get(settingName);
        return marProductInfoFieldSetName;
    }

    //return field names (comma separated) based on field set
    public static String getFieldNamesBasedOnFieldSet (String fieldSetName) {
        String fieldNames = '';
        List<Schema.FieldSetMember> fieldSetMembers = SObjectType.Case.FieldSets.getMap().get(fieldSetName).getFields();
        for (Schema.FieldSetMember fieldSetMember : fieldSetMembers) {
            fieldNames += ',' + fieldSetMember.getFieldPath();
        }
        return fieldNames;
    }

    /**
    * Generic method to call HTTP endpoint
    *
    * @return httpResponse
    */


    public static HttpResponse sendHTTPRequest (String messageBody, Map<String, String> messageHeader, String method, String endPoint) {

        Http http = new http();
        HttpRequest httpRequest = new HttpRequest();
        HttpResponse httpResponse = new HttpResponse();

        if (method != 'GET') {
            httpRequest.setBody(messageBody);
        }

        httpRequest.setMethod(method);
        httpRequest.setEndpoint(endPoint);
        for (String headerId : messageHeader.keySet()) {
            httpRequest.setHeader(headerId, messageHeader.get(headerId));
        }

        try {
            httpResponse = http.send(httpRequest);

        } catch (Exception e) {
            GlobalUtility.logMessage('Error', 'GlobalUtility', 'sendHTTPRequest', '', 'Could not send message to HTTP Endpoint', e.getMessage(), messageBody, e, 0);

        }
        return httpResponse;

    }
    public static void wait (Integer millisec) {
        if (millisec == null || millisec < 0) {
            millisec = 0;
        }
        Long startTime = DateTime.now().getTime();
        Long finishTime = DateTime.now().getTime();
        while ((finishTime - startTime) < millisec) {
            finishTime = DateTime.now().getTime();
        }
    }



    /**
   * COMSTRUC-5022 Update Case Details In Registion -- Publish Event when selected fields changes
  **/
    public void publishMarCaseDetailsChangesEvent(Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {

        Boolean releaseToggle = GlobalUtility.getReleaseToggle('Release_Toggles__mdt', 'Send_MAR_Case', 'IsActive__c');

        if (releaseToggle == null) {
            releaseToggle = false;
        }

        if (!Test.isRunningTest()) {
            if (!releaseToggle) {
                return;
            }
        }

        Id medicalAlarmID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId();
        List<Schema.FieldSetMember> trackedFields = SObjectType.Case.FieldSets.getMap().get('FS_MAR_Send_Appian').getFields();
        set<Id> caseIds = new set<Id>();
        for (Case newCase : (list<Case>)newMap.values()) {
            if (newCase.RecordTypeId == medicalAlarmID && String.isNotBlank(newCase.Mar_External_Id__c)) {
                system.debug('Trigger Appian Case Name:' + newCase.CaseNumber);
                Case oldCase = (Case) oldmap.get(newCase.Id);
                List<Schema.FieldSetMember> trackechecklist = new list<Schema.FieldSetMember>();
                trackechecklist.addAll(trackedFields);
                for (Schema.FieldSetMember fsm : trackechecklist) {
                    String fieldName = fsm.getFieldPath();
                    String oldValue = String.valueOf(oldCase.get(fieldName));
                    String newValue = String.valueOf(newCase.get(fieldName));
                    if (oldValue != newValue) {
                        caseIds.add(newCase.Id);
                    }
                }
            }
        }

        if (caseIds.size()!=0) {
            MarUpdateAppianRegistration.updateAppianRegistration_Future(caseIds);
        }

    }
}