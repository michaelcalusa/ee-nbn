/*
Author              : Arpit Narain
Created Date        : 14 Sep 2018
Description         : Test class for MarUtil Class
Modification Log    :
-----------------------------------------------------------------------------------------------------------------
History <Date>      <Authors Name>              <Brief Description of Change>

*/
@IsTest
public class MarUtilTest {

    @IsTest
    public static void checkGetProductInfoViewOnlyReturnsValidMarProductInfoWrapper () {

        String responseBody = '{"locationId":"LOC000000563017","isMultiple":"No","nbnActiveStatus":"Yes","csllStatus":"N/A","csllTerminationDate":"","inflightOrder":"No","orderStatus":"Connect - Complete","activatedDate":"18/02/2015 03:06 AM","batteryBackupService":"No","dd":"10/02/2017","activeCopper":"","rsp1":"Telstra Corporation Ltd"}';
        Test.setMock(HttpCalloutMock.class, new HttpMarCallOutImpl(200, responseBody));
        Case newCase = getMarCase();
        Test.startTest();
        MarProductInfoViewOnlyController.MarProductInfoWrapper marProductInfoWrapper = MarUtil.getProductInfoViewOnly(String.valueOf(newCase.Id));
        Test.stopTest();
        System.assertEquals('N/A', marProductInfoWrapper.csllStatus);
        System.assertEquals('Connect - Complete', marProductInfoWrapper.orderStatus);
        System.assertEquals('Yes', marProductInfoWrapper.nbnActive);

    }

    @IsTest
    public static void checkGetProductInfoViewOnlyReturnsNotOkStatusError () {

        String responseBody = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/><title>Error 500 ' +
                '[Input ProfileLocation: Root; DocumentLocation: /Root]: Unable to create JSON files from data, the document may not ' +
                'be well-formed json</title></head><body><h2>HTTP ERROR 500</h2><p>Problem accessing /ws/rest/scs/sites/LOC000038464041. ' +
                'Reason:<pre>    [Input ProfileLocation: Root; DocumentLocation: /Root]: Unable to create JSON files from data, ' +
                'the document may not be well-formed json</pre></p></body></html>';
        Test.setMock(HttpCalloutMock.class, new HttpMarCallOutImpl(500, responseBody));
        Case newCase = getMarCase();
        Test.startTest();
        MarProductInfoViewOnlyController.MarProductInfoWrapper marProductInfoWrapper = MarUtil.getProductInfoViewOnly(String.valueOf(newCase.Id));
        Test.stopTest();
        System.assertEquals(GlobalUtility.getMDConfiguration('Product_Info_Error_Message'), marProductInfoWrapper.errors);
    }


    @IsTest
    public static void checkGetProductInfoViewOnlyReturnsInvalidXMLBodyError () {

        String responseBody = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/><title>Error 500' +
                ' [Input ProfileLocation: Root; DocumentLocation: /Root]: Unable to create JSON files from data, the document may ' +
                'not be well-formed json</title></head><body><h2>HTTP ERROR 500</h2><p>Problem accessing /ws/rest/scs/sites/LOC000038464041.' +
                ' Reason:<pre>    [Input ProfileLocation: Root; DocumentLocation: /Root]: Unable to create JSON files from data, the document ' +
                'may not be well-formed json</pre></p></body></html>';
        Test.setMock(HttpCalloutMock.class, new HttpMarCallOutImpl(200, responseBody));
        Case newCase = getMarCase();
        Test.startTest();
        MarProductInfoViewOnlyController.MarProductInfoWrapper marProductInfoWrapper = MarUtil.getProductInfoViewOnly(String.valueOf(newCase.Id));
        Test.stopTest();
        System.assertEquals(GlobalUtility.getMDConfiguration('Product_Info_Error_Message'), marProductInfoWrapper.errors);
    }

    @IsTest
    public static void checkProcessSuccessfulResponseWhenMessageBodyEmptyIsNullThenErrorIsShown () {

        MarProductInfoViewOnlyController.MarProductInfoWrapper marProductInfoWrapper = new MarProductInfoViewOnlyController.MarProductInfoWrapper();
        Test.startTest();
        MarUtil.processSuccessfulResponse('',marProductInfoWrapper);
        Test.stopTest();
        System.assertEquals(GlobalUtility.getMDConfiguration('Product_Info_Error_Message'),marProductInfoWrapper.errors);
        System.assert(String.isBlank(marProductInfoWrapper.nbnActive));
    }

    @IsTest
    public static void checkProcessSuccessfulResponseWhenErrorMessageIsNotNullThenErrorIsShown () {

        String responseBody = '{"id":null,"errorMessage":"Product Instance Id was not found. No Product Instance was ' +
                'found for the provided Product Instance Id."}';
        MarProductInfoViewOnlyController.MarProductInfoWrapper marProductInfoWrapper = new MarProductInfoViewOnlyController.MarProductInfoWrapper();
        Test.startTest();
        MarUtil.processSuccessfulResponse(responseBody,marProductInfoWrapper);
        Test.stopTest();
        String expectedString = 'Product Instance Id was not found. No Product Instance was found for the provided Product Instance Id.';
        System.assertEquals(expectedString,marProductInfoWrapper.errors);
        System.assert(String.isBlank(marProductInfoWrapper.nbnActive));
    }

    @IsTest
    public static void checkProcessSuccessfulResponseWhenMultipleIsYesThenErrorIsShown () {

        String responseBody = '{"locationId":"LOC000000563017","isMultiple":"Yes","nbnActiveStatus":"Yes","csllStatus":"N/A","csllTerminationDate":"","inflightOrder":"No","orderStatus":"Connect - Complete","activatedDate":"18/02/2015 03:06 AM","batteryBackupService":"No","dd":"10/02/2017","activeCopper":"","rsp1":"Telstra Corporation Ltd"}';
        MarProductInfoViewOnlyController.MarProductInfoWrapper marProductInfoWrapper = new MarProductInfoViewOnlyController.MarProductInfoWrapper();
        Test.startTest();
        MarUtil.processSuccessfulResponse(responseBody,marProductInfoWrapper);
        Test.stopTest();
        System.assertEquals( GlobalUtility.getMDConfiguration('Product_Info_Mutiple_CSLL_Message'),marProductInfoWrapper.errors);
        System.assert(String.isBlank(marProductInfoWrapper.nbnActive));
    }

    @IsTest
    public static void checkGetDateReturnsNullIfDateStringIsEmptyString () {
        Date getDateValue = MarUtil.getDate('');
        System.assert(getDateValue == null);

    }

    @IsTest
    public static void checkGetDateReturnsNullIfDateTimeStringIsEmptyString () {
        DateTime getDateTimeValue = MarUtil.getDatetimeGmt('');
        System.assert(getDateTimeValue == null);

    }

    public static Case getMarCase () {

        // Create Site record
        Id recordTypeId = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId();
        Site__c UnverifiedSite = new Site__c();
        UnverifiedSite.RecordTypeId = recordTypeId;
        UnverifiedSite.Location_Id__c = 'LOC000106753648';
        UnverifiedSite.isSiteCreationThroughCode__c = true;
        insert UnverifiedSite;


        // Create Contact record
        Id conRecordTypeId = schema.SObjecttype.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId();
        Contact contactRec = new Contact ();
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        contactRec.recordTypeId = conRecordTypeId;
        insert contactRec;


        Id marRecTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Medical Alarm').getRecordTypeId();
        Case marCase = new Case(RecordTypeId = marRecTypeId,
                Site__c = UnverifiedSite.Id,
                ContactId = contactRec.id,
                Case_Concat__c = String.valueOf(UnverifiedSite.Id) + String.valueOf(contactRec.id),
                Product_Info_Last_Refresh_Date__c = System.Now(),
                CSLL_Termination_Date__c = System.Now(),
                Activated_Date__c = System.Now(),
                Status = 'New');
        insert marCase;


        return marCase;
    }

}