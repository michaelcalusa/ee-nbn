public with sharing class MaximoCallOut {
/***************************************************************************************************
Class Name          : MaximoCallOut
Author              : Suraj Malla
Description         : Class to make a callout to Maximo from Salesforce
Modification Log    :
* Developer             Date            Description
Suraj Malla        05-Jun-2018      	Created  
****************************************************************************************************/     
    public class maximoException extends Exception {}
    
    public static Map<String, String> SendCreateQuoteRequestToMaximo(Id optyId){
        Map<String, String> outcomeMap = new Map<String, String>();
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        request.setEndpoint('callout:MAXIMO');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', 'urn:processDocument');
        
        //For Maximo Test environment, there is an additional parameter needed in the request header. This variable will not be used in Production.
        Maximo_Integration_Variables__c objCusSetMax = Maximo_Integration_Variables__c.getInstance('maximoTestEnvHeaderVariable');
        String sMaxTestEnvHeaderVar = objCusSetMax.Value__c;
        
        if(sMaxTestEnvHeaderVar != null){
            request.setHeader('nbn-environment-override', sMaxTestEnvHeaderVar);
        }

        //Generate the XML body
        MaximoSOAPXMLHelper objBody = new MaximoSOAPXMLHelper();
        String sXMLRequest = objBody.generateXMLRequest(optyId);
        //system.debug('@@sXMLRequest= '+ sXMLRequest);
        String sXMLResponse;
        
        if(sXMLRequest != null){
            request.setBody(sXMLRequest);
            
            try{
                response = http.send(request);
                
                if(response.getStatusCode() != 200){
                    sXMLResponse = response.getBody();
                    throw new maximoException('There was an error while submitting Opportunity Id:' + optyId + '. The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
                }
                else{
                    sXMLResponse = response.getBody();
                    //system.debug('Response is 200'+sXMLResponse);
                    Map<String, String> respDataMap = new Map<String, String>();
                	
                	//sXMLResponse = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><CreateNBNINTMACREQUESTResponse xmlns="http://www.ibm.com/maximo" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" creationDateTime="2018-06-01T12:08:44+10:00" transLanguage="EN" baseLanguage="EN" messageID="4752568.1527818925613427141"><NBNINTMACREQUESTMboKeySet><NBNINTMACREQUEST><WONUM>MRQ10000000563</WONUM><CORRELATIONID>06a158f8-a89a-c0b0-ac3d-79a0245b0def</CORRELATIONID><NBNREFERENCEID>006O000000A6d8QIAR</NBNREFERENCEID><STATUS>WAPPR</STATUS><REPORTDATE>2018-06-05 15:58:18.0</REPORTDATE></NBNINTMACREQUEST></NBNINTMACREQUESTMboKeySet></CreateNBNINTMACREQUESTResponse></soapenv:Body></soapenv:Envelope>';
                   	//sXMLResponse = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><CreateNBNINTMACREQUESTResponse xmlns="http://www.ibm.com/maximo" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" creationDateTime="2018-06-01T12:14:03+10:00" transLanguage="EN" baseLanguage="EN" messageID="2960299.1527819244316165985"><NBNINTMACREQUESTMboKeySet><NBNINTMACREQUEST><ERRORKEY>000000</ERRORKEY><ERRORMSG>The following mandatory field or fields are not provided: Scope Of Work</ERRORMSG><CORRELATIONID>06a158f8-a89a-c0b0-ac3d-79a0245b0def</CORRELATIONID></NBNINTMACREQUEST></NBNINTMACREQUESTMboKeySet></CreateNBNINTMACREQUESTResponse></soapenv:Body></soapenv:Envelope>'; 
                	//sXMLResponse = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><CreateNBNINTMACREQUESTResponse xmlns="http://www.ibm.com/maximo" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" creationDateTime="2018-06-01T12:14:03+10:00" transLanguage="EN" baseLanguage="EN" messageID="2960299.1527819244316165985"><NBNINTMACREQUESTMboKeySet><NBNINTMACREQUEST><ERRORKEY>000000</ERRORKEY><ERRORMSG></ERRORMSG><CORRELATIONID>06a158f8-a89a-c0b0-ac3d-79a0245b0def</CORRELATIONID></NBNINTMACREQUEST></NBNINTMACREQUESTMboKeySet></CreateNBNINTMACREQUESTResponse></soapenv:Body></soapenv:Envelope>';    
                    
                	Dom.Document doc = new Dom.Document();
                    doc.load(sXMLResponse);
                    
                    //Retrieve the root element for this document.
                    Dom.XMLNode rootElement = doc.getRootElement();
                
                	//Traverse to reach the parent node of the data elements
                    do{
                        for(Dom.XmlNode childNode: rootElement.getChildElements()){
                            rootElement = childNode;
                            //System.debug('rootElement Name: ' + rootElement.getName());
                        } 
                    } while(rootElement.getName() != 'NBNINTMACREQUEST');
                    
                	//Fetch all values in a map
                    for(Dom.XmlNode childNode: rootElement.getChildElements()){
                        respDataMap.put(childNode.getName(), childNode.getText());
                    }
                	
                	//System.debug('respDataMap map: ' + respDataMap.Values());
                
                	//Check if a success or a validation error response was sent                	
                    if(respDataMap.containsKey('ERRORKEY') && respDataMap.containsKey('ERRORMSG')){
                        if(respDataMap.get('ERRORMSG') != ''){
                            outcomeMap.put('Validation Error', respDataMap.get('ERRORMSG'));
                        }else{
                            outcomeMap.put('Critical Error', 'There was a problem submitting the quote to Maximo. Please try after some time. If the problem persists, please contact the administrator or the technical team.');
                            throw new maximoException('Some of the error response mandatory elements are missing or are null in the maximo response for the Opportunity Id:' + optyId);
                        }   
                    }else{
                        //Success scenario
                        Opportunity objOpty = [SELECT Id, Costing_Status__c, Quote_Requested_Date__c, Quote_ID__c FROM Opportunity WHERE Id=:optyId];
                        
                        if(respDataMap.get('WONUM') != '' && respDataMap.get('STATUS') != '' && respDataMap.get('NBNREFERENCEID') != '' && respDataMap.get('REPORTDATE') != ''){
                            objOpty.Costing_Status__c = respDataMap.get('STATUS');
                            objOpty.Quote_ID__c = respDataMap.get('WONUM');
                            String requestedDate = respDataMap.get('REPORTDATE').substring(0,10);
                            objOpty.Quote_Requested_Date__c = Date.valueOf(requestedDate);

                            update objOpty;
                            outcomeMap.put('Success', 'The Quote creation request to Maximo is successful');
                        }else{
                            outcomeMap.put('Critical Error', 'There was a problem submitting the quote to Maximo. Please try after some time. If the problem persists, please contact the administrator or the technical team.');
                            throw new maximoException('Some of the success response mandatory elements are missing or are null in the maximo response for the Opportunity Id:' + optyId);
                        }                      
                    }
                 }
            }catch(Exception ex) {
                if(outcomeMap.isEmpty()){
                    outcomeMap.put('Critical Error', 'There was a problem submitting the quote to Maximo. Please try after some time. If the problem persists, please contact the administrator or the technical team.');
                }
                GlobalUtility.logMessage('Error', 'MaximoCallOut', 'CreateQuoteRequestMRQToMaximo:OpportunityId: ' + optyId, ' ', '', '', 
                                         '--------------------------------SF Request XML ------------------------------' + sXMLRequest + '--------------------------------Maximo Response XML ------------------------------' + sXMLResponse, 
                                         ex, 0);
            }         
        }
        return outcomeMap;
    }
    
}