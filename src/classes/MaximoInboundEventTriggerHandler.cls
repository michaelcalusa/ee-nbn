public class MaximoInboundEventTriggerHandler {
    /***************************************************************************************************
Class Name          : MaximoInboundEventTriggerHandler
Author              : Suraj Malla
Description         : Trigger Handler Class to process inbound messages from Maximo to Salesforce
Modification Log    :
* Developer             Date            Description
Suraj Malla        05-Jun-2018      	Created  
****************************************************************************************************/ 
    public static void processMaximoEvents(list<MaximoInboundIntegration__e> maxEvents){ 
        Set<String> setOptyId = new Set<String>();
        List<Opportunity> lstObjOptytoUpdate;
        Map<ID, Opportunity> eventOptyToProcessMap;
        Map<ID, Opportunity> eventOptyProcessedMap;
        
        
        try{
            Maximo_Integration_Variables__c objCusSetMax = Maximo_Integration_Variables__c.getInstance('QuoteInboundEventType');
            String sQuoteUpdateEvent = objCusSetMax.Value__c;
            
            //System.debug('@@sQuoteUpdateEvent: ' + sQuoteUpdateEvent);   
            for(MaximoInboundIntegration__e maxEv: maxEvents){
                if(maxEv.Event_Type__c == sQuoteUpdateEvent)
                    setOptyId.add(maxEv.Event_Id__c);
            }
            
            /// Query the Opportuntiy table with the Id's
            if(setOptyId.size() > 0){           
                //Stores the unique Opportunity records to be updated from the trigger
                eventOptyToProcessMap = new Map<ID, Opportunity>([Select Id, Name, Costing_Status__c, Quote_Received_Date__c FROM Opportunity WHERE Id =:setOptyId]);
                
                eventOptyProcessedMap = new Map<ID, Opportunity>();
                
                //Run the loop for all records in the trigger and for multiple updates to the same record, store the latest update against the record in the map.            
                for(MaximoInboundIntegration__e objMax : maxEvents){
                    if(objMax.Event_Id__c != null && eventOptyToProcessMap.containsKey(objMax.Event_Id__c )){
                        Opportunity objOptytoUpdate = eventOptyToProcessMap.get(objMax.Event_Id__c);
                        //Set the fields on the opportunity
                        objOptytoUpdate.Costing_Status__c = objMax.Status__c;
                        
                        if(objMax.MAC_Specialist__c != null){
                            objOptytoUpdate.MAC_Specialist__c = objMax.MAC_Specialist__c;
                        }
                        
                        if(objMax.Processed_Date__c != null){
                            if (objMax.Notify_Reason__c != null && objMax.Notify_Reason__c == 'NBN RFQ Endorsed'){
                                objOptytoUpdate.Quote_Received_Date__c = Date.valueOf(objMax.Processed_Date__c);
                            }
                            objOptytoUpdate.Quote_Status_Update_Date__c = Date.valueOf(objMax.Processed_Date__c);
                        }
                        
                        if(objMax.Expiration_Date__c != null){
                            objOptytoUpdate.Quote_Expiration_Date__c = Date.valueOf(objMax.Expiration_Date__c);
                        }
                        //--------- Endorsed Quote Update----
                        if(objMax.Quote_Value__c != null){
                            objOptytoUpdate.Quote_Value__c = objMax.Quote_Value__c;
                        }
                        
                        if(objMax.Notify_Reason__c != null){
                            objOptytoUpdate.Quote_Status__c = objMax.Notify_Reason__c;
                        }
                        //-----------------------------------
                        //Store the updated opportunity objects 
                        eventOptyProcessedMap.put(objMax.Event_Id__c, objOptytoUpdate);
                    }
                }
                
                if(eventOptyProcessedMap.size()> 0){
                    lstObjOptytoUpdate = eventOptyProcessedMap.values();
                    //System.debug('@@lstObjOptytoUpdate: ' + lstObjOptytoUpdate);
                    Update lstObjOptytoUpdate;
                }
                
            }
            
        }catch(Exception ex) {
            GlobalUtility.logMessage('Error', 'NotifyMac', 'NotifyMAC:ListOfOptygettingUpdated: ' + setOptyId, ' ', '', '', 
                                     'InboundEventsfromMaximo: '+ eventOptyToProcessMap + ' ------------------- Events processed and ready to update in SF----' + eventOptyProcessedMap 
                                     , ex, 0);
        }         
    }
}