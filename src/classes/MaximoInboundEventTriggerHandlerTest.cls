@isTest (seeAllData=false)
public class MaximoInboundEventTriggerHandlerTest {        
    static testmethod void maximoSubscribedEventsTest(){
        String sOptyId;
        List<MaximoInboundIntegration__e> toPublish = new List<MaximoInboundIntegration__e>();        
		try{
       		sOptyId = MaximoQuoteRequestControllerTest.createTestData();
            System.debug('*@#Created Opp Id:'+sOptyId);
       	}
        catch(Exception e){
            System.debug('****Exception in test data');
        }
         
        Maximo_Integration_Variables__c objCusSetMax = new Maximo_Integration_Variables__c(Name='QuoteInboundEventType',Value__c='Quote_Update');
        insert objCusSetMax;
		
        MaximoInboundIntegration__e b = new MaximoInboundIntegration__e();
       	b.Correlation_Id__c = '06a158f8-a89a-c0b0-ac3d-79a0245b0def';
        b.Event_Id__c = sOptyId;
        b.Event_Type__c = 'Quote_Update';
        b.Source__c = 'Maximo';
        b.Status__c = 'CAN';
       	b.Processed_Date__c = Date.valueOf('2018-06-27');
        b.Expiration_Date__c = Date.valueOf('2018-06-27');
        b.MAC_Specialist__c = 'James';
       	b.MAC_Request_Id__c = '23232323';
	b.Quote_Value__c = 5000.23;
        toPublish.add(b);        
        
        Test.startTest();
        
        List<Database.SaveResult> results = EventBus.publish(toPublish);           
        for (Database.SaveResult sr : results) {
    		if (sr.isSuccess()) {
        		System.debug('****Successfully published event.');
    		} 
        	else {
        		for(Database.Error err : sr.getErrors()) {
            	System.debug('****Error returned: ' +
                        err.getStatusCode() +
                        ' - ' +
                        err.getMessage());
        		}
    		}       
		}
        
        Test.stopTest();                
    }
    
}