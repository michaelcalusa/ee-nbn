public with sharing class MaximoQuoteRequestController {
/*------------------------------------------------------------------------
Author:        Suraj Malla
Company:       NBNco
Description:   Controller Class for the MaximoQuoteRequestPage 
History
22/05/18      Suraj Malla     Created
----------------------------------------------------------------------------*/    
    public Boolean bStartMaximoChecks{get;set;}
    public Boolean bSiteDtlsComplete{get;set;}
    public Boolean bOptytlsComplete{get;set;}
    
    public Boolean bSiteAdded{get; set;}
    List<Opportunity> lstCurrentOpty;
    public String integStatus{get;set;}
    public Boolean refreshPage {get; set;}
        
    public MaximoQuoteRequestController(ApexPages.StandardController std){
        Opportunity opty = (Opportunity) std.getRecord();
        bSiteAdded = false;
        bSiteDtlsComplete = false;
        bOptytlsComplete = false;
        refreshPage = false;
        bStartMaximoChecks = false; // Variable that determines whether the Create Maximo UI messages and validations trigger
        lstCurrentOpty = [SELECT ID, Site__c, StageName, Quote_ID__c, Create_MRQ_Description__c,Street_Type__c,Requested_Completion_Date__c,Network_Mapping__c,Service_Area_Module_SAM__c,Technology_Type__c,Sub_Team__c,Reference_Number__c, Site__r.Road_Name__c, Site__r.Road_Number_1__c, Site__r.Locality_Name__c, Site__r.Post_Code__c, Site__r.State_Territory_Code__c, (SELECT Attachment.Id,Attachment.Name FROM Attachments) FROM Opportunity WHERE ID =:opty.Id];		
    }
    
    public String getOpportunityId(){
        return lstCurrentOpty[0].Id;        
    }
    
    public String getQuoteId(){
        return lstCurrentOpty[0].Quote_ID__c;        
    }
    
    public PageReference allowSubmitRequest() {
        if(lstCurrentOpty[0].StageName == 'Costing' && lstCurrentOpty[0].Quote_ID__c == null ){
            bStartMaximoChecks = true;
            
            if(lstCurrentOpty[0].Site__c != null){
                bSiteAdded = true;                
                //System.debug('@@lstCurrentOpty[0]: ' + lstCurrentOpty[0]);              
                SObject sObjSite = lstCurrentOpty[0].getSObject('Site__r');
               
                if(String.valueOf(sObjSite.get('Road_Name__c')) != null && String.valueOf(sObjSite.get('Road_Number_1__c')) != null && String.valueOf(sObjSite.get('Locality_Name__c'))  != null && String.valueOf(sObjSite.get('Post_Code__c'))  != null && String.valueOf(sObjSite.get('State_Territory_Code__c')) != null){
                    bSiteDtlsComplete = true;
                }
                
                if(String.valueOf(lstCurrentOpty[0].Requested_Completion_Date__c) != null && lstCurrentOpty[0].Service_Area_Module_SAM__c != null && lstCurrentOpty[0].Network_Mapping__c != null && lstCurrentOpty[0].Technology_Type__c != null && lstCurrentOpty[0].Sub_Team__c != null){
                    bOptytlsComplete = true;
                }            
            }            
        }
        return null;
    }
    
    public PageReference requestMaximoQuote() {
        Map<String, String> resultsMap = new Map<String, String>();
        resultsMap =  MaximoCallOut.SendCreateQuoteRequestToMaximo(lstCurrentOpty[0].Id);
        if(resultsMap.get('Validation Error') != null){
            integStatus = resultsMap.get('Validation Error');
             //ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, integStatus));
        } else if(resultsMap.get('Success') != null){
            refreshPage = true;
        } else if(resultsMap.get('Critical Error') != null){
            integStatus = resultsMap.get('Critical Error');
        }
        
        System.debug('@@resultsMap' + resultsMap);
        return null;
    }
}