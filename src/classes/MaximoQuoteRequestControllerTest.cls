@isTest (SeeAllData=true)
public with sharing class MaximoQuoteRequestControllerTest {
    /*------------------------------------------------------------------------
Author:        Suraj Malla
Company:       NBNco
Description:   Test class for class MaximoQuoteRequestController
History
22/05/18      Suraj Malla     Created
----------------------------------------------------------------------------*/
    static Account objAccount;
    static Contact objContact;
    static Site__c objSite;
    static Opportunity objOpp;
    static Map<String, String> responseHeaders = new Map<String, String>();
    static String successResponse;
    static String validationErrorResponse;
    static String validationerrorInfoMissResponse;
    static String successInfoMissResponse;
    
    public static string createTestData(){		
        //Setup data
        objAccount = new Account(
            Name = 'Test CW Account',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId(),
            Tier__c = '1',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert objAccount;
        
        objContact = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId(),
            AccountId = objAccount.Id,
            Email = 'testing' + objAccount.Id + '@example.com',
            Preferred_Phone__c = '0211111111'
        );
        insert objContact;
        
        
        objSite = new Site__c(
            RecordTypeId = Schema.SObjectType.Site__c.getRecordTypeInfosByName().get('Unverified').getRecordTypeId(),
            Site_Address__c = '100 CHATSWOOD, NSW 2150',
            Name = '100 CHATSWOOD, NSW 2150',
            Unit_Number__c = '100',
            //Level_Number__c = '100',
            //Lot_Number__c = '401',
            //Road_Number_1__c = '411',
            Road_Name__c = 'CHATSWOOD',
            Road_Suffix_Code__c = 'RD',
            Locality_Name__c = 'SHAILER PARK',
            Post_Code__c = '4128',
            State_Territory_Code__c = 'NSW'
        );
        insert objSite;
        
        // Create  Opportunity
        objOpp =  new Opportunity(
            Name = 'Test Opp', 
            AccountId = objAccount.Id, 
            CloseDate = System.today(),
            StageName = 'New',
            recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Recoverable Works').getRecordTypeId(),
            Site__c = objSite.Id
        );
        insert objOpp;
        
        // Create Attachment
        Attachment attach=new Attachment();   	
        attach.Name='NBN-Test Attachment.zip';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.ContentType = 'application/x-zip-compressed';
        attach.body=bodyBlob;
        attach.parentId=objOpp.id;
        insert attach;
        
        return objOpp.Id;
    }
    
    static testMethod void testMaximoRequestButtonValidations(){		
        createTestData();
        
        //Validation Error Scenario: 
        //Set the RW Type, Fill up Maximo Fields leaving 1 blank and Change the Phase of the Opty to 'Costing' to trigger the validations
        
        objOpp.Requested_Completion_Date__c = Date.valueOf('2018-06-27');
        objOpp.Network_Mapping__c = 'Distribution Network';
        objOpp.Technology_Type__c = 'FIBRE';
        objOpp.StageName = 'Costing';
        objOpp.RW_Type__c = 'PCD Relocation';
        //objOpp.Service_Area_Module_SAM__c = 'SAM';		
        update objOpp;
        
        
        //Testing with some missing site and opportunity details 
        ApexPages.StandardController sc = new ApexPages.StandardController(objOpp);		
        MaximoQuoteRequestController quoteReq = new MaximoQuoteRequestController(sc);
        quoteReq.allowSubmitRequest();			
        
        System.assertEquals(quoteReq.bOptytlsComplete, false);
        
        // Now add all the details for the Site and Opportunity
        objSite.Road_Number_1__c = '411';
        update objSite;
        
        objOpp.Service_Area_Module_SAM__c = 'SAM';
        update objOpp;
        
        quoteReq = new MaximoQuoteRequestController(sc);
        quoteReq.allowSubmitRequest();
        
        System.assert(quoteReq.bOptytlsComplete);
        
        //Testing the Maximo callout scenarios
        
        Test.startTest();
                
        //1. Callout Successful scenario.
        successResponse = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><CreateNBNINTMACREQUESTResponse xmlns="http://www.ibm.com/maximo" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" creationDateTime="2018-06-01T12:08:44+10:00" transLanguage="EN" baseLanguage="EN" messageID="4752568.1527818925613427141"><NBNINTMACREQUESTMboKeySet><NBNINTMACREQUEST><WONUM>MRQ10000000563</WONUM><CORRELATIONID>06a158f8-a89a-c0b0-ac3d-79a0245b0def</CORRELATIONID><NBNREFERENCEID>006O000000A6d8QIAR</NBNREFERENCEID><STATUS>WAPPR</STATUS></NBNINTMACREQUEST></NBNINTMACREQUESTMboKeySet></CreateNBNINTMACREQUESTResponse></soapenv:Body></soapenv:Envelope>';
        
        //StaticResourceCalloutMock mockCallout = new StaticResourceCalloutMock();
        //mockCallout.setStaticResource('MaximoCalloutTest');
        //mockCallout.setStatusCode(200);
        //mockCallout.setHeader('Content-Type', 'application/xml');
        //Test.setMock(HttpCalloutMock.class, mockCallout);
        
        responseHeaders.put('Content-Type', 'application/xml');
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, successResponse,responseHeaders));      
        quoteReq.requestMaximoQuote();
        
        Test.stopTest();
        
    } 
    static testMethod void maximoCalloutValidationErrors() {
        
        createTestData();
        
        //Validation Error Scenario: 
        //Set the RW Type, Fill up Maximo Fields leaving 1 blank and Change the Phase of the Opty to 'Costing' to trigger the validations
        
        objOpp.Requested_Completion_Date__c = Date.valueOf('2018-06-27');
        objOpp.Network_Mapping__c = 'Distribution Network';
        objOpp.Technology_Type__c = 'FIBRE';
        objOpp.StageName = 'Costing';
        objOpp.RW_Type__c = 'PCD Relocation';
        //objOpp.Service_Area_Module_SAM__c = 'SAM';		
        update objOpp;

        //Testing with some missing site and opportunity details 
        ApexPages.StandardController sc = new ApexPages.StandardController(objOpp);		
        MaximoQuoteRequestController quoteReq = new MaximoQuoteRequestController(sc);

        //2. Callout ValidationError Scenario
        
        Test.startTest();
        validationerrorResponse = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><CreateNBNINTMACREQUESTResponse xmlns="http://www.ibm.com/maximo" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" creationDateTime="2018-06-01T12:14:03+10:00" transLanguage="EN" baseLanguage="EN" messageID="2960299.1527819244316165985"><NBNINTMACREQUESTMboKeySet><NBNINTMACREQUEST><ERRORKEY>000000</ERRORKEY><ERRORMSG>The following mandatory field or fields are not provided: Scope Of Work</ERRORMSG><CORRELATIONID>06a158f8-a89a-c0b0-ac3d-79a0245b0def</CORRELATIONID></NBNINTMACREQUEST></NBNINTMACREQUESTMboKeySet></CreateNBNINTMACREQUESTResponse></soapenv:Body></soapenv:Envelope>';    
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, validationerrorResponse,responseHeaders));
        quoteReq.requestMaximoQuote();
        
        
        validationerrorInfoMissResponse = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><CreateNBNINTMACREQUESTResponse xmlns="http://www.ibm.com/maximo" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" creationDateTime="2018-06-01T12:14:03+10:00" transLanguage="EN" baseLanguage="EN" messageID="2960299.1527819244316165985"><NBNINTMACREQUESTMboKeySet><NBNINTMACREQUEST><ERRORKEY>000000</ERRORKEY><ERRORMSG></ERRORMSG><CORRELATIONID>06a158f8-a89a-c0b0-ac3d-79a0245b0def</CORRELATIONID></NBNINTMACREQUEST></NBNINTMACREQUESTMboKeySet></CreateNBNINTMACREQUESTResponse></soapenv:Body></soapenv:Envelope>'; 
        
        //3. Critical Error Scenario (Mandatory response element is null)
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, validationerrorInfoMissResponse,responseHeaders));
        quoteReq.requestMaximoQuote();
        
        successInfoMissResponse = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><CreateNBNINTMACREQUESTResponse xmlns="http://www.ibm.com/maximo" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" creationDateTime="2018-06-01T12:08:44+10:00" transLanguage="EN" baseLanguage="EN" messageID="4752568.1527818925613427141"><NBNINTMACREQUESTMboKeySet><NBNINTMACREQUEST><WONUM></WONUM><CORRELATIONID>06a158f8-a89a-c0b0-ac3d-79a0245b0def</CORRELATIONID><NBNREFERENCEID>006O000000A6d8QIAR</NBNREFERENCEID><STATUS>WAPPR/STATUS></NBNINTMACREQUEST></NBNINTMACREQUESTMboKeySet></CreateNBNINTMACREQUESTResponse></soapenv:Body></soapenv:Envelope>';
       
        //4. Critical Error Scenario (Mandatory response element is null)
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, successInfoMissResponse,responseHeaders));
        quoteReq.requestMaximoQuote();
                
        successInfoMissResponse = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><CreateNBNINTMACREQUESTResponse xmlns="http://www.ibm.com/maximo" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" creationDateTime="2018-06-01T12:08:44+10:00" transLanguage="EN" baseLanguage="EN" messageID="4752568.1527818925613427141"><NBNINTMACREQUESTMboKeySet><NBNINTMACREQUEST><WONUM>MRQ10000000563</WONUM><CORRELATIONID>06a158f8-a89a-c0b0-ac3d-79a0245b0def</CORRELATIONID><NBNREFERENCEID>006O000000A6d8QIAR</NBNREFERENCEID><STATUS>ABC/STATUS></NBNINTMACREQUEST></NBNINTMACREQUESTMboKeySet></CreateNBNINTMACREQUESTResponse></soapenv:Body></soapenv:Envelope>';
        //5. Critical Error Scenario (Mandatory response element value is wrong)
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, successInfoMissResponse,responseHeaders));
        quoteReq.requestMaximoQuote();

        //6. Internal Server error
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(500, 'Internal server error',responseHeaders));
        quoteReq.requestMaximoQuote();

        Test.stopTest();
    }
    
    static testMethod void maximoServiceNotAvailableError() {
        
        createTestData();
        
        //Validation Error Scenario: 
        //Set the RW Type, Fill up Maximo Fields leaving 1 blank and Change the Phase of the Opty to 'Costing' to trigger the validations
        
        objOpp.Requested_Completion_Date__c = Date.valueOf('2018-06-27');
        objOpp.Network_Mapping__c = 'Distribution Network';
        objOpp.Technology_Type__c = 'FIBRE';
        objOpp.StageName = 'Costing';
        objOpp.RW_Type__c = 'PCD Relocation';
        //objOpp.Service_Area_Module_SAM__c = 'SAM';		
        update objOpp;

        ApexPages.StandardController sc = new ApexPages.StandardController(objOpp);		
        MaximoQuoteRequestController quoteReq = new MaximoQuoteRequestController(sc);
		
        Test.startTest();
        //6. Internal Server error
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(500, 'Internal server error',responseHeaders));
        quoteReq.requestMaximoQuote();

        Test.stopTest();
    }
}