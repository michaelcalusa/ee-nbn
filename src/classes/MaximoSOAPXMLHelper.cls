public class MaximoSOAPXMLHelper {
/*------------------------------------------------------------------------
Author:        Suraj Malla
Company:       NBNco
Description:   Class to generate the XML for maximo integration
History
22/05/18      Suraj Malla     Created
----------------------------------------------------------------------------*/

    String nameSpace;
    String prefix;
    DOM.XmlNode xmlElement;
    String sValue;
    String sFieldName;
    String UUID;
    String sAttachURL;
    
    public MaximoSOAPXMLHelper(){

        Maximo_Integration_Variables__c objCusSetMax = Maximo_Integration_Variables__c.getInstance('createQuoteNS');
        nameSpace = objCusSetMax.Value__c;       
       	
        objCusSetMax = Maximo_Integration_Variables__c.getInstance('createQuotePrefix');
        prefix = objCusSetMax.Value__c;
        
        objCusSetMax = Maximo_Integration_Variables__c.getInstance('createQuoteAttachmentURL');
        sAttachURL = objCusSetMax.Value__c;
    }         
    
    public string generateXMLRequest(String sOpportunityId){
        //UUID Generation
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        UUID = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        
        Dom.Document doc = new Dom.Document();
        String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
        String xsi = 'http://www.w3.org/2001/XMLSchema-instance';
        String sMethod = 'createMACRequest';
        
        /*
// Creating each element without automation
body.addChildElement('CreateNBNINTMACREQUEST', nameSpace, prefix).
addChildElement('NBNINTMACREQUESTSet', nameSpace, prefix);

Dom.XmlNode rootElement = body.addChildElement('NBNINTMACREQUEST', nameSpace, prefix);
rootElement.addChildElement('METHOD',nameSpace, prefix).
addTextNode('POST');
rootElement.addChildElement('CORRELATIONID', nameSpace, prefix).
addTextNode('AAA-XXX-YYYY');
*/
        
        // In order to fetch the value from the Opportunity and child record, fetch the fields that needs to be queried from the custom settings
        Map<String, String> mapReqdFields = fetchRequiredFields();
        
        List<String> lstAccAddrFields = new List<String>();
        List<String> lstConFields = new List<String>();
        List<String> lstOptyFields = new List<String>();
        List<String> lstAttachFields = new List<String>();
        
        for(String sfieldNameKey: mapReqdFields.keySet()){
            //Object name is in the Values, so checking the object name for each key
            if(mapReqdFields.get(sfieldNameKey) == 'Site__r'){
                //lstAccAddrFields.add('Site__r.' + sfieldNameKey);  
                lstAccAddrFields.add(mapReqdFields.get(sfieldNameKey) + '.' + sfieldNameKey);               
            }else if(mapReqdFields.get(sfieldNameKey) == 'Contact'){
                lstConFields.add(mapReqdFields.get(sfieldNameKey) + '.' + sfieldNameKey);
            }else if(mapReqdFields.get(sfieldNameKey) == 'Opportunity'){
                lstOptyFields.add(sfieldNameKey);
            }else if(mapReqdFields.get(sfieldNameKey) == 'Attachment'){
                lstAttachFields.add(mapReqdFields.get(sfieldNameKey) + '.' + sfieldNameKey);
            }
        }
        
        String sAccountAddrFields = String.join(lstAccAddrFields, ',');
        String sConFields = String.join(lstConFields, ',');
        String sOptyFields = String.join(lstOptyFields, ',');
        String sAttachmentFields = String.join(lstAttachFields, ',');
                
        //'SELECT ' + sOptyFields + ', ' + sAccountAddrFields + ', (SELECT ID'  + sConFields + ' FROM OpportunityContactRoles where IsPrimary=TRUE), (SELECT '+ sAttachmentFields + ' FROM Attachments) FROM Opportunity WHERE ID =:sOpportunityId';
        String queryString = 'SELECT ' + sOptyFields + ', ' + sAccountAddrFields + ', (SELECT '+ sAttachmentFields + ' FROM Attachments WHERE ContentType = \'application/x-zip-compressed\' AND Name LIKE \'%NBN-%\') FROM Opportunity WHERE ID =:sOpportunityId';
        //System.debug('@@ queryString is: ' + queryString);        
        
        //Fetch the field values needed later
        List<Opportunity> lstCurrentOpty = Database.query(queryString);        
        //System.debug('@@ lstCurrentOpty[0] : ' + lstCurrentOpty[0]);
        
        
        //Fetch the whole structure of Custom Settings
        List<Maximo_Service_Fields__c> lstServiceFieldsRecs = [SELECT Name, Request_Name__c, Sequence__c, Namespace_Prefix__c, Parent_Node__c, Type__c, XmlLabel__c, Source_Object__c, Field_Name__c, Default_Value__c, Active__c, MultipleOccurence__c, Attribute__c, Attribute_Value__c
                                                               FROM Maximo_Service_Fields__c 
                                                               WHERE Request_Name__c = :sMethod
                                                               AND Active__c = True
                                                               ORDER BY Sequence__c];
        
        //System.Debug('@@ lstServiceFieldsRecs: ' + lstServiceFieldsRecs.size() + ' ' + lstServiceFieldsRecs);
        
        //Constructing the xml
        
        Dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');
        envelope.setNamespace('xsi', xsi);
        envelope.setNamespace(prefix, nameSpace);
        envelope.setAttributeNs('schemaLocation', soapNS, xsi, null);
        
        Dom.XmlNode body = envelope.addChildElement('Body', soapNS, 'soapenv');
        
        // Start Constructing the body       
        //Root element
        Dom.XmlNode root = body.addChildElement('CreateNBNINTMACREQUEST', nameSpace, prefix);
        appendSObjectToXML(root, lstCurrentOpty[0], lstServiceFieldsRecs);
        
        System.debug('@@ xmlbody: ' + doc.toXmlString());
        return doc.toXmlString();
    }
    
    private Map<String, String> fetchRequiredFields(){       
        Map<String, String> objectFields = new Map<String, String>();
        for(Maximo_Service_Fields__c field: [SELECT Name, Field_Name__c, Source_Object__c FROM Maximo_Service_Fields__c WHERE Type__c = 'Field' AND Active__c = True]){
            //Here the 'key' is the fieldname to create uniqueness and the 2nd part of the map can contain the object name which occurs on more than 1 record.
            objectFields.put(field.Field_Name__c, field.Source_Object__c);
        }        
        return objectFields;
    }
    
    private void appendSObjectToXML(Dom.XmlNode rootNode, Opportunity eachOpty, List<Maximo_Service_Fields__c> lstServiceFieldsRecs){
        
        Map<String, Dom.XmlNode> xmlNodeMap = new Map<String, Dom.XmlNode>();
        List<Maximo_Service_Fields__c> lstSingOccurRecs = new List<Maximo_Service_Fields__c>();
        List<Maximo_Service_Fields__c> lstMultiOccurRecs = new List<Maximo_Service_Fields__c>();
        
        //Split the single occurence and multiple child occurence records
        for(Maximo_Service_Fields__c objSrvField:lstServiceFieldsRecs){
            if(objSrvField.MultipleOccurence__c == false){
                lstSingOccurRecs.add(objSrvField);
            }else{
                lstMultiOccurRecs.add(objSrvField);
            }        	
        }   
        
        for(Maximo_Service_Fields__c objFieldRec:lstSingOccurRecs){
            if(objFieldRec.Type__c == 'XMLNode'){
                if(objFieldRec.Parent_Node__c == 'root'){    
                    xmlNodeMap.put(objFieldRec.XmlLabel__c, rootNode);
                }else{
                    xmlNodeMap.put(objFieldRec.XmlLabel__c, xmlNodeMap.get(objFieldRec.Parent_Node__c).addChildElement(objFieldRec.XmlLabel__c, nameSpace,objFieldRec.Namespace_Prefix__c));
                }
            }else if(objFieldRec.Type__c == 'StaticElement'){
                
                xmlElement = xmlNodeMap.get(objFieldRec.Parent_Node__c).addChildElement(objFieldRec.XmlLabel__c, nameSpace, objFieldRec.Namespace_Prefix__c);
             
                //For the CorrelationId element, overwrite the default with the below generated UUID
                if(objFieldRec.XmlLabel__c == 'CORRELATIONID'){
                    xmlElement.addTextNode(UUID);
                }else{
                    //Default path for all the other static elements
                    xmlElement.addTextNode(objFieldRec.Default_Value__c);
                }
                
            }else if(objFieldRec.Type__c == 'Field'){                
                //Get the required field
                sValue = '';
                sFieldName = objFieldRec.Field_Name__c;
                
                if(objFieldRec.Source_Object__c == 'Site__r'){
                    SObject sObjSite = eachOpty.getSObject('Site__r');
                    sValue = String.valueOf(sObjSite.get(sFieldName));
                    
                }else{ //For Opportunity fields
                    if(sFieldName == 'Requested_Completion_Date__c' ){
                        Datetime dt = (date)eachOpty.get(sFieldName);
                        sValue = dt.format('yyyy-MM-dd');
                    }else{                       
                        sValue = String.valueOf(eachOpty.get(sFieldName));
                    }
                }
                
                //Setting the value is a common code for all object field types
                if(sValue != null){
                    xmlElement = xmlNodeMap.get(objFieldRec.Parent_Node__c).addChildElement(objFieldRec.XmlLabel__c, nameSpace, objFieldRec.Namespace_Prefix__c);
                    xmlElement.addTextNode(sValue);
                }
            }
            
            //If the Object Field element or a Static Element or a node has attributes defined, set them in the common code below
            if(objFieldRec.Attribute__c != null && objFieldRec.Attribute_Value__c != null){
                xmlElement.setAttribute(objFieldRec.Attribute__c, objFieldRec.Attribute_Value__c);
            }
        }
        
        //Multiple occurences node and child element creation will be determined by their occurence in the 'eachOpty' Opportunity instance variable. 
        
        if(!lstMultiOccurRecs.isEmpty()){          
            //Add Attachments
            for(Attachment sObjOptyAtt : eachOpty.Attachments){
                //Call MultipleObjectXML construction function
                appendMultisObjectsToXML(xmlNodeMap, sObjOptyAtt, lstMultiOccurRecs, 'Attachment');
            }
            
            //Add Contacts
                                
           /* for(OpportunityContactRole optyConRole : eachOpty.OpportunityContactRoles){                
                SObject sObjCont = optyConRole.getSObject('Contact');
                //Call MultipleObjectXML construction function
                appendMultisObjectsToXML(xmlNodeMap, sObjCont, lstMultiOccurRecs, 'Contact');
            }*/
        }
    }
    
    private void appendMultisObjectsToXML(Map<String, Dom.XmlNode> xmlNodeMap, SObject objChildObject, List<Maximo_Service_Fields__c> lstMultiOccurRecs, String sSourceObjName){
        
        for(Maximo_Service_Fields__c objFieldRec:lstMultiOccurRecs){
            
            if(objFieldRec.Type__c == 'XMLNode' && objFieldRec.Source_Object__c == sSourceObjName){
                xmlNodeMap.put(objFieldRec.XmlLabel__c, xmlNodeMap.get(objFieldRec.Parent_Node__c).addChildElement(objFieldRec.XmlLabel__c, nameSpace,objFieldRec.Namespace_Prefix__c));
                
            }else if(objFieldRec.Type__c == 'StaticElement' && objFieldRec.Source_Object__c == sSourceObjName){
                xmlElement = xmlNodeMap.get(objFieldRec.Parent_Node__c).addChildElement(objFieldRec.XmlLabel__c, nameSpace, objFieldRec.Namespace_Prefix__c);
                xmlElement.addTextNode(objFieldRec.Default_Value__c);
                
            }else if(objFieldRec.Type__c == 'Field' && objFieldRec.Source_Object__c == sSourceObjName){             
                //Get the required field
                sValue = '';
                sFieldName = objFieldRec.Field_Name__c;
                sValue = String.valueOf(objChildObject.get(sFieldName));
                
                if(sValue != null && objFieldRec.Field_Name__c == 'Id' && objFieldRec.Source_Object__c == 'Attachment'){
                    sValue = sAttachURL + sValue + '/Body';
                }
                
                //Setting the value is a common code for all object field types
                if(sValue != null){
                    xmlElement = xmlNodeMap.get(objFieldRec.Parent_Node__c).addChildElement(objFieldRec.XmlLabel__c, nameSpace, objFieldRec.Namespace_Prefix__c);
                    xmlElement.addTextNode(sValue);
                }
            }
            
            //If the Object Field element or a Static Element or a node has attributes defined, set them in the common code below
            if(objFieldRec.Attribute__c != null && objFieldRec.Attribute_Value__c != null && objFieldRec.Source_Object__c == sSourceObjName){
                xmlElement.setAttribute(objFieldRec.Attribute__c, objFieldRec.Attribute_Value__c);
            }          
        }
    }    
}