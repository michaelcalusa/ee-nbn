/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class will be used to transform and transfer MAR records from MAR_Int to MAR table
Modifications:
*/
public class MedicalAlarmRegisterTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
    public String marIntQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {            
    }
    
     public MedicalAlarmRegisterTransformation(String ejId)
    {
         exJobId = ejId;
         Batch_Job__c bj = new Batch_Job__c();
         bj.Name = 'MARTransform'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        marIntQuery = 'select Id,Registration_ID__c, Contact_Id__c, Registration_Type__c,How_did_you_hear__c,Provided_Date__c,Data_Source__c,Monitored__c,Unmonitored__c,Locally_Monitored__c,Unknown__c,Address_1__c,Address_2__c,Suburb__c,State__c,Post_Code__c,Location_ID__c,Name_of_Retirement_Village__c,Contact_Role__c,Number_of_Units_in_Retirement_Village__c,Number_of_Units_with_Medical_Alarm__c,Description__c,CRMOD_Owner__c from MAR_Int__c where Transformation_Status__c = \'Extracted from CRM\' order by CreatedDate'; 
        return Database.getQueryLocator(marIntQuery);            
    }
    public void execute(Database.BatchableContext BC,List<MAR_Int__c> listOfMARInt)
    {        
        try
        {
            Map<String, Id> mapListOfCon = new Map<String, Id>(); 
            List<String> listOfConIds = new List<String>();
            List<String> listOfConToUpdMARFlag = new List<String>();
            for(MAR_Int__c mari : listOfMARInt)
            {                                                 
                if(!String.isBlank(mari.Contact_Id__c) && mari.Contact_Id__c != 'No Match Row Id')
                {                	                     
                    listOfConIds.add(mari.Contact_Id__c); 
                }                
            } 
            for(Contact con : [select On_Demand_Id_P2P__c, Id from Contact where On_Demand_Id_P2P__c in :listOfConIds FOR UPDATE])
            {
                mapListOfCon.put(con.On_Demand_Id_P2P__c, con.id);            
            }
           
            List<MAR__c> listOfMAR = new List<Mar__c>(); 
            map <String, MAR__c> marMap = new map<String, MAR__c>();
            for(MAR_Int__c marInt : listOfMARInt)
            {
                MAR__c mar = new MAR__c();
                mar.Name = marInt.Registration_ID__c;
                mar.On_Demand_Id__c = marInt.Registration_ID__c;
                if(!String.isBlank(marInt.Contact_Id__c))
                {                    
                    //Contact c = new Contact(On_Demand_Id_P2P__c = marInt.Contact_Id__c);                        
                    //mar.Contact__r =c;                    
                    mar.Contact__c = mapListOfCon.get(marInt.Contact_Id__c);
                    //listOfConToUpdMARFlag.add(mapListOfCon.get(marInt.Contact_Id__c));
                }
                mar.Registration_Type__c = marInt.Registration_Type__c;
                mar.How_did_you_hear__c = marInt.How_did_you_hear__c;
                String providedDate = marInt.Provided_Date__c;                
                if(!String.isBlank(providedDate))
                {
                    //String providedDate2 = providedDate.substringBefore(' ');
                    //mar.Provided_Date__c = date.parse(providedDate2.replace('-','/'));     
                    mar.Provided_Date__c = StringToDate.strDate(providedDate);
                }                
                mar.Data_Source__c = marInt.Data_Source__c;
                mar.Monitored__c = marInt.Monitored__c;
                mar.Unmonitored__c = marInt.Unmonitored__c;
                mar.Locally_Monitored__c = marInt.Locally_Monitored__c;
                mar.Unknown__c = marInt.Unknown__c;
                mar.Address_1__c = marInt.Address_1__c;
                mar.Address_2__c = marInt.Address_2__c;
                mar.Suburb__c = marInt.Suburb__c;
                mar.State__c = marInt.State__c;
                mar.Post_Code__c = marInt.Post_Code__c;
                mar.Location_Id__c = marInt.Location_ID__c;
                mar.Name_of_Retirement_Village__c = marInt.Name_of_Retirement_Village__c;
                if(!String.isBlank(marInt.Contact_Role__c) && marInt.Contact_Role__c == '<No Values>')
                {
                    mar.Contact_Role__c = '';
                }
                else
                {
                    mar.Contact_Role__c = marInt.Contact_Role__c;                    
                }

                mar.Number_of_Units_in_Retirement_Village__c = marInt.Number_of_Units_in_Retirement_Village__c;
                mar.Number_of_Units_with_Medical_Alarm__c = marInt.Number_of_Units_with_Medical_Alarm__c;
                mar.CRMOD_Owner__c = marInt.CRMOD_Owner__c;
                mar.Ownership__c = 'CRMOD';
                mar.Description__c = marInt.Description__c;
                //listOfMAR.add(mar); 
                marMap.put(marInt.Registration_ID__c,mar);
                
                
            }
            listOfMAR = marMap.values();
            List<Database.UpsertResult> upsertMARResults = Database.Upsert(listOfMAR,MAR__c.Fields.Name,false);
            List<Database.Error> upsertMARError;
            String errorMessage, fieldsAffected;
            String[] listOfAffectedFields;
            StatusCode sCode;
            List<Error_Logging__c> errMARList = new List<Error_Logging__c>();
            List<MAR_Int__c> updMARIntRes = new List<MAR_Int__c>();
            for(Integer j=0; j<upsertMARResults.size();j++)
            {
                MAR_Int__c updMARInt = new MAR_Int__c();
                if(!upsertMARResults[j].isSuccess())
                {
                    upsertMARError = upsertMARResults[j].getErrors();
                    for(Database.Error er: upsertMARError)
                    {
                        sCode = er.getStatusCode();
                        errorMessage = er.getMessage();
                        listOfAffectedFields = er.getFields();
                    }
                    fieldsAffected = String.join(listOfAffectedFields,',');
                    errMARList.add(new Error_Logging__c(Name='MAR',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isCreated__c=upsertMARResults[j].isCreated(),isSuccess__c=upsertMARResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfMAR[j].Name,Schedule_Job__c=bjId));
                    listOfMARInt[j].Transformation_Status__c = 'Error';
                    listOfMARInt[j].Registration_ID__c = listOfMAR[j].Name;
                    listOfMARInt[j].Schedule_Job_Transformation__c = bjId;
                }
                else
                {
                    if(String.isNotBlank(mapListOfCon.get(listOfMARInt[j].Contact_Id__c)))
                    {                        
                        listOfMARInt[j].Transformation_Status__c = 'Ready for Contact Update';
                    }
                    else
                    {  
                        if(String.isBlank(listOfMARInt[j].Contact_Id__c) || listOfMARInt[j].Contact_Id__c == 'No Match Row Id')
                        {                            
                            listOfMARInt[j].Transformation_Status__c = 'Copied to Base Table';
                        }
                        else
                        {
                            listOfMARInt[j].Transformation_Status__c = 'Association Error';
                            errMARList.add(new Error_Logging__c(Name='MAR Tranformation',Error_Message__c='Contact Record is not associated',Fields_Afftected__c='Contact',isCreated__c=upsertMARResults[j].isCreated(),
                            isSuccess__c=upsertMARResults[j].isSuccess(),Record_Row_Id__c=upsertMARResults[j].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfMARInt[j].Id,Schedule_Job__c=bjId));
                            
                        }                        
                    }       
                    
                   // updMARInt.Registration_ID__c = listOfMAR[j].Name;
                    listOfMARInt[j].Schedule_Job_Transformation__c = bjId;
                   // updMARIntRes.add(updMARInt);                    
                }
            }
            system.debug('errMARList==>'+errMARList);
            Insert errMARList;
            Update listOfMARInt;   
            if(errMARList.isEmpty())      
            {
                status = 'Completed';
            } 
            else
            {
                status = 'Error';
            }  
            recordCount = recordCount+listOfMARInt.size();             
        }
        catch(Exception e)
        {            
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='MAR Transformation',Error_Message__c= e.getMessage()+' Line Number: '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob marJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
        {
            bj.Status__c= marJob.Status;
        }
        else
        {
             bj.Status__c=status;
        }
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = marJob.id;
        bj.End_Time__c  = marJob.CompletedDate;
        upsert bj;
        if (CRM_Transformation_Job__c.getinstance('UpdateContactMARFlag') <> null && CRM_Transformation_Job__c.getinstance('UpdateContactMARFlag').on_off__c)
   	    {
        	Database.executeBatch(new UpdateContactMARFlag(exJobId));
        }
        if (CRM_Transformation_Job__c.getinstance('MARContactTransformation') <> null && CRM_Transformation_Job__c.getinstance('MARContactTransformation').on_off__c)
   	    {
        	Database.executeBatch(new MARContactTransformation(exJobId));
        }
    }     
}