/**
 * This class contains unit tests for validating the behavior of Apex classes
 * Author: Suman gunaganti
 */
@isTest
Public class MockHttpResponseGenerator_Test implements HttpCalloutMock{
		
    	private Integer code; // setStatusCode Example : 200
        private String bodyAsString; // JSON Response
        private Map<String, String> responseHeaders; // header. Example : 'Content-Type', 'application/JSON'
        public MockHttpResponseGenerator_Test(Integer code, String body, Map<String, String> responseHeaders) {
            this.code = code;
            this.bodyAsString = body;
            this.responseHeaders = responseHeaders;
        }
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(code);
            resp.setBody(bodyAsString);
            if(responseHeaders != null) {
                for (String key : responseHeaders.keySet()) {
                    resp.setHeader(key, responseHeaders.get(key));
                }
            }
            return resp;
        }
}