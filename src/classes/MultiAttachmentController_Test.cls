@isTest(seeAllData = false)
public class MultiAttachmentController_Test {
    static testMethod void TestMethod_createOppAndAttach() {
        Opportunity opp=new Opportunity(Name='Unit Test', StageName='New',closeDate=Date.ValueOf(System.Datetime.now()));
        insert opp;
        ApexPages.currentPage().getParameters().put('id', opp.id);
        MultiAttachmentController controller=new MultiAttachmentController();
        //controller.sobjId=opp.id;     
        System.assertEquals(0, controller.getAttachments().size());     
        System.assertEquals(1, controller.newAttachments.size());
        controller.addMore();
        // populate the first and third new attachments
        List<Attachment> newAtts=controller.newAttachments;
        newAtts[0].Name='Unit Test 1';
        newAtts[0].Description='Unit Test 1';
        newAtts[0].Body=Blob.valueOf('Unit Test 1');

        newAtts[2].Name='Unit Test 2';
        newAtts[2].Description='Unit Test 2';
        newAtts[2].Body=Blob.valueOf('Unit Test 2');
        
        controller.save();
        
        System.assertEquals(2, controller.getAttachments().size());
        System.assertNotEquals(null, controller.done());
    }

}