//Apex controller for Lightning Component PaymentComponentBPoint
public class NBNBPointPaymentController {
	
	public static Map<String, BPoint_Static_Parameters__mdt> mapBpointParams;
	public static string strBase64Authorization;


	//Default controller
	public NBNBPointPaymentController() {

		//Retrieve records from Metadata type BPoint_Static_Parameters__mdt and Create Bpoint Authorization Key
		loadBpointParams();
		
	}

	//Retrieve records from Metadata type BPoint_Static_Parameters__mdt and Create Bpoint Authorization Key
	public static void loadBpointParams()
	{
		//Retrieve records from Metadata type BPoint_Static_Parameters__mdt
    	List<BPoint_Static_Parameters__mdt> bpointParams = [Select DeveloperName, MasterLabel, Parameter_Type__c, Parameter_Value__c,Parameter_Value_Long__c
    														From BPoint_Static_Parameters__mdt];

    	if(bpointParams == null || (bpointParams !=null && bpointParams.size() == 0))
    	{
    		//return 'Error retrieving values from BPoint_Static_Parameters__mdt object';
    		system.debug('Error retrieving values from BPoint_Static_Parameters__mdt object');
    		throw new AuraHandledException('Error retrieving values from BPoint_Static_Parameters__mdt object');
    	}

    	mapBpointParams = new Map<String, BPoint_Static_Parameters__mdt>();
    	for(BPoint_Static_Parameters__mdt param : bpointParams)
    	{
    		mapBpointParams.put(param.DeveloperName,param);
    	}	


    	//Create Base 64 encoded Authorization
    	String strAuthorization = '';
    	strAuthorization = mapBpointParams.get('Username').Parameter_Value__c + '|' +
    						mapBpointParams.get('Merchant_Number').Parameter_Value__c + ':' +
    						mapBpointParams.get('Password').Parameter_Value__c;
    	Blob blobAuthorization = Blob.valueOf(strAuthorization);

    	strBase64Authorization = EncodingUtil.base64Encode(blobAuthorization);
    	//If bpoint api user credentials are not provided in metadata types, use default authorization value.
    	//Note: Default authorization will not work in Prod, api user credentials must be provided in metadata types
    	//Note: Default authorization will not work in Sandboxes if the associated credentials are changed. 
    	if(mapBpointParams.get('Username').Parameter_Value__c == 'TBD' || String.isEmpty(mapBpointParams.get('Username').Parameter_Value__c))
    		strBase64Authorization = 'YXBpdXNlcjF8REVNT05TVFJBVElPMTExODpBMy8zTitnajho';
    	System.debug('Base 64 encoded Authorization value is - ' + strBase64Authorization);
	}

	//This method is called from the associated Lightning component 
	//Calls analyseInput and getBpointAuthKey methods
	//Code related to techChoice starts here
   /* @AuraEnabled
    public static String getAuthKey(String PriceItemName,String ApplicationID) {

    	system.debug('ApplicationID is -' + ApplicationID);
    	system.debug('PriceItemName is -' + PriceItemName);
    	//Retrieve the amount associated with prodct type
    	Decimal amount = analyseInput(PriceItemName);

    	if(amount == null)
    	{
    		//return 'Error getting authkey';
    		system.debug('Error retrieving amount from price item object for Prodcut Type - ' + PriceItemName);
    		
    		BpointPaymentUtils.BpointInteractionLog logBpointInteraction = new BpointPaymentUtils.BpointInteractionLog('LoadPaymentComponent',ApplicationID,'FAILED',String.valueOf(Datetime.now()),true);

    		//throw new AuraHandledException('Error getting authkey in retrieving values from BPoint_Static_Parameters__mdt object');
    		return 'Error retrieving amount from price item object for Product Type - ' + PriceItemName;
    	}


        return JSON.serialize(getBpointAuthKey(amount,ApplicationID));
    }

    //Checks for not null of prodCode and calls method getTechChoiceApplicationFee to get the amount
    public static Decimal analyseInput(string prodCode)
    {
    	//Decimal amount=getTechChoiceApplicationFee();
    	Decimal amount=null;
    	if(String.isNotEmpty(prodCode))
    	{
    		amount = getTechChoiceApplicationFee(prodCode);
    	}

    	//return null;
    	return amount;
    }

    //Query Price Item object where Name matches with prodCode and return One Off Cost
    public static Decimal getTechChoiceApplicationFee(string prodCode)
    {
    	List<cspmb__Price_Item__c> priceItemRecords = [SELECT Name, cspmb__One_Off_Cost__c, cspmb__Price_Item_Code__c,RecordType.Name FROM cspmb__Price_Item__c 
    								WHERE cspmb__Is_Active__c = true and Name =:prodCode 
    								and RecordType.Name = 'Tech Choice - IPS'];

    	if(priceItemRecords.size() > 0 && priceItemRecords[0].cspmb__One_Off_Cost__c != null)
    	return priceItemRecords[0].cspmb__One_Off_Cost__c;
    	else return null;
    	//else throw new AuraHandledException('Error in getAuthkey while finding Price priceItemRecords');

    }*/// Code related to techChoice Ends here

    //http post request to get Bpoint Auth Key
    public static BpointPaymentUtils.ResponseToUi getBpointAuthKey(Decimal amount,String ApplicationID)
    {
    	String strReturnValue = '';
    	BpointPaymentUtils.BpointInteractionLog logBpointInteraction;
    	BpointPaymentUtils.ResponseToUi responseToUi;
    	BpointPaymentUtils.CustomSuccessResponse sucessResponse = null;
	    BpointPaymentUtils.CustomErrorResponse errorResponse = null;
    	try
    	{


	    	//Load Bpoint Parameters from Metadata type if not loaded
	    	if(mapBpointParams == null)loadBpointParams();

	        Http http = new Http();
	        HttpRequest request = new HttpRequest();
	        request.setEndpoint(mapBpointParams.get('Create_Auth_Key_URL').Parameter_Value__c);
	        request.setMethod('POST');
	        //request.setHeader('Authorization', 'YXBpdXNlcjF8REVNT05TVFJBVElPMTExODpBMy8zTitnajho');
	        request.setHeader('Authorization', strBase64Authorization);
	        request.setHeader('Content-Type', mapBpointParams.get('Content_Type').Parameter_Value__c);
	                
	        BpointPaymentUtils.IframeParameters iframePar = new BpointPaymentUtils.IframeParameters();
	        iframePar.CSS = mapBpointParams.get('IFrameParameters_CSS').Parameter_Value_Long__c;
	       // iframePar.CSS = '.control-label{font-size: 40px;}';
	        //iframePar.CSS = '.form-control{border: 2px solid red}';
	        iframePar.ShowSubmitButton = Boolean.valueOf(mapBpointParams.get('IFrameParameters_ShowSubmitButton').Parameter_Value__c);
	        
	        BpointPaymentUtils.ProcessTxnData procTxnData = new BpointPaymentUtils.ProcessTxnData();
	        procTxnData.Action = mapBpointParams.get('ProcessTxnData_Action').Parameter_Value__c;
	        procTxnData.TokenisationMode = Integer.valueOf(mapBpointParams.get('ProcessTxnData_TokenisationMode').Parameter_Value__c);
	        procTxnData.TestMode = Boolean.valueOf(mapBpointParams.get('ProcessTxnData_TestMode').Parameter_Value__c);
	        procTxnData.SubType = mapBpointParams.get('ProcessTxnData_SubType').Parameter_Value__c;
	        procTxnData.Type_z = mapBpointParams.get('ProcessTxnData_Type').Parameter_Value__c;
	        procTxnData.BillerCode = mapBpointParams.get('ProcessTxnData_BillerCode').Parameter_Value__c;
            
	        procTxnData.Amount = amount * 100; //Karan: Put multiplier of 100.
	        
	        procTxnData.Crn1 = ApplicationID;
	        procTxnData.Currency_z = mapBpointParams.get('ProcessTxnData_Currency').Parameter_Value__c;
	     	//procTxnData.MerchantReference = 'Test MerchantReference';
	            
	        BpointPaymentUtils.BpointAuthKeyRequest bAuthKeyRequest = new BpointPaymentUtils.BpointAuthKeyRequest();
	        bAuthKeyRequest.ProcessTxnData = procTxnData;
	        //bAuthKeyRequest.RedirectionUrl = 'https://nbn--cpqdev.cs58.my.salesforce.com/apex/RetrieveTransactionResult';
	        bAuthKeyRequest.RedirectionUrl = URL.getSalesforceBaseUrl().toExternalForm(); //'https://nbn--cpqdev.cs58.my.salesforce.com/';//apex/PaymentVFPage';
	        bAuthKeyRequest.IframeParameters = iframePar;
	        
	        request.setBody(JSON.serialize(bAuthKeyRequest).replace('"Type_z"','"Type"').replace('"Currency_z"','"Currency"'));
	        system.debug('Body: ' + request.getBody());
	        HttpResponse response = http.send(request);
	        system.debug('response authkey ' + response);
	        BpointPaymentUtils.BpointAuthKeyResponse BpObject;
	        //BpointPaymentUtils.CustomSuccessResponse sucessResponse = null;
	        //BpointPaymentUtils.CustomErrorResponse errorResponse = null;

	       
	        /*if (response.getStatusCode() == 200 && string.isNotBlank(response.getBody()) && response.getBody()!='{}') {
	            BpObject = (BpointPaymentUtils.BpointAuthKeyResponse)JSON.deserialize(response.getBody(), BpointPaymentUtils.BpointAuthKeyResponse.Class);
	            strReturnValue = mapBpointParams.get('BPoint_Iframe_URL').Parameter_Value__c + BpObject.AuthKey;
	            sucessResponse = new BpointPaymentUtils.CustomSuccessResponse('renderbpointiframe', 'sucess',strReturnValue,'',amount);
	        }
	        else
	        {
	        	//error logic here
	        	strReturnValue = 'Error getting Authkey from BPoint Status -' + response.getStatus()  + ' StatusCode -' +
	        						response.getStatusCode();
	        	errorResponse = new BpointPaymentUtils.CustomErrorResponse('renderbpointiframe','error',response.getStatusCode(),response.getStatus(), strReturnValue);
	        }*/

	        //http status code 200 indicates a valid response retured by Bpoint
	        if (response.getStatusCode() == 200 && string.isNotBlank(response.getBody()) && response.getBody()!='{}'){
	        	BpObject = (BpointPaymentUtils.BpointAuthKeyResponse)JSON.deserialize(response.getBody(), BpointPaymentUtils.BpointAuthKeyResponse.Class);
	        }
	        else{
	        	strReturnValue = 'Error getting Authkey from BPoint Status -' + response.getStatus()  + ' StatusCode -' +
	        						response.getStatusCode();
	        	errorResponse = new BpointPaymentUtils.CustomErrorResponse('renderbpointiframe','error',String.valueOf(response.getStatusCode()),response.getStatus(), strReturnValue);	        	
	        }

	        //APIResponse.ResponseCode == 0 inidates a sucessfull scenario which returns Bpoint Iframe URL and Auth key
	        if(BpObject != null && BpObject.APIResponse.ResponseCode == 0)
	        {
	            strReturnValue = mapBpointParams.get('BPoint_Iframe_URL').Parameter_Value__c + BpObject.AuthKey;
	            sucessResponse = new BpointPaymentUtils.CustomSuccessResponse('renderbpointiframe', 'success',strReturnValue,BpObject.AuthKey,'',amount);	        	
	        }
	        else{
	        	strReturnValue = 'Error getting Authkey from BPoint Status -' + BpObject.APIResponse.ResponseCode  + ' StatusCode -' +
	        						String.valueOf(BpObject.APIResponse.ResponseCode);
	        	errorResponse = new BpointPaymentUtils.CustomErrorResponse('renderbpointiframe','error',String.valueOf(BpObject.APIResponse.ResponseCode),BpObject.APIResponse.ResponseText, strReturnValue,'Bpoint Render Error');	

	        }



	        System.debug('getBpointAuthKey outpout is : ' + strReturnValue);
	        if(sucessResponse == null){
	        	responseToUi = new BpointPaymentUtils.ResponseToUi(sucessResponse, errorResponse, true);
	        	if(BpObject != null){
	        	logBpointInteraction = new BpointPaymentUtils.BpointInteractionLog('LoadPaymentComponent',ApplicationID,'OK',String.valueOf(Datetime.now()),false);
	        	logBpointInteraction.setAuthKeyTransactionDetails(String.valueOf(amount),String.valueOf(BpObject.APIResponse.ResponseCode),BpObject.APIResponse.ResponseText,true);	        		
	        	}
	        	else{
	        		logBpointInteraction = new BpointPaymentUtils.BpointInteractionLog('LoadPaymentComponent',ApplicationID,'FAILED',String.valueOf(Datetime.now()),TRUE);
	        	}

	        }
	        else{
	        	responseToUi = new BpointPaymentUtils.ResponseToUi(sucessResponse, errorResponse, false);
	        	logBpointInteraction = new BpointPaymentUtils.BpointInteractionLog('LoadPaymentComponent',ApplicationID,'OK',String.valueOf(Datetime.now()),false);
	        	logBpointInteraction.setAuthKeyTransactionDetails(String.valueOf(amount),String.valueOf(BpObject.APIResponse.ResponseCode),BpObject.APIResponse.ResponseText,true);
	        }

        
        }catch(System.CalloutException ex)
        {
        	System.debug('Error in Getting Authkey');
        	logBpointInteraction = new BpointPaymentUtils.BpointInteractionLog('LoadPaymentComponent',ApplicationID,'FAILED',String.valueOf(Datetime.now()),true);
    		//BpointPaymentUtils.logBpointError('LoadPaymentComponent',ApplicationID);
    		//throw new AuraHandledException('Error in Getting Authkey');
        }
        catch(Exception ex)
    	{
    		logBpointInteraction = new BpointPaymentUtils.BpointInteractionLog('LoadPaymentComponent',ApplicationID,'FAILED',String.valueOf(Datetime.now()),true);
    		//BpointPaymentUtils.logBpointError('LoadPaymentComponent',ApplicationID);
    		//throw new AuraHandledException('Error in Getting Authkey');
    	}
    	
    	//If Salesforce error or Http exception or any handled exception
    	if(responseToUi == null){
    		strReturnValue = 'System Error - Please try again later';
    		errorResponse = new BpointPaymentUtils.CustomErrorResponse('renderbpointiframe','error',null,null, strReturnValue,'Bpoint Render Error');
    		responseToUi = new BpointPaymentUtils.ResponseToUi(sucessResponse, errorResponse, true);
    	}
    	return responseToUi;

    }



    //This methods is called from the Lighning component when User clicks on Process Payment button
    //Http post method to get result key of payment, and calls getTransactionResult method to get more details
     @AuraEnabled
    public static string submitPayment(String authKey,String ApplicationID)
    {
    	String strReturnValue;
    	BpointPaymentUtils.ResponseToUi responseToUi;
    	BpointPaymentUtils.CustomSuccessResponse sucessResponse = null;
	    BpointPaymentUtils.CustomErrorResponse errorResponse = null;
	    BpointPaymentUtils.BpointInteractionLog logBpointInteraction = new BpointPaymentUtils.BpointInteractionLog('ProcessPayment',ApplicationID,'OK',String.valueOf(Datetime.now()),false);
        try
        {

        	System.debug('Application Id is - ' + ApplicationID);
        	//BpointPaymentUtils.BpointInteractionLog logBpointInteraction = new BpointPaymentUtils.BpointInteractionLog('ProcessPayment',ApplicationID,'OK',String.valueOf(Datetime.now()),false);

	        //Load Bpoint Parameters from Metadata type if not loaded
	    	if(mapBpointParams == null)loadBpointParams();

	    	String sBody = '{"TxnReq" : {"StoreCard" : false}}';
		    Http http = new Http();

		    HttpRequest request = new HttpRequest();
		    request.setEndpoint(mapBpointParams.get('Process_Iframe_Payment_URL').Parameter_Value__c + authKey);
		    request.setMethod('POST');
		    request.setHeader('Authorization', strBase64Authorization);
		    request.setHeader('Content-Type', mapBpointParams.get('Content_Type').Parameter_Value__c);
		    
		    BpointPaymentUtils.TxnReq txnreq = new BpointPaymentUtils.TxnReq();
		    
		    txnreq.StoreCard = Boolean.valueOf(mapBpointParams.get('TxnReq_StoreCard').Parameter_Value__c); 
		    
		    BpointPaymentUtils.BPointProcessPayment bProcessPayment = new BpointPaymentUtils.BPointProcessPayment();
		    bProcessPayment.TxnReq = txnreq;
		    
		    request.setBody(sBody);
		    HttpResponse response = http.send(request);

		    BpointPaymentUtils.BPointResponseIframeTxn BpObject;
		    //BpointPaymentUtils.CustomSuccessResponse sucessResponse = null;
	        //BpointPaymentUtils.CustomErrorResponse errorResponse = null;
		    String resultKey;
		    //String strReturnValue;
	       
	        /*if (response.getStatusCode() == 200 && string.isNotBlank(response.getBody()) && response.getBody()!='{}') {
	            BpObject = (BpointPaymentUtils.BPointResponseIframeTxn)JSON.deserialize(response.getBody(), BpointPaymentUtils.BPointResponseIframeTxn.Class);
	            resultKey = BpObject.ResultKey;
	        }
	        else
	        {
	        	//error logic here
	        	throw new AuraHandledException('Error in getting submitPayment response');	

	        }*/

	        //http status code 200 indicates a valid response retured by Bpoint
	        if (response.getStatusCode() == 200 && string.isNotBlank(response.getBody()) && response.getBody()!='{}') {
	            BpObject = (BpointPaymentUtils.BPointResponseIframeTxn)JSON.deserialize(response.getBody(), BpointPaymentUtils.BPointResponseIframeTxn.Class);
	        }
	        else{
	        	strReturnValue = 'Error in getting submitPayment response -' + response.getStatus()  + ' StatusCode -' +
	        						response.getStatusCode();
	        	errorResponse = new BpointPaymentUtils.CustomErrorResponse('submitpayment','error',String.valueOf(response.getStatusCode()),response.getStatus(), strReturnValue);	        	
	        }

	        if(errorResponse != null)JSON.serialize(errorResponse);

	        //APIResponse.ResponseCode == 0 inidates a sucessfull scenario which returns Bpoint Iframe URL and Auth key
	        if(BpObject != null && BpObject.APIResponse.ResponseCode == 0)
	        {
	            resultKey = BpObject.ResultKey;
	            //???Need to store result key in sucessreponse
	            sucessResponse = new BpointPaymentUtils.CustomSuccessResponse('submitpayment', 'success',strReturnValue,'',0.00);	        	
	        }
	        else {
	        	strReturnValue = 'Error in getting submitPayment response - -' + BpObject.APIResponse.ResponseCode  + ' StatusCode -' +
	        						String.valueOf(BpObject.APIResponse.ResponseCode);
	        	errorResponse = new BpointPaymentUtils.CustomErrorResponse('submitpayment','error',String.valueOf(BpObject.APIResponse.ResponseCode),BpObject.APIResponse.ResponseText, strReturnValue,'API Response Code');	

	        }

	        if(errorResponse != null)JSON.serialize(errorResponse);


	        //Code executes after resultKey is sucessfully returned by BPoint
	        //BpointPaymentUtils.ResponseToUi responseToUi = BpointPaymentUtils.getTransactionResult(resultKey, mapBpointParams, strBase64Authorization,ApplicationID);
	        responseToUi = BpointPaymentUtils.getTransactionResult(resultKey, mapBpointParams, strBase64Authorization,ApplicationID);
		    
		    logBpointInteraction.save();
	

			//storeResponse();
			system.debug(responseToUi); //Karan: Debug
		    return JSON.serialize(responseToUi);
	    }catch(System.CalloutException ex)
        {
        	//System.debug('Error in Getting Authkey');
        	logBpointInteraction = new BpointPaymentUtils.BpointInteractionLog('ProcessPayment',ApplicationID,'FAILED',String.valueOf(Datetime.now()),true);
    		//BpointPaymentUtils.logBpointError('LoadPaymentComponent',ApplicationID);
    		//throw new AuraHandledException('Error in Getting Authkey');
        }
        catch(Exception ex)
    	{
    		logBpointInteraction = new BpointPaymentUtils.BpointInteractionLog('ProcessPayment',ApplicationID,'FAILED',String.valueOf(Datetime.now()),true);
    		//BpointPaymentUtils.logBpointError('LoadPaymentComponent',ApplicationID);
    		//throw new AuraHandledException('Error in Getting Authkey');
    	}
	    //catch(Exception ex)
       	//{
        //	throw new AuraHandledException('Error in submitPayment');	
       	//}

       	strReturnValue = 'System Error - Please try again later';
    	errorResponse = new BpointPaymentUtils.CustomErrorResponse('submitpayment','error',null,null, strReturnValue,'Hard Failure');
    	responseToUi = new BpointPaymentUtils.ResponseToUi(sucessResponse, errorResponse, true);
       	//return null;
       	return JSON.serialize(responseToUi);

    } 
    //Method Called from invoicepaymentcmp.cmp
    @AuraEnabled
    public static Boolean logInvoicepaymenttransaction(string applicationId){
        BpointPaymentUtils.BpointInteractionLog logBpointinteraction = new BpointPaymentUtils.BpointInteractionLog('SendInv',applicationId,'OK',String.valueOf(Datetime.now()),true);
        return ![SELECT ID FROM BPoint_Component_Transaction_Record__c WHERE Application_Id__c =: applicationId LIMIT 1].isEmpty();
    }
    //Method to retrieve total amount
    @AuraEnabled
    public static Decimal getAmountNewDevelopments(string PriceItemName, String PremiseCount, String Context){
        system.debug(PremiseCount);
        List<cspmb__Price_Item__c> priceItemRecords = [SELECT Name, cspmb__One_Off_Cost__c, cspmb__Price_Item_Code__c,RecordType.Name FROM cspmb__Price_Item__c 
    								WHERE cspmb__Is_Active__c = true and Name =:PriceItemName 
    								and RecordType.Name = 'New Developments'];
		system.debug('price item records ' + PremiseCount);
         if(priceItemRecords.size() > 0 && priceItemRecords[0].cspmb__One_Off_Cost__c != null && integer.valueOf(PremiseCount) > 0){
    		return priceItemRecords[0].cspmb__One_Off_Cost__c * integer.valueOf(PremiseCount); 
            }
        return null;
    }
    
    @AuraEnabled
    public static String getAuthKeywithAmount(Decimal Amount, String ApplicationId){
        return JSON.serialize(getBpointAuthKey(Amount,ApplicationID));
    }

    //Method called from associated Lightning component init method to determie if the running or issandbox or not
    @AuraEnabled
    public static Boolean runningInASandbox() {
        return [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
    }

    
}