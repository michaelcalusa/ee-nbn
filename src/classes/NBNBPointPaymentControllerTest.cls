@isTest
private class NBNBPointPaymentControllerTest{
    static integer statusCode = 200;
    static string getauthkeyjsonres = '{'+
          '			"APIResponse":{'+
          '                   "ResponseCode": 0,'+
          '                   "ResponseText":"Success"'+
          '                		  },'+
          '         "AuthKey": "df998fea-f309-4e6e-9629-7149799dc028"'+
                         '}';
    static string getauthkeyjsonresError = '{'+
          '			"APIResponse":{'+
          '                   "ResponseCode": 1,'+
          '                   "ResponseText":"Error"'+
          '                		  },'+
          '         "AuthKey": "df998fea-f309-4e6e-9629-7149799dc028"'+
                         '}';
    static string submitpaymentjsonres = '{'+
          '			"APIResponse":{'+
          '                   "ResponseCode": 0,'+
          '                   "ResponseText":"Success"'+
          '                		  },'+
          '         "ResultKey": "df998fea-f309-4e6e-9629-7149799dc028",'+
          '			"RedirectionUrl": "https://www.bpoint.com.au/webapi/v3/txns/tokenreceipt?ResponseCode=0&ResponseText=Success&ResultKey=df998fea-f309-4e6e-9629-7149799dc028"'+
                         '}';
    
    static string gettrasactionjsonres = '{'+
          '			"APIResponse":{'+
          '                   "ResponseCode": 0,'+
          '                   "ResponseText":"Success"'+
          '                		  },'+
          '          "TxnResp" : {'+
          '                   "Action" : "payment",'+
          '					  "Amount" : 19900,'+
          '					  "AmountOriginal" : 19900,'+
          '					  "AmountSurcharge" : 0,'+
          '					  "ThreeDSResponse" : null,'+
          '					  "AuthoriseId" : "384582",'+
          '					  "BankAccountDetails" : null,'+
          '					  "BankResponseCode" : "16",'+
          ' 				  "CVNResult" : {'+
          '					 			"CVNResultCode" : "Unsupported"'+
          '						 		},'+
          '			"CardDetails" : {'+
          '						"CardHolderName" : "John Smith",'+
          '						"ExpiryDate" : "0521",'+
          '						"MaskedCardNumber" : "512345...346"'+
          '							},'+
          '			"CardType" : "MC",'+
          '			"Currency" : "AUD",'+
          '			"MerchantReference" : "test merchant ref",'+
          '			"IsThreeDS" : false,'+
          '			"IsCVNPresent" : true,'+
          '			"MerchantNumber  " : "5353109000000000",'+
          '			"OriginalTxnNumber" : null,'+
          '			"ProcessedDateTime" : "2014-12-12T12:27:15.6830000",'+
          '			"RRN" : "434612384582",'+
          '			"ReceiptNumber" : "49316631179",'+
          '			"Crn1" : "test crn1",'+
          '			"Crn2" : "test crn2",'+
          '			"Crn3" : "test crn3",'+
          '			"ResponseCode" : "0",'+
          '			"ResponseText" : "Approved",'+
          '			"BillerCode" : "",'+
          '			"SettlementDate" : "20141212",'+
          '			"Source" : "api",'+
          '			"StoreCard" : false,'+
          '			"IsTestTxn" : false,'+
          '			"SubType" : "single",'+
          '			"TxnNumber" : "1179",'+
          '			"DVToken" : null,'+
          '			"Type" : "internet"'+
          '			}'+   
    			'}';
    
    static string gettrasactionjsonresError = '{'+
          '			"APIResponse":{'+
          '                   "ResponseCode": 998,'+
          '                   "ResponseText":"Success"'+
          '                		  },'+
          '          "TxnResp" : {'+
          '                   "Action" : "payment",'+
          '					  "Amount" : 19900,'+
          '					  "AmountOriginal" : 19900,'+
          '					  "AmountSurcharge" : 0,'+
          '					  "ThreeDSResponse" : null,'+
          '					  "AuthoriseId" : "384582",'+
          '					  "BankAccountDetails" : null,'+
          '					  "BankResponseCode" : "09",'+
          ' 				  "CVNResult" : {'+
          '					 			"CVNResultCode" : "Unsupported"'+
          '						 		},'+
          '			"CardDetails" : {'+
          '						"CardHolderName" : "John Smith",'+
          '						"ExpiryDate" : "0521",'+
          '						"MaskedCardNumber" : "512345...346"'+
          '							},'+
          '			"CardType" : "MC",'+
          '			"Currency" : "AUD",'+
          '			"MerchantReference" : "test merchant ref",'+
          '			"IsThreeDS" : false,'+
          '			"IsCVNPresent" : true,'+
          '			"MerchantNumber  " : "5353109000000000",'+
          '			"OriginalTxnNumber" : null,'+
          '			"ProcessedDateTime" : "2014-12-12T12:27:15.6830000",'+
          '			"RRN" : "434612384582",'+
          '			"ReceiptNumber" : "49316631179",'+
          '			"Crn1" : "test crn1",'+
          '			"Crn2" : "test crn2",'+
          '			"Crn3" : "test crn3",'+
          '			"ResponseCode" : "0",'+
          '			"ResponseText" : "Approved",'+
          '			"BillerCode" : "",'+
          '			"SettlementDate" : "20141212",'+
          '			"Source" : "api",'+
          '			"StoreCard" : false,'+
          '			"IsTestTxn" : false,'+
          '			"SubType" : "single",'+
          '			"TxnNumber" : "1179",'+
          '			"DVToken" : null,'+
          '			"Type" : "internet"'+
          '			}'+   
    			'}';
    static Map<String, String> responseHeaders = new Map<String, String>();
    
   @testSetup static void testData(){
        insert new Payment_Component_Response_Matrix__c(Bank_Response_Code__c = '16', Name = 'R85', Response_Code__c = '0', Response_text__c = 'Approved', Type__c = 'Transaction Response Code');
        insert new Payment_Component_Response_Matrix__c(Bank_Response_Code__c = '09', Name = 'R86', Response_Code__c = '1', Response_text__c = Null, Type__c = 'Transaction Response Code');
		insert new Payment_Component_Response_Matrix__c(Bank_Response_Code__c = '12', Name = 'R89', Response_Code__c = '1', Response_text__c = 'Invalid Transaction – Please check your Card Details and try again', Retry__c = True, Type__c = 'Transaction Response Code');
		insert new Payment_Component_Response_Matrix__c(Bank_Response_Code__c = Null, Name = 'R81', Response_Code__c = '998', Response_text__c = 'Invalid request payload', Retry__c = True, Type__c = 'API Response Code');
        insert new Payment_Component_Response_Matrix__c(Bank_Response_Code__c = Null, Name = 'R1', Response_Code__c = Null, Response_text__c = 'An error occurred with the Card Details you have entered.', Retry__c = False, Type__c = 'Hard Failure');
    } 
    
    
    static testMethod void testgetauthkey() {
       	/*Id ndRtId = GlobalCache.getRecordTypeId('cspmb__Price_Item__c', GlobalConstants.NEW_DEVELOPMENTS);
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name='Application Fee', cspmb__One_Off_Cost__c = 660, cspmb__Price_Item_Code__c = 'TCAPPFEE', recordtypeId = ndRtId, cspmb__Is_Active__c = true);
        insert priceItem;*/
       /*	PageReference pageRef = Page.testbpointapppage;
        Test.setCurrentPage(pageRef);*/
        responseHeaders.put('Content-Type','application/JSON');
        Test.startTest();
        MockHttpResponseGenerator_Test getauthkeyfakeresponse = new MockHttpResponseGenerator_Test(statusCode, getauthkeyjsonres, responseHeaders);
        Test.setMock(HttpCalloutMock.class, getauthkeyfakeresponse);
        BpointPaymentUtils.ResponseToUi authkey = NBNBPointPaymentController.getBpointAuthKey(600.00,'58534112342342000342');
        Test.stopTest();
        //System.assertEquals(getauthkeyjsonres, authkey);
        System.debug('auth key..' + authkey); 
    }
    
    static testMethod void testgetauthkeyNoResponse() {
       	/*Id ndRtId = GlobalCache.getRecordTypeId('cspmb__Price_Item__c', GlobalConstants.NEW_DEVELOPMENTS);
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name='Application Fee', cspmb__One_Off_Cost__c = 660, cspmb__Price_Item_Code__c = 'TCAPPFEE', recordtypeId = ndRtId, cspmb__Is_Active__c = true);
        insert priceItem;*/
        responseHeaders.put('Content-Type','application/JSON');
        Test.startTest();
        MockHttpResponseGenerator_Test getauthkeyfakeresponse = new MockHttpResponseGenerator_Test(statusCode, getauthkeyjsonresError, Null);
        Test.setMock(HttpCalloutMock.class, getauthkeyfakeresponse);
        BpointPaymentUtils.ResponseToUi authkey = NBNBPointPaymentController.getBpointAuthKey(600.00,'58534112342342000342');
        Test.stopTest();
        //System.assertEquals(getauthkeyjsonres, authkey);
        System.debug('auth key..' + authkey); 
    }
    
    static testMethod void testgetauthkeyNoPriceData() {
       	/*Id ndRtId = GlobalCache.getRecordTypeId('cspmb__Price_Item__c', GlobalConstants.NEW_DEVELOPMENTS);
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name='Application Fee', cspmb__One_Off_Cost__c = 660, cspmb__Price_Item_Code__c = 'TCAPPFEE', recordtypeId = ndRtId, cspmb__Is_Active__c = true);
        insert priceItem;*/
        responseHeaders.put('Content-Type','application/JSON');
        Test.startTest();
        MockHttpResponseGenerator_Test getauthkeyfakeresponse = new MockHttpResponseGenerator_Test(0, getauthkeyjsonres, responseHeaders);
        Test.setMock(HttpCalloutMock.class, getauthkeyfakeresponse);
        BpointPaymentUtils.ResponseToUi authkey = NBNBPointPaymentController.getBpointAuthKey(600.00,'58534112342342000342');
        Test.stopTest();
        //System.assertEquals(getauthkeyjsonres, authkey);
        System.debug('auth key..' + authkey); 
    }
    
    static testMethod void testgetauthkeyError() {
        
       	/*Id ndRtId = GlobalCache.getRecordTypeId('cspmb__Price_Item__c', GlobalConstants.NEW_DEVELOPMENTS);
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name='Single Dwelling Unit - Class 3/4', cspmb__One_Off_Cost__c = 660, cspmb__Price_Item_Code__c = 'TCAPPFEE', recordtypeId = ndRtId, cspmb__Is_Active__c = true);
        insert priceItem;*/
        responseHeaders.put('Content-Type','application/JSON');
        Test.startTest();
        MockHttpResponseGenerator_Test getauthkeyfakeresponse = new MockHttpResponseGenerator_Test(0, getauthkeyjsonres, responseHeaders);
        Test.setMock(HttpCalloutMock.class, getauthkeyfakeresponse);
        BpointPaymentUtils.ResponseToUi authkey = NBNBPointPaymentController.getBpointAuthKey(600.00,'58534112342342000342');
        Test.stopTest();
        //System.assertEquals(getauthkeyjsonres, authkey);
        System.debug('auth key..' + authkey); 
    }
    
    static testMethod void testSubmitPayment() {
       	/*Id ndRtId = GlobalCache.getRecordTypeId('cspmb__Price_Item__c', GlobalConstants.NEW_DEVELOPMENTS);
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name='Single Dwelling Unit - Class 3/4', cspmb__One_Off_Cost__c = 660, cspmb__Price_Item_Code__c = 'TCAPPFEE', recordtypeId = ndRtId, cspmb__Is_Active__c = true);
        insert priceItem;*/
        Test.startTest();
        responseHeaders.put('Content-Type','application/JSON');
        MockHttpResponseGenerator_Test getsubmitfakeresponse = new MockHttpResponseGenerator_Test(statusCode,gettrasactionjsonres,responseHeaders);
        Test.setMock(HttpCalloutMock.class, getsubmitfakeresponse);
        String resauthkey = NBNBPointPaymentController.submitPayment('df998fea-f309-4e6e-9629-7149799dc028', '58534112342342000342');
        system.debug('resauthkey...' + resauthkey);
       // System.assertEquals(submitpaymentjsonres, resauthkey);
        Test.stopTest();
    }
    
    static testMethod void testSubmitPaymentNoResponse() {
       	/*Id ndRtId = GlobalCache.getRecordTypeId('cspmb__Price_Item__c', GlobalConstants.NEW_DEVELOPMENTS);
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name='Application Fee', cspmb__One_Off_Cost__c = 660, cspmb__Price_Item_Code__c = 'TCAPPFEE', recordtypeId = ndRtId, cspmb__Is_Active__c = true);
        insert priceItem;*/
        Test.startTest();
        responseHeaders.put('Content-Type','application/JSON');
        MockHttpResponseGenerator_Test getsubmitfakeresponse = new MockHttpResponseGenerator_Test(statusCode,getauthkeyjsonresError,responseHeaders);
        Test.setMock(HttpCalloutMock.class, getsubmitfakeresponse);
        String resauthkey = NBNBPointPaymentController.submitPayment('df998fea-f309-4e6e-9629-7149799dc028', '58534112342342000342');
        system.debug('resauthkey...' + resauthkey);
       // System.assertEquals(submitpaymentjsonres, resauthkey);
        Test.stopTest();
    }
    
    static testMethod void testSubmitPaymentInvalidResponse() {
       /*	Id ndRtId = GlobalCache.getRecordTypeId('cspmb__Price_Item__c', GlobalConstants.NEW_DEVELOPMENTS);
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name='Application Fee', cspmb__One_Off_Cost__c = 660, cspmb__Price_Item_Code__c = 'TCAPPFEE', recordtypeId = ndRtId, cspmb__Is_Active__c = true);
        insert priceItem;*/
        Test.startTest();
        responseHeaders.put('Content-Type','application/JSON');
        MockHttpResponseGenerator_Test getsubmitfakeresponse = new MockHttpResponseGenerator_Test(statusCode,gettrasactionjsonresError,responseHeaders);
        Test.setMock(HttpCalloutMock.class, getsubmitfakeresponse);
        String resauthkey = NBNBPointPaymentController.submitPayment('df998fea-f309-4e6e-9629-7149799dc028', '58534112342342000342');
        system.debug('resauthkey...' + resauthkey);
        //System.assertEquals(gettrasactionjsonres, resauthkey);
        Test.stopTest();
    }
    
    static testMethod void testSubmitPaymentError() {
       	/*Id ndRtId = GlobalCache.getRecordTypeId('cspmb__Price_Item__c', GlobalConstants.NEW_DEVELOPMENTS);
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name='Application Fee', cspmb__One_Off_Cost__c = 660, cspmb__Price_Item_Code__c = 'TCAPPFEE', recordtypeId = ndRtId, cspmb__Is_Active__c = true);
        insert priceItem;*/
        Test.startTest();
        responseHeaders.put('Content-Type','application/JSON');
        MockHttpResponseGenerator_Test getsubmitfakeresponse = new MockHttpResponseGenerator_Test(0,gettrasactionjsonres,responseHeaders);
        Test.setMock(HttpCalloutMock.class, getsubmitfakeresponse);
        String resauthkey = NBNBPointPaymentController.submitPayment('df998fea-f309-4e6e-9629-7149799dc028', '58534112342342000342');
        system.debug('resauthkey...' + resauthkey);
       // System.assertEquals(submitpaymentjsonres, resauthkey);
        Test.stopTest();
    }
    
    static testMethod void testBpointUtils(){
        BpointPaymentUtils.TxnResp txn = new BpointPaymentUtils.TxnResp();
        BpointPaymentUtils.TransactionResponse txnres = new BpointPaymentUtils.TransactionResponse();
        txn.AmountOriginal = 4500;
        //BpointPaymentUtils.CustomErrorResponse('t23', '0', '2fddf-2434xx-64454-334343', '19', 'Approved', txn);
    }
    
    static testMethod void testgetAmountNewDevelopments() {
       	Id ndRtId = GlobalCache.getRecordTypeId('cspmb__Price_Item__c', GlobalConstants.NEW_DEVELOPMENTS);
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(Name='Single Dwelling Unit - Class 3/4', cspmb__One_Off_Cost__c = 600, cspmb__Price_Item_Code__c = 'TCAPPFEE', recordtypeId = ndRtId, cspmb__Is_Active__c = true);
        insert priceItem;
        Test.startTest();
        Decimal newDevfee = NBNBPointPaymentController.getAmountNewDevelopments('Single Dwelling Unit - Class 3/4', '4', 'NewDev');
        system.assertEquals(2400.00, newDevfee);
        Test.stopTest();
    }
    
    static testMethod void testgetAuthKeywithAmount(){
        string AuthkeywithAmt = NBNBPointPaymentController.getAuthKeywithAmount(2400, '343525235454545');
        //BpointPaymentUtils.CustomErrorResponse('t23', '0', '2fddf-2434xx-64454-334343', '19', 'Approved', txn);
    }
}