/***************************************************************************************************
    Class Name          : NBNGoArticleInformation
    Class Type          : Integration Class
    Version             : 1.0 
    Created Date        : 02-June-2017
    Description         : View article information by NBNGo App
    Input Parameters    : JSON Request 
    Output Parameters   : HelperIntegrationUtility.responseStatus


    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats       02-June-2017      View article information by NBNGo App
****************************************************************************************************/ 
@RestResource(urlMapping='/viewArticle/*')
global class NBNGoArticleInformation{
    static string jsonRequest, responseJsonMessage;
    static string currentDateTime = HelperIntegrationUtility.getcurrentDateTime(system.now());
    static string exceptionMessage = HelperUtility.getErrorMessage('008');
    static HelperIntegrationUtility.getArticleResponse getResponse = new HelperIntegrationUtility.getArticleResponse();

    @HttpGet
    global static void getNBNGoArticleInfo() {
        try{
            String strArticleNumber = RestContext.request.params.get('article');
            getResponse = processArticleRequest(strArticleNumber);
            if(String.IsBlank(getResponse.code)){
                GlobalUtility.logMessage('Error','NBNGoArticleInformation','getNBNGoArticleInfo','','getNBNGoArticleInfo','getNBNGoArticleInfo method error',jsonRequest,null,0);
                getResponse = HelperIntegrationUtility.generateArticleResponse('view NBNGo Article',false,'','008',exceptionMessage,currentDateTime,null); // 008
            }
            
            responseJsonMessage = JSON.serializePretty(getResponse,true);  
            responseJsonMessage = '{ "getArticleInfo" : ' + responseJsonMessage + ' }';
            restContext.response.responseBody = Blob.valueOf(responseJsonMessage);
            restContext.response.addHeader('Content-Type', 'application/json');  
        }Catch(Exception ex){
            System.debug('--getNBNGoArticleInfo method exception--' + ex.getMessage());
            // Log the Exception
            GlobalUtility.logMessage('Error','NBNGoArticleInformation','getNBNGoArticleInfo','','getNBNGoArticleInfo','getNBNGoArticleInfo method error',jsonRequest,ex,0);
            getResponse = HelperIntegrationUtility.generateArticleResponse('view NBNGo Article',false,'','008',exceptionMessage,currentDateTime,null);
                
            responseJsonMessage = JSON.serializePretty(getResponse,true);  
            responseJsonMessage = '{ "getArticleInfo" : ' + responseJsonMessage + ' }';
            restContext.response.responseBody = Blob.valueOf(responseJsonMessage);
            restContext.response.addHeader('Content-Type', 'application/json');
        }
    }
    
    static HelperIntegrationUtility.getArticleResponse processArticleRequest(String strArticleNumber){
        HelperIntegrationUtility.NBNGoArticleInfo responseDetails;

        if(!String.IsBlank(strArticleNumber)){
            List<FAQ__kav> lstArticles = [ SELECT Id, Title, ArticleNumber, Internal_Content__c, Retail_Service_Provider_Content__c, Public_Content__c, LastPublishedDate FROM FAQ__kav WHERE ArticleNumber =: strArticleNumber AND PublishStatus = 'Online' AND Language = 'en_US' Limit 1 UPDATE VIEWSTAT ];
            
            // Track the article view
            NBNGoArticleInformationHelper.trackArticleView(lstArticles[0].Title, lstArticles[0].ArticleNumber, 'FAQ');
            
            if(!lstArticles.isEmpty()){
                String strContent = '';
                if(!String.IsBlank(lstArticles[0].Internal_Content__c)){
                    strContent = lstArticles[0].Internal_Content__c;
                }
                
                if(!String.IsBlank(lstArticles[0].Retail_Service_Provider_Content__c)){
                    if(String.IsBlank(strContent)){
                        strContent = lstArticles[0].Retail_Service_Provider_Content__c;
                    }else{
                        strContent = strContent + lstArticles[0].Retail_Service_Provider_Content__c;
                    }
                }
                
                if(!String.IsBlank(lstArticles[0].Public_Content__c)){
                    if(String.IsBlank(strContent)){
                        strContent = lstArticles[0].Public_Content__c;
                    }else{
                        strContent = strContent + lstArticles[0].Public_Content__c;
                    }
                }
                
                strContent = NBNGoArticleInformationHelper.stripArticleContent(strContent);
                responseDetails = new HelperIntegrationUtility.NBNGoArticleInfo(lstArticles[0].Title, strContent, string.valueOf(lstArticles[0].LastPublishedDate));
             }else{
                return HelperIntegrationUtility.generateArticleResponse('view NBNGo Article',true,'','042',HelperUtility.getErrorMessage('042'),currentDateTime,responseDetails);
            }
        }else{
            return HelperIntegrationUtility.generateArticleResponse('view NBNGo Article',false,'','041',HelperUtility.getErrorMessage('041'),currentDateTime,responseDetails);
        }

        return HelperIntegrationUtility.generateArticleResponse('view NBNGo Article',true,'','040',HelperUtility.getErrorMessage('040'),currentDateTime,responseDetails);
    }
}