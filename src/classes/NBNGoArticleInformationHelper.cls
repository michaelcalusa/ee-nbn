/***************************************************************************************************
    Class Name          : NBNGoArticleInformationHelper
    Class Type          : NBNGoArticleInformation helper Class
    Version             : 1.0 
    Created Date        : 07-June-2017
    Description         : Track article information by NBNGo App

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats       02-June-2017      View article information by NBNGo App
****************************************************************************************************/ 
public class NBNGoArticleInformationHelper{
    
    // This method will receive the article html content and return the content after removing images and anchor tags
    public static string stripArticleContent(String strContent){
        String strTrimmedContent = '';

        if(!string.isBlank(strContent)){
            // Remove images with pattern <img ...></img>
            if(strContent.contains('<img')){
                List<String> lstImgContents = strContent.split('<img([\\w\\W]+?)img>');

                for(String s : lstImgContents){
                    strTrimmedContent = strTrimmedContent + s;
                }
            }
            else{
                strTrimmedContent = strContent;
            }
            
            // Remove images with pattern <img .../>
            if(strTrimmedContent.contains('<img')){
                List<String> lstImgContent = strTrimmedContent.split('<img([\\w\\W]+?)>');
                strTrimmedContent = '';
                for(String s : lstImgContent){
                    strTrimmedContent = strTrimmedContent + s;
                }
            }
            
            // Remove anchor tags
            if(strTrimmedContent.contains('<a')){
                strTrimmedContent = strTrimmedContent.replace('<a','#sfapp#<a').replace('</a>','</a>#sfapp#');
                List<String> lstLinkContents = strTrimmedContent.split('#sfapp#');
                strTrimmedContent = '';
                for(String s : lstLinkContents){
                    if(s.contains('<a') && s.contains('nbnco.com.au')){
                        strTrimmedContent = strTrimmedContent + s;
                    }
                    else if(s.contains('<a') && !s.contains('nbnco.com.au')){
                        List<String> lstLink = s.split('<a([\\w\\W]+?)>');

                        if(lstLink.size() > 1){
                            strTrimmedContent = strTrimmedContent + lstLink[1].replace('</a>','');
                        }
                        else if(lstLink.size() == 1){
                            strTrimmedContent = strTrimmedContent + lstLink[0].replace('</a>','');
                        }
                    }
                    else if(!s.contains('<a')){
                        strTrimmedContent = strTrimmedContent + s;
                    }
                }
            }

            strTrimmedContent = strTrimmedContent.replaceAll('(?!(\\shref|\\starget))((\\s+\\w+=")[^>]*("))','').replaceFirst('<span><span><i>','<h3><span>').replaceFirst('</i></span></span>','</span></h3>').replaceFirst('<span><span>','<h2>').replaceFirst('</span></span>','</h2>').normalizeSpace();
            strTrimmedContent = Label.nbn_go_Article_CSS + '<div>' + strTrimmedContent + '</div>';
            system.debug('---strTrimmedContent---' + strTrimmedContent);
        }
        return strTrimmedContent;
    }
    
    // This method will receive the article details and captueres them in 'Knowledge Tracking' for reporting purpose
    @future(callout=false)
    public static void trackArticleView(String strTitle, String strNumber, String strType){
        try{
            Knowledge_Tracking__c kt;
            
            // Search for the existing article tracking 
            List<Knowledge_Tracking__c> lstArticleTrack = [ SELECT Id, Article_Number__c, Article_Title__c, Article_Type__c, nbngo_View_Count__c FROM Knowledge_Tracking__c WHERE Article_Number__c =: strNumber ];
            
            // If found then UPDATE the count by 1
            if(lstArticleTrack.size() > 0){
                kt = new Knowledge_Tracking__c(Id = lstArticleTrack[0].Id);
                kt.nbngo_View_Count__c = lstArticleTrack[0].nbngo_View_Count__c + 1;
            }
            // If NOT found then INSERT the tracking record
            else{
                kt = new Knowledge_Tracking__c();
                kt.Article_Number__c = strNumber;
                kt.Article_Title__c = strTitle;
                kt.Article_Type__c = strType;
                kt.nbngo_View_Count__c = 1;
            }
            
            upsert kt;

        }Catch(Exception ex){
            System.debug('--trackArticleView method exception--' + ex.getMessage());
            // Log the Exception
            GlobalUtility.logMessage('Error','NBNGoArticleInformation','trackArticleView','','createNBNGoCase','trackArticleView method error','',ex,0);
        }
    }
}