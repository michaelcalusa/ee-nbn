/***************************************************************************************************
    Class Name  :  NBNGoArticleInformation_Test
    Class Type  : Test Class 
    Version     : 1.0 
    Created Date: 07-June-2017
    Function    : This class contains unit test scenarios for NBNGoArticleInformation apex class which handles view article logic for NBNGo App
    Used in     : None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            19/05/2017                 Created
****************************************************************************************************/
@isTest(seeAllData = false)
public class NBNGoArticleInformation_Test{
   
    static testMethod void TestMethod_ViewArticle() {
        Profile p = [ SELECT Id from Profile WHERE Name = 'System Administrator' ];
        User u = new User(Username = 'nbngosupport@nbnco.com.au', Email='nbngo@nbnco.com.au', Alias = 'nbntest', CommunityNickname = 'nbntest', TimeZoneSidKey = 'Australia/Sydney', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', ProfileId = p.Id, LanguageLocaleKey = 'en_US');
        u.LastName = 'NBN Go App';
        insert u;
        
        List<FAQ__kav> lstFAQ = new List<FAQ__kav>();
        FAQ__kav faqA = new FAQ__kav(Title='test article A', UrlName='testarticleurlA', Language='en_US', Internal_Content__c = 'internal test<img></img><a>test</a><a href="https://nbnco.com.au/test">test</a><img />', Retail_Service_Provider_Content__c = 'RSP test', Public_Content__c = 'public test');
        FAQ__kav faqB = new FAQ__kav(Title='test article B', UrlName='testarticleurlB', Language='en_US', Retail_Service_Provider_Content__c = 'RSP test', Public_Content__c = 'public test');
        FAQ__kav faqC = new FAQ__kav(Title='test article C', UrlName='testarticleurlC', Language='en_US', Public_Content__c = 'public test');
        
        lstFAQ.add(faqA);
        lstFAQ.add(faqB);
        lstFAQ.add(faqC);
        insert lstFAQ;
        
        List<FAQ__kav> lstFAQs = [ SELECT Id, ArticleNumber, KnowledgeArticleId FROM FAQ__kav WHERE PublishStatus = 'Draft' AND Language = 'en_US' ];
        for (FAQ__kav article : lstFAQs) {
            KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
        }
        
        system.runAs(u){
            Test.startTest();
            
            RestRequest req = new RestRequest(); 
            req.params.put('article', lstFAQs[0].ArticleNumber);
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/viewArticle';  //Request URL
            req.httpMethod = 'GET';//HTTP Request Type
            RestContext.request = req;
            RestContext.response= res;
            NBNGoArticleInformation.getNBNGoArticleInfo();
            
            RestRequest reqB = new RestRequest(); 
            reqB.params.put('article', lstFAQs[1].ArticleNumber);
            RestResponse resB = new RestResponse();
            reqB.requestURI = '/services/apexrest/viewArticle';  //Request URL
            reqB.httpMethod = 'GET';//HTTP Request Type
            RestContext.request = reqB;
            RestContext.response= resB;
            NBNGoArticleInformation.getNBNGoArticleInfo();
            
            RestRequest reqC = new RestRequest(); 
            reqC.params.put('article', lstFAQs[2].ArticleNumber);
            RestResponse resC = new RestResponse();
            reqC.requestURI = '/services/apexrest/viewArticle';  //Request URL
            reqC.httpMethod = 'GET';//HTTP Request Type
            RestContext.request = reqC;
            RestContext.response= resC;
            NBNGoArticleInformation.getNBNGoArticleInfo();
            
            // Dreaft
            RestRequest reqD = new RestRequest(); 
            reqD.params.put('article', lstFAQ[0].ArticleNumber);
            RestResponse resD = new RestResponse();
            reqD.requestURI = '/services/apexrest/viewArticle';  //Request URL
            reqD.httpMethod = 'GET';//HTTP Request Type
            RestContext.request = reqD;
            RestContext.response= resD;
            NBNGoArticleInformation.getNBNGoArticleInfo();
            
            //Incorrect article
            RestRequest reqE = new RestRequest(); 
            reqE.params.put('article', '12345');
            RestResponse resE = new RestResponse();
            reqE.requestURI = '/services/apexrest/viewArticle';  //Request URL
            reqE.httpMethod = 'GET';//HTTP Request Type
            RestContext.request = reqE;
            RestContext.response= resE;
            NBNGoArticleInformation.getNBNGoArticleInfo();
            
            //Read article again
            RestRequest reqF = new RestRequest(); 
            reqF.params.put('article', lstFAQs[2].ArticleNumber);
            RestResponse resF = new RestResponse();
            reqF.requestURI = '/services/apexrest/viewArticle';  //Request URL
            reqF.httpMethod = 'GET';//HTTP Request Type
            RestContext.request = reqF;
            RestContext.response= resF;
            NBNGoArticleInformation.getNBNGoArticleInfo();
            
            Test.stopTest();
        }
    }
}