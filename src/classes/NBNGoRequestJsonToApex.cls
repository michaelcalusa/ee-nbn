/***************************************************************************************************
    Class Name          : NBNGoRequestJsonToApex
    Class Type          : NBNGo App Request envelope
    Version             : 1.0 
    Created Date        : 09-May-2017
    Description         : NBNGo App Request envelope

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats       09-May-2017      NBNGo App Request envelope
****************************************************************************************************/ 
public class NBNGoRequestJsonToApex {
    public CreateCaseRequestDetails createCaseRequestDetails {get;set;} 
    
    public static NBNGoRequestJsonToApex parse(String json) {
        return new NBNGoRequestJsonToApex(System.JSON.createParser(json));
    }
    
    public NBNGoRequestJsonToApex(JSONParser parser) {
        while (parser.nextToken() != JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != JSONToken.VALUE_NULL) {
                    if (text == 'createCaseRequestDetails') {
                        createCaseRequestDetails = new CreateCaseRequestDetails(parser);
                    }else {
                        System.debug(LoggingLevel.WARN, 'Root consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public static void consumeObject(JSONParser parser) {
        Integer depth = 0;
        do {
            JSONToken curr = parser.getCurrentToken();
            if (curr == JSONToken.START_OBJECT || 
                curr == JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == JSONToken.END_OBJECT ||
                curr == JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    
    public class NBNGoUSerInfo {
        public String emailAddress {get;set;} 

        public NBNGoUSerInfo(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'emailAddress') {
                            emailAddress = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'CaseRequester consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class PrimaryContactDetails {
        public String firstName {get;set;} 
        public String lastName {get;set;} 
        public String phoneNumber {get;set;} 
        public String emailAddress {get;set;} 
        public String preferredContactMethod {get;set;} 

        public PrimaryContactDetails(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'firstName') {
                            firstName = parser.getText();
                        } else if (text == 'lastName') {
                            lastName = parser.getText();
                        } else if (text == 'phoneNumber') {
                            phoneNumber = parser.getText();
                        } else if (text == 'emailAddress') {
                            emailAddress = parser.getText();
                        } else if (text == 'preferredContactMethod') {
                            preferredContactMethod = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'CaseRequester consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class SecondaryContactDetails {
        public String firstName {get;set;} 
        public String lastName {get;set;} 
        public String emailAddress {get;set;} 

        public SecondaryContactDetails(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'firstName') {
                            firstName = parser.getText();
                        } else if (text == 'lastName') {
                            lastName = parser.getText();
                        } else if (text == 'emailAddress') {
                            emailAddress = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'CaseRequester consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class CaseDetails {
        public String isNewCase {get;set;}
        public String caseNumber {get;set;}
        public String caseType {get;set;}
        public String dateofComplaintevent {get;set;}
        public String description {get;set;}
        public String desiredOutcome {get;set;}
        public String whatDamaged {get;set;}
        public String whereDamaged {get;set;}
        public String whenDamaged {get;set;}
        public String howDamaged {get;set;}
        public String whoDamaged {get;set;}

        public CaseDetails(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'isNewCase') {
                            isNewCase = parser.getText();
                        } else if (text == 'caseNumber') {
                            caseNumber = parser.getText();
                        } else if (text == 'caseType') {
                            caseType = parser.getText();
                        } else if (text == 'dateofComplaintevent') {
                            dateofComplaintevent = parser.getText();
                        } else if (text == 'description') {
                            description = parser.getText();
                        } else if (text == 'desiredOutcome') {
                            desiredOutcome = parser.getText();
                        } else if (text == 'whatDamaged') {
                            whatDamaged = parser.getText();
                        } else if (text == 'whereDamaged') {
                            whereDamaged = parser.getText();
                        } else if (text == 'whenDamaged') {
                            whenDamaged = parser.getText();
                        } else if (text == 'howDamaged') {
                            howDamaged = parser.getText();
                        } else if (text == 'whoDamaged') {
                            whoDamaged = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'CaseDetails consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }

    public class LocationAddress {
        public String premisetype {get;set;} 
        public String unitNumber {get;set;} 
        public String streetOrLotnumber {get;set;} 
        public String streetName {get;set;} 
        public String streetType {get;set;} 
        public String suburbOrlocality {get;set;} 
        public String state {get;set;} 
        public String postCode {get;set;}
        public String country {get;set;} 

        public LocationAddress(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'premisetype') {
                            premisetype = parser.getText();
                        } else if (text == 'unitNumber') {
                            unitNumber = parser.getText();
                        } else if (text == 'streetOrLotnumber') {
                            streetOrLotnumber = parser.getText();
                        } else if (text == 'streetName') {
                            streetName = parser.getText();
                        } else if (text == 'streetType') {
                            streetType = parser.getText();
                        } else if (text == 'suburbOrlocality') {
                            suburbOrlocality = parser.getText();
                        } else if (text == 'state') {
                            state = parser.getText();
                        } else if (text == 'postCode') {
                            postCode = parser.getText();
                        } else if (text == 'country') {
                            country = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'LocationAddress consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class CreateCaseRequestDetails {
        public String businessChannel {get;set;} 
        public String notifyNBNGoUser {get;set;}
        public String timestamp {get;set;} 
        
        public NBNGoUSerInfo nbngoUserInfo {get;set;}
        public CaseDetails caseDetails {get;set;} 
        public PrimaryContactDetails primaryContactDetails {get;set;}
        public SecondaryContactDetails secondaryContactDetails {get;set;}
        public LocationAddress locationAddress {get;set;} 
        
        public CreateCaseRequestDetails(JSONParser parser) {
            while (parser.nextToken() != JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != JSONToken.VALUE_NULL) {
                        if (text == 'businessChannel') {
                            businessChannel = parser.getText();
                        } else if (text == 'notifyNBNGoUser') {
                            notifyNBNGoUser = parser.getText();
                        } else if (text == 'timestamp') {
                            timestamp = parser.getText();
                        } else if (text == 'nbngoUserInfo') {
                            nbngoUserInfo = new NBNGoUSerInfo(parser);
                        } else if (text == 'caseDetails') {
                            caseDetails = new CaseDetails(parser);
                        } else if (text == 'locationAddress') {
                            locationAddress = new LocationAddress(parser);
                        } else if (text == 'primaryContactDetails') {
                            primaryContactDetails = new PrimaryContactDetails(parser);
                        } else if (text == 'secondaryContactDetails') {
                            secondaryContactDetails = new SecondaryContactDetails(parser);
                        } else {
                            System.debug(LoggingLevel.WARN, 'CreateCaseRequestDetails consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
}