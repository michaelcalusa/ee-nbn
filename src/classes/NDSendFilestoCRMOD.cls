/***************************************************************************************************
    Class Name          : NDSendFilestoCRMOD
    Test Class          :
    Version             : 1.0 
    Created Date        : 22-Nov-2017
    Author              : Dilip athley
    Description         : This class will send the files to CRMOD when the Task status is Submitted
    Input Parameters    :Task map from TaskTriggerHandler

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 

****************************************************************************************************/ 
public class NDSendFilestoCRMOD 
{ 
	public static void sendFiles(Map<Id,Task> areaPlanTaskMap)
    {
        set<Id> taskIdSet = new set<Id>();
        Set<Id> stageAppIdSet = new Set<Id>();
        List<ContentVersion> cvListToUpdate = new List<ContentVersion>();
        List<ContentDocumentLink> cdlNewList = new List<ContentDocumentLink>();
        List<Developments__e> eventListToPublish = new  List<Developments__e>();
        Set<Id> conDocIdSet = new Set<Id>();
        Set<String> saRequestIds = new Set<String>();
       Set<String> devCRMIds = new Set<String>();
        Map<Id,ContentDocumentLink> cdlMapTask = new Map<Id,ContentDocumentLink>();
        Map<Id,ContentDocumentLink> cdlMapStageApp = new Map<Id,ContentDocumentLink>();
        List<Task> taskClosed = new List<Task>();
        List<Task> remidTasks = new List<Task>();
        List<Task> pcnTasklist = new List<Task>();
        DOM.Document doc = new DOM.Document();
        DOM.Document devDoc = new DOM.Document();
        DOM.Document updDevDoc = new DOM.Document();
        String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';    
        String cus ='urn:crmondemand/ws/customobject3/10/2004';
        string cus1 = 'urn:/crmondemand/xml/customObject3';
        String devCus ='urn:crmondemand/ws/customobject2/10/2004';
        string devCus1 = 'urn:/crmondemand/xml/customObject2';
        String wsse = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        string wsu = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';
        boolean sendCRM = false;
        boolean updDev = false;
        
        try
        {
            for(Task t : areaPlanTaskMap.Values())
            {
                 stageAppIdSet.add(t.WhatId);
                 If(t.Deliverable_Status__c == 'Submitted')
                     taskIdSet.add(t.id);
        	}
            if(!taskIdSet.isEmpty())
            {
                Map<Id, Stage_Application__c> stageAppMap = new Map<Id,Stage_Application__c>([select Id, Request_Id__c, Development__r.Development_ID__c, Development__r.Active_Status__c from Stage_Application__c where Id in : stageAppIdSet and Request_Id__c != null FOR UPDATE]);
            
                for(ContentDocumentLink cdl : [SELECT ContentDocumentId,Id,LinkedEntityId,ShareType,Visibility FROM ContentDocumentLink WHERE LinkedEntityId in : taskIdSet])
                {
                    If((cdl.LinkedEntityId.getSObjectType().getDescribe().getName()=='Task') && (stageAppMap.containsKey(areaPlanTaskMap.get(cdl.LinkedEntityId).whatId)))
                    {
                       cdlMapTask.put(cdl.ContentDocumentId,cdl);
                       conDocIdSet.add(cdl.ContentDocumentId);
                    }
                }
                
                //creating the xml structure for sending the file to CRM for Stage App
                dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');        
                envelope.setNamespace('cus', cus);
                envelope.setNamespace('cus1', cus1);
                //Header Part
                dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
                dom.XmlNode security= header.addChildElement('Security', wsse, 'wsse');
                dom.XmlNode usernameToken = security.addChildElement('UsernameToken',wsse,null);
                usernameToken.addChildElement('Username',wsse,null).addTextNode(CRMOD_Credentials__c.getinstance('User Name').Value__c);
                dom.XmlNode password = usernameToken.addChildElement('Password',wsse,null);
                password.setAttribute('Type','http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');
                password.addTextNode(CRMOD_Credentials__c.getinstance('Password').Value__c);
                //body part
                dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
                dom.XmlNode StageInsertInput = body.addChildElement('CustomObject3WS_CustomObject3InsertChild_Input',cus,null);
                dom.XmlNode listOfStage = StageInsertInput.addChildElement('ListOfCustomObject3',cus1,null);
                
                
                //creating the xml structure for updating Development in CRM
                dom.XmlNode uDevEnvelope = updDevDoc.createRootElement('Envelope', soapNS, 'soapenv');        
                envelope.setNamespace('cus', cus);
                envelope.setNamespace('cus1', cus1);
                //Header Part
                dom.XmlNode uDevHeader = uDevEnvelope.addChildElement('Header', soapNS, null);
                dom.XmlNode uDevSecurity= uDevHeader.addChildElement('Security', wsse, 'wsse');
                dom.XmlNode uDevUsernameToken = uDevSecurity.addChildElement('UsernameToken',wsse,null);
                uDevUsernameToken.addChildElement('Username',wsse,null).addTextNode(CRMOD_Credentials__c.getinstance('User Name').Value__c);
                dom.XmlNode uDevPassword = uDevUsernameToken.addChildElement('Password',wsse,null);
                uDevPassword.setAttribute('Type','http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');
                uDevPassword.addTextNode(CRMOD_Credentials__c.getinstance('Password').Value__c);
                //body part
                dom.XmlNode uDevBody = uDevEnvelope.addChildElement('Body', soapNS, null);
                dom.XmlNode uDevInsertInput = uDevBody.addChildElement('CustomObject2WS_CustomObject2Update_Input',devCus,null);
                dom.XmlNode uListOfDev = uDevInsertInput.addChildElement('ListOfCustomObject2',devCus1,null);
                
                ////creating the xml structure for sending the file to CRM for Development
                dom.XmlNode devEnvelope = devDoc.createRootElement('Envelope', soapNS, 'soapenv');        
                envelope.setNamespace('cus', cus);
                envelope.setNamespace('cus1', cus1);
                //Header Part
                dom.XmlNode devHeader = devEnvelope.addChildElement('Header', soapNS, null);
                dom.XmlNode devSecurity= devHeader.addChildElement('Security', wsse, 'wsse');
                dom.XmlNode devUsernameToken = devSecurity.addChildElement('UsernameToken',wsse,null);
                devUsernameToken.addChildElement('Username',wsse,null).addTextNode(CRMOD_Credentials__c.getinstance('User Name').Value__c);
                dom.XmlNode devPassword = devUsernameToken.addChildElement('Password',wsse,null);
                devPassword.setAttribute('Type','http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText');
                devPassword.addTextNode(CRMOD_Credentials__c.getinstance('Password').Value__c);
                //body part
                dom.XmlNode devBody = devEnvelope.addChildElement('Body', soapNS, null);
                dom.XmlNode devInsertInput = devBody.addChildElement('CustomObject2WS_CustomObject2InsertChild_Input',devCus,null);
                dom.XmlNode listOfDev = devInsertInput.addChildElement('ListOfCustomObject2',devCus1,null);
                
                for(ContentVersion cv : [SELECT ContentDocumentId,Id,Synced__c,FileExtension,ContentSize,Title FROM ContentVersion WHERE ContentDocumentId in : conDocIdSet])
                { 
                     if(!cv.Synced__c)
                     {
                        sendCRM = true;
                               //Stage Part
                               dom.XmlNode stage = listOfStage.addChildElement('CustomObject3',cus1,null);
                               dom.XmlNode stageId = stage.addChildElement('CustomObject3Id',cus1,null);
                               stageId.addTextNode(stageAppMap.get(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).whatId).Request_Id__c);
                               dom.XmlNode listOfAtt = stage.addChildElement('ListOfAttachment',cus1,null);
                               dom.XmlNode att = listOfAtt.addChildElement('Attachment',cus1,null);
                               dom.XmlNode disFileName = att.addChildElement('DisplayFileName',cus1,null);
                               if(string.isNotBlank(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Sub_Type__c))
                               {
                                  disFileName.addTextNode(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Sub_Type__c+' - '+cv.Title); 
                               }
                               else
                               {
                                    disFileName.addTextNode(cv.Title);                               
                               }
                               
                               
                               dom.XmlNode fileNameURL = att.addChildElement('FileNameOrURL',cus1,null);
                               //system.debug(CRMOD_Credentials__c.getinstance('AttachmentURL').Value__c);
                               // system.debug(cvMap.get(cdl.ContentDocumentId));                          
                               fileNameURL.addTextNode(AttachmentURL__c.getinstance('AttachmentURL').Value__c+cv.Id);
                               
                               //Development Part
                               dom.XmlNode dev = listOfDev.addChildElement('CustomObject2',devCus1,null);
                               dom.XmlNode devId = dev.addChildElement('CustomObject2Id',devCus1,null);
                               devId.addTextNode(stageAppMap.get(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).whatId).Development__r.Development_ID__c);                                                                              
                               dom.XmlNode listOfDevAtt = dev.addChildElement('ListOfAttachment',devCus1,null);
                               dom.XmlNode devAtt = listOfDevAtt.addChildElement('Attachment',devCus1,null);
                               dom.XmlNode devDisFileName = devAtt.addChildElement('DisplayFileName',devCus1,null);
                               if(string.isNotBlank(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Sub_Type__c))
                               {
                                  devDisFileName.addTextNode(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Sub_Type__c+' - '+cv.Title); 
                               }
                               else
                               {
                                    devDisFileName.addTextNode(cv.Title);                               
                               }
                               
                                                    
                               dom.XmlNode devFileNameURL = devAtt.addChildElement('FileNameOrURL',devCus1,null);
                               //system.debug(CRMOD_Credentials__c.getinstance('AttachmentURL').Value__c);
                               // system.debug(cvMap.get(cdl.ContentDocumentId));
                               devFileNameURL.addTextNode(AttachmentURL__c.getinstance('AttachmentURL').Value__c+cv.Id);
                               
                               //Development Update Part
                               if((areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Sub_Type__c == 'Master Plan' || areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Sub_Type__c == 'Service Plan') && stageAppMap.get(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).whatId).Development__r.Active_Status__c == 'Incomplete')
                               {
                                   updDev = true;
                                   dom.XmlNode uDev = uListOfDev.addChildElement('CustomObject2',devCus1,null);
                                   dom.XmlNode uDevId = uDev.addChildElement('CustomObject2Id',devCus1,null);
                                   uDevId.addTextNode(stageAppMap.get(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).whatId).Development__r.Development_ID__c);
                                   dom.XmlNode devActiveStatus = uDev.addChildElement('CustomPickList10',devCus1,null);
                                   devActiveStatus.addTextNode('Active');
                               }
                         	cv.Synced__c = true;      // Enable flag to indicate that file has been published                                          
                           	cvListToUpdate.add(cv);
                         
                     }
                }
                
                if(sendCRM)
                {
                    //call the class to send the request to CRM.
                   sendToCRM.SendToCRMOD(doc.toXmlString(), '"document/urn:crmondemand/ws/customobject3/10/2004:CustomObject3InsertChild"', json.serialize(cvListToUpdate));
                   sendToCRM.SendToCRMOD(devDoc.toXmlString(), '"document/urn:crmondemand/ws/customobject2/10/2004:CustomObject2InsertChild"', '');               
                }
                if(updDev)
                {
                    sendToCRM.SendToCRMOD(updDevDoc.toXmlString(), '"document/urn:crmondemand/ws/customobject2/10/2004:CustomObject2Update"','');
                }
                
            
            }
            
        }
        catch(Exception ex)
        {
            system.debug(' message '+ex.getMessage()+' at line '+ex.getStacktracestring());
            GlobalUtility.logMessage('Error', 'InsertAttachmentCRMOD', 'sendRequest', ' ', '', '', '', ex, 0);
        }     
    }
    
    
}