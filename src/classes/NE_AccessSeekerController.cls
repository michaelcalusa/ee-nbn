public with sharing class NE_AccessSeekerController {
    
    @AuraEnabled
    public String getAccessSeekerId() {
        
        String accessSeekerId = '';
        Account[] recordList;
        
        recordList = [  SELECT Account.Access_Seeker_ID__c
                        FROM Account
                       	WHERE id IN
                            (SELECT AccountId
                             From User
                             WHERE username=:UserInfo.getUsername())];
        
        if (recordList.size() > 0) {
            accessSeekerId = recordList[0].Access_Seeker_ID__c;
        }
        
        return accessSeekerId;
    }
    
}