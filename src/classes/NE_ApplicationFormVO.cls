public with sharing class NE_ApplicationFormVO {
    @AuraEnabled public String userId { get; set; }

    @AuraEnabled public NE_LocationDetailsVO locationDetails { get; set; }
    @AuraEnabled public NE_AccessSeekerVO asDetails { get; set; }
    @AuraEnabled public NE_AccessSeekerBillingVO billDetails { get; set; }
    @AuraEnabled public NE_AccessSeekerBillingAddressVO billDetailsAddress { get; set; }
    @AuraEnabled public NE_ServiceDetailsVO serviceDetails { get; set; }
    @AuraEnabled public NE_ServiceRequirementsVO serviceRequirements { get; set; }
    @AuraEnabled public NE_DeliveryRequirementsVO deliveryRequirements { get; set; }
    @AuraEnabled public NE_BusinessDetailsVO businessDetails { get; set; }
    @AuraEnabled public NE_PrimaryContactDetailsVO primaryContactDetails { get; set; }
    @AuraEnabled public NE_SecondaryContactDetailsVO secondaryContactDetails { get; set; }
    @AuraEnabled public NE_SiteContactsVO siteContacts { get; set; }
    @AuraEnabled public NE_ContractVO contract { get; set; }
    @AuraEnabled public NE_ApplicationSummaryVO applicationSummary { get; set; }

    public class NE_LocationDetailsVO {
        @AuraEnabled public Boolean isExistingLocation { get; set; }
        @AuraEnabled public String existingLocationType { get; set; }
        @AuraEnabled public Boolean isValidLatLong { get; set; }
        @AuraEnabled public String latitude { get; set; }
        @AuraEnabled public String longitude { get; set; }
        @AuraEnabled public String locationId { get; set; }
        @AuraEnabled public String cpi { get; set; }
        @AuraEnabled public String serviceId { get; set; }
        @AuraEnabled public String streetNumber { get; set; }
        @AuraEnabled public String streetName { get; set; }
        @AuraEnabled public String suburb { get; set; }
        @AuraEnabled public String postcode { get; set; }
        @AuraEnabled public String state { get; set; }
        @AuraEnabled public String notes { get; set; }
    }

    public class NE_AccessSeekerVO {
        @AuraEnabled public String asId { get; set; }
        @AuraEnabled public String firstName { get; set; }
        @AuraEnabled public String lastName { get; set; }
        @AuraEnabled public String contactNumber { get; set; }
        @AuraEnabled public String additionalContactNumber { get; set; }
        @AuraEnabled public String email { get; set; }
    }

    public class NE_AccessSeekerBillingVO {
        @AuraEnabled public String abn { get; set; }
        @AuraEnabled public String companyName { get; set; }
        @AuraEnabled public String firstName { get; set; }
        @AuraEnabled public String lastName { get; set; }
        @AuraEnabled public String contactNumber { get; set; }
        @AuraEnabled public String additionalContactNumber { get; set; }
        @AuraEnabled public String email { get; set; }
    }

    public class NE_AccessSeekerBillingAddressVO {
        @AuraEnabled public String street { get; set; }
        @AuraEnabled public String city { get; set; }
        @AuraEnabled public String state { get; set; }
        @AuraEnabled public String postcode { get; set; }
        @AuraEnabled public String country { get; set; }
    }

    public class NE_ServiceDetailsVO {
        @AuraEnabled public String applicationType { get; set; }
        @AuraEnabled public String applicationUniqueId { get; set; }
        @AuraEnabled public String serviceEndPoint { get; set; }
        @AuraEnabled public String serviceEndPointId { get; set; }
    }

    public class NE_ServiceRequirementsVO {
        @AuraEnabled public String trafficClass4 { get; set; }
        @AuraEnabled public String trafficClass2 { get; set; }
        @AuraEnabled public String trafficClass1 { get; set; }
    }

    public class NE_DeliveryRequirementsVO {
        @AuraEnabled public Date proposedProjectDeliveryDate { get; set; }
        @AuraEnabled public String additionalDetails { get; set; }
        @AuraEnabled public List<String> fileNames { get; set; }
        @AuraEnabled public List<String> documentIds { get; set; }
        @AuraEnabled public List<NE_Attachment> attachments { get; set; }

        public NE_DeliveryRequirementsVO() {
            this.proposedProjectDeliveryDate = null;
            this.additionalDetails = '';
            this.fileNames = new List<String>();
            this.documentIds = new List<String>();
            this.attachments = new List<NE_Attachment>();
        }
    }

    public class NE_Attachment {
        @AuraEnabled public String fileName { get; set; }
        @AuraEnabled public String documentId { get; set; }
    }

    public class NE_BusinessDetailsVO {
        @AuraEnabled public String abn { get; set; }
        @AuraEnabled public String companyName { get; set; }
        @AuraEnabled public String contactNumber { get; set; }
        @AuraEnabled public String email { get; set; }
    }

    public class NE_PrimaryContactDetailsVO {
        @AuraEnabled public String role { get; set; }
        @AuraEnabled public String firstName { get; set; }
        @AuraEnabled public String lastName { get; set; }
        @AuraEnabled public String contactNumber { get; set; }
        @AuraEnabled public String email { get; set; }
    }

    public class NE_SecondaryContactDetailsVO {
        @AuraEnabled public String role { get; set; }
        @AuraEnabled public String firstName { get; set; }
        @AuraEnabled public String lastName { get; set; }
        @AuraEnabled public String contactNumber { get; set; }
        @AuraEnabled public String email { get; set; }
    }

    public class NE_SiteContactsVO {
        @AuraEnabled public String abn { get; set; }
        @AuraEnabled public String companyName { get; set; }
        @AuraEnabled public String role { get; set; }
        @AuraEnabled public String firstName { get; set; }
        @AuraEnabled public String lastName { get; set; }
        @AuraEnabled public String contactNumber { get; set; }
        @AuraEnabled public String email { get; set; }
    }

    public class NE_ContractVO {
        @AuraEnabled public Boolean contractAgreed { get; set; }
    }

    public class NE_ApplicationSummaryVO {
        @AuraEnabled public String applicationNumber { get; set; }
        @AuraEnabled public String quoteStatus { get; set; }
        @AuraEnabled public Date updatedDate { get; set; }
        @AuraEnabled public Decimal quote { get; set; }
        @AuraEnabled public String createdByName { get; set; }
    }
}