public without sharing class NE_Application_CreateController {
    public static final String RETAIL_SERVICE_PROVIDER_RSP = 'Retail Service Provider (RSP)';
    public static final String RSP_APPLICATION_NOTE = 'RSP Application Note';
    public static final String NEW_LOCATION = 'New Location';
    public static final String BUSINESS_CONTACT = 'Business Contact';
    public static final String SITE_CONTACT = 'Site Contact';

    private static final String EXTERNAL_CONTACT = 'External Contact';
    private static final String BUSINESS_END_CUSTOMER = 'Business End Customer';
    private static final String RECOVERABLE_WORKS = 'Recoverable Works';
    private static final String RETAIL_SERVICE_PROVIDER = 'Retail Service Provider';
    private static final String TIMEOUT = 'TIMEOUT';
    private static final String SESSION_TIMEOUT_MESSAGE = 'Your session has expired. Please log out and log in again.';

    private static Map<String, Id> CONTRACT_RECORD_TYPES = HelperUtility.pullAllRecordTypes('Contract');
    private static Map<String, Id> SITE_RECORD_TYPES = HelperUtility.pullAllRecordTypes('Site__c');
    private static Map<String, Id> OPPORTUNITY_RECORD_TYPES = HelperUtility.pullAllRecordTypes('Opportunity');

    private static final AccountService accountService = new AccountService();
    private static final ContactService contactService = new ContactService();

    @AuraEnabled
    public static NE_ApplicationFormVO initApplicationForm() {
        NE_ApplicationFormVO appForm = new NE_ApplicationFormVO();
        appForm.userId = UserInfo.getUserId();

        NE_ApplicationFormVO.NE_LocationDetailsVO locDetails = new NE_ApplicationFormVO.NE_LocationDetailsVO();
        locDetails.isExistingLocation = true;
        locDetails.existingLocationType = 'Migration';
        locDetails.isValidLatLong = false;
        appForm.locationDetails = locDetails;

        appForm.asDetails = new NE_ApplicationFormVO.NE_AccessSeekerVO();
        NE_AccessSeekerController asController = new NE_AccessSeekerController();
        appForm.asDetails.asId = asController.getAccessSeekerId();

        appForm.billDetails = new NE_ApplicationFormVO.NE_AccessSeekerBillingVO();
        appForm.billDetailsAddress = new NE_ApplicationFormVO.NE_AccessSeekerBillingAddressVO();

        appForm.serviceDetails = new NE_ApplicationFormVO.NE_ServiceDetailsVO();
        appForm.serviceRequirements = new NE_ApplicationFormVO.NE_ServiceRequirementsVO();
        appForm.deliveryRequirements = new NE_ApplicationFormVO.NE_DeliveryRequirementsVO();

        appForm.businessDetails = new NE_ApplicationFormVO.NE_BusinessDetailsVO();
        appForm.primaryContactDetails = new NE_ApplicationFormVO.NE_PrimaryContactDetailsVO();
        appForm.secondaryContactDetails = new NE_ApplicationFormVO.NE_SecondaryContactDetailsVO();
        appForm.siteContacts = new NE_ApplicationFormVO.NE_SiteContactsVO();

        appForm.contract = new NE_ApplicationFormVO.NE_ContractVO();

        return appForm;
    }

    @AuraEnabled
    public static String createApplication(String appForm) {
        System.debug(System.LoggingLevel.INFO, 'Received Network Extension Application Form:\n' + appForm);

        if (UserInfo.getSessionId() == null) {
            System.debug(System.LoggingLevel.INFO, 'The user session has expired.');
            return JSON.serialize(new CreateApplicationResponse(TIMEOUT, SESSION_TIMEOUT_MESSAGE));
        }

        NE_ApplicationFormVO neApplicationFormVO = (NE_ApplicationFormVO) JSON.deserialize(appForm, NE_ApplicationFormVO.class);
        List<OpportunityContactRole> opportunityContactRoles = new List<OpportunityContactRole>();

        Opportunity opportunity = new Opportunity();

        Site__c site = createSite(neApplicationFormVO, opportunity);

        setOpportunityDefaultValues(opportunity, site.Name, neApplicationFormVO.locationDetails);

        User user = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        NE_ApplicationFormVO.NE_AccessSeekerVO neAccessSeekerVO = neApplicationFormVO.asDetails;

        Contact accountContact = createExternalContact(
                neAccessSeekerVO.firstName,
                neAccessSeekerVO.lastName,
                neAccessSeekerVO.contactNumber,
                neAccessSeekerVO.email,
                neAccessSeekerVO.additionalContactNumber,
                user.AccountId);
        opportunityContactRoles.add(createOpportunityContactRole(accountContact.Id, RETAIL_SERVICE_PROVIDER_RSP, false));

        NE_ApplicationFormVO.NE_DeliveryRequirementsVO neDeliveryRequirementsVO = neApplicationFormVO.deliveryRequirements;
        opportunity.Requested_Completion_Date__c = neDeliveryRequirementsVO.proposedProjectDeliveryDate;
        opportunity.CloseDate = neDeliveryRequirementsVO.proposedProjectDeliveryDate;

        Contract contract = createBillingDetails(neApplicationFormVO);
        opportunity.AccountId = contract.AccountId;
        opportunity.ContractId = contract.Id;

        opportunity.NE_Application_Type__c = neApplicationFormVO.serviceDetails.applicationType;
        opportunity.NE_Application_Unique_Identifier__c = neApplicationFormVO.serviceDetails.applicationUniqueId;
        opportunity.NE_Service_End_Point__c = neApplicationFormVO.serviceDetails.serviceEndPoint;
        opportunity.NE_Service_End_Point_Identifier__c = neApplicationFormVO.serviceDetails.serviceEndPointId;
        opportunity.NE_Traffic_Class_4__c = neApplicationFormVO.serviceRequirements.trafficClass4;
        opportunity.NE_Traffic_Class_2__c = neApplicationFormVO.serviceRequirements.trafficClass2;
        opportunity.NE_Traffic_Class_1__c = neApplicationFormVO.serviceRequirements.trafficClass1;

        insert opportunity;
        String opportunityReferenceNumber = [SELECT Reference_Number__c FROM Opportunity WHERE Id = :opportunity.Id].Reference_Number__c;

        createOpportunityNote(neDeliveryRequirementsVO, opportunity.Id);
        createSiteNote(neApplicationFormVO.locationDetails, opportunityReferenceNumber, site.Id);
        createAttachments(neDeliveryRequirementsVO, opportunity.Id);
        createBusinessDetails(neApplicationFormVO, opportunityContactRoles);
        createSiteContact(neApplicationFormVO, site.Id, opportunityContactRoles);
        insertOpportunityContactRoles(opportunityContactRoles, opportunity.Id);

        System.debug('Network Extension Application has been created successfully. Reference Number is ' + opportunityReferenceNumber);
        
        System.enqueueJob(new NE_EmailNotificationQueueHandler(opportunity.Id));
        System.debug('Email Notification for Network Extension Application has been scheduled successfully. Reference Number is ' + opportunityReferenceNumber);

        return JSON.serialize(new CreateApplicationResponse('OK', opportunityReferenceNumber));
    }

    private static Site__c createSite(NE_ApplicationFormVO appForm, Opportunity opportunity) {
        NE_ApplicationFormVO.NE_LocationDetailsVO neLocationDetailsVO = appForm.locationDetails;

        Site__c site = tryToFindExistingSite(neLocationDetailsVO);

        if (site == null) {
            site = createNewSite(neLocationDetailsVO, appForm);
        }

        opportunity.Site__r = site;
        opportunity.Site__c = site.Id;
        opportunity.State__c = site.State_Territory_Code__c;

        return site;
    }

    private static Site__c createNewSite(NE_ApplicationFormVO.NE_LocationDetailsVO neLocationDetailsVO, NE_ApplicationFormVO appForm) {
        Site__c site = new Site__c();

        site.RecordTypeId = SITE_RECORD_TYPES.get('Unverified');
        System.debug('trying to create a new site ' + neLocationDetailsVO.locationId);

        site.Latitude__c = neLocationDetailsVO.latitude;
        site.Longitude__c = neLocationDetailsVO.longitude;
        if (String.isNotBlank(neLocationDetailsVO.streetNumber)) {
            site.Road_Number_1__c = neLocationDetailsVO.streetNumber;
        } else if (String.isNotBlank(appForm.serviceDetails.applicationUniqueId)) {
            site.Road_Number_1__c = appForm.serviceDetails.applicationUniqueId;
        } else {
            site.Road_Number_1__c = 'NA';
        }

        site.Road_Name__c = neLocationDetailsVO.streetName;
        site.Locality_Name__c = neLocationDetailsVO.suburb;
        site.State_Territory_Code__c = neLocationDetailsVO.state;
        site.Post_Code__c = neLocationDetailsVO.postcode;
        site.Description__c = neLocationDetailsVO.notes;
        site.CreatedById = UserInfo.getUserId();

        //TODO: find out what the name should be. Note the length of this field is limited to 80
        List<String> siteNameArguments = new List<String>();
        siteNameArguments.add(site.Road_Number_1__c);
        siteNameArguments.add(site.Road_Name__c);
        siteNameArguments.add(site.Locality_Name__c);
        siteNameArguments.add(site.State_Territory_Code__c);
        siteNameArguments.add(site.Post_Code__c);
        site.Name = String.format('{0} {1}, {2}, {3} {4}', siteNameArguments);
        if (site.Name.length() > 80) {
            site.Name = site.Name.substring(0, 80);
        }

        if (neLocationDetailsVO.isExistingLocation) {
            site.Location_Id__c = neLocationDetailsVO.locationId;
            site.NE_Service_ID_FNN_ULL__c = neLocationDetailsVO.serviceId;
            if (isNotNewLine(neLocationDetailsVO)) {
                site.NE_Copper_Path_ID__c = neLocationDetailsVO.cpi;
            }
            reQualifySite(site);
        }

        insert site;

        return site;
    }

    private static void reQualifySite(Site__c site) {
        String roadNumberBeforeReQualify = site.Road_Number_1__c;
        String stateBeforeReQualify = site.State_Territory_Code__c;
        String suburbBeforeReQualify = site.Locality_Name__c;
        try {
            HelperUtility.executeServiceQualification(site, site.Location_Id__c);
        } catch (Exception e) {
            GlobalUtility.logMessage('Error', NE_Application_CreateController.class.getName(), 'updateSiteInfo', '', 'Error in re-qualifying site', e.getMessage(), 'Failed to re-qualify the site with location ID ' + site.Location_Id__c, e, 0);
        } finally {
            if (String.isBlank(site.Road_Number_1__c)) {
                site.Road_Number_1__c = roadNumberBeforeReQualify;
            }
            if (String.isBlank(site.State_Territory_Code__c)) {
                site.State_Territory_Code__c = stateBeforeReQualify;
            }
            if (String.isBlank(site.Locality_Name__c)) {
                site.Locality_Name__c = suburbBeforeReQualify;
            }
        }
    }

    private static Site__c tryToFindExistingSite(NE_ApplicationFormVO.NE_LocationDetailsVO neLocationDetailsVO) {
        Site__c existingSite = null;
        if (neLocationDetailsVO.isExistingLocation) {
            System.debug('trying to find the existing site ' + neLocationDetailsVO.locationId);
            List<Site__c> matchingSites = [
                    SELECT Id, Name, State_Territory_Code__c
                    FROM Site__c
                    WHERE Location_Id__c = :neLocationDetailsVO.locationId
            ];

            if (matchingSites != null && matchingSites.size() > 0) {
                existingSite = matchingSites.get(0);
                existingSite.NE_Service_ID_FNN_ULL__c = neLocationDetailsVO.serviceId;
                if (isNotNewLine(neLocationDetailsVO)) {
                    existingSite.NE_Copper_Path_ID__c = neLocationDetailsVO.cpi;
                }
                update existingSite;
            }
        }

        return existingSite;
    }

    private static Boolean isNotNewLine(NE_ApplicationFormVO.NE_LocationDetailsVO neLocationDetailsVO) {
        return !'New Line'.equals(neLocationDetailsVO.existingLocationType);
    }

    private static void insertOpportunityContactRoles(List<OpportunityContactRole> opportunityContactRoles, Id opportunityId) {
        for (OpportunityContactRole opportunityContactRole : opportunityContactRoles) {
            opportunityContactRole.OpportunityId = opportunityId;
        }
        insert opportunityContactRoles;
    }

    private static void createSiteContact(NE_ApplicationFormVO neApplicationFormVO, Id siteId, List<OpportunityContactRole> opportunityContactRoles) {
        NE_ApplicationFormVO.NE_SiteContactsVO neSiteContactsVO = neApplicationFormVO.siteContacts;
        if (neSiteContactsVO != null && String.isNotBlank(neSiteContactsVO.abn)) {
            Account siteAccount = accountService.getOrCreateAccount(BUSINESS_END_CUSTOMER, neSiteContactsVO.abn, neSiteContactsVO.companyName, '', '', '');
            Contact siteContact = createBusinessCustomerContact(
                    neSiteContactsVO.firstName,
                    neSiteContactsVO.lastName,
                    neSiteContactsVO.contactNumber,
                    neSiteContactsVO.email,
                    neSiteContactsVO.role,
                    siteAccount.Id
            );

            opportunityContactRoles.add(createOpportunityContactRole(siteContact.Id, SITE_CONTACT, false));

            List<Site_Contacts__c> existingSiteContacts = [SELECT Id FROM Site_Contacts__c WHERE Related_Site__c = :siteId AND Related_Contact__c = :siteContact.Id];
            if (existingSiteContacts.isEmpty()) {
                Site_Contacts__c siteContacts = new Site_Contacts__c();
                siteContacts.Related_Contact__c = siteContact.Id;
                siteContacts.Related_Site__c = siteId;
                insert siteContacts;
            }
        }
    }

    private static void createBusinessDetails(NE_ApplicationFormVO neApplicationFormVO, List<OpportunityContactRole> opportunityContactRoles) {
        NE_ApplicationFormVO.NE_BusinessDetailsVO neBusinessDetailsVO = neApplicationFormVO.businessDetails;
        Account businessAccount = accountService.getOrCreateAccount(BUSINESS_END_CUSTOMER, neBusinessDetailsVO.abn, neBusinessDetailsVO.companyName, neBusinessDetailsVO.email, neBusinessDetailsVO.contactNumber, '');

        NE_ApplicationFormVO.NE_PrimaryContactDetailsVO nePrimaryContactDetailsVO = neApplicationFormVO.primaryContactDetails;
        Contact businessPrimaryContact = createBusinessCustomerContact(nePrimaryContactDetailsVO.firstName,
                nePrimaryContactDetailsVO.lastName,
                nePrimaryContactDetailsVO.contactNumber,
                nePrimaryContactDetailsVO.email,
                nePrimaryContactDetailsVO.role,
                businessAccount.Id);
        opportunityContactRoles.add(createOpportunityContactRole(businessPrimaryContact.Id, BUSINESS_CONTACT, true));

        NE_ApplicationFormVO.NE_SecondaryContactDetailsVO neSecondaryContactDetailsVO = neApplicationFormVO.secondaryContactDetails;
        if (neSecondaryContactDetailsVO != null && String.isNotBlank(neSecondaryContactDetailsVO.email)) {
            Contact businessSecondaryContact = createBusinessCustomerContact(neSecondaryContactDetailsVO.firstName,
                    neSecondaryContactDetailsVO.lastName,
                    neSecondaryContactDetailsVO.contactNumber,
                    neSecondaryContactDetailsVO.email,
                    neSecondaryContactDetailsVO.role,
                    businessAccount.Id);
            opportunityContactRoles.add(createOpportunityContactRole(businessSecondaryContact.Id, BUSINESS_CONTACT, false));
        }
    }

    private static OpportunityContactRole createOpportunityContactRole(Id contactId, String role, Boolean isPrimary) {
        OpportunityContactRole opportunityContactRole = new OpportunityContactRole();
        opportunityContactRole.ContactId = contactId;
        opportunityContactRole.Role = role;
        opportunityContactRole.IsPrimary = isPrimary;
        return opportunityContactRole;
    }

    private static Contract createBillingDetails(NE_ApplicationFormVO neApplicationFormVO) {
        NE_ApplicationFormVO.NE_AccessSeekerBillingVO neAccessSeekerBillingVO = neApplicationFormVO.billDetails;
        NE_ApplicationFormVO.NE_AccessSeekerBillingAddressVO neAccessSeekerBillingAddressVO = neApplicationFormVO.billDetailsAddress;

        Account account = accountService.getOrCreateAccount(RETAIL_SERVICE_PROVIDER, neAccessSeekerBillingVO.abn, neAccessSeekerBillingVO.companyName, '', '', 'Organisation');

        Contact billingContact = createExternalContact(
                neAccessSeekerBillingVO.firstName,
                neAccessSeekerBillingVO.lastName,
                neAccessSeekerBillingVO.contactNumber,
                neAccessSeekerBillingVO.email,
                neAccessSeekerBillingVO.additionalContactNumber,
                account.Id
        );

        Contract accountAddress = new Contract();
        accountAddress.AccountId = account.Id;
        accountAddress.Billing_Contact__r = billingContact;
        accountAddress.Billing_Contact__c = billingContact.Id;
        accountAddress.Dunning_Contact__r = billingContact;
        accountAddress.Dunning_Contact__c = billingContact.Id;

        accountAddress.RecordTypeId = CONTRACT_RECORD_TYPES.get(RECOVERABLE_WORKS);
        accountAddress.BillingCity = neAccessSeekerBillingAddressVO.city;
        accountAddress.BillingPostalCode = neAccessSeekerBillingAddressVO.postcode;
        accountAddress.BillingState = neAccessSeekerBillingAddressVO.state;
        accountAddress.BillingStreet = neAccessSeekerBillingAddressVO.street;
        accountAddress.BillingCountry = 'Australia';

        insert accountAddress;

        return accountAddress;
    }

    private static Contact createBusinessCustomerContact(String firstName, String lastName, String contactNumber, String email, String jobTitle, Id accountId) {
        return contactService.getOrCreateContact(accountId, BUSINESS_END_CUSTOMER, firstName, lastName, contactNumber, email, null, jobTitle);
    }

    private static Contact createExternalContact(String firstName, String lastName, String contactNumber, String email, String additionalContactNumber, Id accountId) {
        return contactService.getOrCreateContact(accountId, EXTERNAL_CONTACT, firstName, lastName, contactNumber, email, additionalContactNumber, null);
    }


    private static void createAttachments(NE_ApplicationFormVO.NE_DeliveryRequirementsVO neDeliveryRequirementsVO, Id opportunityId) {
        if (!neDeliveryRequirementsVO.documentIds.isEmpty()) {
            List<ContentDocument> contentDocuments = [
                    SELECT Title, FileType, FileExtension, LatestPublishedVersion.ContentSize, LatestPublishedVersion.VersionData
                    FROM ContentDocument
                    WHERE Id IN :neDeliveryRequirementsVO.documentIds
            ];

            List<Attachment> attachments = new List<Attachment>();
            for (ContentDocument contentDocument : contentDocuments) {
                Attachment attachment = new Attachment();
                attachment.ParentId = opportunityId;
                attachment.ContentType = contentDocument.FileType;
                attachment.Name = contentDocument.Title + '.' + contentDocument.FileExtension;
                attachment.Body = contentDocument.LatestPublishedVersion.VersionData;
                attachments.add(attachment);
            }
            insert attachments;
        }
    }

    private static void createOpportunityNote(NE_ApplicationFormVO.NE_DeliveryRequirementsVO neDeliveryRequirementsVO, Id opportunityId) {
        if (String.isNotBlank(neDeliveryRequirementsVO.additionalDetails)) {
            createNote(opportunityId, RSP_APPLICATION_NOTE, neDeliveryRequirementsVO.additionalDetails);
        }
    }

    private static void createSiteNote(NE_ApplicationFormVO.NE_LocationDetailsVO locationDetailsVO, String opportunityReferenceNumber, Id siteId) {
        if (String.isNotBlank(locationDetailsVO.notes)) {
            createNote(siteId, opportunityReferenceNumber, locationDetailsVO.notes);
        }
    }

    private static Note createNote(Id parentId, String title, String body) {
        Note note = new Note();
        note.ParentId = parentId;
        note.Title = title;
        note.Body = body;
        note.IsPrivate = false;
        insert note;
        return note;
    }

    private static void setOpportunityDefaultValues(Opportunity opportunity, String siteName, NE_ApplicationFormVO.NE_LocationDetailsVO neLocationDetailsVO) {
        opportunity.RecordTypeId = OPPORTUNITY_RECORD_TYPES.get('Recoverable Works Major/Complex');
        opportunity.Name = siteName;
        opportunity.RW_Type__c = 'Network Extension';
        opportunity.StageName = 'New';
        opportunity.Source__c = 'RSP Channel';
        opportunity.Input_Type__c = 'RSP Direct';
        opportunity.Technology_Type__c = 'FTTN';
        opportunity.Network_Mapping__c = 'Non Premise Location';
        if (neLocationDetailsVO.isExistingLocation) {
            opportunity.Work_Type__c = neLocationDetailsVO.existingLocationType;
        } else {
            opportunity.Work_Type__c = NEW_LOCATION;
        }
    }

    public class CreateApplicationResponse {
        public String status { get; set; }
        public String body { get; set; }

        public CreateApplicationResponse(String status, String body) {
            this.status = status;
            this.body = body;
        }

    }
}