public without sharing class NE_Application_ReviewController {

    @AuraEnabled
    public static NE_ApplicationFormVO viewApplication(String appId) {
        Opportunity opportunity = [
                SELECT Reference_Number__c, StageName, CloseDate, Site__c, State__c, Work_Type__c, Requested_Completion_Date__c, Amount, LastModifiedDate, CreatedBy.Name,
                        NE_Application_Type__c, NE_Application_Unique_Identifier__c, NE_Service_End_Point__c, NE_Service_End_Point_Identifier__c, NE_Traffic_Class_1__c, NE_Traffic_Class_2__c, NE_Traffic_Class_4__c,
                        Site__r.Road_Number_1__c, Site__r.Road_Name__c, Site__r.Locality_Name__c, Site__r.State_Territory_Code__c, Site__r.Post_Code__c, Site__r.Latitude__c, Site__r.Longitude__c, Site__r.Description__c,Site__r.Location_Id__c,Site__r.NE_Copper_Path_ID__c,Site__r.NE_Service_ID_FNN_ULL__c, Site__r.Id, (SELECT Role, IsPrimary, Contact.FirstName, Contact.LastName, Contact.Phone, Contact.Email, Contact.OtherPhone, Contact.Job_Title__c, Contact.Account.ABN__c, Contact.Account.Name, Contact.Account.Phone, Contact.Account.Email__c FROM OpportunityContactRoles), (SELECT Id, Title, Body FROM Notes WHERE Title = :NE_Application_CreateController.RSP_APPLICATION_NOTE), (SELECT Id, Name FROM Attachments),
                        Contract.Account.Name, Contract.Account.ABN__c,
                        Contract.Billing_Contact__r.FirstName, Contract.Billing_Contact__r.LastName,Contract.Billing_Contact__r.Phone,Contract.Billing_Contact__r.OtherPhone,Contract.Billing_Contact__r.Email,
                        Contract.BillingStreet, Contract.BillingCity, Contract.BillingState, Contract.BillingPostalCode, Contract.BillingCountry
                FROM Opportunity
                WHERE Reference_Number__c = :appId
        ];


        NE_ApplicationFormVO appForm = new NE_ApplicationFormVO();
        appForm.userId = UserInfo.getUserId();

        populateApplicationSummary(appForm, appId, opportunity);

        populateLocationDetails(opportunity, appId, appForm);

        populateServiceDetails(appForm, opportunity);

        populateServiceRequirements(appForm, opportunity);

        populateDeliveryRequirements(appForm, opportunity);

        populateAccessSeekerDetails(appForm, opportunity);

        populateBillingDetails(appForm, opportunity);

        populateBusinessDetails(appForm, opportunity);

        populateSiteContactDetails(opportunity, appForm);

        return appForm;
    }

    private static void populateBusinessDetails(NE_ApplicationFormVO appForm, Opportunity opportunity) {
        appForm.businessDetails = new NE_ApplicationFormVO.NE_BusinessDetailsVO();
        appForm.primaryContactDetails = new NE_ApplicationFormVO.NE_PrimaryContactDetailsVO();

        for (OpportunityContactRole opportunityContactRole : opportunity.OpportunityContactRoles) {
            if (NE_Application_CreateController.BUSINESS_CONTACT.equals(opportunityContactRole.Role)) {
                if (opportunityContactRole.IsPrimary) {
                    appForm.primaryContactDetails.role = opportunityContactRole.Contact.Job_Title__c;
                    appForm.primaryContactDetails.firstName = opportunityContactRole.Contact.FirstName;
                    appForm.primaryContactDetails.lastName = opportunityContactRole.Contact.LastName;
                    appForm.primaryContactDetails.contactNumber = opportunityContactRole.Contact.Phone;
                    appForm.primaryContactDetails.email = opportunityContactRole.Contact.Email;

                    appForm.businessDetails.abn = opportunityContactRole.Contact.Account.ABN__c;
                    appForm.businessDetails.companyName = opportunityContactRole.Contact.Account.Name;
                    appForm.businessDetails.contactNumber = opportunityContactRole.Contact.Account.Phone;
                    appForm.businessDetails.email = opportunityContactRole.Contact.Account.Email__c;
                } else {
                    appForm.secondaryContactDetails = new NE_ApplicationFormVO.NE_SecondaryContactDetailsVO();
                    appForm.secondaryContactDetails.role = opportunityContactRole.Contact.Job_Title__c;
                    appForm.secondaryContactDetails.firstName = opportunityContactRole.Contact.FirstName;
                    appForm.secondaryContactDetails.lastName = opportunityContactRole.Contact.LastName;
                    appForm.secondaryContactDetails.contactNumber = opportunityContactRole.Contact.Phone;
                    appForm.secondaryContactDetails.email = opportunityContactRole.Contact.Email;
                }
            }
        }
    }

    private static void populateSiteContactDetails(Opportunity opportunity, NE_ApplicationFormVO appForm) {
        for (OpportunityContactRole opportunityContactRole : opportunity.OpportunityContactRoles) {
            if (NE_Application_CreateController.SITE_CONTACT.equals(opportunityContactRole.Role)) {
                appForm.siteContacts = new NE_ApplicationFormVO.NE_SiteContactsVO();
                appForm.siteContacts.abn = opportunityContactRole.Contact.Account.ABN__c;
                appForm.siteContacts.companyName = opportunityContactRole.Contact.Account.Name;

                appForm.siteContacts.role = opportunityContactRole.Contact.Job_Title__c;
                appForm.siteContacts.firstName = opportunityContactRole.Contact.FirstName;
                appForm.siteContacts.lastName = opportunityContactRole.Contact.LastName;
                appForm.siteContacts.contactNumber = opportunityContactRole.Contact.Phone;
                appForm.siteContacts.email = opportunityContactRole.Contact.Email;
                break;
            }
        }
    }

    private static void populateBillingDetails(NE_ApplicationFormVO appForm, Opportunity opportunity) {
        appForm.billDetails = new NE_ApplicationFormVO.NE_AccessSeekerBillingVO();
        appForm.billDetails.abn = opportunity.Contract.Account.ABN__c;
        appForm.billDetails.companyName = opportunity.Contract.Account.Name;

        appForm.billDetails.firstName = opportunity.Contract.Billing_Contact__r.FirstName;
        appForm.billDetails.lastName = opportunity.Contract.Billing_Contact__r.LastName;
        appForm.billDetails.contactNumber = opportunity.Contract.Billing_Contact__r.Phone;
        appForm.billDetails.additionalContactNumber = opportunity.Contract.Billing_Contact__r.OtherPhone;
        appForm.billDetails.email = opportunity.Contract.Billing_Contact__r.Email;

        appForm.billDetailsAddress = new NE_ApplicationFormVO.NE_AccessSeekerBillingAddressVO();
        appForm.billDetailsAddress.street = opportunity.Contract.BillingStreet;
        appForm.billDetailsAddress.city = opportunity.Contract.BillingCity;
        appForm.billDetailsAddress.state = opportunity.Contract.BillingState;
        appForm.billDetailsAddress.postcode = opportunity.Contract.BillingPostalCode;
    }

    private static void populateAccessSeekerDetails(NE_ApplicationFormVO appForm, Opportunity opportunity) {
        appForm.asDetails = new NE_ApplicationFormVO.NE_AccessSeekerVO();

        appForm.asDetails.asId = new NE_AccessSeekerController().getAccessSeekerId();
        for (OpportunityContactRole opportunityContactRole : opportunity.OpportunityContactRoles) {
            if (NE_Application_CreateController.RETAIL_SERVICE_PROVIDER_RSP.equals(opportunityContactRole.Role)) {
                appForm.asDetails.firstName = opportunityContactRole.Contact.FirstName;
                appForm.asDetails.lastName = opportunityContactRole.Contact.LastName;
                appForm.asDetails.contactNumber = opportunityContactRole.Contact.Phone;
                appForm.asDetails.additionalContactNumber = opportunityContactRole.Contact.OtherPhone;
                appForm.asDetails.email = opportunityContactRole.Contact.Email;
                break;
            }
        }
    }

    private static void populateDeliveryRequirements(NE_ApplicationFormVO appForm, Opportunity opportunity) {
        appForm.deliveryRequirements = new NE_ApplicationFormVO.NE_DeliveryRequirementsVO();
        appForm.deliveryRequirements.proposedProjectDeliveryDate = opportunity.Requested_Completion_Date__c;
        if (opportunity.Notes != null && opportunity.Notes.size() > 0) {
            appForm.deliveryRequirements.additionalDetails = opportunity.Notes.get(0).Body;
        }
    }

    private static void populateServiceRequirements(NE_ApplicationFormVO appForm, Opportunity opportunity) {
        appForm.serviceRequirements = new NE_ApplicationFormVO.NE_ServiceRequirementsVO();
        appForm.serviceRequirements.trafficClass1 = opportunity.NE_Traffic_Class_1__c;
        appForm.serviceRequirements.trafficClass2 = opportunity.NE_Traffic_Class_2__c;
        appForm.serviceRequirements.trafficClass4 = opportunity.NE_Traffic_Class_4__c;
    }

    private static void populateServiceDetails(NE_ApplicationFormVO appForm, Opportunity opportunity) {
        appForm.serviceDetails = new NE_ApplicationFormVO.NE_ServiceDetailsVO();
        appForm.serviceDetails.applicationType = opportunity.NE_Application_Type__c;
        appForm.serviceDetails.applicationUniqueId = opportunity.NE_Application_Unique_Identifier__c;
        appForm.serviceDetails.serviceEndPoint = opportunity.NE_Service_End_Point__c;
        appForm.serviceDetails.serviceEndPointId = opportunity.NE_Service_End_Point_Identifier__c;
    }

    private static void populateLocationDetails(Opportunity opportunity, String appId, NE_ApplicationFormVO appForm) {
        NE_ApplicationFormVO.NE_LocationDetailsVO locDetails = new NE_ApplicationFormVO.NE_LocationDetailsVO();
        if (NE_Application_CreateController.NEW_LOCATION.equals(opportunity.Work_Type__c)) {
            locDetails.isExistingLocation = false;
        } else {
            locDetails.isExistingLocation = true;
            locDetails.existingLocationType = opportunity.Work_Type__c;
        }

        locDetails.isValidLatLong = true;
        locDetails.latitude = opportunity.Site__r.Latitude__c;
        locDetails.longitude = opportunity.Site__r.Longitude__c;
        locDetails.streetNumber = opportunity.Site__r.Road_Number_1__c;
        locDetails.streetName = opportunity.Site__r.Road_Name__c;
        locDetails.suburb = opportunity.Site__r.Locality_Name__c;
        locDetails.state = opportunity.Site__r.State_Territory_Code__c;
        locDetails.postcode = opportunity.Site__r.Post_Code__c;

        List<Note> siteNotes = [SELECT Title, Body FROM Note WHERE ParentId = :opportunity.Site__r.Id AND Title = :appId];
        if (siteNotes != null && siteNotes.size() > 0) {
            locDetails.notes = siteNotes.get(0).Body;
        }

        locDetails.locationId = opportunity.Site__r.Location_Id__c;
        locDetails.cpi = opportunity.Site__r.NE_Copper_Path_ID__c;
        locDetails.serviceId = opportunity.Site__r.NE_Service_ID_FNN_ULL__c;
        appForm.locationDetails = locDetails;
    }

    private static void populateApplicationSummary(NE_ApplicationFormVO appForm, String appId, Opportunity opportunity) {
        appForm.applicationSummary = new NE_ApplicationFormVO.NE_ApplicationSummaryVO();
        appForm.applicationSummary.applicationNumber = appId;
        appForm.applicationSummary.quoteStatus = NE_Application_SearchController.OPP_STATUS_MAPPING_DB_TO_UI.get(opportunity.StageName);
        appForm.applicationSummary.updatedDate = opportunity.LastModifiedDate.date();
        appForm.applicationSummary.quote = opportunity.Amount;
        appForm.applicationSummary.createdByName = opportunity.CreatedBy.Name;
    }

}