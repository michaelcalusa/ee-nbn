@IsTest
private class NE_Application_ReviewControllerTest {
    @IsTest
    private static void shouldPopulateApplicationSummaryFromOpportunity() {
        Test.startTest();

        User commUser = DF_TestData.createDFCommUser();

        System.runAs(commUser) {
            Opportunity opportunity = createOpportunity();

            NE_ApplicationFormVO neApplicationFormVO = NE_Application_ReviewController.viewApplication(opportunity.Reference_Number__c);

            Assert.equals(commUser.Id, neApplicationFormVO.userId);

            verifyApplicationSummary(neApplicationFormVO, opportunity);
        }

        Test.stopTest();
    }

    @IsTest
    private static void shouldPopulateLocationDetailsFromOpportunity() {
        Test.startTest();

        User commUser = DF_TestData.createDFCommUser();

        System.runAs(commUser) {
            Opportunity opportunity = createOpportunity();

            NE_ApplicationFormVO neApplicationFormVO = NE_Application_ReviewController.viewApplication(opportunity.Reference_Number__c);

            verifyLocationDetails(neApplicationFormVO, opportunity);
            Assert.equals(false, neApplicationFormVO.locationDetails.isExistingLocation);

            opportunity.Work_Type__c = 'Migration';
            update opportunity;

            neApplicationFormVO = NE_Application_ReviewController.viewApplication(opportunity.Reference_Number__c);
            Assert.equals(true, neApplicationFormVO.locationDetails.isExistingLocation);
            Assert.equals('Migration', neApplicationFormVO.locationDetails.existingLocationType);
        }

        Test.stopTest();
    }

    @IsTest
    private static void shouldPopulateServiceDetailsFromOpportunity() {
        Test.startTest();

        User commUser = DF_TestData.createDFCommUser();

        System.runAs(commUser) {
            Opportunity opportunity = createOpportunity();

            NE_ApplicationFormVO neApplicationFormVO = NE_Application_ReviewController.viewApplication(opportunity.Reference_Number__c);

            NE_ApplicationFormVO.NE_ServiceDetailsVO neServiceDetailsVO = neApplicationFormVO.serviceDetails;
            Assert.equals(opportunity.NE_Application_Type__c, neServiceDetailsVO.applicationType);
            Assert.equals(opportunity.NE_Application_Unique_Identifier__c, neServiceDetailsVO.applicationUniqueId);
            Assert.equals(opportunity.NE_Service_End_Point__c, neServiceDetailsVO.serviceEndPoint);
            Assert.equals(opportunity.NE_Service_End_Point_Identifier__c, neServiceDetailsVO.serviceEndPointId);
        }

        Test.stopTest();
    }

    @IsTest
    private static void shouldPopulateServiceRequirementsFromOpportunity() {
        Test.startTest();

        User commUser = DF_TestData.createDFCommUser();

        System.runAs(commUser) {
            Opportunity opportunity = createOpportunity();

            NE_ApplicationFormVO neApplicationFormVO = NE_Application_ReviewController.viewApplication(opportunity.Reference_Number__c);

            NE_ApplicationFormVO.NE_ServiceRequirementsVO neServiceRequirementsVO = neApplicationFormVO.serviceRequirements;
            Assert.equals(neServiceRequirementsVO.trafficClass1, opportunity.NE_Traffic_Class_1__c);
            Assert.equals(neServiceRequirementsVO.trafficClass2, opportunity.NE_Traffic_Class_2__c);
            Assert.equals(neServiceRequirementsVO.trafficClass4, opportunity.NE_Traffic_Class_4__c);
        }

        Test.stopTest();
    }

    @IsTest
    private static void shouldPopulateDeliveryRequirementsFromOpportunity() {
        Test.startTest();

        User commUser = DF_TestData.createDFCommUser();

        System.runAs(commUser) {
            NE_ApplicationFormVO neApplicationFormVO = NE_Application_CreateControllerTest.createApplicationForm();
            neApplicationFormVO.deliveryRequirements.additionalDetails = 'urgent application';
            String referenceNumber = ((NE_Application_CreateController.CreateApplicationResponse)JSON.deserialize(NE_Application_CreateController.createApplication(JSON.serialize(neApplicationFormVO)), NE_Application_CreateController.CreateApplicationResponse.class)).body;
            Opportunity opportunity = NE_Application_CreateControllerTest.getOpportunity(referenceNumber);
            neApplicationFormVO = NE_Application_ReviewController.viewApplication(referenceNumber);

            NE_ApplicationFormVO.NE_DeliveryRequirementsVO deliveryRequirementsVO = neApplicationFormVO.deliveryRequirements;
            Assert.equals(opportunity.Requested_Completion_Date__c, deliveryRequirementsVO.proposedProjectDeliveryDate);
            Assert.equals('urgent application', deliveryRequirementsVO.additionalDetails);
        }

        Test.stopTest();
    }

    @IsTest
    private static void shouldPopulateAccessSeekerDetailsFromOpportunity() {
        Test.startTest();

        User commUser = DF_TestData.createDFCommUser();
        commUser = [SELECT Account.Access_Seeker_ID__c FROM User WHERE Id = :commUser.Id];

        System.runAs(commUser) {
            Opportunity opportunity = createOpportunity();

            NE_ApplicationFormVO neApplicationFormVO = NE_Application_ReviewController.viewApplication(opportunity.Reference_Number__c);

            NE_ApplicationFormVO.NE_AccessSeekerVO accessSeekerVO = neApplicationFormVO.asDetails;
            Assert.equals(accessSeekerVO.asId, commUser.Account.Access_Seeker_ID__c);
            Assert.equals('John', accessSeekerVO.firstName);
            Assert.equals('Smith', accessSeekerVO.lastName);
            Assert.equals('0412345678', accessSeekerVO.contactNumber);
            Assert.equals('abc@test.com', accessSeekerVO.email);
        }

        Test.stopTest();
    }

    @IsTest
    private static void shouldPopulateBillingDetailsFromOpportunity() {
        Test.startTest();

        User commUser = DF_TestData.createDFCommUser();

        System.runAs(commUser) {
            Opportunity opportunity = createOpportunity();

            NE_ApplicationFormVO neApplicationFormVO = NE_Application_ReviewController.viewApplication(opportunity.Reference_Number__c);

            NE_ApplicationFormVO.NE_AccessSeekerBillingVO billingVO = neApplicationFormVO.billDetails;
            Assert.equals('11223491505', billingVO.abn);
            Assert.equals('rich company', billingVO.companyName);
            Assert.equals('Bill', billingVO.firstName);
            Assert.equals('Gates', billingVO.lastName);
            Assert.equals('company@rich.com', billingVO.email);
            Assert.equals('0288888888', billingVO.contactNumber);
            Assert.equals('0312345678', billingVO.additionalContactNumber);

            NE_ApplicationFormVO.NE_AccessSeekerBillingAddressVO billingAddressVO = neApplicationFormVO.billDetailsAddress;
            Assert.equals('123 whatever st', billingAddressVO.street);
            Assert.equals('nowhere', billingAddressVO.city);
            Assert.equals('NSW', billingAddressVO.state);
            Assert.equals('2000', billingAddressVO.postcode);
        }

        Test.stopTest();
    }

    @IsTest
    private static void shouldPopulateBusinessDetailsFromOpportunity() {
        Test.startTest();

        User commUser = DF_TestData.createDFCommUser();

        System.runAs(commUser) {
            NE_ApplicationFormVO neApplicationFormVO = NE_Application_CreateControllerTest.createApplicationForm();
            neApplicationFormVO.secondaryContactDetails = NE_Application_CreateControllerTest.createSecondaryContact();

            String referenceNumber = ((NE_Application_CreateController.CreateApplicationResponse)JSON.deserialize(NE_Application_CreateController.createApplication(JSON.serialize(neApplicationFormVO)), NE_Application_CreateController.CreateApplicationResponse.class)).body;

            neApplicationFormVO = NE_Application_ReviewController.viewApplication(referenceNumber);

            NE_ApplicationFormVO.NE_BusinessDetailsVO businessDetailsVO = neApplicationFormVO.businessDetails;
            Assert.equals('86136533741', businessDetailsVO.abn);
            Assert.equals('nbn co', businessDetailsVO.companyName);

            NE_ApplicationFormVO.NE_PrimaryContactDetailsVO primaryContactDetailsVO = neApplicationFormVO.primaryContactDetails;
            Assert.equals('primary', primaryContactDetailsVO.firstName);
            Assert.equals('contact', primaryContactDetailsVO.lastName);
            Assert.equals('primary@contact.com', primaryContactDetailsVO.email);
            Assert.equals('0299998888', primaryContactDetailsVO.contactNumber);
            Assert.equals('primary contact', primaryContactDetailsVO.role);

            NE_ApplicationFormVO.NE_SecondaryContactDetailsVO secondaryContactDetailsVO = neApplicationFormVO.secondaryContactDetails;
            Assert.equals('secondary', secondaryContactDetailsVO.firstName);
            Assert.equals('contact', secondaryContactDetailsVO.lastName);
            Assert.equals('secondary@contact.com', secondaryContactDetailsVO.email);
            Assert.equals('0211112222', secondaryContactDetailsVO.contactNumber);
            Assert.equals('secondary contact', secondaryContactDetailsVO.role);
        }

        Test.stopTest();
    }

    @IsTest
    private static void shouldPopulateSiteContactFromOpportunity() {
        Test.startTest();

        User commUser = DF_TestData.createDFCommUser();

        System.runAs(commUser) {
            NE_ApplicationFormVO neApplicationFormVO = NE_Application_CreateControllerTest.createApplicationForm();
            neApplicationFormVO.siteContacts = NE_Application_CreateControllerTest.createSiteContact();
            String referenceNumber = ((NE_Application_CreateController.CreateApplicationResponse)JSON.deserialize(NE_Application_CreateController.createApplication(JSON.serialize(neApplicationFormVO)), NE_Application_CreateController.CreateApplicationResponse.class)).body;
            Opportunity opportunity = NE_Application_CreateControllerTest.getOpportunity(referenceNumber);

            neApplicationFormVO = NE_Application_ReviewController.viewApplication(opportunity.Reference_Number__c);

            NE_ApplicationFormVO.NE_SiteContactsVO neSiteContactsVO = neApplicationFormVO.siteContacts;
            Assert.equals('26008672179', neSiteContactsVO.abn);
            Assert.equals('site contact', neSiteContactsVO.companyName);
            Assert.equals('site contact', neSiteContactsVO.role);
            Assert.equals('site', neSiteContactsVO.firstName);
            Assert.equals('contact', neSiteContactsVO.lastName);
            Assert.equals('0266666666', neSiteContactsVO.contactNumber);
            Assert.equals('site@contact.com', neSiteContactsVO.email);
        }

        Test.stopTest();
    }

    private static Opportunity createOpportunity() {
        NE_ApplicationFormVO neApplicationFormVO = NE_Application_CreateControllerTest.createApplicationForm();
        String createApplicationResponse = NE_Application_CreateController.createApplication(JSON.serialize(neApplicationFormVO));
        String referenceNumber = ((NE_Application_CreateController.CreateApplicationResponse)JSON.deserialize(createApplicationResponse, NE_Application_CreateController.CreateApplicationResponse.class)).body;
        return NE_Application_CreateControllerTest.getOpportunity(referenceNumber);
    }

    private static void verifyLocationDetails(NE_ApplicationFormVO neApplicationFormVO, Opportunity opportunity) {
        NE_ApplicationFormVO.NE_LocationDetailsVO locationDetails = neApplicationFormVO.locationDetails;

        Site__c site = opportunity.Site__r;
        Assert.equals(true, locationDetails.isValidLatLong);
        Assert.equals(site.Latitude__c, locationDetails.latitude);
        Assert.equals(site.Longitude__c, locationDetails.longitude);
        Assert.equals(site.Road_Number_1__c, locationDetails.streetNumber);
        Assert.equals(site.Road_Name__c, locationDetails.streetName);
        Assert.equals(site.Locality_Name__c, locationDetails.suburb);
        Assert.equals(site.State_Territory_Code__c, locationDetails.state);
        Assert.equals(site.Post_Code__c, locationDetails.postcode);
        Assert.equals(site.Location_Id__c, locationDetails.locationId);
        Assert.equals(site.NE_Copper_Path_ID__c, locationDetails.cpi);
        Assert.equals(site.NE_Service_ID_FNN_ULL__c, locationDetails.serviceId);
        Assert.equals('beware of dogs', neApplicationFormVO.locationDetails.notes);
    }

    private static void verifyApplicationSummary(NE_ApplicationFormVO neApplicationFormVO, Opportunity opportunity) {
        NE_ApplicationFormVO.NE_ApplicationSummaryVO neApplicationSummaryVO = neApplicationFormVO.applicationSummary;
        Assert.equals(opportunity.Reference_Number__c, neApplicationSummaryVO.applicationNumber);
        Assert.equals(opportunity.LastModifiedDate.date(), neApplicationSummaryVO.updatedDate);
        Assert.equals(opportunity.Amount, neApplicationSummaryVO.quote);
        Assert.equals(opportunity.CreatedBy.Name, neApplicationSummaryVO.createdByName);
        Assert.equals('Submitted', neApplicationFormVO.applicationSummary.quoteStatus);
    }
}