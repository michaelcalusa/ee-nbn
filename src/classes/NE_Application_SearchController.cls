public without sharing class NE_Application_SearchController {

    private static final String RW_TYPE_NETWORK_EXT = 'Network Extension';
    public static final Map<String, String> OPP_STATUS_MAPPING_DB_TO_UI = new Map<String, String>();
    public static final Map<String, Set<String>> OPP_STATUS_MAPPING_UI_TO_DB = new Map<String, Set<String>>();

    static {
        initDb2UiMap();
        initUi2DbMap();
    }

    private static void initDb2UiMap() {
        OPP_STATUS_MAPPING_DB_TO_UI.put('New', 'Submitted');
        OPP_STATUS_MAPPING_DB_TO_UI.put('Qualifying', 'Acknowledged');
        OPP_STATUS_MAPPING_DB_TO_UI.put('Assessing', 'Acknowledged');
        OPP_STATUS_MAPPING_DB_TO_UI.put('Analysing', 'In progress');
        OPP_STATUS_MAPPING_DB_TO_UI.put('Estimating', 'In progress');
        OPP_STATUS_MAPPING_DB_TO_UI.put('Costing', 'In progress');
        OPP_STATUS_MAPPING_DB_TO_UI.put('Approving', 'In progress');
        OPP_STATUS_MAPPING_DB_TO_UI.put('Waiting For Response', 'Pending');
        OPP_STATUS_MAPPING_DB_TO_UI.put('Quoting', 'Quoted');
        OPP_STATUS_MAPPING_DB_TO_UI.put('Closed Won', 'Offer Accepted');
        OPP_STATUS_MAPPING_DB_TO_UI.put('Closed Lost', 'Offer Declined');
    }

    private static void initUi2DbMap() {
        OPP_STATUS_MAPPING_UI_TO_DB.put('Submitted', new Set<String>{
                'New'
        });
        OPP_STATUS_MAPPING_UI_TO_DB.put('Acknowledged', new Set<String>{
                'Qualifying', 'Assessing'
        });
        OPP_STATUS_MAPPING_UI_TO_DB.put('In progress', new Set<String>{
                'Analysing', 'Estimating', 'Costing', 'Approving'
        });
        OPP_STATUS_MAPPING_UI_TO_DB.put('Pending', new Set<String>{
                'Waiting For Response'
        });
        OPP_STATUS_MAPPING_UI_TO_DB.put('Quoted', new Set<String>{
                'Quoting'
        });
        OPP_STATUS_MAPPING_UI_TO_DB.put('Offer Accepted', new Set<String>{
                'Closed Won'
        });
        OPP_STATUS_MAPPING_UI_TO_DB.put('Offer Declined', new Set<String>{
                'Closed Lost'
        });
    }

    /*
	 * searchTerm = applicationId or LocationId
	 * searchDate = date application submitted
	 * searchStatus = maps to Opportunity Status as follows:
	 * 		Submitted 		->	New
     *      Acknowledged 	->	Qualifying, Assessing
     *      In progress		-> 	Analysing, Estimating, Costing, Approving
     *      Pending			-> 	Waiting for Response
     *      Quoted 			->	Quoting 
     *      Offer Accepted 	->	Closed Won
     *      Offer Declined 	->	Closed Lost
     * maxRecords = limit on no. of records returned.
	 */
    @AuraEnabled
    public static String searchApplicationsAsJson(
            String searchTerm,
            String searchDate,
            String searchStatus,
            Integer maxResults) {

        return JSON.serialize(searchApplications(searchTerm, searchDate, searchStatus, maxResults));
    }

    @AuraEnabled
    public static List<ApplicationSearchResult> searchApplications(
            String searchTerm,
            String searchDate,
            String searchStatus,
            Integer maxResults) {

        System.debug('searchApplications params:' +
                '  searchTerm = ' + searchTerm +
                ', searchDate = ' + searchDate +
                ', searchStatus = ' + searchStatus +
                ', maxResults = ' + maxResults);

        List<ApplicationSearchResult> applicationList = new List<ApplicationSearchResult>();
        try {
            String accountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;

            Set<String> userIds = new Map<String, User> ([SELECT Id from User where ContactId IN (SELECT Id from Contact where AccountId = :accountId)]).keySet();
            //List<String> userIds = [SELECT Id from User where ContactId IN (SELECT Id from Contact where AccountId = :accountId)].User;
            System.debug('userIds: ' + JSON.serialize(userIds));

            String queryStr = 'select Reference_Number__c, Site__c, CreatedDate, Work_Type__c, NE_Application_Type__c, Amount, StageName from Opportunity ';
            queryStr += 'where RW_Type__c = :RW_TYPE_NETWORK_EXT and IsDeleted = false and CreatedById in :userIds ';

            String searchLocationId = '';
            if (String.isNotEmpty(searchTerm)) {
                //if locationId searchTerm is provided
                if (searchTerm.startsWithIgnoreCase('LOC')) {
                    searchLocationId = String.escapeSingleQuotes(searchTerm.toUpperCase());
                    //more than 1 site may have the same locationId
                    List<Site__c> siteList = [select Id from Site__c where Location_Id__c = :searchLocationId];
                    if (siteList.isEmpty()) {
                        //System.debug('Returning empty application list as no known sites were found matching location ID ' + searchLocationId);
                        return applicationList;
                    } else {
                        Set<String> siteIds = new Set<String>();
                        for (Site__c site : siteList) {
                            siteIds.add(site.Id);
                        }
                        String locationIdPredicate = 'and Site__c in :siteIds ';
                        queryStr += locationIdPredicate;
                    }
                } else {
                    //if applicationId searchTerm is provided
                    String searchApplicationId = String.escapeSingleQuotes(searchTerm);
                    String applicationIdPredicate = 'and Reference_Number__c = :searchApplicationId ';
                    queryStr += applicationIdPredicate;
                }
            }

            //if searchDate is provided
            if (String.isNotEmpty(searchDate)) {
                DateTime searchDateTime = DateTime.valueOf(searchDate);//searchDate must be 'yyyy-MM-dd HH:mm:ss' format
                DateTime createdDateStart = DateTime.newinstance(searchDateTime.year(), searchDateTime.month(), searchDateTime.day(), 0, 0, 0);
                DateTime createdDateEnd = createdDateStart.addDays(1);
                String submittedDatePredicate = 'and CreatedDate >= :createdDateStart and CreatedDate <= :createdDateEnd ';
                queryStr += submittedDatePredicate;
                //System.debug('searchDate range is: ' + createdDateStart.format() + ' - ' + createdDateEnd.format());
            }

            //if searchStatus is provided
            if (String.isNotEmpty(searchStatus)) {
                String searchStatusEsc = String.escapeSingleQuotes(searchStatus);
                Set<String> statusSet = OPP_STATUS_MAPPING_UI_TO_DB.get(searchStatusEsc);
                if (statusSet != null) {
                    String statusPredicate = 'and StageName IN :statusSet ';
                    queryStr += statusPredicate;
                    //System.debug('searchStatus set is: ' + JSON.serialize(statusSet));
                }
            }

            Integer max = Integer.valueof(maxResults);//SFDC lightning bug - Integer values are not serialized properly
            queryStr += 'ORDER BY CreatedDate DESC LIMIT :max';
            System.debug('Search NE Applications query string is: ' + queryStr);

            List<Opportunity> opportunityList = Database.query(queryStr);

            Map<String, String> opportunitySiteLocationIdMap = new Map<String, String>();

            if (String.isEmpty(searchLocationId)) {
                //find locationIds for all Sites
                Set<String> opportunitySiteIds = new Set<String>();
                for (Opportunity opp : opportunityList) {
                    opportunitySiteIds.add(opp.Site__c);
                }

                List<Site__c> siteList = [select Id, Location_Id__c from Site__c where Id in :opportunitySiteIds];
                for (Site__c s : siteList) {
                    opportunitySiteLocationIdMap.put(s.Id, s.Location_Id__c);
                }
            }

            for (Opportunity opp : opportunityList) {

                //System.debug('Status is: ' + opp.StageName + ' -> ' + OPP_STATUS_MAPPING_DB_TO_UI.get(opp.StageName));

                ApplicationSearchResult result =
                        new ApplicationSearchResult(
                                opp.Reference_Number__c,
                                String.isNotEmpty(searchLocationId) ? searchLocationId : opportunitySiteLocationIdMap.get(opp.Site__c),
                                opp.CreatedDate.format('dd/MM/yyyy HH:mm a'),
                                OPP_STATUS_MAPPING_DB_TO_UI.get(opp.StageName),
                                opp.Work_Type__c,
                                opp.NE_Application_Type__c,
                                opp.Amount);

                applicationList.add(result);
            }

            return applicationList;

        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }


    public class ApplicationSearchResult {
        private String applicationId;
        private String locationId;
        private String submittedDate;
        private String status;
        private String requestType;
        private String applicationType;
        private Decimal quote;

        public ApplicationSearchResult(String applicationId,
                String locationId,
                String submittedDate,
                String status,
                String requestType,
                String applicationType,
                Decimal quote) {
            this.applicationId = applicationId;
            this.locationId = locationId;
            this.submittedDate = submittedDate;
            this.status = status;
            this.requestType = requestType;
            this.applicationType = applicationType;
            this.quote = quote;
        }

        @AuraEnabled public String getApplicationId() {
            return applicationId;
        }

        @AuraEnabled public String getLocationId() {
            return locationId;
        }

        @AuraEnabled public String getSubmittedDate() {
            return submittedDate;
        }

        @AuraEnabled public String getStatus() {
            return status;
        }

        @AuraEnabled public String getRequestType() {
            return requestType;
        }

        @AuraEnabled public String getApplicationType() {
            return applicationType;
        }

        @AuraEnabled public Decimal getQuote() {
            return quote;
        }
    }

}