@isTest
private class NE_Application_SearchController_Test {

    @isTest
    private static void shouldGetSingleOpportunityForUserAccountAndNetworkExtensionUserApplications() {

        User nbn360commUser = createNBN360CommUser();
        Contact nbn360commContact = [SELECT Id, AccountId From Contact WHERE Id = :nbn360commUser.ContactId];

        system.runAs(nbn360commUser) {
            Site__c site11 = insertSite('LOC123456789011');
            Opportunity opp11 = insertOpportunity(nbn360commContact.AccountId, site11, 'Network Extension', 'Closed Won', DateTime.newInstance(2018, 12, 1, 19, 30, 10), '', '');
        }

        User commUser = DF_TestData.createDFCommUser();
        Contact commContact = [SELECT Id, AccountId From Contact WHERE Id = :commUser.ContactId];

        system.runAs(commUser) {

            Site__c site1 = insertSite('LOC123456789012');
            Site__c site2 = insertSite('LOC123456789012');

            insert new List<Opportunity> {
                    createOpportunity(commContact.AccountId, site1, 'Variation', 'New', DateTime.newInstance(2018, 12, 1, 19, 30, 10), '', ''),
                    createOpportunity(commContact.AccountId, site1, 'Network Extension', 'New', DateTime.newInstance(2018, 12, 4, 14, 15, 10), '', '')
            };

            Test.startTest();
            //search all applications
            List<NE_Application_SearchController.ApplicationSearchResult> applicationList =
                    NE_Application_SearchController.searchApplications('', '', '', 5);

            System.debug(JSON.serialize(applicationList));
            Assert.equals(1, applicationList.size());
            Assert.equals('LOC123456789012', applicationList[0].getLocationId());
            Assert.equals('Submitted', applicationList[0].getStatus());
            Assert.equals('04/12/2018 14:15 PM', applicationList[0].getSubmittedDate());
            Assert.equals(99.9, applicationList[0].getQuote());

            //search all applications submitted on 2018-12-01
            applicationList = NE_Application_SearchController.searchApplications('', '2018-12-01 00:00:00', '', 5);
            Assert.equals(0, applicationList.size());

            Test.stopTest();
        }
    }

    @isTest
    private static void shouldGetSingleOpportunityForSelectedDate() {

        User commUser = DF_TestData.createDFCommUser();
        Contact commContact = [SELECT Id, AccountId From Contact WHERE Id = :commUser.ContactId];

        System.runAs(commUser) {

            Site__c site1 = insertSite('LOC123456789012');
            Site__c site2 = insertSite('LOC123456789012');

            insert new List<Opportunity> {
                    createOpportunity(commContact.AccountId, site1, 'Network Extension', 'New', DateTime.newInstance(2018, 12, 1, 19, 30, 10), '', ''),
                    createOpportunity(commContact.AccountId, site1, 'Network Extension', 'New', DateTime.newInstance(2018, 12, 3, 14, 15, 10), '', '')
            };

            Test.startTest();
            //search all applications submitted on 2018-12-01
            List<NE_Application_SearchController.ApplicationSearchResult> applicationList =
                    NE_Application_SearchController.searchApplications('', '2018-12-01 00:00:00', '', 5);

            System.debug(JSON.serialize(applicationList));
            Assert.equals(1, applicationList.size());
            Assert.equals('LOC123456789012', applicationList[0].getLocationId());
            Assert.equals('Submitted', applicationList[0].getStatus());
            Assert.equals('01/12/2018 19:30 PM', applicationList[0].getSubmittedDate());
            Assert.equals(99.9, applicationList[0].getQuote());

            //search all applications submitted on 2018-12-03
            applicationList = NE_Application_SearchController.searchApplications('', '2018-12-03 00:00:00', '', 5);
            Assert.equals(1, applicationList.size());
            Assert.equals('LOC123456789012', applicationList[0].getLocationId());
            Assert.equals('Submitted', applicationList[0].getStatus());
            Assert.equals('03/12/2018 14:15 PM', applicationList[0].getSubmittedDate());
            Assert.equals(99.9, applicationList[0].getQuote());

            test.stopTest();
        }
    }

    @isTest
    private static void shouldGetSingleOpportunityForSelectedStatus() {

        User commUser = DF_TestData.createDFCommUser();
        Contact commContact = [SELECT Id, AccountId From Contact WHERE Id = :commUser.ContactId];



        System.runAs(commUser) {

            Site__c site1 = insertSite('LOC123456789012');

            insert new List<Opportunity> {
                    createOpportunity(commContact.AccountId, site1, 'Network Extension', 'New', DateTime.newInstance(2018, 12, 1, 19, 30, 10), '', ''),
                    createOpportunity(commContact.AccountId, site1, 'Network Extension', 'Qualifying', DateTime.newInstance(2018, 12, 1, 14, 15, 10), '', '')
            };

            Test.startTest();
            List<NE_Application_SearchController.ApplicationSearchResult> applicationList =
                    NE_Application_SearchController.searchApplications('', '2018-12-01 00:00:00', 'Submitted', 5);

            System.debug(JSON.serialize(applicationList));
            Assert.equals(1, applicationList.size());
            Assert.equals('LOC123456789012', applicationList[0].getLocationId());
            Assert.equals('Submitted', applicationList[0].getStatus());
            Assert.equals('01/12/2018 19:30 PM', applicationList[0].getSubmittedDate());
            Assert.equals(99.9, applicationList[0].getQuote());

            applicationList = NE_Application_SearchController.searchApplications('', '2018-12-01 00:00:00', 'Acknowledged', 5);
            Assert.equals(1, applicationList.size());
            Assert.equals('LOC123456789012', applicationList[0].getLocationId());
            Assert.equals('Acknowledged', applicationList[0].getStatus());
            Assert.equals('01/12/2018 14:15 PM', applicationList[0].getSubmittedDate());
            Assert.equals(99.9, applicationList[0].getQuote());

            test.stopTest();
        }
    }

    @isTest
    private static void shouldGetSingleOpportunityForSelectedLocationId() {

        User commUser = DF_TestData.createDFCommUser();
        Contact commContact = [SELECT Id, AccountId From Contact WHERE Id = :commUser.ContactId];

        system.runAs(commUser) {

            Site__c site1 = insertSite('LOC123456789011');
            Site__c site2 = insertSite('LOC123456789012');

            insert new List<Opportunity> {
                    createOpportunity(commContact.AccountId, site1, 'Network Extension', 'New', DateTime.newInstance(2018, 12, 1, 19, 30, 10), '', ''),
                    createOpportunity(commContact.AccountId, site2, 'Network Extension', 'New', DateTime.newInstance(2018, 12, 1, 19, 30, 10), '', '')
            };

            Test.startTest();

            //search all applications submitted on 2018-12-01 at site 1 (LOC123456789011)
            List<NE_Application_SearchController.ApplicationSearchResult> applicationList = NE_Application_SearchController.searchApplications('LOC123456789011', '', '', 5);
            Assert.equals(1, applicationList.size());
            Assert.equals('LOC123456789011', applicationList[0].getLocationId());
            Assert.equals('Submitted', applicationList[0].getStatus());
            Assert.equals('01/12/2018 19:30 PM', applicationList[0].getSubmittedDate());
            Assert.equals(99.9, applicationList[0].getQuote());

            //search all applications submitted on 2018-12-01 at site 2 (LOC123456789012)
            applicationList = NE_Application_SearchController.searchApplications('LOC123456789012', '', '', 5);
            Assert.equals(1, applicationList.size());
            Assert.equals('LOC123456789012', applicationList[0].getLocationId());
            Assert.equals('Submitted', applicationList[0].getStatus());
            Assert.equals('01/12/2018 19:30 PM', applicationList[0].getSubmittedDate());
            Assert.equals(99.9, applicationList[0].getQuote());

            applicationList = NE_Application_SearchController.searchApplications('LOC123456789013', '', '', 5);
            Assert.equals(0, applicationList.size());

            test.stopTest();
        }
    }

    @isTest
    private static void shouldGetSingleOpportunityForSelectedApplicationNumber() {

        User commUser = DF_TestData.createDFCommUser();
        Contact commContact = [SELECT Id, AccountId From Contact WHERE Id = :commUser.ContactId];

        system.runAs(commUser) {

            Site__c site1 = insertSite('LOC123456789011');
            Site__c site2 = insertSite('LOC123456789012');

            insert new List<Opportunity> {
                    createOpportunity(commContact.AccountId, site1, 'Network Extension', 'New', DateTime.newInstance(2018, 12, 1, 19, 30, 10), '', ''),
                    createOpportunity(commContact.AccountId, site1, 'Network Extension', 'New', DateTime.newInstance(2018, 12, 3, 14, 15, 10), '', '')
            };

            test.startTest();

            List<NE_Application_SearchController.ApplicationSearchResult> applicationList =
                    NE_Application_SearchController.searchApplications('', '2018-12-01 00:00:00', 'Submitted', 5);

            System.debug(JSON.serialize(applicationList));
            Assert.equals(1, applicationList.size());

            applicationList = NE_Application_SearchController.searchApplications(applicationList[0].getApplicationId(), '', '', 5);
            Assert.equals(1, applicationList.size());
            Assert.equals('LOC123456789011', applicationList[0].getLocationId());
            Assert.equals('Submitted', applicationList[0].getStatus());
            Assert.equals('01/12/2018 19:30 PM', applicationList[0].getSubmittedDate());
            Assert.equals(99.9, applicationList[0].getQuote());

            test.stopTest();
        }
    }

    @isTest
    private static void shouldGetMultipleOpportunitiesBySearchCriteria() {

        User commUser = DF_TestData.createDFCommUser();
        Contact commContact = [SELECT Id, AccountId From Contact WHERE Id = :commUser.ContactId];

        system.runAs(commUser) {

            Site__c site1 = insertSite('LOC123456789011');
            Site__c site3 = insertSite('LOC123456789013');

            List<Opportunity> opportunities = new List<Opportunity>{
                    createOpportunity(commContact.AccountId, site1, 'Network Extension', 'New', DateTime.newInstance(2018, 12, 1, 19, 30, 10), '', ''),
                    createOpportunity(commContact.AccountId, site1, 'Network Extension', 'Qualifying', DateTime.newInstance(2018, 11, 10, 19, 35, 10), '', ''),
                    createOpportunity(commContact.AccountId, site3, 'Network Extension', 'Assessing', DateTime.newInstance(2018, 12, 1, 16, 30, 10), '', '')
            };
            insert opportunities;

            Test.startTest();

            //first search All NE opportunities for User Account
            List<NE_Application_SearchController.ApplicationSearchResult> applicationList =
                    NE_Application_SearchController.searchApplications('', '', '', 5);
            //System.debug(JSON.serialize(applicationList));
            Assert.equals(3, applicationList.size());

            applicationList = NE_Application_SearchController.searchApplications('LOC123456789011', '', '', 5);
            Assert.equals(2, applicationList.size());

            applicationList = NE_Application_SearchController.searchApplications('', '2018-12-01 00:00:00', '', 5);
            Assert.equals(2, applicationList.size());

            applicationList = NE_Application_SearchController.searchApplications('', '', 'Acknowledged', 5);
            Assert.equals(2, applicationList.size());

            applicationList = NE_Application_SearchController.searchApplications('LOC123456789011', '2018-11-10 00:00:00', 'Acknowledged', 5);
            Assert.equals(1, applicationList.size());

            test.stopTest();
        }
    }

    @isTest
    private static void shouldGetMultipleOpportunitiesForMyRSP() {

        // USER 1 - RSP 1
        //User commUser = DF_TestData.createDFCommUser();
        //Contact commContact = [SELECT Id, AccountId From Contact WHERE Id = :commUser.ContactId];

        // USER 2 - RSP 2
        Account account2 = createAccount('ASI000000555519');
        User commUser2 = createNECommUser(account2, 'tester1@t2.com');
        Contact commContact2 = [SELECT Id, AccountId From Contact WHERE Id = :commUser2.ContactId];

        // USER 3 - RSP 3 - shares account with above
        User commUser3 = createNECommUser(account2, 'tester2@t2.com');
        Contact commContact3 = [SELECT Id, AccountId From Contact WHERE Id = :commUser3.ContactId];

        system.runAs(commUser2) {

            Site__c site1 = insertSite('LOC123456789011');
            Site__c site3 = insertSite('LOC123456789013');
            insert new List<Opportunity> {
                    createOpportunity(commContact2.AccountId, site1, 'Network Extension', 'New', DateTime.newInstance(2018, 12, 1, 19, 30, 10), '', ''),
                    createOpportunity(commContact2.AccountId, site1, 'Network Extension', 'Qualifying', DateTime.newInstance(2018, 11, 10, 19, 35, 10), '', '')
            };
        }
        system.runAs(commUser3) {

            Site__c site1 = insertSite('LOC123456789014');
            insertOpportunity(commContact3.AccountId, site1, 'Network Extension', 'New', DateTime.newInstance(2018, 12, 1, 19, 30, 10), '', '');
        }

        system.runAs(commUser2) {
            test.startTest();

            //first search All NE opportunities for User Account
            List<NE_Application_SearchController.ApplicationSearchResult> applicationList =
                    NE_Application_SearchController.searchApplications('', '', '',5);
            //System.debug(JSON.serialize(applicationList));
            Assert.equals(3, applicationList.size());

            applicationList = NE_Application_SearchController.searchApplications('LOC123456789011', '', '', 5);
            Assert.equals(2, applicationList.size());

            applicationList = NE_Application_SearchController.searchApplications('', '2018-12-01 00:00:00', '', 5);
            Assert.equals(2, applicationList.size());

            applicationList = NE_Application_SearchController.searchApplications('', '', 'Acknowledged', 5);
            Assert.equals(1, applicationList.size());

            applicationList = NE_Application_SearchController.searchApplications('LOC123456789011', '2018-11-10 00:00:00', 'Acknowledged', 5);
            Assert.equals(1, applicationList.size());
            test.stopTest();

        }

    }


    @isTest
    private static void shouldGetMultipleOpportunitiesAsJsonString() {

        User commUser = DF_TestData.createDFCommUser();
        Contact commContact = [SELECT Id, AccountId From Contact WHERE Id = :commUser.ContactId];

        system.runAs(commUser) {

            Site__c site1 = insertSite('LOC123456789011');
            Site__c site3 = insertSite('LOC123456789013');

            List<Opportunity> opportunities = new List<Opportunity>{
                    createOpportunity(commContact.AccountId, site1, 'Network Extension', 'New', DateTime.newInstance(2018, 12, 1, 19, 30, 10), 'Migration', 'Traffic Light + Traffic Control Unit (TCU)'),
                    createOpportunity(commContact.AccountId, site1, 'Network Extension', 'Qualifying', DateTime.newInstance(2018, 11, 10, 09, 35, 10), 'Transition', 'Traffic Cameras (Speed or Red-light Camera)'),
                    createOpportunity(commContact.AccountId, site3, 'Network Extension', 'Assessing', DateTime.newInstance(2018, 12, 1, 16, 30, 10), 'New Line', 'Applications Tranche 1 product launch March 2019 (soft launch)'),
                    createOpportunity(commContact.AccountId, site3, 'Network Extension', 'New', DateTime.newInstance(2018, 11, 1, 15, 30, 10), 'New Location', 'Tram Stop Electronic Signage (time table)')
            };
            insert opportunities;

            Test.startTest();

            //first get All opportunities
            List<NE_Application_SearchController.ApplicationSearchResult> applicationList =
                    NE_Application_SearchController.searchApplications('', '', '', 5);

            System.debug(JSON.serialize(applicationList));
            Assert.equals(4, applicationList.size());

            String jsonResult = NE_Application_SearchController.searchApplicationsAsJson('', '', '', 5);

            //Assert ORDER BY CreatedDate DESC
            Assert.equals(JSON.serialize(applicationList), jsonResult);
            Assert.equals('LOC123456789011', applicationList[0].getLocationId());
            Assert.equals('Submitted', applicationList[0].getStatus());
            Assert.equals('01/12/2018 19:30 PM', applicationList[0].getSubmittedDate());
            Assert.equals(99.9, applicationList[0].getQuote());
            Assert.equals('Migration', applicationList[0].getRequestType());
            Assert.equals('Traffic Light + Traffic Control Unit (TCU)', applicationList[0].getApplicationType());

            Assert.equals('LOC123456789013', applicationList[1].getLocationId());
            Assert.equals('Acknowledged', applicationList[1].getStatus());
            Assert.equals('01/12/2018 16:30 PM', applicationList[1].getSubmittedDate());
            Assert.equals(99.9, applicationList[1].getQuote());
            Assert.equals('New Line', applicationList[1].getRequestType());
            Assert.equals('Applications Tranche 1 product launch March 2019 (soft launch)', applicationList[1].getApplicationType());

            Assert.equals('LOC123456789011', applicationList[2].getLocationId());
            Assert.equals('Acknowledged', applicationList[2].getStatus());
            Assert.equals('10/11/2018 09:35 AM', applicationList[2].getSubmittedDate());
            Assert.equals(99.9, applicationList[2].getQuote());
            Assert.equals('Transition', applicationList[2].getRequestType());
            Assert.equals('Traffic Cameras (Speed or Red-light Camera)', applicationList[2].getApplicationType());

            Assert.equals('LOC123456789013', applicationList[3].getLocationId());
            Assert.equals('Submitted', applicationList[3].getStatus());
            Assert.equals('01/11/2018 15:30 PM', applicationList[3].getSubmittedDate());
            Assert.equals(99.9, applicationList[3].getQuote());
            Assert.equals('New Location', applicationList[3].getRequestType());
            Assert.equals('Tram Stop Electronic Signage (time table)', applicationList[3].getApplicationType());


            test.stopTest();
        }
    }


    private static User createNBN360CommUser() {
        Profile commProfile = [
                SELECT Id
                FROM Profile
                WHERE Name = 'NBN360 Access Seeker Community'
        ];

        Account commAcct = new Account();
        commAcct.Name = 'NBN360 RSPTest Acct';
        commAcct.Access_Seeker_ID__c = 'ASI500050005000';
        insert commAcct;

        Contact commContact = new Contact();
        commContact.LastName = 'testContact';
        commContact.AccountId = commAcct.Id;
        insert commContact;

        User commUser = new User();
        commUser.Alias = 'test123';
        commUser.Email = 'test123@noemail.com';
        commUser.Emailencodingkey = 'ISO-8859-1';
        commUser.Lastname = 'Testing';
        commUser.Languagelocalekey = 'en_US';
        commUser.Localesidkey = 'en_AU';
        commUser.Profileid = commProfile.Id;
        commUser.IsActive = true;
        commUser.ContactId = commContact.Id;
        commUser.Timezonesidkey = 'Australia/Sydney';
        commUser.Username = 'nbn360tester@noemail.com.test';

        insert commUser;

        return commUser;
    }

    private static Site__c insertSite(String locationId) {
        Site__c site = new Site__c();
        site.Name = 'created-by-apex-test-' + Math.abs(Crypto.getRandomInteger());
        site.Location_Id__c = locationId;

        insert site;
        return site;
    }

    private static Opportunity insertOpportunity(String accountId,
            Site__c site,
            String rwType,
            String stageName,
            Datetime createdDate,
            String requestType,
            String applicationType
    ) {
        Opportunity opportunity = createOpportunity( accountId, site, rwType, stageName, createdDate, requestType, applicationType);
        insert opportunity;
        return opportunity;
    }

    private static Opportunity createOpportunity(String accountId,
            Site__c site,
            String rwType,
            String stageName,
            Datetime createdDate,
            String requestType,
            String applicationType
    ) {
        Opportunity opp = new Opportunity();
        opp.Name = 'created-by-apex-test-' + Math.abs(Crypto.getRandomInteger());
        opp.AccountId = accountId;
        opp.RW_Type__c = rwType;
        opp.Site__c = site.Id;
        opp.CreatedDate = createdDate;
        opp.CloseDate = Date.today().addDays(7);
        //Quoting or Closed Won only only if Approving Status is Internal Approval Received
        if (stageName.equals('Quoting') || stageName.equals('Closed Won')) {
            opp.Approving_Status__c = 'Internal Approval Received';
        }
        opp.StageName = stageName;
        if (stageName.equals('Closed Won')) {
            opp.Closed_Reason_Codes__c = 'Work completed/ Closed';
        } else if (stageName.equals('Closed Lost')) {
            opp.Closed_Reason_Codes__c = 'Customer Rejected/ Withdrawn';
        }

        //opp.NE_Application_Type__c = ?
        opp.Work_Type__c = requestType;
        opp.NE_Application_Type__c = applicationType;
        opp.Amount = 99.90;
        return opp;
    }

    private static Quote insertQuote(Opportunity opp, Decimal value) {
        Quote quote = new Quote();
        quote.Name = 'created-by-apex-test-' + Math.abs(Crypto.getRandomInteger());
        quote.OpportunityId = opp.Id;
        //quote.Subtotal = value;
        //quote.GrandTotal = value;

        insert quote;

        QuoteLineItem qli = new QuoteLineItem();
        //qli.Name = 'created-by-apex-test-'+ Math.abs(Crypto.getRandomInteger());
        qli.QuoteId = quote.Id;
        qli.Quantity = 1;
        //qli.ListPrice = 100;
        qli.UnitPrice = 100;
        //qli.Subtotal = 100;
        //qli.TotalPrice = 100;

        insert qli;

        return quote;
    }
    public static Account createAccount(String AccessSeekerId) {
        Account commAcct = new Account();
        commAcct.Name = 'RSPTest Acct 2';
        commAcct.Access_Seeker_ID__c = AccessSeekerId;
        insert commAcct;
        return commAcct;
    }

    // Create df community user and related records
    public static User createNECommUser(Account commAcct, String username) {

        Profile commProfile = [ SELECT Id
        FROM Profile
        WHERE Name = 'DF Partner User'];



        Contact commContact = new Contact();
        commContact.LastName ='testContact2';
        commContact.AccountId = commAcct.Id;
        insert commContact;

        User commUser = new User();
        commUser.Alias = 'test1234';
        commUser.Email = 'test1234@noemail.com';
        commUser.Emailencodingkey = 'ISO-8859-1';
        commUser.Lastname = 'Testing';
        commUser.Languagelocalekey = 'en_US';
        commUser.Localesidkey = 'en_AU';
        commUser.Profileid = commProfile.Id;
        commUser.IsActive = true;
        commUser.ContactId = commContact.Id;
        commUser.Timezonesidkey = 'Australia/Sydney';
        commUser.Username = username;


        insert commUser;



        return commUser;
    }



}