public class NE_EmailNotificationQueueHandler implements Queueable {
    @TestVisible private static final String ORG_WIDE_NO_REPLY_EMAIL_ADDRESS = 'noreply@nbnco.com.au';
    @TestVisible private static final String NE_EMAIL_NOTIFICATION_TEMPLATE_NAME = 'NE Notification';
    
    @TestVisible private static final String DEV_NAME_NE_NOTIFICATION_SANDBOX = 'NE_Notification_Sandbox';
    @TestVisible private static final String DEV_NAME_NE_NOTIFICATION = 'NE_Notification';
    
    private String opportunityId;
    
    public NE_EmailNotificationQueueHandler(String opptyId) {
        this.opportunityId = opptyId;
    }
    
    public void execute(QueueableContext context) {
        try {
            NE_EmailService emailService = (NE_EmailService) ObjectFactory.getInstance(NE_EmailService.class);
            
            String devName = NS_SOABillingEventService.runningInSandbox()? DEV_NAME_NE_NOTIFICATION_SANDBOX : DEV_NAME_NE_NOTIFICATION;
            NE_Email_Setting__mdt emailSettings = emailService.getEmailSettings(devName);
            if (!emailSettings.Is_Active__c) {
                GlobalUtility.logMessage('Info', NE_EmailNotificationQueueHandler.class.getName(), 'execute', opportunityId, '', 
                                         'Network Extension email notification is toggled off, will skip sending emails.', '', null, 0);
                return;
            }
            
            List<String> recipients = getRecipients(emailSettings.Recipient_Addresses__c);
            if (recipients == null || recipients.isEmpty()) {
                GlobalUtility.logMessage('Error', NE_EmailNotificationQueueHandler.class.getName(), 'execute', opportunityId, '', 
                                         'Cannot send network extension notification emails as the recipient list is empty, please check the metadata: ' + devName, '', null, 0);
                return;
            }
            
            OrgWideEmailAddress orgWideEmailAddress = emailService.getOrgWideEmailAddress(ORG_WIDE_NO_REPLY_EMAIL_ADDRESS);
            if (orgWideEmailAddress == null) {
                GlobalUtility.logMessage('Error', NE_EmailNotificationQueueHandler.class.getName(), 'execute', opportunityId, '', 
                                         'Cannot fetch organization wide email address for [' + ORG_WIDE_NO_REPLY_EMAIL_ADDRESS +'], will skip sending emails.', '', null, 0);
                return;            
            }
            
            emailService.sendEmail(orgWideEmailAddress, recipients, NE_EMAIL_NOTIFICATION_TEMPLATE_NAME, opportunityId);
        } catch (Exception e) {
            GlobalUtility.logMessage('Error', NE_EmailNotificationQueueHandler.class.getName(), 'execute', opportunityId, '', 
                                     'Cannot send email notification: ' + e.getMessage(), '', e, 0);
        }
    }
        
    @TestVisible
    private List<String> getRecipients(String recipientAddresses) {
        if (String.isNotBlank(recipientAddresses)) {
            return recipientAddresses.split(';');
        }
        return null;
    }
    
}