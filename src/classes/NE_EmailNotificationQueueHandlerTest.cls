@IsTest
private class NE_EmailNotificationQueueHandlerTest {
	
    @isTest(SeeAllData=false) 
    static void shouldGetRecipients() {
        NE_EmailNotificationQueueHandler handler = new NE_EmailNotificationQueueHandler('id');
        
        List<String> result = handler.getRecipients(null);
        Assert.equals(result, null);
        
        result = handler.getRecipients('   ');
        Assert.equals(result, null);
        
        result = handler.getRecipients('test');
        Assert.notEquals(result, null);
        Assert.equals(result.size(), 1);
        Assert.equals(result.get(0), 'test');
        
        result = handler.getRecipients('test1; test2');
        Assert.notEquals(result, null);
        Assert.equals(result.size(), 2);
        Assert.equals(result.get(0), 'test1');
        Assert.equals(result.get(1), ' test2');
    }
    
    @isTest(SeeAllData=false) 
    static void shouldNotCallSendEmailWhenEmailToggleIsOff() {
        NE_Email_Setting__mdt mockedSetting = new NE_Email_Setting__mdt(Is_Active__c=false, Recipient_Addresses__c='test');
        
        NE_EmailNotificationQueueHandler handler = new NE_EmailNotificationQueueHandler('id');
        
        RecordingStubProvider stubProvider = new RecordingStubProvider(NE_EmailService.class);
        String devName = NS_SOABillingEventService.runningInSandbox()? 
            NE_EmailNotificationQueueHandler.DEV_NAME_NE_NOTIFICATION_SANDBOX : 
        	NE_EmailNotificationQueueHandler.DEV_NAME_NE_NOTIFICATION;
        
        ArgumentMatcher stringMatcher = new StringArgumentMatcher(devName);
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>{stringMatcher};

        stubProvider.given('getEmailSettings').when(argMatcherList).thenReturn(mockedSetting);
        ObjectFactory.setStubProvider(NE_EmailService.class, stubProvider);
        
        Test.startTest();
        handler.execute(null);
        Test.stopTest();
        
        stubProvider.verify('sendEmail', RecordingStubProvider.NEVER);
        stubProvider.verify('getOrgWideEmailAddress', RecordingStubProvider.NEVER);
        StubProvider.verify('getEmailSettings', 1);
    }
    
    @isTest(SeeAllData=false) 
    static void shouldNotCallSendEmailWhenEmailToggleIsOnButRecipientIsBlank() {
        NE_Email_Setting__mdt mockedSetting = new NE_Email_Setting__mdt(Is_Active__c=true, Recipient_Addresses__c=' ');
        
        NE_EmailNotificationQueueHandler handler = new NE_EmailNotificationQueueHandler('id');
        
        RecordingStubProvider stubProvider = new RecordingStubProvider(NE_EmailService.class);
        String devName = NS_SOABillingEventService.runningInSandbox()? 
            NE_EmailNotificationQueueHandler.DEV_NAME_NE_NOTIFICATION_SANDBOX : 
        	NE_EmailNotificationQueueHandler.DEV_NAME_NE_NOTIFICATION;
        
        ArgumentMatcher stringMatcher = new StringArgumentMatcher(devName);
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>{stringMatcher};

        stubProvider.given('getEmailSettings').when(argMatcherList).thenReturn(mockedSetting);
        ObjectFactory.setStubProvider(NE_EmailService.class, stubProvider);
        
        Test.startTest();
        handler.execute(null);
        Test.stopTest();
        
        stubProvider.verify('sendEmail', RecordingStubProvider.NEVER);
        stubProvider.verify('getOrgWideEmailAddress', RecordingStubProvider.NEVER);
        StubProvider.verify('getEmailSettings', 1);
    }
    
    @isTest(SeeAllData=false) 
    static void shouldNotCallSendEmailWhenToggleIsOnAndRecipientIsNotBlankButOrgWideEmailAddressIsNull() {
        NE_Email_Setting__mdt mockedSetting = new NE_Email_Setting__mdt(Is_Active__c=true, Recipient_Addresses__c='test@test.com');
        
        NE_EmailNotificationQueueHandler handler = new NE_EmailNotificationQueueHandler('id');
        
        RecordingStubProvider stubProvider = new RecordingStubProvider(NE_EmailService.class);
        String devName = NS_SOABillingEventService.runningInSandbox()? 
            NE_EmailNotificationQueueHandler.DEV_NAME_NE_NOTIFICATION_SANDBOX : 
        	NE_EmailNotificationQueueHandler.DEV_NAME_NE_NOTIFICATION;
        
        ArgumentMatcher stringMatcher = new StringArgumentMatcher(devName);
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>{stringMatcher};
            
        ArgumentMatcher stringMatcher2 = new StringArgumentMatcher('noreply@nbnco.com.au');
        List<ArgumentMatcher> argMatcherList2 = new List<ArgumentMatcher>{stringMatcher2};

        stubProvider.given('getEmailSettings').when(argMatcherList).thenReturn(mockedSetting);
        stubProvider.given('getOrgWideEmailAddress').when(argMatcherList2).thenReturn(null);
        ObjectFactory.setStubProvider(NE_EmailService.class, stubProvider);
        
        Test.startTest();
        handler.execute(null);
        Test.stopTest();
        
        stubProvider.verify('sendEmail', RecordingStubProvider.NEVER);
        StubProvider.verify('getOrgWideEmailAddress', 1);
        StubProvider.verify('getEmailSettings', 1);
    }
    
    @isTest(SeeAllData=false) 
    static void shouldSendEmail() {
        NE_Email_Setting__mdt mockedSetting = new NE_Email_Setting__mdt(Is_Active__c=true, Recipient_Addresses__c='test@test.com');
        OrgWideEmailAddress orgWideEmailAddress = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'noreply@nbnco.com.au' LIMIT 1];
        
        String opptyId = 'id';
        NE_EmailNotificationQueueHandler handler = new NE_EmailNotificationQueueHandler(opptyId);
        
        RecordingStubProvider stubProvider = new RecordingStubProvider(NE_EmailService.class);
        String devName = NS_SOABillingEventService.runningInSandbox()? 
            NE_EmailNotificationQueueHandler.DEV_NAME_NE_NOTIFICATION_SANDBOX : 
        	NE_EmailNotificationQueueHandler.DEV_NAME_NE_NOTIFICATION;
        
        ArgumentMatcher stringMatcher = new StringArgumentMatcher(devName);
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>{stringMatcher};
            
        ArgumentMatcher stringMatcher2 = new StringArgumentMatcher('noreply@nbnco.com.au');
        List<ArgumentMatcher> argMatcherList2 = new List<ArgumentMatcher>{stringMatcher2};
        
        GenericObjectArgumentCaptor orgAddressCaptor = new GenericObjectArgumentCaptor();
        GenericObjectArgumentCaptor recipientsCaptor = new GenericObjectArgumentCaptor();
        ArgumentMatcher templateNameCaptor = new StringArgumentMatcher(NE_EmailNotificationQueueHandler.NE_EMAIL_NOTIFICATION_TEMPLATE_NAME);
        ArgumentMatcher opptyIdCaptor = new StringArgumentMatcher(opptyId);
        List<ArgumentMatcher> argMatcherList3 = new List<ArgumentMatcher>{orgAddressCaptor, recipientsCaptor, templateNameCaptor, opptyIdCaptor};
        
        stubProvider.given('getEmailSettings').when(argMatcherList).thenReturn(mockedSetting);
        stubProvider.given('getOrgWideEmailAddress').when(argMatcherList2).thenReturn(orgWideEmailAddress);
        stubProvider.given('sendEmail').when(argMatcherList3).thenReturn(null);
        ObjectFactory.setStubProvider(NE_EmailService.class, stubProvider);
        
        Test.startTest();
        handler.execute(null);
        Test.stopTest();
        
        stubProvider.verify('sendEmail', 1, argMatcherList3);
        StubProvider.verify('getOrgWideEmailAddress', 1, argMatcherList2);
        StubProvider.verify('getEmailSettings', 1, argMatcherList);
        
        Assert.equals(orgAddressCaptor.getCaptured(), orgWideEmailAddress);
        Assert.equals(recipientsCaptor.getCaptured(), new List<String>{'test@test.com'});
    }
    
    @isTest(SeeAllData=false) 
    static void shouldNotThrowExceptionOnExecute() {        
        NE_EmailNotificationQueueHandler handler = new NE_EmailNotificationQueueHandler('id');
        
        RecordingStubProvider stubProvider = new RecordingStubProvider(NE_EmailService.class);
        String devName = NS_SOABillingEventService.runningInSandbox()? 
            NE_EmailNotificationQueueHandler.DEV_NAME_NE_NOTIFICATION_SANDBOX : 
        	NE_EmailNotificationQueueHandler.DEV_NAME_NE_NOTIFICATION;
        
        ArgumentMatcher stringMatcher = new StringArgumentMatcher(devName);
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>{stringMatcher};

        stubProvider.given('getEmailSettings').when(argMatcherList).thenThrow(new CustomException('test custom exception'));
        ObjectFactory.setStubProvider(NE_EmailService.class, stubProvider);
        
        Test.startTest();
        
        try{
            handler.execute(null);
        } catch (Exception e){
        	Assert.fail('should not come here.');    
        }
        
        Test.stopTest();
        
        stubProvider.verify('sendEmail', RecordingStubProvider.NEVER);
        stubProvider.verify('getOrgWideEmailAddress', RecordingStubProvider.NEVER);
        stubProvider.verify('getEmailSettings', 1);
    }
}