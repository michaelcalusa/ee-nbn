public class NE_EmailService {
    
    public NE_EmailService() {
        // default constructor, this is needed for 'ObjectFactory'
    }
    
    public Messaging.SingleEmailMessage buildEmail(OrgWideEmailAddress orgWideEmailAddress, List<String> toAddresses, String templateName, String whatId) {
        Map<String, String> whatIdToTemplateNameMap = new Map<String, String>();
        whatIdToTemplateNameMap.put(whatId, templateName);
        List<Messaging.SingleEmailMessage> emails = buildEmails(orgWideEmailAddress, toAddresses, null, null, whatIdToTemplateNameMap);
        return emails.get(0);
    }
    
    public Messaging.SendEmailResult sendEmail(OrgWideEmailAddress orgWideEmailAddress, List<String> toAddresses, String templateName, String whatId) {
    	Map<String, String> whatIdToTemplateNameMap = new Map<String, String>();
        whatIdToTemplateNameMap.put(whatId, templateName);
        List<Messaging.SendEmailResult> results = sendEmails(orgWideEmailAddress, toAddresses, null, null, whatIdToTemplateNameMap, false);
        return results.get(0);
    }
    
    public Messaging.SingleEmailMessage buildEmail(OrgWideEmailAddress orgWideEmailAddress, List<String> toAddresses, List<String> ccAddresses, List<String> bccAddresses, String templateName, String whatId) {
        Map<String, String> whatIdToTemplateNameMap = new Map<String, String>();
        whatIdToTemplateNameMap.put(whatId, templateName);
        List<Messaging.SingleEmailMessage> emails = buildEmails(orgWideEmailAddress, toAddresses, ccAddresses, bccAddresses, whatIdToTemplateNameMap);
        return emails.get(0);
    }
    
    public Messaging.SendEmailResult sendEmail(OrgWideEmailAddress orgWideEmailAddress, List<String> toAddresses, List<String> ccAddresses, List<String> bccAddresses, String templateName, String whatId) {
    	Map<String, String> whatIdToTemplateNameMap = new Map<String, String>();
        whatIdToTemplateNameMap.put(whatId, templateName);
        List<Messaging.SendEmailResult> results = sendEmails(orgWideEmailAddress, toAddresses, ccAddresses, bccAddresses, whatIdToTemplateNameMap, false);
        return results.get(0);
    }
    
    public List<Messaging.SingleEmailMessage> buildEmails(OrgWideEmailAddress orgWideEmailAddress, List<String> toAddresses, List<String> ccAddresses, List<String> bccAddresses, Map<String, String> whatIdToTemplateNameMap) {
        Contact randomContact = DF_OrderEmailService.getRandomContact();
        
        Set<String> templateNames = new Set<String>();
		templateNames.addAll(whatIdToTemplateNameMap.values());
        Map<String, String> templateNameToTemplateIdMap = getEmailTemplateNameToIdMap(templateNames);
        
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        
        Set<String> whatIdSet = whatIdToTemplateNameMap.keySet();
        String templateName = null;
        String templateId = null;
        for (String whatId : whatIdSet) {
            templateName = whatIdToTemplateNameMap.get(whatId);
            templateId = templateNameToTemplateIdMap.get(templateName);
            emails.add(buildEmail(orgWideEmailAddress, toAddresses, ccAddresses, bccAddresses, templateId, randomContact.Id, whatId));
        }
        
        return emails;
    }
    
    public List<Messaging.SendEmailResult> sendEmails(OrgWideEmailAddress orgWideEmailAddress, List<String> toAddresses, List<String> ccAddresses, List<String> bccAddresses, Map<String, String> whatIdToTemplateNameMap, Boolean allOrNothing) {
        List<Messaging.SingleEmailMessage> emails = buildEmails(orgWideEmailAddress, toAddresses, ccAddresses, bccAddresses, whatIdToTemplateNameMap);
        return Messaging.sendEmail(emails, allOrNothing);
    }
    
    // converted into a non-static version so that we can mock
    public OrgWideEmailAddress getOrgWideEmailAddress(String address) {
        return DF_OrderEmailService.getOrgWideEmailAddress(address);
    }
    
    public Map<String, String> getEmailTemplateNameToIdMap(Set<String> templateNames) {
        Map<String, String> templateNameToTemplateIdMap = new Map<String, String>();
        if (templateNames == null || templateNames.isEmpty()) {
            return templateNameToTemplateIdMap;
        }
        
		List<EmailTemplate> templates = [SELECT Id, Name FROM EmailTemplate WHERE Name IN :templateNames];
		for(EmailTemplate template : templates) {
			templateNameToTemplateIdMap.put(template.Name, template.Id);
		}
		return templateNameToTemplateIdMap;
    }
     
    public NE_Email_Setting__mdt getEmailSettings(String devName) {
        return [SELECT Is_Active__c, Recipient_Addresses__c
                FROM   NE_Email_Setting__mdt
                WHERE  DeveloperName = :devName LIMIT 1];
    }
    
    private void forceToRenderEmailBody(Messaging.SingleEmailMessage email) {
        // Force to render email body as Salesforce requires a Contact associated with the email
        Savepoint sp = Database.setSavepoint();
        Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{email});
        Database.rollback(sp);
    }
    
    private Messaging.SingleEmailMessage buildEmail(OrgWideEmailAddress orgWideEmailAddress, List<String> toAddresses, List<String> ccAddresses, List<String> bccAddresses, String templateId, String contactId, String whatId) {
        Messaging.SingleEmailMessage emailWithContactId = setupMailProperties(orgWideEmailAddress, toAddresses, ccAddresses, bccAddresses, templateId, contactId, whatId);
        forceToRenderEmailBody(emailWithContactId);        
        Messaging.SingleEmailMessage emailWithBody = copyEmailBody(emailWithContactId);
        return emailWithBody;
    }
    
    @TestVisible
    private Messaging.SingleEmailMessage copyEmailBody(Messaging.SingleEmailMessage originalEmail) {
        Messaging.SingleEmailMessage emailWithBody = new Messaging.SingleEmailMessage();
        
        emailWithBody.setOrgWideEmailAddressId(originalEmail.getOrgWideEmailAddressId());
        emailWithBody.setToAddresses(originalEmail.getToAddresses());
        emailWithBody.setCcAddresses(originalEmail.getCcAddresses());
        emailWithBody.setBccAddresses(originalEmail.getBccAddresses());
        
        emailWithBody.setPlainTextBody(originalEmail.getPlainTextBody());
        emailWithBody.setHTMLBody(originalEmail.getHTMLBody());
        emailWithBody.setSubject(originalEmail.getSubject());
        
        return emailWithBody;
    }
    
    @TestVisible
    private Messaging.SingleEmailMessage setupMailProperties(OrgWideEmailAddress orgWideEmailAddress, List<String> toAddresses, List<String> ccAddresses, List<String> bccAddresses, String templateId, String contactId, String whatId) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        // Set From
        mail.setOrgWideEmailAddressId(orgWideEmailAddress.Id);
        
        mail.setToAddresses(toAddresses);        
        
        if (ccAddresses != null && !ccAddresses.isEmpty()) {
            mail.setCcAddresses(ccAddresses);
        }
        
        if (bccAddresses != null && !bccAddresses.isEmpty()) {
            mail.setBccAddresses(bccAddresses);
        }
        
        // Set Body
        mail.setTemplateID(templateId);
        mail.setTargetObjectId(contactId);
        mail.setWhatId(whatId);
        mail.setSaveAsActivity(false);
        mail.setUseSignature(false);
        
        return mail;
    }
}