@IsTest
private class NE_EmailServiceTest {
    
    @isTest(SeeAllData=true) 
    static void shouldBuildEmail() {
        OrgWideEmailAddress orgWideEmailAddress= [select id from OrgWideEmailAddress limit 1];
        
        List<String> toAddresses = new List<String> {'toAddress@test.com'};
        List<String> ccAddresses = new List<String> {'ccAddress@test.com'};
        List<String> bccAddresses = null;
        
		// Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
		
        // ensure the group is created for sharing
        EE_DataSharingUtility.findGroup(DF_Constants.NBN_SELECT_GROUP_NAME + acct.Access_Seeker_ID__c);
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.Order_GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55bada';
        insert dfQuote;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        insert dfOrder;

        dfOrder = NS_SOABillingEventUtils.getDFOrder(dfOrder.Id);
        
        String templateName = 'NS Order Acknowledged';
        String whatId = dfOrder.id;
        
        Test.startTest();
        
        NE_EmailService emailService = new NE_EmailService();
        Messaging.SingleEmailMessage mail = emailService.buildEmail(orgWideEmailAddress, toAddresses, ccAddresses, bccAddresses, templateName, whatId);
        
        Assert.equals(mail.getOrgWideEmailAddressId(), orgWideEmailAddress.Id);
        Assert.equals(mail.getToAddresses(), toAddresses);
        Assert.equals(mail.getCcAddresses(), ccAddresses);
        Assert.equals(mail.getBccAddresses(), bccAddresses);
        
        Assert.equals(mail.getTemplateId(), null, 'template id should be null');
        Assert.equals(mail.getWhatId(), null, 'what id should be null');
        Assert.equals(String.isBlank(mail.getPlainTextBody()), true);
        
        String htmlBody = mail.getHtmlBody();
        Assert.equals(String.isBlank(htmlBody), false);
        Assert.equals(htmlBody.contains(acct.Access_Seeker_ID__c), true);
                
        Test.stopTest();
    }
    
    @isTest(SeeAllData=true) 
    static void shouldSendEmail() {
        OrgWideEmailAddress orgWideEmailAddress= [select id from OrgWideEmailAddress WHERE Address = 'noreply@nbnco.com.au' limit 1];
        
        List<String> toAddresses = new List<String> {'toAddress@test.com'};
        List<String> ccAddresses = new List<String> {'ccAddress@test.com'};
        List<String> bccAddresses = null;
        
		// Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
		
        // ensure the group is created for sharing
        EE_DataSharingUtility.findGroup(DF_Constants.NBN_SELECT_GROUP_NAME + acct.Access_Seeker_ID__c);
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.Order_GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55bada';
        insert dfQuote;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        insert dfOrder;

        dfOrder = NS_SOABillingEventUtils.getDFOrder(dfOrder.Id);
        
        String templateName = 'NS Order Acknowledged';
        String whatId = dfOrder.id;
        
        Test.startTest();
        
        NE_EmailService emailService = new NE_EmailService();
        Messaging.SendEmailResult result = emailService.sendEmail(orgWideEmailAddress, toAddresses, ccAddresses, bccAddresses, templateName, whatId);
        
        Assert.equals(result.isSuccess(), true);
            
        Test.stopTest();
    }
    
    @isTest(SeeAllData=true) 
    static void shouldBuildEmailWithoutCC() {
        OrgWideEmailAddress orgWideEmailAddress= [select id from OrgWideEmailAddress limit 1];
        
        List<String> toAddresses = new List<String> {'toAddress@test.com'};
        
		// Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
		
        // ensure the group is created for sharing
        EE_DataSharingUtility.findGroup(DF_Constants.NBN_SELECT_GROUP_NAME + acct.Access_Seeker_ID__c);
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.Order_GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55bada';
        insert dfQuote;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        insert dfOrder;

        dfOrder = NS_SOABillingEventUtils.getDFOrder(dfOrder.Id);
        
        String templateName = 'NS Order Acknowledged';
        String whatId = dfOrder.id;
        
        Test.startTest();
        
        NE_EmailService emailService = new NE_EmailService();
        Messaging.SingleEmailMessage mail = emailService.buildEmail(orgWideEmailAddress, toAddresses, templateName, whatId);
        
        Assert.equals(mail.getOrgWideEmailAddressId(), orgWideEmailAddress.Id);
        Assert.equals(mail.getToAddresses(), toAddresses);
        Assert.equals(mail.getCcAddresses(), null);
        Assert.equals(mail.getBccAddresses(), null);
        
        Assert.equals(mail.getTemplateId(), null, 'template id should be null');
        Assert.equals(mail.getWhatId(), null, 'what id should be null');
        Assert.equals(String.isBlank(mail.getPlainTextBody()), true);
        
        String htmlBody = mail.getHtmlBody();
        Assert.equals(String.isBlank(htmlBody), false);
        Assert.equals(htmlBody.contains(acct.Access_Seeker_ID__c), true);
        
        Test.stopTest();
    }
	
    @isTest(SeeAllData=true) 
    static void shouldSendEmailWithoutCC() {
        OrgWideEmailAddress orgWideEmailAddress= [select id from OrgWideEmailAddress WHERE Address = 'noreply@nbnco.com.au' limit 1];
        
        List<String> toAddresses = new List<String> {'toAddress@test.com'};
        
		// Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
		
        // ensure the group is created for sharing
        EE_DataSharingUtility.findGroup(DF_Constants.NBN_SELECT_GROUP_NAME + acct.Access_Seeker_ID__c);
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.Order_GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55bada';
        insert dfQuote;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        insert dfOrder;

        dfOrder = NS_SOABillingEventUtils.getDFOrder(dfOrder.Id);
        
        String templateName = 'NS Order Acknowledged';
        String whatId = dfOrder.id;
        
        Test.startTest();
        
        NE_EmailService emailService = new NE_EmailService();
        Messaging.SendEmailResult result = emailService.sendEmail(orgWideEmailAddress, toAddresses, templateName, whatId);
        
        Assert.equals(result.isSuccess(), true);
            
        Test.stopTest();
    }
        
    @isTest(SeeAllData=true)
    static void shouldGetEmailSettings() {
        NE_EmailService emailService = new NE_EmailService();
        NE_Email_Setting__mdt settings = emailService.getEmailSettings('NE_Notification_Sandbox');
        
        Assert.notEquals(settings, null);
        Assert.equals(String.isNotBlank(settings.Recipient_Addresses__c), true);
        
        settings = emailService.getEmailSettings('NE_Notification');
        
        Assert.notEquals(settings, null);
        Assert.equals(String.isNotBlank(settings.Recipient_Addresses__c), true);
    }
    
    @isTest(SeeAllData=true)
    static void shouldGetOrgWideEmailAddress() {
		NE_EmailService emailService = new NE_EmailService();
        OrgWideEmailAddress orgAddress = emailService.getOrgWideEmailAddress('noreply@nbnco.com.au');
        
        Assert.notEquals(orgAddress, null);
        Assert.notEquals(orgAddress.Id, null);
    }
    
    @isTest
    static void shouldReturnEmptyMapOnGetEmailTemplateNameToIdMapWhenInputIsEmptyOrNull() {
        NE_EmailService emailService = new NE_EmailService();
        Map<String, String> result = emailService.getEmailTemplateNameToIdMap(null);
        
        Assert.equals(result.isEmpty(), true);
        
        Map<String, String> result2 = emailService.getEmailTemplateNameToIdMap(new Set<String>());
        
        Assert.equals(result2.isEmpty(), true);
    }
    
    @isTest
    static void shouldSetupMailProperties() {
        OrgWideEmailAddress orgWideEmailAddress= [select id from OrgWideEmailAddress limit 1];
        EmailTemplate template = [Select id from EmailTemplate where name = 'Corporate affairs-nbnElectorateEnquiries' limit 1];
        
        Contact contact = new contact( FirstName = 'FirstName' , LastName = 'LastName');
        insert contact;
        
        Case caseObj = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            ContactId = contact.Id
        );
        insert caseObj;
            
        List<String> toAddresses = new List<String> {'toAddress'};
        List<String> ccAddresses = new List<String> {'ccAddress'};
        List<String> bccAddresses = new List<String> {'bccAddress'};
        String templateId = template.id;
        String contactId = contact.id;
        String whatId = caseObj.id;
        
        Test.startTest();
        
        NE_EmailService emailService = new NE_EmailService();
        Messaging.SingleEmailMessage mail = emailService.setupMailProperties(orgWideEmailAddress, toAddresses, ccAddresses, bccAddresses, templateId, contactId, whatId);
        
        Assert.equals(mail.getOrgWideEmailAddressId(), orgWideEmailAddress.Id);
        Assert.equals(mail.getToAddresses(), toAddresses);
        Assert.equals(mail.getCcAddresses(), ccAddresses);
        Assert.equals(mail.getBccAddresses(), bccAddresses);
        
        Assert.equals(mail.getTemplateId(), templateId);
        Assert.equals(mail.getTargetObjectId(), contactId);
        Assert.equals(mail.getWhatId(), whatId);
		Assert.equals(mail.getSaveAsActivity(), false);
        Assert.equals(mail.getUseSignature(), false);
        
        Test.stopTest();
    }
    
    
    @isTest
    static void shouldCopyEmail() {
        OrgWideEmailAddress orgWideEmailAddress= [select id from OrgWideEmailAddress limit 1];
        String subject = 'subject';
        String htmlBody = 'html-body';
        String textBody = 'text-body';
        
        List<String> toAddresses = new List<String> {'toAddress'};
        List<String> ccAddresses = new List<String> {'ccAddress'};
        List<String> bccAddresses = new List<String> {'bccAddress'};

        Messaging.SingleEmailMessage originalEmail = new Messaging.SingleEmailMessage();
        originalEmail.setOrgWideEmailAddressId(orgWideEmailAddress.id);
        originalEmail.setSubject(subject);
        originalEmail.setToAddresses(toAddresses);
        originalEmail.setCcAddresses(ccAddresses);
        originalEmail.setBccAddresses(bccAddresses);
        originalEmail.setHtmlBody(htmlBody);
        originalEmail.setPlainTextBody(textBody);
        
        Test.startTest();
        
        NE_EmailService emailService = new NE_EmailService();
        Messaging.SingleEmailMessage mail = emailService.copyEmailBody(originalEmail);
        
        Assert.equals(mail.getOrgWideEmailAddressId(), originalEmail.getOrgWideEmailAddressId());
        Assert.equals(mail.getSubject(), originalEmail.getSubject());
        Assert.equals(mail.getToAddresses(), originalEmail.getToAddresses());
        Assert.equals(mail.getCcAddresses(), originalEmail.getCcAddresses());
        Assert.equals(mail.getBccAddresses(), originalEmail.getBccAddresses());
        Assert.equals(mail.getHtmlBody(), originalEmail.getHtmlBody());
        Assert.equals(mail.getPlainTextBody(), originalEmail.getPlainTextBody());
        
        Test.stopTest();
    }
}