public without sharing class NE_HomeStatsController {
    private static final String RW_TYPE_NETWORK_EXT = 'Network Extension';
    
    @AuraEnabled
    public static UserStats getStats() {
        try {
            UserStats stats = new UserStats();
            stats.countByUser = countOppByUser();
            stats.countByRsp = countOppByRsp();
            
            return stats;
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        } 
    }
    
    private static Integer countOppByUser(){
        Integer countByUser = 0;
        for (AggregateResult result : [SELECT COUNT(Id) countByUser 
                                       FROM Opportunity
                                       WHERE CreatedById = :UserInfo.getUserId()
                                       AND RW_Type__c = :RW_TYPE_NETWORK_EXT
                                       AND IsDeleted = false]) {
			countByUser += (Integer) result.get('countByUser');
		}
        return countByUser;  
    }
    
    private static Integer countOppByRsp(){

        String accountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;

        Set<String> userIds = new Map<String, User> (
                                                        [SELECT Id
                                                        FROM User
                                                        WHERE ContactId IN
                                                        (SELECT Id from Contact where AccountId = :accountId)]
                                                    ).keySet();

        Integer countByRsp = 0;
        for (AggregateResult result : [SELECT COUNT(Id) countByRsp 
                                       FROM Opportunity
                                       WHERE RW_Type__c = :RW_TYPE_NETWORK_EXT
                                       AND IsDeleted = false
                                       AND CreatedById IN :userIds]) {
			countByRsp += (Integer) result.get('countByRsp');
		}        
        return countByRsp; 
    }
    
    public class UserStats {
        @AuraEnabled public Integer countByUser {get; set;}
        @AuraEnabled public Integer countByRsp {get; set;}
        
        public UserStats() {
            this.countByUser = 0;
            this.countByRsp = 0;
        }
    }
}