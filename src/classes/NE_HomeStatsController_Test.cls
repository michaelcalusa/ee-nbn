@isTest 
private class NE_HomeStatsController_Test {
    
    @isTest
    private static void shouldGetUserStats() {
        Account commAcct = createAccount('ASI500050005000');

        User commUser = createNECommUser(commAcct, 'tester3@noemail.com.test');

        Contact commContact = [SELECT Id, AccountId From Contact WHERE Id = :commUser.ContactId];
        test.startTest();
        system.runAs(commUser) {
            // insert 5 opportunities
            insertNetworkExtOpp(commContact.AccountId, 'Network Extension');
            insertNetworkExtOpp(commContact.AccountId, 'Network Extension');
            insertNetworkExtOpp(commContact.AccountId, 'Variation');
            insertNetworkExtOpp('', 'Network Extension');
            insertNetworkExtOpp('', 'Damage');


            //test.startTest();
            NE_HomeStatsController.UserStats result = NE_HomeStatsController.getStats();
            System.debug(result);
            
            // Assertions
            Assert.equals(3, result.countByRsp, 'Should have 3 opportunities by Rsp but got: ' + result.countByRsp);
            Assert.equals(3, result.countByUser, 'Should have 3 opportunities by User but got: ' + result.countByUser);
            
            //test.stopTest();
        }

        User commUser2 = createNECommUser(commAcct, 'tester4@noemail.com.test');
        Contact commContact2 = [SELECT Id, AccountId From Contact WHERE Id = :commUser2.ContactId];

        system.runAs(commUser2) {
            // insert 1 opportunities
            insertNetworkExtOpp(commContact2.AccountId, 'Network Extension');

            //test.startTest();
            NE_HomeStatsController.UserStats result = NE_HomeStatsController.getStats();
            System.debug(result);

            // Assertions
            Assert.equals(4, result.countByRsp, 'Should have 4 opportunities by Rsp but got: ' + result.countByRsp);
            Assert.equals(1, result.countByUser, 'Should have 1 opportunities by User but got: ' + result.countByUser);

            //test.stopTest();
        }
        test.stopTest();
    }
    
    private static void insertNetworkExtOpp(String accountId, String rwType){
        Opportunity Opp = new Opportunity();
        Opp.Name = 'created-by-apex-test-'+ Math.abs(Crypto.getRandomInteger());
        if(!String.isEmpty(accountId)) {
            Opp.Accountid = accountId;
        }
        Opp.Closedate = System.today();
        Opp.RW_Type__c = rwType;
        Opp.StageName = 'New';
        
        insert Opp;
    }

    public static Account createAccount(String AccessSeekerId) {
        Account commAcct = new Account();
        commAcct.Name = 'RSPTest Acct 2';
        commAcct.Access_Seeker_ID__c = AccessSeekerId;
        insert commAcct;
        return commAcct;
    }

    // Create df community user and related records
    public static User createNECommUser(Account commAcct, String username) {

        Profile commProfile = [ SELECT Id
                                FROM Profile
                                WHERE Name = 'DF Partner User'];



        Contact commContact = new Contact();
        commContact.LastName ='testContact2';
        commContact.AccountId = commAcct.Id;
        insert commContact;

        User commUser = new User();
        commUser.Alias = 'test1234';
        commUser.Email = 'test1234@noemail.com';
        commUser.Emailencodingkey = 'ISO-8859-1';
        commUser.Lastname = 'Testing';
        commUser.Languagelocalekey = 'en_US';
        commUser.Localesidkey = 'en_AU';
        commUser.Profileid = commProfile.Id;
        commUser.IsActive = true;
        commUser.ContactId = commContact.Id;
        commUser.Timezonesidkey = 'Australia/Sydney';
        commUser.Username = username;


        insert commUser;



        return commUser;
    }
    
}