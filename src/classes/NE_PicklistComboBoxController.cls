public class NE_PicklistComboBoxController {

    @AuraEnabled
	public static List<SelectItem> getPicklistData(String objectName, String fieldName) {
    
        List<SelectItem> options = new List<SelectItem>();
    	
        for(PicklistEntry e: ((SObject)(Type.forName('Schema.'+objectName).newInstance())).getSObjectType()
        	.getDescribe().fields.getMap().get(fieldName).getDescribe().getPicklistValues()) {
            
            options.add(new SelectItem(e.getValue(), e.getLabel()));
        }
    	return options;
	}
}