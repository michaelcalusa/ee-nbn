@isTest 
public class NE_PicklistComboBoxController_Test {

    @isTest
    private static void shouldGetPicklistSelectItems() {
        
        List<SelectItem> expectedoptions = new List<SelectItem>();
    	
        for(PicklistEntry e: ((SObject)(Type.forName('Schema.Site__c').newInstance())).getSObjectType()
        	.getDescribe().fields.getMap().get('State_Territory_Code__c').getDescribe().getPicklistValues()) {
            
            expectedoptions.add(new SelectItem(e.getValue(), e.getLabel()));
        }

        List<SelectItem> actualoptions = NE_PicklistComboBoxController.getPicklistData('Site__c', 'State_Territory_Code__c');
        System.assertEquals(true, listsAreEqual(expectedoptions, actualoptions), 'PicklistComboBoxController did not return expected list of SelectItems');
    }
    
    private static boolean listsAreEqual(List<SelectItem> aList, List<SelectItem> bList){
		
        Boolean areEqual = true;
        for(SelectItem expectedItem : aList){
            Boolean itemFound = false;
            for(SelectItem actualItem : bList){
                if(actualItem.value.equals(expectedItem.value) && actualItem.label.equals(expectedItem.label))
                {
                    itemFound = true;
                    break;
                }
            }
           	areEqual &= itemFound;
        }
		return aList.size() == bList.size() && areEqual; 
	} 
}