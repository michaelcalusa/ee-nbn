public class NOCContactMatrix_CX {
 @AuraEnabled
     public static List<String> getAccountsList(string accRecordType){
        List<String> options = new List<String>();
        set<String> setOptions = new set<String>();
        
        String accRecordTypeId = string.valueof(HelperUtility.pullAllRecordTypes('team_member__c').get(accRecordType)); 
        system.debug('==accRecordTypeId=='+accRecordTypeId);
        
         for(team_member__c tm: [select id, Account_Name__c from team_member__c where recordTypeID=: accRecordTypeId])
         {
             setOptions.add(tm.Account_Name__c);
         }
         
       /* for(Account acc: [select id, Name from Account where recordTypeID=: accRecordTypeId])
        {
            setOptions.add(acc.Name);            
        } */       
        options.addall(setOptions);
        options.sort();        
        return options;         
     }
    
     @AuraEnabled
     public static List<contactsWrapper> getContactsList(string accRecordId,String contactType, String fieldToSort, String SortingOrder){
       List<Team_Member__c> lstTeamMembers = new List<Team_Member__c>();
       List<contactsWrapper> lstWrapper = new List<contactsWrapper>();
       map<string, contact> mapContacts = new map<string, contact>();
       map<string, string> mapTeamMemberCusContacts = new map<string, string>();
       map<string, string> mapTeamMemberNBNContacts = new map<string, string>();
       List<Team_Member__c> listTMs = new List<Team_Member__c>();
       set<string> setNBNContacts = new set<string>();
       set<string> setCustomerContacts = new set<string>();
         String contactfieldToSort;
       system.debug('=== accRecordId ==='+ accRecordId); 
         if(fieldToSort == 'Account') {fieldToSort = 'Account_r.Name';}
         else if(fieldToSort == 'Role') {fieldToSort = 'Role__c';}
         else if(fieldToSort == 'RoleType'){fieldToSort = 'Role_Type__c';}
         else if(fieldToSort == 'Name'){
             if(contactType=='customer')
               fieldToSort = 'Customer_Contact__r.Name';
             else
               fieldToSort = 'NBN_Contact__r.Name';
         }
         else if(fieldToSort == 'JobTitle' ){
             if(contactType=='customer')
                fieldToSort = 'Customer_Contact__r.Job_Title__c';
             else
                fieldToSort = 'NBN_Contact__r.Job_Title__c';
         }
         else if(fieldToSort == 'Email' ){
             if(contactType=='customer')
                fieldToSort = 'Customer_Contact__r.Email';
             else
                fieldToSort = 'NBN_Contact__r.Email';
         }
         
       String query = 'SELECT ';       
       query += 'id, Role__c, Role_Type__c, Customer_Contact__c,NBN_Contact__c,Account__c,';
       query += 'NBN_Contact__r.Name,NBN_Contact__r.Job_Title__c, NBN_Contact__r.Email, NBN_Contact__r.Phone, NBN_Contact__r.MobilePhone,';
       query += 'Customer_Contact__r.Name,Customer_Contact__r.Job_Title__c, Customer_Contact__r.Email, Customer_Contact__r.Phone, Customer_Contact__r.MobilePhone, Account__r.Name from Team_Member__c'; 
       query += ' WHERE Account__r.Name=\''+ accRecordId +'\'' ;
         if(contactType!='' && fieldToSort!=''){
             query  = query + ' ORDER BY '+ fieldToSort + ' '+ SortingOrder ;
         }
        system.debug('=== query ==='+ query);  
         List<Team_Member__c> teamMembers =  Database.query(query);  
         //[Select id, Role__c, Role_Type__c, Customer_Contact__c, NBN_Contact__c,Account__r.Name from Team_Member__c where Account__r.Name=: accRecordId Order By Role__c  DESC]
         for(Team_Member__c tm: teamMembers){
             setNBNContacts.add(tm.NBN_Contact__c);  
             setCustomerContacts.add(tm.Customer_Contact__c);
             string ccKeyString = tm.Role__c+'~'+tm.Role_Type__c+'~'+tm.Customer_Contact__c;
             string nbnKeyString = tm.Role__c+'~'+tm.Role_Type__c+'~'+tm.NBN_Contact__c;
             mapTeamMemberCusContacts.put(ccKeyString, tm.Customer_Contact__c);
             mapTeamMemberNBNContacts.put(nbnKeyString, tm.NBN_Contact__c); 
             listTMs.add(tm);
         }
         
         system.debug('==setNBNContacts=='+setNBNContacts);
         system.debug('==setCustomerContacts=='+setCustomerContacts);
        /*  String conQuery = 'SELECT ';       
          conQuery += 'name, id, email, firstName, LastName, Job_Title__c, phone, MobilePhone  from contact where id in: setNBNContacts OR id in: setCustomerContacts' ;
         if(contactType!=''){
             conQuery  = conQuery + ' ORDER BY '+ contactfieldToSort + ' '+ SortingOrder ;
         }
         List<contact> conList =  Database.query(conQuery); */
         // [select name, id, email, firstName, LastName, Job_Title__c, phone, MobilePhone  from contact where id in: setNBNContacts OR id in: setCustomerContacts]
         for(Contact c: [select name, id, email, firstName, LastName, Job_Title__c, phone, MobilePhone  from contact where id in: setNBNContacts OR id in: setCustomerContacts]){
             mapContacts.put(c.id, c);
         }
         system.debug('==mapContacts=='+mapContacts);
         
         for(Team_Member__c tm: listTMs)
         {
             string ccKeyString = tm.Role__c+'~'+tm.Role_Type__c+'~'+tm.Customer_Contact__c;
             if(mapTeamMemberCusContacts.containskey(ccKeyString))
             { 
                 if(tm.Customer_Contact__c!=null)
                 lstWrapper.add(new contactsWrapper('accDropdown',tm.Role__c, tm.Role_Type__c, 'customer', mapContacts.get(mapTeamMemberCusContacts.get(ccKeyString))));
             }
             
             string nbnKeyString = tm.Role__c+'~'+tm.Role_Type__c+'~'+tm.NBN_Contact__c;
             if(mapTeamMemberNBNContacts.containskey(nbnKeyString))
             {   
                  if(tm.NBN_Contact__c!=null)
                 lstWrapper.add(new contactsWrapper('accDropdown',tm.Role__c, tm.Role_Type__c, 'nbn', mapContacts.get(mapTeamMemberNBNContacts.get(nbnKeyString))));
             }             
         }
         system.debug('=== lstWrapper ==='+ lstWrapper); 
         return  lstWrapper;         
     }
    
    
    @AuraEnabled
    public static List<String> searchContact(String searchKeyWord) {
        String searchKey = searchKeyWord + '%';
        Set<ID> setConIds = new Set<ID>();
        List<String> lstSearchedContacts = new List<String>(); 
        Map<String,List<Contact>> mapContactInfo = new Map<String,List<Contact>>();
        try{
            // Search the Contacts
            List<Team_Member__c> lstContacts = [ SELECT NBN_Contact__c, NBN_Contact__r.Name, Customer_Contact__c, Customer_Contact__r.Name FROM Team_Member__c WHERE RecordType.Name = 'Customer' AND (NBN_Contact__r.Name LIKE: searchKey OR Customer_Contact__r.Name LIKE: searchKey) LIMIT 5 ];
            system.debug('---lstContacts----' + lstContacts);
            
            // Map the correct Contacts
            for(Team_Member__c tm : lstContacts){
                if(String.isNotEmpty(tm.NBN_Contact__c) && tm.NBN_Contact__r.Name.toLowerCase().startsWith(searchKeyWord.toLowerCase())){
                    setConIds.add(tm.NBN_Contact__c);
                }
                
                if(String.isNotEmpty(tm.Customer_Contact__c) && tm.Customer_Contact__r.Name.toLowerCase().startsWith(searchKeyWord.toLowerCase())){
                    setConIds.add(tm.Customer_Contact__c);
                }
            }
            system.debug('---ConIds----' + setConIds);
            
            // Return the found Contacts
            //lstSearchedContacts = [ SELECT ID, Name FROM Contact WHERE Id IN: setConIds ORDER BY Name ASC ];
            
            for(Contact c : [ SELECT ID, Name FROM Contact WHERE Id IN: setConIds ]){
                if(mapContactInfo.containsKey(c.Name)){
                    List<Contact> lstTemp = mapContactInfo.get(c.Name);
                    lstTemp.add(c);
                    mapContactInfo.put(c.Name,lstTemp);
                }
                else{
                    mapContactInfo.put(c.Name,new List<Contact>{c});
                }
            }
            system.debug('---mapContactInfo----' + mapContactInfo);
            lstSearchedContacts.addAll(mapContactInfo.keySet());
            system.debug('---mapContactInfo.keySet()----' + mapContactInfo.keySet());
            system.debug('---lstSearchedContacts----' + lstSearchedContacts);
        }Catch(Exception ex){
            system.debug('---Error NOCSearchController - searchContact---' + ex.getMessage());
        }
        
        return lstSearchedContacts;
    }
    
    @AuraEnabled
    public static List<contactsWrapper> getContacts(String strContactName,String contactType, String fieldToSort, String SortingOrder) {
        List<contactsWrapper> lstContactsWrapper = new List<contactsWrapper>();
        system.debug('---strContactName----' + strContactName);
        try{
            // Search the searchterm 
			string rectype = 'Customer';
            
            if(fieldToSort == 'Account') {fieldToSort = 'Account_r.Name';}
            else if(fieldToSort == 'Role') {fieldToSort = 'Role__c';}
            else if(fieldToSort == 'RoleType'){fieldToSort = 'Role_Type__c';}
            else if(fieldToSort == 'Name'){
                if(contactType=='customer')
                    fieldToSort = 'Customer_Contact__r.Name';
                else
                    fieldToSort = 'NBN_Contact__r.Name';
            }
            else if(fieldToSort == 'JobTitle' ){
                if(contactType=='customer')
                    fieldToSort = 'Customer_Contact__r.Job_Title__c';
                else
                    fieldToSort = 'NBN_Contact__r.Job_Title__c';
            }
            else if(fieldToSort == 'Email' ){
                if(contactType=='customer')
                    fieldToSort = 'Customer_Contact__r.Email';
                else
                    fieldToSort = 'NBN_Contact__r.Email';
            }            
                        
			String query = 'SELECT ';       
			query += 'id, Role__c, Role_Type__c, Customer_Contact__c,NBN_Contact__c,Account__c,';
			query += 'NBN_Contact__r.Name,NBN_Contact__r.Job_Title__c, NBN_Contact__r.Email, NBN_Contact__r.Phone, NBN_Contact__r.MobilePhone,';
			query += 'Customer_Contact__r.Name,Customer_Contact__r.Job_Title__c, Customer_Contact__r.Email, Customer_Contact__r.Phone, Customer_Contact__r.MobilePhone, Account__r.Name from Team_Member__c'; 
			query += ' WHERE RecordType.Name = \'' + rectype + '\' AND (NBN_Contact__r.Name =: strContactName OR Customer_Contact__r.Name =: strContactName) ';
			//query += ' WHERE Account__r.Name=\''+ accRecordId +'\'' ;
			if(contactType!='' && fieldToSort!=''){
             query  = query + ' ORDER BY '+ fieldToSort + ' '+ SortingOrder ;
			}
        system.debug('=== query ==='+ query);  
         List<Team_Member__c> teamMembers =  Database.query(query);
            //List<Team_Member__c> lstContacts = [ SELECT Role__c, Role_Type__c, NBN_Contact__r.Name, Customer_Contact__r.Name, NBN_Contact__c, Customer_Contact__c  FROM Team_Member__c WHERE RecordType.Name = 'Customer' AND (NBN_Contact__r.Name =: strContactName OR Customer_Contact__r.Name =: strContactName) ];
            
            Set<ID> setConIds = new Set<ID>();
            for(Team_Member__c tmA : teamMembers){
                if(string.isNotEmpty(tmA.NBN_Contact__c)){
                    setConIds.add(tmA.NBN_Contact__c);
                }
                
                if(string.isNotEmpty(tmA.Customer_Contact__c)){
                    setConIds.add(tmA.Customer_Contact__c);
                }
            }
            
            // Get all realted nbn and customer contacts
            Map<Id,Contact> mapContacts = new Map<Id,Contact>([ SELECT Id, Name, FirstName, LastName, Job_Title__c, Email, Phone, MobilePhone, Account.Name FROM Contact WHERE ID IN: setConIds]);
            
            // Create a search results wrapper
            if(mapContacts.size() > 0){
                for(Team_Member__c tmB : teamMembers){
                    system.debug('---tm----' + tmB.NBN_Contact__r.Name + '--' + tmB.NBN_Contact__c);
                    if(string.isNotEmpty(tmB.NBN_Contact__c) && tmB.NBN_Contact__r.Name.toLowerCase().startsWith(strContactName.toLowerCase()) && mapContacts.containsKey(tmB.NBN_Contact__c)){
                        contactsWrapper cwA = new contactsWrapper('searchBox',tmB.Role__c,tmB.Role_Type__c, 'nbn',mapContacts.get(tmB.NBN_Contact__c));
                        lstContactsWrapper.add(cwA);
                    }
                    
                    if(string.isNotEmpty(tmB.Customer_Contact__c) && tmB.Customer_Contact__r.Name.toLowerCase().startsWith(strContactName.toLowerCase()) && mapContacts.containsKey(tmB.Customer_Contact__c)){
                        contactsWrapper cwB = new contactsWrapper('searchBox',tmB.Role__c,tmB.Role_Type__c, 'customer',mapContacts.get(tmB.Customer_Contact__c));
                        lstContactsWrapper.add(cwB);
                    }
                }
            }

        }Catch(Exception ex){
            system.debug('---Error NOCSearchController - getContacts---' + ex.getMessage());
        }
        
        return lstContactsWrapper;
    }
    
     public class contactsWrapper {
        @AuraEnabled
        public string searchSource { get;set; } 
        @AuraEnabled
        public string roleValue { get;set; }
        @AuraEnabled
        public string roleTypeValue { get;set; }
        @AuraEnabled
        public string conType { get;set; }        
        @AuraEnabled
        public Contact conRecord { get;set; }
        
        public contactsWrapper(String searchSource, String roleValue, String roleTypeValue, String conType, Contact conRecord) {
             this.searchSource = searchSource;
            this.roleValue = roleValue;            
            this.roleTypeValue = roleTypeValue;
            this.conType = conType;            
            this.conRecord = conRecord;
        }   
    }
}