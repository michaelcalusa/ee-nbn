/***************************************************************************************************
    Class Name          : NOCContactMatrix_CX_Test
    Version             : 1.0 
    Created Date        : 14-July-2017
    Description         : Test Class for NOCContactMatrix_CX

    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats       14-July-2017      Test Class for NOCContactMatrix_CX
****************************************************************************************************/ 
@isTest(SeeAllData=false)
public class NOCContactMatrix_CX_Test{
    static testMethod void TestMethod_SearchContact() {
        // Create Customer Account & Contact data
        List<Account> lstAcc = TestDataUtility.getListOfAccounts(1, true);
        List<Contact> lstCon = TestDataUtility.getListOfContact(2, false);
        
        for(contact con : lstCon){
            con.AccountId = lstAcc[0].id;
        }  
        insert lstCon;
        
        // Create Company Account & Contact data
        List<Account> lstCompanyAcc = TestDataUtility.getListOfCompanyAccounts(1, false);
        lstCompanyAcc[0].Name = 'nbn';
        insert lstCompanyAcc;
        List<Contact> lstCompanyCon = TestDataUtility.getListOfCompanyContact(1, false);
        
        for(contact con: lstCompanyCon){
        con.AccountId = lstCompanyAcc[0].id;
        con.Status__c= 'Active';
        }
        insert lstCompanyCon;
        
        //Create Team Member data (Contact Matrix)
        List<Team_Member__c> lstTeamMembers = TestDataUtility.insertListOfTeamMembers(1,true,lstAcc[0],lstCon[0],lstCompanyCon[0]); 
        system.debug('***lstTeamMembers ==>'+ lstTeamMembers );
        
        // Community Contact    
        Profile p = [SELECT Id FROM Profile WHERE Name='NOC Contact Matrix Community'];    
        Contact con = new Contact(LastName ='testCommunityUser',AccountId = lstAcc[0].id);
        insert con;  
        
        User user = new User(alias = 'testcu', email='test123@noemail.com',
        emailencodingkey='UTF-8', lastname='testCommunityUser', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p.id, country='United States',IsActive =true,
        ContactId = con.Id,
        timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        
        Team_Member__c tm = new Team_Member__c(Customer_Contact__c = lstCon[0].Id, NBN_Contact__c = lstCompanyCon[0].ID);
        system.runAs(user) {
        //statements to be executed by this test user.
        
        Test.startTest();
        NOCContactMatrix_CX.getAccountsList('Company');
        
        String strAccountId = lstCompanyAcc[0].Id;
        //AccountsListLightningComponent_CX.getContactsList(strAccountId,'customer','Account','ASC');
        NOCContactMatrix_CX.getContactsList(strAccountId,'customer','Role','ASC');
        NOCContactMatrix_CX.getContactsList(strAccountId,'customer','RoleType','ASC');
        NOCContactMatrix_CX.getContactsList(strAccountId,'customer','Name','ASC');
        NOCContactMatrix_CX.getContactsList(strAccountId,'customer','JobTitle','ASC');
        NOCContactMatrix_CX.getContactsList(strAccountId,'customer','Email','ASC');
        NOCContactMatrix_CX.getContactsList(strAccountId,'nbn','Name','ASC');
        NOCContactMatrix_CX.getContactsList(strAccountId,'nbn','JobTitle','ASC');
        NOCContactMatrix_CX.getContactsList(strAccountId,'nbn','Email','ASC');
        
        NOCContactMatrix_CX.getContacts('John','customer','Account','ASC');
        NOCContactMatrix_CX.getContacts('John','customer','Role','ASC');
        NOCContactMatrix_CX.getContacts('John','customer','RoleType','ASC');
        NOCContactMatrix_CX.getContacts('John','customer','Name','ASC');
        NOCContactMatrix_CX.getContacts('John','customer','JobTitle','ASC');
        NOCContactMatrix_CX.getContacts('John','customer','Email','ASC');
        NOCContactMatrix_CX.getContacts('John','nbn','Name','ASC');
        NOCContactMatrix_CX.getContacts('John','nbn','JobTitle','ASC');
        NOCContactMatrix_CX.getContacts('John','nbn','Email','ASC');
        
        NOCContactMatrix_CX.searchContact('John');
         
        Test.stopTest();
    }
    }
}