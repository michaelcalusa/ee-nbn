public with sharing class NS_BusinessContactDetails{

    @AuraEnabled
    public String busContId {get;set;}
    @AuraEnabled
    public String busContFirstName {get;set;}
    @AuraEnabled
    public String busContSurname {get;set;}
    @AuraEnabled
    public String busContRole {get;set;}
    @AuraEnabled
    public String busContEmail {get;set;}
    @AuraEnabled
    public String busContNumber {get;set;}
    @AuraEnabled
    public String busContStreetAddress {get;set;}
    @AuraEnabled
    public String busContSuburb {get;set;}
    @AuraEnabled
    public String busContPostcode {get;set;}
    @AuraEnabled
    public String busContState {get;set;}

     public NS_BusinessContactDetails(String id, String contactFirstName, String contactSurname,  String contactRole, 
                                      String contactEmail, String contactPhone, String streetAddress, String suburb, String postCode, String state ){

        busContId = id;
        busContFirstName = contactFirstName;
        busContSurname = contactSurname;
        busContRole = contactRole;
        busContNumber = contactPhone;
        busContEmail = contactEmail;
        busContRole = contactRole;
        busContStreetAddress = streetAddress;
        busContSuburb = suburb;
        busContPostcode = postCode;
        busContState = state;

     }
     public NS_BusinessContactDetails(){}

}