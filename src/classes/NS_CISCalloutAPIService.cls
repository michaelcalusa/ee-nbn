public class NS_CISCalloutAPIService {
    /**
    * Entry point for CIS call.
    *
    * @param      List<DF_SF_Request__c>    request
    *
    * @return     void.
    */
    public static List<DF_SF_Request__c> getLocation(List<DF_SF_Request__c> lstServiceFeasiblityRequest) {
        List<DF_SF_Request__c> lstSfrToUpdate = new List<DF_SF_Request__c>();
        List<DF_SF_Request__c> lstSfrToCallout = new List<DF_SF_Request__c>(lstServiceFeasiblityRequest);
        for (DF_SF_Request__c sfrRec : lstSfrToCallout) {
            lstSfrToUpdate.add(makeCISCallOut(sfrRec));
        }
        return lstSfrToUpdate;
    }

    /**
     * Validation and service call of request.
     *
     * @param      DF_SF_Request__c    request
     *
     * @return     DF_SF_Request__c
     */
    public static DF_SF_Request__c makeCISCallOut(DF_SF_Request__c sfReq) {

        HttpResponse res;
        boolean hasError = false;

        String searchType = sfReq.Search_Type__c;

        System.debug('searchType: ' + searchType);
        try {
            if (String.isNotEmpty(searchType)) {
                // SearchByLocationID
                if (searchType.equalsIgnoreCase(NS_CISCalloutAPIUtils.SEARCH_TYPE_LOCATION_ID)) {
                    // Validate data inputs
                    if (NS_CISCalloutAPIUtils.hasValidSearchByLocationIdInputs(sfReq)) {
                        String locationId = sfReq.Location_Id__c;
                        System.debug('makeCISCallOut locationId :' + locationId);
                        res = queryCisByLocationId(locationId);
                    } else {
                        setErrorStatuses(sfReq, NS_CISCalloutAPIUtils.STATUS_ERROR, NS_CISCalloutAPIUtils.STATUS_INVALID_LOCID);
                        hasError = true;
                    }
                    // SearchByLatAndLong
                } else if (searchType.equalsIgnoreCase(NS_CISCalloutAPIUtils.SEARCH_TYPE_LAT_LONG)) {
                    // Validate data inputs
                    if (NS_CISCalloutAPIUtils.hasValidSearchByLatLongInputs(sfReq)) {
                        String latitude = sfReq.Latitude__c;
                        String longitude = sfReq.Longitude__c;
                        String queryString = NS_CISCalloutAPIUtils.URL_FILTER_PREFIX + NS_CISCalloutAPIUtils.LATITUDE + NS_CISCalloutAPIUtils.URL_PARAM_ASSIGN_OPR + latitude + NS_CISCalloutAPIUtils.URL_PARAM_DELIMITER + NS_CISCalloutAPIUtils.LONGITUDE + NS_CISCalloutAPIUtils.URL_PARAM_ASSIGN_OPR + longitude + '&include=containmentBoundaries';
                        res = queryCis(queryString);
                    } else {
                        setErrorStatuses(sfReq, NS_CISCalloutAPIUtils.STATUS_ERROR, NS_CISCalloutAPIUtils.STATUS_INVALID_LATLONG);
                        hasError = true;
                    }
                    // SearchByAddress
                } else if (searchType.equalsIgnoreCase(NS_CISCalloutAPIUtils.SEARCH_TYPE_ADDRESS)) {
                    // Validate data inputs
                    if (NS_CISCalloutAPIUtils.hasValidSearchByAddressInputs(sfReq)) {
                        String addrQueryFilter = NS_CISCalloutAPIUtils.getInputAddressQueryFilter(sfReq);
                        // Convert to urlEndcoded
                        String searchParam = NS_CISCalloutAPIUtils.convertToURLEncodedStr(addrQueryFilter);
                        if (String.isEmpty(searchParam)) {
                            setErrorStatuses(sfReq, NS_CISCalloutAPIUtils.STATUS_ERROR, NS_CISCalloutAPIUtils.STATUS_INVALID_ADDRESS);
                            hasError = true;
                        } else {
                            String queryString = NS_CISCalloutAPIUtils.URL_FILTER_PREFIX + searchParam + NS_CISCalloutAPIUtils.URL_FILTER_SUFFIX;
                            res = queryCis(queryString);
                        }
                    } else {
                        setErrorStatuses(sfReq, NS_CISCalloutAPIUtils.STATUS_ERROR, NS_CISCalloutAPIUtils.STATUS_INVALID_ADDRESS);
                        hasError = true;
                    }
                } else {
                    setErrorStatuses(sfReq, NS_CISCalloutAPIUtils.STATUS_ERROR, NS_CISCalloutAPIUtils.STATUS_INVALID_UNKNOWN);
                    hasError = true;
                }
            } else {
                setErrorStatuses(sfReq, NS_CISCalloutAPIUtils.STATUS_ERROR, NS_CISCalloutAPIUtils.STATUS_INVALID_UNKNOWN);
                hasError = true;
            }

            if (!hasError && String.isNotEmpty(res.getBody()) ) {
                String responseBody = res.getBody();
                system.debug('=====  Processing Response ====');
                SF_ServiceFeasibilityResponse retResponse  = processSFResponse(responseBody, sfReq);
                system.debug('=====  Finished Processing Response ====' + retResponse);
                //sfReq.Response__c = json.serialize(retResponse);
                if (retResponse != null && res.getStatusCode() == SF_Constants.HTTP_STATUS_200) {
                    if (retResponse.derivedTechnology != null && (retResponse.derivedTechnology).equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FTTP)) {
                        retResponse.Status = NS_CISCalloutAPIUtils.STATUS_INVALID_FTTP;
                        sfReq.Status__c = NS_CISCalloutAPIUtils.STATUS_COMPLETED;
                    } else {
                        system.debug('======= in final else ===== ' + retResponse.Status);
                        retResponse = new NS_WhiteListQuoteUtil().getQuoteFromWhiteList(retResponse);
                        if (retResponse.Status == NS_CISCalloutAPIUtils.STATUS_VALID) {
                            sfReq.Status__c = NS_CISCalloutAPIUtils.STATUS_COMPLETED;
                        } else {
                            sfReq.Status__c = NS_CISCalloutAPIUtils.STATUS_ERROR;
                        }
                    }
                } else if (res.getStatusCode() == SF_Constants.HTTP_STATUS_404) {
                    retResponse.status = NS_CISCalloutAPIUtils.STATUS_INVALID_LOCID;
                } else {
                    retResponse = new SF_ServiceFeasibilityResponse();
                    retResponse.LocId = sfReq.Location_Id__c;
                    retResponse.status = NS_CISCalloutAPIUtils.STATUS_INVALID_FTTP;
                    sfReq.Status__c = NS_CISCalloutAPIUtils.STATUS_ERROR;
                }
                sfReq.Response__c = json.serialize(retResponse);
                System.debug('Value of Response :' + sfReq.Response__c);
            }
        } catch (Exception e) {
            //GlobalUtility.logMessage(DF_Constants.ERROR, NS_CISCalloutAPIService.class.getName(), 'makeCISCallOut', sfReq.Name, 'Unkown error while performing SF request', '', '', e, 0);
            System.debug('Exception happened on NS_CISCalloutAPIService.makeCISCallOut() :' + e.getMessage() + ' \n ' + e.getStackTraceString());
            setErrorStatuses(sfReq, NS_CISCalloutAPIUtils.STATUS_ERROR, NS_CISCalloutAPIUtils.STATUS_INVALID_UNKNOWN);
        }

        return sfReq;
    }

    /**
     * Processing the response.
     *
     * @param      DF_Integration_Setting__mdt    integrSetting
     * @param      String    queryString
     *
     * @return     HTTPResponse
     */
    public static SF_ServiceFeasibilityResponse processSFResponse(String response, DF_SF_Request__c sfReq) {

        SF_ServiceFeasibilityResponse sfResponse = NS_CISCalloutAPIUtils.getDFSFReqDTOFromLocationIdResults(response, sfReq);

        return sfResponse;
    }
    /**
     * Making the HTTP call and populating the response.
     *
     * @param      DF_Integration_Setting__mdt    integrSetting
     * @param      String    queryString
     *
     * @return     HTTPResponse
     */
    public static HTTPResponse queryCisByLocationId(String locationId) {
        String queryString = '/' + locationId + '?include=containmentBoundaries';

        return queryCis(queryString);
    }

    /**
     * Making the HTTP call and populating the response.
     *
     * @param      DF_Integration_Setting__mdt    integrSetting
     * @param      String    queryString
     *
     * @return     HTTPResponse
     */
    public static HttpResponse queryCis(String queryString) {
        DF_Integration_Setting__mdt integrSetting = NS_CISCalloutAPIUtils.getIntegrationSettings(NS_CISCalloutAPIUtils.INTEGR_SETTING_NAME_CIS_LOCATION_API);
        final String namedCredential = integrSetting.Named_Credential__c;
        final Integer timeoutLimit = Integer.valueOf(integrSetting.Timeout__c);
        final String nbnEnvOverride = integrSetting.DP_Environment_Override__c;
        final String contextPath = integrSetting.Context_Path__c;

        System.debug('NAMED_CREDENTIAL: ' + namedCredential);
        System.debug('TIMEOUT_LIMIT: ' + timeoutLimit);
        System.debug('NBN_ENV_OVRD_VAL: ' + nbnEnvOverride);

        // Build endpoint
        final String END_POINT = DF_Constants.NAMED_CRED_PREFIX + namedCredential + contextPath + queryString;
        System.debug('END_POINT: ' + END_POINT);

        Map<String, String> headerMap = new Map<String, String>();
        NBNIntegrationInputs__c nbnCISPlacesInstance = NBNIntegrationInputs__c.getInstance(EE_CISLocationAPIService.CIS_PLACES_API_SETTING);
        if (nbnCISPlacesInstance != null) {
            headerMap.put('x-api-key', nbnCISPlacesInstance.x_api_key__c);
        } else {
            System.debug(System.LoggingLevel.WARN, 'The custom setting EE-CISPlacesAPI is NOT configured. The callout to CIS may fail.');
        }
        if (nbnEnvOverride != null) {
            headerMap.put(DF_IntegrationUtils.NBN_ENV_OVRD, nbnEnvOverride);
        }
        headerMap.put(DF_Constants.CONTENT_TYPE, DF_Constants.CONTENT_TYPE_JSON);
        headerMap.put(NS_CISCalloutAPIUtils.REQ_HDR_REFERER, DF_Constants.NBN_DOMAIN);

        // Generate request
        HttpRequest req = NS_CISCalloutAPIUtils.getHttpRequest(END_POINT, timeoutLimit, headerMap);

        // Send and get response
        HttpResponse res = new Http().send(req);

        System.debug('Response Status Code: ' + res.getStatusCode());
        System.debug('Response Body: ' + res.getBody());

        return res;
    }

    private static void setErrorStatuses(DF_SF_Request__c sfReq, String requestStatus, String responseStatus){
        sfReq.Status__c = requestStatus;
        SF_ServiceFeasibilityResponse sfResponse = (SF_ServiceFeasibilityResponse) System.JSON.deserialize(sfReq.Response__c, SF_ServiceFeasibilityResponse.class);
        sfResponse.Status = responseStatus;
        sfReq.Response__c = System.JSON.serialize(sfResponse);
    }

}