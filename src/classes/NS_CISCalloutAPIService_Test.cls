/**
 * Created by alan on 2019-03-04.
 */

@IsTest
public class NS_CISCalloutAPIService_Test {

    @isTest static void test_shouldBuildCorrectQueryFilterStringForSearchByLatLong(){

        String latitude = '-45.000000';
        String longitude = '45.000000';

        DF_SF_Request__c request = new DF_SF_Request__c();
//        request.Opportunity_Bundle__c = opptyBundle.Id;
        request.Search_Type__c = NS_CISCalloutAPIUtils.SEARCH_TYPE_LAT_LONG;
        request.Latitude__c = latitude;
        request.Longitude__c = longitude;
        request.Status__c = NS_CISCalloutAPIUtils.STATUS_PENDING;
        request.Response__c = JSON.serialize(new SF_ServiceFeasibilityResponse());

        // stub cis
        RequestCapturingCisCalloutMock cisCalloutMock = new RequestCapturingCisCalloutMock();
        Test.setMock(HttpCalloutMock.class, cisCalloutMock);

        Test.startTest();

        NS_CISCalloutAPIService.makeCISCallOut(request);

        System.assert(cisCalloutMock.getRequestQueryString().endsWith('?filter=geocode.latitude='+latitude+',geocode.longitude='+longitude+'&include=containmentBoundaries'));

        Test.stopTest();
    }

    private class RequestCapturingCisCalloutMock implements HttpCalloutMock {

        private String requestQueryString;

        public HttpResponse respond(HttpRequest httpRequest){

            requestQueryString = httpRequest.getEndpoint();

            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(200);
            resp.setStatus('ok');
            resp.setHeader('Content-Type', 'application/json');
            resp.setBody('{ 	"data": [{	"type": "location",	"id": "LOC000000000025",	"attributes": {	"dwellingType": "MDU",	"region": "Urban",	"regionValueDefault": "FALSE",	"externalTerritory": "FALSE",	"address": {	"unstructured": "HILLSIDE NURSING HOME  UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290",	"roadNumber1": "3",	"roadName": "VIOLET TOWN",	"roadTypeCode": "RD",	"locality": "MOUNT HUTTON",	"postCode": "2290",	"unitNumber": "212",	"unitType": "UNIT",	"state": "NSW",	"siteName": "HILLSIDE NURSING HOME"	},	"primaryAccessTechnology": {	"technologyType": "Fibre to the node",	"serviceClass": "13",	"rolloutType": "Brownfields"	},	"csaId": "CSA200000010384",	"serviceabilityMessage": null,	"serviceabilityDate": null,	"serviceabilityClassReason": null,	"geoCode": {	"latitude": "-32.9873616",	"longitude": "151.67177704",	"geographicDatum": "GDA94",	"srid": "4283"	},	"landUse": "RESIDENTIAL"	},	"relationships": {	"containmentBoundaries": {	"data": [{	"id": "2BLT-03-11",	"type": "NetworkBoundary",	"boundaryType": "ADA"	},	{	"id": "2BLT-03",	"type": "NetworkBoundary",	"boundaryType": "SAM"	},	{	"id": "S2 - Mount Hutton - Windale",	"type": "NetworkBoundary",	"boundaryType": "SSA"	},	{	"id": "2BLT",	"type": "NetworkBoundary",	"boundaryType": "FSA"	},	{	"id": "2HAM",	"type": "NetworkBoundary",	"boundaryType": "AAR"	}]	},	"MDU": {	"data": {	"id": "LOC100062870710",	"type": "location"	}	}	} 	}], 	"included": [{	"type": "NetworkBoundary",	"id": "2BLT-03-11",	"attributes": {	"boundaryType": "ADA",	"status": "INSERVICE",	"technologyType": "Copper",	"technologySubType": "FTTN"	} 	}, 	{	"type": "NetworkBoundary",	"id": "2BLT-03",	"attributes": {	"boundaryType": "SAM",	"status": "INSERVICE",	"technologyType": "Multiple"	} 	}, 	{	"type": "NetworkBoundary",	"id": "S2 - Mount Hutton - Windale",	"attributes": {	"boundaryType": "SSA",	"status": "PLANNED",	"technologyType": "Satellite"	} 	}, 	{	"type": "NetworkBoundary",	"id": "2BLT",	"attributes": {	"boundaryType": "FSA",	"status": "INSERVICE",	"technologyType": "Multiple"	} 	}, 	{	"type": "NetworkBoundary",	"id": "2HAM",	"attributes": {	"boundaryType": "AAR",	"status": "PLANNED",	"technologyType": "Multiple"	} 	}] }');
            return resp;
        }

        public String getRequestQueryString(){
            return requestQueryString;
        }
    }
    
    @isTest static void shouldSetErrorStatusesOnSFRequests() {
        // stub cis
        CisExceptionCalloutMock cisCalloutMock = new CisExceptionCalloutMock();
        Test.setMock(HttpCalloutMock.class, cisCalloutMock);

        List<DF_SF_Request__c> requests = new List<DF_SF_Request__c>();
        
        requests.add(createSFRequest('wrong_search_type', null, null, null, null));
        
        requests.add(createSFRequest(NS_CISCalloutAPIUtils.SEARCH_TYPE_LOCATION_ID, '', null, null, null));
        requests.add(createSFRequest(NS_CISCalloutAPIUtils.SEARCH_TYPE_LOCATION_ID, null, null, null, null));
        
        requests.add(createSFRequest(NS_CISCalloutAPIUtils.SEARCH_TYPE_LAT_LONG, null, '', '', null));
        requests.add(createSFRequest(NS_CISCalloutAPIUtils.SEARCH_TYPE_LAT_LONG, null, null, null, null));
        
        requests.add(createSFRequest(NS_CISCalloutAPIUtils.SEARCH_TYPE_ADDRESS, null, null, null, 'wrong_postcode'));
        requests.add(createSFRequest(NS_CISCalloutAPIUtils.SEARCH_TYPE_ADDRESS, null, null, null, null));
        
        requests.add(createSFRequest(NS_CISCalloutAPIUtils.SEARCH_TYPE_LOCATION_ID, 'LOC123456789012', null, null, null));
        
        Test.startTest();
        List<DF_SF_Request__c> results = NS_CISCalloutAPIService.getLocation(requests);
        Test.stopTest();
        
        Assert.equals(8, results.size());
        
        DF_SF_Request__c result;
        
        result = results.get(0);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_ERROR, result.Status__c);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_INVALID_UNKNOWN, getResponseStatus(result));
        
        result = results.get(1);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_ERROR, result.Status__c);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_INVALID_LOCID, getResponseStatus(result));
        
        result = results.get(2);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_ERROR, result.Status__c);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_INVALID_LOCID, getResponseStatus(result));
        
        result = results.get(3);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_ERROR, result.Status__c);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_INVALID_LATLONG, getResponseStatus(result));
        
        result = results.get(4);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_ERROR, result.Status__c);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_INVALID_LATLONG, getResponseStatus(result));
        
        result = results.get(5);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_ERROR, result.Status__c);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_INVALID_ADDRESS, getResponseStatus(result));
        
        result = results.get(6);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_ERROR, result.Status__c);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_INVALID_ADDRESS, getResponseStatus(result));
        
        result = results.get(7);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_ERROR, result.Status__c);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_INVALID_UNKNOWN, getResponseStatus(result));
        
    }
    
    private static String getResponseStatus(DF_SF_Request__c request){
        SF_ServiceFeasibilityResponse sfResponse = (SF_ServiceFeasibilityResponse) System.JSON.deserialize(request.Response__c, SF_ServiceFeasibilityResponse.class);
        return sfResponse.Status;
    }
    
    private static DF_SF_Request__c createSFRequest(String searchType, String locId, String latitude, String longitude, String postcode){
        DF_SF_Request__c request = new DF_SF_Request__c();
        request.Search_Type__c = searchType;
        request.Location_Id__c = locId;
        request.Latitude__c = latitude;
        request.Longitude__c = longitude;
        request.Status__c = NS_CISCalloutAPIUtils.STATUS_PENDING;
        request.Postcode__c = postcode;
        request.Response__c = JSON.serialize(new SF_ServiceFeasibilityResponse());
        
        return request;
    }
    
    private class CisExceptionCalloutMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest httpRequest){
            throw new CustomException('CIS server error');
        }
    }
}