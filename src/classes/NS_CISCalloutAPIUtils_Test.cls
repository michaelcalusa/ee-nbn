/**
 * Created by alan on 2019-02-25.
 */

@IsTest
private class NS_CISCalloutAPIUtils_Test {
    @isTest static void test_shouldReturnResponseWithAddressListAndStructuredAddressListAndInvalidAddressStatusWhenMultipleAddressFoundForLatLongSearch() {

        DF_SF_Request__c sfRequest = new DF_SF_Request__c();
        sfRequest.Search_Type__c = NS_CISCalloutAPIUtils.SEARCH_TYPE_LAT_LONG;
        sfRequest.Latitude__c = '10.000000';
        sfRequest.Longitude__c = '10.000000';
        sfRequest.Status__c = NS_CISCalloutAPIUtils.STATUS_PENDING;

        SF_ServiceFeasibilityResponse sfResponse = new SF_ServiceFeasibilityResponse();
        sfResponse.Status = NS_CISCalloutAPIUtils.STATUS_PENDING;
        sfRequest.Response__c = JSON.serialize(sfResponse);

        Test.startTest();

        SF_ServiceFeasibilityResponse response = NS_CISCalloutAPIUtils.getDFSFReqDTOFromLocationIdResults(getMultiLocationResponse(), sfRequest);

        System.assertEquals(NS_CISCalloutAPIUtils.STATUS_INVALID_ADDRESS, response.status);
        System.assertEquals(false, response.AddressList.isEmpty());
        System.assertEquals(false, response.structuredAddressList.isEmpty());

        for (Integer i = 0; i < response.structuredAddressList.size(); i++) {
            Map<String, String> structuredAddress = response.structuredAddressList[i];
            if ('HILLSIDE NURSING HOME  UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290'.equals(structuredAddress.get('unstructured'))) {
                System.assertEquals('UNIT', structuredAddress.get('unitType'));
                System.assertEquals('212', structuredAddress.get('unitNumber'));
                System.assertEquals('1', structuredAddress.get('levelNumber'));
                System.assertEquals('3', structuredAddress.get('streetLotNumber1'));
                System.assertEquals('', structuredAddress.get('streetLotNumber2'));
                System.assertEquals('VIOLET TOWN', structuredAddress.get('streetName'));
                System.assertEquals('RD', structuredAddress.get('streetType'));
                System.assertEquals('MOUNT HUTTON', structuredAddress.get('suburbLocality'));
                System.assertEquals('2290', structuredAddress.get('postcode'));
                System.assertEquals('NSW', structuredAddress.get('state'));
            } else if ('HILLSIDE NURSING HOME  UNIT 213 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290'.equals(structuredAddress.get('unstructured'))) {
                System.assertEquals('UNIT', structuredAddress.get('unitType'));
                System.assertEquals('213', structuredAddress.get('unitNumber'));
                System.assertEquals('', structuredAddress.get('levelNumber'));
                System.assertEquals('3', structuredAddress.get('streetLotNumber1'));
                System.assertEquals('', structuredAddress.get('streetLotNumber2'));
                System.assertEquals('VIOLET TOWN', structuredAddress.get('streetName'));
                System.assertEquals('RD', structuredAddress.get('streetType'));
                System.assertEquals('MOUNT HUTTON', structuredAddress.get('suburbLocality'));
                System.assertEquals('2290', structuredAddress.get('postcode'));
                System.assertEquals('NSW', structuredAddress.get('state'));
            } else {
                System.assert(false, 'unexpected address returned in response.');
            }
        }

        Test.stopTest();
    }

    @isTest static void test_shouldReturnResponseWithAddressListAndStructuredAddressListAndInvalidAddressStatusWhenMultipleAddressFoundForAddressSearch() {

        DF_SF_Request__c sfRequest = new DF_SF_Request__c();
        sfRequest.Search_Type__c = NS_CISCalloutAPIUtils.SEARCH_TYPE_ADDRESS;
        sfRequest.State__c = 'state';
        sfRequest.Postcode__c = 'postcode';
        sfRequest.Suburb_Locality__c = 'suburbLocality';
        sfRequest.Street_Name__c = 'streetName';
        sfRequest.Street_Type__c = 'streetType';
        sfRequest.Street_or_Lot_Number__c = 'streetLotNumber';
        sfRequest.Unit_Type__c = 'unitType';
        sfRequest.Unit_Number__c = 'unitNumber';
        sfRequest.Level__c = 'evel';
        sfRequest.Status__c = NS_CISCalloutAPIUtils.STATUS_PENDING;

        SF_ServiceFeasibilityResponse sfResponse = new SF_ServiceFeasibilityResponse();
        sfResponse.Status = NS_CISCalloutAPIUtils.STATUS_PENDING;
        sfRequest.Response__c = JSON.serialize(sfResponse);

        Test.startTest();

        SF_ServiceFeasibilityResponse response = NS_CISCalloutAPIUtils.getDFSFReqDTOFromLocationIdResults(getMultiLocationResponse(), sfRequest);

        System.assertEquals(NS_CISCalloutAPIUtils.STATUS_INVALID_ADDRESS, response.status);
        System.assertEquals(false, response.AddressList.isEmpty());
        System.assertEquals(false, response.structuredAddressList.isEmpty());

        for (Integer i = 0; i < response.structuredAddressList.size(); i++) {
            Map<String, String> structuredAddress = response.structuredAddressList[i];
            if ('HILLSIDE NURSING HOME  UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290'.equals(structuredAddress.get('unstructured'))) {
                System.assertEquals('UNIT', structuredAddress.get('unitType'));
                System.assertEquals('212', structuredAddress.get('unitNumber'));
                System.assertEquals('1', structuredAddress.get('levelNumber'));
                System.assertEquals('3', structuredAddress.get('streetLotNumber1'));
                System.assertEquals('', structuredAddress.get('streetLotNumber2'));
                System.assertEquals('VIOLET TOWN', structuredAddress.get('streetName'));
                System.assertEquals('RD', structuredAddress.get('streetType'));
                System.assertEquals('MOUNT HUTTON', structuredAddress.get('suburbLocality'));
                System.assertEquals('2290', structuredAddress.get('postcode'));
                System.assertEquals('NSW', structuredAddress.get('state'));
            } else if ('HILLSIDE NURSING HOME  UNIT 213 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290'.equals(structuredAddress.get('unstructured'))) {
                System.assertEquals('UNIT', structuredAddress.get('unitType'));
                System.assertEquals('213', structuredAddress.get('unitNumber'));
                System.assertEquals('', structuredAddress.get('levelNumber'));
                System.assertEquals('3', structuredAddress.get('streetLotNumber1'));
                System.assertEquals('', structuredAddress.get('streetLotNumber2'));
                System.assertEquals('VIOLET TOWN', structuredAddress.get('streetName'));
                System.assertEquals('RD', structuredAddress.get('streetType'));
                System.assertEquals('MOUNT HUTTON', structuredAddress.get('suburbLocality'));
                System.assertEquals('2290', structuredAddress.get('postcode'));
                System.assertEquals('NSW', structuredAddress.get('state'));
            } else {
                System.assert(false, 'unexpected address returned in response.');
            }
        }

        Test.stopTest();
    }

    @isTest static void test_shouldThrowExceptionAndReturnNullWhenMultipleAddressFoundForLocationIdSearch() {

        DF_SF_Request__c sfRequest = new DF_SF_Request__c();
        sfRequest.Search_Type__c = NS_CISCalloutAPIUtils.SEARCH_TYPE_LOCATION_ID;
        sfRequest.Location_Id__c = 'LOC000000000001';
        sfRequest.Status__c = NS_CISCalloutAPIUtils.STATUS_PENDING;

        SF_ServiceFeasibilityResponse sfResponse = new SF_ServiceFeasibilityResponse();
        sfResponse.Status = NS_CISCalloutAPIUtils.STATUS_PENDING;
        sfRequest.Response__c = JSON.serialize(sfResponse);

        Test.startTest();

        SF_ServiceFeasibilityResponse response = NS_CISCalloutAPIUtils.getDFSFReqDTOFromLocationIdResults(getMultiLocationResponse(), sfRequest);
        System.assert(true, response == null);

        Test.stopTest();
    }

    @IsTest
    private static void shouldSetResponseStatusToValidForWirelessAndSatellite() {
        DF_SF_Request__c sfRequest = new DF_SF_Request__c();
        sfRequest.Search_Type__c = NS_CISCalloutAPIUtils.SEARCH_TYPE_LOCATION_ID;
        sfRequest.Location_Id__c = 'LOC000000000001';
        sfRequest.Status__c = NS_CISCalloutAPIUtils.STATUS_PENDING;
        sfRequest.Product_Type__c = 'NBN_SELECT';

        SF_ServiceFeasibilityResponse sfResponse = new SF_ServiceFeasibilityResponse();
        sfResponse.Status = NS_CISCalloutAPIUtils.STATUS_PENDING;
        sfRequest.Response__c = JSON.serialize(sfResponse);

        Test.startTest();
        for (String primaryAccessTechnology : new String[]{
                'Wireless', 'Satellite'
        }) {
            Utils_CISResponseBuilder utilsCISResponseBuilder = new Utils_CISResponseBuilder()
                    .withData(new Utils_CISResponseBuilder.DataBuilder()
                            .withAttributes(
                                    new Utils_CISResponseBuilder.AttributesBuilder()
                                            .withPrimaryAccessTechnology(
                                            new Utils_CISResponseBuilder.PrimaryAccessTechnologyBuilder()
                                                    .withTechnologyType(primaryAccessTechnology)
                                                    .withServiceClass('5')
                                                    .withRolloutType('Brownfields')
                                                    .build())
                                            .build())
                            .build())
                    .withIncluded(new Utils_CISResponseBuilder.IncludedBuilder()
                            .withId('2ALB')
                            .withType('NetworkBoundary')
                            .build()
                    );

            SF_ServiceFeasibilityResponse response = NS_CISCalloutAPIUtils.getDFSFReqDTOFromLocationIdResults(utilsCISResponseBuilder.buildJson(), sfRequest);
            Assert.equals(NS_CISCalloutAPIUtils.STATUS_VALID, response.Status);
            Assert.equals(primaryAccessTechnology, response.derivedTechnology);
        }
        Test.stopTest();

    }

    @IsTest
    private static void shouldSetResponseStatusLocIdNotFoundWhenResponseCannotBeParsed() {
        DF_SF_Request__c sfRequest = new DF_SF_Request__c();
        sfRequest.Search_Type__c = NS_CISCalloutAPIUtils.SEARCH_TYPE_LOCATION_ID;
        sfRequest.Location_Id__c = 'LOC000000000001';
        sfRequest.Status__c = NS_CISCalloutAPIUtils.STATUS_PENDING;
        sfRequest.Product_Type__c = 'NBN_SELECT';

        Test.startTest();

        String locationIdNotFoundResponse = 'Location not found.No location was found for the provided nbn location Id';
        SF_ServiceFeasibilityResponse response = NS_CISCalloutAPIUtils.getDFSFReqDTOFromLocationIdResults(locationIdNotFoundResponse, sfRequest);
        Assert.equals(NS_CISCalloutAPIUtils.STATUS_INVALID_LOCID, response.Status);
        Assert.equals('LOC000000000001', response.LocId);
        Test.stopTest();
    }

    @isTest static void test_shouldReturnTrueForValidLocationId() {
        DF_SF_Request__c sfRequest = new DF_SF_Request__c();
        Boolean result = false;

        Test.startTest();

        sfRequest.Location_Id__c = 'LOC000000000001';
        result = NS_CISCalloutAPIUtils.hasValidSearchByLocationIdInputs(sfRequest);
        Assert.equals(true, result);

        sfRequest.Location_Id__c = 'loc000000000001';
        result = NS_CISCalloutAPIUtils.hasValidSearchByLocationIdInputs(sfRequest);
        Assert.equals(false, result);

        sfRequest.Location_Id__c = '';
        result = NS_CISCalloutAPIUtils.hasValidSearchByLocationIdInputs(sfRequest);
        Assert.equals(false, result);

        sfRequest.Location_Id__c = null;
        result = NS_CISCalloutAPIUtils.hasValidSearchByLocationIdInputs(sfRequest);
        Assert.equals(false, result);

        Test.stopTest();
    }

    @isTest static void test_shouldReturnTrueForValidLatLong() {
        DF_SF_Request__c sfRequest = new DF_SF_Request__c();
        Boolean result = false;

        Test.startTest();

        sfRequest.Latitude__c = '90.000';
        sfRequest.Longitude__c = '-180';
        result = NS_CISCalloutAPIUtils.hasValidSearchByLatLongInputs(sfRequest);
        Assert.equals(true, result);

        sfRequest.Latitude__c = '89.123456';
        sfRequest.Longitude__c = '-176.654321';
        result = NS_CISCalloutAPIUtils.hasValidSearchByLatLongInputs(sfRequest);
        Assert.equals(true, result);

        sfRequest.Latitude__c = null;
        sfRequest.Longitude__c = null;
        result = NS_CISCalloutAPIUtils.hasValidSearchByLatLongInputs(sfRequest);
        Assert.equals(false, result);

        sfRequest.Latitude__c = '';
        sfRequest.Longitude__c = '';
        result = NS_CISCalloutAPIUtils.hasValidSearchByLatLongInputs(sfRequest);
        Assert.equals(false, result);

        sfRequest.Latitude__c = 'null';
        sfRequest.Longitude__c = 'null';
        result = NS_CISCalloutAPIUtils.hasValidSearchByLatLongInputs(sfRequest);
        Assert.equals(false, result);

        sfRequest.Latitude__c = '-32.1234567890';
        sfRequest.Longitude__c = '151.0987654321';
        result = NS_CISCalloutAPIUtils.hasValidSearchByLatLongInputs(sfRequest);
        Assert.equals(true, result);

        sfRequest.Latitude__c = '-32.12345678901';
        sfRequest.Longitude__c = '151.09876543210';
        result = NS_CISCalloutAPIUtils.hasValidSearchByLatLongInputs(sfRequest);
        Assert.equals(false, result);

        Test.stopTest();
    }


    private static String getMultiLocationResponse() {
        Utils_CISResponseBuilder utilsCISResponseBuilder = new Utils_CISResponseBuilder()
                .withData(new Utils_CISResponseBuilder.DataBuilder().build())
                .withData(new Utils_CISResponseBuilder.DataBuilder()
                        .withAttributes(
                                new Utils_CISResponseBuilder.AttributesBuilder()
                                        .withAddress(
                                        new Utils_CISResponseBuilder.AddressBuilder()
                                                .withUnstructured('HILLSIDE NURSING HOME  UNIT 213 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290')
                                                .withRoadNumber1('3')
                                                .withRoadName('VIOLET TOWN')
                                                .withRoadTypeCode('RD')
                                                .withLocality('MOUNT HUTTON')
                                                .withPostCode('2290')
                                                .withUnitNumber('213')
                                                .withUnitType('UNIT')
                                                .withState('NSW')
                                                .withSiteName('HILLSIDE NURSING HOME')
                                                .withLevelNumber('')
                                                .withComplexAddress(new Utils_CISResponseBuilder.ComplexAddressBuilder().withSiteName('Building B').withRoadNumber1('1').withRoadName('Flinders').withRoadTypeCode('Lane').build())
                                                .build())
                                        .build())
                        .build());

        String multiLocationResponse = utilsCISResponseBuilder.buildJson();
        return multiLocationResponse;
    }
}