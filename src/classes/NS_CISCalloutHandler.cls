public class NS_CISCalloutHandler {
    public static Id searchLocationFromCIS(List<DF_SF_Request__c> sfReqIdList){
        List<DF_SF_Request__c> lstSfrToUpdate = new List<DF_SF_Request__c>();
        DF_Opportunity_Bundle__c bundle;
        String bundleId ;
        if (!sfReqIdList.isEmpty()) {
            lstSfrToUpdate = NS_CISCalloutAPIService.getLocation(sfReqIdList); 
            
            if(sfReqIdList[0].Opportunity_Bundle__c == null)
            {
                system.debug('======  Creating Opportunity Bundle =====');
                bundle = NS_CISCalloutAPIUtils.createOpptyBundle('NBN_SELECT');
                insert bundle;
                bundleId = bundle.id;
            }
            else
            {
                system.debug('======  Did not create Opportunity Bundle =====');
               bundleId = sfReqIdList[0].Opportunity_Bundle__c; 
            }
            
            
            if(!lstSfrToUpdate.isEmpty()){
                for (DF_SF_Request__c sfRec : lstSfrToUpdate){
                    sfRec.Opportunity_Bundle__c = bundleId;
                }
            }
            
            
            if(lstSfrToUpdate != null && lstSfrToUpdate.size() > 0)
            {
                if(lstSfrToUpdate[0].id == null)
                {
                    Database.insert(lstSfrToUpdate);      
                }
                else
                {
                	Database.update(lstSfrToUpdate);          
                }
            	
            }
            
            
        }
        return bundleId; 
    } 
}