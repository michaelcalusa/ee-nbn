/**
 * Created by alan on 2019-04-17.
 */

public class NS_CS_Cancellation_Service {

    public DF_Order__c getOrderForCancellation(String orderGuid){

        return [
                SELECT  Id, Order_Id__c, DF_Quote__c, Order_Status__c, Product_Charges_JSON__c, BPI_Id__c, Order_JSON__c,
                        Site_Survey_Charges_JSON__c, NBN_Commitment_Date__c, UNI_Port__c, Order_Sub_Status__c, Cancel_Initiated_Stage__c,
                        Appian_Notification_Date__c, Service_Region__c, Order_Notifier_Response_JSON__c, NTD_Installation_Date__c,
                        DF_Quote__r.GUID__c, DF_Quote__r.Order_GUID__c, DF_Quote__r.Quote_Name__c,DF_Quote__r.Fibre_Build_Cost__c, DF_Quote__r.New_Build_Cost__c, DF_Quote__r.RSP_Response_On_Cost_Variance__c,
                        DF_Quote__r.Opportunity__c, DF_Quote__r.Opportunity__r.Commercial_Deal__c, Opportunity_Bundle__r.Account__c,
                        Opportunity_Bundle__r.Account__r.Billing_ID__c
                FROM    DF_Order__c
                WHERE   DF_Quote__r.Order_GUID__c = :orderGuid
        ];
    }

    public void cancelOpportunitySubscriptionsAndServices(Id opportunityId){

        List<Sobject> csObjects = new List<sobject>();

        Opportunity opp = new opportunity(Id = OpportunityId, StageName = 'Cancelled Lost');
        csObjects.add(opp);

        List<Csord__Order__c> csOrders = [
                Select  Csordtelcoa__Opportunity__c,
                        (select csord__Status__c From csord__Subscriptions__r),
                        (Select csord__Status__c From csord__Services__r)
                From Csord__Order__c
                Where Csordtelcoa__Opportunity__c = :opportunityId
        ];

        if(!csOrders.isEmpty()) {
            for(Csord__Order__c csorder :csOrders){
                for (Csord__Subscription__c csSub : csOrder.csord__Subscriptions__r) {
                    csSub.csord__Status__c = DF_Constants.DF_ORDER_STATUS_CANCELLED;
                    csObjects.add((Sobject) cssub);
                }
                for (Csord__Service__c csService : csOrder.csord__Services__r) {
                    csService.csord__Status__c = DF_Constants.DF_ORDER_STATUS_CANCELLED;
                    csObjects.add((Sobject) csService);
                }
            }
        } else{
            System.debug(LoggingLevel.WARN, 'Could not find Csord__Order__c for Opportunity '+opportunityId);
        }

        update csObjects;
    }

    public Opportunity createCancellationOpportunityAndAssociatedProduct(DF_Order__c nsOrder) {

        Opportunity cancellationChildOpp = new Opportunity(
                Name = DF_Constants.CANCEL_OPPORTUNITY_NAME,
                CloseDate = Date.today().addDays(20),
                StageName = 'New',
                RecordTypeId = DF_CS_API_Util.getRecordType(DF_Constants.NBN_SELECT_RECORDTYPE_NAME, 'Opportunity'),
                AccountId = nsOrder.Opportunity_Bundle__r.Account__c,
                Parent_Opportunity__c = nsOrder.DF_Quote__r.Opportunity__c,
                Commercial_Deal__c  = nsOrder.DF_Quote__r.Opportunity__r.Commercial_Deal__c);

        insert cancellationChildOpp;

        cscfga.API_1.ApiSession apisession = DF_CS_API_Util.createApiSession(NULL);
        cscfga__Product_Basket__c basket = apiSession.getBasket();

        DF_CS_API_Util.associateBasketWithOppty(basket.Id, cancellationChildOpp.Id);

        apiSession.setProductToConfigure(new Cscfga__Product_Definition__c(
                Id = NS_Custom_Options__c.getValues('SF_INFLIGHT_CANCELLATION_DEFINITION_ID').Value__c),
                new Map<String, String> {'containerType' => 'basket', 'linkedId' => basket.Id}
        );

        cscfga.ProductConfiguration productConfig = apiSession.getConfiguration();

        productConfig.getAttribute('InflightStage').setValue(nsOrder.Cancel_Initiated_Stage__c);

        //check for cost variance
        if(nsOrder.DF_Quote__r.New_Build_Cost__c > 0){
            productConfig.getAttribute('FBC_Charge').setValue(String.valueOf(nsOrder.DF_Quote__r.New_Build_Cost__c));
        }
        else if(nsOrder.DF_Quote__r.Fibre_Build_Cost__c > 0){
            productConfig.getAttribute('FBC_Charge').setValue(String.valueOf(nsOrder.DF_Quote__r.Fibre_Build_Cost__c));
        }

        apiSession.executeRules();
        apiSession.validateConfiguration();
        basket = apiSession.persistConfiguration(true);

        insert new Csbb__Product_Configuration_Request__c(
                csbb__Product_Basket__c = basket.Id,
                csbb__Product_Configuration__c = productConfig.getId(),
                csbb__Offer__c = NS_Custom_Options__c.getValues('SF_INFLIGHT_CANCELLATION_OFFER_ID').Value__c,
                csbb__Optionals__c = '{"selectedAddressDisplay":null}',
                csbb__Product_Category__c = NS_Custom_Options__c.getValues('SF_CATEGORY_ID').Value__c,
                csbb__Status__c='finalized'
        );

        DF_CS_API_Util.syncWithOpportunity(basket.Id);

        cancellationChildOpp.StageName = 'Closed Won';
        update cancellationChildOpp;

        return cancellationChildOpp;
    }

    public String getOrderCancelChargesJson(DF_Order__c nsOrder, Opportunity cancellationChildOpportunity, String interactionDateTime) {

        return getSOACancelOrderRequest(
                    getInflightCancelDetails(
                            cancellationChildOpportunity.Id,
                            nsOrder.Id,
                            nsOrder.Order_Id__c,
                            nsOrder.Order_Status__c,
                            interactionDateTime,
                            nsOrder.Opportunity_Bundle__r.Account__r.Billing_ID__c
                    ));
    }


    private NS_OrderInflightCancelDetails getInflightCancelDetails(
                                                                    Id cancellationChildOpportunityId,
                                                                    String nsOrderId,
                                                                    String orderId,
                                                                    String interactionStatus,
                                                                    String interactionDateTime,
                                                                    String billingId
    ) {
        NS_OrderInflightCancelDetails nsOrderInflightCancel = new NS_OrderInflightCancelDetails(nsOrderId, orderId, interactionDateTime, interactionStatus, billingId);

        Csord__Order__c cancellationOrder =
        [
            SELECT
                    Name,
                    csordtelcoa__Opportunity__r.Commercial_Deal__r.Name,
                    csordtelcoa__Opportunity__r.Commercial_Deal__r.End_Date__c,
                    csordtelcoa__Opportunity__r.Account.Name,
            (
                SELECT Name,csord__Total_Price__c,csord__Line_Description__c
                FROM csord__Order_Line_Items__r
            )
            FROM Csord__Order__c
            WHERE csordtelcoa__Opportunity__c = :cancellationChildOpportunityId
            AND csord__Primary_Order__c != NULL
            AND csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.Id = :NS_Custom_Options__c.getValues('SF_INFLIGHT_CANCELLATION_DEFINITION_ID').Value__c
            LIMIT 1
        ];

        for(csord__Order_Line_Item__c oli: cancellationOrder.csord__Order_Line_Items__r){

            if(oli.csord__Line_Description__c.containsIgnoreCase(DF_Constants.CANCEL_OPPORTUNITY_NAME)){
                String[] arrStr = oli.csord__Line_Description__c.split('-');
                nsOrderInflightCancel.cancellationCode = !arrStr.isEmpty()? arrStr.get(0).trim() : Null;
                nsOrderInflightCancel.cancellationFee = oli.csord__Total_Price__c;
            }
            else if(oli.csord__Line_Description__c.containsIgnoreCase('Fibre Build Charge')){
                String[] arrStr = oli.csord__Line_Description__c.split('-');
                nsOrderInflightCancel.fibreBuildCode = !arrStr.isEmpty()? arrStr.get(0).trim() : Null;
                nsOrderInflightCancel.fibreBuildCost = oli.csord__Total_Price__c;
            }
            else if(oli.csord__Line_Description__c.containsIgnoreCase('DBP Cancellation Discount')){
                nsOrderInflightCancel.cancellationDiscount = oli.csord__Total_Price__c;
            }
            else if(oli.csord__Line_Description__c.containsIgnoreCase('FBC Discount')){
                nsOrderInflightCancel.fibreBuildDiscount = oli.csord__Total_Price__c;
            }
        }

        Id commercialDeal = cancellationOrder.csordtelcoa__Opportunity__r.Commercial_Deal__c;
        nsOrderInflightCancel.dealId = commercialDeal <> Null ? cancellationOrder.csordtelcoa__Opportunity__r.Commercial_Deal__r.Name: Null;
        nsOrderInflightCancel.dealName = commercialDeal <> Null ? cancellationOrder.csordtelcoa__Opportunity__r.Account.Name: Null;
        nsOrderInflightCancel.dealEndDate = commercialDeal <> Null ? string.valueOf(cancellationOrder.csordtelcoa__Opportunity__r.Commercial_Deal__r.End_Date__c) : Null;

        return nsOrderInflightCancel;
    }


    private String getSOACancelOrderRequest(NS_OrderInflightCancelDetails nsOrderInflightCancel) {

        String soaCancelOrderRequest = null;

        DF_SOAOrderDataJSONWrapper.ManageBillingEventRequest ssMR = new DF_SOAOrderDataJSONWrapper.ManageBillingEventRequest();
        ssMR.notificationType = DF_Constants.NOTIFICATION_TYPE;

        DF_SOAOrderDataJSONWrapper.ItemInvolvesProduct ssIIP = new DF_SOAOrderDataJSONWrapper.ItemInvolvesProduct();
        ssIIP.id = nsOrderInflightCancel.orderId;
        ssIIP.accessServiceTechnologyType = DF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FIBRE;

        DF_SOAOrderDataJSONWrapper.ProductOrderComprisedOf ssPC = new DF_SOAOrderDataJSONWrapper.ProductOrderComprisedOf();
        ssPC.itemInvolvesProduct = ssIIP;

        DF_SOAOrderDataJSONWrapper.ProductOrder ssPO = new DF_SOAOrderDataJSONWrapper.ProductOrder();
        ssPO.orderType = DF_Constants.ORDER_TYPE_CONNECT;
        ssPO.interactionStatus = nsOrderInflightCancel.interactionStatus;
        ssPO.interactionDateComplete = nsOrderInflightCancel.interactionDateTime;
        ssPO.id = nsOrderInflightCancel.orderId;
        ssPO.SFid = nsOrderInflightCancel.sfId;
        ssPO.productOrderComprisedOf = ssPC;
        if(nsOrderInflightCancel.dealId <> Null){
            ssPO.commercialDealId = nsOrderInflightCancel.dealId;
            ssPO.commercialDealName = nsOrderInflightCancel.dealName;
            ssPO.commercialDealEndDate = nsOrderInflightCancel.dealEndDate;
        }

        DF_SOAOrderDataJSONWrapper.AccessSeekerInteraction ssASI = new DF_SOAOrderDataJSONWrapper.AccessSeekerInteraction();
        ssASI.billingAccountId = nsOrderInflightCancel.billingAccountId;
        ssPO.accessSeekerInteraction = ssASI;

        DF_SOAOrderDataJSONWrapper.DiscountType cancelDiscount;
        if(nsOrderInflightCancel.cancellationDiscount < 0){
            cancelDiscount = new DF_SOAOrderDataJSONWrapper.DiscountType();
            cancelDiscount.type = 'PSD';
            cancelDiscount.amount = string.valueOf(nsOrderInflightCancel.cancellationDiscount);
        }

        DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge cancellationData =
                getOneTimeChargeProdPriceCharge(nsOrderInflightCancel.orderId, nsOrderInflightCancel.cancellationCode, nsOrderInflightCancel.cancellationFee, cancelDiscount);
        String nsOrderFibreBuildCode = nsOrderInflightCancel.fibreBuildCode;
        Decimal nsOrderFibreBuildCost = nsOrderInflightCancel.fibreBuildCost;

        DF_SOAOrderDataJSONWrapper.DiscountType fBiscount;
        if(nsOrderInflightCancel.fibreBuildDiscount < 0){
            fBiscount = new DF_SOAOrderDataJSONWrapper.DiscountType();
            fBiscount.type = 'PSD';
            fBiscount.amount = string.valueOf(nsOrderInflightCancel.fibreBuildDiscount);
        }
        DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge fibreBuildData = getOneTimeChargeProdPriceCharge(nsOrderInflightCancel.orderId, nsOrderFibreBuildCode, nsOrderFibreBuildCost, fBiscount);

        List<DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge> ssOCList = new List<DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge>();

        if(cancellationData != null) {
            ssOCList.add(cancellationData);
        }

        if(fibreBuildData != null) {
            ssOCList.add(fibreBuildData);
        }

        DF_SOAOrderDataJSONWrapper.ProductOrderInvolvesCharges ssPOC = new DF_SOAOrderDataJSONWrapper.ProductOrderInvolvesCharges();
        ssPOC.OneTimeChargeProdPriceCharge = ssOCList;
        ssMR.ProductOrder = ssPO;
        ssMR.ProductOrderInvolvesCharges = ssPOC;

        soaCancelOrderRequest = json.serialize(ssMR, true);
        removeDiscountFromSoaJSON(soaCancelOrderRequest);

        return soaCancelOrderRequest;
    }


    private DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge getOneTimeChargeProdPriceCharge(String chargeReference,
                                                                            String code,
                                                                            Decimal amount,
                                                                            DF_SOAOrderDataJSONWrapper.DiscountType dtype)
    {
        DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge otcppc = null;

        if(String.isNotBlank(code) && String.isNotBlank(chargeReference) && amount != null) {
            otcppc = new DF_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge();
            otcppc.quantity = '1.0';
            otcppc.name = 'Order Cancellation Charges';
            otcppc.ID = code;
            otcppc.chargeReferences = chargeReference;
            otcppc.amount = String.valueOf(amount);
            if(dtype != Null){
                otcppc.discount = new List<DF_SOAOrderDataJSONWrapper.DiscountType>{dtype};
            }
        }

        return otcppc;
    }


    private void removeDiscountFromSoaJSON(String soaJson) {

        if(String.isNotBlank(soaJson)) {
            soaJson = soaJson.replace('"discount" : null,','');
            soaJson = soaJson.replace('"discount":null,','');
            soaJson = soaJson.replace(',{"type":"TERM","amount":null}','');
            soaJson = soaJson.replace('{"type":"PSD","amount":null}','');
            soaJson = soaJson.replace(',{"type":null,"amount":null}','');
            soaJson = soaJson.replace('{"type":null,"amount":null}','');
            soaJson = soaJson.replace('"discount":[],','');
            soaJson = soaJson.replace('"discount":[{"type":"TERM","amount":null}],','');
            soaJson = soaJson.replace('"discount":[{"type":"PSD","amount":null}],','');
            soaJson = soaJson.replace('[,{','[{');
            soaJson = soaJson.replace(',"discount":[{"type":"TERM","amount":"0.00"}]','');
            soaJson = soaJson.replace(',"discount":[{"type":"PSD","amount":"0.00"}]','');
        }
    }


    private class NS_OrderInflightCancelDetails {
        String interactionDateTime {get; set;}
        String interactionStatus {get; set;}
        String cancellationCode {get; set;}
        Decimal cancellationFee {get; set;}
        String fibreBuildCode {get; set;}
        Decimal fibreBuildCost {get; set;}
        String orderId {get; set;}
        String sfId {get; set;}
        String billingAccountId {get; set;}
        Decimal fibreBuildDiscount {get; set;}
        Decimal cancellationDiscount {get; set;}
        String dealId {get; set;}
        String dealName {get; set;}
        String dealEndDate {get; set;}

        public NS_OrderInflightCancelDetails(String salesforceId, String ordId, String interactionDtTime, String interactionSts, String billingId) {
            sfId = salesforceId;
            orderId = ordId;
            interactionDateTime = interactionDtTime;
            interactionStatus = interactionSts;
            billingAccountId = billingId;
        }
    }

}