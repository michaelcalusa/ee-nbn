@isTest
public class NS_CS_Cancellation_Service_Test {

    @testSetup static void testDataSetup() {

        SF_TestData.Parameters createdObjects = SF_TestData.setupAllData(null);

        createdObjects.quote.Fibre_Build_Cost__c = 1000;
        createdObjects.quote.New_Build_Cost__c = 1500;
        update createdObjects.quote;

        createdObjects.order.Order_Status__c = DF_Constants.DF_ORDER_STATUS_CANCELLED;
        createdObjects.order.Cancel_Initiated_Stage__c = 'Build';
        createdObjects.order.Order_Sub_Status__c = '';
        createdObjects.order.OrderType__c = 'Connect';
        createdObjects.order.Order_Id__c = 'BCO-12345';
        createdObjects.order.Appian_Notification_Date__c = Datetime.now();
        update createdObjects.order;

        Csord__Order__c csOrder = DF_TestData.createCSOrder('NBN Select - Product Charges', createdObjects.account.Id, createdObjects.opp);
        DF_TestData.createSubscription('NBN Select - Product Charges','BPI000000012911','Upgrade', createdObjects.account.Id, csOrder);
    }

    @IsTest
    static void shouldGetOrderForCancellation(){

        Df_order__c nsOrder  = [SELECT Id, Order_Status__c, Order_Sub_Status__c, Cancel_Initiated_Stage__c, DF_Quote__r.Order_GUID__c, DF_Quote__r.Opportunity__c FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1];
        nsOrder.Order_Status__c = DF_Constants.DF_ORDER_STATUS_CANCELLED;
        update nsOrder;

        Test.startTest();

            NS_CS_Cancellation_Service service = new NS_CS_Cancellation_Service();
            DF_Order__c cancelledOrder = service.getOrderForCancellation(nsOrder.DF_Quote__r.Order_GUID__c);

        Test.stopTest();

        Assert.equals(DF_Constants.DF_ORDER_STATUS_CANCELLED, cancelledOrder.Order_Status__c);
        Assert.equals(null, cancelledOrder.Order_Sub_Status__c);
        Assert.equals('Build', cancelledOrder.Cancel_Initiated_Stage__c);

    }

    @IsTest
    static void shouldCancelOpportunitySubscriptionsAndServices(){

        Df_order__c nsOrder  = [SELECT Id, Order_Status__c, Order_Sub_Status__c, Cancel_Initiated_Stage__c, DF_Quote__r.Order_GUID__c, DF_Quote__r.Opportunity__c FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1];

        Opportunity opp = [SELECT Id, Name, StageName FROM Opportunity WHERE Id = :nsOrder.DF_Quote__r.Opportunity__c];

        Csord__Order__c csOrder = [SELECT Id FROM Csord__Order__c WHERE csordtelcoa__Opportunity__c = :opp.Id];
        Csord__Subscription__c csSubscription = [SELECT csord__Status__c FROM Csord__Subscription__c WHERE csord__Order__c = :csOrder.Id];

        Assert.equals('New', opp.StageName);
        Assert.equals(null, csSubscription.csord__Status__c);

        Test.startTest();

            NS_CS_Cancellation_Service service = new NS_CS_Cancellation_Service();
            service.cancelOpportunitySubscriptionsAndServices(nsOrder.DF_Quote__r.Opportunity__c);

        Test.stopTest();

        opp = [SELECT Id, StageName FROM Opportunity WHERE Id = :nsOrder.DF_Quote__r.Opportunity__c];
        Assert.equals('Cancelled Lost', opp.StageName);
        csSubscription = [SELECT csord__Status__c FROM Csord__Subscription__c WHERE csord__Order__c = :csOrder.Id];
        Assert.equals('Cancelled', csSubscription.csord__Status__c);
    }

    @IsTest
    static void shouldCreateCancellationOpportunityAndAssociatedProduct(){

        Df_order__c nsOrder  = [SELECT Id, Order_Id__c, Order_Status__c, Order_Sub_Status__c, Cancel_Initiated_Stage__c, Appian_Notification_Date__c,
                DF_Quote__r.Order_GUID__c, DF_Quote__r.Fibre_Build_Cost__c, DF_Quote__r.New_Build_Cost__c, DF_Quote__r.Opportunity__c, DF_Quote__r.Opportunity__r.Commercial_Deal__c,
                Opportunity_Bundle__r.Account__c, Opportunity_Bundle__r.Account__r.Billing_ID__c
        FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1];

        cscfga__Product_Basket__c basket = DF_TestData.buildBasket();
        basket.csbb__Account__c = nsOrder.Opportunity_Bundle__r.Account__c;
        insert basket;

        DF_AttributeTest attTest1 = new DF_AttributeTest('InflightStage', 'Design', false, null);
        DF_AttributeTest attTest2 = new DF_AttributeTest('FBC_Charge', '100', false, null);

        List<DF_AttributeTest> attributeTestList = new List<DF_AttributeTest>{attTest1, attTest2};

        DF_TestService.CsObjects csObjects = DF_TestService.createProductConfigData('Direct Fibre - Inflight Build Cancellation Charge', basket, attributeTestList);

        List<NS_Custom_Options__c> nsCustomOptions = new List<NS_Custom_Options__c>();
        nsCustomOptions.add(new NS_Custom_Options__c(name = 'SF_INFLIGHT_CANCELLATION_DEFINITION_ID', Value__c = csObjects.productDefinition.Id));
        nsCustomOptions.add(new NS_Custom_Options__c(name = 'SF_INFLIGHT_CANCELLATION_OFFER_ID', Value__c = csObjects.productConfiguration.cscfga__Configuration_Offer__c));
        nsCustomOptions.add(new NS_Custom_Options__c(name = 'SF_CATEGORY_ID', Value__c = csObjects.productCategory.Id));
        insert nsCustomOptions;

        Test.startTest();

            NS_CS_Cancellation_Service service = new NS_CS_Cancellation_Service();
            Opportunity cancelOpp = service.createCancellationOpportunityAndAssociatedProduct(nsOrder);

        Test.stopTest();

        Assert.equals('Cancellation Charge', cancelOpp.Name);
        Assert.equals('Closed Won', cancelOpp.StageName);
        Assert.equals(DF_CS_API_Util.getRecordType('NBN Select', 'Opportunity'), cancelOpp.RecordTypeId);
        Assert.equals(nsOrder.Opportunity_Bundle__r.Account__c, cancelOpp.AccountId);
        Assert.equals(nsOrder.DF_Quote__r.Opportunity__c, cancelOpp.Parent_Opportunity__c);
        Assert.equals(nsOrder.DF_Quote__r.Opportunity__r.Commercial_Deal__c, cancelOpp.Commercial_Deal__c);

        Csbb__Product_Configuration_Request__c prodConfigRequest = [
                SELECT Id, csbb__Offer__c, csbb__Product_Category__c, csbb__Status__c, csbb__Optionals__c
                FROM Csbb__Product_Configuration_Request__c
                WHERE csbb__Product_Category__c = :NS_Custom_Options__c.getValues('SF_CATEGORY_ID').Value__c
        ];
        Assert.equals(NS_Custom_Options__c.getValues('SF_INFLIGHT_CANCELLATION_OFFER_ID').Value__c, prodConfigRequest.csbb__Offer__c);
        Assert.equals('finalized', prodConfigRequest.csbb__Status__c);
        Assert.equals('{"selectedAddressDisplay":null}', prodConfigRequest.csbb__Optionals__c);

//        Boolean cancellationOrderLineItemFound = false;
//
//        Csord__Order__c cancellationOrder =
//        [
//                SELECT
//                        Name,
//                        csordtelcoa__Opportunity__r.Commercial_Deal__r.Name,
//                        csordtelcoa__Opportunity__r.Commercial_Deal__r.End_Date__c,
//                        csordtelcoa__Opportunity__r.Account.Name,
//                (
//                        SELECT Name,csord__Total_Price__c,csord__Line_Description__c
//                        FROM csord__Order_Line_Items__r
//                )
//                FROM Csord__Order__c
//                WHERE csordtelcoa__Opportunity__c = :cancelOpp.Id
//                AND csord__Primary_Order__c != NULL
//                AND csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.Id = :NS_Custom_Options__c.getValues('SF_INFLIGHT_CANCELLATION_DEFINITION_ID').Value__c
//                LIMIT 1
//        ];
//
//        for(csord__Order_Line_Item__c oli: cancellationOrder.csord__Order_Line_Items__r){
//
//            if(oli.csord__Line_Description__c.containsIgnoreCase('Cancellation Charge')){
//                cancellationOrderLineItemFound = true;
//                //assert cancellation code and fee
//                String[] arrStr = oli.csord__Line_Description__c.split('-');
//                Assert.equals('code', arrStr.get(0).trim());
//                Assert.equals('fee', oli.csord__Total_Price__c);
//            }
//        }
//
//        Assert.equals(true, cancellationOrderLineItemFound);

    }

    @IsTest
    static void shouldGenerateExpectedCancelChargesJson(){

        Df_order__c nsOrder  = [SELECT Id, Order_Id__c, Order_Status__c, Order_Sub_Status__c, Cancel_Initiated_Stage__c, Appian_Notification_Date__c,
                DF_Quote__r.Order_GUID__c, DF_Quote__r.Fibre_Build_Cost__c, DF_Quote__r.New_Build_Cost__c, DF_Quote__r.Opportunity__c, DF_Quote__r.Opportunity__r.Commercial_Deal__c,
                Opportunity_Bundle__r.Account__c, Opportunity_Bundle__r.Account__r.Billing_ID__c
        FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1];

        cscfga__Product_Basket__c basket = DF_TestData.buildBasket();
        basket.csbb__Account__c = nsOrder.Opportunity_Bundle__r.Account__c;
        insert basket;

        DF_AttributeTest attTest1 = new DF_AttributeTest('InflightStage', 'Design', false, null);
        DF_AttributeTest attTest2 = new DF_AttributeTest('FBC_Charge', '100', false, null);

        List<DF_AttributeTest> attributeTestList = new List<DF_AttributeTest>{attTest1, attTest2};

        DF_TestService.CsObjects csObjects = DF_TestService.createProductConfigData('Direct Fibre - Inflight Build Cancellation Charge', basket, attributeTestList);

        List<NS_Custom_Options__c> nsCustomOptions = new List<NS_Custom_Options__c>();
        nsCustomOptions.add(new NS_Custom_Options__c(name = 'SF_INFLIGHT_CANCELLATION_DEFINITION_ID', Value__c = csObjects.productDefinition.Id));
        nsCustomOptions.add(new NS_Custom_Options__c(name = 'SF_INFLIGHT_CANCELLATION_OFFER_ID', Value__c = csObjects.productConfiguration.cscfga__Configuration_Offer__c));
        nsCustomOptions.add(new NS_Custom_Options__c(name = 'SF_CATEGORY_ID', Value__c = csObjects.productCategory.Id));
        insert nsCustomOptions;

        NS_CS_Cancellation_Service service = new NS_CS_Cancellation_Service();

        Test.startTest();

            Opportunity cancelOpp = service.createCancellationOpportunityAndAssociatedProduct(nsOrder);

            csord__Order__c cancellationChargeOrder = new csord__Order__c(Name = 'Cancellation Charge', csord__Identification__c = 'Order_a1h5D000000HUDPQA7_1');

            insert cancellationChargeOrder;

            Csord__Order__c inflightCancellationOrder = new csord__Order__c
                ( Name = 'Direct Fibre - Inflight Build Cancellation Charge'
                        , csordtelcoa__Opportunity__c = cancelOpp.Id
                        , csord__Primary_Order__c = cancellationChargeOrder.id
                        , csordtelcoa__Product_Configuration__c = csObjects.productConfiguration.Id
                        , csord__Status2__c = 'Order Submitted'
                        , csord__Order_Type__c = 'Product Delivery'
                        , csord__Product_Type__c = 'Recoverable Works'
                        , csord__Identification__c = 'Order_a1h5D000000HUDPQA4_0'
                );

            insert inflightCancellationOrder;

            List<Csord__Order_Line_Item__c> testOLIList = new List<csord__Order_Line_Item__c>();

            testOLIList.add(new csord__Order_Line_Item__c
                ( Name = 'PCD Relocation'
                        , csord__Order__c = inflightCancellationOrder.Id
                        , csord__Line_Description__c = 'Cancellation Charge-NS_NRC_CANX_BUILD_FEE'
                        , OLI_Type__c = 'Initial Basket'
                        , Memo_Line__c = ''
                        , csord__Total_Price__c = decimal.valueOf('60000.00')
                        , GST__c = 'GST (Inclusive)'
                        , Cost__c = decimal.valueOf('1000.00')
                        , Margin__c = decimal.valueOf('15.00')
                        , Quantity__c = decimal.valueOf('1')
                        , Unit_Price__c = decimal.valueOf('1150.00')
                        , csord__Identification__c = 'Order_a1h5D000000HUDPQA4_0'
                ));

            insert testOLIList;

            String orderCancelChargesJson = service.getOrderCancelChargesJson(nsOrder, cancelOpp, '2019-04-12T01:23:57Z');

        Test.stopTest();

        //sfid Id is generated different every time so we need to mask it before asserting JSON string
        Integer sfidStartPos = orderCancelChargesJson.indexOf('"SFid":"')+8;
        Integer sfidEndPos = orderCancelChargesJson.indexOf('"', sfidStartPos);
        String sfidId = orderCancelChargesJson.substring(sfidStartPos, sfidEndPos);
        System.debug('sfid is: '+sfidId);

        System.debug('Order_Cancel_Charges_JSON__c: ' + orderCancelChargesJson);

        Assert.equals('{"ProductOrderInvolvesCharges":{"OneTimeChargeProdPriceCharge":[{"quantity":"1.0","name":"Order Cancellation Charges","ID":"Cancellation Charge","chargeReferences":"BCO-12345","amount":"60000.00"}]},"productOrder":{"SFid":"XXX","productOrderComprisedOf":{"itemInvolvesProduct":{"id":"BCO-12345","accessServiceTechnologyType":"Fibre"}},"orderType":"Connect","interactionStatus":"Cancelled","interactionDateComplete":"2019-04-12T01:23:57Z","id":"BCO-12345","accessSeekerInteraction":{"billingAccountID":"BAN000000892801"}},"notificationType":"BillingEvent"}',
                orderCancelChargesJson.replace(sfidId, 'XXX'));

    }

}