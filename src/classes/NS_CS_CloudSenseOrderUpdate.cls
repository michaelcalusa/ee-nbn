/*
Class Name: NS_CS_CloudSenseOrderUpdate
Description: Align CloudSense order status to DF_Order status whenever the notifications are recieved from APPIAN

*/

public class NS_CS_CloudSenseOrderUpdate{

    public static String updateCSOrderStatus(Map<Id, String> oppIdDForderStatusMap){

            List<csord__Order__c> ordListToUpdate = new List<csord__Order__c>();
            for(csord__Order__c ord : [SELECT Id, Name, csord__Status__c, csordtelcoa__Opportunity__c FROM csord__Order__c WHERE csordtelcoa__Opportunity__c IN: oppIdDForderStatusMap.KeySet()]){
                ord.csord__Status2__c = oppIdDForderStatusMap.get(ord.csordtelcoa__Opportunity__c);
                ordListToUpdate.add(ord);
            }

            try{
                Database.update(ordListToUpdate);
                return 'Success';
            }
            catch(DmlException e){
                System.debug('The following exception has occurred: ' + e.getMessage());
                return e.getMessage();
            }
    }
}