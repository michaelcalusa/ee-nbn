/*
Class Name: NS_CS_CloudSenseOrderUpdateTest
Description: Test class for CloudSenseOrderUpdate
*/


@isTest
private class NS_CS_CloudSenseOrderUpdate_Test {
    @isTest static void updateCSOrderStatusTest() {
        Account acc =  SF_TestData.createAccount('TestAccount');
        insert acc;
        Opportunity opp = SF_TestData.createOpportunity('TestOpportunity');
        insert opp;
        csord__Order_Request__c ordReq = SF_TestData.buildOrderRequest();
        insert ordReq;
        csord__Order__c ord = SF_TestData.buildOrder('TestOrder', acc.Id, opp.Id,'Order Submitted',ordReq.ID);
        insert ord;
        String message = NS_CS_CloudSenseOrderUpdate.updateCSOrderStatus(new Map<Id, String> {opp.Id =>'Acknowledge'});
        System.assertEquals('Success', message);
    }

}