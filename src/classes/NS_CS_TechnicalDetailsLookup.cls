global with sharing class NS_CS_TechnicalDetailsLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){
        return '["OpportunityId", "AccountId", "TechnologyType", "FibreDistance", "RAGStatus"]';
    }
    
    public final String STEP = 'STEP';
    public final String MTR = 'MTR';

    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionId){
    	System.debug('searchFields ::'+searchFields);
    	String RECORDTYPE = SF_CS_API_Util.getCustomSettingValue('SF_RECORDTYPE_DEVELOPER_NAME');
    	
    	Id recordTypeId = Schema.SObjectType.cspmb__Price_Item__c.getRecordTypeInfosByDeveloperName().get(RECORDTYPE).getRecordTypeId();
    	List<cspmb__Price_Item__c> pilist = [Select Id, Name, cspmb__One_Off_Charge__c, cspmb__Recurring_Charge__c, cspmb__Is_Active__c, cspmb__Effective_Start_Date__c, cspmb__Effective_End_Date__c,
    												Overhead__c, Margin__c, Manhattan_Factor__c, Price_Item_Header__r.Is_Active__c, Price_Item_Header__r.Name
    												from cspmb__Price_Item__c 
    												where Name=:searchFields.get('TechnologyType')
    												AND RecordTypeId = :recordTypeId
    												AND cspmb__Effective_Start_Date__c <=: System.Today() AND cspmb__Effective_End_Date__c >=: System.Today()
    												AND Price_Item_Header__r.Name = :RECORDTYPE];

    	//use first Price Item record to
    	cspmb__Price_Item__c retconlist = new cspmb__Price_Item__c();
    	System.debug('Anydatatype_msg'+pilist);
    	if(!pilist.isEmpty()) retconlist = pilist[0];

    	//Query associated Technical Items
    	List<Technical_Detail_Price_Item_Assoc__c> techDetailsList = new List<Technical_Detail_Price_Item_Assoc__c>();
    	if(retconlist != null){
    		techDetailsList = [SELECT Id, Price_Item__c, Technical_Detail__c, Technical_Detail__r.Id, Technical_Detail__r.Cost__c,
    				Technical_Detail__r.Is_Recurring__c, Technical_Detail__r.Multiplication_Factor__c, Technical_Detail__r.Unit__c
    				FROM Technical_Detail_Price_Item_Assoc__c
    				WHERE Price_Item__c=:retconlist.Id];
	    	
	    	Decimal recurringCharge=0.00;
	    	Decimal nonRecurringCharge=0.00;
	    	Integer fibreDist = 0;
	    	Decimal mFactor = retconlist.Manhattan_Factor__c == null? 1 : retconlist.Manhattan_Factor__c;

	    	if(searchFields.get('FibreDistance') != null) fibreDist = Integer.valueOf(searchFields.get('FibreDistance'));
	    	String ragStatus = searchFields.get('RAGStatus');
	    	//Loop through technical items
	    	for(Technical_Detail_Price_Item_Assoc__c td: techDetailsList){
	    		Decimal cost;
	    		//Check the unit of Technical Item
		    	//if STEP: Divide the distance by step and calculate cost
		    	//if MTR: Multiply by factor (%dist) and calculate cost
		    	if(td.Technical_Detail__r.Unit__c == STEP){
		    		Decimal denominator = td.Technical_Detail__r.Multiplication_Factor__c == null? 1 : (td.Technical_Detail__r.Multiplication_Factor__c == 0? 1: td.Technical_Detail__r.Multiplication_Factor__c);
		    		cost = ((fibreDist*mFactor).divide(denominator, 0, System.RoundingMode.CEILING))*td.Technical_Detail__r.Cost__c;
		    	} else if(td.Technical_Detail__r.Unit__c == MTR){
		    		Decimal factor = td.Technical_Detail__r.Multiplication_Factor__c == null? 1 : td.Technical_Detail__r.Multiplication_Factor__c;
		    		cost = (fibreDist*mFactor*factor)*td.Technical_Detail__r.Cost__c;
		    	}

		    	//Check isRecurring of Technical Item
		    	//if recurring add the value into recurring cost else add the value into one-off cost
		    	if(td.Technical_Detail__r.Is_Recurring__c == True){
		    		recurringCharge += cost;
		    	} else
		    		nonRecurringCharge += cost;

	    	}
    	
    		//temporarily (DO NOT UPDATE) set the value in retconlist object and return the object 
        
	    	retconlist.cspmb__One_Off_Charge__c = nonRecurringCharge.setScale(2);
	    	retconlist.cspmb__Recurring_Charge__c = recurringCharge.setScale(2);
			return new List<cspmb__Price_Item__c>{retconlist};

		} else{

			return null;
		}
          
	}

}