@isTest
private class NS_CS_TechnicalDetailsLookup_Test {
    
    
    @testsetup static void NS_CS_TechDtlsLookupTestDataSetup(){    
        NS_Custom_Options__c cs = new NS_Custom_Options__c(name = 'SF_RECORDTYPE_DEVELOPER_NAME', Value__c = 'NBN_Select');
        insert cs;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Oppty';
        opp.StageName = 'New';
        opp.CloseDate = System.today();
        opp.RecordTypeId = SF_CS_API_Util.getDFRecordType();
        insert opp;

        Price_Item_Header__c pHeader = new Price_Item_Header__c();
        pHeader.Name = 'NBN_Select';
        pHeader.External_ID__c = 'NS000012';
        pHeader.Is_Active__c = true;
        insert pHeader;
        
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c();
        priceItem.Name = 'FTTN';
        priceItem.cspmb__Is_Active__c = true;
        priceItem.Price_Item_Header__c = pHeader.Id;
        priceItem.RecordTypeId = Schema.SObjectType.cspmb__Price_Item__c.getRecordTypeInfosByDeveloperName().get('NBN_Select').getRecordTypeId();
        priceItem.cspmb__Effective_Start_Date__c = System.Today() - 2;
        priceItem.cspmb__Effective_End_Date__c = System.Today() + 2;
            
        insert priceItem;
    
        List<Technical_Detail__c> td = new List<Technical_Detail__c>();
        Technical_Detail__c td1 = new Technical_Detail__c();
        td1.Cost__c = 120.00;
        td1.Is_Recurring__c = false;
        td1.Multiplication_Factor__c = 0.2;
        td1.Unit__c = 'MTR';
        td1.External_ID__c = 'TECH000003';
        td.add(td1);
        Technical_Detail__c td2 = new Technical_Detail__c();
        td2.Cost__c = 120.00;
        td2.Is_Recurring__c = false;
        td2.Multiplication_Factor__c = null;
        td2.External_ID__c = 'TECH000004';
        td2.Unit__c = 'STEP';
        td.add(td2);

        insert td;

        List<Technical_Detail_Price_Item_Assoc__c> techDetailsList = new List<Technical_Detail_Price_Item_Assoc__c>();
        Technical_Detail_Price_Item_Assoc__c techDetails1 = new Technical_Detail_Price_Item_Assoc__c();
        techDetails1.Price_Item__c = priceItem.Id;
        techDetails1.External_ID__c = 'TECH000001';
        techDetails1.Technical_Detail__c = td1.Id;
        techDetailsList.add(techDetails1);
        
        Technical_Detail_Price_Item_Assoc__c techDetails2 = new Technical_Detail_Price_Item_Assoc__c();
        techDetails2.Price_Item__c = priceItem.Id;
        techDetails2.External_ID__c = 'TECH000002';
        techDetails2.Technical_Detail__c = td2.Id;
        techDetailsList.add(techDetails2);

        insert techDetailsList;
        
    }


    static testmethod void doLookupSearchTest(){

        //["OpportunityId", "AccountId", "TechnologyType", "FibreDistance", "RAGStatus"]'
        Opportunity oppty = [select id from Opportunity][0];
        Map<String, String> searchFields= new Map<String, String>();
        searchFields.put('OpportunityId', oppty.Id);
        searchFields.put('TechnologyType', 'FTTN');
        searchFields.put('FibreDistance', '100');
        searchFields.put('RAGStatus', 'Green');


        NS_CS_TechnicalDetailsLookup lookupSearch = new NS_CS_TechnicalDetailsLookup();
        string str = lookupSearch.getRequiredAttributes();
        system.assertEquals('["OpportunityId", "AccountId", "TechnologyType", "FibreDistance", "RAGStatus"]', str);

        List<cspmb__Price_Item__c> price = (List<cspmb__Price_Item__c>) lookupSearch.doDynamicLookupSearch(searchFields, null);
        system.assertEquals(14400.00, price[0].cspmb__One_Off_Charge__c);
        system.assertEquals(0.00, price[0].cspmb__Recurring_Charge__c);
    }
    
}