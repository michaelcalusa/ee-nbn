public with sharing class NS_ContactDetails{
//wrapper class for the site details screen in the
            //'siteContId'    : maxsiteContId + 1,        
            //'siteContFirstName': 'Lonzo',
            //'siteContSurname': 'Ball',
            //'siteContEmail': 'lball@test.com',
            //'siteContNumber': '(03) 9311 2010'
	@AuraEnabled
    public String siteContId {get;set;}
    @AuraEnabled
    public String siteContFirstName {get;set;}
    @AuraEnabled
    public String siteContSurname {get;set;}
    @AuraEnabled
    public String siteContEmail {get;set;}
    @AuraEnabled
    public String siteContNumber {get;set;}

     public NS_ContactDetails(String id, String contactFirstName, String contactSurname, String contactPhone, String contactEmail){
         siteContId = id;
         siteContFirstName = contactFirstName;
         siteContSurname = contactSurname;
         siteContNumber = contactPhone;
         siteContEmail = contactEmail;
     }
     public NS_ContactDetails(){}

}