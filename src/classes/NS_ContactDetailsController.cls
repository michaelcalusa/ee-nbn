public with sharing class NS_ContactDetailsController{

    @AuraEnabled
    public static String saveOrderDetails(String strOrderId, String strOrderJson){
        return NS_Order_ItemController.saveOrderDetails(strOrderId, strOrderJson);
    }

    @AuraEnabled
    public static String addSiteContactToContactsAndOppContactRole(String contactRoleType, String jsonContactDetails, Id quoteId, String strOrderId){

        try{
            NS_ContactDetails contactDetails = (NS_ContactDetails)JSON.deserialize(jsonContactDetails, NS_ContactDetails.class);
			System.debug('Deserialised site contact from JSON: ' + contactDetails);
            
            if(!validateEmail(contactDetails.siteContEmail)){
                DmlException e = new DmlException();
                e.setMessage('INVALID_EMAIL_ADDRESS');
                throw e;
            }
            
            Id accountRecordTypeId = Schema.SobjectType.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId();
            List<Contact> existingContacts = [
                SELECT Id,
                        FirstName,
                        LastName,
                        Email,
                        Phone
                FROM Contact
                WHERE FirstName =:contactDetails.siteContFirstName
                    AND LastName =:contactDetails.siteContSurname
                    AND (Phone =:contactDetails.siteContNumber OR Email =:contactDetails.siteContEmail)
                    AND recordtypeid=:accountRecordTypeId
                LIMIT 1
            ];
			Id contactId;
            if(existingContacts.isEmpty()){
                Contact c = new Contact();
                c.FirstName = contactDetails.siteContFirstName;
            	c.LastName = contactDetails.siteContSurname;
            	c.Email = contactDetails.siteContEmail;
            	c.Phone= contactDetails.siteContNumber;
                c.RecordTypeId = accountRecordTypeId;
                
                Database.DMLOptions dml = new Database.DMLOptions();
                dml.DuplicateRuleHeader.AllowSave = true;
                contactId = Database.insert(c,dml).getId();
                System.debug('Created new Contact '+ c);
            }else{
                existingContacts[0].Phone = contactDetails.siteContNumber;
                existingContacts[0].Email = contactDetails.siteContEmail;
                Database.update(existingContacts[0]);
                contactId = existingContacts[0].Id;
                System.debug('Updated existing Contact '+ existingContacts[0]);
            }

            /*Save contact details to OrderJson*/
            if(String.isNotBlank(strOrderId)){
                DF_Order__c orderRec = [
                    SELECT  Id,
                            Order_JSON__c
                    FROM DF_Order__c
                    WHERE Id = :strOrderId 
                    LIMIT 1
                ];
                string ordrJson = orderRec.Order_JSON__c;
                NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot mainWrap = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot)JSON.deserialize(ordrJson, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class);
                list<NS_OrderDataJSONWrapper.ItemInvolvesContact> tempItemCons = new List<NS_OrderDataJSONWrapper.ItemInvolvesContact>();
              
                for(Integer i=0; i<mainWrap.resourceOrder.itemInvolvesContact.size(); i++){
                    NS_OrderDataJSONWrapper.ItemInvolvesContact item = mainWrap.resourceOrder.itemInvolvesContact.get(i);
                    if( 'Site' != item.type ){
                        //Add all existing business contacts and only allow a single site contact per order
                        tempItemCons.add(item);
                    }
                }

                NS_OrderDataJSONWrapper.ItemInvolvesContact itemInvolvesContact = new NS_OrderDataJSONWrapper.ItemInvolvesContact();
                System.debug('Order ItemInvolvesContact: ' + itemInvolvesContact);
                itemInvolvesContact.contactName= contactDetails.siteContFirstName+' '+contactDetails.siteContSurname;
                itemInvolvesContact.emailAddress=contactDetails.siteContEmail;
                itemInvolvesContact.phoneNumber=contactDetails.siteContNumber;
                itemInvolvesContact.type='Site';
                itemInvolvesContact.role='';
                itemInvolvesContact.unstructuredAddress = new NS_OrderDataJSONWrapper.UnstructuredAddress();
                itemInvolvesContact.unstructuredAddress.addressLine1='';
                itemInvolvesContact.unstructuredAddress.localityName='';
                itemInvolvesContact.unstructuredAddress.postcode='';
                itemInvolvesContact.unstructuredAddress.stateTerritoryCode='';

                tempItemCons.add(itemInvolvesContact);

                mainWrap.resourceOrder.itemInvolvesContact = tempItemCons;
                string orderJsonUpdate = JSON.serialize(mainWrap);
                orderRec.Order_JSON__c = orderJsonUpdate;
                Database.update(orderRec);
                system.debug('---orderJsonUpdate-- '+orderJsonUpdate);
            }
            /*End of Order Json update logic*/

            DF_Quote__c dfQuote = [SELECT Opportunity__c FROM DF_Quote__c WHERE Id =:quoteId  LIMIT 1];

            List<OpportunityContactRole> ocrs = [
                SELECT  Id,
                        ContactId,
                        OpportunityId,
                        Role,
                        IsPrimary
                FROM OpportunityContactRole 
                WHERE OpportunityId =:dfQuote.Opportunity__c AND Role =:contactRoleType 
                LIMIT 1
            ];
            
            if(ocrs.isEmpty()){
                OpportunityContactRole oppContRole = new OpportunityContactRole();
            	oppContRole.ContactId = contactId;
                oppContRole.OpportunityId = dfQuote.Opportunity__c;
            	oppContRole.Role = contactRoleType;
            	oppContRole.IsPrimary = false;
            	insert oppContRole;
            	System.debug('Saved new OpportunityContactRole: ' + oppContRole);
            }else{
                ocrs[0].ContactId = contactId; //allow only 1 site contact per opportunity
                update ocrs[0];
             	System.debug('Updating existing OpportunityContactRole linking Opportunity and Site Contact');
            }
            
            return contactId;
        }
        catch(Exception e){
            GlobalUtility.logMessage('Error', 
                                     'NS_ContactDetailsController', 
                                     'addSiteContactToContactsAndOppContactRole', 
                                     jsonContactDetails, 
                                     'Could not add/update Site Contact on new NS Order ' + strOrderId + ' for Quote ' + quoteId, 
                                     e);
            System.debug('Could not add/update Site Contact on new NS Order: ' + e);
            throw new AuraHandledException(e.getMessage());
        }
    }



    @AuraEnabled
    public static void unlinkSiteContactToContactsAndOppContactRole (
                String contactRoleType,
                Id quoteId,
                String strOrderId ) {

        try{
            /*Save contact details to OrderJson*/
            if(String.isNotBlank(strOrderId)){
                DF_Order__c orderRec = [
                        SELECT  Id,
                                Order_JSON__c
                        FROM DF_Order__c
                        WHERE Id = :strOrderId
                        LIMIT 1
                ];
                string ordrJson = orderRec.Order_JSON__c;
                NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot mainWrap = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot)JSON.deserialize(ordrJson, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class);
                list<NS_OrderDataJSONWrapper.ItemInvolvesContact> tempItemCons = new List<NS_OrderDataJSONWrapper.ItemInvolvesContact>();

                // remove all sites from the contact list
                for(Integer i=0; i<mainWrap.resourceOrder.itemInvolvesContact.size(); i++){
                    NS_OrderDataJSONWrapper.ItemInvolvesContact item = mainWrap.resourceOrder.itemInvolvesContact.get(i);
                    if('Site' != item.type){
                        // Add all existing business contacts and only allow a single site contact per order
                        tempItemCons.add(item);
                    }
                }

                mainWrap.resourceOrder.itemInvolvesContact = tempItemCons;
                string orderJsonUpdate = JSON.serialize(mainWrap);
                orderRec.Order_JSON__c = orderJsonUpdate;
                Database.update(orderRec);
                system.debug('---orderJsonUpdate-- '+orderJsonUpdate);
            }
            /*End of Order Json update logic*/

            DF_Quote__c dfQuote = [
                    SELECT Opportunity__c
                    FROM DF_Quote__c
                    WHERE Id =:quoteId
                    LIMIT 1 ];

            List<OpportunityContactRole> ocrs = [
                    SELECT  Id
                    FROM OpportunityContactRole
                    WHERE OpportunityId =:dfQuote.Opportunity__c AND Role =:contactRoleType
                    LIMIT 1 ];

            if ( !ocrs.isEmpty() ) {
                delete ocrs[0];
                System.debug('Updating existing OpportunityContactRole linking Opportunity and Site Contact');
            }

            return;
        }
        catch(Exception e) {
            GlobalUtility.logMessage('Error',
                    'NS_ContactDetailsController',
                    'unlinkSiteContactToContactsAndOppContactRole',
                    '',
                    'Could not unlink Site Contact on new NS Order ' + strOrderId + ' for Quote ' + quoteId,
                    e);
            System.debug('Could not unlink Site Contact on new NS Order: ' + e);
            throw new AuraHandledException(e.getMessage());
        }
    }



    @AuraEnabled
    public static String addBusinessContactToContactsAndOppContactRole(String contactRoleType, String jsonContactDetails, Id quoteId, String strOrderId){

        try{
            NS_BusinessContactDetails contactDetails = (NS_BusinessContactDetails)JSON.deserialize(jsonContactDetails, NS_BusinessContactDetails.class);
            System.debug('Deserialised business contact from JSON: ' + contactDetails);
            
            if(!validateEmail(contactDetails.busContEmail)){
                DmlException e = new DmlException();
                e.setMessage('INVALID_EMAIL_ADDRESS');
                throw e;
            }
            
            Id accountRecordTypeId = Schema.SobjectType.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId();
            List<Contact> existingContacts = [
                SELECT  Id,
                        FirstName,
                        LastName,
                        Email,
                        Phone,
                        Title,
                        MailingState,
                        MailingStreet,
                        MailingPostalCode,
                        MailingCity
                FROM Contact
                WHERE FirstName =:contactDetails.busContFirstName 
                    AND LastName =:contactDetails.busContSurname
                    AND (Phone =:contactDetails.busContNumber OR Email =:contactDetails.busContEmail)
                    AND recordtypeid=:accountRecordTypeId
                LIMIT 1];

            Contact c = new Contact();
            c.LastName = contactDetails.busContSurname;
            c.FirstName = contactDetails.busContFirstName;
            c.Email = contactDetails.busContEmail;
            c.Phone= contactDetails.busContNumber;
            c.MailingStreet = contactDetails.busContStreetAddress;
            c.MailingPostalCode = contactDetails.busContPostcode;
            c.MailingCity = contactDetails.busContSuburb;
            c.MailingState = contactDetails.busContState;
            c.Title = contactDetails.busContRole;
            
            Id contactId;
            if(existingContacts.isEmpty()){
                c.RecordTypeId = accountRecordTypeId;
			    Database.DMLOptions dml = new Database.DMLOptions();
                dml.DuplicateRuleHeader.AllowSave = true;
                contactId = Database.insert(c,dml).getId();
                System.debug('Created new Contact '+ c);
            }else{
                c.Id = existingContacts[0].Id;
                contactId = Database.update(c).getId();
                System.debug('Updated existing Contact '+ c);
            }
            
            /*Save contact details to OrderJson*/
            if(String.isNotBlank(strOrderId)){
                DF_Order__c orderRec = [
                    SELECT  Id,
                            Order_JSON__c
                    FROM DF_Order__c
                    WHERE Id = :strOrderId 
                    LIMIT 1
                ];
                string ordrJson = orderRec.Order_JSON__c;
                NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot mainWrap = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot)JSON.deserialize(ordrJson, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class);
                
                list<NS_OrderDataJSONWrapper.ItemInvolvesContact> tempItemCons = new List<NS_OrderDataJSONWrapper.ItemInvolvesContact>();
                
                for(Integer i=0; i<mainWrap.resourceOrder.itemInvolvesContact.size(); i++){
                    NS_OrderDataJSONWrapper.ItemInvolvesContact item = mainWrap.resourceOrder.itemInvolvesContact.get(i);
                    if('Business' != item.type){
                        //Add all existing site contacts and only allow a single business contact per order
                        tempItemCons.add(item);
                    }
                }
                
                NS_OrderDataJSONWrapper.ItemInvolvesContact itemInvolvesContact = new NS_OrderDataJSONWrapper.ItemInvolvesContact();
                System.debug('Order ItemInvolvesContact: ' + itemInvolvesContact);
                itemInvolvesContact.contactName= contactDetails.busContFirstName+' '+contactDetails.busContSurname;
                itemInvolvesContact.emailAddress=contactDetails.busContEmail;
                itemInvolvesContact.phoneNumber=contactDetails.busContNumber;
                itemInvolvesContact.type='Business';
                itemInvolvesContact.role=contactDetails.busContRole;
                itemInvolvesContact.unstructuredAddress = new NS_OrderDataJSONWrapper.UnstructuredAddress();
                itemInvolvesContact.unstructuredAddress.addressLine1=contactDetails.busContStreetAddress;               
                itemInvolvesContact.unstructuredAddress.localityName=contactDetails.busContSuburb;
                itemInvolvesContact.unstructuredAddress.postcode=contactDetails.busContPostcode;
                itemInvolvesContact.unstructuredAddress.stateTerritoryCode=contactDetails.busContState;
                tempItemCons.add(itemInvolvesContact);
                
                mainWrap.resourceOrder.itemInvolvesContact = tempItemCons;
                string orderJsonUpdate = JSON.serialize(mainWrap);
                orderRec.Order_JSON__c = orderJsonUpdate;
                Database.update(orderRec);
                system.debug('---orderJsonUpdate-- '+orderJsonUpdate);
            }
            /*End of Order Json update logic*/

            DF_Quote__c dfQuote = [SELECT Opportunity__c FROM DF_Quote__c WHERE Id =:quoteId  LIMIT 1];

            OpportunityContactRole[] ocrs = [
                SELECT  Id,
                        ContactId,
                        OpportunityId,
                        Role,
                        IsPrimary
                FROM OpportunityContactRole 
                WHERE OpportunityId =:dfQuote.Opportunity__c AND Role =:contactRoleType 
                LIMIT 1
            ];
          
            if(ocrs.isEmpty()){
                OpportunityContactRole oppContRole = new OpportunityContactRole();
            	oppContRole.ContactId = contactId;
                oppContRole.OpportunityId = dfQuote.Opportunity__c;
            	oppContRole.Role = contactRoleType;
            	oppContRole.IsPrimary = false;
            	insert oppContRole;
                System.debug('Saved new OpportunityContactRole: ' + oppContRole);
                
//				EE updates the Quote as follows but do we need to do this for NS ?              
//            	dfQuote.Business_Contact_Name__c = contactDetails.busContFirstName+' '+contactDetails.busContSurname;
//            	update dfQuote;
            }else{
                ocrs[0].ContactId = contactId; //allow only 1 business contact per opportunity
                update ocrs[0];
             	System.debug('Updating existing OpportunityContactRole linking Opportunity and Business Contact');
           	}
            
            return contactId;
        }
        catch(Exception e){
            GlobalUtility.logMessage('Error', 
                                     'NS_ContactDetailsController', 
                                     'addBusinessContactToContactsAndOppContactRole', 
                                     jsonContactDetails, 
                                     'Could not add/update Business Contact on new NS Order ' + strOrderId + ' for Quote ' + quoteId, 
                                     e);
            System.debug('Could not add/update Business Contact on new NS Order: ' + e);
           	throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String getBusinessContactsAndOppContactRole( Id quoteId,String oppName ){

        System.debug('QuoteId:' + quoteId + ' oppName:' + oppName);
        try{
            List<OpportunityContactRole> opptyContactList  = [
                SELECT  ContactId,
                        Contact.FirstName,
                        Contact.LastName,
                        Contact.Email ,
                        Contact.Phone,
                        Contact.MailingStreet,
                        Contact.MailingCity,
                        Contact.MailingPostalCode,
                        Contact.MailingState ,
                        Contact.Title
                FROM OpportunityContactRole 
                WHERE Role = 'Business Contact' 
                AND Opportunity.Name=:oppName 
                AND OpportunityId IN (SELECT Opportunity__c FROM DF_Quote__c WHERE Id =:quoteId)
            ];

            List<NS_BusinessContactDetails> lstBusinessContactDetails = new List<NS_BusinessContactDetails>();
         
            if (!opptyContactList.isEmpty()) {
                for(OpportunityContactRole opc :opptyContactList){
                    NS_BusinessContactDetails bContact = new NS_BusinessContactDetails(
                                            opc.ContactId,
                                            opc.Contact.FirstName,
                                            opc.Contact.LastName,
                                            opc.Contact.Title,
                                            opc.Contact.Email,
                                            opc.Contact.Phone,
                                            opc.Contact.MailingStreet,
                                            opc.Contact.MailingCity,
                                            opc.Contact.MailingPostalCode,
                                            opc.Contact.MailingState);
                    lstBusinessContactDetails.add(bContact);
                }
            }
            String serializedData = json.serialize(lstBusinessContactDetails);
            System.debug('!!! getBusinessContactsAndOppContactRole '+ serializedData);
            return serializedData;
        }
        catch(Exception e){
            System.debug('Error in NS_ContactDetailsController.getBusinessContactsAndOppContactRole: ' + e);
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String getSiteContactsAndOppContactRole( Id quoteId, String oppName ){

        System.debug('QuoteId:' + quoteId + ' oppName:' + oppName);
        try{
            List<OpportunityContactRole> opptyContactList = [
                SELECT  ContactId,
                        Contact.FirstName,
                        Contact.LastName,
                        Role,
                        Contact.Email ,
                        Contact.Phone
                FROM OpportunityContactRole 
                WHERE Role = 'Site Contact'
      			AND Opportunity.Name=:oppName 
                AND OpportunityId IN (SELECT Opportunity__c FROM DF_Quote__c WHERE Id =:quoteId)
            ];

            List<NS_ContactDetails> lstBusinessContactsData = new List<NS_ContactDetails>();

            If(!opptyContactList.isEmpty()){
                for(OpportunityContactRole cd : opptyContactList){
                    NS_ContactDetails sContact = new NS_ContactDetails(cd.ContactId, cd.Contact.FirstName, cd.Contact.LastName,cd.Contact.Phone,cd.Contact.Email);
                    lstBusinessContactsData.add(sContact);
                }
            }
            String serializedData = json.serialize(lstBusinessContactsData);
            System.debug('!!! getSiteContactsAndOppContactRole '+ serializedData);
            return serializedData;
        }
        catch(Exception e){
            System.debug('Error in NS_ContactDetailsController.getSiteContactsAndOppContactRole: ' + e);
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static Boolean validateEmail(String email) {
        Pattern MyPattern = Pattern.compile('^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$');
        return MyPattern.matcher(email).matches();
    }
}