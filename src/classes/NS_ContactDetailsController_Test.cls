@isTest
private class NS_ContactDetailsController_Test {


    @isTest static void testShouldAllowOnlyOneSiteContactAndOneBusinessContactPerOpportunity() {

        User dfuser = createDFCommUser();

        system.runAs(dfuser) {

            PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'NS_BC_Customer_Plus_Permission'];
            insert new PermissionSetAssignment(AssigneeId = dfuser.id, PermissionSetId = ps.Id);

            // Create OpptyBundle
            DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(dfuser.AccountId);
            insert opptyBundle;

            // Create Oppty
            Opportunity oppty = SF_TestData.createOpportunity('LOC111111111111');
            oppty.Opportunity_Bundle__c = opptyBundle.Id;
            insert oppty;

            // Create DFQuote
            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';
            DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');
            dfQuote1.Proceed_to_Pricing__c = true;
            List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
            dfQuoteList.add(dfQuote1);
            insert dfQuoteList;

            //insert order
            DF_Order__c testOrder = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle.Id);
            testOrder.Heritage_Site__c = 'Yes';
            testOrder.Induction_Required__c = 'Yes';
            testOrder.Security_Required__c = 'Yes';
            testOrder.Site_Notes__c = 'Site Notes';
            testOrder.Trading_Name__c = 'Trader Joes TEST SITE';
            testOrder.Order_JSON__c = '{"resourceOrder":{"workPracticesRequired":"Yes","workingAtHeightsConsideration":"Yes","tradingName":"asas","siteType":"Residential","siteName":"asas","siteAccessInstructions":"as","securityRequirements":"as","safetyHazards":"Yes","resourceOrderType":"nbnSelect","resourceOrderComprisedOf":{"referencesResourceOrderItem":[{"itemInvolvesResource":{"type":"NTD","batteryBackupRequired":"Yes"},"action":"ADD"}],"itemInvolvesLocation":{"id":"LOC000100874844"}},"propertyOwnershipStatus":"Leased","ownerAwareOfProposal":"No","orderType":"Connect","notes":"asasas","keysRequired":"No","jsaswmRequired":"No","itemInvolvesContact":[{"unstructuredAddress":{"stateTerritoryCode":null,"postcode":null,"localityName":null,"addressLine3":null,"addressLine2":null,"addressLine1":null},"type":null,"role":null,"phoneNumber":null,"emailAddress":null,"contactName":null,"company":null}],"interactionStatus":null,"inductionRequired":"No","id":null,"heritageSite":"Yes","dateReceived":"2018-09-25T05:04:52.059Z","customerRequiredDate":"2018-12-06","contractedLocationInstructions":"as","confinedSpaceRequirements":"Yes","associatedReferenceId":"NSQ-0000000594","afterHoursSiteVisit":"Yes","additionalNotes":"asassa","accessSeekerInteraction":{"billingAccountID":null},"accessSeekerContact":{"contactPhone":null,"contactName":null}},"notificationType":null}';
            Insert testOrder;

            String siteContactJson = '{"siteContId":"","siteContIndex":1,"siteContFirstName":"SiteAFirstName","siteContSurname":"SiteASurName","siteContEmail":"test@site.com","siteContNumber":"0412123122"}';
            String siteContactJson2 = '{"siteContId":"","siteContIndex":2,"siteContFirstName":"SiteBFirstName","siteContSurname":"SiteBSurName","siteContEmail":"test@siteb.com","siteContNumber":"0412123123"}';
            String businessContactJson = '{"busContIndex":1,"busContId":"","busContFirstName":"BusinessAFirstname","busContSurname":"BusinessASurname","busContRole":"BusinessARole","busContEmail":"business@contact.com","busContNumber":"0412123122","busContStreetAddress":"Business St","busContSuburb":"Business Town","busContPostcode":"2000","busContState":"NSW"}';
            String businessContactJson2 = '{"busContIndex":2,"busContId":"","busContFirstName":"BusinessBFirstname","busContSurname":"BusinessBSurname","busContRole":"BusinessBRole","busContEmail":"business@contactb.com","busContNumber":"0412123123","busContStreetAddress":"BusinessB St","busContSuburb":"BusinessB Town","busContPostcode":"3000","busContState":"VIC"}';

            test.startTest();

                NS_ContactDetailsController.addBusinessContactToContactsAndOppContactRole('Business Contact', businessContactJson, dfQuote1.Id, testOrder.id);
                NS_ContactDetailsController.addBusinessContactToContactsAndOppContactRole('Business Contact', businessContactJson2, dfQuote1.Id, testOrder.id);
                NS_ContactDetailsController.addSiteContactToContactsAndOppContactRole('Site Contact', siteContactJson, dfQuote1.Id, testOrder.id);
                NS_ContactDetailsController.addSiteContactToContactsAndOppContactRole('Site Contact', siteContactJson2, dfQuote1.Id, testOrder.id);

                List<NS_BusinessContactDetails> lstBusinessContactDetails = (List<NS_BusinessContactDetails>) JSON.deserialize(NS_ContactDetailsController.getBusinessContactsAndOppContactRole(testOrder.DF_Quote__c, oppty.Name), List<NS_BusinessContactDetails>.class);
                System.assertEquals(1, lstBusinessContactDetails.size());
                NS_BusinessContactDetails businessContactDetails = lstBusinessContactDetails.get(0);
                System.assertEquals('BusinessBFirstname', businessContactDetails.busContFirstName);
                System.assertEquals('BusinessBSurname', businessContactDetails.busContSurname);
                System.assertEquals('BusinessBRole', businessContactDetails.busContRole);
            	System.assertEquals('business@contactb.com', businessContactDetails.busContEmail);
            	System.assertEquals('0412123123', businessContactDetails.busContNumber);
            	System.assertEquals('BusinessB St', businessContactDetails.busContStreetAddress);
            	System.assertEquals('BusinessB Town', businessContactDetails.busContSuburb);
            	System.assertEquals('3000', businessContactDetails.busContPostcode);
            	System.assertEquals('VIC', businessContactDetails.busContState);

                List<NS_ContactDetails> lstSiteContactDetails = (List<NS_ContactDetails>) JSON.deserialize(NS_ContactDetailsController.getSiteContactsAndOppContactRole(testOrder.DF_Quote__c, oppty.Name), List<NS_ContactDetails>.class);
                System.assertEquals(1, lstSiteContactDetails.size());
                NS_ContactDetails siteContactDetails = lstSiteContactDetails.get(0);
                System.assertEquals('SiteBFirstName', siteContactDetails.siteContFirstName);
                System.assertEquals('SiteBSurName', siteContactDetails.siteContSurname);
            	System.assertEquals('test@siteb.com', siteContactDetails.siteContEmail);
            	System.assertEquals('0412123123', siteContactDetails.siteContNumber);

                List<OpportunityContactRole> businessOppContactRoles = [
                    SELECT Id, Contact.FirstName, Contact.LastName, Contact.Email, Contact.Phone
                    FROM OpportunityContactRole 
                    WHERE OpportunityId = :oppty.Id AND Role = 'Business Contact'
                ];
                List<OpportunityContactRole> siteOppContactRoles = [
                    SELECT Id, Contact.FirstName, Contact.LastName, Contact.Email, Contact.Phone 
                    FROM OpportunityContactRole 
                    WHERE OpportunityId = :oppty.Id AND Role = 'Site Contact'
                ];

                System.assertEquals(1, businessOppContactRoles.size());
            	System.assertEquals('BusinessBFirstname', businessOppContactRoles[0].Contact.FirstName);
                System.assertEquals('BusinessBSurname', businessOppContactRoles[0].Contact.LastName);
            	System.assertEquals('business@contactb.com', businessOppContactRoles[0].Contact.Email);
            	System.assertEquals('0412123123', businessOppContactRoles[0].Contact.Phone);
            
                System.assertEquals(1, siteOppContactRoles.size());
     			System.assertEquals('SiteBFirstName', siteOppContactRoles[0].Contact.FirstName);
                System.assertEquals('SiteBSurName', siteOppContactRoles[0].Contact.LastName);
            	System.assertEquals('test@siteb.com', siteOppContactRoles[0].Contact.Email);
            	System.assertEquals('0412123123', siteOppContactRoles[0].Contact.Phone);
            
            test.stopTest();
        }

    }


    @isTest static void testShouldUnlinkSiteContact() {

        User dfuser = createDFCommUser();

        system.runAs(dfuser) {

            PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'NS_BC_Customer_Plus_Permission'];
            insert new PermissionSetAssignment(AssigneeId = dfuser.id, PermissionSetId = ps.Id);

            // Create OpptyBundle
            DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(dfuser.AccountId);
            insert opptyBundle;

            // Create Oppty
            Opportunity oppty = SF_TestData.createOpportunity('LOC111111111111');
            oppty.Opportunity_Bundle__c = opptyBundle.Id;
            insert oppty;

            // Create DFQuote
            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';
            DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');
            dfQuote1.Proceed_to_Pricing__c = true;
            List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
            dfQuoteList.add(dfQuote1);
            insert dfQuoteList;

            //insert order
            DF_Order__c testOrder = new DF_Order__c(DF_Quote__c = dfQuoteList[0].Id, Opportunity_Bundle__c = opptyBundle.Id);
            testOrder.Heritage_Site__c = 'Yes';
            testOrder.Induction_Required__c = 'Yes';
            testOrder.Security_Required__c = 'Yes';
            testOrder.Site_Notes__c = 'Site Notes';
            testOrder.Trading_Name__c = 'Trader Joes TEST SITE';
            testOrder.Order_JSON__c = '{"resourceOrder":{"workPracticesRequired":"Yes","workingAtHeightsConsideration":"Yes","tradingName":"asas","siteType":"Residential","siteName":"asas","siteAccessInstructions":"as","securityRequirements":"as","safetyHazards":"Yes","resourceOrderType":"nbnSelect","resourceOrderComprisedOf":{"referencesResourceOrderItem":[{"itemInvolvesResource":{"type":"NTD","batteryBackupRequired":"Yes"},"action":"ADD"}],"itemInvolvesLocation":{"id":"LOC000100874844"}},"propertyOwnershipStatus":"Leased","ownerAwareOfProposal":"No","orderType":"Connect","notes":"asasas","keysRequired":"No","jsaswmRequired":"No","itemInvolvesContact":[{"unstructuredAddress":{"stateTerritoryCode":null,"postcode":null,"localityName":null,"addressLine3":null,"addressLine2":null,"addressLine1":null},"type":null,"role":null,"phoneNumber":null,"emailAddress":null,"contactName":null,"company":null}],"interactionStatus":null,"inductionRequired":"No","id":null,"heritageSite":"Yes","dateReceived":"2018-09-25T05:04:52.059Z","customerRequiredDate":"2018-12-06","contractedLocationInstructions":"as","confinedSpaceRequirements":"Yes","associatedReferenceId":"NSQ-0000000594","afterHoursSiteVisit":"Yes","additionalNotes":"asassa","accessSeekerInteraction":{"billingAccountID":null},"accessSeekerContact":{"contactPhone":null,"contactName":null}},"notificationType":null}';
            Insert testOrder;

            String siteContactJson = '{' +
                    '"siteContId":"",' +
                    '"siteContIndex":1,' +
                    '"siteContFirstName":"SiteAFirstName",' +
                    '"siteContSurname":"SiteASurName",' +
                    '"siteContEmail":"test@site.com",' +
                    '"siteContNumber":"0412123122"}';

            String businessContactJson = '{' +
                    '"busContIndex":1,' +
                    '"busContId":"",' +
                    '"busContFirstName":"BusinessAFirstname",' +
                    '"busContSurname":"BusinessASurname",' +
                    '"busContRole":"BusinessARole",' +
                    '"busContEmail":"business@contact.com",' +
                    '"busContNumber":"0412123122",' +
                    '"busContStreetAddress":"Business St",' +
                    '"busContSuburb":"Business Town",' +
                    '"busContPostcode":"2000",' +
                    '"busContState":"NSW"}';

            test.startTest();

            // business contact
            NS_ContactDetailsController.addBusinessContactToContactsAndOppContactRole(
                    'Business Contact',
                    businessContactJson,
                    dfQuote1.Id,
                    testOrder.id);

            // site contact
            NS_ContactDetailsController.addSiteContactToContactsAndOppContactRole(
                    'Site Contact',
                    siteContactJson,
                    dfQuote1.Id,
                    testOrder.id);

            // now unlink one
            NS_ContactDetailsController.unlinkSiteContactToContactsAndOppContactRole(
                    'Site Contact',
                    dfQuote1.Id,
                    testOrder.id);

            List<NS_BusinessContactDetails> lstBusinessContactDetails = (List<NS_BusinessContactDetails>) JSON.deserialize(NS_ContactDetailsController.getBusinessContactsAndOppContactRole(testOrder.DF_Quote__c, oppty.Name), List<NS_BusinessContactDetails>.class);

            System.assertEquals(1, lstBusinessContactDetails.size());

            NS_BusinessContactDetails businessContactDetails = lstBusinessContactDetails.get(0);
            System.assertEquals('BusinessAFirstname', businessContactDetails.busContFirstName);
            System.assertEquals('BusinessASurname', businessContactDetails.busContSurname);
            System.assertEquals('BusinessARole', businessContactDetails.busContRole);
            System.assertEquals('business@contact.com', businessContactDetails.busContEmail);
            System.assertEquals('0412123122', businessContactDetails.busContNumber);
            System.assertEquals('Business St', businessContactDetails.busContStreetAddress);
            System.assertEquals('Business Town', businessContactDetails.busContSuburb);
            System.assertEquals('2000', businessContactDetails.busContPostcode);
            System.assertEquals('NSW', businessContactDetails.busContState);

            List<NS_ContactDetails> lstSiteContactDetails = (List<NS_ContactDetails>) JSON.deserialize(NS_ContactDetailsController.getSiteContactsAndOppContactRole(testOrder.DF_Quote__c, oppty.Name), List<NS_ContactDetails>.class);
            System.assertEquals(0, lstSiteContactDetails.size());

            List<OpportunityContactRole> businessOppContactRoles = [
                    SELECT Id, Contact.FirstName, Contact.LastName, Contact.Email, Contact.Phone
                    FROM OpportunityContactRole
                    WHERE OpportunityId = :oppty.Id AND Role = 'Business Contact'
            ];

            List<OpportunityContactRole> siteOppContactRoles = [
                    SELECT Id, Contact.FirstName, Contact.LastName, Contact.Email, Contact.Phone
                    FROM OpportunityContactRole
                    WHERE OpportunityId = :oppty.Id AND Role = 'Site Contact'
            ];

            System.assertEquals(1, businessOppContactRoles.size());
            System.assertEquals('BusinessAFirstname', businessOppContactRoles[0].Contact.FirstName);
            System.assertEquals('BusinessASurname', businessOppContactRoles[0].Contact.LastName);
            System.assertEquals('business@contact.com', businessOppContactRoles[0].Contact.Email);
            System.assertEquals('0412123122', businessOppContactRoles[0].Contact.Phone);

            System.assertEquals(0, siteOppContactRoles.size());

            test.stopTest();
        }

    }



    // Create df community user and related records
    public static User createDFCommUser() {
        Profile commProfile = [
                SELECT Id
                FROM Profile
                WHERE Name = 'DF Partner User'
        ];

        system.debug('createDFCommUser - commProfile: ' + commProfile);

        Account commAcct = new Account();
        commAcct.Name = 'My account';
        commAcct.Tier__c = '1';
        //commAcct.Number_of_Employees_for_ICT__c = '1';
        insert commAcct;
        system.debug('createDFCommUser - test account inserted...: account id: ' + commAcct.Id);

        Contact commContact = new Contact();
        commContact.LastName = 'testContact';
        commContact.AccountId = commAcct.Id;
        insert commContact;
        system.debug('createDFCommUser - test contact inserted...: contact id: ' + commContact.Id);

        User commUser = new User();
        commUser.Alias = 'test123';
        commUser.Email = 'test123@noemail.com';
        commUser.Emailencodingkey = 'ISO-8859-1';
        commUser.Lastname = 'Testing';
        commUser.Languagelocalekey = 'en_US';
        commUser.Localesidkey = 'en_AU';
        commUser.Profileid = commProfile.Id;
        commUser.IsActive = true;
        commUser.ContactId = commContact.Id;
        commUser.Timezonesidkey = 'Australia/Sydney';
        commUser.Username = 'tester@noemail.com.test';

        insert commUser;
        system.debug('createDFCommUser - test user inserted...: user id: ' + commUser);


        return commUser;
    }
}