/*
Description : 
Author : 
TestClass : 
Date : 
*/

public class NS_CostVarianceAcceptReject{
    
    public static void updateCSRecorsInCVAccept(String dfQuoteId){

        DF_Quote__c dfQuote = [SELECT Id, Name, Opportunity__c, New_Build_Cost__c FROM DF_Quote__c WHERE Id =: dfQuoteId LIMIT 1][0];
        ID opportunityId = dfQuote.Opportunity__c;
        String pcId;
        Decimal costVar;
        List<cscfga__Product_Configuration__c> pcList = new List<cscfga__Product_Configuration__c>();
        List<csord__Order_Line_Item__c> oliList = new List<csord__Order_Line_Item__c>();

        if(dfQuote != null && dfQuote.Opportunity__c != null){
            
            costVar= dfQuote.New_Build_Cost__c;
            System.debug('<><><<>'+dfQuote);
            List<csord__Order__c> ordList= [Select Id, csord__Status2__c, csord__Identification__c, (SELECT Id, Name, csord__Total_Price__c FROM csord__Order_Line_Items__r WHERE Name = 'Fibre Build Cost - NSAS_NRC_FBC') FROM csord__Order__c WHERE csordtelcoa__Opportunity__c =:dfQuote.Opportunity__c];
            system.debug('ordToUpdateList<><> '+ordList[0].csord__Order_Line_Items__r);
            for(csord__Order__c ord : ordList){
                for(csord__Order_Line_Item__c oli : ord.csord__Order_Line_Items__r){
                    oli.csord__Total_Price__c = costVar;
                    oliList.add(oli);
                }
                pcId = ord.csord__Identification__c.substringAfter('_').substringBefore('_0');  
                system.debug('<><><><>'+pcId);
            }
            
            if(oliList.size()>0){
                try{
                    Database.update(oliList);
                }
                catch(DmlException e){
                    System.debug('The following exception has occurred: ' + e.getMessage());
                }
                
            }
            System.debug('<><><><>');

            List<Opportunity> oppList = [SELECT Id, amount FROM Opportunity WHERE Id =: opportunityId LIMIT 1];
            System.debug('<><><><>'+oppList);
            if(oppList.size()>0){
                oppList[0].amount = costVar;
                try{
                    Database.update(oppList);
                }
                catch(DmlException e){
                    System.debug('The following exception has occurred: ' + e.getMessage());
                }
            }

            if(pcId != null){
                pcList = [Select Id, Name, cscfga__Product_Basket__c FROM cscfga__Product_Configuration__c WHERE Id =: pcId];
            }

            if(pcList.size()>0){
                cscfga__Product_Basket__c basket = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c where id = :pcList[0].cscfga__Product_Basket__c];
                cscfga.API_1.ApiSession apiSession = DF_CS_API_Util.createApiSession(basket);
                apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = pcList[0].Id, cscfga__Product_Basket__c = pcList[0].cscfga__Product_Basket__c));        
                cscfga.ProductConfiguration currConfig = apiSession.getConfiguration();

                for(cscfga.Attribute att : currConfig.getAttributes()) {
                    //System.debug('Attribute name is : ' + att);
                    if(att.getName() == 'FibreBuildChargeOverride'){
                        att.setValue(String.valueOf(costVar)); 
                    }
                }

                apiSession.executeRules(); 
                apiSession.validateConfiguration();
                apiSession.persistConfiguration();
            }
        }

    }

}