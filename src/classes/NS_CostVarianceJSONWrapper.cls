public class NS_CostVarianceJSONWrapper {
    public class NS_CostVarianceJSONRoot{
        //public NS_OrderNotifierJSONWrapper.Headers headers{get;set;}
        public Body body{get;set;}
        public NS_CostVarianceJSONRoot(){
            /*this.headers = new NS_OrderNotifierJSONWrapper.Headers();
            this.headers.msgName = null;
            this.headers.msgType = null;
            this.headers.security = null;
            this.headers.timestamp = null;
            this.headers.activityName = null;
            this.headers.correlationId = null;
            this.headers.orderPriority = null;
            this.headers.accessSeekerID = null;
            this.headers.businessServiceName = null;
            this.headers.communicationPattern = null;
            this.headers.businessProcessVersion = null;
            this.headers.businessServiceVersion = null;*/
            
            this.body = new Body();
            this.body.manageResourceOrderRequest = new ManageResourceOrderRequest();
            this.body.manageResourceOrderRequest.resourceOrder = new ResourceOrder();
            this.body.manageResourceOrderRequest.resourceOrder.id = null;
            this.body.manageResourceOrderRequest.resourceOrder.action = null;
        }
    }
    public class Body{
        public ManageResourceOrderRequest manageResourceOrderRequest{get;set;}
    }
    public class ManageResourceOrderRequest{
        public ResourceOrder resourceOrder{get;set;}
    }
    public class ResourceOrder{
        public String id{get;set;}
        public String action{get;set;}
    }
}