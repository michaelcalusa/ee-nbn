/**
 * Created by gobindkhurana on 4/9/18.
 */

@isTest(SeeAllData=true)
private class NS_HomeController_Test {

    @isTest public static void test_getUserDetails() {
        User commUser;

        commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser){
            test.startTest();

            NS_HomeController.getUserDetails();

            test.stopTest();
        }
    }
}