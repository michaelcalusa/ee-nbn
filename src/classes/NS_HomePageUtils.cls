/**
 * Created by gobindkhurana on 4/9/18.
 */

public with sharing class NS_HomePageUtils {
//Includes all the utility methods for Home (Landing page)

/*
* Gets the logged in user details based on the permission sets assigned to the user
* Parameters : N/A
* @Return : Returns a string which says the type of user
*/

public static String getLoggedInUserDetails() {
    String userDetails = null;

    boolean NSUsrCredentials =NS_SF_Utils.hasValidPermissions();

    if(NSUsrCredentials == true)
    {
        userDetails = 'businessPlus';
    }
    else
    {
        userDetails = 'business';
    }
return userDetails;
}
}