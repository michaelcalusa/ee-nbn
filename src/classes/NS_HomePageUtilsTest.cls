/**
 * Created by gobindkhurana on 7/9/18.
 */

@isTest(SeeAllData=true)
public class NS_HomePageUtilsTest {

    @isTest public static void test_getUserDetails() {
        User commUser;

        commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser){
            test.startTest();

            NS_HomePageUtils.getLoggedInUserDetails();

            test.stopTest();
        }
    }
}