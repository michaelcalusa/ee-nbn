/**
 * Created by gobindkhurana on 4/9/18.
 */

@isTest(SeeAllData=true)
private class NS_NavigationBurgerMenuController_Test {


    @isTest public static void test_getUserDetails() {
        User commUser;

        commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser){
            test.startTest();

            NS_NavigationWithBurgerMenuController.getUserDetails();

            test.stopTest();
        }
    }
}