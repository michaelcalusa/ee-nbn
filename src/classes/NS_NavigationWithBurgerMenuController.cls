/**
 * Created by gobindkhurana on 4/9/18.
 */

public with sharing class NS_NavigationWithBurgerMenuController {

//Includes the methods for displaying Navigation bar with burger menu

/*
* Gets the logged in user details based on the permission sets assigned to the user
* Parameters : N/A
* @Return : Returns a string which says the type of user
*/
    @AuraEnabled
    public static String getUserDetails() {
        String userDetails = null;
        try {
            userDetails = NS_HomePageUtils.getLoggedInUserDetails();
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
        return userDetails;
    }
}