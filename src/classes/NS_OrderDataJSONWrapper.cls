public class NS_OrderDataJSONWrapper {
    public class NS_OrderDataJSONRoot{
        @AuraEnabled
        public ResourceOrder resourceOrder {get;set;}
        @AuraEnabled
        public String notificationType{get;set;}
        @AuraEnabled
        public String notificationReason{get;set;}

        public NS_OrderDataJSONRoot(){
            this.notificationType=null;
            this.resourceOrder = new ResourceOrder();
            this.resourceOrder.notes =null;
            this.resourceOrder.note =null;
            this.resourceOrder.orderType=null;
            this.resourceOrder.resourceOrderType=null;
            this.resourceOrder.associatedReferenceId=null;
            this.resourceOrder.siteName=null;
            this.resourceOrder.siteType=null;
            this.resourceOrder.tradingName=null;
            this.resourceOrder.heritageSite=null;
            this.resourceOrder.inductionRequired=null;
            this.resourceOrder.dateReceived=null;
            this.resourceOrder.customerRequiredDate=null;
            this.resourceOrder.afterHoursSiteVisit=null;
            this.resourceOrder.propertyOwnershipStatus=null;
            this.resourceOrder.keysRequired=null;
            this.resourceOrder.securityRequirements=null;
            this.resourceOrder.siteAccessInstructions=null;
            this.resourceOrder.contractedLocationInstructions=null;
            this.resourceOrder.ownerAwareOfProposal=null;
            this.resourceOrder.safetyHazards=null;
            this.resourceOrder.jsaswmRequired=null;
            this.resourceOrder.workPracticesRequired=null;
            this.resourceOrder.workingAtHeightsConsideration=null;
            this.resourceOrder.additionalNotes=null;
            this.resourceOrder.confinedSpaceRequirements=null;
            this.resourceOrder.id=null;
            this.resourceOrder.interactionStatus=null;
            this.resourceOrder.interactionSubstatus=null;
            this.resourceOrder.accessSeekerInteraction= new AccessSeekerInteraction();
            this.resourceOrder.accessSeekerInteraction.billingAccountID=null;
            this.resourceOrder.accessSeekerContact = new AccessSeekerContact();
            this.resourceOrder.accessSeekerContact.contactName=null;
            this.resourceOrder.accessSeekerContact.contactPhone=null;
            this.resourceOrder.itemInvolvesContact = new List<ItemInvolvesContact>();
            ItemInvolvesContact itemCon = new ItemInvolvesContact();
            itemCon.role=null;
            itemCon.type=null;
            itemCon.contactName=null;
            itemCon.phoneNumber=null;
            itemCon.emailAddress=null;
            //itemCon.company=null;
            itemCon.unstructuredAddress = new UnstructuredAddress();
            itemCon.unstructuredAddress.addressLine1 = null;
            itemCon.unstructuredAddress.addressLine2 = null;
            itemCon.unstructuredAddress.addressLine3 = null;
            itemCon.unstructuredAddress.localityName = null;
            itemCon.unstructuredAddress.postcode = null;
            itemCon.unstructuredAddress.stateTerritoryCode = null;
            this.resourceOrder.itemInvolvesContact.add(itemCon);
            this.resourceOrder.resourceOrderComprisedOf = new ResourceOrderComprisedOf();
            this.resourceOrder.resourceOrderComprisedOf.itemInvolvesLocation = new ItemInvolvesLocation();
            this.resourceOrder.resourceOrderComprisedOf.itemInvolvesLocation.id = null;
            this.resourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem = new List<ReferencesResourceOrderItem>();
            ReferencesResourceOrderItem refResOrderItem = new ReferencesResourceOrderItem();
            refResOrderItem.action = null;
            refResOrderItem.itemInvolvesResource = new ItemInvolvesResource();
            refResOrderItem.itemInvolvesResource.type = null;
            refResOrderItem.itemInvolvesResource.batteryBackupRequired = null;
            this.resourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem.add(refResOrderItem);
            this.resourceOrder.ntdInstallAppointmentDate=null;
            this.resourceOrder.newBuildCost=null;
            this.resourceOrder.plannedCompletionDate=null;
            //CPST-7850 | michaelcalusa@nbnco.com.au | ability to show revised completion date in NS
            this.resourceOrder.revisedCompletionDate=null;
        }
    }
    
    public class ResourceOrder {
        public Note note {get;set;}
        public String notes {get;set;}
        public String orderType {get;set;}
        public String resourceOrderType {get;set;}
        public String associatedReferenceId {get;set;}
        public String siteName {get;set;}
        public String siteType {get;set;}
        public String tradingName {get;set;}
        public String heritageSite {get;set;}
        public String inductionRequired {get;set;}
        public Datetime dateReceived{get;set;}
        public String customerRequiredDate {get;set;}
        public String afterHoursSiteVisit {get;set;}
        public String propertyOwnershipStatus {get;set;}
        public String keysRequired {get;set;}
        public String securityRequirements {get;set;}
        public String siteAccessInstructions {get;set;}
        public String contractedLocationInstructions {get;set;}
        public String ownerAwareOfProposal {get;set;}
        public String safetyHazards {get;set;}
        public String jsaswmRequired {get;set;}
        public String workPracticesRequired {get;set;}
        public String workingAtHeightsConsideration {get;set;}
        public String additionalNotes {get;set;}
        public String confinedSpaceRequirements {get;set;}
        public String id{get;set;}
        public String interactionStatus{get;set;}
        public String interactionSubstatus{get;set;}
        public String interactionDateComplete{get;set;}
        public AccessSeekerInteraction accessSeekerInteraction {get;set;}
        public AccessSeekerContact accessSeekerContact {get;set;}
        public List<ItemInvolvesContact> itemInvolvesContact {get;set;}
        public ResourceOrderComprisedOf resourceOrderComprisedOf {get;set;}
        public String ntdInstallAppointmentDate {get;set;}
        public String newBuildCost {get;set;}
        public String plannedCompletionDate {get;set;}
        //CPST-7850 | michaelcalusa@nbnco.com.au | ability to show revised completion date in NS
        public String revisedCompletionDate {get;set;}
    }

    //CPST-7850 | michaelcalusa@nbnco.com.au | ability to show revised completion date in NS
    public class Note {
        public String id {get;set;}
        public String description {get;set;}
    }
    
    public class AccessSeekerInteraction {
        public String billingAccountID {get;set;}
    }
    public class AccessSeekerContact {
        public String contactName {get;set;}
        public String contactPhone {get;set;}
    }
    
    public class ItemInvolvesContact {
        public String role {get;set;}
        public String type {get;set;}
        public String contactName {get;set;}
        public String phoneNumber {get;set;}
        public String emailAddress {get;set;}
        public UnstructuredAddress unstructuredAddress {get;set;}
    }
    
    public class ResourceOrderComprisedOf {
        public ItemInvolvesLocation itemInvolvesLocation {get;set;}
        public List<ReferencesResourceOrderItem> referencesResourceOrderItem {get;set;}
    }
    
    public class UnstructuredAddress {
        public String addressLine1 {get;set;}
        public String addressLine2 {get;set;}
        public String addressLine3 {get;set;}
        public String localityName {get;set;}
        public String postcode {get;set;}
        public String stateTerritoryCode {get;set;}
    }
    
    public class ItemInvolvesLocation {
        public String id {get;set;}
    }
    
    public class ItemInvolvesResource {
        public String type {get;set;}
        public String batteryBackupRequired {get;set;}
    }
    
    public class ReferencesResourceOrderItem {
        public String action {get;set;}
        public ItemInvolvesResource itemInvolvesResource {get;set;}               
    }

}