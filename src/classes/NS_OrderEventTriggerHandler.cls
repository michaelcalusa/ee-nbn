public class NS_OrderEventTriggerHandler {
    
    public static void eventTypesIdentification(list<NS_Order_Event__e> ordEvents){ 
        System.debug('NS_OrderEventTriggerHandler.eventTypesIdentification(): event size is:' + ordEvents.size());
        
        List<NS_Order_Event__e> appianCalloutList = new List<NS_Order_Event__e>();
        List<NS_Order_Event__e> sfProcessingList = new List<NS_Order_Event__e>();
        
        DF_Order_Settings__mdt ordSetting = NS_Order_Utils.getOrderSettings('NSOrderSettings');
        System.debug('ordSetting value is:' + ordSetting);
        
        Map<String, String> eventIdToEmailTemplateMap = NS_SF_Utils.initEventIdToEmailTemplateMap(ordSetting);
        
        for(NS_Order_Event__e oEvent: ordEvents){ 
            System.debug('Event is:' + oEvent);

            if(ordSetting.OrderCreateCode__c.equalsIgnoreCase(oEvent.Event_Id__c) 
               || ordSetting.OrderCostVarianceCode__c.equalsIgnoreCase(oEvent.Event_Id__c))
            {
            	appianCalloutList.add(oEvent);
            }
            else if (ordSetting.OrderCancelledEventCode__c.equalsIgnoreCase(oEvent.Event_Id__c)) {
                System.debug('Received cancel event, enqueuing new NS_OrderInflightCancellation for eventId ' + oEvent.Event_Id__c);
                System.enqueueJob(new NS_OrderInflightCancellation(oEvent));
            }
            else if(String.isNotBlank(oEvent.Event_Id__c) 
                    && eventIdToEmailTemplateMap.containsKey(oEvent.Event_Id__c.toLowerCase())
                    && ordSetting.OrderAckSource__c.equalsIgnoreCase(oEvent.Source__c))
            {
                sfProcessingList.add(oEvent);
            }
        }
        
        if (!appianCalloutList.isEmpty()) {
            List<List<NS_Order_Event__e>> resultList = (List<List<NS_Order_Event__e>>)SF_CS_API_Util.spliceBy(appianCalloutList,Integer.valueOf(ordSetting.OrderCreateBatchSize__c));
            system.debug('Salesforce to Appain Event batches size: ' + resultList.size());      
            for(List<NS_Order_Event__e> evtLst : resultList){
                System.enqueueJob(new NS_OrdertoAppainEventHandler(evtLst));
            }
        }
        
        if (!sfProcessingList.isEmpty()) {
            List<List<NS_Order_Event__e>> resultList = (List<List<NS_Order_Event__e>>)SF_CS_API_Util.spliceBy(sfProcessingList,Integer.valueOf(ordSetting.OrderAckBatchSize__c));
            system.debug('OrderNotifier to Salesforce Event batches size: ' + resultList.size());
            for(List<NS_Order_Event__e> evtLst : resultList){
                System.enqueueJob(new NS_OrderInboundEventHandler(evtLst));
            }
        }
    }
}