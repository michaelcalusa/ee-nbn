public class NS_OrderInboundEventHandler implements Queueable {
    private List<NS_Order_Event__e> lstToProcess;
    private DF_Order_Settings__mdt ordSetting;
    private Map<String, DF_Order__c> mapGuidOrder;
    private Map<String, DF_Quote__c> mapGuidQuote;
    private Map<Id, String> oppIdDFOrderStatusMap;

    public NS_OrderInboundEventHandler(List<NS_Order_Event__e> evtLst) {
        this.lstToProcess = evtLst;
        ordSetting = NS_Order_Utils.getOrderSettings('NSOrderSettings');
        mapGuidOrder = new Map<String, DF_Order__c>();
        mapGuidQuote = new Map<String, DF_Quote__c>();
        oppIdDFOrderStatusMap = new Map<Id, String>();
    }

    public void execute(QueueableContext qCon) {
        Set<String> setGuids = new Set<String>();
        Set<String> quotesRequiringUpdates = new Set<String>();

        for (NS_Order_Event__e evt : lstToProcess) {
            System.debug('Event is ' + evt);
            setGuids.add(evt.Event_Record_Id__c);
            //check if DF_Quote records need to be updated
            if (isQuoteUpdateRequired(evt)) {
                quotesRequiringUpdates.add(evt.Event_Record_Id__c);
            }
        }

        if (!setGuids.isEmpty()) {
            //CPST-7850 | michaelcalusa@nbnco.com.au | ability to show revised completion date in NS
            List<DF_Order__c> lstOrder = [
                    SELECT  Id,
                            Order_Id__c,
                            DF_Quote__c,
                            Order_Status__c,
                            Product_Charges_JSON__c,
                            BPI_Id__c,
                            Order_JSON__c,
                            DF_Quote__r.Quote_Name__c,
                            Site_Survey_Charges_JSON__c,
                            NBN_Commitment_Date__c,
                            UNI_Port__c,
                            Order_Sub_Status__c,
                            DF_Quote__r.Order_GUID__c,
                            Appian_Notification_Date__c,
                            DF_Quote__r.Opportunity__c,
                            Service_Region__c,
                            DF_Quote__r.GUID__c,
                            Order_Notifier_Response_JSON__c,
                            NTD_Installation_Appointment_Date__c,
                            NTD_Installation_Appointment_Time__c,
                            DF_Quote__r.Fibre_Build_Cost__c,
                            DF_Quote__r.New_Build_Cost__c,
                            Cancel_Initiated_Stage__c,
                            Revised_Delivery_Date__c
                    FROM DF_Order__c
                    WHERE DF_Quote__r.Order_GUID__c IN :setGuids
            ];

            for (DF_Order__c dfO : lstOrder) {
                mapGuidOrder.put(dfO.DF_Quote__r.Order_GUID__c, dfo);
            }

            if (!quotesRequiringUpdates.isEmpty()) {
                List<DF_Quote__c> lstQuote = (List<DF_Quote__c>) Database.query(SF_CS_API_Util.getQuery(new DF_Quote__c(), ' WHERE Order_GUID__c IN :quotesRequiringUpdates'));
                for (DF_Quote__c dfQ : lstQuote) {
                    mapGuidQuote.put(dfQ.Order_GUID__c, dfQ);
                }
            }
        }
        updateOrderRecord();
    }

    private void updateOrderRecord() {
        List<DF_Order__c> lstOrderToUpdate = new List<DF_Order__c>();
        List<DF_Quote__c> lstQuoteToUpdate = new List<DF_Quote__c>();

        Set<Id> constCompletedOrderIds = new Set<Id>();

        try {
            for (NS_Order_Event__e evt : lstToProcess) {
                DF_Order__c ordObj = mapGuidOrder.get(evt.Event_Record_Id__c);
                System.debug('=====checking ordObj====');
                System.debug(ordObj);
                if (ordObj != null) {
                    System.debug('Event ID: ' + evt.Event_Id__c);
                    System.debug('Order notifier message:  ' + evt.Message_Body__c);
                    NS_OrderNotifierJSONWrapper.NS_OrderNotifierJSONRoot ordNotifierRes = (NS_OrderNotifierJSONWrapper.NS_OrderNotifierJSONRoot) JSON.deserialize(evt.Message_Body__c, NS_OrderNotifierJSONWrapper.NS_OrderNotifierJSONRoot.class);
                    System.debug('ordNotifierRes: ' + ordNotifierRes);

                    NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot manageResourceOrderNotification = ordNotifierRes.body.manageResourceOrderNotification;
                    NS_OrderDataJSONWrapper.ResourceOrder manageResourceOrderNotificationResourceOrder = manageResourceOrderNotification.resourceOrder;

                    ordObj.Order_Notifier_Response_JSON__c = JSON.serialize(manageResourceOrderNotification);
                    ordObj.Order_Id__c = manageResourceOrderNotificationResourceOrder.id;

                    String strStatus = manageResourceOrderNotificationResourceOrder.interactionStatus;

                    if (evt.Event_Id__c.equalsIgnoreCase(ordSetting.OrderRspActionReqdEventCode__c)) {//RSP Action required for Cost Variance
                        DF_Quote__c quoteRec = updateCostVariance(ordNotifierRes, ordObj, evt.Event_Record_Id__c);
                        lstQuoteToUpdate.add(quoteRec);
                        System.debug('lstQuoteToUpdate.size >>>>> ' + lstQuoteToUpdate.size());
                    } else if (evt.Event_Id__c.equalsIgnoreCase(ordSetting.OrderRspActionCompEventCode__c)) {
                        DF_Quote__c quoteRec = mapGuidQuote.get(evt.Event_Record_Id__c);
                        quoteRec.Appian_Response_On_Cost_Variance__c = 'Completed';
                        lstQuoteToUpdate.add(quoteRec);
                    } else if (evt.Event_Id__c.equalsIgnoreCase(ordSetting.OrderNTDInstallationDateUpdateEventCode__c)) { // NTD Install Date Update
                        String ntdInstallAppointmentDate = manageResourceOrderNotificationResourceOrder.ntdInstallAppointmentDate;
                        System.debug('ordObj.NTD_Installation_Date__c: ' + ntdInstallAppointmentDate);
                        if (String.isNotEmpty(String.valueOf(ntdInstallAppointmentDate))) {
                            DateTime dt = Datetime.valueOfGmt(ntdInstallAppointmentDate.replace('T', ' '));
                            ordObj.NTD_Installation_Appointment_Date__c = dt.dateGmt();
                            ordObj.NTD_Installation_Appointment_Time__c = dt.timeGmt();

                            NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJsonStr = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot) JSON.deserialize(ordObj.Order_JSON__c, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class);

                            orderJsonStr.resourceOrder.ntdInstallAppointmentDate = ntdInstallAppointmentDate;
                            String orderJson = JSON.serialize(orderJsonStr);
                            ordObj.Order_JSON__c = orderJson;

                        }
                    } else if (evt.Event_Id__c.equalsIgnoreCase(ordSetting.OrderConstCompleteEventCode__c)) {
                        //BatterBackup Changes
                        NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJsonStr = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot) JSON.deserialize(ordObj.Order_JSON__c, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class);
                        List<NS_OrderDataJSONWrapper.ReferencesResourceOrderItem> rOList = orderJsonStr.resourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem;
                        List<NS_OrderDataJSONWrapper.ReferencesResourceOrderItem> rOResList = manageResourceOrderNotificationResourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem;
                        if (rOList.size() == rOResList.size()) {
                            for (Integer i = 0; i < rOList.size(); i++) {
                                orderJsonStr.resourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem[i].itemInvolvesResource.batteryBackupRequired = manageResourceOrderNotificationResourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem[i].itemInvolvesResource.batteryBackupRequired;
                            }
                        }

                        String orderJson = JSON.serialize(orderJsonStr);
                        ordObj.Order_JSON__c = orderJson;
                        //batteryBackup end

                        strStatus = 'Complete';//ordNotifierRes.body.manageResourceOrderNotification.notificationType;
                        NS_Order_Utils.populateBillingJSON(ordObj);

                        constCompletedOrderIds.add(ordObj.Id);
                    }

                    System.debug('=====setting order statuses====');
                    System.debug('===strStatus====' + strStatus);
                    System.debug('===interactionStatus====' + manageResourceOrderNotificationResourceOrder.interactionStatus);
                    System.debug('===interactionSubstatus====' + manageResourceOrderNotificationResourceOrder.interactionSubstatus);

                    ordObj.Order_Status__c = strStatus;
                    ordObj.Order_Sub_Status__c = manageResourceOrderNotificationResourceOrder.interactionSubstatus;

                    ordObj.Notification_Type__c = manageResourceOrderNotification.notificationType;
                    //CPST-7850 | michaelcalusa@nbnco.com.au | ability to show revised completion date in NS
                    if (GlobalUtility.getReleaseToggle('NBN_Select_MR1908')) {
                        if (evt.Event_Id__c.equalsIgnoreCase(ordSetting.OrderReschedEventCode__c)
                        || evt.Event_Id__c.equalsIgnoreCase(ordSetting.OrderOnSchedEventCode__c)) {
                            ordObj.Revised_Delivery_Date__c = null;
                            if (String.isNotBlank(manageResourceOrderNotificationResourceOrder.revisedCompletionDate)) {
                                ordObj.Revised_Delivery_Date__c = Date.valueOf(manageResourceOrderNotificationResourceOrder.revisedCompletionDate);
                            }
                        }
                        //set reason code and description
                        setNSOrderReasonNotifications(ordObj, manageResourceOrderNotificationResourceOrder);
                    } else {
                        ordObj.Notification_Reason__c = manageResourceOrderNotification.notificationReason;
                    }

                    System.debug('===Planned Completion Date====' + manageResourceOrderNotificationResourceOrder.plannedCompletionDate);

                    if (String.isNotBlank(manageResourceOrderNotificationResourceOrder.plannedCompletionDate)) {
                        ordObj.NBN_Commitment_Date__c = Date.valueOf(manageResourceOrderNotificationResourceOrder.plannedCompletionDate);
                    }

                    System.debug('notificationReason====');
                    System.debug(manageResourceOrderNotification.notificationReason);
                    System.debug('notificationType====');
                    System.debug(manageResourceOrderNotification.notificationType);
                    if (String.isNotEmpty(ordNotifierRes.headers.timestamp)) {
                        ordObj.Appian_Notification_Date__c = Datetime.valueOfGmt(ordNotifierRes.headers.timestamp.replace('T', ' ').replace('Z', ''));
                    }

                    if (evt.Event_Id__c.equalsIgnoreCase(ordSetting.OrderAccEventCode__c)) {
                        updateContacts(manageResourceOrderNotificationResourceOrder.itemInvolvesContact, ordObj);
                    }

                    lstOrderToUpdate.add(ordObj);
                    oppIdDFOrderStatusMap.put(ordObj.DF_Quote__r.Opportunity__c, strStatus);
                }
            }

            if (!lstOrderToUpdate.isEmpty()) {
                Database.SaveResult[] lstSaveResult = Database.update(lstOrderToUpdate);
                for (Database.SaveResult dsr : lstSaveResult) {
                    if (dsr.isSuccess() && constCompletedOrderIds.contains(dsr.getId())) {
                        // send billing event for each completed order
                        String param = String.valueOf(dsr.getId()) + NS_SOABillingEventUtils.PARAMETER_DELIMITER + NS_SOABillingEventUtils.CHARGE_TYPE_PRODUCT;
                        AsyncQueueableUtils.createRequests(NS_SOACreateBillingEventHandler.HANDLER_NAME, new List<String>{param});
                    }
                }
            }

            if (!oppIdDFOrderStatusMap.isEmpty()) {
                NS_CS_CloudSenseOrderUpdate.updateCSOrderStatus(oppIdDFOrderStatusMap);
            }

            if (!lstQuoteToUpdate.isEmpty()) {
                Database.update(lstQuoteToUpdate);
            }

            sendEmailNotification();
        } catch (Exception ex) {
            System.debug('NS_OrderInboundEventHandler::updateOrderRecord: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
            GlobalUtility.logMessage(SF_Constants.ERROR, NS_OrderInboundEventHandler.class.getName(), 'updateOrderRecord', '', '', '', '', ex, 0);
        }
    }

    private DF_Quote__c updateCostVariance(NS_OrderNotifierJSONWrapper.NS_OrderNotifierJSONRoot ordNotifierRes, DF_Order__c dfOrder, String strGuid) {
        DF_Quote__c quoteRec;
        String orderNewBuildCost = ordNotifierRes.body.manageResourceOrderNotification.resourceOrder.newBuildCost;
        if (String.isNotBlank(orderNewBuildCost) && Decimal.valueOf(orderNewBuildCost) != dfOrder.DF_Quote__r.Fibre_Build_Cost__c) {
            System.debug('===== In first condition for Updating new FBC =====');
            if (mapGuidQuote.containsKey(strGuid)) {
                System.debug('===== Updating new FBC =====');
                quoteRec = mapGuidQuote.get(strGuid);
                quoteRec.New_Build_Cost__c = Decimal.valueOf(orderNewBuildCost);
            }
        }
        return quoteRec;
    }

    private void updateContacts(List<NS_OrderDataJSONWrapper.ItemInvolvesContact> lstContacts, DF_Order__c dfOrder) {
        try {
            List<Contact> lstConToUpdate = new List<Contact>();
            List<OpportunityContactRole> lstConRole = [
                    SELECT Id, ContactId, Role, Contact.LastName,
                            Contact.FirstName, Contact.Email, Contact.Phone,
                            Contact.MailingStreet, Contact.MailingPostalCode,
                            Contact.MailingCity, Contact.MailingState, Contact.Title
                    FROM OpportunityContactRole
                    WHERE OpportunityId = :dfOrder.DF_Quote__r.Opportunity__c
            ];

            for (NS_OrderDataJSONWrapper.ItemInvolvesContact ordCon : lstContacts) {
                for (OpportunityContactRole ocr : lstConRole) {
                    if (ordCon.Type.equalsIgnoreCase(ocr.Role.split(' ', 2)[0])) {
                        if (String.isNotBlank(ordCon.contactName)) {
                            Contact conRec = new Contact();
                            conRec.Id = ocr.ContactId;
                            if (ordCon.contactName.contains(' ')) {
                                conRec.LastName = ordCon.contactName.split(' ', 2)[1];
                                conRec.FirstName = ordCon.contactName.split(' ', 2)[0];
                            } else {
                                conRec.LastName = ordCon.contactName;
                            }
                            if (String.isNotBlank(ordCon.emailAddress)) {
                                conRec.Email = ordCon.emailAddress;
                            }
                            // if(String.isNotBlank(ordCon.company)){
                            //    conRec.Company__c=ordCon.company;
                            // }
                            if (String.isNotBlank(ordCon.phoneNumber)) {
                                conRec.Phone = ordCon.phoneNumber;
                            }
                            if (String.isNotBlank(ordCon.role)) {
                                conRec.Title = ordCon.role;
                            }
                            if (String.isNotBlank(ordCon.unstructuredAddress.addressLine1)) {
                                conRec.MailingStreet = ordCon.unstructuredAddress.addressLine1;
                            }
                            if (String.isNotBlank(ordCon.unstructuredAddress.localityName)) {
                                conRec.MailingCity = ordCon.unstructuredAddress.localityName;
                            }
                            if (String.isNotBlank(ordCon.unstructuredAddress.stateTerritoryCode)) {
                                conRec.MailingState = ordCon.unstructuredAddress.stateTerritoryCode;
                            }
                            if (String.isNotBlank(ordCon.unstructuredAddress.postcode)) {
                                conRec.MailingPostalCode = ordCon.unstructuredAddress.postcode;
                            }
                            lstConToUpdate.add(conRec);
                        }
                        lstConRole.remove(lstConRole.indexOf(ocr));
                        break;
                    }
                }
            }
            if (!lstConToUpdate.isEmpty()) {
                Database.DMLOptions dml = new Database.DMLOptions();
                dml.DuplicateRuleHeader.allowSave = true;
                Database.update(lstConToUpdate, dml);
            }

            NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJsonRoot = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot) JSON.deserialize(dfOrder.Order_JSON__c, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class);
            orderJsonRoot.resourceOrder.itemInvolvesContact = new List<NS_OrderDataJSONWrapper.ItemInvolvesContact>();
            orderJsonRoot.resourceOrder.itemInvolvesContact.addAll(lstContacts);
            dfOrder.Order_JSON__c = JSON.serialize(orderJsonRoot);

        } catch (Exception ex) {
            System.debug('NS_OrderInboundEventHandler::updateContacts: ' + ex.getMessage() + '\n' + ex.getStackTraceString());
            GlobalUtility.logMessage(SF_Constants.ERROR, NS_OrderInboundEventHandler.class.getName(), 'updateContacts', '', '', '', '', ex, 0);
        }
    }

    private void sendEmailNotification() {
        Map<String, String> orderIdToTemplateMap = new Map<String, String>();
        Map<String, String> eventIdToEmailTemplateMap = NS_SF_Utils.initEventIdToEmailTemplateMap(ordSetting);

        DF_Order__c ordObj = null;
        for (NS_Order_Event__e evt : lstToProcess) {
            ordObj = mapGuidOrder.get(evt.Event_Record_Id__c);
            if (ordObj != null && String.isNotBlank(evt.Event_Id__c)) {
                orderIdToTemplateMap.put(ordObj.Id, eventIdToEmailTemplateMap.get(evt.Event_Id__c.toLowerCase()));
            }
        }

        System.debug('NS_OrderInboundEventHandler >>>  sendEmailNotification >>> ' + orderIdToTemplateMap);
        DF_OrderEmailService.sendNSOrderEmail(orderIdToTemplateMap);
    }

    private Boolean isQuoteUpdateRequired(NS_Order_Event__e evt) {
        return evt.Event_Id__c.equalsIgnoreCase(ordSetting.OrderRspActionReqdEventCode__c) || evt.Event_Id__c.equalsIgnoreCase(ordSetting.OrderRspActionCompEventCode__c);
    }

    /**
    * CPST-7850: add reason code and description for pending, resume notification and for future notifications
    */
    private static void setNSOrderReasonNotifications(DF_Order__c ordObj, NS_OrderDataJSONWrapper.ResourceOrder rscOrder) {
        if(rscOrder.note != null && rscOrder.note.id != null && rscOrder.note.description != null) {
            ordObj.Reason_Code__c = rscOrder.note.id;
            ordObj.Notification_Reason__c = rscOrder.note.description;
        }
    }
}