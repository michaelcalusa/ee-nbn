public class NS_OrderInflightCancelUtil {

	private static final Integer 	TIMEOUT_LIMIT 			= 60000;
	private static final String 	END_POINT 				= DF_Constants.NAMED_CRED_PREFIX + 'Appian';
	private static final String 	ORDER_CANCEL_REQUEST 	= 'OrderCancelRequest';
	private static final String 	SUCCESS_RESP 			= 'Success Response';

	private static final Map<String, String> cancelHeader = new Map<String, String> {
			'businessChannel'					=> DF_Constants.BUSINESS_CHANNEL,
			DF_Constants.HTTP_HEADER_ACCEPT 	=> DF_Constants.CONTENT_TYPE_JSON,
			DF_Constants.CONTENT_TYPE			=> DF_Constants.CONTENT_TYPE_JSON,
			'activityName' 						=> 'cancelOrder',
			'businessProcessVersion' 			=> 'V13.0',
			'businessServiceVersion' 			=> 'V2.0',
			'msgType' 							=> 'Request',
			'security' 							=> 'Placeholder Security',
			'msgName'							=> 'ManageProductOrdercancelOrderRequest',
			'businessServiceName' 				=> 'ManageProductOrder',
			'communicationPattern' 				=> 'RequestReply',
			'orderPriority'						=> '6'
	};


	/**
 	* Method to get RecordType Id
 	*
 	* @param 	String RECORD_TYPE_NAME
 	* @param 	String SobjectType
 	* @return 	Id recTypeId
	*/
	public static Id getRecordTypeId(final String RECORD_TYPE_NAME, final String SobjectType) {

		Id recTypeId = null;

		if(String.isNotBlank(RECORD_TYPE_NAME) && String.isNotBlank(SobjectType)) {

			recTypeId = Schema.getGlobalDescribe().get(SobjectType).getDescribe().getRecordTypeInfosByName().get(RECORD_TYPE_NAME).getRecordTypeId();
		}

		return recTypeId;
	}

	/**
	 * Get List of DF Orders using DF Order Ids
	 *
	 * @param	  List<String> dfOrdersId
	 *
	 * @return	 List<DF_Order__c>
	*/

	public static List<DF_Order__c> getDFOrdersByIds(List<String> dfOrderIds) {
		
		List<DF_Order__c> dfOrders = null;

		if (dfOrderIds != null && !dfOrderIds.isEmpty()) {
			dfOrders = [
					SELECT 	Id,
							Cancel_Initiated_Stage__c,
							Customer_Required_Date__c,
							DF_Quote__r.Order_GUID__c,
							DF_Quote__r.Quote_Name__c,
							NBN_Commitment_Date__c,
							Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c,
                        	Opportunity_Bundle__r.Account__c,
                        	DF_Quote__r.Opportunity__c,
                        	DF_Quote__r.Opportunity__r.Commercial_Deal__c,
                        	DF_Quote__r.Fibre_Build_Cost__c,
							Order_Id__c,
							Order_Status__c,
							Order_Sub_Status__c,
							RecordType.DeveloperName,
							(
								SELECT 
									CreatedDate,
									NewValue,
									OldValue 
								FROM 
									Histories
								WHERE 
									Field IN ('Order_Sub_Status__c')
								ORDER BY 
									CreatedDate DESC
							)
						FROM 
							DF_Order__c 
						WHERE 
							Id IN :dfOrderIds
			];
		}
		return dfOrders;
	}



	/**
	 * Method to get DF Order Settings custom metadata
	 *
	 * @return 	DF_Order_Settings__mdt dfOrderSettings
	*/
	public static DF_Order_Settings__mdt getDfOrderSettings() {
		DF_Order_Settings__mdt dfOrderSettings = [ SELECT
														OrderCancelBatchSize__c,
														OrderCancelEventCode__c,
														OrderCancelSource__c
													FROM DF_Order_Settings__mdt
													WHERE DeveloperName = 'NSOrderSettings'];
		return dfOrderSettings;
	}


	/**
	 * Method to send Cancel Request to Appian
	 *
	 * @return 
	*/
	public static Boolean sendCancelRequest(String orderId) {

		String requestBody;

		System.debug('orderId: ' + orderId);

		final String 	guid;

		Http http = new Http();	 
		HttpRequest req;			  
		HttpResponse res;


		List<String> dfOrderIds = new List<String>();
		List<DF_Order__c> orderList = new List<DF_Order__c>();
		DF_Order__c dfOrder;

		if (String.isBlank(orderId)) {
			throw new CustomException('No orderId paramater in sendCancelRequest');
		}

		try {

			dfOrderIds.add(orderId);
			orderList = NS_OrderInflightCancelUtil.getDFOrdersByIds(dfOrderIds);

			if (orderList.size() != 1) {
				throw new CustomException('Found ' + orderList.size() + ' orders with the id: ' + orderId);
			}
			dfOrder = orderList.get(0);

			DF_Order_Settings__mdt dfOrderSettings = getDfOrderSettings();

			String cancelCode = dfOrderSettings.OrderCancelEventCode__c;
			if (String.isBlank(cancelCode)) {
				throw new CustomException('The DF_Order_Settings_mdt  cancel code is not set');
			}

			guid = dfOrder.DF_Quote__r.Order_GUID__c;
			if (String.isBlank(guid)) {
				throw new CustomException('The DF_Quote.Order_GUID__c is not set for DF_Order with Id: ' + orderId);
			}


			String quoteName = dfOrder.DF_Quote__r.Quote_Name__c;
			if ( String.isBlank(quoteName) ) {
				throw new CustomException('The Quote_Name_c is blank for orderId: ' + orderId);
			}

			// Generate Payload
			requestBody =  '{"eventIdentifier":"' + cancelCode 	+ 		'",' +
			 				'"externalSystemCorrelationId":"' + guid + 	'",' +
					 		'"eventBody":{"resourceOrder": {"id": "' + quoteName + '"}}}';

			System.debug('Payload request: ' + requestBody);

			// Generate request
			req = getHttpRequest(END_POINT, TIMEOUT_LIMIT, requestBody, getHeaders(dfOrder));
			System.debug('req: ' + req);

			// Send and get response			   
			res = http.send(req);

			System.debug('Response Status Code: ' + res.getStatusCode());
			System.debug('Response Body: ' + res.getBody());
		
			if (res.getStatusCode() == DF_Constants.HTTP_STATUS_200) {

				GlobalUtility.logMessage(
						DF_Constants.INFO,
						NS_OrderInflightCancelUtil.class.getName(),
						ORDER_CANCEL_REQUEST,
						dfOrder.Order_Id__c,
						guid,
						SUCCESS_RESP,
						requestBody,
						null,
						0);

				return true;


//				Map<String, String> orderToTemplateMap = new Map<String, String>();
//				orderToTemplateMap.put(dfOrder.Id, NS_Constants.TEMPLATE_ORDER_CANCEL_INITIATED);
//				DF_OrderEmailService.sendEmail(orderToTemplateMap);

			} else {
				throw new CustomException('Error occured during callout to Appian. Http Status Code:' + res.getStatusCode());
			}	 
								 
		} catch (Exception e) {

			GlobalUtility.logMessage(	DF_Constants.ERROR,
										NS_OrderInflightCancelUtil.class.getName(),
										ORDER_CANCEL_REQUEST,
										dfOrder.Order_Id__c,
										guid,
										e.getMessage(),
										requestBody,
										e,
										0);

			throw new CustomException(e.getMessage());
	   }		
	}

	public static Map<String, String> getHeaders(DF_Order__c dfOrder) {

		String guid = '';
		Map <String, String> headers = new Map<String, String>();

		try {
			System.debug('@@@ dfOrder.Opportunity_Bundle__r.Account__r: ' + dfOrder.Opportunity_Bundle__r.Account__r);

			headers.put(DF_IntegrationUtils.TIMESTAMP,
						Datetime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\''));

			guid = dfOrder.DF_Quote__r.Order_GUID__c;
			if (String.isNotBlank(guid)) {
				headers.put(DF_IntegrationUtils.CORRELATION_ID, guid);
 			} else {
				throw new CustomException(DF_IntegrationUtils.ERR_CORRELATION_ID_NOT_FOUND);
			}

			String accessSeeker = dfOrder.Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c;
			if (String.isNotBlank(accessSeeker)) {
				headers.put(DF_IntegrationUtils.ACCESS_SEEKER_ID, accessSeeker);
 			} else {
				throw new CustomException(DF_IntegrationUtils.ERR_ACCESS_SEEKER_ID_NOT_FOUND);
			}

			headers.putAll(cancelHeader);

			System.debug('::header::'+headers);
												 
		} catch (Exception e) {  
			GlobalUtility.logMessage(	DF_Constants.ERROR,
										NS_OrderInflightCancelUtil.class.getName(),
										ORDER_CANCEL_REQUEST + ' :getHeaders',
										dfOrder.Order_Id__c,
										guid,
										e.getMessage(),
										String.valueOf(headers),
										e,
										0);
			throw new CustomException(e.getMessage());
		}
		return headers;
	}

	public static HttpRequest getHttpRequest(String endpoint, Integer timeoutLimit, String requestStr, Map<String, String> headerMap) {
		HttpRequest req = new HttpRequest();		
		
		try {			   
			req.setEndpoint(endpoint);	  
			req.setMethod(DF_Constants.HTTP_METHOD_POST);			 
			req.setTimeout(timeoutLimit);
			
			// Set all headers
			for (String key : headerMap.keySet()){
				req.setHeader(key,headerMap.get(key));
			}
			req.setBody(requestStr);

		} catch (Exception e) {  
			throw new CustomException(e.getMessage());
		}
			   
		return req;
	}

}