/**
 * Created by alan on 2019-04-17.
 */

public class NS_OrderInflightCancellation implements Queueable {

    private static final String FORMAT_INTERACTION_DATE_TIME = 'yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'';

    private NS_Order_Event__e cancelEvent;

    public NS_OrderInflightCancellation(NS_Order_Event__e evt) {
        this.cancelEvent = evt;
    }

    public void execute(QueueableContext queueableContext) {

        try {
            System.debug('Cancel Order Event: ' + cancelEvent);
            System.debug('Event message body: ' + cancelEvent.Message_Body__c);

            NS_CS_Cancellation_Service nsCsCancellationService = (NS_CS_Cancellation_Service)ObjectFactory.getInstance(NS_CS_Cancellation_Service.class);

            DF_Order__c nsOrder = nsCsCancellationService.getOrderForCancellation(cancelEvent.Event_Record_Id__c);

            System.debug('Order: ' + nsOrder);

            NS_OrderNotifierJSONWrapper.NS_OrderNotifierJSONRoot notificationJson =
                    (NS_OrderNotifierJSONWrapper.NS_OrderNotifierJSONRoot) JSON.deserialize(cancelEvent.Message_Body__c, NS_OrderNotifierJSONWrapper.NS_OrderNotifierJSONRoot.class);
            NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot manageResourceOrderNotification = notificationJson.body.manageResourceOrderNotification;

            System.debug('Order_Notifier_Response_JSON__c: ' + JSON.serialize(manageResourceOrderNotification));

            nsOrder.Order_Notifier_Response_JSON__c = JSON.serialize(manageResourceOrderNotification);
            NS_OrderDataJSONWrapper.ResourceOrder manageResourceOrderNotificationResourceOrder = manageResourceOrderNotification.resourceOrder;
//            nsOrder.Order_Status__c = manageResourceOrderNotificationResourceOrder.interactionStatus;
            //hard code cancel status ignoring notification same as EE?
            nsOrder.Order_Status__c = DF_Constants.DF_ORDER_STATUS_CANCELLED;
            nsOrder.Order_Sub_Status__c = manageResourceOrderNotificationResourceOrder.interactionSubstatus;
            nsOrder.Notification_Type__c = manageResourceOrderNotification.notificationType;
            nsOrder.Notification_Reason__c = manageResourceOrderNotification.notificationReason;
            if (String.isNotEmpty(notificationJson.headers.timestamp)) {
                nsOrder.Appian_Notification_Date__c = Datetime.valueOfGmt(notificationJson.headers.timestamp.replace('T', ' ').replace('Z', ''));
            }

            nsCsCancellationService.cancelOpportunitySubscriptionsAndServices(nsOrder.DF_Quote__r.Opportunity__c);

            if(isCancelChargeable(nsOrder, manageResourceOrderNotificationResourceOrder.note)){
                Opportunity cancellationChildOpportunity = nsCsCancellationService.createCancellationOpportunityAndAssociatedProduct(nsOrder);

                String interactionDateTime = manageResourceOrderNotificationResourceOrder.interactionDateComplete;
                if(String.isBlank(interactionDateTime)){
                    interactionDateTime = Datetime.now().formatGmt(FORMAT_INTERACTION_DATE_TIME);
                }

                nsOrder.Order_Cancel_Charges_JSON__c = nsCsCancellationService.getOrderCancelChargesJson(nsOrder, cancellationChildOpportunity, interactionDateTime);
                System.debug('Order_Cancel_Charges_JSON__c='+nsOrder.Order_Cancel_Charges_JSON__c);
            }

            update nsOrder;

            DF_OrderEmailService mailService = (DF_OrderEmailService)ObjectFactory.getInstance(DF_OrderEmailService.class);
            mailService.sendNsOrderMail(new Map<String, String>{(String)nsOrder.Id => SF_Constants.TEMPLATE_ORDER_CANCELLED});

        } catch(Exception e){
            GlobalUtility.logMessage(SF_Constants.ERROR, NS_OrderInflightCancellation.class.getName(), 'execute', cancelEvent.Message_Body__c, 'An unexpected error occured while processing order cancelled event', e);
            throw e;
        }
    }

    private Boolean isCancelChargeable(DF_Order__c nsOrder, NS_OrderDataJSONWrapper.Note notificationReason){

//        return !'Rejected'.equals(nsOrder.DF_Quote__r.RSP_Response_On_Cost_Variance__c);
        //TODO: create NOTIFICATION_REASON_CODE enum
        return notificationReason != null;
    }

}