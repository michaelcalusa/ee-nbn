/**
 * Created by alan on 2019-04-17.
 */

@isTest
public class NS_OrderInflightCancellation_Test {

    @testSetup static void testDataSetup() {

        SF_TestData.Parameters createdObjects = SF_TestData.setupAllData(null);

        createdObjects.quote.Fibre_Build_Cost__c = 1000;
        update createdObjects.quote;

        createdObjects.order.Order_Status__c = DF_Constants.DF_ORDER_STATUS_CANCEL_INITIATED;
        createdObjects.order.Cancel_Initiated_Stage__c = 'Build';
        createdObjects.order.Order_Sub_Status__c = '';
        createdObjects.order.OrderType__c = 'Connect';
        createdObjects.order.Order_Id__c = 'BCO-12345';
        createdObjects.order.Appian_Notification_Date__c = Datetime.now();
        update createdObjects.order;
    }

    @isTest
    static void shouldUpdateOrderAndSetCancellationChargesWhenCancellationIsChargeable(){

        NS_Order_Event__e evt = new NS_Order_Event__e();
        evt.Event_Id__c = 'eventId';
        evt.Source__c = 'sourceId';
        evt.Event_Record_Id__c = 'orderGuid';

        String evtMessageBody = '{\"body\":{\"manageResourceOrderNotification\":{\"resourceOrder\":{\"id\":\"ROR000000003709\",\"note\":{\"id\":\"NOSN1411\",\"description\":\"Build Completion Date provided\"},\"notes\":\"Lore Ipsum\",\"siteName\":\"South Wharf\",\"siteType\":\"Retailer\",\"orderType\":\"Connect\",\"tradingName\":\"Woolies\",\"dateReceived\":\"2019-03-04T00:57:30Z\",\"heritageSite\":\"No\",\"keysRequired\":\"Yes\",\"newBuildCost\":null,\"safetyHazards\":\"No\",\"additionalNotes\":\"Comments\",\"jsasswmRequired\":\"Yes\",\"inductionRequired\":\"No\",\"interactionStatus\":\"InProgress\",\"interactionDateComplete\": \"2019-04-12T01:23:57Z\",\"resourceOrderType\":\"NBN Select\",\"accessSeekerContact\":{\"contactName\":\"Zac Swin\",\"contactPhone\":\"+61401654321\"},\"afterHoursSiteVisit\":\"Yes\",\"itemInvolvesContact\":[{\"role\":\"Manager\",\"type\":\"Business\",\"contactName\":\"David Lee\",\"phoneNumber\":\"0412 345 443\",\"emailAddress\":\"davidlee@example.com\",\"unstructuredAddress\":{\"postcode\":\"3000\",\"addressLine1\":\"Suite 9\",\"addressLine2\":\"Level 21\",\"addressLine3\":\"655 Doclands Drive\",\"localityName\":\"Melbourne CBD\",\"stateTerritoryCode\":\"VIC\"}},{\"role\":\"\",\"type\":\"Site\",\"contactName\":\"Bruce Mac\",\"phoneNumber\":\"0412 345 443\",\"emailAddress\":\"bmac@example.com\",\"unstructuredAddress\":{\"postcode\":\"\",\"addressLine1\":\"\",\"addressLine2\":\"\",\"addressLine3\":\"\",\"localityName\":\"\",\"stateTerritoryCode\":\"\"}}],\"customerRequiredDate\":\"2017-02-25T00:00:00Z\",\"interactionSubstatus\":\"\",\"ownerAwareOfProposal\":\"Yes\",\"securityRequirements\":\"Yes\",\"associatedReferenceId\":\"NSQ-040320190304\",\"plannedCompletionDate\":\"2019-05-27T00:00:00Z\",\"workPracticesRequired\":\"Yes\",\"siteAccessInstructions\":\"Instructions\",\"propertyOwnershipStatus\":\"Leased\",\"resourceOrderComprisedOf\":{\"itemInvolvesLocation\":{\"id\":\"LOCMMP3456789195\"},\"referencesResourceOrderItem\":[{\"action\":\"ADD\",\"itemInvolvesResource\":{\"type\":\"NTD\",\"batteryBackupRequired\":\"Yes\"}}]},\"confinedSpaceRequirements\":\"Yes\",\"ntdInstallAppointmentDate\":null,\"workingAtHeightsConsideration\":\"Yes\",\"contractedLocationInstructions\":\"Contract\"},\"notificationType\":\"OrderCancelled\"}},\"headers\":{\"msgName\":\"ManageResourceOrdercreateOrderRequest\",\"msgType\":\"Notification\",\"security\":\"Placeholder Security\",\"timestamp\":\"2019-03-04T01:08:28Z\",\"activityName\":\"cancelOrder\",\"correlationId\":\"0c7a833d-dda3-444c-89df-040320190304\",\"orderPriority\":\"6\",\"accessSeekerID\":\"ASI331834535517\",\"businessChannel\":\"Enterprise Portal\",\"businessServiceName\":\"ManageResourceOrder\",\"communicationPattern\":\"SubmitNotification\",\"businessProcessVersion\":\"V2.0\",\"businessServiceVersion\":\"V2.0\"},\"metadata\":{\"source\":\"appian-nbncodev\",\"application\":\"nbnSelect\"}}';
        evt.Message_Body__c = evtMessageBody;

        RecordingStubProvider stubProvider = new RecordingStubProvider(NS_CS_Cancellation_Service.class);

        Df_order__c nsOrder  = [
                SELECT  Id, Order_Status__c, Order_Sub_Status__c,
                        DF_Quote__r.Order_GUID__c, DF_Quote__r.Opportunity__c, DF_Quote__r.RSP_Response_On_Cost_Variance__c,
                        Order_Cancel_Charges_JSON__c, Order_Notifier_Response_JSON__c, Appian_Notification_Date__c,
                        Notification_Reason__c, Notification_Type__c

                FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1
        ];

        stubProvider.given('getOrderForCancellation')
                .when(new List<ArgumentMatcher>{new StringArgumentMatcher('orderGuid')})
                .thenReturn(nsOrder);

        Opportunity cancellationChildOpportunity = new Opportunity(
                Name = 'Cancellation Charge'
        );

        stubProvider.given('createCancellationOpportunityAndAssociatedProduct')
                .when(new List<ArgumentMatcher>{new GenericObjectArgumentCaptor()})
                .thenReturn(cancellationChildOpportunity);

        stubProvider.given('getOrderCancelChargesJson')
        .when(new List<ArgumentMatcher>{
                new GenericObjectArgumentCaptor(),
                new GenericObjectArgumentCaptor(),
                new StringArgumentMatcher('2019-04-12T01:23:57Z')
                })
        .thenReturn('{}');

        ObjectFactory.setStubProvider(NS_CS_Cancellation_Service.class, stubProvider);

        RecordingStubProvider emailServiceStubProvider = new RecordingStubProvider(DF_OrderEmailService.class);
        ObjectFactory.setStubProvider(DF_OrderEmailService.class, emailServiceStubProvider);

        Test.startTest();

            System.enqueueJob(new NS_OrderInflightCancellation(evt));

        Test.stopTest();

        //assert order attributes and verify interactions

        NS_OrderNotifierJSONWrapper.NS_OrderNotifierJSONRoot notificationJson =
                (NS_OrderNotifierJSONWrapper.NS_OrderNotifierJSONRoot) JSON.deserialize(evtMessageBody, NS_OrderNotifierJSONWrapper.NS_OrderNotifierJSONRoot.class);
        NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot manageResourceOrderNotification = notificationJson.body.manageResourceOrderNotification;

        nsOrder  = [
                SELECT  Id, Order_Status__c, Order_Sub_Status__c, DF_Quote__r.Opportunity__c,
                        Order_Cancel_Charges_JSON__c, Order_Notifier_Response_JSON__c, Appian_Notification_Date__c,
                        Notification_Reason__c, Notification_Type__c
                FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1
        ];

        Assert.equals(JSON.serialize(manageResourceOrderNotification), nsOrder.Order_Notifier_Response_JSON__c);
        Assert.equals('Cancelled', nsOrder.Order_Status__c);
        Assert.equals(null, nsOrder.Order_Sub_Status__c);
        Assert.equals('OrderCancelled', nsOrder.Notification_Type__c);
        Assert.equals(null, nsOrder.Notification_Reason__c);
        Assert.equals(Datetime.valueOfGmt('2019-03-04 01:08:28'), nsOrder.Appian_Notification_Date__c);
        Assert.equals('{}', nsOrder.Order_Cancel_Charges_JSON__c);

        stubProvider.verify('cancelOpportunitySubscriptionsAndServices', 1, new List<ArgumentMatcher>{new StringArgumentMatcher(nsOrder.DF_Quote__r.Opportunity__c)});

        ArgumentCaptor argCaptor = new GenericObjectArgumentCaptor();
        stubProvider.verify('createCancellationOpportunityAndAssociatedProduct', 1, new List<ArgumentMatcher>{argCaptor});
        DF_Order__c capturedOrder = (DF_Order__c)argCaptor.getCaptured();
        Assert.equals(nsOrder.Id, capturedOrder.Id);

        ArgumentCaptor argCaptor2 = new GenericObjectArgumentCaptor();
        ArgumentMatcher argMatcher3 = new StringArgumentMatcher('2019-04-12T01:23:57Z');
        stubProvider.verify('getOrderCancelChargesJson', 1, new List<ArgumentMatcher>{argCaptor, argCaptor2, argMatcher3});
        capturedOrder = (DF_Order__c)argCaptor.getCaptured();
        Assert.equals(nsOrder.Id, capturedOrder.Id);
        Opportunity capturedCancellationOpportunity = (Opportunity)argCaptor2.getCaptured();
        Assert.equals(cancellationChildOpportunity, capturedCancellationOpportunity);

        emailServiceStubProvider.verify('sendNsOrderMail', 1, new List<ArgumentMatcher>{argCaptor});
        Map<String, String>  capturedMapArg = (Map<String, String>)argCaptor.getCaptured();
        Assert.equals(1, capturedMapArg.size());
        Assert.equals(SF_Constants.TEMPLATE_ORDER_CANCELLED, capturedMapArg.get((String)nsOrder.Id));
    }

    @isTest
    static void shouldUpdateOrderButNotSetCancellationChargesWhenCancellationIsNotChargeable(){

        //note: if OrderCancelled notification payload is missing a resourceOrder.note
        // then it is from rejected cost variance and therefore the order is deemed to be not chargeable
        NS_Order_Event__e evt = new NS_Order_Event__e();
        evt.Event_Id__c = 'eventId';
        evt.Source__c = 'sourceId';
        evt.Event_Record_Id__c = 'orderGuid';
        String evtMessageBody = '{\"body\":{\"manageResourceOrderNotification\":{\"resourceOrder\":{\"id\":\"ROR000000003709\",\"notes\":\"Lore Ipsum\",\"siteName\":\"South Wharf\",\"siteType\":\"Retailer\",\"orderType\":\"Connect\",\"tradingName\":\"Woolies\",\"dateReceived\":\"2019-03-04T00:57:30Z\",\"heritageSite\":\"No\",\"keysRequired\":\"Yes\",\"newBuildCost\":null,\"safetyHazards\":\"No\",\"additionalNotes\":\"Comments\",\"jsasswmRequired\":\"Yes\",\"inductionRequired\":\"No\",\"interactionStatus\":\"InProgress\",\"interactionDateComplete\": \"2019-04-12T01:23:57Z\",\"resourceOrderType\":\"NBN Select\",\"accessSeekerContact\":{\"contactName\":\"Zac Swin\",\"contactPhone\":\"+61401654321\"},\"afterHoursSiteVisit\":\"Yes\",\"itemInvolvesContact\":[{\"role\":\"Manager\",\"type\":\"Business\",\"contactName\":\"David Lee\",\"phoneNumber\":\"0412 345 443\",\"emailAddress\":\"davidlee@example.com\",\"unstructuredAddress\":{\"postcode\":\"3000\",\"addressLine1\":\"Suite 9\",\"addressLine2\":\"Level 21\",\"addressLine3\":\"655 Doclands Drive\",\"localityName\":\"Melbourne CBD\",\"stateTerritoryCode\":\"VIC\"}},{\"role\":\"\",\"type\":\"Site\",\"contactName\":\"Bruce Mac\",\"phoneNumber\":\"0412 345 443\",\"emailAddress\":\"bmac@example.com\",\"unstructuredAddress\":{\"postcode\":\"\",\"addressLine1\":\"\",\"addressLine2\":\"\",\"addressLine3\":\"\",\"localityName\":\"\",\"stateTerritoryCode\":\"\"}}],\"customerRequiredDate\":\"2017-02-25T00:00:00Z\",\"interactionSubstatus\":\"\",\"ownerAwareOfProposal\":\"Yes\",\"securityRequirements\":\"Yes\",\"associatedReferenceId\":\"NSQ-040320190304\",\"plannedCompletionDate\":\"2019-05-27T00:00:00Z\",\"workPracticesRequired\":\"Yes\",\"siteAccessInstructions\":\"Instructions\",\"propertyOwnershipStatus\":\"Leased\",\"resourceOrderComprisedOf\":{\"itemInvolvesLocation\":{\"id\":\"LOCMMP3456789195\"},\"referencesResourceOrderItem\":[{\"action\":\"ADD\",\"itemInvolvesResource\":{\"type\":\"NTD\",\"batteryBackupRequired\":\"Yes\"}}]},\"confinedSpaceRequirements\":\"Yes\",\"ntdInstallAppointmentDate\":null,\"workingAtHeightsConsideration\":\"Yes\",\"contractedLocationInstructions\":\"Contract\"},\"notificationType\":\"OrderCancelled\"}},\"headers\":{\"msgName\":\"ManageResourceOrdercreateOrderRequest\",\"msgType\":\"Notification\",\"security\":\"Placeholder Security\",\"timestamp\":\"2019-03-04T01:08:28Z\",\"activityName\":\"cancelOrder\",\"correlationId\":\"0c7a833d-dda3-444c-89df-040320190304\",\"orderPriority\":\"6\",\"accessSeekerID\":\"ASI331834535517\",\"businessChannel\":\"Enterprise Portal\",\"businessServiceName\":\"ManageResourceOrder\",\"communicationPattern\":\"SubmitNotification\",\"businessProcessVersion\":\"V2.0\",\"businessServiceVersion\":\"V2.0\"},\"metadata\":{\"source\":\"appian-nbncodev\",\"application\":\"nbnSelect\"}}';
        evt.Message_Body__c = evtMessageBody;

        RecordingStubProvider stubProvider = new RecordingStubProvider(NS_CS_Cancellation_Service.class);

        Df_order__c nsOrder  = [
                SELECT  Id, Order_Status__c, Order_Sub_Status__c,
                        DF_Quote__r.Order_GUID__c, DF_Quote__r.Opportunity__c, DF_Quote__r.RSP_Response_On_Cost_Variance__c,
                        Order_Cancel_Charges_JSON__c, Order_Notifier_Response_JSON__c, Appian_Notification_Date__c,
                        Notification_Reason__c, Notification_Type__c

                FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1
        ];

        stubProvider.given('getOrderForCancellation')
                .when(new List<ArgumentMatcher>{new StringArgumentMatcher('orderGuid')})
                .thenReturn(nsOrder);

        ObjectFactory.setStubProvider(NS_CS_Cancellation_Service.class, stubProvider);

        RecordingStubProvider emailServiceStubProvider = new RecordingStubProvider(DF_OrderEmailService.class);
        ObjectFactory.setStubProvider(DF_OrderEmailService.class, emailServiceStubProvider);

        Test.startTest();

        System.enqueueJob(new NS_OrderInflightCancellation(evt));

        Test.stopTest();

        //assert order attributes and verify interactions

        NS_OrderNotifierJSONWrapper.NS_OrderNotifierJSONRoot notificationJson =
                (NS_OrderNotifierJSONWrapper.NS_OrderNotifierJSONRoot) JSON.deserialize(evtMessageBody, NS_OrderNotifierJSONWrapper.NS_OrderNotifierJSONRoot.class);
        NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot manageResourceOrderNotification = notificationJson.body.manageResourceOrderNotification;

        nsOrder  = [
                SELECT  Id, Order_Status__c, Order_Sub_Status__c, DF_Quote__r.Opportunity__c,
                        Order_Cancel_Charges_JSON__c, Order_Notifier_Response_JSON__c, Appian_Notification_Date__c,
                        Notification_Reason__c, Notification_Type__c
                FROM df_order__c WHERE orderType__c = 'Connect' AND Order_Id__c = 'BCO-12345' LIMIT 1
        ];

        Assert.equals(JSON.serialize(manageResourceOrderNotification), nsOrder.Order_Notifier_Response_JSON__c);
        Assert.equals('Cancelled', nsOrder.Order_Status__c);
        Assert.equals(null, nsOrder.Order_Sub_Status__c);
        Assert.equals('OrderCancelled', nsOrder.Notification_Type__c);
        Assert.equals(null, nsOrder.Notification_Reason__c);
        Assert.equals(Datetime.valueOfGmt('2019-03-04 01:08:28'), nsOrder.Appian_Notification_Date__c);
        Assert.equals(null, nsOrder.Order_Cancel_Charges_JSON__c);

        stubProvider.verify('cancelOpportunitySubscriptionsAndServices', 1, new List<ArgumentMatcher>{new StringArgumentMatcher(nsOrder.DF_Quote__r.Opportunity__c)});

        stubProvider.verify('createCancellationOpportunityAndAssociatedProduct', 0);

        stubProvider.verify('getOrderCancelChargesJson', 0);

        ArgumentCaptor argCaptor = new GenericObjectArgumentCaptor();
        emailServiceStubProvider.verify('sendNsOrderMail', 1, new List<ArgumentMatcher>{argCaptor});
        Map<String, String>  capturedMapArg = (Map<String, String>)argCaptor.getCaptured();
        Assert.equals(1, capturedMapArg.size());
        Assert.equals(SF_Constants.TEMPLATE_ORDER_CANCELLED, capturedMapArg.get((String)nsOrder.Id));
    }

}