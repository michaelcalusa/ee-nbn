/**
 * Created by philipstafford-jones on 23/4/19.
 */

@IsTest
private class NS_OrderInflightUtilCancelUtilTest {
    @IsTest
    static void testGetMetaData() {
        DF_Order_Settings__mdt mdt = NS_OrderInflightCancelUtil.getDfOrderSettings();
        System.assertEquals('SF-NS-000004', mdt.OrderCancelEventCode__c);
    }
}