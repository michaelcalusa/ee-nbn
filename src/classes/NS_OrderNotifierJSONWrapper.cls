public class NS_OrderNotifierJSONWrapper {
    public class NS_OrderNotifierJSONRoot{
        public Headers headers{get;set;}
        public Body body{get;set;}
        public Metadata metadata{get;set;}
        public NS_OrderNotifierJSONRoot(){
            this.headers = new Headers();
            this.headers.msgName = null;
            this.headers.msgType = null;
            this.headers.security = null;
            this.headers.timestamp = null;
            this.headers.activityName = null;
            this.headers.correlationId = null;
            this.headers.orderPriority = null;
            this.headers.accessSeekerID = null;
            this.headers.businessServiceName = null;
            this.headers.communicationPattern = null;
            this.headers.businessProcessVersion = null;
            this.headers.businessServiceVersion = null;
            this.headers.businessChannel = null;
            this.body = new Body();
            this.body.manageResourceOrderNotification = new NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot();
            this.metadata = new Metadata();
            this.metadata.source = null;
            this.metadata.application = null;
        }
    }
    public class Body{
        public NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot manageResourceOrderNotification{get;set;}
    }
    public class Headers{
        public String msgName{get;set;}
        public String msgType{get;set;}
        public String security{get;set;}
        public String timestamp{get;set;}
        public String activityName{get;set;}
        public String correlationId{get;set;}
        public String orderPriority{get;set;}
        public String accessSeekerID{get;set;}
        public String businessServiceName{get;set;}
        public String communicationPattern{get;set;}
        public String businessProcessVersion{get;set;}
        public String businessServiceVersion{get;set;}
        public String businessChannel{get;set;}
    }
    public class Metadata{
        public String source{get;set;}
        public String application{get;set;}
    }
}