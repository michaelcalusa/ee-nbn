/*Created by dheeraj*/
public class NS_OrderSubmittedNotificationController {

    @AuraEnabled
    public static  List<OrderHistory> getOrderNotifications(string orderId) {

        Map<String,List<String>> pickValueMap = PickListUtils.getDependentOptions('DF_Order__c','Order_Status__c','Order_Sub_Status__c');

        //maplist for dependent picklist values for Substatus
        map<string, string> orderStatusSubstatusMap = new map<string, string>();
        for(string sList : pickValueMap.keySet()){
            if(pickValueMap.get(sList).isEmpty()) {
                orderStatusSubstatusMap.put(sList, sList);
            }
            else{
                for(string str : pickValueMap.get(sList)) {
                    orderStatusSubstatusMap.put(str, sList);
                }
            }
        }

        DF_Order_History_Repo historyRepo = (DF_Order_History_Repo)ObjectFactory.getInstance(DF_Order_History_Repo.class);
        List<DF_Order_History_Wrapper> dforderHistoryList = historyRepo.getNsNotificationHistory(orderId);

        return getOrderHistoryList(groupFieldChangesByModifiedDate(dforderHistoryList), orderStatusSubstatusMap);
    }

    public static Map<String, List<DF_Order_History_Wrapper>> groupFieldChangesByModifiedDate(List<DF_Order_History_Wrapper> dforderHistoryList) {

        Map<String, List<DF_Order_History_Wrapper>> historyDeltasByModifiedDateMap = new Map<String, List<DF_Order_History_Wrapper>>();

        for(DF_Order_History_Wrapper dfh : dforderHistoryList) {
            String dateStr = String.valueof(dfh.getModifiedDate());
            if(!historyDeltasByModifiedDateMap.containsKey(dateStr)){
                historyDeltasByModifiedDateMap.put(dateStr, new List<DF_Order_History_Wrapper>());
            }
            historyDeltasByModifiedDateMap.get(dateStr).add(dfh);
        }
        return historyDeltasByModifiedDateMap;
    }

    public static List<OrderHistory> getOrderHistoryList(Map<String, List<DF_Order_History_Wrapper>> historyDeltasByModifiedDateMap, Map<String, String> orderStatusSubstatusMap) {

        List<OrderHistory> orderHistoryList = new List<OrderHistory>();

        OrderHistory previousState = null;

        for(String dateOfActivity : historyDeltasByModifiedDateMap.keySet()) {
            OrderHistory o = new OrderHistory();

            for(DF_Order_History_Wrapper dfh : historyDeltasByModifiedDateMap.get(dateOfActivity)) {
                if(dfh.getField() == 'Appian_Notification_Date__c') {
                    o.appianDate = DateTime.valueof(dfh.getNewValue());
                }
                else if(dfh.getField() == 'Order_Status__c') {
                    o.status = string.valueof(dfh.getNewValue());
                }
                else if(dfh.getField() == 'Order_Sub_Status__c') {
                    o.subStatus = string.valueof(dfh.getNewValue());
                    if(o.subStatus != null && o.status == null) {
                        if(orderStatusSubstatusMap != null) {
                            o.status = orderStatusSubstatusMap.get(o.subStatus);
                        }
                    }
                }
                else if(dfh.getField() == 'Notification_Type__c') {
                    o.notificationType = string.valueof(dfh.getNewValue());
                }
                else if(dfh.getField() == 'Notification_Reason__c') {
                    o.notificationReason = string.valueof(dfh.getNewValue());
                }
                //CPST-7850 | michaelcalusa@nbnco.com.au | ability to show revised completion date in NS
                else if(dfh.getField() == 'Reason_Code__c') {
                    o.notificationReasonCode = string.valueof(dfh.getNewValue());
                }
                // CPST-7968
                o.lastModifiedBy = dfh.getLastModifiedBy();
            }

            if(o.appianDate == null && dateOfActivity != null){
                //deafult to CreatedDate if no change to Appian_Notification_Date__c
                o.appianDate = Datetime.valueOf(dateOfActivity);
            }
            if(String.isBlank(o.status) && previousState != null){
                //populate based on previous state as there was no change/delta
                o.status = previousState.status;
            }
            //change to order could be manual change and not a result of a notification
//            if(String.isBlank(o.notificationType) && previousState != null){
//                //populate based on previous state as there was no change/delta
//                o.notificationType = previousState.notificationType;
//            }
            previousState = o;

            orderHistoryList.add(o);
        }
        system.debug('orderHistory::'+orderHistoryList);
        //note: do not change above SOQL query to use orderby DESC otherwise we will not be able to determine previous state of order when looping through history
        return reverseOrder(orderHistoryList);
    }

    private static List<OrderHistory> reverseOrder(List<OrderHistory> orderHistoryList){
        List<OrderHistory> reversedOrderHistoryList = new List<OrderHistory>();
        for(Integer i = orderHistoryList.size()-1; i >=0; i--){
            reversedOrderHistoryList.add(orderHistoryList.get(i));
        }
        return reversedOrderHistoryList;
    }
    
    public class OrderHistory {
        @AuraEnabled public String status {get; set;}
        @AuraEnabled public String subStatus {get; set;}
        @AuraEnabled public DateTime appianDate {get; set;}
        @AuraEnabled public String  notificationType {get; set;}
        @AuraEnabled public String  notificationReason {get; set;}
        //CPST-7850 | michaelcalusa@nbnco.com.au | ability to show revised completion date in NS
        @AuraEnabled public String  notificationReasonCode {get; set;}
        // CPST-7968
        @AuraEnabled public String  lastModifiedBy {get; set;}
    }

}