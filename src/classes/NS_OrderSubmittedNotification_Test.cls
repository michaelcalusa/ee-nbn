@isTest
private class NS_OrderSubmittedNotification_Test{
    @testSetup static void testDataSetup() {
        Id oppId = SF_TestService.getQuoteRecords();
        List<Opportunity> oppList = [SELECT Id FROM OPPORTUNITY WHERE Opportunity_Bundle__c = :oppId];
        System.debug('PP oppList:'+oppList);
        Opportunity opp = oppList.get(0);
        
        List<DF_Quote__c> dfQuoteList = [SELECT Id,Address__c,LAPI_Response__c,RAG__c,Fibre_Build_Cost__c,Opportunity_Bundle__c,Opportunity__c
                                     FROM DF_Quote__c WHERE Opportunity__c = :oppList.get(0).id limit 1];

        //List<DF_Order__c> dfOrderLst = new List<DF_Order__c>();
        
        //for(DF_Quote__c qtItem : dfQuoteList){
            DF_Order__c ord = new DF_Order__c();
            ord.DF_Quote__c = dfQuoteList[0].id;
            
            ord.Order_Status__c = 'Submitted';
            ord.Order_Sub_Status__c = null;
            ord.Notification_Type__c = null;
            ord.appian_notification_date__c = null;
            ord.Opportunity_Bundle__c = [Select id from DF_Opportunity_Bundle__c limit 1].Id;
        //    dfOrderLst.add(ord);
        //}

        insert ord;
        
        
    }
    
    @isTest static void shouldReturnNotificationHistory() {

        Datetime notification1Date = Datetime.now().addDays(-2);
        DF_Order_History_Wrapper lineItem1 = new DF_Order_History_Wrapper('Order_Status__c', 'Acknowledged', notification1Date);
        DF_Order_History_Wrapper lineItem2 = new DF_Order_History_Wrapper('Notification_Type__c', 'OrderAccepted', notification1Date);
        DF_Order_History_Wrapper lineItem3 = new DF_Order_History_Wrapper('Appian_Notification_Date__c', notification1Date, notification1Date);

        Datetime notification2Date = Datetime.now().addDays(-1);
        DF_Order_History_Wrapper lineItem4 = new DF_Order_History_Wrapper('Order_Status__c', 'In Progress', notification2Date);
        DF_Order_History_Wrapper lineItem5 = new DF_Order_History_Wrapper('Order_Sub_Status__c', 'Pending', notification2Date);
        DF_Order_History_Wrapper lineItem6 = new DF_Order_History_Wrapper('Notification_Type__c', 'RSPActionRequired', notification2Date);
        DF_Order_History_Wrapper lineItem7 = new DF_Order_History_Wrapper('Appian_Notification_Date__c', notification2Date, notification2Date);

        Datetime notification3Date = Datetime.now();
        DF_Order_History_Wrapper lineItem8 = new DF_Order_History_Wrapper('Order_Status__c', 'In Progress', notification3Date);
        DF_Order_History_Wrapper lineItem9 = new DF_Order_History_Wrapper('Order_Sub_Status__c', null, notification3Date);
        DF_Order_History_Wrapper lineItem10 = new DF_Order_History_Wrapper('Notification_Type__c', 'ConstructionStarted', notification3Date);
        DF_Order_History_Wrapper lineItem11 = new DF_Order_History_Wrapper('Appian_Notification_Date__c', notification3Date, notification3Date);

        List<DF_Order_History_Wrapper> orderHistoryList = new List<DF_Order_History_Wrapper>{lineItem1,lineItem2,lineItem3,lineItem4,lineItem5,lineItem6,lineItem7,lineItem8,lineItem9, lineItem10, lineItem11};

        String orderId = 'orderId';

        RecordingStubProvider stubProvider = new RecordingStubProvider(DF_Order_History_Repo.class);
        List<ArgumentMatcher> argMatchers = new List<ArgumentMatcher>{new StringArgumentMatcher(orderId)};
        stubProvider.given('getNsNotificationHistory').when(argMatchers).thenReturn(orderHistoryList);
        ObjectFactory.setStubProvider(DF_Order_History_Repo.class, stubProvider);

        Test.startTest();

        List<NS_OrderSubmittedNotificationController.OrderHistory> notificationList = NS_OrderSubmittedNotificationController.getOrderNotifications(orderId);

        Assert.equals(3, notificationList.size());

        NS_OrderSubmittedNotificationController.OrderHistory orderHistory = notificationList.get(0);
        Assert.equals('In Progress', orderHistory.status);
        Assert.isNull(orderHistory.subStatus);
        Assert.equals('ConstructionStarted', orderHistory.notificationType);
        Assert.equals(notification3Date, orderHistory.appianDate);

        orderHistory = notificationList.get(1);
        Assert.equals('In Progress', orderHistory.status);
        Assert.equals('Pending', orderHistory.subStatus);
        Assert.equals('RSPActionRequired', orderHistory.notificationType);
        Assert.equals(notification2Date, orderHistory.appianDate);

        orderHistory = notificationList.get(2);
        Assert.equals('Acknowledged', orderHistory.status);
        Assert.isNull(orderHistory.subStatus);
        Assert.equals('OrderAccepted', orderHistory.notificationType);
        Assert.equals(notification1Date, orderHistory.appianDate);

        Test.stopTest();
    }

    
}