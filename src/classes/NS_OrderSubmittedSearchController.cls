/**
 * Created by gobindkhurana on 4/9/18.
 */

public with sharing class NS_OrderSubmittedSearchController {

/*
* Fetches order records for the given search parameters
* Parameters : searchTerm, serachDate, searchStatus, isTriggerBySearch
* @Return : Returns a serialized string of list of DF Order records
*/

public static final String LOC_ID = 'LOC';
public static final String BUNDLE_ID = 'NSB-';
public static final String QUOTE_ID = 'NSQ-';
public static final String ORDER_ID = 'ROR';
public static final String ORDER_STATUS_DRAFT = 'In Draft';

@AuraEnabled
public static String getRecords(String searchTerm, String searchDate, String searchStatus, Boolean isTriggerBySearch) {

    System.debug('PPPP Search Term value is : ' + searchTerm);
    System.debug('PPPP Search Date value is : ' + searchDate);
    System.debug('PPPP Search Status value is : ' + searchStatus);
    System.debug('PPPP isTriggerBySearch : ' + isTriggerBySearch);

    //get the EE recordTypeId for DF_Order__c
    string orderRTId = SF_CS_API_Util.getRecordType(SF_Constants.NBN_SELECT_NAME, SF_Constants.ORDER_OBJECT);

    List<DF_Order__c> orderList = new List<DF_Order__c>();
    try {
        List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
        System.debug('PPPP User details ' + userLst);

        if(userLst.size()>0 && userLst.get(0).AccountId!=null){
            if(String.isBlank(searchTerm) && String.isBlank(searchDate) && String.isBlank(searchStatus)){
                String queryStr = 'SELECT Id, Name, Order_Id__c, Order_Submitted_Date__c, toLabel(Order_Status__c), BPI_Id__c, DF_Quote__r.Location_Id__c, DF_Quote__r.Address__c, Opportunity_Bundle__r.Name,DF_Quote__r.Quote_Name__c FROM DF_Order__c WHERE toLabel(Order_Status__c) != ' + '\'' + ORDER_STATUS_DRAFT + '\' AND recordTypeId = ' + '\'' + orderRTId + '\' AND Opportunity_Bundle__r.Account__c = ' + '\'' + userLst.get(0).AccountId + '\' ORDER BY Order_Submitted_Date__c DESC NULLS FIRST';
                orderList = (List<DF_Order__c>)SF_CS_API_Util.getQueryRecords(queryStr);
            }
            else if((String.isNotBlank(searchTerm) || String.isNotBlank(searchDate) || String.isNotBlank(searchStatus)) && isTriggerBySearch){
                String locId,ordId,quoteId;
                String queryStr = 'SELECT Id, Name, Order_Id__c, Order_Submitted_Date__c, toLabel(Order_Status__c), BPI_Id__c, DF_Quote__r.Location_Id__c, DF_Quote__r.Address__c, Opportunity_Bundle__r.Name,DF_Quote__r.Quote_Name__c FROM DF_Order__c WHERE toLabel(Order_Status__c) != ' + '\'' + ORDER_STATUS_DRAFT + '\' AND recordTypeId = ' + '\'' + orderRTId + '\' AND Opportunity_Bundle__r.Account__c = ' + '\'' + userLst.get(0).AccountId + '\'';
                if(String.isNotBlank(searchTerm)){
                    if(searchTerm.startsWithIgnoreCase(LOC_ID)){
                        locId = searchTerm;
                        queryStr += ' AND DF_Quote__r.Location_Id__c = ' + '\'' + locId + '\'';
                    }
                    else if(searchTerm.startsWithIgnoreCase(QUOTE_ID)){
                        quoteId = searchTerm;
                        queryStr += ' AND DF_Quote__r.Quote_Name__c = ' + '\'' + quoteId + '\'';
                    }
                    else if(searchTerm.startsWithIgnoreCase(ORDER_ID)){
                        ordId = searchTerm;
                        queryStr += ' AND Order_Id__c = ' + '\'' + ordId + '\'';
                    }
                    else{
                        List<DF_Order__c> orderEmptyList = new List<DF_Order__c>();
                        return JSON.serialize(orderEmptyList);
                    }
                }
                if(String.isNotBlank(searchDate))
                    queryStr += ' AND Order_Submitted_Date__c = ' + String.escapeSingleQuotes(searchDate);
                if(String.isNotBlank(searchStatus))
                    queryStr += ' AND toLabel(Order_Status__c) = ' + '\'' + searchStatus + '\'';

                System.debug('PPPP queryStr: '+queryStr);
                queryStr += ' ORDER BY Order_Submitted_Date__c DESC NULLS FIRST';
                orderList = (List<DF_Order__c>)SF_CS_API_Util.getQueryRecords(queryStr);
                System.debug('PPPP orderList: '+orderList);
            }
        }
        else{
            throw new CustomException('User record with the RSP account not found');
        }

    } catch(Exception e) {
        throw new CustomException(e.getMessage());
    }

    return JSON.serialize(orderList);
}

/*
* Fetches picklist values for Order status field
* Parameters : N/A
* @Return : Returns a serialized string of picklist values
*/
@AuraEnabled
public static String getPickListValuesIntoList(){
List<String> pickListValuesList= new List<String>();
Schema.DescribeFieldResult fieldResult = DF_Order__c.Order_Status__c.getDescribe();
List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
for( Schema.PicklistEntry pickListVal : ple){
if((!pickListVal.getLabel().equalsIgnoreCase('In Draft')) &&
   (!pickListVal.getLabel().equalsIgnoreCase('Sent To Billing')) && (!pickListVal.getLabel().equalsIgnoreCase('Billing Completed')))
pickListValuesList.add(pickListVal.getLabel());
}
return JSON.serialize(pickListValuesList);
}
}