@isTest 
private class NS_OrderSubmittedSearchController_Test {


    @isTest
    private static void test_getRecordsWithEmptySearchTerm() {

        User commUser;
        commUser = SF_TestData.createDFCommUser();
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            DF_Order__c ordObj = SF_TestService.createOrderRecord(userLst.get(0).AccountId);
            ordObj.Order_Status__c = 'Submitted';
            update ordObj;

            ordObj = SF_TestService.createOrderRecord(userLst.get(0).AccountId);
            ordObj.Order_Status__c = 'Acknowledged';
            update ordObj;

            test.startTest();

            Boolean triggeredBySearch = false;
            String result = NS_OrderSubmittedSearchController.getRecords('','','',triggeredBySearch);
            List<DF_Order__c> ordersList = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
            System.assertEquals(2, ordersList.size());

            triggeredBySearch = true;
            result = NS_OrderSubmittedSearchController.getRecords('','','',triggeredBySearch);
            ordersList = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
            System.assertEquals(2, ordersList.size());

            test.stopTest();
        }

    }
    
    @isTest
    private static void test_getRecordsWithInvalidSearchTerm() {
        // Setup data      
        String searchTerm = 'This search term will cause no record to return';
        String result = '';
        List<DF_Order__c> deserializedResult = new List<DF_Order__c>();
        
        User commUser;
        commUser = SF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            test.startTest();
            result = NS_OrderSubmittedSearchController.getRecords(searchTerm,null,'',true);
            deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
            test.stopTest();
        }                                    
        
        // Assertions
        system.AssertEquals(0, deserializedResult.size());
    }
    
    @isTest
    private static void test_getRecordsWithValidSearchTerm() {
        
        String searchTerm = 'ORD000000000641';
        String result = '';
        List<DF_Order__c> deserializedResult = new List<DF_Order__c>();
        
        User commUser;
        commUser = SF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            SF_TestService.createOrderRecord(userLst.get(0).AccountId);
            test.startTest();
            result = NS_OrderSubmittedSearchController.getRecords(searchTerm,'','',true);
            deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
            test.stopTest();                
        }                                    
        
    }
    
    @isTest
    private static void test_getRecordsWithValidSearchTermLOCID() {
        
        String searchTerm = 'LOC000035375038';
        String result = '';
        List<DF_Order__c> deserializedResult = new List<DF_Order__c>();
        
        User commUser;
        commUser = SF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            SF_TestService.createOrderRecord(userLst.get(0).AccountId);
            test.startTest();
            result = NS_OrderSubmittedSearchController.getRecords(searchTerm,'','',true);
            deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
            test.stopTest();                
        }                                    
        
    }
    
    @isTest
    private static void test_getRecordsWithValidSearchTermBundleID() {
        
        String result = '';
        List<DF_Order__c> deserializedResult = new List<DF_Order__c>();
        
        User commUser;
        commUser = SF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            SF_TestService.createOrderRecord(userLst.get(0).AccountId);
            List<DF_Opportunity_Bundle__c> oppBundle = [SELECT ID, Name FROM DF_Opportunity_Bundle__c WHERE Account__c = :userLst.get(0).AccountId];
            DF_Opportunity_Bundle__c oB = oppBundle.size()>0 ? oppBundle.get(0) : null;
            test.startTest();
            result = NS_OrderSubmittedSearchController.getRecords(oB.Name,'','',true);
            deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
            test.stopTest();                
        }                                    
        
    }
    
    @isTest
    private static void test_getRecordsWithValidSearchStatus() {
        
        String searchStatus = 'In Draft';
        String result = '';
        List<DF_Order__c> deserializedResult = new List<DF_Order__c>();
        
        User commUser;
        commUser = SF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            SF_TestService.createOrderRecord(userLst.get(0).AccountId);
            test.startTest();
            result = NS_OrderSubmittedSearchController.getRecords('','',searchStatus,true);
            deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
            test.stopTest();                
        }                                    
        
    }
    
    @isTest
    private static void test_getRecordsWithValidSearchDate() {
        
        String result = '';
        List<DF_Order__c> deserializedResult = new List<DF_Order__c>();
        
        User commUser;
        commUser = SF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            List<User> userLst = [SELECT AccountId,ContactId FROM User where id = :UserInfo.getUserId()];
            SF_TestService.createOrderRecord(userLst.get(0).AccountId);
            test.startTest();
            result = NS_OrderSubmittedSearchController.getRecords('','2018-04-24','',true);
            deserializedResult = (List<DF_Order__c>)JSON.deserialize(result, List<DF_Order__c>.Class);
            test.stopTest();                
        }                                    
        
    }
    
    @isTest
    private static void test_getPicklistValues() {
        
        String result = '';
        
        test.startTest();
        result = NS_OrderSubmittedSearchController.getPickListValuesIntoList();
        test.stopTest();                
        
        // Assertions
        system.AssertNotEquals('', result);
    }
}