/** By dheeraj
* Class Name: DF_OrderSubmittedSummaryController
* Description: This class used to show the DF Order submitted 
* Summary on partner community.
*
* */
public with sharing class NS_OrderSubmittedSummaryController {

    public static List<DF_Order__c> dfOrder;
    public static NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJson;

    @AuraEnabled public static LocationData lData {get; set;}
    @AuraEnabled public static NTDData nData {get; set;}

    @AuraEnabled public static List<RelatedContacts> lstBizContacts {get; set;}
    @AuraEnabled public static List<RelatedContacts> lstSiteContacts {get; set;}
    
    public static void getOrderData(String orderId) {   
        lstBizContacts = new List<RelatedContacts>();
        lstSiteContacts = new List<RelatedContacts>();   
        List<RelatedContacts> lstRoles = new List<RelatedContacts>();  
        
        System.debug('----orderId---'+orderId);
        dfOrder =  [SELECT  Name,
                            Order_JSON__c,
                            DF_Quote__r.Opportunity__r.Name,
                            DF_Quote__r.Latitude__c,
                            UNI_Port__c,
                            Order_Id__c,
                            BPI_Id__c,
                            DF_Quote__r.Longitude__c,
                            DF_Quote__r.Opportunity__c,
                            DF_Quote__r.Address__c,
                            Trading_Name__c,
                            Customer_Required_Date__c,
                            Heritage_Site__c,
                            Induction_Required__c,
                            Security_Required__c,
                            toLabel(Order_Sub_Status__c),
                            Order_Notifier_Response_JSON__c,
                            toLabel(Order_Status__c),
                            DF_Quote__r.Location_Id__c,
                            Service_Region__c,
                            NBN_Commitment_Date__c,
                            UNI_Id__c,
                            BTD_Id__c,
                            PRI_Id__c,
                            Opportunity_Bundle__r.Opportunity_Bundle_Name__c,
                            Power_Supply_1__c,
                            NTD_Installation_Appointment_Date__c,
                            NTD_Installation_Appointment_Time__c,
                            DF_Quote__r.Fibre_Build_Cost__c,
                            DF_Quote__r.New_Build_Cost__c,
                            DF_Quote__r.Id,
                            DF_Quote__r.RSP_Response_On_Cost_Variance__c,
                            DF_Quote__r.Appian_Response_On_Cost_Variance__c,
                            DF_Quote__r.Order_GUID__c,
                            DF_Quote__r.Quote_Name__c,
                            CreatedBy.Name,
                            LastModifiedBy.Name,
                            Revised_Delivery_Date__c
                        FROM DF_Order__c
                        WHERE Id=:orderId];
        
       
        if(!dfOrder.isEmpty()) {
            System.debug('---OrderJson'+ dfOrder[0].Order_JSON__c);
            if(String.isNotEmpty(dfOrder[0].Order_JSON__c)) {
                NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJson = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot)JSON.deserialize(dfOrder[0].Order_JSON__c, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class); 
                System.debug('---OrderJsondeserialize'+orderJson);
                                
                 lData = NS_OrderSubmittedSummaryHelper.getLocationHelper(dfOrder[0], orderJson);
                
                 nData = NS_OrderSubmittedSummaryHelper.getNTDHelper(nData, dfOrder[0], orderJson);
                 
                 //get biz / site contacts
                lstRoles = NS_OrderSubmittedSummaryHelper.getContactsHelper(lstRoles, orderJson);
                 for(RelatedContacts opptyRoles : lstRoles) {   
                     if(opptyRoles.contactType == 'Business') {
                         lstBizContacts.add(opptyRoles);
                     } else if (opptyRoles.contactType == 'Site') {
                         lstSiteContacts.add(opptyRoles);
                     }
                }
            }                                  
                                                           
        }
    }

    /**
    * @description Check If the Order is Cancellable
    * @param orderId - Salesforce record ID of an DF_Order Record
    * @return
    */
    @AuraEnabled
    public static Boolean canCancelOrder(String orderId) {
        // can cancel anytime unless order is 'Cancel Initiated' or 'Cancelled'
        // for now return true
        System.debug('orderId: ' + orderId);

        try {
            DF_Order__c order = [SELECT Order_Status__c,
                                        NBN_Commitment_Date__c,
                                        DF_Quote__r.Appian_Response_On_Cost_Variance__c,
                                        DF_Quote__r.New_Build_Cost__c
                                FROM DF_Order__c
                                WHERE Id = :orderId
                                LIMIT 1];

            String status = order.Order_Status__c;

            System.debug('status: ' + status);
            System.debug('NBN Commitment Date: ' + order.NBN_Commitment_Date__c);

            DF_Order_Settings__mdt ordSetting = NS_Order_Utils.getOrderSettings('NSOrderSettings');

            Integer businessDays = 0;
            if ( ordSetting.OrderCancelBusinessDays__c.intValue() > 0 ) {
                businessDays = ordSetting.OrderCancelBusinessDays__c.intValue();
            }

            System.debug('Cancel Order Business Days: ' + businessDays);
            System.debug('New Build cost: ' + order.DF_Quote__r.New_Build_Cost__c);
            System.debug('Appain response on cost variance: ' + order.DF_Quote__r.Appian_Response_On_Cost_Variance__c);

            // CPST-7492 Disable 'Cancel' for cost variance
            if( inCostVarianceState(order.DF_Quote__r.New_Build_Cost__c,
                                    order.DF_Quote__r.Appian_Response_On_Cost_Variance__c) ) {
                 System.debug('Cant cancel - in cost variance state');
                 return false;
            }

            if ( status == 'InProgress' && order.NBN_Commitment_Date__c == null ) {
                System.debug('Can cancel - NBN Commitment date not set and In Progress');
                return true;
            }

            // cant cancel if within 'businessDays' of commitment date
            if ( status == 'InProgress' &&
                    order.NBN_Commitment_Date__c != null &&
                    isWithinBusinessDays(order.NBN_Commitment_Date__c, businessDays) == false
                    ) {
                System.debug('Can cancel - Not within business days and In Progress');
                return true;
            }

        } catch (Exception e) {
            System.debug('Throw exception: ' + e.getMessage());
            throw new AuraHandledException('Exception in canCancelOrder: ' + e.getMessage());
        }
        System.debug('return FALSE');
        System.debug('Cant cancel - failed all rules');
        return false;
    }

    private static Boolean inCostVarianceState(Decimal newBuildCost, String appianResponseOnCostVariance) {
        // new build cost starts the process
        // cost varaince == Complete ends it
        if ( newBuildCost != null && appianResponseOnCostVariance != 'Completed') {
            return true;
        }
        return  false;
    }


    @TestVisible
    private static Date today = Date.today();

    @TestVisible
    private static Boolean isWithinBusinessDays(Date targetDate, Integer businessDays) {
        System.debug('isWithinBusinessDays target date: ' + targetDate);
        System.debug('isWithinBusinessDays business days offset from today: ' + businessDays);
        System.debug('isWithinBusinessDays tested date: ' + DF_AS_GlobalUtility.addBussinessDays(today, businessDays));

        if ( targetDate == null || targetDate > DF_AS_GlobalUtility.addBussinessDays(today, businessDays)) {
            return false;
        }
        return true;
    }


    @AuraEnabled
    public static Boolean cancelOrder(String orderId) {

        Boolean canCancel = false;
        try {
            canCancel = canCancelOrder(orderId);
        } catch (Exception e) {
            throw new AuraHandledException('Failure in canCancelOrder(' + orderId + ')');
        }

        if (canCancel) {
            try {
                NS_OrderInflightCancelUtil.sendCancelRequest(orderId);
            } catch (Exception e) {
                throw new AuraHandledException('Could not send cancel request: ' + e.getMessage());
            }
        } else {
            throw new AuraHandledException('Can_Not_Cancel');
        }

        try {
                DF_Order__c order = [   SELECT  Order_Status__c,
                                                Order_Sub_Status__c,
                                                Cancel_Initiated_Stage__c
                                        FROM    DF_Order__c
                                        WHERE Id = :orderId
                                        LIMIT 1];


                order.Cancel_Initiated_Stage__c = getDesignStage(orderId);
                System.debug('Cancel_Initiated_Stage__c: ' + order.Cancel_Initiated_Stage__c);

                order.Order_Status__c = 'Cancel Initiated';
                order.Order_Sub_Status__c = '';

                update order;

        } catch (DmlException e) {
            throw new AuraHandledException('Unable to set DF_Order__c.Order_Status__c to Cancel Initiated');
        }
        return true;
    }


    // returns Plan, Design or Build
    // looks down the order history fore relevant events
    @TestVisible
    private static String getDesignStage(String orderId) {

        String designStage = 'Plan';

        DF_Order_History_Repo historyRepo = (DF_Order_History_Repo) ObjectFactory.getInstance(DF_Order_History_Repo.class);
        List<DF_Order_History_Wrapper> orderHistoryList = historyRepo.getNsNotificationHistory(orderid);

        // process the order history list in reverse
        // to get the latest matching event
        for(Integer i = orderHistoryList.size() - 1; i >= 0; i--) {
            DF_Order_History_Wrapper orderHistory = orderHistoryList[i];

            String field = orderHistory.getField();
            Datetime modified = orderHistory.getModifiedDate();

            System.debug('getDesignStage List is' + field + ' = ' + orderHistory.getNewValue() +  ' : ' + modified);

            if ('Notification_Type__c'.equals(field)) {
                String value = (String) orderHistory.getNewValue();
                if (value.equalsIgnoreCase(DF_Constants.DF_ORDER_SUB_STATUS_ORDER_SCHEDULED)) {
                    designStage = 'Design';
                    break;
                }
                if (value.equalsIgnoreCase(DF_Constants.DF_ORDER_SUB_STATUS_CONSTRUCTION_STARTED)) {
                    designStage = 'Build';
                    break;
                }
            }
        }

        System.debug('Design Stage: ' + designStage);

        return designStage;
    }


    @AuraEnabled
    public static Map <String, Object> getAllOrderDetails(String orderId) {
        Map <String, Object> orderData = new Map<String, Object>();
        getOrderData(orderId);
        orderData.put('location' , ldata);
        orderData.put('businessContacts', lstBizContacts);
        orderData.put('siteContacts', lstSiteContacts);
        orderData.put('NTD', nData);
        return orderData;
    }


    @AuraEnabled
    public static Boolean updateRSPResponseOnCostVariance(String quoteId, String rspResponse, String orderGuid, String quoteName) {
        
        DF_Quote__c q = new DF_Quote__c(Id = quoteId,RSP_Response_On_Cost_Variance__c=rspResponse);
        System.debug('QUOTE ID:---' + quoteId);
        System.debug('QUOTE NAME:---' + quoteName);
        System.debug('ORDER GUID:---' + orderGuid);
        System.debug('RDP RESPONSE:---' + rspResponse);
        try
        {
            update q;
            NS_Order_Utils.createNsCostVarianceEvent(quoteName, orderGuid, rspResponse);
            NS_CostVarianceAcceptReject.updateCSRecorsInCVAccept(quoteId);
            return true;
        }
        catch(Exception ex)
        {
            return false;
        }
        
    }
                                                              
        
    public class LocationData {
        @AuraEnabled public String locId {get; set;}
        @AuraEnabled public String orderId {get; set;}
        @AuraEnabled public String latitude {get; set;}
        @AuraEnabled public String longitude {get; set;}
        @AuraEnabled public String address {get; set;}
        @AuraEnabled public String customerRequiredDate {get; set;}
        @AuraEnabled public String security {get; set;}
        @AuraEnabled public String notes {get; set;}
        @AuraEnabled public String tradingName {get; set;}
        @AuraEnabled public String induction {get; set;}
        @AuraEnabled public String AHA {get; set;}
        @AuraEnabled public String heritageSite {get; set;}
        @AuraEnabled public String priId {get; set;}
        @AuraEnabled public String siteName {get; set;}
        @AuraEnabled public String siteType {get; set;}
        @AuraEnabled public String bundleId {get; set;}
        @AuraEnabled public String siteAccIns {get; set;}
        @AuraEnabled public String contrLocIns {get; set;}
        @AuraEnabled public String propOwnStats {get; set;}
        @AuraEnabled public String areKeysReq {get; set;}
        @AuraEnabled public String installationNotes {get; set;}
        @AuraEnabled public String ntdInstallAppointmentDate {get; set;}
        @AuraEnabled public String fibreBuildCost {get;set;}
        @AuraEnabled public String newBuildCost {get;set;}
        @AuraEnabled public String rspResponseOnCostVariance {get;set;}
        @AuraEnabled public String appianResponseOnCostVariance {get;set;}
        @AuraEnabled public String quoteId {get;set;}
        @AuraEnabled public String quoteName {get;set;}
        @AuraEnabled public String orderGuid {get;set;}
        @AuraEnabled public String orderStatus {get;set;}
        @AuraEnabled public String orderSubStatus {get;set;}
		@AuraEnabled public String EBT {get;set;}
        @AuraEnabled public String BCD {get;set;}
        @AuraEnabled public String createdBy {get;set;}
        @AuraEnabled public String lastModifiedBy {get;set;}
        //CPST-7848 | michaelcalusa@nbnco.com.au | ability to show revised completion date in NS
        @AuraEnabled public String revisedCompletionDate {get;set;}
    }
    
    public class RelatedContacts {
        public String contactType {get; set;}
        @AuraEnabled public String name {get; set;}
        @AuraEnabled public String role {get; set;}
        @AuraEnabled public String email {get; set;}
        @AuraEnabled public String phone {get; set;}
        @AuraEnabled public String street {get; set;}
        @AuraEnabled public String suburb {get; set;}
        @AuraEnabled public String postcode {get; set;}
        @AuraEnabled public String state {get; set;}
        @AuraEnabled public String company {get; set;}
    }
    
    public class NTDData {
        @AuraEnabled public String batteryBackupRequired {get; set;}
        @AuraEnabled public String ntdType {get; set;}    
    }
}