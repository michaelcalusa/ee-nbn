/**
 * Created by philipstafford-jones on 16/4/19.
 */

@IsTest
public class NS_OrderSubmittedSummaryControllerTest {

    public static final String orderSubmitStr= '{"resourceOrder":{"workPracticesRequired":"Yes","workingAtHeightsConsideration":"No","tradingName":"test","siteType":"Gas Meter","siteName":"test","siteAccessInstructions":"adas","securityRequirements":"sd","safetyHazards":"Yes","resourceOrderType":"nbnSelect","resourceOrderComprisedOf":{"referencesResourceOrderItem":[{"itemInvolvesResource":{"type":"NTD","batteryBackupRequired":"No"},"action":"ADD"}],"itemInvolvesLocation":{"id":"LOC000096895622"}},"propertyOwnershipStatus":"Owned","ownerAwareOfProposal":"Yes","orderType":"Connect","notes":"asd","keysRequired":"er","jsaswmRequired":"No","itemInvolvesContact":[{"unstructuredAddress":{"stateTerritoryCode":"","postcode":"","localityName":"","addressLine3":null,"addressLine2":null,"addressLine1":""},"type":"Business","role":"","phoneNumber":"0486798655","emailAddress":"d@d.com","contactName":"asd asda","company":"asd"},{"unstructuredAddress":{"stateTerritoryCode":"","postcode":"","localityName":"","addressLine3":null,"addressLine2":null,"addressLine1":""},"type":"Site","role":"","phoneNumber":"0458798766","emailAddress":"e@e.com","contactName":"asd asd","company":""}],"inductionRequired":"Yes","heritageSite":"No","customerRequiredDate":"2018-09-24","contractedLocationInstructions":"ad","confinedSpaceRequirements":"No","associatedReferenceId":"NSQ-0000000563","afterHoursSiteVisit":"Yes","additionalNotes":"asd","accessSeekerInteraction":{"billingAccountID":null},"accessSeekerContact":{"contactPhone":null,"contactName":null}}}';


    @isTest
    static void testExecuteForInboundCall1() {

        Id oppId = SF_TestService.getQuoteRecords();
        List<Opportunity> oppList = [SELECT Id FROM OPPORTUNITY WHERE Opportunity_Bundle__c = :oppId];
        System.debug('PP oppList:' + oppList);
        Opportunity opp = oppList.get(0);

        List<DF_Quote__c> dfQuoteList = [
                SELECT Id,
                        Address__c,
                        LAPI_Response__c,
                        RAG__c,
                        Fibre_Build_Cost__c,
                        Opportunity_Bundle__c,
                        Opportunity__c
                FROM DF_Quote__c
                WHERE Opportunity__c = :oppList.get(0).id
                limit 1
        ];

        DF_Order__c ord = new DF_Order__c();
        ord.DF_Quote__c = dfQuoteList[0].id;
        ord.Order_Json__c = orderSubmitStr;
        ord.Opportunity_Bundle__c = [Select id from DF_Opportunity_Bundle__c limit 1].Id;

        insert ord;

        id ordId = [select id from df_order__c limit 1].id;
        Test.startTest();
                Map <String, Object> details = NS_OrderSubmittedSummaryController.getAllOrderDetails(ordId);
//                System.assert(details.get('location'));

//                orderData.put('location' , ldata);
//        orderData.put('businessContacts', lstBizContacts);
//        orderData.put('siteContacts', lstSiteContacts);
//        orderData.put('NTD', nData);
        Test.stopTest();
    }

    @IsTest
    private static void testWithinBusinessDays() {

        NS_OrderSubmittedSummaryController.today = Date.newInstance(2019, 04, 16); // Tuesday before good friday

        Test.startTest();

            // without holidays
            System.assertEquals(
                    NS_OrderSubmittedSummaryController.isWithinBusinessDays(Date.newInstance(2019,4,16), 1), true);
            System.assertEquals(
                    NS_OrderSubmittedSummaryController.isWithinBusinessDays(Date.newInstance(2019,4,17), 1), true);
            System.assertEquals(
                    NS_OrderSubmittedSummaryController.isWithinBusinessDays(Date.newInstance(2019,4,18), 1), false);

            // with holidays
            System.assertEquals(
                    NS_OrderSubmittedSummaryController.isWithinBusinessDays(Date.newInstance(2019,4,24), 4), true);
            System.assertEquals(
                    NS_OrderSubmittedSummaryController.isWithinBusinessDays(Date.newInstance(2019,4,25), 4), false);

        Test.stopTest();
    }


    @IsTest
    private static void canCancelOrderIfCommitmentDateNotSet() {

        User commUser = DF_TestData.createDFCommUser();
        User user = [Select contact.accountid from User where ID=:commUser.ID Limit 1];

        System.runAs(commUser) {

            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());

            DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(user.contact.accountid);
            insert opptyBundle;

            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';

            DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
            dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
            insert dfQuote;

            DF_Order__c order = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'InProgress');
            order.NBN_Commitment_Date__c = null;
            insert order;

            DF_Order__c orderCancelInitiated = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'Cancel Initiated');
            orderCancelInitiated.NBN_Commitment_Date__c = null;
            insert orderCancelInitiated;

            Test.startTest();
                System.assertEquals(NS_OrderSubmittedSummaryController.canCancelOrder(order.Id), true);
                System.assertEquals(NS_OrderSubmittedSummaryController.canCancelOrder(orderCancelInitiated.Id), false);
            Test.stopTest();
        }
    }

    @IsTest
    private static void cantCancelOrderIfCommitmentDateWithin1Day() {

        User commUser = DF_TestData.createDFCommUser();
        User user = [Select contact.accountid from User where ID=:commUser.ID Limit 1];


        System.runAs(commUser) {
            Date today = Date.today();

            DF_Opportunity_Bundle__c oppBundle = DF_TestData.createOpportunityBundle(user.contact.accountid);
            insert oppBundle;

            Date sunday = today.toStartOfWeek();
            Date monday = sunday.addDays(1);
            Date wednesday = monday.addDays(2);

            NS_OrderSubmittedSummaryController.today = monday;
            System.debug('monday is ' + monday);

            DF_Order__c orderToday = new DF_Order__c();
            orderToday.Opportunity_Bundle__c = oppBundle.Id;
            orderToday.Order_Status__c = 'InProgress';
            orderToday.NBN_Commitment_Date__c = monday;
            insert orderToday;

            DF_Order__c orderTommorow = new DF_Order__c();
            orderTommorow.Opportunity_Bundle__c = oppBundle.Id;
            orderTommorow.Order_Status__c = 'InProgress';
            orderTommorow.NBN_Commitment_Date__c = monday.addDays(1);
            insert orderTommorow;

            DF_Order__c orderTwoDaysTime = new DF_Order__c();
            orderTwoDaysTime.Opportunity_Bundle__c = oppBundle.Id;
            orderTwoDaysTime.Order_Status__c = 'InProgress';
            orderTwoDaysTime.NBN_Commitment_Date__c = monday.addDays(2);
            insert orderTwoDaysTime;

        Test.startTest();

            System.assertEquals(false, NS_OrderSubmittedSummaryController.canCancelOrder(orderToday.Id));
            System.assertEquals(false, NS_OrderSubmittedSummaryController.canCancelOrder(orderTommorow.Id));
            System.assertEquals(true, NS_OrderSubmittedSummaryController.canCancelOrder(orderTwoDaysTime.Id));

        Test.stopTest();
        }
    }


    @IsTest
    private static void cantCancelOrderIfCommitmentDateWithin1DayOverWeekend() {

        User commUser = DF_TestData.createDFCommUser();
        User user = [Select contact.accountid from User where ID=:commUser.ID Limit 1];


        System.runAs(commUser) {

            Date today = Date.today();

            DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(user.contact.accountid);
            insert opptyBundle;

            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';

            DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
            insert dfQuote;


            Date lastSunday = today.toStartOfWeek();
            Date nextMonday = lastSunday.addDays(8);
            Date friday = lastSunday.addDays(5);

            NS_OrderSubmittedSummaryController.today = friday;

            System.debug('TODAY: ' + friday);

            DF_Order__c orderNextMonday = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'InProgress');
            orderNextMonday.recordTypeId = SF_CS_API_Util.getRecordType(SF_Constants.NBN_SELECT_NAME, SF_Constants.ORDER_OBJECT);
            orderNextMonday.NBN_Commitment_Date__c = nextMonday;
            insert orderNextMonday;



            System.debug('Next Monday: ' + nextMonday);

            DF_Order__c orderNextTuesday = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'InProgress');
            orderNextMonday.recordTypeId = SF_CS_API_Util.getRecordType(SF_Constants.NBN_SELECT_NAME, SF_Constants.ORDER_OBJECT);
            orderNextTuesday.NBN_Commitment_Date__c = nextMonday.addDays(1);
            insert orderNextTuesday;

            System.debug('Next Tuesday: ' + nextMonday.addDays(1));

            Test.startTest();

                System.assertEquals(false, NS_OrderSubmittedSummaryController.canCancelOrder(orderNextMonday.Id));
                System.assertEquals(true, NS_OrderSubmittedSummaryController.canCancelOrder(orderNextTuesday.Id));

            Test.stopTest();
        }
    }



    @IsTest
    private static void cantCancelOrderIfStatusNotValid() {

        User commUser = DF_TestData.createDFCommUser();
        User user = [Select contact.accountid from User where ID=:commUser.ID Limit 1];

        system.runAs(commUser) {

            Date today = Date.today();

            DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(user.contact.accountid);
            insert opptyBundle;

            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';

            DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
            dfQuote.New_Build_Cost__c = 1000.00;
            insert dfQuote;

            DF_Order__c order = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'Cancelled');
            order.NBN_Commitment_Date__c = null;
            order.recordTypeId = SF_CS_API_Util.getRecordType(SF_Constants.NBN_SELECT_NAME, SF_Constants.ORDER_OBJECT);
            order.NBN_Commitment_Date__c = today.addDays(10);
            order.DF_Quote__c = dfQuote.Id;
            insert order;


            Test.startTest();

                System.assertEquals(NS_OrderSubmittedSummaryController.canCancelOrder(order.Id), false);

                order.Order_Status__c = 'Submitted';
                update order;
                System.assertEquals(NS_OrderSubmittedSummaryController.canCancelOrder(order.Id), false);

                order.Order_Status__c = 'Acknowledged';
                update order;
                System.assertEquals(NS_OrderSubmittedSummaryController.canCancelOrder(order.Id), false);

                order.Order_Status__c = 'Cancel Initiated';
                update order;
                System.assertEquals(NS_OrderSubmittedSummaryController.canCancelOrder(order.Id), false);

            Test.stopTest();
        }
    }

    @IsTest
    private static void
    cantCancelOrderIfInCostVariance() {

        User commUser = DF_TestData.createDFCommUser();
        User user = [Select contact.accountid from User where ID=:commUser.ID Limit 1];

        System.runAs(commUser) {

            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());

            DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(user.contact.accountid);
            insert opptyBundle;

            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';

            DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
            dfQuote.New_Build_Cost__c = 1000.00;
            insert dfQuote;

            Date today = Date.today();

            DF_Order__c order = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'InProgress');
            order.NBN_Commitment_Date__c = null;
            order.recordTypeId = SF_CS_API_Util.getRecordType(SF_Constants.NBN_SELECT_NAME, SF_Constants.ORDER_OBJECT);
            order.NBN_Commitment_Date__c = today.addDays(10);
            order.DF_Quote__c = dfQuote.Id;
            insert order;

            Test.startTest();
                System.assertEquals(false, NS_OrderSubmittedSummaryController.canCancelOrder(order.Id));
            Test.stopTest();
        }
    }

    @IsTest
    private static void
    canCancelOrderWhenCostVarianceComplete() {

        User commUser = DF_TestData.createDFCommUser();
        User user = [Select contact.accountid from User where ID=:commUser.ID Limit 1];

        System.runAs(commUser) {

            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());

            DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(user.contact.accountid);
            insert opptyBundle;

            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';

            DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
            dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
            dfQuote.Appian_Response_On_Cost_Variance__c = 'Completed';
            dfQuote.New_Build_Cost__c = 1000.00;
            insert dfQuote;

            Date today = Date.today();

            DF_Order__c order = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'InProgress');
            order.NBN_Commitment_Date__c = null;
            order.recordTypeId = SF_CS_API_Util.getRecordType(SF_Constants.NBN_SELECT_NAME, SF_Constants.ORDER_OBJECT);
            order.NBN_Commitment_Date__c = today.addDays(20);
            order.DF_Quote__c = dfQuote.Id;
            insert order;

            Test.startTest();
            System.assertEquals(true, NS_OrderSubmittedSummaryController.canCancelOrder(order.Id));
            Test.stopTest();

        }
    }


    @IsTest
    private static void
    getDesignPhaseTest() {

        User commUser = DF_TestData.createDFCommUser();
        User user = [Select contact.accountid from User where ID=:commUser.ID Limit 1];

        System.runAs(commUser) {

            System.debug('Current User: ' + UserInfo.getUserName());
            System.debug('Current Profile: ' + UserInfo.getProfileId());

            DF_Opportunity_Bundle__c opptyBundle = DF_TestData.createOpportunityBundle(user.contact.accountid);
            insert opptyBundle;

            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';

            DF_Quote__c dfQuote = DF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
            dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
            dfQuote.Appian_Response_On_Cost_Variance__c = 'Completed';
            dfQuote.New_Build_Cost__c = 1000.00;
            insert dfQuote;

            DF_Order__c order = DF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'InProgress');
            order.recordTypeId = SF_CS_API_Util.getRecordType(SF_Constants.NBN_SELECT_NAME, SF_Constants.ORDER_OBJECT);
            order.DF_Quote__c = dfQuote.Id;
            insert order;

            DF_Order_Settings__mdt ordSetting = NS_Order_Utils.getOrderSettings('NSOrderSettings');
            String orderScheduleEvent = DF_Constants.DF_ORDER_SUB_STATUS_ORDER_SCHEDULED;
            String orderConstructionStarted = DF_Constants.DF_ORDER_SUB_STATUS_CONSTRUCTION_STARTED;

            System.debug('Order Scheduled Event: ' + orderScheduleEvent);
            System.debug('Construction Started Event: ' + orderConstructionStarted);

            Test.startTest();
                System.assertEquals('Plan', NS_OrderSubmittedSummaryController.getDesignStage(order.Id));
//                order.Notification_Type__c = orderScheduleEvent;
//                update order;
            Test.stopTest();

//            System.assertEquals('Design', NS_OrderSubmittedSummaryController.getDesignStage(order.Id));

//            order.Notification_Type__c = orderConstructionStarted;
//            update order;
//
//            System.assertEquals('Build', NS_OrderSubmittedSummaryController.getDesignStage(order.Id));


        }
    }
}