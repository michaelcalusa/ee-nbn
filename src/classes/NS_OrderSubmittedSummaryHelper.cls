public class NS_OrderSubmittedSummaryHelper {
    public static NS_OrderSubmittedSummaryController.LocationData getLocationHelper(DF_Order__c dfOrder, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJson) {   

        //adding the location data to show on detail screen
        NS_OrderSubmittedSummaryController.LocationData lData = new NS_OrderSubmittedSummaryController.LocationData();
        lData.locId = dfOrder.DF_Quote__r.Location_Id__c;
        lData.orderId = dfOrder.Name; 
        lData.latitude = dfOrder.DF_Quote__r.Latitude__c;
        lData.longitude = dfOrder.DF_Quote__r.Longitude__c;
        lData.address = dfOrder.DF_Quote__r.Address__c;
        lData.tradingName = dfOrder.Trading_Name__c;
        lData.heritageSite = dfOrder.Heritage_Site__c; 
        lData.customerRequiredDate = Utils_DateTime.format(dfOrder.Customer_Required_Date__c);
        lData.induction = dfOrder.Induction_Required__c;
        lData.security = dfOrder.Security_Required__c;
        lData.priId = dfOrder.PRI_Id__c;
        lData.createdBy = dfOrder.CreatedBy.Name;

        lData.bundleId = dfOrder.Opportunity_Bundle__r.Opportunity_Bundle_Name__c;
        
        if(dfOrder.NTD_Installation_Appointment_Date__c != null) {
            Datetime dt = Datetime.newInstanceGmt(dfOrder.NTD_Installation_Appointment_Date__c, dfOrder.NTD_Installation_Appointment_Time__c);
            lData.ntdInstallAppointmentDate = dt.formatGmt('dd/MM/yyyy hh:mm a');
        }
        
        // estimated build timeframe
        NS_WhiteListQuoteUtil whiteListQuoteUtil = (NS_WhiteListQuoteUtil) ObjectFactory.getInstance(NS_WhiteListQuoteUtil.class);
        lData.EBT = whiteListQuoteUtil.getEBTFromWhiteList(lData.locId);
        
        lData.fibreBuildCost = String.valueOf(dfOrder.DF_Quote__r.Fibre_Build_Cost__c);
        lData.newBuildCost = String.valueOf(dfOrder.DF_Quote__r.New_Build_Cost__c);

        // return date as a String using user locale
        lData.BCD = Utils_DateTime.format(dfOrder.NBN_Commitment_Date__c);
        //CPST-7848 | michaelcalusa@nbnco.com.au | ability to show revised completion date in NS
        if (GlobalUtility.getReleaseToggle('NBN_Select_MR1908')) {
            if(dfOrder.Revised_Delivery_Date__c != null) {
                lData.revisedCompletionDate = Utils_DateTime.format(dfOrder.Revised_Delivery_Date__c);
            }
        }

        lData.rspResponseOnCostVariance =dfOrder.DF_Quote__r.RSP_Response_On_Cost_Variance__c;
        lData.appianResponseOnCostVariance =dfOrder.DF_Quote__r.Appian_Response_On_Cost_Variance__c;
        lData.quoteId =dfOrder.DF_Quote__r.Id;
        lData.quoteName = dfOrder.DF_Quote__r.Quote_Name__c;
        lData.orderGuid = dfOrder.DF_Quote__r.Order_GUID__c;
        lData.orderStatus = dfOrder.Order_Status__c;
        lData.orderSubStatus = dfOrder.Order_Sub_Status__c;

        if(orderJson != null) { //getting the OrderJson from getOrderData(orderId) method
            System.debug('orderJson: ' +orderJson);
            NS_OrderDataJSONWrapper.ResourceOrder objRscOrd = orderJson.resourceOrder;
            System.debug('---OrderJsonAttributes'+objRscOrd);
            
            if(objRscOrd != null) {
                lData.AHA = objRscOrd.afterHoursSiteVisit; 

                lData.notes = objRscOrd.notes;
                lData.siteAccIns = objRscOrd.siteAccessInstructions;

                lData.contrLocIns = objRscOrd.contractedLocationInstructions;
                lData.propOwnStats = objRscOrd.propertyOwnershipStatus;
                
                lData.areKeysReq = objRscOrd.keysRequired;
                lData.tradingName = objRscOrd.tradingName;
                lData.heritageSite = objRscOrd.heritageSite;
                lData.customerRequiredDate = String.valueOf(objRscOrd.customerRequiredDate);
                lData.induction = objRscOrd.inductionRequired;
                lData.security = objRscOrd.securityRequirements;
                lData.priId = objRscOrd.associatedReferenceId;
                lData.siteName = objRscOrd.siteName;
                lData.siteType = objRscOrd.siteType;
                lData.installationNotes = objRscOrd.additionalNotes;
            }
        }
        
        return lData;    
    }
    
    public static List<NS_OrderSubmittedSummaryController.RelatedContacts> getContactsHelper(List<NS_OrderSubmittedSummaryController.RelatedContacts> lstContacts, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJson) {
       /* return [SELECT Contact.Name, Contact.Email, Role, Contact.MailingCity, 
                            Contact.Phone, Contact.MailingState, Contact.MailingPostalCode,
                            Contact.MailingStreet
                            from OpportunityContactRole Where OpportunityId = :opptyId]; */
        NS_OrderDataJSONWrapper.ResourceOrder objRscOrd = orderJson.resourceOrder;
        if(objRscOrd != null) {
            List<NS_OrderDataJSONWrapper.ItemInvolvesContact> contacts = objRscOrd.itemInvolvesContact;
            if(contacts != null && !contacts.isEmpty()) {
                for(NS_OrderDataJSONWrapper.ItemInvolvesContact con : contacts) {
                    NS_OrderSubmittedSummaryController.RelatedContacts relContact = new NS_OrderSubmittedSummaryController.RelatedContacts();
                    relContact.contactType = con.type;
                    relContact.name = con.contactName;
                    relContact.role = con.role;
                    relContact.email = con.emailAddress;
                    relContact.phone = con.phoneNumber;
                    if(con.unstructuredAddress != null) {
                        NS_OrderDataJSONWrapper.UnstructuredAddress addressNode = con.unstructuredAddress;
                        relContact.street = addressNode.addressLine1 ;//+' '+ addressNode.addressLine2 +' '+addressNode.addressLine3;
                        relContact.street = String.isNotBlank(addressNode.addressLine2)? relContact.street+ +' '+ addressNode.addressLine2: relContact.street;
                        relContact.street = String.isNotBlank(addressNode.addressLine3)? relContact.street+ +' '+ addressNode.addressLine3: relContact.street;
                        relContact.suburb = addressNode.localityName;
                        relContact.postcode = addressNode.postcode;
                        relContact.state = addressNode.stateTerritoryCode;
                    }
                    lstContacts.add(relContact);
                }
            }
        }                    
        return lstContacts;                                   
    } 
    
    public static NS_OrderSubmittedSummaryController.NTDData getNTDHelper(NS_OrderSubmittedSummaryController.NTDData nData, DF_Order__c dfOrder, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJson) {   
        if(dfOrder != null) {
            
            if(orderJson != null) { //getting the OrderJson from getOrderData(orderId) method
                                              
                NS_OrderDataJSONWrapper.ResourceOrder objRscOrd = orderJson.resourceOrder;
                //adding the NTD details to show on UI   
                if(objRscOrd != null) {              
                    NS_OrderDataJSONWrapper.ResourceOrderComprisedOf objRscComp = objRscOrd.resourceOrderComprisedOf;
                    if(objRscComp != null) {
                        
                        List<NS_OrderDataJSONWrapper.ReferencesResourceOrderItem> lstRefRscItem = objRscComp.referencesResourceOrderItem;
                        System.debug('::RefRsc::'+lstRefRscItem);
                        if(lstRefRscItem != null) {
                            //integer countOvc = 0;
                            
                            for(NS_OrderDataJSONWrapper.ReferencesResourceOrderItem lstRef : lstRefRscItem) {
                                NS_OrderDataJSONWrapper.ItemInvolvesResource itemRsc = lstRef.itemInvolvesResource;
                                
                                //NTD/BTD                                
                                if(lstRef.itemInvolvesResource != null) {                                
                                    System.debug('::itemNTD::'+lstRef.itemInvolvesResource);
                                                                        
                                    nData = new NS_OrderSubmittedSummaryController.NTDData();
                                    //nData.btdId = dfOrder.BTD_Id__c;
                                    //nData.ntdType = itemRsc.type;
                                    nData.batteryBackupRequired = itemRsc.batteryBackupRequired;
                                    System.debug('::BatteryBackup::'+itemRsc.batteryBackupRequired);
                                    System.debug('::BatteryBackup1::'+lstRef.itemInvolvesResource.batteryBackupRequired);
                                    //nData.batteryBackupRequired=itemRsc.
                                    /*nData.ntdMounting = itemRsc.btdMounting;
                                    nData.powerSupply1 = itemRsc.powerSupply1;
                                    nData.powerSupply2 = itemRsc.powerSupply2;*/
                                }                                
                            }
                        }
                    }
                }
            }
                                                        
        }            
        return nData;
    }             
}