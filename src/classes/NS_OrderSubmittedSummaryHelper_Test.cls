@IsTest
public class NS_OrderSubmittedSummaryHelper_Test {
    
    @IsTest static void should_call_getEBTFromWhiteList_on_getLocationHelper_method() {
        String locId = 'LOC123456789012';
        String returnEBT = 'return value';
        
        Account acc = SF_TestData.createAccount('My account');
        acc.Access_Seeker_ID__c = 'ASI000000000019';
        insert acc;
        
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acc.Id);
        insert opptyBundle;
        
        // Create Oppty
        Opportunity oppty = SF_TestData.createOpportunity(locId);
        oppty.AccountId = acc.Id;
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;
        
        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = SF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;
        
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        
        // Create DFQuote recs
        DF_Quote__c dfQuote = SF_TestData.createDFQuote(address, latitude, longitude, locId, oppty.Id, opptyBundle.Id, 1000, 'Green');        
        dfQuote.Proceed_to_Pricing__c = true;
        dfQuote.Location_Id__c = locId;
        insert dfQuote;
        
        DF_Order__c dfOrder = new DF_Order__c();
        dfOrder.DF_Quote__c = dfQuote.Id;
        dfOrder.Opportunity_Bundle__c = opptyBundle.Id;
        insert dfOrder;
        
        Id orderId = dfOrder.Id;
        DF_Order__c fetchedOrder = getOrder(orderId);
        
        RecordingStubProvider stubProvider = new RecordingStubProvider(NS_WhiteListQuoteUtil.class);
        ArgumentMatcher locIdMatcher = new StringArgumentMatcher(locId);        
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>{locIdMatcher};
            
        stubProvider.given('getEBTFromWhiteList').when(argMatcherList).thenReturn(returnEBT);
        ObjectFactory.setStubProvider(NS_WhiteListQuoteUtil.class, stubProvider);
        
        NS_OrderSubmittedSummaryController.LocationData result = NS_OrderSubmittedSummaryHelper.getLocationHelper(fetchedOrder, null);
        
        stubProvider.verify('getEBTFromWhiteList', 1, argMatcherList);
        
        Assert.equals(returnEBT, result.EBT);
        Assert.equals(locId, result.locId);
    }

    @IsTest static void shouldPopulateOrderDetails() {

        User user = SF_TestData.createDFCommUser();

        user = [SELECT Id, Account.Id, Name FROM User where Id =:user.Id];

        System.runAs(user) {
            String locId = 'LOC123456789012';

            DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(user.Account.Id);
            insert opptyBundle;

            // Create Oppty
            Opportunity oppty = SF_TestData.createOpportunity(locId);
            oppty.AccountId = user.Account.Id;
            oppty.Opportunity_Bundle__c = opptyBundle.Id;
            insert oppty;

            // Create Product Basket
            cscfga__Product_Basket__c prodBasket = SF_TestData.buildBasket();
            prodBasket.cscfga__Opportunity__c = oppty.Id;
            insert prodBasket;

            String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
            String latitude = '-33.840213';
            String longitude = '151.207368';

            // Create DFQuote recs

            String appianResponse = 'OK';
            DF_Quote__c dfQuote = SF_TestData.createDFQuote(address, latitude, longitude, locId, oppty.Id, opptyBundle.Id, 1000, 'Green');
            dfQuote.Appian_Response_On_Cost_Variance__c = appianResponse;
            insert dfQuote;

            DF_Order__c dfOrder = new DF_Order__c();
            dfOrder.DF_Quote__c = dfQuote.Id;
            dfOrder.Opportunity_Bundle__c = opptyBundle.Id;
            dfOrder.Order_Status__c = 'InProgress';
            dfOrder.Order_Sub_Status__c = 'OrderScheduled';
            dfOrder.Customer_Required_Date__c = Date.newInstance(2019, 5, 20);
            dfOrder.NBN_Commitment_Date__c = Date.newInstance(2019, 8, 20);

            insert dfOrder;

            Id orderId = dfOrder.Id;
            DF_Order__c fetchedOrder = getOrder(orderId);


            NS_OrderSubmittedSummaryController.LocationData result = NS_OrderSubmittedSummaryHelper.getLocationHelper(fetchedOrder, null);

            Assert.equals(appianResponse, result.appianResponseOnCostVariance);
            Assert.equals('In Progress', result.orderStatus);
            Assert.equals('Order Scheduled', result.orderSubStatus);
            Assert.equals(user.Name, result.createdBy);
            Assert.equals('20/05/2019', result.customerRequiredDate);
            Assert.equals('20/08/2019', result.BCD);
        }
    }

    private static DF_Order__c getOrder(Id orderId) {
        DF_Order__c fetchedOrder = [
                SELECT Id, Name, DF_Quote__r.Location_Id__c,
                        DF_Quote__r.Latitude__c,
                        DF_Quote__r.Longitude__c,
                        DF_Quote__r.Address__c,
                        Opportunity_Bundle__r.Opportunity_Bundle_Name__c,
                        DF_Quote__r.Fibre_Build_Cost__c,
                        DF_Quote__r.New_Build_Cost__c,
                        DF_Quote__r.RSP_Response_On_Cost_Variance__c,
                        DF_Quote__r.Appian_Response_On_Cost_Variance__c,
                        DF_Quote__r.Id,
                        DF_Quote__r.Quote_Name__c,
                        DF_Quote__r.Order_GUID__c,Trading_Name__c,Heritage_Site__c, Customer_Required_Date__c, Induction_Required__c,
                        Security_Required__c, PRI_Id__c, NTD_Installation_Date__c, NBN_Commitment_Date__c, toLabel(Order_Status__c), toLabel(Order_Sub_Status__c),
                        CreatedBy.Name,
                        NTD_Installation_Appointment_Time__c, NTD_Installation_Appointment_Date__c
                FROM DF_Order__c
                WHERE Id = :orderId
        ];
        return fetchedOrder;
    }

}