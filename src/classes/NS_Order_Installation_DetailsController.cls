public with sharing class NS_Order_Installation_DetailsController {
    
    @AuraEnabled
    public static String getOrderDetails(String strOrderId){
        return NS_Order_ItemController.getOrderDetails(strOrderId);
    }
    
	@AuraEnabled
    public static List<String> getUpcomingHolidays(){
        List<String> toReturn = new List<String>();
        List<Holiday> holidays = [select Activitydate from holiday where activitydate >= TODAY];        
        
        for(Holiday h : holidays)
        {
            toReturn.add(string.valueOf(h.ActivityDate));
        }
        return toReturn;
    }    
    
    @AuraEnabled
    public static String saveOrderDetails(String strOrderId, String strOrderJson){
        
        return NS_Order_ItemController.saveOrderDetails(strOrderId, strOrderJson);
    }
    
    @AuraEnabled
    public static String createCSOrders(String strOrderId){
        
        String orderCreationMessage;
        System.debug('strOrderId: ' + strOrderId);
        try{
            if(String.isNotBlank(strOrderId)){
                DF_Order__c orderRec;
                Opportunity oppty;
                orderRec = [SELECT Id, Order_JSON__c, DF_Quote__r.Order_GUID__c,DF_Quote__r.Opportunity__c,Order_Status__c FROM DF_Order__c WHERE Id =:strOrderId LIMIT 1];
                if(orderRec != null && orderRec.DF_Quote__r.Opportunity__c != null){
                    oppty = [SELECT Id, StageName, (SELECT Id FROM cscfga__Product_Baskets__r LIMIT 1) FROM Opportunity WHERE Id =: orderRec.DF_Quote__r.Opportunity__c LIMIT 1];
                }
                cscfga__Product_Basket__c basket = oppty.cscfga__Product_Baskets__r;
                String syncResults = SF_CS_API_Util.syncWithOpportunity(basket.Id);
                if(syncResults.equalsIgnoreCase('sychronised with opportunity')){
                    SF_DesktopAssessmentController.updateOppClosedWon(oppty,basket);
                    orderCreationMessage = 'Passed';
                }
            }
            
        } catch(Exception ex) {
            System.debug('NS_Order_Installation_DetailsController::submitOrder: EXCEPTION: '+ ex.getMessage() + '\n' + ex.getStackTraceString());  
            orderCreationMessage = 'Failed';
        }
        return orderCreationMessage;
    }
    
    @AuraEnabled
    public static DF_Order__c submitOrder(String strOrderId){
        
        DF_Order__c orderRec;
        System.debug('strOrderId: ' + strOrderId);
        try{
            if(String.isNotBlank(strOrderId)){
                orderRec = [SELECT Id, Order_JSON__c, DF_Quote__r.Order_GUID__c,Order_Status__c FROM DF_Order__c WHERE Id = :strOrderId LIMIT 1];
                
                if(orderRec.Order_Status__c == 'In Draft' || orderRec.Order_Status__c == ''){
                    system.debug('---Order Status ::before'+orderRec.Order_Status__c);
                    orderRec.Order_Status__c = 'Pending Submission';
                    system.debug('---Order Status:: after'+orderRec.Order_Status__c);
                    NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderRoot = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot)JSON.deserialize(orderRec.Order_JSON__c, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class);
                    orderRoot.resourceOrder.dateReceived = System.Datetime.now();
                    orderRec.Order_JSON__c = JSON.serialize(orderRoot);
                }
                
                Database.update(orderRec);
                NS_Order_Utils.createNsOrderEvent(orderRec);
            }
            
        }catch(Exception ex){
            System.debug('NS_Order_Installation_DetailsController::submitOrder: EXCEPTION: '+ ex.getMessage() + '\n' + ex.getStackTraceString());  
        }
        return (orderRec);
    }
}