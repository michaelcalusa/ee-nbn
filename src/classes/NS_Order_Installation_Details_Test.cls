@isTest
public class NS_Order_Installation_Details_Test {
    
    @testSetup
    static void testSetup(){
        User nbnUser1 = SF_TestData.createJitUser('AS00000099', 'Access Seeker nbnSelect Feasibility', 'nbnRsp1');
        Account acc1 = [SELECT Id FROM Account WHERE Access_Seeker_ID__c = 'AS00000099' LIMIT 1];
        DF_Opportunity_Bundle__c oppBundle = SF_TestData.createOpportunityBundle(acc1.Id);
        
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        Test.startTest();

        System.runAs(rspUser1){
            Database.insert(oppBundle);
            Opportunity opp= SF_TestData.createOpportunity('Test');
            insert opp;
            cscfga__Product_Basket__c basket = SF_TestData.buildBasketWithOpportnity(opp.Id);
            insert basket;
            cscfga__Product_Category__c pc = SF_TestData.buildProductCategory();
            insert pc;
            cscfga__Product_Definition__c pd = SF_TestData.buildProductDefinitionWithCategory('NBNSelect', pc.Id);
            insert pd;
            cscfga__Attribute_Definition__c ad = SF_TestData.buildAttributeDefinitionAsLineItem('FBC',pd.Id, true);
            insert ad;
            cscfga__Product_Configuration__c pConfig = SF_TestData.buildProductConfigwithBasket(pd.Id, basket.Id);
            insert pConfig;
            csbb__Product_Configuration_Request__c pcr = SF_TestData.buildConfigRequest(basket.Id,pConfig.Id, pc.Id);
            insert pcr;
            cscfga__Attribute__c attr = SF_TestData.buildAttributeAsLineItem('FBC', '2000', pConfig.Id, ad.Id, true);
            insert attr;
            //NS_Custom_Options__c df = SF_TestData.insertDFCustomOptionsCustomSettingRecords('SF_OPPORTUNITY_STAGE', 'Closed Won');
            //insert df;
            List<NS_Custom_Options__c>  lstNsCS = SF_TestData.createCustomSettingsforSiteSurveyCharge('','','');
            Database.insert(lstNsCS);
            
            DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, oppBundle.Id, null, 'Green');
            Database.insert(dfQuote1);
            dfQuote1.Opportunity__c = opp.Id; 
            Database.update(dfQuote1);
            DF_Order__c dfOrder1 = SF_TestData.createDFOrder(dfQuote1.Id, oppBundle.Id, 'In Draft');
            Database.insert(dfOrder1);
        }

        Test.stopTest();
    }

    static testMethod void testGetOrderDetails_Positive(){
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        System.runAs(rspUser1){
            DF_Order__c dfO= [SELECT Id, Order_JSON__c, DF_Quote__r.Quote_Name__c FROM DF_Order__c WHERE Order_Status__c = 'In Draft'];
            System.assertEquals(true, String.isBlank(dfO.Order_JSON__c));
            String strResponse  = NS_Order_Installation_DetailsController.getOrderDetails(String.valueOf(dfO.Id));
            NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJson = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot)JSON.deserialize(strResponse, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class); 
            System.assertEquals(dfO.DF_Quote__r.Quote_Name__c, orderJson.resourceOrder.associatedReferenceId);
        }
    }
    static testMethod void testGetOrderDetails_Negative(){
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        System.runAs(rspUser1){
            try{
                String strResponse  = NS_Order_Installation_DetailsController.getOrderDetails('TEST');
            }catch(Exception ex){
                //do nothing, exception expected
            }
        }
    }
    static testMethod void testSaveOrderDetails_Positive(){
        String strOrderJson ='{"resourceOrder":{"id":"ROR000000001425","notes":"test","siteName":"siteTest","siteType":"Education","orderType":"Connect","tradingName":"tradeTest","dateReceived":"2018-09-25T22:16:59Z","heritageSite":"Yes","keysRequired":"test","safetyHazards":"0","additionalNotes":"test","jsasswmRequired":null,"inductionRequired":"Yes","interactionStatus":"InProgress","resourceOrderType":"NBN Select","accessSeekerContact":{"contactName":null,"contactPhone":null},"afterHoursSiteVisit":"Yes","itemInvolvesContact":[{"role":"test","type":"Business","contactName":"test test","phoneNumber":"0423237872","emailAddress":"test@dsc.com","unstructuredAddress":{"postcode":"3003","addressLine1":"test","addressLine2":"","addressLine3":"","localityName":"test","stateTerritoryCode":"SA"}},{"role":"","type":"Site","contactName":"test test","phoneNumber":"0423232424","emailAddress":"test@sdcs.com","unstructuredAddress":{"postcode":"","addressLine1":"","addressLine2":"","addressLine3":"","localityName":"","stateTerritoryCode":""}}],"customerRequiredDate":"2018-11-15","ownerAwareOfProposal":"Yes","securityRequirements":"test","associatedReferenceId":"NSQ-0000000574","plannedCompletionDate":null,"workPracticesRequired":"Yes","siteAccessInstructions":"test","interactionDateComplete":null,"propertyOwnershipStatus":"Leased","resourceOrderComprisedOf":{"itemInvolvesLocation":{"id":"LOC000049831879"},"referencesResourceOrderItem":[{"action":"ADD","itemInvolvesResource":{"type":"NTD","batteryBackupRequired":"Yes"}}]},"confinedSpaceRequirements":"Yes","workingAtHeightsConsideration":"Yes","contractedLocationInstructions":"test"},"notificationType":"OrderAccepted"}';
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        System.runAs(rspUser1){
            DF_Order__c dfO= [SELECT Id, Order_JSON__c, DF_Quote__r.Quote_Name__c, Trading_Name__c FROM DF_Order__c WHERE Order_Status__c = 'In Draft'];
            System.assertEquals(true, String.isBlank(dfO.Order_JSON__c));
            String strResponse  = NS_Order_Installation_DetailsController.saveOrderDetails(String.valueOf(dfO.Id), strOrderJson);
            DF_Order__c updatedDfO= [SELECT Id, Order_JSON__c, DF_Quote__r.Quote_Name__c, Trading_Name__c FROM DF_Order__c WHERE Id = :dfO.Id];
            NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJson = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot)JSON.deserialize(NS_Order_ItemController.getOrderDetails(String.valueOf(dfO.Id)), NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class); 
            System.assertEquals(updatedDfO.Trading_Name__c, orderJson.resourceOrder.tradingName);
        }
    }
    static testMethod void testSaveOrderDetails_Negative(){
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        System.runAs(rspUser1){
            try{
                String strResponse  = NS_Order_Installation_DetailsController.saveOrderDetails('TEST', null);
            }catch(Exception ex){
                //do nothing, exception expected
            }
        }
    }
    
    static testMethod void testsubmitOrder_Positive(){
        String strOrderJson ='{"resourceOrder":{"id":"ROR000000001425","notes":"test","siteName":"siteTest","siteType":"Education","orderType":"Connect","tradingName":"tradeTest","dateReceived":"2018-09-25T22:16:59Z","heritageSite":"Yes","keysRequired":"test","safetyHazards":"0","additionalNotes":"test","jsasswmRequired":null,"inductionRequired":"Yes","interactionStatus":"InProgress","resourceOrderType":"NBN Select","accessSeekerContact":{"contactName":null,"contactPhone":null},"afterHoursSiteVisit":"Yes","itemInvolvesContact":[{"role":"test","type":"Business","contactName":"test test","phoneNumber":"0423237872","emailAddress":"test@dsc.com","unstructuredAddress":{"postcode":"3003","addressLine1":"test","addressLine2":"","addressLine3":"","localityName":"test","stateTerritoryCode":"SA"}},{"role":"","type":"Site","contactName":"test test","phoneNumber":"0423232424","emailAddress":"test@sdcs.com","unstructuredAddress":{"postcode":"","addressLine1":"","addressLine2":"","addressLine3":"","localityName":"","stateTerritoryCode":""}}],"customerRequiredDate":"2018-11-15","ownerAwareOfProposal":"Yes","securityRequirements":"test","associatedReferenceId":"NSQ-0000000574","plannedCompletionDate":null,"workPracticesRequired":"Yes","siteAccessInstructions":"test","interactionDateComplete":null,"propertyOwnershipStatus":"Leased","resourceOrderComprisedOf":{"itemInvolvesLocation":{"id":"LOC000049831879"},"referencesResourceOrderItem":[{"action":"ADD","itemInvolvesResource":{"type":"NTD","batteryBackupRequired":"Yes"}}]},"confinedSpaceRequirements":"Yes","workingAtHeightsConsideration":"Yes","contractedLocationInstructions":"test"},"notificationType":"OrderAccepted"}';
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        System.runAs(rspUser1){
            DF_Order__c dfO= [SELECT Id, Order_JSON__c, DF_Quote__r.Order_GUID__c,Order_Status__c FROM DF_Order__c WHERE Order_Status__c = 'In Draft'];
            System.assertEquals(true, String.isBlank(dfO.Order_JSON__c));
            DF_Order__c orderRec = NS_Order_Installation_DetailsController.submitOrder(String.valueOf(dfO.Id));
            //DF_Order__c updatedDfO= [SELECT Id, Order_JSON__c, DF_Quote__r.Order_GUID__c,Order_Status__c FROM DF_Order__c WHERE Id = :dfO.Id];
            NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJson = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot)JSON.deserialize(NS_Order_ItemController.getOrderDetails(String.valueOf(dfO.Id)), NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class); 
            System.assertEquals(orderRec.Order_Status__c, 'Pending Submission');
            //System.assertEquals(orderJson.resourceOrder.dateReceived, System.Datetime.now());
            Database.update(orderRec);
            NS_Order_Event__e orderEvent = NS_Order_Utils.createNsOrderEvent(orderRec);
            
        }
    }
    
    static testMethod void testsubmitOrder_Negative(){
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        System.runAs(rspUser1){
            try{
                NS_Order_Installation_DetailsController.submitOrder('TEST');
            }catch(Exception ex){
                //do nothing, exception expected
            }
        }
    }

    static testMethod void testcreateCSOrder_Positive(){
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        System.runAs(rspUser1){
            try{
                DF_Order__c dfO= [SELECT Id, Order_JSON__c, DF_Quote__r.Order_GUID__c,Order_Status__c FROM DF_Order__c WHERE Order_Status__c = 'In Draft'];
                String message = NS_Order_Installation_DetailsController.createCSOrders(dfo.Id);
                System.assertEquals(message,'Passed');
            }catch(Exception ex){
               
            }
        }
    }

    static testMethod void testcreateCSOrder_Negative(){
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        System.runAs(rspUser1){
            try{
                String message = NS_Order_Installation_DetailsController.createCSOrders('');
                System.assertEquals(message,null);
            }catch(Exception ex){
                
            }
        }
    }
            
            
}