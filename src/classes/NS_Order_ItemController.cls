public class NS_Order_ItemController {
    
    @AuraEnabled
    public static String getOrderDetails(String strOrderId){
        NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJson = new NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot();
        try{
            if(String.isNotBlank(strOrderId)){
                DF_Order__c orderRec = [SELECT Id, Order_JSON__c,DF_Quote__r.Quote_Name__c, DF_Quote__r.Location_Id__c, Opportunity_Bundle__r.Account__r.Billing_ID__c FROM DF_Order__c WHERE Id = :strOrderId LIMIT 1];
                if(String.isNotBlank(orderRec.Order_JSON__c)){
                    orderJson = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot)JSON.deserialize(orderRec.Order_JSON__c, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class); 
                }else{
                    //populate additional values 
                    orderJson.resourceOrder.associatedReferenceId = orderRec.DF_Quote__r.Quote_Name__c;
                    orderJson.resourceOrder.accessSeekerInteraction.billingAccountID = orderRec.Opportunity_Bundle__r.Account__r.Billing_ID__c;
                    orderJson.resourceOrder.resourceOrderComprisedOf.itemInvolvesLocation.id = orderRec.DF_Quote__r.Location_Id__c;
                    orderJson.resourceOrder.orderType = 'Connect';
                    orderJson.resourceOrder.resourceOrderType = 'nbnSelect';
                    orderJson.resourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem = new List<NS_OrderDataJSONWrapper.ReferencesResourceOrderItem>();
                    NS_OrderDataJSONWrapper.ReferencesResourceOrderItem refResOrderItem = new NS_OrderDataJSONWrapper.ReferencesResourceOrderItem();
                    refResOrderItem.action = 'ADD';
                    refResOrderItem.itemInvolvesResource = new NS_OrderDataJSONWrapper.ItemInvolvesResource();
                    refResOrderItem.itemInvolvesResource.type = 'NTD';
                    orderJson.resourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem.add(refResOrderItem);
                }
            }
        }catch(Exception ex){
            System.debug('NS_Order_ItemController::getOrderDetails: EXCEPTION: '+ ex.getMessage() + '\n' + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage());
        }
        System.debug('ORDER GET JSON: ' + JSON.serialize(orderJson));
        return JSON.serialize(orderJson);
    }
    @AuraEnabled
    public static String saveOrderDetails(String strOrderId, String strOrderJson){
        DF_Order__c orderRec;
        System.debug('strOrderId: ' + strOrderId + '\nstrOrderJson: ' + strOrderJson);
        try{
            if(String.isNotBlank(strOrderId)){
                orderRec = [SELECT Id, Order_JSON__c, Customer_Required_Date__c, Heritage_Site__c, Induction_Required__c, Installation_Notes__c, Security_Required__c, Trading_Name__c, Site_Notes__c FROM DF_Order__c WHERE Id = :strOrderId LIMIT 1];
                NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJson = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot)JSON.deserialize(strOrderJson, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class); 
                
                /***add fields to save***/
                orderRec.Order_JSON__c = strOrderJson;
                System.debug('orderRec.Order_JSON__c: ' + strOrderJson);
                if(String.isNotBlank(orderJson.resourceOrder.customerRequiredDate)){
                    orderRec.Customer_Required_Date__c = Date.valueOf(orderJson.resourceOrder.customerRequiredDate);
                }
                if(String.isNotBlank(orderJson.resourceOrder.heritageSite)){
                    orderRec.Heritage_Site__c = orderJson.resourceOrder.heritageSite;
                }
                if(String.isNotBlank(orderJson.resourceOrder.inductionRequired)){
                    orderRec.Induction_Required__c = orderJson.resourceOrder.inductionRequired;
                }
                if(String.isNotBlank(orderJson.resourceOrder.additionalNotes)){
                    orderRec.Installation_Notes__c = orderJson.resourceOrder.additionalNotes;
                }
                /*if(String.isNotBlank(orderJson.resourceOrder.securityRequirements)){
                    orderRec.Security_Required__c = orderJson.resourceOrder.securityRequirements;
                }*/
                if(String.isNotBlank(orderJson.resourceOrder.tradingName)){
                    orderRec.Trading_Name__c = orderJson.resourceOrder.tradingName;
                }
                if(String.isNotBlank(orderJson.resourceOrder.notes)){
                    orderRec.Site_Notes__c = orderJson.resourceOrder.notes;
                }
                Database.update(orderRec);
            }
        }catch(Exception ex){
            System.debug('NS_Order_ItemController::saveOrderDetails: EXCEPTION: '+ ex.getMessage() + '\n' + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage());
        }
        System.debug('ORDER SAVE JSON: ' + JSON.serialize(orderRec.Order_JSON__c));
        return JSON.serialize(orderRec.Order_JSON__c);
    }
}