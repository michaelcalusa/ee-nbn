@isTest
public class NS_Order_ItemController_Test {
    
    @testSetup
    static void testSetup(){
        User nbnUser1 = SF_TestData.createJitUser('AS00000099', 'Access Seeker nbnSelect Feasibility', 'nbnRsp1');
        Account acc1 = [SELECT Id FROM Account WHERE Access_Seeker_ID__c = 'AS00000099' LIMIT 1];
        DF_Opportunity_Bundle__c oppBundle = SF_TestData.createOpportunityBundle(acc1.Id);
        
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        Test.startTest();

        System.runAs(rspUser1){
            Database.insert(oppBundle);
            DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, oppBundle.Id, null, 'Green');
            Database.insert(dfQuote1);
            DF_Order__c dfOrder1 = SF_TestData.createDFOrder(dfQuote1.Id, oppBundle.Id, 'In Draft');
            Database.insert(dfOrder1);
        }

        Test.stopTest();
    }

    static testMethod void testGetOrderDetails_Positive(){
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        System.runAs(rspUser1){
            DF_Order__c dfO= [SELECT Id, Order_JSON__c, DF_Quote__r.Quote_Name__c FROM DF_Order__c WHERE Order_Status__c = 'In Draft'];
            System.assertEquals(true, String.isBlank(dfO.Order_JSON__c));
            String strResponse  = NS_Order_ItemController.getOrderDetails(String.valueOf(dfO.Id));
            NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJson = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot)JSON.deserialize(strResponse, NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class); 
            System.assertEquals(dfO.DF_Quote__r.Quote_Name__c, orderJson.resourceOrder.associatedReferenceId);
        }
    }

    static testMethod void testGetOrderDetails_Negative(){
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        System.runAs(rspUser1){
            try{
                String strResponse  = NS_Order_ItemController.getOrderDetails('TEST');
            }catch(Exception ex){
                //do nothing, exception expected
            }
        }
    }

    static testMethod void testSaveOrderDetails_Positive(){
        String strOrderJson ='{"resourceOrder":{"id":"ROR000000001425","notes":"test","siteName":"siteTest","siteType":"Education","orderType":"Connect","tradingName":"tradeTest","dateReceived":"2018-09-25T22:16:59Z","heritageSite":"Yes","keysRequired":"test","safetyHazards":"0","additionalNotes":"test","jsasswmRequired":null,"inductionRequired":"Yes","interactionStatus":"InProgress","resourceOrderType":"NBN Select","accessSeekerContact":{"contactName":null,"contactPhone":null},"afterHoursSiteVisit":"Yes","itemInvolvesContact":[{"role":"test","type":"Business","contactName":"test test","phoneNumber":"0423237872","emailAddress":"test@dsc.com","unstructuredAddress":{"postcode":"3003","addressLine1":"test","addressLine2":"","addressLine3":"","localityName":"test","stateTerritoryCode":"SA"}},{"role":"","type":"Site","contactName":"test test","phoneNumber":"0423232424","emailAddress":"test@sdcs.com","unstructuredAddress":{"postcode":"","addressLine1":"","addressLine2":"","addressLine3":"","localityName":"","stateTerritoryCode":""}}],"customerRequiredDate":"2018-11-15","ownerAwareOfProposal":"Yes","securityRequirements":"test","associatedReferenceId":"NSQ-0000000574","plannedCompletionDate":null,"workPracticesRequired":"Yes","siteAccessInstructions":"test","interactionDateComplete":null,"propertyOwnershipStatus":"Leased","resourceOrderComprisedOf":{"itemInvolvesLocation":{"id":"LOC000049831879"},"referencesResourceOrderItem":[{"action":"ADD","itemInvolvesResource":{"type":"NTD","batteryBackupRequired":"Yes"}}]},"confinedSpaceRequirements":"Yes","workingAtHeightsConsideration":"Yes","contractedLocationInstructions":"test"},"notificationType":"OrderAccepted"}';
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        System.runAs(rspUser1){
            DF_Order__c dfO= [SELECT Id, Order_JSON__c, DF_Quote__r.Quote_Name__c, Trading_Name__c FROM DF_Order__c WHERE Order_Status__c = 'In Draft'];
            System.assertEquals(true, String.isBlank(dfO.Order_JSON__c));
            String strResponse  = NS_Order_ItemController.saveOrderDetails(String.valueOf(dfO.Id), strOrderJson);
            DF_Order__c updatedDfO= [SELECT Id, Order_JSON__c, DF_Quote__r.Quote_Name__c, Trading_Name__c FROM DF_Order__c WHERE Id = :dfO.Id];
            NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJson = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot)JSON.deserialize(NS_Order_ItemController.getOrderDetails(String.valueOf(dfO.Id)), NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class); 
            System.assertEquals(updatedDfO.Trading_Name__c, orderJson.resourceOrder.tradingName);
        }
    }

    static testMethod void testSaveOrderDetails_Negative(){
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        System.runAs(rspUser1){
            try{
                String strResponse  = NS_Order_ItemController.saveOrderDetails('TEST', null);
            }catch(Exception ex){
                //do nothing, exception expected
            }
        }
    }

    static testMethod void testNS_OrderDataJSONWrapper(){
        NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot orderJsonRoot = new NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot();
        orderJsonRoot.resourceOrder = new NS_OrderDataJSONWrapper.ResourceOrder();
        orderJsonRoot.notificationType='test';
        orderJsonRoot.resourceOrder.notes ='test';
        orderJsonRoot.resourceOrder.orderType='test';
        orderJsonRoot.resourceOrder.resourceOrderType='test';
        orderJsonRoot.resourceOrder.associatedReferenceId='test';
        orderJsonRoot.resourceOrder.siteName='test';
        orderJsonRoot.resourceOrder.siteType='test';
        orderJsonRoot.resourceOrder.tradingName='test';
        orderJsonRoot.resourceOrder.heritageSite='test';
        orderJsonRoot.resourceOrder.inductionRequired='test';
        orderJsonRoot.resourceOrder.dateReceived=System.Datetime.now();
        orderJsonRoot.resourceOrder.customerRequiredDate='test';
        orderJsonRoot.resourceOrder.afterHoursSiteVisit='test';
        orderJsonRoot.resourceOrder.propertyOwnershipStatus='test';
        orderJsonRoot.resourceOrder.keysRequired='test';
        orderJsonRoot.resourceOrder.securityRequirements='test';
        orderJsonRoot.resourceOrder.siteAccessInstructions='test';
        orderJsonRoot.resourceOrder.contractedLocationInstructions='test';
        orderJsonRoot.resourceOrder.ownerAwareOfProposal='test';
        orderJsonRoot.resourceOrder.safetyHazards='test';
        orderJsonRoot.resourceOrder.jsaswmRequired='test';
        orderJsonRoot.resourceOrder.workPracticesRequired='test';
        orderJsonRoot.resourceOrder.workingAtHeightsConsideration='test';
        orderJsonRoot.resourceOrder.additionalNotes='test';
        orderJsonRoot.resourceOrder.confinedSpaceRequirements='test';
        orderJsonRoot.resourceOrder.id='test';
        orderJsonRoot.resourceOrder.interactionStatus='test';
        orderJsonRoot.resourceOrder.accessSeekerInteraction= new NS_OrderDataJSONWrapper.AccessSeekerInteraction();
        orderJsonRoot.resourceOrder.accessSeekerInteraction.billingAccountID='test';
        orderJsonRoot.resourceOrder.accessSeekerContact = new NS_OrderDataJSONWrapper.AccessSeekerContact();
        orderJsonRoot.resourceOrder.accessSeekerContact.contactName='test';
        orderJsonRoot.resourceOrder.accessSeekerContact.contactPhone='test';
        orderJsonRoot.resourceOrder.itemInvolvesContact = new List<NS_OrderDataJSONWrapper.ItemInvolvesContact>();
        NS_OrderDataJSONWrapper.ItemInvolvesContact itemCon = new NS_OrderDataJSONWrapper.ItemInvolvesContact();
        itemCon.role='test';
        itemCon.type='test';
        itemCon.contactName='test';
        itemCon.phoneNumber='test';
        itemCon.emailAddress='test';
        itemCon.unstructuredAddress = new NS_OrderDataJSONWrapper.UnstructuredAddress();
        itemCon.unstructuredAddress.addressLine1 = 'test';
        itemCon.unstructuredAddress.addressLine2 = 'test';
        itemCon.unstructuredAddress.addressLine3 = 'test';
        itemCon.unstructuredAddress.localityName = 'test';
        itemCon.unstructuredAddress.postcode = 'test';
        itemCon.unstructuredAddress.stateTerritoryCode = 'test';
        orderJsonRoot.resourceOrder.itemInvolvesContact.add(itemCon);
        orderJsonRoot.resourceOrder.resourceOrderComprisedOf = new NS_OrderDataJSONWrapper.ResourceOrderComprisedOf();
        orderJsonRoot.resourceOrder.resourceOrderComprisedOf.itemInvolvesLocation = new NS_OrderDataJSONWrapper.ItemInvolvesLocation();
        orderJsonRoot.resourceOrder.resourceOrderComprisedOf.itemInvolvesLocation.id = 'test';
        orderJsonRoot.resourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem = new List<NS_OrderDataJSONWrapper.ReferencesResourceOrderItem>();
        NS_OrderDataJSONWrapper.ReferencesResourceOrderItem refResOrderItem = new NS_OrderDataJSONWrapper.ReferencesResourceOrderItem();
        refResOrderItem.action = 'test';
        refResOrderItem.itemInvolvesResource = new NS_OrderDataJSONWrapper.ItemInvolvesResource();
        refResOrderItem.itemInvolvesResource.type = 'test';
        refResOrderItem.itemInvolvesResource.batteryBackupRequired = 'test';
        orderJsonRoot.resourceOrder.resourceOrderComprisedOf.referencesResourceOrderItem.add(refResOrderItem);
        String strJson = JSON.serialize(orderJsonRoot);
    }
}