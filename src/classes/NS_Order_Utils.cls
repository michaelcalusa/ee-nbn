public class NS_Order_Utils {
    public static final String ACCESS_SEEKER_ID = 'accessSeekerID';
    public static final String CORRELATION_ID = 'correlationId';
    public static final String TIMESTAMP = 'timestamp';
    public static final String NBN_ENV_OVRD = 'NBN-Environment-Override';
    public static final String PRODUCT_TYPE = 'productName';

    public static final String ERR_ACCESS_SEEKER_ID_NOT_FOUND = 'accessSeekerId for RSP Account was not found.';
    public static final String ERR_CORRELATION_ID_NOT_FOUND = 'correlationId was not found.';
    public static final String ERR_REQUEST_BODY_EMPTY = 'Request body is empty.';
    public static final String ERR_RESP_BODY_MISSING = 'Response Body is missing.';

    public static NS_Order_Event__e createNsOrderEvent(DF_Order__c nsOrder) {

        DF_Order_Settings__mdt ordSetting = getOrderSettings('NSOrderSettings');

        NS_Order_Event__e evt = new NS_Order_Event__e();
        evt.Event_Id__c = ordSetting.OrderCreateCode__c ;//'SF-DF-000002';
        evt.Event_Record_Id__c = nsOrder.DF_Quote__r.Order_GUID__c;//nsOrder.Id;
        evt.Source__c = ordSetting.OrderCreateSource__c ;
        evt.Message_Body__c = nsOrder.Order_JSON__c;//JSON.serialize(dfOrder);
        Database.SaveResult dsr = EventBus.publish(evt);
        System.debug('NS ORDER EVENT PUBLISH STATUS: ' + dsr) ;
        return evt;
    }
    public static NS_Order_Event__e createNsCostVarianceEvent(String strQuoteName, String strOrderGuid, String strAction) {

        DF_Order_Settings__mdt ordSetting = getOrderSettings('NSOrderSettings');

        NS_Order_Event__e evt = new NS_Order_Event__e();
        evt.Event_Id__c = ordSetting.OrderCostVarianceCode__c ;//'SF-DF-000003';
        evt.Event_Record_Id__c = strOrderGuid;//nsOrder.Id;
        evt.Source__c = ordSetting.OrderCreateSource__c ;
        evt.Message_Body__c = createNsCostVariancePayload(strQuoteName, strAction);//JSON.serialize(dfOrder);
        Database.SaveResult dsr = EventBus.publish(evt);
        System.debug('NS ORDER EVENT PUBLISH STATUS: ' + dsr) ;
        return evt;
    }
    public static String createNsCostVariancePayload(String strQuoteName, String strAction) {
        String strCostVarianceJSON;
        NS_CostVarianceJSONWrapper.NS_CostVarianceJSONRoot cvRoot = new NS_CostVarianceJSONWrapper.NS_CostVarianceJSONRoot();
        cvRoot.body.manageResourceOrderRequest.resourceOrder.id = strQuoteName;
        cvRoot.body.manageResourceOrderRequest.resourceOrder.action = strAction;
        strCostVarianceJSON = JSON.serialize(cvRoot);
        return strCostVarianceJSON;
    }
    public static DF_Order_Settings__mdt getOrderSettings(String strDeveloperName) {
        //return (DF_Order_Settings__mdt) SF_CS_API_Util.getSobjectRecords('DF_Order_Settings__mdt', ' WHERE DeveloperName =:'+strDeveloperName)[0];
        return (DF_Order_Settings__mdt) Database.query(SF_CS_API_Util.getQuery(new DF_Order_Settings__mdt(), ' WHERE DeveloperName =:strDeveloperName'))[0];
    }


    public static String populateBillingJSON(DF_Order__c orderRec) {
        try {
            NS_SOAOrderDataJSONWrapper.NS_SOADataJSONRoot SOAJson = new NS_SOAOrderDataJSONWrapper.NS_SOADataJSONRoot();

            String OppId = orderRec.DF_Quote__r.Opportunity__c;
            System.debug('Opp id' + OppId);

            csord__Order_Line_Item__c csorderRec = [Select Id,csord__Order__r.csordtelcoa__Opportunity__c,csord__Total_Price__c from csord__Order_Line_Item__c where csord__Order__r.csordtelcoa__Opportunity__c = :OppId LIMIT 1];

            System.debug('csorderRec' + csorderRec);

            String ordrJson = orderRec.Order_JSON__c;
            NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot mainWrap = (NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot) JSON.deserialize(ordrJson,
                    NS_OrderDataJSONWrapper.NS_OrderDataJSONRoot.class);
            SOAJson.body = new NS_SOAOrderDataJSONWrapper.Body();

            SOAJson.body.ManageBillingEventRequest = new NS_SOAOrderDataJSONWrapper.ManageBillingEventRequest();

            NS_SOAOrderDataJSONWrapper.ProductOrder productOrder = new NS_SOAOrderDataJSONWrapper.ProductOrder();
            SOAJson.body.ManageBillingEventRequest.productOrder = productOrder;

            productOrder.orderType = 'Connect';
            productOrder.SFid = String.valueOf(orderRec.Id);
            productOrder.plannedCompletionDate = null;
            productOrder.customerRequiredDate = null;
            productOrder.afterHoursAppointment = null;
            productOrder.interactionStatus = 'Complete';
            productOrder.id = orderRec.DF_Quote__r.Quote_Name__c;//String.valueOf(orderRec.Id);
            productOrder.afterHoursAppointment = null;
            if (productOrder.interactionStatus == 'Complete') {
                productOrder.interactionDateComplete = Datetime.now().formatGmt('yyyy-MM-dd HH:mm:ss');
            }
            productOrder.accessSeekerInteraction = new NS_SOAOrderDataJSONWrapper.AccessSeekerInteraction();

            productOrder.accessSeekerInteraction.billingAccountID = mainWrap.resourceOrder.accessSeekerInteraction.billingAccountID;

            NS_SOAOrderDataJSONWrapper.ProductOrderComprisedOf productOrderComprisedOf = new NS_SOAOrderDataJSONWrapper.ProductOrderComprisedOf();
            productOrder.productOrderComprisedOf = productOrderComprisedOf;


            productOrderComprisedOf.action = 'ADD';
            productOrderComprisedOf.itemInvolvesLocation = new NS_SOAOrderDataJSONWrapper.ItemInvolvesLocation();
            productOrderComprisedOf.itemInvolvesLocation.id = mainWrap.resourceOrder.resourceOrderComprisedOf.itemInvolvesLocation.id;

            NS_SOAOrderDataJSONWrapper.ItemInvolvesProduct itemInvolvesProduct = new NS_SOAOrderDataJSONWrapper.ItemInvolvesProduct();
            productOrderComprisedOf.itemInvolvesProduct = itemInvolvesProduct;
            itemInvolvesProduct.zone = null;
            itemInvolvesProduct.term = null;
            itemInvolvesProduct.templateId = null;
            itemInvolvesProduct.serviceRestorationSLA = null;
            itemInvolvesProduct.id = orderRec.DF_Quote__r.Quote_Name__c;
            itemInvolvesProduct.accessServiceTechnologyType = 'Fibre';
            itemInvolvesProduct.accessAvailabilityTarget = null;

            productOrderComprisedOf.referencesProductOrderItem = new List<NS_SOAOrderDataJSONWrapper.ReferencesProductOrderItem>();
            NS_SOAOrderDataJSONWrapper.ReferencesProductOrderItem refPrdOrderItem = new NS_SOAOrderDataJSONWrapper.ReferencesProductOrderItem();

            productOrderComprisedOf.referencesProductOrderItem.add(refPrdOrderItem);

            NS_SOAOrderDataJSONWrapper.ProductOrderInvolvesCharges productOrderInvolvesCharges = new NS_SOAOrderDataJSONWrapper.ProductOrderInvolvesCharges();
            SOAJson.body.ManageBillingEventRequest.ProductOrderInvolvesCharges = productOrderInvolvesCharges;
            productOrderInvolvesCharges.OneTimeChargeProdPriceCharge = new List<NS_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge>();
            NS_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge refProdPrice = new NS_SOAOrderDataJSONWrapper.OneTimeChargeProdPriceCharge();
            refProdPrice.type = null;
            refProdPrice.quantity = '1.0';
            refProdPrice.name = 'nbn Select Design and Construction';
            refProdPrice.ID = 'NS_NRC_DC';
            refProdPrice.chargeReferences = orderRec.DF_Quote__r.Quote_Name__c;
            refProdPrice.amount = String.valueOf(csorderRec.csord__Total_Price__c);
            System.debug('refProdPrice.amount' + csorderRec.csord__Total_Price__c);
            productOrderInvolvesCharges.OneTimeChargeProdPriceCharge.add(refProdPrice);


            productOrderInvolvesCharges.ProdPriceCharge = new List<NS_SOAOrderDataJSONWrapper.ProdPriceCharge>();
            NS_SOAOrderDataJSONWrapper.ProdPriceCharge refPrdPrcChg = new NS_SOAOrderDataJSONWrapper.ProdPriceCharge();

            productOrderInvolvesCharges.ProdPriceCharge.add(refPrdPrcChg);


            SOAJson.body.ManageBillingEventRequest.notificationType = 'BillingEvent';
            System.debug('---OrderType' + productOrder.orderType);

            //String soaJsonUpdate = JSON.serialize(SOAJson);
            String soaJsonUpdate = JSON.serialize(SOAJson.body.ManageBillingEventRequest);
            System.debug('---soaJsonUpdate-- ' + soaJsonUpdate);
            orderRec.Product_Charges_JSON__c = soaJsonUpdate;

            System.debug('----createBillingEventRequestStr::' + orderRec.Product_Charges_JSON__c);
            return orderRec.Product_Charges_JSON__c;

        } catch (Exception e) {
            System.debug('Error in addingOrderiNfo to SOA: ' + e.getMessage() + '\n' + e.getStackTraceString());
            throw new AuraHandledException(e.getMessage());
        }
    }
}