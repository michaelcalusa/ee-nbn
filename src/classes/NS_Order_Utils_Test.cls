@isTest
public class NS_Order_Utils_Test {
    @testSetup
    static void testSetup(){
        User nbnUser1 = SF_TestData.createJitUser('AS00000099', 'Access Seeker nbnSelect Feasibility', 'nbnRsp1');
        Account acc1 = [SELECT Id FROM Account WHERE Access_Seeker_ID__c = 'AS00000099' LIMIT 1];
        DF_Opportunity_Bundle__c oppBundle = SF_TestData.createOpportunityBundle(acc1.Id);
        Opportunity opp= SF_TestData.createOpportunity('TestOpportunity');
        opp.accountid=acc1.id;
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        Test.startTest();
        System.runAs(rspUser1){
            Database.insert(oppBundle);
            Database.insert(opp);

            DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', opp.Id, oppBundle.Id, null, 'Green');
            dfQuote1.Order_GUID__c = NS_SF_Utils.generateGUID();
            Database.insert(dfQuote1);
            DF_Order__c dfOrder1 = SF_TestData.createDFOrder(dfQuote1.Id, oppBundle.Id, 'In Draft');
            //dfOrder1.Order_JSON__c = '{"resourceOrder":{"id":"ROR000000001425","notes":"test","siteName":"test","siteType":"Education","orderType":"Connect","tradingName":"test","dateReceived":"2018-09-25T22:16:59Z","heritageSite":"Yes","keysRequired":"test","safetyHazards":"0","additionalNotes":"test","jsasswmRequired":null,"inductionRequired":"Yes","interactionStatus":"Acknowledged","resourceOrderType":"NBN Select","accessSeekerContact":{"contactName":null,"contactPhone":null},"afterHoursSiteVisit":"Yes","itemInvolvesContact":[{"role":"test","type":"Business","contactName":"test test","phoneNumber":"0423237872","emailAddress":"test@dsc.com","unstructuredAddress":{"postcode":"3003","addressLine1":"test","addressLine2":"","addressLine3":"","localityName":"test","stateTerritoryCode":"SA"}},{"role":"","type":"Site","contactName":"test test","phoneNumber":"0423232424","emailAddress":"test@sdcs.com","unstructuredAddress":{"postcode":"","addressLine1":"","addressLine2":"","addressLine3":"","localityName":"","stateTerritoryCode":""}}],"customerRequiredDate":"2018-11-15","ownerAwareOfProposal":"Yes","securityRequirements":"test","associatedReferenceId":"NSQ-0000000574","plannedCompletionDate":null,"workPracticesRequired":"Yes","siteAccessInstructions":"test","interactionDateComplete":null,"propertyOwnershipStatus":"Leased","resourceOrderComprisedOf":{"itemInvolvesLocation":{"id":"LOC000049831879"},"referencesResourceOrderItem":[{"action":"ADD","itemInvolvesResource":{"type":"NTD","batteryBackupRequired":"Yes"}}]},"confinedSpaceRequirements":"Yes","workingAtHeightsConsideration":"Yes","contractedLocationInstructions":"test"},"notificationType":"OrderAccepted"}';
            dfOrder1.Order_JSON__c = '{"resourceOrder":{"workPracticesRequired":"No","workingAtHeightsConsideration":"No","tradingName":"Contigo Ltd","siteType":"Industrial","siteName":"Contigo AU","siteAccessInstructions":"Site Access","securityRequirements":"Get keys","safetyHazards":"No","resourceOrderType":"nbnSelect","resourceOrderComprisedOf":{"referencesResourceOrderItem":[{"itemInvolvesResource":{"type":"NTD","batteryBackupRequired":"Yes"},"action":"ADD"}],"itemInvolvesLocation":{"id":"LOC000066186204"}},"propertyOwnershipStatus":"Owned","ownerAwareOfProposal":"Yes","orderType":"Connect","notes":"Additional","keysRequired":"At main gate","jsaswmRequired":"Yes","itemInvolvesContact":[{"unstructuredAddress":{"stateTerritoryCode":"VIC","postcode":"3008","localityName":"Docklands","addressLine3":"","addressLine2":"","addressLine1":"380 Docklands Dr"},"type":"Business","role":"COO","phoneNumber":"0404040511","emailAddress":"business@contigo.com","contactName":"Business Cowman","company":null},{"unstructuredAddress":{"stateTerritoryCode":"","postcode":"","localityName":"","addressLine3":"","addressLine2":"","addressLine1":""},"type":"Site","role":"","phoneNumber":"0404040511","emailAddress":"site@quantuso.com","contactName":"Site Details","company":null}],"interactionStatus":null,"inductionRequired":"Yes","id":"ROR000000001425","heritageSite":"Yes","dateReceived":"2018-10-03T04:41:55.182Z","customerRequiredDate":"2018-12-25","contractedLocationInstructions":"Contract","confinedSpaceRequirements":"No","associatedReferenceId":"NSQ-0000000610","afterHoursSiteVisit":"No","additionalNotes":"Installation","accessSeekerInteraction":{"billingAccountID":"BAN1234567"},"accessSeekerContact":{"contactPhone":null,"contactName":null}},"notificationType":null}';
            Database.insert(dfOrder1);

            csord__Order_Request__c csOrderReq = SF_TestData.buildOrderRequest();
            Database.insert(csOrderReq);

            csord__Order__c csOrder = SF_TestData.buildOrder('TestCsOrder', acc1.id, opp.Id, 'ConstructionComplete', csOrderReq.Id);
            Database.insert(csOrder);

            csord__Order_Line_Item__c csOrdLineItem = SF_TestData.buildOrderRequestLineItem('TestOrderLineItem', csOrder.Id);
            Database.insert(csOrdLineItem);
        }
        Test.stopTest();
    }
    
    static testMethod void testSendNSOrderDetailstoSOA(){
        DF_Order__c orderRec = [SELECT Id,Order_Id__c,DF_Quote__c,Order_Status__c,Product_Charges_JSON__c,BPI_Id__c, Order_JSON__c, DF_Quote__r.Quote_Name__c, 
                                Site_Survey_Charges_JSON__c,NBN_Commitment_Date__c, UNI_Port__c, Order_Sub_Status__c, DF_Quote__r.Order_GUID__c,
                                Appian_Notification_Date__c, DF_Quote__r.Opportunity__c, Service_Region__c, DF_Quote__r.GUID__c, Order_Notifier_Response_JSON__c 
                                FROM DF_Order__c LIMIT 1];
        Test.startTest();
        String str = NS_Order_Utils.populateBillingJSON(orderRec);
        NS_SOAOrderDataJSONWrapper.ManageBillingEventRequest manageBillingEventRequest = (NS_SOAOrderDataJSONWrapper.ManageBillingEventRequest) JSON.deserialize(str, NS_SOAOrderDataJSONWrapper.ManageBillingEventRequest.class);
        Assert.equals(orderRec.DF_Quote__r.Quote_Name__c, manageBillingEventRequest.productOrder.productOrderComprisedOf.itemInvolvesProduct.id);
        Test.stopTest();
    }
    static testMethod void testGetOrderSettings(){
        Test.startTest();
        	DF_Order_Settings__mdt ordSetting = NS_Order_Utils.getOrderSettings('NSOrderSettings');
        Test.stopTest();
        
        Assert.equals('ORDACK101', ordSetting.OrderAckEventCode__c);
        Assert.equals('ORDACC101', ordSetting.OrderAccEventCode__c);
        Assert.equals('ORDSCHED101', ordSetting.OrderSchEventCode__c);
        Assert.equals('RSPACTREQRD101', ordSetting.OrderRspActionReqdEventCode__c);
        Assert.equals('INFOREQREMINDER101', ordSetting.OrderInfoReqReminderEventCode__c);
        Assert.equals('RSPACTIONCOMP101', ordSetting.OrderRspActionCompEventCode__c);
        Assert.equals('ORDCANCELLED101', ordSetting.OrderCancelledEventCode__c);
        Assert.equals('CONSTART101', ordSetting.OrderConstructionCode__c);
        Assert.equals('APPTSCHED101', ordSetting.OrderNTDInstallationDateUpdateEventCode__c);
        Assert.equals('CONCOMP101', ordSetting.OrderConstCompleteEventCode__c);
    }
    static testMethod void testCreateNsOrderEvent(){
        DF_Order__c dfOrd = [SELECT Id, DF_Quote__r.Order_GUID__c, Order_Json__c FROM DF_Order__c LIMIT 1];
        Test.startTest();
        	NS_Order_Event__e orderEvt = NS_Order_Utils.createNsOrderEvent(dfOrd);
        Test.stopTest();
        System.assertEquals(dfOrd.DF_Quote__r.Order_GUID__c, orderEvt.Event_Record_Id__c);
    }
    
    static testMethod void testNS_SOAOrderDataJSONWrapper(){
        
        string soaJSON1 = '{"ProductOrderInvolvesCharges":{"ProdPriceCharge":[{"type":null,"quantity":null,"name":null,"ID":null,"chargeReferences":null,"amount":null}],"OneTimeChargeProdPriceCharge":[{"type":null,"quantity":"1.0","name":"nbn Select Design & Construction","ID":"NS_NRC_DC","chargeReferences":"NSQ-0000000545","amount":"111.00"}]},"productOrder":{"SFid":null,"productOrderComprisedOf":{"referencesProductOrderItem":[{"referencesProductOrderItem":null,"itemInvolvesProduct":null,"action":null}],"itemInvolvesProduct":{"zone":null,"term":null,"templateId":null,"serviceRestorationSLA":null,"QuoteId":null,"id":"NSQ-0000000545","accessServiceTechnologyType":"Fibre","accessAvailabilityTarget":null},"itemInvolvesLocation":{"id":"LOC000066186204"},"action":"ADD"},"plannedCompletionDate":null,"orderType":"Connect","interactionStatus":"Complete","interactionDateComplete":"2018-10-04 00:06:55","id":"a7Ip00000003WBt","customerRequiredDate":null,"afterHoursAppointment":null,"accessSeekerInteraction":{"billingAccountID":"BAN000000004489"}},"notificationType":"BillingEvent"}' ;       NS_SOAOrderDataJSONWrapper.NS_SOADataJSONRoot SOAJson = new NS_SOAOrderDataJSONWrapper.NS_SOADataJSONRoot();
       
        
        Test.startTest();
        	NS_SOAOrderDataJSONWrapper json = NS_SOAOrderDataJSONWrapper.parse(soaJSON1);
        Test.stopTest();       
    }
}