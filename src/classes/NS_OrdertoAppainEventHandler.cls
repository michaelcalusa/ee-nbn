public with sharing class NS_OrdertoAppainEventHandler implements Queueable{
    
    private List<NS_Order_Event__e> lstToProcess;
    
    public NS_OrdertoAppainEventHandler(List<NS_Order_Event__e> evtLst) {      
        lstToProcess = evtLst;
    }
    
    public void execute(QueueableContext context) {
        
        //NS_Order_Event__e customSettingValues = Business_Platform_Events__c.getOrgDefaults();      
        DF_Order_Settings__mdt ordSetting = NS_Order_Utils.getOrderSettings('NSOrderSettings');
        
        String evtBody;
        for(NS_Order_Event__e evt : lstToProcess){
            if(evt.Event_Id__c.equalsIgnoreCase(ordSetting.OrderCreateCode__c)
               || evt.Event_Id__c.equalsIgnoreCase(ordSetting.OrderCostVarianceCode__c)) {
                   system.debug('Salesforce to Appain event received for the Event ID '+evt.Event_Id__c);
                   evtBody = '{"eventIdentifier":"'+evt.Event_Id__c+'","externalSystemCorrelationId":"'+evt.Event_Record_Id__c+'","eventBody":'+evt.Message_Body__c+'}';
                   AppianCallout.SendInformationToAppian(evtBody, evt.Event_Id__c+';'+evt.Event_Record_Id__c ,null);                 
               }
        }
    }
}