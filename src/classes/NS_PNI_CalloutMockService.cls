@isTest
public class NS_PNI_CalloutMockService implements WebServiceMock{
    private String strStatus;
    private String strLocId;
    public NS_PNI_CalloutMockService(String strCalloutStatus, String strLocactioId){
        this.strStatus = strCalloutStatus;
        this.strLocId = strLocactioId;
    }
    public void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
            
            NS_PNI_QueryService.queryResponse_element pniResponse = new NS_PNI_QueryService.queryResponse_element();
            pniResponse.queryResultList = new NS_PNI_QueryService.queryResultList_element();
            pniResponse.queryResultList.queryResult = new List<NS_PNI_QueryService.queryResult_element>();
            NS_PNI_QueryService.queryResult_element resElement = new NS_PNI_QueryService.queryResult_element();
            NS_PNI_QueryService.describedby_element descElement = new NS_PNI_QueryService.describedby_element();
            descElement.characteristic = new NS_PNI_QueryService.characteristic_element();
            resElement.describedby =  new List<NS_PNI_QueryService.describedby_element>();
            resElement.Id = strLocId;
            
            if(strStatus.equalsIgnoreCase('FAILURE')){
                NS_PNI_QueryService.exception_element exceptionElement= new NS_PNI_QueryService.exception_element();
                exceptionElement.exceptionType = 'OLT_ERROR';
                exceptionElement.exceptionCode = 'PNI_001';
                exceptionElement.exceptionDescription = 'OLT_DOES_NOT_EXIST';
                pniResponse.exception_x = exceptionElement;
            }else{
                descElement.characteristic.ID = 'AssetStatus';
                descElement.value = 'INSERVICE';
                resElement.describedby.add(descElement);
                
                descElement = new NS_PNI_QueryService.describedby_element();
                descElement.characteristic = new NS_PNI_QueryService.characteristic_element();
                descElement.characteristic.ID = 'AssetId';
                descElement.value='3CBR-25-01-BJL-002';
                resElement.describedby.add(descElement);
                
                descElement = new NS_PNI_QueryService.describedby_element();
                descElement.characteristic = new NS_PNI_QueryService.characteristic_element();
                descElement.characteristic.ID = 'AssetOwner';
                descElement.value='NBNCO';
                resElement.describedby.add(descElement);
                
                descElement = new NS_PNI_QueryService.describedby_element();
                descElement.characteristic = new NS_PNI_QueryService.characteristic_element();
                descElement.characteristic.ID = 'AssetDistance';
                descElement.value='284';
                resElement.describedby.add(descElement);
                
                descElement = new NS_PNI_QueryService.describedby_element();
                descElement.characteristic = new NS_PNI_QueryService.characteristic_element();
                descElement.characteristic.ID = 'AssetLat';
                descElement.value='-37.7327605711';
                resElement.describedby.add(descElement);
                
                descElement = new NS_PNI_QueryService.describedby_element();
                descElement.characteristic = new NS_PNI_QueryService.characteristic_element();
                descElement.characteristic.ID = 'AssetLong';
                descElement.value='144.983770025';
                resElement.describedby.add(descElement);
                
                descElement = new NS_PNI_QueryService.describedby_element();
                descElement.characteristic = new NS_PNI_QueryService.characteristic_element();
                descElement.characteristic.ID = 'OLT_Exists';
                descElement.value='True';
                resElement.describedby.add(descElement);
                
                descElement = new NS_PNI_QueryService.describedby_element();
                descElement.characteristic = new NS_PNI_QueryService.characteristic_element();
                descElement.characteristic.ID = 'AssetTypeCode';
                descElement.value='TYCO_OFDC_A4';
                resElement.describedby.add(descElement);
                
                descElement = new NS_PNI_QueryService.describedby_element();
                descElement.characteristic = new NS_PNI_QueryService.characteristic_element();
                descElement.characteristic.ID = 'OLT_ID';
                descElement.value='3CBR-00-00-OLT-0001';
                resElement.describedby.add(descElement);
                }
            pniResponse.queryResultList.queryResult.add(resElement);

            response.put('response_x', pniResponse);
        }
}