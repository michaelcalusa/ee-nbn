/************************************************************************************
Version                 : 1.0 
Description/Function    : invoke asynchronous callouts to PNI, update the response on SFR records   
* Developer                   Date                   Description
* -------------------------------------------------------------------------                
REJEESH RAGHAVAN             26-06-2018              Created.
REJEESH RAGHAVAN             08-08-2018              Updated to incorporate sending multiple loacations to PNI.
************************************************************************************/
public class NS_PNI_SendLocationCalloutQueueHandler implements Queueable, Database.AllowsCallouts{
    private List<DF_SF_Request__c> lstSfrToCallout;
    public static final Integer MAX_PNI_NO_PARALLELS = String.isBlank(SF_CS_API_Util.getCustomSettingValue('PNI_PARALLEL_QUEUE_SIZE'))?1:Integer.valueOf(SF_CS_API_Util.getCustomSettingValue('PNI_PARALLEL_QUEUE_SIZE'));
    
    public NS_PNI_SendLocationCalloutQueueHandler(List<DF_SF_Request__c> lstServiceFeasiblityRequest){
        this.lstSfrToCallout = new List<DF_SF_Request__c>(lstServiceFeasiblityRequest);
    }
    public void execute(QueueableContext qc){
        try{
            List<List<DF_SF_request__c>> lstSplitSFR = new List<List<DF_SF_request__c>>(NS_PNI_Util.spliceSFRBy(this.lstSfrToCallout,MAX_PNI_NO_PARALLELS));
            List<Set<Id>> lstSetSfrId = new List<Set<Id>>();
            Set<Id> setSfrId;
            for(List<DF_SF_Request__c> lstSFR: lstSplitSFR){
                setSfrId= new Set<Id>();
                for(DF_SF_Request__c sfrRec:lstSFR){
                    setSfrId.add(sfrRec.Id);
                }
                lstSetSfrId.add(setSfrId);
            }
            for(Set<Id> setSfrIds: lstSetSfrId){
                //do PNI Callout
                makePNICallout(setSfrIds);
            }
        }catch (Exception ex) {
            System.debug('NS_PNI_SendLocationCalloutQueueHandler::execute EXCEPTION: '+ ex.getMessage() + '\n' +ex.getStackTraceString());
            throw new CustomException(ex.getMessage());
        }
    }
    
    @future
    (callout=true)
    public static void makePNICallout(Set<Id> setSfrId){
        String oppBundleName;
        List<DF_SF_request__c> lstSfr = new List<DF_SF_Request__c>(), lstSfrToUpdate = new List<DF_SF_Request__c>();
        //List<DF_SF_request__c> lstSfrToUpdate = new List<DF_SF_Request__c>();
        SF_ServiceFeasibilityResponse sfrJSRec;
        
        NS_PNI_QueryService pniCalloutObj = new NS_PNI_QueryService ();
        NS_PNI_QueryService.queryServicePort queryServiceObj = new NS_PNI_QueryService.queryServicePort();
        queryServiceObj.timeout_x = 120000;
        NS_PNI_QueryService.queryInputList_element lstPniInput = new NS_PNI_QueryService.queryInputList_element();
        NS_PNI_QueryService.queryInput_element pniInputRec;
        /*QueryOptions Begin*/
        String strQueryOptions = SF_CS_API_Util.getCustomSettingValue('PNI_QUERY_OPTIONS');
        NS_PNI_QueryService.queryOptions_element pniQueryOptionsRec = new NS_PNI_QueryService.queryOptions_element();
        if(String.isNotBlank(strQueryOptions)){
            pniQueryOptionsRec.describedby = new List<NS_PNI_QueryService.describedby_element>();
            NS_PNI_QueryService.describedby_element descOptions;// = new NS_PNI_QueryService.describedby_element();
            if(strQueryOptions.containsIgnoreCase(';')){
                for(String strJointDetails: strQueryOptions.split(';')){
                    descOptions = new NS_PNI_QueryService.describedby_element();
                    descOptions.characteristic = new NS_PNI_QueryService.characteristic_element();
                    descOptions.value=strJointDetails.split(',')[1];
                    descOptions.characteristic.ID = strJointDetails.split(',')[0];
                    pniQueryOptionsRec.describedby.add(descOptions);
                }
            }else{
                descOptions.value=strQueryOptions.split(',')[0];
                descOptions.characteristic.ID = strQueryOptions.split(',')[1];
                pniQueryOptionsRec.describedby.add(descOptions);
            }
        }
        /*QueryOptions End*/
        NS_PNI_QueryService.queryResponse_element pniResponse = new NS_PNI_QueryService.queryResponse_element();
        lstPniInput.queryInput = new List<NS_PNI_QueryService.queryInput_element>();
        String strEnvironmentType = SF_CS_API_Util.getCustomSettingValue('PNI_ENVIRONMENT_TYPE');
        if(String.isNotBlank(strEnvironmentType)){
            queryServiceObj.inputHttpHeaders_x = new Map<String,String>();
            queryServiceObj.inputHttpHeaders_x.put('NBN-Environment-Override',strEnvironmentType);
        }
        
        try{
            //lstSfr= Database.query(SF_CS_API_Util.getQuery(new DF_SF_Request__c(), ' WHERE Id IN :setSfrId'));
            //we cannot use the above method as we need Opportunity_Bundle_Name__c
            lstSfr= [SELECT Id, 
                            Name, 
                            Location_Id__c, 
                            Opportunity_Bundle__r.Opportunity_Bundle_Name__c, 
                            Response__c
                            FROM DF_SF_Request__c 
                            WHERE Id IN :setSfrId];
            //loop through SFR records, create reuest for PNI
            for(DF_SF_Request__c sfrRec: lstSfr){
                oppBundleName = sfrRec.Opportunity_Bundle__r.Opportunity_Bundle_Name__c;
                if(String.isNotBlank(sfrRec.Location_Id__c)){
                    pniInputRec = new NS_PNI_QueryService.queryInput_element();
                    pniInputRec.type_x='Location';
                    //pniInput.subtype=;
                    pniInputRec.id=sfrRec.Location_Id__c;
                    //pniInput.systemId=;
                }
                lstPniInput.queryInput.add(pniInputRec);
            }
            //callout PNI
            pniResponse = queryServiceObj.getNearestAsset(oppBundleName,pniQueryOptionsRec,lstPniInput);
            //upadte PNI response
            lstSfrToUpdate.addAll(NS_PNI_Util.updatePniResponse(pniResponse,lstSfr));
            Database.update(lstSfrToUpdate);
        }catch(Exception ex){
            System.debug('NS_PNI_SendLocationCalloutQueueHandler::makePNICallout EXCEPTION: '+ ex.getMessage() + '\n' +ex.getStackTraceString());
            
            for(DF_SF_Request__c sfrRec: lstSfr){
                sfrJSRec = (SF_ServiceFeasibilityResponse)System.JSON.deserialize(sfrRec.Response__c, SF_ServiceFeasibilityResponse.class);
                sfrJSRec.pniException =  ex.getTypeName();
                sfrJSRec.Status = SF_LAPI_APIServiceUtils.STATUS_VALID;
                sfrRec.Response__c=JSON.serialize(sfrJSRec);
                sfrRec.Status__c = SF_LAPI_APIServiceUtils.STATUS_ERROR;
            }
            Database.update(lstSfr);
            //throw new CustomException(ex.getMessage());
        }
    }
}