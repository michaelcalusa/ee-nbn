@isTest
public class NS_PNI_SendLocationCalloutQueue_Test {
    static testMethod void testPniCallout_Success(){
        //Setup data
        String locationId = 'LOC000035375038';
        String response = '{"data":[{"type":"location","id":"LOC000096895622","attributes":{"rolloutRegionIdentifier":"3CBR-25","premises":{"disconnectionDate":null,"readyForServiceDate":"2018-03-01","serviceClass":"30","isServiceable":true,"isFrustrated":false,"serviceType":"Brownfields FTTC-Copper","primaryTechnology":"Copper","technologyType":"Copper","csaId":"CSA200000010503","serviceClassDescription":"Planned to be served by FTTC (as yet not serviceable)","serviceClassReason":null,"technologyOverride":"Undefined","premisesFlag":true,"disconnectionType":null,"obsolete":false,"marketable":true,"gnafPersistentId":"GAVIC419821282","originalGnafPersistentId":"GAVIC419821282","serviceableLocationType":"N","locationConnectionStatus":null,"locationStatus":"ACTIVE","locationName":null,"locationDescription":null,"locationType":"SITE","serviceLevelRegion":"URBAN","buildingType":null,"propertyType":"SDU1","asaId":null,"fsaId":"3WDT","mpsId":null,"csaName":"Thornbury 2 CSA","samId":"3CBR-25","daid":"3CBR-25-01","infillFlag":false,"tlsInfillBoundary":null,"infillRegionReadyFor":null,"serviceContinuityRegion":null,"polygonId":null,"parcelId":null,"priId":null,"mostrecentconnectionDate":null,"poiId":"3TNB","poiName":"Thornbury","transitionalPoiId":null,"transitionalPoiName":null,"transitionalCsaId":null,"transitionalCsaName":null,"landUse":"RESIDENTIAL","landUseSubclass":null,"newDevelopmentsChargeApplies":null,"firstConnectionDate":null,"firstDisconnectionDate":null,"mostRecentDisconnectionDate":null,"accessTechnologyType":"FTTN","accessSeekerId":null,"isComplexPremise":null,"orderId":null,"orderStatus":null,"orderDateReceived":null,"bandwidthProfile":null,"priorityAssist":null,"deliveryPointIdentifier":null,"isDeleted":"N","sourceSystemC":"UDS","sourceContributor":"PNI","secondaryComplexName":null,"complexRoadNumber1":null,"complexRoadNumber2":null,"complexRoadName":null,"complexRoadTypeCode":null,"complexRoadSuffixCode":null,"complexAddressString":null,"listing_type":"PFL"},"geopoint":{"latitude":-37.735267,"longitude":144.983216}},"relationships":[{"address":{"data":[{"type":"address","id":"LOC000096895622"}]}}]}],"included":[{"type":"address","id":"LOC000096895622","attributes":{"meta":{"relevanceScore":1},"principal":{"fullText":"60 ELIZABETH ST COBURG NORTH VIC 3058 Australia","levelType":null,"levelNumber":null,"unitType":null,"unitNumber":null,"roadNumber1":"60","roadNumber2":null,"roadName":"ELIZABETH","roadTypeCode":"ST","locality":"COBURG NORTH","postcode":"3058","state":"VIC"},"geopoint":{"latitude":-37.735267,"longitude":144.983216},"aliases":[{"fullText":"60 ELIZABETH ST RESERVOIR VIC 3058 Australia","levelType":null,"levelNumber":null,"unitType":null,"unitNumber":null,"roadNumber1":"60","roadNumber2":null,"roadName":"ELIZABETH","roadTypeCode":"ST","locality":"RESERVOIR","postcode":"3058","state":"VIC"},{"fullText":"60 ELIZABETH ST PRESTON VIC 3058 Australia","levelType":null,"levelNumber":null,"unitType":null,"unitNumber":null,"roadNumber1":"60","roadNumber2":null,"roadName":"ELIZABETH","roadTypeCode":"ST","locality":"PRESTON","postcode":"3058","state":"VIC"},{"fullText":"60 ELIZABETH ST FAWKNER VIC 3058 Australia","levelType":null,"levelNumber":null,"unitType":null,"unitNumber":null,"roadNumber1":"60","roadNumber2":null,"roadName":"ELIZABETH","roadTypeCode":"ST","locality":"FAWKNER","postcode":"3058","state":"VIC"},{"fullText":"60 ELIZABETH ST COBURG VIC 3058 Australia","levelType":null,"levelNumber":null,"unitType":null,"unitNumber":null,"roadNumber1":"60","roadNumber2":null,"roadName":"ELIZABETH","roadTypeCode":"ST","locality":"COBURG","postcode":"3058","state":"VIC"},{"fullText":"60 ELIZABETH ST PASCOE VALE VIC 3058 Australia","levelType":null,"levelNumber":null,"unitType":null,"unitNumber":null,"roadNumber1":"60","roadNumber2":null,"roadName":"ELIZABETH","roadTypeCode":"ST","locality":"PASCOE VALE","postcode":"3058","state":"VIC"}]}}],"meta":{"totalMatchedResources":1}}';
        NS_Custom_Options__c cs1 = new NS_Custom_Options__c(name = 'PNI_PARALLEL_QUEUE_SIZE', Value__c = '1');
        NS_Custom_Options__c cs2 = new NS_Custom_Options__c(name = 'PNI_ENVIRONMENT_TYPE', Value__c = 'DEV');
        NS_Custom_Options__c cs3 = new NS_Custom_Options__c(name = 'PNI_VALID_TECHNOLOGIES', Value__c = 'FTTN,FTTB,FTTC');
        List<NS_Custom_Options__c> lstCustSettings = new List<NS_Custom_Options__c>{cs1,cs2,cs3};
        Database.insert(lstCustSettings);
        
         //Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        //Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        //Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, SF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);        
        sfReq.Product_Type__c = 'NBN_SELECT';
        insert sfReq;
        
        Test.setMock(WebServiceMock.class, new NS_PNI_CalloutMockService('SUCCESS',locationId));
        
        Test.startTest();
            System.enqueueJob(new NS_PNI_SendLocationCalloutQueueHandler(new List<DF_SF_Request__c>{sfReq}));
        Test.stopTest();
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id];
                                            //AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_COMPLETED];
        system.Assert(sfReqList.size() > 0);
    }
    static testMethod void testPniCallout_Failure(){
        //Setup data
        String locationId = 'LOC000035375038';
        String response = '{"data":[{"type":"location","id":"LOC000096895622","attributes":{"rolloutRegionIdentifier":"3CBR-25","premises":{"disconnectionDate":null,"readyForServiceDate":"2018-03-01","serviceClass":"30","isServiceable":true,"isFrustrated":false,"serviceType":"Brownfields FTTC-Copper","primaryTechnology":"Copper","technologyType":"Copper","csaId":"CSA200000010503","serviceClassDescription":"Planned to be served by FTTC (as yet not serviceable)","serviceClassReason":null,"technologyOverride":"Undefined","premisesFlag":true,"disconnectionType":null,"obsolete":false,"marketable":true,"gnafPersistentId":"GAVIC419821282","originalGnafPersistentId":"GAVIC419821282","serviceableLocationType":"N","locationConnectionStatus":null,"locationStatus":"ACTIVE","locationName":null,"locationDescription":null,"locationType":"SITE","serviceLevelRegion":"URBAN","buildingType":null,"propertyType":"SDU1","asaId":null,"fsaId":"3WDT","mpsId":null,"csaName":"Thornbury 2 CSA","samId":"3CBR-25","daid":"3CBR-25-01","infillFlag":false,"tlsInfillBoundary":null,"infillRegionReadyFor":null,"serviceContinuityRegion":null,"polygonId":null,"parcelId":null,"priId":null,"mostrecentconnectionDate":null,"poiId":"3TNB","poiName":"Thornbury","transitionalPoiId":null,"transitionalPoiName":null,"transitionalCsaId":null,"transitionalCsaName":null,"landUse":"RESIDENTIAL","landUseSubclass":null,"newDevelopmentsChargeApplies":null,"firstConnectionDate":null,"firstDisconnectionDate":null,"mostRecentDisconnectionDate":null,"accessTechnologyType":"FTTN","accessSeekerId":null,"isComplexPremise":null,"orderId":null,"orderStatus":null,"orderDateReceived":null,"bandwidthProfile":null,"priorityAssist":null,"deliveryPointIdentifier":null,"isDeleted":"N","sourceSystemC":"UDS","sourceContributor":"PNI","secondaryComplexName":null,"complexRoadNumber1":null,"complexRoadNumber2":null,"complexRoadName":null,"complexRoadTypeCode":null,"complexRoadSuffixCode":null,"complexAddressString":null,"listing_type":"PFL"},"geopoint":{"latitude":-37.735267,"longitude":144.983216}},"relationships":[{"address":{"data":[{"type":"address","id":"LOC000096895622"}]}}]}],"included":[{"type":"address","id":"LOC000096895622","attributes":{"meta":{"relevanceScore":1},"principal":{"fullText":"60 ELIZABETH ST COBURG NORTH VIC 3058 Australia","levelType":null,"levelNumber":null,"unitType":null,"unitNumber":null,"roadNumber1":"60","roadNumber2":null,"roadName":"ELIZABETH","roadTypeCode":"ST","locality":"COBURG NORTH","postcode":"3058","state":"VIC"},"geopoint":{"latitude":-37.735267,"longitude":144.983216},"aliases":[{"fullText":"60 ELIZABETH ST RESERVOIR VIC 3058 Australia","levelType":null,"levelNumber":null,"unitType":null,"unitNumber":null,"roadNumber1":"60","roadNumber2":null,"roadName":"ELIZABETH","roadTypeCode":"ST","locality":"RESERVOIR","postcode":"3058","state":"VIC"},{"fullText":"60 ELIZABETH ST PRESTON VIC 3058 Australia","levelType":null,"levelNumber":null,"unitType":null,"unitNumber":null,"roadNumber1":"60","roadNumber2":null,"roadName":"ELIZABETH","roadTypeCode":"ST","locality":"PRESTON","postcode":"3058","state":"VIC"},{"fullText":"60 ELIZABETH ST FAWKNER VIC 3058 Australia","levelType":null,"levelNumber":null,"unitType":null,"unitNumber":null,"roadNumber1":"60","roadNumber2":null,"roadName":"ELIZABETH","roadTypeCode":"ST","locality":"FAWKNER","postcode":"3058","state":"VIC"},{"fullText":"60 ELIZABETH ST COBURG VIC 3058 Australia","levelType":null,"levelNumber":null,"unitType":null,"unitNumber":null,"roadNumber1":"60","roadNumber2":null,"roadName":"ELIZABETH","roadTypeCode":"ST","locality":"COBURG","postcode":"3058","state":"VIC"},{"fullText":"60 ELIZABETH ST PASCOE VALE VIC 3058 Australia","levelType":null,"levelNumber":null,"unitType":null,"unitNumber":null,"roadNumber1":"60","roadNumber2":null,"roadName":"ELIZABETH","roadTypeCode":"ST","locality":"PASCOE VALE","postcode":"3058","state":"VIC"}]}}],"meta":{"totalMatchedResources":1}}';
        NS_Custom_Options__c cs1 = new NS_Custom_Options__c(name = 'PNI_PARALLEL_QUEUE_SIZE', Value__c = '1');
        NS_Custom_Options__c cs2 = new NS_Custom_Options__c(name = 'PNI_ENVIRONMENT_TYPE', Value__c = 'DEV');
        NS_Custom_Options__c cs3 = new NS_Custom_Options__c(name = 'PNI_VALID_TECHNOLOGIES', Value__c = 'FTTN,FTTB,FTTC');
        List<NS_Custom_Options__c> lstCustSettings = new List<NS_Custom_Options__c>{cs1,cs2,cs3};
        Database.insert(lstCustSettings);
        
         //Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        //Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        //Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, SF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);        
        sfReq.Product_Type__c = 'NBN_SELECT';
        insert sfReq;
        
        Test.setMock(WebServiceMock.class, new NS_PNI_CalloutMockService('FAILURE', locationId));
        
        Test.startTest();
            System.enqueueJob(new NS_PNI_SendLocationCalloutQueueHandler(new List<DF_SF_Request__c>{sfReq}));
        Test.stopTest();
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id];
                                            //AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_COMPLETED];
        system.Assert(sfReqList.size() > 0);
    }
}