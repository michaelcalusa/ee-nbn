/************************************************************************************
Version                 : 1.0 
Description/Function    : Mark SFR records as processed, and Enqueue callout to PNI   
* Developer                   Date                   Description
* -------------------------------------------------------------------------                
REJEESH RAGHAVAN             25-06-2018              Created.
************************************************************************************/
public class NS_PNI_SendLocationQueueHandler implements Queueable, Database.AllowsCallouts{
    private String strOppBundleId;
    private Set<Id> setSFRIds;
    private List<DF_SF_Request__c> lstSFRtoProcess;
    public static final Integer MAX_NO_PARALLELS = 50;
    public NS_PNI_SendLocationQueueHandler(List<DF_SF_Request__c> lstServiceFeasibilityRequests){
        this.lstSFRtoProcess = new List<DF_SF_Request__c>(lstServiceFeasibilityRequests);
    }
    public void execute(QueueableContext queCont){
         try {
            system.debug('lstSFRtoProcess size: ' + lstSFRtoProcess.size());
            //mark SFR records as processed
            if (!lstSFRtoProcess.isEmpty()) {
                for(DF_SF_Request__c sfrRec: lstSFRtoProcess){
                    sfrRec.Status__c = SF_LAPI_APIServiceUtils.STATUS_PROCESSED;
                }
                Database.update(lstSFRtoProcess);
                //enqueue callout to PNI
                makePNICallout(lstSFRtoProcess);
            }
        }catch (Exception ex) {
        	System.debug('NS_PNI_SendLocationQueueHandler::execute EXCEPTION: '+ ex.getMessage() + '\n' +ex.getStackTraceString());
            throw new CustomException(ex.getMessage());
        }
    }
    
    @testVisible
    private static void makePNICallout(List<DF_SF_Request__c> lstSfrToCallout) {
        if(!Test.isRunningTest()){
            System.enqueueJob(new NS_PNI_SendLocationCalloutQueueHandler(lstSfrToCallout));
        }
    }
}