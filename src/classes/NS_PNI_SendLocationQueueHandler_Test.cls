@isTest
public class NS_PNI_SendLocationQueueHandler_Test {
    static testMethod void testNS_PNI_SendLocationQueueHandler(){
        String locationId = 'LOC000035375038';
         //Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        //Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        //Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, SF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, '', addressMap);        
        sfReq.Product_Type__c = 'NBN_SELECT';
        insert sfReq;
 
        test.startTest();
        	System.enqueueJob(new NS_PNI_SendLocationQueueHandler(new List<DF_SF_Request__c>{sfReq}));
        test.stopTest();
        
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id//];
                                            AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_PROCESSED];
        system.Assert(sfReqList.size() > 0);
    }
}