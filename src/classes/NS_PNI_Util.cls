/************************************************************************************
Version                 : 1.0 
Description/Function    : Utility methods for PNI callout   
* Developer                   Date                   Description
* -------------------------------------------------------------------------                
REJEESH RAGHAVAN             26-06-2018              Created.
REJEESH RAGHAVAN             10-08-2018              Updated to incorporate receiving multiple loacations response from PNI.
************************************************************************************/
public class NS_PNI_Util {
    public static List<DF_SF_Request__c> filterSfrList(List<DF_SF_Request__c> lstAllSFR){
        List<DF_SF_Request__c> returnList = new List<DF_SF_Request__c>();
        for(DF_SF_Request__c sfrRec : lstAllSFR){
            
            if(filterSfrList(sfrRec)){
                   returnList.add(sfrRec);
            }
        }
        return returnList;
    }
    public static Boolean filterSfrList(DF_SF_Request__c sfrRec){
        Boolean isValidPniSfr = false;
        String strValidSiteStatus = SF_CS_API_Util.getCustomSettingValue('SF_SITESTATUS');
        Set<String> setValidTech = new Set<String>(SF_CS_API_Util.getCustomSettingValue('PNI_VALID_TECHNOLOGIES').toUpperCase().split(','));
        SF_ServiceFeasibilityResponse sfrJSRec;
        sfrJSRec = (SF_ServiceFeasibilityResponse)System.JSON.deserialize(sfrRec.Response__c, SF_ServiceFeasibilityResponse.class);
        if(sfrJSRec.derivedTechnology!= null
            && sfrJSRec.Status != null
            && strValidSiteStatus.equalsIgnoreCase(sfrJSRec.Status)
            && setValidTech.contains(sfrJSRec.derivedTechnology.toUpperCase())){
                   isValidPniSfr = true;
            }
        return isValidPniSfr;
    }
    
    public static List<List<DF_SF_Request__c>> spliceSFRBy(List<DF_SF_Request__c> objs, Integer size){
        List<List<DF_SF_Request__c>> lstSFRList = new List<List<DF_SF_Request__c>>();
        Integer numberOfChunks = objs.size() / size;
        
        for(Integer j = 0; j < numberOfChunks; j++ ){           
            List<DF_SF_Request__c> someList = new List<DF_SF_Request__c>();

            for(Integer i = j * size; i < (j+1) * size; i++){
                someList.add(objs[i]);
            }
            lstSFRList.add(someList);
        }
        
        if(numberOfChunks * size < objs.size()){
            List<DF_SF_Request__c> aList = new List<DF_SF_Request__c>();
            for(Integer k = numberOfChunks * size ; k < objs.size(); k++){
                aList.add(objs[k]);
            }
            lstSFRList.add(aList);
        }
        return lstSFRList;
    }
    public static List<DF_SF_Request__c> updatePniResponse(NS_PNI_QueryService.queryResponse_element pniResponse, List<DF_SF_Request__c> lstSFR){
        List<DF_SF_Request__c> lstSfrUpdated = new List<DF_SF_Request__c>();
        try{
            SF_ServiceFeasibilityResponse sfrJSRec;
            Map<String, NS_PNI_QueryService.queryResult_element> mapQueryResponse;
            NS_PNI_QueryService.queryResult_element resElement;
            //check if there are any callout errors
            if(pniResponse.exception_x == null){
                //if no errors, create a map of locationId & response record
                mapQueryResponse = new Map<String, NS_PNI_QueryService.queryResult_element>();
                for(NS_PNI_QueryService.queryResult_element responseElement: pniResponse.queryResultList.queryResult){
                    mapQueryResponse.put(responseElement.Id, responseElement);
                }
                //iterate through SFR records and update the PNI response on the sfr record
                for(DF_SF_Request__c sfrRec: lstSFR){
                    sfrJSRec = (SF_ServiceFeasibilityResponse)System.JSON.deserialize(sfrRec.Response__c, SF_ServiceFeasibilityResponse.class);
                    resElement = mapQueryResponse.get(sfrJSRec.LocId);
                    if(resElement.exception_x == null){
                        sfrJSRec.systemId = resElement.systemId;
                        sfrJSRec.id = resElement.Id;
                        for(NS_PNI_QueryService.describedby_element descElement: resElement.describedby){
                            if(descElement.characteristic.ID.equalsIgnoreCase('AssetStatus')){
                                sfrJSRec.fibreJointStatus = descElement.value;
                            }else if(descElement.characteristic.ID.equalsIgnoreCase('AssetId')){
                                sfrJSRec.fibreJointId = descElement.value;
                            }else if(descElement.characteristic.ID.equalsIgnoreCase('AssetOwner')){
                                sfrJSRec.assetOwner = descElement.value;
                            }else if(descElement.characteristic.ID.equalsIgnoreCase('AssetDistance')){
                                sfrJSRec.Distance = Decimal.valueOf(descElement.value).round(System.RoundingMode.CEILING).intValue();
                            }else if(descElement.characteristic.ID.equalsIgnoreCase('AssetLat')){
                                sfrJSRec.assetLat = descElement.value;
                            }else if(descElement.characteristic.ID.equalsIgnoreCase('AssetLong')){
                                sfrJSRec.assetLong = descElement.value;
                            }else if(descElement.characteristic.ID.equalsIgnoreCase('OLT_Exists')){
                                sfrJSRec.OLT_Exists = descElement.value;
                            }else if(descElement.characteristic.ID.equalsIgnoreCase('AssetTypeCode')){
                                sfrJSRec.fibreJointTypeCode = descElement.value;
                            }else if(descElement.characteristic.ID.equalsIgnoreCase('OLT_ID')){
                                sfrJSRec.OLT_ID = descElement.value;
                            }
                        }
                        if(String.isNotBlank(sfrJSRec.assetLong) && String.isNotBlank(sfrJSRec.assetLat)){
                            sfrJSRec.fibreJointLatLong=sfrJSRec.assetLat+', '+sfrJSRec.assetLong;
                        }
                        sfrJSRec.pniException = null;
                    }else{
                        sfrJSRec.id = resElement.Id;
                        sfrJSRec.pniException = 'Type= ' + resElement.exception_x.exceptionType +
                            ', Code= ' + resElement.exception_x.exceptionCode +
                            ', Description= ' + resElement.exception_x.exceptionDescription;
                        sfrJSRec.Status = SF_LAPI_APIServiceUtils.STATUS_VALID;
                        sfrRec.Response__c = JSON.serialize(sfrJSRec);
                        sfrRec.Status__c = SF_LAPI_APIServiceUtils.STATUS_COMPLETED;
                    }
                    sfrJSRec.Status = SF_LAPI_APIServiceUtils.STATUS_VALID;
                    sfrRec.Response__c = JSON.serialize(sfrJSRec);
                    sfrRec.Status__c = SF_LAPI_APIServiceUtils.STATUS_COMPLETED;
                    lstSfrUpdated.add(sfrRec);
                }
            }else{
                //if there were any callout errors update the same on SFR records.
                for(DF_SF_Request__c sfrRec: lstSFR){
                    sfrJSRec = (SF_ServiceFeasibilityResponse)System.JSON.deserialize(sfrRec.Response__c, SF_ServiceFeasibilityResponse.class);
                    sfrJSRec.pniException = 'Type= ' + pniResponse.exception_x.exceptionType +
                        ', Code= ' + pniResponse.exception_x.exceptionCode +
                        ', Description= ' + pniResponse.exception_x.exceptionDescription;
                    sfrJSRec.Status = SF_LAPI_APIServiceUtils.STATUS_VALID;
                    sfrRec.Response__c = JSON.serialize(sfrJSRec);
                    sfrRec.Status__c = SF_LAPI_APIServiceUtils.STATUS_COMPLETED;
                    lstSfrUpdated.add(sfrRec);
                }
            }
        }catch(Exception ex){
            System.debug('NS_PNI_Util::updatePniResponse EXCEPTION: '+ ex.getMessage() + '\n' +ex.getStackTraceString());
            throw new CustomException(ex.getMessage());
        }
        
        return lstSfrUpdated;
    }
}