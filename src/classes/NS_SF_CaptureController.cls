/**
 * Created by Gobind.Khurana on 18/05/2018.
 */

public class NS_SF_CaptureController {
    // Errors
    public static final String ERR_OPPTY_BUNDLE_ID_NOT_FOUND = 'Opportunity Bundle Id was not found.';
    public static final String ERR_COMM_USR_ACCT_ID_NOT_FOUND = 'Community User AccountId was not found.';
    public static final String ERR_COMM_USR_ID_NOT_FOUND = 'Community UserId was not found.';
    public static final String ERR_COMM_USR_NOT_FOUND = 'Community User was not found.'; 

    @AuraEnabled
    public static String searchByLocationID(String productType, String locId) {
        Id opptyBundleId;
        String searchSrc = 'LocId';

        Map<String, String> addressInputsMap = new Map<String, String>();

        try {
            system.debug('**productType***'+productType);
			
            // Create Oppty Bundle rec
            opptyBundleId = createOpptyBundle(productType);

            if (opptyBundleId != null) {
                createServiceFeasibilityRequest(searchSrc, productType, SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, locId, null, null, opptyBundleId, null, addressInputsMap);

                // Invoke SendLocationRequest Batch LAPI integration process
                Id jobId = System.enqueueJob(new SF_SendLocationRequestQueueHandler(opptyBundleId));

                if (jobId != null) {
                    system.debug('SF_SendLocationRequestQueueHandler queueable job started - jobId: ' + jobId);
                }
            } else {
                throw new CustomException(NS_SF_CaptureController.ERR_OPPTY_BUNDLE_ID_NOT_FOUND);
            }

        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }

        return opptyBundleId;
    }


    public static Id createOpptyBundle(String productType) {
        Id commUserAccountId;
        DF_Opportunity_Bundle__c opptyBundle = new DF_Opportunity_Bundle__c();
        list <Object_Record_Type__mdt> l_prod_RecordTypes = new list <Object_Record_Type__mdt>(); 
        String prod_RecordType;

        system.debug('**productType in createOpptyBundle**: '+productType);
                
        l_prod_RecordTypes = (list<Object_Record_Type__mdt>) SF_CS_API_Util.getCustomMetadataByType('Object_Record_Type__mdt', productType);

        system.debug('**l_prod_RecordTypes**: '+l_prod_RecordTypes);

        if(l_prod_RecordTypes.size()>0)
        {
            prod_RecordType = l_prod_RecordTypes[0].Opportunity_Bundle_Record_Type_Name__c;
        }

        opptyBundle.RecordTypeId = Schema.SObjectType.DF_Opportunity_Bundle__c.getRecordTypeInfosByDeveloperName().get(prod_RecordType).getRecordTypeId();

        try {
            // Get account id from Partner Community user
            commUserAccountId = getCommUserAccountId();

            if (commUserAccountId != null) {
                opptyBundle.Account__c = commUserAccountId;
            } else {
                throw new CustomException(NS_SF_CaptureController.ERR_COMM_USR_ACCT_ID_NOT_FOUND);
            }

            Database.insert(opptyBundle);
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }

        return opptyBundle.Id;
    }

    public static void createServiceFeasibilityRequest(String searchSource, String productType, String searchType, String locId, String latitude, String longitude, Id opptyBundleId, String response, Map<String, String> addressInputsMap) {
        DF_SF_Request__c sfReq;

        // Vars to populate DTO Response values
        List<String> dtoAddressList = new List<String>();
        String dtoAddress;
        String dtoLatitude;
        String dtoLongitude;
        String dtoLocId;
        Integer dtoDistance; // Will always be initially null
        String dtoAccessTechnology; // Will always be initially null
        String dtoStatus = SF_LAPI_APIServiceUtils.STATUS_PENDING;

        system.debug('**productType in createServiceFeasibilityRequest**: '+productType);

        try {
            sfReq = new DF_SF_Request__c();
            sfReq.Location_Id__c = locId;
            sfReq.Latitude__c = latitude;
            sfReq.Longitude__c = longitude;
            sfReq.Opportunity_Bundle__c = opptyBundleId;
            sfReq.Search_Type__c = searchType;
            sfReq.Status__c = SF_LAPI_APIServiceUtils.STATUS_PENDING;
            sfReq.Product_Type__c = productType;
            sfReq.Search_Source__c = searchSource;

            // Search By Location ID
            if (searchType.equalsIgnoreCase(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID)) {
                dtoLocId = locId;
                
            } 

            // Create sfResponse
            SF_ServiceFeasibilityResponse sfResponse = new SF_ServiceFeasibilityResponse(dtoLocId, dtoLatitude, dtoLongitude, dtoAddressList, dtoDistance, dtoAccessTechnology, dtoStatus, null, null, null, null, null, null, null);
            String dfSFRespDTO = JSON.serialize(sfResponse);
            system.debug('***sfReq.Product_Type__c in createServiceFeasibilityRequest** :'+sfReq.Product_Type__c );

            sfReq.Response__c = dfSFRespDTO;
            insert sfReq;
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    public static Id getCommUserAccountId() {
        Id commUserAccountId;
        Id commUserId;
        User commUser;

        try {
            // Get Community user AccountId
            commUserId = UserInfo.getUserId();

            if (commUserId != null) {
                commUser = [SELECT AccountId
                FROM User
                WHERE Id = :commUserId];
            } else {
                throw new CustomException(ERR_COMM_USR_ID_NOT_FOUND);
            }

            if (commUser != null) {
                if (commUser.AccountId != null) {
                    commUserAccountId = commUser.AccountId;
                } else {
                    throw new CustomException(NS_SF_CaptureController.ERR_COMM_USR_ACCT_ID_NOT_FOUND);
                }
            } else {
                throw new CustomException(NS_SF_CaptureController.ERR_COMM_USR_NOT_FOUND);
            }
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }

        return commUserAccountId;
    }
}