/**
 * Created by Dheeraj.Mandavilli on 21/06/2018.
 */
public class NS_SF_LocationbyAddCls {

    // Errors
    public static final String ERR_OPPTY_BUNDLE_ID_NOT_FOUND = 'Opportunity Bundle Id was not found.';
    public static final String ERR_COMM_USR_ACCT_ID_NOT_FOUND = 'Community User AccountId was not found.';
    public static final String ERR_COMM_USR_ID_NOT_FOUND = 'Community UserId was not found.';
    public static final String ERR_COMM_USR_NOT_FOUND = 'Community User was not found.';


    @AuraEnabled
    public static list<String> retUnitTypeValues()
    {
        list <String> li_UnitTypeValues = new list <String>();

        List<Schema.PicklistEntry> pick_list_values = new List<Schema.PicklistEntry>();

        pick_list_values = NS_SF_Utils.returnPickList('DF_SF_Request__c','Unit_Type__c');

        li_UnitTypeValues.add('Select option...'+','+'null');

        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            li_UnitTypeValues.add(a.getLabel()+','+a.getValue());//add the value  to our final list
        }

        system.debug('**li_UnitTypeValues**: '+li_UnitTypeValues);

        return li_UnitTypeValues;
    }

    @AuraEnabled
    public static list<String> retStreetTypeValues()
    {
        list <String> li_StreetTypeValues = new list <String>();

        List<Schema.PicklistEntry> pick_list_values = new List<Schema.PicklistEntry>();

        pick_list_values = NS_SF_Utils.returnPickList('DF_SF_Request__c','Street_Type__c');

        li_StreetTypeValues.add('Select option...'+','+'null');

        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
            li_StreetTypeValues.add(a.getLabel()+','+a.getValue());//add the value  to our final list
        }

        system.debug('**li_StreetTypeValues**: '+li_StreetTypeValues);

        return li_StreetTypeValues;

    }

    @AuraEnabled
    public static String searchByAddress(String productType, String state, String postcode, String suburbLocality, String streetName, String streetType,
            String streetLotNumber, String unitType, String unitNumber, String level) 
    {   
        Map<String, String> addressInputsMap = new Map<String, String>();
        addressInputsMap.put('state', state);
        addressInputsMap.put('postcode', postcode);
        addressInputsMap.put('suburbLocality', suburbLocality);
        addressInputsMap.put('streetName', streetName);
        addressInputsMap.put('streetType', streetType);
        addressInputsMap.put('streetLotNumber', streetLotNumber);
        addressInputsMap.put('unitType', unitType);
        addressInputsMap.put('unitNumber', unitNumber);
        addressInputsMap.put('level', level);
        
        Id opptyBundleId;
        String searchSrc='Address';
        
        try {
            String cisSetting = SF_CS_API_Util.getCustomSettingValue('CIS_ENABLE_CALLOUT');
			Boolean isCISEnabled = String.isBlank(cisSetting) ? false : Boolean.valueOf(cisSetting);
            
            System.debug('============>isCISEnabled::::'+isCISEnabled);
            
            if(!isCISEnabled){
                // Create Oppty Bundle rec
                opptyBundleId = createOpptyBundle(productType);
                
                if (opptyBundleId != null) {
                    createServiceFeasibilityRequest(searchSrc, productType, SF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, null, null, null, opptyBundleId, null, addressInputsMap);
                    
                    if (!Test.isRunningTest()) {
                        // Invoke SendLocationRequest Batch LAPI integration process
                        Id jobId = System.enqueueJob(new SF_SendLocationRequestQueueHandler(opptyBundleId));
                        
                        if (jobId != null) {
                            system.debug('DF_SendLocationRequestQueueHandler queueable job started - jobId: ' + jobId);
                        }
                    }
                } else {
                    throw new CustomException(NS_SF_LocationbyAddCls.ERR_OPPTY_BUNDLE_ID_NOT_FOUND);
                }
            } else {
                List<DF_SF_Request__c> sfReqIdList = NS_CISCalloutAPIUtils.createServiceFeasibilityRequest(searchSrc, productType, SF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, null, null, null, opptyBundleId, null, addressInputsMap);                
                opptyBundleId = NS_CISCalloutHandler.searchLocationFromCIS(sfReqIdList);
            }
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        
        return opptyBundleId;
    }


    public static Id createOpptyBundle(String productType) {
        Id commUserAccountId;
        DF_Opportunity_Bundle__c opptyBundle = new DF_Opportunity_Bundle__c();
        list <Object_Record_Type__mdt> l_prod_RecordTypes = new list <Object_Record_Type__mdt>(); 
        String prod_RecordType;

        system.debug('**productType in createOpptyBundle**: '+productType);
                
        l_prod_RecordTypes = (list<Object_Record_Type__mdt>) SF_CS_API_Util.getCustomMetadataByType('Object_Record_Type__mdt', productType);

        system.debug('**l_prod_RecordTypes**: '+l_prod_RecordTypes);

        if(l_prod_RecordTypes.size()>0)
        {
            prod_RecordType = l_prod_RecordTypes[0].Opportunity_Bundle_Record_Type_Name__c;
        }

        opptyBundle.RecordTypeId = Schema.SObjectType.DF_Opportunity_Bundle__c.getRecordTypeInfosByDeveloperName().get(prod_RecordType).getRecordTypeId();

        try {
            // Get account id from Partner Community user
            commUserAccountId = getCommUserAccountId();

            if (commUserAccountId != null) {
                opptyBundle.Account__c = commUserAccountId;
            } else {
                throw new CustomException(NS_SF_LocationbyAddCls.ERR_COMM_USR_ACCT_ID_NOT_FOUND);
            }

            insert opptyBundle;
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }

        return opptyBundle.Id;
    }

    public static void createServiceFeasibilityRequest(String searchSource, String productType, String searchType, String locId, String latitude, String longitude, Id opptyBundleId, String response, Map<String, String> addressInputsMap) {
        DF_SF_Request__c sfReq;

        // Vars to populate DTO Response values
        List<String> dtoAddressList = new List<String>();
        String dtoAddress;
        String dtoLatitude;
        String dtoLongitude;
        String dtoLocId;
        Integer dtoDistance; // Will always be initially null
        String dtoAccessTechnology; // Will always be initially null
        String dtoStatus = SF_LAPI_APIServiceUtils.STATUS_PENDING;

        system.debug('**productType in createServiceFeasibilityRequest**: '+productType);

        try {
            sfReq = new DF_SF_Request__c();
            sfReq.Location_Id__c = locId;
            sfReq.Latitude__c = latitude;
            sfReq.Longitude__c = longitude;
            sfReq.Opportunity_Bundle__c = opptyBundleId;
            sfReq.Search_Type__c = searchType;
            sfReq.Status__c = SF_LAPI_APIServiceUtils.STATUS_PENDING;
            sfReq.Product_Type__c = productType;
            sfReq.Search_Source__c = searchSource;

            
                // Search By Address
            if (searchType.equalsIgnoreCase(SF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS)) {
                // Update Address field inputs
                if (!addressInputsMap.isEmpty()) {
                    sfReq.State__c = addressInputsMap.get('state');
                    sfReq.Postcode__c = addressInputsMap.get('postcode');
                    sfReq.Suburb_Locality__c = addressInputsMap.get('suburbLocality');
                    sfReq.Street_Name__c = addressInputsMap.get('streetName');
                    sfReq.Street_Type__c = addressInputsMap.get('streetType');
                    sfReq.Street_or_Lot_Number__c = addressInputsMap.get('streetLotNumber');
                    sfReq.Unit_Type__c = addressInputsMap.get('unitType');
                    sfReq.Unit_Number__c = addressInputsMap.get('unitNumber');
                    sfReq.Level__c = addressInputsMap.get('level');

                    dtoAddress = SF_LAPI_APIServiceUtils.getInputAddressFullText(sfReq);

                    if (String.isNotEmpty(dtoAddress)) {
                        dtoAddressList.add(dtoAddress);
                    }
                }
            }

            // Create sfResponse
            SF_ServiceFeasibilityResponse sfResponse = new SF_ServiceFeasibilityResponse(dtoLocId, dtoLatitude, dtoLongitude, dtoAddressList, dtoDistance, dtoAccessTechnology, dtoStatus, null, null, null, null, null, null, null);
            String dfSFRespDTO = JSON.serialize(sfResponse);
            system.debug('***sfReq.Product_Type__c in createServiceFeasibilityRequest** :'+sfReq.Product_Type__c );

            sfReq.Response__c = dfSFRespDTO;
            insert sfReq;
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    public static Id getCommUserAccountId() {
        Id commUserAccountId;
        Id commUserId;
        User commUser;

        try {
            // Get Community user AccountId
            commUserId = UserInfo.getUserId();

            if (commUserId != null) {
                commUser = [SELECT AccountId
                FROM User
                WHERE Id = :commUserId];
            } else {
                throw new CustomException(ERR_COMM_USR_ID_NOT_FOUND);
            }

            if (commUser != null) {
                if (commUser.AccountId != null) {
                    commUserAccountId = commUser.AccountId;
                } else {
                    throw new CustomException(NS_SF_LocationbyAddCls.ERR_COMM_USR_ACCT_ID_NOT_FOUND);
                }
            } else {
                throw new CustomException(NS_SF_LocationbyAddCls.ERR_COMM_USR_NOT_FOUND);
            }
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }

        return commUserAccountId;
    }

    
}