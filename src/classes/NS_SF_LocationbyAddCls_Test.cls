/**
 * Created by Dheeraj.Kanna on 03/07/2018.
 */
@isTest
public class NS_SF_LocationbyAddCls_Test { 


   static String productType = 'NBN_SELECT';

    @isTest static void test_retUnitTypeValues() {
        User commUser;
        list <String> li_UnitTypeValues = new list <String>();

        commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser){
            test.startTest();

            li_UnitTypeValues = NS_SF_LocationbyAddCls.retUnitTypeValues();
            system.debug('*li_UnitTypeValues*'+li_UnitTypeValues);

            test.stopTest();
        }
    }

    @isTest static void test_retStreetTypeValues() {
        User commUser;
        list <String> li_StreetTypeValues = new list <String>();

        commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser){
            test.startTest();

            li_StreetTypeValues = NS_SF_LocationbyAddCls.retStreetTypeValues();
            system.debug('*li_StreetTypeValues*'+li_StreetTypeValues);

            test.stopTest();
        }
    }

    @isTest static void test_searchByAddress() {
        // Setup data
        String opptyBundleId;
        String state = 'VIC';
        String postcode = '3000';
        String suburbLocality = 'Melbourne';
        String streetName = 'Flinders';
        String streetType = 'ST';
        String streetLotNumber = '21';
        String unitType = '';
        String unitNumber = '';
        String level = '';

        User commUser;
        commUser = SF_TestData.createDFCommUser();

        // Generate mock response
        String response = SF_IntegrationTestData.buildAddressAPIRespSuccessful();

        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = NS_SF_LocationbyAddCls.searchByAddress(NS_SF_LocationbyAddCls_Test.productType, state, postcode, suburbLocality, streetName, streetType, streetLotNumber, unitType, unitNumber, level);// level, complexSiteName, complexBuildingName, complexStreetName, complexStreetType, complexStreetNumber);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

    

    @isTest static void test_createOpptyBundle() {
        // Setup data
        Id opptyBundleId;
        User commUser;

        commUser = SF_TestData.createDFCommUser();

        system.debug('**NS_SF_LocationbyAddCls_Test.productType**'+NS_SF_LocationbyAddCls_Test.productType);

        system.runAs(commUser) {
            // Statements to be executed by this test user
            test.startTest();

            opptyBundleId = NS_SF_LocationbyAddCls.createOpptyBundle(NS_SF_LocationbyAddCls_Test.productType);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

    @isTest static void test_createServiceFeasibilityRequest() {
        // Setup data
        String mockResponse = '';

        String searchType = 'SearchByLocationID';
        String locId = 'LOC000035375038';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        Id opptyBundleId;
        Map<String, String> addressInputsMap = new Map<String, String>();

        User commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser) {
            opptyBundleId = NS_SF_LocationbyAddCls.createOpptyBundle(NS_SF_LocationbyAddCls_Test.productType);
        }

        test.startTest();

        NS_SF_LocationbyAddCls.createServiceFeasibilityRequest(null,NS_SF_LocationbyAddCls_Test.productType, searchType, locId, null, null, opptyBundleId, null, addressInputsMap);

        test.stopTest();

        // Assertions
        List<DF_SF_Request__c> existingSFRequestList = [SELECT Id
        FROM   DF_SF_Request__c];

        system.Assert(existingSFRequestList.size() > 0);
    }

    @isTest static void test_getCommUserAccountId() {
        // Setup data
        Id commUserAccountId;
        User commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser) {
            test.startTest();

            commUserAccountId = NS_SF_LocationbyAddCls.getCommUserAccountId();

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, commUserAccountId);
    }

}