/**
 * Created by Dheeraj.Kanna on 03/07/2018.
 */
@isTest
public class NS_SF_LocationbyLatLngCls_Test {
	static String productType = 'NBN_SELECT';


    @isTest static void test_searchByLatLong() {
        // Setup data
        Id opptyBundleId;
        String latitude = '-33.840213';
        String longitude = '151.207368';

        User commUser;
        commUser = SF_TestData.createDFCommUser();

        // Generate mock response
        String response = SF_IntegrationTestData.buildLocationAPIRespSuccessful();

        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

        system.runAs(commUser) {
            test.startTest();

            opptyBundleId = NS_SF_LocationbyLatLngCls.searchByLatLong(NS_SF_LocationbyLatLngCls_Test.productType, latitude, longitude);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

    @isTest static void test_createOpptyBundle() {
        // Setup data
        Id opptyBundleId;
        User commUser;

        commUser = SF_TestData.createDFCommUser();

        system.debug('**NS_SF_LocationbyLatLngCls_Test.productType**'+NS_SF_LocationbyLatLngCls_Test.productType);

        system.runAs(commUser) {
            // Statements to be executed by this test user
            test.startTest();

            opptyBundleId = NS_SF_LocationbyLatLngCls.createOpptyBundle(NS_SF_LocationbyLatLngCls_Test.productType);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, opptyBundleId);
    }

    @isTest static void test_createServiceFeasibilityRequest() {
        // Setup data
        String mockResponse = '';

        String searchType = 'SearchByLocationID';
        String locId = 'LOC000035375038';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        Id opptyBundleId;
        Map<String, String> addressInputsMap = new Map<String, String>();

        User commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser) {
            opptyBundleId = NS_SF_LocationbyLatLngCls.createOpptyBundle(NS_SF_LocationbyLatLngCls_Test.productType);
        }

        test.startTest();

        NS_SF_LocationbyLatLngCls.createServiceFeasibilityRequest(null,NS_SF_LocationbyLatLngCls_Test.productType, searchType, locId, null, null, opptyBundleId, null, addressInputsMap);

        test.stopTest();

        // Assertions
        List<DF_SF_Request__c> existingSFRequestList = [SELECT Id
        FROM   DF_SF_Request__c];

        system.Assert(existingSFRequestList.size() > 0);
    }

    @isTest static void test_getCommUserAccountId() {
        // Setup data
        Id commUserAccountId;
        User commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser) {
            test.startTest();

            commUserAccountId = NS_SF_LocationbyLatLngCls.getCommUserAccountId();

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, commUserAccountId);
    }
}