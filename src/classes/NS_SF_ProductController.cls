/**
 * Created by Gobind.Khurana on 21/05/2018.
 */

public class NS_SF_ProductController {

    public static final String GREEN = 'Green';
    /*
    * Includes the logic to create product basket.
    * A product basket is created for every opportunity. This product basket includes one product configurations - Build contribution.
    * Parameters : DF_Quote object.
    * @Return : void
    */
    @future
    public static void createProductBasket(Id quoteId) {
        DF_Quote__c dfQuote = [
                SELECT name, Location_Id__c,Address__c,Latitude__c,Longitude__c,Opportunity__c,Opportunity_Bundle__c, GUID__c,
                        Opportunity__r.Name,RAG__c,Opportunity_Bundle__r.name,Fibre_Build_Cost__c,LAPI_Response__c, Quote_Name__c, Opportunity_Bundle__r.Opportunity_Bundle_Name__c,
                        Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c
                FROM DF_Quote__c
                WHERE id = :quoteId
        ];

        if (dfQuote != null) {

            //get the opportunity id from quote
            String oppId = dfQuote.Opportunity__c;

            cscfga__Product_Basket__c basket;
            cscfga.API_1.ApiSession apiSession;

            //create api session
            apiSession = SF_CS_API_Util.createApiSession(basket);

            //create basket for the session and associate it with the opportunity
            basket = SF_CS_API_Util.createBasket(apiSession);
            basket.cscfga__Opportunity__c = oppId;
            update basket;

            //add product configurations and related product to basket
            String result = createProductBasketWithDefinitions(apiSession, basket, dfQuote);
            System.debug('Create product basket result: ' + result);

            /*OrderingOnBehalfOfRSP*/
            BusinessEvent__e nsBusEvent = NS_SF_ValidationController.createBusinessEvent((SF_ServiceFeasibilityResponse) JSON.deserialize(dfQuote.LAPI_Response__c, SF_ServiceFeasibilityResponse.class), dfQuote, true);
            Database.SaveResult results = EventBus.publish(nsBusEvent);
            if (results.isSuccess()) {
                System.debug('Successfully published event.');
            } else {
                for (Database.Error err : results.getErrors()) {
                    System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
                }
            }

        }
    }

    /*
    * Includes the logic to create product basket with definition (Product charges (with a related product OVC), build contribution).
    * Parameters : apiSession, basket, quote object, technology, csa, sam.
    * @Return : String that says if the creation of products was successful
    */
    private static String createProductBasketWithDefinitions(cscfga.API_1.ApiSession apiSession, cscfga__Product_Basket__c basket, DF_Quote__c quote) {

        String result;
        if (apiSession != null) {
            apiSession = SF_CS_API_Util.createApiSession(basket);
        }

        List<csbb__Product_Configuration_Request__c> pcrList = new List<csbb__Product_Configuration_Request__c>();

        SF_ServiceFeasibilityResponse sfr = (SF_ServiceFeasibilityResponse) System.JSON.deserialize(quote.LAPI_Response__c, SF_ServiceFeasibilityResponse.class);
        String technologyVal = sfr.derivedTechnology;
        Integer distance = sfr.Distance;
        String rag = quote.RAG__c;

        //fetch custom setting values for product charges, build contribution, ovc and category
        String fbcOfferId = SF_CS_API_Util.getCustomSettingValue('SF_BUILD_CONTRIBUTION_OFFER_ID');
        String fbcDefId = SF_CS_API_Util.getCustomSettingValue('SF_BUILD_CONTRIBUTION_DEFINITION_ID');
        String categoryId = SF_CS_API_Util.getCustomSettingValue('SF_CATEGORY_ID');
        String autoQuoteRags = SF_CS_API_Util.getCustomSettingValue('SF_AUTOQUOTERAGS');

        if (String.isNotBlank(fbcOfferId) && String.isNotBlank(categoryId) && String.isNotBlank(fbcDefId)) {
            //query fbc offer
            SObject fbcOffers = new cscfga__Configuration_Offer__c();
            String queryStringOfr = SF_CS_API_Util.getQuery(fbcOffers, ' WHERE Id = ' + '\'' + fbcOfferId + '\'');
            List<cscfga__Configuration_Offer__c> fbcOfrList = (List<cscfga__Configuration_Offer__c>) SF_CS_API_Util.getQueryRecords(queryStringOfr);
            cscfga__Configuration_Offer__c fbcOffer = !fbcOfrList.isEmpty() ? fbcOfrList.get(0) : null;

            //query category value
            SObject categoryObject = new cscfga__Product_Category__c();
            String queryStrng = SF_CS_API_Util.getQuery(categoryObject, ' WHERE Id = ' + '\'' + categoryId + '\'');
            List<cscfga__Product_Category__c> categoryList = (List<cscfga__Product_Category__c>) SF_CS_API_Util.getQueryRecords(queryStrng);
            cscfga__Product_Category__c category = !categoryList.isEmpty() ? categoryList.get(0) : null;

            if (fbcOffer != null && category != null) {
                Boolean isCISEnabled = SF_CS_API_Util.isCustomSettingEnabled('CIS_ENABLE_CALLOUT');

                apiSession.setProductToConfigure(new cscfga__Product_Definition__c(Id = fbcDefId), new Map<String, String>{
                        'containerType' => 'basket', 'linkedId' => basket.Id
                });
                cscfga.ProductConfiguration config1 = apiSession.getRootConfiguration();
                for (cscfga.Attribute att : config1.getAttributes()) {
                    if (isCISEnabled && sfr.whiteListQuoteAmount != null) {
                        if (att.getName() == 'FibreBuildChargeOverride') {
                            att.setValue(String.valueOf(sfr.whiteListQuoteAmount));
                            att.setDisplayValue(String.valueOf(sfr.whiteListQuoteAmount));
                        }
                    }
                    else {
                        if (att.getName() == 'TechnologyType') {
                            att.setValue(technologyVal);
                        } else if (att.getName() == 'FibreDistance') {
                            att.setValue(String.valueOf(distance));
                        } else if (att.getName() == 'RAGStatus') {
                            att.setValue(rag);
                        }
                    }
                }

                apiSession.executeRules();
                cscfga.ValidationResult vr = apiSession.validateConfiguration(); //ValidationResult
                System.debug('Validation results' + vr);
                apiSession.persistConfiguration();

                cscfga__Product_Configuration__c config = config1.getSObject();
                pcrList.add(SF_CS_API_Util.createProductConfigurationRequest(basket, config, fbcOfferId, category));
                System.debug('config--' + config);

                //revalidate configurations to get the final totals
                Set<Id> bsktIds = new Set<Id>{
                        basket.id
                };
                cscfga.ProductConfigurationBulkActions.calculateTotals(bsktIds);


                //update the DFquote object with calculate charge
                cscfga__Product_Configuration__c prodConfig = [SELECT Id, Name, cscfga__Product_Basket__c,cscfga__total_one_off_charge__c,cscfga__Product_Bundle__c,cscfga__Configuration_Offer__c from cscfga__Product_Configuration__c where Id = :config.Id LIMIT 1];
                if (autoQuoteRags.contains(quote.RAG__c)) {
                    if (!isCISEnabled) {
                        quote.Fibre_Build_Cost__c = prodConfig.cscfga__total_one_off_charge__c;
                        update quote;
                    }
                }

                if (!pcrList.isEmpty()) {
                    insert pcrList;
                    result = 'Success';
                } else {
                    result = 'Product configuration request records do not exist';
                }
            } else {
                result = 'FBC product definition or category does not exist';
            }
        } else {
            result = 'Custom settings does not exist';
        }
        return result;
    }

    /*
    * Includes the logic to get basket id.
    * Parameters : quote id.
    * @Return : String that says if the execution of functionality was successful. If so basket id is returned
    */
    @AuraEnabled
    public static String getBasketId(String quoteId) {
        String basketId;
        if (String.isNotBlank(quoteId)) {
            List<DF_Quote__c> quoteList = [SELECT Id, Name, RAG__c, Location_Id__c, Address__c, Opportunity__c, LAPI_Response__c, Fibre_Build_Cost__c FROM DF_Quote__c WHERE Id = :quoteId];
            DF_Quote__c quoteObj = !quoteList.isEmpty() ? quoteList.get(0) : null;
            String oppId = quoteObj != null ? quoteObj.Opportunity__c : null;
            if (String.isNotBlank(oppId)) {
                List<cscfga__Product_Basket__c> baskets = [SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :oppId];
                basketId = !baskets.isEmpty() ? baskets.get(0) != null ? baskets.get(0).Id : null : null;
                if (String.isEmpty(basketId))
                    basketId = 'Error';
            } else
                    basketId = 'Error';
        } else
                basketId = 'Error';
        return basketId;
    }

    /*
    * Includes the logic to get the product configuration for Product charges and OVC and other DF Quote related information as a serialized string.
    * Parameters : quote id.
    * @Return : String that says if the execution of functionality was successful. If so, a serialized string is returned
    */
    @AuraEnabled
    public static String getQuickQuoteData(String quoteId) {
        String serializedResponse;
        String bcDefId = SF_CS_API_Util.getCustomSettingValue('SF_BUILD_CONTRIBUTION_DEFINITION_ID');
        if (String.isNotBlank(quoteId) && String.isNotBlank(bcDefId)) {
            List<DF_Quote__c> quoteList = [SELECT Id, Name, RAG__c, Location_Id__c, Address__c, Opportunity__c, LAPI_Response__c, Fibre_Build_Cost__c FROM DF_Quote__c WHERE Id = :quoteId];
            DF_Quote__c quoteObj = !quoteList.isEmpty() ? quoteList.get(0) : null;
            String oppId = quoteObj != null ? quoteObj.Opportunity__c : null;
            List<cscfga__Product_Basket__c> baskets = [SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :oppId];
            String basketId = !baskets.isEmpty() ? baskets.get(0) != null ? baskets.get(0).Id : null : null;
            Map<Id, cscfga__Product_Configuration__c> configs = new Map<Id, cscfga__Product_Configuration__c>([Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and cscfga__Product_Definition__c != :bcDefId]);
            if (!configs.isEmpty()) {
                //fetches configurations from the product basket
                Map<String, Object> configResponse = cscfga.API_1.getProductConfigurations(new List<Id>(configs.keySet()));
                System.debug('PPPP configResponse: ' + configResponse);
                if (quoteObj != null && configResponse != null && String.isNotBlank(basketId))
                    //serialize the information required for quick quote screen
                    serializedResponse = json.serialize(new SF_QuickQuoteData(quoteObj.Name, quoteObj.Location_Id__c, quoteObj.Address__c, configResponse, basketId, quoteObj.Fibre_Build_Cost__c)); else
                        serializedResponse = 'Error';
            } else
                    serializedResponse = 'Error';
        } else
                serializedResponse = 'Error';
        System.debug('PPPP serializedResponse: ' + serializedResponse);
        return serializedResponse;
    }

    /*
    * Includes the logic to fetch select options values for attributes displayed in quick quote screen.
    * Parameters : basket id, boolean which says if its Product charges/OVC, picklist names list.
    * @Return : String that says if the execution of functionality was successful. If so, all picklist values are returned as a serialized string
    */
    @AuraEnabled
    public static String getOptionsList(String basketId, Boolean pcConfig, List<String> attNameList) {

        String configId;
        String pcDefId = SF_CS_API_Util.getCustomSettingValue('SF_PRODUCT_CHARGE_DEFINITION_ID');
        String ovcDefId = SF_CS_API_Util.getCustomSettingValue('SF_OVC_DEFINITION_ID');
        String optionsResponse;
        Map<String, List<cscfga__Select_Option__c>> optionsMap = new Map<String, List<cscfga__Select_Option__c>>();
        List<cscfga__Select_Option__c> optionsList;
        if (String.isNotBlank(basketId) && !attNameList.isEmpty() && String.isNotBlank(pcDefId) && String.isNotBlank(ovcDefId)) {
            List<cscfga__Product_Basket__c> baskets = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c where id = :basketId];
            cscfga__Product_Basket__c basket = !baskets.isEmpty() ? baskets.get(0) : null;
            //fetch relevant configuration id
            if (pcConfig) {
                List<cscfga__Product_Configuration__c> pcConfigList = [Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and cscfga__Product_Definition__c = :pcDefId];
                cscfga__Product_Configuration__c configObj = !pcConfigList.isEmpty() ? pcConfigList.get(0) : null;
                configId = configObj != null ? configObj.Id : null;
            } else {
                List<cscfga__Product_Configuration__c> ovcConfigList = [Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and cscfga__Product_Definition__c = :ovcDefId];
                cscfga__Product_Configuration__c configObj = !ovcConfigList.isEmpty() ? ovcConfigList.get(0) : null;
                configId = configObj != null ? configObj.Id : null;
            }
            System.debug('YYYY configId:' + configId);
            if (String.isNotBlank(configId)) {
                cscfga.API_1.ApiSession apiSession = SF_CS_API_Util.createApiSession(basket);
                apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = configId, cscfga__Product_Basket__c = basketId));
                cscfga.ProductConfiguration currConfig = apiSession.getConfiguration();
                for (String attName : attNameList) {
                    cscfga.Attribute att = apiSession.getAttributeForCurrentConfig(attName);
                    //method to fetch select options values
                    optionsList = att != null ? att.getAvailableOptions() : null;
                    if (optionsList != null)
                        optionsMap.put(attName, optionsList);
                }
                if (optionsMap != null)
                    optionsResponse = JSON.serialize(optionsMap); else
                        optionsResponse = 'Error';
            } else
                    optionsResponse = 'Error';
        } else
                optionsResponse = 'Error';
        System.debug('YYYY optionsResponse:' + optionsResponse);
        return optionsResponse;
    }


    @AuraEnabled
    public static void processUpdateDFQuoteStatus(String quoteId) {
        DF_Quote__c dfQuoteToUpdate;
        List<DF_Quote__c> dfQuotesToUpdateList;

        try {
            if (String.isNotEmpty(quoteId)) {
                dfQuotesToUpdateList = [
                        SELECT Status__c
                        FROM DF_Quote__c
                        WHERE Id = :quoteId
                        AND Status__c <> :SF_Constants.QUOTE_STATUS_QUICK_QUOTE
                ];
            }

            // Perform update
            if (!dfQuotesToUpdateList.isEmpty()) {
                dfQuoteToUpdate = dfQuotesToUpdateList[0];

                if (dfQuoteToUpdate != null) {
                    dfQuoteToUpdate.Status__c = SF_Constants.QUOTE_STATUS_QUICK_QUOTE;
                    update dfQuoteToUpdate;
                }
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

}