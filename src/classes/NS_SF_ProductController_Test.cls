@isTest
public class NS_SF_ProductController_Test {
    /**
	* Tests queueable method
	*/
    @isTest static void queueableMethodTest()
    {
        Id oppId = SF_TestService.getServiceFeasibilityRequest();
        oppId = SF_TestService.getQuoteRecords();
        Account acc = SF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = SF_TestService.getNewBasketWithConfig(acc);
        Opportunity opp = SF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        List<DF_Quote__c> quoteList = [SELECT Id, RAG__c, Opportunity_Bundle__c, Opportunity__c, LAPI_Response__c FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppId LIMIT 1];
        Test.startTest();
        Id jobId = System.enqueueJob(new SF_ProcessBasketQueueHandler(quoteList));
        Test.stopTest();
    
    }
    
    /**
	* Tests getBasketID method
	*/
    @isTest static void getBasketIDMethodTest()
    {
        Id oppId = SF_TestService.getServiceFeasibilityRequest();
        oppId = SF_TestService.getQuoteRecords();
        Account acc = SF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = SF_TestService.getNewBasketWithConfig(acc);
        Opportunity opp = SF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        Test.startTest();
        List<DF_Quote__c> quoteList = [SELECT Id, RAG__c, Opportunity_Bundle__c, Opportunity__c, LAPI_Response__c FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppId LIMIT 1];
        String result = NS_SF_ProductController.getBasketId(quoteList.get(0).Id);
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
	* Tests getBasketID method for error condition
	*/
    @isTest static void getBasketIDMethodErrorTest()
    {
        Test.startTest();
        String result = NS_SF_ProductController.getBasketId('');
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
    /**
	* Tests getQuickQuoteData method
	*/
    @isTest static void getQuickQuoteDataMethodTest()
    {
        Id oppId = SF_TestService.getServiceFeasibilityRequest();
        oppId = SF_TestService.getQuoteRecords();
        Account acc = SF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = SF_TestService.getNewBasketWithConfig(acc);
        Opportunity opp = SF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        Test.startTest();
        List<DF_Quote__c> quoteList = [SELECT Id, RAG__c, Opportunity_Bundle__c, Opportunity__c, LAPI_Response__c FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppId LIMIT 1];
        String result = NS_SF_ProductController.getQuickQuoteData(quoteList.get(0).Id);
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
	* Tests getQuickQuoteData method error condition
	*/
    @isTest static void getQuickQuoteDataMethodErrorTest()
    {
        Test.startTest();
        String result = NS_SF_ProductController.getQuickQuoteData('');
        System.assertEquals('Error', result);
        Test.stopTest();
    }
    
    /**
	* Tests getOptionsList method
	*/
    @isTest static void getOptionsListMethodTest()
    {
        Id oppId = SF_TestService.getServiceFeasibilityRequest();
        oppId = SF_TestService.getQuoteRecords();
        Account acc = SF_TestData.createAccount('Test Account');
        insert acc;
        cscfga__Product_Basket__c basketObj = SF_TestService.getNewBasketWithConfigQuickQuote(acc);
        Opportunity opp = SF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        List<String> attNameList = new List<String>{'Term'};
        Test.startTest();
        String result = NS_SF_ProductController.getOptionsList(basketObj.Id, true, attNameList);
        System.assertNotEquals('', result);
        Test.stopTest();
    }
    
    /**
	* Tests getOptionsList method error condition
	*/
    @isTest static void getOptionsListMethodErrorTest()
    {
        List<String> attNameList = new List<String>{'Term'};
        Test.startTest();
        String result = NS_SF_ProductController.getOptionsList('', true, attNameList);
        System.assertEquals('Error', result);
        Test.stopTest();
    }

    
    /**
	* Tests NS_SF_QuickQuoteData
	*/
    @isTest static void SF_QuickQuoteDataTest()
    {
        Test.startTest();
        SF_QuickQuoteData qqObj = new SF_QuickQuoteData('EEQ-1234567891', 'LOC123456789', 'My address', null, 'Id', 375.64);
        Test.stopTest();
    }
    
    @isTest static void test_processUpdateDFQuoteStatus() {
    	// Setup data   	   	    	    	
    	List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
    	
    	String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
    	String latitude = '-33.840213';
    	String longitude = '151.207368';

		// Create Account
        Account acct = SF_TestData.createAccount('My account');
        insert acct;

		// Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

		// Create Oppty
		Opportunity oppty = SF_TestData.createOpportunity('LOC111111111111');
		oppty.Opportunity_Bundle__c = opptyBundle.Id;
		insert oppty;

		// Create DFQuote recs
    	DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');	    	
    	dfQuote1.Proceed_to_Pricing__c = true;
    	dfQuoteList.add(dfQuote1);
    	insert dfQuoteList;                       
        
        Test.startTest();

        NS_SF_ProductController.processUpdateDFQuoteStatus(String.valueOf(dfQuote1.Id));
        
        Test.stopTest();
        
		// Assertions
		List<DF_Quote__c> dfQuoteVerifyList = [SELECT Id
									     	   FROM   DF_Quote__c 
									     	   WHERE  Status__c = :SF_Constants.QUOTE_STATUS_QUICK_QUOTE];
									 
        system.AssertEquals(false, dfQuoteVerifyList.isEmpty());        
    }    
}