public class NS_SF_QuoteController {

    @AuraEnabled
    public static String getOrderSummaryData(String opptyBundleId) {
        Map<String, Decimal> recurringCostsMap = new Map<String, Decimal>();
        Map<String, Decimal> nonRecurringCostsMap = new Map<String, Decimal>();

        String parentOppBundleName = getOppBundleName(opptyBundleId);

        try {
            List<NS_SF_OrderSummaryData> orderSummaryDataList = new List<NS_SF_OrderSummaryData>();

            // Get DF Quote data
            List<DF_Quote__c> dfQuoteList = getQuoteRecords(opptyBundleId);

            // Build dfQuoteId set
            Set<String> dfQuoteIdSet = buildDFQuoteIdSet(dfQuoteList);

            // Get Quote Cost data
            getQuoteCosts(dfQuoteIdSet, recurringCostsMap, nonRecurringCostsMap);

            // Process List
            if (!dfQuoteList.isEmpty()) {

                NS_WhiteListQuoteUtil whitelistUtil = (NS_WhiteListQuoteUtil) ObjectFactory.getInstance(NS_WhiteListQuoteUtil.class);
                Map<String, String> locIdToEBTMap = whitelistUtil.getEbtMap(dfQuoteList, 'Location_Id__c');
                
                Set<String> tempLocIdLst = new Set<String>();
                    
                for (DF_Quote__c dfQuote : dfQuoteList) {
                    tempLocIdLst.add(dfQuote.Location_Id__c);
                    Decimal nonRecurringCost = nonRecurringCostsMap.get(String.valueOf(dfQuote.Id));
                    Decimal recurringCost = recurringCostsMap.get(String.valueOf(dfQuote.Id));

                    // Add new entry to the output list
                    NS_SF_OrderSummaryData osDataItem = new NS_SF_OrderSummaryData(
                                                                    dfQuote.Id,
                                                                    dfQuote.Location_Id__c,
                                                                    dfQuote.Quote_Name__c,
                                                                    dfQuote.Address__c,
                                                                    dfQuote.Fibre_Build_Cost__c,
                                                                    nonRecurringCost,
                                                                    recurringCost, '',
                                                                    dfQuote.Latitude__c,
                                                                    dfQuote.Longitude__c,
                                                                    dfQuote.Opportunity__c);
                    osDataItem.oppBundleName=parentOppBundleName;
                    osDataItem.oppBundleId= opptyBundleId;

                    osDataItem.EBT = locIdToEBTMap.get(dfQuote.Location_Id__c);

                    orderSummaryDataList.add(osDataItem);
                }

                updateQuoteSummaryData(tempLocIdLst, orderSummaryDataList);
            }

            return JSON.serialize(orderSummaryDataList);
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    private static final Set<String> ORDER_STATUSES_TO_EXCLUDE = new Set<String>{'In Draft', ''};

    private static void updateQuoteSummaryData(Set<String> tempLocIdLst, List<NS_SF_OrderSummaryData> orderSummaryDataList) {
        Id nsOrderRecType = SF_CS_API_Util.getRecordType(SF_Constants.NBN_SELECT_NAME, SF_Constants.ORDER_OBJECT);

        List<DF_Order__c> ordersAtSameLocations = NS_Without_Sharing_Utils.getSameLocationOrders(tempLocIdLst, nsOrderRecType, ORDER_STATUSES_TO_EXCLUDE);

        String accessSeekerID = [SELECT Contact.Account.Access_Seeker_ID__c FROM User WHERE Id = :UserInfo.getUserId()][0].Contact.Account.Access_Seeker_ID__c;

        System.debug('---AccSeekId on User' + accessSeekerID);

        for (NS_SF_OrderSummaryData tempWrp : orderSummaryDataList) {

            for (DF_Order__c tempDfOrd : ordersAtSameLocations) {
                if (tempWrp.locId == tempDfOrd.DF_Quote__r.Location_Id__c) {
                    String oppAccSeekId = tempDfOrd.Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c;
                    System.debug('---AccSeekId on Account' + oppAccSeekId);
                    if (String.isNotBlank(oppAccSeekId) && String.isNotBlank(accessSeekerID) && oppAccSeekId == accessSeekerID) {
                        tempWrp.isOwnerOrder = (tempDfOrd.Order_Status__c != 'Cancelled' || tempDfOrd.Opportunity_Bundle__c == tempWrp.oppBundleId);
                    } else if(tempDfOrd.Order_Status__c != 'Cancelled') {
                        tempWrp.hasOrder = true;
                    }

                    if (tempWrp.oppBundleId == tempDfOrd.Opportunity_Bundle__c || tempDfOrd.Order_Status__c != 'Cancelled') {
                        tempWrp.status = tempDfOrd.Order_Status__c; //getting order status
                    }
                    tempWrp.orderId = tempDfOrd.Id;
                    System.debug('---OrderId from my logic' + tempWrp.orderId);
                }
            }
        }
    }

    public static List<DF_Quote__c> getQuoteRecords(String opptyBundleId) {
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        try {
            if (String.isNotEmpty(opptyBundleId)) {
                String queryCondition = ' WHERE ((RAG__c = \'' + NS_SF_RAGController.RAG_GREEN + '\') OR (RAG__c = \'' + NS_SF_RAGController.RAG_AMBER + '\''
                        + ' AND Fibre_Build_Cost__c != null) OR (RAG__c = \'' + NS_SF_RAGController.RAG_RED + '\''
                        + ' AND Approved__c = true AND Fibre_Build_Cost__c != null))'
                        + ' AND Opportunity_Bundle__c = \'' + opptyBundleId + '\''
                        + ' AND Status__c != \'' + SF_Constants.QUOTE_STATUS_ACCEPTED + '\''
                        + ' AND Proceed_to_Pricing__c = true'
                        + ' ORDER BY Quote_Name__c Asc';

                String queryStr = SF_CS_API_Util.getQuery(new DF_Quote__c(), queryCondition);
                dfQuoteList = Database.query(queryStr);

                System.debug('**queryStr**'+queryStr);
            }
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }

        return dfQuoteList;
    }

    public static Set<String> buildDFQuoteIdSet(List<DF_Quote__c> dfQuoteList) {
        Set<String> dfQuoteIdSet = new Set<String>();

        try {
            if (!dfQuoteList.isEmpty()) {
                for (DF_Quote__c dfQuote : dfQuoteList) {
                    dfQuoteIdSet.add(dfQuote.Id);
                }
            }
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }

        return dfQuoteIdSet;
    }

    public static void getQuoteCosts(Set<String> dfQuoteIdSet, Map<String, Decimal> recurringCostsMap, Map<String, Decimal> nonRecurringCostsMap) {
        Set<String> opptyIdSet = new Set<String>();
        Set<String> prodBasketIdSet = new Set<String>();

        Map<String, String> dfQuoteIdToOpptyIdMap = new Map<String, String>();
        Map<String, String> opptyIdToProdBasketIdMap = new Map<String, String>();
        Map<String, String> prodBasketIdToProdConfigIdMap = new Map<String, String>();
        Map<String, Decimal> prodConfigIdToRecurringCostMap = new Map<String, Decimal>();
        Map<String, Decimal> prodConfigIdToNonRecurringCostMap = new Map<String, Decimal>();

        List<DF_Quote__c> dfQuoteList;
        List<cscfga__Product_Basket__c> prodBasketList;
        List<cscfga__Product_Configuration__c> prodConfigList;

        try {
            // Query DF Quote to get Oppty Ids
            if (!dfQuoteIdSet.isEmpty()) {
                dfQuoteList = [SELECT Opportunity__c
                FROM   DF_Quote__c
                WHERE  Id IN :dfQuoteIdSet];

                if (!dfQuoteList.isEmpty()) {
                    for (DF_Quote__c dfQuote : dfQuoteList) {
                        opptyIdSet.add(dfQuote.Opportunity__c);
                        dfQuoteIdToOpptyIdMap.put(dfQuote.Id, dfQuote.Opportunity__c);
                    }
                }

                // Query Product Basket to get Product Basket Ids
                if (!opptyIdSet.isEmpty()) {
                    prodBasketList = [SELECT cscfga__Opportunity__c
                    FROM   cscfga__Product_Basket__c
                    WHERE  cscfga__Opportunity__c IN :opptyIdSet];

                    if (!prodBasketList.isEmpty()) {
                        for (cscfga__Product_Basket__c prodBasket : prodBasketList) {
                            prodBasketIdSet.add(prodBasket.Id);
                            opptyIdToProdBasketIdMap.put(prodBasket.cscfga__Opportunity__c, prodBasket.Id);
                        }
                    }
                }

                // Query Product Configuration to get charges
                if (!prodBasketIdSet.isEmpty()) {
                    prodConfigList = [SELECT cscfga__Product_Basket__c,
                            cscfga__Recurring_Charge__c,
                            cscfga__One_Off_Charge__c
                    FROM   cscfga__Product_Configuration__c
                    WHERE  Name = 'Direct Fibre - Product Charges'
                    AND    cscfga__Product_Basket__c IN :prodBasketIdSet];

                    if (!prodConfigList.isEmpty()) {
                        for (cscfga__Product_Configuration__c prodConfig : prodConfigList) {
                            prodBasketIdToProdConfigIdMap.put(prodConfig.cscfga__Product_Basket__c, prodConfig.Id);
                            prodConfigIdToRecurringCostMap.put(prodConfig.Id, prodConfig.cscfga__Recurring_Charge__c);
                            prodConfigIdToNonRecurringCostMap.put(prodConfig.Id, prodConfig.cscfga__One_Off_Charge__c);
                        }
                    }
                }

                // Build recurringCostsMap - using all maps/data we have accumulated
                if (!prodBasketIdToProdConfigIdMap.keySet().isEmpty()) {
                    // Loop thru dfQuote ids list and traverse thru maps to map dfQuoteId to recurring costs
                    for (String dfQuoteId : dfQuoteIdToOpptyIdMap.keySet()) {
                        // Get opptyId
                        String opptyId = dfQuoteIdToOpptyIdMap.get(dfQuoteId);

                        // Get Product Basket Id
                        String prodBasketId = opptyIdToProdBasketIdMap.get(opptyId);

                        // Get Product Configuration Id
                        String prodConfigId = prodBasketIdToProdConfigIdMap.get(prodBasketId);

                        // Get Recurring Charge
                        Decimal recurringCharge = prodConfigIdToRecurringCostMap.get(prodConfigId);
                        recurringCostsMap.put(dfQuoteId, recurringCharge);

                        // Get Non Recurring Charge
                        Decimal nonRecurringCharge = prodConfigIdToNonRecurringCostMap.get(prodConfigId);
                        nonRecurringCostsMap.put(dfQuoteId, nonRecurringCharge);
                    }
                }
            }
        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    /*
        * Fetch Opportunity Bundle name for the given parent Opportunity Bundle Id
        * Parameters : Opportunity Bundle Id.
        * @Return : Returns Opportunity Bundle name
    */
    @AuraEnabled
    public static String getOppBundleName(String oppBundleId) {
        String oppName;
        List<DF_Opportunity_Bundle__c> bundleList = new List<DF_Opportunity_Bundle__c>();
        if(String.isNotEmpty(oppBundleId)){
            bundleList = [SELECT Id, Name, Opportunity_Bundle_Name__c FROM DF_Opportunity_Bundle__c where
                    Id = :oppBundleId ];
            if(!bundleList.isEmpty()){
                oppName = bundleList.get(0) != null ? bundleList.get(0).Opportunity_Bundle_Name__c : null;
            }
        }
        return oppName;
    }

        /*
        * Fetch Opportunity Bundle Id for the given parent Opportunity Bundle Name
        * Parameters : Opportunity Bundle Name.
        * @Return : Returns Opportunity Bundle id
    */
    @AuraEnabled
    public static String getOppBundleId(String oppBundleName) {
        String oppId;
        List<DF_Opportunity_Bundle__c> bundleList = new List<DF_Opportunity_Bundle__c>();
        if(String.isNotEmpty(oppBundleName)){
            bundleList = [SELECT Id, Name, Opportunity_Bundle_Name__c FROM DF_Opportunity_Bundle__c where
                    Opportunity_Bundle_Name__c = :oppBundleName ];
            if(!bundleList.isEmpty()){
                oppId = bundleList.get(0) != null ? bundleList.get(0).Id : null;
            }
        }
        return oppId;
    }

            /*
        * Fetch Opportunity Bundle Id for the given parent Quote Name
        * Parameters : DF Quote Name.
        * @Return : Returns Opportunity Bundle id
    */
    @AuraEnabled
    public static String getOppBundleIdfromQuote(String quoteName) {
        String oppId;
        List<DF_Quote__c> quoteList = new List<DF_Quote__c>();
        if(String.isNotEmpty(quoteName)){
            quoteList = [SELECT Id, Quote_Name__c, Opportunity_Bundle__c FROM DF_Quote__c WHERE Quote_Name__c = :quoteName];
            if(!quoteList.isEmpty()){
                oppId = quoteList.get(0) != null ? quoteList.get(0).Opportunity_Bundle__c : null;
            }
        }
        return oppId;
    }

     @AuraEnabled
    public static String createNS_Order(String nsQuoteId){
        String retVal;



        if(nsQuoteId != null && nsQuoteId != ''){
            DF_Quote__c nsQuoteRec= [SELECT Id, Opportunity_Bundle__c, Order_GUID__c FROM DF_Quote__c WHERE Id =:nsQuoteId  LIMIT 1];
            DF_Order__c[] existingDF_Order = [SELECT Id  FROM DF_Order__c WHERE DF_Quote__c =: nsQuoteId];
            if(existingDF_Order.size() > 0){
                    System.debug('!!!!! createNS_Order existing IN IF' +existingDF_Order);
                    retVal = existingDF_Order[0].Id;
                }
                else{
                    String orderRTId = SF_CS_API_Util.getRecordType(SF_Constants.NBN_SELECT_NAME, SF_Constants.ORDER_OBJECT);
                    DF_Order__c order = new DF_Order__c(DF_Quote__c = nsQuoteId, Opportunity_Bundle__c = nsQuoteRec.Opportunity_Bundle__c, recordTypeId = orderRTId);

                    insert order;
                    System.debug('!!!!! createNS_Order New IN IF' +order);
                    nsQuoteRec.Order_GUID__c= NS_SF_Utils.generateGUID();
                    Database.update(nsQuoteRec);
                    retVal = order.Id;
                }
                return retVal;
        }
        return 'ERROR';
    }


    private static final String BUNDLE_ID = 'NSB-';
    private static final String QUOTE_ID = 'NSQ-';

    /*
    * Fetches order records for the given search parameters
    * Parameters : searchTerm, serachDate, searchStatus, isTriggerBySearch
    * @Return : Returns a serialized String of list of DF Order records
    */
    @AuraEnabled
    public static String getRecords(String searchTerm, Boolean isTriggerBySearch) {
        System.debug('PPPP Search Term value is : ' + searchTerm);
        System.debug('PPPP isTriggerBySearch : ' + isTriggerBySearch);

        String parentOppBundleId;
        String orderSummaryData;

        if(String.isNotBlank(searchTerm) && isTriggerBySearch){
            if(searchTerm.startsWithIgnoreCase(BUNDLE_ID)){
                parentOppBundleId = getOppBundleId(searchTerm);
                System.debug('parentOppBundleId'+parentOppBundleId);
            } else if(searchTerm.startsWithIgnoreCase(QUOTE_ID)) {
                parentOppBundleId = getOppBundleIdfromQuote(searchTerm);
                System.debug('parentOppBundleId'+parentOppBundleId);
            }
        }

        try {
            orderSummaryData = getOrderSummaryData(parentOppBundleId);
        }
        catch(Exception e) {
            throw new CustomException(e.getMessage());
        }
        return orderSummaryData;
    }


}