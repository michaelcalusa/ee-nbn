@IsTest
private class NS_SF_QuoteController_Test {

    @IsTest static void test_getQuoteRecords() {
        // Setup data                      
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        /* Account acct = SF_TestData.createAccount('My account');
         insert acct;
 
      Create OpptyBundle
         DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
         insert opptyBundle;*/

        Account acc = SF_TestData.createAccount('My account');
        insert acc;
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acc.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = SF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = SF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuote1.Location_Id__c = 'LOC111111111111';
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;

        // Create Product Definition
        cscfga__Product_Definition__c prodDef = SF_TestData.buildProductDefinition('Direct Fibre - Product Charges', '');
        insert prodDef;

        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig = SF_TestData.buildProductConfig(prodDef.Id);
        prodConfig.Name = 'Direct Fibre - Product Charges';
        prodConfig.cscfga__Product_Basket__c = prodBasket.Id;
        insert prodConfig;


        Test.startTest();

        List<DF_Quote__c> dfQuoteList2 = NS_SF_QuoteController.getQuoteRecords(opptyBundle.Id);

        Test.stopTest();

        // Assertions
        system.AssertEquals(false, dfQuoteList2.isEmpty());
    }

    @IsTest static void test_buildDFQuoteIdSet() {
        // Setup data                      
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = SF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = SF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = SF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;

        // Create Product Definition
        cscfga__Product_Definition__c prodDef = SF_TestData.buildProductDefinition('Direct Fibre - Product Charges', '');
        insert prodDef;

        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig = SF_TestData.buildProductConfig(prodDef.Id);
        prodConfig.Name = 'Direct Fibre - Product Charges';
        prodConfig.cscfga__Product_Basket__c = prodBasket.Id;
        insert prodConfig;

        Test.startTest();

        Set<String> dfQuoteIdSet = NS_SF_QuoteController.buildDFQuoteIdSet(dfQuoteList);

        Test.stopTest();

        // Assertions
        system.AssertEquals(false, dfQuoteIdSet.isEmpty());
    }

    @IsTest static void test_getOrderSummaryData() {
        // Setup data           
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = SF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = SF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = SF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;

        // Create Product Definition
        cscfga__Product_Definition__c prodDef = SF_TestData.buildProductDefinition('Direct Fibre - Product Charges', '');
        insert prodDef;

        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig = SF_TestData.buildProductConfig(prodDef.Id);
        prodConfig.Name = 'Direct Fibre - Product Charges';
        prodConfig.cscfga__Product_Basket__c = prodBasket.Id;
        insert prodConfig;

        RecordingStubProvider stubProvider = new RecordingStubProvider(NS_WhiteListQuoteUtil.class);
        ArgumentCaptor soListCaptor = new GenericObjectArgumentCaptor();
        ArgumentMatcher fieldNameStringMatcher = new StringArgumentMatcher('Location_Id__c');
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>{
                soListCaptor, fieldNameStringMatcher
        };

        stubProvider.given('getEbtMap').when(argMatcherList).thenReturn(new Map<String, String>());
        ObjectFactory.setStubProvider(NS_WhiteListQuoteUtil.class, stubProvider);

        Test.startTest();

        String serializedList = NS_SF_QuoteController.getOrderSummaryData(opptyBundle.Id);

        Test.stopTest();

        // Assertions
        system.AssertEquals(false, String.isEmpty(serializedList));

        stubProvider.verify('getEbtMap', 1, argMatcherList);
        List<DF_Quote__c> capturedSoList = (List<DF_Quote__c>) soListCaptor.getCaptured();
        Assert.equals(1, capturedSoList.size());
        Assert.equals('LOC111111111111', ((DF_Quote__c) capturedSoList.get(0)).Location_Id__c);
    }

    @IsTest private static void shouldSetOwnOrderFalseAndHasOrderFalseWhenOrderIsCancelledAndOrderDoesNotBelongToTheBundle() {
        User user = createUserAndAssignPermission();

        System.runAs(user) {

            DF_Opportunity_Bundle__c bundleWithCancelledOrder = createOpportunityBundle(user.Account);
            // There is an existing order but it is cancelled
            createQuoteAndOrder('LOC000000000001', 'Cancelled', bundleWithCancelledOrder);

            DF_Opportunity_Bundle__c myBundle = createOpportunityBundle(user.Account);
            createDfQuote('LOC000000000001', myBundle);

            Test.startTest();

            List<NS_SF_OrderSummaryData> nsOrderSummary = (List<NS_SF_OrderSummaryData>) JSON.deserialize(NS_SF_QuoteController.getOrderSummaryData(myBundle.Id), List<NS_SF_OrderSummaryData>.class);

            Test.stopTest();

            Assert.equals(1, nsOrderSummary.size());

            NS_SF_OrderSummaryData nsOrderData = nsOrderSummary.get(0);
            Assert.equals(false, nsOrderData.isOwnerOrder);
            Assert.equals(false, nsOrderData.hasOrder);
            Assert.equals('LOC000000000001', nsOrderData.locId);
            Assert.equals('', nsOrderData.status);
        }
    }

    @IsTest private static void shouldSetOwnOrderTrueAndHasOrderFalseWhenOrderIsCancelledAndOrderBelongsToTheBundle() {
        User user = createUserAndAssignPermission();

        System.runAs(user) {

            DF_Opportunity_Bundle__c bundleWithCancelledOrder = createOpportunityBundle(user.Account);
            // There is an existing order but it is cancelled
            createQuoteAndOrder('LOC000000000001', 'Cancelled', bundleWithCancelledOrder);

            Test.startTest();

            List<NS_SF_OrderSummaryData> nsOrderSummary = (List<NS_SF_OrderSummaryData>) JSON.deserialize(NS_SF_QuoteController.getOrderSummaryData(bundleWithCancelledOrder.Id), List<NS_SF_OrderSummaryData>.class);

            Test.stopTest();

            Assert.equals(1, nsOrderSummary.size());

            NS_SF_OrderSummaryData nsOrderData = nsOrderSummary.get(0);
            Assert.equals(true, nsOrderData.isOwnerOrder);
            Assert.equals(false, nsOrderData.hasOrder);
            Assert.equals('LOC000000000001', nsOrderData.locId);
            Assert.equals('Cancelled', nsOrderData.status);
        }
    }


    @IsTest private static void shouldSetOwnOrderFalseAndHasOrderFalseWhenThereIsNoExistingOrder() {
        User user = createUserAndAssignPermission();

        System.runAs(user) {
            DF_Opportunity_Bundle__c myOpportunityBundle = createOpportunityBundle(user.Account);

            createDfQuote('LOC000000000002', myOpportunityBundle);

            Test.startTest();

            List<NS_SF_OrderSummaryData> nsOrderSummary = (List<NS_SF_OrderSummaryData>) JSON.deserialize(NS_SF_QuoteController.getOrderSummaryData(myOpportunityBundle.Id), List<NS_SF_OrderSummaryData>.class);

            Test.stopTest();

            Assert.equals(1, nsOrderSummary.size());

            NS_SF_OrderSummaryData nsOrderData = nsOrderSummary.get(0);
            Assert.equals(false, nsOrderData.isOwnerOrder);
            Assert.equals(false, nsOrderData.hasOrder);
            Assert.equals('LOC000000000002', nsOrderData.locId);
            Assert.equals('', nsOrderData.status);
        }
    }

    @IsTest private static void shouldSetOwnOrderTrueAndHasOrderFalseWhenThereIsInFlightOrderOwnedByThisAccessSeekerInTheSameBundle() {
        User user = createUserAndAssignPermission();

        System.runAs(user) {
            DF_Opportunity_Bundle__c myOpportunityBundle = createOpportunityBundle(user.Account);

            createQuoteAndOrder('LOC000000000003', 'In Progress', myOpportunityBundle);

            Test.startTest();

            List<NS_SF_OrderSummaryData> nsOrderSummary = (List<NS_SF_OrderSummaryData>) JSON.deserialize(NS_SF_QuoteController.getOrderSummaryData(myOpportunityBundle.Id), List<NS_SF_OrderSummaryData>.class);

            Test.stopTest();

            Assert.equals(1, nsOrderSummary.size());

            NS_SF_OrderSummaryData nsOrderData = nsOrderSummary.get(0);
            Assert.equals(true, nsOrderData.isOwnerOrder);
            Assert.equals(false, nsOrderData.hasOrder);
            Assert.equals('LOC000000000003', nsOrderData.locId);
            Assert.equals('In Progress', nsOrderData.status);
        }
    }

    @IsTest private static void shouldSetOwnOrderTrueAndHasOrderFalseWhenThereIsInFlightOrderOwnedByThisAccessSeekerInAnotherBundle() {
        User user = createUserAndAssignPermission();

        System.runAs(user) {
            DF_Opportunity_Bundle__c myOpportunityBundle = createOpportunityBundle(user.Account);
            createDfQuote('LOC000000000003', myOpportunityBundle);

            DF_Opportunity_Bundle__c anotherBundle = createOpportunityBundle(user.Account);
            createQuoteAndOrder('LOC000000000003', 'In Progress', anotherBundle);

            Test.startTest();

            List<NS_SF_OrderSummaryData> nsOrderSummary = (List<NS_SF_OrderSummaryData>) JSON.deserialize(NS_SF_QuoteController.getOrderSummaryData(myOpportunityBundle.Id), List<NS_SF_OrderSummaryData>.class);

            Test.stopTest();

            Assert.equals(1, nsOrderSummary.size());

            NS_SF_OrderSummaryData nsOrderData = nsOrderSummary.get(0);
            Assert.equals(true, nsOrderData.isOwnerOrder);
            Assert.equals(false, nsOrderData.hasOrder);
            Assert.equals('LOC000000000003', nsOrderData.locId);
            Assert.equals('In Progress', nsOrderData.status);
        }
    }


    @IsTest private static void shouldSetOwnOrderFalseAndHasOrderTrueWhenThereIsInFlightOrderOwnedByAnotherAccessSeeker() {
        User user = createUserAndAssignPermission();

        Account anotherAccount = SF_TestData.createAccount('another account');
        anotherAccount.Access_Seeker_ID__c = 'ASI123456789012';
        insert anotherAccount;

        DF_Opportunity_Bundle__c anotherOpportunityBundle = createOpportunityBundle(anotherAccount);
        createQuoteAndOrder('LOC000000000004', 'In Progress', anotherOpportunityBundle);

        System.runAs(user) {
            DF_Opportunity_Bundle__c myOpportunityBundle = createOpportunityBundle(user.Account);

            createDfQuote('LOC000000000004', myOpportunityBundle);

            Test.startTest();

            List<NS_SF_OrderSummaryData> nsOrderSummary = (List<NS_SF_OrderSummaryData>) JSON.deserialize(NS_SF_QuoteController.getOrderSummaryData(myOpportunityBundle.Id), List<NS_SF_OrderSummaryData>.class);

            Test.stopTest();

            Assert.equals(1, nsOrderSummary.size());

            NS_SF_OrderSummaryData nsOrderData = nsOrderSummary.get(0);
            Assert.equals(false, nsOrderData.isOwnerOrder);
            Assert.equals(true, nsOrderData.hasOrder);
            Assert.equals('LOC000000000004', nsOrderData.locId);
            Assert.equals('In Progress', nsOrderData.status);
        }
    }

    @IsTest private static void shouldSetOwnOrderFalseAndHasOrderFalseWhenThereIsCancelledOrderOwnedByAnotherAccessSeeker() {
        User user = createUserAndAssignPermission();

        Account anotherAccount = SF_TestData.createAccount('another account');
        anotherAccount.Access_Seeker_ID__c = 'ASI123456789012';
        insert anotherAccount;

        DF_Opportunity_Bundle__c anotherOpportunityBundle = createOpportunityBundle(anotherAccount);
        createQuoteAndOrder('LOC0000000005', 'Cancelled', anotherOpportunityBundle);

        System.runAs(user) {

            DF_Opportunity_Bundle__c myOpportunityBundle = createOpportunityBundle(user.Account);

            createDfQuote('LOC0000000005', myOpportunityBundle);

            Test.startTest();

            List<NS_SF_OrderSummaryData> nsOrderSummary = (List<NS_SF_OrderSummaryData>) JSON.deserialize(NS_SF_QuoteController.getOrderSummaryData(myOpportunityBundle.Id), List<NS_SF_OrderSummaryData>.class);

            Test.stopTest();

            Assert.equals(1, nsOrderSummary.size());

            NS_SF_OrderSummaryData nsOrderData = nsOrderSummary.get(0);
            Assert.equals(false, nsOrderData.isOwnerOrder);
            Assert.equals(false, nsOrderData.hasOrder);
            Assert.equals('LOC0000000005', nsOrderData.locId);
            Assert.equals('', nsOrderData.status);
        }
    }

    @IsTest static void test_getQuoteCosts() {
        // Setup data
        Set<String> dfQuoteIdSet = new Set<String>();
        Map<String, Decimal> recurringCostsMap = new Map<String, Decimal>();
        Map<String, Decimal> nonRecurringCostsMap = new Map<String, Decimal>();

        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        // Create Account
        Account acct = SF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = SF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        // Create Product Basket
        cscfga__Product_Basket__c prodBasket = SF_TestData.buildBasket();
        prodBasket.cscfga__Opportunity__c = oppty.Id;
        insert prodBasket;

        // Create DFQuote recs
        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;

        // Create Product Definition
        cscfga__Product_Definition__c prodDef = SF_TestData.buildProductDefinition('Direct Fibre - Product Charges', '');
        insert prodDef;

        // Create Product Configuration
        cscfga__Product_Configuration__c prodConfig = SF_TestData.buildProductConfig(prodDef.Id);
        prodConfig.Name = 'Direct Fibre - Product Charges';
        prodConfig.cscfga__Product_Basket__c = prodBasket.Id;
        insert prodConfig;

        dfQuoteIdSet.add(dfQuote1.Id);

        Test.startTest();

        NS_SF_QuoteController.getQuoteCosts(dfQuoteIdSet, recurringCostsMap, nonRecurringCostsMap);

        Test.stopTest();
    }

    @IsTest static void getOppBundleNameTest() {
        Id oppId = SF_TestService.getServiceFeasibilityRequest();

        Test.startTest();
        String oppName = NS_SF_QuoteController.getOppBundleName(oppId);
        System.assertNotEquals('', oppName);
        Test.stopTest();
    }

    @IsTest static void getOppBundleIdTest() {
        Account acc = SF_TestData.createAccount('My account');
        insert acc;
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acc.Id);
        insert opptyBundle;

        String oppName = opptyBundle.Opportunity_Bundle_Name__c;

        List<DF_Opportunity_Bundle__c> bundleList = new List<DF_Opportunity_Bundle__c>();
        bundleList = [
                SELECT Id, Name, Opportunity_Bundle_Name__c
                FROM DF_Opportunity_Bundle__c
                where
                        Opportunity_Bundle_Name__c = :oppName
        ];


        Test.startTest();
        id oppId = NS_SF_QuoteController.getOppBundleId(oppName);
        System.assertNotEquals('', oppId);
        Test.stopTest();
    }

    @IsTest static void getOppBundleIdfromQuoteTest() {

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';


        // Create Account
        Account acct = SF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = SF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        // Create DFQuote recs
        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;

        String quoteName = dfQuote1.Quote_Name__c;

        Test.startTest();
        id oppId = NS_SF_QuoteController.getOppBundleIdfromQuote(quoteName);
        System.assertNotEquals('', oppId);
        Test.stopTest();
    }

    @IsTest static void getRecordsTest() {

        String searchTerm = 'NSB-0000000374';
        String searchTerm1 = 'NSQ-0000000569';
        Boolean isTriggerBySearch = true;


        Test.startTest();

        String quoteSummarydata = NS_SF_QuoteController.getRecords(searchTerm, isTriggerBySearch);
        quoteSummarydata = NS_SF_QuoteController.getRecords(searchTerm1, isTriggerBySearch);
        System.assertNotEquals('', quoteSummarydata);
        Test.stopTest();

    }

    @IsTest static void createNS_OrderTest() {

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';


        // Create Account
        Account acct = SF_TestData.createAccount('My account');
        insert acct;

        // Create OpptyBundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create Oppty
        Opportunity oppty = SF_TestData.createOpportunity('LOC111111111111');
        oppty.Opportunity_Bundle__c = opptyBundle.Id;
        insert oppty;

        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();
        // Create DFQuote recs
        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', oppty.Id, opptyBundle.Id, 1000, 'Green');
        dfQuote1.Proceed_to_Pricing__c = true;
        dfQuoteList.add(dfQuote1);
        insert dfQuoteList;

        String quoteId = dfQuote1.id;

        Test.startTest();
        String orderId = NS_SF_QuoteController.createNS_Order(quoteId);
        System.assertNotEquals('', orderId);
        Test.stopTest();
    }

    private static void createQuoteAndOrder(String locationId, String orderStatus, DF_Opportunity_Bundle__c myOpportunityBundle) {
        DF_Quote__c dfQuote = createDfQuote(locationId, myOpportunityBundle);
        DF_Order__c dfOrder = SF_TestData.createDFOrder(dfQuote.Id, myOpportunityBundle.Id, orderStatus);
        insert dfOrder;
    }

    private static DF_Opportunity_Bundle__c createOpportunityBundle(Account acct) {
        DF_Opportunity_Bundle__c opportunityBundle = SF_TestData.createOpportunityBundle(acct.Id);
        opportunityBundle.Account__r = acct;
        insert opportunityBundle;
        return opportunityBundle;
    }

    private static DF_Quote__c createDfQuote(String locationId, DF_Opportunity_Bundle__c opportunityBundle) {
        Opportunity oppty = SF_TestData.createOpportunity(locationId);
        oppty.Opportunity_Bundle__c = opportunityBundle.Id;
        insert oppty;

        DF_Quote__c dfQuote = SF_TestData.createDFQuote('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia', '-33.840213', '151.207368', locationId, oppty.Id, opportunityBundle.Id, 1000, 'Green');
        dfQuote.Proceed_to_Pricing__c = true;
        insert dfQuote;
        return dfQuote;
    }

    private static User createUserAndAssignPermission() {
        User user = DF_TestData.createDFCommUserMyAccount();
        user = [SELECT Id, Account.Name, Account.Access_Seeker_ID__c FROM User WHERE Id = :user.Id];

        System.runAs(new User(Id = UserInfo.getUserId())) {
            PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'NS_BC_Customer_Permission'];
            insert new PermissionSetAssignment(AssigneeId = user.Id, PermissionSetId = ps.Id);
        }
        return user;
    }

}