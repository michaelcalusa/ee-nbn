/**
* Created by Gobind.Khurana on 21/05/2018.
*/

public class NS_SF_RAGController {

    static public final String RAG_GREEN = 'Green';
    static public final String RAG_AMBER = 'Amber';
    static public final String RAG_RED = 'Red';
    static public final String READY = 'Ready';
    static public final String PENDING = 'Pending';
    static public final String INPROGRESS = 'In Progress';
    public static String strBsmApprovalRequired = SF_CS_API_Util.getCustomSettingValue('BSM_APPROVAL_REQUIRED');

    @AuraEnabled
    public static void processProceedToPricing(List<String> dfQuoteIdList) {
        List<DF_Quote__c> dfQuoteToUpdateList = new List<DF_Quote__c>();

        try {
            // Build dfQuote list to update
            if (!dfQuoteIdList.isEmpty()) {
                dfQuoteToUpdateList = [
                        SELECT Proceed_To_Pricing__c
                        FROM DF_Quote__c
                        WHERE Quote_Name__c IN :dfQuoteIdList
                        AND Proceed_To_Pricing__c <> true
                ];
            }

            // Perform updates
            if (!dfQuoteToUpdateList.isEmpty()) {
                for (DF_Quote__c dfQuote : dfQuoteToUpdateList) {
                    dfQuote.Proceed_To_Pricing__c = true;
                }

                update dfQuoteToUpdateList;
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    /*
    * Fetch Opportunity Bundle name for the given parent Opportunity Bundle Id
    * Parameters : Opportunity Bundle Id.
    * @Return : Returns Opportunity Bundle name
    */
    @AuraEnabled
    public static String getOppBundleName(String oppBundleId) {
        String oppName;
        List<DF_Opportunity_Bundle__c> bundleList = new List<DF_Opportunity_Bundle__c>();
        if (String.isNotEmpty(oppBundleId)) {
            bundleList = [
                    SELECT Id, Name, Opportunity_Bundle_Name__c
                    FROM DF_Opportunity_Bundle__c
                    where
                            Id = :oppBundleId
            ];
            if (!bundleList.isEmpty()) {
                oppName = bundleList.get(0) != null ? bundleList.get(0).Opportunity_Bundle_Name__c : null;
            }
        }
        return oppName;
    }

    /*
    * Processes RAG information and serializes the result to a string
    * Parameters : Opportunity Bundle Id.
    * @Return : Serialized string for displaying in the lightning UI
    */
    // Builds data for all lists
    @AuraEnabled
    public static String getRAGData(String oppId) {

        List<DF_Quote__c> readyDFQuoteList;
        List<DF_Quote__c> pendingDFQuoteList;
        List<DF_Quote__c> inProgressDFQuoteList;
        List<SF_RAGClass> ragDataList = new List<SF_RAGClass>();

        String parentOppBundleName = getOppBundleName(oppId);
        String serializedData;

        try {
            // List 1
            readyDFQuoteList = getReadyQuoteRecords(oppId);
            // List 2
            pendingDFQuoteList = getPendingQuoteRecords(oppId);
            // List 3
            inProgressDFQuoteList = getInProgressQuoteRecords(oppId);

            // Process List 1: Ready List
            if (!readyDFQuoteList.isEmpty()) {
                NS_WhiteListQuoteUtil quoteUtil = (NS_WhiteListQuoteUtil) ObjectFactory.getInstance(NS_WhiteListQuoteUtil.class);
                Map<String, String> locIdToEBTMap = quoteUtil.getEbtMap(readyDFQuoteList, 'Location_Id__c');

                for (DF_Quote__c dfQuote : readyDFQuoteList) {
                    // Add new entry to the output list
                    SF_RAGClass rc = new SF_RAGClass(
                            dfQuote.Quote_Name__c,
                            dfQuote.Location_Id__c,
                            dfQuote.Address__c,
                            dfQuote.RAG__c,
                            dfQuote.Fibre_Build_Cost__c,
                            NS_SF_RAGController.READY);

                    rc.quoteType = dfQuote.RAG__c == 'Green' ? 'Auto' : 'Manual';
                    rc.oppBundleName = parentOppBundleName;
                    rc.Latitude = dfQuote.Latitude__c;
                    rc.Longitude = dfQuote.Longitude__c;

                    // get EBT by Loc Id
                    rc.EBT = locIdToEBTMap.get(rc.LocId);

                    ragDataList.add(rc);
                }
            }

            // Process List 2: Pending List
            if (!pendingDFQuoteList.isEmpty()) {
                for (DF_Quote__c dfQuote : pendingDFQuoteList) {
                    // Add new entry to the output list
                    SF_RAGClass rc = new SF_RAGClass(dfQuote.Quote_Name__c, dfQuote.Location_Id__c, dfQuote.Address__c, dfQuote.RAG__c, dfQuote.Fibre_Build_Cost__c, NS_SF_RAGController.PENDING);
                    rc.quoteType = dfQuote.RAG__c == 'Red' ? 'Manual' : 'Manual';
                    rc.oppBundleName = parentOppBundleName;
                    rc.Latitude = dfQuote.Latitude__c;
                    rc.Longitude = dfQuote.Longitude__c;
                    ragDataList.add(rc);
                }
            }

            // Process List 3: In Progress List
            if (!inProgressDFQuoteList.isEmpty()) {
                for (DF_Quote__c dfQuote : inProgressDFQuoteList) {
                    // Add new entry to the output list
                    //SF_RAGClass rc = new SF_RAGClass(dfQuote.Quote_Name__c, dfQuote.Location_Id__c, dfQuote.Address__c, dfQuote.RAG__c, dfQuote.Fibre_Build_Cost__c, NS_SF_RAGController.INPROGRESS);
                    SF_RAGClass rc = new SF_RAGClass(dfQuote.Quote_Name__c, dfQuote.Location_Id__c, dfQuote.Address__c, dfQuote.RAG__c, null, NS_SF_RAGController.INPROGRESS);
                    rc.quoteType = dfQuote.RAG__c == 'Green' ? 'Auto' : 'Manual';
                    rc.oppBundleName = parentOppBundleName;
                    rc.Latitude = dfQuote.Latitude__c;
                    rc.Longitude = dfQuote.Longitude__c;
                    system.debug('Quote: ' + dfQuote);
                    system.debug('rc: ' + rc);
                    ragDataList.add(rc);
                }
            }

            serializedData = JSON.serialize(ragDataList);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }

        return serializedData;
    }

    /*
    * Fetch Quote records for the given Opportunity Bundle Id
    * Parameters : Opportunity Bundle Id.
    * @Return : Returns a list of DF Quote (custom Quote object for Direct Fibre) records
    */
    // Get DF Quote records for list 1
    @TestVisible
    private static List<DF_Quote__c> getReadyQuoteRecords(String opportunityBundleId) {
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        try {
            if (String.isNotEmpty(opportunityBundleId)) {
                Set<Id> opportunityIdsInBundle = getOpportunityIdsInBundle(opportunityBundleId);

                if (!opportunityIdsInBundle.isEmpty()) {
                    Set<Id> opportunityIdsWithBasket = getOpportunityIdsWithBasket(opportunityIdsInBundle);

                    String queryCondition = new Utils_StringBuilder(' WHERE (')
                            .append(' RAG__c = \'').append(NS_SF_RAGController.RAG_GREEN).append('\'')
                            .append(' OR (RAG__c = \'').append(NS_SF_RAGController.RAG_AMBER).append('\' AND Fibre_Build_Cost__c != NULL)')
                            .append(' OR (RAG__c = \'').append(NS_SF_RAGController.RAG_RED).append('\' AND Fibre_Build_Cost__c != NULL AND Approved__c = true)')
                            .append(')')
                            .append(' AND Opportunity_Bundle__c = \'').append(opportunityBundleId).append('\'')
                            .append(' AND Opportunity__c != NULL AND Opportunity__c in :opportunityIdsWithBasket')
                            .append(getApprovedByBusinessQuery())
                            .append(' ORDER BY Name Asc')
                            .build();

                    String queryStr = SF_CS_API_Util.getQuery(new DF_Quote__c(), queryCondition);
                    dfQuoteList = Database.query(queryStr);
                }
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return dfQuoteList;
    }

    // Get DF Quote records for list 2
    public static List<DF_Quote__c> getPendingQuoteRecords(String oppId) {
        String queryCondition;
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        try {
            if (!String.isEmpty(oppId)) {
                sObject qObject = new DF_Quote__c();

                queryCondition = ' WHERE RAG__c = ' + '\'' + NS_SF_RAGController.RAG_RED + '\''
                        + ' AND Approved__c = false'
                        + ' AND Opportunity_Bundle__c = ' + '\'' + oppId + '\'' + ' ORDER BY Name Asc';
                system.debug('NS_SF_RAGController.getPendingQuoteRecords - queryCondition: ' + queryCondition);

                String queryStr = SF_CS_API_Util.getQuery(qObject, queryCondition);
                dfQuoteList = Database.query(queryStr);
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return dfQuoteList;
    }

    @TestVisible
    private static List<DF_Quote__c> getInProgressQuoteRecords(String opportunityBundleId) {
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        try {
            if (String.isNotEmpty(opportunityBundleId)) {

                Set<Id> opportunityIdsInBundle = getOpportunityIdsInBundle(opportunityBundleId);

                if (!opportunityIdsInBundle.isEmpty()) {
                    Set<Id> opportunityIdsWithBasket = getOpportunityIdsWithBasket(opportunityIdsInBundle);

                    String strBsmQuery = ' AND (Opportunity__c = NULL OR Opportunity__c NOT IN :opportunityIdsWithBasket))';
                    if ('1'.equals(strBsmApprovalRequired)) {
                        strBsmQuery = ') ';
                    }

                    String queryCondition = new Utils_StringBuilder(' WHERE (')
                            .append('(RAG__c = \'').append(NS_SF_RAGController.RAG_GREEN).append('\'')
                            .append(strBsmQuery)
                            .append(' OR (RAG__c = \'').append(NS_SF_RAGController.RAG_AMBER).append('\' AND Fibre_Build_Cost__c = NULL)')
                            .append(' OR (RAG__c = \'').append(NS_SF_RAGController.RAG_RED).append('\' AND Approved__c = true AND Fibre_Build_Cost__c = NULL))')
                            .append(' AND Opportunity_Bundle__c = \'').append(opportunityBundleId).append('\'')
                            .append(' ORDER BY Name Asc')
                            .build();

                    String queryStr = SF_CS_API_Util.getQuery(new DF_Quote__c(), queryCondition);
                    dfQuoteList = Database.query(queryStr);
                }
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return dfQuoteList;
    }

    @AuraEnabled
    public static void updateDFQuotesToApprovedAndPublishEvent(List<String> dfQuoteIdList) {
        List<DF_Quote__c> dfQuoteToUpdateList = new List<DF_Quote__c>();
        List<BusinessEvent__e> eventList = new List<BusinessEvent__e>();
        system.debug('--dfQuoteIdList--@ ' + dfQuoteIdList);
        try {
            // Build dfQuote list to update
            if (!dfQuoteIdList.isEmpty()) {
                dfQuoteToUpdateList = [
                        SELECT Approved__c,GUID__c, name, Location_Id__c,Address__c,Latitude__c,Longitude__c,
                                Opportunity__c,Opportunity_Bundle__c, Quote_Name__c,
                                Opportunity__r.Name,RAG__c,Opportunity_Bundle__r.name,Opportunity_Bundle__r.Opportunity_Bundle_Name__c,
                                Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c,
                                Fibre_Build_Cost__c,LAPI_Response__c
                        FROM DF_Quote__c
                        WHERE Quote_Name__c IN :dfQuoteIdList
                        AND Approved__c <> true
                ];
            }

            // Perform updates
            if (!dfQuoteToUpdateList.isEmpty()) {
                for (DF_Quote__c dfQuote : dfQuoteToUpdateList) {
                    dfQuote.Approved__c = true;
                    //dfQuote.RAG__c = 'Amber';
                    if (dfQuote.LAPI_Response__c != null && dfQuote.LAPI_Response__c != '') {
                        eventList.add(NS_SF_ValidationController.createBusinessEvent((SF_ServiceFeasibilityResponse) JSON.deserialize(dfQuote.LAPI_Response__c, SF_ServiceFeasibilityResponse.class),
                                dfQuote, false));
                    }
                }

                update dfQuoteToUpdateList;

                if (eventList.size() > 0) {
                    List<Database.SaveResult> results = EventBus.publish(eventList);
                    for (Database.SaveResult sr : results) {
                        if (sr.isSuccess()) {
                            System.debug('Successfully published event.');
                        } else {
                            for (Database.Error err : sr.getErrors()) {
                                System.debug('Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static Boolean hasValidPermissions() {

        Boolean hasValidPerms = false;
        hasValidPerms = NS_SF_Utils.hasValidPermissions();

        return hasValidPerms;
    }

    private static Set<Id> getOpportunityIdsInBundle(String opportunityBundleId) {
        Set<Id> oppIds = new Set<Id>();
        try {
            DF_Opportunity_Bundle__c bundle = [SELECT Id, (SELECT Id FROM Opportunities__r) FROM DF_Opportunity_Bundle__c WHERE Id = :opportunityBundleId];
            for (Opportunity opp : bundle.Opportunities__r) {
                oppIds.add(opp.Id);
            }
        } catch (Exception e) {
            System.debug(System.LoggingLevel.WARN, 'Failed to get opportunities by bundle ID ' + opportunityBundleId + '. Error:\n' + e.getMessage());
        }
        return oppIds;
    }

    private static String getApprovedByBusinessQuery() {
        String strBsmQuery = ' ';
        if ('1'.equals(strBsmApprovalRequired)) {
            strBsmQuery = ' AND ( Appian_Approval_Required__c = \'' + strBsmApprovalRequired + '\' AND Appian_Decision__c = \'Approve\') ';
        }
        return strBsmQuery;
    }

    private static Set<Id> getOpportunityIdsWithBasket(Set<Id> opportunityIdsInBundle) {
        Set<Id> opportunityIdsWithBasket = new Set<Id>();
        for (cscfga__Product_Basket__c basket : [SELECT cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c IN :opportunityIdsInBundle]) {
            opportunityIdsWithBasket.add(basket.cscfga__Opportunity__c);
        }
        return opportunityIdsWithBasket;
    }

}