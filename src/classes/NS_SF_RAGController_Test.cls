@isTest
private class NS_SF_RAGController_Test {

    /**
    * Tests getOppBundleName method
    */
    @isTest static void getOppBundleNameTest() {
        Id oppId = SF_TestService.getServiceFeasibilityRequest();

        Test.startTest();
        String oppName = NS_SF_RAGController.getOppBundleName(oppId);
        System.assertNotEquals('', oppName);
        Test.stopTest();
    }

    @isTest static void test_getRAGData() {
        // Setup data               
        List<DF_Quote__c> dfQuoteList = new List<DF_Quote__c>();

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        Account acc = SF_TestData.createAccount('My account');
        insert acc;

        createSharingGroup(acc);

        DF_Opportunity_Bundle__c oppBundle = SF_TestData.createOpportunityBundle(acc.Id);
        insert oppBundle;

        Opportunity opp = createOpportunity('Opp 1', acc, oppBundle);
        insert opp;
        cscfga__Product_Basket__c csBasket = SF_TestData.buildBasketWithOpportnity(opp.Id);
        Database.insert(csBasket);
        // Create Ready list recs
        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', opp.Id, oppBundle.Id, null, 'Green');
        DF_Quote__c dfQuote2 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111112', null, oppBundle.Id, 1000, 'Amber');

        // Create Pending list recs
        DF_Quote__c dfQuote3 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111113', null, oppBundle.Id, null, 'Red');
        DF_Quote__c dfQuote4 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111114', null, oppBundle.Id, null, 'Red');

        // Create InProgress list recs
        DF_Quote__c dfQuote5 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111115', null, oppBundle.Id, null, 'Amber');
        DF_Quote__c dfQuote6 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111116', null, oppBundle.Id, null, 'Red');
        dfQuote4.Approved__c = true;

        dfQuoteList.add(dfQuote1);
        dfQuoteList.add(dfQuote2);
        dfQuoteList.add(dfQuote3);
        dfQuoteList.add(dfQuote4);
        dfQuoteList.add(dfQuote5);
        dfQuoteList.add(dfQuote6);
        insert dfQuoteList;

        RecordingStubProvider stubProvider = new RecordingStubProvider(NS_WhiteListQuoteUtil.class);
        ArgumentCaptor soListCaptor = new GenericObjectArgumentCaptor();
        ArgumentMatcher fieldNameStringMatcher = new StringArgumentMatcher('Location_Id__c');
        List<ArgumentMatcher> argMatcherList = new List<ArgumentMatcher>{
                soListCaptor, fieldNameStringMatcher
        };

        stubProvider.given('getEbtMap').when(argMatcherList).thenReturn(new Map<String, String>());
        ObjectFactory.setStubProvider(NS_WhiteListQuoteUtil.class, stubProvider);

        Test.startTest();

        String serializedList = NS_SF_RAGController.getRAGData(oppBundle.Id);

        Test.stopTest();

        stubProvider.verify('getEbtMap', 1, argMatcherList);
        List<DF_Quote__c> capturedSoList = (List<DF_Quote__c>) soListCaptor.getCaptured();
        Assert.equals(1, capturedSoList.size());
        Assert.equals('LOC111111111111', ((DF_Quote__c) capturedSoList.get(0)).Location_Id__c);

        // Assertions
        system.AssertEquals(false, String.isEmpty(serializedList));
    }

    @isTest static void test_updateDFQuotesToApprovedAndPublishEvent() {
        // Setup data               
        List<String> dfQuoteIdList = new List<String>();

        String locId = 'LOC111111111111';
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        Decimal cost = 5555.55;
        String rag = 'Red';

        Account acc = SF_TestData.createAccount('My account');
        insert acc;
        DF_Opportunity_Bundle__c oppBundle = SF_TestData.createOpportunityBundle(acc.Id);
        insert oppBundle;

        Opportunity opp = SF_TestData.createOpportunity('Opp 1');
        insert opp;

        // Generate mock response
        List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp2 = SF_TestData.buildResponseString('LOC000002696724', '42.78931', '147.05578', addressList, 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);

        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, locId, opp.Id, oppBundle.Id, cost, rag, resp2);
        insert dfQuote1;

        dfQuote1 = [
                SELECT Quote_Name__c
                FROM DF_Quote__c
                WHERE Id = :dfQuote1.Id
        ];

        dfQuoteIdList.add(dfQuote1.Quote_Name__c);

        test.startTest();

        NS_SF_RAGController.updateDFQuotesToApprovedAndPublishEvent(dfQuoteIdList);

        test.stopTest();

        // Assertions
        List<DF_Quote__c> existingDFQuoteList = [
                SELECT Id
                FROM DF_Quote__c
        ];

        system.AssertEquals(false, existingDFQuoteList.isEmpty());
    }

    @isTest static void test_hasValidPermissions() {
        // Setup data
        Boolean hasValidPerms = false;
        User nbnUser1 = SF_TestData.createJitUser('AS00000099', 'Access Seeker nbnSelect Feasibility', 'nbnRsp1');
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];

        test.startTest();
        System.runAs(rspUser1) {
            hasValidPerms = NS_SF_RAGController.hasValidPermissions();
        }
        test.stopTest();

        // Assertions
        system.AssertEquals(false, hasValidPerms);
    }

    @isTest static void test_processProceedToPricing() {
        // Setup data               
        List<String> dfQuoteIdList = new List<String>();

        String locId = 'LOC111111111111';
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        Decimal cost = 5555.55;
        String rag = 'Red';

        Account acc = SF_TestData.createAccount('My account');
        insert acc;
        DF_Opportunity_Bundle__c oppBundle = SF_TestData.createOpportunityBundle(acc.Id);
        insert oppBundle;

        Opportunity opp = SF_TestData.createOpportunity('Opp 1');
        insert opp;

        // Generate mock response
        List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp2 = SF_TestData.buildResponseString('LOC000002696724', '42.78931', '147.05578', addressList, 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);

        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, locId, opp.Id, oppBundle.Id, cost, rag, resp2);
        insert dfQuote1;

        dfQuote1 = [
                SELECT Quote_Name__c
                FROM DF_Quote__c
                WHERE Id = :dfQuote1.Id
        ];

        dfQuoteIdList.add(dfQuote1.Quote_Name__c);

        test.startTest();

        NS_SF_RAGController.processProceedToPricing(dfQuoteIdList);

        test.stopTest();

        // Assertions
        List<DF_Quote__c> existingDFQuoteList = [
                SELECT Id
                FROM DF_Quote__c
                WHERE Proceed_To_Pricing__c = true
        ];

        system.AssertEquals(false, existingDFQuoteList.isEmpty());
    }

    @IsTest
    static void shouldGetReadyQuoteRecordsByBundleId() {
        String locId = 'LOC111111111111';
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        Decimal cost = 5555.55;
        String rag = 'Green';

        Account acc = SF_TestData.createAccount('My account');
        acc.Access_Seeker_ID__c = 'ASI500050005000';
        insert acc;

        createSharingGroup(acc);

        DF_Opportunity_Bundle__c oppBundle = SF_TestData.createOpportunityBundle(acc.Id);
        insert oppBundle;

        List<Opportunity> opportunities = new List<Opportunity>();
        Opportunity opportunity1 = createOpportunity('Opp 1', acc, oppBundle);
        Opportunity opportunity2 = createOpportunity('Opp 2', acc, oppBundle);
        opportunities.add(opportunity1);
        opportunities.add(opportunity2);
        insert opportunities;


        List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp2 = SF_TestData.buildResponseString('LOC000002696724', '42.78931', '147.05578', addressList, 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);

        List<DF_Quote__c> quotes = new List<DF_Quote__c>();
        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, locId, opportunity1.Id, oppBundle.Id, cost, rag, resp2);
        DF_Quote__c dfQuote2 = SF_TestData.createDFQuote(address, latitude, longitude, locId, opportunity2.Id, oppBundle.Id, cost, rag, resp2);
        quotes.add(dfQuote1);
        quotes.add(dfQuote2);
        insert quotes;

        cscfga__Product_Basket__c productBasket = new cscfga__Product_Basket__c();
        productBasket.cscfga__Opportunity__c = opportunity1.Id ;
        insert productBasket;

        Test.startTest();
        List<DF_Quote__c> dfQuotes = NS_SF_RAGController.getReadyQuoteRecords(oppBundle.Id);
        Test.stopTest();

        Assert.equals(1, dfQuotes.size());
        Assert.equals(dfQuotes.get(0).Id, dfQuote1.Id);

    }

    @IsTest
    static void shouldGetInProgressQuoteRecordsByBundleId() {
        String locId = 'LOC111111111111';
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        Decimal cost = 5555.55;
        String rag = 'Green';

        Account acc = SF_TestData.createAccount('My account');
        acc.Access_Seeker_ID__c = 'ASI500050005000';
        insert acc;

        Group aGroup = new Group();
        aGroup.Name = 'BSM_NS_' + acc.Access_Seeker_ID__c;
        insert aGroup;

        DF_Opportunity_Bundle__c oppBundle = SF_TestData.createOpportunityBundle(acc.Id);
        insert oppBundle;

        List<Opportunity> opportunities = new List<Opportunity>();
        Opportunity opportunity1 = createOpportunity('Opp 1', acc, oppBundle);
        Opportunity opportunity2 = createOpportunity('Opp 2', acc, oppBundle);
        opportunities.add(opportunity1);
        opportunities.add(opportunity2);
        insert opportunities;


        List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp2 = SF_TestData.buildResponseString('LOC000002696724', '42.78931', '147.05578', addressList, 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);

        List<DF_Quote__c> quotes = new List<DF_Quote__c>();
        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, locId, opportunity1.Id, oppBundle.Id, cost, rag, resp2);
        DF_Quote__c dfQuote2 = SF_TestData.createDFQuote(address, latitude, longitude, locId, opportunity2.Id, oppBundle.Id, cost, rag, resp2);
        quotes.add(dfQuote1);
        quotes.add(dfQuote2);
        insert quotes;

        cscfga__Product_Basket__c productBasket = new cscfga__Product_Basket__c();
        productBasket.cscfga__Opportunity__c = opportunity1.Id ;
        insert productBasket;

        Test.startTest();
        List<DF_Quote__c> dfQuotes = NS_SF_RAGController.getInProgressQuoteRecords(oppBundle.Id);
        Test.stopTest();

        Assert.equals(1, dfQuotes.size());
        Assert.equals(dfQuotes.get(0).Id, dfQuote2.Id);

    }

    private static Opportunity createOpportunity(String opportunityName, Account acc, DF_Opportunity_Bundle__c oppBundle) {
        Opportunity opportunity1 = SF_TestData.createOpportunity(opportunityName);
        opportunity1.AccountId = acc.Id;
        opportunity1.Opportunity_Bundle__c = oppBundle.Id;
        return opportunity1;
    }

    private static void createSharingGroup(Account acc) {
        Group aGroup = new Group();
        aGroup.Name = 'BSM_NS_' + acc.Access_Seeker_ID__c;
        insert aGroup;
    }

}