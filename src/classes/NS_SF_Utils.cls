/**
 * Created by Gobind.Khurana on 28/05/2018.
 */

public class NS_SF_Utils {
    
    public static List<Schema.PicklistEntry> returnPickList(String ObjectApi_name, String Field_name)
    {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed 
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        
        return pick_list_values;
    }
    
    @AuraEnabled
    public static Boolean hasValidPermissions() {
        String CUSTOM_PERM_NAME = 'NBN_Select_Create_Order';
        
        Boolean hasValidPerms = false;
        ID userId;
        
        Map<ID, User> validUserMap = new Map<ID, User>();
        Set<Id> permissionSetIdSet = new Set<Id>();
        
        try {
            // Get users id
            userId = UserInfo.getUserId();
            
            // Get the list of permission sets that the NBN_Select_Create_Order CustomPermission is associated with
            for (SetupEntityAccess access : [SELECT ParentId
                                             FROM   SetupEntityAccess
                                             WHERE  SetupEntityId IN
                                             (SELECT Id
                                              FROM   CustomPermission
                                              WHERE  DeveloperName = :CUSTOM_PERM_NAME)]) {
                                                  // Add the id of the permission set that the NBN_Select_Create_Order CustomPermission is associated with
                                                  permissionSetIdSet.add(access.ParentId);
                                              }
            
            if (!permissionSetIdSet.isEmpty()) {
                // Build user map of users who have a Permission Set assigned to them (that is associated to the NBN_Select_Create_Order Custom Permission)
                validUserMap = new Map<ID, User>([SELECT Id,
                                                  Username
                                                  FROM User
                                                  WHERE Id IN (SELECT AssigneeId
                                                               FROM   PermissionSetAssignment
                                                               WHERE  PermissionSetId IN :permissionSetIdSet)]);
            }
            
            if (!validUserMap.keySet().isEmpty()) {
                // Check if users id is found
                if (validUserMap.keySet().contains(userId)) {
                    hasValidPerms = true;
                }
            }
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        
        return hasValidPerms;
    }

    public static String getNSStatus(String derivedTechnology) {
        string NSstatus;
        system.debug('**derivedTechnology** in getNSStatus'+derivedTechnology);

        if (String.isNotEmpty(derivedTechnology)) {
            if ((derivedTechnology).containsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FTTP))
                NSstatus = SF_LAPI_APIServiceUtils.STATUS_INVALID_FTTP;
           else if((derivedTechnology).containsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FTTB))
               NSstatus = SF_LAPI_APIServiceUtils.STATUS_VALID;
           else if((derivedTechnology).containsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FTTC))
               NSstatus = SF_LAPI_APIServiceUtils.STATUS_VALID;
           else if((derivedTechnology).containsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FTTN))
               NSstatus = SF_LAPI_APIServiceUtils.STATUS_VALID;
            else if((derivedTechnology).containsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_HFC))
                NSstatus = SF_LAPI_APIServiceUtils.STATUS_VALID;
            else if((derivedTechnology).containsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_WIRELESS))
                NSstatus = SF_LAPI_APIServiceUtils.STATUS_VALID;
            else if((derivedTechnology).containsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_SATELLITE))
                NSstatus = SF_LAPI_APIServiceUtils.STATUS_VALID;
            else if ((derivedTechnology).containsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_UNDEFINED))
                //NSstatus = SF_LAPI_APIServiceUtils.STATUS_INVALID_UNKNOWN;
                NSstatus = SF_LAPI_APIServiceUtils.STATUS_VALID;
            else
                //NSstatus = SF_LAPI_APIServiceUtils.STATUS_INVALID_UNKNOWN;
                NSstatus = SF_LAPI_APIServiceUtils.STATUS_VALID;
        }else{
            NSstatus = SF_LAPI_APIServiceUtils.STATUS_VALID;
        }
        system.debug('**NSstatus**'+NSstatus);

        return NSstatus;
    }
    
    public static void setQuoteToAmber(DF_Quote__c dfqRec){
        dfqRec.RAG__c =SF_Constants.RAG_AMBER;//'Amber';
        dfqRec.Fibre_Build_Cost__c=null;
    }
    
    public static void setQuoteToGreen(DF_Quote__c dfqRec){
        dfqRec.RAG__c =SF_Constants.RAG_GREEN;//'Green';
        dfqRec.Fibre_Build_Cost__c=null;
    }
    
    public static void setQuoteToRed(DF_Quote__c dfqRec){
        dfqRec.RAG__c =SF_Constants.RAG_RED;//'Red';
        dfqRec.Fibre_Build_Cost__c=null;
    }
    public static String generateGUID() {       
        String kHexChars = '0123456789abcdef';
        String guId = '';
        Integer nextByte = 0;
        try {
            for (Integer i = 0; i < 16; i++) {
                if (i == 4 || i == 6 || i == 8 || i == 10) {
                    guId+= '-';
                }
    
                nextByte = (Math.round(Math.random() * 255)-128) & 255;
    
                if (i == 6) {
                    nextByte = nextByte & 15;
                    nextByte = nextByte | (4 << 4);
                }
    
                if (i == 8) {
                    nextByte = nextByte & 63;
                    nextByte = nextByte | 128;
                }

                guId+= getCharAtIndex(kHexChars, nextByte >> 4);
                guId+= getCharAtIndex(kHexChars, nextByte & 15);
            }
    
            guId = trimStringToSize(guId, 36);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return guId;
    }
    public static String getCharAtIndex(String str, Integer index) {        
        try {
            if (str == null) {
                return null;
            } 
    
            if (str.length() <= 0) {
                return str;
            } 
    
            if (index == str.length()) {
                return null;
            }   
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return str.substring(index, index + 1);
    }
    public static String trimStringToSize(String inputString, Integer maxLength) {   
        String outputString;

        try {           
            if (String.isNotEmpty(inputString) && maxLength != null && maxLength > 0) {
                if (inputString.length() > maxLength) {                                                 
                    outputString = inputString.substring(0, maxLength);
                } else {
                    outputString = inputString;
                }
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }

        return outputString;
    }
    
    public static Map<String, String> initEventIdToEmailTemplateMap(DF_Order_Settings__mdt ordSetting){
        Map<String, String> eventIdToEmailTemplateMap = new Map<String, String>();
        //ignore case
        eventIdToEmailTemplateMap.put(ordSetting.OrderAckEventCode__c.toLowerCase(), SF_Constants.TEMPLATE_ORDER_ACKNOWLEDGED);
        eventIdToEmailTemplateMap.put(ordSetting.OrderAccEventCode__c.toLowerCase(), SF_Constants.TEMPLATE_ORDER_ACCEPTED);
        eventIdToEmailTemplateMap.put(ordSetting.OrderSchEventCode__c.toLowerCase(), SF_Constants.TEMPLATE_ORDER_SCHEDULED);
        eventIdToEmailTemplateMap.put(ordSetting.OrderRspActionReqdEventCode__c.toLowerCase(), SF_Constants.TEMPLATE_ORDER_RSP_ACTION_REQUIRED);
        eventIdToEmailTemplateMap.put(ordSetting.OrderInfoReqReminderEventCode__c.toLowerCase(), SF_Constants.TEMPLATE_ORDER_INFO_REQUIRED_REMINDER);
        eventIdToEmailTemplateMap.put(ordSetting.OrderRspActionCompEventCode__c.toLowerCase(), SF_Constants.TEMPLATE_ORDER_RSP_ACTION_COMPLETED);
        eventIdToEmailTemplateMap.put(ordSetting.OrderCancelledEventCode__c.toLowerCase(), SF_Constants.TEMPLATE_ORDER_CANCELLED);
        eventIdToEmailTemplateMap.put(ordSetting.OrderConstructionCode__c.toLowerCase(), SF_Constants.TEMPLATE_ORDER_CONSTRUCTION_STARTED);
        eventIdToEmailTemplateMap.put(ordSetting.OrderNTDInstallationDateUpdateEventCode__c.toLowerCase(), SF_Constants.TEMPLATE_ORDER_APPOINTMENT_SCHEDULED);
        eventIdToEmailTemplateMap.put(ordSetting.OrderConstCompleteEventCode__c.toLowerCase(), SF_Constants.TEMPLATE_ORDER_CONSTRUCTION_COMPLETED);
        //CPST-7850 | michaelcalusa@nbnco.com.au | ability to show revised completion date in NS
        if (GlobalUtility.getReleaseToggle('NBN_Select_MR1908')) {
            eventIdToEmailTemplateMap.put(ordSetting.OrderReschedEventCode__c.toLowerCase(), SF_Constants.TEMPLATE_ORDER_RESCHEDULED);
            eventIdToEmailTemplateMap.put(ordSetting.OrderOnSchedEventCode__c.toLowerCase(), SF_Constants.TEMPLATE_ORDER_ON_SCHEDULE);
        }
        return eventIdToEmailTemplateMap;
    }
}