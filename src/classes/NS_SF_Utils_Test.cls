@isTest
public class NS_SF_Utils_Test {
    static testMethod void testGetNSStatus(){
        String testResult1, testResult2, testResult3, testResult4, testResult5, testResult6, testResult7, testResult8, testResult9;
        Test.startTest();
            testResult1 = NS_SF_Utils.getNSStatus(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FTTP);
            testResult2 = NS_SF_Utils.getNSStatus(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FTTB);
            testResult3 = NS_SF_Utils.getNSStatus(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FTTC);
        
            testResult4 = NS_SF_Utils.getNSStatus(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FTTN);
            testResult5 = NS_SF_Utils.getNSStatus(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_HFC);
            testResult6 = NS_SF_Utils.getNSStatus(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_WIRELESS);
            testResult7 = NS_SF_Utils.getNSStatus(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_SATELLITE);
            testResult8 = NS_SF_Utils.getNSStatus(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_UNDEFINED);
            testResult9 = NS_SF_Utils.getNSStatus('NONE');
        Test.stopTest();
        System.assertEquals(testResult1, SF_LAPI_APIServiceUtils.STATUS_INVALID_FTTP);
        System.assertEquals(testResult2, SF_LAPI_APIServiceUtils.STATUS_VALID);
        System.assertEquals(testResult3, SF_LAPI_APIServiceUtils.STATUS_VALID);
        
        System.assertEquals(testResult4, SF_LAPI_APIServiceUtils.STATUS_VALID);
        System.assertEquals(testResult5, SF_LAPI_APIServiceUtils.STATUS_VALID);
        System.assertEquals(testResult6, SF_LAPI_APIServiceUtils.STATUS_VALID);
        System.assertEquals(testResult7, SF_LAPI_APIServiceUtils.STATUS_VALID);
        System.assertEquals(testResult8, SF_LAPI_APIServiceUtils.STATUS_VALID);
        System.assertEquals(testResult9, SF_LAPI_APIServiceUtils.STATUS_VALID);
        
    }

    @isTest static void test_hasValidPermissions() {
        // Setup data
        Boolean hasValidPerms = false;
        User testUser = SF_TestData.createDFCommUser();
        System.runAs(testUser){
            test.startTest();

            hasValidPerms = NS_SF_Utils.hasValidPermissions();

            test.stopTest();
        }
        // Assertions
        system.AssertEquals(false, hasValidPerms);
    }
    
    @isTest static void shouldGetEventIdToEmailTemplateMap(){
        DF_Order_Settings__mdt ordSetting = new DF_Order_Settings__mdt();
        
        ordSetting.OrderAckEventCode__c = 'OrderAckEventCode';
        ordSetting.OrderAccEventCode__c = 'OrderAccEventCode';
        ordSetting.OrderSchEventCode__c = 'OrderSchEventCode';
        ordSetting.OrderRspActionReqdEventCode__c = 'OrderRspActionReqdEventCode';
        ordSetting.OrderInfoReqReminderEventCode__c = 'OrderInfoReqReminderEventCode';
        ordSetting.OrderRspActionCompEventCode__c = 'OrderRspActionCompEventCode';
        ordSetting.OrderCancelledEventCode__c = 'OrderCancelledEventCode';
        ordSetting.OrderConstructionCode__c = 'OrderConstructionCode';
        ordSetting.OrderNTDInstallationDateUpdateEventCode__c = 'OrderNTDInstallationDateUpdateEventCode';
        ordSetting.OrderConstCompleteEventCode__c = 'OrderConstCompleteEventCode';
        
        Map<String, String> eventIdToEmailTemplateMap = NS_SF_Utils.initEventIdToEmailTemplateMap(ordSetting);
        
        Assert.equals('NS Order Acknowledged', eventIdToEmailTemplateMap.get('orderackeventcode'));
        Assert.equals('NS Order Accepted', eventIdToEmailTemplateMap.get('orderacceventcode'));
        Assert.equals('NS Order Scheduled', eventIdToEmailTemplateMap.get('orderscheventcode'));
        Assert.equals('NS Order RSP Action Required', eventIdToEmailTemplateMap.get('orderrspactionreqdeventcode'));
        Assert.equals('NS Order Info Required Reminder', eventIdToEmailTemplateMap.get('orderinforeqremindereventcode'));
        Assert.equals('NS Order RSP Action Completed', eventIdToEmailTemplateMap.get('orderrspactioncompeventcode'));
        Assert.equals('NS Order Cancelled', eventIdToEmailTemplateMap.get('ordercancelledeventcode'));
        Assert.equals('NS Order Construction Started', eventIdToEmailTemplateMap.get('orderconstructioncode'));
        Assert.equals('NS Order Appointment Scheduled', eventIdToEmailTemplateMap.get('orderntdinstallationdateupdateeventcode'));
        Assert.equals('NS Order Construction Complete', eventIdToEmailTemplateMap.get('orderconstcompleteeventcode'));
    }

    static testMethod void testSetQuoteToAmber(){
        String locId = 'LOC111111111111';
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        Decimal cost = 5555.55;
        String rag = 'Red';
        
        Account acc = SF_TestData.createAccount('My account');
        insert acc;
        DF_Opportunity_Bundle__c oppBundle = SF_TestData.createOpportunityBundle(acc.Id);
        insert oppBundle;

        Opportunity opp = SF_TestData.createOpportunity('Opp 1');
        insert opp;

        // Generate mock response
        List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp2;
        resp2='{"Status":"Valid","samID":"3FKN-22","RAG":null,"primaryTechnology":"HFC","Longitude":"144.920398","locLatLong":"-37.700759, 144.920398","LocId":"LOC000107887780","Latitude":"-37.700759","fsaID":"3FKN","fibreJointTypeDescription":null,"fibreJointTypeCode":"TYCO_OFDC_A4","fibreJointStatus":"INSERVICE","fibreJointLatLong":"-7.654550, 145.072173","fibreJointId":"3CBR-25-05-BJL-014","Distance":2000,"derivedTechnology":"HFC","csaId":"CSA300000000738","AddressList":["21 CHURCHILL ST GLENROY VIC 3046 Australia"],"AccessTechnology":"HFC"}';
        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, locId, opp.Id, oppBundle.Id, cost, rag, resp2);
        
        dfQuote1.RecordTypeId = Schema.SObjectType.DF_Quote__c.getRecordTypeInfosByName().get('NBN Select').getRecordTypeId();
        dfQuote1.Approved__c=false;
        insert dfQuote1;
        
        dfQuote1 = [SELECT Name, Quote_Name__c, Opportunity__c, Opportunity_Bundle__c, Opportunity__r.Name, Opportunity_Bundle__r.Opportunity_Bundle_Name__c, RAG__c
                    FROM   DF_Quote__c
                    WHERE  Id = :dfQuote1.Id];
        Test.startTest();
            NS_SF_Utils.setQuoteToAmber(dfQuote1);
        Test.stopTest();
        System.assertEquals('Amber',dfQuote1.RAG__c);
        System.assertEquals(null,dfQuote1.Fibre_Build_Cost__c);
    }
    static testMethod void testSetQuoteToGreen(){
        String locId = 'LOC111111111111';
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        Decimal cost = 5555.55;
        String rag = 'Green';
        
        Account acc = SF_TestData.createAccount('My account');
        insert acc;
        DF_Opportunity_Bundle__c oppBundle = SF_TestData.createOpportunityBundle(acc.Id);
        insert oppBundle;

        Opportunity opp = SF_TestData.createOpportunity('Opp 1');
        insert opp;

        // Generate mock response
        List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp2;
        resp2='{"Status":"Valid","samID":"3FKN-22","RAG":null,"primaryTechnology":"HFC","Longitude":"144.920398","locLatLong":"-37.700759, 144.920398","LocId":"LOC000107887780","Latitude":"-37.700759","fsaID":"3FKN","fibreJointTypeDescription":null,"fibreJointTypeCode":"TYCO_OFDC_A4","fibreJointStatus":"INSERVICE","fibreJointLatLong":"-7.654550, 145.072173","fibreJointId":"3CBR-25-05-BJL-014","Distance":2000,"derivedTechnology":"HFC","csaId":"CSA300000000738","AddressList":["21 CHURCHILL ST GLENROY VIC 3046 Australia"],"AccessTechnology":"HFC"}';
        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, locId, opp.Id, oppBundle.Id, cost, rag, resp2);
        
        dfQuote1.RecordTypeId = Schema.SObjectType.DF_Quote__c.getRecordTypeInfosByName().get('NBN Select').getRecordTypeId();
        dfQuote1.Approved__c=false;
        insert dfQuote1;
        
        dfQuote1 = [SELECT Name, Quote_Name__c, Opportunity__c, Opportunity_Bundle__c, Opportunity__r.Name, Opportunity_Bundle__r.Opportunity_Bundle_Name__c, RAG__c
                    FROM   DF_Quote__c
                    WHERE  Id = :dfQuote1.Id];
        Test.startTest();
            NS_SF_Utils.setQuoteToGreen(dfQuote1);
        Test.stopTest();
        System.assertEquals('Green',dfQuote1.RAG__c);
        System.assertEquals(null,dfQuote1.Fibre_Build_Cost__c);
    }
    static testMethod void testSetQuoteToRed(){
        String locId = 'LOC111111111111';
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        Decimal cost = 5555.55;
        String rag = 'Red';
        
        Account acc = SF_TestData.createAccount('My account');
        insert acc;
        DF_Opportunity_Bundle__c oppBundle = SF_TestData.createOpportunityBundle(acc.Id);
        insert oppBundle;

        Opportunity opp = SF_TestData.createOpportunity('Opp 1');
        insert opp;

        // Generate mock response
        List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp2;
        resp2='{"Status":"Valid","samID":"3FKN-22","RAG":null,"primaryTechnology":"HFC","Longitude":"144.920398","locLatLong":"-37.700759, 144.920398","LocId":"LOC000107887780","Latitude":"-37.700759","fsaID":"3FKN","fibreJointTypeDescription":null,"fibreJointTypeCode":"TYCO_OFDC_A4","fibreJointStatus":"INSERVICE","fibreJointLatLong":"-7.654550, 145.072173","fibreJointId":"3CBR-25-05-BJL-014","Distance":2000,"derivedTechnology":"HFC","csaId":"CSA300000000738","AddressList":["21 CHURCHILL ST GLENROY VIC 3046 Australia"],"AccessTechnology":"HFC"}';
        DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, locId, opp.Id, oppBundle.Id, cost, rag, resp2);
        
        dfQuote1.RecordTypeId = Schema.SObjectType.DF_Quote__c.getRecordTypeInfosByName().get('NBN Select').getRecordTypeId();
        dfQuote1.Approved__c=false;
        insert dfQuote1;
        
        dfQuote1 = [SELECT Name, Quote_Name__c, Opportunity__c, Opportunity_Bundle__c, Opportunity__r.Name, Opportunity_Bundle__r.Opportunity_Bundle_Name__c, RAG__c
                    FROM   DF_Quote__c
                    WHERE  Id = :dfQuote1.Id];
        Test.startTest();
            NS_SF_Utils.setQuoteToRed(dfQuote1);
        Test.stopTest();
        System.assertEquals('Red',dfQuote1.RAG__c);
        System.assertEquals(null,dfQuote1.Fibre_Build_Cost__c);
    }
    static testMethod void testReturnPickList(){
        NS_SF_Utils.returnPickList('DF_Order__c', 'Order_Sub_Status__c');
    }
}