/**
 * Created by Gobind.Khurana on 21/05/2018.
 */

public class NS_SF_ValidationController {

    public static final String PENDINGSTATUS = 'Pending';
    public static final String DUPLICATE_SITE_STATUS = 'Duplicate';
    public static final String MULTIPLE_ADDRESS_STATUS = 'Invalid - Multiple Addresses';
    public static final String MULTIPLE_ADDRESS_DISPLAY = 'Multiple Addresses Found';

    /*
        * Fetch Service Feasibility records for the given parent Opportunity Bundle Id
        * Parameters : Opportunity Bundle Id.
        * @Return : Returns a list of service feasibility records
    */
    @AuraEnabled
    public static List<DF_SF_Request__c> getSFRecords(String oppBundleId) {
        List<DF_SF_Request__c> sfRecords = new List<DF_SF_Request__c>();
        if (String.isNotEmpty(oppBundleId)) {
            sObject sfObject = new DF_SF_Request__c();
            String queryStr = SF_CS_API_Util.getQuery(sfObject, ' WHERE Opportunity_Bundle__c = ' + '\'' + oppBundleId + '\' ORDER BY Name');
            sfRecords = (List<DF_SF_Request__c>) SF_CS_API_Util.getQueryRecords(queryStr);
        }
        return sfRecords;
    }

    /**
     * mark duplicates among the valid locations
     */
    public static List<DF_SF_Request__c> markDuplicateLocIds(List<DF_SF_Request__c> lstSfr) {
        String VALID_SITESTATUS = SF_CS_API_Util.getCustomSettingValue('SF_SITESTATUS');
        Set<String> setLocId = new Set<String>();
        for (DF_SF_Request__c sfrRec : lstSfr) {
            SF_ServiceFeasibilityResponse sfResponse = (SF_ServiceFeasibilityResponse) System.JSON.deserialize(sfrRec.Response__c, SF_ServiceFeasibilityResponse.class);
            if (!setLocId.contains(sfrRec.Location_Id__c)) {
                if (VALID_SITESTATUS.equalsIgnoreCase(sfResponse.Status)) {
                    setLocId.add(sfrRec.Location_Id__c);
                }
            } else if (String.isNotBlank(sfrRec.Location_Id__c)) {
                sfResponse.Status = DUPLICATE_SITE_STATUS;
                sfrRec.Response__c = JSON.serialize(sfResponse);
            }
        }
        return lstSfr;
    }

    public static List<DF_SF_Request__c> markMultipleAddress(List<DF_SF_Request__c> lstSfr) {
        for (DF_SF_Request__c sfrRec : lstSfr) {
            SF_ServiceFeasibilityResponse sfResponse = (SF_ServiceFeasibilityResponse) System.JSON.deserialize(sfrRec.Response__c, SF_ServiceFeasibilityResponse.class);
            if (sfResponse.structuredAddressList != null && !sfResponse.structuredAddressList.isEmpty()) {
                sfResponse.Status = MULTIPLE_ADDRESS_STATUS;
                sfrRec.Response__c = JSON.serialize(sfResponse);
            }
        }
        return lstSfr;
    }

    @AuraEnabled
    public static String getSfRequestIncludingResponseAddress(String requestId) {
        try {
            DF_SF_Request__c request;

            if (String.isNotEmpty(requestId)) {
                request = [
                        SELECT Location_Id__c,
                                Latitude__c,
                                Longitude__c,
                                State__c,
                                Postcode__c,
                                Suburb_Locality__c,
                                Street_Name__c,
                                Street_Type__c,
                                Street_or_Lot_Number__c,
                                Unit_Type__c,
                                Unit_Number__c,
                                Level__c,
                                Complex_or_Site_Name__c,
                                Building_Name_in_a_Complex__c,
                                Street_Name_in_a_Complex__c,
                                Street_Type_in_a_Complex__c,
                                Street_Number_in_a_Complex__c,
                                Address__c,
                                Status__c,
                                Search_Type__c,
                                Response__c
                        FROM DF_SF_Request__c
                        WHERE Id = :requestId
                ];
            }

            SF_ServiceFeasibilityResponse sfResponse = (SF_ServiceFeasibilityResponse) System.JSON.deserialize(request.Response__c, SF_ServiceFeasibilityResponse.class);
            SF_ServiceFeasibilityResponse filteredSFResponse = new SF_ServiceFeasibilityResponse();
            filteredSFResponse.AddressList = sfResponse.AddressList;
            filteredSFResponse.structuredAddressList = sfResponse.structuredAddressList;

            request.Response__c = JSON.serialize(filteredSFResponse);
            return JSON.serialize(request);

        } catch (Exception e) {
            GlobalUtility.logMessage(DF_Constants.ERROR, DF_SF_ValidationController.class.getName(), 'getSfRequestAddress', requestId, '', '', '', e, 0);
            return '{ "failure" : true }';
        }
    }

    /*
        * Fetch Opportunity Bundle name for the given parent Opportunity Bundle Id
        * Parameters : Opportunity Bundle Id.
        * @Return : Returns Opportunity Bundle name
    */
    @AuraEnabled
    public static String getOppBundleName(String oppBundleId) {
        String oppName;
        List<DF_Opportunity_Bundle__c> bundleList = new List<DF_Opportunity_Bundle__c>();
        if (String.isNotEmpty(oppBundleId)) {
            bundleList = [
                    SELECT Id, Name, Opportunity_Bundle_Name__c
                    FROM DF_Opportunity_Bundle__c
                    where
                            Id = :oppBundleId
            ];
            if (!bundleList.isEmpty()) {
                oppName = bundleList.get(0) != null ? bundleList.get(0).Opportunity_Bundle_Name__c : null;
            }
        }
        return oppName;
    }

    /*
        * Deserialize Service Feasibility records based on DF_ServiceFeasibilityResponse class
        * Parameters : List of Service Feasibility records.
        * @Return : Returns a list of DF_ServiceFeasibilityResponse records
    */
    @AuraEnabled
    public static List<SF_ServiceFeasibilityResponse> deserializeList(List<DF_SF_Request__c> servicefeasibilityList) {
        List<SF_ServiceFeasibilityResponse> servicefeasibilityRespList = new List<SF_ServiceFeasibilityResponse>();
        System.debug('***servicefeasibilityList**: ' + servicefeasibilityList);

        if (servicefeasibilityList != null && !servicefeasibilityList.isEmpty()) {
            for (DF_SF_Request__c sfr : servicefeasibilityList) {
                SF_ServiceFeasibilityResponse response = (SF_ServiceFeasibilityResponse) System.JSON.deserialize(sfr.Response__c, SF_ServiceFeasibilityResponse.class);
                response.sfRequestId = String.valueOf(sfr.Id);
                response.sfSearchType = sfr.Search_Type__c;
                servicefeasibilityRespList.add(response);
            }
        }
        return servicefeasibilityRespList;
    }

    /*
        * Fetches site data (into a serialized String) based on the response stored in Service feasibility object
        * Parameters : Opportunity Bundle Id.
        * @Return : Serialized String for displaying in the lightning UI
    */
    @AuraEnabled
    public static String getSiteData(String oppBundleId) {
        List<DF_SF_Request__c> servicefeasibilityList = markMultipleAddress(markDuplicateLocIds(getSFRecords(oppBundleId)));
        List<SF_SiteData> siteDataList = new List<SF_SiteData>();
        String parentOppBundleName = getOppBundleName(oppBundleId);
        List<SF_ServiceFeasibilityResponse> servicefeasibilityRespList = deserializeList(servicefeasibilityList);
        System.debug('***servicefeasibilityRespList**: ' + servicefeasibilityRespList);

        if (!servicefeasibilityRespList.isEmpty()) {
            siteDataList = getSiteDataforResponse(servicefeasibilityRespList, parentOppBundleName);
        }
        String serializedData = json.serialize(siteDataList);
        return serializedData;
    }

    /*
     * Get all valid DF_ServiceFeasibilityResponse records that can proceed for RAG assessment.
     * Parameters : List of Service Feasibility records
     * @Return : List of DF_ServiceFeasibilityResponse records
    */
    @AuraEnabled
    public static List<SF_ServiceFeasibilityResponse> getValidSiteData(List<DF_SF_Request__c> servicefeasibilityList) {
        markDuplicateLocIds(servicefeasibilityList);
        List<SF_ServiceFeasibilityResponse> validSFList = new List<SF_ServiceFeasibilityResponse>();
        List<SF_ServiceFeasibilityResponse> servicefeasibilityRespList = deserializeList(servicefeasibilityList);
        String SITESTATUS = SF_CS_API_Util.getCustomSettingValue('SF_SITESTATUS');
        if (!servicefeasibilityRespList.isEmpty()) {
            for (SF_ServiceFeasibilityResponse sfr : servicefeasibilityRespList) {
                if (String.isNotEmpty(SITESTATUS)) {
                    if (SITESTATUS.equalsIgnoreCase(sfr.Status)) {
                        validSFList.add(sfr);
                    }
                }
            }
        }
        return validSFList;
    }

    /*
        * Map the response stored in Service feasibility object to SF_SiteData object. Any List of addresses is converted to a single String separated by \n to be displayed in lightning data table
        * Parameters : Instance of DF_ServiceFeasibilityResponse class.
        * @Return : List of Site data that is populated based on the response field in Service Feasibility object
    */
    @AuraEnabled
    public static List<SF_SiteData> getSiteDataforResponse(List<SF_ServiceFeasibilityResponse> servicefeasibilityList, String parentOppBundleName) {
        List<SF_SiteData> siteDataList = new List<SF_SiteData>();
        String SITESTATUS = SF_CS_API_Util.getCustomSettingValue('SF_SITESTATUS');
        //Boolean flag to determine if the list has and Valid locations
        Boolean validFlag = false;
        //check if status of any record is valid
        for (SF_ServiceFeasibilityResponse sfr : servicefeasibilityList) {
            System.debug('getSiteDataforResponse1::SFR: ' + sfr);
            if (String.isNotEmpty(SITESTATUS)) {
                if (SITESTATUS.equalsIgnoreCase(sfr.Status)) {
                    validFlag = true;
                    break;
                }
            }
        }
        //check if status of any record is pending
        for (SF_ServiceFeasibilityResponse sfr : servicefeasibilityList) {
            System.debug('getSiteDataforResponse2::SFR: ' + sfr);
            if (PENDINGSTATUS.equalsIgnoreCase(sfr.Status)) {
                validFlag = false;
                break;
            }
        }
        for (SF_ServiceFeasibilityResponse sfr : servicefeasibilityList) {
            String addColloborated = '';
            if (sfr.AddressList != null && !sfr.AddressList.isEmpty()) {
                for (String str : sfr.AddressList) {
                    if (addColloborated == '') {
                        addColloborated = str;
                    } else {
                        addColloborated = MULTIPLE_ADDRESS_DISPLAY;
                        break;
                    }
                }
            }
            SF_SiteData sd = new SF_SiteData(sfr.LocId, sfr.Latitude, sfr.Longitude, addColloborated, sfr.Status, validFlag);
            sd.oppBundleName = parentOppBundleName;
            sd.searchType = sfr.sfSearchType;
            sd.requestId = sfr.sfRequestId;
            siteDataList.add(sd);

        }
        return siteDataList;
    }

    @AuraEnabled
    public static String editByLocId(String sfRequestId, String locationId) {
        System.debug('sfRequestId=' + sfRequestId + ', locationId=' + locationId);

        try {
            // fetch original request
            DF_SF_Request__c request = SF_LAPI_APIServiceUtils.getSFReqDetails(sfRequestId);
            // clear old info
            clearRequestInfo(request);
            // set new info
            setNewLocIdInfo(request, locationId);

            return searchAndUpdateResult(request);
        } catch (Exception e) {
            //log error and update status
            throw new CustomException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String editByLatLong(String sfRequestId, String latitude, String longitude) {
        System.debug('sfRequestId=' + sfRequestId + ', latitude=' + latitude + ', longitude=' + longitude);

        try {
            // fetch original request
            DF_SF_Request__c request = SF_LAPI_APIServiceUtils.getSFReqDetails(sfRequestId);
            // clear old info
            clearRequestInfo(request);
            // set new info
            setNewLatLongInfo(request, latitude, longitude);

            return searchAndUpdateResult(request);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String editByAddress(String sfRequestId, String state, String postcode, String suburbLocality, String streetName, String streetType,
            String streetLotNumber, String unitType, String unitNumber, String level, String complexSiteName,
            String complexBuildingName, String complexStreetName, String complexStreetType, String complexStreetNumber) {
        System.debug('sfRequestId=' + sfRequestId + ', state=' + state + ', postcode=' + postcode + ', suburbLocality=' + suburbLocality + ', streetName=' + streetName + ', streetType=' + streetType
                + ', streetLotNumber=' + streetLotNumber + ', unitType=' + unitType + ', unitNumber=' + unitNumber + ', level=' + level + ', complexSiteName=' + complexSiteName
                + ', complexBuildingName=' + complexBuildingName + ', complexStreetName=' + complexStreetName + ', complexStreetType=' + complexStreetType + ', complexStreetNumber=' + complexStreetNumber);

        try {
            // fetch original request
            DF_SF_Request__c request = SF_LAPI_APIServiceUtils.getSFReqDetails(sfRequestId);
            // clear old info
            clearRequestInfo(request);
            // set new info
            setNewAddressInfo(request, state, postcode, suburbLocality, streetName, streetType,
                    streetLotNumber, unitType, unitNumber, level, complexSiteName,
                    complexBuildingName, complexStreetName, complexStreetType, complexStreetNumber);

            return searchAndUpdateResult(request);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    @TestVisible
    private static void clearRequestInfo(DF_SF_Request__c request) {
        request.Search_Type__c = '';

        request.Location_Id__c = '';

        request.Latitude__c = '';
        request.Longitude__c = '';

        request.State__c = '';
        request.Postcode__c = '';
        request.Suburb_Locality__c = '';
        request.Street_Name__c = '';
        request.Street_Type__c = '';
        request.Street_or_Lot_Number__c = '';
        request.Unit_Type__c = '';
        request.Unit_Number__c = '';
        request.Level__c = '';
        request.Complex_or_Site_Name__c = '';
        request.Building_Name_in_a_Complex__c = '';
        request.Street_Name_in_a_Complex__c = '';
        request.Street_Type_in_a_Complex__c = '';
        request.Street_Number_in_a_Complex__c = '';

        request.Status__c = '';
        request.Response__c = '';
    }

    @TestVisible
    private static void setNewLocIdInfo(DF_SF_Request__c request, String locationId) {

        SF_ServiceFeasibilityResponse sfResponse = new SF_ServiceFeasibilityResponse(
                locationId,
                null,
                null,
                new List<String>(),
                null,
                null,
                SF_LAPI_APIServiceUtils.STATUS_PENDING,
                null,
                null,
                null,
                null,
                null,
                null,
                null);

        request.Search_Type__c = SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID;
        request.Location_Id__c = locationId;
        request.Status__c = SF_LAPI_APIServiceUtils.STATUS_PENDING;
        request.Response__c = JSON.serialize(sfResponse);
    }

    @TestVisible
    private static void setNewLatLongInfo(DF_SF_Request__c request, String latitude, String longitude) {

        SF_ServiceFeasibilityResponse sfResponse = new SF_ServiceFeasibilityResponse(
                null,
                latitude,
                longitude,
                new List<String>(),
                null,
                null,
                SF_LAPI_APIServiceUtils.STATUS_PENDING,
                null,
                null,
                null,
                null,
                null,
                null,
                null);

        // set new info
        request.Search_Type__c = SF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG;
        request.Latitude__c = latitude;
        request.Longitude__c = longitude;
        request.Status__c = SF_LAPI_APIServiceUtils.STATUS_PENDING;
        request.Response__c = JSON.serialize(sfResponse);
    }

    @TestVisible
    private static void setNewAddressInfo(DF_SF_Request__c request, String state, String postcode, String suburbLocality, String streetName, String streetType,
            String streetLotNumber, String unitType, String unitNumber, String level, String complexSiteName,
            String complexBuildingName, String complexStreetName, String complexStreetType, String complexStreetNumber) {
        // set new info
        request.Search_Type__c = SF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS;
        request.State__c = state;
        request.Postcode__c = postcode;
        request.Suburb_Locality__c = suburbLocality;
        request.Street_Name__c = streetName;
        request.Street_Type__c = streetType;
        request.Street_or_Lot_Number__c = streetLotNumber;
        request.Unit_Type__c = unitType;
        request.Unit_Number__c = unitNumber;
        request.Level__c = level;
//        request.Complex_or_Site_Name__c = complexSiteName;
//        request.Building_Name_in_a_Complex__c = complexBuildingName;
//        request.Street_Name_in_a_Complex__c = complexStreetName;
//        request.Street_Type_in_a_Complex__c = complexStreetType;
//        request.Street_Number_in_a_Complex__c = complexStreetNumber;
        request.Status__c = SF_LAPI_APIServiceUtils.STATUS_PENDING;

        List<String> dtoAddressList = new List<String>();
        String dtoAddress = SF_LAPI_APIServiceUtils.getInputAddressFullText(request);
        if (String.isNotEmpty(dtoAddress)) {
            dtoAddressList.add(dtoAddress);
        }

        SF_ServiceFeasibilityResponse sfResponse = new SF_ServiceFeasibilityResponse(
                null,
                null,
                null,
                dtoAddressList,
                null,
                null,
                SF_LAPI_APIServiceUtils.STATUS_PENDING,
                null,
                null,
                null,
                null,
                null,
                null,
                null);

        request.Response__c = JSON.serialize(sfResponse);
    }

    @TestVisible
    private static String searchAndUpdateResult(DF_SF_Request__c request) {
        // perform CIS synchronously
        NS_CISCalloutAPIService.makeCISCallOut(request);
        update request;
        SF_ServiceFeasibilityResponse retResponse = (SF_ServiceFeasibilityResponse) JSON.deserialize(request.Response__c, SF_ServiceFeasibilityResponse.class);
        if (NS_CISCalloutAPIUtils.STATUS_VALID == retResponse.Status) {
            return getSingleSiteDataWithoutBundleName(request);
        } else {
            return '{ "failure" : true }';
        }
    }

    @TestVisible
    private static String getSingleSiteDataWithoutBundleName(DF_SF_Request__c sfRequest) {
        List<SF_ServiceFeasibilityResponse> servicefeasibilityRespList = deserializeList(new List<DF_SF_Request__c>{
                sfRequest
        });
        List<SF_SiteData> siteDataList = getSiteDataforResponse(servicefeasibilityRespList, '');
        return json.serialize(siteDataList.get(0));
    }

    /*
        * Once the user clicks on Submit feasibility request all valid records are processed to generate rag status.
        * Every DF_Quote record is mapped to an opportunity and all opportunity records created are mapped to a parent opportunity bundle record
        * Parameters : Opportunity Bundle Id.
        * @Return : null
    */
    @AuraEnabled
    public static void processSiteData(String oppBundleId, String productType) {
        try {
            List<DF_SF_Request__c> serviceFeasibilityList = getSFRecords(oppBundleId);
            List<SF_ServiceFeasibilityResponse> validSFList = getValidSiteData(serviceFeasibilityList);
            Id oppId = serviceFeasibilityList.isEmpty() ? null : serviceFeasibilityList.get(0) != null ? serviceFeasibilityList.get(0).Opportunity_Bundle__c : null;

            DF_Opportunity_Bundle__c oOppBundle = [SELECT Id, Account__c FROM DF_Opportunity_Bundle__c WHERE Id = :oppBundleId LIMIT 1];
            List<Object_Record_Type__mdt> mdtRecords = (List<Object_Record_Type__mdt>) SF_CS_API_Util.getCustomMetadataByType('Object_Record_Type__mdt', productType);
            String recordTypeName = mdtRecords.isEmpty() ? null : mdtRecords[0].Opportunity_Record_Type_Name__c;
            Id oppRecordTypeId = recordTypeName == null ? null : Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(recordTypeName).getRecordTypeId();

            System.debug('PPPP oppRecordTypeId: ' + oppRecordTypeId);
            List<Opportunity> oppList = new List<Opportunity>();
            Map<String, Opportunity> oppSFMap = new Map<String, Opportunity>();

            for (SF_ServiceFeasibilityResponse sr : validSFList) {
                String STAGENAME = SF_CS_API_Util.getCustomSettingValue('SF_STAGENAME');
                Opportunity oppo = new Opportunity();
                // Opportunity Name is set to the LOC ID
                oppo.Name = PENDINGSTATUS;
                oppo.StageName = String.isNotEmpty(STAGENAME) ? STAGENAME : null;
                oppo.CloseDate = (Date.today()).addDays(60);
                oppo.Opportunity_Bundle__c = oppId;
                oppo.RecordTypeId = oppRecordTypeId;
                if (oOppBundle != null) {
                    // assign the accountId and it will be used to create sharing rules
                    oppo.AccountId = oOppBundle.Account__c;
                }
                oppList.add(oppo);
                oppSFMap.put(sr.LocId, oppo);
            }
            if (!oppList.isEmpty()) {
                insert oppList;
            }

            List<BusinessEvent__e> eventList = new List<BusinessEvent__e>();
            List<DF_Quote__c> quoteList = generateRAGAndPublishEvent(validSFList, oppSFMap, oppBundleId, productType);
            if (!quoteList.isEmpty()) {
                insert quoteList;
            }
            System.debug('**quoteList**' + quoteList);

            Set<Id> qtIds = new Set<Id>();
            for (DF_Quote__c qtVal : quoteList) {
                qtIds.add(qtVal.id);
            }

            // change name of opportunity to quote name EEQ000000001
            updateOppNames(qtIds);

            String UNAVAILABLE = SF_CS_API_Util.getCustomSettingValue('SF_UNAVAILABLE');
            List<DF_Quote__c> refQuoteList = [
                    SELECT Name, GUID__c, Location_Id__c,Address__c,Latitude__c,Longitude__c,Opportunity__c,Opportunity_Bundle__c,
                            Opportunity__r.Name,RAG__c,Opportunity_Bundle__r.name,Fibre_Build_Cost__c,LAPI_Response__c, Quote_Name__c, Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c, Opportunity_Bundle__r.Opportunity_Bundle_Name__c
                    FROM DF_Quote__c
                    WHERE id in :qtIds
            ];
            List<DF_Quote__c> quotesToBeProcessedList = new List<DF_Quote__c>();
            for (DF_Quote__c qtVal : refQuoteList) {
                if (qtVal.RAG__c != UNAVAILABLE && String.isNotEmpty(qtVal.RAG__c)) {
                    eventList.add(createBusinessEvent((SF_ServiceFeasibilityResponse) JSON.deserialize(qtVal.LAPI_Response__c, SF_ServiceFeasibilityResponse.class),
                            qtVal, true));
                    quotesToBeProcessedList.add(qtVal);
                }
            }

            //Get the count to determine the list size for every enqueue job
            Integer MAX_NO_PARALLELS = Integer.valueOf(SF_CS_API_Util.getCustomSettingValue('PRODUCT_BASKET_PARALLEL_QUEUE_SIZE'));
            if (!quotesToBeProcessedList.isEmpty()) {
                List<List<DF_Quote__c>> lstWrapper = SF_CS_API_Util.spliceBy(quotesToBeProcessedList, MAX_NO_PARALLELS);
                for (List<DF_Quote__c> lquote : lstWrapper) {
                    if (!Test.isRunningTest()) {
                        // Queue jobs for creation of product baskets
                        Id jobId = System.enqueueJob(new SF_ProcessBasketQueueHandler(lquote));
                    }
                }
            }
        } catch (AsyncException e) {
            throw new CustomException(e.getMessage());
        }
    }

    /*
        * Includes the logic to generate rag status.
        * Every DF_Quote record is mapped to an opportunity and all opportunity records created to mapped to a opportunity bundle record
        * Parameters : All valid Service Feasibility records, Map of Location Id to Opportunity (that identifies the opp for every location), Opportunity Bundle Id.
        * @Return : List of Quote records
    */
    @TestVisible
    private static List<DF_Quote__c> generateRAGAndPublishEvent(List<SF_ServiceFeasibilityResponse> validSFList, Map<String, Opportunity> oppSFMap, String oppBundleId, String productType) {
        List<DF_Quote__c> quoteList = new List<DF_Quote__c>();
        Id quoteRecordTypeId = getQuoteRecordTypeId(productType);
        String queryStr = SF_CS_API_Util.getQuery(new DF_FBC_Rule__c(), ' WHERE RecordType.Name = \'NBN Select\'');
        System.debug('queryStr::' + queryStr);
        List<DF_FBC_Rule__c> ragRecords = (List<DF_FBC_Rule__c>) SF_CS_API_Util.getQueryRecords(queryStr);
        System.debug('----@@ragRecords' + ragRecords);
        Boolean isCISEnabled = SF_CS_API_Util.isCustomSettingEnabled('CIS_ENABLE_CALLOUT');

        for (SF_ServiceFeasibilityResponse serviceFeasibilityResponse : validSFList) {
            DF_Quote__c quote = new DF_Quote__c();
            quote.Location_Id__c = serviceFeasibilityResponse.LocId;
            quote.Address__c = (!serviceFeasibilityResponse.AddressList.isEmpty()) ? serviceFeasibilityResponse.AddressList.get(0) : null;
            quote.Latitude__c = serviceFeasibilityResponse.Latitude;
            quote.Longitude__c = serviceFeasibilityResponse.Longitude;
            quote.Opportunity__c = oppSFMap.get(serviceFeasibilityResponse.LocId).Id;
            quote.Opportunity_Bundle__c = oppBundleId;
            quote.GUID__c = NS_SF_Utils.generateGUID();
            quote.RecordTypeId = quoteRecordTypeId;
            serviceFeasibilityResponse.derivedTechnology = getDerivedTechnologyValue(serviceFeasibilityResponse.primaryTechnology, serviceFeasibilityResponse.AccessTechnology, serviceFeasibilityResponse.serviceType);
            quote.LAPI_Response__c = JSON.serialize(serviceFeasibilityResponse);

            if (productType.equalsIgnoreCase('NBN_SELECT')) {
                if (isCISEnabled) {
                    NS_SF_Utils.setQuoteToGreen(quote);
                    quote.Fibre_Build_Cost__c = serviceFeasibilityResponse.whiteListQuoteAmount;
                } else if (SF_CS_API_Util.isCustomSettingEnabled('NS_ENABLE_AUTO_QUOTE')) {
                    autoQuote(serviceFeasibilityResponse, ragRecords, quote);
                } else {
                    NS_SF_Utils.setQuoteToRed(quote);
                }
            }
            quoteList.add(quote);
        }
        return quoteList;
    }

    private static void autoQuote(SF_ServiceFeasibilityResponse sfr, List<DF_FBC_Rule__c> ragRecords, DF_Quote__c quote) {
        String primeTech = sfr.primaryTechnology;
        String accessTech = sfr.derivedTechnology;

        Integer maxFibreDist = sfr.Distance;
        String assetId = sfr.fibreJointId;
        String OLTExist = sfr.OLT_Exists;
        String assetType = '';
        Boolean isAuto = false;
        //asset id format: 3BRA-10-10-SMP-010
        if (String.isNotBlank(assetId)) {
            //String asstId = '3BRA-10-10-SMP-010';

            List<String> splitedItems = assetId.split('-');

            assetType = splitedItems.size() > 0 ? splitedItems[3] : '';

            System.debug('--assetType-- ' + assetType);

        }

        for (DF_FBC_Rule__c rule : ragRecords) {
            Boolean checkPrimarTech = String.isNotBlank(primeTech) ? (primeTech == rule.Primary_Technology__c) : false;
            Boolean checkAccessTech = String.isNotBlank(accessTech) ? (accessTech == rule.Access_Technology__c) : false;
            Boolean checkOLTExist = String.isNotBlank(OLTExist) ? (OLTExist == 'True') : false;
            Boolean checkMaxFibreDist = (maxFibreDist != null) ? maxFibreDist <= rule.Maximum_Distance__c : false;

            String tempAsstType = rule.Asset_Type__c;
            System.debug('--tempAsstType-- ' + tempAsstType);

            List<String> tempAssts = String.isNotBlank(tempAsstType) ? tempAsstType.split(';') : new List<String>();
            Boolean checkAssetType = false;
            for (String tempStr : tempAssts) {

                if (String.isNotBlank(assetType) && (assetType == tempStr)) {
                    checkAssetType = true;

                }
            }

            System.debug('--checkAssetType-- ' + checkAssetType);
            System.debug('--checkPrimarTech-- ' + checkPrimarTech);
            System.debug('--checkAccessTech-- ' + checkAccessTech);
            System.debug('--checkMaxFibreDist-- ' + checkMaxFibreDist);
            System.debug('--checkOLTExist-- ' + checkOLTExist);

            if (checkPrimarTech && checkAccessTech && checkMaxFibreDist && checkOLTExist && checkAssetType) {
                isAuto = true;
            }
        }
        System.debug('-----isAuto' + isAuto);
        if (isAuto) {
            NS_SF_Utils.setQuoteToGreen(quote);
        } else {
            NS_SF_Utils.setQuoteToRed(quote);
        }
    }

    private static Id getQuoteRecordTypeId(String productType) {
        List<Object_Record_Type__mdt> mdtRecords = (List<Object_Record_Type__mdt>) SF_CS_API_Util.getCustomMetadataByType('Object_Record_Type__mdt', productType);
        String recordTypeName = mdtRecords.isEmpty() ? null : mdtRecords[0].DF_Quote_Record_Type_Name__c;
        Id quoteRecordTypeId = recordTypeName == null ? null : Schema.SObjectType.DF_Quote__c.getRecordTypeInfosByDeveloperName().get(recordTypeName).getRecordTypeId();
        return quoteRecordTypeId;
    }

    public static BusinessEvent__e createBusinessEvent(SF_ServiceFeasibilityResponse sfr, DF_Quote__c qt, Boolean isNew) {
        BusinessEvent__e evt = new BusinessEvent__e();
        String mdtDevName = qt.Quote_Name__c.left(2);
        if (String.isNotEmpty(mdtDevName)) {
            List<Business_Platform_Event__mdt> mdtRecords = (List<Business_Platform_Event__mdt>) SF_CS_API_Util.getCustomMetadataByType('Business_Platform_Event__mdt', mdtDevName);
            evt.Event_Id__c = mdtRecords.isEmpty() ? null : mdtRecords[0].DF_SFC_Desktop_Assessment__c;
            evt.Source__c = mdtRecords.isEmpty() ? null : mdtRecords[0].SFToAppainSource__c;
        }
        evt.Event_Record_Id__c = qt.GUID__c;
        evt.Event_Sub_Type__c = 'NBN_SELECT';
        SF_AppainRequest.DesktopAssessmentRequest req = new SF_AppainRequest.DesktopAssessmentRequest();
        req.salesforceParentOppId = qt.Opportunity_Bundle__c;
        req.salesforceOppName = qt.Opportunity__r.Name;
        req.salesforceParentOppName = qt.Opportunity_Bundle__r.Opportunity_Bundle_Name__c;
        req.salesforceQuoteId = qt.id;
        req.salesforceQuoteName = qt.Quote_Name__c;
        req.basketId = '';
        /*OrderingOnBehalfOfRSP*/
        Boolean isCISEnabled = SF_CS_API_Util.isCustomSettingEnabled('CIS_ENABLE_CALLOUT');
        if (isCISEnabled) {
            req.cost = String.isNotBlank(String.valueOf(sfr.whiteListQuoteAmount)) ? String.valueOf(sfr.whiteListQuoteAmount) : null;
            req.estimatedBuildTimeframe = String.isNotBlank(String.valueOf(sfr.whiteListEBTInDays)) ? String.valueOf(sfr.whiteListEBTInDays) : null;
        } else {
            req.cost = String.isNotBlank(String.valueOf(qt.Fibre_Build_Cost__c)) ? String.valueOf(qt.Fibre_Build_Cost__c) : null;
        }
        req.bsmApprovalRequired = SF_CS_API_Util.getCustomSettingValue('BSM_APPROVAL_REQUIRED');
        req.accessSeekerId = qt.Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c;
        if (isNew) {
            req.salesforceStatus = (SF_Constants.RAG_STATUS_RED.equalsIgnoreCase(qt.RAG__c)) ? SF_Constants.APPAIN_DESKTOP_ASSESSMENT_STATUS_PENDING : SF_Constants.APPAIN_DESKTOP_ASSESSMENT_STATUS_IN_PROCESS;
        } else {
            req.salesforceStatus = (SF_Constants.RAG_STATUS_RED.equalsIgnoreCase(qt.RAG__c)) ? SF_Constants.APPAIN_DESKTOP_ASSESSMENT_STATUS_IN_PROCESS : SF_Constants.APPAIN_DESKTOP_ASSESSMENT_STATUS_IN_PROCESS;
        }
        req.fibreJointId = sfr.fibreJointId;
        req.fibreJointStatus = sfr.fibreJointStatus;
        req.fibreJointLatLong = sfr.fibreJointLatLong;
        req.fibreJointTypeCode = sfr.fibreJointTypeCode;
        req.locId = sfr.LocId;
        req.locLatLong = sfr.locLatLong;
        System.debug('---NS_SF_ValidationController::createBusinessEvent:locLatLong' + req.locLatLong);
        req.address = (!sfr.AddressList.isEmpty()) ? sfr.AddressList.get(0) : null;
        req.ragValue = qt.RAG__c.toUpperCase();
        req.actualDistance = String.valueOf(sfr.Distance);
        System.debug('---NS_SF_ValidationController::createBusinessEvent:Distance' + req.actualDistance);
        evt.Message_Body__c = JSON.serialize(req);
        return evt;
    }


    @AuraEnabled
    public static List<Opportunity> getOpportunities(String oppBundleId) {
        List<Opportunity> oppList = new List<Opportunity>();
        if (String.isNotEmpty(oppBundleId)) {
            oppList = [
                    SELECT Id
                    FROM Opportunity
                    WHERE Opportunity_Bundle__c = :oppBundleId
                    ORDER BY Id
            ];
        }
        return oppList;
    }

    /*
        * Get derived technology value based on Primary Technology and Access Technology Type from LAPI response.
        * Parameters : Primary Technology and Access Technology Type
        * @Return : Derived technology
    */
    public static String getDerivedTechnologyValue(String primaryTechnology, String accessTechnology, String serviceType) {
        String derivedTechnology;

        System.debug('**In getDerivedTechnologyValue***');
        System.debug('**primaryTechnology**' + primaryTechnology);
        System.debug('**accessTechnology**' + accessTechnology);
        System.debug('**serviceType**' + serviceType);

        if (String.isNotEmpty(primaryTechnology)) {
            if (primaryTechnology.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FIBRE)) {
                derivedTechnology = SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FTTP;
            } else if (primaryTechnology.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_HFC)) {
                derivedTechnology = SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_HFC;
            } else if (primaryTechnology.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_WIRELESS) || primaryTechnology.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FIXED_WIRELESS)) {
                derivedTechnology = SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_WIRELESS;
            } else if (primaryTechnology.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_SATELLITE)) {
                derivedTechnology = SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_SATELLITE;
            } else if (primaryTechnology.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_UNDEFINED) || primaryTechnology.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_NULL)) {
                derivedTechnology = SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_UNDEFINED;
            } else if (primaryTechnology.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_COPPER)) {
                if (String.isNotEmpty(serviceType)) {
                    if (serviceType.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_SERVICE_TYPE_BROWN_FTTB) || serviceType.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_SERVICE_TYPE_GREEN_FTTB)) {
                        derivedTechnology = SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FTTB;
                    } else if (serviceType.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_SERVICE_TYPE_BROWN_FTTC) || serviceType.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_SERVICE_TYPE_GREEN_FTTC)) {
                        derivedTechnology = SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FTTC;
                    } else if (serviceType.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_SERVICE_TYPE_BROWN_FTTN) || serviceType.equalsIgnoreCase(SF_Constants.SERVICE_FEASIBILITY_SERVICE_TYPE_GREEN_FTTN)) {
                        derivedTechnology = SF_Constants.SERVICE_FEASIBILITY_TECHNOLOGY_FTTN;
                    }
                }
            }
        }
        return derivedTechnology;
    }

    public static void updateOppNames(Set<Id> qtIds) {
        List<DF_Quote__c> qtOppIdNameList = [SELECT Opportunity__c, Quote_Name__c FROM DF_Quote__c WHERE Id IN:qtIds];
        Map<Id, String> oppIdQuoteNameMap = new Map<Id, String>(); //?
        Set<Id> qtOppIds = new Set<Id>(); //???
        for (DF_Quote__c qtVal : qtOppIdNameList) {
            qtOppIds.add(qtVal.Opportunity__c);
            oppIdQuoteNameMap.put(qtVal.Opportunity__c, qtVal.Quote_Name__c);
        }

        List<Opportunity> oppsToUpdate = [SELECT Id,Name FROM Opportunity WHERE Id IN:qtOppIds];
        //List<Opportunity> updateOpps = new List<Opportunity
        for (Opportunity oppToUpDate : oppsToUpdate) {
            System.debug('!!! oppIdQuoteNameMap.get(oppToUpDate.Id)' + oppIdQuoteNameMap.get(oppToUpDate.Id)); //deleteme
            oppToUpDate.Name = oppIdQuoteNameMap.get(oppToUpDate.Id);
        }
        if (!oppsToUpdate.isEmpty()) {
            update oppsToUpdate;
        }

    }
}