public with sharing class NS_SFtoAppainEventHandler implements Queueable{
    
    private List<BusinessEvent__e> lstToProcess;

    public NS_SFtoAppainEventHandler(List<BusinessEvent__e> evtLst) {       
        lstToProcess = evtLst;
    }

    public void execute(QueueableContext context) {

        Business_Platform_Events__c customSettingValues = Business_Platform_Events__c.getOrgDefaults();       
        
        for(BusinessEvent__e evt : lstToProcess){
            if((evt.Event_Id__c!=null)){
                system.debug('Salesforce to Appain event received for the Event ID '+evt.Event_Id__c);
                String evtBody = '{"eventIdentifier":"'+evt.Event_Id__c+'","externalSystemCorrelationId":"'+evt.Event_Record_Id__c+'","eventBody":'+evt.Message_Body__c+'}';
                AppianCallout.SendInformationToAppian(evtBody, '', '');             
            }                       
        }
    }
}