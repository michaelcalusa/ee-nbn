@isTest
public class NS_SFtoAppainEventHandler_Test {

    @isTest
    static void testExecuteMethod()
    {
        Id oppId = SF_TestService.getQuoteRecords();
        List<Opportunity> oppList = [SELECT Id FROM OPPORTUNITY WHERE Opportunity_Bundle__c = :oppId];
        System.debug('PP oppList:'+oppList);
        Account acc = SF_TestData.createAccount('Test Account');
        Business_Platform_Events__c settings = SF_TestData.createCustomSettingsforBusinessPlatformEvents();
        insert settings;
        
        List<BusinessEvent__e> toProcess = new List<BusinessEvent__e>();
        for(Integer i=0;i<21;i++){
            String msg = '{ opportunityId:' + oppId +'}';
            BusinessEvent__e b = new BusinessEvent__e();
            b.Event_Id__c = 'SF-DF-000001';
            b.Event_Record_Id__c = String.valueOf(i);
            b.Event_Type__c = 'Desktop assesment';
            b.Source__c = 'Salesforce';
            b.Event_Sub_Type__c = 'NBN_SELECT';
            b.Message_Body__c = msg;
            toProcess.add(b);
        }
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, '', null));
        test.startTest();  
        try{
        Id jobId = System.enqueueJob(new NS_SFtoAppainEventHandler(toProcess));
        }
        catch(Exception e){ 
        }
        test.stopTest();  
    }
}