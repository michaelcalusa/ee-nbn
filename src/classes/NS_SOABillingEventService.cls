public class NS_SOABillingEventService {
    
    @future(callout=true)
    public static void createBillingEvent(String param) {
        system.debug('param: ' + param);

        final String END_POINT;
        final Integer TIMEOUT_LIMIT;
        final String NAMED_CREDENTIAL;
        // Headers
        final String NBN_ENV_OVRD_VAL;

        // Errors
        final String ERROR_MESSAGE = 'A HTTP error occurred in class method NS_SOABillingEventService.createBillingEvent';

        Http http = new Http();
        HttpRequest req;
        HTTPResponse res;

        String requestStr;
        String responseBodyStatus;

        Map<String, String> headerMap = new Map<String, String>();

        try {
            ParsedResult result = getParamInputs(param);
            system.debug('dfOrderId: ' + result.dfOrderId);

            // Get dfOrder and its details
            DF_Order__c dfOrder = NS_SOABillingEventUtils.getDFOrder(result.dfOrderId);

            if (NS_SOABillingEventUtils.isBillingEventSent(dfOrder, result.chargeType)) {
                GlobalUtility.logMessage('Info', NS_SOABillingEventService.class.getName(), 'createBillingEvent',  result.dfOrderId, '',
                                         'Billing event already sent, skipping for DF_Order: Id=' + result.dfOrderId + ', chargeType=' + result.chargeType, '', null, 0);
                return;
            }

            DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
                                                         Timeout__c,
                                                         Prod_EndPoint__c,
                                                         DP_Environment_Override__c
                                                         FROM   DF_Integration_Setting__mdt
                                                         WHERE  DeveloperName = :NS_SOABillingEventUtils.INTEGR_SETTING_NAME_CREATE_BILL_EVT];

            NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
            TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
            NBN_ENV_OVRD_VAL = integrSetting.DP_Environment_Override__c;

            system.debug('NAMED_CREDENTIAL: ' + NAMED_CREDENTIAL);
            system.debug('TIMEOUT_LIMIT: ' + TIMEOUT_LIMIT);
            system.debug('NBN_ENV_OVRD_VAL: ' + NBN_ENV_OVRD_VAL);

            if (runningInSandbox()) {
                NamedCredential nc = [select endpoint from NamedCredential where developerName=:NAMED_CREDENTIAL LIMIT 1];
                if (nc.endpoint.equalsIgnoreCase(integrSetting.Prod_EndPoint__c)) {
                    throw new CustomException(DF_IntegrationUtils.ERR_PROD_URL_CONFIGURED);
                }
            }

            // Generate Payload
            requestStr = NS_SOABillingEventUtils.getBillingEventRequest(dfOrder, result.chargeType);
            system.debug('Payload request: ' + requestStr);

            if (String.isEmpty(requestStr)) {
                throw new CustomException(NS_Order_Utils.ERR_REQUEST_BODY_EMPTY);
            }

            // Build endpoint
            END_POINT = SF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL;
            system.debug('END_POINT: ' + END_POINT);

            headerMap.put(NS_Order_Utils.NBN_ENV_OVRD, NBN_ENV_OVRD_VAL);
            NS_SOABillingEventUtils.getHeaders(dfOrder, headerMap, result.chargeType);

            // Generate request
            req = NS_SOABillingEventUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, requestStr, headerMap);
            system.debug('req: ' + req);

            // Send and get response
            res = http.send(req);

            system.debug('Response Status Code: ' + res.getStatusCode());
            system.debug('Response Body: ' + res.getBody());

            if (res.getStatusCode() == SF_Constants.HTTP_STATUS_200) {
                system.debug('send success');
                NS_SOABillingEventUtils.SOACreateBillingEventSuccessPostProcessing(dfOrder, result.chargeType);
            } else {
                system.debug('send failure');

                List<String> paramList = new List<String>{param};
                AsyncQueueableUtils.retry(NS_SOACreateBillingEventHandler.HANDLER_NAME, paramList, ERROR_MESSAGE);
            }
        } catch (Exception e) {
            GlobalUtility.logMessage(SF_Constants.ERROR, NS_SOABillingEventService.class.getName(), 'createBillingEvent', '', '', '', '', e, 0);

            List<String> paramList = new List<String>{param};
            AsyncQueueableUtils.retry(NS_SOACreateBillingEventHandler.HANDLER_NAME, paramList, (e.getMessage() + '\n\n' + e.getStackTraceString()));
        }
    }

    public static boolean runningInSandbox()
    {
        Organization org=[SELECT IsSandbox FROM Organization LIMIT 1];
        return org.isSandbox;
    }

    public class ParsedResult {
        public String chargeType {get; set;}
        public Id dfOrderId {get; set;}
    }

    @TestVisible private static ParsedResult getParamInputs(String param) {
        List<String> paramInputList;

        ParsedResult result = new ParsedResult();

        String dfOrderId;

        try {
            if (String.isNotEmpty(param)) {
                paramInputList = param.split(NS_SOABillingEventUtils.PARAMETER_DELIMITER, 2);

                if (!paramInputList.isEmpty()) {
                    if (paramInputList.size() == 2) {
                        dfOrderId = paramInputList[0];

                        if (String.isNotEmpty(dfOrderId)) {
                            result.dfOrderId = dfOrderId;
                        }

                        result.chargeType = paramInputList[1];
                    }
                }
            }
            return result;
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }
    
}