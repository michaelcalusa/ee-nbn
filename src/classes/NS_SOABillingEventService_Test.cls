@isTest
private class NS_SOABillingEventService_Test {
    
    @isTest static void test_getParamInputs() {                 
        /** Setup data **/
        
        // Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
        
        DF_Order__c dfOrder = SF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        insert dfOrder; 
        
        String param = dfOrder.Id+',SITE SURVEY';
        
        test.startTest();           
        
        NS_SOABillingEventService.ParsedResult result = NS_SOABillingEventService.getParamInputs(param);

        test.stopTest();

        // Assertions
        Assert.equals(dfOrder.Id, result.dfOrderId);
        Assert.equals('SITE SURVEY', result.chargeType);
    }

    @isTest static void test_createBillingEvent_Successful_CancelEvent() {
        /** Setup data  **/
        createNSCustomSettings();

        String response;
        User commUser;

        // Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;

        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        insert dfQuote;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        dfOrder.Order_Cancel_Charges_JSON__c = '{"field":"value"}';
        dfOrder.SOA_Order_Cancel_Charges_Sent__c = false;
        insert dfOrder;

        final String DELIMITER = ',';
        final String CHARGE_TYPE = NS_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER;
        String param = dfOrder.Id + DELIMITER + CHARGE_TYPE;

        // Generate mock response
        response = '{"Ack":"true"}';

        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

        // Set up commUser to run test as
        commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser) {
            test.startTest();

            NS_SOABillingEventService.createBillingEvent(param);

            test.stopTest();
        }

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT SOA_Order_Cancel_Charges_Sent__c
                                         FROM   DF_Order__c
                                         WHERE  Id = :dfOrder.Id];

        system.AssertEquals(true, dfOrderList[0].SOA_Order_Cancel_Charges_Sent__c);
    }

    @isTest static void test_createBillingEvent_Failure_CancelEvent() {
        /** Setup data  **/
        createCustomSettings();
        createNSCustomSettings();

        String response;
        User commUser;

        // Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;

        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        insert dfQuote;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        dfOrder.Order_Cancel_Charges_JSON__c = '{"field":"value"}';
        dfOrder.SOA_Order_Cancel_Charges_Sent__c = false;
        insert dfOrder;

        final String DELIMITER = ',';
        final String CHARGE_TYPE = NS_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER;
        String param = dfOrder.Id + DELIMITER + CHARGE_TYPE;

        // Generate mock response
        response = '{"Ack":"true"}';

        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(500, response, null));

        // Set up commUser to run test as
        commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser) {
            test.startTest();

            NS_SOABillingEventService.createBillingEvent(param);

            test.stopTest();
        }

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT SOA_Order_Cancel_Charges_Sent__c
                                         FROM   DF_Order__c
                                         WHERE  Id = :dfOrder.Id];

        system.AssertEquals(false, dfOrderList[0].SOA_Order_Cancel_Charges_Sent__c);
    }


    @isTest static void test_createBillingEvent_Successful_CancelEvent_AlreadySent() {
        /** Setup data  **/
        createNSCustomSettings();

        String response;
        User commUser;

        // Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;

        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        insert dfQuote;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        dfOrder.Order_Cancel_Charges_JSON__c = '{"field":"value"}';
        dfOrder.SOA_Order_Cancel_Charges_Sent__c = true;
        insert dfOrder;

        final String DELIMITER = ',';
        final String CHARGE_TYPE = NS_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER;
        String param = dfOrder.Id + DELIMITER + CHARGE_TYPE;

        // Generate mock response
        response = '{"Ack":"true"}';

        // Set mock callout class to always throw exceptions
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseExceptionGenerator());

        // Set up commUser to run test as
        commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser) {
            test.startTest();

            try{
                NS_SOABillingEventService.createBillingEvent(param);
            } catch(Exception e) {
                Assert.fail('Should not throw exception');
            }

            test.stopTest();
        }

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT SOA_Order_Cancel_Charges_Sent__c
                                         FROM   DF_Order__c
                                         WHERE  Id = :dfOrder.Id];

        system.AssertEquals(true, dfOrderList[0].SOA_Order_Cancel_Charges_Sent__c);
    }

    class MockHttpResponseExceptionGenerator implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            throw new CustomException('MockHttpExceptionGenerator');
        }
    }

    static void createNSCustomSettings() {
        try {
            NS_Custom_Options__c nsOption = NS_Custom_Options__c.getValues('NS_SOA_PRODUCT_TYPE');
            if (nsOption == null) {
                nsOption = new NS_Custom_Options__c();
            }
            nsOption.Name = 'NS_SOA_PRODUCT_TYPE';
            nsOption.Value__c = 'NS';

            upsert nsOption;
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    static void createCustomSettings() {
        try {
            Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
            asrConfigSettings.Max_No_of_Future_Calls__c = 50;
            asrConfigSettings.Max_No_of_Parallels__c = 10;
            asrConfigSettings.Max_No_of_Retries__c = 3;
            asrConfigSettings.Max_No_of_Rows__c = 1;      
            upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {           
            throw new CustomException(e.getMessage());
        }                
    }  
    
}