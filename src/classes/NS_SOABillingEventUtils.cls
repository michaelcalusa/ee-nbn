public class NS_SOABillingEventUtils {
    
    public static final String INTEGR_SETTING_NAME_CREATE_BILL_EVT = 'NS_SOA_Create_Billing_Event';
    public static final String CHARGE_TYPE_SITE_SURVEY = 'SITE SURVEY';
    public static final String CHARGE_TYPE_PRODUCT = 'PRODUCT'; 
    public static final String CHARGE_TYPE_CANCEL_ORDER = 'CANCEL ORDER';
    public static final String PARAMETER_DELIMITER = ',';
    public static final String ERR_DFORDER_SITE_SURVEY_CHARGES_JSON_NOT_FOUND = 'DF Order - Site Survey Charges JSON not found.';
    public static final String ERR_DFORDER_SITE_PRODUCT_CHARGES_JSON_NOT_FOUND = 'DF Order - Product Charges JSON not found.';
    
    public static void getHeaders(DF_Order__c dfOrder, Map<String, String> headerMap, String chargeType) {     
        // Headers
        final String TIMESTAMP_VAL;  
        final String CORRELATION_ID_VAL;
        final String ACCESS_SEEKER_ID_VAL;
        final String PRODUCT_TYPE;
        
        try {               
            TIMESTAMP_VAL = DateTime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            
            headerMap.put(NS_Order_Utils.TIMESTAMP, TIMESTAMP_VAL);
            
            if (dfOrder != null) {
				// aligned with EE implementations                
                if(NS_SOABillingEventUtils.CHARGE_TYPE_PRODUCT.equalsIgnoreCase(chargeType)) {
                    CORRELATION_ID_VAL = dfOrder.DF_Quote__r.Order_GUID__c;
                } else {
                    CORRELATION_ID_VAL = dfOrder.DF_Quote__r.GUID__c;
                } 
                system.debug('CORRELATION_ID_VAL ::'+ CORRELATION_ID_VAL );
                
                ACCESS_SEEKER_ID_VAL = dfOrder.Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c;
                system.debug('ACCESS_SEEKER_ID_VAL ::'+ ACCESS_SEEKER_ID_VAL );
                
                String productType = SF_CS_API_Util.getCustomSettingValue('NS_SOA_PRODUCT_TYPE');
                system.debug('productType::'+productType);
                
                if(String.isNotBlank(productType)){
                    PRODUCT_TYPE=productType;
                }
                
                //PRODUCT_TYPE = 'nbn select';
                system.debug('PRODUCT_TYPE ::'+ PRODUCT_TYPE );
                
                headerMap.put(NS_Order_Utils.CORRELATION_ID, CORRELATION_ID_VAL);                          
                headerMap.put(NS_Order_Utils.ACCESS_SEEKER_ID, ACCESS_SEEKER_ID_VAL);
                headerMap.put(NS_Order_Utils.PRODUCT_TYPE, PRODUCT_TYPE);
            }
            
            headerMap.put(SF_Constants.CONTENT_TYPE, SF_Constants.CONTENT_TYPE_JSON);
            headerMap.put(SF_Constants.HTTP_HEADER_ACCEPT, SF_Constants.CONTENT_TYPE_JSON);
        } catch (Exception e) {  
            throw new CustomException(e.getMessage());
        }
    }
    
    public static HttpRequest getHttpRequest(String endPoint, Integer timeoutLimit, String requestStr, Map<String, String> headerMap) {                                                      
        HttpRequest req = new HttpRequest();        
        
        try {               
            req.setEndpoint(endpoint);      
            req.setMethod(SF_Constants.HTTP_METHOD_POST);
            req.setTimeout(timeoutLimit);
            
            // Set all headers
            if (String.isNotEmpty(headerMap.get(NS_Order_Utils.CORRELATION_ID))) {
                system.debug('::GUID::'+headerMap.get(NS_Order_Utils.CORRELATION_ID));             
                req.setHeader(NS_Order_Utils.CORRELATION_ID, headerMap.get(NS_Order_Utils.CORRELATION_ID));           
            } else {                
                throw new CustomException(NS_Order_Utils.ERR_CORRELATION_ID_NOT_FOUND);
            }            
            
            req.setHeader(NS_Order_Utils.TIMESTAMP, headerMap.get(NS_Order_Utils.TIMESTAMP));
            req.setHeader(NS_Order_Utils.PRODUCT_TYPE, headerMap.get(NS_Order_Utils.PRODUCT_TYPE));
            
            if (String.isNotEmpty(headerMap.get(NS_Order_Utils.ACCESS_SEEKER_ID))) {               
                req.setHeader(NS_Order_Utils.ACCESS_SEEKER_ID, headerMap.get(NS_Order_Utils.ACCESS_SEEKER_ID));               
            } else {                
                throw new CustomException(NS_Order_Utils.ERR_ACCESS_SEEKER_ID_NOT_FOUND);
            }   
            
            // Only set NBN ENV Override header if Data Power NBN ENV Override - Integration settings val was set
            if (String.isNotEmpty(headerMap.get(NS_Order_Utils.NBN_ENV_OVRD))) {               
                req.setHeader(NS_Order_Utils.NBN_ENV_OVRD, headerMap.get(NS_Order_Utils.NBN_ENV_OVRD));               
            }               
            
            req.setHeader(SF_Constants.CONTENT_TYPE, headerMap.get(SF_Constants.CONTENT_TYPE));
            req.setHeader(SF_Constants.HTTP_HEADER_ACCEPT, headerMap.get(SF_Constants.CONTENT_TYPE));
            
            req.setBody(requestStr);                                 
        } catch (Exception e) {  
            throw new CustomException(e.getMessage());
        }
        
        return req;
    }   
    
    public static void SOACreateBillingEventSuccessPostProcessing(DF_Order__c dfOrder, String chargeType) {
        try {    
            
            if (dfOrder != null) {
                if (chargeType.equalsIgnoreCase(NS_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY)) {
                    dfOrder.SOA_SiteSurvey_Charges_Sent__c = true;                                                                              
                } else if (chargeType.equalsIgnoreCase(NS_SOABillingEventUtils.CHARGE_TYPE_PRODUCT)) {
                    dfOrder.SOA_Product_Charges_Sent__c = true;
                } else if (chargeType.equalsIgnoreCase(NS_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER)) {
                    dfOrder.SOA_Order_Cancel_Charges_Sent__c = true;
                }
            }
            
            update dfOrder;
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }        
    }
    
    public static boolean isBillingEventSent(DF_Order__c dfOrder, String chargeType) {
        if (dfOrder != null) {
            if (chargeType.equalsIgnoreCase(NS_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY)) {
                return dfOrder.SOA_SiteSurvey_Charges_Sent__c == true;                                                                              
            } else if (chargeType.equalsIgnoreCase(NS_SOABillingEventUtils.CHARGE_TYPE_PRODUCT)) {
                return dfOrder.SOA_Product_Charges_Sent__c == true;
            } else if (chargeType.equalsIgnoreCase(NS_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER)) {
                return dfOrder.SOA_Order_Cancel_Charges_Sent__c == true;
            }
        }
        return false;
    }
    
    public static String getBillingEventRequest(DF_Order__c dfOrder, String chargeType) {	 	    		
        if (dfOrder != null) {
            if (chargeType.equalsIgnoreCase(NS_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY)) {               		         		
                return dfOrder.Site_Survey_Charges_JSON__c;        			
            } else if (chargeType.equalsIgnoreCase(NS_SOABillingEventUtils.CHARGE_TYPE_PRODUCT)) {            		            		
                return dfOrder.Product_Charges_JSON__c;         			
            } else if (chargeType.equalsIgnoreCase(NS_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER)) {
                return dfOrder.Order_Cancel_Charges_JSON__c;
            }
        }
        return '';
    }
    
    public static DF_Order__c getDFOrder(Id dfOrderId) {                                             
        DF_Order__c dfOrder;
        List<DF_Order__c> dfOrderList = new List<DF_Order__c>();
        
        try {
            if (String.isNotEmpty(dfOrderId)) {
                dfOrderList = [SELECT DF_Quote__r.GUID__c, DF_Quote__r.Order_GUID__c,
                               Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c,
                               Site_Survey_Charges_JSON__c,
                               SOA_SiteSurvey_Charges_Sent__c,
                               Product_Charges_JSON__c,
                               SOA_Product_Charges_Sent__c,
                               Order_Cancel_Charges_JSON__c,
                               SOA_Order_Cancel_Charges_Sent__c
                               FROM   DF_Order__c            
                               WHERE  Id = :dfOrderId];
                
                if (!dfOrderList.isEmpty()) {
                    dfOrder = dfOrderList[0];
                }
            }                                                     
        } catch (Exception e) {    
            throw new CustomException(e.getMessage());
        }
        
        return dfOrder;
    }
    
}