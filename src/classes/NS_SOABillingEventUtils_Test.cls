@isTest
private class NS_SOABillingEventUtils_Test {
    
    @isTest static void test_getHttpRequest() {                     
        /** Setup data **/   
        final String END_POINT;
        final String NAMED_CREDENTIAL;        
        final Integer TIMEOUT_LIMIT;
        
        final String NBN_ENV_OVRD_VAL;
        final String TIMESTAMP_VAL;  
        final String CORRELATION_ID_VAL;
        final String ACCESS_SEEKER_ID_VAL;        
        
        final String CORRELATION_ID;
        final String ACCESS_SEEKER_ID;
        String requestStr = 'requestStr';
        final String PRODUCT_TYPE;
        
        HttpRequest req;
        User commUser;

        Map<String, String> headerMap = new Map<String, String>();

        DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
                                                     Timeout__c,
                                                     DP_Environment_Override__c
                                                     FROM   DF_Integration_Setting__mdt
                                                     WHERE  DeveloperName = :NS_SOABillingEventUtils.INTEGR_SETTING_NAME_CREATE_BILL_EVT];

        NAMED_CREDENTIAL = integrSetting.Named_Credential__c;
        TIMEOUT_LIMIT = Integer.valueOf(integrSetting.Timeout__c);
        NBN_ENV_OVRD_VAL = integrSetting.DP_Environment_Override__c;
        END_POINT = SF_Constants.NAMED_CRED_PREFIX + NAMED_CREDENTIAL;

        TIMESTAMP_VAL = DateTime.now().formatGmt('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        CORRELATION_ID_VAL = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        ACCESS_SEEKER_ID_VAL = 'ASI500050005000';
        PRODUCT_TYPE='NS';

        // Setup headerMap
        headerMap.put(NS_Order_Utils.NBN_ENV_OVRD, NBN_ENV_OVRD_VAL);
        headerMap.put(NS_Order_Utils.TIMESTAMP, TIMESTAMP_VAL);
        headerMap.put(NS_Order_Utils.CORRELATION_ID, CORRELATION_ID_VAL);
        headerMap.put(NS_Order_Utils.ACCESS_SEEKER_ID, ACCESS_SEEKER_ID_VAL);
        headerMap.put(NS_Order_Utils.PRODUCT_TYPE, PRODUCT_TYPE);
        headerMap.put(SF_Constants.CONTENT_TYPE, SF_Constants.CONTENT_TYPE_JSON);
        headerMap.put(SF_Constants.HTTP_HEADER_ACCEPT, SF_Constants.CONTENT_TYPE_JSON);

        commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser) {
            // Statements to be executed by this test user
            test.startTest();

            req = NS_SOABillingEventUtils.getHttpRequest(END_POINT, TIMEOUT_LIMIT, requestStr, headerMap);

            test.stopTest();
        }

        // Assertions
        system.AssertNotEquals(null, req);
    }

    @isTest static void test_getHeaders() {
        /** Setup data **/
        final String NBN_ENV_OVRD_VAL;
        Map<String, String> headerMap = new Map<String, String>();

        // Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;

        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.Order_GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55bada';
        insert dfQuote;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        insert dfOrder;

        dfOrder = NS_SOABillingEventUtils.getDFOrder(dfOrder.Id);

        // Setup headerMap
        headerMap.put(NS_Order_Utils.NBN_ENV_OVRD, NBN_ENV_OVRD_VAL);

        test.startTest();

        NS_SOABillingEventUtils.getHeaders(dfOrder, headerMap, 'PRODUCT');

        test.stopTest();

        // Assertions
        system.AssertEquals(7, headerMap.size());
        system.AssertEquals('eddbe103-b9aa-4a35-9e3e-83448f55bada', headerMap.get(NS_Order_Utils.CORRELATION_ID));
    }

    @isTest static void test_getHeaders_nonProduct() {
        /** Setup data **/
        final String NBN_ENV_OVRD_VAL;
        Map<String, String> headerMap = new Map<String, String>();

        // Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;

        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        dfQuote.Order_GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55bada';
        insert dfQuote;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        insert dfOrder;

        dfOrder = NS_SOABillingEventUtils.getDFOrder(dfOrder.Id);

        // Setup headerMap
        headerMap.put(NS_Order_Utils.NBN_ENV_OVRD, NBN_ENV_OVRD_VAL);

        test.startTest();

        NS_SOABillingEventUtils.getHeaders(dfOrder, headerMap, 'others');

        test.stopTest();

        // Assertions
        system.AssertEquals(7, headerMap.size());
        system.AssertEquals('eddbe103-b9aa-4a35-9e3e-83448f55badq', headerMap.get(NS_Order_Utils.CORRELATION_ID));
    }

    @isTest static void test_getDFOrder() {
        /** Setup data **/
        DF_Order__c dfOrder;

        // Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;

        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create DF Order
        DF_Order__c dfOrderToCreate = SF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');

        dfOrderToCreate.Site_Survey_Charges_JSON__c = 'Site_Survey_Charges_JSON__c';
        dfOrderToCreate.SOA_SiteSurvey_Charges_Sent__c = true;
        dfOrderToCreate.Product_Charges_JSON__c = 'Product_Charges_JSON__c';
        dfOrderToCreate.SOA_Product_Charges_Sent__c = true;
        dfOrderToCreate.Order_Cancel_Charges_JSON__c = 'Order_Cancel_Charges_JSON__c';
        dfOrderToCreate.SOA_Order_Cancel_Charges_Sent__c = false;
        insert dfOrderToCreate;

        NS_SOAOrderDataJSONWrapper.NS_SOADataJSONRoot soaJson = new NS_SOAOrderDataJSONWrapper.NS_SOADataJSONRoot();

        test.startTest();

        dfOrder = NS_SOABillingEventUtils.getDFOrder(dfOrderToCreate.Id);

        test.stopTest();

        // Assertions
        system.AssertNotEquals(null, dfOrder);
        Assert.equals('Site_Survey_Charges_JSON__c', dfOrderToCreate.Site_Survey_Charges_JSON__c);
        Assert.equals(true, dfOrderToCreate.SOA_SiteSurvey_Charges_Sent__c);
        Assert.equals('Product_Charges_JSON__c', dfOrderToCreate.Product_Charges_JSON__c);
        Assert.equals(true, dfOrderToCreate.SOA_Product_Charges_Sent__c);
        Assert.equals('Order_Cancel_Charges_JSON__c', dfOrderToCreate.Order_Cancel_Charges_JSON__c);
        Assert.equals(false, dfOrderToCreate.SOA_Order_Cancel_Charges_Sent__c);
    }

    @isTest static void test_SOACreateBillingEventSuccessPostProcessing_Site_Survey_Charge() {
        /** Setup data **/
        // Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;

        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        insert dfOrder;

        test.startTest();

        NS_SOABillingEventUtils.SOACreateBillingEventSuccessPostProcessing(dfOrder, NS_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY);

        test.stopTest();

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT SOA_SiteSurvey_Charges_Sent__c
                                         FROM   DF_Order__c
                                         WHERE  Id = :dfOrder.Id];

        system.AssertEquals(true, dfOrderList[0].SOA_SiteSurvey_Charges_Sent__c);
    }

    @isTest static void test_SOACreateBillingEventSuccessPostProcessing_Product_Charge() {
        /** Setup data **/
        // Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;

        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        insert dfOrder;

        test.startTest();

        NS_SOABillingEventUtils.SOACreateBillingEventSuccessPostProcessing(dfOrder, NS_SOABillingEventUtils.CHARGE_TYPE_PRODUCT);

        test.stopTest();

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT SOA_Product_Charges_Sent__c
                                         FROM   DF_Order__c
                                         WHERE  Id = :dfOrder.Id];

        system.AssertEquals(true, dfOrderList[0].SOA_Product_Charges_Sent__c);
    }

    @isTest static void test_SOACreateBillingEventSuccessPostProcessing_Cancel() {
        /** Setup data **/
        // Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;

        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        dfOrder.SOA_Order_Cancel_Charges_Sent__c = false;
        insert dfOrder;

        test.startTest();

        NS_SOABillingEventUtils.SOACreateBillingEventSuccessPostProcessing(dfOrder, NS_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER);

        test.stopTest();

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT SOA_Order_Cancel_Charges_Sent__c
                                         FROM   DF_Order__c
                                         WHERE  Id = :dfOrder.Id];

        system.AssertEquals(true, dfOrderList[0].SOA_Order_Cancel_Charges_Sent__c);
    }

    @isTest static void test_SOACreateBillingEventSuccessPostProcessing_others() {
        /** Setup data **/
        // Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;

        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(null, opptyBundle.Id, 'In Draft');
        dfOrder.SOA_Product_Charges_Sent__c = false;
        dfOrder.SOA_SiteSurvey_Charges_Sent__c = false;
        dfOrder.SOA_Order_Cancel_Charges_Sent__c = false;
        insert dfOrder;

        test.startTest();

        NS_SOABillingEventUtils.SOACreateBillingEventSuccessPostProcessing(dfOrder, 'unknown');

        test.stopTest();

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT SOA_Order_Cancel_Charges_Sent__c,
                                         SOA_Product_Charges_Sent__c,
                                         SOA_SiteSurvey_Charges_Sent__c
                                         FROM   DF_Order__c
                                         WHERE  Id = :dfOrder.Id];

        system.AssertEquals(false, dfOrderList[0].SOA_Order_Cancel_Charges_Sent__c);
        system.AssertEquals(false, dfOrderList[0].SOA_Product_Charges_Sent__c);
        system.AssertEquals(false, dfOrderList[0].SOA_SiteSurvey_Charges_Sent__c);
    }

    @isTest static void test_isBillingEventSent_orderIsNull() {
        test.startTest();

        boolean result = NS_SOABillingEventUtils.isBillingEventSent(null, 'unknown');

        test.stopTest();

        Assert.equals(false, result);
    }

    @isTest static void test_isBillingEventSent() {
        boolean result = false;
        test.startTest();

        DF_Order__c dfOrder = new DF_Order__c();
        dfOrder.SOA_SiteSurvey_Charges_Sent__c = true;
        dfOrder.SOA_Product_Charges_Sent__c = true;
        dfOrder.SOA_Order_Cancel_Charges_Sent__c = null;

        result = NS_SOABillingEventUtils.isBillingEventSent(dfOrder, NS_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY);
        Assert.equals(true, result);

        result = NS_SOABillingEventUtils.isBillingEventSent(dfOrder, NS_SOABillingEventUtils.CHARGE_TYPE_PRODUCT);
        Assert.equals(true, result);

        result = NS_SOABillingEventUtils.isBillingEventSent(dfOrder, NS_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER);
        Assert.equals(false, result);

        result = NS_SOABillingEventUtils.isBillingEventSent(dfOrder, 'unknown');
        Assert.equals(false, result);

        test.stopTest();

    }

    @isTest static void test_getBillingEventRequest() {
        String result = null;
        test.startTest();

        DF_Order__c dfOrder = new DF_Order__c();
        dfOrder.Site_Survey_Charges_JSON__c = 'Site_Survey_Charges_JSON__c';
        dfOrder.Product_Charges_JSON__c = 'Product_Charges_JSON__c';
        dfOrder.Order_Cancel_Charges_JSON__c = 'Order_Cancel_Charges_JSON__c';

        result = NS_SOABillingEventUtils.getBillingEventRequest(dfOrder, NS_SOABillingEventUtils.CHARGE_TYPE_SITE_SURVEY);
        Assert.equals('Site_Survey_Charges_JSON__c', result);

        result = NS_SOABillingEventUtils.getBillingEventRequest(dfOrder, NS_SOABillingEventUtils.CHARGE_TYPE_PRODUCT);
        Assert.equals('Product_Charges_JSON__c', result);

        result = NS_SOABillingEventUtils.getBillingEventRequest(dfOrder, NS_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER);
        Assert.equals('Order_Cancel_Charges_JSON__c', result);

        result = NS_SOABillingEventUtils.getBillingEventRequest(dfOrder, 'unknown');
        Assert.equals('', result);

        result = NS_SOABillingEventUtils.getBillingEventRequest(null, 'unkown');
        Assert.equals('', result);

        result = NS_SOABillingEventUtils.getBillingEventRequest(null, NS_SOABillingEventUtils.CHARGE_TYPE_CANCEL_ORDER);
        Assert.equals('', result);

        test.stopTest();

    }
}