public class NS_SOACreateBillingEventHandler extends AsyncQueueableHandler {
    public static final String HANDLER_NAME = 'NS_SOACreateBillingEventHandler';

    public NS_SOACreateBillingEventHandler() {
        // No of params must be 1
        super(NS_SOACreateBillingEventHandler.HANDLER_NAME, 1);
    }

    public override void executeWork(List<String> paramsList) {
        // Enforce max of 1 input parameter
        if (paramsList.size() == 1) {
            invokeProcess(paramsList[0]);
        } else {
            throw new CustomException(AsyncQueueableUtils.ERR_INVALID_PARAMS);
        }
    }

    private void invokeProcess(String param) {
        NS_SOABillingEventService.createBillingEvent(param);
    }
}