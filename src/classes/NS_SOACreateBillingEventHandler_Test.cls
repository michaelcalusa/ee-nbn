@isTest
private class NS_SOACreateBillingEventHandler_Test {

    @isTest static void test_executeWork() {
        /** Setup data  **/
        setup();

        String response;
        User commUser;

        // Create Acct
        Account acct = SF_TestData.createAccount('Test Account');
        acct.Access_Seeker_ID__c = 'ASI500050005000';
        insert acct;

        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';

        DF_Quote__c dfQuote = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', null, opptyBundle.Id, null, 'Green');
        dfQuote.GUID__c = 'eddbe103-b9aa-4a35-9e3e-83448f55badq';
        insert dfQuote;

        // Create DF Order
        DF_Order__c dfOrder = SF_TestData.createDFOrder(dfQuote.Id, opptyBundle.Id, 'In Draft');
        dfOrder.Product_Charges_JSON__c = '{"field":"value"}';
        dfOrder.SOA_Product_Charges_Sent__c = false;
        insert dfOrder;

        final String DELIMITER = ',';
        final String CHARGE_TYPE = NS_SOABillingEventUtils.CHARGE_TYPE_PRODUCT;
        String param = dfOrder.Id + DELIMITER + CHARGE_TYPE;

        List<String> paramsList = new List<String>();
        paramsList.add(param);

        // Generate mock response
        response = '{"Ack":"true"}';

        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));

        // Set up commUser to run test as
        commUser = SF_TestData.createDFCommUser();

        system.runAs(commUser) {
            test.startTest();

            NS_SOACreateBillingEventHandler handler = new NS_SOACreateBillingEventHandler();
            // Invoke Call
            handler.executeWork(paramsList);

            test.stopTest();
        }

        // Assertions
        List<DF_Order__c> dfOrderList = [SELECT SOA_Product_Charges_Sent__c
                                         FROM   DF_Order__c
                                         WHERE  Id = :dfOrder.Id];

        system.AssertEquals(true, dfOrderList[0].SOA_Product_Charges_Sent__c);
    }

    @isTest static void test_executeWork_withInvalidParamsError() {
        List<String> paramsList = new List<String>();
        paramsList.add('param1');
        paramsList.add('param2');

        test.startTest();
        try {
            NS_SOACreateBillingEventHandler handler = new NS_SOACreateBillingEventHandler();

            // Invoke Call
            handler.executeWork(paramsList);

            Assert.fail('Should have thrown exception');
        } catch (Exception e) {
            // Assertions
            Assert.equals(AsyncQueueableUtils.ERR_INVALID_PARAMS, e.getMessage());
            Assert.equals(true, e instanceOf CustomException);
        }
        test.stopTest();
    }

    /** Data Creation **/
    @testSetup static void setup() {
        try {
            createCustomSettings();
            createNSCustomSettings();
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    @isTest static void createCustomSettings() {
        try {
            Async_Request_Config_Settings__c asrConfigSettings = Async_Request_Config_Settings__c.getOrgDefaults();
            asrConfigSettings.Max_No_of_Future_Calls__c = 50;
            asrConfigSettings.Max_No_of_Parallels__c = 10;
            asrConfigSettings.Max_No_of_Retries__c = 3;
            asrConfigSettings.Max_No_of_Rows__c = 1;
            upsert asrConfigSettings Async_Request_Config_Settings__c.Id;
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

    @isTest static void createNSCustomSettings() {
        try {
            NS_Custom_Options__c nsOption = NS_Custom_Options__c.getValues('NS_SOA_PRODUCT_TYPE');
            if (nsOption == null) {
                nsOption = new NS_Custom_Options__c();
            }
            nsOption.Name = 'NS_SOA_PRODUCT_TYPE';
            nsOption.Value__c = 'NS';

            upsert nsOption;
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }

}