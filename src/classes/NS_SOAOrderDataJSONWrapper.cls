public class NS_SOAOrderDataJSONWrapper {
   public class NS_SOADataJSONRoot{
    public Headers headers;
    public Body body;
        
    }
    public class Headers {
        public String activityName;
        public String msgName;
        public String businessServiceName;
        public String businessServiceVersion;
        public String msgType;
        public String timestamp;
        public String accessSeekerID;
        public String correlationId;
        public String security;
        public String communicationPattern;
        public String businessChannel;
        public String orderPriority;
    }

    public class ProductOrderComprisedOf {
        public String action;
        public ItemInvolvesLocation itemInvolvesLocation;
        public ItemInvolvesProduct itemInvolvesProduct ;
        public List<ReferencesProductOrderItem> referencesProductOrderItem ;
    }
    
    public class ReferencesProductOrderItem
    {
        public string action ;
        public ItemInvolvesProduct2 itemInvolvesProduct ;
        public List<ReferencesProductOrderItem2> referencesProductOrderItem ;
    }
    
    public class ItemInvolvesProduct2
    {
        public string id ;
        public string type ;
        public string btdType ;
        public string btdMounting ;
        public string powerSupply1 ;
        public string powerSupply2 ;
        public string poi ;
        public string nni ;
        public string routeType ;
        public string sVLanId ;
        public string maximumFrameSize ;
        public string cosHighBandwidth ;
        public string cosMediumBandwidth ;
        public string cosLowBandwidth ;
        public string uniVLanId ;
        public string connectedTo ;
    }
    
    public class ReferencesProductOrderItem2
    {
        public string action ;
        public ItemInvolvesProduct3 itemInvolvesProduct ;
    }
    
    public class ItemInvolvesProduct3
    {
        public string id ;
        public string type ;
        public string interfaceBandwidth ;
        public string portId ;
        public string interfaceType ;
        public string ovcType ;
        public string tpId ;
        public string cosMappingMode ;
        public string transientId ;
    }
    
    public class ItemInvolvesProduct
    {
        public string id ;
        public string term ;
        public string serviceRestorationSLA ;
        public string accessAvailabilityTarget ;
        public string zone ;
        public string accessServiceTechnologyType ;
        public string templateId ;
        public string QuoteId;
    }
    
    public class ProductOrderInvolvesCharges {
        public List<OneTimeChargeProdPriceCharge> OneTimeChargeProdPriceCharge;
        public List<ProdPriceCharge> ProdPriceCharge;
    }

    public class ItemInvolvesLocation {
        public String id;
    }

    public class Body {
        public ManageBillingEventRequest ManageBillingEventRequest;
    }

    public class AccessSeekerInteraction {
        public String billingAccountID;
    }

    public class ProductOrder {
        public String id;
        public String SFid;
        public AccessSeekerInteraction accessSeekerInteraction;
        public String interactionStatus;
        public String orderType;
        public String interactionDateComplete;
        public String customerRequiredDate;
        public string afterHoursAppointment;
        public string plannedCompletionDate;
        public ProductOrderComprisedOf productOrderComprisedOf;
    }

    public class OneTimeChargeProdPriceCharge {
        public String ID;
        public String name;
        public String amount;
        public String chargeReferences;
        public string type; 
        public string quantity;
    }
    
    public class ProdPriceCharge {
        public String ID;
        public String name;
        public String amount;
        public String chargeReferences;
        public string type; 
        public string quantity;
    }

    public class ManageBillingEventRequest {
        public String notificationType;
        public ProductOrder productOrder;
        public ProductOrderInvolvesCharges ProductOrderInvolvesCharges;
    }

    
    public static NS_SOAOrderDataJSONWrapper parse(String json) {
        return (NS_SOAOrderDataJSONWrapper) System.JSON.deserialize(json, NS_SOAOrderDataJSONWrapper.class);
    }
}