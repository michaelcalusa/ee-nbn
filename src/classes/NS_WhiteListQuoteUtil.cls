public class NS_WhiteListQuoteUtil {
    
    public NS_WhiteListQuoteUtil(){
        // default constructor, this is needed for 'ObjectFactory'
    }
    
    public SF_ServiceFeasibilityResponse getQuoteFromWhiteList(SF_ServiceFeasibilityResponse sfResponse)
    {        
        // Check Status. If not valid, then return response without whitelist Quote
        if(String.isEmpty(sfResponse.Status) || sfResponse.Status.toLowerCase() != 'valid')
        {
            return sfResponse;
        }
        
        List<NS_LocationWhitelist__c> locationWhitelist = 
            [SELECT EBT_Days__c, Quote__c 
             FROM NS_LocationWhitelist__c 
             WHERE IsActive__c = true AND Loc_ID__c = :sfResponse.LocId LIMIT 1];
        
        // If Location not found in Whitelist, then set status and return response without whitelist Quote
        if(locationWhitelist.isEmpty())
        {
            sfResponse.Status = NS_CISCalloutAPIUtils.STATUS_INVALID_FTTP;
            return sfResponse;
        }
        
        sfResponse.whiteListEBTInDays = (Double) locationWhitelist[0].EBT_Days__c;
        sfResponse.whiteListQuoteAmount = (Double) locationWhitelist[0].Quote__c;
        
        sfResponse.Status = NS_CISCalloutAPIUtils.STATUS_VALID;
        return sfResponse;
        
    }
    
    public String getEBTFromWhiteList(String locationId) {
        Map<String, String> locIdToEBTMap = getEbtMap(new Set<String>{locationId}); 
        return locIdToEBTMap.get(locationId);
    }
    
    public Map<String, String> getEbtMap(Set<String> locIds) {
        Map<String, String> locIdToEBTMap = new Map<String, String>();
        
        for (NS_LocationWhitelist__c wl : [SELECT EBT_Days__c, Loc_ID__c
                                           FROM NS_LocationWhitelist__c
                                           WHERE IsActive__c = true 
                                           AND Loc_ID__c IN :locIds]) 
        {
            locIdToEBTMap.put(wl.Loc_ID__c, formatEBT(wl.EBT_Days__c));
        }
        
        return locIdToEBTMap;
    }
    
    public Map<String, String> getEbtMap(List<SObject> soList, String locIdFieldName) {
        Set<String> locIds = new Set<String>();
        String locId;
        for (SObject obj : soList) {
            locId = (String) obj.get(locIdFieldName);
            if (String.isNotBlank(locId)) {
                locIds.add(locId);
            }
        }
        return getEbtMap(locIds);
    }
    
    private String formatEBT(Decimal ebtValue) {
        // EBT_Days__c is mandatory, it cannot be null
        return ebtValue.intValue() + ' business days';
    }
    
}