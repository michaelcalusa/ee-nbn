@IsTest
private class NS_WhiteListQuoteUtilTest {
    
    @isTest(SeeAllData=false)
    static void test_getEBTFromWhiteList() {

        // Setup data
        String locationId = 'LOC000000000001';
        NS_LocationWhitelist__c whitelist = new NS_LocationWhitelist__c();
        whitelist.Loc_ID__c = locationId;
        whitelist.Name = locationId;
        whitelist.Version__c = 0;
        whitelist.Quote__c = 2000;
        whitelist.EBT_Days__c = 30.0;
        insert whitelist;

        Test.startTest();
            NS_WhiteListQuoteUtil quoteutil = new NS_WhiteListQuoteUtil();
            String EBT = quoteutil.getEBTFromWhiteList(locationId);
            System.assertEquals('30 business days', EBT);
        Test.stopTest();

    }
    
    @isTest(SeeAllData=false) 
    static void shouldTrucateEBTToIntegerWhenEBTIsNotInteger() {

        // Setup data
        String locationId = 'LOC000000000001';
        NS_LocationWhitelist__c whitelist = new NS_LocationWhitelist__c();
        whitelist.Loc_ID__c = locationId;
        whitelist.Name = locationId;
        whitelist.Version__c = 1;
        whitelist.Quote__c = 2000;
        whitelist.EBT_Days__c = 5.5;
        insert whitelist;

        Test.startTest();
            NS_WhiteListQuoteUtil quoteutil = new NS_WhiteListQuoteUtil();
            String EBT = quoteutil.getEBTFromWhiteList(locationId);
            System.assertEquals('5 business days', EBT);
        Test.stopTest();

    }
    
    @isTest(SeeAllData=false) 
    static void shouldReturnNullWhenLocIdNotExists() {
        String locationId = 'LOC100000000001';
        Test.startTest();
            NS_WhiteListQuoteUtil quoteutil = new NS_WhiteListQuoteUtil();
            String EBT = quoteutil.getEBTFromWhiteList(locationId);
            Assert.isNull(EBT);
        Test.stopTest();
    }
    
    @isTest(SeeAllData=false) 
    static void shouldGetEbtMap() {
        
        // Setup data
        String locationId = 'LOC000000000001';
        NS_LocationWhitelist__c whitelist = new NS_LocationWhitelist__c();
        whitelist.Loc_ID__c = locationId;
        whitelist.Name = locationId;
        whitelist.Version__c = 1;
        whitelist.Quote__c = 2000;
        whitelist.EBT_Days__c = 5.5;
        insert whitelist;
        
        String locationId2 = 'LOC000000000002';
        NS_LocationWhitelist__c whitelist2 = new NS_LocationWhitelist__c();
        whitelist2.Loc_ID__c = locationId2;
        whitelist2.Name = locationId2;
        whitelist2.Version__c = 1;
        whitelist2.Quote__c = 2000;
        whitelist2.EBT_Days__c = 30.0;
        insert whitelist2;
        
        String locationId3 = 'LOC000000000003';
        NS_LocationWhitelist__c whitelist3 = new NS_LocationWhitelist__c();
        whitelist3.Loc_ID__c = locationId3;
        whitelist3.Name = locationId3;
        whitelist3.Version__c = 1;
        whitelist3.Quote__c = 2000;
        whitelist3.EBT_Days__c = 30.0;
        whitelist3.IsActive__c = false;
        insert whitelist3;
        
        
        Test.startTest();
        NS_WhiteListQuoteUtil quoteutil = new NS_WhiteListQuoteUtil();
        Set<String> locIds = new Set<String> { 'LOC000000000001', 'LOC000000000002', 'LOC000000000003'};
        Map<String, String> locIdToEBTMap = quoteutil.getEbtMap(locIds);
        
        Assert.equals(locIdToEBTMap.size(), 2);
        Assert.equals(locIdToEBTMap.get('LOC000000000001'), '5 business days');
        Assert.equals(locIdToEBTMap.get('LOC000000000002'), '30 business days');
        Assert.isNull(locIdToEBTMap.get('LOC000000000003'));
        
        Test.stopTest();
    }
    
    @isTest(SeeAllData=false) 
    static void shouldGetEbtMapBySObjectList() {
        
        // Setup data
        String locationId = 'LOC000000000001';
        NS_LocationWhitelist__c whitelist = new NS_LocationWhitelist__c();
        whitelist.Loc_ID__c = locationId;
        whitelist.Name = locationId;
        whitelist.Version__c = 1;
        whitelist.Quote__c = 2000;
        whitelist.EBT_Days__c = 5.5;
        insert whitelist;
        
        String locationId2 = 'LOC000000000002';
        NS_LocationWhitelist__c whitelist2 = new NS_LocationWhitelist__c();
        whitelist2.Loc_ID__c = locationId2;
        whitelist2.Name = locationId2;
        whitelist2.Version__c = 1;
        whitelist2.Quote__c = 2000;
        whitelist2.EBT_Days__c = 30.0;
        insert whitelist2;
        
        String locationId3 = 'LOC000000000003';
        NS_LocationWhitelist__c whitelist3 = new NS_LocationWhitelist__c();
        whitelist3.Loc_ID__c = locationId3;
        whitelist3.Name = locationId3;
        whitelist3.Version__c = 1;
        whitelist3.Quote__c = 2000;
        whitelist3.EBT_Days__c = 30.0;
        whitelist3.IsActive__c = false;
        insert whitelist3;
        
        
        Test.startTest();
        NS_WhiteListQuoteUtil quoteutil = new NS_WhiteListQuoteUtil();
        List<SObject> soList = new List<SObject>();
        
        DF_Quote__c quote1 = new DF_Quote__c(Location_Id__c='LOC000000000001');
        DF_Quote__c quote2 = new DF_Quote__c(Location_Id__c='LOC000000000002');
        DF_Quote__c quote3 = new DF_Quote__c(Location_Id__c='LOC000000000003');
        soList.add(quote1);
        soList.add(quote2);
        soList.add(quote3);
        
        Map<String, String> locIdToEBTMap = quoteutil.getEbtMap(soList, 'Location_Id__c');
        
        Assert.equals(locIdToEBTMap.size(), 2);
        Assert.equals(locIdToEBTMap.get('LOC000000000001'), '5 business days');
        Assert.equals(locIdToEBTMap.get('LOC000000000002'), '30 business days');
        Assert.isNull(locIdToEBTMap.get('LOC000000000003'));
        
        Test.stopTest();
    }
}