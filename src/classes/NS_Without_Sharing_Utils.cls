public without sharing class NS_Without_Sharing_Utils {
    public static List<DF_Order__c> getSameLocationOrders(Set<String> setLocId, Id recordTypeId, Set<String> setOrderStatus) {
        List<DF_Order__c> lstAllOrders = [
                SELECT Id,
                        DF_Quote__c,
                        Opportunity_Bundle__c,
                        Opportunity_Bundle__r.Account__r.Access_Seeker_ID__c,
                        DF_Quote__r.Location_Id__c,
                        toLabel(Order_Status__c),
                        DF_Quote__r.Opportunity__c
                FROM DF_Order__c
                WHERE DF_Quote__r.Location_Id__c IN :setLocId
                AND RecordTypeId = :recordTypeId
                AND (NOT (Order_Status__c IN :setOrderStatus))
        ];
        return lstAllOrders;
    }
}