@isTest
private class NS_Without_Sharing_Utils_Test {

    @isTest static void test_getSameLocationOrders() {
        // Setup data
        List<DF_Order__c> lstAllOrders = new List<DF_Order__c>();
        String locId = 'LOC111111111111';
        Set<string> tempLocIdLst = new Set<string>{'LOC111111111111'};
        Set<String> setOrderStatus = new Set<String>{'In Draft', ''};

        Id nsOrderRecType = SF_CS_API_Util.getRecordType(SF_Constants.NBN_SELECT_NAME, SF_Constants.ORDER_OBJECT);
        Id nsQuoteRecType = SF_CS_API_Util.getRecordType(SF_Constants.NBN_SELECT_NAME, SF_Constants.QUOTE_OBJECT);
        //SF_CS_API_Util.getRecordType('NBN Select','DF_Quote__c');

        Account account = new Account();
        account.Access_Seeker_ID__c = 'ASI123456';
        account.Name = 'Fred';
        insert account;

        DF_Opportunity_Bundle__c opportunityBundle = new DF_Opportunity_Bundle__c();
        opportunityBundle.Account__c = account.Id;
        insert opportunityBundle;

//        Opportunity childOpp1 = SF_TestData.createOpportunity('Child1');
//        childOpp1.Opportunity_Bundle__c = opportunityBundle.Id;
//        insert childOpp1;

        DF_Quote__c quote = new DF_Quote__c();
        quote.RecordTypeId = nsQuoteRecType;
        quote.Location_Id__c = locId;
        quote.Opportunity_Bundle__c = opportunityBundle.Id;
        insert quote;

        DF_Order__c order = new DF_Order__c();
        order.Order_Status__c = 'InProgress';
        order.RecordTypeId = nsOrderRecType;
        order.DF_Quote__c = quote.Id;
        order.Opportunity_Bundle__c = opportunityBundle.Id;
        insert order;

        order = new DF_Order__c();
        order.Order_Status__c = 'In Draft';
        order.RecordTypeId = nsOrderRecType;
        order.DF_Quote__c = quote.Id;
        order.Opportunity_Bundle__c = opportunityBundle.Id;
        insert order;

        Test.startTest();
           lstAllOrders = NS_Without_Sharing_Utils.getSameLocationOrders(tempLocIdLst, nsOrderRecType, setOrderStatus);
        Test.stopTest();

        DF_Order__c orderResult = lstAllOrders.get(0);
        System.assertEquals(1, lstAllOrders.size());
        DF_Order__c result = lstAllOrders.get(0);
        System.assertEquals('In Progress', result.Order_Status__c);
     }
}