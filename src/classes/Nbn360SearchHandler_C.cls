/* Author :     Dattaraj Deshmukh
*  Company :    Arxxus Technologies
*  Summary :    This class acts as a controller for Search Page. Retuns records up to a threshold set in "nbn360_Configurations" custom settings.
*  Test Class : Nbn360SearchHandler_Test
*  Modified By: 
*/

public with sharing class Nbn360SearchHandler_C {
    public String fullAddress{get;set;}
    public String locationId{get;set;}
    public String localityName{get;set;}
    public String selectedFullAddressFromTypeAhead {get;set;}
    public String selectedLocationIdFromTypeAhead {get;set;}
    public String disconnectionStartDate {get;set;}
    public String disconnectionEndDate {get;set;}
    public String mentionedDisconnectionEndDate {get;set;}
    public String boundaryId {get;set;}
    public List<String> stateTerritoryCodes {get;set;}
    public List<String> serviceClassList {get;set;}
    public transient List<Location__c> locations {get;set;}
    public String recordLimitErrorMessage {get;set;}
    public String noOfRecordsAboveThreshold {get;set;}
    public String noOfRecordsBelowThreshold {get;set;}
    public String noResultFromQueryMessage{get;set;}
    public nbn360_Configurations__c config {get;set;}
    String searchThreshold;
    /*This method reads messages defined in a custom labels.*/
    @TestVisible
    private void initialiseSettings() {
        //Get custom setting record
        config = nbn360_Configurations__c.getOrgDefaults();
        
        //get threshold value from custom settings
        searchThreshold = config.Search_Threshold__c;
        searchThreshold = (searchThreshold == null || Integer.valueOf(searchThreshold) > 10000 ) ? '10000' : searchThreshold;
        
        /*Populate default messages to be displayed on the page. 
         * Custom Labels are used to populate messages.
         */
        String defaultErrorMessage = 'Your search do not return any results, please revise your search criteria.';
        String messageForNoResults = System.Label.Search_Message_For_No_Result;
        noResultFromQueryMessage  = String.isBlank(messageForNoResults) ? defaultErrorMessage : messageForNoResults;
        
        defaultErrorMessage = 'Your search has returned more than 10,000 records. Select "Export" for a complete view of your data.';
        String messageForRecordsAboveThreshold = System.Label.Search_Message_Over_Threshold;
        noOfRecordsAboveThreshold = String.isBlank(messageForRecordsAboveThreshold) ? defaultErrorMessage : messageForRecordsAboveThreshold;
       
        defaultErrorMessage = 'Select "Export" for a complete view of your data.';    
        String messageForRecordsBelowThreshold = System.Label.Search_Message_Under_Threshold;
        noOfRecordsBelowThreshold = String.isBlank(messageForRecordsBelowThreshold) ? defaultErrorMessage : messageForRecordsBelowThreshold;
    }
    
    public Nbn360SearchHandler_C(){
        stateTerritoryCodes = new List<String>();
        serviceClassList= new List<String>();
        initialiseSettings();
    }
    
    /* This functions creates SOQL string for Location__c object and returns it to the calling function. */
    @TestVisible
    private string buildSoqlQuery(){
        String queryString ='';
     
        queryString = 'Select p.Full_Address__c, p.State_Territory_Code__c, p.Locality_Name__c, p.Service_Class__c, '+
                                        'p.Name, p.NBN_Location_ID__c, p.Id, p.Building_Type__c, p.Disconnection_Date__c, '+
                                        'p.List_Type__c, p.Postcode__c, p.Distribution_Area_Id__c, p.Rollout_Region_ID__c, '+
                                        'p.Expected_date_of_RFS__c, p.Service_Type__c, p.Service_Class_Reason__c, '+
                                        ' p.Service_Class_Description__c From Location__c p';
        String WHERE_CLAUSE = ' WHERE ';    
        Boolean hasFilters = false;
        
        //Check if full address is blank
        if(String.isNotBlank(selectedFullAddressFromTypeAhead)){
            queryString += WHERE_CLAUSE + ' Full_Address__c like \'%'+String.escapeSingleQuotes(selectedFullAddressFromTypeAhead.trim())+'%\'';
            hasFilters = true;
        }
        
        //Check if location ID is blank
        if(String.isNotBlank(selectedLocationIdFromTypeAhead)){
            if(hasFilters)
                queryString +=' AND NBN_Location_ID__c like \'%'+String.escapeSingleQuotes(selectedLocationIdFromTypeAhead.trim())+'%\'';
            else
                queryString += WHERE_CLAUSE + ' NBN_Location_ID__c like \'%'+String.escapeSingleQuotes(selectedLocationIdFromTypeAhead.trim())+'%\'';
            hasFilters = true;
        }
        
        //remove blank character from stateTerritoryCodes list.  
        stateTerritoryCodes = removeBlankStringFromList(stateTerritoryCodes);
        //get states selected
        if( !stateTerritoryCodes.isEmpty()){
            if(hasFilters){
                String filterString = ' AND (';
                queryString += + buildQueryForMultiselectPicklists(stateTerritoryCodes ,'State_Territory_Code__c',filterString);
            }
            else{
                String filterString = ' (';
                queryString += WHERE_CLAUSE + buildQueryForMultiselectPicklists(stateTerritoryCodes ,'State_Territory_Code__c',filterString);
            }
            hasFilters = true;
        }
        
        //remove blank character like '' from list
        serviceClassList = removeBlankStringFromList(serviceClassList);
        if(!serviceClassList.isEmpty()){
            
            String qString='';
            if(hasFilters){
                String filterString = ' AND (';
                queryString += + buildQueryForMultiselectPicklists(serviceClassList ,'Service_Class__c',filterString);
            }
            else{
                String filterString = ' (';    
                queryString += WHERE_CLAUSE + buildQueryForMultiselectPicklists(serviceClassList ,'Service_Class__c',filterString);  
            }
            hasFilters = true;  
        }
        
        //Add locality name filter if it is not blank
        if(String.isNotBlank(localityName)){
            if(hasFilters)
                queryString += ' AND Locality_Name__c like \'%'+String.escapeSingleQuotes(localityName.trim())+'%\'';
            else
                queryString += WHERE_CLAUSE + ' Locality_Name__c like \'%'+String.escapeSingleQuotes(localityName.trim())+'%\'';
            hasFilters = true;
        }
        
        //Add BoundaryId filter if it is not blank
        if(String.isNotBlank(boundaryId)){
            if(hasFilters)
                queryString += ' AND (Rollout_Region_ID__c like \'%'+String.escapeSingleQuotes(boundaryId.trim())+'%\' OR Distribution_Area_ID__c like \'%'+String.escapeSingleQuotes(boundaryId.trim())+'%\')';
            else
                queryString += WHERE_CLAUSE + ' (Rollout_Region_ID__c like \'%'+String.escapeSingleQuotes(boundaryId.trim())+'%\' OR Distribution_Area_ID__c like \'%'+String.escapeSingleQuotes(boundaryId.trim())+'%\')';
            hasFilters = true;
        }
        
        
        //apply disconnection date filters
        if(String.isNotBlank(disconnectionStartDate) || String.isNotBlank(mentionedDisconnectionEndDate )){
           try{ 
                Date startDate;
                
                //If disconnection start date is not blank, then convert it into expected format for SOQL query.
                if(String.isNotBlank(disconnectionStartDate )){
                    startDate = date.parse(disconnectionStartDate);
                    DateTime startDateTime = startDate;
                    disconnectionStartDate = startDateTime.format('yyyy-MM-dd');
                }    
                
                //If disconnection end date is not blank, then convert it into expected format for SOQL query.
                if(String.isNotBlank(mentionedDisconnectionEndDate )){
                    Date endDate = date.parse(mentionedDisconnectionEndDate);
                    DateTime endDateTime = endDate;
                    mentionedDisconnectionEndDate = endDateTime.format('yyyy-MM-dd');
                    
                    //Throw error message if enddate is less than startdate 
                    if(endDate < startDate){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Disconnection end date should be greater than start date'));
                    }
                }
                
                if(hasFilters){
                    String queryPrefix = ' AND ';
                    queryString += buildQueryForDisconnectionDates(disconnectionStartDate, mentionedDisconnectionEndDate, queryPrefix);
                }
                else{
                    String queryPrefix = WHERE_CLAUSE; 
                    queryString += buildQueryForDisconnectionDates(disconnectionStartDate, mentionedDisconnectionEndDate, queryPrefix);
                }
                hasFilters = true;
            }
            catch(Exception ex){
                System.debug(System.LoggingLevel.INFO,'Exception occured !'+ex.getStackTraceString());
                GlobalUtility.logMessage('Error','Nbn360SearchHandler_C','buildSoqlQuery','','','','',ex,0);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Exception occured ! Please contact your System Administrator'));
            }
        }
        
       // String searchThreshold = config.Search_Threshold__c;
        //searchThreshold = (searchThreshold == null || Integer.valueOf(searchThreshold) > 10000 ) ? '10000' : searchThreshold;
        //queryString += ' ORDER BY NBN_Location_ID__c ASC ';
        queryString +=' LIMIT '+ searchThreshold;
        
        return queryString;
    }
    
    /*This method is used to generate query string for Disconeection date field*/
    @TestVisible
    private String buildQueryForDisconnectionDates(String disconnectionStartDate, String disconnectionEndDate, String queryPrefix){
        String queryString='';
        
        //start date and end date is not null/blank , build query for date range
        if(String.isNotBlank(disconnectionStartDate ) && String.isNotBlank(mentionedDisconnectionEndDate )){
            queryString += ' '+ queryPrefix + 'Disconnection_Date__c  >= '+disconnectionStartDate.trim()+' AND Disconnection_Date__c <= '+mentionedDisconnectionEndDate.trim();
        }
        else{
            
            //select '<=' or '>=' depending upon which variable is populated.
            String operator = String.isNotBlank(disconnectionStartDate) ? '>=' : String.isNotBlank(mentionedDisconnectionEndDate) ?  '<=' : '';
            if(String.isNotBlank(operator)){
               String dateValue = String.isNotBlank(disconnectionStartDate) ? disconnectionStartDate.trim(): mentionedDisconnectionEndDate.trim();
               queryString += ' '+ queryPrefix +'  Disconnection_Date__c  '+operator+' '+ dateValue;
            }
        }
        
        return queryString;
    }
    
    /*This method is used to generate query string from service class/ state territory code select options*/
    @TestVisible
    private String buildQueryForMultiselectPicklists(List<String> multiSelectPicklists, String apiNameOfField, String qString){
        String queryString=qString;
        
        for(String selectedString : multiSelectPicklists){
                queryString += ' '+ apiNameOfField + ' = \''+String.escapeSingleQuotes(selectedString.trim())+'\' OR';
        }  
        
        //remove extra "OR" from generated string.
        queryString = queryString.removeEndIgnoreCase('OR');
        queryString +=' ) ';
        
        return queryString;
    }
    
    /*This method is used to remove blank string from service class/ state territory code select option*/
    @TestVisible
    private List<String> removeBlankStringFromList(List<String> pickListEntries){
        set<String> selectOptionSet = new Set<String>();
        selectOptionSet.addAll(pickListEntries);
        selectOptionSet.remove('');
        pickListEntries.clear();
        pickListEntries.addAll(selectOptionSet);
        return pickListEntries;
    }
    
    
    /* This function is responsible searching Locations records */
    public pageReference searchLocations(){
        try{
            locations = new List<Location__c>();
            
            String queryString = buildSoqlQuery();
            system.debug('queryString: '+queryString);
            
            locations = Database.query(queryString);
            Integer locationsCount = locations.size();
             
            /*Show message depending on number of records fetched by query. */
            if((locationsCount >= Integer.valueOf(searchThreshold))){
               recordLimitErrorMessage = noOfRecordsAboveThreshold;
            }
            else{
               recordLimitErrorMessage = noOfRecordsBelowThreshold; 
            }
            return null;
        }
        catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Exception occured ! Please contact your System Administrator'));
            GlobalUtility.logMessage('Error','Nbn360SearchHandler_C','searchLocations','','','','',ex,0); 
            
            return null;
        } 
    }
    
    //get Service_Class__c picklist entries
    public List<Selectoption> getServiceClass(){
        Schema.Describesobjectresult res= Location__c.SobjectType.getDescribe();    
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Location__c.Service_Class__c.getDescribe();
        
        List<Schema.PicklistEntry> serviceClassPicklist = fieldResult.getPicklistValues();
        
        options.add(new SelectOption('','Any service class'));
        for( Schema.PicklistEntry f : serviceClassPicklist){
             options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        return options;
    }
    
    //get State_Territory_Code__c picklist entries
    public List<SelectOption> getTerritoryCodes() {
         List<SelectOption> options = new List<SelectOption>();
         Schema.DescribeFieldResult fieldResult = Location__c.State_Territory_Code__c.getDescribe();
         List<Schema.PicklistEntry> stateTerritoryPicklist = fieldResult.getPicklistValues();
         options.add(new SelectOption('','Any state/territory'));
         for( Schema.PicklistEntry f : stateTerritoryPicklist ){
              options.add(new SelectOption(f.getLabel(), f.getValue()));
         }
        return options;
    }
}