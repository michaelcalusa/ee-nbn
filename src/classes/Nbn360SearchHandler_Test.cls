@isTest
private class Nbn360SearchHandler_Test{
	 
	 @testSetup static void setupTestData(){
	 	list<Location__c> locations = new list<location__c>();
	 	Integer locCnt;
	 	for(locCnt=1; locCnt <=50; locCnt++){
	 		Location__c loc = new Location__c();
	 		loc.Full_Address__c = 'test address'+locCnt;
	 		loc.NBN_Location_ID__c = 'LOC000000'+locCnt;
	 		loc.State_Territory_Code__c = 'NSW';
	 		loc.Service_Class__c = '1';
	 		loc.Locality_Name__c = 'Brainburn';
	 		loc.Rollout_Region_ID__c = 'NCRH-20';
	 		loc.Distribution_Area_ID__c = 'NCRH-20-7272-HDHDH';
	 		loc.Disconnection_Date__c = date.newinstance(2014,12,25);
	 		locations.add(loc);
	 	}
	 	
	 	//create locations for different state, service class, rollout region Id
	 	for(locCnt=51; locCnt <=100; locCnt++){
	 		Location__c loc = new Location__c();
	 		loc.Full_Address__c = 'test address'+locCnt;
	 		loc.NBN_Location_ID__c = 'LOC000111'+locCnt;
	 		loc.State_Territory_Code__c = 'WA';
	 		loc.Service_Class__c = '3';
	 		loc.Locality_Name__c = 'EAST VICTORIA PARK';
	 		loc.Rollout_Region_ID__c = '6VIC-10';
	 		loc.Distribution_Area_ID__c = '6VIC-10-20';
	 		loc.Disconnection_Date__c = date.newinstance(2015,06,25); // yyyy,mm,dd
	 		locations.add(loc);
	 	}
	 	//Insert locations records
	 	Database.insert(locations);
	 	
	 	//Insert custom setting record
	 	nbn360_Configurations__c config = new nbn360_Configurations__c();
	 	//config.Search_Threshold__c = '10000';
	 	config.Search_Table_Page_Size__c = 100;
	 	config.Search_Export_Report_Id__c='abc';
	 	insert config;
	 	
	 }
	 
	 static testMethod void Test_PicklistEntries(){
		Nbn360SearchHandler_C nbn360 = new Nbn360SearchHandler_C();
		List<SelectOption> territoryCodes =  nbn360.getTerritoryCodes();
		List<SelectOption> serviceClass =  nbn360.getServiceClass();
		
		system.assertNotEquals(null, territoryCodes);
		system.assertNotEquals(null, serviceClass);
	 }
	 
	 static testMethod void Test_Filters_For_SoqlQuery(){
		Nbn360SearchHandler_C nbn360 = new Nbn360SearchHandler_C();
		nbn360.selectedFullAddressFromTypeAhead = 'test address';
		nbn360.stateTerritoryCodes.add('NSW');
		nbn360.serviceClassList.add('1');
		nbn360.localityName = 'Brainburn';
		nbn360.disconnectionStartDate = '25/12/2014';
		nbn360.selectedLocationIdFromTypeAhead = 'LOC000000';
		
		nbn360.searchLocations();
		
		system.assertEquals(50, nbn360.locations.size());
		
	 }
	 
	 static testMethod void Test_MultiSelect_Filters_For_SoqlQuery(){
		Nbn360SearchHandler_C nbn360 = new Nbn360SearchHandler_C();
		nbn360.selectedFullAddressFromTypeAhead = 'test address';
		nbn360.stateTerritoryCodes.add('NSW');
		nbn360.stateTerritoryCodes.add('WA');
		nbn360.serviceClassList.add('1');
		nbn360.serviceClassList.add('3');
		nbn360.disconnectionStartDate = '25/12/2014';
		nbn360.mentionedDisconnectionEndDate = '25/06/2015';
		nbn360.selectedLocationIdFromTypeAhead = 'LOC000';
		
		nbn360.searchLocations();
		
		system.assertEquals(100, nbn360.locations.size());
	 }
	 
	 static testMethod void Test_SearchMatchingNoRecords(){
	 	nbn360_Configurations__c config = nbn360_Configurations__c.getOrgDefaults();
	 	
	 	Nbn360SearchHandler_C nbn360 = new Nbn360SearchHandler_C();
		nbn360.selectedFullAddressFromTypeAhead = 'Sydney';
		nbn360.searchLocations();
		system.assertEquals(0, nbn360.locations.size());
		system.assertEquals(Label.Search_Message_For_No_Result, nbn360.noResultFromQueryMessage);
	 }
	 
	 static testMethod void Test_FullAddress_Search(){
	 	Nbn360SearchHandler_C nbn360 = new Nbn360SearchHandler_C();
		nbn360.selectedFullAddressFromTypeAhead = 'test address';
		nbn360.searchLocations();
		system.assertEquals(100, nbn360.locations.size());
	 }
	 
	 static testMethod void Test_NbnLocationId_Search(){
	 	Nbn360SearchHandler_C nbn360 = new Nbn360SearchHandler_C();
		nbn360.selectedLocationIdFromTypeAhead = 'LOC000111';
		nbn360.searchLocations();
		system.assertEquals(50, nbn360.locations.size());
	 }
	 
	 
	 static testMethod void Test_StateTerritoryCodes_Search(){
	 	Nbn360SearchHandler_C nbn360 = new Nbn360SearchHandler_C();
		
		//Check with multiple states selected.
		nbn360.stateTerritoryCodes.add('NSW');
		nbn360.stateTerritoryCodes.add('WA');
		nbn360.searchLocations();
		system.assertEquals(100, nbn360.locations.size());
	 	
	 	
	 	//Check with only on state selected.
	 	nbn360.stateTerritoryCodes.clear();
	 	nbn360.stateTerritoryCodes.add('NSW');
	 	nbn360.searchLocations();
		system.assertEquals(50, nbn360.locations.size());
	 	
	 }
	 
	 
	 static testMethod void Test_ServiceClassList_Search(){
	 	Nbn360SearchHandler_C nbn360 = new Nbn360SearchHandler_C();
		nbn360.serviceClassList.add('1');
		nbn360.serviceClassList.add('3');
		nbn360.searchLocations();
		system.assertEquals(100, nbn360.locations.size());
	 
		
		//Check with only on state selected.
	 	nbn360.serviceClassList.clear();
	 	nbn360.serviceClassList.add('1');
	 	nbn360.searchLocations();
		system.assertEquals(50, nbn360.locations.size());
	 
	 }
	 
	 
	 static testMethod void Test_LocalityName_Search(){
	 	Nbn360SearchHandler_C nbn360 = new Nbn360SearchHandler_C();
		nbn360.localityName = 'Brainburn';
		nbn360.searchLocations();
		
		system.assertEquals(50, nbn360.locations.size());
	 }
	 
	 static testMethod void Test_BoundaryId_Search(){
	 	Nbn360SearchHandler_C nbn360 = new Nbn360SearchHandler_C();
		nbn360.boundaryId = 'NCRH-20';
		nbn360.searchLocations();
		
		system.assertEquals(50, nbn360.locations.size());
	 }
	 
	 static testMethod void Test_DisconnectionDate_Search(){
	 	Nbn360SearchHandler_C nbn360 = new Nbn360SearchHandler_C();
		nbn360.disconnectionStartDate = '25/12/2014';
		nbn360.mentionedDisconnectionEndDate = '25/12/2016';
		nbn360.searchLocations();
		system.assertEquals(100, nbn360.locations.size());
		
		nbn360.disconnectionStartDate = '27/12/2016';
		nbn360.mentionedDisconnectionEndDate = '25/12/2016';
		nbn360.searchLocations();
		system.debug('TEST MESSAGE: '+ApexPages.hasMessages());
		system.assertEquals(true, ApexPages.hasMessages());
		
		
	 }
	 
	 static testMethod void Test_DisconnectionDate_Exception(){
	 	Nbn360SearchHandler_C nbn360 = new Nbn360SearchHandler_C();
		nbn360.disconnectionStartDate = 'abc';
		nbn360.searchLocations();
		
		List<Application_Log__c> logs = [Select Id from Application_Log__c];
		system.assertNotEquals(null, logs.size());
		
	 }
	 
	 static testMethod void Test_RecordsGreaterThanDisconnectionDate(){
	 	Nbn360SearchHandler_C nbn360 = new Nbn360SearchHandler_C();
		nbn360.disconnectionStartDate = '24/12/2014';
		nbn360.searchLocations();
		system.assertEquals(100, nbn360.locations.size());
		
		

	 }
	
}