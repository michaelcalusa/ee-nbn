/*------------------------------------------------------------------------
Author:        Sanghita Das
Company:       Appirio
Description:   Handler class for inboundNetworkInventoryIntegration
Test Class:    NetworkInventoryIntegrationHandler_Test
History
30/01/2018     Sanghita Das   Created refer CUSTSA-10775
--------------------------------------------------------------------------*/

public class NetworkInventoryIntegrationHandler{
    
    
    public static void NtworkInv(List<NetworkInventoryInboundIntegration__e > lstEvents){
        
        // Prepare map for responses
        Map<String, AltConWrapper.InboundJSONMessage> mapResponse = new Map<String,AltConWrapper.InboundJSONMessage>();
        for (NetworkInventoryInboundIntegration__e event: lstEvents) {
            String str = String.valueOf(event.InboundJSONMessage__c);
            str = str.replaceFirst('exception', 'exception_x');
            
            GlobalUtility.logMessage(GlobalConstants.INFO_RECTYPE_NAME,'NetworkInventory','AltCon','test','','', str, null, 0);
                
            
            AltConWrapper.InboundJSONMessage objWrapper = new AltConWrapper.InboundJSONMessage();
            objWrapper = (AltConWrapper.InboundJSONMessage)JSON.deserialize(str, AltConWrapper.InboundJSONMessage.Class);
            
            if(objWrapper.metadata != null && String.isBlank(objWrapper.metadata.nbnCorrelationId) == false){
                mapResponse.put(objWrapper.metadata.nbnCorrelationId, objWrapper);
            }
        }
        System.debug('^^^' + mapResponse);
        
        
        // Prepare map for Tasks
        Map<String, Task> mapTasks = new Map<String, Task>();
        if(mapResponse.keyset().size() > 0){
            for(task taskRecord :[SELECT Id, PNI_Inbound_Int_Stat__c, Pair_Network_Cable_Id__c, NBNCorrelationId__c, 
                                    Network_Inventory_Error_Code__c, Network_Inventory_Swap_Status__c 
                                    FROM Task WHERE NBNCorrelationId__c IN :mapResponse.keyset()]){
                mapTasks.put(taskRecord.NBNCorrelationId__c, taskRecord);
            }
        }
        System.debug('^^^' + mapTasks);
        
        // Prepare list to update Tasks.
        List<Task> taskToUpdate = new List<task>();
        for(Task t :mapTasks.values()){
            AltConWrapper.InboundJSONMessage objRes = mapResponse.get(t.NBNCorrelationId__c);
            
            if(objRes.status == Label.Success){
                // Label.Success_Screen_Message = Distribution pair swap was successful	
                t.PNI_Inbound_Int_Stat__c = Label.Success_Screen_Message;
                t.Pair_Network_Cable_Id__c = objRes.pairConnected;
                t.Network_Inventory_Swap_Status__c = objRes.status;
                
                // if success remove error messages history.
                t.Network_Inventory_Error_Code__c = '';
                t.error__c = ''; 
                
                taskToUpdate.add(t);
            }
            
            else If(objRes.status == Label.Failure){
                // Failure_Screen_Message = Distribution pair swap was unsuccessful
                // Label.NetworkInventoryException = Go to PNI to investigate further.
                t.PNI_Inbound_Int_Stat__c = Label.Failure_Screen_Message;
                if(objRes.exception_x != null){
                	t.Network_Inventory_Error_Code__c = objRes.exception_x.code;
                	t.error__c = objRes.exception_x.code + ': ' + objRes.exception_x.message;// + Label.NetworkInventoryException;
                }
                t.Network_Inventory_Swap_Status__c = objRes.status;
                
                
                taskToUpdate.add(t);
            }
        }
        
        System.debug('^^^' + taskToUpdate);
        update taskToUpdate;
    }
}