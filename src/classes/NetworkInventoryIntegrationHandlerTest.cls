@isTest
private class NetworkInventoryIntegrationHandlerTest {
    
    // Test Data
    @testSetup static void setup() {
        Task objTask = new Task();
        objTask.RecordTypeId = GlobalCache.getRecordTypeId('Task','GCC Interaction');
        objTask.PRI__c = 'PRI003000710422';
        objTask.Reason_for_Call__c = 'Pair Swap';
        objTask.Additional_Comments__c = 'Some Description';
        objTask.Additional_Info__c = 'Some Description';
        objTask.Any_follow_up_action_advised__c = 'Some Description';
        objTask.Bypassed_DPU_port_number__c = '22';
        objTask.Cable_Record_Enquiry_Details__c = 'Some Description';
        objTask.CIU_DPU_ID__c = 'Some Description';
        objTask.C_Pair_Destination__c = '22';
        objTask.C_Pair_Source__c = '22';
        objTask.CPI_ID__c = '';
        objTask.Details_of_temp_fix_provided__c = 'Yes';
        objTask.Has_the_hazard_been_made_safe__c = 'Yes';
        objTask.Hazard_Details__c = 'Some Description';
        objTask.Inventory_Update__c ='Some Description';
        objTask.Lead_in_Pair__c = 'Some Description';
        objTask.M_Pair_Destination__c = '22';
        objTask.M_Pair_Source__c = '22';
        objTask.NAR_SR_Created__c = 'Yes';
        objTask.NAR_SR_number__c = '22';
        objTask.O_Pair_Destination__c = '22';
        objTask.O_Pair_Source__c = '22';
        objTask.Pillar_ID_Destination__c = '22';
        objTask.Pillar_ID_Source__c = '22';
        objTask.Reason_for_CIU_By_pass__c = 'Some Description';
        objTask.Severity_of_the_Hazard__c = 'Some Description';
        objTask.Site_Contact__c = 'Some Description';
        objTask.Source_of_Hazard__c = 'Some Description';
        objTask.Telstra_Reference_number__c = 'Some Description';
        objTask.Telstra_Tech_Contact_number__c = '0400000000';
        objTask.Telstra_Tech_ID__c = 'Some Description';
        objTask.Telstra_Tech_name__c = 'Some Description';
        objTask.X_Pair_Destination__c = '22';
        objTask.X_Pair_Source__c = '22';
        objTask.NBNCorrelationId__c= '0e8cedd0-ad98-11e6-bf2e-47644ada7c0f'; 
        objTask.Network_Inventory_Swap_Status__c=Label.Success; 
        insert objTask;
    }
    
    //success
    @isTest static void testMethod1() {
        Test.startTest();
        
        String strJSON_1 = '{"transactionId":"aeb8a1007f60271d","metadata":{"nbnCorrelationId":"0e8cedd0-ad98-11e6-bf2e-47644ada7c0f","tokenId":"3c08d723a26f1234","copperPathId":"CPI300000403245","changeReason":"DATA-CORRECTION","requestedPair":"P:12"},"copperPathId":"CPI300000403245","status":"Success","pairConnected":"abcde:123"}';


        NetworkInventoryInboundIntegration__e inbound = new NetworkInventoryInboundIntegration__e (InboundJSONMessage__c = strJSON_1);
        
        EventBus.publish(inbound);
        
        Test.stopTest();
    }
    
    //Failed
    @isTest static void testMethod2() {
        Test.startTest();
        
        String strJSON_1 = '{"transactionId":"aeb8a1007f60271d","metadata":{"nbnCorrelationId":"0e8cedd0-ad98-11e6-bf2e-47644ada7c0f","tokenId":"3c08d723a26f1234","copperPathId":"CPI300000403245","changeReason":"DATA-CORRECTION","requestedPair":"P:12"},"copperPathId":"CPI300000403245","status":"Failure","pairConnected":"abcde:123"}';


        NetworkInventoryInboundIntegration__e inbound = new NetworkInventoryInboundIntegration__e (InboundJSONMessage__c = strJSON_1);
        
        EventBus.publish(inbound);
        
        Test.stopTest();
    }
}