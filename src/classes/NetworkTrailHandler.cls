/***************************************************************************************************
Class Name:         NetworkTrailHandler
Class Type:         Handler Class 
Version:            1.0 
Created Date:       14 March 2018
Function:           This class handles the incoming network trail events 
Input Parameters:   None 
Output Parameters:  None
Description:        Called from NetworkTrail Trigger
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                

* Sanghita Das      15/12/2017      Refer CUSTSA-7909 for Epic description
****************************************************************************************************/ 

public class NetworkTrailHandler{
    
    public static Boolean OPair = false;
    public static Boolean MPair = false;
    
    public static void NtworkInv(List<NetworkTrail__e > lstTrails){
        
        List<task> taskToUpdate=new List<task>();
        List<Task_Custom__c> custTaskList= new List<Task_Custom__c>();
        Map<String, AltConWrapper.NetWorkTrialResp > ObjMap =new Map<String, AltConWrapper.NetWorkTrialResp >();
        Map<String,Task> TaskMap= new Map<String,Task>();
        Map<String,Task_Custom__c> TaskCustMap= new Map<String,Task_Custom__c>();
        
        
        if (lstTrails != null && lstTrails.size() > 0){
            for (NetworkTrail__e event: lstTrails) {
                System.debug('^^^ InboundJSONMessage__c' + event.InboundJSONMessage__c);
                
                GlobalUtility.logMessage(GlobalConstants.INFO_RECTYPE_NAME,'NetworkTrailPlatformEvent','AltCon','test','','', event.InboundJSONMessage__c, null, 0);
                
                String JSONmsg = String.valueOf(event.InboundJSONMessage__c);
                
                JSONmsg = JSONmsg.replaceFirst('exception', 'exception_x');
                
                AltConWrapper.NetWorkTrialResp responseObject = new AltConWrapper.NetWorkTrialResp();
                
                responseObject = (AltConWrapper.NetWorkTrialResp)JSON.deserialize(JSONmsg, AltConWrapper.NetWorkTrialResp.Class); 
                
                JSONParser parser = JSON.createParser(JSONmsg);
                
                if(responseObject.metadata != null && responseObject.metadata.accessServiceTechnologyType != null){ 
                    
                    // FTTN & FTTC
                    if(
                        (responseObject.metadata.accessServiceTechnologyType.contains('Fibre To The Node'))  ||
                    (responseObject.metadata.accessServiceTechnologyType.contains('Fibre To The Building'))  ||
                    (responseObject.metadata.accessServiceTechnologyType.contains('Fibre To The Curb') && responseObject.pillars != null && responseObject.pillars.size()>1 ) ){
                        
                        while (parser.nextToken() != null) {
                            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                                String fieldName = parser.getText();                
                                parser.nextToken();  
                                String fieldValue = parser.getText();
                                
                                //System.debug('^^^' + fieldName +' :: ' + fieldValue);
                                
                                if(fieldName.contains('subtype') && (fieldValue.contains('OPair') || fieldValue.contains('O Pair') )) {
                                    OPair = true;
                                }
                                
                                if(fieldName.contains('subtype') && (fieldValue.contains('MPair') || fieldValue.contains('M Pair') )) {
                                    MPair = true;
                                }
                                
                               
                                
                                if(fieldName.contains('twistedPairName') && fieldValue != null && OPair == true) {
                                    //System.debug('^^^responseObject.Opair ' + responseObject.Opair);
                                    
                                    
                                    if (responseObject.OPair == null){
                                        responseObject.OPair = fieldValue;
                                    }
                                    
                                    if (responseObject.OPair != null && responseObject.pillars.size() > 1){
                                        responseObject.OPair += ',' + fieldValue;
                                    }
                                    
                                    OPair = false;
                                    
                                    //System.debug('^^^responseObject.Opair ' + responseObject.Opair);                       
                                }
                                
                                
                                if(fieldName.contains('twistedPairName') && fieldValue != null && MPair == true) {
                                    //System.debug('^^^responseObject.MPair ' + responseObject.MPair);
                                    
                                    if (responseObject.MPair == null){
                                        responseObject.MPair = fieldValue;
                                    }
                                    
                                    if (responseObject.MPair != null && responseObject.pillars.size() > 1){
                                        responseObject.MPair += ',' + fieldValue;
                                    }
                                    
                                    MPair = false;
                                    
                                                            
                                }
                            }
                        }
                    }
                    
                    System.debug('^^^responseObject.MPair ' + responseObject.MPair); 
                    System.debug('^^^responseObject.OPair ' + responseObject.OPair);
                    
                    
                    if(responseObject.metadata.accessServiceTechnologyType.contains('Fibre To The Curb') && responseObject.pillars != null && responseObject.pillars.size() <= 1){
                        if(responseObject.copperDistributionCables != null){
                            responseObject.OPair = responseObject.copperDistributionCables[0].twistedPairName;
                        }
                    }
                    System.debug('^^^responseObject.OPair ' + responseObject.OPair);
                    
    
                }
                
                System.debug('^^^responseObject.OPair##' + responseObject.OPair);
                System.debug('^^^responseObject.MPair##' + responseObject.MPair);
                ObjMap.put(responseObject.metadata.nbnCorrelationId, responseObject);           
            }
            
            
            System.debug('^^^ObjMap.keyset()' + ObjMap.keyset());
            
            
            
            if(ObjMap.keyset().size() > 0 && ObjMap.keyset() != null){
                
                for(Task taskRecord :[SELECT Id, Copper_Lead_In_Cable_Pair__c, NetworkTrailCoRelationID__c, NBNCorrelationId__c, TCO_or_MDF__c, Copper_Lead_In_Cable__c, Copper_Distribution_Cable__c, O_Pair__c, 
                        Copper_Main_Cable__c, M_pair__c, FTTN_Node__c, Type_Of_Node__c, NetworkTrail_Status__c 
                        FROM Task WHERE NBNCorrelationId__c in :ObjMap.keyset()]){
                            
                            TaskMap.put(taskRecord.NBNCorrelationId__c,taskRecord);
                }
                
                
                
                for(Task_Custom__c  custTaskRecord :[SELECT Id, NetworkTrailCoRelationID__c, Opair__c, MPair__c FROM Task_Custom__c WHERE NetworkTrailCoRelationID__c IN:ObjMap.keyset()]){                
                    
                    TaskCustMap.put(custTaskRecord.NetworkTrailCoRelationID__c,custTaskRecord);
                    
                }  
            }
            
            
            System.debug('^^^TaskMap' + TaskMap);
            System.debug('^^^TaskCustMap' + TaskCustMap);
            
            
            
            for(Task t: TaskMap.values()){
                AltConWrapper.NetWorkTrialResp InboundJSONMsg = ObjMap.get(t.NBNCorrelationId__c);
                
                Task_Custom__c custTask = TaskCustMap.get(t.NBNCorrelationId__c);
                
                system.debug('^^^custTask::' + custTask); 
                system.debug('^^^Task t '+ t);
                system.debug('^^^InboundJSONMsg' + InboundJSONMsg);
                
                If(InboundJSONMsg!=null){
                    if (InboundJSONMsg.status!=null ){
                        t.NetworkTrail_Status__c=InboundJSONMsg.status;
                    }
                    
                    
                    If(InboundJSONMsg.status == 'Completed' && InboundJSONMsg.exception_x == null){
                        
                        if (InboundJSONMsg.tco != null){
                            if (InboundJSONMsg.tco.name != '' && InboundJSONMsg.tco.name != null){
                                t.TCO_or_MDF__c =InboundJSONMsg.tco.name;
                            }
                        }
                        
                        // BUILDING MDF
                        if (InboundJSONMsg.fttbSite != null && InboundJSONMsg.fttbSite.mdfs != null && InboundJSONMsg.fttbSite.mdfs.size() > 0){
                            for(AltConWrapper.mdfs objMDFS :InboundJSONMsg.fttbSite.mdfs){
                                if (objMDFS.subtype == 'BUILDING_MDF' && objMDFS.chassisSubtype == 'MDF_C'){
                                    t.NetworkTrailCoRelationID__c = objMDFS.Id;
                                }
                            }
                        }
                        
                        
                        
                        if (InboundJSONMsg.mdf != null){
                            if (InboundJSONMsg.mdf.name != '' && InboundJSONMsg.mdf.name != null){
                                t.TCO_or_MDF__c =InboundJSONMsg.mdf.name;
                            }
                        }
                        
                        if (InboundJSONMsg.copperLeadinCable != null){    
                            if (InboundJSONMsg.copperLeadinCable.name != null){
                                t.Copper_Lead_In_Cable__c=InboundJSONMsg.copperLeadinCable.name;
                                t.Copper_Lead_In_Cable_Pair__c=InboundJSONMsg.copperLeadinCable.twistedPairId;
                            }
                        }
                        
                        //DPU 
                        if (InboundJSONMsg.Dpu!=null){
                            if (InboundJSONMsg.Dpu.serialNumber !=null) t.DPU_Serial_Number__c=InboundJSONMsg.Dpu.serialNumber ;
                            else if(InboundJSONMsg.Dpu.Id !=null) t.CIU_DPU_ID__c=InboundJSONMsg.Dpu.Id ;
                        }
                        
                        
                        
                        //  CIU 
                        if(InboundJSONMsg.ciu!=null){ 
                            if (InboundJSONMsg.ciu.twistedPairName!=null) 
                            {
                                String s1=(InboundJSONMsg.ciu.twistedPairName).substringAfter(':');system.debug('@@s1'+s1);
                                t.DPU_Port_Number__c=s1;
                            }
                            if (InboundJSONMsg.ciu.twistedPairStatus!=null) t.CIU_Cut_In_Status__c=InboundJSONMsg.ciu.twistedPairStatus; 
                        }
                        
                        
                        if(InboundJSONMsg.copperDistributionCables != null){  
                            if (InboundJSONMsg.copperDistributionCables[0].name!=null){
                                t.Copper_Distribution_Cable__c = InboundJSONMsg.copperDistributionCables[0].name;
                            }
                        }
                        
                        system.debug('^^^@@InboundJSONMsg.Opair' + InboundJSONMsg.Opair + '' + '^^^task Value   '+ t);
                        
                        
                        // Added as part of CUSTSA-9093
                        if (InboundJSONMsg.Opair!=null && InboundJSONMsg.Opair.contains(',')){
                            List<string> oPairList = new List<string>();
                            for (String str :InboundJSONMsg.Opair.split(',')){
                                if (!oPairList.contains(str))
                                    oPairList.add(str);
                            }
                            if (!oPairList.isEmpty()){
                                custTask.Opair__c = String.join(oPairList, ',');
                            }
                            
                        }
                        else if (InboundJSONMsg.Opair != null && !InboundJSONMsg.Opair.contains(',')) {
                            t.O_Pair__c = InboundJSONMsg.Opair;  
                        }
                        
                        
                        // Added as part of CUSTSA-9093
                        if (InboundJSONMsg.Mpair!=null && InboundJSONMsg.Mpair.contains(','))
                        {   
                            List<string> mPairList = new List<string>();
                            for (String str :InboundJSONMsg.Mpair.split(',')){
                                 if (!mPairList.contains(str))
                                    mPairList.add(str);
                            }
                            if (!mPairList.isEmpty())
                                custTask.Mpair__c = String.join(mPairList, ',');
                        }
                        else if (InboundJSONMsg.Mpair != null && !InboundJSONMsg.Mpair.contains(',')) {
                            t.M_pair__c=InboundJSONMsg.Mpair;  
                        }
                        
                        
                        if (InboundJSONMsg.copperMainCable != null){
                            if (InboundJSONMsg.copperMainCable.name != null){
                                t.Copper_Main_Cable__c = InboundJSONMsg.copperMainCable.name;
                            }
                            
                            // Commented out as part of CUSTSA-9093
                            if (InboundJSONMsg.copperMainCable.twistedPairName != null && InboundJSONMsg.Mpair != null){
                                if(InboundJSONMsg.Mpair.contains(',') == false){
                                    t.M_pair__c=InboundJSONMsg.copperMainCable.twistedPairName;
                                }
                            }
                        }
                        
                        
                        
                        if (InboundJSONMsg.fttnNode != null){
                            if (InboundJSONMsg.fttnNode.id!=null)t.FTTN_Node__c=InboundJSONMsg.fttnNode.id;
                            if (InboundJSONMsg.fttnNode.type!=null)t.Type_Of_Node__c=InboundJSONMsg.fttnNode.type;
                            if (InboundJSONMsg.fttnNode.subtype!=null )t.Type_Of_Node__c=InboundJSONMsg.fttnNode.subtype;
                        }
                        
                        taskToUpdate.add(t);
                        custTaskList.add(custTask);
                    }
                    else If(InboundJSONMsg.status == Label.Failure || InboundJSONMsg.exception_x != null ){              
                        t.Error_trial__c=InboundJSONMsg.exception_x.message+Label.NetworkInventoryException;  
                        taskToUpdate.add(t);
                        //custTaskList.add(custTask);
                    } 
                }   
            }
             
            if(taskToUpdate.size() > 0){
                System.debug('^^^taskToUpdate' + taskToUpdate);
                update taskToUpdate ; 
            }
            
            if(custTaskList.size() > 0){
                System.debug('^^^custTaskList' + custTaskList);
                update custTaskList;  
            }
        }
    }          
}