@isTest
private class NetworkTrailHandler_Test {
     static String jsonFTTB = '{                                                                        ' + 
'  "transactionId": "aeb8a1007f60271d",                                   ' + 
'  "status": "Completed",                                                 ' + 
'  "metadata": {                                                          ' + 
'    "nbnCorrelationId": "0e8cedd0-ad98-11e6-bf2e-47644ada7c0f",          ' + 
'    "tokenId": "3c08d723a26f1234",                                       ' + 
'    "copperPathId": "CPI300004189161",                                   ' + 
'    "accessServiceTechnologyType": "Fibre To The Building",              ' + 
'    "incidentId": "INC000000000012"                                      ' + 
'  },                                                                     ' + 

'  "tco":{                                                                ' + 
'    "id":"148689111451627491",                                           ' + 
'    "name":"",                                                           ' + 
'    "type":"Premise Equipment",                                          ' + 
'    "subtype":"TCO",                                                     ' + 
'    "status":"In Service",                                               ' + 
'    "platformReferenceId":"1291492788",                                  ' + 
'    "locationId":"",                                                     ' + 
'    "locationLatitude":"",                                               ' + 
'    "locationLongitude":""                                               ' + 
'  },                                                                     ' + 

'  "dpu":{                                                                ' + 
'    "id":"148689111451627491",                                           ' + 
'    "name":"",                                                           ' + 
'    "type":"Premise Equipment",                                          ' + 
'    "subtype":"TCO",                                                     ' + 
'    "status":"In Service",                                               ' + 
'    "platformReferenceId":"1291492788",                                  ' + 
'    "locationId":"",                                                     ' + 
'    "locationLatitude":"",                                               ' + 
'    "locationLongitude":""                                               ' + 
'  }, ' +

'  "ciu":{                                                                ' + 
'    "id":"148689111451627491",                                           ' + 
'    "name":"",                                                           ' + 
'    "type":"Premise Equipment",                                          ' + 
'    "subtype":"TCO",                                                     ' + 
'    "status":"In Service",                                               ' + 
'    "platformReferenceId":"1291492788",                                  ' + 
'    "locationId":"",                                                     ' + 
'    "locationLatitude":"",                                               ' + 
'    "locationLongitude":""                                               ' + 
'  }, ' +




'  "fttbSite": {                                                          ' + 
'    "id": "4ALL-20-002-FBU-0002",                                        ' + 
'    "type": "Site",                                                      ' + 
'    "subtype": "FTTB",                                                   ' + 
'    "status": "Designed",                                                ' + 
'    "platformReferenceId": "1744230689_support",                         ' + 
'    "locationLatitude": "-28.0374770678",                                ' + 
'    "locationLongitude": "151.979218011",                                ' + 
'    "mdfs": [                                                            ' + 
'      {                                                                  ' + 
'        "id": "4ALL-20-002-FRM-0003",                                    ' + 
'        "type": "Distribution Frame",                                    ' + 
'        "subtype": "BUILDING_MDF",                                       ' + 
'        "status": "Designed",                                            ' + 
'        "platformReferenceId": "1744230774",                             ' + 
'        "chassisId": "4ALL-20-002-CBL-0002",                             ' + 
'        "chassisType": "Chassis",                                        ' + 
'        "chassisSubtype": "MDF_C",                                       ' + 
'        "chassisStatus": "Designed",                                     ' + 
'        "chassisPlatformReferenceId": "1744230797",                      ' + 
'        "portId": "9",                                                   ' + 
'        "portName": "TBD",                                               ' + 
'        "portPlatformReferenceId": "1744231281",                         ' + 
'        "portStatus": "Designed"                                         ' + 
'      },                                                                 ' + 
'      {                                                                  ' + 
'        "id": "4ALL-20-002-FRM-0004",                                    ' + 
'        "type": "Distribution Frame",                                    ' + 
'        "subtype": "BUILDING_MDF",                                       ' + 
'        "status": "Designed",                                            ' + 
'        "platformReferenceId": "1744230775",                             ' + 
'        "chassisId": "4ALL-20-002-XBL-0002",                             ' + 
'        "chassisType": "Chassis",                                        ' + 
'        "chassisSubtype": "MDF_X",                                       ' + 
'        "chassisStatus": "Designed",                                     ' + 
'        "chassisPlatformReferenceId": "1744230799",                      ' + 
'        "portId": "9",                                                   ' + 
'        "portName": "TBD",                                               ' + 
'        "portPlatformReferenceId": "1744232681",                         ' + 
'        "portStatus": "Designed"                                         ' + 
'      }                                                                  ' + 
'    ]                                                                    ' + 
'  },                                                                     ' + 
'  "copperLeadinCable":{                                                  ' + 
'    "id":"182219421351754719",                                           ' + 
'    "name":"4TBA0111DPU001:4-4|182219421351754719:2-2",                  ' + 
'    "type":"Cable",                                                      ' + 
'    "subtype":"CLS",                                                     ' + 
'    "status":"In Service",                                               ' + 
'    "platformReferenceId":"1291492811",                                  ' + 
'    "length":"35.16",                                                    ' + 
'    "lengthType":"",                                                     ' + 
'    "installationType":"Underground",                                    ' + 
'    "twistedPairId":"1",                                                 ' + 
'    "twistedPairName":"4TBA0111DPU001:4",                                ' + 
'    "twistedPairPlatformReferenceId":"1291492813",                       ' + 
'    "twistedPairStatus":"In Service",                                    ' + 
'    "twistedPairInstallationStatus":"Installed"                          ' + 
'  },                                                                     ' + 
'  "copperDistributionCables": [                                          ' + 
'    {                                                                    ' + 
'      "id": "4ALL-20-01-010",                                            ' + 
'      "name": "BALAP183:1-40|4ALL2001010:41-800",                        ' + 
'      "type": "Cable",                                                   ' + 
'      "subtype": "CDS",                                                  ' + 
'      "status": "In Service",                                            ' + 
'      "platformReferenceId": "1746135739",                               ' + 
'      "length": "61.087",                                                ' + 
'      "lenghtType": "Model",                                             ' + 
'      "twistedPairId": "9",                                              ' + 
'      "twistedPairName": "BALAP183:9",                                   ' + 
'      "twistedPairPlatformReferenceId": "1746135740",                    ' + 
'      "twistedPairStatus": "Designed",                                   ' + 
'      "twistedPairInstallationStatus": "Installed"                       ' + 
'    }                                                                    ' + 
'  ],                                                                     ' + 
'  "pillars": [                                                           ' + 
'    {                                                                    ' + 
'      "id": "166297062255658086",                                        ' + 
'      "type": "Pillar",                                                  ' + 
'      "subtype": "CCU",                                                  ' + 
'      "status": "In Service",                                            ' + 
'      "platformReferenceId": "1746133627",                               ' + 
'      "terminationModules": [                                            ' + 
'        {                                                                ' + 
'          "id": "166297062255658086_O",                                  ' + 
'          "type": "Termination Module",                                  ' + 
'          "subtype": "O Pair",                                           ' + 
'          "status": "In Service",                                        ' + 
'          "platformReferenceId": "1746133632",                           ' + 
'          "twistedPairId": "9",                                          ' + 
'          "twistedPairName": "BALAP183:9",                               ' + 
'          "twistedPairPlatformReferenceId": "1746133632_9",              ' + 
'          "twistedPairStatus": "In Service",                             ' + 
'          "twistedPairInstallationStatus": "Installed"                   ' + 
'        },                                                               ' + 
'        {                                                                ' + 
'          "id": "166297062255658086_M",                                  ' + 
'          "type": "Termination Module",                                  ' + 
'          "subtype": "M Pair",                                           ' + 
'          "status": "In Service",                                        ' + 
'          "platformReferenceId": "1746133629",                           ' + 
'          "twistedPairId": "9",                                          ' + 
'          "twistedPairName": "AAAB1:9",                                  ' + 
'          "twistedPairPlatformReferenceId": "1746133629_9",              ' + 
'          "twistedPairStatus": "In Service",                             ' + 
'          "twistedPairInstallationStatus": "Installed"                   ' + 
'        }                                                                ' + 
'      ]                                                                  ' + 
'    }                                                                    ' + 
'  ],                                                                     ' + 
'  "copperMainCable": {                                                   ' + 
'    "id": "107881237589457119",                                          ' + 
'    "name": "AAAB1:1-1200",                                              ' + 
'    "type": "Cable",                                                     ' + 
'    "subtype": "CMS",                                                    ' + 
'    "status": "In Service",                                              ' + 
'    "platformReferenceId": "1746133659",                                 ' + 
'    "twistedPairId": "9",                                                ' + 
'    "twistedPairName": "AAAB1:9",                                        ' + 
'    "twistedPairPlatformReferenceId": "1746133660",                      ' + 
'    "twistedPairStatus": "In Service",                                   ' + 
'    "twistedPairInstallationStatus": "Installed"                         ' + 
'  }                                                                      ' + 
'}';


    // Test Data
    @testSetup static void setup() {
        Task objTask = new Task();
        objTask.RecordTypeId = GlobalCache.getRecordTypeId('Task','GCC Interaction');
        objTask.PRI__c = 'PRI003000710422';
        objTask.Reason_for_Call__c = 'Pair Swap';
        objTask.Additional_Comments__c = 'Some Description';
        objTask.Additional_Info__c = 'Some Description';
        objTask.Any_follow_up_action_advised__c = 'Some Description';
        objTask.Bypassed_DPU_port_number__c = '22';
        objTask.Cable_Record_Enquiry_Details__c = 'Some Description';
        objTask.CIU_DPU_ID__c = 'Some Description';
        objTask.C_Pair_Destination__c = '22';
        objTask.C_Pair_Source__c = '22';
        objTask.CPI_ID__c = '';
        objTask.Details_of_temp_fix_provided__c = 'Yes';
        objTask.Has_the_hazard_been_made_safe__c = 'Yes';
        objTask.Hazard_Details__c = 'Some Description';
        objTask.Inventory_Update__c ='Some Description';
        objTask.Lead_in_Pair__c = 'Some Description';
        objTask.M_Pair_Destination__c = '22';
        objTask.M_Pair_Source__c = '22';
        objTask.NAR_SR_Created__c = 'Yes';
        objTask.NAR_SR_number__c = '22';
        objTask.O_Pair_Destination__c = '22';
        objTask.O_Pair_Source__c = '22';
        objTask.Pillar_ID_Destination__c = '22';
        objTask.Pillar_ID_Source__c = '22';
        objTask.Reason_for_CIU_By_pass__c = 'Some Description';
        objTask.Severity_of_the_Hazard__c = 'Some Description';
        objTask.Site_Contact__c = 'Some Description';
        objTask.Source_of_Hazard__c = 'Some Description';
        objTask.Telstra_Reference_number__c = 'Some Description'; 
        objTask.Telstra_Tech_Contact_number__c = '0400000000';
        objTask.Telstra_Tech_ID__c = 'Some Description';
        objTask.Telstra_Tech_name__c = 'Some Description'; 
        objTask.X_Pair_Destination__c = '22';
        objTask.X_Pair_Source__c = '22';
        objTask.NBNCorrelationId__c= '0e8cedd0-ad98-11e6-bf2e-47644ada7c0f'; 
        objTask.Network_Inventory_Swap_Status__c=Label.Success; 
        insert objTask;
        
        Task_Custom__c  objCustom = new Task_Custom__c();
        objCustom.NetworkTrailCoRelationID__c = objTask.NBNCorrelationId__c;
        insert objCustom;
        
    }
    
    @isTest static void testMethod_withoutException() {
        
        Test.startTest();
        
        List<NetworkTrail__e> lst = new List<NetworkTrail__e>();
        
        
        String strJSON_1 = '{"transactionId":"aeb8a1007f60271d","metadata":{"nbnCorrelationId":"0e8cedd0-ad98-11e6-bf2e-47644ada7c0f","tokenId":"3c08d723a26f1234","copperPathId":"CPI300000403245","changeReason":"DATA-CORRECTION","requestedPair":"P:12"},"copperPathId":"CPI300000403245","status":"Completed","pairConnected":"abcde:123"}';
        lst.add(new NetworkTrail__e(InboundJSONMessage__c = strJSON_1));
        
        
        
        
        EventBus.publish(lst);
        
        Test.stopTest();
    }
    
    
    @isTest static void testMethod_FTTB() {
        
        Test.startTest();
        
        List<NetworkTrail__e> lst = new List<NetworkTrail__e>();
        
        
        lst.add(new NetworkTrail__e(InboundJSONMessage__c = jsonFTTB));
        
        
        
        
        EventBus.publish(lst);
        
        Test.stopTest();
    }
    
    
    //success
    @isTest static void testMethod_FTTC() {
        
        Test.startTest();
        
        List<NetworkTrail__e> lst = new List<NetworkTrail__e>();
        
        
        String strJSON_1 = '{"transactionId":"aeb8a1007f60271d","metadata":{"nbnCorrelationId":"0e8cedd0-ad98-11e6-bf2e-47644ada7c0f","tokenId":"3c08d723a26f1234","copperPathId":"CPI300000403245","changeReason":"DATA-CORRECTION","requestedPair":"P:12"},"copperPathId":"CPI300000403245","status":"Success","pairConnected":"abcde:123"}';
        lst.add(new NetworkTrail__e(InboundJSONMessage__c = strJSON_1));
        
        String strJSON_2 = '{ "transactionId":"aeb8a1007f60271d", "status":"Completed", "metadata":{ "nbnCorrelationId":"0e8cedd0-ad98-11e6-bf2e-47644ada7c0f", "tokenId":"3c08d723a26f1234", "copperPathId":"CPI300000403245", "accessServiceTechnologyType":"Fibre To The Node", "incidentId":"INC000000012" }, "tco":{ "id":"148689111451627491", "name":"", "type":"Premise Equipment", "subtype":"TCO", "status":"In Service", "platformReferenceId":"1291492788", "locationId":"", "locationLatitude":"", "locationLongitude":"" }, "mdf":{ "id":"E0001", "name":"P17:194", "type":"Joint", "subtype":"TAP", "status":"In Service", "platformReferenceId":"781274058", "locationId":"LOC000012345679", "locationLatitude":"-37.6854", "locationLongitude":"144.565", "portId":"157", "portName":"P17:194", "portPlatformReferenceId":"893746589", "portStatus":"Designed", "portInstallationStatus":"Not Installed" }, "copperLeadinCable":{ "id":"182219421351754719", "name":"4TBA0111DPU001:4-4|182219421351754719:2-2", "type":"Cable", "subtype":"CLS", "status":"In Service", "platformReferenceId":"1291492811", "length":"35.16", "lengthType":"", "installationType":"Underground", "twistedPairId":"1", "twistedPairName":"4TBA0111DPU001:4", "twistedPairPlatformReferenceId":"1291492813", "twistedPairStatus":"In Service", "twistedPairInstallationStatus":"Installed" }, "copperDistributionCables":[ { "id":"4TBA-01-11-CDS-004", "name":"4TBA0111CCU001:4-4", "type":"Cable", "subtype":"CDS", "status":"In Service", "platformReferenceId":"1291492821", "length":"69", "lengthType":"", "twistedPairCapacity":"", "twistedPairId":"1", "twistedPairName":"4TBA0111CCU001:4", "twistedPairPlatformReferenceId":"1291492823", "twistedPairStatus":"In Service", "twistedPairInstallationStatus":"Installed" } ], "pillars":[ { "id":"101559892156683127", "name":"4TBA-01-11-CCU-001", "type":"Pillar", "subtype":"CCU", "status":"In Service", "platformReferenceId":"1291491672", "distributionPairCapacity":"900", "terminationModules":[ { "id":"101559892156683127_O", "name":"4TBA0111CCU001:1-1200", "type":"Termination Module", "subtype":"O Pair ", "status":"In Service", "platformReferenceId":"1291491676", "twistedPairId":4, "twistedPairName":"4TBA0111CCU001:4", "twistedPairPlatformReferenceId":"713844058", "twistedPairStatus":"In Service", "twistedPairInstallationStatus":"Installed" }, { "id":"101559892156683127_C", "name":"4TBA0111CCU002:1-1200", "type":"Termination Module", "subtype":"C Pair", "status":"In Service", "platformReferenceId":"1291491678", "twistedPairId":4, "twistedPairName":"4TBA0111CCU002:4", "twistedPairPlatformReferenceId":"713844059", "twistedPairStatus":"In Service", "twistedPairInstallationStatus":"Installed" } ] } ], "fttnNode":{ "id":"6CAN-08-04-FNO-001", "name":"", "type":"FTTN Node", "status":"In Service", "platformReferenceId":"727426051" }, "copperMainCable":{ "id":"M0002", "name":"EXCH01 M2:1201-1350", "type":"Cable", "subtype":"CMS", "status":"In Service", "platformReferenceId":"713844079", "length":"", "lengthType":"", "twistedPairId":"56", "twistedPairName":"EXCH01 M2:1256", "twistedPairPlatformReferenceId":"451604097", "twistedPairStatus":"In Service", "twistedPairInstallationStatus":"Installed" } }';
        lst.add(new NetworkTrail__e(InboundJSONMessage__c = strJSON_2));
        
        String strJSON_3 = '{"transactionId": "df4ce88b37cf0b61","status": "Completed","metadata": {"nbnCorrelationId": "0e8cedd0-ad98-11e6-bf2e-222222222222","tokenId": "3c08d723a26f1234","copperPathId": "CPI000000000008","accessServiceTechnologyType": "Fibre To The Curb","incidentId": "INC000000000012"},"tco": {"id": "2CBT-09-15-CTB-023","type": "Premise Equipment","subtype": "TCO","status": "In Service","platformReferenceId": "964494379","locationLatitude": "-34.0858","locationLongitude": "150.7895"},"copperLeadinCable": {"id": "000000004216337286","name": "2CBT0915CCJ001:13-13|000000004216337286:2-2","type": "Cable","subtype": "CLS","status": "In Service","platformReferenceId": "964598922","length": "10.0","lenghtType": "AsBuilt","twistedPairId": "1","twistedPairName":"2CBT0915CCJ001:13","twistedPairPlatformReferenceId": "964598923","twistedPairStatus": "In Service","twistedPairInstallationStatus": "Installed"},"copperDistributionCables": [{"id": "000000006500662570","name":"2CBT0915CCJ001:13-13|000000006500662570:2-2","type": "Cable","subtype": "CDS","status": "In Service","platformReferenceId": "964619493","length": "40.5","lenghtType": "AsBuilt","twistedPairId":"1","twistedPairName": "2CBT0915CCJ001:13","twistedPairPlatformReferenceId": "964619495","twistedPairStatus": "In Service","twistedPairInstallationStatus": "Installed"},{"id": "000000006500662523","name": "2CBT0915CCJ001:31-31|2CBT0915CCJ001:2-5|2CBT0915CCJ001:36-36|2CBT0915CCJ001:7-20|000000006500662523:21-30","type": "Cable","subtype": "CDS","status": "In Service","platformReferenceId": "964643263","length": "140.5","lenghtType": "AsBuilt","twistedPairId": "13","twistedPairName": "2CBT0915CCJ001:13","twistedPairPlatformReferenceId": "964643265","twistedPairStatus": "In Service","twistedPairInstallationStatus": "Installed"},{"id": "000000006500662503","name":"2CBT0915CCJ001:51-52|000000015004190095:3-3|2CBT0915CCJ001:54-56|4ROM0107CIP678:7-7|2CBT0915CCJ001:8-8|2CBT0915CCJ001:59-72|TH:23-23|2CBT0915CC001:74-76|2CBT0915CCJ001:27-27|2CBT0915CCJ001:78-80|2CBT0915CCJ001:31-31|2CBT0915CCJ001:2-5|2CBT0915CCJ001:36-36|2CBT0915CCJ001:7-40|2CBT0915CCJ001:71-82|TH:83-83|2CBT0915CCJ001:84-100","type": "Cable","subtype": "CDS","status": "In Service","platformReferenceId": "964586063","length": "112.9","lenghtType": "AsBuilt","twistedPairId": "43","twistedPairName": "2CBT0915CCJ001:13","twistedPairPlatformReferenceId": "964586064","twistedPairStatus": "In Service","twistedPairInstallationStatus": "Installed"},{"id": "000000006500661089","name":"2CBT0913CIP034:1-1|CBTNP164:202-202|2CBT0913CIP034:3-3|CBTNP164:204-206|TH:7-7|CBTNP164:208-208|CBTNP164:54-54|TH:10-11|CBTNP164:212-212|TH:13-13|CBTNP164:214-218|TH:19-19|CBTNP164:220-220|TH:21-21|CBTNP164:222-222|2CBT0913CIP034:23-23|2CBT0908CIP122:24-24|TH:25-25|2CBT0908CIP135:26-26|CBTNP164:227-230|2CBT0913CIP022:31-31|CBTNP164:232-232|TH:33-33|2CBT0913CIP034:34-34|CBTNP164:235-236|2CBT0913CIP022:37-37|CBTNP164:238-241|TH:42-42|000000015012581168:43-43|CBTNP164:244-245|2TWH0710CIP027:46-46|TH:47-47|CBTNP164:248-248|3BWK0607CSS002:49-49|TH:50-50|CBTNP164:251-253|TH:54-54|CBTNP164:146-146|CBTNP164:256-256|TH:57-57|CBTNP164:258-262|CBTNP164:247-247|CBTNP164:264-302|CBTNP164:158-158|CBTNP164:304-306|CBTNP164:357-357|CBTNP164:308-326|CBTNP164:100-100|CBTNP164:328-328|TH:129-129|CBTNP164:330-330|CBTNP164:351-355|CBTNP164:336-336|TH:137-137|CBTNP164:358-390|TH:191-191|CBTNP164:392-400|000000006500661089:181-200","type": "Cable","subtype": "CDS","status": "In Service","platformReferenceId":"964645574","length": "256.4","lenghtType": "AsBuilt","twistedPairId": "143","twistedPairName": "CBTNP164:363","twistedPairPlatformReferenceId": "964645575","twistedPairStatus": "In Service","twistedPairInstallationStatus": "Installed"},{"id": "000000006500661081","name":"2CBT0913CIP034:1-1|CBTNP164:202-202|2CBT0913CIP034:3-3|CBTNP164:204-206|TH:7-7|CBTNP164:208-208|CBTNP164:54-54|TH:10-11|CBTNP164:212-212|TH:13-3|CBTNP164:214-218|TH:19-19|CBTNP164:220-220|TH:21-21|CBTNP164:222-222|2CBT0913CIP034:23-23|2CBT0908CIP122:24-24|TH:25-25|2CBT0908CIP135:26-26|CBTNP164:227-230|2CBT0913CIP022:31-31|CBTNP164:232-232|TH:33-33|2CBT0913CIP034:34-34|CBTNP164:235-236|2CBT0913CIP022:37-37|CBTNP164:238-241|TH:42-42|CBTNP164:329-329|CBTNP164:244-246|CBTNP164:132-132|CBTNP164:248-248|CBTNP164:134-134|TH:50-50|CBTNP164:251-253|TH:54-54|CBTNP164:146-146|CBTNP164:256-256|TH:57-57|CBTNP164:258-262|CBTNP164:247-247|CBTNP164:264-302|CBTNP164:158-158|CBTNP164:304-326|CBTNP164:100-100|CBTNP164:328-328|TH:129-129|CBTNP164:330-330|TH:131-131|CBTNP164:391-391|CBTNP164:333-335|2CBT0923CIP092:156-156|CBTNP164:337-355|CBTNP164:336-336|CBTNP164:357-390|TH:191-191|CBTNP164:392-400","type": "Cable","subtype": "CDS","status": "In Service","platformReferenceId": "964611443","length":"166.1","lenghtType": "AsBuilt","twistedPairId": "163","twistedPairName": "CBTNP164:363","twistedPairPlatformReferenceId": "964611444","twistedPairStatus": "In Service","twistedPairInstallationStatus": "Installed"},{"id": "000000006500661067","name":"2CBT0913CIP034:1-1|CBTNP164:202-202|2CBT0913CIP034:3-3|CBTNP164:204-206|TH:7-7|CBTNP164:208-208|CBTNP164:54-54|TH:10-11|CBTNP164:212-212|TH:13-3|CBTNP164:214-218|TH:19-19|CBTNP164:220-220|TH:21-21|CBTNP164:222-222|2CBT0913CIP034:23-23|2CBT0908CIP122:24-24|TH:25-25|2CBT0908CIP135:26-26|CBTNP164:227-230|2CBT0913CIP022:31-31|CBTNP164:232-232|TH:33-33|2CBT0913CIP034:34-34|CBTNP164:235-236|2CBT0913CIP022:37-37|CBTNP164:238-241|TH:42-42|CBTNP164:243-246|CBTNP164:132-132|CBTNP164:248-248|CBTNP164:134-134|TH:50-50|CBTNP164:251-253|TH:54-54|CBTNP164:146-146|CBTNP164:256-256|TH:57-57|CBTNP164:258-262|CBTNP164:247-247|CBTNP164:264-302|CBTNP164:158-158|CBTNP164:304-326|CBTNP164:100-100|CBTNP164:328-330|TH:131-131|CBTNP164:391-391|CBTNP164:333-355|2CBT0923CIP092:156-156|CBTNP164:357-390|TH:191-191|CBTNP164:392-400","type": "Cable","subtype": "CDS","status": "InService","platformReferenceId": "964636236","length": "271.4","lenghtType": "AsBuilt","twistedPairId": "163","twistedPairName": "CBTNP164:363","twistedPairPlatformReferenceId": "964636237","twistedPairStatus": "In Service","twistedPairInstallationStatus": "Installed"},{"id": "000000006500661063","name":"CBTNP164:201-206|CBTNP164:26-26|CBTNP164:208-208|CBTNP164:54-54|CBTNP164:210-246|CBTNP164:132-132|CBTNP164:248-248|CBTNP164:134-134|TH:50-50|CBNP164:251-253|CBTNP164:250-250|CBTNP164:146-146|CBTNP164:256-256|CBTNP164:254-254|CBTNP164:258-262|CBTNP164:247-247|CBTNP164:264-302|CBTNP164:158-158|CBTNP164:304-326|CBTNP164:100-100|CBTNP164:328-330|TH:131-131|CBTNP164:391-391|CBTNP164:333-355|2CBT0923CIP092:156-156|CBTNP164:357-390|TH:191-191|CBTNP164:392-400","type": "Cable","subtype": "CDS","status": "In Service","platformReferenceId": "964639900","length": "161.1","lenghtType": "AsBuilt","twistedPairId": "163","twistedPairName":"CBTNP164:363","twistedPairPlatformReferenceId": "964639901","twistedPairStatus": "In Service","twistedPairInstallationStatus": "Installed"},{"id": "000000015002831817","name":"CBTNP164:201-206|CBTNP164:26-26|CBTNP164:208-208|CBTNP164:54-54|CBTNP164:210-246|CBTNP164:132-132|CBTNP164:248-248|CBTNP164:134-134|CBTNP164:25-254|CBTNP164:146-146|CBTNP164:256-302|CBTNP164:158-158|CBTNP164:304-330|TH:131-131|CBTNP164:332-355|CBTNP164:41-41|CBTNP164:357-400","type": "Cable","subtype": "CDS","status": "In Service","platformReferenceId": "964640400","length":"1.5","lenghtType": "AsBuilt","twistedPairId": "163","twistedPairName": "CBTNP164:363","twistedPairPlatformReferenceId": "964640401","twistedPairStatus": "In Service","twistedPairInstallationStatus": "Installed"}],"pillars": [{"id": "2CBT-09-15-CCJ-001","type": "Pillar","subtype": "CCU","status": "In Service","platformReferenceId": "1496969277","terminationModules": [{"id": "2CBT-09-15-CCJ-001_O","type": "Termination Module","subtype": "O Pair","status": "In Service","platformReferenceId": "1496969327","twistedPairId": "13","twistedPairName": "2CBT0915CCJ001:13","twistedPairPlatformReferenceId": "1496969327_13","twistedPairStatus": "In Service","twistedPairInstallationStatus": "Installed"},{"id": "2CBT-09-15-CCJ-001_M","type": "Termination Module","subtype": "MPair","status": "In Service","platformReferenceId": "1496969279","twistedPairId": "13","twistedPairName": "CBTNP164:363","twistedPairPlatformReferenceId": "1496969279_13","twistedPairStatus": "In Service","twistedPairInstallationStatus": "Installed"}]},{"id": "000000015002828698","type": "Pillar","subtype": "CCU","status": "In Service","platformReferenceId": "964406933","terminationModules": [{"id": "000000015002828698_O","type": "Termination Module","subtype": "O Pair","status": "InService","platformReferenceId": "964406943","twistedPairId": "363","twistedPairName":"CBTNP164:363","twistedPairPlatformReferenceId": "964406943_363","twistedPairStatus": "InService","twistedPairInstallationStatus": "Installed"},{"id":"000000015002828698_M","type": "Termination Module","subtype": "M Pair","status": "InService","platformReferenceId": "964406935","twistedPairId": "237","twistedPairName":"CBTNG3:37","twistedPairPlatformReferenceId": "964406935_237","twistedPairStatus": "InService","twistedPairInstallationStatus": "Installed"}]}],"copperMainCable": {"id": "000000015002829444","name": "CBTNG3:1-190|CBTNG3:391-391|CBTNG3:192-199|TH:200-200","type": "Cable","subtype":"CMS","status": "In Service","platformReferenceId": "964631440","twistedPairId": "37","twistedPairName":"CBTNG3:37","twistedPairPlatformReferenceId": "964631441","twistedPairStatus": "In Service","twistedPairInstallationStatus": "Installed"}} ';      
        lst.add(new NetworkTrail__e(InboundJSONMessage__c = strJSON_3));
        
        String strJSON_4 = '{  "transactionId": "aeb8a1007f60271d",  "status": "Completed",  "metadata": {    "nbnCorrelationId": "0e8cedd0-ad98-11e6-bf2e-47644ada7c0f",   "tokenId": "3c08d723a26f1234",    "copperPathId": "CPI300000403245",    "accessServiceTechnologyType": "Fibre To The Node",    "incidentId":"INC000000000012"  }}';          
        lst.add(new NetworkTrail__e(InboundJSONMessage__c = strJSON_4));
        
        String strJSON_5 = '{"transactionId":"aeb8a1007f60271d","status":"Completed","metadata":{"nbnCorrelationId":"0e8cedd0-ad98-11e6-bf2e-47644ada7c0f","tokenId":"3c08d723a26f1234","copperPathId":"CPI300000403245","accessServiceTechnologyType":"FibreToTheCurb","incidentId":"INC000000012"},"tco":{"id":"148689111451627491","name":"","type":"PremiseEquipment","subtype":"TCO","status":"InService","platformReferenceId":"1291492788","locationId":"","locationLatitude":"","locationLongitude":""},"copperLeadinCable":{"id":"182219421351754719","name":"4TBA0111DPU001:4-4|182219421351754719:2-2","type":"Cable","subtype":"CLS","status":"InService","platformReferenceId":"1291492811","length":"35.16","lengthType":"","installationType":"Underground","twistedPairId":"1","twistedPairName":"4TBA0111DPU001:4","twistedPairPlatformReferenceId":"1291492813","twistedPairStatus":"InService","twistedPairInstallationStatus":"Installed"},"copperDistributionCables":[{"id":"4TBA-01-11-CDS-004","name":"4TBA0111CCU001:4-4","type":"Cable","subtype":"CDS","status":"InService","platformReferenceId":"1291492821","length":"69","lengthType":"","twistedPairCapacity":"","twistedPairId":"1","twistedPairName":"4TBA0111CCU001:4","twistedPairPlatformReferenceId":"1291492823","twistedPairStatus":"InService","twistedPairInstallationStatus":"Installed"}],"pillars":[{"id":"101559892156683127","name":"4TBA-01-11-CCU-001","type":"Pillar","subtype":"CCU","status":"InService","platformReferenceId":"1291491672","distributionPairCapacity":"900","terminationModules":[{"id":"101559892156683127_O","name":"4TBA0111CCU001:1-1200","type":"TerminationModule","subtype":"OPair","status":"InService","platformReferenceId":"1291491676","twistedPairId":4,"twistedPairName":"4TBA0111CCU001:4","twistedPairPlatformReferenceId":"713844058","twistedPairStatus":"InService","twistedPairInstallationStatus":"Installed"},{"id":"101559892156683127_M","name":"4TBA0111CCU002:1-1200","type":"TerminationModule","subtype":"MPair","status":"InService","platformReferenceId":"1291491678","twistedPairId":4,"twistedPairName":"4TBA0111CCU002:4","twistedPairPlatformReferenceId":"713844059","twistedPairStatus":"InService","twistedPairInstallationStatus":"Installed"}]}],"copperMainCable":{"id":"M0002","name":"EXCH01M2:1201-1350","type":"Cable","subtype":"CMS","status":"InService","platformReferenceId":"713844079","length":"","lengthType":"","twistedPairId":"56","twistedPairName":"EXCH01M2:1256","twistedPairPlatformReferenceId":"451604097","twistedPairStatus":"InService","twistedPairInstallationStatus":"Installed"},"ciu":{"id":"4TBA-01-11-CIU-004","name":"4TBA0111DPU001:4-4|182219421351754719:2-2","type":"Joint","subtype":"CIU","status":"InService","platformReferenceId":"1291492783","twistedPairId":4,"twistedPairName":"4TBA0111DPU001:4","twistedPairPlatformReferenceId":"1291492785_1291492811_1","twistedPairStatus":"InService","twistedPairInstallationStatus":"Installed"},"dpu":{"id":"4TBA-01-11-DPU-001","name":"4TBA0111CCU001:4-4|4TBA0111DPU001:4-4","type":"DPU","status":"InService","platformReferenceId":"1291492308","portId":"157","portName":"P17:194","portPlatformReferenceId":"893746589","portStatus":"Designed","portInstallationStatus":"NotInstalled"}}';
        lst.add(new NetworkTrail__e(InboundJSONMessage__c = strJSON_5));
        
        String strJSON_6 = '{  "transactionId": "aeb8a1007f60271d","status": "Failure",  "metadata": {    "nbnCorrelationId": "0e8cedd0-ad98-11e6-bf2e-47644ada7c0f",    "tokenId":"3c08d723a26f1234",    "copperPathId": "CPI300000403245",    "accessServiceTechnologyType": "Fibre To The Node",    "incidentId":"INC000000000012"  },  "exception": {    "code": "000000",    "message": "A technical error has occurred during the processing of therequest.Invalid response returned by the proxy layer."  }}';
        lst.add(new NetworkTrail__e(InboundJSONMessage__c = strJSON_6));
        
        
        
        EventBus.publish(lst);
        
        Test.stopTest();
    }
    
    
}