@isTest(seeAllData=false)
public class NewContactTriggerHandler_Test {
    static Id endUserRecordTypeId = GlobalCache.getRecordTypeId('Contact', GlobalConstants.END_USER_RECTYPE_NAME);
    static Id customerContactRecordTypeId = GlobalCache.getRecordTypeId('Contact', 'Customer Contact');
    static Id partnerContactRecordTypeId = GlobalCache.getRecordTypeId('Contact', 'Partner Contact');
    static Id partnerAccountRecordTypeId = GlobalCache.getRecordTypeId('Account', 'Partner Account');
    static testmethod void test1(){
        Contact ct = new Contact();
        ct.LastName = 'Test';
        ct.Email = 'test@test.com';
        ct.RecordTypeId = endUserRecordTypeId;
        insert ct;
        ct.LastName = 'Update Test';
        ct.AccountId = null;
        NewContactTriggerHandler.isBeforeUpdateFirstRun = true;
        update ct;
        delete ct;
        undelete ct;
    }
    
      // customer contact
    static testmethod void test2(){
        Contact ct = new Contact();
        ct.LastName = 'TestCustomer';
        ct.Email = 'test@test.com';
        ct.RecordTypeId = customerContactRecordTypeId;        
        insert ct;
        ct.Email = 'test1@test.com';
        ct.Is_This_Contact_Matrix_Field_Update__c = true;
        update ct;
        NewContactTriggerHandler.isBeforeUpdateFirstRun = true;
        update ct;
    }
    
    //Partner contact
    static testmethod void test3(){
        account acc = new account(name='testClass account', RecordTypeId=partnerAccountRecordTypeId);
        insert acc;
        Contact ct = new Contact();
        ct.LastName = 'TestClassPartner';
        ct.Email = 'test@test.com';
        ct.accountid=acc.id;
        ct.RecordTypeId = partnerContactRecordTypeId;
        //ct.ICT_Community_User__c = true;
        ct.ICT_Contact_Role__c = 'Training User';
        insert ct;
        Learning_Record__c lrec = new Learning_Record__c(Contact__c=ct.id,Course_Status__c='Certified');
        insert lrec;
        profile commProfile = [select id from profile where name='ICT Customer Community Plus User'];
        user communityUsr = new user();
        communityUsr = new user(contactid=ct.id,LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',LanguageLocaleKey='en_US',TimeZoneSidKey='Australia/Sydney',CommunityNickname='icttclass',Alias='ictclass',Email='test@test.com',firstName = 'test first', lastName = 'test last', username='testclassuser@test.com', profileid=commProfile.id);
        test.startTest();
            insert communityUsr;
        test.stopTest();
    }
    
    //Partner contact
    static testmethod void test4(){
        account acc = new account(name='testClass account', RecordTypeId=partnerAccountRecordTypeId);
        insert acc;
        Contact ct = new Contact();
        ct.LastName = 'TestClassPartner';
        ct.Email = 'test@test.com';
        ct.status__c = 'Active';
        ct.accountid=acc.id;
        ct.RecordTypeId = partnerContactRecordTypeId;
        //ct.ICT_Community_User__c = true;
        ct.ICT_Contact_Role__c = 'Training User';
        insert ct;

        profile commProfile = [select id from profile where name='ICT Customer Community Plus User'];
        user communityUsr = new user();
        communityUsr = new user(contactid=ct.id,LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',LanguageLocaleKey='en_US',TimeZoneSidKey='Australia/Sydney',CommunityNickname='icttclass',Alias='ictclass',Email='test@test.com',firstName = 'test first', lastName = 'test last', username='testclassuser@test.com', profileid=commProfile.id);
        insert communityUsr;
        test.startTest();
        Contact c = [ select status__c from Contact where id =: ct.Id ];
            c.status__c = 'Inactive';
            update c;
        test.stopTest();
    }
    
    static testmethod void test5(){
        List<Account> accList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        List<user> userList = new List<user>();
        List<Learning_Record__c> lrngRcrdList = new List<Learning_Record__c>();
        
        for(integer i = 0; i < 5; i++){
            account acc = new account(name='testClass account'+i, RecordTypeId=partnerAccountRecordTypeId);
            accList.add(acc);
        }
        insert accList;
        
        for(account acc: accList){
            acc.isPartner = true;
        }
        update accList;
        
        for(integer j = 0; j < 5; j++){
            Contact ct = new Contact(LastName = 'TestClassPartner'+j, Email = 'test@test'+j+'.com', status__c = 'Active', accountid = accList[j].id, RecordTypeId = partnerContactRecordTypeId, ICT_Contact_Role__c = 'Training User');
            conList.add(ct);
        }
        insert conList;
        
        for(integer k = 0; k < 5; k++){
            Learning_Record__c lrec = new Learning_Record__c(Contact__c=conList[k].id,Course_Status__c='Certified');
            lrngRcrdList.add(lrec);
        }
        insert lrngRcrdList;
        
        for(Contact con: conList){
            con.ICT_Contact_Role__c = 'Training User;Sales Delegate';
        }
        update conList;
        
        profile commProfile = [select id from profile where name='ICT Partner Community User'];
        for(integer l = 0; l < 5; l++){
            user communityUsr = new user(contactid=conList[l].id,LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',LanguageLocaleKey='en_US',TimeZoneSidKey='Australia/Sydney',CommunityNickname='icttclass'+l,Alias='ictclass',Email=conList[l].Email,firstName = 'test first', lastName = conList[l].lastName, username=conList[l].Email+'.ict', profileid=commProfile.id);
            userList.add(communityUsr);
        }
        test.startTest();
            insert userList;
        test.stopTest();
    }
}