public without sharing class NewDevCustomRegisterController {
    public NewDevCustomRegisterController() {}

    @TestVisible 
    private static boolean isValidPassword(String password, String confirmPassword) {
        return password == confirmPassword;
    }
    
    @TestVisible 
    private static void validatePassword(User u, String password, String confirmPassword) {
        if(!Test.isRunningTest()) {
        Site.validatePassword(u, password, confirmPassword);
        }
        return;
    }
    
    @AuraEnabled
    public static NewDevCustomRegisterController.ICTUserInformation checkCommunityUser(String strContactID) {
        NewDevCustomRegisterController.ICTUserInformation userinfo;    
        try{
            List<Contact> lstCon = [ SELECT Id, FirstName, Email, Portal_Access_Code__c, AccountId FROM Contact WHERE Portal_Unique_Code__c =: strContactID];  
            if(!lstCon.isEmpty()){
                // Search for the existing ICT Community User
                List<User> lstUser = [ SELECT Id FROM User WHERE ContactId =: lstCon[0].Id AND isPortalEnabled = true AND Profile.Name = 'New Dev Portal User' ];
                if(!lstUser.isEmpty()){
                    //Contact is also a portal user so show him Login screen
                    userinfo = new NewDevCustomRegisterController.ICTUserInformation(true, lstCon[0].Email, '');
                }else{
                    //Need to create Portal user
                    //check if the provided Email i.e. username is unique among existing users.
                    List<User> userList = [Select Id, Username from User Where Username = :lstCon[0].Email];
                    if (!userList.isEmpty()){
                        userinfo = new NewDevCustomRegisterController.ICTUserInformation(false, lstCon[0].Email, System.Label.NewDev_Portal_Register_Error);
                    }else{
                        userinfo = new NewDevCustomRegisterController.ICTUserInformation(false, lstCon[0].Email, '');
                    }
                }
            }
            else{
                userinfo = new NewDevCustomRegisterController.ICTUserInformation(false, '', System.Label.NewDev_Contact_not_Found);
            }
        }   
        Catch(Exception ex){
            userinfo = new NewDevCustomRegisterController.ICTUserInformation(false, '', System.Label.ICT_URL_Validation);
            system.debug('--NewDevCustomRegisterController--checkCommunityUser' + ex.getMessage());
        }       
        return userinfo;
    }
    
    @AuraEnabled
    public static String selfRegister(String firstname ,String lastname, String email, String password, String confirmPassword, String startUrl, String strContactID, String accessCode) {
        system.debug('---firstname---' + firstname);
        system.debug('---lastname---' + lastname);
        system.debug('---email---' + email);
        system.debug('---password---' + password);
        system.debug('---confirmPassword---' + confirmPassword);
        system.debug('---startUrl---' + startUrl);
        system.debug('---strContactID---' + strContactID);
        system.debug('---accessCode---' + accessCode);
        Savepoint sp = null;
        try {
            sp = Database.setSavepoint();
            // Validate the user inputs         
            if (email == null || String.isEmpty(email)) {
                return System.Label.NewDevs_Email_Required;
            }  
            // Declare community user vairables
            ID accId, conId, profileId;
            String strNickname, strCommUserId, strContactRecordTypeID; 
            // Search for the existing ICT Contact
            List<Contact> lstCon = [ SELECT Id, FirstName, LastName, Email, Portal_Access_Code__c, AccountId FROM Contact WHERE Portal_Unique_Code__c =: strContactID AND AccountId != NULL];  
            // hold contacts original email
            string originalEmail = null;
            if(!lstCon.isEmpty()){  
                if(accessCode==null || (lstCon[0].Portal_Access_Code__c != accessCode)){
                  return System.Label.NewDev_Portal_Access_Code;                
                } 
                //check if the provided email on registration form is same as contact email.
                if (lstCon[0].Email <> email){
                    //update Contacts Email to the one provided by user on Registration form
                    //needs to be done since SF matches contact for user creation based on Email
                    //Email on Contact must be the same as created User
                    //before update, check if the updated contact will still be unique w.r.t FN, LN and Email
                   	List<Contact> tempContactList = [Select Id from Contact Where FirstName = :lstCon[0].FirstName and LastName = :lstCon[0].LastName and Email = :email];
                    if (!tempContactList.isEmpty()){
                        system.debug('contact already exist in the system');
                        return System.Label.ICT_Error_Message;
                    }
                    originalEmail = lstCon[0].Email;
                    lstCon[0].Email = email;
                    if (!test.isRunningTest()){
                        update lstCon[0];
                    }
                }
                firstname = lstCon[0].FirstName;
                lastname = lstCon[0].LastName;
                conId = lstCon[0].Id;
                accId = lstCon[0].AccountId;
                // Get the community user profile
                //List<Profile> lstProfile = [ SELECT Id FROM Profile WHERE Name = 'New Dev Portal User'];
                List<Profile> lstProfile = [ SELECT Id FROM Profile WHERE Name = :Label.New_Dev_Portal_User_Profile_Name];
                if(!lstProfile.isEmpty()){
                    profileId = lstProfile[0].Id;
                }    
                // Assign community user nick name
                strNickname = ((firstname != null && firstname.length() > 0) ? firstname.substring(0,1) : '' ) + lastname.substring(0,1);
                strNickname += String.valueOf(Crypto.getRandomInteger()).substring(1,7);                
                // Create community user
                User commUser = new User();
                commUser.Username = email;
                commUser.Email = email;
                commUser.FirstName = firstname;
                commUser.LastName = lastname;
                commUser.CommunityNickname = strNickname;
                commUser.ContactId = conId; 
                commUser.ProfileId = profileId;
                commUser.Alias = accessCode;
                commUser.TimeZoneSidKey = 'Australia/Sydney'; 
                commUser.LocaleSidKey = 'en_AU'; 
                commUser.EmailEncodingKey = 'ISO-8859-1'; 
                commUser.LanguageLocaleKey = 'en_US';
                system.debug('---community user---' + commUser);  
                /**
                 * Custom Password Check Logic to force Password Complexity of minimum (custom label) charater Length
                 * One Uppercase letter, one Lower Case Letter and Password Should not contain the 
                 * Username or FirstName/LastName
                ***/
                string s = '^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d!_$%@#£€*?&]{' + Label.New_Dev_Community_Password_Length + ',}$';
                //Pattern MyPattern = Pattern.compile('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d!_$%@#£€*?&]{8,}$');        
                Pattern MyPattern = Pattern.compile(s);
                // Then instantiate a new Matcher object "MyMatcher"
                Matcher passwordMyMatcher = MyPattern.matcher(password);
                Matcher confirmpasswordMyMatcher = MyPattern.matcher(confirmPassword);      
                
                if(!Test.isRunningTest()){
                    if(!passwordMyMatcher.matches() || !confirmpasswordMyMatcher.matches()) {
                        Database.rollback(sp);
                        return System.Label.NewDevs_Password_Format_Match; 
                    }
                }          
                /*************************End of Custom Passowrd Check Logic*************************/                          
                try{
                    // Create community user
                    strCommUserId = Site.createPortalUser(commUser, accId, password);
                    system.debug('---community user id---' + strCommUserId);                
                    // create a fake userId for test.
                    if (Test.isRunningTest()) {
                        strCommUserId = 'fakeUserId';           
                    }                  
                    // If Community User created successfully 
                    if (strCommUserId != null) {   
                        // Update contact with the acceptance of T&C                                                
                        lstCon[0].Portal_T_C_Acceptance__c = true;
                        //update Contact Email back to the original one
                        //this tweak is required in order to fulfill business need for letting users use shared Email
                        if (originalEmail <> null){
                            lstCon[0].Email = originalEmail;
                        }
                        if (!test.isRunningTest()){
                            update lstCon[0];
                        }
                        // Assign Permission Set - New_Development_Knowledge_Read_Only
                        NewDevCustomRegisterController.assignPS(strCommUserId);                      
                        NewDevCustomRegisterController.reassignTasks(conId, strCommUserId);   
                        // Password is not NULL then do auto community login
                        if (password != null && password.length() > 1) {
                            ApexPages.PageReference prefAutoLogin = Site.login(email, password, startUrl);
                            if(!Test.isRunningTest()) {
                                aura.redirect(prefAutoLogin);
                            }
                        }
                        else{
                            // Password is NULL then redirect user to community login screen
                            ApexPages.PageReference prefLogin = new PageReference(startUrl);
                            if(!Test.isRunningTest()) {
                                aura.redirect(prefLogin);
                            }
                        }     
                    }              
                }Catch(Exception exp){
                    Database.rollback(sp);
                    GlobalUtility.logMessage('Error','New Development Portal','user registration','','registration process','registration method error','',exp,0);
                    system.debug('---NewDevPortalRegisterController--inner exp-' + exp.getMessage());
                    return System.Label.ICT_Error_Message;           
                }
            }else{
                return System.Label.NewDev_Contact_not_Found;
            }
            return null;
        }catch (Exception ex) {
            //moving DML to future method
            Database.rollback(sp);
            createLog('Error','New Development Portal','user registration','auto registration process','registration method error', ex.getMessage(),0);
            system.debug('---NewDevPortalRegisterController--outer exp-' + ex.getMessage());
            return System.Label.ICT_Error_Message;           
        }
    }
    
    @future
    public static void createLog(String logLevel, String sourceClass, String sourceFunction, String referenceId, String referenceInfo, String logMessage, long timeTaken){
        GlobalUtility.logIntMessage(logLevel, sourceClass, sourceFunction, '', referenceId, referenceInfo, logMessage, timeTaken);
    }
    
    //method to reassign tasks from contact to user
    @future
    public static void reassignTasks(Id conId, string strCommUserId){system.debug('inside reassign');
        //Assign the tasks assigned to contact to the newly created user MSEU-5809
        try{
            Customer_Service_Setting__c CS = Customer_Service_Setting__c.getOrgDefaults();
            string unassignedUId = CS.Unassigned_User_Id__c;
            list<Task> taskUpdList = new List<Task>();
            for (task t: [SELECT Id, WhatId, WhoId, OwnerId, Type_Custom__c, Sub_Type__c FROM Task where WhoId = :conId AND OwnerId = :unassignedUId AND Type_Custom__c = 'Deliverables' AND Sub_Type__c in ('Master Plan', 'Pit & Pipe Design', 'Stage Plan', 'Service Plan', 'As-built Design', 'Insurance', 'PCN')]){
                t.OwnerId = strCommUserId;
                taskUpdList.add(t);
            }
            update taskUpdList;system.debug('tasklist===='+taskUpdList);
            Database.DMLOptions dlo = new Database.DMLOptions();
            dlo.EmailHeader.triggerUserEmail = true;
            database.update(taskUpdList , dlo);
        }catch(DMLException ex){
            GlobalUtility.logMessage('Error','New Development Portal','user registration custom','','login process','reassignTasks','',ex,0); 
            system.debug('---NewDevPortalRegisterController--outer exp-' + ex.getMessage()); 
        }
    }
    
 	//This method will be used to assign the permission set MSEU-5809.   
    @future
    public static void assignPS(String strCommUserId){
        try{
            List<PermissionSet> lstPS = [ SELECT id FROM PermissionSet Where Name = 'New_Development_Knowledge_Read_Only' ];
            if(!lstPS.isEmpty()){
                PermissionSetAssignment psa = new PermissionSetAssignment();
                psa.PermissionSetId = lstPS[0].Id;
                psa.AssigneeId = strCommUserId;
                insert psa;
            }
        }
        catch (Exception ex){
            GlobalUtility.logMessage('Error','New Development Portal','user registration','','auto registration process','registration method error','',ex,0); 
            system.debug('---NewDevPortalRegisterController--outer exp-' + ex.getMessage());                
        }     
    }
    
    public class ICTUserInformation{
        @AuraEnabled
        public Boolean isUserExist {get;set;}
        
        @AuraEnabled
        public String strUserEmail {get;set;}
        
        @AuraEnabled
        public String strErrorMessage {get;set;}
        
        public ICTUserInformation(Boolean isUser, String strEmail, String strError){
            this.isUserExist = isUser;
            this.strUserEmail = strEmail;
            this.strErrorMessage = strError;
        }
    }
    
    @AuraEnabled
    public static String login(String username, String password, String startUrl, string did, string strContactID) {
        try{
            ApexPages.PageReference lgn = Site.login(username, password, startUrl);
            //associte the logged in user with the passed development and respective stages, if applicable
			if (did != 'Not found' && did != null && did != 'undefined' && did != ''){
				NewDevCustomRegisterController.associateRecords(username, did);
                //re-assign development deliverable task
                List<Contact> lstCon = [SELECT Id, FirstName, Email, Portal_Access_Code__c, AccountId FROM Contact WHERE Portal_Unique_Code__c =: strContactID];  
                if(!lstCon.isEmpty()){
                	// Search for the existing ICT Community User
                    List<User> lstUser = [SELECT Id, ContactId FROM User WHERE Username =: username AND isPortalEnabled = true AND Profile.Name = 'New Dev Portal User' ];
                    if(!lstUser.isEmpty()){
                        NewDevCustomRegisterController.reassignTasks(lstCon[0].Id, lstUser[0].Id);
                    }
                }
			}
			aura.redirect(lgn);
            return null;
        } 
        catch (Exception ex) {
            //moving DML to future method
            createLog('Error','NewDevCustomRegisterController','login','login Process','login method error', ex.getMessage(),0);
            system.debug(' stage ex message '+ex.getMessage()+' line '+ex.getStacktraceString());
            return ex.getMessage();
        }
    }
    
	public static void associateRecords(string username, string did){
		//find Development and upsert Development Contact for the logged in user
		List<User> userList = [Select ContactId from User Where Username = :username];
        if (!userList.isEmpty()){
            List<Development_Contact__c> devContactList = [Select Id from Development_Contact__c Where Contact__c = :userList[0].ContactId and Development__c = :did and Roles__c = 'Applicant'];
			if (devContactList.isEmpty()){
                Development_Contact__c devContact = new Development_Contact__c();
                devContact.Contact__c = userList[0].ContactId;
                devContact.Development__c = did;
                devContact.Roles__c = 'Applicant';
                insert devContact;
            }
		}
		//fetch related stages for development and upsert stage Contacts
		List<Stage_Application__c> stageList = [Select Id from Stage_Application__c Where Development__c = :did];
		if (!stageList.isEmpty()){
        	Set<Id> ExistingStageContactSet = new Set<Id>();
            List<StageApplication_Contact__c> stageContactList = new List<StageApplication_Contact__c>();
            stageContactList = [Select Id, Stage_Application__c from StageApplication_Contact__c Where Stage_Application__c in :stageList and Contact__c = :userList[0].ContactId and Roles__c = 'Applicant'];
            if (stageContactList.size()>0){
                for (StageApplication_Contact__c stage : stageContactList){
                    ExistingStageContactSet.add(stage.Stage_Application__c);
                }
            }
            stageContactList.clear();
			for (Stage_Application__c stage : stageList){
				if (!ExistingStageContactSet.Contains(stage.Id)){
                    StageApplication_Contact__c stageContact = new StageApplication_Contact__c();
                    stageContact.Contact__c = userList[0].ContactId;
                    stageContact.Stage_Application__c = stage.Id;
                    stageContact.Roles__c = 'Applicant';
                    stageContactList.add(stageContact);
                }
			}
			if (stageContactList.size()>0){
				insert stageContactList;
			}
		}
	}

    @AuraEnabled
    public static Boolean getIsUsernamePasswordEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getUsernamePasswordEnabled();
    }

    @AuraEnabled
    public static Boolean getIsSelfRegistrationEnabled() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getSelfRegistrationEnabled();
    }

    @AuraEnabled
    public static String getSelfRegistrationUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        if (authConfig.getSelfRegistrationEnabled()) {
            return authConfig.getSelfRegistrationUrl();
        }
        return null;
    }

    @AuraEnabled
    public static String getForgotPasswordUrl() {
        Auth.AuthConfiguration authConfig = getAuthConfig();
        return authConfig.getForgotPasswordUrl();
    }
    
    @TestVisible
    private static Auth.AuthConfiguration getAuthConfig(){
        Id networkId = Network.getNetworkId();
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(networkId,'');
        return authConfig;
    }
}