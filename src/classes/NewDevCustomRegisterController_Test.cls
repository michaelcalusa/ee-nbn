/***************************************************************************************************
Class Name:  NewDevCustomRegisterController_Test
Created Date: 26-09-2018
Function    : This is a test class for NewDevCustomRegisterController
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Naseer				   26-09-2018               Created
****************************************************************************************************/

@IsTest(SeeAllData = true)
public class NewDevCustomRegisterController_Test {
    static Contact testContact;
    static list<Contact> testContactList;
    static User testUser;
    static list<User> testUserList;
    static List<Development__c> developmentList;
    static List<Stage_Application__c> stageList;
    
    
    static void setupTestData(){
        testContact = new Contact();
        Id portalUserProfileId = [SELECT Id FROM Profile WHERE name = 'New Dev Portal User'].id;
        Profile SysProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];         
        Id contactRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();       
        Id developmentRTId = Schema.SObjectType.Development__c.getRecordTypeInfosByName().get('Small').getRecordTypeId();       
        String accountName = 'Account Test';
        // initializing User record to create Setup object records (Role and User)
        User UserToCreateSetupObj = TestDataUtility.createTestUser(false,SysProfile.id);
        // Creating Role to assign to a User. Current User should have role associated when creating Community User (including Accound & Contact record)
        UserRole testRole = new UserRole(name = 'TEST ROLE');
        User testRunUser = new User();
        System.runAs(UserToCreateSetupObj){
            Database.insert(testRole);
            testRunUser = TestDataUtility.createTestUser(false,SysProfile.id);
            testRunUser.userroleid = testRole.id;
            testRunUser.UserName = 'Test'+testRunUser.UserName;
        }
        System.runAs(testRunUser){
            testContact = TestDataUtility.createCommunityContactTestRecords(2,true,'John','Smith', contactRTId, accountName)[0];
            testContact.Portal_Unique_Code__c = testContact.Id;
            testContact.Portal_Access_Code__c = 'rgnzh';
            update testContact;
            developmentList = TestDataUtility.createTestDevelopmentRecords(2, testContact.AccountId, developmentRTId);
            stageList = TestDataUtility.createTestStageRecords(1, 'SDU', developmentList[0].Id);
            testUser = TestDataUtility.createTestCommunityUserFromContact(true,portalUserProfileId,testContact);
        }
    }
    
    public static testMethod void testIsValidPassword() {
        System.assert(NewDevCustomRegisterController.isValidPassword('password?@12334', 'password?@12334') == true);
        System.assert(NewDevCustomRegisterController.isValidPassword('password?@12334', 'dummyPassword') == false);
        System.assert(NewDevCustomRegisterController.isValidPassword('password?@12334', null) == false);
        System.assert(NewDevCustomRegisterController.isValidPassword(null, 'fakePwd') == false);
    }
    
    public static testMethod void testIsvalidatePassword() {
        NewDevCustomRegisterController.validatePassword(new User(),'test','test');     
    }
    
    public static testMethod void checkCommunityUserTest() {
        Test.startTest();
        setupTestData();
        NewDevCustomRegisterController.ICTUserInformation userInfo = NewDevCustomRegisterController.checkCommunityUser(testContact.Id);
        Test.stopTest();
    } 
    
    public static testMethod void checkCommunityUserNotPortalUserTest() {
        Test.startTest();
        Id contactRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        String accountName = 'Account Test';
        Contact testContact1 = TestDataUtility.createCommunityContactTestRecords(2,true,'John','Smith', contactRTId, accountName)[0];
        NewDevCustomRegisterController.ICTUserInformation userInfo = NewDevCustomRegisterController.checkCommunityUser(testContact1.Id);
        Test.stopTest();
    }
    
    public static testMethod void checkCommunityUserContactNotExistingTest() {
        setupTestData();
        string puc = 'user doesnt exist';
        Test.startTest();
        	NewDevCustomRegisterController.ICTUserInformation userInfo = NewDevCustomRegisterController.checkCommunityUser(puc);
        Test.stopTest();
    }
    
    public static testMethod void selfRegisterTest() {
        setupTestData();
        Test.startTest();
        	String userInfo = NewDevCustomRegisterController.selfRegister('CommunityUser', 'Test', 'TestCommunityUser@gmail.com', 'Abv!23456', 'Abv!23456', ' ', testContact.Id, 'rgnzh');
        Test.stopTest();
    }
    
    public static testMethod void selfRegisterAccessCodeNullTest() {
        Test.startTest();
        setupTestData();
        String userInfo = NewDevCustomRegisterController.selfRegister('CommunityUser', 'Test', 'TestCommunityUser@gmail.com', 'Abv!23456', 'Abv!23456', ' ', testContact.Id, '');
        Test.stopTest();
    }
    
    public static testMethod void selfRegisterEmailNullTest() {
        Test.startTest();
        setupTestData();
        String userInfo = NewDevCustomRegisterController.selfRegister('CommunityUser', 'Test', '', 'Abv!23456', 'Abv!23456', ' ', testContact.Id, 'rgnzh');
        Test.stopTest();
    }
    
    public static testMethod void selfRegisterPasswordNotMatchingTest() {
        Test.startTest();
        setupTestData();
        String userInfo = NewDevCustomRegisterController.selfRegister('CommunityUser', 'Test', '', '', '', ' ', testContact.Id, 'rgnzh');
        Test.stopTest();
    }
    
    public static testMethod void selfRegisterContactNotFoundTest() {
        Test.startTest();
        Id contactRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        String accountName = 'Account Test';
        Contact testContact1 = TestDataUtility.createCommunityContactTestRecords(2,true,'John','Smith', contactRTId, accountName)[0];
        String userInfo = NewDevCustomRegisterController.selfRegister('CommunityUser', 'Test', 'TestCommunityUser@gmail.com', '', '', ' ', testContact1.Id, 'rgnzh');
        Test.stopTest();
    }
    
    
    public static testMethod void testLoginWithValidCredentials() {
        setupTestData();
        System.assertNotEquals('null', NewDevCustomRegisterController.login('Smith0@nbn.test.user.com', 'Abv!23456', null, developmentList[0].Id, testContact.Id));
    }
    public static testMethod void testLoginWithInvalidCredentials() {
  		System.assertEquals('Argument 1 cannot be null', NewDevCustomRegisterController.login('testUser', 'fakepwd', null, null, null));
 	}
	
 	public static testMethod void NewDevCustomRegisterControllerInstantiation() {
  		NewDevCustomRegisterController controller = new NewDevCustomRegisterController();
  		System.assertNotEquals(controller, null);
 	}	

 	public static testMethod void testIsUsernamePasswordEnabled() {
  		System.assertEquals(true, NewDevCustomRegisterController.getIsUsernamePasswordEnabled());
 	}

 	public static testMethod void testIsSelfRegistrationEnabled() {
  		System.assertEquals(false, NewDevCustomRegisterController.getIsSelfRegistrationEnabled());
 	}

 	public static testMethod void testGetSelfRegistrationURL() {
  		System.assertEquals(null, NewDevCustomRegisterController.getSelfRegistrationUrl());
 	}

 	public static testMethod void testAuthConfig() {
  		Auth.AuthConfiguration authConfig = NewDevCustomRegisterController.getAuthConfig();
  		System.assertNotEquals(null, authConfig);
 	}
}