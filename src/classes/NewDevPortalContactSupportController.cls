/***************************************************************************************************
    Class Name          : NewDevPortalContactSupportController
    Version             : 1.0 
    Created Date        : 17-Feb-2018
    Author              : Sidharth
    Description         : NewDev Portal Knowledge search Controller class. 
    Input Parameters    :  
    Output Parameters   : 
    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Sidharth            17-Feb-2018
****************************************************************************************************/ 

public class NewDevPortalContactSupportController {
  
  @AuraEnabled
    public static list<Object> getKnowledgeArticles(String knowledgeSearchText) {   
         system.debug('inside NewDevPortalContactSupportController ');   
         string serachText = '%'+knowledgeSearchText+'%'; 
         system.debug('serachText like'+serachText);       
         list <Object> articleList = new list<Object>();
         list<FAQ__kav> faqArticle = new list<FAQ__kav>();
         list<Web_Forms__kav> wfArticle = new list<Web_Forms__kav>();
         list<Work_Instruction__kav> wiArticle = new list<Work_Instruction__kav>();
         faqArticle = [Select Id,Title,LastPublishedDate,ArticleNumber,Summary From FAQ__kav Where PublishStatus='Online' AND Language='en_US' AND (Title like :serachText OR Summary like :serachText)];
         wfArticle = [Select Id,Title,LastPublishedDate,ArticleNumber,Summary From Web_Forms__kav Where PublishStatus='Online' AND Language='en_US' AND (Title like :serachText OR Summary like :serachText)];
         wiArticle = [Select Id,Title,LastPublishedDate,ArticleNumber,Summary From Work_Instruction__kav Where PublishStatus='Online' AND Language='en_US' AND (Title like :serachText OR Summary like :serachText)];
         articleList.addAll(faqArticle);
         articleList.addAll(wfArticle);
         articleList.addAll(wiArticle);
         system.debug(articleList);
         return articleList; 
    }   
    
    @AuraEnabled 
    public static list<Object> getKnowledgeDetails (String knowledgeId) { 
    
     list <Object> articleList = new list<Object>();
     list<FAQ__kav> faqArticle = new list<FAQ__kav>();
     list<Web_Forms__kav> wfArticle = new list<Web_Forms__kav>();
     list<Work_Instruction__kav> wiArticle = new list<Work_Instruction__kav>();
     faqArticle = [Select Id,Title,LastPublishedDate,ArticleNumber,Summary,New_Development_Content__c From FAQ__kav Where Id =:knowledgeId];
     wfArticle = [Select Id,Title,LastPublishedDate,ArticleNumber,Summary,New_Development_Content__c From Web_Forms__kav Where Id =:knowledgeId];
     wiArticle = [Select Id,Title,LastPublishedDate,ArticleNumber,Summary,New_Development_Content__c From Work_Instruction__kav Where Id =:knowledgeId];
     articleList.addAll(faqArticle);
     articleList.addAll(wfArticle);
     articleList.addAll(wiArticle);
     system.debug(articleList);
     return articleList; 
    }
}