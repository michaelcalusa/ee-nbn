public class NewDevPortalController {
    
    @AuraEnabled
    public static List<Stage_Application__c> getStageApplications(){                  
        List<StageApplication_Contact__c> listOfStgCont= [SELECT Stage_Application__r.Id,Stage_Application__r.Name,Stage_Application__r.Account__r.Name,Stage_Application__r.CreatedDate,Stage_Application__r.Estimated_First_Service_Connection_Date__c,Stage_Application__r.Application_Status__c from StageApplication_Contact__c where contact__c = :[SELECT ContactId from user where Id = : userInfo.getUserId()].ContactId];              
        List<Id> stgIds = new List<Id>();
        for(StageApplication_Contact__c stgCont : listOfStgCont){
            stgIds.add(stgCont.Stage_Application__r.Id);
        }
        List<Stage_Application__c> listOfStg = [SELECT Id,Name,Stage__c,Account_Name__c,CreatedDate,Estimated_First_Service_Connection_Date__c,Application_Status__c from Stage_Application__c where Id = : stgIds AND Application_Status__c NOT IN ('Application Withdrawn','Application Rejected','In Service') ORDER BY CreatedDate DESC];              
        return listOfStg;
    }
    
    @AuraEnabled
    public static List<Stage_Application__c> getCompletedStageApplications(){                  
        List<StageApplication_Contact__c> listOfStgCont= [SELECT Stage_Application__r.Id,Stage_Application__r.Name,Stage_Application__r.Account__r.Name,Stage_Application__r.CreatedDate,Stage_Application__r.Estimated_First_Service_Connection_Date__c,Stage_Application__r.Application_Status__c from StageApplication_Contact__c where contact__c = :[SELECT ContactId from user where Id = : userInfo.getUserId()].ContactId];              
        List<Id> stgIds = new List<Id>();
        for(StageApplication_Contact__c stgCont : listOfStgCont){
            stgIds.add(stgCont.Stage_Application__r.Id);
        }
        List<Stage_Application__c> listCompOfStg = [SELECT Id,Name,Stage__c,Account_Name__c,CreatedDate,Estimated_First_Service_Connection_Date__c,Application_Status__c from Stage_Application__c where Id = : stgIds AND Application_Status__c IN ('Application Withdrawn','Application Rejected','In Service') ORDER BY CreatedDate DESC];              
        System.debug('Completed Stg : ' + listCompOfStg);
        return listCompOfStg;
    }
    
    @AuraEnabled
    public static List<Stage_Application__c> getStageAppDetails(String stageAppId){ 
        system.debug('stageAppId>>>'+stageAppId);      
        List<Stage_Application__c> stageAppDetList = [SELECT Id,Name,Build_Type__c,Local_Technology__c,No_of_Premises__c,Estimated_First_Service_Connection_Date__c,Estimated_Ready_for_Service_Date__c,stage__c from Stage_Application__c where Id =:stageAppId];                     
        system.debug('stageAppDetList >>>'+stageAppDetList );
        if (stageAppDetList != null && stageAppDetList.size() > 0) {
            for (integer i=0; i<stageAppDetList.size(); i++) {
                 if (stageAppDetList[i].Local_Technology__c != null && stageAppDetList[i].Local_Technology__c != '') {
                     string techtype = stageAppDetList[i].Local_Technology__c;
                     if (techtype.containsIgnoreCase('FTTP')) {
                          stageAppDetList[i].Local_Technology__c = 'nbn™ Fibre to the Premises (FTTP)';
                     }
                     else if (techtype.containsIgnoreCase('FTTB')) {
                               stageAppDetList[i].Local_Technology__c = 'nbn™ Fibre to the Building (FTTB)';
                     }
                     else if (techtype.containsIgnoreCase('FTTC')) {
                               stageAppDetList[i].Local_Technology__c = 'nbn™ Fibre to the Curb (FTTC)';
                     }
                     else if (techtype.containsIgnoreCase('FTTN')) {
                               stageAppDetList[i].Local_Technology__c = 'nbn™ Fibre to the Node (FTTN)';
                     }
                     else if (techtype.containsIgnoreCase('HFC')) {
                               stageAppDetList[i].Local_Technology__c = 'nbn™ Hybrid Fibre Coaxial (HFC)';
                     }
                     else if (techtype.containsIgnoreCase('Fixed Wireless')) {
                               stageAppDetList[i].Local_Technology__c = 'nbn™ Fixed Wireless ';
                     }
                     else if (techtype.containsIgnoreCase('Sky Muster')) {
                               stageAppDetList[i].Local_Technology__c = 'Sky Muster™';
                     }
                 }
            }
        } 
        return stageAppDetList;
    }
    
    @AuraEnabled
    public static List<Task> getStageAppTasks(String stgAppID, String selTaskFilter){
        system.debug('stgAppID>'+stgAppID+'selTaskFilter>'+selTaskFilter);
        String[] taskDeliverableStatus;
        if (selTaskFilter == 'Approved/waiting') {
                 taskDeliverableStatus = new String[]{'Submitted', 'Pending Documents', 'Accepted'};
        }
        else {
                 taskDeliverableStatus = new String[]{'Submitted', 'Pending Documents', 'Accepted', 'Rejected'};
        }         
        List<Task> stgAppTaskList= [SELECT Id,Subject,Due_Date__c,Cycle__c,Description,ActivityDate,Deliverable_Status__c,LastModifiedDate from Task where (whatId =:stgAppID and Type_Custom__c = 'Deliverables' and Status != 'Closed' and Deliverable_Status__c IN :taskDeliverableStatus) ORDER BY Subject ASC, Cycle__c ASC ];
        system.debug('stgAppTaskList>>>'+stgAppTaskList);
        return stgAppTaskList;
    }
    
    @AuraEnabled
    public static List<Task> getTaskDetails(String taskId){ 
        system.debug('getTaskDetails>>>'+taskId);
        List<Task> listOfTask = [SELECT Id,Subject,Due_Date__c,Status,Cycle__c,Description,Deliverable_Status__c,WhatId from Task where Id =: taskId];
        system.debug('getTaskDetails listOfTask>>>'+listOfTask);
        return listOfTask;
    }
    
     @AuraEnabled
    public static Stage_Application__c getStageAppHeaderDetail(Id recId){ 
        system.debug('getRecordId>>>'+recId);
        Stage_Application__c stgApp = new Stage_Application__c();
        if(recId.getSObjectType().getDescribe().getName()=='Task'){
           stgApp = getStageAppDetails(getTaskDetails(recId)[0].WhatId)[0];
        }
        else if(recId.getSObjectType().getDescribe().getName()=='Stage_Application__c'){
          stgApp = getStageAppDetails(recId)[0];
        }
        return stgApp;
    }
   
    @AuraEnabled
    public static String getTaskExplanation(String taskId){                  
        String subject= [SELECT Id,Subject from Task where Id = :taskId].Subject; 
        String explanation = [SELECT Id,Explanation__c from New_Development_Deliverables__mdt WHERE Subject__c = : subject limit 1].Explanation__c;
        System.debug('Task Exp : '+explanation);
        return explanation;
    }
    
    @AuraEnabled
    public static list<Object> getKnowledgeArticle(String taskId){   
        String subject= [SELECT Id,Subject,Sub_Type__c from Task where Id = :taskId].Sub_Type__c;
        subject= 'NDPortal - '+subject; 
        system.debug(subject);
        list <Object> articleList = new list<Object>();
        list<FAQ__kav> faqArticle = new list<FAQ__kav>();
        list<Web_Forms__kav> wfArticle = new list<Web_Forms__kav>();
        list<Work_Instruction__kav> wiArticle = new list<Work_Instruction__kav>();
        faqArticle = [select Id,Title,LastPublishedDate,ArticleNumber,Summary,New_Development_Content__c FROM FAQ__kav WHERE PublishStatus='Online' AND Language='en_US' AND Syndication__c INCLUDES (:subject)];
        wfArticle = [select Id,Title,LastPublishedDate,ArticleNumber,Summary,New_Development_Content__c FROM Web_Forms__kav WHERE PublishStatus='Online' AND Language='en_US' AND Syndication__c INCLUDES (:subject)];
        wiArticle = [select Id,Title,LastPublishedDate,ArticleNumber,Summary,New_Development_Content__c FROM Work_Instruction__kav WHERE PublishStatus='Online' AND Language='en_US' AND Syndication__c INCLUDES (:subject)];
        system.debug(faqArticle);
        articleList.addAll(faqArticle);
        articleList.addAll(wfArticle);
        articleList.addAll(wiArticle);
        system.debug(articleList);
        return articleList;
    }
    
    @AuraEnabled
    public static boolean getAccCustomerClass()
    {
        boolean isClass12 = false;
        list<Contact> conList = new list<Contact>();
        list<Account> accList = new list<Account>();
        list<User> userList = [SELECT ContactId from user where Id = : userInfo.getUserId()];
        if(!userList.isEmpty())
        {
           conList = [select Id, AccountId from contact where Id = :userList[0].ContactId];
            if(!conList.isEmpty())
            {
                accList = [select Id, customer_class__c,account_Types__c from account where Id = :conList[0].AccountId];
                if(!accList.isEmpty())
                {
                    if (string.isBlank(accList[0].customer_class__c) || accList[0].customer_class__c == 'Class1/2')
                    {
                        isClass12 = true;
                    }
                }
            } 
        }
       return isClass12; 
    }
    
    @AuraEnabled
    public static List<SelectOption> getStageApplicationwithStatusList(){
        List<SelectOption> stageAppwithStatusOptions = new List<SelectOption>();
        List<Stage_Application__c> stageAppList = getStageApplications();
        List<Stage_Application__c> stageAppCompletedList = getCompletedStageApplications();
        for(Stage_Application__c stageApp :stageAppList){
            if(stageApp.Application_Status__c == null){
                stageAppwithStatusOptions.add(new SelectOption(stageApp.Name,stageApp.Id));
            }
            else{
                stageAppwithStatusOptions.add(new SelectOption(stageApp.Name+' - '+stageApp.Application_Status__c,stageApp.Id));
            }
            
        }
        for(Stage_Application__c stageApp :stageAppCompletedList){
            if(stageApp.Application_Status__c == null){
                stageAppwithStatusOptions.add(new SelectOption(stageApp.Name,stageApp.Id));
            }
            else{
                stageAppwithStatusOptions.add(new SelectOption(stageApp.Name+' - '+stageApp.Application_Status__c,stageApp.Id));
            }
        }
        return stageAppwithStatusOptions;
    }
    
    @AuraEnabled
    public static case createComplexQuery(case cs){
        Id rtypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND Name = 'Complex Query'].Id;
        cs.recordTypeId = rtypeId;
        cs.Phase__c = 'Planning';
        cs.Category__c = 'New Developments';
        cs.Sub_Category__c = 'Portal';
        cs.ContactId = [SELECT ContactId from user where Id = : userInfo.getUserId()].ContactId;
        cs.Primary_Contact_Role__c = 'Developer/Builder';
        cs.Origin = 'New Developments Portal';
        upsert cs;
        return cs;
    }
    @AuraEnabled
    public static case initializeCase(){
        return new Case();
    }
    public class SelectOption {
        public SelectOption(String label, String value) {
            this.value = value;
            this.label = label;
        }
    
    
        @AuraEnabled
        public String label { get;set; }
        @AuraEnabled
        public String value { get;set; }
    }
}