@isTest
public class NewDevPortalController_Test {
    public testMethod static void test1(){
        //Create portal account owner
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        Integer len = 4;
        Blob blobKey = crypto.generateAesKey(128);
        String key = EncodingUtil.convertToHex(blobKey);
        String usernm = key.substring(0,len);
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = 'newdev'+usernm+'@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        Test.startTest();
        //Create account
        system.runAs(portalAccountOwner1){
            Account portalAccount1 = new Account(
                Name = 'TestAccount',
                OwnerId = portalAccountOwner1.Id
            );
            Database.insert(portalAccount1);
            
            //Create contact
            Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'McTesty',
                AccountId = portalAccount1.Id,
                Email = System.now().millisecond() + 'test@test.com'
            );
            Database.insert(contact1);   
            //Create user
            Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'New Dev Portal User' Limit 1];
            User user1 = new User(
                Username = System.now().millisecond() + 'test12345@test.com',
                ContactId = contact1.Id,
                ProfileId = portalProfile.Id,
                Alias = 't%1@ws$e',
                Email = 'test12345@test.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'McTesty',
                CommunityNickname = 'T3$112E5',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US'
            );
            Database.insert(user1);
            SA_Auto_Reference_Number__c refNumber = new SA_Auto_Reference_Number__c(Name = 'SA001', Last_Sequence_Number__c = '000000000');
            insert refNumber;
            Stage_Application__c stageApp = new Stage_Application__c(Local_Technology__c = 'FTTP');
            insert stageApp;
            StageApplication_Contact__c stageAppCon = new StageApplication_Contact__c(Stage_Application__c = stageApp.Id, Contact__c = contact1.Id);
            insert stageAppCon;
            system.runAs(user1) {
                NewDevPortalController.getStageApplications();
                NewDevPortalController.getCompletedStageApplications();
                Id stageAppId = [SELECT Id FROM Stage_Application__c LIMIT 1].Id;
                NewDevPortalController.getStageAppDetails(stageApp.Id);
                stageApp.Local_Technology__c = 'FTTB';
                update stageApp;
                NewDevPortalController.getStageAppDetails(stageApp.Id);
                stageApp.Local_Technology__c = 'FTTC';
                update stageApp;
                NewDevPortalController.getStageAppDetails(stageApp.Id);
                stageApp.Local_Technology__c = 'FTTN';
                update stageApp;
                NewDevPortalController.getStageAppDetails(stageApp.Id);
                stageApp.Local_Technology__c = 'HFC';
                update stageApp;
                NewDevPortalController.getStageAppDetails(stageApp.Id);
                stageApp.Local_Technology__c = 'Fixed Wireless';
                update stageApp;
                NewDevPortalController.getStageAppDetails(stageApp.Id);
                stageApp.Local_Technology__c = 'Sky Muster';
                update stageApp;
                NewDevPortalController.getStageAppDetails(stageApp.Id);
                Id devRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId();
                Task ts = new Task(whatId = stageApp.Id, 
                                   Subject = '1. Service Plan',
                                   RecordTypeId =  devRecordTypeId                                  
                                  );
                insert ts;
                NewDevPortalController.getStageAppTasks(stageApp.Id,'Approved/waiting');
                NewDevPortalController.getStageAppTasks(stageApp.Id,'');
                NewDevPortalController.getTaskDetails(ts.Id);
                NewDevPortalController.getTaskExplanation(ts.Id);
                NewDevPortalController.getKnowledgeArticle(ts.Id);
                NewDevPortalController.getAccCustomerClass();
                NewDevPortalController.getStageApplicationwithStatusList();
                NewDevPortalController.initializeCase();
            }
            
        }
        Test.stopTest();
    }
}