/***************************************************************************************************
    Class Name          : NewDevPortalGenerateAccessCode
    Version             : 1.0 
    Created Date        : 01-Mar-2018
    Author              : Sidharth
    Description         : This class generates NewDev Portal Unique/Access code and updates on Contact(MSEU-5530)
    Input Parameters    : 
    Output Parameters   : 
    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Sidharth Manure       01-Mar-2018
****************************************************************************************************/ 

global class NewDevPortalGenerateAccessCode {

    Webservice static void getPortalAccessCode(String contId) {
        system.debug('contId>>>'+contId); 
        string newPortalUniqueCode;
        string newAccessCode;
        List<Contact> contactAccessCodeUpdateList = new List<Contact>();
        try {
             if (contId != null) {
                 contactAccessCodeUpdateList = [Select Portal_Unique_Code__c, Portal_Access_Code__c From Contact Where Id =: contId];
                 newAccessCode = HelperNewDevUtility.generatePortalAccessCode(6);
                 system.debug('newAccessCode >>>'+newAccessCode); 
                 if (contactAccessCodeUpdateList != null && contactAccessCodeUpdateList.size() >0) {
                          for (integer i=0; i<contactAccessCodeUpdateList.size(); i++) {
                              if (contactAccessCodeUpdateList[i].Portal_Unique_Code__c == null || contactAccessCodeUpdateList[i].Portal_Unique_Code__c == '') {
                                  newPortalUniqueCode = NewDevelopmentsController.generateUniqueApplicationId(20);
                                  contactAccessCodeUpdateList[i].Portal_Unique_Code__c = newPortalUniqueCode; // assign new Portal Unique code
                              }
                              contactAccessCodeUpdateList[i].Portal_Access_Code__c = newAccessCode; // assign new Portal access code
                          }
                          System.debug('Contact List to update Unque/Access code>>>'+contactAccessCodeUpdateList);
                          update contactAccessCodeUpdateList; // Update new Portal access/unique code on Contact
                 }        
             }  
        } catch(Exception e) {
               system.debug('Exception Occured>>> '+e);     
        }
    }
}