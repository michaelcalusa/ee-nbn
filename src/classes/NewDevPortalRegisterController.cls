/***************************************************************************************************
    Class Name          : NewDevPortalRegisterController
    Created Date        : 24-Jan-2018
    Author              : Anjani Tiwari
    Description         : Logic to Register New Dev Community users   
****************************************************************************************************/ 
public without sharing  class NewDevPortalRegisterController{

    public NewDevPortalRegisterController() {}

    @TestVisible 
    private static boolean isValidPassword(String password, String confirmPassword) {
        return password == confirmPassword;
    }
    
   /* @TestVisible 
    private static boolean siteAsContainerEnabled(String communityUrl) {
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(communityUrl,'');
        return authConfig.isCommunityUsingSiteAsContainer();
    }*/
    
    @TestVisible 
    private static void validatePassword(User u, String password, String confirmPassword) {
        if(!Test.isRunningTest()) {
        Site.validatePassword(u, password, confirmPassword);
        }
        return;
    }
    
    @AuraEnabled
    public static NewDevPortalRegisterController.ICTUserInformation checkCommunityUser(String strContactID) {
        NewDevPortalRegisterController.ICTUserInformation userinfo;
       // String strContactRecordTypeID = getContactRecordTypeID();
        
        try{
        
        
            List<Contact> lstCon = [ SELECT Id, FirstName, Email, Portal_Access_Code__c, AccountId FROM Contact WHERE Portal_Unique_Code__c =: strContactID];
            
            if(!lstCon.isEmpty()){
                // Search for the existing ICT Community User
                List<User> lstUser = [ SELECT Id FROM User WHERE ContactId =: lstCon[0].Id AND isPortalEnabled = true AND Profile.Name = 'New Dev Portal User' ];
                
                if(!lstUser.isEmpty()){
                    userinfo = new NewDevPortalRegisterController.ICTUserInformation(true, lstCon[0].Email, '');
                }
                else{
                    userinfo = new NewDevPortalRegisterController.ICTUserInformation(false, lstCon[0].Email, '');
                }
            }
            else{
                userinfo = new NewDevPortalRegisterController.ICTUserInformation(false, '', System.Label.NewDev_Contact_not_Found);
            }
        }   
        Catch(Exception ex){
          //  userinfo = new NewDevPortalRegisterController.ICTUserInformation(false, '', System.Label.ICT_URL_Validation);
            system.debug('--NewDevPortalRegisterController--checkCommunityUser' + ex.getMessage());
        }       
        return userinfo;
    }
    
    @AuraEnabled
    public static String selfRegister(String firstname ,String lastname, String email, String password, String confirmPassword, String startUrl, String strContactID, String accessCode) {
        system.debug('---firstname---' + firstname);
        system.debug('---lastname---' + lastname);
        system.debug('---email---' + email);
        system.debug('---password---' + password);
        system.debug('---confirmPassword---' + confirmPassword);
        system.debug('---startUrl---' + startUrl);
        system.debug('---strContactID---' + strContactID);
        system.debug('---accessCode---' + accessCode);
        //return firstname + '--' + lastname + '--' + email + '--' + password + '--' + confirmPassword + strContactID;
        Savepoint sp = null;
        try {
            sp = Database.setSavepoint();
            
            // Validate the user inputs
            
            if (email == null || String.isEmpty(email)) {
                return System.Label.NewDevs_Email_Required;
            }
            
           /*else if ((password == null || String.isEmpty(password)) || (confirmPassword == null || String.isEmpty(confirmPassword))) {
                return System.Label.NewDevs_Password_Empty_Check;
            }  
            
           else if (!isValidPassword(password, confirmPassword) || (password == null || String.isEmpty(password)) || (confirmPassword == null || String.isEmpty(confirmPassword))) {
                return System.Label.NewDevs_Password_Empty_Check;
            }  */          

            // Declare community user vairables
            ID accId, conId, profileId;
            String strNickname, strCommUserId, strContactRecordTypeID;
            
           // strContactRecordTypeID = getContactRecordTypeID();
            //return firstname + '--' + lastname + '--' + email + '--' + password + '--' + confirmPassword + '---' + strContactID;
            // Search for the existing ICT Contact
            List<Contact> lstCon = [ SELECT Id, FirstName, LastName, Portal_Access_Code__c, AccountId FROM Contact WHERE Portal_Unique_Code__c =: strContactID AND AccountId != NULL];
            
            if(!lstCon.isEmpty()){
                
                if(accessCode==null || (lstCon[0].Portal_Access_Code__c != accessCode)){
                  return System.Label.NewDev_Portal_Access_Code;                
                }
                
                firstname = lstCon[0].FirstName;
                lastname = lstCon[0].LastName;
                conId = lstCon[0].Id;
                accId = lstCon[0].AccountId;
                
                
                // Get the community user profile
                List<Profile> lstProfile = [ SELECT Id FROM Profile WHERE Name = 'New Dev Portal User'];
                if(!lstProfile.isEmpty()){
                    profileId = lstProfile[0].Id;
                }
                
                // Assign community user nick name
                strNickname = ((firstname != null && firstname.length() > 0) ? firstname.substring(0,1) : '' ) + lastname.substring(0,1);
                strNickname += String.valueOf(Crypto.getRandomInteger()).substring(1,7);
                
                // Create community user
                User commUser = new User();
                commUser.Username = email;
                commUser.Email = email;
                commUser.FirstName = firstname;
                commUser.LastName = lastname;
                commUser.CommunityNickname = strNickname;
                commUser.ContactId = conId; 
                commUser.ProfileId = profileId;
                commUser.CommunityNickname = accessCode;
                commUser.Alias = accessCode;
                commUser.TimeZoneSidKey = 'Australia/Sydney'; 
                commUser.LocaleSidKey = 'en_AU'; 
                commUser.EmailEncodingKey = 'ISO-8859-1'; 
                commUser.LanguageLocaleKey = 'en_US';
                system.debug('---community user---' + commUser);
                
                //validatePassword(commUser, password, confirmPassword);
                /**
                 * Custom Password Check Logic to force Password Complexity of minimum 10 charater Length
                 * One Uppercase letter, one Lower Case Letter and Password Should not contain the 
                 * Username or FirstName/LastName
                ***/
                Pattern MyPattern = Pattern.compile('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d!_$%@#£€*?&]{10,}$');        
                // Then instantiate a new Matcher object "MyMatcher"
                Matcher passwordMyMatcher = MyPattern.matcher(password);
                Matcher confirmpasswordMyMatcher = MyPattern.matcher(confirmPassword);      
                
                if(!Test.isRunningTest()){
                    if(!passwordMyMatcher.matches() || !confirmpasswordMyMatcher.matches()) {
                        return System.Label.NewDevs_Password_Format_Match; 
                    }
        
                   // if(password.containsIgnoreCase(commUser.Username.substringBefore('@')) || confirmPassword.containsIgnoreCase(commUser.Username.substringBefore('@')) || password.containsIgnoreCase(commUser.FirstName) || password.containsIgnoreCase(commUser.LastName) || confirmPassword.containsIgnoreCase(commUser.FirstName) || confirmPassword.containsIgnoreCase(commUser.LastName)) {
                    //    return System.Label.NewDevs_Password_Username_Check; 
                   // }
                }
                
                /*************************End of Custom Passowrd Check Logic*************************/                
               
                try{
                    // Create community user
                    strCommUserId = Site.createPortalUser(commUser, accId, password);
                    system.debug('---community user id---' + strCommUserId); 
                                    
                    // create a fake userId for test.
                    if (Test.isRunningTest()) {
                        strCommUserId = 'fakeUserId';           
                    }
                    
                    // If Community User created successfully 
                    if (strCommUserId != null) { 
                       
                      // Update contact with the acceptance of T&C                                                
                        lstCon[0].Portal_T_C_Acceptance__c = true;
                        update lstCon[0];
                        
                        // Assign Permission Set - New_Development_Knowledge_Read_Only
                        NewDevPortalRegisterController.assignPS(strCommUserId);
                        
                        //Assign the tasks assigned to contact to the newly created user MSEU-5809
                        Customer_Service_Setting__c CS = Customer_Service_Setting__c.getOrgDefaults();
                        string unassignedUId = CS.Unassigned_User_Id__c;
                        list<Task> taskUpdList = new List<Task>();
                        for (task t: [SELECT Id, WhatId, WhoId, OwnerId, Type_Custom__c, Sub_Type__c FROM Task where WhoId = :conId AND OwnerId = :unassignedUId AND Type_Custom__c = 'Deliverables' AND Sub_Type__c in ('Master Plan', 'Pit & Pipe Design', 'Stage Plan', 'Service Plan', 'As-built Design', 'Insurance', 'PCN')])
                        {
                            t.OwnerId = strCommUserId;
                            taskUpdList.add(t);
                        }
                        //update taskUpdList;
						Database.DMLOptions dlo = new Database.DMLOptions();
                        dlo.EmailHeader.triggerUserEmail = true;
                        database.update(taskUpdList , dlo);
                        
                        // Password is not NULL then do auto community login
                        if (password != null && password.length() > 1) {
                            ApexPages.PageReference prefAutoLogin = Site.login(email, password, startUrl);
                            if(!Test.isRunningTest()) {
                                aura.redirect(prefAutoLogin);
                            }
                        }
                        else
                        {
                            // Password is NULL then redirect user to community login screen
                            ApexPages.PageReference prefLogin = new PageReference(startUrl);
                            if(!Test.isRunningTest()) {
                                aura.redirect(prefLogin);
                            }
                        }
                        
                    }              
                }Catch(Exception exp){
                    GlobalUtility.logMessage('Error','New Development Portal','user registration','','registration process','registration method error','',exp,0);
                    system.debug('---NewDevPortalRegisterController--inner exp-' + exp.getMessage());
                    return System.Label.ICT_Error_Message; 
                    
                }
            }
            else{
                return System.Label.NewDev_Contact_not_Found;
            }
            return null;
         }
        catch (Exception ex) {
            GlobalUtility.logMessage('Error','New Development Portal','user registration','','auto registration process','registration method error','',ex,0);
            Database.rollback(sp);
            system.debug('---NewDevPortalRegisterController--outer exp-' + ex.getMessage());
            return System.Label.ICT_Error_Message;           
        }
    }
 //This method will be used to assign the permission set MSEU-5809.   
    @future
    public static void assignPS(String strCommUserId)
    {
        try
        {
            List<PermissionSet> lstPS = [ SELECT id FROM PermissionSet Where Name = 'New_Development_Knowledge_Read_Only' ];
            if(!lstPS.isEmpty())
            {
                PermissionSetAssignment psa = new PermissionSetAssignment();
                psa.PermissionSetId = lstPS[0].Id;
                psa.AssigneeId = strCommUserId;
                insert psa;
            }
        }
        catch (Exception ex)
        {
            GlobalUtility.logMessage('Error','New Development Portal','user registration','','auto registration process','registration method error','',ex,0);
            
            system.debug('---NewDevPortalRegisterController--outer exp-' + ex.getMessage());
                     
        }
        
    }
    
   /* private static String getContactRecordTypeID(){
        String strRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strRecordTypeID = rtMapByName.get('Partner Contact').getRecordTypeId();    

        return strRecordTypeID;
    }*/
    
    public class ICTUserInformation{
        @AuraEnabled
        public Boolean isUserExist {get;set;}
        
        @AuraEnabled
        public String strUserEmail {get;set;}
        
        @AuraEnabled
        public String strErrorMessage {get;set;}
        
        public ICTUserInformation(Boolean isUser, String strEmail, String strError){
            this.isUserExist = isUser;
            this.strUserEmail = strEmail;
            this.strErrorMessage = strError;
        }
    }
}