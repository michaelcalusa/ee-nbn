/***************************************************************************************************
Class Name:  RSPReportCSVScheduler_Test
Created Date: 02-03-2018
Function    : This is a test class for NewDevPortalRegisterController
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Rinu Kachirakkal		   02-03-2018               Created
****************************************************************************************************/

@isTest
public class NewDevPortalRegisterController_Test {
    static Contact testContact;
    static list<Contact> testContactList;
    
    static User testUser;
    static list<User> testUserList;
    
    static void setupTestData(){
        testContact = new Contact();
        Id portalUserProfileId = [SELECT Id FROM Profile WHERE name = 'New Dev Portal User'].id;
        
        Profile SysProfile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        Id contactRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        
        String accountName = 'Account Test';
        // initializing User record to create Setup object records (Role and User)
        User UserToCreateSetupObj = TestDataUtility.createTestUser(false,SysProfile.id);
        // Creating Role to assign to a User. Current User should have role associated when creating Community User (including Accound & Contact record)
        UserRole testRole = new UserRole(name = 'TEST ROLE');
        User testRunUser = new User();
        System.runAs(UserToCreateSetupObj){
            Database.insert(testRole);
            testRunUser = TestDataUtility.createTestUser(false,SysProfile.id);
            testRunUser.userroleid = testRole.id;
            testRunUser.UserName = 'Test'+testRunUser.UserName;
        }
        System.runAs(testRunUser){
            testContact = TestDataUtility.createCommunityContactTestRecords(2,true,'John','Smith', contactRTId, accountName)[0];
            testContact.Portal_Unique_Code__c = testContact.Id;
            testContact.Portal_Access_Code__c = 'rgnzh';
            update testContact;
            testUser = TestDataUtility.createTestCommunityUserFromContact(true,portalUserProfileId,testContact);
        }
    }
    
    public static testMethod void testIsValidPassword() {
        System.assert(NewDevPortalRegisterController.isValidPassword('password?@12334', 'password?@12334') == true);
        System.assert(NewDevPortalRegisterController.isValidPassword('password?@12334', 'dummyPassword') == false);
        System.assert(NewDevPortalRegisterController.isValidPassword('password?@12334', null) == false);
        System.assert(NewDevPortalRegisterController.isValidPassword(null, 'fakePwd') == false);
    }
    
    public static testMethod void testIsvalidatePassword() {
        NewDevPortalRegisterController.validatePassword(new User(),'test','test');     
    }
    
    public static testMethod void checkCommunityUserTest() {
        Test.startTest();
        setupTestData();
        NewDevPortalRegisterController.ICTUserInformation userInfo = NewDevPortalRegisterController.checkCommunityUser(testContact.Id);
        Test.stopTest();
    } 
    
    public static testMethod void checkCommunityUserNotPortalUserTest() {
        Test.startTest();
        Id contactRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        String accountName = 'Account Test';
        Contact testContact1 = TestDataUtility.createCommunityContactTestRecords(2,true,'John','Smith', contactRTId, accountName)[0];
        NewDevPortalRegisterController.ICTUserInformation userInfo = NewDevPortalRegisterController.checkCommunityUser(testContact1.Id);
        Test.stopTest();
    }
    
    public static testMethod void checkCommunityUserContactNotExistingTest() {
        Test.startTest();
        setupTestData();
        NewDevPortalRegisterController.ICTUserInformation userInfo = NewDevPortalRegisterController.checkCommunityUser('');
        Test.stopTest();
    }
    
    public static testMethod void selfRegisterTest() {
        Test.startTest();
        setupTestData();
        String userInfo = NewDevPortalRegisterController.selfRegister('CommunityUser', 'Test', 'TestCommunityUser@gmail.com', 'Abv!23456', 'Abv!23456', ' ', testContact.Id, 'rgnzh');
        Test.stopTest();
    }
    
    public static testMethod void selfRegisterAccessCodeNullTest() {
        Test.startTest();
        setupTestData();
        String userInfo = NewDevPortalRegisterController.selfRegister('CommunityUser', 'Test', 'TestCommunityUser@gmail.com', 'Abv!23456', 'Abv!23456', ' ', testContact.Id, '');
        Test.stopTest();
    }
    
    public static testMethod void selfRegisterEmailNullTest() {
        Test.startTest();
        setupTestData();
        String userInfo = NewDevPortalRegisterController.selfRegister('CommunityUser', 'Test', '', 'Abv!23456', 'Abv!23456', ' ', testContact.Id, 'rgnzh');
        Test.stopTest();
    }
    
    public static testMethod void selfRegisterPasswordNotMatchingTest() {
        Test.startTest();
        setupTestData();
        String userInfo = NewDevPortalRegisterController.selfRegister('CommunityUser', 'Test', '', '', '', ' ', testContact.Id, 'rgnzh');
        Test.stopTest();
    }
    
    public static testMethod void selfRegisterContactNotFoundTest() {
        Test.startTest();
        Id contactRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        String accountName = 'Account Test';
        Contact testContact1 = TestDataUtility.createCommunityContactTestRecords(2,true,'John','Smith', contactRTId, accountName)[0];
        String userInfo = NewDevPortalRegisterController.selfRegister('CommunityUser', 'Test', 'TestCommunityUser@gmail.com', '', '', ' ', testContact1.Id, 'rgnzh');
        Test.stopTest();
    }
    
}