/************************************************************************************************************
Version                 : 1.0 
Created Date            : 30-10-2017
Description/Function    :   1. This class executes the process on Submission of New Devs Web form.
                           
                           NOTE-To inactivate the logic method execution in the trigger context create the 
                           custom setting "Trigger Execution" with the Check box checked.
Used in                 : 
Modification Log        :
* Developer                   Date                   Description
* -------------------------------------------------------------------------                
* SUKUMAR SALLA             30-10-2017             Created the Class
* Krishna Sai				06-04-2018             MOdified the class for Opty Creation
**************************************************************************************************************/
public class NewDevWebFormTriggerHandler{
  
    
    /***************************************************************************************************
    Method Name         : processNewDevFlowExecution 
    Method Type         : 
    Version             : 1.0 
    Created Date        : 30-10-2017 
    Function            : 
    Input Parameters    : newdev application record list 
    Output Parameters   : None   
    Description         :   1. Account Depulication Check / Creation / Update
                                a. Applicant Account creation
                                b. Billing Account Creation
                            2. Contact Depulication Check / Creation / Update
                                a. Applicant Contact creation
                                b. Billing Contact creation
                                c. Contract Signatory creation
                                d. Account payable creation
                            3. Account Address creation
                            4. Development and Development Contact creation
                            5. Stage Application and Stage Application Contacts creation
                            6. Opportunity Creation
                            7. Product Basket creation and associating to Opportunity
                            8. Update NewDev Application record with all record Ids. 
    Used in             : 
    Modification Log    :
    * Developer                   Date                   Description
    * --------------------------------------------------------------------------------------------------                 
    * SUKUMAR SALLA              30-10-2017                Created
    ****************************************************************************************************/   
    public static void processNewDevFlowExecution(List<NewDev_Application__c> trgNewList){
        system.debug('**** START: PROCESS_NEWDEV_FLOW_EXECUTION ****');
        HelperNewDevUtility.ResultsWrapper accountResults;
        HelperNewDevUtility.contactResultsWrapper contactResults;
        HelperNewDevUtility.resultsNewAccountAddress accAddressResults;
        HelperNewDevUtility.resultsNewSite siteResults;
        HelperNewDevUtility.developmentResultsWrapper devResults;
        HelperNewDevUtility.stageApplicationResultsWrapper stageAppResults;
        List<NewDev_Application__c> newDevAppObjList = new List<NewDev_Application__c>(); 
        map<Id, NewDev_Application__c>mapNewDevApplication = new map<Id, NewDev_Application__c>();
        Id OpptyId;
        String basketId;
        // Loop through newTriggerList
        for(NewDev_Application__c newDevApp: trgNewList){
            mapNewDevApplication.put(newDevApp.id, newDevApp);
        }
        
        if(mapNewDevApplication!=null){
            for(string id: mapNewDevApplication.keyset()){
                NewDev_Application__c newDevAppObj = mapNewDevApplication.get(id);
                accountResults = HelperNewDevUtility.CreateAccount(newDevAppObj);                                           
                system.debug('**** PROCESS_NEWDEV_FLOW_EXECUTION : accountResults ==>'+ accountResults);
                if(accountResults!=null){
                    contactResults = HelperNewDevUtility.CreateContact(accountResults.billingAccountId, accountResults.recordTypeId, accountResults.designerAccountId, newDevAppObj);
                    newDevAppObj.SF_Account_ID__c = accountResults.billingAccountId;                 
                    system.debug('**** PROCESS_NEWDEV_FLOW_EXECUTION : contactResults ==>' + contactResults);
                }
                if(contactResults != Null){
                    accAddressResults = HelperNewDevUtility.CreateAccountAddress(newDevAppObj, contactResults, accountResults);
                    newDevAppObj.SF_Applicant_Contact_ID__c = contactResults.AuthorisedConId; 
                    newDevAppObj.SF_AP_Contact_ID__c = contactResults.payableConId;
                    newDevAppObj.SF_Billing_Contact_ID__c = contactResults.BillingConId;
                    newDevAppObj.SF_Contract_Signatory_Contact_ID__c = contactResults.ContractSignatoryConId;
                    newDevAppObj.Contact_Portal_Access_Code__c = [select id, Portal_Access_Code__c from contact where id=:contactResults.AuthorisedConId].Portal_Access_Code__c;
                }
                
                if(accountResults != Null && contactResults != Null && accAddressResults != Null){ 
                    siteResults = HelperNewDevUtility.CreateSite(newDevAppObj);
                    system.debug('**** PROCESS_NEWDEV_FLOW_EXECUTION : Site Results ==>' + siteResults);
                }
                
                if(accountResults != Null && contactResults != Null && siteResults != Null && accAddressResults != Null){                   
                    devResults = HelperNewDevUtility.CreateDevelopment(newDevAppObj, contactResults, accountResults, siteResults);
                    system.debug('**** PROCESS_NEWDEV_FLOW_EXECUTION : Development Results ==>' + devResults);
                    newDevAppObj.SF_Account_Address_ID__c = accAddressResults.aId; 
                }
                
                if(accountResults != Null && contactResults != Null && siteResults != Null && accAddressResults != Null && devResults!=Null){                   
                    stageAppResults = HelperNewDevUtility.CreateStageApplication(newDevAppObj, contactResults, accountResults, siteResults, devResults);
                    system.debug('**** PROCESS_NEWDEV_FLOW_EXECUTION : Stage Application Results ==>' + stageAppResults);
                    newDevAppObj.SF_Development_ID__c = devResults.devId; 
                    if(stageAppResults!=null){ newDevAppObj.SF_Stage_ID__c = stageAppResults.stgId;}        
                    newDevAppObj.Status__c = 'Records creation completed';
                }
                
                /* If application is of Tech Assessment don't create Opportunity.
                   If Billing Entity with NO ABN don't create Opportunity.
                   If Application role is All other requestors don't create Opportunity. 
                   If Upgraded Customer Class or Customer Class = Class 1/2          */
                
                if(accountResults != Null && contactResults != Null && siteResults != Null && accAddressResults != Null && newDevAppObj.Service_Delivery_Type__c!= 'SD-1'
                   && stageAppResults!=Null && !newDevAppObj.Technical_Assessment__c && newDevAppObj.Applicant_Role__c!='All Other requestors' && newDevAppObj.Upgraded_Customer_Class__c!='Class1/2' && newDevAppObj.Customer_Class__c!= 'Class1/2'){ 
                       OpptyId = HelperNewDevUtility.CreateOpportunityandInvoice(newDevAppObj, accountResults, contactResults, siteResults, accAddressResults, stageAppResults);
                       System.debug('PROCESS_NEWDEV_FLOW_EXECUTION : Opportunity ID ==>' + OpptyId);                     
                }
                
                if(OpptyId!=Null && stageAppResults!=Null){
                    Stage_Application__c stgApp = stageAppResults.stgApp;                   
                    string mduID = NewDevelopments_Settings__c.getOrgDefaults().PriceItem_MDU_Class3_4_RecordId__c;
                    string sduID = NewDevelopments_Settings__c.getOrgDefaults().PriceItem_SDU_Class3_4_RecordId__c;                                      
                    string offerId = (newDevAppObj.Dwelling_Type__c == 'Multi Dwelling Unit (MDU)'? mduID: sduID);
                    system.debug('offerId '+offerId);
                    if(offerId != null && offerId!=''){
                        basketId = ProductBasketController.setProductQuantity(Integer.valueof(stgApp.No_of_Premises__c) , OpptyId, offerId);
                        system.debug('PROCESS_NEWDEV_FLOW_EXECUTION : Basket ID ==>'+ basketId);
                        newDevAppObj.SF_Opportunity_ID__c = OpptyId;
                        newDevAppObj.Status__c = 'Records creation completed';                        
                    }   
                    //if basket created, update opportunity to closed won
                    if(basketId != null && basketId!=Null){
                        OpptyId = HelperNewDevUtility.UpdateRelatedOpportunity(OpptyId);
                    }  
                }
                newDevAppObjList.add(newDevAppObj);              
            }  
        }
        
        try{
            if(newDevAppObjList.size()>0){                 
                upsert newDevAppObjList;
            }
        }catch(Exception ex){GlobalUtility.logMessage('Error', 'NewDevWebFormTriggerHandler', 'UpdateNewDevApplicationRecordWithIDs', ' ', '', '', '', ex, 0);}
        
    }
    
    }