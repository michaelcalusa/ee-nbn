/************************************************************************************************************
Version                 : 1.0 
Created Date            : 19-03-2018
Description/Function    : Constants for New Dev application.
Used in                 : Multiple classes
=============================================================================================================
Modification Log        :
-------------------------------------------------------------------------------------------------------------
* Developer                   Date                   Description
* ------------------------------------------------------------------------- ---------------------------------               
* Ravindran Shanmnugam        19-03-2018             Created the Class
**************************************************************************************************************/

global class NewDev_Constants {
    
    public static String CASEPHASE = 'Credit & Collections';
    public static string CASECATEGORY = 'New Development';
    public static string CASESUBCATEGORY = 'Promise to Pay/Payment Extension';
    public static String BILLINGCONTACT = 'Billing Contact';
    
    // New Dev Offers 
    public static String BACKHAUL_OFFER = 'Backhaul Charge';
    public static String MDU_OFFER = 'Multi Dwelling Unit - Class 1/2';
    public static String SDU_OFFER = 'Single Dwelling Unit - Class 1/2';

    // Dwelling Type Constants
    public static String SDU_ONLY = 'SDU Only';
    public static String ROAD_INFRA = 'New Road Infrastructure';
    public static String COMBINED = 'Combined SDU/Super Lot';
    public static String SUPERLOTS = 'Super Lots Only';
    public static String SDU = 'Single Dwelling Unit (SDU)';
    public static String SDU1 = 'SDU';
    public static string SDUInvoice = 'Single Dwelling Unit';
    public static string MDUInvoice = 'Multi Dwelling Unit';


    public static String MDU_ONLY = 'MDU Only';
    public static String HYD_MDU = 'Hybrid MDU';
    public static String HOR_MDU = 'Horizontal MDU';
    public static String MDU = 'Multi Dwelling Unit (MDU)';
    public static String MDU1 = 'MDU';
    public static string FIBRETV = 'Fibre TV';
    public static string ESSENTIALSERVICES = 'Essential Services';
    
    public static string RECOVERABLEWORKS = 'Recoverable Works';
    public static string NEWDEVSCLASS3 = 'NewDevs Class 3-4';
    public static string RECOVERABLEDAMAGES = 'Recoverable Damages';
    public static string STANDARDINVOICE = 'Standard Invoice';

    // Service Delivery type 
    public static String SD1 = 'SD-1';
    public static String SD2 = 'SD-2';

    //Customer Class
    public static String CLASS_1_2 = 'Class1/2';
    public static String CLASS_3_4 = 'Class3/4';
    
    //Invoice Status/name/Record Type Name
    public static String ISSUED = 'Issued';
    public static string NEWSTATUS ='New';
    public static string REQUESTED = 'Requested';
    public static string TOBEISSUED = 'To be issued';
    public static string INVOICE = 'Invoice';
    //public static string REASONCODE = 'Incorrect Premise Count';
    public static string REASONCODE = 'INC_PREM_COUNT';
    
    //Task Constants
    public static String STAGEBILLING= 'Stage Billing';
    public static string COMPLETED = 'Completed';
    
    //OLI TYPE OF ORDER LINE ITEM
    public static string QUANTITYCHANGEORDERLINEITEM = 'Qty Change';
}