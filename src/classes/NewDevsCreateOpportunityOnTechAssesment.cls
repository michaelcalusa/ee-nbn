/*
@Auther : Anjani Tiwari
@Date : 7/Feb/2018
@Description : Create opportunity and related records automation after Appian Technical Assesment. 
*/
 public class NewDevsCreateOpportunityOnTechAssesment{
 
     public static void CreateOpportunity(set<Id> stageAppIds,set<Id> stageAppAccIds,set<Id> stageAppConIds,Set<String> newDevRefNo){
          System.debug('@##stageAppIds#$'+stageAppIds+'@##stageAppAccIds#$'+stageAppAccIds+'@#newDevRefNo##$'+ newDevRefNo +'@#stageAppConIds##$'+stageAppConIds);
          Id AccAddressNewDevClass3_4_RT = GlobalCache.getRecordTypeId('Contract', GlobalConstants.ACC_ADDRESS_NEWDEV_CLASS_3_4); 
          Map<String, NewDev_Application__c> NewdevAppMap = New Map<String, NewDev_Application__c>();
                  
        for(NewDev_Application__c newDevAppObj : [Select Dwelling_Type__c, Application_Reference_Number__c, Payment_Method__c, Billing_Country__c ,Billing_Street_1__c,Billing_Suburb__c,Billing_State__c,Billing_Postcode__c   from NewDev_Application__c where Application_Reference_Number__c in : newDevRefNo]){           
           NewdevAppMap.put(newDevAppObj.Application_Reference_Number__c,newDevAppObj);
        }
        
        Map<String,Contract> accAddMap = new Map<String,Contract>();
        for(Contract addrs : [select Id,Billing_Contact__c,AccountId,BillingStreet,BillingCity,BillingState,BillingPostalCode, BillingCountry From Contract where AccountId in : stageAppAccIds AND RecordTypeId = :AccAddressNewDevClass3_4_RT AND Status='Active']){            
            //accAddMap.put(''+addrs.AccountId+addrs.Billing_Contact__c,addrs);
             accAddMap.put(''+addrs.AccountId+addrs.BillingStreet+addrs.BillingCity+addrs.BillingState+addrs.BillingPostalCode+addrs.BillingCountry,addrs);
             System.debug('@@accAddMap'+accAddMap);
        }
           for(Stage_Application__c  stgApp: [Select Id,No_of_Premises__c,Class__c,Applicant_Role__c,Reference_Number__c,Account__r.Id,Primary_Contact__r.Id,Primary_Location__r.Site_Address__c,Primary_Location__r.Id,(SELECT Roles__c, Id, Contact__c FROM Stage_Application_Contacts__r where Roles__c INCLUDES('Applicant')) from Stage_Application__c where Id in: stageAppIds]){
              Try{ 
                 
                NewDev_Application__c newDevAppObj = New NewDev_Application__c (Payment_Method__c = 'Invoice');                
                HelperNewDevUtility.ResultsWrapper accountResults = new HelperNewDevUtility.ResultsWrapper(stgApp.Account__r.Id,null,'','',False);
                HelperNewDevUtility.contactResultsWrapper contactResults = new HelperNewDevUtility.contactResultsWrapper(null,stgApp.Stage_Application_Contacts__r[0].Contact__c,null,null,False,False,False,False);              
                HelperNewDevUtility.resultsNewSite siteResults = new HelperNewDevUtility.resultsNewSite(stgApp.Primary_Location__r.Id,False,stgApp.Primary_Location__r.Site_Address__c);
                HelperNewDevUtility.stageApplicationResultsWrapper stageAppResults = new HelperNewDevUtility.stageApplicationResultsWrapper(stgApp.Id,stgApp,True);
                HelperNewDevUtility.resultsNewAccountAddress accAddressResults;
                if(accAddMap.Containskey(''+stgApp.Account__r.Id+NewdevAppMap.get(stgApp.Reference_Number__c).Billing_Street_1__c+NewdevAppMap.get(stgApp.Reference_Number__c).Billing_Suburb__c+NewdevAppMap.get(stgApp.Reference_Number__c).Billing_State__c +NewdevAppMap.get(stgApp.Reference_Number__c).Billing_Postcode__c+NewdevAppMap.get(stgApp.Reference_Number__c).Billing_Country__c)){
                   accAddressResults = new HelperNewDevUtility.resultsNewAccountAddress(accAddMap.get(''+stgApp.Account__r.Id+NewdevAppMap.get(stgApp.Reference_Number__c).Billing_Street_1__c+NewdevAppMap.get(stgApp.Reference_Number__c).Billing_Suburb__c+NewdevAppMap.get(stgApp.Reference_Number__c).Billing_State__c +NewdevAppMap.get(stgApp.Reference_Number__c).Billing_Postcode__c+NewdevAppMap.get(stgApp.Reference_Number__c).Billing_Country__c).Id,True);
                   system.debug('##accAddressResults$if$'+accAddressResults );
                }
                else{
                  accAddressResults = new HelperNewDevUtility.resultsNewAccountAddress(null,True);                
                }
                system.debug('##accAddressResults$$'+accAddressResults );
                Id OpptyId = HelperNewDevUtility.CreateOpportunityandInvoice(newDevAppObj, accountResults, contactResults, siteResults, accAddressResults, stageAppResults);
                system.debug('@OpptyId@'+OpptyId);
                
                string mduID = NewDevelopments_Settings__c.getOrgDefaults().PriceItem_MDU_Class3_4_RecordId__c;
                string sduID = NewDevelopments_Settings__c.getOrgDefaults().PriceItem_SDU_Class3_4_RecordId__c;                   
                system.debug('@mduID@'+mduID+'$$sduID$'+sduID);
                system.debug('$$Dweling'+NewdevAppMap.get(stgApp.Reference_Number__c).Dwelling_Type__c);
                
                string offerId = (NewdevAppMap.get(stgApp.Reference_Number__c).Dwelling_Type__c == 'Multi Dwelling Unit (MDU)'? mduID:sduID);  
                system.debug('@offerId @'+offerId );
                Id basketId = ProductBasketController.setProductQuantity(Integer.valueof(stgApp.No_of_Premises__c) , OpptyId, offerId);
                system.debug('##basketId#'+basketId);
                If(basketId!=null){
                Id OpUpId =  HelperNewDevUtility.UpdateRelatedOpportunity(OpptyId);
                   system.debug('##OpUpId #'+OpUpId);
                }
            }catch(Exception ex){GlobalUtility.logMessage('Error', 'NewDevsCreateOpportunityOnTechAssesment', 'CreateOpportunity', ' ', '', '', '', ex, 0);}     
        }
        
    }
 }