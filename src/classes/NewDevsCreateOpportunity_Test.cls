/*
@Auther : John Huang
@Date : 8/3/2018
@Description : Test Class for class NewDevsCreateOpportunityOnTechAssesment
*/
@isTest
public class NewDevsCreateOpportunity_Test { 
    
    static testMethod void newDevsCreateOpportunityUnitTest(){
        
        Account a = new Account(Name='TestAccount');
        insert a;
        Contact c = new Contact (AccountId= a.Id, FirstName = 'firstName' , LastName = 'lastName');
        insert c;
        Development__c dev = new Development__c(Name= 'TestDev',Account__c = a.id, Primary_Contact__c = c.id, Development_ID__c = 'dev01', Suburb__c = 'Test');
        insert dev;

        SA_Auto_Reference_Number__c refNumber = new SA_Auto_Reference_Number__c(Name = 'SA001', Last_Sequence_Number__c = '000000000');
        insert refNumber;
        
        Site__c s = new Site__c(Name='123 Pitt Street');
        s.Site_Address__c = '123 Pitt Street Sydney NSW 2000';
        insert s;
        
        Stage_Application__c stgApp = new Stage_Application__c(Reference_Number__c ='12345',Request_ID__c='TestRq1',  Dwelling_Type__c ='SDU Only', Account__c=a.Id, Primary_Contact__c=c.Id,
                                                               Primary_Location__c = s.Id, Development__c=dev.id,Active_Status__c ='Active',  
                                                               Name = 'TestApp', Estimated_Ready_for_Service_Date__c =system.today());
        
        insert stgApp;
        
        NewDev_Application__c newDevApp = new NewDev_Application__c();
        newDevApp.Application_Reference_Number__c = '12345';
        newDevApp.Billing_State__c = 'NSW';
        newDevApp.Billing_Postcode__c = '2000';
        newDevApp.Billing_Suburb__c = 'Sydney';
        newDevApp.Billing_Street_1__c = '123 Pitt Street';
        newDevApp.Billing_Country__c = 'Australia';
        insert newDevApp;
        Id AccAddressNewDevClass3_4_RT = GlobalCache.getRecordTypeId('Contract', GlobalConstants.ACC_ADDRESS_NEWDEV_CLASS_3_4);
        Contract cont = new Contract();
        cont.AccountId = a.Id;
        cont.BillingCity = 'Sydney';
        cont.BillingCountry = 'Australia';
        cont.BillingPostalCode = '2000';
        cont.BillingState = 'NSW';
        cont.BillingStreet = '123 Pitt Street';
        cont.Status = 'Pending Execution';
        cont.Billing_Contact__c = c.Id;
        cont.RecordTypeId = AccAddressNewDevClass3_4_RT;
        insert cont;
		cont.Status = 'Active';     
        update cont;
        
        StageApplication_Contact__c sc = new StageApplication_Contact__c();
        sc.Contact__c = c.Id;
        sc.Stage_Application__c = stgApp.Id;
        sc.Roles__c = 'Applicant';
        insert sc;
        
        Set<Id> stageAppIds = new Set<Id>();
        stageAppIds.add(stgApp.Id);
        
        Set<Id> stageAppAccIds = new Set<Id>();
        stageAppAccIds.add(a.Id);
        
        Set<Id> stageAppConIds = new Set<Id>();
        stageAppConIds.add(c.Id);
        
        Set<String> newDevRefNo = new Set<String>();
        newDevRefNo.add('12345');
        
        NewDevsCreateOpportunityOnTechAssesment.CreateOpportunity(stageAppIds, stageAppAccIds, stageAppConIds, newDevRefNo);
    }

}