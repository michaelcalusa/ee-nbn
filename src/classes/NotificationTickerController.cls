/*
 *@Author:       Allanah Mae San Miguel
 *@Created Date: 2017-04-17
 *@Description:  Controller for rsp community notificationTicker component
 *@Author:       Beau Anderson
 *@Update Date: 2017-07-05
 *@Description:  added method to get current community so that the correct community prefix can be added to lightning output URLs
 */

public class NotificationTickerController {
    
@AuraEnabled
    public static String getNotificationTitles(String topic) {
        Set<Id> articleIds = new Set<Id>();
        
        for ( TopicAssignment ta: [SELECT EntityId,TopicId FROM TopicAssignment WHERE TopicId =:topic LIMIT 1000]) {
           articleIds.add(ta.EntityId);
        }
        
        List<KnowledgeArticleVersion> knowledgeArticles = [SELECT UrlName,Title FROM KnowledgeArticleVersion WHERE PublishStatus = 'Online' AND Language = 'en_US' AND Id IN: articleIds];
        
        String serializedArticles = json.serialize(knowledgeArticles);
        System.debug(serializedArticles);
        return serializedArticles;
    }
    
    @AuraEnabled
    public static String getCommunityNameStr() {
    
        String commName = '';
        String ntwrkId = '';
        Id currentUserId = UserInfo.getUserId();
        if(Test.isRunningTest()){
        List<NetworkMember> lstntwrkMem = [Select id,memberid, networkid from NetworkMember where memberId =: currentUserId];
        for(NetworkMember net:lstntwrkMem){
           ntwrkId = String.valueOf(net.networkid);
          }
        }
            ntwrkId = Network.getNetworkId();
            List<User> lstUser = [Select id,CurrentNetwork__c from User where Id =: currentUserId];
            for(User usr:lstUser){
            usr.CurrentNetwork__c = String.valueOf(ntwrkId);
          }
          update lstUser;
          
        List<Network> lstCommName = [SELECT UrlPathPrefix FROM Network where Id =:ntwrkId]; 
        for(Network com:lstCommName)
        {
         if(com.id == ntwrkId)
         {
           commName = com.UrlPathPrefix;
         }
        }
        return commName;
    }
}