/*
 *@Author:       Allanah Mae San Miguel
 *@Created Date: 2017-05-04
 *@Description:  Test class for notification ticker component
  *@Author:       Beau Anderson
 *@Updated Date: 2017-08-30
 *@Description:  Add test for TopicDynamicCustomPicklist_L class. Updated topic creation to use testdatautility class
 */

@isTest
private class NotificationTickerControllerTest {
    @testSetup static void createTestData() {
        //articles
        List<FAQ__kav> outageArticles = new List<FAQ__kav>();
        outageArticles.add(new FAQ__kav (Title='1 Unit Test', UrlName='12345'));
        outageArticles.add(new FAQ__kav (Title='2 Bluewolf Test', UrlName='123456'));
        outageArticles.add(new FAQ__kav (Title='3 Broadband', UrlName='123457'));

        List<News__kav> newsArticles = new List<News__kav>();
        newsArticles.add(new News__kav (Title='1 Cat', UrlName='123458'));
        newsArticles.add(new News__kav (Title='2 Mouse', UrlName='123459'));
        newsArticles.add(new News__kav (Title='3 Dog Test', UrlName='123450'));

        insert outageArticles;
        insert newsArticles;

        Set<Id> articleIds = new Set<Id>();
        outageArticles = [SELECT Id, KnowledgeArticleId FROM FAQ__kav WHERE PublishStatus = 'Draft' AND Language = 'en_US' ORDER BY Title];
        for (FAQ__kav article : outageArticles) {
            KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
            articleIds.add(article.Id);
        }

        newsArticles = [SELECT Id, KnowledgeArticleId FROM News__kav WHERE PublishStatus = 'Draft' AND Language = 'en_US' ORDER BY Title];
        for (News__kav article : newsArticles) {
            KbManagement.PublishingService.publishArticle(article.KnowledgeArticleId, true);
            articleIds.add(article.Id);
        }

        //topics
        List<Topic> topics = new List<Topic>();
        topics = TestDataUtility.createTestTopic();
        

        //topic assignment
        List<TopicAssignment> topicAssignments = new List<TopicAssignment>();
        TopicAssignment ta;
        for (FAQ__kav article : outageArticles) {
            ta = new TopicAssignment(TopicId = topics[0].Id, EntityId = article.Id);
            topicAssignments.add(ta);
        }
        for (News__kav article : newsArticles) {
            ta = new TopicAssignment(TopicId = topics[1].Id, EntityId = article.Id);
            topicAssignments.add(ta);
        }
        insert topicAssignments;
        
        User adminUser1 = [Select id,name from User where alias = 'ginte'];
                        
        Id p = [select id from profile where name='RSP Customer Community Plus Login User'].id;
        
        system.runAs(adminUser1){
        Account ac = new Account(name ='Community',OwnerId = adminUser1.id);
        insert ac;

        Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
        insert con;

        User commUser = new User(alias = 'test123', email='test123@noemail.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = p, country='Australia',IsActive =true,
        ContactId = con.Id,
        timezonesidkey='Australia/Sydney', username='tester@noemail.com');
        insert commUser;
       }
      }
    
        @isTest static void getNotificationTitlesTest() {
        Id topicId = [SELECT Id FROM Topic WHERE Name = 'OutagesTest' LIMIT 1].Id;
        List<TopicAssignment> outageArticles = [SELECT EntityId
                                                    FROM TopicAssignment
                                                    WHERE TopicId = :topicId];
                                                   
        User commUser = [Select id,name from User where username = 'tester@noemail.com'];
        
        Set<Id> articleIds = new Set<Id>();
        for (TopicAssignment ta : outageArticles) {
            articleIds.add(ta.EntityId);
        }
        Test.startTest();
            String results = NotificationTickerController.getNotificationTitles(topicId);
            List<KnowledgeArticleVersion> articles = (List<KnowledgeArticleVersion>)JSON.deserialize(results,List<KnowledgeArticleVersion>.class);
    
            System.assertEquals(articleIds.size(), articles.size());
        system.runAs(commUser){
            NotificationTickerController.getCommunityNameStr();
        }

            for (KnowledgeArticleVersion article : articles) {
                if (!articleIds.contains(article.Id)) {
                    System.assert(false);
                }
            }
    }

    
    @isTest static void getTopicsTest(){  
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('', '');  
        TopicDynamicCustomPicklist_LC tdcp = new TopicDynamicCustomPicklist_LC();
        
        Test.startTest();
        VisualEditor.DataRow defaultVal = tdcp.getDefaultValue();
        VisualEditor.DynamicPickListRows custPickVals = tdcp.getValues();
        system.assertNotEquals(null, custPickVals); 
        system.assertNotEquals(null, defaultVal);
        Test.stopTest();
      } 
}