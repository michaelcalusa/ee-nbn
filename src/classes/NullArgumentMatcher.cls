/**
 * Created by alan on 2019-02-26.
 */

@isTest
public class NullArgumentMatcher extends ArgumentMatcher{

    public override Boolean isMatch(Object arg){
        return arg == null;
    }
}