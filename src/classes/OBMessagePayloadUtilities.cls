/*------------------------------------------------------------------------
Author:        Asif Khan
Company:       NBN
Description:   Utility class to transform the sObject into Outbound object

Test Class: OBMessagePayloadUtilitiesTest

History
<Date>      <Authors Name>      <Action>      <Brief Description of Change>
25.03.2018   Asif Khan           Created
--------------------------------------------------------------------------*/
public class OBMessagePayloadUtilities
{
    public class  JsonPayloadInfo
    {
        public string jsonPayload {get;set;}        
        public string externalId {get;set;}        
        
        public JsonPayloadInfo()
        {            
        }
    }
    
    /*public class  OBStagingConfiguration
    {
        public Map<string, string> mapObjectFieldsMapping {get;set;}
        public Map<string, string> mapObjectFieldsDeleteMapping {get;set;}
        public List<string> availableRecordTypes {get;set;}
        public List<string> excludeChangeCaptureFields {get;set;}
        public string ConfigFor {get;set;}
        public string sObjectName  {get;set;}  
        public string externalIdField {get;set;}
        
        public OBStagingConfiguration()
        {
            mapObjectFieldsMapping = new Map<string, string>();
            mapObjectFieldsDeleteMapping = new Map<string, string>();
            availableRecordTypes = new List<string>();
            excludeChangeCaptureFields = new List<string>();
        }
    }*/
    
    /*public static Map<String, List<OBStagingConfiguration>> mapOBStagingConfiguration 
    {
        get {
            if (mapOBStagingConfiguration == null) 
            {                   
                mapOBStagingConfiguration = new Map<string,List<OBStagingConfiguration>>();
                List<Staging_Object_Config__mdt> sObjectConfig = [Select Fields_Map__c,Exclude_Sync_Event_Fields__c,External_Id_Field__c, MasterLabel, MappingType__c, Object__c,RecordType_Names__c,Delete_Fields_Map__c, Config_For__c from Staging_Object_Config__mdt where MappingType__c = 'OutBound'];
                for(Staging_Object_Config__mdt sOM : sObjectConfig)
                {                               
                    OBStagingConfiguration oSC = new OBStagingConfiguration();
                    System.debug(sOM.Fields_Map__c); 
                    
                    if(!string.isBlank(sOM.Fields_Map__c))
                    {
                        Map<String,String> mapFieldsMapping = (Map<String,String>) JSON.deserialize(sOM.Fields_Map__c, Map<String,String>.class);
                        system.debug('SIze>>>>>'+mapFieldsMapping.size());
                        system.debug('Meta>>>>>'+mapFieldsMapping); 
                        
                        if(mapFieldsMapping.size() > 0)
                        {
                            //mapObjectFieldsMapping.put(sOM.sOM.Object__c , mapFieldsMapping);
                            oSC.mapObjectFieldsMapping.putAll(mapFieldsMapping);
                        }
                    }
                    
                    if(!string.isBlank(sOM.Delete_Fields_Map__c))
                    {
                        System.debug(sOM.Delete_Fields_Map__c); 
                        Map<String,String> mapDeleteFieldsMapping = (Map<String,String>) JSON.deserialize(sOM.Delete_Fields_Map__c, Map<String,String>.class);
                        system.debug('SIze>>>>>'+mapDeleteFieldsMapping.size());
                        system.debug('Meta>>>>>'+mapDeleteFieldsMapping); 
                        
                        if(mapDeleteFieldsMapping.size() > 0)
                        {
                            //mapObjectFieldsMapping.put(sOM.sOM.Object__c , mapFieldsMapping);
                            oSC.mapObjectFieldsDeleteMapping.putAll(mapDeleteFieldsMapping);
                        }
                    }
                                        
                    System.debug(sOM.RecordType_Names__c); 
                    if(!string.isBlank(sOM.RecordType_Names__c))
                    {
                       List<string> lstRecTypes = sOM.RecordType_Names__c.split(',');
                        system.debug('SIze>>>>>'+lstRecTypes.size());
                        system.debug('Meta>>>>>'+lstRecTypes); 
                        
                        if(lstRecTypes.size() > 0)
                        {
                            oSC.availableRecordTypes.addAll(lstRecTypes);
                            //mapObjectRecordTypeMapping.put(sOM.Object__c , lstRecTypes);
                        } 
                    }
                    
                    System.debug(sOM.Exclude_Sync_Event_Fields__c); 
                    if(!string.isBlank(sOM.Exclude_Sync_Event_Fields__c))
                    {
                       List<string> excludeChangeCapture = sOM.Exclude_Sync_Event_Fields__c.split(',');
                        system.debug('SIze>>>>>'+excludeChangeCapture.size());
                        system.debug('Meta>>>>>'+excludeChangeCapture); 
                        
                        if(excludeChangeCapture.size() > 0)
                        {
                            oSC.excludeChangeCaptureFields.addAll(excludeChangeCapture);
                            //mapObjectRecordTypeMapping.put(sOM.Object__c , excludeChangeCapture);
                        } 
                    }
                    
                    oSC.ConfigFor = sOM.Config_For__c;
                    oSC.sObjectName = sOM.Object__c;
                    oSC.externalIdField = sOM.External_Id_Field__c;
                    if(mapOBStagingConfiguration.containsKey(sOM.Object__c))
                    {
                       mapOBStagingConfiguration.get(sOM.Object__c).add(oSC);
                    }
                    else
                    {
                        mapOBStagingConfiguration.put(sOM.Object__c, new List<OBStagingConfiguration>{oSC});
                    }                    
                    
                }                                        
            }
            
            return mapOBStagingConfiguration;
        }
        set;
    }*/        
    
    public OBMessagePayloadUtilities()
    {        
    }
    
    Public static void createOBMessage(Map<Id,sObject> oldMap, Map<Id, sObject> newMap, string action, string sObjectName)
    {
        List<ApplicationLogWrapper> msgs = new List<ApplicationLogWrapper>();
        Map<Id,RecordType> sObjectRecordTypes = new Map<ID, RecordType>([SELECT Id,Name FROM RecordType WHERE SobjectType=:sObjectName]);
                
        if(StagingObjConfigUtilities.mapOBStagingConfiguration.containsKey(sObjectName))
        {    
            map<id,user> userMaps = getUsersMap(oldMap, newMap);
            
            List<sObject> finalListSObj = new List<sObject>();
            try
            {
                if(newMap!= null && newMap.size() > 0)
                {
                    for(sObject newSObj : newMap.values())
                    {
                        try
                        {
                            sObject oldObj = oldMap !=null && oldMap.size() > 0 ?  oldMap.get(newSObj.Id) : null;
                            //map<string, string> jsonPayloads = getOBMessages(oldObj, newSObj, sObjectRecordTypes, action, sObjectName);
                            map<string, JsonPayloadInfo> mapJsonPayloadInfo = getOBMessages(oldObj, newSObj, sObjectRecordTypes, action, sObjectName, userMaps);
                            if(mapJsonPayloadInfo.size() > 0 )
                            {
                                string recordRestUrl = '/services/data/v33.0/sobjects/'+sObjectName+'/'+newSObj.Id;
                                for(string pLoad : mapJsonPayloadInfo.keySet())
                                {                                       
                                    sObject sObj = OBMessageUtilities.createOutboundMessage(action, recordRestUrl, mapJsonPayloadInfo.get(pLoad).externalId, sObjectName, mapJsonPayloadInfo.get(pLoad).jsonPayload, newSObj.Id, 'salesforce', pLoad);
                                    if(sObj != null)
                                    {
                                        finalListSObj.add(sObj);
                                    }
                                }    
                            }
                        }
                        catch(Exception ex)
                        {
                            ApplicationLogWrapper msg = new ApplicationLogWrapper();
                            msg.source = 'OBMessagePayloadUtilities';
                            msg.logMessage = ex.getMessage();
                            msg.sourceFunction = 'createOBMessage';
                            msg.referenceId = newSObj.Id;
                            msg.referenceInfo = 'Unable to create oubound message from given info';
                            msg.payload = JSON.Serialize(newSObj);
                            msg.debugLevel = 'Error';
                            msg.Timer = 0;                                                                                    
                            msgs.add(msg);
                        }
                        
                        
                    }   
                }
                else if(oldMap!= null && oldMap.size() > 0)
                {
                    for(sObject oldSObj : oldMap.values())
                    {
                        try
                        {
                            sObject newObj = newMap !=null && newMap.size() > 0 ? newMap.get(oldSObj.Id) : null;
                            map<string, JsonPayloadInfo> mapJsonPayloadInfo = getOBMessages(oldMap.get(oldSObj.Id), newObj, sObjectRecordTypes, action, sObjectName, userMaps);
                            if(mapJsonPayloadInfo != null && mapJsonPayloadInfo.size() > 0 )
                            {
                                for(string pLoad : mapJsonPayloadInfo.keySet())
                                {              
                                    string recordRestUrl = '/services/data/v33.0/sobjects/'+sObjectName+'/'+oldSObj.Id;
                                    sObject sObj = OBMessageUtilities.createOutboundMessage(action, null, mapJsonPayloadInfo.get(pLoad).externalId, sObjectName, mapJsonPayloadInfo.get(pLoad).jsonPayload, oldSObj.Id, 'salesforce', pLoad);
                                    if(sObj != null)
                                    {
                                        finalListSObj.add(sObj);
                                    }
                                }    
                            }
                        }
                        catch(Exception ex)
                        {
                            ApplicationLogWrapper msg = new ApplicationLogWrapper();
                            msg.source = 'OBMessagePayloadUtilities';
                            msg.logMessage = ex.getMessage();
                            msg.sourceFunction = 'createOBMessage';
                            msg.referenceId = oldSObj.Id;
                            msg.referenceInfo = 'Unable to create oubound message from given info';
                            msg.payload = JSON.Serialize(oldSObj);
                            msg.debugLevel = 'Error';
                            msg.Timer = 0;                                                                                    
                            msgs.add(msg);
                        }                                                
                    }
                }
                
                
                if(finalListSObj.size() > 0)
                {                 	
                    List<ApplicationLogWrapper> logMsgs = OBMessageUtilities.insertOubtboundRecs(finalListSObj);
                    if(logMsgs.size() > 0)
                        msgs.addAll(logMsgs);
                }                    
                
                if(msgs.size() > 0)
                {
                    GlobalUtility.logMessage(msgs);
                }
            }
            catch(Exception Ex)
            {
                GlobalUtility.logMessage('Error','OBMessagePayloadUtilities', 'createOBMessage','','Error in creation of Outbound messages ','','',ex,0);
            }                        
        }
    }
    
    Public static map<id,user> getUsersMap(Map<Id,sObject> oldMap, Map<Id, sObject> newMap)
	{
        set<string> userIds = new set<string>();
        Map<Id,sObject> objLst = newMap != null && newMap.size() > 0 ? newMap : oldMap;
        for(sObject obj : objLst.values())
        {
            userIds.add(string.valueOf(obj.get('OwnerId')));
            userIds.add(string.valueOf(obj.get('CreatedById')));
            userIds.add(string.valueOf(obj.get('LastModifiedById')));
        }
        map<id,user> mpuser = new map<id,user>([select id,name from user where id in :userIds]);
        
        return mpuser;
    }
    
    public static map<string, JsonPayloadInfo> getOBMessages(sObject oldObj, sObject newObj, Map<Id,RecordType>  sObjectRecordTypes, string action, string sObjectName, map<id,user> userMaps)
    {
        map<string, JsonPayloadInfo> mapFinalPayLoads = new map<string, JsonPayloadInfo>();
        if(StagingObjConfigUtilities.mapOBStagingConfiguration.size() > 0 && StagingObjConfigUtilities.mapOBStagingConfiguration.containsKey(sObjectName))
        {
            List<StagingObjConfigUtilities.StagingObjConfig>  lstOBStagingConfiguration = StagingObjConfigUtilities.mapOBStagingConfiguration.get(sObjectName);
            Map<Id, sObject> sobjNewRecs = new Map<Id, sObject>();
            
            for(StagingObjConfigUtilities.StagingObjConfig oSC: lstOBStagingConfiguration)
            {
                Boolean isNeedSync = false;
                Map<string, string> dynMapSFMDMFields = new Map<string, string>();
                string recordTypeName = newObj != null ? sObjectRecordTypes.get(string.valueOf(newObj.get('RecordTypeId'))).Name : sObjectRecordTypes.get(string.valueOf(oldObj.get('RecordTypeId'))).Name;
                try{
                    if(oSC.availableRecordTypes.contains(recordTypeName))
                    {
                        if(action != 'Delete')
                        {
                            for(string sField: oSC.mapObjectFieldsMapping.keySet())
                            {
                                system.debug('oSC.mapObjectFieldsMapping.get(sField)>>>>>>>>>>'+oSC.mapObjectFieldsMapping.get(sField));
                                system.debug('sField>>>>>>>>>>'+sField);
                                
                                //if(action == 'Update' && (!IBMessagePayloadUtilities.doNotCreateOBMsgFor.containsKey(newObj.Id) || (IBMessagePayloadUtilities.doNotCreateOBMsgFor.containsKey(newObj.Id) && IBMessagePayloadUtilities.doNotCreateOBMsgFor.get(newObj.Id) != oSC.ConfigFor)))
                                if(action == 'Update' && oSC.isModificationAllowed)
                                {
                                    if((!IBMessagePayloadUtilities.doNotCreateOBMsgFor.containsKey(newObj.Id) 
                                        || (IBMessagePayloadUtilities.doNotCreateOBMsgFor.containsKey(newObj.Id) 
                                            && IBMessagePayloadUtilities.doNotCreateOBMsgFor.get(newObj.Id) != oSC.ConfigFor)))
                                    {
                                        if(newObj.get(sField) != oldObj.get(sField))
                                        {
                                            system.debug('newObj.get(sField)>>>>>>>>>>'+newObj.get(sField) + ' >>>>> ' +'oldObj.get(sField)>>>>>>>>>>'+oldObj.get(sField));
                                            dynMapSFMDMFields.put(oSC.mapObjectFieldsMapping.get(sField), string.valueOf(newObj.get(sField)));
                                            if(!oSC.excludeChangeCaptureFields.contains(sField))
                                                isNeedSync =true;
                                            
                                        }   
                                    }                                    
                                } 
                                else if(action == 'Insert' &&  oSC.isCreationAllowed)
                                {
                                    if((!IBMessagePayloadUtilities.doNotCreateOBMsgFor.containsKey(string.valueOf(newObj.get(oSC.externalIdField))) 
                                        || (IBMessagePayloadUtilities.doNotCreateOBMsgFor.containsKey(string.valueOf(newObj.get(oSC.externalIdField))) && 
                                            IBMessagePayloadUtilities.doNotCreateOBMsgFor.get(string.valueOf(newObj.get(oSC.externalIdField))) != oSC.ConfigFor)))
                                    {
                                        if(!string.isBlank(string.valueOf(newObj.get(sField))))
                                        {
                                         	dynMapSFMDMFields.put(oSC.mapObjectFieldsMapping.get(sField), string.valueOf(newObj.get(sField)));
                                        	isNeedSync =true;         
                                        }                                        
                                    }                                
                                }
                            }
                            
                            if(dynMapSFMDMFields.Size()>0 && isNeedSync)
                            {
                                JsonPayloadInfo jPI = new JsonPayloadInfo();                            
                                jPI.externalId = string.valueOf(newObj.get(oSC.externalIdField) != null ? newObj.get(oSC.externalIdField) : '');
                                system.debug('newObj>>>>>'+newObj); 
                                system.debug('action>>>>>'+action); 
                                system.debug('dynMapSFMDMFields>>>>>'+dynMapSFMDMFields); 
                                system.debug('jPI.externalId>>>>>'+jPI.externalId); 
                                string jsonPayLoad = createJson(newObj, action, dynMapSFMDMFields, jPI.externalId, userMaps);
                                if(jsonPayLoad != null)
                                {                                
                                    jPI.jsonPayload = jsonPayLoad;                                
                                }
                                mapFinalPayLoads.put(oSC.ConfigFor, jPI);
                            }
                        }
                        else if(oSC.isDeletionAllowed)
                        {
                            for(string sField: oSC.mapObjectFieldsDeleteMapping.keySet())
                            {
                                dynMapSFMDMFields.put(oSC.mapObjectFieldsDeleteMapping.get(sField), string.valueOf(oldObj.get(sField)));
                            }
                            
                            JsonPayloadInfo jPI = new JsonPayloadInfo();                        
                            jPI.externalId = string.valueOf(oldObj.get(oSC.externalIdField) != null ? oldObj.get(oSC.externalIdField) : '');
                            string jsonPayLoad = createJson(oldObj, action, dynMapSFMDMFields, jPI.externalId, userMaps);
                            jPI.jsonPayload = jsonPayLoad;
                            mapFinalPayLoads.put(oSC.ConfigFor, jPI);                           
                        }
                    }
                }
                catch(Exception Ex){
                    GlobalUtility.logMessage('Error','OBMessagePayloadUtilities', 'getOBMessages','',action + ' of MDM Outbound object records failed ','','',ex,0);
                }
                
            }
        }
        
        
        return mapFinalPayLoads;
    }
    
    public static string createJson(sObject sObjRec, string action, Map<string,string> mapSFMDMFields, string externalId, map<id,user> userMaps)
    {
        JSONGenerator generator = JSON.createGenerator(true); 
        generator.writeStartObject();
        generator.writeFieldName('Payload');
        generator.writeStartObject();
        generator.writeFieldName('ChangeEventHeader');
        generator.writeStartObject(); 
        generator.writeObjectField('entityName', sObjRec.Id.getSObjectType().getDescribe().getName()); 
        generator.writeObjectField('SFDCRecordId', sObjRec.Id);
        generator.writeObjectField('ExternalId', !string.isBlank(externalId) ? externalId : '');
        generator.writeObjectField('Event', action);
        generator.writeEndObject();
        if(mapSFMDMFields.size() > 0){
            for(string m: mapSFMDMFields.keySet())
            {
                string val = mapSFMDMFields.get(m) != null ? mapSFMDMFields.get(m) : '';
                generator.writeStringField(m, val); 
                if(m == 'LastModifiedById' || m == 'CreatedById' || m == 'OwnerId')
                {
					if(userMaps != null & userMaps.size() > 0)
                    {
                        string fieldName = m.replace('Id', 'Name');
                        if(userMaps.containsKey(val))
                        {
                            generator.writeStringField(fieldName, userMaps.get(val).Name);
                        }
                    }
                }
            }
        }
        generator.writeEndObject();
        generator.writeEndObject();

        String jsonString = generator.getAsString();
        return jsonString;
    }                       
}