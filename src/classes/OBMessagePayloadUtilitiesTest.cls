/***************************************************************************************************
    Class Name  :  OBMessagePayloadUtilitiesTest
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  March 26,2019
    Function    :  This class contains unit test scenarios for OBMessagePayloadUtilities
	Used in     :  None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            March 26,2019               Created
****************************************************************************************************/

@isTest(seeAllData = false)
public Class OBMessagePayloadUtilitiesTest{
    static final string[] ABN_Nos = new string[]{'67099422723','74070625850', '52115553341', '38843605157', '69477029568', 	'79051193065' , '91118703665' };
    static final string[] err_ABN_Nos = new string[]{'67099422723','74070625851', null};        
    static final string MDM_Destination =     'MDM';
    static final string MDM_Source =     'salesforce';
    
    //Insert BEC Account Records
    static testmethod void testInsertBECAcc_OBStagingRecs_MDM(){
        Map<Id,Account> accRecs = new Map<Id,Account>();
        Staging_Object_Config__mdt configOutbnd = new Staging_Object_Config__mdt();
        List<Outbound_Staging__c> obRecs;
        Test.startTest();
        accRecs = TestDataFactory_BSM.createAccTestRecords(1, true, 'Business_End_Customer' , ABN_Nos);
        obRecs = [Select Id, Destination__c, Callback_URL__c, Event__c, External_Id__c, Object__c, Payload__c, SFDC_Record_Id__c, 
                  Source__c from Outbound_Staging__c where SFDC_Record_Id__c IN: accRecs.keySet()];
        Test.stopTest();
        system.assertEquals(1, obRecs.Size());
        if(obRecs.Size() > 0){
            for(Outbound_Staging__c o: obRecs){
                system.assertEquals(MDM_Destination, o.Destination__c);
                system.assertEquals('Insert', o.Event__c);
                system.assertEquals('Account', o.Object__c);
                system.assert(accRecs.containsKey(o.SFDC_Record_Id__c));
                system.assertEquals(MDM_Source, o.Source__c);
            }
        }
    }
    //Update BEC Account Records
    static testmethod void testUpdate1BECAcc_OBStagingRecs_MDM(){
        Map<Id,Account> accMap = new Map<Id,Account>();
        List<Account> accList = new List<Account>();
        List<Outbound_Staging__c> obRecs;
        Staging_Object_Config__mdt configOutbnd = new Staging_Object_Config__mdt();
        Test.startTest();
        configOutbnd = TestDataFactory_BSM.getMetadataTestRecords('Outbound', 'Account', 'MDMAccountOB');
        accMap = TestDataFactory_BSM.createAccTestRecords(1, true, 'Business_End_Customer', ABN_Nos);
        for(Account a: accMap.values()){
            a.Comments__c = 'Testing Update with excluded fields for sync for MDM';
            accList.add(a);
        }
        if(accList.Size()>0){
            update accList;
        }
        obRecs = [Select Id, Destination__c, Callback_URL__c, Event__c, External_Id__c, Object__c, Payload__c, SFDC_Record_Id__c, Source__c from Outbound_Staging__c where SFDC_Record_Id__c IN: accMap.keySet() and Event__c = 'Update'];
        Test.stopTest();
        system.assertEquals(0, obRecs.Size());
        if(obRecs.Size() > 0){
            for(Outbound_Staging__c o: obRecs){
                system.assertEquals(MDM_Destination, o.Destination__c);
                system.assertEquals('Update', o.Event__c);
                system.assertEquals('Account', o.Object__c);
                system.assert(accMap.containsKey(o.SFDC_Record_Id__c));
                system.assertEquals(MDM_Source, o.Source__c);
                /*for(string s: payloadInfo.keySet()){
                   o.Payload__c.contains(s);
                }*/
            }
        }
    }   
    //Update BEC Account Records
    static testmethod void testUpdateBECAcc_OBStagingRecs_MDM(){
        Map<Id,Account> accMap = new Map<Id,Account>();
        List<Account> accList = new List<Account>();
        List<Outbound_Staging__c> obRecs;
        Test.startTest();            
        accMap = TestDataFactory_BSM.createAccTestRecords(1, true, 'Business_End_Customer', ABN_Nos);
        for(Account a: accMap.values()){
            a.No_of_Employees__c = '7';
            accList.add(a);
        }
        if(accList.Size()>0){
            update accList;
        }
        obRecs = [Select Id, Destination__c, Callback_URL__c, Event__c, External_Id__c, Object__c, Payload__c, SFDC_Record_Id__c, Source__c from Outbound_Staging__c where SFDC_Record_Id__c IN: accMap.keySet() and Event__c = 'Update'];
        Test.stopTest();
        system.assertEquals(1, obRecs.Size());
        if(obRecs.Size() > 0){
            for(Outbound_Staging__c o: obRecs){
                system.assertEquals(MDM_Destination, o.Destination__c);
                system.assertEquals('Update', o.Event__c);
                system.assertEquals('Account', o.Object__c);
                system.assert(accMap.containsKey(o.SFDC_Record_Id__c));
                system.assertEquals(MDM_Source, o.Source__c);
            }
        }
    } 
    //Delete BEC Account Records
    static testmethod void testDeleteBECAcc_OBStagingRecs_MDM(){
        Map<Id,Account> accMap = new Map<Id,Account>();
        List<Account> accList = new List<Account>();
        List<Outbound_Staging__c> obRecs;
        Test.startTest();
        accMap = TestDataFactory_BSM.createAccTestRecords(1, true, 'Business_End_Customer', ABN_Nos);
        for(Account a: accMap.values()){
            a.No_of_Employees__c = '7';
            accList.add(a);
        }
        if(accList.Size()>0){
            Delete accList;
        }
        obRecs = [Select Id, Destination__c, Callback_URL__c, Event__c, External_Id__c, Object__c, Payload__c, SFDC_Record_Id__c, Source__c from Outbound_Staging__c where SFDC_Record_Id__c IN: accMap.keySet() and Event__c = 'Delete'];
        Test.stopTest();
        system.assertEquals(1, obRecs.Size());
        if(obRecs.Size() > 0){
            for(Outbound_Staging__c o: obRecs){
                system.assertEquals(MDM_Destination, o.Destination__c);
                system.assertEquals('Delete', o.Event__c);
                system.assertEquals('Account', o.Object__c);
                system.assert(accMap.containsKey(o.SFDC_Record_Id__c));
                system.assertEquals(MDM_Source, o.Source__c);
            }
        }
    }      
    //Insert non BEC Account Records
    static testmethod void testnonBECAccOBStagingRecs_MDM(){
        Map<Id,Account> accRecs = new Map<Id,Account>();
        List<Outbound_Staging__c> obRecs;
        Test.startTest();
        accRecs = TestDataFactory_BSM.createAccTestRecords(1, true, 'Customer', ABN_Nos);
        obRecs = [Select Id, Destination__c, Callback_URL__c, Event__c, External_Id__c, Object__c, Payload__c, SFDC_Record_Id__c, Source__c from Outbound_Staging__c where SFDC_Record_Id__c IN: accRecs.keySet()];
        Test.stopTest();
        system.assertEquals(0, obRecs.Size());
    } 
    //Insert Outbound Records for Contact Updates in Salesforce - Contact 
    public static testmethod void testInsertBECCon_OBStagingRecs_MDM(){
        Map<Id,Contact> conMap = new Map<Id,Contact>();
        List<Contact> conLst = new List<Contact>();
         List<Outbound_Staging__c> obRecs;
        Test.startTest();
        conMap = TestDataFactory_BSM.createConTestRecords(1, true, 'Business_End_Customer' , ABN_Nos);
        obRecs = [Select Id, Destination__c, Callback_URL__c, Event__c, External_Id__c, Object__c, Payload__c, SFDC_Record_Id__c, 
                  Source__c from Outbound_Staging__c where SFDC_Record_Id__c IN: conMap.keySet()];
 		system.assertEquals(1, obRecs.Size());
        if(obRecs.Size() > 0){
            for(Outbound_Staging__c o: obRecs){
                system.assertEquals(MDM_Destination, o.Destination__c);
                system.assertEquals('Insert', o.Event__c);
                system.assertEquals('Contact', o.Object__c);
                system.assert(conMap.containsKey(o.SFDC_Record_Id__c));
                system.assertEquals(MDM_Source, o.Source__c);
            }
        }
    }    

}