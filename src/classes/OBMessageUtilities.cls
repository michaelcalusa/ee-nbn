/*------------------------------------------------------------------------
Author:        Swetha K
Company:       NBN
Description:   Utility class to transform the sObject into Outbound object

Test Class: MDMOB_UtilitiesTest

History
<Date>      <Authors Name>      <Action>      <Brief Description of Change>
20.03.2018   Swetha K           Created
--------------------------------------------------------------------------*/
public class OBMessageUtilities {
  
    public static List<ApplicationLogWrapper> insertOubtboundRecs(list<sObject> lstOfsObjects)
    {
        List<ApplicationLogWrapper> msgs = new List<ApplicationLogWrapper>();            
        try
        {            
            Database.SaveResult[] srList = Database.insert(lstOfsObjects, false);                                        
            List<ApplicationLogWrapper> logMsgs = getApplicationLogs(srList, lstOfsObjects);
            if(logMsgs.size() > 0)
                msgs.addAll(logMsgs);            
        }
        catch(Exception Ex)
        {
            GlobalUtility.logMessage('Error','OBMessageUtilities', 'insertOubtboundRecs','','Insert of Outbound records failed'+ 
            lstOfsObjects[0].getSObjectType().getDescribe().getName(),'DML error','',ex,0);
        }
        
        return msgs;
        
    }
    
    public static sObject createOutboundMessage(string action, string callbckURL, string ExternalId, string sourceObject, string payload, string SFRecordId, string source, string destination){
        
        //first method
        sObject sObj = Schema.getGlobalDescribe().get('Outbound_Staging__c').newSObject() ;
        sObj.put('Event__c', action);
        sObj.put('Callback_URL__c', callbckURL);
        sObj.put('External_Id__c', ExternalId);
        sObj.put('Object__c', sourceObject);
        sObj.put('Payload__c', payload);
        sObj.put('SFDC_Record_Id__c', SFRecordId);
        sObj.put('Source__c', source);
        sObj.put('Destination__c', destination);
        
        //second method
		/*Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objName);
        SObject newSobject = targetType.newSObject();
        Map<String, Schema.sObjectField> targetFields = targetType.getDescribe().fields.getMap();
        system.debug(targetFields);

        newSobject.put(targetFields.get(action__c), action__c)*/

        return sObj;
    }
    
    public static List<ApplicationLogWrapper> getApplicationLogs(Database.SaveResult[] srList, List<sObject> sObjectList)
    {
        List<ApplicationLogWrapper> msgs = new List<ApplicationLogWrapper>();
        
        if(srList!=null)
        {
            for(Integer i=0;i<srList.size();i++)
            {
                Database.SaveResult sr = srList.get(i);
                system.debug('Database.SaveResult >>>>>>>> '+JSON.Serialize(sr));
                sObject recordFailed = sObjectList.get(i);
                string recordId = recordFailed.id;//sr.getId();
                
                if(!string.isBlank(string.valueOf(recordFailed.get('SFDC_Record_Id__c')))) 
                    recordId = string.valueOf(recordFailed.get('SFDC_Record_Id__c'));
                   
                
                system.debug('mapwithTargetIds.containsKey >>>>>>>> '+recordId);
                if (!sr.isSuccess())                                 
                {                                    
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) 
                    {                                        
                        system.debug('!sr.isSuccess() >>>>>>>> '+err.getStatusCode() + ': ' + err.getMessage());
                        ApplicationLogWrapper msg = new ApplicationLogWrapper();
                        msg.source = 'OBMessageUtilities';
                        msg.logMessage = err.getStatusCode() + ': ' + err.getMessage();
                        msg.sourceFunction = 'insertOubtboundRecs';
                        msg.referenceId = recordId;
                        msg.referenceInfo = err.getFields() !=null ? JSON.Serialize(err.getFields()) : '';
                        msg.payload = JSON.Serialize(recordFailed);
                        msg.debugLevel = 'Error';
                        msg.Timer = 0;                                                                                    
                        msgs.add(msg);
                        
                        System.debug('The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Faield to synch the record from Inbound message error: ' + err.getFields());
                    }
                }
        	}
        
        }
        return msgs;
    }
}