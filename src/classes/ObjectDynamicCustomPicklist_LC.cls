public class ObjectDynamicCustomPicklist_LC extends VisualEditor.DynamicPickList{
/*------------------------------------------------------------
Author:     Beau Anderson
Company:       Contractor
Description:   Class to get all objects for an org that aren't part of a managed package. These objects will be used to pass into a lightning component in order to get records for given queue chosen by the end user
Inputs:        None
Test Class:    
History
<Date>      <Author>     <Description>
------------------------------------------------------------*/

    public override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('', '');
        return defaultValue;
    }
    
    public override VisualEditor.DynamicPickListRows getValues(){
        Map<String, Schema.SObjectType> sObj = Schema.getGlobalDescribe();
        VisualEditor.DynamicPickListRows  custPickVals = new VisualEditor.DynamicPickListRows();
        VisualEditor.DataRow pickVal1 = new VisualEditor.DataRow('','');
        custPickVals.addRow(pickVal1);
        for(Schema.SObjectType s:sObj.values())
        {
            VisualEditor.DataRow pickVal2 = new VisualEditor.DataRow(string.valueOf(s),string.valueOf(s));
            custPickVals.addRow(pickVal2);
        }
        return custPickVals;
    }
}