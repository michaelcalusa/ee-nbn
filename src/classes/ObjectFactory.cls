/**
 * Created by alan on 2019-02-22.
 */

public class ObjectFactory {

    private static Map<Type, Object> singletonMap = new Map<Type, Object>();

    private static Map<String, Object> stubProviderMap = new Map<String, Object>();

    public static Object getInstance(Type type){

        if(Test.isRunningTest()) {
            //check if stubProvider Id is available
            try {
                StubProvider__c stubProvider = StubProvider__c.getValues(type.getName());
                if(stubProvider != null){
                    String instanceId = stubProvider.guid__c;
                    system.debug('found StubProvider instance ' + type.getName() + ':' + instanceId);
                    return stubProviderMap.get(instanceId);
                }
            }catch (exception e) {
                system.debug('error retrieving StubProvider: ' + e);
            }
        }

        if(!singletonMap.containsKey(type)){
            singletonMap.put(type, type.newInstance());
        }
        return singletonMap.get(type);

    }

    public static void setStubProvider(Type typeToMock, RecordingStubProvider stubProviderInstance){
        if(Test.isRunningTest()) {
            String guid = NS_SF_Utils.generateGUID();
            //insert into custom settings so StubProvider is only visible within the context of the current test transaction
            insert new StubProvider__c(Name = typeToMock.getName(), guid__c = guid);
            stubProviderMap.put(guid, Test.createStub(typeToMock, stubProviderInstance));
        }
    }

    public static void removeStubProvider(Type typeToMock){
        if(Test.isRunningTest()) {
            //get StubProvider instance from custom settings and remove from stubProviderMap
            StubProvider__c stubProvider = StubProvider__c.getValues(typeToMock.getName());
            if(stubProvider != null) {
                String instanceId = stubProvider.guid__c;
                stubProviderMap.remove(instanceId);
                system.debug('removed StubProvider instance ' + typeToMock.getName() + ':' + instanceId);
            }
        }
    }
}