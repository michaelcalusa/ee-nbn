@RestResource(urlMapping='/objectmap/*')
global class ObjectMapperRestResource {
    
    @HttpGet
    global static Map<String, String> getObjectValueMap() {
        
        Map<String, String> objectMap = new Map<String, String>();
        String query = '';
        
        try{        
            RestRequest req = RestContext.request;
            String objectName = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
            System.debug('object name = '+ objectName);
            
            if(req.params.isEmpty()){
                throw new CustomException('Expected query predicate in request params.');
            }else{
                String selectAttributes = 'Id';
                List<String> columns = new List<String>();
                //get all field names for this SObject          
                Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
                for (Schema.SObjectField sField : fieldMap.values()) {
                    String fieldAPIName = sField.getDescribe().getName();
                    if(!'Id'.equals(fieldAPIName)){
                        columns.add(fieldAPIName);
                    }
                }
                for(String column : columns){
                    selectAttributes += ',' + column;
                }
                query = 'SELECT ' + selectAttributes + ' FROM ' + objectName + ' WHERE ';
                
                List<String> keys = new List<String>(req.params.keySet());
                for (Integer i = 0; i<keys.size(); i++){
                    String value = String.escapeSingleQuotes(req.params.get(keys.get(i)));
                    String queryPredicate = keys.get(i) + ' = :value ';
                    query+= ( i > 0 ? ' AND ' + queryPredicate : queryPredicate);
                } 
                query+= ' LIMIT 1';
                System.debug('query string is: ' + query);
                
                for (sobject s: database.query(query)) {
                    //System.debug(s);
                    for(String key : columns){
                        objectMap.put(key, String.valueOf(s.get(key)));
                    }
                }
            }
            
        }catch(Exception e){
            System.debug(LoggingLevel.ERROR, e);
            objectMap.put('ERROR', e.getMessage() + ' Query string is: '+ query);
        }
        
        return objectMap;
    }
}