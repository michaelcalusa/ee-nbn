public class ObjectQueueListDynamicPicklist_LC extends VisualEditor.DynamicPickList{
/*------------------------------------------------------------
Author:     Beau Anderson
Company:       Contractor
Description:   Class to get all queues for a given object. The selected queue will be used to set and attirbute in the lightning controller. The attribute value will then be passed in
               from the lightning component to then drive the select query which returns a list data set. This class will be used in a design component as part of the ObjectsListByQueue
               component bundle
Test Class:    ObjectQueueListDynamicPicklist_Test_LC
History
<Date>      <Author>     <Description>
------------------------------------------------------------*/

    public override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('', '');
        return defaultValue;
    }
    
    public override VisualEditor.DynamicPickListRows getValues(){
        
        String componentName = 'ObjectsListByQueue';
        String objObject = '';
        List<LightningComponentConfigurations__c> lstLightningComponentConfig = new List<LightningComponentConfigurations__c>();
        lstLightningComponentConfig = CustomSettingsUtility.getLightningComponentConfiguration(componentName);
        for(LightningComponentConfigurations__c custSet : lstLightningComponentConfig){
            if(custSet.Component__c == componentName){
                objObject = custSet.Object_API_Name__c;
            }
        }
        
        Map<String,String> mapQueueIds = new Map<String,String>();
        
        If(objObject != null && !String.isblank(objObject)){
            for(QueueSobject qsob:[select QueueId,SobjectType from QueueSobject where SobjectType=:objObject]){
                mapQueueIds.put(qsob.QueueId,qsob.SobjectType);
            } 
        }
        else{
            for(QueueSobject qsob:[select QueueId,SobjectType from QueueSobject]){
                mapQueueIds.put(qsob.QueueId,qsob.SobjectType);
            }
        }
        
        Map<String, Schema.SObjectType> sObj = Schema.getGlobalDescribe();
        VisualEditor.DynamicPickListRows  queuePickVals = new VisualEditor.DynamicPickListRows();
        VisualEditor.DataRow pickVal1 = new VisualEditor.DataRow('','');
        queuePickVals.addRow(pickVal1);
        for(Group gr:[SELECT Name,Id FROM Group where Id IN:mapQueueIds.keyset()])
        {
            VisualEditor.DataRow pickVal2 = new VisualEditor.DataRow(string.valueOf(gr.name),string.valueOf(gr.Id));
            queuePickVals.addRow(pickVal2);
        }
        return queuePickVals;
    }
}