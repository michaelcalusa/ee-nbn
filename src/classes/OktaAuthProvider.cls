/***************************************************************************************
Class Name          :   OktaAuthProvider
Test Class          :   OktaAuthProvider_Test 
Created Date        :   24/10/2018 
Function            :   Used for Okta Client Credentails flow.
                        Referenced in Auth Provider.
                        Auth Provide is refrenced in Named Credentails.
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------
* Syed Moosa Nazir TN       24/10/2018                 Created
****************************************************************************************/
public class OktaAuthProvider extends Auth.AuthProviderPluginClass{
    public static final String RESOURCE_CALLBACK = '/services/authcallback/';
    public static final String DEFAULT_TOKEN_TYPE = 'BearerToken';
    public static final String ENCODING_XML = 'application/x-www-form-urlencoded;charset=UTF-8';
    public static final String ENCODING_JSON = 'application/json';
    public static final String DUMMY_CODE = '999';
    public static final String DOUBLEQUOTE = '"';
    // This class is dependant on this Custom Metadata Type created to hold custom parameters
    public static final String CUSTOM_MDT_NAME = 'Okta_Auth_Provider__mdt'; 
    public static final String CMT_FIELD_CALLBACK_URL = 'Callback_URL__c';
    public static final String CMT_FIELD_PROVIDER_NAME = 'Auth_Provider_Name__c';
    public static final String CMT_FIELD_AUTHTOKEN_URL = 'Access_Token_URL__c';
    public static final String CMT_FIELD_CLIENT_ID = 'Client_Id__c';
    public static final String CMT_FIELD_CLIENT_SECRET = 'Client_Secret__c';
    public static final String CMT_FIELD_USE_JSON = 'Use_JSON_Encoding__c';
    public static final String CMT_FIELD_SCOPE = 'Scope__c';

    public static final String GRANT_TYPE_PARAM = 'grant_type';
    public static final String CLIENT_ID_PARAM = 'client_id';
    public static final String CLIENT_SECRET_PARAM = 'client_secret';
    public static final String SCOPE_PARAM = 'scope';
    public static final String GRANT_TYPE_CLIENT_CREDS = 'client_credentials'; 
    public static final String STATE = 'state';
    public static final String CODE = 'code';
    /**********************************************************************************
    @Description:
    Name of custom metadata type to store this auth provider configuration fields.
    This method is required by its abstract parent class.
    **********************************************************************************/
    public String getCustomMetadataType() {
        return CUSTOM_MDT_NAME;
    }
    /**********************************************************************************
    @Description:
    Initiate callback. No End User authorization required in this flow so skip straight to the Token request.
    The interface requires the callback url to be defined. 
    Eg: https://test.salesforce.com/services/authcallback/<authprovidername>
    **********************************************************************************/
    public PageReference initiate(Map<string,string> config, String stateToPropagate) {
        final PageReference pageRef = new PageReference(getCallbackUrl(config));
        pageRef.getParameters().put(STATE, stateToPropagate);
        pageRef.getParameters().put(CODE, DUMMY_CODE); // Empirically found this is required, but unused
        System.debug('OktaAuthProvider==>initiate==>pageRef.getUrl==>   '+pageRef.getUrl());
        return pageRef;
    }
    /**********************************************************************************
    @Description:
    This method composes the callback URL automatically UNLESS it has been overridden through Configuration.
    Normally one should not override the callback URL, but it's there in case the generated URL doesn't work.
    Ex: https://{salesforce-hostname}/services/authcallback/{urlsuffix}
    **********************************************************************************/
    private String getCallbackUrl(Map<string,string> config) {
        final String overrideUrl = config.get(CMT_FIELD_CALLBACK_URL);
        final String generatedUrl = URL.getSalesforceBaseUrl().toExternalForm() + RESOURCE_CALLBACK + config.get(CMT_FIELD_PROVIDER_NAME);
        System.debug('OktaAuthProvider==>getCallbackUrl==>overrideUrl==>    '+overrideUrl);
        System.debug('OktaAuthProvider==>getCallbackUrl==>generatedUrl==>   '+generatedUrl);
        return String.isEmpty(overrideUrl) ? generatedUrl : overrideUrl;
    }
    /**********************************************************************************
    @Description:
    Handle callback (from initial loop back "code" step in the flow).
    In the Client Credentials flow, this method retrieves the access token directly.
    Required by parent class.
    Error handling here is a bit painful as the UI never displays the exception or error message supplied here.
    The exception is thrown for Logging/Debugging purposes only.
    **********************************************************************************/
    public Auth.AuthProviderTokenResponse handleCallback(Map<string,string> config, Auth.AuthProviderCallbackState state) {
        System.debug('OktaAuthProvider==>handleCallback==>config==> '+config);
        System.debug('OktaAuthProvider==>handleCallback==>state==>  '+state);
        final TokenResponse response = retrieveToken(config);
        if (response.isError()) {
            throw new TokenException(response.getErrorMessage());
        }
        return new Auth.AuthProviderTokenResponse(
            config.get(CMT_FIELD_PROVIDER_NAME),
            response.access_token,
            config.get(CMT_FIELD_CLIENT_SECRET),
            state.queryParameters.get('state')
        );
    }
    /**********************************************************************************
    @Description:
    Refresh is required by the parent class and it's used if the original Access Token has expired.
    In the Client Credentials flow, there is no Refresh token,
    so its implementation is exactly the same as the Initiate() step.
    **********************************************************************************/
    public override Auth.OAuthRefreshResult refresh(Map<String,String> config, String refreshToken) {
        System.debug('OktaAuthProvider==>refresh==>config==>    '+config);
        System.debug('OktaAuthProvider==>refresh==>refreshToken==>  '+refreshToken);
        final TokenResponse response = retrieveToken(config);
        System.debug('OktaAuthProvider==>refresh==>response==>  '+response);
        return new Auth.OAuthRefreshResult(response.access_token, response.token_type);
    }
    /**********************************************************************************
    @Description:
    getUserInfo is required by the Parent class, but not fully supported by this provider.
    Effectively the Client Credentials flow is only useful for Server-to-Server API integrations
    and cannot be used for other contexts such as a Registration Handler for Communities.
    **********************************************************************************/ 
    public Auth.UserData getUserInfo(Map<string,string> config, Auth.AuthProviderTokenResponse response) {
        System.debug('OktaAuthProvider==>getUserInfo==>config==>    '+config);
        System.debug('OktaAuthProvider==>getUserInfo==>response==>  '+response);
        final TokenResponse token = retrieveToken(config);
        final Auth.UserData userData = new Auth.UserData(
            null, // identifier
            null, // firstName
            null, // lastName
            null, // fullName
            null, // email
            null, // link
            null, // userName
            null,  //locale
            config.get(CMT_FIELD_PROVIDER_NAME), //provider
            null, // siteLoginUrl
            new Map<String,String>()
        );
        System.debug('OktaAuthProvider==>getUserInfo==>token==> '+token);
        System.debug('OktaAuthProvider==>getUserInfo==>userData==>  '+userData);
        return userData;
    }
    
    /**********************************************************************************
    @Description:
    Private method that gets the Auth Token using the Client Credentials Flow.
    **********************************************************************************/ 
    private TokenResponse retrieveToken(Map<String,String> config) {  
        System.debug('OktaAuthProvider==>retrieveToken==>config==>  '+config);
        final Boolean useJSONEncoding = Boolean.valueOf(config.get(CMT_FIELD_USE_JSON));
        final HttpRequest req = new HttpRequest();
        final PageReference endpoint = new PageReference(
            config.get(CMT_FIELD_AUTHTOKEN_URL)
        );
        if (!useJSONEncoding) { // Including the Query String breaks JSON encoded OAuth
            endpoint.getParameters().put('grant_type',GRANT_TYPE_CLIENT_CREDS);
        }
        // Determine whether or not to use JSON encoding
        final String encoding = useJSONEncoding ? ENCODING_JSON : ENCODING_XML;
        final String encodedParams = encodeParameters(config,encoding);
        // Set Request parameters
        req.setEndpoint(endpoint.getUrl()); 
        req.setHeader('Content-Type',encoding); 
        req.setMethod('POST'); 
        req.setBody(encodedParams);
        final HTTPResponse res = new Http().send(req); 
        System.debug('OktaAuthProvider==>retrieveToken==>request=getEndpoint==> '+req.getEndpoint());
        System.debug('OktaAuthProvider==>retrieveToken==>encoding(request=Header)==>    '+encoding);
        System.debug('OktaAuthProvider==>retrieveToken==>response=getStatus==>  ' +res.getStatus());
        System.debug('OktaAuthProvider==>retrieveToken==>response=getStatusCode==>  ' +res.getStatusCode());
        System.debug('OktaAuthProvider==>retrieveToken==>response=getBody==>    '+res.getBody());
        final Integer statusCode = res.getStatusCode();
        if ( statusCode == 200) {
            TokenResponse token =  deserializeToken(res.getBody());
            // Ensure values for key fields
            token.token_type = (token.token_type == null) ? DEFAULT_TOKEN_TYPE : token.token_type;
            return token;
    
        } else {
            return deserializeToken(res.getBody());
        }

    }
    /**********************************************************************************
    @Description:
    Private method that deserialise response and return token
    **********************************************************************************/ 
    @testVisible
    private TokenResponse deserializeToken(String responseBody) {        
        // use default parsing for everything we can.
        TokenResponse parsedResponse = (TokenResponse) System.JSON.deserialize(responseBody, TokenResponse.class);
        System.debug('OktaAuthProvider==>deserializeToken==>parsedResponse==>   ' + parsedResponse);
        return parsedResponse;
    }
    /**********************************************************************************
    @Description:
    Private method that Conditionally encode parameters as URL-style or JSON
    **********************************************************************************/
    @testVisible
    private String encodeParameters(Map<String,String> config,String encoding) {
        // Pull out the subset of configured parameters that will be sent
        Map<String,String> params = new Map<String,String>();
        params.put(GRANT_TYPE_PARAM,GRANT_TYPE_CLIENT_CREDS);
        params.put(CLIENT_ID_PARAM, config.get(CMT_FIELD_CLIENT_ID));
        params.put(CLIENT_SECRET_PARAM, config.get(CMT_FIELD_CLIENT_SECRET));
        final String scope = config.get(CMT_FIELD_SCOPE);
        if (!String.isEmpty(scope)) {
            params.put(SCOPE_PARAM,scope);
        }
        return encoding == ENCODING_JSON ? encodeAsJSON(params) : encodeAsURL(params);
    }
    /**********************************************************************************
    @Description:
    Private method that encode parameters as JSON
    **********************************************************************************/
    private String encodeAsJSON(Map<String,String> params) {
        String output = '{';
        for (String key : params.keySet()) {
            output += (output == '{' ? '' : ', ');
            output += DOUBLEQUOTE + key + DOUBLEQUOTE + ':';
            output += DOUBLEQUOTE + params.get(key) + DOUBLEQUOTE;
        }
        output += '}';
        return output;
    }
    /**********************************************************************************
    @Description:
    Private method that encode parameters as URL-style
    **********************************************************************************/
    private String encodeAsURL(Map<String,String> params) {
        String output = '';
        for (String key : params.keySet()) {
            output += (String.isEmpty(output) ? '' : '&');
            output += key + '=' + params.get(key);
        }
        return output;
    }

    /**********************************************************************************
    @Description:
    Wrapper class to store the HTTP response
    // OAuth Response is a JSON body like this on a Successful call
    {
        "access_token" : "kRxqmPr2b223uzTUGnndQhXWv8F4",
        "token_type" : "BearerToken",
        "expires_in" : "3599",
        "scope" : ""
    }
    // On failure - below structure
    { 
        "error_description" : "Invalid value for 'client_id' parameter.",
        "error" : "invalid_client"
        "errorCode": "E0000022",
        "errorSummary": "The endpoint does not support the provided HTTP method",
        "errorLink": "E0000022",
        "errorId": "oaeNKIiUrjjRryNFGIRGUSV1Q",
        "errorCauses": []
    }
    **********************************************************************************/
    public class TokenResponse {
        // success scenario variables
        public String access_token {get;set;}
        public String token_type {get;set;}
        public String expires_in {get;set;}
        public String scope {get;set;}
        // error scenario variables
        public String error_description {get; set;}
        public String error {get; set;}
        public String errorCode {get; set;}
        public String errorSummary {get; set;}
        public String errorLink {get; set;}
        public String errorId {get; set;}
        public List<String> errorCauses {get;set;}
        public Boolean isError() {
            return error != null;
        }
        public String getErrorMessage() {
            if (error != null) {
                return errorCode;
            }
            return null;
        }
    }
    /**********************************************************************************
    @Description:
    Custom exception type so we can wrap and rethrow
    **********************************************************************************/
    public class TokenException extends Exception {}
}