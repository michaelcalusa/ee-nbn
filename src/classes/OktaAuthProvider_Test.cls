/***************************************************************************************
Test Class          :   OktaAuthProvider_Test
Main Class Name     :   OktaAuthProvider
Created Date        :   24/10/2018 
Function            :   Used for Okta Client Credentails flow.
                        Referenced in Auth Provider.
                        Auth Provide is refrenced in Named Credentails.
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------
* Syed Moosa Nazir TN       24/10/2018                 Created
****************************************************************************************/
@IsTest(isParallel=true)
public class OktaAuthProvider_Test {
    public static final String CONTENT_TYPE_HEADER = 'Content-Type';
    public static final String CONTENT_TYPE_JSON = 'application/json';
    private static final String ACCESS_TOKEN = 'gv6iCx48OaH76ufiGXBUlRTRTXxx';
    private static final String EXPIRES_IN = '123';
    private static final String STATE = 'mocktestState'; 
    private static final String TOKEN_TYPE = 'Bearer'; 
    
    private static final String PROVIDER_NAME = 'UnitTestProvider'; 
    
    private static final String CALLBACK_URL_OVERRIDE = 
    System.URL.getSalesforceBaseUrl().getHost()+'/services/authcallback/' + PROVIDER_NAME; 

    private static final String TEST_USER_EMAIL = 'developer@example.com';
    private static final String KEY = 'testKey'; 
    private static final String SECRET = 'testSecret'; 
    private static final String SCOPE = 'View';

    private static final String STATE_TO_PROPOGATE = 'testState'; 
    private static final String ACCESS_TOKEN_URL = 
    'http://www.dummyhost.com/accessTokenUri';
    private static final String EMPTY_VALUE = ''; 

    private static final String jsonGoodToken = '{'+
    '  \"token_type\" : \"Bearer\",'+
    '  \"access_token\" : \"gv6iCx48OaH76ufiGXBUlRTRTXxx\",'+
    '  \"scope\" : \"\"'+
    '}';
    private static final String jsonError2 = '{ \"errorcode\" : \"invalid_client\", \"error_description\" : \"invalid_client\", \"errorSummary\" : \"invalid_client\", \"errorLink\" : \"invalid_client\", \"errorId\" : \"invalid_client\", \"error\" :\"Client credentials are invalid\"}';

    
    private static Map<String,String> setupAuthProviderConfig () 
    { 
        final Map<String,String> authProviderConfiguration = new Map<String,String>(); 
        authProviderConfiguration.put(OktaAuthProvider.CMT_FIELD_PROVIDER_NAME, PROVIDER_NAME); 
        authProviderConfiguration.put(OktaAuthProvider.CMT_FIELD_AUTHTOKEN_URL, ACCESS_TOKEN_URL); 
        authProviderConfiguration.put(OktaAuthProvider.CMT_FIELD_CALLBACK_URL, EMPTY_VALUE); 
        authProviderConfiguration.put(OktaAuthProvider.CMT_FIELD_CLIENT_ID, KEY); 
        authProviderConfiguration.put(OktaAuthProvider.CMT_FIELD_CLIENT_SECRET,SECRET); 
        authProviderConfiguration.put(OktaAuthProvider.CMT_FIELD_SCOPE,SCOPE);
        authProviderConfiguration.put(OktaAuthProvider.CMT_FIELD_USE_JSON,'False');        
       
        return authProviderConfiguration; 
    
    } 
    
    static testMethod void testMetadataType()
    {
        final OktaAuthProvider provider = new OktaAuthProvider(); 
        final String actual = provider.getCustomMetadataType();
        final String expected = OktaAuthProvider.CUSTOM_MDT_NAME;
        System.assertEquals(expected, actual);
        
    }
    
    static testMethod void testInitiateMethod()
    {
        final Map<String,String> authProviderConfiguration = setupAuthProviderConfig(); 
        final OktaAuthProvider provider = new OktaAuthProvider(); 
        
        final PageReference expectedUrl = new PageReference(authProviderConfiguration.get('Callback_URL__c'));
        expectedUrl.getParameters().put('state',STATE_TO_PROPOGATE);

        final PageReference actualUrl = provider.initiate(authProviderConfiguration, STATE_TO_PROPOGATE); 
        System.assertEquals(expectedUrl.getParameters().get('state'), actualUrl.getParameters().get('state'));
   
    }
    
    static testMethod void testHandleCallback() 
    { 
        final Map<String,String> authProviderConfiguration = setupAuthProviderConfig(); 
        final OktaAuthProvider provider = new OktaAuthProvider(); 
        
        Test.setMock(HttpCalloutMock.class, new OktaMockService(jsonGoodToken,200)); 
    
        final Map<String,String> queryParams = new Map<String,String>(); 
        queryParams.put('code','code'); 
        queryParams.put('state',STATE); 
        
        final Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null,null,queryParams); 
        
        final Auth.AuthProviderTokenResponse actualAuthProvResponse = provider.handleCallback(authProviderConfiguration, cbState); 
        
        final Auth.AuthProviderTokenResponse expectedAuthProvResponse = new Auth.AuthProviderTokenResponse(PROVIDER_NAME, ACCESS_TOKEN, SECRET, STATE); 
    
        System.assertEquals(expectedAuthProvResponse.provider, actualAuthProvResponse.provider); 
        System.assertEquals(expectedAuthProvResponse.oauthToken, actualAuthProvResponse.oauthToken); 
        System.assertEquals(expectedAuthProvResponse.oauthSecretOrRefreshToken, actualAuthProvResponse.oauthSecretOrRefreshToken); 
        System.assertEquals(expectedAuthProvResponse.state, actualAuthProvResponse.state); 

    }

    static testMethod void testHandleCallbackJson() 
    { 
        final Map<String,String> authProviderConfiguration = setupAuthProviderConfig();
        // override the USE_JSON config
        authProviderConfiguration.put(OktaAuthProvider.CMT_FIELD_USE_JSON,'True');

        final OktaAuthProvider provider = new OktaAuthProvider(); 
        
        Test.setMock(HttpCalloutMock.class, new OktaMockService(jsonGoodToken,200)); 
    
        final Map<String,String> queryParams = new Map<String,String>(); 
        queryParams.put('code','code'); 
        queryParams.put('state',STATE); 
        
        final Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null,null,queryParams); 
        
        final Auth.AuthProviderTokenResponse actualAuthProvResponse = provider.handleCallback(authProviderConfiguration, cbState); 
        
        final Auth.AuthProviderTokenResponse expectedAuthProvResponse = new Auth.AuthProviderTokenResponse(PROVIDER_NAME, ACCESS_TOKEN, SECRET, STATE); 
    
        System.assertEquals(expectedAuthProvResponse.provider, actualAuthProvResponse.provider); 
        System.assertEquals(expectedAuthProvResponse.oauthToken, actualAuthProvResponse.oauthToken); 
        System.assertEquals(expectedAuthProvResponse.oauthSecretOrRefreshToken, actualAuthProvResponse.oauthSecretOrRefreshToken); 
        System.assertEquals(expectedAuthProvResponse.state, actualAuthProvResponse.state); 

    }

    static testMethod void testHandleCallbackError1() 
    { 
        final Map<String,String> authProviderConfiguration = setupAuthProviderConfig(); 
        final OktaAuthProvider provider = new OktaAuthProvider(); 
        
        Test.setMock(HttpCalloutMock.class, new OktaMockService(jsonError2,401)); 
    
        final Map<String,String> queryParams = new Map<String,String>(); 
        queryParams.put('code','code'); 
        queryParams.put('state',STATE); 
        
        final Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null,null,queryParams); 
        
        try {
            final Auth.AuthProviderTokenResponse actualAuthProvResponse = provider.handleCallback(authProviderConfiguration, cbState); 
            System.Assert(false,'Should throw an Exception on bad http return code!');
        } catch (OktaAuthProvider.TokenException ex) {
            System.AssertEquals('invalid_client',ex.getMessage());
        }
    }

    static testMethod void testHandleCallbackError2() 
    { 
        final Map<String,String> authProviderConfiguration = setupAuthProviderConfig(); 
        final OktaAuthProvider provider = new OktaAuthProvider(); 
        
        Test.setMock(HttpCalloutMock.class, new OktaMockService(jsonError2,401)); 
    
        final Map<String,String> queryParams = new Map<String,String>(); 
        queryParams.put('code','code'); 
        queryParams.put('state',STATE); 
        
        final Auth.AuthProviderCallbackState cbState = new Auth.AuthProviderCallbackState(null,null,queryParams); 
        
        try {
            final Auth.AuthProviderTokenResponse actualAuthProvResponse = provider.handleCallback(authProviderConfiguration, cbState); 
            System.Assert(false,'Should throw an Exception on bad http return code!');
        } catch (OktaAuthProvider.TokenException ex) {
            System.AssertEquals('invalid_client',ex.getMessage());
        }
    } 

    static testMethod void testRefresh() {

        final Map<String,String> config = setupAuthProviderConfig(); 
        final OktaAuthProvider provider = new OktaAuthProvider(); 
        
        Test.setMock(HttpCalloutMock.class, new OktaMockService(jsonGoodToken,200)); 

        final Auth.OAuthRefreshResult actual = provider.refresh(config,'myUnusedRefreshToken');        
    
        final Auth.OAuthRefreshResult expected = new Auth.OAuthRefreshResult(ACCESS_TOKEN, TOKEN_TYPE); 
    
        System.assertEquals(expected.accessToken, actual.accessToken); 
        System.assertEquals(expected.refreshToken, actual.refreshToken); 
        System.assertEquals(expected.error,actual.error);
    } 
    
    static testMethod void testGetUserInfo() 
    { 
        Map<String,String> authProviderConfiguration = setupAuthProviderConfig(); 
        OktaAuthProvider provider = new OktaAuthProvider(); 
    
        Test.setMock(HttpCalloutMock.class, new OktaMockService(jsonGoodToken,200)); 
    
        Auth.AuthProviderTokenResponse response = new Auth.AuthProviderTokenResponse(PROVIDER_NAME, ACCESS_TOKEN ,'sampleOauthSecret', STATE); 
        Auth.UserData actualUserData = provider.getUserInfo(authProviderConfiguration, response) ; 
    
        Map<String,String> provMap = new Map<String,String>(); 
        provMap.put('key1', 'value1'); 
        provMap.put('key2', 'value2'); 
    
        final Auth.UserData expectedUserData = new Auth.UserData(
              null // identifier
            , null // firstName
            , null // lastName
            , null  // fullName
            , null // email
            , null // link
            , null // userNAme
            , null  // locale
            , PROVIDER_NAME  // provider
            , null // siteLoginUrl
            , new Map<String,String>()); 
    
        System.assertNotEquals(actualUserData,null); 
        System.assertEquals(expectedUserData.firstName, actualUserData.firstName); 
        System.assertEquals(expectedUserData.lastName, actualUserData.lastName); 
        System.assertEquals(expectedUserData.fullName, actualUserData.fullName); 
        System.assertEquals(expectedUserData.username, actualUserData.username); 
        System.assertEquals(expectedUserData.locale, actualUserData.locale); 
        System.assertEquals(expectedUserData.provider, actualUserData.provider); 
        System.assertEquals(expectedUserData.siteLoginUrl, actualUserData.siteLoginUrl); 
    } 

    static testmethod void testEncodingAsJSON() {
        final Map<String,String> config = setupAuthProviderConfig();

        final String output = new OktaAuthProvider().encodeParameters(config,OktaAuthProvider.ENCODING_JSON);
        System.debug('encoded jSON: ' + output);

        // Assert that we deserialize parse the output as JSON
        Map<String,Object> result = (Map<String,Object>)JSON.deserializeUntyped(output);
 
        System.AssertEquals('client_credentials',(String)result.get(OktaAuthProvider.GRANT_TYPE_PARAM),'Grant type did not match');
        System.AssertEquals(KEY,(String)result.get(OktaAuthProvider.CLIENT_ID_PARAM),'Client key did not match');
        System.AssertEquals(SECRET,(String)result.get(OktaAuthProvider.CLIENT_SECRET_PARAM),'Client secret did not match');
        System.AssertEquals(SCOPE,(String)result.get(OktaAuthProvider.SCOPE_PARAM),'Scope did not match');
    }

    static testmethod void testTokenMembers() {
        OktaAuthProvider.TokenResponse resp = (OktaAuthProvider.TokenResponse) JSON.deserialize(jsonGoodToken, OktaAuthProvider.TokenResponse.class);
        System.AssertEquals('Bearer',resp.token_type);
        System.AssertEquals(null,resp.expires_in);
        System.AssertEquals('gv6iCx48OaH76ufiGXBUlRTRTXxx',resp.access_token);
        System.AssertEquals('',resp.scope);
    }

    static testmethod void testErrorBad() {
        OktaAuthProvider.TokenResponse resp = (OktaAuthProvider.TokenResponse) JSON.deserialize(jsonGoodToken, OktaAuthProvider.TokenResponse.class);
        System.AssertEquals(null,resp.getErrorMessage());
    }
    
     
    // Implement a mock http response generator for Okta. 
    public class OktaMockService implements HttpCalloutMock 
    {
        String jsonResponse;
        Integer httpCode;

        public OktaMockService(String json, Integer code) {
            this.jsonResponse=json;
            this.httpCode=code;
        }

        public HTTPResponse respond(HTTPRequest req) 
        { 
            if (req.getHeader(CONTENT_TYPE_HEADER)==CONTENT_TYPE_JSON) {
                // Check for Query Parameters on the Endpoint URL and if any are found, return a server error
                final PageReference endpoint = new PageReference(req.getEndpoint());
                if (endpoint.getParameters().size() > 0) {
                    this.httpCode=500;
                    this.jsonResponse= jsonError2;
                }
            }
            // Create the response 
            HttpResponse res = new HttpResponse(); 
            res.setHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_JSON); 
            res.setBody(jsonResponse); 
            res.setStatusCode(this.httpCode); 
            return res; 
        } 
    }
    
}