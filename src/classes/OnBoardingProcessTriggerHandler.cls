/*--------------------------------------------------------------------------------------------------------------------------
Author:        Syed Moosa Nazir TN
Company:       Wipro Technologies
Description:   A class created to manage trigger actions from the Onboarding Step object 
               Responsible for:
               1 - The Onboarding step “Start Date” cannot be prior to the Onboarding Process “Start Date” (BAU-33)
               
History
<Date>            <Authors Name>        <Brief Description of Change> 
29-06-2016      Syed Moosa Nazir TN     Created.
---------------------------------------------------------------------------------------------------------------------------*/
public without sharing class OnBoardingProcessTriggerHandler extends TriggerHandler {
    /*------------------------------------------------------------
    Author:         Syed Moosa Nazir TN
    Company:        Wipro Technologies
    Description:    Constructor
    Inputs:         
    Returns:        
    
    History
    <Date>            <Authors Name>        <Brief Description of Change>
    29-06-2016      Syed Moosa Nazir TN     Created
    ----------------------------------------------------------------------------------------------------------------------*/
    public OnBoardingProcessTriggerHandler() {
        if(Test.isRunningTest()){
            this.setMaxLoopCount(10);
        }else{this.setMaxLoopCount(2);}      
        System.Debug( 'NBN: -> OnBoardingProcessTriggerHandler invoked' );        
    }
    /*----------------------------------------------------------------------------------------------------------------------
    Author:         Syed Moosa Nazir TN
    Company:        Wipro Technologies
    Description:    "beforeUpdate" method overrides the method of TriggerHandler extended method.
                    It calls the business logic methods ("validateOnBoardingProcessStartDate")
    Inputs:         map <id,SObject> oldMap, map <id,SObject> newMap
    Returns:        
    
    History
    <Date>            <Authors Name>        <Brief Description of Change>
    29-06-2016      Syed Moosa Nazir TN     Created
    ----------------------------------------------------------------------------------------------------------------------*/
    protected override void beforeUpdate(map <id,SObject> oldMap, map <id,SObject> newMap) {
        validateOnBoardingProcessStartDate((Map <Id,Onboarding_Process__c>) (newMap));
    }
    /*----------------------------------------------------------------------------------------------------------------------
    Author:         Syed Moosa Nazir TN
    Company:        Wipro Technologies
    Description:    "validateOnBoardingProcessStartDate" method validates the Onboarding Process "Start Date".
                    The Onboarding step “Start Date” cannot be prior to the Onboarding Process “Start Date”
                    This method called by "beforeUpdate" method
    Inputs:         map<id,Onboarding_Process__c> OnBoardingProcessNewMap
    Returns:        void method.
    
    History
    <Date>            <Authors Name>        <Brief Description of Change>
    29-06-2016      Syed Moosa Nazir TN     "validateOnBoardingProcessStartDate" method validates the Onboarding Process 
                                            "Start Date".
    ----------------------------------------------------------------------------------------------------------------------*/
    private void validateOnBoardingProcessStartDate(map<id,Onboarding_Process__c> OnBoardingProcessNewMap){ 
        for(Onboarding_Process__c OnBoardingProcess : [select id, (select id, Start_Date__c from Product_On_Boarding_Steps__r where Start_Date__c  <> null ORDER BY Start_Date__c asc LIMIT 1) from Onboarding_Process__c  where ID IN: OnBoardingProcessNewMap.keySet()]){
            if((OnBoardingProcess.Product_On_Boarding_Steps__r.size() > 0) && (OnBoardingProcessNewMap.get(OnBoardingProcess.id).Start_Date__c <> null) && (OnBoardingProcessNewMap.get(OnBoardingProcess.id).Start_Date__c > OnBoardingProcess.Product_On_Boarding_Steps__r.get(0).Start_Date__c)){
                OnBoardingProcessNewMap.get(OnBoardingProcess.id).addError(label.Onboarding_Process_Error_Message);
            }
        }
    }
    // exception class
    public class OnBoardingProcessTriggerHandlerException extends Exception {} 
}