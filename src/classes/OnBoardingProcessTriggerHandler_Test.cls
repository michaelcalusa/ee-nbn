/*--------------------------------------------------------------------------------------------------------------------------
Author:        Syed Moosa Nazir TN
Company:       Wipro Technologies
Description:   A class created to manage trigger actions from the Onboarding Step object 
               Responsible for testing the below scenario
               1 - The Onboarding step “Start Date” cannot be prior to the Onboarding Process “Start Date” (BAU-33)
               
History
<Date>            <Authors Name>        <Brief Description of Change> 
29-06-2016      Syed Moosa Nazir TN     Created.
---------------------------------------------------------------------------------------------------------------------------*/
@isTest
private class OnBoardingProcessTriggerHandler_Test{
	static List<Account> ListOfAccounts = new List<Account> ();
	static List<Onboarding_Process__c> ListOfOnboardingProcess = new List<Onboarding_Process__c> ();
	static List<On_Boarding_Step__c> ListOfOnboardingStep = new List<On_Boarding_Step__c> ();
	static void getRecords(){
		// Create Account
		ListOfAccounts = TestDataUtility.getListOfAccounts(1,true);
		System.assert(ListOfAccounts.get(0).Id != null, 'Account Record not created');
		// Create Onboarding Process
		ListOfOnboardingProcess = TestDataUtility.getListOfOnboardingProcess(1,true,ListOfAccounts.get(0).id);
		System.assert(ListOfOnboardingProcess.get(0).Id != null, 'Onoarding Process Record not created');
		// Query Onboarding Step records which got created by Process Builder
		ListOfOnboardingStep = [select id, Start_Date__c from On_Boarding_Step__c where Related_Product__c IN: ListOfOnboardingProcess];
		// Update Start_Date__c field as Today for the Onboarding Step records
		for(On_Boarding_Step__c OnBoardingStep : ListOfOnboardingStep){
			OnBoardingStep.Start_Date__c = date.today()-3;
		}
		update ListOfOnboardingStep;
		System.assertEquals(ListOfOnboardingStep.get(0).Start_Date__c, date.today()-3);
	}
	static testmethod void validateOnBoardingProcessStartDate_NegativeTest(){
		getRecords();
		Test.startTest();
		// Negative Testing - Onboarding Process "Start_Date__c" is not later than Onboarding Step "Start_Date__c".
		update ListOfOnboardingProcess;
		System.assertEquals(ListOfOnboardingProcess.get(0).Start_Date__c, date.today()-3);
		Test.stopTest();
	}
	static testmethod void validateOnBoardingProcessStartDate_PositiveTest(){
		getRecords();
		Test.startTest();
		// Positive Testing - Onboarding Process "Start_Date__c" is later than Onboarding Step "Start_Date__c".
		// Update Start_Date__c field as Today for the Onboarding Step records
		for(Onboarding_Process__c OnBoardingProcess : ListOfOnboardingProcess){
			OnBoardingProcess.Start_Date__c = date.today();
		}
		try{
			update ListOfOnboardingProcess;
		}
		catch(exception e){
			System.assertEquals([select Start_Date__c from Onboarding_Process__c where id IN: ListOfOnboardingProcess].Start_Date__c, date.today()-3);
		}
		Test.stopTest();
	}
}