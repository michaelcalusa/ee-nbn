Public Without Sharing Class OnBoardingStepTriggerHandler extends TriggerHandler {

/**
Author        :    SantoshRaoBompally
Company       :    WIPRO Technologies
Description   :    HandlerClass for OnBoardingStepManagement to meet the below requirements 
                    req1 (BAU10) - Ability to track the total BusinessHours spent by onBoarding Step on particular step 
Inputs        :        
Test Class    :    OnBoardingStepTriggerHandler_Test
History
<Date> <Authors Name>       <Brief Description of Change>
 
 */
 Public BusinessHours nbnBusinessHours ; // to get the Default Busniesshours of NBN 
 Public Onboarding_Status_Tracking__c ExistingRecord ; // Stores if the Step is already recorded 
 Public boolean thereisarecordexisting ; //Boolean variable to to check if the records are already existing in OnBoarding Status Tracker
 Public List<Onboarding_Status_Tracking__c> OnBoardingStepTrackstobeinserted ;
  Public List<Onboarding_Status_Tracking__c> OnBoardingStepTrackstobeUpdated ;
 public OnBoardingStepTriggerHandler(){
        if(Test.isRunningTest()){
            this.setMaxLoopCount(200);
        }
        else{
            this.setMaxLoopCount(1);
        }
        
       // nbnBusinessHours = [select id from BusinessHours Where IsDefault = true LIMIT 1];
        OnBoardingStepTrackstobeinserted = new List<Onboarding_Status_Tracking__c>();
        OnBoardingStepTrackstobeUpdated = new List<Onboarding_Status_Tracking__c>();
    }
// Update Step Start Time to Current Time when the records is inserted    
  protected override void beforeInsert(List<Sobject> SO){
      system.debug('**insert called**');
    List<On_Boarding_Step__c> Beforeinsertrecords = (List<On_Boarding_Step__c>)SO ;
    try{
        for(On_Boarding_Step__c SingleOnBoardingStepRecord : Beforeinsertrecords){
      
            if(SingleOnBoardingStepRecord.Status__C <> null){
                if(SingleOnBoardingStepRecord.start_date__C==null){
                    SingleOnBoardingStepRecord.Step_Start_Time__c = system.now();
                }
                else{
                    SingleOnBoardingStepRecord.Step_Start_Time__c = SingleOnBoardingStepRecord.start_date__C;
                }
                
            }
        }
    }
    catch(Exception excp){
        system.debug('Exception occured while inserting steps'+excp.getMessage()+excp.getStacktraceString());
    }

  
  }  


 protected override void beforeUpdate(map<id,SObject> oldMap, map<id,SObject> newMap) {
       system.debug('****calling step update*');
   Map<Id,On_Boarding_Step__c> BeforeUpdateOldOnboardingStep = (Map<Id,On_Boarding_Step__c>)oldMap;
  
    //List<Onboarding_Status_Tracking__c> ExitingStatusRecords = [select id,Onboarding_Step__c,Status_Name__c,Number_of_Business_Hours_Elapsed_Backup__c from Onboarding_Status_Tracking__c where Onboarding_Step__c In :BeforeUpdateOldOnboardingStep.keyset() ];
  //  List<Onboarding_Status_Tracking__c> ExitingStatusRecords = [select id,Onboarding_Step__c,Status_Name__c from Onboarding_Status_Tracking__c where Onboarding_Step__c In :BeforeUpdateOldOnboardingStep.keyset() ];
    List<On_Boarding_Step__c> BeforeUpdateOnboardingStep = (List<On_Boarding_Step__c>)newMap.values();
            system.debug('***BeforeUpdateOnboardingStep[0].start_date__c**'+newMap);
          
    try{
        for(On_Boarding_Step__c SingleOnboardingstepRecord : BeforeUpdateOnboardingStep){
         system.debug('***SingleOnboardingstepRecord.start_date__C***'+SingleOnboardingstepRecord.start_date__C);
            system.debug('***BeforeUpdateOldOnboardingStep.get(SingleOnboardingstepRecord.id).start_date__C**'+BeforeUpdateOldOnboardingStep.get(SingleOnboardingstepRecord.id).start_date__C);
         if(SingleOnboardingstepRecord.start_date__C != BeforeUpdateOldOnboardingStep.get(SingleOnboardingstepRecord.id).start_date__C && BeforeUpdateOldOnboardingStep.get(SingleOnboardingstepRecord.id).start_date__C == null){// 
         
          Date startdate = SingleOnboardingstepRecord.start_date__C;
          Datetime startdatetime = datetime.newInstance(startdate.year(), startdate.month(),startdate.day(),9,0,0);
          SingleOnboardingstepRecord.Step_Start_Time__C = startdatetime ;          
          
         }
          // ExistingRecord = new Onboarding_Status_Tracking__c();
           // system.debug('Debug Size:'+ExitingStatusRecords.size());
        /*if(ExitingStatusRecords.size() >0){
         for (Onboarding_Status_Tracking__c SingleStatusTrackStep: ExitingStatusRecords){
                 if(SingleStatusTrackStep.Status_Name__c == BeforeUpdateOldOnboardingStep.get(SingleOnboardingstepRecord.id).Status__C && SingleStatusTrackStep.Onboarding_Step__c == SingleOnboardingstepRecord.id){
                   thereisarecordexisting = true;
                   ExistingRecord = SingleStatusTrackStep ;
                   break ; // Break if a record is found 
                 }else{
                   thereisarecordexisting = false ; 
                 }
            }
            }else{
             thereisarecordexisting = false ; 
            
            }*/
            system.debug('****SingleOnboardingstepRecord.Status__C**'+SingleOnboardingstepRecord.Status__C);
            system.debug('****BeforeUpdateOldOnboardingStep.get(SingleOnboardingstepRecord.id).Status__C**'+BeforeUpdateOldOnboardingStep.get(SingleOnboardingstepRecord.id).Status__C);
             if(SingleOnboardingstepRecord.Status__C <> BeforeUpdateOldOnboardingStep.get(SingleOnboardingstepRecord.id).Status__C && BeforeUpdateOldOnboardingStep.get(SingleOnboardingstepRecord.id).Status__C != null){
           // If there are No Step Records under Onboarding Step, Create a New one Else Update the Existing One 
           // if(!thereisarecordexisting){
            
            Onboarding_Status_Tracking__c onBoardingSttausTrackObj = new Onboarding_Status_Tracking__c();
            onBoardingSttausTrackObj.Status_Name__c = BeforeUpdateOldOnboardingStep.get(SingleOnboardingstepRecord.id).Status__C;
            onBoardingSttausTrackObj.Onboarding_Step__c = SingleOnboardingstepRecord.id;
            Date startStatusDate=date.newinstance(SingleOnboardingstepRecord.Step_Start_Time__c.year(), SingleOnboardingstepRecord.Step_Start_Time__c.month(), SingleOnboardingstepRecord.Step_Start_Time__c.day());
            onBoardingSttausTrackObj.Start_Date__c = startStatusDate ;
            
            if(SingleOnboardingstepRecord.Status__C=='Completed'){
                Date endStatusDateForComplete=date.newinstance(SingleOnboardingstepRecord.Date_Ended__c.year(), SingleOnboardingstepRecord.Date_Ended__c.month(), SingleOnboardingstepRecord.Date_Ended__c.day());
                onBoardingSttausTrackObj.End_Date__c =endStatusDateForComplete;
            }
            
            else{
                Date endStatusDate=date.newinstance(System.now().year(), System.now().month(), System.now().day());
                onBoardingSttausTrackObj.End_Date__c =endStatusDate;
            }
            
            system.debug('Debug1'+SingleOnboardingstepRecord.Step_Start_Time__c);
            system.debug('Debug1'+System.now());
           // system.debug('Debug1'+BusinessHours.diff(nbnBusinessHours.id,SingleOnboardingstepRecord.Step_Start_Time__c,System.now()));
          //  long BusinessHourspent = BusinessHours.diff(nbnBusinessHours.id,SingleOnboardingstepRecord.Step_Start_Time__c,System.now());
          //  onBoardingSttausTrackObj.Number_of_Business_Hours_Elapsed__c = BusinessHourspent/(1000*60*60);
          //  onBoardingSttausTrackObj.Number_of_Business_Days_Elapsed__c =  BusinessHourspent/(1000*60*60)/8 ;
           // onBoardingSttausTrackObj.Number_of_Business_Hours_Elapsed_Backup__c = BusinessHourspent/(1000*60*60) ;
            
            OnBoardingStepTrackstobeinserted.add(onBoardingSttausTrackObj);
            //insert onBoardingSttausTrackObj;
            
            /*}else {
               system.debug('ABCDE'+ nbnBusinessHours.id);
               system.debug('ABCDE'+ SingleOnboardingstepRecord.Step_Start_Time__c);
               system.debug('ABCDE'+ System.now());
               //system.debug('ABCDE'+ BusinessHours.diff(nbnBusinessHours.id,SingleOnboardingstepRecord.Step_Start_Time__c,System.now()) + ExistingRecord.Number_of_Business_Hours_Elapsed_Backup__c );
            ExistingRecord.Start_Date__c = SingleOnboardingstepRecord.Step_Start_Time__c;
            ExistingRecord.End_Date__c = System.now();
           // long BusinessHourspentrepeat = BusinessHours.diff(nbnBusinessHours.id,SingleOnboardingstepRecord.Step_Start_Time__c,System.now());
           // ExistingRecord.Number_of_Business_Hours_Elapsed__c = BusinessHourspentrepeat/(1000*60*60)+ ExistingRecord.Number_of_Business_Hours_Elapsed_Backup__c;
           // ExistingRecord.Number_of_Business_Days_Elapsed__c = ((BusinessHourspentrepeat/(1000*60*60))+ExistingRecord.Number_of_Business_Hours_Elapsed_Backup__c)/8 ;
           // ExistingRecord.Number_of_Business_Hours_Elapsed_Backup__c = BusinessHourspentrepeat/(1000*60*60)+ ExistingRecord.Number_of_Business_Hours_Elapsed_Backup__c ;
           
            OnBoardingStepTrackstobeUpdated.add(ExistingRecord);
           // Update ExistingRecord;
            
            }*/
            if(SingleOnboardingstepRecord.Status__C!='Completed'){
                SingleOnboardingstepRecord.Step_Start_Time__c = system.now();
            }
            

            
            }
         }
         
          system.debug('****OnBoardingStepTrackstobeUpdated****'+OnBoardingStepTrackstobeUpdated);
         system.debug('****OnBoardingStepTrackstobeinserted****'+OnBoardingStepTrackstobeinserted);
         if(OnBoardingStepTrackstobeinserted.size()>0){
            
            Insert OnBoardingStepTrackstobeinserted ;
           
         }else if(OnBoardingStepTrackstobeUpdated.size()>0){
         
            Update OnBoardingStepTrackstobeUpdated ;
            
            }
    }
   catch(Exception excp){
       system.debug('Exception occured while inserting steps'+excp.getMessage()+excp.getStacktraceString());
   }    

     
        
   }

}