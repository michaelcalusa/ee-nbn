@isTest
private class OnBoardingStepTriggerHandler_Test{

/*------------------------------------------------------------------------
Author:        Santosh
Company:       Wipro Technologies 
Description:   Test class for OnBoardingStepTriggerHandler Class
                  1.Create Test Method to Update the OnboardingStep to multiple Steps to track the Changes. 
               
Class:           OnBoardingStepTriggerHandler
History
<Date>            <Authors Name>    <Brief Description of Change> 

--------------------------------------------------------------------------*/  

    static List<Account> ListOfAccounts = new List<Account> ();
    static List<Onboarding_Process__c> ListOfOnboardingProcess = new List<Onboarding_Process__c> ();
    static List<On_Boarding_Step__c> ListOfOnboardingStep = new List<On_Boarding_Step__c> ();
    static void getRecords(){
        // Create Account
        ListOfAccounts = TestDataUtility.getListOfAccounts(1,true);
        System.assert(ListOfAccounts.get(0).Id != null, 'Account Record not created');
        system.debug('****Account*****');
        // Create Onboarding Process
        ListOfOnboardingProcess = TestDataUtility.getListOfOnboardingProcess(1,true,ListOfAccounts.get(0).id);
        system.debug('****ListOfOnboardingProcess*****'+ListOfOnboardingProcess);
        System.assert(ListOfOnboardingProcess.get(0).Id != null, 'Onoarding Process Record not created');
        // Query Onboarding Step records which got created by Process Builder
        ListOfOnboardingStep = [select id, Start_Date__c from On_Boarding_Step__c where Related_Product__c IN: ListOfOnboardingProcess];
        system.debug('****ListOfOnboardingStep*****'+ListOfOnboardingStep);
        // Update Start_Date__c field as Today for the Onboarding Step records
        for(On_Boarding_Step__c OnBoardingStep : ListOfOnboardingStep){
        system.debug('**OnBoardingStep*'+OnBoardingStep);
            OnBoardingStep.Start_Date__c = date.today()-3;
           // OnBoardingStep.Status__c = 'New';
        }
       // update ListOfOnboardingStep;
        //System.assertEquals(ListOfOnboardingStep.get(0).Start_Date__c, date.today()-3);
    }
    
 
 /***
        Test Tracking Business Hours 
    ***/
  
    static testmethod void valdiatestatusTrackingRecords(){
        getRecords();
        
        List<On_Boarding_Step__c> onboardingStepsList=[select id,Related_Product__c, Start_Date__c,Status__c from On_Boarding_Step__c where Status__c='New'  and Start_Date__c=null limit 1];
        system.debug('*****onboardingStepsList**'+onboardingStepsList);
        On_Boarding_Step__c newonboardingstep= new On_Boarding_Step__c(Related_Product__c=onboardingStepsList[0].Related_Product__c,Status__c='New',Start_Date__c=null);
        Test.startTest();
        List<On_Boarding_Step__c> onboardingStepsList3=[select id, Start_Date__c,Status__c from On_Boarding_Step__c where id =: onboardingStepsList[0].Id limit 1];
        onboardingStepsList3[0].Status__c = 'in Analysis';
        update onboardingStepsList3;
        List<On_Boarding_Step__c> onboardingStepsList1=[select id, Start_Date__c,Status__c from On_Boarding_Step__c where id =: onboardingStepsList[0].Id limit 1];
        onboardingStepsList1[0].Start_Date__c=date.today()-3;
        update onboardingStepsList1;
        List<On_Boarding_Step__c> onboardingStepsList2=[select id, Start_Date__c,Status__c from On_Boarding_Step__c where id =: onboardingStepsList[0].Id limit 1];
        onboardingStepsList2[0].Status__c = 'Completed';
        onboardingStepsList2[0].Date_Ended__c=system.today()-1;
       // onboardingStepsList2[0].Start_Date__c=system.today();
        update onboardingStepsList2;

        Test.stopTest();
    }

}