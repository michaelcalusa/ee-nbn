/*
	ClassName: OnboardingStepMassUpdateController
	Developer: Wayne Ni
	Date: 16th July 2018
	Purpose: This controller is used for mass updating product on boarding steps
*/

public class OnboardingStepMassUpdateController {
    
    public ApexPages.StandardSetController setSteps;
    public Boolean getFlagone {get;set;}
    public Boolean getFlagtwo {get;set;}
    
    public OnboardingStepMassUpdateController(ApexPages.StandardSetController  stdController) {
        setSteps = stdController;
    }
    
    public pagereference InvokeOnBoardingUpdateFunction(string inputstatus){
        // do something with the selected records
        //system.debug('referer is '+ApexPages.currentPage().getHeaders().get('Referer'));
        //system.debug(ApexPages.currentPage().getParameters().get('retURL'));
        Set<Id> onboaringstepId = new set<Id>();
        PageReference ref = new PageReference('');
        string url;
        
        //system.debug(setSteps.getSelected());
        
        for ( On_Boarding_Step__c step : (On_Boarding_Step__c[])setSteps.getSelected() ){
            System.debug('Step id = ' + step.id);
            onboaringstepId.add(step.id);
        }
        
        List<On_Boarding_Step__c> updateList = new List<On_Boarding_Step__c>();
        List<On_Boarding_Step__c> onboardingSteps = [select id,Status__c, Related_Product__r.id from On_Boarding_Step__c where id in:onboaringstepId];
        
        if(onboardingSteps.size() > 0){
            //statusSetup(onboardingSteps,inputstatus);
            
            List<On_Boarding_Step__c> FinalupdateList = new  List<On_Boarding_Step__c>();
            
            for(On_Boarding_Step__c singleStep:onboardingSteps){
                singleStep.status__c = inputstatus;
                if(inputstatus == 'Completed'){
                    singleStep.Date_Ended__c = system.now(); 
                    singleStep.Start_Date__c = system.today(); 
                }else{
                    singleStep.Date_Ended__c = null; 
                    singleStep.Start_Date__c = null; 
                }
                
                FinalupdateList.add(singleStep);
            }
            
            try{
		Database.SaveResult[] srList = Database.update(FinalupdateList, false);
                for(Database.SaveResult sr : srList){
                    if (sr.isSuccess() == false) {
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('fields that affected this error: ' + err.getFields());
                            GlobalUtility.logMessage('Error','Onboarding Step mass update controller',err.getMessage(),'', '','','',null,0);
                        }                        
                    }
                }
                system.debug(userinfo.getUiTheme());
                system.debug(userinfo.getUiThemeDisplayed());
                if(userinfo.getUiThemeDisplayed() == 'Theme4d' || userinfo.getUiThemeDisplayed() == 'Theme4t'){
                    url = '/lightning/r/'+onboardingSteps[0].Related_Product__r.id+'/related/Product_On_Boarding_Steps__r/view';
                }else if(userinfo.getUiThemeDisplayed() == 'Theme3' || userinfo.getUiThemeDisplayed() == 'Theme2'){
                    url = '/'+onboardingSteps[0].Related_Product__r.id;
                }
                ref = new PageReference(url);
                ref.setRedirect(true);
             

            }catch(Exception e){
                //system.debug('the error exception is '+e);
                getFlagone = false;
                getFlagtwo = true;
                //onboardingSteps[0].addError('Cannot be reset to previous status');
                GlobalUtility.logMessage('Error','Onboarding Step mass update controller','Oboarding step mass update for this on boarding prcress '+FinalupdateList[0].related_Product__r.id,'','','','',e,0);
            	ref = null;
            }
            
        }else{
            //ref = setSteps.cancel();
            getFlagone = true;
            getFlagtwo = false;
            ref = null;
        }
        return ref;
    }
    
    public PageReference SetInProgress(){
        return InvokeOnBoardingUpdateFunction('In progress');
    } 
    
    public PageReference SetNotRequired(){
        return InvokeOnBoardingUpdateFunction('Not Required');
    } 
    
    public PageReference SetCompleted(){
        return InvokeOnBoardingUpdateFunction('Completed');
    }
    
    public pagereference RedirectToHome(){
        return setSteps.cancel();
    }
    
}