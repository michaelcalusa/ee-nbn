/*
	ClassName: OnboardingStepMassUpdateController_test
	Developer: Wayne Ni
	Date: 16th July 2018
	Purpose: This controller is used for testing controller of mass updating product on boarding steps
*/

@IsTest
public class OnboardingStepMassUpdateController_test {
    //public static string customerAccountRecordTypeId = schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
        
    @testSetup static void setup() {
        string customerAccountRecordTypeId = schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
		//Create test customer Account
        Account OnboardingCustomerTestAccount = new Account();
        OnboardingCustomerTestAccount.recordtypeid = customerAccountRecordTypeId;
        OnboardingCustomerTestAccount.Name = 'Onboarding mass update test customer Account nwglfls';
        OnboardingCustomerTestAccount.Type__c = 'Wholesaler';
        OnboardingCustomerTestAccount.Tier__c = '1 ';
        OnboardingCustomerTestAccount.Status__c = 'Active';
        insert OnboardingCustomerTestAccount;
        
        //create a onboarding process test data
        Onboarding_Process__c testOnboardingProcess = new Onboarding_Process__c();
        testOnboardingProcess.Onboarding_Product__c  = 'Facilities access';
        testOnboardingProcess.Account_ProdOnB__c = OnboardingCustomerTestAccount.Id;
        testOnboardingProcess.Start_Date__c = system.Date.today();
        insert testOnboardingProcess;        
        
                
    }
    
    @isTest static void testMethod1(){
        system.debug('test before query test cusotomer account');
        Onboarding_Process__c QuerytestOnboardingprocess = new Onboarding_Process__c();
        List<Account> testOnboardingCustomerAcc = [select id from Account where name = 'Onboarding mass update test customer Account nwglfls'];
        system.debug('test before querying the onboaridng process ' +testOnboardingCustomerAcc.size());
        if(testOnboardingCustomerAcc.size() > 0){
            QuerytestOnboardingprocess = [select id from Onboarding_Process__c where Account_ProdOnB__c =: testOnboardingCustomerAcc[0].Id];  
        }        
        system.debug('test QuerytestOnboardingprocess ');
        List<On_Boarding_Step__c> testOnboardingStep = [select id,name,Status__c, Related_Product__r.id from On_Boarding_Step__c where Related_Product__r.id =: QuerytestOnboardingprocess.Id];
        
        ApexPages.StandardSetController teststdController = new ApexPages.StandardSetController(testOnboardingStep);
        OnboardingStepMassUpdateController MassUpdateController = new OnboardingStepMassUpdateController(teststdController);
        MassUpdateController.SetInProgress();
        system.debug('test set in progress method output is '+MassUpdateController.SetInProgress());
        system.assertEquals(null, MassUpdateController.SetInProgress());
        
        //Scenario 2 testing
        ApexPages.StandardSetController teststdControllerScenarioTwo = new ApexPages.StandardSetController(testOnboardingStep);        
        
        //define selected records
        List<On_Boarding_Step__c> selectedRecords = new List<On_Boarding_Step__c>();
        for(On_Boarding_Step__c singleStep : testOnboardingStep){
            selectedRecords.add(singleStep);
        }
        teststdControllerScenarioTwo.setSelected(selectedRecords);
        OnboardingStepMassUpdateController MassUpdateControllerScenarioTwo = new OnboardingStepMassUpdateController(teststdControllerScenarioTwo);
        
        MassUpdateControllerScenarioTwo.SetInProgress();
        MassUpdateControllerScenarioTwo.SetCompleted();
        MassUpdateControllerScenarioTwo.SetNotRequired();
        MassUpdateControllerScenarioTwo.RedirectToHome();
        
        string expectedurl = '/lightning/r/'+selectedRecords[0].Related_Product__r.id+'/related/Product_On_Boarding_Steps__r/view';
        PageReference resultPg = new PageReference(expectedurl);
        system.debug('test set scenario two in progress method output is '+MassUpdateControllerScenarioTwo.SetInProgress());
        system.assertNotEquals(resultPg, MassUpdateControllerScenarioTwo.SetInProgress());
        
    }

    @isTest static void testRunAs(){    
        
        //create test user with profile NBN Account Management
        String uniqueUserName = 'AccountManagementTestUser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='NBN Account Management'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='TestingAccountManagementUser', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName);
        //insert u;
        
        Onboarding_Process__c QuerytestOnboardingprocess = new Onboarding_Process__c();
        List<Account> testOnboardingCustomerAcc = [select id from Account where name = 'Onboarding mass update test customer Account nwglfls'];
        system.debug('test before querying the onboaridng process ' +testOnboardingCustomerAcc.size());
        if(testOnboardingCustomerAcc.size() > 0){
            QuerytestOnboardingprocess = [select id from Onboarding_Process__c where Account_ProdOnB__c =: testOnboardingCustomerAcc[0].Id limit 1];  
        }        
        system.debug('test QuerytestOnboardingprocess ');
        List<On_Boarding_Step__c> testOnboardingStepThree = [select id,name,Status__c, Related_Product__r.id from On_Boarding_Step__c where Related_Product__r.id =: QuerytestOnboardingprocess.Id];           
        system.debug('test onboarding step in scenario three is '+testOnboardingStepThree);
        
        //Scenario 3 testing
        ApexPages.StandardSetController teststdControllerScenarioThree = new ApexPages.StandardSetController(testOnboardingStepThree);           
        
        //define selected records
        List<On_Boarding_Step__c> selectedRecords = new List<On_Boarding_Step__c>();
        for(On_Boarding_Step__c singleStep : testOnboardingStepThree){
            selectedRecords.add(singleStep);
        }
        system.debug('test scenario 3 on boarding steps are '+selectedRecords);
        
        teststdControllerScenarioThree.setSelected(testOnboardingStepThree);
        system.debug('test scenario 3 selected records are '+teststdControllerScenarioThree.getSelected());
        OnboardingStepMassUpdateController MassUpdateControllerScenarioThree = new OnboardingStepMassUpdateController(teststdControllerScenarioThree);
                
        System.runAs(u) {            
            MassUpdateControllerScenarioThree.SetCompleted();
            MassUpdateControllerScenarioThree.SetInProgress();
            system.assertEquals(null, MassUpdateControllerScenarioThree.SetInProgress());            
        }
        
        
    }
}