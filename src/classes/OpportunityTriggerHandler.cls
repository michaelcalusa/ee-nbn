/************************************************************************************************
- Modified by: Gnana 
- Story: NPSLCST-33775
- Modified Date: 23/04/2018
- Description: Modified the logic to assign Account Address when the new Opportunity created without
                any Account Address
/************************************************************************************************
- Modified by: Gnana 
- Story: MSEU-9894
- Modified Date: 26/02/2018
- Description: Have added logic to populate Account, Contract(Account Address), 
               Mode of Payment while creating Opportunity based on Stage Application
**************************************************************************************************
- Developed by: Ravindran Shanmugam
- Date Created: 04/10/2017 (dd/MM/yyyy)
- Description: Triggerhandler for opportunity object.
- Version History:
- v1.0 - 04/10/2017, RS: Created
- v1.1 - 12/02/2018, RS: modified the code to exclude "CreditMemo" invoices for "payment received validation" - US2634
**************************************************************************************************
- Modified by: Swetha Kodatham 
- Story: MSEU-10558
- Description: The Business Segment E&G method "createScenarioTasks" is added to create Tasks on the Opportunity 
  based on the Scenario selected on the record.
- 03/10/2018, Commented "createScenarioTasks", "validateEnGOppStage", "validateRelatedTaskStatus"  methods as per 
  the User story MSEU-17956
**************************************************************************************************
- Modified by: Mohith Ramachandraiah 
- Story: MSEU-10599
- Description: The Business Segment E&G private method <validateRelatedTaskStatus> is to give an error message 
                 on opportunity stageName(Phase) from 'proposal submitted' to 'Win' when the status of corresponding tasks is NOT 
                 marked as 'Completed' or 'Not required'.
**************************************************************************************************
-�Modified�by:�Jairaj�Jadhav
-�Story:�MSEU-13512
-�Modified�Date:�06/09/2018
-�Description:�The�method�apexSharingForICTOpp�which�is�being�called�by�"after�insert"�event�of�Opportunity�trigger,�creates�an�apex�based�sharing�record�to�share
���������������the�opportunity�od�Business�Segment�ICT�recordtype�with�the�public�group�of�Partner�Account�users�to�which�Opportunity�owner�belongs�to.
-�v1.0�-�06/09/2018,�Created
**************************************************************************************************
*/

public class OpportunityTriggerHandler extends TriggerHandler {
    /*Constructor*/
    public OpportunityTriggerHandler() {
        if(Test.isRunningTest()){
            this.setMaxLoopCount(20);
        }
        else{
            this.setMaxLoopCount(2);
        }
        
        System.Debug( 'NBN: -> OpportunityTriggerHandler Invoked' );
    }
    /***************************************************************************************
    * @Description : Trigger handlers for events
    ****************************************************************************************/
    
    public override void beforeInsert(List<sobject> newItems){
        /*Gnana - Added below code as part of MSEU-9894*/
        RecordType newDevSCO = [SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'New_Development_SCO' LIMIT 1];
        List<Opportunity> opptyList = new List<Opportunity>();
        for(SObject sObj : newItems){  
            Opportunity opp = (Opportunity) sObj;
            if(opp.RecordTypeId == newDevSCO.Id){
                opptyList.add(opp);                
            }            
        }
        updateAccAddress(opptyList);
        /*Gnana - Added above code as part of MSEU-9894*/
    }
    
    /*Gnana - Added below code as part of MSEU-9894*/
    public static void updateAccAddress(List<Opportunity> newItems) {        
        try{
            Id accAddrRTId = [SELECT Id FROM RecordType WHERE SobjectType = 'Contract' AND DeveloperName = 'NewDevs_Class_3_4' LIMIT 1].Id;
            List<Id> listOfStgIds = new List<Id>();
            List<Id> opptyIds = new List<Id>();
            for(Opportunity opp : newItems){                  
                    opptyIds.add(opp.Id);                
                    listOfStgIds.add(opp.Stage_Application__c);           
            }
            List<Opportunity> listOfOppty = [SELECT Id, Stage_Application__r.Account__c, Stage_Application__c FROM Opportunity WHERE Id IN :opptyIds];
            List<Stage_Application__c> stageAppList = [SELECT Id, Account__r.Id, (SELECT Id,Stage_Application__r.Id,Stage_Application__r.Account__r.Id,Contact__r.Id,Roles__c FROM Stage_Application_Contacts__r WHERE Roles__c INCLUDES ('Billing Contact','Account Payable Contact')) FROM Stage_Application__c WHERE Id IN :listOfStgIds];
            Map<id,List<Stage_Application__c>> AccToStageAppMap = new Map<id,List<Stage_Application__c>>();
            Map<Id,Map<String,List<Id>>> OpptyTOStageContactMap = new Map<Id,Map<String,List<Id>>>();
            Map<Id,List<Contract>> accTOContractMap = new Map<Id,List<Contract>>();
            Map<Id,Id> stgToAccMap = new Map<Id,Id>();
            for(Stage_Application__c stageApp :stageAppList){
                Map<String,List<Id>> stgConInnerMap = new Map<String,List<Id>>();
                for(StageApplication_Contact__c sc : stageApp.Stage_Application_Contacts__r){  
                    List<Id> conTempList = new List<Id>();
                    if(stgConInnerMap.ContainsKey(sc.Roles__c)){
                        conTempList = stgConInnerMap.get(sc.Roles__c);
                    }
                    conTempList.add(sc.Contact__r.Id);
                    stgConInnerMap.put(sc.Roles__c,conTempList);
                }
                OpptyTOStageContactMap.put(stageApp.id,stgConInnerMap);
                List<Stage_Application__c> stageAppListTemp = new List<Stage_Application__c>();
                if(AccToStageAppMap.containsKey(stageApp.Account__r.Id)){
                    stageAppListTemp = AccToStageAppMap.get(stageApp.Account__r.Id);
                }
                stageAppListTemp.add(stageApp);
                AccToStageAppMap.put(stageApp.Account__r.Id,stageAppListTemp);
                stgToAccMap.put(stageApp.Id, stageApp.Account__c);
            }
            
            for(Contract contr :[SELECT Id, AccountId,Billing_Contact__c,Dunning_Contact__c FROM Contract WHERE AccountId IN :AccToStageAppMap.keySet() AND RecordTypeId =: accAddrRTId]){
                List<Contract> contrTemp = new List<Contract>();
                if(accTOContractMap.containsKey(contr.AccountId)){
                    contrTemp = accTOContractMap.get(contr.AccountId);
                }
                contrTemp.add(contr);
                accTOContractMap.put(contr.AccountId,contrTemp);
            }
            
            List<Opportunity> listOfOpptyToUpd = new List<Opportunity>();
            for(Opportunity opp :newItems){
                if(String.isBlank(opp.Mode_of_Payment__c)){
                    opp.Mode_of_Payment__c = 'Invoice';                    
                }
                List<Contract> accAddrList = accTOContractMap.get(stgToAccMap.get(opp.Stage_Application__c));
                opp.AccountId = stgToAccMap.get(opp.Stage_Application__c);                
                Map<String,List<Id>> contactRoleIds = OpptyTOStageContactMap.get(opp.Stage_Application__c);
                if(accAddrList.size() > 1){
                    for(Contract accAddr :accAddrList){
                        for(String str :contactRoleIds.keySet()){
                            if(str.contains('Billing Contact')){
                                for(Id conId :contactRoleIds.get(str)){
                                    accAddr.Billing_Contact__c = conId;
                                }
                            }
                            if(str.contains('Account Payable Contact')){
                                for(Id conId :contactRoleIds.get(str)){
                                    accAddr.Dunning_Contact__c = conId;
                                }
                            }
                            if(opp.ContractId == null){ /*Gnana - Added if condition as part of NPSLCST-33775*/
                                opp.ContractId = accAddr .Id;    
                            }                            
                        }
                    }                    
                }
                else if(accAddrList.size() == 1){
                    if(opp.ContractId == null){/*Gnana - Added if condition as part of NPSLCST-33775*/
                        opp.ContractId = accAddrList[0].Id ;    
                    }                    
                }                
                listOfOpptyToUpd.add(opp);
            }
            system.debug('SUCCESSSSSSSSSSSSSSSSSSSSSSSSS' + listOfOpptyToUpd); 
            newItems = listOfOpptyToUpd;
        }
        catch(exception ex){
            system.debug('message '+ex.getMessage()+' at line '+ex.getStackTraceString());
        }
                                
    }

    /*
    Added by Mohith as a part of MSEU-10599
    Description: The following private method is to give an error message 
                 on opportunity stageName(Phase) from 'proposal submitted'
                 to 'Win' when the status of corresponding tasks is NOT 
                 marked as 'Completed' or 'Not required'.
    */
    /* Commented out as per the User story MSEU - 17956 */
    /*private void validateRelatedTaskStatus(Map<Id, Opportunity> oldItemsMap, Map<Id, Opportunity> newItemsMap){
        Set<Id> OppIds = new Set <Id>();
        ID oppRecTypeId = schema.sobjecttype.Opportunity.getrecordtypeinfosbyname().get('Business Segment E&G').getRecordTypeId();    
        ID taskRecTypeId = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Business Segment E&G').getRecordTypeId();
         
         // Store all oppty records with record type name 'Business Segment E&G' in a Set / 
        for (Opportunity opp:  newItemsMap.values()) {
            if((opp.RecordTypeId == oppRecTypeId) && ((opp.StageName!= oldItemsMap.get(opp.Id).StageName) && opp.StageName == 'Win') ){
                OppIds.add(opp.Id);
            }
        }
        
        //Retrieve the aggregate result for the IDs that we need to throw the error for 
        for(AggregateResult a: [Select Count(Id), WhatId from Task where WhatId IN:OppIds and RecordTypeId =: taskRecTypeId and Type ='Sub Task' and ((Status != 'Completed') and (Status != 'Not required') ) Group by WhatId])
        {
            //id some = (Id)a.get('WhatId');
            if(a.get('expr0') != 0){
                newItemsMap.get((Id)a.get('WhatId')).addError('Please complete all Open tasks or Mark tasks as Not Required', false);
            }
        }
    }*/
    /* The Selected Scenario should be set to null when the Oppty Phase <> Proposal Submitted/Win/Closed for E&G */
    /* Commented out as per the User story MSEU - 17956 */
    /*private void validateEnGOppStage(Map<Id, Opportunity> oldItemsMap, Map<Id, Opportunity> newItemsMap){
        ID oppRecTypeId = schema.sobjecttype.Opportunity.getrecordtypeinfosbyname().get('Business Segment E&G').getRecordTypeId();  
        
        for(Opportunity opp: newItemsMap.values()){*/
            /* Prompt the User to choose a value on Scneario Select field if the Phase is "Proposal submitted" */
            /*if(opp.RecordTypeId == oppRecTypeId && ((opp.StageName != oldItemsMap.get(opp.Id).StageName) && (opp.StageName == 'Proposal submitted')) 
               && (opp.ScenarioSelect__c ==null)){
                   opp.adderror('Please select a value for Scenario Select field',false);
               }*/
            /* Prompt the User to select the Phase as "Proposal submitted" if the Scenario Select is not null */
            /*else if(opp.RecordTypeId == oppRecTypeId && (oldItemsMap.get(opp.Id).isClosed == false && oldItemsMap.get(opp.Id).StageName!= 'Proposal submitted' && opp.StageName != 'Proposal submitted') 
                    && (opp.ScenarioSelect__c !=null)){
                        opp.adderror('Please select Proposal Submitted on Phase field',false);
                    }*/
            /* Clear the Scenario Select value if the Stage is moved from  ClosedLost/ClosedWin/ProposalSubmitted to any other stage */
            /*else if(opp.RecordTypeId == oppRecTypeId && ((opp.StageName != oldItemsMap.get(opp.Id).StageName) && ((opp.StageName != 'Proposal submitted') && (opp.isClosed == false))) 
                    && (opp.ScenarioSelect__c !=null)){
                        opp.ScenarioSelect__c = null;
                    }
        }
    } */
    /*Mohith - Added above code as part of MSEU-10599*/
    
    /***************************************************************************************
    * @Description : Trigger handlers for events
    ****************************************************************************************/
    public  override void beforeUpdate(Map<Id, SObject> oldItemsMap, Map<Id, SObject> newItemsMap) { 
        /* Commented out as per the User story MSEU - 17956 *
        validateEnGOppStage((Map<Id, Opportunity>)oldItemsMap, (Map<Id, Opportunity>)newItemsMap);
        validateRelatedTaskStatus((Map<Id, Opportunity>)oldItemsMap,(Map<Id, Opportunity>)newItemsMap);  */   
        
        Set<Id> OppIds = newItemsMap.keyset();
        RecordType damageRecType = [SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Recoverable_Damage' LIMIT 1];
        RecordType rwApplicationFeeRTID = [SELECT DeveloperName, Id, SobjectType
                                       FROM RecordType
                                       WHERE SobjectType = 'Opportunity'
                                       AND DeveloperName = 'RW_Application_Fee' LIMIT 1];
        //List<Opportunity> oppList = [SELECT Id, Name, AccountId,ContractId, RecordTypeId, StageName FROM Opportunity];
        
        Map <Id, Opportunity> validateList = new Map<Id, Opportunity> ([SELECT Id,ContractId,Account.Customer_Type__c,Account.Registered_Entity_name__c,
                            Account.Name, AccountId, Account.ABN__C, Account.ACN__c, Contract.Billing_Contact__c,
                            Contract.Billing_Contact__r.FirstName,Contract.Billing_Contact__r.LastName,
                            Contract.Billing_Contact__r.Email, Contract.Billing_Contact__r.Phone,
                            Contract.Billing_Contact__r.MobilePhone, Owner.Name,Billing_Contact_Id__c,
                            Contract.BillingStreet,Contract.BillingCity,Contract.BillingPostalCode,Contract.BillingState,
                            Contract.Dunning_Contact__c,Contract.Dunning_Contact__r.FirstName,
                            Contract.Dunning_Contact__r.Phone,Contract.Dunning_Contact__r.MobilePhone,
                            RecordType.Name
                            FROM Opportunity where Id in :OppIds]);

        List<cscfga__Product_Basket__c> prodBasketList = 
            [SELECT csbb__Synchronised_With_Opportunity__c,
                    cscfga__Basket_Status__c,
                    cscfga__Opportunity__c,
                    cscfga__Products_Id_Qty_In_Basket__c,
                    cscfga__Products_In_Basket__c, Total_Cost__c 
            FROM cscfga__Product_Basket__c 
            WHERE cscfga__Opportunity__c in : OppIds];

        //v1.1 changes starts  
        List<Invoice__c> InvoiceList = 
            [ SELECT Opportunity__c,Payment_Status__c, RecordTypeId FROM Invoice__c WHERE Opportunity__c in :OppIds];    
        List<RecordType> InvoiceRecordTypeList = 
        [ SELECT DeveloperName,Id FROM RecordType WHERE SobjectType = 'Invoice__c' ];
        Id CreditMemoRecordTypeId; //v1.1 changes
        Id InvoiceRecordTypeId;  //v1.1 changes
    set<Id> rwAppFeeOptyIds = new set<Id>();
        String MissingField ='';
        for (RecordType recType: InvoiceRecordTypeList) {
            if (recType.DeveloperName == 'Credit_Memo') {
                CreditMemoRecordTypeId = recType.Id;
            } else if(recType.DeveloperName == 'Invoice') {
                InvoiceRecordTypeId =  recType.Id;
            }
        }
        // v1.1 changes ends here            
        List<Opportunity> newOppList = newItemsMap.values();
        String errorMessage = '';
        for (SObject sObj:  newOppList) {
            Opportunity NewOpp = (Opportunity) sObj;
            Opportunity oldOppty = (Opportunity)oldItemsMap.get(sObj.Id);
            Opportunity oldOppId = (Opportunity) oldItemsMap.get(NewOpp.Id);
            if (NewOpp.RecordTypeId == damageRecType.Id) {
                Opportunity oldOpp = (Opportunity) oldItemsMap.get(NewOpp.Id);
                Opportunity opp = (Opportunity) validateList.get(NewOpp.Id);
                
                System.debug('>>>>> Opportunity Stage updated >>> from:'+oldOpp.StageName+' To:'+NewOpp.StageName);
                if (NewOpp.StageName != oldOpp.StageName && NewOpp.StageName == 'Payment Received') {
                    // Invoice payment status validation (Should be 'Fully paid')
                    Boolean atleastOneInvoiceFullyPaid = false;
                    for(Invoice__c invoice : InvoiceList) {
                        //v1.1 changes - to exclude credit memo records
                        if (invoice.Opportunity__c == NewOpp.Id  && invoice.RecordTypeId == InvoiceRecordTypeId) {
                            if (invoice.Payment_Status__c != 'Fully Paid' && invoice.Payment_Status__c != 'Cancelled') {
                                errorMessage = errorMessage + 'Invoice payment status should be "Fully Paid"<br />';
                                atleastOneInvoiceFullyPaid = true;
                                break;
                            }
                            if (invoice.Payment_Status__c == 'Fully Paid')
                                atleastOneInvoiceFullyPaid = true;       
                        }
                    }
                    if (!atleastOneInvoiceFullyPaid) {
                        errorMessage = errorMessage + 'Invalid phase - Atleast one Invoice should be fully paid"<br />';
                    }
                }
                if (NewOpp.StageName != oldOpp.StageName && NewOpp.StageName == 'Invoicing') {
                    
                    if (NewOpp.Has_Active_Damage_Invoice__c == true)
                        errorMessage = errorMessage + 'Opportunity already has an active Invoice<br />';

                    if (prodBasketList.isEmpty()) {
                        errorMessage = errorMessage + 'Product Basket needs to be populated for Invoicing<br />';
                    }
                    // Product basket validation 
                    for(cscfga__Product_Basket__c prodBasket : prodBasketList) {
                        if (prodBasket.cscfga__Opportunity__c == NewOpp.Id) {
                            if (prodBasket.csbb__Synchronised_With_Opportunity__c == false) {
                                errorMessage = errorMessage + 'Product Basket needs to be synced with the Opportunity<br />';
                                break;
                            }
                            if (prodBasket.Total_Cost__c == null) {
                                errorMessage = errorMessage + 'Product Basket amount cannot be 0<br />';
                                break;
                            }
                        }
                    }

                    //AC-2768
                    if(opp.Account.ABN__c != null && opp.Account.ABN__c !='' && opp.AccountId !=null)
                    {
                        if((String.valueOf(opp.Account.Name)).length() > 240)
                        {
                            errorMessage = errorMessage + 'Opportunity Account with ABN must have 240 or less characters<br />';
                        }    
                        
                    }
                    else {
                        if(opp.AccountId !=null && (String.valueOf(opp.Account.Name)).length() > 150)
                        {
                            errorMessage = errorMessage + 'Opportunity Account without ABN must have 150 or less characters<br />';
                        }
                    }

                    if(opp.AccountId ==null ) {
                            errorMessage = errorMessage + 'Account details are required for Invoice creation<br />';
                    } else {
                        if(opp.Account.Customer_Type__c == null || opp.Account.Customer_Type__c == '') {
                            errorMessage = errorMessage + 'Opportunity Account Customer Type is null<br />';
                        }
                        else {
                            if(opp.Account.Customer_Type__c == 'Organisation' && (opp.Account.ABN__c == null || opp.Account.ABN__c == ''))
                                errorMessage = errorMessage + 'Opportunity Account ABN is null<br />';
                        }
                    }                    
                    if(opp.ContractId == null) {
                        errorMessage = errorMessage + 'Account Address is required for Invoice creation<br />';
                    } 
                    else {
                        if(opp.Contract.BillingStreet == null || opp.Contract.BillingStreet == '')
                            errorMessage = errorMessage + 'Billing Street/Address 1 details missing in Account Address<br />';
                        if(opp.Contract.BillingCity == null || opp.Contract.BillingCity == '')
                            errorMessage = errorMessage + 'Billing City details missing in Account Address<br />';
                        if(opp.Contract.BillingState == null || opp.Contract.BillingState == '')
                            errorMessage = errorMessage + 'Billing State details missing in Account Address<br />';
                        if(opp.Contract.BillingPostalCode == null || opp.Contract.BillingPostalCode == '')
                            errorMessage = errorMessage + 'Billing Postal Code details missing in Account Address<br />';
                        if(opp.Contract.Billing_Contact__c == null) {
                            errorMessage = errorMessage + 'Billing Contact required in Account Address<br />';
                        } 
                        else {
                            if(opp.Contract.Billing_Contact__r.FirstName == null)
                                errorMessage = errorMessage + 'Billing Contact First Name is null<br />';
                            if(opp.Contract.Billing_Contact__r.Email == null)
                                errorMessage = errorMessage + 'Billing Contact Email is null<br />';
                            if(opp.Contract.Billing_Contact__r.MobilePhone == null && opp.Contract.Billing_Contact__r.Phone == null)
                                errorMessage = errorMessage + 'At least one Phone number for Billing Contact is required.<br />';
                        }
                        if(opp.Contract.Dunning_Contact__c == null) {
                            errorMessage = errorMessage + 'Dunning Contact details is missing in Account Address<br />';
                        } 
                        else {
                            if(opp.Contract.Dunning_Contact__r.FirstName == null)
                                errorMessage = errorMessage + 'Dunning Contact First Name is null<br />';
                            if(opp.Contract.Dunning_Contact__r.MobilePhone == null && opp.Contract.Dunning_Contact__r.Phone == null)
                                errorMessage = errorMessage + 'At least one Phone number for Dunning Contact is required.<br />';
                        }
                    }
                }
                if (errorMessage != '')
                    Trigger.newmap.get(NewOpp.Id).addError('<span><br/>'+errorMessage+'</span>', false);
            }
            else if(NewOpp.RecordTypeId == rwApplicationFeeRTID.Id){
                if(NewOpp.StageName != oldOppId.StageName && NewOpp.StageName == 'Closed Won'){
                    rwAppFeeOptyIds.add(NewOpp.id); 
                }
            }
    }
        system.debug('rwAppFeeOptyIds--------'+rwAppFeeOptyIds);
        if(rwAppFeeOptyIds.size()>0){
           OpportunityUtility.checkMandatoryFieldsOnSave(rwAppFeeOptyIds);
           if(string.isNotBlank(OpportunityUtility.message)){
                MissingField = OpportunityUtility.message;
           }                
        }
        system.debug('MissingField =========='+MissingField );
        if(string.isNotBlank(Missingfield)){
            for (SObject sObj :  newOppList) {
                Opportunity NewOpp = (Opportunity) sObj;
                Trigger.newmap.get(NewOpp.Id).addError('<span>' + MissingField+ '</span>', false);
            }
        }
    }
    /***************************************************************************************
    * @Description : Trigger handlers for events
    ****************************************************************************************/
    public  override void beforeDelete(Map<Id, SObject> oldItemsMap) {
    }
    /***************************************************************************************
    * @Description : Method that send outbound Communication
    ****************************************************************************************/
    public  override void afterUpdate(Map<Id, SObject> oldItemsMap, Map<Id, SObject> newItemsMap) {
        /*Added below code as part of MSEU-10558 */
        /* Commented out as per the User story 17956 */
        /*createScenarioTasks((Map<Id, Opportunity>)oldItemsMap, (Map<Id, Opportunity>)newItemsMap);*/
    }
    /***************************************************************************************
    * @Description : Trigger handlers for events
    ****************************************************************************************/
    public  override void afterDelete(Map<Id, SObject> oldItemsMap) {
    }
    /***************************************************************************************
    * @Description : Trigger handlers for events
    ****************************************************************************************/
    public  override void afterUndelete(Map<Id, SObject> oldItemsMap) {
    }
    /***************************************************************************************
    * @Description : Trigger handlers for events
    ****************************************************************************************/
    public  override void afterInsert(Map<Id, SObject> newItemsMap) {
        EE_DisableTriggers__c cs = EE_DisableTriggers__c.getOrgDefaults();
        if(!(cs.DisableEEOpportunityTrigger__c))
        {
            EE_DataSharingUtility.processOpportunitySharingRules(Trigger.new);
        }
        apexSharingForICTOpp((Map<Id, Opportunity>) newItemsMap);
    }
    /***************************************************************************************
    * @Description : AfterUpdate Methods - MSEU - 10558 
    * Added by Swetha Kodatham
    ****************************************************************************************/
    /* Commented out as per the User story 17956 *
    public void createScenarioTasks(Map<Id, Opportunity> oldItemsMap, Map<Id, Opportunity> newItemsMap){
        List<Task> taskLst = new List<Task>();
        ID opptyRecTypeId = schema.sobjecttype.Opportunity.getrecordtypeinfosbyname().get('Business Segment E&G').getRecordTypeId();  
        ID taskRecTypeId = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Business Segment E&G').getRecordTypeId();
        List<Sub_Tasks__mdt> subTasksLst = [Select Id, Type__c, Sub_Task_Name__c,Task_Name__c, Related_Object_API_Name__c  
                                            from Sub_Tasks__mdt where Type__c = 'Scenario Sub Task' 
                                            and Related_Object_API_Name__c = 'Opportunity' Order by Task_Name__c];
   
        for(Opportunity o: newItemsMap.values()){
            if(o.RecordTypeId == opptyRecTypeId && ((o.StageName != oldItemsMap.get(o.Id).StageName) && (o.StageName == 'Proposal submitted')) ||
              ((o.StageName == 'Proposal submitted') && (o.ScenarioSelect__c != oldItemsMap.get(o.Id).ScenarioSelect__c))){
                for(Sub_Tasks__mdt t: subTasksLst){
                    if(o.ScenarioSelect__c == t.Task_Name__c){
                        Task taskRec = new Task(Subject = t.Task_Name__c+': '+t.Sub_Task_Name__c, Start_Date__c= date.today(), ownerId = o.OwnerId,
                                               RecordTypeId = taskRecTypeId, Status = 'Open', Type = 'Sub Task', WhatId = o.Id, Priority ='Normal') ;
                        taskLst.add(taskRec);
                    }
                }
            }
        }
        if(taskLst.Size()>0){
            insert taskLst;
        }
    } */
    /* Added by Swetha Kodatham */
    
    public void apexSharingForICTOpp(Map<Id, Opportunity> newItemsMap){
        List<id> ictOppIDList = new List<id>();
        List<user> ownerUsrList = new List<user>();
        map<id, string> oppIDAccIDMap = new map<id, string>();
        map<string, id> grpNameGrpIDMap = new map<string, id>();
        List<OpportunityShare> oppShareList = new List<OpportunityShare>();
        try{
            for(Opportunity opp: newItemsMap.values()){
                if(opp.recordtypeid == GlobalCache.getRecordTypeId('Opportunity', 'Business Segment ICT')){
                    ictOppIDList.add(opp.id);
                }
            }
            
            if(ictOppIDList.size() > 0 && !ictOppIDList.isEmpty()){
                for(opportunity opp: [select id, owner.contact.account.Account_ID__c from opportunity where owner.contactid != null AND ID IN: ictOppIDList]){
                    oppIDAccIDMap.put(opp.id, opp.owner.contact.account.Account_ID__c.replace('-', ''));
                }
                if(oppIDAccIDMap != null && !oppIDAccIDMap.values().isEmpty()){
                    for(group grp: [select id, developername from group where type = 'Regular' AND developername IN: oppIDAccIDMap.values()]){
                        grpNameGrpIDMap.put(grp.developername, grp.id);
                    }
                    if(grpNameGrpIDMap != null && !grpNameGrpIDMap.values().isEmpty()){
                        for(id oppID: oppIDAccIDMap.keySet()){
                            OpportunityShare oppShare = new OpportunityShare();
                            oppShare.OpportunityId = oppID;
                            oppShare.UserOrGroupId = grpNameGrpIDMap.get(oppIDAccIDMap.get(oppID));
                            oppShare.OpportunityAccessLevel = 'edit';
                            oppShareList.add(oppShare);
                        }
                        if(oppShareList.size() > 0 && !oppShareList.isEmpty()){
                            insert oppShareList;
                        }
                    }
                }
            }
        }catch(exception ex){
            system.debug('exception ex==>'+ex);
            GlobalUtility.logMessage('Error','ICT Partner Program Community','apexSharingForICTOpp','','Apex Based Sharing','Apex Based Sharing error','',ex,0);
        }
    }
}