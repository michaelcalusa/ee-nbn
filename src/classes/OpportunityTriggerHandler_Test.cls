/************************************************************************************
- Modified by: Gnana 
- Story: MSEU-9894
- Modified Date: 26/02/2018
************************************************************************************
- Developed by: Ravindran Shanmugam
- Date Created: 06/10/2017 (dd/MM/yyyy)
- Description: Test class for Opportunity Trigger Handler.
- Version History:
- v1.0 - 06/10/2017, RS: Created
***********************************************************************************/
@isTest
private class OpportunityTriggerHandler_Test{
	private static Account testAccount; 
	private static Contact noBillContact;
	private static Contact billContact; 
	private static cscfga__Product_Basket__c prodBasket ;
	private static cscfga__Product_Category__c prodCategory ;
	private static cscfga__Product_Definition__c prodDefn;
	private static cscfga__Product_Configuration__c prodConfig;
	private static RecordType damageRecType;
	/*Gnana - Added below code as part of MSEU-9894*/
	private static Contract accAddr;
	private static Contract accAddr2;
	private static Development__c devObj;	
	private static Stage_Application__c stageAppObj; 
	private static StageApplication_Contact__c stgBillContObj;	
	private static StageApplication_Contact__c stgAPContObj;	
	/*Gnana - Added above code as part of MSEU-9894*/

	/*@testSetup static void createTestRecords() {

		testAccount = TestDataClass.CreateTestAccountData();
		billContact = TestDataClass.CreateTestContactData();
	}*/

	@isTest static void Invoicing_NoAcct_NoContract() {
		damageRecType = [SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Recoverable_Damage' LIMIT 1];
		System.assertEquals(damageRecType.DeveloperName, 'Recoverable_Damage');
		// Create Test Opportunity
		Opportunity objOpp =  new Opportunity ( Name='Test Opp', CloseDate=System.today(), StageName = 'New', 
				recordTypeId = damageRecType.Id);
		insert objOpp;
		//Create Product basket under Opportunity
		prodBasket = TestDataClass.CreateTestBasketData(objOpp.Id);
		prodCategory = TestDataClass.createProdCateg();
		insert prodCategory;
		prodDefn = TestDataClass.createProDef(prodCategory);
		insert prodDefn;
		prodConfig = TestDataClass.createProdConfig(prodBasket);
		prodConfig.cscfga__Configuration_Status__c ='Valid';
		prodConfig.cscfga__Product_Definition__c = prodDefn.id;
		prodConfig.Cost__c = 100;
		insert prodConfig;
		
		prodBasket = [Select Id, Total_Cost__c from cscfga__Product_Basket__c where Id =:prodBasket.Id];
		objOpp.StageName = 'Invoicing';
		try {
			update objOpp;
		} catch(Exception e) {}
		objOpp = [ select StageName from Opportunity where Id =:objOpp.Id ];
		System.assertEquals(objOpp.StageName,'New');
	} 

@isTest static void Invoicing_WithAcct_NoContract() {
		damageRecType = [SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Recoverable_Damage' LIMIT 1];
		System.assertEquals(damageRecType.DeveloperName, 'Recoverable_Damage');
		testAccount = new Account(Customer_Type__c = 'Organisation', ABN__c='66676633401',
			Name = '1234567890123456789012345678901234567');
		insert testAccount;
		// Create Test Opportunity
		Opportunity objOpp =  new Opportunity(Name = 'Test Opp', AccountId = testAccount.Id, CloseDate = System.today(),
				StageName = 'New', recordTypeId = damageRecType.Id);
		insert objOpp;

		//Create Product basket under Opportunity
		prodBasket = TestDataClass.CreateTestBasketData(objOpp.Id);
		prodCategory = TestDataClass.createProdCateg();
		insert prodCategory;
	
		prodDefn = TestDataClass.createProDef(prodCategory);
		insert prodDefn;
	
		prodConfig = TestDataClass.createProdConfig(prodBasket);
		prodConfig.cscfga__Configuration_Status__c ='Valid';
		prodConfig.cscfga__Product_Definition__c = prodDefn.id;
		prodConfig.Cost__c = 100;
		insert prodConfig;
		prodBasket = [Select Id, Total_Cost__c from cscfga__Product_Basket__c where Id =:prodBasket.Id];
		System.assertEquals(prodBasket.Total_Cost__c,prodConfig.Cost__c);
		objOpp.StageName = 'Invoicing';
		try {
			update objOpp;
		} catch(Exception e) {}
		objOpp = [ select StageName from Opportunity where Id =:objOpp.Id ];
		System.assertEquals(objOpp.StageName,'New');
	} 


	@isTest static void Invoicing_withAcct_Contract_NoDunning() {

		RecordType damageRecType = [SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Recoverable_Damage' LIMIT 1];
		//testAccount = TestDataClass.CreateTestAccountData();
		testAccount = new Account(Customer_Type__c = 'Organisation', 
			Name = '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901');
		insert testAccount;
		noBillContact = new Contact(LastName = 'LastName');
		insert noBillContact; 
		Contract con = new Contract(Status='Pending Execution',AccountId = testAccount.Id, 
				BillingCountry = 'Australia');//, Billing_Contact__c = noBillContact.Id); 
		insert con;
		Opportunity objOpp =  new Opportunity(Name = 'Test Opp', ContractId = con.Id,  AccountId = testAccount.Id, CloseDate = System.today(),StageName = 'New', recordTypeId = damageRecType.Id);
		insert objOpp;

		//Create Product basket under Opportunity
		prodBasket = TestDataClass.CreateTestBasketData(objOpp.Id);
		prodCategory = TestDataClass.createProdCateg();
		insert prodCategory;
		prodDefn = TestDataClass.createProDef(prodCategory);
		insert prodDefn;
		prodConfig = TestDataClass.createProdConfig(prodBasket);
		prodConfig.cscfga__Configuration_Status__c ='Valid';
		prodConfig.cscfga__Product_Definition__c = prodDefn.id;
		prodConfig.Cost__c = 100;
		insert prodConfig;
		
		prodBasket = [Select Id, Total_Cost__c from cscfga__Product_Basket__c where Id =:prodBasket.Id];
		
		System.assertEquals(prodBasket.Total_Cost__c,prodConfig.Cost__c);
		objOpp.StageName = 'Invoicing';
		try {
			update objOpp;
		} catch(Exception e) {}
		objOpp = [ select StageName from Opportunity where Id =:objOpp.Id ];
		System.assertEquals(objOpp.StageName,'New');

		objOpp.StageName = 'Payment Received';
		try {update objOpp;} catch(Exception e){}
	} 

	@isTest static void Invoicing_withAcct_Contract_dummyDunning() {

		RecordType damageRecType = [SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Recoverable_Damage' LIMIT 1];
		testAccount = TestDataClass.CreateTestAccountData();
		noBillContact = new Contact(LastName = 'LastName');
		insert noBillContact; 
		Contract con = new Contract(Status='Pending Execution',AccountId = testAccount.Id, 
				BillingCountry = 'Australia', Dunning_Contact__c =noBillContact.Id, Billing_Contact__c = noBillContact.Id); 
		insert con;
		Opportunity objOpp =  new Opportunity(Has_Active_Damage_Invoice__c = true, Name = 'Test Opp', ContractId = con.Id,  
			AccountId = testAccount.Id, CloseDate = System.today(),StageName = 'New', recordTypeId = damageRecType.Id);
		insert objOpp;

		//Create Product basket under Opportunity
		prodBasket = TestDataClass.CreateTestBasketData(objOpp.Id);
		prodCategory = TestDataClass.createProdCateg();
		insert prodCategory;
	
		prodDefn = TestDataClass.createProDef(prodCategory);
		insert prodDefn;
	
		prodConfig = TestDataClass.createProdConfig(prodBasket);
		prodConfig.cscfga__Configuration_Status__c ='Valid';
		prodConfig.cscfga__Product_Definition__c = prodDefn.id;
		prodConfig.Cost__c = 0;
		insert prodConfig;
		
		prodBasket = [Select Id, Total_Cost__c from cscfga__Product_Basket__c where Id =:prodBasket.Id];
		
		System.assertEquals(prodBasket.Total_Cost__c,prodConfig.Cost__c);
		objOpp.StageName = 'Invoicing';
		try {
			update objOpp;
		} catch(Exception e) {}
		objOpp = [ select StageName from Opportunity where Id =:objOpp.Id ];
		System.assertEquals(objOpp.StageName,'New');
	} 

	@isTest static void Invoicing_withAcct_validContract() {

		RecordType damageRecType = [SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Recoverable_Damage' LIMIT 1];
		System.assertEquals(damageRecType.DeveloperName, 'Recoverable_Damage');
        Test.startTest();	
		testAccount = TestDataClass.CreateTestAccountData();
		billContact = TestDataClass.CreateTestContactData();
		Contract con = TestDataClass.CreateTestContractData(testAccount.Id, billContact.Id);
		// Create Test Opportunity
		Opportunity objOpp =  new Opportunity(Name = 'Test Opp', ContractId = con.Id,  AccountId = testAccount.Id, 
			CloseDate = System.today(),StageName = 'New', recordTypeId = damageRecType.Id, Has_Active_Damage_Invoice__c = true);
		insert objOpp;
		System.debug('>>>>>>>>>>>>> Opp inserted');
		//Create Product basket under Opportunity
		prodBasket = TestDataClass.CreateTestBasketData(objOpp.Id);
		prodCategory = TestDataClass.createProdCateg();
		insert prodCategory;
		System.debug('>>>>>>>>>>>>> Prod Category inserted');
		prodDefn = TestDataClass.createProDef(prodCategory);
		insert prodDefn;
		System.debug('>>>>>>>>>>>>> prod defn inserted');

		prodConfig = TestDataClass.createProdConfig(prodBasket);
		prodConfig.cscfga__Configuration_Status__c ='Valid';
		prodConfig.cscfga__Product_Definition__c = prodDefn.id;
		prodConfig.Cost__c = 0;
		insert prodConfig;
		System.debug('>>>>>>>>>>>>> prod COnfig inserted');

		prodBasket = [Select Id, Total_Cost__c, csbb__Synchronised_With_Opportunity__c, csordtelcoa__Synchronised_with_Opportunity__c from cscfga__Product_Basket__c where Id =:prodBasket.Id];
		//prodBasket.csbb__Synchronised_With_Opportunity__c = true;
		//prodBasket.csordtelcoa__Synchronised_with_Opportunity__c = true;
		//update prodBasket;
		System.debug('>>>>>>>>>>>>> PB updated to sync');

		System.assertEquals(prodBasket.Total_Cost__c,prodConfig.Cost__c);

        /*csordtelcoa__Orders_Subscriptions_Options__c oso        = new csordtelcoa__Orders_Subscriptions_Options__c();
        oso.csordtelcoa__Module_Name__c                         = 'CS Order Implementation Module Telco A' ;
        oso.csordtelcoa__Module_Version_Number__c               = '1.0';
        oso.csordtelcoa__Order_Initial_State__c                 = 'Order Submitted';
        oso.csordtelcoa__Service_Initial_State__c               = 'Service created';
        oso.csordtelcoa__Service_Number_Prefix__c               = 'SVC-';
        oso.csordtelcoa__Subscription_Initial_State__c          = 'Subscription created';
        oso.csordtelcoa__Subscription_Number_Prefix__c          = 'SN-';
        oso.csordtelcoa__Opportunity_Stages_creating_orders__c  = 'Invoicing, Closed Won';
        oso.csordtelcoa__Module_Debug_Prefix__c                 = 'One Order per Basket';
        oso.csordtelcoa__Generate_Orders_From_Basket__c         = true;
        oso.csordtelcoa__Use_Opportunity_Record_Types__c        = true;
        oso.csordtelcoa__Generate_Orders_From_Bundle__c         = false;
        oso.csordtelcoa__Enforce_Synchronisation__c             = true;
        insert oso; */
		System.debug('>>>>>>>>>>>>> Options inserted');
		try {
			//objOpp.StageName = 'Assessing'; update objOpp;
			//objOpp.StageName = 'Costing';	update objOpp;
			objOpp.StageName = 'Invoicing'; update objOpp;
		} catch (Exception e) {}
		System.debug('>>>>>>>>>>>>> Opp updated to Invoicing');
		objOpp = [select stagename from Opportunity where id = :objOpp.Id];
		System.debug('>>>>>>>>>>>>>'+objOpp.StageName);
		System.assertEquals(objOpp.StageName,'New');
        Test.stopTest();
	}

	@isTest(SeeAllData=true) static void PaymtRecvd_unpaid() {

		RecordType damageRecType = [SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Recoverable_Damage' LIMIT 1];
		System.assertEquals(damageRecType.DeveloperName, 'Recoverable_Damage');
		testAccount = TestDataClass.CreateTestAccountData();
		billContact = TestDataClass.CreateTestContactData();

		testAccount = [select Id from Account limit 1];
		billContact = [select Id from Contact limit 1];
		Contract con = TestDataClass.CreateTestContractData(testAccount.Id, billContact.Id);
		// Create Test Opportunity
		Test.startTest();
		Opportunity objOpp =  new Opportunity(Name = 'Test Opp', Has_Active_Damage_Invoice__c = true, ContractId = con.Id,  AccountId = testAccount.Id, CloseDate = System.today(),StageName = 'Waiting for Payment', recordTypeId = damageRecType.Id);
		insert objOpp;

		ObjectCommonSettings__c oc = new ObjectCommonSettings__c(Name ='erer', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval',
			Cancellation_Requested__c = 'Cancellation Requested' ,Requested__c= 'Requested');

		//oc.put('Requested__c', 'Requested');
		insert oc ;
		ObjectCommonSettings__c invoiceStatuses = ObjectCommonSettings__c.getValues('Invoice Statuses');
		System.assertEquals('Requested', invoiceStatuses.Requested__c);

		Invoice__C invRecord = new Invoice__c(Name='Inv Test' , Opportunity__c = objOpp.Id, Status__C = 'Requested', 
			Payment_Status__c='UnPaid');
		insert invRecord;
		//Create Product basket under Opportunity
		prodBasket = TestDataClass.CreateTestBasketData(objOpp.Id);
		prodCategory = TestDataClass.createProdCateg();
		insert prodCategory;
	
		prodDefn = TestDataClass.createProDef(prodCategory);
		insert prodDefn;
	
		prodConfig = TestDataClass.createProdConfig(prodBasket);
		prodConfig.cscfga__Configuration_Status__c ='Valid';
		prodConfig.cscfga__Product_Definition__c = prodDefn.id;
		prodConfig.Cost__c = 100;
		insert prodConfig;
		prodBasket = [Select Id, Total_Cost__c from cscfga__Product_Basket__c where Id =:prodBasket.Id];
		
		System.assertEquals(prodBasket.Total_Cost__c,prodConfig.Cost__c);

		objOpp.StageName = 'Payment Received';
		try {update objOpp;} catch(Exception e){}
		Test.stopTest();
	} 

	@isTest(SeeAllData=true) static void PaymtRecvd_Fullpaid() {

		RecordType damageRecType = [SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Recoverable_Damage' LIMIT 1];
		System.assertEquals(damageRecType.DeveloperName, 'Recoverable_Damage');
		testAccount = TestDataClass.CreateTestAccountData();
		billContact = TestDataClass.CreateTestContactData();

		testAccount = [select Id from Account limit 1];
		billContact = [select Id from Contact limit 1];
		Contract con = TestDataClass.CreateTestContractData(testAccount.Id, billContact.Id);
		// Create Test Opportunity
		Test.startTest();
		Opportunity objOpp =  new Opportunity(Name = 'Test Opp', Has_Active_Damage_Invoice__c = true, ContractId = con.Id,  AccountId = testAccount.Id, CloseDate = System.today(),StageName = 'Waiting for Payment', recordTypeId = damageRecType.Id);
		insert objOpp;

		ObjectCommonSettings__c oc = new ObjectCommonSettings__c(Name ='erer', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval',
			Cancellation_Requested__c = 'Cancellation Requested' ,Requested__c= 'Requested');

		//oc.put('Requested__c', 'Requested');
		//insert oc ;
		ObjectCommonSettings__c invoiceStatuses = ObjectCommonSettings__c.getValues('Invoice Statuses');
		System.assertEquals('Requested', invoiceStatuses.Requested__c);

		Invoice__C invRecord = new Invoice__c(Name='Inv Test' , Opportunity__c = objOpp.Id, Status__C = 'Requested', 
			Payment_Status__c='Fully Paid');
		insert invRecord;
		//Create Product basket under Opportunity
		prodBasket = TestDataClass.CreateTestBasketData(objOpp.Id);
		//prodCategory = TestDataClass.createProdCateg();
		//insert prodCategory;
	
		//prodDefn = TestDataClass.createProDef(prodCategory);
		prodDefn = new cscfga__Product_Definition__c(cscfga__Description__c= 'testDescription');
		insert prodDefn;
	
		prodConfig = TestDataClass.createProdConfig(prodBasket);
		prodConfig.cscfga__Configuration_Status__c ='Valid';
		prodConfig.cscfga__Product_Definition__c = prodDefn.id;
		prodConfig.Cost__c = 100;
		insert prodConfig;
		prodBasket = [Select Id, Total_Cost__c from cscfga__Product_Basket__c where Id =:prodBasket.Id];
		
		System.assertEquals(prodBasket.Total_Cost__c,prodConfig.Cost__c);
		objOpp.Has_Active_Damage_Invoice__c = false;
		//update objOpp;
		objOpp.StageName = 'Payment Received';
		update objOpp;
		Test.stopTest();
	} 

	@isTest static void Opp_Delete() {
		RecordType damageRecType = [SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'Recoverable_Damage' LIMIT 1];
		System.assertEquals(damageRecType.DeveloperName, 'Recoverable_Damage');
		testAccount = TestDataClass.CreateTestAccountData();
		billContact = TestDataClass.CreateTestContactData();

		testAccount = [select Id from Account limit 1];
		billContact = [select Id from Contact limit 1];
		Contract con = TestDataClass.CreateTestContractData(testAccount.Id, billContact.Id);
		// Create Test Opportunity
		Test.startTest();
		Opportunity objOpp =  new Opportunity(Name = 'Test Opp', Has_Active_Damage_Invoice__c = true, ContractId = con.Id,  AccountId = testAccount.Id, CloseDate = System.today(),StageName = 'Waiting for Payment', recordTypeId = damageRecType.Id);
		insert objOpp;
		System.assertNotEquals(null, objOpp.Id);
		delete objOpp;
	}
	
	/*Gnana - Added below code as part of MSEU-9894*/
    	static void getRecords(){   

	    	testAccount = TestDataClass.CreateTestAccountData();
	    	billContact = TestDataClass.CreateTestContactData();
	        noBillContact = TestDataClass.CreateTestContactData();

		// Create Account Address (Contract)		        
	        accAddr = new Contract();
	        Id accAddrRTId = [SELECT Id FROM RecordType WHERE SobjectType = 'Contract' AND DeveloperName = 'NewDevs_Class_3_4' LIMIT 1].Id;
	        accAddr.RecordTypeId = accAddrRTId;
		accAddr.Status = 'Pending Execution';
	        accAddr.AccountId = testAccount.Id;
	        accAddr.Billing_Contact__c = billContact.Id;
	        accAddr.Dunning_Contact__c = noBillContact.Id;
	        accAddr.BillingCountry = 'Australia';
		accAddr.ShippingCountry = 'Australia';
	        insert accAddr;

		// Create Account Address (Contract)		        
	        accAddr2 = new Contract();        
	        accAddr2.RecordTypeId = accAddrRTId;
		accAddr2.Status = 'Pending Execution';
	        accAddr2.AccountId = testAccount.Id;
	        Contact bCon = TestDataClass.CreateTestContactData();
	        accAddr2.Billing_Contact__c = bCon.Id;
	        Contact dCon = TestDataClass.CreateTestContactData();
	        accAddr2.Dunning_Contact__c = dCon.Id;
	        accAddr2.BillingCountry = 'Australia';
		accAddr2.ShippingCountry = 'Australia';
	        insert accAddr2;
        
	        // Create Development  
	        devObj = new Development__c();      
	        devObj.Name='testName';
	        devObj.Development_ID__c='AKHS833';
	        devObj.Account__c=testAccount.Id;
	        devObj.Primary_Contact__c=noBillContact.Id;
	        devObj.Suburb__c='testsuburd';
	        insert devObj;
        
	        // Create Stage Application
	        stageAppObj = new Stage_Application__c();        
	        stageAppObj.Name='testName';
	        stageAppObj.Request_ID__c ='AKHS833';
	        stageAppObj.Development__c=devObj.Id;
	        stageAppObj.Account__c=testAccount.Id;
	        stageAppObj.Primary_Contact__c=noBillContact.Id;
	        insert stageAppObj;  
        
	        // Create Stage Application Billing Contact
	        stgBillContObj = new StageApplication_Contact__c();
	        stgBillContObj.Stage_Application__c = stageAppObj.Id;
	        stgBillContObj.Contact__c = billContact.Id;
	        stgBillContObj.Roles__c = 'Billing Contact';
	        insert stgBillContObj;
        
	        // Create Stage Application Accounts Payable Contact
	        stgAPContObj = new StageApplication_Contact__c();
	        stgAPContObj.Stage_Application__c = stageAppObj.Id;
	        stgAPContObj.Contact__c = noBillContact.Id;
	        stgAPContObj.Roles__c = 'Account Payable Contact';
	        insert stgAPContObj;

    	}
    
	@isTest (SeeAllData=true) static void CreateLCOOppty() {
	        getRecords();
	        
	        Id lcoRecTypeId = [SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'New_Development_SCO' LIMIT 1].Id;
	        Test.startTest();
	        // Create LCO Opportunity        
	        Opportunity oppWithStg =  new Opportunity(Name = 'Test Opp', Stage_Application__c = stageAppObj.Id,CloseDate = System.today(),StageName = 'New', recordTypeId = lcoRecTypeId);
		insert oppWithStg;

	        // Create LCO Opportunity        
	        Opportunity oppWithOutStg =  new Opportunity(Name = 'Test Opp',CloseDate = System.today(),StageName = 'New', recordTypeId = lcoRecTypeId);
		insert oppWithOutStg;
	        Test.stopTest();
	} 
	/*Gnana - Added above code as part of MSEU-9894*/
    /* 4/10/2018 - Commented out as per the User story MSEU - 17956 */
    /* Swetha - Added above code as part of MSEU - 10558 */
    	/*@isTest static void EnG_createOpptyTasks() {
            List<Opportunity> opptyLst = new List<Opportunity>();
            List<Opportunity> opptyLst1 = new List<Opportunity>();
            Id accRecTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Partner Account').getRecordTypeId();
            Id conRecTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('Partner Contact').getRecordTypeId();
            Id oppRecTypeId = schema.sobjecttype.Opportunity.getrecordtypeinfosbyname().get('Business Segment E&G').getRecordTypeId();
            //Insert an Account
            Account acc = new Account(RecordTypeId=accRecTypeId, Name = 'EnGTestAccount');
            insert acc;
            //Insert Contact
            Contact con = new Contact(LastName = 'TestName', Status__c ='Active', MailingState='NSW');
            insert con;
            //Insert Opportunity
            Opportunity opp = new Opportunity(Name = 'TestOpp', Account = acc, StageName = 'Account Allocated', RecordTypeId = oppRecTypeId,CloseDate = date.today());
            insert opp;
            Test.startTest();
            opp.StageName = 'Proposal submitted';
            try{
                update opp;}
            catch(Exception e){
                 System.assert(e.getMessage().contains('Please select a value for Scenario Select field'));
            }
            opp.ScenarioSelect__c = 'nbn Select/EE through RSP';
            try{
                update opp;}
            catch(Exception e){
                 System.assert(e.getMessage().contains('Please select Proposal Submitted on Phase field'));
            }
            opp.StageName = 'Proposal submitted';
            opp.ScenarioSelect__c = 'nbn Select/EE through RSP';
            update opp;
            System.assertEquals(4, [Select count() from Task where WhatId =: opp.Id and Start_Date__c = today and Type = 'Sub Task']);

            for(integer i=0;i<25;i++){Opportunity oppRec = new Opportunity(Name = 'TestOpp'+i, Account = acc, StageName = 'Account Allocated', RecordTypeId = oppRecTypeId,CloseDate = date.today());
                                      opptyLst.add(oppRec);} insert opptyLst;
            System.assertEquals(4, [Select count() from Task where Start_Date__c = today and Type = 'Sub Task']);
            for(Opportunity o: [Select ScenarioSelect__c from Opportunity where id IN: opptyLst] ){
                o.StageName = 'Proposal submitted';
                o.ScenarioSelect__c = 'MTM';
                opptyLst1.add(o);
            }
            update opptyLst1;
            System.assertEquals(29, [Select count() from Task where Start_Date__c = today and Type = 'Sub Task']);
            Test.stopTest();
	} */
    /* 4/10/2018 - Commented out as per the User story MSEU - 17956 */
    /* Swetha - Added above code as part of MSEU - 10558 */
    // Adding test method code as part of MSEU - 10559 for Oppty Trigger Handler  
    /*@isTest static void EnG_validateOpptyTasks(){
        List<Opportunity> opptyLst = new List<Opportunity>();
        List<Opportunity> opptyLst1 = new List<Opportunity>();
        Id accRecTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Partner Account').getRecordTypeId();
        Id conRecTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('Partner Contact').getRecordTypeId();
        Id oppRecTypeId = schema.sobjecttype.Opportunity.getrecordtypeinfosbyname().get('Business Segment E&G').getRecordTypeId();
        //Insert an Account
        Account acc = new Account(RecordTypeId=accRecTypeId, Name = 'EnGAccount_Test');
        insert acc;
        //Insert Contact
        Contact con = new Contact(LastName = 'EnG_IndustryEng', Status__c ='Active', MailingState='NSW');
        insert con;
        //Insert Opportunity
        Opportunity opp = new Opportunity(Name = 'EnG_TestOpp', Account = acc, StageName = 'Account Allocated', RecordTypeId = oppRecTypeId,CloseDate = date.today());
        insert opp;
        Test.startTest();
        opp.StageName = 'Proposal submitted';
        opp.ScenarioSelect__c = 'MTM';
        update opp;
        opp.StageName = 'Win';
        Test.stopTest();
        try{
            update opp;
        }catch(Exception e){
            System.assert(e.getMessage().contains('Please complete all Open tasks or Mark tasks as Not Required'));
        }
        
    }*/

}