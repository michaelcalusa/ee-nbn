/*================================================
    * @Developer - Gagan Agarwal
    * @Class Name : OpportunityUtility
    * @Purpose: This is a generic class to call methods used in OpportunityTriggerHandler
    * @created date:July 12,2018
    * @Last modified date:
    * @Last modified by : 
================================================*/

Public with sharing class OpportunityUtility {
    public static string message='';
    /**
    *  @name: sendEmailForCommercialWorks
    *  @param: Set<id> oppIds
    *  @Description: This Method sends email to Billing Contact for RW application Fee
    */
    @InvocableMethod(label='Send T&Cs to billing contact' description='To be called from Process builder to when RW APP FEE opty is closed won')
    public static void sendEmailForCommercialWorks(List<id> oppIds){
        map<string,id> mapTempnameID = new map<string,id>();
        list<Messaging.SingleEmailMessage> lstSingleMail = new list<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage emails;
        
        for(EmailTemplate objEmailTemplate : [Select Id, Name,Subject,HtmlValue, developerName,Folder.developername from EmailTemplate 
                                                where Folder.developername !=null]){
            mapTempnameID.put(objEmailTemplate.developerName,objEmailTemplate.id);
        }
        OrgWideEmailAddress owe = [SELECT ID,IsAllowAllProfiles,DisplayName,Address FROM OrgWideEmailAddress WHERE DisplayName= 'nbn Commercial Works' LIMIT 1]; 
        for(Opportunity oppty:[Select id,ContractId,AccountId,Parent_Opportunity__c,StageName,contract.Billing_Contact__c,
                               contract.Billing_Contact__r.email,Parent_Opportunity_Reference_Number__c,
                               contract.Billing_Contact__r.IsEmailBounced
                               from Opportunity 
                               where id in:oppIds and recordtype.developername='RW_Application_Fee']){
           if(oppty.contract.Billing_Contact__r.email !=null && !oppty.contract.Billing_Contact__r.IsEmailBounced
                && oppty.contract.Billing_Contact__c !=null){ 
                emails = new Messaging.SingleEmailMessage();
                emails.setWhatId(oppty.id);
                emails.setSaveAsActivity(true);
                 emails.setOrgWideEmailAddressId(owe.id);
                emails.setTargetObjectId(oppty.contract.Billing_Contact__c);
                emails.setTemplateId(mapTempnameID.get('RW_Application_Fee_T_C_s'));
                lstSingleMail.add(emails);
            }
        }
        if(!lstSingleMail.isEmpty()){
            Messaging.SendEmail(lstSingleMail);  
        }
    }
    
    /**
    *  @name: checkMandatoryFieldsOnSave
    *  @param: Set<id> oppIds
    *  @Description: This Method is used to make mandatory fields check
    */
    public static string checkMandatoryFieldsOnSave(Set<id> oppIds){
        message = '';
        Boolean isMandatoryInfoMissing = false;
        List<Opportunity> oppList = [SELECT Id,ContractId,Account.Customer_Type__c,Account.Registered_Entity_name__c,Account.Name,
                                        AccountId,Account.ABN__C,Account.ACN__c,Contract.Billing_Contact__c,Amount,
                                        Contract.Billing_Contact__r.FirstName,Contract.Billing_Contact__r.LastName,
                                        Contract.Billing_Contact__r.Email,Contract.Billing_Contact__r.Phone,Contract.Billing_Contact__r.MobilePhone,
                                        Owner.Name,Billing_Contact_Id__c,
                                        Contract.BillingStreet,Contract.BillingCity,Contract.BillingPostalCode,Contract.BillingState,
                                        Contract.Dunning_Contact__c,Contract.Dunning_Contact__r.FirstName,Contract.Dunning_Contact__r.Phone,Contract.Dunning_Contact__r.MobilePhone,
                                        RecordType.Name,Contract.Dunning_Contact__r.Email
                                        FROM Opportunity where Id in:oppIds];
            
            for(Opportunity opp:oppList){
                if(opp.AccountId == null){
                    message = message + 'Opportunity Account is null<br />';
                }
                if(opp.AccountId !=null && string.isNotBlank(opp.Account.ABN__c) && (String.valueOf(opp.Account.Name)).length() > 240)
                {
                    message = message + 'Opportunity Account with ABN must have 240 or less characters<br />';
                }
                if(opp.AccountId !=null && string.isBlank(opp.Account.ABN__c) && (String.valueOf(opp.Account.Name)).length() > 150){
                   message = message + 'Opportunity Account without ABN must have 150 or less characters<br />';
                }
                if(opp.AccountId !=null && string.isBlank(opp.Account.Customer_Type__c)){
                    message = message + 'Opportunity Account Customer Type is null<br />';
                }
                if(string.isNotBlank(opp.Account.Customer_Type__c) && opp.Account.Customer_Type__c == 'Organisation' 
                    && (string.isBlank(opp.Account.ABN__c))){
                    message = message + 'Opportunity Account ABN is null<br />';
                }
                if(opp.Amount == null || opp.Amount <= 0){
                    message = message + 'Opportunity Amount is null or 0<br />';
                }
                if(opp.ContractId == null)
                {
                    message = message + 'Opportunity Account Address is null<br />';
                }
                if(opp.ContractId !=null && string.isBlank(opp.Contract.BillingStreet)){
                        message = message + 'Billing Street/Address 1 is mising on Opportunity Account Address <br />';
                }
                if(opp.ContractId !=null && string.isBlank(opp.Contract.BillingCity)){
                    message = message + 'Billing City is mising on Opportunity Account Address<br />';
                }
                if(opp.ContractId !=null && string.isBlank(opp.Contract.BillingState)){
                    message = message + 'Billing State is mising on Opportunity Account Address<br />';
                }
                if(opp.ContractId !=null && string.isBlank(opp.Contract.BillingPostalCode)){
                    message = message + 'Billing Postal Code is mising on Opportunity Account Address<br />';
                }
                if(opp.ContractId !=null && opp.Contract.Billing_Contact__c == null){
                    message = message + 'Billing Contact is missing on Opportunity Account Address<br />';
                }
                if(opp.ContractId !=null && opp.Contract.Billing_Contact__c != null && string.isBlank(opp.Contract.Billing_Contact__r.FirstName)){
                    message = message + 'Billing Contact First Name is null<br />';
                }
                if(opp.ContractId !=null && opp.Contract.Billing_Contact__c != null && string.isBlank(opp.Contract.Billing_Contact__r.LastName)){
                    message = message + 'Billing Contact Last Name is null<br />';
                }
                if(opp.ContractId !=null && opp.Contract.Billing_Contact__c != null && opp.Contract.Billing_Contact__r.Email == null){
                    message = message + 'Billing Contact Email is null<br />';
                }
                //if(opp.Contract.Billing_Contact__r.Phone == null)message = message + 'Billing Contact Phone is null<br />';
                //if(opp.Contract.Billing_Contact__r.MobilePhone == null)message = message + 'Billing Contact Mobile Phone is null<br />';
                if(opp.ContractId !=null && opp.Contract.Billing_Contact__c != null 
                    && opp.Contract.Billing_Contact__r.MobilePhone == null && opp.Contract.Billing_Contact__r.Phone == null)
                {
                    message = message + 'At least one Phone number for Billing Contact is required.<br />';
                }
                 
                if(opp.ContractId !=null && opp.Contract.Dunning_Contact__c == null)
                {
                    message = message + 'Dunning Contact is missing on Opportunity Account Address<br />';
                }
                if(opp.ContractId !=null && opp.Contract.Dunning_Contact__c !=null && string.isBlank(opp.Contract.Dunning_Contact__r.FirstName)){
                    message = message + 'Dunning Contact First Name is null<br />';
                }
                if(opp.ContractId !=null && opp.Contract.Dunning_Contact__c !=null 
                    && opp.Contract.Dunning_Contact__r.MobilePhone == null && opp.Contract.Dunning_Contact__r.Phone == null)
                {
                    message = message + 'At least one Phone number for Dunning Contact is required.<br />';
                }
                if(opp.ContractId !=null && opp.Contract.Dunning_Contact__c !=null && opp.Contract.Dunning_Contact__r.Email == null){
                    message = message + 'Dunning Contact Email is null<br />';
                }
                if(string.isNotBlank(message)){
                    isMandatoryInfoMissing = true;
                }
    
                if(isMandatoryInfoMissing)
                {
                    message ='Some of the mandatory information for Billing is missing.<br />' + message;
                    //message = message;
                    return message;
                }
        }
        return message;
    }
}