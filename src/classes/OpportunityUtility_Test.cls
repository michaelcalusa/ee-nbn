/*================================================
    * @Developer - Gagan Agarwal
    * @Class Name : OpportunityUtility_Test 
    * @Purpose: This is a test class used to test the functionality of OpportunityUtility class
    * @created date: July 12,2018
    * @Last modified date:
    * @Last modified by : 
================================================*/
@isTest
public class OpportunityUtility_Test {
        
    static testMethod void myTest1(){
        Id devRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Recoverable Works').getRecordTypeId();
    
        Account a = DCBTestDataUtil.getNewAccount(); 
        a.Customer_Type__c  = 'Person';
        update a;
        
        Contact c = DCBTestDataUtil.getNewContact();
        c.firstname='Test';
        c.MobilePhone ='98988989';
        update c;

       
        Contract accAddress = new Contract();
        accAddress.AccountId=a.id;
        accAddress.recordtypeId = devRecordTypeId;
        accAddress.Billing_Contact__c = c.id;
        accAddress.Dunning_Contact__c = c.id;
        accAddress.Status = 'Pending Execution'; 
        accAddress.BillingCountry = 'Australia';
        accAddress.BillingStreet = 'Test';
        accAddress.BillingCity = 'Test';
        accAddress.BillingState = 'VIC';
        accAddress.BillingPostalCode = '3000';
        insert accAddress;
        
        Id devRecordTypeId1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RW Application Fee').getRecordTypeId();
        
        System.debug('Total Number of SOQL Queries allowed in this apex code context: ' +  Limits.getQueries());
        //Opportunity testOpty= DCBTestDataUtil.getNewOpportunity(stageApp.Id);
        Opportunity testOpty = new Opportunity(Name = 'Test Opp', AccountId = a.Id, CloseDate = System.today(),
        StageName = 'New');
        testOpty.accountid = a.id;
        testOpty.contractId = accAddress.id;
        insert testOpty;
        
        Opportunity testOpty1= new Opportunity();
        testOpty1.accountid = a.id;
        testOpty1.recordTypeId = devRecordTypeId1;
        testOpty1.contractId = accAddress.id;
        testOpty1.Name ='Test RW';
        //testOpty1.Stage_Application__c = stageApp.Id;
        testOpty1.stageName = 'New';
        testOpty1.CloseDate = system.today();
        testOpty1.Parent_Opportunity__c = testOpty.id;
        testOpty1.Amount = 50;
        insert testOpty1;
        
        System.debug('Total Number of SOQL Queries allowed in this apex code context 1: ' +  Limits.getQueries());
        test.startTest();
        testOpty1.stageName ='Closed Won';
        update testOpty1;
        test.stopTest();
        
    }
    static testMethod void myTest2(){
    try{    
        Id devRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Recoverable Works').getRecordTypeId();
    
    
        Account a = DCBTestDataUtil.getNewAccount(); 
        a.Customer_Type__c  = 'Person';
        update a;
        
        Contact c = DCBTestDataUtil.getNewContact();
        c.firstname='Test';
        c.MobilePhone ='98988989';
        update c;

       
        /*Contract accAddress = new Contract();
        accAddress.AccountId=a.id;
        accAddress.recordtypeId = devRecordTypeId;
        accAddress.Billing_Contact__c = c.id;
        accAddress.Dunning_Contact__c = c.id;
        accAddress.Status = 'Pending Execution'; 
        accAddress.BillingCountry = 'Australia';
        //accAddress.BillingStreet = 'Test';
        accAddress.BillingCity = 'Test';
        accAddress.BillingState = 'VIC';
        accAddress.BillingPostalCode = '3000';
        insert accAddress;*/
        
        Id devRecordTypeId1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RW Application Fee').getRecordTypeId();
        
        
        Opportunity testOpty1= new Opportunity();
        //testOpty1.accountid = a.id;
        testOpty1.recordTypeId = devRecordTypeId1;
        //testOpty1.contractId = accAddress.id;
        testOpty1.Name ='Test RW';
        //testOpty1.Stage_Application__c = stageApp.Id;
        testOpty1.stageName = 'New';
        testOpty1.CloseDate = system.today();
        //testOpty1.Parent_Opportunity__c = testOpty.id;
        testOpty1.Amount = 50;
        insert testOpty1;
        
        test.startTest();
        testOpty1.stageName ='Closed Won';
        update testOpty1;
        test.stopTest(); 
        }catch(Exception e){
            System.Assert(string.isNotBlank(e.getMessage()));
        }    
    }
    static testMethod void myTest3(){
    try{    
        Id devRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Recoverable Works').getRecordTypeId();
    
    
        Account a = DCBTestDataUtil.getNewAccount(); 
        a.Customer_Type__c  = 'Organisation';
        a.ABN__c = 'Test';
        a.Name = 'Rifjrho33GCte4mZr3B8CREMj0SXkkw4rPkqXXqJFlBckgKk1brgUKW09oum0pTywmbsIatXnnOzNNA5iPMy776H6xgJ1RY4sLklMoIlda20s309S7XkzHurSip29vVXbV2ADkN62MOWmwx3la2hiv6AyfqdGUebSFJIccKTtQcwnCPF3FW5PBcKVWAUvxObug9X7ofLUkDGm7FijK6rjIVfLvprCMlKXPq7EVRnluUvQofIz';
        update a;
        
        Contact c = DCBTestDataUtil.getNewContact();
        c.firstname='Test';
        c.MobilePhone ='98988989';
        update c;

       
        Contract accAddress = new Contract();
        accAddress.AccountId=a.id;
        accAddress.recordtypeId = devRecordTypeId;
        //accAddress.Billing_Contact__c = c.id;
        //accAddress.Dunning_Contact__c = c.id;
        accAddress.Status = 'Pending Execution'; 
        accAddress.BillingCountry = 'Australia';
        //accAddress.BillingStreet = 'Test';
        //accAddress.BillingCity = 'Test';
        //accAddress.BillingState = 'VIC';
        //accAddress.BillingPostalCode = '3000';
        insert accAddress;
        
                  
        Id devRecordTypeId1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RW Application Fee').getRecordTypeId();
        
        
        Opportunity testOpty1= new Opportunity();
        testOpty1.accountid = a.id;
        testOpty1.recordTypeId = devRecordTypeId1;
        testOpty1.contractId = accAddress.id;
        testOpty1.Name ='Test RW';
        testOpty1.stageName = 'New';
        testOpty1.CloseDate = system.today();
        //testOpty1.Parent_Opportunity__c = testOpty.id;
        testOpty1.Amount = 50;
        insert testOpty1;
        
        test.startTest();
        testOpty1.stageName ='Closed Won';
        update testOpty1;
        test.stopTest(); 
        }catch(Exception e){
            system.debug('e.getMessage()-----------'+e.getMessage());
            System.Assert(string.isNotBlank(e.getMessage()));
            
       }    
    }
    static testMethod void myTest4(){
    try{    
        Id devRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Recoverable Works').getRecordTypeId();
    
    
        Account a = DCBTestDataUtil.getNewAccount(); 
        a.Name = 'Rifjrho33GCte4mZr3B8CREMj0SXkkw4rPkqXXqJFlBckgKk1brgUKW09oum0pTywmbsIatXnnOzNNA5iPMy776H6xgJ1RY4sLklMoIlda20s309S7XkzHurSip29vVXbV2ADkN62MOWmwx3la2hiv6AyfqdGUebSFJIccKTtQcwnCPF3FW5PBcKVWAUvxObug9X7ofLUkDGm7FijK6rjIVfLvprCMlKXPq7EVRnluUvQofIz';
        update a;
        
        Contact c = DCBTestDataUtil.getNewContact();
        c.firstname='Test';
        c.MobilePhone ='98988989';
        update c;

       
        Contract accAddress = new Contract();
        accAddress.AccountId=a.id;
        accAddress.recordtypeId = devRecordTypeId;
        //accAddress.Billing_Contact__c = c.id;
        //accAddress.Dunning_Contact__c = c.id;
        accAddress.Status = 'Pending Execution'; 
        accAddress.BillingCountry = 'Australia';
        //accAddress.BillingStreet = 'Test';
        //accAddress.BillingCity = 'Test';
        //accAddress.BillingState = 'VIC';
        //accAddress.BillingPostalCode = '3000';
        insert accAddress;
        
        Id devRecordTypeId1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RW Application Fee').getRecordTypeId();
        
        Opportunity testOpty1= new Opportunity();
        testOpty1.accountid = a.id;
        testOpty1.recordTypeId = devRecordTypeId1;
        testOpty1.contractId = accAddress.id;
        testOpty1.Name ='Test RW';
        testOpty1.stageName = 'New';
        testOpty1.CloseDate = system.today();
        //testOpty1.Parent_Opportunity__c = testOpty.id;
        testOpty1.Amount = 50;
        insert testOpty1;
        
        test.startTest();
        testOpty1.stageName ='Closed Won';
        update testOpty1;
        test.stopTest(); 
        }catch(Exception e){
           System.Assert(string.isNotBlank(e.getMessage()));
            
       }    
    }
    static testMethod void myTest5(){
    try{    
        Id devRecordTypeId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('Recoverable Works').getRecordTypeId();
    
    
        Account a = DCBTestDataUtil.getNewAccount(); 
        a.Customer_Type__c  = 'Organisation';
        //a.ABN__c = 'Test';
        a.Name = 'Rifjrho33GCte4mZr3B8CREMj0SXkkw4rPkqXXqJFlBckgKk1brgUKW09oum0pTywmbsIatXnnOzNNA5iPMy776H6xgJ1RY4sLklMoIlda20s309S7XkzHurSip29vVXbV2ADkN62MOWmwx3la2hiv6AyfqdGUebSFJIccKTtQcwnCPF3FW5PBcKVWAUvxObug9X7ofLUkDGm7FijK6rjIVfLvprCMlKXPq7EVRnluUvQofIz';
        update a;
        
        Contact c = DCBTestDataUtil.getNewContact();
        //c.firstname='Test';
        //c.MobilePhone ='98988989';
        //c.LastName = '';
        c.Email = '';
        update c;

       
        Contract accAddress = new Contract();
        accAddress.AccountId=a.id;
        accAddress.recordtypeId = devRecordTypeId;
        accAddress.Billing_Contact__c = c.id;
        accAddress.Dunning_Contact__c = c.id;
        accAddress.Status = 'Pending Execution'; 
        accAddress.BillingCountry = 'Australia';
        accAddress.BillingStreet = 'Test';
        accAddress.BillingCity = 'Test';
        accAddress.BillingState = 'VIC';
        accAddress.BillingPostalCode = '3000';
        insert accAddress;
        
        Id devRecordTypeId1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('RW Application Fee').getRecordTypeId();
        
        Opportunity testOpty1= new Opportunity();
        testOpty1.accountid = a.id;
        testOpty1.recordTypeId = devRecordTypeId1;
        testOpty1.contractId = accAddress.id;
        testOpty1.Name ='Test RW';
        testOpty1.stageName = 'New';
        testOpty1.CloseDate = system.today();
        //testOpty1.Parent_Opportunity__c = testOpty.id;
        testOpty1.Amount = 0;
        insert testOpty1;
        
        test.startTest();
        testOpty1.stageName ='Closed Won';
        update testOpty1;
        test.stopTest(); 
        }catch(Exception e){
           if(e.getMessage().contains('Opportunity')){
                System.Assert(e.getMessage().contains('Opportunity'));
            }
            if(e.getMessage().contains('Contact')){
                System.Assert(e.getMessage().contains('Contact'));
            }
        }    
    }
}