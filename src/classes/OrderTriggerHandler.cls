/*Created By Vipul Jain for Story ACT-16605
Purpose: Handler to Order Trigger
*/
public with sharing class OrderTriggerHandler {
	/*
	Method to be called on AfterUpdate Trigger Call
	Parameters: two Maps of Id with Record of Orders, one with newer details and other with older instance
	*/
    public static void onAfterUpdate(Map<Id, Customer_Connections_Order__c> newMap, Map<Id, Customer_Connections_Order__c> oldMap){
		//Variables for further processing
		Set<String> applicableStatuses = new Set<String>{Constants.Inprogress_Held,Constants.Held};
		Map<Id, Case> closeNCreateTasksForCaseMap =new Map<Id, Case>();
		Map<Id, Case> updateTasksDueDateForCaseMap = new Map<Id, Case>();
		String Subject;
		Date duedate;
		String subType;
		//Lists of Taks to be updated and Inserted after required operations
		List<Task> tasksToBeUpdated = new List<Task>();
		List<Task> tasksList = new List<Task>();
		//Fetching Cust Conn CM Case record Type
		Id agedOrderRecTypID = Schema.SObjectType.case.getRecordTypeInfosByName().get(Constants.CustConCM).getRecordTypeId(); 
		//Below condition is to check that recursion will not happen and only 1 time execution is performed
	
                if(Constants.stopRecursion==false){
        	//Map of All R2T type case records which has Resolver Group and related order is in Held to be used for task creation and updation process
            Map<Id, Case> casesMap = new Map<Id, Case>([SELECT Id, Resolver_Group__c,recordTypeID,ownerId,cc_team__c,Order_ID__c,CC_Order__c,CC_Order__r.Plan_Remediation_Date__c
                                      FROM Case 
                                      WHERE RecordTypeId =: agedOrderRecTypID
                                      AND CC_Team__c =: Constants.Right_Second_Time_Team
                                      AND CC_Order__c IN: newMap.keySet()
                                      AND CC_Order__r.Order_Status__c IN: applicableStatuses
                                      AND Resolver_Group__c != null
                                      ]);
            Set<Id> applicableCases = new Set<Id>();
            
            //Below Loop is to identify cases on which new task needs to be created and older ones needs to be closed and cases on which tasks needs to be updated
            for(Case cs : casesMap.values()){
                if(string.ValueOf(cs.ownerId).startsWith('005') && cs.CC_Order__r.Plan_Remediation_Date__c != oldMap.get(cs.CC_Order__c).Plan_Remediation_Date__c){
                    if(oldMap.get(cs.CC_Order__c).Plan_Remediation_Date__c == null){
                        if(!closeNCreateTasksForCaseMap.containsKey(cs.Id))
                            closeNCreateTasksForCaseMap.put(cs.Id,cs);
                    }
                    else{
                        if(!updateTasksDueDateForCaseMap.containsKey(cs.Id))
                            updateTasksDueDateForCaseMap.put(cs.Id,cs);
                    }
                    applicableCases.add(cs.Id);
                }
            }
            //Fetching all the tasks associated to Applicable Cases
            Map<Id, Task> tasksMap = new Map<Id, Task>([SELECT Id, due_date__c, WhatId, Outcome__c, Subject
                                      FROM Task
                                      WHERE WhatId IN :applicableCases]);
            //Tasks due date is updated with the PRD value
            if(!updateTasksDueDateForCaseMap.isEmpty()){
                for(Task tsk : tasksMap.values()){
                    if(updateTasksDueDateForCaseMap.containsKey(tsk.WhatId) && tsk.Subject != null && String.valueOf(tsk.Subject).startsWith(Constants.RemInProgPRDAvail) && tsk.Outcome__c == Constants.OpenValue){
                        Task taskupd = new Task(Id=tsk.Id,due_date__c = Date.valueOf(updateTasksDueDateForCaseMap.get(tsk.WhatId).CC_Order__r.Plan_Remediation_Date__c));
                        tasksToBeUpdated.add(taskupd);
                    }
                }
            }
            Set<Id> caseIdsToExclude = new Set<Id>();
			//Older tasks are closed and new tasks are created
            if(!closeNCreateTasksForCaseMap.isEmpty()){
                for(Task tsk : tasksMap.values()){
                    if(closeNCreateTasksForCaseMap.containsKey(tsk.WhatId) && tsk.Outcome__c != Constants.COMPLETED && tsk.Subject != null){
                    	//Marking Awaiting Triage and NO PRD Tasks as Complete
                        if(String.valueOf(tsk.Subject).startsWith(Constants.REMEDIATIONNOPRD) ||tsk.Subject == Constants.AWAITING_TRIAGE){
                        	Task taskupd = new Task(Id=tsk.id,outcome__c = Constants.COMPLETED);
                        	tasksToBeUpdated.add(taskupd);	
                        }
                        //Updating due date Available PRD task and excluding it from further calculation of new task creation
                        else if(String.valueOf(tsk.Subject).startsWith(Constants.RemInProgPRDAvail)){
                        	Task taskupd = new Task(Id=tsk.Id,due_date__c = Date.valueOf(closeNCreateTasksForCaseMap.get(tsk.WhatId).CC_Order__r.Plan_Remediation_Date__c));
                        	tasksToBeUpdated.add(taskupd);
                        	caseIdsToExclude.add(tsk.WhatId);
                        }
                    }
                }
                //New Task creation if record is not excluded above
                for(Case cs : closeNCreateTasksForCaseMap.values()){
                	if(!caseIdsToExclude.contains(cs.Id)){
	                    subject = Constants.RemInProgPRDAvail +' '+ cs.Resolver_Group__c;
	                    duedate = Date.valueOf(cs.CC_Order__r.Plan_Remediation_Date__c);
	                    if(cs.Resolver_Group__c.startsWith(Constants.EXTERNAL)){
	                        subType = Constants.EXTERNAL;
	                    }     
	
	                    else{
	                        subType = Constants.INTERNAL;
	                    }     
	                    tasksList.add(caseUtil.createTask(subject,cs,subType,duedate));
                	}
                }
            }
            //Updating the tasks
            if(tasksToBeUpdated.size()>0){
                update tasksToBeUpdated;      
            }
            //Inserting new Tasks
            if(tasksList.size()>0){
                insert tasksList;
            }
            //Marking stopRecursion to true so as to make process run single time
             Constants.stopRecursion=true;
        }
       }
}