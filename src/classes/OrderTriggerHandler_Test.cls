//Created Date	:	01 June 2018
//Description	:	Test Class for OrderTriggerHandler.cls and some parts of CaseUtil.cls
@isTest
private class OrderTriggerHandler_Test {
    @testSetup
    static void dataSetup(){
        Profile caseManagerProfile = [SELECT Id FROM Profile WHERE Name = 'NBN Right Second Time User' LIMIT 1];        
        User caseManager = TestDataUtility.createTestUser(false, caseManagerProfile.Id);
        caseManager.Username = 'test@nbn.test.user.com';
        insert caseManager;
        List<Customer_Connections_Order__c> ccOrdrList = new List<Customer_Connections_Order__c>();
        for(integer i = 5; i < 10; i++){
            Customer_Connections_Order__c ccOrdr = new Customer_Connections_Order__c(Order_ID__c='ord-123'+i,Order_Status__c = 'Held',Location_ID__c='LOC000080307033');
            ccOrdrList.add(ccOrdr);
        }
        insert ccOrdrList;
        List<Case> caseList = new List<Case>();
        case ccCase1 = new case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),Loc_Id__c='LOC000080307033',Order_ID__c='ord-1235',CC_Team__c= 'Right Second Time Team');
        insert ccCase1;
        ccCase1.OwnerId = caseManager.Id;
        ccCase1.Resolver_Group__c = 'Internal : NOC';
        update ccCase1;
        
    }
    static testMethod void testWithNoPRDNoResolver(){
		Profile caseManagerProfile = [SELECT Id FROM Profile WHERE Name = 'NBN Right Second Time User' LIMIT 1];        
        User caseManager = TestDataUtility.createTestUser(false, caseManagerProfile.Id);
        insert caseManager;
        List<Customer_Connections_Order__c> ccOrdrList = new List<Customer_Connections_Order__c>();
        for(integer i = 0; i < 5; i++){
            Customer_Connections_Order__c ccOrdr = new Customer_Connections_Order__c(Order_ID__c='ord-123'+i,Order_Status__c = 'Held',Location_ID__c='LOC000080307033');
            ccOrdrList.add(ccOrdr);
        }
        insert ccOrdrList;
        List<Case> caseList = new List<Case>();
        Test.startTest();
        case ccCase1 = new case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),Loc_Id__c='LOC000080307033',CC_Order__c=ccOrdrList[0].id,CC_Team__c= 'Right Second Time Team',Order_ID__c=ccOrdrList[0].Order_ID__c);
        insert ccCase1;
        ccCase1.OwnerId = caseManager.Id;
        update ccCase1;
        ccCase1.Resolver_Group__c = 'External : SSL';
        update ccCase1;
        ccOrdrList[0].Plan_Remediation_Date__c = Date.today();
        update ccOrdrList[0];
        Test.stopTest();
        List<Task> taskObjList = [SELECT id,due_date__c,outcome__c,Subject from Task where whatId =: ccCase1.id];            
            for(Task tsk: taskObjList){
            	if(tsk.subject.startsWith('Remediation In Progress - No PRD')){
                    system.assertEquals(tsk.outcome__c,'Open');
                    system.assert(tsk.due_date__c!= null);
                }
                else  if(tsk.subject.startsWith('Awaiting remediation triage')){
                    system.assertEquals(tsk.outcome__c,'Completed');
                }
                
            }
    }
    
    static testMethod void testWithNoPRDYesResolver(){
		Profile caseManagerProfile = [SELECT Id FROM Profile WHERE Name = 'NBN Right Second Time User' LIMIT 1];        
        User caseManager = TestDataUtility.createTestUser(false, caseManagerProfile.Id);
        insert caseManager;
        List<Customer_Connections_Order__c> ccOrdrList = new List<Customer_Connections_Order__c>();
        for(integer i = 0; i < 5; i++){
            Customer_Connections_Order__c ccOrdr = new Customer_Connections_Order__c(Order_ID__c='ord-123'+i,Order_Status__c = 'Held',Location_ID__c='LOC000080307033');
            ccOrdrList.add(ccOrdr);
        }
        insert ccOrdrList;
        List<Case> caseList = new List<Case>();
        Test.startTest();
        case ccCase1 = new case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),Loc_Id__c='LOC000080307033',CC_Order__c=ccOrdrList[0].id,CC_Team__c= 'Right Second Time Team',Order_ID__c=ccOrdrList[0].Order_ID__c);
        insert ccCase1;
        ccCase1.OwnerId = caseManager.Id;
        ccCase1.Resolver_Group__c = 'Internal : IWF';
        update ccCase1;
        ccCase1.Resolver_Group__c = '';
        update ccCase1;
        ccCase1.Resolver_Group__c = 'External : SSL';
        update ccCase1;
        Test.stopTest();
        List<Task> taskObjList = [SELECT id,due_date__c,outcome__c,Subject from Task where whatId =: ccCase1.id];            
            for(Task tsk: taskObjList){
            	   
                if(tsk.subject.startsWith('Remediation In Progress - No PRD')){
                    system.assertEquals(tsk.outcome__c,'Open');
                    system.assert(tsk.due_date__c!= null);
                }
            }
    }
    
    static testMethod void testWithYesPRDNoResolver(){
		Profile caseManagerProfile = [SELECT Id FROM Profile WHERE Name = 'NBN Right Second Time User' LIMIT 1];        
        User caseManager = TestDataUtility.createTestUser(false, caseManagerProfile.Id);
        insert caseManager;
        List<Customer_Connections_Order__c> ccOrdrList = new List<Customer_Connections_Order__c>();
        for(integer i = 0; i < 5; i++){
            Customer_Connections_Order__c ccOrdr = new Customer_Connections_Order__c(Order_ID__c='ord-123'+i,Order_Status__c = 'Held',Plan_Remediation_Date__c = Date.today().addDays(-1),Location_ID__c='LOC000080307033');
            ccOrdrList.add(ccOrdr);
        }
        insert ccOrdrList;
        List<Case> caseList = new List<Case>();
        Test.startTest();
        case ccCase1 = new case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),Loc_Id__c='LOC000080307033',CC_Order__c=ccOrdrList[0].id,CC_Team__c= 'Right Second Time Team',Order_ID__c=ccOrdrList[0].Order_ID__c);
        insert ccCase1;
        ccCase1.OwnerId = caseManager.Id;
        update ccCase1;
        ccCase1.Resolver_Group__c = 'Internal : IWF';
        update ccCase1;
        ccCase1.Resolver_Group__c = 'External : SSL';
        update ccCase1;
        ccCase1.Resolver_Group__c = 'Internal : IWF';
        update ccCase1;
        ccOrdrList[0].Plan_Remediation_Date__c = null;
        update ccOrdrList[0];
        Test.stopTest();
       List<Task> taskObjList = [SELECT id,due_date__c,outcome__c,Subject from Task where whatId =: ccCase1.id];            
            for(Task tsk: taskObjList){
            	   
                if(tsk.subject.startsWith('Remediation In Progress - No PRD')){
                    system.assertEquals(tsk.outcome__c,'Open');
                }
            }
    }
    
    static testMethod void testWithYesPRDYesResolver(){
		Profile caseManagerProfile = [SELECT Id FROM Profile WHERE Name = 'NBN Right Second Time User' LIMIT 1];        
        User caseManager = TestDataUtility.createTestUser(false, caseManagerProfile.Id);
        insert caseManager;
        List<Customer_Connections_Order__c> ccOrdrList = new List<Customer_Connections_Order__c>();
        for(integer i = 0; i < 5; i++){
            Customer_Connections_Order__c ccOrdr = new Customer_Connections_Order__c(Order_ID__c='ord-123'+i,Order_Status__c = 'Held',Plan_Remediation_Date__c = Date.today().addDays(-1),Location_ID__c='LOC000080307033');
            ccOrdrList.add(ccOrdr);
        }
        insert ccOrdrList;
        List<Case> caseList = new List<Case>();
        Test.startTest();
        case ccCase1 = new case(recordTypeID=Schema.SObjectType.case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId(),Loc_Id__c='LOC000080307033',CC_Order__c=ccOrdrList[0].id,CC_Team__c= 'Right Second Time Team',Order_ID__c=ccOrdrList[0].Order_ID__c);
        insert ccCase1;
        ccCase1.OwnerId = caseManager.Id;
        ccCase1.Resolver_Group__c = 'Internal : IWF';
        update ccCase1;
        ccCase1.Resolver_Group__c = '';
        update ccCase1;
        ccCase1.Resolver_Group__c = 'External : SSL';
        update ccCase1;
        ccOrdrList[0].Plan_Remediation_Date__c = null;
        update ccOrdrList[0];
        ccCase1.Resolver_Group__c = '';
        update ccCase1;  
        ccCase1.Resolver_Group__c = 'External : SSL';
        update ccCase1;
        Test.stopTest();
       List<Task> taskObjList = [SELECT id,due_date__c,outcome__c,Subject from Task where whatId =: ccCase1.id];            
            for(Task tsk: taskObjList){
            	   
                 if(tsk.subject.startsWith('Remediation In Progress - PRD Available')){
                    system.assertEquals(tsk.outcome__c,'Completed');
                }
            }
        }
    //Added Below Method by Vipul for ACT-17408 
    static testMethod void testWithYesPRDYesResolver_UpdatePRD(){
        List<Customer_Connections_Order__c> orderList = [SELECT id, Order_Id__c,Plan_Remediation_Date__c, Order_Status__c FROM Customer_Connections_Order__c WHERE Order_Id__c = 'ord-1235'];
        Customer_Connections_Order__c testOrd = new Customer_Connections_Order__c(Id=orderList[0].Id);
        testOrd.Plan_Remediation_Date__c=system.now();
        testOrd.Order_Status__c = 'In progress - Held';
        Test.startTest();
        update testOrd;
        Test.stopTest();
         List<Task> taskObjList = [SELECT id,due_date__c,outcome__c,Subject from Task];     
         for(Task tsk: taskObjList){
                if(tsk.subject.startsWith('Remediation In Progress - No PRD')){
                    system.assertEquals(tsk.outcome__c,'Completed');
                    system.assert(tsk.due_date__c!= null);
                }else if(tsk.subject.startsWith('Remediation In Progress - PRD Available')){
                    system.assertEquals(tsk.outcome__c,'Open');
                }else if(tsk.subject.startsWith('Awaiting remediation triage')){
                    system.assertEquals(tsk.outcome__c,'Completed');
                }       
                       
        }
    }
    //Added by Ramya for ACT-17565 
    //Method to test the scenario of changing the PRD value to blank before updating the Resolver group.
     static testMethod void testWithBlankPRDChangeResolver(){
		Customer_Connections_Order__c orderObj = [SELECT id, Order_Id__c,Plan_Remediation_Date__c, Order_Status__c FROM Customer_Connections_Order__c WHERE Order_Id__c = 'ord-1235'];
		Case caseObj =  [SELECT id,resolver_group__c from case where cc_order__c =: orderObj.id];
		User caseManager = [SELECT id from User where username = 'test@nbn.test.user.com'];
		system.runAs(caseManager){
			Test.startTest();
				orderObj.Plan_Remediation_Date__c = null;
				update orderObj;
				caseObj.resolver_group__c = 'External : SSL';
				update caseObj;
			Test.stopTest();		
		
            List<Task> taskObjList = [SELECT id,due_date__c,outcome__c,Subject from Task where whatId =: caseObj.id];            
            for(Task tsk: taskObjList){
                if(tsk.subject.startsWith('Remediation In Progress - No PRD')){
                    system.assertEquals(tsk.outcome__c,'Open');
                    system.assert(tsk.due_date__c!= null);
                }else if(tsk.subject.startsWith('Remediation In Progress - PRD Available')){
                    system.assertEquals(tsk.outcome__c,'Completed');
                }else if(tsk.subject.startsWith('Awaiting remediation triage')){
                    system.assertEquals(tsk.outcome__c,'Completed');
                }
            }
            
        }
    }
}