public class OrderValidatorRequest {

    // Fields
    public ProductOrder productOrder;

    // Constructor
    public OrderValidatorRequest() {
    }

    public class ProductOrder {
        public AccessSeekerInteraction accessSeekerInteraction { get; set; }
        public String orderType { get; set; }
        public String salesforceQuoteId { get; set; }
        public String salesforceBundleId { get; set; }
        public String customerRequiredDate { get; set; }        
        public String afterHoursAppointment { get; set; }       
        public String inductionRequired { get; set; }   
        public String securityClearance { get; set; }   
        public String notes { get; set; }
        public String csaId { get; set; }   
        public String tradingName { get; set; } 
        public String heritageSite { get; set; }    
        public List<ItemInvolvesContact> itemInvolvesContact { get; set; }  
        public ProductOrderComprisedOf productOrderComprisedOf { get; set; }    
        public AccessSeekerContact accessSeekerContact { get; set; }
    }

    public class AccessSeekerInteraction {
        public String billingAccountID { get; set; }
    }

    public class ItemInvolvesContact {
        public String type { get; set; }
        public String contactName { get; set; }
        public String role { get; set; }
        public String emailAddress { get; set; }
        public String phoneNumber { get; set; }
        public UnstructuredAddress unstructuredAddress { get; set; }
    }

    public class ProductOrderComprisedOf {
        public String action { get; set; }
        public ItemInvolvesLocation itemInvolvesLocation { get; set; }
        public ItemInvolvesProductL1 itemInvolvesProduct { get; set; }
        public List<ReferencesProductOrderItemL1> referencesProductOrderItem { get; set; }
    }

    public class UnstructuredAddress {
        public String addressLine1 { get; set; }
        public String addressLine2 { get; set; }
        public String addressLine3 { get; set; }
        public String localityName { get; set; }    
        public String postcode { get; set; }
        public String stateTerritoryCode { get; set; }  
    }

    public class ItemInvolvesLocation {
        public String id { get; set; }
    }

    public class ItemInvolvesProductL1 {
        public String term { get; set; }    
        public String serviceRestorationSLA { get; set; }
        public String accessAvailabilityTarget { get; set; }        
        public String zone { get; set; }    
        public String productType { get; set; }
        public String templateId { get; set; }
        public String templateVersion { get; set; }                 
    }

    public class ReferencesProductOrderItemL1 {
        public String action { get; set; }
        public ItemInvolvesProductL2 itemInvolvesProduct { get; set; }      
        public List<ReferencesProductOrderItemL2> referencesProductOrderItem { get; set; }                              
    }

    public class ItemInvolvesProductL2 {        
        public String type { get; set; }
        // NTD / BTD fields 
        public String btdType { get; set; } 
        public String btdMounting { get; set; }     
        public String powerSupply1 { get; set; }    
        public String powerSupply2 { get; set; }
        // OVC fields
        public String poi { get; set; } 
        public String nni { get; set; }     
        public String routeType { get; set; }   
        public Integer sVLanId { get; set; }        
        public String maximumFrameSize { get; set; }    
        public String cosHighBandwidth { get; set; }    
        public String cosMediumBandwidth { get; set; }
        public String cosLowBandwidth { get; set; }             
        public String uniVLanId { get; set; }   
        public String connectedTo { get; set; }                                                                     
        public String cosMappingMode { get; set; }
        public String ovcTransientId { get; set; }                                                              
    }

    public class ItemInvolvesProductL3 {
        // UNI fields
        public String type { get; set; }    
        public String interfaceBandwidth { get; set; }  
        public String interfaceType { get; set; }       
        public String ovcType { get; set; } 
        public String tpId { get; set; }                                    
        public String transientId { get; set; }
    }

    public class ReferencesProductOrderItemL2 {
        public String action { get; set; }      
        public ItemInvolvesProductL3 itemInvolvesProduct { get; set; }                                  
    }

    public class AccessSeekerContact {
         public String contactName { get; set; }
         public String contactPhone { get; set; }
    }
    
}