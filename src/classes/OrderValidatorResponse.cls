public class OrderValidatorResponse {

    public class OrderValidatorResponseRoot {
        public String status { get; set; }
        public List<ValidationError> validationErrors { get; set; }
    }

    public class ValidationError {        
        public String type { get; set; }
        public List<Error> typeErrors { get; set; }
    }

    public class Error {        
        public String id { get; set; }
        public List<Field> fields { get; set; }
    }

    public class Field {
    	public String fieldName { get; set; }
    	public List<FieldError> fieldErrors { get; set; }    
    }

    public class FieldError {    	
        public String code { get; set; }
        public String description { get; set; }        
    }
}