/***************************************************************************************************
Class Name:  PCCAsAttachmentCTRL
Version     : 1.0 
Created Date: 26-02-2018
Function    : controller class for generateAndSendPCC to generate and send PCC & Council letters
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------      
* Gnana           			07-05-2018				 Modified to add Council_Reference_Number__c
* Suman GunaGanti			26-02-2018               Created
****************************************************************************************************/
public without sharing class PCCAsAttachmentCTRL {
	public Stage_application__c stgapp {get;set;}
	public PCCAsAttachmentCTRL(ApexPages.StandardController stdcontroller){
        if (!Test.isRunningTest()) stdcontroller.addFields(new List<String>{'Name','Address__c','Suburb__c','State__c','Postal_Code__c',
            'Primary_Contact_Name__c','Account_Name__c','Development__r.Development_Number__c','Primary_Contact_Email__c',
            'Application_ID__c', 'Application_Number__c','Primary_Location__r.Site_Address__c','No_of_Premises__c',
            'Real_Property_Description__c','Sent_Council_Letter__c', 'PCC_Issued__c', 'Development__r.Council_Reference_Number__c'});
        this.stgapp = (Stage_application__c)stdcontroller.getRecord(); 

    }
    public void attachPDF(){
        String dtype = ApexPages.currentPage().getParameters().get('DocType');
        //SendDeliverableDocstoDeveloper.SendDeliverableDocstoDeveloper(new List<ID>{stgapp.Id}, dtype);
        if(dtype == 'PCC'){
            sendPccAsAttachment.sendPccEmailWithAttachment(new List<ID>{stgapp.Id});
        }
        else if(dtype == 'CL'){
            sendCouncilLetterAsAttachment.sendCLEmailWithAttachment(new List<ID>{stgapp.Id});
        }
    }
    public PageReference redirectBack(){
        PageReference currstgapp = new PageReference('/'+stgapp.id);
        currstgapp.setRedirect(true);
        return currstgapp;
    }
    
    
}