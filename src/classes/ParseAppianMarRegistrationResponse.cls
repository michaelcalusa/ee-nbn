/***************************************************************************************************
    Class Name          : ParseAppianMarRegistrationResponse
    Version             : 1.0
    Created Date        : 03-Jul-2018
    Author              : Arpit Narain
    Description         : Class to parse Appian response for MAR Registration as well as case creation
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/

public class ParseAppianMarRegistrationResponse {

    public Boolean success {get;set;}
    public String processId {get;set;}

    public ParseAppianMarRegistrationResponse(JSONParser parser){

        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'success') {
                        success = parser.getBooleanValue();
                    } else if (text == 'processId') {
                        processId = parser.getText();
                    } else {
                        System.debug(LoggingLevel.WARN, 'Parse Appian Mar Registration Response consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }

    public static ParseAppianMarRegistrationResponse parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new ParseAppianMarRegistrationResponse(parser);
    }

    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT ||
                    curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                    curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }

}