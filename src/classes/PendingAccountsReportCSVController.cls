// *************************************************
// Developed by: Viswanatha Goki
// Date Created: 18.10.2016 (dd.MM.yyyy)
// Last Update: 18.10.2016 (dd.MM.yyyy)
// Description: Generates CSV of Pending Accounts Report
// Modifications
//     - Add in additional items to the CSV (Alistair Borley)
// *************************************************

public class PendingAccountsReportCSVController {
    public string csvFileData {get; set;}
    public string message {get; set;}

    public ID DocId {get; set;}
 
    public PendingAccountsReportCSVController(){
        message = 'Generating Report';
    }

    public string getAddressLine1(String address)
    {
        string addressLine1 = '';
        if(address != null)
        {
            addressLine1 = address.substringBefore('\n');
            addressLine1 = addressLine1.replace('\r\n', '');
            addressLine1 = addressLine1.replace('\n', '');
            addressLine1 = addressLine1.replace('\r', '');
            if(addressLine1.containsAny(','))
            {
                addressLine1 = '\"' + addressLine1 + '\"';
            }
            return addressLine1;

        } 
        else return '';
    }

    public string getAddressLine2(String address)
    {
        String addressLine2 = '';
        if(address != null)
        {
            addressLine2 = address.substringAfter('\n');
            addressLine2 = addressLine2.replace('\r\n', ', ');
            addressLine2 = addressLine2.replace('\n', ', ');
            addressLine2 = addressLine2.replace('\r', ', ');
            if(addressLine2.containsAny(','))
            {
                addressLine2 = '\"' + addressLine2 + '\"';
            }
            return addressLine2;
        }
        else
        return '';
    }

    public string formatStringWithCommaForCSVFile(String value)
    {
        if(value != null)
        {
            if(value.containsAny(','))
            {
                value = '\"' + value + '\"';
            }
            return value;
        }
        else
        return '';
    }
 
    public PageReference makeCSVFile(){

        try
        {
        	PendingAccountsReportCSV__c pendingAccountReport = PendingAccountsReportCSV__c.getValues('Pending Accounts Report');
        	List<String> CustomerTypes = pendingAccountReport.Customer_Type__c.split(',');
        	
            String fileName = /*'PendingAccountsReport_'*/pendingAccountReport.Filename_Prefix__c + '_' + System.Now();

            System.debug('makeCSVFile : ' + fileName);
            string headerName = pendingAccountReport.HeaderName_1__c + pendingAccountReport.HeaderName_2__c;
            //string headerName = 'Customer Type,Registered Entity Name,Account ID,ABN,ACN,Given Name,Family Name,Work Email,Work Phone #,Address 1,Address 2,Address 3,City,Post Code,State,Owner,Work Mobile #,Development Type,Developer Agreement,' +
            //                    + 'Customer Classification,Created By Module,Original Party Ref,TFN,Billing Original Contact Ref,' +
            //                    + 'Dunning Original Contact Ref,Dunning Contact Given Name,Dunning Contact Family Name,' +
            //                    + 'Dunning Contact Work Email,Dunning Contact Work Phone #,Dunning Contact Work Mobile #,Contract ID';
            //-String[] attrName = new String[]{'Option', 'GST', 'Price'};
            string rows = '';

            // Get all of the Opportunities that a ready for Invoicing
            List<Opportunity> theOpportunities = new List<Opportunity>();

            theOpportunities = [ SELECT Id, Reference_Number__c, Name, StageName,PO_Number__c,ContractId,Owner.Name,
                                 AccountID, Account.AR_Billing_Account_Number__c,Account.Name,Account.ABN__c,Account.ACN__c,
                                 Account.TFN__c,Account.ParentID
                                 from Opportunity WHERE StageName = :pendingAccountReport.Opty_Stage_Name__c /*'Closed Won'*/ and AccountID != null
                                 and (Account.AR_Billing_Account_Number__c = '' or Account.AR_Billing_Account_Number__c = null)
                                 and ContractId != null];
            System.debug('The Opportunities ready for Invoicing with No Billing Account Number: ' + theOpportunities.size());

            if(theOpportunities.size() == 0)
            {
                message = 'No eligible records to generate report.';
                return null;
            }

            // Iterate over the list building up the output line in the CSV file
            for (Opportunity opp : theOpportunities) 
            {
                System.debug('Opportunity Name: ' + opp.Name + ';ID: ' + opp.Id);

                /*
                List<ContractContactRole> listContractContactRole = [Select Id, ContractId, ContactId, Role, 
                                                                Contract.BillingStreet, Contract.BillingCity,
                                                                Contract.BillingState, Contract.BillingPostalCode,
                                                                Contact.FirstName,Contact.LastName,Contact.AR_Contact_Id__c,
                                                                Contact.Email,Contact.Phone,Contact.MobilePhone 
                                                                from ContractContactRole Where Role = 'Billing Contact' and ContractId =:opp.ContractId];
                */
                List<Contract> listContract =  [Select Id, Billing_Contact__c, BillingStreet, BillingCity,
                                                                BillingState, BillingPostalCode,
                                                                Billing_Contact__r.FirstName,Billing_Contact__r.LastName,Billing_Contact__r.AR_Contact_Id__c,
                                                                Billing_Contact__r.Email,Billing_Contact__r.Phone,Billing_Contact__r.MobilePhone,
                                                                Dunning_Contact__c, Dunning_Contact__r.FirstName,Dunning_Contact__r.LastName,
                                                                Dunning_Contact__r.Email,Dunning_Contact__r.Phone,Dunning_Contact__r.MobilePhone,
                                                                Record_Type_Name__c
                                                                from Contract Where Billing_Contact__c != null and Id =:opp.ContractId];                  

                if(listContract == null || listContract.size() == 0) continue;
                system.debug('Contract Id is - ' + listContract[0].Id);
                Boolean hasBillingContact = true;
                Boolean hasDunningContact = true;
                if(listContract == null || listContract.size() == 0) 
                {
                    hasBillingContact = false;
                    hasDunningContact = false;
                }
                else
                {
                    if(listContract[0].Dunning_Contact__c == null)hasDunningContact = false;
                }
                system.debug('Two - ' + listContract[0].Id);
                /*string headerName1 = 'Customer Type,Registered Entity Name,Account ID,ABN,ACN,Given Name,Family Name,Work Email,Work Phone #,'+
                                + 'Address 1,Address 2,Address 3,City,Post Code,State,Owner,Work Mobile #,Development Type,' +
                                + 'Developer Agreement' +
                                + ',Customer Classification,Created By Module,Original Party Ref,TFN,Billing Original Contact Ref,' +
                                + 'Dunning Original Contact Ref,Dunning Contact Given Name,Dunning Contact Family Name,' +
                                + 'Dunning Contact Work Email,Dunning Contact Work Phone #,Dunning Contact Work Mobile #,Contract ID';*/
                if(rows != null)
                {
                    rows = rows + (opp.Account.ABN__c == null || opp.Account.ABN__c == ''? CustomerTypes.get(0)/*'PERSON'*/ : CustomerTypes.get(1)/*'ORGANIZATION'*/) + ','; // Customer Type
                    rows = rows + formatStringWithCommaForCSVFile(opp.Account.Name) + ','; // Registered Entity NameOrganizatio
                    rows = rows + opp.AccountId + ','; // Salesforce Account Id
                    rows = rows + opp.Account.ABN__c + ','; //ABN
                    rows = rows + opp.Account.ACN__c + ','; //ACN
                    rows = rows + (hasBillingContact? formatStringWithCommaForCSVFile(listContract[0].Billing_Contact__r.FirstName) : '') + ','; //Given Name
                    rows = rows + (hasBillingContact? formatStringWithCommaForCSVFile(listContract[0].Billing_Contact__r.LastName) : '') + ','; //Family Name
                    rows = rows + (hasBillingContact? listContract[0].Billing_Contact__r.Email : '') + ','; //Work Email
                    rows = rows + (hasBillingContact? listContract[0].Billing_Contact__r.Phone : '') + ','; //Work Phone
                    rows = rows + (hasBillingContact? getAddressLine1(listContract[0].BillingStreet) : '') + ','; //Address 1
                    rows = rows + (hasBillingContact? getAddressLine2(listContract[0].BillingStreet) : '') + ','; //Address 2
                    rows = rows + ','; //Address 3
                    rows = rows + (hasBillingContact? listContract[0].BillingCity : '') + ','; //City
                    rows = rows + (hasBillingContact? listContract[0].BillingPostalCode : '') + ','; //Post Code
                    rows = rows + (hasBillingContact? listContract[0].BillingState : '') + ','; //State
                    rows = rows + opp.Owner.Name + ','; //Owner
                    rows = rows + (hasBillingContact? listContract[0].Billing_Contact__r.MobilePhone : '') + ','; //Work Mobile
                    rows = rows + ','; //Development Type
                    rows = rows + ','; //Developer Agreement
                    rows = rows + listContract[0].Record_Type_Name__c + ','; //Customer Classification
                    rows = rows + pendingAccountReport.Module__c + ',' /*'SFDC,'*/; //Created By Module
                    system.debug('Three - ' + listContract[0].Id);
                    rows = rows + (opp.Account.ParentID !=null? opp.Account.ParentID : opp.AccountId) + ','; //Original Party Ref(Parent Account ID)    
                    system.debug('Four - ' + listContract[0].Id);
                    rows = rows + (opp.Account.TFN__c != null? opp.Account.TFN__c : '')+ ','; //TFN
                    rows = rows + (hasBillingContact? listContract[0].Billing_Contact__c : '')+ ','; //Billing Original Contact Ref
                    rows = rows + (listContract[0].Dunning_Contact__c != null || listContract[0].Dunning_Contact__c != ''? listContract[0].Dunning_Contact__c : '')+ ','; //Dunning Original Contact Ref
                    rows = rows + (hasDunningContact? listContract[0].Dunning_Contact__r.FirstName : '')+ ','; //Dunning Contact Given Name
                    rows = rows + (hasDunningContact? listContract[0].Dunning_Contact__r.LastName : '')+ ','; //Dunning Contact Family Name
                    rows = rows + (hasDunningContact? listContract[0].Dunning_Contact__r.Email : '')+ ','; //Dunning Contact Work Email
                    rows = rows + (hasDunningContact? listContract[0].Dunning_Contact__r.Phone : '')+ ','; //Dunning Contact Work Phone#
                    rows = rows + (hasDunningContact? listContract[0].Dunning_Contact__r.MobilePhone : '')+ ','; //Dunning Contact Work Mobile#
                    rows = rows + listContract[0].Id + ','; //Contrat Id
                    rows = rows + '\n';
                }
            }

            if(rows == null){
                rows = '';
            }
            
            csvFileData = headerName + '\n' + rows;  
            csvFileData = csvFileData.replace('null','');

            Document tDoc = new Document();
            tDoc.Name = fileName;
            tDoc.Type = pendingAccountReport.File_Extension__c/*'csv'*/;
            tDoc.body = Blob.valueOf(csvFileData); 

            tDoc.FolderId = UserInfo.getUserId(); 
            system.debug('FolderId : ' + tDoc.FolderId); 
            insert tDoc;
            
            DocId = tDoc.id;
            system.debug('DocId : ' + DocId);
            return new PageReference('/'+DocId);
        }catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
            message = 'Error while generating report: ' + ex.getMessage();
        }
        return null;
    }   
}