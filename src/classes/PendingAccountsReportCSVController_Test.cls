@isTest
private class PendingAccountsReportCSVController_Test {
    
    @testsetup static void setupCommonData() {
        PendingAccountsReportCSV__c pendingAccountReport = new PendingAccountsReportCSV__c
            ( Name = 'Pending Accounts Report'
            , Filename_Prefix__c = 'PendingAccountsReport'
            , Opty_Stage_Name__c = 'Closed Won'
            , HeaderName_1__c = 'Customer Type,Registered Entity Name,Account ID,ABN,ACN,Given Name,Family Name,Work Email,Work Phone #,Address 1,Address 2,Address 3,City,Post Code,State,Owner,Work Mobile #,Development Type,Developer Agreement,Customer Classification,Created By Module,'
            , HeaderName_2__c = 'Original Party Ref,TFN,Billing Original Contact Ref,Dunning Original Contact Ref,Dunning Contact Given Name,Dunning Contact Family Name,Dunning Contact Work Email,Dunning Contact Work Phone #,Dunning Contact Work Mobile #,Contract ID'
            , Customer_Type__c = 'PERSON,ORGANIZATION'
            , Module__c = 'SFDC'
            , File_Extension__c = 'csv');
            
        insert pendingAccountReport;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Read Only'];
        
        User u = new User
            ( Alias = 'standt'
            , Email = 'sysAdminuser@cloudsensesolutions.com.au.cpqdev'
            , EmailEncodingKey = 'ISO-8859-1'
            , LastName = 'Testing'
            , LanguageLocaleKey = 'en_US'
            , LocaleSidKey = 'en_AU'
            , ProfileId = p.Id
            , TimeZoneSidKey = 'Australia/Sydney'
            , UserName = 'sitUser@cloudsensesolutions.com'
            );
        
        insert u;   
    }
    
    static User getUser() {
        return [SELECT id, name from User where UserName = 'sitUser@cloudsensesolutions.com' LIMIT 1];
    }
    
    static PendingAccountsReportCSV__c getPendingAccountsReportCSV() {
        return [SELECT id, HeaderName_1__c from PendingAccountsReportCSV__c where Name = 'Pending Accounts Report' LIMIT 1];
    }
    
    @isTest static void test_method_one() {
        // Implement test code
        Opportunity objOpp = TestDataClass.CreateTestOpportunityData();
        //cscfga__Product_Basket__c objBasket = CreateTestBasketData();
        //Update basket with Opportunity - Dont need this

        //Create Account Data
        Account objAccount = TestDataClass.CreateTestAccountData();
        //Create Contact Data
        Contact objContact = TestDataClass.CreateTestContactData();
        //Create Contract Data
        Contract objContract = TestDataClass.CreateTestContractData(objAccount.Id,objContact.Id);
        update objContract;

        //Create ContractContactRole Data
        ContractContactRole objContractRole = TestDataClass.CreateTestContractContactRoleData(objContact.Id,objContract.Id);
        //Link Opp with COntract and Account
        objOpp.AccountId = objAccount.Id;
        objOpp.ContractId = objContract.Id;
        objOpp.StageName = 'Closed Won';
        update objOpp;

        PendingAccountsReportCSVController controllerCSVGeneration = new PendingAccountsReportCSVController();
        String nextPage = controllerCSVGeneration.makeCSVFile().getUrl();
        
        //Assert whether there are any documents created in documents folder
        List<Document> listDocument = [Select Name,Id from Document where Name like 'PendingAccountsReport%'];
        system.assert(listDocument.size() > 0);

    }
    
    @isTest static void test_method_two() {

            PendingAccountsReportCSVController controllerCSVGeneration = new PendingAccountsReportCSVController();
            PageReference nextPageRef = controllerCSVGeneration.makeCSVFile();
            
            system.assert(nextPageRef == null);
            
            Opportunity objOpp = TestDataClass.CreateTestOpportunityData();
    
            //Create Account Data
            Account objAccount = TestDataClass.CreateTestAccountData();
            //Create Contact Data
            Contact objContact = TestDataClass.CreateTestContactData();
            //Create Contract Data
            Contract objContract = TestDataClass.CreateTestContractData(objAccount.Id,objContact.Id);
            update objContract;
    
            //Create ContractContactRole Data
            ContractContactRole objContractRole = TestDataClass.CreateTestContractContactRoleData(objContact.Id,objContract.Id);
            //Link Opp with COntract and Account
            objOpp.AccountId = objAccount.Id;
            objOpp.ContractId = objContract.Id;
            objOpp.StageName = 'Closed Won';
            update objOpp;
    
            controllerCSVGeneration = new PendingAccountsReportCSVController();
            String nextPage = controllerCSVGeneration.makeCSVFile().getUrl();
            
            //Assert whether there are any documents created in documents folder
            List<Document> listDocument = [Select Name,Id from Document where Name like 'PendingAccountsReport%'];
            system.assert(listDocument.size() > 0);
        
    }
    
    @isTest static void test_method_three() {
        // Implement test code
        Opportunity objOpp = TestDataClass.CreateTestOpportunityData();

        //Create Account Data
        Account objAccount = TestDataClass.CreateTestAccountData();
        //Create Contact Data
        Contact objContact = TestDataClass.CreateTestContactData();
        objContact.FirstName = null;
        update objContact;
        //Create Contract Data
        Contract objContract = TestDataClass.CreateTestContractData(objAccount.Id,objContact.Id);
        objContract.BillingStreet = null;
        objContract.BillingCity = null;
        update objContract;

        //Create ContractContactRole Data
        ContractContactRole objContractRole = TestDataClass.CreateTestContractContactRoleData(objContact.Id,objContract.Id);
        //Link Opp with COntract and Account
        objOpp.AccountId = objAccount.Id;
        objOpp.ContractId = objContract.Id;
        objOpp.StageName = 'Closed Won';
        update objOpp;

        PendingAccountsReportCSVController controllerCSVGeneration = new PendingAccountsReportCSVController();
        
        // force an exception
        PendingAccountsReportCSV__c parcsv = getPendingAccountsReportCSV();
        parcsv.Filename_Prefix__c = '============================================================================================================================PendingAccountsReport========================================================================================================';
        update parcsv;
        
        PageReference ref = controllerCSVGeneration.makeCSVFile();
        
        Boolean hasMessages = ApexPages.hasMessages(Apexpages.Severity.Error);
        system.assertEquals(true, hasMessages);

    }
    
    
}