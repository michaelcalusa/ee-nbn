// *************************************************
// Developed by: Alistair Borley
// Date Created: 15.10.2016 (dd.MM.yyyy)
// Last Update: 15.10.2016 (dd.MM.yyyy)
// Description: Generates CSV of Pending Invoices Report
// Modifications
//     - Add in additional items to the CSV (Alistair Borley)
// *************************************************

public class PendingInvoicesReportCSVController {
    public string csvFileData {get; set;}
    public string message {get; set;}

    public ID DocId {get; set;}
 
    public PendingInvoicesReportCSVController(){
        message = 'Generating Report';
    }
    public string getAddressLine1(String address)
    {
        string addressLine1 = '';
        if(address != null)
        {
            addressLine1 = address.substringBefore('\n');
            addressLine1 = addressLine1.replace('\r\n', '');
            addressLine1 = addressLine1.replace('\n', '');
            addressLine1 = addressLine1.replace('\r', '');
            if(addressLine1.containsAny(','))
            {
                addressLine1 = '\"' + addressLine1 + '\"';
            }
            return addressLine1;

        } 
        else return '';
    }

    public string getAddressLine2(String address)
    {
        String addressLine2 = '';
        if(address != null)
        {
            addressLine2 = address.substringAfter('\n');
            addressLine2 = addressLine2.replace('\r\n', ', ');
            addressLine2 = addressLine2.replace('\n', ', ');
            addressLine2 = addressLine2.replace('\r', ', ');
            if(addressLine2.containsAny(','))
            {
                addressLine2 = '\"' + addressLine2 + '\"';
            }
            return addressLine2;
        }
        else
        return '';
    }

    public string formatStringWithCommaForCSVFile(String value)
    {
        if(value != null)
        {
            if(value.containsAny(','))
            {
                value = '\"' + value + '\"';
            }
            return value;
        }
        else
        return '';
    }
 
    public PageReference makeCSVFile(){
        try
        { 
            ObjectCommonSettings__c invoiceSettings = ObjectCommonSettings__c.getValues('Invoice Statuses');
            PendingInvoicesReportCSV__c pendingInvoiceReport = PendingInvoicesReportCSV__c.getValues('Pending Invoices Report');
            List<String> attrNameList = pendingInvoiceReport.AttrName_List__c.split(',');
            List<String> transTypes = pendingInvoiceReport.TransTypes__c.split(',');

            String fileName = /*'PendingInvoicesReport_'*/pendingInvoiceReport.Filename_Prefix__c + '_' + System.Now();

            System.debug('makeCSVFile : ' + fileName);
            string headerName = pendingInvoiceReport.HeaderName_1__c + pendingInvoiceReport.HeaderName_2__c + pendingInvoiceReport.HeaderName_3__c;
            //string headerName = 'TransType,IncGST,CRM Reference ID,Contact ID,Billing Account Number,Registered Entity Name,Site ID,Developer PO Number,Given Name,Family Name,Address 1,Address 2,Address 3,City,State,Post Code,Work Email,Work Phone #,Memo Line,Description,Reference,Dwelling Type,Finance Project Number,No. of Premises,Invoice Amount,Region Code,Adjustment Line Description,Final Invoice Adjustment Amount (Inc GST),Damage Claim Project Number,Damage Claim ID' + 
            //                    + ',Source System,Source Invoice Id,Bpoint Ref,Upfront Invoice,Related Source Invoice Id,' +
            //                    + 'Source Invoice Line Id,Tax Code,Quantity,Item Price,Related Source Invoice Line Id';
            String[] attrName = new String[]{'Option', 'GST', 'Price','Memo Line'}; // Where is this field used ???
            string rows = '';

            // Get all of the Opportunities that a ready for Invoicing
            List<Opportunity> theOpportunities = new List<Opportunity>();

            theOpportunities = [ SELECT Id, Reference_Number__c, Name, AccountID,ContractId, StageName,PO_Number__c,Invoicing_Status__c,
                                 RW_Type__c
                                 from Opportunity 
                                 WHERE StageName = :pendingInvoiceReport.Opty_Stage_Name__c /*'Closed Won'*/ and AccountID != null];
            System.debug('The Opportunities ready for Invoicing : ' + theOpportunities.size());

            List<Invoice_Line_Item__c> listLineItem = new List<Invoice_Line_Item__c>();
            listLineItem = [SELECT Invoice__c,Invoice__r.Name, Invoice__r.BPoint_Ref__c,Invoice__r.Related_Invoice__r.Name,
                            Name,Tax_Code__c,Quantity__c,Unit_Price__c,Related_Line_Item__r.Name,Amount__c,Inc_GST__c,Memo_Line__c,
                            Invoice__r.Opportunity__c,Description__c,Invoice__r.Status__c,
                            Order_Line_Item__r.csord__Order__r.Name
                            FROM Invoice_Line_Item__c where Invoice__r.Status__c = :invoiceSettings.Requested__c/*'Requested'*/
                            and Invoice__r.Record_Type_Name__c = :invoiceSettings.Record_Type_Invoice__c/*'Invoice'*/];

            Map<Id,List<Invoice_Line_Item__c>> mapOppLineItems = new Map<Id,List<Invoice_Line_Item__c>>();
            for(Invoice_Line_Item__c lineItem:listLineItem)
            {
                if(mapOppLineItems.get(lineItem.Invoice__r.Opportunity__c) == null)
                {
                    List<Invoice_Line_Item__c> oppLineItems = new List<Invoice_Line_Item__c>();
                    oppLineItems.add(lineItem);                   
                    mapOppLineItems.put(lineItem.Invoice__r.Opportunity__c, oppLineItems);
                }
                else
                {
                    List<Invoice_Line_Item__c> oppLineItems = mapOppLineItems.get(lineItem.Invoice__r.Opportunity__c);
                    oppLineItems.add(lineItem);
                    mapOppLineItems.put(lineItem.Invoice__r.Opportunity__c,oppLineItems);
                }
            }

            if(theOpportunities == null || theOpportunities.size() == 0 || mapOppLineItems == null || mapOppLineItems.size() == 0)
            {
                message = 'No eligible records to generate report.';
                return null;
            }

            Account theAccount = new Account();
            set<Id> accIds=new set<Id>();
            set<Id> contractIds = new set<Id>();
            for(Opportunity opp:theOpportunities)
            {
                accIds.add(opp.AccountId);
                if(opp.ContractId!=null)contractIds.add(opp.ContractId);
            }
            List<Account> accnts = [Select Id, Name, AR_Billing_Account_Number__c from Account where Id in: accIds];
            map<Id,Account> mapAccountIds = new map<Id,Account>();
            for(Account acc:accnts)
            {
                if(!mapAccountIds.containsKey(acc.Id))mapAccountIds.put(acc.Id,acc);
            }

            List<Contract> contracts = [Select Id, AR_Site_Use_Id__c, Billing_Contact__c, BillingStreet, BillingCity,
                                                                BillingState, BillingPostalCode,
                                                                Billing_Contact__r.FirstName,Billing_Contact__r.LastName,Billing_Contact__r.AR_Contact_Id__c,
                                                                Billing_Contact__r.Email,Billing_Contact__r.Phone,Billing_Contact__r.MobilePhone 
                                                                from Contract Where Billing_Contact__c != null and Id in: contractIds];
            map<id,Contract> mapContractIds = new map<id,Contract>();
            for(Contract con:contracts)
            {
                if(!mapContractIds.containsKey(con.Id))mapContractIds.put(con.Id,con);
            }

            


            // Iterate over the list building up the output line in the CSV file
            for (Integer i=0; i < theOpportunities.size(); i++) 
            {
                System.debug('Opportunity (' + i + ') Result ' + theOpportunities[i].Name + ' ID ' + theOpportunities[i].Id);

                //theAccount = [Select Name, AR_Billing_Account_Number__c from Account where Id =: theOpportunities[i].AccountID];
                theAccount = mapAccountIds.get(theOpportunities[i].AccountID);
                System.debug('Account : ' + theAccount.Name + ' AR Billing Account Number : ' + theAccount.AR_Billing_Account_Number__c);
                
                

                /*List<Contract> listContract =  [Select Id, AR_Site_Use_Id__c, Billing_Contact__c, BillingStreet, BillingCity,
                                                                BillingState, BillingPostalCode,
                                                                Billing_Contact__r.FirstName,Billing_Contact__r.LastName,Billing_Contact__r.AR_Contact_Id__c,
                                                                Billing_Contact__r.Email,Billing_Contact__r.Phone,Billing_Contact__r.MobilePhone 
                                                                from Contract Where Billing_Contact__c != null and Id =:theOpportunities[i].ContractId];*/
                List<Contract> listContract = new List<Contract>();
                if(theOpportunities[i].ContractId!=null && mapContractIds.containsKey(theOpportunities[i].ContractId))
                {
                    listContract.add(mapContractIds.get(theOpportunities[i].ContractId));
                }

                if(listContract == null || listContract.size() == 0) continue;

                List<Invoice_Line_Item__c> oppLineItems = mapOppLineItems.get(theOpportunities[i].Id);
                if(oppLineItems == null || oppLineItems.size() == 0)
                {
                    //As there are no related Invoice Line Items for the opportunity-Skip
                    continue;
                }


                for(Invoice_Line_Item__c lineItem : oppLineItems) 
                { 
                        if (rows != null)
                        {
                            Boolean isRWTypeDamage = false;
                            if(theOpportunities[i].RW_Type__c!= null && theOpportunities[i].RW_Type__c == 'Damage')
                            {
                                isRWTypeDamage = true;
                            }
                            //**Modifi]y below if RW Type is Damage then Recoverable Damages else Recoverable Works
                            rows = rows + (isRWTypeDamage? transTypes.get(0) /*'Recoverable Damages'*/: transTypes.get(1)/*'Recoverable Works'*/)+ ','; //TransType
                            rows = rows + (lineItem.Inc_GST__c?'Y':'N') + ','; // IncGST
                            rows = rows + theOpportunities[i].Reference_Number__c +','; // CRM Reference ID
                            rows = rows + listContract[0].Billing_Contact__r.AR_Contact_Id__c +','; // Contact ID ---- AR Contact ID
                            rows = rows + theAccount.AR_Billing_Account_Number__c + ','; // AR Billing Account Number
                            rows = rows + formatStringWithCommaForCSVFile(theAccount.Name) +','; // Registered Entity Name
                            rows = rows + listContract[0].AR_Site_Use_Id__c +','; // Site ID ---- AR Site Use ID from Contract Object
                            rows = rows + theOpportunities[i].PO_Number__c +','; // Developer PO Number- Field on Opportunity
                            rows = rows + formatStringWithCommaForCSVFile(listContract[0].Billing_Contact__r.FirstName) +','; // Given Name - First Name from From Bill To Contact association with Opportunity Contact
                            rows = rows + formatStringWithCommaForCSVFile(listContract[0].Billing_Contact__r.LastName) +','; // Family Name - Last Name from From Bill To Contact association with Opportunity Contact
                            rows = rows + getAddressLine1(listContract[0].BillingStreet) +','; // Address 1 - From Bill To Contact association with Opportunity Contact
                            rows = rows + getAddressLine2(listContract[0].BillingStreet) +','; // Address 2 - From Bill To Contact association with Opportunity Contact
                            rows = rows +','; // Address 3 - From Bill To Contact association with Opportunity Contact
                            rows = rows + listContract[0].BillingCity +','; // City - From Bill To Contact association with Opportunity Contact
                            rows = rows + listContract[0].BillingState +','; // State - From Bill To Contact association with Opportunity Contact
                            rows = rows + listContract[0].BillingPostalCode +','; // Post Code - From Bill To Contact association with Opportunity Contact
                            rows = rows + listContract[0].Billing_Contact__r.Email +','; // Work Email - From Bill To Contact association with Opportunity Contact
                            rows = rows + listContract[0].Billing_Contact__r.Phone +','; // Work Phone # - From Bill To Contact association with Opportunity Contact
                            rows = rows + lineItem.Memo_Line__c +',';// Memo Line - Name of Product Config under Basket
                            //rows = rows + lineItem.Description__c +',';// Description - Name of Product added(Price Item Description flows all the way to Invoice Line Item description)
                            rows = rows + lineItem.Order_Line_Item__r.csord__Order__r.Name +',';// Description - Name of Product added(Price Item Description flows all the way to Invoice Line Item description)
                            rows = rows + theOpportunities[i].Reference_Number__c +','; // Reference --- Reference Number on Opportunity
                            rows = rows +','; // Dwelling Type - blank
                            rows = rows +','; // Finance Project Number - blank
                            rows = rows + '1' +',';// No. of Premises - Set to 1
                            //**Check if below is the total amount for the product added(unit price times the quantity)
                            rows = rows + lineItem.Amount__c +','; // Invoice Amount (for this line item)
                            rows = rows + '0' +',';// Region Code - Set to 0
                            rows = rows +','; // Adjustment Line Description - Not provided here
                            rows = rows +','; // Final Invoice Adjustment Amount (Inc GST) - Not provided here
                            rows = rows +','; // Damage Claim Project Number  - Not provided here
                            rows = rows +','; // Damage Claim ID - Not provided here
                            rows = rows +'SALESFORCE,'; // Source System - always Salesforce
                            rows = rows + lineItem.Invoice__r.Name +',';
                            rows = rows + lineItem.Invoice__r.BPoint_Ref__c +',';
                            rows = rows +',';//Upfront Invoice - Blank for Phase 1b
                            rows = rows + lineItem.Invoice__r.Related_Invoice__r.Name +',';//Related Source Invoice ID
                            rows = rows + lineItem.Name +',';//Source Invoice Line Id
                            rows = rows + lineItem.Tax_Code__c +',';//Tax Code
                            rows = rows + lineItem.Quantity__c +',';//Quanity
                            rows = rows + lineItem.Unit_Price__c +',';//Item Price 
                            rows = rows + lineItem.Related_Line_Item__r.Name +',';//Related Source Invoice Line ID
                            rows = rows + '\n';
                        }
                    }
            }

            if(rows == null){
                rows = '';
            }
            
            csvFileData = headerName + '\n' + rows;  
            csvFileData = csvFileData.replace('null','');

            Document tDoc = new Document();
            tDoc.Name = fileName;
            tDoc.Type = pendingInvoiceReport.File_Extension__c;//'csv';
            tDoc.body = Blob.valueOf(csvFileData); 

            tDoc.FolderId = UserInfo.getUserId(); 
            system.debug('FolderId : ' + tDoc.FolderId); 
            insert tDoc;
            
            DocId = tDoc.id;
            system.debug('DocId : ' + DocId);
            return new PageReference('/'+DocId);
            //message = 'report generated';

        }catch(Exception ex)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
            message = 'Error while generating report: ' + ex.getMessage();
        }
        return null;
    }   
    
}