@isTest
private class PendingInvoicesReportCSVController_Test {
	
	@testsetup static void setupCommonData() {
		ObjectCommonSettings__c invSet = 
        	new ObjectCommonSettings__c(Name = 'Invoice Statuses', Requested__c = 'Requested', Record_Type_Invoice__c = 'Invoice');
		
		insert invSet;
		
		PendingInvoicesReportCSV__c pendingInvoiceReport = new PendingInvoicesReportCSV__c
			( Name = 'Pending Invoices Report'
			, AttrName_List__c = 'Option,GST,Price,Memo Line'
			, File_Extension__c = 'csv'
			, Filename_Prefix__c = 'PendingInvoicesReport'
			, HeaderName_1__c = 'TransType,IncGST,CRM Reference ID,Contact ID,Billing Account Number,Registered Entity Name,Site ID,Developer PO Number,Given Name,Family Name,Address 1,Address 2,Address 3,City,State,Post Code,Work Email,Work Phone #,Memo Line,Description,Reference,'
			, HeaderName_2__c = 'Dwelling Type,Finance Project Number,No. of Premises,Invoice Amount,Region Code,Adjustment Line Description,Final Invoice Adjustment Amount (Inc GST),Damage Claim Project Number,Damage Claim ID,Source System,Source Invoice Id,Bpoint Ref,Upfront Invoice,'
			, HeaderName_3__c = 'Related Source Invoice Id,Source Invoice Line Id,Tax Code,Quantity,Item Price,Related Source Invoice Line Id'
			, RW_Type__c = 'Damage'
			, Opty_Stage_Name__c = 'Closed Won'
			, TransTypes__c = 'Recoverable Damages,Recoverable Works');
			
		insert pendingInvoiceReport;
	}
	
	static PendingInvoicesReportCSV__c getPendingInvoicesReportCSV() {
		return [SELECT id, HeaderName_1__c from PendingInvoicesReportCSV__c where Name = 'Pending Invoices Report' LIMIT 1];
	}
	
	/*@isTest static void test_method_one() {
		// Create Test Opportunity
		Opportunity objOpp = TestDataClass.CreateTestOpportunityData();

		//Create Account Data
		Account objAccount = TestDataClass.CreateTestAccountData();
		//Create Contact Data
		Contact objContact = TestDataClass.CreateTestContactData();
		//Create Contract Data
		Contract objContract = TestDataClass.CreateTestContractData(objAccount.Id,objContact.Id);
		//Create ContractContactRole Data
		ContractContactRole objContractRole = TestDataClass.CreateTestContractContactRoleData(objContact.Id,objContract.Id);
		//Link Opp with COntract and Account
		objOpp.AccountId = objAccount.Id;
		objOpp.ContractId = objContract.Id;
		objOpp.StageName = 'Closed Won';
		update objOpp;

		Invoice__c inv = new Invoice__c(Name ='Test',Opportunity__c =objOpp.Id,Status__c='Requested');
		insert inv;
		Invoice_Line_Item__c invLine = new Invoice_Line_Item__c(Name='test',Invoice__c =inv.Id);
		Insert invLine;
		Invoice_Line_Item__c invLine2 = new Invoice_Line_Item__c(Name='test2',Invoice__c =inv.Id);
		insert invLine2;*/
		
		/*//Create Product basket under Opportunity
		cscfga__Product_Basket__c objBasket = TestDataClass.CreateTestBasketData(objOpp.Id);


		cscfga__Product_Category__c testProdCateg = TestDataClass.createProdCateg();
        insert testProdCateg;
    
        cscfga__Product_Definition__c testProDef = TestDataClass.createProDef(testProdCateg);
        insert testProDef;
    
        cscfga__Product_Configuration__c prodConfig = TestDataClass.createProdConfig(objBasket);
        prodConfig.cscfga__Configuration_Status__c ='Valid';
        prodConfig.cscfga__Product_Definition__c = testProDef.id;
        insert prodConfig;
    

       
        
        cscfga__Attribute_Definition__c testAttrDef = TestDataClass.createAttrDef(testProDef);  
        //testAttrDef.cscfga__configuration_output_mapping__c = 'test';
        insert testAttrDef;
        
        cscfga__Attribute__c testAttr1 = TestDataClass.createAttr(prodConfig,testAttrDef);
        testAttr1.cscfga__Product_Configuration__c = prodConfig.Id;   
        //testAttr.Configuration_Output_Mapping__c = Opportunity.Name;
        testAttr1.cscfga__Value__c = '1000';
        testAttr1.Name = 'Option';
        insert testAttr1; */
		
		// Add Product Config to Basket
		//Add attributes to Product Config
		//Create Account Data
		//Create Contact Data
		//Create Contract Data
		//Create ContractContactRole Data
		//Link Opp with COntract and Account

		/*PendingInvoicesReportCSVController controllerCSVGeneration = new PendingInvoicesReportCSVController();
		//String nextPage = controllerCSVGeneration.makeCSVFile().getUrl();
		PageReference ref = controllerCSVGeneration.makeCSVFile();
		//Assert whether there are any documents created in documents folder
		List<Document> listDocument = [Select Name,Id from Document where Name like 'PendingInvoicesReport%'];
		system.assert(listDocument.size() > 0);
		

	}*/
	
	@isTest static void test_method_two() {
		// Create Test Opportunity
		Opportunity objOpp = TestDataClass.CreateTestOpportunityData();

		//Create Account Data
		Account objAccount = TestDataClass.CreateTestAccountData();
		//Create Contact Data
		Contact objContact = TestDataClass.CreateTestContactData();
		//Create Contract Data
		Contract objContract = TestDataClass.CreateTestContractData(objAccount.Id,objContact.Id);
		//Create ContractContactRole Data
		ContractContactRole objContractRole = TestDataClass.CreateTestContractContactRoleData(objContact.Id,objContract.Id);
		//Link Opp with COntract and Account
		objOpp.AccountId = objAccount.Id;
		objOpp.ContractId = objContract.Id;
		objOpp.StageName = 'Closed Won';
		update objOpp;
		

		PendingInvoicesReportCSVController controllerCSVGeneration = new PendingInvoicesReportCSVController();
		PageReference ref = controllerCSVGeneration.makeCSVFile();
		system.assertEquals('No eligible records to generate report.', controllerCSVGeneration.message);
		
		
		// Create Test Opportunity
		Opportunity objOpp2 = TestDataClass.CreateTestOpportunityData();
		objOpp2.AccountId = objAccount.Id;
		objOpp2.ContractId = objContract.Id;
		objOpp2.StageName = 'Closed Won';
		objOpp2.RW_Type__c = 'Damage';
		update objOpp2;
		
		Invoice__c inv = new Invoice__c(Name = 'Test', Opportunity__c = objOpp2.Id, Status__c = 'Requested');
		insert inv;
		Invoice_Line_Item__c invLine = new Invoice_Line_Item__c(Name = 'test', Invoice__c = inv.Id);
		insert invLine;
		Invoice_Line_Item__c invLine2 = new Invoice_Line_Item__c(Name = 'test2', Invoice__c = inv.Id);
		insert invLine2;
		
		PendingInvoicesReportCSVController controllerCSVGeneration2 = new PendingInvoicesReportCSVController();
		PageReference ref2 = controllerCSVGeneration2.makeCSVFile();
		List<Document> listDocument2 = [Select Name,Id from Document where Name like 'PendingInvoicesReport%'];
		system.assert(listDocument2.size() > 0);
		
	}
	
	@isTest static void test_method_three() {
		// Create Test Opportunity
		Opportunity objOpp = TestDataClass.CreateTestOpportunityData();

		//Create Account Data
		Account objAccount = TestDataClass.CreateTestAccountData();
		//Create Contact Data
		Contact objContact = TestDataClass.CreateTestContactData();
		objContact.FirstName = null;
		update objContact;
		//Create Contract Data
		Contract objContract = TestDataClass.CreateTestContractData(objAccount.Id,objContact.Id);
		objContract.BillingStreet = null;
		objContract.BillingCity = null;
		update objContract;
		//Create ContractContactRole Data
		ContractContactRole objContractRole = TestDataClass.CreateTestContractContactRoleData(objContact.Id,objContract.Id);
		//Link Opp with COntract and Account
		objOpp.AccountId = objAccount.Id;
		objOpp.ContractId = objContract.Id;
		objOpp.StageName = 'Closed Won';
		update objOpp;
		
		Invoice__c inv = new Invoice__c(Name = 'Test', Opportunity__c = objOpp.Id, Status__c = 'Requested');
		insert inv;
		Invoice_Line_Item__c invLine = new Invoice_Line_Item__c(Name = 'test', Invoice__c = inv.Id);
		insert invLine;
		Invoice_Line_Item__c invLine2 = new Invoice_Line_Item__c(Name = 'test2', Invoice__c = inv.Id);
		insert invLine2;
		
		PendingInvoicesReportCSVController controllerCSVGeneration = new PendingInvoicesReportCSVController();
		
		// force an exception
		PendingInvoicesReportCSV__c pircsv = getPendingInvoicesReportCSV();
		pircsv.Filename_Prefix__c = '============================================================================================================================PendingInvoicesReport========================================================================================================';
		update pircsv;
		
		PageReference ref = controllerCSVGeneration.makeCSVFile();
		
		Boolean hasMessages = ApexPages.hasMessages(Apexpages.Severity.Error);
			
		system.assertEquals(true, hasMessages);
	}
	
	
}