public without sharing class PortalTriage {
    /***************************************************************************************************
    Class Name          : PortalTriage
    Version             : 1.0 
    Created Date        : 23 - Jan - 2018
    Author              : Dilip Athley
    Description         : Logic to identify the user who clicked the Portal link has to be navigated to Login or Registration page
    Input Parameters    : Portal Unique Code
    Output Parameters   : boolean flag set to true if user record is created against contact or false.
    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Dilip Athley       
****************************************************************************************************/ 

    @AuraEnabled
     public static boolean identifyContact(string portalUCode)
     {
         system.debug(portalUCode);
         
         list <User> userList = new list<User>();
         string email;
         list<Contact> conList = [select Id, Email from Contact where Portal_Unique_Code__c =:portalUCode limit 1];
         system.debug('Contact list-->'+conList);
         if(conList.size()>0)
         {
             email = conlist[0].email;
             userList = [select Id, email from user where ContactId =: conList[0].Id AND isPortalEnabled = true AND Profile.Name = 'New Dev Portal User' AND IsActive = true];
             system.debug('Contact list-->'+userList);
         }        
         
         if(userList.size()>0)
         {
             system.debug('Reached here true');
             return true;
         }
         else
         {
             system.debug('Reached here false');
             return false;
         }
         
     }
}