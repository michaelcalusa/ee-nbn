@isTest
private class PortalTriage_Test {
     /***************************************************************************************************
    Class Name          : PortalTriage_Test
    Version             : 1.0 
    Created Date        : 06 - Feb - 2018
    Author              : Dilip Athley
    Description         : Test class for PortalTriage class
    Input Parameters    : 
    Output Parameters   : 
    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Dilip Athley       
****************************************************************************************************/ 
    
    static testMethod void PortalTriage_Test()
    {
        
        list<Contact> conList = new list<Contact>();
        Contact con1 = new Contact();
        con1.FirstName = 'TestFN1';
        con1.LastName = 'TestLN1';
        con1.Email = 'testfn@testln1.com';
        con1.Portal_Unique_Code__c = '12345678901';
        conList.add(con1);
        Contact con2 = new Contact();
        con2.FirstName = 'TestFN2';
        con2.LastName = 'TestLN2';
        con2.Email = 'testfn@testln2.com';
        con2.Portal_Unique_Code__c = '12345678902';
        conList.add(con2);
        insert conList;
        Test.StartTest();
        boolean testFlag = PortalTriage.identifyContact('12345678901');
        Test.stopTest(); 
    }

}