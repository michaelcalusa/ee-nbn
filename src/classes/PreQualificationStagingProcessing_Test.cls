@IsTest
public class PreQualificationStagingProcessing_Test {
    
    @testsetup static void testRecordSetup(){
        
        List<Global_Form_Staging__c> gfslist = new List<Global_Form_Staging__c>();
        Global_Form_Staging__c gfs1 = new Global_Form_Staging__c();
        gfs1.Content_Type__c = 'application/json';
        gfs1.Data__c = '{"layer2Network":true,"newServiceProvider":true,"technologyCategories": ["Fixed Line","Fixed Wireless","Satellite"],"enquiryType":"Business","abn":"86156563741","regName":"NBN CO LIMITED","givenName":"Test Wayne 18","familyName":"Test Wayne 18","phoneNumber":"0488111333","emailAddress":"wayneni@nbnco.com.au","website":"nbnco.co.au","jobTitle":"Mr.","address":{"addressId":"LOC000048823749","formattedAddress":"132 Westbury Cl, Balaclava, VIC","source":"lapi"}}';
        gfs1.Status__c = 'Completed';
        gfs1.Type__c = 'pre-qualification questionnaire'; 
        gfslist.add(gfs1);
        
        insert gfslist;
        
        
    }
    
    
    @isTest static void ProcessBuilderInvokeMethod_Test(){

 		List<Global_Form_Staging__c> testrecords = [select id from Global_Form_Staging__c];
        system.debug('testrecords is '+testrecords);
 		PreQualificationStagingRecordsProcessing.ProcessBuilderInvokeMethod(testrecords);
        
        List<Lead> results = [select id,lastname,firstname from lead where LastName = 'Test Wayne 18'];
        system.assertNotEquals(null, results);        
    }

}