public class PreQualificationStagingRecordsProcessing {
    
    //Class variables
    public static final string Pre_Qua_Resource_Type = 'pre-qualification questionnaire';
    public static boolean InsertLeadFlag = true;
    
    @InvocableMethod
    public static void ProcessBuilderInvokeMethod(List<Global_Form_Staging__c> trgNewList){
      system.debug('proces builder invoked the process');
      Id recordid = trgNewList[0].id;
      
      List<Id> recordIds = new List<Id> ();
      if(trgNewList != null && trgNewList.size() > 0){
          for(Global_Form_Staging__c singleRecord:trgNewList){
              recordIds.add(singleRecord.id);
          }            
      }
	  
      //Processing the 'completed' status pre-qualification records
      List<Global_form_staging__c> InterestedPreQuaRecords = [select id, Name, Status__c, Type__c, Data__c, Content_Type__c from Global_Form_Staging__c  where id in: recordIds];        
        
      List<Global_form_staging__c> CompletedPreQuaRecords = new List<Global_form_staging__c>();
      List<Global_form_staging__c> InProgressPreQuaRecords = new List<Global_form_staging__c>();  
      List<Global_form_staging__c> CancelledPreQuaRecords = new List<Global_form_staging__c>();  
        
      for(Global_form_staging__c singleGlobalFormStagingRecord: InterestedPreQuaRecords){
          if(singleGlobalFormStagingRecord.status__c == 'Completed'){
             CompletedPreQuaRecords.add(singleGlobalFormStagingRecord);
          }/*
          else if(singleGlobalFormStagingRecord.status__c == 'In progress'){
             InProgressPreQuaRecords.add(singleGlobalFormStagingRecord); 
          }else if(singleGlobalFormStagingRecord.status__c == 'Cancelled'){
             CancelledPreQuaRecords.add(singleGlobalFormStagingRecord);
          }*/
      }
        
        
      if(CompletedPreQuaRecords != null && CompletedPreQuaRecords.size() > 0 ){
            WebToLeadProcessingTriggerHandlerUtil.PreQualificationCompletedProcessing(CompletedPreQuaRecords);
      }
        
      //Processing the 'In Progress' status pre-qualification records
      /*
	  if(InProgressPreQuaRecords != null && InProgressPreQuaRecords.size() > 0 ){
          WebToLeadProcessingTriggerHandlerUtil.PreQualificationInProgressProcessing(InProgressPreQuaRecords);
      }             
	  */    
        
    }

}