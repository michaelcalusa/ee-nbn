/***************************************************************************************************
    Class Name          : PublishPlatformEventOnTask
    Test Class          :
    Version             : 1.0 
    Created Date        : 30-Nov-2017
    Author              : Anjani Tiwari
    Description         : Subscribe Platform Event from Appian. 
    Input Parameters    :

    Modification Log    :
    * Developer             Date            Description
    Naseer Abbasi           1/30/2019       Added Attribute Updates event
    * ----------------------------------------------------------------------------                 

****************************************************************************************************/ 
 public class ProcessAppianSubscribedEvents{
    
    Static Id taskRecTypId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(); 
    Static New_Dev_Platform_Events__c identifierList = New_Dev_Platform_Events__c.getOrgDefaults();
  
    public static void CreateTasksOnApplication(List<Developments__e> taskCreateEventList){
        List<Task> taskList = new List<Task>();
        List<Stage_Application__c> stgAppTobeUpdatedList = new List<Stage_Application__c>();
        List<Site__c> siteTobeUpdatedList = new List<Site__c>();
        set<Id> stageAppOptyIds = new set<Id>();               
        set<Id> stageAppIds = new set<Id>();
        set<Id> stageAppAccIds = new set<Id>();
        set<Id> stageAppConIds = new set<Id>();
        set<String> newDevAppRefNo = new Set<String>();
        
        for(Developments__e evt : taskCreateEventList){
            Map<String,String> evtBodyMap = new Map<String,String>(); 
            List<String> bodyList = evt.Message_Body__c.split(',');
            
            for(String s : bodyList){
               List<String> strVal = s.split(':');
                evtBodyMap.put(strVal[0],strVal.size()==2?strVal[1]:'');
            }
            
            if(evtBodyMap.containskey('applicationId')){
              stageAppIds.add(evtBodyMap.get('applicationId'));
            }
        }
        
         Map<Id,stage_Application__c> stageAppList= new Map<Id,stage_Application__c>([Select Id,Active_Status__c,SD_Reference_Number__c,Class__c, Estimated_Ready_for_Service_Date__c, Applicant_Role__c,Comment__c ,Reference_Number__c,Account__r.Id,Primary_Contact__r.Id, Relationship_Manager__c,Relationship_Manager__r.isActive,Application_Status__c,Primary_Location__r.Technology_Type__c from Stage_Application__c where Id in: stageAppIds]);
        System.debug('@stageAppList=='+ stageAppList);
        
        for(Developments__e ev : taskCreateEventList){
            Map<String,String> strMap = new Map<String,String>(); 
            List<String> strList = ev.Message_Body__c.split(',');
            
            for(String s : strList){
               List<String> strVal = s.split(':');
                strMap.put(strVal[0],strVal.size()==2?strVal[1]:'');
            }  
            if (ev.Updated_Attributes__c <> '' && ev.Updated_Attributes__c <> null){
                List<String> updatedAttributeList = ev.Updated_Attributes__c.split(';');
                system.debug('updatedAttributeList,==='+updatedAttributeList); 
                for(String s : updatedAttributeList){
                List<String> strVal = s.split(':');
                    strMap.put(strVal[0],strVal.size()==2?strVal[1]:'');
                } 
            }
            
            if(strMap.containskey('applicationId')){
               // Add comment on application whenever Application is updated by Appian
               if(strMap.containskey('comments')){
                         if(stageAppList.get(strMap.get('applicationId')).Comment__c!=null)
                               stageAppList.get(strMap.get('applicationId')).Comment__c = 'Date : '+System.Now().format('dd/MM/yyyy hh:mm a')+'\nFeedback from nbn : \n'+strMap.get('comments')+'\n-------------------------------------------------------\n'+stageAppList.get(strMap.get('applicationId')).Comment__c;
                            else
                               stageAppList.get(strMap.get('applicationId')).Comment__c = 'Date : '+System.Now().format('dd/MM/yyyy hh:mm a')+'\nFeedback from nbn : \n'+strMap.get('comments');
              }  
               
               if(ev.Event_Id__c == identifierList.Area_Planning_Assessment_Assessed__c){
                   if(strMap.containskey('serviceDeliveryType')){
                      if(strMap.get('serviceDeliveryType') == 'SD1')
                         stageAppList.get(strMap.get('applicationId')).Service_Delivery_Type__c = 'SD-1';
                      else if(strMap.get('serviceDeliveryType') == 'SD2'){
                         stageAppList.get(strMap.get('applicationId')).Service_Delivery_Type__c = 'SD-2';
                         if(stageAppList.get(strMap.get('applicationId')).Class__c == 'Class3/4'){
                            if(stageAppList.get(strMap.get('applicationId')).Applicant_Role__c == 'Owner Occupier'){
                              stageAppList.get(strMap.get('applicationId')).Application_Status__c ='Payment Not Required';
                            }else{
                              stageAppList.get(strMap.get('applicationId')).Application_Status__c ='Payment Pending';
                              stageAppOptyIds.add(stageAppList.get(strMap.get('applicationId')).Id);
                              stageAppAccIds.add(stageAppList.get(strMap.get('applicationId')).Account__r.Id);
                              stageAppConIds.add(stageAppList.get(strMap.get('applicationId')).Primary_Contact__r.Id);
                              newDevAppRefNo.add(stageAppList.get(strMap.get('applicationId')).Reference_Number__c);
                            }                                                        
                         }
                      }                                           
                   }
                   if(strMap.containskey('primaryTechnologyType')){
                       stageAppList.get(strMap.get('applicationId')).Local_Technology__c = strMap.get('primaryTechnologyType');                     
                       stageAppList.get(strMap.get('applicationId')).Primary_Location__r.Technology_Type__c = strMap.get('primaryTechnologyType');                       
                   }    
                   stgAppTobeUpdatedList.add(stageAppList.get(strMap.get('applicationId')));
                   siteTobeUpdatedList.add(stageAppList.get(strMap.get('applicationId')).Primary_Location__r);                  
               }
               else if(ev.Event_Id__c == identifierList.Area_Planning_Assessment_Rejected__c){                   
                   stageAppList.get(strMap.get('applicationId')).Application_Status__c = 'Application Rejected';
                   if(strMap.containskey('serviceDeliveryType')){
                      if(strMap.get('serviceDeliveryType') == 'SD1')
                         stageAppList.get(strMap.get('applicationId')).Service_Delivery_Type__c = 'SD-1';
                      else if(strMap.get('serviceDeliveryType') == 'SD2')
                         stageAppList.get(strMap.get('applicationId')).Service_Delivery_Type__c = 'SD-2';
                   }
                   if(strMap.containskey('primaryTechnologyType')){
                       System.debug('primaryTechnologyType'+strMap.get('primaryTechnologyType'));
                       stageAppList.get(strMap.get('applicationId')).Local_Technology__c = strMap.get('primaryTechnologyType');  
                       System.debug('Primary_Location__r.Technology_Type__c'+stageAppList.get(strMap.get('applicationId')).Primary_Location__r.Technology_Type__c);
                       stageAppList.get(strMap.get('applicationId')).Primary_Location__r.Technology_Type__c = strMap.get('primaryTechnologyType');                       
                   }                   
                   stgAppTobeUpdatedList.add(stageAppList.get(strMap.get('applicationId')));
                   siteTobeUpdatedList.add(stageAppList.get(strMap.get('applicationId')).Primary_Location__r);
               }               
               
               Task t = new Task();              
               t.RecordTypeId = taskRecTypId;
               t.Priority = '3-General';
               t.Status = 'Open';              
               t.WhatId = strMap.get('applicationId');    // stage App Id  
               t.Event_Identifier__c = ev.Event_Id__c;
               
               if(strMap.containskey('comments')){
                  t.Description = 'Date : '+System.Now().format('dd/MM/yyyy hh:mm a')+'\nFeedback from nbn : \n'+strMap.get('comments');
               }           
              
               if((stageAppList.get(strMap.get('applicationId')).Relationship_Manager__c!=null) && (stageAppList.get(strMap.get('applicationId')).Relationship_Manager__r.isActive == true)){
                   t.OwnerId = stageAppList.get(strMap.get('applicationId')).Relationship_Manager__c;
               }
               else{
                   t.OwnerId = identifierList.Unassigned_User_Id__c;
                   t.Team__c = identifierList.Back_Office_Team_BOT_Id__c;
               }
              
               If(ev.Event_Id__c == identifierList.Application_was_a_duplicate__c){                  // AP-SF-000006
                    t.Type_Custom__c = 'Service Delivery Rejection'; 
                    t.Sub_Type__c = 'Duplicate';
                    t.Subject = 'Service Delivery Rejected(Duplicate Application)';
                    taskList.add(t);
              }
               else if(ev.Event_Id__c == identifierList.Application_commercially_not_viable__c){     // AP-SF-000007
                    t.Type_Custom__c = 'Service Delivery Rejection'; 
                    t.Sub_Type__c = 'Commercially Not Viable';
                    t.Subject = 'Service Delivery Rejected(Commercially Not Viable)';
                    taskList.add(t);
               }
               else if(ev.Event_Id__c == identifierList.Additional_Information_Required__c){         // AP-SF-000008 
                    t.Type_Custom__c = 'Additional Information Required';                      
                    t.Sub_Type__c = 'Miscellanies';
                    t.Subject = 'Additional Information Required(Miscellanies)' ;                    
                    taskList.add(t);
               }
               else if(ev.Event_Id__c == identifierList.Additional_Document_Required__c){         // AP-SF-000009 
                    t.Type_Custom__c = 'Additional Document Required';                      
                    t.Sub_Type__c = 'Miscellanies';
                    t.Subject = 'Additional Document Required(Miscellanies)' ;
                    t.Deliverable_Status__c = 'Pending Documents';
                    if(strMap.containskey('documentType')){
                      t.Description = 'Date : '+System.Now().format('dd/MM/yyyy hh:mm a')+'\nDocument Type:'+strMap.get('documentType')+'\nFeedback from nbn : \n'+strMap.get('comments');
                      t.Document_Type_to_Submit__c = strMap.get('documentType');
                    }  
                    taskList.add(t);
               }
               else if(ev.Event_Id__c == identifierList.Address_Info_cannot_be_confirmed__c){        // AP-SF-000010
                    t.Type_Custom__c = 'Address Information cannot be Confirmed';
                    t.Subject ='Address Information cannot be Confirmed';
                    taskList.add(t);
               }
               else if(ev.Event_Id__c == identifierList.LASE_Risk_is_Identified__c){                 // AP-SF-000014
                    t.Type_Custom__c = 'LASE Risk Identified'; 
                    t.Subject = 'LASE Risk Identified';
                    taskList.add(t);
               }
               else if(ev.Event_Id__c == identifierList.Premise_Count_Mismatch__c){                 // AP-SF-000003
                    t.Type_Custom__c = 'Premise Count Mismatch'; 
                    t.Subject = 'Premise Count Mismatch'; 
                    if(strMap.containsKey('Premise_Count__c') && strMap.containsKey('Premise_Count_actual__c'))
                        t.Description = t.Description+'\nPremise Count Given : '+strMap.get('Premise_Count__c')+'\nPremise Count Actual : '+strMap.get('Premise_Count_actual__c');
                        stageAppList.get(strMap.get('applicationId')).Premise_Count_Actual__c = strMap.get('Premise_Count_actual__c');
                    taskList.add(t);
               }
               else if(ev.Event_Id__c == identifierList.Location_RFSed__c){                 // AP-SF-000013
                    t.Type_Custom__c = 'Location RFSed'; 
                    t.Subject = 'Location RFSed';
                    t.Status = 'Closed'; 
                    stageAppList.get(strMap.get('applicationId')).Application_Status__c = 'Location RFSed';                   
                    stgAppTobeUpdatedList.add(stageAppList.get(strMap.get('applicationId')));
                    taskList.add(t);
               }          
               else if(ev.Event_Id__c == identifierList.Inbound_Delivery_Time_Impacted__c){          // AP-SF-000012
                    t.Subject = 'Timeline Impacted';
                    t.Type_Custom__c = 'Timeline Impacted'; 
                    t.Sub_Type__c = '';
                    t.Status = 'Closed';
                    stageAppList.get(strMap.get('applicationId')).Estimated_Ready_for_Service_Date__c = date.valueOf(strMap.get('rfsDate'));                    
                    stgAppTobeUpdatedList.add(stageAppList.get(strMap.get('applicationId')));
                    taskList.add(t);
               }
               else if(ev.Event_Id__c == identifierList.Appian_Application_Reference_No__c){         // AP-SF-000032  
                     system.debug('stgeAppEntry===='+strMap.get('appianApplicationReference')); 
                     if(strMap.containskey('appianApplicationReference')){              
                         stageAppList.get(strMap.get('applicationId')).SD_Reference_Number__c =  strMap.get('appianApplicationReference');                        
                   }
                    system.debug('stgeAppList===='+stageAppList);
                    stgAppTobeUpdatedList.add(stageAppList.get(strMap.get('applicationId')));
               }
                else if(ev.Event_Id__c == identifierList.Application_On_Hold__c){          // AP-SF-000033
                    t.Subject = 'Application on Hold';
                    t.Type_Custom__c = 'Service Delivery on Hold'; 
                    t.Sub_Type__c = '';
                    t.Status = 'Closed';
                    if(strMap.containskey('estimatedRfsDate')){  
                       stageAppList.get(strMap.get('applicationId')).Estimated_Ready_for_Service_Date__c = date.valueOf(strMap.get('estimatedRfsDate'));                    
                    }
                    stageAppList.get(strMap.get('applicationId')).Active_Status__c = 'On Hold';
                    stgAppTobeUpdatedList.add(stageAppList.get(strMap.get('applicationId')));
                    taskList.add(t);
               }
                else if(ev.Event_Id__c == identifierList.Application_Resumed__c){          // AP-SF-000034
                    t.Subject = 'Application Resumed';
                    t.Type_Custom__c = 'Service Delivery Resumed'; 
                    t.Sub_Type__c = '';
                    t.Status = 'Closed';
                    stageAppList.get(strMap.get('applicationId')).Active_Status__c = 'Active';                                      
                    stgAppTobeUpdatedList.add(stageAppList.get(strMap.get('applicationId')));
                    taskList.add(t);
               }
               else if(ev.Event_Id__c == identifierList.Remediation_Report_Pass__c){
                    String s1 = strMap.get('DocUrl') <> null?strMap.get('DocUrl').substringAfter('files/'): Null;
                    String docId = s1<>Null? s1.substringBefore('/content'): Null;
                    t.Subject = 'Remediation Report Pass';
                    t.Type_Custom__c = 'Remediation Report Created - Pass';
                    t.Status = 'Closed';
                    Insert t;
                    if(docId <> Null){
                        createDocumentLink(Id.valueof(docId), t.Id);
                        createDocumentLink(Id.valueof(docId),Id.valueOf(strMap.get('applicationId')));
                       }
               }
                else if (ev.Event_Id__c == identifierList.Attribute_Updates_Appian__c){
                    if (strMap.get('samId') <> null){
                        stageAppList.get(strMap.get('applicationId')).SAM__c = strMap.get('samId');
                    }
                    if (strMap.get('fsaId') <> null){
                        stageAppList.get(strMap.get('applicationId')).FSA__c = strMap.get('fsaId');
                    }
                    if (strMap.get('actualTechnologyType') <> null){
                        stageAppList.get(strMap.get('applicationId')).Local_Technology__c = strMap.get('actualTechnologyType');
                    }
                    if (stageAppList.get(strMap.get('applicationId')).Service_Delivery_Type__c == 'SD1'){
                        if (strMap.get('dwellingType') <> null){
                            stageAppList.get(strMap.get('applicationId')).Dwelling_Type__c = strMap.get('dwellingType');
                        }
                    }
                    stgAppTobeUpdatedList.add(stageAppList.get(strMap.get('applicationId')));
                }
           }
         }
         system.debug('@@@'+taskList);
         if(taskList!=null){
             insert taskList;
         }
        if(stgAppTobeUpdatedList!=null){
            update stgAppTobeUpdatedList;
        }
        system.debug('@siteTobeUpdatedList@@'+siteTobeUpdatedList);
        if(siteTobeUpdatedList!=null){
            update siteTobeUpdatedList;
        }
        system.debug('@stageAppOptyIds@@'+stageAppOptyIds);
        if(stageAppOptyIds!=null){
           NewDevsCreateOpportunityOnTechAssesment.CreateOpportunity(stageAppOptyIds,stageAppAccIds,stageAppConIds,newDevAppRefNo);
        }
     }
    
   
   
    // Update the Task on Application and Create new Task of same type if required 
     public static void UpdateTasksOnApplication(List<Developments__e> taskUpdateEventList){
      set<Id> stageAppIds = new set<Id>();
      list<Task> tasksToUpdate = new list<Task>();
      list<Task> tasksToInsert = new list<Task>();
         for(Developments__e evt : taskUpdateEventList){
            Map<String,String> evtBodyMap = new Map<String,String>(); 
            List<String> bodyList = evt.Message_Body__c.split(',');
            
            system.debug('@evt==='+evt);
            
            
            for(String s : bodyList){
               List<String> strVal = s.split(':');
               evtBodyMap.put(strVal[0],strVal[1]);
            }
            system.debug('@evtBodyMap==='+evtBodyMap);
            
            if(evtBodyMap.containskey('applicationId')){
              stageAppIds.add(evtBodyMap.get('applicationId'));
            }
         }
         system.debug('@stageAppIds==='+stageAppIds);
         system.debug('@taskRecTypId==='+taskRecTypId);
         
         Map<Id,stage_Application__c> stageAppList= new Map<Id,stage_Application__c>([Select Id, Relationship_Manager__c,Relationship_Manager__r.isActive from Stage_Application__c where Id in: stageAppIds]);
         System.debug('@stageAppLst=='+ stageAppList);
                
         Map<Id, List<Task>> taskOnApp = new Map<Id, List<Task>>();
        for(Task tsk : [Select Id,WhatId,WhoId,Subject,Status,Description,Deliverable_Status__c,Document_Type_to_Submit__c,Type_Custom__c,Due_Date__c,Sub_Type__c,Team__c,OwnerId,RecordTypeId,Cycle__c from Task where (RecordTypeId =: taskRecTypId) AND (WhatId in : stageAppIds)]){
             system.debug('@tsk@==='+tsk);
             if(taskOnApp.containsKey(tsk.WhatId)) {
                 taskOnApp.get(tsk.WhatId).add(tsk);                
             } else{
                 taskOnApp.put(tsk.WhatId, new List<Task>{tsk});
             }
           
         }
         system.debug('@taskOnApp==='+taskOnApp);
         for(Developments__e evt : taskUpdateEventList){
            Map<String,String> strMap = new Map<String,String>(); 
            List<String> strList = evt.Message_Body__c.split(',');
            
            for(String s : strList){
               List<String> strVal = s.split(':');
               strMap.put(strVal[0],strVal[1]);
            }
             if(strMap.containskey('applicationId')){
                list<Task> tskList = taskOnApp.get(strMap.get('applicationId'));
                
                 for(Task t : tskList){
                      if((t.Type_Custom__c == 'Deliverables' || (t.Type_Custom__c == 'Additional Document Required' && strMap.containskey('documentType') && t.Document_Type_to_Submit__c == strMap.get('documentType')) ) && t.Deliverable_Status__c == 'Submitted' && t.Status=='Open' &&
                         ((t.Sub_Type__c =='Master Plan' && evt.Event_Id__c == identifierList.Master_Plan_Accepted__c)|| 
                          (t.Sub_Type__c =='Pit & Pipe Design' && evt.Event_Id__c == identifierList.Design_Document_Accepted__c) || 
                          (t.Sub_Type__c =='Stage Plan' && evt.Event_Id__c == identifierList.Stage_Plan_Accepted__c ) ||
                          (t.Sub_Type__c =='Service Plan' && evt.Event_Id__c == identifierList.Service_Plan_Accepted__c) ||
                          (t.Sub_Type__c =='As-built Design' && evt.Event_Id__c == identifierList.As_built_Design_Accepted__c) ||
                          (t.Sub_Type__c =='Miscellanies' && evt.Event_Id__c == identifierList.Additional_Document_Accepted__c))){                        //  SF-SD-000004, SF-SD-000020 , AP-SF-000026
                         t.Deliverable_Status__c = 'Accepted';
                         if(strMap.containskey('comments')){
                            if(t.Description!=null)
                               t.Description = 'Date : '+System.Now().format('dd/MM/yyyy hh:mm a')+'\nFeedback from nbn : \n'+strMap.get('comments')+'\n-------------------------------------------------------\n'+t.Description;
                            else
                               t.Description = 'Date : '+System.Now().format('dd/MM/yyyy hh:mm a')+'\nFeedback from nbn : \n'+strMap.get('comments');
                         }
                         tasksToUpdate.add(t);
                     }
                     else If((t.Type_Custom__c == 'Deliverables' || (t.Type_Custom__c == 'Additional Document Required' && strMap.containskey('documentType') && t.Document_Type_to_Submit__c == strMap.get('documentType')) ) && t.Deliverable_Status__c == 'Submitted' && t.Status=='Open' &&
                            ((t.Sub_Type__c =='Master Plan' && evt.Event_Id__c == identifierList.Master_Plan_Rejected__c)||
                             (t.Sub_Type__c =='Pit & Pipe Design' && evt.Event_Id__c == identifierList.Design_Document_Rejected__c) || 
                             (t.Sub_Type__c =='Stage Plan' && evt.Event_Id__c == identifierList.Stage_Plan_Rejected__c)||
                             (t.Sub_Type__c =='Service Plan' && evt.Event_Id__c == identifierList.Service_Plan_Rejected__c)||
                             (t.Sub_Type__c =='As-built Design' && evt.Event_Id__c == identifierList.As_built_Design_Rejected__c)||
                             (t.Sub_Type__c =='Miscellanies' && evt.Event_Id__c == identifierList.Additional_Document_Rejected__c))){                        //  SF-SD-000005, SF-SD-000021 , AP-SF-000027
                         t.Deliverable_Status__c = 'Rejected';
                         if(strMap.containskey('comments')){
                            if(t.Description!=null)
                               t.Description = 'Date : '+System.Now().format('dd/MM/yyyy hh:mm a')+'\nFeedback from nbn : \n'+strMap.get('comments')+'\n-------------------------------------------------------\n'+t.Description;
                            else
                               t.Description = 'Date : '+System.Now().format('dd/MM/yyyy hh:mm a')+'\nFeedback from nbn : \n'+strMap.get('comments');
                         }
                         tasksToUpdate.add(t);
                         Task newTsk = t.clone();
                         newTsk.Deliverable_Status__c = 'Pending Documents';
                                 
                                 //MSEU-10110 story change start.
                                 
                         if(newTsk.Sub_Type__c =='Master Plan' ||
                             newTsk.Sub_Type__c =='Pit & Pipe Design' || 
                             newTsk.Sub_Type__c =='Stage Plan' ||
                             newTsk.Sub_Type__c =='Service Plan'||
                             newTsk.Sub_Type__c =='As-built Design')
                         {
                             BusinessHours bh ; 
                             for (BusinessHours BusinessHour : [SELECT Id FROM BusinessHours WHERE IsDefault=true limit 1] ) {      
                                 bh = BusinessHour;
                             } 
                             DateTime myDateTime = BusinessHours.add(bh.Id, system.now(), (Long)(10 * 60 * 60 * 8 * 1000)); 
                             
                             newTsk.Due_Date__c =  myDateTime.date();
                             
                         }
                         else
                         {
                             if((stageAppList.get(strMap.get('applicationId')).Relationship_Manager__c!=null) && (stageAppList.get(strMap.get('applicationId')).Relationship_Manager__r.isActive == true)){
                             newTsk.OwnerId = stageAppList.get(strMap.get('applicationId')).Relationship_Manager__c;
                             }
                             else{
                                 newTsk.OwnerId = identifierList.Unassigned_User_Id__c;
                                 newTsk.Team__c = identifierList.Back_Office_Team_BOT_Id__c;
                               }
                             
                         }//MSEU-10110 story change end.
                                 
                         newTsk.Cycle__c = t.Cycle__c+1;                       
                         
                         system.debug('@newTsk==='+newTsk);
                         tasksToInsert.add(newTsk);
                     }
                 }
               }            
         }
         system.debug('@tasksToUpdate=='+tasksToUpdate);
         system.debug('@tasksToInsert=='+tasksToInsert);
         if(tasksToUpdate!=null && (!tasksToUpdate.isEmpty()))
            update tasksToUpdate;
         if(tasksToInsert!=null && (!tasksToInsert.isEmpty()))
            insert tasksToInsert;       
     }
     //Remediation fail events
     public static void remediationTasksOnApplication(List<Developments__e> remedEventList){
         list<Task> remidiationTasks = new list<TasK>();
         list<Task> tskList = new list<Task>();
         set<Id> stageAppIds = new set<Id>();
         for(Developments__e evt : remedEventList){
            Map<String,String> evtBodyMap = new Map<String,String>(); 
            List<String> bodyList = evt.Message_Body__c.split(',');
            for(String s : bodyList){
               List<String> strVal = s.split(':');
               evtBodyMap.put(strVal[0],strVal[1]);
            }
            system.debug('@evtBodyMap==='+evtBodyMap);
            
            if(evtBodyMap.containskey('applicationId')){
              stageAppIds.add(evtBodyMap.get('applicationId'));
            }
         }
         Map<Id,stage_Application__c> stageAppList= new Map<Id,stage_Application__c>([Select Id, Relationship_Manager__c,Relationship_Manager__r.isActive from Stage_Application__c where Id in: stageAppIds]);
         Map<Id, List<Task>> taskOnApp = new Map<Id, List<Task>>();
         for(Task tsk : [Select Id,WhatId,Subject,Status,Description,Deliverable_Status__c,Type_Custom__c,Sub_Type__c,Team__c,OwnerId,RecordTypeId,Cycle__c from Task where (RecordTypeId =: taskRecTypId) AND (WhatId in : stageAppIds)]){
             system.debug('@tsk@==='+tsk);
             if(taskOnApp.containsKey(tsk.WhatId)) {
                 taskOnApp.get(tsk.WhatId).add(tsk);                
             } else{
                 taskOnApp.put(tsk.WhatId, new List<Task>{tsk});
             }
           
         }
         for(Developments__e evt : remedEventList){
            Map<String,String> strMap = new Map<String,String>(); 
            List<String> strList = evt.Message_Body__c.split(',');
            
            for(String s : strList){
               List<String> strVal = s.split(':');
               strMap.put(strVal[0],strVal[1]);
            }
             if(strMap.containskey('applicationId')){
                 if(taskOnApp.get(strMap.get('applicationId')) != Null){
                     tskList = taskOnApp.get(strMap.get('applicationId'));
                     for(Task t : tskList){
                         if(t.Type_Custom__c == 'Remediation Report Created - Fail' &&
                             t.Status == 'Closed'){
                                 remidiationTasks.add(t);
                            }
                     }                     
                 }
                     String s1 = strMap.get('DocUrl') <> null?strMap.get('DocUrl').substringAfter('files/'): Null;
                     string docId = s1<>Null? s1.substringBefore('/content'): Null;
                     if(remidiationTasks.size()<=1){
                         Task drnewTsk = new Task();
                         drnewTsk.Type_Custom__c = 'Remediation Report Created - Fail';
                         drnewTsk.Status = 'Open';
                         drnewTsk.Subject = 'Remediation Report Fail';
                         drnewTsk.WhatId = Id.valueOf(strMap.get('applicationId'));
                         drnewTsk.RecordTypeId = taskRecTypId;
                        // drnewTsk.OwnerId = identifierList.Unassigned_User_Id__c;
                        if((stageAppList.get(strMap.get('applicationId')).Relationship_Manager__c!=null) && (stageAppList.get(strMap.get('applicationId')).Relationship_Manager__r.isActive == true)){
                                drnewTsk.OwnerId = stageAppList.get(strMap.get('applicationId')).Relationship_Manager__c;
                             }
                             else{
                                 drnewTsk.OwnerId = identifierList.Unassigned_User_Id__c;
                                 drnewTsk.Team__c = identifierList.Back_Office_Team_BOT_Id__c;
                               }
                         drnewTsk.Cycle__c = remidiationTasks.size()+1;
                         if(strMap.containskey('comments')){
                             drnewTsk.Description = 'Date : '+System.Now().format('dd/MM/yyyy hh:mm a')+'\nFeedback from nbn : \n'+strMap.get('comments');
                           }                      
                         insert drnewTsk;
                         if(docId <> NUll){
                             createDocumentLink(Id.valueof(docId), drnewTsk.Id);                     
                             createDocumentLink(Id.valueof(docId), Id.valueOf(strMap.get('applicationId')));
                         }                         
                     }
                     else if(remidiationTasks.size()== 2){
                             Task defectJeopardyTask = new Task();
                             defectJeopardyTask.Status = 'Open';
                             defectJeopardyTask.Type_Custom__c = 'Defects Jeopardy';
                             defectJeopardyTask.Subject = 'Defects Jeopardy';
                             defectJeopardyTask.WhatId = Id.valueOf(strMap.get('applicationId'));
                             defectJeopardyTask.RecordTypeId = taskRecTypId;
                             if((stageAppList.get(strMap.get('applicationId')).Relationship_Manager__c!=null) && (stageAppList.get(strMap.get('applicationId')).Relationship_Manager__r.isActive == true)){
                                defectJeopardyTask.OwnerId = stageAppList.get(strMap.get('applicationId')).Relationship_Manager__c;
                             }
                             else{
                                 defectJeopardyTask.OwnerId = identifierList.Unassigned_User_Id__c;
                                 defectJeopardyTask.Team__c = identifierList.Back_Office_Team_BOT_Id__c;
                               }
                             insert defectJeopardyTask;
                             if(docId <> NUll){
                                 createDocumentLink(Id.valueof(docId), defectJeopardyTask.Id);                     
                                 createDocumentLink(Id.valueof(docId), Id.valueOf(strMap.get('applicationId')));
                             }
                             

                         }

             }
             
         }
         
     }
     @future
     public static void createDocumentLink(Id DocumentId, Id LinkedId){
         List<ContentDocumentLink> dlink = [Select Id From ContentDocumentLink where ContentDocumentId =: DocumentId AND LinkedEntityId=:LinkedId];
         if(dlink.isEmpty()){
             ContentDocumentLink cdlNew = new ContentDocumentLink();
             cdlNew.ContentDocumentId = DocumentId;
             cdlNew.LinkedEntityId  = LinkedId; 
             cdlNew.ShareType ='V';
             cdlNew.Visibility = 'AllUsers';
             insert cdlNew;
         }         
     }
 }