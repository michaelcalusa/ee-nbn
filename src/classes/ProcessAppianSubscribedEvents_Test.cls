/*
@Auther : Anjani Tiwari
@Date : 21/Dec/2017
@Description : Test Class for class ProcessAppianSubscribedEvents

Modification Log    :
* Developer             Date            Description
* Surya Reddy          8-MAR-2018       Added Code coverage for Assessment & Rejected Events from Appian.
* ----------------------------------------------------------------------------                 

*/
@isTest
Public class ProcessAppianSubscribedEvents_Test{
    
    static Stage_Application__c stgApp;
    static Stage_Application__c stgApp1;
    static Stage_Application__c stgApp2;
    static List<Task> tskList = new List<Task>();
    
    
    Static {
        
        
        New_Dev_Platform_Events__c settings = New_Dev_Platform_Events__c.getOrgDefaults();
        settings.Additional_Information_Required__c = 'AP-SF-000008';
        settings.Application_was_a_duplicate__c = 'AP-SF-000006';
        settings.Application_commercially_not_viable__c = 'AP-SF-000007';
        settings.Address_Info_cannot_be_confirmed__c = 'AP-SF-000010';
        settings.LASE_Risk_is_Identified__c = 'AP-SF-000014';
        settings.Premise_Count_Mismatch__c = 'AP-SF-000003';
        settings.Location_RFSed__c ='AP-SF-000013';
        settings.Area_Planning_Assessment_Assessed__c = 'AP-SF-000002';
        settings.Area_Planning_Assessment_Rejected__c = 'AP-SF-000001';
        settings.Inbound_Delivery_Time_Impacted__c ='AP-SF-000012';
        settings.Remediation_Report_Fail__c = 'AP-SF-000016';
        settings.Remediation_Report_Pass__c = 'AP-SF-000015';
        settings.As_built_Design_Rejected__c = 'AP-SF-000023';
        settings.Service_Plan_Accepted__c = 'AP-SF-000028';
        settings.Additional_Document_Required__c = 'AP-SF-000009';
        settings.Additional_Document_Accepted__c = 'AP-SF-000026';
        settings.Additional_Document_Rejected__c= 'AP-SF-000027';
        settings.Appian_Application_Reference_No__c= 'AP-SF-000032';
        settings.Application_On_Hold__c= 'AP-SF-000033';
        settings.Application_Resumed__c= 'AP-SF-000034';
        settings.Unassigned_User_Id__c = UserInfo.getUserId();
        upsert settings New_Dev_Platform_Events__c.Id;
        
        Account a = new Account(Name='TestAccount');
        insert a;
        Contact c = new Contact (AccountId= a.Id, FirstName = 'firstName' , LastName = 'lastName');
        insert c;
        Development__c dev = new Development__c(Name= 'TestDev',Account__c = a.id, Primary_Contact__c = c.id, Development_ID__c = 'dev01', Suburb__c = 'Test');
        insert dev;
        
        SA_Auto_Reference_Number__c refNumber = new SA_Auto_Reference_Number__c(Name = 'SA001', Last_Sequence_Number__c = '000000000');
        insert refNumber;
        
        List<Site__c> siteRecords = TestDataUtility.createSiteTestRecords(2,true);
        
        stgApp = new Stage_Application__c(Reference_Number__c ='ref-0001',Primary_Location__c=siteRecords[0].Id,Class__c = 'Class3/4', Request_ID__c='TestRq1',  Dwelling_Type__c ='SDU Only',
                                          Development__c=dev.id,Active_Status__c ='Active',  Name = 'TestApp', Estimated_Ready_for_Service_Date__c =system.today());
        
        insert stgApp;
        stgApp = [Select Id,Application_ID__c,Application_Number__c,Address__c ,Suburb__c, Relationship_Manager__c,Relationship_Manager__r.isActive,Estimated_Ready_for_Service_Date__c from Stage_Application__c where id = : stgApp.id];       
        system.debug('@StageAppTest'+stgApp+stgApp.Application_ID__c);
        
        stgApp1 = new Stage_Application__c(Reference_Number__c ='ref-0001',Primary_Location__c=siteRecords[1].Id,Class__c = 'Class3/4', Request_ID__c='TestRq1',  Dwelling_Type__c ='SDU Only',
                                           Development__c=dev.id,Active_Status__c ='Active',  Name = 'TestApp1',Relationship_Manager__c =UserInfo.getUserId() ,Estimated_Ready_for_Service_Date__c =system.today());
        
        insert stgApp1;
        stgApp1 = [Select Id,Application_ID__c, Application_Number__c,Address__c ,Estimated_Ready_for_Service_Date__c,Relationship_Manager__c,Relationship_Manager__r.isActive from Stage_Application__c where id = : stgApp1.id];
        
        stgApp2 = new Stage_Application__c(Reference_Number__c ='ref-0001',Primary_Location__c=siteRecords[1].Id,Class__c = 'Class3/4', Request_ID__c='TestRq2',  Dwelling_Type__c ='SDU Only',
                                           Development__c=dev.id,Active_Status__c ='Active',  Name = 'TestApp2',Relationship_Manager__c =UserInfo.getUserId() ,Estimated_Ready_for_Service_Date__c =system.today());
        
        insert stgApp2;
        stgApp2 = [Select Id,Application_ID__c, Application_Number__c,Address__c ,Estimated_Ready_for_Service_Date__c,Relationship_Manager__c,Relationship_Manager__r.isActive from Stage_Application__c where id = : stgApp2.id];
        
        Task sp = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                           WhatId = stgApp.id,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Submitted' ,Type_Custom__c = 'Deliverables',Sub_Type__c ='Service Plan');
        tskList.add(sp);
        Task abd = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                            WhatId = stgApp1.id,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Submitted' ,Type_Custom__c = 'Deliverables',Sub_Type__c ='As-built Design');
        tskList.add(abd);
        Task abr = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Closed',
                            WhatId = stgApp1.id,ownerId = UserInfo.getUserId(),Deliverable_Status__c = '' ,Type_Custom__c = 'Remediation Report Created - Fail',Sub_Type__c ='');
        tskList.add(abr);
        Task abr1 = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Closed',
                            WhatId = stgApp1.id,ownerId = UserInfo.getUserId(),Deliverable_Status__c = '' ,Type_Custom__c = 'Remediation Report Created - Fail',Sub_Type__c ='');
        tskList.add(abr1);
        insert tskList;
        
        ContentVersion conVer = new ContentVersion();
        conVer.ContentLocation = 'S';
        conVer.PathOnClient='PCC_'+stgApp.Application_Number__c+'_'+stgApp.Address__c+' '+stgApp.Suburb__c+'.pdf';
        conVer.Title = 'PCC_'+stgApp.Application_Number__c+'_'+stgApp.Address__c+' '+stgApp.Suburb__c+'.pdf';
        
        conVer.VersionData = blob.valueOf('Unit.Test'); 
        insert conVer;
        Id DocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
        ProcessAppianSubscribedEvents.createDocumentLink(DocId, stgApp.id);
        
    }
    
    static testMethod void AppianSubscribedEvents_Test(){
        List<Developments__e> eventList = new List<Developments__e> ();
        Developments__e ev1 = new Developments__e(Event_Id__c='AP-SF-000014',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                  Message_Body__c='eventIdentifier:AP-SF-000014,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev1);
        Developments__e ev2 = new Developments__e(Event_Id__c='AP-SF-000006',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                  Message_Body__c='eventIdentifier:AP-SF-000014,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev2);
        Developments__e ev3 = new Developments__e(Event_Id__c='AP-SF-000008',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                  Message_Body__c='eventIdentifier:AP-SF-000014,applicationId:'+stgApp.Id+',comments:Duplicated,rfsDate:2022-12-11', Source__c='Appian');
        eventList.add(ev3);
        Developments__e ev4 = new Developments__e(Event_Id__c='AP-SF-000007',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                  Message_Body__c='eventIdentifier:AP-SF-000014,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev4);
        Developments__e ev5 = new Developments__e(Event_Id__c='AP-SF-000010',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                  Message_Body__c='eventIdentifier:AP-SF-000014,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev5);
        Developments__e ev6 = new Developments__e(Event_Id__c='AP-SF-000003',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                  Message_Body__c='eventIdentifier:AP-SF-000014,applicationId:'+stgApp.Id+',Premise_Count__c:10,Premise_Count_actual__c:15,comments:Duplicated', Source__c='Appian');
        eventList.add(ev6);
        Developments__e ev9 = new Developments__e(Event_Id__c='AP-SF-000016',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                  Message_Body__c='eventIdentifier:AP-SF-000016,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev9); 
        Developments__e ev10 = new Developments__e(Event_Id__c='AP-SF-000015',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000015,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev10);
        Developments__e ev11 = new Developments__e(Event_Id__c='AP-SF-000022',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000022,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev11);
        Developments__e ev12 = new Developments__e(Event_Id__c='AP-SF-000023',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000023,applicationId:'+stgApp1.Id+',comments:Duplicated,documentType:Insurance', Source__c='Appian');
        eventList.add(ev12);
        Developments__e ev14 = new Developments__e(Event_Id__c='AP-SF-000009',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000023,applicationId:'+stgApp1.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev14);
        Developments__e ev15 = new Developments__e(Event_Id__c='AP-SF-000026',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000023,applicationId:'+stgApp1.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev15);
        Developments__e ev16 = new Developments__e(Event_Id__c='AP-SF-000027',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000023,applicationId:'+stgApp1.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev16);
        Developments__e ev17 = new Developments__e(Event_Id__c='AP-SF-000016',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='remediation fail',Event_Type__c='remediation fail', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000016,applicationId:'+stgApp1.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev17);
        Developments__e ev18 = new Developments__e(Event_Id__c='AP-SF-000015',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='remediation pass',Event_Type__c='remediation pass', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000015,applicationId:'+stgApp1.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev18);
        Developments__e ev19 = new Developments__e(Event_Id__c='SF-SD-000006',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Remediation not complete',Event_Type__c='Remediation not complete', 
                                                   Message_Body__c='eventIdentifier:SF-SD-000006,applicationId:'+stgApp1.Id+',comments:Duplicated', Source__c='Salesforce');
        eventList.add(ev19);
        Developments__e ev20 = new Developments__e(Event_Id__c='AP-SF-000016',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='remediation fail',Event_Type__c='remediation fail', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000016,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev20);
        Developments__e ev21 = new Developments__e(Event_Id__c='AP-SF-000016',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='remediation fail',Event_Type__c='remediation fail', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000016,applicationId:'+stgApp1.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev21);
        Developments__e ev22 = new Developments__e(Event_Id__c='AP-SF-000032',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Appian reference No',Event_Type__c='Appian Id', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000032,applicationId:'+stgApp1.Id+',comments:Duplicated,appianApplicationReference:ND000022z', Source__c='Appian');
        eventList.add(ev22);
        Developments__e ev23 = new Developments__e(Event_Id__c='AP-SF-000033',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Application Status changed',Event_Type__c='On hold', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000033,applicationId:'+stgApp2.Id+',comments:onHold,estimatedRfsDate:2018-04-30Z', Source__c='Appian');
        eventList.add(ev23);
        Developments__e ev24 = new Developments__e(Event_Id__c='AP-SF-000034',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Application Status changed',Event_Type__c='Resumed', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000033,applicationId:'+stgApp.Id+',comments:Resumed,estimatedRfsDate:2018-04-30Z', Source__c='Appian');
        eventList.add(ev24);
      /*   Developments__e ev25 = new Developments__e(Event_Id__c='AP-SF-000016',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='remediation fail',Event_Type__c='remediation fail', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000016,applicationId:'+stgApp1.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev25); */
        
        
        if(eventList!=null && (!eventList.isEmpty())){
            List<Database.SaveResult> results = EventBus.publish(eventList);
            for (Database.SaveResult sr : results) {
                if (sr.isSuccess()) {
                    System.debug('Successfully published event.');
                } else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('Error returned: ' + err.getStatusCode() +   ' - ' + err.getMessage());
                    }    
                }
            }      
            
        }
        system.debug('eventList++'+ eventList);
        ProcessAppianSubscribedEvents.CreateTasksOnApplication(eventList);
        ProcessAppianSubscribedEvents.UpdateTasksOnApplication(eventList);
        ProcessAppianSubscribedEvents.remediationTasksOnApplication(eventList);
    }
    
    static testMethod void AppianSubscribedEvents_Test2(){ 
        
        
        List<Developments__e> eventList = new List<Developments__e> ();
        Developments__e ev7 = new Developments__e(Event_Id__c='AP-SF-000013',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                  Message_Body__c='eventIdentifier:AP-SF-000013,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev7);
        Developments__e ev8 = new Developments__e(Event_Id__c='AP-SF-000012',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                                  Message_Body__c='eventIdentifier:AP-SF-000012,applicationId:'+stgApp1.Id+',comments:Duplicated,rfsDate:2017-12-05', Source__c='Appian');
        eventList.add(ev8);
        Developments__e ev25 = new Developments__e(Event_Id__c='AP-SF-000016',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='remediation fail',Event_Type__c='remediation fail', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000016,applicationId:'+stgApp1.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev25);
        if(eventList!=null && (!eventList.isEmpty())){
            List<Database.SaveResult> results = EventBus.publish(eventList);
            for (Database.SaveResult sr : results) {
                if (sr.isSuccess()) {
                    System.debug('Successfully published event.');
                } else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('Error returned: ' + err.getStatusCode() +   ' - ' + err.getMessage());
                    }    
                }
            }      
            
        }
        system.debug('eventList++'+ eventList);
        ProcessAppianSubscribedEvents.CreateTasksOnApplication(eventList);
        ProcessAppianSubscribedEvents.UpdateTasksOnApplication(eventList);
        ProcessAppianSubscribedEvents.remediationTasksOnApplication(eventList);      
    }
    
    static testMethod void AppianSubscribedEvents_Test3(){ 
        
        List<Developments__e> eventList = new List<Developments__e> ();
        Developments__e ev1 = new Developments__e(Event_Id__c='AP-SF-000001',Event_Sub_Type__c='Rejection',Event_Type__c='Application Assessment Process', 
                                                  Message_Body__c='eventIdentifier:AP-SF-000001,applicationId:'+stgApp.Id+',primaryTechnologyType:FTTC,serviceDeliveryType:SD2,applicationRecommendationStatus:Rejected,comments:SD2 Application Rejected', Source__c='Appian');
        eventList.add(ev1);
        Developments__e ev2 = new Developments__e(Event_Id__c='AP-SF-000002',Event_Sub_Type__c='Assessed',Event_Type__c='Application Assessment Process', 
                                                  Message_Body__c='eventIdentifier:AP-SF-000002,applicationId:'+stgApp1.Id+',primaryTechnologyType:FTTC,serviceDeliveryType:SD2,applicationRecommendationStatus:Approved,comments:SD2 Application Approved', Source__c='Appian');
        eventList.add(ev2);
        Developments__e ev25 = new Developments__e(Event_Id__c='AP-SF-000016',Correlation_Id__c = stgApp.Application_ID__c ,Event_Sub_Type__c='remediation fail',Event_Type__c='remediation fail', 
                                                   Message_Body__c='eventIdentifier:AP-SF-000016,applicationId:'+stgApp1.Id+',comments:Duplicated', Source__c='Appian');
        eventList.add(ev25);
        if(eventList!=null && (!eventList.isEmpty())){
            List<Database.SaveResult> results = EventBus.publish(eventList);
            for (Database.SaveResult sr : results) {
                if (sr.isSuccess()) {
                    System.debug('Successfully published event.');
                } else {
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('Error returned: ' + err.getStatusCode() +   ' - ' + err.getMessage());
                    }    
                }
            }  
        }
        system.debug('eventList++'+ eventList);
        ProcessAppianSubscribedEvents.CreateTasksOnApplication(eventList);
        ProcessAppianSubscribedEvents.UpdateTasksOnApplication(eventList);
        ProcessAppianSubscribedEvents.remediationTasksOnApplication(eventList);
    }
}