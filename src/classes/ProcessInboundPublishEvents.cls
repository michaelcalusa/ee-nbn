/*------------------------------------------------------------
    Author:        Kashyap Murthy
    Company:       Contractor
    Description:   Processes the incoming JSON message from the Platform Event.
    Returns:       The record id for the account
    History
    <Date>      <Authors Name>     <Brief Description of Change> 
    ------------------------------------------------------------*/
public class ProcessInboundPublishEvents implements IProcessInboundPublishEvents{
    
    /*
       Method to process the business logic for Remedy To Salesforce Intergration.
    */
    public static void processInboundPublishEvents (List<String> inboundJSONMessageList, String integrationType){
    	//ProcessRemedyToSalesforceIntergration.processRemedyToSalesforceIntergration (inboundJSONMessageList, integrationType);
    } 
}