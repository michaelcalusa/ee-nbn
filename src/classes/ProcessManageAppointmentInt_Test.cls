/***************************************************************************************************
Class Name:  ProcessManageAppointmentIntergration_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 30/05/2018
Function    : Test script used to cover ProcessManageAppointmentIntergration class
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
  Murali Krishna              30/05/2018             test class used to cover unit test of
                                                     ProcessManageAppointmentIntergration class
****************************************************************************************************/
@isTest()
private class ProcessManageAppointmentInt_Test {

    @testSetup static void loadTestDataForManageAppointmentClass() 
    {
         ID recordTypeId = schema.sobjecttype.Incident_Management__c.getrecordtypeinfosbyname().get('Incident Management FTTN/B').getRecordTypeId();
         List<Incident_Management__c> lstIncidentManagementRecords = new List<Incident_Management__c>();
         Incident_Management__c incidentManagementRecord;
         //Insert Engagement Inbound Integration Record;
         EngagementInboundIntegration__c engagementInboundRecord = new EngagementInboundIntegration__c();
         engagementInboundRecord.Name = 'EngagementAppointmentMapping';
         engagementInboundRecord.Child_Json_Field_Key__c = 'appointmentId';
         engagementInboundRecord.ChildObjectNode__c = 'metadata';
         engagementInboundRecord.Has_Child_Object_Key_For_Processing__c = true;
         engagementInboundRecord.IntegrationType__c = 'EngagementInboundIntegration';
         engagementInboundRecord.ParenJsonIncidentKeyField__c = 'nbnReferenceId';
         engagementInboundRecord.Parent_Json_Field_Key__c = 'appointmentStatus';
         engagementInboundRecord.Parent_Json_Key__c = 'data';
         engagementInboundRecord.TransactionStatusJsonKey__c = 'transactionStatus';
         engagementInboundRecord.ExceptionJsonChildFieldKey__c = 'exception';
         engagementInboundRecord.ExceptionJsonChildErrorNodeKey__c = 'code';
         engagementInboundRecord.ExceptionJsonChildErrorDescKey__c = 'message';
         Insert engagementInboundRecord;
         //Insert Incident Management Records
         for(Integer i = 0; i < 10; i++)
         {
             incidentManagementRecord = new Incident_Management__c();
             incidentManagementRecord.Name = 'INC0000025985' + i;
             incidentManagementRecord.RecordTypeId = recordTypeId;
             incidentManagementRecord.assignee__c = 'TestUser';
             incidentManagementRecord.AppointmentId__c = 'APT10000053317' + i;
             incidentManagementRecord.User_Action__c = 'resolveIncident';
             incidentManagementRecord.MaximoEngagementActionPending__c = true;
             if(i / 2 == 0)
                 incidentManagementRecord.Engagement_Type__c = 'Commitment';
             else
                 {
                     incidentManagementRecord.Engagement_Type__c = 'Appointment';
                 }
             lstIncidentManagementRecords.Add(incidentManagementRecord); 
         }
         if(lstIncidentManagementRecords != null && !lstIncidentManagementRecords.Isempty())
             Insert lstIncidentManagementRecords;
    }
    Static testMethod void testManageAppointmentProcess()
    {
        StaticResource resource = [SELECT Body FROM StaticResource WHERE Name='MaximoAppointmentCancellation'];
        Blob listOfJsons = resource.Body;
        List<String> listOfrecords = listOfJsons.toString().split(';');
        List<ManageAppointment__e> lstManagementAppointments = new List<ManageAppointment__e>();
        ManageAppointment__e manageAppointmentRecord;
        for(string currentJson : listOfrecords)
        {
            manageAppointmentRecord = new ManageAppointment__e();
            manageAppointmentRecord.InboundJSONMessage__c = currentJson;
            lstManagementAppointments.Add(manageAppointmentRecord);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstManagementAppointments);
        Test.StopTest();
    }
}