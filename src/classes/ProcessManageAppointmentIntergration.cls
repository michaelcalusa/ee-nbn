/*------------------------------------------------------------
Author:        Murali Krishna
Company:       NBN CO
Description:   Class helps to process the Inbound JSON Messages coming as part of Platform Event
1. Method DeSerialises the JSON and parse the object as a Map
2. Method reads the Custom Setting Object based on the Integration Type and get 
all Salesforce Fields and JSON Key. A Map of Key, value will be created 
3. Method reads the Map and Converts the map of Salesforce Field as Key and Value 
from the Previous Map
4. A Generic SObject of an ObjectTyoe is created and Persisted.
Returns:       The record id for the account
History
<Date>      <Authors Name>          <Brief Description of Change> 
            Murali Krishna          Created
            Syed Moosa Nazir TN     Added Try-catch to handle exception
------------------------------------------------------------*/
public class ProcessManageAppointmentIntergration{
    public static void ProcessManageAppointmentIntergration(List<String> inboundJSONMessageList, String integrationType) {
        try {
            //Query List Of Index Fields Based On The Integration Type
            List<EngagementInboundIntegration__c> lstEngagementInboundIntegrationSettings = CustomSettingsUtility.getAllEngagementInboundIntegrationSettings(integrationType);
            Map<String,Object> platformEventData;
            Map<String,Object> appointmentMasterData;
            Map<String,Object> appointmentMetadata;
            Map<String,Object> appointmentExceptiondata;
            Map<String,Object> appointmentErrordata;
            String currentWorkingRecordAppoitmentStatus;
            String currentWorkingRecordAppoitmentId;
            String currentWorkingIncidentNumber;
            //List to store appointmentids which is used for querying the Incident Records for updating
            List<String> lstAppointmentIds = new List<String>();
            //List to Store Appointments for which transaction status is error.
            Map<String,List<String>> lstErrorAppointmentsMap = new Map<String,List<String>>();
            //List to store the error codes that gets triggered from cancel appointment api callout.
            List<String> errorCodes; 
            //List to store the Incident Management Records for updation
            List<Incident_Management__c> lstUpdateIncidentManagementRecords = new List<Incident_Management__c>();
            //List to store the Error Incident Management Records for updation
            List<Incident_Management__c> lstUpdateErrorIncidentManagementRecords = new List<Incident_Management__c>();
            //Variable to refer current working incident record.
            Incident_Management__c currentIncidentManagementRecord;
            //List to store the feed items that needs to be inserted when user perform cancel appointment using resolveIncident.(Cancel Appointment is triggerd when the appointment/Commitment is in reserved and user performs ResolveIncident)
            List<FeedItem> lstFeedItems;
            //List to store custom label parameters;
            List<String> customLabelParameters;
            FeedItem currentWorkingFeedItem;
            for (String inboundPlatformEvent : inboundJSONMessageList) {
              //Deserialize the values from JSON 
              platformEventData = (Map <String,Object>)JSON.deserializeUntyped(inboundPlatformEvent);
              for(EngagementInboundIntegration__c engagementinboundIntegrationSetting : lstEngagementInboundIntegrationSettings) {
                  if(platformEventData != NULL)
                  { 
                      if(!((String)platformEventData.get(engagementinboundIntegrationSetting.TransactionStatusJsonKey__c)).equalsIgnoreCase('Error'))
                      {
                          currentWorkingRecordAppoitmentStatus =  (String)platformEventData.get(engagementinboundIntegrationSetting.Parent_Json_Field_Key__c);
                          if(currentWorkingRecordAppoitmentStatus.EqualsIgnoreCase('CANCELLED') && engagementinboundIntegrationSetting.Has_Child_Object_Key_For_Processing__c){
                              appointmentMetadata =  (Map<String,Object>)platformEventData.get(engagementinboundIntegrationSetting.ChildObjectNode__c);
                              currentWorkingRecordAppoitmentId = (String)appointmentMetadata.get(engagementinboundIntegrationSetting.Child_Json_Field_Key__c);
                              if(!lstAppointmentIds.Contains(currentWorkingRecordAppoitmentId))
                                  lstAppointmentIds.Add(currentWorkingRecordAppoitmentId);    
                          }
                      }
                      else
                      {
                          appointmentExceptiondata = (Map<String,Object>)platformEventData.get(engagementinboundIntegrationSetting.ExceptionJsonChildFieldKey__c); 
                          List<Map<String, Object>> errorMaps = new List<Map<String, Object>>();
                          List<Object> myMapObjects = (List<Object>)platformEventData.get('errors');
                          if(myMapObjects != null && !myMapObjects.isEmpty())
                          {
                              for (Object obj : myMapObjects) {
                                  errorMaps.add((Map<String, Object>)obj);
                              }
                          }
                          appointmentMetadata =  (Map<String,Object>)platformEventData.get(engagementinboundIntegrationSetting.ChildObjectNode__c);
                          currentWorkingRecordAppoitmentId = (String)appointmentMetadata.get(engagementinboundIntegrationSetting.Child_Json_Field_Key__c);            
                          appointmentExceptiondata = (Map<String,Object>)platformEventData.get(engagementinboundIntegrationSetting.ExceptionJsonChildFieldKey__c); 
                          errorCodes = new List<String>();
                          if(appointmentExceptiondata != Null)
                          {
                              errorCodes.Add((String)appointmentExceptiondata.get(engagementinboundIntegrationSetting.ExceptionJsonChildErrorNodeKey__c));
                              errorCodes.Add((String)appointmentExceptiondata.get(engagementinboundIntegrationSetting.ExceptionJsonChildErrorDescKey__c));
                          }
                          if(errorMaps != Null && !errorMaps.isEmpty())
                          {
                              errorCodes.Add((String)errorMaps[0].get('errorCode'));
                              errorCodes.Add((String)errorMaps[0].get('errorMessage'));
                          }
                          if(!lstErrorAppointmentsMap.keySet().Contains(currentWorkingRecordAppoitmentId))
                          {
                              lstErrorAppointmentsMap.put(currentWorkingRecordAppoitmentId,errorCodes);
                          }
                      }
                  }
              }    
            }
            //Update User Actions Of Incident Management Record
            if(lstAppointmentIds != NULL && !lstAppointmentIds.isEmpty()) {
              lstFeedItems = new List<FeedItem>();
              List<String> feedItemMessage;
              for(Incident_Management__c incidentManagementRecord : [Select Id,User_Action__c,Engagement_Type__c,AppointmentId__c,MaximoEngagementActionPending__c,Add_Note_Change__c From Incident_Management__c Where AppointmentId__c In :lstAppointmentIds]) {
                  //Check if the cancellation is due to change EUET operation actions then update the associated feilds. 
                  currentIncidentManagementRecord = new Incident_Management__c(Id = incidentManagementRecord.Id);
                  if(incidentManagementRecord.MaximoEngagementActionPending__c)
                  {
                    currentIncidentManagementRecord.MaximoEngagementActionPending__c = false;
                    currentIncidentManagementRecord.AppointmentId__c = NULL;
                    currentIncidentManagementRecord.RandomAction__c = Math.round(Math.random()*1000);
                    currentIncidentManagementRecord.Awaiting_Current_Incident_Status__c = true;
                    if(!(String.isBlank(incidentManagementRecord.Engagement_Type__c)) && incidentManagementRecord.Engagement_Type__c.EqualsIgnoreCase('Commitment')) {
                        currentIncidentManagementRecord.User_Action__c = 'changeEUETAppointment';
                        currentIncidentManagementRecord.reasonCode__c  = Label.RSPNotesPendingCode;
                        currentIncidentManagementRecord.Awaiting_Current_SLA_Status__c = true;
                    }
                    if(!(String.isBlank(incidentManagementRecord.Engagement_Type__c)) && incidentManagementRecord.Engagement_Type__c.EqualsIgnoreCase('Appointment')) {
                        currentIncidentManagementRecord.User_Action__c = 'changeEUETCommitment';
                    }
                 }
                 //Check if the cancellation is due to Resolve Incident operation then post a chatter post with appointment/commitment details. 
                 if(!(String.isBlank(incidentManagementRecord.User_Action__c)) && incidentManagementRecord.User_Action__c.Equals('resolveIncident'))
                 {
                     currentIncidentManagementRecord.Add_Note_Change__c = incidentManagementRecord.Add_Note_Change__c ? false : true; 
                     //Set Custom Label Variables.
                     customLabelParameters = new List<String>();
                     customLabelParameters.Add(incidentManagementRecord.Engagement_Type__c);
                     customLabelParameters.Add('Successful');
                     customLabelParameters.Add(incidentManagementRecord.Engagement_Type__c);
                     customLabelParameters.Add(incidentManagementRecord.AppointmentId__c);
                     //initialize Feed Item Record
                     currentWorkingFeedItem = new FeedItem();
                     feedItemMessage = (String.format(System.Label.ResolveIncidentCancelAppointmentMessage,customLabelParameters)).Split(';');
                     System.Debug(feedItemMessage);
                     currentWorkingFeedItem.body = feedItemMessage[0] + '\r\n' + feedItemMessage[1];
                     currentWorkingFeedItem.NetworkScope = 'AllNetworks';
                     currentWorkingFeedItem.ParentId = incidentManagementRecord.Id;
                     currentWorkingFeedItem.Status = 'Published';
                     currentWorkingFeedItem.Visibility = 'InternalUsers';
                     currentWorkingFeedItem.Type = 'TextPost';
                     lstFeedItems.Add(currentWorkingFeedItem);           
                 }
                 //Add current incident management record to list for updation.
                 lstUpdateIncidentManagementRecords.add(currentIncidentManagementRecord);
              }
              //Update Incidentmanagement Records;
              if(lstUpdateIncidentManagementRecords != NULL && !lstUpdateIncidentManagementRecords.IsEmpty()) {
                  UPDATE lstUpdateIncidentManagementRecords;
              }
              //Insert Chatter Feeds
              if(lstFeedItems != NULL && !lstFeedItems.IsEmpty()) {
                  Insert lstFeedItems;  
              }
            }
            //Update Error Codes In Incident Management Record
            if(lstErrorAppointmentsMap != Null && !lstErrorAppointmentsMap.keySet().isEmpty())
            {
                lstFeedItems = new List<FeedItem>();
                List<String> feedItemMessage;
                for(Incident_Management__c incidentManagementRecord : [Select Id,User_Action__c,Engagement_Type__c,AppointmentId__c,MaximoEngagementActionPending__c,Add_Note_Change__c From Incident_Management__c Where AppointmentId__c In :lstErrorAppointmentsMap.keySet()]) {
                    //Check if the cancellation is due to change EUET operation actions then update the associated feilds. 
                    currentIncidentManagementRecord = new Incident_Management__c(Id = incidentManagementRecord.Id);
                    if(incidentManagementRecord.MaximoEngagementActionPending__c)
                    {
                        currentIncidentManagementRecord.CancelAppointmentErrorCode__c = (lstErrorAppointmentsMap.get(incidentManagementRecord.AppointmentId__c))[0];
                        currentIncidentManagementRecord.CancelAppointmentErrorMessage__c = (lstErrorAppointmentsMap.get(incidentManagementRecord.AppointmentId__c))[1];
                    }
                    //Check if the cancellation is due to Resolve Incident operation actions then post with appointment/commitment details. 
                    if(!(String.isBlank(incidentManagementRecord.User_Action__c)) && incidentManagementRecord.User_Action__c.Equals('resolveIncident'))
                    {
                        currentIncidentManagementRecord.Add_Note_Change__c = incidentManagementRecord.Add_Note_Change__c ? false : true; 
                        customLabelParameters = new List<String>();
                        customLabelParameters.Add(incidentManagementRecord.Engagement_Type__c);
                        customLabelParameters.Add('Failed');
                        customLabelParameters.Add(incidentManagementRecord.Engagement_Type__c);
                        customLabelParameters.Add(incidentManagementRecord.AppointmentId__c);
                        currentWorkingFeedItem = new FeedItem();
                        feedItemMessage = (String.format(System.Label.ResolveIncidentCancelAppointmentMessage,customLabelParameters)).Split(';');
                        currentWorkingFeedItem.body = feedItemMessage[0] + '\r\n' + feedItemMessage[1];
                        currentWorkingFeedItem.NetworkScope = 'AllNetworks';
                        currentWorkingFeedItem.ParentId = incidentManagementRecord.Id;
                        currentWorkingFeedItem.Status = 'Published';
                        currentWorkingFeedItem.Visibility = 'InternalUsers';
                        currentWorkingFeedItem.Type = 'TextPost';
                        lstFeedItems.Add(currentWorkingFeedItem);           
                    }
                    //Add current incident management record to list for updation.
                    lstUpdateErrorIncidentManagementRecords.Add(currentIncidentManagementRecord); 
               }
               //Update Incidentmanagement Records;
               if(lstUpdateErrorIncidentManagementRecords != NULL && !lstUpdateErrorIncidentManagementRecords.IsEmpty()){
                   UPDATE lstUpdateErrorIncidentManagementRecords;
               }
               //Insert Chatter Feeds
               if(lstFeedItems != NULL && !lstFeedItems.IsEmpty()) {
                   Insert lstFeedItems;  
               }       
            }
            if(test.IsrunningTest())
                throw new applicationException('CustomException');
        }
        catch (Exception ex) {
            System.Debug(ex.getMessage());
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'ProcessManageAppointmentIntergration','ProcessManageAppointmentIntergration','','',ex.getMessage(), ex.getStackTraceString(),ex, 0);
        }
    }
    public class applicationException extends Exception {}
}