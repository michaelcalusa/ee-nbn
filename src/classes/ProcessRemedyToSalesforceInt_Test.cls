/***************************************************************************************************
Class Name:  ProcessRemedyToSalesforceInt_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 10/10/2018
Function    : Test script used to cover ProcessRemedyToSalesforceIntegration class
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
  Murali Krishna              10/10/2018             test class used to cover unit test of
                                                     ProcessRemedyToSalesforceIntegration class
****************************************************************************************************/
@isTest()
private class ProcessRemedyToSalesforceInt_Test
{
    @testSetup static void loadTestDataForProcessRemedyToSalesforceInt() 
    {
        //Load Inbound Platform Event Map Custom Settings From Static Resource
        List<SObject> inboundPlatformEventMapSettings = Test.loadData(InboundPlatformEventMap__c.sObjectType,'InboundPlatformEventMapCustomSettingsDataFile');
        //Load Remedy To Salesforce Field Mapping Custom Settings From Static Resource.
        List<SObject> remedyToSalesforceJsonCustomSettings = Test.loadData(RemedyIncidentDetailsJSONtoSFFields__c.sObjectType,'RemedyToSalesforceJsonSettings');
        //Create Test Accounts In Salesforce For Linking Access Seeker Ids;
        //Load Record Type of Account Object related to Jigsaw Environment.
        ID testAccountRecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get(GlobalConstants.RSP_RECTYPE_NAME).getRecordTypeId();
        //List To Store List Of accounts that needs to be inserted for testing.
        List<Account> lstTestAccounts = New List<Account>();
        Account testAccount;
        Integer accessSeekerCounter = 1074;
        String accessSeekerId;
        for(Integer i = 0;i < 10; i++)
        {
            accessSeekerCounter += i;
            accessSeekerId  = 'ASI00000000' + accessSeekerCounter;
            testAccount = New Account();
            testAccount.Name = 'TPG Internet Pty Limited' + i;
            testAccount.recordTypeId = testAccountRecordTypeId;
            testAccount.Tier__c = '1';
            testAccount.Access_Seeker_ID__c = accessSeekerId;
            lstTestAccounts.Add(testAccount);
        }
        if(lstTestAccounts != Null && !lstTestAccounts.isEmpty())
            Insert lstTestAccounts;
    }
    Static testMethod void testProcessRemedyToSalesforceChannel1Success()
    {
        StaticResource remedyToSalesforceSampleJsonResource = [SELECT Body FROM StaticResource WHERE Name='RemedyToSalesforcIntegrationSampleJsons'];
        Blob listOfJsons = remedyToSalesforceSampleJsonResource.Body;
        List<String> listOfSampleJsons = listOfJsons.toString().split(';');
        List<RemedyInboundIntegration__e> lstRemedyToSalesforceJsonEvents = new List<RemedyInboundIntegration__e>();
        RemedyInboundIntegration__e remedyToSalesforceJsonEvent;
        for(Integer i = 0; i < 10; i++)
        {
            remedyToSalesforceJsonEvent = New RemedyInboundIntegration__e();
            remedyToSalesforceJsonEvent.InboundJSONMessage__c = listOfSampleJsons[i];
            lstRemedyToSalesforceJsonEvents.add(remedyToSalesforceJsonEvent);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstRemedyToSalesforceJsonEvents);
        Test.StopTest();
        //Assertion Section;
        List<Incident_Management__c> lstManagementRecords = [Select Id From Incident_Management__c];
        System.AssertEquals(3,lstManagementRecords.Size());
    }
    Static testMethod void testProcessRemedyToSalesforceChannel2Success()
    {
        StaticResource remedyToSalesforceSampleJsonResource = [SELECT Body FROM StaticResource WHERE Name='RemedyToSalesforcIntegrationSampleJsons'];
        Blob listOfJsons = remedyToSalesforceSampleJsonResource.Body;
        List<String> listOfSampleJsons = listOfJsons.toString().split(';');
        List<RemedyInboundIntegrationChannel2__e> lstRemedyToSalesforceJsonEvents = new List<RemedyInboundIntegrationChannel2__e>();
        RemedyInboundIntegrationChannel2__e remedyToSalesforceJsonEvent;
        for(Integer i = 9; i < 16; i++)
        {
            remedyToSalesforceJsonEvent = New RemedyInboundIntegrationChannel2__e();
            remedyToSalesforceJsonEvent.InboundJSONMessage__c = listOfSampleJsons[i];
            lstRemedyToSalesforceJsonEvents.add(remedyToSalesforceJsonEvent);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstRemedyToSalesforceJsonEvents);
        Test.StopTest();
        //Assertion Section;
        List<Incident_Management__c> lstManagementRecords = [Select Id From Incident_Management__c];
        System.AssertEquals(7,lstManagementRecords.Size());
    }
    Static testMethod void testProcessRemedyToSalesforceChannel3Success()
    {
        StaticResource remedyToSalesforceSampleJsonResource = [SELECT Body FROM StaticResource WHERE Name='RemedyToSalesforcIntegrationSampleJsons'];
        Blob listOfJsons = remedyToSalesforceSampleJsonResource.Body;
        List<String> listOfSampleJsons = listOfJsons.toString().split(';');
        List<RemedyInboundIntegrationChannel3__e> lstRemedyToSalesforceJsonEvents = new List<RemedyInboundIntegrationChannel3__e>();
        RemedyInboundIntegrationChannel3__e remedyToSalesforceJsonEvent;
        for(Integer i = 9; i < 16; i++)
        {
            remedyToSalesforceJsonEvent = New RemedyInboundIntegrationChannel3__e();
            remedyToSalesforceJsonEvent.InboundJSONMessage__c = listOfSampleJsons[i];
            lstRemedyToSalesforceJsonEvents.add(remedyToSalesforceJsonEvent);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstRemedyToSalesforceJsonEvents);
        Test.StopTest();
        //Assertion Section;
        List<Incident_Management__c> lstManagementRecords = [Select Id From Incident_Management__c];
        System.AssertEquals(7,lstManagementRecords.Size());
    }
    Static testMethod void testProcessRemedyToSalesforceChannel4Success()
    {
        StaticResource remedyToSalesforceSampleJsonResource = [SELECT Body FROM StaticResource WHERE Name='RemedyToSalesforcIntegrationSampleJsons'];
        Blob listOfJsons = remedyToSalesforceSampleJsonResource.Body;
        List<String> listOfSampleJsons = listOfJsons.toString().split(';');
        List<RemedyInboundIntegrationChannel4__e> lstRemedyToSalesforceJsonEvents = new List<RemedyInboundIntegrationChannel4__e>();
        RemedyInboundIntegrationChannel4__e remedyToSalesforceJsonEvent;
        for(Integer i = 9; i < 16; i++)
        {
            remedyToSalesforceJsonEvent = New RemedyInboundIntegrationChannel4__e();
            remedyToSalesforceJsonEvent.InboundJSONMessage__c = listOfSampleJsons[i];
            lstRemedyToSalesforceJsonEvents.add(remedyToSalesforceJsonEvent);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstRemedyToSalesforceJsonEvents);
        Test.StopTest();
        //Assertion Section;
        List<Incident_Management__c> lstManagementRecords = [Select Id From Incident_Management__c];
        System.AssertEquals(7,lstManagementRecords.Size());
    }
    Static testMethod void testProcessRemedyToSalesforceChannel5Success()
    {
        StaticResource remedyToSalesforceSampleJsonResource = [SELECT Body FROM StaticResource WHERE Name='RemedyToSalesforcIntegrationSampleJsons'];
        Blob listOfJsons = remedyToSalesforceSampleJsonResource.Body;
        List<String> listOfSampleJsons = listOfJsons.toString().split(';');
        List<RemedyInboundIntegrationChannel5__e> lstRemedyToSalesforceJsonEvents = new List<RemedyInboundIntegrationChannel5__e>();
        RemedyInboundIntegrationChannel5__e remedyToSalesforceJsonEvent;
        for(Integer i = 9; i < 16; i++)
        {
            remedyToSalesforceJsonEvent = New RemedyInboundIntegrationChannel5__e();
            remedyToSalesforceJsonEvent.InboundJSONMessage__c = listOfSampleJsons[i];
            lstRemedyToSalesforceJsonEvents.add(remedyToSalesforceJsonEvent);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstRemedyToSalesforceJsonEvents);
        Test.StopTest();
        //Assertion Section;
        List<Incident_Management__c> lstManagementRecords = [Select Id From Incident_Management__c];
        System.AssertEquals(7,lstManagementRecords.Size());
    }
    Static testMethod void testProcessRemedyToSalesforceChannel6Success()
    {
        StaticResource remedyToSalesforceSampleJsonResource = [SELECT Body FROM StaticResource WHERE Name='RemedyToSalesforcIntegrationSampleJsons'];
        Blob listOfJsons = remedyToSalesforceSampleJsonResource.Body;
        List<String> listOfSampleJsons = listOfJsons.toString().split(';');
        List<RemedyInboundIntegrationChannel6__e> lstRemedyToSalesforceJsonEvents = new List<RemedyInboundIntegrationChannel6__e>();
        RemedyInboundIntegrationChannel6__e remedyToSalesforceJsonEvent;
        for(Integer i = 9; i < 16; i++)
        {
            remedyToSalesforceJsonEvent = New RemedyInboundIntegrationChannel6__e();
            remedyToSalesforceJsonEvent.InboundJSONMessage__c = listOfSampleJsons[i];
            lstRemedyToSalesforceJsonEvents.add(remedyToSalesforceJsonEvent);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstRemedyToSalesforceJsonEvents);
        Test.StopTest();
        //Assertion Section;
        List<Incident_Management__c> lstManagementRecords = [Select Id From Incident_Management__c];
        System.AssertEquals(7,lstManagementRecords.Size());
    }
    Static testMethod void testProcessRemedyToSalesforceChannel7Success()
    {
        StaticResource remedyToSalesforceSampleJsonResource = [SELECT Body FROM StaticResource WHERE Name='RemedyToSalesforcIntegrationSampleJsons'];
        Blob listOfJsons = remedyToSalesforceSampleJsonResource.Body;
        List<String> listOfSampleJsons = listOfJsons.toString().split(';');
        List<RemedyInboundIntegrationChannel7__e> lstRemedyToSalesforceJsonEvents = new List<RemedyInboundIntegrationChannel7__e>();
        RemedyInboundIntegrationChannel7__e remedyToSalesforceJsonEvent;
        for(Integer i = 9; i < 16; i++)
        {
            remedyToSalesforceJsonEvent = New RemedyInboundIntegrationChannel7__e();
            remedyToSalesforceJsonEvent.InboundJSONMessage__c = listOfSampleJsons[i];
            lstRemedyToSalesforceJsonEvents.add(remedyToSalesforceJsonEvent);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstRemedyToSalesforceJsonEvents);
        Test.StopTest();
        //Assertion Section;
        List<Incident_Management__c> lstManagementRecords = [Select Id From Incident_Management__c];
        System.AssertEquals(7,lstManagementRecords.Size());
    }
    Static testMethod void testProcessRemedyToSalesforceChannel8Success()
    {
        StaticResource remedyToSalesforceSampleJsonResource = [SELECT Body FROM StaticResource WHERE Name='RemedyToSalesforcIntegrationSampleJsons'];
        Blob listOfJsons = remedyToSalesforceSampleJsonResource.Body;
        List<String> listOfSampleJsons = listOfJsons.toString().split(';');
        List<RemedyInboundIntegrationChannel8__e> lstRemedyToSalesforceJsonEvents = new List<RemedyInboundIntegrationChannel8__e>();
        RemedyInboundIntegrationChannel8__e remedyToSalesforceJsonEvent;
        for(Integer i = 9; i < 16; i++)
        {
            remedyToSalesforceJsonEvent = New RemedyInboundIntegrationChannel8__e();
            remedyToSalesforceJsonEvent.InboundJSONMessage__c = listOfSampleJsons[i];
            lstRemedyToSalesforceJsonEvents.add(remedyToSalesforceJsonEvent);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstRemedyToSalesforceJsonEvents);
        Test.StopTest();
        //Assertion Section;
        List<Incident_Management__c> lstManagementRecords = [Select Id From Incident_Management__c];
        System.AssertEquals(7,lstManagementRecords.Size());
    }
    Static testMethod void testProcessRemedyToSalesforceChannel9Success()
    {
        StaticResource remedyToSalesforceSampleJsonResource = [SELECT Body FROM StaticResource WHERE Name='RemedyToSalesforcIntegrationSampleJsons'];
        Blob listOfJsons = remedyToSalesforceSampleJsonResource.Body;
        List<String> listOfSampleJsons = listOfJsons.toString().split(';');
        List<RemedyInboundIntegrationChannel9__e> lstRemedyToSalesforceJsonEvents = new List<RemedyInboundIntegrationChannel9__e>();
        RemedyInboundIntegrationChannel9__e remedyToSalesforceJsonEvent;
        for(Integer i = 9; i < 16; i++)
        {
            remedyToSalesforceJsonEvent = New RemedyInboundIntegrationChannel9__e();
            remedyToSalesforceJsonEvent.InboundJSONMessage__c = listOfSampleJsons[i];
            lstRemedyToSalesforceJsonEvents.add(remedyToSalesforceJsonEvent);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstRemedyToSalesforceJsonEvents);
        Test.StopTest();
        //Assertion Section;
        List<Incident_Management__c> lstManagementRecords = [Select Id From Incident_Management__c];
        System.AssertEquals(7,lstManagementRecords.Size());
    }
    Static testMethod void testProcessRemedyToSalesforceChannel10Success()
    {
        StaticResource remedyToSalesforceSampleJsonResource = [SELECT Body FROM StaticResource WHERE Name='RemedyToSalesforcIntegrationSampleJsons'];
        Blob listOfJsons = remedyToSalesforceSampleJsonResource.Body;
        List<String> listOfSampleJsons = listOfJsons.toString().split(';');
        List<RemedyInboundIntegrationChannel10__e> lstRemedyToSalesforceJsonEvents = new List<RemedyInboundIntegrationChannel10__e>();
        RemedyInboundIntegrationChannel10__e remedyToSalesforceJsonEvent;
        for(Integer i = 9; i < 16; i++)
        {
            remedyToSalesforceJsonEvent = New RemedyInboundIntegrationChannel10__e();
            remedyToSalesforceJsonEvent.InboundJSONMessage__c = listOfSampleJsons[i];
            lstRemedyToSalesforceJsonEvents.add(remedyToSalesforceJsonEvent);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstRemedyToSalesforceJsonEvents);
        Test.StopTest();
        //Assertion Section;
        List<Incident_Management__c> lstManagementRecords = [Select Id From Incident_Management__c];
        System.AssertEquals(7,lstManagementRecords.Size());
    }
    Static testMethod void testProcessRemedyToSalesforceChannelFailureIJF()
    {
        StaticResource remedyToSalesforceSampleJsonResource = [SELECT Body FROM StaticResource WHERE Name='RemedyToSalesforcIntegrationSampleExceptionJsons'];
        Blob listOfJsons = remedyToSalesforceSampleJsonResource.Body;
        List<String> listOfSampleJsons = listOfJsons.toString().split(';');
        List<RemedyInboundIntegration__e> lstRemedyToSalesforceJsonEvents = new List<RemedyInboundIntegration__e>();
        RemedyInboundIntegration__e remedyToSalesforceJsonEvent;
        for(Integer i = 0; i < 2; i++)
        {
            remedyToSalesforceJsonEvent = New RemedyInboundIntegration__e();
            remedyToSalesforceJsonEvent.InboundJSONMessage__c = listOfSampleJsons[i];
            lstRemedyToSalesforceJsonEvents.add(remedyToSalesforceJsonEvent);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstRemedyToSalesforceJsonEvents);
        Test.StopTest();
        //Assertion Section;
        List<RemedyToSalesforceIntegrationLog__c> lstRemedySalesforcIntegrationLogs = [Select Id From RemedyToSalesforceIntegrationLog__c Where ProcessedSuccessfully__c = False];
        System.AssertEquals(2,lstRemedySalesforcIntegrationLogs.Size());
    }
    Static testMethod void testProcessRemedyToSalesforceChannelFailureDbF()
    {
        StaticResource remedyToSalesforceSampleJsonResource = [SELECT Body FROM StaticResource WHERE Name='RemedyToSalesforcIntegrationSampleExceptionJsons'];
        Blob listOfJsons = remedyToSalesforceSampleJsonResource.Body;
        List<String> listOfSampleJsons = listOfJsons.toString().split(';');
        List<RemedyInboundIntegration__e> lstRemedyToSalesforceJsonEvents = new List<RemedyInboundIntegration__e>();
        RemedyInboundIntegration__e remedyToSalesforceJsonEvent;
        for(Integer i = 2; i < 6; i++)
        {
            remedyToSalesforceJsonEvent = New RemedyInboundIntegration__e();
            remedyToSalesforceJsonEvent.InboundJSONMessage__c = listOfSampleJsons[i];
            lstRemedyToSalesforceJsonEvents.add(remedyToSalesforceJsonEvent);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstRemedyToSalesforceJsonEvents);
        Test.StopTest();
        //Assertion Section;
        List<RemedyToSalesforceIntegrationLog__c> lstRemedySalesforcIntegrationLogs = [Select Id,FailedMessageDetails__c From RemedyToSalesforceIntegrationLog__c Where ProcessedSuccessfully__c = False];
        System.AssertEquals(4,lstRemedySalesforcIntegrationLogs.Size());
    }
}