/************************************************************************************************************
Version                 : 1.0 
Created Date            : 30-10-2017
Description/Function    : This class executes the creation of Product Basket and sync with Opportunity.
Used in                 : NewDevWebFormTriggerHandler Apex Class
=============================================================================================================
Modification Log        :
-------------------------------------------------------------------------------------------------------------
* Developer                   Date                   Description
* ------------------------------------------------------------------------- ---------------------------------               
* SUKUMAR SALLA             30-10-2017             Created the Class
**************************************************************************************************************/
public class ProductBasketController {
	//public static cscfga.API_1.ApiSession apiSession;
 	//public static cscfga.SessionManager.SessionInfo session;
    public static String setProductQuantity(Integer PQty , String OpptyId, string offerId){
        cscfga__Configuration_Offer__c offer = [Select id,cscfga__Template__c from cscfga__Configuration_Offer__c where Id = :offerId];
        string newdevProductCategoryID = NewDevelopments_Settings__c.getOrgDefaults().NewDev_Product_Category_RecordId__c;
        system.debug('offer..'+ offer);
        cscfga.SessionManager.SessionInfo session = cscfga.SessionManager.getOrCreateSession('SessionKey: ' + Datetime.now(), 'User Session'); 
        cscfga.API_1.ApiSession apiSession = cscfga.API_1.getApiSession();
        apiSession.loadUserSession(session.userSession.Id);
        cscfga__Product_Basket__c basket = apiSession.getBasket();
        cscfga.ProductBasketManager.addOfferTobasket(offer, basket);
        cscfga__Product_Configuration__c prodConfig = [SELECT Id, Name, cscfga__Product_Basket__c,cscfga__Product_Bundle__c,cscfga__Configuration_Offer__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :Basket.Id LIMIT 1];
	
       
        apiSession.setconfigurationToEdit(prodConfig);
		cscfga.Attribute att = apiSession.getAttributeForCurrentConfig('Quantity');
       	att.setValue(string.valueOf(PQty));
		apiSession.executeRules();
		cscfga.ValidationResult vr = apiSession.validateConfiguration(); //ValidationResult
		system.debug('Validation results' + vr);
        apiSession.persistConfiguration();
        csbb__Product_Configuration_Request__c pcr = new csbb__Product_Configuration_Request__c();
		pcr.csbb__Product_Basket__c= basket.Id;
		pcr.csbb__Product_Configuration__c = prodConfig.Id;
		pcr.csbb__Offer__c = offerId;
		pcr.csbb__Optionals__c = '{"selectedAddressDisplay":null}';
		pcr.csbb__Product_Category__c = newdevProductCategoryID;
		pcr.csbb__Status__c='finalized';
		insert pcr;
        associateBasketWithOppty(basket.Id, OpptyId);
        String syncResults = syncWithOpportunity(basket.Id);
        system.debug('Sync results' + syncResults);
        return basket.Id;
    }
    
     public static void associateBasketWithOppty(Id BasketId, Id OpptyId){
        cscfga__Product_Basket__c BK = [Select Id, cscfga__Opportunity__c From cscfga__Product_Basket__c Where Id =: BasketId];
        Bk.cscfga__Opportunity__c = OpptyId;
        try{
            Update Bk;            
        }catch(Exception ex){GlobalUtility.logMessage('Error', 'ProductBasketSpikeController', 'associateBaskettoOppty', ' ', '', '', '', ex, 0);}
        
    }
    public static String syncWithOpportunity(Id BasketId){
        List <cscfga__Product_Configuration__c> orphanProdConfs = 
          [select c.id
          from cscfga__Product_Configuration__c c
          where 
            cscfga__product_basket__c = :basketId and
            cscfga__root_configuration__c = null and
            id not in 
              (select csbb__product_configuration__c
              from csbb__Product_Configuration_Request__c
              where csbb__product_basket__c = :basketId
              and csbb__product_configuration__c != null
              )
          ];
    
     System.debug('*** CW CustomButtons SynchronizeWithOpportunity ');
    
    if (orphanProdConfs.size() > 0) {
      delete orphanProdConfs;
    }
    
    cscfga__Product_Basket__c pb = [select id, cscfga__opportunity__c, csbb__Synchronised_with_Opportunity__c, cscfga__Basket_Status__c  
                                    from cscfga__Product_Basket__c where id = :basketId];

    if(pb.cscfga__Basket_Status__c != 'Valid') {
        return 'Synchronise with opportunity failed: product basket has to be valid.';
       }
        System.debug('*** CW PB status' + pb.cscfga__Basket_Status__c );
        
        if(pb.csbb__Synchronised_with_Opportunity__c) {
            pb.csbb__Synchronised_with_Opportunity__c=false;
            pb.csordtelcoa__Synchronised_with_Opportunity__c = false;
            update pb;
           }
        pb.csbb__Synchronised_with_Opportunity__c = true;
        pb.csordtelcoa__Synchronised_with_Opportunity__c = true;
        update pb;
        return 'sychronised with opportunity';
    }
}