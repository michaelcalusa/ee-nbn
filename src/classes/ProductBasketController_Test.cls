/*************************************************
 Author: Suman Gunaganti
 Description: Test class for ProductBasketController
 Date: 05/02/2018
**************************************************/

@IsTest
public class ProductBasketController_Test {
    static testMethod void TestProductBasketController(){
        
        Opportunity testOpty = new Opportunity(Name = 'Test Opp', CloseDate = System.today(), StageName = 'New');
        insert testOpty;
        system.assert(testOpty.Id != null);
     	
        cscfga__Product_Category__c testProdCateg = TestDataClass.createProdCateg();
        testProdCateg.Name = 'New Developments';
        testProdCateg.cscfga__Active__c = true;
        
        insert testProdCateg;
    
        cscfga__Product_Definition__c testProDef = TestDataClass.createProDef(testProdCateg);
        testProDef.Name = 'single dwelling unit';
        testProDef.csordtelcoa__Product_Type__c = 'Delivery';
        testProDef.cscfga__Active__c = true;
        
        insert testProDef;
        
        
        cscfga__Attribute_Definition__c testAttrDef = new cscfga__Attribute_Definition__c();
        testAttrDef.Name = 'Price';
        testAttrDef.cscfga__Is_Line_Item__c = true;
        testAttrDef.cscfga__Product_Definition__c = testProDef.Id;
        testAttrDef.cscfga__Data_Type__c = 'Decimal';
        testAttrDef.cscfga__Enable_null_option__c = true;
        testAttrDef.csordtelcoa__Calculate_Delta__c = true;
        
        insert testAttrDef;
        
        cscfga__Attribute_Definition__c testAttrDef_2 = new cscfga__Attribute_Definition__c();
        testAttrDef_2.Name = 'Cost';
        testAttrDef_2.cscfga__Is_Line_Item__c = false;
        testAttrDef_2.cscfga__Product_Definition__c = testProDef.Id;
        testAttrDef_2.cscfga__Data_Type__c = 'Decimal';
        testAttrDef_2.cscfga__Enable_null_option__c = true;
        
        insert testAttrDef_2;
        
        cscfga__Attribute_Definition__c testAttrDef_3 = new cscfga__Attribute_Definition__c();
        testAttrDef_3.Name = 'Margin';
        testAttrDef_3.cscfga__Is_Line_Item__c = false;
        testAttrDef_3.cscfga__Product_Definition__c = testProDef.Id;
        testAttrDef_3.cscfga__Data_Type__c = 'Decimal';
        testAttrDef_3.cscfga__Enable_null_option__c = true;
        
        insert testAttrDef_3;
        
        cscfga__Attribute_Definition__c testAttrDef_4 = new cscfga__Attribute_Definition__c();
        testAttrDef_4.Name = 'Quantity';
        testAttrDef_4.cscfga__Is_Line_Item__c = false;
        testAttrDef_4.cscfga__Product_Definition__c = testProDef.Id;
        testAttrDef_4.cscfga__Data_Type__c = 'Integer';
        testAttrDef_4.cscfga__Default_Value__c = '1';
        testAttrDef_4.cscfga__Enable_null_option__c = true;
        
        insert testAttrDef_4;
        cscfga__Attribute_Definition__c testAttrDef_5 = new cscfga__Attribute_Definition__c();
        testAttrDef_5.Name = 'Memo Line Id';
        testAttrDef_5.cscfga__Is_Line_Item__c = false;
        testAttrDef_5.cscfga__Product_Definition__c = testProDef.Id;
        testAttrDef_5.cscfga__Data_Type__c = 'String';
        testAttrDef_5.cscfga__Enable_null_option__c = true;
        
        insert testAttrDef_5;
        
        
		// Add some products to the basket
		//testProdConfig.cscfga__Product_Basket__c = testBasket.Id;
		
       cscfga__Configuration_Offer__c Offer = new cscfga__Configuration_Offer__c(Name='SDU Offer', cscfga__Active__c=TRUE, cscfga__Description__c='SDU offer desc');
        insert Offer;
        cscfga__Product_Configuration__c  testProdConfig = new cscfga__Product_Configuration__c();
        testProdConfig.cscfga__Product_Definition__c = testProDef.id;
		testProdConfig.Name = 'New Development SDU charge';
        testProdConfig.cscfga__Configuration_Offer__c = offer.ID;
        insert testProdConfig;
        system.assert(testProdConfig.Id != null);
        cscfga__Attribute__c testAttr = new cscfga__Attribute__c();
        testAttr.cscfga__Product_Configuration__c = testProdConfig.Id;
        testAttr.cscfga__Attribute_Definition__c = testAttrDef.Id;
        testAttr.Name = 'Price';
        testAttr.cscfga__Line_Item_Description__c = 'PCD Relocation'; // same as product configuration name
        testAttr.cscfga__is_active__c = true;
        testAttr.cscfga__Is_Line_Item__c = true;
        testAttr.cscfga__Discount_Type__c = 'amount';
        testAttr.cscfga__Price__c = decimal.valueOf('1322.50');
        
        insert testAttr;
        
        cscfga__Attribute__c testAttr2 = new cscfga__Attribute__c();
        testAttr2.cscfga__Product_Configuration__c = testProdConfig.Id;
        testAttr2.cscfga__Attribute_Definition__c = testAttrDef_2.Id;
        testAttr2.Name = 'Cost';
        testAttr2.cscfga__is_active__c = true;
        testAttr2.cscfga__Value__c = '1150.00';
        testAttr2.cscfga__Discount_Type__c = 'amount';
        
        insert testAttr2;
        
        cscfga__Attribute__c testAttr3 = new cscfga__Attribute__c();
        testAttr3.cscfga__Product_Configuration__c = testProdConfig.Id;
        testAttr3.cscfga__Attribute_Definition__c = testAttrDef_3.Id;
        testAttr3.Name = 'Margin';
        testAttr3.cscfga__is_active__c = true;
        testAttr3.cscfga__Value__c = '15.00';
        testAttr3.cscfga__Discount_Type__c = 'amount';
        
        insert testAttr3;
        
        cscfga__Attribute__c testAttr4 = new cscfga__Attribute__c();
        testAttr4.cscfga__Product_Configuration__c = testProdConfig.Id;
        testAttr4.cscfga__Attribute_Definition__c = testAttrDef_4.Id;
        testAttr4.Name = 'Quantity';
        testAttr4.cscfga__is_active__c = true;
        testAttr4.cscfga__Value__c = '1';
        testAttr4.cscfga__Discount_Type__c = 'amount';
        
        insert testAttr4;
        
        cscfga__Attribute__c testAttr5 = new cscfga__Attribute__c();
        testAttr5.cscfga__Product_Configuration__c = testProdConfig.Id;
        testAttr5.cscfga__Attribute_Definition__c = testAttrDef_5.Id;
        testAttr5.Name = 'Memo Line Id';
        testAttr5.cscfga__is_active__c = true;
        testAttr5.cscfga__Discount_Type__c = 'amount';
        
        insert testAttr5;

        ProductBasketController.setProductQuantity(5, testOpty.ID, Offer.ID);
     }
}