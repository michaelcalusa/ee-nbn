/***************************************************************************************************
    Class Name          : PublishPlatformEventOnTask
    Test Class          :
    Version             : 1.0 
    Created Date        : 22-Nov-2017
    Author              : Anjani Tiwari
    Description         : Publish Platform Event for file upload on stage application/Task
    Input Parameters    :

    Modification Log    :
    * Developer             Date            Description
      Anjani Tiwari        19-Mar-2018      Remediation report can be sent with attachement.
      Naseer Abbasi        23 Jan 2019      Service plan attachment to be shared with appian on Payment Received 
    * ----------------------------------------------------------------------------                 

****************************************************************************************************/ 
  public class PublishPlatformEventOnTask{
     
     Static New_Dev_Platform_Events__c identifierList = New_Dev_Platform_Events__c.getOrgDefaults();
     
     public static void PublishAttachmentOnTask(Map<Id,Task> areaPlanTaskMap){
        set<Id> taskIdSet = new set<Id>();
        Set<Id> stageAppIdSet = new Set<Id>();
        List<ContentVersion> cvListToUpdate = new List<ContentVersion>();
        List<ContentDocumentLink> cdlNewList = new List<ContentDocumentLink>();
        List<Developments__e> eventListToPublish = new  List<Developments__e>();
        Set<Id> conDocIdSet = new Set<Id>();
        Map<Id,ContentDocumentLink> cdlMapTask = new Map<Id,ContentDocumentLink>();
        Map<Id,ContentDocumentLink> cdlMapStageApp = new Map<Id,ContentDocumentLink>();
        List<Task> taskClosed = new List<Task>();
        List<Task> remidTasks = new List<Task>();
        List<Task> pcnTasklist = new List<Task>();       
        String remidDocUrls ;
        
        for(Task t : areaPlanTaskMap.Values()){
             system.debug('@taskDes@#'+t.Description);
             stageAppIdSet.add(t.WhatId);
             If(t.Deliverable_Status__c == 'Submitted')
                 taskIdSet.add(t.id);
             if(t.Status == 'Closed' && t.Type_Custom__c == 'Additional Information Required')
                 taskClosed.add(t);
             if(t.Status == 'Closed' && t.Type_Custom__c == 'Remediation Report Created - Fail'){
                 remidTasks.add(t);
                 taskIdSet.add(t.Id);
             }
             if(t.Deliverable_Status__c == 'Submitted' && t.Type_Custom__c == 'Deliverables' && t.Sub_Type__c == 'PCN')
                 pcnTasklist.add(t);
             system.debug('@@#'+pcnTasklist);
        }
        
        Map<Id, Stage_Application__c> stageAppMap = new Map<Id,Stage_Application__c>([Select Id, Application_Number__c,Application_Status__c,Service_Delivery_Type__c,Technical_Assessment__c from Stage_Application__c where Id in : stageAppIdSet]);
        system.debug('**stageAppMap**'+stageAppMap); 
 
        taskIdSet.addAll(stageAppIdSet);
        for(ContentDocumentLink cdl : [SELECT ContentDocumentId,Id,LinkedEntityId,ShareType,Visibility FROM ContentDocumentLink WHERE LinkedEntityId in : taskIdSet]){
            If(cdl.LinkedEntityId.getSObjectType().getDescribe().getName()=='Task'){
               cdlMapTask.put(cdl.ContentDocumentId,cdl);
               conDocIdSet.add(cdl.ContentDocumentId);
            }
            else
               cdlMapStageApp.put(cdl.ContentDocumentId,cdl);
         }
        system.debug('***cdlMapTask***'+cdlMapTask);
        system.debug('**conDocIdSet**'+conDocIdSet);  
        
        Set<Id> pcnTaskIds = new Set<Id>();      
        for(ContentVersion cv : [SELECT ContentDocumentId,Id,Synced__c,FileExtension,ContentSize,Title FROM ContentVersion WHERE ContentDocumentId in : conDocIdSet]){ 
             if(!cv.Synced__c){
                  if(areaPlanTaskMap.containsKey(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId)){
                      system.debug('inside..');
                      Developments__e devEvent = new Developments__e() ;         
                      devEvent.Call_Back_URL__c = identifierList.Call_Back_Base_URL__c+'connect/files/'+cv.ContentDocumentId+'/content'; 
                      devEvent.Event_Type__c = 'Deliverable';
                      devEvent.Source__c = 'Salesforce';
                      devEvent.Message_Body__c = 'Title:'+cv.Title+','+'FileExtension:'+cv.FileExtension+','+'ContentSize:'+cv.ContentSize;
                      devEvent.Correlation_Id__c = stageAppMap.get(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).whatId).Application_Number__c;                          
                      devEvent.Deliverable_Id__c = cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId;

                      If((areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Sub_Type__c == 'Master Plan') && ((stageAppMap.get(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).whatId).Application_Status__c == 'Service Delivery' && 
                         stageAppMap.get(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).whatId).Service_Delivery_Type__c == 'SD-2') ||(stageAppMap.get(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).whatId).Technical_Assessment__c))){
                              devEvent.Event_Sub_Type__c = 'Master Plan';
                              devEvent.Event_Id__c = identifierList.Master_Plan_Submitted__c;                             
                      }
                      else if(stageAppMap.get(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).whatId).Application_Status__c == 'Service Delivery' && 
                              stageAppMap.get(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).whatId).Service_Delivery_Type__c == 'SD-2' &&
                              areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Type_Custom__c == 'Deliverables'){   
                                 system.debug('inside 2');                
                               if(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Sub_Type__c == 'Pit & Pipe Design'){
                                  devEvent.Event_Sub_Type__c = 'Design Document Uploaded';
                                  devEvent.Event_Id__c = identifierList.Design_Document_Uploaded__c;
                               }
                               else if(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Sub_Type__c == 'Stage Plan'){
                                    devEvent.Event_Sub_Type__c = 'Stage Plan';
                                    devEvent.Event_Id__c = identifierList.Stage_Plan_Submitted__c;
                               } 
                               else if(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Sub_Type__c == 'Service Plan'){
                                  system.debug('inside 3');
                                    devEvent.Event_Sub_Type__c = 'Service Plan';
                                    devEvent.Event_Id__c = identifierList.Service_Plan_Submitted__c;
                               }
                               else if(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Sub_Type__c == 'As-built Design'){
                                    devEvent.Event_Sub_Type__c = 'As-built Design';
                                    devEvent.Event_Id__c = identifierList.As_built_Design_Submitted__c;
                               }
                               else if(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Sub_Type__c == 'PCN'){
                                     devEvent.Event_Type__c = 'Practical Completion Notice';
                                     devEvent.Event_Sub_Type__c = '';                          
                                     devEvent.Event_Id__c = identifierList.Practical_Completion_Notice__c;
                                     pcnTaskIds.add(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Id);
                               } 
                       }
                       /*else if (stageAppMap.get(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).whatId).Application_Status__c == 'Payment Received' && 
                              stageAppMap.get(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).whatId).Service_Delivery_Type__c == 'SD-2' &&
                              areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Type_Custom__c == 'Deliverables' &&
                              areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Sub_Type__c == 'Service Plan'){
                              devEvent.Event_Sub_Type__c = 'Service Plan';
                              devEvent.Event_Id__c = identifierList.Service_Plan_Submitted__c;      
                       }*/                        
                       else if(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Type_Custom__c == 'Additional Document Required'){
                          devEvent.Event_Sub_Type__c = 'Additional Document';                          
                          devEvent.Event_Id__c = identifierList.Additional_Document_Submitted__c;
                          devEvent.Message_Body__c = 'documentType:'+areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Document_Type_to_Submit__c+','+'Title:'+cv.Title+','+'FileExtension:'+cv.FileExtension+','+'ContentSize:'+cv.ContentSize;
                       }
                       else if(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).Type_Custom__c == 'Remediation Report Created - Fail'){ 
                           if(remidDocUrls==null)
                             remidDocUrls=identifierList.Call_Back_Base_URL__c+'connect/files/'+cv.ContentDocumentId+'/content';
                           else
                             remidDocUrls=remidDocUrls+';'+identifierList.Call_Back_Base_URL__c+'connect/files/'+cv.ContentDocumentId+'/content';
                           cv.Synced__c = true; 
                           cvListToUpdate.add(cv);                         
                       }
   
                       if(devEvent.Event_Id__c != null){                       
                           cv.Synced__c = true;      // Enable flag to indicate that file has been published                                                        
                           system.debug('**devEvent**'+devEvent); 
                           eventListToPublish.add(devEvent);                                                 
                           cvListToUpdate.add(cv);
                        }
                     } 
                 } 
               // Attach file to the stage Application  record  
                  if(!cdlMapStageApp.containsKey(cv.ContentDocumentId)){
                     ContentDocumentLink cdlNew = new ContentDocumentLink();  
                     cdlNew.ContentDocumentId = cv.ContentDocumentId ;
                     cdlNew.LinkedEntityId  = stageAppMap.get(areaPlanTaskMap.get(cdlMapTask.get(cv.ContentDocumentId).LinkedEntityId).whatId).id; 
                     cdlNew.ShareType ='V';
                     cdlNew.Visibility = 'AllUsers';
                     cdlNewList.add(cdlNew);    
                  }          
        }
      // PCN task published when there are no attachment on that
         for(Task tk : pcnTasklist){
            system.debug('@@###'+tk);
            if((!pcnTaskIds.Contains(tk.Id)) && stageAppMap.get(tk.whatId).Service_Delivery_Type__c == 'SD-2' && stageAppMap.get(tk.whatId).Application_Status__c == 'Service Delivery'){
               system.debug('@@#pcnTaskIds##'+pcnTaskIds);
               Developments__e devEvent = new Developments__e() ; 
               devEvent.Event_Type__c = 'Practical Completion Notice';
               devEvent.Event_Sub_Type__c = '';                          
               devEvent.Event_Id__c = identifierList.Practical_Completion_Notice__c;
               devEvent.Deliverable_Id__c = tk.Id;
               devEvent.Correlation_Id__c = stageAppMap.get(tk.whatId).Application_Number__c;
               devEvent.Source__c = 'Salesforce';   
               eventListToPublish.add(devEvent);
            }
         }
    
    
    
  // Send notification to Appain when addition information required task closed 
         for(Task tsk : taskClosed){
             Developments__e taskCloseEvent = new Developments__e() ; 
             taskCloseEvent.Call_Back_URL__c = identifierList.Call_Back_Base_URL__c+'sobjects/Stage_Application__c/'+tsk.WhatId;
             if(stageAppMap.containsKey(tsk.WhatId))
             taskCloseEvent.Correlation_Id__c = stageAppMap.get(tsk.WhatId).Application_Number__c;
             taskCloseEvent.Deliverable_Id__c = tsk.Id;
             taskCloseEvent.Event_Id__c = identifierList.Additional_Info_Received__c;
             taskCloseEvent.Event_Sub_Type__c = '';
             taskCloseEvent.Event_Type__c = 'Additional Info Received';
             taskCloseEvent.Triggering_Object__c = 'Stage_Application__c';
             taskCloseEvent.Source__c = 'Salesforce';
             system.debug('**TaskClose**'+taskCloseEvent); 
             eventListToPublish.add(taskCloseEvent);
         }
         for(Task tsk : remidTasks){
             Developments__e taskDfEvent = new Developments__e() ; 
             taskDfEvent.Call_Back_URL__c = identifierList.Call_Back_Base_URL__c+'sobjects/Stage_Application__c/'+tsk.WhatId;
             if(stageAppMap.containsKey(tsk.WhatId))
             taskDfEvent.Correlation_Id__c = stageAppMap.get(tsk.WhatId).Application_Number__c;
             taskDfEvent.Event_Id__c = identifierList.Remediation_Complete__c;
             taskDfEvent.Event_Sub_Type__c = '';
             taskDfEvent.Event_Type__c = 'Remediation Complete';
             taskDfEvent.Message_Body__c = 'Comments:'+tsk.Description+',Documents:'+remidDocUrls;
             taskDfEvent.Triggering_Object__c = 'Stage_Application__c';
             taskDfEvent.Source__c = 'Salesforce';
             system.debug('**TaskClose**'+taskDfEvent); 
             eventListToPublish.add(taskDfEvent);
         }
        system.debug('*EventListToPublish**'+eventListToPublish);
        if(EventListToPublish!=null){
           List<Database.SaveResult> results = EventBus.publish(eventListToPublish);
           for(Database.SaveResult sr : results) {
               if (sr.isSuccess()) {
                  System.debug('Successfully published event.');
               } 
               else {
                  for(Database.Error err : sr.getErrors()) {
                      System.debug('Error returned: ' +  err.getStatusCode() +  ' - ' + err.getMessage());
                  }
               }      
           }
        }
        if(cvListToUpdate!=null){
           update cvListToUpdate;          // when file published enable sync flag to restrict the file for next send 
        }
        if(cdlNewList!=null){
           insert cdlNewList;             // insert file on Stage Application
        }       
     }
         
     
     // This invocable method will be called from Process builder "Service Delivery Identified" to publish SF-SD-000003 event. 
   @InvocableMethod(label='kick off SD2 process' description='To be called from Process builder to kick-off SD process and premise count')
   public static void kickOffSD2Process(List<ID> appIds) {
      system.debug('AppIds==='+appIds);
      List<Developments__e> eventListToPublish = new List<Developments__e>();        
      List<Stage_Application__c> appListToUpdate = new List<Stage_Application__c>();
      for(Stage_Application__c app : [Select Id,Application_Number__c, (Select Id,Location_URL__c from Premises__r) from Stage_Application__c where id in :appIds]){
         system.debug('@app==='+app+app.Premises__r);
         Developments__e devEvent = new Developments__e() ; 
         devEvent.Call_Back_URL__c = identifierList.Call_Back_Base_URL__c +'sobjects/Stage_Application__c/'+app.Id; 
         devEvent.Correlation_Id__c = app.Application_Number__c ;
         devEvent.Event_Id__c = identifierList.Kickoff_SD_2_process__c;
         devEvent.Event_Type__c = 'Kickoff SD 2 process';
         devEvent.Event_Sub_Type__c = '';
         devEvent.Triggering_Object__c = 'Stage_Application__c';
         devEvent.Source__c = 'Salesforce';
         if(app.Premises__r!=null){
            for(Premise__c pms : app.Premises__r){
               System.debug('@pms==='+pms);
               if(devEvent.Premises_URLs__c != null)
                  devEvent.Premises_URLs__c = identifierList.Call_Back_Base_URL__c +'sobjects/Premise__c/'+pms.Id+','+devEvent.Premises_URLs__c;
               else
                  devEvent.Premises_URLs__c = identifierList.Call_Back_Base_URL__c +'sobjects/Premise__c/'+pms.Id;
            }
         }
         system.debug('@devEvent==='+devEvent);
         eventListToPublish.add(devEvent);
      }
        
      system.debug('*EventListToPublish===='+eventListToPublish);
      if(EventListToPublish!=null){
         List<Database.SaveResult> results = EventBus.publish(eventListToPublish);
         for(Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
               System.debug('Successfully published event for SD2 Process');
            } 
            else {
               for(Database.Error err : sr.getErrors()) {
                  System.debug('Error returned: ' +  err.getStatusCode() +  ' - ' + err.getMessage());
               }
            }      
         }
      }
   }  
}