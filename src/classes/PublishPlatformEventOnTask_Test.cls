/*
@Auther : Anjani Tiwari
@Date : 21/Dec/2017
@Description : Test Class for class PublishPlatformEventOnTask
*/
 @isTest
 Public class PublishPlatformEventOnTask_Test{
     static testMethod void PublishAttachmentOnTask_Test(){
          
          // added my john huang
      	  SA_Auto_Reference_Number__c refNumber = new SA_Auto_Reference_Number__c(Name = 'SA001', Last_Sequence_Number__c = '000000006');
          insert refNumber;
          List<string> listOfProfileName = new List<string> {'System Administrator'};
          list<Profile> listOfProfiles = TestDataUtility.getListOfProfiles(listOfProfileName);
          User testRunningUser = TestDataUtility.createTestUser(true,listOfProfiles.get(0).id);
          insert new New_Dev_Platform_Events__c(Unassigned_User_Id__c=testRunningUser.Id);
          Account a = new Account(Name='TestAccount');
          insert a;
          Contact c = new Contact (AccountId= a.Id, FirstName = 'firstName' , LastName = 'lastName', Email='test@test.com'); 
          insert c;
          Development__c dev = new Development__c(Name= 'TestDev',Account__c = a.id, Primary_Contact__c = c.id, Development_ID__c = 'dev01', Suburb__c = 'Test');
          insert dev;
          
          List<Id> idList = new List<Id>();      
          Stage_Application__c stgApp = new Stage_Application__c(Reference_Number__c ='ref-0001',Request_ID__c='TestRq1',  Dwelling_Type__c ='SDU Only',  Application_Status__c = 'Service Delivery', Service_Delivery_Type__c = 'SD-2',
                                                               Primary_Contact__c = c.id, Development__c=dev.id,Active_Status__c ='Active',  Name = 'TestApp', Estimated_Ready_for_Service_Date__c =system.today());          
          insert stgApp;
          
          
          stgApp = [Select Id,Application_ID__c, Relationship_Manager__c,Relationship_Manager__r.isActive,Estimated_Ready_for_Service_Date__c from Stage_Application__c where id = : stgApp.id];       
          idList.add(stgApp.Id);
          system.debug('@StageAppTest'+stgApp+stgApp.Application_ID__c);
          Site__c ste = new Site__c(Name = 'Ste name');
          insert ste;
          Premise__c prms = new Premise__c(Location__c = ste.id, Stage_Application__c=stgApp.Id);
          insert prms ;
          
          List<Task> tskList = new List<Task>();
          Task cl = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Closed',
                             WhatId = stgApp.id,WhoId=c.Id ,ownerId = UserInfo.getUserId() ,Type_Custom__c = 'Additional Information Required');
          tskList.add(cl);
          Task mp = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                             WhatId = stgApp.id,WhoId=c.Id ,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Deliverables',Sub_Type__c ='Master Plan');
          tskList.add(mp);
          Task pp = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                             WhatId = stgApp.id,WhoId=c.Id ,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Deliverables',Sub_Type__c ='Pit & Pipe Design');
          tskList.add(pp);
          Task sp = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                             WhatId = stgApp.id,WhoId=c.Id ,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Deliverables',Sub_Type__c ='Stage Plan');
          tskList.add(sp);
          Task ssp = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                             WhatId = stgApp.id,WhoId=c.Id ,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Deliverables',Sub_Type__c ='Service Plan');
          tskList.add(ssp);
          Task abd = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                             WhatId = stgApp.id,WhoId=c.Id ,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Deliverables',Sub_Type__c ='As-built Design');
          tskList.add(abd);
          Task adr = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Open',
                             WhatId = stgApp.id,WhoId=c.Id ,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Additional Document Required',Sub_Type__c ='');
          tskList.add(adr);
          Task PCN = new Task(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Submitted',
                             WhatId = stgApp.id,WhoId=c.Id ,ownerId = UserInfo.getUserId() ,Deliverable_Status__c = 'Pending Documents' ,Type_Custom__c = 'Deliverables',Sub_Type__c ='PCN');
          tskList.add(PCN );
          insert tskList;
          
          list<ContentVersion> cvList = new list<ContentVersion>();
          for(Integer i=0;i<10;i++){
          ContentVersion v = new ContentVersion();
          v.versionData = Blob.valueOf('Test Content');
          v.title = 'testing upload'+i;
          v.pathOnClient ='/somepath.txt';
          cvList.add(v);
          }
          insert cvList;
          
          set<id> cvId = new set<id>();
          for(ContentVersion cv : cvList)
          cvId.add(cv.Id);          
          
          List<ContentDocument> cdList = [SELECT LatestPublishedVersionId FROM ContentDocument WHERE LatestPublishedVersionId in: cvId ];
          List<ContentDocumentLink> cdlNewList = new List<ContentDocumentLink>();
          List<Task> tskListUpdate = new List<Task>();
          Map<Id,Task> taskMap = new Map<Id,Task>();
          Integer i=0;
          for(Task t : tskList){
              if(t.Deliverable_Status__c == 'Pending Documents'){               
                 ContentDocumentLink cdlNew = new ContentDocumentLink();  
                 cdlNew.ContentDocumentId = cdList[i].Id ;
                 cdlNew.LinkedEntityId  = t.id; 
                 cdlNew.ShareType ='V';
                 cdlNew.Visibility = 'AllUsers';
                 cdlNewList.add(cdlNew);
                 t.Deliverable_Status__c = 'Submitted';
                 tskListUpdate.add(t); 
                 taskMap.put(t.id,t);
                 i++;
              }
          }
          insert cdlNewList;
          update taskMap.values();
          system.debug('###taskMap===='+taskMap);
          PublishPlatformEventOnTask.PublishAttachmentOnTask(taskMap);
          PublishPlatformEventOnTask.kickOffSD2Process(idList);
          
     }
     
     static testMethod void remeditionNotComplete_Test(){
         Account a = new Account(Name='TestAccount');
         insert a;
         Contact c = new Contact (AccountId= a.Id, FirstName = 'firstName' , LastName = 'lastName', Email = 'test@testcon.com');
         insert c;
         List<string> listOfProfileName = new List<string> {'System Administrator'};
         list<Profile> listOfProfiles = TestDataUtility.getListOfProfiles(listOfProfileName);
         User testRunningUser = TestDataUtility.createTestUser(true,listOfProfiles.get(0).id);
         insert new New_Dev_Platform_Events__c(Unassigned_User_Id__c=testRunningUser.Id);
          // added my john huang
          SA_Auto_Reference_Number__c refNumber = new SA_Auto_Reference_Number__c(Name = 'SA001', Last_Sequence_Number__c = '000000006');
          insert refNumber;
          List<Id> Ids = new List<Id>();
          
          Development__c dev = new Development__c(Name= 'TestDev',Account__c = a.id, Primary_Contact__c = c.id, Development_ID__c = 'dev01', Suburb__c = 'Test');
          insert dev;
		  Stage_Application__c stgApp = new Stage_Application__c(Reference_Number__c ='ref-0001',Request_ID__c='TestRq1',  Dwelling_Type__c ='SDU Only',  Application_Status__c = 'Payment Received',
                                                                 Primary_Contact__c = c.id, Development__c=dev.id,Active_Status__c ='Active',  Name = 'TestApp', Estimated_Ready_for_Service_Date__c =system.today());          
          insert stgApp;
          Task T = new TasK(RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(), Priority = '3-General',Status = 'Closed',
                           WhatId = stgApp.id,ownerId = UserInfo.getUserId() ,Type_Custom__c = 'Remediation Report Created - Fail');
          Insert T;
          Ids.add(T.Id);
          ApOutboundForRemediationNotCompleteTasks.RemediationNotCompleteTaskMethod(Ids);
          
     }
 }