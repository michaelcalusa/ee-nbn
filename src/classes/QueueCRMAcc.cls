public class QueueCRMAcc implements Database.AllowsCallouts
{
    /*----------------------------------------------------------------------------------------
    Author:        Dilip Athley (v-dileepathley)
    Company:       NBNco
    Description:   This class will be used to Extract the Account Records from CRMOD.
    Test Class:    
    History
    <Date>            <Authors Name>    <Brief Description of Change> 
    10/11/2018      Naseer Abbasi       Added Status field to the query and mapping to Account_Int object
    -----------------------------------------------------------------------------------------*/
    //Created the below class to through the custom exceptions
    public class GeneralException extends Exception
            {}
     // this method will be called by the ScheduleCRMExt class, which will create a new batch job (Schedule job) record and call the other methos to query the data from CRMOD.
   public static void execute(String ejId) 
   {
       Batch_Job__c bjPre  = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Type__c='Account Extract' order by createddate desc limit 1];
       // creating the new batch job(Schedule Job) record for this batch.
       Batch_Job__c bj = new Batch_Job__c();
       bj.Name = 'AccountExtract'+System.now(); 
       bj.Start_Time__c = System.now();
       bj.Extraction_Job_ID__c = ejId;
       bj.Status__c = 'Processing';
       bj.Batch_Job_Name__c = 'AccountExtract'+System.now();
       bj.Type__c='Account Extract';
       bjPre.Last_Record__c = true;
       list <Batch_Job__c> batch = new list <Batch_Job__c>();
       batch.add(bjPre);
       batch.add(bj);
       Upsert batch;
       //Calling the method to extract the data from CRM on Demand.
       QueueCRMAcc.callCRMAPIAcc(string.valueof(bj.name));
   }
    // This method will be used to query the data from CRM On Demand and insert the records in Account_Int table.
   @future(callout=true)
    public static void callCRMAPIAcc(String bjName)
    {
        Integer num = 0;
        Map<String, Object> deserializedAcc;
        list<Object> accounts= new List<Object>();
        Map <String, Object> account;
        Map <String, Object> contextinfo;
        Integer i, offset;
        DateTime mDt,lastDate;
        String dte,accData;
        TimeZone tz = UserInfo.getTimeZone();
        String jsessionId;
        Boolean invalid =false, invalidScode = false;
        list<Account_Int__c> acctList = new List<Account_Int__c>();
        Batch_Job__c bj  = [select Id,Name,Status__c,Batch_Job_ID__c,Next_Request_Start_Time__c,End_Time__c from Batch_Job__c where Name =:bjName];
        Integer session =0,invalidCode =0;
        Long sleepTime;
        String accTypes;
        //variable declaration finished         
        try
        {
            //query the previously ran batch job to get the last records modified Date.
            Batch_Job__c bjPre = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Next_Request_Start_Time__c,Last_Record__c from Batch_Job__c where Type__c='Account Extract' and Last_Record__c= true order by createddate desc limit 1];
            //format the extracted date to the format accepted by CRMOD REST API.
            String lastRecDate = bjPre.Next_Request_Start_Time__c.format('yyyy-MM-dd\'T\'HH:mm:ss');
            lastRecDate = lastRecDate+'Z';          
            //Form the end point URL, please note the initial part is in Named Credential.
            String serviceEndpoint= '?orderBy=ModifiedDate:asc&onlyData=True&fields=Id,AccountName,Status,Location,CustomText30,CustomMultiSelectPickList1,CustomText31,CustomText1,CustomText7,MainPhone,PrimaryContactFullName,PrimaryContactId,PrimaryShipToCountry,PrimaryShipToPostalCode,PrimaryShipToCity,PrimaryShipToStreetAddress,PrimaryShipToStreetAddress2,PrimaryShipToStreetAddress3,PrimaryShipToProvince,PrimaryBillToCountry,PrimaryBillToPostalCode,PrimaryBillToCity,PrimaryBillToStreetAddress,PrimaryBillToStreetAddress2,PrimaryBillToStreetAddress3,PrimaryBillToProvince,Owner,ParentAccount,ParentAccountId,CustomPickList11,MainFax,WebSite,CustomInteger0,CustomPickList4,CustomPickList2,CustomBoolean1,Description,CustomText39,CustomText40,CustomPickList10,CustomBoolean0,CustomBoolean3,CustomNumber0,CustomText36,CustomPickList7,CustomDate42,CustomPickList8,CustomText8,CustomText41,CustomText9,CustomPickList9,ModifiedDate&q=ModifiedDateExt%3E'+'\''+lastRecDate+'\'&offset=';
            //get the active Session Id from the Session Id table.
            Session_Id__c jId = [select CRM_Session_Id__c from Session_Id__c where Session_Alive__c=true order by createddate desc limit 1];
            Http http = new Http();
            HttpRequest req = new HttpRequest();
            HttpResponse res;
            req.setMethod('GET');
            req.setHeader('Cookie', jId.CRM_Session_Id__c);
            req.setTimeout(120000);
            req.setHeader('Accept', 'application/json');
            
            do
            {
                //extract the data from CRMOD
                system.debug('**EndPoint URL'+ 'callout:CRMOD_Account'+serviceEndpoint+num);
                req.setEndpoint('callout:CRMOD_Account'+serviceEndpoint+num);
                system.debug('***** reQ'+req);
                // if (test.isRunningTest())
                //{
                //Test.setMock(HTTPCalloutMock.class, new UploadFileMock());
                //} 
                // else
                // {
                res = http.send(req);
                // }
                
                
                system.debug('***** res'+res);
                accData = res.getBody();
                if(res.getStatusCode()==200 && ValidateJSON.validate(accData)) // ValidateJSON.validate(accData) will validate the output is in JSON format or not.
                {
                    //deserialize the data.
                    deserializedAcc = (Map<String, Object>)JSON.deserializeUntyped(accData);
                    if(num==0)
                    {
                        accounts = (list<Object>)deserializedAcc.get('Accounts');
                    }
                    else
                    {
                        accounts.addAll((list<Object>)deserializedAcc.get('Accounts'));
                    }
                    contextinfo = (Map<String, Object>)deserializedAcc.get('_contextInfo');
                    num=num+100;
                    if(num>3000)
                    {
                        invalidScode=true;
                    }
                }
                else if (res.getStatusCode()==403)//if 403 then establish a new session
                {
                    session = session+1;
                    if (session<=3)
                    {
                        jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                        req.setHeader('Cookie', jsessionId);
                        invalid =true;
                    }
                    else
                    {
                        throw new GeneralException('Not able to establish a session with CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                    }
                }
                else //if StatusCode is other then 200 or 403 then retry 3 times and then throw an exception.
                {
                    if(accounts.isEmpty())
                    {
                        invalidCode = invalidCode+1;
                        if (invalidCode>3)
                        {
                            throw new GeneralException('Not able to get the data from CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                        }
                        else
                        {
                            sleepTime=10000*invalidCode; // As per the Oracle's advice if the Status Code is not valid next request has to wait for some time.
                            new Sleep(sleepTime);
                        }  
                    }
                    else
                    {
                        invalidScode=true;
                    }
                    
                }
            }
            while((contextInfo==null || contextInfo.get('lastpage')==false)&&!invalidScode);//loop to get all the data, as REST API give data in pages. (100 Records per page)
            
            if (invalid)
            {
                SessionId.storeSessionId(jId,jsessionId); // store new session id and mark inactive the older one.
                invalid = false;
            }
            //create a list of type account_int__c to insert the data in the database.
            for (i=0;i<accounts.size();i++)
            {
                account= (Map<String, Object>)accounts[i];
                dte =(String)account.get('ModifiedDate');
                dte = '"'+dte+'"';
                mDt = (DateTime) JSON.deserialize(dte, DateTime.class);
                offset = -1*tz.getOffset(mDt)/1000;
                mDt = mDt.addSeconds(offset);
                accTypes = (String)account.get('CustomText9');
                if(!accTypes.equals('Access Seeker'))
                {
                    acctList.add(new Account_Int__c(Name=(String)account.get('Id'),CRM_Account_Id__c=(String)account.get('Id'),Account_Name__c=(String)account.get('AccountName'),
                                                    Location__c=(String)account.get('Location'), Status__c=(String)account.get('Status'), ABN__c=(String)account.get('CustomText30'),Account_Types__c=(String)account.get('CustomMultiSelectPickList1'),
                                                    Email__c=(String)account.get('CustomText7'),Primary_Contact__c=(String)account.get('PrimaryContactFullName'),Primary_Contact_Id__c=(String)account.get('PrimaryContactId'),
                                                    Country__c=(String)account.get('PrimaryShipToCountry'),State__c=(String)account.get('PrimaryShipToProvince'),Post_Code__c=(String)account.get('PrimaryShipToPostalCode'),
                                                    Address_2__c=(String)account.get('PrimaryShipToStreetAddress2'),Address_3__c=(String)account.get('PrimaryShipToStreetAddress3'),
                                                    Number_Street__c=(String)account.get('PrimaryShipToStreetAddress'),City__c=(String)account.get('PrimaryShipToCity'),CRM_Last_Modified_Date__c=mDt,
                                                    ACN__c=(String)account.get('CustomText31'),Registered_Entity_Name__c=(String)account.get('CustomText1'),Bill_Country__c=(String)account.get('PrimaryBillToCountry'),
                                                    Bill_State__c=(String)account.get('PrimaryBillToProvince'),Bill_Post_Code__c=(String)account.get('PrimaryBillToPostalCode'),Main_Phone__c = (String)account.get('MainPhone'),
                                                    Bill_Address_2__c=(String)account.get('PrimaryBillToStreetAddress2'),Bill_Address_3__c=(String)account.get('PrimaryBillToStreetAddress3'),
                                                    Bill_Number_Street__c=(String)account.get('PrimaryBillToStreetAddress'),Bill_City__c=(String)account.get('PrimaryBillToCity'),  
                                                    Owner__c=(String)account.get('Owner'),Parent_Account__c= (String)account.get('ParentAccount'),Parent_Account_Id__c= (String)account.get('ParentAccountId'), 
                                                    Developer_Group__c = (String)account.get('CustomPickList11'),Main_Fax__c = (String)account.get('MainFax'),WebSite__c= (String)account.get('WebSite'), 
                                                    Approximate_of_properties__c=(Integer)account.get('CustomInteger0'), High_Risk__c=(String)account.get('CustomPickList4'), Entity_Type__c=(String)account.get('CustomPickList2'), 
                                                    Legal_Review_Required__c= (Boolean)account.get('CustomBoolean1'),Description__c= (String)account.get('Description'), Trustee_Name__c=(String)account.get('CustomText39'),
                                                    Trustee_ACN__c=(String)account.get('CustomText40'),Non_ABN_Type__c=(String)account.get('CustomPickList10'),Eligible_for_Deemed_To_Accept__c= (Boolean)account.get('CustomBoolean3'),
                                                    Documents_Received__c= (Boolean)account.get('CustomBoolean0'), Upfront_Required__c=(Decimal)account.get('CustomNumber0'), 
                                                    Developer_Billing_Account_Number__c=(String)account.get('CustomText36'), Credit_Check_Required__c= (String)account.get('CustomPickList7'), 
                                                    Credit_Check_Date__c=(String)account.get('CustomDate42'), Credit_Check_Result__c=(String)account.get('CustomPickList8'), 
                                                    Credit_Check_Comments_Conditions__c=(String)account.get('CustomText8'),Billing_Site_ID__c=(String)account.get('CustomText41'),  
                                                    Developer_Classification__c=(String)account.get('CustomPickList9'),Schedule_Job__c=bj.Id));
                }
                
            }
            //code to insert the data.
            Schema.SObjectField crmAccId = Account_Int__c.Fields.CRM_Account_Id__c;
            List<Database.upsertResult> uResults = Database.upsert(acctList,false);
            //error logging while record insertion.
            list<Database.Error> err;
            String msg, fAffected;
            String[]fAff;
            StatusCode sCode;
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            for(Integer idx = 0; idx < uResults.size(); idx++)
            {   
                if(!uResults[idx].isSuccess())
                {
                    err=uResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='Account_Int__c',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=acctList[idx].CRM_Account_Id__c,Schedule_Job__c=bj.Id));
                }
            }
            Insert errList;
            if(errList.isEmpty())
            {
                bj.Status__c= 'Completed';
            }
            else
            {
                bj.Status__c= 'Error';
            }
        }
        
        catch(Exception e)
        {
            bj.Status__c= 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            
            errList.add(new Error_Logging__c(Name='Account_Int__c',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),JSON_Output__c=accData,Schedule_Job__c=bj.Id));
            Insert errList;
        }   
                
        finally
        {
            List<AsyncApexJob> futureCalls = [Select Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate from AsyncApexJob where JobType = 'future' and MethodName='callCRMAPIAcc' order by CreatedDate desc limit 1];
            if(futureCalls.size()>0)
            {
                bj.Batch_Job_ID__c = futureCalls[0].Id;
                bj.End_Time__c = System.now();
                Account_Int__c accInt = [Select Id, CRM_Last_Modified_Date__c from Account_Int__c order by CRM_Last_Modified_Date__c desc limit 1];
                bj.Next_Request_Start_Time__c=accInt.CRM_Last_Modified_Date__c;// setting the last record modified date as the Next Request Start date (date from which the next job will fetch the records.)
                bj.Record_Count__c=acctList.size();
                upsert bj;
            } 
        }
    }
}