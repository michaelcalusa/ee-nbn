/***************************************************************************************************
Class Name:  QueueCRMAcc_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : 
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Sukumar       26/10/2015                 Created
****************************************************************************************************/

@isTest
private class QueueCRMAcc_Test {
    static Extraction_Job__c ExtractionJobRec = new Extraction_Job__c ();
    static Session_Id__c SessionIdRec = new Session_Id__c();
    static string body =  '{'+
        '\"Accounts\":    ['+
        '{'+
        '\"Id\": \"AYCA-3Q9SGG\",'+
        '\"AccountName\": \"Darazzo Pty Ltd ATF The Queen Street Unit Trust\",'+
        '\"Location\": \"SA\",'+
        '\"CustomText30\": \"00000000000\",'+
        '\"CustomMultiSelectPickList1\": \"Developer\",'+
        '\"CustomText31\": \"080484386\",'+
        '\"CustomText1\": \"Darazzo Pty Ltd ATF The Queen Street Unit Trust\",'+
        '\"CustomText7\": \"\",'+
        '\"MainPhone\": \"\",'+
        '\"PrimaryContactFullName\": \"Pat Callisto\",'+
        '\"PrimaryContactId\": \"AYCA-3Q9SGL\",'+
        '\"PrimaryShipToCountry\": \"\",'+
        '\"PrimaryShipToPostalCode\": \"5000\",'+
        '\"PrimaryShipToCity\": \"Adelaide\",'+
        '\"PrimaryShipToStreetAddress\": \"3/169 Pirie St\",'+
        '\"PrimaryShipToStreetAddress2\": \"\",'+
        '\"PrimaryShipToStreetAddress3\": \"\",'+
        '\"PrimaryShipToProvince\": \"SA\",'+
        '\"PrimaryBillToCountry\": \"\",'+
        '\"PrimaryBillToPostalCode\": \"\",'+
        '\"PrimaryBillToCity\": \"\",'+
        '\"PrimaryBillToStreetAddress\": \"\",'+
        '\"PrimaryBillToStreetAddress2\": \"\",'+
        '\"PrimaryBillToStreetAddress3\": \"\",'+
        '\"PrimaryBillToProvince\": \"\",'+
        '\"Owner\": \"Jodie Lunn\",'+
        '\"ParentAccount\": \"\",'+
        '\"ParentAccountId\": \"\",'+
        '\"CustomPickList11\": \"\",'+
        '\"MainFax\": \"\",'+
        '\"WebSite\": \"\",'+
        '\"CustomInteger0\": 0,'+
        '\"CustomPickList4\": \"\",'+
        '\"CustomPickList2\": \"Company\",'+
        '\"CustomBoolean1\": true,'+
        '\"Description\": \"\",'+
        '\"CustomText39\": \"\",'+
        '\"CustomText40\": \"\",'+
        '\"CustomPickList10\": \"ACN\",'+
        '\"CustomBoolean0\": false,'+
        '\"CustomBoolean3\": false,'+
        '\"CustomNumber0\": 50,'+
        '\"CustomText36\": \"\",'+
        '\"CustomPickList7\": \"No\",'+
        '\"CustomDate42\": \"\",'+
        '\"CustomPickList8\": \"\",'+
        '\"CustomText8\": \"\",'+
        '\"CustomText41\": \"\",'+
        '\"CustomPickList9\": \"Tier 3\",'+
        '\"CustomText9\": \"Test\",'+
        '\"ModifiedDate\": \"2016-09-16T17:20:33Z\"'+
        '}       '+
        '],'+
        '\"_contextInfo\":    {'+
        '\"limit\": 100,'+
        '\"offset\": 0,'+
        '\"lastpage\": true'+
        '}'+
        '}';
    static void getRecords (){  
        // create Extraction_Job record
        ExtractionJobRec = TestDataUtility.createExtractionJob(true);
        // Create Batch_Job record
        Batch_Job__c AccountExtractBatchJob = new Batch_Job__c();
        AccountExtractBatchJob.Type__c='Account Extract';
        AccountExtractBatchJob.Extraction_Job_ID__c = ExtractionJobRec.Id;
        AccountExtractBatchJob.Batch_Job_ID__c=ExtractionJobRec.Id;
        AccountExtractBatchJob.Next_Request_Start_Time__c=System.now();
        AccountExtractBatchJob.End_Time__c=System.now();
        AccountExtractBatchJob.Status__c='Processing';
        AccountExtractBatchJob.Last_Record__c=True;
        insert AccountExtractBatchJob;
        // Create Session_Id record
        SessionIdRec.Session_Alive__c=true;
        SessionIdRec.CRM_Session_Id__c= userinfo.getSessionId();
        insert SessionIdRec;
    }
    static testMethod void QueueCRMAcc_SuccessScenario() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMAcc.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMAcc_ExceptionTest() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        body =  '{'+
        '\"Accounts\":    ['+
        '{'+
        '\"Id\": \"AYCA-3Q9SGG\",'+
        '\"AccountName\": \"Darazzo Pty Ltd ATF The Queen Street Unit Trust\",'+
        '\"Location\": \"SA\",'+
        '\"CustomText30\": \"00000000000\",'+
        '\"CustomMultiSelectPickList1\": \"Developer\",'+
        '\"CustomText31\": \"080484386\",'+
        '\"CustomText1\": \"Darazzo Pty Ltd ATF The Queen Street Unit Trust\",'+
        '\"CustomText7\": \"\",'+
        '\"MainPhone\": \"\",'+
        '\"PrimaryContactFullName\": \"Pat Callisto\",'+
        '\"PrimaryContactId\": \"AYCA-3Q9SGL\",'+
        '\"PrimaryShipToCountry\": \"\",'+
        '\"PrimaryShipToPostalCode\": \"5000\",'+
        '\"PrimaryShipToCity\": \"Adelaide\",'+
        '\"PrimaryShipToStreetAddress\": \"3/169 Pirie St\",'+
        '\"PrimaryShipToStreetAddress2\": \"\",'+
        '\"PrimaryShipToStreetAddress3\": \"\",'+
        '\"PrimaryShipToProvince\": \"SA\",'+
        '\"PrimaryBillToCountry\": \"\",'+
        '\"PrimaryBillToPostalCode\": \"\",'+
        '\"PrimaryBillToCity\": \"\",'+
        '\"PrimaryBillToStreetAddress\": \"\",'+
        '\"PrimaryBillToStreetAddress2\": \"\",'+
        '\"PrimaryBillToStreetAddress3\": \"\",'+
        '\"PrimaryBillToProvince\": \"\",'+
        '\"Owner\": \"Jodie Lunn\",'+
        '\"ParentAccount\": \"\",'+
        '\"ParentAccountId\": \"\",'+
        '\"CustomPickList11\": \"\",'+
        '\"MainFax\": \"\",'+
        '\"WebSite\": \"\",'+
        '\"CustomInteger0\": 0,'+
        '\"CustomPickList4\": \"\",'+
        '\"CustomPickList2\": \"Company\",'+
        '\"CustomBoolean1\": true,'+
        '\"Description\": \"\",'+
        '\"CustomText39\": \"\",'+
        '\"CustomText40\": \"\",'+
        '\"CustomPickList10\": \"ACN\",'+
        '\"CustomBoolean0\": false,'+
        '\"CustomBoolean3\": false,'+
        '\"CustomNumber0\": 50,'+
        '\"CustomText36\": \"\",'+
        '\"CustomPickList7\": \"No\",'+
        '\"CustomDate42\": \"\",'+
        '\"CustomPickList8\": \"\",'+
        '\"CustomText8\": \"\",'+
        '\"CustomText41\": \"\",'+
        '\"CustomPickList9\": \"Tier 3\",'+
        '\"ModifiedDate\": \"2016-09-16T17:20:33Z\"'+
        '}       '+
        '],'+
        '\"_contextInfo\":    {'+
        '\"limit\": 100,'+
        '\"offset\": 0,'+
        '\"lastpage\": true'+
        '}'+
        '}';
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMAcc.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMAcc_403StatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 403;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMAcc.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMAcc_InvalidStatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 900;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMAcc.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMAcc_Failure() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        body =  '{'+
        '\"Accounts\":    ['+
        '{'+
        '\"Id\": \"AYCA-3Q9SGG\",'+
        '\"AccountName\": \"Darazzo Pty Ltd ATF The Queen Street Unit Trust\",'+
        '\"Location\": \"SA\",'+
        '\"CustomText30\": \"00000000000\",'+
        '\"CustomMultiSelectPickList1\": \"Developer\",'+
        '\"CustomText31\": \"080484386\",'+
        '\"CustomText1\": \"Darazzo Pty Ltd ATF The Queen Street Unit Trust\",'+
        '\"CustomText7\": \"abcaccc\",'+
        '\"MainPhone\": \"\",'+
        '\"PrimaryContactFullName\": \"Pat Callisto\",'+
        '\"PrimaryContactId\": \"AYCA-3Q9SGL\",'+
        '\"PrimaryShipToCountry\": \"\",'+
        '\"PrimaryShipToPostalCode\": \"5000\",'+
        '\"PrimaryShipToCity\": \"Adelaide\",'+
        '\"PrimaryShipToStreetAddress\": \"3/169 Pirie St\",'+
        '\"PrimaryShipToStreetAddress2\": \"\",'+
        '\"PrimaryShipToStreetAddress3\": \"\",'+
        '\"PrimaryShipToProvince\": \"SA\",'+
        '\"PrimaryBillToCountry\": \"\",'+
        '\"PrimaryBillToPostalCode\": \"\",'+
        '\"PrimaryBillToCity\": \"\",'+
        '\"PrimaryBillToStreetAddress\": \"\",'+
        '\"PrimaryBillToStreetAddress2\": \"\",'+
        '\"PrimaryBillToStreetAddress3\": \"\",'+
        '\"PrimaryBillToProvince\": \"\",'+
        '\"Owner\": \"Jodie Lunn\",'+
        '\"ParentAccount\": \"\",'+
        '\"ParentAccountId\": \"\",'+
        '\"CustomPickList11\": \"\",'+
        '\"MainFax\": \"\",'+
        '\"WebSite\": \"\",'+
        '\"CustomInteger0\": 0,'+
        '\"CustomPickList4\": \"\",'+
        '\"CustomPickList2\": \"Company\",'+
        '\"CustomBoolean1\": true,'+
        '\"Description\": \"\",'+
        '\"CustomText39\": \"\",'+
        '\"CustomText40\": \"\",'+
        '\"CustomPickList10\": \"ACN\",'+
        '\"CustomBoolean0\": false,'+
        '\"CustomBoolean3\": false,'+
        '\"CustomNumber0\": 50,'+
        '\"CustomText36\": \"\",'+
        '\"CustomPickList7\": \"No\",'+
        '\"CustomDate42\": \"\",'+
        '\"CustomPickList8\": \"\",'+
        '\"CustomText8\": \"\",'+
        '\"CustomText41\": \"\",'+
        '\"CustomPickList9\": \"Tier 3\",'+
        '\"CustomText9\": \"Test\",'+
        '\"ModifiedDate\": \"2016-09-16T17:20:33Z\"'+
        '}       '+
        '],'+
        '\"_contextInfo\":    {'+
        '\"limit\": 100,'+
        '\"offset\": 0,'+
        '\"lastpage\": true'+
        '}'+
        '}';
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMAcc.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
}