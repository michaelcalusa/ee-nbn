/*
Class Description
Creator: Karan Shekhar
Purpose: This class will be used to Extract the NTD Pre-Install cases from CRMOD.
Modifications:
*/
public class QueueCRMCases implements Database.AllowsCallouts{
    @TestVisible private static Integer invalidCode =0;
    public class GeneralException extends Exception
            {}
            
  /*  public static String getSessionId(){
      Http http = new Http();
      HttpRequest req = new HttpRequest();
      HttpResponse res;
      String cookie,jsessionId;
      req.setMethod('GET');
      req.setEndpoint('callout:CRMOD_ConnectionUAT');
      res = http.send(req);
       cookie = res.getHeader('Set-Cookie');
      jsessionId = cookie.substring(cookie.indexOf('JSESSIONID='),cookie.indexOf('; path=/OnDemand; HttpOnly; Secure') );
      return(jsessionId);
  }*/
            
   @future(callout=true)
    public static void callCRMAPICases(String bjName){
            Integer num = 0;
            Map<String, Object> deserializedNTDCases;
            list<Object> cases= new List<Object>();
            Map <String, Object> ntdCases;
            Map <String, Object> contextinfo;
            Integer i, offset;
            DateTime mDt,lastDate;
            String dte,casesData;
            TimeZone tz = UserInfo.getTimeZone();
            String jsessionId;
            Boolean invalid =false, invalidScode = false;
            list<Cases_Int__c> caseList = new List<Cases_Int__c>();
            Batch_Job__c bj  = [select Id,Name,Status__c,Batch_Job_ID__c,Next_Request_Start_Time__c,End_Time__c from Batch_Job__c where Name =:bjName];
            system.debug('++++++++> batch Job'+bj);
            Integer session =0;
            Long sleepTime;
            //variable declaration finished         
            try
            {
                 //query the previously ran batch job to get the last records modified Date.
                Batch_Job__c bjPre = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Next_Request_Start_Time__c,Last_Record__c from Batch_Job__c where Type__c='NTD Cases Extract' and Last_Record__c= true order by createddate desc limit 1];
                //format the extracted date to the format accepted by CRMOD REST API.
                String lastRecDate = bjPre.Next_Request_Start_Time__c.format('yyyy-MM-dd\'T\'HH:mm:ss');
                lastRecDate = lastRecDate+'Z';          
                //Form the end point URL, please note the initial part is in Named Credential.
                String serviceEndpoint= '?orderBy=ModifiedDate:asc&onlyData=True&fields=Id,SRNumber,Type,Status,CustomText70,CustomText73,CustomText35,CustomText36,CustomText50,CustomText41,IndexedPick1,CustomText33,OptimizedCustomTextSM1,ModifiedDate&q=Type=\'NTD%20Pre%2DInstall%20Request\'+;+ModifiedDateExt%3E'+'\''+lastRecDate+'\'&offset=';                                       
                Session_Id__c jId = [select CRM_Session_Id__c from Session_Id__c where Session_Alive__c=true order by createddate desc limit 1];
                system.debug('**serviceEndpoint***'+serviceEndpoint);
                //Session_Id__c jId = [select CRM_Session_Id__c from Session_Id__c where Session_Alive__c =true order by createddate desc limit 1];
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                HttpResponse res;
                req.setMethod('GET');
                //comment when connection is from production
               // req.setHeader('Cookie', getSessionId());
                //uncomment when connection is from production
                req.setHeader('Cookie', jId.CRM_Session_Id__c);
                req.setTimeout(120000);
                req.setHeader('Accept', 'application/json');
                do
                {
                    //extract the data from CRMOD
                    req.setEndpoint('callout:CRMOD_NTD'+serviceEndpoint+num);
                    res = http.send(req);
                    casesData = res.getBody();
                    if(res.getStatusCode()==200  && ValidateJSON.validate(casesData)) // ValidateJSON.validate(accData) will validate the output is in JSON format or not.
                    {
                        
                        //deserialize the data.
                        deserializedNTDCases = (Map<String, Object>)JSON.deserializeUntyped(casesData);
                        if(num==0)
                        {
                            cases = (list<Object>)deserializedNTDCases.get('ServiceRequests');
                            system.debug('CaseVales'+cases);
                        }
                        else
                        {
                            cases.addAll((list<Object>)deserializedNTDCases.get('ServiceRequests'));
                            system.debug('CaseVales2222'+cases);
                        }
                        contextinfo = (Map<String, Object>)deserializedNTDCases.get('_contextInfo');
                        num=num+100;
                        if(num>3000)
                        {
                            invalidScode=true;
                        }
                    }
                    else if (res.getStatusCode()==403)//if 403 then establish a new session
                    {
                        session = session+1;
                        if (session<=3)
                        {
                            system.debug(':::-Session'+session);
                            jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                            req.setHeader('Cookie', jsessionId);
                            invalid =true;
                        }
                        else
                        {
                            system.debug('====>Invalid' +invalid);
                            throw new GeneralException('Not able to establish a session with CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                        }
                    }
                    else //if StatusCode is other then 200 or 403 then retry 3 times and then throw an exception.
                    {
                        if(cases.isEmpty())
                        {
                            invalidCode = invalidCode+1;
                        	if (invalidCode>3)
                        	{
                                throw new GeneralException('Not able to get the data from CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                            }
                            else
                       		{
                            	sleepTime=10000*invalidCode; // As per the Oracle's advice if the Status Code is not valid next request has to wait for some time.
                            	new Sleep(sleepTime);
                        	}  
                        }
                        else
                        {
                            invalidScode=true;
                        }            
                    }
                }
                while((contextInfo==null || contextInfo.get('lastpage')==false)&&!invalidScode);//loop to get all the data, as REST API give data in pages. (100 Records per page)
                system.debug('**cases**'+cases);
                if (invalid)
                {
                    //SessionId.storeSessionId(jId,jsessionId); // store new session id and mark inactive the older one.
                    invalid = false;
                }
                //create a list of type account_int__c to insert the data in the database.
                for (i=0;i<cases.size();i++)
                {
                    ntdCases= (Map<String, Object>)cases[i];
                    dte =(String)ntdCases.get('ModifiedDate');
                    dte = '"'+dte+'"';
                    mDt = (DateTime) JSON.deserialize(dte, DateTime.class);
                    offset = -1*tz.getOffset(mDt)/1000;
                    mDt = mDt.addSeconds(offset);
                    caseList.add(new Cases_Int__c(Name=(String)ntdCases.get('Id'),Status__c=(String)ntdCases.get('Status'),CRM_Case_Id__c=(String)ntdCases.get('Id'),Case_number__c=(String)ntdCases.get('SRNumber'),Location_ID__c=(String)ntdCases.get('OptimizedCustomTextSM1'),Lot_Number__c=(String)ntdCases.get('CustomText70'),Post_code__c=(String)ntdCases.get('CustomText33'),State__c=(String)ntdCases.get('IndexedPick1'),Street_Name__c=(String)ntdCases.get('CustomText36'),Street_Number__c=(String)ntdCases.get('CustomText35'),Street_Type__c=(String)ntdCases.get('CustomText50'),Suburb__c=(String)ntdCases.get('CustomText41'),Type__c=(String)ntdCases.get('Type'),Unit_shop_number__c=(String)ntdCases.get('CustomText73'),CRM_Last_Modified_Date__c=mDt,Schedule_Job__c=bj.Id));
                }
                //code to insert the data.
               // Schema.SObjectField crmAccId = Account_Int__c.Fields.CRM_Account_Id__c;
                List<Database.upsertResult> uResults = Database.upsert(caseList,false);
                //error logging while record insertion.
                list<Database.Error> err;
                String msg, fAffected;
                String[]fAff;
                StatusCode sCode;
                list<Error_Logging__c> errList = new List<Error_Logging__c>();
                for(Integer idx = 0; idx < uResults.size(); idx++)
                {   
                    if(!uResults[idx].isSuccess())
                    {
                        err=uResults[idx].getErrors();
                        for (Database.Error er: err)
                        {
                            sCode = er.getStatusCode();
                            msg=er.getMessage();
                            fAff = er.getFields();
                        }
                        fAffected = string.join(fAff,',');
                        errList.add(new Error_Logging__c(Name='Cases_Int__c',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=caseList[idx].CRM_Case_Id__c,Schedule_Job__c=bj.Id));
                    }
                }
                Insert errList;
                if(errList.isEmpty())
                {
                    bj.Status__c= 'Completed';
                }
                else
                {
                    bj.Status__c= 'Error';
                }
            }

            catch(Exception e)
            {   
            system.debug('Exception occured'+ e.getmessage()+e.getstackTraceString());
                bj.Status__c= 'Error';
                list<Error_Logging__c> errList = new List<Error_Logging__c>();
                errList.add(new Error_Logging__c(Name='Cases_Int__c',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),JSON_Output__c=casesData,Schedule_Job__c=bj.Id));
                Insert errList;
            }   
    
            
            finally{
                List<AsyncApexJob> futureCalls = [Select Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate from AsyncApexJob where JobType = 'future' and MethodName='callCRMAPICases' order by CreatedDate desc limit 1];
                if(futureCalls.size()>0){
                    bj.Batch_Job_ID__c = futureCalls[0].Id;
                    bj.End_Time__c = System.now();
                    Cases_Int__c caseInt = [Select Id, CRM_Last_Modified_Date__c from Cases_Int__c order by CRM_Last_Modified_Date__c desc limit 1];
                    bj.Next_Request_Start_Time__c=caseInt.CRM_Last_Modified_Date__c;
                    bj.Record_Count__c=caseList.size();
                    upsert bj;
                }
            }
    }
   public static void execute(String ejId) {
        
          // Extraction_Job__c ej = [select id, status__c from Extraction_Job__c where status__c!='Completed' order by CreatedDate desc limit 1]; 
           system.debug('****ejId***'+ejId);
           List<Batch_Job__c> bjPreList  = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Type__c='NTD Cases Extract' order by createddate desc limit 1];
           Batch_Job__c bj = new Batch_Job__c();
           bj.Name = 'NTDCasesExtract'+System.now(); // Extraction Job ID
           bj.Start_Time__c = System.now();
           bj.Extraction_Job_ID__c = ejId;
           bj.Status__c = 'Processing';
           bj.Batch_Job_Name__c = 'NTDCasesExtract'+System.now();
           bj.Type__c='NTD Cases Extract';
           list <Batch_Job__c> batch = new list <Batch_Job__c>(); 
           if(bjPreList.size()>0){
                bjPreList[0].Last_Record__c = true;
                batch.add(bjPreList[0]);
            }                               
           batch.add(bj);
           Upsert batch;
           QueueCRMCases.callCRMAPICases(string.valueof(bj.name));
   }
}