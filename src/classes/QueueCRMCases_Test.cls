/***************************************************************************************************
Class Name:  QueueCRMCases_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 04/11/2016
Function    : 
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       04/11/2016                 Created
****************************************************************************************************/

@isTest
private class QueueCRMCases_Test {
    static Extraction_Job__c ExtractionJobRec = new Extraction_Job__c ();
    static Session_Id__c SessionIdRec = new Session_Id__c();
    static string body = '{'+
        '   \"ServiceRequests\":    ['+
        '   {'+
        '   \"Id\": \"AYCA-2QF5DO\",'+
        '   \"SRNumber\": \"511066-165309180\",'+
        '   \"Type\": \"NTD Pre-Install Request\",'+
        '   \"Status\": \"Cancelled\",'+
        '   \"CustomText70\": \"627\",'+
        '   \"CustomText73\": \"2\",'+
        '   \"CustomText35\": \"42b\",'+
        '   \"CustomText36\": \"Verdant\",'+
        '   \"CustomText50\": \"Road\",'+
        '   \"CustomText41\": \"Truganina\",'+
        '   \"IndexedPick1\": \"VIC\",'+
        '   \"CustomText33\": \"3029\",'+
        '   \"OptimizedCustomTextSM1\": \"LOC000147117740\",'+
        '   \"ModifiedDate\": \"2016-09-16T17:42:55Z\"'+
        '   }'+    
        '   ],'+
        '   \"_contextInfo\":    {'+
        '   \"limit\": 100,'+
        '   \"offset\": 0,'+
        '   \"lastpage\": true'+
        '   }'+
        '}';
    static void getRecords (){  
        // create Extraction_Job record
        ExtractionJobRec = TestDataUtility.createExtractionJob(true);
        TestDataUtility.getListOfCRMExtractionJobRecords(true);
        // Create Batch_Job record
        Batch_Job__c NTDCasesExtractBatchJob = new Batch_Job__c();
        NTDCasesExtractBatchJob.Type__c='NTD Cases Extract';
        NTDCasesExtractBatchJob.Extraction_Job_ID__c = ExtractionJobRec.Id;
        NTDCasesExtractBatchJob.Batch_Job_ID__c=ExtractionJobRec.Id;
        NTDCasesExtractBatchJob.Next_Request_Start_Time__c=System.now();
        NTDCasesExtractBatchJob.End_Time__c=System.now();
        NTDCasesExtractBatchJob.Status__c='Processing';
        NTDCasesExtractBatchJob.Last_Record__c=true;
        insert NTDCasesExtractBatchJob;
        // Create Session_Id record
        SessionIdRec.Session_Alive__c=true;
        SessionIdRec.CRM_Session_Id__c= userinfo.getSessionId();
        insert SessionIdRec;
    }
    static testMethod void QueueCRMCases_SuccessScenario() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMCases.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMCases_ExceptionTest() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        body = '{'+
        '   \"ServiceRequests\":    ['+
        '   {'+
        '   \"Id\": \"AYCA-2QF5DO\",'+
        '   \"SRNumber\": \"511066-165309180\",'+
        '   \"Type\": \"NTD Pre-Install Request\",'+
        '   \"Status\": \"Cancelled\",'+
        '   \"CustomText70\": \"627\",'+
        '   \"CustomText73\": \"2\",'+
        '   \"CustomText35\": \"42b\",'+
        '   \"CustomText36\": \"Verdant\",'+
        '   \"CustomText50\": \"Road\",'+
        '   \"CustomText41\": \"Truganina\",'+
        '   \"IndexedPick1\": \"VIC\",'+
        '   \"CustomText33\": \"3029\",'+
        '   \"OptimizedCustomTextSM1\": \"LOC000147117740\"'+
        '   }'+    
        '   ],'+
        '   \"_contextInfo\":    {'+
        '   \"limit\": 100,'+
        '   \"offset\": 0,'+
        '   \"lastpage\": true'+
        '   }'+
        '}';
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMCases.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMCases_403StatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 403;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMCases.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMCases_InvalidStatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 900;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        body = '{'+
        '   \"ServiceRequests\":    ['+
        '   {'+
        '   \"Id\": \"AYCA-2QF5DO\",'+
        '   \"SRNumber\": \"511066-165309180\",'+
        '   \"Type\": \"NTD Pre-Install Request\",'+
        '   \"Status\": \"Cancelled\",'+
        '   \"CustomText70\": \"627\",'+
        '   \"CustomText73\": \"2\",'+
        '   \"CustomText35\": \"42b\",'+
        '   \"CustomText36\": \"Verdant\",'+
        '   \"CustomText50\": \"Road\",'+
        '   \"CustomText41\": \"Truganina\",'+
        '   \"IndexedPick1\": \"VIC\",'+
        '   \"CustomText33\": \"3029\",'+
        '   \"OptimizedCustomTextSM1\": \"LOC000147117740\"'+
        '   }'+    
        '   ],'+
        '   \"_contextInfo\":    {'+
        '   \"limit\": 100,'+
        '   \"offset\": 0,'+
        '   \"lastpage\": true'+
        '   }'+
        '}';
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMCases.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMCases_Failure() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        body = '{'+
        '   \"ServiceRequests\":    ['+
        '   {'+
        '   \"Id\": \"AYCA-2QF5DO2QF5DO2QF5DO2QF5DO2QF5DO2QF5DO\",'+
        '   \"SRNumber\": \"511066-165309180\",'+
        '   \"Type\": \"NTD Pre-Install Request\",'+
        '   \"Status\": \"Cancelled\",'+
        '   \"CustomText70\": \"627\",'+
        '   \"CustomText73\": \"2\",'+
        '   \"CustomText35\": \"42b\",'+
        '   \"CustomText36\": \"Verdant\",'+
        '   \"CustomText50\": \"Road\",'+
        '   \"CustomText41\": \"Truganina\",'+
        '   \"IndexedPick1\": \"VIC\",'+
        '   \"CustomText33\": \"3029\",'+
        '   \"OptimizedCustomTextSM1\": \"LOC000147117740\",'+
        '   \"ModifiedDate\": \"2016-09-16T17:42:55Z\"'+
        '   }'+    
        '   ],'+
        '   \"_contextInfo\":    {'+
        '   \"limit\": 100,'+
        '   \"offset\": 0,'+
        '   \"lastpage\": true'+
        '   }'+
        '}';
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMCases.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
}