public class QueueCRMComm implements Database.AllowsCallouts
{
   /*----------------------------------------------------------------------------------------
    Author:        Dilip Athley (v-dileepathley)
    Company:       NBNco
    Description:   This class will be used to Extract the Communication Records from CRMOD.
    Test Class:    
    History
    <Date>            <Authors Name>    <Brief Description of Change> 
    
    -----------------------------------------------------------------------------------------*/
    /*********************************************************************
    US#4118: Below lines have been commented to decommission P2P jobs.
    Author: Jairaj Jadhav
    Company: Wipro Technologies
    
    //Created the below class to through the custom exceptions
    public class GeneralException extends Exception
            {}
     
     // this method will be called by the ScheduleCRMExt class, which will create a new batch job (Schedule job) record and call the other methos to query the data from CRMOD.
    public static void execute(String ejId)
    {
       Batch_Job__c bjPre  = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Type__c='Communication Extract' order by createddate desc limit 1]; 
       // creating the new batch job(Schedule Job) record for this batch.
       Batch_Job__c bj = new Batch_Job__c();
       bj.Name = 'CommExtract'+System.now(); 
       bj.Start_Time__c = System.now();
       bj.Extraction_Job_ID__c = ejId;
       bj.Status__c = 'Processing';
       bj.Batch_Job_Name__c = 'CommExtract'+System.now();
       bj.Type__c='Communication Extract';
       bjPre.Last_Record__c = true;
       list <Batch_Job__c> batch = new list <Batch_Job__c>();
       batch.add(bjPre);
       batch.add(bj);
       Upsert batch;
        //Calling the method to extract the data from CRM on Demand.
       QueueCRMComm.callCRMAPIComm(string.valueof(bj.name));
    }
    // This method will be used to query the data from CRM On Demand and insert the records in Communication_Int table.
    @future(callout=true)
    public static void callCRMAPIComm(String bjName)
    {
            Integer num = 0;
            Map<String, Object> deserializedComm;
            list<Object> comms = new List<Object>();
            Map <String, Object> comm;
            Map <String, Object> contextinfo;
            Integer i,offset;
            DateTime mDt,lastDate;
            String dte,commData;
            TimeZone tz = UserInfo.getTimeZone();
            list<Communication_Int__c> commList = new List<Communication_Int__c>();
            Batch_Job__c bj  = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c from Batch_Job__c where Name =:bjName];
            String jsessionId;
            Boolean invalid =false, invalidScode=false,lastRec = false;
            Integer session =0,invalidCode =0;
            Long sleepTime;
            //variable declaration finished
            try
            {
                //query the previously ran batch job to get the last records modified Date.
                Batch_Job__c bjPre = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Next_Request_Start_Time__c,Last_Record__c from Batch_Job__c where Type__c='Communication Extract' and Last_Record__c= true order by createddate desc limit 1];
                //format the extracted date to the format accepted by CRMOD REST API.
                String lastRecDate = bjPre.Next_Request_Start_Time__c.format('yyyy-MM-dd\'T\'HH:mm:ss');
                lastRecDate = lastRecDate+'Z';
                //Form the end point URL, please note the initial part is in Named Credential.
                String serviceEndpoint= '?orderBy=ModifiedDate:desc&onlyData=True&fields=CustomText1,CustomNote0,IndexedPick0,OwnerAlias,Type,CustomText0,IndexedPick2,ModifiedDate,IndexedPick1,CustomDate27,CustomText3,CustomObject1Name,CustomObject15Id,CustomObject4Id,CustomDate28,CustomText2,CustomDate25,CustomDate26,QuickSearch1,Id&q=ModifiedDate%3E'+'\''+lastRecDate+'\'&offset=';
                //get the active Session Id from the Session Id table.
                Session_Id__c jId = [select CRM_Session_Id__c from Session_Id__c where Session_Alive__c=true order by createddate desc limit 1];
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                req.setMethod('GET');
                req.setHeader('Cookie', jId.CRM_Session_Id__c);
                req.setTimeout(120000);
                req.setHeader('Accept', 'application/json');
                HttpResponse res;
                do
                {
                    //extract the data from CRMOD
                    req.setEndpoint('callout:CRMOD_Communication'+serviceEndpoint+num);
                    res = http.send(req);
                    commData = res.getBody();
                    if(res.getStatusCode()==200 && ValidateJSON.validate(commData)) // ValidateJSON.validate(accData) will validate the output is in JSON format or not.
                    {
                        //deserialize the data.
                        deserializedComm =(Map<String, Object>)JSON.deserializeUntyped(commData);
                        if(num==0)
                        {
                            comms = (list<Object>)deserializedComm.get('CustomObjects9');
                        }
                        else
                        {
                            comms.addAll((list<Object>)deserializedComm.get('CustomObjects9'));
                        }
                    
                        contextinfo = (Map<String, Object>)deserializedComm.get('_contextInfo');
                        num=num+100;
                        if(num>3000)
                        {
                            invalidScode=true;
                        }
                    }
                    else if (res.getStatusCode()==403)
                    {
                        session = session+1;
                        if (session<=3)
                        {
                            jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                            req.setHeader('Cookie', jsessionId);
                            invalid =true;
                        }
                        else
                        {
                            throw new GeneralException('Not able to establish a session with CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                        }
                    }
                    else //if StatusCode is other then 200 or 403 then retry 3 times and then throw an exception.
                    {
                        if(comms.isEmpty())
                        {
                            invalidCode = invalidCode+1;
                            if (invalidCode>3)
                            {
                                throw new GeneralException('Not able to get the data from CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                            }
                            else
                            {
                                sleepTime=10000*invalidCode; // As per the Oracle's advice if the Status Code is not valid next request has to wait for some time.
                                new Sleep(sleepTime);
                            }  
                        }
                        else
                        {
                            invalidScode=true;
                        }
                    }
                }
                while(!lastRec&&(contextInfo==null || contextInfo.get('lastpage')==false)&&!invalidScode);//loop to get all the data, as REST API give data in pages. (100 Records per page)
                if (invalid)
                {
                    SessionId.storeSessionId(jId,jsessionId); // store new session id and mark inactive the older one.
                    invalid=false;
                }
                //create a list of type Communication_int__c to insert the data in the database.
                for (i=0;i<comms.size();i++)
                {
                    comm= (Map<String, Object>)comms[i];
                    dte =(String)comm.get('ModifiedDate');
                    dte = '"'+dte+'"';
                    mDt = (DateTime) JSON.deserialize(dte, DateTime.class);
                    offset = -1*tz.getOffset(mDt)/1000;
                    mDt = mDt.addSeconds(offset);
                    commList.add(new Communication_Int__c(Addressee__c=(String)comm.get('CustomText1'),Address_Lines__c=(String)comm.get('CustomNote0'),
                    Communication_Subtype__c=(String)comm.get('IndexedPick0'),Communication_Type__c=(String)comm.get('Type'),Condition__c=(String)comm.get('CustomText0'),
                    Contractor_Assigned__c=(String)comm.get('IndexedPick2'),CRM_Modified_Date__c=mDt,Delivery_Method__c=(String)comm.get('IndexedPick1'),End_Date__c=(String)comm.get('CustomDate27'),
                    FMF_Good_Faith_Attempts__c=(String)comm.get('CustomText3'),FSA__c=(String)comm.get('CustomObject1Name'),MDU_CP_Id__c=(String)comm.get('CustomObject15Id'),
                    SAM__c=(String)comm.get('CustomObject4Id'),Sent_Date__c=(String)comm.get('CustomDate28'),SharePoint_URL__c=(String)comm.get('CustomText2'),Signed_Date__c=(String)comm.get('CustomDate25'),
                    Start_Date__c=(String)comm.get('CustomDate26'),Stock_Code__c=(String)comm.get('QuickSearch1'),Name=(String)comm.get('Id'),Owner__c = (String)comm.get('OwnerAlias'),Schedule_Job__c=bj.Id));
                }
                //code to insert the data.
                List<Database.upsertResult> uResults = Database.upsert(commList,false);
                //error logging while record insertion.
                list<Database.Error> err;
                String msg, fAffected;
                String[]fAff;
                StatusCode sCode;
                list<Error_Logging__c> errList = new List<Error_Logging__c>();
                for(Integer idx = 0; idx < uResults.size(); idx++)
                {   
                    if(!uResults[idx].isSuccess())
                    {
                        err=uResults[idx].getErrors();
                        for (Database.Error er: err)
                        {
                            sCode = er.getStatusCode();
                            msg=er.getMessage();
                            fAff = er.getFields();
                        }
                        fAffected = string.join(fAff,',');
                        errList.add(new Error_Logging__c(Name='Communication_Int__c',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=commList[idx].Name,Schedule_Job__c=bj.Id));
                    }
                }
                Insert errList;
                if(errList.isEmpty())
                {
                    bj.Status__c= 'Completed';
                }
                else
                {
                    bj.Status__c= 'Error';
                }
            }
            catch(Exception e)
            {
                bj.Status__c= 'Error';
                list<Error_Logging__c> errList = new List<Error_Logging__c>();
                errList.add(new Error_Logging__c(Name='Communication_Int__c',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),JSON_Output__c=commData,Schedule_Job__c=bj.Id));
                Insert errList;
            }   
            finally
            {
                List<AsyncApexJob> futureCalls = [Select Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate from AsyncApexJob where JobType = 'future' and MethodName='callCRMAPIComm' order by CreatedDate desc limit 1];
                if(futureCalls.size()>0){
                    bj.Batch_Job_ID__c = futureCalls[0].Id;
                    bj.End_Time__c = System.now();
                    Communication_Int__c commInt = [Select Id,CRM_Modified_Date__c from Communication_Int__c order by CRM_Modified_Date__c desc limit 1];// setting the last record modified date as the Next Request Start date (date from which the next job will fetch the records.)
                    bj.Next_Request_Start_Time__c=commInt.CRM_Modified_Date__c;
                    bj.Record_Count__c=commList.size();
                    upsert bj;
                }
            }
    }
    *********************************************************************/
   
}