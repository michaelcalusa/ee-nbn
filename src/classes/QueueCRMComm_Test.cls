/***************************************************************************************************
Class Name:  QueueCRMComm_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : 
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Sukumar       26/10/2015                 Created
****************************************************************************************************/

@isTest
private class QueueCRMComm_Test {
    /*********************************************************************
    US#4118: Below lines have been commented to decommission P2P jobs.
    Author: Jairaj Jadhav
    Company: Wipro Technologies
    
    static Extraction_Job__c ExtractionJobRec = new Extraction_Job__c ();
    static Session_Id__c SessionIdRec = new Session_Id__c();
    static string body = '{'+
        '   \"CustomObjects9\":    ['+
        '            {'+
        '         \"CustomText1\": \"The Owner - Strata Plan No. 76117\",'+
        '         \"CustomNote0\": \"C / - Body Corporate 26 Bondi Road The Entrance North NSW 2261\",'+
        '         \"IndexedPick0\": \"Installation (LiFD2)\",'+
        '         \"Type\": \"Schd 3 Notice/Legal Document\",'+
        '         \"CustomText0\": \"\",'+
        '         \"IndexedPick2\": \"UCG\",'+
        '         \"ModifiedDate\": \"2016-09-19T14:56:12Z\",'+
        '         \"IndexedPick1\": \"Post\",'+
        '         \"CustomDate27\": \"2017-01-18\",'+
        '         \"CustomText3\": \"\",'+
        '         \"CustomObject1Name\": \"\",'+
        '         \"CustomObject15Id\": \"AYCA-1Z7P4T\",'+
        '         \"CustomObject4Id\": \"AYCA-OX5O6\",'+
        '         \"CustomDate28\": \"2016-08-16\",'+
        '         \"CustomText2\": \"https://docs.nbnco.net.au/shared/S0035/UCG/notice/2LJT-10%20Installation%20and%20Design%2020160816%2026%20Bondi%20Street%20The%20Entrance%20North%20LOC000086737069.pdf\",'+
        '         \"CustomDate25\": \"\",'+
        '         \"CustomDate26\": \"2016-09-05\",'+
        '         \"QuickSearch1\": \"CON:046i Jan 2016\",'+
        '         \"Id\": \"AYCA-3R9H2N\"'+
        '      }'+
        '   ],'+
        '   \"_contextInfo\":    {'+
        '      \"limit\": 100,'+
        '      \"offset\": 0,'+
        '      \"lastpage\": true'+
        '   }'+
        '}';
    static void getRecords (){  
        // create Extraction_Job record
        ExtractionJobRec = TestDataUtility.createExtractionJob(true);
        // Create Batch_Job record
        Batch_Job__c CommunicationExtractBatchJob = new Batch_Job__c();
        CommunicationExtractBatchJob.Type__c='Communication Extract';
        CommunicationExtractBatchJob.Extraction_Job_ID__c = ExtractionJobRec.Id;
        CommunicationExtractBatchJob.Batch_Job_ID__c=ExtractionJobRec.Id;
        CommunicationExtractBatchJob.Next_Request_Start_Time__c=System.now();
        CommunicationExtractBatchJob.End_Time__c=System.now();
        CommunicationExtractBatchJob.Status__c='Processing';
        CommunicationExtractBatchJob.Last_Record__c=True;
        insert CommunicationExtractBatchJob;
        // Create Session_Id record
        SessionIdRec.Session_Alive__c=true;
        SessionIdRec.CRM_Session_Id__c= userinfo.getSessionId();
        insert SessionIdRec;
    }
    static testMethod void QueueCRMComm_SuccessScenario() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMComm.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMComm_ExceptionTest() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        body = '{'+
        '   \"CustomObjects9\":    ['+
        '            {'+
        '         \"CustomText1\": \"The Owner - Strata Plan No. 76117\",'+
        '         \"CustomNote0\": \"C / - Body Corporate 26 Bondi Road The Entrance North NSW 2261\",'+
        '         \"IndexedPick0\": \"Installation (LiFD2)\",'+
        '         \"Type\": \"Schd 3 Notice/Legal Document\",'+
        '         \"CustomText0\": \"\",'+
        '         \"IndexedPick2\": \"UCG\",'+
        '         \"IndexedPick1\": \"Post\",'+
        '         \"CustomDate27\": \"2017-01-18\",'+
        '         \"CustomText3\": \"\",'+
        '         \"CustomObject1Name\": \"\",'+
        '         \"CustomObject15Id\": \"AYCA-1Z7P4T\",'+
        '         \"CustomObject4Id\": \"AYCA-OX5O6\",'+
        '         \"CustomDate28\": \"2016-08-16\",'+
        '         \"CustomText2\": \"https://docs.nbnco.net.au/shared/S0035/UCG/notice/2LJT-10%20Installation%20and%20Design%2020160816%2026%20Bondi%20Street%20The%20Entrance%20North%20LOC000086737069.pdf\",'+
        '         \"CustomDate25\": \"\",'+
        '         \"CustomDate26\": \"2016-09-05\",'+
        '         \"QuickSearch1\": \"CON:046i Jan 2016\",'+
        '         \"Id\": \"AYCA-3R9H2N\"'+
        '      }'+
        '   ],'+
        '   \"_contextInfo\":    {'+
        '      \"limit\": 100,'+
        '      \"offset\": 0,'+
        '      \"lastpage\": true'+
        '   }'+
        '}';
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMComm.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMComm_403StatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 403;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMComm.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMAcc_InvalidStatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 900;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMComm.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMComm_Failure() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        body = '{'+
        '   \"CustomObjects9\":    ['+
        '            {'+
        '         \"CustomText1\": \"The Owner - Strata Plan No. 76117\",'+
        '         \"CustomNote0\": \"C / - Body Corporate 26 Bondi Road The Entrance North NSW 2261\",'+
        '         \"IndexedPick0\": \"Installation (Test)\",'+
        '         \"Type\": \"Schd 3 Notice/Legal Document\",'+
        '         \"CustomText0\": \"\",'+
        '         \"IndexedPick2\": \"UCG\",'+
        '         \"ModifiedDate\": \"2016-09-19T14:56:12Z\",'+
        '         \"IndexedPick1\": \"Post\",'+
        '         \"CustomDate27\": \"2017-01-18\",'+
        '         \"CustomText3\": \"\",'+
        '         \"CustomObject1Name\": \"\",'+
        '         \"CustomObject15Id\": \"AYCA-1Z7P4T\",'+
        '         \"CustomObject4Id\": \"AYCA-OX5O6\",'+
        '         \"CustomDate28\": \"2016-08-16\",'+
        '         \"CustomText2\": \"https://docs.nbnco.net.au/shared/S0035/UCG/notice/2LJT-10%20Installation%20and%20Design%2020160816%2026%20Bondi%20Street%20The%20Entrance%20North%20LOC000086737069.pdf\",'+
        '         \"CustomDate25\": \"\",'+
        '         \"CustomDate26\": \"2016-09-05\",'+
        '         \"QuickSearch1\": \"CON:046i Jan 2016\",'+
        '         \"Id\": \"AYCA-3R9H2N\"'+
        '      }'+
        '   ],'+
        '   \"_contextInfo\":    {'+
        '      \"limit\": 100,'+
        '      \"offset\": 0,'+
        '      \"lastpage\": true'+
        '   }'+
        '}';
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMComm.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    *********************************************************************/
}