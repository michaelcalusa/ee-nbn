public class QueueCRMCon implements Database.AllowsCallouts
{
   /*----------------------------------------------------------------------------------------
	Author:        Dilip Athley (v-dileepathley)
    Company:       NBNco
    Description:   This class will be used to Extract the Contact Records from CRMOD.
    Test Class:    
    History
    <Date>            <Authors Name>    <Brief Description of Change> 
	
    -----------------------------------------------------------------------------------------*/
	//Created the below class to through the custom exceptions
	public class GeneralException extends Exception
			{}
     // this method will be called by the ScheduleCRMExt class, which will create a new batch job (Schedule job) record and call the other methos to query the data from CRMOD.
    public static void execute(String ejId)
	{ 
		   Batch_Job__c bjPre  = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Type__c='Contact Extract' order by createddate desc limit 1];      
        // creating the new batch job(Schedule Job) record for this batch.   
           Batch_Job__c bj = new Batch_Job__c();
           bj.Name = 'ContactExtract'+System.now();  
           bj.Start_Time__c = System.now();
           bj.Extraction_Job_ID__c = ejId;
           bj.Status__c = 'Processing';
           bj.Batch_Job_Name__c = 'ContactExtract'+System.now();
           bj.Type__c='Contact Extract';
           bjPre.Last_Record__c = true;
           list <Batch_Job__c> batch = new list <Batch_Job__c>();
           batch.add(bjPre);
           batch.add(bj);
           Upsert batch;
        //Calling the method to extract the data from CRM on Demand.
		   QueueCRMCon.callCRMAPICon(string.valueof(bj.name));
	}
    // This method will be used to query the data from CRM On Demand and insert the records in Contact_Int table.
   @future(callout=true)
   public static void callCRMAPICon(String bjName)
	{
		Integer num = 0;
		Map<String, Object> deserializedCon;
		list<Object> contacts= new List<Object>();
		Map <String, Object> contact;
		Map <String, Object> contextinfo;
		Integer i,offset;
		DateTime mDt,lastDate;
		String dte,conData;
		TimeZone tz = UserInfo.getTimeZone();
		Batch_Job__c bj  = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c from Batch_Job__c where Name =:bjName];
		list<Contact_Int__c> conList = new List<Contact_Int__c>();
        list<Contact_Int__c> errConList = new List<Contact_Int__c>();
		list<Site_Int__c> siteList = new List<Site_Int__c>();
		String jsessionId;
		Boolean invalid =false, invalidScode=false;
		Integer session =0,invalidCode =0;
		Long sleepTime;
        String fName;
        String lName;
		//variable declaration finished		
		try
		{
             //query the previously ran batch job to get the last records modified Date.
			Batch_Job__c bjPre = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Next_Request_Start_Time__c,Last_Record__c from Batch_Job__c where Type__c='Contact Extract' and Last_Record__c= true order by createddate desc limit 1];
			//format the extracted date to the format accepted by CRMOD REST API.
            String lastRecDate = bjPre.Next_Request_Start_Time__c.format('yyyy-MM-dd\'T\'HH:mm:ss');
			lastRecDate = lastRecDate+'Z';
            
            //Form the end point URL, please note the initial part is in Named Credential.
			String serviceEndpoint= '?orderBy=ModifiedDate:asc&onlyData=True&fields=Id,ContactFirstName,ContactLastName,AccountName,Description,AccountId,PIMCompanyName,ContactEmail,CellularPhone,WorkPhone,WorkFax,CustomMultiSelectPickList3,CustomMultiSelectPickList2,CustomObject3Name,HomePhone,MrMrs,ModifiedDate,CustomPhone0,CustomObject2Name,CustomText36,AlternateZipCode,AlternateCity,AlternateProvince,AlternateCountry,AlternateAddress1,AlternateAddress2,AlternateAddress3,CustomMultiSelectPickList5,CustomText37,LeadSource,CustomMultiSelectPickList4,CustomText34,CustomText35,CustomObject1Name,CustomText40,CustomText42,CustomText36,CustomText39,CustomText43,CustomText44,CreatedByFullName,UpdatedByFullName,OptOut,OwnerFullName,CustomPickList0,ModifiedDate&q=ModifiedDateExt%3E'+'\''+lastRecDate+'\'&offset=';
			//get the active Session Id from the Session Id table.
            Session_Id__c jId = [select CRM_Session_Id__c from Session_Id__c where Session_Alive__c=true order by createddate desc limit 1];
			Http http = new Http();
			HttpRequest req = new HttpRequest();
			HttpResponse res;
			req.setMethod('GET');
			req.setHeader('Cookie', jId.CRM_Session_Id__c);
			req.setTimeout(120000);
			req.setHeader('Accept', 'application/json');
			do
			{
                //extract the data from CRMOD
				req.setEndpoint('callout:CRMOD_Contact'+serviceEndpoint+num);
				res = http.send(req);
				conData = res.getBody();
				if(res.getStatusCode()==200 && ValidateJSON.validate(conData))// ValidateJSON.validate(accData) will validate the output is in JSON format or not.
				{
		 			//deserialize the data.
		 			deserializedCon =(Map<String, Object>)JSON.deserializeUntyped(conData);
		 			if(num==0)
	 				{
	 					contacts = (list<Object>)deserializedCon.get('Contacts');
	 				}
		 			else
		 			{
		 				contacts.addAll((list<Object>)deserializedCon.get('Contacts'));
		 			}
		 			contextinfo = (Map<String, Object>)deserializedCon.get('_contextInfo');
					num=num+100;
                    if(num>3000)
                    {
                        invalidScode=true;
                    }
				}
				else if (res.getStatusCode()==403)
				{
					session = session+1;
					if (session<=3)
					{
						jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
						req.setHeader('Cookie', jsessionId);
						invalid =true;
					}
					else
					{
						throw new GeneralException('Not able to establish a session with CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
					}
				}
				else //if StatusCode is other then 200 or 403 then retry 3 times and then throw an exception.
				{
					if(contacts.isEmpty())
                        {
                            invalidCode = invalidCode+1;
                        	if (invalidCode>3)
                        	{
                                throw new GeneralException('Not able to get the data from CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                            }
                            else
                       		{
                            	sleepTime=10000*invalidCode; // As per the Oracle's advice if the Status Code is not valid next request has to wait for some time.
                            	new Sleep(sleepTime);
                        	}  
                        }
                        else
                        {
                            invalidScode=true;
                        }	
				}
			}
			while((contextInfo==null || contextInfo.get('lastpage')==false)&&!invalidScode);//loop to get all the data, as REST API give data in pages. (100 Records per page)
			if (invalid)
			{
				SessionId.storeSessionId(jId,jsessionId); // store new session id and mark inactive the older one.
				invalid=false;
			}
            //create a list of type contact_int__c to insert the data in the database.
			for (i=0;i<contacts.size();i++)
			{
				contact= (Map<String, Object>)contacts[i];
				dte =(String)contact.get('ModifiedDate');
			  	dte = '"'+dte+'"';
			  	mDt = (DateTime) JSON.deserialize(dte, DateTime.class);
				offset = -1*tz.getOffset(mDt)/1000;
				mDt = mDt.addSeconds(offset);
                fName = (String)contact.get('ContactFirstName');
                lName = (String)contact.get('ContactLastName');
                if(fName.indexOfIgnoreCase('ANON', 0)!=0 || lName.indexOfIgnoreCase('ANON', 0)!=0)
                {
                    conList.add(new Contact_Int__c(Name=(String)contact.get('Id'),Given_Name__c=(String)contact.get('ContactFirstName'),
                    Family_Name__c=(String)contact.get('ContactLastName'),CRM_Last_Modified_Date__c=(String)contact.get('ModifiedDate'),
                    CRM_Contact_Id__c=(String)contact.get('Id'),Contact_Types__c=(String)contact.get('CustomMultiSelectPickList5'),
                    Account_Id__c=(String)contact.get('AccountId'),Primary_Account_Name__c=(String)contact.get('AccountName'),CRM_Modified_Date__c=mDt,
                    Development_Name__c=(String)contact.get('CustomObject2Name'),Location_ID__c=(String)contact.get('CustomText36'),Mobile__c=(String)contact.get('CustomPhone0'),
                    Salutation__c=(String)contact.get('MrMrs'),Work_Email__c=(String)contact.get('ContactEmail'),Email__c=(String)contact.get('PIMCompanyName'),
                    Work_Phone__c=(String)contact.get('WorkPhone'),Unsubscribed_from_Communications__c=(String)contact.get('CustomMultiSelectPickList3'),
                    Subscribed_to_Communications__c=(String)contact.get('CustomMultiSelectPickList2'),Number_Street__c=(String)contact.get('AlternateAddress1'),
                    Address_2__c=(String)contact.get('AlternateAddress2'),Address_3__c=(String)contact.get('AlternateAddress3'),City__c=(String)contact.get('AlternateCity'),
                    Country__c=(String)contact.get('AlternateCountry'),State__c=(String)contact.get('AlternateProvince'),Post_Code__c=(String)contact.get('AlternateZipCode'),
                    Phone__c=(String)contact.get('HomePhone'),Work_Mobile__c=(String)contact.get('CellularPhone'),Health_Safety_Considerations__c=(String)contact.get('CustomMultiSelectPickList4'),Social_Media_Account__c=(String)contact.get('CustomText37'),
                    Source__c= (String)contact.get('LeadSource'),Preferred_contact_method__c=(String)contact.get('CustomPickList0'),Work_Fax__c=(String)contact.get('WorkFax'),
                    Description__c = (String)contact.get('Description'),Created_By__c = (String)contact.get('CreatedByFullName'),Modified_By__c =(String)contact.get('UpdatedByFullName'),
                    Owner__c = (String)contact.get('OwnerFullName'),FSA__c= (String)contact.get('CustomObject1Name'),Latitude__c= (String)contact.get('CustomText34'),Longitude__c= (String)contact.get('CustomText35'),
                    MDU_LOC_ID__c= (String)contact.get('CustomText42'),Opt_Out__c= (Boolean)contact.get('OptOut'),Premises_Type__c= (String)contact.get('CustomText39'),
                    Rollout_Type__c= (String)contact.get('CustomText44'),SAM__c= (String)contact.get('CustomText40'),Technology_Type__c= (String)contact.get('CustomText43'),
                    Schedule_Job__c=bj.Id));
                //For the valid address (all address related field is there) create a record in Site_Int__c table.
                    if(((String.isNotBlank((String)contact.get('AlternateAddress1')) || String.isNotBlank((String)contact.get('AlternateAddress2')) 
                    || String.isNotBlank((String)contact.get('AlternateAddress3'))) && String.isNotBlank((String)contact.get('AlternateCity')) 
                    && String.isNotBlank((String)contact.get('AlternateProvince')) && String.isNotBlank((String)contact.get('AlternateZipCode'))))
                    {
                        siteList.add(new Site_Int__c(Number_Street__c=(String)contact.get('AlternateAddress1'),Address_2__c=(String)contact.get('AlternateAddress2'),
                        Address_3__c=(String)contact.get('AlternateAddress3'),City__c=(String)contact.get('AlternateCity'),Country__c=(String)contact.get('AlternateCountry'),
                        State__c=(String)contact.get('AlternateProvince'),Post_Code__c=(String)contact.get('AlternateZipCode'),Location_ID__c=(String)contact.get('CustomText36'),
                        CRM_Contact_Id__c=(String)contact.get('Id'),FSA__c= (String)contact.get('CustomObject1Name'),Latitude__c= (String)contact.get('CustomText34'),
                        Longitude__c= (String)contact.get('CustomText35'),MDU_LOC_ID__c= (String)contact.get('CustomText42'),
                        Premises_Type__c= (String)contact.get('CustomText39'),Rollout_Type__c= (String)contact.get('CustomText44'),
                        SAM__c= (String)contact.get('CustomText40'),Technology_Type__c= (String)contact.get('CustomText43'),Schedule_Job__c=bj.Id));
                    }
                }
		    	
			}
            //code to insert the data.
			List<Database.upsertResult> uResults = Database.upsert(conList,false);
			List<Database.upsertResult> uSiteResults = Database.upsert(siteList,false);
            //error logging while record insertion.
			list<Database.Error> err;
			String msg, fAffected;
			String[]fAff;
			StatusCode sCode;
			list<Error_Logging__c> errList = new List<Error_Logging__c>();
			for(Integer idx = 0; idx < uResults.size(); idx++)
			{	
				if(!uResults[idx].isSuccess())
				{
					err=uResults[idx].getErrors();
					for (Database.Error er: err)
					{
						sCode = er.getStatusCode();
						msg=er.getMessage();
	            		fAff = er.getFields();
					}
					fAffected = string.join(fAff,',');
                    if(String.valueof(sCode) == 'INVALID_EMAIL_ADDRESS')
                    {
                        
                        conList[idx].Description__c = conList[idx].Description__c + ' Email: ' + conList[idx].Email__c;
                        conList[idx].Email__c = '';
                        errConList.add(conList[idx]);
                    }
                    else
                    {
                        errList.add(new Error_Logging__c(Name='Contact_Int__c',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=conList[idx].CRM_Contact_Id__c,Schedule_Job__c=bj.Id));
                    }
					
				}
			}
			List<Database.upsertResult> uErrResults = Database.upsert(errConList,false);
			for(Integer idx = 0; idx < uSiteResults.size(); idx++)
			{	
				if(!uSiteResults[idx].isSuccess())
				{
					err=uSiteResults[idx].getErrors();
					for (Database.Error er: err)
					{
						sCode = er.getStatusCode();
						msg=er.getMessage();
	            		fAff = er.getFields();
					}
					fAffected = string.join(fAff,',');
					errList.add(new Error_Logging__c(Name='Site_Int__c',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uSiteResults[idx].isCreated(),isSuccess__c=uSiteResults[idx].isSuccess(),Record_Row_Id__c=uSiteResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=siteList[idx].CRM_Contact_Id__c,Schedule_Job__c=bj.Id));
				}
			}
			Insert errList;
			
			if(errList.isEmpty())
			{
				bj.Status__c= 'Completed';
			}
			else
			{
				bj.Status__c= 'Error';
				
			}
		}
		catch(Exception e)
		{
			bj.Status__c= 'Error';
			list<Error_Logging__c> errList = new List<Error_Logging__c>();
			errList.add(new Error_Logging__c(Name='Contact_Int__c',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),JSON_Output__c=conData,Schedule_Job__c=bj.Id));
			Insert errList;
		}
		finally
		{	
			List<AsyncApexJob> futureCalls = [Select Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate from AsyncApexJob where JobType = 'future' and MethodName='callCRMAPICon' order by CreatedDate desc limit 1];
			if(futureCalls.size()>0)
            {
                bj.Batch_Job_ID__c = futureCalls[0].Id;
                bj.End_Time__c = System.now();
                Contact_Int__c conInt = [Select Id, CRM_Modified_Date__c from Contact_Int__c order by CRM_Modified_Date__c desc limit 1];
                bj.Next_Request_Start_Time__c= conInt.CRM_Modified_Date__c;// setting the last record modified date as the Next Request Start date (date from which the next job will fetch the records.)
                bj.Record_Count__c=conList.size();
                upsert bj;
            }
		}
    }
   
}