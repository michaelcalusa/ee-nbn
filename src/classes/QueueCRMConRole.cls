public class QueueCRMConRole implements Database.AllowsCallouts
{
   /*----------------------------------------------------------------------------------------
    Author:        Dilip Athley (v-dileepathley)
    Company:       NBNco
    Description:   This class will be used to Extract the Account Records from CRMOD.
    Test Class:    
    History
    <Date>            <Authors Name>    <Brief Description of Change> 
    
    -----------------------------------------------------------------------------------------*/
    //Created the below class to through the custom exceptions 
    public class GeneralException extends Exception
            {}
    // this method will be called by the ScheduleCRMExt class, which will create a new batch job (Schedule job) record and call the other methos to query the data from CRMOD.
    public static void execute(String ejId) 
    {
        Batch_Job__c bjPre  = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Type__c='Contact Role Extract' order by createddate desc limit 1];
        // creating the new batch job(Schedule Job) record for this batch.
        Batch_Job__c bj = new Batch_Job__c();
        bj.Name = 'ConRoleExtract'+System.now(); // Extraction Job ID
        bj.Start_Time__c = System.now();
        bj.Extraction_Job_ID__c = ejId;
        bj.Status__c = 'Processing';
        bj.Batch_Job_Name__c = 'ConRoleExtract'+System.now();
        bj.Type__c='Contact Role Extract';
        bjPre.Last_Record__c = true;
        list <Batch_Job__c> batch = new list <Batch_Job__c>();
        batch.add(bjPre);
        batch.add(bj);
        Upsert batch;
        //Calling the method to extract the data from CRM on Demand.
        QueueCRMConRole.callCRMAPIConRole(string.valueof(bj.name));
    }
   // This method will be used to query the data from CRM On Demand and insert the records in Junction_Object_Int table. 
   @future(callout=true)
    public static void callCRMAPIConRole(String bjName)
    {
            Integer num = 0;
            Map<String, Object> deserializedMod;
            list<Object> mods = new List<Object>();
            Map <String, Object> mod;
            Map <String, Object> contextinfo;
            Integer i, offset;
            DateTime mDt,lastDate;
            String dte, modData;
            TimeZone tz = UserInfo.getTimeZone();
            
            list<Junction_Object_Int__c> junList = new List<Junction_Object_Int__c>();
            String jsessionId;
            Boolean invalid =false, invalidScode = false;
            Batch_Job__c bj  = [select Id,Name,Status__c,Batch_Job_ID__c,Next_Request_Start_Time__c,End_Time__c from Batch_Job__c where Name =:bjName];
            Integer session =0,invalidCode =0;
            Long sleepTime;
        //variable declaration finished     
            try
            {
                 //query the previously ran batch job to get the last records modified Date.
                Batch_Job__c bjPre = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Next_Request_Start_Time__c,Last_Record__c from Batch_Job__c where Type__c='Contact Role Extract' and Last_Record__c= true order by createddate desc limit 1];
                //format the extracted date to the format accepted by CRMOD REST API.
                String lastRecDate = bjPre.Next_Request_Start_Time__c.format('yyyy-MM-dd\'T\'HH:mm:ss');
                lastRecDate = lastRecDate+'Z';
                //Form the end point URL, please note the initial part is in Named Credential.
                String serviceEndpoint= '?orderBy=ModifiedDate:asc&onlyData=True&fields=Id,ModifiedDate,Type,CustomObject15Id,CustomObject7Id,ContactId&q=Type=\'MAR%20Registration\'+;+ModifiedDate%3E'+'\''+lastRecDate+'\'&offset=';
                //get the active Session Id from the Session Id table.
                Session_Id__c jId = [select CRM_Session_Id__c from Session_Id__c where Session_Alive__c=true order by createddate desc limit 1];
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                HttpResponse res;
                req.setMethod('GET');
                req.setHeader('Cookie', jId.CRM_Session_Id__c);
                req.setTimeout(120000);
                req.setHeader('Accept', 'application/json');
                do
                {
                    //extract the data from CRMOD
                    req.setEndpoint('callout:CRMOD_Contact_Role'+serviceEndpoint+num);
                    res = http.send(req);
                    modData = res.getBody();
                    if(res.getStatusCode()==200 && ValidateJSON.validate(modData))// ValidateJSON.validate(accData) will validate the output is in JSON format or not.
                    {
                        //deserialize the data.
                        deserializedMod = (Map<String, Object>)JSON.deserializeUntyped(modData);
                        
                        if(num==0)
                        {
                            mods = (list<Object>)deserializedMod.get('CustomObjects6');
                        }
                        else
                        {
                            mods.addAll((list<Object>)deserializedMod.get('CustomObjects6'));
                        }
                        contextinfo = (Map<String, Object>)deserializedMod.get('_contextInfo');
                        num=num+100;
                        if(num>3000)
                        {
                            invalidScode=true;
                        }
                    }
                    else if (res.getStatusCode()==403)//if 403 then establish a new session
                    {
                        session = session+1;
                        if (session<=3)
                        {
                            jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                            req.setHeader('Cookie', jsessionId);
                            invalid =true;
                        }
                        else
                        {
                            throw new GeneralException('Not able to establish a session with CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                        }
                    }
                    else //if StatusCode is other then 200 or 403 then retry 3 times and then throw an exception.
                    {
                        if(mods.isEmpty())
                        {
                            invalidCode = invalidCode+1;
                        	if (invalidCode>3)
                        	{
                                throw new GeneralException('Not able to get the data from CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                            }
                            else
                       		{
                            	sleepTime=10000*invalidCode; // As per the Oracle's advice if the Status Code is not valid next request has to wait for some time.
                            	new Sleep(sleepTime);
                        	}  
                        }
                        else
                        {
                            invalidScode=true;
                        }
                    }
                }
                while((contextInfo==null || contextInfo.get('lastpage')==false)&&!invalidScode);//loop to get all the data, as REST API give data in pages. (100 Records per page)
                if (invalid)
                {
                    SessionId.storeSessionId(jId,jsessionId); // store new session id and mark inactive the older one.
                    invalid=false;
                }
                //create a list of type Junction_Object_Int__c to insert the data in the database.
                for (i=0;i<mods.size();i++)
                {
                    mod= (Map<String, Object>)mods[i];
                    dte =(String)mod.get('ModifiedDate');
                    dte = '"'+dte+'"';
                    mDt = (DateTime) JSON.deserialize(dte, DateTime.class);
                    offset = -1*tz.getOffset(mDt)/1000;
                    mDt = mDt.addSeconds(offset);
                    if(mod.get('Type')=='MAR Registration')
                    {
                        junList.add(new Junction_Object_Int__c(Name=(String)mod.get('Id'),CRM_Record_Id__c=(String)mod.get('Id'),CRM_Modified_Date__c=mDt,Child_Id__c=(String)mod.get('ContactId'),Child_Name__c='Contact',Event_Name__c='Associate',Object_Id__c=(String)mod.get('CustomObject7Id'),Object_Name__c='MAR',Schedule_Job__c=bj.Id));
                    }
                }
                //code to insert the data.
                Schema.SObjectField crmAccId = Junction_Object_Int__c.Fields.CRM_Record_Id__c;
                List<Database.upsertResult> uResults = Database.upsert(junList,false);
                //error logging while record insertion.
                list<Database.Error> err;
                String msg, fAffected;
                String[]fAff;
                StatusCode sCode;
                list<Error_Logging__c> errList = new List<Error_Logging__c>();
                for(Integer idx = 0; idx < uResults.size(); idx++)
                {   
                    if(!uResults[idx].isSuccess())
                    {
                        err=uResults[idx].getErrors();
                        for (Database.Error er: err)
                        {
                            sCode = er.getStatusCode();
                            msg=er.getMessage();
                            fAff = er.getFields();
                        }
                        fAffected = string.join(fAff,',');
                        errList.add(new Error_Logging__c(Name='Junction_Object_Int__c',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=junList[idx].CRM_Record_Id__c,Schedule_Job__c=bj.Id));
                    }
                }
                Insert errList;
                if(errList.isEmpty())
                {
                    bj.Status__c= 'Completed';
                }
                else
                {
                    bj.Status__c= 'Error';
                }
            }
            catch(Exception e)
            {
                bj.Status__c= 'Error';
                list<Error_Logging__c> errList = new List<Error_Logging__c>();
                errList.add(new Error_Logging__c(Name='ContactRole_Int__c',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),JSON_Output__c=modData,Schedule_Job__c=bj.Id));
                Insert errList;
            }   
            finally
            {
                List<AsyncApexJob> futureCalls = [Select Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate from AsyncApexJob where JobType = 'future' and MethodName='callCRMAPIConRole' order by CreatedDate desc limit 1];
                if(futureCalls.size()>0){
                bj.Batch_Job_ID__c = futureCalls[0].Id;
                bj.End_Time__c = System.now();
                Junction_Object_Int__c junInt = [Select Id, CRM_Modified_Date__c,object_name__c from Junction_Object_Int__c where object_name__c in ('MDU','MAR') order by CRM_Modified_Date__c desc limit 1];
                bj.Next_Request_Start_Time__c=junInt.CRM_Modified_Date__c;// setting the last record modified date as the Next Request Start date (date from which the next job will fetch the records.)
                bj.Record_Count__c=junList.size();
                upsert bj;
                }
            }
    }
  
}