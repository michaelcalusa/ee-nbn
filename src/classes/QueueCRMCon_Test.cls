/***************************************************************************************************
Class Name:  QueueCRMCon_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : 
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Sukumar       26/10/2015                 Created
****************************************************************************************************/
@isTest
private class QueueCRMCon_Test {
    static Extraction_Job__c ExtractionJobRec = new Extraction_Job__c ();
    static Session_Id__c SessionIdRec = new Session_Id__c();
    static String body = '{'+
        '\"Contacts\": ['+
        '{'+
        '\"Id\": \"AYCA-3R1S7I\",'+
        '\"ContactFirstName\": \"Kerry\",'+
        '\"ContactLastName\": \"Bliss\",'+
        '\"AccountName\": \"\",'+
        '\"Description\": \"\",'+
        '\"AccountId\": \"No Match Row Id\",'+
        '\"PIMCompanyName\": \"\",'+
        '\"ContactEmail\": \"\",'+
        '\"CellularPhone\": \"\",'+
        '\"WorkPhone\": \"\",'+
        '\"WorkFax\": \"\",'+
        '\"CustomMultiSelectPickList3\": \"\",'+
        '\"CustomMultiSelectPickList2\": \"\",'+
        '\"CustomObject3Name\": \"\",'+
        '\"HomePhone\": \"\",'+
        '\"MrMrs\": \"\",'+
        '\"ModifiedDate\": \"2016-09-16T17:18:06Z\",'+
        '\"CustomPhone0\": \"\",'+
        '\"CustomObject2Name\": \"\",'+
        '\"CustomText36\": \"\",'+
        '\"AlternateZipCode\": \"2578\",'+
        '\"AlternateCity\": \"Bundanoon\",'+
        '\"AlternateProvince\": \"NSW\",'+
        '\"AlternateCountry\": \"Australia\",'+
        '\"AlternateAddress1\": \"8 Tobin Place\",'+
        '\"AlternateAddress2\": \"\",'+
        '\"AlternateAddress3\": \"\",'+
        '\"CustomMultiSelectPickList5\": \"\",'+
        '\"CustomText37\": \"\",'+
        '\"LeadSource\": \"\",'+
        '\"CustomMultiSelectPickList4\": \"\",'+
        '\"CustomText34\": \"\",'+
        '\"CustomText35\": \"\",'+
        '\"CustomObject1Name\": \"\",'+
        '\"CustomText40\": \"\",'+
        '\"CustomText42\": \"\",'+
        '\"CustomText39\": \"\",'+
        '\"CustomText43\": \"\",'+
        '\"CustomText44\": \"\",'+
        '\"CreatedByFullName\": \"Timea Nadjpal\",'+
        '\"UpdatedByFullName\": \"Timea Nadjpal\",'+
        '\"OptOut\": false,'+
        '\"OwnerFullName\": \"Timea Nadjpal\",'+
        '\"CustomPickList0\": \"\"'+
        '}'+
        '   ],'+
        '\"_contextInfo\": {'+
        '\"limit\": 100,'+
        '\"offset\": 0,'+
        '\"lastpage\": true'+
        '}'+
        '}';
    
    static void getRecords (){  
        // create Extraction_Job record
        ExtractionJobRec = TestDataUtility.createExtractionJob(true);
        // Create Batch_Job record
        Batch_Job__c AccountExtractBatchJob = new Batch_Job__c();
        AccountExtractBatchJob.Type__c='Contact Extract';
        AccountExtractBatchJob.Extraction_Job_ID__c = ExtractionJobRec.Id;
        AccountExtractBatchJob.Batch_Job_ID__c=ExtractionJobRec.Id;
        AccountExtractBatchJob.Next_Request_Start_Time__c=System.now();
        AccountExtractBatchJob.End_Time__c=System.now();
        AccountExtractBatchJob.Status__c='Processing';
        AccountExtractBatchJob.Last_Record__c=True;
        insert AccountExtractBatchJob;
        // Create Session_Id record
        SessionIdRec.Session_Alive__c=true;
        SessionIdRec.CRM_Session_Id__c= userinfo.getSessionId();
        insert SessionIdRec;
    }
    
    static testMethod void QueueCRMAcc_SuccessScenario() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue); 
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMCon.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    
      static testMethod void QueueCRMAcc_ExceptionTest() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        
        body = '{'+
        '\"Contacts\": ['+
        '{'+
        '\"Id\": \"AYCA-3R1S7I-AYCA-3R1S7I\",'+
        '\"ContactFirstName\": \"Kerry\",'+
        '\"ContactLastName\": \"Bliss\",'+
        '\"AccountName\": \"\",'+
        '\"Description\": \"\",'+
        '\"AccountId\": \"No Match Row Id  No Match Row Id\",'+
        '\"PIMCompanyName\": \"\",'+
        '\"ContactEmail\": \"\",'+
        '\"CellularPhone\": \"\",'+
        '\"WorkPhone\": \"\",'+
        '\"WorkFax\": \"\",'+
        '\"CustomMultiSelectPickList3\": \"\",'+
        '\"CustomMultiSelectPickList2\": \"\",'+
        '\"CustomObject3Name\": \"\",'+
        '\"HomePhone\": \"\",'+
        '\"MrMrs\": \"\",'+
        '\"ModifiedDate\": \"2016-09-16T17:18:06Z\",'+
        '\"CustomPhone0\": \"\",'+
        '\"CustomObject2Name\": \"\",'+
        '\"CustomText36\": \"\",'+
        '\"AlternateZipCode\": \"2578\",'+
        '\"AlternateCity\": \"Bundanoon\",'+
        '\"AlternateProvince\": \"NSW\",'+
        '\"AlternateCountry\": \"Australia\",'+
        '\"AlternateAddress1\": \"8 Tobin Place\",'+
        '\"AlternateAddress2\": \"\",'+
        '\"AlternateAddress3\": \"\",'+
        '\"CustomMultiSelectPickList5\": \"\",'+
        '\"CustomText37\": \"\",'+
        '\"LeadSource\": \"\",'+
        '\"CustomMultiSelectPickList4\": \"\",'+
        '\"CustomText34\": \"\",'+
        '\"CustomText35\": \"\",'+
        '\"CustomObject1Name\": \"\",'+
        '\"CustomText40\": \"\",'+
        '\"CustomText42\": \"\",'+
        '\"CustomText39\": \"\",'+
        '\"CustomText43\": \"\",'+
        '\"CustomText44\": \"\",'+
        '\"CreatedByFullName\": \"Timea Nadjpal\",'+
        '\"UpdatedByFullName\": \"Timea Nadjpal\",'+
        '\"OptOut\": false,'+
        '\"OwnerFullName\": \"Timea Nadjpal\",'+
        '\"CustomPickList0\": \"\"'+
        '}'+
        '   ],'+
        '\"_contextInfo\": {'+
        '\"limit\": 100,'+
        '\"offset\": 0,'+
        '\"lastpage\": true'+
        '}'+
        '}';
        
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMCon.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    
    static testMethod void QueueCRMAcc_403StatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 403;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMCon.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMAcc_InvalidStatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 900;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMCon.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
   
}