/***************************************************************************************************
Class Name:  QueueCRMDev_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 04/11/2016
Function    : 
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       04/11/2016                 Created
****************************************************************************************************/
@isTest
private class QueueCRMDev_Test {
    static Extraction_Job__c ExtractionJobRec = new Extraction_Job__c ();
    static Session_Id__c SessionIdRec = new Session_Id__c();
    static string body = '{'+
        '   \"CustomObjects2\":['+
        '    {'+
        '         \"Id\": \"AYCA-3QA3EI\",'+
        '         \"Name\": \"60 & 70 Queen Street\",'+
        '         \"Type\": \"Medium/Large\",'+
        '         \"CustomPickList26\": \"\",'+
        '         \"AccountName\": \"Darazzo Pty Ltd ATF The Queen Street Unit Trust\",'+
        '         \"AccountId\": \"AYCA-3Q9SGG\",'+
        '         \"ContactId\": \"AYCA-2AI5J3\",'+
        '         \"Owner\": \"Claire McGregor\",'+
        '         \"CustomText36\": \"-34.932064\",'+
        '         \"CustomText32\": \"138.604627\",'+
        '         \"CustomText46\": \"\",'+
        '         \"ContactFullName\": \"Claire Mcgregor\",'+
        '         \"CustomPickList3\": \"MDA\",'+
        '         \"CustomPickList10\": \"Closed\",'+
        '         \"CustomText0\": \"60-70 Queen Street\",'+
        '         \"CustomText34\": \"Adelaide\",'+
        '         \"CustomPickList8\": \"SA\",'+
        '         \"CustomText37\": \"5000\",'+
        '         \"CustomText4\": \"60 - 70 Queen Street, Adelaide SA\",'+
        '         \"CustomText1\": \"\",'+
        '         \"CustomObject1Name\": \"\",'+
        '         \"CustomObject4Name\": \"\",'+
        '         \"CustomPickList31\": \"\",'+
        '         \"IndexedPick1\": \"Development Withdrawn\",'+
        '         \"IndexedPick0\": \"Not Started\",'+
        '         \"CustomNumber4\": 0,'+
        '         \"CustomInteger2\": 50,'+
        '         \"IndexedPick2\": \"\",'+
        '         \"CustomDate33\": \"\",'+
        '         \"CustomInteger7\": 0,'+
        '         \"CustomPickList22\": \"Yes\",'+
        '         \"ModifiedDate\": \"2016-09-16T17:20:03Z\"'+
        '    }'+
        '   ],'+
        '   \"_contextInfo\":    {'+
        '      \"limit\": 100,'+
        '      \"offset\": 0,'+
        '      \"lastpage\": true'+
        '   }'+
        '}';
    static void getRecords (){  
        // create Extraction_Job record
        ExtractionJobRec = TestDataUtility.createExtractionJob(true);
        // Create Batch_Job record
        Batch_Job__c DevelopmentExtractBatchJob = new Batch_Job__c();
        DevelopmentExtractBatchJob.Type__c='Development Extract';
        DevelopmentExtractBatchJob.Extraction_Job_ID__c = ExtractionJobRec.Id;
        DevelopmentExtractBatchJob.Batch_Job_ID__c=ExtractionJobRec.Id;
        DevelopmentExtractBatchJob.Next_Request_Start_Time__c=System.now();
        DevelopmentExtractBatchJob.End_Time__c=System.now();
        DevelopmentExtractBatchJob.Status__c='Processing';
        DevelopmentExtractBatchJob.Last_Record__c=true;
        insert DevelopmentExtractBatchJob;
        // Create Session_Id record
        SessionIdRec.Session_Alive__c=true;
        SessionIdRec.CRM_Session_Id__c= userinfo.getSessionId();
        insert SessionIdRec;
    }
    static testMethod void QueueCRMDev_SuccessScenario() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMDev.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMDev_ExceptionTest() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        body = '{'+
        '   \"CustomObjects2\":['+
        '    {'+
        '         \"Id\": \"AYCA-3QA3EI\",'+
        '         \"Name\": \"60 & 70 Queen Street\",'+
        '         \"Type\": \"Medium/Large\",'+
        '         \"CustomPickList26\": \"\",'+
        '         \"AccountName\": \"Darazzo Pty Ltd ATF The Queen Street Unit Trust\",'+
        '         \"AccountId\": \"AYCA-3Q9SGG\",'+
        '         \"ContactId\": \"AYCA-2AI5J3\",'+
        '         \"Owner\": \"Claire McGregor\",'+
        '         \"CustomText36\": \"-34.932064\",'+
        '         \"CustomText32\": \"138.604627\",'+
        '         \"CustomText46\": \"\",'+
        '         \"ContactFullName\": \"Claire Mcgregor\",'+
        '         \"CustomPickList3\": \"MDA\",'+
        '         \"CustomPickList10\": \"Closed\",'+
        '         \"CustomText0\": \"60-70 Queen Street\",'+
        '         \"CustomText34\": \"Adelaide\",'+
        '         \"CustomPickList8\": \"SA\",'+
        '         \"CustomText37\": \"5000\",'+
        '         \"CustomText4\": \"60 - 70 Queen Street, Adelaide SA\",'+
        '         \"CustomText1\": \"\",'+
        '         \"CustomObject1Name\": \"\",'+
        '         \"CustomObject4Name\": \"\",'+
        '         \"CustomPickList31\": \"\",'+
        '         \"IndexedPick1\": \"Development Withdrawn\",'+
        '         \"IndexedPick0\": \"Not Started\",'+
        '         \"CustomNumber4\": 0,'+
        '         \"CustomInteger2\": 50,'+
        '         \"IndexedPick2\": \"\",'+
        '         \"CustomDate33\": \"\",'+
        '         \"CustomInteger7\": 0,'+
        '         \"CustomPickList22\": \"Yes\"'+
        '    }'+
        '   ],'+
        '   \"_contextInfo\":    {'+
        '      \"limit\": 100,'+
        '      \"offset\": 0,'+
        '      \"lastpage\": true'+
        '   }'+
        '}';
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMDev.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMDev_403StatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 403;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMDev.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMDev_InvalidStatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 900;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMDev.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMDev_Failure() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        body = '{'+
        '   \"CustomObjects2\":['+
        '    {'+
        '         \"Id\": \"AYCA-3QA3EIAYCA-3QA3EIAYCA-3QA3EIAYCA-3QA3EIAYCA-3QA3EIAYCA-3QA3EI\",'+
        '         \"Name\": \"60 & 70 Queen Street\",'+
        '         \"Type\": \"Medium/Large\",'+
        '         \"CustomPickList26\": \"\",'+
        '         \"AccountName\": \"Darazzo Pty Ltd ATF The Queen Street Unit Trust\",'+
        '         \"AccountId\": \"AYCA-3Q9SGG\",'+
        '         \"ContactId\": \"AYCA-2AI5J3\",'+
        '         \"Owner\": \"Claire McGregor\",'+
        '         \"CustomText36\": \"-34.932064\",'+
        '         \"CustomText32\": \"138.604627\",'+
        '         \"CustomText46\": \"\",'+
        '         \"ContactFullName\": \"Claire Mcgregor\",'+
        '         \"CustomPickList3\": \"MDA\",'+
        '         \"CustomPickList10\": \"Closed\",'+
        '         \"CustomText0\": \"60-70 Queen Street\",'+
        '         \"CustomText34\": \"Adelaide\",'+
        '         \"CustomPickList8\": \"SA\",'+
        '         \"CustomText37\": \"5000\",'+
        '         \"CustomText4\": \"60 - 70 Queen Street, Adelaide SA\",'+
        '         \"CustomText1\": \"\",'+
        '         \"CustomObject1Name\": \"\",'+
        '         \"CustomObject4Name\": \"\",'+
        '         \"CustomPickList31\": \"\",'+
        '         \"IndexedPick1\": \"Development Withdrawn\",'+
        '         \"IndexedPick0\": \"Not Started\",'+
        '         \"CustomNumber4\": 0,'+
        '         \"CustomInteger2\": 50,'+
        '         \"IndexedPick2\": \"\",'+
        '         \"CustomDate33\": \"\",'+
        '         \"CustomInteger7\": 0,'+
        '         \"CustomPickList22\": \"Yes\",'+
        '         \"ModifiedDate\": \"2016-09-16T17:20:03Z\"'+
        '    }'+
        '   ],'+
        '   \"_contextInfo\":    {'+
        '      \"limit\": 100,'+
        '      \"offset\": 0,'+
        '      \"lastpage\": true'+
        '   }'+
        '}';
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMDev.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
}