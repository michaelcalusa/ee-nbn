/***************************************************************************************************
Class Name:  QueueCRMJobs_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 04/11/2016
Function    : 
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       04/11/2016                 Created
****************************************************************************************************/
@isTest
private class QueueCRMJobs_Test {
    static Extraction_Job__c ExtractionJobRec = new Extraction_Job__c ();
    static Session_Id__c SessionIdRec = new Session_Id__c();
    static string body  = '{'+
        '   \"ModificationTrackings\":    ['+
        '    {'+
        '         \"Id\": \"AYCA-3T4O99\",'+
        '         \"ModifiedDate\": \"2016-10-04T08:20:20Z\",'+
        '         \"ObjectName\": \"CustomObject3\",'+
        '         \"ObjectId\": \"AYCA-310P9W\",'+
        '         \"EventName\": \"Dissociate\",'+
        '         \"ChildId\": \"AYCA-1X9NAE\",'+
        '         \"ChildName\": \"Contact\"'+
        '    }'+
        '   ],'+
        '   \"_contextInfo\":    {'+
        '      \"limit\": 100,'+
        '      \"offset\": 0,'+
        '      \"lastpage\": true'+
        '   }'+
        '}';
    static void getRecords (){  
        // create Extraction_Job record
        ExtractionJobRec = TestDataUtility.createExtractionJob(true);
        // Create Batch_Job record
        Batch_Job__c ModificationExtractBatchJob = new Batch_Job__c();
        ModificationExtractBatchJob.Type__c='Modification Extract';
        ModificationExtractBatchJob.Extraction_Job_ID__c = ExtractionJobRec.Id;
        ModificationExtractBatchJob.Batch_Job_ID__c=ExtractionJobRec.Id;
        ModificationExtractBatchJob.Next_Request_Start_Time__c=System.now();
        ModificationExtractBatchJob.End_Time__c=System.now();
        ModificationExtractBatchJob.Status__c='Processing';
        ModificationExtractBatchJob.Last_Record__c=true;
        insert ModificationExtractBatchJob;
        // Create Session_Id record
        SessionIdRec.Session_Alive__c=true;
        SessionIdRec.CRM_Session_Id__c= userinfo.getSessionId();
        insert SessionIdRec;
    }
    static testMethod void QueueCRMDev_SuccessScenario() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMJobs.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMDev_ExceptionTest() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        body = '{'+
        '   \"ModificationTrackings\":    ['+
        '    {'+
        '         \"Id\": \"AYCA-3T4O99\",'+
        '         \"ObjectName\": \"CustomObject3\",'+
        '         \"ObjectId\": \"AYCA-310P9W\",'+
        '         \"EventName\": \"Dissociate\",'+
        '         \"ChildId\": \"AYCA-1X9NAE\",'+
        '         \"ChildName\": \"Contact\"'+
        '    }'+
        '   ],'+
        '   \"_contextInfo\":    {'+
        '      \"limit\": 100,'+
        '      \"offset\": 0,'+
        '      \"lastpage\": true'+
        '   }'+
        '}';
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMJobs.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMDev_403StatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 403;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMJobs.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMDev_InvalidStatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 900;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMJobs.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMDev_Failure() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        body = '{'+
        '   \"ModificationTrackings\":    ['+
        '    {'+
        '         \"Id\": \"AYCA-3T4O99AYCA-3T4O99AYCA-3T4O99AYCA-3T4O99AYCA-3T4O99AYCA-3T4O99AYCA-3T4O99\",'+
        '         \"ModifiedDate\": \"2016-10-04T08:20:20Z\",'+
        '         \"ObjectName\": \"CustomObject3\",'+
        '         \"ObjectId\": \"AYCA-310P9W\",'+
        '         \"EventName\": \"Dissociate\",'+
        '         \"ChildId\": \"AYCA-1X9NAE\",'+
        '         \"ChildName\": \"Contact\"'+
        '    }'+
        '   ],'+
        '   \"_contextInfo\":    {'+
        '      \"limit\": 100,'+
        '      \"offset\": 0,'+
        '      \"lastpage\": true'+
        '   }'+
        '}';
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMJobs.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
}