public class QueueCRMMAR implements Database.AllowsCallouts
{
    /*----------------------------------------------------------------------------------------
    Author:        Dilip Athley (v-dileepathley)
    Company:       NBNco
    Description:   This class will be used to Extract the Account Records from CRMOD.
    Test Class:    
    History
    <Date>            <Authors Name>    <Brief Description of Change> 
    
    -----------------------------------------------------------------------------------------*/
    //Created the below class to through the custom exceptions
    public class GeneralException extends Exception
            {}
    // this method will be called by the ScheduleCRMExt class, which will create a new batch job (Schedule job) record and call the other methos to query the data from CRMOD.
    public static void execute(String ejId)
    {
       Batch_Job__c bjPre  = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Type__c='MAR Extract' order by createddate desc limit 1]; 
       // creating the new batch job(Schedule Job) record for this batch.
       Batch_Job__c bj = new Batch_Job__c();
       bj.Name = 'MARExtract'+System.now();  // Extraction Job ID
       bj.Start_Time__c = System.now();
       bj.Extraction_Job_ID__c = ejId;
       bj.Status__c = 'Processing';
       bj.Batch_Job_Name__c = 'MARExtract'+System.now();
       bj.Type__c='MAR Extract';
       bjPre.Last_Record__c = true;
       list <Batch_Job__c> batch = new list <Batch_Job__c>();
       batch.add(bjPre);
       batch.add(bj);
       Upsert batch;
        //Calling the method to extract the data from CRM on Demand.
       QueueCRMMAR.callCRMAPIMAR(string.valueof(bj.name));
    }
    // This method will be used to query the data from CRM On Demand and insert the records in Account_Int table.
    @future(callout=true)
    public static void callCRMAPIMAR(String bjName)
    {
            Integer num = 0;
            Map<String, Object> deserializedMAR;
            list<Object> mars = new List<Object>();
            Map <String, Object> mar;
            Map <String, Object> contextinfo;
            Integer i,offset;
            DateTime mDt,lastDate;
            String dte,marData;
            TimeZone tz = UserInfo.getTimeZone();
            list<MAR_Int__c> marList = new List<MAR_Int__c>();
            Batch_Job__c bj  = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c from Batch_Job__c where Name =:bjName];
            String jsessionId;
            Boolean invalid =false, invalidScode=false;
            Integer session =0,invalidCode =0;
            Long sleepTime;
        //variable declaration finished     
            try
            {
                //query the previously ran batch job to get the last records modified Date.
                Batch_Job__c bjPre = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Next_Request_Start_Time__c,Last_Record__c from Batch_Job__c where Type__c='MAR Extract' and Last_Record__c= true order by createddate desc limit 1];
                //format the extracted date to the format accepted by CRMOD REST API.
                String lastRecDate = bjPre.Next_Request_Start_Time__c.format('yyyy-MM-dd\'T\'HH:mm:ss');
                lastRecDate = lastRecDate+'Z';
                //Form the end point URL, please note the initial part is in Named Credential.
                String serviceEndpoint= '?orderBy=ModifiedDate:asc&onlyData=True&fields=Id,AccountId,CustomText36,CustomText34,ContactId,CustomPickList3,ModifiedDate,IndexedPick2,Description,CustomPickList0,CustomBoolean1,QuickSearch1,CustomBoolean0,CustomText2,CustomNumber0,CustomNumber1,CustomText31,ExchangeDate,Name,CustomPickList2,CustomPickList1,CustomText33,Type,CustomBoolean3,CustomBoolean2&q=ModifiedDate%3E'+'\''+lastRecDate+'\'&offset=';
                //get the active Session Id from the Session Id table.
                Session_Id__c jId = [select CRM_Session_Id__c from Session_Id__c where Session_Alive__c=true order by createddate desc limit 1];
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                HttpResponse res;
                req.setMethod('GET');
                req.setHeader('Cookie', jId.CRM_Session_Id__c);
                req.setTimeout(120000);
                req.setHeader('Accept', 'application/json');
                do
                {
                    //extract the data from CRMOD
                    req.setEndpoint('callout:CRMOD_MAR'+serviceEndpoint+num);
                    res = http.send(req);
                    marData = res.getBody();
                    if(res.getStatusCode()==200 && ValidateJSON.validate(marData))// ValidateJSON.validate(accData) will validate the output is in JSON format or not.
                    {
                        //deserialize the data.
                        deserializedMAR =(Map<String, Object>)JSON.deserializeUntyped(marData);
                        if(num==0)
                        {
                            mars = (list<Object>)deserializedMAR.get('CustomObjects7');
                        }
                        else
                        {
                            mars.addAll((list<Object>)deserializedMAR.get('CustomObjects7'));
                        }
                        contextinfo = (Map<String, Object>)deserializedMAR.get('_contextInfo');
                        num=num+100;
                        if(num>3000)
                        {
                            invalidScode=true;
                        }
                    }
                    else if (res.getStatusCode()==403)//if 403 then establish a new session
                    {
                        session = session+1;
                        if (session<=3)
                        {
                            jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                            req.setHeader('Cookie', jsessionId);
                            invalid =true;
                        }
                        else
                        {
                            throw new GeneralException('Not able to establish a session with CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                        }
                    }
                    else //if StatusCode is other then 200 or 403 then retry 3 times and then throw an exception.
                    {
                       if(mars.isEmpty())
                        {
                            invalidCode = invalidCode+1;
                        	if (invalidCode>3)
                        	{
                                throw new GeneralException('Not able to get the data from CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                            }
                            else
                       		{
                            	sleepTime=10000*invalidCode; // As per the Oracle's advice if the Status Code is not valid next request has to wait for some time.
                            	new Sleep(sleepTime);
                        	}  
                        }
                        else
                        {
                            invalidScode=true;
                        }
                    }
                }
                while((contextInfo==null || contextInfo.get('lastpage')==false)&&!invalidScode);
                if (invalid)
                {
                    SessionId.storeSessionId(jId,jsessionId); // store new session id and mark inactive the older one.
                    invalid=false;
                }
                //create a list of type MAR_Int__c to insert the data in the database.
                for (i=0;i<mars.size();i++)
                {
                    mar= (Map<String, Object>)mars[i];
                    dte =(String)mar.get('ModifiedDate');
                    dte = '"'+dte+'"';
                    mDt = (DateTime) JSON.deserialize(dte, DateTime.class);
                    offset = -1*tz.getOffset(mDt)/1000;
                    mDt = mDt.addSeconds(offset);
                    marList.add(new MAR_Int__c(Name = (String)mar.get('Id'),Account_Id__c=(String)mar.get('AccountId'),Address_1__c=(String)mar.get('CustomText36'),
                    Address_2__c=(String)mar.get('CustomText34'),Contact_Id__c=(String)mar.get('ContactId'),Contact_Role__c=(String)mar.get('CustomPickList3'),CRM_Modified_Date__c=mDt,
                    Data_Source__c=(String)mar.get('IndexedPick2'),Description__c=(String)mar.get('Description'),How_did_you_hear__c=(String)mar.get('CustomPickList0'),
                    Locally_Monitored__c=(Boolean)mar.get('CustomBoolean1'),Location_ID__c=(String)mar.get('QuickSearch1'),Monitored__c=(Boolean)mar.get('CustomBoolean0'),
                    Name_of_Retirement_Village__c=(String)mar.get('CustomText2'),Number_of_Units_in_Retirement_Village__c=(Decimal)mar.get('CustomNumber0'),
                    Number_of_Units_with_Medical_Alarm__c=(Decimal)mar.get('CustomNumber1'),Post_Code__c=(String)mar.get('CustomText31'),Provided_Date__c=(String)mar.get('ExchangeDate'),
                    Registration_ID__c=(String)mar.get('Name'),Registration_Type__c=(String)mar.get('CustomPickList2'),State__c=(String)mar.get('CustomPickList1'),Suburb__c=(String)mar.get('CustomText33'),
                    Type__c=(String)mar.get('Type'),Unknown__c=(Boolean)mar.get('CustomBoolean3'),Unmonitored__c=(Boolean)mar.get('CustomBoolean2'),Schedule_Job__c=bj.Id));    
                }
                //code to insert the data.
                List<Database.upsertResult> uResults = Database.upsert(marList,false);
                //error logging while record insertion.
                list<Database.Error> err;
                String msg, fAffected;
                String[]fAff;
                StatusCode sCode;
                list<Error_Logging__c> errList = new List<Error_Logging__c>();
                for(Integer idx = 0; idx < uResults.size(); idx++)
                {   
                    if(!uResults[idx].isSuccess())
                    {
                        err=uResults[idx].getErrors();
                        for (Database.Error er: err)
                        {
                            sCode = er.getStatusCode();
                            msg=er.getMessage();
                            fAff = er.getFields();
                        }
                        fAffected = string.join(fAff,',');
                        errList.add(new Error_Logging__c(Name='MAR_Int__c',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=marList[idx].Name,Schedule_Job__c=bj.Id));
                    }
                }
                Insert errList;
                if(errList.isEmpty())
                {
                    bj.Status__c= 'Completed';
                }
                else
                {
                    bj.Status__c= 'Error';
                }
            }
            catch(Exception e)
            {
                bj.Status__c= 'Error';
                list<Error_Logging__c> errList = new List<Error_Logging__c>();
                errList.add(new Error_Logging__c(Name='MAR_Int__c',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),JSON_Output__c=marData,Schedule_Job__c=bj.Id));
                Insert errList;
            }   
            finally
            {
                List<AsyncApexJob> futureCalls = [Select Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate from AsyncApexJob where JobType = 'future' and MethodName='callCRMAPIMAR' order by CreatedDate desc limit 1];
                If(futureCalls.size()>0){
                    bj.Batch_Job_ID__c = futureCalls[0].Id;
                    bj.End_Time__c = System.now();
                    MAR_Int__c marInt = [Select Id,CRM_Modified_Date__c from MAR_Int__c order by CRM_Modified_Date__c desc limit 1];
                    bj.Next_Request_Start_Time__c=marInt.CRM_Modified_Date__c;// setting the last record modified date as the Next Request Start date (date from which the next job will fetch the records.)
                    bj.Record_Count__c=marList.size();
                    upsert bj;
                }
            }
    }
    
   
}