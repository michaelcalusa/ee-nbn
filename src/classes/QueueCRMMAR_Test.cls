/***************************************************************************************************
Class Name:  QueueCRMMAR_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : 
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Sukumar       26/10/2015                 Created
****************************************************************************************************/
@isTest
private class QueueCRMMAR_Test {
    static Extraction_Job__c ExtractionJobRec = new Extraction_Job__c ();
    static Session_Id__c SessionIdRec = new Session_Id__c();
    static String body = '{'+
        '\"CustomObjects7\": ['+
        '{'+
        '\"Id\": \"AYCA-3R20KU\",'+
        '\"AccountId\": \"\",'+
        '\"CustomText36\": \"\",'+
        '\"CustomText34\": \"\",'+
        '\"ContactId\": \"AYCA-3R1VYS\",'+
        '\"CustomPickList3\": \"<No Values>\",'+
        '\"ModifiedDate\": \"2016-09-16T17:46:22Z\",'+
        '\"IndexedPick2\": \"Web - Online Registration\",'+
        '\"Description\": \"\",'+
        '\"CustomPickList0\": \"\",'+
        '\"CustomBoolean1\": false,'+
        '\"QuickSearch1\": \"LOC000030820708\",'+
        '\"CustomBoolean0\": true,'+
        '\"CustomText2\": \"\",'+
        '\"CustomNumber0\": 0,'+
        '\"CustomNumber1\": 0,'+
        '\"CustomText31\": \"\",'+
        '\"ExchangeDate\": \"2016-09-16\",'+
        '\"Name\": \"AYCA-3R20KU\",'+
        '\"CustomPickList2\": \"Individual\",'+
        '\"CustomPickList1\": \"\",'+
        '\"CustomText33\": \"\",'+
        '\"Type\": \"\",'+
        '\"CustomBoolean3\": false,'+
        '\"CustomBoolean2\": false'+
        '}'+
        '],'+
        '\"_contextInfo\": {'+
        '\"limit\": 100,'+
        '\"offset\": 0,'+
        '\"lastpage\": true'+
        '}'+
        '}';
    
    static void getRecords (){  
        // create Extraction_Job record
        ExtractionJobRec = TestDataUtility.createExtractionJob(true);
        // Create Batch_Job record
        Batch_Job__c AccountExtractBatchJob = new Batch_Job__c();
        AccountExtractBatchJob.Type__c='MAR Extract';
        AccountExtractBatchJob.Extraction_Job_ID__c = ExtractionJobRec.Id;
        AccountExtractBatchJob.Batch_Job_ID__c=ExtractionJobRec.Id;
        AccountExtractBatchJob.Next_Request_Start_Time__c=System.now();
        AccountExtractBatchJob.End_Time__c=System.now();
        AccountExtractBatchJob.Status__c='Processing';
        AccountExtractBatchJob.Last_Record__c=True;
        insert AccountExtractBatchJob;
        // Create Session_Id record
        SessionIdRec.Session_Alive__c=true;
        SessionIdRec.CRM_Session_Id__c= userinfo.getSessionId();
        insert SessionIdRec;
    }
    
    static testMethod void QueueCRMAcc_SuccessScenario() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue); 
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMMAR.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    
      static testMethod void QueueCRMAcc_ExceptionTest() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        
        body = '{'+
        '\"CustomObjects7\": ['+
        '{'+
        '\"Id\": \"AYCA-3R20KU\",'+
        '\"AccountId\": \"\",'+
        '\"CustomText36\": \"\",'+
        '\"CustomText34\": \"\",'+
        '\"ContactId\": \"AYCA-3R1VYS-AYCA-3R1VYS\",'+
        '\"CustomPickList3\": \"<No Values>\",'+
        '\"ModifiedDate\": \"2016-09-16T17:46:22Z\",'+
        '\"IndexedPick2\": \"Web - Online Registration\",'+
        '\"Description\": \"\",'+
        '\"CustomPickList0\": \"\",'+
        '\"CustomBoolean1\": false,'+
        '\"QuickSearch1\": \"LOC000030820708\",'+
        '\"CustomBoolean0\": true,'+
        '\"CustomText2\": \"\",'+
        '\"CustomNumber0\": 0,'+
        '\"CustomNumber1\": 0,'+
        '\"CustomText31\": \"\",'+
        '\"ExchangeDate\": \"2016-09-16\",'+
        '\"Name\": \"AYCA-3R20KU\",'+
        '\"CustomPickList2\": \"Individual\",'+
        '\"CustomPickList1\": \"\",'+
        '\"CustomText33\": \"\",'+
        '\"Type\": \"\",'+
        '\"CustomBoolean3\": false,'+
        '\"CustomBoolean2\": false'+
        '}'+
        '],'+
        '\"_contextInfo\": {'+
        '\"limit\": 100,'+
        '\"offset\": 0,'+
        '\"lastpage\": true'+
        '}'+
        '}';
        
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMMAR.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    
    static testMethod void QueueCRMAcc_403StatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 403;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMMAR.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMAcc_InvalidStatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 900;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMMAR.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
   
}