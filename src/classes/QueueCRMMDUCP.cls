public class QueueCRMMDUCP implements Database.AllowsCallouts
{
    /*----------------------------------------------------------------------------------------
    Author:        Dilip Athley (v-dileepathley)
    Company:       NBNco
    Description:   This class will be used to Extract the MDU/CP Records from CRMOD.
    Test Class:    
    History
    <Date>            <Authors Name>    <Brief Description of Change> 
    
    -----------------------------------------------------------------------------------------*/
    /*********************************************************************
    US#4118: Below lines have been commented to decommission P2P jobs.
    Author: Jairaj Jadhav
    Company: Wipro Technologies
    
    //Created the below class to through the custom exceptions
    public class GeneralException extends Exception
            {}
   // this method will be called by the ScheduleCRMExt class, which will create a new batch job (Schedule job) record and call the other methos to query the data from CRMOD.
    public static void execute(String ejId)
    {
       Batch_Job__c bjPre  = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Type__c='MDU Extract' order by createddate desc limit 1]; 
       // creating the new batch job(Schedule Job) record for this batch.   
        Batch_Job__c bj = new Batch_Job__c();
       bj.Name = 'MDUExtract'+System.now();  
       bj.Start_Time__c = System.now();
       bj.Extraction_Job_ID__c = ejId;
       bj.Status__c = 'Processing';
       bj.Batch_Job_Name__c = 'MDUExtract'+System.now();
       bj.Type__c='MDU Extract';
       bjPre.Last_Record__c = true;
       list <Batch_Job__c> batch = new list <Batch_Job__c>();
       batch.add(bjPre);
       batch.add(bj);
       Upsert batch;
        //Calling the method to extract the data from CRM on Demand.
       QueueCRMMDUCP.callCRMAPIMDUCP(string.valueof(bj.name));
    }
    // This method will be used to query the data from CRM On Demand and insert the records in MDU_CP_Int table.
     @future(callout=true)
    public static void callCRMAPIMDUCP(String bjName)
    {
            
            list<MDU_CP_Int__c> mduList = new List<MDU_CP_Int__c>();
            Integer num = 0;
            Map<String, Object> deserializedMDU;
            list<Object> mdus = new List<Object>();
            Map <String, Object> mdu;
            Map <String, Object> contextinfo;
            Integer i,offset;
            DateTime mDt,lastDate;
            String dte,mduData;
            TimeZone tz = UserInfo.getTimeZone();
            
            Batch_Job__c bj  = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c from Batch_Job__c where Name =:bjName];
            String jsessionId;
            Boolean invalid =false, invalidScode=false,lastRec = false;
            Integer session =0,invalidCode =0;
            Long sleepTime;
        //variable declaration finished     
            try
            {
                //query the previously ran batch job to get the last records modified Date.
                Batch_Job__c bjPre = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Next_Request_Start_Time__c,Last_Record__c from Batch_Job__c where Type__c='MDU Extract' and Last_Record__c= true order by createddate desc limit 1];
                //format the extracted date to the format accepted by CRMOD REST API.
                String lastRecDate = bjPre.Next_Request_Start_Time__c.format('yyyy-MM-dd\'T\'HH:mm:ss');
                lastRecDate = lastRecDate+'Z';      
                //Form the end point URL, please note the initial part is in Named Credential.
                String serviceEndpoint= '?orderBy=ModifiedDate:asc&onlyData=True&fields=CustomText10,AccountId,IndexedPick1,Name,ContactId,CustomPickList5,Id,CustomBoolean1,IndexedPick0,CustomDate5,CustomObject1Name,CustomText39,CustomPickList4,CustomNote0,CustomText44,CustomBoolean2,CustomText43,CustomInteger6,CustomInteger9,CustomInteger8,CustomInteger7,CustomInteger12,CustomInteger10,CustomInteger11,CustomInteger1,CustomInteger0,CustomPickList2,CustomText33,CustomText34,CustomPickList1,QuickSearch2,QuickSearch1,CustomText30,CustomPickList8,Type,CustomText31,CustomObject4Name,CustomText32,CustomText13,CustomBoolean3,ModifiedDate&q=ModifiedDate%3E'+'\''+lastRecDate+'\'&offset=';
                //get the active Session Id from the Session Id table.
                Session_Id__c jId = [select CRM_Session_Id__c from Session_Id__c where Session_Alive__c=true order by createddate desc limit 1];
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                HttpResponse res;
                req.setMethod('GET');
                req.setHeader('Cookie', jId.CRM_Session_Id__c);
                req.setTimeout(120000);
                req.setHeader('Accept', 'application/json');
                do
                {
                    //extract the data from CRMOD
                    req.setEndpoint('callout:CRMOD_MDU_CP'+serviceEndpoint+num);
                    res = http.send(req);
                    mduData = res.getBody();
                    if(res.getStatusCode()==200 && ValidateJSON.validate(mduData))// ValidateJSON.validate(accData) will validate the output is in JSON format or not.
                    {
                        //deserialize the data.
                        deserializedMDU =(Map<String, Object>)JSON.deserializeUntyped(mduData);
                        if(num==0)
                        {
                            mdus = (list<Object>)deserializedMDU.get('CustomObjects15');
                        }
                        else
                        {
                            mdus.addAll((list<Object>)deserializedMDU.get('CustomObjects15'));
                        }
                        contextinfo = (Map<String, Object>)deserializedMDU.get('_contextInfo');
                        num=num+100;
                        if(num>3000)
                        {
                            invalidScode=true;
                        }
                    }
                    else if (res.getStatusCode()==403)//if 403 then establish a new session
                    {
                        session = session+1;
                        if (session<=3)
                        {
                            jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                            req.setHeader('Cookie', jsessionId);
                            invalid =true;
                        }
                        else
                        {
                            throw new GeneralException('Not able to establish a session with CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                        }
                    }
                    else  //if StatusCode is other then 200 or 403 then retry 3 times and then throw an exception.
                    {
                        if(mdus.isEmpty())
                        {
                            invalidCode = invalidCode+1;
                            if (invalidCode>3)
                            {
                                throw new GeneralException('Not able to get the data from CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                            }
                            else
                            {
                                sleepTime=10000*invalidCode; // As per the Oracle's advice if the Status Code is not valid next request has to wait for some time.
                                new Sleep(sleepTime);
                            }  
                        }
                        else
                        {
                            invalidScode=true;
                        }
                    }
                }
                while((contextInfo==null || contextInfo.get('lastpage')==false)&&!invalidScode);//loop to get all the data, as REST API give data in pages. (100 Records per page)
                if (invalid)
                {
                    SessionId.storeSessionId(jId,jsessionId); // store new session id and mark inactive the older one.
                    invalid=false;
                }
                //create a list of type MDU_CP_Int__c to insert the data in the database.
                for (i=0;i<mdus.size();i++)
                {
                    mdu= (Map<String, Object>)mdus[i];
                    dte =(String)mdu.get('ModifiedDate');
                    dte = '"'+dte+'"';
                    mDt = (DateTime) JSON.deserialize(dte, DateTime.class);
                    offset = -1*tz.getOffset(mDt)/1000;
                    mDt = mDt.addSeconds(offset);
                    mduList.add(new MDU_CP_Int__c(Access_Information__c=(String)mdu.get('CustomText10'),Account_Id__c=(String)mdu.get('AccountId'),Active_Status__c=(String)mdu.get('IndexedPick1'),
                    Building_ID__c=(String)mdu.get('Name'),Contact_Id__c=(String)mdu.get('ContactId'),CRM_Modified_Date__c=mDt,Contractor_Assigned__c=(String)mdu.get('CustomPickList5'),
                    CRM_Record_Id__c=(String)mdu.get('Id'),Crown_Land__c=(Boolean)mdu.get('CustomBoolean1'),Engagement_Progress__c=(String)mdu.get('IndexedPick0'),
                    Frustrated_Active_Status__c=(String)mdu.get('CustomDate5'),FSA__c=(String)mdu.get('CustomObject1Name'),HEC_Overlay__c=(String)mdu.get('CustomText39'),
                    Land_Use__c=(String)mdu.get('CustomPickList4'),Legal_Owner_Address__c=(String)mdu.get('CustomNote0'),Legal_Owner_Addressee__c=(String)mdu.get('CustomText44'),
                    Maintenance__c=(Boolean)mdu.get('CustomBoolean2'),MPS_Id__c=(String)mdu.get('CustomText43'),No_of_ATMs__c=(Integer)mdu.get('CustomInteger6'),
                    No_of_Emergency_Services__c=(Integer)mdu.get('CustomInteger9'),No_of_Fire_Services__c=(Integer)mdu.get('CustomInteger8'),
                    No_of_Lifts__c=(Integer)mdu.get('CustomInteger7'),No_of_Other_Non_Premises__c=(Integer)mdu.get('CustomInteger12'),No_of_Security_Services__c=(Integer)mdu.get('CustomInteger10'),
                    No_of_Vending_Machines__c=(Integer)mdu.get('CustomInteger11'),Number_of_Levels__c=(Integer)mdu.get('CustomInteger1'),Number_of_Premises__c=(Integer)mdu.get('CustomInteger0'),
                    On_Hold_Reason__c=(String)mdu.get('CustomPickList2'),Physical_City__c=(String)mdu.get('CustomText33'),Physical_Post_Code__c=(String)mdu.get('CustomText34'),
                    Physical_State__c=(String)mdu.get('CustomPickList1'),Physical_Street_Name__c=(String)mdu.get('QuickSearch2'),Physical_Street_Number__c=(String)mdu.get('QuickSearch1'),
                    Plan_Number__c=(String)mdu.get('CustomText30'),Program_Manager__c=(String)mdu.get('CustomPickList8'),Program_Type__c=(String)mdu.get('Type'),RCP__c=(String)mdu.get('CustomText31'),
                    SAM__c=(String)mdu.get('CustomObject4Name'),Service_Area__c=(String)mdu.get('CustomText32'),Site_Name__c=(String)mdu.get('CustomText13'),
                    Vegetation_Tree_Lopping__c=(Boolean)mdu.get('CustomBoolean3'),Schedule_Job__c=bj.Id));
                }
                //code to insert the data.
                List<Database.upsertResult> uResults = Database.upsert(mduList,false);
                list<Database.Error> err;
                //error logging while record insertion.
                String msg, fAffected;
                String[]fAff;
                StatusCode sCode;
                list<Error_Logging__c> errList = new List<Error_Logging__c>();
                for(Integer idx = 0; idx < uResults.size(); idx++)
                {   
                    if(!uResults[idx].isSuccess())
                    {
                        err=uResults[idx].getErrors();
                        for (Database.Error er: err)
                        {
                            sCode = er.getStatusCode();
                            msg=er.getMessage();
                            fAff = er.getFields();
                        }
                        fAffected = string.join(fAff,',');
                        errList.add(new Error_Logging__c(Name='MDU_Int__c',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=mduList[idx].CRM_Record_Id__c,Schedule_Job__c=bj.Id));
                    }
                }
                Insert errList;
                if(errList.isEmpty())
                {
                    bj.Status__c= 'Completed';
                }
                else
                {
                    bj.Status__c= 'Error';
                }
            }
            catch(Exception e)
            {
                bj.Status__c= 'Error';
                list<Error_Logging__c> errList = new List<Error_Logging__c>();
                errList.add(new Error_Logging__c(Name='MDU_Int__c',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),JSON_Output__c=mduData,Schedule_Job__c=bj.Id));
                Insert errList;
            }   
            finally
            {
                List<AsyncApexJob> futureCalls = [Select Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate from AsyncApexJob where JobType = 'future' and MethodName='callCRMAPIMDUCP' order by CreatedDate desc limit 1];
                if(futureCalls.size()>0){
                    bj.Batch_Job_ID__c = futureCalls[0].Id;
                    bj.End_Time__c = System.now();
                    MDU_CP_Int__c mduInt = [Select Id,CRM_Modified_Date__c from MDU_CP_Int__c order by CRM_Modified_Date__c desc limit 1];
                    bj.Next_Request_Start_Time__c=mduInt.CRM_Modified_Date__c;// setting the last record modified date as the Next Request Start date (date from which the next job will fetch the records.)
                    bj.Record_Count__c=mduList.size();
                    upsert bj;
                }
            }
    }
    *********************************************************************/
}