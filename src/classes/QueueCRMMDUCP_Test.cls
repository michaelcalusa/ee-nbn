/***************************************************************************************************
Class Name:  QueueCRMMDUCP_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : 
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Sukumar       26/10/2015                 Created
****************************************************************************************************/
@isTest
private class QueueCRMMDUCP_Test {
    /*********************************************************************
    US#4118: Below lines have been commented to decommission P2P jobs.
    Author: Jairaj Jadhav
    Company: Wipro Technologies
    
    static Extraction_Job__c ExtractionJobRec = new Extraction_Job__c ();
    static Session_Id__c SessionIdRec = new Session_Id__c();
    static String body= '{'+
        '\"CustomObjects15\": ['+
        '{'+
        '\"CustomText10\": \"Body Corporate, 26 Bondi Road, The Entrance North NSW 2261\",'+
        '\"AccountId\": \"\",'+
        '\"IndexedPick1\": \"In Progress\",'+
        '\"Name\": \"LOC000086737069\",'+
        '\"ContactId\": \"\",'+
        '\"CustomPickList5\": \"UCG\",'+
        '\"Id\": \"AYCA-1Z7P4T\",'+
        '\"CustomBoolean1\": false,'+
        '\"IndexedPick0\": \"LIFD2 Notice Sent\",'+
        '\"CustomDate5\": \"\",'+
        '\"CustomObject1Name\": \"2LJT\",'+
        '\"CustomText39\": \"\",'+
        '\"CustomPickList4\": \"\",'+
        '\"CustomNote0\": \"C / - Body Corporate 26 Bondi Road The Entrance North NSW 2261\",'+
        '\"CustomText44\": \"\",'+
        '\"CustomBoolean2\": false,'+
        '\"CustomText43\": \"2LJT-10-00-MPS-048\",'+
        '\"CustomInteger6\": 0,'+
        '\"CustomInteger9\": 0,'+
        '\"CustomInteger8\": 0,'+
        '\"CustomInteger7\": 0,'+
        '\"CustomInteger12\": 0,'+
        '\"CustomInteger10\": 0,'+
        '\"CustomInteger11\": 0,'+
        '\"CustomInteger1\": 0,'+
        '\"CustomInteger0\": 3,'+
        '\"CustomPickList2\": \"\",'+
        '\"CustomText33\": \"THE ENTRANCE\",'+
        '\"CustomText34\": \"2261\",'+
        '\"CustomPickList1\": \"NSW\",'+
        '\"QuickSearch2\": \"BONDI RD\",'+
        '\"QuickSearch1\": \"26\",'+
        '\"CustomText30\": \"The Owner - Strata Plan No. 76117\",'+
        '\"CustomPickList8\": \"\",'+
        '\"Type\": \"MDU Residential\",'+
        '\"CustomText31\": \"New South Wales North 2\",'+
        '\"CustomObject4Name\": \"2LJT-10\",'+
        '\"CustomText32\": \"Long Jetty\",'+
        '\"CustomText13\": \"\",'+
        '\"CustomBoolean3\": false,'+
        '\"ModifiedDate\": \"2016-09-19T14:56:14Z\"'+
        '}'+
        '],'+
        '\"_contextInfo\": {'+
        '\"limit\": 100,'+
        '\"offset\": 0,'+
        '\"lastpage\": true'+
        '}'+
        '}';
    
    static void getRecords (){  
        // create Extraction_Job record
        ExtractionJobRec = TestDataUtility.createExtractionJob(true);
        // Create Batch_Job record
        Batch_Job__c AccountExtractBatchJob = new Batch_Job__c();
        AccountExtractBatchJob.Type__c='MDU Extract';
        AccountExtractBatchJob.Extraction_Job_ID__c = ExtractionJobRec.Id;
        AccountExtractBatchJob.Batch_Job_ID__c=ExtractionJobRec.Id;
        AccountExtractBatchJob.Next_Request_Start_Time__c=System.now();
        AccountExtractBatchJob.End_Time__c=System.now();
        AccountExtractBatchJob.Status__c='Processing';
        AccountExtractBatchJob.Last_Record__c=True;
        insert AccountExtractBatchJob;
        // Create Session_Id record
        SessionIdRec.Session_Alive__c=true;
        SessionIdRec.CRM_Session_Id__c= userinfo.getSessionId();
        insert SessionIdRec;
    }
    
    static testMethod void QueueCRMAcc_SuccessScenario() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue); 
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMMDUCP.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    
      static testMethod void QueueCRMAcc_ExceptionTest() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        
        body = '{'+
        '\"CustomObjects15\": ['+
        '{'+
        '\"CustomText10\": \"Body Corporate, 26 Bondi Road, The Entrance North NSW 2261\",'+
        '\"AccountId\": \"\",'+
        '\"IndexedPick1\": \"In Progress\",'+
        '\"Name\": \"LOC000086737069\",'+
        '\"ContactId\": \"\",'+
        '\"CustomPickList5\": \"UCG\",'+
        '\"Id\": \"AYCA-1Z7P4T\",'+
        '\"CustomBoolean1\": false,'+
        '\"IndexedPick0\": \"LIFD2 Notice Sent\",'+
        '\"CustomDate5\": \"\",'+
        '\"CustomObject1Name\": \"2LJT\",'+
        '\"CustomText39\": \"\",'+
        '\"CustomPickList4\": \"\",'+
        '\"CustomNote0\": \"C / - Body Corporate 26 Bondi Road The Entrance North NSW 2261\",'+
        '\"CustomText44\": \"\",'+
        '\"CustomBoolean2\": false,'+
        '\"CustomText43\": \"2LJT-10-00-MPS-048\",'+
        '\"CustomInteger6\": 0,'+
        '\"CustomInteger9\": 0,'+
        '\"CustomInteger8\": 0,'+
        '\"CustomInteger7\": 0,'+
        '\"CustomInteger12\": 0,'+
        '\"CustomInteger10\": 0,'+
        '\"CustomInteger11\": 0,'+
        '\"CustomInteger1\": 0,'+
        '\"CustomInteger0\": 3,'+
        '\"CustomPickList2\": \"\",'+
        '\"CustomText33\": \"THE ENTRANCE\",'+
        '\"CustomText34\": \"2261\",'+
        '\"CustomPickList1\": \"NSW\",'+
        '\"QuickSearch2\": \"BONDI RD\",'+
        '\"QuickSearch1\": \"26\",'+
        '\"CustomText30\": \"The Owner - Strata Plan No. 76117\",'+
        '\"CustomPickList8\": \"\",'+
        '\"Type\": \"MDU Residential\",'+
        '\"CustomText31\": \"New South Wales North 2\",'+
        '\"CustomObject4Name\": \"2LJT-10\",'+
        '\"CustomText32\": \"Long Jetty\",'+
        '\"CustomText13\": \"\",'+
        '\"CustomBoolean3\": false,'+
        '\"ModifiedDate\": \"\"'+
        '}'+
        '],'+
        '\"_contextInfo\": {'+
        '\"limit\": 100,'+
        '\"offset\": 0,'+
        '\"lastpage\": true'+
        '}'+
        '}';
        
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMMDUCP.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    
     static testMethod void QueueCRMAcc_ExceptionTest1() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        
        body = '{'+
        '\"CustomObjects15\": ['+
        '{'+
        '\"CustomText10\": \"Body Corporate, 26 Bondi Road, The Entrance North NSW 2261\",'+
        '\"AccountId\": \"\",'+
        '\"IndexedPick1\": \"In Progress\",'+
        '\"Name\": \"LOC000086737069\",'+
        '\"ContactId\": \"AYCA-1Z7P4TAYCA-1Z7P4TAYCA-1Z7P4TAYCA-1Z7P4T\",'+
        '\"CustomPickList5\": \"UCG\",'+
        '\"Id\": \"AYCA-1Z7P4T-AYCA-1Z7P4T\",'+
        '\"CustomBoolean1\": false,'+
        '\"IndexedPick0\": \"LIFD2 Notice Sent\",'+
        '\"CustomDate5\": \"\",'+
        '\"CustomObject1Name\": \"2LJT\",'+
        '\"CustomText39\": \"\",'+
        '\"CustomPickList4\": \"\",'+
        '\"CustomNote0\": \"C / - Body Corporate 26 Bondi Road The Entrance North NSW 2261\",'+
        '\"CustomText44\": \"\",'+
        '\"CustomBoolean2\": false,'+
        '\"CustomText43\": \"2LJT-10-00-MPS-048\",'+
        '\"CustomInteger6\": 0,'+
        '\"CustomInteger9\": 0,'+
        '\"CustomInteger8\": 0,'+
        '\"CustomInteger7\": 0,'+
        '\"CustomInteger12\": 0,'+
        '\"CustomInteger10\": 0,'+
        '\"CustomInteger11\": 0,'+
        '\"CustomInteger1\": 0,'+
        '\"CustomInteger0\": 3,'+
        '\"CustomPickList2\": \"\",'+
        '\"CustomText33\": \"THE ENTRANCE\",'+
        '\"CustomText34\": \"2261\",'+
        '\"CustomPickList1\": \"NSW\",'+
        '\"QuickSearch2\": \"BONDI RD\",'+
        '\"QuickSearch1\": \"26\",'+
        '\"CustomText30\": \"The Owner - Strata Plan No. 76117\",'+
        '\"CustomPickList8\": \"\",'+
        '\"Type\": \"MDU Residential\",'+
        '\"CustomText31\": \"New South Wales North 2\",'+
        '\"CustomObject4Name\": \"2LJT-10\",'+
        '\"CustomText32\": \"Long Jetty\",'+
        '\"CustomText13\": \"\",'+
        '\"CustomBoolean3\": false,'+
         '\"ModifiedDate\": \"2016-09-19T14:56:14Z\"'+
        '}'+
        '],'+
        '\"_contextInfo\": {'+
        '\"limit\": 100,'+
        '\"offset\": 0,'+
        '\"lastpage\": true'+
        '}'+
        '}';
        
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMMDUCP.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    
    static testMethod void QueueCRMAcc_403StatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 403;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMMDUCP.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMAcc_InvalidStatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 900;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMMDUCP.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    *********************************************************************/
}