public class QueueCRMSA implements Database.AllowsCallouts
{
    /*----------------------------------------------------------------------------------------
    Author:        Dilip Athley (v-dileepathley)
    Company:       NBNco
    Description:   This class will be used to Extract the Account Records from CRMOD.
    Test Class:    
    History
    <Date>            <Authors Name>    <Brief Description of Change> 
    
    -----------------------------------------------------------------------------------------*/
    //Created the below class to through the custom exceptions 
    public class GeneralException extends Exception
            {}
    // this method will be called by the ScheduleCRMExt class, which will create a new batch job (Schedule job) record and call the other methos to query the data from CRMOD.
    public static void execute(String ejId)
    {
       Batch_Job__c bjPre  = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Type__c='Stage Application Extract' order by createddate desc limit 1];  
       // creating the new batch job(Schedule Job) record for this batch.
        Batch_Job__c bj = new Batch_Job__c();
       bj.Name = 'SAExtract'+System.now();  
       bj.Start_Time__c = System.now();
       bj.Extraction_Job_ID__c = ejId;
       bj.Status__c = 'Processing';
       bj.Batch_Job_Name__c = 'SAExtract'+System.now();
       bj.Type__c='Stage Application Extract';
       bjPre.Last_Record__c = true;
       list <Batch_Job__c> batch = new list <Batch_Job__c>();
       batch.add(bjPre);
       batch.add(bj);
       Upsert batch;
        //Calling the method to extract the data from CRM on Demand.
       QueueCRMSA.callCRMAPISA(string.valueof(bj.name));
    }
    // This method will be used to query the data from CRM On Demand and insert the records in Stage_Application_Int table.
    @future(callout=true)
    public static void callCRMAPISA(String bjName)
    {
            Integer num = 0;
            Map<String, Object> deserializedSA;
            list<Object> sApps= new List<Object>();
            Map <String, Object> sa;
            Map <String, Object> contextinfo;
            Integer i, offset;
            DateTime mDt,lastDate;
            String dte,saData;
            TimeZone tz = UserInfo.getTimeZone();
            Batch_Job__c bj  = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c from Batch_Job__c where Name =:bjName];
            list<Stage_Application_Int__c> saList = new List<Stage_Application_Int__c>();
            String jsessionId;
            Boolean invalid =false, invalidScode=false;
            Integer session =0,invalidCode =0;
            Long sleepTime;
        //variable declaration finished     
            try
            {
                //query the previously ran batch job to get the last records modified Date.
                Batch_Job__c bjPre = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Next_Request_Start_Time__c,Last_Record__c from Batch_Job__c where Type__c='Stage Application Extract' and Last_Record__c= true order by createddate desc limit 1];
                //format the extracted date to the format accepted by CRMOD REST API.
                String lastRecDate = bjPre.Next_Request_Start_Time__c.format('yyyy-MM-dd\'T\'HH:mm:ss');
                lastRecDate = lastRecDate+'Z';
                //Form the end point URL, please note the initial part is in Named Credential.
                String serviceEndpoint= '?orderBy=ModifiedDate:asc&onlyData=True&fields=Id,AccountName,AccountId,CustomPickList0,CustomObject1Name,CustomDate40,CustomObject4Name,CustomText58,CustomText40,CustomText39,Owner,CustomText50,CustomText0,Name,IndexedPick0,CustomObject2Name,CustomObject2IntegrationId,Type,CustomDate31,CustomText32,CustomInteger9,CustomText37,ContactFullName,ContactId,CustomPickList6,CustomText46,CustomObject5Name,ModifiedDate&q=ModifiedDate%3E'+'\''+lastRecDate+'\'&offset=';
                //get the active Session Id from the Session Id table.
                Session_Id__c jId = [select CRM_Session_Id__c from Session_Id__c where Session_Alive__c=true order by createddate desc limit 1];
                Http http = new Http();
                HttpRequest req = new HttpRequest();
                HttpResponse res;
                req.setMethod('GET');
                req.setTimeout(120000);
                req.setHeader('Cookie', jId.CRM_Session_Id__c);
                req.setHeader('Accept', 'application/json');
                do
                {
                    //extract the data from CRMOD
                    req.setEndpoint('callout:CRMOD_SA'+serviceEndpoint+num);
                    res = http.send(req);
                    saData = res.getBody();
                    if(res.getStatusCode()==200 && ValidateJSON.validate(saData))// ValidateJSON.validate(accData) will validate the output is in JSON format or not.
                    {
                        //deserialize the data.
                        deserializedSA =(Map<String, Object>)JSON.deserializeUntyped(saData);
                        if(num==0)
                        {
                            sApps = (list<Object>)deserializedSA.get('CustomObjects3');
                        }
                        else
                        {
                            sApps.addAll((list<Object>)deserializedSA.get('CustomObjects3'));
                        }
                        contextinfo = (Map<String, Object>)deserializedSA.get('_contextInfo');
                        num=num+100;
                        if(num>3000)
                        {
                            invalidScode=true;
                        }
                    }
                    else if (res.getStatusCode()==403)//if 403 then establish a new session
                    {
                        session = session+1;
                        if (session<=3)
                        {
                            jsessionId = SessionId.getSessionId(); //get the new session id by calling the getSessionId() method of class SessionId.
                            req.setHeader('Cookie', jsessionId);
                            invalid =true;
                        }
                        else
                        {
                            throw new GeneralException('Not able to establish a session with CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                        }
                    }
                    else //if StatusCode is other then 200 or 403 then retry 3 times and then throw an exception.
                    {
                        if(sApps.isEmpty())
                        {
                            invalidCode = invalidCode+1;
                        	if (invalidCode>3)
                        	{
                                throw new GeneralException('Not able to get the data from CRMOD, Error - '+res.getStatusCode()+'- '+res.getStatus());
                            }
                            else
                       		{
                            	sleepTime=10000*invalidCode; // As per the Oracle's advice if the Status Code is not valid next request has to wait for some time.
                            	new Sleep(sleepTime);
                        	}  
                        }
                        else
                        {
                            invalidScode=true;
                        }
                    }
                }
                while((contextInfo==null || contextInfo.get('lastpage')==false)&&!invalidScode);//loop to get all the data, as REST API give data in pages. (100 Records per page)
                if (invalid)
                {
                    SessionId.storeSessionId(jId,jsessionId); // store new session id and mark inactive the older one.
                    invalid=false;
                }
                //create a list of type Stage_Application_Int__c to insert the data in the database.
                for (i=0;i<sApps.size();i++)
                {
                    sa= (Map<String, Object>)sApps[i];
                    dte =(String)sa.get('ModifiedDate');
                    dte = '"'+dte+'"';
                    mDt = (DateTime) JSON.deserialize(dte, DateTime.class);
                    offset = -1*tz.getOffset(mDt)/1000;
                    mDt = mDt.addSeconds(offset);
                    saList.add(new Stage_Application_Int__c(Name=(String)sa.get('Name'),Request_Id__c=(String)sa.get('Id'),CRM_Last_Modified_Date__c=mDt,Account__c=(String)sa.get('AccountName'),
                    Account_Id__c=(String)sa.get('AccountId'),Active_Status__c=(String)sa.get('CustomPickList0'),Address__c=(String)sa.get('CustomText0'),Application_Name__c=(String)sa.get('Name'),
                    Application_Status__c=(String)sa.get('IndexedPick0'),Development_Name__c=(String)sa.get('CustomObject2Name'),Development_Id__c=(String)sa.get('CustomObject2IntegrationId'),
                    Dwelling_Type__c=(String)sa.get('Type'),Estimated_First_Service_Connection_Date__c=(String)sa.get('CustomDate31'),Estimated_RSP_Ready_to_Order_Date__c=(String)sa.get('CustomText32'),
                    Post_Code__c=(String)sa.get('CustomText37'),Primary_Contact__c=(String)sa.get('ContactFullName'),Primary_Contact_Id__c=(String)sa.get('ContactId'),
                    State__c=(String)sa.get('CustomPickList6'),Suburb__c=(String)sa.get('CustomText46'),No_of_Premises__c =(Integer)sa.get('CustomInteger9'),OLT__c = (String)sa.get('CustomObject5Name'),
                    DLT_Owner__c = (String)sa.get('Owner'),FDA__c =(String)sa.get('CustomText50'),FSA__c=(String)sa.get('CustomObject1Name'),SAM__c=(String)sa.get('CustomObject4Name'),
                    Latitude__c = (String)sa.get('CustomText40'),Longitude__c=(String)sa.get('CustomText39'),Target_Delivery_Date__c =(String)sa.get('CustomDate40'),
                    Local_Technology__c=(String)sa.get('CustomText58'),Schedule_Job__c=bj.Id));
                }
                //code to insert the data.
                List<Database.upsertResult> uResults = Database.upsert(saList,false);
                //error logging while record insertion.
                list<Database.Error> err;
                String msg, fAffected;
                String[]fAff;
                StatusCode sCode;
                list<Error_Logging__c> errList = new List<Error_Logging__c>();
                for(Integer idx = 0; idx < uResults.size(); idx++)
                {   
                    if(!uResults[idx].isSuccess())
                    {
                        err=uResults[idx].getErrors();
                        for (Database.Error er: err)
                        {
                            sCode = er.getStatusCode();
                            msg=er.getMessage();
                            fAff = er.getFields();
                        }
                        fAffected = string.join(fAff,',');
                        errList.add(new Error_Logging__c(Name='Stage_Application_Int__c',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=saList[idx].Request_Id__c,Schedule_Job__c=bj.Id));
                    }
                }
                Insert errList;
                if(errList.isEmpty())
                {
                    bj.Status__c= 'Completed';
                }
                else
                {
                    bj.Status__c= 'Error';
                }
            }
            catch(Exception e)
            {
                bj.Status__c= 'Error';
                list<Error_Logging__c> errList = new List<Error_Logging__c>();
                errList.add(new Error_Logging__c(Name='Stage_Application_Int__c',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),JSON_Output__c=saData,Schedule_Job__c=bj.Id));
                Insert errList;
            }   
            finally
            {
                List<AsyncApexJob> futureCalls = [Select Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate from AsyncApexJob where JobType = 'future' and MethodName='callCRMAPISA' order by CreatedDate desc limit 1];
                If(futureCalls.size()>0)
                {
                    bj.Batch_Job_ID__c = futureCalls[0].Id;
                    bj.End_Time__c = System.now();
                    Stage_Application_Int__c saInt = [Select Id,CRM_Last_Modified_Date__c from Stage_Application_Int__c order by CRM_Last_Modified_Date__c desc limit 1];
                    bj.Next_Request_Start_Time__c=saInt.CRM_Last_Modified_Date__c;// setting the last record modified date as the Next Request Start date (date from which the next job will fetch the records.)
                    bj.Record_Count__c=saList.size();
                    upsert bj;
                }
                
            }
    }
    
    
}