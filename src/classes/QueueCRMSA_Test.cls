/***************************************************************************************************
Class Name:  QueueCRMSA_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : 
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Sukumar       26/10/2015                 Created
****************************************************************************************************/
@isTest
private class QueueCRMSA_Test {
    static Extraction_Job__c ExtractionJobRec = new Extraction_Job__c ();
    static Session_Id__c SessionIdRec = new Session_Id__c();
    static String body = '{'+
        '\"CustomObjects3\": ['+
        '{'+
        '\"Id\": \"AYCA-3QHQ87\",'+
        '\"AccountName\": \"PJ & KL Aunger Family Trust & South Park Projects Pty Ltd\",'+
        '\"AccountId\": \"AYCA-3QHQ1H\",'+
        '\"CustomPickList0\": \"Closed\",'+
        '\"CustomObject1Name\": \"5MYB\",'+
        '\"CustomDate40\": \"\",'+
        '\"CustomObject4Name\": \"5MYB-02\",'+
        '\"CustomText58\": \"FTTN\",'+
        '\"CustomText40\": \"-35.145229\",'+
        '\"CustomText39\": \"139.281115\",'+
        '\"Owner\": \"Developer Liaison Team\",'+
        '\"CustomText50\": \"\",'+
        '\"CustomText0\": \"13 McQuarrie St\",'+
        '\"Name\": \"Alex Aunger Close - 1\",'+
        '\"IndexedPick0\": \"Application Withdrawn\",'+
        '\"CustomObject2Name\": \"Alex Aunger Close\",'+
        '\"CustomObject2IntegrationId\": \"AYCA-3QHQ2Q\",'+
        '\"Type\": \"New Road Infrastructure\",'+
        '\"CustomDate31\": \"\",'+
        '\"CustomText32\": \"\",'+
        '\"CustomInteger9\": 0,'+
        '\"CustomText37\": \"5253\",'+
        '\"ContactFullName\": \"Claire Mcgregor\",'+
        '\"ContactId\": \"AYCA-2AI5J3\",'+
        '\"CustomPickList6\": \"SA\",'+
        '\"CustomText46\": \"Murray Bridge\",'+
        '\"CustomObject5Name\": \"\",'+
        '\"ModifiedDate\": \"2016-09-16T17:40:07Z\"'+
        '}  '+
        '],'+
        '\"_contextInfo\": {'+
        '\"limit\": 100,'+
        '\"offset\": 0,'+
        '\"lastpage\": true'+
        '}'+
        '}';
    
    static void getRecords (){  
        // create Extraction_Job record
        ExtractionJobRec = TestDataUtility.createExtractionJob(true);
        // Create Batch_Job record
        Batch_Job__c AccountExtractBatchJob = new Batch_Job__c();
        AccountExtractBatchJob.Type__c='Stage Application Extract';
        AccountExtractBatchJob.Extraction_Job_ID__c = ExtractionJobRec.Id;
        AccountExtractBatchJob.Batch_Job_ID__c=ExtractionJobRec.Id;
        AccountExtractBatchJob.Next_Request_Start_Time__c=System.now();
        AccountExtractBatchJob.End_Time__c=System.now();
        AccountExtractBatchJob.Status__c='Processing';
        AccountExtractBatchJob.Last_Record__c=True;
        insert AccountExtractBatchJob;
        // Create Session_Id record
        SessionIdRec.Session_Alive__c=true;
        SessionIdRec.CRM_Session_Id__c= userinfo.getSessionId();
        insert SessionIdRec;
    }
    
    static testMethod void QueueCRMAcc_SuccessScenario() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue); 
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMSA.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    
      static testMethod void QueueCRMAcc_ExceptionTest() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        
        body = '{'+
        '\"CustomObjects3\": ['+
        '{'+
        '\"Id\": \"AYCA-3QHQ87\",'+
        '\"AccountName\": \"PJ & KL Aunger Family Trust & South Park Projects Pty Ltd\",'+
        '\"AccountId\": \"AYCA-3QHQ1H\",'+
        '\"CustomPickList0\": \"Closed\",'+
        '\"CustomObject1Name\": \"5MYB\",'+
        '\"CustomDate40\": \"\",'+
        '\"CustomObject4Name\": \"5MYB-02\",'+
        '\"CustomText58\": \"FTTN\",'+
        '\"CustomText40\": \"-35.145229\",'+
        '\"CustomText39\": \"139.281115\",'+
        '\"Owner\": \"Developer Liaison Team\",'+
        '\"CustomText50\": \"\",'+
        '\"CustomText0\": \"13 McQuarrie St\",'+
        '\"Name\": \"Alex Aunger Close - 1\",'+
        '\"IndexedPick0\": \"Application Withdrawn\",'+
        '\"CustomObject2Name\": \"Alex Aunger Close\",'+
        '\"CustomObject2IntegrationId\": \"AYCA-3QHQ2Q\",'+
        '\"Type\": \"New Road Infrastructure\",'+
        '\"CustomDate31\": \"\",'+
        '\"CustomText32\": \"\",'+
        '\"CustomInteger9\": 0,'+
        '\"CustomText37\": \"5253\",'+
        '\"ContactFullName\": \"Claire Mcgregor\",'+
        '\"ContactId\": \"AYCA-2AI5J3\",'+
        '\"CustomPickList6\": \"SA\",'+
        '\"CustomText46\": \"Murray Bridge\",'+
        '\"CustomObject5Name\": \"\",'+
        '\"ModifiedDate\": \"\"'+
        '}  '+
        '],'+
        '\"_contextInfo\": {'+
        '\"limit\": 100,'+
        '\"offset\": 0,'+
        '\"lastpage\": true'+
        '}'+
        '}';
        
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMSA.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    
     static testMethod void QueueCRMAcc_ExceptionTest1() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        
        body = '{'+
        '\"CustomObjects3\": ['+
        '{'+
        '\"Id\": \"AYCA-3QHQ87\",'+
        '\"AccountName\": \"PJ & KL Aunger Family Trust & South Park Projects Pty Ltd\",'+
        '\"AccountId\": \"AYCA-3QHQ1H AYCA-3QHQ1H AYCA-3QHQ1H\",'+
        '\"CustomPickList0\": \"Closed\",'+
        '\"CustomObject1Name\": \"5MYB\",'+
        '\"CustomDate40\": \"\",'+
        '\"CustomObject4Name\": \"5MYB-02\",'+
        '\"CustomText58\": \"FTTN\",'+
        '\"CustomText40\": \"-35.145229\",'+
        '\"CustomText39\": \"139.281115\",'+
        '\"Owner\": \"Developer Liaison Team\",'+
        '\"CustomText50\": \"\",'+
        '\"CustomText0\": \"13 McQuarrie St\",'+
        '\"Name\": \"Alex Aunger Close - 1\",'+
        '\"IndexedPick0\": \"Application Withdrawn\",'+
        '\"CustomObject2Name\": \"Alex Aunger Close\",'+
        '\"CustomObject2IntegrationId\": \"AYCA-3QHQ2Q\",'+
        '\"Type\": \"New Road Infrastructure\",'+
        '\"CustomDate31\": \"\",'+
        '\"CustomText32\": \"\",'+
        '\"CustomInteger9\": 0,'+
        '\"CustomText37\": \"5253\",'+
        '\"ContactFullName\": \"Claire Mcgregor\",'+
        '\"ContactId\": \"AYCA-2AI5J3\",'+
        '\"CustomPickList6\": \"SA\",'+
        '\"CustomText46\": \"Murray Bridge\",'+
        '\"CustomObject5Name\": \"\",'+
        '\"ModifiedDate\": \"2016-09-16T17:40:07Z\"'+
        '}  '+
        '],'+
        '\"_contextInfo\": {'+
        '\"limit\": 100,'+
        '\"offset\": 0,'+
        '\"lastpage\": true'+
        '}'+
        '}';
        
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMSA.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    
    static testMethod void QueueCRMAcc_403StatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 403;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMSA.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
    static testMethod void QueueCRMAcc_InvalidStatusCode() {
        getRecords();
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        // Test the functionality
        Test.startTest();
        integer statusCode = 900;
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        QueueCRMSA.execute(string.valueof(ExtractionJobRec.id));
        Test.stopTest(); 
    }
   
}