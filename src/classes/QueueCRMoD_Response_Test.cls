/***************************************************************************************************
Class Name:  QueueCRMoD_Response_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : 
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Sukumar       26/10/2015                 Created
****************************************************************************************************/
@isTest
public class QueueCRMoD_Response_Test implements HttpCalloutMock {
        private Integer code; // setStatusCode Example : 200
        private String bodyAsString; // JSON Response
        private Map<String, String> responseHeaders; // header. Example : 'Content-Type', 'application/JSON'
        public QueueCRMoD_Response_Test(Integer code, String body, Map<String, String> responseHeaders) {
            this.code = code;
            this.bodyAsString = body;
            this.responseHeaders = responseHeaders;
        }
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(code);
            resp.setBody(bodyAsString);
            if(responseHeaders != null) {
                for (String key : responseHeaders.keySet()) {
                    resp.setHeader(key, responseHeaders.get(key));
                }
            }
            return resp;
        }
}