public with sharing class QueueController_LC {
/*------------------------------------------------------------
Author:     Alex Odago
Company:       Contractor
Description:   Details of what the controller does. Indication of the JIRA ticket that the class relates to.
Inputs:        None
Test Class:    Add test class
History
<Date>      <Author>     <Description>
------------------------------------------------------------*/
    
    public static Integer TotalCount =0;
    
    public static Map<String, Map<String,Map<Integer,String>>> getColumnLabelAndAttributes(String componentName, String sObjectName) {
		// Variable declaration
		Map<String, Map<String,Map<Integer,String>>> lightningAttributeMap = new Map<String, Map<String,Map<Integer,String>>>();
		List<LightningComponentConfigurations__c> lstLightningComponentConfig = new List<LightningComponentConfigurations__c>();
		List<LightningComponentConfigurations__c> lstLightningComponentConfigbyUser = new List<LightningComponentConfigurations__c>();
		// Logic:
		lstLightningComponentConfig = CustomSettingsUtility.getLightningComponentConfiguration(componentName,sObjectName);
		lstLightningComponentConfigbyUser = CustomSettingsUtility.getObjectConfig(lstLightningComponentConfig, GlobalUtility.getProfileName(), GlobalUtility.getRoleName());
		lightningAttributeMap = GlobalUtility.getColumnLabelAndAttributesAsMap(lightningAttributeMap,lstLightningComponentConfigbyUser,componentName,sObjectName);
		// Debug:
		System.debug('QueueController_LC.getColumnLabelAndAttributes lightningComponentConfig --->'+lstLightningComponentConfig);
		System.debug('QueueController_LC.getColumnLabelAndAttributes lightningComponentConfigbyUser --->'+lstLightningComponentConfigbyUser);
		System.debug('QueueController_LC.getColumnLabelAndAttributes lightningAttributeMap --->'+lightningAttributeMap);
		return lightningAttributeMap;
	}
    // Created by: Syed. Created this method to fetch column header and fetch data to display in UI. 
	public class LghtDataTableAttribts{
		public Map<Integer,JIGSAW_SummaryTableController.DataTableColumns> mapOfColumnOrderTableAttr = new Map<Integer,JIGSAW_SummaryTableController.DataTableColumns> ();
		public List<Integer> listOfColumnsOrder = new List<Integer>();
		public List<String> listOfFieldApiNames = new List<String>();
	}
	public static LghtDataTableAttribts prepareLghtDataTableAttribts(String componentName, String sObjectName) {
		LghtDataTableAttribts dataTableAttrWrap = new LghtDataTableAttribts();
		// 1st outer most map
		Map<String, Map<String,Map<Integer,String>>> lightningAttributeMap = new Map<String, Map<String,Map<Integer,String>>>();
		// 2nd outer most map
		Map<String,Map<Integer,String>> mapOfLabelNameWithOrder = new Map<String,Map<Integer,String>> ();
		// 3rd outer most map
		Map<Integer,String> mapOfOrderWithType = new Map<Integer,String> ();
		
		// Final map ==> Order - Field API Name, Field Label Name, Field Type, Sortable
		Map<Integer,JIGSAW_SummaryTableController.DataTableColumns> mapOfColumnOrderTableAttr = new Map<Integer,JIGSAW_SummaryTableController.DataTableColumns> ();
		
		lightningAttributeMap = getColumnLabelAndAttributes(componentName, sObjectName);
		List<Integer> listOfColumnsOrder = new List<Integer>();
		List<String> listOfFieldApiNames = new List<String>();
		if(lightningAttributeMap <> null){
			for(String fieldAPIName :lightningAttributeMap.keySet()){
				mapOfLabelNameWithOrder = new Map<String,Map<Integer,String>> ();
				mapOfLabelNameWithOrder = lightningAttributeMap.get(fieldAPIName);
				if(mapOfLabelNameWithOrder <> null){
					for(String labelName :mapOfLabelNameWithOrder.keySet()){
						mapOfOrderWithType = new Map<Integer,String> ();
						mapOfOrderWithType = mapOfLabelNameWithOrder.get(labelName);
						if(mapOfOrderWithType <> null){
							for(Integer OrderNumber :mapOfOrderWithType.keySet()){
								if(mapOfOrderWithType.get(OrderNumber).equalsIgnoreCase('date')){
									JIGSAW_SummaryTableController.DataTableColumns dataTableAttr = new JIGSAW_SummaryTableController.DataTableColumns(fieldAPIName, labelName, 'text', true);
									mapOfColumnOrderTableAttr.put(OrderNumber,dataTableAttr);
									listOfColumnsOrder.add(OrderNumber);
									listOfFieldApiNames.add(fieldAPIName);
								}
								else{
									JIGSAW_SummaryTableController.DataTableColumns dataTableAttr = new JIGSAW_SummaryTableController.DataTableColumns(fieldAPIName, labelName, mapOfOrderWithType.get(OrderNumber), true);
									mapOfColumnOrderTableAttr.put(OrderNumber,dataTableAttr);
									listOfColumnsOrder.add(OrderNumber);
									listOfFieldApiNames.add(fieldAPIName);
								}
							}
						}
					}
				}
			}
		}
		dataTableAttrWrap.mapOfColumnOrderTableAttr = mapOfColumnOrderTableAttr;
		dataTableAttrWrap.listOfColumnsOrder = listOfColumnsOrder;
		dataTableAttrWrap.listOfFieldApiNames = listOfFieldApiNames;
		return dataTableAttrWrap;
	}
	@AuraEnabled
    public static JIGSAW_SummaryTableController.SummaryWrapper getIncidentsFromLC(
        String componentName, String sObjectName, String sortStr,List<String> lstQueue, List<String> lstTech, List<String> lstSLA, List<String> lstStatus, Date dateFrom, Date dateTo) {
		
        system.debug( ' lstQueue - ' + lstQueue);
        system.debug( ' lstTech - ' + lstTech);
        system.debug( ' lstSLA - ' + lstSLA);
        system.debug( ' lstStatus - ' + lstStatus);
        system.debug( ' dateFrom - ' + dateFrom);
        system.debug( ' dateTo - ' + dateTo);
            
       	JIGSAW_SummaryTableController.SummaryWrapper objWrapper = new JIGSAW_SummaryTableController.SummaryWrapper();
		List<Sobject> lstRecords = new List<SObject>();
		List <JIGSAW_SummaryTableController.DataTableColumns> lstColumns = new List <JIGSAW_SummaryTableController.DataTableColumns> ();
        try{
			// prepare columns
			LghtDataTableAttribts dataTableAttr = new LghtDataTableAttribts();
			dataTableAttr = prepareLghtDataTableAttribts(componentName, sObjectName);
			system.debug('QueueController_LC==>mapOfColumnOrderTableAttr==>'+dataTableAttr.mapOfColumnOrderTableAttr);
			if(dataTableAttr.listOfColumnsOrder <> null)
				dataTableAttr.listOfColumnsOrder.sort();
			
			// Add columns in the table
			for(integer columnNumber :dataTableAttr.listOfColumnsOrder){
				if(dataTableAttr.mapOfColumnOrderTableAttr <> null && dataTableAttr.mapOfColumnOrderTableAttr.get(columnNumber) <> null)
					lstColumns.add(dataTableAttr.mapOfColumnOrderTableAttr.get(columnNumber));
			}
			// Prepare Data
			String objectFields = GlobalUtility.createSOQLFieldString(dataTableAttr.listOfFieldApiNames);			
			/*Set<Id> SetOfQueueId = new Set<Id>();
            Set<String> UserGroupName = new Set<String>(); // Change 
            List<CollaborationGroup> lstGroup = new List<CollaborationGroup>(); // Change
			for(GroupMember g :[Select GroupId,Group.Name from GroupMember where Group.Type='Queue' and UserorGroupId =:userinfo.getuserId()]){
				SetOfQueueId.add(g.GroupId);
                UserGroupName.add(g.Group.Name);
			}*/
           // System.debug('QueueName0 :' + SetOfQueueId);         
           // System.debug('QueueName2 :' + UserGroupName);
			//if(String.isBlank(objectFields))
			//	objectFields = 'Id';
			//String query = 'SELECT ' + objectFields +' FROM ' + SObjectName + ' WHERE OwnerId IN:SetOfQueueId AND Industry_Status__c = \'Acknowledged\' ORDER BY SLA_Resolution_Date_Time__c NULLS LAST LIMIT 1000';
            
            // START - CUSTSA-28210 Update default list query
            Set<String> incidentStatus = new Set<String>();
          if(lstStatus.isEmpty() || lstStatus.size() == 0 ){
                String[] status = System.Label.Jigsaw_HomePageIncidentExcludeStatus.split(';');
                system.debug('split is --' + status);
                system.debug('split is --' + status.size());
                for(integer i =0; i < status.size() ; i++ ){
                    incidentStatus.add(status[i]);
                } 
            } 
            String lookupPeriod = System.Label.Jigsaw_Incident_lookup_Period;
            
			if(String.isBlank(objectFields))
				objectFields = 'Id';
            
            // START - CUSTSA-28210 Update default list query
			//String query = 'SELECT ' + objectFields +' FROM ' + SObjectName + ' WHERE Assigned_Group__c IN : UserGroupName AND assignee__c = \'\' ORDER BY SLA_Resolution_Date_Time__c NULLS LAST LIMIT 1000';
			String query = 'SELECT ' + objectFields +' FROM ' + SObjectName + ' WHERE ';
            system.debug('query executing... ' + query);
            
            //CUSTSA-28602 updated by RM
            System.debug('sortStr====>>>'+sortStr);
            // Add one day to chosen SubmittedTo date to incorporate current chosen date with time.
            if(!String.isEmpty(String.valueOf(dateTo)))
                dateTo = dateTo.addDays(1);
   
	    // Get all the filters as query string 
			if(!lstStatus.isEmpty() && lstStatus.size() > 0 && lstStatus[0] == 'All'){
                lstStatus.add('Cancelled');
				lstStatus.add('Rejected'); 
            }
            String filterString =  setfilters(lstTech, lstSLA, lstStatus, dateFrom, dateTo);
            // Set Lookup period to blank if operator choose filters
            //if(!String.isEmpty(filterString) && (!String.isEmpty(String.valueOf(dateFrom)) || !String.isEmpty(String.valueOf(dateTo))))
            //    lookupPeriod = '';
            
            if(String.isNotEmpty(sortStr)){
                //CUSTSA-28602 updated by RM
                //get sorted data
                //query = getFinalQuery(query,UserGroupName,'',sortStr,false,true,incidentStatus,lookupPeriod);
                  query = getFinalQuery(query,lstQueue,filterString,sortStr,false,true,incidentStatus,lookupPeriod);
            }else{
               // query = getFinalQuery(query,UserGroupName,'','',false,false,incidentStatus,lookupPeriod);
                  query = getFinalQuery(query,lstQueue,filterString,'',false,false,incidentStatus,lookupPeriod);
            }
            
            //END - CUSTSA-28210
			if(dateFrom != null){
             	DateTime dateFromDt = DateTime.newInstance(dateFrom, Time.newInstance(0, 0, 0, 0));   
            }
            if(dateTo != null){
            	DateTime dateToDt = DateTime.newInstance(dateTo, Time.newInstance(0, 0, 0, 0));
            }
            
            system.debug('QueueController_LC==>getIncidentsFromLC==>query:'+query);
			lstRecords = Database.query(query);
			system.debug('QueueController_LC==>getIncidentsFromLC==>lstRecords.size:'+lstRecords.size());
			// Fill Wrapper
			objWrapper.lstColumns = lstColumns;
			objWrapper.totalRecords = lstRecords.size();
			objWrapper.lstSobjectData = lstRecords;
			
		}catch(exception ex){
			system.debug('getMessage==>'+Ex.getMessage()+'getStackTraceString'+Ex.getStackTraceString());
			GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'QueueController_LC','getIncidentsFromLC','','',Ex.getMessage(), Ex.getStackTraceString(),Ex, 0);        
        }
		return objWrapper;
	}
    
    public static String getFinalQuery(String strBase, List<String> lstQueue, String strFilter, String strSort, Boolean isFilterchange, Boolean isSortChange, Set<String> incidentStatus, String lookupPeriod){
        String strFinalQuery = strBase + 'Assigned_Group__c IN : lstQueue ';
        system.debug('status values -' + incidentStatus);
        system.debug('base query is -' + strFinalQuery);
        if(!String.isEmpty(strFilter)){
            strFinalQuery += strFilter;
        }
        if(!incidentStatus.isEmpty() && incidentStatus.size() > 0){
            strFinalQuery += ' AND Current_Status__c NOT IN : incidentStatus';
        }
        system.debug('query after filter  -' + incidentStatus);
        
        if(!String.isEmpty(lookupPeriod)){
            strFinalQuery += ' AND CurrentslaDueDateCalc__c = LAST_N_DAYS:' + Integer.valueOf(lookupPeriod);
        }
        //LAST_N_DAYS:365
        if(isSortChange && !String.isEmpty(strSort)){
            strFinalQuery += strSort;
        }
        else{
            strFinalQuery += ' ORDER BY CurrentslaDueDateCalc__c ';
        }
        system.debug(' query after sort -' + strFinalQuery);
        strFinalQuery += ' NULLS LAST LIMIT 1000';
        system.debug('final query  -' + strFinalQuery);
        return(strFinalQuery);
    }
    // Method to get all the group names, logged in user is associated with.
    @AuraEnabled
    public static List<String> getUserGroupsFromLC(){
        
        List<String> userGroupName = new List<String>(); // Change 
        //userGroupName.add('All');
        for(GroupMember g :[Select GroupId,Group.Name from GroupMember where Group.Type='Queue' and UserorGroupId =:userinfo.getuserId()]){
            userGroupName.add(g.Group.Name);
        }
        return userGroupName;
    }
    // Create Filter string accoridng to filters chosen by operator
    public static String setfilters(List<String> lstTech, List<String> lstSLA, List<String> lstStatus, Date dateFrom, Date dateTo){
        
        String filterQuery = '';
        if(!lstTech.isEmpty() && lstTech.size() > 0 && lstTech[0] != 'All')
            filterQuery = (String.isEmpty(filterQuery))? ' AND Prod_Cat__c IN : lstTech ': filterQuery + ' AND Prod_Cat__c IN : lstTech ';
        if(!lstSLA.isEmpty() && lstSLA.size() > 0 && lstSLA[0] != 'All')
            filterQuery = (String.isEmpty(filterQuery))? ' AND serviceRestorationSLA__c IN : lstSLA ': filterQuery + ' AND serviceRestorationSLA__c IN : lstSLA ';
        if(!lstStatus.isEmpty() && lstStatus.size() > 0 && lstStatus[0] != 'All'){
            // Replace 'Incident Accepted' status value with 'In Progress'
            if(lstStatus.indexOf('Incident Accepted') != -1){
                lstStatus.remove(lstStatus.indexOf('Incident Accepted'));
                lstStatus.add('In Progress');
            }
            filterQuery = (String.isEmpty(filterQuery))? ' AND Current_Status__c IN : lstStatus ': filterQuery + ' AND Current_Status__c IN : lstStatus ';
        
        }
		else if(!lstStatus.isEmpty() && lstStatus.size() > 0 && lstStatus[0] == 'All'){
			filterQuery = (String.isEmpty(filterQuery))? ' AND Current_Status__c NOT IN : lstStatus ': filterQuery + ' AND Current_Status__c NOT IN  : lstStatus ';          
        }
        if(!(String.isEmpty(String.valueOf(dateFrom)))){
            filterQuery = (String.isEmpty(filterQuery))? ' AND Reported_Date__c >= : dateFromDt ': filterQuery + ' AND Reported_Date__c >= : dateFromDt ';
        }
        if(!(String.isEmpty(String.valueOf(dateTo)))){
            filterQuery = (String.isEmpty(filterQuery))? ' AND Reported_Date__c <= : dateToDt ': filterQuery + ' AND Reported_Date__c <= : dateToDt ';
        }
        
        System.debug(' final filter string is - '+ filterQuery);
        return filterQuery;
    }
}