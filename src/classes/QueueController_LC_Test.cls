/***************************************************************************************************
Class Type: Test Class
Version     : 1.0
Function    : provide test coverage for QueueController_LC, lightning controller for components
Used in     : None
Modification Log :
* Developer                   Date                   Description
  Murali                     20-05-2018              Added set up method & test methods for corresponding methods in QueueController_LC
Marc                         12-06-2018              comment not used method fetchFieldNames and get_searchedSObjectRecords 
* ----------------------------------------------------------------------------
****/
@isTest
public class QueueController_LC_Test
{
    //test Method for getIncidentsFromLC Method of QueueController_LC Class
    Static TestMethod void getIncidentsFromLCTest()
    {
        List<String> lstTech = new list<String> { 'NHAS'};
        List<String> lstSLA = new list<String> { 'PRI-Standard SLA'};
        List<String> lstStatus = new list<String> { 'Pending'};
		List<String> lstQueue = new list<String> { 'FTTX Assurance Tier1'};
        Date dateFrom = Date.today();
        Date dateTo = Date.today();    
        INSERT new LightningComponentConfigurations__c(name='ObjectsListByQueue',Component__c='ObjectsListByQueue',Object_API_Name__c='Incident_Management__c',Role_or_Profile_Based__c='Profile',Role_or_Profile_Name__c='System Administrator');
        Test.StartTest();
            QueueController_LC.getIncidentsFromLC('ObjectsListByQueue','Incident_Management__c','',lstQueue,lstTech,lstSLA,lstStatus,dateFrom,dateTo);
        Test.StopTest();
    }
    
    Static TestMethod void getUserGroupsFromLCTest()
    {
        Test.StartTest();
     		List<String> lstQueues = QueueController_LC.getUserGroupsFromLC();  
        Test.StopTest();
    }
    
    Static TestMethod void getIncidentsFilterList()
    {
        List<String> lstTech = new list<String> { 'NHAS'};
        List<String> lstSLA = new list<String> { 'PRI-Standard SLA'};
        List<String> lstStatus = new list<String> ();
		List<String> lstQueue = new list<String> { 'FTTX Assurance Tier1'};
        Date dateFrom = Date.today();
        Date dateTo = Date.today();    
        INSERT new LightningComponentConfigurations__c(name='ObjectsListByQueue',Component__c='ObjectsListByQueue',Object_API_Name__c='Incident_Management__c',Role_or_Profile_Based__c='Profile',Role_or_Profile_Name__c='System Administrator');
        Test.StartTest();
            QueueController_LC.getIncidentsFromLC('ObjectsListByQueue','Incident_Management__c','',lstQueue,lstTech,lstSLA,lstStatus,dateFrom,dateTo);
        Test.StopTest();
    }
    
}