/**
* @author :Naga
* @date   :04/07/2016
* @description : Class to get Customer console quick links from Custom settings and to display on Console Application as quick links button.
* @Change History:
*/
Public class QuickLinksComponentController {
    Public List<QuickLinks__c> links; // List Custom setting declaration
    Public Map<Integer,QuickLinks__c> mapOfQuickLinks;//to sort links in given order
    /**
                * @author : Naga
                * @date   : 04/07/2016
                * @description : When page get called getLinks method called and it will get quick links detaisl from Custom Settings.
                * @return List of Quick lniks.
                */
    Public List<QuickLinks__c> getLinks() 
    {                   
        if (links == null) {
            links = QuickLinks__c.getAll().values();
            if(links == null)
                links = new List<QuickLinks__c>();
            else{
                links =  new List<QuickLinks__c>();    
                                mapOfQuickLinks = new Map<Integer,QuickLinks__c>();
                List<Integer> qlSortOrderLst = new List<Integer>();
                                for(QuickLinks__c ql : QuickLinks__c.getAll().values()){
                                mapOfQuickLinks.put(Integer.valueOf(ql.Order__c),ql);
                                }
                qlSortOrderLst.addall(mapOfQuickLinks.keySet());
                qlSortOrderLst.sort();
                                for(Integer order: qlSortOrderLst){
                                if(mapOfQuickLinks.containsKey(order))
                                                links.add(mapOfQuickLinks.get(order));
                                }            }
        }
        return links;     
    }
}