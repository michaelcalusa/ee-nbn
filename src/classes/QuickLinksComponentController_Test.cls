/***************************************************************************************************
Class Name:  QuickLinksComponentController_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 03-11-2016
Function    : This class contains unit test scenarios for  QuickLinksComponentController apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Hari Kalannagari         03-11-2016                Created
****************************************************************************************************/
@isTest
private class QuickLinksComponentController_Test{
    static testMethod void QuickLinksWithValues_Test(){
        List<QuickLinks__c> qllst = new List<QuickLinks__c>();
        QuickLinks__c ql = new QuickLinks__c();
        ql.order__c=.02145;
        ql.Name='TestName';
        qllst.add(ql);
        insert qllst;
        QuickLinksComponentController qlcc = new QuickLinksComponentController();
        qlcc.getLinks();
    }
}