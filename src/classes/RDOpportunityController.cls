public class RDOpportunityController {
    public Contact contact;    
    public opportunity opp{get; set;}
    public Restoration__c rest{get; set;}    
    public Contact getContact() {
        if(contact == null) contact = new Contact();
        return contact;
    }
    
    public String initiatorName{get;set;}
    public String businessUnit{get;set;}
    public String damageDetail{get;set;}
    public Date damageOccurDate{get;set;}
    public String locationId{get;set;}
    
    public String knowDamageCauser{get;set;}
    public String restorationRaised{get;set;}
    public String causerIndividualOrBusiness{get;set;}
    public String individualFirstName{get;set;}
    public String individualLastName{get;set;}
    public String individualContactNumber{get;set;}
    public String individualEmail{get;set;}
    public String individualHomeAddress{get;set;}
    public String businessCompanyName{get;set;}
    public String businessABN{get;set;}
    public String businessSiteContactName{get;set;}
    public String businessContactNumber{get;set;}
    public String businessEmailAddress{get;set;}
    public String businessBusinessAddress{get;set;}
    
    public String policeInvolved{get;set;}
    public String policeOfficerName{get;set;}    
    public String policeOfficerContactNumber{get;set;}
    public String policeStationName{get;set;}
    public String policeRelevantInfo{get;set;}    
    
    
    public RDOpportunityController(){opp= new opportunity(); rest= new Restoration__c();}
    public PageReference Saveto(){
        try{
        opportunity oppToInsert= new opportunity();
        
        oppToInsert.name= opp.name;
        
        DateTime dT = System.now();
        Date dateToday = date.newinstance(dT.year(), dT.month(), dT.day());
        oppToInsert.closedate= dateToday;
        
        oppToInsert.StageName= 'New';
        oppToInsert.leadSource= opp.LeadSource;        
               
        //oppToInsert.Ownerid = UserInfo.getUserId();
        oppToInsert.Ownerid = Label.rdAssignee;        
        oppToInsert.Type= opp.Type;
        
        RecordType rt= [SELECT id FROM RecordType WHERE DeveloperName='Recoverable_Damage' and SobjectType='Opportunity' and IsActive=true limit 1];
        oppToInsert.RecordTypeId=rt.id;
        
        oppToInsert.Description=oppToInsert.Description<>null?oppToInsert.Description:''+'Name:'+contact.FirstName+' '+contact.LastName+'\n';
        oppToInsert.Description=oppToInsert.Description+'Contact Number:'+contact.phone+'\n';
        oppToInsert.Source__c=opp.Source__c;
        oppToInsert.Source_Other__c=opp.Source_Other__c;
        
        if(restorationRaised=='Yes'){
            if(rest.Type__c=='Temporary'){
                oppToInsert.Temp_fix_completed__c='Yes';
            }
            else if(rest.Type__c=='Permanent'){
                oppToInsert.Perm_Fix_Completed__c='Yes';
            }
            else{
                oppToInsert.Temp_fix_completed__c='Yes';
            }
        }
        //oppToInsert.Temp_fix_completed__c=opp.Temp_fix_completed__c;
        
        oppToInsert.Description+='Damage detail:'+damageDetail+'\n';
        oppToInsert.Description+=damageOccurDate<>null?('Damage occur date:'+damageOccurDate.format()+'\n'):'';
        oppToInsert.Asset_Type__c=opp.Asset_Type__c;
        oppToInsert.State__c=opp.State__c;
        //oppToInsert.Description+='LOC ID:'+locationId+'\n';
        oppToInsert.Description+=(locationId<>null && locationId<>'')?('LOC ID:'+locationId+'\n'):'';
        oppToInsert.Description+='Damage Causer:'+knowDamageCauser+'\n';
        if(knowDamageCauser<>null && knowDamageCauser=='Yes'){
            oppToInsert.Description+=causerIndividualOrBusiness<>null?('Individual or Business:'+causerIndividualOrBusiness+'\n'):'';
        }
        //Incase Individual
        if(causerIndividualOrBusiness=='Individual'){
            oppToInsert.Description+=individualFirstName<>null?('First Name:'+individualFirstName+'\n'):'';
            oppToInsert.Description+=individualLastName<>null?('Last Name:'+individualLastName+'\n'):'';
            oppToInsert.Description+=individualContactNumber<>null?('Contact Number:'+individualContactNumber+'\n'):'';
            oppToInsert.Description+=individualEmail<>null?('Email:'+individualEmail+'\n'):'';
            oppToInsert.Description+=individualHomeAddress<>null?('Home Address:'+individualHomeAddress+'\n'):'';
        }
        if(causerIndividualOrBusiness=='Business'){
            oppToInsert.Description+=businessCompanyName<>null?('Company Name:'+businessCompanyName+'\n'):'';
            oppToInsert.Description+=businessABN<>null?('ABN:'+businessABN+'\n'):'';
            oppToInsert.Description+=businessSiteContactName<>null?('Site Contact Name:'+businessSiteContactName+'\n'):'';
            oppToInsert.Description+=businessContactNumber<>null?('Contact Number:'+businessContactNumber+'\n'):'';
            oppToInsert.Description+=businessEmailAddress<>null?('Email:'+businessEmailAddress+'\n'):'';
            oppToInsert.Description+=businessBusinessAddress<>null?('Business Address:'+businessBusinessAddress+'\n'):'';
        }
        oppToInsert.Description+=policeInvolved<>null?('Police Involved:'+policeInvolved+'\n'):'';   
        if(policeInvolved=='Yes'){
            oppToInsert.Description+=policeOfficerName<>null?('Police Officers Name:'+policeOfficerName+'\n'):'';
            oppToInsert.Description+=policeOfficerContactNumber<>null?('Police Officers Contact Number:'+policeOfficerContactNumber+'\n'):'';
            oppToInsert.Description+=policeStationName<>null?('Police Station Name:'+policeStationName+'\n'):'';
            oppToInsert.Description+=opp.Police_Report_Number__c<>null?('Police Report ID:'+opp.Police_Report_Number__c+'\n'):'';            
        }
        oppToInsert.Description+=policeRelevantInfo<>null?('Other Relevant Info:'+policeRelevantInfo+'\n'):'';
        Id relatedOpp=SearchRelatedOpps(rest.NOC_Reference__c,rest.Ticket_of_Work__c);
        if(relatedOpp<>null){
            oppToInsert.Parent_Opportunity__c=relatedOpp;
        }
        insert oppToInsert;
        rest.Opportunity__c=oppToInsert.Id;
        insert rest;        
        PageReference pageRef = new PageReference('/apex/MultiAttachment?id='+String.ValueOf(oppToInsert.Id));
        pageRef.setRedirect(true);
        return pageRef;                
        }
        catch(Exception e){
            System.debug('Error Occurred:'+e.getMessage());
            System.debug('| Line:'+e.getLineNumber());
            System.debug('| Stack Trace:'+e.getStackTraceString());
            return null; 
        }
        
    }   
    public Id SearchRelatedOpps(String noc, String tow){
        List<Restoration__c> rest=new List<Restoration__c>(); 
        Boolean nocExists=false;
        if(noc<>null){
            rest=[SELECT Id, Name, Opportunity__c, NOC_Reference__c, Ticket_of_Work__c, Opportunity__r.Id, Opportunity__r.CreatedDate FROM Restoration__c 
                            WHERE NOC_Reference__c=:noc 
                            ORDER BY Opportunity__r.CreatedDate limit 1];
            if(rest.size()>0){nocExists=true;}
        }
        if(tow<>null && !nocExists){
            rest=[SELECT Id, Name, Opportunity__c, NOC_Reference__c, Ticket_of_Work__c, Opportunity__r.Id, Opportunity__r.CreatedDate FROM Restoration__c 
                            WHERE Ticket_of_Work__c=:tow 
                            ORDER BY Opportunity__r.CreatedDate limit 1];
        }
        if(rest.size()>0){
            return rest[0].Opportunity__r.Id;
        }
        else{return null;}
    }
}