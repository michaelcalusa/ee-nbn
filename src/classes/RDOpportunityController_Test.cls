@isTest(seeAllData = false)
public class RDOpportunityController_Test {
    static testMethod void TestMethod_createOpp() {
        RDOpportunityController rdOppCtl=new RDOpportunityController();
        rdOppCtl.getContact();
        rdOppCtl.opp.name='testName';                
        rdOppCtl.initiatorName='testInitiator';
        rdOppCtl.businessABN='11111111111';
        rdOppCtl.businessBusinessAddress='testBusinessAddress';
        rdOppCtl.businessCompanyName='testCompany';
        rdOppCtl.businessContactNumber='2222222222';
        rdOppCtl.businessEmailAddress='ddd@ddd.ae';
        rdOppCtl.businessSiteContactName='testSiteContact';
        rdOppCtl.businessUnit='testBU';
        rdOppCtl.causerIndividualOrBusiness='Individual';
        rdOppCtl.damageDetail='dsfasfasfa';
        rdOppCtl.damageOccurDate=Date.valueOf(System.Datetime.now());
        rdOppCtl.policeInvolved='Yes';
        rdOppCtl.Saveto();
        rdOppCtl.policeInvolved='No';
        rdOppCtl.causerIndividualOrBusiness='Business';
        rdOppCtl.Saveto();
    }
}