/***************************************************************************************************
    Class Name          : RSPBillingCaseViewController
    Version             : 1.0 
    Created Date        : 13-Dec-2017
    Author              : Rupendra Kumar Vats
    Description         : Get all cases for the logged in Community Contact
    Test Class          : RSPBillingCaseViewControllerTest

    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Rupendra Kumar Vats       
****************************************************************************************************/ 
public class RSPBillingCaseViewController{
    @AuraEnabled
    public static CaseViewWrapper RSPBillingCases(){
        CaseViewWrapper wrapper;
        Boolean hasContactMatrix = true, hasNoContactMatrix = false;
        List<Case> lstBillingCases = new List<Case>();
        system.debug('Current Login User ********'+UserInfo.getName()); 
        List<user> lstUser = [select id, name, ContactID, IsPortalEnabled from user where id = :UserInfo.getUserId()];
        system.debug(lstUser);
        try{
            if(!lstUser.isEmpty()){
                String strContactID = lstUser[0].ContactID;
                list<Team_member__c> contactMatrixCheck = [ Select id From Team_member__c Where Role__c IN ('Escalations - Billing', 'Operational - Billing') AND Customer_Contact__c =: strContactID ];
                system.debug('the contact matrix is '+contactMatrixCheck);
                if(!contactMatrixCheck.isEmpty()){
                    List<Contact> lstContact = [ SELECT AccountId FROM Contact WHERE Id =: strContactID ];
                    if(!lstContact.isEmpty()){
                        lstBillingCases = [ SELECT CaseNumber, RecordType.Name, Type, Number_of_Transactions__c, Subject, Amount__c, Status, CreatedDate, CreatedBy.Name FROM Case WHERE AccountId =: lstContact[0].AccountId AND Origin='Web' AND Form_Status__c != 'In Progress' Order By CreatedDate DESC  ]; 
                        
                        wrapper = new CaseViewWrapper(hasNoContactMatrix, lstBillingCases);
                    }   
                }
                else{
                    wrapper = new CaseViewWrapper(hasContactMatrix, lstBillingCases);
                }
            }
            else{
                wrapper = new CaseViewWrapper(hasNoContactMatrix, lstBillingCases);
            }
        }Catch(Exception ex){
            system.debug('---RSPBillingCaseViewController---RSPBillingCases()---' + ex.getMessage());
            wrapper = new CaseViewWrapper(hasNoContactMatrix, lstBillingCases);
        }           

        return wrapper;
    }
    
    
    
    public class CaseViewWrapper{
        @AuraEnabled
        public Boolean isContactMatrix {get;set;}
        @AuraEnabled
        public List<Case> lstBillingCases {get;set;}
        
        public CaseViewWrapper(Boolean isMatrix, List<Case> lstCases){
            this.isContactMatrix = isMatrix;
            this.lstBillingCases = lstCases;
        }
    }    
}