/***************************************************************************************************
    Class Name  :  RSPBillingCaseViewControllerTest
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  Dec18, 2017 
    Function    :  This class contains unit test scenarios for RSPBillingCaseViewController
    Used in     :  None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Rupendra Vats            Dec18, 2017                 Created
****************************************************************************************************/

@isTest(seeAllData = false)
public Class RSPBillingCaseViewControllerTest{
    static testMethod void TestMethod_RSPCaseView() {
        String strConRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRecordTypeID = rtMapByName.get('Customer Contact').getRecordTypeId();    
        
        String strAccRecordTypeID;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Customer').getRecordTypeId();  
        
        profile commProfile = [select id from profile where name='RSP Customer Community Plus Profile'];
        
        Account acc = new Account(Name = 'wholesale comm', RecordTypeID = strAccRecordTypeID, OwnerId = UserInfo.getUserId());
        insert acc;
        
        Contact con = new Contact(accountid=acc.id,firstName = 'test first', lastName = 'test last', recordtypeid=strConRecordTypeID);
        insert con;
        
        Team_member__c tm = new Team_member__c(Account__c=acc.Id, Customer_Contact__c=con.Id, Role__c='Operational - Billing', Role_Type__c='All', RecordTypeId = Schema.SObjectType.Team_member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId());
        insert tm;
        
        user communityUsr = new user();
        System.runAs (new User(Id = UserInfo.getUserId())){
            UserRole r = new UserRole(name = 'TEST ROLE', PortalType='CustomerPortal', PortalAccountId=acc.Id);
            communityUsr = new user(PortalRole = 'Manager',userroleid=r.id,contactid=con.id,LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',LanguageLocaleKey='en_US',TimeZoneSidKey='Australia/Sydney',CommunityNickname='testtt',Alias='tttt',Email='test@test.com',firstName = 'test first', lastName = 'test last', username='testclassuser@test.com', profileid=commProfile.id);
            insert communityUsr;
        }
        
        system.runAs(communityUsr){
            test.startTest();
            Case c = new Case(RecordTypeId= Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing Dispute').getRecordTypeId(), Category__c = 'Billing Enquiry', Number_of_Transactions__c = 2, Subject = 'Testing', Amount__c= 67);
            insert c;
            RSPBillingCaseViewController.RSPBillingCases();
            test.stopTest();
        }
    }
}