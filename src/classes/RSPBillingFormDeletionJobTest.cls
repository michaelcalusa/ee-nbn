@isTest
public class RSPBillingFormDeletionJobTest {
    static testMethod void  TestRSPInprogCaseDeletionJob () {
        String strConRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRecordTypeID = rtMapByName.get('Customer Contact').getRecordTypeId();    
        
        String strAccRecordTypeID;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Customer').getRecordTypeId();  
        
        profile commProfile = [select id from profile where name='RSP Customer Community Plus Profile'];
        
        Account acc = new Account(Name = 'wholesale comm', RecordTypeID = strAccRecordTypeID, OwnerId = UserInfo.getUserId());
        insert acc;
        
        Contact con = new Contact(accountid=acc.id,firstName = 'test first', lastName = 'test last', recordtypeid=strConRecordTypeID);
        insert con;
        
        Team_member__c tm = new Team_member__c(Account__c=acc.Id, Customer_Contact__c=con.Id, Role__c='Operational - Billing', Role_Type__c='All', RecordTypeId = Schema.SObjectType.Team_member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId());
        insert tm;
        
        user communityUsr = new user();
        System.runAs (new User(Id = UserInfo.getUserId())){
            //UserRole r = new UserRole(name = 'TEST ROLE', PortalType='CustomerPortal', PortalAccountId=acc.Id);
            communityUsr = new user(contactid=con.id,LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',LanguageLocaleKey='en_US',TimeZoneSidKey='Australia/Sydney',CommunityNickname='testtt',Alias='tttt',Email='test@test.com',firstName = 'test first', lastName = 'test last', username='testclassuser@test.com', profileid=commProfile.id);
            insert communityUsr;
        }
        map<string,string> testRecordTypeMap = new map<string,string>();
        testRecordTypeMap.put('other claims','Billing claims');
        
        system.runAs(communityUsr){
            Case c = new Case(RecordTypeId= Schema.SObjectType.Case.getRecordTypeInfosByName().get('Billing Dispute').getRecordTypeId(), Category__c = 'Billing Enquiry',type='Other claims', Number_of_Transactions__c = 2, Subject = 'Testing', Amount__c= 67);
            BillingFormController.saveCase(c,'Billing Dispute','other claims');
            System.assertEquals(c.form_Status__c, 'In Progress');
            Datetime onehourOld = Datetime.now().addMinutes(-70);
            Test.setCreatedDate(c.Id, onehourOld);
            test.startTest();
            database.executeBatch(new RSPBillingFormInProgressCaseDeletionJob());
            test.stopTest();
        }
    }
    
    static testMethod void  TestRSPInprogCaseDeletionJob1 () {
        RSPBillingFormInProgressDelJobSchedule testschedule = new RSPBillingFormInProgressDelJobSchedule();
        test.startTest();
        System.schedule('testingInProgressDeletionJob', '0 0 * * * ?', testschedule );
        test.stopTest();
    }    
}