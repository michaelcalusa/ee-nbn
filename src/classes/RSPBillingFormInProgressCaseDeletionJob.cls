/***************************************************************************************************
    Class Name          : RSPBillingFormInProgressCaseDeletionJob
    Version             : 1.0 
    Created Date        : 20-Jun-2018
    Author              : Ganesh Sawant
    Description         : This is clean up job where we will be deleting the billing Cases of type
                          Billing Enquiry, Billing Dispute, Billing Claim which are in progress.
    Test Class          : RSPBillingFormInProgressCaseDeletionJobTest

    Modification Log    :
    * Developer             Date            Description
    * ------------------------------------------------------------------------------------------------                 
    * Ganesh Sawant      
****************************************************************************************************/ 
global class RSPBillingFormInProgressCaseDeletionJob implements Database.batchable<sObject>, Database.AllowsCallouts, Database.Stateful {
    global Database.QueryLocator start(Database.BatchableContext BC){
        String FORM_STATUS = 'In Progress';
        string WEBOrigin = 'Web';
        //querying the record type IDs for the Case of type Billing Enquiry, Billing Claim and Billing Dispute
        list<Id> billingCaseRecTypeIdList = new list<Id>();
        id billingEnquiryRecTypID = Schema.SObjectType.case.getRecordTypeInfosByName().get('Billing Enquiry').getRecordTypeId();
        billingCaseRecTypeIdList.add(billingEnquiryRecTypID);
        id billingDisputeRecTypID = Schema.SObjectType.case.getRecordTypeInfosByName().get('Billing Dispute').getRecordTypeId();
        billingCaseRecTypeIdList.add(billingDisputeRecTypID);
        id billingClaimRecTypID = Schema.SObjectType.case.getRecordTypeInfosByName().get('Billing Claim').getRecordTypeId();
    	billingCaseRecTypeIdList.add(billingClaimRecTypID);
        string query = 'Select Id, CreatedDate From Case Where RecordTypeId IN :billingCaseRecTypeIdList' +
                       ' AND Origin =\''+WEBOrigin+'\' AND Form_Status__c =\''+FORM_STATUS+'\'';
        System.debug(query);
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Case> scope){
        system.debug('------scope'+scope);
        //Only In Progress cases which are an hour old will be considered for Deletion
        Datetime hourold = Datetime.now().addMinutes(-60);        
        list<Case> caseDelList = new list<Case>();
        for(Case cas : scope){
            if(cas.createdDate <= hourold){
        		caseDelList.add(cas);                
            }
        }        
        try{        
            if(!caseDelList.isEmpty()){
                delete caseDelList;
            }
         }catch(Exception e){
             system.debug('e'+e.getMessage());
             GlobalUtility.logMessage('Error','RSPBillingFormInProgressCaseDeletionJob','Execute','','',e.getMessage(),'',e,0);
         }        
    }
    global void finish(Database.BatchableContext BC){
        
    }
}