/*------------------------------------------------------------
Author:        Ganesh Sawant
Company:       Cognizant
Description:   Apex schedule job that will execute RSPBillingFormInProgressCaseDeletionJob.
Test Class:    TBA
------------------------------------------------------------*/

public class RSPBillingFormInProgressDelJobSchedule  implements Schedulable{

    public static void execute(SchedulableContext ctx)
    {
        Database.executeBatch(new RSPBillingFormInProgressCaseDeletionJob());
    }
}