/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class is to process RSP escaltion emails and create an Interation if the Incident Number exists in SF
Modifications:  5th feb | 2018 Shubham Jaiswal | MSEU-9111
*/
global class RSPEscalationEmailServiceHandler implements Messaging.InboundEmailHandler 
{

    public Id ackEmailMessageId;
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope)  
    {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();               
        
        try
        {                                
            System.debug('Email Subject = ' + email.subject);                
            if(String.isNotBlank(email.subject) && email.subject.containsIgnoreCase('INC'))
            {                    
                //String incFormat = '[I]{1}[N]{1}[C]{1}[0-9]{12}';                  
                Pattern p = Pattern.compile(label.IncidentNumber);
                Matcher m = p.matcher(email.subject);
                if(m.find())
                {                             
                    
                    String IncidentNo = m.group(0);
                    
                    System.debug('Found Incident Number in Email Subject :' +IncidentNo );                                        
                    
                    List<Case> cases = new List<Case>([select id,Thread_Id__c,Site_Technology_Type__c,RecordTypeId,RecordType.Developername,Incident_Number__c,Escalated_Date__c,RSP_ISP__c,Escalated_By__c,Escalation_Channel__c,Escalation_Source__c,OpCat1__c, RSP_Escalation_Email_Content__c, RSP_Escalation_Email_Subject__c from Case where (Status <> 'Closed' AND Status <> 'Cancelled') AND (RecordType.DeveloperName='Activation_Jeopardy' OR  RecordType.DeveloperName='Assurance_Jeopardy' OR RecordType.DeveloperName='Escalation_Triage') AND Incident_Number__c = :IncidentNo limit 1]);
                    if(!cases.isEmpty())                       
                    {          
                        RSPEscalationHelper rspObj = new RSPEscalationHelper();
                        Boolean firstEscaltion = false;
                        if(cases.get(0).Escalated_Date__c == null){
                            firstEscaltion = true;
                        }
                        
                        String accRecordTypeId = GlobalCache.getRecordTypeId('Account', 'Retail Service Provider');
                        List<Contact> cont = new List<Contact>([select id, name, email, accountid from Contact where email = :email.fromAddress and account.recordtypeid = :accRecordTypeId limit 1]);       
                        String ContactId, AccountId;                 
                        if(cont.isEmpty())
                        {                    
                            Contact c = new Contact(); 
                            c.LastName = email.fromName;
                            c.Email = email.fromAddress;
                            c.AccountId = String.isNotBlank(cases.get(0).RSP_ISP__c) ? cases.get(0).RSP_ISP__c : Label.RSP_Escalation_Account;
                            cont.add(c);
                            INSERT cont;                  
                            System.debug('New Contact Created');
                        }
                        
                        Case updCase = new Case();                          
                        updCase.Escalated_By__c = cont.get(0).Id;  
                        if(cases.get(0).Escalated_By__c == null && cases.get(0).Escalation_Channel__c == null && cases.get(0).Escalation_Source__c == null)//update for MSEU-9111 
                        {
                            updCase.OwnerId = [select name from Group where name = 'Customer Delivery Support Team' and Type = 'Queue'].Id;
                        }
                        String escDate = string.valueOf(cases.get(0).Escalated_Date__c);
                        
                        if(String.isBlank(escDate))
                        {
                            updCase.Escalated_Date__c = System.now();
                        }
                        updCase.Escalation_Channel__c = 'Email';
                        updCase.Escalation_Source__c = 'RSP';
                        updCase.Id = cases.get(0).Id;
                        updCase.RSP_Escalated__c = true;
                        //copying email content to for sending the details on SME assignment
                        updCase.RSP_Escalation_Email_Content__c = email.plainTextBody;
                        updCase.RSP_Escalation_Email_Subject__c = email.subject;
                        System.debug('Case Updated');
                        
                        
                        //Moved up by Wayne, start here 
                        List<Case_Contact__c> caseconts = new List<Case_Contact__c>([select id,Name from Case_Contact__c where Contact__c = :cont.get(0).Id AND Case__c = :cases.get(0).Id limit 1]);                               
                        if(caseconts.isEmpty())
                        {                                                                   
                            Case_Contact__c cc = new Case_Contact__c();
                            cc.Contact__c = cont.get(0).Id;
                            cc.Case__c = cases.get(0).Id;
                            cc.Role__c = 'RSP/ISP/Access Seeker';
                            insert cc;  
                            System.debug('Case Contact Created');
                        }
                        
                        EmailMessage newEmail = new EmailMessage();
                         
                        newEmail.FromAddress = email.fromAddress;
                        newEmail.FromName = email.fromName;
                        //newEmail.CcAddress = String.join(email.ccAddresses, ';');
                        //newEmail.Headers = email.headers;
                        newEmail.ToAddress = String.join(email.toAddresses, ';');
                        newEmail.Subject = email.subject;
                        newEmail.TextBody = email.plainTextBody;
                        newEmail.HtmlBody = email.htmlBody;
                        newEmail.ParentId = cases.get(0).Id; 
                        newEmail.Status = '0';
                        newEmail.Incoming = true;
                         
                        insert newEmail;    
                        ackEmailMessageId = newEmail.id;
                        system.debug('@@@@@@@@@@@@@@######'+ newEmail.id);
                        
                        /*Task t = new Task();                                    
                        t.RecordTypeId = GlobalCache.getRecordTypeId('Task', 'Interaction');
                        t.Type = 'EU';
                        t.Priority = '3-General';
                        t.Sub_Type__c = 'Inbound';
                        t.Category__c = 'Email';
                        t.Subject = email.subject;
                        t.Description = email.plainTextBody;
                        t.Status = 'Open';
                        t.Due_Date__c = Date.today()+1;
                        t.Outcome__c = 'Not Completed';
                        t.WhatId = cases.get(0).Id;
                        t.WhoId = cont.get(0).Id;                          
                        insert t;
                        
                        System.debug('New Task Created');*/                      
                        
                        //This loop will process text attacchments like txt files
                        if (email.textAttachments != null && email.textAttachments.size() > 0)
                        {
                            List<String> listOfHeaders = new List<String>();
                            List<Attachment> attTxtList = new List<Attachment>();
                            for(Messaging.InboundEmail.textAttachment tAttachment : email.textAttachments)
                            {
                                Attachment att = new Attachment();
                                att.name = tAttachment.filename;
                                att.body = Blob.valueOf(tAttachment.body);
                                //att.parentid = t.id;
                                att.parentid = newEmail.id;
                                attTxtList.add(att);
                                //insert att;
                                //creating attachment under Case so we can use it while sending case to SME
                                Attachment caseAtt = new Attachment();
                                caseAtt.name = tAttachment.filename;
                                caseAtt.body = Blob.valueOf(tAttachment.body);
                                caseAtt.parentid = updCase.Id;
                                attTxtList.add(caseAtt);   
                                System.debug('---------------'+attTxtList);
                            }
                            if(!attTxtList.isEmpty()) {
                                insert attTxtList;
                            }
                        }
                        
                        //This loop will process text attacchments like xls,csv,image,video files
                        if (email.binaryAttachments != null && email.binaryAttachments.size() > 0)
                        {
                            List<Attachment> attBinList = new list<Attachment>();
                            for(Messaging.InboundEmail.binaryAttachment bAttachment : email.binaryAttachments)
                            {
                                Attachment att = new Attachment();
                                att.name = bAttachment.filename;
                                att.body = bAttachment.body;
                                //att.parentid = t.id;
                                att.parentid = newEmail.id;
                                attBinList.add(att);
                                //insert att;
                                Attachment caseAtt = new Attachment();
                                caseAtt.name = bAttachment.filename;
                                caseAtt.body = bAttachment.body;
                                //att.parentid = t.id;
                                caseAtt.parentid = updCase.id;
                                attBinList.add(caseAtt);
                            }
                            if(!attBinList.isEmpty()) {
                                insert attBinList;
                            }                                                       
                        }
                        
                        sendAckEmail(email.subject,email.fromAddress,cases.get(0));
                        //Moved up by Wayne, end here here 
                        
                        rspObj.cdmAutoAllocation(updCase,cases.get(0));
                        //update updCase
                        System.debug('Case Updated');
                        
                        
                        //the block where it was supposed to be
                        
                    }
                    else
                    {
                        System.debug('No Case found for the Incident No' + IncidentNo);                                 
                        sendAutoReply(email.subject,email.fromAddress);
                    }
                }                   
                else
                {
                    System.debug('Email Subject '+ email.subject + ' doest not have Incident No.');
                    sendAutoReply(email.subject,email.fromAddress);                                        
                }
            }                 
            else
            {
                System.debug('Subject is Blank or No INC or Contains Automatic Reply');
                sendAutoReply(email.subject,email.fromAddress);
            }
        }           
        catch(Exception e)
        {
            System.debug('Exception' + e);   
            GlobalUtility.logMessage('Error','RSPEscalationEmailServiceHandler','handleInboundEmail','','','',email.subject + '    '+ email.plainTextBody,e,0);
        }
        
        result.success = true;
        return result;           
    }
    public void sendAutoReply(String subject, String email)
    {
        System.debug('Sending Auto Response');                
        List<Messaging.SingleEmailMessage>  sendEmails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage sendEmail = new Messaging.SingleEmailMessage();
        String emailBody = 'Thank you for your email that was sent to escalations@nbnco.com.au<mailto:escalations@nbnco.com.au>' + '\n' + '\n';
        emailBody = emailBody + 'nbn is unable to escalate your request without an INC Number. It is possible the INC number you have quoted does not exist or has been closed.'  + '\n' + '\n';
        emailBody = emailBody + 'To progress further, we recommend the following:'  + '\n' + '\n';
        emailBody = emailBody + '   ·         For a new issue please raise an INC and follow BAU process '  + '\n' + '\n';
        emailBody = emailBody + '   ·         For a closed issue please raise a new INC for nbn to investigate your enquiry and follow BAU processes '  + '\n' + '\n';
        emailBody = emailBody + '   ·         For an error in INC number, please reply with the correct INC number in the subject line '  + '\n' + '\n';
        emailBody = emailBody + 'Kind Regards,'  + '\n';
        emailBody = emailBody + 'Customer Delivery Management Team  '  + '\n' + '\n';
        sendEmail.setPlainTextBody(emailBody);
        sendEmail.setSubject('RE: '+subject);
        
        String rspAddress = System.Label.RSP_Escalation_Email_Address;
        //OrgWideEmailAddress[] owea = [select Id,Address,DisplayName from OrgWideEmailAddress where Address = :rspAddress];
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply@nbnco.com.au'];
        if ( owea.size() > 0 ) 
        {
            sendEmail.setOrgWideEmailAddressId(owea.get(0).Id) ;
        }        
        sendEmail.setToAddresses(new String[] {email});
        sendEmails.add(sendEmail);
        Messaging.sendEmail(sendEmails);        
    }
    public void sendAckEmail(String subject, String email, Case cs)
    {
        System.debug('Sending Acknowledgement Email');                
        List<Messaging.SingleEmailMessage>  sendEmails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage sendEmail = new Messaging.SingleEmailMessage();
        String emailBody = 'This is to advise that your email has been received to escalations@nbnco.com.au<escalations@nbnco.com.au>' + '\n' + '\n';        
        emailBody = emailBody + 'Kind Regards,'  + '\n';
        emailBody = emailBody + 'Customer Delivery Management Team  '  + '\n' + '\n';
        emailBody = emailBody +  cs.Thread_Id__c + '\n';
        emailBody = emailBody +  'Do not remove the reference no.' + '\n' + '\n';
        sendEmail.setPlainTextBody(emailBody);
        sendEmail.setSubject('RE: '+subject);
        
        String rspAddress = System.Label.RSP_Escalation_Email_Address;
        OrgWideEmailAddress[] owea = [select Id,Address,DisplayName from OrgWideEmailAddress where Address = :rspAddress];
        //OrgWideEmailAddress[] owea = [select Id,Address,DisplayName from OrgWideEmailAddress where Address = 'noreply@nbnco.com.au'];
        if ( owea.size() > 0 ) 
        {
          sendEmail.setOrgWideEmailAddressId(owea.get(0).Id) ;
        }     
        sendEmail.setToAddresses(new String[] {email});
        sendEmails.add(sendEmail);
        Messaging.sendEmail(sendEmails);
        
        EmailMessage newEmail = new EmailMessage();
        newEmail.FromAddress = owea.get(0).Address;
        newEmail.FromName = owea.get(0).DisplayName;
        newEmail.ToAddress = email;
        newEmail.Subject = subject;
        newEmail.TextBody = emailBody;
        //newEmail.HtmlBody = '<html><body><b>Hello</b></body></html>'; // email body
        newEmail.ParentId = cs.Id; 
        newEmail.Status = '3';
        newEmail.Incoming = false;
        if(ackEmailMessageId != null){
            newEmail.ReplyToEmailMessageId = ackEmailMessageId;
        }
        /*Contact Ids of recipients
        String[] toIds = new String[]{'003B000000AxcEjIAJ'}; 
        emailMessage.toIds = toIds;*/

        insert newEmail;    
        system.debug('@@@@@@@@@@@@@@######'+ newEmail.id);        
    }
    
}