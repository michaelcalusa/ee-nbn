/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class is to Test RSPEscalationEmailServiceHandler
*/
@isTest
Public class RSPEscalationEmailServiceHandler_Test 
{
    
    public static testmethod void dotest1()
    {           
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Test.startTest();
        /* Test Case - No Case with the Incident Number */
        email.subject = 'INC123456789012';
        email.fromAddress = 'someaddress@email.com';
        email.fromname = 'somename';
        email.plainTextBody = 'email body\n2225256325\nTitle'; 
        email.toAddresses = new List<String>{'test1@test.com'};
        RSPEscalationEmailServiceHandler testInbound=new RSPEscalationEmailServiceHandler();
        testInbound.handleInboundEmail(email, env);
        /* Test Case - No Case with the Incident Number */
        Test.stopTest();
        
    }    
    
    public static testmethod void dotest2() {
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Test.startTest();
        
        email.subject = 'INC12345';
        email.fromAddress = 'someaddress@email.com';
        email.fromname = 'somename';
        email.plainTextBody = 'email body\n2225256325\nTitle'; 
        email.toAddresses = new List<String>{'test1@test.com'};
        RSPEscalationEmailServiceHandler testInbound2=new RSPEscalationEmailServiceHandler();
        testInbound2.handleInboundEmail(email, env);
        Test.stopTest();    
    
    }
    
    public static testmethod void dotest3(){
        Test.startTest();
        Site__c site = new Site__c();
        site.Location_Id__c = 'LOC000083810149';
        site.Technology_Type__c = 'HFC';
        insert site;
        
        Account TestAcc = new Account();
        TestAcc.Name = 'TestAccount';
        TestAcc.Tier__c = '1';
        TestAcc.Status__c = 'Active';
        TestAcc.Type__c = 'RSP';
        TestAcc.recordtypeid = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId();
        
        //[select id from recordtype where developername = 'Customer' and sobjecttype = 'Account' limit 1].id;
        insert TestAcc ;   
        
        Id rspAccId = [select id from Account where recordtype.developername = 'Retail_Service_Provider' limit 1].id;

        Case_Assignment__C caseAssgnObj = new case_Assignment__C();
        caseAssgnObj.Available__c=true;
        caseAssgnObj.User__c = userinfo.getUserId();
        caseAssgnObj.User_Type__c = 'RSP Escalation';
        caseAssgnObj.respesc_CaseType__c = 'Customer Incident';
        caseAssgnObj.rspesc_RSP_Account__c = rspAccId;
        insert caseAssgnObj;
        
        RSP_SME_Assignment__c rspsme = new RSP_SME_Assignment__c();
        rspsme.Business_Hours_Team__c = true;
        rspsme.Technology_Type__c = 'HFC';
        rspsme.SME_Team__c = 'Customersupport_HFC';
        rspsme.SME_Team_Email__c = 'test2@test.com';
        insert rspsme;

        Case testcase = new case();
        testcase.AccountID = TestAcc.id; 
        testcase.Origin = 'Email';
        testcase.type = 'Activations';
        testcase.status= 'New';
        testcase.subject = 'test subject';
        testcase.Incident_Number__c = 'INC123456789012';
        testcase.RSP_ISP__c = rspAccId;
        testcase.Site__c = site.id;
        testcase.Opcat1__c = 'Customer Incident';
        testcase.recordtypeID = [select id from recordtype where developername = 'Activation_Jeopardy' limit 1].id ;      
        insert testcase;       
        Test.stopTest();
    
    
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        email.subject = 'INC123456789012';
        email.fromAddress = 'someaddress@email.com';
        email.fromname = 'somename';
        email.plainTextBody = 'email body\n2225256325\nTitle'; 
        email.toAddresses = new List<String>{'test1@test.com'};
        RSPEscalationEmailServiceHandler testInbound3=new RSPEscalationEmailServiceHandler();
        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            
        // add an Text atatchment  
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
        
        
        testInbound3.handleInboundEmail(email, env);
             
    
    }
    
        public static testmethod void dotest4(){
        Test.startTest();
        Site__c site = new Site__c();
        site.Location_Id__c = 'LOC000083810149';
        site.Technology_Type__c = 'HFC';
        insert site;
        
        Account TestAcc = new Account();
        TestAcc.Name = 'TestAccount';
        TestAcc.Tier__c = '1';
        TestAcc.Status__c = 'Active';
        TestAcc.Type__c = 'RSP';
        TestAcc.recordtypeid = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId();
        
        //[select id from recordtype where developername = 'Customer' and sobjecttype = 'Account' limit 1].id;
        insert TestAcc ;   
        
        Id rspAccId = [select id from Account where recordtype.developername = 'Retail_Service_Provider' limit 1].id;

        Case_Assignment__C caseAssgnObj = new case_Assignment__C();
        caseAssgnObj.Available__c=true;
        caseAssgnObj.User__c = userinfo.getUserId();
        caseAssgnObj.User_Type__c = 'RSP Escalation';
        caseAssgnObj.respesc_CaseType__c = 'Customer Incident';
        caseAssgnObj.rspesc_RSP_Account__c = rspAccId;
        insert caseAssgnObj;
        
        RSP_SME_Assignment__c rspsme = new RSP_SME_Assignment__c();
        rspsme.Business_Hours_Team__c = true;
        rspsme.Technology_Type__c = 'HFC';
        rspsme.SME_Team__c = 'Customersupport_HFC';
        rspsme.SME_Team_Email__c = 'test2@test.com';
        insert rspsme;

        Case testcase = new case();
        testcase.AccountID = TestAcc.id; 
        testcase.Origin = 'Email';
        testcase.type = 'Activations';
        testcase.status= 'New';
        testcase.subject = 'test subject';
        testcase.Incident_Number__c = 'INC123456789012';
        testcase.RSP_ISP__c = rspAccId;
        testcase.Site__c = site.id;
        testcase.Opcat1__c = 'Customer Incident';
        testcase.recordtypeID = [select id from recordtype where developername = 'Activation_Jeopardy' limit 1].id ;      
        insert testcase;       
        Test.stopTest();
    
    
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

        email.subject = 'INC123456789012';
        email.fromAddress = 'someaddress@email.com';
        email.fromname = 'somename';
        email.plainTextBody = 'email body\n2225256325\nTitle'; 
        email.toAddresses = new List<String>{'test1@test.com'};
        RSPEscalationEmailServiceHandler testInbound4=new RSPEscalationEmailServiceHandler();
        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            
        // add an Text atatchment  
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
        
        
        testInbound4.handleInboundEmail(email, env);
        
        testcase.SME_Team__c = '';
        update testcase;     
    
    }
}