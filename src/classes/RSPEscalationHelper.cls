/**************************************************************
Created Date        : 05-April-2018
Author              : Shubham Jaiswal/Ganesh Sawant
Description         : 
Modification Log    :
Developer             Date            Description

***************************************************************/
public class RSPEscalationHelper{
    
    public void cdmAutoAllocation(Case updatedCase,Case oldCase){
        
        Boolean caseUpdateSuccess = false;
        List<Case_Assignment__c> csOwner = new List<Case_Assignment__c>();
        String caseType = (oldCase.recordtype.developername == 'Assurance_Jeopardy')?'Customer Incident':'Service Request';
        
        system.debug('!!!!!!!!!!' + oldCase.RSP_ISP__c + updatedCase.RSP_ISP__c );
        if(oldCase.RSP_ISP__c != null){
            csOwner = [Select id,User__c,Num_Cases__c from Case_Assignment__c where rspesc_RSP_Account__c =:oldCase.RSP_ISP__c AND respesc_CaseType__c includes (:oldCase.Opcat1__c) AND Available__c = true AND User_Type__c = 'RSP Escalation' ORDER BY Num_Cases__c ASC NULLS FIRST];
            system.debug('Case owner @@@@@@@@@' + csOwner);
        }
        
        if(csOwner.size() > 0){
            updatedCase.OwnerId = csOwner[0].User__c;
            updatedCase.Case_Assignment__c = csOwner[0].id;
        }
        
        //Performing SME Assignment based on RSP SME Assignment Object matrix
        updatedCase = smeTeamAssignment(updatedCase,oldCase);
        
        try{
            update updatedCase;
            caseUpdateSuccess = true;
            //CDMSMEEmailNotificeAfterAssign(updatedCase);
        } 
        catch (DMLException e){
            insert new Error_Logging__c(Name='RSP Escalation Error',Error_Message__c='Escalated Case not updated.',isSuccess__c=false,CRM_Record_Id__c=updatedCase.Id);
        }
        
        
        
        system.debug('@@@@@@@@@@@@@caseUpdateSuccess' + caseUpdateSuccess );
        if(caseUpdateSuccess && csOwner.size() > 0){
            Case_Assignment__c updCA = new Case_Assignment__c(id = csOwner[0].id);
            if(csOwner[0].Num_Cases__c == null){
                updCA.Num_Cases__c = 1;
            }
            else{
                updCA.Num_Cases__c = csOwner[0].Num_Cases__c + 1;
            }
            system.debug('@@@@@@@@@@@@@caseUpdateSuccess2' + caseUpdateSuccess );
            update updCA;
        }
    }
    
    public case smeTeamAssignment(Case updateCase, Case existingCase) {
        System.debug('-----SMETeamAssignmentMethod Called'+updateCase.Escalated_Date__c);
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE Name='RSP Escalations Business Hours' limit 1];
        System.debug('Business Hours'+bh);
        boolean withinBH = false;
        if(bh!=null){
            withinBH = BusinessHours.isWithin(bh.id, updateCase.Escalated_Date__c);
        }
        System.debug('Is within BH'+withinBH);
        System.debug('Existing Case values'+existingCase.Site_Technology_Type__c+existingCase.RecordTypeId);
        if(existingCase.Site_Technology_Type__c!=null && existingCase.Opcat1__c =='Customer Incident'){
            list<RSP_SME_Assignment__c> rspAssignmentList = [Select Id, Name, SME_Team__c, SME_Team_Email__c, L2_Contact_Email__c From RSP_SME_Assignment__c Where Business_Hours_Team__c=: withinBH AND Technology_Type__c=:existingCase.Site_Technology_Type__c];
            if(!rspAssignmentList.isEmpty()){
                updateCase.SME_Team__c = rspAssignmentList[0].SME_Team__c;
                updateCase.RSP_SME_Assignment__c = rspAssignmentList[0].Id;
                updateCase.SME_Team_Email__c = rspAssignmentList[0].SME_Team_Email__c;
                updateCase.TL_Email__c = rspAssignmentList[0].L2_Contact_Email__c;
                System.debug('SME Value'+updateCase.SME_Team__c);
            }
        }
        return updateCase;
    }
    
    public void caseAssignmentBeforeUpdate(Map<id,Case> updatedCases,Map<id,Case> oldCases){
        system.debug('!!!!!!!!!!!!!!0');
        Map<id,Case_Assignment__c> mapIdtoCA = new Map<id,Case_Assignment__c>([Select id,User__c,Num_Cases__c,rspesc_RSP_Account__c,respesc_CaseType__c from Case_Assignment__c where Available__c = true AND User_Type__c = 'RSP Escalation']);
        Map<String,Case_Assignment__c> mapStringtoCA = new Map<String,Case_Assignment__c>();
        for(Case_Assignment__c ca:mapIdtoCA.values()){
            mapStringtoCA.put(String.valueof(ca.User__c)+String.valueof(ca.rspesc_RSP_Account__c),ca);
        }
        
        for(Case cs :updatedCases.values()){
            Case newCase = updatedCases.get(cs.id);
            Case_Assignment__c newCA = mapStringtoCA.get(String.valueof(newCase.OwnerId)+String.valueof(newCase.RSP_ISP__c));
            system.debug('!!!!!!!!!!!!!!1' + newCA);
            if(newCA != null){
                system.debug('!!!!!!!!!!!!!!2');
                newCase.Case_Assignment__c = newCA.Id;
            }
            else{
                system.debug('!!!!!!!!!!!!!!3');
                newCase.Case_Assignment__c = null;
            }           
        }
    }

    public void rspSMEAssignmentBeforeUpdate(Map<id,Case> updatedCases,Map<id,Case> oldCases){
        Map<string, RSP_SME_Assignment__c> rspSMEAssignmentMap = new Map<string, RSP_SME_Assignment__c>();
        for(RSP_SME_Assignment__c rspSMEAssignment :[Select Id, Name, SME_Team__c, SME_Team_Email__c, L2_Contact_Email__c, Technology_Type__c From RSP_SME_Assignment__c]) {
            if(rspSMEAssignment.Technology_Type__c!=null){
                rspSMEAssignmentMap.put(rspSMEAssignment.SME_Team__c+rspSMEAssignment.Technology_Type__c.toLowerCase(), rspSMEAssignment);
            }
            else if(rspSMEAssignment.Technology_Type__c == null){
                rspSMEAssignmentMap.put(rspSMEAssignment.SME_Team__c+rspSMEAssignment.Technology_Type__c, rspSMEAssignment);                
            }
        }
        for(Case cs :updatedCases.values()){
            Case newCase = updatedCases.get(cs.id);            
            Case oldCase = oldCases.get(cs.Id);
            if(newCase.SME_Team__c!= oldCase.SME_Team__c && newCase.SME_Team__c!=null  && oldCase.Escalated_Date__c == newCase.Escalated_Date__c){
                system.debug('Entered SME Logic'+newCase.SME_Team__c);
                if(newCase.Site_Technology_Type__c!=null && rspSMEAssignmentMap.containsKey(newCase.SME_Team__c+newCase.Site_Technology_Type__c.toLowerCase())){
                    system.debug('RSP SME MAP'+rspSMEAssignmentMap.get(newCase.SME_Team__c+newCase.Site_Technology_Type__c.toLowerCase()));
                    newCase.RSP_SME_Assignment__c = rspSMEAssignmentMap.get(newCase.SME_Team__c+newCase.Site_Technology_Type__c.toLowerCase()).Id;
                    newCase.SME_Team_Email__c = rspSMEAssignmentMap.get(newCase.SME_Team__c+newCase.Site_Technology_Type__c.toLowerCase()).SME_Team_Email__c;
                    newCase.TL_Email__c = rspSMEAssignmentMap.get(newCase.SME_Team__c+newCase.Site_Technology_Type__c.toLowerCase()).L2_Contact_Email__c;
                }
                else if(newCase.Site_Technology_Type__c==null && rspSMEAssignmentMap.containsKey(newCase.SME_Team__c+newCase.Site_Technology_Type__c)){
                    system.debug('RSP SME MAP'+rspSMEAssignmentMap.get(newCase.SME_Team__c+newCase.Site_Technology_Type__c));
                    newCase.RSP_SME_Assignment__c = rspSMEAssignmentMap.get(newCase.SME_Team__c+newCase.Site_Technology_Type__c).Id;
                    newCase.SME_Team_Email__c = rspSMEAssignmentMap.get(newCase.SME_Team__c+newCase.Site_Technology_Type__c).SME_Team_Email__c;
                    newCase.TL_Email__c = rspSMEAssignmentMap.get(newCase.SME_Team__c+newCase.Site_Technology_Type__c).L2_Contact_Email__c;
                }                
                else {
                    newCase.RSP_SME_Assignment__c = null;
                    newCase.SME_Team_Email__c = null;
                    newCase.TL_Email__c = null;      
                }
            }
            if(newCase.SME_Team__c!= oldCase.SME_Team__c && newCase.SME_Team__c==null && newCase.RSP_SME_Assignment__c!=null){
                    newCase.RSP_SME_Assignment__c = null;
                    newCase.SME_Team_Email__c = null;
                    newCase.TL_Email__c = null;
            }             
        }
    }

    
    public void caseAssignmentAfterUpdate(Map<id,Case> updatedCases,Map<id,Case> oldCases){
        system.debug('@@@@@@@@@@6');
        Map<String,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosByName();
        Set<Id> rspRecordTypeIds = new Set<Id>{
            rtMap.get('Activations Jeopardy').getRecordTypeId(),
                rtMap.get('Assurance Jeopardy').getRecordTypeId()
                }; 
                    Map<id,Case_Assignment__c> caUpdatesMap = new Map<id,Case_Assignment__c>();
        Map<id,Case_Assignment__c> mapIdtoCA = new Map<id,Case_Assignment__c>([Select id,User__c,Num_Cases__c,rspesc_RSP_Account__c,respesc_CaseType__c from Case_Assignment__c where Available__c = true AND User_Type__c = 'RSP Escalation']);
        Map<String,Case_Assignment__c> mapStringtoCA = new Map<String,Case_Assignment__c>();
        for(Case_Assignment__c ca:mapIdtoCA.values()){
            mapStringtoCA.put(String.valueof(ca.User__c)+String.valueof(ca.rspesc_RSP_Account__c),ca);
        }
        
        Set<Id> caseOwnerIds = new Set<Id>();
        for(Case cs :updatedCases.values()){
            system.debug('***********************-2' + updatedCases.get(cs.id).OwnerId + ' ' + oldCases.get(cs.id).OwnerId);
            caseOwnerIds.add(updatedCases.get(cs.id).OwnerId);
            caseOwnerIds.add(oldCases.get(cs.id).OwnerId);
        }
        
        Map<String,Integer> mapUsertoOpenCase = new Map<String,Integer>();
        if(caseOwnerIds.size() > 0){
            for(AggregateResult agr :[Select count(id)ct,OwnerId,RSP_ISP__c From Case where OwnerID in :caseOwnerIds AND recordtypeID IN:rspRecordTypeIds AND (Status != 'Closed' AND Status != 'Cancelled') group by Ownerid,RSP_ISP__c]){
                mapUsertoOpenCase.put(String.valueof(agr.get('OwnerId')) + String.valueof(agr.get('RSP_ISP__c')),Integer.valueOf(agr.get('ct')));
                system.debug('***********************-1'+ String.valueof(agr.get('OwnerId')) + ' ' + String.valueof(agr.get('RSP_ISP__c')) + ' ' +Integer.valueOf(agr.get('ct')));
            }
        }
        
        
        for(Case cs :updatedCases.values()){
            Case newCase = updatedCases.get(cs.id);
            Case oldCase = oldCases.get(cs.id);
            system.debug('***********************0' + newCase.ownerid + ' ' + oldCase.ownerid);
            //Owner Update
            Case_Assignment__c newCA;
            if(newCase.OwnerId != oldCase.OwnerId){
                newCA = mapStringtoCA.get(String.valueof(newCase.OwnerId)+String.valueof(newCase.RSP_ISP__c));
                system.debug('***********************1' + newCA);
            }
            else{
                newCA = mapIdtoCA.get(newCase.Case_Assignment__c);
                system.debug('***********************2' + newCA);
            }
            if(newCA != null){
                Integer openCasesCount = mapUsertoOpenCase.containsKey(String.valueof(newCase.OwnerId) + String.valueof(newCase.RSP_ISP__c))?mapUsertoOpenCase.get(String.valueof(newCase.OwnerId) + String.valueof(newCase.RSP_ISP__c)):0;
                system.debug('***********************3' + openCasesCount);
                if(openCasesCount != null && openCasesCount != newCA.Num_Cases__c){
                    caUpdatesMap.put(newCA.Id,new Case_Assignment__c(id = newCA.id,Num_Cases__c = openCasesCount));
                }
            }
            system.debug('***********************4' + oldCase.Case_Assignment__c);
            if(mapIdtoCA.containsKey(oldCase.Case_Assignment__c) && newCase.OwnerId != oldCase.OwnerId){
                Case_Assignment__c oldCA = mapIdtoCA.get(oldCase.Case_Assignment__c);
                system.debug('***********************5' + oldCA);
                if(oldCA != null){
                    String mapKey = String.valueof(oldCase.OwnerId) + String.valueof(oldCase.RSP_ISP__c);
                    Integer openCasesCount = mapUsertoOpenCase.containsKey(mapKey)?mapUsertoOpenCase.get(mapKey):0;
                    system.debug('***********************6' + openCasesCount);
                    if(openCasesCount != null && openCasesCount != oldCA.Num_Cases__c){
                        caUpdatesMap.put(oldCA.Id,new Case_Assignment__c(id = oldCA.id,Num_Cases__c = openCasesCount));
                    }                  
                }
            }
        }
        
        if(caUpdatesMap.size() > 0 && Trigger.isAfter){
            system.debug('***********************7' + caUpdatesMap);
            update caUpdatesMap.values();
        }   
    }
    
    public void CDMSMEEmailNotificeAfterAssign(List<case> CasesAfterCDMSMEAssign, List<case> OldSMECases){
        //get the old case map
        Map<Id, case> OldSMECasesMap = new Map<Id,case>(OldSMECases);
        Map<Id, case> NewSMECasesMap = new Map<Id,case>(CasesAfterCDMSMEAssign);
        
        //get the EmailMessage from old cases and its attachments
        Map<id,Attachment> EmailMsgAttachmentsMap = new Map<id,Attachment>();
        List<EmailMessage> EmailMsgs = [select id,subject, textbody, CreatedDate from Emailmessage where parentid in:OldSMECasesMap.keySet() order by CreatedDate asc limit 1];
        Map<Id, EmailMessage> EmailMsgsMap = new Map<Id, EmailMessage>(EmailMsgs);
        if(OldSMECasesMap.size() == 1){
            List<Attachment> EmailMsgAttachments = [select id, name, Body from attachment where parentid in: EmailMsgsMap.keySet()];            
            system.debug('the email msg attchments from mailmessage are '+EmailMsgAttachments);
            EmailMsgAttachmentsMap = new Map<id,Attachment>(EmailMsgAttachments);
        }  
        
        //get the attchments from case directly
        //system.debug('the case id key set is '+NewSMECasesMap.keySet());
        List<Id> AttachmentsIds = new List<Id>();
        List<attachment> AllAttachmentIdList =[select id,parentid from attachment where parentid in:NewSMECasesMap.keySet()];

        
        //get the record type id of Assurance Jepoardy, Active Jeporday
        system.debug('*** RSPEscalation EmailNotification Service Variable Initialization: begin ***');
        set<id> RspEscalationRecordIds = new set<id>();
        Id AssuranceJepoardyRecordId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Assurance Jeopardy').getRecordTypeId();
        Id ActiveJepoardyRecordId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Activations Jeopardy').getRecordTypeId();
        RspEscalationRecordIds.add(AssuranceJepoardyRecordId);
        RspEscalationRecordIds.add(ActiveJepoardyRecordId);
        
        system.debug('*** RSPEscalation EmailNotification Service Variable Initialization: 25% ***');
        //Task recordid, list and single email list initialization
        Id TaskRecordTypeId = Schema.SObjectType.task.getRecordTypeInfosByName().get('Task').getRecordTypeId();
        List<Task> SMEAssignTasks = new List<Task>();
        List<Messaging.SingleEmailMessage>  SMEAssignEmails = new List<Messaging.SingleEmailMessage>();
        EmailMessage newEmail;
        
        //Query the from address and Email templateId
        EmailTemplate templateId = [Select id from EmailTemplate where name = 'RSPSMEAssigned'];
        String rspAddress = System.Label.RSP_Escalation_Email_Address;
        OrgWideEmailAddress[] owea = [select Id,Address,DisplayName from OrgWideEmailAddress where Address =: rspAddress];
        system.debug('*** RSPEscalation EmailNotification Service Variable Initialization: 50% ***');
        
        //Query for owner email
        Map<id,string> OwnerIds = new Map<id,String>();
        for(case InvidualUpdatedCase:CasesAfterCDMSMEAssign){
            OwnerIds.put(InvidualUpdatedCase.id,InvidualUpdatedCase.ownerid);
        }
        
        List<user> UserMatrixs = [select Email,Id from user where id In: OwnerIds.values()];
        Map<Id,String> UserMatrixMap  = new map<Id,string>();
        for(User UserMatrix:UserMatrixs){
            UserMatrixMap.put(UserMatrix.id,UserMatrix.Email);
        }
        
        Database.DMLOptions dlo = new Database.DMLOptions();
        dlo.EmailHeader.triggerUserEmail = true;
        
        //Query the SME Team assignment matrix and convert it into map
        Map<String,String> SMEAssignMatrixMap = new Map<String,String>();
        List<RSP_SME_Assignment__c> SMEAssignMatrix = [select SME_Team__c ,SME_Team_Email__c from RSP_SME_Assignment__c];
        for(RSP_SME_Assignment__c SMEAssignMatrixRecord:SMEAssignMatrix){
            SMEAssignMatrixMap.put(SMEAssignMatrixRecord.SME_Team__c,SMEAssignMatrixRecord.SME_Team_Email__c);
        }
        system.debug('*** RSPEscalation EmailNotification Service Variable Initialization: Complete ***');
        
        //get the SME Team, CDM field value from the updated case
        system.debug('*** RSPEscalation EmailNotification Service Iterate cases with SME field change ***');
        for(case CaseAfterCDMSMEAssign:CasesAfterCDMSMEAssign){
            system.debug('*** RSPEscalation EmailNotification Processing case, id: '+CaseAfterCDMSMEAssign.id+' ***');
            String UpdatedCaseSMETeamFieldValue = CaseAfterCDMSMEAssign.SME_Team__c;
            String UpdatedCaserecordTypeId = CaseAfterCDMSMEAssign.RecordTypeId;
            String UpdatedCaseIncidentNo = CaseAfterCDMSMEAssign.Incident_Number__c;
            String UpdatedCaseQueueMail = CaseAfterCDMSMEAssign.Owner_Email__c;
            Datetime NewCaseRSPEscaltedDate = CaseAfterCDMSMEAssign.Escalated_Date__c;
            Boolean ProceedFlag;
            
            //get the old case
            Case CaseBeforeCMDSMEAssign = OldSMECasesMap.get(CaseAfterCDMSMEAssign.id);
            String OldCaseSMETeamFieldValue = CaseBeforeCMDSMEAssign.SME_Team__c;
            Datetime OldCaseRSPEscaltedDate = CaseBeforeCMDSMEAssign.Escalated_Date__c;
            
            //email realted attributes
            Id CaseOwnerId = CaseAfterCDMSMEAssign.OwnerId;
            Id CaseId = CaseAfterCDMSMEAssign.Id;
            String HtmlFullbody;
            String[] toAddresses;
            String[] CcAddresses;
            String CaseOwnerEmailAddresses;
            //String SMETeamAddress = SMEAssignMatrixMap.get(UpdatedCaseSMETeamFieldValue);
            String SMETeamAddress = CaseAfterCDMSMEAssign.SME_Team_Email__c;
            system.debug('*** RSPEscalation SME Team email is: '+SMETeamAddress+' ***');
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();            
            newEmail = new EmailMessage();  
            List<EmailMessage> EmailMessages = new List<EmailMessage>();
            
            if(owea.size() > 0){
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
                newEmail.FromAddress = owea.get(0).Address;
                newEmail.FromName = owea.get(0).DisplayName;
            }
            
            //checked if the case owner is CDM or queue, if queue then stop further processing
            system.debug('*** RSPEscalation EmailNotification case CDM ownership check***');
            if(string.valueOf(CaseOwnerId).startsWith('005')){
                ProceedFlag = true;
                CaseOwnerEmailAddresses = UserMatrixMap.get(CaseOwnerId);
                //CaseOwnerEmailAddresses = CaseAfterCDMSMEAssign.SME_Team_Email__c;
            }else{
                ProceedFlag = false;
            }
            
            system.debug('*** RSPEscalation EmailNotification case CDM ownership status: '+ProceedFlag+'***');
            if(ProceedFlag == true){ 
                system.debug('UpdatedCaseSMETeamFieldValue is'+UpdatedCaseSMETeamFieldValue);
                if(UpdatedCaseSMETeamFieldValue == null  && NewCaseRSPEscaltedDate != OldCaseRSPEscaltedDate
                  && NewCaseRSPEscaltedDate != null){
                    system.debug('*** RSPEscalation EmailNotification SME is assigned with no SME,creating task for CDM ***');
                    //if the SME Team field is not populated, then create a task for CDM
                    Task SMEAssignTask = new Task();
                    SMEAssignTask.RecordTypeId = TaskRecordTypeId;
                    SMEAssignTask.OwnerId = CaseOwnerId;
                    SMEAssignTask.Subject = 'SME not assigned';
                    SMEAssignTask.Status = 'Open';
                    SMEAssignTask.Priority = '3-General';
                    SMEAssignTask.Description = 'System is unable to find the SME for this case. Please manually assign SME for this case.For more details, click the following link:'+'\n'
                        +'https://'+URL.getSalesforceBaseUrl().getHost()+'/'+CaseAfterCDMSMEAssign.Id;
                    SMEAssignTask.WhatId = CaseAfterCDMSMEAssign.Id;
                    SMEAssignTask.setOptions(dlo);                    
                    SMEAssignTasks.add(SMEAssignTask);
                }else if(UpdatedCaseSMETeamFieldValue != null && 
                         UpdatedCaseSMETeamFieldValue != OldCaseSMETeamFieldValue &&
                         UpdatedCaseSMETeamFieldValue != 'Manual Allocation'){  
                    //if the SME Team field has been changed to not null and that value is not euqal to manual allocation
                    //then send email to SME group mail and CC CDM
                    system.debug('*** RSPEscalation EmailNotification SME is assigned with SME,sending email to SME ***');
                                        
                    if(String.isNotEmpty(SMETeamAddress)){
                        system.debug('the SME Team email address is not null, preparing the email');
                        toAddresses = new string[]{SMETeamAddress};
                        newEmail.ToAddress = SMETeamAddress;
                        newEmail.ParentId = CaseId; 
                        
                        if(String.isNotEmpty(CaseOwnerEmailAddresses)){
                            CcAddresses = new String[]{CaseOwnerEmailAddresses};   
                            newEmail.CcAddress = CaseOwnerEmailAddresses;                                                   
                        }else{
                            CcAddresses = new String[]{};        
                        }
                        
                        system.debug('cc email address is'+CcAddresses);
                        //system.debug(CaseOwnerEmailAddresses);
                        Messaging.reserveSingleEmailCapacity(1);                    
                        mail.setToAddresses(toAddresses);
                        mail.setCcAddresses(CcAddresses);                       
                           
                        Messaging.SingleEmailMessage result = Messaging.renderStoredEmailTemplate(templateId.Id,CaseOwnerId,CaseId);
                        mail.setSubject(result.getSubject());                        
                        mail.setHtmlBody(result.getHtmlBody());
                        mail.setPlainTextBody(result.getPlainTextBody());
                        
                        if(AllAttachmentIdList.size()>0){
                            for(attachment SoloAtta:AllAttachmentIdList){
                                if(SoloAtta.parentId == CaseAfterCDMSMEAssign.Id){
                                    AttachmentsIds.add(SoloAtta.Id);
                                }
                            }
                            
                            if(AttachmentsIds.size()>0){
                                mail.setEntityAttachments(AttachmentsIds);
                            }
                            
                        }
                      
                        newEmail.Subject = result.getSubject();
                        newEmail.HtmlBody = result.getHtmlBody();
                        newEmail.TextBody = result.getPlainTextBody();
                        
                        SMEAssignEmails.add(mail); 
                        EmailMessages.add(newEmail);
                    }
                }
            }
        }
        //insert the list of task and send list of emails
        system.debug('*** RSPEscalation EmailNotification: creating tasks ***');
        if(SMEAssignTasks.size() > 0){
            try{
                List<Database.SaveResult> sr = Database.insert(SMEAssignTasks,dlo);//insert SMEAssignTasks;
                system.debug('*** RSPEscalation EmailNotification: creating tasks successfully***');                  
            }catch(DmlException e) {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }
        }else{
            system.debug('*** RSPEscalation EmailNotification: No tasks need to be created***');   
        }
        
        system.debug('*** RSPEscalation EmailNotification: sending emails ***');
        if(SMEAssignEmails.size()>0){
            try{
                Messaging.sendEmail(SMEAssignEmails);
                newEmail.Status = '3';
                newEmail.Incoming = false;
                insert newEmail;                 
                system.debug('*** RSPEscalation EmailNotification: sending emails successfully ***'+''+Limits.getEmailInvocations());
            }catch(Exception e) {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }
        }else{
            system.debug('*** RSPEscalation EmailNotification: No emails needs to be sent***');  
        }
        

                        
    }
    
    public void caseEntitlementAssignment(List<Case> cs){
        System.debug('((((((((((((2'+cs);
        
        Set<String> smeSet = new Set<String>();
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE Name='RSP Escalations Business Hours' limit 1];
        Map<String,Id> mapRSPEntitlement = new Map<String,Id>();
        for(Entitlement ent :[SELECT Id,Name FROM entitlement where name IN ('RSP Escalations Entitlements - BH','RSP Escalations Entitlements - NBH')]){
            mapRSPEntitlement.put(ent.name,ent.id);
        }
        for(RSP_SME_Assignment__c rsa :[SELECT id,SME_Team__c,Technology_Type__c from RSP_SME_Assignment__c]){
            smeSet.add(rsa.SME_Team__c + rsa.Technology_Type__c);  
        }
        if(bh!=null){
            for(Case c :cs){
                if(smeSet.contains(c.SME_Team__c + c.Site_Technology_Type__c)){
                    if(BusinessHours.isWithin(bh.id,c.Escalated_Date__c) && mapRSPEntitlement.containsKey('RSP Escalations Entitlements - BH')){
                        c.EntitlementId = mapRSPEntitlement.get('RSP Escalations Entitlements - BH');
                    }
                    else if(!BusinessHours.isWithin(bh.id,c.Escalated_Date__c) && mapRSPEntitlement.containsKey('RSP Escalations Entitlements - NBH')){
                        c.EntitlementId = mapRSPEntitlement.get('RSP Escalations Entitlements - NBH');
                    }
                }
                else{
                    c.EntitlementId = null;
                }
            }
        }        
    }
    
    public static void completeMilestone(List<Id> caseIds, DateTime complDate) {
      
    List<CaseMilestone> cmsToUpdate = [select Id, completionDate,TargetDate,isViolated from CaseMilestone cm where caseId in :caseIds and completionDate = null];
        if (cmsToUpdate.isEmpty() == false){
          for (CaseMilestone cm : cmsToUpdate){
              /*if(cm.isViolated){
                  cm.completionDate = cm.TargetDate;
              }
              else{*/
                  cm.completionDate = complDate;
              //}
          }
          update cmsToUpdate;
        } // end if
    }
    
}