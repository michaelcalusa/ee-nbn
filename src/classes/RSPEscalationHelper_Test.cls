@isTest
public class RSPEscalationHelper_Test {
    
    @testSetup static void setup(){
        //create test Site data
        Site__c TestSite = new Site__c();
        TestSite.Name = 'LOC000065821123';
        TestSite.Technology_Type__c = 'FTTP';
        insert TestSite;
        
        //Create test Account data
        Account RsptestAccount = new Account();
        RsptestAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Retail Service Provider').getRecordTypeId();
        RsptestAccount.Name = 'RSPTestAccount';
        RsptestAccount.Status__c = 'Active';
        insert RsptestAccount;
        
        //Create test user data
        Profile p = [SELECT Id FROM Profile WHERE Name='NBN Customer Delivery Support']; 
        User u = new User(Alias = 'Tester', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', ProfileId = p.Id, 
                          UserName='RspTestuser@testorg.com',TimeZoneSidKey = 'Australia/Sydney',
                          LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US');
        insert u;
        
        //create test Team Assignment matrix data
        Case_Assignment__c TeamAssignTestRecord = new Case_Assignment__c();
        TeamAssignTestRecord.User__c = u.id;
        TeamAssignTestRecord.Available__c = true;
        TeamAssignTestRecord.respesc_CaseType__c = 'Customer Incident';
        TeamAssignTestRecord.rspesc_RSP_Account__c = RsptestAccount.Id;
        insert TeamAssignTestRecord;
        
        
        //create test SME matrix data Business Hour Team
        List<RSP_SME_Assignment__c> SMEAssignTestRecords = new List<RSP_SME_Assignment__c>();
        RSP_SME_Assignment__c TestSMEAssignRecordBH = new RSP_SME_Assignment__c();
        TestSMEAssignRecordBH.Technology_Type__c = 'FTTP';
        TestSMEAssignRecordBH.Business_Hours_Team__c = True;
        TestSMEAssignRecordBH.SME_Team__c = 'Copper Escalations Team';
        TestSMEAssignRecordBH.SME_Team_Email__c = 'test1@example.com';
        SMEAssignTestRecords.add(TestSMEAssignRecordBH);
        
        //create test SME matrix data Outside Business Hour Team
        RSP_SME_Assignment__c TestSMEAssignRecordNonBH = new RSP_SME_Assignment__c();
        TestSMEAssignRecordNonBH.Technology_Type__c = 'FTTP';
        TestSMEAssignRecordNonBH.Business_Hours_Team__c = False;
        TestSMEAssignRecordNonBH.SME_Team__c = 'Fibre Escalations Team';
        TestSMEAssignRecordNonBH.SME_Team_Email__c = 'wayneni@nbnco.com.au';
        SMEAssignTestRecords.add(TestSMEAssignRecordNonBH);
        insert SMEAssignTestRecords;
        
        //create test case data
        //Test Case One
        List<Case> Testcases = new List<Case>();
        case TestCaseOne = new case();
        TestCaseOne.Escalation_Channel__c  = 'Email';
        TestCaseOne.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assurance Jeopardy').getRecordTypeId();
        TestCaseOne.Escalation_Source__c = 'RSP';
        TestCaseOne.RSP_Escalated__c = true;
        TestCaseOne.OwnerId = u.Id;
        TestCaseOne.Incident_Number__c = 'INC000007321660';
        TestCaseOne.RSP_ISP__c = RsptestAccount.Id;
        TestCaseOne.OpCat1__c = 'Customer Incident';
        TestCaseOne.SME_Team__c = null;
        TestCaseOne.SME_Team_Email__c = 'test@testadddress.com';
        TestCaseOne.Escalated_Date__c = system.now().adddays(-4);
        Testcases.add(TestCaseOne);
        

        //Test Case Two        
        case TestCaseTwo = new case();
        TestCaseTwo.Escalation_Channel__c  = 'Email';
        TestCaseTwo.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Assurance Jeopardy').getRecordTypeId();
        TestCaseTwo.Escalation_Source__c = 'RSP';
        TestCaseTwo.RSP_Escalated__c = true;
        TestCaseTwo.OwnerId = u.Id;
        TestCaseTwo.Incident_Number__c = 'INC000007321660';
        TestCaseTwo.RSP_ISP__c = RsptestAccount.Id;
        TestCaseTwo.Site__c = TestSite.id; 
        TestCaseTwo.OpCat1__c = 'Customer Incident';
        TestCaseTwo.SME_Team__c = 'Fibre Escalation Team';
        TestCaseTwo.Escalated_Date__c = system.now();
        Testcases.add(TestCaseTwo);
        
        insert Testcases;
        
        
    }
    
    @isTest static void CDMSMEEmailTestWithoutSMEPopulated_ValidCDM(){ 
        
        test.startTest();
        user testuser = [select id from user where UserName='RspTestuser@testorg.com'];
        case oldcase = [select SME_Team__c from case where OwnerId =: testuser.Id and SME_Team__c = null limit 1];
        //oldcase.SME_Team__c = 'Copper Escalations Team';
        oldcase.Escalated_Date__c = system.now();
        update oldcase;
        Task testtask = [select Id, Subject, OwnerId from Task where OwnerId =:testuser.Id limit 1];
        test.stopTest();
        //system.assertEquals('SME not assigned', testtask.Subject);
        
        /*
        Integer invocations = Limits.getEmailInvocations();
        test.stopTest();
        //Email the sender and cc, so it should be 2
        system.assertEquals(2, invocations, 'An email should be sent'); 
        */
        
    }
    
    @isTest static void CDMSMEEmailTestWithSMEPopulatedScenario_validCDM(){
        test.startTest();
        user testuser = [select id from user where UserName='RspTestuser@testorg.com'];
        case oldcase = [select SME_Team__c from case where OwnerId =: testuser.Id and SME_Team__c != null limit 1];
        oldcase.SME_Team__c = 'Fibre Escalations Team';
        update oldcase;
        
        Integer invocations = Limits.getEmailInvocations();
        test.stopTest();
        //Email the sender and cc, so it should be 2
        //system.assertEquals(2, invocations, 'An email should be sent');   
        
        /*
        Task testtask = [select Id, Subject, OwnerId from Task where OwnerId =:testuser.Id limit 1];
        test.stopTest();
        system.assertEquals('SME not assigned', testtask.Subject);*/
        
    }
    

}