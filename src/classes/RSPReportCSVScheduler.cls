/***************************************************************************************************
Class Name:  RSPReportCSVScheduler
Created Date: 02-03-2018
Function    : This class is to generate and send RSP reports on a daily schedule
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Rinu Kachirakkal		   02-03-2018               Created
****************************************************************************************************/
global with sharing class RSPReportCSVScheduler implements schedulable {
    
    global void execute(SchedulableContext sc) {
        scheduleJob();
    }
    
    @future(callout=true)
    public static void scheduleJob() {

        // Get the report ID
        List <Report> reportList = [SELECT Id, Name, DeveloperName FROM Report WHERE 
                                    DeveloperName = 'RSPReportExtract'];
        List <OrgWideEmailAddress> ordWideAddressList = [SELECT Id FROM OrgWideEmailAddress WHERE DisplayName = 'Developer Liaison'];
        
        List<RSP_Report_To_Address__mdt> toAddresses = [SELECT Id, To_Address__c FROM RSP_Report_To_Address__mdt];
            
        
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        
        List< Messaging.EmailFileAttachment> attachmentList = new List< Messaging.EmailFileAttachment>();
                
        String reportId = (String)reportList.get(0).get('Id');
        String reportName = (String)reportList.get(0).get('Name');
        
        // To run a report synchronously
        Reports.ReportResults results = Reports.ReportManager.runReport(reportId, true);
        
        ApexPages.PageReference report = new ApexPages.PageReference('/'+reportId+'?csv=1');
        
        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        attachment.setFileName(reportName + '.csv');
        
        Blob content = !Test.isRunningTest() ? report.getContent() : Blob.valueOf('Test');
        attachment.setBody(content);
        
        attachment.setContentType('text/csv');
        attachmentList.add(attachment);
        
        message.setFileAttachments(attachmentList);
        message.setSubject('RSP Salesforce Report Extract for ' + Date.today());
        String htmlBody = '<html>  <body> <table> <tr> <td>  <span >Hi Team,<br/><br/></span> <span>Please find attached the RSP Report Extract.</span> <br/> <br/> <br/>'
						  + '<span>Thank you and regards,<br/><b>nbn</b>™ New Developments team</span><br/><br/><br/><br/>'
						  + '</td> </tr> </table> </div> </body> </html>';
		message.setHtmlBody(htmlBody); 
		if(!ordWideAddressList.isEmpty()) {
            message.setOrgWideEmailAddressId(ordWideAddressList.get(0).Id);
        }
        
        List<String> sendTo = new List<String>();
        
        for (RSP_Report_To_Address__mdt toAddress : toAddresses) {
        	sendTo.add(toAddress.To_Address__c);
        }
        
        message.setToAddresses(sendTo);
        emailList.add(message); 
        
        if(!emailList.isEmpty()){
            Messaging.sendEmail(emailList); 
        }
    }
}