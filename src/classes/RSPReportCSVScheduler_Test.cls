/***************************************************************************************************
Class Name:  RSPReportCSVScheduler_Test
Created Date: 02-03-2018
Function    : This is a test class for RSPReportCSVScheduler
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Rinu Kachirakkal		   02-03-2018               Created
****************************************************************************************************/
@isTest
public class RSPReportCSVScheduler_Test {
    @isTest(SeeAllData='true') // This is to get the report details
	public static void testScheduleJob() {
		List <Report> reportList = [SELECT Id,DeveloperName FROM Report where
          DeveloperName = 'RSPReportExtract'];
      	String reportId = (String)reportList.get(0).get('Id');

        //Create test data
		Account testAccount = TestDataClass.CreateTestAccountData();
		Contact billContact = TestDataClass.CreateTestContactData();
        Contact noBillContact = TestDataClass.CreateTestContactData();

		// Create Account Address (Contract)		        
        Contract accAddr = new Contract();
        Id accAddrRTId = Schema.SObjectType.Contract.getRecordTypeInfosByName().get('NewDevs Class 1-2').getRecordTypeId();
        accAddr.RecordTypeId = accAddrRTId;
		accAddr.Status = 'Pending Execution';
        accAddr.AccountId = testAccount.Id;
        accAddr.Billing_Contact__c = billContact.Id;
        accAddr.Dunning_Contact__c = noBillContact.Id;
        accAddr.BillingCountry = 'Australia';
		accAddr.ShippingCountry = 'Australia';
        insert accAddr;

		// Create Account Address (Contract)		        
        Contract accAddr2 = new Contract();        
        accAddr2.RecordTypeId = accAddrRTId;
		accAddr2.Status = 'Pending Execution';
        accAddr2.AccountId = testAccount.Id;
        Contact bCon = TestDataClass.CreateTestContactData();
        accAddr2.Billing_Contact__c = bCon.Id;
        Contact dCon = TestDataClass.CreateTestContactData();
        accAddr2.Dunning_Contact__c = dCon.Id;
        accAddr2.BillingCountry = 'Australia';
		accAddr2.ShippingCountry = 'Australia';
        insert accAddr2;
        
        //Create a Dveleopments object record
        Development__c devObj = new Development__c();      
        devObj.Name='testName';
        devObj.Development_ID__c='AKHS833';
        devObj.Account__c=testAccount.Id;
        devObj.Primary_Contact__c=noBillContact.Id;
        devObj.Suburb__c='testsuburd';
        insert devObj;
        
        // Create Stage Application object record
        Stage_Application__c stageAppObj = new Stage_Application__c();        
        stageAppObj.Name='testName';
        stageAppObj.Request_ID__c ='AKHS833';
        stageAppObj.Development__c=devObj.Id;
        stageAppObj.Account__c=testAccount.Id;
        stageAppObj.Primary_Contact__c=noBillContact.Id;
        insert stageAppObj; 
        
        Test.startTest();

        RSPReportCSVScheduler rspScheduler = new RSPReportCSVScheduler();
		String schedule = '0 0 2 * * ?'; 
		system.schedule('RSP Report Scheduler Test', schedule, rspScheduler);	
		Test.stopTest();
    }

}