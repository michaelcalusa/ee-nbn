/***************************************************************************************************
Class Name:         RecordAlternateContinuityInteraction_CX
Class Type:         Extension Class 
Version:            1.0 
Created Date:       13 Sept 2017
Function:           This class drives dynamic page elements of an Alternate Continuity interaction record based on the Task object as well as integration with NBNCo on premise system CIS
Input Parameters:   None 
Output Parameters:  None
Description:        Used in RecordAlternateContinuityInteraction Visualforce page
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Beau Anderson     13/9/2017      Created - Version 1.0 Refer CUSTSA-5361 for Epic description
* Narashimha Binkam 30/10/2017      Added code for integration and bug fixing
* Sanghita Das      15/12/2017      Refer CUSTSA-8359 for Epic description
****************************************************************************************************/ 

public with sharing class RecordAltCon_CX {   
    
    // VF Attributes
    public Task taskRecord {get; set;}
    public Boolean isRecordInteraction {get; set;} 
    public Boolean isValidatePRIDisabled {get;set;}
    public Boolean isNetworkTrailDisabled{get;set;}
    public Boolean isPairDisabled{get;set;}
    public String strTrial{get; private set;}
    public Boolean pnlNetworkTrailResponse {get; set;}
    public Boolean pnlPairSwapResponse {get; set;}
    public String errorMsg {get; set;}
    
    public String overrideFaultyPair{get;set;}
    
    public Boolean isPolarEnabled{ get; set; }
    
    public Boolean isSaveAndCloseEnable{ get; set; }
    
    // Class Level Attrributes
    private Task_Custom__c taskCustomRecord;
    private Account assuranceAccount;
    private Id taskRecordTypeId;
    private Integer timOutValue = 0;
    
    static final String TASK_PRI_API_INCLUDE_VALUE = '?include=address';
    static final String TASK_SAVE_AND_CLOSE_MESSAGE = 'Thank you for your service ' + UserInfo.getFirstName() + '. We have recorded the following information. If needed please update the interaction.';
    static final String TASK_TELSTRA_TECH_NUMBER_FORMAT = 'Phone number must be format 04 followed by 8 numeric digits. Please check the number entered';
    static final String TASK_PRI_API_CONNECTION_FAILED = 'Network communitcation problem. Could not connect to NBNCo onpremise system';
  
    public String typeOfCall{
        get{
            
            if(this.taskRecord.Reason_for_Call__c == 'Pair Swap' || 
                this.taskRecord.Reason_for_Call__c == 'Inventory Update'){
                return 'PAIRSWAP';
            }
            
            if(this.taskRecord.Reason_for_Call__c == 'Cable Record Enquiry'){
                return 'NETWORK';
            }
            return null;
        }
        
    }
    
    public Integer polarCount{
        get{
            if(polarCount == null) return 0;return polarCount;
            
        }set;
    }
    
    // Constructor
    public RecordAltCon_CX(ApexPages.StandardController sController){
        
        List<String> lstFields = new List<String>(Task.getSObjectType().getDescribe().fields.getMap().keySet());
        if (!Test.isRunningTest())sController.addFields(lstFields);
        
        this.taskRecord = (Task)sController.getRecord();
        
        if(this.taskRecord != null && this.taskRecord.Id != null){
            isRecordInteraction = false;
        }
        else{
            isRecordInteraction = true;
        }
        
        if(!String.isBlank(this.taskRecord.CPI_ID__c)){
            isValidatePRIDisabled = true;
        }
        else{
            isValidatePRIDisabled = false;
        }
        
        
        //isNetworkTrailRendered = false;
        //isPairRendered = false;
        isPolarEnabled = false;
        pnlNetworkTrailResponse = false;
        pnlPairSwapResponse = false;
        isSaveAndCloseEnable = false;
        errorMsg = '';
        overrideFaultyPair = 'Yes';
        
        if(assuranceAccount == null){
            Id recTypeID = GlobalCache.getRecordTypeId('Account','Company');
            List<Account> lst = [SELECT Id, Name FROM Account WHERE RecordTypeId=:recTypeID AND Name = 'nbn Service Assurance'];
            if(lst.size() > 0){
                assuranceAccount = lst.get(0);
            }
            else{
                errorMsg = 'nbn Service Assurance record not found.';
            }
        }
        
        if(taskRecordTypeId == null){
            taskRecordTypeId = GlobalCache.getRecordTypeId('Task','GCC Interaction');
        }
    }
    
    // Action for Record Interaction button
    public void recordInteraction(){
        isRecordInteraction = false;
    }
    
    
    // Action Reason for call
    public void actionReasonForCall(){
        isPolarEnabled = false;
        errorMsg = '';
    
        if(this.taskRecord.Reason_for_Call__c == 'Inventory Update'){
            overrideFaultyPair = 'Yes';
        }
        else{
            overrideFaultyPair = 'No';
        }
    }
    
    // Validate PRI
    public void validatePRI(){
    
        if(String.isBlank(this.taskRecord.PRI__c) == true){
            taskRecord.PRI__c.addError(Pri_Format__C.getInstance('TASK_PRI_FORMAT_ERROR').ErrorDescription__c);
            return;
        } 
        
        Matcher isPRIValid = Pattern.compile('^PRI[0-9]{12}$').matcher(this.taskRecord.PRI__c);
        
        Matcher isPhoneValid = Pattern.compile('^04[0-9]{8}$').matcher(this.taskRecord.Telstra_Tech_Contact_number__c);
        
        if(isPRIValid.matches() == false){
            taskRecord.PRI__c.addError(Pri_Format__C.getInstance('TASK_PRI_FORMAT_ERROR').ErrorDescription__c);
            return;
        }
        
        if(isPhoneValid.matches() == false){
            taskRecord.Telstra_Tech_Contact_number__c.addError(TASK_TELSTRA_TECH_NUMBER_FORMAT);
            return;
        }
        
        
        
        HttpResponse response = callOut_CIS(this.taskRecord.PRI__c);
        
        if(response == null){
            return;
        }
        
        
        
        List<AltConWrapper.priWrapper> lstWrapper = new List<AltConWrapper.priWrapper>();
    
        //errorMsg = response.getBody();
        //return;
        
        AltConWrapper.RootProdObject objWrapper;
        try{
            objWrapper = (AltConWrapper.RootProdObject)JSON.deserialize(response.getBody(), AltConWrapper.RootProdObject.Class);
            for(Integer i = 0; i <= objWrapper.data.size()-1; i++){
                try{
                    taskRecord.CPI_ID__c = objWrapper.data[i].attributes.copperPathId;
                }
                catch(Exception ex){
                    taskRecord.PRI__c.addError('Copper Path Id not found: ' + ex.getMessage());
                    return;
                }           
                if(objWrapper.data[i].relationships != null && objWrapper.data[i].relationships.address != null 
                    && objWrapper.data[i].relationships.address.data != null){
                
                    taskRecord.Location_ID__c = objWrapper.data[i].relationships.address.data.id;
                
                }
                
                if(objWrapper.included[i]!= null && objWrapper.included[i].attributes != null &&
                    objWrapper.included[i].attributes.address != null){
                        
                    taskRecord.Location_Address__c = objWrapper.included[i].attributes.address.unstructured;
                }
                
                if(objWrapper.included[i] != null && objWrapper.included[i].attributes != null && objWrapper.included[i].attributes.PrimaryAccessTechnology != null){
                    taskRecord.PRITechName__c = objWrapper.included[i].attributes.PrimaryAccessTechnology.technologyType;
                }
                
                if(objWrapper.data[i].type != null){
                    taskRecord.PRITechType__c = objWrapper.data[i].type;
                }
            } 
        }
        catch(Exception ex){
            taskRecord.PRI__c.addError('Error in parsing response: ' + ex.getMessage());
            return;
        }
        
        
        // Insert New Task
        this.taskRecord.RecordTypeId = taskRecordTypeId;
        taskRecord.whatid = assuranceAccount.Id;
        insert this.taskRecord;
        System.debug('^^^' + this.taskRecord.Id);
        
        
        // Query fields again
        List<String> lstFields = new List<String>(Task.getSObjectType().getDescribe().fields.getMap().keySet());
        Id taskId = this.taskRecord.Id;
        String qry = 'Select ' + String.join(lstFields,',') + ' FROM Task WHERE Id = :taskId';
        this.taskRecord = Database.query(qry);
        
        
        isValidatePRIDisabled = true;
        errorMsg = 'PRI has been validated successfully.';
        
                
        
        
    }
        
    // Pair Swap
    public void actionPairSwap(){
        // if submit swap clicked again, reset the staus to blank.      
        taskRecord.Network_Inventory_Swap_Status__c = '';      
        callout_PNI('ChangeDistributionPair', taskRecord.Reason_for_Call__c, taskRecord.Desired_Pair_ID__c);
    }
    
    // Network Trail
    public void actionNetworkTrail(){
        // if Network trail clicked again, reset the staus to blank.      
        taskRecord.NetworkTrail_Status__c = ''; 
        callout_PNI('NetworkTrail', taskRecord.PRITechName__c, '');
    }
    
    // Polar
    public void actionPolar() {
        Integer remainingSec = getTimeOut() - polarCount;
        errorMsg = 'Checking for response, (' + remainingSec + ' seconds remaining) ....';
        
        
        polarCount = polarCount + 5;
        
        if (polarCount > getTimeOut()){
            isPolarEnabled = false;
            isPairDisabled = false;
            isNetworkTrailDisabled = false;
            isSaveAndCloseEnable = true;
            errorMsg = 'Request timed out, please save and complete the interaction.';
            return;
        }
        
        
        System.debug('^^^' + polarCount);
        System.debug('^^^' + getTimeOut());
        
        // Network Trail
        if(taskRecord.NBNCorrelationId__c != null){
            
            // Query fields again
            List<String> lstFields = new List<String>(Task.getSObjectType().getDescribe().fields.getMap().keySet());
            Id taskId = this.taskRecord.Id;
            String qry = 'Select ' + String.join(lstFields,',') + ',Task_Custom__r.Opair__c, Task_Custom__r.MPair__c' +  ' FROM Task WHERE Id = :taskId';
            this.taskRecord = Database.query(qry);
            
            System.debug('^^^taskRecord' + taskRecord);
            
            // Pair Swap
            if(String.isBlank(taskRecord.Network_Inventory_Swap_Status__c) == false){
                errorMsg = '';
                pnlPairSwapResponse = true;
                isPolarEnabled = false;
                isNetworkTrailDisabled = false;
                isSaveAndCloseEnable = true;
            }
            
            // Pair Swap
            if(String.isBlank(taskRecord.Network_Inventory_Swap_Status__c) == false && String.isBlank(taskRecord.Error__c) == true){
                isPairDisabled = true;
            }
            else{
                isPairDisabled = false;
            }
            
            
            // Network Trail
            if(String.isBlank(taskRecord.NetworkTrail_Status__c) == false){
                
                List<String> pillarList;
                strTrial = '';
                
                if (String.isBlank(taskRecord.NetworkTrailCoRelationID__c) == false){
                    strTrial += 'Building MDF: ' + taskRecord.NetworkTrailCoRelationID__c + '<br/>';
                }
                
                if (String.isBlank(taskRecord.TCO_or_MDF__c) == false){
                    strTrial += 'TCO or MDF: ' + taskRecord.TCO_or_MDF__c+ '<br/>';
                }
                
                if (String.isBlank(taskRecord.Copper_Lead_In_Cable__c) == false){
                    strTrial += 'Copper Lead-In Cable: ' + taskRecord.Copper_Lead_In_Cable__c+ '<br/>';
                }
                if (String.isBlank(taskRecord.Copper_Lead_In_Cable_Pair__c) == false){
                    //strTrial += 'Copper Lead-In Cable Pair: ' + taskRecord.Copper_Lead_In_Cable_Pair__c+ '<br/>';
                }
                
                if (String.isBlank(taskRecord.DPU_Serial_Number__c) == false){
                    strTrial += 'DPU Serial Number: ' + taskRecord.DPU_Serial_Number__c+ '<br/>';
                }
                if (String.isBlank(taskRecord.CIU_DPU_ID__c) == false){
                    //strTrial += 'DPU Name: ' + taskRecord.CIU_DPU_ID__c+ '<br/>';
                }
                if (String.isBlank(taskRecord.DPU_Port_Number__c) == false){
                    strTrial += 'DPU Port Number: ' + taskRecord.DPU_Port_Number__c+ '<br/>';
                }
                if (String.isBlank(taskRecord.CIU_Cut_In_Status__c) == false){
                    strTrial += 'CIU Cut-In Status: ' + taskRecord.CIU_Cut_In_Status__c+ '<br/>';
                }
                if (String.isBlank(taskRecord.Copper_Distribution_Cable__c) == false){
                    strTrial += 'Copper Distribution Cable: ' + taskRecord.Copper_Distribution_Cable__c+ '<br/>';
                }
                
                if (String.isBlank(taskRecord.O_Pair__c) == false){
                    strTrial += 'O Pair: ' + taskRecord.O_Pair__c+ '<br/>';
                }
                
                if (String.isBlank(taskRecord.Task_Custom__r.OPair__c) == false){
                    pillarList = taskRecord.Task_Custom__r.OPair__c.split(',');
                    
                    if(pillarList.size()>0){
                        integer i=97;
                        List<integer> iList = new List<integer>{i};
                        for(String s :pillarList){
                            String convertedChar = String.fromCharArray(iList);
                            strTrial += 'O Pair: ' + convertedChar + s + '<br/>';
                            iList.clear();
                            iList.add(i++);
                        }
                    }
                }
                
                
                if (String.isBlank(taskRecord.Copper_Main_Cable__c) == false){
                    strTrial += 'Copper Main Cable: ' + taskRecord.Copper_Main_Cable__c+ '<br/>';
                }
                
                if (String.isBlank(taskRecord.M_pair__c) == false){
                    strTrial += 'M pair: ' + taskRecord.M_pair__c+ '<br/>';
                }
                
                if (String.isBlank(taskRecord.Task_Custom__r.MPair__c) == false){
                    strTrial += 'M pair: ' + taskRecord.Task_Custom__r.MPair__c + '<br/>';
                }
                
                if (String.isBlank(taskRecord.FTTN_Node__c) == false){
                    strTrial += 'FTTN Node: ' + taskRecord.FTTN_Node__c + '<br/>';
                }
                
                if (String.isBlank(taskRecord.Type_Of_Node__c) == false){
                    strTrial += 'Type Of Node: ' + taskRecord.Type_Of_Node__c + '<br/>';
                }
                
                if(String.isBlank(taskRecord.Error_trial__c) == false){
                    System.debug('^^^failed'); 
                    taskRecord.Error_trial__c = taskRecord.Error_trial__c;
                }
                
                errorMsg = '';
                pnlNetworkTrailResponse = true;
                isPolarEnabled = false;
                isPairDisabled = false;
                isNetworkTrailDisabled = false;
                isSaveAndCloseEnable = true;
                System.debug('^^^strTrial ' + strTrial);
            } 
            
        }
    }
    
    
    
    
    private Integer getTimeOut(){
        NBNIntegrationInputs__c customSetting;
        
        if(typeOfCall == 'PAIRSWAP'){
            customSetting = NBNIntegrationInputs__c.getInstance('ChangeDistributionPair');
        }
        
        if(typeOfCall == 'NETWORK'){
            customSetting = NBNIntegrationInputs__c.getInstance('NetworkTrail');
        }
        
        try{
            if(customSetting.UI_TimeOut__c != null){
                // UI_TimeOut__c = seconds 
                timOutValue = Integer.valueOf(customSetting.UI_TimeOut__c);
            }
        }
        catch(exception ex){
            timOutValue = 30;
        }
        
        if(timOutValue < 5){
            timOutValue = 30;
        }
        return timOutValue;
    }
    
    
    
    
    
    // Helper method to generateUnique Number
    private static String uniqueNumber(string guuid){ 
        Integer nextByte = 0;
        String kHexChars = '0123456789abcdef';
        for(Integer i=0; i<16; i++){
            if (i==4 || i==6 || i==8 || i==10){
                guuid += '-';
             }
             nextByte = (Math.round(Math.random() * 255)-128) & 255;
             
             if (i==6){
                 nextByte = nextByte & 15;
                 nextByte = nextByte | (4 << 4); 
             }
             
             if (i==8){
                 nextByte = nextByte & 63;
                 nextByte = nextByte | 128;
             }
             
             guuid += getCharAtIndex(kHexChars, nextByte >> 4);
             guuid += getCharAtIndex(kHexChars, nextByte & 15);
        }
        return guuid;
    }
    
    // Helper method to get Character at Index
    private static string getCharAtIndex(String str, Integer index) {
        if (str == null){
            return null;
        }
        
        if (str.length() <= 0){
            return str;
        }
        
        if (index == str.length()){
            return null;
        }
        return str.substring(index, index+1);
    }
    
    
    public void saveTask(){
        System.debug('@@' + taskRecord.Type_of_Line_Swap__c);
        System.debug('@@' + taskRecord.Reason_for_Call__c);
        upsert taskRecord;
        errorMsg = TASK_SAVE_AND_CLOSE_MESSAGE;
    }
    
    public PageReference saveTaskandComplete(){
        upsert taskRecord;
        PageReference pg = Page.ViewAlternateContinuityInteraction;
        pg.getParameters().put('Id', taskRecord.Id);
        pg.getParameters().put('showMsg', '1');
        pg.setredirect(true);
        return pg;
    }
    
    public PageReference cancelTask() {
        String profileName = [Select Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
        
        if(profileName == 'System Administrator'){
            
            try{
                // Delete Task.
                if(taskRecord != null && taskRecord.Id != null)
                    delete taskRecord;
                if(taskCustomRecord != null && taskCustomRecord != null)
                    delete taskCustomRecord;
                }
            catch(Exception ex){
                
            }
        }
        
            
        PageReference pg = Page.RecordAlternateContinuityInteraction;
        pg.setredirect(true);
        return pg;
    }
    
    // Helper method to make callout and Validate PRI
    private HttpResponse callOut_CIS (String strPRI){
        HttpResponse response = new HttpResponse();
                 
        String include = String.valueOf(TASK_PRI_API_INCLUDE_VALUE);  
        NBNIntegrationInputs__c nbnCISProductInstaces = NBNIntegrationInputs__c.getInstance('CISProductsAPI');        
        HttpRequest request = new HttpRequest();
        Http http = new Http();
        //Header being set due to requirements from the API that we are integrating with. Details can be found on NBNCo sharepoint site
        String URLEndPoint = nbnCISProductInstaces.EndpointURL__c;
        List<String> priList = new List<String>{strPRI};
        URLEndPoint = String.format(URLEndPoint,priList );

        request.setEndpoint('callout:NBNAPPGATEWAY' + URLEndPoint);       
        request.setMethod('GET');
        request.setHeader('X-NBN-Delivery-Partner-Id',String.valueOf(nbnCISProductInstaces.AccessSeekerId__c));
        request.setHeader('X-Global-Transaction-ID', uniqueNumber(''));
        request.setHeader('X-NBN-BusinessChannel', String.valueOf(nbnCISProductInstaces.XNBNBusinessChannel__c));
        request.setHeader('NBN-Environment-Override', String.valueOf(nbnCISProductInstaces.NBN_Environment_Override__c));
        request.setTimeOut(Integer.valueof(nbnCISProductInstaces.Call_TimeOut_In_MilliSeconds__c));
        
            System.debug('@@@@@' + request);        
        try{
            response = http.send(request); 
        }
        catch(Exception ex){ 
            System.debug('@@@@@' + ex.getMessage());
            this.taskRecord.PRI__c.addError('PRI Validation callout failed: ' + ex.getMessage());
            return null;
        }
        System.debug('^^^' + response);
        
        if(response != null && response.getStatusCode() == 200 && response.getBody() != '{}' && String.isBlank(response.getBody()) == false){
            System.debug('^^^' + response.getBody());
            return response;
        }
        
        //GlobalConstants.TASK_PRI_API_CONNECTION_FAILED = 'Network communitcation problem. Could not connect to NBNCo onpremise system';
        // Pri_Format__C.getInstance('TASK_PRI_NOT_FOUND_ERROR').ErrorDescription__c = 'WARNING! The PRI number has not been found on NBNCo systems for the copper technology type and associated NBNCo delivery partner. Please check the number,confirm the technology type is copper and the delivery partner is correct
        
        if(response != null){
            String msg = 'ERROR in callout: ' + response.getStatusCode() + ' ' + response.getStatus();
            taskRecord.PRI__c.addError(msg);
            return null;
        }
        else{
            taskRecord.PRI__c.addError('ERROR in callout: ' + GlobalConstants.TASK_PRI_API_CONNECTION_FAILED);
            return null;
        }
        return null;
        
    }
    
    // Helper method to call NBN Micro Services
    private void callout_PNI(String calloutType, String technologyType, String requestedPair){
        polarCount = 0;
        String strnbnCorrelationId = uniqueNumber('');
        errorMsg = '';
        HttpResponse response = new HttpResponse();
        Http rh = new Http();
        HttpRequest req = new HttpRequest();
        
       NBNIntegrationInputs__c cType  = NBNIntegrationInputs__c.getInstance(calloutType);
        req.setMethod('POST');
        
        if(cType.Is_Api_Gateway__c){
         req.setHeader('x-api-key', cType.x_api_key__c);
        }
        req.setHeader('nbn-role', cType.nbn_Role__c);
        req.setHeader('nbn-requestor-platform', cType.XNBNBusinessChannel__c);
        req.setHeader('nbn-correlation-id', strnbnCorrelationId);
        req.setHeader('Content-Type', 'application/json');
        req.setTimeOut(integer.valueof(cType.Call_TimeOut_In_MilliSeconds__c));

    
        req.setHeader('NBN-Environment-Override', String.valueOf(cType.NBN_Environment_Override__c));
        //req.setHeader('NBN-Environment-Override', 'SIT1');
        
        String endpoint;
        if(cType.Is_Api_Gateway__c){
          endpoint = cType.API_GW_Callout_URL__c + cType.EndpointURL__c;
        }
        else{
          endpoint = 'callout:NBNAPPGATEWAY' + cType.EndpointURL__c;
        }
        
        
        // for deverloper environments use mock responses.
        if(System.URL.getSalesforceBaseUrl().getHost().containsIgnoreCase('csadev') || 
            System.URL.getSalesforceBaseUrl().getHost().containsIgnoreCase('altcont')){
            req.setEndpoint('https://nb7c069.restletmocks.net' + cType.EndpointURL__c);
        }
        else{
            req.setEndpoint(endpoint);
        }
        
        Map<String, String> body = new Map<String, String>();
        body.put('tokenId', cType.Token__c);
        body.put('copperPathId', taskRecord.CPI_ID__c);
        
        
        
        if(String.isBlank(requestedPair) == false){
            body.put('requestedPair', requestedPair);
            body.put('incidentId', requestedPair);
        }
        
        if(this.taskRecord.Reason_for_Call__c == 'Inventory Update'){
            body.put('changeReason', 'DATA-CORRECTION');
        }
        
        else if(this.taskRecord.Reason_for_Call__c == 'Pair Swap'){
            body.put('changeReason', 'FAULT');
        }
        
        
        if(calloutType == 'NetworkTrail') {
            body.put('accessServiceTechnologyType', technologyType);
        }
        
        if(this.taskRecord.Reason_for_Call__c == 'Inventory Update' || this.taskRecord.Reason_for_Call__c == 'Pair Swap'){
            body.put('overrideFaultyPair', overrideFaultyPair);
        }
        
        
        String sbody = JSON.serialize(body);
        req.setBody(sbody);
        system.debug('@@@HTTPRequestBody:' + sbody); 
            
        try{
            response = rh.send(req); 
            if(response != null){
                system.debug('^^^response ' + response);
                system.debug('^^^response ' + response.getStatus());
                system.debug('^^^response ' + response.getStatusCode());
                system.debug('^^^response ' + response.getBody());
            }
        }
        catch(Exception ex){
            System.debug('^^^' + ex.getMessage());
            errorMsg = 'An unknown exception has occurred while processing the request. ' + ex.getMessage();
            return;
        }
        
        // Request accepted for processing
        if(response != null && response.getStatusCode() == 202){
            
            if(calloutType == 'ChangeDistributionPair') {
                errorMsg = 'Pair Swap request submitted successfully.';
                isPairDisabled = true;
            }
            else if(calloutType == 'NetworkTrail') {
                errorMsg = 'Network Trail request has been submitted successfully.';
                isNetworkTrailDisabled = true;
                
                // Upsert Task Custom Object
                if(taskCustomRecord != null){
                    taskCustomRecord.NetworkTrailCoRelationID__c = strnbnCorrelationId;
                    taskCustomRecord.OPair__c = '';
                    taskCustomRecord.MPair__c = '';
                }
                else{
                    taskCustomRecord = new Task_Custom__c(OPair__c = '', MPair__c = '', NetworkTrailCoRelationID__c = strnbnCorrelationId);
                }
                
                upsert taskCustomRecord;
                system.debug('^^^taskCustomRecord' + taskCustomRecord );
                taskRecord.Task_Custom__c = taskCustomRecord.Id;
            }
            
            // Update Task Record
            taskRecord.NBNCorrelationId__c = strnbnCorrelationId;
            
            update taskRecord;
            isPolarEnabled = true;
            return;
        }
        
        if(response != null){
            errorMsg = 'ERROR in post request to PNI: ' + response.getStatusCode() + ' ' + response.getStatus();
        }
        else{
            errorMsg = 'An unknown exception has occurred while processing the request.';
        }
    }
    

}