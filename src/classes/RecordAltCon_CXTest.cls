@isTest
private class RecordAltCon_CXTest {
    
    static String strBodyValidatePRI = '{"data":[{"id":"PRI003000710422","type":"csllProductInstance","attributes":{"copperPathId":"CPI300006462521"},"relationships":{"address":{"data":{"id":"LOC000080833411","type":"location"},"links":{"related":"https://cis-api-sit3.slb.nbndc.local/rest/places/v1/locations/LOC000080833411"}}}}],"included":[{"type":"location","id":"LOC000080833411","attributes":{"dwellingType":"SDU","region":"Urban","regionValueDefault":"FALSE","externalTerritory":"FALSE","address":{"unstructured":"LOT 9 10 YURUGA RD DURAL NSW 2158","roadNumber1":"10","roadName":"YURUGA","roadTypeCode":"RD","locality":"DURAL","postCode":"2158","state":"NSW","lotNumber":"9","planNumber":"9//DP735652"},"primaryAccessTechnology":{"technologyType":"Fibre To The Node","serviceClass":"12","rolloutType":"Brownfields"},"landUse":"AGRICULTURAL","geoCode":{"latitude":"-33.695061","longitude":"151.012085","geographicDatum":"GDA94","srid":"4283"}},"relationships":{"containmentBoundaries":{"data":[{"id":"2DUR","type":"NetworkBoundary","boundaryType":"FSA"},{"id":"S2 -Dural-Khurst-Wisemans Fery","type":"NetworkBoundary","boundaryType":"SSA"},{"id":"2CAS","type":"NetworkBoundary","boundaryType":"AAR"},{"id":"2DUR-20-13","type":"NetworkBoundary","boundaryType":"ADA"},{"id":"2DUR-20","type":"NetworkBoundary","boundaryType":"SAM"}]}}}]}';
            
    
    static String strBodyInventory = '{"transactionId":"aeb8a1007f60271d","metadata":{"nbnCorrelationId":"0e8cedd0-ad98-11e6-bf2e-47644ada7c0f","tokenId":"3c08d723a26f1234","copperPathId":"CPI300000403245","changeReason":"DATA-CORRECTION","requestedPair":"P:12"},"copperPathId":"CPI300000403245","status":"Success","pairConnected":"abcde:123"}';

    static String strBodyPairSwap = '{"transactionId":"aeb8a1007f60271d","metadata":{"nbnCorrelationId":"0e8cedd0-ad98-11e6-bf2e-47644ada7c0f","tokenId":"3c08d723a26f1234","copperPathId":"CPI300000403245","changeReason":"DATA-CORRECTION","requestedPair":"P:12"},"copperPathId":"CPI300000403245","status":"Success","pairConnected":"abcde:123"}';

    // Test Data 
    @testSetup static void setup() {
        // create custom setting records for Alternate Continuity.
        List<SObject> lstToInsert = new List<SObject>();
        
        NBNIntegrationInputs__c objCS1 = new NBNIntegrationInputs__c();        
        objCS1.Name = 'ChangeDistributionPair';
        objCS1.AccessSeekerId__c = 'testId';
        objCS1.Call_TimeOut_In_MilliSeconds__c = '60000';
        objCS1.EndpointURL__c='/v1/test';
        objCS1.NBNBreadcrumbId__c = 'test';
        objCS1.NBN_Environment_Override__c='SIT1';
        objCS1.nbn_Role__c='NBN';
        objCS1.Service_Name__c='test serviec';
        objCS1.Token__c='8e73b7e33b22ad58';
        objCS1.XNBNBusinessChannel__c='Salesforce';
        objCS1.UI_TimeOut__c = 60;
        objCS1.NBN_Environment_Override__c='TEST';
        lstToInsert.add(objCS1);
        
        
        NBNIntegrationInputs__c objCS2 = new NBNIntegrationInputs__c();        
        objCS2.Name = 'NetworkTrail';
        objCS2.AccessSeekerId__c = 'testId';
        objCS2.Call_TimeOut_In_MilliSeconds__c = '60000';
        objCS2.EndpointURL__c='/v1/test';
        objCS2.NBNBreadcrumbId__c = 'test';
        objCS2.NBN_Environment_Override__c='SIT1';
        objCS2.nbn_Role__c='NBN';
        objCS2.Service_Name__c='test serviec';
        objCS2.Token__c='8e73b7e33b22ad58';
        objCS2.XNBNBusinessChannel__c='Salesforce';
        objCS2.UI_TimeOut__c = 60;
        objCS2.NBN_Environment_Override__c='TEST';
        lstToInsert.add(objCS2);
        
        NBNIntegrationInputs__c objCS3 = new NBNIntegrationInputs__c();        
        objCS3.Name = 'CISProductsAPI';
        objCS3.AccessSeekerId__c = 'testId';
        objCS3.Call_TimeOut_In_MilliSeconds__c = '60000';
        objCS3.EndpointURL__c='/v1/test';
        objCS3.NBNBreadcrumbId__c = 'test';
        objCS3.NBN_Environment_Override__c='SIT1';
        objCS3.nbn_Role__c='NBN';
        objCS3.Service_Name__c='test serviec';
        objCS3.Token__c='8e73b7e33b22ad58';
        objCS3.XNBNBusinessChannel__c='Salesforce';
        objCS3.UI_TimeOut__c = 60;
        objCS3.NBN_Environment_Override__c='TEST';
        lstToInsert.add(objCS3);
        
        
        Pri_Format__C format1 = new Pri_Format__C();
        format1.Name = 'TASK_PRI_FORMAT_ERROR';
        format1.ErrorDescription__c = 'please provide valide pri format value';
        lstToInsert.add(format1);
        
        
        Pri_Format__C format2 = new Pri_Format__C();
        format2.Name = 'TASK_PRI_NOT_FOUND_ERROR';
        format2.ErrorDescription__c = 'please provide valide pri format value';
        lstToInsert.add(format2);
        
        
        Account objAccount = new Account();
        objAccount.name = 'nbn Service Assurance';
        objAccount.RecordTypeId = GlobalCache.getRecordTypeId('Account','Company');
        lstToInsert.add(objAccount);
        
        Task objTask = new Task();
        objTask.RecordTypeId = GlobalCache.getRecordTypeId('Task','GCC Interaction');
        objTask.PRI__c = 'PRI003000710422';
        objTask.Reason_for_Call__c = 'Pair Swap';
        objTask.Additional_Comments__c = 'Some Description';
        objTask.Additional_Info__c = 'Some Description';
        objTask.Any_follow_up_action_advised__c = 'Some Description';
        objTask.Bypassed_DPU_port_number__c = '22';
        objTask.Cable_Record_Enquiry_Details__c = 'Some Description';
        objTask.CIU_DPU_ID__c = 'Some Description';
        objTask.C_Pair_Destination__c = '22';
        objTask.C_Pair_Source__c = '22';
        objTask.CPI_ID__c = '';
        objTask.Details_of_temp_fix_provided__c = 'Yes';
        objTask.Has_the_hazard_been_made_safe__c = 'Yes';
        objTask.Hazard_Details__c = 'Some Description';
        objTask.Inventory_Update__c ='Some Description';
        objTask.Lead_in_Pair__c = 'Some Description';
        objTask.M_Pair_Destination__c = '22';
        objTask.M_Pair_Source__c = '22';
        objTask.NAR_SR_Created__c = 'Yes';
        objTask.NAR_SR_number__c = '22';
        objTask.O_Pair_Destination__c = '22';
        objTask.O_Pair_Source__c = '22';
        objTask.Pillar_ID_Destination__c = '22';
        objTask.Pillar_ID_Source__c = '22';
        objTask.Reason_for_CIU_By_pass__c = 'Some Description';
        objTask.Severity_of_the_Hazard__c = 'Some Description';
        objTask.Site_Contact__c = 'Some Description';
        objTask.Source_of_Hazard__c = 'Some Description';
        objTask.Telstra_Reference_number__c = 'Some Description';
        objTask.Telstra_Tech_Contact_number__c = '0400000000';
        objTask.Telstra_Tech_ID__c = 'Some Description';
        objTask.Telstra_Tech_name__c = 'Some Description';
        objTask.X_Pair_Destination__c = '22';
        objTask.X_Pair_Source__c = '22';
        objTask.NBNCorrelationId__c= '0e8cedd0-ad98-11e6-bf2e-47644ada7c0f'; 
        objTask.Network_Inventory_Swap_Status__c=Label.Success; 
        lstToInsert.add(objTask);
        
        insert lstToInsert;
    }
    
    // without passing Task Id
    @isTest static void testMethod1() {
        Test.startTest();
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(new Task()); 
        RecordAltCon_CX  ext = new RecordAltCon_CX(stdController);
        ext.recordInteraction();
        
        ext.taskRecord.Reason_for_Call__c = 'Pair Swap';
        ext.taskRecord.PRI__c = 'PRI003000710422';
        ext.taskRecord.Telstra_Reference_number__c = 'test';
        ext.taskRecord.Telstra_Tech_name__c = 'test';
        ext.taskRecord.Telstra_Tech_ID__c = 'test';
        ext.taskRecord.Telstra_Tech_Contact_number__c = '0458900000';
        ext.taskRecord.Additional_Comments__c = 'test';
        ext.taskRecord.Any_follow_up_action_advised__c = 'test';
        ext.taskRecord.Type_of_Line_Swap__c = 'Distribution Pair';
        ext.taskRecord.NBNCorrelationId__c = '0e8cedd0-ad98-11e6-bf2e-47644ada7c0f';
        ext.actionReasonForCall();
        
        Test.stopTest();
        
    }
    
    
    
    
    // validate PRI
    @isTest static void testMethod_validatePRI() {
        Test.startTest();
        // Get the first test account by using a SOQL query
        ApexPages.StandardController stdController = new ApexPages.StandardController(new Task()); 
        RecordAltCon_CX  ext = new RecordAltCon_CX(stdController);
        ext.recordInteraction();
        
        ext.taskRecord.Reason_for_Call__c = 'Pair Swap';
        ext.taskRecord.PRI__c = 'PRI003000710422';
        ext.taskRecord.Telstra_Reference_number__c = 'test';
        ext.taskRecord.Telstra_Tech_name__c = 'test';
        ext.taskRecord.Telstra_Tech_ID__c = 'test';
        ext.taskRecord.Telstra_Tech_Contact_number__c = '0458900000';
        ext.taskRecord.Additional_Comments__c = 'test';
        ext.taskRecord.Any_follow_up_action_advised__c = 'test';
        ext.taskRecord.Type_of_Line_Swap__c = 'Distribution Pair';
        ext.taskRecord.NBNCorrelationId__c = '0e8cedd0-ad98-11e6-bf2e-47644ada7c0f';
        
        // validate PRI mock resonse
        MockHttpResponseGenerator objMock = new MockHttpResponseGenerator();
        objMock.dummyResponse = strBodyValidatePRI;
        objMock.dummyStatusCode = 200;
        Test.setMock(HttpCalloutMock.class, objMock);
        ext.validatePRI();
        
        Test.stopTest();
        
    }
    
    
    // invalid PRI Number
    @isTest static void testMethod_invalidPRINumber() {
        Test.startTest();
        // Get the first test account by using a SOQL query
        ApexPages.StandardController stdController = new ApexPages.StandardController(new Task()); 
        RecordAltCon_CX  ext = new RecordAltCon_CX(stdController);

        ext.taskRecord.Reason_for_Call__c = 'Pair Swap';
        ext.taskRecord.PRI__c = 'sdfsdf';
        ext.taskRecord.Telstra_Reference_number__c = 'test';
        ext.taskRecord.Telstra_Tech_name__c = 'test';
        ext.taskRecord.Telstra_Tech_ID__c = 'test';
        ext.taskRecord.Telstra_Tech_Contact_number__c = 'dfdsf';
        ext.taskRecord.Additional_Comments__c = 'test';
        ext.taskRecord.Any_follow_up_action_advised__c = 'test';
        ext.taskRecord.Type_of_Line_Swap__c = 'Distribution Pair';
        ext.taskRecord.NBNCorrelationId__c = '0e8cedd0-ad98-11e6-bf2e-47644ada7c0f';
        
        // validate PRI mock resonse
        MockHttpResponseGenerator objMock = new MockHttpResponseGenerator();
        objMock.dummyResponse = strBodyValidatePRI;
        objMock.dummyStatusCode = 200;
        Test.setMock(HttpCalloutMock.class, objMock);
        ext.validatePRI();
        
        Test.stopTest();
        
    }
    
    
    
    
    // Pair Swap
    @isTest static void testMethod_Pair_Swap() {
        Test.startTest();
        
        List<String> lstFields = new List<String>(Task.getSobjectType().getDescribe().fields.getMap().keySet());
        Task objTask = Database.query('SELECT ' + String.join(lstFields, ',') + ' FROM Task LIMIT 1');
        
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objTask); 
        RecordAltCon_CX  ext = new RecordAltCon_CX(stdController);
        ext.recordInteraction();
        
        ext.taskRecord.Reason_for_Call__c = 'Pair Swap';
        ext.taskRecord.PRI__c = 'PRI003000710422';
        ext.taskRecord.Telstra_Reference_number__c = 'test';
        ext.taskRecord.Telstra_Tech_name__c = 'test';
        ext.taskRecord.Telstra_Tech_ID__c = 'test';
        ext.taskRecord.Telstra_Tech_Contact_number__c = '0458900000';
        ext.taskRecord.Additional_Comments__c = 'test';
        ext.taskRecord.Any_follow_up_action_advised__c = 'test';
        ext.taskRecord.Type_of_Line_Swap__c = 'Distribution Pair';
        ext.taskRecord.NBNCorrelationId__c = '0e8cedd0-ad98-11e6-bf2e-47644ada7c0f';
        
        
        MockHttpResponseGenerator objMock = new MockHttpResponseGenerator();
        objMock.dummyResponse = strBodyPairSwap;
        objMock.dummyStatusCode = 202;
        Test.setMock(HttpCalloutMock.class, objMock);
        
        
        ext.taskRecord.Reason_for_Call__c = 'Pair Swap';
        ext.taskRecord.Type_of_Line_Swap__c = 'Distribution Pair';
        ext.actionPairSwap();
        
        // Below values should update by Platform event.
        objTask.NetworkTrailCoRelationID__c = 'test';
        objTask.TCO_or_MDF__c = 'test';
        objTask.Copper_Lead_In_Cable__c = 'test';
        objTask.Copper_Lead_In_Cable_Pair__c = 'test';
        objTask.DPU_Serial_Number__c = 'test';
        objTask.CIU_DPU_ID__c = 'test';
        objTask.DPU_Port_Number__c = 'test';
        objTask.CIU_Cut_In_Status__c = 'test';
        objTask.Copper_Distribution_Cable__c = 'test';
        objTask.O_Pair__c = 'test';
        objTask.Copper_Main_Cable__c = 'test';
        objTask.M_pair__c = 'test';
        objTask.FTTN_Node__c = 'test';
        objTask.Type_Of_Node__c = 'test';
        objTask.FTTN_Node__c = 'test';
        objTask.Error_trial__c = 'test';
        objTask.NetworkTrail_Status__c = 'Completed';
        objTask.Network_Inventory_Swap_Status__c = 'Completed'; 
        update objTask;
        
        ext.actionPolar();
        Test.stopTest();
    }
    
    
    // By passing Task Id Cable Record Enquiry
    @isTest static void testMethod_cable_record() {
        Test.startTest();
        
        List<String> fields = new List<String>(Task.getSobjectType().getDescribe().fields.getMap().keySet());
        Task objTask = Database.query('SELECT ' + String.join(fields, ',') + ' FROM Task LIMIT 1');
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objTask); 
        RecordAltCon_CX  ext = new RecordAltCon_CX(stdController);
        ext.recordInteraction();
        
        ext.taskRecord.Reason_for_Call__c = 'Cable Record Enquiry';
        ext.taskRecord.PRI__c = 'PRI003000710422';
        ext.taskRecord.Telstra_Reference_number__c = 'test';
        ext.taskRecord.Telstra_Tech_name__c = 'test';
        ext.taskRecord.Telstra_Tech_ID__c = 'test';
        ext.taskRecord.Telstra_Tech_Contact_number__c = '0458900000';
        ext.taskRecord.Additional_Comments__c = 'test';
        ext.taskRecord.Any_follow_up_action_advised__c = 'test';
        ext.taskRecord.Reason_for_Call__c = 'Cable Record Enquiry';
        ext.taskRecord.NBNCorrelationId__c = '0e8cedd0-ad98-11e6-bf2e-47644ada7c0f';
        
        
        
        
        
        
        MockHttpResponseGenerator objMock = new MockHttpResponseGenerator();
        objMock.dummyResponse = strBodyInventory;
        objMock.dummyStatusCode = 202;
        Test.setMock(HttpCalloutMock.class, objMock);
        ext.actionNetworkTrail();
        
        
        // Below values should update by Platform event.
        objTask.NetworkTrailCoRelationID__c = 'test';
        objTask.TCO_or_MDF__c = 'test';
        objTask.Copper_Lead_In_Cable__c = 'test';
        objTask.Copper_Lead_In_Cable_Pair__c = 'test';
        objTask.DPU_Serial_Number__c = 'test';
        objTask.CIU_DPU_ID__c = 'test';
        objTask.DPU_Port_Number__c = 'test';
        objTask.CIU_Cut_In_Status__c = 'test';
        objTask.Copper_Distribution_Cable__c = 'test';
        objTask.O_Pair__c = 'test';
        objTask.Copper_Main_Cable__c = 'test';
        objTask.M_pair__c = 'test';
        objTask.FTTN_Node__c = 'test';
        objTask.Type_Of_Node__c = 'test';
        objTask.FTTN_Node__c = 'test';
        objTask.Error_trial__c = 'test';
        objTask.NetworkTrail_Status__c = 'Completed';
        objTask.Network_Inventory_Swap_Status__c = 'Completed'; 
        update objTask;
        
        
        ext.actionPolar();
        ext.saveTask();
        ext.saveTaskandComplete();
        ext.cancelTask();
        Test.stopTest();
        
    }
    
    class MockHttpResponseGenerator implements HttpCalloutMock {
        public String dummyResponse;
        public Integer dummyStatusCode;
        
        // Implement this interface method
        public HTTPResponse respond(HTTPRequest req) {
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(dummyResponse);
            res.setStatusCode(dummyStatusCode);
            return res;
        }
    }
}