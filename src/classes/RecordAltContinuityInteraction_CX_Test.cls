/***************************************************************************************************
Class Name:         RecordAltContinuityInteraction_CX_Test 
Class Type:         Extension Class 
Version:            1.0 
Created Date:       13 Sept 2017
Function:           This test class is to cover RecordAlternateContinuityInteraction_CX class that drives dynamic page elements of an Alternate Continuity interaction record based on the Task object as well as integration with NBNCo on premise system CIS
Input Parameters:   None 
Output Parameters:  None
Description:        Used in RecordAlternateContinuityInteraction Visualforce page. Test class name is shortened due to name limits
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Narasimha Binkam      13/10/2017      Created - Version 1.0 Refer CUSTSA-5361 for Epic description
****************************************************************************************************/ 

@isTest
public class RecordAltContinuityInteraction_CX_Test{   
    
    public static testMethod void validate(){     
        Pri_Format__C customObj = TestDataUtility.createCustomSettingForPriFormatError();
        Pri_Format__C priCustomObj = TestDataUtility.createCustomSettingForPriEmptyError();  
        Pri_Format__C priCstObj = TestDataUtility.createCustomSettingForPriNotFnd();        
        Id recTypeID = GlobalCache.getRecordTypeId('Account','Company');       
        Account accObj = TestDataUtility.createAccount();
        Task taskObj = TestDataUtility.createTestTaskValidPRI();          
        ApexPages.StandardController controller = new ApexPages.StandardController(taskObj); 
        RecordAlternateContinuityInteraction_CX  racI = new RecordAlternateContinuityInteraction_CX(controller);
        racI.recordInteraction();
        racI.saveTask();  
        test.startTest(); 
        NBNIntegrationInputs__c FTTN = TestDataUtility.createCustSetForNBNIntegrationInputs('FTTN');
        NBNIntegrationInputs__c ChangeDistributionPair = TestDataUtility.createCustSetForNBNIntegrationInputs('ChangeDistributionPair');
            Task taskObj2 = TestDataUtility.createTestTaskValidPRI();        
            ApexPages.StandardController controller2 = new ApexPages.StandardController(taskObj2); 
            RecordAlternateContinuityInteraction_CX  racI2 = new RecordAlternateContinuityInteraction_CX(controller2);
            racI2.validatePRIFormat();
            IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();
            // Verify the  call out CISPRI
            mockResp.strResponseType = 'CISPRI';
            mockResp.status= Label.success;        
            String strPRI = 'PRI003000710422';
            Test.setMock(HttpCalloutMock.class, mockResp);
            racI2.validatePRI();           
            racI2.manageConditionalMPairRequired();
            racI2.manageConditionalOPairRequired();
            racI2.updateTask();
            racI2.cancelUpdate();
             try{
           racI2.getNetTrailStat();    
           }catch(exception e){}  
        try{
           racI2.postNetworkTrail();
              }catch(exception e){} 
              
              try{
                 racI2.getSwapStatus();
              }catch(exception e){}
           // racI2.getLocationAndAddressForPRI(strPRI);       
        test.stopTest();
        
        Task taskObjinv = TestDataUtility.createTestTaskInvalidFormatPRI();  
        ApexPages.StandardController controller3 = new ApexPages.StandardController(taskObjinv); 
        RecordAlternateContinuityInteraction_CX  racI3 = new RecordAlternateContinuityInteraction_CX (controller3);
        racI3.validatePRIFormat();
        racI3.validatePRI();
        racI3.updateTask();  
        
        Task taskObjinv1 = TestDataUtility.createTestTaskInvalidFormatPRI(); 
        ApexPages.StandardController controller4 = new ApexPages.StandardController(taskObjinv1); 
        RecordAlternateContinuityInteraction_CX  racI33 = new RecordAlternateContinuityInteraction_CX (controller4);
        racI33.updateTask();
        
        Task taskObjblank = TestDataUtility.createTestTask();    
        ApexPages.StandardController controller44 = new ApexPages.StandardController(taskObjblank); 
        RecordAlternateContinuityInteraction_CX  racI4 = new RecordAlternateContinuityInteraction_CX (controller44);
        racI4.validatePRIFormat();
        
        Task taskObjnonExist = TestDataUtility.createTestTaskNonExistPRI();   
        ApexPages.StandardController controller5 = new ApexPages.StandardController(taskObjnonExist); 
        RecordAlternateContinuityInteraction_CX  racI5 = new RecordAlternateContinuityInteraction_CX(controller5);
    }
    public static testMethod void IntegrationMethods(){
         test.startTest();  
        
            IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();
            mockResp.strResponseType = 'PNI';
            mockResp.status= Label.success;        
            String token='3c08d723a26f1234';
            String copperPathId='CPI300000403245';
            String changeReason='FAULT|DATA-CORRECTION|OTHER';
            String desiredPair='P12:345';
            String url='/v1/changeDistributionPair';
            String role='NBN';
            String requestor='Salesforce';
            String nbnCorrelationId='9dc761c5-930d-461c-8be3-63c1c018d947';
            String serviceId='serv';
            string requestorId='requestorId';
            String callOutURL='callout:NBNAPPGATEWAYSIT';
            String strPRI = 'PRI003000710422';
            
            Task taskObj22 = TestDataUtility.createTestTaskValidPRI();       
            ApexPages.StandardController controller22 = new ApexPages.StandardController(taskObj22); 
            RecordAlternateContinuityInteraction_CX  racI2 = new RecordAlternateContinuityInteraction_CX(controller22);
            racI2.validatePRIFormat();
           
            // Verify the  call out CISPRI
            mockResp.strResponseType = 'CISPRI';
            mockResp.status= Label.success;        
            
            Test.setMock(HttpCalloutMock.class, mockResp);          
            racI2.callnbnMicroservice('','','');           
            
       
        test.stopTest(); 
    }
     
    public static testMethod void TestCancelTask(){ 
        test.startTest();  
        Task taskObj = TestDataUtility.createTestTaskValidPRI();         
            ApexPages.StandardController controller = new ApexPages.StandardController(taskObj); 
            RecordAlternateContinuityInteraction_CX  racI = new RecordAlternateContinuityInteraction_CX(controller);
            racI.cancelTask();      
        test.stopTest();  
    }
    
    public static testMethod void ValidateData(){ 
        test.startTest();  
        Task taskObj = TestDataUtility.createTestTaskInvalidData();         
            ApexPages.StandardController controller = new ApexPages.StandardController(taskObj); 
            RecordAlternateContinuityInteraction_CX  racI = new RecordAlternateContinuityInteraction_CX(controller);
            racI.saveTask();      
        test.stopTest();  
    }
    
    public static testMethod void TestClearPRI(){
           
        Id recTypeID = GlobalCache.getRecordTypeId('Account','Company');
        List<Account> accountList = new List<Account>();        
        Account accObj = TestDataUtility.createAccount();       
        Task taskObjinv = TestDataUtility.createTestTaskInvalidFormatPRI();   
        test.startTest();  
            ApexPages.StandardController controller2 = new ApexPages.StandardController(taskObjinv); 
            RecordAlternateContinuityInteraction_CX  racI2 = new RecordAlternateContinuityInteraction_CX(controller2);
            racI2.clearPRI();
         test.stopTest();  
     }
     
    public static testMethod void TestSaveTaskandComplete(){     
        Pri_Format__C customObj = TestDataUtility.createCustomSettingForPriFormatError();
        Pri_Format__C priCustomObj = TestDataUtility.createCustomSettingForPriEmptyError();  
        Pri_Format__C priCstObj = TestDataUtility.createCustomSettingForPriNotFnd();
        Id recTypeID = GlobalCache.getRecordTypeId('Account','Company');
        List<Account> accountList = new List<Account>();        
        Account accObj = TestDataUtility.createAccount();
        Task taskObj2 = TestDataUtility.createTestTaskValidPRI();         
        test.startTest();          
            ApexPages.StandardController controller2 = new ApexPages.StandardController(taskObj2); 
            RecordAlternateContinuityInteraction_CX  racI2 = new RecordAlternateContinuityInteraction_CX(controller2);
            racI2.saveTaskandComplete();       
        test.stopTest();  
   }
   
   
   public static testMethod void TestCloseTask(){
        Id recTypeID = GlobalCache.getRecordTypeId('Account','Company');       
        Account accObj = TestDataUtility.createAccount();        
        Task taskObj2 = TestDataUtility.createTestTaskValidPRI(); 
        test.startTest();         
            ApexPages.StandardController controller2 = new ApexPages.StandardController(taskObj2); 
            RecordAlternateContinuityInteraction_CX  racI2 = new RecordAlternateContinuityInteraction_CX(controller2);
            racI2.closeTask();       
        test.stopTest();  
     }
     
     
   
   public static testMethod void TestCopyPillarValue(){     
        Id recTypeID = GlobalCache.getRecordTypeId('Account','Company');
        Account accObj = TestDataUtility.createAccount();
        Task taskObj2 = TestDataUtility.createTestTaskValidPRI();         
        test.startTest();  
            ApexPages.StandardController controller2 = new ApexPages.StandardController(taskObj2); 
            RecordAlternateContinuityInteraction_CX  racI2 = new RecordAlternateContinuityInteraction_CX(controller2);           
        test.stopTest();  
     }
 
    public static testMethod void validateCISSendRequest(){     
        Pri_Format__C customObj = TestDataUtility.createCustomSettingForPriFormatError();
        Pri_Format__C priCustomObj = TestDataUtility.createCustomSettingForPriEmptyError(); 
        Pri_Format__C priCstObj = TestDataUtility.createCustomSettingForPriNotFnd();        
        Test.startTest(); 
            Task taskObjinv = TestDataUtility.createTestTaskInvalidFormatPRI();        
            ApexPages.StandardController controller2 = new ApexPages.StandardController(taskObjinv); 
            RecordAlternateContinuityInteraction_CX  racI2 = new RecordAlternateContinuityInteraction_CX (controller2);
            //raci2.saveTaskandComplete();
            raci2.cancelUpdate();       
            String productInstanceJSONValidResponse = testDataUtility.cisProductInstanceFakeValidJSONResponse();
            String pri = 'PRI003000710422';
            HttpResponse response = new HttpResponse();
            List<IntegrationWrapper.priWrapper> PRIListFromExternalSystemByPRI = new List<IntegrationWrapper.priWrapper>();
            response.setBody(productInstanceJSONValidResponse);
            response.setStatusCode(200);
            RecordAlternateContinuityInteraction_CX.validatePRIUsingExtSystemByPRI(pri,response,PRIListFromExternalSystemByPRI);         
        Test.stopTest();
    }
    
    public static testMethod void validatePairFormat(){           
        Id recTypeID = GlobalCache.getRecordTypeId('Account','Company');
        List<Account> accountList = new List<Account>();        
        Account accObj = TestDataUtility.createAccount();       
        Task taskObjinv = TestDataUtility.createTestTaskInvalidDesiredFormat();   
        test.startTest();        
            ApexPages.StandardController controller2 = new ApexPages.StandardController(taskObjinv); 
            RecordAlternateContinuityInteraction_CX  racI2 = new RecordAlternateContinuityInteraction_CX(controller2);
            racI2.validatePairFormat();       
         test.stopTest();  
     }
     
       public static testMethod void postPairSwap(){           
        Id recTypeID = GlobalCache.getRecordTypeId('Account','Company');
        List<Account> accountList = new List<Account>();        
        Account accObj = TestDataUtility.createAccount();       
        Task taskObjinv = TestDataUtility.createTestTaskDesiredFormat();   
        test.startTest();        
            ApexPages.StandardController controller2 = new ApexPages.StandardController(taskObjinv); 
            RecordAlternateContinuityInteraction_CX  racI2 = new RecordAlternateContinuityInteraction_CX(controller2);
            racI2.boolPairFormatOK =true;
            system.debug('racI2.boolPairFormatOK@@'+racI2.boolPairFormatOK);
            racI2.postPairSwap();       
         test.stopTest();  
     }
    
    
        public static testMethod void SwapFail(){           
        Id recTypeID = GlobalCache.getRecordTypeId('Account','Company');
        List<Account> accountList = new List<Account>();        
        Account accObj = TestDataUtility.createAccount(); 
        String message= testDataUtility.mockPNiSwapPairSuccessCall();
        Task taskObjinv = TestDataUtility.createTestTaskDesiredFormat();   
        NBNIntegrationInputs__c FTTN = TestDataUtility.createCustSetForNBNIntegrationInputs('FTTN');
        NBNIntegrationInputs__c ChangeDistributionPair = TestDataUtility.createCustSetForNBNIntegrationInputs('ChangeDistributionPair');        
        Task taskObj2 = TestDataUtility.createTaskInvalidSwap();        
        ApexPages.StandardController controller2 = new ApexPages.StandardController(taskObj2); 
        RecordAlternateContinuityInteraction_CX  racI2 = new RecordAlternateContinuityInteraction_CX(controller2);
        racI2.validatePRIFormat();
        IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();
        // Verify the  call out CISPRI
        mockResp.strResponseType = 'CISPRI';
        mockResp.status= Label.success;        
        String strPRI = 'PRI003000710422';
        Test.setMock(HttpCalloutMock.class, mockResp);
        racI2.validatePRI(); 
        try{
        racI2.getNetTrailStat();    
        }catch(exception e){}  
        try{
         racI2.postNetworkTrail();
          }catch(exception e){} 
         try{
             racI2.getSwapStatus();
          }catch(exception e){}
       // racI2.getLocationAndAddressForPRI(strPRI);         
     }
       
       public static testMethod void NetworkTrail(){           
        Id recTypeID = GlobalCache.getRecordTypeId('Account','Company');
        List<Account> accountList = new List<Account>();        
        Account accObj = TestDataUtility.createAccount();         
        Task taskObjinv = TestDataUtility.createTaskpostNetworkTrail();   
        NBNIntegrationInputs__c FTTN = TestDataUtility.createCustSetForNBNIntegrationInputs('FTTN');
        NBNIntegrationInputs__c ChangeDistributionPair = TestDataUtility.createCustSetForNBNIntegrationInputs('ChangeDistributionPair');        
        Task taskObj2 = TestDataUtility.createTaskInvalidSwap();        
        ApexPages.StandardController controller2 = new ApexPages.StandardController(taskObj2); 
        RecordAlternateContinuityInteraction_CX  racI2 = new RecordAlternateContinuityInteraction_CX(controller2);
        racI2.validatePRIFormat();
        IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();
        // Verify the  call out CISPRI
        mockResp.strResponseType = 'CISPRI';
        mockResp.status= Label.success;        
        String strPRI = 'PRI003000710422';
        Test.setMock(HttpCalloutMock.class, mockResp);
        racI2.validatePRI();          
        try{
        racI2.strCopperPairId='test';
        racI2.strReasonForCall='Cable Record Enquiry';
        racI2.postNetworkTrail();
        }catch(exception e){} 
             
     }
     
     public static testMethod void getNetTrailStat(){           
        Id recTypeID = GlobalCache.getRecordTypeId('Account','Company');
        List<Account> accountList = new List<Account>();        
        Account accObj = TestDataUtility.createAccount();         
        Task taskObjinv = TestDataUtility.NetTrailStatusSucc();   
        NBNIntegrationInputs__c FTTN = TestDataUtility.createCustSetForNBNIntegrationInputs('FTTN');
        NBNIntegrationInputs__c ChangeDistributionPair = TestDataUtility.createCustSetForNBNIntegrationInputs('ChangeDistributionPair');        
        Task taskObj2 = TestDataUtility.createTaskInvalidSwap();        
        ApexPages.StandardController controller2 = new ApexPages.StandardController(taskObj2); 
        RecordAlternateContinuityInteraction_CX  racI2 = new RecordAlternateContinuityInteraction_CX(controller2);
        racI2.validatePRIFormat();
        IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();
        // Verify the  call out CISPRI
        mockResp.strResponseType = 'CISPRI';
        mockResp.status= Label.success;        
        String strPRI = 'PRI003000710422';
        Test.setMock(HttpCalloutMock.class, mockResp);
        racI2.validatePRI();          
        try{        
        racI2.getNetTrailStat();
        }catch(exception e){} 
             
     }
    
 
 }