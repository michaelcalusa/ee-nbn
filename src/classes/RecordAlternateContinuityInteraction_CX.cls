/***************************************************************************************************
Class Name:         RecordAlternateContinuityInteraction_CX
Class Type:         Extension Class 
Version:            1.0 
Created Date:       13 Sept 2017
Function:           This class drives dynamic page elements of an Alternate Continuity interaction record based on the Task object as well as integration with NBNCo on premise system CIS
Input Parameters:   None 
Output Parameters:  None
Description:        Used in RecordAlternateContinuityInteraction Visualforce page
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Beau Anderson     13/9/2017      Created - Version 1.0 Refer CUSTSA-5361 for Epic description
* Narashimha Binkam 30/10/2017      Added code for integration and bug fixing
* Sanghita Das      15/12/2017      Refer CUSTSA-8359 for Epic description
****************************************************************************************************/ 

public with sharing class RecordAlternateContinuityInteraction_CX { 
   
    public Task taskRecord {get; set;}
    public Task_Custom__c taskRecord_cust {get; set;}
    public String strReasonForCall {get; set;}
    public String strTaskId {get; set;}
    public String strPRI {get; set;}
    public String strPillarSource {get; set;}
    public String strThankYou {get; set;}
    public String strByPassedPort {get; set;}
    public String strTelstraTechPhone {get; set;}
    public String strRequiredError {get; set;}
    public String strDesiredPairId {get; set;}
    public String strnbnCorrelationId {get; set;}
    public String strReasonForChange {get; set;}
    public String strCopperPairId {get; set;}
    public String strSpecialCharacterCheck1 {get; set;}
    public String strSpecialCharacterCheck2 {get; set;}
    public String strSpecialCharacterCheck3 {get; set;}
    public String strTrial{get; private set;}   
    public Boolean boolRecordInteraction {get; set;}
    public Boolean boolReadOnly {get; set;}
    public Boolean boolCancel {get; set;}
    public Boolean boolClose {get; set;}
    public Boolean boolUpdate {get; set;}
    public Boolean boolRefNumValidationPassed {get; set;}
    public Boolean boolTechIdValidationPassed {get; set;}
    public Boolean boolPoValidationPassed {get; set;}
    public Boolean boolPhValidationPassed {get; set;}
    public Boolean boolChValidationPassed {get; set;}
    public Boolean boolValidationsPassed {get; set;}
    public Boolean boolRecordSaved {get; set;}
    public Boolean boolMPairPopulated {get; set;}
    public Boolean boolOPairPopulated {get; set;}
    public Boolean boolPillarPopulated {get; set;}   
    public Boolean boolPNIstatId{get; set;} 
    public Boolean boolPNIstatErr{get; set;}
    public Boolean boolCometDSub{get; set;}
    public Boolean boolPairFormatOK{get; set;}
    public Boolean boolNetworkTrail{get; set;}  
    public Boolean boolNetTrail{get; set;} 
    public Boolean booltimeOut{get; set;} 
    public Boolean boolTrailTimeOut{get; set;}
    public Boolean boolDisableSwap{get; set;}
    public Boolean boolDisableTrail{get; set;}  
    public Boolean boolTrialSucc{get; set;} 
    public Boolean boolTrialErr{get; set;} 
    public Boolean boolNTstatId{get; set;} 
    public ApexPages.StandardController sController;
    public static List<Account> lstAcc = new List<Account>();
    public static Set<Id> whatIdSet = new Set<Id>();
    public static Set<String> statusSet = new Set<String>();     
    public Boolean throwError {get; set;} 
    public Integer count = 0;
    public Integer networkTrailTimeOut = 0;

    public RecordAlternateContinuityInteraction_CX(ApexPages.StandardController sController){
        taskRecord = new Task();        
        this.sController = sController;            
        this.taskRecord = (Task)sController.getRecord();  
        strReasonForCall = '';
        boolRecordSaved = false;
        boolRecordInteraction = false;
        boolReadOnly = false;
        boolUpdate = false;
        boolPairFormatOK=false;
        throwError=false;
        strTaskId = '';
        strPRI = '';
        boolClose = true;
        strDesiredPairId='';
        boolValidationsPassed=false; 
        boolChValidationPassed=false;
        boolPoValidationPassed=false;
        boolPhValidationPassed=false;
        boolRefNumValidationPassed=false;
        boolTechIdValidationPassed=false;
        boolNetTrail =false;
        boolDisableSwap=false;
        boolDisableTrail=false;
        boolTrialSucc=false;
        boolTrialErr=false;  
        boolNTstatId=false; 
        strTrial='';
    }   
   

    public void recordInteraction(){
        boolRecordInteraction = true;
        boolCancel = false;
        boolClose = false;
    }
    
    public Matcher checkPRIFormat(){
        Pattern priPattern = Pattern.compile('^PRI[0-9]{12}$');
        Matcher priMatcher = priPattern.matcher(strPRI);
        return priMatcher;
    }
    
    public void validatePRIFormat(){    
        saveTask();
        throwError = false;
        If(strPRI == null || String.isblank(strPRI)){
        Pri_Format__C PRIcustomObj = Pri_Format__C.getInstance('TASK_PRI_EMPTY_ERROR');
        taskRecord.PRI__c.addError(PRIcustomObj.ErrorDescription__c);
        }
    }
    
    public Matcher checkPhoneFormat(){
        Pattern priPattern = Pattern.compile('^04[0-9]{8}$');
        Matcher priPhoneMatcher = priPattern.matcher(strTelstraTechPhone);
        return priPhoneMatcher;
    }
    
    public Matcher checkSpecialCharacter1(){
        Pattern priPattern = Pattern.compile('^[a-z  A-Z]*$');  
        Matcher specialCharacter1 = priPattern.matcher(strSpecialCharacterCheck1);
        return specialCharacter1;
    }
    
     public Matcher checkDesiredFieldFormat(){
        Pattern desPattern = Pattern.compile('([A-Z][0-9]{2}:([0-9]{2}))|([A-Z][0-9]{2}:([0-9]{3}))');
        Matcher desPhoneMatcher = desPattern.matcher(strDesiredPairId );
        return desPhoneMatcher;
    }
    
    public void validatePRI(){    
        try{
            Matcher priMatcher = checkPRIFormat();
            if(!priMatcher.matches()){
                Pri_Format__C PRIcustomObj = Pri_Format__C.getInstance('TASK_PRI_FORMAT_ERROR');
                taskRecord.PRI__c.addError(PRIcustomObj.ErrorDescription__c);
            }
            else{
                getLocationAndAddressForPRI(strPRI);
                update taskRecord;
                strCopperPairId = (String)taskRecord.get(Schema.Task.CPI_ID__c);
                system.debug('@@taskRecord.PRITechType__c'+taskRecord.PRITechName__c);
                system.debug('@@strCopperPairId'+strCopperPairId);
                system.debug('@@strReasonForCall'+strReasonForCall);
                system.debug('@@PRITechName__c'+taskRecord.PRITechName__c);
                if(taskRecord.PRITechType__c=='csllProductInstance' && (taskRecord.PRITechName__c=='Fibre To The Node' || taskRecord.PRITechName__c=='Fibre To The Curb') && strCopperPairId!=null && strReasonForCall=='Cable Record Enquiry'){
                    boolNetTrail =true; 
                }
         }
         }
         catch(Exception ex){
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'RecordAlternateContinuityInteraction_CX','validatePRI',string.valueOf(taskRecord.id),'ref id '+string.valueOf(taskRecord.id)+' is a Task','','',ex,0);
         }
    }
    
    public void clearPRI(){
        taskRecord.Location_ID__c = '';
        taskRecord.Location_Address__c = '';
        taskRecord.CPI_ID__c = '';
        strCopperPairId = '';
        throwError = false;
        saveTask();
    }
    
    public void saveTask(){
        try{     
            Id recTypeID = GlobalCache.getRecordTypeId('Account','Company');
            if(RecordAlternateContinuityInteraction_CX.lstAcc.isEmpty()){
                RecordAlternateContinuityInteraction_CX.lstAcc = [select id,name from Account where RecordTypeId=:recTypeID AND name = 'nbn Service Assurance'];
            }
               
            for(Account acc:RecordAlternateContinuityInteraction_CX.lstAcc){
                if(acc.name == 'nbn Service Assurance'){
                taskRecord.whatid = acc.id;
                }
            } 
            if(!RecordAlternateContinuityInteraction_CX.whatIdSet.contains(taskRecord.whatid)){
                RecordAlternateContinuityInteraction_CX.whatIdSet.add(taskRecord.whatid);
            }
            upsert taskRecord;    
            boolRecordSaved = true;
            strReasonForCall = (String)taskRecord.get(Schema.Task.Reason_for_Call__c); 
            strPRI = (String)taskRecord.get(Schema.Task.PRI__c);
            strPillarSource = (String)taskRecord.get(Schema.Task.Pillar_ID_Source__c);
            strByPassedPort = (String)taskRecord.get(Schema.Task.Bypassed_DPU_port_number__c);
            strTelstraTechPhone = (String)taskRecord.get(Schema.Task.Telstra_Tech_Contact_number__c);
            strSpecialCharacterCheck1 = (String)taskRecord.get(Schema.Task.Telstra_Tech_name__c);
            strSpecialCharacterCheck2 = (String)taskRecord.get(Schema.Task.Telstra_Reference_number__c);
            strSpecialCharacterCheck3 = (String)taskRecord.get(Schema.Task.Telstra_Tech_ID__c);
            //If there is a change in how SF shows page messages or if this solution goes lightning it will be worth moving 
            //the validations to validation rules rather that doing in code. Due to how VF shows error messages i.e not next to the
            //field and also due to how rendering is used. Validations have been done in code
            If((strTelstraTechPhone != null && !string.isblank(strTelstraTechPhone)) || (strSpecialCharacterCheck1 != null && !string.isblank(strSpecialCharacterCheck1)) || (strByPassedPort != null && !string.isblank(strByPassedPort))
             || (strSpecialCharacterCheck2 != null && !string.isblank(strSpecialCharacterCheck2)) || (strSpecialCharacterCheck3 != null && !string.isblank(strSpecialCharacterCheck3))){ 
             
                if(strSpecialCharacterCheck2 != null && !string.isblank(strSpecialCharacterCheck2)){
                    Pattern stringPattern = Pattern.compile('^[a-zA-Z0-9- ]*$');  
                    Matcher specialCharacter2 = stringPattern.matcher(strSpecialCharacterCheck2);
                    if(!specialCharacter2.matches()){
                        taskRecord.Telstra_Reference_number__c.addError(GlobalConstants.TASK_SPECIAL_CHAR_CHECK);
                        boolRecordSaved = false;
                        boolRefNumValidationPassed=false;
                    }else{
                         boolRefNumValidationPassed=true;
                    }
                }
                if(strSpecialCharacterCheck3 != null && !string.isblank(strSpecialCharacterCheck3)){
                    Pattern stringPattern = Pattern.compile('^[a-zA-Z0-9- ]*$');  
                    Matcher specialCharacter3 = stringPattern.matcher(strSpecialCharacterCheck3);
                    if(!specialCharacter3.matches()){
                        taskRecord.Telstra_Tech_ID__c.addError(GlobalConstants.TASK_SPECIAL_CHAR_CHECK);
                        boolRecordSaved = false;
                        boolTechIdValidationPassed=false;
                    }else{
                        boolTechIdValidationPassed=true;
                    }
                }       
                if(strTelstraTechPhone != null && !string.isblank(strTelstraTechPhone)){
                    Matcher priPhoneMatcher = checkPhoneFormat();
                    if(!priPhoneMatcher.matches()){
                        taskRecord.Telstra_Tech_Contact_number__c.addError(GlobalConstants.TASK_TELSTRA_TECH_NUMBER_FORMAT);
                        boolRecordSaved = false;
                        boolPhValidationPassed=false;
                    }else{
                        boolPhValidationPassed=true;
                    }
                }
                if(strSpecialCharacterCheck1 != null && !string.isblank(strSpecialCharacterCheck1)){
                    Matcher specialCharacter = checkSpecialCharacter1();
                    if(!specialCharacter.matches()){
                        taskRecord.Telstra_Tech_name__c.addError(GlobalConstants.TASK_SPECIAL_CHAR_CHECK);
                        boolRecordSaved = false;
                        boolChValidationPassed = false;
                    }
                    else{
                        boolChValidationPassed = true;
                    }
                }
                if(strByPassedPort != null && !string.isblank(strByPassedPort)){
                    Pattern priPattern = Pattern.compile('^[0-9]{1,20}$');
                    Matcher priMatcher = priPattern.matcher(strByPassedPort);
                    if(!priMatcher.matches()){
                        taskRecord.Bypassed_DPU_port_number__c.addError(GlobalConstants.TASK_BYPASSED_PORT_NUMBER);
                        boolRecordSaved = false;
                        boolPoValidationPassed = false;
                    }
                    else{
                        boolPoValidationPassed=true;
                    }
                }boolPoValidationPassed=true;
            }
          if(!boolChValidationPassed || !boolPhValidationPassed || !boolPoValidationPassed || !boolRefNumValidationPassed || !boolTechIdValidationPassed){
              boolValidationsPassed=false;  
          }else{
              boolValidationsPassed=true; 
          }
          if(!boolValidationsPassed){
               boolRecordSaved = false;
          }else{
               upsert taskRecord;
               boolRecordSaved = true;
          }
          taskRecord.recalculateFormulas();
          system.debug('@@saved and val in save;' +boolRecordSaved +boolChValidationPassed +boolPhValidationPassed +boolPoValidationPassed +boolValidationsPassed);
        }
        catch(Exception ex){
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'RecordAlternateContinuityInteraction_CX','saveTask',string.valueOf(taskRecord.id),'ref id '+string.valueOf(taskRecord.id)+' is a Task','','',ex,0);
        }
    }
    
    public void saveTaskandComplete(){
        saveTask();
        Matcher priMatcher = checkPRIFormat();
        system.debug('@@saved and val;' +boolRecordSaved +boolValidationsPassed);
        if(priMatcher.matches()){
            if(boolRecordSaved && boolValidationsPassed){
                strThankYou = GlobalConstants.TASK_SAVE_AND_CLOSE_MESSAGE;
                boolReadOnly = true;
            }
            else{
                strThankYou = '';
                boolReadOnly = false;
            }
            boolClose = false;
            throwError = false;
        }
        else{
            Pri_Format__C PRIcustomObj = Pri_Format__C.getInstance('TASK_PRI_FORMAT_ERROR');
            taskRecord.PRI__c.addError(PRIcustomObj.ErrorDescription__c);
            boolRecordSaved = false;
        }
    }
    
    public void updateTask(){
        boolReadOnly = false;
        boolClose = false;
        boolUpdate = true;
        booltimeOut=false;
        boolTrailTimeOut=false;
        boolDisableSwap=false;
        boolDisableTrail=false;
    }
    
    public PageReference cancelTask() {
        PageReference startPage = new PageReference('/apex/RecordAlternateContinuityInteraction');
        try{
            boolRecordInteraction = false;
            boolCancel = true;
            boolClose = true;
            boolCometDSub = false;
            strReasonForCall = null;
            taskRecord.Reason_for_Call__c = null;
            //deletePermissionSetAssignment();
            strTaskId = (String)taskRecord.get(Schema.Task.id);
                if(strTaskId != '' && strTaskId != null && boolRecordSaved == true && boolUpdate == false ){                
                     delete taskrecord;
                     If((String)taskRecord_cust.get(Schema.Task_Custom__c.id)!='' && (String)taskRecord_cust.get(Schema.Task_Custom__c.id)!=null)delete taskRecord_cust;
                }
            startPage = new PageReference('/apex/RecordAlternateContinuityInteraction');
            startPage.setRedirect(true);
            return startPage;
        }
        catch(Exception ex){
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'RecordAlternateContinuityInteraction_CX','cancelTask',string.valueOf(taskRecord.id),'ref id '+string.valueOf(taskRecord.id)+' is a Task','','',ex,0);
            return null;
        }
    }
    
    public void cancelUpdate(){
        Matcher priMatcher = checkPRIFormat();
        if(priMatcher.matches()){
            boolReadOnly = true;
            boolClose = false; 
            boolCometDSub = false; 
        }else{
            Pri_Format__C PRIcustomObj = Pri_Format__C.getInstance('TASK_PRI_FORMAT_ERROR');
            taskRecord.PRI__c.addError(PRIcustomObj.ErrorDescription__c);
        }
    }
    
    public PageReference closeTask(){
        saveTask();
        //deletePermissionSetAssignment();
        if(boolRecordSaved == true){
            taskrecord.Status = 'Completed';
            if(!RecordAlternateContinuityInteraction_CX.statusSet.contains(taskrecord.Status)){
                upsert taskRecord;
                RecordAlternateContinuityInteraction_CX.statusSet.add(taskrecord.Status);
            }
            PageReference startPage = new PageReference('/apex/RecordAlternateContinuityInteraction');
            startPage.setRedirect(true);
            return startPage;
        }
        else{
            return null;
        }
    }
    
    public void manageConditionalMPairRequired(){
        boolMPairPopulated = true;
    }
    
    public void manageConditionalOPairRequired(){
        boolOPairPopulated = true;
    }
    
    public static List<integrationWrapper.priWrapper> validatePRIUsingExtSystemByPRI(String PRI,HttpResponse response,List<integrationWrapper.priWrapper> PRIListFromExternalSystemByPRI){
    try{        
        if(response.getBody()!='{}'&& response.getBody()!=''){   
            integrationWrapper.RootProdObject RtObject = (integrationWrapper.RootProdObject)JSON.deserialize(response.getBody(),integrationWrapper.RootProdObject.Class);
            for(integer i=0; i<=RtObject.data.size()-1; i++){
                integrationWrapper.priWrapper wrap = new integrationWrapper.priWrapper();
                String priAddress = RtObject.included[i].attributes.address.unstructured;
                wrap.priID = RtObject.data[i].id;
                wrap.copperPathId = RtObject.data[i].attributes.copperPathId;
                wrap.locId = RtObject.data[i].relationships.address.data.id;
                wrap.address = priAddress;
                wrap.type = RtObject.data[i].type;
                wrap.techType=RtObject.included[i].attributes.PrimaryAccessTechnology.technologyType;
                PRIListFromExternalSystemByPRI.add(wrap);
                system.debug('validatePRIUsingExtSystemByPRI@@'+wrap);
            }
        }      
      }
      catch(Exception ex){
        GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'RecordAlternateContinuityInteraction_CX','validatePRIUsingExtSystemByPRI','','','',string.valueOf(response.getBody()),ex,0);
      }     
        return PRIListFromExternalSystemByPRI;
    }
    
    @TestVisible public void getLocationAndAddressForPRI(String strPRI){
        List<integrationWrapper.priWrapper> PRIListFromExternalSystemByPRI = new List<integrationWrapper.priWrapper>();
        HttpResponse response = new HttpResponse();
        if(!Test.isRunningTest()){
            response = integrationUtility.getHttpResponse(strPRI,'');
        }
        else{
            String productInstanceJSONValidResponse = testDataUtility.cisProductInstanceFakeValidJSONResponse();
            response.setBody(productInstanceJSONValidResponse);
            response.setStatusCode(200);
            response.setStatus('Not Found');
        }
        if(response.getStatus() == String.valueOf(Label.ProductInstance_API_Status_Code_404)){
            throwError = true;
            Pri_Format__C PRIcustomObj = Pri_Format__C.getInstance('TASK_PRI_NOT_FOUND_ERROR');
            strRequiredError = PRIcustomObj.ErrorDescription__c;
            taskRecord.Location_ID__c = '';
            taskRecord.Location_Address__c = '';
            taskRecord.CPI_ID__c = '';
        }
        if(response.getStatusCode() == 404 && response.getStatus() != String.valueOf(Label.ProductInstance_API_Status_Code_404)){
            taskRecord.PRI__c.addError(GlobalConstants.TASK_PRI_API_CONNECTION_FAILED);
        }
        if(response.getStatusCode() == 200){
            PRIListFromExternalSystemByPRI = validatePRIUsingExtSystemByPRI(strPRI,response,PRIListFromExternalSystemByPRI);
         }
        if(PRIListFromExternalSystemByPRI != null && PRIListFromExternalSystemByPRI.size() > 0){
            for(integrationWrapper.priWrapper pwrap:PRIListFromExternalSystemByPRI){
                taskRecord.CPI_ID__c = pwrap.copperPathId;
                taskRecord.Location_ID__c = pwrap.locId;
                taskRecord.Location_Address__c = pwrap.address;
                taskRecord.PRITechName__c=pwrap.techType;
                taskRecord.PRITechType__c=pwrap.type ; 
            }
        }else{
            throwError = true;
        Pri_Format__C PRIcustomObj = Pri_Format__C.getInstance('TASK_PRI_NOT_FOUND_ERROR');
        strRequiredError = PRIcustomObj.ErrorDescription__c;
            taskRecord.Location_ID__c = '';
            taskRecord.Location_Address__c = '';
            taskRecord.CPI_ID__c = '';
        } 
    }
    
    public void postPairSwap(){    
    count = 0;
    booltimeOut=false;
    validatePairFormat();  
    
    strDesiredPairId = (String)taskRecord.get(Schema.Task.Desired_Pair_ID__c);
        If((strDesiredPairId == null || string.isblank(strDesiredPairId)) && strReasonForCall == 'Pair Swap'){
            boolPairFormatOK = true;
        }
        //aded due to having to use a reason for change calc field in order to default a value for the Reason for change field
       if(taskRecord.get(Schema.Task.Reason_for_Change__c) == null || taskRecord.get(Schema.Task.Reason_for_Change__c) ==''){
           Schema.DescribeFieldResult fieldResult = Task.Reason_for_Change__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
           for(Schema.PicklistEntry f : ple)
           {    
                   if(taskRecord.get(Schema.Task.Reason_for_Change_Calc__c) == f.getLabel()){
                   strReasonForChange = f.getValue();
               }
           }
       }
       else{
           strReasonForChange = (String)taskRecord.get(Schema.Task.Reason_for_Change__c);
       }  
        if(boolPairFormatOK){       
           boolCometDSub = true;
           boolDisableSwap=true;
           callnbnMicroservice(strReasonForChange,strDesiredPairId,Label.ChangeDistributionPair);
       }  
     } 
     
     public void callnbnMicroservice(String param1,String Param2, String custSetName){  
       HttpResponse mResp = new HttpResponse();
        if(!Test.isRunningTest()){ 
            String strnbnCorrelationId = '';
            strnbnCorrelationId = integrationUtility.newGuid(strnbnCorrelationId);
            System.debug('@@Generated correlation;'+strnbnCorrelationId);
            if(custSetName==Label.ChangeDistributionPair)taskRecord.NBNCorrelationId__c=strnbnCorrelationId ;
            mResp = integrationUtility.postReqTMS(taskRecord.CPI_ID__c,param1,Param2,strnbnCorrelationId,custSetName);  
            if(custSetName=='NetworkTrail')Process_Obj(strnbnCorrelationId);
        } 
         else{
            String PNIJSONValidResponse = testDataUtility.mockPNiSwapPairSuccessCall();
            mResp.setBody(PNIJSONValidResponse);
            mResp.setStatusCode(202);
            mResp.setStatus('Success');                                
        }
        processResp(mResp,custSetName);
     }
     
     public void Process_Obj(String strnbnCorrelationId){
            taskRecord.NetworkTrailCoRelationID__c=strnbnCorrelationId ; 
            taskRecord_cust = new Task_Custom__c(OPair__c='',MPair__c='',NetworkTrailCoRelationID__c=strnbnCorrelationId);
            insert taskRecord_cust ;system.debug('taskRecord_cust @@'+ taskRecord_cust );
            taskRecord.Task_Custom__c=taskRecord_cust.id;  
            update taskRecord; 
     }
     
     
      //method to process Response received from Microservice
      public void processResp(HttpResponse mResp,String custSetName){     
      if(mResp.getStatusCode() == 400 || mResp.getStatusCode() == 401  || mResp.getStatusCode() == 404 || mResp.getStatusCode() == 500 || mResp.getStatusCode() == 502){                     
         if  (custSetName==Label.ChangeDistributionPair) {  
             GlobalUtility.logIntMessage(GlobalConstants.ERROR_RECTYPE_NAME,'RecordAlternateContinuityInteraction_CX','postPairSwap','','','',string.valueOf(mResp.getBody()),0); 
          } 
          if  (custSetName=='NetworkTrail') {  
             GlobalUtility.logIntMessage(GlobalConstants.ERROR_RECTYPE_NAME,'RecordAlternateContinuityInteraction_CX','postNetworkTrail','','','',string.valueOf(mResp.getBody()),0); 
          }        
        }
        if(mResp.getStatusCode() == 202){ 
             if  (custSetName==Label.ChangeDistributionPair) {
                integrationWrapper.NIRespData RtRespObject = (integrationWrapper.NIRespData)JSON.deserialize(mResp.getBody(),integrationWrapper.NIRespData.Class); 
                if(RtRespObject.metadata.nbnCorrelationId!=null){
                  strnbnCorrelationId = RtRespObject.metadata.nbnCorrelationId;              
                  taskRecord.NBNCorrelationId__c=strnbnCorrelationId;
                  update taskRecord;
                  System.debug('@@202 Return correlation;'+strnbnCorrelationId);
            }
          } 
          
         if(custSetName=='NetworkTrail') {  
            integrationWrapper.NetWorkTrialResp NetworkTrail = (integrationWrapper.NetWorkTrialResp )JSON.deserialize(mResp.getBody(),integrationWrapper.NetWorkTrialResp .Class); 
            if(NetworkTrail.metadata.nbnCorrelationId!=null ){                    
                taskRecord.NetworkTrailCoRelationID__c=NetworkTrail.metadata.nbnCorrelationId;
                taskRecord_cust = new Task_Custom__c(OPair__c='',MPair__c='',NetworkTrailCoRelationID__c=strnbnCorrelationId);
                taskRecord.Task_Custom__c=taskRecord_cust.id;
                update taskRecord;
               // System.debug('@@202 NetworkTrailReturn correlation;'+NetworkTrail.metadata.nbnCorrelationId);
             } 
          }      
        } 
      }
        
    public void validatePairFormat(){ 
    strDesiredPairId = (String)taskRecord.get(Schema.Task.Desired_Pair_ID__c);
        If(strDesiredPairId != null && !string.isblank(strDesiredPairId)){
            Matcher desiredMatcher = checkDesiredFieldFormat();
            if(!desiredMatcher.matches()){
                taskRecord.Desired_Pair_ID__c.addError(GlobalConstants.TASK_DESIRED_PAIR_ID_FORMAT);
                boolPairFormatOK = false;
            }
            else{
                boolPairFormatOK = true;
            }  
        } 
        system.debug('@@boolPairFormatOK2 '+boolPairFormatOK ); 
    }  
    
    public void getSwapStatus() { 
     count ++;
     NBNIntegrationInputs__c TimeOut  = NBNIntegrationInputs__c.getInstance('ChangeDistributionPair');   
     if (count==Integer.valueOf(TimeOut.UI_TimeOut__c)){
     booltimeOut=true; 
     boolCometDSub = false; 
     }
        if(taskRecord.NBNCorrelationId__c !=null){          
        list<task> taskRec=[select PNI_Inbound_Int_Stat__c,Pair_Network_Cable_Id__c,Error__c,Network_Inventory_Error_Code__c,Network_Inventory_Swap_Status__c from task where NBNCorrelationId__c =:taskRecord.NBNCorrelationId__c];          
            if(taskRec.size()>0){
                if(taskRec[0].Network_Inventory_Swap_Status__c!=null  && taskRec[0].Network_Inventory_Swap_Status__c==Label.Success){       
                    taskRecord.Pair_Network_Cable_Id__c=taskRec[0].Pair_Network_Cable_Id__c;
                    taskRecord.Network_Inventory_Swap_Status__c=taskRec[0].Network_Inventory_Swap_Status__c;
                    taskRecord.PNI_Inbound_Int_Stat__c=Label.Success_Screen_Message;
                    boolPNIstatId=true;
                    boolPNIstatErr=false;
                    boolCometDSub = false;  
                }    
                if(taskRec[0].Network_Inventory_Swap_Status__c!=null  && taskRec[0].Network_Inventory_Swap_Status__c==Label.Failure){ 
                    taskRecord.PNI_Inbound_Int_Stat__c=Label.Failure_Screen_Message;
                    taskRecord.Network_Inventory_Error_Code__c=taskRec[0].Network_Inventory_Error_Code__c;            
                    taskRecord.Network_Inventory_Swap_Status__c=taskRec[0].Network_Inventory_Swap_Status__c;            
                    taskRecord.error__c=taskRec[0].error__c;                        
                    boolPNIstatErr=true;
                    boolCometDSub = false; 
                    
                }
             }
        }
    }
    
    
    public void postNetworkTrail(){  
    //system.debug('taskRecord.PRITechType__c'+taskRecord.PRITechType__c);taskRecord.PRITechName__c=='Fibre To The Node' && !=null && strReasonForCall=='Cable Record Enquiry');       
    system.debug('@@taskRecord.PRITechType__c'+taskRecord.PRITechName__c);
    system.debug('@@strCopperPairId'+strCopperPairId);
    system.debug('@@strReasonForCall'+strReasonForCall);
    system.debug('@@PRITechName__c'+taskRecord.PRITechName__c);
        if(taskRecord.PRITechType__c=='csllProductInstance' && (taskRecord.PRITechName__c=='Fibre To The Node' || taskRecord.PRITechName__c=='Fibre To The Curb') && strCopperPairId!=null && strReasonForCall=='Cable Record Enquiry'){
            boolNetTrail =true; 
            boolNTstatId=true;
            boolTrailTimeOut=false;
            boolDisableTrail=true;          
            system.debug('boolNetTrail@@'+boolNetTrail);
            system.debug('boolNTstatId@@'+boolNTstatId);
            callnbnMicroservice(taskRecord.PRITechName__c,'','NetworkTrail');
        }      
    }
    
    
     public void getNetTrailStat() { 
     networkTrailTimeOut ++;
     system.debug('boolTrailTimeOut@@ boolNTstatId @@ NetworkTrailCoRelationID__c'+boolTrailTimeOut+boolNTstatId + taskRecord.NetworkTrailCoRelationID__c);
     NBNIntegrationInputs__c TimeOut  = NBNIntegrationInputs__c.getInstance('NetworkTrail');
     if (networkTrailTimeOut==Integer.valueOf(TimeOut.UI_TimeOut__c)){
         boolTrailTimeOut=true; 
         boolNTstatId = false; 
     }
        if(taskRecord.NetworkTrailCoRelationID__c !=null){          
        list<task> taskRec=[select NetworkTrailCoRelationID__c,Task.Task_Custom__r.Opair__c,Task.Task_Custom__r.MPair__c,Error_trial__c,DPU_Serial_Number__c,CIU_DPU_ID__c,DPU_Port_Number__c,CIU_Cut_In_Status__c,TCO_or_MDF__c,Copper_Lead_In_Cable__c,Copper_Distribution_Cable__c,O_Pair__c,Copper_Main_Cable__c,M_pair__c,FTTN_Node__c,Type_Of_Node__c,NetworkTrail_Status__c  from task where NetworkTrailCoRelationID__c =:taskRecord.NetworkTrailCoRelationID__c];          
            system.debug('@@taskRec'+taskRec);
            if(taskRec.size()>0){
            system.debug('@@taskRec[0].NetworkTrail_Status__c'+taskRec[0].NetworkTrail_Status__c);
            List<String> pillarList;
                if(taskRec[0].NetworkTrail_Status__c!=null  && taskRec[0].NetworkTrail_Status__c=='Completed'){  
                    if (taskRec[0].TCO_or_MDF__c!=null) strTrial ='TCO or MDF: '+ taskRec[0].TCO_or_MDF__c+ '<br/>';  
                    if (taskRec[0].Copper_Lead_In_Cable__c!= null)strTrial += 'Copper Lead-In Cable: '+taskRec[0].Copper_Lead_In_Cable__c+'<br/>'; 
                    if (taskRec[0].DPU_Serial_Number__c!= null)strTrial += 'DPU Serial Number: '+taskRec[0].DPU_Serial_Number__c+'<br/>'; 
                    if (taskRec[0].CIU_DPU_ID__c!= null)strTrial += 'DPU Name: '+taskRec[0].CIU_DPU_ID__c+'<br/>'; 
                    if (taskRec[0].DPU_Port_Number__c!= null)strTrial += 'DPU Port Number: '+taskRec[0].DPU_Port_Number__c+'<br/>'; 
                    if (taskRec[0].CIU_Cut_In_Status__c!= null)strTrial += 'CIU Cut-In Status: '+taskRec[0].CIU_Cut_In_Status__c+'<br/>';                     
                    if (taskRec[0].Copper_Distribution_Cable__c!=null)strTrial +='Copper Distribution Cable: '+taskRec[0].Copper_Distribution_Cable__c+'<br/>';
                    if (taskRec[0].O_Pair__c!=null)strTrial += 'O Pair:'+taskRec[0].O_Pair__c+'<br/>';
                    if (taskRec[0].Task_Custom__r.OPair__c!=null){
                        pillarList=taskRec[0].Task_Custom__r.OPair__c.split(',');
                        if(pillarList.size()>0){
                        integer i=97;
                        List<integer> iList=new List<integer>{i};
                            for(String s:pillarList){
                                String convertedChar = String.fromCharArray(iList);
                                strTrial += 'O Pair:'+convertedChar +s+'<br/>';
                                iList.clear();
                                iList.add(i++);
                            }
                        }
                    }
                    if (taskRec[0].Copper_Main_Cable__c!=null) strTrial +='Copper Main Cable:'+taskRec[0].Copper_Main_Cable__c+'<br/>';
                    if (taskRec[0].M_pair__c!=null) strTrial +='M pair: '+taskRec[0].M_pair__c+'<br/>';
                    if (taskRec[0].Task_Custom__r.MPair__c!=null){
                    strTrial += 'O Pair:'+taskRec[0].Task_Custom__r.MPair__c+'<br/>';
                    }
                    if (taskRec[0].FTTN_Node__c!=null) strTrial +='FTTN Node:'+taskRec[0].FTTN_Node__c+'<br/>';
                    if (taskRec[0].Type_Of_Node__c!=null) strTrial +='Type Of Node:'+taskRec[0].Type_Of_Node__c+'<br/>';
                    boolTrialSucc=true;  
                    boolNTstatId=false;                    
                    boolTrialErr=false;
                    system.debug('@@strTrial '+strTrial );
                    system.debug('@@taskRecord....__'+taskRecord);
                }  
                
                if(taskRec[0].NetworkTrail_Status__c!=null  && taskRec[0].Error_trial__c!=null){
                system.debug('failed'); 
                    taskRecord.Error_trial__c=taskRec[0].Error_trial__c;    
                    boolNTstatId=false;
                    boolTrialErr=true;
                    boolTrialSucc=false;
                    system.debug('boolNTstatId '+boolNTstatId+'boolTrialErr'+boolTrialErr);
                }
             }
        }
    }
}