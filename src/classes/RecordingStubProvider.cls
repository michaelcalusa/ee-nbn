/**
 * Created by alan on 2019-02-25.
 */

@isTest
public class RecordingStubProvider implements System.StubProvider{

    public static Integer NEVER = 0;
    private Type p_stubbedType;
    private Map<String, List<StubMatcher>> p_expectedMethodInvocations = new Map<String, List<StubMatcher>>();
    private Map<String, List<List<Object>>> p_actualMethodInvocations = new Map<String, List<List<Object>>>();

    public RecordingStubProvider(Type stubbedType){
        p_stubbedType = stubbedType;
    }

    public Object handleMethodCall( Object stubbedObject,
                                    String stubbedMethodName,
                                    Type returnType,
                                    List<Type> listOfParamTypes,
                                    List<String> listOfParamNames,
                                    List<Object> listOfArgs) {

        //record method invocation
        if(!p_actualMethodInvocations.containsKey(stubbedMethodName)){
            p_actualMethodInvocations.put(stubbedMethodName, new List<List<Object>>());
        }
        p_actualMethodInvocations.get(stubbedMethodName).add(listOfArgs);
        System.debug('RecordingStubProvider for ' + p_stubbedType.getName()  + ' invoked with methodName ' + stubbedMethodName + 'and args ');
        return this.handleInvocation(stubbedMethodName, p_actualMethodInvocations.get(stubbedMethodName).size()-1);
    }

    private Object handleInvocation(String stubbedMethodName, Integer invocationNumber){

        List<StubMatcher> stubMatchers = p_expectedMethodInvocations.get(stubbedMethodName);
        if(stubMatchers != null && stubMatchers.size() > invocationNumber){
            if(stubMatchers.get(invocationNumber).matches(p_actualMethodInvocations.get(stubbedMethodName).get(invocationNumber))){
                System.debug('RecordingStubProvider invoking matching Stub');
                return stubMatchers.get(invocationNumber).invoke();
            }
        }

        System.debug('RecordingStubProvider did not return stub for ' + stubbedMethodName  + ', invocation ' + invocationNumber);
        return null;
    }

    public StubMatcher given(String stubbedMethodName){
        StubMatcher stubMatcher = new StubMatcher();
        if(!p_expectedMethodInvocations.containsKey(stubbedMethodName)){
            p_expectedMethodInvocations.put(stubbedMethodName, new List<StubMatcher>());
        }
        p_expectedMethodInvocations.get(stubbedMethodName).add(stubMatcher);

        return stubMatcher;
    }

    public class StubMatcher {

        private Exception p_exceptionToThrow;
        private Object p_returnValue;
        private List<ArgumentMatcher> p_argMatchers;

        public StubMatcher when(List<ArgumentMatcher> argMatchers){
            p_argMatchers = argMatchers;
            return this;
        }

        public void thenReturn(Object returnValue){
            p_returnValue = returnValue;
        }

        public void thenVoid(){}

        public void thenThrow(Exception e){
            p_exceptionToThrow = e;
        }

        public Boolean matches(List<Object> actualArgs){
            if(p_argMatchers.size() != actualArgs.size()){
                System.debug('StubMatcher expected number of args (' + p_argMatchers.size() + ') differs from actual number of args (' + actualArgs.size() + ').');
                return false;
            }
            for (Integer i = 0; i < actualArgs.size(); i++) {
                //assert expected matches actual
                if(!p_argMatchers.get(i).matches(actualArgs.get(i))){
                    System.debug('StubMatcher failed to match arg ' + i + ' ' + printObject(actualArgs.get(i)));
                    return false;
                }
            }
            return true;
        }

        public Object invoke(){
            if(p_exceptionToThrow != null){
                throw p_exceptionToThrow;
            }else {
                return p_returnValue;
            }
        }

        private String printObject(Object obj){
            try{
                return obj == null ? 'null' : obj.toString();
            }catch(Exception e){
                System.debug('could not print object');
                //TODO: how to print out items in collection
                return '';
            }
        }
    }

    //verify interactions
    public void verify(String stubbedMethodName, List<ArgumentMatcher> expectedArgs){
        this.verify(stubbedMethodName, 1, expectedArgs);
    }

    public void verify(String stubbedMethodName, Integer invocationNumber, List<ArgumentMatcher> expectedArgs){
        System.assert(invocationNumber > 0, 'InvocationNumber must be greater than 0');
        try {
            List<List<Object>> actualArgsList = p_actualMethodInvocations.get(stubbedMethodName);
            if(null == actualArgsList){
                System.assert(false, 'Did not register any invocation of ' + stubbedMethodName);
            }
            if(actualArgsList.size() < invocationNumber){
                System.assert(false, 'Did not register invocation ' + invocationNumber + ' of ' + stubbedMethodName);
            }
            List<Object> actualArgs = actualArgsList.get(invocationNumber-1);

            if(expectedArgs.size() != actualArgs.size()){
                System.assert(false, 'Expected number of args (' + expectedArgs.size() + ') differs from actual number of args (' + actualArgs.size() + ').');
            }

            Boolean verified = true;
            for (Integer i = 0; i < expectedArgs.size(); i++) {
                //assert expected matches actual
                if(!expectedArgs.get(i).matches(actualArgs.get(i))){
                    System.debug('Failed to match arg ' + i + ' expected (' + printObject(expectedArgs.get(i)) +'), actual ('+printObject(actualArgs.get(i))+')');
                    verified &= false;
                }
            }
            System.assertEquals(true, verified, 'Expected args and actual args for ' + stubbedMethodName + ' invocation ' + invocationNumber + ' differ');

        }catch(Exception e){
            System.assert(false, e.getMessage());
        }
    }

    public void verify(String stubbedMethodName, Integer expectedInvocations){
        Integer actualInvocations = 0;
        if(p_actualMethodInvocations.get(stubbedMethodName) != null){
            actualInvocations = p_actualMethodInvocations.get(stubbedMethodName).size();
        }

        if(expectedInvocations == 0){
            System.assertEquals(null, p_actualMethodInvocations.get(stubbedMethodName), 'Expected 0 invocations of ' + stubbedMethodName + ' but registered ' + actualInvocations);
        }else{
            System.assertEquals(expectedInvocations, p_actualMethodInvocations.get(stubbedMethodName).size(), 'Expected ' + expectedInvocations + ' invocations of ' + stubbedMethodName + ' but registered ' + actualInvocations);
        }
    }

    public void verifyZeroInteractions(){
        Integer actualInteractions = 0;
        for(String stubbedMethodName : p_actualMethodInvocations.keySet()){
            actualInteractions += p_actualMethodInvocations.get(stubbedMethodName).size();
        }
        System.assertEquals(true, p_actualMethodInvocations.isEmpty(), 'Expected 0 interactions but registered ' + actualInteractions);
    }

    private String printObject(Object obj){
        try{
            return obj == null ? 'null' : obj.toString();
        }catch(Exception e){
            System.debug('could not print object');
            //TODO: how to print out items in collection
            return '';
        }
    }

}