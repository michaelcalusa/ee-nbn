public class RelatedCasesController {
    @AuraEnabled
    public static List<Case> getCases(ID recordId) {
       list<ID> caseRecordTypeIDList = new list<ID>();
		ID queryCID = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Query').getRecordTypeId();
		caseRecordTypeIDList.add(queryCID);
        ID FormalComplaintCID = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Formal Complaint').getRecordTypeId();
		caseRecordTypeIDList.add(FormalComplaintCID);
        ID ComplexComplaintCID = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Complex Complaint').getRecordTypeId();
		caseRecordTypeIDList.add(ComplexComplaintCID);
        ID UrgentComplaintCID = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Urgent Complaint').getRecordTypeId();
		caseRecordTypeIDList.add(UrgentComplaintCID);
        ID ActivationsJeopardyCID = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Activations Jeopardy').getRecordTypeId();
		caseRecordTypeIDList.add(ActivationsJeopardyCID);
        ID AssuranceJeopardyCID = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Assurance Jeopardy').getRecordTypeId();
		caseRecordTypeIDList.add(AssuranceJeopardyCID);
        ID EscalationsTriageCID = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Escalations Triage').getRecordTypeId();
		caseRecordTypeIDList.add(EscalationsTriageCID);
        ID CustConnCMCID = Schema.Sobjecttype.Case.getRecordTypeInfosByName().get('Cust Conn CM').getRecordTypeId();
		caseRecordTypeIDList.add(CustConnCMCID);
        
       Case parentCase = [Select Id, Loc_Id__c from Case where ID = :recordID];
       
       List<Case> cases =
                    [Select Id, CaseNumber, Case_Record_Type__c, RecordTypeName__c, Status, Subject From Case Where RecordTypeID IN :caseRecordTypeIDList AND Loc_Id__c= :parentCase.Loc_Id__c AND ID!= :recordId AND Loc_Id__c!=NULL];    
                    return cases;
    }
}