/*
    ReleaseToggles are configured in Release_Toggles__mdt custom metadata
 */

public class ReleaseToggle {


    public Boolean isActive(String releaseToggleName){

        //if release toggle is not found or has been removed default to isEnabled=false
        Boolean isActive = false;

        String SOQL = 'Select IsActive__c From Release_Toggles__mdt Where DeveloperName = \'' + releaseToggleName + '\' LIMIT 1' ;
        try {
            SObject s = database.query(SOQL);
            if(s != null){
                isActive = (Boolean)s.get('IsActive__c');
            }else{
                System.debug(LoggingLevel.WARN, 'Could not find release toggle with name ' + releaseToggleName + '. Defaulting to IsActive=false');
            }
        }catch (Exception e) {
            System.debug(LoggingLevel.ERROR, e.getMessage());
        }

        return isActive;
    }
}