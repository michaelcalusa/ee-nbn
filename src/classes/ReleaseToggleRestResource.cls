/**
 * Created by alan on 2019-02-27.
 */

@RestResource(urlMapping='/ReleaseToggles')
global with sharing class ReleaseToggleRestResource {

    @HttpGet
    global static Map<String, Boolean> getReleaseToggles() {

        Map<String, Boolean> togglesMap = new Map<String, Boolean>();

        for (sobject s: database.query('Select DeveloperName, IsActive__c From Release_Toggles__mdt')) {
            togglesMap.put((String)s.get('DeveloperName'), (boolean)s.get('IsActive__c'));
        }

        return togglesMap;
    }

}