/**
 * Created by alan on 2019-03-20.
 */

@IsTest
private class ReleaseToggleTest {

    //not ideal to assume a toggle is active but we just want to confirm that the ReleaseToggle class is working as expected
    @IsTest
    static void shouldReturnTrueWhenToggleIsActive() {
        ReleaseToggle releaseToggle = (ReleaseToggle)ObjectFactory.getInstance(ReleaseToggle.class);
        System.assertEquals(true, releaseToggle.isActive('MarCaseAdditionalTrailHistory'));
    }

    @IsTest
    static void shouldReturnFalseWhenToggleNotFound() {
        ReleaseToggle releaseToggle = (ReleaseToggle)ObjectFactory.getInstance(ReleaseToggle.class);
        System.assertEquals(false, releaseToggle.isActive('UnknownToggle'));
    }
}