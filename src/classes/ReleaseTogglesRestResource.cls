@RestResource(urlMapping='/releasetoggles')
global class ReleaseTogglesRestResource {

    @HttpGet
    global static Map<String, String> getReleaseToggles() {

        Map<String, String> togglesMap = new Map<String, String>();

        for (sobject s: database.query('Select DeveloperName, IsActive__c From Release_Toggles__mdt')) {
            togglesMap.put((String)s.get('DeveloperName'), String.valueOf(s.get('IsActive__c')));
        }

        return togglesMap;
    }

}