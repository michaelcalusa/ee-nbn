/***************************************************************************************************
Class Name:         RemedyAssociationHandler
Class Type:         Trigger handler class
Version:            1.0 
Created Date:       
Function:           Update Incident Details (PRI and AVC) on After Insert of Remedy_Association__c record
Modification Log:
* Developer          	Date             Description
* --------------------------------------------------------------------------------------------------                 
* Syed Moosa Nazir TN	13/06/2018		Updated code by removing unnecessary commented code as part of CUSTSA-17093
****************************************************************************************************/
public class RemedyAssociationHandler implements ITriggerHandler {
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    //Checks to see if the trigger has been disabled either by custom setting or by running code
    public Boolean IsDisabled(){
        return TriggerDisabled;
    }    
    /*************************************************************************************************
    Trigger Handler Action Methods:
    *************************************************************************************************/
    public void BeforeInsert(List<SObject> newItems, Map<Id, SObject> newItemMap) {}
    public void BeforeUpdate(List<SObject> newItems, List<SObject> oldItems, Map<Id, SObject> newItemMap, Map<Id, SObject> oldItemMap) {}
    public void BeforeDelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    public void AfterInsert(List<SObject> newItems, Map<Id, SObject> newItemMap) {
		UpdateIncidentDetails(newItems, newItemMap);
    }
    public void AfterUpdate(List<SObject> newItems, List<SObject> oldItems, Map<Id, SObject> newItemMap, Map<Id, SObject> oldItemMap) {}
    public void AfterDelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    public void AfterUndelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
	/*************************************************************************************************
    Business Logic Methods:
    *************************************************************************************************/
    public void UpdateIncidentDetails(List<SObject> newItems, Map<Id, SObject> newItemMap){
		try{
			List<Incident_Management__c> incidentManagementList = new List<Incident_Management__c>();
			Map<String, List<String>> incidentAssociationMap = new Map<String, List<String>>();
			for (Remedy_Association__c iterAssosiation : (List<Remedy_Association__c>)newItems){
				if(incidentAssociationMap.containsKey(iterAssosiation.Incident__c)) {
					List<String> identifierStrList = incidentAssociationMap.get(iterAssosiation.Incident__c); 
					identifierStrList.add(iterAssosiation.associatedIdentifier__c);
					incidentAssociationMap.put(iterAssosiation.Incident__c, identifierStrList);
				}  else{
					incidentAssociationMap.put(iterAssosiation.Incident__c, new List<String> { iterAssosiation.associatedIdentifier__c });
				}
			}
			if (!incidentAssociationMap.isEmpty()){
				for (String key : incidentAssociationMap.keySet()) {
					Incident_Management__c im = new Incident_Management__c();
					im.Id = key;
					List<String> associatedIdentifierList = incidentAssociationMap.get(key);
					for (String identifier : associatedIdentifierList){
						if(String.isNotBlank(identifier)){
							if (identifier.startsWith('PRI')){
								im.PRI_ID__c = identifier;
							}
							if (identifier.startsWith('AVC')){
								im.AVC_Id__c = identifier;
							} 
						}
					}
					incidentManagementList.add(im);
				}   
			}
			if (!incidentManagementList.isEmpty()){
				IncidentManagementTriggerHandler.TriggerDisabled = true;
				UPDATE incidentManagementList;
				IncidentManagementTriggerHandler.TriggerDisabled = false;
			}
		}
        catch (Exception ex){
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'RemedyAssociationHandler','UpdateIncidentDetails','','',ex.getMessage(), ex.getStackTraceString(),ex, 0);
        }
	}
}