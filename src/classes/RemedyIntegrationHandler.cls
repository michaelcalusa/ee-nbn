/***************************************************************************************************
Class Name:         RemedyIntegrationHandler
Class Type:         Trigger handler class
Version:            1.0 
Created Date:       23 April 2018
Function:           Handle creation of all outbound platform events & errors
Input Parameters:   triggers new, old
Output Parameters:  None
Description:        To be used as a service class
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* SV                23/04/2018      Created - Version 1.0 
****************************************************************************************************/  
public class RemedyIntegrationHandler implements ITriggerHandler {
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    // Checks to see if the trigger has been disabled either by custom setting or by running code
    // Set of String for whom we do not need lock : Added By GS
    public static Set<String> actionsWithNoLock = new Set<String>{'addNote'};
        
    public Boolean IsDisabled()
    {
        return TriggerDisabled;
    }    
    public void BeforeInsert(List<SObject> newItems, Map<Id, SObject> newItemMap) {}
    public void BeforeUpdate(List<SObject> newItems, List<SObject> oldItems, Map<Id, SObject> newItemMap, Map<Id, SObject> oldItemMap) {
        updateStatusToSuccess(newItems);
    }
    public void BeforeDelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    public void AfterInsert(List<SObject> newItems, Map<Id, SObject> newItemMap) 
    {
       processOutboundJSONs(newItems, 'RemedyIntegrationHandling__c');
    }
    public void AfterUpdate(List<SObject> newItems, List<SObject> oldItems, Map<Id, SObject> newItemMap, Map<Id, SObject> oldItemMap) 
    {
       processOutboundJSONs(newItems, 'RemedyIntegrationHandling__c');
    }
    public void AfterDelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    public void AfterUndelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    //method to set the status to success for max attempts of retry on updateSalesforceRecordId action
    public void updateStatusToSuccess(List<SObject> newItems){
         for(SObject sobj : newItems) {
            Decimal tmpCounter = (sobj.get('Retry_Counter__c') != null) ? (Decimal) sobj.get('Retry_Counter__c') : 0.0;
            Decimal maxCounter = Decimal.valueOf(Label.JIGSAW_Retry_Counter);
            if(tmpCounter >= maxCounter) {
                String tmpStatus = (String)sobj.get('Status__c');
                String action = (String)sobj.get('action__c');
                if(tmpStatus  != null && 
                   tmpStatus.equalsIgnoreCase('Failed') && 
                   action.equalsIgnoreCase('updateSalesforceRecordId')) {
                       sobj.put('Status__c','Success');
                }
            }
         }
    }
    //method to queue the outbound platform events
    public void processOutboundJSONs(List<SObject> newItems, String SObjectName)
    {	
         // Production Defect : Inc Numbers and Map to keep Incidents with no need of lock : Added By GS
         Set<String> incWithNoLock = new Set<String>();
         Map<String,sObject> incMapWithNoLock = new Map<String,sObject>();
         // Production Defect Ends. 

         //Set of strings to store the incident numbers which still have the tranasctions failed in the Integration handling object
         Set<String> failedincNum = new Set<String>();
         //Set of strings to store the incident numbers which still have the transactions failed in the Integration handling object
         Set<String> inprogressIncNum = new Set<String>();
         //Set of string to store the failed incidents which have user action as updateSalesforceRecordId
         Set<String> failedIncNumForupdateSalesforceRecordId = new Set<String>();
         //Set of string to store the failed RPA records which have user action as updateSalesforceRecordId
         List<RemedyIntegrationHandling__c> failedRIHListForupdateSalesforceRecordId = new List<RemedyIntegrationHandling__c>();
         //map of incident number and Integration handling object
         Map<String,SObject> incNumIntegrationObjectMap = new Map<String,SObject>();
         //Set of strings of all incident numbers
         Set<String> incNumList = new Set<String>();
         //Generic List of Incident Management Object
         List<SObject> incObjList = new List<SObject>();
         SObjectName = (SObjectName == null || String.IsBlank(SObjectName)) ? 'RemedyIntegrationHandling__c' : SObjectName;
         Schema.SObjectType sobjType = Schema.getGlobalDescribe().get(SObjectName);
         String RPAoperatorActionName;
         String RPAStatus;
         //Map to store Remedy Integration Handling Record with Incident Number as key.
         Map<String,Sobject> incidentremedyIntegrationHandlingMap = new Map<String,Sobject>();
         //populate the incNumList
         for(SObject sobj : newItems) 
         {
            if(sobj.get('incidentNumber__c') != null)
            {
                incNumList.add((String)sobj.get('incidentNumber__c'));
            }
            RPAoperatorActionName = (String)sobj.get('action__c');
            RPAStatus = (String)sobj.get('Status__c');
            if( sobj.get('incidentNumber__c') != null && 
                String.isNotBlank(RPAoperatorActionName) && 
                ( RPAoperatorActionName.equalsIgnoreCase('relateToIncident') || 
                  RPAoperatorActionName.equalsIgnoreCase('nbnHeld') || 
                  RPAoperatorActionName.equalsIgnoreCase('addNote')
                ) && 
                String.isNotBlank(RPAStatus) && 
                (RPAStatus.equalsIgnoreCase('Success') || RPAStatus.equalsIgnoreCase('Failed'))
            )
            {
                if(!incidentremedyIntegrationHandlingMap.keyset().Contains((String)sobj.get('incidentNumber__c')))
                {
                    incidentremedyIntegrationHandlingMap.put((String)sobj.get('incidentNumber__c'),sobj);
                }
            }
             
            // To prepare Set of Incident numbers which does not require lock. : Added By GS
            if(String.isNotBlank(RPAoperatorActionName) && actionsWithNoLock.contains(RPAoperatorActionName)
              											&& sobj.get('incidentNumber__c') != null){
               incWithNoLock.add((String)sobj.get('incidentNumber__c'));
            }
            // Ends
            
         }
         
         system.debug('All Inc list'+incNumList);
         if(incNumList.size() > 0) {
             List<SObject> sObjList = new List<SObject>();
             Set<String> tmpList = new Set<String>{'Failed','Awaiting Response'};
             //query the remedy integration handling object for failed incidents
             sObjList = database.query('SELECT'+remedyRPAErrorHandlingUtil.prepareFieldSet(SObjectName)+' FROM '+SObjectName+' WHERE incidentNumber__c IN:incNumList AND Status__c IN:tmpList');
             for(SObject sobj : sObjList) {
                 String tmpStatus = (String)sobj.get('Status__c');
                 String action = (String)sobj.get('action__c');
                 if(tmpStatus  != null && 
                    tmpStatus.equalsIgnoreCase('Failed') && 
                    action.equalsIgnoreCase('updateSalesforceRecordId')) {
                     failedIncNumForupdateSalesforceRecordId.add((String)sobj.get('incidentNumber__c')); 
                     failedRIHListForupdateSalesforceRecordId.add((RemedyIntegrationHandling__c) sobj);
                 }
                 else if(tmpStatus  != null && tmpStatus.equalsIgnoreCase('Failed'))
                     failedincNum.add((String)sobj.get('incidentNumber__c')); 
                 else if(tmpStatus  != null && tmpStatus.equalsIgnoreCase('Awaiting Response'))
                     inprogressIncNum.add((String)sobj.get('incidentNumber__c')); 
             }
             system.debug('Failed Inc list'+failedincNum);
             system.debug('In Progress Inc list'+inprogressIncNum);
             // remove the set of incident #s from incNumList which still have failed transactions with user action as updatesalesforce record id: send the boolean RPA Error occurred as false
             if(failedIncNumForupdateSalesforceRecordId.size() > 0) { 
                 incNumList.removeAll(failedIncNumForupdateSalesforceRecordId);
                 if(!failedRIHListForupdateSalesforceRecordId.isEmpty()){
                    errorsController_LC.createRetryRecords(failedRIHListForupdateSalesforceRecordId);
                 }
             }
             // remove the set of incident #s from incNumList which still have failed transactions

             // To prepare Map of Incident SObjects which does not require lock. : Added By GS
             if(!incWithNoLock.isEmpty()){
                 incMapWithNoLock = fetchIncidentMap(incWithNoLock,'Incident_Management__c','Incident_Number__c');
             }
             // Ends 

             if(failedincNum.size() > 0) { 
                 incNumList.removeAll(failedincNum);
                 //fetch the incident list to pass failedincNum as input to update the boolean value in Incident Management object to show the error record
                 incObjList.addAll(fetchIncidentList(failedincNum, true, 'Incident_Management__c',incidentremedyIntegrationHandlingMap,incMapWithNoLock));
             }
             
             //remove in progress incident #s from incNumList 
             if(inprogressIncNum.size() > 0) { 
                 incNumList.removeAll(inprogressIncNum);
                 //fetch the incident list to pass failedincNum as input to update the boolean value in Incident Management object to show the error record
                 incObjList.addAll(fetchIncidentList(inprogressIncNum, false, 'Incident_Management__c',incidentremedyIntegrationHandlingMap,incMapWithNoLock));
             }
             
             system.debug('Queued Inc list'+incNumList);
             if(incNumList.size() > 0) {
                 //fetch the incident list to pass incNumList as input to update the boolean value to hide the error record
                 incObjList.addAll(fetchIncidentList(incNumList, false, 'Incident_Management__c',incidentremedyIntegrationHandlingMap,incMapWithNoLock));
                 //query the integrationHandling object to get the JSONs created first for each incident an publish them on Event bus
                 String tmp = 'Queued';
                 sObjList.clear();
                 sObjList = database.query('SELECT'+remedyRPAErrorHandlingUtil.prepareFieldSet(SObjectName)+' FROM '+SObjectName+' WHERE incidentNumber__c IN:incNumList AND Status__c =:tmp');
                 tmp = '';
                 //create a map of incident number and remedy integration handling object to store only one oldest record per incident number
                 for(SObject sobj : sObjList){
                     tmp = (String)sobj.get('incidentNumber__c');
                     if(incNumIntegrationObjectMap.containsKey(tmp)) {
                         if((DateTime)sobj.get('CreatedDate') < (DateTime)incNumIntegrationObjectMap.get(tmp).get('CreatedDate'))
                             incNumIntegrationObjectMap.put(tmp,sobj);
                     }
                     else 
                         incNumIntegrationObjectMap.put(tmp,sobj); 
                 }
                 system.debug('incNumIntegrationObjectMap-->'+incNumIntegrationObjectMap);
                 //Create a list of Remedy Integration handling object to update the status to Awaiting Response when the platform event is posted on bus
                 List<SObject> updateRIHObjStatusList = new List<SObject>();
                 // create a list of remedy outbound platform events
                 List<RemedyOutboundIntegration__e> outboundJSONEventList = new List<RemedyOutboundIntegration__e>();
                 //loop through incNumIntegrationObjectMap keySet to publish one event per incident
                 for(String s : incNumIntegrationObjectMap.keySet()){
                     if(incNumIntegrationObjectMap.get(s).get('OutboundJSONMessage__c') != null) {
                        RemedyOutboundIntegration__e eOutbound = new RemedyOutboundIntegration__e();
                        eOutbound.OutboundJSONMessage__c = (String)incNumIntegrationObjectMap.get(s).get('OutboundJSONMessage__c');
                        outboundJSONEventList.add(eOutbound);
                        incNumIntegrationObjectMap.get(s).put('Status__c','Awaiting Response');
                        incNumIntegrationObjectMap.get(s).put('requestTimeStamp__c',system.now());      
                        updateRIHObjStatusList.add(incNumIntegrationObjectMap.get(s));
                     }
                 }
                 //publish the events on bus
                 if(outboundJSONEventList.size() > 0) {
                     List<Database.SaveResult> peList = EventBus.Publish(outboundJSONEventList);
                     //disable the trigger and update the Status__c to Awaiting Response
                     if(updateRIHObjStatusList.size() > 0){
                         RemedyIntegrationHandler.TriggerDisabled = true;
                         Database.update(updateRIHObjStatusList,false);
                         RemedyIntegrationHandler.TriggerDisabled = false;
                     }
                 }
                 
             }
             //update the incident Management list
             if(incObjList.size() > 0) {
                 IncidentManagementTriggerHandler.TriggerDisabled = true;
                 List<Database.SaveResult> srList = Database.update(incObjList,false);
                 IncidentManagementTriggerHandler.TriggerDisabled = false;
             } 
             system.debug('Incident List-->'+incObjList);
         }
    }
    
    public Map<String,sObject> fetchIncidentMap(Set<String> incNumList, String sObjectName, String key){
        Map<String,sObject> incMpWithoutLock = new Map<String,sObject>();
        for(sobject sObj : database.query('SELECT'+remedyRPAErrorHandlingUtil.prepareFieldSet(SObjectName)+' FROM '+SObjectName+' WHERE Incident_Number__c IN:incNumList')){
            if(sObj.get(key) != null && sObj.get(key) instanceof String){ 
            	incMpWithoutLock.put((String)sObj.get(key),sObj);
            }
        }
        return incMpWithoutLock;
    }
    
    //method to create & return the incident list
    public List<SObject> fetchIncidentList(Set<String> incNumList, Boolean isError, String SObjectName,Map<String,Sobject> successremedyIntegrationHandlingMap, Map<String,sObject> incMapWithoutLock)
    {
        SObjectName = (SObjectName == null || String.IsBlank(SObjectName)) ? 'Incident_Management__c' : SObjectName;
        Schema.SObjectType sobjType = Schema.getGlobalDescribe().get(SObjectName);   
        List<SObject> sObjList = new List<SObject>();
        Set<String> incNumWithLock = new Set<String>();
        RemedyIntegrationHandling__c currentRemedyIntegrationHandlingRecord;
        String ExceptionMessage;
        String currentUserAction;
        List<String> outJSONs = new List<String>();
        Incident_Management__c oldIM;
        Incident_Management__c newIM;
        
        // Preparing Set of Incident Numbers which requires lock : Added By GS
        for(String incNum : incNumList){
            if(incMapWithoutLock.containsKey(incNum)){
                sObjList.add(incMapWithoutLock.get(incNum));
            }
            else{
                incNumWithLock.add(incNum);
            }
        }
        // Ends
        
        system.debug('incMapWithoutLock'+incMapWithoutLock);
        system.debug('incNumWithLock'+incNumWithLock);
        system.debug('incNumWithoutLocksObjList'+sObjList);
        
        //query the Incident Management Object
        if(!incNumWithLock.isEmpty()){
            try{
                sObjList.addAll(database.query('SELECT'+remedyRPAErrorHandlingUtil.prepareFieldSet(SObjectName)+' FROM '+SObjectName+' WHERE Incident_Number__c IN:incNumWithLock' + ' FOR UPDATE'));
            }
            catch(Exception exe){
                system.debug('Exception Occured'+ exe.getMessage());
                GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'incidentManagementController_LC','fetchIncidentList','','',exe.getMessage(), exe.getStackTraceString(),exe, 0);
                sObjList.addAll(database.query('SELECT'+remedyRPAErrorHandlingUtil.prepareFieldSet(SObjectName)+' FROM '+SObjectName+' WHERE Incident_Number__c IN:incNumWithLock' + ' FOR UPDATE'));
            }
        }
        for(SObject so : sObjList)
        {
            currentRemedyIntegrationHandlingRecord = (RemedyIntegrationHandling__c)successremedyIntegrationHandlingMap.get((String)so.get('Name'));
            currentUserAction = (String)so.get('User_Action__c');
            if(String.isNotBlank(currentUserAction) && currentUserAction.equalsIgnoreCase('relateToIncident'))
            {
                if(currentRemedyIntegrationHandlingRecord != Null && currentRemedyIntegrationHandlingRecord.action__c.equalsIgnoreCase(currentUserAction) && currentRemedyIntegrationHandlingRecord.Status__c.equalsIgnoreCase('Success'))
                {
                    if(UserInfo.getName() == 'Automated Process')
                    {
                        so.put('DisableOperatorActions__c', False);
                        so.put('Send_SINI_Request__c', False);
                        so.put('Network_Incidents_For_Linking__c', '');
                        so.put('User_Action__c', 'relateToIncidentCompleted');
                        so.put('SINILinkingExceptionMessage__c','');
                    }
                    if(UserInfo.getName() != 'Automated Process')
                    {
                        so.put('DisableOperatorActions__c', False);
                        so.put('Send_SINI_Request__c', False);
                        so.put('Network_Incidents_For_Linking__c', '');
                        so.put('SINILinkingExceptionMessage__c','');
                    }
                }
                if(currentRemedyIntegrationHandlingRecord != Null && currentRemedyIntegrationHandlingRecord.action__c.equalsIgnoreCase(currentUserAction) && currentRemedyIntegrationHandlingRecord.Status__c.equalsIgnoreCase('Failed'))
                {
                    so.put('DisableOperatorActions__c', False);
                    so.put('Send_SINI_Request__c', False);
                    so.put('Network_Incidents_For_Linking__c', '');
                    if(String.isNotBlank(currentRemedyIntegrationHandlingRecord.InboundJSONMessage__c))
                    {
                        ExceptionMessage = currentRemedyIntegrationHandlingRecord.InboundJSONMessage__c.Substring(currentRemedyIntegrationHandlingRecord.InboundJSONMessage__c.indexof('errorMessage') + 4,currentRemedyIntegrationHandlingRecord.InboundJSONMessage__c.indexof('proposedAction') - 3);
                        so.put('SINILinkingExceptionMessage__c',ExceptionMessage);
                    }
                    else { so.put('SINILinkingExceptionMessage__c','Exception occurred while processing SI NI Linking'); }
                }
            }
            if(UserInfo.getName() != 'Automated Process' && String.isNotBlank(currentUserAction) && currentUserAction.equalsIgnoreCase('nbnHeld') && (boolean)so.get('Send_SINI_Request__c'))
            {
                if(currentRemedyIntegrationHandlingRecord != Null && currentRemedyIntegrationHandlingRecord.action__c.equalsIgnoreCase(currentUserAction) && currentRemedyIntegrationHandlingRecord.Status__c.equalsIgnoreCase('Success'))
                {
                    oldIM = (Incident_Management__c)so;
                    newIM = (Incident_Management__c)so;
                    newIM.User_Action__c = 'relateToIncident';
                    outJSONs.add(SalesforceToRemedyProcessor.buildSalesforceToRemedyJSONPayload(oldIM, newIM));
                    so.put('User_Action__c', 'relateToIncident');
                    so.put('DisableOperatorActions__c', true);  
                }
            }
            so.put('RPA_Error_Occurred__c', isError);
            if((isError && currentRemedyIntegrationHandlingRecord == Null) || (isError && currentRemedyIntegrationHandlingRecord != Null && currentRemedyIntegrationHandlingRecord.action__c.equalsIgnoreCase('nbnHeld')))
            {
                so.put('Awaiting_Current_Incident_Status__c', False);
                so.put('Awaiting_Current_SLA_Status__c', False);
                so.put('RequestAppointmentSLAStatus__c', False); 
            }
        }
        //invoke the utility method that creates a log of outboundJSONs
        if(!outJSONs.isEmpty()){ remedyRPAErrorHandlingUtil.deserializeRPAErrorJSON(outJSONs,'Outbound'); }
        return sObjList;
    }
    
}