/***************************************************************************************************
Class Name:         RemedyMilestoneTriggerHandler
Class Type:         Trigger handler class
Version:            1.0 
Created Date:       28 March 2018
Function:           Link RemedyMilestone__c with SLA based on slaRequestId__c - CUSTSA-11268
Input Parameters:   triggers new, old
Output Parameters:  None
Description:        To be used as a service class
Modification Log:
* Developer          	Date             Description
* --------------------------------------------------------------------------------------------------                 
* Rohit Mathur      	28/03/2018      Created - Version 1.0 - CUSTSA-11268
* Syed Moosa Nazir TN	25/03/2018		Updated the SLA SOQL query where clause from slaRequestId__c field to SLAIdentifier__c field
****************************************************************************************************/
public class RemedyMilestoneTriggerHandler implements ITriggerHandler {

    public static Boolean TriggerDisabled = false;
    public Boolean IsDisabled() {
        return TriggerDisabled;
    }
	/*************************************************************************************************
    Trigger Handler Action Methods:
    *************************************************************************************************/
    public void BeforeInsert(List < SObject > newItems, Map < Id, SObject > newItemMap) {
        LinkMilestoneToSLA(newItems, null, null, null);
    }

    public void BeforeUpdate(List < SObject > newItems, List < SObject > oldItems, Map < Id, SObject > newItemMap, Map < Id, SObject > oldItemMap) {
        LinkMilestoneToSLA(newItems, oldItems, newItemMap, oldItemMap);
    }
    public void BeforeDelete(List < SObject > oldItems, Map < Id, SObject > oldItemMap) {}
    public void AfterInsert(List < SObject > newItems, Map < Id, SObject > newItemMap) {}
    public void AfterUpdate(List < SObject > newItems, List < SObject > oldItems, Map < Id, SObject > newItemMap, Map < Id, SObject > oldItemMap) {}
    public void AfterDelete(List < SObject > oldItems, Map < Id, SObject > oldItemMap) {}
    public void AfterUndelete(List < SObject > oldItems, Map < Id, SObject > oldItemMap) {}
	/*************************************************************************************************
    Business Logic Methods:
    *************************************************************************************************/
    public void LinkMilestoneToSLA(List < SObject > newItems, List < SObject > oldItems, Map < Id, SObject > newItemMap, Map < Id, SObject > oldItemMap) {
		try{
			set <string> slaRequestIdSet = new set <string> ();
			map <string, id> slaRequestIdSlaIdMap = new map <String, id> ();
			if (newItems <> null && !newItems.isEmpty()) {
				for (RemedyMilestone__c mil: (list < RemedyMilestone__c >) newItems) {
					if(String.isNotBlank(mil.slaRequestId__c)) {
						slaRequestIdSet.add(mil.slaRequestId__c);
					}
				}
				if (!slaRequestIdSet.isEmpty()) {
					String SLAQuery = 'Select ' + QueryUtils.getCommaSeparatedFieldForAnObject('SLA__C') +
						' From SLA__C ' +
						' Where SLAIdentifier__c IN : slaRequestIdSet';

					list < SLA__c > slaList = database.query(SLAQuery);
					if (!slaList.isEmpty()) {
						for (SLA__c sla: slaList) {
							slaRequestIdSlaIdMap.put(sla.SLAIdentifier__c, sla.Id);
						}
					}else{
						for (RemedyMilestone__c mil: (list < RemedyMilestone__c > ) newItems) {
							mil.SLA__c = null;
						}
					}
					if(!slaRequestIdSlaIdMap.keySet().isEmpty()) {
						for (RemedyMilestone__c mil: (list < RemedyMilestone__c > ) newItems) {
							if(slaRequestIdSlaIdMap.containsKey(mil.slaRequestId__c) && slaRequestIdSlaIdMap.get(mil.slaRequestId__c) != null) {
								mil.SLA__c = slaRequestIdSlaIdMap.get(mil.slaRequestId__c);
							}
						}
					}
				}
			}
		}
        catch (Exception ex){
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'RemedyMilestoneTriggerHandler','LinkMilestoneToSLA','','',ex.getMessage(), ex.getStackTraceString(),ex, 0);
        }
    }
}