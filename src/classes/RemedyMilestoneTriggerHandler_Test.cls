/***************************************************************************************************
Class Type: Test Class
Version     : 1.0
Function    : provide test coverage for RemedyMilestoneTriggerHandler
Used in     : None
Modification Log :
* Developer                   Date                   Description
  Anshul                     13-06-2018               Added set up method & test methods for corresponding methods in RemedyMilestoneTriggerHandler
* ----------------------------------------------------------------------------
****/

@isTest
public with sharing class RemedyMilestoneTriggerHandler_Test {

    // method to insert test data
    @testSetup static void setupMethod() {
        // Create common test Incidents.
        List<Incident_Management__c> lstIncidents = new List<Incident_Management__c>();
        for(Integer i=0; i<2; i++) {
            lstIncidents.add(
                    new Incident_Management__c(Appointment_Start_Date__c = System.now(), Appointment_End_Date__c = System.now(),
                            Target_Commitment_Date__c = System.now()));
        }
        insert lstIncidents;




        // Create common test SLA__C
        List<SLA__C> lstSLA = new List<SLA__C>();
        SLA__C testSLA1 = new SLA__C();
        testSLA1.Incident__c = lstIncidents.get(0).Id;
        testSLA1.Current_SLA__c = true;
        testSLA1.SLAType__c = 'Incident Resolution Time';
        testSLA1.slaStatus__c = 'In Process';
        testSLA1.SLAIdentifier__c='9999';

        lstSLA.add(testSLA1);



        /*SLA__C testSLA2 = new SLA__C();
        testSLA2.Incident__c = lstIncidents.get(0).Id;
        testSLA2.Current_SLA__c = true;
        testSLA2.SLAType__c = 'Incident Response Time';
        testSLA2.slaStatus__c = 'Warning';
        lstSLA.add(testSLA2);


        SLA__C testSLA3 = new SLA__C();
        testSLA3.Incident__c = lstIncidents.get(1).Id;
        testSLA3.Current_SLA__c = false;
        testSLA3.SLAType__c = 'Incident Response Time';
        testSLA3.slaStatus__c = 'Pending';
        lstSLA.add(testSLA3);*/

        insert lstSLA;

    }

    @isTest static void BeforeInsertTest() {
        list<SLA__C> slaList = [Select Id,Incident__c,Current_SLA__c,SLAType__c,slaStatus__c,SLAIdentifier__c from SLA__C];
        Test.startTest();

        if(slaList.size() > 0 ){
            RemedyMilestone__c mileStoneRecord = new RemedyMilestone__c(milestoneStatus__c='test', RemedyMilestoneId__c='test1234' ,slaRequestId__c=slaList[0].SLAIdentifier__c, dueDate__c=Datetime.now());
            insert mileStoneRecord;
        }
        Test.stopTest();
        // Perform some testing
    }
}