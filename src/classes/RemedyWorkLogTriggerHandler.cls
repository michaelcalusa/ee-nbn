/*------------------------------------------------------------
Author:         Rohit Mathur
Company:        NbnCo
Description:    Handler class for RemedyWorkLogTrigger to meet following requirements
                Case1: Stop creation of worklog if the body of worklog matches with any of the feed items on 
                the related incident management record
Inputs:         
Test Class:     RemedyWorkLogTriggerHandler_Test
History
<Date>          <Authors Name>          <Brief Description of Change>
23/03/2018      Rohit Mathur            Created the trigger
24/05/2018      Syed Moosa Nazir TN     Updated the trigger logic to create chatter post in trigger rather than in process builder and VF Flow.
------------------------------------------------------------*/
public class RemedyWorkLogTriggerHandler implements ITriggerHandler {
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    //Checks to see if the trigger has been disabled either by custom setting or by running code
    public Boolean IsDisabled() {
        return TriggerDisabled;
    }
    public void BeforeInsert(List < SObject > newItems, Map < Id, SObject > newItemMap) {
        blockWorkLogCreation(newItems, null, null, null);
    }
    public void BeforeUpdate(List < SObject > newItems, List < SObject > oldItems, Map < Id, SObject > newItemMap, Map < Id, SObject > oldItemMap) {
        blockWorkLogCreation(newItems, oldItems, newItemMap, oldItemMap);
    }
    public void BeforeDelete(List < SObject > oldItems, Map < Id, SObject > oldItemMap) {}
    public void AfterInsert(List < SObject > newItems, Map < Id, SObject > newItemMap) {
        errorHandlingMethodForEUETChange(newItems, newItemMap);
    }
    public void AfterUpdate(List < SObject > newItems, List < SObject > oldItems, Map < Id, SObject > newItemMap, Map < Id, SObject > oldItemMap) {
        errorHandlingMethodForEUETChange(newItems, newItemMap);
    }
    public void AfterDelete(List < SObject > oldItems, Map < Id, SObject > oldItemMap) {}
    public void AfterUndelete(List < SObject > oldItems, Map < Id, SObject > oldItemMap) {}

    /**********************************************************************************************************************
     **********************************************************************************************************************
     *****************************************     Business Logic Methods    **********************************************
     **********************************************************************************************************************
     **********************************************************************************************************************/
    public void blockWorkLogCreation(List < SObject > newItems, List < SObject > oldItems, Map < Id, SObject > newItemMap, Map < Id, SObject > oldItemMap) {
        Set < Id > incidentManagementIDSet = new Set < Id > ();
        if (newItems <> null && !newItems.isEmpty()) {
            List < FeedItem > listOfFeedItemsToCreate = new List < FeedItem > ();
            for (Remedy_Work_Log__c rwLog: (List < Remedy_Work_Log__c > ) newItems) {
                incidentManagementIDSet.add(rwLog.Incident__c);
            }
            if (!incidentManagementIDSet.isEmpty()) {
                //collect feed item records for the Incidents which we collected from TRIGGER.NEW(Remedy_Work_Log__c) list
                Map < Id, list < FeedItem >> mapOfFeedItemsWithIncidentId = new Map < Id, list < FeedItem >> ();
				//Added the where clause in the for loop as part of Prod Inc - INC000010809119 where the notes will be available in SF if added on diff dates in Remedy.
                for (FeedItem feedItemRecord: [SELECT Id, Body, Visibility, ParentId FROM FeedItem WHERE ParentId IN: incidentManagementIDSet AND createdDate = today]) {
                    if (mapOfFeedItemsWithIncidentId.containsKey(feedItemRecord.ParentId))
                        mapOfFeedItemsWithIncidentId.get(feedItemRecord.ParentId).add(feedItemRecord);
                    else
                        mapOfFeedItemsWithIncidentId.put(feedItemRecord.ParentId, new List < FeedItem > {
                            feedItemRecord
                        });
                }
                for (Remedy_Work_Log__c rwLog: (List < Remedy_Work_Log__c > ) newItems) {
                   if (mapOfFeedItemsWithIncidentId <> null && !mapOfFeedItemsWithIncidentId.isEmpty() &&
                        mapOfFeedItemsWithIncidentId.get(rwLog.Incident__c) <> null && !mapOfFeedItemsWithIncidentId.get(rwLog.Incident__c).isEmpty()) {
                        boolean isWorklogValid = true;
                        for (FeedItem feedRecord: mapOfFeedItemsWithIncidentId.get(rwLog.Incident__c)) {
                            if (String.isNotBlank(rwLog.Note_Details__c) && String.isNotBlank(feedRecord.Body) && equalsNormCaseAndWhitespace(rwLog.Note_Details__c,feedRecord.Body))
                            isWorklogValid = false;
                        }
                        // Create Chatter feed
                        if (isWorklogValid) {
                            if (String.isNotBlank(rwLog.Note_Details__c) && String.isNotBlank(rwLog.Note_Internal_External__c) && String.isNotBlank(rwLog.Added_By__c) && String.isNotBlank(rwLog.Incident__c) && rwLog.Date_Timestamp__c <> null) {
                                if (rwLog.Note_Internal_External__c.equalsIgnoreCase('Public')) {
                                    FeedItem feedItemsToCreate = new FeedItem();
                                    String feedBody = '';
                                    //Due to salesforce feed/chatter body charachter limit
                                    //we need to truncate the body of worklog
                                    if(rwLog.Note_Details__c.length() >= 10000){
                                        feedBody = rwLog.Note_Details__c.substring(0, 9999);
                                    }else{
                                        feedBody = rwLog.Note_Details__c;
                                    }
                                    feedItemsToCreate.Body = feedBody;
                                    feedItemsToCreate.CreatedById = rwLog.CreatedById;
                                    feedItemsToCreate.CreatedDate = rwLog.Date_Timestamp__c;
                                    feedItemsToCreate.NetworkScope = 'AllNetworks';
                                    feedItemsToCreate.ParentId = rwLog.Incident__c;
                                    feedItemsToCreate.Status = 'Published';
                                    feedItemsToCreate.Type = 'TextPost';
                                    feedItemsToCreate.Visibility = 'AllUsers';
                                    feedItemsToCreate.Title = rwLog.Added_By__c;
                                    listOfFeedItemsToCreate.add(feedItemsToCreate);
                                } else if (rwLog.Note_Internal_External__c.equalsIgnoreCase('Internal')) {
                                    FeedItem feedItemsToCreate = new FeedItem();
                                    String feedBody = '';
                                    //Due to salesforce feed/chatter body charachter limit
                                    //we need to truncate the body of worklog
                                    if(rwLog.Note_Details__c.length() >= 10000){
                                        feedBody = rwLog.Note_Details__c.substring(0, 9999);
                                    }else{
                                        feedBody = rwLog.Note_Details__c;
                                    }
                                    feedItemsToCreate.Body = feedBody;
                                    feedItemsToCreate.CreatedById = rwLog.CreatedById;
                                    feedItemsToCreate.CreatedDate = rwLog.Date_Timestamp__c;
                                    feedItemsToCreate.NetworkScope = 'AllNetworks';
                                    feedItemsToCreate.ParentId = rwLog.Incident__c;
                                    feedItemsToCreate.Status = 'Published';
                                    feedItemsToCreate.Type = 'TextPost';
                                    system.debug('RemedyWorkLogTriggerHandler >> blockWorkLogCreation >> Added_By__c : ' + rwLog.Added_By__c);
                                    system.debug('RemedyWorkLogTriggerHandler >> blockWorkLogCreation >> Condition : ' + (String.isNotBlank(rwLog.Added_By__c) && (rwLog.Added_By__c.equalsIgnoreCase('fuseadmin') || rwLog.Added_By__c.equalsIgnoreCase('aomaster'))));
                                    if (String.isNotBlank(rwLog.Added_By__c) && (rwLog.Added_By__c.equalsIgnoreCase('fuseadmin') || rwLog.Added_By__c.equalsIgnoreCase('aomaster'))){
                                        feedItemsToCreate.Title = 'Auto Processing';
                                    }
                                    else{
                                        feedItemsToCreate.Title = rwLog.Added_By__c;
                                    }
                                    feedItemsToCreate.Visibility = 'InternalUsers';
                                    listOfFeedItemsToCreate.add(feedItemsToCreate);
                                }
                            }
                        }
                    } else {
                        if (String.isNotBlank(rwLog.Note_Details__c) && String.isNotBlank(rwLog.Note_Internal_External__c) && String.isNotBlank(rwLog.Added_By__c) && String.isNotBlank(rwLog.Incident__c) && rwLog.Date_Timestamp__c <> null) {
                            // Create Chatter feed
                            if (rwLog.Note_Internal_External__c.equalsIgnoreCase('Public')) {
                                FeedItem feedItemsToCreate = new FeedItem();
                                String feedBody = '';
                                //Due to salesforce feed/chatter body charachter limit
                                //we need to truncate the body of worklog
                                if(rwLog.Note_Details__c.length() >= 10000){
                                    feedBody = rwLog.Note_Details__c.substring(0, 9999);
                                }else{
                                    feedBody = rwLog.Note_Details__c;
                                }
                                feedItemsToCreate.Body = feedBody;
                                feedItemsToCreate.CreatedById = rwLog.CreatedById;
                                feedItemsToCreate.CreatedDate = rwLog.Date_Timestamp__c;
                                feedItemsToCreate.NetworkScope = 'AllNetworks';
                                feedItemsToCreate.ParentId = rwLog.Incident__c;
                                feedItemsToCreate.Status = 'Published';
                                feedItemsToCreate.Type = 'TextPost';
                                feedItemsToCreate.Visibility = 'AllUsers';
                                feedItemsToCreate.Title = rwLog.Added_By__c;
                                listOfFeedItemsToCreate.add(feedItemsToCreate);
                            } else if (rwLog.Note_Internal_External__c.equalsIgnoreCase('Internal')) {
                                FeedItem feedItemsToCreate = new FeedItem();
                                String feedBody = '';
                                //Due to salesforce feed/chatter body charachter limit
                                //we need to truncate the body of worklog
                                if(rwLog.Note_Details__c.length() >= 10000){
                                    feedBody = rwLog.Note_Details__c.substring(0, 9999);
                                }else{
                                    feedBody = rwLog.Note_Details__c;
                                }
                                feedItemsToCreate.Body = feedBody;
                                feedItemsToCreate.CreatedById = rwLog.CreatedById;
                                feedItemsToCreate.CreatedDate = rwLog.Date_Timestamp__c;
                                feedItemsToCreate.NetworkScope = 'AllNetworks';
                                feedItemsToCreate.ParentId = rwLog.Incident__c;
                                feedItemsToCreate.Status = 'Published';
                                feedItemsToCreate.Type = 'TextPost';
                                feedItemsToCreate.Visibility = 'InternalUsers';
                                system.debug('RemedyWorkLogTriggerHandler >> blockWorkLogCreation >> Added_By__c : ' + rwLog.Added_By__c);
                                system.debug('RemedyWorkLogTriggerHandler >> blockWorkLogCreation >> Condition : ' + (String.isNotBlank(rwLog.Added_By__c) && (rwLog.Added_By__c.equalsIgnoreCase('fuseadmin') || rwLog.Added_By__c.equalsIgnoreCase('aomaster'))));
                                if (String.isNotBlank(rwLog.Added_By__c) && (rwLog.Added_By__c.equalsIgnoreCase('fuseadmin') || rwLog.Added_By__c.equalsIgnoreCase('aomaster'))){
                                    feedItemsToCreate.Title = 'Auto Processing';
                                }
                                else{
                                    feedItemsToCreate.Title = rwLog.Added_By__c;
                                }
                                listOfFeedItemsToCreate.add(feedItemsToCreate);
                            }
                        }
                    }
                }
                if (!listOfFeedItemsToCreate.isEmpty()){
                    try{
                        INSERT listOfFeedItemsToCreate;
                        //start of code snippet - added by SV - to update the boolean value Add_Note_Change__c which triggers the auto refresh for Work Log UI component
                        Set<Id> incIdSetForWorkLogs = new Set<Id>();
                        for(feedItem fi : listOfFeedItemsToCreate) {
                            incIdSetForWorkLogs.add(fi.ParentId);
                        }
                        if (!incIdSetForWorkLogs.isEmpty()){
                            List<SObject> sobjList = new List<SObject>();
                            String queryString =  'SELECT ' + QueryUtils.getCommaSeparatedFieldForAnObject('Incident_Management__c') +
                                      ' FROM Incident_Management__c' +
                                      ' WHERE Id IN: incIdSetForWorkLogs';
                            sobjList = Database.query(queryString);
                            for( SObject so : sobjList )
                                so.put('Add_Note_Change__c', (boolean) so.get('Add_Note_Change__c') ? false : true);
                            if(!sobjList.isEmpty()) {
                                IncidentManagementTriggerHandler.TriggerDisabled = true;
                                List<Database.SaveResult> srList = Database.update(sobjList, false);
                                IncidentManagementTriggerHandler.TriggerDisabled = false;
                            }    
                        }
                        // end of code snippet to update the boolean value Add_Note_Change__c
                    }catch(Exception ex){
                          //Added below code as per part of CUSTSA-17093 - Gaurav Tiwari.
                          GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'RemedyWorkLogTriggerHandler','blockWorkLogCreation','','',ex.getMessage(), ex.getStackTraceString(),ex, 0);
                    }
                }
            }
        }
    }
    
    /*  Purpose: This method is used to stop the spinners when the commitment creation fails in Remedy
        Developer: SV
        Created Date: 21/05/18
        JIRA ref#: CUSTSA-15744
    */
    public void errorHandlingMethodForEUETChange(List<SObject> newItems, Map<Id, SObject> newItemMap){
        Set<Id> incidentManagementIDSet = new Set<Id>();
        String errorLabel = System.Label.CommitmentFailureInRemedy;
        String sObjectName = 'Incident_Management__c';
        if (newItems != null && newItems.size() > 0) {
            for (Remedy_Work_Log__c rwLog : (List<Remedy_Work_Log__c>) newItems) {
                if (rwLog.Incident__c != null && rwLog.Note_Details__c != null && errorLabel != null && rwLog.Note_Details__c.equalsIgnoreCase(errorLabel))
                    incidentManagementIDSet.add(rwLog.Incident__c);
            }
            //fetch the incident records and update the boolean values to stop the spinners
            if (incidentManagementIDSet.size() > 0) {
                List<SObject> sobjList = new List<SObject>();
                String queryString =  'SELECT ' + QueryUtils.getCommaSeparatedFieldForAnObject(sObjectName) +
                                      ' FROM ' + sObjectName +
                                      ' WHERE Id IN: incidentManagementIDSet';
                sobjList = Database.query(queryString);
                for (SObject so : sObjList) {
                    so.put('Awaiting_Current_Incident_Status__c', False);
                    so.put('Awaiting_Current_SLA_Status__c', False);
                   // so.put('remedyFailedToCreateCommitment__c', True);
                }
                if (sObjList.size() > 0) {
                    IncidentManagementTriggerHandler.TriggerDisabled = true;
                    List<Database.SaveResult> srList = Database.update(sObjList,false);
                    IncidentManagementTriggerHandler.TriggerDisabled = false;
                } 
            }
        }
    }
    // returns true if the strings are equal after normalizing case and whitespace:
    public static Boolean equalsNormCaseAndWhitespace(String workLogNoteDetails, String feedRecordBody) {
        // before invoking equalsIgnoreCase, replace any contiguous run of whitespace 
        // chars with a single space-char; also trim the start/end of whitespace:
        return workLogNoteDetails == null || feedRecordBody == null ? workLogNoteDetails == feedRecordBody : 
                workLogNoteDetails.replaceAll('\\s+', ' ').trim().equalsIgnoreCase(feedRecordBody.replaceAll('\\s+', ' ').trim());
    }
}