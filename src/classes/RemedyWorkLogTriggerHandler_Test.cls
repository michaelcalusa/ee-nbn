/*------------------------------------------------------------
Author:         Rohit Mathur
<Date>          <Authors Name>          <Brief Description of Change>
23/03/2018      Rohit Mathur            Created the test class
24/05/2018      Syed Moosa Nazir TN     Updated Test class as part of migrating Processbuilder logic to Trigger Handler
------------------------------------------------------------*/
@isTest
private class RemedyWorkLogTriggerHandler_Test {
    
    @testSetup static void setup() {
        
        // Create common test Incidents.
        insert new Incident_Management__c (Incident_Number__c='INC000006920225', recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Incident Management FTTN/B').getRecordTypeId(), Appointment_Start_Date__c=system.now(),Appointment_End_Date__c=system.now(), Target_Commitment_Date__c=system.now(),Reported_Date__c=system.now(),Awaiting_Current_Incident_Status__c=true, Awaiting_Current_SLA_Status__c = true);
        List<Incident_Management__c> lstIncidents = new List<Incident_Management__c>();
        for(Integer i=0; i<2; i++) {
            lstIncidents.add(
                new Incident_Management__c(Appointment_Start_Date__c = System.now(), Appointment_End_Date__c = System.now(),
                                                                Target_Commitment_Date__c = System.now()));
        }
        INSERT lstIncidents;
        List<Remedy_Work_Log__c> listOfWrkLog = new List<Remedy_Work_Log__c>();
        Remedy_Work_Log__c wrkLog = new Remedy_Work_Log__c();
        wrkLog.Added_By__c = 'fuseadmin';
        wrkLog.Date_Timestamp__c = System.now();
        wrkLog.Incident__c = lstIncidents[0].id;
        wrkLog.Note_Details__c = 'Test Notes';
        wrkLog.Note_Internal_External__c = 'Internal';
        wrkLog.worklogIdentifier__c = 'WLG977766667777';
        wrkLog.WorklogType__c = 'GeneralInformation';
        listOfWrkLog.add(wrkLog);
        
        Remedy_Work_Log__c wrkLog2 = new Remedy_Work_Log__c();
        wrkLog2.Added_By__c = 'fuseadmin';
        wrkLog2.Date_Timestamp__c = System.now();
        wrkLog2.Incident__c = lstIncidents[0].id;
        wrkLog2.Note_Details__c = 'Test Notes';
        wrkLog2.Note_Internal_External__c = 'Public';
        wrkLog2.worklogIdentifier__c = 'WLG977766668888';
        wrkLog2.WorklogType__c = 'GeneralInformation';
        listOfWrkLog.add(wrkLog2);
        INSERT listOfWrkLog;
    }

    private static testMethod void InternalWorkLogsFeedItemCreationTest() {
        test.startTest();
            list<Remedy_Work_Log__c> wrLogList = [select id,Note_Details__c from Remedy_Work_Log__c where worklogIdentifier__c ='WLG977766667777' Limit 1];
            for(Remedy_Work_Log__c logs : wrLogList){
                logs.Note_Details__c = 'Test Notes 2';
                logs.Added_By__c = 'Syed';
            }
            UPDATE wrLogList;
        test.stopTest();
    }
    private static testMethod void PublicWorkLogsFeedItemCreationTest() {
        test.startTest();
            list<Remedy_Work_Log__c> wrLogList = [select id,Note_Details__c from Remedy_Work_Log__c where worklogIdentifier__c ='WLG977766667777' Limit 1];
            for(Remedy_Work_Log__c logs : wrLogList){
                logs.Note_Details__c = 'Test Notes 3';
                logs.Note_Internal_External__c = 'Public';
                logs.Added_By__c = 'fuseadmin';
            }
            UPDATE wrLogList;
            for(Remedy_Work_Log__c logs : wrLogList){
                logs.Note_Details__c = 'Test Notes 4';
                logs.Note_Internal_External__c = 'Public';
                logs.Added_By__c = 'Syed';
            }
            UPDATE wrLogList;
            DELETE wrLogList;
            UNDELETE wrLogList;
            
        test.stopTest();
    }
    
    
    static testMethod void testMethod_workLog_RemedyFailure(){
        Remedy_Work_Log__c rh = new Remedy_Work_Log__c();
        rh.Incident__c = [SELECT Id, Awaiting_Current_SLA_Status__c  FROM Incident_Management__c][0].Id;
        rh.Note_Details__c = 'Target Commitment Date/Time (TCD) is not available because of No Slot Available / Technical issues. Please re trigger Commitment';
        System.assertEquals(True, [SELECT Id, Awaiting_Current_SLA_Status__c  FROM Incident_Management__c][0].Awaiting_Current_SLA_Status__c,'Before Work Log Insert');
        insert rh;
        System.assertEquals(False, [SELECT Id, Awaiting_Current_SLA_Status__c  FROM Incident_Management__c][0].Awaiting_Current_SLA_Status__c,'After Work Log Insert');
    }
}