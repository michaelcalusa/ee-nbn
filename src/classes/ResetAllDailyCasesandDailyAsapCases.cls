global class ResetAllDailyCasesandDailyAsapCases Implements Schedulable
            {
                       global void execute(SchedulableContext sc)
                        {
                                    Reset();
                        }
                       
                        public void Reset()  {
                        
                        List<Case_Assignment__c> AllCaseAssignments = [select id,Num_Daily_Cases__c,Num_Daily_ASAP_Cases__c from Case_Assignment__c];
                        
                        for(Case_Assignment__c individualAssignment: AllCaseAssignments ){
                        
                          individualAssignment.Num_Daily_Cases__c = 0;
                          individualAssignment.Num_Daily_ASAP_Cases__c = 0;
                        
                        }
                         
                         Update AllCaseAssignments  ;     
                                   
                        }
            }