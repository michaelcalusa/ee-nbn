@isTest
public class ResetAllDailyCasesandDailyAsapCases_Test {
    static testmethod void test(){
        Test.StartTest();
        List<case_Assignment__C> caseAssgnlst = new List<case_Assignment__C>();
        case_Assignment__C caseAssgnObj = new case_Assignment__C();
        caseAssgnObj.Available__c=true;
        caseAssgnObj.User__c = userinfo.getUserId();
        insert caseAssgnObj;
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('ResetAllDailyCasesandDailyAsapCases',CRON_EXP, new ResetAllDailyCasesandDailyAsapCases()); 
        caseAssgnObj.Available__c=false;
        update caseAssgnObj;
        caseAssgnObj.Available__c=true;
        update caseAssgnObj;
        Test.stopTest();
    }
}