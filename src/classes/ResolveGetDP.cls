/***************************************************************************************************
Class Name          : ResolveGetDP
Test Class          : ResolveGetDP_Test
Version             : 1.0 
Created Date        : 09-Sep-2018
Author              : Dilip/Sidharth
Description         : Gets DP name from P6 System and updates the case. This class is created for Project Resolve(Automating Case DP allocation)
Input Parameters    : Case List
Output Parameters   :  
Modification Log    :

* Developer             Date            Description
* ------------------------------------------------------------------------------------------------   


****************************************************************************************************/ 

Global Class ResolveGetDP 
{
    Static Resolve_Auto_Allocation_Failure_Reason__c identifierList = Resolve_Auto_Allocation_Failure_Reason__c.getOrgDefaults();
    Static map<String, ResolveP6Integration.ObjectInfo> p6ObjMap;
    Static String techType, pgmDelObjectId;
    Static Case cs;
    Static List<Case_DP_detail__c> caseDPdetailsList = new List<Case_DP_detail__c>();
    Static Map <String, Set<String>> activityCodeMap = new Map<String, Set<String>>();
    
    @InvocableMethod
    public static void getDP(list<Case> caseList) {
        System.debug('Inside - Class-ResolveGetDP > Method-getDP'); 
        if (caseList != null && caseList.size()>0) {
            getDPP6(json.serialize(caseList),false);
        }    
    }
    
    @future(callout=true)
    public static void getDPP6(string caseList,boolean retrieveOnly) {
        List<Case> csList = (List<Case>)JSON.deserialize(caseList, List<case>.class);
        cs = [Select Id,CreatedDate,Phase__c,Category__c,Sub_Category__c, Site__r.SAM__c,Site__r.Technology_Type__c,Site__r.Rollout_Type__c, site__r.State_Territory_Code__c From Case Where Id =: csList[0].Id];
        System.debug('Get DP for case> '+cs);
        String cookie, objectId, projectId, projCodObjId, p6ProgramDeliveryCode, p6ActivityCodes; //techType,
        List<ResolveP6Integration.ActivityInfo> actInfoList;  
        List<ResolveP6Integration.ObjectInfo> objectList;
        List<ResolveP6Integration.ProjCodeResponse> projCodeRspList;
        List<Resolve_DP_Mapping_National__mdt> dpDesignList = [SELECT Id, P6_DP_Name__c, Salesforce_DP_Name__c, Technology_Type__c, Build_DP__c, State_wise_Assignment__c, Label FROM Resolve_DP_Mapping_National__mdt where Build_DP__c = false];
        List<Resolve_DP_Mapping_National__mdt> dpBuildList = [SELECT Id, P6_DP_Name__c, Salesforce_DP_Name__c, Technology_Type__c, Build_DP__c, State_wise_Assignment__c, Label FROM Resolve_DP_Mapping_National__mdt where Build_DP__c = true];
        List<Resolve_DP_State_wise_Assignment__mdt> sadpDesignList = [SELECT Id, Parent_DP_Name__c, Salesforce_DP_Name__c, Technology_Type__c, State__c, Label FROM Resolve_DP_State_wise_Assignment__mdt where Technology_Type__c = '']; 
        List<Resolve_DP_State_wise_Assignment__mdt> sadpBuildList = [SELECT Id, Parent_DP_Name__c, Salesforce_DP_Name__c, Technology_Type__c, State__c, Label FROM Resolve_DP_State_wise_Assignment__mdt where Technology_Type__c != '']; 
        List<Resolve_P6_Activity_Code__mdt> p6ActivityCodeList = [SELECT Id,Label,Build_Start__c,Build_WR__c,Design_Approved__c,Design_Start__c,PC_Met__c,Program_Delivery__c,Ready_for_service__c FROM Resolve_P6_Activity_Code__mdt]; 
        p6ObjMap = new map<String, ResolveP6Integration.ObjectInfo>();
        Set<String> buildStartActivitycodeSet = new Set<String>();
        Set<String> buildWRActivitycodeSet = new Set<String>();
        Set<String> rfsActivitycodeSet = new Set<String>();
        Set<String> designStartcodeSet = new Set<String>();
        Set<String> designApprovedcodeSet = new Set<String>();
        Set<String> pcMetcodeSet = new Set<String>();
        boolean buildDPFound = false, designDPFound = false;
        boolean inScopeForAutoAllocation;
        
        try 
        {
           inScopeForAutoAllocation = ResolveGetDPHelper.getAutoAllocationScope(cs); // check Case's Phase-Category-Subcategory is in scope for auto allocation
           System.debug('Case in Scope for Auto allocation>>>'+inScopeForAutoAllocation);
           cookie = ResolveP6Integration.getAuth();
           projectId =  '\''+'%'+cs.Site__r.SAM__c+'\'';  // '%SAMID' (To get all P6 projects matching Case SAM Id)
           System.debug('PROJECTID>>>>'+projectId);
            if(String.isNotBlank(cookie) && String.isNotBlank(projectId)) {
               objectList =  ResolveP6Integration.getObjectId(cookie, projectId);
               System.debug('P6 Project Object List>>>>'+objectList);
               for (ResolveP6Integration.ObjectInfo p6Obj :objectList) {
                    p6ObjMap.put(p6Obj.objName, p6Obj);
               }
               if (!p6ObjMap.isEmpty()) {
                   for (String objName : p6ObjMap.keySet()) {
                       if (String.isBlank(objectId)) {
                           objectId = '\''+p6ObjMap.get(objName).objectId+'\''; // 'ObjectId'
                       }
                       else {
                           objectId = objectId+ ','+'\''+p6ObjMap.get(objName).objectId+'\''; // 'ObjectId1','ObjectId2'
                       }
                   } 
               }   
               System.debug('OBJECTID>>>'+objectId);
               
               if(string.isNotBlank(objectId)) {
                    p6ProgramDeliveryCode = Label.Resolve_P6_Program_Delivery_code; //P6 Program Delivery code 4707
                    if((cs.Phase__c == 'Planning' || cs.Phase__c == 'Information')) {
                        projCodObjId = Label.Resolve_Design_Code; // P6 Design Contractor Code 1528
                    }
                    else {
                             projCodObjId = Label.Resolve_Build_Code; // P6 Build Contractor Code 296
                    }
                    projCodeRspList = ResolveP6Integration.getDPName(cookie, objectId, projCodObjId, p6ProgramDeliveryCode); // Get P6 DP details, Program Delivery details
                    System.debug('projCodeRspList>>>>'+projCodeRspList);
                   //From metadata 'Resolve_P6_Activity_Code_mdt', get P6 Activity Codes matching 'Program Delivery' 
                    for (ResolveP6Integration.ProjCodeResponse p6Det :projCodeRspList) {
                        for (Resolve_P6_Activity_Code__mdt sfActivityCode : p6ActivityCodeList) {
                            buildStartActivitycodeSet.add(sfActivityCode.Build_Start__c);
                            buildWRActivitycodeSet.add(sfActivityCode.Build_WR__c);
                            rfsActivitycodeSet.add(sfActivityCode.Ready_for_service__c);
                            designStartcodeSet.add(sfActivityCode.Design_Start__c);
                            designApprovedcodeSet.add(sfActivityCode.Design_Approved__c);
                            pcMetcodeSet.add(sfActivityCode.PC_Met__c);
                            if (p6Det.projCodeObjId == Label.Resolve_P6_Program_Delivery_code && (p6Det.projCodeValue).contains(sfActivityCode.Program_Delivery__c)) { // Get Program delivery record(Code4707) matching in Resolve_P6_Activity_Code__mdt
                                if (String.isBlank(p6ActivityCodes)) {
                                    p6ActivityCodes = '\''+sfActivityCode.Build_Start__c+'\''+', \''+sfActivityCode.Build_WR__c+'\''+', \''+sfActivityCode.Ready_for_service__c+'\''+', \''+sfActivityCode.Design_Start__c+'\''+', \''+sfActivityCode.Design_Approved__c+'\''+', \''+sfActivityCode.PC_Met__c+'\''; //'ActivityCode1', 'ActivityCode2'
                                    pgmDelObjectId =  '\''+p6Det.objectId+'\'';
                                }
                                else { 
                                        pgmDelObjectId = pgmDelObjectId + ','+' \''+p6Det.objectId+'\'';
                                        if (String.isNotBlank(sfActivityCode.Build_Start__c) && !p6ActivityCodes.contains(sfActivityCode.Build_Start__c)) {
                                                p6ActivityCodes = p6ActivityCodes + ','+' \''+sfActivityCode.Build_Start__c+'\'';  // if Build Start Code not available in 'p6ActivityCodes' then add it. This is to remove duplicate codes.
                                        } 
                                        if (String.isNotBlank(sfActivityCode.Build_WR__c) && !p6ActivityCodes.contains(sfActivityCode.Build_WR__c)) {
                                                 p6ActivityCodes = p6ActivityCodes + ','+'\''+sfActivityCode.Build_WR__c+'\''; 
                                        }
                                        if (String.isNotBlank(sfActivityCode.Ready_for_service__c) && !p6ActivityCodes.contains(sfActivityCode.Ready_for_service__c)) {
                                                 p6ActivityCodes = p6ActivityCodes + ','+'\''+sfActivityCode.Ready_for_service__c+'\''; 
                                        }
                                        if (String.isNotBlank(sfActivityCode.Design_Start__c) && !p6ActivityCodes.contains(sfActivityCode.Design_Start__c)) {
                                                    p6ActivityCodes = p6ActivityCodes + ','+' \''+sfActivityCode.Design_Start__c+'\'';  
                                        }
                                        if (String.isNotBlank(sfActivityCode.Design_Approved__c) && !p6ActivityCodes.contains(sfActivityCode.Design_Approved__c)) {
                                                    p6ActivityCodes = p6ActivityCodes + ','+' \''+sfActivityCode.Design_Approved__c+'\'';  
                                        }
                                        if (String.isNotBlank(sfActivityCode.PC_Met__c) && !p6ActivityCodes.contains(sfActivityCode.PC_Met__c)) {
                                                  p6ActivityCodes = p6ActivityCodes + ','+' \''+sfActivityCode.PC_Met__c+'\'';  
                                        }
                                }
                            }
                        }
                        System.debug('P6 ACTIVITY CODES for Object>>>'+p6Det.objectId+' are '+p6ActivityCodes);
                    }
                    activityCodeMap.put('BuildStart', buildStartActivitycodeSet);
                    activityCodeMap.put('BuildWR', buildWRActivitycodeSet);
                    activityCodeMap.put('RFSActivity', rfsActivitycodeSet);
                    activityCodeMap.put('DesignStart', designStartcodeSet);
                    activityCodeMap.put('DesignApproved', designApprovedcodeSet);
                    activityCodeMap.put('PCMet', pcMetcodeSet);
                    System.debug('activityCodeMap>>>'+activityCodeMap); 
                    System.debug('pgmDelObjectId>>'+pgmDelObjectId+'P6 ACTIVITY CODES>>>'+p6ActivityCodes);
                    actInfoList =  ResolveP6Integration.getActivityDetails(cookie, pgmDelObjectId, p6ActivityCodes);
                    System.debug('ACTIONINFOLIST>>>>'+actInfoList);
                    if(actInfoList.size() == 0)
                    {
                        System.debug('Null RFS Date from P6');                  
                        cs.Auto_Allocation_Failure_Reason__c = identifierList.RFS_date_Not_Found_In_P6__c;
                    }
                    else if(actInfoList.size() > 0) {
                        if (inScopeForAutoAllocation) {
                            ResolveGetDPHelper.AutoAllocDPInfo autoAllocDPInfo = ResolveGetDPHelper.getAutoAllocDPdetails(actInfoList, projCodeRspList,p6ObjMap,activityCodeMap, cs, pgmDelObjectId); 
                            System.debug('autoAllocDPInfo>>>'+autoAllocDPInfo);
                            if (autoAllocDPInfo.autoAllocRFSdate != null) { 
                                String RFSDate_From_P6= autoAllocDPInfo.autoAllocRFSdate;
                                if (RFSDate_From_P6 != null) {
                                    Date rFSDate = Date.valueof(RFSDate_From_P6);
                                    System.debug('Case CreatedDate - '+cs.CreatedDate+' RFS Date from P6 '+rFSDate);
                                    if (cs.CreatedDate < rFSDate ) 
                                    {  
                                        System.debug('Case CreatedDate '+cs.CreatedDate+' is before RFS Date '+rFSDate);
                                        if((cs.Phase__c == 'Planning' || cs.Phase__c == 'Information') && (cs.Category__c.contains('HFC') || cs.Category__c.contains('HDA/Foxtrot'))) {
                                            cs.DP_Team__c = 'Telstra (Design)';
                                        }
                                        else {
                                            if(projCodeRspList.size() > 0) {
                                                string p6DPName = autoAllocDPInfo.autoAllocDPName;
                                                System.debug('p6DPName>>>'+p6DPName);
                                                if((cs.Phase__c == 'Planning' || cs.Phase__c == 'Information')) {
                                                    for(Resolve_DP_Mapping_National__mdt dpDesign: dpDesignList) {
                                                        if(dpDesign.P6_DP_Name__c == p6DPName) {
                                                            if(!dpDesign.State_wise_Assignment__c) {
                                                                designDPFound = true;
                                                                cs.DP_Team__c = dpDesign.Salesforce_DP_Name__c;
                                                            }
                                                            else if(String.isNotBlank(cs.Site__r.State_Territory_Code__c)){
                                                                for(Resolve_DP_State_wise_Assignment__mdt saDP : sadpDesignList) {
                                                                    if(saDP.Parent_DP_Name__c == dpDesign.P6_DP_Name__c && cs.Site__r.State_Territory_Code__c == saDP.State__c ) {
                                                                        designDPFound = true;
                                                                        cs.DP_Team__c = saDP.Salesforce_DP_Name__c;
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                designDPFound = false;
                                                            }
                                                            break;
                                                        }
                                                    }
                                                    if(!designDPFound ) {                                        
                                                        cs.Auto_Allocation_Failure_Reason__c = identifierList.DP_Queue_Not_Found__c;
                                                        designDPFound = false;
                                                    }
                                                }
                                                else if((cs.Phase__c == 'Build')) {
                                                    for(Resolve_DP_Mapping_National__mdt dpBuild: dpBuildList) {
                                                        if(dpBuild.P6_DP_Name__c == p6DPName && String.isBlank(dpBuild.Technology_Type__c)) {                               
                                                            if(!dpBuild.State_wise_Assignment__c) {
                                                                buildDPFound = true;
                                                                cs.DP_Team__c = dpBuild.Salesforce_DP_Name__c;
                                                            }
                                                            else if(String.isNotBlank(cs.Site__r.State_Territory_Code__c)){
                                                                for(Resolve_DP_State_wise_Assignment__mdt saDP : sadpBuildList) {
                                                                    if(saDP.Parent_DP_Name__c == dpBuild.P6_DP_Name__c && cs.Site__r.State_Territory_Code__c == saDP.State__c ) {
                                                                        buildDPFound = true;
                                                                        cs.DP_Team__c = saDP.Salesforce_DP_Name__c;
                                                                    }
                                                                }
                                                            }
                                                            else {
                                                                     buildDPFound = false;
                                                            }
                                                            break;
                                                        }
                                                        else if(dpBuild.P6_DP_Name__c == p6DPName && String.isNotBlank(dpBuild.Technology_Type__c) && dpBuild.Technology_Type__c == autoAllocDPInfo.autoAllocP6ObjTechType) {
                                                            if(!dpBuild.State_wise_Assignment__c) {
                                                                buildDPFound = true;
                                                                cs.DP_Team__c = dpBuild.Salesforce_DP_Name__c;
                                                            }
                                                            else if(String.isNotBlank(cs.Site__r.State_Territory_Code__c)){
                                                                for(Resolve_DP_State_wise_Assignment__mdt saDP : sadpBuildList) {
                                                                    if(saDP.Parent_DP_Name__c == dpBuild.P6_DP_Name__c && cs.Site__r.State_Territory_Code__c == saDP.State__c &&  saDP.Technology_Type__c == autoAllocDPInfo.autoAllocP6ObjTechType) {
                                                                        buildDPFound = true;
                                                                        cs.DP_Team__c = saDP.Salesforce_DP_Name__c;
                                                                    }
                                                                }
                                                            }
                                                            else {
                                                                    buildDPFound = false;
                                                            }
                                                            break;
                                                        }
                                                    }
                                                    if(!buildDPFound) {                                        
                                                        cs.Auto_Allocation_Failure_Reason__c = identifierList.DP_Queue_Not_Found__c;                                        
                                                        buildDPFound = false;
                                                    }
                                                }
                                                
                                            }
                                            else if(projCodeRspList.size() == 0) {
                                                    cs.Auto_Allocation_Failure_Reason__c = identifierList.DP_Not_Found__c;
                                            } 
                                            else {
                                                    cs.Auto_Allocation_Failure_Reason__c = identifierList.Multipls_DPs_Found__c;
                                            }
                                       }
                                    }
                                    else { 
                                        cs.Auto_Allocation_Failure_Reason__c = identifierList.Complaint_date_falls_on_after_RFS_date__c;                        
                                    }
                                }
                                else if (autoAllocDPInfo.autoAllocRFSdate == null) {
                                        System.debug('Null RFS Date from P6');  
                                        cs.Auto_Allocation_Failure_Reason__c = identifierList.RFS_date_Not_Found_In_P6__c;
                                }
                                else {
                                    cs.Auto_Allocation_Failure_Reason__c = identifierList.Integration_Error__c;
                                }
                            }
                            else {
                                   cs.Auto_Allocation_Failure_Reason__c = identifierList.Auto_allocation_attempted_but_failed__c;
                            }
                        }
                        caseDPdetailsList = ResolveGetDPHelper.getP6DPdetails(actInfoList, projCodeRspList, activityCodeMap, cs);
                        System.debug('caseDPdetailsList>>>>>'+caseDPdetailsList);
                    }
                }
                else
                {
                    cs.Auto_Allocation_Failure_Reason__c = identifierList.Project_not_found_in_P6__c;
                }
            }
            else {
                cs.Auto_Allocation_Failure_Reason__c = identifierList.Integration_Error__c;
            }
            
            ResolveP6Integration.logout(cookie);
            if(!retrieveOnly){
                update cs;
            }
            ResolveGetDPHelper.updateCaseDPListOnCase(caseDPdetailsList, cs);
        } 
        catch (exception ex) {        
            ResolveP6Integration.logout(cookie);
            cs.Auto_Allocation_Failure_Reason__c = identifierList.Default__c;            
            System.debug('Exception Occured '+ex);
            GlobalUtility.logMessage('Error', 'ResolveGetDP', 'getDPP6', ' ', '', '', cs.Id, ex, 0);
            update cs;  
        }  
    }
    
    webservice static String getDPFromCaseButton(String caseId) {
        System.debug('Inside - Class-getDPFromCaseButton> '+caseId); 
        String userProfile = getLoggedInUserProfile();
        if (userProfile=='System Administrator' ||  userProfile=='NBN SME User' || userProfile=='NBN Contact Centre Operations User') { 
            Case caseDPFromButton = [Select Id,CreatedDate,Phase__c,Category__c,Sub_Category__c, Site__r.SAM__c,Site__r.Technology_Type__c,Site__r.Rollout_Type__c, site__r.State_Territory_Code__c From Case Where Id =:caseId];
            if (caseDPFromButton.Site__c != null && (caseDPFromButton.Site__r.SAM__c != null && caseDPFromButton.Site__r.Rollout_Type__c != null && caseDPFromButton.Site__r.Technology_Type__c != null)) {
                list<Case> caseDPFromButtonList= new list<Case>();
                caseDPFromButtonList.add(caseDPFromButton);
                getDPP6(json.serialize(caseDPFromButtonList),true); // Fetch P6 details
                return 'SUCCESS';
            }
            else {
                    return 'MandInfoReqd';
            }
        }
        else {
                return 'InvalidProfile';
        }
    } 
    
    @AuraEnabled
     public static Case getCaseDetails(String caseId) { 
        System.debug('Inside method getCaseDetails> '+caseId);
        Case cs = [Select Id,CreatedDate,Phase__c,Category__c,Sub_Category__c, Site__r.Name,  Site__r.Site_Address__c, Site__r.SAM__c,Site__r.Technology_Type__c,Site__r.Rollout_Type__c, site__r.State_Territory_Code__c From Case Where Id =:Id.valueof(caseId)];
        return cs;
    }
    
    @AuraEnabled
     public static String getDPFromCaseButtonLex(Case cse) { 
        System.debug('Inside method getDPFromCaseButtonLex> '+cse);
        String userProfile;
        userProfile = getLoggedInUserProfile();
        if (userProfile=='System Administrator' ||  userProfile=='NBN SME User' || userProfile=='NBN Contact Centre Operations User') { 
            list<Case> caseDPFromButtonLexList = new list<Case>();
            caseDPFromButtonLexList.add(cse);
            getDPP6(json.serialize(caseDPFromButtonLexList),true);
            return 'SUCCESS';
        }
        else {
                return 'InvalidProfile';
        }
    }
    
    public static String getLoggedInUserProfile() { 
        String pname;
        List<Profile> uProfileList = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId()];
        pname = uProfileList[0].Name;
        return pname;
    }
    
}