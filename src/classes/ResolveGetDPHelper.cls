/***************************************************************************************************
Class Name          : ResolveGetDPHelper 
Test Class          : ResolveGetDP_Test
Version             : 1.0 
Created Date        : 08-Mar-2019
Author              : Sidharth
Description         : Helper class of ResolveGetDP
Input Parameters    : 
Output Parameters   :  
Modification Log    :

* Developer             Date            Description
* ------------------------------------------------------------------------------------------------   
Sidharth               08/03/2019       Helper class of ResolveGetDP

****************************************************************************************************/ 

public Class ResolveGetDPHelper {

    
    public static boolean getAutoAllocationScope(Case cs) {
        System.debug('Insids Method- getAutoAllocationScope>>> '+cs);
        boolean caseInScope= false;
        for (Case_Assignment_Matrix__c cas : [Select Id,Phase__c,Category__c,Sub_Category__c,Is_eligible_for_DP_routing__c From Case_Assignment_Matrix__c]) {
            if (cs.Phase__c==cas.Phase__c && cs.Category__c==cas.Category__c && cs.Sub_Category__c==cas.Sub_Category__c) {
                caseInScope = cas.Is_eligible_for_DP_routing__c;
            }
        }
        return caseInScope;
    }
    
    public static AutoAllocDPInfo getAutoAllocDPdetails(list<ResolveP6Integration.ActivityInfo> actInfoList, list<ResolveP6Integration.ProjCodeResponse> prjDPInfoList, map<String, ResolveP6Integration.ObjectInfo> p6ObjMap, Map <String, Set<String>> activityCodeMap, Case cs, String pgmDelObjectId) { 
        System.debug('Insids Method- getAutoAllocDPdetails>>> ActivityInfo'+actInfoList);
        System.debug('Insids Method- getAutoAllocDPdetails>>> Project DP Info'+prjDPInfoList);
        List<Resolve_P6_DP_Search_Order__mdt> rDPSearchOrderList;
        AutoAllocDPInfo aainfo = new AutoAllocDPInfo();
        boolean p6ObjFound = false;
        String objId,p6OrderTechType, techType;
       
        p6OrderTechType = getCaseTechType(cs.Category__c);
        rDPSearchOrderList = [SELECT Id, SF_Techtype__c, P6_Search_Order__c, P6_SAM_Id_Prefix__c FROM Resolve_P6_DP_Search_Order__mdt Where SF_Techtype__c LIKE :p6OrderTechType Order By P6_Search_Order__c ASC];
        System.debug('rDPSearchOrderList>>>>'+rDPSearchOrderList);
        for (Resolve_P6_DP_Search_Order__mdt dpSerOrd : rDPSearchOrderList) {
            for (ResolveP6Integration.ObjectInfo p6Obj : p6ObjMap.values()) {
                if ((p6Obj.objName).contains(dpSerOrd.P6_SAM_Id_Prefix__c) && pgmDelObjectId.Contains(p6Obj.objectId) ) { //
                    objId = p6Obj.objectId;
                    p6ObjFound = true;
                    if((p6Obj.objName).contains('FTTB') || (p6Obj.objName).contains('Fibre') || (p6Obj.objName).contains('FTTN') || (p6Obj.objName).contains('FTTX')) {
                       techType = 'FTTX';
                    }   
                    else if((p6Obj.objName).contains('FTTC')) {
                            techType = 'FTTC'; 
                    }     
                    else if((p6Obj.objName).contains('FTTP')) {
                            techType = 'FTTP';
                    }        
                    else if((p6Obj.objName).contains('HFC') || (p6Obj.objName).contains('HDA/Foxtrot')) { 
                            techType = 'HFC';
                    }
                    break;
                }
            } 
            if (p6ObjFound) {
                break;     
            }    
        }
        System.debug('Matched objId>>>'+objId+' techType>>>'+techType);
        if (String.isNotBlank(objId)) {
            for (ResolveP6Integration.ActivityInfo ai :actInfoList) {
                if (ai.projectObjectId == objId && (activityCodeMap.get('RFSActivity')).contains(ai.activityId)) {
                    aainfo.autoAllocRFSdate = ai.finishDate;
                    break;
                }   
            }
            for (ResolveP6Integration.ProjCodeResponse pcr : prjDPInfoList) { 
                if (pcr.objectId == objId && pcr.projCodeObjId != Label.Resolve_P6_Program_Delivery_code) {
                    aainfo.autoAllocDPName = pcr.projCodeValue;
                    break;
                }  
            }
            aainfo.autoAllocP6ObjTechType = techType;
        }
        return aainfo;
    }  
    
    public static List<Case_DP_detail__c> getP6DPdetails(list<ResolveP6Integration.ActivityInfo> actInfoList, list<ResolveP6Integration.ProjCodeResponse> projCodeRspList, Map<String, Set<String>> activityCodeMap, Case cs ) { 
        System.debug('Insids Method- getP6DPdetails>>> ActivityInfo'+actInfoList);
        System.debug('Insids Method- getP6DPdetails>>> Project DP Info'+projCodeRspList);
        Map<String, Case_DP_detail__c> caseDPdetailMap = new Map<String, Case_DP_detail__c>();
        List<Case_DP_detail__c> dpDetailsList = new list<Case_DP_detail__c>();
        for (ResolveP6Integration.ActivityInfo ai : actInfoList) {
            Case_DP_detail__c cdpd = new Case_DP_detail__c();
            if (!caseDPdetailMap.containsKey(ai.projectObjectId)) { // display all activities related to a P6 ProjectId on Case
                cdpd.Case__c = cs.Id;
                cdpd.SAM_Id__c = cs.Site__r.SAM__c;
                cdpd.P6_Project_ID__c = ai.projectId;
                cdpd.P6_ObjectId__c = ai.projectObjectId;
                if ((activityCodeMap.get('BuildStart')).contains(ai.activityId) && String.isNotBlank(ai.startDate)) { 
                    cdpd.Build_Start__c = Date.valueof(ai.startDate);
                }
                if ((activityCodeMap.get('BuildWR')).contains(ai.activityId) && String.isNotBlank(ai.startDate)) { 
                    cdpd.Build_WR__c = Date.valueof(ai.startDate);
                }
                if ((activityCodeMap.get('RFSActivity')).contains(ai.activityId) && String.isNotBlank(ai.finishDate)) {     
                    cdpd.Ready_for_Service__c = Date.valueof(ai.finishDate);
                }
                if ((activityCodeMap.get('DesignStart')).contains(ai.activityId) && String.isNotBlank(ai.startDate)) {     
                    cdpd.Design_Start__c = Date.valueof(ai.startDate);
                }
                if ((activityCodeMap.get('DesignApproved')).contains(ai.activityId) && String.isNotBlank(ai.startDate)) { 
                    cdpd.Design_Approved__c = Date.valueof(ai.startDate);
                }
                if ((activityCodeMap.get('PCMet')).contains(ai.activityId) && String.isNotBlank(ai.startDate)) {
                    cdpd.PC_Met__c = Date.valueof(ai.startDate);
                }
                for (ResolveP6Integration.ProjCodeResponse pr: projCodeRspList) {
                    if (pr.objectId == ai.projectObjectId && String.isNotBlank(pr.projCodeValue) && pr.projCodeObjId == Label.Resolve_P6_Program_Delivery_code) {
                        cdpd.Program__c = pr.projCodeValue;
                    }
                    else if (pr.objectId == ai.projectObjectId && String.isNotBlank(pr.projCodeValue) && pr.projCodeObjId != Label.Resolve_P6_Program_Delivery_code) {
                        if (pr.projCodeObjId == Label.Resolve_Build_Code) {
                            cdpd.Build_Contractor__c = pr.projCodeValue;
                        }
                        else if (pr.projCodeObjId == Label.Resolve_Design_Code) {
                                cdpd.Design_Contractor__c = pr.projCodeValue;
                        }
                    }
                }
            }
            else {
                    cdpd = caseDPdetailMap.get(ai.projectObjectId);
                    if ((activityCodeMap.get('BuildStart')).contains(ai.activityId) && String.isNotBlank(ai.startDate)) { 
                        cdpd.Build_Start__c = Date.valueof(ai.startDate);
                    }
                    if ((activityCodeMap.get('BuildWR')).contains(ai.activityId) && String.isNotBlank(ai.startDate)) { 
                        cdpd.Build_WR__c = Date.valueof(ai.startDate);
                    }
                    if ((activityCodeMap.get('RFSActivity')).contains(ai.activityId) && String.isNotBlank(ai.finishDate)) { 
                        cdpd.Ready_for_Service__c = Date.valueof(ai.finishDate);
                    }
                    if ((activityCodeMap.get('DesignStart')).contains(ai.activityId) && String.isNotBlank(ai.startDate)) { 
                        cdpd.Design_Start__c = Date.valueof(ai.startDate);
                    }
                    if ((activityCodeMap.get('DesignApproved')).contains(ai.activityId) && String.isNotBlank(ai.startDate)) { 
                        cdpd.Design_Approved__c = Date.valueof(ai.startDate);
                    }
                    if ((activityCodeMap.get('PCMet')).contains(ai.activityId) && String.isNotBlank(ai.startDate)) { 
                        cdpd.PC_Met__c = Date.valueof(ai.startDate);
                    }
           }
           caseDPdetailMap.put(ai.projectObjectId, cdpd);
           System.debug('P6 record to be displayed>>>'+caseDPdetailMap);
        }
        dpDetailsList = caseDPdetailMap.values();
        System.debug('P6 Detail Lsit to be displayed>>>'+dpDetailsList);
        return dpDetailsList;
    }
    
    public static void updateCaseDPListOnCase(list<Case_DP_detail__c> caseDPdetailsListOnCase, Case cs ) { 
       System.debug('Insids Method- updateCaseDPListOnCase>>>'+caseDPdetailsListOnCase);
       list<Case_DP_detail__c> existingDPdetailsOnCaseList = new List<Case_DP_detail__c>();
       list<Case_DP_detail__c> caseDPDetOnCaseInsertList = new List<Case_DP_detail__c>();
       list<Case_DP_detail__c> caseDPDetOnCaseUpdateList = new List<Case_DP_detail__c>();
       existingDPdetailsOnCaseList = [Select Id,SAM_Id__c,P6_Project_ID__c,P6_ObjectId__c,Case__c,Build_Start__c,Build_WR__c,Build_Contractor__c,Design_Start__c,Design_Approved__c,Design_Contractor__c,PC_Met__c,Ready_for_Service__c From Case_DP_detail__c Where Case__c =:cs.Id];
       if (existingDPdetailsOnCaseList != null && existingDPdetailsOnCaseList.size() >0) {
           for (Case_DP_detail__c cdet : caseDPdetailsListOnCase) {
               for (Case_DP_detail__c existcdet : existingDPdetailsOnCaseList) {
                   if (cdet.P6_Project_ID__c == existcdet.P6_Project_ID__c ) {
                       existcdet.P6_Project_ID__c = cdet.P6_Project_ID__c;
                       existcdet.P6_ObjectId__c = cdet.P6_ObjectId__c;
                       existcdet.Build_Start__c = cdet.Build_Start__c;
                       existcdet.Build_WR__c = cdet.Build_WR__c;
                       existcdet.Ready_for_Service__c = cdet.Ready_for_Service__c;
                       existcdet.Design_Start__c = cdet.Design_Start__c;
                       existcdet.Design_Approved__c = cdet.Design_Approved__c;
                       existcdet.PC_Met__c = cdet.PC_Met__c;
                       caseDPDetOnCaseUpdateList.add(existcdet);
                   }
                   else {
                           caseDPDetOnCaseInsertList.add(cdet);   
                   }
               }
           }
           System.debug('caseDPDetOnCaseUpdateList>>>'+caseDPDetOnCaseUpdateList);
           System.debug('caseDPDetOnCaseInsertList>>>'+caseDPDetOnCaseInsertList);
           if (caseDPDetOnCaseUpdateList != null ) {
               insert caseDPDetOnCaseInsertList;
           }    
           if (caseDPDetOnCaseUpdateList != null ) {
               update caseDPDetOnCaseUpdateList;
           }
       }
       else {
              System.debug('caseDPdetailsListOnCase>>>'+caseDPdetailsListOnCase);  
              insert caseDPdetailsListOnCase;
       }    
           
    } 
    
    public static String getCaseTechType(String tType) { 
        String p6TechType;
        if (tType.contains('FTTB')) {
            p6TechType = 'FTTB';
        }   
        else if (tType.contains('Fibre')) {
                p6TechType = 'Fibre';
        }         
        else if (tType.contains('FTTN')) {
                p6TechType = 'FTTN';
        }
        else if (tType.contains('FTTC')) {
                 p6TechType = 'FTTC'; 
        }     
        else if(tType.contains('FTTP')) {
                 p6TechType = 'FTTP';
        } 
        else if (tType.contains('HFC')) {
                p6TechType = 'HFC';
        } 
        else if (tType.contains('HDA/Foxtrot')) {
                p6TechType = 'HDA/Foxtrot';
        }
        System.debug('p6TechType>>>'+p6TechType);
        return p6TechType;
    }
    
    public class AutoAllocDPInfo {
        public String autoAllocDPName {get;set;} 
        public String autoAllocRFSdate {get;set;}        
        Public String autoAllocStatus {get;set;}
        Public String autoAllocP6ObjTechType {get;set;}
        public AutoAllocDPInfo () {
        }
    }

}