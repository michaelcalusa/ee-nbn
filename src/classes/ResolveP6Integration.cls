/***************************************************************************************************
Class Name          : ResolveP6Integration
Test Class          : ResolveGetDP_Test
Version             : 1.0 
Created Date        : 12-Nov-2018
Author              : Dilip
Description         : This class will have the various methods for the P6 Integration for Resolve project
Input Parameters    : 
Output Parameters   :  
Modification Log    :

* Developer             Date            Description
* ------------------------------------------------------------------------------------------------   


****************************************************************************************************/ 
public class ResolveP6Integration 
{
    Static Resolve_P6_Credentials__c P6UserCredentials = Resolve_P6_Credentials__c.getOrgDefaults();
    Static NBNIntegrationInputs__c resDataPowerEnv = NBNIntegrationInputs__c.getInstance('P6ServicesAPI');
    
    public class GeneralException extends Exception
    {}
    
    public static string getAuth()
    {
        try
        {
            //variable declarations
            DOM.Document doc = new DOM.Document();
            String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';  
            String v1 ='http://xmlns.oracle.com/Primavera/P6/WS/Authentication/V1';
            Http http = new Http(); 
            HttpRequest req = new HttpRequest();
            HttpResponse res;
            String cookie,jsessionId;
            String url = '/AuthenticationService';
            //end
            ////preparing the soap request 
            dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');        
            envelope.setNamespace('v1', v1);
            dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
            dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
            dom.XmlNode login = body.addChildElement('Login', v1, null);
            dom.XmlNode username = login.addChildElement('UserName', v1, null).addTextNode(P6UserCredentials.Username__c);
            dom.XmlNode password = login.addChildElement('Password', v1, null).addTextNode(P6UserCredentials.Password__c);
            //end
            
            req.setMethod('POST');
            req.setEndpoint(resDataPowerEnv.Call_Out_URL__c+url);
            req.setHeader('Content-Type', 'text/xml');
            req.setHeader('SOAPAction','urn:Login');
            if (String.isNotBlank(resDataPowerEnv.NBN_Environment_Override__c)) req.setHeader('NBN-Environment-Override',resDataPowerEnv.NBN_Environment_Override__c);
            
            req.setBodyDocument(doc);
            res = http.send(req);
            System.debug(doc.toXmlString());
            System.debug(res.getBodyDocument().toXmlString());
            system.debug('Cookie-->'+res.getHeader('Set-Cookie'));
            if(res.getStatusCode()==200)
            {
                cookie = res.getHeader('Set-Cookie');
                jsessionId = cookie.substring(cookie.indexOf('JSESSIONID='),cookie.indexOf('; path=/; HttpOnly'));
                system.debug('jsessionId-->'+jsessionId);
                return jsessionId ;                
            }
            else
            {
                throw new GeneralException('Not able to establish a session with P6, Error - '+res.getStatusCode()+'- '+res.getStatus());
            }
            
        }
        catch(exception ex)
        {
            GlobalUtility.logMessage('Error', 'ResolveP6Integration', 'getAuth', ' ', 'error in authentication', '', '', ex, 0);
            return null;
        }
        
    }
    
    public static List<ObjectInfo> getObjectId(String cookie, string projId)
    {
        
        //variable declarations
        String objectId, objName;
        DOM.Document doc = new DOM.Document();
        DOM.Document resDoc = new DOM.Document();
        String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';  
        String v2 ='http://xmlns.oracle.com/Primavera/P6/WS/Project/V2';
        Http http = new Http(); 
        HttpRequest req = new HttpRequest();
        HttpResponse res;
        Dom.XMLNode rootNode, resBody;
        string strResponse;
        List<ObjectInfo> objInfoList = new List<ObjectInfo>();
        try
        {
            dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');        
            envelope.setNamespace('v2', v2);
            dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
            dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
            dom.XmlNode readProj = body.addChildElement('ReadProjects', v2, null);
            dom.XmlNode field1 = readProj.addChildElement('Field', v2, null).addTextNode('ObjectId');         
            dom.XmlNode field2 = readProj.addChildElement('Field', v2, null).addTextNode('Id');
            dom.XmlNode filter = readProj.addChildElement('Filter', v2, null).addTextNode('Id like '+projId+''); //Id Like '%2HMK-23'
            req.setMethod('POST');
            req.setEndpoint(resDataPowerEnv.Call_Out_URL__c+'/ProjectService');
            req.setHeader('Content-Type', 'text/xml');
            req.setHeader('SOAPAction','ReadProjects');
            if (String.isNotBlank(resDataPowerEnv.NBN_Environment_Override__c)) req.setHeader('NBN-Environment-Override',resDataPowerEnv.NBN_Environment_Override__c);
            req.setHeader('Cookie', cookie); 
            req.setTimeout(Integer.valueof(resDataPowerEnv.Call_TimeOut_In_MilliSeconds__c));
            req.setBodyDocument(doc);
            res = http.send(req);
            System.debug(doc.toXmlString());
            System.debug(res.getBodyDocument().toXmlString());
            strResponse = res.getBody();
            resDoc.load(res.getBody());
            rootNode = resDoc.getRootElement();
            resBody = rootNode.getChildElement('Body', soapNS);
            if(strResponse.contains('<faultcode>'))
            {
                throw new GeneralException('Response is not correct - '+res.getBodyDocument().toXmlString());
            }
            else
            {
                ObjectInfo objInfo;
                for(Dom.XMLNode child : resBody.getChildElements()[0].getChildElements()) {
                    system.debug('child-->'+child.getChildElements());
                    for (Dom.XMLNode subChild : child.getChildElements() ) {
                        if(subChild.getName() == 'ObjectId') {
                            objectId = string.isNotBlank(subChild.getText()) ? subChild.getText() : '';
                        }
                        if (subChild.getName() == 'Id') {
                            objName = string.isNotBlank(subChild.getText()) ? subChild.getText() : '';
                        }
                    }  
                    objInfo= new ObjectInfo(objName, objectId, ''); 
                    system.debug('objInfo--->'+objInfo);
                    objInfoList.add(objInfo); 
                }
            }
            return objInfoList;
        }
        catch (exception ex)
        {
            GlobalUtility.logMessage('Error', 'ResolveP6Integration', 'getObjectId', ' ', 'error in getting object Id', '', '', ex, 0);
            ObjectInfo objInfo = new ObjectInfo ('', '', ex.getMessage());
            system.debug('objInfo Error--->'+objInfo);
            objInfoList.add(objInfo);
            return objInfoList;
        }
    }
    
    public static List<ActivityInfo> getActivityDetails(String Cookie, String projObjId, String p6ActivityCodes)
    {
        
        //variable declarations
        DOM.Document doc = new DOM.Document();
        DOM.Document resDoc = new DOM.Document();
        String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';  
        String v1 ='http://xmlns.oracle.com/Primavera/P6/WS/Activity/V1';
        Http http = new Http(); 
        HttpRequest req = new HttpRequest();
        HttpResponse res;
        String finishDate, status, objectId, activityId, projectObjectId, startDate, projectId;
        Dom.XMLNode rootNode, resBody;
        string strResponse;
        List<ActivityInfo> actInfoList = new List<ActivityInfo>();
        
        //ends
        try
        {
            //request xml creation
            dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');        
            envelope.setNamespace('v1', v1);
            dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
            dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
            dom.XmlNode readAct = body.addChildElement('ReadActivities', v1, null);
            dom.XmlNode field1 = readAct.addChildElement('Field', v1, null).addTextNode('Status');
            dom.XmlNode field2 = readAct.addChildElement('Field', v1, null).addTextNode('ObjectId');
            dom.XmlNode field3 = readAct.addChildElement('Field', v1, null).addTextNode('FinishDate');
            dom.XmlNode field4 = readAct.addChildElement('Field', v1, null).addTextNode('Id');
            dom.XmlNode field5 = readAct.addChildElement('Field', v1, null).addTextNode('ProjectObjectId');
            dom.XmlNode field6 = readAct.addChildElement('Field', v1, null).addTextNode('StartDate');
            dom.XmlNode field7 = readAct.addChildElement('Field', v1, null).addTextNode('ProjectId');
            dom.XmlNode filter = readAct.addChildElement('Filter', v1, null).addTextNode('ProjectObjectId in ('+projObjId+') and Id in ('+p6ActivityCodes+')');
            
            req.setMethod('POST');
            req.setEndpoint(resDataPowerEnv.Call_Out_URL__c+'/ActivityService');
            req.setHeader('Content-Type', 'text/xml');
            req.setHeader('SOAPAction','ReadActivities');
            req.setHeader('Cookie', cookie); 
            if (String.isNotBlank(resDataPowerEnv.NBN_Environment_Override__c)) req.setHeader('NBN-Environment-Override',resDataPowerEnv.NBN_Environment_Override__c);
            req.setTimeout(Integer.valueof(resDataPowerEnv.Call_TimeOut_In_MilliSeconds__c));
            req.setBodyDocument(doc);
            res = http.send(req);
            System.debug(doc.toXmlString());
            System.debug(res.getBodyDocument().toXmlString());
            strResponse = res.getBody();
            resDoc.load(res.getBody());
            rootNode = resDoc.getRootElement();
            resBody = rootNode.getChildElement('Body', soapNS);
            if(strResponse.contains('<faultcode>'))
            {
                throw new GeneralException('Response is not correct - '+res.getBodyDocument().toXmlString());
            }
            else
            {
                for(Dom.XMLNode child : resBody.getChildElements()[0].getChildElements())
                {
                    for(Dom.XMLNode gChild : child.getChildElements())
                    {
                        system.debug('child-->'+gChild.getText());
                        if(gChild.getName() == 'FinishDate')
                        {
                            finishDate = string.isNotBlank(gChild.getText()) ? gChild.getText()+'Z' : '';
                        }
                        if(gChild.getName() == 'ObjectId')
                        {
                            objectId = string.isNotBlank(gChild.getText()) ? gChild.getText() : '';
                        }
                        if(gChild.getName() == 'Status')
                        {
                            status = string.isNotBlank(gChild.getText()) ? gChild.getText() : '';
                        }   
                        if(gChild.getName() == 'Id')
                        {
                            activityId = string.isNotBlank(gChild.getText()) ? gChild.getText() : '';
                        }
                        if(gChild.getName() == 'ProjectObjectId')
                        {
                            projectObjectId = string.isNotBlank(gChild.getText()) ? gChild.getText() : '';
                        }
                        if(gChild.getName() == 'StartDate') 
                        {
                            startDate = string.isNotBlank(gChild.getText()) ? gChild.getText()+'Z' : '';
                        }
                        if(gChild.getName() == 'ProjectId') 
                        {
                            projectId = string.isNotBlank(gChild.getText()) ? gChild.getText() : '';
                        }
                        
                    }
                    ActivityInfo actInfo = new ActivityInfo (finishDate, status, objectId, activityId, projectObjectId, startDate, projectId, '');
                    system.debug('actInfo--->'+actInfo);
                    actInfoList.add(actInfo);                    
                }               
                
            }
            system.debug('actInfoList-->'+actInfoList);
            return actInfoList;
        }
        catch(exception ex)
        {
            GlobalUtility.logMessage('Error', 'ResolveP6Integration', 'getRFSDate', ' ', 'error in getting RFS Date', '', '', ex, 0);
            ActivityInfo actInfo = new ActivityInfo ('', '', '', '', '', '', '', ex.getMessage());
            system.debug('actInfo--->'+actInfo);
            actInfoList.add(actInfo);
            return actInfoList;
        }
        
    }
    
    public static List<ProjCodeResponse> getDPName(String Cookie, String projObjId, string projCodObjId, string p6ProgramDeliveryCode)
    {
        
        //variable declarations
        DOM.Document doc = new DOM.Document();
        DOM.Document resDoc = new DOM.Document();
        String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';  
        String v1 ='http://xmlns.oracle.com/Primavera/P6/WS/ProjectCodeAssignment/V1';
        Http http = new Http(); 
        HttpRequest req = new HttpRequest();
        HttpResponse res;
        String projCodeTypeName, projCodeValue, objectId, projCodeObjId;
        Dom.XMLNode rootNode, resBody;
        string strResponse;
        List<ProjCodeResponse> projCodeRspList = new List<ProjCodeResponse>();
        
        //ends
        try
        {
            //request xml creation
            dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');        
            envelope.setNamespace('v1', v1);
            dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
            dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
            dom.XmlNode readProjCodeAss = body.addChildElement('ReadProjectCodeAssignments', v1, null);
            dom.XmlNode field1 = readProjCodeAss.addChildElement('Field', v1, null).addTextNode('ProjectCodeTypeName');
            dom.XmlNode field2 = readProjCodeAss.addChildElement('Field', v1, null).addTextNode('ProjectCodeValue');      
            dom.XmlNode field3 = readProjCodeAss.addChildElement('Field', v1, null).addTextNode('ProjectCodeTypeObjectId'); 
            dom.XmlNode filter = readProjCodeAss.addChildElement('Filter', v1, null).addTextNode('ProjectObjectId in ('+projObjId+') and (ProjectCodeTypeObjectId='+projCodObjId+' or ProjectCodeTypeObjectId='+p6ProgramDeliveryCode+ ')');
            
            req.setMethod('POST');
            req.setEndpoint(resDataPowerEnv.Call_Out_URL__c+'/ProjectCodeAssignmentService'); 
            req.setHeader('Content-Type', 'text/xml');
            req.setHeader('SOAPAction','ReadProjectCodeAssignments');
            req.setHeader('Cookie', cookie); 
            req.setTimeout(Integer.valueof(resDataPowerEnv.Call_TimeOut_In_MilliSeconds__c));
            req.setBodyDocument(doc);
            if (String.isNotBlank(resDataPowerEnv.NBN_Environment_Override__c)) req.setHeader('NBN-Environment-Override',resDataPowerEnv.NBN_Environment_Override__c);
            
            res = http.send(req);
            System.debug(doc.toXmlString());
            System.debug(res.getBodyDocument().toXmlString());
            strResponse = res.getBody();
            resDoc.load(res.getBody());
            rootNode = resDoc.getRootElement();
            resBody = rootNode.getChildElement('Body', soapNS);
            if(strResponse.contains('<faultcode>'))
            {
                throw new GeneralException('Response is not correct - '+res.getBodyDocument().toXmlString());
            }
            else
            {
                for(Dom.XMLNode child : resBody.getChildElements()[0].getChildElements())
                {
                    for(Dom.XMLNode gChild : child.getChildElements())
                    {
                        system.debug('child-->'+gChild.getText());
                        if(gChild.getName() == 'ProjectCodeTypeName')
                        {
                            projCodeTypeName = string.isNotBlank(gChild.getText()) ? gChild.getText() : '';
                        }
                        if(gChild.getName() == 'ProjectObjectId')
                        {
                            objectId = string.isNotBlank(gChild.getText()) ? gChild.getText() : '';
                        }
                        if(gChild.getName() == 'ProjectCodeValue')
                        {
                            projCodeValue = string.isNotBlank(gChild.getText()) ? gChild.getText() : '';
                        } 
                        if(gChild.getName() == 'ProjectCodeTypeObjectId')
                        {
                            projCodeObjId = string.isNotBlank(gChild.getText()) ? gChild.getText() : '';
                        } 
                        
                        
                    }
                    ProjCodeResponse projCodeRsp = new ProjCodeResponse (projCodeTypeName, objectId, projCodeValue, projCodeObjId, '');
                    system.debug('projCodeRsp--->'+projCodeRsp);
                    projCodeRspList.add(projCodeRsp);                    
                }               
                
            }
            system.debug('projCodeRspList-->'+projCodeRspList);
            return projCodeRspList;
        }
        catch(exception ex)
        {
            GlobalUtility.logMessage('Error', 'ResolveP6Integration', 'getDPName', ' ', 'error in getting DP Name', '', '', ex, 0);
            ProjCodeResponse projCodeRsp = new ProjCodeResponse ('', '', '', '', ex.getMessage());
            system.debug('projCodeRsp--->'+projCodeRsp);
            projCodeRspList.add(projCodeRsp);   
            return projCodeRspList;
        }
        
    }
    
    public static boolean logout(string cookie)
    {
        try
        {
            //variable declarations
            DOM.Document doc = new DOM.Document();
            String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';  
            String v1 ='http://xmlns.oracle.com/Primavera/P6/WS/Authentication/V1';
            Http http = new Http(); 
            HttpRequest req = new HttpRequest();
            HttpResponse res;
            //end
            ////preparing the soap request 
            dom.XmlNode envelope = doc.createRootElement('Envelope', soapNS, 'soapenv');        
            envelope.setNamespace('v1', v1);
            dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
            dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
            dom.XmlNode login = body.addChildElement('Logout', v1, null);            
            //end
            
            req.setMethod('POST');
            req.setEndpoint(resDataPowerEnv.Call_Out_URL__c+'/AuthenticationService');
            req.setHeader('Content-Type', 'text/xml');
            req.setHeader('SOAPAction','urn:Logout');
            req.setHeader('Cookie', cookie); 
            req.setBodyDocument(doc);
            if (String.isNotBlank(resDataPowerEnv.NBN_Environment_Override__c)) req.setHeader('NBN-Environment-Override',resDataPowerEnv.NBN_Environment_Override__c);
            
            res = http.send(req);
            System.debug(doc.toXmlString());
            System.debug(res.getBodyDocument().toXmlString());            
            if(res.getStatusCode()==200)
            {                
                return true ;                
            }
            else
            {
                throw new GeneralException('Not able to logout, Error - '+res.getStatusCode()+'- '+res.getStatus());
            }
            
        }
        catch(exception ex)
        {
            GlobalUtility.logMessage('Error', 'ResolveP6Integration', 'logout', ' ', 'error in logout', '', '', ex, 0);
            return false;
        }
        
    }
    
    public class ActivityInfo
    {
        public String finishDate {get;set;}        
        public String status {get;set;}
        public String objectId {get;set;}        
        Public String error {get;set;}
        Public String activityId {get;set;}
        Public String projectObjectId {get;set;} 
        public String startDate {get;set;} 
        public String projectId {get;set;}
        public ActivityInfo (String finishDate, String status, String objectId, String activityId, String projectObjectId, String startDate, String projectId, string error )
        {
            this.finishDate = finishDate;            
            this.objectId = objectId;
            this.status = status;
            this.activityId = activityId;
            this.projectObjectId = projectObjectId;   
            this.startDate = startDate;
            this.projectId = projectId; 
            this.error = error;
        }
    }
    
    public class ProjCodeResponse
    {
        public String projCodeTypeName {get;set;}        
        public String objectId {get;set;}
        public String projCodeValue {get;set;}
        public String projCodeObjId {get;set;}        
        Public String error {get;set;}
        public ProjCodeResponse (String projCodeTypeName, String objectId, String projCodeValue, String projCodeObjId, string error)
        {
            this.projCodeTypeName = projCodeTypeName;            
            this.objectId = objectId;
            this.projCodeValue = projCodeValue;
            this.projCodeObjId = projCodeObjId;
            this.error = error;
        }
    }
    
    public class ObjectInfo
    {
        public String objName {get;set;} 
        public String objectId {get;set;}        
        Public String error {get;set;}
        public ObjectInfo (String objName, String objectId, string error)
        {
            this.objName = objName;            
            this.objectId = objectId;
            this.error = error;
        }
    }
    
}