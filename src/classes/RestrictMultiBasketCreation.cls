//*************************************************
// Developed by: Viswanatha Goki
// Date Created: 25.09.2016 (dd.MM.yyyy)
// Last Update: 18.10.2016 (dd.MM.yyyy)
// Description: Custom Controller Apex class invoked from Onclick of New Product basket
//              button on Opportunity Page. Restricts creating multi product baskets under an opportunity.
//              If a Product basket already exists the class redirects to the existing Product Basket detail page.
// Modifications
//     - Add in additional items to the CSV (Alistair Borley)
//*************************************************

public with sharing class RestrictMultiBasketCreation {

    public String oppId;
    public String oppName;
    public PageReference pageRef;
    public String apexUrl;
    public CPQSettings__c cpqSettings;

    public PageReference init()
    {
        List<cscfga__Product_Basket__c> basketList = [Select Id from cscfga__Product_Basket__c
                                                        where   cscfga__Opportunity__c =:oppId];
        if(basketList !=null && basketList.size() > 0)
        {
             apexUrl = '/apex/ConfigurationBasketDetailAng?id=' + basketList[0].Id +'&sfdc.override=1';
             apexUrl='/' + basketList[0].Id;
             pageRef = new PageReference(apexUrl);
        }
        else
        {
            //To do: Get Url from Custom Setting - https://nbn--cpqdev--cscfga.cs6.visual.force.com
            // TF apexUrl = cpqSettings.CloudSense_Page_URL_Prefix__c + '/apex/SelectProductCategoryForBasket?CF00NN0000001zDtW=' + oppName;
            // TF apexUrl = apexUrl + '&CF00NN0000001zDtW_lkid=' + oppId;
            // TF apexUrl = apexUrl + '&scontrolCaching=1&retURL=/' + oppId + '&sfdc.override=1';
            
            //apexUrl = '/apex/selectproductcategory?CF00NN0000001zDtW=Test+another&CF00NN0000001zDtW_lkid=';
            //apexUrl = apexUrl + oppId + '&configURL=%2Fapex%2Fcscfga__configuration&containerType=basket&linkedId=';
            //apexUrl = apexUrl + oppId + '&retURL=%2F' + oppId +'&scontrolCaching=1&sfdc.override=1';


            //To do: Get Url from Custom Setting - https://nbn--cpqdev--cscfga.cs6.visual.force.com
            apexUrl = cpqSettings.CloudSense_Page_URL_Prefix__c + '/apex/csbb__NewBasketRedirect?' + cpqSettings.OpportunityLookupIdOnBasket__c + '=' + EncodingUtil.urlEncode(oppName, 'UTF-8');
            apexUrl = apexUrl + '&' + cpqSettings.OpportunityLookupIdOnBasket__c + '_lkid=' + oppId;
            apexUrl = apexUrl + '&scontrolCaching=1&retURL=/' + oppId + '&sfdc.override=1';
            
            pageRef = new PageReference(apexUrl);
        }
        return pageRef;
    }
    
    public RestrictMultiBasketCreation() 
    {
        pageRef = null;
        apexUrl = '';
        oppName = '';
        cpqSettings = CPQSettings__c.getvalues('CPQ Settings');
        oppId = ApexPages.currentPage().getParameters().get('id');
        if(oppId != null)
        {
            Opportunity opp = [Select Id, Name from Opportunity where Id=:oppId];
            oppName = opp.Name;
        }
    }


}