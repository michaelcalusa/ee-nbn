@isTest
private class RestrictMultiBasketCreation_Test
{
/*------------------------------------------------------------------------
Author:        Viswanatha
Company:       CloudSense
Description:   Test class for RestrictMultiBasketCreation Class
               1 - Test to Redirect to a new Product basket Page when None exists under opportunity
               2 - Test to Redirect to existing Product basket page when already exists under opportunity
               
Class:           RestrictMultiBasketCreation_Test
History
<Date>            <Authors Name>    <Brief Description of Change> 
--------------------------------------------------------------------------*/  
	
	@isTest static void test_method_one() 
	{
		TestDataClass.CreateTestCPQSettingsData();
		Opportunity objOpp = TestDataClass.CreateTestOpportunityData();
		

		//Test scenario where Basket is not Existing under an Opportunity
		ApexPages.currentPage().getParameters().put('id',objOpp.id);
		RestrictMultiBasketCreation controllerRestrictMultiBasket = new RestrictMultiBasketCreation();
		String nextPage = controllerRestrictMultiBasket.init().getUrl();

		//Test scenario where Basket is Already Existing under an Opportunity
		cscfga__Product_Basket__c objBasket = TestDataClass.CreateTestBasketData(objOpp.Id);
		objBasket.cscfga__Opportunity__c = objOpp.Id;
		update objBasket;

		ApexPages.currentPage().getParameters().put('id',objOpp.id);
		controllerRestrictMultiBasket = new RestrictMultiBasketCreation();
		nextPage = controllerRestrictMultiBasket.init().getUrl();
		system.assertEquals('/'+objBasket.Id,nextPage);

	}

	
	
}