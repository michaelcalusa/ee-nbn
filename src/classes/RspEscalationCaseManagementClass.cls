@RestResource(urlMapping='/SalesforceSailIntergation')
global with sharing class RspEscalationCaseManagementClass
{

    @HttpPost
    global static String PopulateCaseStageRecord(){
    
   //EnvelopeURls
    try{ 
    String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
    String NbnNS = 'http://nbnco.com.au/schema/assurance/notify-WDincident/request/v1';
    String HeadNS = 'http://www.nbnco.com.au/dmm/common/message-header/v1';
    String IncidentIdfromSAIL = '';
    String IncidentRaisedDateTime = '';
    String PlannedResolutionDateTime = '';
    String RequestStampDateTime = '';
    String RequestStampDateTimeCheck = '';
    string categoryTypeOP= '';
    string categoryname = '';
    string categoryvalue = '';
    String NType = '';
    String NSubtype = '';
   // Extract the XML from the request body    
        RestRequest req = RestContext.request;
        blob reqbody = req.requestBody ;
        string reqbodystring = reqbody.tostring();

   // Create an instance of Case Stage Record 
   Case_Stage__c CS = new Case_Stage__c();
   
    
    // Parse XML using DOM     
        Dom.Document doc = new Dom.Document();
        doc.load(reqbodystring);
        system.debug('santoshlogging: '+ reqbodystring );
        dom.XmlNode envelope = Doc.getRootElement();
        dom.XmlNode header = envelope.getChildElement('Header', soapNS);
        dom.XmlNode MessageHeader = header.getChildElement('MessageHeader', HeadNS);
        dom.XmlNode body = envelope.getChildElement('Body', soapNS);
        dom.XmlNode notifyWDIncidentRequest= body.getChildElement('notifyWDIncidentRequest', NbnNS); 
       system.debug(notifyWDIncidentRequest); 
    
    // Form Here we'll start accepting required data 
       
       RequestStampDateTime = MessageHeader.getChildElement('timestamp', HeadNS).getText();
       CS.Request_Stamp_DateTime_String__c = MessageHeader.getChildElement('timestamp', HeadNS).getText();
       CS.Notification_Type__c =  notifyWDIncidentRequest.getChildElement('notificationType', NbnNS).getText();
       Ntype = notifyWDIncidentRequest.getChildElement('type', NbnNS).getText();
       NSubtype = notifyWDIncidentRequest.getChildElement('subType', NbnNS).getText();
       
       If(Ntype == 'notification' && NSubtype == 'incident'){
       
       
    // Get Incident Details 
       dom.XmlNode incident = notifyWDIncidentRequest.getChildElement('incident', NbnNS);
       system.debug('Santosh'+incident.getChildElement('TargetSLATime', NbnNS));
       IncidentIdfromSAIL = incident.getChildElement('id', NbnNS).getText();
       CS.IncidentID__c = incident.getChildElement('id', NbnNS).getText();
       CS.Request_Type__c =  incident.getChildElement('type', NbnNS).getText();
       CS.Priority__c = incident.getChildElement('priority', NbnNS).getText();
       If(incident.getChildElement('reportedSource', NbnNS) != null) CS.Source__c = incident.getChildElement('reportedSource', NbnNS).getText();
       If(incident.getChildElement('incidentnotes', NbnNS) != null) CS.Comments__c= incident.getChildElement('incidentnotes', NbnNS).getText();
       CS.Incident_Raised_DateTime_String__c = incident.getChildElement('reportedDate', NbnNS).getText();
       IncidentRaisedDateTime = incident.getChildElement('reportedDate', NbnNS).getText();
       If(incident.getChildElement('TargetSLATime', NbnNS) != null) CS.Planned_Resolution_DateTime_String__c = incident.getChildElement('TargetSLATime', NbnNS).getText();
       If(incident.getChildElement('TargetSLATime', NbnNS) != null) PlannedResolutionDateTime = incident.getChildElement('TargetSLATime', NbnNS).getText();
       //MSEU-2873 - Addition of Answer 
       if(CS.Request_Type__c == 'User Service Request'){
            Dom.XMLNode requestDetailsContainer = incident.getChildElement('requestDetailsContainer', NbnNS);
            if(requestDetailsContainer != null){
                Dom.XMLNode requestDetailsList = requestDetailsContainer.getChildElement('requestDetailsList', NbnNS);
                if(requestDetailsList != null){
                    for(Dom.XMLNode requestDetail :requestDetailsList.getChildElements())
                    {
                        if( requestDetail != null && requestDetail.getChildElement('question', NbnNS).getText() == 'Location ID')
                        {
                            system.debug('@@@@@@@@Shubham'+requestDetail.getChildElement('answer', NbnNS).getText());
                            CS.LocationIDAnswer__c = requestDetail.getChildElement('answer', NbnNS).getText();
                        }
                    }
                }
            }
       }
   // Get Industry Details 
       
       if(incident.getChildElement('industryStatus', NbnNS) != null)CS.Industry_Status__c = incident.getChildElement('industryStatus', NbnNS).getText();
       
  // Get Access Seeker Details  
       dom.XmlNode accessSeeker = incident.getChildElement('accessSeeker', NbnNS);
       
       if(accessSeeker.getChildElement('id', NbnNS) != null) CS.AccessSeeker_ID__c = accessSeeker.getChildElement('id', NbnNS).getText();
       if(accessSeeker.getChildElement('name', NbnNS) != null) CS.AccessSeeker_Name__c = accessSeeker.getChildElement('name', NbnNS).getText();
             
       if(accessSeeker.getChildElement('accessSeekerContact', NbnNS) != null){
       dom.XmlNode accessSeekerContact = accessSeeker.getChildElement('accessSeekerContact', NbnNS);
       
       if(accessSeekerContact.getChildElement('firstName', NbnNS) != null) CS.AccessSeeker_FirstName__c = accessSeekerContact.getChildElement('firstName', NbnNS).getText();
       if(accessSeekerContact.getChildElement('lastName', NbnNS) != null) CS.AccessSeeker_LastName__c = accessSeekerContact.getChildElement('lastName', NbnNS).getText();
       }
   // Get Order Details 
       dom.XmlNode order = incident.getChildElement('order', NbnNS);
     if(order != null){  
      if(order.getChildElement('id', NbnNS) != null) CS.OrderNumber__c = order.getChildElement('id', NbnNS).getText();
      }
   // Get Assignee Details 
      dom.XmlNode assignee = incident.getChildElement('assignee', NbnNS);
      
      if(assignee != null){ 
      if(assignee.getChildElement('assigneeLoginId', NbnNS) != null )  CS.Owner_FederationID__c = assignee.getChildElement('assigneeLoginId', NbnNS).getText();
      if(assignee.getChildElement('assignedGroup', NbnNS) != null) CS.Owner_Group__c = assignee.getChildElement('assignedGroup', NbnNS).getText();
      } 
   // Get OpCategory Details 
     dom.XmlNode categoryListContainer = incident.getChildElement('categoryListContainer', NbnNS);
        if(categoryListContainer != null){ 
        system.debug('santosh1-categoryListContainer'+ categoryListContainer );
         dom.XmlNode incidentCategoryList = categoryListContainer.getChildElement('incidentCategoryList', NbnNS);
         if(incidentCategoryList != null){
         system.debug('santosh2-incidentCategoryList '+ incidentCategoryList );
            dom.XmlNode [] incidentCategory = incidentCategoryList.getchildelements() ; //Get all Record Elements
          
            for(Dom.XMLNode Cat : incidentCategory) //Loop Through Records
            {
            system.debug('santosh3-Cat '+ Cat );
            
              for (dom.XmlNode awr : Cat.getchildren() ) {
                    system.debug('santosh4-awr '+ awr.getname() + '----'+ awr.gettext());
                    
                                     if (awr.getname() == 'categoryType') {
                                          categoryTypeOP= awr.gettext();
                                        }  
                                     if (awr.getname() == 'category') {
                                    if(awr.getChildElement('name', NbnNS) != null) categoryname =  awr.getChildElement('name', NbnNS).getText();
                                    if(awr.getChildElement('value', NbnNS) != null) categoryvalue =  awr.getChildElement('value', NbnNS).getText();                           
                                } 
                         }       
                system.debug('santosh5'+ categoryTypeOP + categoryname + categoryvalue );
                if(categoryTypeOP== 'OpCat' && categoryname == 'OpCat1'){
                system.debug('santosh6');
                CS.OpCat1__c = categoryvalue ;
                }
                if(categoryTypeOP== 'OpCat' && categoryname == 'OpCat2'){
                CS.OpCat2__c = categoryvalue ;
                }
                if(categoryTypeOP== 'OpCat' && categoryname == 'OpCat3'){
                CS.OpCat3__c = categoryvalue ;
                }
                
                categoryTypeOP = '';
                categoryname = '';
                categoryvalue = '';
            
           }
          }
         }
         
    // Process LocIDs 
    dom.XmlNode Place = incident.getChildElement('place', NbnNS);
    if(Place != null){
       dom.XmlNode [] places = Place.getchildelements() ;
    
     for(dom.XmlNode placeelement : places){
     
     if(placeelement.getChildElement('type', NbnNS) != null){  
        if( placeelement.getChildElement('type', NbnNS).getText() == 'TSL'){
        if(placeelement.getChildElement('id', NbnNS) != null) CS.LocationIDTSL__c = placeelement.getChildElement('id', NbnNS).getText() ;
        }
        
        if( placeelement.getChildElement('type', NbnNS).getText() == 'CSL'){
        if(placeelement.getChildElement('id', NbnNS) != null) CS.LocationIDCSL__c = placeelement.getChildElement('id', NbnNS).getText() ;
        }
     
     }  
     
    
    }
    }
   
    // Process Date Time Values    
      IncidentRaisedDateTime  = IncidentRaisedDateTime.replace('T',' ');
      IncidentRaisedDateTime  = IncidentRaisedDateTime.replace('Z','');
      CS.Incident_Raised_DateTime__c = Datetime.valueOfgmt(IncidentRaisedDateTime);
      
      PlannedResolutionDateTime = PlannedResolutionDateTime.replace('T',' ');
      PlannedResolutionDateTime = PlannedResolutionDateTime.replace('Z','');
      if(PlannedResolutionDateTime != '') CS.Planned_Resolution_DateTime__c = Datetime.valueOfgmt(PlannedResolutionDateTime);
      // Verify if this Incident is already created 
      
     List<Case_Stage__C> CSExisting = [Select Id,IncidentID__c,Request_Stamp_DateTime_String__c,lastmodifieddate from Case_Stage__C where IncidentID__c = :IncidentIdfromSAIL LIMIT 1];
     
     If(CSExisting.size() > 0){
     
     
     RequestStampDateTimeCheck = CSExisting[0].Request_Stamp_DateTime_String__c ;
     
     RequestStampDateTimeCheck = RequestStampDateTimeCheck.replace('T',' ');
     RequestStampDateTimeCheck = RequestStampDateTimeCheck.replace('Z','');
      
     RequestStampDateTime = RequestStampDateTime.replace('T',' ');
     RequestStampDateTime = RequestStampDateTime.replace('Z','');
     
     if(Datetime.valueOfgmt(RequestStampDateTime) > Datetime.valueOfgmt(RequestStampDateTimeCheck)){
          cs.id = CSExisting[0].id ;
          Update CS ;
     }     
     }else{
     
     if(CS.Request_Type__c == 'User Service Restoration' || CS.Request_Type__c == 'User Service Request')
     insert CS ;
     
     }
     
     }  
        RestResponse res = Restcontext.response;
        }Catch(exception e){
        // Handle the exception in need. 
        system.debug(e.getmessage());
        } 
        
        return 'Salesforce Received the Request';
        
   }
}