/***************************************************************************************************
Class Name:         SDM_CISIntegrationBatch
Class Type:         Makes nightly and hourly callouts to update orders and appointments 
					related to pre orders
Company :           Appirio
Created Date:       28 Sept 2018
Created By:			Sunaiyana Thakuria
****************************************************************************************************/
global class SDM_CISIntegrationBatch implements Database.Batchable<sObject>,Database.Stateful, Database.AllowsCallouts,Schedulable{
    
    global String batchType;
    
    global SDM_CISIntegrationBatch(String execBatchType){
        batchType = execBatchType;
    }
    
    global void execute(SchedulableContext sc){        
        SDM_CISIntegrationBatch job = new SDM_CISIntegrationBatch(batchType);
        Database.executeBatch(job,1);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {        
        String query = '';
        if(batchType == 'nightly'){
            Id recordTypeForOppty = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pre-Order').getRecordTypeId();
            query = 'Select id,SDM_NBN_Location_ID__c,SDM_nbn_Order_ID__c from Opportunity where SDM_NBN_Location_ID__c!=null and stagename!=\'Cancelled\' and recordtypeid = \''+recordTypeForOppty+'\'';
        }
        else if(batchType == 'hourly')
            query = 'select id,Order__r.Order_ID__c,Order__r.Pre_Order__r.SDM_NBN_Location_ID__c from Appointment__c where Appointment_Date__c=today and Order__r.Order_ID__c!=null and Order__r.Pre_Order__c!=null and Order__r.Pre_Order__r.SDM_NBN_Location_ID__c!=null';
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        Integer responseCode = 0;
        //Nightly batch to get Orders, Appointments and WorkOrders based on pre order location IDs
        if(batchType == 'nightly'){
            List<Opportunity> opptys = (List <Opportunity>)scope;
                        
            for(Opportunity o : opptys){               
                responseCode = IntegrationUtility.getLocationOrders(o.SDM_NBN_Location_ID__c,o.SDM_nbn_Order_ID__c);
            }
        }
        //Hourly batch to get Appointments and WorkOrders based on Order ID and related pre order location IDs
        else if(batchType == 'hourly'){
            List<Appointment__c> appointments = (List <Appointment__c>)scope;
            for(Appointment__c appointment : appointments){
                responseCode = IntegrationUtility.getLocationOrders(appointment.Order__r.Pre_Order__r.SDM_NBN_Location_ID__c, appointment.Order__r.Order_ID__c);
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}