/***************************************************************************************************
Class Name:         SDM_CISIntegrationBatchScheduler
Class Type:         Schedules nightly batch and allows users to schedule it via UI
Company :           Appirio
Created Date:       04 October 2018
Created By:			Sunaiyana Thakuria
****************************************************************************************************/
global class SDM_CISIntegrationBatchScheduler implements Schedulable{

    global void execute(SchedulableContext sc){        
        SDM_CISIntegrationBatch job = new SDM_CISIntegrationBatch('nightly');
        Database.executeBatch(job,1);
    }
}