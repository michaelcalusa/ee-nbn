@isTest
public class SDM_CISIntegrationBatchTest {
    
    //Inserting 100 records each for Opportunity, Orders and Appointments
    @TestSetup
    static void testDataInsert() 
    {
        List<Opportunity> opptys = new List<Opportunity>();
        List<Customer_Connections_Order__c> orders = new List<Customer_Connections_Order__c>();
        List<Appointment__c> appointments = new List<Appointment__c>();
        
        Id recordTypeForOppty = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pre-Order').getRecordTypeId();
        Id recordTypeForAcc = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business End Customer').getRecordTypeId();
        
        Account acc = new Account();
        acc.Name = 'test';
        acc.recordtypeid = recordTypeForAcc;
        insert acc;
        Integer counter = 100;

        Opportunity o = new Opportunity();
        //for(Integer i=0;i<10;i++){
            o = new Opportunity();
            o.RecordTypeId = recordTypeForOppty;
            o.name = 'Test';
            o.StageName = Constants.stagesValue;
            o.SDM_NBN_Location_ID__c = 'LOC000000001'+String.valueOf(counter);
            o.AccountId = acc.id;
            o.CloseDate = system.today();
            //counter++;
            opptys.add(o);
        //}
        insert opptys;
        
        counter = 100;
        Customer_Connections_Order__c order = new Customer_Connections_Order__c();
        for(Opportunity opp : opptys){
            order = new Customer_Connections_Order__c();
            order.Pre_Order__c = opp.id;
            order.Order_ID__c = 'ORD00001399441'+String.valueOf(counter);
            counter++;
            orders.add(order);
        }
        insert orders;
        
        Appointment__c app = new Appointment__c();
        for(Customer_Connections_Order__c ord : orders){
            app = new Appointment__c();
            app.Appointment_Date__c = system.today();
            app.Order__c = ord.id;
            appointments.add(app);
        }
        insert appointments;
    }
    
    //Testing execution of nightly batch
    static testMethod void testMethod1() 
    {
        Test.startTest();
        SDM_CISIntegrationBatchScheduler obj = new SDM_CISIntegrationBatchScheduler();
        //Database.executeBatch(obj,1);
        obj.execute(null);
        Test.stopTest();
        
    }
    
    //Testing execution of scheduler and hourly batch as per parameter sent
    static testMethod void testMethod2() 
    {
        Test.startTest();
        SDM_CISIntegrationBatch obj = new SDM_CISIntegrationBatch('hourly');
        obj.execute(null); 
        Test.stopTest();
    }
}