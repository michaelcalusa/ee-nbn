public class SDM_EmailUtilityClass {
    
    public static void sendEmailWithoutAttachments(List<String> toAddresses, String htmlBody, String subject){
        if(toAddresses!=null && toAddresses.size()>0){
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setSubject(subject);
            message.setHtmlBody(htmlBody);
            message.setToAddresses( toAddresses );
            Messaging.sendEmail( new Messaging.SingleEmailMessage[] { message } );  
        }
    }
    
    public static void sendEmailWithAttachments(List<String> toAddresses, String htmlBody, String subject, List<Messaging.EmailFileAttachment> attachments){
        if(toAddresses!=null && toAddresses.size()>0){
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setSubject(subject);
            message.setHtmlBody(htmlBody);
            message.setToAddresses( toAddresses );
            if(attachments!=null && attachments.size()>0)
            message.setFileAttachments(attachments);
            Messaging.sendEmail( new Messaging.SingleEmailMessage[] { message } );  
        }
    }    
}