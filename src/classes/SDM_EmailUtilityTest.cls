@isTest
public class SDM_EmailUtilityTest {
    
    public testmethod static void testEmailMethods(){
        test.startTest();
        SDM_EmailUtilityClass.sendEmailWithoutAttachments(new String[] { 'test@test.com' },'','');
        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        attachment.setFileName('test.txt');
        attachment.setBody(Blob.valueOf('test'));
        attachment.setContentType('text/csv');
        SDM_EmailUtilityClass.sendEmailWithAttachments(new String[] { 'test@test.com' },'','',new Messaging.EmailFileAttachment[] {attachment});
        test.stopTest();
    }
}