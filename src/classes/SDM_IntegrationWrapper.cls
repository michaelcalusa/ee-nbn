/***************************************************************************************************
Class Name:         SDM_IntegrationWrapper
Class Type:         WrapperClass 
Company :           Appirio
Created Date:       27 Sept 2018
Created By:			Shubham Paboowal
Updated By:         Wayne Ni, 05-04-2019
****************************************************************************************************/ 
public class SDM_IntegrationWrapper {
    public Data data;    
    
	public class Appdetails {
		public String appointmentId;
		public String nbnslottype;
		public String nbndemandtype;
		public String status;
        	public String reason;
        	public String statusdatetime;
        	public String creationdate;
        	public String endusertype;
		public String nbnappstart;
		public String nbnappend;
		public String nbnappstartlocal;
		public String nbnappstartlocaltime;
		public String nbnappendlocal;
		public String nbnappendlocaltime;
		public List<Wodetails> wodetails;
        
	        //Added by Wayne, update for new API
	        public List<contactdetails> ContactDetails;
	        public String buildingtype;
	        public String buildingname;
	        public String operatinghours;
	        public String siteconsiderations;
	        public String hazardousinfo;
	        public String accessinstructions;
	        public String inductionrequirements;
	        public String accreditationreq;
	        public String siteinfo;
	        public String specialcondition;
	}
	
	public class contactdetails{
	        public String phoneNumber;
	        public String type_z;
	        public String ContactName;
	        public String notes;
	}
    
	public class Accessseekerdetails {
		public String accessseekerid;
		public String accessseekername;
	}

	public class Wodetails {
		public String woid;
		public String nbnworkforce;
		public String woparent;
		public String wostatus;
		public String woreasoncode;
		public String woprd;
	        public string wosrq;
	        public string servicerequest;
		public String wopriority;
        
	        //Added by Wayne, API Update
	        public String cwonotes;
	        public String phytagged;
	        public String projecttype;
	        public String wodescription;
	        public String nbnhierarchytype;
	        public String wospecialcn;
	        public String woprbcode;
	        public String wocausecode;
	        public String woremedycode;
	        public String woclosuresummary;
	}
	
    	public class Data {
	        public String locationid;
        

        
	        public List<Orderdetails> orderdetails;
        
	        //Added by Wayne, API update
	        public String serviceabilitydate;
	        public String disconnectiondate;
	        public String nbntechtype;
    	}

	public class Orderdetails {
		public String nbnorderid;
		public String asreferenceid;
		public String orderstatus;
		public String dateacknowledged;
		public String dateaccepted;
		public String datecompleted;
		public String orderserviceabilityclass;
		public String sla;
		public List<Appdetails> appdetails;
        
        
	        //Added by Wayne, API update
	        public String accessseekerid;
	        public String accessseekername;
	        public String nbnordertype;
	        public String orderreason;
	        public String ordercreatedate;
		public List<AVC> avc;
	        public String endusertype;
	        public String priid;
	        public String unidid;
	        public String univid;
	        public String ntdid;
	        public String copperpathid;
	        public String copperpairsc;
	        public String serviceidentifier;
	        public String mdflocation;
	        public String mdfxpair;
	        public String mdfcpair;
	        public String producttemplate;
	        public String priorityassist;
	        public String installoptions;
	        //End here
	}
    
    public class AVC {
        public String avctype;
        public String id;
        public String bandwidth;        
    }
    
    public class Appdetails_Z {
        
    }

	
    public static SDM_IntegrationWrapper parse(String json) {
    	return (SDM_IntegrationWrapper) System.JSON.deserialize(json, SDM_IntegrationWrapper.class);
    }
}