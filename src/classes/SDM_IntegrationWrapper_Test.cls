/***************************************************************************************************
Class Name:         SDM_IntegrationWrapper_Test
Class Type:         Test Class for SDM_IntegrationWrapper
Company :           Appirio
Created Date:       27 Sept 2018
Created By:			Shubham Paboowal
****************************************************************************************************/ 
@IsTest
public class SDM_IntegrationWrapper_Test {
	
	static testMethod void testParse() {
		 String json = '{'+
        '   \"data\":{  '+
		'  \"locationid\": \"LOC000000206007\",'+
        ' \"serviceabilitydate\":\"21 Nov 2030 13:12:12\",'+
		'  \"accessseekerdetails\": {'+
		'    \"accessseekerid\": \"ASI000000220282\",'+
		'    \"accessseekername\": \"Telstra\"'+
		'  },'+
		'  \"orderdetails\": ['+
		'    {'+
        '	   \"accessseekerid\": \"ASI000000220282\",' +
        '	   \"avc\": [{\"avctype\" : \"AVC-V\",\"id\" : \"AVC000079068817\",\"bandwidth\": \"D0.15_U0.15_Mbps_TC1_C\" },{\"avctype\" : \"AVC-D\",\"id\" : \"AVC000079068818\",\"bandwidth\": \"“D12_U1_Mbps_TC4_P, D12_U1_Mbps_TC2_P, D0.15_U0.15_Mbps_TC1_C\" },{\"avctype\" : \"AVC-M\",\"id\" : \"AVC0000790688198\",\"bandwidth\": \"D0.15_U0.15_Mbps_TC1_C\" }],'+
        '      \"accessseekername\": \"Telstra\",'+
		'      \"nbnorderid\": \"ORD010004399548\",'+
		'      \"asreferenceid\": \"24334\",'+
		'      \"orderstatus\": \"Inprogress\",'+
		'      \"dateacknowledged\": \"2017-10-11T10:29:00\",'+
		'      \"dateaccepted\": \"2017-10-11T10:29:00\",'+
		'      \"datecompleted\": \"2017-10-11T10:29:00\",'+
		'      \"orderserviceabilityclass\": \"12\",'+
		'      \"sla\": \"\",'+
		'      \"appdetails\": [{'+
		'        \"appointmentId\": \"APT100000045564\",'+
		'        \"nbnslottype\": \"08-12\",'+
		'        \"nbndemandtype\": \"STANDARD INSTALL\",'+
		'        \"status\": \"Cancel\",'+
		'		\"nbnappstart\": \"2018-05-08T11:26:10\",'+
		'        \"nbnappend\": \"2018-05-08T11:26:10\",'+
		'        \"nbnappstartlocal\": \"2018-05-08T00:00:00\",'+
		'        \"nbnappstartlocaltime\": \"2018-05-08T08:00:00\",'+
		'        \"nbnappendlocal\": \"2018-05-08T00:00:00\",'+
		'        \"nbnappendlocaltime\": \"2018-05-08T12:00:00\",'+
		'		\"wodetails\": [{'+
		'          \"woid\": \"WOR100000201067\",'+
		'          \"nbnworkforce\": \"DP1019\",'+
		'          \"woparent\": \"WOR100000201051\",'+
		'          \"wostatus\": \"CANCEL\",'+
		'          \"woreasoncode\": \"ORDER-CAN\",'+
		'          \"woprd\": \"2018-05-08T11:16:44+1000\",'+
		'          \"wopriority\": \"STANDARD\"'+
		'        }]'+
		'      }]'+
		'    },'+
		'    {'+
		'      \"nbnorderid\": \"ORD010004399549\",'+
        '      \"accessseekername\": \"Telstra\",'+            
		'      \"asreferenceid\": \"24334\",'+
		'      \"orderstatus\": \"Inprogress\",'+
		'      \"dateacknowledged\": \"2017-10-11T10:29:00\",'+
		'      \"dateaccepted\": \"2017-10-11T10:29:00\",'+
		'      \"datecompleted\": \"2017-10-11T10:29:00\",'+
		'      \"orderserviceabilityclass\": \"12\",'+
		'      \"sla\": \"\",'+
		'      \"appdetails\": [{'+
		'        \"appointmentId\": \"APT100000045565\",'+
		'        \"nbnslottype\": \"08-12\",'+
		'        \"nbndemandtype\": \"STANDARD INSTALL\",'+
		'        \"status\": \"Cancel\",'+
		'		\"nbnappstart\": \"2018-05-08T11:26:10\",'+
		'        \"nbnappend\": \"2018-05-08T11:26:10\",'+
		'        \"nbnappstartlocal\": \"2018-05-08T00:00:00\",'+
		'        \"nbnappstartlocaltime\": \"2018-05-08T08:00:00\",'+
		'        \"nbnappendlocal\": \"2018-05-08T00:00:00\",'+
		'        \"nbnappendlocaltime\": \"2018-05-08T12:00:00\",'+
		'		\"wodetails\": [{'+
		'          \"woid\": \"WOR100000201068\",'+
		'          \"nbnworkforce\": \"DP1019\",'+
		'          \"woparent\": \"WOR100000201055\",'+
		'          \"wostatus\": \"CANCEL\",'+
		'          \"woreasoncode\": \"ORDER-CAN\",'+
		'          \"woprd\": \"2018-05-08T11:16:44+1000\",'+
		'          \"wopriority\": \"STANDARD\"'+
		'        }]'+
		'      }]'+
		'    }'+
		'  ]'+
        ' }'+
		'}';
        
		SDM_IntegrationWrapper obj = SDM_IntegrationWrapper.parse(json);
		System.assert(obj != null);
	}
}