/**
 * Apex Controller for looking up an SObject via SOSL
 */
public with sharing class SDM_LookupSObjectController 
{
    /**
     * Aura enabled method to search a specified SObject for a specific strings
     */
    @AuraEnabled
    public static List<SObject> lookup(String searchString, String sObjectAPIName, Integer maxRecordLimit, String[] fieldList,String filterField,String filterRecords,String primaryFieldAPI)
    {
        
        String finalSearchString = String.escapeSingleQuotes(searchString);
        String finalSObjectAPIName = String.escapeSingleQuotes(sObjectAPIName);
        String filterFieldAPI;
        if(String.isBlank(filterField)){
            filterFieldAPI='Name';
        }else{
            filterFieldAPI=filterField;
        }
        String primaryField = '';
        if(String.isBlank(primaryFieldAPI)){
            primaryField='Name';
        }else{
            primaryField=primaryFieldAPI;
        }
           
        String fields  = '';
        if(fieldList != null && fieldList.size() > 0)
        {
            fields  = ',';
            Integer j = 1;
            for (String i: fieldList) {
                fields = fields + i;
                if(j<fieldList.size())
                {
                    fields = fields + ',';
                    j=j+1;
                }
            }
        }
        String NameField = '';
        if(!(primaryField.containsIgnoreCase('name')) && !(fields.containsIgnoreCase('name'))){
            NameField = 'Name,';
        }
        String filterCondition = '';
        if(String.IsNotBlank(filterRecords)){
            filterCondition = ' AND ' + filterRecords;
        }
        if(finalSearchString == '' || finalSearchString .length() == 1){
            String query = 'select Id,' + NameField + primaryField + fields  + ' from ' + finalSObjectAPIName  + ' where '+ filterFieldAPI + ' like \'%' + finalSearchString + '%\' AND LastViewedDate !=null' + filterCondition  +' ORDER BY LastViewedDate DESC limit '+ maxRecordLimit ;
            system.debug('-----'+query);

            return Database.query(query);
        }
        else{
            
            String query = 'select Id,' + NameField + primaryField + fields  + ' from ' + finalSObjectAPIName  + ' where '+ filterFieldAPI + ' like \'%' + finalSearchString + '%\'' + filterCondition  +' limit '+ maxRecordLimit ;
            system.debug('-----'+query);
            
            return Database.query(query);
        }
        
    }
}