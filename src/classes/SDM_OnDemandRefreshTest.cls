@isTest
public class SDM_OnDemandRefreshTest{    

@testSetup static void loadTestDataForCustomSettings()
    {
        List<sObject> lstNbnintegrationinputs = Test.loadData(NBNIntegrationInputs__c.sObjectType, 'NBNIntegrationInputsTestData');
        List<sObject> lstNbnintegrationErrors = Test.loadData(NBNIntegationErrors__c.sObjectType, 'NBNIntegrationErrors');
        List<sObject> sdmIntegrationParams = Test.loadData(SDM_CISIntegrationParams__c.sObjectType, 'SDM_IntegrationParams');
    }
    
testmethod static void successRefreshOnDemand() 
{

IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();
  
  mockResp.strResponseType = 'LocationOrders';    
  mockResp.status= Label.Success;            
  mockResp.StatusCode = 200;  
    //setMockRes Class
    Test.setMock(HttpCalloutMock.class, mockResp);
    Test.StartTest();
    Integer responseCode = SDM_OnDemandRefreshController.refreshOpptyOrders('LOC000000206007');
    Test.StopTest();
    
    system.assertEquals(responseCode,200);


}

/*testmethod static void FailureRefreshOnDemand() 
{

   IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();
      
      mockResp.strResponseType = 'LocationOrders';
        mockResp.status= Label.Failure;
        mockResp.StatusCode = 404;
        //setMockRes Class
        Test.setMock(HttpCalloutMock.class, mockResp);            
        Test.StartTest();
        Integer responseCode = SDM_OnDemandRefreshController.refreshOpptyOrders('LOC000000000000'); 
        Test.StopTest();
        system.assertEquals(responseCode,404);


}*/


}