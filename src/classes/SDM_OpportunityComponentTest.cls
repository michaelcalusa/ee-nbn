@isTest
public class SDM_OpportunityComponentTest {
    
    //@testSetup
    testmethod static void testOpptyCancellation(){
        Id rectype = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pre-Order').getRecordTypeId();
        Opportunity objOpp =  new Opportunity ( Name='Test Opp', CloseDate=System.today(), StageName = 'New', 
                                               recordTypeId = rectype);
       insert objOpp;
        SDM_OpptyButtonController.updateOpptyStage(objOpp.id);
        SDM_OpptyButtonController.cancelOppty(objOpp.id);
    }
}