/***************************************************************************************************
Class Name:         SDM_OpportunityMassOwnerHandler
Class Type:         Manages mass owner change for opportunities in list view and 
                    related list
Company :           Appirio
Created Date:       4 Oct 2018
Created By:         Sunaiyana Thakuria and Anurag Sharma
****************************************************************************************************/
Public class SDM_OpportunityMassOwnerHandler{
    
    public ApexPages.StandardSetController standardController;
    
    public String lst_opps{get;set;}
    public Boolean showInfo{get;set;}
    public String filterName{get;set;}
    
    public SDM_OpportunityMassOwnerHandler(ApexPages.StandardsetController stdController){
        this.standardController = stdController;
    }
    
    public void selectedOpportunities(){
        showInfo = false;
        List<Opportunity> eligibleOpps = new List<Opportunity>();
        
        //Getting parameter to redirect lightning component to previous page after owner update
        filterName = ApexPages.CurrentPage().getParameters().get(Constants.redirectParam);
        
        //Getting logged in user details to selectively let user change owners on basis of assigened role
        Id userId = UserInfo.getUserId();
        String roleName = [select UserRole.name from user where id=:userId].UserRole.name;
        
        // Get the selected records
        List<Opportunity> selectedOpps = (List<Opportunity>)standardController.getSelected();    
        
        //Handles owner update for requests generating from related list in Account layout
        if(filterName==null && selectedOpps.size()==0){
            String id = ApexPages.CurrentPage().getParameters().get('id');
            eligibleOpps = [select id, Reference_Number__c,name, owner.name, ownerid from Opportunity where accountid =:id ];
            
            //Segregates eligible opportunities based on role 
            if(eligibleOpps.size()>0){
                Boolean isEligible  = false;
                if(roleName==Constants.roleName)
                    isEligible = true;
                else
                    for(Opportunity o : eligibleOpps){
                        if(o.ownerid==userId){
                            isEligible = true;
                            break;
                        }
                    }
                if(isEligible)
                    lst_opps = json.serialize(eligibleOpps);
                else{
                    eligibleOpps = new List<Opportunity>();
                    lst_opps = json.serialize(eligibleOpps);
                }
            }
            
            filterName = '/' + id;
        }
        //Handles owner change requests generating from list view
        else if(roleName==Constants.roleName){
            lst_opps = json.serialize([select id, Reference_Number__c,name, owner.name, ownerid from Opportunity where id IN:selectedOpps ]);
        }
        
        else{
            eligibleOpps = [select id, Reference_Number__c,name, owner.name, ownerid from Opportunity where id IN:selectedOpps and ownerid=:userId];
            lst_opps = json.serialize(eligibleOpps );
            
            if(eligibleOpps.size() < selectedOpps.size()){
                showInfo = true;
            }
        }
        
    }
    
    //Checks if selected owner for update belongs to SDM profile and handles update accordingly
    @AuraEnabled
    public static String UpdateOppOwners(List<String> updatedOpps){
        // Do update here  
        try{        
            List<Opportunity> toUpdateOppList = new List<Opportunity>();
            Opportunity o = new Opportunity();
            Id newOwnerId;
            
            for(Integer i = 0;i<updatedOpps.size();i++){
                o =  (Opportunity)JSON.deserialize(updatedOpps[i],Opportunity.class);
                newOwnerId = o.ownerid;
                toUpdateOppList.add(o);
            }
            
            List<User> noneligibleOpps = [select id,profile.name from user where profile.name!=:Constants.profileName and id = : newOwnerId limit 1];
            
            //reinitialising the to update list if the selected user for update holds a profile 
            //other than "SDM User Profile"
            if(noneligibleOpps!=null && noneligibleOpps.size()>0){
                toUpdateOppList = new List<Opportunity>();
            }
            
            update toUpdateOppList;
            
            if(toUpdateOppList.size()>0)
                return 'success';
            else
                return 'fail';
        }
        catch(Exception ex){
            return ex.getMessage();
        }
        
    }
    
    @AuraEnabled
    public static String checkEligibility(String userId){
        
        String roleName = [select UserRole.name from user where id=:userId].UserRole.name;
        return roleName;
    }
    
}