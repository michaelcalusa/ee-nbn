Global class SDM_OpptyButtonController  {
	
    @AuraEnabled
    public static void updateOpptyStage(Id recId){
        List<Opportunity> oppty = [select stagename from opportunity where id=:recId];
        if(oppty.size()>0){
            oppty[0].stagename = 'Cancelled';
            update oppty;
        }
    }
    
    webservice static void cancelOppty(Id recId){
        List<Opportunity> oppty = [select stagename from opportunity where id=:recId];
        if(oppty.size()>0){
            oppty[0].stagename = 'Cancelled';
            update oppty;
        }
    }
}