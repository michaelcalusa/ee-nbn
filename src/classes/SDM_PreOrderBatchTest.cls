@isTest 
public class SDM_PreOrderBatchTest {
    
    static testMethod void testMethod1() 
    {
        Account acc = new Account();
        acc.Name = 'test';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.account_contact_recordType).getRecordTypeId();
        insert acc;
        
        SDM_PreOrder_Staging__c testData = new SDM_PreOrder_Staging__c();
        testData.SDM_Account__c = acc.id;
        testData.SDM_Primary_Contact_Name__c = 'John Mcmohan';
        testData.SDM_Secondary_Contact_Name__c= 'Julie Mcmohan';
        testData.SDM_Primary_Contact_Number__c= '9999988888';
        testData.SDM_Secondary_Contact_Number__c= '9999988888';
        
        insert testData;
        
        Test.startTest();
        	new Constants();
            SDM_PreOrderDataLoaderBatch obj = new SDM_PreOrderDataLoaderBatch();
            String sch = '0 30 * * * ?';
            system.schedule('Pre Order Data Purging Batch every 30 mins', sch, obj);
            DataBase.executeBatch(obj);            
        
        Test.stopTest();
        System.assertEquals(1, [Select id from Opportunity].size());
    }
    
    static testMethod void testMethod2() 
    {
        Account acc = new Account();
        acc.Name = 'test';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(Constants.account_contact_recordType).getRecordTypeId();
        insert acc;
        
        SDM_PreOrder_Staging__c testData = new SDM_PreOrder_Staging__c();
        testData.SDM_Account__c = acc.id;
        testData.SDM_Primary_Contact_Name__c = 'John Mcmohan';
        testData.SDM_Secondary_Contact_Name__c= 'Julie Mcmohan';
        testData.SDM_Primary_Contact_Number__c= '9999988888';
        testData.SDM_Secondary_Contact_Number__c= '9999988888';
        
        insert testData;
        
        
        Datetime oldDate = Datetime.now().addDays(-5);
        Test.setCreatedDate(testData.Id, oldDate);
        
        Test.startTest();            
            
            SDM_PreOrderDataPurgingBatch obj2 = new SDM_PreOrderDataPurgingBatch();            
            String sch = '0 30 * * * ?';
            system.schedule('Pre Order Data Purging Batch every 30 mins', sch, obj2);       
            
            DataBase.executeBatch(obj2);
            
        Test.stopTest();
        System.assertEquals(0, [Select id from Opportunity].size());
    }
}