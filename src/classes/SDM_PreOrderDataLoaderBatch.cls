/***************************************************************************************************
Class Name:         SDM_PreOrderDataLoaderBatch
Class Type:         Executes every 10 minutes to get staging records to pre orders
Company :           Appirio
Created Date:       29 Aug 2018
Created By:			Sunaiyana Thakuria
****************************************************************************************************/
global class SDM_PreOrderDataLoaderBatch implements Database.Batchable<sObject>,Database.Stateful,Schedulable{
    
    global Integer createdRecordsCount;
    global Integer updatedRecordsCount;
    global Integer FailureRecordsCount;
    global String errors;
    global String csvBody;
    global String emailOfOwner;
    global String nameOfOwner;
    global Map<String,String> preOrderIdPerRef;
    
    //initialising global variables for emailing batch execution related data
    global SDM_PreOrderDataLoaderBatch(){
        createdRecordsCount = 0;
        updatedRecordsCount = 0;
        FailureRecordsCount = 0;
        errors = '';
        emailOfOwner = '';
        nameOfOwner = '';
        preOrderIdPerRef = new Map<String,String>();
        csvBody = 'Opportunity Reference Id,Account Name,Business Trading Name,Project Code,RSP Site ID,'+
            'Customer Site Address,NBN Location ID,NBN Tech Type,Primary Contact,Primary Contact Number,'+
            'Secondary Contact,Secondary Contact Number,NBN Order ID,RSP Planned Cutover Date,'+
            'RSP Actual Cutover Date,RSP Cutover Status,Site Details,Special Conditions,RSP Name,'+
            'Proposed Resolution Date,RFS Date\n';
        
    }
    
    global void execute(SchedulableContext sc){
        SDM_PreOrderDataLoaderBatch job = new SDM_PreOrderDataLoaderBatch();
        Database.executeBatch(job,300);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //Updated query(Added Site_Details__c & Special_Conditions__c) for CCT-1389
        String query = 'SELECT SDM_Account__c,owner.email,SDM_Primary_Contact_Name__c, SDM_Primary_Contact_Number__c,SDM_Secondary_Contact_Name__c, SDM_Secondary_Contact_Number__c, SDM_Business_Trading_Name__c, SDM_Customer_Site_Address__c, '+
            'SDM_nbn_install_date__c,Opportunity_Reference_Id__c,SDM_NBN_Location_ID__c, SDM_NBN_Order_ID__c, SDM_NBN_Tech_Type__c, OwnerId, '+
            'Name, SDM_Project_Code__c, SDM_RSP_Actual_Cutover_Date__c, '+
            'SDM_RSP_CutOver_Status__c, RSP_Planned_Cutover_Date__c, SDM_RSP_Site_ID__c,'+
            //Add new fields here by Wayne
            'RSP_Order_Reference_ID__c, BEARs_Verified__c, Description__c,'+
            //end here
            'Processed__c, SDM_RFS_Date__c,owner.firstName, SDM_RSP_Name__c,SDM_Planned_Resolution_Date__c,owner.lastName, Site_Details__c, Special_Conditions__c FROM SDM_PreOrder_Staging__c where Processed__c=false';// and createddate>=:startTime';
        return Database.getQueryLocator(query);
    }
    
    //Maps all nencessary fields and creates pre order data corresponding to each staging record
    global void execute(Database.BatchableContext BC, List<SDM_PreOrder_Staging__c> scope)
    {
        List<Opportunity> preOrders = new List<Opportunity>();
        List<id> oppIds = new List<Id>();
        Opportunity oppty = new Opportunity();
        List<String> locationIds = new List<String>();
        List<String> refIds = new List<String>();
        Map<String,Id> siteIdPerStaging = new Map<String,Id>();
        Map<String,Id> primaryContactsPerStaging = new Map<String,Id>();
        Map<String,Id> secondaryContactsPerStaging = new Map<String,Id>();
        Map<String,Id> opptyToUpdate = new Map<String,Id>();
        Map<String,String> primaryContacts = new Map<String,String>();
        Map<String,String> secondaryContacts = new Map<String,String>();
        Map<String,String> primaryContactsNumber = new Map<String,String>();
        Map<String,String> secondaryContactsNumber = new Map<String,String>();
        
        Id recordTypeForOppty = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Constants.SDM_OPPORTUNITY_RECORDTYPE).getRecordTypeId();
        if(scope.size()>0){
            emailOfOwner = scope[0].owner.email;
            if(scope[0].owner.firstName!=null)
                nameOfOwner = scope[0].owner.firstName + ' ';
            nameOfOwner = nameOfOwner + scope[0].owner.lastName;
        }
        //Getting all location IDs for site mapping
        for(SDM_PreOrder_Staging__c pos : scope){         
            locationIds.add(pos.SDM_NBN_Location_ID__c);
            if(pos.Opportunity_Reference_Id__c!=null)
                refIds.add(pos.Opportunity_Reference_Id__c);
            if(pos.SDM_Secondary_Contact_Name__c!=null){
            	secondaryContacts.put(pos.SDM_Secondary_Contact_Number__c+pos.SDM_Secondary_Contact_Name__c.toUpperCase(),pos.SDM_Secondary_Contact_Name__c);
            	secondaryContactsNumber.put(pos.SDM_Secondary_Contact_Number__c+pos.SDM_Secondary_Contact_Name__c.toUpperCase(),pos.SDM_Secondary_Contact_Number__c);
            }
            if(pos.SDM_Primary_Contact_Name__c!=null){
            	primaryContacts.put(pos.SDM_Primary_Contact_Number__c+pos.SDM_Primary_Contact_Name__c.toUpperCase(),pos.SDM_Primary_Contact_Name__c);
            	primaryContactsNumber.put(pos.SDM_Primary_Contact_Number__c+pos.SDM_Primary_Contact_Name__c.toUpperCase(),pos.SDM_Primary_Contact_Number__c);
            }
            }
        
        if(locationIds.size()>0){
            //Calling method to return all the site ids for every location ID
            siteIdPerStaging = addSiteMapping(locationIds);            
        }
        
        if(primaryContacts.size()>0){
            //Calling method to return all the contact ids for every primary phone number
            primaryContactsPerStaging = addContactMapping(primaryContacts,primaryContactsNumber);
        }
        
        if(secondaryContacts.size()>0){
            //Calling method to return all the contact ids for every secondary phone number
            secondaryContactsPerStaging = addContactMapping(secondaryContacts,secondaryContactsNumber);
        }
        
        //Separating existing opportunities for update
        for(Opportunity o : [Select id,reference_number__c from Opportunity where reference_number__c in : refIds and RecordTypeId =:recordTypeForOppty]){
            opptyToUpdate.put(o.reference_number__c, o.id);
        }
        
        Set<Id> opptyIds = new Set<Id>();
        
        //Mapping all fields for upsert
        for(SDM_PreOrder_Staging__c pos : scope){
            oppty = new Opportunity();
            
            if(pos.Opportunity_Reference_Id__c!=null && opptyToUpdate.size()>0 && opptyToUpdate.containsKey(pos.Opportunity_Reference_Id__c) && !(opptyIds.size()>0 && opptyIds.contains(opptyToUpdate.get(pos.Opportunity_Reference_Id__c)))) {
                oppty.id = opptyToUpdate.get(pos.Opportunity_Reference_Id__c);
                opptyIds.add(opptyToUpdate.get(pos.Opportunity_Reference_Id__c));
            }
            //Mapping fields for record creation and exclusion from update
            else{
                oppty.StageName = Constants.stagesValue;
                oppty.CloseDate = system.today().addMonths(1);
                oppty.RecordTypeId = recordTypeForOppty;
                oppty.Name = pos.Name;
                oppty.OwnerId = pos.OwnerId;
            }
            //Added for Story-1527 #Start
            oppty.SDM_RSP_Name__c = pos.SDM_RSP_Name__c;
            oppty.SDM_Planned_Resolution_Date__c = pos.SDM_Planned_Resolution_Date__c;
            oppty.SDM_RFS_Date__c = pos.SDM_RFS_Date__c;
            //Added for Story-1527 #End
            
            //Added for Story MSEU-21043 #Start
            oppty.RSP_Order_Reference_ID__c = pos.RSP_Order_Reference_ID__c;
            oppty.Description = pos.Description__c;
            oppty.BEARs_Verified__c = pos.BEARs_Verified__c;            
            //Added for Story MSEU-21043 #End 
            
            oppty.AccountId = pos.SDM_Account__c;
            oppty.SDM_Customer_Site_Address__c = pos.SDM_Customer_Site_Address__c;
            oppty.SDM_nbn_install_date__c = pos.SDM_nbn_install_date__c;
            oppty.SDM_NBN_Location_ID__c = pos.SDM_NBN_Location_ID__c;
            oppty.SDM_nbn_Order_ID__c = pos.SDM_NBN_Order_ID__c;
            oppty.SDM_nbn_Tech_type__c = pos.SDM_NBN_Tech_Type__c;
            oppty.SDM_Actual_Cutover_Date__c = pos.SDM_RSP_Actual_Cutover_Date__c;
            oppty.RSP_Planned_Cutover_Date__c = pos.RSP_Planned_Cutover_Date__c;
            oppty.SDM_nbn_install_date__c = pos.SDM_nbn_install_date__c;
            oppty.SDM_Business_Trading_name__c = pos.SDM_Business_Trading_Name__c;
            oppty.RSP_CutOver_Status__c = pos.SDM_RSP_CutOver_Status__c;
            oppty.PreOrder_Staging_Id__c = pos.id;
            oppty.SDM_Project_Code__c = pos.SDM_Project_Code__c;
            oppty.SDM_RSP_Site_ID__c = pos.SDM_RSP_Site_ID__c;
            oppty.Site_Details__c = pos.Site_Details__c;
            oppty.Special_Conditions__c = pos.Special_Conditions__c;
            //Added for CCT-1389 #End
            if(siteIdPerStaging.size()>0){
                oppty.Site__c = siteIdPerStaging.get(pos.SDM_NBN_Location_ID__c);
            }          
            
            if(primaryContactsPerStaging.size()>0 && (pos.SDM_Primary_Contact_Name__c!=null && pos.SDM_Primary_Contact_Name__c!=''))
                oppty.Primary_Contact__c = primaryContactsPerStaging.get(pos.SDM_Primary_Contact_Number__c+pos.SDM_Primary_Contact_Name__c.toUpperCase());
            if(secondaryContactsPerStaging.size()>0 && (pos.SDM_Secondary_Contact_Name__c!=null && pos.SDM_Secondary_Contact_Name__c!=''))
                oppty.Secondary_Contact__c = secondaryContactsPerStaging.get(pos.SDM_Secondary_Contact_Number__c+pos.SDM_Secondary_Contact_Name__c.toUpperCase());
            
            preOrders.add(oppty);
            
        }
        system.debug('CheckPre>>>> '+preOrders);
        
        //Getting all the inserted record ids for output file generation and error detection
        if(preOrders.size() > 0){
            try{
                Database.UpsertResult[] results = Database.upsert(preOrders,false);
                
                for(Integer i= 0, size = results.size(); i < size; i++) {
                    if(results[i].isSuccess()) {
                        if(results[i].isCreated()) {
                            createdRecordsCount++;
                            oppIds.add(results[i].getId());
                        } else {
                            updatedRecordsCount ++;
                            oppIds.add(results[i].getId());
                        }
                    }
                    else{
                        for(Database.Error err : results[i].getErrors()){
                            errors += err.getMessage()+' , ';
                        }
                        FailureRecordsCount ++;
                    }
                }       
                
                //Creating csv file body for emailing
                List<Opportunity> opptysInserted = [select id,SDM_RFS_Date__c,SDM_Planned_Resolution_Date__c,SDM_RSP_Name__c,Site_Details__c,Special_Conditions__c,PreOrder_Staging_Id__c,name,SDM_Business_Trading_Name__c,SDM_Customer_Site_Address__c,SDM_nbn_install_date__c,SDM_NBN_Location_ID__c,SDM_NBN_Order_ID__c,SDM_NBN_Tech_Type__c,SDM_Project_Code__c,SDM_Actual_Cutover_Date__c,RSP_Planned_Cutover_Date__c,SDM_RSP_Site_ID__c,RSP_CutOver_Status__c,reference_number__c,Account.name,Primary_Contact__r.name,Primary_Contact__r.phone,Secondary_Contact__r.name,Secondary_Contact__r.phone from Opportunity where id in : oppIds];
                DateTime d = System.now();
                String cutoverDate = '';
                String actualDate = '';
                String prDate = '';
                String rfpDate = '';
                
                if(opptysInserted.size()>0)
                    for(Opportunity o:opptysInserted){
                        cutoverDate = '';
                        actualDate = '';
                        prDate = '';
                        rfpDate = '';
                        if(o.RSP_Planned_Cutover_Date__c!=null){
                            d = o.RSP_Planned_Cutover_Date__c;
                           // cutoverDate = DateTime.newInstance(d.year(),d.month(),d.day(),d.hour(),d.minute(),d.second()).format('yyyy-MM-dd\'T\'HH:mm:ss');
                           cutoverDate = d.format('dd/MM/yyyy'); 
                        }
                        if(o.SDM_Actual_Cutover_Date__c!=null){
                            d = o.SDM_Actual_Cutover_Date__c;
                          //  actualDate = DateTime.newInstance(d.year(),d.month(),d.day(),d.hour(),d.minute(),d.second()).format('yyyy-MM-dd\'T\'HH:mm:ss');
                          actualDate = d.format('dd/MM/yyyy'); 
                        }
                        if(o.SDM_Planned_Resolution_Date__c!=null){
                            d = o.SDM_Planned_Resolution_Date__c;
                           // prDate = DateTime.newInstance(d.year(),d.month(),d.day(),0,0,0).format('yyyy-MM-dd\'T\'HH:mm:ss');
                           prDate = d.format('dd/MM/yyyy');
                        }
                        if(o.SDM_RFS_Date__c!=null){
                            d = o.SDM_RFS_Date__c;
                           // rfpDate = DateTime.newInstance(d.year(),d.month(),d.day(),0,0,0).format('yyyy-MM-dd\'T\'HH:mm:ss');
                           rfpDate = d.format('dd/MM/yyyy');
                        }
                        preOrderIdPerRef.put(o.PreOrder_Staging_Id__c, o.Reference_Number__c);
                        csvBody = csvBody + o.Reference_Number__c+',"'+ o.Account.name+'","'+ o.SDM_Business_Trading_Name__c
                            +'","'+ o.SDM_Project_Code__c+'","'+ o.SDM_RSP_Site_ID__c+'","'+o.SDM_Customer_Site_Address__c
                            +'",'+o.SDM_NBN_Location_ID__c+','+ o.SDM_NBN_Tech_Type__c
                            +',"'+ o.Primary_Contact__r.name +'",'+ o.Primary_Contact__r.phone
                            +',"'+ o.Secondary_Contact__r.name +'",'+ o.Secondary_Contact__r.phone
                            +',"'+o.SDM_NBN_Order_ID__c+'",'+ cutoverDate +','+ actualDate
                            +','+ o.RSP_CutOver_Status__c +',"'+ o.Site_Details__c
                            +'","'+ o.Special_Conditions__c +'","'+ o.SDM_RSP_Name__c 
                            +'",'+ prDate +','+ rfpDate +'\n';
                    }
                
                csvBody = csvBody.replace('null', '');
            }catch(Exception e){
                system.debug('***Oppty mapping'+e.getLineNumber()+e.getMessage());
            }
        }
    }  
    
    //Querying and sorting site ID per location ID
    public Map<String,Id> addSiteMapping(List<String> locationIds){
        try{
            Map<String,Id> siteIdPerStaging = new Map<String,Id>();           
            if(locationIds.size()>0)                
                for(Site__c s : [select id,Location_Id__c from Site__c where Location_Id__c in: locationIds]){                    
                    siteIdPerStaging.put(s.Location_Id__c,s.id);
                }
            
            return siteIdPerStaging;
        }Catch(Exception e){
            system.debug('***Site Mapping'+e.getLineNumber()+e.getMessage());
            return null;
        }
    }
    
    //Querying and sorting Contact ID per Contact
    public Map<String,Id> addContactMapping(Map<String,String> contacts,Map<String,String> contactsNumbers){
        Map<String,Id> contactsPerStaging = new Map<String,Id>();
                Id recordTypeForContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.account_contact_recordType).getRecordTypeId();   
        try{
            Map<String,String> contactsToCreate = new Map<String,String>();
            Map<String,String> contactsToCreateNumbers = new Map<String,String>();
            Boolean isMatchFound  = false;
            
            if(contacts.size()>0)
                for(Contact c : [select id,Phone,name from contact where Phone in: contactsNumbers.values() and Name in : contacts.values() and recordtypeid=:recordTypeForContact]){
                    if(c.name!=null && c.name!='')
                    contactsPerStaging.put(c.Phone+c.name.toUpperCase(),c.id);
                }           
            
            for(String phone : contacts.keySet()){
                if(!contactsPerStaging.containsKey(phone)){
                    if(contacts.get(phone)!=null && contacts.get(phone)!=''){
                    	contactsToCreate.put(contactsNumbers.get(phone)+contacts.get(phone).toUpperCase(),contacts.get(phone));
                    	contactsToCreateNumbers.put(contactsNumbers.get(phone)+contacts.get(phone).toUpperCase(),contactsNumbers.get(phone));
                    }
                    }
            }
            Map<String,id> temp = new Map<String,id>();
            temp = createMissingContacts(contactsToCreate,contactsToCreateNumbers);
            if(temp!=null && temp.size()>0)
                contactsPerStaging.putAll(temp);
            return contactsPerStaging;
        }Catch(Exception e){
            system.debug('***###Error captured at line ' + e.getlinenumber() + '  Details : ' + e.getmessage());
            return contactsPerStaging;
        }
    }
    
    //Based on name, phone number and record type creates new contacts for mapping with pre orders
    public Map<String,Id> createMissingContacts(Map<String,String> contacts,Map<String,String> contactsNumbers){
        Map<String,Id> contactIdMapping = new Map<String,Id>();
        Id recordTypeForContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(Constants.account_contact_recordType).getRecordTypeId();   
        String[] fullName = new List<String>();                   
        String FirstName = '';
        String LastName = '';
        Contact con = new Contact();
        List<Contact> contactsToInsert = new List<Contact>();
        String tempName = '';
        
        for(String phone : contacts.keySet()){
            FirstName = '';
            LastName = '';
            tempName = contacts.get(phone);
            if(tempName!=null && tempName!=''){
                fullName = contacts.get(phone).split(' ');
                if(fullName!=null){
                    if(fullName.size()==1){
                        FirstName = '';
                        LastName = fullName[0];
                    }
                    else if(fullName.size()>1){
                        FirstName = contacts.get(phone).substringBeforeLast(' ');
                        LastName = contacts.get(phone).substringAfterLast(' ');
                    }
                }
            }
            
            if(LastName!=null && LastName!=''){
                con = new Contact(FirstName=FirstName, LastName =LastName,Phone=contactsNumbers.get(phone), recordtypeid = recordTypeForContact );
                contactsToInsert.add(con);       
            }
        }
        if(contactsToInsert.size()>0)
            insert contactsToInsert;
        String key = '';
        for(Contact c : contactsToInsert){
            if(c.FirstName!=null && c.FirstName!='')
                key = c.phone+c.FirstName.toUpperCase()+' '+c.LastName.toUpperCase();
            else
                key = c.phone+c.LastName.toUpperCase();
            contactIdMapping.put(key, c.id);
        }
        system.debug('****ContactIdMapping******* '+contactIdMapping);
        return contactIdMapping;
        
    }
    
    //Consolidates all the newly created/updated records into a response file and emails it to the uploading user
    //this provides the newly created reference id and helps them in updating any fields in previously created records
    //as required
    global void finish(Database.BatchableContext BC)
    {
        AsyncApexJob a = [SELECT Id,Status,JobType,NumberOfErrors,JobItemsProcessed,TotalJobItems,CompletedDate,ExtendedStatus
                          FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        
        List<String> toAddresses = new List<String>();
        
        //sending email to support team in case of errors
        if(((a!=null && a.NumberOfErrors>0) || FailureRecordsCount>0) && Label.SDM_PreLoaderBatchSupport!='')    
            toAddresses = Label.SDM_PreLoaderBatchSupport.split(',');  
        if(Test.isRunningTest()){
            emailOfOwner = 'Test@test.com';
        }
        if((createdRecordsCount+updatedRecordsCount+FailureRecordsCount)>0 && emailOfOwner!=''){
            toAddresses.add(emailOfOwner);
            //}  
            List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();
            DateTime d = System.now();
            String temp = DateTime.newInstance(d.year(),d.month(),d.day(),d.hour(),d.minute(),d.second()).format('dd-MM-yyyy HH.mm.ss');
            String filename = 'PREORDER_by_'+nameOfOwner+'_RESPONSE_'+temp+'.csv';
            
            //creating attachement for inserted records
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(filename);
            efa.setBody(Blob.valueOf(csvBody));
            attachments.add(efa);
            
            String body = 'The batch Apex job completed on  ' + a.CompletedDate + ',<br/><br/>' +
                'Job Status : ' + a.Status + '<br/>'+
                'Total Job Items processed : ' + a.TotalJobItems + '<br/>'+
                'Number of Job Items processed : ' + a.JobItemsProcessed + '<br/>' +
                'Number of Failures : '+ a.NumberOfErrors+ '<br/><br/><br/>' +
                'Total number of records created : '+  createdRecordsCount + '<br/>'+
                'Total number of records updated : '+  updatedRecordsCount + '<br/>' +
                'Total number of records failed : '+ FailureRecordsCount + '<br/><br/>';            
            if(errors!=''){
                body = body + 'Errors: '+ errors + '<br/><br/>' +
                    'The support team will get back to you once the error is fixed. <br/><br/>' ;
            }
            
            String subject = 'Batch Job for Pre Order data load with job id - '+ BC.getJobId();
            if(toAddresses.size()>0)
                SDM_EmailUtilityClass.sendEmailWithAttachments(toAddresses,body,subject,attachments);
            
            //Calling a second batch to sync ref id to pre orders
            if(preOrderIdPerRef.size()>0){
                SDM_PreOrderUpdateBatch job = new SDM_PreOrderUpdateBatch(preOrderIdPerRef);
                Database.executeBatch(job,300);
            }
        }
    }        
}