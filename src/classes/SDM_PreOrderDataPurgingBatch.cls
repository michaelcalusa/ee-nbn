global class SDM_PreOrderDataPurgingBatch implements Database.Batchable<sObject>,Schedulable{
    
    global void execute(SchedulableContext sc){
        SDM_PreOrderDataPurgingBatch job = new SDM_PreOrderDataPurgingBatch();
        Database.executeBatch(job);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id FROM SDM_PreOrder_Staging__c WHERE CreatedDate < LAST_N_DAYS:' + Label.SDM_Number_of_Days_To_Purge_Records;
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<SDM_PreOrder_Staging__c> scope)
    {
        delete scope;
        
    }  
    
    global void finish(Database.BatchableContext BC)
    {
        
    }  
}