/***************************************************************************************************
Class Name:         SDM_PreOrderUpdateBatch
Class Type:         Updates the Opportunity reference id into the staging object records
Company :           Appirio
Created Date:       29 Aug 2018
Created By:			Sunaiyana Thakuria
****************************************************************************************************/
global class SDM_PreOrderUpdateBatch implements Database.Batchable<sObject>,Database.Stateful{
   
    global Map<String,String> preorderIds;
    
    global SDM_PreOrderUpdateBatch(Map<String,String> preorderIdsPerRef){
        preorderIds = new Map<String,String>();
        preorderIds = preorderIdsPerRef; 
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Set<String>  temp = new Set<String>();
        if(preorderIds.size()>0)
        temp = preorderIds.keySet();

        String query = 'SELECT Processed__c,Opportunity_Reference_Id__c,id FROM SDM_PreOrder_Staging__c where id in : temp ';
        return Database.getQueryLocator(query);
    }
    
    //Once the data has been moved to pre order, reference id gets created and 
    //then copied to corresponding staging records
    global void execute(Database.BatchableContext BC, List<SDM_PreOrder_Staging__c> scope)
    {
        for(SDM_PreOrder_Staging__c pos : scope){
            pos.Opportunity_Reference_Id__c = preorderIds.get(pos.id);
            pos.Processed__c = true;
        }
        try{
            Database.update(scope,false);
        }
        catch(Exception e){
            system.debug('---ERROR---'+e.getlinenumber()+'--'+e.getMessage());
        }
    }  
    
    
    global void finish(Database.BatchableContext BC)
    {
        
    }        
    
}