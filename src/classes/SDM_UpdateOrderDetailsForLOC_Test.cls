/***************************************************************************************************
Class Name:         SDM_UpdateOrderDetailsForLOC_Test
Class Type:         Test Class for SDM_UpdateOrderDetailsForLOC
Company :           Appirio
Created Date:       1 Oct 2018
Created By:         Shubham Paboowal
****************************************************************************************************/
@isTest
public class SDM_UpdateOrderDetailsForLOC_Test {
    static testMethod void testParsingLogicForCreate(){
        String json = '{'+
        '   \"data\":{  '+
        '  \"locationid\": \"LOC000000206007\",'+
        ' \"serviceabilitydate\":\"21 FEB 2030 13:12:12\",'+
        ' \"disconnectiondate\":\"2017-10-11T10:28:00\",'+
        '  \"accessseekerdetails\": {'+
        '    \"accessseekerid\": \"ASI000000220282\",'+
        '    \"accessseekername\": \"Telstra\"'+
        '  },'+
        '  \"orderdetails\": ['+
        '    {'+
        '     \"avc\": [{\"avctype\" : \"AVC-V\",\"id\" : \"AVC000079068817\",\"bandwidth\": \"D0.15_U0.15_Mbps_TC1_C\" },{\"avctype\" : \"AVC-D\",\"id\" : \"AVC000079068818\",\"bandwidth\": \"“D12_U1_Mbps_TC4_P, D12_U1_Mbps_TC2_P, D0.15_U0.15_Mbps_TC1_C\" },{\"avctype\" : \"AVC-M\",\"id\" : \"AVC0000790688198\",\"bandwidth\": \"D0.15_U0.15_Mbps_TC1_C\" }],'+
        '     \"accessseekerid\": \"ASI000000220282\",'+
        '     \"accessseekername\": \"Telstra\",'+
        '      \"nbnorderid\": \"ORD010004399548\",'+
        '      \"asreferenceid\": \"24334\",'+
        '      \"orderstatus\": \"Inprogress\",'+
        '      \"dateacknowledged\": \"2017-10-11T10:29:00\",'+
        '      \"dateaccepted\": \"2017-10-11T10:29:00\",'+
        '      \"datecompleted\": \"2017-10-11T10:29:00\",'+
        '      \"orderserviceabilityclass\": \"12\",'+
        '      \"sla\": \"\",'+
        '      \"appdetails\": [{'+
        '       \"contactdetails": ['+
        '       {\"phoneNumber\": \"+61 459 088 686\",\"type\": \"End User\",\"contactName\": \"John Smith\",\"notes\": \"datata\"}],'+
        '        \"appointmentId\": \"APT100000045564\",'+
        '        \"nbnslottype\": \"08-12\",'+
        '        \"nbndemandtype\": \"STANDARD INSTALL\",'+
        '        \"status\": \"Cancel\",'+
        '       \"nbnappstart\": \"2018-05-08T11:26:10\",'+
        '        \"nbnappend\": \"2018-05-08T11:26:10\",'+
        '        \"nbnappstartlocal\": \"2018-05-08T00:00:00\",'+
        '        \"nbnappstartlocaltime\": \"2018-05-08T08:00:00\",'+
        '        \"nbnappendlocal\": \"2018-05-08T00:00:00\",'+
        '        \"nbnappendlocaltime\": \"2018-05-08T12:00:00\",'+
        '       \"wodetails\": [{'+
        '          \"woid\": \"WOR100000201067\",'+
        '          \"nbnworkforce\": \"DP1019\",'+
        '          \"woparent\": \"WOR100000201051\",'+
        '          \"wostatus\": \"CANCEL\",'+
        '          \"woreasoncode\": \"ORDER-CAN\",'+
        '          \"woprd\": \"2018-05-08T11:16:44+1000\",'+
        '          \"wopriority\": \"STANDARD\"'+
        '        }]'+
        '      }]'+
        '    },'+
        '    {'+
        '      \"nbnorderid\": \"ORD010004399549\",'+
        '      \"asreferenceid\": \"24334\",'+
        '      \"orderstatus\": \"Inprogress\",'+
        '      \"dateacknowledged\": \"2017-10-11T10:29:00\",'+
        '      \"dateaccepted\": \"2017-10-11T10:29:00\",'+
        '      \"datecompleted\": \"2017-10-11T10:29:00\",'+
        '      \"orderserviceabilityclass\": \"12\",'+
        '      \"sla\": \"\",'+
        '      \"appdetails\": [{'+
        '        \"appointmentId\": \"APT100000045565\",'+
        '        \"nbnslottype\": \"08-12\",'+
        '        \"nbndemandtype\": \"STANDARD INSTALL\",'+
        '        \"status\": \"Cancel\",'+
        '       \"nbnappstart\": \"2018-05-08T11:26:10\",'+
        '        \"nbnappend\": \"2018-05-08T11:26:10\",'+
        '        \"nbnappstartlocal\": \"2018-05-08T00:00:00\",'+
        '        \"nbnappstartlocaltime\": \"2018-05-08T08:00:00\",'+
        '        \"nbnappendlocal\": \"2018-05-08T00:00:00\",'+
        '        \"nbnappendlocaltime\": \"2018-05-08T12:00:00\",'+
        '       \"wodetails\": [{'+
        '          \"woid\": \"WOR100000201068\",'+
        '          \"nbnworkforce\": \"DP1019\",'+
        '          \"woparent\": \"WOR100000201055\",'+
        '          \"wostatus\": \"CANCEL\",'+
        '          \"woreasoncode\": \"ORDER-CAN\",'+
        '          \"woprd\": \"2018-05-08T11:16:44+1000\",'+
        '          \"wopriority\": \"STANDARD\"'+
        '        }]'+
        '      }]'+
        '    }'+
        '  ]'+
        ' }'+
        '}';
        Id recordTypeForAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business End Customer').getRecordTypeId();
        Account acc = new Account();
        acc.RecordTypeId = recordTypeForAccount;
        acc.Name = 'Test Account';
        acc.ABN__c = '51824753556';
        //acc.Account_Types__c = 'Access Seeker';
        acc.State__c = 'VIC';
        insert acc;
        Id recordTypeForOpportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Constants.SDM_OPPORTUNITY_RECORDTYPE).getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.SDM_NBN_Location_ID__c = 'LOC000000206007';
        opp.SDM_RSP_Name__c = 'Telstra';
        opp.SDM_nbn_Order_ID__c = 'ORD010004399548';
        opp.CloseDate = Date.today();
        opp.RecordTypeId = recordTypeForOpportunity;
        opp.AccountId = acc.Id;
        opp.Name = 'PO-1234';
        opp.StageName = 'Pre nbn Order Management';
        insert opp;
        
        Test.startTest();
        SDM_UpdateOrderDetailsForLOC.processJSON(json);
        Test.stopTest();
        
        List<Customer_Connections_Order__c> ordersList = [SELECT Id, (SELECT Id FROM Appointments__r) FROM Customer_Connections_Order__c 
                                                          WHERE Order_Id__c = 'ORD010004399548'
                                                          AND Pre_Order__c = :opp.Id];
        system.assertEquals(1, ordersList.size());
        system.assertEquals(1, ordersList[0].Appointments__r.size());
        
        List<WorkOrder> woList = [SELECT Id FROM WorkOrder
                                  WHERE Work_Order_Id__c = 'WOR100000201067'
                                  AND Appointment__r.Order__c = :ordersList[0].Id];
        system.assertEquals(1, woList.size());
        
    }
    
    
    
    static testMethod void testParsingLogicForUpdate(){
        String json = '{'+
        '   \"data\":{  '+
        '  \"locationid\": \"LOC000000206007\",'+
        ' \"serviceabilitydate\":\"21 Nov 2030 13:12:12\",'+
        '  \"accessseekerdetails\": {'+
        '    \"accessseekerid\": \"ASI000000220282\",'+
        '    \"accessseekername\": \"Telstra\"'+
        '  },'+
        '  \"orderdetails\": ['+
        '    {'+
        '      \"accessseekerid\": \"ASI000000220282\",' +
        '      \"avc\": [{\"avctype\" : \"AVC-V\",\"id\" : \"AVC000079068817\",\"bandwidth\": \"D0.15_U0.15_Mbps_TC1_C\" },{\"avctype\" : \"AVC-D\",\"id\" : \"AVC000079068818\",\"bandwidth\": \"“D12_U1_Mbps_TC4_P, D12_U1_Mbps_TC2_P, D0.15_U0.15_Mbps_TC1_C\" },{\"avctype\" : \"AVC-M\",\"id\" : \"AVC0000790688198\",\"bandwidth\": \"D0.15_U0.15_Mbps_TC1_C\" }],'+
        '      \"accessseekername\": \"Telstra\",'+
        '      \"nbnorderid\": \"ORD010004399548\",'+
        '      \"asreferenceid\": \"24334\",'+
        '      \"orderstatus\": \"Inprogress\",'+
        '      \"dateacknowledged\": \"2017-10-11T10:29:00\",'+
        '      \"dateaccepted\": \"2017-10-11T10:29:00\",'+
        '      \"datecompleted\": \"2017-10-11T10:29:00\",'+
        '      \"orderserviceabilityclass\": \"12\",'+
        '      \"sla\": \"\",'+
        '      \"appdetails\": [{'+
        '        \"appointmentId\": \"APT100000045564\",'+
        '        \"nbnslottype\": \"08-12\",'+
        '        \"nbndemandtype\": \"STANDARD INSTALL\",'+
        '        \"status\": \"Cancel\",'+
        '       \"nbnappstart\": \"2018-05-08T11:26:10\",'+
        '        \"nbnappend\": \"2018-05-08T11:26:10\",'+
        '        \"nbnappstartlocal\": \"2018-05-08T00:00:00\",'+
        '        \"nbnappstartlocaltime\": \"2018-05-08T08:00:00\",'+
        '        \"nbnappendlocal\": \"2018-05-08T00:00:00\",'+
        '        \"nbnappendlocaltime\": \"2018-05-08T12:00:00\",'+
        '       \"wodetails\": [{'+
        '          \"woid\": \"WOR100000201067\",'+
        '          \"nbnworkforce\": \"DP1019\",'+
        '          \"woparent\": \"WOR100000201051\",'+
        '          \"wostatus\": \"CANCEL\",'+
        '          \"woreasoncode\": \"ORDER-CAN\",'+
        '          \"woprd\": \"2018-05-08T11:16:44+1000\",'+
        '          \"wopriority\": \"STANDARD\"'+
        '        }]'+
        '      }]'+
        '    },'+
        '    {'+
        '      \"nbnorderid\": \"ORD010004399549\",'+
        '      \"accessseekername\": \"Telstra\",'+            
        '      \"asreferenceid\": \"24334\",'+
        '      \"orderstatus\": \"Inprogress\",'+
        '      \"dateacknowledged\": \"2017-10-11T10:29:00\",'+
        '      \"dateaccepted\": \"2017-10-11T10:29:00\",'+
        '      \"datecompleted\": \"2017-10-11T10:29:00\",'+
        '      \"orderserviceabilityclass\": \"12\",'+
        '      \"sla\": \"\",'+
        '      \"appdetails\": [{'+
        '        \"appointmentId\": \"APT100000045565\",'+
        '        \"nbnslottype\": \"08-12\",'+
        '        \"nbndemandtype\": \"STANDARD INSTALL\",'+
        '        \"status\": \"Cancel\",'+
        '       \"nbnappstart\": \"2018-05-08T11:26:10\",'+
        '        \"nbnappend\": \"2018-05-08T11:26:10\",'+
        '        \"nbnappstartlocal\": \"2018-05-08T00:00:00\",'+
        '        \"nbnappstartlocaltime\": \"2018-05-08T08:00:00\",'+
        '        \"nbnappendlocal\": \"2018-05-08T00:00:00\",'+
        '        \"nbnappendlocaltime\": \"2018-05-08T12:00:00\",'+
        '       \"wodetails\": [{'+
        '          \"woid\": \"WOR100000201068\",'+
        '          \"nbnworkforce\": \"DP1019\",'+
        '          \"woparent\": \"WOR100000201055\",'+
        '          \"wostatus\": \"CANCEL\",'+
        '          \"woreasoncode\": \"ORDER-CAN\",'+
        '          \"woprd\": \"2018-05-08T11:16:44+1000\",'+
        '          \"wopriority\": \"STANDARD\"'+
        '        }]'+
        '      }]'+
        '    }'+
        '  ]'+
        ' }'+
        '}';
        Id recordTypeForAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business End Customer').getRecordTypeId();
        Account acc = new Account();
        acc.RecordTypeId = recordTypeForAccount;
        acc.Name = 'Test Account';
        acc.ABN__c = '51824753556';
        //acc.Account_Types__c = 'Access Seeker';
        acc.State__c = 'VIC';
        insert acc;
        Id recordTypeForOpportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Constants.SDM_OPPORTUNITY_RECORDTYPE).getRecordTypeId();
        Opportunity opp = new Opportunity();
        opp.SDM_NBN_Location_ID__c = 'LOC000000206007';
        opp.SDM_nbn_Order_ID__c  = 'ORD010004399548';
        opp.SDM_RSP_Name__c = 'Telstra';
        opp.CloseDate = Date.today();
        opp.RecordTypeId = recordTypeForOpportunity;
        opp.AccountId = acc.Id;
        opp.Name = 'PO-1234';
        opp.StageName = 'Pre nbn Order Management';
        insert opp;
        
        Customer_Connections_Order__c ord = new Customer_Connections_Order__c();
        ord.Order_ID__c = 'ORD010004399548';
        ord.Location_ID__c = 'LOC000000206007';
        ord.Pre_Order__c = opp.Id;
        insert ord;
        
        Appointment__c apt = new Appointment__c();
        apt.Name = 'APT100000045564';
        apt.Order__c = ord.Id;
        insert apt;
        
        Id recordTypeForWorkOrder = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get(Constants.SDM_WORK_ORDER_RECORDTYPE).getRecordTypeId();
        WorkOrder wo = new WorkOrder();
        wo.RecordTypeId = recordTypeForWorkOrder;
        wo.Work_Order_Id__c = 'WOR100000201067';
        wo.Appointment__c = apt.Id;
        insert wo;
        
        Test.startTest();
        SDM_UpdateOrderDetailsForLOC.processJSON(json);
        Test.stopTest();
        
        List<WorkOrder> woList = [SELECT Id, Work_Order_Reason__c, Appointment__r.Demand_Type__c, Appointment__r.Order__r.Serviceability_Class__c FROM WorkOrder
                                  WHERE Id = :wo.Id
                                  AND Appointment__c = :apt.Id
                                  AND Appointment__r.Order__c = :ord.Id];
        system.assertEquals(12, woList[0].Appointment__r.Order__r.Serviceability_Class__c);
        system.assertEquals('STANDARD INSTALL', woList[0].Appointment__r.Demand_Type__c);
        system.assertEquals('ORDER-CAN', woList[0].Work_Order_Reason__c);
        
    }
    
}