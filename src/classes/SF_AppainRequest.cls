public with sharing class SF_AppainRequest {
	public class DesktopAssessmentRequest{
		public String salesforceOppName {get;set;}
		public String salesforceParentOppId {get;set;}
		public String salesforceParentOppName {get;set;}
		public String salesforceQuoteId {get;set;}
		public String salesforceQuoteName {get;set;}
		public String basketId {get;set;}
		public String salesforceStatus {get;set;}
		public String fibreJointId {get;set;}
		public String fibreJointStatus  {get;set;}
		public String fibreJointLatLong {get;set;}
		public String fibreJointTypeCode {get;set;}
		public String locId {get;set;}
		public String locLatLong {get;set;}
		public String address {get;set;}
		public String ragValue {get;set;}
		public String actualDistance {get;set;}
        public String cost{get;set;}
        public String estimatedBuildTimeframe{get;set;} // Only for Whitelist for now
        public String bsmApprovalRequired{get;set;}
        public String accessSeekerId{get;set;}
	}
}