/*
* Created By Rejeesh Raghavan on 01/06/2018
* */
public with sharing class SF_AppainToSFEventHandler implements Queueable{
    
    private List<BusinessEvent__e> lstToProcess;
    
    public SF_AppainToSFEventHandler(List<BusinessEvent__e> evtLst) {       
        lstToProcess = evtLst;
    }
    
    public void execute(QueueableContext context) {
        
        Business_Platform_Events__c customSettingValues = Business_Platform_Events__c.getOrgDefaults();       
        
        for(BusinessEvent__e evt : lstToProcess){
            if((evt.Event_Id__c==customSettingValues.SFC_Desktop_Assessment_completed__c)){
                system.debug('Appain Update event received for the Event ID '+evt.Event_Id__c);
                updateProductConfigFBCValue(evt.Message_Body__c);
            }               
        }
    }
    
    //@future(callout=true)
    public static void updateProductConfigFBCValue(String evtBody){
        system.debug('@@updateProductConfigFBCValue==='+evtBody);   
        
        try{          
            System.debug('Appain Message Body : ' + evtBody);
            Map<String,String> evtBodyMap = new Map<String,String>(); 
            List<String> bodyList = evtBody.split(',');
            
            for(String s : bodyList){
                List<String> strVal = s.split(':');
                if(strVal.size()>1){
                    evtBodyMap.put(strVal[0],strVal[1]);
                }
            }
            
            if(evtBodyMap.containskey('opportunityId') && evtBodyMap.containskey('cost') && evtBodyMap.containskey('salesforceQuoteId')){
                String oppId ;//= evtBodyMap.get('opportunityId');
                String cost =  evtBodyMap.get('cost');
                String quoteId = evtBodyMap.get('salesforceQuoteId');
                String strAppianRequired = evtBodyMap.get('bsmApprovalRequired');
                String strAppianDecision = evtBodyMap.get('bsmDecision');
                if(String.isNotBlank(quoteId)){
                    DF_Quote__c quoteRec = [SELECT Id,
                                            Name, 
                                            RecordType.Name, 
                                            Location_Id__c, 
                                            Address__c, 
                                            Opportunity__c, 
                                            RAG__c, 
                                            LAPI_Response__c, 
                                            Opportunity_Bundle__c, 
                                            Fibre_Build_Cost__c, 
                                            GUID__c,
                                            Appian_Approval_Required__c,
                                            Appian_Decision__c,
                                            Approved__c
                                            FROM DF_Quote__c 
                                            WHERE Id = :quoteId
                                            LIMIT 1];
                    oppId = quoteRec.Opportunity__c;
                    updateAppianDecision(quoteRec, strAppianRequired, strAppianDecision);
                    if(quoteRec.RAG__c.equalsIgnoreCase(SF_Constants.RAG_GREEN) ||
                       ((!quoteRec.RAG__c.equalsIgnoreCase(SF_Constants.RAG_GREEN)) && quoteRec.Approved__c)){
                           updateFBC(oppId,quoteRec,cost);
                       }
                    
                    System.debug('Updating product basket with the Desktop Assesment Cost : ' + cost);
                    System.debug('PP Start of DA - oppId: '+oppId);
                    System.debug('PP Start of DA - quoteId: '+quoteId);
                }
            }
        }catch(System.CalloutException ex) {
            GlobalUtility.logMessage('Error', 'AppianResponseHandlingError', 'updateProductConfigFBCValue', ' ', '', '', '', ex, 0);
        }         
    }
    
    public static void updateFBC(String oppId, DF_Quote__c quoteObj,String cost){
        
        String strProductDefinitionType='SF_BUILD_CONTRIBUTION_DEFINITION_ID';
        String bcDefId = SF_CS_API_Util.getCustomSettingValue(strProductDefinitionType);
        List<cscfga__Product_Basket__c> baskets = [Select Id, Name, cscfga__User_Session__c from cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :oppId];
        String basketId = !baskets.isEmpty() ? baskets.get(0) != null ? baskets.get(0).Id : null : null;
        List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c>([Select id from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and cscfga__Product_Definition__c = :bcDefId]);
        
        cscfga.API_1.ApiSession apiSession = SF_CS_API_Util.createApiSession(baskets.get(0));
        apiSession.setConfigurationToEdit(new cscfga__Product_Configuration__c(Id = configs.get(0).id, cscfga__Product_Basket__c = basketId));        
        cscfga.ProductConfiguration currConfig = apiSession.getConfiguration();
        
        for(cscfga.Attribute att : currConfig.getAttributes()) {
            System.debug('related Attribute name is : ' + att.getName());
            if(att.getName() == 'FibreBuildChargeOverride'){
                att.setValue(cost);
                att.setDisplayValue(cost);
            }
        }
        
        apiSession.executeRules(); // run the rules after setting the attribute values
        apiSession.validateConfiguration();
        apiSession.persistConfiguration(true);
        
        //revalidate configurations to get the final totals
        Set<Id> bsktIds = new Set<Id>{basketId};
            cscfga.ProductConfigurationBulkActions.calculateTotals(bsktIds);
        
        /*currConfig = apiSession.getConfiguration();

        for(cscfga.Attribute att : currConfig.getAttributes()) {
        System.debug('related ff Attribute name is : ' + att.getName());
        System.debug('related value Attribute name is : ' + att.getValue()); 
        if(att.getName() == 'Fibre Build Contribution'){
        quoteObj.Fibre_Build_Cost__c = Decimal.valueOf(att.getValue());
        }               
        }*/
        cost = String.isBlank(cost)?'0.0':cost;
        quoteObj.Fibre_Build_Cost__c = Decimal.valueOf(cost);
        
        update quoteObj;
    }
    public static DF_Quote__c updateAppianDecision(DF_Quote__c qRec, String strAppianRequired, String strAppianDecision){
        qRec.Appian_Approval_Required__c = strAppianRequired;
        qRec.Appian_Decision__c = strAppianDecision;
        return qRec;
    }
    
}