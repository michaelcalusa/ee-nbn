@isTest
private class SF_AppainToSFEventHandler_Test {
    @testSetup
    static void testSetup(){
        User nbnUser1 = SF_TestData.createJitUser('AS00000099', 'Access Seeker nbnSelect Feasibility', 'nbnRsp1');
        Account acc1 = [SELECT Id FROM Account WHERE Access_Seeker_ID__c = 'AS00000099' LIMIT 1];
        DF_Opportunity_Bundle__c oppBundle = SF_TestData.createOpportunityBundle(acc1.Id);
        Opportunity opp= SF_TestData.createOpportunity('TestOpportunity');
        opp.accountid=acc1.id;
        cscfga__Product_Basket__c basketObj = SF_TestService.getNewBasketWithConfigSiteSurvey(acc1);
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        String latitude = '-33.840213';
        String longitude = '151.207368';
            Test.startTest();

        System.runAs(rspUser1){
            Database.insert(oppBundle);
            Database.insert(opp);

            DF_Quote__c dfQuote1 = SF_TestData.createDFQuote(address, latitude, longitude, 'LOC111111111111', opp.Id, oppBundle.Id, null, 'Red');
            dfQuote1.Approved__c = true;
            Database.insert(dfQuote1);
            DF_Order__c dfOrder1 = SF_TestData.createDFOrder(dfQuote1.Id, oppBundle.Id, 'In Draft');
            //dfOrder1.Order_JSON__c = '{"resourceOrder":{"id":"ROR000000001425","notes":"test","siteName":"test","siteType":"Education","orderType":"Connect","tradingName":"test","dateReceived":"2018-09-25T22:16:59Z","heritageSite":"Yes","keysRequired":"test","safetyHazards":"0","additionalNotes":"test","jsasswmRequired":null,"inductionRequired":"Yes","interactionStatus":"Acknowledged","resourceOrderType":"NBN Select","accessSeekerContact":{"contactName":null,"contactPhone":null},"afterHoursSiteVisit":"Yes","itemInvolvesContact":[{"role":"test","type":"Business","contactName":"test test","phoneNumber":"0423237872","emailAddress":"test@dsc.com","unstructuredAddress":{"postcode":"3003","addressLine1":"test","addressLine2":"","addressLine3":"","localityName":"test","stateTerritoryCode":"SA"}},{"role":"","type":"Site","contactName":"test test","phoneNumber":"0423232424","emailAddress":"test@sdcs.com","unstructuredAddress":{"postcode":"","addressLine1":"","addressLine2":"","addressLine3":"","localityName":"","stateTerritoryCode":""}}],"customerRequiredDate":"2018-11-15","ownerAwareOfProposal":"Yes","securityRequirements":"test","associatedReferenceId":"NSQ-0000000574","plannedCompletionDate":null,"workPracticesRequired":"Yes","siteAccessInstructions":"test","interactionDateComplete":null,"propertyOwnershipStatus":"Leased","resourceOrderComprisedOf":{"itemInvolvesLocation":{"id":"LOC000049831879"},"referencesResourceOrderItem":[{"action":"ADD","itemInvolvesResource":{"type":"NTD","batteryBackupRequired":"Yes"}}]},"confinedSpaceRequirements":"Yes","workingAtHeightsConsideration":"Yes","contractedLocationInstructions":"test"},"notificationType":"OrderAccepted"}';
            dfOrder1.Order_JSON__c = '{"resourceOrder":{"workPracticesRequired":"No","workingAtHeightsConsideration":"No","tradingName":"Contigo Ltd","siteType":"Industrial","siteName":"Contigo AU","siteAccessInstructions":"Site Access","securityRequirements":"Get keys","safetyHazards":"No","resourceOrderType":"nbnSelect","resourceOrderComprisedOf":{"referencesResourceOrderItem":[{"itemInvolvesResource":{"type":"NTD","batteryBackupRequired":"Yes"},"action":"ADD"}],"itemInvolvesLocation":{"id":"LOC000066186204"}},"propertyOwnershipStatus":"Owned","ownerAwareOfProposal":"Yes","orderType":"Connect","notes":"Additional","keysRequired":"At main gate","jsaswmRequired":"Yes","itemInvolvesContact":[{"unstructuredAddress":{"stateTerritoryCode":"VIC","postcode":"3008","localityName":"Docklands","addressLine3":"","addressLine2":"","addressLine1":"380 Docklands Dr"},"type":"Business","role":"COO","phoneNumber":"0404040511","emailAddress":"business@contigo.com","contactName":"Business Cowman","company":null},{"unstructuredAddress":{"stateTerritoryCode":"","postcode":"","localityName":"","addressLine3":"","addressLine2":"","addressLine1":""},"type":"Site","role":"","phoneNumber":"0404040511","emailAddress":"site@quantuso.com","contactName":"Site Details","company":null}],"interactionStatus":null,"inductionRequired":"Yes","id":"ROR000000001425","heritageSite":"Yes","dateReceived":"2018-10-03T04:41:55.182Z","customerRequiredDate":"2018-12-25","contractedLocationInstructions":"Contract","confinedSpaceRequirements":"No","associatedReferenceId":"NSQ-0000000610","afterHoursSiteVisit":"No","additionalNotes":"Installation","accessSeekerInteraction":{"billingAccountID":"BAN1234567"},"accessSeekerContact":{"contactPhone":null,"contactName":null}},"notificationType":null}';
            Database.insert(dfOrder1);
            
            csord__Order_Request__c csOrderReq = SF_TestData.buildOrderRequest();
            Database.insert(csOrderReq);
            
            csord__Order__c csOrder = SF_TestData.buildOrder('TestCsOrder', acc1.id, opp.Id, 'ConstructionComplete', csOrderReq.Id);
            Database.insert(csOrder);
            
            csord__Order_Line_Item__c csOrdLineItem = SF_TestData.buildOrderRequestLineItem('TestOrderLineItem', csOrder.Id);
            Database.insert(csOrdLineItem);
            
            basketObj.cscfga__Opportunity__c = dfQuote1.Opportunity__c;
            Database.update(basketObj);
            
            Business_Platform_Events__c busPltfrmSettings = SF_TestData.createCustomSettingsforBusinessPlatformEvents();
            Database.insert(busPltfrmSettings);
        }
        Test.stopTest();
    }

    static testMethod void testUpdateProductConfigFBCValue(){
        User rspUser1 = [SELECT Id FROM User WHERE FirstName = 'nbnRsp1' LIMIT 1];
        System.runAs(rspUser1){
            DF_Quote__c dfQ = [SELECT Id, Opportunity__c, GUID__c  FROM DF_Quote__c WHERE Location_Id__c = 'LOC111111111111' LIMIT 1];
            BusinessEvent__e busEvent = new BusinessEvent__e();
            busEvent.Event_Type__c = 'NS SFC Desktop Assessment Completed';
            busEvent.Event_Id__c = 'AP-SF-01000';
            busEvent.Event_Record_Id__c='test0001';
            busEvent.Source__c = 'Appian';
            busEvent.Message_Body__c = 'opportunityId:'+ dfQ.GUID__c+',cost:1233,salesforceQuoteId:' + dfQ.Id;
            
            test.startTest(); 
            	Id jobId = System.enqueueJob(new SF_AppainToSFEventHandler(new List<BusinessEvent__e> {busEvent}));
            test.stopTest();
            DF_Quote__c updatedDfQ = [SELECT Id, Fibre_Build_Cost__c  FROM DF_Quote__c WHERE id = :dfQ.Id LIMIT 1];
            System.assertEquals(Decimal.valueOf('1233'), updatedDfQ.Fibre_Build_Cost__c);
        }
    }
    /*static void testExecuteMethod()
    {
        Id oppId = SF_TestService.getServiceFeasibilityRequest();
        oppId = SF_TestService.getQuoteRecords();
        Account acc = SF_TestData.createAccount('Test Account');
        insert acc;
        List<DF_Quote__c> dfQuoteList = [SELECT Id,Address__c,LAPI_Response__c,RAG__c,Fibre_Build_Cost__c,Opportunity_Bundle__c,Opportunity__c
                                         FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppid LIMIT 1];
        cscfga__Product_Basket__c basketObj = SF_TestService.getNewBasketWithConfigSiteSurvey(acc);
        Opportunity opp = SF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basketObj.cscfga__Opportunity__c = dfQuoteList[0].Opportunity__c;//opp.Id;
        update basketObj;
        Business_Platform_Events__c settings = SF_TestData.createCustomSettingsforBusinessPlatformEvents();
        insert settings;
        
        
        List<BusinessEvent__e> toProcess = new List<BusinessEvent__e>();
        for(Integer i=0;i<1;i++){
            String msg = 'opportunityId:' + opp.Id + ',cost:' + 5555 + ',salesforceQuoteId:' + dfQuoteList.get(0).id +'';
            BusinessEvent__e b = new BusinessEvent__e();
            b.Event_Id__c = 'AP-SF-01000';
            b.Event_Record_Id__c = String.valueOf(i);
            b.Event_Type__c = 'Desktop assesment';
            b.Source__c = 'Appian';
            b.Message_Body__c = 
            toProcess.add(b);
        }           
        test.startTest(); 
        Id jobId = System.enqueueJob(new SF_AppainToSFEventHandler(toProcess));
        test.stopTest(); 
    }*/
    
    /*@isTest
    static void testBuildJSONMethod()
    {
        Id oppId = SF_TestService.getServiceFeasibilityRequest();
        oppId = SF_TestService.getQuoteRecords();
        Account acc = SF_TestData.createAccount('Test Account');
        acc.Access_Seeker_ID__c = 'AS12345';
        acc.Billing_ID__c = 'BL12345';
        insert acc;
        cscfga__Product_Basket__c basketObj = SF_TestService.getNewBasketWithConfigSiteSurvey(acc);
        Opportunity opp = SF_TestData.createOpportunity('Test Opp');
        opp.Opportunity_Bundle__c = oppId;
        insert opp;
        basketObj.cscfga__Opportunity__c = opp.Id;
        update basketObj;
        Business_Platform_Events__c settings = SF_TestData.createCustomSettingsforBusinessPlatformEvents();
        insert settings;
        
        List<DF_Quote__c> dfQuoteList = [SELECT Id,Address__c,LAPI_Response__c,RAG__c,Location_Id__c,Fibre_Build_Cost__c,Opportunity_Bundle__c,Opportunity__c
                                         FROM DF_Quote__c WHERE Opportunity_Bundle__c = :oppid LIMIT 1];
        
        String priceItemCode = '';
        DF_Order__c ordObj = new DF_Order__c();
        ordObj.DF_Quote__c = dfQuoteList.get(0).Id;
		ordObj.Opportunity_Bundle__c = oppId;
        insert ordObj;
        Opportunity childOpp = SF_TestData.createOpportunity('Test Child Opp');
        childOpp.Parent_Opportunity__c = opp.Id;
        insert childOpp;
        csord__Order__c csOrder = new csord__Order__c();
        csOrder.csordtelcoa__Opportunity__c = childOpp.Id;
        csOrder.csord__Identification__c = 'Order_a1hN0000001UNpfIAG_0';
        insert csOrder;
        csord__Order_Line_Item__c csOLI = new csord__Order_Line_Item__c();
        csOLI.csord__Order__c = csOrder.Id;
        csOLI.csord__Identification__c = 'a1FN0000001MpC5MAK_0';
        insert csOLI;
        
        Test.startTest(); 
        	String jsonStr = SF_AppainToSFEventHandler.buildJSON(csOrder,priceItemCode,opp.Id,ordObj,dfQuoteList.get(0),acc);
        Test.stopTest(); 
    }*/
}