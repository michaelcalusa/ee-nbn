/**
* Created by Gobind.Khurana on 21/05/2018.
*/

public class SF_BulkUploadApexController {
    //Class that creates an opportunity bundle and attaches the csv file to that record. It also processes the contents of the csv file uploaded 
    //name for the csv file to be attached
    public static final String uploadFileName = 'Bulk Upload.csv';
    
    @AuraEnabled
    public static Id saveChunk(String fileName, String base64Data, String contentType, String fileId,string productType) {
        // Create an opportunity bundle record whose id is returned after processing
        system.debug('----ProductType-----'+productType);        
        Id parentId = NS_SF_CaptureController.createOpptyBundle(productType);        
        system.debug('----parentId-----'+parentId);
        Id attachmentId;
        //integer rowCount;
        System.debug('PP base64Data:'+base64Data);
        
        
        // check if fileId id ''(Always blank in first chunk), then call the saveTheFile method for larger files call appendtofile in future
        if(String.isNotEmpty(parentId)){
            if (fileId == '') {
                fileId = saveTheFile(parentId, fileName, base64Data, contentType);
            }
            attachmentId = Id.valueOf(fileId);
            if(String.isNotEmpty(attachmentId)){
                SF_BulkUploadUtils.processBulkUploadFile(parentId,productType);
                
            }
        }
        return parentId;
    }
    
    
    public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        
        Attachment oAttachment = new Attachment();
        oAttachment.parentId = parentId;
        
        oAttachment.Body = EncodingUtil.base64Decode(base64Data);
        system.debug('--@saveTheFile-- '+oAttachment.Body.toString());
        
        oAttachment.Name = uploadFileName;
        oAttachment.ContentType = contentType;
        
        insert oAttachment;
        
        return oAttachment.Id;
    }
    
    @AuraEnabled
    public static String downloadAttachment(){
        
        String BULK_UPLOAD_TEMPLATE = SF_CS_API_Util.getCustomSettingValue('BULK_UPLOAD_TEMPLATE');
        return BULK_UPLOAD_TEMPLATE;
    }
    
    @AuraEnabled
    public static boolean isValidSize(String base64Data, String contentType){
        String recordLimt = SF_CS_API_Util.getCustomSettingValue('NS_BULKUPLOAD_RECORD_LIMIT');//NS_Custom_Options__c.getValues('NS_BULKUPLOAD_RECORD_LIMIT');
        
        system.debug('---recordLimt'+recordLimt);
        
        boolean isValidSize = false;
        
        if(String.isNotBlank(recordLimt)){
            
            String ENCODING_SCHEME = 'ISO-8859-1';
            
            string decodedStr = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            //System.debug('@isValidSize-- decodedStr-- '+decodedStr);
            
            Blob blobVal = EncodingUtil.base64Decode(decodedStr);
            //string finalCsvStr = EncodingUtil.base64Encode(blobVal);//blobVal.toString();
            system.debug('-----blobVal@@before'+blobVal.size());
            
            //string finalCsvStr= blobtoString(blobVal);
            
            string finalCsvStr = blobVal.toString();
            finalCsvStr = finalCsvStr.replaceAll('(\r\n|\r)', '\n');
            // Seperate every row of the excel file
            List<string> csvFileRowsArray = finalCsvStr.split('\n');
            
            system.debug('---csvFileRowsArray.size()-- '+csvFileRowsArray.size());
            
            // Start from second line (skip header & blank lines)
            Integer actualRowCount = 0;
            for (Integer i = 1; i < csvFileRowsArray.size(); i++) {
                if (String.isNotBlank(csvFileRowsArray[i])) {
                    actualRowCount++;
                }
            }
            system.debug('actualRowCount:' + actualRowCount);
            
            integer finalLimit = string.isNotBlank(recordLimt) ? integer.valueOf(recordLimt) : 0;
            
            isValidSize = actualRowCount<=finalLimit;
            
            
        }else{
            isValidSize = true;
        }
        return isValidSize;
    }
}