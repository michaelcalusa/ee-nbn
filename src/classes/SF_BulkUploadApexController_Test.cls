@IsTest
public class SF_BulkUploadApexController_Test {

     /**
    * Tests saveChunk method
    */
    
    @isTest static void isValidSize()
    {
        String base64Data = 'U2VhcmNoVHlwZSxMb2NhdGlvbklkLExhdGl0dWRlLExvbmdpdHVkZSxVbml0TnVtYmVyLFVuaXRUeXBlLFN0cmVldG9yTG90TnVtYmVyLFN0cmVldE5hbWUsU3RyZWV0VHlwZSxTdWJ1cmJvckxvY2FsaXR5LFN0YXRlLFBvc3Rjb2RlDQpTZWFyY2hCeUxvY2F0aW9uSUQsTE9DMDAwMDAyNjk2NzI0LCwsLCwsLCwsLA0KU2VhcmNoQnlMYXRMb25nLCwtNDIuNzg5MzEsMTQ3LjA1NTc4LCwsLCwsLCwNClNlYXJjaEJ5QWRkcmVzcywsLCwxLFVOSVQsMTEsUEFDSUZJQyxXQVksQkVMRE9OLFdBLDYwMjc';
        String contentType = 'application/vnd.ms-excel';
                
        
         NS_Custom_Options__c rowLimit = new NS_Custom_Options__c(name='NS_BULKUPLOAD_RECORD_LIMIT');
         rowLimit.Value__c = '600';
        Insert rowLimit;
        
        SF_BulkUploadApexController.isValidSize(base64Data, contentType);
        
    }
    @isTest static void saveChunkTest()
    {
        String base64Data = 'U2VhcmNoVHlwZSxMb2NhdGlvbklkLExhdGl0dWRlLExvbmdpdHVkZSxVbml0TnVtYmVyLFVuaXRUeXBlLFN0cmVldG9yTG90TnVtYmVyLFN0cmVldE5hbWUsU3RyZWV0VHlwZSxTdWJ1cmJvckxvY2FsaXR5LFN0YXRlLFBvc3Rjb2RlDQpTZWFyY2hCeUxvY2F0aW9uSUQsTE9DMDAwMDAyNjk2NzI0LCwsLCwsLCwsLA0KU2VhcmNoQnlMYXRMb25nLCwtNDIuNzg5MzEsMTQ3LjA1NTc4LCwsLCwsLCwNClNlYXJjaEJ5QWRkcmVzcywsLCwxLFVOSVQsMTEsUEFDSUZJQyxXQVksQkVMRE9OLFdBLDYwMjc';
        String contentType = 'application/vnd.ms-excel';
        User commUser;
        string productType = 'NBN_SELECT';
        commUser = SF_TestData.createDFCommUser();
        
        
        system.runAs(commUser) {
            Test.startTest();
            Id oppBundleId = SF_BulkUploadApexController.saveChunk('', base64Data, contentType, '', productType);
            System.assertnotEquals('', oppBundleId);
            Test.stopTest();
        }
    }
    
    /**
    * Tests downloadAttachment method
    */
    @isTest static void downloadAttachmentTest()
    {
        List<NS_Custom_Options__c> csList = SF_TestData.createCustomSettings();
        insert csList;
        Test.startTest();
        String baseUrl = SF_BulkUploadApexController.downloadAttachment();
        System.assertNotEquals('', baseUrl);
        Test.stopTest();
    }
}