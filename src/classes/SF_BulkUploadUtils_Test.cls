/**
* Created by Dheeraj.Kanna on 22/05/2018.
*/
@isTest
private class SF_BulkUploadUtils_Test {

    @isTest static void test_processBulkUploadFile_locationidSearch() {      	 	    	
    	// Setup data
		final String ENCODING_SCHEME = 'ISO-8859-1';
		String csvContent = 'LocationId,Latitude,Longitude,UnitType,UnitNumber,StreetorLotNumber,StreetName,StreetType,SuburborLocality,State,Postcode';
		csvContent = csvContent + '\n';
		csvContent = csvContent + 'LOC000002696724,-42.78931,147.05578,UNIT,1,11,PACIFIC,Way,BELDON,Western Australia,6027';

		// Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf(csvContent);
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    
		
    	test.startTest();           

		SF_BulkUploadUtils.processBulkUploadFile(opptyBundle.Id,'NBN_Select');			
                                        
        test.stopTest();                

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id
											FROM   DF_SF_Request__c];
											
		system.AssertEquals(1, sfReqList.size());											
    }

    @isTest static void test_processBulkUploadFile_locationidSearch_overSizeLocationId() {      	 	    	
    	// Setup data
		final String ENCODING_SCHEME = 'ISO-8859-1';
		String csvContent = 'LocationId,Latitude,Longitude,UnitType,UnitNumber,StreetorLotNumber,StreetName,StreetType,SuburborLocality,State,Postcode';
		csvContent = csvContent + '\n';
		csvContent = csvContent + 'LOC00000269672499999,-42.78931,147.05578,UNIT,1,11,PACIFIC,Way,BELDON,Western Australia,6027';

		// Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf(csvContent);
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    
		
    	test.startTest();           

		SF_BulkUploadUtils.processBulkUploadFile(opptyBundle.Id,'NBN_Select');			
                                        
        test.stopTest();                

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id
											FROM   DF_SF_Request__c];
											
		system.AssertEquals(1, sfReqList.size());											
    }
    
    @isTest static void test_processBulkUploadFile_latLongSearch() {      	 	    	
    	// Setup data
		final String ENCODING_SCHEME = 'ISO-8859-1';
		String csvContent = 'LocationId,Latitude,Longitude,UnitType,UnitNumber,StreetorLotNumber,StreetName,StreetType,SuburborLocality,State,Postcode';
		csvContent = csvContent + '\n';
		csvContent = csvContent + ',-42.78931,147.05578,UNIT,1,11,,Way,,Western Australia,6027';

		// Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf(csvContent);
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    
		
    	test.startTest();           

		SF_BulkUploadUtils.processBulkUploadFile(opptyBundle.Id,'NBN_Select');
                                        
        test.stopTest();                

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id
											FROM   DF_SF_Request__c];
											
		system.AssertEquals(1, sfReqList.size());	
    }

    @isTest static void test_processBulkUploadFile_latLongSearch_overSizeLatLong() {      	 	    	
    	// Setup data
		final String ENCODING_SCHEME = 'ISO-8859-1';
		String csvContent = 'LocationId,Latitude,Longitude,UnitType,UnitNumber,StreetorLotNumber,StreetName,StreetType,SuburborLocality,State,Postcode';
		csvContent = csvContent + '\n';
		csvContent = csvContent + ',-42.7893111111111111111111111,147.0557811111111111111111111,UNIT,1,11,,Way,,Western Australia,6027';

		// Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf(csvContent);
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    
		
    	test.startTest();           

		SF_BulkUploadUtils.processBulkUploadFile(opptyBundle.Id,'NBN_Select');
                                        
        test.stopTest();                

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id
											FROM   DF_SF_Request__c];
											
		system.AssertEquals(1, sfReqList.size());	
    }
    
    @isTest static void test_processBulkUploadFile_addressSearch() {      	 	    	
    	// Setup data
		final String ENCODING_SCHEME = 'ISO-8859-1';
		String csvContent = 'LocationId,Latitude,Longitude,UnitType,UnitNumber,StreetorLotNumber,StreetName,StreetType,SuburborLocality,State,Postcode';
		csvContent = csvContent + '\n';
		csvContent = csvContent + ',,,UNIT,1,11,PACIFIC,Way,BELDON,Western Australia,6027';

		// Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf(csvContent);
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    
		
    	test.startTest();           

		SF_BulkUploadUtils.processBulkUploadFile(opptyBundle.Id,'NBN_Select');
                                        
        test.stopTest();                

        // Assertions
		List<DF_SF_Request__c> sfReqList = [SELECT Id
											FROM   DF_SF_Request__c];
											
		system.AssertEquals(1, sfReqList.size());	
    }        
    
    @isTest static void test_blobToString() {      	 	    	
    	// Setup data
		final String ENCODING_SCHEME = 'ISO-8859-1';

		// Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf('LocationId,Latitude,Longitude');
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    	
    	
		Blob fileContentBlob;

    	test.startTest();           

		fileContentBlob = SF_BulkUploadUtils.getAttachmentFile(opptyBundle.Id);
		String fileContentStr = SF_BulkUploadUtils.blobToString(fileContentBlob, ENCODING_SCHEME);
                                        
        test.stopTest();                

        // Assertions
		system.AssertNotEquals(null, fileContentStr);
    }
    
    @isTest static void test_getAttachmentFile() {      	 	    	
    	// Setup data

		// Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;
    	
    	// Create attachment    
        Attachment attachmnt = new Attachment();
        attachmnt.parentId = opptyBundle.Id;
        Blob bodyBlob = Blob.valueOf('LocationId,Latitude,Longitude');
        attachmnt.body = bodyBlob;
        attachmnt.Name = 'Bulk Upload.csv';
        attachmnt.ContentType = 'application/vnd.ms-excel';
        insert attachmnt;    	
    	
		Blob fileContentBlob;

    	test.startTest();           

		fileContentBlob = SF_BulkUploadUtils.getAttachmentFile(opptyBundle.Id);
                                        
        test.stopTest();                

        // Assertions
		system.AssertNotEquals(null, fileContentBlob);
    }    
    
    @isTest
    static void test_validateUnitType() {
        system.AssertEquals(false, SF_BulkUploadUtils.validateUnitType(''));
    }

    @isTest
    static void test_validateStreetType() {
        system.AssertEquals(false, SF_BulkUploadUtils.validateStreetType(''));
        system.AssertEquals(false, SF_BulkUploadUtils.validateStreetType(null));
        system.AssertEquals(false, SF_BulkUploadUtils.validateStreetType('  '));
        system.AssertEquals(false, SF_BulkUploadUtils.validateStreetType('XX'));

        system.AssertEquals(true, SF_BulkUploadUtils.validateStreetType('Rd'));
        system.AssertEquals(true, SF_BulkUploadUtils.validateStreetType('Road'));

    }

    @isTest
    static void test_validateState() {
        system.AssertEquals(true, SF_BulkUploadUtils.validateState(' NSW '));
        system.AssertEquals(true, SF_BulkUploadUtils.validateState(' WA '));

        system.AssertEquals(true, SF_BulkUploadUtils.validateState(' ACT '));

        system.AssertEquals(true, SF_BulkUploadUtils.validateState(' NT '));
        system.AssertEquals(true, SF_BulkUploadUtils.validateState(' VIC '));
        system.AssertEquals(true, SF_BulkUploadUtils.validateState(' QLD '));
        system.AssertEquals(true, SF_BulkUploadUtils.validateState(' TAS '));

        system.AssertEquals(true, SF_BulkUploadUtils.validateState(' NEW  SOUTH  WALES'));
        system.AssertEquals(true, SF_BulkUploadUtils.validateState(' AUSTRALIAN  CAPITAL  TERRITORY '));
        system.AssertEquals(true, SF_BulkUploadUtils.validateState(' WESTERN  AUSTRALIA '));
        system.AssertEquals(true, SF_BulkUploadUtils.validateState(' NORTHERN  TERRITORY'));
        system.AssertEquals(true, SF_BulkUploadUtils.validateState(' VICTORIA '));
        system.AssertEquals(true, SF_BulkUploadUtils.validateState('QUEENSLAND'));
        system.AssertEquals(true, SF_BulkUploadUtils.validateState('TASMANIA'));


        system.AssertEquals(false, SF_BulkUploadUtils.validateState(null));
        system.AssertEquals(false, SF_BulkUploadUtils.validateState(''));
        system.AssertEquals(false, SF_BulkUploadUtils.validateState(' '));
        system.AssertEquals(false, SF_BulkUploadUtils.validateState('TASMNIA'));

    }

    @isTest
    static void test_getUnitType() {
        system.AssertEquals('Suite', SF_BulkUploadUtils.getUnitTypeAPIName(' Suite'));
        system.AssertEquals('Suite', SF_BulkUploadUtils.getUnitTypeAPIName('SUITE '));
        system.AssertEquals('MBTH', SF_BulkUploadUtils.getUnitTypeAPIName('  Marine Berth '));
        system.AssertEquals('MBTH', SF_BulkUploadUtils.getUnitTypeAPIName(' MBTH '));
        system.AssertEquals('WHSE', SF_BulkUploadUtils.getUnitTypeAPIName(' WAREHOUSE '));

        system.AssertEquals('MSNT',  SF_BulkUploadUtils.getUnitTypeAPIName(' MAISONETTE '));
        system.AssertEquals('MSNT', SF_BulkUploadUtils.getUnitTypeAPIName(' MSNT'));
        system.AssertEquals('SHED', SF_BulkUploadUtils.getUnitTypeAPIName('SHED'));
        system.AssertEquals('SHOP', SF_BulkUploadUtils.getUnitTypeAPIName('SHOP    '));
        system.AssertEquals('SITE', SF_BulkUploadUtils.getUnitTypeAPIName('SITE'));

        system.AssertEquals(null, SF_BulkUploadUtils.getUnitTypeAPIName(' Lot '));
        system.AssertEquals(null, SF_BulkUploadUtils.getUnitTypeAPIName(' WAHOUSE '));
        system.AssertEquals(null, SF_BulkUploadUtils.getUnitTypeAPIName(''));
        system.AssertEquals(null, SF_BulkUploadUtils.getUnitTypeAPIName(null));
        system.AssertEquals(null, SF_BulkUploadUtils.getUnitTypeAPIName('    '));

    }

    @isTest
    static void test_getStreetType() {
        system.AssertEquals(null, SF_BulkUploadUtils.getStreetTypeAPIName(''));

        system.AssertEquals('RD', SF_BulkUploadUtils.getStreetTypeAPIName('  Rd  '));
        system.AssertEquals('RD', SF_BulkUploadUtils.getStreetTypeAPIName(' Road '));
        system.AssertEquals('CT', SF_BulkUploadUtils.getStreetTypeAPIName(' court '));
        system.AssertEquals('CT', SF_BulkUploadUtils.getStreetTypeAPIName(' Courts '));

        system.AssertEquals(null, SF_BulkUploadUtils.getStreetTypeAPIName(' Cours '));
        system.AssertEquals(null, SF_BulkUploadUtils.getStreetTypeAPIName('   '));
        system.AssertEquals(null, SF_BulkUploadUtils.getStreetTypeAPIName(''));
        system.AssertEquals(null, SF_BulkUploadUtils.getStreetTypeAPIName(null));

    }

    @isTest
    static void test_getState() {
        system.AssertEquals('NSW', SF_BulkUploadUtils.getStateAPIName(' new south   WalES '));
        system.AssertEquals('NSW', SF_BulkUploadUtils.getStateAPIName('NEW  SOUTH  WALES'));
        system.AssertEquals('NSW', SF_BulkUploadUtils.getStateAPIName(' NSW '));

        system.AssertEquals('QLD', SF_BulkUploadUtils.getStateAPIName('QUEENSLAND'));
        system.AssertEquals('TAS', SF_BulkUploadUtils.getStateAPIName('TASMANIA'));
        system.AssertEquals('ACT', SF_BulkUploadUtils.getStateAPIName('AUSTRALIan capital Territory'));
        system.AssertEquals('WA', SF_BulkUploadUtils.getStateAPIName('weSTERN  AustrALIA'));
        system.AssertEquals('WA', SF_BulkUploadUtils.getStateAPIName('WA'));
        system.AssertEquals('ACT', SF_BulkUploadUtils.getStateAPIName(' ACt '));
        system.AssertEquals('TAS', SF_BulkUploadUtils.getStateAPIName('Tas'));

        system.AssertEquals(null, SF_BulkUploadUtils.getStateAPIName(null));
        system.AssertEquals(null, SF_BulkUploadUtils.getStateAPIName(' '));
        system.AssertEquals(null, SF_BulkUploadUtils.getStateAPIName(''));
        system.AssertEquals(null, SF_BulkUploadUtils.getStateAPIName(' NPD '));
    }
    
}