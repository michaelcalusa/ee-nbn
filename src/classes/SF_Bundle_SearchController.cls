/**
 * Created by Gobind.Khurana on 21/05/2018.
 */

public with sharing class SF_Bundle_SearchController {

    /*
        * Fetch bundle recent records for the given max count
        * Parameters : maxCount.
        * @Return : Returns a list of DF Bundle search result records
    */

    @AuraEnabled
    public static String getRecords(String searchTerm, Boolean isTriggerBySearch, String productType) {
        System.debug('Search Term values is : ' + searchTerm);
        System.debug('isTriggerBySearch : ' + isTriggerBySearch);
        System.debug('productType : ' + productType);

        List<DF_Opportunity_Bundle__c> bundleList = new List<DF_Opportunity_Bundle__c>();
        List<Object_Record_Type__mdt> mdtRecords = (List<Object_Record_Type__mdt>) SF_CS_API_Util.getCustomMetadataByType('Object_Record_Type__mdt', productType);
        String recordTypeName = mdtRecords.isEmpty() ? null : mdtRecords[0].Opportunity_Bundle_Record_Type_Name__c;
        System.debug('recordTypeName:: '+recordTypeName);
        Id oppRecordTypeId = recordTypeName == null? null : Schema.SObjectType.DF_Opportunity_Bundle__c.getRecordTypeInfosByDeveloperName().get(recordTypeName).getRecordTypeId();

        try {
            //if(String.isEmpty(searchTerm)){
            String strOppBundleExiry = SF_CS_API_Util.getCustomSettingValue('NS_BUNDLE_EXPIRY_DURATION');
            Integer intOppBundleExpiryDuration = String.isBlank(strOppBundleExiry)? 60: Integer.valueOf(strOppBundleExiry);


            Date dtExpiry;
            if (GlobalUtility.getReleaseToggle('NBN_Select_MR1908')) {
                //CPST-8108 - 60 business days expiry
                Date today = System.Date.today();
                dtExpiry = DF_AS_GlobalUtility.subtractBusinessDays(today, intOppBundleExpiryDuration);
            } else {
                dtExpiry = System.Date.today().addDays(-intOppBundleExpiryDuration);
            }

            System.debug('Expiry date for NSQ is: ' + dtExpiry);

            List<User> userLst = [
                    SELECT  AccountId,
                            ContactId
                    FROM User
                    WHERE id = :UserInfo.getUserId()];

            System.debug('User details ' + userLst);

            if(userLst.size()>0 && userLst.get(0).AccountId!=null){
                if(String.isEmpty(searchTerm) && !isTriggerBySearch){
                    bundleList = [
                            SELECT Id,
                                Name,
                                Opportunity_Bundle_Name__c,
                                CreatedDate,
                                DF_Quote_Count__c,
                                DF_SF_Request_Count__c
                            FROM DF_Opportunity_Bundle__c
                            WHERE Account__c = :userLst.get(0).AccountId
                                AND RecordTypeId =:oppRecordTypeId
                                AND CreatedDate >= :dtExpiry
                            ORDER BY Name DESC NULLS FIRST ];
                }
                else if(!String.isEmpty(searchTerm)){
                    bundleList = [
                            SELECT Id,
                                    Name,
                                    Opportunity_Bundle_Name__c,
                                    CreatedDate,
                                    DF_Quote_Count__c,
                                    DF_SF_Request_Count__c
                            FROM DF_Opportunity_Bundle__c
                            WHERE Account__c = :userLst.get(0).AccountId
                                AND Opportunity_Bundle_Name__c = :searchTerm
                                AND RecordTypeId =:oppRecordTypeId
                                AND CreatedDate >= :dtExpiry
                            ORDER BY Name DESC NULLS FIRST];
                }
            }else{
                throw new CustomException('User record with the RSP account not found');
            }


        } catch(Exception e) {
            throw new CustomException(e.getMessage());
        }

        return JSON.serialize(bundleList);
    }
}