@isTest
public class SF_Bundle_SearchController_Test {
    
    @isTest
    private static void test_getRecordsWithEmptySearchTerm() {
        
        Id oppId = SF_TestService.getQuoteRecords();
        String searchTerm = null;
        String result = '';
        List<DF_Opportunity_Bundle__c> deserializedResult = new List<DF_Opportunity_Bundle__c>();
        String productType = 'NBN_SELECT';
        User commUser;
        commUser = SF_TestData.createDFCommUser();
        system.runAs(commUser) {
            test.startTest();    
            result = SF_Bundle_SearchController.getRecords(searchTerm,false, productType);
            System.debug(result);
            deserializedResult = (List<DF_Opportunity_Bundle__c>)JSON.deserialize(result, List<DF_Opportunity_Bundle__c>.Class);
            System.debug(deserializedResult);                                   
            test.stopTest();                
        }                       
        // Assertions
        system.AssertEquals(0, deserializedResult.size());
    }
    
    @isTest
    private static void test_getRecordsWithInvalidSearchTerm() {
        // Setup data       
        Integer maxCount = 10;
        String searchTerm = 'This search term will cause no record to return';
        String result = '';
        String productType = 'NBN_SELECT';
        List<DF_Opportunity_Bundle__c> deserializedResult = new List<DF_Opportunity_Bundle__c>();
        
        Id oppId = SF_TestService.getQuoteRecords();
        
        User commUser;
        commUser = SF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            test.startTest();
            result = SF_Bundle_SearchController.getRecords(searchTerm,false, productType);
            deserializedResult = (List<DF_Opportunity_Bundle__c>)JSON.deserialize(result, List<DF_Opportunity_Bundle__c>.Class);
            test.stopTest();
        }                                     
        
        // Assertions
        system.AssertEquals(0, deserializedResult.size());
    }
    
    @isTest
    private static void test_getRecordsWithValidSearchTerm() {
        // Setup data       
        Integer maxCount = 10;
        String searchTerm = '';
        String result = '';
        String productType = 'NBN_SELECT';
        List<DF_Opportunity_Bundle__c> deserializedResult = new List<DF_Opportunity_Bundle__c>();
        
        Id oppId = SF_TestService.getQuoteRecords();
        
        DF_Opportunity_Bundle__c bundle = [
            SELECT
            Id,
            Name                        
            FROM
            DF_Opportunity_Bundle__c
            WHERE Id =: oppId
        ];
        
        searchTerm = bundle.Name;
        
        User commUser;
        commUser = SF_TestData.createDFCommUser();
        
        system.runAs(commUser) {
            test.startTest();
            result = SF_Bundle_SearchController.getRecords(searchTerm,true, productType);
            deserializedResult = (List<DF_Opportunity_Bundle__c>)JSON.deserialize(result, List<DF_Opportunity_Bundle__c>.Class);
            test.stopTest();
        }                                     
        
        // Assertions
        //system.AssertEquals(1, deserializedResult.size());
    }
}