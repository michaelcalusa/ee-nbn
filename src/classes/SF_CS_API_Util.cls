/**
* Created by Gobind.Khurana on 22/05/2018.
*/

public class SF_CS_API_Util {
    
    /*
    * Creates an APISession for the Product Basket creation
    * Parameters : basket, basket for which the session is created.
    * @Return : cscfga.API_1.ApiSession returns an API session
    */
    public static cscfga.API_1.ApiSession createApiSession(cscfga__Product_Basket__c basket) {
        cscfga.SessionManager.SessionInfo session;
        
        if(basket == null) {
            session = cscfga.SessionManager.getOrCreateSession('SessionKey: ' + Datetime.now(), 'User Session');
        } else {
            session = cscfga.SessionManager.loadSession(basket.cscfga__User_Session__c);
        }
        cscfga.API_1.ApiSession apiSession = cscfga.API_1.getApiSession();
        apiSession.loadUserSession(session.userSession.Id);
        
        return apiSession;
    }
    
    /*
    * Creates an Basket for the APISession Passed.
    * Parameters : apiSession, apiSession object
    * @Return : cscfga__Product_Basket__c returns an Product Basket
    */
    public static cscfga__Product_Basket__c createBasket(cscfga.API_1.ApiSession apiSession) {
        cscfga__Product_Basket__c basket;
        if(apiSession != null)
            basket = apiSession.getBasket();
        return basket;
    }
    
    /*
    * Find production definition based on the id passed
    * Parameters : defId, Product definition id
    * @Return : List<cscfga__Product_Definition__c> list of product definition
    */
    public static List<cscfga__Product_Definition__c> findProdDefinition(String defId){
        sObject product_Definition_Obj = new cscfga__Product_Definition__c();
        List<cscfga__Product_Definition__c> lst_Prd_Definition = new List<cscfga__Product_Definition__c>();
        if(String.isNotEmpty(defId)){
            String queryStr = getQuery(product_Definition_Obj, ' WHERE cscfga__Active__c = true and Id = '+ '\'' + defId + '\'');
            if(queryStr != null && queryStr.trim().length()>0){
                lst_Prd_Definition = (List<cscfga__Product_Definition__c>)getQueryRecords(queryStr);
            }
        }
        return lst_Prd_Definition;
    }
    
    /*
    * Adds the product configuration to the basket
    * Parameters : defId, Product definition id
    * Parameters : apiSession, apiSession to which the config needs to be added.
    * @Return : List<cscfga__Product_Definition__c> list of product definition
    */
    public static cscfga.ProductConfiguration addProductConfigToBasket(String prodDefId, cscfga.API_1.ApiSession apiSession) {
        cscfga.ProductConfiguration prdCfg = null;
        if(String.isNotEmpty(prodDefId) && apiSession != null) {
            apiSession.setProductToConfigure(new cscfga__Product_Definition__c(Id = prodDefId));
            
            apiSession.executeRules();
            prdCfg = apiSession.getConfiguration();
        }
        return prdCfg;
    }
    
    /*
    * Saves the product configuration to the basket
    * Parameters : product configuration, apiSession to which the config needs to be added
    * @Return : basket to which the configuration is saved
    */
    public static cscfga__Product_Basket__c saveConfig(cscfga.ProductConfiguration config, cscfga.API_1.ApiSession apiSession) {
        cscfga__Product_Basket__c basket;
        if(config != null && apiSession != null) {
            apiSession.executeRules();
            cscfga.ValidationResult vr = apiSession.validateConfiguration(); //ValidationResult
            basket = apiSession.persistConfiguration(true);
            
            config = null;
        }
        return basket;
    }
    
    /*
    * Method to associate basket with Opportunity
    * Parameters : basketId, Opportunity Id.
    * @Return : void
    */
    public static void associateBasketWithOppty(Id BasketId, Id OpptyId){
        if(BasketId != null && OpptyId != null){
            cscfga__Product_Basket__c BK = [Select Id, cscfga__Opportunity__c From cscfga__Product_Basket__c Where Id =: BasketId];
            Bk.cscfga__Opportunity__c = OpptyId;
            update Bk;
        }
    }
    
    /*
    * Method to sync basket with Opportunity
    * Parameters : basketId
    * @Return : String which tells if the synchronisation was successful or if there was an error
    */
    public static String syncWithOpportunity(Id BasketId){
        if(BasketId != null){
            List <cscfga__Product_Configuration__c> orphanProdConfs = [select c.id
                                                                       from cscfga__Product_Configuration__c c
                                                                       where cscfga__product_basket__c = :basketId and
                                                                       cscfga__root_configuration__c = null and
                                                                       id not in (select csbb__product_configuration__c
                                                                                  from csbb__Product_Configuration_Request__c
                                                                                  where csbb__product_basket__c = :basketId
                                                                                  and csbb__product_configuration__c != null
                                                                                 )];
            
            if (orphanProdConfs.size() > 0) {
                delete orphanProdConfs;
            }
            
            cscfga__Product_Basket__c pb = [select id, cscfga__opportunity__c, csbb__Synchronised_with_Opportunity__c, cscfga__Basket_Status__c
                                            from cscfga__Product_Basket__c where id = :basketId];
            
            if(pb.cscfga__Basket_Status__c != 'Valid') {
                return 'Synchronise with opportunity failed: product basket has to be valid.';
            }
            
            if(pb.csbb__Synchronised_with_Opportunity__c) {
                pb.csbb__Synchronised_with_Opportunity__c=false;
                pb.csordtelcoa__Synchronised_with_Opportunity__c = false;
                update pb;
            }
            pb.csbb__Synchronised_with_Opportunity__c = true;
            pb.csordtelcoa__Synchronised_with_Opportunity__c = true;
            update pb;
            return 'sychronised with opportunity';
        } else
            return 'Synchronise with opportunity failed: basket id null';
    }
    
    /*
    * Method to get custom setting value
    * Parameters : custom setting name.
    * @Return : String which contains the custom setting value
    */
    public static String getCustomSettingValue(String csName){
        String value = null;
        if(String.isNotBlank(csName)){
            NS_Custom_Options__c customSettingVal = NS_Custom_Options__c.getValues(csName);
            if(customSettingVal != null){
                value = customSettingVal.Value__c;
            }
        }
        return value;
    }
    
    /*
    * Method to get boolean custom setting value
    * Parameters : custom setting name.
    * @Return : true if custom setting equals 'true', ignoring case. false otherwise
    */
    public static Boolean isCustomSettingEnabled(String csName){
        return 'true'.equalsIgnoreCase(getCustomSettingValue(csName));
    }

    /*
    * Method to create product configuration request
    * Parameters : basket, product configuration, offer, category.
    * @Return : Returns a product configuration request instance
    */
    public static csbb__Product_Configuration_Request__c createProductConfigurationRequest(cscfga__Product_Basket__c basket, cscfga__Product_Configuration__c config, String offerId, cscfga__Product_Category__c category ){
        csbb__Product_Configuration_Request__c pcr = new csbb__Product_Configuration_Request__c();
        if(basket != null && config != null && offerId != null && category != null){
            pcr.csbb__Product_Basket__c = basket.Id;
            pcr.csbb__Product_Configuration__c = config.Id;
            pcr.csbb__Offer__c = offerId;
            pcr.csbb__Optionals__c = '{"selectedAddressDisplay":null}';
            pcr.csbb__Product_Category__c = category.Id;
            pcr.csbb__Status__c='finalized';
            //pcr.csbb__Total_OC__c='';
            //pcr.CSBB__TOTAL_QRC__C='';
            //pcr.CSBB__TOTAL_SOV__C='';
            //pcr.CSBB__TOTAL_YRC__C='';
        }
        return pcr;
    }
    
    /*
    * Method to get a list of product configs linked to the basket
    * Parameters : basketId, Id of the basket to be queried.
    * @Return : List<cscfga__Product_Configuration__c> list of product configuration
    */
    public static List<cscfga__Product_Configuration__c> getProductBasketConfigs(String basketId) {
        sObject product_Confg_Obj = new cscfga__Product_Configuration__c();
        List<cscfga__Product_Configuration__c> lst_Prd_Cfgs = new List<cscfga__Product_Configuration__c>();
        if(!String.isEmpty(basketId)){
            String queryStr = getQuery(product_Confg_Obj, ' WHERE cscfga__Product_Basket__c = '+ '\'' + basketId + '\'');
            if(queryStr != null && queryStr.trim().length()>0){
                lst_Prd_Cfgs = (List<cscfga__Product_Configuration__c>)getQueryRecords(queryStr);
            }
        }
        return lst_Prd_Cfgs;
    }
    
    
    /*
    * Method to get a single string of object fields with seperator
    * Parameters : List<String> theList, String separator
    * @Return : String
    */
    @AuraEnabled
    public static String joinList(List<String> theList, String separator) {
        if (theList == null) { return null; }
        if (separator == null) { separator = ''; }
        String joined = '';
        Boolean firstItem = true;
        for (String item : theList) {
            if(null != item) {
                if(firstItem){ firstItem = false; }
                else { joined += separator; }
                joined += item;
            }
        }
        return joined;
    }
    
    /*
    * Method to create query String with Condition
    * Parameters : sObject ObjectName, String queryCondition
    * @Return : String
    */
    @AuraEnabled
    public static String getQuery(sObject ObjectName, String queryCondition){
        String query;
        List<String> fieldsList = discoverAccessibleFields(ObjectName);
        if(ObjectName != null && !fieldsList.isEmpty()){
            query = 'SELECT '+ joinList(fieldsList, ',') + ' FROM '+ ObjectName.getSObjectType();
            if(String.isNotBlank(queryCondition)){
                query = query + queryCondition;
            }
        }
        return query;
    }
     /*
    * Method to get list of Sobject Records
    * Parameters : String ObjectName, String queryCondition
    * @Return : List<Sobject>
    */
    /*public static List<Sobject> getSobjectRecords(String strObjectName, String strFilterCondition){
        Sobject queryObject = Schema.getGlobalDescribe().get(strObjectName).newSobject();
        return Database.query(getQuery(queryObject, strFilterCondition));
    }*/
    /*
    * Method to get List of Fields which are accessible to User
    * Parameters : sObject ObjectName
    * @Return : List<String>
    */
    @AuraEnabled
    public static List<String> discoverAccessibleFields(sObject newObj) {
        List<String> accessibleFields = new List<String>();
        if(newObj != null){
            Map<String, Schema.SobjectField> fields = newObj.getSObjectType().getDescribe().fields.getMap();
            for (String s : fields.keySet()) {
                if (fields.get(s).getDescribe().isAccessible()) {
                    accessibleFields.add(s);
                }
            }
        }
        return accessibleFields;
    }
    
    
    /*
    * Method to do run SOQL on dynamic query String
    * Parameters : String queryString
    * @Return : List<sObject>
    */
    @AuraEnabled
    public static List<sObject> getQueryRecords(String queryString){
        if(queryString != null && queryString.trim().length()>0){
            queryString += ' limit '+ (Limits.getLimitQueryRows() - Limits.getQueryRows());
            List<sObject> lst_QueryRecords = Database.query(queryString);
            return lst_QueryRecords;
        } else{
            return null;
        }
    }
    
    /*
    * Method to get RecordType Id for all Direct Fibre custom objects      
    * Parameters : recordtype developer name, sObject type
    * @Return : Id of the recordtype
    */
    public static Id getRecordType(String RTNAME, String SobjectType){
        Id recTypeId = null;
        if(String.isNotBlank(RTNAME) && String.isNotBlank(SobjectType)){
            recTypeId= Schema.getGlobalDescribe().get(SobjectType).getDescribe().getRecordTypeInfosByName().get(RTNAME).getRecordTypeId();
        }
        return recTypeId;
    }
    
    /*
    * Method to get RecordType Id for Direct Fibre
    * Parameters : none
    * @Return : Id of the recordtype
    */
    public static Id getDFRecordType(){
        String RTNAME = SF_CS_API_Util.getCustomSettingValue('SF_RECORDTYPE_DEVELOPER_NAME');
        Id oppRecordTypeId;
        if(String.isNotEmpty(RTNAME)){
            sObject rtObject = new RecordType();
            String queryStr = SF_CS_API_Util.getQuery(rtObject, ' WHERE BusinessProcessId != null AND SobjectType = '+ '\'' + 'Opportunity' + '\'' + ' AND IsActive = true AND DeveloperName = '+ '\'' + RTNAME + '\'');
            List<RecordType> rtList = (List<RecordType>)SF_CS_API_Util.getQueryRecords(queryStr);
            oppRecordTypeId = !rtList.isEmpty() ? rtList.get(0).Id : null;
        }
        return oppRecordTypeId;
    }
    
    
    /*
    *   Method to return metadata record by type and name
    *   Paramters: String metadata type, String DeveloperName
    *   @Return : List<sObjects>
    */
    public static List<sObject> getCustomMetadataByType(String metadataType, String name){
        SObjectType metadataObj = Schema.getGlobalDescribe().get(metadataType);
        
        system.debug('***metadataObj**'+metadataObj);
        String queryString = 'SELECT '+ getSobjectFields(metadataObj) +
            ' FROM ' + metadataType +
            ' WHERE DeveloperName = \'' + name + '\'';
        
        system.debug('**metadataType in getCustomMetadataByType** :'+metadataType+'**name** :'+name);
        
        List<sObject> result = Database.query(queryString);
        
        system.debug('**result in getCustomMetadataByType** :'+result);
        return result;
    }
    
    public static String getSobjectFields(SObjectType sot){
        String fieldString;
        //SObjectType sot = Schema.getGlobalDescribe().get(so);
        List<Schema.SObjectField> fields = sot.getDescribe().fields.getMap().values();
        fieldString = fields[0].getDescribe().LocalName;
        for (Integer i = 1; i < fields.size(); i++) {
            fieldString += ',' + fields[i].getDescribe().LocalName;
        }
        return fieldString;
    }
    
    /*
    * Method to splice a list into several lists
    * Parameters : List of sObject (the list that has to be spliced) and the size of each list
    * @Return : List<List<sObject>>
    */
    @AuraEnabled
    public static List<List<sObject>> spliceBy(List<sObject> objs, Integer size){
        List<List<sObject>> resultList = new List<List<sObject>>();
        
        Integer numberOfChunks = objs.size() / size;
        
        for(Integer j = 0; j < numberOfChunks; j++ ){
            List<sObject> someList = new List<sObject>();
            for(Integer i = j * size; i < (j+1) * size; i++){
                someList.add(objs[i]);
            }
            resultList.add(someList);
        }
        if(numberOfChunks * size < objs.size()){
            List<sObject> aList = new List<sObject>();
            for(Integer k = numberOfChunks * size ; k < objs.size(); k++){
                aList.add(objs[k]);
            }
            resultList.add(aList);
        }
        return resultList;
    }
    
}