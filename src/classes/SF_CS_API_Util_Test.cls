/**
* Class for testing Cloudsense API Util methods.
*/
@isTest
public class SF_CS_API_Util_Test {
    /**
	* Tests findProdDefinition method
	*/
    @isTest static void findProdDefinitionTest()
    {
        Account acc = SF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = SF_TestService.getNewBasketWithConfig(acc);
        Id pdtDefinitionId;
        List<cscfga__Product_Configuration__c> productConfigList = [SELECT cscfga__Product_Definition__c FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basket.Id];
        if(!productConfigList.isEmpty()){
            pdtDefinitionId = productConfigList.get(0).cscfga__Product_Definition__c;
        
        Test.startTest();
        
        List<cscfga__Product_Definition__c> pdtDefinitionList = SF_CS_API_Util.findProdDefinition(pdtDefinitionId);
        System.debug('PP pdtDefinitionList:'+pdtDefinitionList);
        Test.stopTest();
        }
    }
    
    /**
	* Tests getProductBasketConfigs method
	*/
    @isTest static void getProductBasketConfigsTest()
    {
        Account acc = SF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = SF_TestService.getNewBasketWithConfig(acc);
        
        Test.startTest();
        
        List<cscfga__Product_Configuration__c> prodConfigList = SF_CS_API_Util.getProductBasketConfigs(basket.Id);
        System.debug('PP prodConfigList:'+prodConfigList);
        Test.stopTest();
    }

    @IsTest
    private static void shouldReturnTrueIfCustomSettingIsTrue() {
        Test.startTest();
        for (String trueString : new String[]{'true', 'TRuE', 'TRUE'}) {
            String optionName = 'testCustomOptions';
            NS_Custom_Options__c testCustomOptions = new NS_Custom_Options__c(Name = optionName, Value__c = trueString);
            insert testCustomOptions;
            Assert.isTrue(SF_CS_API_Util.isCustomSettingEnabled(optionName));
        }

        Test.stopTest();
    }

    @IsTest
    private static void shouldReturnFalseIfCustomSettingIsNotTrue() {
        Test.startTest();
        for (String falseString : new String[]{'false', 'FAlsE', 'FALSE', '', null, 'blah'}) {
            String optionName = 'testCustomOptions';
            NS_Custom_Options__c testCustomOptions = new NS_Custom_Options__c(Name = optionName, Value__c = falseString);
            insert testCustomOptions;
            Assert.isFalse(SF_CS_API_Util.isCustomSettingEnabled(optionName));
        }

        Test.stopTest();
    }

    @IsTest
    private static void shouldReturnFalseWhenOptionNameIsNull() {
        Test.startTest();

        Assert.isFalse(SF_CS_API_Util.isCustomSettingEnabled(null));

        Test.stopTest();
    }

    @IsTest
    private static void shouldReturnFalseWhenCustomOptionIsNotPresent() {
        Test.startTest();

        Assert.isFalse(SF_CS_API_Util.isCustomSettingEnabled('some random thing'));

        Test.stopTest();
    }
}