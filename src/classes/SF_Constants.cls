public with sharing class SF_Constants {
	
	public static final String RAG_STATUS_RED = 'RED'; 
	public static final String APPAIN_DESKTOP_ASSESSMENT_STATUS_PENDING = 'PENDING';
	public static final String APPAIN_DESKTOP_ASSESSMENT_STATUS_IN_PROCESS = 'IN PROGRESS';
	public static final String APPAIN_DESKTOP_ASSESSMENT_STATUS_COMPLETED = 'COMPLETED';

	public static final String QUOTE_STATUS_QUICK_QUOTE = 'Quick Quote';
	public static final String QUOTE_STATUS_ORDER_DRAFT = 'Order Draft';
	public static final String QUOTE_STATUS_SUBMITTED = 'Submitted';
	public static final String QUOTE_STATUS_ACCEPTED = 'Accepted';

    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_UNDEFINED = 'Undefined';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_NULL = 'Null';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_SATELLITE = 'Satellite';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_WIRELESS = 'Wireless';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FIBRE = 'Fibre';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_HFC = 'HFC';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_COPPER = 'Copper';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_HFC_PILOT = 'HFC Pilot';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FTTC = 'FTTC';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FIBRE_TO_THE_CURB = 'Fibre to the curb';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FTTB = 'FTTB';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FIBRE_TO_THE_BUILDING = 'Fibre to the building';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FIXED_WIRELESS = 'Fixed Wireless';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FIBRE_TO_THE_NODE = 'Fibre to the node';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FTTN = 'FTTN';
    public static final String SERVICE_FEASIBILITY_TECHNOLOGY_FTTP = 'FTTP';
    
    public static final String SERVICE_FEASIBILITY_SERVICE_TYPE_BROWN_FTTB = 'Brownfields FTTB-Copper';
    public static final String SERVICE_FEASIBILITY_SERVICE_TYPE_GREEN_FTTB = 'Greenfields FTTB-Copper';
    public static final String SERVICE_FEASIBILITY_SERVICE_TYPE_BROWN_FTTC = 'Brownfields FTTC-Copper';
    public static final String SERVICE_FEASIBILITY_SERVICE_TYPE_GREEN_FTTC = 'Greenfields FTTC-Copper';
    public static final String SERVICE_FEASIBILITY_SERVICE_TYPE_BROWN_FTTN = 'Brownfields FTTN-Copper';
    public static final String SERVICE_FEASIBILITY_SERVICE_TYPE_GREEN_FTTN = 'Greenfields FTTN-Copper';
    public static final String SERVICE_FEASIBILITY_PRODUCT_UNAVAILABLE = 'Product Unavailable';
    
    public static final String NAMED_CRED_PREFIX = 'callout:';
    public static final String HTTP_METHOD_GET = 'GET';
    public static final String HTTP_METHOD_POST = 'POST';
 	public static final String HTTP_HEADER_ACCEPT = 'ACCEPT';    
    public static final String CONTENT_TYPE = 'Content-Type';
    public static final String CONTENT_TYPE_JSON = 'application/json';     
    public static final String NBN_DOMAIN = 'https://www.nbnco.com.au';
    
    public static final Integer HTTP_STATUS_200 = 200;
    public static final Integer HTTP_STATUS_201 = 201;
    public static final Integer HTTP_STATUS_202 = 202;
    public static final Integer HTTP_STATUS_203 = 203;
    public static final Integer HTTP_STATUS_204 = 204;
    public static final Integer HTTP_STATUS_205 = 205;
    public static final Integer HTTP_STATUS_206 = 206;
    public static final Integer HTTP_STATUS_404 = 404;
    public static final Integer HTTP_STATUS_500 = 500;
    
    public static final String ERROR = 'Error';   

    public static final String ACTIVITY_NAME = 'createBillingEvent';
    public static final String MSG_NAME = 'ManageBillingEventcreateBillingEventRequest';
    public static final String BUSINESS_SERVICE_NAME = 'ManageBillingEvent';
    public static final String BUSINESS_SERVICE_VERSION = 'V2.0';
    public static final String MSG_TYPE = 'Request';
    public static final String SECURITY = 'Placeholder Security';
    public static final String COMMUNICATION_PATTERN = 'RequestReply';
    public static final String BUSINESS_CHANNEL = 'Enterprise Fulfilment';
    public static final String ORDER_PRIORITY = '6';
    public static final String NOTIFICATION_TYPE = 'BillingEvent';
    public static final String INTERACTION_STATUS = 'Complete';
    public static final String ORDER_TYPE = 'Connect'; 
    public static final String ACTION = 'ADD';
    public static final String SITE_SURVEY_NAME = 'Site Survey Charges';
    public static final String NBN_SELECT_NAME = 'NBN Select';
    public static final String ORDER_OBJECT = 'DF_Order__c';
    public static final String QUOTE_OBJECT = 'DF_Quote__c';
    
    public static final String RAG_RED = 'Red';
    public static final String RAG_GREEN = 'Green';
    public static final String RAG_AMBER = 'Amber';

    public static final String NS_BUSINESS_PERMISSION_SET = 'NS_BC_Customer_Permission';
    public static final String NS_BUSINESS_PLUS_PERMISSION_SET = 'NS_BC_Customer_Plus_Permission';

	public static final String TEMPLATE_ORDER_ACKNOWLEDGED = 'NS Order Acknowledged';
    public static final String TEMPLATE_ORDER_ACCEPTED = 'NS Order Accepted';
    public static final String TEMPLATE_ORDER_SCHEDULED = 'NS Order Scheduled';
    public static final String TEMPLATE_ORDER_RSP_ACTION_REQUIRED = 'NS Order RSP Action Required'; 
    public static final String TEMPLATE_ORDER_INFO_REQUIRED_REMINDER = 'NS Order Info Required Reminder';
    public static final String TEMPLATE_ORDER_RSP_ACTION_COMPLETED = 'NS Order RSP Action Completed'; 
    public static final String TEMPLATE_ORDER_CANCELLED = 'NS Order Cancelled';
    public static final String TEMPLATE_ORDER_CONSTRUCTION_STARTED = 'NS Order Construction Started';
    public static final String TEMPLATE_ORDER_APPOINTMENT_SCHEDULED = 'NS Order Appointment Scheduled';
    public static final String TEMPLATE_ORDER_CONSTRUCTION_COMPLETED = 'NS Order Construction Complete';
    //CPST-7850 | michaelcalusa@nbnco.com.au | ability to show revised completion date in NS
    public static final String TEMPLATE_ORDER_RESCHEDULED = 'NS Order Rescheduled';
    public static final String TEMPLATE_ORDER_ON_SCHEDULE = 'NS Order Rescheduled';
}