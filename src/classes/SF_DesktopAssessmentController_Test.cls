@IsTest
public class SF_DesktopAssessmentController_Test {

    /**
	* Tests processForDA method
	*/
    @isTest static void processForDATest()
    {
        Account acc = SF_TestData.createAccount('Test Account');
        cscfga__Product_Basket__c basket = SF_TestService.getNewBasketWithConfig(acc);
        Opportunity parentOpp = SF_TestData.createOpportunity('Test Parent Opp');
        insert parentOpp;
        Opportunity opp = SF_TestData.createOpportunity('Test Opp');
        opp.Parent_Opportunity__c = parentOpp.Id;
        insert opp;
        basket.cscfga__Opportunity__c = opp.Id;
        update basket;
        
        Test.startTest();
        String oppId = SF_DesktopAssessmentController.processForDA(opp.Id);
        Test.stopTest();
        System.assertNotEquals('null',oppId);
    }
}