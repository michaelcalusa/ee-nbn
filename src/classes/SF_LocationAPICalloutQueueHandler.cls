/************************************************************************************
Version                 : 1.0 
Description/Function    : Make Callouts to LAPI, and Enqueue callout to PNI   
* Developer                   Date                   Description
* -------------------------------------------------------------------------                
REJEESH RAGHAVAN             27-06-2018              Created.
************************************************************************************/
public class SF_LocationAPICalloutQueueHandler implements Queueable, Database.AllowsCallouts{
	private List<DF_SF_Request__c> lstSfrToCallout;
    private String strOppBunId;
    
    public SF_LocationAPICalloutQueueHandler(String strOpportunityBundleId, List<DF_SF_Request__c> lstServiceFeasiblityRequest){ 
        this.lstSfrToCallout = new List<DF_SF_Request__c>(lstServiceFeasiblityRequest);
        this.strOppBunId = strOpportunityBundleId;
    }
    public void execute(QueueableContext qc){
        
        Boolean isCISEnabled = String.isBlank(SF_CS_API_Util.getCustomSettingValue('CIS_ENABLE_CALLOUT'))?false:Boolean.valueOf(SF_CS_API_Util.getCustomSettingValue('CIS_ENABLE_CALLOUT'));    
        
        if(isCISEnabled)
        {
            NS_CISCalloutHandler.searchLocationFromCIS(this.lstSfrToCallout);
        	return;
    	}
        
        
    	List<DF_SF_Request__c> lstSfrToUpdate = new List<DF_SF_Request__c>(), lstSfrToPNI = new List<DF_SF_Request__c>();
        try{
        	Boolean isPniEnabled = String.isBlank(SF_CS_API_Util.getCustomSettingValue('PNI_ENABLE_CALLOUT'))?false:Boolean.valueOf(SF_CS_API_Util.getCustomSettingValue('PNI_ENABLE_CALLOUT'));
	        for(DF_SF_Request__c sfrRec: lstSfrToCallout){
	            //do LAPI Callout
	            lstSfrToUpdate.add(SF_LocationAPIService.getLocation(sfrRec));
                SF_LAPI_APIServiceUtils.markDuplicateLocId(lstSfrToUpdate);
	        }
	        //update SFR records with LAPI response
	        if(!lstSfrToUpdate.isEmpty()){
                //filter list of valid technologies to be sent ot PNI
                lstSfrToPNI.addAll(NS_PNI_Util.filterSfrList(lstSfrToUpdate));
                SF_ServiceFeasibilityResponse sfrJSRec;
	        	for(DF_SF_Request__c pniSfrRec: lstSfrToUpdate){
                    if(isPniEnabled && NS_PNI_Util.filterSfrList(pniSfrRec)){
                        sfrJSRec = (SF_ServiceFeasibilityResponse)System.JSON.deserialize(pniSfrRec.Response__c, SF_ServiceFeasibilityResponse.class);
                        sfrJSRec.Status = SF_LAPI_APIServiceUtils.STATUS_PENDING;
                        pniSfrRec.Response__c = JSON.serialize(sfrJSRec);
                    }
                }
                System.debug('SF_LocationAPICalloutQueueHandler::SFR: ' + sfrJSRec);
	            Database.update(lstSfrToUpdate);
	        }
	        //check if PNI callout is enabled
            if(isPniEnabled && !lstSfrToPNI.isEmpty()){
                //enqueue PNI callout
                if(!Test.isRunningTest()){
                    System.enqueueJob(new NS_PNI_SendLocationQueueHandler(lstSfrToPNI));
                }
            }
        }catch (Exception ex) {
        	System.debug('SF_LocationAPICalloutQueueHandler::execute EXCEPTION: '+ ex.getMessage() + '\n' +ex.getStackTraceString());
            throw new CustomException(ex.getMessage());
        }
    }
}