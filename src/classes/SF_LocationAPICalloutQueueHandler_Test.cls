@isTest
public class SF_LocationAPICalloutQueueHandler_Test {
    static testMethod void test_LocationCalloutRequestQueue() {
		//Setup data
        String locationId = 'LOC000035375038';
        String response = '{"data":[{"type":"location","id":"LOC000066186204","attributes":{"rolloutRegionIdentifier":"4ATH-01","premises":{"disconnectionDate":"2018-03-09","readyForServiceDate":"2016-08-19","serviceClass":"12","isServiceable":true,"isFrustrated":false,"serviceType":"Brownfields FTTN-Copper","primaryTechnology":"Copper","technologyType":"Copper","csaId":"CSA400000010875","serviceClassDescription":"Serviceable by Copper, Existing Copper Pair in-place not active with NBN Co.","serviceClassReason":null,"technologyOverride":"Undefined","premisesFlag":true,"disconnectionType":"Standard","obsolete":false,"marketable":true,"gnafPersistentId":null,"originalGnafPersistentId":null,"serviceableLocationType":"N","locationConnectionStatus":null,"locationStatus":"ACTIVE","locationName":null,"locationDescription":null,"locationType":"UNIT","serviceLevelRegion":"MAJOR RURAL","buildingType":null,"propertyType":"SDU1","asaId":null,"fsaId":"4ATH","mpsId":null,"csaName":"Townsville 2 CSA","samId":"4ATH-01","daid":"4ATH-01-11","infillFlag":false,"tlsInfillBoundary":null,"infillRegionReadyFor":null,"serviceContinuityRegion":null,"polygonId":null,"parcelId":null,"priId":null,"mostrecentconnectionDate":null,"poiId":"4TNS","poiName":"Townsville","transitionalPoiId":null,"transitionalPoiName":null,"transitionalCsaId":null,"transitionalCsaName":null,"landUse":"RESIDENTIAL","landUseSubclass":null,"newDevelopmentsChargeApplies":null,"firstConnectionDate":null,"firstDisconnectionDate":null,"mostRecentDisconnectionDate":null,"accessTechnologyType":null,"accessSeekerId":null,"isComplexPremise":null,"orderId":null,"orderStatus":null,"orderDateReceived":null,"bandwidthProfile":null,"priorityAssist":null,"deliveryPointIdentifier":null,"isDeleted":"N","sourceSystemC":"UDS","sourceContributor":"PNI","secondaryComplexName":null,"complexRoadNumber1":null,"complexRoadNumber2":null,"complexRoadName":null,"complexRoadTypeCode":null,"complexRoadSuffixCode":null,"complexAddressString":null,"listing_type":"HFL"},"geopoint":{"latitude":-17.263666,"longitude":145.482294}},"relationships":[{"address":{"data":[{"type":"address","id":"LOC000066186204"}]}}]}],"included":[{"type":"address","id":"LOC000066186204","attributes":{"meta":{"relevanceScore":1},"principal":{"fullText":"UNIT 1 18 WHITING ST ATHERTON QLD 4883 Australia","levelType":null,"levelNumber":null,"unitType":"UNIT","unitNumber":"1","roadNumber1":"18","roadNumber2":null,"roadName":"WHITING","roadTypeCode":"ST","locality":"ATHERTON","postcode":"4883","state":"QLD"},"geopoint":{"latitude":-17.263666,"longitude":145.482294},"aliases":[{"fullText":"UNIT 1 18 WHITING ST WONGABEL QLD 4883 Australia","levelType":null,"levelNumber":null,"unitType":"UNIT","unitNumber":"1","roadNumber1":"18","roadNumber2":null,"roadName":"WHITING","roadTypeCode":"ST","locality":"WONGABEL","postcode":"4883","state":"QLD"},{"fullText":"UNIT 1 18 WHITING ST CARRINGTON QLD 4883 Australia","levelType":null,"levelNumber":null,"unitType":"UNIT","unitNumber":"1","roadNumber1":"18","roadNumber2":null,"roadName":"WHITING","roadTypeCode":"ST","locality":"CARRINGTON","postcode":"4883","state":"QLD"},{"fullText":"UNIT 1 18 WHITING ST EAST BARRON QLD 4883 Australia","levelType":null,"levelNumber":null,"unitType":"UNIT","unitNumber":"1","roadNumber1":"18","roadNumber2":null,"roadName":"WHITING","roadTypeCode":"ST","locality":"EAST BARRON","postcode":"4883","state":"QLD"},{"fullText":"UNIT 1 18 WHITING ST KAIRI QLD 4883 Australia","levelType":null,"levelNumber":null,"unitType":"UNIT","unitNumber":"1","roadNumber1":"18","roadNumber2":null,"roadName":"WHITING","roadTypeCode":"ST","locality":"KAIRI","postcode":"4883","state":"QLD"},{"fullText":"UNIT 1 18 WHITING ST TOLGA QLD 4883 Australia","levelType":null,"levelNumber":null,"unitType":"UNIT","unitNumber":"1","roadNumber1":"18","roadNumber2":null,"roadName":"WHITING","roadTypeCode":"ST","locality":"TOLGA","postcode":"4883","state":"QLD"},{"fullText":"UNIT 1 18 WHITING ST WALKAMIN QLD 4883 Australia","levelType":null,"levelNumber":null,"unitType":"UNIT","unitNumber":"1","roadNumber1":"18","roadNumber2":null,"roadName":"WHITING","roadTypeCode":"ST","locality":"WALKAMIN","postcode":"4883","state":"QLD"}]}}],"meta":{"totalMatchedResources":1}}';
		NS_Custom_Options__c cs1 = new NS_Custom_Options__c(name = 'PNI_ENABLE_CALLOUT', Value__c = 'true');
        NS_Custom_Options__c cs2 = new NS_Custom_Options__c(name = 'SF_SITESTATUS', Value__c = 'Valid');
        NS_Custom_Options__c cs3 = new NS_Custom_Options__c(name = 'PNI_VALID_TECHNOLOGIES', Value__c = 'FTTN,FTTB,FTTC');
        NS_Custom_Options__c cs4 = new NS_Custom_Options__c(name = 'PNI_QUERY_OPTIONS', Value__c = 'ADA_JOINT,FJL;SAM_JOINT,DJL');
        List<NS_Custom_Options__c> lstCustSettings = new List<NS_Custom_Options__c>{cs1,cs2,cs3,cs4};
		Database.insert(lstCustSettings);
        
        //Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        //Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        //Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, SF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);        
        sfReq.Product_Type__c = 'NBN_SELECT';
        insert sfReq;
 
        // Generate mock response
        //response = SF_IntegrationTestData.buildLocationDistanceAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
 
        test.startTest();
        	System.enqueueJob(new SF_LocationAPICalloutQueueHandler(String.valueOf(opptyBundle.Id), new List<DF_SF_Request__c>{sfReq}));
        test.stopTest();
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id//];
                                            AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_LAPI_COMPLETED];
        system.Assert(sfReqList.size() > 0);
    }
    static testMethod void test_LocationCalloutRequestQueue_unsuccessful_400() {                   
        //Setup data
        String locationId = 'LOC000035375038';
        String response;
		NS_Custom_Options__c cs1 = new NS_Custom_Options__c(name = 'PNI_ENABLE_CALLOUT', Value__c = 'true');
        NS_Custom_Options__c cs2 = new NS_Custom_Options__c(name = 'SF_SITESTATUS', Value__c = 'Valid');
        NS_Custom_Options__c cs3 = new NS_Custom_Options__c(name = 'PNI_VALID_TECHNOLOGIES', Value__c = 'FTTN,FTTB,FTTC');
        List<NS_Custom_Options__c> lstCustSettings = new List<NS_Custom_Options__c>{cs1,cs2,cs3};
		Database.insert(lstCustSettings);
         
        //Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        //Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        //Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, SF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);        
        sfReq.Product_Type__c = 'NBN_SELECT';
        insert sfReq;
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(400, '', null));
 
        Test.startTest();
        	System.enqueueJob(new SF_LocationAPICalloutQueueHandler(String.valueOf(opptyBundle.Id), new List<DF_SF_Request__c>{sfReq}));
        Test.stopTest();
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id//];
                                            AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_ERROR];
        system.Assert(sfReqList.size() > 0);
    }
}