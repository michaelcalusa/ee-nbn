/**
* Created by Gobind.Khurana on 22/05/2018.
*/

public class SF_LocationAPIService {
    
    public static final String ERR_INVALID_SEARCH_BY_LOCID_INPUTS = 'Invalid SearchByLocationId Inputs.';
    public static final String ERR_INVALID_SEARCH_BY_LATLONG_INPUTS = 'Invalid SearchByLatLong Inputs.';
    public static final String ERR_INVALID_SEARCH_BY_ADDR_INPUTS = 'Invalid SearchByAddress Inputs.';
    public static final String ERR_INVALID_SEARCH_TYPE = 'Search Type was invalid.';
    public static final String ERR_SEARCH_TYPE_NOT_FOUND = 'Search Type was not found.';
    public static final String ERR_ADDR_API_ENDPOINT_PARAMS_EMPTY = 'Address API endpoint parameters are empty.';
    public static final String ERR_RESP_BODY_MISSING = 'Response Body is missing.';
    
    public static Boolean stubPNI = false;
    
    /* 
    Purpose : Orchestrates processing for the different search types 
    */
    @future(callout=true)
    public static void getLocation(String sfReqId) {
        List<DF_SF_request__c> lstSfr = new List<DF_SF_Request__c>();
        lstSfr= Database.query( SF_CS_API_Util.getQuery(new DF_SF_Request__c(), ' WHERE Id = :sfReqId LIMIT 1'));
        DF_SF_request__c sfrToUpdate;
        if(!lstSfr.isEmpty()){
            sfrToUpdate = getLocation(lstSfr[0]);
            Database.update(sfrToUpdate);
        }
    }
    public static DF_SF_Request__c getLocation(DF_SF_Request__c sfReq) {
        //DF_SF_Request__c sfReq;
        String searchType;
        
        try {
            // Get SF Req data
            //sfReq = SF_LAPI_APIServiceUtils.getSFReqDetails(sfReqId);
            
            if (sfReq != null) {
                searchType = sfReq.Search_Type__c;
            }
            
            //system.debug('**sfReqId: ' + sfReqId);
            system.debug('**searchType: ' + searchType);
            
            if (String.isNotEmpty(searchType)) {
                // SearchByLocationID
                if (searchType.equalsIgnoreCase(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID)) {
                    // Validate data inputs
                    if (SF_LAPI_APIServiceUtils.hasValidSearchByLocationIdInputs(sfReq)) {
                        sfReq = getLocationByLocationId(null, sfReq);
                    } else {
                        throw new CustomException(SF_LocationAPIService.ERR_INVALID_SEARCH_BY_LOCID_INPUTS);
                    }
                    // SearchByLatLong  
                } else if (searchType.equalsIgnoreCase(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG)) {
                    // Validate data inputs
                    if (SF_LAPI_APIServiceUtils.hasValidSearchByLatLongInputs(sfReq)) {
                        sfReq=getLocationByLatLong(sfReq);
                    } else {
                        throw new CustomException(SF_LocationAPIService.ERR_INVALID_SEARCH_BY_LATLONG_INPUTS);
                    }
                    // SearchByAddress                  
                } else if (searchType.equalsIgnoreCase(SF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS)) {
                    // Validate data inputs
                    if (SF_LAPI_APIServiceUtils.hasValidSearchByAddressInputs(sfReq)) {
                        sfReq=getLocationByAddress(sfReq);
                    } else {
                        throw new CustomException(SF_LocationAPIService.ERR_INVALID_SEARCH_BY_ADDR_INPUTS);
                    }
                } else {
                    throw new CustomException(SF_LocationAPIService.ERR_INVALID_SEARCH_TYPE);
                }
            } else {
                throw new CustomException(SF_LocationAPIService.ERR_SEARCH_TYPE_NOT_FOUND);
            }
        } catch (Exception e) {
            GlobalUtility.logMessage(SF_LAPI_APIServiceUtils.ERROR, SF_LocationAPIService.class.getName(), 'getLocation', '', '', '', '', e, 0);
            throw new CustomException(e.getMessage());
        }
        return sfReq;
    }
    
    
    public static DF_SF_Request__c getLocationByLocationId(String locationId, DF_SF_Request__c sfReq) {
  final String END_POINT;
        
  Http http = new Http();
  HttpRequest req;
  HTTPResponse res;
        
  String response;
  String locationIdParam;
  String apiName;
  try {
            // Use locationId if populated, else use DF_SF_Request__c sfReq
   if (String.isNotEmpty(locationId)) {
                // Need to query the SFReq rec
    locationIdParam = locationId;
                
                // Get SF Req data - eg. search type etc
    sfReq = SF_LAPI_APIServiceUtils.getSFReqDetails(sfReq.Id);
   } else {
    locationIdParam = sfReq.Location_Id__c;
   }
            
            system.debug('***locationIdParam: **'+locationIdParam);
            
   system.debug('** prodType in getLocationByLocationId: '+sfReq.Product_Type__c);
            
   if(sfReq.Product_Type__c == 'NBN_SELECT')
   {
       apiName = 'Location_API';
   }
            
   DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c,
           Enable_Stubbing_PNI__c
   FROM   DF_Integration_Setting__mdt
   WHERE  DeveloperName = :apiName];
            
   stubPNI = integrSetting.Enable_Stubbing_PNI__c;
   system.debug('**stubPNI: ' + stubPNI);
            
            // Build endpoint
   END_POINT = SF_LAPI_APIServiceUtils.NAMED_CRED_PREFIX + integrSetting.Named_Credential__c + '/' + locationIdParam;
   system.debug('**END_POINT: ' + END_POINT);
            
            // Generate request
   req = SF_LAPI_APIServiceUtils.getHttpRequest(END_POINT);
   system.debug('**LAPI Request sent: '+req);
   system.debug('**Request Endpoint'+req.getEndpoint());
            
            // Send and get response
   res = http.send(req);
            
   system.debug('Response Status Code: ' + res.getStatusCode());
   system.debug('Response Body: ' + res.getBody());
            
   if (stubPNI) {
    if (res.getStatusCode() == SF_LAPI_APIServiceUtils.HTTP_STATUS_200 || res.getStatusCode() == SF_LAPI_APIServiceUtils.HTTP_STATUS_201
            || res.getStatusCode() == SF_LAPI_APIServiceUtils.HTTP_STATUS_202 || res.getStatusCode() == SF_LAPI_APIServiceUtils.HTTP_STATUS_203
            || res.getStatusCode() == SF_LAPI_APIServiceUtils.HTTP_STATUS_204 || res.getStatusCode() == SF_LAPI_APIServiceUtils.HTTP_STATUS_205
            || res.getStatusCode() == SF_LAPI_APIServiceUtils.HTTP_STATUS_206) {
     response = res.getBody();
                        
     if (String.isNotEmpty(response)) {
                            // Update sfReq.Status to "Completed"
                            // Update sfReq.Response with data from response
                            sfReq = SF_LAPI_APIServiceUtils.getLocationByLocationIdSuccessPostProcessing(response, locationId, sfReq);
     } else {
      throw new CustomException(SF_LocationAPIService.ERR_RESP_BODY_MISSING);
     }
    } else {
                        // Update sfReq.Status to "Completed" or "In Error"
                        sfReq = SF_LAPI_APIServiceUtils.getLocationByLocationIdFailurePostProcessing(res.getStatusCode(), sfReq);
    }
   }
            // If not stubbing
   else {
    system.debug('**res.getStatusCode()**'+res.getStatusCode());
    if (res.getStatusCode() == SF_LAPI_APIServiceUtils.HTTP_STATUS_200) {
     response = res.getBody();
                    system.debug('responsebody'+response);
                    system.debug('**Else stubPNI **');
                    
     if (String.isNotEmpty(response)) {
                        // Update sfReq.Status to "Completed"
                        // Update sfReq.Response with data from response
                        sfReq = SF_LAPI_APIServiceUtils.getLocationByLocationIdSuccessPostProcessing(response, locationId, sfReq);
     } else {
      throw new CustomException(SF_LocationAPIService.ERR_RESP_BODY_MISSING);
     }
    } else {
                    // Update sfReq.Status to "Completed" or "In Error"
                    sfReq = SF_LAPI_APIServiceUtils.getLocationByLocationIdFailurePostProcessing(res.getStatusCode(), sfReq);
    }
   }
  } catch (Exception e) {
   SF_LAPI_APIServiceUtils.getLocationByLocationIdFailurePostProcessing(null, sfReq);
   GlobalUtility.logMessage(SF_LAPI_APIServiceUtils.ERROR, SF_LocationAPIService.class.getName(), 'getLocationByLocationId', '', '', '', '', e, 0);
            // Dont throw error so dml is executed/not rolled back
        }
        return sfReq;
  }
    
    
    public static DF_SF_Request__c getLocationByAddress(DF_SF_Request__c sfReq) {
        final String END_POINT;
        
        Http http = new Http();
        HttpRequest req;
        HTTPResponse res;
        
        String response;
        
        try {
            // Build address params for endpoint
            String inputAddressQueryFilter = SF_LAPI_APIServiceUtils.getInputAddressQueryFilter(sfReq);
            
            // Convert to urlEndcoded           
            String searchParams = SF_LAPI_APIServiceUtils.convertToURLEncodedStr(inputAddressQueryFilter);
            
            if (String.isEmpty(searchParams)) {
                throw new CustomException(SF_LocationAPIService.ERR_ADDR_API_ENDPOINT_PARAMS_EMPTY);
 }
            
            DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c
                                                         FROM   DF_Integration_Setting__mdt
                                                         WHERE  DeveloperName = 'Address_API'];
            
            END_POINT = SF_LAPI_APIServiceUtils.NAMED_CRED_PREFIX + integrSetting.Named_Credential__c + '?filter=' + searchParams;
            system.debug('END_POINT: ' + END_POINT);
            
            // Generate request 
            req = SF_LAPI_APIServiceUtils.getHttpRequest(END_POINT);
            
            // Send and get response               
            res = http.send(req);
            
            system.debug('Response Status Code: ' + res.getStatusCode());
            system.debug('Response Body: ' + res.getBody());
            
            if (res.getStatusCode() == SF_LAPI_APIServiceUtils.HTTP_STATUS_200) {
                // Includes:
                // - Multiple results returned
                // - 0 results returned             
                response = res.getBody();
                
                if (String.isNotEmpty(response)) {
                    // Update sfReq.Status to "Completed"
                    // Update sfReq.Response with data from response
                    
                    sfReq = SF_LAPI_APIServiceUtils.getLocationByAddressSuccessPostProcessing(response, sfReq);
                } else {
                    throw new CustomException(SF_LocationAPIService.ERR_RESP_BODY_MISSING);
                }
            } else {
                // Update sfReq.Status to "In Error"
                // Update sfReq.Response.Status to "Invalid"
                sfReq =SF_LAPI_APIServiceUtils.getLocationByAddressFailurePostProcessing(sfReq);
            }
        } catch (Exception e) {
            GlobalUtility.logMessage(SF_LAPI_APIServiceUtils.ERROR, SF_LocationAPIService.class.getName(), 'getLocationByAddress', '', '', '', '', e, 0);
            throw new CustomException(e.getMessage());
        }
        return sfReq;
    }
    
    public static DF_SF_Request__c getLocationByLatLong(DF_SF_Request__c sfReq) {
        String END_POINT;
        
        Http http = new Http();
        HttpRequest req;
        HTTPResponse res;
        
        String response;
        String latLongParams;
        String apiName;
        
        try {
            // Build endpoint      
            latLongParams = '?filter=latitude==' + sfReq.Latitude__c + ';longitude==' + sfReq.Longitude__c;
            system.debug('latLongParams: ' + latLongParams);
            
            if(sfReq.Product_Type__c == 'NBN_SELECT')
            {
        apiName = 'Location_API';
            }
            
            system.debug('**sfReq.Product_Type__c in getLocationByLatLong**: '+sfReq.Product_Type__c );
            
            DF_Integration_Setting__mdt integrSetting = [SELECT Named_Credential__c
                                                         FROM   DF_Integration_Setting__mdt
                                                         WHERE  DeveloperName = :apiName];
            
            END_POINT = SF_LAPI_APIServiceUtils.NAMED_CRED_PREFIX + integrSetting.Named_Credential__c + latLongParams;
            system.debug('END_POINT: ' + END_POINT);
            
            // Generate request 
            req = SF_LAPI_APIServiceUtils.getHttpRequest(END_POINT);
            
            // Send and get response               
            res = http.send(req);
            
            system.debug('Response Status Code: ' + res.getStatusCode());
            system.debug('Response Body: ' + res.getBody());
            
            if (res.getStatusCode() == SF_LAPI_APIServiceUtils.HTTP_STATUS_200) {
                // Response Status 200:
                //   Results would be one of the following outcomes:
                // - Results returned
                // - 0 results returned                           
                response = res.getBody();
                
                if (String.isNotEmpty(response)) {
                    // Update sfReq.Status to "Completed"
                    // Update sfReq.Response with data from response
                    sfReq = SF_LAPI_APIServiceUtils.getLocationByLatLongSuccessPostProcessing(response, sfReq);
                } else {
                    throw new CustomException(SF_LocationAPIService.ERR_RESP_BODY_MISSING);
                }
            } else {
                // Response Status 400 / 404 etc
                // Update sfReq.Status to "In Error"                
                // Update sfReq.Response.Status to "Invalid"
                sfReq = SF_LAPI_APIServiceUtils.getLocationByLatLongFailurePostProcessing(res.getStatusCode(), sfReq);
            }
        } catch (Exception e) {
            GlobalUtility.logMessage(SF_LAPI_APIServiceUtils.ERROR, SF_LocationAPIService.class.getName(), 'getLocationByLatLong', '', '', '', '', e, 0);
            throw new CustomException(e.getMessage());
        }
        return sfReq;
    }
}