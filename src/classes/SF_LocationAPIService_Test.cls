@isTest
private class SF_LocationAPIService_Test {

    @isTest static void test_getLocation_ByLocationId() {                   
        // Setup data       
        String locationId = 'LOC000035375038';
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, SF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);        
        insert sfReq;
 
        // Generate mock response
        response = SF_IntegrationTestData.buildLocationDistanceAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
 
        test.startTest();           
        
        SF_LocationAPIService.getLocation(sfReq);
                                        
        test.stopTest();                
                             
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id
                                            AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_COMPLETED];

        //system.Assert(sfReqList.size() > 0);
    }
    @isTest static void test_getLocationId_ByLocationId() {                   
        // Setup data       
        String locationId = 'LOC000035375038';
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, SF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);        
        insert sfReq;
 
        // Generate mock response
        response = SF_IntegrationTestData.buildLocationDistanceAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
 
        test.startTest();
        
        SF_LocationAPIService.getLocation(sfReq.Id);
                                        
        test.stopTest();                
                             
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id
                                            AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_COMPLETED];

        //system.Assert(sfReqList.size() > 0);
    }

    @isTest static void test_getLocation_ByAddress() {                      
        // Setup data       
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Address values
        String state = 'VIC';
        String postcode = '3000';
        String suburbLocality = 'Melbournje';
        String streetName = 'Jump';
        String streetType = 'ST';
        String streetLotNumber = '21';
        String unitType = 'UNIT';
        String unitNumber = '1';

        String level;
        String complexSiteName;
        String complexBuildingName;
        String complexStreetName;
        String complexStreetType;
        String complexStreetNumber;                     

        // Build address map
        Map<String, String> addressMap = new Map<String, String>();
        addressMap.put('state', state);
        addressMap.put('postcode', postcode);
        addressMap.put('suburbLocality', suburbLocality);
        addressMap.put('streetName', streetName);
        addressMap.put('streetType', streetType);
        addressMap.put('streetLotNumber', streetLotNumber);
        addressMap.put('unitType', unitType);
        addressMap.put('unitNumber', unitNumber);
        addressMap.put('level', level);
        addressMap.put('complexSiteName', complexSiteName);
        addressMap.put('complexBuildingName', complexBuildingName);
        addressMap.put('complexStreetName', complexStreetName);
        addressMap.put('complexStreetType', complexStreetType);
        addressMap.put('complexStreetNumber', complexStreetNumber);

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, SF_LAPI_APIServiceUtils.STATUS_PENDING, null, null, null, opptyBundle.Id, response, addressMap);      
        insert sfReq;
 
        // Generate mock response
        response = SF_IntegrationTestData.buildAddressAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
 
        test.startTest();           
        
        SF_LocationAPIService.getLocation(sfReq);
                                        
        test.stopTest();                                             
    }

    @isTest static void test_getLocation_ByLatLong() {                      
        // Setup data       
        String latitude = '-42.78931';
        String longitude = '147.05578'; 
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG, SF_LAPI_APIServiceUtils.STATUS_PENDING, null, latitude, longitude, opptyBundle.Id, response, addressMap);        
        sfReq.Product_Type__c ='NBN_SELECT';
        insert sfReq;
 
        system.debug(Logginglevel.ERROR, '---@sfReq--- '+sfReq);
        system.debug(Logginglevel.ERROR, '---@sfReq.Id--- '+sfReq.Id);
          
        // Generate mock response
        response = SF_IntegrationTestData.buildLocationAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
 
        test.startTest();    
        system.debug(Logginglevel.ERROR, '---@sfReq.Id--- '+sfReq.Id);
        try{
         SF_LocationAPIService.getLocation(sfReq); 
        }
        catch (exception ex)
        { 
        }                                       
        test.stopTest();                                             
        
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id
                                            AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_COMPLETED];

        //system.Assert(sfReqList.size() > 0);
    }

    @isTest static void test_getLocationByLocationId() {                    
        // Setup data       
        String locationId = 'LOC000035375038';
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, SF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);        
        insert sfReq;

        // Generate mock response
        response = SF_IntegrationTestData.buildLocationDistanceAPIRespSuccessful();
 
          // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
 
        test.startTest();           
        
        SF_LocationAPIService.getLocationByLocationId(locationId, sfReq);
                                        
        test.stopTest();                                             
        
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id
                                            AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_COMPLETED];

        //system.Assert(sfReqList.size() > 0);
    }
   
    @isTest static void test_getLocationByLocationId_unsuccessful_400() {                   
        // Setup data       
        String locationId = 'LOC000035375030';
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        /*Id oppBunId = SF_CaptureController.createOpptyBundle('NBN Select');
        List<DF_Opportunity_Bundle__c> lstOppBundle = Database.query(SF_CS_API_Util.getQuery(new DF_Opportunity_Bundle__c(), ' WHERE Id = :oppBunId'));
        DF_Opportunity_Bundle__c opptyBundle = lstOppBundle[0];*/
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, SF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);        
        insert sfReq;

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(400, '', null));
  
        test.startTest();           
        
        SF_LocationAPIService.getLocationByLocationId(locationId, sfReq);
                                        
        test.stopTest();                
        
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id];
                                            //AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_ERROR];

        system.Assert(sfReqList.size() > 0);
    }
    
    @isTest static void test_getLocationByLocationId_unsuccessful_404() {                   
        // Setup data       
        String locationId = 'LOC000035375038';
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, SF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);        
        insert sfReq;

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(404, '', null));
  
        test.startTest();           
        
        SF_LocationAPIService.getLocationByLocationId(locationId, sfReq);
                                        
        test.stopTest();                
        
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id
                                            AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_COMPLETED];

        //system.Assert(sfReqList.size() > 0);
    }             

    @isTest static void test_getLocationByLocationId_witherror_emptyResponseBody() {                    
        // Setup data               
        String locationId = 'LOC000035375038';
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, SF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);        
        insert sfReq;

        // Generate mock response
        response = '';
 
          // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
 
        test.startTest();           
        
        try {
            SF_LocationAPIService.getLocationByLocationId(locationId, sfReq);       
        } catch (Exception e) {
        }       
                                        
        test.stopTest();                                             
        
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id];
                                            //AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_ERROR];

        system.Assert(sfReqList.size() > 0);
    }

    @isTest static void test_getLocationByAddress_successful() {                    
        // Setup data       
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Address values
        String state = 'VIC';
        String postcode = '3000';
        String suburbLocality = 'Melbourne';
        String streetName = 'Jump';
        String streetType = 'ST';
        String streetLotNumber = '21';
        String unitType = 'UNIT';
        String unitNumber = '1';

        String level;
        String complexSiteName;
        String complexBuildingName;
        String complexStreetName;
        String complexStreetType;
        String complexStreetNumber;                     

        // Build address map
        Map<String, String> addressMap = new Map<String, String>();
        addressMap.put('state', state);
        addressMap.put('postcode', postcode);
        addressMap.put('suburbLocality', suburbLocality);
        addressMap.put('streetName', streetName);
        addressMap.put('streetType', streetType);
        addressMap.put('streetLotNumber', streetLotNumber);
        addressMap.put('unitType', unitType);
        addressMap.put('unitNumber', unitNumber);
        addressMap.put('level', level);
        addressMap.put('complexSiteName', complexSiteName);
        addressMap.put('complexBuildingName', complexBuildingName);
        addressMap.put('complexStreetName', complexStreetName);
        addressMap.put('complexStreetType', complexStreetType);
        addressMap.put('complexStreetNumber', complexStreetNumber);

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, SF_LAPI_APIServiceUtils.STATUS_PENDING, null, null, null, opptyBundle.Id, response, addressMap);      
        insert sfReq;
  
        // Generate mock response
        response = SF_IntegrationTestData.buildAddressAPIRespSuccessful();
  
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
  
        test.startTest();           
        
        SF_LocationAPIService.getLocationByAddress(sfReq);
                                        
        test.stopTest();                
    } 

    @isTest static void test_getLocationByAddress_witherror_emptyResponseBody() {                   
        // Setup data       
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Address values
        String state = 'VIC';
        String postcode = '3000';
        String suburbLocality = 'Melbourne';
        String streetName = 'Jump';
        String streetType = 'ST';
        String streetLotNumber = '21';
        String unitType = 'UNIT';
        String unitNumber = '1';

        String level;
        String complexSiteName;
        String complexBuildingName;
        String complexStreetName;
        String complexStreetType;
        String complexStreetNumber;                     

        // Build address map
        Map<String, String> addressMap = new Map<String, String>();
        addressMap.put('state', state);
        addressMap.put('postcode', postcode);
        addressMap.put('suburbLocality', suburbLocality);
        addressMap.put('streetName', streetName);
        addressMap.put('streetType', streetType);
        addressMap.put('streetLotNumber', streetLotNumber);
        addressMap.put('unitType', unitType);
        addressMap.put('unitNumber', unitNumber);
        addressMap.put('level', level);
        addressMap.put('complexSiteName', complexSiteName);
        addressMap.put('complexBuildingName', complexBuildingName);
        addressMap.put('complexStreetName', complexStreetName);
        addressMap.put('complexStreetType', complexStreetType);
        addressMap.put('complexStreetNumber', complexStreetNumber);

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, SF_LAPI_APIServiceUtils.STATUS_PENDING, null, null, null, opptyBundle.Id, response, addressMap);      
        insert sfReq;
  
        // Generate mock response
        response = '';
  
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
  
        test.startTest();           
        
        try {
            SF_LocationAPIService.getLocationByAddress(sfReq);
        } catch (Exception e) {
        }           
                                        
        test.stopTest();                
    } 

    @isTest static void test_getLocationByAddress_witherror_emptyAddressEndpointParameters() {                      
        // Setup data       
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Address values
        String state = '';
        String postcode = '';
        String suburbLocality = '';
        String streetName = '';
        String streetType = '';
        String streetLotNumber = '';
        String unitType = '';
        String unitNumber = '';

        String level;
        String complexSiteName;
        String complexBuildingName;
        String complexStreetName;
        String complexStreetType;
        String complexStreetNumber;                     

        // Build address map
        Map<String, String> addressMap = new Map<String, String>();
        addressMap.put('state', state);
        addressMap.put('postcode', postcode);
        addressMap.put('suburbLocality', suburbLocality);
        addressMap.put('streetName', streetName);
        addressMap.put('streetType', streetType);
        addressMap.put('streetLotNumber', streetLotNumber);
        addressMap.put('unitType', unitType);
        addressMap.put('unitNumber', unitNumber);
        addressMap.put('level', level);
        addressMap.put('complexSiteName', complexSiteName);
        addressMap.put('complexBuildingName', complexBuildingName);
        addressMap.put('complexStreetName', complexStreetName);
        addressMap.put('complexStreetType', complexStreetType);
        addressMap.put('complexStreetNumber', complexStreetNumber);

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, SF_LAPI_APIServiceUtils.STATUS_PENDING, null, null, null, opptyBundle.Id, response, addressMap);      
        insert sfReq;
  
        // Generate mock response
        response = '';
  
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
  
        test.startTest();           
        
        try {
            SF_LocationAPIService.getLocationByAddress(sfReq);
        } catch (Exception e) {
        }           
                                        
        test.stopTest();                
    } 

    @isTest static void test_getLocationByAddress_unsuccessful() {                      
        // Setup data       
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        // Address values
        String state = 'VIC';
        String postcode = '3000';
        String suburbLocality = 'Melbourne';
        String streetName = 'Jump';
        String streetType = 'ST';
        String streetLotNumber = '21';
        String unitType = 'UNIT';
        String unitNumber = '1';

        String level;
        String complexSiteName;
        String complexBuildingName;
        String complexStreetName;
        String complexStreetType;
        String complexStreetNumber;                     

        // Build address map
        Map<String, String> addressMap = new Map<String, String>();
        addressMap.put('state', state);
        addressMap.put('postcode', postcode);
        addressMap.put('suburbLocality', suburbLocality);
        addressMap.put('streetName', streetName);
        addressMap.put('streetType', streetType);
        addressMap.put('streetLotNumber', streetLotNumber);
        addressMap.put('unitType', unitType);
        addressMap.put('unitNumber', unitNumber);
        addressMap.put('level', level);
        addressMap.put('complexSiteName', complexSiteName);
        addressMap.put('complexBuildingName', complexBuildingName);
        addressMap.put('complexStreetName', complexStreetName);
        addressMap.put('complexStreetType', complexStreetType);
        addressMap.put('complexStreetNumber', complexStreetNumber);

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_ADDRESS, SF_LAPI_APIServiceUtils.STATUS_PENDING, null, null, null, opptyBundle.Id, response, addressMap);      
        insert sfReq;

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(400, '', null));
  
        test.startTest();           
        
        SF_LocationAPIService.getLocationByAddress(sfReq);
                                        
        test.stopTest();                
    }

    @isTest static void test_getLocationByLatLong_successful() {                    
        // Setup data       
        String latitude = '-15.451568';
        String longitude = '145.178216';
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG, SF_LAPI_APIServiceUtils.STATUS_PENDING, null, latitude, longitude, opptyBundle.Id, response, addressMap);        
        insert sfReq;

        // Generate mock response
        response = SF_IntegrationTestData.buildLocationAPIRespSuccessful();

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
  
        test.startTest();           
        try{
         SF_LocationAPIService.getLocationByLatLong(sfReq);   
        }
        catch(exception e){
            
        }
                                        
        test.stopTest();                
        
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id
                                            AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_COMPLETED];

        //system.Assert(sfReqList.size() > 0);
    }

    @isTest static void test_getLocationByLatLong_witherror_emptyResponseBody() {                   
        // Setup data       
        String latitude = '-15.451568';
        String longitude = '145.178216';
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG, SF_LAPI_APIServiceUtils.STATUS_PENDING, null, latitude, longitude, opptyBundle.Id, response, addressMap);        
        insert sfReq;

        // Generate mock response
        response = '';

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
  
        test.startTest();           
                
        try {
            SF_LocationAPIService.getLocationByLatLong(sfReq);
        } catch (Exception e) {
        }       
                                        
        test.stopTest();                
    }
    
    @isTest static void test_getLocationByLatLong_unsuccessful() {                      
        // Setup data       
        String latitude = '-15.451568';
        String longitude = '145.178216';
        String response;

        // Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        // Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        // Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LAT_LONG, SF_LAPI_APIServiceUtils.STATUS_PENDING, null, latitude, longitude, opptyBundle.Id, response, addressMap);        
     
        insert sfReq;

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(400, '', null));
  
        test.startTest();           
        try {
        SF_LocationAPIService.getLocationByLatLong(sfReq);    
        }
        catch(Exception ex){
            
        }                              
        test.stopTest();                
        
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id
                                            AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_ERROR];

        //system.Assert(sfReqList.size() > 0);
    }    
}