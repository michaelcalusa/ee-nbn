@isTest
public class SF_OrderSummaryData_Test {

    @isTest
    public static void testOrderSummaryData(){
        SF_OrderSummaryData osDataItem = new SF_OrderSummaryData('DFQuoteId1', 'LOC0000000001234', 
                                                                 'Test Name', 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia', 
                                                                 232.00, 30.95, 232.00, SF_Constants.RAG_STATUS_RED);
        System.assertEquals('DFQuoteId1', osDataItem.id);
        System.assertEquals('LOC0000000001234', osDataItem.locId);
        System.assertEquals('Test Name', osDataItem.quoteId);
        System.assertEquals('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia', osDataItem.address);
        System.assertEquals(232.00, osDataItem.FBC);
        System.assertEquals(30.95, osDataItem.SCNR);
        System.assertEquals(232.00, osDataItem.SCR);
        System.assertEquals(SF_Constants.RAG_STATUS_RED, osDataItem.status);
    }
}