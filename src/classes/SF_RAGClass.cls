/**
* Created by Gobind.Khurana on 22/05/2018.
*/

public class SF_RAGClass {
    // Wrapper class to map values to the service feasibility screen
    @AuraEnabled
    public String SQId {get;set;}
    @AuraEnabled
    public String LocId {get;set;}
    @AuraEnabled
    public String Address {get;set;}
    @AuraEnabled
    public String RAG {get;set;}
    @AuraEnabled
    public Decimal FBC {get;set;}
    @AuraEnabled
    public String SFStatus {get;set;}
    @AuraEnabled
    public String oppBundleName {get;set;}
    @AuraEnabled
    public String quoteType {get;set;}
    @AuraEnabled
    public String Latitude {get;set;}
    @AuraEnabled
    public String Longitude {get;set;}
    @AuraEnabled
    public String EBT {get;set;}
    
    public SF_RAGClass(String sq, String loc, String add, String ragVal, Decimal cost) {
        SQId = sq;
        LocId = loc;
        Address = add;
        RAG = ragVal;
        FBC = cost;
    }
    
    public SF_RAGClass(String sq, String loc, String add, String ragVal, Decimal cost, String status) {
        SQId = sq;
        LocId = loc;
        Address = add;
        RAG = ragVal;
        FBC = cost;
        SFStatus = status;
    }
    
}