/**
* Created by Gobind.Khurana on 22/05/2018.
*/

public class SF_SendLocationRequestQueueHandler implements Queueable, Database.AllowsCallouts {
    /* 
        Salesforce Limits: 
        -50 future calls per batch transaction
        -100 callouts per transaction
        
        QueueHandler Limits:
        -Max 50 invocations (of getLocation future method)
         
        This equates to:
        -> 50 future calls (per transaction)
        -> Max 2 callouts (per transaction) - 1 Address API callout / 1 Location API callout
        
        -This SendLocationRequestQueueHandler will fire off max 50 SFReqs (future calls) in 1 txn (1 iteration of the Queuehandler)
         If there are more than 50 SFReqs to be sent, then this job (txn) will spawn again for the next 50 SFReqs and so on 
         till all pending SFReqs have been processed for an Opportunity Bundle.
    */
    
    public static final Integer MAX_NO_PARALLELS = 50; // Max number of future calls to be made in 1 txn
    private String opportunityBundleId;
    
    // Constructor
    public SF_SendLocationRequestQueueHandler(String opptyBundleId) {
        system.debug('opptyBundleId: ' + opptyBundleId);
        this.opportunityBundleId = opptyBundleId;
    }

    public void execute(QueueableContext context) {
        Id existingJobId;
        List<DF_SF_Request__c> sfReqIdList;

        try {
            // JobId is the AsyncApexJob Id
            existingJobId = context.getJobId();
            system.debug('existingJobId: ' + existingJobId);

            // Get recs from SFReq table for only the one opptyBundleId
            sfReqIdList = getSFReqIdList(opportunityBundleId);
            system.debug('sfReqIdList size: ' + sfReqIdList.size());
            if (!sfReqIdList.isEmpty()) {
                makeCallout(sfReqIdList);
            }

            // if needed more than 50 SFRequests in 1 txn (50 seperate callouts) - re-invoke queueable job
            // check if anything else to run
            if (isAnythingPendingToProcess(opportunityBundleId)) {
                enqueueMoreLAPI(opportunityBundleId);
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
    }


    @TestVisible private void makeCallout( List<DF_SF_Request__c> lstSfrToLapi) {
        if (!Test.isRunningTest()) {
			System.enqueueJob(new SF_LocationAPICalloutQueueHandler(this.opportunityBundleId, lstSfrToLapi));
        }
    }

    @future
    private static void enqueueMoreLAPI(String opptyBundleId){
        if (!Test.isRunningTest()) {
            Id jobId = System.enqueueJob(new SF_SendLocationRequestQueueHandler(opptyBundleId));

            if (jobId != null) {
                system.debug('SF_SendLocationRequestQueueHandler queueable job started - jobId: ' + jobId);
            }
        }
    }

    @TestVisible private static List<DF_SF_Request__c> getSFReqIdList(String opptyBundleId) {
        Map<String, DF_SF_Request__c> sfReqMap = new Map<String, DF_SF_Request__c>();
        List<String> sfReqIdList= new List<String>();
        List<DF_SF_Request__c> sfReqUpdateList;

        try {
            sfReqMap = new Map<String, DF_SF_Request__c>([SELECT Id, Name,
                                                          		  Location_Id__c,
                                                                  Latitude__c,
                                                                  Longitude__c,
                                                                  State__c,
                                                                  Postcode__c,
                                                                  Suburb_Locality__c,
                                                                  Street_Name__c,
                                                                  Street_Type__c,
                                                                  Street_or_Lot_Number__c,
                                                                  Unit_Type__c,
                                                                  Unit_Number__c,
                                                                  Level__c,
                                                                  Complex_or_Site_Name__c,
                                                                  Building_Name_in_a_Complex__c,
                                                                  Street_Name_in_a_Complex__c,
                                                                  Street_Type_in_a_Complex__c,
                                                                  Street_Number_in_a_Complex__c,
                                                                  Address__c,
                                                                  Status__c,
                                                                  Search_Type__c,
                                                                  Product_Type__c,
                                                                  Search_Source__c,
                                                        		  Response__c,
                                                          		  Opportunity_Bundle__c
            FROM     DF_SF_Request__c
            WHERE    Opportunity_Bundle__c = :opptyBundleId
            AND      Status__c = :SF_LAPI_APIServiceUtils.STATUS_PENDING
            ORDER BY Id
            LIMIT :SF_SendLocationRequestQueueHandler.MAX_NO_PARALLELS]);
            
            system.debug('sfReqMap size: ' + sfReqMap.size());
            
            // Returns ids from keyset
            if (!sfReqMap.keySet().isEmpty()) {
                // Create new list from a set
                sfReqIdList = new List<String>(sfReqMap.keySet());
            }
            
            // Get sfReq recs to update
            sfReqUpdateList = new List<DF_SF_Request__c>(sfReqMap.values());
            system.debug('sfReqUpdateList size: ' + sfReqUpdateList.size());
            
            if (!sfReqUpdateList.isEmpty()) {
                // Loop through and set each rec to Status = "Processed"
                for (DF_SF_Request__c sfReq : sfReqUpdateList) {
                    sfReq.Status__c = SF_LAPI_APIServiceUtils.STATUS_PROCESSED;
                }
                update sfReqUpdateList;
            }
            
            system.debug('sfReqIdList size: ' + sfReqIdList.size());
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
        
        return sfReqUpdateList;
    }
    
    @TestVisible private static Boolean isAnythingPendingToProcess(String opptyBundleId) {
        List<DF_SF_Request__c> sfReqIdList;
        Boolean anythingPendingToProcess = false;
        
        try {
            sfReqIdList = [SELECT Id
            FROM   DF_SF_Request__c
            WHERE  Opportunity_Bundle__c = :opptyBundleId
            AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_PENDING LIMIT 1];
            
            system.debug('sfReqIdList.size: ' + sfReqIdList.size());
            
            if (!sfReqIdList.isEmpty()) {
                anythingPendingToProcess = true;
            }
            
            system.debug('anythingPendingToProcess: ' + anythingPendingToProcess);
        } catch (Exception e) {
            throw new CustomException(e.getMessage());
        }
        
        return anythingPendingToProcess;
    }
}