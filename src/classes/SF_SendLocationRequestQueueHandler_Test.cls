@isTest
public class SF_SendLocationRequestQueueHandler_Test {
	@isTest
    static void test_LocationREquestQueue() {                   
        //Setup data
        String locationId = 'LOC000035375038';
        String response;

        //Create acct
        Account acct = SF_TestData.createAccount('Test Account');
        insert acct;
        
        //Create Oppty Bundle
        DF_Opportunity_Bundle__c opptyBundle = SF_TestData.createOpportunityBundle(acct.Id);
        insert opptyBundle;

        Map<String, String> addressMap = new Map<String, String>();

        //Create sfreq
        DF_SF_Request__c sfReq = SF_TestData.createSFRequest(SF_LAPI_APIServiceUtils.SEARCH_TYPE_LOCATION_ID, SF_LAPI_APIServiceUtils.STATUS_PENDING, locationId, null, null, opptyBundle.Id, response, addressMap);        
        insert sfReq;
 
        // Generate mock response
        response = SF_IntegrationTestData.buildLocationDistanceAPIRespSuccessful();
 
         // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200, response, null));
 
        test.startTest();
        	System.enqueueJob(new SF_SendLocationRequestQueueHandler(String.valueOf(opptyBundle.Id)));
            SF_SendLocationRequestQueueHandler.isAnythingPendingToProcess(String.valueOf(opptyBundle.Id));
        test.stopTest();
        // Assertions
        List<DF_SF_Request__c> sfReqList = [SELECT Id 
                                            FROM   DF_SF_Request__c 
                                            WHERE  Opportunity_Bundle__c = :opptyBundle.Id];
                                            //AND    Status__c = :SF_LAPI_APIServiceUtils.STATUS_COMPLETED];
        system.Assert(sfReqList.size() > 0);
    }
}