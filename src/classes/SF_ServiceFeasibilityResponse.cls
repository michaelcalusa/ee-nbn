/**
 * Created by Gobind.Khurana on 22/05/2018.
 */

public class SF_ServiceFeasibilityResponse {

    //class to map response from service feasibility response
    public List<String> AddressList;
    public List<Map<String, String>> structuredAddressList {get;set;}
    public String Latitude {get;set;}
    public String Longitude {get;set;}
    public String LocId {get;set;}
    public Integer Distance {get;set;}
    public String AccessTechnology {get;set;}
    public String serviceType {get;set;}
    public String Status {get;set;}
    public String RAG {get;set;}
    public String fibreJointId {get;set;}
    public String fibreJointStatus {get;set;}
    public String fibreJointLatLong {get;set;}
    public String fibreJointTypeCode {get;set;}
    public String fibreJointTypeDescription {get;set;}
    public String locLatLong {get;set;}
    public String primaryTechnology {get;set;}
    public String derivedTechnology {get;set;}
    public String fsaID {get;set;}
    public String samID {get;set;}
    public String csaId {get;set;}
    public Boolean isNoFibreJointFound {get;set;}
    /*PNI Details begin*/
    public String id{get;set;}
    public String systemId {get;set;}
    public String assetLat {get;set;}
    public String assetLong {get;set;}
    public String assetOwner {get;set;}
    public String OLT_Exists {get;set;}
    public String OLT_ID {get;set;}
    public String pniException{get;set;}
    public Double whiteListQuoteAmount{get;set;}
    public Double whiteListEBTInDays{get;set;}
	    
    /*public String assetStatus {get;set;}
    public String assetId {get;set;}
    public String assetDistance {get;set;}
    public String assetTypeCode {get;set;}*/

    /*PNI Details end*/
    public String serviceClass{get;set;}

    /* Correlate to original DF_SF_Request__c object */
    public String sfRequestId {get;set;}  //DF_SF_Request__c.Id
    public String sfSearchType {get;set;} //DF_SF_Request__c.Search_Type__c

    public SF_ServiceFeasibilityResponse(){}

    public SF_ServiceFeasibilityResponse(String locId, String latitude, String longitude, List<String> addressList, Integer distance, String technology, String status, String rag,
            String fibreJointId, String fibreJointStatus, String fibreJointLatLong, String fibreJointTypeCode, String fibreJointTypeDescription, String locLatLong) {

        this.AddressList = new List<String>(addressList);
        this.structuredAddressList = new List<Map<String, String>>();
        this.Latitude = latitude;
        this.Longitude = longitude;
        this.LocId = locId;
        this.Distance = distance;
        this.AccessTechnology = technology;
        this.Status = status;
        this.RAG = rag;
        this.fibreJointId = fibreJointId;
        this.fibreJointStatus = fibreJointStatus;
        this.fibreJointLatLong = fibreJointLatLong;
        this.fibreJointTypeCode = fibreJointTypeCode;
        this.fibreJointTypeDescription = fibreJointTypeDescription;
        this.locLatLong = locLatLong;

    }
    
    /*public SF_ServiceFeasibilityResponse(String locId, String latitude, String longitude, List<String> addressList, Integer distance, String technology, String status, String rag,
            String fibreJointId, String fibreJointStatus, String fibreJointLatLong, String fibreJointTypeCode, String fibreJointTypeDescription, String locLatLong, String systemId, String assetStatus, String assetId,
            String assetOwner, String assetDistance, String assetLat, String assetLong, String oLT_Exists, String assetTypeCode, String oLT_ID, String pniException) {

        this.AddressList = new List<String>(addressList);
        this.Latitude = latitude;
        this.Longitude = longitude;
        this.LocId = locId;
        this.Distance = distance;
        this.AccessTechnology = technology;
        this.Status = status;
        this.RAG = rag;
        this.fibreJointId = fibreJointId;
        this.fibreJointStatus = fibreJointStatus;
        this.fibreJointLatLong = fibreJointLatLong;
        this.fibreJointTypeCode = fibreJointTypeCode;
        this.fibreJointTypeDescription = fibreJointTypeDescription;
        this.locLatLong = locLatLong;
        
        this.systemId = systemId;
        this.assetStatus = assetStatus;
        this.assetId = assetId;
        this.assetOwner = assetOwner;
        this.assetDistance = assetDistance;
        this.assetLat = assetLat;
        this.assetLong = assetLong;
        this.oLT_Exists = oLT_Exists;
        this.assetTypeCode = assetTypeCode;
        this.oLT_ID = oLT_ID;
        this.pniException = pniException;
    }*/
}