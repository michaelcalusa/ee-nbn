/**
 * Created by Gobind.Khurana on 22/05/2018.
 */

public class SF_SiteData {

    //wrapper class to map values in the location validation screen
    @AuraEnabled
    public String locId {get;set;}
    @AuraEnabled
    public String latitude {get;set;}
    @AuraEnabled
    public String longitude {get;set;}
    @AuraEnabled
    public String address {get;set;}
    @AuraEnabled
    public String status {get;set;}
    @AuraEnabled
    public Boolean addCount {get;set;}
    @AuraEnabled
    public String oppBundleName {get;set;}
    @AuraEnabled
    public String searchType {get;set;}
    @AuraEnabled
    public String requestId {get;set;}

    public SF_SiteData(String locationId,String lat, String longit, String add, String stat, Boolean flag)
    {
        locId = locationId;
        latitude = lat;
        longitude = longit;
        address = add;
        status = stat;
        addCount = flag;
    }
}