/**
* Created by Gobind.Khurana on 22/05/2018.
*/

@IsTest
public class SF_TestData {
    
    public static string IDENTIFICATION ='IDE1322';


    // Create Account
    public Static Account createAccount(String name) {
        Account a = new Account();
        a.Name = name;
        
        return a;
    }
    
    // Retrieve Account
    public Static Account getAccount(Id accountId){
        List<Account> accounts = [SELECT Id, Name FROM Account WHERE Id = :accountId];
        if(!accounts.isEmpty()){
            return accounts[0];
        } else {
            return null;
        }
        
    }
    
    //Create empty basket
    public static cscfga__Product_Basket__c buildEmptyBasket() {
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        return basket;
    }
    
    //Create basket
    public static cscfga__Product_Basket__c buildBasket() {
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.cscfga__Basket_Status__c = 'Valid';
        basket.csbb__Synchronised_with_Opportunity__c = false;
        basket.Name = 'TestBasket';
        cscfga__User_Session__c s = buildUserSession();
        insert s;
        basket.cscfga__User_Session__c = s.Id;
        return basket;
    }
    
    //Create user session
    public static cscfga__User_Session__c buildUserSession() {
        cscfga__User_Session__c session = new cscfga__User_Session__c();
        session.Name = 'User Session';
        session.cscfga__PC_Return_URL__c = '/';
        return session;
    }
    
    //Create product definition
    public static cscfga__Product_Definition__c buildProductDefinition(String productName, String planName) {
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.Name = productName;
        pd.cscfga__Description__c = productName;
        return pd;
    }
    
    //attribute
    public static cscfga__Attribute__c buildAttribute(String name, String value, Id productConfigId) {
        cscfga__Attribute__c att = new cscfga__Attribute__c();
        att.name = name;
        att.cscfga__Value__c = value;
        att.cscfga__Product_Configuration__c = productConfigId;
        return att;
    }
    
    //create product configuration
    public static cscfga__Product_Configuration__c buildProductConfig(Id productDefId) {
        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c();
        config.name = 'test config';
        config.cscfga__Product_Definition__c = productDefId;
        return config;
    }
    
    //create product category
    public static cscfga__Product_Category__c   buildProductCategory() {
        cscfga__Product_Category__c   pc =  new cscfga__Product_Category__c  ();
        pc.name = 'Business';
        pc.cscfga__Active__c  = true;
        pc.cscfga__Browseable__c  = true;
        return pc;
    }
    
    //create offer
    public static cscfga__Configuration_Offer__c createOffer(String name) {
        cscfga__Configuration_Offer__c  offer =  new cscfga__Configuration_Offer__c ();
        offer.cscfga__Active__c = true;
        offer.Name = name;
        return offer;
    }
    
    //create callout results
    public static csbb__Callout_Result__c  buildCallOutResults() {
        csbb__Callout_Result__c coResults = new csbb__Callout_Result__c ();
        coResults.csbb__SOAP_Request_Message__c = '<?xml version="1.0" encoding="UTF-8"?><env:Envelope><env:Body><sqCheck xmlns="http://soap.sforce.com/schemas/class/SQAvailabilityCheck"><addressId>112233</addressId></sqCheck ></env:Body></env:Envelope>';
        return coResults;
    }
    
    //Create callout product results
    public static csbb__Callout_Product_Result__c  buildCalloutProductResults(csbb__Product_Configuration_Request__c  pcr, csbb__Callout_Result__c cor) {
        csbb__Callout_Product_Result__c  productRes = new csbb__Callout_Product_Result__c ();
        productRes.csbb__Product_Configuration_Request__c = pcr.id;
        productRes.csbb__Callout_Result__c = cor.id;
        return productRes;
    }
    
    //create product definition
    public static cscfga__Attribute_Definition__c buildAttributeDefinition(String name, Id productDefinitionId) {
        cscfga__Attribute_Definition__c attDef = new cscfga__Attribute_Definition__c();
        attDef.name = name;
        attDef.cscfga__Label__c = name;
        attDef.cscfga__Product_Definition__c = productDefinitionId;
        return attDef;
    }
    
    //create product definition
    public static cscfga__Attribute_Definition__c buildAttributeDefinitionQQ(String name, Id productDefinitionId, String dt) {
        cscfga__Attribute_Definition__c attDef = new cscfga__Attribute_Definition__c();
        attDef.name = name;
        attDef.cscfga__Label__c = name;
        attDef.cscfga__Product_Definition__c = productDefinitionId;
        attDef.cscfga__Is_Line_Item__c = true;
        attDef.cscfga__Data_Type__c = dt;
        attDef.cscfga__Enable_null_option__c = true;
        attDef.csordtelcoa__Calculate_Delta__c = true;
        return attDef;
    }
    
    //create product definition
    public static cscfga__Select_Option__c buildSelectOptions(String name, cscfga__Attribute_Definition__c ad) {
        cscfga__Select_Option__c so = new cscfga__Select_Option__c();
        so.name = name;
        so.cscfga__Value__c = name;
        so.cscfga__Attribute_Definition__c = ad.Id;
        return so;
    }
    
    //create product definition
    public static cscfga__Attribute_Definition__c buildAttributeDefinitionSelectOptions(String name, Id productDefinitionId, String dt) {
        cscfga__Attribute_Definition__c attDef = new cscfga__Attribute_Definition__c();
        attDef.name = name;
        attDef.cscfga__Label__c = name;
        attDef.cscfga__Product_Definition__c = productDefinitionId;
        attDef.cscfga__Is_Line_Item__c = true;
        attDef.cscfga__Data_Type__c = dt;
        attDef.cscfga__Type__c = 'Select List';
        attDef.cscfga__Enable_null_option__c = true;
        attDef.csordtelcoa__Calculate_Delta__c = true;
        return attDef;
    }
    
    //create attribute field definition
    public static cscfga__Attribute_Field_Definition__c buildAttributeFieldDefinition(String name, Id attrDefinitionId,String defVal) {
        cscfga__Attribute_Field_Definition__c attFieldDef = new cscfga__Attribute_Field_Definition__c();
        attFieldDef.name = name;
        attFieldDef.cscfga__Default_Value__c = defVal;
        attFieldDef.cscfga__Attribute_Definition__c = attrDefinitionId;
        return attFieldDef;
    }
    
    //create price attribute
    public static cscfga__Attribute__c buildPriceAttribute(String name, String value, Id productConfigId, Boolean isRC, Decimal price) {
        cscfga__Attribute__c att = new cscfga__Attribute__c();
        att.name = name;
        att.cscfga__Value__c = value;
        att.cscfga__Product_Configuration__c = productConfigId;
        att.cscfga__Is_Line_Item__c = isRC;
        att.cscfga__Price__c = price;
        return att;
    }
    
    //create screen flow
    public static cscfga__Screen_Flow__c  buildScreenFlow(String sfName) {
        cscfga__Screen_Flow__c sf = new cscfga__Screen_Flow__c(name = sfName);
        sf.cscfga__Template_Reference__c = 'Test screen flow Ref';
        return sf;
    }
    
    //create screen flow product association
    public static cscfga__Screen_Flow_Product_Association__c buildSFAssociation(Id prodDefId, Id sfId) {
        cscfga__Screen_Flow_Product_Association__c sfA = new cscfga__Screen_Flow_Product_Association__c();
        sfA.cscfga__Product_Definition__c = prodDefId;
        sfA.cscfga__Screen_Flow__c = sfId;
        return sfA;
    }
    
    //create opportunity
    public static Opportunity createOpportunity(String name){
        NS_Custom_Options__c cs = new NS_Custom_Options__c(name = 'SF_RECORDTYPE_DEVELOPER_NAME', Value__c = 'NBN_Select');
        insert cs;
        Opportunity opp = new Opportunity();
        opp.Name = name;
        opp.StageName = 'New';
        opp.CloseDate = System.today();
        opp.RecordTypeId = SF_CS_API_Util.getDFRecordType();
        return opp;
    }
    
    //create opportunity bundle record
    public static DF_Opportunity_Bundle__c createOpportunityBundle(Id acct){
        DF_Opportunity_Bundle__c opp = new DF_Opportunity_Bundle__c();
        opp.Account__c = acct;
        opp.RecordTypeId = SF_CS_API_Util.getRecordType('NBN Select', 'DF_Opportunity_Bundle__c');
        return opp;
    }

    //create rules in the Fibre build category rule object
    public static DF_FBC_Rule__c createRagRuleTable (String tech, Decimal min, Decimal max, String val){
        DF_FBC_Rule__c fbc = new DF_FBC_Rule__c();
        fbc.Access_Technology__c = tech;
        fbc.Minimum_Distance__c = min;
        fbc.Maximum_Distance__c = max;
        fbc.RAG_Status__c = val;
        fbc.RecordTypeId = SF_CS_API_Util.getRecordType('NBN Select', 'DF_FBC_Rule__c');
        return fbc;
    }

    //create service feasibility request records
    public static DF_SF_Request__c createSFRequest(String add, String lat, String longit, String locId, Id oppId, String resp){
        DF_SF_Request__c sfr = new DF_SF_Request__c();
        sfr.Address__c = add;
        sfr.Latitude__c = lat;
        sfr.Longitude__c = longit;
        sfr.Location_Id__c = locId;
        sfr.Opportunity_Bundle__c = oppId;
        sfr.Response__c = resp;
        return sfr;
    }

    //create service feasibility request records (version 2)
    public static DF_SF_Request__c createSFRequest(String searchType, String status, String locId, String latitude, String longitude, Id opptyBundleId, String response, Map<String, String> addressMap) {
        DF_SF_Request__c sfReq = new DF_SF_Request__c();
        sfReq.Search_Type__c = searchType;
        sfReq.Status__c = status;
        sfReq.Location_Id__c = locId;
        sfReq.Latitude__c = latitude;
        sfReq.Longitude__c = longitude;
        sfReq.Opportunity_Bundle__c = opptyBundleId;
        sfReq.Response__c = response;

        if (!addressMap.isEmpty()) {
            sfReq.State__c = addressMap.get('state');
            sfReq.Postcode__c = addressMap.get('postcode');
            sfReq.Suburb_Locality__c = addressMap.get('suburbLocality');
            sfReq.Street_Name__c = addressMap.get('streetName');
            sfReq.Street_Type__c = addressMap.get('streetType');
            sfReq.Street_or_Lot_Number__c = addressMap.get('streetLotNumber');
            sfReq.Unit_Type__c = addressMap.get('unitType');
            sfReq.Unit_Number__c = addressMap.get('unitNumber');
            sfReq.Level__c = addressMap.get('level');
            sfReq.Complex_or_Site_Name__c = addressMap.get('complexSiteName');
            sfReq.Building_Name_in_a_Complex__c = addressMap.get('complexBuildingName');
            sfReq.Street_Name_in_a_Complex__c = addressMap.get('complexStreetName');
            sfReq.Street_Type_in_a_Complex__c = addressMap.get('complexStreetType');
            sfReq.Street_Number_in_a_Complex__c = addressMap.get('complexStreetNumber');
        }

        return sfReq;
    }

    //create df quote records
    public static DF_Quote__c createDFQuote(String add, String lat, String longit, String locId, Id oppId, Id parentOppId, Decimal cost, String rag){
        DF_Quote__c q = new DF_Quote__c();
        q.Address__c = add;
        q.Latitude__c = lat;
        q.Longitude__c = longit;
        q.Location_Id__c = locId;
        q.Opportunity__c = oppId;
        q.Opportunity_Bundle__c = parentOppId;
        q.Fibre_Build_Cost__c = cost;
        q.RAG__c = rag;
        q.Order_GUID__c =  NS_SF_Utils.generateGUID();
        q.GUID__c =  NS_SF_Utils.generateGUID();
        q.recordTypeId = SF_CS_API_Util.getRecordType('NBN Select', 'DF_Quote__c');
        return q;
    }

    //create df quote records
    public static DF_Quote__c createDFQuote(String add, String lat, String longit, String locId, Id oppId, Id parentOppId, Decimal cost, String rag, String response){
        DF_Quote__c q = new DF_Quote__c();
        q.Address__c = add;
        q.Latitude__c = lat;
        q.Longitude__c = longit;
        q.Location_Id__c = locId;
        q.Opportunity__c = oppId;
        q.Opportunity_Bundle__c = parentOppId;
        q.Fibre_Build_Cost__c = cost;
        q.RAG__c = rag;
        q.LAPI_Response__c = response;
        q.Order_GUID__c =  NS_SF_Utils.generateGUID();
        q.GUID__c =  NS_SF_Utils.generateGUID();
        q.recordTypeId = SF_CS_API_Util.getRecordType('NBN Select','DF_Quote__c');
        return q;
    }

    // Create DF Order record
    public static DF_Order__c createDFOrder(Id dfQuoteId, Id opptyBundleId, String orderStatus) {
        DF_Order__c dfOrder = new DF_Order__c();

        dfOrder.DF_Quote__c = dfQuoteId;
        dfOrder.Opportunity_Bundle__c = opptyBundleId;
        dfOrder.Order_Status__c = orderStatus;
        dfOrder.recordTypeId = SF_CS_API_Util.getRecordType('NBN Select', 'DF_Order__c');
        return dfOrder;
    }

    //creates values for custom settings
    public static List<NS_Custom_Options__c> createCustomSettings(){
        List<NS_Custom_Options__c> csList = new List<NS_Custom_Options__c>();
        NS_Custom_Options__c cs1 = new NS_Custom_Options__c(name = 'SF_RECORDTYPE_DEVELOPER_NAME', Value__c = 'NBN_Select');
        NS_Custom_Options__c cs2 = new NS_Custom_Options__c(name = 'SF_SITESTATUS', Value__c = 'Valid');
        NS_Custom_Options__c cs3 = new NS_Custom_Options__c(name = 'SF_STAGENAME', Value__c = 'New');
        NS_Custom_Options__c cs4 = new NS_Custom_Options__c(name = 'SF_UNAVAILABLE', Value__c = 'Unavailable');
        NS_Custom_Options__c cs5 = new NS_Custom_Options__c(name = 'BULK_UPLOAD_TEMPLATE', Value__c = 'https://nbn--DFDEVSB.cs72.my.salesforce.com/sfc/p/5D0000004iSY/a/5D00000000Wb/iz3b9Q5G_iNMUNsLnKuftIKJUk.RmIvGCJxNPYabapc');
        NS_Custom_Options__c cs6 = new NS_Custom_Options__c(name = 'PRODUCT_BASKET_PARALLEL_QUEUE_SIZE', Value__c = '1');
        NS_Custom_Options__c cs7 = new NS_Custom_Options__c(name = 'SF_AUTOQUOTERAGS', Value__c = 'Green');
        csList.add(cs1);
        csList.add(cs2);
        csList.add(cs3);
        csList.add(cs4);
        csList.add(cs5);
        csList.add(cs6);
        csList.add(cs7);
        return csList;
    }

    //create custom settings for product charge
    public static List<NS_Custom_Options__c> createCustomSettingsforProdCharge(String category, String offer, String def){
        List<NS_Custom_Options__c> csList = new List<NS_Custom_Options__c>();
        NS_Custom_Options__c cs1 = new NS_Custom_Options__c(name = 'SF_CATEGORY_ID', Value__c = category);
        NS_Custom_Options__c cs2 = new NS_Custom_Options__c(name = 'SF_PRODUCT_CHARGE_OFFER_ID', Value__c = offer);
        NS_Custom_Options__c cs3 = new NS_Custom_Options__c(name = 'SF_PRODUCT_CHARGE_DEFINITION_ID', Value__c = def);
        csList.add(cs1);
        csList.add(cs2);
        csList.add(cs3);
        return csList;
    }

    //create custom settings for build contribution
    public static List<NS_Custom_Options__c> createCustomSettingsforBuildContribution(String category, String offer, String def){
        List<NS_Custom_Options__c> csList = new List<NS_Custom_Options__c>();
        NS_Custom_Options__c cs1 = new NS_Custom_Options__c(name = 'SF_CATEGORY_ID', Value__c = category);
        NS_Custom_Options__c cs2 = new NS_Custom_Options__c(name = 'SF_BUILD_CONTRIBUTION_OFFER_ID', Value__c = offer);
        NS_Custom_Options__c cs3 = new NS_Custom_Options__c(name = 'SF_BUILD_CONTRIBUTION_DEFINITION_ID', Value__c = def);
        csList.add(cs1);
        csList.add(cs2);
        csList.add(cs3);
        return csList;
    }

    //create custom settings for build contribution
    public static List<NS_Custom_Options__c> createCustomSettingsforOVC(String category, String offer, String def){
        List<NS_Custom_Options__c> csList = new List<NS_Custom_Options__c>();
        NS_Custom_Options__c cs1 = new NS_Custom_Options__c(name = 'SF_CATEGORY_ID', Value__c = category);
        NS_Custom_Options__c cs2 = new NS_Custom_Options__c(name = 'SF_OVC_DEFINITION_ID', Value__c = def);
        csList.add(cs1);
        csList.add(cs2);
        return csList;
    }

    //Create service feasibility response string
    public static String buildResponseString(String locId, String latitude, String longitude, List<String> addressList, Integer distance, String technology, String status, String rag, String fibreJointId, String fibreJointStatus, String fibreJointLatLong, String fibreJointTypeCode, String fibreJointTypeDescription, String locLatLong, String primaryTechnology, String derivedTechnology){
        String dtoResponse = '';

        SF_ServiceFeasibilityResponse sfResponse = new SF_ServiceFeasibilityResponse(locId, latitude, longitude, addressList, distance, technology, status, rag, fibreJointId, fibreJointStatus, fibreJointLatLong, fibreJointTypeCode, fibreJointTypeDescription, locLatLong);
        sfResponse.primaryTechnology = primaryTechnology;
        sfResponse.derivedTechnology = derivedTechnology;
        dtoResponse = JSON.serialize(sfResponse);
        return dtoResponse;
    }

    //create custom settings for site survey charge
    public static List<NS_Custom_Options__c> createCustomSettingsforSiteSurveyCharge(String category, String offer, String def){
        List<NS_Custom_Options__c> csList = new List<NS_Custom_Options__c>();
        NS_Custom_Options__c cs1 = new NS_Custom_Options__c(name = 'SF_CATEGORY_ID', Value__c = category);
        NS_Custom_Options__c cs2 = new NS_Custom_Options__c(name = 'SF_SITE_SURVEY_CHARGE_OFFER_ID', Value__c = offer);
        NS_Custom_Options__c cs3 = new NS_Custom_Options__c(name = 'SF_SITE_SURVEY_CHARGE_DEFINITION_ID', Value__c = def);
        NS_Custom_Options__c cs4 = new NS_Custom_Options__c(name = 'SF_OPPORTUNITY_STAGE', Value__c = 'Closed Won');
        csList.add(cs1);
        csList.add(cs2);
        csList.add(cs3);
        csList.add(cs4);
        return csList;
    }

    //create custom settings for site survey charge
    public static Business_Platform_Events__c createCustomSettingsforBusinessPlatformEvents(){
        Business_Platform_Events__c settings = new Business_Platform_Events__c();
        settings.AppainToSFSource__c = 'Appian';
        settings.Batch_Size__c = 23;
        settings.DF_SFC_Desktop_Assessment__c = 'SF-DF-000001';
        settings.SFC_Desktop_Assessment_completed__c = 'AP-SF-01000';
        settings.SFToAppainSource__c = 'Salesforce';
        return settings;
    }

    //Create an attachment
    public static Attachment createAttachment(Id parentId){
        final String ENCODING_SCHEME = 'ISO-8859-1';
        String csvContent = 'SearchType,LocationId,Latitude,Longitude,UnitNumber,UnitType,StreetorLotNumber,StreetName,StreetType,SuburborLocality,State,Postcode';
        csvContent = csvContent + '\n';
        csvContent = csvContent + 'SearchByLocationID,LOC000002696724,,,,,,,,,,';
        String base64Data = 'TG9jIElkLEFkZHJlc3MsTGF0aXR1ZGUsTG9uZ2l0dWRlDQo';
        String contentType = 'application/vnd.ms-excel';
        Attachment a = new Attachment();
        a.parentId = parentId;
        Blob bodyBlob = Blob.valueOf(csvContent);
        a.body = bodyBlob;
        a.Name = 'Bulk Upload.csv';
        a.ContentType = 'application/vnd.ms-excel';
        return a;
    }

    // Create df community user and related records
    public static User createDFCommUser() {
        Account commAcct = new Account();
        commAcct.Name = 'RSPTest Acct';

        return createDFCommUser(commAcct);
    }

    public static User createDFCommUser(Account commAcct) {
        Profile commProfile = [SELECT Id
                               FROM Profile
                               WHERE Name = 'DF Partner User'];

        system.debug('createDFCommUser - commProfile: ' + commProfile);

        if(commAcct == null) {
            commAcct = new Account();
            commAcct.Name = 'RSPTest Acct';
        }
        insert commAcct;
        system.debug('createDFCommUser - test account inserted...: account id: ' + commAcct.Id);

        Contact commContact = new Contact();
        commContact.LastName ='testContact';
        commContact.AccountId = commAcct.Id;
        insert commContact;
        system.debug('createDFCommUser - test contact inserted...: contact id: ' + commContact.Id);

        User commUser = new User();
        commUser.Alias = 'test123';
        commUser.Email = 'test123@noemail.com';
        commUser.Emailencodingkey = 'ISO-8859-1';
        commUser.Lastname = 'Testing';
        commUser.Languagelocalekey = 'en_US';
        commUser.Localesidkey = 'en_AU';
        commUser.Profileid = commProfile.Id;
        commUser.IsActive = true;
        commUser.ContactId = commContact.Id;
        commUser.Timezonesidkey = 'Australia/Sydney';
        commUser.Username = 'tester@noemail.com.test';

        insert commUser;
        system.debug('createDFCommUser - test user inserted...: user id: ' + commUser.Id);

        return commUser;
    }

    public class Parameters {
        public Account account {get; set;}
        public Contact contact {get; set;}
        public User user {get; set;}
        public DF_Opportunity_Bundle__c bundle {get; set;}
        public Opportunity opp {get; set;}
        public DF_FBC_Rule__c rule {get; set;}
        public DF_SF_Request__c request {get; set;}
        public DF_Quote__c quote {get; set;}
        public DF_Order__c order {get; set;}
    }

    public static Parameters setupAllData(Parameters params){
        if (params == null) {
            params = new Parameters();
        }

        List<NS_Custom_Options__c> options = SF_TestData.createCustomSettings();
        insert options;

        if (params.account == null) {
            params.account = SF_TestData.createAccount('Test Account');
            params.account.Access_Seeker_ID__c = 'ASI500050005000';
            params.account.Commercial_Deal__c = true;
            params.account.Billing_ID__c = 'BAN000000892801';
        }
        insert params.account;

        // ensure the group is created for sharing
        EE_DataSharingUtility.findGroup(DF_Constants.NBN_SELECT_GROUP_NAME + params.account.Access_Seeker_ID__c);

        if (params.contact == null) {
            params.contact = new Contact();
            params.contact.LastName ='testContact';
        }
        params.contact.AccountId = params.account.Id;
        insert params.contact;

        Profile profile = [SELECT Id FROM Profile WHERE Name = 'DF Partner User'];

        if (params.user == null) {
            params.user = new User();
            params.user.Alias = 'test123';
            params.user.Email = 'test123@noemail.com';
            params.user.Emailencodingkey = 'ISO-8859-1';
            params.user.Lastname = 'Testing';
            params.user.Languagelocalekey = 'en_US';
            params.user.Localesidkey = 'en_AU';
            params.user.IsActive = true;
            params.user.Timezonesidkey = 'Australia/Sydney';
            params.user.Username = 'tester@noemail.com.test';
        }
        params.user.Profileid = profile.Id;
        params.user.ContactId = params.contact.Id;
        insert params.user;

        if (params.bundle == null) {
            params.bundle = SF_TestData.createOpportunityBundle(params.account.Id);
        }
        params.bundle.Account__c = params.account.Id;
        insert params.bundle;

        if (params.opp == null) {
            params.opp = SF_TestData.createOpportunity('Test Opp');
        }
        params.opp.AccountId = params.account.Id;
        params.opp.Opportunity_Bundle__c = params.bundle.Id;
        insert params.opp;

        if (params.rule == null) {
            params.rule = SF_TestData.createRagRuleTable('FTTN', 100, 2000, 'Red');
        }
        insert params.rule;

        String locId = 'LOC000035375038';
        String latitude = '-33.840213';
        String longitude = '151.207368';
        String address = 'LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia';
        if (params.request == null) {
            List<String> addressList = new List<String>{address};
            String sfResponse = SF_TestData.buildResponseString(locId, latitude, longitude, addressList , 500, 'HFC', 'Completed', null,
                                                                    '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173',
                                                                    'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'HFC', null);
            Map<String, String> addressMap = new Map<String, String>();
            params.request = SF_TestData.createSFRequest('SearchByLocationID', 'Completed', locId, latitude, longitude, params.bundle.Id, sfResponse, addressMap);
        }
        params.request.Opportunity_Bundle__c = params.bundle.Id;
        insert params.request;

        if (params.quote == null) {
        	params.quote = DF_TestData.createDFQuote(address, latitude, longitude, locId, params.opp.Id, params.bundle.Id, null, 'Green');
        }
        params.quote.Opportunity__c = params.opp.Id;
        params.quote.Opportunity_Bundle__c = params.bundle.Id;
		insert params.quote;

		if (params.order == null) {
        	params.order = DF_TestData.createDFOrder(params.quote.Id, params.bundle.Id, 'In Draft');
        }
        params.order.DF_Quote__c = params.quote.Id;
        params.order.Opportunity_Bundle__c = params.bundle.Id;
		insert params.order;

        return params;
    }

    public static User createJitUser(String strAccessSeekerId, String strJitProfile, String strUserName){
        Account acc = new Account(recordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Customer').getRecordTypeId(), Tier__c = '1', Name='test nbn', Access_Seeker_ID__c = strAccessSeekerId);
        Database.insert(acc);
        
        Map<String, String> mapAttributes = new Map<String, String>();
        mapAttributes.put('User.UserRoleId',strAccessSeekerId);
        mapAttributes.put('User.FederationIdentifier',strUserName);
        mapAttributes.put('User.LastName',strUserName);
        mapAttributes.put('User.FirstName',strUserName);
        mapAttributes.put('User.Email',strUserName+'@nbnco.com.au.nbnRsp');
        mapAttributes.put('User.ProfileId',strJitProfile);
        
        JITHandler handler = new JITHandler();
        User jitUser = handler.createUser(null,null,null,'testSAML',mapAttributes,null);
        return jitUser;
    }
    

    public static csord__Order__c  buildOrder(String name, Id accountId, Id OppId,String status, Id orderReqId) {
        csord__Order__c  ord = new csord__Order__c();
        ord.name = name;
        ord.csord__Account__c = accountId;
        ord.csordtelcoa__Opportunity__c= oppId;
        ord.csord__Status2__c = status;
        ord.csord__Order_Request__c = orderReqId;
        ord.csord__Identification__c = IDENTIFICATION;
        return ord; 
    }
    
    public static csord__Subscription__c buildSubscription(String name, Id accountId, Id orderId, id orderReqId) {
        csord__Subscription__c subs = new csord__Subscription__c();
        subs.name = name;
        subs.csord__Account__c = accountId;
        subs.csord__Order__c = orderId;
        subs.csord__Identification__c = IDENTIFICATION;
        subs.csord__Order_Request__c = orderReqId;
        return subs;
    }
    
    public static csord__Order_Request__c buildOrderRequest() {
        csord__Order_Request__c orderReq = new csord__Order_Request__c();
        orderReq.name = 'OR-12121';
        orderReq.csord__Module_Name__c = 'TestReq';
        orderReq.csord__Module_Version__c = '12';
        return orderReq;
    } 
    
    public static csord__Service__c buildService(String name, Id orderId, Id subscriptionId, Id orderReq) {
        csord__Service__c ser = new csord__Service__c();
        ser.name = name;
        ser.csord__Order__c = orderId;
        ser.csord__Subscription__c = subscriptionId;
        ser.csord__Identification__c = IDENTIFICATION;
        ser.csord__Order_Request__c = orderReq;
        return ser;
    }
    
    public static csord__Service_Line_Item__c buildServiceLineItem(String name, Id serviceId) {
        csord__Service_Line_Item__c sli = new csord__Service_Line_Item__c();
        sli.Name = name;
        sli.csord__Service__c = serviceId;
        sli.csord__Line_Description__c = '------------ Sli description x 2';
        sli.csord__Identification__c = IDENTIFICATION;
        return sli;
    }

    public static csord__Order_Line_Item__c buildOrderRequestLineItem(String name, Id ordId) {
        csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c();
        oli.Name = name;
        oli.csord__Order__c = ordId;
        oli.csord__Identification__c = IDENTIFICATION;
        return oli;
    }
    
    public static cscfga__Product_Basket__c buildBasketWithOpportnity(Id opportunityId) {
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        basket.cscfga__Basket_Status__c = 'Valid';
        basket.csbb__Synchronised_with_Opportunity__c = false;
        basket.Name = 'TestBasket';
        cscfga__User_Session__c session = new cscfga__User_Session__c();
        session.Name = 'User Session';
        session.cscfga__PC_Return_URL__c = '/';
        insert session;
        basket.cscfga__User_Session__c = session.Id;
        basket.cscfga__Opportunity__c = opportunityId;
        return basket;
    }
    
    public static cscfga__Product_Definition__c buildProductDefinitionWithCategory(String productName, Id pcatId) {
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.Name = productName;
        pd.cscfga__Description__c = productName;
        pd.cscfga__Product_Category__c = pcatId;
        pd.cscfga__Active__c = true;
        pd.csordtelcoa__Product_Type__c = 'Subscription';
        return pd;
    }
    
    public static cscfga__Attribute_Definition__c buildAttributeDefinitionAsLineItem(String name, Id productDefinitionId, Boolean islineItem) {
        cscfga__Attribute_Definition__c attDef = new cscfga__Attribute_Definition__c();
        attDef.name = name;
        attDef.cscfga__Label__c = name;
        attDef.cscfga__Product_Definition__c = productDefinitionId;
        attDef.cscfga__Is_Line_Item__c = islineItem;
        attDef.cscfga__Line_Item_Description__c = 'Test';
        return attDef;
    }
    
    public static cscfga__Product_Configuration__c buildProductConfigwithBasket(Id productDefId, Id basketId){
        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c();
        config.name = 'NBNSelect';
        config.cscfga__Product_Definition__c = productDefId;
        config.cscfga__Product_Basket__c = basketId;
        //config.cscfga__Configuration_Status__c = 'Vaild';
        config.cscfga__Contract_Term__c = 12;
        config.cscfga__Quantity__c = 1;
        config.cscfga__Unit_Price__c = 2000;
        return config;
    }
    
    public static csbb__Product_Configuration_Request__c  buildConfigRequest(Id basketID, Id configId, Id pCatId) {
        csbb__Product_Configuration_Request__c  pcr = new csbb__Product_Configuration_Request__c();
        pcr.csbb__Product_Configuration__c = configId;
        pcr.csbb__Product_Basket__c = basketId;
        pcr.csbb__Product_Category__c = pCatId;
        pcr.csbb__Total_OC__c = 2000;
        return pcr;
    }
    
    public static cscfga__Attribute__c buildAttributeAsLineItem(String name, String value, Id productConfigId, Id attDefId, Boolean isLineItem) {
        cscfga__Attribute__c att = new cscfga__Attribute__c();
        att.name = name;
        att.cscfga__Value__c = value;
        att.cscfga__Attribute_Definition__c = attDefId;
        att.cscfga__Is_Line_Item__c = isLineItem;
        att.cscfga__Product_Configuration__c = productConfigId;
        return att;
    }
    
     public static DF_Custom_Options__c insertDFCustomOptionsCustomSettingRecords(String name, String value) {
        DF_Custom_Options__c dfco = new DF_Custom_Options__c();
        dfco.name = name;
        dfco.Value__c = value;
        return dfco;
    }

}