/**
* Created by Gobind.Khurana on 22/05/2018.
*/

@IsTest
public class SF_TestService {
    
    //create order records
    public static DF_Order__c createOrderRecord(Id accId){
        DF_Opportunity_Bundle__c parentOpp = SF_TestData.createOpportunityBundle(accId);
        insert parentOpp;
        Opportunity childOpp1 = SF_TestData.createOpportunity('Child1');
        childOpp1.Opportunity_Bundle__c = parentOpp.Id;
        insert childOpp1;
        List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp1 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList , 500, 'Wireless', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Wireless', null);
        DF_Quote__c quote1 = SF_TestData.createDFQuote('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia', '-15.451568', '145.178216', 'LOC000035375038', childOpp1.Id, parentOpp.Id, 1500.00, 'Red', resp1);
        insert quote1;
        DF_Order__c ordObj = SF_TestData.createDFOrder(quote1.Id,parentOpp.Id,'In Draft');
        insert ordObj;
        return ordObj;
    }
    //create service feasibility and map it to the opportunity bundle record
    public static Id getServiceFeasibilityRequest(){
        Account acc = SF_TestData.createAccount('My account');
        insert acc;

        return generateSFRequests(acc);
    }

    public static Id generateSFRequests(Account account) {
        List<NS_Custom_Options__c> csList = SF_TestData.createCustomSettings();
        if (!csList.isEmpty()) {
            insert csList;
        }

        DF_Opportunity_Bundle__c parentOpp = SF_TestData.createOpportunityBundle(account.Id);
        insert parentOpp;

        insert new List<DF_FBC_Rule__c>{
                SF_TestData.createRagRuleTable('FTTN', 100, 2000, 'Red'),
                SF_TestData.createRagRuleTable('FTTC', 200, 3000, 'Red'),
                SF_TestData.createRagRuleTable('FTTB', 200, 3000, 'Red'),
                SF_TestData.createRagRuleTable('FTTP', 200, 3000, 'Red')
        };

        List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp1 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'HFC', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'HFC', null);
        String resp2 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'HFC', 'Pending', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'HFC', null);
        String resp3 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'Fibre', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Fibre', null);
        String resp4 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'Satellite', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Satellite', null);
        String resp5 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'FTTC', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);
        String resp6 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);
        String resp7 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'Fibre to the building', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);
        String resp8 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'Undefined', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Undefined', null);
        String resp9 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'null', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'null', null);
        String resp10 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'Satellite', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);
        String resp11 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'Wireless', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);
        String resp12 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'Undefined', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);
        String resp13 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'HFC', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);
        String resp14 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'Wireless', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Wireless', null);
        String resp15 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList, 500, 'Fibre', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);

        // Address values
        String state = 'QLD';
        String postcode = '4895';
        String suburbLocality = 'COOKTOWN';
        String streetName = 'UNIT';
        String streetType = 'ST';
        String streetLotNumber = '204';
        String unitType = 'UNIT';
        String unitNumber = '1';

        String level;
        String complexSiteName;
        String complexBuildingName;
        String complexStreetName;
        String complexStreetType;
        String complexStreetNumber;

        // Build address map
        Map<String, String> addressMap = new Map<String, String>();
        addressMap.put('state', state);
        addressMap.put('postcode', postcode);
        addressMap.put('suburbLocality', suburbLocality);
        addressMap.put('streetName', streetName);
        addressMap.put('streetType', streetType);
        addressMap.put('streetLotNumber', streetLotNumber);
        addressMap.put('unitType', unitType);
        addressMap.put('unitNumber', unitNumber);
        addressMap.put('level', level);
        addressMap.put('complexSiteName', complexSiteName);
        addressMap.put('complexBuildingName', complexBuildingName);
        addressMap.put('complexStreetName', complexStreetName);
        addressMap.put('complexStreetType', complexStreetType);
        addressMap.put('complexStreetNumber', complexStreetNumber);

        insert new List<DF_SF_Request__c>{
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp2, addressMap),
                SF_TestData.createSFRequest('SearchByAddress', 'Completed', null, null, null, parentOpp.Id, resp1, addressMap),
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp3, addressMap),
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp4, addressMap),
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp5, addressMap),
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp6, addressMap),
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp7, addressMap),
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp8, addressMap),
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp9, addressMap),
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp10, addressMap),
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp11, addressMap),
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp12, addressMap),
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp13, addressMap),
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp14, addressMap),
                SF_TestData.createSFRequest('SearchByLocationID', 'Pending', 'LOC000035375038', null, null, parentOpp.Id, resp15, addressMap)
        };

        return parentOpp.Id;
    }

    //create quote records that is linked to an opportunity of its own and all oppotunities linked to the Opportunity bundle record
    public static Id getQuoteRecords(){
        Id parentOppId = getServiceFeasibilityRequest();
        Opportunity childOpp1 = SF_TestData.createOpportunity('Child1');
        childOpp1.Opportunity_Bundle__c = parentOppId;
        insert childOpp1;
        Opportunity childOpp2 = SF_TestData.createOpportunity('Child2');
        childOpp2.Opportunity_Bundle__c = parentOppId;
        insert childOpp2;
        List<String> addressList = new List<String>();
        addressList.add('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia');
        String resp1 = SF_TestData.buildResponseString('LOC000035375038', '-15.451568', '145.178216', addressList , 500, 'Wireless', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Wireless', null);
        String resp2 = SF_TestData.buildResponseString('LOC000002696724', '42.78931', '147.05578', addressList , 500, 'Fibre To The Node', 'Valid', null, '3CBR-25-05-BJL-014', 'INSERVICE', '-7.654550, 145.072173', 'TYCO_OFDC_A4', null, '-7.654550, 145.072173', 'Copper', null);
        DF_Quote__c quote1 = SF_TestData.createDFQuote('LOT 204 1 UNIT ST COOKTOWN QLD 4895 Australia', '-15.451568', '145.178216', 'LOC000035375038', childOpp1.Id, parentOppId, 1500.00, 'Red', resp1);
        DF_Quote__c quote2 = SF_TestData.createDFQuote('17 NICHOLSON ST NEW NORFOLK TAS 7140 Australia', '-42.78931', '147.05578', 'LOC000002696724', childOpp2.Id, parentOppId, 1500.00, 'Amber', resp2);
        insert quote1;
        insert quote2;
        return parentOppId;
    }
    
    //Create Attachment
    public static Id createAttachmentForBulk(Id oppBundleId){
        Attachment a = SF_TestData.createAttachment(oppBundleId);
        insert a;
        return a.Id;
    }
    
    //create new basket with product configuration
    public static cscfga__Product_Basket__c getNewBasketWithConfig(Account acc) {
        DF_AttributeTest testName1 = new DF_AttributeTest('LoC ID', 'Test Value', false, null);
        List<DF_AttributeTest> attributeTests = new List<DF_AttributeTest>{testName1};
            return getNewBasketWithProductConfigData(acc, attributeTests);
    }
    
    //create new basket with product configuration
    public static cscfga__Product_Basket__c getNewBasketWithConfigQuickQuote(Account acc) {
        DF_AttributeTest testName1 = new DF_AttributeTest('CSA', 'CSA400000010875', false, null);
        DF_AttributeTest testName2 = new DF_AttributeTest('SAM ID', '4GUL-01', false, null);
        DF_AttributeTest testName3 = new DF_AttributeTest('Hidden_OVC_Charges', '7563.10', true, 7563.10);
        DF_AttributeTest testName4 = new DF_AttributeTest('OVC Recurring Charge', '354.20', true, 354.20);
        DF_AttributeTest testName5 = new DF_AttributeTest('OVC', 'a1eN0000000iClM', true, null);
        
        List<DF_AttributeTest> attributeTests1 = new List<DF_AttributeTest>{testName2,testName3,testName5};
            List<DF_AttributeTest> attributeTests2 = new List<DF_AttributeTest>{testName1,testName4};
                
                //Create Basket
                cscfga__Product_Basket__c basket = SF_TestData.buildBasket();
        basket.csbb__Account__c = acc.id;
        insert basket;
        
        //create pcr data
        cscfga__Product_Configuration__c parentc = createproductConfigDataQQ('Direct Fibre - Product Charges',basket, attributeTests1);
        cscfga__Product_Configuration__c childc = createproductConfigDataQQ('OVC',basket, attributeTests2);
        childc.cscfga__Parent_Configuration__c = parentc.Id;
        update childc;
        return basket;
    }
    
    //create new basket with product configuration
    public static cscfga__Product_Basket__c getNewBasketWithProductConfigData(Account acc, List<DF_AttributeTest> atts) {
        
        //Create Basket
        cscfga__Product_Basket__c basket = SF_TestData.buildEmptyBasket();
        basket.csbb__Account__c = acc.id;
        
        insert basket;
        
        //create pcr data
        createproductConfigData('Direct Fibre - Product Charges',basket, atts);
        createproductConfigData('Direct Fibre - Build Contribution',basket, atts);
        createproductConfigData('Direct Fibre - Site Survey Charges',basket, atts);
        createproductConfigData('OVC',basket, atts);
        
        return basket;
    }
    //create new basket with product configuration
    public static cscfga__Product_Basket__c getNewBasketWithConfigSiteSurvey(Account acc) {
        DF_AttributeTest testName1 = new DF_AttributeTest('LoC ID', 'Test Value', false, null);
        List<DF_AttributeTest> attributeTests = new List<DF_AttributeTest>{testName1};
            return getNewBasketWithProductConfigDataSiteSurvey(acc, attributeTests);
    }
    
    //create new basket with product configuration
    public static cscfga__Product_Basket__c getNewBasketWithProductConfigDataSiteSurvey(Account acc, List<DF_AttributeTest> atts) {
        
        //Create Basket
        cscfga__Product_Basket__c basket = SF_TestData.buildBasket();
        basket.csbb__Account__c = acc.id;
        
        insert basket;
        
        //create pcr data
        createproductConfigData('Direct Fibre - Product Charges',basket, atts);
        createproductConfigData('Direct Fibre - Build Contribution',basket, atts);
        createproductConfigData('Direct Fibre - Site Survey Charges',basket, atts);
        createproductConfigData('OVC',basket, atts);
        
        return basket;
    }
    
    
    //create product config data
    public static cscfga__Product_Configuration__c createproductConfigDataQQ(String pdtName, cscfga__Product_Basket__c basket, List<DF_AttributeTest> atts){
        cscfga__Product_Category__c  pc = SF_TestData.buildProductCategory();
        insert pc;
        
        cscfga__Product_Definition__c prodDef = SF_TestData.buildProductDefinition(pdtName, 'Test Plan');
        prodDef.cscfga__Active__c = true;
        insert prodDef;
        createSFAssociationForProdDefinition(prodDef.id);
        
        cscfga__Configuration_Offer__c  offer = SF_TestData.createOffer(pdtName);
        insert offer;
        
        //Create Product configuration
        cscfga__Product_Configuration__c config = SF_TestData.buildProductConfig(prodDef.id);
        config.Name = pdtName;
        config.cscfga__Product_Basket__c = basket.id;
        config.cscfga__Contract_Term__c = 12;
        config.cscfga__total_contract_value__c = 325;
        config.cscfga__Configuration_Offer__c = offer.Id;
        insert config;
        
        List<cscfga__Attribute_Definition__c> attDefList =  new List<cscfga__Attribute_Definition__c>();
        String dt;
        for(DF_AttributeTest attTest : atts) {
            if(attTest.Name =='CSA' || attTest.Name =='SAM ID' || attTest.Name =='OVC')
                dt = 'String';
            else if(attTest.Name =='Hidden_OVC_Charges' || attTest.Name =='OVC Recurring Charge')
                dt = 'Decimal';
            cscfga__Attribute_Definition__c attDef = SF_TestData.buildAttributeDefinitionQQ(attTest.name, prodDef.id,dt);
            attDefList.add(attDef);
        }
        insert attDefList;
        
        cscfga__Attribute_Definition__c ad = SF_TestData.buildAttributeDefinitionSelectOptions('Term', prodDef.id,'String');
        insert ad;
        List<cscfga__Select_Option__c> soList = new List<cscfga__Select_Option__c>();
        soList.add(SF_TestData.buildSelectOptions('12 months',ad));
        soList.add(SF_TestData.buildSelectOptions('24 months',ad));
        soList.add(SF_TestData.buildSelectOptions('36 months',ad));
        insert soList;
        
        cscfga__Attribute__c attri = SF_TestData.buildAttribute('Term','12 months',config.Id);
        attri.cscfga__Attribute_Definition__c = ad.Id;
        attri.cscfga__is_active__c = true;
        attri.cscfga__Is_Line_Item__c = true;
        insert attri;
        List<cscfga__Attribute__c> attsList = new List<cscfga__Attribute__c>();
        for(DF_AttributeTest attTest : atts) {
            cscfga__Attribute__c att = SF_TestData.buildAttribute(attTest.name,attTest.value,config.Id);
            att.cscfga__is_active__c = true;
            att.cscfga__Is_Line_Item__c = true;
            attsList.add(att);
        }
        insert attsList;
        List<cscfga__Attribute__c> attsListUpdate = new List<cscfga__Attribute__c>();
        for(cscfga__Attribute_Definition__c adf : attDefList){
            for(cscfga__Attribute__c at : attsList){
                if(adf.Name == at.Name){
                    at.cscfga__Attribute_Definition__c = adf.Id;
                    attsListUpdate.add(at);
                }
            }
        }
        update attsListUpdate;
        //Create Product Confuguration Request using existing method
        
        csbb__Product_Configuration_Request__c pcr  = createPCRWIthCalloutResults();
        pcr.csbb__Product_Configuration__c = config.id;
        pcr.csbb__Product_Basket__c = basket.id;
        pcr.csbb__Total_OC__c = 25;
        pcr.csbb__Total_MRC__c = 25;
        
        update pcr;
        
        //insert custom settings
        List<NS_Custom_Options__c> csList = new List<NS_Custom_Options__c>();
        if(pdtName.equalsIgnoreCase('Direct Fibre - Product Charges')){
            csList = SF_TestData.createCustomSettingsforProdCharge(pc.ID,offer.Id,prodDef.Id);
        }
        else if(pdtName.equalsIgnoreCase('Direct Fibre - Build Contribution')){
            csList = SF_TestData.createCustomSettingsforBuildContribution(pc.ID,offer.Id,prodDef.Id);
        }
        else if(pdtName.equalsIgnoreCase('Direct Fibre - Site Survey Charges')){
            csList = SF_TestData.createCustomSettingsforSiteSurveyCharge(pc.ID,offer.Id,prodDef.Id);
        }
        else if(pdtName.equalsIgnoreCase('OVC')){
            csList = SF_TestData.createCustomSettingsforOVC(pc.ID,offer.Id,prodDef.Id);
        }
        if(!csList.isEmpty())
            insert csList;
        return config;
    }
    
    //create product config data
    public static void createproductConfigData(String pdtName, cscfga__Product_Basket__c basket, List<DF_AttributeTest> atts){
        cscfga__Product_Category__c  pc = SF_TestData.buildProductCategory();
        insert pc;
        
        cscfga__Product_Definition__c prodDef = SF_TestData.buildProductDefinition(pdtName, 'Test Plan');
        insert prodDef;
        createSFAssociationForProdDefinition(prodDef.id);
        
        cscfga__Configuration_Offer__c  offer = SF_TestData.createOffer(pdtName);
        insert offer;
        //Create Product configuration
        cscfga__Product_Configuration__c config = SF_TestData.buildProductConfig(prodDef.id);
        config.Name = pdtName;
        config.cscfga__Product_Basket__c = basket.id;
        config.cscfga__Contract_Term__c = 12;
        config.cscfga__total_contract_value__c = 325;
        config.cscfga__Configuration_Offer__c = offer.Id;
        insert config;
        
        //Create Product Confuguration Request using existing method
        
        csbb__Product_Configuration_Request__c pcr  = createPCRWIthCalloutResults();
        pcr.csbb__Product_Configuration__c = config.id;
        pcr.csbb__Product_Basket__c = basket.id;
        pcr.csbb__Total_OC__c = 25;
        pcr.csbb__Total_MRC__c = 25;
        
        update pcr;
        
        List<cscfga__Attribute_Definition__c> attDefList =  new List<cscfga__Attribute_Definition__c>();
        List<cscfga__Attribute__c> attList =  new List<cscfga__Attribute__c>();
        for(DF_AttributeTest attTest : atts) {
            cscfga__Attribute_Definition__c attDef = SF_TestData.buildAttributeDefinition(attTest.name, prodDef.id);
            attDefList.add(attDef);
        }
        insert attDefList;
        cscfga__Attribute_Definition__c attDef = !attDefList.isEmpty() ? attDefList.get(0) : null;
        for(DF_AttributeTest attTest : atts) {
            cscfga__Attribute__c att  = SF_TestData.buildPriceAttribute(attTest.name, attTest.value, config.id, attTest.isLineItem, attTest.price);
            att.cscfga__Attribute_Definition__c = attDef.id;
            attList.add(att);
        }
        insert attList;
        
        List<cscfga__Attribute_Field_Definition__c> attFdDefList =  new List<cscfga__Attribute_Field_Definition__c>();
        for(cscfga__Attribute_Definition__c def : attDefList){
            cscfga__Attribute_Field_Definition__c fdDef = new cscfga__Attribute_Field_Definition__c();
            fdDef.name = 'Address';
            fdDef.cscfga__Default_Value__c = 'Sample';
            fdDef.cscfga__Attribute_Definition__c = def.id;
            attFdDefList.add(fdDef);
        }
        
        if(attFdDefList.size()>0){
            insert attFdDefList;
        }
        
        //insert custom settings
        List<NS_Custom_Options__c> csList = new List<NS_Custom_Options__c>();
        if(pdtName.equalsIgnoreCase('Direct Fibre - Product Charges')){
            csList = SF_TestData.createCustomSettingsforProdCharge(pc.ID,offer.Id,prodDef.Id);
        }
        else if(pdtName.equalsIgnoreCase('Direct Fibre - Build Contribution')){
            csList = SF_TestData.createCustomSettingsforBuildContribution(pc.ID,offer.Id,prodDef.Id);
        }
        else if(pdtName.equalsIgnoreCase('Direct Fibre - Site Survey Charges')){
            csList = SF_TestData.createCustomSettingsforSiteSurveyCharge(pc.ID,offer.Id,prodDef.Id);
        }
        else if(pdtName.equalsIgnoreCase('OVC')){
            csList = SF_TestData.createCustomSettingsforOVC(pc.ID,offer.Id,prodDef.Id);
        }
        if(!csList.isEmpty())
            insert csList;
    }
    
    //create product configuration request
    public static csbb__Product_Configuration_Request__c createPCRWIthCalloutResults() {
        cscfga__Product_Category__c  pc = SF_TestData.buildProductCategory();
        insert pc;
        csbb__Product_Configuration_Request__c pcr = new csbb__Product_Configuration_Request__c();
        pcr.csbb__Product_Category__c = pc.id;
        insert pcr;
        csbb__Callout_Result__c cor = SF_TestData.buildCallOutResults();
        insert cor;
        csbb__Callout_Product_Result__c copr = SF_TestData.buildCalloutProductResults(pcr, cor);
        insert copr;
        
        return pcr;
    }
    
    //create product definition and associate with screen flows
    public static void createSFAssociationForProdDefinition(Id prodDefId) {
        List<cscfga__Screen_Flow__c> sfs = new List<cscfga__Screen_Flow__c>();
        cscfga__Screen_Flow__c sf1 = SF_TestData.buildScreenFlow('SF Name 1');
        sfs.add(sf1);
        cscfga__Screen_Flow__c sf2 = SF_TestData.buildScreenFlow('SF Name 2');
        sfs.add(sf2);
        insert sfs;
        
        List<cscfga__Screen_Flow_Product_Association__c> sfAs = new List<cscfga__Screen_Flow_Product_Association__c>();
        cscfga__Screen_Flow_Product_Association__c sfA1 = SF_TestData.buildSFAssociation(prodDefId, sf1.id);
        sfAs.add(sfA1);
        cscfga__Screen_Flow_Product_Association__c sfA2 = SF_TestData.buildSFAssociation(prodDefId, sf2.id);
        sfAs.add(sfA2);
        insert sfAs;
    }
}