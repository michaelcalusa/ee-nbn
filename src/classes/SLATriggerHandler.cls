public class SLATriggerHandler implements ITriggerHandler {
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    //static variable to cover the try catch of setCurrentSLAOnIncidents method
    public static Boolean throwException = false;
    // Checks to see if the trigger has been disabled either by custom setting or by running code
    public Boolean IsDisabled(){
        return TriggerDisabled;
    }
    /*************************************************************************************************
    Trigger Handler Action Methods:
    *************************************************************************************************/
    public void BeforeInsert(List<SObject> newItems, Map<Id, SObject> newItemMap) {
    }
    public void BeforeUpdate(List<SObject> newItems, List<SObject> oldItems, Map<Id, SObject> newItemMap, Map<Id, SObject> oldItemMap){
        
    }
    public void BeforeDelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    public void AfterInsert(List<SObject> newItems, Map<Id, SObject> newItemMap) {
        setCurrentSLAOnIncidents(newItems,Null);
    }
    public void AfterUpdate(List<SObject> newItems, List<SObject> oldItems, Map<Id, SObject> newItemMap, Map<Id, SObject> oldItemMap){
        setCurrentSLAOnIncidents(newItems,oldItemMap);
    }
    public void AfterDelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    public void AfterUndelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    /***************************************************************************************************
    Method Name:        setCurrentSLAOnIncidents
    Method Parameters:  Lits of Inserted/Updated SLA Object Records
    Method Return Type: Void
    Method Description :- used for Deriving the current SLA Associated with Incident record and tagging
                          it to the record.
    Version:            1.0 
    Created Date:       20 Febraury 2019
    ***************************************************************************************************/
    private void setCurrentSLAOnIncidents(List<SObject> newItems,Map<Id, SObject> oldItemMap)
    {
        //Variable to store the map of incident recordid and response slas,with Incident RecordId as key and list of SLA Records as value.
        Map<Id,SLA__c> responseSLAMap = New Map<Id,SLA__c>();
        //Variable to store the map of incident recordid and resolution slas,with Incident RecordId as key and list of SLA Records as value.
        Map<Id,SLA__c> resolutionSLAMap = New Map<Id,SLA__c>();
        //Variable to store List of Incident record id's for which there is a change in sla's
        List<Id> lstIncidentIds = new List<Id>();
        //Variable used to intialize the incident management record that needs to be updated
        Incident_Management__c updateIncidentManagementRecord;
        //Variable to store the list of incident management records that needs to be updated.
        List<Incident_Management__c> lstUpdateIncidentManagementRecords = new List<Incident_Management__c>();
        //Map variable used for storing the SLA Records with SLA ID as Key, it is used for updating the Current SLA Flag of SLA Records.
        Map<Id,SLA__c> updateCurrentSLAMap = New Map<Id,SLA__c>();
        //List Variable to store the Id's of the current SLA Records.
        List<Id> lstCurrentSLARecordIds = new List<Id>();
        //List Variable to store the id's of the updated SLA Records.
        List<Id> lstupdatedSLARecordIds = new List<Id>();
        //Temparory varaible used for intializing SLA Records
        SLA__c updateSLARecord;
        //List Variable used for storing the terminal status slarecords.
        Map<Id,SLA__c> lstTerminalStatusSLAMap = new Map<Id,SLA__c>();
        //Iterate over the newly inserted records to get the response and resolution sla maps.
        for(SLA__c currentIterationSLARecord : (List<SLA__C>)newItems)
        {
            if(String.isNotBlank(currentIterationSLARecord.Incident__c))
            {
                if(!lstIncidentIds.Contains(currentIterationSLARecord.Incident__c))  
                {
                    lstIncidentIds.Add(currentIterationSLARecord.Incident__c);         
                }
                if(String.isNotBlank(currentIterationSLARecord.slaType__c) && String.isNotBlank(currentIterationSLARecord.slaStatus__c))
                {
                    if(currentIterationSLARecord.slaType__c == 'Incident Response Time' && (currentIterationSLARecord.slaStatus__c.Equals('In Process') || currentIterationSLARecord.slaStatus__c.Equals('Warning') || currentIterationSLARecord.slaStatus__c.Equals('Missed Goal') || currentIterationSLARecord.slaStatus__c.Equals('Pending')))
                    {
                        responseSLAMap.put(currentIterationSLARecord.Incident__c,currentIterationSLARecord);    
                    }
                    if(currentIterationSLARecord.slaType__c == 'Incident Resolution Time' && (currentIterationSLARecord.slaStatus__c.Equals('In Process') || currentIterationSLARecord.slaStatus__c.Equals('Warning') || currentIterationSLARecord.slaStatus__c.Equals('Missed Goal') || currentIterationSLARecord.slaStatus__c.Equals('Pending')))
                    {
                        resolutionSLAMap.put(currentIterationSLARecord.Incident__c,currentIterationSLARecord);
                    }
                }
                //Add the updated record id to the list lstupdatedSLARecordIDs, it is used for filtering the records while selecting the existing list of sla records
                lstupdatedSLARecordIds.Add(currentIterationSLARecord.Id);
                //Intialize the SLA Records for Updating Current SLA Flags
                updateSLARecord = new SLA__C(Id = currentIterationSLARecord.Id);
                updateSLARecord.Current_SLA__c = false;
                updateCurrentSLAMap.put(currentIterationSLARecord.Id,updateSLARecord);
                //Check if the change is only related to status and Status is in termination state.
                if(oldItemMap != Null && !oldItemMap.isEmpty())
                {
                    if(currentIterationSLARecord.Current_SLA__c && String.isNotBlank(currentIterationSLARecord.slaType__c) && currentIterationSLARecord.slaType__c == 'Incident Resolution Time' && (String.isNotBlank(currentIterationSLARecord.slaStatus__c) && (currentIterationSLARecord.slaStatus__c.Equals('Met') || currentIterationSLARecord.slaStatus__c.Equals('Missed'))))
                    {
                        if(currentIterationSLARecord.slaStatus__c != ((SLA__c)oldItemMap.get(currentIterationSLARecord.Id)).slaStatus__c)
                        {
                            if(!lstTerminalStatusSLAMap.Keyset().Contains(currentIterationSLARecord.Incident__c))
                            {
                                lstTerminalStatusSLAMap.Put(currentIterationSLARecord.Incident__c,currentIterationSLARecord);
                            }
                        }
                    }
                }
                System.Debug(lstTerminalStatusSLAMap);
            }
        }
        //Iterate over the existing records to get the response and resolution sla maps for the incident records for which there is a change in SLA.
        if(lstIncidentIds != Null && !lstIncidentIds.isEmpty())
        {
            for(SLA__c currentIterationSLARecord : [Select Id,Current_SLA__c,slaType__c,slaStatus__c,Incident__c,slaStartTime__c,slaPauseStartTime__c,slaDueDate__c,RemedySLACreatedDate__c From SLA__c Where Incident__c In : lstIncidentIds])
            {
                if(!lstupdatedSLARecordIds.Contains(currentIterationSLARecord.Id))
                {
                    if(String.isNotBlank(currentIterationSLARecord.slaType__c) && String.isNotBlank(currentIterationSLARecord.slaStatus__c))
                    {
                        if(responseSLAMap.get(currentIterationSLARecord.Incident__c) == Null && currentIterationSLARecord.slaType__c == 'Incident Response Time' && (currentIterationSLARecord.slaStatus__c.Equals('In Process') || currentIterationSLARecord.slaStatus__c.Equals('Warning') || currentIterationSLARecord.slaStatus__c.Equals('Missed Goal') || currentIterationSLARecord.slaStatus__c.Equals('Pending')))
                        {
                            responseSLAMap.put(currentIterationSLARecord.Incident__c,currentIterationSLARecord);    
                        }
                        if(resolutionSLAMap.get(currentIterationSLARecord.Incident__c) == Null && currentIterationSLARecord.slaType__c == 'Incident Resolution Time' && (currentIterationSLARecord.slaStatus__c.Equals('In Process') || currentIterationSLARecord.slaStatus__c.Equals('Warning') || currentIterationSLARecord.slaStatus__c.Equals('Pending') || currentIterationSLARecord.slaStatus__c.Equals('Missed Goal')))
                        {
                            resolutionSLAMap.put(currentIterationSLARecord.Incident__c,currentIterationSLARecord);
                        }
                    }
                    //Intialize the SLA Records for Updating Current SLA Flags
                    updateSLARecord = new SLA__C(Id = currentIterationSLARecord.Id);
                    updateSLARecord.Current_SLA__c = false;
                    updateCurrentSLAMap.put(currentIterationSLARecord.Id,updateSLARecord);
                }
            }
        }
        lstUpdateIncidentManagementRecords = setCurrentSLA(lstIncidentIds,responseSLAMap,resolutionSLAMap,lstCurrentSLARecordIds);
        try
        {
            //Update IncidentManagement Records with Current SLA And Other SLA attributes.
            if(lstUpdateIncidentManagementRecords != Null && !lstUpdateIncidentManagementRecords.isEmpty())
            {
                System.Debug(lstUpdateIncidentManagementRecords);
                IncidentManagementTriggerHandler.TriggerDisabled = true;
                update lstUpdateIncidentManagementRecords;
                IncidentManagementTriggerHandler.TriggerDisabled = false;
                if(Test.isRunningTest() && throwException)
                {
                    throw new applicationException('CustomException');
                }
            }
            //Update SLARecords With CurrentSLA Flag.
            if(lstCurrentSLARecordIds != Null && !lstCurrentSLARecordIds.isEmpty())
            {
                updateCurrentSLAFlagOnSLARecords(updateCurrentSLAMap,lstCurrentSLARecordIds);
            }
            //Update Incident Management Records when SLA Records Moves To Termnal State 
            if(lstTerminalStatusSLAMap != Null && !lstTerminalStatusSLAMap.isEmpty())
            {
                updateTerminalStatusIncidentRecords(lstTerminalStatusSLAMap);
            }
        }
        catch(Exception ex)
        {
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'SLATriggerHandler','setCurrentSLAOnIncidents','','Tried to process current SLA logic but failed due to exception stated in exception details.',ex.getMessage(),JSON.serialize(lstUpdateIncidentManagementRecords),ex,0);
        }
    }
    /***************************************************************************************************
    Method Name:        setCurrentSLA
    Method Parameters:  List of IncidentManagementRecords,Response SLA Map, Resolution SLA Map and List of Ids For recording Current SLA Records.
    Method Return Type: IncidentManagementRecord
    Method Description :- used for selecting the right sla record based on logic used for determining the current SLA
    
    Logic used for determining the current sla
    Any incident that is created and moved to salesofrce from remedy will be associated with two sla one is response and
    The other is Resolution sla.
    Only the SLAs which have slaType as either 'Incident Resolution Time' or 'Incident Response Time' should be considered for business rules.
    If slaStatus of Resolution SLA is either 'In Process' or 'Warning' or 'Missed Goal', then that SLA is the currently active SLA.
    If slaStatus of Resolution SLA is 'Pending', then check if slaStartTime of any Response SLA is later than slaPauseStartTime of Resolution SLA and slaStatus of the Response SLA is not equal to 'Met' or 'Missed'. 
    If such a Response SLA is found, that is the currently active SLA.
    If slaStatus of Resolution SLA is 'Missed Goal', then check if slaStartTime of any Response SLA is later than slaDueDate of Resolution SLA and slaStatus of the Response SLA is not equal to 'Met' or 'Missed'. If such a Response SLA is found, that is the currently active SLA.
    Else, the Resolution SLA is the currently active SLA.

    Version:            1.0 
    Created Date:       20 Febraury 2019
    ***************************************************************************************************/
    private List<Incident_Management__c> setCurrentSLA(List<Id> lstIncidentIds,Map<Id,SLA__c> responseSLAMAP,Map<Id,SLA__c> resolutionSLAMap,List<Id> lstCurrentSLARecordIds)
    {
        //List variable to store the incident management records for updating the current sla field values.
        List<Incident_Management__c> lstUpdIncidentManagementRecords = new List<Incident_Management__c>();
        //Variables to store the response sla record from Response SLA Map for the iterated Incident Management record
        SLA__c responseSLA;
        SLA__c resolutionSLA;
        //Iterate over Incident management records,set the current sla,slatype and sladue date fields based on the current sla selection logic.
        for(Incident_Management__c currentIterationIncidentRecord : [Select Id,Current_SLA__c,SLA_Type__c,SLAResponseOrResolve__c,SLA_Resolution_Date_Time__c,Awaiting_Current_SLA_Status__c,RequestAppointmentSLAStatus__c From Incident_Management__c Where Id In :lstIncidentIds])
        {
            resolutionSLA = resolutionSLAMap.get(currentIterationIncidentRecord.Id);
            responseSLA = responseSLAMAP.get(currentIterationIncidentRecord.Id);
            if(resolutionSLA != Null)
            {
                if(resolutionSLA.slaStatus__c.Equals('In Process') || resolutionSLA.slaStatus__c.Equals('Warning') || ((resolutionSLA.slaStatus__c.Equals('Pending') || resolutionSLA.slaStatus__c.Equals('Missed Goal')) && responseSLA == Null))
                {
                    setIncidentManagementRecordDetails(currentIterationIncidentRecord,resolutionSLA,'Resolve',lstCurrentSLARecordIds);
                    lstUpdIncidentManagementRecords.Add(currentIterationIncidentRecord);
                }
                else if(resolutionSLA.slaStatus__c.Equals('Pending') && responseSLA != Null && resolutionSLA.slaPauseStartTime__c <= responseSLA.RemedySLACreatedDate__c)
                {
                    setIncidentManagementRecordDetails(currentIterationIncidentRecord,responseSLA,'Response',lstCurrentSLARecordIds);
                    lstUpdIncidentManagementRecords.Add(currentIterationIncidentRecord);
                }
                else if(resolutionSLA.slaStatus__c.Equals('Missed Goal') && responseSLA != Null && responseSLA.RemedySLACreatedDate__c >= resolutionSLA.slaDueDate__c)
                {
                    setIncidentManagementRecordDetails(currentIterationIncidentRecord,responseSLA,'Response',lstCurrentSLARecordIds);
                    lstUpdIncidentManagementRecords.Add(currentIterationIncidentRecord);
                }
            }
            else if(resolutionSLA == Null && responseSLA != Null)
            {
                setIncidentManagementRecordDetails(currentIterationIncidentRecord,responseSLA,'Response',lstCurrentSLARecordIds);
                lstUpdIncidentManagementRecords.Add(currentIterationIncidentRecord);
            }
        }
        return lstUpdIncidentManagementRecords;
    }
    /***************************************************************************************************
    Method Name:        setIncidentManagementRecordDetails
    Method Parameters:  IncidentManagementRecord, currentslaRecord and SlaReponsetype(Response or Resolve)
    Method Return Type: Void
    Method Description :- used for setting field values of IncidentManagementRecord with the current SLA Record
                          Field Values.
    Version:            1.0 
    Created Date:       20 Febraury 2019
    ***************************************************************************************************/
    private void setIncidentManagementRecordDetails(Incident_Management__c incidentManagementRecord,SLA__c slaRecord,String slaResponse,List<Id> lstCurrentSLARecordIds)
    {
        incidentManagementRecord.Current_SLA__c =  slaRecord.Id;
        incidentManagementRecord.SLA_Resolution_Date_Time__c = slaRecord.slaDueDate__c;
        incidentManagementRecord.Awaiting_Current_SLA_Status__c = false;
        incidentManagementRecord.RequestAppointmentSLAStatus__c = false;
        incidentManagementRecord.SLAResponseOrResolve__c = slaResponse;
        incidentManagementRecord.SLA_Type__c = (slaResponse == 'Response') ? 'Incident Response Time' : 'Incident Resolution Time';
        lstCurrentSLARecordIds.Add(slaRecord.Id);
    }
    /***************************************************************************************************
    Method Name:        updateCurrentSLAFlagOnSLARecords
    Method Parameters:  Map of Curent SLA Id and SLA Record,List of current SLA Recordids updated via current SLA Selection Logic.
    Method Return Type: Void
    Method Description :- used for updating current sla field value in SLA Records.
    Version:            1.0 
    Created Date:       20 Febraury 2019
    ***************************************************************************************************/
    private void updateCurrentSLAFlagOnSLARecords(Map<Id,SLA__c> updateCurrentSLAMap,List<Id> lstCurrentSLARecordIds)
    {
        //Temparory varaible used for intializing SLA Records
        SLA__c updateSLARecord;
        for(Id currentSLARecordId : lstCurrentSLARecordIds)
        {
            updateSLARecord = updateCurrentSLAMap.get(currentSLARecordId);
            updateSLARecord.Current_SLA__c = true;
            updateCurrentSLAMap.remove(currentSLARecordId);
            updateCurrentSLAMap.put(currentSLARecordId,updateSLARecord);
        }
        SLATriggerHandler.TriggerDisabled = true;
        Update updateCurrentSLAMap.Values();
        SLATriggerHandler.TriggerDisabled = false;
    }
    /***************************************************************************************************
    Method Name:        updateTerminalStatusIncidentRecords
    Method Parameters:  Map of Inciden Id and SLA Record,List of current SLA Recordids created via current SLA Selection Logic.
    Method Return Type: Void
    Method Description :- used for updating current sla field value in SLA Records.
    Version:            1.0 
    Created Date:       20 Febraury 2019
    ***************************************************************************************************/
    private void updateTerminalStatusIncidentRecords(Map<Id,SLA__c> lstTerminalStatusSLAMap)
    {
        //Variable used to intialize the incident management record that needs to be updated
        Incident_Management__c updateIncidentManagementRecord;
        //Variable to store the list of incident management records that needs to be updated.
        List<Incident_Management__c> lstUpdateIncidentManagementRecords = new List<Incident_Management__c>();
        For(Incident_Management__c currentIterationIncidentRecord : [Select Id,Current_SLA__c,Awaiting_Current_SLA_Status__c From Incident_Management__c where ID IN :lstTerminalStatusSLAMap.keySet() AND Current_Status__c = 'Closed' For Update])
        {
            System.Debug(currentIterationIncidentRecord);
            if(currentIterationIncidentRecord.Current_SLA__c == lstTerminalStatusSLAMap.get(currentIterationIncidentRecord.Id).Id)
            {
                updateIncidentManagementRecord = new Incident_Management__C(id = currentIterationIncidentRecord.Id);
                updateIncidentManagementRecord.Awaiting_Current_SLA_Status__c = false;
                lstUpdateIncidentManagementRecords.add(updateIncidentManagementRecord);
            }
        }
        if(lstUpdateIncidentManagementRecords != Null && !lstUpdateIncidentManagementRecords.isEmpty())
        {
            System.Debug(lstUpdateIncidentManagementRecords);
            IncidentManagementTriggerHandler.TriggerDisabled = true;
            update lstUpdateIncidentManagementRecords;
            IncidentManagementTriggerHandler.TriggerDisabled = false;
        }
    }
    private class applicationException extends Exception {}
}