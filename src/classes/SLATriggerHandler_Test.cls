@isTest
private class SLATriggerHandler_Test {
    @testSetup static void setup()
    {
        //Create Test Incident Records
        List<Incident_Management__c> lstTestIncidents = new List<Incident_Management__c>();
        Incident_Management__c testIncidentRecord; 
        for(Integer i = 0;i < 10; i++)
        {
            testIncidentRecord = new Incident_Management__c();
            testIncidentRecord.Name = 'INC00000972327' + i;
            testIncidentRecord.Appointment_Start_Date__c = System.now();
            testIncidentRecord.Appointment_End_Date__c = System.now();
            testIncidentRecord.Target_Commitment_Date__c = System.now();
            testIncidentRecord.AVC_Id__c = 'AVC12345677121';
            lstTestIncidents.Add(testIncidentRecord);
        }
        Insert(lstTestIncidents);
    }
    @isTest static void setCurrentSLAResponseSLA()
    {
        List<SLA__C> lstSLA = new List<SLA__C>();
        SLA__C responseSLA;
        for(Incident_Management__c currentIncidentManagementRecord : [Select Id,Name From Incident_Management__c Where AVC_Id__c = 'AVC12345677121'])
        {
            responseSLA = new SLA__c();
            responseSLA.Incident__c = currentIncidentManagementRecord.Id;
            responseSLA.SLAType__c = 'Incident Response Time';
            responseSLA.slaStatus__c = 'In Process';
            responseSLA.slaPauseStartTime__c = datetime.newInstance(2013, 9, 15, 13, 30, 0);
            lstSLA.add(responseSLA);
        }
        Test.startTest();
            Insert lstSLA;
        Test.stopTest();
        //Assertion Section
        List<SLA__c> lstCurrentSLARecords = [Select Id,Name From SLA__c Where Current_SLA__c = true];
        System.AssertEquals(lstSLA.size(),lstCurrentSLARecords.size());
        List<Incident_Management__c> lstSLATaggedIncidents = [Select Id,Name From Incident_Management__c Where Current_SLA__c in :lstSLA];
        System.AssertEquals(10,lstSLATaggedIncidents.size());
    }
    @isTest static void setCurrentSLAResolutionSLA()
    {
        List<SLA__C> lstResponseSLA = new List<SLA__C>();
        List<SLA__C> lstResolutionSLA = new List<SLA__C>();
        SLA__C responseSLA;
        SLA__c resolutionSLA;
        for(Incident_Management__c currentIncidentManagementRecord : [Select Id,Name From Incident_Management__c Where AVC_Id__c = 'AVC12345677121'])
        {
            //Intialise Response SLAS
            responseSLA = new SLA__c();
            responseSLA.Incident__c = currentIncidentManagementRecord.Id;
            responseSLA.SLAType__c = 'Incident Response Time';
            responseSLA.slaStatus__c = 'MET';
            lstResponseSLA.add(responseSLA);
            //Intialise Resolution SLAS
            resolutionSLA = new SLA__c();
            resolutionSLA.Incident__c = currentIncidentManagementRecord.Id;
            resolutionSLA.SLAType__c = 'Incident Resolution Time';
            resolutionSLA.slaStatus__c = 'In Process';
            lstResolutionSLA.Add(resolutionSLA);
        }
        Insert lstResponseSLA;
        Test.startTest();
            Insert lstResolutionSLA;
        Test.stopTest();
        //Assertion Section
        List<SLA__c> lstCurrentSLARecords = [Select Id,Name From SLA__c Where Current_SLA__c = true];
        System.AssertEquals(lstResolutionSLA.size(),lstCurrentSLARecords.size());
        List<Incident_Management__c> lstSLATaggedIncidents = [Select Id,Name From Incident_Management__c Where Current_SLA__c in :lstResolutionSLA];
        System.AssertEquals(10,lstSLATaggedIncidents.size());    
    }
    @isTest static void setCurrentSLAPendingSLA()
    {
        List<SLA__C> lstResponseSLA = new List<SLA__C>();
        List<SLA__C> lstResolutionSLA = new List<SLA__C>();
        List<SLA__C> lstUpdateResolutionSLA = new List<SLA__C>();
        SLA__C responseSLA;
        SLA__c resolutionSLA;
        for(Incident_Management__c currentIncidentManagementRecord : [Select Id,Name From Incident_Management__c Where AVC_Id__c = 'AVC12345677121'])
        {
            //Intialise Response SLAS
            responseSLA = new SLA__c();
            responseSLA.Incident__c = currentIncidentManagementRecord.Id;
            responseSLA.SLAType__c = 'Incident Response Time';
            responseSLA.slaStatus__c = 'In Process';
            responseSLA.RemedySLACreatedDate__c  = System.now() + 1;
            lstResponseSLA.add(responseSLA);
            //Intialise Resolution SLAS
            resolutionSLA = new SLA__c();
            resolutionSLA.Incident__c = currentIncidentManagementRecord.Id;
            resolutionSLA.SLAType__c = 'Incident Resolution Time';
            resolutionSLA.slaStatus__c = 'In Process';
            resolutionSLA.slaPauseStartTime__c = System.now();
            lstResolutionSLA.Add(resolutionSLA);
        }
        Insert lstResolutionSLA;
        for(SLA__C loopedSLARecord : lstResolutionSLA)
        {
            resolutionSLA = new SLA__c(Id = loopedSLARecord.Id);
            resolutionSLA.slaStatus__c = 'Pending';
            lstUpdateResolutionSLA.Add(resolutionSLA);
        }
        Test.startTest();
            Update lstUpdateResolutionSLA;
            Insert lstResponseSLA;
        Test.stopTest();
        //Assertion Section
        List<SLA__c> lstCurrentSLARecords = [Select Id,Name From SLA__c Where Current_SLA__c = true];
        System.AssertEquals(lstResponseSLA.size(),lstCurrentSLARecords.size());
        List<Incident_Management__c> lstSLATaggedIncidents = [Select Id,Name From Incident_Management__c Where Current_SLA__c in :lstResponseSLA];
        System.AssertEquals(10,lstSLATaggedIncidents.size());    
    }
    @isTest static void setCurrentSLAMissedGoalSLA()
    {
        List<SLA__C> lstResponseSLA = new List<SLA__C>();
        List<SLA__C> lstResolutionSLA = new List<SLA__C>();
        List<SLA__C> lstUpdateResolutionSLA = new List<SLA__C>();
        SLA__C responseSLA;
        SLA__c resolutionSLA;
        for(Incident_Management__c currentIncidentManagementRecord : [Select Id,Name From Incident_Management__c Where AVC_Id__c = 'AVC12345677121'])
        {
            //Intialise Response SLAS
            responseSLA = new SLA__c();
            responseSLA.Incident__c = currentIncidentManagementRecord.Id;
            responseSLA.SLAType__c = 'Incident Response Time';
            responseSLA.slaStatus__c = 'In Process';
            responseSLA.RemedySLACreatedDate__c  = System.now() + 1;
            lstResponseSLA.add(responseSLA);
            //Intialise Resolution SLAS
            resolutionSLA = new SLA__c();
            resolutionSLA.Incident__c = currentIncidentManagementRecord.Id;
            resolutionSLA.SLAType__c = 'Incident Resolution Time';
            resolutionSLA.slaStatus__c = 'In Process';
            resolutionSLA.slaDueDate__c = System.now();
            lstResolutionSLA.Add(resolutionSLA);
        }
        Insert lstResolutionSLA;
        Insert lstResponseSLA;
        for(SLA__C loopedSLARecord : lstResolutionSLA)
        {
            resolutionSLA = new SLA__c(Id = loopedSLARecord.Id);
            resolutionSLA.slaStatus__c = 'Missed Goal';
            lstUpdateResolutionSLA.Add(resolutionSLA);
        }
        Test.startTest();
            Update lstUpdateResolutionSLA;
        Test.stopTest();
        //Assertion Section
        List<SLA__c> lstCurrentSLARecords = [Select Id,Name From SLA__c Where Current_SLA__c = true];
        System.AssertEquals(lstResponseSLA.size(),lstCurrentSLARecords.size());
        List<Incident_Management__c> lstSLATaggedIncidents = [Select Id,Name From Incident_Management__c Where Current_SLA__c in :lstResponseSLA];
        System.AssertEquals(10,lstSLATaggedIncidents.size());    
    }
    @isTest static void setCurrentSLATerminalState()
    {
        List<SLA__C> lstResponseSLA = new List<SLA__C>();
        List<SLA__C> lstResolutionSLA = new List<SLA__C>();
        List<SLA__C> lstUpdateResolutionSLA = new List<SLA__C>();
        List<Incident_Management__c> lstUpdateIncidentManagementRecords = new List<Incident_Management__c>();
        Incident_Management__c updateIncidentManagementRecord;
        SLA__C responseSLA;
        SLA__c resolutionSLA;
        for(Incident_Management__c currentIncidentManagementRecord : [Select Id,Name From Incident_Management__c Where AVC_Id__c = 'AVC12345677121'])
        {
            //Update Incident Management Recoords
            updateIncidentManagementRecord = new Incident_Management__c(Id = currentIncidentManagementRecord.Id);
            updateIncidentManagementRecord.Industry_Status__c = 'Closed';
            lstUpdateIncidentManagementRecords.Add(updateIncidentManagementRecord);
            //Intialise Response SLAS
            responseSLA = new SLA__c();
            responseSLA.Incident__c = currentIncidentManagementRecord.Id;
            responseSLA.SLAType__c = 'Incident Response Time';
            responseSLA.slaStatus__c = 'MET';
            lstResponseSLA.add(responseSLA);
            //Intialise Resolution SLAS
            resolutionSLA = new SLA__c();
            resolutionSLA.Incident__c = currentIncidentManagementRecord.Id;
            resolutionSLA.SLAType__c = 'Incident Resolution Time';
            resolutionSLA.slaStatus__c = 'In Process';
            lstResolutionSLA.Add(resolutionSLA);
        }
        Update lstUpdateIncidentManagementRecords;
        Insert lstResponseSLA;
        Insert lstResolutionSLA;
        for(SLA__C loopedSLARecord : lstResolutionSLA)
        {
            resolutionSLA = new SLA__c(Id = loopedSLARecord.Id);
            resolutionSLA.slaStatus__c = 'Met';
            lstUpdateResolutionSLA.Add(resolutionSLA);
        }
        Test.startTest();
            Update lstUpdateResolutionSLA;
        Test.stopTest();
        //Assertion Section
        List<SLA__c> lstCurrentSLARecords = [Select Id,Name From SLA__c Where Current_SLA__c = true];
        System.AssertEquals(lstResolutionSLA.size(),lstCurrentSLARecords.size());
        List<Incident_Management__c> lstSLATaggedIncidents = [Select Id,Name From Incident_Management__c Where Current_SLA__c in :lstResolutionSLA];
        System.AssertEquals(10,lstSLATaggedIncidents.size());    
    }
    @isTest static void SlatriggerHandlerDelete()
    {
        List<SLA__C> lstSLA = new List<SLA__C>();
        SLA__C responseSLA;
        for(Incident_Management__c currentIncidentManagementRecord : [Select Id,Name From Incident_Management__c Where AVC_Id__c = 'AVC12345677121'])
        {
            responseSLA = new SLA__c();
            responseSLA.Incident__c = currentIncidentManagementRecord.Id;
            responseSLA.SLAType__c = 'Incident Response Time';
            responseSLA.slaStatus__c = 'In Process';
            responseSLA.slaPauseStartTime__c = datetime.newInstance(2013, 9, 15, 13, 30, 0);
            lstSLA.add(responseSLA);
        }
        Insert lstSLA;
        Test.startTest();
            Delete lstSLA;
            Undelete lstSLA;
        Test.stopTest();
        //Assertion Section
        /*List<SLA__c> lstCurrentSLARecords = [Select Id,Name From SLA__c Where Current_SLA__c = true];
        System.AssertEquals(lstSLA.size(),lstCurrentSLARecords.size());
        List<Incident_Management__c> lstSLATaggedIncidents = [Select Id,Name From Incident_Management__c Where Current_SLA__c in :lstSLA];
        System.AssertEquals(10,lstSLATaggedIncidents.size());*/
    }
    @isTest static void SlatriggerHandlerException()
    {
        List<SLA__C> lstSLA = new List<SLA__C>();
        SLA__C responseSLA;
        List<SLA__C> lstUpdateResolutionSLA = new List<SLA__C>();
        for(Incident_Management__c currentIncidentManagementRecord : [Select Id,Name From Incident_Management__c Where AVC_Id__c = 'AVC12345677121'])
        {
            responseSLA = new SLA__c();
            responseSLA.Incident__c = currentIncidentManagementRecord.Id;
            responseSLA.SLAType__c = 'Incident Response Time';
            responseSLA.slaStatus__c = 'In Process';
            responseSLA.slaPauseStartTime__c = datetime.newInstance(2013, 9, 15, 13, 30, 0);
            lstSLA.add(responseSLA);
        }
        Test.startTest();
            SLATriggerHandler.throwException = true;
            Insert lstSLA;
            SLATriggerHandler.throwException = false;
        Test.stopTest();
    }
}