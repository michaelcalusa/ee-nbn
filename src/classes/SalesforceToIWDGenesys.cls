public class SalesforceToIWDGenesys {

    

    public class SalesforceToIWDGenesysWrapper {
        public String caseOwner; 
        public String caseNumber;
        public String caseRecordType;
        public String channel;   
        public String status;    
        public String priority;  
        public String toEmailAddress;    
        public String fromEmailAddressDomain;    
        public DateTime lastModifiedDate;  
        public String hseImpact; 
        public String tioLevel;  
        public String escalationSource;
        
        public String transactionId;        
        public Long timeStamp;
        public String emailAddress;
        public String contact;
        public String primaryContactRole;
        public String subject;
        public String phase;
        public String category;
        public String subcategory;
        public String description;
        public String smeTeam;
        public String tioFlag;
        public String caseMilestone;
        public String lastModifiedBy;
        public String recordId;
        public String lastModifiedChannel;
            
       
    }

    
    public static SalesforceToIWDGenesys parse(String json) {
        return (SalesforceToIWDGenesys) System.JSON.deserialize(json, SalesforceToIWDGenesys.class);
    }
}