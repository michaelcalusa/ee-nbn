public class SalesforceToIWDGenesysProcessor {

    public static void buildSalesforceToIWDGenesysJSONPayload(List<Case> caseFromEmailList,boolean isCaseUpdated) {
         System.debug('**********SalesforceToIWDGenesysProcessor***************'+caseFromEmailList);
          System.debug('**********isCaseUpdated***************'+isCaseUpdated);
        String guuid = '';
        guuid = IntegrationUtility.newGuid(guuid);
           List<Id> caseIdList = new List<Id>();
for(case caseRecord: caseFromEmailList){
    caseIdList.add(caseRecord.id);
}

Map<String,Case> caseDetialsByCaseIdMap = new Map<String,Case>([select id,LastModifiedBy.name,recordtype.name from case where Id =: caseIdList]);
System.debug('**********caseDetialsByCaseIdMap ***************'+caseDetialsByCaseIdMap );
         List<IWDGenesysOutboundIntegration__e> iwdGenesysOutboundIntegrationList = new List<IWDGenesysOutboundIntegration__e>();
        for(Case caseRecord:caseFromEmailList) {
            SalesforceToIWDGenesys.SalesforceToIWDGenesysWrapper salesforceToIWDGenesysWrapperObj = new SalesforceToIWDGenesys.SalesforceToIWDGenesysWrapper();
            
            // salesforceToIWDGenesysWrapperObj.timeStamp = '';
            //salesforceToIWDGenesysWrapperObj.emailAddress = '';
            //salesforceToIWDGenesysWrapperObj.contact = caseRecord.ContactEmail;
            salesforceToIWDGenesysWrapperObj.primaryContactRole= caseRecord.Primary_Contact_Role__c;
            //salesforceToIWDGenesysWrapperObj.subject = caseRecord.Subject;
            //salesforceToIWDGenesysWrapperObj.phase = caseRecord.Phase__c;
            //salesforceToIWDGenesysWrapperObj.category = caseRecord.Category__c;
            //salesforceToIWDGenesysWrapperObj.subcategory = caseRecord.Sub_Category__c;
            //salesforceToIWDGenesysWrapperObj.description = caseRecord.Description;
            //salesforceToIWDGenesysWrapperObj.caseOwner  = caseRecord.ownerID;
             //salesforceToIWDGenesysWrapperObj.isCaseUpdated = isCaseUpdated;
            //salesforceToIWDGenesysWrapperObj.priority   = caseRecord.Priority; 
            //salesforceToIWDGenesysWrapperObj.toEmailAddress  = caseRecord.SuppliedEmail;
           // salesforceToIWDGenesysWrapperObj.fromEmailAddressDomain  = caseRecord.;
           //salesforceToIWDGenesysWrapperObj.fromEmailAddressDomain  = '';
            //salesforceToIWDGenesysWrapperObj.tioFlag = '';
            
            String lastModifiedByName = ((case)caseDetialsByCaseIdMap.get(caseRecord.Id)).LastModifiedBy.name;
            String recordTypeByName = ((case)caseDetialsByCaseIdMap.get(caseRecord.Id)).recordtype.name;
            salesforceToIWDGenesysWrapperObj.timeStamp = (caseRecord.LastModifiedDate).getTime()/1000;
            salesforceToIWDGenesysWrapperObj.caseNumber  = caseRecord.CaseNumber;
            salesforceToIWDGenesysWrapperObj.caseRecordType  = recordTypeByName;
            salesforceToIWDGenesysWrapperObj.channel  = caseRecord.Origin; 
            salesforceToIWDGenesysWrapperObj.status  = caseRecord.Status;
            salesforceToIWDGenesysWrapperObj.lastModifiedDate = caseRecord.LastModifiedDate;//Request Received Date
           // salesforceToIWDGenesysWrapperObj.hseImpact  = caseRecord.HSE_Impact__c; 
           salesforceToIWDGenesysWrapperObj.hseImpact  = 'No'; 
            salesforceToIWDGenesysWrapperObj.tioLevel   = caseRecord.TIO__c; 
            salesforceToIWDGenesysWrapperObj.escalationSource = caseRecord.Escalation_Source__c;
           // salesforceToIWDGenesysWrapperObj.smeTeam = caseRecord.SME_Team__c;
            
            if(caseRecord.LastModifiedChannel__c ==  null){
                salesforceToIWDGenesysWrapperObj.lastModifiedChannel= caseRecord.Origin;
            }else{
                salesforceToIWDGenesysWrapperObj.lastModifiedChannel= caseRecord.LastModifiedChannel__c;
            }
             salesforceToIWDGenesysWrapperObj.phase = caseRecord.Phase__c;
            salesforceToIWDGenesysWrapperObj.category = caseRecord.Category__c;
            salesforceToIWDGenesysWrapperObj.subcategory = caseRecord.Sub_Category__c;
               
               
            salesforceToIWDGenesysWrapperObj.transactionId = guuid;           
            salesforceToIWDGenesysWrapperObj.recordId= caseRecord.id;
            System.debug('*************Name************'+lastModifiedByName );
            salesforceToIWDGenesysWrapperObj.lastModifiedBy = lastModifiedByName;
            
            if(isCaseUpdated) {
                salesforceToIWDGenesysWrapperObj.caseMilestone = 'update';
            }else{
                salesforceToIWDGenesysWrapperObj.caseMilestone = 'Submit';
            }
    
            String jason = JSON.serialize(salesforceToIWDGenesysWrapperObj, true);
              System.debug('**********jason***************'+jason);
            System.debug('jason--->' + jason);
            IWDGenesysOutboundIntegration__e iwdOutboundRecord = new IWDGenesysOutboundIntegration__e();
            iwdOutboundRecord.OutboundJSONMessage__c = jason;

            iwdGenesysOutboundIntegrationList.add(iwdOutboundRecord);
            
        }
       EventBus.publish(iwdGenesysOutboundIntegrationList);
    }
}