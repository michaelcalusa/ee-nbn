@isTest
public class SalesforceToIWDProcessor_Test {
    
    @testSetup static void setUpData() {
        id queryRecTypID = Schema.SObjectType.case.getRecordTypeInfosByName().get('Query').getRecordTypeId();
     
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId();
        insert testAccount;
        
        Contact cont = new Contact();
        cont.FirstName='Test';
        cont.LastName='Test';
        cont.Email='t@t.com';
        cont.Accountid= testAccount.id;
        cont.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('External Contact').getRecordTypeId();
        insert cont;
        
        Case caseRecord = new Case();
        caseRecord.Origin = 'Email'; 
        caseRecord.Status = 'Re-Opened';
        //caseRecord.LastModifiedDate=system.now();
        caseRecord.ContactId = cont.id;
        caseRecord.RecordTypeId = queryRecTypID ;
        caseRecord.TIO_Level__c='TIO L1'; 
        caseRecord.Primary_Contact_Role__c = 'Contractor';
       caseRecord.LastModifiedChannel__c='Service Portal';
        
        caseRecord.Phase__c='Aware';
        caseRecord.Category__c = 'Aware';
        caseRecord.Sub_Category__c='Availability/Rollout/Timeframe';
        
        insert caseRecord;
         
    }
    
    static testMethod void testJSONProcessing(){
        Case caseObj = [select id, Origin , Status , LastModifiedDate, HSE_Impact__c ,RecordTypeId , TIO_Level__c, LastModifiedChannel__c,
         Phase__c, Category__c ,Sub_Category__c from case limit 1];
     caseObj.HSE_Impact__c = 'No';
        test.startTest();
          update caseObj ;
        test.stopTest();
    }
}