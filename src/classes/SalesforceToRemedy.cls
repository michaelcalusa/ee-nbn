public class SalesforceToRemedy {

    public class Before {
        public String assignedGroup;
        public String assignedUser;
        public String industryStatus;
        public String industrySubStatus;
        public String status;
        public String company;
        public String department;
        public String federationId;
        public String statusReason;
        public String summary;
        public String notesRSP;
        public String incidentStatus;
        //Added as part of resolve incident 
        public String resolutionCategoryTier1;
        public String resolutionCategoryTier2;
        public String resolutionCategoryTier3;
        public String resolutionNote;
        public String pendingReasonCode;
        public String requestSubject;
        public String declineReason;
        public String declineComment;
        public String resolutionRejectionAcceptReason;
        public String resolutionRejectionAcceptComment;
        public String endUserEngagementType;
        public String rspResolutionRejectionReason;
        public String rspResolutionRejectionComment;
        public String requestDescription;
        public String plannedRemediationDate;
        public String appointmentId;
		public String pendingReasonResponseType;
        public String opCat1;
        public String opCat2;
        public String opCat3;
        
        
    }
    public class relatedItemDataWrapper {
        public String relatedItemIdentifier;
        public String relatedItemType;
        public String relationshipType;
    }
    public class worklogDataWrapper{
        public String worklogDetails;
        public String worklogInternalExternal;
    }
    
    public class afterWorklogWrapper{
        public worklogDataWrapper worklogData;
    }
    public class IncidentData {
        public Before before;
        public Before after;
        public afterWorklogWrapper afterWorklog;
		// START - CUSTSA-28544 Add salesforceRecordId to send Incident recordId to IWD 
        public String salesforceRecordId;
    }
    public class RequestInformationData {
        public String reasonCode;
        public String pendingType;
    }

    public class SalesforceToRemedyWrapper {
        public String incidentIdentifier;
        public String action;
        public String transactionId;
        public String salesForceUser;
        public String timeStamp;
        public IncidentData incidentData;
        //Included as part SINI Linking operation.
        public relatedItemDataWrapper relatedItemData;
        //Done for US 10451 - Gaurav Tiwari
        //public RequestInformationData requestInformationData;
    }

    
    public static SalesforceToRemedy parse(String json) {
        return (SalesforceToRemedy) System.JSON.deserialize(json, SalesforceToRemedy.class);
    }
	
	
}