/*------------------------------------------------------------
Author:         Kashyap Murthy
Company:        NbnCo
Description:    This class is used in IncidentManagementTriggerHandler to send/publish
                changes made to Incident Management records to Remedy.
                
Inputs:         
Test Class:     
History
<Date>          <Authors Name>     <Brief Description of Change>
21/02/2018      Kashyap Murthy      Created trigger. 
08/03/2018     Rohit Mathur        Added comments to the class.
------------------------------------------------------------*/
public class SalesforceToRemedyProcessor 
{
    //Variables used for constructing the json message.
    public static string federationId;
    private static Map<string,QueueCompanyDepartmentMapping__c> queueCompanyDepartmentMap;
    private static Map<string,SaleforceToRemedyAfterValue__mdt> SaleforceToRemedyAfterValueListMap;
    private static List<string> lstUserActions;
    private static List<string> lstAfterStatusUserActions;
    static
    {
        //Set the actions for which RPA action needs to be triggered from custom label.
        lstUserActions = Label.RPAOperatorActions.split(';');
        //Set the actions for which after status values needs to be populated from custom metadata.
        lstAfterStatusUserActions = Label.AfterStatusUserActions.split(';');
        //Intialize queueCompanyDepartmentMaP from Custom settings with queuename as key.
        queueCompanyDepartmentMap = new Map<string,QueueCompanyDepartmentMapping__c>();
        Map<string,QueueCompanyDepartmentMapping__c> companyDepartmentInfoMap = QueueCompanyDepartmentMapping__c.getAll();
        QueueCompanyDepartmentMapping__c queueCompanyDepartmentInfo;
        for (String key : companyDepartmentInfoMap.keySet()) 
        {
            queueCompanyDepartmentInfo = companyDepartmentInfoMap.get(key);
            if(!queueCompanyDepartmentMap.keySet().contains(queueCompanyDepartmentInfo.QueueName__c))
            {
                queueCompanyDepartmentMap.put(queueCompanyDepartmentInfo.QueueName__c,queueCompanyDepartmentInfo);
            }
        }
        //Intialize SaleforceToRemedyAfterValueListMap from custom metadate with action name as key.
        SaleforceToRemedyAfterValueListMap = new Map<string,SaleforceToRemedyAfterValue__mdt>();
        SaleforceToRemedyAfterValue__mdt[] lstSaleforceToRemedyAfterValue = [Select Id,Incident_Status__c,Industry_Status__c,Industry_Sub_Status__c,User_Action__c,Status_Reason__c,End_User_Engagement_Type__c,Request_Subject__c,Pending_Reason_Code__c from SaleforceToRemedyAfterValue__mdt];
        for(SaleforceToRemedyAfterValue__mdt salesforceToRemedyAfterValue : lstSaleforceToRemedyAfterValue)
        {
            if(!SaleforceToRemedyAfterValueListMap.keyset().Contains(salesforceToRemedyAfterValue.User_Action__c))
            {
                SaleforceToRemedyAfterValueListMap.put(salesforceToRemedyAfterValue.User_Action__c,salesforceToRemedyAfterValue);
            }
        }
    }
    public static String buildSalesforceToRemedyJSONPayload(Incident_Management__c oldIncidentManagement, Incident_Management__c newIncidentManagement) 
    {
        QueueCompanyDepartmentMapping__c queueCompanyDepartmentInfo;
        SaleforceToRemedyAfterValue__mdt SaleforceToRemedyAfterValueOBJ;
        DateTime plannedRemediationDateTime;
        Long plannedRemediationDateInSeconds;
        SalesforceToRemedy.SalesforceToRemedyWrapper salesforceToRemedyWrapper = new SalesforceToRemedy.SalesforceToRemedyWrapper();
        salesforceToRemedyWrapper.incidentIdentifier = oldIncidentManagement.Incident_Number__c;
        system.debug('SalesforceToRemedyProcessor.buildSalesforceToRemedyJSONPayload inc num in SalesforceToRemedyProcessor : '+salesforceToRemedyWrapper.incidentIdentifier);
        salesforceToRemedyWrapper.action = newIncidentManagement.User_Action__c;
        salesforceToRemedyWrapper.transactionId = IntegrationUtility.newGuid('');
        salesforceToRemedyWrapper.salesForceUser = newIncidentManagement.Last_modified_By__c;
        salesforceToRemedyWrapper.timeStamp = String.valueOf((newIncidentManagement.LastModifiedDate.getTime()) / 1000);
        if(newIncidentManagement.User_Action__c.Equals('User Assigned') || lstUserActions.Contains(newIncidentManagement.User_Action__c)) 
        {
            //Set Before Parameters for JSON.
            SalesforceToRemedy.Before before = new SalesforceToRemedy.Before();
            if (String.isNotBlank(oldIncidentManagement.Assigned_Group__c) && newIncidentManagement.User_Action__c != 'requestNewAppoinmentFromRSP' && newIncidentManagement.User_Action__c != 'updateOperationCategorisation') 
            {
                before.assignedGroup = oldIncidentManagement.Assigned_Group__c;
            }
            if (String.isNotBlank(oldIncidentManagement.assignee__c) && newIncidentManagement.User_Action__c != 'requestNewAppoinmentFromRSP') 
            {
                before.assignedUser = oldIncidentManagement.assignee__c;
            }
            if (String.isNotBlank(oldIncidentManagement.Industry_Status__c)) 
            {
                before.industryStatus = oldIncidentManagement.Industry_Status__c;
            }
            if (String.isNotBlank(oldIncidentManagement.Industry_Sub_Status__c) && newIncidentManagement.User_Action__c != 'rspAcceptResolution' && newIncidentManagement.User_Action__c != 'rspRejectResolution') 
            {
                before.industrySubStatus = oldIncidentManagement.Industry_Sub_Status__c;
            }
            if (String.isNotBlank(oldIncidentManagement.Incident_Status__c)) 
            {
                before.status = oldIncidentManagement.Incident_Status__c;
            }
            if (String.isNotBlank(oldIncidentManagement.Status_Reason__c) && newIncidentManagement.User_Action__c != 'acceptAndAssign'  
                && newIncidentManagement.User_Action__c != 'requestNewAppoinmentFromRSP'
                && newIncidentManagement.User_Action__c != 'rspAcceptResolution' && newIncidentManagement.User_Action__c != 'rspRejectResolution' && newIncidentManagement.User_Action__c != 'ASRespondedToInfoRequested'
                && newIncidentManagement.User_Action__c != 'updateOperationCategorisation') 
            {
                before.statusReason = oldIncidentManagement.Status_Reason__c;
            }
            if (String.isNotBlank(oldIncidentManagement.summary__c) && newIncidentManagement.User_Action__c != 'acceptAndAssign' 
                && newIncidentManagement.User_Action__c != 'requestNewAppoinmentFromRSP'
                && newIncidentManagement.User_Action__c != 'rspAcceptResolution' && newIncidentManagement.User_Action__c != 'rspRejectResolution' && newIncidentManagement.User_Action__c != 'ASRespondedToInfoRequested') 
            {
                before.summary = oldIncidentManagement.summary__c;
            }
            //Set after Parameters for JSON.
            SalesforceToRemedy.Before after = new SalesforceToRemedy.Before();
            //set Company and Department attributes
            queueCompanyDepartmentInfo = queueCompanyDepartmentMap.get(newIncidentManagement.Assigned_Group__c);
            if (queueCompanyDepartmentInfo != Null && String.isNotBlank(queueCompanyDepartmentInfo.Company__c) && newIncidentManagement.User_Action__c != 'updateOperationCategorisation') 
            {
                after.company = queueCompanyDepartmentInfo.Company__c;
            }
            if (queueCompanyDepartmentInfo != Null && String.isNotBlank(queueCompanyDepartmentInfo.department__c) && newIncidentManagement.User_Action__c != 'updateOperationCategorisation') 
            {
                after.department = queueCompanyDepartmentInfo.department__c;
            }
            // Added By Gaurav Saraswat to pick federation id from global variabl
            if (String.isNotBlank(federationId) && newIncidentManagement.User_Action__c != 'reassignIncident' && newIncidentManagement.User_Action__c != 'requestNewAppoinmentFromRSP' && newIncidentManagement.User_Action__c != 'updateOperationCategorisation') 
            {
                after.federationId = federationId;
            }
            if (String.isNotBlank(newIncidentManagement.Assigned_Group__c) && newIncidentManagement.User_Action__c != 'requestNewAppoinmentFromRSP' && newIncidentManagement.User_Action__c != 'updateOperationCategorisation') 
            {
                after.assignedGroup = newIncidentManagement.Assigned_Group__c;
            }
            if (String.isNotBlank(newIncidentManagement.assignee__c) && newIncidentManagement.User_Action__c != 'requestNewAppoinmentFromRSP' && newIncidentManagement.User_Action__c != 'updateOperationCategorisation') 
            {
                after.assignedUser = newIncidentManagement.assignee__c;
            }
            if(!lstAfterStatusUserActions.Contains(newIncidentManagement.User_Action__c))
            {
                if (String.isNotBlank(newIncidentManagement.Industry_Status__c)) 
                {
                    after.industryStatus = newIncidentManagement.Industry_Status__c;
                }
                if (String.isNotBlank(newIncidentManagement.Industry_Sub_Status__c)) 
                {
                    after.industrySubStatus = newIncidentManagement.Industry_Sub_Status__c;
                }
                if (String.isNotBlank(newIncidentManagement.Incident_Status__c)) 
                {
                    after.status = newIncidentManagement.Incident_Status__c;
                }
            }
            else
            {
                SaleforceToRemedyAfterValueOBJ = SaleforceToRemedyAfterValueListMap.get(newIncidentManagement.User_Action__c);
                if(SaleforceToRemedyAfterValueOBJ != Null)
                {
                    if (String.isNotBlank(SaleforceToRemedyAfterValueOBJ.Industry_Status__c)) 
                    {
                        after.industryStatus = SaleforceToRemedyAfterValueOBJ.Industry_Status__c;
                    }
                    if (String.isNotBlank(SaleforceToRemedyAfterValueOBJ.Industry_Sub_Status__c)) 
                    {
                        after.industrySubStatus = SaleforceToRemedyAfterValueOBJ.Industry_Sub_Status__c;
                    }
                    if (String.isNotBlank(SaleforceToRemedyAfterValueOBJ.Incident_Status__c)) 
                    {
                        after.status = SaleforceToRemedyAfterValueOBJ.Incident_Status__c;
                    }
                    if (String.isNotBlank(SaleforceToRemedyAfterValueOBJ.Status_Reason__c)) 
                    {
                        after.statusReason = SaleforceToRemedyAfterValueOBJ.Status_Reason__c;
                    }
                    if (String.isNotBlank(SaleforceToRemedyAfterValueOBJ.End_User_Engagement_Type__c)) 
                    {
                        after.endUserEngagementType = SaleforceToRemedyAfterValueOBJ.End_User_Engagement_Type__c;
                    }
                    if (String.isNotBlank(SaleforceToRemedyAfterValueOBJ.Pending_Reason_Code__c)) 
                    {
                        after.pendingReasonCode = SaleforceToRemedyAfterValueOBJ.Pending_Reason_Code__c;
                    }
                    if (String.isNotBlank(SaleforceToRemedyAfterValueOBJ.Request_Subject__c)) 
                    {
                        after.requestSubject = SaleforceToRemedyAfterValueOBJ.Request_Subject__c;
                    }
                }
            }
            if (String.isNotBlank(newIncidentManagement.Status_Reason__c) && newIncidentManagement.User_Action__c != 'acceptAndAssign' 
                && newIncidentManagement.User_Action__c != 'requestNewAppoinmentFromRSP' && newIncidentManagement.User_Action__c != 'resolveIncident'
                && newIncidentManagement.User_Action__c != 'rspAcceptResolution' && newIncidentManagement.User_Action__c != 'rspRejectResolution'
                && newIncidentManagement.User_Action__c != 'nbnOutOfHeld' && newIncidentManagement.User_Action__c != 'nbnHeld'
                && newIncidentManagement.User_Action__c != 'changePRD' && newIncidentManagement.User_Action__c != 'ASRespondedToInfoRequested'
                ) 
            {
                after.statusReason = newIncidentManagement.Status_Reason__c;
            }
            if (String.isNotBlank(newIncidentManagement.summary__c) && newIncidentManagement.User_Action__c != 'acceptAndAssign' 
                && newIncidentManagement.User_Action__c != 'requestNewAppoinmentFromRSP' 
                && newIncidentManagement.User_Action__c != 'rspAcceptResolution' && newIncidentManagement.User_Action__c != 'rspRejectResolution'
                && newIncidentManagement.User_Action__c != 'nbnOutOfHeld' && newIncidentManagement.User_Action__c != 'nbnHeld'
                && newIncidentManagement.User_Action__c != 'changePRD' && newIncidentManagement.User_Action__c != 'ASRespondedToInfoRequested'
                && newIncidentManagement.User_Action__c != 'updateOperationCategorisation' 
               ) 
            {
                after.summary = newIncidentManagement.summary__c;
            }
            if(newIncidentManagement.User_Action__c == 'rspRejectResolution')
            {
                after.rspResolutionRejectionReason = newIncidentManagement.DeclineReason__c;
                after.rspResolutionRejectionComment = newIncidentManagement.DeclineComment__c;
            }
            SalesforceToRemedy.IncidentData incidentData = new SalesforceToRemedy.IncidentData();
            incidentData.before = before;
            incidentData.after = after;
            salesforceToRemedyWrapper.incidentData = incidentData;
        }
        if(newIncidentManagement.User_Action__c.equals('relateToIncident'))
        {
            SalesforceToRemedy.relatedItemDataWrapper relatedItemDataObject = new SalesforceToRemedy.relatedItemDataWrapper();
            relatedItemDataObject.relatedItemType = 'Incident';
            relatedItemDataObject.relationshipType = 'Related to';
            List<String> relatedNetworkIncidents = newIncidentManagement.Network_Incidents_For_Linking__c.split(';');
            relatedItemDataObject.relatedItemIdentifier = relatedNetworkIncidents[0];
            salesforceToRemedyWrapper.relatedItemData = relatedItemDataObject;
        }
        if(newIncidentManagement.User_Action__c == 'addNote')
        {
            SalesforceToRemedy.worklogDataWrapper wlDataObj = new SalesforceToRemedy.worklogDataWrapper();
            if(newIncidentManagement.Add_note_internal__c != oldIncidentManagement.Add_note_internal__c)
            {
                wlDataObj.worklogDetails = newIncidentManagement.Add_note_internal__c;
                wlDataObj.worklogInternalExternal = 'internal';
            }
            else if(newIncidentManagement.Add_note_external__c != oldIncidentManagement.Add_note_external__c)
            {
                wlDataObj.worklogDetails = newIncidentManagement.Add_note_external__c;
                wlDataObj.worklogInternalExternal = 'external';  
            }
            SalesforceToRemedy.afterWorklogWrapper afterWLData = new SalesforceToRemedy.afterWorklogWrapper();
            afterWLData.worklogData = wlDataObj;
            SalesforceToRemedy.IncidentData incidentData = new SalesforceToRemedy.IncidentData();
            incidentData.afterWorklog = afterWLData;
            salesforceToRemedyWrapper.incidentData = incidentData;
        }
        if(newIncidentManagement.User_Action__c == 'requestInffromRSP' || newIncidentManagement.User_Action__c == 'nbnOutOfHeld')
        {
           salesforceToRemedyWrapper.incidentData.after.notesRSP = newIncidentManagement.Add_note_external__c;
        }
        if(newIncidentManagement.User_Action__c == 'nbnHeld' || newIncidentManagement.User_Action__c == 'changePRD')
        {
            salesforceToRemedyWrapper.incidentData.after.notesRSP = newIncidentManagement.Add_note_external__c;
            if(newIncidentManagement.Planned_Remediation_Date__c != Null)
            {
                plannedRemediationDateInSeconds = (newIncidentManagement.Planned_Remediation_Date__c.getTime()) / 1000;
                salesforceToRemedyWrapper.incidentData.after.plannedRemediationDate = String.valueOf(plannedRemediationDateInSeconds);
            }
            if(newIncidentManagement.User_Action__c == 'nbnHeld' && String.isNotBlank(newIncidentManagement.asRequestDescription__c))
                salesforceToRemedyWrapper.incidentData.after.requestDescription = newIncidentManagement.asRequestDescription__c;
        }
        if(newIncidentManagement.User_Action__c == 'ASRespondedToInfoRequested')
        {
            salesforceToRemedyWrapper.incidentData.after.notesRSP = newIncidentManagement.Add_note_external__c;
            // Add code ref CUSTSA-16671 - RSP responded for New Appointment
            // Commented below code and put in else below.
            /* if(String.isNotBlank(newIncidentManagement.RSP_Appointment_Id__c))
            {
                salesforceToRemedyWrapper.incidentData.after.appointmentId = newIncidentManagement.RSP_Appointment_Id__c;
            } */
           if(String.isNotBlank(newIncidentManagement.RSP_Appointment_Id__c) && newIncidentManagement.RSP_Appointment_Id__c.contains('-'))
            {
                String [] strApptWithReasonType = newIncidentManagement.RSP_Appointment_Id__c.split('-');
                System.debug('new appt passed value at 0 ' + strApptWithReasonType[0]);
                System.debug('new appt passed value at 1 ' + strApptWithReasonType[1]);
                salesforceToRemedyWrapper.incidentData.after.appointmentId = strApptWithReasonType[0];
                salesforceToRemedyWrapper.incidentData.after.pendingReasonResponseType = strApptWithReasonType[1];
            }
            else if(String.isNotBlank(newIncidentManagement.RSP_Appointment_Id__c))
            {
                salesforceToRemedyWrapper.incidentData.after.appointmentId = newIncidentManagement.RSP_Appointment_Id__c;
            }
        }
        if(newIncidentManagement.User_Action__c == 'resolveIncident')
        {
            salesforceToRemedyWrapper.incidentData.before.resolutionCategoryTier1 = oldIncidentManagement.ResolutionCategoryTier1__c;
            salesforceToRemedyWrapper.incidentData.before.resolutionCategoryTier2 = oldIncidentManagement.ResolutionCategoryTier2__c;
            salesforceToRemedyWrapper.incidentData.before.resolutionCategoryTier3 = oldIncidentManagement.ResolutionCategoryTier3__c;
            salesforceToRemedyWrapper.incidentData.before.notesRSP = oldIncidentManagement.Add_note_external__c;
            SalesforceToRemedyWrapper.incidentData.before.resolutionNote = oldIncidentManagement.Add_note_internal__c;
            salesforceToRemedyWrapper.incidentData.after.resolutionCategoryTier1 = newIncidentManagement.ResolutionCategoryTier1__c;
            salesforceToRemedyWrapper.incidentData.after.resolutionCategoryTier2 = newIncidentManagement.ResolutionCategoryTier2__c;
            salesforceToRemedyWrapper.incidentData.after.resolutionCategoryTier3 = newIncidentManagement.ResolutionCategoryTier3__c;
            salesforceToRemedyWrapper.incidentData.after.notesRSP = newIncidentManagement.Add_note_external__c;
            salesforceToRemedyWrapper.incidentData.after.resolutionNote = newIncidentManagement.Add_note_internal__c;
        }
        if(newIncidentManagement.User_Action__c == 'requestNewAppoinmentFromRSP')
        {   
            salesforceToRemedyWrapper.incidentData.after.notesRSP = newIncidentManagement.Add_note_external__c;    
            salesforceToRemedyWrapper.incidentData.after.pendingReasonCode = newIncidentManagement.reasonDescription__c;  
			
			// CUSTSA-27098 - Change below if condition. Add condition as per story
			if(newIncidentManagement.Current_Status__c == 'Tech Offsite' && newIncidentManagement.AppointmentId__c != ''
               && newIncidentManagement.Appointment_Status__c != '' && newIncidentManagement.Appointment_Status__c.toUpperCase() == 'INCOMPLETE' 
               && newIncidentManagement.Current_work_order_status__c.toUpperCase() == 'PENDING' )
			/* if((newIncidentManagement.Current_Status__c == 'Tech Offsite' && newIncidentManagement.AppointmentId__c != ''
               && newIncidentManagement.Appointment_Status__c != '' && newIncidentManagement.Appointment_Status__c.toUpperCase() == 'INCOMPLETE' 
               && newIncidentManagement.Current_work_order_status__c.toUpperCase() == 'PENDING') ||
				(newIncidentManagement.Current_Status__c == 'In Progress' && newIncidentManagement.Appointment_Status__c != '' && 
                newIncidentManagement.Appointment_Status__c.toUpperCase() == 'RESERVED' && newIncidentManagement.Engagement_Type__c.toUpperCase() == 'APPOINTMENT'))*/
			   {
                salesforceToRemedyWrapper.incidentData.after.requestSubject = 'Appointment Reschedule Required';   
            }else{
                salesforceToRemedyWrapper.incidentData.after.requestSubject = 'Appointment Required';
            }
        }
        if(newIncidentManagement.User_Action__c == 'acceptIncident' || newIncidentManagement.User_Action__c == 'changeEUETAppointment')
        {            
            salesforceToRemedyWrapper.incidentData.after.notesRSP = newIncidentManagement.Add_note_external__c;    
        }
        if(newIncidentManagement.User_Action__c == 'acceptResolutionRejection' || newIncidentManagement.User_Action__c == 'declineResolutionRejection')
        {
            if(newIncidentManagement.User_Action__c == 'acceptResolutionRejection')
            {
                //MR1812/CUSTSA-25529 --Start
                string acceptReasonValue = newIncidentManagement.Jigsaw_Accept_Reason__c;
                string acceptfirstHalf = acceptReasonValue.substringBefore('(').trim();
                string acceptsecondHalf = acceptReasonValue.substringAfter('(');
                string acceptsecondHalfFinal = acceptsecondHalf.substringBefore(')').trim();
                String acceptfinaldeclineReasonValue = acceptsecondHalfFinal + 'Accepted - '+ acceptfirstHalf;
                salesforceToRemedyWrapper.incidentData.after.resolutionRejectionAcceptReason =  + acceptfinaldeclineReasonValue; 
                salesforceToRemedyWrapper.incidentData.after.resolutionRejectionAcceptComment = newIncidentManagement.Jigsaw_Accept_Comment__c;    
                //MR1812/CUSTSA-25529 --End     
            }            
            else if(newIncidentManagement.User_Action__c == 'declineResolutionRejection')
            {                
                string declineReasonValue = newIncidentManagement.DeclineReason__c;
                string firstHalf = declineReasonValue.substringBefore('(').trim();
                string secondHalf = declineReasonValue.substringAfter('(');
                string secondHalfFinal = secondHalf.substringBefore(')').trim();
                String finaldeclineReasonValue = secondHalfFinal + ' - '+ firstHalf;
                salesforceToRemedyWrapper.incidentData.after.declineReason = finaldeclineReasonValue; 
                salesforceToRemedyWrapper.incidentData.after.declineComment = newIncidentManagement.DeclineComment__c;                   
            }    
        }
        // CUSTSA-16673
        if(newIncidentManagement.User_Action__c == 'updateOperationCategorisation'){
            if(String.isNotBlank(newIncidentManagement.Op_Cat_Inflight_Changes__c)){
                List<String> lst = newIncidentManagement.Op_Cat_Inflight_Changes__c.split(';');
                if(lst.size() > 1){
                    if(String.isNotBlank(lst.get(0)))
                        salesforceToRemedyWrapper.incidentData.after.opCat1 = lst.get(0);
                    if(String.isNotBlank(lst.get(1)))
                        salesforceToRemedyWrapper.incidentData.after.opCat2 = lst.get(1);
                    // CUSTSA-31682
                    if((newIncidentManagement.Prod_Cat__c == 'NHAS' || newIncidentManagement.Prod_Cat__c == 'NHUR') && lst.size() == 3)
                        salesforceToRemedyWrapper.incidentData.after.opCat3 = lst.get(2);
                }
                
            }
        }
        String jason = JSON.serialize(salesforceToRemedyWrapper, true);
        System.debug('SalesforceToRemedyProcessor.buildSalesforceToRemedyJSONPayload jason :' + jason);
        return jason;
    }
	
	// START CUSTSA-28544 Send Incident RecordId to Genesys IWD
    public static String buildSalesforceRecordIdPayload( Incident_Management__c newIncidentManagement) 
    {
        SalesforceToRemedy.SalesforceToRemedyWrapper salesforceToRemedyWrapper = new SalesforceToRemedy.SalesforceToRemedyWrapper();
        salesforceToRemedyWrapper.incidentIdentifier = newIncidentManagement.Incident_Number__c;
        salesforceToRemedyWrapper.action = 'updateSalesforceRecordId';
        salesforceToRemedyWrapper.transactionId = IntegrationUtility.newGuid('');
        salesforceToRemedyWrapper.salesForceUser = newIncidentManagement.Last_modified_By__c;
        salesforceToRemedyWrapper.timeStamp = String.valueOf((newIncidentManagement.LastModifiedDate.getTime()) / 1000);
		
        SalesforceToRemedy.IncidentData incRecId = new SalesforceToRemedy.IncidentData();
        incRecId.salesforceRecordId = newIncidentManagement.id;
        salesforceToRemedyWrapper.incidentData = incRecId;
        
        String incRecIdJSON = JSON.serialize(salesforceToRemedyWrapper, true);
        System.debug('SalesforceToRemedyProcessor.buildSalesforceRecordIdPayload jason :--' + incRecIdJSON);
        return incRecIdJSON;
    }
    // END
}