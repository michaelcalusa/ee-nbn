@isTest
public class SalesforceToRemedyProcessor_Test {
    
    @testSetup static void setUpData() {
        ID recordTypeId = schema.sobjecttype.Incident_Management__c.getrecordtypeinfosbyname().get('Incident Management FTTN/B').getRecordTypeId();
        
        Incident_Management__c im = new Incident_Management__c();
        im.RecordTypeId = recordTypeId;
        im.assignee__c = 'ABC USER';
        im.Assigned_Group__c = 'CSC-HFC';
        im.Industry_Status__c = 'Acknowledged';
        im.Industry_Sub_Status__c = 'Pending';
        im.Incident_Status__c = 'Pending';
        im.Name = 'ABCD';
        im.Incident_Number__c = 'INC000006932587';
        
        insert im;
    }
    
    static testMethod void testJSONProcessing(){
        Incident_Management__c im = [select id, assignee__c, Assigned_Group__c, Industry_Status__c, Industry_Sub_Status__c, Incident_Status__c, name, Incident_Number__c, User_Action__c from Incident_Management__c limit 1];
        im.User_Action__c = 'User Assigned';
        im.assignee__c = 'Contract User Name';
        im.Assigned_Group__c = 'CSC-HFC';
        im.Jigsaw_Accept_Reason__c = 'Test';
        test.startTest();
          update im;
         /* im.User_Action__c = 'dispatchTechnician';
          im.summary__c  = 'testDispatch';
          update im;*/
          im.User_Action__c = 'addNote';
          im.Add_note_internal__c = 'testAddNote';
          update im;
          im.User_Action__c = 'requestInffromRSP';
          update im;
          im.User_Action__c = 'acceptAndAssign';
          update im;
          im.User_Action__c = 'acceptIncident';
          update im;
          im.User_Action__c = 'resolveIncident';
          update im;
          im.User_Action__c = 'requestNewAppoinmentFromRSP';
          update im;
          im.User_Action__c = 'changeEUETAppointment';
          update im;
           im.User_Action__c = 'changeEUETCommitment';
          update im;
          im.User_Action__c = 'acceptResolutionRejection';
          im.Add_note_internal__c = 'testAddNote';
          update im;
         
          im.User_Action__c = 'declineResolutionRejection';
          im.DeclineReason__c='tetsReason';
          im.DeclineComment__c ='testcomments ';
          update im;
          
         
           
        test.stopTest();
    }
    
    static testMethod void testparse(){
        String jsonStr='{ "name":"test"}';
        
        test.startTest();
        	SalesforceToRemedy.parse(jsonStr);
        test.stopTest();
    }
}