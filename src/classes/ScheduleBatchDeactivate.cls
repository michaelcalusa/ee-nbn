global class ScheduleBatchDeactivate implements Schedulable {

    global void execute(SchedulableContext sc) {

        BatchDeactivation userBatch = new BatchDeactivation();
         
        ID batchprocessid = Database.executeBatch(userBatch); 
      
    }
    
   
}