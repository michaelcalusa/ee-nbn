public class ScheduleCRMExt implements Schedulable
{
    public static void execute(SchedulableContext ctx)
    {
           if(CRM_Extraction_Job__c.getinstance('All Objects Extraction') <> null && CRM_Extraction_Job__c.getinstance('All Objects Extraction').on_off__c)
           {
               CronTrigger ct = [SELECT id, TimesTriggered,StartTime, NextFireTime,EndTime,State,CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger WHERE Id = :ctx.getTriggerId()];  
               Extraction_Job__c ej = new Extraction_Job__c();
               ej.Name = String.valueof(ct.ID);  // Extraction Job ID
               ej.Start_Time__c = System.now();
               //ej.End_Time__c = ct.EndTime; 
               ej.Extraction_Job_Name__c = ct.CronJobDetail.Name+System.now();
               ej.Status__c = 'Processing'; 
               ej.RecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Extraction_Job__c'  and name = 'Extraction Jobs'].Id;
               Database.SaveResult sResult = database.insert(ej,false);
               if(CRM_Extraction_Job__c.getinstance('Account Extraction') <> null && CRM_Extraction_Job__c.getinstance('Account Extraction').on_off__c)
               {
                    QueueCRMAcc.execute(sResult.getId());
               }
               if(CRM_Extraction_Job__c.getinstance('Contact Extraction') <> null && CRM_Extraction_Job__c.getinstance('Contact Extraction').on_off__c)
               {
                    QueueCRMCon.execute(sResult.getId());
               }
               if(CRM_Extraction_Job__c.getinstance('Development Extraction') <> null && CRM_Extraction_Job__c.getinstance('Development Extraction').on_off__c)
               {
                    QueueCRMDev.execute(sResult.getId());
               }
               if(CRM_Extraction_Job__c.getinstance('Stage Application Extraction') <> null && CRM_Extraction_Job__c.getinstance('Stage Application Extraction').on_off__c)
               {
                    QueueCRMSA.execute(sResult.getId());
               }
               if(CRM_Extraction_Job__c.getinstance('MAR Extraction') <> null && CRM_Extraction_Job__c.getinstance('MAR Extraction').on_off__c)
               {
                    QueueCRMMAR.execute(sResult.getId());
               }
               /*********************************************************************
               US#4118: Below lines have been commented to decommission P2P jobs.
               Author: Jairaj Jadhav
               Company: Wipro Technologies
               if(CRM_Extraction_Job__c.getinstance('MDU/CP Extraction') <> null && CRM_Extraction_Job__c.getinstance('MDU/CP Extraction').on_off__c)
               {
                    QueueCRMMDUCP.execute(sResult.getId());
               }
               if(CRM_Extraction_Job__c.getinstance('Communication Extraction') <> null && CRM_Extraction_Job__c.getinstance('Communication Extraction').on_off__c)
               {
                    QueueCRMComm.execute(sResult.getId());
               }
               *********************************************************************/
              if(CRM_Extraction_Job__c.getinstance('NTD Cases Extraction') != null && CRM_Extraction_Job__c.getinstance('NTD Cases Extraction').on_off__c){
                
                    QueueCRMCases.execute(sResult.getId());
               }
               if(CRM_Extraction_Job__c.getinstance('Intersection Table Extraction') <> null && CRM_Extraction_Job__c.getinstance('Intersection Table Extraction').on_off__c)
               {
                   QueueCRMJobs.execute(sResult.getId());
                   QueueCRMConRole.execute(sResult.getId());
               }
               
           }
    }
    
}