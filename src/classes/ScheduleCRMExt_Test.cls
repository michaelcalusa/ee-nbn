@isTest
public class ScheduleCRMExt_Test {
    static testmethod void ScheduleCRMExt_Test() {   
        // Custom Setting record creation
        TestDataUtility.getListOfCRMExtractionJobRecords(true);
        TestDataUtility.insertListOfBatchJobs(true, TestDataUtility.createExtractionJob(true));
        // Test ScheduleCRMExt Class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('ScheduleCRMExt_Test',CRON_EXP, new ScheduleCRMExt());
        Test.stopTest();
    }
}