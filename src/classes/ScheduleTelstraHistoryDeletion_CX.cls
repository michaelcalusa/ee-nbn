/***************************************************************************************************
Class Name:         ScheduleTelstraHistoryDeletion_CX 
Class Type:         Schedule Class 
Version:            1.0 
Created Date:       13 October 2017
Function:           This Schedule class is to invoke a batch class to purge 18 months post creation of Telstra Technician private data 
Input Parameters:   None 
Output Parameters:  None
Description:        Purging of Telstra Technician private data  of 18 months post creation
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Beau Anderson      13/10/2017      Created - Version 1.0 Refer CUSTSA-2564for Epic description
****************************************************************************************************/ 

public class ScheduleTelstraHistoryDeletion_CX implements schedulable
{
       public static void execute(SchedulableContext sc){
          BatchTelstraHistoryDeletion_CX telstraHistoryObj = new BatchTelstraHistoryDeletion_CX();
          ID batchprocessid = Database.executeBatch(telstraHistoryObj);  
       }
       
       public static void start() {
         //At 00:00 on day-of-month 1 in January.
         String scheduleTime = '0 0 0 25 11 ?';
         System.schedule('TelstraHistoryDeletion', scheduleTime ,new ScheduleTelstraHistoryDeletion_CX());
       }   
}