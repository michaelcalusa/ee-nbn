/***************************************************************************************************
Class Name:  SendDeliverableDocstoDeveloper
Version     : 1.0 
Created Date: 26-02-2018
Function    : This class is to generate and send PCC & Council letters
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Suman Gunaganti			26-02-2018               Created
* Rinu Kachirakkal          28-02-2018               Modified
* Krishna Sai               01-03-2018               Modified
****************************************************************************************************/
public class SendDeliverableDocstoDeveloper {
    Static Id taskRecTypId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(); 
    Static New_Dev_Platform_Events__c identifierList = New_Dev_Platform_Events__c.getOrgDefaults();
    Static Id pcctemplateId = [SELECT ID, DeveloperName,Name FROM EmailTemplate WHERE DeveloperName = 'NewDev_PCC_Email_Template' LIMIT 1].ID;
    Static Id CltemplateId = [SELECT ID, DeveloperName,Name FROM EmailTemplate WHERE DeveloperName = 'NewDev_Council_Letter_Email_Template' LIMIT 1].ID;
    // Modified the OWA to Developer Liaison instead of noreply@nbnco.com.au
    static OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = 'Developer Liaison'];
    
    @future(callout=true)
	public static void SendDeliverableDocstoDeveloper(List<id> StgIds, string DocType){
		Map<Id, stage_application__c> stgmap;
		if(docType=='PCC'){
			stgmap = new Map<Id, stage_application__c>([Select Id, Primary_Contact__c, Address__c, Suburb__c, Sent_Council_Letter__c, PCC_Issued__c, PCC_Issued_Date__c, Application_Number__c From stage_application__c Where Id IN :StgIds AND PCC_Issued__c=False]);
			
		}
		else if(docType=='CL'){
			stgmap = new Map<Id, stage_application__c>([Select Id, Primary_Contact__c, Address__c, Suburb__c, Sent_Council_Letter__c, PCC_Issued__c, PCC_Issued_Date__c, Application_Number__c From stage_application__c Where Id IN :StgIds AND Sent_Council_Letter__c=False AND Application_Status__c !='Payment Not Required']);
		
		}
    	List<Stage_Application__c> stgappstobeupdated = new List<Stage_Application__c>();
    	Map<Id, Id> stgAppUsers = new Map<Id, Id>();
    	Set<Id> cIds = new Set<Id>();
    	Map<Id, set<String>> semailids = new Map<Id, set<String>>();
    	List<Task> tlist = new List<Task>();
    	for(stage_application__c stgapp: stgmap.values()){
    		cids.add(stgapp.Primary_Contact__c);
    	}
        for(User u:[Select Id, contactId From User Where contactId IN: cids and IsActive = True]){
            stgAppUsers.put(u.ContactId, u.Id);
        }
    	for(StageApplication_Contact__c stgcon: [Select Id, Stage_Application__c, Stage_Application__r.Primary_Contact__c, Contact__c From StageApplication_Contact__c where Stage_Application__c IN : StgIds]){
    		if(stgcon.Stage_Application__r.Primary_Contact__c != stgcon.Contact__c){
    			if(semailids.containsKey(stgcon.Stage_Application__c)){
	    			semailids.get(stgcon.Stage_Application__c).add(stgcon.Contact__c);
	    		}
	    		else{
	    			semailids.put(stgcon.Stage_Application__c, new set<String>{stgcon.Contact__c});
	    		}
	    			
	    	}
    	}
    	for(stage_application__c stg :stgmap.values())
           {
               Blob pdfbody;
               PageReference pref= page.PCCAsAttachment;
               if(DocType == 'CL'){
               	pref= page.NewDevCouncilLetterTemplate;
               }
               pref.getParameters().put('id',(Id)stg.Id);
               pref.setRedirect(true);
               Attachment attachment = new Attachment();
               if(Test.isRunningTest()) {
                   pdfbody = blob.valueOf('Unit.Test');
               } else {
                   pdfbody = pref.getContentAsPDF();
                   }
               Messaging.SingleEmailMessage semail= new Messaging.SingleEmailMessage();
               Messaging.EmailFileAttachment attach= new Messaging.EmailFileAttachment();
               attach.setBody(pdfbody);
               attach.setFileName('PCC_'+stg.Application_Number__c+'_'+stg.Address__c+' '+stg.Suburb__c+'.pdf');
               semail.setTemplateId(pcctemplateId);
                if(DocType == 'CL'){
               	attach.setFileName('Council Letter_'+stg.Application_Number__c+'_'+stg.Address__c+' '+stg.Suburb__c+'.pdf');
               	semail.setTemplateId(CltemplateId);
               }
               List<string> emailIds= new List<String>(semailids.get(stg.Id));
               semail.setToAddresses(new List<String>{stg.Primary_Contact__c});
               semail.setCcAddresses(emailIds);
               semail.setTargetObjectId(stg.Primary_Contact__c);
               semail.setWhatId(stg.Id);
               semail.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});
               if ( owea.size() > 0 ) {
               	 semail.setOrgWideEmailAddressId(owea.get(0).Id);
				}
               if(!Test.isRunningTest()) 
                   Messaging.sendEmail(new Messaging.SingleEmailMessage[]{semail});
               //Create file and link it to stage app and task
               ContentVersion conVer = new ContentVersion();
			   conVer.ContentLocation = 'S';
			   conVer.PathOnClient='PCC_'+stg.Application_Number__c+'_'+stg.Address__c+' '+stg.Suburb__c+'.pdf';
			   conVer.Title = 'PCC_'+stg.Application_Number__c+'_'+stg.Address__c+' '+stg.Suburb__c+'.pdf';
			   if(DocType=='CL'){
                   conVer.PathOnClient = 'Council Letter_'+stg.Application_Number__c+'_'+stg.Address__c+' '+stg.Suburb__c+'.pdf';
                   conVer.Title = 'Council Letter_'+stg.Application_Number__c+'_'+stg.Address__c+' '+stg.Suburb__c+'.pdf';
               }
			   conVer.VersionData = pdfbody; 
               insert conVer;
               Id DocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
               createDocumentLink(DocId, stg.id);
               Task T = new Task();
               T.Type_Custom__c = 'Deliverables';
               T.Sub_Type__c = DocType =='PCC'? 'PCC': 'Council Letter';
               T.recordtypeID = taskRecTypId;
               T.Subject = DocType =='PCC'? 'Practical Completion Certificate': 'Council Letter';
               T.WhatId = stg.Id;
               T.Status = 'Closed';
               T.Deliverable_Status__c = 'Accepted';
               if(stgAppUsers.get(stg.Primary_Contact__c) != Null){
                   T.ownerid = stgAppUsers.get(stg.Primary_Contact__c);
                   //createDocumentLink(DocId, stgAppUsers.get(stg.Primary_Contact__c));                   
               }
               else{
                   T.ownerid = identifierList.Unassigned_User_Id__c;
                   T.Team__c = identifierList.Back_Office_Team_BOT_Id__c;
               }
               Insert T;
               createDocumentLink(DocId, T.Id);
               if(DocType == 'PCC'){
               	stg.PCC_Issued__c = True;
               	stg.PCC_Issued_Date__c = Date.today();
               } 
               else if(DocType =='CL'){
               	stg.Sent_Council_Letter__c = True;
               }
               stgappstobeupdated.add(stg);
           }
           if(!stgappstobeupdated.isEmpty()){
           	Update stgappstobeupdated;
           }
           
    	
    }
    public static void createDocumentLink(Id DocumentId, Id LinkedId){
         List<ContentDocumentLink> dlink = [Select Id From ContentDocumentLink where ContentDocumentId =: DocumentId AND LinkedEntityId=:LinkedId];
         if(dlink.isEmpty()){
             ContentDocumentLink cdlNew = new ContentDocumentLink();
             cdlNew.ContentDocumentId = DocumentId;
             cdlNew.LinkedEntityId  = LinkedId; 
             cdlNew.ShareType ='V';
             cdlNew.Visibility = 'AllUsers';
             insert cdlNew;
         }         
     }
    
}