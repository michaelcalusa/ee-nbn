@isTest
public class SendDeliverableDocstoDeveloper_Test {
    static User testRunningUser = new User ();
    static Contact conObj = new Contact();
    static Account actObj = new Account();
    static Development__c devObj = new Development__c();
    static Site__c siteObj = new Site__c();
    static StageApplication_Contact__c stgappcon = new StageApplication_Contact__c();
    static Stage_Application__c stageAppObj = new Stage_Application__c();
    static void getRecords(){   
        List<string> listOfProfileName = new List<string> {'System Administrator'};
        list<Profile> listOfProfiles = TestDataUtility.getListOfProfiles(listOfProfileName);
        testRunningUser = TestDataUtility.createTestUser(true,listOfProfiles.get(0).id);
        
        // Create Account
        actObj.name = 'Test Account';
        insert actObj;
        
        // Create Contact
        conObj.LastName = 'testLastName';
        conObj.Email = 'test@test.com';
        conObj.AccountId = actObj.Id;
        insert conObj;

        //stage application contacts
        stgappcon.Contact__c = conObj.Id;        
        
        // Create Development        
        devObj.Name='testName';
        devObj.Development_ID__c='AKHS833';
        devObj.Account__c=actObj.Id;
        devObj.Primary_Contact__c=conObj.Id;
        devObj.Suburb__c='testsuburd';
        insert devObj;
        //Site Object
        siteObj.Site_Address__c = '3 Leila rd Ormond';
        siteObj.Name = '3 Leila rd Ormond';
        insert siteObj;
        insert new SA_Auto_Reference_Number__c(Name='SA001',Last_Sequence_Number__c='00000233232323');
        insert new New_Dev_Platform_Events__c(Unassigned_User_Id__c=testRunningUser.Id);
       
    }
    static testMethod void SendDeliverableDocstoDeveloper_Test1(){
        getRecords();
        Test.startTest();
        System.runAs(testRunningUser){
            stageAppObj.Name='testName';
            stageAppObj.Active_Status__c='Active';
            stageAppObj.Dwelling_Type__c='SDU Only';
            stageAppObj.Development__c=devObj.Id;
            stageAppObj.Build_Type__c='Pit and Pipe';
            stageAppObj.Class__c='Class3/4';
            stageAppObj.Service_Delivery_Type__c='SD-2';
            stageAppObj.Primary_Contact__c = conObj.Id;
            stageAppObj.Account__c= actObj.Id;
            stageAppObj.Primary_Location__c = siteObj.Id;
            insert stageAppObj;
            stgappcon.Stage_Application__c = stageAppObj.Id;
            stgappcon.Roles__c = 'Contract Signatory';
            insert stgappcon;
            PageReference pageRef = Page.generateAndSendPCC;
        	Test.setCurrentPage(pageRef);
        	ApexPages.StandardController sc = new ApexPages.standardController(stageAppObj);
        	PCCAsAttachmentCTRL pcccontroller = new PCCAsAttachmentCTRL(sc);
            ApexPages.currentPage().getParameters().put('Id', stageAppObj.Id);
            ApexPages.currentPage().getParameters().put('DocType', 'PCC');
            pcccontroller.attachPDF();
            PCCAsAttachmentCTRL Clcontroller = new PCCAsAttachmentCTRL(sc);
            ApexPages.currentPage().getParameters().put('Id', stageAppObj.Id);
            ApexPages.currentPage().getParameters().put('DocType', 'CL');
            Clcontroller.attachPDF();
            PageReference pr =  Clcontroller.redirectBack();
            system.debug('pr+ '+pr);
        }
        Test.stopTest();
        System.assertEquals(TRUE, [SELECT PCC_Issued__c FROM stage_application__c WHERE ID =:stageAppObj.Id].PCC_Issued__c);
        System.assertEquals(TRUE, [SELECT Sent_Council_Letter__c FROM stage_application__c WHERE ID =:stageAppObj.Id].Sent_Council_Letter__c);
    }

}