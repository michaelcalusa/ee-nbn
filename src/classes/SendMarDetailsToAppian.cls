/***************************************************************************************************
    Class Name          : SendMarDetailsToAppian
    Version             : 1.0
    Created Date        : 28-Jun-2018
    Author              : Arpit Narain
    Description         : Handler class to process Mar Events
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/

public class SendMarDetailsToAppian implements SendObjectToAppian  {

    private static final String marSelfRegAppianURL = GlobalUtility.getMDConfiguration('MarSelfRegAppianURL');
    private static final String appianCallOut = GlobalUtility.getMDConfiguration('MarAppianEndPoint');
    private static final String httpPostMethod = 'POST';

    public void sendDetailsToAppian(Map<String, String> httpHeaderMap, List<Object> generateMarMessageForAppianList, List<sObject> marList) {


        httpHeaderMap.put('Content-Type', 'application/json;charset=UTF-8');
        String httpMessage = JSON.serialize(generateMarMessageForAppianList);
        String httpEndPoint = appianCallOut + marSelfRegAppianURL;

        try {
            HttpResponse httpResponse = MarUtil.sendHTTPRequest(httpMessage, httpHeaderMap, httpPostMethod, httpEndPoint);

            GlobalUtility.logMessage('Info', 'httpMarCallOut', 'MarDetails2Appian', '', 'HTTP EndPoint: ' + httpEndPoint
                    , 'Response Status Code: ' + httpResponse.getStatusCode() + '\n Response Body: ' + httpResponse.getBody(), 'Message Sent: ' + httpMessage, null, 0);

            processAppianResponse(httpResponse, httpEndPoint, marList);

        } catch (Exception e) {
            GlobalUtility.logMessage('Error', 'httpMarCallOut', 'MarDetails2Appian', '', 'HTTP EndPoint: ' + httpEndPoint
                    , 'Error calling Appian ', 'Failed message: ' + httpMessage, e, 0);
        }
    }


    public void processAppianResponse(HttpResponse httpResponse, String httpEndPoint, List<sObject> marList) {


        if (httpResponse.getStatusCode() != 200) {
            GlobalUtility.logMessage('Info', 'httpMarCallOut', 'MarDetails2Appian', '', 'HTTP EndPoint: ' + httpEndPoint
                    , 'Response Status Code: ' + httpResponse.getStatusCode() + '\n Response Body: ' + httpResponse.getBody(), 'Retrying processing as status code not 200', null, 0);

            if (!String.isBlank(httpResponse.getBody())) {
                updateAppainProcessId(httpResponse, marList);
            }

            EventRetryAndErrorHandler.RetryOrthrowExMessage();

        } else {
            if (!String.isBlank(httpResponse.getBody())) {
                updateAppainProcessId(httpResponse, marList);
            }
        }

    }

    public void updateAppainProcessId(HttpResponse httpResponse, List<sObject> marList) {
        try {

            List<MAR__c> mars = new List<MAR__c>();
            for (sObject sobj : marList){
                mars.add((MAR__c) sobj);
            }
            if (httpResponse.getStatusCode() == 200) {
                try {
                    ParseAppianMarRegistrationResponse parseAppianMarRegistrationResponse = ParseAppianMarRegistrationResponse.parse(httpResponse.getBody());
                    for (MAR__c mar : mars) {
                        mar.External_Process_ID__c = parseAppianMarRegistrationResponse.processId;
                    }
                } catch (Exception e) {
                    for (MAR__c mar : mars) {
                        mar.External_Process_Error__c = 'Could not parse response from Appian, Refer Application log for details';
                    }
                    GlobalUtility.logMessage('Error', 'httpMarCallOut', 'MarDetails2Appian', '', 'Could not update Appian Process Id - Update Error '
                            , null, 'Could not update Appian Process Id', e, 0);
                }
            } else {
                for (MAR__c mar : mars) {
                    mar.External_Process_Error__c = httpResponse.getBody();
                }
            }

            Database.SaveResult[] updateResults = Database.update(marList, false);
            for (Database.SaveResult updateResult : updateResults) {

                if (!updateResult.isSuccess()) {
                    for (Database.Error errors : updateResult.getErrors()) {
                        GlobalUtility.logMessage('Error', 'httpMarCallOut', 'MarDetails2Appian', '', 'Could not update Appian Process Id - Update Error '
                                , 'Mar ID: ' + updateResult.getId() + '\n Errors: ' + updateResult.getErrors(), 'Could not update Appian Process Id', null, 0);
                    }
                }
            }
        } catch (Exception e) {

            GlobalUtility.logMessage('Error', 'httpMarCallOut', 'MarDetails2Appian', '', 'Could not update Appian Process Id - Apex error '
                    , null, 'Could not update Appian Process Id', e, 0);
        }
    }




}