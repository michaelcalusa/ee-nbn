/***************************************************************************************************
    Class Name          : SendObjectToAppian
    Version             : 1.0
    Created Date        : 03-Jul-2018
    Author              : Arpit Narain
    Description         : Interface to sending MAR, CASE create, CASE update details to Appain
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/

public  interface SendObjectToAppian {

    void sendDetailsToAppian(Map<String, String> httpHeaderMap, List<Object> generatedObjectForAppian, List<sObject> sObjectList);
    void processAppianResponse(HttpResponse httpResponse, String httpEndPoint, List<sObject> sObjectList);
    void updateAppainProcessId(HttpResponse httpResponse, List<sObject> sObjectList);




}