/*----------------------------------------------------------------------------------------
    Author:        Dilip Athley (v-dileepathley)
    Company:       NBNco
    Description:   This class will be used for sending the files to CRMOD MSEU-7622.
    Test Class:    
    History
    <Date>            <Authors Name>    <Brief Description of Change> 
    
    -----------------------------------------------------------------------------------------*/
public class SendToCRM {
    public class GeneralException extends Exception
            {}
    @future(callout=true)
    public static void SendToCRMOD(string reqString, string wsType, string cvList)
    {
        
        try
        {
            DOM.Document doc = new DOM.Document();
        	doc.load(reqString);
            Http http = new Http();
            HttpResponse res;
            HttpRequest req = new HttpRequest();	
            req.setMethod('POST');
			req.setEndpoint(CRMOD_Credentials__c.getinstance('url').Value__c);
			req.setHeader('Content-Type', 'text/xml');
			req.setHeader('SOAPAction',wsType);
		
			req.setBodyDocument(doc);
			res = http.send(req);
			System.debug(doc.toXmlString());
			System.debug(res.getBodyDocument().toXmlString());
            if(res.getStatusCode()!= 200)
            {
                throw new GeneralException('Not able to send the file to CRM Error - '+'Status Code - '+res.getStatusCode()+' Status - '+res.getStatus()+' Resposne Body - '+res.getBodyDocument().toXmlString());
            }
            else
            {
               if(string.isNotBlank(cvList))
               {
                   List<ContentVersion> cvListToUpdate = (List<ContentVersion>)JSON.deserialize(cvList, List<ContentVersion>.class);
                   system.debug('cvListToUpdate  '+cvListToUpdate);
                   update cvListToUpdate;
                   
               }
            }
        }
        catch(Exception ex)
        {
            GlobalUtility.logMessage('Error', 'InsertAttachmentCRMOD', 'sendRequest', ' ', '', '', '', ex, 0);
        }   	
    }
}