@isTest
private class SendToCRM_Test {
   
       static testMethod void sendToCRMTest() {
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        TestDataUtility.getListOfCRMODCredentialsRecords(true);
        // Test the functionality
        Test.startTest();
        integer statusCode = 200;
        string body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cus="urn:crmondemand/ws/customobject3/" xmlns:cus1="urn:/crmondemand/xml/customObject3"><soapenv:Header><wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><wsse:UsernameToken> <wsse:Username>NBNCO/SALESFORCEINTERFACE</wsse:Username><wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">CRMtest15</wsse:Password>    </wsse:UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><cus:CustomObject3WS_CustomObject3InsertChild_Input>       <cus1:ListOfCustomObject3><!--Zero or more repetitions:--><cus1:CustomObject3><!--Optional:-->     <cus1:CustomObject3Id>AYCA-3YVAC7</cus1:CustomObject3Id><cus1:ListOfAttachment><!--Zero or more repetitions:--><cus1:Attachment><cus1:DisplayFileName>Master Plan</cus1:DisplayFileName><!--Optional:-->                     <cus1:FileNameOrURL>https://nbn--sukumar.cs58.my.salesforce.com/00T0l000003qnt?</cus1:FileNameOrURL></cus1:Attachment> </cus1:ListOfAttachment></cus1:CustomObject3></cus1:ListOfCustomObject3><!--Optional:--><cus:Echo>?</cus:Echo></cus:CustomObject3WS_CustomObject3InsertChild_Input></soapenv:Body></soapenv:Envelope>';
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        List<ContentVersion> cvList = [SELECT ContentDocumentId,Id,Synced__c,FileExtension,ContentSize,Title FROM ContentVersion where synced__c = false]; 
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        SendToCRM.SendToCRMOD(body,'"document/urn:crmondemand/ws/customobject2/10/2004:CustomObject2Update"',json.serialize(cvList));
        Test.stopTest(); 
    }
    
    static testMethod void sendToCRMTest_negative() {
        string setHeaderCookieValue = 'JSESSIONID='+userinfo.getSessionId()+'; path=/OnDemand; HttpOnly; Secure';
        TestDataUtility.getListOfCRMODCredentialsRecords(true);
        // Test the functionality
        Test.startTest();
        integer statusCode = 500;
        string body = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:cus="urn:crmondemand/ws/customobject3/" xmlns:cus1="urn:/crmondemand/xml/customObject3"><soapenv:Header><wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"><wsse:UsernameToken> <wsse:Username>NBNCO/SALESFORCEINTERFACE</wsse:Username><wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">CRMtest15</wsse:Password>    </wsse:UsernameToken></wsse:Security></soapenv:Header><soapenv:Body><cus:CustomObject3WS_CustomObject3InsertChild_Input>       <cus1:ListOfCustomObject3><!--Zero or more repetitions:--><cus1:CustomObject3><!--Optional:-->     <cus1:CustomObject3Id>AYCA-3YVAC7</cus1:CustomObject3Id><cus1:ListOfAttachment><!--Zero or more repetitions:--><cus1:Attachment><cus1:DisplayFileName>Master Plan</cus1:DisplayFileName><!--Optional:-->                     <cus1:FileNameOrURL>https://nbn--sukumar.cs58.my.salesforce.com/00T0l000003qnt?</cus1:FileNameOrURL></cus1:Attachment> </cus1:ListOfAttachment></cus1:CustomObject3></cus1:ListOfCustomObject3><!--Optional:--><cus:Echo>?</cus:Echo></cus:CustomObject3WS_CustomObject3InsertChild_Input></soapenv:Body></soapenv:Envelope>';
        Map<String, String> responseHeaders = new Map<String, String> ();
        responseHeaders.put('Content-Type','application/JSON');
        responseHeaders.put('Set-Cookie',setHeaderCookieValue);
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
        
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        SendToCRM.SendToCRMOD(body,'"document/urn:crmondemand/ws/customobject2/10/2004:CustomObject2Update"','');
        Test.stopTest(); 
    }
}