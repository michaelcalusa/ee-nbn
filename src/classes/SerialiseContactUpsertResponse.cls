/***************************************************************************************************
    Class Name          : SerialiseContactUpsertResponse
    Version             : 1.0
    Created Date        : 03-Jul-2018
    Author              : Arpit Narain
    Description         : Class to parse response from Appian to Apex class
    Modification Log    :
    * Developer             Date            Description
    * ----------------------------------------------------------------------------

****************************************************************************************************/

public class SerialiseContactUpsertResponse {

    public String index;
    public String id;
    public String success;
    public List<Database.Error> error;

    public SerialiseContactUpsertResponse() {

    }


    public SerialiseContactUpsertResponse(Database.UpsertResult upsertResult, String index) {

        this.id = upsertResult.Id;
        this.success = String.valueOf(upsertResult.isSuccess());
        this.error = upsertResult.errors;
        this.index = index;

    }
}