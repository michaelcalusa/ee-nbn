/*
Class Description
Creator: Dilip Athley (v-dileepathley)
Purpose: This class will be used to get the session id, if the current session Id is not valid. It will also be used to insert the session id records.
Modifications:
*/
public class SessionId
{
    //this method will be used to get the new session id by establishing connection with CRM OD.
    public static String getSessionId()
	{
			Http http = new Http();
			HttpRequest req = new HttpRequest();
			HttpResponse res;
			String cookie,jsessionId;
			req.setMethod('GET');
			req.setEndpoint('callout:CRMOD_Connection');
			res = http.send(req);
 			cookie = res.getHeader('Set-Cookie');
			jsessionId = cookie.substring(cookie.indexOf('JSESSIONID='),cookie.indexOf('; path=/OnDemand; HttpOnly; Secure') );
			return(jsessionId);
	}
	 //this method will be used to store the new session id.
	public static void storeSessionId(session_Id__c jId, String jsessionId)
	{
		Session_Id__c sessionId = new Session_Id__c();
		sessionId.Name = jsessionId;
		sessionId.CRM_Session_Id__c = jsessionId;
		sessionId.Session_Alive__c = true;
		jId.Session_Alive__c=false;
		list<Session_Id__c> session = new list<Session_Id__c>();
		session.add(jId);
		session.add(sessionId);
		upsert session;
	}
}