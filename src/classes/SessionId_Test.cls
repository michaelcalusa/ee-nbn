/***************************************************************************************************
Class Name:  SessionId_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : 
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Sukumar       26/10/2015                 Created
****************************************************************************************************/
@isTest
private class SessionId_Test {
    static Extraction_Job__c ExtractionJobRec = new Extraction_Job__c ();
    static Session_Id__c SessionIdRec = new Session_Id__c();
    static String body = '{'+ '\"CustomObjects3\": \"AYCA-3QHQ87\" '+'}';
    
    static void getRecords (){
        // Create Session_Id record
        SessionIdRec.Session_Alive__c=true;
        SessionIdRec.CRM_Session_Id__c= '12345';
        insert SessionIdRec;
    }
    
    static testMethod void SessionId_SuccessScenario(){
    getRecords();
    Test.startTest();
    integer statusCode = 200;
    string setHeaderCookieValue = 'JSESSIONID='+'12345'+'; path=/OnDemand; HttpOnly; Secure';
    Map<String, String> responseHeaders = new Map<String, String> ();
    responseHeaders.put('Content-Type','application/JSON');
    responseHeaders.put('Set-Cookie',setHeaderCookieValue); 
    QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,body,responseHeaders);
    Test.setMock(HttpCalloutMock.class, fakeResponse1);
    string sid = SessionId.getSessionId();
    SessionId.storeSessionId(SessionIdRec, sid);
    Test.stopTest();
    }
}