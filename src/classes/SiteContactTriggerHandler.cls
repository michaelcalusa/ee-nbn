/***************************************************************************************************
Version     : 1.0 
Created Date: 16-10-2018 
Description/Function    :  Used in SiteContactTrigger Apex Trigger
Used in     : 
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* SAKTHIVEL MADESH         16-10-2018            Created the Class to avoid Recursion
****************************************************************************************************/
public class SiteContactTriggerHandler{

    public static Boolean isExecuting = true;
}