global with sharing class SiteQualification_CX {


    Public Site__c siteRecord {get; set;}
    Public Map<String, Id> mapOfSiteRecordTypeNamesAndId {get; set;}
    Public Boolean siteUpdated {get; set;}
    Public String addressString {get; set;}
    Public Boolean showAddressSearchBox {get; set;}
    Public String recId {get; set;}
    Public Boolean isErr {get; set;}
    
    public SiteQualification_CX(ApexPages.StandardController controller) {
        siteUpdated = false;
        isErr = false;
        showAddressSearchBox = false;
        recId = controller.getId();
        /*Addition of All the Site address fields to query in order to  pre populate advance search
          Address text boxes with Site record's Address details - Site Cleanup Project MSEU - 3393
        */
        if (recId !=null)
            siteRecord = [SELECT Id, Name,Serviceability_Class__c, Rollout_Type__c, Technology_Type__c, 
                                 Location_Id__c, Site_Address__c,Qualification_Date__c,RecordTypeId, 
                                 Unit_Number__c, Road_Name__c, Level_Number__c, Lot_Number__c, Road_Number_1__c, Road_Number_2__c, Road_Type_Code__c, Road_Suffix_Code__c,
                                 Post_Code__c, Locality_Name__c, State_Territory_Code__c, Requalification_Failed_Date__c, Require_Qualification__c FROM Site__c
                           WHERE id=:recId];
                           
        // Prepare Map of RecordTypeName and Corresponding Id.
        mapOfSiteRecordTypeNamesAndId = HelperUtility.pullAllRecordTypes('site__c');
    }
    
    Public pagereference reQualifySiteRecord()
    {
        pagereference p;
    
       // If RecordType is "Verified" and LocID is not equal to Null Need to reQualify
        If(siteRecord.Location_Id__c!=null && (siteRecord.RecordTypeId==mapOfSiteRecordTypeNamesAndId.get('Verified')))      
           rqVerifiedSiteWithLocId(); 
           
        // If RecordType is "UnVerified" and LocID is not equal to Null Then Check the LocID in CIS and If available update the Site with Qualification details
        // If RecordType is "UnVerified" and LocID is not equal to Null Then Check the LocID in CIS and If NOT available Throw Error Message
        // Error Message: "A site with Location Id LOCXXXXXX could not be found in CIS. Please validate that this Location Id is correct before attempting
        // to Qualify the address again.
        If(siteRecord.Location_Id__c!=null && (siteRecord.RecordTypeId==mapOfSiteRecordTypeNamesAndId.get('Unverified') || siteRecord.RecordTypeId==mapOfSiteRecordTypeNamesAndId.get('MDU/CP')))
           rqUnverifiedSiteWithLocId(); 
           
           
        // If RecordType is "UnVerified" and LocID is equal to Null Then Check the address string 
         If(siteRecord.Location_Id__c==null && siteRecord.RecordTypeId==mapOfSiteRecordTypeNamesAndId.get('Unverified'))
           rqUnverifiedSiteWithOutLocId();   

        if(siteUpdated)
        {
            p = new Pagereference('/'+siteRecord.id);
            p.setRedirect(false);         
        }       
        return p;
    }
    
    Public Void rqVerifiedSiteWithLocId()
    {       
        // If there is any updates to Site Qualification Details Save the site record.
        if(checkQualifcationDetails()) 
         saveSiteAfterReQualification();                                                                                                 
    }
    
    Public Void rqUnverifiedSiteWithLocId()
    {
        if(checkQualifcationDetails())
         saveSiteAfterReQualification();
        else
        {
         isErr = True;
		if(ApexPages.currentPage() != null){         
	         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'A site with Location Id '+ siteRecord.Location_Id__c +' could not be found in ELMS. Please validate that this Location Id is correct before attempting to Qualify the address again.');
	         ApexPages.addMessage(myMsg);
		}
		
		//Auto Requalify Batch requirement - Setting up timestamp for the Site failed to requalify so we can pick them up after specific Time interval
		siteRecord.Requalification_Failed_Date__c = Date.today();
		update siteRecord;
       }
    }
    
    
    Public Void rqUnverifiedSiteWithOutLocId()
    {
        addressString = siteRecord.Site_Address__c;
        showAddressSearchBox = True;
    }
    
    
    Public void saveSiteAfterReQualification()
    {
        try{
        	  siteRecord.Requalification_Failed_Date__c = NULL;
        	  siteRecord.Require_Qualification__c = false;
              update siteRecord;
           }
           
         catch(exception ex)
        {
        	if(ApexPages.currentPage() != null){
            apexpages.addmessages(ex);
        	}
        }  
       
    }
    
    
    Public Boolean checkQualifcationDetails()
    {
      system.debug('****siteRecord'+siteRecord);
      DateTime oldSiteQD = siteRecord.Qualification_Date__c;
      siteRecord = HelperUtility.getSiteQualificationForExistingSite(string.valueof(siteRecord.id));
      siteUpdated = True;
      /*AddressSearch_CX.sWrapper qDtls = new AddressSearch_CX.sWrapper();
        qDtls = AddressSearch_CX.getQualificationDetailsfromExtSystem(siteRecord.Location_Id__c);
        system.debug('****checkQualifcationDetails - qDtls'+qDtls);
        if(qDtls!=null)
        {
               system.debug('****siteRecord1'+siteRecord.Technology_Type__c);
               system.debug('****siteRecord2'+qDtls.technologyType); 
              If(siteRecord.Technology_Type__c!= qDtls.technologyType)
              {
                 siteRecord.Technology_Type__c =  qDtls.technologyType;
                 siteUpdated = True;
              }
              
              If(siteRecord.Serviceability_Class__c != Integer.valueof(qDtls.serviceClass))
              {
                  siteRecord.Serviceability_Class__c = Integer.valueof(qDtls.serviceClass);
                  siteUpdated = True;
              }    
                   
              If(siteRecord.Rollout_Type__c != qDtls.rolloutType) 
              {
                  siteRecord.Rollout_Type__c = qDtls.rolloutType;
                  siteUpdated = True;
              } 
              
              If(siteUpdated)
              {
                 siteRecord.Qualification_Date__c = (qDtls.qualificationDate!=null ? system.now() : null);
                 siteRecord.RecordTypeId = mapOfSiteRecordTypeNamesAndId.get('Verified') ;
              }
              
              
              If(siteRecord.Technology_Type__c == qDtls.technologyType && siteRecord.Rollout_Type__c == qDtls.rolloutType && siteRecord.Serviceability_Class__c == Integer.valueof(qDtls.serviceClass)) 
              {                 
                  siteUpdated = True;
              } 
              
              If(qDtls.isSiteAvailable==false)
              {
                 siteUpdated = false;
              }    */
              
              //If(oldSiteQD==siteRecord.Qualification_Date__c)
              If(siteRecord.Qualification_Date__c == null)
              {
                 siteUpdated = false;
              }    

              
       // } 
        system.debug('***checkQualifcationDetails==>'+siteUpdated);
       return siteUpdated;
    }
    

    
    @RemoteAction
    public static site__c getAddressSearchData(AddressSearch_CX.addressWrapper selectedAddress, string sId){
        system.debug('**** selectedAddress.locationId ==>'+ selectedAddress.locationId);
        Site__c sRecord = HelperUtility.instantiateUnStructuredVerifiedSite(selectedAddress);
        system.debug('sRecord =>'+sRecord);
        
        
        
        Map<String, Id> mapOfSiteRecordTypeNamesAndId = new Map<String, Id>();
        mapOfSiteRecordTypeNamesAndId = HelperUtility.pullAllRecordTypes('site__c');
        
        //Site__c sRecord = HelperUtility.getSiteQualificationForExistingSite(sId);
        /* 
                                 Location_Id__c, Site_Address__c FROM Site__c
                           WHERE id=:sId];
         sRecord.Location_Id__c = selectedAddress.locationId;   
         AddressSearch_CX.sWrapper qDtls = new AddressSearch_CX.sWrapper();
         qDtls = AddressSearch_CX.getQualificationDetailsfromExtSystem(sRecord.Location_Id__c); 
         
        Site__c sRecord = [SELECT Id, Serviceability_Class__c, Rollout_Type__c, Technology_Type__c, 
         sRecord.Technology_Type__c =  qDtls.technologyType; 
         sRecord.Serviceability_Class__c = Integer.valueof(qDtls.serviceClass); 
         sRecord.Rollout_Type__c = qDtls.rolloutType;
         sRecord.Unit_Number__c = qDtls.unitNumber;
         sRecord.Level_Number__c = qDtls.levelNumber;
         sRecord.Post_Code__c = Decimal.Valueof(qDtls.postCode);
         sRecord.Locality_Name__c = qDtls.locality;
         sRecord.Road_Type_Code__c = qDtls.roadTypeCode;
         sRecord.Road_Name__c = qDtls.roadName;
         sRecord.Road_Number_1__c = qDtls.roadNumber1;
         sRecord.Unit_Type_Code__c = qDtls.type;  
         sRecord.Qualification_Date__c = (qDtls.qualificationDate!=null ? system.now() : null);
         sRecord.RecordTypeId = mapOfSiteRecordTypeNamesAndId.get('Verified') ;      
       */
       
         
       
       
       string siteId;
       site__c finalSiteRec = new site__c();
         
        if(sRecord.id==null)
        {
           site__c existingSite= new site__c();
           existingSite = sRecord;
           existingSite.id = sId;
           existingSite.isSiteCreationThroughCode__c = True;
           system.debug('*** existingSite1 ==>'+existingSite);
           update existingSite; 
           system.debug('*** existingSite2 ==>'+existingSite);   
           siteId = existingSite.id;
           finalSiteRec = existingSite;
        }
        else
        {
            sRecord.isSiteCreationThroughCode__c = True;
            update sRecord;
            system.debug('*** sRecord ==>'+sRecord);
            siteId = sRecord.id;
            finalSiteRec = sRecord;
        }
        
       system.debug('*** siteId ==>'+siteId);
        
       //return string.valueof(siteId);
       return finalSiteRec;
    }
    
   Public pagereference navigateToSite()
   {
      pagereference P;
   
      if(siteRecord!=null)
       { p = new pagereference('/'+siteRecord.id); }
      return P;
   }
   
   // SyedMoosaNazir - 12-08-2016 - To create a Unverified Site Address
    @RemoteAction
    public static AddressSearch_CX.addressWrapper createUnverifiedSite(AddressSearch_CX.StructuredAddress inputAddress){
        Id siteId =  string.valueof(HelperUtility.createStructuredUnVerifiedSite (inputAddress));
        AddressSearch_CX.addressWrapper createdUnverifiedAddress = new AddressSearch_CX.addressWrapper ();
        List<Site__c> siteRecords = [SELECT Id, Name, Site_Address__c, Location_Id__c, Asset_Number__c FROM site__c WHERE Id =: siteId LIMIT 1];
        createdUnverifiedAddress.address = siteRecords.get(0).Site_Address__c;
        createdUnverifiedAddress.locationId = siteRecords.get(0).Location_Id__c;
        createdUnverifiedAddress.addressFrom =  'UnVerified';
        createdUnverifiedAddress.assetNumber = siteRecords.get(0).Asset_Number__c;
        createdUnverifiedAddress.sfRecordId = siteRecords.get(0).Id;
        createdUnverifiedAddress.SiteSfRecordName = siteRecords.get(0).Name;
        system.debug('createdUnverifiedAddress==>'+createdUnverifiedAddress);
        return createdUnverifiedAddress;
    }

}