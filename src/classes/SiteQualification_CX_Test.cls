/****************************************************************************************************************
Class Name:  SiteQualification_CX_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 02-11-2016
Function    : This class contains unit test scenarios for SiteQualification_CX apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir         02-11-2016                Created
* Ganesh Sawant            25-09-2017                Updated - Added test Coverage for Auto Site Qualify Batch
* Ganesh Sawant            24-08-2018                Updated - Added test Coverage for Auto Site Qualify Batch 2
****************************************************************************************************************/
@isTest
private class SiteQualification_CX_Test {
    static integer statusCode = 200;
    static Map<String, String> responseHeaders = new Map<String, String> ();
    static List<Site__c> UnverifiedSitesList = new List<Site__c> ();
    static String jsonInput = '{'+
        '  \"data\": ['+
        '    {'+
        '      \"type\": \"location\",'+
        '      \"id\": \"LOC123456789012\",'+
        '      \"attributes\": {'+
        '        \"gnafId\": \"G8932387982982\",'+
        '        \"dwellingType\": \"MDU\",'+
        '        \"region\": \"Urban\",'+
        '        \"regionValueDefaulted\": false,'+
        '        \"externalTerritory\": false,'+
        '        \"landUse\": \"Commercial\",'+
        '        \"landSubUse\": \"Retail\",'+
        '        \"address\": {'+
        '          \"unstructured\": \"Building A 1-2 Park LANE N Parkview 12345 APT 1 F 1 15B-16 Trevenar ST N Ashbury NSW 2193\",'+
        '          \"siteName\": \"Parkview\",'+
        '          \"locationDescriptor\": \"Near the school\",'+
        '          \"levelType\": \"F\",'+
        '          \"levelNumber\": \"1\",'+
        '          \"unitType\": \"APT\",'+
        '          \"unitNumber\": \"1\",'+
        '          \"lotNumber\": \"12345\",'+
        '          \"planNumber\": \"N-78238\",'+
        '          \"roadNumber1\": \"15B\",'+
        '          \"roadNumber2\": \"16\",'+
        '          \"roadName\": \"Trevenar\",'+
        '          \"roadTypeCode\": \"ST\",'+
        '          \"roadSuffixCode\": \"N\",'+
        '          \"locality\": \"Ashbury\",'+
        '          \"postCode\": \"2193\",'+
        '          \"state\": \"NSW\",'+
        '          \"complexAddress\": {'+
        '            \"siteName\": \"Building A\",'+
        '            \"roadNumber1\": \"1\",'+
        '            \"roadNumber2\": \"2\",'+
        '            \"roadName\": \"Park\",'+
        '            \"roadTypeCode\": \"LANE\",'+
        '            \"roadSuffixCode\": \"N\"'+
        '          }'+
        '        },'+
        '        \"aliasAddresses\": ['+
        '          {'+
        '            \"unstructured\": \"Building A 1-2 Park LANE N Parkview 12345 APT 1 F 1 13A-16 Trevenar ST N Ashbury NSW 2193\",'+
        '            \"siteName\": \"Parkview\",'+
        '            \"locationDescriptor\": \"Near the school\",'+
        '            \"levelType\": \"F\",'+
        '            \"levelNumber\": \"1\",'+
        '            \"unitType\": \"APT\",'+
        '            \"unitNumber\": \"1\",'+
        '            \"lotNumber\": \"12345\",'+
        '            \"planNumber\": \"N-78238\",'+
        '            \"roadNumber1\": \"15\",'+
        '            \"roadNumber2\": \"16\",'+
        '            \"roadName\": \"Trevenar\",'+
        '            \"roadTypeCode\": \"ST\",'+
        '            \"roadSuffixCode\": \"N\",'+
        '            \"locality\": \"Ashbury\",'+
        '            \"postCode\": \"2193\",'+
        '            \"state\": \"NSW\",'+
        '            \"complexAddress\": {'+
        '              \"siteName\": \"Building A\",'+
        '              \"roadNumber1\": \"1\",'+
        '              \"roadNumber2\": \"2\",'+
        '              \"roadName\": \"Park\",'+
        '              \"roadTypeCode\": \"LANE\",'+
        '              \"roadSuffixCode\": \"N\"'+
        '            }'+
        '          }'+
        '        ],'+
        '        \"geocode\": {'+
        '          \"latitude\": \"37.8136\",'+
        '          \"longitude\": \"144.9631\",'+
        '          \"geographicDatum\": \"GDA94\",'+
        '          \"srid\": \"4283\"'+
        '        },'+
        '        \"primaryAccessTechnology\": {'+
        '          \"technologyType\": \"Fibre\",'+
        '          \"serviceClass\": 3,'+
        '          \"rolloutType\": \"Brownfield\"'+
        '        }'+
        '      },'+
        '      \"relationships\": {'+
        '        \"containmentBoundaries\": {'+
        '          \"data\": ['+
        '            {'+
        '              \"id\": \"ADA-001\",'+
        '              \"type\": \"networkBoundary\"'+
        '            },'+
        '            {'+
        '              \"id\": \"MPS-001\",'+
        '              \"type\": \"networkBoundary\"'+
        '            }'+
        '          ]'+
        '        },'+
        '        \"MDU\": {'+
        '          \"data\": {'+
        '            \"id\": \"LOC212345678901\",'+
        '            \"type\": \"location\"'+
        '          }'+
        '        }'+
        '      }'+
        '    }'+
        '  ],'+
        '  \"included\": ['+
        '    {'+
        '      \"type\": \"networkBoundary\",'+
        '      \"id\": \"ADA-001\",'+
        '      \"attributes\": {'+
        '        \"boundaryType\": \"ADA\",'+
        '        \"status\": \"INSERVICE\",'+
        '        \"technologyType\": \"Fibre\"'+
        '      }'+
        '    },'+
        '    {'+
        '      \"type\": \"networkBoundary\",'+
        '      \"id\": \"MPS-001\",'+
        '      \"attributes\": {'+
        '        \"boundaryType\": \"MPS\",'+
        '        \"status\": \"INSERVICE\",'+
        '        \"technologyType\": \"Fibre\"'+
        '      }'+
        '    }'+
        '  ]'+
        '}';
    static void testData(){
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        UnverifiedSitesList = new List<Site__c>();
        integer numberOfRecords = 1;
        for(integer counter = 0 ; counter < numberOfRecords ; counter++){
            Site__c UnverifiedSite = new Site__c();
            UnverifiedSite.RecordTypeId = recordTypeId;
            UnverifiedSite.Site_Address__c = '100 CHATSWOOD, NSW 2150'+counter;
            UnverifiedSite.Name = '100 CHATSWOOD, NSW 2150'+counter;
            UnverifiedSite.Locality_Name__c = 'CHATSWOOD';
            UnverifiedSite.Road_Name__c = 'CHATSWOOD';
            UnverifiedSitesList.add(UnverifiedSite);
        }
    }
    static testMethod void SiteQualification_CX_NoLocId(){
        testData();
        insert UnverifiedSitesList;
        Id [] fixedSearchResults= new Id[20];
        for(Site__c site:UnverifiedSitesList){
            fixedSearchResults.add(site.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.StartTest();
        // Testing Visualforce Page -- Standard Controller -- Site Record
        PageReference AddSitePage = Page.AddSite;
        Test.setCurrentPage(AddSitePage);
        ApexPages.StandardController StndSiteContr = new ApexPages.standardController(UnverifiedSitesList.get(0));
        ApexPages.CurrentPage().getparameters().put('id',UnverifiedSitesList.get(0).Id);
        SiteQualification_CX SiteQualification_CX_Con = new SiteQualification_CX(StndSiteContr);
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,jsonInput,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        SiteQualification_CX_Con.reQualifySiteRecord();
        Test.stopTest();
    }
    static testMethod void SiteQualification_CX_UVsite_WithLocId(){
        testData();
        UnverifiedSitesList.get(0).Location_Id__c = 'LOC123456789012';
        insert UnverifiedSitesList;
        Id [] fixedSearchResults= new Id[20];
        for(Site__c site:UnverifiedSitesList){
            fixedSearchResults.add(site.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.StartTest();
        // Testing Visualforce Page -- Standard Controller -- Site Record
        PageReference AddSitePage = Page.AddSite;
        Test.setCurrentPage(AddSitePage);
        ApexPages.StandardController StndSiteContr = new ApexPages.standardController(UnverifiedSitesList.get(0));
        ApexPages.CurrentPage().getparameters().put('id',UnverifiedSitesList.get(0).Id);
        SiteQualification_CX SiteQualification_CX_Con = new SiteQualification_CX(StndSiteContr);
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,jsonInput,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        SiteQualification_CX_Con.reQualifySiteRecord();
        Test.stopTest();
    }
    static testMethod void SiteQualification_CX_Vsite_WithLocId(){
        testData();
        UnverifiedSitesList.get(0).RecordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Verified').getRecordTypeId();
        UnverifiedSitesList.get(0).Location_Id__c = 'LOC123456789012';
        insert UnverifiedSitesList;
        Id [] fixedSearchResults= new Id[20];
        for(Site__c site:UnverifiedSitesList){
            fixedSearchResults.add(site.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.StartTest();
        // Testing Visualforce Page -- Standard Controller -- Site Record
        PageReference AddSitePage = Page.AddSite;
        Test.setCurrentPage(AddSitePage);
        ApexPages.StandardController StndSiteContr = new ApexPages.standardController(UnverifiedSitesList.get(0));
        ApexPages.CurrentPage().getparameters().put('id',UnverifiedSitesList.get(0).Id);
        SiteQualification_CX SiteQualification_CX_Con = new SiteQualification_CX(StndSiteContr);
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,jsonInput,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        SiteQualification_CX_Con.reQualifySiteRecord();
        Test.stopTest();
    }
    static testMethod void SiteQualification_CX_Vsite_WithLocId2(){
        testData();
        UnverifiedSitesList.get(0).Location_Id__c = null;
        insert UnverifiedSitesList;
        Id [] fixedSearchResults= new Id[20];
        for(Site__c site:UnverifiedSitesList){
            fixedSearchResults.add(site.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.StartTest();
        // Testing Visualforce Page -- Standard Controller -- Site Record
        PageReference AddSitePage = Page.AddSite;
        Test.setCurrentPage(AddSitePage);
        ApexPages.StandardController StndSiteContr = new ApexPages.standardController(UnverifiedSitesList.get(0));
        ApexPages.CurrentPage().getparameters().put('id',UnverifiedSitesList.get(0).Id);
        SiteQualification_CX SiteQualification_CX_Con = new SiteQualification_CX(StndSiteContr);
        responseHeaders.put('Content-Type','application/JSON');
        AddressSearch_CX.addressWrapper wrap = new AddressSearch_CX.addressWrapper();
        wrap.address = UnverifiedSitesList.get(0).Site_Address__c;
        wrap.locationId = 'LOC123456789012';
        wrap.addressFrom = 'Verified';
        wrap.sfRecordId = UnverifiedSitesList.get(0).Id;
        wrap.assetNumber = UnverifiedSitesList.get(0).Asset_Number__c;
        wrap.SiteSfRecordName = UnverifiedSitesList.get(0).Name;
        AddressSearch_CX.StructuredAddress StructuredAddress_Class = new AddressSearch_CX.StructuredAddress ();
        StructuredAddress_Class.unitNumber = '100';
        StructuredAddress_Class.levelNumber = '100';
        StructuredAddress_Class.lotNumber = '100';
        StructuredAddress_Class.roadNumber1 = '100';
        StructuredAddress_Class.roadName = 'Miller';
        StructuredAddress_Class.roadSuffixCode = 'RD';
        StructuredAddress_Class.localityName = 'North Sydney';
        StructuredAddress_Class.postCode = '2150';
        StructuredAddress_Class.stateTerritoryCode = 'NSW';
        StructuredAddress_Class.AssetNumber = '100';
        StructuredAddress_Class.AssetType = 'Cabinet';
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,jsonInput,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        SiteQualification_CX_Con.reQualifySiteRecord();
        SiteQualification_CX.getAddressSearchData(wrap, string.valueOf(UnverifiedSitesList.get(0).Id));
        SiteQualification_CX_Con.navigateToSite();
        SiteQualification_CX.createUnverifiedSite(StructuredAddress_Class);
        Test.stopTest();
    }
    static testMethod void SiteQualification_CX_Vsite_WithLocId3(){
        testData();
        UnverifiedSitesList.get(0).Location_Id__c = null;
        insert UnverifiedSitesList;
        Id [] fixedSearchResults= new Id[20];
        for(Site__c site:UnverifiedSitesList){
            fixedSearchResults.add(site.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);
        Test.StartTest();
        // Testing Visualforce Page -- Standard Controller -- Site Record
        PageReference AddSitePage = Page.AddSite;
        Test.setCurrentPage(AddSitePage);
        ApexPages.StandardController StndSiteContr = new ApexPages.standardController(UnverifiedSitesList.get(0));
        ApexPages.CurrentPage().getparameters().put('id',UnverifiedSitesList.get(0).Id);
        SiteQualification_CX SiteQualification_CX_Con = new SiteQualification_CX(StndSiteContr);
        responseHeaders.put('Content-Type','application/JSON');
        AddressSearch_CX.addressWrapper wrap = new AddressSearch_CX.addressWrapper();
        wrap.address = UnverifiedSitesList.get(0).Site_Address__c;
        wrap.locationId = 'LOC123456789012';
        wrap.addressFrom = 'Verified';
        wrap.sfRecordId = null;
        wrap.assetNumber = UnverifiedSitesList.get(0).Asset_Number__c;
        wrap.SiteSfRecordName = UnverifiedSitesList.get(0).Name;
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,jsonInput,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);
        SiteQualification_CX_Con.reQualifySiteRecord();
        SiteQualification_CX.getAddressSearchData(wrap, string.valueOf(UnverifiedSitesList.get(0).Id));
        Test.stopTest();
    }
    
     /*static testMethod void autoSiteQualificationPositiveTest(){
        testData();
        //create Customer Service Setting  
        Customer_Service_Setting__c css = new Customer_Service_Setting__c();
        css.Auto_Requalify_Limit__c = '100';
        css.Auto_Requalify_Retry_days__c = '60';
        css.SetupOwnerId = UserInfo.getOrganizationId();
        css.Unassigned_User_Id__c = UserInfo.getUserId();     
        insert css;
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        Site__c unverifiedSite = new Site__c();
        unverifiedSite.Location_Id__c = 'LOC123456789012';
        unverifiedSite.Name = 'LOC123456789012';
        unverifiedSite.RecordTypeID = recordTypeId;
        insert unverifiedSite;
        Test.StartTest();        
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,jsonInput,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);        
        AutoSiteQualification testRun = new AutoSiteQualification();
        database.executeBatch(testRun, 1);    
        Test.stopTest();
        Site__c updatedSite = [Select Id, RecordType.DeveloperName, Requalification_Failed_Date__c From Site__c Where ID =: unverifiedSite.Id];
        System.assertEquals(updatedSite.RecordType.DeveloperName, 'Verified');
        System.assertEquals(updatedSite.Requalification_Failed_Date__c, null);
    }   
    
     static testMethod void autoSiteQualificationNegativeTest(){
        testData();
        //create Customer Service Setting  
        Customer_Service_Setting__c css = new Customer_Service_Setting__c();
        css.Auto_Requalify_Limit__c = '100';
        css.Auto_Requalify_Retry_days__c = '60';
        css.SetupOwnerId = UserInfo.getOrganizationId();
        css.Unassigned_User_Id__c = UserInfo.getUserId();     
        insert css;
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        Site__c unverifiedSite = new Site__c();
        unverifiedSite.Location_Id__c = 'LOC123456789011';
        unverifiedSite.Name = 'LOC123456789012';
        unverifiedSite.RecordTypeID = recordTypeId;
        insert unverifiedSite;
        Test.StartTest();        
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,jsonInput,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);        
        AutoSiteQualification testRun = new AutoSiteQualification();
        database.executeBatch(testRun, 1);    
        Test.stopTest();
        Site__c updatedSite = [Select Id, RecordType.DeveloperName, Requalification_Failed_Date__c From Site__c Where ID =: unverifiedSite.Id];
        System.assertEquals(updatedSite.RecordType.DeveloperName, 'Unverified');
        System.assertEquals(updatedSite.Requalification_Failed_Date__c, Date.Today());
    }     
    
     static testMethod void autoSiteQualificationScheduleTest(){
        testData();    
        AutoSiteQualificationSchedule a = new AutoSiteQualificationSchedule();
        String cronStr = '0 15 * * * ?';
        System.schedule('Testing Schedule Job', cronStr, a);

     }
     
      static testMethod void autoSiteQualificationbatch2PositiveTest(){
        testData();
        //create Customer Service Setting  
        Customer_Service_Setting__c css = new Customer_Service_Setting__c();
        css.Auto_Requalify_limit_2__c = '10000';
        css.Auto_Requalify_Retry_days__c = '60';
        css.SetupOwnerId = UserInfo.getOrganizationId();
        css.Unassigned_User_Id__c = UserInfo.getUserId();     
        css.SiteJobAbortLimitInMins__c = 300;
        css.SiteQualificationClassID__c = [Select Id From ApexClass Where Name='AutoSiteQualificationBatch2'].Id;           
        insert css;
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        Site__c unverifiedSite = new Site__c();
        unverifiedSite.Location_Id__c = 'LOC123456789012';
        unverifiedSite.Name = 'LOC123456789012';
        unverifiedSite.RecordTypeID = recordTypeId;
        unverifiedSite.Require_Qualification__c = TRUE;          
        insert unverifiedSite;
        Test.StartTest();        
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,jsonInput,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);        
        AutoSiteQualificationBatch2 testRun = new AutoSiteQualificationBatch2();
        database.executeBatch(testRun, 1);    
        Test.stopTest();
        Site__c updatedSite = [Select Id, RecordType.DeveloperName, Requalification_Failed_Date__c, Require_Qualification__c From Site__c Where ID =: unverifiedSite.Id];
        System.assertEquals(updatedSite.RecordType.DeveloperName, 'Verified');
        System.assertEquals(updatedSite.Requalification_Failed_Date__c, null);
        System.assertEquals(updatedSite.Require_Qualification__c, FALSE);
    }   
    
     static testMethod void autoSiteQualificationbatch2NegativeTest(){
        testData();
        //create Customer Service Setting  
        Customer_Service_Setting__c css = new Customer_Service_Setting__c();
        css.Auto_Requalify_limit_2__c = '10000';
        css.Auto_Requalify_Retry_days__c = '60';
        css.SetupOwnerId = UserInfo.getOrganizationId();
        css.Unassigned_User_Id__c = UserInfo.getUserId();    
        css.SiteJobAbortLimitInMins__c = 300;
        css.SiteQualificationClassID__c = [Select Id From ApexClass Where Name='AutoSiteQualificationBatch2'].Id; 
        insert css;
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Unverified').getRecordTypeId();
        Site__c unverifiedSite = new Site__c();
        unverifiedSite.Location_Id__c = 'LOC123456789011';
        unverifiedSite.Name = 'LOC123456789012';
        unverifiedSite.RecordTypeID = recordTypeId;
        unverifiedSite.Require_Qualification__c = TRUE;
        insert unverifiedSite;
        Test.StartTest();        
        responseHeaders.put('Content-Type','application/JSON');
        QueueCRMoD_Response_Test fakeResponse1 = new QueueCRMoD_Response_Test(statusCode,jsonInput,responseHeaders);
        Test.setMock(HttpCalloutMock.class, fakeResponse1);        
        AutoSiteQualificationBatch2 testRun = new AutoSiteQualificationBatch2();
        database.executeBatch(testRun, 1);    
        Test.stopTest();
        Site__c updatedSite = [Select Id, RecordType.DeveloperName, Requalification_Failed_Date__c, Require_Qualification__c From Site__c Where ID =: unverifiedSite.Id];
        System.assertEquals(updatedSite.RecordType.DeveloperName, 'Unverified');
        System.assertEquals(updatedSite.Requalification_Failed_Date__c, Date.Today());
        System.assertEquals(updatedSite.Require_Qualification__c, TRUE);
    }     
    
     static testMethod void AutoSiteQualificationBatch2ScheduleTest(){
        testData();    
        AutoSiteQualificationBatch2Schedule a = new AutoSiteQualificationBatch2Schedule();
        String cronStr = '0 0 23 * * ?';
        System.schedule('Testing Schedule Job', cronStr, a);

     } */          
}