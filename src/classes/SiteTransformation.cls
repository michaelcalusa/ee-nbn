/*
Class Description
Creator: v-dileepathley
Purpose: This class will be used to transform and transfer Site records from Contacts_Int to Site and also to create the Site Contact records
Modifications:
*/
public class SiteTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
    public String siteQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {
        
    }
    public SiteTransformation(String ejId)
    {
         exJobId = ejId;
         Batch_Job__c bj = new Batch_Job__c();
         bj.Name = 'SiteTransformation'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
            siteQuery = 'Select CRM_Contact_Id__c,Unique_Site__c,Address_2__c,Address_3__c,City__c,FSA__c,Latitude__c,Longitude__c,MDU_LOC_ID__c,Premises_Type__c,Rollout_Type__c,SAM__c,Technology_Type__c,Country__c,Location_ID__c,Number_Street__c,Post_Code__c,State__c,Combined_Address__c FROM Site_Int__c where Transformation_Status__c = \'Extracted from CRM\' order by createddate asc';
            return Database.getQueryLocator(siteQuery);   
    }
    public void execute(Database.BatchableContext BC,List<Site_Int__c> SiteIlist)
    {
        try
        {
            String vsRecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Site__c'  and name = 'Verified'].Id;
            String uvsRecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Site__c'  and name = 'Unverified'].Id;
            
            list <Site__c> verified = new list<Site__c>();
            list <Site__c> uVerified = new list<Site__c>();
            list<Site_Contacts__c> siteCon = new list<Site_Contacts__c>();
            list <string> intSId = new list<string>();
            list <string> intUId = new list<string>();
            list <string> intSFConId = new list<string>();
            map<string,Site__c> uVSites = new map<string,Site__c>();
            map<string,Site__c> uUVSites = new map<string,Site__c>();
            map<string,Site_Contacts__c> scMap = new map<string,Site_Contacts__c>();
            list<Contact> contList = new list<Contact>();
            list<Site_Int__c> usucSite = new list<Site_Int__c>();
            list<Site_Int__c> sucSite = new list<Site_Int__c>();
            list<Contact> conList = new list<Contact>();
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            map<String,Contact> contMap = new map<String,Contact>();
            Boolean incomAdd = false;
            String address, lat, lng, fsa, sam, mlId, pType, rType, tType;
            
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.allowFieldTruncation = true;
            
            List<String> listOfConIds = new List<String>(); 
            List<String> listOfContIds = new List<String>(); 
            
            Map<String, Id> mapListOfCon = new Map<String, Id>();
            for(Site_Int__C sInt : SiteIlist)
            {                                
                listOfConIds.add(sInt.CRM_Contact_Id__c);     
            } 
            for(Contact con : [select On_Demand_Id_P2P__c, Id from Contact where On_Demand_Id_P2P__c in :listOfConIds FOR UPDATE])
            {
                mapListOfCon.put(con.On_Demand_Id_P2P__c, con.id);        
                listOfContIds.add(con.id); 
            } 
            
            for (Site_Contacts__c scExt : [select Id,Site_Contact_Unique__c,Is_Residential_Address__c from Site_Contacts__c where Related_Contact__c in :listOfContIds FOR UPDATE])
            {
                scExt.Is_Residential_Address__c = false;
                scMap.put(scExt.Site_Contact_Unique__c,scExt);
            }
            
            for(Site_Int__c con: SiteIlist)
            {
                incomAdd = false;
                if(!String.isBlank(con.Location_ID__c))
                {
                    Site__c verifiedSite = new Site__c();
                    verifiedSite.Location_Id__c=con.Location_ID__c;
                    verifiedSite.Site_Address__c=con.Combined_Address__c;
                    verifiedSite.Locality_Name__c=con.City__c;
                    if (String.isNotBlank(con.State__c))
                    {
                        if (con.State__c.toUpperCase() == 'ACT' ||con.State__c.toUpperCase() == 'NSW' || con.State__c.toUpperCase() == 'NT' || con.State__c.toUpperCase() == 'QLD' || 
                        con.State__c.toUpperCase() == 'TAS' || con.State__c.toUpperCase() == 'SA' || con.State__c.toUpperCase() == 'VIC' || con.State__c.toUpperCase() == 'WA')
                        {
                            verifiedSite.State_Territory_Code__c=con.State__c.toUpperCase();
                        }    
                        else
                        {
                            incomAdd = true;
                        }
                    }
                    else
                    {
                        incomAdd = true;
                    }
                  	if(String.isNotBlank(con.Post_Code__c))
                    {
                        if (ValidatePostCode.validate(con.Post_Code__c))
                        {
                            verifiedSite.Post_Code__c=con.Post_Code__c;
                        }
                        else
                        {
                            incomAdd = true;
                        }
                    }
                    else
                    {
                        incomAdd = true;
                    }
                    verifiedSite.Road_Number_1__c=con.Number_Street__c;
                    verifiedSite.Road_Number_2__c=con.Address_2__c;
                    verifiedSite.Road_Name__c = con.Address_3__c;
                    verifiedSite.RecordTypeId=vsRecordTypeId;
                    verifiedSite.MDU_CP_On_Demand_Id__c = con.Location_ID__c;
                    verifiedSite.Name= con.Combined_Address__c;
                    verifiedSite.FSA__c = con.FSA__c;
                    verifiedSite.Latitude__c = con.Latitude__c;
                    verifiedSite.Longitude__c = con.Longitude__c;
                    verifiedSite.MDU_LocId__c = con.MDU_LOC_ID__c;
                    verifiedSite.Premises_Type__c = con.Premises_Type__c;
                    verifiedSite.Rollout_Type__c = con.Rollout_Type__c;
                    verifiedSite.SAM__c = con.SAM__c;
                    verifiedSite.Technology_Type__c = con.Technology_Type__c;
                    verifiedSite.isSiteCreationThroughCode__c =true;
                    verifiedSite.CRMOD_Contact_Site__c = true;
                    verifiedSite.setOptions(dml);
                    if (!incomAdd)
                    {
                        uVSites.put(con.Location_ID__c,verifiedSite);
                    }
                    else
                    {
                        con.Transformation_Status__c = 'Error Site Creation';
                        con.Schedule_Job_Transformation__c = bjId;
                        usucSite.add(con);
                        address = String.isNotBlank(con.Combined_Address__c) ?' Address: '+ con.Combined_Address__c : '';
                        lat = String.isNotBlank(con.Latitude__c) ?' Latitude: ' + con.Latitude__c : '' ;
                        lng = String.isNotBlank(con.Longitude__c) ?' Longitude: ' + con.Longitude__c : '';
                        fsa = String.isNotBlank(con.FSA__c) ?' FSA: ' + con.FSA__c : '';
                        sam = String.isNotBlank(con.SAM__c) ?' SAM: ' + con.SAM__c : '';
                        mlId = String.isNotBlank(con.MDU_LOC_ID__c) ?' MDU LOC Id: ' + con.MDU_LOC_ID__c : '';
                        pType = String.isNotBlank(con.Premises_Type__c) ?' Premise Type: ' + con.Premises_Type__c : '';
                        rType = String.isNotBlank(con.Rollout_Type__c) ?' Rollout Type: ' + con.Rollout_Type__c : '';
                        tType = String.isNotBlank(con.Technology_Type__c) ?' Technology Type:'+con.Technology_Type__c : '';
                        contMap.put(mapListOfCon.get(con.CRM_Contact_Id__c),new Contact(Id = mapListOfCon.get(con.CRM_Contact_Id__c),Incomplete_Address__c= 'Location Id:'+con.Location_ID__c+ address +lat+lng +fsa+sam +mlId +pType+rType+tType));
                        errList.add(new Error_Logging__c(Name='Site_Tranformation',Error_Message__c= 'not a valid Post Code',CRM_Record_Id__c =con.Id,Schedule_Job__c=bjId));
                    }
                }
                else
                {
                    Site__c uverifiedSite = new Site__c();
                    uverifiedSite.Site_Address__c=con.Combined_Address__c;
                    uverifiedSite.Locality_Name__c=con.City__c;
                     if (con.State__c.toUpperCase() == 'ACT' ||con.State__c.toUpperCase() == 'NSW' || con.State__c.toUpperCase() == 'NT' || con.State__c.toUpperCase() == 'QLD' || 
                        con.State__c.toUpperCase() == 'TAS' || con.State__c.toUpperCase() == 'SA' || con.State__c.toUpperCase() == 'VIC' || con.State__c.toUpperCase() == 'WA')
                    {
                        uverifiedSite.State_Territory_Code__c=con.State__c.toUpperCase();
                    }    
                    else
                    {
                        incomAdd = true;
                    }
                    if(String.isNotBlank(con.Post_Code__c))
                    {
                        if (ValidatePostCode.validate(con.Post_Code__c))
                        {
                            uverifiedSite.Post_Code__c=con.Post_Code__c;
                        }
                        else
                        {
                            incomAdd = true;
                        }
                    }
                    uverifiedSite.Road_Number_1__c=con.Number_Street__c;
                    uverifiedSite.Road_Number_2__c=con.Address_2__c;
                    uverifiedSite.Road_Name__c = con.Address_3__c;
                    uverifiedSite.Site_Address_Unique__c=con.Combined_Address__c.toUpperCase();
                    uverifiedSite.RecordTypeId=uvsRecordTypeId;
                    uverifiedSite.Name= con.Combined_Address__c;
                    uverifiedSite.FSA__c = con.FSA__c;
                    uverifiedSite.Latitude__c = con.Latitude__c;
                    uverifiedSite.Longitude__c = con.Longitude__c;
                    uverifiedSite.MDU_LocId__c = con.MDU_LOC_ID__c;
                    uverifiedSite.Premises_Type__c = con.Premises_Type__c;
                    uverifiedSite.Rollout_Type__c = con.Rollout_Type__c;
                    uverifiedSite.SAM__c = con.SAM__c;
                    uverifiedSite.Technology_Type__c = con.Technology_Type__c;
                    uverifiedSite.isSiteCreationThroughCode__c =true;
                    uverifiedSite.CRMOD_Contact_Site__c = true;
                    uverifiedSite.setOptions(dml);
                    if (!incomAdd)
                    {
                        uUVSites.put(con.Combined_Address__c.toUpperCase(),uverifiedSite);
                    }
                    else
                    {
                        con.Transformation_Status__c = 'Error Site Creation';
                        con.Schedule_Job_Transformation__c = bjId;
                        usucSite.add(con);
                        address = String.isNotBlank(con.Combined_Address__c) ?'Address: '+ con.Combined_Address__c : '';
                        lat = String.isNotBlank(con.Latitude__c) ?' Latitude: ' + con.Latitude__c : '' ;
                        lng = String.isNotBlank(con.Longitude__c) ?' Longitude: ' + con.Longitude__c : '';
                        fsa = String.isNotBlank(con.FSA__c) ?' FSA: ' + con.FSA__c : '';
                        sam = String.isNotBlank(con.SAM__c) ?' SAM: ' + con.SAM__c : '';
                        mlId = String.isNotBlank(con.MDU_LOC_ID__c) ?' MDU LOC Id: ' + con.MDU_LOC_ID__c : '';
                        pType = String.isNotBlank(con.Premises_Type__c) ?' Premise Type: ' + con.Premises_Type__c : '';
                        rType = String.isNotBlank(con.Rollout_Type__c) ?' Rollout Type: ' + con.Rollout_Type__c : '';
                        tType = String.isNotBlank(con.Technology_Type__c) ?' Technology Type:'+con.Technology_Type__c : '';
                        contMap.put(mapListOfCon.get(con.CRM_Contact_Id__c),new Contact(Id = mapListOfCon.get(con.CRM_Contact_Id__c),Incomplete_Address__c= address +lat+lng +fsa+sam +mlId +pType+rType+tType));
                        errList.add(new Error_Logging__c(Name='Site_Tranformation',Error_Message__c= 'not a valid Post Code/State',CRM_Record_Id__c =con.Id,Schedule_Job__c=bjId));
                    }
                }
            }
            verified = uVSites.values();
            uVerified = uUVSites.values();
            Schema.SObjectField locId = Site__c.Fields.MDU_CP_On_Demand_Id__c;
            List<Database.upsertResult> uVSResults = Database.upsert(verified,locId,false);
            Schema.SObjectField siteAdd = Site__c.Fields.Site_Address_Unique__c;
            List<Database.upsertResult> uUVSResults = Database.upsert(uVerified,siteAdd,false);
            
            list<Database.Error> err;
            String msg, fAffected;
            String[]fAff;
            StatusCode sCode;
            
            
            for(Integer idx = 0; idx < uVSResults.size(); idx++)
            {   
                if(!uVSResults[idx].isSuccess())
                {
                    err=uVSResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='Site_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uVSResults[idx].isCreated(),
                    isSuccess__c=uVSResults[idx].isSuccess(),Record_Row_Id__c=uVSResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c = verified[idx].Location_Id__c,Schedule_Job__c=bjId));
                    intUId.add(verified[idx].Location_Id__c);
                }
                else
                {
                    for (Site_Int__c conInt: siteIlist)
                    {
                        if(!String.isBlank(conInt.Location_Id__c))
                        {
                            if(verified[idx].Location_Id__c == conInt.Location_Id__c)
                            {
                                scMap.put(mapListOfCon.get(conInt.CRM_Contact_Id__c)+' '+uVSResults[idx].getId(),new Site_Contacts__c(Related_Contact__c=mapListOfCon.get(conInt.CRM_Contact_Id__c),Related_Site__c=uVSResults[idx].getId(),Is_Residential_Address__c=true,Site_Contact_Unique__c=mapListOfCon.get(conInt.CRM_Contact_Id__c)+' '+uVSResults[idx].getId()));
                                contMap.put(mapListOfCon.get(conInt.CRM_Contact_Id__c),new Contact(Id = mapListOfCon.get(conInt.CRM_Contact_Id__c),Residential_Address__c=uVSResults[idx].getId()));
                                intSId.add(verified[idx].Location_Id__c);
                            }
                        }
                    }
                    
                }
            }
            for(Integer idx = 0; idx < uUVSResults.size(); idx++)
            {   
                if(!uUVSResults[idx].isSuccess())
                {
                    err=uUVSResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='Site_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uUVSResults[idx].isCreated(),
                    isSuccess__c=uUVSResults[idx].isSuccess(),Record_Row_Id__c=uUVSResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=uVerified[idx].Site_Address_Unique__c,Schedule_Job__c=bjId));
                    intUId.add(uVerified[idx].Site_Address_Unique__c);
                }
                else
                {
                    for (Site_Int__c conInt: siteIlist)
                    {
                        if(!String.isBlank(conInt.Combined_Address__c))
                        {
                            if(uVerified[idx].Site_Address_Unique__c == conInt.Unique_Site__c)
                            {
                                scMap.put(mapListOfCon.get(conInt.CRM_Contact_Id__c)+' '+uUVSResults[idx].getId(),new Site_Contacts__c(Related_Contact__c=mapListOfCon.get(conInt.CRM_Contact_Id__c),Related_Site__c=uUVSResults[idx].getId(),Is_Residential_Address__c=true,Site_Contact_Unique__c=mapListOfCon.get(conInt.CRM_Contact_Id__c)+' '+uUVSResults[idx].getId()));
                                contMap.put(mapListOfCon.get(conInt.CRM_Contact_Id__c),new Contact(Id = mapListOfCon.get(conInt.CRM_Contact_Id__c),Residential_Address__c=uUVSResults[idx].getId()));
                                intSId.add(uVerified[idx].Site_Address_Unique__c);
                            }
                        }
                    }
                    
                }
            }
            
            Schema.SObjectField scUnique = Site_Contacts__c.Fields.Site_Contact_Unique__c;
            siteCon = scMap.values();
            List<Database.upsertResult> uSCResults = Database.upsert(siteCon,scUnique,false);
            
            for(Integer idx = 0; idx < uSCResults.size(); idx++)
            {   
                if(!uSCResults[idx].isSuccess())
                {
                    err=uSCResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='Contact_Site_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uSCResults[idx].isCreated(),
                    isSuccess__c=uSCResults[idx].isSuccess(),Record_Row_Id__c=uSCResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=siteCon[idx].Related_Contact__c,Schedule_Job__c=bjId));
                }
                else
                {
                    intSFConId.add(siteCon[idx].Related_Contact__c);
                }
            }
            contList = contMap.values();
            List<Database.upsertResult> uCResults = Database.upsert(contList,false);
            
            for(Integer idx = 0; idx < uCResults.size(); idx++)
            {   
                if(!uCResults[idx].isSuccess())
                {
                    err=uCResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='Contact_Site_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uCResults[idx].isCreated(),
                    isSuccess__c=uCResults[idx].isSuccess(),Record_Row_Id__c=uCResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=contList[idx].Id,Schedule_Job__c=bjId));
                }
                else
                {
                }
            }
            
            conList = [select On_Demand_Id_P2P__c from contact where id in: intSFConId]; 
            
            for (Site_Int__c cSite: [select id,CRM_Contact_Id__c,Transformation_Status__c from Site_Int__c where Transformation_Status__c ='Extracted from CRM' and Unique_Site__c in : intSId])
            {
                for (Contact odId: conList)
                {
                    if(odId.On_Demand_Id_P2P__c == cSite.CRM_Contact_Id__c)
                    {
                        cSite.Transformation_Status__c = 'Copied to Base Table';    
                        cSite.Schedule_Job_Transformation__c = bjId;
                    }
                }
                if(cSite.Transformation_Status__c == 'Extracted from CRM')
                {
                    cSite.Transformation_Status__c = 'Error Site Contact Creation';
                    cSite.Schedule_Job_Transformation__c = bjId;
                }
                sucSite.add(cSite);
            }
            
            
            for (Site_Int__c ucSite: [select id,Transformation_Status__c from Site_Int__c where Transformation_Status__c ='Extracted from CRM' and Unique_Site__c in : intUId])
            {
                ucSite.Transformation_Status__c = 'Error Site Creation';
                ucSite.Schedule_Job_Transformation__c = bjId;
                usucSite.add(ucSite);
            }
            update sucSite;
            update usucSite;
            Insert errList;
             if(errList.isEmpty())      
            {
                status = 'Completed';
            } 
            else
            {
                status = 'Error';
            }  
            recordCount = recordCount + siteIlist.size();     
        }
        catch (Exception e)
        {
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='Contact_Site_Tranformation',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob siteJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
        {
            bj.Status__c= siteJob.Status;
        }
        else
        {
             bj.Status__c=status;
        }
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = siteJob.id;
        bj.End_Time__c  = siteJob.CompletedDate;
        upsert bj;
    }   

}