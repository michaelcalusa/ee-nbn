/***************************************************************************************************
Class Name:  SiteTransformation_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 02-11-2016
Function    : This class contains unit test scenarios for SiteTransformation apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir         02-11-2016                Created
****************************************************************************************************/
@isTest
public class SiteTransformation_Test {
    static List<Site_Int__c> siteRecList = new List<Site_Int__c>();
    static void getRecords (){
        // Custom Setting record creation
        TestDataUtility.getListOfCRMTransformationJobRecords(true);
        // Create Junction_Object_Int__c record
        date CRM_Modified_Date = date.newinstance(1963, 01,24);
        List<Junction_Object_Int__c> actintList = new List<Junction_Object_Int__c>();
        Junction_Object_Int__c actint1 = new Junction_Object_Int__c();
        actint1.Name='TestName';
        actint1.CRM_Modified_Date__c=CRM_Modified_Date;
        actint1.Transformation_Status__c = 'Exported from CRM';
        actint1.Object_Name__c = 'Account';
        actint1.Child_Name__c = 'Contact';
        actint1.Event_Name__c = 'Associate';
        actint1.Object_Id__c = 'Account123';
        actint1.Child_Id__c = 'Contact123';
        actintList.add(actint1);
        insert actintList;
        // Create Contact record
        Contact contactRec = new Contact ();
        contactRec.on_demand_id_P2P__c = 'Contact123';
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        insert contactRec;
    }
    static void getSiteRecords (){
        Site_Int__c siteRec1= new Site_Int__c();
        siteRec1.CRM_Contact_Id__c = 'Contact123';
        siteRec1.Address_2__c = 'Test';
        siteRec1.Address_3__c = 'Test';
        siteRec1.City__c = 'Sydney';
        siteRec1.FSA__c = 'Test';
        siteRec1.Latitude__c = '1212';
        siteRec1.Longitude__c = '1234';
        siteRec1.MDU_LOC_ID__c = 'Loc123';
        siteRec1.Premises_Type__c = 'Residential';
        siteRec1.Rollout_Type__c = 'test';
        siteRec1.SAM__c = 'Test';
        siteRec1.Technology_Type__c = 'Test';
        siteRec1.Country__c = 'AUS';
        siteRec1.Location_ID__c= 'LOC123';
        siteRec1.Number_Street__c= '123';
        siteRec1.Post_Code__c ='1234';
        siteRec1.State__c ='TAS';
        siteRec1.Transformation_Status__c='Extracted from CRM';
        siteRecList.add(siteRec1);
        Site_Int__c siteRec2 = new Site_Int__c();
        siteRec2.CRM_Contact_Id__c = 'Contact12345';
        siteRec2.Address_2__c = 'Test';
        siteRec2.Address_3__c = 'Test';
        siteRec2.City__c = 'Sydney';
        siteRec2.FSA__c = 'Test';
        siteRec2.Latitude__c = '1212';
        siteRec2.Longitude__c = '1234';
        siteRec2.MDU_LOC_ID__c = 'Loc12345';
        siteRec2.Premises_Type__c = 'Residential';
        siteRec2.Rollout_Type__c = 'test';
        siteRec2.SAM__c = 'Test';
        siteRec2.Technology_Type__c = 'Test';
        siteRec2.Country__c = 'AUS';
        siteRec2.Location_ID__c= 'LOC12345';
        siteRec2.Number_Street__c= '123';
        siteRec2.Post_Code__c ='123456';
        siteRec2.State__c ='TN';
        siteRec2.Transformation_Status__c='Extracted from CRM';
        siteRecList.add(siteRec2);
        Site_Int__c siteRec3 = new Site_Int__c();
        siteRec3.CRM_Contact_Id__c = 'Contact123456';
        siteRec3.Address_2__c = 'Test';
        siteRec3.Address_3__c = 'Test';
        siteRec3.City__c = 'Sydney';
        siteRec3.FSA__c = 'Test';
        siteRec3.Latitude__c = '1212';
        siteRec3.Longitude__c = '1234';
        siteRec3.MDU_LOC_ID__c = 'Loc123456';
        siteRec3.Premises_Type__c = 'Residential';
        siteRec3.Rollout_Type__c = 'test';
        siteRec3.SAM__c = 'Test';
        siteRec3.Technology_Type__c = 'Test';
        siteRec3.Country__c = 'AUS';
        siteRec3.Location_ID__c= 'LOC123456';
        siteRec3.Number_Street__c= '123';
        siteRec3.Transformation_Status__c='Extracted from CRM';
        siteRecList.add(siteRec3);
        Site_Int__c siteRec4 = new Site_Int__c();
        siteRec4.CRM_Contact_Id__c = 'Contact123456';
        siteRec4.Address_2__c = 'Test';
        siteRec4.Address_3__c = 'Test';
        siteRec4.City__c = 'Sydney';
        siteRec4.FSA__c = 'Test';
        siteRec4.Latitude__c = '1212';
        siteRec4.Longitude__c = '1234';
        siteRec4.Premises_Type__c = 'Residential';
        siteRec4.Rollout_Type__c = 'test';
        siteRec4.SAM__c = 'Test';
        siteRec4.Technology_Type__c = 'Test';
        siteRec4.Country__c = 'AUS';
        siteRec4.Number_Street__c= '123';
        siteRec4.Transformation_Status__c='Extracted from CRM';
        siteRec4.Post_Code__c ='1234';
        siteRec4.State__c ='VIC';
        siteRecList.add(siteRec4);
        Site_Int__c siteRec6 = new Site_Int__c();
        siteRec6.CRM_Contact_Id__c = 'Contact123456';
        siteRec6.Address_2__c = 'Test';
        siteRec6.Address_3__c = 'Test';
        siteRec6.City__c = 'Sydney';
        siteRec6.FSA__c = 'Test';
        siteRec6.Latitude__c = '1212';
        siteRec6.Longitude__c = '1234';
        siteRec6.Premises_Type__c = 'Residential';
        siteRec6.Rollout_Type__c = 'test';
        siteRec6.SAM__c = 'Test';
        siteRec6.Technology_Type__c = 'Test';
        siteRec6.Country__c = 'AUS';
        siteRec6.Number_Street__c= '123';
        siteRec6.Transformation_Status__c='Extracted from CRM';
        siteRec6.Post_Code__c ='1234';
        siteRec6.State__c ='TN';
        siteRecList.add(siteRec6);
        
    }
    static testMethod void SiteTransformation_Test1(){
        getRecords();
        getSiteRecords();
        insert siteRecList;
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());   
        Test.stopTest();
    }
    static testMethod void SiteTransformation_Test2(){
        getRecords();
        getSiteRecords();
        Site_Int__c siteRec5 = new Site_Int__c();
        siteRec5.CRM_Contact_Id__c = 'Contact123456';
        siteRec5.Address_2__c = 'Test';
        siteRec5.Address_3__c = 'Test';
        siteRec5.City__c = 'Sydney';
        siteRec5.FSA__c = 'Test';
        siteRec5.Latitude__c = '1212';
        siteRec5.Longitude__c = '1234';
        siteRec5.Premises_Type__c = 'Residential';
        siteRec5.Rollout_Type__c = 'test';
        siteRec5.SAM__c = 'Test';
        siteRec5.Technology_Type__c = 'Test';
        siteRec5.Country__c = 'AUS';
        siteRec5.Number_Street__c= '123';
        siteRec5.Transformation_Status__c='Extracted from CRM';
        siteRec5.Post_Code__c ='123456';
        Site_Int__c siteRec6 = new Site_Int__c();
        siteRec6.CRM_Contact_Id__c = 'Contact123456';
        siteRec6.Address_2__c = 'Test';
        siteRec6.Address_3__c = 'Test';
        siteRec6.City__c = 'Sydney';
        siteRec6.FSA__c = 'Test';
        siteRec6.Latitude__c = '1212';
        siteRec6.Longitude__c = '1234';
        siteRec6.Premises_Type__c = 'Residential';
        siteRec6.Rollout_Type__c = 'test';
        siteRec6.SAM__c = 'Test';
        siteRec6.Technology_Type__c = 'Test';
        siteRec6.Country__c = 'AUS';
        siteRec6.Number_Street__c= '123';
        siteRec6.Transformation_Status__c='Extracted from CRM';
        siteRec6.Post_Code__c ='ANCFDD';
        siteRecList.add(siteRec6);
        insert siteRecList;
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());   
        Test.stopTest();
    }
}