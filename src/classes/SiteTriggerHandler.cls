/***************************************************************************************************
Version     : 1.0 
Created Date: 25-10-2016 
Description/Function    :  Used in SiteTrigger Apex Trigger
Used in     : 
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN        25-10-2016           Created the Class
****************************************************************************************************/
public class SiteTriggerHandler{
    //Class level variables that are commonly used
    private boolean isExecuting = false;
    private integer batchSize;
    private List<Site__c> trgOldList = new List<Site__c> ();
    private List<Site__c> trgNewList = new List<Site__c> ();
    private Map<id,Site__c> trgOldMap = new Map<id,Site__c> ();
    private Map<id,Site__c> trgNewMap = new Map<id,Site__c> ();
    private boolean isBefore;
    private boolean isAfter;
    private boolean isInsert;
    private boolean isUpdate;
    private boolean isDelete;
    private boolean isUnDelete;
    private static boolean isAsync;
    // Below 7 boolean variables are used to Prevent recursion
    public static boolean isBeforeInsertFirstRun = true;
    public static boolean isBeforeUpdateFirstRun = true;
    public static boolean isBeforeDeleteFirstRun = true;
    public static boolean isAfterInsertFirstRun = true;
    public static boolean isAfterUpdateFirstRun = true;
    public static boolean isAfterDeleteFirstRun = true;
    public static boolean isAfterUndeleteFirstRun = true;
    /***************************************************************************************************
    Method Name:  SiteTriggerHandler
    Method Type: Constructor
    Version     : 1.0 
    Created Date: 25-10-2016 
    Function    : used to assign the Trigger values to the Class variables
    Input Parameters: 
    Output Parameters: None
    Description:   
    Used in     : "SiteTrigger" Trigger
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       25-10-2016             Created
    ****************************************************************************************************/  
    public SiteTriggerHandler(boolean isExecuting, integer batchSize, List<Site__c> trgOldList, List<Site__c> trgNewList, Map<id,Site__c> trgOldMap, Map<id,Site__c> trgNewMap, boolean isBefore, boolean isAfter, boolean isInsert, boolean isUpdate, boolean isDelete, boolean isUnDelete){
        this.isExecuting = isExecuting;
        this.BatchSize = batchSize;
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
        this.isBefore = isBefore;
        this.isAfter = isAfter;
        this.isInsert = isInsert;
        this.isUpdate = isUpdate;
        this.isDelete = isDelete;
        this.isUnDelete = isUnDelete;
        SiteTriggerHandler.isAsync = System.isBatch() || System.isFuture();
    }
    public void OnAfterInsert(){
        if(!HelperUtility.isTriggerMethodExecutionDisabled('findSiteDuplicate')){
            if(isAfterInsertFirstRun){
                findSiteDuplicate(trgNewList);
                isAfterInsertFirstRun = false;
            }
        }
    }
    public void OnBeforeUpdate(){
        if(!HelperUtility.isTriggerMethodExecutionDisabled('findSiteDuplicate')){
            if(isBeforeUpdateFirstRun){
                findSiteDuplicate(trgNewList);
                isBeforeUpdateFirstRun = false;
            }
        }
    }
    public void OnBeforeDelete(){}
    public void OnBeforeInsert(){}
    public void OnAfterUpdate(){}
    public void OnAfterDelete(){}
    public void OnUndelete(){}
    /*---------------------------------------------------------------------------------------------------------------------------------------------------*/
    // Business Logic Method
    /***************************************************************************************************
    Method Name:  findSiteDuplicate
    Method Type: method
    Version     : 1.0 
    Created Date: 25-10-2016 
    Function    : used to find the Site Duplicate in Salesforce. Site Uniqueness is considered only for Verified and Unverified Recordtype. User story : ICRM-1756
    Input Parameters: List<Site__c>
    Output Parameters: None
    Description:   
    Used in     : "OnAfterInsert" Method and "OnBeforeUpdate" Method 
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       25-10-2016             Created
    ****************************************************************************************************/  
    public void findSiteDuplicate(List<Site__c> trgNewList){
        Set<String> setofLocationIds = new Set<String> ();
        Set<String> setofMDUCPLocationIds = new Set<String> ();
        Id MDU_CP_RecordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('MDU/CP').getRecordTypeId();
        for(Site__c siteRecord : trgNewList){
            if(String.isNotBlank(siteRecord.Location_Id__c) && String.isNotBlank(siteRecord.RecordTypeId) && (siteRecord.RecordTypeId != MDU_CP_RecordTypeId))
                setofLocationIds.add(siteRecord.Location_Id__c);
            if(String.isNotBlank(siteRecord.Location_Id__c) && String.isNotBlank(siteRecord.RecordTypeId) && (siteRecord.RecordTypeId == MDU_CP_RecordTypeId))
                setofMDUCPLocationIds.add(siteRecord.Location_Id__c);
        }
        Map<string, site__c> mapOfSites = new Map<string, site__c> ();
        for(Site__c siteRecord : [SELECT Id, Name, Location_Id__c FROM Site__c WHERE Location_Id__c IN :setofLocationIds AND ID NOT IN: trgNewList AND (RecordType.Name =: GlobalConstants.SITE_VERIFIED OR RecordType.Name =: GlobalConstants.SITE_UNVERIFIED)]){
            mapOfSites.put(siteRecord.Location_Id__c, siteRecord);
        }
        //MDUCP Duplicate https://jira-cc.slb.nbndc.local/browse/FY17-306 
        Map<string, site__c> mapOfMDUCPSites = new Map<string, site__c> ();
        for(Site__c siteRecord : [SELECT Id, Name, Location_Id__c FROM Site__c WHERE Location_Id__c IN :setofMDUCPLocationIds AND ID NOT IN: trgNewList AND RecordType.Name =: GlobalConstants.SITE_MDUCP]){
            mapOfMDUCPSites.put(siteRecord.Location_Id__c, siteRecord);
        }
        for(Site__c siteRecord : trgNewList){
            if(String.isNotBlank(siteRecord.Location_Id__c) && mapOfSites.get(siteRecord.Location_Id__c) <> null && String.isNotBlank(siteRecord.RecordTypeId) && (siteRecord.RecordTypeId != MDU_CP_RecordTypeId))
                siteRecord.addError(HelperUtility.getErrorMessage('031')+' "'+mapOfSites.get(siteRecord.Location_Id__c).Name+'"', false);
            
            if(String.isNotBlank(siteRecord.Location_Id__c) && mapOfMDUCPSites.get(siteRecord.Location_Id__c) <> null && String.isNotBlank(siteRecord.RecordTypeId) && (siteRecord.RecordTypeId == MDU_CP_RecordTypeId))
                siteRecord.addError(HelperUtility.getErrorMessage('031')+' "'+mapOfMDUCPSites.get(siteRecord.Location_Id__c).Name+'"', false);
        }
    }
}