/***************************************************************************************************
Class Name:  SiteTriggerHandler_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 03-11-2016
Function    : This class contains unit test scenarios for  SiteTriggerHandler apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Hari Kalannagari         03-11-2016                Created
****************************************************************************************************/
@isTest
private class SiteTriggerHandler_Test {
    static testMethod void test(){
        //create Site record
        Site__c siteObj = new Site__c();
        siteObj.Name='SampleSite';
        siteObj.Location_Id__c='LOC122345678909';
        insert siteObj;
    }
    static testMethod void test2(){
        //create Site record
               
        try{
            Site__c siteObj = new Site__c();
            siteObj.Name='TestSite';
            insert siteObj;
            
            Site__c siteObj1 = new Site__c();
            siteObj1.Name='TestSite';
            insert siteObj1;
            
            siteObj.Name='TestSite';
            update siteObj;
         }catch(Exception ex){
             system.assert(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
         }
    }
}