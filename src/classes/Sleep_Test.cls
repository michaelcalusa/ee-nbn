/***************************************************************************************************
Class Name:  Sleep_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : 
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Sukumar       26/10/2015                 Created
****************************************************************************************************/
@isTest
private class Sleep_Test{
       
    static testMethod void Sleep_SuccessScenario(){
    
    Test.startTest();
        
    Sleep s =  new Sleep(0000001L);
    Test.stopTest();
    }
    }