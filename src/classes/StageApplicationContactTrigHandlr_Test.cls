/***************************************************************************************************
Class Name:  StageApplicationContactTrigHandlr_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 03-11-2016
Function    : This class contains unit test scenarios for  StageApplicationContactTriggerHandler apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Hari Kalannagari         03-11-2016                Created
****************************************************************************************************/
@isTest
public class StageApplicationContactTrigHandlr_Test {
    static User testRunningUser = new User ();
    static Contact conObj = new Contact();
    static Stage_Application__c stageAppObj = new Stage_Application__c();
    /***************************************************************************************************
    Method Name:  getRecords
    Method Type: Constructor
    Version     : 1.0 
    Created Date: 13 July 2016
    Description:  Common Test data used for all test methods
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Syed Moosa Nazir TN       13/07/2015                Created
    ****************************************************************************************************/
    static void getRecords (){
        // Custom Setting - "Error Message" Records
        List<Error_Message__c> ListofErrorMessage = TestDataUtility.getListOfErrorMessage(true);
        // Add all the Profile names which are required for testing.
        List<string> listOfProfileName = new List<string> {'System Administrator'};
        // Query Profiles
        list<Profile> listOfProfiles = TestDataUtility.getListOfProfiles(listOfProfileName);
        // Instantiate User and assign values. Not creating User
        testRunningUser = TestDataUtility.createTestUser(true,listOfProfiles.get(0).id);
        // Create Account
        Account actObj = new Account();
        actObj.name = 'Test Account';
        insert actObj;
        // Create Contact
        conObj.LastName = 'testLastName';
        insert conObj;
        // Create Development
        Development__c devObj = new Development__c();
        devObj.Name='testName';
        devObj.Development_ID__c='AKHS833';
        devObj.Account__c=actObj.Id;
        devObj.Primary_Contact__c=conObj.Id;
        devObj.Suburb__c='testsuburd';
        insert devObj;
		
		SA_Auto_Reference_Number__c refNumber = new SA_Auto_Reference_Number__c(Name = 'SA001', Last_Sequence_Number__c = '000000000');
            insert refNumber;
        // Create Stage Application
        stageAppObj.Name='testName';
        stageAppObj.Active_Status__c='Active';
        stageAppObj.Dwelling_Type__c='SDU Only';
        stageAppObj.Development__c=devObj.Id;
        stageAppObj.Request_ID__c='SampleReqId';
        insert stageAppObj;
    }
    
    static testMethod void StageApplicationContactTrigHandlr_Test1(){
        getRecords();
        // Test Trigger Functionality
        Test.startTest();
        System.runAs(testRunningUser){
            // Create Stage Application Contact
            StageApplication_Contact__c stageappconObj = new StageApplication_Contact__c();
            stageappconObj.Contact__c=conObj.Id;
            stageappconObj.Stage_Application__c=stageAppObj.Id;
            // DML Operations
            insert stageappconObj;
            update stageappconObj;
            delete stageappconObj;
            undelete stageappconObj;
        }
        Test.stopTest();
    }
    static testMethod void StageApplicationContactTrigHandlr_Test2(){
        getRecords();
        conObj.On_Demand_Id__c = 'test123';
        update conObj;
        // Test Trigger Functionality
        Test.startTest();
        System.runAs(testRunningUser){
            // Create Stage Application Contact
            StageApplication_Contact__c stageappconObj = new StageApplication_Contact__c();
            stageappconObj.Contact__c=conObj.Id;
            stageappconObj.Stage_Application__c=stageAppObj.Id;
            // DML Operations
            insert stageappconObj;
            try{
                delete stageappconObj;
            }
            catch(Exception Ex){
                boolean expectedExceptionThrown =  Ex.getMessage().contains('ErrorMessage6') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
        }
        Test.stopTest();
    }
}