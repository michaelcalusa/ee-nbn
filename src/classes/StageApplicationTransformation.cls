/*
Class Description
Creator: Gnanasambantham M (gnanasambanthammurug)
Purpose: This class will be used to transform and transfer Stage Application records from Stage_Application_Int to Stage_Application__c
Modifications:
*/
public class StageApplicationTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
    public String stageAppIntQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {            
    }
    public StageApplicationTransformation(String ejId)
    {
         exJobId = ejId;
         Batch_Job__c bj = new Batch_Job__c();
         bj.Name = 'StageApplicationTransform'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        stageAppIntQuery = 'select Id,Request_Id__c,Latitude__c,Application_Name__c,Account__c,Account_Id__c,Primary_Contact__c,Primary_Contact_Id__c,Dwelling_Type__c,Address__c,Suburb__c,State__c,Post_Code__c,Estimated_First_Service_Connection_Date__c,Active_Status__c,No_of_Premises__c,OLT__c,DLT_Owner__c,Application_Status__c,Development_Name__c,Development_Id__c,FDA__c,FSA__c,SAM__c,Longitude__c,Estimated_RSP_Ready_to_Order_Date__c,Estimated_Ready_for_Service_Date__c,Target_Delivery_Date__c,Local_Technology__c from Stage_Application_Int__c where Transformation_Status__c = \'Extracted from CRM\' order by createddate asc'; 
        return Database.getQueryLocator(stageAppIntQuery);    
        
    }
    public void execute(Database.BatchableContext BC,List<Stage_Application_Int__c> listOfSAInt)
    {
      try
        {           

            List<String> listOfConIds = new List<String>();  
            List<String> listOfAccIds = new List<String>(); 
            List<String> listOfDevIds = new List<String>();
            Boolean tStatus = false;
            Map<String, Id> mapListOfCon = new Map<String, Id>();
            for(Stage_Application_Int__c si : listOfSAInt)
            {                                
                if(!String.isBlank(si.Primary_Contact_Id__c) && si.Primary_Contact_Id__c != 'No Match Row Id')
                {
                	listOfConIds.add(si.Primary_Contact_Id__c);       
                }
                if(!String.isBlank(si.Account_Id__c) && si.Account_Id__c != 'No Match Row Id')
                {                	
                    listOfAccIds.add(si.Account_Id__c);
                }
                if(!String.isBlank(si.Development_Id__c) && si.Development_Id__c != 'No Match Row Id')
                {                	                    
                    listOfDevIds.add(si.Development_Id__c);
                }                                         
            } 
            for(Contact con : [select On_Demand_Id_P2P__c, Id from Contact where On_Demand_Id_P2P__c in :listOfConIds FOR UPDATE])
            {
                mapListOfCon.put(con.On_Demand_Id_P2P__c, con.id);            
            }       
            
            Map<String, Id> mapListOfAcc = new Map<String, Id>();
            for(Account acc : [select on_demand_id__c, Id from Account where on_demand_Id__c in :listOfAccIds FOR UPDATE])
            {
                mapListOfAcc.put(acc.on_demand_id__c, acc.id);            
            } 
            
            Map<String, Id> mapListOfDev = new Map<String, Id>();        
            for(Development__c dev : [select Development_Id__c, Id from Development__c where Development_Id__c in :listOfDevIds FOR UPDATE])
            {
                mapListOfDev.put(dev.Development_Id__c, dev.id);            
            }
            
            List<Stage_Application__c> listOfSA = new List<Stage_Application__c>();  
            Map<String,Stage_Application__c> saMap = new Map<String,Stage_Application__c>();                          
            
            for(Stage_Application_Int__c sai : listOfSAInt)
            {                
                
                Stage_Application__c sa = new Stage_Application__c();
                
                sa.Request_Id__c = sai.Request_Id__c;
                sa.Name = sai.Application_Name__c;
                
                
                if(!String.isBlank(sai.Account_Id__c))
                {
                    //Account a = new Account(On_Demand_Id__c = sai.Account_Id__c);                        
                    //sa.Account__r =a; 
                    sa.Account__c = mapListOfAcc.get(sai.Account_Id__c);                                           
                }
                
                if(!String.isBlank(sai.Primary_Contact_Id__c))
                {
                    //Contact c = new Contact(On_Demand_Id_P2P__c = sai.Primary_Contact_Id__c);                        
                    //sa.Primary_Contact__r =c;    
                    sa.Primary_Contact__c = mapListOfCon.get(sai.Primary_Contact_Id__c);                    
                }     

                if(!String.isBlank(sai.Development_Id__c))
                {
                    //Development__c d = new Development__c(Development_ID__c = sai.Development_Id__c); 
                    //sa.Development__r = d;                 
                    sa.Development__c = mapListOfDev.get(sai.Development_Id__c);       
                }
                
                sa.Active_Status__c = sai.Active_Status__c;
                sa.Address__c = sai.Address__c;
                sa.Suburb__c = sai.Suburb__c;
                sa.State__c = sai.State__c;
                sa.Post_Code__c = sai.Post_Code__c;
                sa.No_of_Premises__c = sai.No_of_Premises__c;
                sa.OLT__c = sai.OLT__c;
                sa.DLT_Owner__c = sai.DLT_Owner__c;
                sa.Application_Status__c = sai.Application_Status__c;
                sa.FDA__c = sai.FDA__c;
                sa.FSA__c = sai.FSA__c;
                sa.SAM__c = sai.SAM__c;

                String strLatitude = sai.Latitude__c;
                if(!String.isBlank(strLatitude))
                {
                  sa.Location__Latitude__s = Decimal.valueOf(strLatitude);    
                }                
                String strLongitude = sai.Longitude__c;
                if(!String.isBlank(strLongitude))
                {
                  sa.Location__Longitude__s = Decimal.valueOf(strLongitude);    
                }

                String EFSCD = sai.Estimated_First_Service_Connection_Date__c;                
                if(!String.isBlank(EFSCD))
                {
                    //String EFSCD2 = EFSCD.substringBefore(' ');
                    //sa.Estimated_First_Service_Connection_Date__c = date.parse(EFSCD2.replace('-','/'));   
                     sa.Estimated_First_Service_Connection_Date__c = StringToDate.strDate(EFSCD);
                }

                 sa.Estimated_RSP_Ready_to_Order_Date__c = sai.Estimated_RSP_Ready_to_Order_Date__c;                
                

                String ERFSD = sai.Estimated_Ready_for_Service_Date__c;                
                if(!String.isBlank(ERFSD))
                {
                    //String ERFSD2 = ERFSD.substringBefore(' ');
                   // sa.Estimated_First_Service_Connection_Date__c = date.parse(ERFSD2.replace('-','/'));    
                    sa.Estimated_Ready_for_Service_Date__c = StringToDate.strDate(ERFSD);
                }  
                
                String TDD = sai.Target_Delivery_Date__c;                
                if(!String.isBlank(TDD))
                {
                    //String TDD2 = TDD.substringBefore(' ');
                    //sa.Estimated_First_Service_Connection_Date__c = date.parse(TDD2.replace('-','/'));     
                    sa.Target_Delivery_Date__c = StringToDate.strDate(TDD);
                }                
                
                sa.Dwelling_Type__c = sai.Dwelling_Type__c;
                sa.Local_Technology__c = sai.Local_Technology__c;
                
                sa.Ownership__c = 'CRMOD';
                
                saMap.put(sai.Request_Id__c,sa);
            }
            listOfSA = saMap.values();
            List<Database.UpsertResult> upsertSAResults = Database.Upsert(listOfSA,Stage_Application__c.Fields.Request_Id__c,false);
            List<Database.Error> upsertSAError;
            String errorMessage, fieldsAffected;
            String[] listOfAffectedFields;
            StatusCode sCode;
            List<Error_Logging__c> errSAList = new List<Error_Logging__c>();
            List<Stage_Application_Int__c> updSAIntRes = new List<Stage_Application_Int__c>();
            for(Integer j=0; j<upsertSAResults.size();j++)
            {
                //Stage_Application_Int__c updSAInt = new Stage_Application_Int__C();
                if(!upsertSAResults[j].isSuccess())
                {
                    upsertSAError = upsertSAResults[j].getErrors();
                    for(Database.Error er: upsertSAError)
                    {
                        sCode = er.getStatusCode();
                        errorMessage = er.getMessage();
                        listOfAffectedFields = er.getFields();
                    }
                    fieldsAffected = String.join(listOfAffectedFields,',');
                    errSAList.add(new Error_Logging__c(Name='StageApplication_Tranformation',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isCreated__c=upsertSAResults[j].isCreated(),isSuccess__c=upsertSAResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfSA[j].Id,Schedule_Job__c=bjId));
                    listOfSAInt[j].Transformation_Status__c = 'Error';
                    //updSAInt.Request_Id__c = listOfSA[j].Request_Id__c;
                    listOfSAInt[j].Schedule_Job_Transformation__c = bjId;
                    //updSAIntRes.add(updSAInt);
                }
                else
                {
                	if(String.isBlank(listOfSAInt[j].Account_Id__c) || listOfSAInt[j].Account_Id__c == 'No Match Row Id' || String.isNotBlank(mapListOfAcc.get(listOfSAInt[j].Account_Id__c)))
					{
                    		listOfSAInt[j].Transformation_Status__c = 'Copied to Base Table';
					}
					else
					{
						tStatus = true;
						errSAList.add(new Error_Logging__c(Name='StageApplication_Tranformation',Error_Message__c='Account Record is not associated',Fields_Afftected__c=fieldsAffected,isCreated__c=upsertSAResults[j].isCreated(),isSuccess__c=upsertSAResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfSAInt[j].Id,Schedule_Job__c=bjId));
					}
					
					if(String.isBlank(listOfSAInt[j].Primary_Contact_Id__c) || listOfSAInt[j].Primary_Contact_Id__c == 'No Match Row Id' || String.isNotBlank(mapListOfCon.get(listOfSAInt[j].Primary_Contact_Id__c)))
					{
                    		listOfSAInt[j].Transformation_Status__c = 'Copied to Base Table';
					}
					else
					{
						tStatus = true;
						errSAList.add(new Error_Logging__c(Name='StageApplication_Tranformation',Error_Message__c='Contact Record is not associated',Fields_Afftected__c=fieldsAffected,isCreated__c=upsertSAResults[j].isCreated(),isSuccess__c=upsertSAResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfSAInt[j].Id,Schedule_Job__c=bjId));
					}
					
					if(String.isBlank(listOfSAInt[j].Development_Id__c) || listOfSAInt[j].Development_Id__c == 'No Match Row Id' || String.isNotBlank(mapListOfDev.get(listOfSAInt[j].Development_Id__c)))
					{
                    		listOfSAInt[j].Transformation_Status__c = 'Copied to Base Table';
					}
					else
					{
						tStatus = true;
						errSAList.add(new Error_Logging__c(Name='StageApplication_Tranformation',Error_Message__c='Development Record is not associated',Fields_Afftected__c=fieldsAffected,isCreated__c=upsertSAResults[j].isCreated(),isSuccess__c=upsertSAResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=listOfSAInt[j].Id,Schedule_Job__c=bjId));
					}
					if(tStatus)
					{
						listOfSAInt[j].Transformation_Status__c = 'Association Error';
					}
                    //updSAInt.Request_Id__c = listOfSA[j].Request_Id__c;
                    listOfSAInt[j].Schedule_Job_Transformation__c = bjId;
                    //updSAIntRes.add(updSAInt);                    
                }
            }
            system.debug('errSAList==>'+errSAList);
            Insert errSAList;
            Update listOfSAInt;
            if(errSAList.isEmpty())      
            {
                status = 'Completed';
            } 
            else
            {
                status = 'Error';
            }  
            recordCount = recordCount+ listOfSAInt.size();  
        }
        catch(Exception e)
        {            
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='Stage Application Transformation',Error_Message__c= e.getMessage()+' Line Number: '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }
    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob saJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
       	{
       		bj.Status__c= saJob.Status;
       	}
       	else
       	{
       		 bj.Status__c=status;
       	}
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = saJob.id;
        bj.End_Time__c  = saJob.CompletedDate;
        upsert bj;
        if (CRM_Transformation_Job__c.getinstance('StageContactTransformation') <> null && CRM_Transformation_Job__c.getinstance('StageContactTransformation').on_off__c)
   	    {
        	Database.executeBatch(new StageContactTransformation(exJobId));
        }
    }    
}