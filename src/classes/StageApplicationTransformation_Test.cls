/***************************************************************************************************
Class Name:  StageApplicationTransformation_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 31-10-2016
Function    : This class contains unit test scenarios for StageApplicationTransformation apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       31-10-2016                Created
****************************************************************************************************/
@isTest
private class StageApplicationTransformation_Test {
    static date CRM_Modified_Date = date.newinstance(1963, 01,24);
    static void getRecords (){
        // Custom Setting record creation
        TestDataUtility.getListOfCRMTransformationJobRecords(true);
        // Create Account record
        List<Account> listOfAccounts = new List<Account> (); 
        Account accRec = new Account();
        accRec.name = 'Test Account';
        accRec.on_demand_id__c = 'Account1';
        listOfAccounts.add(accRec);
        Account accRec2 = new Account();
        accRec2.name = 'Test Account';
        accRec2.on_demand_id__c = 'Account2';
        listOfAccounts.add(accRec2);
        insert listOfAccounts;
        // Create Contact record
        List<Contact> ListOfContacts = new List<Contact> ();
        Contact contactRec = new Contact ();
        contactRec.on_demand_id_P2P__c = 'Contact1';
        contactRec.On_Demand_Id__c  = 'Contact1';
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        ListOfContacts.add(contactRec);
        Contact contactRec2 = new Contact ();
        contactRec2.on_demand_id_P2P__c = 'Contact2';
        contactRec2.On_Demand_Id__c  = 'Contact2';
        contactRec2.LastName = 'Test Contact2';
        contactRec2.Email = 'TestEmail2@nbnco.com.au';
        ListOfContacts.add(contactRec2);
        insert ListOfContacts;
        // Create Development record
        List<Development__c> ListOfDevelopment = new List<Development__c> ();
        Development__c Development1 = new Development__c ();
        Development1.Name = 'Test';
        Development1.Development_ID__c = 'Development1';
        Development1.Account__c = listOfAccounts.get(0).Id;
        Development1.Primary_Contact__c = ListOfContacts.get(0).Id;
        Development1.Suburb__c = 'North Sydney';
        ListOfDevelopment.add(Development1);
        Development__c Development2 = new Development__c ();
        Development2.Name = 'Test';
        Development2.Development_ID__c = 'Development2';
        Development2.Account__c = listOfAccounts.get(1).Id;
        Development2.Primary_Contact__c = ListOfContacts.get(1).Id;
        Development2.Suburb__c = 'North Sydney';
        ListOfDevelopment.add(Development2);
        insert ListOfDevelopment;   
    }
    static void setUpStageApplicationIntRecords(){
        // Create Stage Application record
        List<Stage_Application_Int__c> ListOfStageApplicationInt = new List<Stage_Application_Int__c>();
        Stage_Application_Int__c StageApplicationIntRec1 = new Stage_Application_Int__c();
        StageApplicationIntRec1.Name = 'TestName';
        StageApplicationIntRec1.Primary_Contact_Id__c = 'Contact1';
        StageApplicationIntRec1.Account_Id__c = 'Account1';
        StageApplicationIntRec1.Development_Id__c = 'Development1';
        StageApplicationIntRec1.CRM_Last_Modified_Date__c = CRM_Modified_Date;
        StageApplicationIntRec1.Latitude__c = '80.209566';
        StageApplicationIntRec1.Longitude__c = '151.209566';
        StageApplicationIntRec1.Request_Id__c = 'AYCA-2F1V03';
        StageApplicationIntRec1.Dwelling_Type__c = 'SDU Only';
        StageApplicationIntRec1.Active_Status__c = 'Active';
        StageApplicationIntRec1.Estimated_First_Service_Connection_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec1.Estimated_Ready_for_Service_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec1.Target_Delivery_Date__c = string.valueOf(CRM_Modified_Date);
        ListOfStageApplicationInt.add(StageApplicationIntRec1);
        Stage_Application_Int__c StageApplicationIntRec2 = new Stage_Application_Int__c();
        StageApplicationIntRec2.Name = 'TestName';
        StageApplicationIntRec2.Primary_Contact_Id__c = 'Contact11111';
        StageApplicationIntRec2.Account_Id__c = 'Account1111';
        StageApplicationIntRec2.Development_Id__c = 'Development1';
        StageApplicationIntRec2.CRM_Last_Modified_Date__c = CRM_Modified_Date;
        StageApplicationIntRec2.Latitude__c = '80.209566';
        StageApplicationIntRec2.Longitude__c = '151.209566';
        StageApplicationIntRec2.Request_Id__c = 'AYCA-2F1V0333';
        StageApplicationIntRec2.Dwelling_Type__c = 'SDU Only';
        StageApplicationIntRec2.Active_Status__c = 'Active';
        StageApplicationIntRec2.Estimated_First_Service_Connection_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec2.Estimated_Ready_for_Service_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec2.Target_Delivery_Date__c = string.valueOf(CRM_Modified_Date);
        ListOfStageApplicationInt.add(StageApplicationIntRec2);
        Stage_Application_Int__c StageApplicationIntRec3 = new Stage_Application_Int__c();
        StageApplicationIntRec3.Name = 'TestName';
        StageApplicationIntRec3.Primary_Contact_Id__c = 'Contact2';
        StageApplicationIntRec3.Account_Id__c = 'Account2';
        StageApplicationIntRec3.Development_Id__c = 'Development2';
        StageApplicationIntRec3.CRM_Last_Modified_Date__c = CRM_Modified_Date;
        StageApplicationIntRec3.Latitude__c = '80.209566';
        StageApplicationIntRec3.Longitude__c = '151.209566';
        StageApplicationIntRec3.Estimated_First_Service_Connection_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec3.Estimated_Ready_for_Service_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec3.Target_Delivery_Date__c = string.valueOf(CRM_Modified_Date);
        ListOfStageApplicationInt.add(StageApplicationIntRec3);
        insert ListOfStageApplicationInt;
    }
    static testMethod void StageApplicationTransformation_Test1(){
        getRecords();
        setUpStageApplicationIntRecords();
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());   
        Test.stopTest();
    }
}