public without sharing Class StageApplicationTriggerHandler extends TriggerHandler{
    
    public StageApplicationTriggerHandler(){
        if(Test.isRunningTest()){
            this.setMaxLoopCount(10);
        }
        else{
            this.setMaxLoopCount(10);
        }        
        //OnlyLeadCase = false; 
    }
    
    protected override void beforeInsert(List<SObject> newItems) {          
       
        try{
             SA_Auto_Reference_Number__c seqNum = [SELECT Last_Sequence_Number__c FROM SA_Auto_Reference_Number__c LIMIT 1 FOR UPDATE];  
              Integer latestSeqNum = Integer.valueOf(seqNum.Last_Sequence_Number__c);
            for(SObject sObj : newItems){  
                Stage_Application__c stg = (Stage_Application__c) sObj; 
                System.debug('Stg App Reference Number Before = ' + stg.Reference_Number__c);
                if(stg.Application_Number__c == null){
                    Integer counter = latestSeqNum + 1; 
                    String str = String.valueOf(counter); 
                    for(Integer i=str.length();i<9;i++){
                        str = '0' + str;                
                    }                
                    stg.Application_Number__c = 'STG-M' + str;
                    seqNum.Last_Sequence_Number__c = str;                    
                }
            }  
            UPDATE seqNum; 
        }
        catch(Exception e){
            System.debug('Stage Application Reference Number Update' + e);            
        }
    }
    
    protected override void afterInsert(map<id,SObject> newMap) {  
        Map<Id,Stage_Application__c> afterInsertNewStgApp = (Map<Id,Stage_Application__c>)newMap;
        CreateDeliverableTasks(afterInsertNewStgApp);        
    }
    //after Tech assessment 
    protected override void afterUpdate(map<id,SObject> oldMap, map<id,SObject> newMap) {
        Map<Id,Stage_Application__c> afterUpdateStgApp = (Map<Id,Stage_Application__c>)newMap;
        Map<Id,Stage_Application__c> beforeUpdateStgApp = (Map<Id,Stage_Application__c>)oldMap;
        Map<Id,Stage_Application__c> stageAppMapforTasks = new Map<Id,Stage_Application__c>();
        Map<Id,String> stglist = new Map<Id,String>();
        set<Id> stgAppIds = new set<Id>();
        for(Stage_Application__c stg: afterUpdateStgApp.values()){
            Stage_Application__c oldstg = beforeUpdateStgApp.get(stg.Id);
            if(stg.Service_Delivery_Type__c != Null && 
                stg.Build_Type__c == 'Pit and Pipe' && oldstg.Service_Delivery_Type__c == Null) {
                   stageAppMapforTasks.put(stg.Id, stg);
               }
            
            if (stg.Technical_Assessment__c && oldstg.Service_Delivery_Type__c == Null &&
                stg.Service_Delivery_Type__c != Null){
                
                stglist.put(stg.Id,stg.Service_Delivery_Type__c);
                
            }
          //**Start **Addedby : Anjani Tiwari, ** Send the deliverable Tasks (Documents) to Appian after payment recieved 
            if(stg.Service_Delivery_Type__c == 'SD-2' && stg.Application_Status__c == 'Service Delivery' && oldstg.Application_Status__c != 'Service Delivery'){
              stgAppIds.add(stg.Id);
            }
            //**end
        }
        
        if(!stglist.isEmpty())
        {
            List<NewDev_Application__c> updNDlist = new List<NewDev_Application__c>();           
            
            for (NewDev_Application__c nd: [Select Id,SF_Stage_ID__c from NewDev_Application__c where SF_Stage_ID__c =:stglist.keyset()])
            {
               if(stglist.get(nd.SF_Stage_ID__c) =='SD-1')
                  nd.status__c = 'Tech Assessment SD1 Send to CRMOD';
               else
                  nd.status__c = 'Tech Assessment SD2 Completed';
                updNDlist.add(nd);
                
            }
            update updNDlist;
            system.debug('@#^TEW**'+updNDlist);
        }
        
        
        
        if(!stageAppMapforTasks.isEmpty()){
            CreateDeliverableTasks(stageAppMapforTasks); 
        }
        
    //**Start **Addedby : Anjani Tiwari, ** Send the deliverable Tasks (Documents) to Appian after payment recieved     
        Id taskRecTypId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId(); 
        Map<Id,Task> submitDeliverable = new Map<Id,Task>([Select Id,WhatId,Status,Deliverable_Status__c,Type_Custom__c,Sub_Type__c,RecordTypeId,Description  from Task where (RecordTypeId =: taskRecTypId) AND (WhatId in : stgAppIds) AND Deliverable_Status__c = 'Submitted' AND Type_Custom__c = 'Deliverables']);      
        If(!submitDeliverable.isEmpty()){
          PublishPlatformEventOnTask.PublishAttachmentOnTask(submitDeliverable); 
        }
    }

    protected override void beforeUpdate(map<id,SObject> oldMap, map<id,SObject> newMap) {
        system.debug('inside bbefore upadate');
        Map<Id,Stage_Application__c> newValuesMap = (Map<Id,Stage_Application__c>)newMap;
        Map<Id,Stage_Application__c> oldValuesMap = (Map<Id,Stage_Application__c>)oldMap;
        Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.Stage_Application__c.fields.getMap(); 
        set<string> integratedFieldSet =  new set<string>();
        // select fields to be integrated from metadata 
        for (New_Development_Integration__mdt integrationField : [
            Select MasterLabel, Triggered_By__c, Field_API_Name__c, Object_API_Name__c from New_Development_Integration__mdt 
            Where Valid__c = true 
            and Triggered_By__c = 'Update'
            and Object_API_Name__c = 'Stage_Application__c'
            and Valid__c = true
            ]){
                integratedFieldSet.add(integrationField.Field_API_Name__c);
        }
        for(Stage_Application__c newstg: newValuesMap.values()){
            if (!integratedFieldSet.isEmpty() && (newstg.SD_Reference_Number__c <> null || newstg.SD_Reference_Number__c <> '')){
                Stage_Application__c currentstg = oldValuesMap.get(newstg.Id);
                for (String fieldName : mapFields.keyset()){ 
                    try {
                        if(newstg.get(fieldName) != currentstg.get(fieldName) && integratedFieldSet.Contains(fieldName)){ 
                            newstg.Integrated_Attribute_Updated__c = true;
                        } 
                    } 
                    catch (Exception e){ 
                        System.Debug('Error while updating stage integration flag: ' + e); 
                    } 
                }
            }
        }
    }
   
    Private void CreateDeliverableTasks(Map<Id,Stage_Application__c> AllStgAppNewValues){
        try{
            System.debug('Inside CreateDeliverableTasks Method');
            List<String> deliverables = new List<String>();
            List<String> subject = new List<String>();            
            List<Id> conIds = new List<Id>();
            Map<Id,User> conUserMap = new Map<Id,User>();
            Id unAssignedUserId = [select Id, name,username  from user where username like 'unassigned@nbnco.com.au%'].Id;
            for(Stage_Application__c stg : AllStgAppNewValues.values()){
                conIds.add(stg.Applicant_Contact__c);
            }
            for(User us :[SELECT Id, Name, ContactId FROM User WHERE ContactId IN :conIds]){
                conUserMap.put(us.ContactId, us);
            }
            List<New_Development_Deliverables__mdt> delvTasks = [select Id, Build_Type__c, Class__c, Deliverables__c, Service_Delivery__c, Subject__c, Technical_Assessment_Required__c,Is_Pit_and_Pipe_Private__c from New_Development_Deliverables__mdt];
            List<Task> tasksToInsert = new List<Task>();            
            for(Stage_Application__c stg : AllStgAppNewValues.values()){
                System.debug('Inside Stage App Loop');                
                for(New_Development_Deliverables__mdt del : delvTasks){
                    System.debug('Inside New_Development_Deliverables__mdt Loop' + del);  
                    if(del.Build_Type__c == stg.Build_Type__c && 
                       del.Class__c == stg.Class__c && 
                       del.Service_Delivery__c == stg.Service_Delivery_Type__c &&
                       del.Technical_Assessment_Required__c == False &&
                       stg.Technical_Assessment__c == False &&
                      del.Is_Pit_and_Pipe_Private__c == stg.Is_Pit_and_Pipe_Private__c){
                           System.debug('Match found' + del);  
                           deliverables.add(del.Deliverables__c);   
                           subject.add(del.Subject__c);                        
                       }
                    else if (stg.Technical_Assessment__c == True &&
                             stg.Service_Delivery_Type__c == Null &&
                             del.Technical_Assessment_Required__c == True &&
                             del.Build_Type__c == stg.Build_Type__c &&
                             del.Class__c == stg.Class__c){
                                 deliverables.add(del.Deliverables__c);
                                 subject.add(del.Subject__c);                                
                             }
                    else if (stg.Technical_Assessment__c == True &&
                             stg.Is_Pit_and_Pipe_Private__c == True &&
                             stg.Service_Delivery_Type__c == 'SD-2' &&
                             stg.Class__c == 'Class3/4' &&
                             del.Build_Type__c == 'Pit and Pipe' &&
                             del.Class__c == 'Class3/4' &&
                             del.Deliverables__c != 'Master Plan'){
                                 deliverables.add(del.Deliverables__c);
                                 subject.add(del.Subject__c);                                
                             }
                    
                }
                if(deliverables.size() > 0){                    
                    for(Integer i=0; i<deliverables.size(); i++){
                        System.debug('Task Creation');
                        Task t = new Task();                                    
                        t.RecordTypeId = GlobalCache.getRecordTypeId('Task', 'New Development');
                        t.Type_Custom__c = 'Deliverables';
                        t.Priority = '3-General';
                        t.Sub_Type__c = deliverables.get(i);            
                        t.Subject = subject.get(i);            
                        t.Status = 'Open';
                        t.Deliverable_Status__c = 'Pending Documents';
                        if(t.Sub_Type__c != 'As-built design' && t.Sub_Type__c != 'PCN'){
                            t.Due_Date__c = Date.today();     
                        }                    
                        t.WhatId = stg.Id;                        
                        t.WhoId = stg.Applicant_Contact__c;
                        if(conUserMap.containsKey(stg.Applicant_Contact__c)){
                            t.OwnerId = conUserMap.get(stg.Applicant_Contact__c).Id;
                        }
                        else{
                            t.OwnerId = unAssignedUserId;
                        }
                        
                        t.IsVisibleInSelfService = true;
                        tasksToInsert.add(t);                          
                    }                                      
                }                
            }
            System.debug('*** Stg App Trigger Handler - Task Creation: List of Tasks ready to Insert ****' + tasksToInsert);  
            if(tasksToInsert.size()>0){     
                System.debug('inserted successfully');
                //insert tasksToInsert; 
                Database.dmloptions dml = new Database.dmloptions();
                dml.EmailHeader.triggerUserEmail = false;
                dml.optAllOrNone = false;
                Database.SaveResult[] srList = Database.insert(tasksToInsert, dml);
                //Database.SaveResult[] srList = Database.insert(tasksToInsert, false);
                for (Database.SaveResult sr : srList) {
                    if (sr.isSuccess()) {
                        System.debug('Successfully inserted Task ID: ' + sr.getId());
                    }
                    else{
                        System.debug('Not Successfull' + sr.getErrors());
                    }
                }
            }                   
        }
        
        Catch(Exception e){
            e.getmessage();
        }
    }
    
    
}