/***************************************************************************************************
Class Name:  StageApplicationTriggerHandler_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 07-02-2018
Function    : This class contains unit test scenarios for  StageApplicationTriggerHandler apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Gnana				         07-02-2018               Created
****************************************************************************************************/

@isTest
public class StageApplicationTriggerHandler_Test {
	
    static User testRunningUser = new User ();
    static Contact conObj = new Contact();
    static Development__c devObj = new Development__c();
    static Stage_Application__c stageAppObj = new Stage_Application__c();
    static Stage_Application__c updatestgAppObj = new Stage_Application__c();
    static void getRecords(){   
        
        List<string> listOfProfileName = new List<string> {'System Administrator'};
        list<Profile> listOfProfiles = TestDataUtility.getListOfProfiles(listOfProfileName);
        testRunningUser = TestDataUtility.createTestUser(true,listOfProfiles.get(0).id);
        
        // Create Account
        Account actObj = new Account();
        actObj.name = 'Test Account';
        insert actObj;
        
        // Create Contact
        conObj.LastName = 'testLastName';
        insert conObj;
        
        // Create Development        
        devObj.Name='testName';
        devObj.Development_ID__c='AKHS833';
        devObj.Account__c=actObj.Id;
        devObj.Primary_Contact__c=conObj.Id;
        devObj.Suburb__c='testsuburd';
        insert devObj;
    }
    
    @isTest (SeeAllData=true) static void StageApplicationTriggerHandler_Test1(){
        getRecords();
        // Test Trigger Functionality
        Test.startTest();
        System.runAs(testRunningUser){
            // Create Stage Application
            stageAppObj.Name='testName';
            stageAppObj.Active_Status__c='Active';
            stageAppObj.Dwelling_Type__c='SDU Only';
            stageAppObj.Development__c=devObj.Id;
            //stageAppObj.Request_ID__c='SampleReqId';
            stageAppObj.Build_Type__c='Pit and Pipe';
            stageAppObj.Class__c='Class3/4';
            stageAppObj.Service_Delivery_Type__c='SD-2';
            stageAppObj.Is_Pit_and_Pipe_Private__c=true;
            insert stageAppObj;

            // DML Operations
        }
        Test.stopTest();
    }
    
     @isTest (SeeAllData=true) static void StageApplicationTriggerHandler_Test2(){
        getRecords();
        // Test Trigger Functionality
        Test.startTest();
        System.runAs(testRunningUser){
            // Create Stage Application
            stageAppObj.Name='testName';
            stageAppObj.Active_Status__c='Active';
            stageAppObj.Dwelling_Type__c='SDU Only';
            stageAppObj.Development__c=devObj.Id;
            //stageAppObj.Request_ID__c='SampleReqId';
            stageAppObj.Build_Type__c='Pit and Pipe';
            stageAppObj.Technical_Assessment__c = true;
            insert stageAppObj;
            updatestgAppObj = [select id, Class__c, Service_Delivery_Type__c From Stage_Application__c where Id = :stageAppObj.Id];
            updatestgAppObj.Class__c='Class3/4';
            updatestgAppObj.Service_Delivery_Type__c='SD-2';
            Update updatestgAppObj;
            // DML Operations
        }
        Test.stopTest();
	}
             
    @isTest (SeeAllData=true) static void StageApplicationTriggerHandler_Test3(){
        getRecords();
        // Test Trigger Functionality
        Test.startTest();
        System.runAs(testRunningUser){
            // Create Stage Application
            stageAppObj.Name='testName';
            stageAppObj.Active_Status__c='Active';
            stageAppObj.Dwelling_Type__c='SDU Only';
            stageAppObj.Development__c=devObj.Id;
            //stageAppObj.Request_ID__c='SampleReqId';
            stageAppObj.Build_Type__c='Pit and Pipe';
            stageAppObj.Technical_Assessment__c = true;
            stageAppObj.Is_Pit_and_Pipe_Private__c = true;
            stageAppObj.Service_Delivery_Type__c = 'SD-2';
            stageAppObj.Class__c = 'Class3/4';
            // DML Operations
            insert stageAppObj;
        }
        Test.stopTest();
    }
    
    @isTest (SeeAllData=true) static void StageApplicationTriggerHandler_Test4(){
        getRecords();
        // Test Trigger Functionality
        Test.startTest();
        System.runAs(testRunningUser){
            // Create Stage Application
            stageAppObj.Name='testName';
            stageAppObj.Active_Status__c='Active';
            stageAppObj.Dwelling_Type__c='SDU Only';
            stageAppObj.Development__c=devObj.Id;
            //stageAppObj.Request_ID__c='SampleReqId';
            stageAppObj.Build_Type__c='Pit and Pipe';
            stageAppObj.Technical_Assessment__c = true;                        
            stageAppObj.Class__c = 'Class3/4';
            // DML Operations
            insert stageAppObj;
        }
        Test.stopTest();
    }
}