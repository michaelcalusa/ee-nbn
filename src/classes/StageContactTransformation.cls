/*
Class Description
Creator: v-dileepathley
Purpose: This class will be used to transform and transfer Stage Application Contact records from Junction_Object__c table.
Modifications:
*/
public class StageContactTransformation implements Database.Batchable<Sobject>, Database.Stateful
{
    public String scQuery;
    public string bjId;
    public string status;
    public Integer recordCount = 0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {
        
    }
    public StageContactTransformation(String ejId)
    {
         exJobId = ejId;
         Batch_Job__c bj = new Batch_Job__c();
         bj.Name = 'SAContactTransformation'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        scQuery = 'Select Child_Id__c,Child_Name__c,Event_Name__c,Object_Id__c,Object_Name__c from Junction_Object_Int__c where Transformation_Status__c = \'Exported from CRM\' and Object_Name__c = \'CustomObject3\' order by createddate asc';
        return Database.getQueryLocator(scQuery);   
    }
    
    public void execute(Database.BatchableContext BC,List<Junction_Object_Int__c> junList)
    {
        try
        {
            list<StageApplication_Contact__c> scAddList = new list<StageApplication_Contact__c>();
            map<String,StageApplication_Contact__c> scAddMap = new map<String,StageApplication_Contact__c>();
            list<StageApplication_Contact__c> scDelList = new list<StageApplication_Contact__c>();
            
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.allowFieldTruncation = true;
            
            list <Junction_Object_Int__c> aStage = new list<Junction_Object_Int__c>();
            list <Junction_Object_Int__c> dStage = new list<Junction_Object_Int__c>();
            
            String stageConUnique;
            list<string> stageConUniqueId =  new list<string>();
            
            List<String> listOfSAIds = new List<String>();
            List<String> listOfConIds = new List<String>(); 
            Map<String, Id> mapListOfCon = new Map<String, Id>();
            Map<String, Id> mapListOfSA = new Map<String, Id>(); 
            
            for(Junction_Object_Int__c sai : junList)
            {                                
                listOfConIds.add(sai.Child_Id__c);     
                listOfSAIds.add(sai.Object_Id__c);
            }
            
            for(Contact con : [select On_Demand_Id_P2P__c, Id from Contact where On_Demand_Id_P2P__c in :listOfConIds FOR UPDATE])
            {
                mapListOfCon.put(con.On_Demand_Id_P2P__c, con.id);            
            }  
                 
            for(Stage_Application__c sa : [select Request_Id__c, Id from Stage_Application__c where Request_Id__c in :listOfSAIds FOR UPDATE])
            {
                mapListOfSA.put(sa.Request_Id__c, sa.id);            
            }
            for(Junction_Object_Int__c jun: junList)
            {
                StageApplication_Contact__c stageCon = new StageApplication_Contact__c();
                stageCon.setOptions(dml);
                
                if(jun.Event_Name__c == 'Associate')
                {
                    Contact con = new Contact(On_Demand_Id_P2P__c = jun.Child_Id__c);
                    //stageCon.Contact__r = con;
                    //Stage_Application__c stage = new Stage_Application__c(Request_Id__c = jun.Object_Id__c);
                    //stageCon.Stage_Application__r = stage;
                    //stageCon.StageApp_Contact_Unique__c = jun.Object_Id__c+' '+jun.Child_Id__c;
                    stageCon.StageApp_Contact_Unique__c = mapListOfSA.get(jun.Object_Id__c)+' '+mapListOfCon.get(jun.Child_Id__c);
                    stageCon.Contact__c = mapListOfCon.get(jun.Child_Id__c);
                    stageCon.Stage_Application__c = mapListOfSA.get(jun.Object_Id__c);
                    
                    aStage.add(jun);
                    //scAddList.add(stageCon);
                    scAddMap.put(mapListOfSA.get(jun.Object_Id__c)+' '+mapListOfCon.get(jun.Child_Id__c),stageCon);
                }
                
                if(jun.Event_Name__c == 'Dissociate')
                {
                    stageConUnique = mapListOfSA.get(jun.Object_Id__c)+' '+mapListOfCon.get(jun.Child_Id__c);
                    dStage.add(jun);
                    stageConUniqueId.add(stageConUnique);
                }
            }
            if(!stageConUniqueId.isEmpty())
            {
                scDelList = [select Id from StageApplication_Contact__c where StageApp_Contact_Unique__c in: stageConUniqueId];
            }
            scAddList = scAddMap.values();
            Schema.SObjectField scUnique = StageApplication_Contact__c.Fields.StageApp_Contact_Unique__c;
            List<Database.upsertResult> uResults = Database.upsert(scAddList,scUnique,false);
            List<Database.deleteResult> dResults = Database.delete(scDelList,false);
            list<Database.Error> err;
            String msg, fAffected;
            String[]fAff;
            StatusCode sCode;
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            for(Integer idx = 0; idx < uResults.size(); idx++)
            {   
                if(!uResults[idx].isSuccess())
                {
                    err=uResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='StageAppContact_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,isCreated__c=uResults[idx].isCreated(),
                    isSuccess__c=uResults[idx].isSuccess(),Record_Row_Id__c=uResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=aStage[idx].Id,Schedule_Job__c=bjId));
                    aStage[idx].Transformation_Status__c = 'Error';
                    aStage[idx].Schedule_Job_Transformation__c = bjId;
                }
                else
                {
                        aStage[idx].Transformation_Status__c = 'Copied to Base Table';
                        aStage[idx].Schedule_Job_Transformation__c = bjId;
                }
            }
            
            for(Integer idx = 0; idx < dResults.size(); idx++)
            {   
                if(!dResults[idx].isSuccess())
                {
                    err=dResults[idx].getErrors();
                    for (Database.Error er: err)
                    {
                        sCode = er.getStatusCode();
                        msg=er.getMessage();
                        fAff = er.getFields();
                    }
                    fAffected = string.join(fAff,',');
                    errList.add(new Error_Logging__c(Name='StageAppContact_Tranformation',Error_Message__c=msg,Fields_Afftected__c=fAffected,
                    isSuccess__c=dResults[idx].isSuccess(),Record_Row_Id__c=dResults[idx].getId(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=dStage[idx].Id,Schedule_Job__c=bjId));
                    dStage[idx].Transformation_Status__c = 'Error';
                    dStage[idx].Schedule_Job_Transformation__c = bjId;
                }
                else
                {
                        dStage[idx].Transformation_Status__c = 'Copied to Base Table';
                        dStage[idx].Schedule_Job_Transformation__c = bjId;
                }
            }
            aStage.addAll(dStage);
            Update aStage;
            Insert errList;
            if(errList.isEmpty())      
            {
                status = 'Completed';
            } 
            else
            {
                status = 'Error';
            }  
            recordCount = recordCount + junList.size();  
        }
        catch (Exception e)
        {
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='StageAppContact_Tranformation',Error_Message__c= e.getMessage()+' '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
        
    }
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob saConJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
        {
            bj.Status__c= saConJob.Status;
        }
        else
        {
             bj.Status__c=status;
        }
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = saConJob.id;
        bj.End_Time__c  = saConJob.CompletedDate;
        upsert bj;
        if (CRM_Transformation_Job__c.getinstance('CasesTransformation') <> null && CRM_Transformation_Job__c.getinstance('CasesTransformation').on_off__c)
   	    {
       		 Database.executeBatch(new CasesTransformation(exJobId)); 
        }
    } 
}