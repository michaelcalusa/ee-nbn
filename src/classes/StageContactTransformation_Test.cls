/***************************************************************************************************
Class Name:  StageContactTransformation_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 31-10-2016
Function    : This class contains unit test scenarios for StageContactTransformation apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       31-10-2016                Created
****************************************************************************************************/
@isTest
private class StageContactTransformation_Test {
    static date CRM_Modified_Date = date.newinstance(1963, 01,24);
    static List<Development__c> ListOfDevelopment = new List<Development__c> ();
    static List<Stage_Application__c> ListOfStageApplication = new List<Stage_Application__c> ();
    static List<Contact> ListOfContacts = new List<Contact> ();
    static void getRecords (){
        // Custom Setting record creation
        TestDataUtility.getListOfCRMTransformationJobRecords(true);
        // Create Account record
        List<Account> listOfAccounts = new List<Account> (); 
        Account accRec = new Account();
        accRec.name = 'Test Account';
        accRec.on_demand_id__c = 'Account1';
        listOfAccounts.add(accRec);
        insert listOfAccounts;
        // Create Contact record
        Contact contactRec = new Contact ();
        contactRec.on_demand_id_P2P__c = 'Contact1';
        contactRec.On_Demand_Id__c  = 'Contact1';
        contactRec.LastName = 'Test Contact';
        contactRec.Email = 'TestEmail@nbnco.com.au';
        ListOfContacts.add(contactRec);
        insert ListOfContacts;
        // Create Development record
        Development__c Development1 = new Development__c ();
        Development1.Name = 'Test';
        Development1.Development_ID__c = 'Development1';
        Development1.Account__c = listOfAccounts.get(0).Id;
        Development1.Primary_Contact__c = ListOfContacts.get(0).Id;
        Development1.Suburb__c = 'North Sydney';
        ListOfDevelopment.add(Development1);
        insert ListOfDevelopment;
    }
    static void setUpStageApplicationIntRecords(){
	
		SA_Auto_Reference_Number__c refNumber = new SA_Auto_Reference_Number__c(Name = 'SA001', Last_Sequence_Number__c = '000000000');
            insert refNumber;
        // Create Stage Application record
        List<Stage_Application_Int__c> ListOfStageApplicationInt = new List<Stage_Application_Int__c>();
        Stage_Application_Int__c StageApplicationIntRec1 = new Stage_Application_Int__c();
        StageApplicationIntRec1.Name = 'TestName';
        StageApplicationIntRec1.Primary_Contact_Id__c = 'Contact1';
        StageApplicationIntRec1.Account_Id__c = 'Account1';
        StageApplicationIntRec1.Development_Id__c = 'Development1';
        StageApplicationIntRec1.CRM_Last_Modified_Date__c = CRM_Modified_Date;
        StageApplicationIntRec1.Latitude__c = '80.209566';
        StageApplicationIntRec1.Longitude__c = '151.209566';
        StageApplicationIntRec1.Request_Id__c = 'StageApplicationInt1';
        StageApplicationIntRec1.Dwelling_Type__c = 'SDU Only';
        StageApplicationIntRec1.Active_Status__c = 'Active';
        StageApplicationIntRec1.Estimated_First_Service_Connection_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec1.Estimated_Ready_for_Service_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec1.Target_Delivery_Date__c = string.valueOf(CRM_Modified_Date);
        ListOfStageApplicationInt.add(StageApplicationIntRec1);
        Stage_Application_Int__c StageApplicationIntRec2 = new Stage_Application_Int__c();
        StageApplicationIntRec2.Name = 'TestName';
        StageApplicationIntRec2.Primary_Contact_Id__c = 'Contact11111';
        StageApplicationIntRec2.Account_Id__c = 'Account1111';
        StageApplicationIntRec2.Development_Id__c = 'Development1';
        StageApplicationIntRec2.CRM_Last_Modified_Date__c = CRM_Modified_Date;
        StageApplicationIntRec2.Latitude__c = '80.209566';
        StageApplicationIntRec2.Longitude__c = '151.209566';
        StageApplicationIntRec2.Request_Id__c = 'StageApplicationInt2';
        StageApplicationIntRec2.Dwelling_Type__c = 'SDU Only';
        StageApplicationIntRec2.Active_Status__c = 'Active';
        StageApplicationIntRec2.Estimated_First_Service_Connection_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec2.Estimated_Ready_for_Service_Date__c = string.valueOf(CRM_Modified_Date);
        StageApplicationIntRec2.Target_Delivery_Date__c = string.valueOf(CRM_Modified_Date);
        ListOfStageApplicationInt.add(StageApplicationIntRec2);
        insert ListOfStageApplicationInt;
    }
    static testMethod void StageContactTransformation_Test1(){
        getRecords();
        setUpStageApplicationIntRecords();
        Junction_Object_Int__c JunctionObjectInt1 = new Junction_Object_Int__c();
        JunctionObjectInt1.Name='TestName';
        JunctionObjectInt1.CRM_Modified_Date__c=CRM_Modified_Date;
        JunctionObjectInt1.Transformation_Status__c = 'Exported from CRM';
        JunctionObjectInt1.Event_Name__c = 'Associate';
        JunctionObjectInt1.Object_Name__c = 'CustomObject3';
        JunctionObjectInt1.Child_Id__c = 'Contact1';
        JunctionObjectInt1.Object_Id__c = 'StageApplicationInt1';
        insert JunctionObjectInt1;
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());   
        Test.stopTest();
    }
    static testMethod void StageContactTransformation_Test2(){
        getRecords();
        setUpStageApplicationIntRecords();
        Junction_Object_Int__c JunctionObjectInt1 = new Junction_Object_Int__c();
        JunctionObjectInt1.Name='TestName';
        JunctionObjectInt1.CRM_Modified_Date__c=CRM_Modified_Date;
        JunctionObjectInt1.Transformation_Status__c = 'Exported from CRM';
        JunctionObjectInt1.Event_Name__c = 'Associate';
        JunctionObjectInt1.Object_Name__c = 'CustomObject3';
        insert JunctionObjectInt1;
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());   
        Test.stopTest();
    }
    static testMethod void StageContactTransformation_Test3(){
        getRecords();
        setUpStageApplicationIntRecords();
        Junction_Object_Int__c JunctionObjectInt1 = new Junction_Object_Int__c();
        JunctionObjectInt1.Name='TestName';
        JunctionObjectInt1.CRM_Modified_Date__c=CRM_Modified_Date;
        JunctionObjectInt1.Transformation_Status__c = 'Exported from CRM';
        JunctionObjectInt1.Event_Name__c = 'Dissociate';
        JunctionObjectInt1.Object_Name__c = 'CustomObject3';
        JunctionObjectInt1.Child_Id__c = 'Contact1';
        JunctionObjectInt1.Object_Id__c = 'StageApplicationInt1';
        insert JunctionObjectInt1;
        // Create Stage Application record
        Stage_Application__c StageApplication1 = new Stage_Application__c ();
        StageApplication1.Request_ID__c = 'Test';
        StageApplication1.Dwelling_Type__c = 'SDU Only';
        StageApplication1.Development__c = ListOfDevelopment.get(0).Id;
        StageApplication1.Active_Status__c = 'Active';
        StageApplication1.name = 'Test';
        StageApplication1.Request_Id__c = 'StageApplicationInt1';
        ListOfStageApplication.add(StageApplication1);
        insert ListOfStageApplication;
        // Create StageApplication_Contact__c record
        StageApplication_Contact__c StageApplicationContactRec = new StageApplication_Contact__c ();
        StageApplicationContactRec.Contact__c = ListOfContacts.get(0).Id;
        StageApplicationContactRec.Stage_Application__c = ListOfStageApplication.get(0).Id;
        StageApplicationContactRec.StageApp_Contact_Unique__c = ListOfStageApplication.get(0).Id +' '+ListOfContacts.get(0).Id;
        insert StageApplicationContactRec;
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());   
        Test.stopTest();
    }
}