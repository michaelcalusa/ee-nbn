public class  StagingObjConfigUtilities
{
    public class  StagingObjConfig
    {
        public Map<string, string> mapObjectFieldsMapping {get;set;}
        public Map<string, string> mapObjectFieldsDeleteMapping {get;set;}
        public List<string> availableRecordTypes {get;set;}
        //public List<string> availableRecordTypeIds {get;set;}
        public List<string> excludeChangeCaptureFields {get;set;}
        public string ConfigFor {get;set;}
        public Boolean isDeletionAllowed {get;set;}
        public Boolean isCreationAllowed {get;set;}
        public Boolean isModificationAllowed {get;set;}
        public string sObjectName  {get;set;}  
        public string externalIdField {get;set;}
        
        public StagingObjConfig()
        {
            mapObjectFieldsMapping = new Map<string, string>();
            mapObjectFieldsDeleteMapping = new Map<string, string>();
            availableRecordTypes = new List<string>();
            //availableRecordTypeIds = new List<string>();
            excludeChangeCaptureFields = new List<string>();
        }
                
    }
    
    public static Map<String, List<StagingObjConfig>> mapOBStagingConfiguration 
    {
        get {
            if (mapOBStagingConfiguration == null) 
            {                   
                mapOBStagingConfiguration = new Map<string,List<StagingObjConfig>>();
                List<Staging_Object_Config__mdt> sObjectConfig = [Select Fields_Map__c,Exclude_Sync_Event_Fields__c,External_Id_Field__c,is_Deletion_Allowed__c,is_Modification_Allowed__c,is_Creation_Allowed__c, MasterLabel, MappingType__c, Object__c,RecordType_Names__c,Delete_Fields_Map__c, Config_For__c from Staging_Object_Config__mdt where MappingType__c = 'OutBound'];
                for(Staging_Object_Config__mdt sOM : sObjectConfig)
                {                               
                    StagingObjConfig sOC = new StagingObjConfig();
                    System.debug(sOM.Fields_Map__c); 
                    
                    if(!string.isBlank(sOM.Fields_Map__c))
                    {
                        Map<String,String> mapFieldsMapping = (Map<String,String>) JSON.deserialize(sOM.Fields_Map__c, Map<String,String>.class);
                        system.debug('SIze>>>>>'+mapFieldsMapping.size());
                        system.debug('Meta>>>>>'+mapFieldsMapping); 
                        
                        if(mapFieldsMapping.size() > 0)
                        {
                            //mapObjectFieldsMapping.put(sOM.sOM.Object__c , mapFieldsMapping);
                            sOC.mapObjectFieldsMapping.putAll(mapFieldsMapping);
                        }
                    }
                    
                    if(!string.isBlank(sOM.Delete_Fields_Map__c))
                    {
                        System.debug(sOM.Delete_Fields_Map__c); 
                        Map<String,String> mapDeleteFieldsMapping = (Map<String,String>) JSON.deserialize(sOM.Delete_Fields_Map__c, Map<String,String>.class);
                        system.debug('SIze>>>>>'+mapDeleteFieldsMapping.size());
                        system.debug('Meta>>>>>'+mapDeleteFieldsMapping); 
                        
                        if(mapDeleteFieldsMapping.size() > 0)
                        {
                            //mapObjectFieldsMapping.put(sOM.sOM.Object__c , mapFieldsMapping);
                            sOC.mapObjectFieldsDeleteMapping.putAll(mapDeleteFieldsMapping);
                        }
                    }
                                        
                    System.debug(sOM.RecordType_Names__c); 
                    if(!string.isBlank(sOM.RecordType_Names__c))
                    {
                       List<string> lstRecTypes = sOM.RecordType_Names__c.split(',');
                        system.debug('SIze>>>>>'+lstRecTypes.size());
                        system.debug('Meta>>>>>'+lstRecTypes); 
                        
                        if(lstRecTypes.size() > 0)
                        {
                            sOC.availableRecordTypes.addAll(lstRecTypes);
                            /*Map<string, Schema.RecordTypeInfo> recordtypesInfo = getsObjectRecordTypes(sOM.Object__c);
                            if(recordtypesInfo != null && recordtypesInfo.size() > 0)
                            {
                                for(string key : lstRecTypes)
                                {
                                    if(recordtypesInfo.containsKey(key))
                                    {
                                        sOC.availableRecordTypeIds.add(recordtypesInfo.get(key).getName());
                                    }
                                }
                            }*/
                            //mapObjectRecordTypeMapping.put(sOM.Object__c , lstRecTypes);
                        } 
                    }
                    
                    System.debug(sOM.Exclude_Sync_Event_Fields__c); 
                    if(!string.isBlank(sOM.Exclude_Sync_Event_Fields__c))
                    {
                       List<string> excludeChangeCapture = sOM.Exclude_Sync_Event_Fields__c.split(',');
                        system.debug('SIze>>>>>'+excludeChangeCapture.size());
                        system.debug('Meta>>>>>'+excludeChangeCapture); 
                        
                        if(excludeChangeCapture.size() > 0)
                        {
                            sOC.excludeChangeCaptureFields.addAll(excludeChangeCapture);
                            //mapObjectRecordTypeMapping.put(sOM.Object__c , excludeChangeCapture);
                        } 
                    }
                    
                    sOC.ConfigFor = sOM.Config_For__c;
                    sOC.isDeletionAllowed = sOM.is_Deletion_Allowed__c;
                    sOC.isCreationAllowed = sOM.is_Creation_Allowed__c;
                    sOC.isModificationAllowed = sOM.is_Modification_Allowed__c;
                    sOC.sObjectName = sOM.Object__c;
                    sOC.externalIdField = sOM.External_Id_Field__c;
                    if(mapOBStagingConfiguration.containsKey(sOM.Object__c))
                    {
                       mapOBStagingConfiguration.get(sOM.Object__c).add(sOC);
                    }
                    else
                    {
                        mapOBStagingConfiguration.put(sOM.Object__c, new List<StagingObjConfig>{sOC});
                    }                    
                    
                }                                        
            }
            
            return mapOBStagingConfiguration;
        }
        set;
    }
    
    public static Map<String, List<StagingObjConfig>> mapIBStagingConfiguration 
    {
        get 
        {
            if (mapIBStagingConfiguration == null) 
            {                   
                mapIBStagingConfiguration = new Map<string,List<StagingObjConfig>>();
                List<Staging_Object_Config__mdt> sObjectConfig = [Select Fields_Map__c,Exclude_Sync_Event_Fields__c,External_Id_Field__c,is_Deletion_Allowed__c,is_Modification_Allowed__c,is_Creation_Allowed__c, MasterLabel, MappingType__c, Object__c,RecordType_Names__c,Delete_Fields_Map__c, Config_For__c from Staging_Object_Config__mdt where MappingType__c = 'InBound'];
                for(Staging_Object_Config__mdt sOM : sObjectConfig)
                {                               
                    StagingObjConfig sOC = new StagingObjConfig();
                    System.debug(sOM.Fields_Map__c); 
                    
                    if(!string.isBlank(sOM.Fields_Map__c))
                    {
                        Map<String,String> mapFieldsMapping = (Map<String,String>) JSON.deserialize(sOM.Fields_Map__c, Map<String,String>.class);
                        system.debug('SIze>>>>>'+mapFieldsMapping.size());
                        system.debug('Meta>>>>>'+mapFieldsMapping); 
                        
                        if(mapFieldsMapping.size() > 0)
                        {
                            //mapObjectFieldsMapping.put(sOM.sOM.Object__c , mapFieldsMapping);
                            sOC.mapObjectFieldsMapping.putAll(mapFieldsMapping);
                        }
                    }
                    
                    if(!string.isBlank(sOM.Delete_Fields_Map__c))
                    {
                        System.debug(sOM.Delete_Fields_Map__c); 
                        Map<String,String> mapDeleteFieldsMapping = (Map<String,String>) JSON.deserialize(sOM.Delete_Fields_Map__c, Map<String,String>.class);
                        system.debug('SIze>>>>>'+mapDeleteFieldsMapping.size());
                        system.debug('Meta>>>>>'+mapDeleteFieldsMapping); 
                        
                        if(mapDeleteFieldsMapping.size() > 0)
                        {
                            //mapObjectFieldsMapping.put(sOM.sOM.Object__c , mapFieldsMapping);
                            sOC.mapObjectFieldsDeleteMapping.putAll(mapDeleteFieldsMapping);
                        }
                    }
                                        
                    System.debug(sOM.RecordType_Names__c); 
                    if(!string.isBlank(sOM.RecordType_Names__c))
                    {
                       List<string> lstRecTypes = sOM.RecordType_Names__c.split(',');
                        system.debug('SIze>>>>>'+lstRecTypes.size());
                        system.debug('Meta>>>>>'+lstRecTypes); 
                        
                        if(lstRecTypes.size() > 0)
                        {
                            sOC.availableRecordTypes.addAll(lstRecTypes);
                            
                            /*Map<string, Schema.RecordTypeInfo> recordtypesInfo = getsObjectRecordTypes(sOM.Object__c);
                            if(recordtypesInfo != null && recordtypesInfo.size() > 0)
                            {
                                for(string key : lstRecTypes)
                                {
                                    if(recordtypesInfo.containsKey(key))
                                    {
                                        sOC.availableRecordTypeIds.add(recordtypesInfo.get(key).getName());
                                    }
                                }
                            }*/
                            
                            //mapObjectRecordTypeMapping.put(sOM.Object__c , lstRecTypes);
                        }
                    }
                    
                    System.debug(sOM.Exclude_Sync_Event_Fields__c); 
                    if(!string.isBlank(sOM.Exclude_Sync_Event_Fields__c))
                    {
                       List<string> excludeChangeCapture = sOM.Exclude_Sync_Event_Fields__c.split(',');
                        system.debug('SIze>>>>>'+excludeChangeCapture.size());
                        system.debug('Meta>>>>>'+excludeChangeCapture); 
                        
                        if(excludeChangeCapture.size() > 0)
                        {
                            sOC.excludeChangeCaptureFields.addAll(excludeChangeCapture);
                            //mapObjectRecordTypeMapping.put(sOM.Object__c , excludeChangeCapture);
                        } 
                    }
                    
                    sOC.ConfigFor = sOM.Config_For__c;
                    sOC.isDeletionAllowed = sOM.is_Deletion_Allowed__c;
                    sOC.isCreationAllowed = sOM.is_Creation_Allowed__c;
                    sOC.isModificationAllowed = sOM.is_Modification_Allowed__c;
                    sOC.sObjectName = sOM.Object__c;
                    sOC.externalIdField = sOM.External_Id_Field__c;
                    if(mapIBStagingConfiguration.containsKey(sOM.Object__c))
                    {
                       mapIBStagingConfiguration.get(sOM.Object__c).add(sOC);
                    }
                    else
                    {
                        mapIBStagingConfiguration.put(sOM.Object__c, new List<StagingObjConfig>{sOC});
                    }                    
                    
                }                                        
            }
            
            return mapIBStagingConfiguration;
        }
        set;
    }
    
    private static Map<string, MAP<string, Schema.RecordTypeInfo>> sObjRecordTypes {get;set;}
    
    public static Map<string, Schema.RecordTypeInfo> getsObjectRecordTypes(string key)
    {
        Map<string, Schema.RecordTypeInfo> results = new Map<string, Schema.RecordTypeInfo>();
        
        if(!string.isBlank(key))
        {
            if(sObjRecordTypes != null && sObjRecordTypes.size() > 0 && sObjRecordTypes.containsKey(key))
            {                
                results = sObjRecordTypes.get(key);
            }
            else
            {
                updatesObjectRecordTypes(key);
                
                if(sObjRecordTypes != null && sObjRecordTypes.size() > 0 && sObjRecordTypes.containsKey(key))
            	{
                    results = sObjRecordTypes.get(key);
                }
                
            }
        }
        return results;
        
    }
    
    private static void updatesObjectRecordTypes(string key)
    {        
        if(sObjRecordTypes == null)        
        {
            sObjRecordTypes = new Map<string, MAP<string, Schema.RecordTypeInfo>>();
        }
        
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(key);            
        if(targetType != null)
        {
            Schema.DescribeSobjectResult objTypeDesc = targetType.getDescribe();
			if(objTypeDesc != null)
            {
             	Map<string, Schema.RecordTypeInfo> recTypeInfo = objTypeDesc.getRecordTypeInfosByName();
                if(recTypeInfo != null && recTypeInfo.size() > 0)
            		sObjRecordTypes.put(key,recTypeInfo);   
            }            
        }
    }
        
}