/**
 * Created by alan on 2019-02-26.
 */

@isTest
public class StringArgumentMatcher extends ArgumentMatcher{

    private String expectedVal;

    public StringArgumentMatcher(String expectedVal){
        super();
        this.expectedVal = expectedVal;
    }

    public override Boolean isMatch(Object arg){
        return expectedVal.equals(arg);
    }
}