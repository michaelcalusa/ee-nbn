/***************************************************************************************************
Class Name:  StringToDate_Test 
Class Type: Test Class 
Version     : 1.0 
Created Date: 31-10-2016
Function    : This class contains unit test scenarios for StringToDate apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       31-10-2016                Created
****************************************************************************************************/
@isTest
private class StringToDate_Test {
    static testMethod void StringToDate_Test(){
        date x = date.newinstance(1963, 01,24);
        test.startTest();
        StringToDate.strDate(string.valueOf(x));
        test.stopTest();
    }
}