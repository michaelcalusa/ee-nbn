/***************************************************************************************************
    Class Name          : SubscribeNewDevEventHandler
    Test Class          :
    Version             : 1.0 
    Created Date        : 17-Nov-2017
    Author              : Anjani Tiwari
    Description         : Handler Class to check identify the events and send message  to appian
    Input Parameters    :

    Modification Log    :
    * Developer             Date            Description
      Anjani Tiwari        19-Mar-2018      Create SD Reference No on Stage Appreceived from Appian 
    * ----------------------------------------------------------------------------                 

****************************************************************************************************/ 
 public class SubscribeNewDevEventHandler{
    public static void eventTypesIdentification(list<Developments__e>eventDev){
       string evtBody;
       set<string> setAppIds = new set<string>();
       Set<Id> rejectedIds= new Set<Id>();
       Set<Id> assesssedIds= new Set<Id>();  
       List<Developments__e> taskCreateEventList = new List<Developments__e>();
       List<Developments__e> taskUpdateEventList = new List<Developments__e>();
       List<Developments__e> remediationEventList = new List<Developments__e>();
       List<Developments__e> InvoicePaidEventList = new List<Developments__e>();
       New_Dev_Platform_Events__c identifierList = New_Dev_Platform_Events__c.getOrgDefaults();
       Set<String> corelationIds = new Set<String>();
       Map<String, Stage_Application__c> stageAppMap = new Map<String,Stage_Application__c>();
       system.debug('event dev '+eventDev);
       for(Developments__e pEv: eventDev){
          if(pEv.Source__c == 'Salesforce')
             corelationIds.add(pEv.Correlation_Id__c);         
       }
       
       for(Stage_Application__c stgApp : [Select Id, Application_Number__c from Stage_Application__c where Application_Number__c in : corelationIds]){
          stageAppMap.put(stgApp.Application_Number__c, stgApp);
       }      
        system.debug('**stageAppMap**'+stageAppMap); 
       
       for(Developments__e ev: eventDev){
            if(ev.Source__c == 'Salesforce'){
                String strJSON = JSON.serialize(ev);
                String evIdentifier;
                system.debug('**PublishedJSON**'+strJSON);                
                String exCorelationId; 
                
                if(stageAppMap.containsKey(ev.Correlation_Id__c))
                   exCorelationId = stageAppMap.get(ev.Correlation_Id__c).Id;
                   system.debug('**exCorelationId**'+exCorelationId); 
                   
                if((ev.Correlation_Id__c!=null) && (ev.Event_Type__c == 'Webform Submission') && (ev.Event_Sub_Type__c == 'Send To Salesforce')){
                    setAppIds.add(ev.Correlation_Id__c);
                    List<NewDev_Application__c> newDevAppRecords = HelperNewDevUtility.getNewDevAppRecordbyAppId(setAppIds);               
                    if(newDevAppRecords!= null && newDevAppRecords.size()>0 ){
                        NewDevWebFormTriggerHandler.processNewDevFlowExecution(newDevAppRecords);
                    }
                } 
                else if((ev.Correlation_Id__c!=null) && (ev.Event_Type__c == 'Webform Submission') && (ev.Event_Sub_Type__c == 'Send To CRMoD')){
                     setAppIds.add(ev.Correlation_Id__c);
                    List<NewDev_Application__c> newDevAppRecords = HelperNewDevUtility.getNewDevAppRecordbyAppId(setAppIds);               
                    if(newDevAppRecords!= null && newDevAppRecords.size()>0 ){
                        InsertAccountCRMOD.InsertaccCRMOD(newDevAppRecords[0].id);
                    }                   
                }
                else if((ev.Event_Id__c==identifierList.Area_Planning_Assessment__c) || (ev.Event_Id__c==identifierList.Kickoff_SD_2_process__c) ||
                        (ev.Event_Id__c==identifierList.Additional_Info_Received__c) || (ev.Event_Id__c==identifierList.Design_Document_Uploaded__c) || 
                        (ev.Event_Id__c==identifierList.Delivery_Time_Impacted__c)   || (ev.Event_Id__c==identifierList.Premise_Count_Change_SF__c) ||
                        (ev.Event_Id__c==identifierList.Master_Plan_Submitted__c)    || (ev.Event_Id__c==identifierList.Stage_Plan_Submitted__c) ||
                        (ev.Event_Id__c==identifierList.Service_Plan_Submitted__c) ||(ev.Event_Id__c==identifierList.As_built_Design_Submitted__c) ||
                        (ev.Event_Id__c==identifierList.Remediation_Complete__c) || (ev.Event_Id__c==identifierList.Application_Withdrawn__c) ||
                        (ev.Event_Id__c==identifierList.Additional_Document_Submitted__c) || (ev.Event_Id__c==identifierList.Practical_Completion_Notice__c) ||
                        (ev.Event_Id__c==identifierList.Remediation_not_complete__c) || (ev.Event_Id__c == identifierList.Attribute_Updates__c)){
                         system.debug('**$$%**'+ev.Event_Id__c+exCorelationId+strJSON);
                         evtBody = '{"eventIdentifier":"'+ev.Event_Id__c+'","externalSystemCorrelationId":"'+exCorelationId+'","eventBody":'+StrJSON+'}';
                         AppianCallout.SendInformationToAppian(evtBody,ev.Event_Id__c,exCorelationId);     //Appian rest callout for Technical Assesment, Sd2 process, pit @ Pipe doc uploaded, Task closure
                }
                else if(ev.Event_Id__c==identifierList.Invoice_Paid__c){
                    InvoicePaidEventList.add(ev);
                }
             }
          
          
         
             else if(ev.Source__c == 'Appian'){
                    if((ev.Event_Id__c == identifierList.Application_was_a_duplicate__c)      || (ev.Event_Id__c == identifierList.Application_commercially_not_viable__c) ||
                       (ev.Event_Id__c == identifierList.Additional_Information_Required__c)  || (ev.Event_Id__c == identifierList.Address_Info_cannot_be_confirmed__c) ||
                       (ev.Event_Id__c == identifierList.LASE_Risk_is_Identified__c)          || (ev.Event_Id__c == identifierList.Inbound_Delivery_Time_Impacted__c) || 
                       (ev.Event_Id__c == identifierList.Premise_Count_Mismatch__c)           || (ev.Event_Id__c == identifierList.Location_RFSed__c) ||
                       (ev.Event_Id__c == identifierList.Remediation_Report_Pass__c)          || (ev.Event_Id__c == identifierList.Area_Planning_Assessment_Assessed__c) || 
                       (ev.Event_Id__c == identifierList.Area_Planning_Assessment_Rejected__c)||(ev.Event_Id__c == identifierList.Additional_Document_Required__c) ||
                       (ev.Event_Id__c == identifierList.Appian_Application_Reference_No__c)|| (ev.Event_Id__c == identifierList.Application_On_Hold__c) ||
                       (ev.Event_Id__c == identifierList.Application_Resumed__c) || (ev.Event_Id__c == identifierList.Attribute_Updates_Appian__c)){
                       taskCreateEventList.add(ev);                                                        
                    }
                 
                    else if(ev.Event_Id__c == identifierList.Master_Plan_Accepted__c     || ev.Event_Id__c == identifierList.Master_Plan_Rejected__c ||
                            ev.Event_Id__c == identifierList.Design_Document_Accepted__c || ev.Event_Id__c == identifierList.Design_Document_Rejected__c ||
                            ev.Event_Id__c == identifierList.Stage_Plan_Accepted__c      || ev.Event_Id__c == identifierList.Stage_Plan_Rejected__c ||
                            ev.Event_Id__c == identifierList.Service_Plan_Accepted__c    || ev.Event_Id__c == identifierList.Service_Plan_Rejected__c ||
                            ev.Event_Id__c == identifierList.As_built_Design_Accepted__c || ev.Event_Id__c == identifierList.As_built_Design_Rejected__c ||
                            ev.Event_Id__c == identifierList.Additional_Document_Accepted__c || ev.Event_Id__c == identifierList.Additional_Document_Rejected__c){
                            taskUpdateEventList.add(ev); 
                    }
                 else if(ev.Event_Id__c == identifierList.Remediation_Report_Fail__c){
                     remediationEventList.add(ev);                           
                 }
                    
             }    
       }
      
       If(taskCreateEventList!=null && (!taskCreateEventList.isEmpty())){
           ProcessAppianSubscribedEvents.CreateTasksOnApplication(taskCreateEventList);
       }
       system.debug('@taskUpdateEventList==='+taskUpdateEventList);
       If(taskUpdateEventList!=null && (!taskUpdateEventList.isEmpty())){
           ProcessAppianSubscribedEvents.UpdateTasksOnApplication(taskUpdateEventList);
       }
       If(remediationEventList!=null && (!remediationEventList.isEmpty())){
           ProcessAppianSubscribedEvents.remediationTasksOnApplication(remediationEventList);
       }
       If(InvoicePaidEventList!=null && (!InvoicePaidEventList.isEmpty())){
           newDevPlatformEventsSubscriberUtill.updateStageAppStatus(InvoicePaidEventList);
       }
    } 
 }