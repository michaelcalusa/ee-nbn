/*
@Auther : Anjani Tiwari
@Date : 14/Dec/2017
@Description : Test Class for class SubscribeNewDevEventHandler
*/
@isTest
Public class SubscribeNewDevEventHandler_Test{
   
   static testMethod void eventTypesIdentification_Test(){
      ObjectCommonSettings__c invSet = 
            new ObjectCommonSettings__c(Name = 'Invoice Statuses', New__c = 'New', Requested__c = 'Requested', Cancelled__c = 'Cancelled', Pending_Approval__c = 'Pending Approval', 
                Cancellation_Requested__c = 'Cancellation Requested', Cancellation_Pending_Approval__c = 'Cancellation Pending Approval', 
                Payment_Status_Paid__c = 'Paid', GST_Exclusive__c = 'GST (Exclusive)', GST_Inclusive__c = 'GST (Inclusive)', 
                Discount_Type_Amount__c = 'Amount', ILI_Type_Cancel_Inv__c = 'Cancel Inv',Rejected__c = 'Rejected', Refer_to_Credit__c = 'Refer to Credit');
        
      insert invSet;
      // added my john huang
      SA_Auto_Reference_Number__c refNumber = new SA_Auto_Reference_Number__c(Name = 'SA001', Last_Sequence_Number__c = '000000006');
      insert refNumber;
      
      Account a = new Account(Name='TestAccount');
      insert a;
      Contact c = new Contact (AccountId= a.Id, FirstName = 'firstName' , LastName = 'lastName');
      insert c;
      Development__c dev = new Development__c(Name= 'TestDev',Account__c = a.id, Primary_Contact__c = c.id, Development_ID__c = 'dev01', Suburb__c = 'Test');
      insert dev;
      Stage_Application__c stgApp = new Stage_Application__c(Reference_Number__c ='ref-0001',Request_ID__c='TestRq1',  Dwelling_Type__c ='SDU Only',
                                                           Development__c=dev.id,Active_Status__c ='Active',  Name = 'TestApp',Application_Number__c='STG-W00002323');
      insert stgApp;
      Id OpptyrtId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('New Development SCO').getRecordTypeId();
      opportunity opty = new opportunity(Name='test oppty 001', StageName= 'New', CloseDate= Date.today(), accountId = a.Id);
      Insert opty;
      Id InvRtype = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Invoice').getRecordTypeId();
      invoice__c inv = new invoice__c(Name='To be Issued', Stage_Application__c = stgApp.Id, RecordTypeId= InvRtype, Opportunity__c= opty.Id, status__c='Requested');
      insert inv;
      stgApp = [Select Id,Application_ID__c,Application_Number__c from Stage_Application__c where id = : stgApp.id];       
      system.debug('@StageAppTest'+stgApp+stgApp.Application_ID__c);    

      Task t = new Task();              
      t.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Development').getRecordTypeId();
      t.Priority = '3-General';
      t.Status = 'Open';              
      t.WhatId = stgApp.id;
      t.ownerId = UserInfo.getUserId();
      t.Deliverable_Status__c = 'Submitted';
      t.Type_Custom__c = 'Deliverables';
      t.Sub_Type__c ='As-built Design';     
      insert t;      
                                                     
      List<Developments__e> eventList = new List<Developments__e> ();
      Developments__e ev1 = new Developments__e(Event_Id__c='AP-SF-000014',Correlation_Id__c = stgApp.Application_Number__c,Event_Sub_Type__c='Send To Salesforce',Event_Type__c='Webform Submission', 
                                             Message_Body__c='eventIdentifier:AP-SF-000014,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Salesforce');
      eventList.add(ev1);                                       
      Developments__e ev2 = new Developments__e(Event_Id__c='SF-SD-000004',Correlation_Id__c = stgApp.Application_Number__c, Event_Sub_Type__c='Duplicate', Event_Type__c='Service Delivery Rejection',
                                             Message_Body__c='eventIdentifier:SF-SD-000010,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Salesforce');                                
      eventList.add(ev2);
      Developments__e ev3 = new Developments__e(Correlation_Id__c = stgApp.Application_Number__c,Event_Type__c='Webform Submission', 
                                             Message_Body__c='eventIdentifier:SF-SD-000010,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Salesforce');
      eventList.add(ev3);
      Developments__e ev4 = new Developments__e(Event_Id__c='AP-SF-000020',Correlation_Id__c = stgApp.Application_Number__c,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                             Message_Body__c='eventIdentifier:AP-SF-000014,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Appian');      
      eventList.add(ev4);  
      Developments__e ev5 = new Developments__e(Event_Id__c='SF-SD-000015',Correlation_Id__c = stgApp.Application_Number__c,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                             Message_Body__c='eventIdentifier:SF-SD-000015,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Salesforce');      
      eventList.add(ev5);  
      Developments__e ev6 = new Developments__e(Event_Id__c='AP-SF-000034',Correlation_Id__c = stgApp.Application_Number__c,Event_Sub_Type__c='Duplicate',Event_Type__c='Service Delivery Rejection', 
                                             Message_Body__c='eventIdentifier:SF-SD-000015,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Appian');      
      eventList.add(ev6);
      Developments__e ev7 = new Developments__e(Event_Id__c='SF-INV-000507',Event_Record_Id__c= inv.id, Event_Sub_Type__c='Duplicate',Event_Type__c='Invoice Paid', 
                                             Source__c='Salesforce');      
      eventList.add(ev7);
      Developments__e ev8 = new Developments__e(Event_Id__c='AP-SF-000016',Event_Type__c='Remidiation Fail', 
                                             Message_Body__c='eventIdentifier:AP-SF-000016,applicationId:'+stgApp.Id+',comments:Duplicated',Source__c='Appian');      
      eventList.add(ev8); 
      Developments__e ev9 = new Developments__e(Event_Id__c='AP-SF-000014',Correlation_Id__c = stgApp.Application_Number__c,Event_Sub_Type__c='Send To CRMoD',Event_Type__c='Webform Submission', 
                                             Message_Body__c='eventIdentifier:AP-SF-000014,applicationId:'+stgApp.Id+',comments:Duplicated', Source__c='Salesforce');
      eventList.add(ev9);     
      
      New_Dev_Platform_Events__c settings = New_Dev_Platform_Events__c.getOrgDefaults();
      settings.Additional_Info_Received__c = 'SF-SD-000010';
      settings.LASE_Risk_is_Identified__c = 'AP-SF-000014';
      settings.Location_RFSed__c = 'AP-SF-000013';
      settings.Master_Plan_Accepted__c = 'AP-SF-000020';
      settings.As_built_Design_Accepted__c ='AP-SF-000022';
      settings.Service_Plan_Submitted__c = 'SF-SD-000015';
      settings.Additional_Document_Rejected__c = 'AP-SF-000027';
      settings.Application_Resumed__c= 'AP-SF-000034';
      settings.Practical_Completion_Notice__c= 'SF-SD-000004';
      settings.Unassigned_User_Id__c = UserInfo.getUserId();
      upsert settings New_Dev_Platform_Events__c.Id;
      
      Map<String, String> TestMap;
      Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator_Test(200,'testMedhod',TestMap)); 
     Test.startTest();
      if(eventList!=null && (!eventList.isEmpty())){
        List<Database.SaveResult> results = EventBus.publish(eventList);
         for (Database.SaveResult sr : results) {
             if (sr.isSuccess()) {
                System.debug('Successfully published event.');
             } else {
                   for(Database.Error err : sr.getErrors()) {
                     System.debug('Error returned: ' + err.getStatusCode() +   ' - ' + err.getMessage());
                   }    
              }
         }
        SubscribeNewDevEventHandler.eventTypesIdentification(eventList);   
      }
      Test.stopTest();

   }
}