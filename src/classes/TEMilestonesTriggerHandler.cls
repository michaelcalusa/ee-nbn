public class TEMilestonesTriggerHandler{
     //Class level variables that are commonly used
    private boolean isExecuting = false;
    private integer batchSize;
    private List<Transition_Engagement_Milestones__c> trgOldList = new List<Transition_Engagement_Milestones__c> ();
    private List<Transition_Engagement_Milestones__c> trgNewList = new List<Transition_Engagement_Milestones__c> ();
    private Map<id,Transition_Engagement_Milestones__c> trgOldMap = new Map<id,Transition_Engagement_Milestones__c> ();
    private Map<id,Transition_Engagement_Milestones__c> trgNewMap = new Map<id,Transition_Engagement_Milestones__c> ();
    private static boolean isAsync;
    // Below 7 boolean variables are used to Prevent recursion
    public static boolean isBeforeInsertFirstRun = true;
    public static boolean isBeforeUpdateFirstRun = true;
    public static boolean isBeforeDeleteFirstRun = true;
    public static boolean isAfterInsertFirstRun = true;
    public static boolean isAfterUpdateFirstRun = true;
    public static boolean isAfterDeleteFirstRun = true;
    public static boolean isAfterUndeleteFirstRun = true;
    /***************************************************************************************************
    Method Name         : TransitionEngagementMilestonesTriggerHandler 
    Method Type         : Constructor
    Version             : 1.0 
    Created Date        : 
    Function            : 
    Input Parameters    : 
    Output Parameters   : 
    Description         :  
    Used in             : 
    Modification Log    :
    * Developer                   Date                   Description
    * -----------------------------------------------------------------------------------------------                
    * SUKUMAR SALLA             26-04-2018                Created
    ****************************************************************************************************/  
    public TEMilestonesTriggerHandler(boolean isExecuting, integer batchSize, List<Transition_Engagement_Milestones__c> trgOldList, List<Transition_Engagement_Milestones__c> trgNewList, Map<id,Transition_Engagement_Milestones__c> trgOldMap, Map<id,Transition_Engagement_Milestones__c> trgNewMap){
        this.isExecuting = isExecuting;
        this.BatchSize = batchSize;
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
        TEMilestonesTriggerHandler.isAsync = System.isBatch() || System.isFuture();
    }
    public void OnBeforeInsert(){}
    public void OnBeforeUpdate(){
         validateTransitionEngagementMilestones(trgNewList, trgNewMap, trgOldMap);
    }
    public void OnBeforeDelete(){}
    public void OnAfterInsert(){}
    public void OnAfterUpdate(){}
    public void OnAfterDelete(){}
    public void OnUndelete(){}
    /*---------------------------------------------------------------------------------------------------------------------------------------------------*/
    
          
    // This method used to validate Dates on Action Plan based on related tasks
    public void validateTransitionEngagementMilestones(List<Transition_Engagement_Milestones__c> trgNewList, Map<id,Transition_Engagement_Milestones__c> trgNewMap, Map<id,Transition_Engagement_Milestones__c> trgOldMap){    
       
        Map<string, List<task>> mapTransitionEngagementAndRelatedTasks = new Map<string, List<task>>();
        String allErrors = '';
        // Consider only tasks whose type is 'Sub Task'.        
        for(task t:[select id, whatId, Status, Start_Date__c, Completed_Date__c from Task where whatId in: trgNewMap.keyset() and Type ='Sub Task']){ 
            if(!mapTransitionEngagementAndRelatedTasks.containskey(t.whatId)){
                mapTransitionEngagementAndRelatedTasks.put(t.whatId, new List<task>());
                mapTransitionEngagementAndRelatedTasks.get(t.whatId).add(t);
            }
            else{
                mapTransitionEngagementAndRelatedTasks.get(t.whatId).add(t);
            }           
        }
        for(string apid: mapTransitionEngagementAndRelatedTasks.keyset()){            
            Boolean startDateErrorFlag = false;
            Boolean completedDateErrorFlag = false;
            Boolean transitionEngagementNotCompleted = false;
            Boolean transitionEngagementTaskNotRequired = false;
            Boolean transitionEngagementTaskCompleted = false;
            
            Transition_Engagement_Milestones__c ap =  trgNewMap.get(apid);
            List<task> lstTasks = mapTransitionEngagementAndRelatedTasks.get(apid);
            system.debug('**task ==>'+ lstTasks );
            
             for(Task t: lstTasks){ 
                 if(t.Status == 'Open'){
                     transitionEngagementNotCompleted = true;
                 }
                 else if(t.Status == 'Completed'){
                     transitionEngagementTaskCompleted = true;
                 }
                 else if(t.Status == 'Not required'){
                     transitionEngagementTaskNotRequired = true;
                 }
             }
            //======================= Validations on Transition Engagement ====================================//
            // Milestones Start Date should be set earlier than the Milestones Completed Date
            if(ap.Start_Date__c!=null && ap.Completed_Date__c!=null && ap.Start_Date__c>ap.Completed_Date__c){
                allErrors = allErrors+'\n\r'+ 'Milestone Start Date should be set earlier than the Milestone Completed Date.'; 
                //EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_001__c;
            }
            // Milestones Completed Date cannot be set as Milestones status is not "Completed"           
            if(ap.Status__c!='Completed' && ap.Completed_Date__c!=null){                
                allErrors = allErrors+'\n\r'+ 'Milestone Completed Date cannot be set as Milestone status is not "Completed".';
                    //EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_005__c;
            }
            // Milestones Start Date and Milestones Completed Date should not be null when the status of Milestones is completed
            if(ap.Status__c =='Completed' && (ap.Completed_Date__c==null || ap.Start_Date__c==null)){                
                allErrors = allErrors+'\n\r'+ 'Please enter Milestone Start Date and Milestone Completed Date.';
                    //EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_006__c;
            }
            //======================= Validations on Transition Engagement depending on associated Milestones (subtasks) ======================== //
            // Please close all the milestones before Transition Engagement can be Completed
            If(ap.Status__c=='Completed' && transitionEngagementNotCompleted){
                allErrors = allErrors+'\n\r'+ 'Please close all the Sub Tasks before Milestone can be Completed.';
                    //EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_007__c;
            }
           
                        
            for(Task t: lstTasks){
                // Start Date must be set earlier than any of Sub task Start Date
                if(t.Start_Date__c < ap.Start_Date__c){
                  if(!startDateErrorFlag){
                    allErrors = allErrors+'\n\r'+ 'Milestone Start Date must be set earlier than any of Sub task Start Date.';
                        //EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_002__c;
                    startDateErrorFlag = True;
                  }
                }
                // Completed Date must be set later than any of Sub Task End Date
                if(t.Completed_Date__c > ap.Completed_Date__c && ap.Status__c == 'Completed'){
                 if(!completedDateErrorFlag){
                    allErrors = allErrors+'\n\r'+ 'Milestone Completed Date must be set later than any of Sub Task End Date.';
                        //EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_003__c;
                    completedDateErrorFlag = True;
                 }
                }
            }
            if(allErrors!=''){
                    ap.addError(allErrors, false);
            }
        }
    }
    
}