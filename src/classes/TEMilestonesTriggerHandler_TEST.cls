@isTest
public class TEMilestonesTriggerHandler_TEST {
   static testmethod void test_UpdateTEMilestones(){
      List<Account> lstAcc = TestDataUtility.getListOfAccounts(1, true); 
      Opportunity opp = new Opportunity();
      opp.Name = 'Test Opp';
      opp.StageName = 'Qualify';
      opp.AccountId = lstAcc[0].id;
      opp.CloseDate = System.today();        
      insert opp;
      
      Transition_Engagement__c te = new Transition_Engagement__c();
      te.Assigned_To__c = null;
      te.Opportunity__c = opp.id;
      insert te;   
       
      Test.startTest(); 
      try{
      List<Transition_Engagement_Milestones__c> lstTEM = [SELECT Id from Transition_Engagement_Milestones__c Where Transition_Engagement__c =: te.Id];
           List<Task> lstTask = [Select Id from Task Where WhatId =: lstTEM[1].Id];
           lstTask[0].Start_Date__c = System.today() - 2;          
           Update  lstTask[0];
      lstTEM[0].Status__c = 'Completed';
      lstTEM[1].Start_Date__c = System.today();
      lstTEM[1].Completed_Date__c = System.today() - 1;
      Update lstTEM;
       }
       catch(Exception e)           
       {
           String message = e.getMessage();
           Boolean expectedExceptionThrown =  e.getMessage().contains('Script-thrown exception') ? true : false;
           System.assertEquals(expectedExceptionThrown, false);
       }
      Test.stopTest();
      
   }
}