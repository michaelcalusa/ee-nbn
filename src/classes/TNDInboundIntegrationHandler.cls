/*
    * @description: Handler class for trigger TNDInboundIntegration
    * @created:11/13/2017
    
    */
public class TNDInboundIntegrationHandler {
   
  
    public static void TNDInboundInt(List<TNDInboundIntegration__e > TNDevent){
    List<String> inboundJSONMessageList =new List<String>();
    for (TNDInboundIntegration__e event: TNDevent) {       
        System.debug('Inbound TNDInboundIntegration__e  ----->>>>> event.InboundJSONMessage__c: ' + event.InboundJSONMessage__c);              
        System.debug('@@Trigger.New values'+Trigger.New); 
        if (!String.isBlank(event.InboundJSONMessage__c)){
            inboundJSONMessageList.add(event.InboundJSONMessage__c);
            GlobalUtility.logMessage(GlobalConstants.DEBUG_RECTYPE_NAME,'TNDInboundIntegration__e','eventfiredinSF','','event',event.InboundJSONMessage__c,'',null,0);
        }
      }    
    displayTNDresults_LC.processTNDJSON(inboundJSONMessageList );
   }  
}