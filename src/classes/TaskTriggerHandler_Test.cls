/*------------------------------------------------------------------------
Author:        Andrew Zhang
Company:       NBNco
Description:   Test class for TaskTriggerHandler class
History
13/12/2017     Ganesh    Increase Test Converage
----------------------------------------------------------------------------*/

@isTest
private class TaskTriggerHandler_Test {
 
    //test non-admin user can insert task, but cannot delete
    static testmethod void testDelete(){


        //CS case management Team queue
        Customer_Service_Team__c csmt = new Customer_Service_Team__c(
            Name = 'CS Case Management Team',
            Email__c = 'tesxtea@example.com',
            Task_Assignment_Notification__c = true
        );
        insert csmt;
        
        //create unassigned user    
        Profile p = [SELECT Id FROM Profile WHERE Name = 'NBN Contact Centre User' LIMIT 1];   
        User unassignedUser = TestDataUtility.createTestUser(false, p.Id);
        insert unassignedUser;
        System.debug('===unassignedUser:' + unassignedUser);

        //create Customer Service Setting
        Customer_Service_Setting__c css = new Customer_Service_Setting__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            Unassigned_User_Id__c = unassignedUser.Id,
            CS_Case_Management_Team_Id__c = csmt.Id,
            Task_Record_Type_Id_Task__c = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Task').getRecordTypeId(),
            Task_Record_Type_Id_Note__c = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Note').getRecordTypeId(),
            Task_Record_Type_Id_Interaction__c = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Interaction').getRecordTypeId()
        );
        System.debug('===Customer_Service_Setting__c:' + css);
        insert css;
                                        
        //create a user
        User user = TestDataUtility.createTestUser(false, p.Id);
        user.Username = 'testuser1232132@example.com';
        insert user ;
                
        System.runAs(user){

    
            //Setup data
            Customer_Service_Team__c team = new Customer_Service_Team__c(
                Name = 'Test Team',
                Email__c = 'test@example.com',
                Task_Assignment_Notification__c = true
            );
            insert team;
             
            Account account = new Account(
                Name = 'Test Account',
                RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
                Tier__c = '1',
                Type__c = 'RSP',
                Status__c = 'Active'
            );
            insert account;
            
            Task task = new Task(
                RecordTypeId = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Task').getRecordTypeId(),
                Subject = 'Call',
                Status = 'Open',
                Priority = '3-General',
                WhatId = account.Id,
                Team__c = team.Id,
                Due_Date__c = Date.Today(),
                Type_Custom__c = 'Contact',
                Sub_TYpe__c = 'Internal'
            );
            insert task;
            System.debug('===task: ' + task);
            
            test.startTest();   
            
            try{         
                delete task;
            }catch(Exception e){
                System.Assert(e.getMessage().contains(label.Task_Deletion_Error_Message));
            }
            
            test.stopTest();        
        
        }   

    }
    
    //test insert and update
    static testmethod void testInsertAndUpdate(){
        
        List<Task> taskList = new List<Task>();

        //CS case management Team queue
        Customer_Service_Team__c csmt = new Customer_Service_Team__c(
            Name = 'CS Case Management Team',
            Email__c = 'tesxtea@example.com',
            Task_Assignment_Notification__c = true
        );
        insert csmt;
        
        //create unassigned user    
        Profile p = [SELECT Id FROM Profile WHERE Name = 'NBN Contact Centre User' LIMIT 1];   
        User unassignedUser = TestDataUtility.createTestUser(false, p.Id);
        insert unassignedUser;
        System.debug('===unassignedUser:' + unassignedUser);

        //create Customer Service Setting
        Customer_Service_Setting__c css = new Customer_Service_Setting__c(
            SetupOwnerId = UserInfo.getOrganizationId(),
            Unassigned_User_Id__c = unassignedUser.Id,
            CS_Case_Management_Team_Id__c = csmt.Id,
            Task_Record_Type_Id_Task__c = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Task').getRecordTypeId(),
            Task_Record_Type_Id_Note__c = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Note').getRecordTypeId(),
            Task_Record_Type_Id_Interaction__c = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Interaction').getRecordTypeId()
        );
        System.debug('===Customer_Service_Setting__c:' + css);
        insert css;
                

        //create account
        Account account = new Account(
            Name = 'Test Account',
            RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
            Tier__c = '1',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert account;
        
        Contact contact = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId(),
            AccountId = account.Id,
            Email = 'teste@example.com',
            Preferred_Phone__c = '0288889999'
        );
        insert contact;
        
        Case caseObj = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Query').getRecordTypeId(),
            Phase__c = 'Information',
            Category__c = '(I) Communications',
            Sub_Category__c = 'Discovery Centre',
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
            ContactId = contact.Id,
            Primary_Contact_Role__c = 'General Public'
        );
        insert caseObj;
        
        //Create a Dveleopments object record
        Development__c devObj = new Development__c();      
        devObj.Name='testName';
        devObj.Development_ID__c='AKHS833';
        devObj.Account__c=account.Id;
        devObj.Primary_Contact__c=contact.Id;
        devObj.Suburb__c='testsuburd';
        insert devObj;
        
        SA_Auto_Reference_Number__c refNumber = new SA_Auto_Reference_Number__c(Name = 'SA001', Last_Sequence_Number__c = '000000000');
        insert refNumber;
        
        // Create Stage Application object record
        Stage_Application__c stageAppObj=new Stage_Application__c();
        stageAppObj.Name='testName';
        stageAppObj.Request_ID__c ='AKHS833';
        stageAppObj.Development__c=devObj.Id;
        stageAppObj.Account__c=account.Id;
        stageAppObj.Primary_Contact__c=contact.Id;
        insert stageAppObj; 
        
        
        Customer_Service_Team__c team = new Customer_Service_Team__c(
            Name = 'Test Team',
            Email__c = 'test@example.com',
            Task_Assignment_Notification__c = true
        );
        insert team;
            
        test.startTest(); 
        
        //test insert task
        Task task = new Task(
            RecordTypeId = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Task').getRecordTypeId(),
            Subject = 'Call',
            Priority = '3-General',
            WhoId=contact.Id,
            WhatId = account.Id,
           // Team__c = team.Id,
            Due_Date__c = Date.Today(),
            Type_Custom__c = 'Contact',
            Sub_Type__c = 'Internal'
        );
        insert task;
        //taskList.add(task);
        
        Task task2 = new Task(
            RecordTypeId = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Task').getRecordTypeId(),
            Subject = 'Call',
            Priority = '3-General',
            WhatId = caseobj.Id,
            WhoId=contact.Id,
            //Team__c = team.Id,
            Due_Date__c = Date.Today(),
            Type_Custom__c = 'Contact',
            Sub_Type__c = 'Internal'
        );
        //insert task2;
        taskList.add(task2);
        
          
        Task task3 = new Task(
            RecordTypeId = [SELECT Id, Name FROM RecordType WHERE Name = 'New Development' AND SobjectType = 'Task'].Id,
            Subject = 'Call',
            Priority = '3-General',
            WhatId = caseobj.Id,
            WhoId=contact.Id,
           // Team__c = team.Id,
            Due_Date__c = Date.Today(),
            Type_Custom__c = 'Deliverables',
            Deliverable_Status__c= 'Pending Documents'
        );
        //insert task3;
        taskList.add(task3);
        
        System.debug('task3 : '+task3);
        
        Task task4 = new Task(
            RecordTypeId = [SELECT Id, Name FROM RecordType WHERE Name = 'New Development' AND SobjectType = 'Task'].Id,
            Subject = 'Call',
            Priority = '3-General',
            WhatId = caseobj.Id,
            WhoId=contact.Id,
           // Team__c = team.Id,
            Due_Date__c = Date.Today(),
            Type_Custom__c = 'Deliverables',
            Deliverable_Status__c= 'Submitted'
        );
        
        taskList.add(task4);
        
       
        
        Task task5 = new Task(
            RecordTypeId = [SELECT Id, Name FROM RecordType WHERE Name = 'New Development' AND SobjectType = 'Task'].Id,
            Subject = 'Call',
            Priority = '3-General',
            WhatId = stageAppObj.Id,
            WhoId=contact.Id,
            //Team__c = team.Id,
            Due_Date__c = Date.Today(),
            Type_Custom__c = 'Deliverables',
            Deliverable_Status__c= 'Submitted'
        );
        
        taskList.add(task5);
      
        
        
        insert taskList;
        System.debug('taskList: '+taskList);
        
        
        
        
       
        //  TaskTriggerHandler.SendEmailtorecepient('test@test.com',task2.id,caseobj.id);
        
        System.debug('===EmailInvocations: ' + Limits.getEmailInvocations());
        
         
         //Test update task
         Customer_Service_Team__c team2 = new Customer_Service_Team__c(
            Name = 'Test Team2',
            Email__c = 'test@example.com',
            Task_Assignment_Notification__c = true
        );
        insert team2;
        
        taskList.clear();
        //task.Team__c = team2.Id;
        taskList.add(task);
        
        task3.Deliverable_Status__c= 'Accepted';
        taskList.add(task3);
        
        task4.Deliverable_Status__c= 'Rejected';
        taskList.add(task4);
        
        task5.Deliverable_Status__c= 'Pending Documents';
        taskList.add(task5);
         
        update taskList;
        
      
        System.debug('===EmailInvocations: ' + Limits.getEmailInvocations());
        
        test.stopTest();        
    }
    static testmethod void testInsertAndUpdateForAgedOrders(){
        
        //create unassigned user    
        Profile p = [SELECT Id FROM Profile WHERE Name = 'NBN Customer Connections Team Leader' LIMIT 1];   
        User unassignedUser = TestDataUtility.createTestUser(true, p.Id);
        //insert unassignedUser;
        UserRole r = [Select ID From UserRole Where Name = 'Manager of Customer Connections'];
        user ccuser = [Select Id, UserRoleID From User Where ID=:unassignedUser.ID];
        system.runAs(unassignedUser){
            ccuser.UserRoleId = r.ID;
            update ccuser;
        }
        System.debug('===unassignedUser:' + unassignedUser);
        //Create Custom setting Record
        TaskActivityDateExceptionLightning__c cs = new TaskActivityDateExceptionLightning__c();
        cs.Name = 'NBN Customer Connections Team Leader';
        cs.ProfileName__c = 'NBN Customer Connections Team Leader';
        insert cs;

        //create account
        Account account = new Account(
            Name = 'Test Account',
            RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
            Tier__c = '1',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert account;
        
        Contact contact = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId(),
            AccountId = account.Id,
            Email = 'teste@example.com',
            Preferred_Phone__c = '0288889999'
        );
        insert contact;
        
        Case caseObj = new Case(
            Subject = 'Test Case',
            Description = 'Test Case',
            RecordTypeId = schema.sobjecttype.Case.getrecordtypeinfosbyname().get('Cust Conn CM').getRecordTypeId(),
            Origin = 'Email',
            Status = 'Open',
            Priority = '1-ASAP',
      Loc_Id__c = 'LOC000034794556'
        );
        insert caseObj;
            
        test.startTest(); 
        system.runAs(ccuser){
            //test insert task
            Task task = new Task(
                RecordTypeId = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Task').getRecordTypeId(),
                Subject = 'Call',
                Status = 'Open',
                Priority = '3-General',
                Due_Date__c = Date.Today(),
                Type_Custom__c = 'Contact',
                Sub_Type__c = 'Internal'
            );
            insert task;
            //System.assertEquals([Select id, ActivityDate From Task Where ID=:task.ID].ActivityDate, Date.today());
            Task taskupdate = [Select ID, Status From Task Where ID=: task.ID];
            taskupdate.Status = 'Closed';
            update taskupdate;
            //System.assertEquals([Select id, Completed_Date__c From Task Where ID=:task.ID].Completed_Date__c, Date.today());
        }
        
        test.stopTest();        
    }
    
    static testmethod void testAfterUpdate(){
       Action_Plan__c ap = new Action_Plan__c(Status__c = 'In Progress');
       insert ap;
        Account account = new Account(
            Name = 'Test Account',
            RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Household').getRecordTypeId(),
            Tier__c = '1',
            Type__c = 'RSP',
            Status__c = 'Active'
        );
        insert account;
       Contact Con = new Contact(
            LastName = 'Test Contact',
            RecordTypeId = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('External Contact').getRecordTypeId(),
            AccountId = account.Id,
            Email = 'teste@example.com',
            Preferred_Phone__c = '0288889999'
        );
        insert Con;
         Customer_Service_Team__c team = new Customer_Service_Team__c(
            Name = 'Test Team',
            Email__c = 'test@example.com',
            Task_Assignment_Notification__c = true
        );
        insert team;
        String SeniorOpsEngagementRecTypeId = GlobalCache.getRecordTypeId('Task', GlobalConstants.SENIOR_OPS_ENGAGEMENT_TASK_RECTYPE_NAME);
        Task task6 = new Task(
            RecordTypeId = SeniorOpsEngagementRecTypeId,
            Subject = 'Call',
            Priority = '3-General',
            WhatId = ap.Id,
            WhoId=con.Id,
            Team__c = team.Id,
            Due_Date__c = Date.Today(),
            Status = 'Open'
        );
        
        insert task6;
        update task6;
    } 
}