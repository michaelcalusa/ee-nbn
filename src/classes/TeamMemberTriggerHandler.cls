public class TeamMemberTriggerHandler {

   private boolean isExecuting = false;
    private integer batchSize;
    private List<Team_Member__c > trgOldList = new List<Team_Member__c > ();
    private List<Team_Member__c > trgNewList = new List<Team_Member__c > ();
    private Map<id,Team_Member__c > trgOldMap = new Map<id,Team_Member__c > ();
    private Map<id,Team_Member__c > trgNewMap = new Map<id,Team_Member__c > ();
    private static boolean isAsync;
    // Below 7 boolean variables are used to Prevent recursion
    public static boolean isBeforeInsertFirstRun = true;
    public static boolean isBeforeUpdateFirstRun = true;
    public static boolean isBeforeDeleteFirstRun = true;
    public static boolean isAfterInsertFirstRun = true;
    public static boolean isAfterUpdateFirstRun = true;
    public static boolean isAfterDeleteFirstRun = true;
    public static boolean isAfterUndeleteFirstRun = true;
    /***************************************************************************************************
    Method Name         : NewContactTriggerHandler 
    Method Type         : Constructor
    Version             : 1.0 
    Created Date        : 22-09-2016 
    Function            : used to assign the Trigger values to the Class variables
    Input Parameters    : Trigger Context Variables (6 parameters - 1 boolean, 1 integer, 2 List and 2 Map) 
    Output Parameters   : None
    Description         :  
    Used in             : "Contact Trigger" Trigger
    Modification Log    :
    * Developer                   Date                   Description
    * --------------------------------------------------------------------------------------------------                 
    * SUKUMAR SALLA              22-09-2016                Created
    ****************************************************************************************************/  
    public TeamMemberTriggerHandler (boolean isExecuting, integer batchSize, List<Team_Member__c > trgOldList, List<Team_Member__c > trgNewList, Map<id,Team_Member__c > trgOldMap, Map<id,Team_Member__c > trgNewMap){
        this.isExecuting = isExecuting;
        this.BatchSize = batchSize;
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
        TeamMemberTriggerHandler.isAsync = System.isBatch() || System.isFuture();
    }
    public void OnBeforeInsert(){}
    
    public void OnBeforeUpdate(){         
        if(!HelperUtility.isTriggerMethodExecutionDisabled('CheckFieldUpdates')){
            if(isBeforeUpdateFirstRun){             
                CheckFieldUpdates(trgNewList, trgOldMap);              
                isBeforeUpdateFirstRun = false;
            }
        }
    }
    public void OnBeforeDelete(){}
    public void OnAfterInsert(){}
    public void OnAfterUpdate(){
      if(!HelperUtility.isTriggerMethodExecutionDisabled('CheckFieldUpdates')){
            if(isAfterInsertFirstRun){             
                UnCheckUpdateFlags(trgNewList, trgOldMap);              
                isAfterInsertFirstRun = false;
            }
        }   
    }
    public void OnAfterDelete(){}
    public void OnUndelete(){}
    /*---------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    public void CheckFieldUpdates(List<Team_Member__c> trgNewList, Map<id,Team_Member__c> oldMap){
         // FS_CONTACT_MATRIX_FIELD_TRACKING
         
         map<String, Id> mapRecordType = new map<string, Id>();
         map<string, Team_Member__c> mapContactMatrix = new map<string, Team_Member__c>();
         map<string, string> mapContact = new map<string, string>();
         map<string, string>mapAccount = new map<string, string>();
         map<string, contact> mapFinalContact = new map<string, contact>();
         map<string, Account> mapFinalAccount = new map<string, Account>();
         mapRecordType = HelperUtility.pullAllRecordTypes('Team_Member__c');
    
         for(Team_Member__c tm:  trgNewList){           
            if(/*tm.recordTypeId == string.valueof(mapRecordType.get('ContactMatrix'))  &&*/ tm.Is_This_Contact_Matrix_Field_Update__c){
                mapContactMatrix.put(tm.id,tm);
                mapContact.put(tm.id, tm.Customer_Contact__c);
                mapAccount.put(tm.id, tm.Account__c);
            }
         }
         system.debug('** mapContactMatrix ==>'+ mapContactMatrix);
         system.debug('** mapContact ==>'+ mapContact);
         system.debug('** mapAccount ==>'+ mapAccount);
         
         for(contact c: [select id, Is_This_Contact_Matrix_Field_Update__c, Updated_Fields_Info__c from Contact where id in: mapContact.values() AND Is_This_Contact_Matrix_Field_Update__c =: True]){
             mapFinalContact.put(c.id,c);
         }
         
         system.debug('** mapFinalContact ==>'+ mapFinalContact);
         
         for(Account a: [select id, Is_This_Contact_Matrix_Field_Update__c, Updated_Fields_Info__c from Account where id in: mapAccount.values() AND Is_This_Contact_Matrix_Field_Update__c =: True]){
             mapFinalAccount.put(a.id,a);
         }
         
         system.debug('** mapFinalAccount ==>'+ mapFinalAccount);
         
         List<Schema.FieldSetMember> lstFieldSetFields = new List<Schema.FieldSetMember>();
         string updatedFieldContent;
         set<String> fieldSet = new Set<String>();
         set<String> changedFieldSet  = new Set<String>();
            map<string, string> mapFieldSet = new map<string, string>();
            map<string, string> mapUpdatedField = new map<string, string>();
         if(mapContactMatrix.size()>0){
            lstFieldSetFields = HelperUtility.readFieldSet('FS_CONTACT_MATRIX_FIELD_TRACKING', 'Team_Member__c');
           
            
            
            for(Schema.FieldSetMember fields :lstFieldSetFields){
            fieldSet.add(fields.getFieldPath());
                mapFieldSet.put(fields.getFieldPath(), fields.getLabel());
            }
         }
         
            if(mapFieldSet!=null)
            {
                string updatedFieldContent2;
                for(string tmId:mapContactMatrix.keySet()){
                    Team_Member__c tmObj = mapContactMatrix.get(tmId);
                    
                    account acc;
                    if(mapFinalAccount.containskey(mapAccount.get(tmId))){
                      acc = mapFinalAccount.get(mapAccount.get(tmId));
                      if(updatedFieldContent != null)
                        updatedFieldContent +=  acc.Updated_Fields_Info__c + '\n' ;
                      else
                        updatedFieldContent =  acc.Updated_Fields_Info__c + '\n' ;
                    }
                    
                    contact cont;
                    
                    if(mapFinalContact.containskey(mapContact.get(tmId))){
                     cont = mapFinalContact.get(mapContact.get(tmId));
                     if(updatedFieldContent != null)
                        updatedFieldContent +=  cont.Updated_Fields_Info__c + '\n' ;
                     else
                       updatedFieldContent =  cont.Updated_Fields_Info__c + '\n' ;
                    }
                                       
                    //updatedFieldContent += 'Contact Matrix' +'\n' ;
                    
                    
                    for(string s: mapFieldSet.keyset()){
                        if(tmObj.get(s) != oldMap.get(tmObj.Id).get(s)){
                          changedFieldSet.add(s);//adding fields whose value changed
                          updatedFieldContent2 += mapFieldSet.get(s)+ ' is Changed from ' + oldMap.get(tmObj.Id).get(s) + ' to ' + tmObj.get(s) + '\n';
                        }
                    }
                    
                    if(changedFieldSet.size()>0)
                    {
                       updatedFieldContent += 'Contact Matrix' +'\n' + updatedFieldContent2 ;                        
                    }
                     mapUpdatedField.put(tmId, updatedFieldContent);
                               
                }  
                
                     
            }
         
            if(changedFieldSet.size()>0 || (updatedFieldContent!='' && updatedFieldContent != null)){
                for (Id tmId:mapContactMatrix.keySet()){
                    Team_Member__c tmObj = mapContactMatrix.get(tmId);
                    tmObj.Updated_Fields_Info__c = mapUpdatedField.get(tmId);
                    //accObj.Is_This_Contact_Matrix_Field_Update__c = False;
                }           
            }
    }
    
     public void UnCheckUpdateFlags(List<Team_Member__c> trgNewList, Map<id,Team_Member__c> oldMap){
     
        map<string, string> mapContact = new map<string, string>();
        map<string, string>mapAccount = new map<string, string>();
        map<string, contact> mapFinalContact = new map<string, contact>();
        map<string, account>mapFinalAccount = new map<string, account>();
        list<account> lstAccountToUpdate = new list<account>();
        list<contact> lstContactToUpdate = new list<contact>();
        
        for(Team_Member__c tm:  trgNewList){                    
                //mapContactMatrix.put(tm.id,tm);
                mapContact.put(tm.id, tm.Customer_Contact__c);
                mapAccount.put(tm.id, tm.Account__c);
         }
         
         for(contact c: [select id, Is_This_Contact_Matrix_Field_Update__c, Updated_Fields_Info__c from Contact where id in: mapContact.values() AND Is_This_Contact_Matrix_Field_Update__c =: True]){
             mapFinalContact.put(c.id,c);
         }
         
         for(Account a: [select id, Is_This_Contact_Matrix_Field_Update__c, Updated_Fields_Info__c from Account where id in: mapAccount.values() AND Is_This_Contact_Matrix_Field_Update__c =: True]){
             mapFinalAccount.put(a.id,a);
         }
         
         for(Team_Member__c tm:  trgNewList){
         
         
            if(mapFinalAccount.containskey(mapAccount.get(tm.Id))){
            account a = mapFinalAccount.get(mapAccount.get(tm.Id));
            a.Is_This_Contact_Matrix_Field_Update__c = false;
            a.Updated_Fields_Info__c = '';
            lstAccountToUpdate.add(a);
            }
            
            if(mapFinalContact.containskey(mapContact.get(tm.Id))){
            contact c = mapFinalContact.get(mapContact.get(tm.Id));
            c.Is_This_Contact_Matrix_Field_Update__c = false;
            c.Updated_Fields_Info__c = '';
            lstContactToUpdate.add(c);
            }
          
       }
     
     if(lstContactToUpdate.size()>0)
       update lstContactToUpdate;
     
     if(lstAccountToUpdate.size()>0)
       update lstAccountToUpdate;    
       
}
}