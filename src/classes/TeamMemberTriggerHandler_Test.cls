/*****************************************************************************
Class Name  :  TeamMemberTriggerHandler_Test
Class Type  :  Test Class 
Version     :  1.0 
Created Date:  26/03/2017
Function    :  This class contains unit test scenarios for TeamMemberTriggerHandler apex class
Used in     :  None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* SUKUMAR SALLA             26/03/2017                 Created
******************************************************************************/
@isTest
public class TeamMemberTriggerHandler_Test {
  
  static testMethod void testmethod1(){
  
      // Create Customer Account & Contact data
    List<Account> lstAcc = TestDataUtility.getListOfAccounts(1, false);
    for(Account acc: lstAcc){
         acc.Is_This_Contact_Matrix_Field_Update__c= true;
     }  
    insert lstAcc;
    
    List<Contact> lstCon = TestDataUtility.getListOfContact(1, false);
      
    for(contact con : lstCon){
         con.AccountId = lstAcc[0].id;
         con.Is_This_Contact_Matrix_Field_Update__c = true;
     }  
    insert lstCon;
    
    // Create Company Account & Contact data
    List<Account> lstCompanyAcc = TestDataUtility.getListOfCompanyAccounts(1, false);
    lstCompanyAcc[0].Name = 'nbn';
     insert lstCompanyAcc;
    List<Contact> lstCompanyCon = TestDataUtility.getListOfCompanyContact(1, false);
    
    for(contact con: lstCompanyCon){
       con.AccountId = lstCompanyAcc[0].id;
       con.Status__c= 'Active';
    }
    insert lstCompanyCon;
    
    //Create Team Member data (Contact Matrix)
    List<Team_Member__c> lstTeamMembers = TestDataUtility.insertListOfTeamMembers(1,false,lstAcc[0],lstCon[0],lstCompanyCon[0]);
    insert lstTeamMembers;
    system.debug('***lstTeamMembers ==>'+ lstTeamMembers );
    for(Team_Member__c tm: lstTeamMembers)
    {
       tm.Operating_Hours__c = '8-5';
       tm.Is_This_Contact_Matrix_Field_Update__c=true;
       tm.Updated_Fields_Info__c='8-5';
    }
    
    update lstTeamMembers;
    
  }
}