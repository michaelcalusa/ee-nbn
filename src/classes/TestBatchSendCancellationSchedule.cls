@isTest
public class TestBatchSendCancellationSchedule {
    
    static testMethod void myUnitTest() {
    test.startTest();
        AsyncApexJob aa = new AsyncApexJob();
       
    BatchSendCancellationSchedule nightlyUpdate = new BatchSendCancellationSchedule();
    String schedule = '0 0 23 * * ?';
    system.schedule('Nightly Update', schedule, nightlyUpdate);
        
    test.stopTest();
    }
}