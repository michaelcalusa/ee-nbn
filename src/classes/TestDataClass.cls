public with sharing class TestDataClass {
	public TestDataClass() {
		
	}
	/*method to create a test Product Configuration*/
    public static cscfga__Product_Configuration__c createProdConfig(cscfga__Product_Basket__c  ProdBasket){
       cscfga__Product_Configuration__c  testProdConfig = new cscfga__Product_Configuration__c();
       testProdConfig.cscfga__Product_Basket__c = ProdBasket.id;
       return testProdConfig ;
    }
    
    /*method to create a test product category*/
    public static cscfga__Product_Category__c createProdCateg(){
        cscfga__Product_Category__c testProdCateg = new cscfga__Product_Category__c();        
        return testProdCateg ;        
    }
    
    /*method to create a test product Definition*/
    public static cscfga__Product_Definition__c createProDef(cscfga__Product_Category__c prodCateg){
        cscfga__Product_Definition__c testProDef = new cscfga__Product_Definition__c();
        testProDef.cscfga__Product_Category__c= prodCateg.Id;
        testProDef.cscfga__Description__c= 'testDescription';
        return testProDef ;        
    }
    
    /*method to create a test Attribute Definition*/
    public static cscfga__Attribute_Definition__c createAttrDef(cscfga__Product_Definition__c proDef ){
        cscfga__Attribute_Definition__c testAttrDef = new cscfga__Attribute_Definition__c();
        testAttrDef.cscfga__Product_Definition__c = proDef.Id;
        testAttrDef.Name= 'Option';
        return testAttrDef ;        
    }
    
     /*method to create a test Attribute*/
    public static cscfga__Attribute__c createAttr(cscfga__Product_Configuration__c prodConfig,cscfga__Attribute_Definition__c testAttrdef){
        cscfga__Attribute__c testAttr = new cscfga__Attribute__c();
        testAttr.cscfga__Product_Configuration__c= prodConfig.Id; 
        //testAttr.cscfga__Value__c = testFulfilmentItem.Id;
    //testAttr.Name = 'Fulfilment Item';
        //testAttr.Sales_Order_ID__c = so.Id;
        testAttr.cscfga__Attribute_Definition__c = testAttrDef.Id;
        return testAttr;
    }

	public static Opportunity CreateTestOpportunityData()
	{
		Opportunity objOpp = new Opportunity(Name = 'Test Opp', CloseDate = System.today(),StageName = 'Invoicing');

        //Added to pass the condition where Internal Status should be received if opp stage needs to be set to Invoicing
        objOpp.Approving_Status__c = 'Internal Approval Received';
		insert objOpp;
		return objOpp;
	}
	public static cscfga__Product_Basket__c CreateTestBasketData(String oppId)
	{
		cscfga__Product_Basket__c objBasket = new cscfga__Product_Basket__c(Name = 'Test basket',cscfga__Opportunity__c = oppId);
		insert objBasket;
		return objBasket;
	}

	public Static Account CreateTestAccountData()
    {
        Account objAccount = new Account(Name = 'Test Account, Organization');
        insert objAccount;
        return objAccount;
    }
    public Static Contact CreateTestContactData()
    {
        Contact objContact = new contact( FirstName = 'FirstName' , LastName = 'LastName');
        insert objContact;
        return objContact;
    }
    public Static Contract CreateTestContractData(String accId,String billingContactId)
    {
    	Contract objContract = new Contract(Status='Pending Execution',AccountId = accId,Billing_Contact__c = billingContactId);

        //Added by vish - Dunning Contact Columns are included in pending accounts report
        objContract.Dunning_Contact__c = billingContactId;
        objContract.BillingStreet = '32, Test Street\n32, test Street';
        
        objContract.BillingPostalCode = '8002';
        objContract.BillingCountry = 'Australia';
        objContract.BillingCity = 'Melbourne';
        objContract.BillingState = 'VIC';
        
    	insert objContract;
    	return objContract;
    }
    public Static ContractContactRole CreateTestContractContactRoleData(String contactId,String contrId)
    {
    	ContractContactRole objContractRole = new ContractContactRole(ContactId=contactId,ContractId=contrId,Role='Billing Contact');
    	insert objContractRole;
    	return objContractRole;
    }
    public static void CreateTestCPQSettingsData()
    {
        CPQSettings__c cpqSettings = new CPQSettings__c();
        cpqSettings.Name = 'CPQ Settings';
        cpqSettings.CloudSense_Page_URL_Prefix__c = 'https://nbn--cpqdev--cscfga.cs6.visual.force.com';
        cpqSettings.Conga_Composer_URL__c ='https://composer.congamerge.com?SolMgr=1&sessionId=';
        cpqSettings.Billing_Contact_Conga_Query__c = '';
        cpqSettings.RW_Conga_Template_Group__c = 'RW';
        cpqSettings.Damage_Conga_Template_Group__c = 'Damage';

        insert cpqSettings;
    }
    public Static Case CreateTestCaseData()
    {
        Case objCase = new Case( Origin = 'Migrations' , Status = 'New' , Sub_Status__c= 'Pending Assignment', Subject = 'Test');
        insert objCase;
        return objCase;
    }
}