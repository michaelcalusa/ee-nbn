/*------------------------------------------------------------------------
Author:        Asif Khan
Company:       NBN
Description:   Utility class to transform the sObject into Outbound object

Test Class: OBMessagePayloadUtilitiesTest

History
<Date>      <Authors Name>      <Action>      <Brief Description of Change>
25.03.2018   Asif Khan           Created
--------------------------------------------------------------------------*/

public class TestDataFactory_BSM {
/**
* Test Method to Create Account records
* @Params [Integer numberOfRecords : count of Records to be created], [Boolean insertRecords : Boolean Value to define if record needs to be inserted],
[string recType : Record Type of an Account], [List<string> ABN_Nos :  List of ABN No.s]
*/
    public static Map<Id,Account> createAccTestRecords(integer numberOfRecords,boolean insertRecords, string recType, List<string> ABN_Nos){
        Id recordTypeId = schema.sobjecttype.Account.getRecordTypeInfosByDeveloperName().get(recType).getRecordTypeId();
        string state = (recType == 'Business_End_Customer') ? 'NSW' : '';
        list<Account> accRecs = new list<Account>();
        for(integer counter = 0 ; counter < numberOfRecords ; counter++){
            accRecs.add( new Account ( Name = 'BSMTestAccount ' + counter,  ABN__c = ABN_Nos[counter], Status__c = 'Active', State__c = state, Tier__c ='3', RecordTypeId = recordTypeId));
        }
        if(insertRecords){
            insert accRecs;
        }
        Map<Id, Account> accRecsMap = new Map<Id, Account>(accRecs);
        return accRecsMap;
    }
/**
* Test Method to Create Contact records
* @Params [Integer numberOfRecords : count of Records to be created], [Boolean insertRecords : Boolean Value to define if record needs to be inserted],
[string recType : Record Type of an Contact], 
*/
    
    public static Map<id,Contact> createConTestRecords(integer numberOfRecords,boolean insertRecords, string recType, List<string> ABN){
        Id conrecordTypeId = schema.sobjecttype.Contact.getRecordTypeInfosByDeveloperName().get(recType).getRecordTypeId();
        string accrecordType, accId;
        Map<Id,Account> accRecs = new Map<Id,Account>();
        if(recType == 'Business_End_Customer')
         accrecordType = recType;
        else if(recType == 'Customer_Contact')
             accrecordType = 'Customer';
        else if(recType == 'Partner_Contact')
             accrecordType = 'Partner_Account';
       accRecs = TestDataFactory_BSM.createAccTestRecords(1,true,accrecordType, ABN);
        for (ID idKey : accRecs.keyset()) {

            accId = idKey;
        break;}

        list<Contact> conRecs = new list<Contact>();
        for(integer counter = 0 ; counter < numberOfRecords ; counter++){
            conRecs.add( new Contact ( FirstName = 'BSMTest', LastName = 'Contact ' + counter,  AccountId = accId, Status__c = 'Active', State__c = 'NSW', RecordTypeId = conrecordTypeId));
        }
        if(insertRecords){
            insert conRecs;
        }
        Map<Id, Contact> conRecsMap = new Map<Id, Contact>(conRecs);
        return conRecsMap;
    }    
/**
* Test Method to get staging object configuration 
* @Params [Integer numberOfRecords : count of Records to be created], [Boolean insertRecords : Boolean Value to define if record needs to be inserted],
[string recType : Record Type of an Account], [List<string> ABN_Nos :  List of ABN No.s]
*/
    public static Staging_Object_Config__mdt getMetadataTestRecords(string event, string srcObject, string label){
        Staging_Object_Config__mdt metaRec = new Staging_Object_Config__mdt();
        metaRec = [Select MasterLabel, DeveloperName, Object__c, RecordType_Names__c, MappingType__c, Config_For__c, is_Creation_Allowed__c, is_Modification_Allowed__c, is_Deletion_Allowed__c, Fields_Map__c from Staging_Object_Config__mdt where MappingType__c =: event
                   and Object__c =: srcObject and MasterLabel =: label Limit 1];
        //string fieldsMap = '{\"ABN__c\":\"ABN\"}';
        return metaRec;
    }    
/**
* Test Method to Create InboundStaging records
* @Params [Integer numberOfRecords : count of Records to be created], [Boolean insertRecords : Boolean Value to define if record needs to be inserted],
[string recId : Record Id of a destination object], [string destObject : Destination object, string action : Insert/Update/Delete], [string source : External System like MDM],
[string payload : Payload received from External system, string externalId : External Id of the record]
*/
    
    public static Map<Id,Inbound_Staging__c> createInboundStagingRecords(integer numberOfRecords,boolean insertRecords, string recId, string destObject, string action, string source,
                                                            			string payload, string externalId){
        list<Inbound_Staging__c> inbStagingLst = new list<Inbound_Staging__c>();
        for(integer counter = 0 ; counter < numberOfRecords ; counter++){
            inbStagingLst.add( new Inbound_Staging__c ( SFDC_Record_Id__c = recId,  Object__c = destObject, Event__c = action, Source__c = source, Payload__c = payload, External_Id__c = externalId));
        }
        if(insertRecords){
            insert inbStagingLst;
        }
        Map<Id, Inbound_Staging__c> inbStagingMap = new Map<Id, Inbound_Staging__c>(inbStagingLst);
        return inbStagingMap;
    }     
}