/***************************************************************************************************
Class Name  :  Test_SiteContactTrigger
Class Type  :  Test Class 
Version     : 1.0 
Created Date: 12/01/2017
Function    : This class contains unit test scenarios for SiteContactTrigger
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Naga       12/01/2016                 Created
****************************************************************************************************/
@isTest
public class Test_SiteContactTrigger {
    public static testmethod void updateResidentialAddress(){
             Contact con = new Contact();
             con.LastName='Test';
             con.Email = 'test@test.com';
             insert con;
             
             Site__c site = new Site__c();
             site.Name = '100 Waverton St, Sydney, NSW 2000';
             site.Location_Id__c = 'LOC123456789012';
             site.Site_Address__c = '100 Waverton St, Sydney, NSW 2000';
             site.Technology_Type__c = 'Copper';
             site.Serviceability_Class__c = 10;
             site.Rollout_Type__c = 'Brownfields';
             site.Unit_Number__c = '1';
             site.Level_Number__c = '2015';
             site.Post_Code__c = '2015';
             site.Locality_Name__c = 'Test';
             site.Road_Type_Code__c = 'ST';
             site.Road_Name__c = 'Test';
             site.Road_Number_1__c = 'Test';
             site.Unit_Type_Code__c = 'Test';
             site.Lot_Number__c = '12';
             site.Road_Suffix_Code__c = 'CT';
             //site.Asset_Type__c = 'Test';
             //site.Asset_Number__c = '';
             site.state_Territory_Code__c = 'ACT';
             site.Latitude__c = '-33.77216378';
             site.Longitude__c = '151.08294035';
             site.Premises_Type__c = 'Residential';
             site.recordTypeId =[SELECT Id FROM RecordType where SobjectType='Site__c' and Name='Verified'].Id;
         insert site;
         SiteContactTriggerHandler.isExecuting = true;
         Site_Contacts__c siteCt = new Site_Contacts__c();
              siteCt.Is_Residential_Address__c = true;
              siteCt.Related_Contact__c = con.id;
              siteCt.Related_Site__c = site.id;
              siteCt.Role__c = 'General Public';
          insert siteCt ;
    }
}