public class TopicDynamicCustomPicklist_LC extends VisualEditor.DynamicPickList{
/*------------------------------------------------------------
Author:     Beau Anderson
Company:       Contractor
Description:   Class to get all topics assigned to the current community and can be reused for any custom lightning component to show a picklist of topics in order to set a 
               topic id. Right now (28082017) a custom Apex class must be created to show a dynamic picklist of topics in the component configuration 
               dialog box. This may change in the future so this class may become redundant in which case it can be deleted. Also if the field CurrentNewtork__c
               on the user object is not being used for another purpose it can also be deleted.
Inputs:        None
Test Class:    NotificationTickerControllerTest
History
<Date>      <Author>     <Description>
------------------------------------------------------------*/

    public override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('', '');
        return defaultValue;
    }
    
    public override VisualEditor.DynamicPickListRows getValues(){
        List<Topic> topic = new List<Topic>();
        Id currentUserId = UserInfo.getUserId();
        String networkId = '';
        List<User> lstUser = [Select id,CurrentNetwork__c from User where Id =: currentUserId];
        for(User usr:lstUser){
           networkId = usr.CurrentNetwork__c;
        } 
        topic = [Select Id,Name from Topic where networkid =:networkId];
        VisualEditor.DynamicPickListRows  custPickVals = new VisualEditor.DynamicPickListRows();
        for(Topic top:topic)
        {
          VisualEditor.DataRow pickVal = new VisualEditor.DataRow(string.valueOf(top.name),string.valueOf(top.id));
          custPickVals.addRow(pickVal);
        }
        for(User usr:lstUser){
           usr.CurrentNetwork__c = '';
        }
        update lstUser;
        return custPickVals;
    }
}