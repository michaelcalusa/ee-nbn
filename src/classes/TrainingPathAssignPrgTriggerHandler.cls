public without sharing class TrainingPathAssignPrgTriggerHandler {
   
    
    private List<lmscons__Training_Path_Assignment_Progress__c> trgOldList = new List<lmscons__Training_Path_Assignment_Progress__c> ();
    private List<lmscons__Training_Path_Assignment_Progress__c> trgNewList = new List<lmscons__Training_Path_Assignment_Progress__c> ();
    private Map<id,lmscons__Training_Path_Assignment_Progress__c> trgOldMap = new Map<id,lmscons__Training_Path_Assignment_Progress__c> ();
    private Map<id,lmscons__Training_Path_Assignment_Progress__c> trgNewMap = new Map<id,lmscons__Training_Path_Assignment_Progress__c> ();   
    
    public TrainingPathAssignPrgTriggerHandler(List<lmscons__Training_Path_Assignment_Progress__c> trgOldList, List<lmscons__Training_Path_Assignment_Progress__c> trgNewList, Map<id,lmscons__Training_Path_Assignment_Progress__c> trgOldMap, Map<id,lmscons__Training_Path_Assignment_Progress__c> trgNewMap){
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
    }
    
    public void OnBeforeInsert(){
    system.debug('---OnBeforeInsert---');
        if(!HelperUtility.isTriggerMethodExecutionDisabled('lrCourseStatusUpdate')){
            //lrCourseStatusUpdate(trgNewList);
        }
    }
    
    public void OnBeforeUpdate(){
        system.debug('---OnBeforeUpdate---');
        if(!HelperUtility.isTriggerMethodExecutionDisabled('lrCourseStatusUpdate')){
            lrCourseStatusUpdate(trgNewList);
        }
    }
    
    public void OnAfterInsert(){
        system.debug('---OnAfterInsert---');
        createLearningRecords(trgNewList);
    }
    
    public void OnAfterUpdate(){
       system.debug('---OnAfterUpdate---');
    }
    /* 
        User Story : MSEU-8158
        This method functions for - 
        (a) Qualified courses
        (b) Eligible Communities member
        (c) Create Learning Records 
    */
    public static void createLearningRecords(List<lmscons__Training_Path_Assignment_Progress__c> lstCourseAssignment){
        try{
            system.debug('--- createLearningRecords start ---');
            //set<Id> setTranscriptIds = new set<Id> ();
            Map<Id, String> mapTranscriptToCourse = new Map<Id, String>();
            List<Learning_Record__c> lstLearningRec = new List<Learning_Record__c>();
            List<ID> lstCommIds = new List<ID>();
            
            // Get the Course assigned user ids from Course Assignment
            for(lmscons__Training_Path_Assignment_Progress__c ca : lstCourseAssignment){
                system.debug('--- Certificate Qualification ---' + ca.lmscons__Training_Path__r.Certificate_Qualification__c + '---' + ca.Certificate_Available__c);
                if(ca.Certificate_Available__c){
                    mapTranscriptToCourse.put(ca.lmscons__Transcript__c, ca.Course_Name__c);                
                }
            }
            
            if(!mapTranscriptToCourse.isEmpty()){
                // Get the Course assigned user to get Contact and Account information
                Map<ID, lmscons__Transcript__c> mapUserToTranscript = new Map<ID, lmscons__Transcript__c>([ SELECT Id, lmscons__Trainee__c, lmscons__Trainee__r.contactId, lmscons__Trainee__r.contact.ICT_Contact_Role__c, lmscons__Trainee__r.contact.AccountId FROM lmscons__Transcript__c WHERE Id IN :mapTranscriptToCourse.keySet() ]);
                
                Set<ID> setUserIds = new Set<ID>();
                for(lmscons__Transcript__c transA : mapUserToTranscript.values()){
                    setUserIds.add(transA.lmscons__Trainee__c);
                }
                
                // Verify the Contacts existence in the Community
                List<CFS_Training_Eligibility__mdt> lstEligibility = [ SELECT CommunityID__c FROM CFS_Training_Eligibility__mdt ];
                for(CFS_Training_Eligibility__mdt cfs : lstEligibility){
                    lstCommIds.add(cfs.CommunityID__c);
                }
                
                List<NetworkMember> lstMembers = [ SELECT MemberId, NetworkId FROM NetworkMember Where NetworkId IN: lstCommIds AND MemberId IN: setUserIds ];
                system.debug('--- lstMembers ---' + lstMembers);
                
                if(!lstMembers.isEmpty()){
                    // Create a map to get users count for each community
                    Map<ID, Set<ID>> mapNetworkIdToMembers = new Map<ID, Set<ID>>();
                    for(NetworkMember nm : lstMembers){
                        Set<Id> setID = new Set<Id>();
                        if(mapNetworkIdToMembers.containsKey(nm.NetworkId)){
                            setID = mapNetworkIdToMembers.get(nm.NetworkId);
                            setID.add(nm.MemberId);
                        }
                        else{
                            setID.add(nm.MemberId);
                        }
                        mapNetworkIdToMembers.put(nm.NetworkId, setID);
                    }
                    system.debug('--- mapNetworkIdToMembers ---' + mapNetworkIdToMembers);
                    
                    // Create Learning Records by for each community users
                    for(ID networkID : mapNetworkIdToMembers.keySet()){
                        Set<Id> setMemberIds = mapNetworkIdToMembers.get(networkID);
                        system.debug('--- mapNetworkIdToMembers ---' + mapNetworkIdToMembers);
                        for(lmscons__Training_Path_Assignment_Progress__c ca : lstCourseAssignment){
                            system.debug('--- ca---' + ca.Course_Name__c);
                            if(mapUserToTranscript.containsKey(ca.lmscons__Transcript__c) && ca.Certificate_Available__c){
                                lmscons__Transcript__c lmstrans = mapUserToTranscript.get(ca.lmscons__Transcript__c);
                                system.debug('--- setMemberIds---' + setMemberIds);
                                system.debug('--- user---' + lmstrans.lmscons__Trainee__c);
                                system.debug('--- Contact Role---' + lmstrans.lmscons__Trainee__r.contact.ICT_Contact_Role__c);
                                if(setMemberIds.contains(lmstrans.lmscons__Trainee__c) && lmstrans.lmscons__Trainee__r.contact.ICT_Contact_Role__c.contains('Training User')){
                                    system.debug('--- ca.Course_Name__c---' + ca.Course_Name__c);
                                    Learning_Record__c lr = new Learning_Record__c();
                                    lr.Account__c = lmstrans.lmscons__Trainee__r.contact.AccountId;
                                    lr.Contact__c = lmstrans.lmscons__Trainee__r.contactId;
                                    lr.Course_Name__c = ca.Course_Name__c;
                                    lr.Course_Status__c = 'Not Started';
                                    lr.Course__c = ca.lmscons__Training_Path__c;
                                    
                                    lstLearningRec.add(lr);                 
                                }
                            }
                        }
                    }
                    system.debug('--- lstLearningRec ---' + lstLearningRec);
                    if(lstLearningRec.size() > 0){
                        insert lstLearningRec;
                    }
                }
            }
            system.debug('--- createLearningRecords end ---');
        }catch(Exception ex){
            GlobalUtility.logMessage('Error','TrainingPathAssignPrgTriggerHandler','createLearningRecords','','Mulitiple Learning Records','Mulitiple Learning Records Error','',ex,0);
        }
    }
    
    public static void lrCourseStatusUpdate(List<lmscons__Training_Path_Assignment_Progress__c> trgNewListOfTrainingPathAssgnPrgs){
        try{
            system.debug('--- lrCourseStatusUpdate start ---');
            
            set<Id> setOfTrasriptId = new set<Id> ();
            Set<Learning_Record__c> listOflearningRecordToUpdate = new Set<Learning_Record__c> ();
            
            Set<Id> setOfContactId = new Set<Id> ();
            system.debug('-----trgNewListOfTrainingPathAssgnPrgs--' + trgNewListOfTrainingPathAssgnPrgs);
            for(lmscons__Training_Path_Assignment_Progress__c var : trgNewListOfTrainingPathAssgnPrgs){
                setOfTrasriptId.add(var.lmscons__Transcript__c);
            }
            Map<Id, lmscons__Transcript__c> mapOfTranscript = new Map<Id, lmscons__Transcript__c> ([SELECT Id, lmscons__Trainee__c, lmscons__Trainee__r.Id, lmscons__Trainee__r.contactId FROM lmscons__Transcript__c WHERE Id IN :setOfTrasriptId]);
            for(lmscons__Transcript__c var : mapOfTranscript.values()){
                setOfContactId.add(var.lmscons__Trainee__r.contactId);
            }
            Map<Id, Contact> mapOfContactWithLearingRec = new Map<Id, Contact> ([SELECT Id, (SELECT Id, Course_Status__c, Course__r.Name FROM Learning_Record__r WHERE Course_Status__c != 'Certified'  Order By CreatedDate Desc) FROM Contact WHERE ID IN :setOfContactId]);
            
            for(lmscons__Training_Path_Assignment_Progress__c var : trgNewListOfTrainingPathAssgnPrgs){
                if(!mapOfTranscript.isEmpty() &&
                mapOfTranscript.get(var.lmscons__Transcript__c) <> null &&
                mapOfTranscript.get(var.lmscons__Transcript__c).lmscons__Trainee__r.contactId <> null &&
                !mapOfContactWithLearingRec.isEmpty() &&
                mapOfContactWithLearingRec.get(mapOfTranscript.get(var.lmscons__Transcript__c).lmscons__Trainee__r.contactId) <> null &&
                !mapOfContactWithLearingRec.get(mapOfTranscript.get(var.lmscons__Transcript__c).lmscons__Trainee__r.contactId).Learning_Record__r.IsEmpty()
                ){
                    system.debug('--- learning records --' + mapOfContactWithLearingRec.get(mapOfTranscript.get(var.lmscons__Transcript__c).lmscons__Trainee__r.contactId).Learning_Record__r);
                    for(Learning_Record__c lr : mapOfContactWithLearingRec.get(mapOfTranscript.get(var.lmscons__Transcript__c).lmscons__Trainee__r.contactId).Learning_Record__r){
                        system.debug('--- course name ---' + var.Course_Name__c);
                        if(lr.Course__r.Name == var.Course_Name__c){
                            system.debug('--- course status name ---' + var.lmscons__StatusPicklist__c);
                            if(string.isNotBlank(var.lmscons__StatusPicklist__c) && (var.lmscons__StatusPicklist__c.equalsIgnoreCase('In Progress') || var.lmscons__StatusPicklist__c.equalsIgnoreCase('Completed')))
                                lr.Course_Status__c = var.lmscons__StatusPicklist__c;
                            else if(string.isBlank(var.lmscons__StatusPicklist__c))
                                lr.Course_Status__c = 'Not Started';
                            
                            lr.Course_Name__c = var.Course_Name__c;
                            listOflearningRecordToUpdate.add(lr);
                        }
                    }
                    
                }
            }
            system.debug('--- listOflearningRecordToUpdate ---' + listOflearningRecordToUpdate);
            if(!listOflearningRecordToUpdate.isEmpty()){
                List<Learning_Record__c> listOflearningRecord = new List<Learning_Record__c> ();
                Set<ID> setLRIDs = new Set<ID>();
                for(Learning_Record__c lr : listOflearningRecordToUpdate){
                    if(!setLRIDs.contains(lr.ID)){
                        setLRIDs.add(lr.ID);
                        listOflearningRecord.add(lr);
                    }
                }
                
                system.debug('--- listOflearningRecord ---' + listOflearningRecord);
                UPDATE listOflearningRecord;
            }
            system.debug('--- lrCourseStatusUpdate end ---');
        }catch(Exception ex){
            GlobalUtility.logMessage('Error','TrainingPathAssignPrgTriggerHandler','lrCourseStatusUpdate','','Learning records status sync with CFS','Status Sync Error','',ex,0);
         
        }
    }


}