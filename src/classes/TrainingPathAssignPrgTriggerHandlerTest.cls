@isTest(seeAllData=false)
public class TrainingPathAssignPrgTriggerHandlerTest {
   
    static testmethod void lrCourseStatusUpdateTest() {
        User ownerID = [Select ID From User Where UserRoleId!=NULL LIMIT 1];
        //Create Business Account
        Account businessAccount = new Account();
        businessAccount.OwnerId = ownerID.ID;
        businessAccount.Name = 'Test Business Account';
        businessAccount.RecordTypeId = schema.sobjecttype.Account.getrecordtypeinfosbyname().get('Partner Account').getRecordTypeId();
        businessAccount.Account_Types__c = 'Other';
        insert businessAccount;
        
        //Create Business Contact
        Contact businessContact = new Contact();
        businessContact.AccountID = businessAccount.ID;
        businessContact.LastName = 'Test BS Contact';
        businessContact.Lase_State__c = 'NSW;VIC';
        businessContact.RecordTypeId  = schema.sobjecttype.Contact.getrecordtypeinfosbyname().get('Partner Contact').getRecordTypeId();   
        insert businessContact;
        
        //Id lerarningAssignment = [Select id From lmscons__Transcript_Line__c LIMIT 1].ID;
        Learning_Record__c lr1 = new Learning_Record__c();
        lr1.Account__c = businessAccount.Id;
        lr1.Contact__c = businessContact.Id;
        lr1.Course_Status__c = 'Not Started';
        insert lr1;
        
        Profile portalProfile = [SELECT Id FROM Profile Where Name ='ICT Customer Community Plus User' Limit 1];
        test.startTest();
        User user1 = new User(
        Username = System.now().millisecond() + 'test12345@test.com',
        ContactId = businessContact.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'icttest12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
        insert user1;   
        test.stoptest();     
        lmscons__Transcript__c testTranscript = new lmscons__Transcript__c(lmscons__Trainee__c = user1.ID);
        insert testTranscript;
        lmscons__Training_Path__c testCourse = new lmscons__Training_Path__c(Name = 'TestCourse', Certificate_Qualification__c= true);
        insert testCourse;
        lmscons__Training_Content__c testModule = new lmscons__Training_Content__c(lmscons__Title__c = 'Test Module');
        insert testModule;
        lmscons__Training_Path_Item__c testCourseItem = new lmscons__Training_Path_Item__c(lmscons__Training_Content__c = testModule.ID,
                                                                                          lmscons__Training_Path__c = testCourse.ID);
        insert testCourseItem;
        lmscons__Training_Path_Assignment_Progress__c testCourseAssignment = new lmscons__Training_Path_Assignment_Progress__c(lmscons__Training_Path__c = testCourse.ID,
                                                                                                                              lmscons__Transcript__c = testTranscript.ID);
        insert testCourseAssignment;
        lmscons__Training_Path_Assignment_Progress__c queryCourseAssignment = [Select ID, Learning_Record__c, lmscons__StatusPicklist__c From lmscons__Training_Path_Assignment_Progress__c Where ID=: testCourseAssignment.ID];

        //Assertion for Not Started Learning Record Status Value
        queryCourseAssignment.lmscons__StatusPicklist__c = null;
        update queryCourseAssignment;
        Learning_Record__c queryLearningRecord = [Select id, Course_Status__c From Learning_Record__c Where ID=: lr1.ID];
        
        //Checking Inprogress Status of Learning Record
        queryCourseAssignment.lmscons__StatusPicklist__c = 'In Progress';
        update queryCourseAssignment;
        Learning_Record__c queryLearningRecord1 = [Select id, Course_Status__c From Learning_Record__c Where ID=: lr1.ID];
    }
}