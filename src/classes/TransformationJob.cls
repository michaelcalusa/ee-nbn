public class TransformationJob implements Schedulable
{
	public static void execute(SchedulableContext ctx)
   	{
   		if (CRM_Transformation_Job__c.getinstance('Transformation Job') <> null && CRM_Transformation_Job__c.getinstance('Transformation Job').on_off__c)
   		{
	   		CronTrigger ct = [SELECT id, TimesTriggered,StartTime, NextFireTime,EndTime,State,CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType FROM CronTrigger WHERE Id = :ctx.getTriggerId()];  
		    Extraction_Job__c ej = new Extraction_Job__c();
		    ej.Name = String.valueof(ct.ID);  // Extraction Job ID
		    ej.Start_Time__c = System.now();
		   //ej.End_Time__c = ct.EndTime; 
		    ej.Extraction_Job_Name__c = ct.CronJobDetail.Name+System.now();
		    ej.Status__c = 'Processing'; 
		    ej.RecordTypeId = [Select Id,SobjectType,Name From RecordType WHERE SobjectType ='Extraction_Job__c'  and name = 'Transformation Jobs'].Id;
		    Database.SaveResult sResult = database.insert(ej,false);
         	String ejId = sResult.getId();
		    Database.executeBatch(new AccountTransformation(ejId));
   		}
   	}
}