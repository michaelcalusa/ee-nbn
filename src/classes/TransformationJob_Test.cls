/***************************************************************************************************
Class Name:  TransformationJob_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 31-10-2016
Function    : This class contains unit test scenarios for TransformationJob schedule class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       31-10-2016                Created
****************************************************************************************************/
@isTest
public class TransformationJob_Test {
    static testMethod void TransformationJobScheduleClass_Test(){
        // Custom Setting record creation
        CRM_Transformation_Job__c CRM_Transformation_Job_CS = new CRM_Transformation_Job__c ();
        CRM_Transformation_Job_CS.Name = 'Transformation Job';
        CRM_Transformation_Job_CS.on_off__c = true;
        insert CRM_Transformation_Job_CS;
        // Test the schedule class
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        String jobId = System.schedule('TransformationJobScheduleClass_Test',CRON_EXP, new TransformationJob());
        TransformationJob trnsfm = new TransformationJob();
        Test.stopTest();        
    }
}