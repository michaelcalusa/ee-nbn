/************************************************************************************************************
Version                 : 1.0 
Created Date            : 
Description/Function    :   
Used in                 : 
Modification Log        :
* Developer                   Date                   Description
* -------------------------------------------------------------------------                
* SUKUMAR SALLA             26-04-2018              Created the Class
**************************************************************************************************************/
public class TransitionEngagementTriggerHandler {
    //Class level variables that are commonly used
    private boolean isExecuting = false;
    private integer batchSize;
    private List<Transition_Engagement__c > trgOldList = new List<Transition_Engagement__c > ();
    private List<Transition_Engagement__c > trgNewList = new List<Transition_Engagement__c > ();
    private Map<id,Transition_Engagement__c > trgOldMap = new Map<id,Transition_Engagement__c > ();
    private Map<id,Transition_Engagement__c > trgNewMap = new Map<id,Transition_Engagement__c > ();
    private static boolean isAsync;
    // Below 7 boolean variables are used to Prevent recursion
    public static boolean isBeforeInsertFirstRun = true;
    public static boolean isBeforeUpdateFirstRun = true;
    public static boolean isBeforeDeleteFirstRun = true;
    public static boolean isAfterInsertFirstRun = true;
    public static boolean isAfterUpdateFirstRun = true;
    public static boolean isAfterDeleteFirstRun = true;
    public static boolean isAfterUndeleteFirstRun = true;
    /***************************************************************************************************
    Method Name         : TransitionEngagementTriggerHandler 
    Method Type         : Constructor
    Version             : 1.0 
    Created Date        : 
    Function            : 
    Input Parameters    : 
    Output Parameters   : 
    Description         :  
    Used in             : 
    Modification Log    :
    * Developer                   Date                   Description
    * -----------------------------------------------------------------------------------------------                
    * SUKUMAR SALLA             26-04-2018                Created
    ****************************************************************************************************/  
    public TransitionEngagementTriggerHandler(boolean isExecuting, integer batchSize, List<Transition_Engagement__c> trgOldList, List<Transition_Engagement__c> trgNewList, Map<id,Transition_Engagement__c> trgOldMap, Map<id,Transition_Engagement__c> trgNewMap){
        this.isExecuting = isExecuting;
        this.BatchSize = batchSize;
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
        TransitionEngagementTriggerHandler.isAsync = System.isBatch() || System.isFuture();
    }
    public void OnBeforeInsert(){
         CheckExistingTransitionEngagement(trgNewList);         
    }
    public void OnBeforeUpdate(){
         validateTransitionEngagement(trgNewList, trgNewMap, trgOldMap);
    }
    public void OnBeforeDelete(){}
    public void OnAfterInsert(){
       system.debug('***TransitionEngagement Trigger - After Insert***');
       createMilestoneAsTasks(trgNewList, null);
    }
    public void OnAfterUpdate(){
        updateTaskAssignment(trgNewList, trgNewMap, trgOldMap);
    }
    public void OnAfterDelete(){}
    public void OnUndelete(){}
    /*---------------------------------------------------------------------------------------------------------------------------------------------------*/
    
     // Method description 
    public void  updateTaskAssignment(List<Transition_Engagement__c> trgNewList,  Map<id,Transition_Engagement__c> trgNewMap, Map<id,Transition_Engagement__c> trgOldMap){
        set<string> transitionEngagementsUpdated = new set<string>();
        set<string> setTEMToUpdate = new set<string>();
        system.debug('trgNewList ==>'+ trgNewList);
        system.debug('trgNewMap ==>'+ trgNewMap);
        system.debug('trgOldMap ==>'+ trgOldMap);
        for(Transition_Engagement__c t: trgNewList){
            system.debug('te before ==>'+ trgNewMap.get(t.id).Assigned_To__c);
            system.debug('te after ==>'+ trgOldMap.get(t.id).Assigned_To__c);
            if(trgNewMap.get(t.id).Assigned_To__c!=trgOldMap.get(t.id).Assigned_To__c){
                transitionEngagementsUpdated.add(t.id);
            }
        }
        
        Map<string, List<Transition_Engagement_Milestones__c>> mapMilestones = new Map<string, List<Transition_Engagement_Milestones__c>>();
        for(Transition_Engagement_Milestones__c tem: [SELECT id, Transition_Engagement__C FROM Transition_Engagement_Milestones__c WHERE Transition_Engagement__C IN: transitionEngagementsUpdated]){
            if(!mapMilestones.containskey(tem.Transition_Engagement__c)){
                mapMilestones.put(tem.Transition_Engagement__c, new List<Transition_Engagement_Milestones__c>());
                mapMilestones.get(tem.Transition_Engagement__c).add(tem);
            }
            else
            {
                mapMilestones.get(tem.Transition_Engagement__c).add(tem);
            }            
            setTEMToUpdate.add(tem.id);
        }
        
        Map<string, List<Task>> mapSubTasks = new Map<string, List<Task>>();
        for(Task t: [SELECT id, whatId, Type FROM task WHERE whatId IN: setTEMToUpdate AND Type =: 'Sub Task']){
            if(!mapSubTasks.containskey(t.whatId)){
                mapSubTasks.put(t.whatId, new List<task>());
                mapSubTasks.get(t.whatId).add(t);
            }
            else{
                mapSubTasks.get(t.whatId).add(t);
            }
        }
        
        List<Task> lstTasksTobeUpdate = new List<Task>();
        List<Transition_Engagement_Milestones__c> lstTEMToUpdate = new List<Transition_Engagement_Milestones__c>();
        for(string teId: transitionEngagementsUpdated){     
            Transition_Engagement__c te =  trgNewMap.get(teId);
            List<Transition_Engagement_Milestones__c> lstTEMs = mapMilestones.get(teId);
            for(Transition_Engagement_Milestones__c tem: lstTEMs){
                tem.OwnerId = te.Assigned_To__c;
                lstTEMToUpdate.add(tem);
                List<task> lstTasks = mapSubTasks.get(tem.id);
                for(task st: lstTasks){
                    st.OwnerId = te.Assigned_To__c;
                    lstTasksTobeUpdate.add(st);
                }
            }
        }
        if(lstTEMToUpdate.size()>0){
            update lstTEMToUpdate;
        }
        if(lstTasksTobeUpdate.size()>0) {
           // update lstTasksTobeUpdate;
        }
    }

       
    // Method description
    public void  CheckExistingTransitionEngagement(List<Transition_Engagement__c> trgNewList){
         Map<Id, Transition_Engagement__c> mapOppToTransitionEngagement = new Map<Id, Transition_Engagement__c>();
         set<string> OpportuniteswithTransitionEngagements = new set<string>();
         // prepare the map with oppId and transition engagment record
         for( Transition_Engagement__c te : trgNewList) {
               mapOppToTransitionEngagement.put(te.Opportunity__c, te);      
         }
         // create set with oppIds which have transition engagment associated        
        for(Transition_Engagement__c te : [select Opportunity__c,OpportunityID__c from Transition_Engagement__c where Opportunity__c in: mapOppToTransitionEngagement.keyset()]){
            OpportuniteswithTransitionEngagements.add(te.Opportunity__c);
        }        
        // Validation: Only 1 Transition Engagement is allowed per Opportunity
        for(Transition_Engagement__c te : trgNewList){
            if(OpportuniteswithTransitionEngagements.contains(te.Opportunity__c)){                	
                te.addError(EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_004__c,false);
            }
        }       
    }
    
    
    // Method description
    public void createMilestoneAsTasks(List<Transition_Engagement__c> trgNewList, Map<id,Transition_Engagement__c> trgOldMap){
         List<Transition_Engagement_Milestones__c> lstTransitionEngagementMilestones = new List<Transition_Engagement_Milestones__c>();
         List<Task> listOfTasksToInsert = new List<Task>(); 

         // Query and get the milestone details from custom metadata
         // insert related Transition Engagement milestones
         List<Sub_Tasks__mdt> lstMilestones = [select id, Type__c, Task_Name__c, Related_Object_API_Name__c, Milestone__c
                                             FROM Sub_Tasks__mdt 
                                             WHERE Type__c = 'Milestone' 
                                             AND Related_Object_API_Name__c = 'Transition_Engagement__c' 
                                             ORDER BY Sequence_Number__c DESC];
         String userId = UserInfo.getUserId();
          for( Transition_Engagement__c te : trgNewList) {
             for(Sub_Tasks__mdt st: lstMilestones){ 
                 Transition_Engagement_Milestones__c tem = new Transition_Engagement_Milestones__c();
                 tem.OwnerId = userId;
                 tem.Milestone_Name__c = st.Task_Name__c;
                 tem.Status__c = 'Open';
                 tem.Transition_Engagement__c = te.Id;
                 tem.Milestone_Sequence__c = st.Milestone__c;
                 tem.TEMilestonesUniqueCode__c = (te.OpportunityID__c!=null?te.OpportunityID__c:te.Opportunity__c)+st.Task_Name__c;
                 lstTransitionEngagementMilestones.add(tem);
             }
          }
          if(lstTransitionEngagementMilestones.size()>0){
           Insert lstTransitionEngagementMilestones;
          }
         
        // Query and get the milestone subtasks details from custom metadata
        // insert related milestone subtasks 
        List<Sub_Tasks__mdt> lstSubTasks = [select id, Type__c, Task_Name__c, Related_Object_API_Name__c, Milestone__c, Sub_Task_Name__c 
                                             FROM Sub_Tasks__mdt 
                                             WHERE Type__c = 'Milestone Sub Task' 
                                             AND Related_Object_API_Name__c = 'Transition_Engagement_Milestone__c' 
                                             ORDER BY Sequence_Number__c DESC];
         ID recordTypeId = schema.sobjecttype.Task.getrecordtypeinfosbyname().get('Business Segment E&G').getRecordTypeId();
         for( Transition_Engagement_Milestones__c tme : lstTransitionEngagementMilestones) {
            for(Sub_Tasks__mdt st: lstSubTasks){                               
                Task t = new Task();
                if(st.Milestone__c == tme.Milestone_Sequence__c){
                    t.OwnerId = Label.Unassigned_UserId;
                	t.Subject = st.Sub_Task_Name__c;
                	t.Status = 'Open';
                	t.Priority = 'Normal';
                	t.Type = 'Sub Task';
                	t.WhatId = tme.Id;
                	t.RecordTypeId = recordTypeId; 
                    //EnG_Settings__c.getOrgDefaults().TE_Task_Record_Type_Id__c; //'0125D0000004dGB';                
                	listOfTasksToInsert.add(t);
                }
            }           
        }        
        if(listOfTasksToInsert.size()>0){
           Insert listOfTasksToInsert;
        }
    }  
    
    // This method used to validate Dates on Action Plan based on related tasks
    public void validateTransitionEngagement(List<Transition_Engagement__c> trgNewList, Map<id,Transition_Engagement__c> trgNewMap, Map<id,Transition_Engagement__c> trgOldMap){    
       
        Map<string, List<task>> mapTransitionEngagementAndRelatedTasks = new Map<string, List<task>>();
        Map<string, List<Transition_Engagement_Milestones__c>> mapTransitionEngagementAndRelatedMilestones = new Map<string, List<Transition_Engagement_Milestones__c>>();
        String allErrors = '';
         
        for(Transition_Engagement_Milestones__c tem:[select id, Transition_Engagement__c, Status__c, Start_Date__c, Completed_Date__c from Transition_Engagement_Milestones__c where Transition_Engagement__c in: trgNewMap.keyset()]){ 
            if(!mapTransitionEngagementAndRelatedMilestones.containskey(tem.Transition_Engagement__c)){
                mapTransitionEngagementAndRelatedMilestones.put(tem.Transition_Engagement__c, new List<Transition_Engagement_Milestones__c>());
                mapTransitionEngagementAndRelatedMilestones.get(tem.Transition_Engagement__c).add(tem);
            }
            else{
                mapTransitionEngagementAndRelatedMilestones.get(tem.Transition_Engagement__c).add(tem);
            }           
        }
        
        for(string apid: mapTransitionEngagementAndRelatedMilestones.keyset()){            
            Boolean startDateErrorFlag = false;
            Boolean completedDateErrorFlag = false;
            Boolean transitionEngagementNotCompleted = false;
            Boolean transitionEngagementTaskNotRequired = false;
            Boolean transitionEngagementTaskCompleted = false;
            
            Transition_Engagement__c ap =  trgNewMap.get(apid);
            List<Transition_Engagement_Milestones__c> lstMilestones = mapTransitionEngagementAndRelatedMilestones.get(apid);
            system.debug('**task ==>'+ lstMilestones );
            
             for(Transition_Engagement_Milestones__c t: lstMilestones){ 
                 if(t.Status__c == 'Open'){
                     transitionEngagementNotCompleted = true;
                 }
                 else if(t.Status__c == 'Completed' ){
                     transitionEngagementTaskCompleted = true;
                 }
                 else if(t.Status__c == 'Not required'){
                     transitionEngagementTaskNotRequired = true;
                 }
             }
            //======================= Validations on Transition Engagement ====================================//
            // Engagement Start Date should be set earlier than the Engagement Completed Date
            if(ap.Engagement_Start_Date__c!=null && ap.Engagement_Completed_Date__c!=null && ap.Engagement_Start_Date__c>ap.Engagement_Completed_Date__c /*&& ap.Status__c=='Engagement is completed'*/){
                allErrors = allErrors+'\n\r'+ EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_001__c;
            }
            // Engagement Completed Date cannot be set as Transition Engagement status is not "Engagement is completed"           
            if(ap.Status__c!='Engagement is completed' && ap.Engagement_Completed_Date__c!=null){                
                allErrors = allErrors+'\n\r'+ EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_005__c;
            }
            // Engagement Start Date and Engagement Completed Date should not be null when the status is Engagment is completed
            if(ap.Status__c =='Engagement is completed' && (ap.Engagement_Completed_Date__c==null || ap.Engagement_Start_Date__c==null)){                
                allErrors = allErrors+'\n\r'+ EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_006__c;
            }
            //======================= Validations on Transition Engagement depending on associated Milestones (subtasks) ======================== //
            // Please close all the milestones before Transition Engagement can be Completed
            If(ap.Status__c=='Engagement is completed' && transitionEngagementNotCompleted){
                allErrors = allErrors+'\n\r'+ EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_007__c;
            }
            // Transition Engagement must include at least 1 Milestone
            If(ap.Status__c=='Engagement is completed' && transitionEngagementTaskNotRequired && !transitionEngagementTaskCompleted && !transitionEngagementNotCompleted){
                allErrors = allErrors+'\n\r'+ EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_008__c;
            }
                        
            for(Transition_Engagement_Milestones__c t: lstMilestones){
                // Start Date must be set earlier than any of Sub task Start Date
                if(t.Start_Date__c < ap.Engagement_Start_Date__c){
                  if(!startDateErrorFlag){
                    allErrors = allErrors+'\n\r'+ EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_002__c;
                    startDateErrorFlag = True;
                  }
                }
                // Completed Date must be set later than any of Sub Task End Date
                if(t.Completed_Date__c > ap.Engagement_Completed_Date__c && ap.Status__c == 'Engagement is completed'){
                 if(!completedDateErrorFlag){
                    allErrors = allErrors+'\n\r'+ EnG_Settings__c.getOrgDefaults().TE_Validation_Error_Message_003__c;
                    completedDateErrorFlag = True;
                 }
                }
            }
            if(allErrors!=''){
                    ap.addError(allErrors, false);
            }
        }
    }   
}