@isTest
public class TransitionEngagementTriggerHandler_TEST {
  static testmethod void test_CreateAndUpdateTE(){
      UserRole r = [SELECT Id FROM UserRole WHERE DeveloperName='Manager_Enterprise_Government_Transition_Engagement'];      
      User u = new User(
          ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
          LastName = 'last',
          Email = 'testtransitionuser@test.com',
          Username = 'testtransitionuser@test.com' + System.currentTimeMillis(),
          CompanyName = 'TEST',
          Title = 'title',
          Alias = 'alias',
          TimeZoneSidKey = 'America/Los_Angeles',
          EmailEncodingKey = 'UTF-8',
          LanguageLocaleKey = 'en_US',
          LocaleSidKey = 'en_US',
          UserRoleId = r.Id
      );
      insert u;
      
      Test.startTest();
      User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
      System.runAs (thisUser) {
      List<Account> lstAcc = TestDataUtility.getListOfAccounts(1, true); 
      Opportunity opp = new Opportunity();
      opp.Name = 'Test Opp';
      opp.StageName = 'Qualify';
      opp.AccountId = lstAcc[0].id;
      opp.CloseDate = System.today();        
      insert opp;
      
      Transition_Engagement__c te = new Transition_Engagement__c();
      te.Assigned_To__c = null;
      te.Opportunity__c = opp.id;
      insert te;       
      
      //te.Status__c = 'Engagement on track';
      te.Assigned_To__c = u.id;
      update te;    
      Test.stopTest();
}     
  }
}