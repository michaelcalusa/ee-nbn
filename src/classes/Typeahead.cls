// Controller class for the Force.com Typeahead component
public with sharing class Typeahead {
    
  //Generate SOSL query and fetch records.  
  @RemoteAction
  public static list<sObject> searchRecords( String queryString, String objectName, 
        list<String> fieldNames, String fieldsToSearch, String filterClause, String orderBy, Integer recordLimit ) {
    
        if (queryString == null) return null;
    
        String sQuery = String.escapeSingleQuotes( queryString );
        if (sQuery.length() == 0) return null;
    
        String sObjectName = (objectName == null) ? 'Contact' : String.escapeSingleQuotes( objectName );
    
        String sInFields = 
            (fieldsToSearch == null || fieldsToSearch == '' || fieldsToSearch.toUpperCase() == 'ALL') ? '' : 
                ( ' IN ' + String.escapeSingleQuotes(fieldsToSearch) + ' FIELDS' );
        
        String sFields = (fieldNames == null || fieldNames.isEmpty()) ? 'Id, Name' : 
            String.escapeSingleQuotes( String.join( fieldNames, ', ' ) );  
         
        String sOrder =  (orderBy == null || orderBy == '') ? '' : (' ORDER BY ' + String.escapeSingleQuotes(orderBy));

        
        String sLimit = (recordLimit == null || recordLimit == 0 || recordLimit >= 2000) ? '' : 
            ( ' LIMIT ' + String.valueOf(recordLimit));
        
        // can't escape the filter clause
        String sWhere = (filterClause == null || filterClause == '') ? '' : 
            ( ' WHERE ' + filterClause );
        
        system.debug('TP QS:'+      'FIND \'' + sQuery + '*\'' + sInFields + 
                ' RETURNING ' + sObjectName + '( ' + sFields + sWhere + sOrder + sLimit + ' )');
        
        list<list<sObject>> results = Search.query(
            'FIND \'' + sQuery + '*\'' + sInFields + 
                ' RETURNING ' + sObjectName + '( ' + sFields + sWhere + sOrder + sLimit + ' )'
        );
        
        return results[0];
  }
  
  //Generate SOQL query and fetch records.
  @RemoteAction
  public static list<sObject> searchRecordsWithSOQL( String PrimaryField, String queryString, String objectName, 
        list<String> fieldNames, String fieldsToSearch, String filterClause, String orderBy, Integer recordLimit ) {
        
        
        if (queryString == null) return null;
    
        String sQuery = String.escapeSingleQuotes( queryString );
        if (sQuery.length() == 0) return null;
    
        String sObjectName = (objectName == null) ? 'Contact' : String.escapeSingleQuotes( objectName );
    
        String sInFields = 
            (fieldsToSearch == null || fieldsToSearch == '' || fieldsToSearch.toUpperCase() == 'ALL') ? '' : 
                ( ' IN ' + String.escapeSingleQuotes(fieldsToSearch) + ' FIELDS' );
        
        String sFields = (fieldNames == null || fieldNames.isEmpty()) ? 'Id, Name' : 
            String.escapeSingleQuotes( String.join( fieldNames, ', ' ) );  
        
        String sOrder =  (orderBy == null || orderBy == '') ? '' : (' ORDER BY ' + String.escapeSingleQuotes(orderBy));
                
        String limits = (recordLimit == null || recordLimit == 0 || recordLimit > 10000) ? '2000' : String.valueOf(recordLimit); 
        String soqlQueryString = 'Select '+sFields+ ' FROM '+ sObjectName + ' Where '+ PrimaryField + ' like \'%'+sQuery+'%\' '+ sOrder +' LIMIT '+limits;
        
        system.debug('Type ahead soqlQueryString: '+soqlQueryString);
        List<SObject> results = Database.query(soqlQueryString);
        
        return results;
    }
      

}