/***************************************************************************************************
Class Name  :  TypeaheadComponentController
Test Class Type  :  TypeaheadComponentController_Test 
Version     : 1.0 
Created Date: 11/11/2016
Function    : case and contact Serach typeahesd
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Wipro       11/11/2016                 Created
* Gnana       14/8/2017                 MSEU-3474 - Modified FIND SOSL query logic to search Contact 
										 Text in Name, Email and Phone Fields
****************************************************************************************************/
global class TypeaheadComponentController {
     @RemoteAction
  global static list<sObject> searchRecords( String queryString, String objectName, list<String> fieldNames, String fieldsToSearch, String filterClause, String orderBy, Integer recordLimit ) {
    //case type search from search wizard page
    if(objectName == 'Case'){
          set<string> caseRTSet = new set<string>();
          map<String,Id> mapofCaseRecordTypeNameandId = new Map<String,Id>();
          List<Id> caseRtList = new List<Id>();
        
          for(SW_Case_Search_Types__mdt caseType : [select DeveloperName,MasterLabel from SW_Case_Search_Types__mdt]){
              caseRTSet.add(caseType.MasterLabel);
          }
          
          for(Schema.RecordTypeInfo info : Schema.getGlobalDescribe().get('Case').getDescribe().getRecordTypeInfos()){
              mapofCaseRecordTypeNameandId.put(info.getName(),info.getRecordTypeId());
          }
          
          for(String rtID : mapofCaseRecordTypeNameandId.keySet()){
              if(caseRTSet.contains(rtID)){
                  caseRtList.add(mapofCaseRecordTypeNameandId.get(rtID));
              } 
          } 
          filterClause = ' RecordTypeId IN : caseRtList';
      }
      //Ends here 
    if (queryString == null) return null;

    String sQuery = String.escapeSingleQuotes( queryString );
    if (sQuery.length() == 0) return null;

    String sObjectName = (objectName == null) ? 'Contact' : String.escapeSingleQuotes( objectName );

    String sInFields = 
        (fieldsToSearch == null || fieldsToSearch == '' || fieldsToSearch.toUpperCase() == 'ALL') ? '' : 
            ( ' IN ' + String.escapeSingleQuotes(fieldsToSearch) + ' FIELDS' );
    
    String sFields = (fieldNames == null || fieldNames.isEmpty()) ? 'Id, Name' : 
        String.escapeSingleQuotes( String.join( fieldNames, ', ' ) );  
    
    String sOrder = ' ORDER BY ' + ( (orderBy == null || orderBy == '') ? 'Name' : String.escapeSingleQuotes(orderBy) ); 
    
    String sLimit = (recordLimit == null || recordLimit == 0 || recordLimit >= 2000) ? '' : 
        ( ' LIMIT ' + String.valueOf(recordLimit));
    
    // can't escape the filter clause
    String sWhere = (filterClause == null || filterClause == '') ? '' : 
        ( ' WHERE ' + filterClause );

    //Search the text based on the Object
    list<list<sObject>> results = new list<list<sObject>> ();
    list<sObject> listOfObjects = new list<sObject> ();
    results.add(listOfObjects);
	if(objectName == 'Contact'){
        Set<Id> setOfContactId = new Set<Id> ();
        //Search the text in Contact Name Fields
        list<list<sObject>> NameResult = Search.query(
            'FIND \'' + sQuery + '*\'' + 'IN NAME FIELDS' + 
                ' RETURNING ' + sObjectName + '( ' + sFields + sWhere + sOrder + sLimit + ' )'
        );
        //Search the text in Contact Email Fields
        list<list<sObject>> EmailResult = Search.query(
            'FIND \'' + sQuery + '*\'' + 'IN EMAIL FIELDS' + 
                ' RETURNING ' + sObjectName + '( ' + sFields + sWhere + sOrder + sLimit + ' )'
        );
        //Search the text in Contact Phone Fields
        list<list<sObject>> PhoneResult = Search.query(
            'FIND \'' + sQuery + '*\'' + 'IN PHONE FIELDS' + 
                ' RETURNING ' + sObjectName + '( ' + sFields + sWhere + sOrder + sLimit + ' )'
        );
        //PPLCULTURE-7565_Search the text in EnableId Field
        list<list<sObject>> EnableIdResult = Search.query(
            'FIND \'' + sQuery + '*\'' + 'IN ALL FIELDS' + 
                ' RETURNING ' + sObjectName + '( ' + sFields + sWhere + sOrder + sLimit + ' )'
        );
        //Combine all the results
        if(NameResult <> null && NameResult[0] <> null && !NameResult[0].isEmpty()){
            for(sObject cont :NameResult[0]){
               setOfContactId.add(cont.Id);
            }
            results[0].addAll(NameResult[0]);
        }
        if(EmailResult <> null && EmailResult[0] <> null && !EmailResult[0].isEmpty()){
            for(sObject cont :EmailResult[0]){
                if(!setOfContactId.contains(cont.Id)){
                    results[0].add(cont);
           		}
            }
            
        }
        if(PhoneResult <> null && PhoneResult[0] <> null && !PhoneResult[0].isEmpty()){
            for(sObject cont :PhoneResult[0]){
                if(!setOfContactId.contains(cont.Id)){
                    results[0].add(cont);
           		}
            }
        }
        if(EnableIdResult <> null && EnableIdResult[0] <> null && !EnableIdResult[0].isEmpty()){
            for(sObject cont :EnableIdResult[0]){
                if(!setOfContactId.contains(cont.Id)){
                    results[0].add(cont);
           		}
            }
        }
    }
    else{
        results = Search.query(
            'FIND \'' + sQuery + '*\'' + sInFields + 
                ' RETURNING ' + sObjectName + '( ' + sFields + sWhere + sOrder + sLimit + ' )'
        );
    }

    system.debug('result....'+results[0]);
    return results[0];
  }  

}