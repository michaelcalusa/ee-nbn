/***************************************************************************************************
Class Name:  TypeaheadComponentController_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 31-10-2016
Function    : This class contains unit test scenarios for TypeaheadComponentController apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Hari Kalannagari	       	4-11-2016                Created
****************************************************************************************************/
@isTest
private class TypeaheadComponentController_Test {
    static testMethod void searchRecords_test(){
        list<String> fieldNames = new list<String>();
        fieldNames.add('Id');
        fieldNames.add('CaseNumber');
        fieldNames.add('Status_RecordType__c');
        
        test.startTest();
        TypeaheadComponentController.searchRecords('0000', 'Case', fieldNames, 'ALL', '', 'CaseNumber', 4);
        test.stopTest();
    }
    static testMethod void searchContactRecords_test(){
        list<String> fieldNames = new list<String>();
        List<List<sObject>> results = new List<List<sObject>>();
        List<sObject> searchresults = new List<sObject>();
        fieldNames.add('Name');
        
        test.startTest();
        searchresults = TypeaheadComponentController.searchRecords('Test', 'Contact', fieldNames, 'Name', '', 'Name', 4);
       
    }
}