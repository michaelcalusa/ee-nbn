@isTest
private class Typeahead_Test {
	
	@testSetup static void setupTestData(){
	 	list<Location__c> locations = new list<location__c>();
	 	Integer locCnt;
	 	for(locCnt=1; locCnt <=50; locCnt++){
	 		Location__c loc = new Location__c();
	 		loc.Full_Address__c = 'test address'+locCnt;
	 		loc.NBN_Location_ID__c = 'LOC000000'+locCnt;
	 		loc.State_Territory_Code__c = 'NSW';
	 		loc.Service_Class__c = '1';
	 		loc.Locality_Name__c = 'Brainburn';
	 		loc.Rollout_Region_ID__c = 'NCRH-20';
	 		loc.Distribution_Area_ID__c = 'NCRH-20-7272-HDHDH';
	 		loc.Disconnection_Date__c = date.newinstance(2014,12,25);
	 		locations.add(loc);
	 	}
	 	
	 	for(locCnt=51; locCnt <=100; locCnt++){
	 		Location__c loc = new Location__c();
	 		loc.Full_Address__c = 'test address'+locCnt;
	 		loc.NBN_Location_ID__c = 'LOC000111'+locCnt;
	 		loc.State_Territory_Code__c = 'WA';
	 		loc.Service_Class__c = '3';
	 		loc.Locality_Name__c = 'EAST VICTORIA PARK';
	 		loc.Rollout_Region_ID__c = '6VIC-10';
	 		loc.Distribution_Area_ID__c = '6VIC-10-20';
	 		loc.Disconnection_Date__c = date.newinstance(2015,06,25); // yyyy,mm,dd
	 		locations.add(loc);
	 	}
	 	//Insert locations records
	 	Database.insert(locations);
	 	
	 	//Insert custom setting record
	 	nbn360_Configurations__c config = new nbn360_Configurations__c();
	 	config.Search_Threshold__c = '10000';
	 	//config.Search_Limit_Error_Message__c='Your search has returned more than 10,000 records. Please click export';
	 	config.Search_Table_Page_Size__c = 100;
	 	config.Search_Export_Report_Id__c='abc';
	 	insert config;
	 	
	 }
	
	/*Search based on SOQL Query*/ 
    static testMethod void searchRecordsWithSOQL_Test() {
        String queryString='test'; String objectName = 'Location__c'; 
	    list<String> fieldNames = new list<String>(); String fieldsToSearch='Full_Address__c';
	    String primaryField ='Full_Address__c'; String orderBy='Full_Address__c'; Integer recordLimit = 10000;
	    fieldNames.add('Id');fieldNames.add('Full_Address__c');
	    String filterClause;
	    
	    /*Search based on Full Address*/
        List<sobject> results =  Typeahead.searchRecordsWithSOQL(primaryField, queryString, objectName, fieldNames, fieldsToSearch, filterClause, orderBy, recordLimit);
        system.assertNotEquals(null, results);
        
        
        queryString='LOC000111'; 
        fieldsToSearch='NBN_Location_ID__c'; primaryField ='NBN_Location_ID__c';  orderBy='NBN_Location_ID__c';
        fieldNames.clear();
        fieldNames.add('NBN_Location_ID__c');
        
        /*Search based on NBN_Location_ID__c */
        results.clear();
        results =  Typeahead.searchRecordsWithSOQL(primaryField, queryString, objectName, fieldNames, fieldsToSearch, filterClause, orderBy, recordLimit);
        system.assertEquals(50, results.size());
        
        
    }
    
    /*Search based on SOSL Query*/
    static testMethod void searchRecords_Test() {
        String queryString='test'; String objectName = 'Location__c'; 
	    list<String> fieldNames = new list<String>(); String fieldsToSearch='NAME';
	    String primaryField ='Full_Address__c'; String orderBy='Full_Address__c'; Integer recordLimit = 200;
	    fieldNames.add('Id');
	    fieldNames.add('Full_Address__c');
	    fieldNames.add('NBN_Location_ID__c');
	    String filterClause = 'ID != null ';
	    
	     /*Search based on NBN_Location_ID__c */
        List<sobject> results =  Typeahead.searchRecords(queryString, objectName, fieldNames, fieldsToSearch, filterClause, orderBy, recordLimit);
        system.assertNotEquals(null, results);
    }
}