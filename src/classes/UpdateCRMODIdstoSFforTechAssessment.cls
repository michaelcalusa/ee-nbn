public class UpdateCRMODIdstoSFforTechAssessment {
    @InvocableMethod
    public static void UpdateCRMODIdstoSF(List<Id> ndCRMODIds)
    {
        try{
            List<NewDev_Application__c> listOfNDA = [SELECT SF_Account_ID__c,SF_AP_Contact_ID__c,SF_Contract_Signatory_Contact_ID__c,SF_Billing_Contact_ID__c,
                                                     SF_Applicant_Contact_ID__c,SF_Development_ID__c,SF_Stage_ID__c,CRMoD_Account_ID__c,CRMoD_Applicant_Contact_ID__c,
                                                     CRMoD_Contract_Signatory_Contact_ID__c,CRMoD_Billing_Contact_ID__c,CRMoD_AP_Contact_ID__c,CRMoD_Development_ID__c,
                                                     CRMoD_Stage_ID__c FROM NewDev_Application__c WHERE Id  =: ndCRMODIds];
            string SFAccId,SFApplicantContactID,SFContractSignatoryContactID,SFBillingContactID,SFAPContactID,SFDevelopmentID,CRMoDAccountID,
                CRMoDApplicantContactID,CRMoDContractSignatoryContactID,CRMoDBillingContactID,CRMoDAPContactID,CRMoDDevelopmentID,CRMoDStageID,SFStageID;
            
            
            list<string> CRMContactList = new List<string>();
            list<string> CRMAccList = new List<string>();
            list<string> SFStageId1 = new List<string>();
            list<string> SFDevId1 = new List<string>();
            List<Contact> contactlist = new List<Contact>();
            for(NewDev_Application__c newDevAppFormCRMODIds : listOfNDA){
                SFAccId = newDevAppFormCRMODIds.SF_Account_ID__c;
                SFApplicantContactID = newDevAppFormCRMODIds.SF_AP_Contact_ID__c;
                SFContractSignatoryContactID = newDevAppFormCRMODIds.SF_Contract_Signatory_Contact_ID__c;
                SFBillingContactID =  newDevAppFormCRMODIds.SF_Billing_Contact_ID__c;
                SFAPContactID = newDevAppFormCRMODIds.SF_Applicant_Contact_ID__c;
                SFDevelopmentID = newDevAppFormCRMODIds.SF_Development_ID__c;
                SFStageID = newDevAppFormCRMODIds.SF_Stage_ID__c;
                CRMoDAccountID = newDevAppFormCRMODIds.CRMoD_Account_ID__c;
                CRMoDApplicantContactID = newDevAppFormCRMODIds.CRMoD_Applicant_Contact_ID__c;
                CRMoDContractSignatoryContactID = newDevAppFormCRMODIds.CRMoD_Contract_Signatory_Contact_ID__c;
                CRMoDBillingContactID = newDevAppFormCRMODIds.CRMoD_Billing_Contact_ID__c;
                CRMoDAPContactID =   newDevAppFormCRMODIds.CRMoD_AP_Contact_ID__c;
                CRMoDDevelopmentID = newDevAppFormCRMODIds.CRMoD_Development_ID__c;
                CRMoDStageID = newDevAppFormCRMODIds.CRMoD_Stage_ID__c;
            }
            
            
            if(CRMoDStageID!= null)
            {
                SFStageId1.add(SFStageID);
                List<Stage_Application__c> stageapplicationlist = new List<Stage_Application__c>();
                stageapplicationlist = [SELECT Id,Request_ID__c from Stage_Application__c where Id = :SFStageID limit 1];
                for (Stage_Application__c stage: stageapplicationlist)
                {
                    stage.Request_ID__c = CRMoDStageID;           
                }
                update stageapplicationlist;       
            }
            if(CRMoDDevelopmentID!= null)
            {
                SFDevId1.add(SFDevelopmentID);
                List<Development__c> developmentlist = new List<Development__c>();
                developmentlist = [SELECT Id,Development_ID__c from Development__c where Id = :SFDevelopmentID limit 1];
                for(Development__c development :developmentlist)
                {
                    development.Development_ID__c = CRMoDDevelopmentID;
                }
                 update developmentlist;
            }
            if(CRMoDAccountID!= null)
            {
                CRMAccList.add(SFAccId);
                List<Account> accountlist = new List<Account>();
                accountlist = [SELECT Id,On_Demand_Id__c from Account where Id = :SFAccId limit 1];
                for(Account account :accountlist)
                {
                    account.On_Demand_Id__c = CRMoDAccountID;
                }
                update accountlist;
            }
            
            if(CRMoDApplicantContactID!= null)
            {
                CRMContactList.add(SFApplicantContactID);
                contactlist = [SELECT Id,On_Demand_Id__c from Contact where Id = :SFApplicantContactID limit 1];
                for(Contact contact1 :contactlist)
                {
                    contact1.On_Demand_Id_P2P__c = CRMoDApplicantContactID;
                }
                update contactlist;
                
            }
            
            if(CRMoDContractSignatoryContactID!= null)
            {
                CRMContactList.add(SFContractSignatoryContactID);
                contactlist = [SELECT Id,On_Demand_Id__c from Contact where Id = :SFContractSignatoryContactID limit 1];
                for(Contact contact1 :contactlist)
                {
                    contact1.On_Demand_Id_P2P__c = CRMoDContractSignatoryContactID;
                }
                update contactlist;
            }
            if(CRMoDBillingContactID!= null)
            {
                CRMContactList.add(SFBillingContactID);
                contactlist = [SELECT Id,On_Demand_Id__c from Contact where Id = :SFBillingContactID limit 1];
                for(Contact contact1 :contactlist)
                {
                    contact1.On_Demand_Id_P2P__c = CRMoDBillingContactID;
                }
                update contactlist;
            }
            if(CRMoDAPContactID!= null)
            {
                CRMContactList.add(SFApplicantContactID);
                contactlist = [SELECT Id,On_Demand_Id__c from Contact where Id = :SFAPContactID limit 1];
                for(Contact contact1 :contactlist)
                {
                    contact1.On_Demand_Id_P2P__c = CRMoDAPContactID;
                }
                update contactlist;
            }
            
        }
        catch(Exception e) {
            system.debug('Exception Occured'+e);
        }
        
        
    }
}