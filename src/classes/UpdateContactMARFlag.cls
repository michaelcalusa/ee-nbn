public class UpdateContactMARFlag implements Database.Batchable<Sobject>, Database.Stateful
{
    public String marIntQuery;
    public string bjId;
    public string status;
    public Integer recordCount =0;
    public String exJobId;
    
    public class GeneralException extends Exception
    {            
    }
    public UpdateContactMARFlag(String ejId)
    {
    	 exJobId = ejId;
    	 Batch_Job__c bj = new Batch_Job__c();
    	 bj.Name = 'UpdateContactMARFlag'+System.now(); // Extraction Job ID
         bj.Start_Time__c = System.now();
         bj.Extraction_Job_ID__c = exJobId;
         Database.SaveResult sResult = database.insert(bj,false);
         bjId = sResult.getId();
    }
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        marIntQuery = 'select Id,Registration_ID__c, Contact_Id__c from MAR_Int__c where Transformation_Status__c = \'Ready for Contact Update\''; 
        return Database.getQueryLocator(marIntQuery);            
    }
    public void execute(Database.BatchableContext BC,List<MAR_Int__c> listOfMARInt)
    {        
      	try
        {
            List<Contact> listOfCon = new List<Contact>();
            List<MAR_Contact__c> listOfMARCon = new List<MAR_Contact__c>();
            List<String> marId = new List<String>();
            map <String, Contact> conMap = new map<String, Contact>();
            map <String, MAR_Contact__c> marMap = new map<String, MAR_Contact__c>();
            for(MAR_Int__c marInt : listOfMARInt)
            {
                marId.add(marInt.Id);                
                Contact con = new Contact();
                con.On_Demand_Id_P2P__c = marInt.Contact_Id__c;
                con.MAR__c = true;                
                //listOfCon.add(con);                
                conMap.put(con.On_Demand_Id_P2P__c,con);
                
                MAR_Contact__c marCon = new MAR_Contact__c();
                MAR__c mar = new MAR__c(On_Demand_Id__c = marInt.Registration_ID__c);
                marCon.MAR__r = mar;                
                Contact c = new Contact(On_Demand_Id_P2P__c = marInt.Contact_Id__c);
                marCon.Contact__r = c;                
                marCon.MAR_Contact_Unique__c = marInt.Registration_ID__c + marInt.Contact_Id__c;
                marMap.put(marCon.MAR_Contact_Unique__c, marCon);
            }
            listOfCon =  conMap.values();
            listOfMARCon = marMap.values();
            
            List<Database.UpsertResult> upsertMARResults = Database.Upsert(listOfCon,Contact.Fields.On_Demand_Id_P2P__c,false);
            List<Database.Error> upsertMARError;
            String errorMessage, fieldsAffected;
      		String[] listOfAffectedFields;
            StatusCode sCode;
            List<Error_Logging__c> errMARList = new List<Error_Logging__c>();
            List<MAR_Int__c> updMARIntRes = new List<MAR_Int__c>();
            for(Integer j=0; j<upsertMARResults.size();j++)
            {
                MAR_Int__c updMARInt = new MAR_Int__c();
                if(!upsertMARResults[j].isSuccess())
                {
                    upsertMARError = upsertMARResults[j].getErrors();
                    for(Database.Error er: upsertMARError)
                    {
                        sCode = er.getStatusCode();
                        errorMessage = er.getMessage();
                        listOfAffectedFields = er.getFields();
                    }
                    fieldsAffected = String.join(listOfAffectedFields,',');
                    errMARList.add(new Error_Logging__c(Name='Contact MAR Flag Update',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isSuccess__c=upsertMARResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=marId[j],Schedule_Job__c=bjId));
                    listOfMARInt[j].Transformation_Status__c = 'Error';
                    listOfMARInt[j].Registration_ID__c = marId[j];
                    listOfMARInt[j].Schedule_Job_Transformation__c = bjId;
                    //updMARIntRes.add(updMARInt);
                }
                else
                {
                    listOfMARInt[j].Transformation_Status__c = 'Copied to Base Table';
                    listOfMARInt[j].Registration_ID__c = marId[j];
                    listOfMARInt[j].Schedule_Job_Transformation__c = bjId;
                                      
                }
            }
            Insert errMARList;
            Update listOfMARInt;    
 
            
            List<Database.UpsertResult> upsertMARConResults = Database.upsert(listOfMARCon,MAR_Contact__c.Fields.MAR_Contact_Unique__c,false);
            List<Database.Error> upsertMARConError;
            List<Error_Logging__c> errMARConList = new List<Error_Logging__c>();
            List<MAR_Int__c> updJunIntRes = new List<MAR_Int__c>();
            for(Integer j=0; j<upsertMARConResults.size();j++)
            {
                MAR_Int__c updJunInt = new MAR_Int__c();
                if(!upsertMARConResults[j].isSuccess())
                {
                    upsertMARConError = upsertMARConResults[j].getErrors();
                    for(Database.Error er: upsertMARConError)
                    {
                        sCode = er.getStatusCode();
                        errorMessage = er.getMessage();
                        listOfAffectedFields = er.getFields();
                    }
                    fieldsAffected = String.join(listOfAffectedFields,',');
                    errMARConList.add(new Error_Logging__c(Name='MAR Contact Intersection',Error_Message__c=errorMessage,Fields_Afftected__c=fieldsAffected,isSuccess__c=upsertMARConResults[j].isSuccess(),Status_Code__c=String.valueof(sCode),CRM_Record_Id__c=marId[j],Schedule_Job__c=bjId));
                    updJunInt.Transformation_Status__c = 'Error';
                    updJunInt.Id = marId[j];
                    updJunInt.Schedule_Job_Transformation__c = bjId;
                    updJunIntRes.add(updJunInt);
                }
                else
                {
                    updJunInt.Transformation_Status__c = 'Copied to Base Table';
                    updJunInt.Id = marId[j];
                    updJunInt.Schedule_Job_Transformation__c = bjId;
                    updJunIntRes.add(updJunInt);                    
                }
            }
            system.debug('errMARConList==>'+errMARConList);
            Insert errMARConList;
            Update updJunIntRes;     
            if(errMARConList.isEmpty() && errMARList.isEmpty())      
            {
            	status = 'Completed';
            } 
            else
            {
            	status = 'Error';
            }   
            
            recordCount = recordCount + listOfMARInt.size();          
        }
        catch(Exception e)
        {            
            status = 'Error';
            list<Error_Logging__c> errList = new List<Error_Logging__c>();
            errList.add(new Error_Logging__c(Name='Update Contact MAR Flag Job',Error_Message__c= e.getMessage()+' Line Number: '+e.getLineNumber(),Schedule_Job__c=bjId));
            Insert errList;
        }
    }    
    public void finish(Database.BatchableContext BC)
    {
        System.debug('Batch Job Completed');
        AsyncApexJob marConJob = [SELECT Id, CreatedById, CreatedBy.Name, ApexClassId, MethodName, Status, CreatedDate, CompletedDate,NumberOfErrors, JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        Batch_Job__c bj = [select Id,Name,Status__c,Batch_Job_ID__c,End_Time__c,Last_Record__c from Batch_Job__c where Id =: bjId];
        if(String.isBlank(status))
       	{
       		bj.Status__c= marConJob.Status;
       	}
       	else
       	{
       		 bj.Status__c=status;
       	}
        bj.Record_Count__c= recordCount;
        bj.Batch_Job_ID__c = marConJob.id;
        bj.End_Time__c  = marConJob.CompletedDate;
        upsert bj;
    }        
}