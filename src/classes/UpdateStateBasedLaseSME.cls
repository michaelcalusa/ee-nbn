/*------------------------------------------------------------------------
Author:        Nagalingeswar Reddy Vempalli
Company:       Wipro
Description:   A controller class which allows to update LASE base case when Phase, category, SubCaterogy is null and site is available.
         https://jira.nbnco.net.au/browse/MSEU-851
Test Class:    UpdateStateBasedLaseSME_Test
History
<Date>         <Authors Name>   <Brief Description of Change>
19/07/2017     Nagalingeswar Reddy Vempalli    Created
--------------------------------------------------------------------------*/  
global class UpdateStateBasedLaseSME {
/*------------------------------------------------------------
  Author:        Nagalingeswar Reddy Vempalli
    Company:       Wipro
    Description:   Method calling from Process builder Case Management for BaseCase/SME Report/DP Report to update LASE base case update.
    Inputs:        List of Case Ids
    Outputs:       none
------------------------------------------------------------*/ 
   public static List<case> finalCaseListToUpdate = new List<case>();
    @InvocableMethod(label='Update SME Team when no PCS' description='To be called from Process builder to update SME Team when there is no PCS')
     global static void updateSMETeam(List<id> caseId) {
         Savepoint sp = Database.setSavepoint();
         List<String> baseCaseList = new List<String>();
          //Map<BaseCAse,SMECAse
         map<String,Case> smeCaseMap = new map<String,Case> ();
         
         List<case>  csList = [select Id,CaseNumber,Case_Record_Type_Name__c,Site_State__c,phase__c,Category__c,Sub_Category__c from case where id =: caseId];
         for(Case cs : csList){
             baseCaseList.add(cs.CaseNumber);
         }
         for(Case c : [Select id,IsSMECase__c,Owner_Email__c,BaseCaseNumber__c from case where IsSMECase__c = true and BaseCaseNumber__c =:  baseCaseList]){
             smeCaseMap.put(c.BaseCaseNumber__c,c);
         }
         for(Case cs : csList){
             system.debug('case is LASE......'+cs);
             Case smeCase;
             if(smeCaseMap.containsKey(cs.CaseNumber)){
               smeCase = smeCaseMap.get(cs.CaseNumber);
             }
             if(cs.Case_Record_Type_Name__c == 'LASE_Escalation'){
                 if(String.isEmpty(cs.Phase__c) && cs.Site_State__c=='NSW'){
                      updateBaseCase('LASE NSW/ACT Team',cs,smeCase );
                 }
                 if(String.isEmpty(cs.Phase__c) && cs.Site_State__c=='NT'){
                       updateBaseCase('LASE NT Team',cs,smeCase );
                 }
                 if(String.isEmpty(cs.Phase__c) && cs.Site_State__c=='QLD'){
                       updateBaseCase('LASE QLD Team',cs,smeCase );
                 }
                 if(String.isEmpty(cs.Phase__c) && cs.Site_State__c=='SA'){
                       updateBaseCase('LASE SA Team',cs ,smeCase);
                 }
                 if(String.isEmpty(cs.Phase__c) && cs.Site_State__c=='TAS'){
                       updateBaseCase('LASE TAS Team',cs ,smeCase);
                 }
                 if(String.isEmpty(cs.Phase__c) && cs.Site_State__c=='VIC'){
                       updateBaseCase('LASE VIC Team',cs,smeCase);
                 }
                 if(String.isEmpty(cs.Phase__c) && cs.Site_State__c=='WA'){
                       updateBaseCase('LASE WA Team',cs,smeCase );
                 }
                 if(String.isEmpty(cs.Phase__c) && String.isEmpty(cs.Site_State__c)){
                      updateBaseCase('Default Team',cs,smeCase );
                 }
             }
         }
         try{
             update finalCaseListToUpdate;
         }catch(Exception e){
             Database.rollback(sp);  
             system.debug('e'+e.getMessage());
             GlobalUtility.logMessage('Error','UpdateStateBasedLaseSME','UpdateStateBasedLaseSME','','',e.getMessage(),'',e,0);
         }
         
     }
    public static void updateBaseCase(String smeTeam, Case cs, Case smeCase ){
             cs.SME_Team__c = smeTeam ;
             cs.SME_Assigned_Date__c = System.now();
           cs.SME_Resolution_Reason__c = 'Not Applicable';
           cs.SME_Resolution_Status__c = 'Not Assigned';
             cs.SME_Case__c = smeCase.id;
             cs.SME_Team_Email__c =  cs.Owner_Email__c;
           finalCaseListToUpdate.add(cs);
         }
}