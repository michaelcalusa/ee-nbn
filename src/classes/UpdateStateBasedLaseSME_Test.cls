/***************************************************************************************************
Class Name	: UpdateStateBasedLaseSME_Test
Class Type	: Test Class 
Version     : 1.0 
Created Date: 31/07/2017
Function    : This class contains unit test scenarios for  UpdateStateBasedLaseSME apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Naga       31/07/2017                 Created
****************************************************************************************************/
@IsTest
public class UpdateStateBasedLaseSME_Test {
    /***************************************************************************************************
Method Name	:  testUpdateBaseCase
Method Type	: testmethod
Version     : 1.0 
Created Date: 31/07/2017
Description	:  update base case when  creation.
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Naga      31/07/2017                Created
****************************************************************************************************/
    static testMethod void testUpdateBaseCase() {
        Test.startTest();
        List<Id> csList = new List<Id>();
        Case cs = new Case();
        Default_Queue__c  defaultQueue = new Default_Queue__c();
        defaultQueue.name = '00G28000002HUlI';
        defaultQueue.Active__c = true;
        insert defaultQueue;
        
        Site__c site = new Site__c();
        site.Name = '100 Waverton St, Sydney, NSW 2000';
        site.Location_Id__c = 'LOC123456789012';
        site.Site_Address__c = '100 Waverton St, Sydney, NSW 2000';
        site.Technology_Type__c = 'Copper';
        site.Serviceability_Class__c = 10;
        site.Rollout_Type__c = 'Brownfields';
        site.Unit_Number__c = '1';
        site.Level_Number__c = '2015';
        site.Post_Code__c = '2015';
        site.Locality_Name__c = 'Test';
        site.Road_Type_Code__c = 'ST';
        site.Road_Name__c = 'Test';
        site.Road_Number_1__c = 'Test';
        site.Unit_Type_Code__c = 'Test';
        site.Lot_Number__c = '12';
        site.Road_Suffix_Code__c = 'CT';
        site.state_Territory_Code__c = 'ACT';
        site.Latitude__c = '-33.77216378';
        site.Longitude__c = '151.08294035';
        site.Premises_Type__c = 'Residential';
        site.recordTypeId =[SELECT Id FROM RecordType where SobjectType='Site__c' and Name='Verified'].Id;
        insert site;
        
        String RTId = [SELECT Id FROM RecordType where SobjectType='Case' and Name='LASE Escalation'].Id;
        cs.status = 'Open';
        cs.Priority = '3-General';
        cs.Origin = 'Phone';
        cs.Subject = 'Test subject';
        cs.Description = 'Test Description';
        cs.RecordTypeId = RTId ;
        cs.Escalation_Type__c = 'Objection';
        cs.Site__c = site.id;
        insert cs;
        
        csList.add(cs.Id);
        UpdateStateBasedLaseSME.updateSMETeam(csList);
        Test.stopTest();
    }
}