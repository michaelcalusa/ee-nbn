global class UpdateUserProfile implements Database.Batchable<sObject> {	
	global Id pId; 
	global UpdateUserProfile(){
		pId	= [Select Id from Profile Where Name = :Label.New_Dev_Portal_User_Profile_Name].Id;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		string q;
		string p='New Dev Portal User';
		if (Test.IsRunningTest()){
			q = 'Select Id, ProfileId, Profile.Name, ContactId from User Where ContactId != null and Profile.Name = \''+ String.escapeSingleQuotes(p)+'\' LIMIT 50';
		}else{
			q = 'Select Id, ProfileId, Profile.Name, ContactId from User Where ContactId != null and Profile.Name = \''+ String.escapeSingleQuotes(p)+'\'';
		}
		system.debug('q==='+q);
		return Database.getQueryLocator(q);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<User> userListtoUpdate = new List<User>();
		for (sObject s : scope){
			User u = (User)s;
			u.ProfileId = pId;
			userListtoUpdate.add(u);
		}
		update userListtoUpdate;
	}
	
	global void finish(Database.BatchableContext BC) {}
}