////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*  Name    : UserTriggerHandler
 *  Purpose : This is a class to enroll user as CFS user and assign courses to users
 *  Author  : Nancy Gupta
 *  Date    : 2017-11-08
 *  Version : 1.0
 */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
public without sharing class UserTriggerHandler {
    //method to enroll an SFDC user as CfS consumer user    
    //must be called from a future call since it has a webservice callout  
    @future(callout=true)
    public static void enrollCFSUser(Set<Id> userIdSet){
        try{
            System.debug('DEBUG : userIdSet '+ userIdSet);                
            List<User> userListToEnroll = new List<User>();               
            if(userIdSet != null && userIdSet.size()>0){
                userListToEnroll = [Select Id from User Where Id IN : userIdSet];  
                
                //Enroll SFDC user as CfS consumer user
                
                system.debug('DEBUG : userListToEnroll : '+ userListToEnroll);
                if(userListToEnroll != null && userListToEnroll.size()>0){
                    if(!Test.isRunningTest()){
                        lmscons.ConsumerUserWebservice.CreateConsumerUsers(userListToEnroll, false); 
                    }
                    system.debug('user activation'+userListToEnroll);
                } 
                
                List<User>  userListforAssignmnts  = [Select Id , lmscons__Cornerstone_ID__c , lmscons__CFS_Status__c from User Where Id in :userIdSet]; 
                String courseID = [Select id from lmscons__Training_Path__c where name =: System.Label.ICT_Training_Course limit 1].id;
                for (user u : userListforAssignmnts ) 
                {
                    if ( u.lmscons__Cornerstone_ID__c != null && u.lmscons__CFS_Status__c == 'Active')      
                    {  
                        Map<String, Object> Data = new Map<String, Object>();
                        Data.put('MethodName', 'AssignCourse');
                        Data.put('UsersList', userListToEnroll);
                        Data.put('CourseId', courseID); // Replace the second parameter with SF course Id                    
                        if(!Test.isRunningTest()){
                            lmscons.LMSUtil.InvokePackageMethod(Data); //API call to assign Course to users
                        } 
                        
                    } 
                    
                }  
            }
        }
        Catch(Exception ex){
            GlobalUtility.logMessage('Error','ICT Partner Program Community','enrollCFSUser','','CFS process','CFS process error','',ex,0);   
        }
    }       
}