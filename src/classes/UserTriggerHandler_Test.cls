@isTest
public class UserTriggerHandler_Test {
    public static testmethod void enrollCFSUserTest(){
        try{
            //Create account
            Account portalAccount1 = new Account(
                Name = 'TestAccount'
            );
            Database.insert(portalAccount1);
            system.debug('portalAccount====' +portalAccount1);
            
            //Create contact
            Contact contact1 = new Contact(
                FirstName = 'Test',
                Lastname = 'McTesty',
                AccountId = portalAccount1.Id,
                Email = System.now().millisecond() + 'test@test.com'
            );
            Database.insert(contact1);
          
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs ( thisUser ) {
                UserRole r = new UserRole(name = 'TEST ROLE', PortalType='CustomerPortal', PortalAccountId=portalAccount1.Id);
                //UserRole portalRole = [Select Id From UserRole Where PortalType = 'CustomerPortal' Limit 1];
                Profile profile1 = [Select Id from Profile where name = 'ICT Customer Community Plus User'];
                User uc = new User(
                    UserRoleId = r.Id,
                    ProfileId = profile1.Id,
                    ContactId=contact1.id,
                    Username = System.now().millisecond() + 'test2@test.com',
                    Alias = 'batman',
                    Email='bruce.wayne@wayneenterprises.com',
                    EmailEncodingKey='UTF-8',
                    Firstname='Bruce',
                    Lastname='Wayne',
                    LanguageLocaleKey='en_US',
                    LocaleSidKey='en_US',
                    TimeZoneSidKey='America/Chicago',
                    lmscons__CFS_Status__c= 'Active',
                    CFS_Activation_Date__c = system.today(),
                    lmscons__Cornerstone_ID__c = 'testx7EP0xyrbHLEyejvQngibgOJLTs8o0mtbhVhsB3oL8Y9lP9jV4hJ7jfklp8cK7PyYN2+rCVeP7r6C7uAfQ==' 
                );
                test.startTest();
                
                lmscons__Training_Path__c  testCourse = new lmscons__Training_Path__c();
                testCourse.Name = 'ICT Channel Partner Program';
                testCourse.lmscons__Enable_Certificate__c = true;
                insert testCourse;
                system.debug('testCourse====' +testCourse);
                
                lmscons__Transcript__c testTranscript = new lmscons__Transcript__c ();
                testTranscript.lmscons__Trainee__c = uc.Id;
                testTranscript.lmscons__Trainee_User_Profile__c = 'Appirio Employee';
                insert testTranscript;
                
                lmscons__Training_Path_Assignment_Progress__c testLmscons = new lmscons__Training_Path_Assignment_Progress__c();
                testLmscons.lmscons__Transcript__c = testTranscript.Id;
                testLmscons.lmscons__Training_Path__c = testCourse.Id;
                testLmscons.lmscons__StatusPicklist__c = 'Completed';
                testLmscons.lmscons__Completion_Date__c = Date.today();
                insert testLmscons;
                insert uc;
                update uc;
                test.stopTest();
            }
        }Catch(Exception ex){
            system.debug('---Exception--UserTriggerHandler_Test--enrollCFSUserTest()--' + ex.getMessage());
        }
    }
}