public class Utils_CISResponseBuilder {
    private List<EE_CISLocationResponseParsar.Data> data = new List<EE_CISLocationResponseParsar.Data>();
    private List<EE_CISLocationResponseParsar.Included> included = new List<EE_CISLocationResponseParsar.Included>();

    public class AddressBuilder {
        private String unstructured = 'HILLSIDE NURSING HOME  UNIT 212 3 VIOLET TOWN RD MOUNT HUTTON NSW 2290';
        private String siteName = 'HILLSIDE NURSING HOME';
        private String locationDescriptor ;
        private String levelType = 'F';
        private String levelNumber = '1';
        private String unitType = 'UNIT';
        private String unitNumber = '212';
        private String lotNumber = 'LOT123';
        private String planNumber;
        private String roadNumber1 = '3';
        private String roadNumber2;
        private String roadName = 'VIOLET TOWN';
        private String roadTypeCode = 'RD';
        private String roadSuffixCode;
        private String locality = 'MOUNT HUTTON';
        private String postCode = '2290';
        private String state = 'NSW';
        private EE_CISLocationResponseParsar.ComplexAddress complexAddress;
        private String locationType = 'location';

        public AddressBuilder withUnstructured(String unstructured) {
            this.unstructured = unstructured;
            return this;

        }
        public AddressBuilder withSiteName(String siteName) {
            this.siteName = siteName;
            return this;

        }
        public AddressBuilder withLocationDescriptor(String locationDescriptor) {
            this.locationDescriptor = locationDescriptor;
            return this;

        }
        public AddressBuilder withLevelType(String levelType) {
            this.levelType = levelType;
            return this;

        }
        public AddressBuilder withLevelNumber(String levelNumber) {
            this.levelNumber = levelNumber;
            return this;

        }
        public AddressBuilder withUnitType(String unitType) {
            this.unitType = unitType;
            return this;

        }
        public AddressBuilder withUnitNumber(String unitNumber) {
            this.unitNumber = unitNumber;
            return this;

        }
        public AddressBuilder withLotNumber(String lotNumber) {
            this.lotNumber = lotNumber;
            return this;

        }
        public AddressBuilder withPlanNumber(String planNumber) {
            this.planNumber = planNumber;
            return this;

        }
        public AddressBuilder withRoadNumber1(String roadNumber1) {
            this.roadNumber1 = roadNumber1;
            return this;

        }
        public AddressBuilder withRoadNumber2(String roadNumber2) {
            this.roadNumber2 = roadNumber2;
            return this;

        }
        public AddressBuilder withRoadName(String roadName) {
            this.roadName = roadName;
            return this;

        }
        public AddressBuilder withRoadTypeCode(String roadTypeCode) {
            this.roadTypeCode = roadTypeCode;
            return this;

        }
        public AddressBuilder withRoadSuffixCode(String roadSuffixCode) {
            this.roadSuffixCode = roadSuffixCode;
            return this;

        }
        public AddressBuilder withLocality(String locality) {
            this.locality = locality;
            return this;

        }
        public AddressBuilder withPostCode(String postCode) {
            this.postCode = postCode;
            return this;

        }
        public AddressBuilder withState(String state) {
            this.state = state;
            return this;

        }
        public AddressBuilder withComplexAddress(EE_CISLocationResponseParsar.ComplexAddress complexAddress) {
            this.complexAddress = complexAddress;
            return this;

        }
        public AddressBuilder withType(String type) {
            this.locationType = type;
            return this;

        }

        public EE_CISLocationResponseParsar.Address build() {
            EE_CISLocationResponseParsar.Address address = new EE_CISLocationResponseParsar.Address();
            address.unstructured = this.unstructured ;
            address.siteName = this.siteName ;
            address.locationDescriptor = this.locationDescriptor ;
            address.levelType = this.levelType ;
            address.levelNumber = this.levelNumber ;
            address.unitType = this.unitType ;
            address.unitNumber = this.unitNumber ;
            address.lotNumber = this.lotNumber ;
            address.planNumber = this.planNumber ;
            address.roadNumber1 = this.roadNumber1 ;
            address.roadNumber2 = this.roadNumber2 ;
            address.roadName = this.roadName ;
            address.roadTypeCode = this.roadTypeCode ;
            address.roadSuffixCode = this.roadSuffixCode ;
            address.locality = this.locality ;
            address.postCode = this.postCode ;
            address.state = this.state ;
            address.complexAddress = this.complexAddress ;
            address.type = this.locationType;
            return address;
        }
    }

    public class ComplexAddressBuilder {
        private String siteName = 'Building A';
        private String roadNumber1 = '1';
        private String roadNumber2 = '2';
        private String roadName = 'Park';
        private String roadTypeCode = 'Lane';
        private String roadSuffixCode = '';


        public ComplexAddressBuilder withSiteName(String siteName) {
            this.siteName = siteName;
            return this;
        }
        public ComplexAddressBuilder withRoadNumber1(String roadNumber1) {
            this.roadNumber1 = roadNumber1;
            return this;
        }
        public ComplexAddressBuilder withRoadNumber2(String roadNumber2) {
            this.roadNumber2 = roadNumber2;
            return this;
        }
        public ComplexAddressBuilder withRoadName(String roadName) {
            this.roadName = roadName;
            return this;
        }
        public ComplexAddressBuilder withRoadTypeCode(String roadTypeCode) {
            this.roadTypeCode = roadTypeCode;
            return this;
        }
        public ComplexAddressBuilder withRoadSuffixCode(String roadSuffixCode) {
            this.roadSuffixCode = roadSuffixCode;
            return this;
        }

        public EE_CISLocationResponseParsar.ComplexAddress build() {
            EE_CISLocationResponseParsar.ComplexAddress complexAddress = new EE_CISLocationResponseParsar.ComplexAddress();
            complexAddress.siteName = this.siteName;
            complexAddress.roadNumber1 = this.roadNumber1;
            complexAddress.roadNumber2 = this.roadNumber2;
            complexAddress.roadTypeCode = this.roadTypeCode;
            complexAddress.roadSuffixCode = this.roadSuffixCode;
            return complexAddress;
        }
    }

    public class IncludedBuilder {
        private String type;
        private String id;
        private EE_CISLocationResponseParsar.Attributes attributes;

        public IncludedBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public IncludedBuilder withType(String type) {
            this.type = type;
            return this;
        }

        public IncludedBuilder withAttributes(EE_CISLocationResponseParsar.Attributes attributes) {
            this.attributes = attributes;
            return this;
        }

        public EE_CISLocationResponseParsar.Included build() {
            EE_CISLocationResponseParsar.Included included = new EE_CISLocationResponseParsar.Included();
            included.id = this.id;
            included.type = this.type;
            included.attributes = this.attributes;
            return included;
        }
    }

    public class AttributesBuilder {
        private String dwellingType = 'MDU';
        private String region = 'Urban';
        private String regionValueDefault;
        private String externalTerritory;
        private EE_CISLocationResponseParsar.Address address = new AddressBuilder().build();
        private EE_CISLocationResponseParsar.PrimaryAccessTechnology primaryAccessTechnology = new PrimaryAccessTechnologyBuilder().build();
        private String csaId = 'CSA200000010384';
        private String serviceabilityMessage = null;
        private String serviceabilityDate = null;
        private String serviceabilityClassReason = null;
        private EE_CISLocationResponseParsar.GeoCode geoCode = new GeoCodeBuilder().build();
        private String landUse = 'RESIDENTIAL';
        private String boundaryType;
        private String status;
        private String technologyType;
        private String technologySubType;


        public AttributesBuilder withDwellingType(String dwellingType) {
            this.dwellingType = dwellingType;
            return this;

        }
        public AttributesBuilder withRegion(String region) {
            this.region = region;
            return this;

        }
        public AttributesBuilder withRegionValueDefault(String regionValueDefault) {
            this.regionValueDefault = regionValueDefault;
            return this;

        }
        public AttributesBuilder withExternalTerritory(String externalTerritory) {
            this.externalTerritory = externalTerritory;
            return this;

        }
        public AttributesBuilder withAddress(EE_CISLocationResponseParsar.Address address) {
            this.address = address;
            return this;

        }
        public AttributesBuilder withPrimaryAccessTechnology(EE_CISLocationResponseParsar.PrimaryAccessTechnology primaryAccessTechnology) {
            this.primaryAccessTechnology = primaryAccessTechnology;
            return this;

        }
        public AttributesBuilder withCsaId(String csaId) {
            this.csaId = csaId;
            return this;

        }
        public AttributesBuilder withServiceabilityMessage(String serviceabilityMessage) {
            this.serviceabilityMessage = serviceabilityMessage;
            return this;

        }
        public AttributesBuilder withServiceabilityDate(String serviceabilityDate) {
            this.serviceabilityDate = serviceabilityDate;
            return this;

        }
        public AttributesBuilder withServiceabilityClassReason(String serviceabilityClassReason) {
            this.serviceabilityClassReason = serviceabilityClassReason;
            return this;

        }
        public AttributesBuilder withGeoCode(EE_CISLocationResponseParsar.GeoCode geoCode) {
            this.geoCode = geoCode;
            return this;

        }
        public AttributesBuilder withLandUse(String landUse) {
            this.landUse = landUse;
            return this;

        }
        public AttributesBuilder withBoundaryType(String boundaryType) {
            this.boundaryType = boundaryType;
            return this;

        }
        public AttributesBuilder withStatus(String status) {
            this.status = status;
            return this;

        }
        public AttributesBuilder withTechnologyType(String technologyType) {
            this.technologyType = technologyType;
            return this;

        }
        public AttributesBuilder withTechnologySubType(String technologySubType) {
            this.technologySubType = technologySubType;
            return this;
        }

        public EE_CISLocationResponseParsar.Attributes build() {
            EE_CISLocationResponseParsar.Attributes attributes = new EE_CISLocationResponseParsar.Attributes();
            attributes.dwellingType = this.dwellingType;
            attributes.region = this.region;
            attributes.regionValueDefault = this.regionValueDefault;
            attributes.externalTerritory = this.externalTerritory;
            attributes.address = this.address;
            attributes.primaryAccessTechnology = this.primaryAccessTechnology;
            attributes.csaId = this.csaId;
            attributes.serviceabilityMessage = this.serviceabilityMessage;
            attributes.serviceabilityDate = this.serviceabilityDate;
            attributes.serviceabilityClassReason = this.serviceabilityClassReason;
            attributes.geoCode = this.geoCode;
            attributes.landUse = this.landUse;
            attributes.boundaryType = this.boundaryType;
            attributes.status = this.status;
            attributes.technologyType = this.technologyType;
            attributes.technologySubType = this.technologySubType;
            return attributes;
        }

    }

    public class DataBuilder {
        private String id = 'LOC000000000001';
        private String type = 'location';
        private String boundaryType;
        private EE_CISLocationResponseParsar.Attributes attributes = new AttributesBuilder().build();
        private EE_CISLocationResponseParsar.Relationships relationships;

        public DataBuilder withId(String id) {
            this.id = id;
            return this;

        }
        public DataBuilder withType(String type) {
            this.type = type;
            return this;

        }
        public DataBuilder withBoundaryType(String boundaryType) {
            this.boundaryType = boundaryType;
            return this;

        }
        public DataBuilder withAttributes(EE_CISLocationResponseParsar.Attributes attributes) {
            this.attributes = attributes;
            return this;

        }
        public DataBuilder withRelationships(EE_CISLocationResponseParsar.Relationships relationships) {
            this.relationships = relationships;
            return this;
        }

        public EE_CISLocationResponseParsar.Data build() {
            EE_CISLocationResponseParsar.Data data = new EE_CISLocationResponseParsar.Data();
            data.id = this.id;
            data.type = this.type;
            data.boundaryType = this.boundaryType;
            data.attributes = this.attributes;
            data.relationships = this.relationships;
            return data;
        }
    }

    public class PrimaryAccessTechnologyBuilder {
        private String technologyType = 'Fibre To The Building';
        private String serviceClass = '13';
        private String rolloutType = 'Brownfields';

        public PrimaryAccessTechnologyBuilder withTechnologyType(String technologyType) {
            this.technologyType = technologyType;
            return this;

        }
        public PrimaryAccessTechnologyBuilder withServiceClass(String serviceClass) {
            this.serviceClass = serviceClass;
            return this;

        }
        public PrimaryAccessTechnologyBuilder withRolloutType(String rolloutType) {
            this.rolloutType = rolloutType;
            return this;
        }

        public EE_CISLocationResponseParsar.PrimaryAccessTechnology build() {
            EE_CISLocationResponseParsar.PrimaryAccessTechnology technology = new EE_CISLocationResponseParsar.PrimaryAccessTechnology();
            technology.technologyType = technologyType;
            technology.serviceClass = serviceClass;
            technology.rolloutType = rolloutType;
            return technology;
        }
    }

    public class ContainmentBoundariesBuilder {
        private List<EE_CISLocationResponseParsar.Data> data = new List<EE_CISLocationResponseParsar.Data>();

        public ContainmentBoundariesBuilder withData(EE_CISLocationResponseParsar.Data data) {
            this.data.add(data);
            return this;
        }

        public EE_CISLocationResponseParsar.ContainmentBoundaries build() {
            EE_CISLocationResponseParsar.ContainmentBoundaries containmentBoundaries = new EE_CISLocationResponseParsar.ContainmentBoundaries();
            containmentBoundaries.data = this.data;
            return containmentBoundaries;
        }
    }

    public class GeoCodeBuilder {
        private String latitude = '36.000000';
        private String longitude = '36.000000';
        private String geographicDatum = 'GDA94';
        private String srid = '428';

        public GeoCodeBuilder withLatitude(String latitude) {
            this.latitude = latitude;
            return this;

        }
        public GeoCodeBuilder withLongitude(String longitude) {
            this.longitude = longitude;
            return this;

        }
        public GeoCodeBuilder withGeographicDatum(String geographicDatum) {
            this.geographicDatum = geographicDatum;
            return this;

        }
        public GeoCodeBuilder withSrid(String srid) {
            this.srid = srid;
            return this;

        }

        public EE_CISLocationResponseParsar.GeoCode build() {
            EE_CISLocationResponseParsar.GeoCode geoCode = new EE_CISLocationResponseParsar.GeoCode();
            geoCode.latitude = latitude;
            geoCode.longitude = longitude;
            geoCode.geographicDatum = geographicDatum;
            geoCode.srid = srid;
            return geoCode;
        }
    }

    public class RelationshipsBuilder {
        private EE_CISLocationResponseParsar.ContainmentBoundaries containmentBoundaries;
        private EE_CISLocationResponseParsar.MDU mdu;

        public RelationshipsBuilder withContainmentBoundaries(EE_CISLocationResponseParsar.ContainmentBoundaries containmentBoundaries) {
            this.containmentBoundaries = containmentBoundaries;
            return this;

        }
        public RelationshipsBuilder withMDU(EE_CISLocationResponseParsar.MDU mdu) {
            this.mdu = mdu;
            return this;
        }


        public EE_CISLocationResponseParsar.Relationships build() {
            EE_CISLocationResponseParsar.Relationships relationships = new EE_CISLocationResponseParsar.Relationships();
            relationships.MDU = mdu;
            relationships.containmentBoundaries = containmentBoundaries;
            return relationships;
        }

    }

    public class MDUBuilder {
        private EE_CISLocationResponseParsar.Data data;

        public MDUBuilder withData(EE_CISLocationResponseParsar.Data data) {
            this.data = data;
            return this;
        }

        public EE_CISLocationResponseParsar.MDU build() {
            EE_CISLocationResponseParsar.MDU mdu = new EE_CISLocationResponseParsar.MDU();
            mdu.data = this.data;
            return mdu;
        }
    }


    public Utils_CISResponseBuilder withIncluded(EE_CISLocationResponseParsar.Included included) {
        this.included.add(included);
        return this;
    }

    public Utils_CISResponseBuilder withData(EE_CISLocationResponseParsar.Data data) {
        this.data.add(data);
        return this;
    }

    public EE_CISLocationResponseParsar build() {
        EE_CISLocationResponseParsar cisLocationResponse = new EE_CISLocationResponseParsar();
        cisLocationResponse.data = data;
        cisLocationResponse.included = included;
        return cisLocationResponse;
    }

    public String buildJson() {
        EE_CISLocationResponseParsar cisLocationResponse = new EE_CISLocationResponseParsar();
        cisLocationResponse.data = data;
        cisLocationResponse.included = included;
        String str = JSON.serialize(cisLocationResponse);
        System.debug(str);
        return str;
    }

}