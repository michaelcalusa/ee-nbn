public class Utils_DateTime {
    private static String DEFAULT_FORMAT = 'dd/MM/yyyy';

    public static String format(Date aDate, String format) {
        if (aDate == null) {
            return '';
        }
        return Datetime.newInstance(aDate.year(), aDate.month(), aDate.day()).format(format);
    }

    public static String format(Date aDate) {
        return format(aDate, DEFAULT_FORMAT);
    }

}