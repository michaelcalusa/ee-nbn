@IsTest
public class Utils_DateTimeTest {
    @IsTest
    public static void shouldFormatDateUsingAustralianFormat() {
        Date aDate = Date.newInstance(2019, 05, 20);

        Assert.equals('20/05/2019', Utils_DateTime.format(aDate));
    }

    @IsTest
    public static void shouldFormatDateUsingCustomFormat() {
        Date aDate = Date.newInstance(2019, 05, 20);

        Assert.equals('2019-05-20', Utils_DateTime.format(aDate, 'yyyy-MM-dd'));
    }

    @IsTest
    public static void shouldReturnEmptyWhenDateIsNull() {
        Assert.equals('', Utils_DateTime.format(null));
    }

}