/**
 * A dummy StringBuilder for now.
 * Improve this please.
 */
public class Utils_StringBuilder {
    private String myString;

    public Utils_StringBuilder() {
        this.myString = '';
    }

    public Utils_StringBuilder(String aString) {
        this.myString = aString;
    }

    public Utils_StringBuilder append(String anotherString) {
        myString += anotherString;

        return this;
    }

    public String build() {
        return myString;
    }

}