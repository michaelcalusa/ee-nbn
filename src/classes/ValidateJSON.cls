/*
Class Description
Creator: Dilip Athley (v-dileepathley)
Purpose: This class will be used to validate the json string.
Modifications:
*/
public class ValidateJSON
{
	public static Boolean validate(String source)
	{
		try
		{
			Map<String, Object> deserializedJson;
			deserializedJson = (Map<String, Object>)JSON.deserializeUntyped(source);
		}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}
    
}