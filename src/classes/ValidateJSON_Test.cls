/***************************************************************************************************
Class Name:  ValidateJSON_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 26/10/2015
Function    : 
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Sukumar       26/10/2015                 Created
****************************************************************************************************/
@isTest
private class ValidateJSON_Test {
   
    static String body = '{'+ '\"CustomObjects3\": \"AYCA-3QHQ87\" '+'}';
    
   
    static testMethod void ValidateJSON_SuccessScenario(){   
    Test.startTest();   
    ValidateJSON.validate(body);
    Test.stopTest();
    }
    
    static testMethod void ValidateJSON_FailureScenario(){ 
    body = '{'+ '\"CustomObjects3\": \"AYCA-3QHQ87\" '+',';  
    Test.startTest();   
    ValidateJSON.validate(body);
    Test.stopTest();
    }
}