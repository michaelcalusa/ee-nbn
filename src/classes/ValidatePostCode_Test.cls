/***************************************************************************************************
Class Name:  ValidatePostCode_Test 
Class Type: Test Class 
Version     : 1.0 
Created Date: 31-10-2016
Function    : This class contains unit test scenarios for ValidatePostCode apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Syed Moosa Nazir TN       31-10-2016                Created
****************************************************************************************************/
@isTest
private class ValidatePostCode_Test {
    static testMethod void validatetest(){
        string source='123.46';
        ValidatePostCode.validate(source);
    }
    static testMethod void validatetestcatch(){
        string source='';
        ValidatePostCode.validate(source);
    }
    static testMethod void validatetestTrueCondition(){
        string source='1234';
        ValidatePostCode.validate(source);
    }
}