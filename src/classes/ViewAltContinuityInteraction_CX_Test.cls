@isTest
private class ViewAltContinuityInteraction_CX_Test {
    // Test Data
    @testSetup static void setup() {
        Task objTask = new Task();
        objTask.RecordTypeId = GlobalCache.getRecordTypeId('Task','GCC Interaction');
        objTask.PRI__c = 'PRI003000710422';
        objTask.Reason_for_Call__c = 'Pair Swap';
        objTask.Additional_Comments__c = 'Some Description';
        objTask.Additional_Info__c = 'Some Description';
        objTask.Any_follow_up_action_advised__c = 'Some Description';
        objTask.Bypassed_DPU_port_number__c = '22';
        objTask.Cable_Record_Enquiry_Details__c = 'Some Description';
        objTask.CIU_DPU_ID__c = 'Some Description';
        objTask.C_Pair_Destination__c = '22';
        objTask.C_Pair_Source__c = '22';
        objTask.CPI_ID__c = '';
        objTask.Details_of_temp_fix_provided__c = 'Yes';
        objTask.Has_the_hazard_been_made_safe__c = 'Yes';
        objTask.Hazard_Details__c = 'Some Description';
        objTask.Inventory_Update__c ='Some Description';
        objTask.Lead_in_Pair__c = 'Some Description';
        objTask.M_Pair_Destination__c = '22';
        objTask.M_Pair_Source__c = '22';
        objTask.NAR_SR_Created__c = 'Yes';
        objTask.NAR_SR_number__c = '22';
        objTask.O_Pair_Destination__c = '22';
        objTask.O_Pair_Source__c = '22';
        objTask.Pillar_ID_Destination__c = '22';
        objTask.Pillar_ID_Source__c = '22';
        objTask.Reason_for_CIU_By_pass__c = 'Some Description';
        objTask.Severity_of_the_Hazard__c = 'Some Description';
        objTask.Site_Contact__c = 'Some Description';
        objTask.Source_of_Hazard__c = 'Some Description';
        objTask.Telstra_Reference_number__c = 'Some Description';
        objTask.Telstra_Tech_Contact_number__c = '0400000000';
        objTask.Telstra_Tech_ID__c = 'Some Description';
        objTask.Telstra_Tech_name__c = 'Some Description';
        objTask.X_Pair_Destination__c = '22';
        objTask.X_Pair_Source__c = '22';
        objTask.NBNCorrelationId__c= '0e8cedd0-ad98-11e6-bf2e-47644ada7c0f'; 
        objTask.Network_Inventory_Swap_Status__c =Label.Success;
        objTask.NetworkTrailCoRelationID__c = 'test';
        objTask.TCO_or_MDF__c = 'test';
        objTask.Copper_Lead_In_Cable__c = 'test';
        objTask.Copper_Lead_In_Cable_Pair__c = 'test';
        objTask.DPU_Serial_Number__c = 'test';
        objTask.CIU_DPU_ID__c = 'test';
        objTask.DPU_Port_Number__c = 'test';
        objTask.CIU_Cut_In_Status__c = 'test';
        objTask.Copper_Distribution_Cable__c = 'test';
        objTask.O_Pair__c = 'test';
        objTask.Copper_Main_Cable__c = 'test';
        objTask.M_pair__c = 'test';
        objTask.FTTN_Node__c = 'test';
        objTask.Type_Of_Node__c = 'test';
        objTask.Error_trial__c = 'test';
        objTask.NetworkTrail_Status__c = 'Completed';
        insert objTask;
    }
    
    @isTest static void testMethod1() {
        Test.startTest();
        
        List<String> lstFields = new List<String>(Task.getSobjectType().getDescribe().fields.getMap().keySet());
        Task objTask = Database.query('SELECT ' + String.join(lstFields, ',') + ' FROM Task LIMIT 1');
        
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(objTask); 
        ViewAlternateContinuityInteraction_CX  ext = new ViewAlternateContinuityInteraction_CX(stdController);
        ext.actionUpdate();
        ext.actionRecordInteraction();
        ext.getNetTrailStat();
        ext.getSwapStatus();
        ext.getNetTrailStat();
    }
}