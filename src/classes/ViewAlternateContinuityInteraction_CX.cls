/***************************************************************************************************
Class Name:         ViewAlternateContinuityInteraction_CX
Class Type:         Extension Class 
Version:            1.0 
Created Date:       13 Sept 2017
Function:           This class drives dynamic page elements of an Alternate Continuity interaction record based on the Task object. Will be used in a standard page layout 
* --------------------------------------------------------------------------------------------------                 
* Beau Anderson      09/10/2017      Created - Version 1.0 Refer CUSTSA-5361 for Epic description
****************************************************************************************************/ 
public with sharing class ViewAlternateContinuityInteraction_CX{    
    public Task taskRecord {get; set;}
    public String strReasonForCall {get; set;}
    public String strNetworkInventrySwapStatus {get; set;}
    public String strTrial{get; private set;}  
    public Boolean boolTrialSucc{get; set;} 
    public Boolean boolTrialErr{get; set;} 
    public Boolean boolIsTrial{get; set;}    
    public Boolean boolIsSwap{get; set;} 
    public ApexPages.StandardController sController;
    static final String TASK_SAVE_AND_CLOSE_MESSAGE = 'Thank you for your service ' + UserInfo.getFirstName() + '. We have recorded the following information. If needed please update the interaction.';
    
    public ViewAlternateContinuityInteraction_CX(ApexPages.StandardController sController){
        this.sController = sController;   
        this.taskRecord = (Task)sController.getRecord();
        
        if(String.isBlank(ApexPages.currentPage().getParameters().get('showMsg')) == false){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, TASK_SAVE_AND_CLOSE_MESSAGE));
        }

        
        
        if(taskRecord != null){    
            taskRecord = getTaskRecord();
            strReasonForCall = taskRecord.Reason_for_call__c;
            strNetworkInventrySwapStatus = taskRecord.Network_Inventory_Swap_Status__c;
        }   
        boolIsTrial=true; 
        boolIsSwap=true;
        
        getSwapStatus();
        getNetTrailStat();
    }
    
    private Task getTaskRecord(){
        List<String> lstFields = new List<String>(Task.getSObjectType().getDescribe().fields.getMap().keySet());
        Id taskId = this.taskRecord.Id;
        String qry = 'Select ' + String.join(lstFields,',') + ',Task_Custom__r.Opair__c, Task_Custom__r.MPair__c' +  ' FROM Task WHERE Id = :taskId';
        return Database.query(qry); 
    }
    
    
    
    
     public PageReference actionUpdate() {
         PageReference pg = Page.RecordAlternateContinuityInteraction;
         pg.getParameters().put('Id', taskRecord.Id);
         pg.setRedirect(true);
         return pg;
     }
             
     public PageReference actionRecordInteraction() {
     
         PageReference pg = Page.RecordAlternateContinuityInteraction;
         pg.setRedirect(true);
         return pg;
     }
    public PageReference getSwapStatus() {
        //taskRecord = getTaskRecord();
        if(taskRecord.NBNCorrelationId__c != null && taskRecord.Network_Inventory_Swap_Status__c != null){
            boolIsSwap = false;
        }
        return null;
    }
    
    public PageReference getNetTrailStat() {
        // Network Trail
        if(String.isBlank(taskRecord.NetworkTrail_Status__c) == false){
            List<String> pillarList;
            strTrial = '';
            
            
            if (String.isBlank(taskRecord.NetworkTrailCoRelationID__c) == false){
                strTrial += 'Building MDF: ' + taskRecord.NetworkTrailCoRelationID__c + '<br/>';
            }
            
            if (String.isBlank(taskRecord.TCO_or_MDF__c) == false){
                strTrial += 'TCO or MDF: ' + taskRecord.TCO_or_MDF__c+ '<br/>';
            }
            
            if (String.isBlank(taskRecord.Copper_Lead_In_Cable__c) == false){
                strTrial += 'Copper Lead-In Cable: ' + taskRecord.Copper_Lead_In_Cable__c+ '<br/>';
            }
            if (String.isBlank(taskRecord.Copper_Lead_In_Cable_Pair__c) == false){
                //strTrial += 'Copper Lead-In Cable Pair: ' + taskRecord.Copper_Lead_In_Cable_Pair__c+ '<br/>';
            }
            
            if (String.isBlank(taskRecord.DPU_Serial_Number__c) == false){
                strTrial += 'DPU Serial Number: ' + taskRecord.DPU_Serial_Number__c+ '<br/>';
            }
            if (String.isBlank(taskRecord.CIU_DPU_ID__c) == false){
                //strTrial += 'DPU Name: ' + taskRecord.CIU_DPU_ID__c+ '<br/>';
            }
            if (String.isBlank(taskRecord.DPU_Port_Number__c) == false){
                strTrial += 'DPU Port Number: ' + taskRecord.DPU_Port_Number__c+ '<br/>';
            }
            if (String.isBlank(taskRecord.CIU_Cut_In_Status__c) == false){
                strTrial += 'CIU Cut-In Status: ' + taskRecord.CIU_Cut_In_Status__c+ '<br/>';
            }
            if (String.isBlank(taskRecord.Copper_Distribution_Cable__c) == false){
                strTrial += 'Copper Distribution Cable: ' + taskRecord.Copper_Distribution_Cable__c+ '<br/>';
            }
            
            if (String.isBlank(taskRecord.O_Pair__c) == false){
                strTrial += 'O Pair: ' + taskRecord.O_Pair__c+ '<br/>';
            }
                
            if (String.isBlank(taskRecord.Task_Custom__r.OPair__c) == false){
                pillarList = taskRecord.Task_Custom__r.OPair__c.split(',');
                
                if(pillarList.size()>0){
                    integer i=97;
                    List<integer> iList = new List<integer>{i};
                    for(String s :pillarList){
                        String convertedChar = String.fromCharArray(iList);
                        strTrial += 'O Pair: ' + convertedChar + s + '<br/>';
                        iList.clear();
                        iList.add(i++);
                    }
                }
            }
                
                
            if (String.isBlank(taskRecord.Copper_Main_Cable__c) == false){
                strTrial += 'Copper Main Cable: ' + taskRecord.Copper_Main_Cable__c+ '<br/>';
            }
            
            if (String.isBlank(taskRecord.M_pair__c) == false){
                strTrial += 'M pair: ' + taskRecord.M_pair__c+ '<br/>';
            }
            
            if (String.isBlank(taskRecord.Task_Custom__r.MPair__c) == false){
                strTrial += 'M pair: ' + taskRecord.Task_Custom__r.MPair__c + '<br/>';
            }
            
            if (String.isBlank(taskRecord.FTTN_Node__c) == false){
                strTrial += 'FTTN Node: ' + taskRecord.FTTN_Node__c + '<br/>';
            }
            
            if (String.isBlank(taskRecord.Type_Of_Node__c) == false){
                strTrial += 'Type Of Node: ' + taskRecord.Type_Of_Node__c + '<br/>';
            }
            
            if(String.isBlank(taskRecord.Error_trial__c) == false){
                System.debug('^^^failed'); 
                taskRecord.Error_trial__c = taskRecord.Error_trial__c;
            }
            
            boolIsTrial=false;
            boolTrialSucc=true;                                         
            boolTrialErr=false;
            System.debug('^^^strTrial ' + strTrial);
        }
        
        
        if(taskRecord.NBNCorrelationId__c != null  && taskRecord.Error_trial__c != null){
            taskRecord.Error_trial__c=taskRecord.Error_trial__c;  
            boolIsTrial = false; 
            boolTrialErr = true;
            boolTrialSucc = false;
        }
        return null;
    }
}