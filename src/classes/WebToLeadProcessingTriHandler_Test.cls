@isTest
public class WebToLeadProcessingTriHandler_Test {

    
    @testSetup static void createSetup(){
        List<id> stagingRecordIds = new list<id>();
        
        List<Global_Form_Staging__c> gfslist = new List<Global_Form_Staging__c>();
        Global_Form_Staging__c gfs1 = new Global_Form_Staging__c();
        gfs1.Content_Type__c = 'Test Pre-qualification Record One';
        gfs1.Data__c = '{"layer2Network":true,"newServiceProvider":true,"technologyCategories": ["Fixed Line","Fixed Wireless","Satellite"],"enquiryType":"Business","abn":"86156563741","regName":"NBN CO LIMITED","givenName":"Test Wayne 18","familyName":"Test Wayne 18","phoneNumber":"0488111333","emailAddress":"wayneni@nbnco.com.au","website":"nbnco.co.au","jobTitle":"Mr.","address":{"addressId":"LOC000048823749","formattedAddress":"132 Westbury Cl, Balaclava, VIC","source":"lapi"}}';
        gfs1.Status__c = 'Completed';
        gfs1.Type__c = 'pre-qualification questionnaire'; 
        gfslist.add(gfs1);
        
        insert gfslist;
        
        
    }  
    
    @isTest static void testStagingLeadConversion(){
       List<Global_Form_Staging__c> testRecordOne =  [select Name,Content_Type__c,Data__c,Status__c,Type__c from Global_Form_Staging__c where Content_Type__c = 'Test Pre-qualification Record One' limit 1];
       test.startTest();
       WebToLeadProcessingTriggerHandlerUtil.StagingLeadConversion(testRecordOne);
       test.stopTest();
    }
    
    @isTest static void testStagingLeadConversionTwo(){       
       
       List<Global_Form_Staging__c> gfslist2 = new List<Global_Form_Staging__c>();
       Global_Form_Staging__c gfs2 = new Global_Form_Staging__c();
       gfs2.Content_Type__c = 'Test Pre-qualification Record Two';
       gfs2.Data__c = '{"layer2Network":false,"newServiceProvider":false,"technologyCategories": ["Fixed Line","Fixed Wireless","Satellite"],"enquiryType":"Both","abn":"86156563741","regName":"NBN CO LIMITED","givenName":"Test Wayne 19","familyName":"","phoneNumber":"0488111333","emailAddress":"wayneni@nbnco.com.au","website":"nbnco.co.au","jobTitle":"Mr.","address":{"addressId":"LOC000048823749","formattedAddress":"132 Westbury Cl, Balaclava VIC, Australia","source":"Google"}}';
       gfs2.Status__c = 'Completed';
       gfs2.Type__c = 'pre-qualification questionnaire'; 
       gfslist2.add(gfs2);
       
       insert gfslist2;
        
       test.startTest();
       WebToLeadProcessingTriggerHandlerUtil.StagingLeadConversion(gfslist2);
       test.stopTest();
        
    }
    
    
    

}