public class WebToLeadProcessingTriggerHandlerUtil {
    
    //Class variables
    public static id LeadAccessseekerRecordTypeId = GlobalCache.getRecordTypeId('Lead','Access Seeker');
    public static boolean InsertLeadFlag = true;
        
    //This method is used to convert all the JSON data to actual lead object
    public static void PreQualificationCompletedProcessing(List<Global_form_staging__c> input){               
        List<Global_Form_Staging__c> WebToLeadStagingRecords = input;
               
        system.debug('WebToLeadStagingRecords size is '+ WebToLeadStagingRecords.size());
        if(WebToLeadStagingRecords.size() > 0 ){
            StagingLeadConversion(WebToLeadStagingRecords);
        }
        
    }
    
    //Method to convert actual staging web form object into the leads
    public static void StagingLeadConversion(List<Global_form_staging__c> stagingLeads){
		List<Lead> insertLeads = new List<lead>();
        Set<string> GobalFormStagingNameSet = new Set<string>();
        List<GlobalFormEventTriggerHandlerUtil.GlobalFormErrorLogWrapper> GlobalErrorLogs = new List<GlobalFormEventTriggerHandlerUtil.GlobalFormErrorLogWrapper>();
			        
        for(Global_form_staging__c StagingRecord:stagingLeads){
            
            WebToLeadWrapper StagingLead = (WebToLeadWrapper)JSON.deserialize(StagingRecord.Data__c,WebToLeadWrapper.class);
            
            //Testing purpose only for testers
            //system.debug(StagingLead.givenName);
                        
            //Lead insert logic
            Lead convertedLead = new Lead();

            //testMap.put(StagingRecord.Name,convertedLead);
            GobalFormStagingNameSet.add(StagingRecord.Name);
            
            //Auto populate default fields
            convertedLead.Status = 'Open';
            convertedLead.Global_Form_Staging_Name__c  = 'LEAD'+StagingRecord.Name;  
            //convertedLead.Global_Form_Staging_Id__c = StagingRecord.Id;
            convertedLead.recordtypeid = LeadAccessseekerRecordTypeId;
            
            //Mandatory fields
            system.debug('the lay2Network is '+StagingLead.layer2Network);
            if(StagingLead.layer2Network != null && StagingLead.layer2Network == true){
                convertedLead.Own_or_operate_a_Layer_2_Network__c = 'Yes';
            }else if(StagingLead.layer2Network != null && StagingLead.layer2Network == false){
                convertedLead.Own_or_operate_a_Layer_2_Network__c = 'No';
            }
            
            
            convertedLead.Enquiry_Type__c  = StagingLead.enquiryType;
            if(StagingLead.newServiceProvider!= null && StagingLead.newServiceProvider == true){
                convertedLead.Which_best_describes_your_needs__c = 'Directly purchasing services from nbn and becoming a new nbn Service Provider';
            }else if(StagingLead.newServiceProvider!= null && StagingLead.newServiceProvider == false){
                convertedLead.Which_best_describes_your_needs__c = 'Already have a relationship with an nbn™ wholesale provider and looking to sign the Information Agreement';
            	convertedLead.Status = 'Closed';
                convertedLead.Closure_Reason__c = 'Information Agreement - Closed';                
            }
            
            convertedLead.Wholesale_provider_name__c = stagingLead.wholesaleProviderName;
            convertedLead.ABN__c = StagingLead.abn; 
            convertedLead.Company = StagingLead.regName;
            convertedLead.FirstName = StagingLead.givenName;
            convertedLead.LastName = StagingLead.familyName;
            convertedLead.Phone = StagingLead.phoneNumber;
            convertedLead.Email = StagingLead.emailAddress;                                       
            convertedLead.Title = stagingLead.jobTitle;
            convertedLead.PreQualificationAutomaticEmail__c = true;
            //convertedLead.Technologies_selling_or_interested_in__c = StagingLead.Technologies;
			
			//Fixed Line, Fixed Wireless, Satellite mapping Technology mapping    
            string techPickListValueString = '';
            string techCatePickListValueString = '';
            if(stagingLead.technologyCategories != null && stagingLead.technologyCategories.size()> 0){
                List<String> Technologies = stagingLead.technologyCategories;
                for(string tech:Technologies){
                    techCatePickListValueString = techCatePickListValueString+tech+';';
                    if(tech == 'Fixed Line'){
                        techPickListValueString = techPickListValueString + 'FTTN;FTTC;FTTP;FTTB;HFC;';
                    }else if(tech == 'Fixed Wireless'){
                        techPickListValueString = techPickListValueString+'Fixed Wireless;';
                    }else if(tech == 'Satellite'){
                        techPickListValueString = techPickListValueString+'Skymuster service;';
                    }
                }
                techPickListValueString = techPickListValueString.removeEnd(';');
                convertedLead.Technologies_selling_or_interested_in__c = techPickListValueString;
                convertedLead.Technology_Categories__c = techCatePickListValueString; 
            }
     
                     
            //Address fields
            if(StagingLead.address.formattedAddress != null){
                if(StagingLead.address.formattedAddress.contains(',')){
                    List<String> splitfomrattedAddress = StagingLead.address.formattedAddress.split(','); 
                    if(splitfomrattedAddress.size() >= 3 && StagingLead.address.source == 'LAPI'){                        
                        convertedLead.Street = splitfomrattedAddress[0];
                        convertedLead.City = splitfomrattedAddress[1];
                        convertedLead.State = splitfomrattedAddress[2];
                        
                    }else if(splitfomrattedAddress.size() >= 3 && StagingLead.address.source == 'Google'){                        
                        convertedLead.Street = splitfomrattedAddress[0];
                        //convertedLead.City = splitfomrattedAddress[1];
                        //convertedLead.State = splitfomrattedAddress[2];
                        if(splitfomrattedAddress[1].contains(' ')){
                            List<String> splitSubFormmatedAddress = splitfomrattedAddress[1].split(' ');
                            convertedLead.City = splitSubFormmatedAddress[0];
                        	convertedLead.State = splitSubFormmatedAddress[1];
                        }
                        
                    }else if(splitfomrattedAddress.size() == 2){
                        convertedLead.Street = splitfomrattedAddress[0];
                        convertedLead.City = splitfomrattedAddress[1];
                    }else if(splitfomrattedAddress.size() == 1){
                        convertedLead.Street = splitfomrattedAddress[0];
                    }                
                }else{
                    convertedLead.Street = StagingLead.address.formattedAddress;
                }
                
                convertedLead.Country = 'Australia';                
            }

            
            //optional fields
            convertedLead.Website = StagingLead.website;
            
            if(stagingLead.carrierNumber!= null && stagingLead.carrierNumber.length() > 11){
                convertedLead.Carrier_licence_number__c = stagingLead.carrierNumber.substring(0,11);                
            }else if(stagingLead.carrierNumber != null &&  stagingLead.carrierNumber.length() <= 11){
                convertedLead.Carrier_licence_number__c = stagingLead.carrierNumber; 
            }
            
            //insert convertedLead;
            //convertedLead.setOptions(dmo); 
            insertLeads.add(convertedLead);                                                        
        }
        
        //Option 2 get the mapping between staging name and staging record id
        List<global_form_staging__c> processedStagingRecords = [select id,name from global_form_staging__c where name in: GobalFormStagingNameSet];
        Map<String,id> GlobalStagingRecordNameRecordIdMap = new Map<String,id>();
        for(global_form_staging__c single:processedStagingRecords){
            GlobalStagingRecordNameRecordIdMap.put('LEAD'+single.name,single.id);
        }
		        
        try{   
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule= true;
            dmo.EmailHeader.triggerUserEmail = true;
            dmo.DuplicateRuleHeader.allowSave = true;
            
            List<Database.SaveResult> insertResults = Database.insert(insertLeads,dmo);
            system.debug('Database result is '+insertResults);
            
            for(integer i=0;i < insertResults.size();i++){
                if(insertResults[i].issuccess() == false){
                    lead errorLead = insertLeads[i];
                    GlobalFormEventTriggerHandlerUtil.GlobalFormErrorLogWrapper msg = new GlobalFormEventTriggerHandlerUtil.GlobalFormErrorLogWrapper();
                    msg.debuglevel = 'Error';
                    msg.source = 'FormAPI_WebService';
                    msg.logMessage = string.valueof(insertResults[i].getErrors());
                    msg.sourceFunction = 'Error when inserting the lead reocrds from Website form';
                    msg.referenceId =  null;
                    //msg.referenceInfo = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+errorLead.Global_Form_Staging_Id__c;
                    msg.referenceInfo = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+GlobalStagingRecordNameRecordIdMap.get(errorLead.Global_Form_Staging_Name__c);
                    msg.payload = '';
                    msg.ex = null;
                    msg.timer = 0;
                    msg.FormType = 'Pre-Qualification Questionnaire';
                    GlobalErrorLogs.add(msg); 
                }                
            }

            //insert the Global Form API error logs
            if(GlobalErrorLogs != null && GlobalErrorLogs.size()>0){
                GlobalFormEventTriggerHandlerUtil.logMessage(GlobalErrorLogs);                
            }

            
        }catch(Exception e){
            if(!(test.isRunningTest())){
                GlobalFormEventTriggerHandlerUtil.retryPlatformEventAndLogError('Global Form Event trigger handler general exception thrown', 'StagingLeadConversion() method', e, null);                                        
            }
        } 
        
    }
      
    
    //All wrapper class gose here
    public class WebToLeadWrapper{
        public boolean layer2Network;
        public boolean newServiceProvider;
        public string wholesaleProviderName;
        public List<string> technologyCategories;
        public string enquiryType;
        public string abn;
        public string regName;
        public string givenName;
        public string familyName;
        public string phoneNumber;
        public string emailAddress;  
        public string website;
        public string carrierNumber;
        public string siteAddress;
        public addressWrapper address;
        public string jobTitle;
    }
    
    public class addressWrapper {
        public string formattedAddress;
        public string addressId;
        public string source;
    }
    
}