/***************************************************************************************************
Class Name:         WorkOrderHandler
Class Type:         Trigger handler class
Version:            2.0 
Created Date:       13 December 2017
Function:           Handle all Work Order trigger contexts including triggers from work order object and platform events
Input Parameters:   triggers new, old and platform events
Output Parameters:  None
Description:        To be used as a service class
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Kashyap Murthy    13/12/2017      Created - Version 1.0 
* Rohit Mathur      16/02/2018      Updated - Version 1.1
* Beau Anderson     21/03/2018      Updated - Version 1.2
* Murali Krishna    06/09/2018      Updated - Version 2.0
****************************************************************************************************/
public class WorkOrderHandler implements ITriggerHandler {
    // Allows unit tests (or other code) to disable this trigger for the transaction
    //Set used in querying the log table for updating the processing flags.
    private Static Set<Id> lstRemedyIntegrationLogIds = new Set<Id>();
    private static Integer retryCount = 0;
    public static Boolean TriggerDisabled = false;
    /*
        Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return TriggerDisabled;
    }
    //Start Of Trigger Context Methods
    //Before Insert Trigger Context Method
    public void BeforeInsert(List<SObject> newItems, Map<Id, SObject> newItemMap) {}
    //After Insert Trigger Context Method;
    public void AfterInsert(List<SObject> newItems, Map<Id, SObject> newItemMap) {
        setIncidentValues(newItems,null);
    }
    //Before Update Trigger Context Method;
    public void BeforeUpdate(List<SObject> newItems, List<SObject> oldItems, Map<Id, SObject> newItemMap, Map<Id, SObject> oldItemMap) {}
    //After Update Trigger Context Method;
    public void AfterUpdate(List<SObject> newItems, List<SObject> oldItems, Map<Id, SObject> newItemMap, Map<Id, SObject> oldItemMap) {
        setIncidentValues(newItems,oldItemMap);
    }
    //Before Delete Trigger Context Method;
    public void BeforeDelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    //After Delete Trigger Context Method;
    public void AfterDelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    //After UnDelete Trigger Context Method;
    public void AfterUndelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    //End Of Trigger Context Methods
    //BA- Created due to fact that input from platform event won't be an sObject
    /***************************************************************************************************
    Method Name:        AfterInsertHandlePlatformEvent
    Method Parameters:  List Of Ticket Of Work Platform Events
    Method Return Type: Void
    Method Description :- used for deserializing platform events and updating the work order and incident
                          Management records based on the information received by platform events.
    Version:            1.0 
    Created Date:       12 December 2017
    ***************************************************************************************************/
    public static void AfterInsertHandlePlatformEvent(List<String> newItems) {
        system.debug('>>>> Created' + newItems);
        //Deserialize the Jsons posted to platform events and store it in to integrationWrapper TicketOfWork list.
        Map<String,List<Map<String,Object>>> workRequestRecordsMap = deserializePlatformEvent(newItems);
        system.debug('>>>> Created after deserialize' + workRequestRecordsMap);
        //pass the deserialized platform events json to update Work order method for updating workorder records with latest information received from platform events
        createUpdateWorkRequestRecords(workRequestRecordsMap);      
    }
    /***************************************************************************************************
    Method Name:        setIncidentValues
    Method Parameters:  List Of sObjects
    Method Return Type: Void
    Method Description :- Used for setting and updating Incident Management Record values based on Values of 
                          Work Order Record.
    Version:            1.0 
    Created Date:       06 September 2018
    Update:             11/September/2018 - Added Work Request Record Type Filter
    ***************************************************************************************************/
    public void setIncidentValues(List<SObject> newItems,Map<Id,sObject> oldItemMap) {
        //Variable to store the record types of workorder object.
        map<String, Id> mapOfWorkOrderRecordType = new map<string, Id>();
        //Call pullAllRecordTpes Method of HelperUtility class to get all recordtypes of work order object.
        mapOfWorkOrderRecordType = HelperUtility.pullAllRecordTypes('WorkOrder');
        //Declare variable to hold work request record type id of work order object. 
        Id workRequestRecordTypeId = mapOfWorkOrderRecordType.get('Work Request');
        //variable to hold the list of locationids which is used for deleting the nbnintegration cache records.
        List<String> lstLocationIds = new List<String>();
        //oldItem Record
        WorkOrder oldItemRecord;
        //List to Store Incidents Records That needs to be updated.
        List<Id> lstIncidentIds = new List<Id>();
        //Map to hold the appointment key and its related workrequest record.
        Map<String,WorkOrder> appointmentWorkRequestMap = new Map<String,WorkOrder>();
        //Loop work Order records and pass the record details to setIncidentValues method for updating the incident record with latest work order details.
        for (WorkOrder workOrderIter : (List<WorkOrder>)newItems)
        {
            if (workOrderIter.Incident__c != null && workOrderIter.recordTypeId <> null && workOrderIter.recordtypeId == workRequestRecordTypeId)
            {
                //check if there is a change in ticketofworkstatus if there is a change then delet the nbnintegration cache records.
                if(oldItemMap != null && !oldItemMap.isEmpty())
                {
                    oldItemRecord = (WorkOrder)oldItemMap.get(workOrderIter.Id);
                    if((workOrderIter.status != oldItemRecord.status || workOrderIter.wrqStatusReason__c != oldItemRecord.wrqStatusReason__c) && !lstLocationIds.Contains(workOrderIter.LocationId__c))
                    {
                        lstLocationIds.add(workOrderIter.LocationId__c);   
                    }
                }
                if(!appointmentWorkRequestMap.Keyset().Contains(workOrderIter.AppointmentId__c))
                {
                    appointmentWorkRequestMap.put(workOrderIter.AppointmentId__c,workOrderIter);
                }
                if(!lstIncidentIds.Contains(workOrderIter.Incident__c))
                {
                    lstIncidentIds.Add(workOrderIter.Incident__c);
                }
            }
        }
        if(lstIncidentIds != Null && !lstIncidentIds.isEmpty())
        {
            setIncidentRecordFieldValues(lstIncidentIds,appointmentWorkRequestMap);
        }
        if(lstLocationIds != Null && !lstLocationIds.isEmpty())
        {
            deleteNbnIntegrationCacheRecords(lstLocationIds);
        }
    }
    /***************************************************************************************************
    Method Name:        setIncidentRecordFieldValues
    Method Parameters:  List of Incident RecordIds and AppointmentWorkRequestMap
    Method Return Type: Void
    Method Description :- Used for setting and updating Incident Management Record values based on Values of 
                          Work Order Record updates.
    Version:            1.0 
    Created Date:       06 September 2018
    ***************************************************************************************************/
    private void setIncidentRecordFieldValues(List<Id> lstIncidentRecords,Map<String,WorkOrder> appointmentWorkRequestMap)
    {
        //Variable to hold list of incident records that needs to be updated.
        List<Incident_Management__c> lstUpdateIncidentManagementRecords = new List<Incident_Management__c>();
        //variable to hold the current incident management record that needs to be updated.
        Incident_Management__c updateIncidentManagementRecord;
        //Variable to hold the Work Order that is extracted from appointmentworkorderMap;
        WorkOrder currentWorkOrderRecord;
        String pcrCode;
        Map<String,Jigsaw_PCR_Action_Filter__c> getPCRDetails = new Map<String,Jigsaw_PCR_Action_Filter__c>();
        for( Jigsaw_PCR_Action_Filter__c obj : [SELECT Action_Type__c,C_Code__c,C_Description__c,Id,PCR_Code_Label__c,PCR_Code__c,PCR_Description__c,P_Code__c,P_Description__c,R_Code__c,R_Description__c,Show_Additional_Field__c,With_Appointment__c,With_Commitment__c,WO_Sub_Type__c,WO_Type_FTTN_FTTB__c,WO_Version__c,With_Remediation__c FROM Jigsaw_PCR_Action_Filter__c]) 
        {
            getPCRDetails.put(obj.PCR_Code_Label__c + '_' + obj.WO_Type_FTTN_FTTB__c, obj);
        }
        //Map variable to store derived map values retrived from NBNDerivedFaultType__c custom settings with Fault Type as key.
        Map<String,String> derivedMap = new Map<String,String>();
        //Loop on NBNDerivedFaultType__c custom settings to prepare derivedMap.
        for(NBNDerivedFaultType__c cset : (NBNDerivedFaultType__c.getAll()).Values()) {
            if(!derivedMap.Keyset().Contains(cset.derivedValue__c))
                derivedMap.put(cset.derivedValue__c,cset.Abbreviated_Value__c);
        }
        //Temporay variable used for storing Appointment type retrieved from ticket of work record.used in deriving WOSummary
        String apptType = '';
        //Temporay variable used for storing Appointment Date retrieved from ticket of work record.used in deriving WOSummary//Temporay variable used for preparing work order summary.
        String apptDate = '';
        DateTime tempDate;
        //Query Incident Management Record and check if there is any change in the appointment id associated with current work request and Incident Management Record if there is a change the reset the 
        //Incident management Current Workrqeuest to null.
        for(Incident_Management__c currentIncidentManagementRecord : [Select Id,User_Action__c,Awaiting_Ticket_of_Work__c,Engagement_Type__c,Fault_Type__c,AppointmentId__c,Technology__c,AVC_Id__c,Target_Commitment_Date__c,Appointment_Start_Date__c,Current_Work_Order_Appointment_Id__c From Incident_Management__c Where Id In :lstIncidentRecords])
        {
            currentWorkOrderRecord = appointmentWorkRequestMap.get(currentIncidentManagementRecord.AppointmentId__c);
            if(currentWorkOrderRecord != Null && currentIncidentManagementRecord.AppointmentId__c == currentWorkOrderRecord.AppointmentId__c)
            {
                updateIncidentManagementRecord = new Incident_Management__c(Id = currentIncidentManagementRecord.Id);
                updateIncidentManagementRecord.Work_Request__c = currentWorkOrderRecord.Id;
                updateIncidentManagementRecord.Current_work_order_status__c = currentWorkOrderRecord.status;
                updateIncidentManagementRecord.WOR_ID__c = currentWorkOrderRecord.wrqIdentifier__c;
                updateIncidentManagementRecord.Current_work_order_status_reason__c = currentWorkOrderRecord.wrqStatusReason__c;
                // Stamp PCR description on Incident when P,C and R is not blank on respective Work request. Ref: CUSTSA 23749 - PCR display on Incident status block
                if(String.isNotBlank(currentWorkOrderRecord.closureSymptom__c) && String.isNotBlank(currentWorkOrderRecord.closureCause__c) && String.isNotBlank(currentWorkOrderRecord.closureResolution__c))
                {
                    pcrCode =  currentWorkOrderRecord.closureSymptom__c + '_' + currentWorkOrderRecord.closureCause__c + '_' + currentWorkOrderRecord.closureResolution__c + '_' + currentIncidentManagementRecord.Technology__c;
                    if(getPCRDetails != null && getPCRDetails.get(pcrCode) != null)
                        updateIncidentManagementRecord.Jigsaw_WO_Selected_PCR_Desc__c = getPCRDetails.get(pcrCode).PCR_Description__c;
                }
                if(String.isNotBlank(currentIncidentManagementRecord.User_Action__c) && currentIncidentManagementRecord.User_Action__c.equalsIgnoreCase('createWorkRequest') && currentIncidentManagementRecord.Awaiting_Ticket_of_Work__c && String.isNotblank(currentWorkOrderRecord.status) && (currentWorkOrderRecord.status.equalsIgnoreCase('CREATED') || currentWorkOrderRecord.status.equalsIgnoreCase('RELEASED') || currentWorkOrderRecord.status.equalsIgnoreCase('ASSIGNED')))
                {
                    if(String.isNotBlank(currentIncidentManagementRecord.Engagement_Type__c) && currentIncidentManagementRecord.Engagement_Type__c.equals('Commitment')) {
                        apptType = 'COM';
                        if(currentIncidentManagementRecord.Target_Commitment_Date__c != null) {
                            tempDate = currentIncidentManagementRecord.Target_Commitment_Date__c;
                            apptDate = String.valueOf(tempDate.day()) + '/' + String.valueOf(tempDate.month());
                            apptDate = apptDate + ' ' + tempDate.format('a', 'Australia/Sydney');
                        }
                        else {
                            apptDate = 'No target date supplied';
                        }
                    }
                    if(String.isNotBlank(currentIncidentManagementRecord.Engagement_Type__c) && currentIncidentManagementRecord.Engagement_Type__c.equals('Appointment')) {
                        apptType = 'APT';
                        if(currentIncidentManagementRecord.Appointment_Start_Date__c != null) {
                            system.debug('>>>>Inside Update Appointment start date ' + currentIncidentManagementRecord.Appointment_Start_Date__c);
                            tempDate = currentIncidentManagementRecord.Appointment_Start_Date__c;
                            apptDate = String.valueOf(tempDate.day()) + '/' + String.valueOf(tempDate.month());
                            apptDate = apptDate + ' ' + tempDate.format('a', 'Australia/Sydney');
                        }
                        else {
                            apptDate = 'No appointment start date supplied';
                        }
                    }
                    updateIncidentManagementRecord.summary__c = currentIncidentManagementRecord.Technology__c + '-' + ((String.isNotBlank(currentWorkOrderRecord.faultType__c) && derivedMap.containsKey(currentWorkOrderRecord.faultType__c)) ? derivedMap.get(currentWorkOrderRecord.faultType__c) : ' ') + ' | *' + currentIncidentManagementRecord.AVC_Id__c + '*' + currentWorkOrderRecord.wrqIdentifier__c + ' ' + apptType + ' ' + apptDate;
                    updateIncidentManagementRecord.Awaiting_Ticket_of_Work__c = false;
                    updateIncidentManagementRecord.Awaiting_Current_Incident_Status__c = true;
                    updateIncidentManagementRecord.User_Action__c = 'dispatchTechnician';
                }
                lstUpdateIncidentManagementRecords.Add(updateIncidentManagementRecord);
            }
        }
        getPCRDetails = Null;
        //Update Incident Management Records.
        if(lstUpdateIncidentManagementRecords != Null && !lstUpdateIncidentManagementRecords.isEmpty())
        {
            Update lstUpdateIncidentManagementRecords;
        }
    }
    /***************************************************************************************************
    Method Name:        deserializePlatformEvent
    Method Parameters:  List of inbound Json Messages
    Method Return Type: List of integrationWrapper.TicketOfWork records
    Method Description :- Used for Deserializing list of jsonmessages to integrationwrapper.ticket of work
                          Records
    Version:            1.6
    Created Date:       12 December 2017
    Update Date:        17 April 2019
    Changes:            Updated the code to include the logging of messages to remedy integrationlogs.
    ***************************************************************************************************/
    private static Map<String,List<Map<String,Object>>> deserializePlatformEvent(List<string> InboundJSONMessage) {
        system.debug('>>>' + InboundJSONMessage);
        //Temporary variable to strore the deserialised json record.
        Map<String,Object> deserializedJsonRecord;
        //Temporary variable used for storing the deserialised json.
        List<Map<String,Object>> deserialezedObjectMap;
        //Map variable used for building mapping between wrqidentifier and list of jsons that needs to be processed for upserting the records to salesforce.
        Map<String,List<Map<String,Object>>> ticketOfWorkJsonMap = New Map<String,List<Map<String,Object>>>();
        //used for intializing the RemedyIntegrationLog Record.
        RemedyToSalesforceIntegrationLog__c remedyIntegrationLog;
        //List variable used for inserting RemedytoSalesforceIntegrationLog.
        List<RemedyToSalesforceIntegrationLog__c> lstRemedyIntegrationLog = new List<RemedyToSalesforceIntegrationLog__c>();
        //Temporary variable used for getting the salesforce ids of inserted records used for updating failure flags.
        Map<Id,RemedyToSalesforceIntegrationLog__c> remedyIntegrationLogMap = new Map<Id,RemedyToSalesforceIntegrationLog__c>();
        //Variable to note down the order in which the jsons are received.
        Integer OrderNumber = 1;
        //Variable to hold wrqIdentifier
        String wrqIdentifier;
        //Variables used for transforming JSON repsonse.
        String JSONExc = '"exception"';
        String JSONExcrep = '"exception_x"';
        String JSONVal;
        String replacedJSONVal;
        //Loop the list of jsons passed from platform events deserialize json and add to list of lstJSONWrap variable.
        for(String strJson : InboundJSONMessage) {
            system.debug('>>>> 1 ' + strJson);
            JSONVal = String.valueOf(strJson);
            system.debug('>>>> 2 ' + JSONVal);
            replacedJSONVal = JSONVal.replace(JSONExc,JSONExcrep);
            if(String.isNotblank(replacedJSONVal))
            {
                //Intialize the remedy integration log record.
                remedyIntegrationLog = new RemedyToSalesforceIntegrationLog__c();
                try
                {
                    deserializedJsonRecord = (Map<String,Object>)JSON.deserializeUntyped(replacedJSONVal);
                    wrqIdentifier = (string)deserializedJsonRecord.get('ticketOfWorkId');
                    if(!ticketOfWorkJsonMap.Keyset().Contains(wrqIdentifier))
                    {
                        deserialezedObjectMap = new List<Map<String,Object>>();
                        deserialezedObjectMap.Add(deserializedJsonRecord);
                        ticketOfWorkJsonMap.put(wrqIdentifier,deserialezedObjectMap);       
                    }
                    else
                    {
                        (ticketOfWorkJsonMap.get(wrqIdentifier)).Add(deserializedJsonRecord);
                    }
                    remedyIntegrationLog.InsertedTime__c = System.Now().getTime();
                    remedyIntegrationLog.JsonOrder__c = OrderNumber;
                    remedyIntegrationLog.InboundJSONMessage__c = replacedJSONVal;
                    remedyIntegrationLog.Incident_Number__c = ((string)deserializedJsonRecord.get('incidentId'));
                    remedyIntegrationLog.ProcessedSuccessfully__c = True;
                    lstRemedyIntegrationLog.Add(remedyIntegrationLog);
                }
                catch(exception Ex)
                {
                    remedyIntegrationLog.InsertedTime__c = System.Now().getTime();
                    remedyIntegrationLog.JsonOrder__c = OrderNumber;
                    remedyIntegrationLog.InboundJSONMessage__c = replacedJSONVal;
                    remedyIntegrationLog.FailedMessageDetails__c = 'Invalid Json Format';
                    lstRemedyIntegrationLog.Add(remedyIntegrationLog);
                    continue;
                }
                OrderNumber += 1;
            }
        }
        if(lstRemedyIntegrationLog != Null && !lstRemedyIntegrationLog.isEmpty())
        {
            Insert lstRemedyIntegrationLog;
            remedyIntegrationLogMap.putAll(lstRemedyIntegrationLog);
            lstRemedyIntegrationLogIds.addAll(remedyIntegrationLogMap.KeySet());
        }
        return ticketOfWorkJsonMap;
    }
    private static void createUpdateWorkRequestRecords(Map<String,List<Map<String,Object>>> workRequestRecordsMap)
    {
        //Map to Store Child Sobjects To Be Inserted;
        Map<String,sObject> upsertSobjectMap = new Map<String,sObject>();
        //Map Variable to store the list of object values retrived from wrqidentifier and workorder map.
        List<Map<String,Object>> lstWorkRequestDataMap;
        //Temporary Variable used in generating sobject record that needs to be upserted in system.
        Sobject ObjectRecordTobeUpserted;
        //Temporary variable tp store the workrequestidentifier
        String strWRQIdentifier;
        //Temporary variable used in storing Feild Value Retrived from Json Passed By Micro Services.
        String fieldValue;
        //Temporary variable used in storing Salesforce Feild Api Name Retrived from Custom Settings.
        String salesforceFieldApiName;
        //Temporary variable used in constructing datetime feild value from epoch format.
        Long dateValueInEpochFormat;
        //Temporary variable used in constructing datetime feild value from epoch format.
        Datetime DateFieldValue;
        //Temporary Variable to hold the parent relationship object via external id.
        sObject parentsObject;
        //Temporary Variable to Store the externalidfieldtype.
        Schema.SObjectField externalIdFieldType;
        //Temporary Variable used in reading the configuration parameters.
        InboundPlatformEventMap__c inboundPlatformEventRecord = [Select Id,OBJECT__C,ObjectExternalIdFieldForUpsert__c From InboundPlatformEventMap__c Where OBJECT__C = 'WorkOrder' Limit 1];
        //Temporary Variable used in loops for processing Field values extracted from custom settings.
        List<RemedyIncidentDetailsJSONtoSFFields__c> lstRemedyJsonSFFields = [Select Name,DeveloperName__c,IntegrationType__c,Is_Datetime_Field__c,Is_External_Id__c,Is_Long_Value__c,Is_Lookup_Field__c,Is_Parent_Element_Array__c,
                                                                              Is_Parent_Element__c,Parent_JSON_Key__c,Salesforce_Field__c,Is_Object_Identifier__c,towJSONKey__c,ParentObjectRecordIdentifier__c From RemedyIncidentDetailsJSONtoSFFields__c 
                                                                              Where Object__c = 'WorkOrder' and Parent_JSON_Key__c = 'wrqData'];
        Map<String,Schema.SObjectType> globalSobjectTypeMap = Schema.getGlobalDescribe();
        Map<String,Id> objectRecTypes = GlobalCache.getRecordTypeIdsByDeveloperName(WorkOrder.SObjectType);
        if(inboundPlatformEventRecord != Null)
        {
            externalIdFieldType = globalSobjectTypeMap.get(inboundPlatformEventRecord.OBJECT__C).getDescribe().fields.getMap().get(inboundPlatformEventRecord.ObjectExternalIdFieldForUpsert__c);
        }
        for(String wrqIdentifier : workRequestRecordsMap.keyset())
        {
            lstWorkRequestDataMap = workRequestRecordsMap.get(wrqIdentifier);
            For(Map<String,Object> masterJsonMap : lstWorkRequestDataMap)
            {
                if (masterJsonMap != NULL && !masterJsonMap.isEmpty() && lstRemedyJsonSFFields <> null && !lstRemedyJsonSFFields.isEmpty())
                {
                    strWRQIdentifier = (String)masterJsonMap.get('ticketOfWorkId');
                    if(upsertSobjectMap.Keyset().Contains(strWRQIdentifier))
                    {
                        ObjectRecordTobeUpserted = upsertSobjectMap.get(strWRQIdentifier);
                        upsertSobjectMap.Remove(strWRQIdentifier);
                    }
                    else
                    {
                        ObjectRecordTobeUpserted = (globalSobjectTypeMap.get(inboundPlatformEventRecord.OBJECT__C)).newSObject();
                        ObjectRecordTobeUpserted.put(inboundPlatformEventRecord.ObjectExternalIdFieldForUpsert__c,wrqIdentifier);
                    }
                    //Decode Feild Values from Jsons inserted in platform events upsert the records in Salesforce Database using RemedyIncidentDetailsJSONtoSFFields custom settings.
                    for(RemedyIncidentDetailsJSONtoSFFields__c remedyJsonFeildkey : lstRemedyJsonSFFields) 
                    {
                        if(masterJsonMap.containsKey(remedyJsonFeildkey.towJSONKey__c)) 
                        {
                            if(remedyJsonFeildkey.Is_Lookup_Field__c)
                            {
                                parentsObject = (globalSobjectTypeMap.get(remedyJsonFeildkey.DeveloperName__c)).newSObject();
                                parentsObject.put(remedyJsonFeildkey.ParentObjectRecordIdentifier__c,(String)masterJsonMap.get(remedyJsonFeildkey.towJSONKey__c));
                                ObjectRecordTobeUpserted.putSobject(remedyJsonFeildkey.Salesforce_Field__c,parentsObject);
                            }
                            else
                            {
                                if(remedyJsonFeildkey.Is_External_Id__c) 
                                {
                                    ObjectRecordTobeUpserted.put('RecordTypeId',objectRecTypes.get(remedyJsonFeildkey.DeveloperName__c));
                                }
                                fieldValue = (String)masterJsonMap.get(remedyJsonFeildkey.towJSONKey__c);
                                fieldValue = (String.isBlank(fieldValue) || fieldValue.equalsIgnoreCase('null')) ? Null : fieldValue;
                                salesforceFieldApiName = remedyJsonFeildkey.Salesforce_Field__c;
                                if ((String.isNotBlank(fieldValue) && !remedyJsonFeildkey.Is_Datetime_Field__c) || String.isBlank(fieldValue)) 
                                {
                                    ObjectRecordTobeUpserted.put(salesforceFieldApiName,fieldValue);
                                }
                                //check feild value to be inserted or updated is datetime field if so then create a new datetime value and update it on the field. 
                                if(remedyJsonFeildkey.Is_Datetime_Field__c && String.isNotBlank(fieldValue) && !fieldValue.equalsIgnoreCase('null'))
                                {
                                    dateValueInEpochFormat = Long.valueOf(fieldValue + '000');
                                    DateFieldValue = DateTime.newInstance(dateValueInEpochFormat);
                                    ObjectRecordTobeUpserted.put(remedyJsonFeildkey.SALESFORCE_FIELD__C,DateFieldValue);
                                }
                            }
                        }
                    }
                    upsertSobjectMap.Put(wrqIdentifier,ObjectRecordTobeUpserted);
                }
            }
        }
        if(upsertSobjectMap != Null && !upsertSobjectMap.isEmpty())
        {
            upsertSobjectRecordsInDatabase(upsertSobjectMap,inboundPlatformEventRecord,externalIdFieldType);
        }
    }
    /***************************************************************************************************
    Method Name:        upsertSobjectRecordsInDatabase
    Method Parameters:  Map of objects that needs to be inserted in sequense,globalsobjectType used for getting field type,parentSobjectMap 
                        and childobject Sequence Map.
    Method Return Type: Void
    Method Description :- used for upserting the records to salesforce database.
    Version:            1.0 
    Created Date:       25 September 2018
    ***************************************************************************************************/
    private static void upsertSobjectRecordsInDatabase(Map<String,sObject> upsertSobjectMap,InboundPlatformEventMap__c inboundPlatformEventRecord,Schema.SObjectField externalIdFieldType)
    {
        retryCount = 1;
        //Temporary Variable Used in recodring the error message list with respect to each incident Identifier.
        Map<String,List<String>> remedyToSalesforceIntegrationErrorLog = new Map<String,List<String>>();
        //Temporary Variables used in upsert result list to get the error message details.
        List<Database.UpsertResult> SR;
        List<Database.Error> errorDetails;
        //Temporary variable used in building failed message that needs to be logged in Integration Log object.
        String failedMessageDetails;
        //Temporary Variable used in preparing the list of failed messages along with record jsons.
        List<String> lstFailedMessages;
        //temporary variable used in storing list of records to be upserted and send for datatbase operation;
        List<sObject> lstsObjectRecords;
        if(upsertSobjectMap != Null && !upsertSobjectMap.isEmpty())
        {
            String ObjectName = inboundPlatformEventRecord.OBJECT__C;
            String externalIdFieldName = inboundPlatformEventRecord.ObjectExternalIdFieldForUpsert__c;
            lstsObjectRecords = upsertSobjectMap.Values();
            //Declare Concrete List for Upsertion of records
            String concreteListType = 'List<' + ObjectName + '>';
            List<SObject> concreteSobjectRecords = (List<SObject>)Type.forName(concreteListType).newInstance();
            concreteSobjectRecords.addAll(lstsObjectRecords);
            try 
            {
                SR = Database.Upsert((List<WorkOrder>)concreteSobjectRecords,externalIdFieldType,false);
                for(Integer i = 0; i < SR.Size() ; i++)
                {
                    if(!SR[i].isSuccess())
                    {
                        errorDetails = SR[i].getErrors();
                        failedMessageDetails = errorDetails[0].getMessage() + '\r\n' +JSON.serialize(lstsObjectRecords[i]);
                        if(!remedyToSalesforceIntegrationErrorLog.Keyset().Contains((String)lstsObjectRecords[i].get(externalIdFieldName)))
                        {
                            lstFailedMessages = New List<String>();
                            lstFailedMessages.Add(failedMessageDetails);
                            remedyToSalesforceIntegrationErrorLog.put((String)lstsObjectRecords[i].get(externalIdFieldName),lstFailedMessages);
                        }
                    }
                }
            }
            catch (Exception Ex) 
            {
                if(ex.getMessage() !=null && ex.getMessage().Contains('UNABLE_TO_LOCK_ROW, unable to obtain') && retryCount < 3)
                {
                    upsertSobjectRecordsInDatabase(upsertSobjectMap,inboundPlatformEventRecord,externalIdFieldType);
                }
                System.Debug(ex.getMessage());
                GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'WorkOrderHandler','upsertSobjectRecordsInDatabase','','Tried to upsert a Work Order by code called from Platfrom Event called ticketofWOrk',String.valueOf(retryCount),'',ex,0);
            }
            //Update RemedyIntegration Log Records
            if(remedyToSalesforceIntegrationErrorLog != Null && !remedyToSalesforceIntegrationErrorLog.isEmpty())
            {
                ProcessRemedyToSalesforceIntergration.updateRemedySalesforceIntegrationLog(remedyToSalesforceIntegrationErrorLog,lstRemedyIntegrationLogIds);
            }
        }
    }
    /***************************************************************************************************
    Method Name:        deleteNbnIntegrationCacheRecords
    Method Parameters:  List of Location Ids.
    Method Return Type: Void
    Method Description :- Used for deleting nbnIntegrationCache Records.
    Version:            1.0
    Created Date:       02 October 2018
    ***************************************************************************************************/
    private Static void deleteNbnIntegrationCacheRecords(List<String> lstLocationIds)
    {
        List<NBNIntegrationCache__c> lstNbnIntegrationCacheRecords = new List<NBNIntegrationCache__c>();
        NBNIntegrationCache__c currentWorkingOrder;
        for(NBNIntegrationCache__c nbnCacheRecord : [Select Id From NBNIntegrationCache__c Where LocId__c In :lstLocationIds and IntegrationType__c = 'WorkHistory'])
        {
            currentWorkingOrder = new NBNIntegrationCache__c(Id = nbnCacheRecord.Id);
            lstNbnIntegrationCacheRecords.Add(currentWorkingOrder);  
        }
        if(lstNbnIntegrationCacheRecords.size() > 0 && !lstNbnIntegrationCacheRecords.isEmpty())
            Delete lstNbnIntegrationCacheRecords;
    }
}