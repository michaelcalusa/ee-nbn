/***************************************************************************************************
Class Name:         WorkOrderHandlerEnhanced
Class Type:         Trigger handler class
Version:            1.0 
Created Date:       13 December 2017
Function:           Handle all Work Order trigger contexts including triggers from work order object and platform events
Input Parameters:   triggers new, old and platform events
Output Parameters:  None
Description:        To be used as a service class
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Kashyap Murthy    13/12/2017      Created - Version 1.0 
* Rohit Mathur      16/02/2018      Updated - Version 1.1
* Beau Anderson     21/03/2018      Updated - Version 1.2
****************************************************************************************************/ 
public class WorkOrderHandlerEnhanced {
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    public void updateWorkOrder(List<String> newItems,List<integrationWrapper.TicketOfWork> lstJSONWrap) {
        try{
             Map<String,integrationWrapper.TicketOfWork> workTransactionIdMap = new Map<String,integrationWrapper.TicketOfWork>();
             Map<String,integrationWrapper.TicketOfWork> IncidentIdWrapperMap = new Map<String,integrationWrapper.TicketOfWork>();
             If(lstJSONWrap != null && !lstJSONWrap.isEmpty()) {
                 for(integrationWrapper.TicketOfWork currentTicketOfWork : lstJSONWrap) {
                     if(!String.isBlank(currentTicketOfWork.transactionId) && !workTransactionIdMap.Keyset().Contains(currentTicketOfWork.transactionId))
                     {
                         workTransactionIdMap.put(currentTicketOfWork.transactionId,currentTicketOfWork);
                     }
                     if(!String.isBlank(currentTicketOfWork.incidentId) && !IncidentIdWrapperMap.KeySet().Contains(currentTicketOfWork.incidentId))
                     {
                         IncidentIdWrapperMap.put(currentTicketOfWork.incidentId,currentTicketOfWork);
                     }
                 }
             }
             WorkOrder currentTicketOfWork;
             integrationWrapper.TicketOfWork ticketOfWorkWrapper;
             String strPlannedCompletionDate;
             Long tempVariable;
             DateTime workOrderDatetime;
             Incident_Management__c currentWorkingIncidentRecord;
             List<WorkOrder> lstWorkOrdersToBeUpdated = new List<WorkOrder>();
             List<Incident_Management__c> lstUpdateIncidentManagementRecords = new List<Incident_Management__c>();
             List<Id> lstIncidentManagementRecordIds = new List<Id>();
             For(WorkOrder currentWorkingOrder : [Select Id,Incident__c,transactionId__c From WorkOrder where transactionId__c IN :workTransactionIdMap.KeySet()])
             {
                 currentTicketOfWork = new WorkOrder(Id = currentWorkingOrder.Id);
                 ticketOfWorkWrapper = workTransactionIdMap.get(currentWorkingOrder.transactionId__c);
                 if(!String.isBlank(ticketOfWorkWrapper.ticketOfWorkStatus) && ticketOfWorkWrapper.ticketOfWorkStatus.equals('CREATED'))
                 {
                    currentTicketOfWork.additionalInformation__c = ticketOfWorkWrapper.additionalInformation;
                    currentTicketOfWork.faultType__c = ticketOfWorkWrapper.faultType;
                    currentTicketOfWork.faultDetails__c = ticketOfWorkWrapper.faultDetails;
                    currentTicketOfWork.status = ticketOfWorkWrapper.ticketOfWorkStatus;
                    currentTicketOfWork.wrqIdentifier__c = ticketOfWorkWrapper.ticketOfWorkId;
                    currentTicketOfWork.wrqStatusReason__c = ticketOfWorkWrapper.ticketOfWorkStatusReason;
                    currentTicketOfWork.closureSummary__c = ticketOfWorkWrapper.closureSummary;
                    currentTicketOfWork.closureSymptom__c = ticketOfWorkWrapper.closureSymptom;
                    currentTicketOfWork.closureCause__c = ticketOfWorkWrapper.closureCause;
                    currentTicketOfWork.closureResolution__c = ticketOfWorkWrapper.closureResolution;
                    currentTicketOfWork.AppointmentId__c = ticketOfWorkWrapper.appointmentId;
                    currentTicketOfWork.appointmentType__c = ticketOfWorkWrapper.appointmentType;
                    if(!String.isBlank(ticketOfWorkWrapper.plannedCompletionDate))
                    {
                        strPlannedCompletionDate = ticketOfWorkWrapper.plannedCompletionDate;
                        if (strPlannedCompletionDate.length() <= 10)
                        {
                            strPlannedCompletionDate = strPlannedCompletionDate + '000';
                            tempVariable = Long.valueOf(strPlannedCompletionDate);
                            workOrderDatetime = DateTime.newInstance(tempVariable);
                            currentTicketOfWork.StartDate = workOrderDatetime;
                            currentTicketOfWork.EndDate = workOrderDatetime;
                        }
                        else
                        {
                            tempVariable = Long.valueOf(strPlannedCompletionDate);
                            workOrderDatetime = DateTime.newInstance(tempVariable);
                            currentTicketOfWork.StartDate = workOrderDatetime;
                            currentTicketOfWork.EndDate = workOrderDatetime;
                        }
                    }
                    else
                    {
                        workOrderDatetime = System.Now();
                        currentTicketOfWork.StartDate = workOrderDatetime;
                        currentTicketOfWork.EndDate = workOrderDatetime;
                    }
                    if(!lstIncidentManagementRecordIds.Contains(currentWorkingOrder.Incident__c))
                        lstIncidentManagementRecordIds.Add(currentWorkingOrder.Incident__c);
                 }
                 if(!String.isBlank(ticketOfWorkWrapper.ticketOfWorkStatus) && ticketOfWorkWrapper.ticketOfWorkStatus.equals('REJECTED'))
                 {
                     currentTicketOfWork.status = ticketOfWorkWrapper.ticketOfWorkStatus;
                     if(ticketOfWorkWrapper.exception_x != Null)
                     {
                         currentTicketOfWork.wrqStatusReason__c  = ticketOfWorkWrapper.exception_x.code + ': ' + ticketOfWorkWrapper.exception_x.message;
                         currentWorkingIncidentRecord = new Incident_Management__c(Id = currentTicketOfWork.Incident__c);
                         currentWorkingIncidentRecord.DispatchTechExceptionMessage__c = ticketOfWorkWrapper.exception_x.message;
                         currentWorkingIncidentRecord.Awaiting_Ticket_of_Work__c = false;
                         currentWorkingIncidentRecord.Awaiting_Current_Incident_Status__c = false;
                         lstUpdateIncidentManagementRecords.Add(currentWorkingIncidentRecord);
                     }
                 }
                 lstWorkOrdersToBeUpdated.Add(currentTicketOfWork);
             }
             //Update WorkOrder Records
             if(lstWorkOrdersToBeUpdated != Null && !lstWorkOrdersToBeUpdated.isEmpty())
                Update lstWorkOrdersToBeUpdated;
             //Update Incident Management Records for which work order status is created.
             if(lstIncidentManagementRecordIds != Null && !lstIncidentManagementRecordIds.isEmpty())
                updateIncidentManagementRecords(lstIncidentManagementRecordIds,IncidentIdWrapperMap);
             //Update Incident Management Records for which work order status is rejected;
             if(lstUpdateIncidentManagementRecords != Null && !lstUpdateIncidentManagementRecords.isEmpty())
                Update lstUpdateIncidentManagementRecords;
        }
        catch(Exception ex) {
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'WorkOrderHandler','updateWorkOrder','','Tried to update a Work Order by code called from Platfrom Event called ticketofWOrk','','',ex,0);
        }
    }
    private void updateIncidentManagementRecords(List<Id> lstIncidentManagementRecords,Map<String,integrationWrapper.TicketOfWork> IncidentIdWrapperMap)
    {
        try
        {
            Incident_Management__c currentIncidentManagementRecord;
            List<Incident_Management__c> listIncToUpdate = new List<Incident_Management__c>();
            Map<String,String> derivedMap = new Map<String,String>();
            for(NBNDerivedFaultType__c cset : (NBNDerivedFaultType__c.getAll()).Values()) {
                if(!derivedMap.Keyset().Contains(cset.incidentFaultType__c))
                    derivedMap.put(cset.incidentFaultType__c,cset.derivedValue__c);
            }
            if(lstIncidentManagementRecords != Null && !lstIncidentManagementRecords.isEmpty())
            {
                String faultType = '';
                String techType = '';
                String avc = '';
                String wo = '';
                String apptType = '';
                String apptDate = '';
                String woSummary = '';
                DateTime apptStart;
                DateTime targCom;
                integrationWrapper.TicketOfWork towRecord;
                for(Incident_Management__c currentIncidentRecord : [Select Id,Name,Awaiting_Ticket_of_Work__c,WOR_ID__c,Summary__c,User_Action__c,Fault_Type__c,Prod_Cat__c,Current_work_order_status_reason__c,Status_Reason__c,AVC_Id__c,Target_Commitment_Date__c,Appointment_Start_Date__c,Engagement_Type__c,Current_Work_Order_Appointment_Id__c from Incident_Management__c Where Id In :lstIncidentManagementRecords]) {
                    towRecord = IncidentIdWrapperMap.get(currentIncidentRecord.Name);
                    if(derivedMap.containsKey(currentIncidentRecord.Fault_Type__c)) {
                        faultType = derivedMap.get(currentIncidentRecord.Fault_Type__c);
                    }
                    if(!String.isBlank(currentIncidentRecord.Prod_Cat__c) && currentIncidentRecord.Prod_Cat__c.Equals('NCAS-FTTN')) {
                        techType = 'FTTN';
                    }
                    if(!String.isBlank(currentIncidentRecord.Prod_Cat__c) && currentIncidentRecord.Prod_Cat__c.Equals('NCAS-FTTB')) {
                        techType = 'FTTB';
                    }
                    if(!String.isBlank(towRecord.appointmentId))
                    {
                        currentIncidentRecord.Current_Work_Order_Appointment_Id__c = towRecord.appointmentId;
                    }
                    if(!String.isBlank(towRecord.appointmentType))
                    {
                        currentIncidentRecord.Engagement_Type__c = towRecord.appointmentType;
                    }
                    if(currentIncidentRecord.Engagement_Type__c.equals('Commitment')) {
                        apptType = 'COM';
                        if(currentIncidentRecord.Target_Commitment_Date__c != null){
                            targCom = currentIncidentRecord.Target_Commitment_Date__c;
                            apptDate = String.valueOf(targCom.day())+'/'+String.valueOf(targCom.month());
                        } else {
                            apptDate = 'No target date supplied';
                        }
                    }
                    if(!String.isBlank(currentIncidentRecord.Engagement_Type__c) && currentIncidentRecord.Engagement_Type__c.equals('Appointment')) {
                            apptType = 'APT';
                            if(currentIncidentRecord.Appointment_Start_Date__c != null) {
                                system.debug('>>>>Inside Update Appointment start date ' + currentIncidentRecord.Appointment_Start_Date__c);
                                apptStart = currentIncidentRecord.Appointment_Start_Date__c;
                                apptDate = String.valueOf(apptStart.day())+'/'+String.valueOf(apptStart.month());
                            }else{
                                apptDate = 'No appointment start date supplied';
                            }
                    }
                    avc = currentIncidentRecord.AVC_Id__c;
                    wo = currentIncidentRecord.WOR_ID__c;    
                    woSummary = techType+'-'+faultType+' | *'+avc+'*'+wo+' '+apptType+' '+apptDate; 
                    system.debug('>>>>Summary'+woSummary);
                    currentIncidentRecord.summary__c = woSummary;
                    currentIncidentRecord.WOR_ID__c = towRecord.ticketOfWorkId; 
                    currentIncidentRecord.Current_work_order_status__c = towRecord.ticketOfWorkStatus;
                    currentIncidentRecord.Current_work_order_status_reason__c = towRecord.ticketOfWorkStatusReason;
                    currentIncidentRecord.Awaiting_Ticket_of_Work__c = false;
                    //currentIncidentRecord.Current_work_order_status_reason__c = 'Dispatched';
                    currentIncidentRecord.Status_Reason__c = 'Dispatched';
                    currentIncidentRecord.User_Action__c = 'dispatchTechnician';
                    currentIncidentRecord.Awaiting_Current_Incident_Status__c = true;
                    listIncToUpdate.add(currentIncidentRecord);
                }
                update listIncToUpdate;
            }
        }
        catch(Exception ex) {
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'WorkOrderHandler','updateIncident','','Tried to update an Incident after TOW created via integration','','',ex,0);
        }  
    }
}