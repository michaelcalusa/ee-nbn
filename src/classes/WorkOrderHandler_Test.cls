/***************************************************************************************************
Class Name:  WorkOrderHandler_Test
Class Type: Test Class 
Version     : 2.0 
Created Date: 17/02/2018
Function    : Test Script Used for unit testing WorkOrderHandler
Modification Log :
* Developer                   Date                   Description
* ---------------------------------------------------------------------------------------------------                 
  Elavarasan Aravendan       17/02/2018              Created - Version 1.0 
  Murali Krishna Achanta     06/09/2018              Updated - Version 2.0
****************************************************************************************************/
@isTest()
public class WorkOrderHandler_Test {
    @testSetup static void loadTestDataForManageAppointmentClass() 
    {
        //Load Inbound Platform Event Map Custom Settings From Static Resource
        List<SObject> inboundPlatformEventMapSettings = Test.loadData(InboundPlatformEventMap__c.sObjectType,'InboundPlatformEventMapCustomSettingsDataFile');
        //Load Remedy To Salesforce Field Mapping Custom Settings From Static Resource.
        List<SObject> remedyToSalesforceJsonCustomSettings = Test.loadData(RemedyIncidentDetailsJSONtoSFFields__c.sObjectType,'RemedyToSalesforceJsonSettings');
        Id incidentManagementrecordTypeId = schema.sobjecttype.Incident_Management__c.getrecordtypeinfosbyname().get('Incident Management FTTN/B').getRecordTypeId();
        Id strWorkRequestRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Work Request').getRecordTypeId();
        List<Incident_Management__c> lstIncidentManagementRecords = new List<Incident_Management__c>();
        List<WorkOrder> lstWorkOrderRecords = new List<WorkOrder>();
        WorkOrder currentWorkOrder;
        List<NBNIntegrationCache__c> lstIntegrationCacheRecords = new List<NBNIntegrationCache__c>();
        NBNIntegrationCache__c nbnIntegrationCacheRecord;
        Incident_Management__c incidentManagementRecord;
        //Insert Derived Fault Types
        Insert new NBNDerivedFaultType__c(Name = 'testNBNFaultType',dockedComposervalue__c = 'test dockedComposervalue',Prod_Cat__c = 'NCAS-FTTN;NCAS-FTTB',Abbreviated_Value__c = 'SPD',incidentFaultType__c = 'Speed Degradation',derivedValue__c = 'Speed Degradation');
        Insert new Jigsaw_PCR_Action_Filter__c(Name = '121',Action_Type__c = 'Action_Finish_Activity',C_Code__c = 'CI15',C_Description__c = 'Software error',PCR_Code_Label__c = 'PI8_CI15_RI7',PCR_Code__c = 'PI8 / CI15 / RI7',PCR_Description__c = 'NBN DSLAM fault / Software error / Suspected network fault',
                                              P_Code__c = 'PI8',P_Description__c = 'NBN DSLAM fault',R_Code__c = 'RI7',R_Description__c = 'Suspected network fault',SR_No__c = 121, Show_Additional_Field__c = 'Y',WO_Type_FTTN_FTTB__c = 'FTTB',WO_Version__c = '2.3.0',With_Appointment__c = 'Y',With_Commitment__c = 'Y');
        for(Integer i = 0; i < 10; i++)
        {
             incidentManagementRecord = new Incident_Management__c();
             incidentManagementRecord.Incident_Number__c = 'INC0000025985' + i;
             incidentManagementRecord.Name = 'INC0000025985' + i;
             incidentManagementRecord.RecordTypeId = incidentManagementrecordTypeId;
             incidentManagementRecord.assignee__c = 'TestUser';
             incidentManagementRecord.AppointmentId__c = 'APT10000053317' + i;
             incidentManagementRecord.User_Action__c = 'createWorkRequest';
             incidentManagementRecord.Awaiting_Ticket_of_Work__c = true;
             if(i / 2 == 0)
             {
                 incidentManagementRecord.Prod_Cat__c = 'NCAS-FTTN';
                 incidentManagementRecord.Technology__c = 'FTTN';
                 incidentManagementRecord.Engagement_Type__c = 'Commitment';
                 incidentManagementRecord.Target_Commitment_Date__c = System.Now();
             }
             else
                 {
                     incidentManagementRecord.Prod_Cat__c = 'NCAS-FTTB';
                     incidentManagementRecord.Technology__c = 'FTTB';
                     incidentManagementRecord.Engagement_Type__c = 'Appointment';
                     incidentManagementRecord.Appointment_Start_Date__c = System.Now();
                 }
             lstIncidentManagementRecords.Add(incidentManagementRecord); 
        }
        if(lstIncidentManagementRecords != null && !lstIncidentManagementRecords.Isempty())
             Insert lstIncidentManagementRecords;
        for(Integer i = 0; i < 10; i++)
        {
             currentWorkOrder = new WorkOrder();
             currentWorkOrder.AppointmentId__c = 'APT10000053317' + i;
             currentWorkOrder.LocationId__c = 'LOC10000053317' + i;
             currentWorkOrder.Incident__c = lstIncidentManagementRecords[i].Id;
             currentWorkOrder.RecordTypeId = strWorkRequestRecordTypeId;
             currentWorkOrder.closureSymptom__c = 'PI8';
             currentWorkOrder.closureCause__c = 'CI15';
             currentWorkOrder.closureResolution__c = 'RI7';
             currentWorkOrder.transactionId__c = 'tran123' + i;
             if(i / 2 == 0)
             {
                 currentWorkOrder.appointmentType__c = 'Commitment';
             }
             else
             {
                 currentWorkOrder.appointmentType__c = 'Appointment';
             }
             //Divide By 3 then add WRQIdentifier Ids
             if(i == 3)
             {
                 currentWorkOrder.wrqIdentifier__c = 'WRQ1000233';
             }
             if(i == 9)
             {
                 currentWorkOrder.wrqIdentifier__c = 'WRQ1000239';
             }
             currentWorkOrder.Status = 'REQUESTED';
             lstWorkOrderRecords.Add(currentWorkOrder);
             //Intialize nbnIntegrationCacheRecord
             nbnIntegrationCacheRecord = new NBNIntegrationCache__c();
             nbnIntegrationCacheRecord.LocId__c = 'LOC10000053317' + i;
             nbnIntegrationCacheRecord.IntegrationType__c = 'WorkHistory';
             lstIntegrationCacheRecords.Add(nbnIntegrationCacheRecord);
        }
        //Insert WorkOrder Records
        if(lstWorkOrderRecords != null && !lstWorkOrderRecords.Isempty())
             Insert lstWorkOrderRecords;
        //Insert nbnintegrationcache Records
        if(lstIntegrationCacheRecords != null && !lstIntegrationCacheRecords.Isempty())
             Insert lstIntegrationCacheRecords;
        
    }
    Static testMethod void testAfterInsertHandlePlatformEvent()
    {
        StaticResource resource = [SELECT Body FROM StaticResource WHERE Name='DispatchTechSampleTextJsons'];
        Blob listOfJsons = resource.Body;
        List<String> listOfrecords = listOfJsons.toString().split(';');
        List<ticketOfWork__e> lstticketOfWork = new List<ticketOfWork__e>();
        ticketOfWork__e ticketofRecord;
        for(string currentJson : listOfrecords)
        {
            ticketofRecord = new ticketOfWork__e();
            ticketofRecord.InboundJSONMessage__c = currentJson;
            lstticketOfWork.Add(ticketofRecord);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstticketOfWork);
        Test.StopTest();
    }
    Static testMethod void testAfterUpdateWorkOrder() 
    {
        List<WorkOrder> lstWorkOrders = [Select Id,AppointmentId__c From WorkOrder Where AppointmentId__c Like 'APT10000053317%'];
        WorkOrder currentWorkingOrder;
        List<WorkOrder> ordersToBeUpdated = new List<WorkOrder>();
        integer count = 10;
        for(WorkOrder workord : lstWorkOrders)
        {
            currentWorkingOrder = new WorkOrder(Id = workord.Id);
            currentWorkingOrder.AppointmentId__c = 'APT10000053317' + count;
            currentWorkingOrder.Status = 'CREATED';
            ordersToBeUpdated.Add(currentWorkingOrder);
            count--;      
        }
        Test.startTest();
            List<Database.SaveResult> sr = Database.Update(ordersToBeUpdated);
        Test.StopTest();
    }
    Static testMethod void testAfterDeleteWorkOrder() 
    {
        List<WorkOrder> lstWorkOrders = [Select Id,AppointmentId__c From WorkOrder Where AppointmentId__c Like 'APT10000053317%'];
        WorkOrder currentWorkingOrder;
        List<WorkOrder> ordersToBeUpdated = new List<WorkOrder>();
        integer count = 10;
        for(WorkOrder workord : lstWorkOrders)
        {
            currentWorkingOrder = new WorkOrder(Id = workord.Id);
            currentWorkingOrder.AppointmentId__c = 'APT10000053317' + count;
            currentWorkingOrder.Status = 'CREATED';
            ordersToBeUpdated.Add(currentWorkingOrder);
            count--;      
        }
        Test.startTest();
            List<Database.DeleteResult> sr = Database.Delete(ordersToBeUpdated);
        Test.StopTest();
    }
    Static testMethod void testAfterUnDeleteWorkOrder() 
    {
        List<WorkOrder> lstWorkOrders = [Select Id,AppointmentId__c From WorkOrder Where AppointmentId__c Like 'APT10000053317%'];
        WorkOrder currentWorkingOrder;
        List<WorkOrder> ordersToBeUpdated = new List<WorkOrder>();
        integer count = 10;
        for(WorkOrder workord : lstWorkOrders)
        {
            currentWorkingOrder = new WorkOrder(Id = workord.Id);
            currentWorkingOrder.AppointmentId__c = 'APT10000053317' + count;
            currentWorkingOrder.Status = 'CREATED';
            ordersToBeUpdated.Add(currentWorkingOrder);
            count--;      
        }
        Database.Delete(ordersToBeUpdated);
        Test.startTest();
            List<Database.UnDeleteResult> sr = Database.UnDelete(ordersToBeUpdated);
        Test.StopTest();
    }
}