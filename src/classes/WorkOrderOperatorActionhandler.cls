/***************************************************************************************************
Class Name:         WorkOrderOperatorActionhandler
Class Type:         Trigger handler class
Version:            1.0 
Created Date:       09 September 2018
Function:           Handle all Work Order operator action trigger contexts including triggers from platform events
Input Parameters:   triggers new, old and platform events
Output Parameters:  None
Description:        To be used as a service class
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Lokesh Agarwal    09/09/2018      Created - Version 1.0 

****************************************************************************************************/ 
public class WorkOrderOperatorActionhandler implements ITriggerHandler {
    
    // Allows unit tests (or other code) to disable this trigger for the transaction
    public static Boolean TriggerDisabled = false;
    
    /*
        Checks to see if the trigger has been disabled either by custom setting or by running code
    */
    public Boolean IsDisabled()
    {
        return TriggerDisabled;
    }    
    
    public void BeforeInsert(List<SObject> newItems, Map<Id, SObject> newItemMap) {}
    
    public void BeforeUpdate(List<SObject> newItems, List<SObject> oldItems, Map<Id, SObject> newItemMap, Map<Id, SObject> oldItemMap) {}
    
    public void BeforeDelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    
    public void AfterInsert(List<SObject> newItems, Map<Id, SObject> newItemMap) {}
    
    public void AfterUpdate(List<SObject> newItems, List<SObject> oldItems, Map<Id, SObject> newItemMap, Map<Id, SObject> oldItemMap) {}
    
    public void AfterDelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    
    public void AfterUndelete(List<SObject> oldItems, Map<Id, SObject> oldItemMap) {}
    
   
    /*
     *  Method to handle the WO Operator Action response reveived from MS under TOW Platform Event.
     */
    public static void updateWorkOrderThroughOperatorAction(List<String> newItems,List<integrationWrapper.WOActionResponse> lstJSONWrap){
        try{
            Map<String, RemedyIncidentDetailsJSONtoSFFields__c> custSetMap = CustomSettingsUtility.getAll(); 
            List<RemedyIncidentDetailsJSONtoSFFields__c> lstCustSet = new List<RemedyIncidentDetailsJSONtoSFFields__c>();
            List<String> lstSfFields = new List<String>();
            List<String> lstJSONKey = new List<String>();
            List<String> lstTransId = new List<String>();
            List<String> lstIncNum = new List<String>();
            Map<String,String> mapErrors = new Map<String,String>();
            Map<String,Map<String,String>> mapIncWO = new Map<String,Map<String,String>>(); 
            system.debug('>>lstJSONWrap value :' + lstJSONWrap);
            String errmsg = system.label.OffsiteTechnicianErrorRetry;
            If(!lstJSONWrap.isEmpty() && lstJSONWrap != null && lstJSONWrap.size() > 0){
                for(integrationWrapper.WOActionResponse trId : lstJSONWrap){
                    if(trId.transactionId != null)
                        lstTransId.add(trId.transactionId);
                    if(trid.exceptions != null && trId.transactionStatus == 'Error')
                       // mapErrors.put(trId.transactionId,'Error Code : ' + trId.exceptions.code + ' -- ' + trId.exceptions.message);
                        mapErrors.put(trId.transactionId, errmsg);
                }
            }
            system.debug('>>mapErrors value :' + mapErrors);
        
            String strTransId = GlobalUtility.createSOQLFieldString(lstTransId);
            String strSObject = '';
            system.debug('>>strTransId :' + strTransId);
            Map<String,String> mapWOfields = new Map<String,String>();
            
            SObjectType WOR = Schema.getGlobalDescribe().get('WorkOrder');
            Map<String,Schema.SObjectField> mfields = WOR.getDescribe().fields.getMap();
            for(Schema.SObjectField sfield : mfields.Values()){
                mapWOfields.put(String.valueOf(sfield),String.valueOf(sfield));
            }
            
            If(custSetMap != null && custSetMap.size() > 0){
                for(RemedyIncidentDetailsJSONtoSFFields__c cset:custSetMap.values()){
                    if(cset.Parent_JSON_Key__c == 'wrqData' && mapWOfields.containsKey(cset.Salesforce_Field__c)){
                        lstCustSet.add(cset);
                    }
                }
            }
            system.debug('>>lstCustSet :' + lstCustSet);
            if(!lstCustSet.isEmpty() && lstCustSet.size() > 0 && lstCustSet != null){
                for(RemedyIncidentDetailsJSONtoSFFields__c towCustset:lstCustSet){
                    lstSfFields.add(towCustset.Salesforce_Field__c);
                    strSObject = lstCustSet[0].Object__c;
                }
            }
             system.debug('lstSfFields are :' + lstSfFields);
            Set<String> setSFFields = new Set<String>();
            setSFFields.addAll(lstSfFields);
             lstSfFields.clear();
            lstSfFields.addAll(setSFFields);
            String objectFields=GlobalUtility.createSOQLFieldString(lstSfFields); 
            //List<String> tempLst = new List<String>{'Lee','Jeff','Dogules','Alok'};
            String commaSeparatedFields ='';
            for(String fieldName : lstTransId){
            system.debug('fieldname is ' + fieldname);
              if((commaSeparatedFields == null || commaSeparatedFields == '') && fieldName != null){
                  commaSeparatedFields = '\'' +fieldName + '\'';
              }else if(fieldName != null){
                  commaSeparatedFields =  commaSeparatedFields + ',\'' + fieldName +'\'';
              }
              system.debug('query is in loop :' + commaSeparatedFields);
            } 
 
            //String listofTrans=GlobalUtility.createSOQLFieldString(lstTransId); 
            //String listofInc=GlobalUtility.createSOQLFieldString(lstIncNum);
             system.debug('objectFields are :' + objectFields);
            List<WorkOrder> lstWorkOrders = new List<WorkOrder>();
            List<WorkOrder> lstWorkOrdersToUpdateCreated = new List<WorkOrder>();
            Map<String,Map<String,String>> mapWOActionError = new Map<String,Map<String,String>>();
           // List<WorkOrder> lstWorkOrdersToUpdateRejected = new List<WorkOrder>();
            String workOrderQuery = '';
            if(commaSeparatedFields!=null ){
              //  workOrderQuery = 'select id,transactionId__c,' + objectFields +' from '+ strSObject +' where transactionId__c IN (\'' +commaSeparatedFields+'\')';
               //workOrderQuery = 'select id,transactionId__c,' + objectFields +' from '+ strSObject +' where transactionId__c IN (' +commaSeparatedFields+')';
               workOrderQuery = 'select id,transactionId__c,Incident__c,WO_Transaction_Status__c from WorkOrder where transactionId__c IN (' +commaSeparatedFields+')';
            }
            system.debug('>>>>workOrderQuery' +workOrderQuery);
            lstWorkOrders = Database.query(workOrderQuery);
            system.debug('>>>>lstWorkOrders' +lstWorkOrders);
            
            
            Map<String,WorkOrder> mapTow = new Map<String,WorkOrder>();
            Map<String,Map<String,integrationWrapper.WOActionResponse>> mapJSONKey = new Map<String,Map<String,integrationWrapper.WOActionResponse>>();
            Map<String,integrationWrapper.WOActionResponse> mapJSON = new Map<String,integrationWrapper.WOActionResponse>();
            Map<String,String> incidentIdsErrorRecords = new Map<String,String>();
            If(!lstWorkOrders.isEmpty() && lstWorkOrders != null && lstWorkOrders.size() > 0){
                for(WorkOrder wo:lstWorkOrders){
                    mapTow.put(wo.transactionId__c,wo); 
                    incidentIdsErrorRecords.put(wo.Incident__c,wo.transactionId__c);
                }
                system.debug('>>>>mapTow' +mapTow);
            }
            // Update Incident Record Status Related to WorkOrder.
            
            List<Incident_Management__c> lstIncToUpdate = new  List<Incident_Management__c>();
                            
            if(!incidentIdsErrorRecords.isEmpty() && incidentIdsErrorRecords.size() > 0){
                List<Incident_Management__c> lstIncidents = [SELECT Id, Jigsaw_WO_TOW_Response_Status__c, Jigsaw_WO_TOW_Response_Message__c, Awaiting_Current_Incident_Status__c FROM Incident_Management__c WHERE ID in: incidentIdsErrorRecords.keyset()];
                if(!lstIncidents.isEmpty()  && lstIncidents.size() > 0){
                    for (Incident_Management__c objInc : lstIncidents){
                        if(!mapErrors.isEmpty() && mapErrors.containsKey(incidentIdsErrorRecords.get(objInc.id))){
                            objInc.Jigsaw_WO_TOW_Response_Status__c = 'ERROR';
                            objInc.Jigsaw_WO_TOW_Response_Message__c = mapErrors.get(incidentIdsErrorRecords.get(objInc.id));
                            objInc.Awaiting_Current_Incident_Status__c = false;
                        } 
                        else{
                            objInc.Jigsaw_WO_TOW_Response_Status__c = 'SUCCESS';
                            objInc.Jigsaw_WO_TOW_Response_Message__c = '';
                        }
                        Integer randomNumber = Math.round(Math.random()*1000);
                        objInc.Jigsaw_WO_TOW_Response_Status__c = objInc.Jigsaw_WO_TOW_Response_Status__c + '_' + String.valueOf(randomNumber);
                        lstIncToUpdate.add(objInc);
                    }
                }
            }
            system.debug('>>lstIncToUpdate value :' + lstIncToUpdate);
            system.debug('>>>>before Created workorderhandler jsonwrap' + lstJSONWrap);
            If(!lstJSONWrap.isEmpty() && lstJSONWrap.size() > 0 && lstJSONWrap != null){
                
                for(integrationWrapper.WOActionResponse towJson:lstJSONWrap){
                    mapJSON.put(towJson.transactionId,towJson);
                }
                system.debug('>>>>before Created workorderhandler' + mapJSON);
            }
            
            if(mapTow != null && mapJSON != null){
                for(String tranId:mapTow.keySet()){
                    if(mapJSON.containsKey(tranId)){ 
                        system.debug('>>>>before Created workorderhandler' + mapJSON.get(tranId).transactionStatus);
                        if(mapJSON.get(tranId).transactionStatus == 'Completed'){
                            system.debug('>>>>Inside Created workorderhandler' );
                            
                       // Below values need to be set in WO with respect to RemedyIncidentDetailsJSONtoSFFields custom setting  
                            mapTow.get(tranId).put('WO_Transaction_Status__c',mapJSON.get(tranId).transactionStatus);
                        //  mapTow.get(tranId).put('faultType__c',mapJSON.get(tranId).techType);
                        //  mapTow.get(tranId).put('faultDetails__c',mapJSON.get(tranId).woSpecVersion);
                            mapTow.get(tranId).put('WO_Revision_Number__c',mapJSON.get(tranId).revisionNumber);
                            system.debug('date time is - ' + mapJSON.get(tranId).revisionTime);
                            //mapTow.get(tranId).put('WO_Revision_Time__c',mapJSON.get(tranId).revisionTime);
                            mapTow.get(tranId).put('Status',mapJSON.get(tranId).currentFieldWorkStatus);
                            mapTow.get(tranId).put('WO_Activity_Id__c',mapJSON.get(tranId).activityId);
                            mapTow.get(tranId).put('WO_Activity_StateId__c',mapJSON.get(tranId).currentActivityStateId);
                            //mapTow.get(tranId).put('WO_Activity_State_DateTime__c',mapJSON.get(tranId).currentActivityStateDateTime);
                            mapTow.get(tranId).put('WO_Interaction_Status__c',mapJSON.get(tranId).interactionStatus);
                          
                            datetime dt= datetime.now();
                            mapTow.get(tranId).put('StartDate',dt);//Added due to SF requirement/system error if not set
                            mapTow.get(tranId).put('EndDate',dt);
                         
                            lstWorkOrdersToUpdateCreated.add(mapTow.get(tranId));  
                        }
                      /*  else if(mapJSON.get(tranId).transactionStatus == 'Error'){
                            Map<String,String> mapError = new Map<String,String>();
                            mapError.put(mapJSON.get(tranId).exceptions.code,mapJSON.get(tranId).exceptions.message);
                            mapWOActionError.put(tranId,mapError);
                        }*/
                        
                    }
                }
                system.debug('>>>>lstWorkOrdersToUpdateCreated' +lstWorkOrdersToUpdateCreated); 
                If(lstWorkOrdersToUpdateCreated !=null && lstWorkOrdersToUpdateCreated.size() > 0){
                    system.debug('#### IM List : '+lstWorkOrdersToUpdateCreated);
                    //Added Map to update the existing records to avoid duplicate objects in the list.
                    Map<id,SObject> imMap = new Map<id,sObject>();
                    imMap.putAll(lstWorkOrdersToUpdateCreated);
                    system.debug('>>>>imMap '+imMap);
                    
                    if(imMap.keySet().size() > 0){
                        update imMap.values();
                    }
                }
               // Checks if Incident list is not empty, then update the Incident status (ERROR or SUCCESS),associated with WO according to receive tracaction IDs
                if(!lstIncToUpdate.isEmpty()  && lstIncToUpdate.size() > 0)
                    Database.update(lstIncToUpdate);
                
                String returnResult = updateRemedyIntegrationHandlingRecordAfterSuccess(mapTow);
                system.debug('update Integration handling result :' + returnResult);
            }
            
        }
        catch(Exception ex){
            GlobalUtility.logMessage(GlobalConstants.ERROR_RECTYPE_NAME,'WorkOrderHandler','updateWorkOrder','','Tried to update a Work Order by code called from Platfrom Event called ticketofWOrk respect to WO Operator Action','','',ex,0);
        }
    }
    
    //BA- Created due to fact that input from platform event won't be an sObject
    public static void updateWorkOrderThroughOperatorAction(List<String> newItems) {
        system.debug('>>>>Created' +newItems);
        List<integrationWrapper.WOActionResponse> lstJSONWrap = deserializeWOActionResponse(newItems);
        system.debug('>>>>Created after deserialize' +lstJSONWrap);
        system.debug('>>>>Incident in transaction' +newItems);
        updateWorkOrderThroughOperatorAction(newItems,lstJSONWrap);      
    }
    
    public static String updateRemedyIntegrationHandlingRecordAfterSuccess(Map<String,WorkOrder> mapTow ) {
        
        List<RemedyIntegrationHandling__c> lstRemIntHandlingRecords = [Select action__c,id,incidentNumber__c,Status__c,transactionId__c,responseTimeStamp__c FROM RemedyIntegrationHandling__c WHERE action__c = 'TechOffSite' and transactionId__c in : mapTow.keyset() ];
        if(!lstRemIntHandlingRecords.isEmpty() && lstRemIntHandlingRecords.size() > 0){
			List<RemedyIntegrationHandling__c> lstRemIntHandlingRecordsToUpdate = new List<RemedyIntegrationHandling__c>(); 
            for (RemedyIntegrationHandling__c recs : lstRemIntHandlingRecords){
				 recs.Status__c = 'Success';
                 recs.responseTimeStamp__c = DateTime.now();
				 lstRemIntHandlingRecordsToUpdate.add(recs);
			}
			
			if(!lstRemIntHandlingRecordsToUpdate.isEmpty() && lstRemIntHandlingRecordsToUpdate.size() > 0){
				update lstRemIntHandlingRecordsToUpdate;
                return 'Successful';
			}
        }
        return 'Error';
	}
    
    public static List<integrationWrapper.WOActionResponse> deserializeWOActionResponse(List<string> InboundJSONMessage){
        Map<String,String> JSONLabelsMap = new Map<String,String>();
        Map<String,String> TicketOfWorkMap = new Map<String,String>();
        String ErrorCodes = '';
        List<integrationWrapper.WOActionResponse> lstJSONWrap = new List<integrationWrapper.WOActionResponse>();
        system.debug('>>>'+ InboundJSONMessage);
        for(String strJson:InboundJSONMessage){
            String JSONExc = '"exception"';
            String JSONExcrep = '"exceptions"';
            system.debug('>>>>1'+strJson);
            String JSONVal = String.valueOf(strJson);
            system.debug('>>>>2'+JSONVal );
            String replacedJSONVal = JSONVal.replace(JSONExc,JSONExcrep);   
            system.debug('>>>>3'+replacedJSONVal );  
            lstJSONWrap.add((integrationWrapper.WOActionResponse)JSON.deserialize(replacedJSONVal,integrationWrapper.WOActionResponse.Class));
            system.debug('>>>>4'+lstJSONWrap); 
        } 
        system.debug('>>>>'+lstJSONWrap);
        return lstJSONWrap;
    } 
    
}