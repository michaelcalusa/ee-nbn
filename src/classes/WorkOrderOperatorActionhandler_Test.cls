/***************************************************************************************************
Class Name:  WorkOrderOperatorActionhandler_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 28/09/2018
Function    : Test Script Used for unit testing WorkOrderOperatorActionhandler
Modification Log :
* Developer                   Date                   Description
* ---------------------------------------------------------------------------------------------------                 
  Lokesh Agarwal	         28/09/2018              Created - Version 1.0 
****************************************************************************************************/
@isTest()
public class WorkOrderOperatorActionhandler_Test {

    
    @testSetup static void loadTestDataForManageAppointmentClass() 
    {
        Id incidentManagementrecordTypeId = schema.sobjecttype.Incident_Management__c.getrecordtypeinfosbyname().get('Incident Management FTTN/B').getRecordTypeId();
        Id strWorkRequestRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Work Order').getRecordTypeId();
        List<Incident_Management__c> lstIncidentManagementRecords = new List<Incident_Management__c>();
        List<WorkOrder> lstWorkOrderRecords = new List<WorkOrder>();
        
        List<RemedyIncidentDetailsJSONtoSFFields__c> lstRID = new List<RemedyIncidentDetailsJSONtoSFFields__c>();
        RemedyIncidentDetailsJSONtoSFFields__c obj = new RemedyIncidentDetailsJSONtoSFFields__c();
        obj.Name = 'wrqStatus';
        obj.JSONKey__c = 'wrqStatus';
        obj.Parent_JSON_Key__c  = 'wrqData';
        obj.Object__c = 'WorkOrder';
        lstRID.add(obj); 
        
        obj = new RemedyIncidentDetailsJSONtoSFFields__c();
        obj.Name = 'wrqIdentifier';
        obj.JSONKey__c = 'wrqIdentifier';
        obj.Parent_JSON_Key__c  = 'wrqData';
        obj.Object__c = 'WorkOrder';
        lstRID.add(obj);
        insert lstRID;
        
        WorkOrder currentWorkOrder;
        Incident_Management__c incidentManagementRecord;
        for(Integer i = 0; i < 10; i++)
        {
             incidentManagementRecord = new Incident_Management__c();
             incidentManagementRecord.Name = 'INC0000025985' + i;
             incidentManagementRecord.RecordTypeId = incidentManagementrecordTypeId;
             incidentManagementRecord.assignee__c = 'TestUser';
             incidentManagementRecord.AppointmentId__c = 'APT10000053317' + i;
             incidentManagementRecord.User_Action__c = 'createWorkRequest';
             incidentManagementRecord.Awaiting_Ticket_of_Work__c = true;
             if(i / 2 == 0)
             {
                 incidentManagementRecord.Prod_Cat__c = 'NCAS-FTTN';
                 incidentManagementRecord.Engagement_Type__c = 'Commitment';
                 incidentManagementRecord.Target_Commitment_Date__c = System.Now();
             }
             else
                 {
                     incidentManagementRecord.Prod_Cat__c = 'NCAS-FTTB';
                     incidentManagementRecord.Engagement_Type__c = 'Appointment';
                     incidentManagementRecord.Appointment_Start_Date__c = System.Now();
                 }
             lstIncidentManagementRecords.Add(incidentManagementRecord); 
        }
        if(lstIncidentManagementRecords != null && !lstIncidentManagementRecords.Isempty())
             Insert lstIncidentManagementRecords;
        for(Integer i = 0; i < 10; i++)
        {
             currentWorkOrder = new WorkOrder();
             currentWorkOrder.AppointmentId__c = 'APT10000053317' + i;
             currentWorkOrder.Incident__c = lstIncidentManagementRecords[i].Id;
             currentWorkOrder.RecordTypeId = strWorkRequestRecordTypeId;
             currentWorkOrder.transactionId__c = 'e00746928454800' + i;
             currentWorkOrder.WO_Transaction_Status__c = 'Requested';
             if(i / 2 == 0)
             {
                 currentWorkOrder.appointmentType__c = 'Commitment';
             }
             else
             {
                 currentWorkOrder.appointmentType__c = 'Appointment';
             }
             lstWorkOrderRecords.Add(currentWorkOrder);
        }
        if(lstWorkOrderRecords != null && !lstWorkOrderRecords.Isempty())
             Insert lstWorkOrderRecords;
    }
    
    Static testMethod void testAfterInsertHandlePlatformEvent()
    {
        StaticResource resource = [SELECT Body FROM StaticResource WHERE Name='Jigsaw_WOSampleTOWResponseJson'];
        Blob listOfJsons = resource.Body;
        List<String> listOfrecords = listOfJsons.toString().split(';');
        List<ticketOfWork__e> lstticketOfWork = new List<ticketOfWork__e>();
        ticketOfWork__e ticketofRecord;
        for(string currentJson : listOfrecords)
        {
            ticketofRecord = new ticketOfWork__e();
            ticketofRecord.InboundJSONMessage__c = currentJson;
            lstticketOfWork.Add(ticketofRecord);
        }
        Test.startTest();
            List<Database.SaveResult> sr = EventBus.publish(lstticketOfWork);
        Test.StopTest();
    }
}