global class batchCommunityContactsCleanup implements Database.batchable<sObject>, Database.Stateful{
    
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        //Extracting the 2 community profiles
        List<String> pList = new List<String>{'RSP Customer Community Plus Profile','RSP Customer Community Plus Login User'};
            List<Profile> profileList = HelperUtility.getProfileByName(pList);
        //Extracting no. of days from custom label
        Integer days = Integer.valueOf(Label.Days_since_Community_user_last_logged_in); 
        DateTime lastDate = System.now().addDays(-days); 
        
        String query = '';
        if(!test.isRunningTest()){
        query += 'Select Id, ContactId, Contact.RecordTypeId, ProfileId, IsActive, IsPortalEnabled, LastLoginDate' 
            + ' FROM User WHERE IsPortalEnabled=true AND IsActive=true' 
            + ' AND ProfileId IN: profileList AND LastLoginDate < :lastDate';
        } else {
        query += 'Select Id, ContactId, Contact.RecordTypeId, ProfileId, IsActive, IsPortalEnabled, LastLoginDate' 
            + ' FROM User WHERE IsPortalEnabled=true AND IsActive=true' 
            + ' AND ProfileId IN: profileList AND LastLoginDate < :lastDate LIMIT 200';            
        }
        //System.debug(query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<User> scope){
        //System.debug('------scope'+scope);
        List<Contact> contactlist = new List<Contact>();
        Id ConRecTypID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId(); 
        for(User u : scope) {
            if(u.Contact.RecordTypeId == ConRecTypID && u.ContactId != null){
                Contact con  = new Contact(id = u.ContactId, Status__c = 'Inactive');
                contactList.add(con);
            }
        }
        if(!contactList.isEmpty()){
            Database.SaveResult[] updateResult = Database.update(contactList, false);
            for (Database.SaveResult r : updateResult){
                if (!r.isSuccess()){
                    for(Database.Error e : r.getErrors()){  
                        //System.debug('Exception' + e);                    
                        GlobalUtility.logMessage('Error','BatchCommunityContactsCleanupJob','Execute: Contact update failed','','', '','',null,0);
                    }
                }
            }
        }
        
    }
    
    
    global void finish(Database.BatchableContext BC){
        
    }
    
}