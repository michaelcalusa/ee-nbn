@isTest
public class batchCommunityContactsCleanupTest {
    
    @isTest(SeeAllData=true)
    static void TestContactsCleanupJob1() {
        test.startTest();
        Database.executeBatch(new batchCommunityContactsCleanup());
        test.stopTest();  
    }
    @isTest
    static void TestContactsCleanupJob2() {
        CommunityContactsCleanupBatch2Schedule testschedule = new CommunityContactsCleanupBatch2Schedule();
        test.startTest();
        System.schedule('CommunityCleanupJob', '0 0 * * * ?', testschedule );
        test.stopTest();
    }    
    
}