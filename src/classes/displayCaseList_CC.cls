public without sharing class displayCaseList_CC{
    public String CRModRecordId; // record ID. Could be Development record
    public String sObjectName {get;set;} // Salesforce Object Name
    public List<Case> caseList {get;set;}
    public boolean errorFound {get;set;}
    public String salesforceUrl{get;set;}
    public List<Schema.FieldSetMember> fieldSetMemberList {get;set;}
    public displayCaseList_CC(){
        fieldSetMemberList = new List<Schema.FieldSetMember> ();
        salesforceUrl = URL.getSalesforceBaseUrl().toExternalForm();
        errorFound = false;
        caseList = new List<Case> ();
        CRModRecordId =  ApexPages.currentPage().getParameters().get('CRModRecordId');
        sObjectName =  ApexPages.currentPage().getParameters().get('sObjectName');
        system.debug('sObjectName==>'+sObjectName);
        system.debug('CRModRecordId==>'+CRModRecordId);
        if(!(String.isEmpty(CRModRecordId)) && !(String.isEmpty(sObjectName))){
            if(sObjectName <> 'Site' && sObjectName <> 'Development' && sObjectName <> 'StageApplication'){
                errorFound = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'sObjectName not appropriate'));
            }
            else{
                findCaseList();
            }
        }else{
            errorFound = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'CRModRecordId OR sObjectName not provided'));
        }
    }
   @TestVisible 
    private void findCaseList(){
        try{
            // get the Field details from the Field Set
            string queryString;
            system.debug('SiteSobje=='+sObjectName);
            if(sObjectName == 'Site'){
                fieldSetMemberList =  HelperUtility.readFieldSet('FS_Case_Display_CRM_oD_MDU_CP','Case');
                string whereClause = 'MDU_CP_Site__r.MDU_CP_On_Demand_Id__c = \''+CRModRecordId+ '\' ORDER BY createdDate DESC';
                queryString = HelperUtility.generateQueryFromFieldSets('Case',fieldSetMemberList,whereClause);
            }
            if(sObjectName == 'Development'){
                fieldSetMemberList =  HelperUtility.readFieldSet('FS_Case_Display_CRM_oD_Development','Case');
                string whereClause = 'Development__r.Development_ID__c = \''+CRModRecordId+ '\' ORDER BY createdDate DESC';
                queryString = HelperUtility.generateQueryFromFieldSets('Case',fieldSetMemberList,whereClause);
            }
            if(sObjectName == 'StageApplication'){
                fieldSetMemberList =  HelperUtility.readFieldSet('FS_Case_Display_CRM_oD_Stage_App','Case');
                string whereClause = 'Stage_Application__r.Request_ID__c = \''+CRModRecordId+ '\' ORDER BY createdDate DESC';
                queryString = HelperUtility.generateQueryFromFieldSets('Case',fieldSetMemberList,whereClause);
            }
            system.debug('queryString==>'+queryString);
            caseList = Database.query(queryString);
            system.debug('caseList==>'+caseList.size());
            if(caseList.isEmpty()){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,'No Records Found in Salesforce for the provided input'));
                errorFound = true;
            }
        }catch(Exception ex){
            system.debug('Exception===>'+ex);
            errorFound = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Unable to Fetch Case detals for the provided input'));
        }
    }
}