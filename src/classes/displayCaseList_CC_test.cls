/***************************************************************************************************
Class Name:  displayCaseList_CC_test
Class Type: Test Class 
Version     : 1.0 
Created Date: 02-11-2016
Function    : This class contains unit test scenarios for displayCaseList_CC apex class.
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Hari Kalannagari	       02-11-2016                Created
****************************************************************************************************/
@isTest
private class displayCaseList_CC_test{
    
static testMethod void testmeth(){
        
        ID recordTypeId = schema.sobjecttype.Site__c.getrecordtypeinfosbyname().get('Verified').getRecordTypeId();
        Site__c site = new Site__c();
        site.Name = '100 Waverton St, Sydney, NSW 2000';
        site.Location_Id__c = 'LOC112234567890';
        site.Site_Address__c = 'siteadr';
        site.RecordTypeId = RecordTypeId;
        site.Technology_Type__c = 'asd23';
        site.Serviceability_Class__c = 1.32;
        site.Rollout_Type__c = 'sdfsdf';
        site.Unit_Number__c = 'sdfsdf';
        site.Level_Number__c = 'cvvcx';
        site.Post_Code__c = '4562';
        site.Locality_Name__c = 'vbvbg';
        site.Road_Type_Code__c = '1233';
        site.Road_Name__c = 'testroadname';
        site.Road_Number_1__c = 'vbvbg';
        site.Unit_Type_Code__c = '3343';
        site.Lot_Number__c = 'vbvhghg';
        site.Road_Suffix_Code__c = '3456';
        site.Asset_Type__c = 'assettest';
        site.Asset_Number__c = '23234';
        site.State_Territory_Code__c = 'ACT';
        site.Qualification_Date__c = Date.today();
        insert site; 
    
    	PageReference pageRef = Page.displayCaseListPage;
    	Test.setCurrentPage(pageRef);
    	ApexPages.currentPage().getParameters().put('sObjectName',site.Id);
    	displayCaseList_CC dispcslst = new displayCaseList_CC();
  
    }
    static testMethod void testmeth2(){

    	displayCaseList_CC dispcslst = new displayCaseList_CC();
        dispcslst.findCaseList();
    }
    static testMethod void testmeth0(){

    	displayCaseList_CC dispcslst = new displayCaseList_CC();
        dispcslst.sObjectName='site';
        dispcslst.findCaseList();
    }
    static testMethod void testmeth3(){

    	displayCaseList_CC dispcslst = new displayCaseList_CC();
        dispcslst.sObjectName='Development';
        dispcslst.findCaseList();
    }
    static testMethod void testmeth4(){

    	displayCaseList_CC dispcslst = new displayCaseList_CC();
        dispcslst.sObjectName='StageApplication';
        dispcslst.findCaseList();
    }
}