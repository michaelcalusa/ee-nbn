/*
    * @description: class to display results to lightening 
    * @created:
    * @params:  
    * @returns: 
    */

public class displayNetworkInventoryresults{

public static  List<Map<String, Object>> NtworkInvOutput=new List<Map<String, Object>>(); 

//method to deserialize JSON 

  public static void  processNtworkInvJSON(List<String> inboundJSONMessageList){

  for (String inboundPublishEvntVar : inboundJSONMessageList){
    Map<String, Object> subResult =new Map<String, Object>();
    subResult = (Map<String, Object>)JSON.deserializeUntyped(inboundPublishEvntVar );
    system.debug('@@subResult '+subResult );
    NtworkInvOutput.add(subResult );
  }
  system.debug('@@TNDoutput'+NtworkInvOutput);
  dispJSON();
  
  }
 //method to display in lightening  
  @auraEnabled
  public static List<Map<String, Object>> dispJSON(){
  return NtworkInvOutput;   
  }

}