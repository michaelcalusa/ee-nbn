/*
    * @description: class to display results to lightening 
    * @created:
    * @params:  
    * @returns: 
    */

public class displayTNDresults_LC{

public static  List<Map<String, Object>> TNDoutput=new List<Map<String, Object>>(); 

//method to deserialize JSON 

  public static void  processTNDJSON(List<String> inboundJSONMessageList){

  for (String inboundPublishEvntVar : inboundJSONMessageList){
    Map<String, Object> subResult =new Map<String, Object>();
    subResult = (Map<String, Object>)JSON.deserializeUntyped(inboundPublishEvntVar );
    system.debug('@@subResult '+subResult );
    TNDoutput.add(subResult );
  }
  system.debug('@@TNDoutput'+TNDoutput);
  dispJSON();
  
  }
 //method to display in lightening  
  @auraEnabled
  public static List<Map<String, Object>> dispJSON(){
  return TNDoutput;   
  }

}