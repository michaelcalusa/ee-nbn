@isTest
public class duplicateRelationShipCheckTest{
    public testMethod static void testErrors1(){
        Map<string,Id> rtypeMap = new Map<string,Id>();
        for(RecordType rt :[SELECT Id, Name FROM RecordType WHERE SobjectType = 'Account' AND Name IN 
                            ('Customer','Downstream Retail Service Provider')]){
            rtypeMap.put(rt.Name,rt.Id);
        }
        Account custAcc = new Account(Name = 'Customer Acc', RecordTypeId = rtypeMap.get('Customer'));
        Account RSPAcc = new Account(Name = 'RSP Acc', RecordTypeId = rtypeMap.get('Downstream Retail Service Provider'), Contact_No_for_Business__c = '123', Contact_No_for_Residential__c = '123');
        List<Account> acList = new List<Account>{custAcc,RSPAcc}; 
        insert acList;
        try{
            Customer_Downstream_RSP_Junction__c jun = new Customer_Downstream_RSP_Junction__c(Master__c = custAcc.Id, Retail_Service_Provider__c = RSPAcc.Id, Service_available_for_Business__c = true);
            insert jun;
            Customer_Downstream_RSP_Junction__c jun1 = new Customer_Downstream_RSP_Junction__c(Master__c = custAcc.Id, Retail_Service_Provider__c = RSPAcc.Id, Service_available_for_Business__c = true);
            insert jun1;
            
        }
        catch(exception ex){
           
        }
    }   
}