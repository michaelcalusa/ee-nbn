/* Purpose: Controller Class for the RPA Error display Lightning component
 *          Refer to Jira: https://jira.nbnco.net.au/browse/CUSTSA-14476 for elaborate information
 * CreatedBy: SV
 * Change log:
 * DeveloperName    ChangeMade    ModifiedTimeStamp
 *     SV            Created          25/04/2018    
 */
public with sharing class errorsController_LC{

    @auraEnabled
    public static List<errorWrapper> getErrorForIncident(String incNum){
        List<SObject> sObjList = new List<SObject>();
        List<errorWrapper> wrapperList = new List<errorWrapper>();
        String objectName = 'RemedyIntegrationHandling__c'; 
        String tmpSTatus = 'Failed';
        sObjList = database.query('SELECT'+remedyRPAErrorHandlingUtil.prepareFieldSet(objectName)+' FROM '+objectName+' WHERE incidentNumber__c =:incNum AND Status__c =:tmpSTatus');
        for(SObject so : sObjList) {
            if(so.get('InboundJSONMessage__c') != null && 
               so.get('incidentNumber__c') != null && 
               so.get('OutboundJSONMessage__c') != null)
                wrapperList.add(deserializeRPAErrorJSON((String)so.get('InboundJSONMessage__c'), 
                                                        (String)so.get('incidentNumber__c'), 
                                                        (String)so.get('Id'), 
                                                        (String)so.get('salesForceUser__c'), 
                                                        (String)so.get('action__c'), 
                                                        (String)so.get('roboticUserName__c'), 
                                                        (String)so.get('OutboundJSONMessage__c'),
                                                        (Decimal)so.get('Retry_Counter__c')
                                                       )
                                );
        }
        if(wrapperList.size() > 0)
            return wrapperList;
        else
            return null;
    }
    
    @auraEnabled
    public static List<errorWrapper> getErrorsForAllIncidents(){
        List<SObject> sObjList = new List<SObject>();
        List<errorWrapper> wrapperList = new List<errorWrapper>();
        String objectName = 'RemedyIntegrationHandling__c';
        String tmpSTatus = 'Failed';
        sObjList = database.query('SELECT'+remedyRPAErrorHandlingUtil.prepareFieldSet(objectName)+' FROM '+objectName+' WHERE Status__c =:tmpSTatus');
        for(SObject so : sObjList) {
            if(so.get('InboundJSONMessage__c') != null && 
               so.get('incidentNumber__c') != null && 
               so.get('OutboundJSONMessage__c') != null)
                wrapperList.add(deserializeRPAErrorJSON((String)so.get('InboundJSONMessage__c'),
                                                        (String)so.get('incidentNumber__c'), 
                                                        (String)so.get('Id'), 
                                                        (String)so.get('salesForceUser__c'), 
                                                        (String)so.get('action__c'), 
                                                        (String)so.get('roboticUserName__c'), 
                                                        (String)so.get('OutboundJSONMessage__c'),
                                                        (Decimal)so.get('Retry_Counter__c')
                                                       )
                                );
        }
        if(wrapperList.size() > 0)
            return wrapperList;
        else
            return null;
    }
    
    @AuraEnabled
    public static List<Incident_Management__c> updateIncident(String recordId, 
                                                              Boolean isRetry, 
                                                              Boolean awaitingCurrentIncStatus,
                                                              Boolean awaitingCurrentSLAStatus){
        List<SObject> sObjList = new List<SObject>();
        List<RemedyIntegrationHandling__c> upsertRIHList = new List<RemedyIntegrationHandling__c>();
        List<errorWrapper> wrapperList = new List<errorWrapper>();
        //variable to capture the list of incident numbers that will need to updated when Retry is performed
        List<String>  incNumList = new List<String>();
        // create a list of remedy outbound platform events
        List<RemedyOutboundIntegration__e> outboundJSONEventList = new List<RemedyOutboundIntegration__e>();
        //List of incidents that will need to updated for Spinners to Kick off on JIGSAW screen
        List<Incident_Management__c> incMgmtListToUpdate = new List<Incident_Management__c>();
        String objectName = 'RemedyIntegrationHandling__c';
        sObjList = database.query('SELECT'+remedyRPAErrorHandlingUtil.prepareFieldSet(objectName)+' FROM '+objectName+' WHERE Id =:recordId');
        for(SObject so : sObjList){
            so.put('Status__c','Success');
            if((String) so.get('incidentNumber__c') != Null){
                incNumList.add((String) so.get('incidentNumber__c'));
            }
            if(isRetry){
                // increment the counter
                Decimal tmpCounter = (so.get('Retry_Counter__c') != null) ? (Decimal) so.get('Retry_Counter__c') : 0.0;
                Decimal maxCounter = Decimal.valueOf(Label.JIGSAW_Retry_Counter);
                //check for the maximum # of times user can retry
                //Future: Add a condition to validate the user action re-tried and specific error codes
                if(tmpCounter < maxCounter) {
                    upsertRIHList.add((RemedyIntegrationHandling__c) so);
                    
                } else{
                    throw new AuraHandledException('You have exhausted retry attempts');
                }
            }
        }
        if(isRetry && !upsertRIHList.isEmpty()){
            createRetryRecords(upsertRIHList);
        }else{
            Database.UPDATE(sObjList);
        }
        if(!incNumList.isEmpty()){
            for(Incident_Management__c inc : [SELECT RPA_Error_Occurred__c,
                                                     Awaiting_Current_SLA_Status__c,
                                                     Awaiting_Current_Incident_Status__c, 
                                                     DynamicToastForIncident__c, 
                                                     RPA_Retry_Counter__c,
                                                     Recent_Action_Triggered_Time__c, 
                                                     User_Action__c 
                                              FROM Incident_Management__c 
                                              WHERE Incident_Number__c IN :incNumList]){
                inc.RPA_Error_Occurred__c = false;
                //on retry set the boolean values to fire the triggers
                if(isRetry){
                    inc.Awaiting_Current_SLA_Status__c = awaitingCurrentSLAStatus;
                    inc.Awaiting_Current_Incident_Status__c = awaitingCurrentIncStatus;
                    inc.RPA_Retry_Counter__c = (inc.RPA_Retry_Counter__c != Null) ? inc.RPA_Retry_Counter__c + 1 :
                                                                                    1;
                    inc.Recent_Action_Triggered_Time__c = System.now();
                }
             	incMgmtListToUpdate.add(inc);
            }
            //update the incidents
            if(!incMgmtListToUpdate.isEmpty()){
                IncidentManagementTriggerHandler.TriggerDisabled = true;
                List<Database.SaveResult> srList = Database.UPDATE(incMgmtListToUpdate,false);
                IncidentManagementTriggerHandler.TriggerDisabled = false;
            }
        }
        return incMgmtListToUpdate;
   }
    
    //method to update the rih records for Retry operation
    public static void createRetryRecords(List<RemedyIntegrationHandling__c> RIHListToUpdate){
        List<SObject> sObjList = new List<SObject>();
        List<RemedyIntegrationHandling__c> upsertRIHList = new List<RemedyIntegrationHandling__c>();
        List<errorWrapper> wrapperList = new List<errorWrapper>();
        // create a list of remedy outbound platform events
        List<RemedyOutboundIntegration__e> outboundJSONEventList = new List<RemedyOutboundIntegration__e>();
        //Set to capture the error codes list from a custom label to allow retry basing on specific error codes 
        Set<String> possibleErrorCodes =  new Set<String>();
        possibleErrorCodes.addAll(Label.JIGSAW_Retry_Error_Codes.split(';'));
        for(RemedyIntegrationHandling__c so : RIHListToUpdate){
            //code snippet to check for error codes: auto retry should be enabled only for specific error codes
            String inJSON = (String) so.get('InboundJSONMessage__c');
            String errorCode;
            if(String.isNotBlank(inJSON)){
                Map<String, Object> tmpInJSONresp = (Map<String, Object>) JSON.deserializeUntyped(inJSON);
                if(tmpInJSONresp.containsKey('errors')) {
                    for (Object instance : (List<Object>)tmpInJSONresp.get('errors')){
                        Map<String, Object> errorResp = (Map<String, Object>)instance;
                        if(errorResp.containsKey('errorCode')){
                            errorCode = (String)errorResp.get('errorCode');
                            break;
                        }
                    }
                }
            }
            //end of code snippet to check for error codes
            so.put('Status__c','Success');
            // increment the counter
            Decimal tmpCounter = (so.get('Retry_Counter__c') != null) ? (Decimal) so.get('Retry_Counter__c') : 0.0;
            Decimal maxCounter = Decimal.valueOf(Label.JIGSAW_Retry_Counter);
            tmpCounter =  tmpCounter + 1;
            //check for the maximum # of times user can retry
            //Future: Add a condition to validate the user action re-tried and exclude specific error codes based on custom label
            if(String.isNotBlank(errorCode) && 
               possibleErrorCodes.contains(errorCode)
              ){
                //don't retry
            }
            else if( tmpCounter <= maxCounter ) {
                // when re-tried from UI, create a new integration handling record
                RemedyIntegrationHandling__c sobj = new RemedyIntegrationHandling__c();
                sobj.put('transactionId__c', IntegrationUtility.newGuid(''));
                if((String) so.get('incidentNumber__c') != Null){
                    sobj.put('incidentNumber__c', (String) so.get('incidentNumber__c'));
                }
                String tmpJSON = (String) so.get('OutboundJSONMessage__c');
                Map<String, Object> tmpInJSONresp = (Map<String, Object>) JSON.deserializeUntyped(tmpJSON);
                if(tmpInJSONresp.containsKey('transactionId')){
                    tmpInJSONresp.put('transactionId', sobj.get('transactionId__c'));
                }
                tmpJSON = JSON.serialize(tmpInJSONresp);
                //create an instance of platform event to trigger the RPA action
                RemedyOutboundIntegration__e outboundEvent = new RemedyOutboundIntegration__e();
                outboundEvent.OutboundJSONMessage__c = tmpJSON;
                outboundJSONEventList.add(outboundEvent);
                sobj.put('OutboundJSONMessage__c', tmpJSON);
                sobj.put('action__c', (String) so.get('action__c'));
                sobj.put('incidentNumber__c', (String) so.get('incidentNumber__c'));
                sobj.put('salesForceUser__c', (String) so.get('salesForceUser__c'));
                sobj.put('InboundJSONMessage__c', '');
                sobj.put('Status__c','Awaiting Response');
                sobj.put('Retry_Counter__c', tmpCounter);
                sobj.put('requestTimeStamp__c', System.now());
                upsertRIHList.add(sobj);
                upsertRIHList.add(so);
            } else {
                //do nothing
            }
        }
        if(!outboundJSONEventList.isEmpty()){
            List<Database.SaveResult> peList = EventBus.Publish(outboundJSONEventList);
            RemedyIntegrationHandler.TriggerDisabled = true;
            Database.UpsertResult[] upList = Database.UPSERT(upsertRIHList,false);
            for (Database.UpsertResult up : upList){
                if(up.isSuccess()){
                    if(up.isCreated())
                        system.debug('record created in class errorsController_LC:'+ up.getId());
                    else
                        system.debug('record updated in class errorsController_LC:'+ up.getId());
                }
                else {
                    for(Database.Error er : up.getErrors()){
                        system.debug('error Message in class errorsController_LC:'+ er.getMessage());
                        system.debug('fields which caused the error in class errorsController_LC:'+ er.getFields());
                        system.debug('error code in class errorsController_LC:'+ er.getStatusCode());
                    }
                }
            }
            RemedyIntegrationHandler.TriggerDisabled = false;
        }
    }
    
    public static errorWrapper deserializeRPAErrorJSON(String JSONStr, 
                                                       String IncidentNumber, 
                                                       String recordId, 
                                                       String sfUserName, 
                                                       String action, 
                                                       String roboticUserName, 
                                                       String outJSON,
                                                       Decimal retryCounter){
         errorWrapper eW = new errorWrapper();
         eW.retryCounter = retryCounter != Null ? retryCounter : 0.0;
         Map<String, Object> tmpInJSONresp = (Map<String, Object>) JSON.deserializeUntyped(JSONStr);
         Map<String, Object> tmpOutJSONresp = (Map<String, Object>) JSON.deserializeUntyped(outJSON);
         List<Map<String, Object>> errorJSONMapList = new List<Map<String, Object>>();
         if(tmpInJSONresp.containsKey('errors')) {
             for (Object instance : (List<Object>)tmpInJSONresp.get('errors'))
                 errorJSONMapList.add((Map<String, Object>)instance);

             for(Map<String, Object> tmpJSONMap : errorJSONMapList){
                 eW.errorType = (tmpJSONMap.containsKey('errorType')) ? (String) tmpJSONMap.get('errorType') : null;
                 eW.errorCode = (tmpJSONMap.containsKey('errorCode')) ? (String)tmpJSONMap.get('errorCode') : null;
                 eW.errorMessage = (tmpJSONMap.containsKey('errorMessage')) ? (String) tmpJSONMap.get('errorMessage') : null;
                 eW.proposedAction = (tmpJSONMap.containsKey('proposedAction')) ? (String) tmpJSONMap.get('proposedAction') : null;
                 if(action == 'relateToIncident')
                 {
                     eW.proposedAction = 'Select \'Acknowledge\' button below and try again using the \'Link Incident(s)\' Jigsaw action.';    
                 }
                 eW.errorTimeStamp = (tmpJSONMap.containsKey('errorTimeStamp')) ? (Long) tmpJSONMap.get('errorTimeStamp') : null;
                 break;
             }
             //deserialize outbound JSON response
             if(tmpOutJSONresp.containsKey('incidentData')) {
                 Map<String, Object> tmpIncidentJSONresp = (Map<String, Object>) tmpOutJSONresp.get('incidentData');
                 if(tmpIncidentJSONresp.containsKey('before')){
                     Map<String, Object> tmpBeforeJSON = (Map<String, Object>) tmpIncidentJSONresp .get('before');
                     ew.before = new Map<String, String>();
                     for(String s : tmpBeforeJSON.keySet())
                         ew.before.put(s, (String) tmpBeforeJSON.get(s));
                 }
                 if(tmpIncidentJSONresp.containsKey('after')){
                     Map<String, Object> tmpAfterJSON = (Map<String, Object>) tmpIncidentJSONresp.get('after');
                     ew.after = new Map<String, String>();
                     for(String s : tmpAfterJSON.keySet())
                         ew.after.put(s, (String) tmpAfterJSON.get(s));
                 }
                 if(tmpIncidentJSONresp.containsKey('afterWorklog')){
                     Map<String, Object> tmpAfterJSON = (Map<String, Object>) tmpIncidentJSONresp.get('afterWorklog');
                     ew.after = new Map<String, String>();
                     if(tmpAfterJSON.containsKey('worklogData')){
                        Map<String, Object> addNoteAfterJSON =  (Map<String, Object>) tmpAfterJSON.get('worklogData');
                        for(String s : addNoteAfterJSON.keySet()){
                            ew.after.put(s, (String) addNoteAfterJSON.get(s));
                        }
                     }
                 }
             }
         }
         eW.incidentNum = IncidentNumber;
         eW.errorId = recordId;
         eW.sfUserName = sfUserName;
         if(String.isNotBlank(action)){
             eW.action = action;
         }
         eW.roboticUserName = roboticUserName;
         return eW;
    }
    
    public class errorWrapper{
        @auraEnabled public String errorId;
        @auraEnabled public String incidentNum;
        @auraEnabled public String sfUserName;
        @auraEnabled public String roboticUserName;
        @auraEnabled public String action;
        @auraEnabled public String errorType;
        @auraEnabled public String errorCode;
        @auraEnabled public String errorMessage;
        @auraEnabled public String proposedAction;
        @auraEnabled public Long errorTimeStamp;
        @auraEnabled public Decimal retryCounter;
        @auraEnabled public Map<String,String> before;
        @auraEnabled public Map<String,String> after;
    }
}