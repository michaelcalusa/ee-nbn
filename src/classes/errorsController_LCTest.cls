/* Purpose: Test class for errorsController_LC Controller Class
 *          Refer to Jira: https://jira.nbnco.net.au/browse/CUSTSA-14476 for elaborate information
 * CreatedBy: SV
 * Change log:
 * DeveloperName    ChangeMade    ModifiedTimeStamp
 *     SV            Created          25/04/2018    
 */
@isTest
private class errorsController_LCTest{
    
    // method to insert test data
    @testSetup static void setupMethod() {
        insert new Incident_Management__c (Incident_Number__c='INC000006920225', recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Incident Management FTTN/B').getRecordTypeId(), Appointment_Start_Date__c=system.now(),Appointment_End_Date__c=system.now(), Target_Commitment_Date__c=system.now(),Reported_Date__c=system.now());
        insert new RemedyIntegrationHandling__c(action__c='addNote',incidentNumber__c='INC000006920225',transactionId__c='469dc3c7-9219-4982-a723-4e7e60580083',Status__c='Failed',RPA_Status__c='Failed',InboundJSONMessage__c='{"transactionId":"469dc3c7-9219-4982-a723-4e7e60580083","incidentId":"INC000006920225","action":"addNote","roboticUserName":"rpauser7","salesForceUser":"Saurabh Sharma","rpaStatus":"Failed","errors":[{"errorKey":"InternalSystemError","errorType":"TechnicalError","errorCode":"","errorMessage":"A technical error has occurred while processing the request. Triggering of Robotic Task has failed.","proposedAction":"Please validate the remedy system manually","errorTimeStamp":1525851067,"rpaRequest":null}]}',OutboundJSONMessage__c='{"transactionId":"469dc3c7-9219-4982-a723-4e7e60580083","timeStamp":"1525779465","salesForceUser":"Automated Process","incidentIdentifier":"INC000006920225","incidentData":{"afterWorklog":{"worklogData":{}}},"action":"addNote"}');
        insert new Incident_Management__c (Incident_Number__c='INC000002597718', recordTypeId = schema.SObjectType.Incident_Management__c.getRecordTypeInfosByName().get('Incident Management FTTN/B').getRecordTypeId(), Appointment_Start_Date__c=system.now(),Appointment_End_Date__c=system.now(), Target_Commitment_Date__c=system.now(),Reported_Date__c=system.now());
        insert new RemedyIntegrationHandling__c(action__c='requestNewAppoinmentFromRSP',incidentNumber__c='INC000002597718',transactionId__c='59acd895-60f3-4734-93cc-b5b622b927cd',Status__c='Failed',RPA_Status__c='Failed',InboundJSONMessage__c='{"transactionId":"59acd895-60f3-4734-93cc-b5b622b927cd","incidentId":"INC000002595834","action":"addNote","roboticUserName":"rpauser6","salesForceUser":"Automated Process","rpaStatus":"Failed","errors":[{"errorKey":"InternalSystemError","errorType":"TechnicalError","errorCode":"","errorMessage":"A technical error has occurred while processing the request. Triggering of Robotic Task has failed.","proposedAction":"Please validate the remedy system manually","errorTimeStamp":1525743520}]}',OutboundJSONMessage__c='{"transactionId":"59acd895-60f3-4734-93cc-b5b622b927cd","timeStamp":"1526053342","salesForceUser":"Saurabh Sharma","incidentIdentifier":"INC000002597718","incidentData":{"before":{"status":"In Progress","industryStatus":"InProgress"},"after":{"status":"Pending","requestSubject":"Appointment Required","pendingReasonCode":"AAIN9704 - Appointment ID does not exist","notesRSP":"New Appointment Required - The Appointment supplied with the Trouble Ticket does not exist or is invalid","industryStatus":"InProgress","federationId":"saurabhsharma","department":"Networks and Services","company":"NBNCo Operations"}},"action":"requestNewAppoinmentFromRSP"}');
    }
    private static testMethod void getErrorForIncidentTest(){
        errorsController_LC.getErrorForIncident([SELECT Id,Incident_Number__c FROM Incident_Management__c][0].Incident_Number__c);
        errorsController_LC.getErrorsForAllIncidents();
        errorsController_LC.updateIncident([SELECT Id FROM RemedyIntegrationHandling__c][0].Id, false, false, false);
        errorsController_LC.updateIncident([SELECT Id FROM RemedyIntegrationHandling__c][0].Id, true, true, true);
    }
}