/*************************************************
- Developed by: Suman Gunaganti
- Date Created: 03/04/2018 (dd/MM/yyyy)
- Description: 
- Version History:

*************************************************/
public class generateInvoiceService
{
    Private static Map<String, String> invoicetransactionTypeMap = new Map<String, String>();
    Private static Map<String, String> creditMemoTypeMap = new Map<String, String>();
    Private static Map<String, String> invoicetransactionNewDevTypeMap = new Map<String, String>();
    Private static Map<String, String> creditMemoNewDevTypeMap = new Map<String, String>();
    Private static Map<String, Map<String, String>> transactionTypeMappingsMap = new Map<String, Map<String, String>>();
    Private static Map<String, Map<String, String>> transactionTypeNewDevMappingsMap = new Map<String, Map<String, String>>();
    Private static Map<Id, oppInvoicesplitMap> OpportyInvSplitMap = new Map<Id, oppInvoicesplitMap>();
    Private static Map<Id,opportunity> OpportunityMap = new Map<Id, opportunity>();
    Private static Map<Id, set<Id>> opptyOrderidMap = new Map<Id, set<Id>>();
    Private static List<Id> orderRequestIds = new List<Id>();
    private static Map<Id, List<ID>> oppidMap = new Map<Id, List<ID>>();
    Private static map<Id, Set<Id>> orderRequestToOppty = new map<Id, Set<Id>>();
    Private static map<Id, list<csord__Order_Line_Item__c>> oReqToOLIMap = new map<Id, list<csord__Order_Line_Item__c>>();
    Private static List<sObject> objs = new List<sObject>();
    Private static ObjectCommonSettings__c objectStatuses;
    public static void generateInvoices(
        list<Id> orderReqIds, Map<Id, set<Id>> opptyOrdidMap, 
        map<Id, list<id>> opportunityOrderIdMap,
        map<Id, Set<Id>> ordReqOppty){
        
        populateTransactionTypeMaps();
        opptyOrderidMap = opptyOrdidMap;
        oppidMap = opportunityOrderIdMap;
        orderRequestIds = orderReqIds;
        orderRequestToOppty = ordReqOppty;
        objectStatuses = ObjectCommonSettings__c.getValues('Invoice Statuses');

        list<csord__Order_Line_Item__c> totalOliList = [SELECT id, name, csord__Order_Request__c, csord__Order__c, 
                csord__Order__r.csordtelcoa__Opportunity__c, csord__Order__r.csordtelcoa__Opportunity__r.ContractId, 
                csord__Order__r.csord__Identification__c, csord__Order__r.csordtelcoa__Product_Configuration__c, 
                csord__Line_Number__c, csord__Line_Description__c, Quantity__c, csord__Total_Price__c, Memo_Line__c, 
                Memo_Line_Id__c, GST__c, Solution__c, Invoice__c, Invoice_Line_Item__c, Unit_Price__c, Cost__c, Margin__c, OLI_Type__c                
            from csord__Order_Line_Item__c where csord__Order_Request__c in :orderRequestIds];
        
        for (Id opptyId : oppidMap.keySet()) {
            list<csord__Order_Line_Item__c> tmpList = new list<csord__Order_Line_Item__c>();
            for (csord__Order_Line_Item__c oli: totalOliList) {
                if (oli.csord__Order__r.csordtelcoa__Opportunity__c == opptyId) {
                    tmpList.add(oli);
                }
            }
            oReqToOLIMap.put(opptyId, tmpList);
            //oReqToOppMap.put(orderRequestId, OpportunityMap.get(oppidMap.get(orderRequestId)));
        }
        OpportunityMap = new Map<Id,opportunity>([select Id, Name, AccountId, ContractId, Amount, Type, 
                                      Mode_of_Payment__c, Customer_Class__c, Account_Address_Record_Type__c, 
                                      Stage_Application__r.Reference_Number__c, Stage_Application__r.Development__c, 
                                      Stage_Application__r.Development__r.Name, Stage_Application__r.Invoice_Split__c, 
                                      contract.RecordType.Name, RecordTypeId, RecordType.Name,OwnerId,
                                      Contract.BillingPostalCode from Opportunity where Id IN :opptyOrderidMap.keySet()]);
        invoiceSplitMap(OpportunityMap.values());
        createInvoices();
    }
    public static void createInvoices(){
        Map<String, Schema.RecordTypeInfo> solutionRecordTypeInfo  = Schema.SObjectType.csord__Solution__c.getRecordTypeInfosByName();
        for (Id orderRequestId : orderRequestIds) {
            for(Id opptyID:orderRequestToOppty.get(orderRequestId)){
                oppInvoicesplitMap oppSplitObj = OpportyInvSplitMap.get(opptyID);
                // Generate a solution to group the Delivery Order and Order Line Items - one solution per order request
                csord__Solution__c solution = new csord__Solution__c (
                Name = OpportunityMap.get(opptyID).Name
                , csord__Account__c = OpportunityMap.get(opptyID).AccountId
                , Opportunity__c = OpportunityMap.get(opptyID).Id
                , csord__Order_Request__c = orderRequestId
                , csord__Status__c = objectStatuses.New__c//'New'
                , Status__c = objectStatuses.New__c//'New'
                , csord__Identification__c = orderRequestId
                , csord__Type__c = transactionTypeMappingsMap.get('Invoice').get(OpportunityMap.get(opptyID).RecordType.Name)
                , OwnerId = OpportunityMap.get(opptyID).OwnerId
                );
                system.debug('record type' + OpportunityMap.get(opptyID).RecordType.Name);
                if(OpportunityMap.get(opptyID).RecordType.Name == 'Recoverable Damage') {
                    solution.RecordTypeId = solutionRecordTypeInfo.get('Recoverable Damage').getRecordTypeId();
                } else {
                    solution.RecordTypeId = solutionRecordTypeInfo.get('Recoverable Works').getRecordTypeId();
                }
                
                if (OpportunityMap.get(opptyID).RecordType.Name == 'New Development LCO'|| OpportunityMap.get(opptyID).RecordType.Name == 'New Development SCO') {
                   solution.csord__Type__c = transactionTypeNewDevMappingsMap.get('Invoice').get(OpportunityMap.get(opptyID).contract.RecordType.Name);
                   solution.Status__c = 'Created';
                   solution.recordtypeId = solutionRecordTypeInfo.get('New Developments').getRecordTypeId();
                }
                //Start - Code Added for US 3314 
                if(OpportunityMap.get(opptyID).RecordType.Name.equalsIgnoreCase('Standard') ){
                        solution.RecordTypeId = solutionRecordTypeInfo.get('Standard').getRecordTypeId();
                        if(OpportunityMap.get(opptyID).Contract.RecordType.Name.equalsIgnoreCase('Dunning')){
                            solution.csord__Type__c = OpportunityMap.get(opptyID).RecordType.Name;
                        }
                }
                //End Code Added for US 3314
                insert solution;
            List<Invoice__c> invlist = new List<Invoice__c>();
            Id recordtypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Invoice').getRecordTypeId();
            // create Invoice
            for(Integer i=0; i<oppSplitObj.NO_OF_INVOICES; i++){
                Invoice__c invoice = new Invoice__c(
                Name = objectStatuses.Invoice_Name__c//'To be issued'
                , Opportunity__c = OpportunityMap.get(opptyID).Id
                , Account__c = OpportunityMap.get(opptyID).AccountId
                , Contract__c = OpportunityMap.get(opptyID).ContractId
                , Status__c = objectStatuses.Requested__c//'Requested'
                , Solution__c = solution.Id
                , Payment_Status__c = objectStatuses.Payment_Status_UnPaid__c//'Unpaid'
                , OwnerId = OpportunityMap.get(opptyID).OwnerId
                , Transaction_Type__c = transactionTypeMappingsMap.get('Invoice').get(OpportunityMap.get(opptyID).RecordType.Name)
                , recordtypeId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Invoice').getRecordTypeId()
                );
                // Start - Recordtype Check for Standard is added - Us 3314
                if(OpportunityMap.get(opptyID).RecordType.Name == 'New Development LCO'
                    || OpportunityMap.get(opptyID).RecordType.Name == 'New Development SCO'
                    || OpportunityMap.get(opptyID).RecordType.Name.equalsIgnoreCase('Standard')) {
                    System.debug('>>>>>>> Opty record type: newdev lco / sco');
                    invoice.Transaction_Type__c = transactionTypeNewDevMappingsMap.get('Invoice').get(OpportunityMap.get(opptyID).contract.RecordType.Name);
                    invoice.Development__c  = OpportunityMap.get(opptyID).Stage_Application__r.Development__c;
                    invoice.Stage_Application__c = OpportunityMap.get(opptyID).Stage_Application__c;
                    System.debug('>>>>>>> inside if - invoiceaux:'+transactionTypeNewDevMappingsMap.get('Invoice').get(OpportunityMap.get(opptyID).contract.RecordType.Name));
                }
                if(OpportunityMap.get(opptyID).RecordType.Name.equalsIgnoreCase('Standard')) {
                    System.debug('>>>>>>> Opty record type: newdev lco / sco');
                    invoice.Transaction_Type__c = transactionTypeNewDevMappingsMap.get('Invoice').get(OpportunityMap.get(opptyID).contract.RecordType.Name);
                }
                //End- Recordtype Check for Standard is added - Us 3314
                if(OpportunityMap.get(opptyID).Mode_of_Payment__c == 'Credit Card'){
                    invoice.BPoint_Ref__c = OpportunityMap.get(opptyID).Stage_Application__r.Reference_Number__c;
                    invoice.Paid_Upfront__c = TRUE;
                }
                if(oppSplitObj.INVOICE_SPLIT == TRUE && i != 0){
                    invoice.Status__c = objectStatuses.New__c;//New
                }
                invlist.add(Invoice);
           }
            Database.SaveResult[] invsrList = Database.insert(invlist, false);
            List<Id> invIds = new List<Id>();
            for (Database.SaveResult sr : invsrList) if(sr.isSuccess())invIds.add(sr.getId());         

             // get PC attributes
            set<Id> pcSet = new set<Id>();
            list<csord__Order_Line_Item__c> oliList = oReqToOLIMap.get(opptyID);
            
            system.debug('***oliList:'+oliList);
            
            for (csord__Order_Line_Item__c oli: oliList) {
                pcSet.add(oli.csord__Order__r.csordtelcoa__Product_Configuration__c);
            }
            
            map<string, list<cscfga__Attribute__c>> attrMap = new map<string, list<cscfga__Attribute__c>>();
            list<cscfga__Attribute__c> attrList = new list<cscfga__Attribute__c>([SELECT id, name, cscfga__Product_Configuration__c, cscfga__Value__c 
                        from cscfga__Attribute__c where cscfga__Product_Configuration__c = :pcSet]);
            for (Id pcId: pcSet) {
                list<cscfga__Attribute__c> tmpList = new list<cscfga__Attribute__c>();
                for (cscfga__Attribute__c attr: attrList) {
                    if (attr.cscfga__Product_Configuration__c == pcId) {
                        tmpList.add(attr);
                    }
                    attrMap.put(pcId, tmpList);
                }
            }
            system.debug('***attrMap:' + attrMap);
            
            // create invoice line items and update order line items
            list<Invoice_Line_Item__c> iliList = new list<Invoice_Line_Item__c>();
            Id devRecordTypeId = Schema.SObjectType.Invoice_Line_Item__c.getRecordTypeInfosByName().get('Invoice Line Item').getRecordTypeId();
            for (csord__Order_Line_Item__c oli: oliList) {
                //populate order list items with pc attributes
                for (cscfga__Attribute__c attr: attrMap.get(oli.csord__Order__r.csordtelcoa__Product_Configuration__c)) {
                    if (attr.Name == objectStatuses.Attr_Name_Quantity__c/*'Quantity'*/) {
                        oli.Quantity__c = Decimal.valueOf(attr.cscfga__Value__c);
                    }
                    if (attr.Name == objectStatuses.Attr_Name_Cost__c/*'Cost'*/) {
                        oli.Cost__c = Decimal.valueOf(attr.cscfga__Value__c);
                    }
                    if (attr.Name == objectStatuses.Attr_Name_Margin__c/*'Margin'*/) {
                        oli.Margin__c = Decimal.valueOf(attr.cscfga__Value__c);
                    }
                    if (attr.Name == objectStatuses.Attr_Name_MemoLineId__c/*'Memo Line Id'*/) {
                        oli.Memo_Line_Id__c = (!String.isBlank(attr.cscfga__Value__c)) ? Decimal.valueOf(attr.cscfga__Value__c) : oli.Memo_Line_Id__c;
                    }
                }
                
                oli.Unit_Price__c = oli.Quantity__c == null ? oli.csord__Total_Price__c : oli.csord__Total_Price__c/oli.Quantity__c;
                
                // Populate the Solution field and Invoice field in OLI
                oli.Solution__c = solution.Id;
                oli.Invoice__c = invsrList[0].getId();
                
                system.debug('***oli:'+oli);
                system.debug('***invoice.id:'+invsrList[0].getId());

                Boolean incGst = false;
                if(oli.GST__c == objectStatuses.GST_Inclusive__c)incGst = true;
                if(incGst)
                {
                    incGst = isGstInclusive(OpportunityMap.get(opptyID).Contract.BillingPostalCode);
                }
                if(incGST)oli.GST__c = objectStatuses.GST_Inclusive__c;
                else oli.GST__c = objectStatuses.GST_Exclusive__c;

                //Check if need to update oli.GST__c

                
                // create invoice line item
                for(integer i=0; i<oppSplitObj.NO_OF_INVOICES; i++){
                    Invoice_Line_Item__c invoiceLineItem = new Invoice_Line_Item__c
                    ( Name = oli.csord__Line_Number__c
                    , Line_Number__c = (OpportunityMap.get(opptyID).RecordType.Name == 'New Development LCO'|| OpportunityMap.get(opptyID).RecordType.Name == 'New Development SCO')?  OpportunityMap.get(opptyID).Stage_Application__r.Development__r.Name : oli.csord__Line_Number__c
                    , Description__c = oli.csord__Line_Description__c
                    , Quantity__c = oli.Quantity__c
                    , Unit_Price__c = oli.Unit_Price__c
                    , Memo_Line__c = oli.Memo_Line__c
                    , Memo_Line_Id__c = oli.Memo_Line_Id__c
                    , Inc_GST__c = (oli.GST__c == objectStatuses.GST_Inclusive__c/*'GST (Inclusive)'*/) ? true : false
                    , Tax_Code__c = (oli.GST__c == objectStatuses.GST_Inclusive__c/*'GST (Inclusive)'*/) ? objectStatuses.Tax_Code_Non_Capital__c/*'GST 10% (NON-CAPITAL)'*/ : objectStatuses.Tax_Code_Exempt__c/*'GST NIL (EXEMPT OR N/A)'*/ 
                    , Invoice__c = invsrList[i].getId()
                    , Order_Line_Item__c = oli.Id
                    , Order__c = oli.csord__Order__c
                    , ILI_Type__c = oli.OLI_Type__c
                    );
                    invoiceLineItem.recordtypeId = devRecordTypeId;
                if (oppSplitObj.INVOICE_SPLIT == TRUE && oppSplitObj.NO_OF_INVOICES >= 1 && oppSplitObj.InvoiceSplitMap != NULL) {
                    invoiceLineItem.Invoice_Split_Percentage__c = oppSplitObj.InvoiceSplitMap.get(i+1);
                    invoiceLineItem.Description__c = oli.csord__Line_Description__c +' ('+oli.Quantity__c+' @ $'+(Integer)oli.Unit_Price__c+')';
                    //invoiceLineItem.recordtypeId = devRecordTypeId;
                    if(OpportunityMap.get(opptyID).Type == 'Backhaul')
                        invoiceLineItem.Description__c = oli.csord__Line_Description__c;
                    if(oppSplitObj.NO_OF_INVOICES == 2 && i==1){
                        invoiceLineItem.Description__c = invoiceLineItem.Description__c + ' - Final '+'('+oppSplitObj.InvoiceSplitMap.get(i+1)+'%)';
                    }
                    else if(oppSplitObj.NO_OF_INVOICES == 1){
                        invoiceLineItem.Description__c = invoiceLineItem.Description__c + ' - '+ '('+oppSplitObj.InvoiceSplitMap.get(i+1)+'%)';
                    }  
                    else {
                        invoiceLineItem.Description__c = invoiceLineItem.Description__c + ' - Upfront '+ '('+oppSplitObj.InvoiceSplitMap.get(i+1)+'%)';
                    }  
                }
               
                //
                system.debug('***invoiceLineItem:' + invoiceLineItem);
                iliList.add(invoiceLineItem);
                    
                }
            }
            // insert invoice line item list
            system.debug('***iliList:' + iliList);
            insert iliList;
            
            list<Invoice_Line_Item__c> iliListAux = [SELECT Id, Order_Line_Item__c from Invoice_Line_Item__c where Order_Line_Item__c in:oliList];
            system.debug('***iliListAux:' + iliListAux);
            for (csord__Order_Line_Item__c oli: oliList) {
                for (Invoice_Line_Item__c ili: iliListAux) {
                    oli.Invoice_Line_Item__c = ili.Id;    
                }
            }
            objs.addAll((List<sObject>)oliList);
            List<Invoice__c> invoiceList = [SELECT id, Record_Type_Name__c, Transaction_Type__c, Unpaid_Balance__c, Development__c, Stage_Application__c from Invoice__c where Id IN :invIds];
            for(Invoice__c inv: invoiceList){
                Decimal unpaidBalance = 0;
                for(Invoice_Line_Item__c invLineItems:[select Id, Amount__c from Invoice_Line_Item__c where Invoice__c = :inv.Id]){
                    unpaidBalance = unpaidBalance + invLineItems.Amount__c;
                }
                inv.Unpaid_Balance__c = unpaidBalance;
            }
            objs.addAll((List<sObject>)invoiceList);
            // Populate the Solution field in Order
            list<csord__Order__c> orders = [Select Id, csord__Solution__c from csord__Order__c where Id IN :opptyOrderidMap.get(opptyID)];
            for (csord__Order__c ord: orders) {
                ord.csord__Solution__c = solution.Id;
            }
            objs.addAll((List<sObject>)orders);
        }
       }
          if(!objs.isEmpty())Database.SaveResult[] srList = Database.update(objs, false);
    }
    public static void populateTransactionTypeMaps(){
        for (Transaction_Type_Mapping__mdt ttm : [SELECT id, Credit_Memo_Transaction_Type__c, 
                                                    Invoice_Transaction_Type__c, Opportunity_Record_Type__c, 
                                                    Account_Address_Record_Type__c from Transaction_Type_Mapping__mdt]) {
        
            invoicetransactionTypeMap.put(ttm.Opportunity_Record_Type__c, ttm.Invoice_Transaction_Type__c);
            creditMemoTypeMap.put(ttm.Opportunity_Record_Type__c, ttm.Credit_Memo_Transaction_Type__c);
            invoicetransactionNewDevTypeMap.put(ttm.Account_Address_Record_Type__c, ttm.Invoice_Transaction_Type__c);
            creditMemoNewDevTypeMap.put(ttm.Account_Address_Record_Type__c, ttm.Credit_Memo_Transaction_Type__c);
        }
        transactionTypeMappingsMap.put('Invoice', invoicetransactionTypeMap);
        transactionTypeMappingsMap.put('Credit Memo', creditMemoTypeMap);
        transactionTypeNewDevMappingsMap.put('Invoice', invoicetransactionNewDevTypeMap);
        transactionTypeNewDevMappingsMap.put('Credit Memo', creditMemoNewDevTypeMap);
        
    }
    public static Boolean isGstInclusive(String strPostCode) {
        Boolean isGstInclusive = true;
        Map<String,Tax_Exempted_Post_Codes__c> mapExemptedPostCodes = Tax_Exempted_Post_Codes__c.getAll();

        //If Post Code is in the exempted list, return false.
        if(!String.isEmpty(strPostCode) &&  mapExemptedPostCodes.containsKey(strPostCode) && (mapExemptedPostCodes.get(strPostCode)).Name == strPostCode)
        {
            isGstInclusive = false;
        }

        return isGstInclusive;
    }
    public static void invoiceSplitMap(List<opportunity> Opptys) {
        Map<Id, Id> splitIds = new Map<Id, Id>();
        Map<Id, List<Invoice_Split_Item__c>> splitIdmap = new Map<Id, List<Invoice_Split_Item__c>>();
        for(Opportunity oppty:Opptys){
            if(oppty.Stage_Application__r.Invoice_Split__c!= Null)splitIds.put(oppty.Id, oppty.Stage_Application__r.Invoice_Split__c);
        }
        for(Invoice_Split_Item__c splitItem: [Select Percentage__c, Sequence__c, Invoice_Split__c From Invoice_Split_Item__c Where Invoice_Split__c =:splitIds.values() ORDER BY Sequence__c ASC NULLS LAST]){
          if(splitIdmap.containsKey(splitItem.Invoice_Split__c)) 
              splitIdmap.get(splitItem.Invoice_Split__c).add(splitItem);
            else
                splitIdmap.put(splitItem.Invoice_Split__c, new List<Invoice_Split_Item__c> {splititem});
        }
        for(Opportunity oppty:Opptys){
            if(oppty.Stage_Application__r.Invoice_Split__c != Null && (oppty.Type == 'Construction' || oppty.Type == 'Backhaul')){
                Map<Decimal, Decimal> InvSplitMap = new Map<Decimal, Decimal>();
                for(Invoice_Split_Item__c invItem: splitIdmap.get(splitIds.get(oppty.id))){
                    InvSplitMap.put(invItem.Sequence__c,invItem.Percentage__c);
                }
                OpportyInvSplitMap.put(oppty.id, new oppInvoicesplitMap(InvSplitMap.size(), TRUE, InvSplitMap));
            }
            else
              OpportyInvSplitMap.put(oppty.id, new oppInvoicesplitMap(1,FALSE,NULL));

            }
     }
    class oppInvoicesplitMap{
        Integer NO_OF_INVOICES {get;set;}
        boolean INVOICE_SPLIT {get;set;}
        Map<Decimal, Decimal> InvoiceSplitMap = new Map<Decimal, Decimal>();
        public oppInvoicesplitMap(Integer noi, Boolean isinvoiceSplit, Map<Decimal, Decimal> splitMap){
            this.NO_OF_INVOICES = noi;
            this.INVOICE_SPLIT = isinvoiceSplit;
            this.InvoiceSplitMap = splitMap;
        }
    }

}