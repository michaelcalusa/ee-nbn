/*************************************************
- Developed by: Suman Gunaganti
- Date Created: 03/04/2018 (dd/MM/yyyy)
- Description: asynchronous invoice generation  
- Version History:

*************************************************/
public class generateInvoicesAsync implements Queueable{
    private Map<Id, List<id>> oppidMap = new Map<Id, List<ID>>();
    private Map<Id, set<Id>> opptyOrderidMap = new Map<Id, set<Id>>();
	private map<Id, Set<Id>> orderRequestToOppty = new map<Id, Set<Id>>();
    private List<Id> orderRequestIds = new List<Id>();
    private List<Id> orderIds = new List<Id>();
    public generateInvoicesAsync(list<Id> orderReqIds, Map<Id, set<Id>>opptyOrderidMap, Map<Id, List<id>> oppidMap, map<Id, Set<Id>> orderRequestToOppty){
        this.orderRequestIds = orderReqIds;
        this.opptyOrderidMap = opptyOrderidMap;
        this.oppidMap = oppidMap;
        this.orderRequestToOppty = orderRequestToOppty;
     }
    public void execute (QueueableContext context){
        generateInvoiceService.generateInvoices(orderRequestIds,opptyOrderidMap,oppidMap,orderRequestToOppty);
    }
    
}