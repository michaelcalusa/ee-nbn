/***************************************************************************************************
Class Name:         incidentWrapper
Class Type:         WrapperClass 
Company :           COntractor
Version:            1.0 
Created Date:       13 Mar 2018
Function:           
Input Parameters:   None 
Output Parameters:  None
Description:        Wrapper class to wrap incident data to send back to lightning components. Created wrapper class as a number of methods will need to use the wrapper 
Modification Log:
* Developer          Date             Description
* --------------------------------------------------------------------------------------------------                 
* Beau Anderson      13/03/2018      Created - Version 1.0 
****************************************************************************************************/ 
public class incidentWrapper{
    public class inmData{
        @AuraEnabled
        public Incident_Management__c inm;
        @AuraEnabled
        public String AppStartdt;
        @AuraEnabled
        public String AppEnddt;
        @AuraEnabled
        public String CmtDt;
        @AuraEnabled
        public String ResDt;
        @AuraEnabled
        public string Current_status;
        @AuraEnabled
        public string Current_status_msg;
        @AuraEnabled
        public string Details;
        @AuraEnabled
        public boolean displayDateTimeSlot;
        @AuraEnabled
        public String Information_Summary;
        @AuraEnabled
        public boolean isResolved;
        @AuraEnabled
        public Boolean hasWRQ;
        @AuraEnabled
        public Boolean isLoggedInUserEqualsAssignee;
        @AuraEnabled
        public string suggestedAction;
        @AuraEnabled
        public string incidentNo;
        @AuraEnabled
        public string FederationIdentifier;
        @AuraEnabled
        public string pendingMessageDetails;
        @AuraEnabled
        public string resolvedMessageDetails;
        @AuraEnabled
        public string tier1Msg;
        @AuraEnabled
        public string tier2Msg;
        @AuraEnabled
        public string tier3Msg;
        @AuraEnabled
        public date resolutionDate;
        @AuraEnabled
        public boolean awaitingResponseIncident;
        @AuraEnabled
        public boolean awaitingResponseSLA;
        @AuraEnabled
        public String DynamicToastMsg;
        @AuraEnabled
        public String DynamicIncidentSubMsg;
        @AuraEnabled
        public Boolean displayAssignToMe;
        @AuraEnabled
        public Boolean currentUserGroupName;
        @AuraEnabled
        public String userFederation;
        @AuraEnabled
        public String currentAssignee;
        @AuraEnabled
        public Boolean isDiffAssignee;
        @AuraEnabled
        public Boolean RPAErrorOccured;
        @AuraEnabled
        public String UserActionValue;
        @AuraEnabled
        public String EngagementType;
        @AuraEnabled
        public String priorityAssist;
        @AuraEnabled
        public String pendingGroupName;//CUSTSA-2755 ADDED BY RM
        
        public inmdata setValues(inmdata inm, String EngagementType, String priorityAssist, String pendingGroupName){
            inm.EngagementType = EngagementType;
             inm.priorityAssist = priorityAssist;
             inm.pendingGroupName = pendingGroupName;//CUSTSA-2755 ADDED BY RM
            return inm;
        }


        public inmData(Incident_Management__c inm,String AppStartdt,String AppEnddt,String CmtDt,String ResDt,
                      string Current_status,
                      string Current_status_msg,
                      string Details,
                      boolean displayDateTimeSlot,
                      string Information_Summary,
                      boolean isResolved,Boolean isLoggedInUserEqualsAssignee, String suggestedAction,String incidentNo, String FederationIdentifier,String pendingMessageDetails, String resolvedMessageDetails, String tier1Msg, String tier2Msg, String tier3Msg,Date resolutionDate,Boolean awaitingResponseIncident,Boolean awaitingResponseSLA,String DynamicToastMsg,String DynamicIncidentSubMsg,Boolean displayAssignToMe,
                      Boolean currentUserGroupName,
                      String userFederation,
                      String currentAssignee,
                      Boolean isDiffAssignee,
                      Boolean RPAErrorOccured,
                      String UserActionValue){
            this.inm = inm;
            this.AppStartdt = AppStartdt;
            this.AppEnddt = AppEnddt;
            this.CmtDt = CmtDt;
            this.ResDt = ResDt;
            this.Current_status = Current_status;
            this.Current_status_msg = Current_status_msg;
            this.Details = Details;
            this.displayDateTimeSlot = displayDateTimeSlot;
            this.Information_Summary = Information_Summary;
            this.isResolved = isResolved;
            this.isLoggedInUserEqualsAssignee = isLoggedInUserEqualsAssignee;
            this.suggestedAction = suggestedAction;
            this.incidentNo = incidentNo;
            this.FederationIdentifier = FederationIdentifier;
            this.pendingMessageDetails = pendingMessageDetails;
            this.resolvedMessageDetails = resolvedMessageDetails;
            this.tier1Msg = tier1Msg;
            this.tier2Msg = tier2Msg;
            this.tier3Msg = tier3Msg;
            this.resolutionDate = resolutionDate;
            this.awaitingResponseIncident =awaitingResponseIncident;
            this.awaitingResponseSLA=awaitingResponseSLA;
            this.DynamicToastMsg = DynamicToastMsg;
            this.DynamicIncidentSubMsg = DynamicIncidentSubMsg;
            this.displayAssignToMe = displayAssignToMe;
            this.currentUserGroupName = currentUserGroupName;
            this.userFederation = userFederation;
            this.currentAssignee = currentAssignee;
            this.isDiffAssignee = isDiffAssignee;
            this.RPAErrorOccured=RPAErrorOccured;
            this.UserActionValue = UserActionValue;
           }
    }
}