/***************************************************************************************************
Class Name:  integrationUtility_Test
Class Type: Test Class 
Version     : 1.0 
Created Date: 13/10/2017
Function    : Cover integrationUtility which contains classes to connect to CIS
Used in     : None
Modification Log :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 

****************************************************************************************************/
@isTest()
    public class integrationUtility_Test{
           public static String token='3c08d723a26f1234';
           public static String copperPathId='CPI300000403245';
           public static String changeReason='FAULT|DATA-CORRECTION|OTHER';
           public static String desiredPair='P12:345';
           public static String url='/v1/changeDistributionPair';
           public static String role='NBN';
           public static String requestor='Salesforce';
           public static String nbnCorrelationId='9dc761c5-930d-461c-8be3-63c1c018d947';
           public static String serviceId='serv';
           public static string requestorId='requestorId';
           public static String callOutURL='callout:NBNAPPGATEWAYSIT';
           public static String strPRI = 'PRI003000710422';
           public static String avcId ='AVC000000773046';
        
        
 
        @testSetup static void loadTestDataForCustomSettings()
        {
            List<SObject> lstToInsert = new List<SObject>();
            NBNIntegrationInputs__c objCS1 = new NBNIntegrationInputs__c();        
            objCS1.Name = 'HealthCard';
            objCS1.Call_Out_URL__c = 'callout:NBNAPPGATEWAY_RSVT';
            objCS1.EndpointURL__c='/rest/services/v1/avcs/service-health-card/';
            objCS1.AccessSeekerId__c = 'testId';
            objCS1.Call_TimeOut_In_MilliSeconds__c = '60000';
            objCS1.NBN_Environment_Override__c='PSVT';
            objCS1.nbn_Role__c='NBN';
            objCS1.Service_Name__c='test serviec';
            objCS1.Token__c='8e73b7e33b22ad58';
            objCS1.XNBNBusinessChannel__c='Salesforce';
            objCS1.UI_TimeOut__c = 6000;
            objCS1.Header_Keys__c = 'nbn-correlation-id';     
            lstToInsert.add(objCS1);
            
            NBNIntegrationInputs__c objCS2 = new NBNIntegrationInputs__c();        
            objCS2.Name = 'SpeedAssurance';
            objCS2.Call_Out_URL__c = 'callout:NBNAPPGATEWAY_RSVT';
            objCS2.EndpointURL__c='/v1/speed-assurance-traffic-light';
            objCS2.AccessSeekerId__c = 'testId';
            objCS2.Call_TimeOut_In_MilliSeconds__c = '60000';
            objCS2.NBN_Environment_Override__c='SIT1';
            objCS2.nbn_Role__c='NBN';
            objCS2.Service_Name__c='test serviec';
            objCS2.Token__c='8e73b7e33b22ad58';
            objCS2.XNBNBusinessChannel__c='Salesforce';
            objCS2.UI_TimeOut__c = 6000;
            objCS2.Header_Keys__c = 'nbn-correlation-id';     
            lstToInsert.add(objCS2);
            insert lstToInsert;
            List<sObject> lstNbnintegrationinputs = Test.loadData(NBNIntegrationInputs__c.sObjectType, 'NBNIntegrationInputsTestData');
            List<sObject> sdmIntegrationParams = Test.loadData(SDM_CISIntegrationParams__c.sObjectType, 'SDM_IntegrationParams');
            
            /*SDM_CISIntegrationParams__c sdmIntegrationParams = new SDM_CISIntegrationParams__c();     
            sdmIntegrationParams.Name = 'a7t0I000000HNfbQAG';
            sdmIntegrationParams.Turn_on_Callout__c = true;
            sdmIntegrationParams.TimeOutInMilliseconds__c = '5000';
            sdmIntegrationParams.EndpointUrl__c = '/v1/getlocationorder/st1/locationorder/';
            sdmIntegrationParams.x_api_key__c = 'pnqPLGG8Y52i4enle3C924EnZECXfAFw5BjCSazc';    
            insert sdmIntegrationParams; */
        }
        @isTest static void integrationUtility_Test() {

             // Verify Responses using Mock classes for Success
            IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();

             // Verify the CISPRI call out
            mockResp.strResponseType = 'CISPRI';
            mockResp.status= Label.success;

            Test.setMock(HttpCalloutMock.class, mockResp);
            HttpResponse resCIS = integrationUtility.getHttpResponse(strPRI,'');

            // Verify the PNI call out
            mockResp.strResponseType = 'PNI';
            mockResp.status= Label.success;

            Test.setMock(HttpCalloutMock.class, mockResp);
           //HttpResponse resPNI = integrationUtility.postReqTMS(copperPathId,'Param1','Param2',nbnCorrelationId,Label.ChangeDistributionPair);
           //HttpResponse resfTTN = integrationUtility.postReqTMS(copperPathId,'Param1','Param2',nbnCorrelationId,'FTTN');
           //integrationUtility.deserializeJSON('{ "name":"CSA", "test":30, "test2":null }','name');
            // Verify the  DiagnosticTests call out
            mockResp.strResponseType = 'DiagnosticTests';
            mockResp.status= Label.success;
            Test.setMock(HttpCalloutMock.class, mockResp);
            //HttpResponse postTestRequests=integrationUtility.postTestRequests('5',1,'force', token,serviceId, requestorId, url, role, requestor,  nbnCorrelationId,'') ;

            // Verify the  Service call out
            mockResp.strResponseType = 'Service';
            mockResp.status= Label.success;
            Test.setMock(HttpCalloutMock.class, mockResp);
            HttpResponse resServ=integrationUtility.getServiceInfo(avcId);
            HttpResponse resLoc=integrationUtility.getLocationInfo('locId');

        }
        @istest Static void getHttpResponseOrderHistory_test()
        {
            String strPRI = 'PRI003000710422';
            // Verify Responses using Mock classes for Success
            IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();
            // Verify the CISPRI call out
            mockResp.strResponseType = 'CISPRI';
            mockResp.status= Label.success;
            Test.setMock(HttpCalloutMock.class, mockResp);
            HttpResponse orderHistoryResponse;
            Test.StartTest();
                orderHistoryResponse = integrationUtility.getHttpResponseOrderHistory(strPRI,'','123');
            Test.StopTest();
        }
        @istest Static void getHttpResponseOrderHistoryNegative_test()
        {
            String strPRI = 'PRI003000710422';
            // Verify Responses using Mock classes for Success
            IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();
            // Verify the CISPRI call out
            mockResp.strResponseType = 'CISPRI';
            mockResp.status= Label.failure;
            Test.setMock(HttpCalloutMock.class, mockResp);
            HttpResponse orderHistoryResponse;
            Test.StartTest();
                orderHistoryResponse = integrationUtility.getHttpResponseOrderHistory(strPRI,'','ASI');
            Test.StopTest();
        }
        @istest Static void getHttpResponse_test()
        {
            String strPRI = 'PRI003000710422';
            // Verify Responses using Mock classes for Success
            IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();
            // Verify the CISPRI call out
            mockResp.strResponseType = 'CISPRI';
            mockResp.status= Label.success;
            Test.setMock(HttpCalloutMock.class, mockResp);
            HttpResponse orderHistoryResponse;
            Test.StartTest();
                orderHistoryResponse = integrationUtility.getHttpResponse(strPRI,'ASI');
            Test.StopTest();
        }
        @istest Static void getHttpResponseNegative_test()
        {
            String strPRI = 'PRI003000710422';
            // Verify Responses using Mock classes for Success
            IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();
            // Verify the CISPRI call out
            mockResp.strResponseType = 'CISPRI';
            mockResp.status= Label.failure;
            Test.setMock(HttpCalloutMock.class, mockResp);
            HttpResponse orderHistoryResponse;
            Test.StartTest();
                orderHistoryResponse = integrationUtility.getHttpResponse(strPRI,'');
            Test.StopTest();
        }
        @istest Static void postReqTMScreateTOWFttb_test()
        {
           IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen();
           // Verify the CISPRI call out
           mockResp.strResponseType = 'CISPRI';
           mockResp.status= Label.success;
           Test.setMock(HttpCalloutMock.class, mockResp);
           HttpResponse resPNI;
           Test.StartTest();
              resPNI = integrationUtility.postReqTMS(copperPathId,'Param1','Param2',nbnCorrelationId,'createTOWFttb');
           Test.StopTest();
        }
        @istest Static void postReqTMSChangeDistributionFttb_test()
        {
           IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen();
           // Verify the CISPRI call out
           mockResp.strResponseType = 'CISPRI';
           mockResp.status= Label.success;
           Test.setMock(HttpCalloutMock.class, mockResp);
           HttpResponse resPNI;
           Test.StartTest();
               //resPNI  = integrationUtility.postReqTMS(copperPathId,'Param1','Param2',nbnCorrelationId,Label.ChangeDistributionPair);
           Test.StopTest();
        }
        @istest Static void postReqTMSNetworkTrail_test()
        {
           IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen();
           // Verify the CISPRI call out
           mockResp.strResponseType = 'CISPRI';
           mockResp.status= Label.success;
           Test.setMock(HttpCalloutMock.class, mockResp);
           HttpResponse resPNI;
           Test.StartTest();
               resPNI = integrationUtility.postReqTMS(copperPathId,'Param1','Param2',nbnCorrelationId,'NetworkTrail');
           Test.StopTest();
        }
        @istest Static void postReqTMScreateTOWFttbNegative_test()
        {
           IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen();
           // Verify the CISPRI call out
           mockResp.strResponseType = 'CISPRI';
           mockResp.status= Label.Failure;
           Test.setMock(HttpCalloutMock.class, mockResp);
           HttpResponse resPNI;
           Test.StartTest();
               resPNI = integrationUtility.postReqTMS(copperPathId,'Param1','Param2',nbnCorrelationId,'createTOWFttb');
           Test.StopTest();
        }
        @istest Static void postReqTMSNetworkTrailNegative_test()
        {
           IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen();
           // Verify the CISPRI call out
           mockResp.strResponseType = 'TOW';
           mockResp.status= Label.success;
           Test.setMock(HttpCalloutMock.class, mockResp);
           HttpResponse resPNI;
           Test.StartTest();
               resPNI = integrationUtility.postReqTMS(copperPathId,'Param1','Param2',nbnCorrelationId,'NetworkTrail');
           Test.StopTest();
        }
        @istest Static Void postTOWRequestsuccess_test()
        {
           IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen();
           integrationWrapperOutbound intWrapperOutbound = new integrationWrapperOutbound();
           intWrapperOutbound.faultType = 'Fault Type';
           intWrapperOutbound.faultDetails = 'Fault Test Details';
           intWrapperOutbound.incidentId = 'INC000006988871';
           // Verify the CISPRI call out
           mockResp.strResponseType = 'CISPRI';
           mockResp.status= Label.Success;
           Test.setMock(HttpCalloutMock.class, mockResp);
           HttpResponse resPNI;
           Test.StartTest();
               resPNI = integrationUtility.postTOWRequest(intWrapperOutbound,'NCAS-FTTN');
               resPNI = integrationUtility.postTOWRequest(intWrapperOutbound,'NCAS-FTTB');
           Test.StopTest();
        }
        @isTest static void integrationUtility_Exception() {
            Test.setMock(HttpCalloutMock.class, new IntUtilityMockHttpRespGen());
            HttpResponse resPri = integrationUtility.getHttpResponse(strPRI,'');
            HttpResponse resServ=integrationUtility.getServiceInfo(avcId);
        }

        @isTest static void integrationUtility_Fail()
        {
        // Verify Responses using Mock classes for  Failure
            IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();

             // Verify the CISPRI call out
            mockResp.strResponseType = 'CISPRI';
            mockResp.status= Label.Failure;

            Test.setMock(HttpCalloutMock.class, mockResp);
            HttpResponse resCIS = integrationUtility.getHttpResponse(strPRI,'');

            // Verify the PNI call out
            mockResp.strResponseType = 'PNI';
            mockResp.status= Label.Failure;
          //  NBNIntegrationInputs__c FTTN = TestDataUtility.createCustSetForNBNIntegrationInputs('FTTN');
          //  NBNIntegrationInputs__c ChangeDistributionPair = TestDataUtility.createCustSetForNBNIntegrationInputs('ChangeDistributionPair');
            Test.setMock(HttpCalloutMock.class, mockResp);
            //HttpResponse resPNI = integrationUtility.postReqTMS(copperPathId,'Param1','Param2',nbnCorrelationId,Label.ChangeDistributionPair);
            //HttpResponse resfTTN = integrationUtility.postReqTMS(copperPathId,'Param1','Param2',nbnCorrelationId,'FTTN');


            // Verify the  DiagnosticTests call out
            mockResp.strResponseType = 'DiagnosticTests';
            mockResp.status= Label.Failure;
            Test.setMock(HttpCalloutMock.class, mockResp);
           // HttpResponse postTestRequests=integrationUtility.postTestRequests('5',1,'deny', token,serviceId, requestorId, url, role, requestor,  nbnCorrelationId,'SELTTest') ;
            
            
            // Verify the  Service call out  
            mockResp.strResponseType = 'Service';
            mockResp.status= Label.Failure;
            Test.setMock(HttpCalloutMock.class, mockResp);            
            HttpResponse resServ=integrationUtility.getServiceInfo(avcId);  
            
           // HttpResponse resDiagnostic1=integrationUtility.postLQDTestRequest(1,token,serviceId,requestorId,url,role,requestor,nbnCorrelationId,callOutURL);
            
        }  
        @isTest static void postCancelAppointmentRequest_test(){
        	 
            HttpResponse response;
            Test.StartTest();
                //response = integrationUtility.postCancelAppointmentRequest('ASI1234','App1234','333','cancelAppointment');
            Test.StopTest(); 
        }
        
        @isTest static void testAllMethods(){
            integrationUtility.getAppointmentDetailsFromCIS('getAppointmentDetailsFromCIS','ASI1234','APT1234');
            integrationUtility.GetAppointmentRequest('getAppointmentDetails','TRNS123');
            integrationUtility.getWorkOrderRequest('siteIds','wo1234','WorkorderRequest');
            integrationUtility.getTestHistorySummary('avc123',60);
            integrationUtility.getTestHistoryDetail('wri123');
            integrationUtility.getGraphsIntegration('urlgraph');
            integrationUtility.getServiceHealthCard('avc123');
            integrationUtility.postCancelAppointmentRequest('ASI1234','APT1234','CLOUSRECODe','CISServicesAPI');
            integrationUtility.postGetAppointmentRequest('ASI1234','APT1234','CISServicesAPI');
            //integrationUtility.postSpeedAssuranceTrafficLight('ASI1234','APT1234');
             String woJSON = '{"woNum": "test","woEngageType" : "test","woSpecVersion": "test","FTTN Service Assurance" : "test","woPCRCode" : "test","isFault" : "false","rootCause" : "test","WoActionType" : "test","wkServiceTestPoint" : "test","addInfo" : "test","closureSummaryText" : "test","incidentId" : "INC000111212","strProdCat" : "FTTN","strAVCId" : "test","strPRI" : "test","strAccessSeekerId" : "test","parent" : "test","WRequest" : "test","WOSpecID" : "test","netBoundaryPoint" : "test","netBoundaryPointDetails" : "' + 
             'test","locationId" :"test","WorkOrderApptID" : "test","selectedPCRCode" :"test","selectedPCRDesc" :"test"}' ;
        
        	incidentManagementController_LC.Wrapper_WorkOrdervalues woWrapperValues =  (incidentManagementController_LC.Wrapper_WorkOrdervalues)JSON.deserialize(woJSON.replace('\n','').replace('\r',''), incidentManagementController_LC.Wrapper_WorkOrdervalues.class); 

            integrationUtility.postTechNote('actiondate', woWrapperValues);
            integrationUtility.postTOWWORKORDERRequest('actiondate','completeJobSafely',woWrapperValues);
            integrationUtility.postTOWWORKORDERRequestOnsiteTechnician('completeJobSafely','actiondate', woWrapperValues);
            integrationUtility.WORemedyIntegrationLogSetting('inc','inJson','outJson','wor1223', 'woStatus', 'respStatus', 'action', 'transactionId');
        }
        
        testmethod static void FailureRefreshOnDemand() 
        {
            
            IntUtilityMockHttpRespGen mockResp = new IntUtilityMockHttpRespGen ();
            mockResp.strResponseType = 'LocationOrders';
            mockResp.status= Label.Failure;
            mockResp.StatusCode = 404;
            //setMockRes Class
            Test.setMock(HttpCalloutMock.class, mockResp);            
            Test.StartTest();
            Integer responseCode = SDM_OnDemandRefreshController.refreshOpptyOrders('LOC000000000000'); 
            mockResp.StatusCode = 400;
            integrationUtility.getLocationOrders('LOC000000000000','ord123');
            mockResp.StatusCode = 429;
            integrationUtility.getLocationOrders('LOC000000000000','ord123');
            mockResp.StatusCode = 500;
            integrationUtility.getLocationOrders('LOC000000000000','ord123');
            mockResp.StatusCode = 200;
            integrationUtility.getLocationOrders('LOC000000000000','ord123');
            Test.StopTest();
            
        }
    }