@isTest
public class integrationWrapTest{

    public static testMethod void PNIresp(){    
    test.startTest();
        integrationWrapper.PNIresp PNI= new integrationWrapper.PNIresp();
        PNI.Status='Success';
        PNI.NetworkID='NA3456789';
        PNI.Error='Fail';
        
        integrationWrapper.copperDistributionCables ic = new integrationWrapper.copperDistributionCables();
            ic.subtype = 'test';
       ic.lengthType = 'test';
       ic.twistedPairCapacity = 'test';
       ic.length = 'test';
       ic.twistedPairId = 'test';
       ic.platformReferenceId = 'test';
       ic.twistedPairName = 'test';
       ic.status = 'test';
       ic.twistedPairPlatformReferenceId = 'test';
       ic.type = 'test';
       ic.twistedPairStatus = 'test';
       ic.name = 'test';
       ic.twistedPairInstallationStatus = 'test';
       ic.id = 'test';
       
       
        integrationWrapper.copperLeadinCable icl = new integrationWrapper.copperLeadinCable();
       
         icl.twistedPairPlatformReferenceId= 'test';
        icl.length= 'test';
        icl.lengthType= 'test';
        icl.platformReferenceId= 'test';
        icl.installationType= 'test';
        icl.status= 'test';
        icl.twistedPairId= 'test';
        icl.subtype= 'test';
        icl.twistedPairName= 'test';
        icl.type= 'test';
        icl.twistedPairStatus= 'test';
        icl.name= 'test';
        icl.twistedPairInstallationStatus= 'test';
        icl.id= 'test';
        
        
          integrationWrapper.copperMainCable cmc = new integrationWrapper.copperMainCable();
      
        cmc.id= 'test';
       cmc.lengthType= 'test';
       cmc.length= 'test';
       cmc.twistedPairId= 'test';
       cmc.platformReferenceId= 'test';
       cmc.twistedPairName= 'test';
       cmc.status= 'test';
       cmc.twistedPairPlatformReferenceId= 'test';
       cmc.subtype= 'test';
       cmc.twistedPairStatus= 'test';
       cmc.type= 'test';
       cmc.twistedPairInstallationStatus= 'test';
       cmc.name= 'test';
       
        
          integrationWrapper.fttnNode ftt= new integrationWrapper.fttnNode();
      
       
           ftt.id = 'test';
        ftt.status = 'test';
        ftt.type = 'test';
        ftt.platformReferenceId = 'test';
        ftt.name = 'test';
        
        
        integrationWrapper.mdf mdf = new integrationWrapper.mdf();
      
              mdf.locationId = 'test';
        mdf.locationLatitude = 'test';
        mdf.locationLongitude = 'test';
        mdf.platformReferenceId = 'test';
        mdf.portId = 'test';
        mdf.status = 'test';
        mdf.portName = 'test';
        mdf.subtype = 'test';
        mdf.portPlatformReferenceId = 'test';
        mdf.type = 'test';
        mdf.portStatus = 'test';
        mdf.name = 'test';
        mdf.portInstallationStatus = 'test';
        mdf.id = 'test';
  
    integrationWrapper.metadataFTT meta= new integrationWrapper.metadataFTT();
      
         meta.incidentId = 'test';
        meta.copperPathId = 'test';
        meta.tokenId = 'test';
        meta.accessServiceTechnologyType = 'test';
        meta.nbnCorrelationId = 'test';
        
         integrationWrapper.pillars pill= new integrationWrapper.pillars();
   
             pill.type = 'test';
        pill.name = 'test';
        pill.id = 'test';
        pill.subtype = 'test';
        pill.distributionPairCapacity = 'test';
        pill.platformReferenceId = 'test';
        pill.terminationModules = null;
        pill.status = 'test';
        
        
          integrationWrapper.tco tco= new integrationWrapper.tco();
   
           tco.locationLongitude = 'test';
       tco.status = 'test';
       tco.subtype = 'test';
       tco.platformReferenceId = 'test';
       tco.type = 'test';
       tco.locationId = 'test';
       tco.name = 'test';
       tco.locationLatitude = 'test';
       tco.id = 'test';
       
          integrationWrapper.terminationModules tm= new integrationWrapper.terminationModules();
   
             tm.name= 'test';
       tm.id= 'test';
       tm.type= 'test';
       tm.twistedPairName= 'test';
        tm.twistedPairId= 1;
       tm.twistedPairPlatformReferenceId= 'test';
       tm.platformReferenceId= 'test';
       tm.twistedPairStatus= 'test';
       tm.status= 'test';
       tm.twistedPairInstallationStatus= 'test';
       tm.subtype= 'test';
 
        
        String Id ='3335';
        String ParentId ='84934';
        String Type ='test';
        String CreatedById ='3455';
        DateTime CreatedDate =system.today();
        String Title ='test';
        String Body ='yesy';
        String NetworkScope ='test';
        String Visibility ='test';
        boolean isLongPost =true;
        integrationWrapper.FeedItemWrapper fitm= new integrationWrapper.FeedItemWrapper(Id, ParentId, Type, CreatedById, CreatedDate, Title, Body, NetworkScope, Visibility,isLongPost);
   
   
  
    test.stopTest();
    }
}