/*------------------------------------------------------------
Author:     Beau Anderson
Company:       Contractor
Description:   Created to allow creation of outbound serialized integration. Should be called from integrationUtility. Created as part of https://jira.nbnco.net.au/browse/CUSTSA-7310 but can be
used for other stories
Inputs:    None
Test Class:    
History
<Date>      <Author>     <Description>
13/03/2018    Beau Anderson    Created
------------------------------------------------------------*/
public class integrationWrapperOutbound{
    public String transactionId;    //aeb8a1007f60271d
    public String tokenId;  //3c08d723a26f1234
    public String accessSeekerId;   //ASI000000000035
    public String ticketOfWorkId;   //WRQ000000000001
    public String ticketOfWorkType; //FTTN Service Assurance
    public String ticketOfWorkStatus;   //CREATED
    public String ticketOfWorkStatusReason; //CREATED
    public String plannedCompletionDate;    //2012-09-23T12:00:00Z
    public String incidentId;   //INC000000000001
    public String additionalInformation;    //additional information
    public String repeatIncidentPriority;   //0
    public String segment;  //Residential
    public String serviceRestorationSla;    //Standard
    public String locationId;   //LOC000000000001
    public String appointmentId;    //APT000000000001
    public String appointmentType;  //Commitment
    public String jobRequirement;   //FTTN Service Assurance
    public String centralSplitterPresent;   //Yes
    public String faultType;    //New Service Never Worked
    public String faultDetails; //fault details
    public String copperPathId; //CPID
    public String networkTrail; //CPID
    public cls_services[] services;
    public cls_resources[] resources;
    
    public class cls_services {
        public String serviceId;    //PRI123456789012
        public String serviceType;  //VSL
        public String copperPairServiceClass;   //Active
        public String downstreamSpeed;  //5100
        public String upstreamSpeed;    //25900
        public String minimumRequiredLayer1UpstreamBitrate;  
        public String minimumRequiredLayer1DownstreamBitrate;
    }
    
    public class cls_resources {
        public String type; //Copper Pair
        public String lineStatus;   //In-active Telstra connected
        public String potsInterconnect; //0298765432
    }
    
    public static integrationWrapperOutbound parse(String json){
        return (integrationWrapperOutbound) System.JSON.deserialize(json, integrationWrapperOutbound.class);
    }
}