/*------------------------------------------------------------
Author:     Gaurav Tiwari
Company:       NBN
Description:   Created to allow creation of outbound serialized integration. Should be called from integrationUtility. Created as part of https://jira.nbnco.net.au/browse/CUSTSA-7310 but can be
used for other stories
Inputs:    None
Test Class:    
History
<Date>      <Author>     <Description>
24/08/2018    Gaurav Tiwari    Created
------------------------------------------------------------*/
public class integrationWrapperWorkOrderOutbound{
    
    public String tokenId;  //3c08d723a26f1234
    public String accessSeekerId;  //ASI000000000035
    public String workOrderId;   //WOR000000000001
    public String woSpecversion; //0.0.0
    public String techType;   //FTTB
    public String revisionNumber; // 1
    public String revisionTime;    //2012-08-23T12:00:00+10:00
    public String currentFieldWorkStatus;   // Active
    public String activityId;    //WOR1234567
    public String currentActivityStateId;   // INPRG
    public String currentActivityStateDateTime;  // 2012-08-23T12:00:00+10:00
    public String activityInstantiatedBy;    // Activity_FTTB_Service_Assurance
    public String workOrderSpecification;   // FTTB_Service_Assurance_Work_Order_Specification
    public ActivityChangeData activityChangeData;
    
    public class ActivityChangeData {
        public String actionDate;    // 2012-08-23T12:00:00+10:00
        public String additionalInfo;  // Additional Info
        public String pcrCode;   // PI5 / CI10 / RI1
        public String closureSummary;  // Action finished
        public String closureDetails;    // closure details
        public String completeJobSafely;  //Additional Controls
        public String requiredControls; //Standard
        public String technicianId; //Onsite
        public String reasonCode; // onsite
        public String dataUpdateType;//add note
        
        
        public cls_SupplimentalClosureDetails[] supplimentalClosureDetails; 
        public String dataIntegritySummary; // ADDRESS INCORRECT
        
        public String workingServiceTestPoint;    // First Telecommunications Outlet
        public String verifiedMACAddress;  // verifiedMACAddress
        public String verifiedDevice;   // verifiedDevice
   
     // Below for FTTN Complete/ InComplete       
        public String  leadinInstallStatus; // Installed
        public String  leadinInstallationType; // Underground with new LIC
        public String leadinIsReplaced; // Yes
        public String leadinAsBuiltLength ; // lead-in As Built Length
        public String licInstallStatus; // Installed
        public String licAsBuiltLength; //LIC As Built Length
        public String licMaterial; // Asbestos
        public String licSize; // 25
   //     
        public String centralSplitterReplaced;  // Yes
        public String centralSplitterMake;    // Central Splitter Make
        public String centralSplitterModel;  //Central Splitter Model
       
        public String fttbMdfPatchInstallStatus; //Installed
        public String fttbMdfPatchPort;    // fttbMdfPatchPort
        public String fttbPstnMdfPatchInstallStatus;  // Installed
        public String fttbPstnMdfPatchPort;   // fttbPstnMdfPatchPort
        public String faultOutsideNBNDemarcation;  // Yes
        public String faultOutsideNBNDemarcationRootCause;    // Internal Wiring Issue
   
      // Below for  FTTN Complete    
        public String nodeSplitterInstallStatus; // Installed
        public String leadinJointInstallStatus; // Installed
        public String pillarPatchInstallStatus; // Installed
        public String nodePatchInstallStatus; // Installed
        public String mdfPatchInstallStxatus ; // Installed
        public String pstnNodeSplitterInstallStatus; // Installed
        public String pstnPillarPatchInstallStatus; // Installed
        public String pstnNodePatchInstallStatus; // Installed
        public String confirmNetworkBoundaryPoint; // MDF
        public String networkBoundaryDetails; // Sample text
        public String agreeInstallAndMethod; // Yes
        public String agreeReceiptOfGuides; // Yes
        public String agreeWorkAndInstall; // Yes
    //
    }
    
    public class cls_SupplimentalClosureDetails{
        
        public String  pcrCode; // PCR001
        public String summary; // supplemental closure summary
        public String details; // supplemental closure details
    }
    
    public static integrationWrapperWorkOrderOutbound parse(String json){
        return (integrationWrapperWorkOrderOutbound) System.JSON.deserialize(json, integrationWrapperWorkOrderOutbound.class);
    }
}