public class mapViewApexController {
	
    @AuraEnabled
    public static map<string,string> getLocationDetailsOnLatLang(decimal latitude, decimal longitude){
        String formatedAddress = '';
        map<string,string> retMap = new map<string,string>{'siteName' => '', 'LocId' => '', 'techType' => '', 
            'serviceStatus' => '', 'rfsMessage' => '', 'serviceType' => '', 'serviceCategory' => '', 
            'FLFResult' => '', 'streetnumber' => '', 'streetname' => '','country' => '', 'postCode' => '', 'suburb' => '',
            'state' => '', 'latitude' => string.valueOf(latitude), 'longitude' => string.valueOf(longitude), 'fsaId' => '',
            'samId' => ''};
        try{
            String strEndPoint = '?key='+ System.Label.GoogleAPIKey + '&latlng='+string.valueOf(latitude)+','+string.valueOf(longitude)+'&sensor=true/false';
            Http h = new Http(); 
            HttpRequest req = new HttpRequest();
            req.setMethod('GET');
            req.setEndpoint('callout:Google_Maps_API' + strEndPoint);
            req.setTimeout(integer.valueof(Label.Location_TimeOut_In_MilliSeconds));
            HttpResponse res = h.send(req);
            system.debug('resssss '+JSON.serialize(res.getBody()));
            system.debug('res '+res.getStatus()+' body '+res.getBody());
            mapResponse response = (mapResponse) JSON.deserialize(res.getBody(),mapResponse.class);
            system.debug('response parsing '+response);
            if(response != null && response.results != null && response.results.size() > 0){
                for(integer i = 0; i < response.results.size();  i++){
                    if(response.results[i].formatted_address != null){
                        formatedAddress = response.results[i].formatted_address;
                        if(retMap.get('siteName') == ''){
                            retMap.put('siteName',formatedAddress);
                        }
                    }
                    if(response.results[i].address_components != null && response.results[i].address_components.size() != 0){
                        string streetNumber = '';
                        string route = '';
                        for(GoogleAddressSearchController.MapInformation mapInfo :response.results[i].address_components){
                            set<string> tempSet = new set<string>();
                            tempSet.addAll(mapInfo.types);
                            if(tempSet.contains('country')){
                                if(retMap.get('country') == ''){
                                   retMap.put('country',mapInfo.long_name);
                                }
                            }
                            if(tempSet.contains('postal_code')){
                                if(retMap.get('postCode') == ''){
                                   retMap.put('postCode',mapInfo.long_name);
                                }
                            }
                            if(tempSet.contains('administrative_area_level_1')){
                                if(retMap.get('state') == ''){
                                   retMap.put('state',mapInfo.short_name);
                                }
                            }
                            if(tempSet.contains('locality')){
                                if(retMap.get('suburb') == ''){
                                   retMap.put('suburb',mapInfo.long_name);
                                }
                            }
                            if(tempSet.contains('street_number')){
                                if(retMap.get('streetnumber') == ''){
                                   retMap.put('streetnumber',mapInfo.long_name);
                                }
                            }
                            if(tempSet.contains('route')){
                                if(retMap.get('streetname') == ''){
                                   retMap.put('streetname',mapInfo.long_name);
                                }
                            }
                        }
                        //retMap.put('street',streetNumber+' '+route);
                    }
                }
            }
            
            LapiResponse Lapiresp = getLocationDetailsLAPI(string.valueOf(latitude),string.valueOf(longitude));
            if(Lapiresp != null && Lapiresp.data != null && Lapiresp.data.size() > 0){
                string samId = 'placeHolder';
                integer index = null;
                for(integer i = 0; i < Lapiresp.data.size(); i++){
                    if(samId == 'placeHolder' && Lapiresp.data[i].attributes.premises.samId != null){
                        samId = Lapiresp.data[i].attributes.premises.samId;
                        index = i;
                    }
                    else if(samId != 'placeHolder' && Lapiresp.data[i].attributes.premises.samId != null &&  samId != Lapiresp.data[i].attributes.premises.samId){
                        index = null;
                        break;
                    }
                }
                if(index != null){
                    retMap.put('LocId',Lapiresp.data[index].id);
                    retMap.put('fsaId',Lapiresp.data[index].attributes.premises.fsaId);
                    retMap.put('samId',Lapiresp.data[index].attributes.premises.samId);
                }
            }
            PapiResponse Papiresp = getLocationDetailsPAPI(string.valueOf(latitude),string.valueOf(longitude));
            if(Papiresp != null && Papiresp.servingArea != null){
                if(Papiresp.servingArea.techType != null){
                    retMap.put('techType',Papiresp.servingArea.techType);
                }
                if(Papiresp.servingArea.serviceStatus != null){
                    retMap.put('serviceStatus',Papiresp.servingArea.serviceStatus);
                }
                if(Papiresp.servingArea.rfsMessage != null){
                    retMap.put('rfsMessage',Papiresp.servingArea.rfsMessage);
                }
                if(Papiresp.servingArea.serviceType != null){
                    retMap.put('serviceType',Papiresp.servingArea.serviceType);
                }
                if(Papiresp.servingArea.serviceCategory != null){
                    retMap.put('serviceCategory',Papiresp.servingArea.serviceCategory);
                }
                string FLFResult = calculateFLF(Papiresp.servingArea.serviceType, Papiresp.servingArea.serviceStatus, Papiresp.servingArea.serviceCategory, Papiresp.servingArea.rfsMessage);
                system.debug('FLFresult '+FLFResult);
                retMap.put('FLFResult',FLFResult);
            }
            
        }
        catch(exception ex){
            system.debug(' message '+ex.getMessage()+' line '+ex.getStackTraceString());
        }
        system.debug('resultmap '+retMap+' null test '+retMap.get('rfsMessage'));
        return retMap;
    }
    public static PapiResponse getLocationDetailsPAPI(string latitude, string longitude){
        PapiResponse resp;
        HttpRequest request = new HttpRequest();
        Http http = new Http();  
        request.setHeader('Referer', 'https://www.nbnco.com.au');
        string URLEndPoint = 'https://places.nbnco.net.au/places/v1/boundary?lat='+latitude+'&lng='+longitude;
        system.debug(URLEndPoint);
        request.setEndpoint(URLEndPoint);
        request.setMethod('GET');
        request.setTimeOut(integer.valueof(Label.Location_TimeOut_In_MilliSeconds));
        HttpResponse response = http.send(request);
        system.debug('---response papi---' + response);   
        system.debug('---response body papi---' + response.getBody());
        resp = (PapiResponse) JSON.deserialize(response.getBody(), PapiResponse.class);
        return resp;
    }
    public static LapiResponse getLocationDetailsLAPI(string latitude, string longitude){
        LapiResponse resp;
        try{
            HttpRequest request = new HttpRequest();
            Http http = new Http();  
            String URLEndPoint = '?'+'filter=latitude=='+latitude+';longitude=='+longitude+';radius==250';
            request.setEndpoint('callout:LocationAPI' +'/'+ URLEndPoint);
            //request.setEndpoint('callout:LocationAPI'+'?'+'filter=latitude=='+latitude+';longitude=='+longitude);
            system.debug(URLEndPoint);
            //request.setEndpoint(URLEndPoint);
            request.setMethod('GET');
            request.setTimeOut(integer.valueof(Label.Location_TimeOut_In_MilliSeconds));
            HttpResponse response = http.send(request);
            system.debug('---response lapi---' + response);   
            system.debug('---response body lapi---' + response.getBody());    
            resp = (LapiResponse) JSON.deserialize(response.getBody(), LapiResponse.class);
            system.debug('resp '+resp);
        }
        catch(exception ex){
            system.debug(' message '+ex.getMessage()+' line '+ex.getStackTraceString());
        }
        return resp;
    }
    
    public static string calculateFLF(string serviceType, string serviceStatus, string servicecategory, string rfsMessage){
        string result = 'outside';
        if(serviceType == 'fibre' || serviceType == 'other'){
            if((serviceStatus == 'available' || serviceStatus == 'in_construction') || 
               (serviceStatus == 'remediating' && servicecategory == 'greenfields') ||
               (serviceStatus == 'proposed' && validrfsMessaage(rfsMessage))){
                   result = 'inside';
            }
            else if(servicecategory == 'greenfields'){
                result = 'inside';
            }
        }
        return result;
    }
    public static boolean validrfsMessaage(string rfsMessage){
        boolean retBool = false;
        if(rfsMessage != '' && rfsMessage.split(' ') != null){
            List<string> rfsSplit = rfsMessage.split(' ');
            if(rfsSplit.size() == 2 && rfsSplit[1].isNumeric()){
                if(rfsSplit[0].length() == 3){
                    retBool = true;
                }
                else if(rfsSplit[0].split('-') != null){
                    List<string> rfsInnerSplit = rfsSplit[0].split('-');
                    if(rfsInnerSplit.size() == 2 && rfsInnerSplit[0].length() == 3 && rfsInnerSplit[1].length() == 3){
                        retBool = true;
                    }
                    
                }
            }
        }
        return retBool;
    }
    public class mapResponse{
        public List<GoogleAddressSearchController.MapDetailsComponents> results;
    }
    
    public class LapiResponse{
        public List<data> data;
    }
    public class data{
        public string id;
        public attributes attributes;
    }
    public class attributes{
        public premises premises;
    }
    public class premises{
        public string fsaId;
        public string samId;
    }
    
    public class PapiResponse{
        public servingArea servingArea;
    }
    public class servingArea{
        public string techType;
        public string serviceStatus;
        public string rfsMessage;
        public string serviceType;
        public string serviceCategory;
        public string FLFResult;
        public string latitude;
        public string longitude;
    }
}