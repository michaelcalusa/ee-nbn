/***************************************************************************************************
Class Name:         mapViewApexController_Test
Class Type:         Test Class 
Version:            1.0 
Created Date:       5th feb 2018
Function:           This is a test class to mapViewApexController
Input Parameters:   None 
Output Parameters:  None
Description:        mapViewApexController has logic to search on map in new devs
Modification Log:
* Developer            Date             Description
* --------------------------------------------------------------------------------------------------                 
* Ramtej Juloori      05/02/2018      Created - Version 1.0 
****************************************************************************************************/ 
@isTest
public class mapViewApexController_Test {
    static testMethod void test1(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new googleAPIMockResponse_Test());
        mapViewApexController.getLocationDetailsOnLatLang(-33.839861,151.209579);
        Test.stopTest();
    }
}