public class milestonePlanController { 
    @AuraEnabled
    public static String getTransitionRecord(String opportunityId) {        
        if(opportunityId != null) {
            for(Transition_Engagement__c existingEngagement : [SELECT Id FROM Transition_Engagement__c WHERE Opportunity__c =:opportunityId ]) {
                return existingEngagement.Id;
            }       
        }  
        return null;
    }
    @AuraEnabled
    public static Boolean checkMilestonePlanRecordStatus(String mpRecId) {        
            Transition_Engagement__c mpRec = [SELECT Id, Status__c FROM Transition_Engagement__c WHERE Id =:mpRecId Limit 1]; 
    if(mpRec.Status__c == 'Completed'){return true;}
        return false;
    }    
    
    public static Map<string, Sub_Tasks__mdt> getMilestoneMetadata(){
        Map<string, Sub_Tasks__mdt>  mapofMilesnNoRecs = new  Map<string, Sub_Tasks__mdt>();
        for(Sub_Tasks__mdt m: [Select Id, DeveloperName, Task_Record_Type__c, Task_Name__c, Sequence_Number__c, Days_to_Complete__c, Assigned_To__c, Milestone__c from 
                               Sub_Tasks__mdt where Type__c ='Milestone' ORDER BY DeveloperName]){
                                   mapofMilesnNoRecs.put(m.Milestone__c, m);
                               }
            return  mapofMilesnNoRecs;
    }
    /* Fetch predefined Milestone Metadata from Oppty */
    @AuraEnabled
    public static List<Transition_Engagement_Milestones__c> fetchMilestoneMetadata(String recordType) {  
        string milestoneRecType;
        Sub_Tasks__mdt metaRec = [Select Assigned_To__c, Task_Record_Type__c from Sub_Tasks__mdt where Type__c ='Milestone' LIMIT 1];
        Id qId = [SELECT Id from Group where DeveloperName =: metaRec.Assigned_To__c  and Type = 'Queue'].Id;
        milestoneRecType = metaRec.Task_Record_Type__c;
        List<Transition_Engagement_Milestones__c> milestoneDataLst = new List<Transition_Engagement_Milestones__c>();
        Id RecTypeId = Schema.SObjectType.Transition_Engagement_Milestones__c.getRecordTypeInfosByDeveloperName().get(milestoneRecType).getRecordTypeId();
        for(Sub_Tasks__mdt m: [Select Id, DeveloperName, Task_Name__c, Sequence_Number__c, Days_to_Complete__c, Assigned_To__c, Milestone__c from Sub_Tasks__mdt where Type__c ='Milestone' AND Task_Record_Type__c =: milestoneRecType ORDER BY DeveloperName]){
            milestoneDataLst.add(new Transition_Engagement_Milestones__c(RecordTypeId = RecTypeId, Milestone_Name__c = m.Task_Name__c, OwnerId=qId, Start_Date__c = Date.today(),
                                                                         Status__c ='Open', Milestone_Sequence__c=m.Milestone__c) );
        }
        if(milestoneDataLst.Size()>0){
            return milestoneDataLst;
        }
        return null;
    } 
    /* Fetch existing and new versions of Milestone Recs from MilestonePlan */    
    @AuraEnabled
    public static List<Transition_Engagement_Milestones__c> fetchMilestoneRecs(String recordType, Id milestonePlanId) {    
        List<MilestoneWrapper> mileWrapperList = new List<MilestoneWrapper>();
        Map<String,List<String>> mapMseqName = new Map<String,List<String>>();
        Map<String,String> mapMNameStatus = new Map<String,String>();
        Set<Transition_Engagement_Milestones__c> msCompletedRecs = new Set<Transition_Engagement_Milestones__c>();
        Set<Transition_Engagement_Milestones__c> msVersionRecs = new Set<Transition_Engagement_Milestones__c>(); 
        Set<String> msSequenceNum = new Set<String>();
        Boolean isNumflag = false;
        String msRecordTypeId,msVer,mName;
        String msName, regExp = '[A-Za-z ]',regex='[0-9]';
        /* Assign to Bid & Transition Engagement Team */
        Sub_Tasks__mdt metaRec = [Select Assigned_To__c, Task_Record_Type__c from Sub_Tasks__mdt where Type__c ='Milestone' LIMIT 1];
        Id qId = [SELECT Id from Group where DeveloperName =: metaRec.Assigned_To__c and Type = 'Queue'].Id;
        
        msRecordTypeId = Schema.SObjectType.Transition_Engagement_Milestones__c.getRecordTypeInfosByDeveloperName().get(metaRec.Task_Record_Type__c).getRecordTypeId();
        
        List<Transition_Engagement_Milestones__c> milestoneDataLst = new List<Transition_Engagement_Milestones__c>();
        for(Transition_Engagement_Milestones__c ms: [Select Id, Milestone_Name__c, Start_Date__c, Close_Date__c, Status__c, OwnerId, Expected_Completion_Date__c, 
                                                     Milestone_Sequence__c, Milestone_On_Track__c, Is_Completed__c from Transition_Engagement_Milestones__c where 
                                                     Transition_Engagement__c =: milestonePlanId AND RecordTypeID =: msRecordTypeId ORDER BY Milestone_Name__c])
        {
            if(ms.Status__c == 'Completed'){msCompletedRecs.add(ms);}
            else if(ms.Status__c == 'Open'|| ms.Status__c == 'In Progress'){
                milestoneDataLst.add(ms);
                msSequenceNum.add(ms.Milestone_Sequence__c); 
            }
            if(ms.Status__c == 'Completed' || ms.Status__c == 'Not Required'){
                List<string> temp = mapMseqName.get(ms.Milestone_Sequence__c);
                if(temp == null){
                    mapMseqName.put(ms.Milestone_Sequence__c,new List<string>{ms.Milestone_Name__c});
                    mapMNameStatus.put(ms.Milestone_Name__c, ms.Status__c);
                }
                else{
                    temp.add(ms.Milestone_Name__c);
                }
            }}
        
        for(Sub_Tasks__mdt m: [Select Id, DeveloperName, Task_Name__c, Sequence_Number__c, Days_to_Complete__c, Assigned_To__c, Milestone__c from 
                               Sub_Tasks__mdt where Type__c = 'Milestone' AND Milestone__c NOT IN: msSequenceNum AND 
                               Task_Record_Type__c  =: metaRec.Task_Record_Type__c ORDER BY DeveloperName]){
                                   if(mapMseqName.containsKey(m.Milestone__c)){
                                       List<string> mNames = mapMseqName.get(m.Milestone__c);
                                       mNames.sort();
                                       
                                       /* Versioning */
                                       Integer verNo; 
                                       msName = mNames[mNames.Size()-1];
                                       msVer = msName.right(2);
                                       isNumflag = msVer.isNumeric();
                                       msVer = msVer.replaceAll(regExp, '');
                                       if(String.isEmpty(msVer)){
                                           msName += ' V1';
                                       }
                                       else if(msVer.isNumeric()){
                                           verNo = Integer.valueOf(msVer);
                                           verNo += 1;
                                           String name = String.valueOf(verNo);  
                                           msName = isNumflag ? msName.left(msName.length()-2)+name : msName.left(msName.length()-1)+name;
                                           /* End of Versioning */
                                       }
                                       
                                       msVersionRecs.add(new Transition_Engagement_Milestones__c(Transition_Engagement__c=milestonePlanId, OwnerId=qId,Milestone_Name__c = msName, 
                                                                                                 RecordTypeId =msRecordTypeId, Start_Date__c = Date.today(),Status__c ='Open', 
                                                                                                 Milestone_Sequence__c=m.Milestone__c) );              
                                   }
                                   else{
                                       milestoneDataLst.add(new Transition_Engagement_Milestones__c(Transition_Engagement__c=milestonePlanId, OwnerId=qId,Milestone_Name__c = m.Task_Name__c, 
                                                                                                    RecordTypeId =msRecordTypeId, Start_Date__c = Date.today(),Status__c ='Open', 
                                                                                                    Milestone_Sequence__c=m.Milestone__c) );}
                               }
        if(msCompletedRecs.Size()>0){milestoneDataLst.addAll(msCompletedRecs);} 
        if(msVersionRecs.Size()>0){milestoneDataLst.addAll(msVersionRecs);}
        if(milestoneDataLst.Size()>0){
            for(Transition_Engagement_Milestones__c m: milestoneDataLst){
                mileWrapperList.add(new MilestoneWrapper(m));
            }
            /* Invoke sort method to sort out the list of recs by Milestone Name */
            mileWrapperList.sort();
            milestoneDataLst.clear();
            for(MilestoneWrapper MW : mileWrapperList){
                milestoneDataLst.add(MW.mileRec);
            }
            return milestoneDataLst;
        }
            return null;
    }  
    /*Sorting method by Milestone Name field */
    public class MilestoneWrapper implements Comparable {
        public Transition_Engagement_Milestones__c mileRec = new Transition_Engagement_Milestones__c();
        public MilestoneWrapper(Transition_Engagement_Milestones__c mileRecord) {
            mileRec = mileRecord;
        }
        public Integer compareTo(Object compareTo) {
            // Cast argument to MilestoneWrapper
            MilestoneWrapper compareTomileRec = (MilestoneWrapper)compareTo;
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if (mileRec.Milestone_Name__c > compareTomileRec.mileRec.Milestone_Name__c) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (mileRec.Milestone_Name__c < compareTomileRec.mileRec.Milestone_Name__c) {
                // Set return value to a negative value.
                returnValue = -1;
            }
            return returnValue;       
        }
    }    
    
    /* Insert Milestone Recs from Oppty */
    @AuraEnabled
    public static void createMilestoneRecs(String opportunityId, String msPlanName, String milestoneDataLst) {  
        List <Transition_Engagement_Milestones__c> mstnRecs = (List<Transition_Engagement_Milestones__c>) System.JSON.deserialize(milestoneDataLst, List<Transition_Engagement_Milestones__c>.Class);
        List<Transition_Engagement_Milestones__c> lstOfmstns = new List<Transition_Engagement_Milestones__c>();
        Map<string, Sub_Tasks__mdt>  mapofMilesnNoRecs = milestonePlanController.getMilestoneMetadata();
        Transition_Engagement__c mstnPlan = new Transition_Engagement__c(Opportunity__c = opportunityId, Assigned_To__c=UserInfo.getUserId());
        try{
            insert mstnPlan;}
        catch(DmlException e) {
            //get DML exception message
            throw new AuraHandledException(e.getMessage()); 
        }
        for(Transition_Engagement_Milestones__c m: mstnRecs){
            if(m.Expected_Completion_Date__c == null){
                m.Expected_Completion_Date__c = m.Start_Date__c.addDays((Integer)mapofMilesnNoRecs.get(m.Milestone_Sequence__c).Days_to_Complete__c) ;
            }
            m.Transition_Engagement__c = mstnPlan.Id;
            lstOfmstns.add(m);
        }
        if(lstOfmstns.Size()>0)
        {
            try{
                insert lstOfmstns;}
            catch(DmlException e) {
                //get DML exception message
                throw new AuraHandledException(e.getMessage()); 
            }
        }
    }   
    /* Insert/Update Milestone Recs from Milestone Plan */
    @AuraEnabled
    public static void updateMilestoneRecs(String msPlanId, String milestoneDataLst) {  
        List <Transition_Engagement_Milestones__c> mstnRecs = (List<Transition_Engagement_Milestones__c>) System.JSON.deserialize(milestoneDataLst, List<Transition_Engagement_Milestones__c>.Class);
        Boolean flag = true;
        Map<Id, Transition_Engagement_Milestones__c> mapOfIdmRec = new Map<Id,Transition_Engagement_Milestones__c>([Select Id,Status__c from Transition_Engagement_Milestones__c where Transition_Engagement__c=: msPlanId and Status__c = 'Completed']);
        Map<string, Sub_Tasks__mdt>  mapofMilesnNoRecs = milestonePlanController.getMilestoneMetadata();
        List<Transition_Engagement_Milestones__c> lstOfmstnstbu = new List<Transition_Engagement_Milestones__c>();
        List<Transition_Engagement_Milestones__c> lstOfmstnsinsert = new List<Transition_Engagement_Milestones__c>();
        for(Transition_Engagement_Milestones__c m: mstnRecs){
            if(m.get('Id') == null){
                if(m.Expected_Completion_Date__c == null){
                    m.Expected_Completion_Date__c = m.Start_Date__c.addDays((Integer)mapofMilesnNoRecs.get(m.Milestone_Sequence__c).Days_to_Complete__c) ;
                }
                lstOfmstnsinsert.add(m);}
            else{
                if(!mapOfIdmRec.containsKey(m.Id)){
                    lstOfmstnstbu.add(m);}
            }
        }
        system.debug(lstOfmstnstbu);
        system.debug(lstOfmstnsinsert);
        if(lstOfmstnstbu.Size()>0)
        {
            try{
                update lstOfmstnstbu;}
            catch(DmlException e) {
                //get DML exception message
                throw new AuraHandledException(e.getMessage());
            }
        }
        
        if(lstOfmstnsinsert.Size()>0){
            try{
                insert lstOfmstnsinsert;}
            catch(DmlException e) {
                //get DML exception message
                throw new AuraHandledException(e.getMessage());
            }
        }
    }  
}