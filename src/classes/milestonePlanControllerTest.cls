@IsTest 
public class milestonePlanControllerTest {
    public static void dataSetup(){
        Id accRecTypeId = schema.sobjecttype.Account.getRecordTypeInfosByDeveloperName().get('Business_End_Customer').getRecordTypeId();
        Id oppRecTypeId = schema.sobjecttype.Opportunity.getRecordTypeInfosByDeveloperName().get('Business_Segment_E_G').getRecordTypeId();
        Id milesRecTypeId = schema.sobjecttype.Transition_Engagement_Milestones__c.getRecordTypeInfosByDeveloperName().get('Bid_Transition_Engagement').getRecordTypeId();
        Account engAcc = new Account(RecordTypeId = accRecTypeId, Name = 'EnGTestAccount', ABN__c ='86136533741', State__c = 'NSW');
        insert engAcc;
        Opportunity engOppty = new Opportunity(Name = 'EnGTestOppty777', RecordTypeId = oppRecTypeId,Account = engAcc, StageName = 'Qualified', CloseDate = date.today());
        insert engOppty;
        Transition_Engagement__c engMP = new Transition_Engagement__c(Opportunity__c=engOppty.Id, Assigned_To__c=UserInfo.getUserId());
        insert engMP;
    }
    
    @IsTest static void getTransitionRecordTest() {
        dataSetup();
        Test.startTest();
        Id opptyId = [Select Id from Opportunity where Name = 'EnGTestOppty777'].Id;
        Id mPId = [Select Id from Transition_Engagement__c where Opportunity__c =: opptyId].Id;
        string retValue = milestonePlanController.getTransitionRecord(opptyId);
        System.assertEquals(retValue, mPId);
        Test.stopTest();
    }
    @IsTest static void checkMilestonePlanRecordStatusTest() {
        dataSetup();
        Test.startTest();
        Id opptyId = [Select Id from Opportunity where Name = 'EnGTestOppty777'].Id;
        Transition_Engagement__c mPRec = [Select Id, Status__c from Transition_Engagement__c where Opportunity__c =: opptyId];
        mPRec.Status__c = 'Completed';
        update mPRec;
        Boolean retValue = milestonePlanController.checkMilestonePlanRecordStatus(mPRec.Id);
        System.assertEquals(retValue, true);
        Test.stopTest();
    }    
    @IsTest static void fetchMilestoneMetadataTest() {
        dataSetup();
        Test.startTest();
        Sub_Tasks__mdt metaRec = [Select Assigned_To__c, Task_Record_Type__c from Sub_Tasks__mdt where Type__c ='Milestone' LIMIT 1];
        List<Transition_Engagement_Milestones__c> milesList = milestonePlanController.fetchMilestoneMetadata(metaRec.Task_Record_Type__c);
        System.assertEquals(milesList.Size(), 13);
        Test.stopTest(); 
    }    
    @IsTest static void fetchMilestoneRecsTest() {
        dataSetup();
        string mpName;
        Test.startTest();
        Sub_Tasks__mdt metaRec = [Select Assigned_To__c, Task_Record_Type__c from Sub_Tasks__mdt where Type__c ='Milestone' LIMIT 1];
        Id opptyId = [Select Id from Opportunity where Name = 'EnGTestOppty777'].Id;
        Id mPId = [Select Id from Transition_Engagement__c where Opportunity__c =: opptyId].Id;
        Object milesList = milestonePlanController.fetchMilestoneMetadata(metaRec.Task_Record_Type__c);
        string lst = JSON.serialize(milesList);
        milestonePlanController.createMilestoneRecs(opptyId, mpName,lst);
        List<Transition_Engagement_Milestones__c> milesList1 = milestonePlanController.fetchMilestoneRecs(metaRec.Task_Record_Type__c, mPId);
        
        for(Transition_Engagement_Milestones__c m: milesList1){
            m.Status__c = 'Not required';
            break;
        }
        string lst1 = JSON.serialize(milesList1);
        milestonePlanController.updateMilestoneRecs(mPId, lst1);
        System.assertEquals(milesList1.Size(), 13);
        List<Transition_Engagement_Milestones__c> milesList2 = milestonePlanController.fetchMilestoneRecs(metaRec.Task_Record_Type__c, mPId);
        for(Transition_Engagement_Milestones__c m: milesList2){
            m.Status__c = 'Completed';
            break;
        }
        string lst2 = JSON.serialize(milesList2);
        milestonePlanController.updateMilestoneRecs(mPId, lst2);
        System.assertEquals(milesList2.Size(), 13);
        List<Transition_Engagement_Milestones__c> milesList3 = milestonePlanController.fetchMilestoneRecs(metaRec.Task_Record_Type__c, mPId);
        Test.stopTest(); 
    }    
    
}