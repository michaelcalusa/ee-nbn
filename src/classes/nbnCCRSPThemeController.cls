public class nbnCCRSPThemeController {
     @AuraEnabled
    public static String getNbnTopTailHeader() {
        /*
        nbnTopTailController tt = new nbnTopTailController();
        String header = tt.getnbnHeader();*/
        String header;
        List<nbnCommunityThemes__c> lstCommTheme = [SELECT id, Header__c FROM nbnCommunityThemes__c Where Name= 'Customer Centre Community Theme'];
        if(!lstCommTheme.isEmpty()){
            header = lstCommTheme[0].Header__c;
        }
        return header;
    }
    @AuraEnabled
    public static String getNbnTopTailFooter() {
        /*
        nbnTopTailController tt = new nbnTopTailController();
        String footer = tt.getnbnFooter();*/
        String footer;
        List<nbnCommunityThemes__c> lstCommTheme = [SELECT id, Footer__c FROM nbnCommunityThemes__c Where Name='Customer Centre Community Theme'];
        if(!lstCommTheme.isEmpty()){
            footer = lstCommTheme[0].Footer__c;
        }
        return footer;
    }
}