/***************************************************************************************************
    Class Name  :  RSPBillingCaseViewControllerTest
    Class Type  :  Test Class 
    Version     :  1.0 
    Created Date:  Jan16, 2018 
    Function    :  This class contains unit test scenarios for nbnCCRSPThemeController
    Used in     :  None
    Modification Log :
    * Developer                   Date                   Description
    * ----------------------------------------------------------------------------                 
    * Ganesh Sawant          Jan16, 2018                Created
****************************************************************************************************/

@isTest(seeAllData = false)
public class nbnCCRSPThemeControllerTest {
    static testMethod void TestMethod_nbnCCRSPThemeController() {
        String strConRecordTypeID;
        Schema.DescribeSObjectResult result = Schema.SObjectType.Contact; 
        Map<String,Schema.RecordTypeInfo> rtMapByName = result.getRecordTypeInfosByName();
        strConRecordTypeID = rtMapByName.get('Customer Contact').getRecordTypeId();    
        
        String strAccRecordTypeID;
        Schema.DescribeSObjectResult resultB = Schema.SObjectType.Account; 
        Map<String,Schema.RecordTypeInfo> rtMapByNameB = resultB.getRecordTypeInfosByName();
        strAccRecordTypeID = rtMapByNameB.get('Customer').getRecordTypeId();  
        
        profile commProfile = [select id from profile where name='RSP Customer Community Plus Profile'];
        
        Account acc = new Account(Name = 'wholesale comm', RecordTypeID = strAccRecordTypeID, OwnerId = UserInfo.getUserId());
        insert acc;
        
        Contact con = new Contact(accountid=acc.id,firstName = 'test first', lastName = 'test last', recordtypeid=strConRecordTypeID);
        insert con;
        
        Team_member__c tm = new Team_member__c(Account__c=acc.Id, Customer_Contact__c=con.Id, Role__c='Operational - Billing', Role_Type__c='All', RecordTypeId = Schema.SObjectType.Team_member__c.getRecordTypeInfosByName().get('Customer').getRecordTypeId());
        insert tm;
        
        nbnCommunityThemes__c testTheme = new nbnCommunityThemes__c(Name = 'Customer Centre Community Theme', 
                                                                   Header__c = 'Test Header', 
                                                                   Footer__c = 'Test Footer');
        insert testTheme;
        
        user communityUsr = new user();
        System.runAs (new User(Id = UserInfo.getUserId())){
            //UserRole r = new UserRole(name = 'Customer User', PortalType='CustomerPortal', PortalAccountId=acc.Id);
            communityUsr = new user(/*PortalRole = 'wholesale comm customer user',userroleid=r.id,*/contactid=con.id,LocaleSidKey='en_AU',EmailEncodingKey='ISO-8859-1',LanguageLocaleKey='en_US',TimeZoneSidKey='Australia/Sydney',CommunityNickname='testtt',Alias='tttt',Email='test@test.com',firstName = 'test first', lastName = 'test last', username='testclassuser@test.com', profileid=commProfile.id);
            insert communityUsr;
        }
        
        system.runAs(communityUsr){
            test.startTest();
			nbnCCRSPThemeController.getNbnTopTailHeader();
            nbnCCRSPThemeController.getNbnTopTailFooter();
            test.stopTest();
        }
    }
}