public class nbnCommunityThemeController {
     @AuraEnabled
    public static String getNbnTopTailHeader() {
        /*
        nbnTopTailController tt = new nbnTopTailController();
        String header = tt.getnbnHeader();*/
        String header;
        List<nbnCommunityThemes__c> lstCommTheme = [SELECT id, Header__c FROM nbnCommunityThemes__c Where Name='FormCommunityTheme'];
        if(!lstCommTheme.isEmpty()){
            system.debug('getting header');
            header = lstCommTheme[0].Header__c;
        }
        return header;
    }
    @AuraEnabled
    public static String getNbnTopTailFooter() {
        /*
        nbnTopTailController tt = new nbnTopTailController();
        String footer = tt.getnbnFooter();*/
        String footer;
        List<nbnCommunityThemes__c> lstCommTheme = [SELECT id, Footer__c FROM nbnCommunityThemes__c Where Name='FormCommunityTheme'];
        if(!lstCommTheme.isEmpty()){
            system.debug('getting footer');
            footer = lstCommTheme[0].Footer__c;
        }
        return footer;
    }
}