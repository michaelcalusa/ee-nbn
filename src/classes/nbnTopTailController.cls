public class nbnTopTailController {

    // These will contain markup pulled from the nbn form template
    // transient String nbnHtmlHead;    // Content of <head> tag 
    // transient String nbnHeader;      // The navigation bar (top)
    // transient String nbnFooter;      // The footer section (tail)
    String nbnHtmlHead;    // Content of <head> tag 
    String nbnHeader;      // The navigation bar (top)
    String nbnFooter;      // The footer section (tail)
     
    public nbnTopTailController () {

        String nbnTopTailMarkup ='';
        
        // Go get the nbn top and tail form template
        //
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint('callout:WebsiteAPI');
        HTTPResponse res = new Http().send(req);

        if (res.getStatusCode() == 200) {
            
            nbnTopTailMarkup = res.getBody();
            
            // Do my parsing
                                 
            nbnHtmlHead = parseHtmlHead(nbnTopTailMarkup);  // Extract the <head> section
            nbnHeader = parseHeader(nbnTopTailMarkup);      // Extract for navigation bar markup
            nbnFooter = parseFooter(nbnTopTailMarkup);      // Extract for footer markup;

        } else {
            // Error handling
            nbnTopTailMarkup = 'error';
        }    
    }
    
    /*
    * nbn form <head> tag
    */
    public String getnbnHtmlHead() {
        return nbnHtmlHead;
    }
    
    /*
    * nbn form header bar
    */
    public String getnbnHeader() {
        return nbnHeader;
    }

    /*
    * nbn form footer
    */
    public String getnbnFooter() {
        return nbnFooter;
    }
     
    /*
    * extract Head section from nbn markup
    */
    String parseHtmlHead (String s) {
        
        String htmlHead = s;
         
        htmlHead = relative2absoluteURLs (htmlHead);                 // firstly fix up the URLs
        htmlHead = htmlHead.substringBetween( '<head>', '</head>' ); // grab everything between the head tags
        htmlHead = htmlHead.substringBefore( '<title>' ) + htmlHead.substringAfter( '</title>' ); // remove the <title> tag
        // htmlHead = htmlHead + '<script src="https://www.nbnco.com.au/etc/designs/nbnco2/js/nbnco_responsive_analytics_public.js"></script>';
         
        return htmlHead;
    }

    /*
    * extract Header (navigation bar) markup 
    */
    String parseHeader (String s) {
        
        String header = s;
         
        header = relative2absoluteURLs (header);
        /*
        * Warning - this method of extracting the header could break if nbn change the markup
        */
        header = header.substringBetween( '<body>', 'Lorem' );
        header = header.substringBeforeLast('<div');
        
        // remove scripts
        // header = header.substringBefore( '<script>' ) + header.substringAfter( '</script>' ); 
        // header = header.substringBefore( '<script>' ) + header.substringAfter( '</script>' ); 
        // header = header.substringBefore( '<script>' ) + header.substringAfter( '</script>' ); 
  
        return header;
    }
    /*
    * extract Footer markup 
    */
    String parseFooter (String s) {
        
        String footer = s;
         
        footer = relative2absoluteURLs (footer);
        /*
        * Warning - this method of extracting the header could break if nbn change the markup
        */
        footer = footer.substringBetween( '<div class="footer">', '</body>' );
        footer = '<div class="footer">' + footer;
        // remove scripts
        // footer = footer.substringBefore( '<script>' ) + footer.substringAfter( '</script>' ); 
         
        return footer;
    }
    
    
    
    /*
    * convert relative URL references to absolute
    */
    String relative2absoluteURLs (String s) {
        
        String absoluteURLs = s;
        
        absoluteURLs = absoluteURLs.replace('href="//', 'href="https://');
        absoluteURLs = absoluteURLs.replace('src="//', 'src="https://');
        absoluteURLs = absoluteURLs.replace('= "//', '= "https://');    

        absoluteURLs = absoluteURLs.replace('href="/', 'href="https://www.nbnco.com.au/'); // should use custom config variable for nbn url
        absoluteURLs = absoluteURLs.replace('src="/', 'src="https://www.nbnco.com.au/'); // should use custom config variable for nbn url    
        absoluteURLs = absoluteURLs.replace('action="/', 'action="https://www.nbnco.com.au/'); // should use custom config variable for nbn url    
        
        
        return absoluteURLs;   
    }
        
    
/*
public PageReference redirectToParsedUrl() {
    HttpRequest req = new HttpRequest();
    req.setEndpoint(url);
    req.setMethod('GET');
    HTTPResponse res = new Http().send(req);
    if (res.getStatusCode() == 200) {
        String html = res.getBody();
        // Do your parsing
        String url = ...
        return new PageReference(url);
    } else {
        // Error handling
    }
}
*/    
    
}