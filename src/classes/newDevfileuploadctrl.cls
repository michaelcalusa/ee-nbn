public without sharing class newDevfileuploadctrl {
    
    @AuraEnabled
    public static Id saveAttachment(Id parentId, String fileName, String base64Data, String contentType) { 

        // Edit permission on parent object (Contact) is required to add attachments
        if (!Schema.sObjectType.Contact.isUpdateable()) {
            throw new System.NoAccessException();
            return null;
        }

        Attachment attachment = new Attachment();
        attachment.parentId = parentId;
        attachment.body = EncodingUtil.base64Decode(base64Data);
        attachment.name = fileName;
        attachment.contentType = contentType;
        insert attachment;
        return attachment.id;
    }
    
    @AuraEnabled
    public static newDevfileuploadctrl.taskDeliverableUpload updateTaskStatus(Id tskId){
        //newDevfileuploadctrl.taskDeliverableUpload uploadDeliverables; 
        Task tsk = [Select Id, Deliverable_Status__c,Sub_Type__c from Task where Id =:tskId];
        system.debug('##Tsklst#'+tsk);
        List<ContentDocumentWrap> contentWrapList = getListContentDocument(tsk.Id);
        try{
            if((contentWrapList != null && contentWrapList.size() > 0) || tsk.Sub_Type__c == 'PCN'){
                tsk.Deliverable_Status__c = 'Submitted';           
                update tsk;            
            }
            else if((contentWrapList != null && contentWrapList.size() <= 0)){
                return new newDevfileuploadctrl.taskDeliverableUpload(tsk.Id,tsk.Sub_Type__c,'No Attachment');
            } 
        }Catch(Exception e){
            string s = e.getmessage();
            return new newDevfileuploadctrl.taskDeliverableUpload(tsk.Id,tsk.Sub_Type__c,'No Permission');
        }         
        system.debug('##Tskupdt#'+tsk); 
        return new newDevfileuploadctrl.taskDeliverableUpload(tsk.Id,tsk.Sub_Type__c,tsk.Deliverable_Status__c) ;          
    } 
     
    @AuraEnabled
    public static List<ContentDocumentWrap> getListContentDocument(String myRecordId) {
        System.debug('******************Inside getListContentDocument');
        List<ContentDocumentWrap> ContentDocumentWraps  = new list<ContentDocumentWrap>();
        List<Id> contentDocList = new List<Id>();
        for(ContentDocumentLink cdl: [ SELECT ContentDocument.Id FROM ContentDocumentLink WHERE LinkedEntityId  = :myRecordId ]){
            contentDocList.add(cdl.ContentDocument.Id);
        }
        Map<Id,string> contentDocToContentVersion = new Map<Id,string>();
        for(ContentVersion cv :[SELECT Id, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :contentDocList]){
            contentDocToContentVersion.put(cv.ContentDocumentId,cv.Id);
        }
        for(ContentDocumentLink cdl: [ SELECT ContentDocument.Id, ContentDocument.Title, ContentDocument.ParentId, ContentDocument.createdDate, 
                                   ContentDocument.FileType, ContentDocument.ContentSize, ContentDocument.Description, ContentDocument.FileExtension,
                                   ContentDocument.CreatedBy.Name FROM ContentDocumentLink WHERE LinkedEntityId  = :myRecordId ]) {
            string createdDate = '';
            createdDate += formatDigit(cdl.ContentDocument.createdDate.day());
            createdDate += '/'+formatDigit(cdl.ContentDocument.createdDate.month());
            createdDate += '/'+string.valueOf(cdl.ContentDocument.createdDate.year());
            ContentDocumentWrap ContentDocumentWrap = new ContentDocumentWrap(false,
                                                                            cdl.ContentDocument.Id ,
                                                                            cdl.ContentDocument.Title,
                                                                            cdl.ContentDocument.FileType,
                                                                            FileSizeToString(cdl.ContentDocument.ContentSize),
                                                                            cdl.ContentDocument.Description,
                                                                            cdl.ContentDocument.FileExtension,
                                                                            createdDate,
                                                                            cdl.ContentDocument.CreatedBy.Name,
                                                                            contentDocToContentVersion.get(cdl.ContentDocument.Id)
                                                                            );
            ContentDocumentWraps.add(ContentDocumentWrap);
        }
        //passed id is that of task related to stage application
        try{
            if (string.valueOf(id.valueOf(myRecordId).getsobjecttype()) == 'NewDev_Application__c'){
                //get the right task
                NewDev_Application__c newDevApp = [SELECT Id, SF_Stage_ID__c FROM NewDev_Application__c WHERE Id = :myRecordId];
                if (newDevApp.SF_Stage_ID__c <> null && newDevApp.SF_Stage_ID__c <> ''){
                    Task t = [Select Id from Task Where WhatId = :newDevApp.SF_Stage_ID__c and Status = 'Open' LIMIT 1];
                    myRecordId = t.Id;
                }else{
                    return ContentDocumentWraps;
                }
            }
            boolean isCRModRecord = false;
            List<Task> taskList = [SELECT Id, WhatId FROM Task WHERE Id = :myRecordId LIMIT 1];
            if(taskList[0].WhatId != null && string.valueOf(taskList[0].WhatId.getsobjecttype()) == 'Stage_Application__c'){
                if([SELECT Id, Request_Id__c FROM Stage_Application__c WHERE Id = :taskList[0].WhatId LIMIT 1].Request_Id__c != null){
                    isCRModRecord = true;
                }
            }
            List<CollaborationGroup> crmodGroup = [SELECT Id, Name, CollaborationType FROM CollaborationGroup WHERE Name = 'CRMod new development user group' LIMIT 1];
            if(crmodGroup != null && crmodGroup.size() > 0 && contentDocList != null && contentDocList.size() > 0 && isCRModRecord){
                Map<Id,set<Id>> docEntityMap = new Map<Id,set<Id>>();
                List<ContentDocumentLink> conDocLinkListToInsert = new List<ContentDocumentLink>();
                List<ContentDocumentLink> contentDocumentList = [SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE ContentDocumentId IN :contentDocList];
                system.debug('contentDocumentList '+contentDocumentList);
                for(ContentDocumentLink cdl :contentDocumentList){
                    set<Id> linkedEntityIdSet = new set<Id>();
                    if(docEntityMap.containsKey(cdl.ContentDocumentId)){
                        linkedEntityIdSet = docEntityMap.get(cdl.ContentDocumentId);
                    }
                    linkedEntityIdSet.add(cdl.LinkedEntityId);
                    docEntityMap.put(cdl.ContentDocumentId,linkedEntityIdSet);
                }
                system.debug('docEntityMap '+docEntityMap);
                for(Id contentDocId :docEntityMap.keySet()){
                    if(!docEntityMap.get(contentDocId).contains(crmodGroup[0].Id)){
                        ContentDocumentLink cdl = new ContentDocumentLink(ContentDocumentId = contentDocId, LinkedEntityId = crmodGroup[0].Id, ShareType = 'V');
                        //ContentDocumentLink cdl = new ContentDocumentLink(ContentDocumentId = '0690l0000005ovz', LinkedEntityId = '0F90l0000004Xdn', ShareType = 'V');
                        conDocLinkListToInsert.add(cdl);
                    }
                }
                try{
                    system.debug('conDocLinkListToInsert '+conDocLinkListToInsert);
                    insert conDocLinkListToInsert;    
                }
                catch(exception ex){
                    system.debug(' error message: '+ex.getMessage()+' at line '+ex.getStackTraceString());
                    GlobalUtility.logMessage('Error', 'newDevfileuploadctrl', 'getListContentDocument', ' ', '', '', '', ex, 0);
                }
            }
        }catch(Exception e){
            GlobalUtility.logMessage('Error', 'newDevfileuploadctrl', 'getListContentDocument', ' ', '', '', '', e, 0);
        }
        return ContentDocumentWraps;
    }
    
    @AuraEnabled
    public static List<ContentDocumentWrap> sortDocuments(String myRecordId){
        return new List<ContentDocumentWrap>();
    }
    
    public static string formatDigit(integer num){
        if(num < 10){
            return '0'+string.valueOf(num);
        }
        else{
            return string.valueOf(num);
        }
    }
    public class ContentDocumentWrap{
        @AuraEnabled
        public Boolean checkbox {get;set;}
        @AuraEnabled
        public Id ContentDocumentId {get;set;}  
        @AuraEnabled
        public String Title {get;set;}   
        @AuraEnabled
        public String FileType {get;set;}   
        @AuraEnabled
        public string ContentSize {get;set;}   
        @AuraEnabled
        public String Description {get;set;}   
        @AuraEnabled
        public String FileExtension {get;set;}  
        @AuraEnabled
        public string uploadedDate {get;set;} 
        @AuraEnabled
        public string createdName {get;set;} 
        @AuraEnabled
        public string fileId {get;set;} 
             
        public ContentDocumentWrap(Boolean checkbox, Id ContentDocumentId, String Title, String FileType, string ContentSize, String Description, String FileExtension, string uploadedDate, string createdName, string fileId){
            this.checkbox = checkbox;
            this.ContentDocumentId = ContentDocumentId;    
            this.Title = Title;         
            this.FileType = FileType;         
            this.ContentSize = ContentSize;         
            this.Description = Description;         
            this.FileExtension = FileExtension;
            this.uploadedDate = uploadedDate;
            this.createdName = createdName; 
            this.fileId = fileId;
        }
    }
    public class taskDeliverableUpload{
        @AuraEnabled
        public Id taskId {get;set;}
        
        @AuraEnabled
        public String taskType {get;set;}
        
        @AuraEnabled
        public String status {get;set;}
              
        public taskDeliverableUpload(Id taskId, String taskType, String status){
            this.taskId = taskId;           
            this.taskType = taskType;
            this.status = status;
        }
    }
    
    @AuraEnabled
    public static List<ContentDocumentWrap> removeContentDocument(String contentDocumentId,String myRecordId) {
        List<Id> removeContentDocumentList  = new list<Id>();  
        removeContentDocumentList.add(contentDocumentId);
            
        if (removeContentDocumentList.size()>0) {
            Database.DeleteResult[] deleteResults = Database.delete(removeContentDocumentList, true);
            for(Database.DeleteResult dr : deleteResults) {                   
                if (!dr.isSuccess()) {
                    for(Database.Error err : dr.getErrors()) {
                        System.debug(LoggingLevel.Error, 'The following error has occurred.'
                            + '\n' + err.getStatusCode() + ': ' + err.getMessage()
                            + '\n fields that affected this error: ' + err.getFields());
                        // Plus further error handling as required
                    }
                }
            }
        } 
        
        return getListContentDocument(myRecordId);
        
    }
    
    public static String FileSizeToString(Long Value){
            /* string representation if a file's size, such as 2 KB, 4.1 MB, etc */
            if (Value < 1024)
              return string.valueOf(Value) + ' Bytes';
            else
            if (Value >= 1024 && Value < (1024*1024))
            {
              //KB
              Decimal kb = Decimal.valueOf(Value);
              kb = kb.divide(1024,2);
              return string.valueOf(kb) + ' kb';
            }
            else
            if (Value >= (1024*1024) && Value < (1024*1024*1024))
            {
              //MB
              Decimal mb = Decimal.valueOf(Value);
              mb = mb.divide((1024*1024),2);
              return string.valueOf(mb) + ' mb';
            }
            else
            {
              //GB
              Decimal gb = Decimal.valueOf(Value);
              gb = gb.divide((1024*1024*1024),2);
             
              return string.valueOf(gb) + ' gb';
            }    
        }
    
}