@isTest
public class newDevfileuploadctrl_Test {
    public testMethod static void test1(){
        Id rtypeId = [SELECT Id FROM RecordType WHERE Name = 'New Development' AND SobjectType = 'Task'].Id;
        Stage_Application__c stageApp = new Stage_Application__c(request_Id__c = string.valueOf(system.Today()));
        insert stageApp;
        CollaborationGroup collab = new CollaborationGroup(name = 'CRMod new development user group', CollaborationType = 'Public');
        insert collab;
        Task ts = new Task(WhatId = stageApp.Id);
        insert ts;
        newDevfileuploadctrl.saveAttachment(ts.Id, 'testFile', 'fileData', 'txt');
        ContentVersion testContentInsert =new ContentVersion(); 
        testContentInsert.ContentURL='http://www.google.com';
        testContentInsert.Title ='Google.com'; 
        insert testContentInsert;
        Id contId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :testContentInsert.Id].ContentDocumentId;
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = contId;
        cdl.LinkedEntityId = ts.Id;
        cdl.ShareType = 'V';
        insert cdl;
        newDevfileuploadctrl.updateTaskStatus(ts.Id);
        newDevfileuploadctrl.sortDocuments(ts.Id);
        newDevfileuploadctrl.removeContentDocument(contId,ts.Id);
        
    }
}