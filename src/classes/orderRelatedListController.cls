/*------------------------------------------------------------  
Author:        Jairaj Jadhav
Company:      Wipro Technologies
Description:  This class is apex controller of OrderRelatedListOnCase Aura component. This class retrives all orders
    which have same location id as on Case.
Test Class:    orderRelatedListController_Test
History
<Date>      <Authors Name>     <Brief Description of Change>

------------------------------------------------------------*/
public class orderRelatedListController{
    @AuraEnabled
    public static list<Customer_Connections_Order__c> returnOrders(ID caseid){
        case ccCase = new case();
        list<Customer_Connections_Order__c> ordrList = new list<Customer_Connections_Order__c>();
        try{
            ccCase = [select id, Loc_Id__c from case where id=:caseid];
            if(ccCase.Loc_Id__c != '' && ccCase.Loc_Id__c != null){
                ordrList = [select id, Order_ID__c, Order_Status__c, Order_Status_Change__c from Customer_Connections_Order__c where Location_ID__c=:ccCase.Loc_Id__c];
            }
            return (!ordrList.isEmpty() && ordrList.size() > 0) ? ordrList: null;
        }catch(exception ex){
            system.debug('DMLException: '+ex);
            return null;
        }
        
    }
}