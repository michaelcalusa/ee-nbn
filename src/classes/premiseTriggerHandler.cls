/************************************************************************************************************
Version                 : 1.0 
Created Date            : 09-01-2018
Description/Function    : Handler class to handle premise trigger
Used in                 : premiseTrigger -- Apex trigger
Modification Log        :
* Developer                   Date                   Description
* -------------------------------------------------------------------------                
* Ramtej Juloori             09-01-2018              Created the Class
**************************************************************************************************************/
public class premiseTriggerHandler{
    //Class level variables that are commonly used
    private boolean isExecuting = false;
    private integer batchSize;
    private List<Premise__c> trgOldList = new List<Premise__c> ();
    private List<Premise__c> trgNewList = new List<Premise__c> ();
    private Map<id,Premise__c> trgOldMap = new Map<id,Premise__c> ();
    private Map<id,Premise__c> trgNewMap = new Map<id,Premise__c> ();
    private static boolean isAsync;
    // Below 7 boolean variables are used to Prevent recursion
    public static boolean isBeforeInsertFirstRun = true;
    public static boolean isBeforeUpdateFirstRun = true;
    public static boolean isBeforeDeleteFirstRun = true;
    public static boolean isAfterInsertFirstRun = true;
    public static boolean isAfterUpdateFirstRun = true;
    public static boolean isAfterDeleteFirstRun = true;
    public static boolean isAfterUndeleteFirstRun = true;
    
    /***************************************************************************************************
    Method Name         : premiseTriggerHandler 
    Method Type         : Constructor
    Version             : 1.0 
    Created Date        : 09-01-2018 
    Function            : used to assign the Trigger values to the Class variables
    Input Parameters    : Trigger Context Variables (6 parameters - 1 boolean, 1 integer, 2 List and 2 Map) 
    Output Parameters   : None
    Description         :  
    Used in             : "PremiseTrigger" Trigger
    Modification Log    :
    * Developer                   Date                   Description
    * --------------------------------------------------------------------------------------------------                 
    * RAMTEJ JULOORI              09-01-2018                Created
    ****************************************************************************************************/ 
    public premiseTriggerHandler(){}
    public premiseTriggerHandler(boolean isExecuting, integer batchSize, List<Premise__c> trgOldList, List<Premise__c> trgNewList, Map<id,Premise__c> trgOldMap, Map<id,Premise__c> trgNewMap){
        this.isExecuting = isExecuting;
        this.BatchSize = batchSize;
        this.trgOldList = trgOldList;
        this.trgNewList = trgNewList;
        this.trgOldMap = trgOldMap;
        this.trgNewMap = trgNewMap;
        premiseTriggerHandler.isAsync = System.isBatch() || System.isFuture();
    }
    
    public void OnBeforeInsert(){
    
    }
    public void OnBeforeUpdate(){
    
    }
    public void OnBeforeDelete(){
    
    }
    public void OnAfterInsert(){
        createSiteDetailsCSV(trgNewMap);
    }
    public void OnAfterUpdate(){
    
    }
    public void OnAfterDelete(){
    
    }
    public void OnUndelete(){
    
    }
    
    /* method to create csv on task of related stage application attached with details of related site */
    public void createSiteDetailsCSV(Map<Id,Premise__c> newMap){
        try{
            List<Id> stageAppIds = new List<Id>();
            for(Premise__c prm :newMap.values()){
                stageAppIds.add(prm.Stage_Application__c);
            }
            List<Premise__c> premiseList = [SELECT Id, LIC_Type__c, Stage_Application__c, Stage_Application__r.Name, Location__c, Location__r.Unit_Number__c, Location__r.Road_Number_1__c, Location__r.Lot_Number__c, Location__r.Road_Name__c, Location__r.Locality_Name__c, Location__r.Post_Code__c, Location__r.State_Territory_Code__c FROM Premise__c WHERE Stage_Application__c IN :stageAppIds AND Stage_Application__r.Build_Type__c = 'Lead In Conduit'];
            system.debug(' premiseList '+premiseList);
            Map<Id,List<Premise__c>> stageToPremiseMap = new Map<Id,List<Premise__c>>();
            
            for(Premise__c prm :premiseList){
                List<Premise__c> prmTempList = new List<Premise__c>();
                if(stageToPremiseMap.containsKey(prm.Stage_Application__c)){
                    prmTempList = stageToPremiseMap.get(prm.Stage_Application__c);
                }
                prmTempList.add(prm);
                stageToPremiseMap.put(prm.Stage_Application__c,prmTempList);
            }            
            Map<Id,Task> stageAppTOTaskMap = createTask(stageToPremiseMap.keySet());
            List<ContentVersion> conVersList = new List<ContentVersion>();
            system.debug('stageToPremiseMap '+stageToPremiseMap);
            for(string stageKey :stageToPremiseMap.keySet()){
                string csvFileString = 'Lead-In Conduit location,Unit Number,Street Number,Lot Number,Street Name,Suburb,Postcode,State\n';
                for(Premise__c prm :stageToPremiseMap.get(stageKey)){
                    string lictype = ' ';
                    string unitNumber = ' ';
                    string roadNumber = ' ';
                    string lotNumber = ' ';
                    string roadName = ' ';
                    string localityName = ' ';
                    string postCode = ' ';
                    string state = ' ';
                    
                    if(prm.LIC_Type__c != null){
                        lictype = prm.LIC_Type__c;
                    }
                    if(prm.Location__r.Unit_Number__c != null){
                        unitNumber = prm.Location__r.Unit_Number__c;
                    }
                    if(prm.Location__r.Road_Number_1__c != null){
                        roadNumber = prm.Location__r.Road_Number_1__c;
                    }
                    if(prm.Location__r.Lot_Number__c != null){
                        lotNumber = prm.Location__r.Lot_Number__c;
                    }
                    if(prm.Location__r.Road_Name__c != null){
                        roadName = prm.Location__r.Road_Name__c;
                    }
                    if(prm.Location__r.Locality_Name__c != null){
                        localityName = prm.Location__r.Locality_Name__c;
                    }
                    if(prm.Location__r.Post_Code__c != null){
                        postCode = prm.Location__r.Post_Code__c;
                    }
                    if(prm.Location__r.State_Territory_Code__c != null){
                        state = prm.Location__r.State_Territory_Code__c;
                    }
                    csvFileString += lictype + ',' + unitNumber + ',' + roadNumber + ',' + lotNumber + ',' + roadName + ',' + localityName + ',' + postCode + ',' + state +'\n';
                }
                Blob csvBlob = Blob.valueOf(csvFileString);
                string base64Data = EncodingUtil.urlDecode('example string', 'UTF-8');
                ContentVersion cv = new ContentVersion();
                cv.ContentLocation = 'S';
                //cv.ContentDocumentId = contentDocumentId;
                //cv.VersionData = EncodingUtil.base64Decode(base64Data);
                cv.VersionData = csvBlob;
                cv.Title = stageToPremiseMap.get(stageKey)[0].Stage_Application__r.Name;
                system.debug(' stageKey '+stageKey);
                cv.PathOnClient = '/'+stageKey+'.csv';
                conVersList.add(cv);
                system.debug(' cv '+cv);
            }
            system.debug('premises csv file'+conVersList);
            insert conVersList;
            List<Id> conVersListIds = new List<Id>();
            for(ContentVersion cv :conVersList){
                conVersListIds.add(cv.Id);
            }
            List<ContentVersion> conVersListTemp = [SELECT ContentDocumentId, PathOnClient FROM ContentVersion WHERE ID IN :conVersListIds];
            List<ContentDocumentLink> contentDocTOInsert = new List<ContentDocumentLink>();
            for(ContentVersion cv :conVersListTemp){
                system.debug('cv.ContentDocumentId '+cv.ContentDocumentId);
                system.debug('conVersList '+conVersList);
                string stageKey = cv.PathOnClient.split('/')[1].split('.csv')[0];
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.ContentDocumentId = cv.ContentDocumentId;
                cdl.LinkedEntityId = stageAppTOTaskMap.get(stageKey).Id;
                cdl.shareType = 'V';
                contentDocTOInsert.add(cdl);
            }
            insert contentDocTOInsert;
            system.debug(' contentDocTOInsert '+contentDocTOInsert);
        }
        catch(exception ex){
            system.debug(' error '+ex.getMessage()+' at '+ex.getStacktraceString());
        }
    }
    
    /* create task for premise related stage application */
    
    public Map<Id,Task> createTask(set<Id> stageApplicationIds){
        List<Task> taskListToBeInserted = new List<Task>();
        Map<Id,Task> retMap = new Map<Id,Task>();
        Id rtypeId = [SELECT Id, Name FROM RecordType WHERE Name = 'New Development' AND SobjectType = 'Task'].Id;
        for(Id stageAppId :stageApplicationIds){
            Task ts = new Task(RecordtypeId = rtypeId, Status = 'Completed', Subject = 'New Development Premise List', whatId = stageAppId);
            taskListToBeInserted.add(ts);
        }
        insert taskListToBeInserted;
        for(Task ts :taskListToBeInserted){
            retMap.put(ts.whatId,ts);
        }
        return retMap;
    }
    
    

}