/* Purpose: Utility class to handle the negative responses from RPA
 *          Refer to Jira: https://jira.nbnco.net.au/browse/CUSTSA-14476 for elaborate information
 * CreatedBy: SV
 * Change log:
 * DeveloperName    ChangeMade    ModifiedTimeStamp
 *     SV            Created          17/04/2018    
 */
global with sharing class remedyRPAErrorHandlingUtil{
    
    global remedyRPAErrorHandlingUtil(){}
    
   //method to deserialize JSON for RPA error object
    global static void deserializeRPAErrorJSON(List<String> inJSONList, String responseType){
        try{
        //map of json keys and sfdc fields
        Map<String,String> json2SfdcFieldMap;
        //variable to store and process individual JSONs
        Map<String, Object> tmpJSONresp;
        List<SObject> logObjList = new List<SObject>();
        for(String JSONStr : inJSONList) {
            json2SfdcFieldMap = new Map<String,String>();
            tmpJSONresp = (Map<String, Object>) JSON.deserializeUntyped(JSONStr);
            // run through the deserialized JSON map to capture the JSON response
            if(tmpJSONresp != null) {
                //loop through RemedyIntegrationErrorHandling__c custom settings which holds the JSON - SFDC field mapping 
                for(RemedyIntegrationErrorHandling__c rih : RemedyIntegrationErrorHandling__c.getAll().values()) {
                    //check if the inbound JSON has the key & its corresponding value and update the Map 
                    if(tmpJSONresp.containsKey(rih.JSONKey__c) && tmpJSONresp.get(rih.JSONKey__c) != null)                           
                        json2SfdcFieldMap.put(rih.SalesforceField__c,(String)tmpJSONresp.get(rih.JSONKey__c));
                }
                //create a list of objects only if payload and transactionId in the payload is not empty
                if(!json2SfdcFieldMap.isEmpty() && 
                   json2SfdcFieldMap.containsKey('transactionId__c')) {
                    if(responseType.equalsIgnoreCase('Inbound'))
                        logObjList.add(createRPAErrorObj(json2SfdcFieldMap, 'RemedyIntegrationHandling__c', JSONStr, ''));
                    else if(responseType.equalsIgnoreCase('Outbound'))
                        logObjList.add(createRPAErrorObj(json2SfdcFieldMap, 'RemedyIntegrationHandling__c', '', JSONStr));  
                }
            }
        }
        //call the upsert method to upsert the records with txnId as extrenal ID
        system.debug('log object-->'+logObjList);
        if(logObjList.size()>0)
            upsertRPAErrors(logObjList,'','');
        }catch(Exception ex){ GlobalUtility.logMessage('Error', remedyRPAErrorHandlingUtil.class.getName(), 'deserializeRPAErrorJSON','','','','',ex,0); }
    }  
    
    //method to create error object
    global static SObject createRPAErrorObj(Map<String, String> json2SfdcFieldMap, String objectName, String inJSON, String outJSON) {
        objectName = (objectName == null || String.IsBlank(objectName)) ? 'RemedyIntegrationHandling__c' : objectName;
        Schema.SObjectType sobjType = Schema.getGlobalDescribe().get(objectName);
        SObject tmp = sobjType.newSObject();
        Map<String, Schema.SObjectField> fieldMap = sobjType.getDescribe().fields.getMap();
        
        if(fieldMap.containsKey('InboundJSONMessage__c') && String.IsNotBlank(inJSON))
            tmp.put('InboundJSONMessage__c', inJSON); 
        else if(fieldMap.containsKey('OutboundJSONMessage__c') && String.IsNotBlank(outJSON))
            tmp.put('OutboundJSONMessage__c', outJSON); 
       
        for(String s : json2SfdcFieldMap.keySet()) {
            if(fieldMap.containsKey(s) && json2SfdcFieldMap.get(s) != null) {
                Schema.DisplayType fieldDataType = fieldMap.get(s).getDescribe().getType();
                if(fieldDataType != Schema.DisplayType.DateTime){
                    tmp.put(s,json2SfdcFieldMap.get(s)); 
                    if(s.equalsIgnoreCase('RPA_Status__c')){
                        tmp.put('Status__c', json2SfdcFieldMap.get(s));
                        tmp.put('responseTimeStamp__c', system.now());  
                    }
                }
                else {
                    String dateAsString = json2SfdcFieldMap.get(s);
                    dateAsString = dateAsString + '000';
                    Long l = Long.valueOf(dateAsString);
                    DateTime dt = DateTime.newInstance(l);
                    tmp.put(s,dt); 
                }
            }
        }    
        return tmp;
    }
    
    //method to upsert resords in error object
    global static void upsertRPAErrors(List<SObject> rpaErrorList, String externalId, String objectName){
        objectName = (objectName == null || String.IsBlank(objectName)) ? 'RemedyIntegrationHandling__c' : objectName;
        Schema.SObjectType sobjType = Schema.getGlobalDescribe().get(objectName);
        if(sobjType.getDescribe().isCreateable() && sobjType.getDescribe().isUpdateable()) {
            if(rpaErrorList.size() > 0) {
                Schema.SObjectField extId = (String.isNotBlank(externalId) && sobjType.getDescribe().fields.getMap().get(externalId) != Null) ? sobjType.getDescribe().fields.getMap().get(externalId) : RemedyIntegrationHandling__c.Fields.transactionId__c;
                // only the below line needs to be changed when the error object changes
                List<RemedyIntegrationHandling__c> myList = new List<RemedyIntegrationHandling__c>();
                for (SObject so : rpaErrorList) {
                    RemedyIntegrationHandling__c tmpRIE = new RemedyIntegrationHandling__c();
                    for (String f : sobjType.getDescribe().fields.getMap().keySet()){
                        if(so.get(f) != null)
                            tmpRIE.put(f, so.get(f));
                    }
                    if(tmpRIE != null)
                        myList.add(tmpRIE);
                }
                Database.UpsertResult[] upList = Database.upsert(myList , extId, false);
                List<Id> objIdList = new List<Id>();
                for (Database.UpsertResult up : upList){
                    if(up.isSuccess()){
                        objIdList.add(up.getId());
                        if(up.isCreated())
                            system.debug('record created in class remedyRPAErrorHandlingUtil:'+ up.getId());
                        else
                            system.debug('record updatedin class remedyRPAErrorHandlingUtil:'+ up.getId());
                    }
                    else {
                        for(Database.Error er : up.getErrors()){
                            system.debug('error Message in class remedyRPAErrorHandlingUtil:'+ er.getMessage());
                            system.debug('fields which caused the error in class remedyRPAErrorHandlingUtil:'+ er.getFields());
                            system.debug('error code in class remedyRPAErrorHandlingUtil:'+ er.getStatusCode());
                        }
                    }
                }
                //Commenting the below code as Incidents will be updated in the trigger handler for Integration Handling Object
                /*if(objIdList.size() > 0) {
                    List<SObject> tmpSObjList = new List<SObject>();
                    Map<String, String> incNumRpaStatusMap = new Map<String, String>();
                    tmpSObjList = database.query('SELECT'+prepareFieldSet(objectName)+' FROM '+objectName+' WHERE Id IN:objIdList');
                    for(SObject inc : tmpSObjList) {
                        String status = (String)inc.get('RPA_Status__c');
                        String tmpIncNum = (String)inc.get('incidentNumber__c');
                        if(status != null && tmpIncNum != null)
                            incNumRpaStatusMap.put(tmpIncNum , status);
                    }
                    if(incNumRpaStatusMap.keySet().size() > 0){
                        List<SObject> incObjList = new List<SObject>();
                        Set<String> incList = incNumRpaStatusMap.keySet();
                        incObjList = database.query('SELECT '+prepareFieldSet('Incident_Management__c')+' FROM Incident_Management__c WHERE Name IN:incList ');
                        system.debug('-- before incident List->'+incObjList);
                        for(SObject s : incObjList) {
                            String incNum = (String) s.get('Name');
                            if (incNumRpaStatusMap.containsKey(incNum) && incNumRpaStatusMap.get(incNum) != null) {
                                if(incNumRpaStatusMap.get(incNum).equalsIgnoreCase('Failed'))
                                    s.put('RPA_Error_Occurred__c', true);   
                                else if((Boolean) s.get('RPA_Error_Occurred__c'))
                                   s.put('RPA_Error_Occurred__c', false); 
                            }
                        }
                        if(incObjList.size() > 0 ){
                            List<Database.saveResult> srList = Database.update(incObjList, false);
                        }
                        system.debug('-- after incident List->'+incObjList);
                    }
                }*/
            }
        }
        else { 
            system.debug(LoggingLevel.Error, 'This user -'+ userInfo.getUserName() + ' - doesn\'t have update access to RemedyIntegrationHandling__c object');
        }
    }
    
    //util method to generate the field set
    public static String prepareFieldSet(String ObjectName){
        Schema.SObjectType sobjType = Schema.getGlobalDescribe().get(objectName);
        String fieldStr = '';
        for( String f : sobjType.getDescribe().fields.getMap().keySet())
            fieldStr += ' ' +f+ ',';
        fieldStr = fieldStr.removeEnd(',');
        return fieldStr;
    } 
}